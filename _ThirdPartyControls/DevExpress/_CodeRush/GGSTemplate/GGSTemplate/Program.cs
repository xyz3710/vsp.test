using System;

namespace GGSTemplate
{
	public class Program
	{
		static void Main(string[] args)
		{
			string _fieldName1;
			int _fieldName2;


			// TODO: 해야할 작업
			// by SUPERMOBILE\xyz37 in 2007년 8월 28일 화요일 오후 12:42 

			// UNDONE: 제거해야할 작업
			// by SUPERMOBILE\xyz37 in 2007년 8월 28일 화요일 오후 12:44

			// NOTICE: 유지보수를 위해 필요한 작업
			// by SUPERMOBILE\xyz37 in 2007년 8월 28일 화요일 오후 12:46

			// CRITICAL: 심각한 버그
			// by SUPERMOBILE\xyz37 in 2007년 8월 28일 화요일 오후 12:47

			// TEST: 테스트용 코드
			// by SUPERMOBILE\xyz37 in 2007년 8월 28일 화요일 오후 12:49

			//_fieldName1 = IsGetDummy(_fieldName2);
			
		}

		#region For Test
		/*
			private const string MENU_WAS_DUPLICATED = "아래 메뉴가 이미 존재합니다.";
			private const string MENU_DUPLICATED_CAPTION = "메뉴 중복";
			private const string ROOT = "ROOT";
			private const string FOLDER = "FOLDER";
			private const string NEWFOLDER = "New Folder";
			private const string LEFT_MENU_AREA_PANE_KEY = "LeftMenuAreaPaneKey";
			private const string LEFT_GROUP_PANE_KEY = "LeftGroupPaneKey";
			private const string FAVORITES_KEY = "FavoritesKey";
			private const string FAVORITES_CAPTION = "즐겨찾기";
			private const string LEFT_ACTIVE_PANE_KEY = "LeftActivePaneKey";

			private delegate void RecursiveAction(UltraTreeNode node);

			private UltraTree _xTreeFavorites;
			private UltraTreeNode _rootNode;
			private DropHightLightDrawFilter _dropHighLightDrawFilter;
			private DockableControlPane _activePane;
		
			public void DockableTreeForm()
			{
				InitMainForm();
				InitNode();
				InitMenuTree();
				// 즐겨찾기 초기화
				InitFavoriteTree();
				InitDragableAction();
			}

			private void InitMainForm()
			{
				IsMdiContainer = true;
			}

			private void InitNode()
			{
				if (_rootNode == null)
            		_rootNode = new UltraTreeNode("GrandRoot", "GrandRoot");

				UltraTreeNode rootMES = AddRootNode("MES", "MES 메뉴");
				UltraTreeNode rootFGMS = AddRootNode("FGMS", "FGMS 메뉴");
				UltraTreeNode rootSYS = AddRootNode("SYS", "SYS 메뉴");

				_rootNode.Nodes.Add(rootMES);
				_rootNode.Nodes.Add(rootFGMS);
				_rootNode.Nodes.Add(rootSYS);

				rootMES.Nodes.Add("mes4", "엠이에스4");
				rootMES.Nodes.Add("mes2", "엠이에스2");
				rootMES.Nodes.Add("mes1", "엠이에스1");

				rootMES.Nodes["mes2"].Nodes.Add("mes21", "엠이에스21");
				rootMES.Nodes["mes2"].Nodes.Add("mes22", "엠이에스22");
				rootMES.Nodes["mes2"].Nodes.Add("mes23", "엠이에스23");

				rootMES.Nodes.Add("mes3", "엠이에스3");
				rootMES.Nodes.Add("mes0", "엠이에스0");

				rootFGMS.Nodes.Add("fgms3", "fgms3");
				rootFGMS.Nodes.Add("fgms1", "fgms1");
				rootFGMS.Nodes.Add("fgms2", "fgms2");
				rootFGMS.Nodes.Add("fgms4", "fgms4");

				rootSYS.Nodes.Add("Sys1", "Sys1");
				rootSYS.Nodes.Add("Sys5", "Sys5");
				rootSYS.Nodes.Add("Sys2", "Sys2");
				rootSYS.Nodes.Add("Sys3", "Sys3");
				rootSYS.Nodes.Add("Sys4", "Sys4");
				rootSYS.Nodes.Add("Sys6", "Sys6");

				RecursiveNode(_rootNode, SetMenuImageAction);

				_rootNode.ExpandAll(ExpandAllType.Always);
			}

			private UltraTreeNode AddRootNode(string key, string text)
			{
				UltraTreeNode rootNode = new UltraTreeNode(string.Format("{0}:{1}", ROOT, key), text);

				SetMenuImageAction(rootNode);
				rootNode.ExpandAll(ExpandAllType.Always);

				return rootNode;
			}

			private void SetMenuImageAction(UltraTreeNode targetNode)
			{
				if (IsRootNode(targetNode) == true)
					targetNode.Override.NodeAppearance.Image = imlMenus.Images["root"];
				else
					targetNode.Override.NodeAppearance.Image = imlMenus.Images["menu"];
			}

			private void InitMenuTree()
			{
				SuspendLayout();

				xDockManager.ImageList = imlPanes;

				DockableGroupPane dgpMenu = new DockableGroupPane(LEFT_GROUP_PANE_KEY);

				dgpMenu.ChildPaneStyle = ChildPaneStyle.TabGroup;

				// 첫번째 image는 Favorites에 할당 이후는 자동 추가
				int imageIndex = 1;

				// Root의 아래 node 만큼 Loop를 돌면서 ControlPane을 생성한다.
				foreach (UltraTreeNode node in _rootNode.Nodes)
				{
					UltraTree xTree = new UltraTree();
					DockableControlPane dcpMenu = new DockableControlPane(node.Text, node.Text, xTree);

					InitTreeAppearance(xTree);
					// Dragable EventHandler를 할당한다.
					InitDragStartAction(xTree);

					// 생성된 TreeView에 Node를 추가한다.
					xTree.Nodes.Add(node);					 

					dcpMenu.Settings.TabAppearance.Image = imlPanes.Images[imageIndex++];
					dcpMenu.Settings.AllowClose = DefaultableBoolean.False;

					// TabGroupPannel에 DockableControlPane을 추가한다.
					dgpMenu.Panes.Add(dcpMenu);
				}

				// 기본 DockAreaPane을 생성한다.
				DockAreaPane dapMenuLeft = new DockAreaPane(DockedLocation.DockedLeft, LEFT_MENU_AREA_PANE_KEY);

				dapMenuLeft.Panes.Add(dgpMenu);
				dapMenuLeft.Pin();
				dapMenuLeft.Size = new Size(200, 748);
				
				xDockManager.DefaultGroupSettings.TabSizing = TabSizing.SizeToFit;
				xDockManager.DefaultGroupSettings.TabStyle = TabStyle.PropertyPageFlat;
				xDockManager.DefaultGroupSettings.PinButtonBehavior = PinButtonBehavior.PinTabGroup;
				xDockManager.LayoutStyle = DockAreaLayoutStyle.FillContainer;
				xDockManager.DockAreas.Add(dapMenuLeft);
				// VS2005 style로 변경한 뒤 Drag Indicator가 나오게 한다.
				xDockManager.WindowStyle = WindowStyle.VisualStudio2005;
				xDockManager.DragWindowStyle = DragWindowStyle.LayeredWindowWithIndicators;

				//xDockManager.DefaultPaneSettings.BorderStylePane = UIElementBorderStyle.Rounded4;
				// Hide, AutoHide 메뉴등을 안나오게 한다.(Float 메뉴만 나온다)
				xDockManager.ShowDisabledButtons = false;

				// Float 메뉴를 구성한다.
				//dapMenuLeft.Float(false, new Rectangle(5, 50, 230, 700));

				ResumeLayout();
			}				

			private void MenuClickAction(UltraTreeNode targetNode)
			{
				if (IsRootNode(targetNode) == true)
					return;

				foreach (Form form in MdiChildren)
					if ((form.Tag as string) == targetNode.Key)
					{
						form.Activate();

						return;
					}

				MdiChildFormSample childForm = new MdiChildFormSample();

				childForm.MdiParent = this;
				childForm.Text = targetNode.Text;
				childForm.Tag = targetNode.Key;
				childForm.lblContext.Text = string.Format("Menu Caption : {0}, Menu Key : {1}\r\n{2}",
					targetNode.Text, targetNode.Key, DateTime.Now.ToShortTimeString());

				childForm.Show();
			}

			private void DeSelectAll()
			{
				UltraTree activeTree = GetActiveTree();
				
				if (activeTree != null && activeTree.Nodes.Count > 0)
            			RecursiveNode(GetActiveTree().Nodes[0].RootNode, DeSelectAction);
			}

			private void DeSelectAction(UltraTreeNode targetNode)
			{
				targetNode.Selected = false;
			}

			private void InitMenuContextMenu(UltraTree xTreeTarget, UltraTreeNode nodeAtPoint)
			{
				IGContextMenu contextMenu = new IGContextMenu();
				IGMenuItem mItemAddFavorites = new IGMenuItem(string.Format("&Add favorites [{0}]", nodeAtPoint), new EventHandler(OnAddFavoritesMenuItemClick));
				
				mItemAddFavorites.Image = imlPanes.Images["favorites"];
				mItemAddFavorites.Tag = xTreeTarget.SelectedNodes;

				contextMenu.MenuItems.Clear();
				contextMenu.MenuItems.Add(0, mItemAddFavorites);
				xTreeTarget.ContextMenu = contextMenu;
			}

			private void OnAddFavoritesMenuItemClick(object sender, EventArgs e)
			{
				IGMenuItem menuItem = sender as IGMenuItem;
				SelectedNodesCollection selectedNodes = menuItem != null ? menuItem.Tag as SelectedNodesCollection : null;
				
				if (selectedNodes != null)
					AddFavoriteMenu(selectedNodes);
			}

			public void RecursiveNode(UltraTreeNode targetNode, RecursiveAction action)
			{
				if (targetNode != null)
				{
					if (action != null)
						action(targetNode);

					if (targetNode.Nodes.Count > 0)
						foreach (UltraTreeNode xTreeNode in targetNode.Nodes)
							RecursiveNode(xTreeNode, action);
				}
			}

			public string[] GetExistsPaths(SelectedNodesCollection sourceNode, UltraTreeNode targetNode)
			{
				Dictionary<string, string> sourceKeys = GetKeynPathList(sourceNode[0]);
				Dictionary<string, string> targetKeys = GetKeynPathList(targetNode);
				List<string> duplicatedMenu = new List<string>();

				foreach (string targetKey in targetKeys.Keys)
					if (sourceKeys.ContainsKey(targetKey) == true)
						duplicatedMenu.Add(targetKeys[targetKey]);

				return duplicatedMenu.ToArray();
			}
		*/
		#endregion
	}
}
