using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

/// Framework 컴포넌
using CMAX.Framework.Common;
using CMAX.Framework.Data;
using CMAX.Framework.Win;
using CMAX.Framework.Win.Controls;
using CMAX.Framework.Win.Util;
using CMAX.Framework.Win.Validation;

/// 공통 Popup, 공통코드
using CMAX.CO.Win.CMN.BLK;

namespace CMAX.Framework.Win.Template.WinTemplate1
{
	/*==========================================================
	* 작 성자		: SUPERMOBILE\xyz37
	* 최초작성일	: 08/27/2007 16:01:59
	* 최종수정자	: SUPERMOBILE\xyz37
	* 최종수정일	: 08/27/2007 16:01:59
	* 주요변경로그
	*
	============================================================*/
	public class Form1 : CMAX.Framework.Win.WinFormBase
	{
		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
		}

		#endregion

		#region Local variables

		/// <summary>
		/// DataSet providing data on Form
		/// </summary>
		private DataSet dsForm = null;

		/// <summary>
		/// WebService
		/// </summary>
		//private ICOWebService.COWebService coWebService = null;

		//private ISMWebService.SMWebService coWebService = null;

		#endregion

		#region Initialization

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		public Form1()
			: base()
		{
			InitializeComponent();

			///
			/// Initialize Data
			/// 
			FormInitialize();
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Init dataset and other info
		/// </summary>
		private void FormInitialize()
		{
			try
			{
				/// 
				/// WebService Initialization, 각 팀마다 맞춰서 팀의 WebService이름으로 사용함
				/// 공통WebService은 CO입니다. 채번처리 기능하용하려면 아래의 coWebService으로 하면 됩니다.
				/// 필요없으면 지우십시오.
				/// 
				//coWebService = new CMAX.Framework.Win.Template.WinTemplate1.ICOWebService.COWebService();
				//coWebService.Url = AppConfigReader.Default.CMAXCOWebService;
				//coWebService.UseDefaultCredentials = true;

				///
				/// 다국어처리
				/// 
				ResourceInitialization();

				///
				/// To do: add your code here to init form's layout
				///

			}
			catch (Exception ex)
			{
				HandleWinException(ex);
			}
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// 다국어 처리
		/// </summary>
		private void ResourceInitialization()
		{
			/// label1.Text = this.ResReader.GetString("ResourceCode")

		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		protected override void OnMenuDataChanged()
		{
			base.OnMenuDataChanged();

			///
			/// Button disable
			/// 
			this.EnableToolBarButton(4, false); // 행 추가 button
			this.EnableToolBarButton(5, false); // 행 삭제 button
			this.EnableToolBarButton(6, false); // 취소 button
			this.EnableToolBarButton(7, false); // Excel export button
			this.EnableToolBarButton(8, false); // 미리보기 button
			this.EnableToolBarButton(9, false); // 출력 button
		}

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#endregion Initialization

		#region Toolbar buttons' event handlers

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Click insert button
		/// </summary>
		protected override void OnInsert()
		{

			///
			/// On inserting, do initialization like Code Auto generate
			/// 
			try
			{

			}
			catch (Exception ex)
			{
				HandleWinException(ex);
			}
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Click Save button
		/// </summary>
		protected override void OnSave()
		{
			///
			/// Do saving
			/// 		
			try
			{
			}
			catch (Exception ex)
			{
				HandleWinException(ex);
			}

		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Click View Button
		/// </summary>
		protected override void OnView()
		{
			///
			/// Do viewing, binding to grid
			/// 
			try
			{
			}
			catch (Exception ex)
			{
				HandleWinException(ex);
			}
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Click Delete button
		/// </summary>
		protected override void OnDelete()
		{
			///
			/// Do Delete
			/// 
			try
			{
			}
			catch (Exception ex)
			{
				HandleWinException(ex);
			}
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Specify the Grid Control to add new row
		/// </summary>
		protected override void OnAddRow()
		{
			///
			/// provide new row with default values
			/// 			
			//UltraGridHelper.AddNewRow(ultrGrid1);
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Specify the Grid Control to delete new row
		/// </summary>
		protected override void OnDeleteRow()
		{
			///
			/// delete current selected rows
			/// 
			//UltraGridHelper.DeleteCurrentRow(ultrGrid1);
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Click Cancel button
		/// </summary>
		protected override void OnCancel()
		{
			try
			{

			}
			catch (Exception ex)
			{
				HandleWinException(ex);
			}
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Click Export Excel
		/// </summary>
		protected override void OnExcelExport(string saveFileName)
		{
			///
			/// saveFileName is the full file path chosen
			/// 
			try
			{
				//UltraGridHelper.ExportExcel(ultraGridExcelExporter1, ultraGrid1, saveFileName);
			}
			catch (Exception ex)
			{
				HandleWinException(ex);
			}
		}

		/// <summary>
		/// Click Print Preview button
		/// </summary>
		protected override void OnPrintPreview()
		{
			///
			/// Print Preview using PrintLayout control
			/// 
		}

		/*==========================================================
		* 작 성자 : SUPERMOBILE\xyz37
		* 최초작성일 : 08/27/2007 16:01:59
		* 최종수정자 : SUPERMOBILE\xyz37
		* 최종수정일 : 08/27/2007 16:01:59
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Click on Print button
		/// </summary>
		protected override void OnPrint()
		{
			///
			/// Print using Print Layout control
			/// 
		}

		#endregion Toolbar buttons' event handlers

	}
}