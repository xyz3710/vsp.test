using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Data;

/// Framework 컴포넌
using CMAX.Framework.Common;
using CMAX.Framework.Data;
using CMAX.Framework.Win;
using CMAX.Framework.Win.Controls;
using CMAX.Framework.Win.Util;
using CMAX.Framework.Win.Validation;

/// 공통 Popup, 공통코드
using CMAX.CO.Win.CMN.BLK;

namespace CMAX.Framework.Win.Template.WinTemplate1
{
	/*==========================================================
	* 작 성자 : 하이네트 Truong Cong Loc
	* 최초작성일 : 2005-06-24
	* 최종수정자 : 하이네트 Truong Cong Loc
	* 최종수정일 : 2005-06-24
	* 주요변경로그
	============================================================*/
	public class Popup1 : CMAX.Framework.Win.PopupBase
	{
		private System.ComponentModel.IContainer components = null;

		#region Form Initialization

		public Popup1()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			///
			/// Initialize Data
			/// 
			FormInitialize();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}


		/*==========================================================
		* 작 성자 : 하이네트 Truong Cong Loc
		* 최초작성일 : 2005-11-29
		* 최종수정자 : 하이네트 Truong Cong Loc
		* 최종수정일 : 2005-11-29
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Init dataset and other info
		/// </summary>
		private void FormInitialize()
		{
			try
			{
				///
				/// 다국어처리
				/// 
				ResourceInitialization();

				///
				/// To do: add your code here to init form's layout
				///

			}
			catch (Exception ex)
			{
				HandleWinException(ex);
			}
		}

		/*==========================================================
		* 작 성자 : 하이네트 Truong Cong Loc
		* 최초작성일 : 2005-11-29
		* 최종수정자 : 하이네트 Truong Cong Loc
		* 최종수정일 : 2005-11-29
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// 다국어 처리
		/// </summary>
		private void ResourceInitialization()
		{
			/// label1.Text = this.ResReader.GetString("ResourceCode")

		}

		#endregion

		#region Buttons'  handlers

		/*==========================================================
		* 작 성자 : 하이네트 Truong Cong Loc
		* 최초작성일 : 2005-06-24
		* 최종수정자 : 하이네트 Truong Cong Loc
		* 최종수정일 : 2005-06-24
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Handling click event of Search button, F3 hotkey
		/// </summary>
		protected override void OnSearch()
		{
			///
			/// Do some query
			/// 
		}

		/*==========================================================
		* 작 성자 : 하이네트 Truong Cong Loc
		* 최초작성일 : 2005-06-24
		* 최종수정자 : 하이네트 Truong Cong Loc
		* 최종수정일 : 2005-06-24
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Handling click event of OK button
		/// </summary>
		protected override void OnOK()
		{
			///
			/// Set Return data 
			///
			// ReturnData = returnDataSet;
		}

		/*==========================================================
		* 작 성자 : 하이네트 Truong Cong Loc
		* 최초작성일 : 2005-06-24
		* 최종수정자 : 하이네트 Truong Cong Loc
		* 최종수정일 : 2005-06-24
		* 주요변경로그
		============================================================*/
		/// <summary>
		/// Handling click event of Cancel button
		/// </summary>
		protected override void OnCancel()
		{

		}

		#endregion Buttons'  handlers

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

