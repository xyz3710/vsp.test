using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.CodeRush.Core;
using DevExpress.CodeRush.PlugInCore;
using DevExpress.CodeRush.StructuralParser;

namespace CR_ExpressionAssignmentPainter
{
	public partial class PlugIn1 : StandardPlugIn
	{
		// DXCore-generated code...
		#region InitializePlugIn
		public override void InitializePlugIn()
		{
			base.InitializePlugIn();

			//
			// TODO: Add your initialization code here.
			//
		}
		#endregion
		#region FinalizePlugIn
		public override void FinalizePlugIn()
		{
			//
			// TODO: Add your finalization code here.
			//

			base.FinalizePlugIn();
		}
		#endregion

		private void PlugIn1_EditorPaintLanguageElement(EditorPaintLanguageElementEventArgs ea)
		{
			if (ea.LanguageElement is AssignmentExpression)
			{
				AssignmentExpression assignmentExpression = (AssignmentExpression)ea.LanguageElement;
				SourcePoint operatorStart = assignmentExpression.OperatorRange.Start;

				ea.PaintArgs.OverlayText(assignmentExpression.OperatorText,
					operatorStart.Line,
					operatorStart.Offset, Color.Red);

				//StrikeThrough strike = new StrikeThrough();
				Underline underLine = new Underline();

				underLine.TextView = ea.PaintArgs.TextView;
				underLine.FillColor = Color.FromArgb(128, Color.Red);
				underLine.OutlineColor = Color.Violet;
				underLine.LineHeight = 6;
				underLine.Range = assignmentExpression.Range;
				underLine.Paint(ea.PaintArgs.Graphics);
			}
		}
	}
}