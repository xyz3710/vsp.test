﻿using DevExpress.XtraPivotGrid;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PoviotGridTest
{
	public partial class Form1 : Form
	{
		public List<Page> Pages { get; set; }

		public Form1()
		{
			InitializeComponent();

			Pages = Enumerable.Range(1, 20).Select(p => new Page
			{
				PageId = p,
				PageName = $"PageName{p:00}",
				Title = $"페이지 {p:00}",
				Permission = p * 2,
				Emps = Enumerable.Range(1, 10).Select(e => new Emp
				{
					PageId = p,
					Name = $"직원 이름:{e:000}",
					Title = $"직책:{e:000}",
					Permission = p * e,
				}).ToList(),
			}).ToList();
			btnPivot2.Click += (sender, e) =>
			{
				pageBindingSource.DataSource = Pages;
			};
			pivotGridControl2.OptionsData.DataProcessingEngine = PivotDataProcessingEngine.Optimized;
			//pivotGridControl2.CustomCellValue += (sender, e) =>
			//{
			//	System.Diagnostics.Debug.WriteLine("Form1.Form1");
			//	e.Value = DateTime.Now.ToString();
			//};
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);
			btnPivot2.PerformClick();
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;

			if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
			{
				switch (keyData)
				{
					case Keys.Q | Keys.Control:
						Close();

						return true;
					case Keys.D1 | Keys.Shift:
						btnPivot1.PerformClick();

						return true;
					case Keys.D2 | Keys.Shift:
						btnPivot2.PerformClick();

						return true;
				}
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}
	}

	[System.Diagnostics.DebuggerDisplay("PageId:{PageId}, PageName:{PageName}, Title:{Title}, Emps:{Emps}", Name = "Page")]
	public class Page
	{
		[DisplayName("페이지 ID")]
		public int PageId { get; set; }

		[DisplayName("페이지 이름")]
		public string PageName { get; set; }

		[DisplayName("제    목")]
		public string Title { get; set; }

		[DisplayName("페이지 권한")]
		public int Permission { get; set; }

		[DisplayName("직원")]
		public List<Emp> Emps { get; set; }
	}

	[System.Diagnostics.DebuggerDisplay("PageId:{PageId}, Name:{Name}, Title:{Title}", Name = "Emp")]
	public class Emp
	{
		[DisplayName("페이지 ID")]
		public int PageId { get; set; }

		[DisplayName("직원 권한")]
		public int Permission { get; set; }

		[DisplayName("직원 이름")]
		public string Name { get; set; }

		[DisplayName("직위")]
		public string Title { get; set; }
	}
}
