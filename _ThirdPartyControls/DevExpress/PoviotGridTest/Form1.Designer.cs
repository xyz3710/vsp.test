﻿using DevExpress.XtraPivotGrid;

namespace PoviotGridTest
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			DevExpress.XtraPivotGrid.DataSourceColumnBinding dataSourceColumnBinding1 = new DevExpress.XtraPivotGrid.DataSourceColumnBinding();
			this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
			this.pivotGridControl2 = new DevExpress.XtraPivotGrid.PivotGridControl();
			this.fieldPageId1 = new DevExpress.XtraPivotGrid.PivotGridField();
			this.fieldPageName1 = new DevExpress.XtraPivotGrid.PivotGridField();
			this.fieldTitle1 = new DevExpress.XtraPivotGrid.PivotGridField();
			this.fieldPermission1 = new DevExpress.XtraPivotGrid.PivotGridField();
			this.fieldEmpName = new DevExpress.XtraPivotGrid.PivotGridField();
			this.fieldEmpPermission = new DevExpress.XtraPivotGrid.PivotGridField();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.btnPivot1 = new System.Windows.Forms.Button();
			this.btnPivot2 = new System.Windows.Forms.Button();
			this.pageBindingSource = new System.Windows.Forms.BindingSource(this.components);
			((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pivotGridControl2)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pageBindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// pivotGridControl1
			// 
			this.pivotGridControl1.DataMember = "Query";
			this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pivotGridControl1.Location = new System.Drawing.Point(3, 3);
			this.pivotGridControl1.Name = "pivotGridControl1";
			this.pivotGridControl1.Size = new System.Drawing.Size(1015, 257);
			this.pivotGridControl1.TabIndex = 0;
			// 
			// pivotGridControl2
			// 
			this.pivotGridControl2.DataSource = this.pageBindingSource;
			this.pivotGridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pivotGridControl2.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldPageId1,
            this.fieldPageName1,
            this.fieldTitle1,
            this.fieldPermission1,
            this.fieldEmpName,
            this.fieldEmpPermission});
			this.pivotGridControl2.Location = new System.Drawing.Point(3, 266);
			this.pivotGridControl2.Name = "pivotGridControl2";
			this.pivotGridControl2.Size = new System.Drawing.Size(1015, 257);
			this.pivotGridControl2.TabIndex = 0;
			// 
			// fieldPageId1
			// 
			this.fieldPageId1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
			this.fieldPageId1.AreaIndex = 0;
			this.fieldPageId1.Caption = "페이지 ID";
			this.fieldPageId1.FieldName = "PageId";
			this.fieldPageId1.Name = "fieldPageId1";
			this.fieldPageId1.Width = 137;
			// 
			// fieldPageName1
			// 
			this.fieldPageName1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
			this.fieldPageName1.AreaIndex = 1;
			this.fieldPageName1.Caption = "페이지 이름";
			this.fieldPageName1.FieldName = "PageName";
			this.fieldPageName1.Name = "fieldPageName1";
			// 
			// fieldTitle1
			// 
			this.fieldTitle1.AreaIndex = 0;
			this.fieldTitle1.Caption = "제    목";
			this.fieldTitle1.FieldName = "Title";
			this.fieldTitle1.Name = "fieldTitle1";
			// 
			// fieldPermission1
			// 
			this.fieldPermission1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
			this.fieldPermission1.AreaIndex = 0;
			this.fieldPermission1.Caption = "페이지 권한";
			this.fieldPermission1.FieldName = "Permission";
			this.fieldPermission1.Name = "fieldPermission1";
			// 
			// fieldEmpName
			// 
			this.fieldEmpName.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
			this.fieldEmpName.AreaIndex = 0;
			this.fieldEmpName.Caption = "직원 이름";
			dataSourceColumnBinding1.ColumnName = "Emps.Name";
			this.fieldEmpName.DataBinding = dataSourceColumnBinding1;
			this.fieldEmpName.FieldName = "Emps.Name";
			this.fieldEmpName.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.Custom;
			this.fieldEmpName.Name = "fieldEmpName";
			this.fieldEmpName.UnboundFieldName = "pivotGridField1";
			// 
			// fieldEmpPermission
			// 
			this.fieldEmpPermission.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
			this.fieldEmpPermission.AreaIndex = 1;
			this.fieldEmpPermission.Caption = "직원 권한";
			this.fieldEmpPermission.FieldName = "Emps.Permission";
			this.fieldEmpPermission.Name = "fieldEmpPermission";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.pivotGridControl1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.pivotGridControl2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1021, 559);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Controls.Add(this.btnPivot1, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.btnPivot2, 1, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 526);
			this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(1021, 33);
			this.tableLayoutPanel2.TabIndex = 1;
			// 
			// btnPivot1
			// 
			this.btnPivot1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnPivot1.Location = new System.Drawing.Point(3, 3);
			this.btnPivot1.Name = "btnPivot1";
			this.btnPivot1.Size = new System.Drawing.Size(504, 27);
			this.btnPivot1.TabIndex = 0;
			this.btnPivot1.Text = "Pivot1 Binding";
			this.btnPivot1.UseVisualStyleBackColor = true;
			// 
			// btnPivot2
			// 
			this.btnPivot2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.btnPivot2.Location = new System.Drawing.Point(513, 3);
			this.btnPivot2.Name = "btnPivot2";
			this.btnPivot2.Size = new System.Drawing.Size(505, 27);
			this.btnPivot2.TabIndex = 0;
			this.btnPivot2.Text = "Pivot2 Binding";
			this.btnPivot2.UseVisualStyleBackColor = true;
			// 
			// pageBindingSource
			// 
			this.pageBindingSource.DataSource = typeof(PoviotGridTest.Page);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1021, 559);
			this.Controls.Add(this.tableLayoutPanel1);
			this.KeyPreview = true;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pivotGridControl2)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pageBindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
		private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Button btnPivot1;
		private System.Windows.Forms.Button btnPivot2;
		private System.Windows.Forms.BindingSource pageBindingSource;
		private DevExpress.XtraPivotGrid.PivotGridField fieldPageId1;
		private DevExpress.XtraPivotGrid.PivotGridField fieldPageName1;
		private DevExpress.XtraPivotGrid.PivotGridField fieldTitle1;
		private DevExpress.XtraPivotGrid.PivotGridField fieldPermission1;
		private DevExpress.XtraPivotGrid.PivotGridField fieldEmpName;
		private DevExpress.XtraPivotGrid.PivotGridField fieldEmpPermission;
	}
}

