﻿using DevExpress.XtraEditors.Repository;

using Medieng.Common.Security;

namespace PermissionRepositoryItemTest
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.gridControl = new DevExpress.XtraGrid.GridControl();
			this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
			this._permissionControlCheckBox1 = new DevExpress.XtraEditors.Repository.PermissionControlCheckButton();
			this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
			this._permissionControlCheckBox2 = new DevExpress.XtraEditors.Repository.PermissionControlCheckButton();
			this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
			this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.lcPermission = new DevExpress.XtraEditors.LabelControl();
			this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
			this.permissionControlLabel1 = new DevExpress.XtraEditors.Repository.PermissionControlLabel();
			this.pivotGridControl1 = new DevExpress.XtraPivotGrid.PivotGridControl();
			this.pageBindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.fieldPageId1 = new DevExpress.XtraPivotGrid.PivotGridField();
			this.fieldMenu1 = new DevExpress.XtraPivotGrid.PivotGridField();
			this.fieldPagePermissionPermission = new DevExpress.XtraPivotGrid.PivotGridField();
			this.riPermission = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
			this.pivotGridField1 = new DevExpress.XtraPivotGrid.PivotGridField();
			this.fieldPagePermissionSimplePermission = new DevExpress.XtraPivotGrid.PivotGridField();
			this.riSimplePermission = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
			this.panel1 = new System.Windows.Forms.Panel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pageBindingSource)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.riPermission)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.riSimplePermission)).BeginInit();
			this.panel1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// gridControl
			// 
			this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.gridControl.Location = new System.Drawing.Point(3, 3);
			this.gridControl.MainView = this.gridView;
			this.gridControl.Name = "gridControl";
			this.gridControl.Size = new System.Drawing.Size(1202, 174);
			this.gridControl.TabIndex = 0;
			this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
			// 
			// gridView
			// 
			this.gridView.GridControl = this.gridControl;
			this.gridView.Name = "gridView";
			// 
			// _permissionControlCheckBox1
			// 
			this._permissionControlCheckBox1.EditValue = ((Medieng.Common.Security.Permission)(((Medieng.Common.Security.Permission.Read | Medieng.Common.Security.Permission.Update) 
            | Medieng.Common.Security.Permission.Create)));
			this._permissionControlCheckBox1.Location = new System.Drawing.Point(13, 14);
			this._permissionControlCheckBox1.Name = "_permissionControlCheckBox1";
			this._permissionControlCheckBox1.Permission = ((Medieng.Common.Security.Permission)(((Medieng.Common.Security.Permission.Read | Medieng.Common.Security.Permission.Update) 
            | Medieng.Common.Security.Permission.Create)));
			this._permissionControlCheckBox1.Size = new System.Drawing.Size(184, 36);
			this._permissionControlCheckBox1.TabIndex = 1;
			// 
			// checkEdit1
			// 
			this.checkEdit1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.checkEdit1.Location = new System.Drawing.Point(521, 25);
			this.checkEdit1.Name = "checkEdit1";
			this.checkEdit1.Properties.AutoHeight = false;
			this.checkEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
			this.checkEdit1.Properties.Caption = "";
			this.checkEdit1.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom;
			this.checkEdit1.Properties.ImageOptions.ImageChecked = global::PermissionRepositoryItemTest.Properties.Resources.Check;
			this.checkEdit1.Properties.ImageOptions.ImageGrayed = global::PermissionRepositoryItemTest.Properties.Resources.Check_Disabled;
			this.checkEdit1.Size = new System.Drawing.Size(167, 102);
			this.checkEdit1.TabIndex = 2;
			// 
			// _permissionControlCheckBox2
			// 
			this._permissionControlCheckBox2.EditValue = Medieng.Common.Security.Permission.AccessDenied;
			this._permissionControlCheckBox2.Location = new System.Drawing.Point(13, 56);
			this._permissionControlCheckBox2.Name = "_permissionControlCheckBox2";
			this._permissionControlCheckBox2.Permission = Medieng.Common.Security.Permission.AccessDenied;
			this._permissionControlCheckBox2.Size = new System.Drawing.Size(184, 36);
			this._permissionControlCheckBox2.TabIndex = 1;
			// 
			// labelControl1
			// 
			this.labelControl1.Appearance.BackColor = System.Drawing.Color.White;
			this.labelControl1.Appearance.Options.UseBackColor = true;
			this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.labelControl1.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Excel_Disabled;
			this.labelControl1.Location = new System.Drawing.Point(106, 99);
			this.labelControl1.Name = "labelControl1";
			this.labelControl1.Size = new System.Drawing.Size(57, 56);
			this.labelControl1.TabIndex = 3;
			// 
			// simpleButton1
			// 
			this.simpleButton1.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Search;
			this.simpleButton1.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
			this.simpleButton1.Location = new System.Drawing.Point(13, 98);
			this.simpleButton1.Name = "simpleButton1";
			this.simpleButton1.Size = new System.Drawing.Size(34, 29);
			this.simpleButton1.TabIndex = 4;
			this.simpleButton1.Text = "simpleButton1";
			// 
			// labelControl2
			// 
			this.labelControl2.Appearance.BackColor = System.Drawing.Color.White;
			this.labelControl2.Appearance.Options.UseBackColor = true;
			this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.labelControl2.ImageOptions.DisabledImage = global::PermissionRepositoryItemTest.Properties.Resources.Search_Disabled;
			this.labelControl2.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Search;
			this.labelControl2.Location = new System.Drawing.Point(13, 132);
			this.labelControl2.Name = "labelControl2";
			this.labelControl2.Size = new System.Drawing.Size(34, 30);
			this.labelControl2.TabIndex = 3;
			// 
			// lcPermission
			// 
			this.lcPermission.Appearance.BackColor = System.Drawing.Color.White;
			this.lcPermission.Appearance.Options.UseBackColor = true;
			this.lcPermission.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lcPermission.Location = new System.Drawing.Point(264, 20);
			this.lcPermission.Name = "lcPermission";
			this.lcPermission.Size = new System.Drawing.Size(230, 30);
			this.lcPermission.TabIndex = 3;
			// 
			// checkEdit2
			// 
			this.checkEdit2.Location = new System.Drawing.Point(54, 99);
			this.checkEdit2.Name = "checkEdit2";
			this.checkEdit2.Properties.AutoHeight = false;
			this.checkEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
			this.checkEdit2.Properties.Caption = "";
			this.checkEdit2.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.Custom;
			this.checkEdit2.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.checkEdit2.Properties.GlyphVerticalAlignment = DevExpress.Utils.VertAlignment.Center;
			this.checkEdit2.Properties.ImageOptions.ImageChecked = global::PermissionRepositoryItemTest.Properties.Resources.Check;
			this.checkEdit2.Properties.ImageOptions.ImageGrayed = global::PermissionRepositoryItemTest.Properties.Resources.Check_Disabled;
			this.checkEdit2.Size = new System.Drawing.Size(46, 28);
			this.checkEdit2.TabIndex = 5;
			// 
			// permissionControlLabel1
			// 
			this.permissionControlLabel1.EditValue = Medieng.Common.Security.Permission.AccessDenied;
			this.permissionControlLabel1.Location = new System.Drawing.Point(264, 56);
			this.permissionControlLabel1.Name = "permissionControlLabel1";
			this.permissionControlLabel1.Permission = Medieng.Common.Security.Permission.AccessDenied;
			this.permissionControlLabel1.Size = new System.Drawing.Size(216, 42);
			this.permissionControlLabel1.TabIndex = 6;
			// 
			// pivotGridControl1
			// 
			this.pivotGridControl1.DataSource = this.pageBindingSource;
			this.pivotGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pivotGridControl1.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.fieldPageId1,
            this.fieldMenu1,
            this.fieldPagePermissionPermission,
            this.pivotGridField1,
            this.fieldPagePermissionSimplePermission});
			this.pivotGridControl1.Location = new System.Drawing.Point(3, 183);
			this.pivotGridControl1.Name = "pivotGridControl1";
			this.pivotGridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riPermission,
            this.riSimplePermission});
			this.pivotGridControl1.Size = new System.Drawing.Size(1202, 416);
			this.pivotGridControl1.TabIndex = 7;
			// 
			// pageBindingSource
			// 
			this.pageBindingSource.DataSource = typeof(PermissionRepositoryItemTest.Page);
			// 
			// fieldPageId1
			// 
			this.fieldPageId1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
			this.fieldPageId1.AreaIndex = 0;
			this.fieldPageId1.Caption = "페이지 Id";
			this.fieldPageId1.FieldName = "PageId";
			this.fieldPageId1.Name = "fieldPageId1";
			// 
			// fieldMenu1
			// 
			this.fieldMenu1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
			this.fieldMenu1.AreaIndex = 1;
			this.fieldMenu1.Caption = "페이지 이름";
			this.fieldMenu1.FieldName = "Menu";
			this.fieldMenu1.Name = "fieldMenu1";
			// 
			// fieldPagePermissionPermission
			// 
			this.fieldPagePermissionPermission.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
			this.fieldPagePermissionPermission.AreaIndex = 0;
			this.fieldPagePermissionPermission.Caption = "페이지 권한";
			this.fieldPagePermissionPermission.FieldEdit = this.riPermission;
			this.fieldPagePermissionPermission.FieldName = "PagePermission.Permission";
			this.fieldPagePermissionPermission.Name = "fieldPagePermissionPermission";
			this.fieldPagePermissionPermission.Options.ShowGrandTotal = false;
			this.fieldPagePermissionPermission.Options.ShowTotals = false;
			// 
			// riPermission
			// 
			this.riPermission.AutoHeight = false;
			this.riPermission.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.riPermission.Name = "riPermission";
			// 
			// pivotGridField1
			// 
			this.pivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
			this.pivotGridField1.AreaIndex = 0;
			this.pivotGridField1.Caption = "작업자";
			this.pivotGridField1.FieldName = "PagePermission.Person";
			this.pivotGridField1.Name = "pivotGridField1";
			// 
			// fieldPagePermissionSimplePermission
			// 
			this.fieldPagePermissionSimplePermission.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
			this.fieldPagePermissionSimplePermission.AreaIndex = 1;
			this.fieldPagePermissionSimplePermission.FieldEdit = this.riSimplePermission;
			this.fieldPagePermissionSimplePermission.FieldName = "PagePermission.SimplePermission";
			this.fieldPagePermissionSimplePermission.Name = "fieldPagePermissionSimplePermission";
			// 
			// riSimplePermission
			// 
			this.riSimplePermission.AutoHeight = false;
			this.riSimplePermission.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
			this.riSimplePermission.Name = "riSimplePermission";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this._permissionControlCheckBox1);
			this.panel1.Controls.Add(this._permissionControlCheckBox2);
			this.panel1.Controls.Add(this.checkEdit1);
			this.panel1.Controls.Add(this.permissionControlLabel1);
			this.panel1.Controls.Add(this.labelControl1);
			this.panel1.Controls.Add(this.checkEdit2);
			this.panel1.Controls.Add(this.labelControl2);
			this.panel1.Controls.Add(this.simpleButton1);
			this.panel1.Controls.Add(this.lcPermission);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(3, 605);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1202, 171);
			this.panel1.TabIndex = 8;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 1;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.gridControl, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.pivotGridControl1, 0, 1);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 3;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 176F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1208, 779);
			this.tableLayoutPanel1.TabIndex = 9;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1208, 779);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "PermissionRepositoryItemTest";
			((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pivotGridControl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pageBindingSource)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.riPermission)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.riSimplePermission)).EndInit();
			this.panel1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private DevExpress.XtraGrid.GridControl gridControl;
		private DevExpress.XtraGrid.Views.Grid.GridView gridView;
		private DevExpress.XtraEditors.Repository.PermissionControlCheckButton _permissionControlCheckBox1;
		private DevExpress.XtraEditors.CheckEdit checkEdit1;
		private PermissionControlCheckButton _permissionControlCheckBox2;
		private DevExpress.XtraEditors.LabelControl labelControl1;
		private DevExpress.XtraEditors.SimpleButton simpleButton1;
		private DevExpress.XtraEditors.LabelControl labelControl2;
		private DevExpress.XtraEditors.LabelControl lcPermission;
		private DevExpress.XtraEditors.CheckEdit checkEdit2;
		private PermissionControlLabel permissionControlLabel1;
		private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControl1;
		private System.Windows.Forms.BindingSource pageBindingSource;
		private DevExpress.XtraPivotGrid.PivotGridField fieldPageId1;
		private DevExpress.XtraPivotGrid.PivotGridField fieldMenu1;
		private DevExpress.XtraPivotGrid.PivotGridField fieldPagePermissionPermission;
		private DevExpress.XtraPivotGrid.PivotGridField pivotGridField1;
		private RepositoryItemCheckedComboBoxEdit riPermission;
		private DevExpress.XtraPivotGrid.PivotGridField fieldPagePermissionSimplePermission;
		private RepositoryItemCheckedComboBoxEdit riSimplePermission;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
	}
}

