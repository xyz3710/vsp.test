﻿using Medieng.Common.Security;

using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PermissionRepositoryItemTest
{
	[System.Diagnostics.DebuggerDisplay("Menu:{Menu}, Person1:{Person1}, Person2:{Person2}, Person3:{Person3}, Person4:{Person4}, Person5:{Person5}", Name = "Page")]
	public class Page
	{
		[DisplayName("페이지 Id")]
		public int PageId { get; set; }

		[DisplayName("페이지 이름")]
		public string Menu { get; set; }

		[DisplayName("사람 1")]
		public Permission Person1 { get; set; }
		[DisplayName("사람 2")]
		public Permission Person2 { get; set; }
		[DisplayName("사람 3")]
		public Permission Person3 { get; set; }
		[DisplayName("사람 4")]
		public Permission Person4 { get; set; }
		[DisplayName("사람 5")]
		public Permission Person5 { get; set; }

		[DisplayName("페이지 권한")]
		public PagePermission PagePermission { get; set; }

		/// <summary>
		/// Employee를 나타내는 String을 반환합니다. 
		/// </summary>
		/// <returns>전체 Property의 값</returns>
		public override string ToString()
		{
			return $"Menu:{Menu}, Person:{Person1}";
		}
	}

	public class PagePermission
	{
		[DisplayName("페이지 Id")]
		public int PageId { get; set; }

		//[DisplayName("권한")]
		public Permission Permission { get; set; }

		[DisplayName("권한")]
		public SimplePermission SimplePermission
		{
			get => (SimplePermission)Permission;
			set => Permission = (Permission)value;
		}

		[DisplayName("작업자")]
		public string Person { get; set; }
	}
}
