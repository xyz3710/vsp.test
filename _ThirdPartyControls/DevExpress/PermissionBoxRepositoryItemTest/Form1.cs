﻿using DevExpress.Printing;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Columns;

using Medieng.Common.Security;

using PermissionRepositoryItemTest.Properties;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PermissionRepositoryItemTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;

			if ((msg.Msg == WM_KEYDOWN) || (msg.Msg == WM_SYSKEYDOWN))
			{
				switch (keyData)
				{
					case Keys.Q | Keys.Control:
						Close();

						return true;
					case Keys.D1 | Keys.Shift:

						return true;
				}
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}

		public List<Page> Pages;
		public List<Page> PivotPages;
		private PermissionRepositoryItem irPermission;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			Pages = new List<Page> {
				new Page { Menu = "페이지1", PageId = 1,  Person1 = Permission.UpdateCreate, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지2", PageId = 2,  Person1 = Permission.UpdateDelete, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지3", PageId = 3,  Person1 = Permission.Read | Permission.Export, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지4", PageId = 4,  Person1 = Permission.UpdateCreate, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지5", PageId = 5,  Person1 = Permission.UpdateDelete, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지6", PageId = 6,  Person1 = Permission.Read | Permission.Export, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지7", PageId = 7,  Person1 = Permission.UpdateCreate, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지8", PageId = 8,  Person1 = Permission.UpdateDelete, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지9", PageId = 9,  Person1 = Permission.Read | Permission.Export, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지10",PageId = 10, Person1 = Permission.UpdateCreate, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지11",PageId = 11, Person1 = Permission.UpdateDelete, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
				new Page { Menu = "페이지12",PageId = 12, Person1 = Permission.Read | Permission.Export, Person2= Permission.Read, Person3 = Permission.Create, Person4 = Permission.Update, Person5= Permission.Export },
			};

			//var prItem = new PermissionRepositoryItem(typeof(PermissionControlCheckButton));	// 선택 가능
			irPermission = new PermissionRepositoryItem(typeof(PermissionControlCheckEdit));  // 선택 불가능

			gridControl.DataSource = Pages;
			gridControl.RepositoryItems.Add(irPermission);

			//gridView.Columns[nameof(Employee.Name)].BestFit();

			foreach (var column in gridView.Columns.Cast<GridColumn>())
			{
				if (column.ColumnType != typeof(Permission))
				{
					continue;
				}

				column.ColumnEdit = irPermission;
				column.Width = 40;
			}
			gridView.BestFitColumns();

			PivotPages = new List<Page>();
			var pp = new List<PagePermission>
			{
				new PagePermission{ PageId = 1, Person = "작업자 = 1", Permission = Permission.Create },
				new PagePermission{ PageId = 2, Person = "작업자 = 2", Permission = Permission.Read },
				new PagePermission{ PageId = 3, Person = "작업자 = 3", Permission = Permission.Update },
				new PagePermission{ PageId = 4, Person = "작업자 = 4", Permission = Permission.Delete },
				new PagePermission{ PageId = 5, Person = "작업자 = 5", Permission = Permission.UpdateCreate },
				new PagePermission{ PageId = 6, Person = "작업자 = 6", Permission = Permission.UpdateDelete },
				new PagePermission{ PageId = 7, Person = "작업자 = 7", Permission = Permission.Create },
				new PagePermission{ PageId = 8, Person = "작업자 = 8", Permission = Permission.Delete },
				new PagePermission{ PageId = 9, Person = "작업자 = 9", Permission = Permission.Create },
				new PagePermission{ PageId =10, Person = "작업자 =10", Permission = Permission.Update },
				new PagePermission{ PageId =11, Person = "작업자 =11", Permission = Permission.Create },
				new PagePermission{ PageId =12, Person = "작업자 =12", Permission = Permission.Create },
			};

			foreach (var p in Pages)
			{
				PivotPages.AddRange(pp.Where(x => x.PageId != p.PageId).Select(x => new Page
				{
					PageId = x.PageId,
					Menu = p.Menu,
					PagePermission = x,
				}));
			}

			var riIndex = new PermissionRepositoryItem(typeof(PermissionControlLabel));
			pivotGridControl1.DataSource = PivotPages;
			pivotGridControl1.OptionsView.HideAllTotals();
			riPermission.AddEnum<Permission>();
			pivotGridControl1.RepositoryItems.Add(irPermission);
			fieldPagePermissionPermission.FieldEdit = irPermission;
			//pivotGridControl1.CustomCellEdit += (sender, ea) =>
			//{
			//	System.Diagnostics.Debug.WriteLine(ea.ColumnField, "Form1.PivotGridControl1_CustomCellEdit");
			//	if (ea.DataField.DataType == typeof(Permission))
			//	{
			//		if (ea.RowIndex == 0)
			//		{
			//			ea.RepositoryItem = riIndex;
			//		}
			//		else
			//		{
			//			ea.RepositoryItem = irPermission;
			//		}
			//	}
			//};
			//var riText = new PermissionRepositoryItem(typeof(PermissionControlText));
			//riText.AddEnum<SimplePermission>();
			//fieldPagePermissionSimplePermission.FieldEdit = riText;
			riSimplePermission.AddEnum<SimplePermission>();
			fieldPagePermissionSimplePermission.FieldEdit = riSimplePermission;

			pivotGridControl1.CustomEditValue += (sender, ea) =>
			{
				if (ea.RowIndex == 0)
				{
					ea.Value = Permission.All;
				}
			};

			var toggle = true;
			labelControl1.Click += (sender, ea) =>
			{
				if (toggle)
				{
					labelControl1.ImageOptions.Image = Resources.Excel;
				}
				else
				{
					labelControl1.ImageOptions.Image = Resources.Excel_Disabled;
				}
				toggle = !toggle;
			};

			_permissionControlCheckBox1.EditValueChanged += (sender, ea) =>
			{
				lcPermission.Text = _permissionControlCheckBox1.Permission.ToString();
			};
			permissionControlLabel1.EditValue = Permission.UpdateDelete;
			permissionControlLabel1.EditValueChanged += (sender, ea) =>
			{
				lcPermission.Text = permissionControlLabel1.Permission.ToString();
			};
		}
	}
}
