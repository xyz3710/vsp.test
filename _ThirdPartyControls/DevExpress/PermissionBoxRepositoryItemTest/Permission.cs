﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Medieng.Common.Security
{
	[Flags]
	public enum Permission
	{
		AccessDenied = 0,

		[Display(Name = "읽기")]
		Read = 1 << 0,
		[Display(Name = "수정")]
		Update = 1 << 1,
		[Display(Name = "추가")]
		Create = 1 << 2,
		[Display(Name = "삭제")]
		Delete = 1 << 3,
		[Display(Name = "엑셀")]
		Export = 1 << 4,

		UpdateCreate = Read | Update | Create,
		UpdateDelete = Read | Update | Delete,

		All = Read | Update | Create | Delete | Export
	}

	[Flags]
	public enum SimplePermission
	{
		[Display(Name = "R")]
		Read = 1 << 0,
		[Display(Name = "U")]
		Update = 1 << 1,
		[Display(Name = "C")]
		Create = 1 << 2,
		[Display(Name = "D")]
		Delete = 1 << 3,
		[Display(Name = "E")]
		Export = 1 << 4,

		//[Display(Name = "모두")]
		//All = Read | Update | Create | Delete | Export
	}

	public static class PermissionExtensions
	{
		/// <summary>
		/// permission에 권한을 추가 합니다.
		/// </summary>
		/// <param name="permission">permission</param>
		/// <param name="targetPermission">추가하려는 permission</param>
		/// <returns></returns>
		public static Permission Add(this Permission permission, Permission targetPermission)
		{
			return permission | targetPermission;
		}

		/// <summary>
		/// permission에 권한을 삭제 합니다.
		/// </summary>
		/// <param name="permission">permission</param>
		/// <param name="targtPermission">삭제하려는 permission</param>
		/// <returns></returns>
		public static Permission Remove(this Permission permission, Permission targtPermission)
		{
			return permission &= ~targtPermission;
		}

		/// <summary>
		/// permission이 masking 권한을 맞족하는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permission"></param>
		/// <param name="masking"></param>
		/// <returns></returns>
		public static bool InMasking(this Permission permission, Permission masking)
		{
			return permission != Permission.AccessDenied && (masking & permission) == permission;
		}

		/// <summary>
		/// permission이 검사하려는 predicatePermission을 갖고 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <param name="predicatePermission">체크하려는 permission</param>
		/// <returns></returns>
		public static bool HasPermission(this Permission permissions, Permission predicatePermission)
		{
			return permissions != Permission.AccessDenied && (permissions & predicatePermission) == predicatePermission;
		}

		/// <summary>
		/// permission에서 predicatePermission을 value 에 의해 추가하거나 제거한다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <param name="predicatePermission">체크하려는 permission</param>
		/// <returns></returns>
		public static void SetPermission(this Permission permissions, bool value, Permission predicatePermission)
		{
			if (value)
			{
				permissions.Add(predicatePermission);
			}
			else
			{
				permissions.Remove(predicatePermission);
			}
		}

		/// <summary>
		/// All permission이 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <returns></returns>
		public static bool CanAll(this Permission permissions)
		{
			return permissions.HasPermission(Permission.All);
		}

		/// <summary>
		/// Read permission이 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <returns></returns>
		public static bool CanRead(this Permission permissions)
		{
			return permissions.HasPermission(Permission.Read);
		}

		/// <summary>
		/// Update permission이 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <returns></returns>
		public static bool CanUpdate(this Permission permissions)
		{
			return permissions.HasPermission(Permission.Update);
		}

		/// <summary>
		/// Create permission이 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <returns></returns>
		public static bool CanCreate(this Permission permissions)
		{
			return permissions.HasPermission(Permission.Create);
		}

		/// <summary>
		/// Delete permission이 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <returns></returns>
		public static bool CanDelete(this Permission permissions)
		{
			return permissions.HasPermission(Permission.Delete);
		}

		/// <summary>
		/// UpdateCreate permission이 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <returns></returns>
		public static bool CanUpdateCreate(this Permission permissions)
		{
			return permissions.HasPermission(Permission.UpdateCreate);
		}

		/// <summary>
		/// UpdateDelete permission이 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <returns></returns>
		public static bool CanUpdateDelete(this Permission permissions)
		{
			return permissions.HasPermission(Permission.UpdateDelete);
		}

		/// <summary>
		/// Export permission이 있는지 여부를 검사 합니다.
		/// </summary>
		/// <param name="permissions">Masking된 permission</param>
		/// <returns></returns>
		public static bool CanExport(this Permission permissions)
		{
			return permissions.HasPermission(Permission.Export);
		}
	}
}
