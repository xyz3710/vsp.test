﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.Mask;

namespace DevExpress.XtraEditors.Repository
{
	[UserRepositoryItem("Register")]
	public class PermissionRepositoryItem : RepositoryItem
	{
		private static readonly object drawControlInit = new object();
		private static readonly object editorControlInit = new object();
		private static readonly object controlTypeChanged = new object();

		public event EventHandler ControlTypeChanged
		{
			add { this.Events.AddHandler(controlTypeChanged, value); }
			remove { this.Events.RemoveHandler(controlTypeChanged, value); }
		}
		public event EventHandler EditorControlInitialized
		{
			add { this.Events.AddHandler(editorControlInit, value); }
			remove { this.Events.RemoveHandler(editorControlInit, value); }
		}
		public event EventHandler DrawControlInitialized
		{
			add { this.Events.AddHandler(drawControlInit, value); }
			remove { this.Events.RemoveHandler(drawControlInit, value); }
		}

		private UserControl _drawControl;
		internal UserControl DrawControl
		{
			get
			{
				if (_drawControl == null)
				{
					if (ControlType == null)
					{
						return null;
					}

					var constructor = ControlType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.CreateInstance | BindingFlags.NonPublic, null, new Type[] { }, null);
					_drawControl = constructor.Invoke(null) as UserControl;

					if (constructor == null)
					{
						return null;
					}

					OnDrawControlInitialized();
				}

				return _drawControl;
			}
		}

		private UserControl _editorControl;
		internal UserControl EditorControl
		{
			get
			{
				if (_editorControl == null)
				{
					if (ControlType == null)
					{
						return null;
					}

					var constructor = ControlType.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.CreateInstance | BindingFlags.NonPublic, null, new Type[] { }, null);

					if (constructor == null)
					{
						return null;
					}

					_editorControl = constructor.Invoke(null) as UserControl;
					_editorControl.Dock = DockStyle.Fill;
					OnEditorControlInitialized();
				}

				return _editorControl;
			}
		}

		private Type _controlType;
		public Type ControlType
		{
			get => _controlType;
			set
			{
				if (_controlType == value)
				{
					return;
				}

				_controlType = value;
				_drawControl = null;
				_editorControl = null;
				OnControlTypeChanged();
				OnPropertiesChanged();
			}
		}

		private void OnControlTypeChanged()
		{
			if (OwnerEdit is PermissionControl permissionControl)
			{
				permissionControl.UpdateControls();
			}

			((EventHandler)Events[controlTypeChanged])?.Invoke(this, EventArgs.Empty);
		}

		private void OnEditorControlInitialized() => ((EventHandler)this.Events[editorControlInit])?.Invoke(_editorControl, EventArgs.Empty);

		private void OnDrawControlInitialized() => ((EventHandler)this.Events[drawControlInit])?.Invoke(_drawControl, EventArgs.Empty);

		static PermissionRepositoryItem() => Register();

		public PermissionRepositoryItem() : base() => ControlType = typeof(PermissionControlCheckButton);

		public PermissionRepositoryItem(Type controlType) : base() => ControlType = controlType;

		internal const string EditorName = "PermissionControl";

		public static void Register()
		{
			EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EditorName, typeof(PermissionControl),
				typeof(PermissionRepositoryItem), typeof(PermissionViewInfo),
				new PermissionPainter(), true, null));
		}

		public override string EditorTypeName => EditorName;

		public override void Assign(RepositoryItem item)
		{
			base.Assign(item);

			if (item is PermissionRepositoryItem rItem)
			{
				Events.AddHandler(drawControlInit, rItem.Events[drawControlInit]);
				Events.AddHandler(editorControlInit, rItem.Events[editorControlInit]);
				Events.AddHandler(controlTypeChanged, rItem.Events[controlTypeChanged]);
				ControlType = rItem.ControlType;
			}
		}

		public override void ResetEvents() => base.ResetEvents();

		protected override bool NeededKeysContains(Keys key)
		{
			switch (key)
			{
				case Keys.F2:
				case Keys.A:
				case Keys.Add:
				case Keys.B:
				case Keys.Back:
				case Keys.C:
				case Keys.Clear:
				case Keys.D:
				case Keys.D0:
				case Keys.D1:
				case Keys.D2:
				case Keys.D3:
				case Keys.D4:
				case Keys.D5:
				case Keys.D6:
				case Keys.D7:
				case Keys.D8:
				case Keys.D9:
				case Keys.Decimal:
				case Keys.Delete:
				case Keys.Divide:
				case Keys.E:
				case Keys.End:
				case Keys.F:
				case Keys.F20:
				case Keys.G:
				case Keys.H:
				case Keys.Home:
				case Keys.I:
				case Keys.Insert:
				case Keys.J:
				case Keys.K:
				case Keys.L:
				case Keys.Left:
				case Keys.M:
				case Keys.Multiply:
				case Keys.N:
				case Keys.NumPad0:
				case Keys.NumPad1:
				case Keys.NumPad2:
				case Keys.NumPad3:
				case Keys.NumPad4:
				case Keys.NumPad5:
				case Keys.NumPad6:
				case Keys.NumPad7:
				case Keys.NumPad8:
				case Keys.NumPad9:
				case Keys.Alt:
				case (Keys.RButton | Keys.ShiftKey):
				case Keys.O:
				case Keys.Oem8:
				case Keys.OemBackslash:
				case Keys.OemCloseBrackets:
				case Keys.Oemcomma:
				case Keys.OemMinus:
				case Keys.OemOpenBrackets:
				case Keys.OemPeriod:
				case Keys.OemPipe:
				case Keys.Oemplus:
				case Keys.OemQuestion:
				case Keys.OemQuotes:
				case Keys.OemSemicolon:
				case Keys.Oemtilde:
				case Keys.P:
				case Keys.Q:
				case Keys.R:
				case Keys.Right:
				case Keys.S:
				case Keys.Space:
				case Keys.Subtract:
				case Keys.T:
				case Keys.U:
				case Keys.V:
				case Keys.W:
				case Keys.X:
				case Keys.Y:
				case Keys.Z:
					return true;
			}
			return base.NeededKeysContains(key);
		}

	}
}
