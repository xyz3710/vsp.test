﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using System.ComponentModel;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors.ViewInfo;
using System.Windows.Forms;
using System.Drawing;

namespace DevExpress.XtraEditors.Repository
{
	internal class PermissionControl : BaseEdit, IAutoHeightControl
	{
		#region IAutoHeightControl implement
		bool IAutoHeightControl.SupportsAutoHeight => true;

		public event EventHandler OnHeightChanged;
		event EventHandler IAutoHeightControl.HeightChanged
		{
			add { OnHeightChanged += value; }
			remove { OnHeightChanged -= value; }
		}

		protected void RaiseHeightChanged()
		{
			OnHeightChanged?.Invoke(this, EventArgs.Empty);
		}

		int IAutoHeightControl.CalcHeight(GraphicsCache cache)
		{
			if (ViewInfo.IsReady == true && ViewInfo is IHeightAdaptable ha)
			{
				return ha.CalcHeight(cache, Width);
			}

			return Height;
		}
		#endregion

		static PermissionControl()
		{
			PermissionRepositoryItem.Register();
		}

		public PermissionControl() : base() => UpdateControls();

		internal void UpdateControls()
		{
			Controls.Clear();

			if (Properties.ControlType == null)
			{
				return;
			}

			Controls.Add(Properties.EditorControl);
			(Properties.EditorControl as IEditValue).EditValueChanged += new EventHandler(this.OnEditValueChanged);
			(Properties.EditorControl as IEditValue).EditValue = EditValue;
		}

		private void OnEditValueChanged(object sender, EventArgs e)
		{
			EditValue = (Properties.EditorControl as IEditValue).EditValue;
			IsModified = true;
		}

		public override string EditorTypeName => PermissionRepositoryItem.EditorName;

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public new PermissionRepositoryItem Properties => base.Properties as PermissionRepositoryItem;

		protected override void OnPropertiesChanged()
		{
			base.OnPropertiesChanged();
			this.RaiseHeightChanged();
		}

		public override object EditValue
		{
			get => base.EditValue;
			set
			{
				base.EditValue = value;

				if (Properties.EditorControl != null)
				{
					(Properties.EditorControl as IEditValue).EditValue = EditValue;
				}
			}
		}

		public override bool AllowMouseClick(System.Windows.Forms.Control control, System.Drawing.Point p)
		{
			if (base.AllowMouseClick(control, p))
			{
				return true;
			}

			foreach (Control c in this.Controls)
			{
				if (c == control)
				{
					return true;
				}

				if (AllowMouseClickNeastedControls(c, control, p))
				{
					return true;
				}
			}

			return false;
		}

		public static bool AllowMouseClickNeastedControls(Control thisControl, Control control, Point p)
		{
			if (thisControl == control)
			{
				return true;
			}

			if (thisControl is BaseEdit && (thisControl as BaseEdit).AllowMouseClick(control, p))
			{
				return true;
			}

			foreach (Control c in thisControl.Controls)
			{
				if (AllowMouseClickNeastedControls(c, control, p))
				{
					return true;
				}
			}

			return false;
		}

		public override void SendMouse(MouseEventArgs e)
		{
			base.SendMouse(e);
			Control control = this.GetChildAtPoint(e.Location);

			while (control != null)
			{
				if (control is BaseEdit)
				{
					var newPoint = PointToScreen(e.Location);
					newPoint = control.PointToClient(newPoint);

					(control as BaseEdit).SendMouse(new MouseEventArgs(e.Button, e.Clicks, newPoint.X, newPoint.Y, e.Delta));

					return;
				}
				else
				{
					control = control.GetChildAtPoint(e.Location);
				}
			}
		}
	}
}
