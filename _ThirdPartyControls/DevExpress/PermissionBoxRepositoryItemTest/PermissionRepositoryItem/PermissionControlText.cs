﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PermissionRepositoryItemTest.Properties;
using DevExpress.Utils.Extensions;
using Medieng.Common.Security;

namespace DevExpress.XtraEditors.Repository
{
	public partial class PermissionControlText : UserControl, IEditValue
	{
		public PermissionControlText()
		{
			InitializeComponent();
			InitControls();
		}

		private void InitControls()
		{
			lcRead.Permission = Permission.Read;
			lcCreate.Permission = Permission.Create;
			lcUpdate.Permission = Permission.Update;
			lcDelete.Permission = Permission.Delete;
			lcExport.Permission = Permission.Export;

			foreach (var lc in tlpMain.Controls.OfType<LabelControlEx>())
			{
				lc.CheckedChanged += (sender, e) =>
				{
					var labelControl = sender as LabelControlEx;
					var permission = labelControl.Permission;

					if (labelControl.Checked == true)
					{
						EditValue = Permission.Add(permission);
					}
					else
					{
						EditValue = Permission.Remove(permission);
					}

					EditValueChanged?.Invoke(this, EventArgs.Empty);
				};
			}
		}

		private Permission _permission;
		public Permission Permission
		{
			get => _permission;
			set => EditValue = value;
		}

		public event EventHandler EditValueChanged;

		public object EditValue
		{
			get => _permission;
			set
			{
				if (value == null)
				{
					lcRead.Checked = false;
					lcCreate.Checked = false;
					lcUpdate.Checked = false;
					lcDelete.Checked = false;
					lcExport.Checked = false;
				}
				else
				{
					_permission = (Permission)Convert.ToInt32(value);

					lcRead.Checked = Permission.CanRead();
					lcCreate.Checked = Permission.CanCreate();
					lcUpdate.Checked = Permission.CanUpdate();
					lcDelete.Checked = Permission.CanDelete();
					lcExport.Checked = Permission.CanExport();
				}

				Refresh();
			}
		}
	}
}
