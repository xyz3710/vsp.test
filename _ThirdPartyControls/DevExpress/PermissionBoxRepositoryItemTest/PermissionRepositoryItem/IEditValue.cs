﻿using System;

namespace DevExpress.XtraEditors.Repository
{
	internal interface IEditValue
	{
		object EditValue { get; set; }
		event EventHandler EditValueChanged;
	}
}
