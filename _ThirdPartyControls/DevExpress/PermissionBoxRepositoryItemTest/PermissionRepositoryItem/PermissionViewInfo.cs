﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors.ViewInfo;
using DevExpress.XtraEditors.Repository;
using DevExpress.Utils.Drawing;

namespace DevExpress.XtraEditors.Repository
{
	internal class PermissionViewInfo : BaseEditViewInfo, IHeightAdaptable
	{
		PermissionViewInfo(RepositoryItem Item) : base(Item) { }

		int IHeightAdaptable.CalcHeight(GraphicsCache cache, int width)
		{
			var prItem = Item as PermissionRepositoryItem;

			if (prItem.ControlType == null)
			{
				return this.CalcMinHeight(cache.Graphics);
			}

			return prItem.DrawControl.Height;
		}

		public override object EditValue
		{
			get => base.EditValue;
			set => base.EditValue = value;
		}

		public override string DisplayText => base.DisplayText;
	}
}
