﻿namespace DevExpress.XtraEditors.Repository
{
	partial class PermissionControlLabel
	{
		/// <summary> 
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 구성 요소 디자이너에서 생성한 코드

		/// <summary> 
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.lcExport = new DevExpress.XtraEditors.LabelControlEx();
			this.lcRead = new DevExpress.XtraEditors.LabelControlEx();
			this.lcCreate = new DevExpress.XtraEditors.LabelControlEx();
			this.lcUpdate = new DevExpress.XtraEditors.LabelControlEx();
			this.lcDelete = new DevExpress.XtraEditors.LabelControlEx();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 5;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.Controls.Add(this.lcExport, 4, 0);
			this.tlpMain.Controls.Add(this.lcRead, 0, 0);
			this.tlpMain.Controls.Add(this.lcCreate, 1, 0);
			this.tlpMain.Controls.Add(this.lcUpdate, 2, 0);
			this.tlpMain.Controls.Add(this.lcDelete, 3, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 1;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Size = new System.Drawing.Size(216, 42);
			this.tlpMain.TabIndex = 0;
			// 
			// lcExport
			// 
			this.lcExport.Appearance.BackColor = System.Drawing.Color.White;
			this.lcExport.Appearance.Options.UseBackColor = true;
			this.lcExport.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lcExport.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lcExport.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Excel_Disabled;
			this.lcExport.Location = new System.Drawing.Point(172, 0);
			this.lcExport.Margin = new System.Windows.Forms.Padding(0);
			this.lcExport.Name = "lcExport";
			this.lcExport.Size = new System.Drawing.Size(44, 42);
			this.lcExport.TabIndex = 4;
			// 
			// lcRead
			// 
			this.lcRead.Appearance.BackColor = System.Drawing.Color.White;
			this.lcRead.Appearance.Options.UseBackColor = true;
			this.lcRead.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lcRead.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lcRead.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Search_Disabled;
			this.lcRead.Location = new System.Drawing.Point(0, 0);
			this.lcRead.Margin = new System.Windows.Forms.Padding(0);
			this.lcRead.Name = "lcRead";
			this.lcRead.Size = new System.Drawing.Size(43, 42);
			this.lcRead.TabIndex = 0;
			// 
			// lcCreate
			// 
			this.lcCreate.Appearance.BackColor = System.Drawing.Color.White;
			this.lcCreate.Appearance.Options.UseBackColor = true;
			this.lcCreate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lcCreate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lcCreate.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Add_Disabled;
			this.lcCreate.Location = new System.Drawing.Point(43, 0);
			this.lcCreate.Margin = new System.Windows.Forms.Padding(0);
			this.lcCreate.Name = "lcCreate";
			this.lcCreate.Size = new System.Drawing.Size(43, 42);
			this.lcCreate.TabIndex = 1;
			// 
			// lcUpdate
			// 
			this.lcUpdate.Appearance.BackColor = System.Drawing.Color.White;
			this.lcUpdate.Appearance.Options.UseBackColor = true;
			this.lcUpdate.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lcUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lcUpdate.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Edit_Disabled;
			this.lcUpdate.Location = new System.Drawing.Point(86, 0);
			this.lcUpdate.Margin = new System.Windows.Forms.Padding(0);
			this.lcUpdate.Name = "lcUpdate";
			this.lcUpdate.Size = new System.Drawing.Size(43, 42);
			this.lcUpdate.TabIndex = 2;
			// 
			// lcDelete
			// 
			this.lcDelete.Appearance.BackColor = System.Drawing.Color.White;
			this.lcDelete.Appearance.Options.UseBackColor = true;
			this.lcDelete.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lcDelete.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lcDelete.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Delete_Disabled;
			this.lcDelete.Location = new System.Drawing.Point(129, 0);
			this.lcDelete.Margin = new System.Windows.Forms.Padding(0);
			this.lcDelete.Name = "lcDelete";
			this.lcDelete.Size = new System.Drawing.Size(43, 42);
			this.lcDelete.TabIndex = 3;
			// 
			// PermissionControlLabel
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tlpMain);
			this.Name = "PermissionControlLabel";
			this.Size = new System.Drawing.Size(216, 42);
			this.tlpMain.ResumeLayout(false);
			this.tlpMain.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private DevExpress.XtraEditors.LabelControlEx lcExport;
		private DevExpress.XtraEditors.LabelControlEx lcCreate;
		private DevExpress.XtraEditors.LabelControlEx lcUpdate;
		private DevExpress.XtraEditors.LabelControlEx lcDelete;
		private DevExpress.XtraEditors.LabelControlEx lcRead;
	}
}
