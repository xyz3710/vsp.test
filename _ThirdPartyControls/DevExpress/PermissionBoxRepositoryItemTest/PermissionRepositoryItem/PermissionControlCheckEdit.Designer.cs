﻿namespace DevExpress.XtraEditors.Repository
{
	partial class PermissionControlCheckEdit
	{
		/// <summary> 
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 구성 요소 디자이너에서 생성한 코드

		/// <summary> 
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.cbExport = new DevExpress.XtraEditors.CheckEdit();
			this.cbRead = new DevExpress.XtraEditors.CheckEdit();
			this.cbCreate = new DevExpress.XtraEditors.CheckEdit();
			this.cbUpdate = new DevExpress.XtraEditors.CheckEdit();
			this.cbDelete = new DevExpress.XtraEditors.CheckEdit();
			this.tlpMain.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cbExport.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbRead.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbCreate.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbUpdate.Properties)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cbDelete.Properties)).BeginInit();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 5;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.Controls.Add(this.cbExport, 4, 0);
			this.tlpMain.Controls.Add(this.cbRead, 0, 0);
			this.tlpMain.Controls.Add(this.cbCreate, 1, 0);
			this.tlpMain.Controls.Add(this.cbUpdate, 2, 0);
			this.tlpMain.Controls.Add(this.cbDelete, 3, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 1;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Size = new System.Drawing.Size(216, 42);
			this.tlpMain.TabIndex = 0;
			// 
			// cbExport
			// 
			this.cbExport.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbExport.Location = new System.Drawing.Point(172, 0);
			this.cbExport.Margin = new System.Windows.Forms.Padding(0);
			this.cbExport.Name = "cbExport";
			this.cbExport.Properties.AutoHeight = false;
			this.cbExport.Properties.Caption = "";
			this.cbExport.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgCheckBox1;
			this.cbExport.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.cbExport.Properties.GlyphVerticalAlignment = DevExpress.Utils.VertAlignment.Center;
			this.cbExport.Size = new System.Drawing.Size(44, 42);
			this.cbExport.TabIndex = 4;
			// 
			// cbRead
			// 
			this.cbRead.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbRead.Location = new System.Drawing.Point(0, 0);
			this.cbRead.Margin = new System.Windows.Forms.Padding(0);
			this.cbRead.Name = "cbRead";
			this.cbRead.Properties.AutoHeight = false;
			this.cbRead.Properties.Caption = "";
			this.cbRead.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgCheckBox1;
			this.cbRead.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.cbRead.Properties.GlyphVerticalAlignment = DevExpress.Utils.VertAlignment.Center;
			this.cbRead.Size = new System.Drawing.Size(43, 42);
			this.cbRead.TabIndex = 0;
			// 
			// cbCreate
			// 
			this.cbCreate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbCreate.Location = new System.Drawing.Point(43, 0);
			this.cbCreate.Margin = new System.Windows.Forms.Padding(0);
			this.cbCreate.Name = "cbCreate";
			this.cbCreate.Properties.AutoHeight = false;
			this.cbCreate.Properties.Caption = "";
			this.cbCreate.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgCheckBox1;
			this.cbCreate.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.cbCreate.Properties.GlyphVerticalAlignment = DevExpress.Utils.VertAlignment.Center;
			this.cbCreate.Size = new System.Drawing.Size(43, 42);
			this.cbCreate.TabIndex = 1;
			// 
			// cbUpdate
			// 
			this.cbUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbUpdate.Location = new System.Drawing.Point(86, 0);
			this.cbUpdate.Margin = new System.Windows.Forms.Padding(0);
			this.cbUpdate.Name = "cbUpdate";
			this.cbUpdate.Properties.AutoHeight = false;
			this.cbUpdate.Properties.Caption = "";
			this.cbUpdate.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgCheckBox1;
			this.cbUpdate.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.cbUpdate.Properties.GlyphVerticalAlignment = DevExpress.Utils.VertAlignment.Center;
			this.cbUpdate.Size = new System.Drawing.Size(43, 42);
			this.cbUpdate.TabIndex = 2;
			// 
			// cbDelete
			// 
			this.cbDelete.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbDelete.Location = new System.Drawing.Point(129, 0);
			this.cbDelete.Margin = new System.Windows.Forms.Padding(0);
			this.cbDelete.Name = "cbDelete";
			this.cbDelete.Properties.AutoHeight = false;
			this.cbDelete.Properties.Caption = "";
			this.cbDelete.Properties.CheckBoxOptions.Style = DevExpress.XtraEditors.Controls.CheckBoxStyle.SvgCheckBox1;
			this.cbDelete.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.cbDelete.Properties.GlyphVerticalAlignment = DevExpress.Utils.VertAlignment.Center;
			this.cbDelete.Size = new System.Drawing.Size(43, 42);
			this.cbDelete.TabIndex = 3;
			// 
			// PermissionControlCheckEdit
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tlpMain);
			this.Name = "PermissionControlCheckEdit";
			this.Size = new System.Drawing.Size(216, 42);
			this.tlpMain.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cbExport.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbRead.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbCreate.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbUpdate.Properties)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cbDelete.Properties)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private DevExpress.XtraEditors.CheckEdit cbExport;
		private DevExpress.XtraEditors.CheckEdit cbCreate;
		private DevExpress.XtraEditors.CheckEdit cbUpdate;
		private DevExpress.XtraEditors.CheckEdit cbDelete;
		private DevExpress.XtraEditors.CheckEdit cbRead;
	}
}
