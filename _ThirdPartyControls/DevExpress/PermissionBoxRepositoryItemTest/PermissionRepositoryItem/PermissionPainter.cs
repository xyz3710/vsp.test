﻿using System;
using System.Drawing;

using DevExpress.XtraEditors.Drawing;

namespace DevExpress.XtraEditors.Repository
{
	internal class PermissionPainter : BaseEditPainter
	{
		public PermissionPainter() : base() { }

		public override void Draw(ControlGraphicsInfoArgs info)
		{
			base.Draw(info);

			if (info.ViewInfo == null)
			{
				return;
			}

			var vi = info.ViewInfo as PermissionViewInfo;

			if (vi.Item == null)
			{
				return;
			}

			var prItem = vi.Item as PermissionRepositoryItem;

			if (prItem.ControlType == null)
			{
				return;
			}

			(prItem.DrawControl as IEditValue).EditValue = vi.EditValue;

			prItem.DrawControl.Bounds = info.Bounds;

			var bitmap = new Bitmap(info.Bounds.Width, info.Bounds.Height);

			prItem.DrawControl.DrawToBitmap(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height));
			info.Graphics.DrawImage(bitmap, info.Bounds.Location);
		}
	}
}
