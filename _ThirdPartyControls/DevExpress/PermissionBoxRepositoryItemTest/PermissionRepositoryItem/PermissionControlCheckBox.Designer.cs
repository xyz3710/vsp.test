﻿namespace DevExpress.XtraEditors.Repository
{
	partial class PermissionControlCheckButton
	{
		/// <summary> 
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region 구성 요소 디자이너에서 생성한 코드

		/// <summary> 
		/// 디자이너 지원에 필요한 메서드입니다. 
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
		/// </summary>
		private void InitializeComponent()
		{
			this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
			this.cbExport = new DevExpress.XtraEditors.CheckButton();
			this.cbRead = new DevExpress.XtraEditors.CheckButton();
			this.cbCreate = new DevExpress.XtraEditors.CheckButton();
			this.cbUpdate = new DevExpress.XtraEditors.CheckButton();
			this.cbDelete = new DevExpress.XtraEditors.CheckButton();
			this.tlpMain.SuspendLayout();
			this.SuspendLayout();
			// 
			// tlpMain
			// 
			this.tlpMain.ColumnCount = 5;
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
			this.tlpMain.Controls.Add(this.cbExport, 4, 0);
			this.tlpMain.Controls.Add(this.cbRead, 0, 0);
			this.tlpMain.Controls.Add(this.cbCreate, 1, 0);
			this.tlpMain.Controls.Add(this.cbUpdate, 2, 0);
			this.tlpMain.Controls.Add(this.cbDelete, 3, 0);
			this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tlpMain.Location = new System.Drawing.Point(0, 0);
			this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
			this.tlpMain.Name = "tlpMain";
			this.tlpMain.RowCount = 1;
			this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tlpMain.Size = new System.Drawing.Size(216, 42);
			this.tlpMain.TabIndex = 0;
			// 
			// cbExport
			// 
			this.cbExport.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbExport.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Excel_Disabled;
			this.cbExport.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
			this.cbExport.Location = new System.Drawing.Point(172, 0);
			this.cbExport.Margin = new System.Windows.Forms.Padding(0);
			this.cbExport.Name = "cbExport";
			this.cbExport.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
			this.cbExport.Size = new System.Drawing.Size(44, 42);
			this.cbExport.TabIndex = 4;
			// 
			// cbRead
			// 
			this.cbRead.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbRead.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Search_Disabled;
			this.cbRead.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
			this.cbRead.Location = new System.Drawing.Point(0, 0);
			this.cbRead.Margin = new System.Windows.Forms.Padding(0);
			this.cbRead.Name = "cbRead";
			this.cbRead.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
			this.cbRead.Size = new System.Drawing.Size(43, 42);
			this.cbRead.TabIndex = 0;
			// 
			// cbCreate
			// 
			this.cbCreate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbCreate.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Add_Disabled;
			this.cbCreate.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
			this.cbCreate.Location = new System.Drawing.Point(43, 0);
			this.cbCreate.Margin = new System.Windows.Forms.Padding(0);
			this.cbCreate.Name = "cbCreate";
			this.cbCreate.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
			this.cbCreate.Size = new System.Drawing.Size(43, 42);
			this.cbCreate.TabIndex = 1;
			// 
			// cbUpdate
			// 
			this.cbUpdate.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbUpdate.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Edit_Disabled;
			this.cbUpdate.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
			this.cbUpdate.Location = new System.Drawing.Point(86, 0);
			this.cbUpdate.Margin = new System.Windows.Forms.Padding(0);
			this.cbUpdate.Name = "cbUpdate";
			this.cbUpdate.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
			this.cbUpdate.Size = new System.Drawing.Size(43, 42);
			this.cbUpdate.TabIndex = 2;
			// 
			// cbDelete
			// 
			this.cbDelete.Dock = System.Windows.Forms.DockStyle.Fill;
			this.cbDelete.ImageOptions.Image = global::PermissionRepositoryItemTest.Properties.Resources.Delete_Disabled;
			this.cbDelete.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
			this.cbDelete.Location = new System.Drawing.Point(129, 0);
			this.cbDelete.Margin = new System.Windows.Forms.Padding(0);
			this.cbDelete.Name = "cbDelete";
			this.cbDelete.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
			this.cbDelete.Size = new System.Drawing.Size(43, 42);
			this.cbDelete.TabIndex = 3;
			// 
			// PermissionControlCheckBox
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.tlpMain);
			this.Name = "PermissionControlCheckBox";
			this.Size = new System.Drawing.Size(216, 42);
			this.tlpMain.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tlpMain;
		private DevExpress.XtraEditors.CheckButton cbExport;
		private DevExpress.XtraEditors.CheckButton cbCreate;
		private DevExpress.XtraEditors.CheckButton cbUpdate;
		private DevExpress.XtraEditors.CheckButton cbDelete;
		private DevExpress.XtraEditors.CheckButton cbRead;
	}
}
