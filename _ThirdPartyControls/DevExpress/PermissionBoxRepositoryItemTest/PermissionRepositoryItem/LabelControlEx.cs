﻿using Medieng.Common.Security;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace DevExpress.XtraEditors
{
	internal class LabelControlEx : LabelControl
	{
		/// <summary>
		/// Occurs when [on CheckedChanged].
		/// </summary>
		public event EventHandler<EventArgs> CheckedChanged;

		/// <summary>
		/// Handles the <see cref="E:OnCheckedChanged"/> event.
		/// </summary>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		protected virtual void OnCheckedChanged(EventArgs e)
		{
			CheckedChanged?.Invoke(this, e);
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Permission Permission { get; set; }

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool Checked { get; set; }

		protected override void OnClick(EventArgs e)
		{
			base.OnClick(e);

			Checked = !Checked;
			OnCheckedChanged(EventArgs.Empty);
		}
	}
}
