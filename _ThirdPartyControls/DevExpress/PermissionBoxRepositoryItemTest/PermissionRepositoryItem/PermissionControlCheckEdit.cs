﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PermissionRepositoryItemTest;
using PermissionRepositoryItemTest.Properties;
using DevExpress.Utils.Extensions;
using Medieng.Common.Security;
using DevExpress.XtraEditors.Controls;
using System.IO;

namespace DevExpress.XtraEditors.Repository
{
	public partial class PermissionControlCheckEdit : UserControl, IEditValue
	{
		static PermissionControlCheckEdit()
		{
			Images = new Dictionary<bool, Bitmap>
			{
				{ true, Resources.Check },
				{ false, Resources.Check_Disabled },
			};
		}

		static Dictionary<bool, Bitmap> Images;

		public PermissionControlCheckEdit()
		{
			InitializeComponent();
			ReadOnly = true;
			InitControls();
		}

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool ReadOnly { get; set; }

		private void InitControls()
		{
			SetReadOnly(cbRead, ReadOnly);
			SetReadOnly(cbCreate, ReadOnly);
			SetReadOnly(cbUpdate, ReadOnly);
			SetReadOnly(cbDelete, ReadOnly);
			SetReadOnly(cbExport, ReadOnly);

			SetCheckBoxStyle(cbRead);
			SetCheckBoxStyle(cbCreate);
			SetCheckBoxStyle(cbUpdate);
			SetCheckBoxStyle(cbDelete);
			SetCheckBoxStyle(cbExport);

			cbRead.Tag = Permission.Read;
			cbCreate.Tag = Permission.Create;
			cbUpdate.Tag = Permission.Update;
			cbDelete.Tag = Permission.Delete;
			cbExport.Tag = Permission.Export;

			if (ReadOnly == false)
			{
				foreach (var cb in tlpMain.Controls.OfType<CheckButton>())
				{
					cb.CheckedChanged += (sender, e) =>
					{
						var checkButton = sender as CheckButton;
						var permission = (Permission)checkButton.Tag;

						if (checkButton.Checked == true)
						{
							EditValue = Permission.Add(permission);
						}
						else
						{
							EditValue = Permission.Remove(permission);
						}

						EditValueChanged?.Invoke(this, EventArgs.Empty);
					};
				}
			}
		}

		private void SetReadOnly(CheckEdit checkEdit, bool readOnly)
		{
			checkEdit.ReadOnly = readOnly;
		}

		private void SetCheckBoxStyle(CheckEdit checkEdit)
		{
			var checkStyle = CheckBoxStyle.SvgStar2;

			checkEdit.Properties.CheckBoxOptions.Style = checkStyle;
			//var checkStyle = CheckBoxStyle.Custom;
			//checkEdit.Properties.ImageOptions.ImageChecked = Images[true];
			//checkEdit.Properties.ImageOptions.ImageGrayed = Images[false];
		}

		private Permission _permission;
		public Permission Permission
		{
			get => _permission;
			set => EditValue = value;
		}

		public event EventHandler EditValueChanged;

		public object EditValue
		{
			get => _permission;
			set
			{
				if (value == null)
				{
					cbRead.Checked = false;
					cbCreate.Checked = false;
					cbUpdate.Checked = false;
					cbDelete.Checked = false;
					cbExport.Checked = false;
				}
				else
				{
					_permission = (Permission)Convert.ToInt32(value);

					cbRead.Checked = Permission.CanRead();
					cbCreate.Checked = Permission.CanCreate();
					cbUpdate.Checked = Permission.CanUpdate();
					cbDelete.Checked = Permission.CanDelete();
					cbExport.Checked = Permission.CanExport();
				}

				Refresh();
			}
		}
	}
}
