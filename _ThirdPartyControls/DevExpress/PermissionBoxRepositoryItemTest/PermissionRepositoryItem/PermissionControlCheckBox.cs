﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using PermissionRepositoryItemTest;
using PermissionRepositoryItemTest.Properties;
using DevExpress.Utils.Extensions;
using Medieng.Common.Security;

namespace DevExpress.XtraEditors.Repository
{
	public partial class PermissionControlCheckButton : UserControl, IEditValue
	{
		static PermissionControlCheckButton()
		{
			Images = new Dictionary<Permission, Tuple<Bitmap, Bitmap>>
			{
				{ Permission.Read, Tuple.Create(Resources.Search, Resources.Search_Disabled) },
				{ Permission.Create, Tuple.Create(Resources.Add, Resources.Add_Disabled) },
				{ Permission.Update, Tuple.Create(Resources.Edit, Resources.Edit_Disabled) },
				{ Permission.Delete, Tuple.Create(Resources.Delete, Resources.Delete_Disabled) },
				{ Permission.Export, Tuple.Create(Resources.Excel, Resources.Excel_Disabled) },
			};
		}

		static Dictionary<Permission, Tuple<Bitmap, Bitmap>> Images;

		public PermissionControlCheckButton()
		{
			InitializeComponent();
			InitControls();
		}

		private void InitControls()
		{
			cbRead.Tag = Permission.Read;
			cbCreate.Tag = Permission.Create;
			cbUpdate.Tag = Permission.Update;
			cbDelete.Tag = Permission.Delete;
			cbExport.Tag = Permission.Export;

			foreach (var cb in tlpMain.Controls.OfType<CheckButton>())
			{
				cb.CheckedChanged += (sender, e) =>
				{
					var checkButton = sender as CheckButton;
					var permission = (Permission)checkButton.Tag;

					if (checkButton.Checked == true)
					{
						checkButton.ImageOptions.Image = Images[permission].Item1;
						EditValue = Permission.Add(permission);
					}
					else
					{
						checkButton.ImageOptions.Image = Images[permission].Item2;
						EditValue = Permission.Remove(permission);
					}

					EditValueChanged?.Invoke(this, EventArgs.Empty);
				};
			}
		}

		private Permission _permission;
		public Permission Permission
		{
			get => _permission;
			set => EditValue = value;
		}

		public event EventHandler EditValueChanged;

		public object EditValue
		{
			get => _permission;
			set
			{
				if (value == null)
				{
					cbRead.Checked = false;
					cbCreate.Checked = false;
					cbUpdate.Checked = false;
					cbDelete.Checked = false;
					cbExport.Checked = false;
				}
				else
				{
					_permission = (Permission)value;

					cbRead.Checked = Permission.CanRead();
					cbCreate.Checked = Permission.CanCreate();
					cbUpdate.Checked = Permission.CanUpdate();
					cbDelete.Checked = Permission.CanDelete();
					cbExport.Checked = Permission.CanExport();
				}

				Refresh();
			}
		}
	}
}
