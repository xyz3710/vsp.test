﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraSplashScreen;

namespace DevExpressSlpashTest
{
	public partial class SplashScreen1 : SplashScreen
	{
		public SplashScreen1()
		{
			InitializeComponent();
			//TargetLookAndFeel.SetSkinStyle("The Asphalt World");
			TargetLookAndFeel.SetSkinStyle("Caramel");
		}

		protected override void OnShown(EventArgs e)
		{
			base.OnShown(e);

			var bound = Screen.PrimaryScreen.Bounds;

			Location = new Point((bound.Width - Width) / 2, (bound.Height - Height) / 2);
		}

		public override void ProcessCommand(Enum cmd, object arg)
		{
			base.ProcessCommand(cmd, arg);

			SplashScreenCommand command = (SplashScreenCommand)cmd;

			switch (command)
			{
				case SplashScreenCommand.SetProgress:

					break;
				case SplashScreenCommand.SetNextTask1:
				case SplashScreenCommand.SetNextTask2:
					lcMessage.Text = (string)arg;

					break;
			}
		}

	}
}