﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using DevExpress.XtraSplashScreen;

namespace DevExpressSlpashTest
{
	public partial class Form1 : Form
	{
		private SplashScreenManager splashScreenManager;

		public Form1()
		{
			InitializeComponent();
			splashScreenManager = new SplashScreenManager(this, typeof(WaitForm1), true, true);
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			//splashScreenManager.ActiveSplashFormTypeInfo = new DevExpress.XtraSplashScreen.TypeInfo(typeof(WaitForm1).Name, DevExpress.XtraSplashScreen.Mode.WaitForm);
			//btnTestSplash.PerformClick();
		}

		private void btnTestWaitForm_Click(object sender, EventArgs e)
		{
			if (splashScreenManager.IsSplashFormVisible == true)
			{
				splashScreenManager.CloseWaitForm();
			}

			if (SplashScreenManager.Default == null)
			{
				// SplashScreenManager.ShowForm을 호출하면 Default가 할당되어서 WaitFormCommand enum의 Set 확장 메서드에서 사용할 수 있다.
				SplashScreenManager.ShowForm(this, typeof(WaitForm1), true, true, false);
			}
			else
			{
				SplashScreenManager.Default.ShowWaitForm();
			}

			// 반드시 ShowWaitForm을 생성 후에 캡션 등을 변경해야 한다.
			SplashScreenManager.Default.SetWaitFormCaption("기다려주세요.");
			SplashScreenManager.Default.SetWaitFormCaption("기다려주세요.");
			SplashScreenManager.Default.SetWaitFormDescription("로딩 중입니다...");

			SplashScreenManager.Default.SendCommand(WaitFormCommand.Wait1, null);
			Thread.Sleep(500);
			WaitFormCommand.Wait2.Set("두번째 기다림");
			Thread.Sleep(1000);
			SplashScreenManager.Default.CloseWaitForm();

			// Instance version
			//splashScreenManager.SetWaitFormCaption("기다려주세요.");
			//splashScreenManager.SetWaitFormDescription("로딩 중입니다...");

			//splashScreenManager.SendCommand(WaitFormCommand.Wait1, null);
			//Thread.Sleep(500);
			////splashScreenManager.SendCommand(WaitFormCommand.Wait2, "두번째 기다림");
			//WaitFormCommand.Wait2.Set("두번째 기다림");
			//Thread.Sleep(1000);
			//splashScreenManager.CloseWaitForm();
		}

		private void btnTestSplash_Click(object sender, EventArgs e)
		{
			// Just show
			//SplashScreenManager.ShowForm(typeof(SplashScreen1));


			//SplashScreen1 splashScreen1 = new SplashScreen1();

			//splashScreen1.ShowMode = DevExpress.XtraSplashScreen.ShowMode.Form;
			//splashScreen1.ShowDialog();

			SplashScreenManager.ShowForm(typeof(SplashScreen1), true, true);

			// The splash screen will be opened in a separate thread. To interact with it, use the SendCommand method.
			for (int i = 1; i <= 100; i++)
			{
				SplashScreenManager.Default.SendCommand(SplashScreenCommand.SetProgress, i);
				//To process commands, override the SplashScreen.ProcessCommand method.
				Thread.Sleep(25);
			}

			SplashScreenManager.Default.SendCommand(SplashScreenCommand.SetNextTask1, "500ms가 걸리는 첫번째 작업 중입니다.");
			Thread.Sleep(500);

			SplashScreenCommand.SetNextTask1.Set("1000ms가 걸리는 두번째 작업 중입니다.");
			Thread.Sleep(1000);

			// Close the Splash Screen.
			SplashScreenManager.CloseForm(false);
		}
	}
}
