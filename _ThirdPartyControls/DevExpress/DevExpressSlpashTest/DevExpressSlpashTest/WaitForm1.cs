﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraWaitForm;

namespace DevExpressSlpashTest
{
	public partial class WaitForm1 : WaitForm
	{
		public WaitForm1()
		{
			InitializeComponent();
			this.progressPanel1.AutoHeight = true;
			TargetLookAndFeel.SetSkinStyle("Caramel");
		}

		#region Overrides

		public override void SetCaption(string caption)
		{
			base.SetCaption(caption);
			this.progressPanel1.Caption = caption;
		}
		public override void SetDescription(string description)
		{
			base.SetDescription(description);
			this.progressPanel1.Description = description;
		}
		public override void ProcessCommand(Enum cmd, object arg)
		{
			base.ProcessCommand(cmd, arg);
			var command = (WaitFormCommand)cmd;
			string msg = Convert.ToString(arg);

			if (string.IsNullOrEmpty(msg) == true)
			{
				switch (command)
				{
					case WaitFormCommand.Wait1:
						SetDescription("첫번째 기다림");

						break;
					case WaitFormCommand.Wait2:
						SetDescription((string)arg);
						break;
				}
			}
			else
			{
				SetDescription(msg);
			}
		}

		#endregion
	}
}