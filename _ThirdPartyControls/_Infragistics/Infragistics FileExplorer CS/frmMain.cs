#region Using directives

	using System;
	using System.Text;
	using System.Diagnostics;
	using System.Drawing;
	using System.IO;
	using System.Collections;
	using System.ComponentModel;
	using System.Windows.Forms;
	using System.Data;
	using Infragistics.Win;
	using Infragistics.Win.UltraWinTree;
	using Infragistics.Win.UltraWinListView;
	using Infragistics.Win.IGControls;
    using Infragistics.Win.Misc;
    using Infragistics.Win.Misc.UltraWinNavigationBar;
    using Infragistics.Win.UltraWinToolbars;

#endregion Using directives

namespace Infragistics.Win.UltraWinListView.Samples
{
	#region frmMain class
	/// <summary>
	/// Main dialog for the Infragistics File Explorer sample application.
	/// </summary>
    public class frmMain : System.Windows.Forms.Form
    {
        #region Constants

        /// <summary>
        /// Change the value of this constant to allow the names of files and directories
        /// to be changed when the end user selects the appropriate option from the context menu.
        /// </summary>
        private static readonly bool ALLOW_FILE_RENAME = false;

        /// <summary>
        /// Change the value of this constant to allow files and directories
        /// to be removed when the end user selects the appropriate option from the context menu.
        /// </summary>
        private static readonly bool ALLOW_FILE_DELETE = false;

        /// <summary>
        /// This enum is used to indicate which control is triggering the navigation to a new
        /// folder. 
        /// </summary>
        private enum NavigateSource
        {
            Tree,
            NavigationToolbar,
            NavigationBar
        }
        #endregion Constants

        #region Member variables
        
        private bool eatNextKeyPress = false;

        #endregion Member variables

        #region Constructor
        /// <summary>
        /// Creates a new instance of the <see cref="frmMain"/> class.
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
        }
        #endregion Constructor

        #region Windows Form Designer generated code

        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Splitter splitterLeft;
        private Infragistics.Win.UltraWinListView.UltraListView lvwFiles;
        private Infragistics.Win.UltraWinTree.UltraTree tvwFolders;
        private FileSystemSupport _fileSystemSupport = null;
        private IconManager _iconManager = null;
        private IGControls.IGContextMenu contextMenu1;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsManager ultraToolbarsManager1;
        private IContainer components;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _frmMain_Toolbars_Dock_Area_Left;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _frmMain_Toolbars_Dock_Area_Right;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _frmMain_Toolbars_Dock_Area_Top;
        private Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea _frmMain_Toolbars_Dock_Area_Bottom;
        private Infragistics.Win.Misc.UltraNavigationBar unbFolders;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn ultraListViewSubItemColumn1 = new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn("SubItemColumn 0");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool1 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("NavigationBar");
            Infragistics.Win.UltraWinToolbars.ControlContainerTool controlContainerTool2 = new Infragistics.Win.UltraWinToolbars.ControlContainerTool("NavigationBar");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.tvwFolders = new Infragistics.Win.UltraWinTree.UltraTree();
            this.splitterLeft = new System.Windows.Forms.Splitter();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.unbFolders = new Infragistics.Win.Misc.UltraNavigationBar();
            this.lvwFiles = new Infragistics.Win.UltraWinListView.UltraListView();
            this.contextMenu1 = new Infragistics.Win.IGControls.IGContextMenu();
            this._frmMain_Toolbars_Dock_Area_Left = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.ultraToolbarsManager1 = new Infragistics.Win.UltraWinToolbars.UltraToolbarsManager(this.components);
            this._frmMain_Toolbars_Dock_Area_Right = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._frmMain_Toolbars_Dock_Area_Top = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this._frmMain_Toolbars_Dock_Area_Bottom = new Infragistics.Win.UltraWinToolbars.UltraToolbarsDockArea();
            this.pnlLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tvwFolders)).BeginInit();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unbFolders)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvwFiles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlLeft
            // 
            this.pnlLeft.Controls.Add(this.tvwFolders);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 34);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(200, 460);
            this.pnlLeft.TabIndex = 1;
            // 
            // tvwFolders
            // 
            this.tvwFolders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvwFolders.Location = new System.Drawing.Point(0, 0);
            this.tvwFolders.Name = "tvwFolders";
            this.tvwFolders.ScrollBounds = Infragistics.Win.UltraWinTree.ScrollBounds.ScrollToFill;
            this.tvwFolders.Size = new System.Drawing.Size(200, 460);
            this.tvwFolders.TabIndex = 0;
            this.tvwFolders.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.tvwFolders_AfterSelect);
            this.tvwFolders.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tvwFolders_MouseUp);
            this.tvwFolders.BeforeExpand += new Infragistics.Win.UltraWinTree.BeforeNodeChangedEventHandler(this.tvwFolders_BeforeExpand);
            // 
            // splitterLeft
            // 
            this.splitterLeft.Location = new System.Drawing.Point(200, 34);
            this.splitterLeft.Name = "splitterLeft";
            this.splitterLeft.Size = new System.Drawing.Size(5, 460);
            this.splitterLeft.TabIndex = 2;
            this.splitterLeft.TabStop = false;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.unbFolders);
            this.pnlMain.Controls.Add(this.lvwFiles);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(205, 34);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(499, 460);
            this.pnlMain.TabIndex = 3;
            // 
            // unbFolders
            // 
            this.unbFolders.Location = new System.Drawing.Point(16, 114);
            this.unbFolders.Name = "unbFolders";
            this.unbFolders.NonAutoSizeHeight = 24;
            this.unbFolders.Size = new System.Drawing.Size(623, 24);
            this.unbFolders.TabIndex = 2;
            this.unbFolders.SelectedLocationChanged += new Infragistics.Win.Misc.UltraWinNavigationBar.SelectedLocationChangedHandler(this.unbFolders_SelectedLocationChanged);
            // 
            // lvwFiles
            // 
            this.lvwFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwFiles.Location = new System.Drawing.Point(0, 0);
            this.lvwFiles.Name = "lvwFiles";
            this.lvwFiles.Size = new System.Drawing.Size(499, 460);
            ultraListViewSubItemColumn1.Key = "SubItemColumn 0";
            this.lvwFiles.SubItemColumns.AddRange(new Infragistics.Win.UltraWinListView.UltraListViewSubItemColumn[] {
            ultraListViewSubItemColumn1});
            this.lvwFiles.TabIndex = 1;
            this.lvwFiles.Text = "ultraListView1";
            this.lvwFiles.ViewChanged += new System.EventHandler(this.lvwFiles_ViewChanged);
            this.lvwFiles.ToolTipDisplaying += new Infragistics.Win.UltraWinListView.ToolTipDisplayingEventHandler(this.lvwFiles_ToolTipDisplaying);
            this.lvwFiles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvwFiles_KeyDown);
            this.lvwFiles.ItemExitingEditMode += new Infragistics.Win.UltraWinListView.ItemExitingEditModeEventHandler(this.lvwFiles_ItemExitingEditMode);
            this.lvwFiles.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lvwFiles_MouseDown);
            this.lvwFiles.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.lvwFiles_KeyPress);
            this.lvwFiles.ItemDoubleClick += new Infragistics.Win.UltraWinListView.ItemDoubleClickEventHandler(this.lvwFiles_ItemDoubleClick);
            // 
            // contextMenu1
            // 
            this.contextMenu1.ImageList = null;
            this.contextMenu1.Style = Infragistics.Win.IGControls.MenuStyle.System;
            // 
            // _frmMain_Toolbars_Dock_Area_Left
            // 
            this._frmMain_Toolbars_Dock_Area_Left.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._frmMain_Toolbars_Dock_Area_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(230)))), ((int)(((byte)(246)))));
            this._frmMain_Toolbars_Dock_Area_Left.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Left;
            this._frmMain_Toolbars_Dock_Area_Left.ForeColor = System.Drawing.SystemColors.ControlText;
            this._frmMain_Toolbars_Dock_Area_Left.Location = new System.Drawing.Point(0, 34);
            this._frmMain_Toolbars_Dock_Area_Left.Name = "_frmMain_Toolbars_Dock_Area_Left";
            this._frmMain_Toolbars_Dock_Area_Left.Size = new System.Drawing.Size(0, 460);
            this._frmMain_Toolbars_Dock_Area_Left.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // ultraToolbarsManager1
            // 
            this.ultraToolbarsManager1.DesignerFlags = 1;
            this.ultraToolbarsManager1.DockWithinContainer = this;
            this.ultraToolbarsManager1.DockWithinContainerBaseType = typeof(System.Windows.Forms.Form);
            this.ultraToolbarsManager1.IsGlassSupported = false;
            controlContainerTool1.ControlName = "unbFolders";
			controlContainerTool1.CanSetWidth = true;
            this.ultraToolbarsManager1.NavigationToolbar.NonInheritedTools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            controlContainerTool1});
            this.ultraToolbarsManager1.NavigationToolbar.Visible = true;
            this.ultraToolbarsManager1.Style = Infragistics.Win.UltraWinToolbars.ToolbarStyle.WindowsVista;
            controlContainerTool2.ControlName = "unbFolders";
            controlContainerTool2.SharedProps.Caption = "NavigationBar";
            controlContainerTool2.SharedProps.Spring = true;
			controlContainerTool2.CanSetWidth = true;
            this.ultraToolbarsManager1.Tools.AddRange(new Infragistics.Win.UltraWinToolbars.ToolBase[] {
            controlContainerTool2});
            this.ultraToolbarsManager1.AfterNavigation += new Infragistics.Win.UltraWinToolbars.AfterNavigationEventHandler(this.ultraToolbarsManager1_AfterNavigation);
            // 
            // _frmMain_Toolbars_Dock_Area_Right
            // 
            this._frmMain_Toolbars_Dock_Area_Right.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._frmMain_Toolbars_Dock_Area_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(230)))), ((int)(((byte)(246)))));
            this._frmMain_Toolbars_Dock_Area_Right.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Right;
            this._frmMain_Toolbars_Dock_Area_Right.ForeColor = System.Drawing.SystemColors.ControlText;
            this._frmMain_Toolbars_Dock_Area_Right.Location = new System.Drawing.Point(704, 34);
            this._frmMain_Toolbars_Dock_Area_Right.Name = "_frmMain_Toolbars_Dock_Area_Right";
            this._frmMain_Toolbars_Dock_Area_Right.Size = new System.Drawing.Size(0, 460);
            this._frmMain_Toolbars_Dock_Area_Right.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _frmMain_Toolbars_Dock_Area_Top
            // 
            this._frmMain_Toolbars_Dock_Area_Top.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._frmMain_Toolbars_Dock_Area_Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(230)))), ((int)(((byte)(246)))));
            this._frmMain_Toolbars_Dock_Area_Top.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Top;
            this._frmMain_Toolbars_Dock_Area_Top.ForeColor = System.Drawing.SystemColors.ControlText;
            this._frmMain_Toolbars_Dock_Area_Top.Location = new System.Drawing.Point(0, 0);
            this._frmMain_Toolbars_Dock_Area_Top.Name = "_frmMain_Toolbars_Dock_Area_Top";
            this._frmMain_Toolbars_Dock_Area_Top.Size = new System.Drawing.Size(704, 34);
            this._frmMain_Toolbars_Dock_Area_Top.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // _frmMain_Toolbars_Dock_Area_Bottom
            // 
            this._frmMain_Toolbars_Dock_Area_Bottom.AccessibleRole = System.Windows.Forms.AccessibleRole.Grouping;
            this._frmMain_Toolbars_Dock_Area_Bottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(230)))), ((int)(((byte)(246)))));
            this._frmMain_Toolbars_Dock_Area_Bottom.DockedPosition = Infragistics.Win.UltraWinToolbars.DockedPosition.Bottom;
            this._frmMain_Toolbars_Dock_Area_Bottom.ForeColor = System.Drawing.SystemColors.ControlText;
            this._frmMain_Toolbars_Dock_Area_Bottom.Location = new System.Drawing.Point(0, 494);
            this._frmMain_Toolbars_Dock_Area_Bottom.Name = "_frmMain_Toolbars_Dock_Area_Bottom";
            this._frmMain_Toolbars_Dock_Area_Bottom.Size = new System.Drawing.Size(704, 0);
            this._frmMain_Toolbars_Dock_Area_Bottom.ToolbarsManager = this.ultraToolbarsManager1;
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(704, 494);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.splitterLeft);
            this.Controls.Add(this.pnlLeft);
            this.Controls.Add(this._frmMain_Toolbars_Dock_Area_Left);
            this.Controls.Add(this._frmMain_Toolbars_Dock_Area_Right);
            this.Controls.Add(this._frmMain_Toolbars_Dock_Area_Top);
            this.Controls.Add(this._frmMain_Toolbars_Dock_Area_Bottom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Infragistics File Explorer";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.pnlLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tvwFolders)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.unbFolders)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lvwFiles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraToolbarsManager1)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion	Windows Form Designer generated code

        #region Main()
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new frmMain());
        }
        #endregion Main()

        #region Dispose
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //	Call the FileSystemSupport class' OnDispose method
                //	to destroy icons that were used by this application
                this.FileSystemSupport.OnDispose();

                //	Call the IconManager class' OnDispose method
                //	to destroy the bitmaps that were created by this application
                IDisposable disposable = this.IconManager as IDisposable;
                disposable.Dispose();
            }

            base.Dispose(disposing);
        }
        #endregion Dispose

        #region Properties

        #region FileSystemSupport
        /// <summary>
        /// Returns the FileSystemSupport instance that provides file
        /// system support for this application.
        /// </summary>
        public FileSystemSupport FileSystemSupport
        {
            get
            {
                if (this._fileSystemSupport == null)
                    this._fileSystemSupport = new FileSystemSupport();

                return this._fileSystemSupport;

            }
        }
        #endregion FileSystemSupport

        #region IconManager
        /// <summary>
        /// Returns the IconManager instance that provides the bitmaps for this application.
        /// </summary>
        public IconManager IconManager
        {
            get
            {
                if (this._iconManager == null)
                    this._iconManager = new IconManager(this);

                return this._iconManager;

            }
        }
        #endregion IconManager

        #region DateFormat
        /// <summary>
        /// Returns the string format to use for the DateTime columns
        /// </summary>
        static private string DateFormat
        {
            get
            {
                return string.Format("{0} {1}", System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
            }
        }
        #endregion DateFormat

        #region FileSizeFormat
        /// <summary>
        /// Returns the string format to use for the file size column
        /// </summary>
        static private string FileSizeFormat
        {
            get
            {
                return "#,###,##0 KB";
            }
        }
        #endregion FileSizeFormat

        #endregion Properties

        #region Methods

        #region InitializeUI
        /// <summary>
        /// Initializes the user interface.
        /// </summary>
        private void InitializeUI()
        {
            //	Configure the UltraTree control for displaying the folders
            this.tvwFolders.HideSelection = false;
            this.tvwFolders.ScrollBounds = ScrollBounds.ScrollToFill;
            this.tvwFolders.ShowLines = false;
            this.tvwFolders.ShowRootLines = false;
            this.tvwFolders.Override.SelectionType = SelectType.Single;
            this.tvwFolders.Override.NodeSpacingAfter = 2;
            this.tvwFolders.Override.ShowExpansionIndicator = ShowExpansionIndicator.CheckOnExpand;
            this.tvwFolders.Override.NodeDoubleClickAction = NodeDoubleClickAction.None;
            this.tvwFolders.Override.HotTracking = DefaultableBoolean.True;

            //	Configure the UltraListView control for displaying the files

            //	Set the DefaultImage to a folder icon, since that is the most common
            //	type of item we will be displaying
            this.lvwFiles.ItemSettings.DefaultImage = this.GetFolderIcon(this.lvwFiles.View);

            //	Set the ContextMenu property to reference the IGContextMenu
            this.lvwFiles.ContextMenu = this.contextMenu1;

            //	Populate the UltraListView's SubItemColumns collection with the
            //	columns that will represent the additional information associated
            //	with the directories and files
            this.lvwFiles.SubItemColumns.Clear();
            UltraListViewSubItemColumn column = null;

            //	Set the text for the MainColumn 
            this.lvwFiles.MainColumn.Text = "Name";

            //	Make sub-items visible by default for both the 'Details' and 'Tiles' views.
            this.lvwFiles.ViewSettingsDetails.SubItemColumnsVisibleByDefault = true;
            this.lvwFiles.ViewSettingsTiles.SubItemsVisibleByDefault = true;

            //	Make all columns visible in the tooltips
            this.lvwFiles.ItemSettings.SubItemsVisibleInToolTipByDefault = true;

            //	Allow/disallow item editing based on the value of the ALLOW_FILE_RENAME constant
            this.lvwFiles.ItemSettings.AllowEdit = frmMain.ALLOW_FILE_RENAME ? DefaultableBoolean.True : DefaultableBoolean.False;

            //	Add a column for the file size; make the DataType long, and use a
            //	format that includes a thousands separator and trailing zero. Also,
            //	Use the column's SubItemAppearance to right-align the displayed values.
            column = this.lvwFiles.SubItemColumns.Add("Size");
            column.DataType = typeof(long);
            column.Format = frmMain.FileSizeFormat;
            column.SubItemAppearance.TextHAlign = HAlign.Right;

            //	Add a column for the file type; make the DataType string.
            column = this.lvwFiles.SubItemColumns.Add("Type");
            column.DataType = typeof(string);

            //	Add a column for the modified date; make the DataType DateTime,
            //	and use a format that includes the short representation of the
            //	date and time.
            column = this.lvwFiles.SubItemColumns.Add("DateModified");
            column.Text = "Date Modified";
            column.DataType = typeof(DateTime);
            column.Format = frmMain.DateFormat;

            //	Add a column for the created date; make the DataType DateTime,
            //	and use a format that includes the short representation of the
            //	date and time...note that we will hide this column by default.
            column = this.lvwFiles.SubItemColumns.Add("DateCreated");
            column.Text = "Date Created";
            column.DataType = typeof(DateTime);
            column.Format = frmMain.DateFormat;
            column.VisibleInTilesView = DefaultableBoolean.False;
            column.VisibleInDetailsView = DefaultableBoolean.False;
            column.VisibleInToolTip = DefaultableBoolean.False;

            //	Add a column for the file attributes...note that we will
            //	hide this column by default.
            column = this.lvwFiles.SubItemColumns.Add("Attributes");
            column.DataType = typeof(string);
            column.VisibleInTilesView = DefaultableBoolean.False;
            column.VisibleInDetailsView = DefaultableBoolean.False;
            column.VisibleInToolTip = DefaultableBoolean.False;

            //	Set ShowGroups to false so we don't show groups initially
            this.lvwFiles.ShowGroups = false;

            //	Assign a custom sort comparer so that folders always
            //	appear before files when the end user sorts items.
            this.lvwFiles.MainColumn.SortComparer = new FileSystemSortComparer(this.lvwFiles.MainColumn);

            //	Assign an instance of the UltraListViewSelectedImageDrawFilter class
            //	to the control's DrawFilter property to handle custom drawing for
            //	selected images.
            this.lvwFiles.DrawFilter = new UltraListViewSelectedImageDrawFilter();

            for (int i = 0; i < this.lvwFiles.SubItemColumns.Count; i++)
            {
                UltraListViewSubItemColumn subItemColumn = this.lvwFiles.SubItemColumns[i];
                subItemColumn.SortComparer = new FileSystemSortComparer(subItemColumn);
            }

            this.PopulateRoot();
        }
        #endregion InitializeUI

        #region PopulateListViewContextMenu
        /// <summary>
        /// Populates the UltraListView's context menu based on the speficied context
        /// </summary>
        private void PopulateListViewContextMenu(object context)
        {
            //	Clear the existing menu items
            this.contextMenu1.MenuItems.Clear();

            IGMenuItem menuItem = null;
            if (context is UltraListViewStyle)
            {
                //	Populate the context menu with the constants of the UltraListViewStyle
                //	enumeration, so we can allow the end user to change the view.
                string[] enumNames = Enum.GetNames(typeof(UltraListViewStyle));
                Array enumValues = Enum.GetValues(typeof(UltraListViewStyle));

                for (int i = 0; i < enumValues.Length; i++)
                {
                    menuItem = new IGMenuItem(enumNames[i], new EventHandler(this.menuItem_Click));
                    UltraListViewStyle enumValue = (UltraListViewStyle)enumValues.GetValue(i);
                    menuItem.Tag = enumValue;

                    //	If this value is the same one that the control's View
                    //	property is set to, check it
                    if (this.lvwFiles.View == enumValue)
                        menuItem.Checked = true;

                    this.contextMenu1.MenuItems.Add(menuItem);
                }

                //	Add a separator
                this.contextMenu1.MenuItems.Add(new IGMenuItem("-"));

                //	Add a menu item for "Arrange Icons By"
                IGMenuItem parentMenuItem = new IGMenuItem("Arrange Icons By");
                parentMenuItem.Tag = "ArrangeIconsBy";
                this.contextMenu1.MenuItems.Add(parentMenuItem);

                //	Add a menu item for each visible column to the 'Arrange Icons By'
                //	menu item, which will provide the end user with a way to sort the
                //	items by a certain column.
                menuItem = new IGMenuItem(this.lvwFiles.MainColumn.TextResolved, new EventHandler(this.menuItem_Click));
                menuItem.Tag = this.lvwFiles.MainColumn;
                menuItem.Checked = this.lvwFiles.MainColumn.Sorting != Sorting.None;
                parentMenuItem.MenuItems.Add(menuItem);

                for (int i = 0; i < this.lvwFiles.SubItemColumns.Count; i++)
                {
                    UltraListViewSubItemColumn column = this.lvwFiles.SubItemColumns[i];
                    if (column.VisibleInDetailsView == DefaultableBoolean.False)
                        continue;

                    menuItem = new IGMenuItem(column.TextResolved, new EventHandler(this.menuItem_Click));
                    menuItem.Tag = column;
                    menuItem.Checked = column.Sorting != Sorting.None;

                    parentMenuItem.MenuItems.Add(menuItem);
                }

                //	Add a separator
                parentMenuItem.MenuItems.Add(new IGMenuItem("-"));

                //	Add a menu item for 'Show Groups'
                IGMenuItem showGroupsMenuItem = new IGMenuItem("Show Groups", new EventHandler(this.menuItem_Click));
                showGroupsMenuItem.Checked = this.lvwFiles.ShowGroups;
                showGroupsMenuItem.Tag = "ShowGroups";
                parentMenuItem.MenuItems.Add(showGroupsMenuItem);
            }
            else
                if (context is UltraListViewItem)
                {
                    //	If the context is an UltraListViewItem, add menu items
                    //	for Delete and Rename. Disable these menu items unless
                    //	their associated application constants have explicitly
                    //	been changed to true.
                    UltraListViewItem item = context as UltraListViewItem;

                    menuItem = new IGMenuItem("Delete", new EventHandler(this.menuItem_Click));
                    menuItem.Tag = item;
                    menuItem.Enabled = frmMain.ALLOW_FILE_DELETE;
                    this.contextMenu1.MenuItems.Add(menuItem);

                    menuItem = new IGMenuItem("Rename", new EventHandler(this.menuItem_Click));
                    menuItem.Tag = item;
                    menuItem.Enabled = frmMain.ALLOW_FILE_RENAME;
                    this.contextMenu1.MenuItems.Add(menuItem);
                }
                else
                    if (context is UltraListViewColumnBase)
                    {
                        //	Add a menu item for each column, which will provide the end user with a way
                        //	to change the visibility of a column.
                        menuItem = new IGMenuItem(this.lvwFiles.MainColumn.TextResolved, new EventHandler(this.menuItem_Click));
                        menuItem.Tag = this.lvwFiles.MainColumn;
                        menuItem.Checked = true;
                        menuItem.Enabled = false;
                        this.contextMenu1.MenuItems.Add(menuItem);

                        for (int i = 0; i < this.lvwFiles.SubItemColumns.Count; i++)
                        {
                            UltraListViewSubItemColumn column = this.lvwFiles.SubItemColumns[i];
                            bool visible = column.VisibleInDetailsView != DefaultableBoolean.False;

                            menuItem = new IGMenuItem(column.TextResolved, new EventHandler(this.menuItem_Click));
                            menuItem.Tag = column;
                            menuItem.Checked = visible;

                            this.contextMenu1.MenuItems.Add(menuItem);
                        }

                    }
        }
        #endregion PopulateListViewContextMenu

        #region PopulateRoot
        /// <summary>
        /// Populates the UltraTree and the UltraNavigationBar with the folders that are to be initially displayed.
        /// </summary>
        private void PopulateRoot()
        {
            //	Add Desktop Node to the tree
            UltraTreeNode desktopNode = this.tvwFolders.Nodes.Add("Desktop", "Desktop");
            desktopNode.Override.NodeAppearance.Image = this.IconManager.GetBitmap(IconName.Desktop);
            desktopNode.Override.ExpandedNodeAppearance.Image = this.IconManager.GetBitmap(IconName.Desktop);


            // Set up the root location of the navigation bar as the Desktop location
            UltraNavigationBarLocation desktopLocation = this.unbFolders.RootLocation;
            desktopLocation.Key = "Desktop";            
            desktopLocation.Settings.Appearance.Image = this.IconManager.GetBitmap(IconName.Desktop);
            // Don't include the text of the Desktop location in the full path of it's children. 
            // This is so that children of the Desktop and My Computer locations have the correct path 
            // like Windows Explorer. For example, "C:\Program Files" is a complete path. 
            // It should not be "Desktop\My Computer\C:\Program Files"
            desktopLocation.IncludeTextInFullPath = false;
            // The tree will be the central driving force of this application, so give the location a 
            // reference to the corresponding node in the tree. 
            desktopLocation.Tag = desktopNode;


            // Add the My Computer node to the tree
            UltraTreeNode myComputerNode = desktopNode.Nodes.Add("MyComputer", "My Computer");
            myComputerNode.Override.NodeAppearance.Image = this.IconManager.GetBitmap(IconName.MyComputer);
            myComputerNode.Override.ExpandedNodeAppearance.Image = this.IconManager.GetBitmap(IconName.MyComputer);
            // Add the My Computer location to the navigation bar.
            UltraNavigationBarLocation myComputerLocation = desktopLocation.Locations.Add("MyComputer", "My Computer");
            myComputerLocation.Settings.Appearance.Image = this.IconManager.GetBitmap(IconName.MyComputer);
            // Don't include the text of the My Computer location in the full path of it's children. 
            // This is so that children of the Desktop and My Computer locations have the correct path 
            // like Windows // Explorer. For example, "C:\Program Files" is a complete path. 
            // It should not be "Desktop\My Computer\C:\Program Files"
            myComputerLocation.IncludeTextInFullPath = false;

            // The tree will be the central driving force of this application, so give the location a 
            // reference to the corresponding node in the tree. 
            myComputerLocation.Tag = myComputerNode;


            //	Assign the folder/open folder icons for all generic folders
            this.tvwFolders.Override.NodeAppearance.Image = this.IconManager.GetBitmap(IconName.FolderSmall);
            this.tvwFolders.Override.ExpandedNodeAppearance.Image = this.IconManager.GetBitmap(IconName.FolderOpenSmall);

            //	Assign the folder icons for all generic folders
            this.unbFolders.LocationSettings.Appearance.Image = this.IconManager.GetBitmap(IconName.FolderSmall);

            //	Expand and select the Desktop node            
            desktopNode.Expanded = true;
            desktopNode.Selected = true;
        }
        #endregion PopulateRoot

        #region PopulateListView
        /// <summary>
        /// Populates the UltraListView control with the subdirectories and
        /// files for the directory represented by the specified UltraTreeNode.
        /// </summary>
        private void PopulateListView(UltraTreeNode node)
        {
            if (node == null || node.Tag == null)
                return;

            try
            {
                //	Call BeginUpdate to prevent drawing while we are populating the control
                this.lvwFiles.BeginUpdate();

                //	Show a wait cursor since this could take a while
                this.Cursor = Cursors.WaitCursor;

                //	Clear the Items collection to removes the directories and files
                //	of the last folder we displayed
                this.lvwFiles.Items.Clear();

                //	Get the sub-directories for the node's directory
                DirectoryInfo[] directories = this.FileSystemSupport.GetDirectories(node);

                //	Get the files for the node's directory
                FileInfo[] files = this.FileSystemSupport.GetFiles(node);

                //	Create an array of UltraListViewItems, with a size equal to the number of directories
                UltraListViewItem[] itemArrayDirectories = new UltraListViewItem[directories.Length];

                //	Create another array of UltraListViewItems, with a size equal to the number of files
                UltraListViewItem[] itemArrayFiles = new UltraListViewItem[files.Length];

                //	Add an item for each sub-directory
                for (int i = 0; i < directories.Length; i++)
                {
                    DirectoryInfo directoryInfo = directories[i];

                    //	Create an array of UltraListViewSubItems, with the same size
                    //	as the control's SubItemColumns collection.
                    UltraListViewSubItem[] subItemArray = new UltraListViewSubItem[this.lvwFiles.SubItemColumns.Count];

                    //	Create new UltraListViewSubItem instances for this item
                    for (int currentSubItem = 0; currentSubItem < subItemArray.Length; currentSubItem++)
                    {
                        subItemArray[currentSubItem] = new UltraListViewSubItem();
                    }

                    //	Assign the values to the UltraListViewSubItem instances
                    subItemArray[0].Value = null;
                    subItemArray[1].Value = "File Folder";
                    subItemArray[2].Value = directoryInfo.LastWriteTime;
                    subItemArray[3].Value = directoryInfo.CreationTime;
                    subItemArray[4].Value = FileSystemSupport.GetDisplayString(directoryInfo.Attributes);

                    //	Create an UltraListViewItem instance, using the constructor which
                    //	takes an array of UltraListViewSubItems
                    UltraListViewItem item = new UltraListViewItem(directoryInfo.Name, subItemArray);

                    //	Store a reference to the associated DirectoryInfo in the item's Tag property
                    item.Tag = directoryInfo;

                    //	Add the UltraListViewItem to the array
                    itemArrayDirectories[i] = item;
                }

                //	Add an item for each file
                for (int i = 0; i < files.Length; i++)
                {
                    FileInfo fileInfo = files[i];

                    //	Create an array of UltraListViewSubItems, with the same size
                    //	as the control's SubItemColumns collection.
                    UltraListViewSubItem[] subItemArray = new UltraListViewSubItem[this.lvwFiles.SubItemColumns.Count];

                    //	Create new UltraListViewSubItem instances for this item
                    for (int currentSubItem = 0; currentSubItem < subItemArray.Length; currentSubItem++)
                    {
                        subItemArray[currentSubItem] = new UltraListViewSubItem();
                    }

                    //	Assign the values to the UltraListViewSubItem instances
                    subItemArray[0].Value = (long)Math.Ceiling((double)fileInfo.Length / (double)1024);
                    subItemArray[1].Value = this.FileSystemSupport.GetFileType(fileInfo.FullName);
                    subItemArray[2].Value = fileInfo.LastWriteTime;
                    subItemArray[3].Value = fileInfo.CreationTime;
                    subItemArray[4].Value = FileSystemSupport.GetDisplayString(fileInfo.Attributes);

                    //	Create an UltraListViewItem instance, using the constructor which
                    //	takes an array of UltraListViewSubItems
                    UltraListViewItem item = new UltraListViewItem(fileInfo.Name, subItemArray);

                    //	Store a reference to the associated FileInfo in the item's Tag property
                    item.Tag = fileInfo;

                    //	Create or reuse an existing Appearance which contains the appropriate icon
                    //	for this item
                    this.SetAppearance(item);

                    //	If this file is a system file or dll, disable it to prevent the end
                    //	user from renaming or deleting a system file.
                    if (fileInfo.Extension.ToLower() == ".sys" || fileInfo.Extension.ToLower() == ".dll")
                        item.Enabled = false;

                    //	Add the UltraListViewItem to the array
                    itemArrayFiles[i] = item;
                }

                //	Add the items to the Items collection using the AddRange method, so that
                //	we only trigger two property change notifications, as opposed to one for
                //	each item added to the collection.
                this.lvwFiles.Items.AddRange(itemArrayDirectories);
                this.lvwFiles.Items.AddRange(itemArrayFiles);

                //	Assign the items to their respective groups
                this.AssignItemsToGroups();
            }
            finally
            {
                //	Activate the first item
                if (this.lvwFiles.ActiveItem == null && this.lvwFiles.Items.Count > 0)
                    this.lvwFiles.ActiveItem = this.lvwFiles.Items[0];

                //	Restore the cursor
                this.Cursor = Cursors.Default;

                //	Call EndUpdate to resume drawing operations
                this.lvwFiles.EndUpdate(true);
            }
        }
        #endregion PopulateListView

        #region SetAppearance
        /// <summary>
        /// If the specified item represents an executable file, bitmap,
        /// or icon, the Image property of that item's Appearance
        /// is set to the appropriate image. If the item does not represent an EXE
        /// or DLL, the UltraListView's Appearances collection is searched for an
        /// existing entry for that file extension and if one exists, it is used.
        /// If no entry exists for that file extension, an Appearance is added to the
        /// UltraListView's Appearances collection for that file extension and it is
        /// then assigned to the specified item's Appearance property.
        /// </summary>
        /// <param name="item">The UltraListViewItem for which to return an Appearance</param>
        private void SetAppearance(UltraListViewItem item)
        {
            FileInfo fileInfo = item.Tag as FileInfo;
            if (fileInfo == null)
                return;

            IntPtr iconHandle = IntPtr.Zero;

            try
            {
                string fileName = fileInfo.FullName;
                string fileExtension = fileInfo.Extension.ToLower();
                Image image = null;
                IconSize iconSize = this.lvwFiles.View == UltraListViewStyle.Details || this.lvwFiles.View == UltraListViewStyle.List ? IconSize.Small : IconSize.Large;

                //	If the file is an EXE, ICO, or BMP file, get the icon associated
                //	with that particular file, if any.
                if (fileExtension.ToLower() == ".exe" || fileExtension.ToLower() == ".ico" || fileExtension.ToLower() == ".bmp")
                {
                    Icon icon = FileSystemSupport.IconFromFile(fileName, iconSize, out iconHandle);
                    image = icon != null ? icon.ToBitmap() : null;

                    if (image != null)
                    {
                        item.Appearance.Image = image;
                        return;
                    }
                }

                //	If an Appearance already exists for this file extension,
                //	use that Appearance
                string appearanceKey = string.Format("{0}_{1}", fileExtension, iconSize.ToString());
                if (this.lvwFiles.Appearances.Exists(appearanceKey))
                    item.Appearance = this.lvwFiles.Appearances[appearanceKey];
                else
                {
                    //	Create the Appearance object for the icon associated with
                    //	this file extension and assign it to the item's Appearance.
                    Appearance appearance = new Appearance();
                    Icon icon = FileSystemSupport.IconFromFileExtension(fileExtension, iconSize, out iconHandle);
                    appearance.Image = icon != null ? DrawUtility.IconToBitmap(icon) : null;
                    int index = this.lvwFiles.Appearances.Add(appearance);
                    item.Appearance = this.lvwFiles.Appearances[index];
                }
            }
            finally
            {
                //	Add the icon
                if (iconHandle != IntPtr.Zero)
                    this.FileSystemSupport.IconHandles.Add(iconHandle);
            }

        }
        #endregion SetAppearance

        #region PopulateChildFolders

        /// <summary>
        /// Populates the child folder of a tree node and also the child locations of the navigationbar. 
        /// </summary>
        private void PopulateChildFolders(UltraNavigationBarLocation location)
        {
            // Get the tree node associated with this location. 
            UltraTreeNode node = location.Tag as UltraTreeNode;
            this.PopulateChildFolders(node, location);
        }

        /// <summary>
        /// Populates the child folder of a tree node and also the child locations of the navigationbar. 
        /// </summary>
        private void PopulateChildFolders(UltraTreeNode node)
        {
            // Get the location associated with this tree node. 
            UltraNavigationBarLocation location = this.unbFolders.FromFullPath(node.FullPath);
            this.PopulateChildFolders(node, location);
        }

        /// <summary>
        /// Populates the child folder of a tree node and also the child locations of the navigationbar. 
        /// </summary>
        private void PopulateChildFolders(UltraTreeNode node, UltraNavigationBarLocation location)
        {
            //	If we have already populated this node's Nodes collection, return
            if (node.HasNodes == true)
                return;

            if (node.Key != "MyComputer" && node.Key != "Desktop")
            {
                //	Get the subdirectories of the directory this node represents
                DirectoryInfo[] directoryInfos = this.FileSystemSupport.GetDirectories(node);
                if (directoryInfos != null && directoryInfos.Length > 0)
                {
                    for (int i = 0; i < directoryInfos.Length; i++)
                    {
                        DirectoryInfo directoryInfo = directoryInfos[i];
                        UltraTreeNode childNode = node.Nodes.Add(directoryInfo.FullName, directoryInfo.Name);
                        childNode.Tag = directoryInfo;

                        UltraNavigationBarLocation childLocation = location.Locations.Add(directoryInfo.FullName, directoryInfo.Name);
                        childLocation.Tag = childNode;                        
                    }
                }

            }
            else
                if (node.Key == "MyComputer" && node.Nodes.Count == 0)
                {
                    string[] driveNames = FileSystemSupport.GetLogicalDrives();

                    for (int i = 0; i < driveNames.Length; i++)
                    {
                        string driveName = driveNames[i];
                        DriveType driveType = FileSystemSupport.GetDriveType(driveName);
                        if (driveType == DriveType.Unknown)
                            continue;

                        string driveDisplayName = driveName.Length >= 2 ? driveName.Substring(0, 2) : driveName;
                        string displayString = string.Format("{0} ({1})", FileSystemSupport.GetDisplayString(driveType), driveDisplayName);
                        Bitmap image = null;

                        UltraTreeNode childNode = node.Nodes.Add(displayString);
                        childNode.Tag = new DirectoryInfo(driveName);

                        UltraNavigationBarLocation childLocation = location.Locations.Add(driveDisplayName, driveDisplayName);
                        childLocation.DisplayText = displayString;
                        childLocation.Tag = childNode;  

                        switch (driveType)
                        {
                            case DriveType.CDRom:
                                {
                                    image = this.IconManager.GetBitmap(IconName.CDRom);
                                }
                                break;

                            case DriveType.Fixed:
                                {
                                    image = this.IconManager.GetBitmap(IconName.LocalDisc);
                                }
                                break;

                            case DriveType.Remote:
                                {
                                    image = this.IconManager.GetBitmap(IconName.NetworkDrive);
                                }
                                break;

                            case DriveType.Removable:
                                {
                                    image = this.IconManager.GetBitmap(IconName.RemovableDrive);
                                }
                                break;
                        }

                        if (image != null)
                        {
                            childNode.Override.NodeAppearance.Image = image;
                            childNode.Override.ExpandedNodeAppearance.Image = image;

                            childLocation.Settings.Appearance.Image = image;
                        }
                    }
                }
        }
        #endregion PopulateChildFolders

        #region AssignItemsToGroups
        /// <summary>
        /// Assigns items to groups.
        /// </summary>
        private void AssignItemsToGroups()
        {
            for (int i = 0; i < this.lvwFiles.SubItemColumns.Count; i++)
            {
                UltraListViewSubItemColumn subItemColumn = this.lvwFiles.SubItemColumns[i];
                if (subItemColumn.Sorting != Sorting.None)
                {
                    this.AssignItemsToGroups(subItemColumn);
                    return;
                }
            }

            this.AssignItemsToGroups(this.lvwFiles.MainColumn);
        }

        /// <summary>
        /// Assigns items to groups based on the specified column.
        /// </summary>
        private void AssignItemsToGroups(UltraListViewColumnBase column)
        {
            //	Remove all existing groups
            this.lvwFiles.Groups.Clear();

            //	If ShowGroups is set to false, return
            if (this.lvwFiles.ShowGroups == false)
                return;

            //	Iterate the UltraListView's Items collection, and assign each
            //	item to the appropriate group. If the group does not yet exist,
            //	add it to the UltraListView's Groups collection.
            for (int i = 0; i < this.lvwFiles.Items.Count; i++)
            {
                UltraListViewItem item = this.lvwFiles.Items[i];
                object columnValue = column is UltraListViewMainColumn ? item.Text : item.SubItems[column.Key].Value;
                string groupKey = string.Empty;
                object tagValue = null;

                if (column.DataType == typeof(DateTime))
                {
                    DateTime dateValue = (DateTime)columnValue;
                    dateValue = dateValue.Date;

                    if (dateValue == DateTime.Today)
                    {
                        groupKey = "Today";
                        tagValue = 0;
                    }
                    else
                        if (dateValue == DateTime.Today.AddDays(-1f))
                        {
                            groupKey = "Yesterday";
                            tagValue = 1;
                        }
                        else
                            if (dateValue > DateTime.Today.AddDays(-7f))
                            {
                                groupKey = "In the last week";
                                tagValue = 2;
                            }
                            else
                                if (dateValue.Month != DateTime.Today.Month &&
                                     dateValue.Year == DateTime.Today.Year)
                                {
                                    groupKey = "Earlier this month";
                                    tagValue = 3;
                                }
                                else
                                    if (dateValue.Year == DateTime.Today.Year)
                                    {
                                        groupKey = "Earlier this year";
                                        tagValue = 4;
                                    }
                                    else
                                        if (dateValue.Year == (DateTime.Today.Year - 1))
                                        {
                                            groupKey = "Last year";
                                            tagValue = 5;
                                        }
                                        else
                                        {
                                            groupKey = "A long time ago";
                                            tagValue = 6;
                                        }
                }
                else
                    if (column.Key == "Size")
                    {
                        long fileSize = columnValue == null ? 0 : (long)columnValue;

                        if (item.Tag is DirectoryInfo)
                        {
                            groupKey = "Folders";
                            tagValue = 99;
                        }
                        else
                            if (fileSize == 0)
                            {
                                groupKey = "Zero";
                                tagValue = 0;
                            }
                            else
                                if (fileSize < 64)
                                {
                                    groupKey = "Tiny";
                                    tagValue = 1;
                                }
                                else
                                    if (fileSize < 128)
                                    {
                                        groupKey = "Small";
                                        tagValue = 2;
                                    }
                                    else
                                        if (fileSize < 1024)
                                        {
                                            groupKey = "Medium";
                                            tagValue = 3;
                                        }
                                        else
                                            if (fileSize < 1048576)
                                            {
                                                groupKey = "Large";
                                                tagValue = 4;
                                            }
                                            else
                                                if (fileSize < 1073741824)
                                                {
                                                    groupKey = "Gigantic";
                                                    tagValue = 5;
                                                }
                                                else
                                                {
                                                    groupKey = "So inconceivably humongous that to even glance at the file sideways causes immediate and irreversible brain damage";
                                                    tagValue = 6;
                                                }
                    }
                    else
                        if (column.Key == "Type" || column.Key == "Attributes")
                            groupKey = columnValue as string;
                        else
                            if (column is UltraListViewMainColumn)
                            {
                                groupKey = columnValue as string;
                                groupKey = groupKey.Length > 0 ? groupKey.Substring(0, 1) : " ";
                                groupKey = groupKey.ToUpper();
                            }

                item.Group = this.GetGroup(groupKey, tagValue);
            }

            //	Sort the Groups collection, using a custom IComparer
            //	implementation which will sort the groups by the value
            //	of the Tag property.
            this.lvwFiles.Groups.Sort(new GroupByValueSortComparer());
        }

        private UltraListViewGroup GetGroup(string key, object tagValue)
        {
            if (key == null || key.Length == 0)
                key = " ";

            if (tagValue == null)
                tagValue = key;

            if (this.lvwFiles.Groups.Exists(key) == false)
                this.lvwFiles.Groups.Add(key);

            UltraListViewGroup group = this.lvwFiles.Groups[key];
            group.Tag = tagValue;
            return group;
        }
        #endregion AssignItemsToGroups

        #region GetFolderIcon
        /// <summary>
        /// Returns the appropriate generic closed folder icon for the specified view.
        /// </summary>
        public Bitmap GetFolderIcon(UltraListViewStyle view)
        {
            Type type = this.GetType();

            switch (view)
            {
                case UltraListViewStyle.Details:
                case UltraListViewStyle.List:
                    {
                        return this.IconManager.GetBitmap(IconName.FolderSmall);
                    }

                case UltraListViewStyle.Icons:
                    {
                        return this.IconManager.GetBitmap(IconName.FolderMedium);
                    }

                default:
                    {
                        return this.IconManager.GetBitmap(IconName.FolderLarge);
                    }
            }
        }
        #endregion GetFolderIcon

        #region Navigate
        private void Navigate(UltraTreeNode node, NavigateSource source)
        {
            // Disable the various events to avoid recursion
            this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.AfterNavigation, false);
            this.unbFolders.EventManager.Disable(UltraNavigationBarEventIds.SelectedLocationChanged);
            this.tvwFolders.EventManager.SetEnabled(TreeEventIds.AfterSelect, false);

            try
            {
                //	Make sure that this node's Nodes collection is populated so
                //	that if the end user double-clicks on an item, we will be able
                //	to locate the associated sub-directory.
                this.PopulateChildFolders(node);

                //	Populate the UltraListView with the sub-folders and files 
                //	for the directory associated with the node that was selected
                this.PopulateListView(node);

                // If the tree was the source of the navigation, then this is unneccessary,
                // since the node is already selected. 
                if (source != NavigateSource.Tree)
                {
                    node.BringIntoView();
                    node.Selected = true;
                }


                // If the source is the navigation toolbar, then we don't want to navigate to it again. 
                if (source != NavigateSource.NavigationToolbar)
                {
                    // We only want to show the text in the drop-down menu, but store the node itself in the
                    // Tag property of a NavigationHistoryItem so that we have easy access to it later should
                    // the user traverse the navigation history.
                    this.ultraToolbarsManager1.NavigationToolbar.NavigateTo(node.Text, node);
                }


                // If the source is the navigation bar, there's no reason to navigate to it again. 
                if (source != NavigateSource.NavigationBar)
                {
                    // Since the tree and the NavigationBar are in synch, using the same path information, 
                    // we can use the FullPath of the node to navigate to the appropriate item in the 
                    // NavigationBar. 
                    this.unbFolders.NavigateTo(node.FullPath, true);
                }
            }
            finally
            {                
                this.ultraToolbarsManager1.EventManager.SetEnabled(ToolbarEventIds.AfterNavigation, true);
                this.unbFolders.EventManager.Enable(UltraNavigationBarEventIds.SelectedLocationChanged);
                this.tvwFolders.EventManager.SetEnabled(TreeEventIds.AfterSelect, true);
            }
        }
        #endregion //Navigate

        #endregion Methods

        #region Event handlers

        #region frmMain_Load
        /// <summary>
        /// Handles the main dialog's Load event.
        /// </summary>
        private void frmMain_Load(object sender, System.EventArgs e)
        {
            //	If the ALLOW_FILE_RENAME or ALLOW_FILE_DELETE constants have been changed to true,
            //	alert the user just in case this was not the intention.
            if (frmMain.ALLOW_FILE_RENAME || frmMain.ALLOW_FILE_DELETE)
            {
                string warning = "Warning: The following application-defined boolean constants have been set to true:";
                warning += Environment.NewLine;
                warning += Environment.NewLine;

                if (frmMain.ALLOW_FILE_RENAME)
                    warning += "ALLOW_FILE_RENAME" + Environment.NewLine;

                if (frmMain.ALLOW_FILE_DELETE)
                    warning += "ALLOW_FILE_DELETE" + Environment.NewLine;

                warning += Environment.NewLine;
                warning += "These constants default to false to prevent data from being modified, since this application supports the renaming and deleting of actual files and folders.";
                warning += Environment.NewLine;
                warning += Environment.NewLine;
                warning += "Would you like to continue running the application?";

                DialogResult result = MessageBox.Show(warning, "Infragistics File Explorer", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);

                if (result == DialogResult.No)
                {
                    this.Close();
                    return;
                }
            }

            this.InitializeUI();
        }
        #endregion //frmMain_Load

        #region tvwFolders_BeforeExpand
        /// <summary>
        /// Handles the UltraTree's BeforeExpand event.
        /// </summary>
        private void tvwFolders_BeforeExpand(object sender, CancelableNodeEventArgs e)
        {
            this.PopulateChildFolders(e.TreeNode);
        }
        #endregion //tvwFolders_BeforeExpand

        #region tvwFolders_MouseUp
        /// <summary>
        /// Handles the UltraTree's MouseUp event
        /// </summary>
        private void tvwFolders_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            UltraTree ultraTree = sender as UltraTree;
            Point pointClicked = new Point(e.X, e.Y);
            UltraTreeNode nodeAtPoint = ultraTree.GetNodeFromPoint(pointClicked);
            if (nodeAtPoint != null && nodeAtPoint.Expanded == false)
            {
                ExpansionIndicatorUIElement expansionIndicatorElement = null;
                expansionIndicatorElement = ultraTree.UIElement.ElementFromPoint(pointClicked) as ExpansionIndicatorUIElement;
                if (expansionIndicatorElement == null)
                    nodeAtPoint.Expanded = true;
            }
        }
        #endregion //tvwFolders_MouseUp

        #region tvwFolders_AfterSelect
        /// <summary>
        /// Handles the UltraTree's AfterSelect event
        /// </summary>
        private void tvwFolders_AfterSelect(object sender, SelectEventArgs e)
        {
            UltraTreeNode selectedNode = e.NewSelections.Count == 1 ? e.NewSelections[0] : null;
            if (selectedNode == null)
                return;

            this.Navigate(selectedNode, NavigateSource.Tree);
        }
        #endregion //tvwFolders_AfterSelect

        #region menuItem_Click
        /// <summary>
        /// Handles the Click event for all IGMenuItems
        /// </summary>
        private void menuItem_Click(object sender, EventArgs e)
        {
            IGMenuItem menuItem = sender as IGMenuItem;
            if (menuItem != null)
            {
                if (menuItem.Tag is UltraListViewStyle)
                {
                    menuItem.Checked = true;
                    UltraListViewStyle view = (UltraListViewStyle)menuItem.Tag;
                    this.lvwFiles.View = view;
                }
                else
                    if (menuItem.Tag is string && (string)menuItem.Tag == "ShowGroups")
                    {
                        this.lvwFiles.ShowGroups = !this.lvwFiles.ShowGroups;
                        this.AssignItemsToGroups();
                    }
                    else
                        if (menuItem.Tag is UltraListViewColumnBase)
                        {
                            if (menuItem.Parent != this.contextMenu1)
                            {
                                IGMenuItem parent = menuItem.Parent as IGMenuItem;
                                if (parent.Tag is string && (string)parent.Tag == "ArrangeIconsBy")
                                {
                                    UltraListViewColumnBase column = menuItem.Tag as UltraListViewColumnBase;
                                    column.Sorting = column.Sorting == Sorting.Descending ? Sorting.Ascending : Sorting.Descending;
                                    this.AssignItemsToGroups(column);
                                }
                            }
                            else
                            {
                                //	Toggle the visibility of the column
                                UltraListViewSubItemColumn column = menuItem.Tag as UltraListViewSubItemColumn;
                                if (column != null)
                                {
                                    column.VisibleInDetailsView = column.VisibleInDetailsView == DefaultableBoolean.False ? DefaultableBoolean.True : DefaultableBoolean.False;
                                    column.VisibleInTilesView = column.VisibleInDetailsView;
                                    column.VisibleInToolTip = column.VisibleInDetailsView;
                                }
                            }
                        }
                        else
                            if (menuItem.Tag is UltraListViewItem)
                            {
                                UltraListViewItem item = menuItem.Tag as UltraListViewItem;

                                if (menuItem.Text == "Delete")
                                {
                                    string message = "Are you sure you want to delete the specified items?";
                                    DialogResult result = MessageBox.Show(message, "Infragistics File Explorer", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                                    if (result == DialogResult.Yes)
                                    {
                                        //	Reverse-iterate the SelectedItems collection, and remove each one,
                                        //	provided that it is not a protected file.
                                        UltraListViewSelectedItemsCollection selectedItems = this.lvwFiles.SelectedItems;
                                        for (int i = selectedItems.Count - 1; i >= 0; i--)
                                        {
                                            UltraListViewItem selectedItem = selectedItems[i];
                                            if (this.FileSystemSupport.DeleteFileSystemObject(selectedItem))
                                                this.lvwFiles.Items.Remove(selectedItem);
                                        }
                                    }
                                }
                                else
                                    if (menuItem.Text == "Rename")
                                        item.BeginEdit();
                            }
            }
        }
        #endregion //menuItem_Click

        #region lvwFiles_MouseDown
        /// <summary>
        /// Handles the UltraListView's MouseDown event
        /// </summary>
        private void lvwFiles_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                Point cursorPos = new Point(e.X, e.Y);
                UltraListView listView = sender as UltraListView;

                //	Use the UltraListView's 'ItemFromPoint' method to hit test for an
                //	UltraListViewItem. Note that we specify true for the 'selectableAreaOnly'
                //	parameter, so that we only get a hit when the cursor is over the text
                //	or image area of the item.
                UltraListViewItem itemAtPoint = listView.ItemFromPoint(cursorPos, true);

                //	If we got a reference to an item, populate the context menu
                //	accordingly and return
                if (itemAtPoint != null)
                {
                    this.PopulateListViewContextMenu(itemAtPoint);
                    return;
                }

                //	Use the UIElement class' ElementFromPoint method to hit test for
                //	the UIElement that represents the header of an UltraListViewColumnBase
                UIElement elementAtPoint = listView.UIElement.ElementFromPoint(cursorPos);
                while (elementAtPoint != null)
                {
                    UltraListViewColumnHeaderUIElement headerElement = elementAtPoint as UltraListViewColumnHeaderUIElement;

                    if (headerElement != null)
                    {
                        //	If we got a reference to a column, populate the context menu
                        //	accordingly and return
                        UltraListViewColumnBase column = headerElement.Column;
                        this.PopulateListViewContextMenu(column);
                        return;
                    }

                    elementAtPoint = elementAtPoint.Parent;
                }

                //	In lieu of any more specific context, populate the context menu
                //	with the different choices for the View property.
                this.PopulateListViewContextMenu(listView.View);
            }
        }
        #endregion //lvwFiles_MouseDown

        #region lvwFiles_ViewChanged
        /// <summary>
        /// Handles the UltraListView's ViewChanged event
        /// </summary>
        private void lvwFiles_ViewChanged(object sender, System.EventArgs e)
        {
            //	Set the DefaultImage property to the appropriate icon
            //	for the current view
            UltraListView listView = sender as UltraListView;
            listView.ItemSettings.DefaultImage = this.GetFolderIcon(listView.View);

            //	Repopulate the UltraListView, so that the appropriate icons
            //	are used for the files as well
            this.PopulateListView(this.tvwFolders.ActiveNode);
        }
        #endregion //lvwFiles_ViewChanged

        #region lvwFiles_ItemDoubleClick
        /// <summary>
        /// Handles the UltraListView's ItemDoubleClick event
        /// </summary>
        private void lvwFiles_ItemDoubleClick(object sender, Infragistics.Win.UltraWinListView.ItemDoubleClickEventArgs e)
        {
            //	If the item that was double-clicked represents a directory,
            //	locate the associated UltraTreeNode and select it, which
            //	will cause the UltraListView to be repopulated with that
            //	directory's contents.
            DirectoryInfo directoryInfo = e.Item.Tag as DirectoryInfo;
            if (directoryInfo != null)
            {
                UltraTreeNode node = this.tvwFolders.GetNodeByKey(directoryInfo.FullName);
                if (node != null)
                {
                    this.tvwFolders.ActiveNode = node;
                    node.Selected = true;
                    this.PopulateChildFolders(node);
                }
            }
        }
        #endregion //lvwFiles_ItemDoubleClick

        #region lvwFiles_ItemExitingEditMode
        /// <summary>
        /// Handles the UltraListView's ItemExitingEditMode event.
        /// </summary>
        private void lvwFiles_ItemExitingEditMode(object sender, Infragistics.Win.UltraWinListView.ItemExitingEditModeEventArgs e)
        {
            //	If the changes are being committed, rename the associated directory/file
            if (e.ApplyChanges)
            {
                DirectoryInfo directoryInfo = e.Item.Tag as DirectoryInfo;
                FileInfo fileInfo = e.Item.Tag as FileInfo;

                //	First, make sure the file or folder does not already exist;
                //	if it does, display a message to the end user and cancel the event.
                bool exists = false;

                if (fileInfo != null)
                {
                    string newFileName = string.Format(@"{0}\{1}", fileInfo.DirectoryName, e.Editor.CurrentEditText);
                    exists = System.IO.File.Exists(newFileName);
                }

                if (directoryInfo != null)
                {
                    string newFolderName = string.Format(@"{0}\{1}", fileInfo.DirectoryName, e.Editor.CurrentEditText);
                    exists = System.IO.Directory.Exists(newFolderName);
                }

                if (exists)
                {
                    string message = string.Format("Cannot rename {0}: A file or folder with the name you specified already exists. Specify a different name.", e.Editor.CurrentEditText);
                    MessageBox.Show(message, "Error Renaming File or Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }


                //	If the end user is trying to rename a system file or folder, set
                //	the ApplyChanges property of the event arguments to false so that
                //	the change is not committed.
                if (directoryInfo != null || fileInfo != null)
                    e.ApplyChanges = this.FileSystemSupport.RenameFileSystemObject(e.Item, e.Editor.CurrentEditText);
            }
        }
        #endregion //lvwFiles_ItemExitingEditMode

        #region lvwFiles_ToolTipDisplaying
        /// <summary>
        /// Handles the UltralistView's ToolTipDisplaying event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lvwFiles_ToolTipDisplaying(object sender, Infragistics.Win.UltraWinListView.ToolTipDisplayingEventArgs e)
        {
            //	Override the default tooltip behavior for folder items,
            //	so we can display a summary of the contents of the folder
            //	when the end user hovers over the item.
            if (e.IsToolTipForSubItem)
                return;

            UltraListViewItem item = e.Item;
            DirectoryInfo directoryInfo = item.Tag as DirectoryInfo;
            if (directoryInfo != null)
            {
                StringBuilder sb = new StringBuilder();
                DirectoryInfo[] directories = directoryInfo.GetDirectories();
                FileInfo[] files = directoryInfo.GetFiles();
                int limiter = 0;

                if (directories.Length == 0 && files.Length == 0)
                    sb.Append("Folder is empty");
                else
                    if (directories.Length > 0)
                    {
                        sb.Append(directoryInfo.Name);
                        sb.Append(Environment.NewLine);
                        sb.Append("Folders: ");
                        sb.Append(Environment.NewLine);

                        limiter = Math.Min(5, directories.Length - 1);
                        for (int i = 0; i <= limiter; i++)
                        {
                            sb.Append(directories[i].Name);
                            if (i < limiter)
                                sb.Append(", ");
                        }

                        if (limiter < (directories.Length - 1))
                            sb.Append("...");
                    }
                    else
                        if (files.Length > 0)
                        {
                            sb.Append(directoryInfo.Name);
                            sb.Append(Environment.NewLine);
                            sb.Append("Files: ");
                            sb.Append(Environment.NewLine);

                            limiter = Math.Min(5, files.Length - 1);
                            for (int i = 0; i <= limiter; i++)
                            {
                                sb.Append(files[i].Name);
                                if (i < limiter)
                                    sb.Append(", ");
                            }

                            if (limiter < (files.Length - 1))
                                sb.Append("...");
                        }


                e.ToolTipText = sb.ToString();
            }
        }
        #endregion //lvwFiles_ToolTipDisplaying

        #region lvwFiles_KeyDown
        /// <summary>
        /// Handles the UltraListView's KeyDown event.
        /// </summary>
        private void lvwFiles_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            UltraListView listView = sender as UltraListView;

            if (listView.ActiveItem != null && listView.ActiveItem.IsInEditMode == false)
            {
                //	If the return key is pressed, call the ItemDoubleClick event
                //	handler code, so that the same logic gets precessed as would
                //	for a double-click
                if (e.KeyData == Keys.Return)
                {
                    this.lvwFiles_ItemDoubleClick(listView, new ItemDoubleClickEventArgs(listView.ActiveItem));
                    e.Handled = true;

                    //	Even though we are handling the keystroke, the KeyPress event
                    //	can still fire so set a flag that will prevent that.
                    this.eatNextKeyPress = true;
                }
                else
                    //	If the delete key is pressed, call the context menu item event
                    //	handler code, so that the same logic gets precessed as would
                    //	for a context menu delete
                    if (e.KeyData == Keys.Delete)
                    {
                        if (frmMain.ALLOW_FILE_DELETE)
                        {
                            IGMenuItem menuItem = new IGMenuItem("Delete");
                            menuItem.Tag = listView.ActiveItem;
                            this.menuItem_Click(menuItem, EventArgs.Empty);
                            e.Handled = true;

                            //	Even though we are handling the keystroke, the KeyPress event
                            //	can still fire so set a flag that will prevent that.
                            this.eatNextKeyPress = true;
                        }
                    }
            }
        }
        #endregion //lvwFiles_KeyDown

        #region lvwFiles_KeyPress
        /// <summary>
        /// Handles the UltraListView's KeyPress event.
        /// </summary>
        private void lvwFiles_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            //	If we handled the last KeyDown, mark this event as handled as well.
            if (this.eatNextKeyPress)
            {
                this.eatNextKeyPress = false;
                e.Handled = true;
            }
        }
        #endregion //lvwFiles_KeyPress

        #region ultraToolbarsManager1_AfterNavigation
        /// <summary>
        /// Handles the UltraToolbarsManager's AfterNavigation event.
        /// </summary>
        private void ultraToolbarsManager1_AfterNavigation(object sender, Infragistics.Win.UltraWinToolbars.AfterNavigationEventArgs e)
        {
            // Since we've stored each node in the Tag property of a NavigationHistoryItem, we can now
            // easily select that node now.
            UltraTreeNode node = e.CurrentItem.Tag as UltraTreeNode;
            this.Navigate(node, NavigateSource.NavigationToolbar);
        }
        #endregion //ultraToolbarsManager1_AfterNavigation

        #region unbFolders_SelectedLocationChanged
        private void unbFolders_SelectedLocationChanged(object sender, SelectedLocationChangedEventArgs e)
        {
            // Since we've stored each node in the Tag property of a Location, we can now
            // easily select that node now.            
            UltraTreeNode node = e.SelectedLocation.Tag as UltraTreeNode;            
            this.Navigate(node, NavigateSource.NavigationBar);
        }
        #endregion unbFolders_SelectedLocationChanged

        #endregion Event handlers
    }
	#endregion frmMain class

	#region FileSystemSortComparer class
		/// <summary>
		/// IComparer implementor used to sort UltraListViewItems in such a way
		/// that items which represent folders always appear before those which
		/// represent files.
		/// </summary>
		public class FileSystemSortComparer : IComparer
		{
			private UltraListViewColumnBase column = null;

			/// <summary>
			/// Constructor.
			/// </summary>
			public FileSystemSortComparer( UltraListViewColumnBase column )
			{
				this.column = column;
			}

			int IComparer.Compare( object x, object y )
			{
				UltraListViewItem item1 = x as UltraListViewItem;
				UltraListViewItem item2 = y as UltraListViewItem;

				if ( item1 == item2 )
					return 0;

				//	If one item represents a directory and the other represents a file,
				//	return a value that will sort the directory before the file.
				if ( item1.Tag is DirectoryInfo && item2.Tag is FileInfo )
					return -1;
				else
				if ( item1.Tag is FileInfo && item2.Tag is DirectoryInfo )
					return 1;
					 
				int retVal = 0;

				//	Get the values to compare
				UltraListViewMainColumn mainColumn = this.column as UltraListViewMainColumn;
				UltraListViewSubItemColumn subItemColumn = this.column as UltraListViewSubItemColumn;

				object value1 = mainColumn != null ? item1.Value : item1.SubItems[subItemColumn].Value;
				object value2 = mainColumn != null ? item2.Value : item2.SubItems[subItemColumn].Value;

				if ( value1 == null && value2 == null )
					retVal = 0;
				else
				if ( value1 == null && value2 != null )
					retVal = -1;
				else
				if ( value1 != null && value2 == null )
					retVal = 1;
				else
				if ( value1 is IComparable && value2 is IComparable )
				{
					IComparable comp1 = value1 as IComparable;
					IComparable comp2 = value2 as IComparable;
					retVal = comp1.CompareTo( comp2 );
				}

				//	Flip the sign of the returned value if the column's Sorting
				//	property is set to 'Descending'
				return this.column.Sorting == Sorting.Descending ? retVal * -1 : retVal;
			}
		}
	#endregion FileSystemSortComparer class

	#region GroupByValueSortComparer class
		/// <summary>
		/// IComparer implementor used to sort UltraListViewGroups based on
		/// the value stored in the Tag property rather than by the value of
		/// the Text property.
		/// </summary>
		public class GroupByValueSortComparer : IComparer
		{
			int IComparer.Compare( object x, object y )
			{
				UltraListViewGroup group1 = x as UltraListViewGroup;
				UltraListViewGroup group2 = y as UltraListViewGroup;

				if ( group1 == group2 )
					return 0;

				IComparable comp1 = group1.Tag as IComparable;
				IComparable comp2 = group2.Tag as IComparable;

				if ( comp1 == null || comp2 == null )
					return 0;

				return comp1.CompareTo( comp2 );
			}
		}
	#endregion GroupByValueSortComparer class
}
