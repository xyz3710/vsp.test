using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinListView.Samples
{
	#region IconManager class
	/// <summary>
	/// Class which serves bitmaps created from icons embedded as resources.
	/// </summary>
	public class IconManager : IDisposable
	{
		#region Constants
		
		private static readonly int iconCount = 11;
		private static readonly string iconFolderName = "Icons";
		
		#endregion Constants

		#region Member variables

		private Form		owningForm = null;
		private Bitmap[]	bitmaps = new Bitmap[IconManager.iconCount];

		#endregion Member variables

		#region Constructor
		/// <summary>
		/// Creates a new instance of the <see cref="IconManager"/> class.
		/// </summary>
		/// <param name="form">The form which contains the embedded icon resources.</param>
		public IconManager( Form form )
		{
			this.owningForm = form;
		}
		#endregion Constructor

		#region GetIcon
		/// <summary>
		/// Returns the Icon corresponding to the specified IconName constant.
		/// Note that the caller is responsible for maintaining the
		/// lifetime of the returned icon, i.e., must dispose of it
		/// when it is no longer needed.
		/// </summary>
		private Icon GetIcon( IconName iconName )
		{
			string fileName = string.Format( "{0}.{1}.ICO", IconManager.iconFolderName, iconName.ToString() );
			Type type = this.owningForm.GetType();

			System.IO.Stream stream = type.Module.Assembly.GetManifestResourceStream( type, fileName );

			Icon icon = null;
			if ( stream != null )
				icon = new Icon( stream );

			stream.Close();

			return icon;
		}
		#endregion GetIcon

		#region GetBitmap
		/// <summary>
		/// Returns the Bitmap corresponding to the specified IconName.
		/// If no icon exists in the associated form's resources, null is returned.
		/// </summary>
		public Bitmap GetBitmap( IconName iconName )
		{
			int index = (int)iconName;

			if ( this.bitmaps[index] == null )
			{
				Icon icon = this.GetIcon( iconName );
				if ( icon != null )
				{
					//	Under version 1.x of the .NET framework, the Icon.ToBitmap method
					//	does not properly preserve transparency information; use the DrawUtility's
					//	IconToBitmap method which does handle it correctly, so the icons which
					//	contain transparent and semi-transparent pixels appear correctly.
					this.bitmaps[index] = DrawUtility.IconToBitmap( icon );
					
					//	Dispose of the icon since we have converted it to a Bitmap
					icon.Dispose();
				}
			}

			return this.bitmaps[index];
		}
		#endregion GetBitmap

		#region IDisposable interface implementation

		/// <summary>
		/// Implements the IDisposable interface's Dispose method.
		/// Disposes of all Bitmaps created by this instance.
		/// </summary>
		void IDisposable.Dispose()
		{
			for ( int i = 0; i < IconManager.iconCount; i ++ )
			{
				if ( this.bitmaps[i] != null )
				{
					this.bitmaps[i].Dispose();
					this.bitmaps[i] = null;
				}
			}
		}

		#endregion IDisposable interface implementation

	}
	#endregion IconManager class

	#region IconName enumeration
	public enum IconName
	{
		//	Members of this enumeration must have the same name as
		//	the filename of the associated icon.

		CDRom,
		Desktop,
		FolderLarge,
		FolderMedium,
		FolderOpenSmall,
		FolderSmall,
		LocalDisc,
		MyComputer,
		NetworkDrive,
		RemovableDrive,
	}
	#endregion IconGroup enumeration

}