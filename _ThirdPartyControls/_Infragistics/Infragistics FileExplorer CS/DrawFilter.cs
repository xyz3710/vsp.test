#region Using directives

using System;
using System.Drawing;
using Infragistics.Win;
using Infragistics.Win.UltraWinListView;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

#endregion Using directives

namespace Infragistics.Win.UltraWinListView.Samples
{
	#region UltraListViewSelectedImageDrawFilter class

	/// <summary>
	/// IUIElementDrawFilter interface implementor used to draw images as selected.
	/// </summary>
	public class UltraListViewSelectedImageDrawFilter : IUIElementDrawFilter
	{
		#region Constants
		private static readonly double[] RED_TINT_FACTOR = new double[4]{.59f, .78f, .85f, .52f  };
		private static readonly double[] GREEN_TINT_FACTOR = new double[4]{.7f, .82f, .85f, .57f };
		private static readonly double[] BLUE_TINT_FACTOR = new double[4]{.88f, .72f, .87f, .71f };
		#endregion Constants
	
		#region Properties

			#region RedTintFactor
		/// <summary>
		/// Returns the tint factor for the red component of bitmap colors
		/// for the currently selected theme.
		/// </summary>
		static public double RedTintFactor
		{
			get
			{
				switch ( XPThemes.CurrentThemeName )
				{
					case "NormalColor": { return UltraListViewSelectedImageDrawFilter.RED_TINT_FACTOR[0]; }
					case "HomeStead": { return UltraListViewSelectedImageDrawFilter.RED_TINT_FACTOR[1]; }
					case "Metallic": { return UltraListViewSelectedImageDrawFilter.RED_TINT_FACTOR[2]; }
					default: { return UltraListViewSelectedImageDrawFilter.RED_TINT_FACTOR[3]; }
				}
			}
		}
			#endregion RedTintFactor

			#region GreenTintFactor
		/// <summary>
		/// Returns the tint factor for the green component of bitmap colors
		/// for the currently selected theme.
		/// </summary>
		static public double GreenTintFactor
		{
			get
			{
				switch ( XPThemes.CurrentThemeName )
				{
					case "NormalColor": { return UltraListViewSelectedImageDrawFilter.GREEN_TINT_FACTOR[0]; }
					case "HomeStead": { return UltraListViewSelectedImageDrawFilter.GREEN_TINT_FACTOR[1]; }
					case "Metallic": { return UltraListViewSelectedImageDrawFilter.GREEN_TINT_FACTOR[2]; }
					default: { return UltraListViewSelectedImageDrawFilter.GREEN_TINT_FACTOR[3]; }
				}
			}
		}
			#endregion GreenTintFactor

			#region BlueTintFactor
		/// <summary>
		/// Returns the tint factor for the green component of bitmap colors
		/// for the currently selected theme.
		/// </summary>
		static public double BlueTintFactor
		{
			get
			{
				switch ( XPThemes.CurrentThemeName )
				{
					case "NormalColor": { return UltraListViewSelectedImageDrawFilter.BLUE_TINT_FACTOR[0]; }
					case "HomeStead": { return UltraListViewSelectedImageDrawFilter.BLUE_TINT_FACTOR[1]; }
					case "Metallic": { return UltraListViewSelectedImageDrawFilter.BLUE_TINT_FACTOR[2]; }
					default: { return UltraListViewSelectedImageDrawFilter.BLUE_TINT_FACTOR[3]; }
				}
			}
		}
			#endregion BlueTintFactor

		#endregion Properties

		#region Methods

			#region TintBitmap
		/// <summary>
		/// Tints the colors of the specified Bitmap by the specified factors.
		/// </summary>
		/// <param name="bitmap">The Bitmap to be tinted.</param>
		/// <param name="redFactor">The factor of the red component by which to tint colors.</param>
		/// <param name="greenFactor">The factor of the green component by which to tint colors.</param>
		/// <param name="blueFactor">The factor of the blue component by which to tint colors.</param>
		static public void TintBitmap( ref Bitmap bitmap, double redFactor, double greenFactor, double blueFactor )
		{
			if ( bitmap == null )
				return;

			int height = bitmap.Height;
			int width = bitmap.Width;

			BitmapData bitmapData = null;
			try
			{
				//	Create a BitmapData instance for the specified Bitmap
				bitmapData = new BitmapData();
				Rectangle rect = new Rectangle( 0, 0, width, height );
				bitmapData = bitmap.LockBits( rect, ImageLockMode.ReadWrite, bitmap.PixelFormat );
				int scanLineWidth = bitmapData.Stride;
				int pixelSize = scanLineWidth / bitmap.Width;

				// create a byte array to hold the bytes for the image
				byte[] bytes = new Byte[scanLineWidth * height];

				IntPtr address = bitmapData.Scan0;

				// get the image data for the image
				Marshal.Copy(address, bytes, 0, bytes.Length);

				// iterate through the scan lines
				for (int scan = 0; scan < height; scan++)
				{
					// keep an offset into the byte array for the start of the next pixel
					int offset = scan * scanLineWidth;

					// interate the columns
					for (int x = 0; x < width; x++)
					{
						Color pixelColor = Color.FromArgb(bytes[offset + 2], bytes[offset + 1], bytes[offset]);

						int newRed = Math.Min( (int)(pixelColor.R * redFactor), 255 );
						int newGreen = Math.Min( (int)(pixelColor.G * greenFactor), 255 );
						int newBlue = Math.Min( (int)(pixelColor.B * blueFactor), 255 );

						bytes[offset + 2] = (byte)newRed;
						bytes[offset + 1] = (byte)newGreen;
						bytes[offset] = (byte)newBlue;

						offset += pixelSize;
					}
				}

				// update the image
				Marshal.Copy(bytes, 0, address, bytes.Length);
			}
			finally
			{
				if ( bitmapData != null )
					bitmap.UnlockBits( bitmapData );
			}
		}

			#endregion TintBitmap

		#endregion Methods

		#region IUIElementDrawFilter interface implementation

		public Infragistics.Win.DrawPhase GetPhasesToFilter( ref UIElementDrawParams drawParams )
		{
			//	If the element is the UIElement which represents the item's image,
			//	and the associated item is selected, return DrawPhase.BeforeDrawImage
			//	to signify that we want to handle the drawing for this element.
			UltraListViewImageUIElement imageElement = drawParams.Element as UltraListViewImageUIElement;
			UltraListViewItemUIElement itemElement =	imageElement != null ?
														imageElement.GetAncestor( typeof(UltraListViewItemUIElement) ) as UltraListViewItemUIElement :
														null;

			UltraListViewItem item = itemElement != null ? itemElement.Item : null;

			if ( item != null && item.IsSelected )
				return DrawPhase.BeforeDrawImage;

			return DrawPhase.None;
		}

		public bool DrawElement( DrawPhase drawPhase, ref UIElementDrawParams drawParams )
		{
			UltraListViewImageUIElement imageElement = drawParams.Element as UltraListViewImageUIElement;
			if ( imageElement != null )
			{
				//	Clone the original bitmap
				Graphics g = drawParams.Graphics;
				Bitmap originalBitmap = imageElement.Image as Bitmap;
				Bitmap clonedBitmap = originalBitmap.Clone() as Bitmap;

				//	Apply the tinted colors to the cloned bitmap
				UltraListViewSelectedImageDrawFilter.TintBitmap( ref clonedBitmap, RedTintFactor, GreenTintFactor, BlueTintFactor );

				//	Draw the color washed bitmap onto the Graphics object
				g.DrawImage( clonedBitmap, imageElement.Rect );

				clonedBitmap.Dispose();

				return true;
			}

			return false;
		}

		#endregion	IUIElementDrawFilter interface implementation


	}
	#endregion UltraListViewSelectedImageDrawFilter class
}