#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win.UltraWinTabs;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Maintains defaultable property settings for an <see cref="MdiTabGroup"/>.
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>MdiTabGroupSettings</b> object is used to affect the appearance and behavior of <see cref="MdiTabGroup"/> 
	/// instances. The values of each property are initialized with default values. These default values are resolved 
	/// by a <see cref="MdiTabGroupSettingsResolved"/> instance that may be accessed from the group's <see cref="MdiTabGroup.SettingsResolved"/> 
	/// property.</p>
	/// 
	/// <p class="body">The most commonly used properties are the <b>TabStyle</b> and <b>TabSizing</b>. The <see cref="TabStyle"/> property is used to determine the look and feel of the tab items and affects the shape of the 
	/// tab item borders. The <see cref="TabSizing"/> property determines how the size of the tab items is calculated. By default, tab items are sized 
	/// based on the amount of space required to display the tab's image and text. The size may be constrained using the <see cref="MinTabWidth"/> and 
	/// <see cref="MaxTabWidth"/> to ensure that the tab items are always within a reasonable size.</p>
	/// 
	/// <p class="body">The class exposes several appearance properties that can be used to affect the appearance of the 
	/// tab area (the area behind the tab items), scroll related items, tab list and close buttons. The tab area may be modified using the 
	/// <see cref="TabAreaAppearance"/>. The scroll items can be modified using the <see cref="ScrollTrackAppearance"/> (scroll thumb 
	/// and track and the <see cref="ScrollButtonAppearance"/>, which affects the scroll buttons and scroll thumb appearance. The <see cref="CloseButtonAppearance"/> 
	/// will affect the appearance of the close button (displayed as an "X" in the tab group area).  The <see cref="TabListButtonAppearance"/> 
	/// will affect the appearance of the tab list button (displayed as a downward arrow indicator in the tab group area).</p>
	/// 
	/// <p class="body">Several properties for manipulating the layout of the tab items are available in this class. The <see cref="TabOrientation"/> determines the area 
	/// in which the tab items will be displayed. By default, the tab items are located on the top and positioned from left to right. This property may be used to 
	/// position the items on the left, right or bottom. The <see cref="TextOrientation"/> is used to determine how the text of the 
	/// tab is displayed with respect to the orientation of the tabs. <see cref="TabPadding"/> may be used to adjust the amount of space 
	/// between the contents of the tab and the borders.</p>
	/// 
	/// <p class="body">The class also has several properties to determine how and when scroll related items are displayed. The most commonly used 
	/// of these are the <see cref="ScrollButtons"/> and <see cref="ScrollButtonTypes"/>. </p>
	/// </remarks>
	/// <seealso cref="MdiTabGroupSettingsResolved"/>
	/// <seealso cref="UltraTabbedMdiManager.TabGroupSettings"/>
	/// <seealso cref="MdiTabGroup.Settings"/>
	/// <seealso cref="MdiTabGroup.SettingsResolved"/>
	[Serializable()]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MdiTabGroupSettings : SubObjectBase,
		IDeserializationCallback,
		ISerializable
	{
		#region Member Variables

		private object						owner = null;

		private const int					AutoSelectDelayDefault = -1;
		private const int					ScrollIncrementDefault = -1;
		private const int					TabsPerRowDefault = -1;
		private const int					TabHeightDefault = -1;
		private const int					MaxTabWidthDefault = -1;
		private const int					MinTabWidthDefault = -1;
		private const int					ScrollTrackExtentDefault = -1;

		private DefaultableBoolean			allowDrop = DefaultableBoolean.Default;
		private DefaultableBoolean			autoSelect = DefaultableBoolean.Default;
		private DefaultableBoolean			showButtonSeparators = DefaultableBoolean.Default;

		// MD 11/7/07 - NA 2008 Vol 1
		// The ShowCloseButton property is now obsolete, this is no longer needed
		//private DefaultableBoolean			showCloseButton = DefaultableBoolean.Default;

		// JDN 8/13/04 NAS2004 Vol3
		private	DefaultableBoolean			showPartialTabs = DefaultableBoolean.Default;
		// JDN 8/17/04 NAS2004 Vol3
		private DefaultableBoolean			showTabListButton = DefaultableBoolean.Default;

		private int							autoSelectDelay = AutoSelectDelayDefault;
		private DefaultableInteger			interTabSpacing = DefaultableInteger.Default;
		private int							tabHeight = TabHeightDefault;
		private int							tabsPerRow = TabsPerRowDefault;
		private int							maxTabWidth = MaxTabWidthDefault;
		private int							minTabWidth = MinTabWidthDefault;
		private int							scrollTrackExtent = ScrollTrackExtentDefault;

		private UIElementButtonStyle		buttonStyle = UIElementButtonStyle.Default;
		private UIElementButtonStyle		tabButtonStyle = UIElementButtonStyle.Default;

		private ScrollArrowStyle			scrollArrowStyle = ScrollArrowStyle.Default;

		private DefaultableTabScrollButtons	scrollButtons = DefaultableTabScrollButtons.Default;
		private TabOrientation				tabOrientation = TabOrientation.Default;
		private TextOrientation				textOrientation = TextOrientation.Default;

		private AppearanceHolder			closeButtonAppearanceHolder = null;
		private AppearanceHolder			scrollButtonAppearanceHolder = null;
		private AppearanceHolder			tabAreaAppearanceHolder = null;
		private AppearanceHolder			scrollTrackAppearanceHolder = null;
		// JDN 8/18/04 NAS2004 Vol3
		private AppearanceHolder			tabListButtonAppearanceHolder = null;

		private static readonly Size		TabPaddingDefault = new Size(-1,-1);
		private Size						tabPadding = TabPaddingDefault;

		private TabSizing					tabSizing = TabSizing.Default;
		private TabStyle					tabStyle = TabStyle.Default;

		private ScrollButtonTypes			scrollButtonTypes = ScrollButtonTypes.Default;

		// AS 2/6/04 WTB1314
		private Margins						tabAreaMargins;

		// JAS 12/20/04 Tab Spacing
		private DefaultableInteger			spaceBeforeTabs	= DefaultableInteger.Default;		
		private DefaultableInteger			spaceAfterTabs	= DefaultableInteger.Default;

		// MD 11/6/07 - NA 2008 Vol 1
		private TabCloseButtonLocation		closeButtonLocation = TabCloseButtonLocation.Default;
		private AppearanceHolder			hotTrackCloseButtonAppearanceHolder;
		private AppearanceHolder			pressedCloseButtonAppearanceHolder;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupSettings"/>
		/// </summary>
		/// <param name="owner">Owning object. Should be a <see cref="MdiTabGroup"/> or <see cref="UltraTabbedMdiManager"/> instance</param>
		public MdiTabGroupSettings(object owner)
		{
			this.owner = owner;
		}

		/// <summary>
		/// Constructor used during deserialization to initialize a new <see cref="MdiTabGroupSettings"/> object
		/// with the serialized property values.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected MdiTabGroupSettings( SerializationInfo info, StreamingContext context )
		{
			foreach( SerializationEntry entry in info )
			{
				switch (entry.Name)
				{
					case MdiTabGroupSettings.TagSerializationName:
						this.DeserializeTag(entry);
						break;

					case "AllowDrop":
						this.allowDrop = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.allowDrop);
						break;
					case "AutoSelect":
						this.autoSelect = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.autoSelect);
						break;
					case "ShowButtonSeparators":
						this.showButtonSeparators = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.showButtonSeparators);
						break;
					case "ShowCloseButton":
						// MD 11/7/07 - NA 2008 Vol 1
						// The show close button member doesn't exist anymore, map the deserialized value for it to
						// the new closeButtonLocation member.
						//this.showCloseButton = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.showCloseButton);
						DefaultableBoolean value = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), DefaultableBoolean.Default );

						if ( value == DefaultableBoolean.True )
							this.closeButtonLocation = TabCloseButtonLocation.HeaderArea;
						else if ( value == DefaultableBoolean.False )
							this.closeButtonLocation = TabCloseButtonLocation.None;

						break;
					case "ShowPartialTabs":
						this.showPartialTabs = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.showPartialTabs);
						break;
					case "ShowTabListButton":
						this.showTabListButton = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.showTabListButton);
						break;
					case "AutoSelectDelay":
						this.autoSelectDelay = (int)Utils.DeserializeProperty(entry, typeof(int), this.autoSelectDelay);
						break;
					case "InterTabSpacing":
						this.interTabSpacing = (DefaultableInteger)Utils.DeserializeProperty(entry, typeof(DefaultableInteger), this.interTabSpacing);
						break;
					case "TabHeight":
						this.tabHeight = (int)Utils.DeserializeProperty(entry, typeof(int), this.tabHeight);
						break;
					case "TabsPerRow":
						this.tabsPerRow = (int)Utils.DeserializeProperty(entry, typeof(int), this.tabsPerRow);
						break;
					case "MaxTabWidth":
						this.maxTabWidth = (int)Utils.DeserializeProperty(entry, typeof(int), this.maxTabWidth);
						break;
					case "MinTabWidth":
						this.minTabWidth = (int)Utils.DeserializeProperty(entry, typeof(int), this.minTabWidth);
						break;
					case "ScrollTrackExtent":
						this.scrollTrackExtent = (int)Utils.DeserializeProperty(entry, typeof(int), this.scrollTrackExtent);
						break;

					case "ButtonStyle":
						this.buttonStyle = (UIElementButtonStyle)Utils.DeserializeProperty(entry, typeof(UIElementButtonStyle), this.buttonStyle);
						break;
					case "TabButtonStyle":
						this.tabButtonStyle = (UIElementButtonStyle)Utils.DeserializeProperty(entry, typeof(UIElementButtonStyle), this.tabButtonStyle);
						break;

					case "ScrollArrowStyle":
						this.scrollArrowStyle = (ScrollArrowStyle)Utils.DeserializeProperty(entry, typeof(ScrollArrowStyle), this.scrollArrowStyle);
						break;

					case "ScrollButtons":
						this.scrollButtons = (DefaultableTabScrollButtons)Utils.DeserializeProperty(entry, typeof(DefaultableTabScrollButtons), this.scrollButtons);
						break;
					case "TabOrientation":
						this.tabOrientation = (TabOrientation)Utils.DeserializeProperty(entry, typeof(TabOrientation), this.tabOrientation);
						break;
					case "TextOrientation":
						this.textOrientation = (TextOrientation)Utils.DeserializeProperty(entry, typeof(TextOrientation), this.textOrientation);
						break;
					
					case "CloseButtonAppearance":
						this.closeButtonAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.closeButtonAppearanceHolder);
						break;
					case "ScrollButtonAppearance":
						this.scrollButtonAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.scrollButtonAppearanceHolder);
						break;
					// JDN 8/18/04 NAS2004 Vol3
					case "TabListButtonAppearance":
						this.tabListButtonAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.tabListButtonAppearanceHolder);
						break;
					case "TabAreaAppearance":
						this.tabAreaAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.tabAreaAppearanceHolder);
						break;
					case "ScrollTrackAppearance":
						this.scrollTrackAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.scrollTrackAppearanceHolder);
						break;

					case "TabPadding":
						this.tabPadding = (Size)Utils.DeserializeProperty(entry, typeof(Size), this.tabPadding);
						break;

					case "TabSizing":
						this.tabSizing = (TabSizing)Utils.DeserializeProperty(entry, typeof(TabSizing), this.tabSizing);
						break;
					case "TabStyle":
						this.tabStyle = (TabStyle)Utils.DeserializeProperty(entry, typeof(TabStyle), this.tabStyle);
						break;

					case "ScrollButtonTypes":
						this.scrollButtonTypes = (ScrollButtonTypes)Utils.DeserializeProperty(entry, typeof(ScrollButtonTypes), this.scrollButtonTypes);
						break;

					// AS 2/6/04 WTB1314
					case "TabAreaMargins":
						this.tabAreaMargins = (Margins)Utils.DeserializeProperty(entry, typeof(Margins), this.tabAreaMargins);
						break;

					// JAS 12/20/04 Tab Spacing
					case "SpaceAfterTabs":
						this.spaceAfterTabs = (DefaultableInteger)Utils.DeserializeProperty(entry, typeof(DefaultableInteger), this.spaceAfterTabs);
						break;

					case "SpaceBeforeTabs":
						this.spaceBeforeTabs = (DefaultableInteger)Utils.DeserializeProperty(entry, typeof(DefaultableInteger), this.spaceBeforeTabs);
						break;

					// MD 11/6/07 - NA 2008 Vol 1
					case "CloseButtonLocation":
						this.closeButtonLocation = (TabCloseButtonLocation)Utils.DeserializeProperty( entry, typeof( TabCloseButtonLocation ), this.closeButtonLocation );
						break;

					case "HotTrackCloseButtonAppearance":
						this.hotTrackCloseButtonAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof( AppearanceHolder ), this.hotTrackCloseButtonAppearanceHolder );
						break;

					case "PressedCloseButtonAppearance":
						this.pressedCloseButtonAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof( AppearanceHolder ), this.pressedCloseButtonAppearanceHolder );
						break;

					default:
						// AS 4/30/03 FxCop Change
						// Explicitly call the overload that takes an IFormatProvider
						//
						//System.Diagnostics.Debug.Assert(false, string.Format("Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						System.Diagnostics.Debug.Assert(false, string.Format(null, "Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						break;
				}
			}
		}

		#endregion //Constructor

		#region Properties

		#region Manager
		/// <summary>
		/// Returns the associated <b>UltraTabbedMdiManager</b>.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraTabbedMdiManager Manager
		{
			get
			{
				if (this.owner is UltraTabbedMdiManager)
					return this.owner as UltraTabbedMdiManager;

				if (this.owner is MdiTabGroup)
					return ((MdiTabGroup)this.owner).Manager;

				return null;
			}
		}
		#endregion //Manager

		#region CloseButtonAppearance
		/// <summary>
		/// Gets or sets the <see cref="Infragistics.Win.Appearance"/> for the close button.
		/// </summary>
		/// <seealso cref="CloseButtonLocation"/>
		/// <seealso cref="MdiTabSettings.AllowClose"/>
		/// <seealso cref="ButtonStyle"/>
		/// <seealso cref="HotTrackCloseButtonAppearance"/>
		/// <seealso cref="PressedCloseButtonAppearance"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabGroupSettings_P_CloseButtonAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase CloseButtonAppearance
		{
			get
			{
				if (this.closeButtonAppearanceHolder == null)
				{
					this.closeButtonAppearanceHolder = new AppearanceHolder();

					this.closeButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.closeButtonAppearanceHolder.Collection = this.Manager.Appearances;

				return this.closeButtonAppearanceHolder.Appearance;
			}

			set
			{
				if (this.closeButtonAppearanceHolder == null				|| 
					!this.closeButtonAppearanceHolder.HasAppearance		||
					value != this.closeButtonAppearanceHolder.Appearance)
				{
					if (null == this.closeButtonAppearanceHolder)
					{
						this.closeButtonAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.closeButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.closeButtonAppearanceHolder.Collection = this.Manager.Appearances;

					this.closeButtonAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.CloseButtonAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="CloseButtonAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="CloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasCloseButtonAppearance
		{
			get
			{
				return (null != this.closeButtonAppearanceHolder &&
					this.closeButtonAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="CloseButtonAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="CloseButtonAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="CloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeCloseButtonAppearance()
		{
			return this.HasCloseButtonAppearance && this.closeButtonAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="CloseButtonAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="CloseButtonAppearance"/> property. If the <see cref="CloseButtonAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="CloseButtonAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="CloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetCloseButtonAppearance()
		{
			if ( this.HasCloseButtonAppearance )
			{
				this.closeButtonAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.CloseButtonAppearance );
			}
		}
		#endregion CloseButtonAppearance

		#region TabListButtonAppearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> for the tab list button
		/// </summary>
		/// <remarks>
		/// <p class="note">Note: The tab list button is only displayed if the <see cref="ShowTabListButton"/> is resolved to true.
		/// The default value of this property is true only when the ViewStyle is set to VisualStudio2005.</p>
		/// </remarks>
		/// <seealso cref="ShowTabListButton"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabListButtonAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase TabListButtonAppearance
		{
			get
			{
				if (this.tabListButtonAppearanceHolder == null)
				{
					this.tabListButtonAppearanceHolder = new AppearanceHolder();

					this.tabListButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.tabListButtonAppearanceHolder.Collection = this.Manager.Appearances;

				return this.tabListButtonAppearanceHolder.Appearance;
			}

			set
			{
				if (this.tabListButtonAppearanceHolder == null				|| 
					!this.tabListButtonAppearanceHolder.HasAppearance		||
					value != this.tabListButtonAppearanceHolder.Appearance)
				{
					if (null == this.tabListButtonAppearanceHolder)
					{
						this.tabListButtonAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.tabListButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.tabListButtonAppearanceHolder.Collection = this.Manager.Appearances;

					this.tabListButtonAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.TabListButtonAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="TabListButtonAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="TabListButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabListButtonAppearance
		{
			get
			{
				return (null != this.tabListButtonAppearanceHolder &&
					this.tabListButtonAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="TabListButtonAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabListButtonAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="TabListButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeTabListButtonAppearance()
		{
			return this.HasTabListButtonAppearance && this.tabListButtonAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="TabListButtonAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="TabListButtonAppearance"/> property. If the <see cref="TabListButtonAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="TabListButtonAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="TabListButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetTabListButtonAppearance()
		{
			if ( this.HasTabListButtonAppearance )
			{
				this.tabListButtonAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.TabListButtonAppearance );
			}
		}
		#endregion TabListButtonAppearance

		#region ScrollButtonAppearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> for the scroll buttons.
		/// </summary>
		/// <seealso cref="ScrollArrowStyle"/>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="TabSizing"/>
		/// <seealso cref="ButtonStyle"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ScrollButtonAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase ScrollButtonAppearance
		{
			get
			{
				if (this.scrollButtonAppearanceHolder == null)
				{
					this.scrollButtonAppearanceHolder = new AppearanceHolder();

					this.scrollButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.scrollButtonAppearanceHolder.Collection = this.Manager.Appearances;

				return this.scrollButtonAppearanceHolder.Appearance;
			}

			set
			{
				if (this.scrollButtonAppearanceHolder == null				|| 
					!this.scrollButtonAppearanceHolder.HasAppearance		||
					value != this.scrollButtonAppearanceHolder.Appearance)
				{
					if (null == this.scrollButtonAppearanceHolder)
					{
						this.scrollButtonAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.scrollButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.scrollButtonAppearanceHolder.Collection = this.Manager.Appearances;

					this.scrollButtonAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.ScrollButtonAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="ScrollButtonAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="ScrollButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasScrollButtonAppearance
		{
			get
			{
				return (null != this.scrollButtonAppearanceHolder &&
					this.scrollButtonAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="ScrollButtonAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ScrollButtonAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="ScrollButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeScrollButtonAppearance()
		{
			return this.HasScrollButtonAppearance && this.scrollButtonAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="ScrollButtonAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="ScrollButtonAppearance"/> property. If the <see cref="ScrollButtonAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="ScrollButtonAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="ScrollButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetScrollButtonAppearance()
		{
			if ( this.HasScrollButtonAppearance )
			{
				this.scrollButtonAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.ScrollButtonAppearance );
			}
		}
		#endregion ScrollButtonAppearance

		#region ScrollTrackAppearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> for the scroll tracks.
		/// </summary>
		/// <remarks>
		/// <p class="note">Note: The scroll track is only displayed if the <b>Thumb</b> enumeration value is included in 
		/// the <see cref="ScrollButtonTypes"/> enumeration and the scroll buttons are displayed.</p>
		/// </remarks>
		/// <seealso cref="ScrollTrackExtent"/>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="TabSizing"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ScrollTrackAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase ScrollTrackAppearance
		{
			get
			{
				if (this.scrollTrackAppearanceHolder == null)
				{
					this.scrollTrackAppearanceHolder = new AppearanceHolder();

					this.scrollTrackAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.scrollTrackAppearanceHolder.Collection = this.Manager.Appearances;

				return this.scrollTrackAppearanceHolder.Appearance;
			}

			set
			{
				if (this.scrollTrackAppearanceHolder == null				|| 
					!this.scrollTrackAppearanceHolder.HasAppearance		||
					value != this.scrollTrackAppearanceHolder.Appearance)
				{
					if (null == this.scrollTrackAppearanceHolder)
					{
						this.scrollTrackAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.scrollTrackAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.scrollTrackAppearanceHolder.Collection = this.Manager.Appearances;

					this.scrollTrackAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.ScrollTrackAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="ScrollTrackAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="ScrollTrackAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasScrollTrackAppearance
		{
			get
			{
				return (null != this.scrollTrackAppearanceHolder &&
					this.scrollTrackAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="ScrollTrackAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ScrollTrackAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="ScrollTrackAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeScrollTrackAppearance()
		{
			return this.HasScrollTrackAppearance && this.scrollTrackAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="ScrollTrackAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="ScrollTrackAppearance"/> property. If the <see cref="ScrollTrackAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="ScrollTrackAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="ScrollTrackAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetScrollTrackAppearance()
		{
			if ( this.HasScrollTrackAppearance )
			{
				this.scrollTrackAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.ScrollTrackAppearance );
			}
		}
		#endregion ScrollTrackAppearance

		#region TabAreaAppearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> for the area under the tabs.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabAreaAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase TabAreaAppearance
		{
			get
			{
				if (this.tabAreaAppearanceHolder == null)
				{
					this.tabAreaAppearanceHolder = new AppearanceHolder();

					this.tabAreaAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.tabAreaAppearanceHolder.Collection = this.Manager.Appearances;

				return this.tabAreaAppearanceHolder.Appearance;
			}

			set
			{
				if (this.tabAreaAppearanceHolder == null				|| 
					!this.tabAreaAppearanceHolder.HasAppearance		||
					value != this.tabAreaAppearanceHolder.Appearance)
				{
					if (null == this.tabAreaAppearanceHolder)
					{
						this.tabAreaAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.tabAreaAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.tabAreaAppearanceHolder.Collection = this.Manager.Appearances;

					this.tabAreaAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.TabAreaAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="TabAreaAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="TabAreaAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabAreaAppearance
		{
			get
			{
				return (null != this.tabAreaAppearanceHolder &&
					this.tabAreaAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="TabAreaAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabAreaAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="TabAreaAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeTabAreaAppearance()
		{
			return this.HasTabAreaAppearance && this.tabAreaAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="TabAreaAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="TabAreaAppearance"/> property. If the <see cref="TabAreaAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="TabAreaAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="TabAreaAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetTabAreaAppearance()
		{
			if ( this.HasTabAreaAppearance )
			{
				this.tabAreaAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.TabAreaAppearance );
			}
		}
		#endregion TabAreaAppearance

		#region AllowDrop
		/// <summary>
		/// Returns or sets whether an <see cref="MdiTab"/> from a different <b>MdiTabGroup</b> may be repositioned to the <see cref="MdiTabGroup"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, tabs from any <see cref="MdiTabGroup"/> may be repositioned within their current <see cref="MdiTab.TabGroup"/> or 
		/// in other tab groups. The <see cref="MdiTabSettings.AllowDrag"/> property is used to limit where a tab may be dragged to or whether an MdiTab may even be dragged. The 
		/// <b>AllowDrop</b> property can be used to limit which tabs may be moved into a particular tab group. Setting this property to false will prevent tabs from 
		/// other MdiTabGroup instances from being added to the tab group's <see cref="MdiTabGroup.Tabs"/> collection.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDragging"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDragOver"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDropped"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.AllowDrop"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_AllowDrop")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean AllowDrop
		{
			get { return this.allowDrop; }
			set
			{
				if (this.allowDrop == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.allowDrop = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.AllowDrop );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AllowDrop"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AllowDrop"/> differs from the default value.</returns>
		/// <seealso cref="AllowDrop"/>
		public bool ShouldSerializeAllowDrop()
		{
			return this.AllowDrop != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="AllowDrop"/> property to its default value.
		/// </summary>
		/// <seealso cref="AllowDrop"/>
		public void ResetAllowDrop()
		{
			this.AllowDrop = DefaultableBoolean.Default;
		}
		#endregion //AllowDrop

		#region AutoSelect
		/// <summary>
		/// Returns or sets whether the <see cref="MdiTab"/> will be automatically selected after hovering over the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, <b>AutoSelect</b> resolves to false. When set to true and the mouse is within the bounds of a 
		/// tab for the <see cref="AutoSelectDelay"/> interval, the tab is automatically selected.</p>
		/// </remarks>
		/// <seealso cref="AutoSelectDelay"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.AutoSelect"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_AutoSelect")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean AutoSelect
		{
			get { return this.autoSelect; }
			set
			{
				if (this.autoSelect == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.autoSelect = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.AutoSelect );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AutoSelect"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AutoSelect"/> differs from the default value.</returns>
		/// <seealso cref="AutoSelect"/>
		public bool ShouldSerializeAutoSelect()
		{
			return this.AutoSelect != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="AutoSelect"/> property to its default value.
		/// </summary>
		/// <seealso cref="AutoSelect"/>
		public void ResetAutoSelect()
		{
			this.AutoSelect = DefaultableBoolean.Default;
		}
		#endregion //AutoSelect

		#region ShowButtonSeparators
		/// <summary>
		/// Returns or sets whether separators will be displayed between button style tabs.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <see cref="TabStyle"/> is resolved to <b>StateButtons</b>, the tabs 
		/// are displayed as state buttons where the selected tab is in the pressed state. By default, there 
		/// is some space between the state button tabs which can be controlled using the <see cref="InterTabSpacing"/>. 
		/// As long as there are a few pixels of space between the state button tabs, a perpendicular line separating 
		/// the tabs will be displayed when this property is set to true (the default value). This can be helpful when the 
		/// <see cref="TabButtonStyle"/> is set to a value that displays the unselected tabs as border less state buttons.</p>
		/// </remarks>
		/// <seealso cref="TabStyle"/>
		/// <seealso cref="TabButtonStyle"/>
		/// <seealso cref="InterTabSpacing"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.ShowButtonSeparators"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ShowButtonSeparators")]
		[LocalizedCategory("LC_Appearance")]
		public DefaultableBoolean ShowButtonSeparators
		{
			get { return this.showButtonSeparators; }
			set
			{
				if (this.showButtonSeparators == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.showButtonSeparators = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ShowButtonSeparators );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ShowButtonSeparators"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ShowButtonSeparators"/> differs from the default value.</returns>
		/// <seealso cref="ShowButtonSeparators"/>
		public bool ShouldSerializeShowButtonSeparators()
		{
			return this.ShowButtonSeparators != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="ShowButtonSeparators"/> property to its default value.
		/// </summary>
		/// <seealso cref="ShowButtonSeparators"/>
		public void ResetShowButtonSeparators()
		{
			this.ShowButtonSeparators = DefaultableBoolean.Default;
		}
		#endregion //ShowButtonSeparators

		#region ShowCloseButton
		/// <summary>
		/// Returns or sets whether a close button will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ShowCloseButton</b> determines whether the close button (displayed as a button with an "X") is displayed in the 
		/// <see cref="MdiTabGroup"/> area. When set to true, the default value, the close button is enabled and disabled based on the <see cref="MdiTabSettings.AllowClose"/> 
		/// state of the selected tab. When clicked, the <see cref="UltraTabbedMdiManager.TabClosing"/> event is invoked, which may be cancelled preventing any 
		/// further action. If the event is not cancelled, the action taken on the associated <see cref="MdiTab.Form"/> is based on the 
		/// <see cref="MdiTabSettings.TabCloseAction"/>. Then the <see cref="UltraTabbedMdiManager.TabClosed"/> event is invoked.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettingsResolved.ShowCloseButton"/>
		/// <seealso cref="MdiTabSettings.TabCloseAction"/>
		/// <seealso cref="MdiTabSettings.AllowClose"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosing"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosed"/>
		/// <seealso cref="CloseButtonAppearance"/>
		/// <seealso cref="ButtonStyle"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ShowCloseButton")]
		[LocalizedCategory("LC_Appearance")]
		// MD 11/7/07 - NA 2008 Vol 1
		// Made obsolete
		[Obsolete( "ShowCloseButton has been deprected, use the CloseButtonLocation property instead" )]
		[Browsable( false )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		[EditorBrowsable( EditorBrowsableState.Never )]
		public DefaultableBoolean ShowCloseButton
		{	
			// MD 11/7/07 - NA 2008 Vol 1
			// There is no showCloseButton member anymore
			//get { return this.showCloseButton; }
			get 
			{
				if ( this.CloseButtonLocation == TabCloseButtonLocation.None )
					return DefaultableBoolean.False;
				
				if ( this.CloseButtonLocation == TabCloseButtonLocation.Default )
					return DefaultableBoolean.Default;

				return DefaultableBoolean.True;
			}
			set
			{
				// MD 11/7/07 - NA 2008 Vol 1
				// There is no showCloseButton member anymore
				//if (this.showCloseButton == value)
				if ( this.ShowCloseButton == value )
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				// MD 11/7/07 - NA 2008 Vol 1
				// There is no showCloseButton member anymore
				//this.showCloseButton = value;
				if ( value == DefaultableBoolean.True )
					this.closeButtonLocation = TabCloseButtonLocation.HeaderArea;
				else if ( value == DefaultableBoolean.False )
					this.closeButtonLocation = TabCloseButtonLocation.None;
				else
					this.closeButtonLocation = TabCloseButtonLocation.Default;

				this.NotifyPropChange( TabbedMdiPropertyIds.ShowCloseButton );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ShowCloseButton"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ShowCloseButton"/> differs from the default value.</returns>
		/// <seealso cref="ShowCloseButton"/>
		// MD 11/7/07 - NA 2008 Vol 1
		// Made obsolete
		[Obsolete( "ShowCloseButton has been deprected, use the CloseButtonLocation property instead" )]
		[EditorBrowsable( EditorBrowsableState.Never )]
		public bool ShouldSerializeShowCloseButton()
		{
			return this.ShowCloseButton != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="ShowCloseButton"/> property to its default value.
		/// </summary>
		/// <seealso cref="ShowCloseButton"/>
		// MD 11/7/07 - NA 2008 Vol 1
		// Made obsolete
		[Obsolete( "ShowCloseButton has been deprected, use the CloseButtonLocation property instead" )]
		[EditorBrowsable( EditorBrowsableState.Never )]
		public void ResetShowCloseButton()
		{
			this.ShowCloseButton = DefaultableBoolean.Default;
		}
		#endregion //ShowCloseButton

		#region ShowPartialTabs
		/// <summary>
		/// Determines whether partial tabs will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ShowPartialTabs</b> property is used to specify whether or not to display an
		///  <see cref="MdiTab"/> if there is insufficent room to display it fully. Setting this property to false
		///  will prevent partial tabs from from being displayed.  The default value is true unless the ViewStyle
		///  is set to VisualStudio2005.</p>
		/// </remarks>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ShowPartialTabs")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean ShowPartialTabs
		{
			get { return this.showPartialTabs; }
			set
			{
				if (this.showPartialTabs == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.showPartialTabs = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ShowPartialTabs );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ShowPartialTabs"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ShowPartialTabs"/> differs from the default value.</returns>
		public bool ShouldSerializeShowPartialTabs()
		{
			return this.ShowPartialTabs != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="ShowPartialTabs"/> property to its default value.
		/// </summary>
		public void ResetShowPartialTabs()
		{
			this.ShowPartialTabs = DefaultableBoolean.Default;
		}		

		#endregion ShowPartialTabs

		#region ShowTabListButton
		// JDN 8/17/04 NAS2004 Vol3
		/// <summary>
		/// Determines whether the tab list button will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ShowTabListButton</b> property is used to specify whether or not to display
		///  the tab list button. Setting this property to false will prevent the button from being displayed.
		///  The default value is false unless the ViewStyle is set to VisualStudio2005.</p>
		/// </remarks>
		/// /// <seealso cref="MdiTabGroupSettingsResolved.ShowTabListButton"/>
		/// <seealso cref="TabListButtonAppearance"/>
		/// <seealso cref="ButtonStyle"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ShowTabListButton")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean ShowTabListButton
		{
			get { return this.showTabListButton; }
			set
			{
				if (this.showTabListButton == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.showTabListButton = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ShowTabListButton );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ShowTabListButton"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ShowTabListButton"/> differs from the default value.</returns>
		public bool ShouldSerializeShowTabListButton()
		{
			return this.ShowTabListButton != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="ShowTabListButton"/> property to its default value.
		/// </summary>
		public void ResetShowTabListButton()
		{
			this.ShowTabListButton = DefaultableBoolean.Default;
		}		

		#endregion ShowTabListButton

		#region AutoSelectDelay
		/// <summary>
		/// Returns or sets how long the mouse must hover over a tab before it is selected when <see cref="AutoSelect"/> is enabled.
		/// </summary>
		/// <remarks>
		/// <p class="body">When <see cref="AutoSelect"/> is set to true, the <b>AutoSelectDelay</b> determines the interval that the mouse 
		/// must be over the tab before it is automatically selected. By default, <b>AutoSelect</b> is not enabled.</p>
		/// </remarks>
		/// <seealso cref="AutoSelect"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.AutoSelectDelay"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_AutoSelectDelay")]
		[LocalizedCategory("LC_Behavior")]
		public int AutoSelectDelay
		{
			get { return this.autoSelectDelay; }
			set
			{
				if (this.autoSelectDelay == value)
					return;

				if (value < -1)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_18"));

				this.autoSelectDelay = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.AutoSelectDelay );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AutoSelectDelay"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AutoSelectDelay"/> differs from the default value.</returns>
		/// <seealso cref="AutoSelectDelay"/>
		public bool ShouldSerializeAutoSelectDelay()
		{
			return this.AutoSelectDelay != AutoSelectDelayDefault;
		}	

		/// <summary>
		/// Resets the <see cref="AutoSelectDelay"/> property to its default value.
		/// </summary>
		/// <seealso cref="AutoSelectDelay"/>
		public void ResetAutoSelectDelay()
		{
			this.AutoSelectDelay = AutoSelectDelayDefault;
		}
		#endregion //AutoSelectDelay

		#region InterTabSpacing
		/// <summary>
		/// Returns or sets the amount of space between tabs.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>InterTabSpacing</b> determines the amount of space between the tab items. When set to a negative value, the 
		/// tab items overlap. When set to a positive value, the specified amount of pixels is left between the tab items. When left at the default value,
		/// the space between the tabs is resolved differently based on other settings, such as the <see cref="TabStyle"/>. For example, the default 
		/// <b>InterTabSpacing</b> for <b>Excel</b> style tabs is -8 to allow the tab items to overlap as they do when using Microsoft Excel.</p>
		/// <p class="note">Note: If the <see cref="InterTabSpacing"/> is less than 0, then all tab items must be at least the absolute value of the <b>InterTabSpacing</b>.</p>
		/// </remarks>
		/// <seealso cref="TabStyle"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.InterTabSpacing"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_InterTabSpacing")]
		[LocalizedCategory("LC_Appearance")]
		public DefaultableInteger InterTabSpacing
		{
			get { return this.interTabSpacing; }
			set
			{
				if (this.interTabSpacing == value)
					return;

				this.interTabSpacing = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.InterTabSpacing );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="InterTabSpacing"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="InterTabSpacing"/> differs from the default value.</returns>
		/// <seealso cref="InterTabSpacing"/>
		public bool ShouldSerializeInterTabSpacing()
		{
			return !this.InterTabSpacing.IsDefault;
		}	

		/// <summary>
		/// Resets the <see cref="InterTabSpacing"/> property to its default value.
		/// </summary>
		/// <seealso cref="InterTabSpacing"/>
		public void ResetInterTabSpacing()
		{
			this.InterTabSpacing = DefaultableInteger.Default;
		}
		#endregion //InterTabSpacing

		#region TabHeight
		/// <summary>
		/// Returns or sets the height for the <see cref="MdiTab"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabHeight</b> controls the logical height of the tab items. By default, the 
		/// TabHeight is based on the size needed to show the image and text completely within the borders for the tab item, which is affected 
		/// by many property settings (e.g. <see cref="TabStyle"/>, <see cref="TextOrientation"/>, etc). For tabs displayed on the top or bottom, 
		/// the TabHeight affects the displayed height of the control while it affects the displayed width of the tabs when the 
		/// <see cref="TabOrientation"/> is such that the tabs are displayed on the left or right.</p>
		/// </remarks>
		/// <seealso cref="TabOrientation"/>
		/// <seealso cref="TextOrientation"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.TabHeight"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabHeight")]
		[LocalizedCategory("LC_Appearance")]
		public int TabHeight
		{
			get { return this.tabHeight; }
			set
			{
				if (this.tabHeight == value)
					return;

				if (value < TabHeightDefault)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_19"));

				this.tabHeight = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabHeight );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabHeight"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabHeight"/> differs from the default value.</returns>
		/// <seealso cref="TabHeight"/>
		public bool ShouldSerializeTabHeight()
		{
			return this.TabHeight != TabHeightDefault;
		}	

		/// <summary>
		/// Resets the <see cref="TabHeight"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabHeight"/>
		public void ResetTabHeight()
		{
			this.TabHeight = TabHeightDefault;
		}
		#endregion //TabHeight

		#region MaxTabWidth
		/// <summary>
		/// Returns or sets the maximum width for the <see cref="MdiTab"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MinTabWidth</b> and <b>MaxTabWidth</b> properties determine the minimum and maximum extents of the tab 
		/// items respectively. By default, the minimum tab width is based on the width required for the borders and there is no maximum 
		/// tab width.</p>
		/// <p class="note">Note: If the <see cref="InterTabSpacing"/> is less than 0, then all tab items must be at least the absolute value of the <b>InterTabSpacing</b>.</p>
		/// </remarks>
		/// <seealso cref="MinTabWidth"/>
		/// <seealso cref="MdiTabSettings.TabWidth"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.MaxTabWidth"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_MaxTabWidth")]
		[LocalizedCategory("LC_Appearance")]
		public int MaxTabWidth
		{
			get { return this.maxTabWidth; }
			set
			{
				if (this.maxTabWidth == value)
					return;

				if (value < MaxTabWidthDefault)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_20"));

				this.maxTabWidth = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.MaxTabWidth );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="MaxTabWidth"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="MaxTabWidth"/> differs from the default value.</returns>
		/// <seealso cref="MaxTabWidth"/>
		public bool ShouldSerializeMaxTabWidth()
		{
			return this.MaxTabWidth != MaxTabWidthDefault;
		}	

		/// <summary>
		/// Resets the <see cref="MaxTabWidth"/> property to its default value.
		/// </summary>
		/// <seealso cref="MaxTabWidth"/>
		public void ResetMaxTabWidth()
		{
			this.MaxTabWidth = MaxTabWidthDefault;
		}
		#endregion //MaxTabWidth

		#region MinTabWidth
		/// <summary>
		/// Returns or sets the minimum width for the <see cref="MdiTab"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MinTabWidth</b> and <b>MaxTabWidth</b> properties determine the minimum and maximum extents of the tab 
		/// items respectively. By default, the minimum tab width is based on the width required for the borders and there is no maximum 
		/// tab width.</p>
		/// <p class="note">Note: If the <see cref="InterTabSpacing"/> is less than 0, then all tab items must be at least the absolute value of the <b>InterTabSpacing</b>.</p>
		/// </remarks>
		/// <seealso cref="MaxTabWidth"/>
		/// <seealso cref="MdiTabSettings.TabWidth"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.MinTabWidth"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_MinTabWidth")]
		[LocalizedCategory("LC_Appearance")]
		public int MinTabWidth
		{
			get { return this.minTabWidth; }
			set
			{
				if (this.minTabWidth == value)
					return;

				if (value < MinTabWidthDefault)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_21"));

				this.minTabWidth = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.MinTabWidth );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="MinTabWidth"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="MinTabWidth"/> differs from the default value.</returns>
		/// <seealso cref="MinTabWidth"/>
		public bool ShouldSerializeMinTabWidth()
		{
			return this.MinTabWidth != MinTabWidthDefault;
		}	

		/// <summary>
		/// Resets the <see cref="MinTabWidth"/> property to its default value.
		/// </summary>
		/// <seealso cref="MinTabWidth"/>
		public void ResetMinTabWidth()
		{
			this.MinTabWidth = MinTabWidthDefault;
		}
		#endregion //MinTabWidth

		#region TabsPerRow
		/// <summary>
		/// Returns or sets the number of <see cref="MdiTab"/> instances visible at a time when the 'TabStyle' is set to TabsPerRow.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabsPerRow</b> property is only used when the <see cref="TabStyle"/> resolves to <b>TabsPerRow</b>. When that 
		/// style is used, this property determines the number of fully visible tabs that will equally share the available tab area. For example, 
		/// if there is 100 pixels of available area and <b>TabsPerRow</b> is set to 4, each tab will have a logical width of 25 pixels.</p>
		/// <p class="note"><b>Note:</b> A value of 0 will be treated the same as -1 indicating that the value has not been set.</p>
		/// </remarks>
		/// <seealso cref="TabStyle"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.TabsPerRow"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabsPerRow")]
		[LocalizedCategory("LC_Appearance")]
		public int TabsPerRow
		{
			get { return this.tabsPerRow; }
			set
			{
				if (this.tabsPerRow == value)
					return;

				if (value < -1)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_22"));

				this.tabsPerRow = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabsPerRow );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabsPerRow"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabsPerRow"/> differs from the default value.</returns>
		/// <seealso cref="TabsPerRow"/>
		public bool ShouldSerializeTabsPerRow()
		{
			return this.TabsPerRow != TabsPerRowDefault;
		}	

		/// <summary>
		/// Resets the <see cref="TabsPerRow"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabsPerRow"/>
		public void ResetTabsPerRow()
		{
			this.TabsPerRow = TabsPerRowDefault;
		}
		#endregion //TabsPerRow

		#region ButtonStyle
		/// <summary>
		/// Returns or sets the style of button used to display the scroll and close buttons.
		/// </summary>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="ShowCloseButton"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.ButtonStyle"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ButtonStyle")]
		[LocalizedCategory("LC_Appearance")]
		public UIElementButtonStyle ButtonStyle
		{
			get { return this.buttonStyle; }
			set
			{
				if (this.buttonStyle == value)
					return;

				if (!Enum.IsDefined(typeof(UIElementButtonStyle), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(UIElementButtonStyle));

				this.buttonStyle = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ButtonStyle );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ButtonStyle"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ButtonStyle"/> differs from the default value.</returns>
		/// <seealso cref="ButtonStyle"/>
		public bool ShouldSerializeButtonStyle()
		{
			return this.ButtonStyle != UIElementButtonStyle.Default;
		}	

		/// <summary>
		/// Resets the <see cref="ButtonStyle"/> property to its default value.
		/// </summary>
		/// <seealso cref="ButtonStyle"/>
		public void ResetButtonStyle()
		{
			this.ButtonStyle = UIElementButtonStyle.Default;
		}
		#endregion //ButtonStyle

		#region TabButtonStyle
		/// <summary>
		/// Returns or sets the style of button used to display a state button style tab item.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <see cref="TabStyle"/> is resolved to <b>StateButtons</b>, the tabs 
		/// are displayed as state buttons where the selected tab is in the pressed state. The <b>TabButtonStyle</b> 
		/// determines the style of buttons displayed including the tab border style.</p>
		/// </remarks>
		/// <seealso cref="TabStyle"/>
		/// <seealso cref="ShowButtonSeparators"/>
		/// <seealso cref="InterTabSpacing"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.TabButtonStyle"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabButtonStyle")]
		[LocalizedCategory("LC_Appearance")]
		public UIElementButtonStyle TabButtonStyle
		{
			get { return this.tabButtonStyle; }
			set
			{
				if (this.tabButtonStyle == value)
					return;

				if (!Enum.IsDefined(typeof(UIElementButtonStyle), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(UIElementButtonStyle));

				this.tabButtonStyle = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabButtonStyle );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabButtonStyle"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabButtonStyle"/> differs from the default value.</returns>
		/// <seealso cref="TabButtonStyle"/>
		public bool ShouldSerializeTabButtonStyle()
		{
			return this.TabButtonStyle != UIElementButtonStyle.Default;
		}	

		/// <summary>
		/// Resets the <see cref="TabButtonStyle"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabButtonStyle"/>
		public void ResetTabButtonStyle()
		{
			this.TabButtonStyle = UIElementButtonStyle.Default;
		}
		#endregion //TabButtonStyle

		#region ScrollArrowStyle
		/// <summary>
		/// Returns or sets the style of scroll buttons displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ScrollArrowStyle</b> determines the display of the arrows in the first/last, next/previous and next 
		/// page / previous page buttons. The <see cref="ScrollButtonTypes"/> property is used to determine which scroll buttons should be displayed. 
		/// The <see cref="ScrollButtons"/> property determines when scroll buttons should be displayed. If scroll buttons are set to always be displayed, 
		/// they will appear disabled when no scrolling can occur. The <b>WindowsXP</b> style uses the Windows Xp style chevrons seen in the spin and scroll buttons. 
		/// The <b>Standard</b> and <b>VisualStudio</b> settings are displayed as triangles indicating the direction of the scroll. The difference being that 
		/// <b>VisualStudio</b> style scroll arrows are not filled when they are disabled as they appear in Visual Studio .Net.</p>
		/// </remarks>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="ButtonStyle"/>
		/// <seealso cref="ScrollButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.ScrollArrowStyle"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ScrollArrowStyle")]
		[LocalizedCategory("LC_Appearance")]
		public ScrollArrowStyle ScrollArrowStyle
		{
			get { return this.scrollArrowStyle; }
			set
			{
				if (this.scrollArrowStyle == value)
					return;

				if (!Enum.IsDefined(typeof(ScrollArrowStyle), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(ScrollArrowStyle));

				this.scrollArrowStyle = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ScrollArrowStyle );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ScrollArrowStyle"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ScrollArrowStyle"/> differs from the default value.</returns>
		/// <seealso cref="ScrollArrowStyle"/>
		public bool ShouldSerializeScrollArrowStyle()
		{
			return this.ScrollArrowStyle != ScrollArrowStyle.Default;
		}	

		/// <summary>
		/// Resets the <see cref="ScrollArrowStyle"/> property to its default value.
		/// </summary>
		/// <seealso cref="ScrollArrowStyle"/>
		public void ResetScrollArrowStyle()
		{
			this.ScrollArrowStyle = ScrollArrowStyle.Default;
		}
		#endregion //ScrollArrowStyle

		#region ScrollButtons
		/// <summary>
		/// Returns or sets whether scroll buttons will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ScrollButtons</b> property is used to determine if and when scroll 
		/// buttons should be displayed. By default, scroll buttons are only displayed when there isn't 
		/// enough room to display all the tab items. The size of the tab items is based on multiple factors 
		/// including the <see cref="MdiTabSettings.TabWidth"/>, <see cref="TabSizing"/>, <see cref="MinTabWidth"/> and 
		/// <see cref="MaxTabWidth"/> properties.</p>
		/// </remarks>
		/// <seealso cref="ScrollArrowStyle"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="ButtonStyle"/>
		/// <seealso cref="ScrollButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.ScrollButtons"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ScrollButtons")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableTabScrollButtons ScrollButtons
		{
			get { return this.scrollButtons; }
			set
			{
				if (this.scrollButtons == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableTabScrollButtons), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableTabScrollButtons));

				this.scrollButtons = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ScrollButtons );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ScrollButtons"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ScrollButtons"/> differs from the default value.</returns>
		/// <seealso cref="ScrollButtons"/>
		public bool ShouldSerializeScrollButtons()
		{
			return this.ScrollButtons != DefaultableTabScrollButtons.Default;
		}	

		/// <summary>
		/// Resets the <see cref="ScrollButtons"/> property to its default value.
		/// </summary>
		/// <seealso cref="ScrollButtons"/>
		public void ResetScrollButtons()
		{
			this.ScrollButtons = DefaultableTabScrollButtons.Default;
		}
		#endregion //ScrollButtons

		#region ScrollButtonTypes
		/// <summary>
		/// Returns or sets the types of scroll buttons to display.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>ScrollButtonTypes</b> is a flag type enumeration (multiple values may be combined using an OR operator) that determines which scroll buttons are displayed. When the scroll buttons are displayed is based 
		/// on the <see cref="ScrollButtons"/> property.</p>
		/// <list type="table">
		/// <item><term>Default</term><description>The value is not set and will be resolved</description></item>
		/// <item><term>None</term><description>No scroll buttons will be displayed</description></item>
		/// <item><term>NextPrevious</term><description>Buttons to navigate to the next and previous item will be displayed</description></item>
		/// <item><term>NextPagePreviousPage</term><description>Buttons to scroll forward and backward by one page will be displayed</description></item>
		/// <item><term>FirstLast</term><description>Buttons to scroll the first and last item into view will be displayed</description></item>
		/// <item><term>Thumb</term><description>A scroll track and scroll thumb are displayed. This is similar to a scroll bar without any scroll buttons.</description></item>
		/// </list>
		/// </remarks>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollArrowStyle"/>
		/// <seealso cref="ButtonStyle"/>
		/// <seealso cref="ScrollButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.ScrollButtonTypes"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ScrollButtonTypes")]
		[LocalizedCategory("LC_Behavior")]
		[Editor(typeof(Infragistics.Win.UltraWinTabs.Design.ScrollButtonTypesUITypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public ScrollButtonTypes ScrollButtonTypes
		{
			get { return this.scrollButtonTypes; }
			set
			{
				if (this.scrollButtonTypes == value)
					return;

				this.scrollButtonTypes = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ScrollButtonTypes );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ScrollButtonTypes"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ScrollButtonTypes"/> differs from the default value.</returns>
		/// <seealso cref="ScrollButtonTypes"/>
		public bool ShouldSerializeScrollButtonTypes()
		{
			return this.scrollButtonTypes != ScrollButtonTypes.Default;
		}	

		/// <summary>
		/// Resets the <see cref="ScrollButtonTypes"/> property to its default value.
		/// </summary>
		/// <seealso cref="ScrollButtonTypes"/>
		public void ResetScrollButtonTypes()
		{
			this.ScrollButtonTypes = ScrollButtonTypes.Default;
		}
		#endregion //ScrollButtonTypes

		#region TabOrientation
		/// <summary>
		/// Returns or sets the orientation of the tabs in the <see cref="MdiTabGroup"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabOrientation</b> property determines the alignment and order in which the tabs are layed out. By default, the value 
		/// resolves to TopLeft which means that the tabs are aligned on the top and are laid out from left to right.</p>
		/// </remarks>
		/// <seealso cref="TextOrientation"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.TabOrientation"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabOrientation")]
		[LocalizedCategory("LC_Behavior")]
		public TabOrientation TabOrientation
		{
			get { return this.tabOrientation; }
			set
			{
				if (this.tabOrientation == value)
					return;

				if (!Enum.IsDefined(typeof(TabOrientation), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(TabOrientation));

				this.tabOrientation = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabOrientation );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabOrientation"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabOrientation"/> differs from the default value.</returns>
		/// <seealso cref="TabOrientation"/>
		public bool ShouldSerializeTabOrientation()
		{
			return this.TabOrientation != TabOrientation.Default;
		}	

		/// <summary>
		/// Resets the <see cref="TabOrientation"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabOrientation"/>
		public void ResetTabOrientation()
		{
			this.TabOrientation = TabOrientation.Default;
		}
		#endregion //TabOrientation

		#region TextOrientation
		/// <summary>
		/// Returns or sets how the tab text is aligned.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TextOrientation</b> determines the orientation of the text in the tab. The orientation may be specified 
		/// relative to the <see cref="TabOrientation"/> or to an absolute value. By default, the text orientation is resolved based on 
		/// the resolved <b>TabOrientation</b> to keep the text parallel with the tab orientation.</p>
		/// </remarks>
		/// <seealso cref="TabOrientation"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.TextOrientation"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TextOrientation")]
		[LocalizedCategory("LC_Behavior")]
		public TextOrientation TextOrientation
		{
			get { return this.textOrientation; }
			set
			{
				if (this.textOrientation == value)
					return;

				if (!Enum.IsDefined(typeof(TextOrientation), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(TextOrientation));

				this.textOrientation = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TextOrientation );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TextOrientation"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TextOrientation"/> differs from the default value.</returns>
		/// <seealso cref="TextOrientation"/>
		public bool ShouldSerializeTextOrientation()
		{
			return this.TextOrientation != TextOrientation.Default;
		}	

		/// <summary>
		/// Resets the <see cref="TextOrientation"/> property to its default value.
		/// </summary>
		/// <seealso cref="TextOrientation"/>
		public void ResetTextOrientation()
		{
			this.TextOrientation = TextOrientation.Default;
		}
		#endregion //TextOrientation

		#region TabPadding
		/// <summary>
		/// Returns or sets the amount of padding around the image and text of the <see cref="MdiTab"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>TabPadding</b> is the amount of space between the contents of the tab (the image and text) and the borders of the tab.</p>
		/// <p class="body">The Width and Height may be set on different levels. In other words, you may set the Width of the <b>TabPadding</b> of the 
		/// <see cref="MdiTabGroup.Settings"/> to 3 but leave the Height set to -1 and the Height of the <see cref="UltraTabbedMdiManager.TabGroupSettings"/> 
		/// <b>TabPadding</b> will be used if specified.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettingsResolved.TabPadding"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabPadding")]
		[LocalizedCategory("LC_Appearance")]
		public Size TabPadding
		{
			get { return this.tabPadding; }
			set
			{
				if (this.tabPadding == value)
					return;

				if (value.Width < -1 || value.Height < -1)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, "The 'TabPadding' must be the default value (-1,-1) or greater.");

				this.tabPadding = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabPadding );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabPadding"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabPadding"/> differs from the default value.</returns>
		/// <seealso cref="TabPadding"/>
		public bool ShouldSerializeTabPadding()
		{
			return this.TabPadding != TabPaddingDefault;
		}	

		/// <summary>
		/// Resets the <see cref="TabPadding"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabPadding"/>
		public void ResetTabPadding()
		{
			this.TabPadding = TabPaddingDefault;
		}
		#endregion //TabPadding

		#region TabSizing
		/// <summary>
		/// Returns or sets the sizing mode used to display the tab items.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>TabSizing</b> determines how the size of the tab items are calculated. </p>	
		/// <list type="table">
		/// <item><term>AutoSize</term><description>The tab size is based on the size required to show the image and text.</description></item>
		/// <item><term>Fixed</term><description>The tab size is based on the <see cref="MdiTabSettings.TabWidth"/> if resolved to a value, 
		/// otherwise the <b>AutoSize</b> value is used.</description></item>
		/// <item><term>Justified</term><description>This sizing mode only affects the tabs when there is not enough room to display all the tab items. Each tab item's size is reduced so that all the tab items are in view.</description></item>
		/// <item><term>SizeToFit</term><description>Increases or decreases the size of the tab so that they occupy the entire available area.</description></item>
		/// <item><term>Compress</term><description>The non selected tabs are sized to just enough space to display the associated image. The selected tab is sized based on the largest image and text area.</description></item>
		/// <item><term>TabsPerRow</term><description>The size of each tab is a percentage of the available area based on the <see cref="TabsPerRow"/> setting.</description></item>
		/// </list>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettingsResolved.TabSizing"/>
		/// <seealso cref="TabsPerRow"/>
		/// <seealso cref="MdiTabSettings.TabWidth"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabSizing")]
		[LocalizedCategory("LC_Appearance")]
		public TabSizing TabSizing
		{
			get { return this.tabSizing; }
			set
			{
				if (this.tabSizing == value)
					return;

				if (!Enum.IsDefined(typeof(TabSizing), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(TabSizing));

				this.tabSizing = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabSizing );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabSizing"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabSizing"/> differs from the default value.</returns>
		/// <seealso cref="TabSizing"/>
		public bool ShouldSerializeTabSizing()
		{
			return this.TabSizing != TabSizing.Default;
		}	

		/// <summary>
		/// Resets the <see cref="TabSizing"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabSizing"/>
		public void ResetTabSizing()
		{
			this.TabSizing = TabSizing.Default;
		}
		#endregion //TabSizing

		#region TabStyle
		/// <summary>
		/// Returns or sets the style of tabs displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">Determines the display style for the tab items. The default resolved value is VisualStudio so that the tabs appear as 
		/// the mdi tabs appear in Visual Studio .Net.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettingsResolved.TabStyle"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabStyle")]
		[LocalizedCategory("LC_Appearance")]
		public TabStyle TabStyle
		{
			get { return this.tabStyle; }
			set
			{
				if (this.tabStyle == value)
					return;

				if (!Enum.IsDefined(typeof(TabStyle), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(TabStyle));

				this.tabStyle = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabStyle );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabStyle"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabStyle"/> differs from the default value.</returns>
		/// <seealso cref="TabStyle"/>
		public bool ShouldSerializeTabStyle()
		{
			return this.TabStyle != TabStyle.Default;
		}	

		/// <summary>
		/// Resets the <see cref="TabStyle"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabStyle"/>
		public void ResetTabStyle()
		{
			this.TabStyle = TabStyle.Default;
		}
		#endregion //TabStyle

		#region ScrollTrackExtent
		/// <summary>
		/// Returns or sets the extent for the scroll track.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>ScrollTrackExtent</b> is the width of the scroll track used to navigate between the tab items. The scroll track 
		/// is only displayed when the <b>Thumb</b> enum is included in the <see cref="ScrollButtonTypes"/> property. The <see cref="ScrollButtons"/> 
		/// property is used to determine when the scroll buttons should be displayed. By default, they are only displayed 
		/// when there isn't enough room to display all the tab items.</p>
		/// </remarks>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="ScrollTrackAppearance"/>
		/// <seealso cref="MdiTabGroupSettingsResolved.ScrollTrackExtent"/>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_ScrollTrackExtent")]
		[LocalizedCategory("LC_Appearance")]
		public int ScrollTrackExtent
		{
			get { return this.scrollTrackExtent; }
			set
			{
				if (this.scrollTrackExtent == value)
					return;

				if (value < ScrollTrackExtentDefault)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_23"));

				this.scrollTrackExtent = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ScrollTrackExtent );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ScrollTrackExtent"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ScrollTrackExtent"/> differs from the default value.</returns>
		/// <seealso cref="ScrollTrackExtent"/>
		public bool ShouldSerializeScrollTrackExtent()
		{
			return this.ScrollTrackExtent != ScrollTrackExtentDefault;
		}	

		/// <summary>
		/// Resets the <see cref="ScrollTrackExtent"/> property to its default value.
		/// </summary>
		/// <seealso cref="ScrollTrackExtent"/>
		public void ResetScrollTrackExtent()
		{
			this.ScrollTrackExtent = ScrollTrackExtentDefault;
		}
		#endregion //ScrollTrackExtent
	
		// JJD 5/19/03 - WTB947
		// Added ForceSerialization as a workaround for a serialization
		// bug MS introdiced in V1.1 of Visual Studio
		#region ForceSerialization

		/// <summary>
		/// Internal property.
		/// </summary>
		/// <remarks>
		/// This property is used internally as a workaround for a serialization bug in Visual Studio that was introduced in version 1.1.
		/// </remarks>
		[ Browsable(false) ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		[ DefaultValue(false) ]
		public bool ForceSerialization
		{
			get	
			{ 
				// JJD 5/19/03 - WTB947
				// Return true if the toolbars manager is inherited
				// to prevent MS's v1.1 serialization bug from surfacing
				if ( this.Manager != null &&
					 this.Manager.IsDesignMode )
				{
					if ( this == this.Manager.TabGroupSettings )
					{
						InheritanceAttribute inherit = Infragistics.Win.Utilities.GetInheritanceAttribute( this.Manager );

						if ( inherit != null &&
							 inherit.InheritanceLevel == InheritanceLevel.Inherited )
							return !this.ShouldSerialize();
					}
				}

				return false; 
			}

			set { } // Don't do anything on a set
		}

		#endregion ForceSerialization

		// AS 2/6/04 WTB1314
		#region TabAreaMargins
		/// <summary>
		/// Returns or sets the amount of space between the edge of the tab group and the tabs and scroll buttons.
		/// </summary>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_TabAreaMargins")]
		[LocalizedCategory("LC_Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public Infragistics.Win.Margins TabAreaMargins
		{
			get
			{
				if (this.tabAreaMargins == null)
				{
					this.tabAreaMargins = new Margins(-1,-1,-1,-1,false);
					this.tabAreaMargins.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.tabAreaMargins;
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabAreaMargins"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabAreaMargins"/> differs from the default value.</returns>
		/// <seealso cref="TabAreaMargins"/>
		public bool ShouldSerializeTabAreaMargins()
		{
			return this.tabAreaMargins != null && this.tabAreaMargins.ShouldSerialize();
		}	

		/// <summary>
		/// Resets the <see cref="TabAreaMargins"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabAreaMargins"/>
		public void ResetTabAreaMargins()
		{
			if (!this.ShouldSerializeTabAreaMargins())
				return;

			this.tabAreaMargins.Reset();
		}
		#endregion //TabAreaMargins

		// JAS 12/20/04 Tab Spacing
		#region SpaceAfterTabs

		/// <summary>
		/// Determines the minimum amount of space in the visible area after the tabs.
		/// If this property is set to a number less than 0 then an ArgumentOutOfRangeException is thrown.
		/// </summary>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_SpaceAfterTabs")]
		[LocalizedCategory("LC_Appearance")]
		public DefaultableInteger SpaceAfterTabs
		{
			get
			{
				return this.spaceAfterTabs; 
			}
			set
			{
				if( this.SpaceAfterTabs != value )
				{
					if( value < 0 )
						throw new ArgumentOutOfRangeException();

					this.spaceAfterTabs = value;
					this.NotifyPropChange( TabbedMdiPropertyIds.SpaceAfterTabs );
				}
			}
		}

		/// <summary>
		/// Indicates if the <see cref="SpaceAfterTabs"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="SpaceAfterTabs"/> differs from the default value.</returns>
		/// <seealso cref="SpaceAfterTabs"/>
		public bool ShouldSerializeSpaceAfterTabs()
		{
			return ! this.spaceAfterTabs.IsDefault;
		}	

		/// <summary>
		/// Resets the <see cref="SpaceAfterTabs"/> property to its default value.
		/// </summary>
		/// <seealso cref="SpaceAfterTabs"/>
		public void ResetSpaceAfterTabs()
		{
			this.SpaceAfterTabs = new DefaultableInteger();
		}

		#endregion // SpaceAfterTabs

		// JAS 12/20/04 Tab Spacing
		#region SpaceBeforeTabs

		/// <summary>
		/// Determines the amount of space in the visible area before the tabs.
		/// If this property is set to a number less than 0 then an ArgumentOutOfRangeException is thrown.
		/// </summary>
		[LocalizedDescription("LD_MdiTabGroupSettings_P_SpaceBeforeTabs")]
		[LocalizedCategory("LC_Appearance")]
		[DefaultValue( typeof(DefaultableInteger), "" )]
		public DefaultableInteger SpaceBeforeTabs
		{
			get
			{
				return this.spaceBeforeTabs; 
			}
			set
			{
				if( this.SpaceBeforeTabs != value )
				{
					if( value < 0 )
						throw new ArgumentOutOfRangeException();

					// JAS 1/21/05 BR01927 - Shouldn't be setting the property in the property's setter. Oops! :)
					//this.SpaceBeforeTabs = value;
					this.spaceBeforeTabs = value;
					this.NotifyPropChange( TabbedMdiPropertyIds.SpaceBeforeTabs );
				}
			}
		}

		/// <summary>
		/// Indicates if the <see cref="SpaceBeforeTabs"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="SpaceBeforeTabs"/> differs from the default value.</returns>
		/// <seealso cref="SpaceBeforeTabs"/>
		public bool ShouldSerializeSpaceBeforeTabs()
		{
			return ! this.spaceBeforeTabs.IsDefault;
		}	

		/// <summary>
		/// Resets the <see cref="SpaceBeforeTabs"/> property to its default value.
		/// </summary>
		/// <seealso cref="SpaceBeforeTabs"/>
		public void ResetSpaceBeforeTabs()
		{
			// MD 11/7/07
			// Noticed while doing work for NA 2008 Vol 1
			// Setting the member will not fire the prop changed event, set the property instead.
			//this.spaceBeforeTabs = new DefaultableInteger();
			this.SpaceBeforeTabs = new DefaultableInteger();
		}

		#endregion // SpaceBeforeTabs

		// MD 11/6/07 - NA 2008 Vol 1
		#region CloseButtonLocation

		/// <summary>
		/// Gets or sets the location of close buttons within the tab group.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Setting this property to None will prevent a middle mouse button click from closing a tab.
		/// </p>
		/// </remarks>
		/// <exception cref="InvalidEnumArgumentException">
		/// The value assigned is not defined in the <see cref="TabCloseButtonLocation"/> enumeration.
		/// </exception>
		[LocalizedDescription( "LD_MdiTabGroupSettings_P_CloseButtonLocation" )]
		[LocalizedCategory( "LC_Behavior" )]
		public TabCloseButtonLocation CloseButtonLocation
		{
			get { return this.closeButtonLocation; }
			set
			{
				if ( this.closeButtonLocation != value )
				{
					if ( Enum.IsDefined( typeof( TabCloseButtonLocation ), value ) == false )
						throw new InvalidEnumArgumentException( "value", (int)value, typeof( TabCloseButtonLocation ) );

					this.closeButtonLocation = value;
					this.NotifyPropChange( TabbedMdiPropertyIds.CloseButtonLocation );
				}
			}
		}

		/// <summary>
		/// Resets the <see cref="CloseButtonLocation"/> property to its default value.
		/// </summary>
		/// <seealso cref="CloseButtonLocation"/>
		public void ResetCloseButtonLocation()
		{
			this.CloseButtonLocation = TabCloseButtonLocation.Default;
		}

		/// <summary>
		/// Indicates if the <see cref="CloseButtonLocation"/> property should be serialized.
		/// </summary>
		/// <returns>Returns True if the <see cref="CloseButtonLocation"/> differs from the default value.</returns>
		/// <seealso cref="CloseButtonLocation"/>
		public bool ShouldSerializeCloseButtonLocation()
		{
			return this.closeButtonLocation != TabCloseButtonLocation.Default;
		}

		#endregion CloseButtonLocation

		#region HotTrackCloseButtonAppearance

		/// <summary>
		/// Gets or sets the <see cref="Infragistics.Win.Appearance"/> for the hot tracked close button.
		/// </summary>
		/// <seealso cref="CloseButtonAppearance"/>
		/// <seealso cref="PressedCloseButtonAppearance"/>
		[LocalizedDescription( "LD_UltraTabControlBase_P_HotTrackCloseButtonAppearance" )]
		[LocalizedCategory( "LC_Appearance" )]
		public AppearanceBase HotTrackCloseButtonAppearance
		{
			get
			{
				if ( this.hotTrackCloseButtonAppearanceHolder == null )
				{
					this.hotTrackCloseButtonAppearanceHolder = new AppearanceHolder();
					this.hotTrackCloseButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if ( this.Manager != null )
					this.hotTrackCloseButtonAppearanceHolder.Collection = this.Manager.Appearances;

				return this.hotTrackCloseButtonAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.hotTrackCloseButtonAppearanceHolder == null ||
					!this.hotTrackCloseButtonAppearanceHolder.HasAppearance ||
					value != this.hotTrackCloseButtonAppearanceHolder.Appearance )
				{
					if ( null == this.hotTrackCloseButtonAppearanceHolder )
					{
						this.hotTrackCloseButtonAppearanceHolder = new Infragistics.Win.AppearanceHolder();
						this.hotTrackCloseButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if ( this.Manager != null )
						this.hotTrackCloseButtonAppearanceHolder.Collection = this.Manager.Appearances;

					this.hotTrackCloseButtonAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.HotTrackCloseButtonAppearance );
				}
			}
		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="HotTrackCloseButtonAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="HotTrackCloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		[Browsable( false )]
		public bool HasHotTrackCloseButtonAppearance
		{
			get
			{
				return ( null != this.hotTrackCloseButtonAppearanceHolder &&
					this.hotTrackCloseButtonAppearanceHolder.HasAppearance );
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="HotTrackCloseButtonAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="HotTrackCloseButtonAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="HotTrackCloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeHotTrackCloseButtonAppearance()
		{
			return this.HasHotTrackCloseButtonAppearance && this.hotTrackCloseButtonAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="HotTrackCloseButtonAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="HotTrackCloseButtonAppearance"/> property. If the <see cref="HotTrackCloseButtonAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="HotTrackCloseButtonAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="HotTrackCloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable( EditorBrowsableState.Advanced )]
		public void ResetHotTrackCloseButtonAppearance()
		{
			if ( this.HasHotTrackCloseButtonAppearance )
			{
				this.hotTrackCloseButtonAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.HotTrackCloseButtonAppearance );
			}
		}

		#endregion HotTrackCloseButtonAppearance

		#region PressedCloseButtonAppearance

		/// <summary>
		/// Gets or sets the <see cref="Infragistics.Win.Appearance"/> for the pressed close button.
		/// </summary>
		/// <seealso cref="CloseButtonAppearance"/>
		/// <seealso cref="HotTrackCloseButtonAppearance"/>
		[LocalizedDescription( "LD_UltraTabControlBase_P_PressedCloseButtonAppearance" )]
		[LocalizedCategory( "LC_Appearance" )]
		public AppearanceBase PressedCloseButtonAppearance
		{
			get
			{
				if ( this.pressedCloseButtonAppearanceHolder == null )
				{
					this.pressedCloseButtonAppearanceHolder = new AppearanceHolder();
					this.pressedCloseButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if ( this.Manager != null )
					this.pressedCloseButtonAppearanceHolder.Collection = this.Manager.Appearances;

				return this.pressedCloseButtonAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.pressedCloseButtonAppearanceHolder == null ||
					!this.pressedCloseButtonAppearanceHolder.HasAppearance ||
					value != this.pressedCloseButtonAppearanceHolder.Appearance )
				{
					if ( null == this.pressedCloseButtonAppearanceHolder )
					{
						this.pressedCloseButtonAppearanceHolder = new Infragistics.Win.AppearanceHolder();
						this.pressedCloseButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if ( this.Manager != null )
						this.pressedCloseButtonAppearanceHolder.Collection = this.Manager.Appearances;

					this.pressedCloseButtonAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.PressedCloseButtonAppearance );
				}
			}
		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="PressedCloseButtonAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="PressedCloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		[Browsable( false )]
		public bool HasPressedCloseButtonAppearance
		{
			get
			{
				return ( null != this.pressedCloseButtonAppearanceHolder &&
					this.pressedCloseButtonAppearanceHolder.HasAppearance );
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="PressedCloseButtonAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="PressedCloseButtonAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="PressedCloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializePressedCloseButtonAppearance()
		{
			return this.HasPressedCloseButtonAppearance && this.pressedCloseButtonAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="PressedCloseButtonAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="PressedCloseButtonAppearance"/> property. If the <see cref="PressedCloseButtonAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="PressedCloseButtonAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="PressedCloseButtonAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable( EditorBrowsableState.Advanced )]
		public void ResetPressedCloseButtonAppearance()
		{
			if ( this.HasPressedCloseButtonAppearance )
			{
				this.pressedCloseButtonAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.PressedCloseButtonAppearance );
			}
		}

		#endregion PressedCloseButtonAppearance

		#endregion Properties

		#region Methods

		#region Public

		#region ShouldSerialize
		/// <summary>
		/// Indicates if any of the property values differ from the default value.
		/// </summary>
		/// <returns>True if the object needs to be serialized.</returns>
		public bool ShouldSerialize()
		{
			return this.ShouldSerializeTag()					||
				this.ShouldSerializeAllowDrop()					||
				this.ShouldSerializeAutoSelect()				||
				this.ShouldSerializeAutoSelectDelay()			||
				this.ShouldSerializeButtonStyle()				||
				this.ShouldSerializeInterTabSpacing()			||
				this.ShouldSerializeScrollArrowStyle()			||
				this.ShouldSerializeScrollButtons()				||
				this.ShouldSerializeShowButtonSeparators()		||

				// MD 11/7/07 - NA 2008 Vol 1
				// This is obsolete
				//this.ShouldSerializeShowCloseButton()			||

				this.ShouldSerializeTabButtonStyle()			||
				this.ShouldSerializeTabHeight()					||
				this.ShouldSerializeTabOrientation()			||
				this.ShouldSerializeTabPadding()				||
				this.ShouldSerializeTabsPerRow()				||
				this.ShouldSerializeTextOrientation()			||
				this.ShouldSerializeCloseButtonAppearance()		||
				this.ShouldSerializeScrollButtonAppearance()	||
				this.ShouldSerializeTabStyle()					||
				this.ShouldSerializeTabSizing()					||
				this.ShouldSerializeMaxTabWidth()				||
				this.ShouldSerializeScrollButtonTypes()			||
				this.ShouldSerializeScrollTrackAppearance()		||
				this.ShouldSerializeScrollTrackExtent()			||

				// AS 2/6/04 WTB1314
				this.ShouldSerializeTabAreaMargins()			||
				// JDN 8/13/04 NAS2004 Vol3
				this.ShouldSerializeShowPartialTabs()			||
				// JDN 8/17/04 NAS2004 Vol3
				this.ShouldSerializeShowTabListButton()			||
				this.ShouldSerializeTabListButtonAppearance()	||

				this.ShouldSerializeTabAreaAppearance()			||

				// JAS 12/20/04 Tab Spacing
				this.ShouldSerializeSpaceAfterTabs()			||
				this.ShouldSerializeSpaceBeforeTabs()			||

				// MD 11/6/07 - Bug 667
				// This should have been considered in here
				this.ShouldSerializeMinTabWidth()				||

				// MD 11/6/07 - NA 2008 Vol 1
				this.ShouldSerializeCloseButtonLocation()		||
				this.ShouldSerializeHotTrackCloseButtonAppearance() ||
				this.ShouldSerializePressedCloseButtonAppearance();
		}
		#endregion ShouldSerialize

		#region Reset
		/// <summary>
		/// Resets the object to its default state.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void Reset()
		{
			this.ResetTag();
			this.ResetAllowDrop();
			this.ResetAutoSelect();
			this.ResetAutoSelectDelay();
			this.ResetButtonStyle();
			this.ResetInterTabSpacing();
			this.ResetScrollArrowStyle();
			this.ResetScrollButtons();
			this.ResetScrollButtonTypes();
			this.ResetShowButtonSeparators();

			// MD 11/7/07 - NA 2008 Vol 1
			// This is obsolete
			//this.ResetShowCloseButton();

			this.ResetTabButtonStyle();
			this.ResetTabHeight();
			this.ResetTabOrientation();
			this.ResetTabsPerRow();
			this.ResetTextOrientation();
			this.ResetCloseButtonAppearance();
			this.ResetScrollButtonAppearance();
			this.ResetTabAreaAppearance();
			this.ResetTabPadding();
			this.ResetTabSizing();
			this.ResetTabStyle();
			this.ResetScrollTrackExtent();
			this.ResetScrollTrackAppearance();

			// AS 2/6/04 WTB1314
			this.ResetTabAreaMargins();
			// JDN 8/13/04 NAS2004 Vol3
			this.ResetShowPartialTabs();
			// JDN 8/17/04 NAS2004 Vol3
			this.ResetShowTabListButton();
			this.ResetTabListButtonAppearance();

			// MD 11/6/07 - Bug 667
			// These should have been reset in here
			this.ResetMaxTabWidth();
			this.ResetMinTabWidth();
			this.ResetSpaceAfterTabs();
			this.ResetSpaceBeforeTabs();

			// MD 11/6/07 - NA 2008 Vol 1
			this.ResetCloseButtonLocation();
			this.ResetHotTrackCloseButtonAppearance();
			this.ResetPressedCloseButtonAppearance();
		}
		#endregion Reset

		#region ToString
		/// <summary>
        /// Returns a <see cref="System.String"/> that represents the current <see cref="MdiTabGroupSettings"/>.
		/// </summary>
        /// <returns>A <see cref="System.String"/> that represents the current <see cref="MdiTabGroupSettings"/>.</returns>
		public override string ToString()
		{
			return string.Empty;
		}
		#endregion //ToString

		#endregion Public

		#region Internal

		#region InitializeOwner
		internal void InitializeOwner( object owner )
		{
			if (owner == this.owner)
				return;

			if (this.owner != null)
				Debug.Fail("An owner was already specified");

			this.owner = owner;
		}
		#endregion InitializeOwner

		#region InitializeFrom
		internal void InitializeFrom( MdiTabGroupSettings source )
		{
			this.Reset();

			if (source == null)
				return;

			if (source.HasCloseButtonAppearance)
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				Infragistics.Win.AppearanceBase app = this.CloseButtonAppearance;
				
				// initialize the appearance holder...
				this.closeButtonAppearanceHolder.InitializeFrom( source.closeButtonAppearanceHolder );
			}
			else if (this.HasCloseButtonAppearance)
				this.closeButtonAppearanceHolder.Reset();

			// JDN 8/18/04 NAS2004 Vol3
			if ( source.HasTabListButtonAppearance )
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				Infragistics.Win.AppearanceBase app = this.TabListButtonAppearance;
				
				// initialize the appearance holder...
				this.tabListButtonAppearanceHolder.InitializeFrom( source.tabListButtonAppearanceHolder );
			}
			else if ( this.HasTabListButtonAppearance )
				this.tabListButtonAppearanceHolder.Reset();


			if (source.HasScrollButtonAppearance)
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				Infragistics.Win.AppearanceBase app = this.ScrollButtonAppearance;
				
				// initialize the appearance holder...
				this.scrollButtonAppearanceHolder.InitializeFrom( source.scrollButtonAppearanceHolder );
			}
			else if (this.HasScrollButtonAppearance)
				this.scrollButtonAppearanceHolder.Reset();

			if (source.HasScrollTrackAppearance)
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				Infragistics.Win.AppearanceBase app = this.ScrollTrackAppearance;
				
				// initialize the appearance holder...
				this.scrollTrackAppearanceHolder.InitializeFrom( source.scrollTrackAppearanceHolder );
			}
			else if (this.HasScrollTrackAppearance)
				this.scrollTrackAppearanceHolder.Reset();

			if (source.HasTabAreaAppearance)
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				Infragistics.Win.AppearanceBase app = this.TabAreaAppearance;
				
				// initialize the appearance holder...
				this.tabAreaAppearanceHolder.InitializeFrom( source.tabAreaAppearanceHolder );
			}
			else if (this.HasTabAreaAppearance)
				this.tabAreaAppearanceHolder.Reset();

			this.allowDrop = source.allowDrop;
			this.autoSelect = source.autoSelect;
			this.showButtonSeparators = source.showButtonSeparators;

			// MD 11/7/07 - NA 2008 Vol 1
			// There is no showCloseButton member anymore
			//this.showCloseButton = source.showCloseButton;

			// JDN 8/13/04 NAS2004 Vol3
			this.showPartialTabs = source.showPartialTabs;
			// JDN 8/17/04 NAS2004 Vol3
			this.showTabListButton = source.showTabListButton;

			this.autoSelectDelay = source.autoSelectDelay;
			this.interTabSpacing = source.interTabSpacing;
			this.tabHeight = source.tabHeight;
			this.tabsPerRow = source.tabsPerRow;
			this.maxTabWidth = source.maxTabWidth;
			this.minTabWidth = source.minTabWidth;
			this.scrollTrackExtent = source.scrollTrackExtent;

			this.buttonStyle = source.buttonStyle;
			this.tabButtonStyle = source.tabButtonStyle;

			this.scrollArrowStyle = source.scrollArrowStyle;

			this.scrollButtons = source.scrollButtons;
			this.tabOrientation = source.tabOrientation;
			this.textOrientation = source.textOrientation;

			this.tabPadding = source.TabPadding;

			this.tabSizing = source.tabSizing;
			this.tabStyle = source.tabStyle;
			
			this.scrollButtonTypes = source.scrollButtonTypes;

			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;
			else
				this.tagValue = null;

			// AS 2/6/04 WTB1314
			if (source.ShouldSerializeTabAreaMargins())
			{
				if (this.tabAreaMargins != null)
					this.tabAreaMargins.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				this.tabAreaMargins = source.tabAreaMargins.Clone() as Margins;
				this.tabAreaMargins.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}
			else
				this.ResetTabAreaMargins();

			// JAS 12/20/04 Tab Spacing
			this.spaceAfterTabs = source.spaceAfterTabs;
			this.spaceBeforeTabs = source.spaceBeforeTabs;

			// MD 11/7/07 - NA 2008 Vol 1
			this.closeButtonLocation = source.closeButtonLocation;

			if ( source.HasHotTrackCloseButtonAppearance )
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				AppearanceBase app = this.HotTrackCloseButtonAppearance;

				// initialize the appearance holder...
				this.hotTrackCloseButtonAppearanceHolder.InitializeFrom( source.hotTrackCloseButtonAppearanceHolder );
			}
			else if ( this.HasHotTrackCloseButtonAppearance )
				this.hotTrackCloseButtonAppearanceHolder.Reset();

			if ( source.HasPressedCloseButtonAppearance )
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				AppearanceBase app = this.PressedCloseButtonAppearance;

				// initialize the appearance holder...
				this.pressedCloseButtonAppearanceHolder.InitializeFrom( source.pressedCloseButtonAppearanceHolder );
			}
			else if ( this.HasPressedCloseButtonAppearance )
				this.pressedCloseButtonAppearanceHolder.Reset();
		}
		#endregion //InitializeFrom

		#endregion Internal

		#region Protected

		#region OnSubObjectPropChanged
		/// <summary>
		/// Invoked when a property on a subobject has changed.
		/// </summary>
		/// <param name="propChange">Object providing information about the property change</param>
		protected override void OnSubObjectPropChanged(Infragistics.Shared.PropChangeInfo propChange)
		{
			// pass along property change notifications.

			if (this.HasCloseButtonAppearance && propChange.Source == this.closeButtonAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.CloseButtonAppearance, propChange);
			else if (this.HasScrollButtonAppearance && propChange.Source == this.scrollButtonAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.ScrollButtonAppearance, propChange);
			else if (this.HasTabAreaAppearance && propChange.Source == this.tabAreaAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.TabAreaAppearance, propChange);
			else if (this.HasScrollTrackAppearance && propChange.Source == this.scrollTrackAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.ScrollTrackAppearance, propChange);
			// AS 2/6/04 WTB1314
			else if (this.tabAreaMargins != null && propChange.Source == this.tabAreaMargins)
				this.NotifyPropChange(TabbedMdiPropertyIds.TabAreaMargins, propChange);
			// JDN 8/18/04 NAS2004 Vol3
			else if (this.HasTabListButtonAppearance && propChange.Source == this.tabListButtonAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.TabListButtonAppearance, propChange);
			else if ( this.HasHotTrackCloseButtonAppearance && propChange.Source == this.hotTrackCloseButtonAppearanceHolder.RootAppearance )
				this.NotifyPropChange( TabbedMdiPropertyIds.HotTrackCloseButtonAppearance, propChange );
			else if ( this.HasPressedCloseButtonAppearance && propChange.Source == this.pressedCloseButtonAppearanceHolder.RootAppearance )
			    this.NotifyPropChange( TabbedMdiPropertyIds.PressedCloseButtonAppearance, propChange );

			base.OnSubObjectPropChanged( propChange );
		}
		#endregion OnSubObjectPropChanged

		#region OnDispose
		/// <summary>
		/// Invoked when the resources utilized by the object are released and the object is being disposed.
		/// </summary>
		protected override void OnDispose()
		{
			this.owner = null;

			if (this.closeButtonAppearanceHolder != null)
			{
				this.closeButtonAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.closeButtonAppearanceHolder.Reset();
				this.closeButtonAppearanceHolder = null;
			}

			if (this.scrollButtonAppearanceHolder != null)
			{
				this.scrollButtonAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.scrollButtonAppearanceHolder.Reset();
				this.scrollButtonAppearanceHolder = null;
			}

			if (this.tabAreaAppearanceHolder != null)
			{
				this.tabAreaAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.tabAreaAppearanceHolder.Reset();
				this.tabAreaAppearanceHolder = null;
			}

			if (this.scrollTrackAppearanceHolder != null)
			{
				this.scrollTrackAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.scrollTrackAppearanceHolder.Reset();
				this.scrollTrackAppearanceHolder = null;
			}

			// JDN 8/18/04 NAS2004 Vol3
			if (this.tabListButtonAppearanceHolder != null)
			{
				this.tabListButtonAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.tabListButtonAppearanceHolder.Reset();
				this.tabListButtonAppearanceHolder = null;
			}

			base.Dispose();
		}
		#endregion OnDispose

		// AS 10/15/03 DNF116
		#region GetObjectData
		/// <summary>
		/// Invoked during the serialization of the object.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		// MD 1/4/07 - FxCop
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
		}
		#endregion //GetObjectData

		// AS 10/15/03 DNF116
		#region OnDeserialization
		/// <summary>
		/// Invoked after the entire object graph has been deserialized.
		/// </summary>
		/// <param name="sender">The object that initiated the callback. The functionality for the this parameter is not currently implemented.</param>
		protected virtual void OnDeserialization(object sender)
		{
		}
		#endregion //OnDeserialization

		#endregion //Protected

		#endregion Methods

		#region Interfaces

		#region IDeserializationCallback
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			if (this.closeButtonAppearanceHolder!= null)
				this.closeButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if (this.scrollButtonAppearanceHolder != null)
				this.scrollButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if (this.tabAreaAppearanceHolder != null)
				this.tabAreaAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if (this.scrollTrackAppearanceHolder != null)
				this.scrollTrackAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// AS 2/6/04 WTB1314
			if (this.tabAreaMargins != null)
				this.tabAreaMargins.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// JDN 8/18/04 NAS2004 Vol3
			if (this.tabListButtonAppearanceHolder!= null)
				this.tabListButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// MD 11/7/07 - NA 2008 Vol 1
			if ( this.hotTrackCloseButtonAppearanceHolder != null )
				this.hotTrackCloseButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if ( this.pressedCloseButtonAppearanceHolder != null )
				this.pressedCloseButtonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// AS 10/15/03 DNF116
			// Call the virtual method so a derived class
			// could get the call.
			this.OnDeserialization(sender);
		}
		#endregion //IDeserializationCallback

		#region ISerializable
		// MD 1/4/07 - FxCop
		// We do not need a full demand here, just a link demand
		//[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinTabbedMdi";

			if (this.ShouldSerializeCloseButtonAppearance())
				Utils.SerializeProperty(info, "CloseButtonAppearance", this.closeButtonAppearanceHolder);

			// JDN 8/18/04 NAS2004 Vol3
			if (this.ShouldSerializeTabListButtonAppearance())
				Utils.SerializeProperty(info, "TabListButtonAppearance", this.tabListButtonAppearanceHolder);

			if (this.ShouldSerializeScrollButtonAppearance())
				Utils.SerializeProperty(info, "ScrollButtonAppearance", this.scrollButtonAppearanceHolder);

			if (this.ShouldSerializeTabAreaAppearance())
				Utils.SerializeProperty(info, "TabAreaAppearance", this.tabAreaAppearanceHolder);

			if (this.ShouldSerializeScrollTrackAppearance())
				Utils.SerializeProperty(info, "ScrollTrackAppearance", this.scrollTrackAppearanceHolder);

			if (this.ShouldSerializeAllowDrop())
				Utils.SerializeProperty(info, "AllowDrop", this.allowDrop);
			if (this.ShouldSerializeAutoSelect())
				Utils.SerializeProperty(info, "AutoSelect", this.autoSelect);
			if (this.ShouldSerializeShowButtonSeparators())
				Utils.SerializeProperty(info, "ShowButtonSeparators", this.showButtonSeparators);

			// MD 11/7/07 - NA 2008 Vol 1
			// This is obsolete
			//if (this.ShouldSerializeShowCloseButton())
			//    Utils.SerializeProperty(info, "ShowCloseButton", this.showCloseButton);

			// JDN 8/13/04 NAS2004 Vol3
			if (this.ShouldSerializeShowPartialTabs())
				Utils.SerializeProperty(info, "ShowPartialTabs", this.showPartialTabs);
			// JDN 8/17/04 NAS2004 Vol3
			if (this.ShouldSerializeShowTabListButton())
				Utils.SerializeProperty(info, "ShowTabListButton", this.showTabListButton);
			if (this.ShouldSerializeAutoSelectDelay())
				Utils.SerializeProperty(info, "AutoSelectDelay", this.autoSelectDelay);
			if (this.ShouldSerializeInterTabSpacing())
				Utils.SerializeProperty(info, "InterTabSpacing", this.interTabSpacing);
			if (this.ShouldSerializeTabHeight())
				Utils.SerializeProperty(info, "TabHeight", this.tabHeight);
			if (this.ShouldSerializeTabsPerRow())
				Utils.SerializeProperty(info, "TabsPerRow", this.tabsPerRow);
			if (this.ShouldSerializeMaxTabWidth())
				Utils.SerializeProperty(info, "MaxTabWidth", this.MaxTabWidth);
			if (this.ShouldSerializeMinTabWidth())
				Utils.SerializeProperty(info, "MinTabWidth", this.MinTabWidth);
			if (this.ShouldSerializeScrollTrackExtent())
				Utils.SerializeProperty(info, "ScrollTrackExtent", this.ScrollTrackExtent);

			if (this.ShouldSerializeButtonStyle())
				Utils.SerializeProperty(info, "ButtonStyle", this.buttonStyle);
			if (this.ShouldSerializeTabButtonStyle())
				Utils.SerializeProperty(info, "TabButtonStyle", this.tabButtonStyle);

			if (this.ShouldSerializeScrollArrowStyle())
				Utils.SerializeProperty(info, "ScrollArrowStyle", this.scrollArrowStyle);

			if (this.ShouldSerializeScrollButtons())
				Utils.SerializeProperty(info, "ScrollButtons", this.scrollButtons);
			if (this.ShouldSerializeTabOrientation())
				Utils.SerializeProperty(info, "TabOrientation", this.tabOrientation);
			if (this.ShouldSerializeTextOrientation())
				Utils.SerializeProperty(info, "TextOrientation", this.textOrientation);

			if (this.ShouldSerializeTabPadding())
				Utils.SerializeProperty(info, "TabPadding", this.tabPadding);

			if (this.ShouldSerializeTabSizing())
				Utils.SerializeProperty(info, "TabSizing", this.TabSizing);
			if (this.ShouldSerializeTabStyle())
				Utils.SerializeProperty(info, "TabStyle", this.TabStyle);
		
			if (this.ShouldSerializeScrollButtonTypes())
				Utils.SerializeProperty(info, "ScrollButtonTypes", this.ScrollButtonTypes);

			if (this.ShouldSerializeTag())
				this.SerializeTag(info);

			// JAS 12/20/04 Tab Spacing
			if( this.ShouldSerializeSpaceAfterTabs() )
				Utils.SerializeProperty( info, "SpaceAfterTabs", this.spaceAfterTabs );

			if( this.ShouldSerializeSpaceBeforeTabs() )
				Utils.SerializeProperty( info, "SpaceBeforeTabs", this.spaceBeforeTabs );

			// MD 11/6/07 - Bug 667
			// This should be run-time serialized
			if ( this.ShouldSerializeTabAreaMargins() )
				Utils.SerializeProperty( info, "TabAreaMargins", this.tabAreaMargins );

			// MD 11/6/07 - NA 2008 Vol 1
			if ( this.ShouldSerializeCloseButtonLocation() )
				Utils.SerializeProperty( info, "CloseButtonLocation", this.closeButtonLocation );

			if ( this.ShouldSerializeHotTrackCloseButtonAppearance() )
				Utils.SerializeProperty( info, "HotTrackCloseButtonAppearance", this.hotTrackCloseButtonAppearanceHolder );

			if ( this.ShouldSerializePressedCloseButtonAppearance() )
				Utils.SerializeProperty( info, "PressedCloseButtonAppearance", this.pressedCloseButtonAppearanceHolder );

			// AS 10/15/03 DNF116
			// Call the virtual method so derived classes
			// can serialize additional info.
			//
			this.GetObjectData(info, context);
		}
		#endregion //ISerializable

		#endregion //Interfaces
	}
}
