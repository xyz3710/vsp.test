#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTabs;
using Infragistics.Win.IGControls;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	#region EventIds
	/// <summary>
	/// Enumeration of <see cref="UltraTabbedMdiManager"/> events
	/// </summary>
	public enum TabbedMdiEventIds
	{
		/// <summary>
		/// Event ID that identifies the <see cref="UltraComponentControlManagerBase.MouseEnterElement"/> event
		/// </summary>
		MouseEnterElement = 0,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraComponentControlManagerBase.MouseLeaveElement"/> event
		/// </summary>
		MouseLeaveElement = 1,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.InitializeContextMenu"/> event
		/// </summary>
		InitializeContextMenu,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.InitializeTab"/> event
		/// </summary>
		InitializeTab,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.InitializeTabGroup"/> event
		/// </summary>
		InitializeTabGroup,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.RestoreTab"/> event
		/// </summary>
		RestoreTab,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabClosing"/> event
		/// </summary>
		TabClosing,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabClosed"/> event
		/// </summary>
		TabClosed,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabDragging"/> event
		/// </summary>
		TabDragging,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabDragOver"/> event
		/// </summary>
		TabDragOver,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabDropped"/> event
		/// </summary>
		TabDropped,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabGroupResizing"/> event
		/// </summary>
		TabGroupResizing,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabGroupResized"/> event
		/// </summary>
		TabGroupResized,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabGroupScrolling"/> event
		/// </summary>
		TabGroupScrolling,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabGroupScrolled"/> event
		/// </summary>
		TabGroupScrolled,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabSelecting"/> event
		/// </summary>
		TabSelecting,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabSelected"/> event
		/// </summary>
		TabSelected,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabDisplaying"/> event
		/// </summary>
		TabDisplaying,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabDisplayed"/> event
		/// </summary>
		TabDisplayed,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabMoving"/> event
		/// </summary>
		TabMoving,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabMoved"/> event
		/// </summary>
		TabMoved,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.SplitterDragging"/> event
		/// </summary>
		SplitterDragging,

		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.SplitterDragged"/> event
		/// </summary>
		SplitterDragged,

		// AS 4/14/03 WTB800
		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.StoreTab"/> event
		/// </summary>
		StoreTab,

		// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
		/// <summary>
		/// Event ID that identifies the <see cref="UltraTabbedMdiManager.TabActivated"/> event
		/// </summary>
		TabActivated,
	}
	#endregion // EventIds

	#region EventGroups enum
	/// <summary>
	/// Identifies groups of <see cref="UltraTabbedMdiManager"/> events
	/// </summary>
	public enum TabbedMdiEventGroups
	{
		/// <summary>
		/// All events 
		/// </summary>
		AllEvents    = 0,

		/// <summary>
		/// Before events
		/// </summary>
		BeforeEvents = 1,

		/// <summary>
		/// After events
		/// </summary>
		AfterEvents  = 2
	}

	#endregion //EventGroups

	#region EventArg classes
	/// <summary>
	/// Abstract base event arguments class for a cancelable event involving a <see cref="ContextMenu"/>
	/// </summary>
	public abstract class ContextMenuBaseEventArgs : CancelEventArgs
	{
		#region Member Variables

		// AS 4/21/03 WTB818
		// Changed to use an IGContextMenu
		//
		private IGContextMenu			contextMenu = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="ContextMenuBaseEventArgs"/>
		/// </summary>
		/// <param name="contextMenu">Associated <b>ContextMenu</b></param>
		protected ContextMenuBaseEventArgs(IGContextMenu contextMenu)
		{
			this.contextMenu = contextMenu;
		}
		#endregion //Constructor

		#region Properties

		#region ContextMenu
		/// <summary>
		/// Returns the associated <see cref="ContextMenu"/>
		/// </summary>
		/// <remarks>
		/// <p class="note">Note: When adding <see cref="System.Windows.Forms.MenuItem"/> objects to the 
		/// <b>ContextMenu</b>, you should use <see cref="IGMenuItem"/> objects so that they will be 
		/// rendered by the <b>ContextMenu</b> using the same <see cref="IGContextMenu.Style"/></p>
		/// </remarks>
		public IGContextMenu ContextMenu
		{
			get { return this.contextMenu; }
		}
		#endregion //ContextMenu

		#endregion //Properties
	}	

	/// <summary>
	/// Cancelable Event arguments class for the <see cref="UltraTabbedMdiManager.InitializeContextMenu"/> event
	/// </summary>
	/// <seealso cref="MdiTabContextMenuEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.InitializeContextMenu"/>
	public class MdiTabContextMenuEventArgs : ContextMenuBaseEventArgs
	{
		#region Member Variables

		private MdiTab					tab = null;
		private MdiTabContextMenu		contextMenuType;
		private UltraTabbedMdiManager	manager = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabContextMenuEventArgs"/>
		/// </summary>
		/// <param name="manager">The owning <see cref="UltraTabbedMdiManager"/></param>
		/// <param name="tab"><b>MdiTab</b> for which the context menu will be displayed</param>
		/// <param name="contextMenu"><b>ContextMenu</b> that will be displayed. The ContextMenu should be initialized based on the <paramref name="tab"/></param>
		/// <param name="contextMenuType">Enumeration indicating the reason that the context menu will be displayed</param>
		public MdiTabContextMenuEventArgs(UltraTabbedMdiManager manager, MdiTab tab, IGContextMenu contextMenu, MdiTabContextMenu contextMenuType) : base(contextMenu)
		{
			this.tab = tab;
			this.contextMenuType = contextMenuType;
			this.manager = manager;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the <see cref="MdiTab"/> for which the context menu will be displayed
		/// </summary>
		public MdiTab Tab
		{
			get { return this.tab; }
		}

		/// <summary>
		/// Returns the reason that the context menu is being displayed.
		/// </summary>
		public MdiTabContextMenu ContextMenuType
		{
			get { return this.contextMenuType; }
		}
		#endregion //Properties

		#region Methods
		/// <summary>
		/// Returns an enumeration indicating the action associated with the menu item.
		/// </summary>
		/// <param name="menuItem">MenuItem to evaluate</param>
		/// <returns>An enumeration indicating the function of the specified menu item.</returns>
		public MdiTabMenuItems GetMenuType( MenuItem menuItem )
		{
			return this.manager.GetMenuType(menuItem);
		}
		#endregion //Methods
	}

	/// <summary>
	/// Event arguments class for an event involving an <see cref="MdiTab"/>
	/// </summary>
	/// <seealso cref="MdiTabEventHandler"/>
	public class MdiTabEventArgs : EventArgs
	{
		#region Member Variables

		private MdiTab				tab = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabEventArgs"/>
		/// </summary>
		/// <param name="tab">The associated <see cref="MdiTab"/></param>
		public MdiTabEventArgs(MdiTab tab)
		{
			this.tab = tab;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the associated <see cref="MdiTab"/>
		/// </summary>
		public MdiTab Tab
		{
			get { return this.tab; }
		}

		#endregion //Properties

		// AS 4/14/03 WTB800
		#region Methods
		internal void Initialize(MdiTab tab)
		{
			this.tab = tab;
		}
		#endregion //Methods
	}

	/// <summary>
	/// Event arguments class for an event involving an <see cref="MdiTabGroup"/>
	/// </summary>
	/// <seealso cref="MdiTabGroupEventHandler"/>
	public class MdiTabGroupEventArgs : EventArgs
	{
		#region Member Variables

		private MdiTabGroup			tabGroup = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupEventArgs"/>
		/// </summary>
		/// <param name="tabGroup">The associated <see cref="MdiTabGroup"/></param>
		public MdiTabGroupEventArgs(MdiTabGroup tabGroup)
		{
			this.tabGroup = tabGroup;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the associated <see cref="MdiTabGroup"/>
		/// </summary>
		public MdiTabGroup TabGroup
		{
			get { return this.tabGroup; }
		}

		#endregion //Properties
	}

	/// <summary>
	/// Event arguments class for a cancelable event involving an <see cref="MdiTab"/>
	/// </summary>
	/// <seealso cref="CancelableMdiTabEventHandler"/>
	public class CancelableMdiTabEventArgs : CancelEventArgs
	{
		#region Member Variables

		private MdiTab				tab = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="CancelableMdiTabEventArgs"/>
		/// </summary>
		/// <param name="tab">The associated <see cref="MdiTab"/></param>
		public CancelableMdiTabEventArgs(MdiTab tab)
		{
			this.tab = tab;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the associated <see cref="MdiTab"/>
		/// </summary>
		public MdiTab Tab
		{
			get { return this.tab; }
		}

		#endregion //Properties
	}

	/// <summary>
	/// Event arguments class for a cancelable event involving an <see cref="MdiTabGroup"/>
	/// </summary>
	/// <seealso cref="CancelableMdiTabGroupEventHandler"/>
	public class CancelableMdiTabGroupEventArgs : CancelEventArgs
	{
		#region Member Variables

		private MdiTabGroup			tabGroup = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="CancelableMdiTabGroupEventArgs"/>
		/// </summary>
		/// <param name="tabGroup">The associated <see cref="MdiTabGroup"/></param>
		public CancelableMdiTabGroupEventArgs(MdiTabGroup tabGroup)
		{
			this.tabGroup = tabGroup;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the associated <see cref="MdiTabGroup"/>
		/// </summary>
		public MdiTabGroup TabGroup
		{
			get { return this.tabGroup; }
		}

		#endregion //Properties
	}

	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.RestoreTab"/> event
	/// </summary>
	/// <seealso cref="RestoreTabEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.RestoreTab"/>
	public class RestoreTabEventArgs : MdiTabEventArgs
	{
		#region Member Variables

		private Form			form = null;
		private bool			wasVisible;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="RestoreTabEventArgs"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> that is being deserialized.</param>
		/// <param name="wasVisible">Indicates if the form associated with the tab was visible when persisted.</param>
		public RestoreTabEventArgs(MdiTab tab, bool wasVisible) : base(tab)
		{
			this.wasVisible = wasVisible;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns or sets the Form associated with the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// During deserialization, the tab must be associated with a particular form. 
		/// The <see cref="MdiTab.PersistedInfo"/> property of the MdiTab may be used 
		/// to store information about the tab that can then be used in this event to 
		/// recreate the Form. If the <b>Form</b> property is not set, the deserialized MdiTab 
		/// will be discarded.
		/// </p>
		/// </remarks>
		public Form Form
		{
			get { return this.form; }
			set { this.form = value; }
		}

		/// <summary>
		/// ReadOnly. Indicates if the associated form was visible when the tab was serialized.
		/// </summary>
		public bool WasVisible
		{
			get { return this.wasVisible; }
		}
		#endregion //Properties
	}

	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabDragOver"/> event.
	/// </summary>
	/// <seealso cref="MdiTabDragOverEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.TabDragOver"/>
	public class MdiTabDragOverEventArgs : EventArgs
	{
		#region Member Variables

		private MdiTab				tab = null;
		private Point				point = Point.Empty;
		private bool				allowDrop = true;
		private Cursor				dragCursor = null;
		private MdiTabGroup			tabGroup = null;
		private MdiTabDropAction	dropAction;

		// AS 3/24/05 NA 2005 Vol 2 - Nested TabGroups
		private Orientation			orientation;
		private RelativePosition	relativePosition;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabDragOverEventArgs"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> being dragged</param>
		/// <param name="point">Current location of the mouse in screen coordinates.</param>
		/// <param name="dropAction">Action that will be taken if the tab dropped at this point.</param>
		/// <param name="tabGroup">Returns the associated <see cref="MdiTabGroup"/></param>
		// AS 3/24/05 NA 2005 Vol 2 - Nested TabGroups
		// Deprecated the old constructor.
		//
		[Obsolete("This constructor is no longer being used. Instead use the new constructor which takes a RelativePosition and Orientation.", false)]
		public MdiTabDragOverEventArgs(MdiTab tab, Point point, MdiTabDropAction dropAction, MdiTabGroup tabGroup) :
			this(tab, point, dropAction, tabGroup, RelativePosition.After, tabGroup == null ? tab.Manager.Orientation : tabGroup.ParentOrientation)
		{
		}

		// AS 3/24/05 NA 2005 Vol 2 - Nested TabGroups
		/// <summary>
		/// Initializes a new <see cref="MdiTabDragOverEventArgs"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> being dragged</param>
		/// <param name="point">Current location of the mouse in screen coordinates.</param>
		/// <param name="dropAction">Action that will be taken if the tab dropped at this point.</param>
		/// <param name="tabGroup">Returns the associated <see cref="MdiTabGroup"/> or null if the root tab groups will be affected.</param>
		/// <param name="relativePosition">Position of the new tab group in relation to the specified tab group.</param>
		/// <param name="orientation">Orientation of the tabgroups collection that will contain the new tab group.</param>
		public MdiTabDragOverEventArgs(MdiTab tab, Point point, MdiTabDropAction dropAction, MdiTabGroup tabGroup, RelativePosition relativePosition, Orientation orientation)
		{
			this.tab = tab;
			this.point = point;
			this.dropAction = dropAction;
			this.tabGroup = tabGroup;

			// AS 3/24/05 NA 2005 Vol 2 - Nested TabGroups
			this.orientation = orientation;
			this.relativePosition = relativePosition;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the <see cref="MdiTab"/> being dragged
		/// </summary>
		public MdiTab Tab
		{
			get { return this.tab; }
		}

		/// <summary>
		/// Returns the location of the mouse, in screen coordinates, that is being processed.
		/// </summary>
		public Point Point
		{
			get { return this.point; }
		}

		/// <summary>
		/// Returns or sets whether the <see cref="Tab"/> may be dropped at this location
		/// </summary>
		public bool AllowDrop
		{
			get { return this.allowDrop; }
			set { this.allowDrop = value; }
		}

		/// <summary>
		/// Returns or sets the <see cref="Cursor"/> to display at this location. Leave this set to null to show the default cursor.
		/// </summary>
		public Cursor DragCursor
		{
			get { return this.dragCursor; }
			set { this.dragCursor = value; }
		}

		/// <summary>
		/// Returns the <see cref="MdiTabGroup"/> that the <see cref="Tab"/> is being dragged over or null if the mouse is not over a <see cref="TabGroup"/>
		/// </summary>
		public MdiTabGroup TabGroup
		{
			get { return this.tabGroup; }
		}

		/// <summary>
		/// Returns the action that will be taken if the mouse is released at this location.
		/// </summary>
		public MdiTabDropAction DropAction
		{
			get { return this.dropAction; }
		}

		// AS 3/24/05 NA 2005 Vol 2 - Nested TabGroups
		/// <summary>
		/// Returns the position of the new group with respect to the <see cref="MdiTabDragOverEventArgs.TabGroup"/>.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note:</b> This property only has meaning when the <see cref="MdiTabDragOverEventArgs.DropAction"/> is 
		/// <b>NewHorizontalGroup</b> or <b>NewVerticalGroup</b>.</p>
		/// </remarks>
		/// <seealso cref="UltraTabbedMdiManager.MoveToNewGroup(MdiTab,MdiTabGroup,RelativePosition,Orientation)"/>
		public RelativePosition RelativePosition
		{
			get { return this.relativePosition; }
		}

		/// <summary>
		/// Returns the orientation of the new group.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note:</b> This property only has meaning when the <see cref="MdiTabDragOverEventArgs.DropAction"/> is 
		/// <b>NewHorizontalGroup</b> or <b>NewVerticalGroup</b>.</p>
		/// </remarks>
		/// <seealso cref="UltraTabbedMdiManager.MoveToNewGroup(MdiTab,MdiTabGroup,RelativePosition,Orientation)"/>
		public Orientation Orientation
		{
			get { return this.orientation; }
		}
		#endregion //Properties
	}

	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabGroupResizing"/> event.
	/// </summary>
	/// <seealso cref="MdiTabGroupResizingEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.TabGroupResizing"/>
	public class MdiTabGroupResizingEventArgs : CancelEventArgs
	{
		#region Member Variables

		private MdiTabGroupsCollection	tabGroups;
		private int[]					newExtents;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupResizingEventArgs"/>
		/// </summary>
		/// <param name="tabGroups">Collection of <see cref="MdiTabGroup"/> objects that are being resized.</param>
		/// <param name="newExtents">Array containing the new extents for the MdiTabGroups in the collection.</param>
		public MdiTabGroupResizingEventArgs( MdiTabGroupsCollection tabGroups, int[] newExtents )
		{
			if (newExtents == null || tabGroups == null || tabGroups.Count != newExtents.Length)
				throw new ArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_0"));

			this.tabGroups = tabGroups;
			this.newExtents = newExtents;
		}
		#endregion //Constructor

		#region Properties

		/// <summary>
		/// Returns a collection of <see cref="MdiTabGroup"/> objects that are being resized.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note:</b> This collection is a flattened collection of all <see cref="MdiTabGroup"/> objects whose <see cref="MdiTabGroup.Extent"/> is 
		/// being changed. Therefore, the collection returned could include groups from different levels.</p>
		/// </remarks>
		public MdiTabGroupsCollection TabGroups
		{
			get { return this.tabGroups; }
		}
		#endregion //Properties

		#region Methods
		/// <summary>
		/// Returns the extent for the specified index of the item in the <see cref="TabGroups"/> collection
		/// </summary>
		/// <param name="index">Index of the item whose new extent should be returned.</param>
		/// <returns>An integer indicating the new extent for the MdiTabGroup at the specified index.</returns>
		public int GetNewExtent( int index )
		{
			if (index < 0 || index > this.TabGroups.Count - 1)
				throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_1"), index, Shared.SR.GetString(null, "LE_V2_Exception_2"));

			return this.newExtents[index];
		}

		/// <summary>
		/// Returns the extent for the specified <see cref="MdiTabGroup"/> in the <see cref="TabGroups"/> collection
		/// </summary>
		/// <param name="tabGroup">MdiTabGroup whose new extent should be returned.</param>
		/// <returns>An integer indicating the new extent for the MdiTabGroup at the specified index.</returns>
		public int GetNewExtent( MdiTabGroup tabGroup )
		{
			int index = this.TabGroups.IndexOf(tabGroup);

			return this.GetNewExtent(index);
		}

		/// <summary>
		/// Returns a copy of the array of new extents for the <see cref="MdiTabGroup"/> objects that have been resized.
		/// </summary>
		/// <returns>Returns a copy of the array of the new extents for the MdiTabGroup objects that have been resized.</returns>
		public int[] GetNewExtents()
		{
			int[] newArray = new int[this.newExtents.Length];

			this.newExtents.CopyTo(newArray,0);

			return newArray;
		}
		#endregion // Methods
	}

	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabGroupResized"/> event.
	/// </summary>
	/// <seealso cref="MdiTabGroupResizedEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.TabGroupResized"/>
	public class MdiTabGroupResizedEventArgs : EventArgs
	{
		#region Member Variables

		private MdiTabGroupsCollection	tabGroups;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupResizedEventArgs"/>
		/// </summary>
		/// <param name="tabGroups">Group of tabs being resized</param>
		public MdiTabGroupResizedEventArgs( MdiTabGroupsCollection tabGroups )
		{
			this.tabGroups = tabGroups;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Collection of <see cref="MdiTabGroup"/> instances that have been resized.
		/// </summary>
		public MdiTabGroupsCollection TabGroups
		{
			get { return this.tabGroups; }
		}
		#endregion //Properties
	}

	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabMoving"/> event
	/// </summary>
	/// <seealso cref="MdiTabMovingEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
	public class MdiTabMovingEventArgs : CancelableMdiTabEventArgs
	{
		#region Member Variables

		private MdiTabGroup				tabGroup = null;
		private int						newTabIndex = 0;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabMovingEventArgs"/>
		/// </summary>
		/// <param name="tab">The associated <see cref="MdiTab"/></param>
		/// <param name="tabGroup">MdiTabGroup that will contain the MdiTab</param>
		/// <param name="tabIndex">Index at which the MdiTab will be added</param>
		public MdiTabMovingEventArgs(MdiTab tab, MdiTabGroup tabGroup, int tabIndex) : base(tab)
		{
			this.tabGroup = tabGroup;
			this.newTabIndex = tabIndex;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the <see cref="MdiTabGroup"/> that will contain the tab being moved.
		/// </summary>
		public MdiTabGroup TabGroup
		{
			get { return this.tabGroup; }
		}

		/// <summary>
		/// Returns the index at which <see cref="MdiTab"/> that will be inserted in the <see cref="MdiTabGroup.Tabs"/> collection of the <see cref="TabGroup"/>.
		/// </summary>
		public int TabIndex
		{
			get { return this.newTabIndex; }
		}

		#endregion //Properties
	}

	/// <summary>
	///	Cancellable event arguments class for <see cref="UltraTabbedMdiManager.SplitterDragging"/> event
	/// </summary>
	/// <seealso cref="MdiSplitterDraggingEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.SplitterDragging"/>
	public class MdiSplitterDraggingEventArgs : CancelableMdiTabGroupEventArgs
	{
		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiSplitterDraggingEventArgs"/>
		/// </summary>
		/// <param name="tabGroup">The associated <see cref="MdiTabGroup"/></param>
		public MdiSplitterDraggingEventArgs(MdiTabGroup tabGroup) : base(tabGroup)
		{
		}
		#endregion //Constructor
	}

	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabGroupScrolling"/> event
	/// </summary>
	/// <seealso cref="MdiTabGroupScrollingEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.TabGroupScrolling"/>
	public class MdiTabGroupScrollingEventArgs : TabScrollingEventArgs
	{
		#region Member Variables

		private MdiTabGroup			tabGroup = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupScrollingEventArgs"/>
		/// </summary>
		/// <param name="tabGroup">The associated <see cref="MdiTabGroup"/></param>
		/// <param name="scrollType">Type of scroll operation</param>
		/// <param name="scrollIncrement">Number of increments that are being scrolled</param>
		public MdiTabGroupScrollingEventArgs(MdiTabGroup tabGroup, Infragistics.Win.UltraWinTabs.ScrollType scrollType, int scrollIncrement) : base(scrollType, scrollIncrement)
		{
			this.tabGroup = tabGroup;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the <see cref="MdiTabGroup"/> being scrolled
		/// </summary>
		public MdiTabGroup TabGroup
		{
			get { return this.tabGroup; }
		}

		#endregion //Properties
	}

	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabGroupScrolled"/> event
	/// </summary>
	/// <seealso cref="MdiTabGroupScrolledEventHandler"/>
	/// <seealso cref="UltraTabbedMdiManager.TabGroupScrolled"/>
	public class MdiTabGroupScrolledEventArgs : TabScrolledEventArgs
	{
		#region Member Variables

		private MdiTabGroup			tabGroup = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupScrolledEventArgs"/>
		/// </summary>
		/// <param name="tabGroup">The associated <see cref="MdiTabGroup"/></param>
		/// <param name="scrollType">Type of scroll operation</param>
		/// <param name="scrollIncrement">Number of increments that are being scrolled</param>
        public MdiTabGroupScrolledEventArgs(MdiTabGroup tabGroup, Infragistics.Win.UltraWinTabs.ScrollType scrollType, int scrollIncrement) : base(scrollType, scrollIncrement)
        {
			this.tabGroup = tabGroup;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns the <see cref="MdiTabGroup"/> that was scrolled
		/// </summary>
		public MdiTabGroup TabGroup
		{
			get { return this.tabGroup; }
		}

		#endregion //Properties
	}

	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabDropped"/> event
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager.TabDropped"/>
	/// <seealso cref="MdiTabDroppedEventHandler"/>
	public class MdiTabDroppedEventArgs : MdiTabEventArgs
	{
		#region Constructor
		/// <summary>
		/// Initializes the <see cref="MdiTabDroppedEventArgs"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> being dragged</param>
		public MdiTabDroppedEventArgs(MdiTab tab) : base(tab)
		{
		}
		#endregion //Constructor
	}

	// AS 4/14/03 WTB800
	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.StoreTab"/> event
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager.StoreTab"/>
	public class StoreTabEventArgs : MdiTabEventArgs
	{
		#region Constructor
		/// <summary>
		/// Initializes the <see cref="StoreTabEventArgs"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> being serialized</param>
		public StoreTabEventArgs(MdiTab tab) : base(tab)
		{
		}
		#endregion //Constructor
	}

	// MD 1/7/08 - BR29406
	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabClosing"/> event.
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager.TabClosing"/>
	public class MdiTabClosingEventArgs : CancelableMdiTabEventArgs
	{
		#region Member Variables

		private MdiTabCloseReason closeReason;

		#endregion Member Variables

		#region Constructor

		/// <summary>
		/// Initializes a new <see cref="MdiTabClosingEventArgs"/> instance.
		/// </summary>
		/// <param name="tab">The tab which is closing.</param>
		/// <param name="closeReason">Indicates why the tab is closing.</param>
		public MdiTabClosingEventArgs( MdiTab tab, MdiTabCloseReason closeReason )
			: base( tab )
		{
			this.closeReason = closeReason;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// Gets the value which indicates why the tab is closing.
		/// </summary>
		public MdiTabCloseReason CloseReason
		{
			get { return this.closeReason; }
		}

		#endregion Properties
	}

	// MD 1/7/08 - BR29406
	/// <summary>
	/// Event arguments class for the <see cref="UltraTabbedMdiManager.TabClosed"/> event.
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager.TabClosed"/>
	public class MdiTabClosedEventArgs : MdiTabEventArgs
	{
		#region Member Variables

		private MdiTabCloseReason closeReason;

		#endregion Member Variables

		#region Constructor

		/// <summary>
		/// Initializes a new <see cref="MdiTabClosedEventArgs"/> instance.
		/// </summary>
		/// <param name="tab">The tab which has closed.</param>
		/// <param name="closeReason">Indicates why the tab has closed.</param>
		public MdiTabClosedEventArgs( MdiTab tab, MdiTabCloseReason closeReason )
			: base( tab )
		{
			this.closeReason = closeReason;
		}

		#endregion Constructor

		#region Properties

		/// <summary>
		/// Gets the value which indicates why the tab has closed.
		/// </summary>
		public MdiTabCloseReason CloseReason
		{
			get { return this.closeReason; }
		}

		#endregion Properties
	}

	#endregion //EventArg classes

	#region Delegates

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.InitializeContextMenu"/> event
	/// </summary>
	/// <seealso cref="MdiTabContextMenuEventArgs"/>
	public delegate void MdiTabContextMenuEventHandler(object sender, MdiTabContextMenuEventArgs e);


	/// <summary>
	/// Delegate for handling an event involving an <see cref="MdiTab"/>
	/// </summary>
	/// <seealso cref="MdiTabEventArgs"/>
	public delegate void MdiTabEventHandler(object sender, MdiTabEventArgs e);

	/// <summary>
	/// Delegate for handling an event involving an <see cref="MdiTabGroup"/>
	/// </summary>
	/// <seealso cref="MdiTabGroupEventArgs"/>
	public delegate void MdiTabGroupEventHandler(object sender, MdiTabGroupEventArgs e);

	/// <summary>
	/// Delegate for handling a cancelable event involving an <see cref="MdiTab"/>
	/// </summary>
	/// <seealso cref="CancelableMdiTabEventArgs"/>
	public delegate void CancelableMdiTabEventHandler(object sender, CancelableMdiTabEventArgs e);

	/// <summary>
	/// Delegate for handling a cancelable event involving an <see cref="MdiTabGroup"/>
	/// </summary>
	/// <seealso cref="CancelableMdiTabGroupEventArgs"/>
	public delegate void CancelableMdiTabGroupEventHandler(object sender, CancelableMdiTabGroupEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.RestoreTab"/> event
	/// </summary>
	/// <seealso cref="RestoreTabEventArgs"/>
	public delegate void RestoreTabEventHandler(object sender, RestoreTabEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.TabDragOver"/> event
	/// </summary>
	/// <seealso cref="MdiTabDragOverEventArgs"/>
	public delegate void MdiTabDragOverEventHandler(object sender, MdiTabDragOverEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.TabGroupResizing"/> event
	/// </summary>
	/// <seealso cref="MdiTabGroupResizingEventArgs"/>
	public delegate void MdiTabGroupResizingEventHandler(object sender, MdiTabGroupResizingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.TabGroupResized"/> event
	/// </summary>
	/// <seealso cref="MdiTabGroupResizedEventArgs"/>
	public delegate void MdiTabGroupResizedEventHandler(object sender, MdiTabGroupResizedEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.TabMoving"/> event
	/// </summary>
	/// <seealso cref="MdiTabMovingEventArgs"/>
	public delegate void MdiTabMovingEventHandler(object sender, MdiTabMovingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.SplitterDragging"/> event
	/// </summary>
	/// <seealso cref="MdiSplitterDraggingEventArgs"/>
	public delegate void MdiSplitterDraggingEventHandler(object sender, MdiSplitterDraggingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.TabGroupScrolled"/> event
	/// </summary>
	/// <seealso cref="MdiTabGroupScrolledEventArgs"/>
	public delegate void MdiTabGroupScrolledEventHandler(object sender, MdiTabGroupScrolledEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.TabGroupScrolling"/> event
	/// </summary>
	/// <seealso cref="MdiTabGroupScrollingEventArgs"/>
	public delegate void MdiTabGroupScrollingEventHandler(object sender, MdiTabGroupScrollingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.TabDropped"/> event
	/// </summary>
	/// <seealso cref="MdiTabDroppedEventArgs"/>
	public delegate void MdiTabDroppedEventHandler(object sender, MdiTabDroppedEventArgs e);

	// AS 4/14/03 WTB800
	/// <summary>
	/// Delegate for handling the <see cref="UltraTabbedMdiManager.StoreTab"/> event
	/// </summary>
	/// <seealso cref="StoreTabEventArgs"/>
	public delegate void StoreTabEventHandler(object sender, StoreTabEventArgs e);

	#endregion //Delegates
}

