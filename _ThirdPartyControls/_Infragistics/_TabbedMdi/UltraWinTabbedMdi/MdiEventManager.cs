#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Class for managing the events of an <see cref="UltraTabbedMdiManager"/> object. It maintains an enabled flag for each event as well as a nested 'in progress' count.
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager"/>
	public class TabbedMdiEventManager : EventManagerBase
	{
		#region Member Variables

		private UltraTabbedMdiManager	manager;
		private static int []			beforeEventIndexes = null;
		private static int []			afterEventIndexes = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <b>TabbedMdiEventManager</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">The EventManager is used to selectively enable and disable events for the control.</p>
		/// </remarks> 
		/// <param name="manager">Owning UltraTabbedMdiManager component.</param>
		public TabbedMdiEventManager(UltraTabbedMdiManager manager) : base( (int)Enum.GetValues(typeof(TabbedMdiEventIds)).Length )
		{
			if (manager == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_3"), Shared.SR.GetString(null, "LE_V2_Exception_4"));

			this.manager = manager;
		}
		#endregion //Constructor

		#region Public members

		/// <summary>
		/// Indicates whether the event identified by the <paramref name="eventid"/> is in progress.
		/// </summary>
		/// <remarks>
		/// <p class="body">To determine if a specific event is processing its event code at the current point in time, use the <b>InProgress</b> method. You should always check whehter and event is processing before disabling it.</p>
		/// <p class="note"><b>Caution</b> Do not disable an event before it has finished processing. Disabling an event while it is still processing can produce unpredictable results.</p>
		/// </remarks>
		/// <param name="eventid">Identifies the event to check.</param>
        /// <returns>true if the event is in progress; otherwise, false.</returns>
		/// <seealso cref="TabbedMdiEventIds"/>
		public bool InProgress(TabbedMdiEventIds eventid )
		{
			return base.InProgress((int)eventid);
		}

		/// <summary>
		/// Indicates whether the event identified by the <paramref name="eventid"/> is enabled.
		/// </summary>
		/// <remarks>
		/// <p class="body">The EventManager is used to selectively enable and disable events for the control. This method will return True if the specified event is enabled and the code in the event's procedure will be executed when the appropriate circumstances arise. If this method returns False, the event is disabled and code entered in the event procedure will not be executed.</p>
		/// </remarks>
		/// <param name="eventid">Identifies the event for which you want to check the enabled state.</param>
		/// <returns>True if the event is currently enabled, False if the event is disabled.</returns>
		/// <seealso cref="TabbedMdiEventIds"/>
		public bool IsEnabled(TabbedMdiEventIds eventid)
		{
			return base.IsEnabled((int)eventid);
		}

		/// <summary>
		/// Enables or disables the event identified by the <paramref name="eventid"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body">The EventManager is used to selectively enable and disable events for the control. You can use this method to enable or disable any event in the control by specifying the event's unique ID and the state you want the event to be set to.</p>
		/// <p class="note"><b>Caution</b> Do not disable an event before it has finished processing. Disabling an event while it is still processing can produce unpredictable results. You should use the <see cref="InProgress"/> method to check whether an event is currently processing code before disabling the event.</p>
		/// </remarks>
		/// <param name="eventid">One of the <see cref="TabbedMdiEventIds"/> indicating which event to update.</param>
		/// <param name="enabled">The state that will be assigned to the event. Specify True to enable  the event, or False to disable it.</param>
		/// <seealso cref="TabbedMdiEventIds"/>
		public void SetEnabled(TabbedMdiEventIds eventid, bool enabled)
		{
			base.SetEnabled((int)eventid, enabled);
		}

		/// <summary>
		/// Indicates whether all events in the specified group are enabled.
		/// </summary>
		/// <remarks>
		/// <p class="body">The EventManager is used to selectively enable and disable events for the control. This method will return True if the events in a group are enabled and the code in the events' procedures will be executed when the appropriate circumstances arise. If this method returns False, at least one event in the group is disabled.</p>
		/// <p class="body"><b>UltraTabbedMdiManager</b> events are categorized into groups to simplify the enabling and disabling of blocks of related events.</p>
		/// </remarks>
		/// <param name="group">The <b>TabbedMdiEventGroups</b> containing the events for which you want to check the enabled state.</param>
		/// <returns>True if all the events in the group are currently enabled, False if any of the group's events are disabled.</returns>
		/// <seealso cref="TabbedMdiEventGroups"/>
		public bool IsEnabled(TabbedMdiEventGroups group)
		{
			switch(group)
			{
				case TabbedMdiEventGroups.AllEvents:
					return this.AllEventsEnabled;

				case TabbedMdiEventGroups.BeforeEvents:
					return base.IsEnabled(this.BeforeEventIndexes);

				case TabbedMdiEventGroups.AfterEvents:
					return base.IsEnabled(this.AfterEventIndexes);

			}

			return false;
		}

		/// <summary>
		/// Enables or disables all the events associated with the specified group.
		/// </summary>
		/// <remarks>
		/// <p class="body">The EventManager is used to selectively enable and disable events for the control. UltraTabbedMdiManager events are categorized into groups to simplify the enabling and disabling of blocks of related events. This method lets you set the enabled state for all of the events in a group at one time.</p>
		/// <p class="note"><b>Caution</b> Do not disable an event before it has finished processing. Disabling an event while it is still processing can produce unpredictable results. You should use the <see cref="InProgress"/> method to check whether an event is currently processing code before disabling the event.</p>
		/// </remarks>
		/// <param name="group">One of the TabbedMdiEventGroups containing the events for which you want to set the enabled state.</param>
		/// <param name="enabled">The state that will be assigned to each event in the group. Specify True to enable all of the group's events, or False to disable all of the group's events.</param>
		/// <seealso cref="TabbedMdiEventGroups"/>
		/// <seealso cref="IsEnabled(TabbedMdiEventGroups)"/>
		public void SetEnabled(TabbedMdiEventGroups group, bool enabled)
		{
			switch(group)
			{
				case TabbedMdiEventGroups.AllEvents:
					base.AllEventsEnabled = enabled;
					break;

				case TabbedMdiEventGroups.BeforeEvents:
					base.SetEnabled(this.BeforeEventIndexes, enabled);
					break;

				case TabbedMdiEventGroups.AfterEvents:
					base.SetEnabled(this.AfterEventIndexes, enabled);
					break;

			}
		}
		#endregion // Public members

		#region Private members
		private int[] AfterEventIndexes
		{
			get
			{
				if ( null == afterEventIndexes )
				{
					afterEventIndexes =  new int [] 
					{
						(int)TabbedMdiEventIds.TabClosed,
						(int)TabbedMdiEventIds.TabDropped,
						(int)TabbedMdiEventIds.TabGroupResized,
						(int)TabbedMdiEventIds.TabGroupScrolled,
						(int)TabbedMdiEventIds.TabSelected,
						(int)TabbedMdiEventIds.TabDisplayed,
						(int)TabbedMdiEventIds.TabMoved,
						(int)TabbedMdiEventIds.SplitterDragged
					};
				}

				return afterEventIndexes;

			}
		}

		private int [] BeforeEventIndexes
		{
			get
			{
				if ( null == beforeEventIndexes )
				{
					beforeEventIndexes =  new int [] 
					{
						(int)TabbedMdiEventIds.TabClosing,
						(int)TabbedMdiEventIds.TabDragging,
						(int)TabbedMdiEventIds.TabGroupResizing,
						(int)TabbedMdiEventIds.TabGroupScrolling,
						(int)TabbedMdiEventIds.TabSelecting,
						(int)TabbedMdiEventIds.TabDisplaying,
						(int)TabbedMdiEventIds.TabMoving,
						(int)TabbedMdiEventIds.SplitterDragging
					};
				}

				return beforeEventIndexes;

			}
		}
		#endregion //Private members

		#region Internal members
		internal void IncrementInProgress(TabbedMdiEventIds eventid)
		{
			base.IncrementInProgress((int)eventid);
		}
 
		internal void DecrementInProgress(TabbedMdiEventIds eventid)
		{
			base.DecrementInProgress((int)eventid);
		}


		internal bool CanFireEvent(TabbedMdiEventIds eventid)
		{
			return this.IsEnabled(eventid);
		}
		#endregion //Internal members
	}
}

