#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Enumeration of the splitter bar events identifiers
	/// </summary>
	public enum SplitterBarEventIds
	{
		/// <summary>
		/// <see cref="SplitterBarControl.SplitterDragging"/>
		/// </summary>
		SplitterDragging,

		/// <summary>
		/// <see cref="SplitterBarControl.SplitterMove"/>
		/// </summary>
		SplitterMove,

		/// <summary>
		/// <see cref="SplitterBarControl.SplitterDragged"/>
		/// </summary>
		SplitterDragged
	}

	#region SplitterMoveEventArgs
	/// <summary>
	/// Event parameter for an event involving the movement of a splitter bar during a drag operation.
	/// </summary>
	public class SplitterMoveEventArgs : System.EventArgs
	{
		#region Member Variables

		private Rectangle			splitterRect = Rectangle.Empty;
		private Cursor				dragCursor = null;
		private Point				mousePosition = Point.Empty;
		private Point				originalMousePosition = Point.Empty;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <b>SplitterMoveEventArgs</b> with the specified splitter rectangle.
		/// </summary>
		/// <param name="splitterRect">Default location for the splitter to be rendered based on its size, orientation and the current mouse position</param>
		/// <param name="mousePosition">Mouse location when the event was invoked.</param>
		/// <param name="originalMousePosition">Mouse location when the drag was initiated</param>
		public SplitterMoveEventArgs( Rectangle splitterRect, Point mousePosition, Point originalMousePosition )
		{
			this.splitterRect = splitterRect;
			this.mousePosition = mousePosition;
			this.originalMousePosition = originalMousePosition;
		}
		#endregion //Constructor

		#region Properties
		/// <summary>
		/// Returns or sets the location and size of the splitter bar
		/// </summary>
		public Rectangle SplitRectangle
		{
			get { return this.splitterRect; }
			set	{ this.splitterRect = value; }	
		}

		/// <summary>
		/// Returns or sets the cursor to display
		/// </summary>
		public Cursor DragCursor
		{
			get { return this.dragCursor; }
			set { this.dragCursor = value; }
		}

		/// <summary>
		/// Returns the location of the mouse when the event was invoked.
		/// </summary>
		public Point MousePosition
		{
			get { return this.mousePosition; }
		}

		/// <summary>
		/// Returns the location of the mouse when the drag was initiated.
		/// </summary>
		public Point OriginalMousePosition
		{
			get { return this.originalMousePosition; }
		}
		#endregion //Properties
	}
	#endregion //SplitterMoveEventArgs

	#region SplitterDraggedEventArgs
	/// <summary>
	/// Event parameter for an event invoked after the splitter bar has been moved.
	/// </summary>
	public class SplitterDraggedEventArgs : System.EventArgs
	{
		#region Member Variables

		private Point				mousePosition = Point.Empty;
		private Point				originalMousePosition = Point.Empty;
		private bool				cancelled = false;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <b>SplitterDraggedEventArgs</b>.
		/// </summary>
		/// <param name="mousePosition">Mouse location where the mouse was released</param>
		/// <param name="originalMousePosition">Mouse location when the drag was initiated</param>
		/// <param name="cancelled">True if the drag was cancelled</param>
		public SplitterDraggedEventArgs( Point mousePosition, Point originalMousePosition, bool cancelled )
		{
			this.mousePosition = mousePosition;
			this.cancelled = cancelled;
			this.originalMousePosition = originalMousePosition;
		}
		#endregion //Constructor

		#region Properties

		/// <summary>
		/// Indicates if the drag was cancelled (e.g. by pressing escape).
		/// </summary>
		public bool Cancelled
		{
			get { return this.cancelled;}
		}

		/// <summary>
		/// Returns the location of the mouse when the event was invoked.
		/// </summary>
		public Point MousePosition
		{
			get { return this.mousePosition; }
		}

		/// <summary>
		/// Returns the location of the mouse when the drag was initiated.
		/// </summary>
		public Point OriginalMousePosition
		{
			get { return this.originalMousePosition; }
		}

		#endregion //Properties
	}
	#endregion //SplitterDraggedEventArgs

	#region Delegates

	/// <summary>
	/// Delegate for handling the event that occurs while the splitter bar is being dragged.
	/// </summary>
	public delegate void SplitterMoveEventHandler(object sender, SplitterMoveEventArgs e );

	/// <summary>
	/// Delegate for handling the event that occurs after the splitter bar has been dragged.
	/// </summary>
	public delegate void SplitterDraggedEventHandler(object sender, SplitterDraggedEventArgs e );

	#endregion //Delegates
}
