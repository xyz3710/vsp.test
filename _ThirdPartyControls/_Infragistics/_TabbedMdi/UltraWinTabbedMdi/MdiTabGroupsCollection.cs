#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using Infragistics.Shared;
using System.Diagnostics;
using System.Collections;

using System.Runtime.Serialization;
using Infragistics.Shared.Serialization;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// A read only collection of <see cref="MdiTabGroup"/> objects.
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>MdiTabGroupsCollection</b> is a read only collection of 
	/// <see cref="MdiTabGroup"/> objects. Each <b>MdiTabGroup</b> object in the collection may be accessed using either the index or <b>Key</b>.</p>
	/// <p class="body">The MdiTabGroupsCollection is used by the UltraTabbedMdiManager (<see cref="UltraTabbedMdiManager.TabGroups"/>) to 
	/// expose the collection of MdiTabGroups that contain the visible MdiTab objects. The collection is also used by <see cref="MdiTabGroupResizingEventArgs"/> and 
	/// <see cref="MdiTabGroupResizedEventArgs"/> classes to expose the collection of tab groups that have been resized.</p>
	/// </remarks>
	/// <seealso cref="UltraTabbedMdiManager.TabGroups"/>
	[Serializable()]
	[System.ComponentModel.TypeConverter(typeof(Infragistics.Shared.ExpandableCollectionConverter))]
	public class MdiTabGroupsCollection : KeyedSubObjectsCollectionBase,
		ISerializable,
		IDeserializationCallback
	{
		#region Member Variables

		private int				initialCapacity = 4;

		#endregion //Member Variables

		#region Constructor

		#region Public
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupsCollection"/>
		/// </summary>
		public MdiTabGroupsCollection()
		{
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupsCollection"/> with the specified initial capacity
		/// </summary>
        /// <param name="initialCapacity">The number of elements that the new collection can initially store. </param>
		public MdiTabGroupsCollection(int initialCapacity)
		{
			this.initialCapacity = initialCapacity;
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupsCollection"/>
		/// </summary>
        /// <param name="tabGroups">An array of MdiTabGroup objects with which to populate the collection.</param>
		public MdiTabGroupsCollection(MdiTabGroup[] tabGroups) : this()
		{
			if (tabGroups == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_15"), Shared.SR.GetString(null, "LE_V2_Exception_16"));

			foreach(MdiTabGroup group in tabGroups)
			{
				if (group == null)
					throw new ArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_17"));
			}

			this.AddRange(tabGroups);
		}
		#endregion // Public

		#region Serialization Constructor
		/// <summary>
		/// Constructor used during deserialization to initialize a new <see cref="MdiTabGroupsCollection"/> object
		/// with the serialized property values.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected MdiTabGroupsCollection( SerializationInfo info, StreamingContext context )
		{
			foreach( SerializationEntry entry in info )
			{
				if ( entry.Value is MdiTabGroup )
				{
					MdiTabGroup tabGroup = (MdiTabGroup)Utils.DeserializeProperty(entry, typeof(MdiTabGroup), null);
			
					if (tabGroup != null)
						this.InternalAdd( tabGroup );			
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							this.initialCapacity =  Math.Max( this.initialCapacity, (int)Utils.DeserializeProperty(entry, typeof(int), this.initialCapacity));
							break;
						}

							// De-serialize the tag property
							//
						case MdiTabGroupsCollection.TagSerializationName:
							this.DeserializeTag(entry);
							break;

						default:
						{
							// AS 4/30/03 FxCop Change
							// Explicitly call the overload that takes an IFormatProvider
							//
							//Debug.Assert( false, string.Format("Invalid entry in {0} de-serialization ctor", this.GetType().Name) );
							Debug.Assert( false, string.Format(null, "Invalid entry in {0} de-serialization ctor", this.GetType().Name) );
							break;
						}
					}
				}
			}
		}
		#endregion //Serialization Constructor

		#endregion //Constructor

		#region Properties

		#region Indexers
		/// <summary>
		/// Returns the <see cref="MdiTabGroup"/> at the specified index.
		/// </summary>
		public MdiTabGroup this[int index]
		{
			get
			{
				return this.GetItem(index) as MdiTabGroup;
			}
		}

		/// <summary>
		/// Returns the <see cref="MdiTabGroup"/> with the specified <see cref="KeyedSubObjectBase.Key"/>
		/// </summary>
		public MdiTabGroup this[string key]
		{
			get
			{
				return this.GetItem(key) as MdiTabGroup;
			}
		}
		#endregion //Indexers

		#endregion //Properties

		#region Methods

		#region Public

		#region Contains
		/// <summary>
		/// Indicates if the specified <see cref="MdiTabGroup"/> is a member of the collection.
		/// </summary>
		/// <param name="tabGroup"><b>MdiTabGroup</b> to evaluate</param>
		/// <returns><b>True</b> if the tab is a member of the collection; otherwise <b>false</b> is returned.</returns>
		public bool Contains( MdiTabGroup tabGroup )
		{
			return base.Contains(tabGroup);
		}
		#endregion //Contains

		#region GetEnumerator

		/// <summary>
		/// Returns the type-safe enumerator for iterating through the <see cref="MdiTabGroup"/> instances in the collection.
		/// </summary>
		/// <returns>A type safe enumerator for iterating through the <see cref="MdiTabGroup"/> objects in the collection.</returns>
		/// <seealso cref="MdiTabGroupEnumerator"/>
		public MdiTabGroupEnumerator GetEnumerator() // non-IEnumerable version
		{
			return new MdiTabGroupEnumerator(this);
		}

		#endregion GetEnumerator

		#endregion //Public

		#region Protected

		#region CreateArray (override)
		/// <summary>
		/// Virtual method used by the All 'get' method to create the array it returns.
		/// </summary>
		/// <returns>The newly created object array</returns>
		/// <remarks>This is normally overridden in a derived class to allocate a type safe array.</remarks>
		protected override object[] CreateArray()
		{
			return new MdiTabGroup[ this.Count ];
		}
		#endregion CreateArray

		#region OnSubObjectPropChanged
		/// <summary>
		/// Invoked when a property has changed on a subobject.
		/// </summary>
		/// <param name="propChange">Object containing information about the change.</param>
		protected override void OnSubObjectPropChanged(Infragistics.Shared.PropChangeInfo propChange)
		{
			if (propChange.Source is MdiTabGroup)
			{
				// pass along any property change notifications
				this.NotifyPropChange( TabbedMdiPropertyIds.MdiTabGroup, propChange );
			}

			base.OnSubObjectPropChanged(propChange);
		}
		#endregion OnSubObjectPropChanged

		// AS 10/15/03 DNF116
		#region GetObjectData
		/// <summary>
		/// Invoked during the serialization of the object.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		// MD 1/4/07 - FxCop
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
		}
		#endregion //GetObjectData

		// AS 10/15/03 DNF116
		#region OnDeserialization
		/// <summary>
		/// Invoked after the entire object graph has been deserialized.
		/// </summary>
		/// <param name="sender">The object that initiated the callback. The functionality for the this parameter is not currently implemented.</param>
		protected virtual void OnDeserialization(object sender)
		{
		}
		#endregion //OnDeserialization

		#endregion Protected

		#region Internal
		internal void Add( MdiTabGroup tabGroup )
		{
			this.Insert(this.Count, tabGroup);
		}

		internal void AddRange( MdiTabGroup[] tabGroups )
		{
			this.InsertRange(this.Count, tabGroups);
		}

		internal void Insert( int index, MdiTabGroup tabGroup )
		{
			Debug.Assert( !this.Contains(tabGroup), "The specified 'MdiTabGroup' is already contained by the collection.");

			tabGroup.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			this.InternalInsert(index, tabGroup);

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTabGroup );
		}

		internal void InsertRange( int index, MdiTabGroup[] tabGroups )
		{
			foreach(MdiTabGroup tabGroup in tabGroups)
				Debug.Assert( !this.Contains(tabGroup), "The specified 'MdiTabGroup' is already contained by the collection.");

			foreach(MdiTabGroup tabGroup in tabGroups)
			{
				tabGroup.SubObjectPropChanged += this.SubObjectPropChangeHandler;

				this.InternalInsert(index, tabGroup);

				index++;
			}

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTabGroup );
		}

		internal void Remove( MdiTabGroup tabGroup )
		{
			Debug.Assert( this.Contains(tabGroup), "The specified 'MdiTabGroup' is not contained by the collection.");

			tabGroup.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalRemove(tabGroup);

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTabGroup );
		}

		internal void Clear()
		{
			foreach(MdiTabGroup tabGroup in this)
				tabGroup.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalClear();

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTabGroup );
		}

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		internal IEnumerable GetNestedTabGroups(bool leafGroupsOnly)
		{
			return new NestedMdiTabGroupsCollection(this, leafGroupsOnly);
		}
		#endregion //Internal

		#endregion //Methods

		#region Base class overrides
		/// <summary>
		/// Returns false to indicate that the collection may not be modified.
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Returns the initial capacity for the collection.
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return this.initialCapacity;
			}
		}
		#endregion //Base class overrides

		#region Interfaces

		#region ISerializable
		/// <summary>
		/// Gets serialization information with all of the non-default information
		/// required to reinstantiate the object.
		/// </summary>
		// MD 1/4/07 - FxCop
		// We do not need a full demand here, just a link demand
		//[System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags=System.Security.Permissions.SecurityPermissionFlag.SerializationFormatter)]
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinTabbedMdi";

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			Utils.SerializeProperty(info, "Count", this.Count);
			
			// add each item in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				Utils.SerializeProperty(info, i.ToString(), this[i]);
			}

			// Serialize the tag property
			//
			if (this.ShouldSerializeTag())
				this.SerializeTag(info);

			// AS 10/15/03 DNF116
			// Call the virtual method so derived classes
			// can serialize additional info.
			//
			this.GetObjectData(info, context);
		}
		#endregion ISerializable

		#region IDeserializationCallback
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			foreach(MdiTabGroup tabGroup in this)
				tabGroup.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// AS 10/15/03 DNF116
			// Call the virtual method so a derived class
			// could get the call.
			this.OnDeserialization(sender);
		}
		#endregion //IDeserializationCallback

		#endregion Interfaces

		#region MdiTabGroupEnumerator class
		/// <summary>
		/// Type-specific enumerator class for enumerating over the <see cref="MdiTabGroup"/> objects of the
		/// <see cref="MdiTabGroupsCollection"/>.
		/// </summary>
		/// <seealso cref="MdiTabGroupsCollection"/>
		/// <seealso cref="MdiTabGroup"/>
		/// <seealso cref="MdiTabGroupsCollection.GetEnumerator()"/>
		public class MdiTabGroupEnumerator : DisposableObjectEnumeratorBase
		{
			/// <summary>
			/// Initializes a new <see cref="MdiTabGroupEnumerator"/>
			/// </summary>
			/// <param name="collection">Collection to iterate</param>
			public MdiTabGroupEnumerator(MdiTabGroupsCollection collection) : base(collection)
			{
			}

			/// <summary>
			/// Returns the current <see cref="MdiTabGroup"/> object in the enumerator.
			/// </summary>
			public MdiTabGroup Current
			{
				get { return ((IEnumerator)this).Current as MdiTabGroup; }
			}
		}
		#endregion //MdiTabGroupEnumerator class

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		#region NestedMdiTabGroupEnumerator class
		private class NestedMdiTabGroupEnumerator : IEnumerator
		{
			#region Member Variables

			private MdiTabGroupsCollection		rootTabGroups;
			private bool						leafGroupsOnly;
			private MdiTabGroup					currentGroup;
			private Stack						parentCollectionStack;

			#endregion //Member Variables

			#region Constructor
			internal NestedMdiTabGroupEnumerator(MdiTabGroupsCollection rootTabGroups, bool leafGroupsOnly)
			{
				this.rootTabGroups = rootTabGroups;
				this.leafGroupsOnly = leafGroupsOnly;
				this.parentCollectionStack = new Stack(3);

				// make sure to reset the enumerator
				((IEnumerator)this).Reset();
			}
			#endregion //Constructor

			#region IEnumerator
			object IEnumerator.Current
			{
				get
				{
					return this.currentGroup;
				}
			}

			bool IEnumerator.MoveNext()
			{
				
				if (this.rootTabGroups.Count == 0)
					return false;

				// temp variable to store the new current group
				MdiTabGroup newGroup = null;

				// figure out the starting pt
				MdiTabGroup tempGroup = this.currentGroup == null ? this.rootTabGroups[0] : this.currentGroup;

				// if we're just starting the enumeration and we want
				// all groups (or this group has tabs), start with the first one
				if (this.currentGroup == null && (!this.leafGroupsOnly || tempGroup.HasTabs))
					newGroup = tempGroup;
				else if (tempGroup.HasTabGroups)
				{
					// if the starting group has child groups, get its first
					// child group that matches the criteria
					while (newGroup == null && tempGroup.HasTabGroups)
					{
						// store the collection the current item has come from
						this.parentCollectionStack.Push(tempGroup.TabGroups);

						// and start with the first item
						tempGroup = tempGroup.TabGroups[0];

						// if this is a leaf group or we're getting all groups...
						if (!tempGroup.HasTabGroups || !this.leafGroupsOnly)
							newGroup = tempGroup;
					}
				}
				else
				{
					while (newGroup == null)
					{
						// we have a leaf group, we either need to get
						// the next sibling node or we need to walk back up
						// the parent chain

						// first check the current group's position in the collection
						MdiTabGroupsCollection parentCollection = this.parentCollectionStack.Peek() as MdiTabGroupsCollection;

						Debug.Assert(parentCollection != null, "Unable to access the parent collection!");

						int index = tempGroup.Index < 0 ? parentCollection.IndexOf(tempGroup) : tempGroup.Index;

						// if there is a sibling after this...
						if (index < parentCollection.Count - 1)
							tempGroup = parentCollection[index + 1];
						else if (parentCollection == this.rootTabGroups)
						{
							// there is no next group
							break;
						}
						else
						{
							Debug.Assert(tempGroup.Parent != null, "Cannot walk up to the parent group since there is no parent!");

							if (tempGroup.Parent == null)
								break;

							// the starting group is the parent group
							tempGroup = tempGroup.Parent;
							this.parentCollectionStack.Pop();
							continue;
						}
						
						// if this is not a leaf group and that's what we're looking for...
						if (!tempGroup.HasTabGroups || !this.leafGroupsOnly)
							newGroup = tempGroup;
						else
						{
							while(tempGroup.HasTabGroups)
							{
								// store the collection the current item has come from
								this.parentCollectionStack.Push(tempGroup.TabGroups);

								// and start with the first item
								tempGroup = tempGroup.TabGroups[0];

								// if this is a leaf group or we're getting all groups...
								if (!tempGroup.HasTabGroups || !this.leafGroupsOnly)
									newGroup = tempGroup;
							}
						}
					}
				}

				if (newGroup != null)
					this.currentGroup = newGroup;

				return newGroup != null;
			}

			void IEnumerator.Reset()
			{
				this.currentGroup = null;
				this.parentCollectionStack.Clear();
				this.parentCollectionStack.Push(this.rootTabGroups);
			}
			#endregion //IEnumerator
		}
		#endregion //NestedMdiTabGroupEnumerator class

		#region NestedMdiTabGroupsCollection
		private class NestedMdiTabGroupsCollection : IEnumerable
		{
			#region Member Variables

			private bool						groupsWithTabsOnly;
			private MdiTabGroupsCollection		tabGroups;

			#endregion //Member Variables

			#region Constructor
			internal NestedMdiTabGroupsCollection(MdiTabGroupsCollection tabGroups, bool groupsWithTabsOnly)
			{
				this.tabGroups = tabGroups;
				this.groupsWithTabsOnly = groupsWithTabsOnly;
			}
			#endregion //Constructor

			#region IEnumerable
			IEnumerator IEnumerable.GetEnumerator()
			{
				return new NestedMdiTabGroupEnumerator(this.tabGroups, this.groupsWithTabsOnly);
			}
			#endregion //IEnumerable
		}
		#endregion //NestedMdiTabGroupsCollection
	}
}
