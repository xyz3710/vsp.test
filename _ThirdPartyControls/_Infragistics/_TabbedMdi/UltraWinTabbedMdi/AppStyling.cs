#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;
using Infragistics.Win.AppStyling;
using Infragistics.Win.AppStyling.Definitions;
using Infragistics.Win.UltraWinTabs;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	#region UltraTabbedMdiManagerRole
	/// <summary>
	/// Component role class used to manage the style settings for an <see cref="UltraTabbedMdiManager"/>
	/// </summary>
	public class UltraTabbedMdiManagerRole : ComponentRole
	{
		#region Member Variables

		private UltraTabbedMdiManager mdiManager;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="UltraTabbedMdiManagerRole"/>
		/// </summary>
		/// <param name="mdiManager">UltraTabbedMdiManager associated with the component</param>
		public UltraTabbedMdiManagerRole(UltraTabbedMdiManager mdiManager) : base(mdiManager, StyleUtils.UltraTabbedMdiManagerRoleName)
		{
			this.mdiManager = mdiManager;

			// initialize the property cache count
			this.PropertiesCacheCount = (int)StyleUtils.CachedProperty.LastValue + 1;
		}
		#endregion //Constructor

		#region Base class overrides

		#region GetRoleNames

		/// <summary>
		/// Used by the associated <see cref="ComponentRole"/> to determine which <see cref="Infragistics.Win.AppStyling.UIRole"/> instances should be cached.
		/// </summary>
		/// <returns>An array of strings containing the names of the role names that should be cached.</returns>
		protected override string[] GetRoleNames( )
		{
			return StyleUtils.GetRoleNames();
		}

		#endregion //GetRoleNames

		#region OnStyleChanged
		/// <summary>
		/// Invoked when the style information has been changed.
		/// </summary>
		protected override void OnStyleChanged()
		{
			// dirty all metrics
			this.mdiManager.OnAppStyleChanged();
		}
		#endregion //OnStyleChanged

		#region SynchronizingObject
		/// <summary>
		/// Object used to synchronize the style change notifications with the ui thread
		/// </summary>
		protected override ISynchronizeInvoke SynchronizingObject
		{
			get { return this.mdiManager.MdiParent; }
		}
		#endregion //SynchronizingObject

		#endregion //Base class overrides
	}
	#endregion //UltraTabbedMdiManagerRole

	#region MdiUIRoleNames
	internal class MdiUIRoleNames
	{
		private MdiUIRoleNames(){}

		// tab item
		internal const string TabItemVertLeft			= "MdiTabItemVerticalLeft";
		internal const string TabItemVertRight			= "MdiTabItemVerticalRight";
		internal const string TabItemHorzTop			= "MdiTabItemHorizontalTop";
		internal const string TabItemHorzBottom			= "MdiTabItemHorizontalBottom";

		// tab item area
		internal const string TabItemAreaVertLeft		= "MdiTabItemAreaVerticalLeft";
		internal const string TabItemAreaVertRight		= "MdiTabItemAreaVerticalRight";
		internal const string TabItemAreaHorzTop		= "MdiTabItemAreaHorizontalTop";
		internal const string TabItemAreaHorzBottom		= "MdiTabItemAreaHorizontalBottom";

		// tab client area
		internal const string TabClientAreaVert		= "MdiTabClientAreaVertical";
		internal const string TabClientAreaHorz		= "MdiTabClientAreaHorizontal";

		// splitter
		internal const string SplitterBar			= "MdiSplitterBar";
		internal const string SplitterBarVert		= "MdiSplitterBarVertical";
		internal const string SplitterBarHorz		= "MdiSplitterBarHorizontal";

		// buttons
		internal const string CloseButton			= "MdiCloseButton";

		// MD 12/18/07
		// Added new roles so appearance of header area and tab close buttons can be styled separately
		internal const string TabItemCloseButton	= "MdiTabItemCloseButton";
		// MD 12/20/07 - Task 1619
		// Added a new role to distinguish close buttons in selected tabs from other close buttons
		internal const string SelectedTabItemCloseButton = "MdiSelectedTabItemCloseButton";
		internal const string HeaderAreaCloseButton	= "MdiHeaderAreaCloseButton";

		internal const string TabListButton			= "MdiTabListButton";
	}
	#endregion //MdiUIRoleNames

	#region StyleUtils
	internal class StyleUtils
	{
		#region Constants

		internal const string UltraTabbedMdiManagerRoleName = "UltraTabbedMdiManager";

		// These constants are finding out the index offset in the ROLES array for getting specific information.
		// 
		internal const int ROLES_STEP = 5, ROLES_ROLE_ENUM = 0, ROLES_ROLE_NAME = 1, ROLES_BASE_ROLE_NAME = 2, ROLES_STATES = 3, ROLES_SUPPORTED_PROPS = 4;

		// These constants are finding out the index offset in the CUSTOMPROPS array for getting specific information.
		// 
		internal const int PROPS_STEP = 6, PROPS_PROP_NAME = 0, PROPS_PROP_TYPE = 1, PROPS_PROP_DEFAULT = 2, PROPS_PROP_CONVERTER = 3, PROPS_PROP_CONSTRAINT = 4, PROPS_PROP_DESC = 5;

		#endregion //Constants

		#region Member Variables

		internal static readonly int TabGroupCachePropertyCount;

		#endregion //Member Variables

		#region Constructor

		static StyleUtils()
		{

			StyleUtils.TabGroupCachePropertyCount = Enum.GetValues(typeof(TabGroupCachedProperty)).Length;
		}

		private StyleUtils() {}

		#endregion //Constructor

		#region Methods

		#region CacheBorderStylePropertyValue

		internal static object CacheBorderStylePropertyValue( UltraTabbedMdiManager manager, CachedProperty prop, Role role, 
			UIElementBorderStyle controlVal, UIElementBorderStyle resolveVal )
		{
			UIElementBorderStyle roleBorderStyle = GetRoleBorderStyle( manager, role );

			return CachePropertyValue( manager, prop, roleBorderStyle, controlVal, UIElementBorderStyle.Default, resolveVal );
		}

		#endregion // CacheBorderStylePropertyValue

		#region CacheButtonStylePropertyValue

		internal static object CacheButtonStylePropertyValue( UltraTabbedMdiManager manager, CachedProperty prop, Role role, 
			UIElementButtonStyle controlVal, UIElementButtonStyle resolveVal )
		{
			UIElementButtonStyle roleButtonStyle = GetRole(manager, role).GetButtonStyle(manager.ComponentRole);

			return CachePropertyValue( manager, prop, roleButtonStyle, controlVal, UIElementButtonStyle.Default, resolveVal );
		}

		internal static object CacheButtonStylePropertyValue( 
			MdiTabGroup tabGroup,				// mdi tab group
			TabGroupCachedProperty cacheProp,	// tab group property to cache
			Role roleId,						// enum identifying the role that provides the style value button style
			object controlVal, 
			object fallbackVal )
		{
			object styleVal;
			object defVal = UIElementButtonStyle.Default;

			styleVal = GetRoleButtonStyle(tabGroup.Manager, roleId);

			return CachePropertyValue(tabGroup, cacheProp, styleVal, controlVal, defVal, fallbackVal);
		}

		#endregion // CacheButtonStylePropertyValue

		#region CachePropertyValue

		internal static object CacheUnresolvedPropertyValue( UltraTabbedMdiManager manager, CustomProperty prop, object defVal )
		{
			return CachePropertyValue(manager, prop, defVal, defVal, defVal);
		}

		internal static object CacheUnresolvedPropertyValue( UltraTabbedMdiManager manager, CachedProperty prop, object styleVal, object defVal )
		{
			return CachePropertyValue(manager, prop, styleVal, defVal, defVal, defVal);
		}

		internal static object CachePropertyValue( UltraTabbedMdiManager manager, CachedProperty prop, 
			object styleValue, object controlVal, object defVal, object resolveVal )
		{
			ComponentRole role = StyleUtils.GetComponentRole(manager);
			if ( null != role )
			{
				OutputUnsetCachedProps(role, (int)prop);

				return role.CachePropertyValue( styleValue, controlVal, defVal, resolveVal, (int)prop );
			}

			return ResolveValueHelper( controlVal, defVal, resolveVal );
		}

		private static object ResolveValueHelper( object val, object defVal, object resolveVal )
		{
			return defVal == resolveVal || ! object.Equals( val, defVal ) ? val : resolveVal;
		}

		internal static object CachePropertyValue( UltraTabbedMdiManager manager, CustomProperty prop, 
			object styleValue, object controlVal, object defVal, object resolveVal )
		{
			ComponentRole role = StyleUtils.GetComponentRole(manager);
			if ( null != role )
			{
				return role.CachePropertyValue( styleValue, controlVal, defVal, resolveVal, (int)prop );
			}

			return ResolveValueHelper( controlVal, defVal, resolveVal );
		}

		internal static object CachePropertyValue( UltraTabbedMdiManager manager, CustomProperty prop, object controlVal, object defVal, object resolveVal )
		{
			ComponentRole role = StyleUtils.GetComponentRole(manager);
			if ( null != role )
			{
				string propName = GetCustomPropertyName( prop );
				object styleVal = null;
				if ( role.HasProperty( propName ) )
					styleVal = role.GetProperty( propName );

				OutputUnsetCachedProps(role, (int)prop);

				return role.CachePropertyValue( styleVal, controlVal, defVal, resolveVal, (int)prop );
			}

			return ResolveValueHelper( controlVal, defVal, resolveVal );
		}

		internal static object CachePropertyValue( MdiTabGroup tabGroup, 
			TabGroupCachedProperty prop, 
			CustomProperty property, 
			object controlVal, 
			object defVal, 
			object fallbackVal )
		{
			object styleVal = null;
			ComponentRole role = tabGroup.Manager != null ? tabGroup.Manager.ComponentRole : null;
			if ( null != role )
			{
				string propName = GetCustomPropertyName( property );
				if ( role.HasProperty( propName ) )
					styleVal = role.GetProperty( propName );
			}

			return CachePropertyValue(tabGroup,
				prop,
				styleVal,
				controlVal,
				defVal,
				fallbackVal);
		}

		internal static object CachePropertyValue( MdiTabGroup tabGroup, 
			TabGroupCachedProperty prop, 
			object styleVal, 
			object controlVal, 
			object defVal, 
			object fallbackVal )
		{
			object finalVal = StyleUtilities.CalculateResolvedValue(
				GetResolutionOrder( tabGroup.Manager ), styleVal, controlVal, defVal, fallbackVal );

			tabGroup.PropertyCache.CachePropertyValue( (int)prop, finalVal );
			return finalVal;
		}

		#endregion // CachePropertyValue

		#region OutputUnsetCachedProps
		[Conditional("DEBUG")]
		internal static void OutputUnsetCachedProps(ComponentRole role, int indexBeingSet)
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			for(int i = 0; i < role.PropertiesCacheCount; i++)
			{
				object val;
				
				if (i != indexBeingSet && false == role.GetCachedProperty(i, out val))
				{
					if (sb.Length > 0)
						sb.Append(",");

					object output = i <= (int)CustomProperty.LastValue ? (object)(CustomProperty)i : (object)(CachedProperty)i;
					sb.Append(output);
				}
			}

			if (sb.Length > 0)
				Debug.WriteLine(sb.ToString(), "Remaining Uncached Properties");
		}
		#endregion //OutputUnsetCachedProps

		#region CalculateBorderStyle
		internal static UIElementBorderStyle CalculateBorderStyle(MdiTab tab, 
			UIRole role, CachedProperty property, UIElementBorderStyle controlVal,
			UIElementBorderStyle fallback)
		{
			UltraTabbedMdiManager manager = tab.Manager;
			object val;
			if (false == StyleUtils.GetCachedPropertyVal(manager, property, out val))
			{
				// just cache the value
				val = StyleUtils.CachePropertyValue(
					manager,
					property,
					role.BorderStyle,
					null, 
					UIElementBorderStyle.Default,
					UIElementBorderStyle.Default);
			}

			return (UIElementBorderStyle)AppStyling.StyleUtilities.CalculateResolvedValue(
				manager.ComponentRole.ResolutionOrder,
				val,
				controlVal,
				UIElementBorderStyle.Default,
				fallback);
		}
		#endregion //CalculateBorderStyle

		#region CalculateButtonStyle
		internal static UIElementButtonStyle CalculateButtonStyle(MdiTab tab, 
			UIRole role, CachedProperty property, UIElementButtonStyle controlVal,
			UIElementButtonStyle fallback)
		{
			UltraTabbedMdiManager manager = tab.Manager;
			object val;
			if (false == StyleUtils.GetCachedPropertyVal(manager, property, out val))
			{
				val = StyleUtils.CachePropertyValue(
					manager,
					property,
					role.GetButtonStyle(manager.ComponentRole),
					null, 
					UIElementButtonStyle.Default,
					UIElementButtonStyle.Default);
			}

			return (UIElementButtonStyle)AppStyling.StyleUtilities.CalculateResolvedValue(
				manager.ComponentRole.ResolutionOrder,
				val,
				controlVal,
				UIElementButtonStyle.Default,
				fallback);
		}
		#endregion //CalculateButtonStyle

		#region DirtyCachedPropertyVals

		internal static void DirtyCachedPropertyVals( UltraTabbedMdiManager manager )
		{
			ComponentRole role = StyleUtils.GetComponentRole(manager);
			if ( null != role )
				role.ClearCachedPropertyValues( );

			if (null != manager)
			{
				foreach(MdiTabGroup tabGroup in manager.TabGroups.GetNestedTabGroups(false))
					tabGroup.PropertyCache.ClearCachedPropertyValues();
			}
		}

		#endregion // DirtyCachedPropertyVals

		#region GetButtonStyle

		internal static UIElementButtonStyle GetButtonStyle( UltraTabbedMdiManager manager )
		{
			return null != manager && null != manager.ComponentRole ? manager.ComponentRole.ButtonStyle : UIElementButtonStyle.Default;
		}

		#endregion // GetRoleButtonStyle

		#region GetCachedPropertyVal

		internal static bool GetCachedPropertyVal( MdiTabGroup tabGroup, TabGroupCachedProperty prop, out object val )
		{
			return tabGroup.PropertyCache.GetCachedProperty((int)prop, out val);
		}

		internal static bool GetCachedPropertyVal( UltraTabbedMdiManager manager, CustomProperty prop, out object val )
		{
			return GetCachedPropertyVal( manager, (int)prop, out val );
		}

		internal static bool GetCachedPropertyVal( UltraTabbedMdiManager manager, CachedProperty prop, out object val )
		{
			return GetCachedPropertyVal( manager, (int)prop, out val );
		}

		private static bool GetCachedPropertyVal( UltraTabbedMdiManager manager, int prop, out object val )
		{
			ComponentRole role = StyleUtils.GetComponentRole(manager);
			if ( null != role )
				return role.GetCachedProperty( prop, out val );

			val = null;
			return false;
		}

		internal static object GetCachedPropertyVal( UltraTabbedMdiManager manager, CustomProperty prop, 
			object controlVal, object defVal, object resolveVal )
		{
			object val;
			if ( ! GetCachedPropertyVal( manager, prop, out val ) )
				val = CachePropertyValue( manager, prop, controlVal, defVal, resolveVal );

			return val;
		}

		#endregion // GetCachedPropertyVal

		#region GetComponentRole
		internal static ComponentRole GetComponentRole(ISupportAppStyling stylableObject)
		{
			return stylableObject != null ? stylableObject.ComponentRole : null;
		}
		#endregion //GetComponentRole

		#region GetCustomPropertyName

		#region GetCustomPropertyName
		internal static string GetCustomPropertyName( CustomProperty prop )
		{
			object[] props = GetCustomPropertiesInfo();

			return (string)props[ (int)prop * PROPS_STEP + PROPS_PROP_NAME];
		}
		#endregion //GetCustomPropertyName

		#endregion // GetCustomPropertyName

		#region GetCustomPropertiesInfo
		private static object[] GetCustomPropertiesInfo( )
		{
			

			return new object[]
			{
				// Name								Type								Default Value					Converter				ValueConstraint				Description
				"TabStyle",							typeof( TabStyle ),					TabStyle.Default,				null,					null,						"LD_MdiTabGroupSettings_P_TabStyle",

				"HotTrack",							typeof( DefaultableBoolean ),		DefaultableBoolean.Default,		null,					null,						"LD_MdiTabSettings_P_HotTrack",

				"ViewStyle",						typeof( ViewStyle ),				ViewStyle.Default,				null,					null,						"LD_UltraTabbedMdiManager_P_ViewStyle",

				// NOTE: This array must be synchronized with the CustomProperty enum.
				// The order of enum values must correspond to the order in that array.
				// 
			};
		}
		#endregion //GetCustomPropertiesInfo

		#region GetResolutionOrder
		internal static ResolutionOrder GetResolutionOrder(UltraTabbedMdiManager manager)
		{
			return manager != null && manager.ComponentRole != null 
				? manager.ComponentRole.ResolutionOrder
				: ResolutionOrder.ControlOnly;
		}
		#endregion //GetResolutionOrder

		#region GetResolutionOrderInfo
		internal static ResolutionOrderInfo GetResolutionOrderInfo(UltraTabbedMdiManager manager)
		{
			return new ResolutionOrderInfo(GetResolutionOrder(manager));
		}
		#endregion //GetResolutionOrderInfo

		#region GetRoleBorderStyle

		internal static UIElementBorderStyle GetRoleBorderStyle( UltraTabbedMdiManager manager, Role eRole )
		{
			UIRole role = GetRole( manager, eRole );
			return null != role ? role.BorderStyle : UIElementBorderStyle.Default;
		}

		#endregion // GetRoleBorderStyle

		#region GetRoleButtonStyle

		internal static UIElementButtonStyle GetRoleButtonStyle( UltraTabbedMdiManager manager, Role eRole )
		{
			UIRole role = GetRole( manager, eRole );
			return null != role ? role.GetButtonStyle( null ) : UIElementButtonStyle.Default;
		}

		#endregion // GetRoleButtonStyle

		#region GetRoleInfo
		private static object[] GetRoleInfo() 
		{
			// NOTE: The order and number of these items must be in sync with the order of the 
			// items in the Role enum!
			return new Object[] {
									// Role enum					Role Name									Base Role Name						States

									// tab item
									Role.TabItemVertLeft,			MdiUIRoleNames.TabItemVertLeft,				UIRoleNames.TabItemVerticalLeft,	new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Active, RoleState.Selected }, SupportedRoleProperties.ButtonStyle,
									Role.TabItemVertRight,			MdiUIRoleNames.TabItemVertRight,			UIRoleNames.TabItemVerticalRight,	new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Active, RoleState.Selected }, SupportedRoleProperties.ButtonStyle,
									Role.TabItemHorzTop,			MdiUIRoleNames.TabItemHorzTop,				UIRoleNames.TabItemHorizontalTop,	new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Active, RoleState.Selected }, SupportedRoleProperties.ButtonStyle,
									Role.TabItemHorzBottom,			MdiUIRoleNames.TabItemHorzBottom,			UIRoleNames.TabItemHorizontalBottom,new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Active, RoleState.Selected }, SupportedRoleProperties.ButtonStyle,

									// tab item area
									Role.TabItemAreaVertLeft,		MdiUIRoleNames.TabItemAreaVertLeft,			UIRoleNames.TabItemAreaVerticalLeft,new RoleState[] { RoleState.Normal }, SupportedRoleProperties.None,
									Role.TabItemAreaVertRight,		MdiUIRoleNames.TabItemAreaVertRight,		UIRoleNames.TabItemAreaVerticalRight,new RoleState[] { RoleState.Normal }, SupportedRoleProperties.None,
									Role.TabItemAreaHorzTop,		MdiUIRoleNames.TabItemAreaHorzTop,			UIRoleNames.TabItemAreaHorizontalTop,new RoleState[] { RoleState.Normal }, SupportedRoleProperties.None,
									Role.TabItemAreaHorzBottom,		MdiUIRoleNames.TabItemAreaHorzBottom,		UIRoleNames.TabItemAreaHorizontalBottom,new RoleState[] { RoleState.Normal }, SupportedRoleProperties.None,

									// tab client area
									Role.TabClientAreaVert,			MdiUIRoleNames.TabClientAreaVert,			UIRoleNames.TabClientAreaVertical,	new RoleState[] { RoleState.Normal }, SupportedRoleProperties.None,
									Role.TabClientAreaHorz,			MdiUIRoleNames.TabClientAreaHorz,			UIRoleNames.TabClientAreaHorizontal,new RoleState[] { RoleState.Normal }, SupportedRoleProperties.None,

									// splitter bar
									Role.SplitterBar,				MdiUIRoleNames.SplitterBar,					UIRoleNames.SplitterBar,			new RoleState[] { RoleState.Normal }, SupportedRoleProperties.BorderStyle,
									Role.SplitterBarVert,			MdiUIRoleNames.SplitterBarVert,				MdiUIRoleNames.SplitterBar,			new RoleState[] { RoleState.Normal }, SupportedRoleProperties.None,
									Role.SplitterBarHorz,			MdiUIRoleNames.SplitterBarHorz,				MdiUIRoleNames.SplitterBar,			new RoleState[] { RoleState.Normal }, SupportedRoleProperties.None,

									// buttons
									Role.CloseButton,				MdiUIRoleNames.CloseButton,					UIRoleNames.TabCloseButton,			new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Pressed }, SupportedRoleProperties.None,

									// MD 12/18/07
									// Added new roles so appearance of header area and tab close buttons can be styled separately
									Role.TabItemCloseButton,		MdiUIRoleNames.TabItemCloseButton,			MdiUIRoleNames.CloseButton,			new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Pressed }, SupportedRoleProperties.None,
									// MD 12/20/07 - Task 1619
									// Added a new role to distinguish close buttons in selected tabs from other close buttons
									Role.SelectedTabItemCloseButton,MdiUIRoleNames.SelectedTabItemCloseButton,	MdiUIRoleNames.TabItemCloseButton,	new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Pressed }, SupportedRoleProperties.None,
									Role.HeaderAreaCloseButton,		MdiUIRoleNames.HeaderAreaCloseButton,		MdiUIRoleNames.CloseButton,			new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Pressed }, SupportedRoleProperties.None,

									Role.TabListButton,				MdiUIRoleNames.TabListButton,				UIRoleNames.TabListButton,			new RoleState[] { RoleState.Normal, RoleState.HotTracked, RoleState.Pressed }, SupportedRoleProperties.None,
			};
		}
		#endregion //GetRoleInfo

		#region GetRole
		internal static UIRole GetRole( MdiTab tab, Role role )
		{
			ResolutionOrderInfo resOrder;
			return GetRole( tab, role, out resOrder );
		}

		internal static UIRole GetRole( MdiTab tab, Role role, out ResolutionOrderInfo resOrder )
		{
			return GetRole( tab.Manager, role, out resOrder );
		}

		internal static UIRole GetRole( IUltraControl ultraControl, Role role )
		{
			ResolutionOrderInfo resOrder;
			return GetRole( ultraControl, role, out resOrder );
		}

		internal static UIRole GetRole( IUltraControl ultraControl, Role role, out ResolutionOrderInfo resOrder )
		{
			ComponentRole componentRole = ultraControl.ComponentRole;
			resOrder = componentRole.GetResolutionOrderInfo();
			return componentRole.GetRole( (int)role );
		}
		#endregion //GetRole

		#region GetRoleNames
		internal static string[] GetRoleNames()
		{
			object[] ROLES = GetRoleInfo();
			string[] roles = new string[ ROLES.Length / ROLES_STEP ];

			// Loop through the ROLES table (flat static array) and create UIRoleDefinition from it.
			// 
			for ( int i = 0; i < ROLES.Length; i += ROLES_STEP )
			{

				string role = (string)ROLES[i + ROLES_ROLE_NAME ];
				roles[i / ROLES_STEP] = role;
			}

			return roles;
		}
		#endregion //GetRoleNames

		#region GetCustomProperties
		internal static AppStyleProperty[] GetCustomProperties()
		{
			object[] PROPERTIES = GetCustomPropertiesInfo( );			
            
			ArrayList list = new ArrayList( );

			for ( int i = 0; i < PROPERTIES.Length; i += PROPS_STEP )
			{
				string name = (string)PROPERTIES[ PROPS_PROP_NAME + i ];
				Type type = (Type)PROPERTIES[ PROPS_PROP_TYPE + i ];
				object defValue = (object)PROPERTIES[ PROPS_PROP_DEFAULT + i ];
				AppStylePropertyConverter converter = (AppStylePropertyConverter)PROPERTIES[ PROPS_PROP_CONVERTER + i ];
				ValueConstraint constraint = (ValueConstraint)PROPERTIES[ PROPS_PROP_CONSTRAINT + i ];
				string desc = (string)PROPERTIES[ PROPS_PROP_DESC + i ];

				AppStyleProperty prop = new AppStyleProperty( name, type, converter );
				prop.DefaultValue = defValue;
				prop.Constraint = constraint;
				prop.Description = Infragistics.Shared.SR.GetString(desc);
				list.Add( prop );
			}

			return (AppStyleProperty[])list.ToArray( typeof( AppStyleProperty ) );
		}
		#endregion //GetCustomProperties

		#region GetRoleDefinitions
		internal static UIRoleDefinition[] GetRoleDefinitions()
		{
			ArrayList roleDefsList = new ArrayList();
			object[] ROLES = GetRoleInfo();

			// Loop through the ROLES table (flat static array) and create UIRoleDefinition from it.
			// 
			for ( int i = 0; i < ROLES.Length; i += ROLES_STEP )
			{
				Role eRole = (Role)ROLES[ i + ROLES_ROLE_ENUM ];
				string role = (string)ROLES[i + ROLES_ROLE_NAME ];
				string baseRole = (string)ROLES[ i + ROLES_BASE_ROLE_NAME ];
				RoleState[] states = (RoleState[])ROLES[ i + ROLES_STATES ];
				SupportedRoleProperties supportedProps = (SupportedRoleProperties)ROLES[ i + ROLES_SUPPORTED_PROPS ];

				RoleState state = 0;
				for ( int j = 0; j < states.Length; j++ )
					state |= states[ j ];

				roleDefsList.Add( new UIRoleDefinition( role, baseRole, state, supportedProps ) );
			}

			return (UIRoleDefinition[])roleDefsList.ToArray( typeof( UIRoleDefinition ) );
		}
		#endregion //GetRoleDefinitions

		#region GetRoleState
		internal static RoleState GetRoleState(MdiTab.AppearanceResolutionPhase phase)
		{
			RoleState state;

			switch(phase)
			{
				case MdiTab.AppearanceResolutionPhase.Tab:
					state = RoleState.Normal;
					break;
				case MdiTab.AppearanceResolutionPhase.Selected:
					state = RoleState.Selected;
					break;
				case MdiTab.AppearanceResolutionPhase.Active:
					state = RoleState.Active;
					break;
				case MdiTab.AppearanceResolutionPhase.HotTrack:
					state = RoleState.HotTracked;
					break;
				case MdiTab.AppearanceResolutionPhase.Final:
					state = RoleState.Normal;
					break;
				default:
					Debug.Assert(false, "Unexpected tab phase:" + phase.ToString());
					state = RoleState.Normal;
					break;
			}

			return state;
		}
		#endregion //GetRoleState

		// AS 5/2/06
		#region GetViewStyle
		internal static object GetViewStyle(ISupportAppStyling stylableObj, StyleUtils.CustomProperty viewStyleProp)
		{
			ComponentRole role = stylableObj.ComponentRole;
			object val = null;

			if (null != role)
			{
				string propName = GetCustomPropertyName( viewStyleProp );

				if ( role.HasProperty( propName ) )
					val = role.GetProperty( propName );

				if (val == null || object.Equals(val, ViewStyle.Default))
				{
					switch(role.ViewStyle)
					{
						case Infragistics.Win.AppStyling.ViewStyle.Office2003:
							val = ViewStyle.Office2003;
							break;
						case Infragistics.Win.AppStyling.ViewStyle.VisualStudio2005:
							val = ViewStyle.VisualStudio2005;
							break;
						case Infragistics.Win.AppStyling.ViewStyle.Standard:
							val = ViewStyle.Standard;
							break;
                        // MBS 8/2/06 <Office2007 look & feel>
                        case Infragistics.Win.AppStyling.ViewStyle.Office2007:
                            val = ViewStyle.Office2007;
                            break;
					}
				}
			}

			return val;
		}
		#endregion //GetViewStyle

		#endregion //Methods

		#region Enums

		#region Role Enum

		// NOTE: The order and number of these items must be in sync with the order of the 
		// items returned from the GetRoleInfo method!
		internal enum Role
		{
			#region TabItem

			/// <summary>
			/// Represents a single vertically oriented <see cref="MdiTab"/> on the left.
			/// </summary>
			TabItemVertLeft,

			/// <summary>
			/// Represents a single vertically oriented <see cref="MdiTab"/> on the right.
			/// </summary>
			TabItemVertRight,

			/// <summary>
			/// Represents a single horizontally oriented <see cref="MdiTab"/> on top.
			/// </summary>
			TabItemHorzTop,

			/// <summary>
			/// Represents a single horizontally oriented <see cref="MdiTab"/> on the bottom.
			/// </summary>
			TabItemHorzBottom,

			#endregion //TabItem

			#region TabItemArea

			/// <summary>
			/// Represents the background of a vertically oriented MdiTabGroup on the left
			/// </summary>
			TabItemAreaVertLeft,

			/// <summary>
			/// Represents the background of a vertically oriented MdiTabGroup on the right
			/// </summary>
			TabItemAreaVertRight,

			/// <summary>
			/// Represents the background of a horizontally oriented MdiTabGroup on the top
			/// </summary>
			TabItemAreaHorzTop,

			/// <summary>
			/// Represents the background of a horizontally oriented MdiTabGroup on the bottom
			/// </summary>
			TabItemAreaHorzBottom,

			#endregion //TabItemArea

			#region TabClientArea

			/// <summary>
			/// Represents the client area of a vertically oriented MdiTabGroup
			/// </summary>
			TabClientAreaVert,

			/// <summary>
			/// Represents the client area of a horizontally oriented MdiTabGroup
			/// </summary>
			TabClientAreaHorz,

			#endregion //TabClientArea

			#region SplitterBar

			/// <summary>
			/// Represents the tab group splitter bar
			/// </summary>
			SplitterBar,

			/// <summary>
			/// Represents a vertical tab group splitter bar
			/// </summary>
			SplitterBarVert,

			/// <summary>
			/// Represents a horizontal tab group splitter bar
			/// </summary>
			SplitterBarHorz,

			#endregion //SplitterBar

			#region Buttons

			/// <summary>
			/// Represents the tab close button
			/// </summary>
			CloseButton,

			// MD 12/18/07
			// Added new roles so appearance of header area and tab close buttons can be styled separately
			/// <summary>
			/// Represents a tab close button within a tab
			/// </summary>
			TabItemCloseButton,

			// MD 12/20/07 - Task 1619
			// Added a new role to distinguish close buttons in selected tabs from other close buttons
			/// <summary>
			/// Represents a tab close button within the selected tab.
			/// </summary>
			SelectedTabItemCloseButton,

			/// <summary>
			/// Represents a tab close button within the header area.
			/// </summary>
			HeaderAreaCloseButton,

			/// <summary>
			/// Represents the tab list button
			/// </summary>
			TabListButton,

			#endregion //Buttons
		}

		#endregion // Role Enum

		#region CustomProperty
		internal enum CustomProperty
		{
			// NOTE: This enum must be synchronized with the GetCustomPropertiesInfo method.
			// The order of enum values must correspond to the order in that array.
			// 
			TabStyle,

			HotTrack,

			ViewStyle,

			// this should be kept in sync with the last item
			LastValue = ViewStyle,
		}
		#endregion //CustomProperty

		#region CachedProperty
		internal enum CachedProperty
		{
			/// <summary>
			/// [Full] BorderStyle for the splitter bar.
			/// </summary>
			BorderSplitter = 1 + CustomProperty.LastValue,

			// NOTE: If you modify this enum, you must make sure the LastValue member below is
			// intialized to the right value.

			LastValue = BorderSplitter
		}
		#endregion //CachedProperty

		#region TabGroupCachedProperty
		internal enum TabGroupCachedProperty
		{
			TabStyle,
			TabButtonStyle,
			ButtonStyle,
		}
		#endregion //TabGroupCachedProperty

		#endregion //Enums
	}
	#endregion //StyleUtils

	#region UltraWinTabbedMdiAssemblyStyleInfo
	/// <summary>
	/// A class that provides the application styling definitions for the UltraWinTabbedMdi assembly.
	/// </summary>
	public class UltraWinTabbedMdiAssemblyStyleInfo : AssemblyStyleInfo
	{
		/// <summary>
		/// Returns an array of objects that define the roles provided by an assembly.
		/// </summary>
        /// <returns>An array of objects that define the roles.</returns>
		/// <seealso cref="UIRoleDefinition"/>
		public override UIRoleDefinition[] GetRoles()
		{
			return StyleUtils.GetRoleDefinitions();
		}

		/// <summary>
		/// Returns an array of objects that define the component roles provided by an assembly.
		/// </summary>
        /// <returns>An array of objects that define the component roles.</returns>
		public override ComponentRoleDefinition[] GetComponents()
		{            
			ComponentRoleDefinition dockDef = new ComponentRoleDefinition( StyleUtils.UltraTabbedMdiManagerRoleName, typeof( UltraTabbedMdiManager ) );

			ArrayList roles = new ArrayList(StyleUtils.GetRoleNames());

			// add in other roles that the control uses such as 
			// spin buttons for the tab items

			// spin buttons
			roles.Add(UIRoleNames.SpinButton);
			roles.AddRange( UIRoleNames.GetSpinButtonRoles( SpinButtonTypes.NextOrPreviousItem ) );

			// scroll track
			roles.Add(UIRoleNames.ScrollBar);
			roles.Add(UIRoleNames.ScrollBarTrackVertical);
			roles.Add(UIRoleNames.ScrollBarTrackHorizontal);
			roles.Add(UIRoleNames.ScrollBarThumbVertical);
			roles.Add(UIRoleNames.ScrollBarThumbHorizontal);

			dockDef.RelatedRoles = (string[])roles.ToArray(typeof(string));

			dockDef.UsesToolTips = true; // AS 5/4/06

			dockDef.CustomProperties = StyleUtils.GetCustomProperties();
			dockDef.SupportedProperties = 
				SupportedComponentProperties.ButtonStyle	| 
				SupportedComponentProperties.UseFlatMode	| 
				SupportedComponentProperties.UseOsThemes	|
				SupportedComponentProperties.ResolutionOrder;
			return new ComponentRoleDefinition[] { dockDef };
		}

	}
	#endregion //UltraWinTabbedMdiAssemblyStyleInfo
}
