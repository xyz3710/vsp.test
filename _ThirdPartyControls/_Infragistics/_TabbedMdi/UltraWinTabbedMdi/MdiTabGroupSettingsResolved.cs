#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinTabs;
using System.ComponentModel;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Represents the resolved group settings for an <see cref="MdiTabGroup"/>.
	/// </summary>
	/// <remarks>
	/// <p class="body">All of the properties of the <see cref="MdiTabGroupSettings"/> object have default values 
	/// that need to be resolved to determine the actual value. This allows the properties to be set for a 
	/// single tab group (<see cref="MdiTabGroup.Settings"/>) or for all <see cref="UltraTabbedMdiManager.TabGroups"/> 
	/// (<see cref="UltraTabbedMdiManager.TabGroupSettings"/>). The <b>MdiTabGroupSettingsResolved</b> is responsible 
	/// for calculating the resolved values based on which properties are explicitly set on the <b>MdiTabGroupSettings</b> 
	/// object used by the tab group and UltraTabbedMdiManager.</p>
	/// </remarks>
	/// <seealso cref="MdiTabGroupSettings"/>
	/// <seealso cref="UltraTabbedMdiManager.TabGroupSettings"/>
	/// <seealso cref="MdiTabGroup.Settings"/>
	/// <seealso cref="MdiTabGroup.SettingsResolved"/>
	public class MdiTabGroupSettingsResolved
	{
		#region Member Variables

		private MdiTabGroup			tabGroup = null;

		// AS 2/6/04 WTB1314
		private Margins				tabAreaMargins = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupSettingsResolved"/>
		/// </summary>
		/// <param name="tabGroup"><b>MdiTabGroup</b> whose settings will be resolved.</param>
		public MdiTabGroupSettingsResolved(MdiTabGroup tabGroup)
		{
			if (tabGroup == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_13"), Shared.SR.GetString(null, "LE_V2_Exception_24"));

			this.tabGroup = tabGroup;
		}
		#endregion //Constructor

		#region Properties

		#region Manager
		private UltraTabbedMdiManager Manager
		{
			get { return this.tabGroup.Manager; }
		}
		#endregion //Manager

		#region AllowDrop
		/// <summary>
		/// Readonly. Indicates whether an <see cref="MdiTab"/> may be repositioned to the <see cref="MdiTabGroup"/> from a different <see cref="MdiTabGroup"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, tabs from any <see cref="MdiTabGroup"/> may be repositioned within their current <see cref="MdiTab.TabGroup"/> or 
		/// in other tab groups. The <see cref="MdiTabSettings.AllowDrag"/> property is used to limit where a tab may be dragged to or whether an MdiTab may even be dragged. The 
		/// <b>AllowDrop</b> property can be used to limit which tabs may be moved into a particular tab group. Setting this property to false will prevent tabs from 
		/// other MdiTabGroup instances from being added to the tab group's <see cref="MdiTabGroup.Tabs"/> collection.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDragging"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDragOver"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDropped"/>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		public bool AllowDrop
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeAllowDrop())
					return tabGroup.Settings.AllowDrop == DefaultableBoolean.True;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeAllowDrop())
					return manager.TabGroupSettings.AllowDrop == DefaultableBoolean.True;

				return true;
			}
		}
		#endregion //AllowDrop

		#region AutoSelect
		/// <summary>
		/// Readonly. Indicates whether the <see cref="MdiTab"/> will be automatically selected after hovering over the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, <b>AutoSelect</b> resolves to false. When set to true and the mouse is within the bounds of a 
		/// tab for the <see cref="AutoSelectDelay"/> interval, the tab is automatically selected.</p>
		/// </remarks>
		/// <seealso cref="AutoSelectDelay"/>
		/// <seealso cref="MdiTabGroupSettings.AutoSelect"/>
		public bool AutoSelect
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeAutoSelect())
					return tabGroup.Settings.AutoSelect == DefaultableBoolean.True;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeAutoSelect())
					return manager.TabGroupSettings.AutoSelect == DefaultableBoolean.True;

				return false;
			}
		}
		#endregion //AutoSelect

		#region ShowButtonSeparators
		/// <summary>
		/// Readonly. Indicates whether separators will be displayed between button style tabs.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <see cref="TabStyle"/> is resolved to <b>StateButtons</b>, the tabs 
		/// are displayed as state buttons where the selected tab is in the pressed state. By default, there 
		/// is some space between the state button tabs which can be controlled using the <see cref="InterTabSpacing"/>. 
		/// As long as there are a few pixels of space between the state button tabs, a perpendicular line separating 
		/// the tabs will be displayed when this property is set to true (the default value). This can be helpful when the 
		/// <see cref="TabButtonStyle"/> is set to a value that displays the unselected tabs as border less state buttons.</p>
		/// </remarks>
		/// <seealso cref="TabStyle"/>
		/// <seealso cref="TabButtonStyle"/>
		/// <seealso cref="InterTabSpacing"/>
		/// <seealso cref="MdiTabGroupSettings.ShowButtonSeparators"/>
		public bool ShowButtonSeparators
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeShowButtonSeparators())
					return tabGroup.Settings.ShowButtonSeparators == DefaultableBoolean.True;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeShowButtonSeparators())
					return manager.TabGroupSettings.ShowButtonSeparators == DefaultableBoolean.True;

				return true;
			}
		}

		#endregion //ShowButtonSeparators

		#region ShowCloseButton
		/// <summary>
		/// Readonly. Indicates whether a close button will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ShowCloseButton</b> determines whether the close button (displayed as a button with an "X") is displayed in the 
		/// <see cref="MdiTabGroup"/> area. When set to true, the default value, the close button is enabled and disabled based on the <see cref="MdiTabSettings.AllowClose"/> 
		/// state of the selected tab. When clicked, the <see cref="UltraTabbedMdiManager.TabClosing"/> event is invoked, which may be cancelled preventing any 
		/// further action. If the event is not cancelled, the action taken on the associated <see cref="MdiTab.Form"/> is based on the 
		/// <see cref="MdiTabSettings.TabCloseAction"/>. Then the <see cref="UltraTabbedMdiManager.TabClosed"/> event is invoked.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.ShowCloseButton"/>
		/// <seealso cref="MdiTabSettings.TabCloseAction"/>
		/// <seealso cref="MdiTabSettings.AllowClose"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosing"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosed"/>
		/// <seealso cref="MdiTabGroupSettings.CloseButtonAppearance"/>
		/// <seealso cref="ButtonStyle"/>
		// MD 11/7/07 - NA 2008 Vol 1
		// Made obsolete
		[Obsolete( "ShowCloseButton has been deprected, use the CloseButtonLocation property instead" )]
		[EditorBrowsable( EditorBrowsableState.Never )]
		public bool ShowCloseButton
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeShowCloseButton())
					return tabGroup.Settings.ShowCloseButton == DefaultableBoolean.True;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeShowCloseButton())
					return manager.TabGroupSettings.ShowCloseButton == DefaultableBoolean.True;

				return true;
			}
		}

		#endregion //ShowCloseButton

		#region ShowPartialTabs
		//JDN 8/13/04 NAS2004 Vol3
		/// <summary>
		/// Readonly. Indicates whether partial tabs will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ShowPartialTabs</b> property is used to specify whether or not to display an
		///  <see cref="MdiTab"/> if there is insufficent room to display it fully. Setting this property to false
		///  will prevent partial tabs from from being displayed.  The default value is true unless the ViewStyle
		///  is set to VisualStudio2005.</p>
		/// </remarks>
		public bool ShowPartialTabs
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeShowPartialTabs())
					return tabGroup.Settings.ShowPartialTabs == DefaultableBoolean.True;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeShowPartialTabs())
					return manager.TabGroupSettings.ShowPartialTabs == DefaultableBoolean.True;

				// AS 5/2/06 AppStyling
				//return manager.ViewStyle == ViewStyle.VisualStudio2005 ? false : true;
				return manager.ViewStyleResolved == ViewStyle.VisualStudio2005 ? false : true;
			}
		}
		#endregion ShowPartialTabs

		#region ShowTabListButton
		//JDN 8/13/04 NAS2004 Vol3
		/// <summary>
		/// Readonly. Indicates whether the tab list button will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ShowTabListButton</b> property is used to specify whether or not to display
		///  the tab list button. Setting this property to false will prevent the button from being displayed.
		///  The default value is false unless the ViewStyle is set to VisualStudio2005.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.ShowTabListButton"/>
		/// <seealso cref="MdiTabGroupSettings.TabListButtonAppearance"/>
		/// <seealso cref="ButtonStyle"/>
		public bool ShowTabListButton
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeShowTabListButton())
					return tabGroup.Settings.ShowTabListButton == DefaultableBoolean.True;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeShowTabListButton())
					return manager.TabGroupSettings.ShowTabListButton == DefaultableBoolean.True;

				// AS 5/2/06 AppStyling
				//return manager.ViewStyle == ViewStyle.VisualStudio2005 ? true : false;
				return manager.ViewStyleResolved == ViewStyle.VisualStudio2005 ? true : false;
			}
		}
		#endregion ShowTabListButton

		#region AutoSelectDelay
		/// <summary>
		/// Readonly. Indicates how long the mouse must hover over a tab before it is selected when <see cref="AutoSelect"/> is enabled.
		/// </summary>
		/// <remarks>
		/// <p class="body">When <see cref="AutoSelect"/> is set to true, the <b>AutoSelectDelay</b> determines the interval that the mouse 
		/// must be over the tab before it is automatically selected. By default, <b>AutoSelect</b> is not enabled.</p>
		/// </remarks>
		/// <seealso cref="AutoSelect"/>
		/// <seealso cref="MdiTabGroupSettings.AutoSelectDelay"/>
		public int AutoSelectDelay
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeAutoSelectDelay())
					return tabGroup.Settings.AutoSelectDelay;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeAutoSelectDelay())
					return manager.TabGroupSettings.AutoSelectDelay;

				return 500;
			}
		}

		#endregion //AutoSelectDelay

		#region InterTabSpacing
		/// <summary>
		/// Readonly. Returns the amount of space between tabs.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>InterTabSpacing</b> determines the amount of space between the tab items. When set to a negative value, the 
		/// tab items overlap. When set to a positive value, the specified amount of pixels is left between the tab items. When left at the default value,
		/// the space between the tabs is resolved differently based on other settings, such as the <see cref="TabStyle"/>. For example, the default 
		/// <b>InterTabSpacing</b> for <b>Excel</b> style tabs is -8 to allow the tab items to overlap as they do when using Microsoft Excel.</p>
		/// <p class="note">Note: If the <see cref="InterTabSpacing"/> is less than 0, then all tab items must be at least the absolute value of the <b>InterTabSpacing</b>.</p>
		/// </remarks>
		/// <seealso cref="TabStyle"/>
		/// <seealso cref="MdiTabGroupSettings.InterTabSpacing"/>
		public DefaultableInteger InterTabSpacing
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeInterTabSpacing())
					return tabGroup.Settings.InterTabSpacing;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeInterTabSpacing())
					return manager.TabGroupSettings.InterTabSpacing;

				return DefaultableInteger.Default;
			}
		}

		#endregion //InterTabSpacing

		#region MaxTabWidth
		/// <summary>
		/// Readonly. Returns the maximum tab width for the <see cref="MdiTab"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MinTabWidth</b> and <b>MaxTabWidth</b> properties determine the minimum and maximum extents of the tab 
		/// items respectively. By default, the minimum tab width is based on the width required for the borders and there is no maximum 
		/// tab width.</p>
		/// <p class="note">Note: If the <see cref="InterTabSpacing"/> is less than 0, then all tab items must be at least the absolute value of the <b>InterTabSpacing</b>.</p>
		/// </remarks>
		/// <seealso cref="MinTabWidth"/>
		/// <seealso cref="MdiTabSettings.TabWidth"/>
		/// <seealso cref="MdiTabGroupSettings.MaxTabWidth"/>
		public int MaxTabWidth
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeMaxTabWidth())
					return tabGroup.Settings.MaxTabWidth;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeMaxTabWidth())
					return manager.TabGroupSettings.MaxTabWidth;

				return TabManager.MaxTabWidthDefault;
			}
		}
		#endregion //MaxTabWidth
		
		#region MinTabWidth
		/// <summary>
		/// ReadOnly. Returns the minimum width for the <see cref="MdiTab"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MinTabWidth</b> and <b>MaxTabWidth</b> properties determine the minimum and maximum extents of the tab 
		/// items respectively. By default, the minimum tab width is based on the width required for the borders and there is no maximum 
		/// tab width.</p>
		/// <p class="note">Note: If the <see cref="InterTabSpacing"/> is less than 0, then all tab items must be at least the absolute value of the <b>InterTabSpacing</b>.</p>
		/// </remarks>
		/// <seealso cref="MaxTabWidth"/>
		/// <seealso cref="MdiTabSettings.TabWidth"/>
		/// <seealso cref="MdiTabGroupSettings.MinTabWidth"/>
		public int MinTabWidth
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeMinTabWidth())
					return tabGroup.Settings.MinTabWidth;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeMinTabWidth())
					return manager.TabGroupSettings.MinTabWidth;

				return TabManager.MinTabWidthDefault;
			}
		}
		#endregion //MinTabWidth

		#region ScrollTrackExtent
		/// <summary>
		/// ReadOnly. Returns the extent for the scroll track displayed between the scroll buttons
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>ScrollTrackExtent</b> is the width of the scroll track used to navigate between the tab items. The scroll track 
		/// is only displayed when the <b>Thumb</b> enum is included in the <see cref="ScrollButtonTypes"/> property. The <see cref="ScrollButtons"/> 
		/// property is used to determine when the scroll buttons should be displayed. By default, they are only displayed 
		/// when there isn't enough room to display all the tab items.</p>
		/// </remarks>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollTrackAppearance"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollTrackExtent"/>
		public int ScrollTrackExtent
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeScrollTrackExtent())
					return tabGroup.Settings.ScrollTrackExtent;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeScrollTrackExtent())
					return manager.TabGroupSettings.ScrollTrackExtent;

				return TabManager.ScrollTrackExtentDefault;
			}
		}
		#endregion //ScrollTrackExtent

		#region TabHeight
		/// <summary>
		/// Readonly. Returns the height for the <see cref="MdiTab"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabHeight</b> controls the logical height of the tab items. By default, the 
		/// TabHeight is based on the size needed to show the image and text completely within the borders for the tab item, which is affected 
		/// by many property settings (e.g. <see cref="TabStyle"/>, <see cref="TextOrientation"/>, etc). For tabs displayed on the top or bottom, 
		/// the TabHeight affects the displayed height of the control while it affects the displayed width of the tabs when the 
		/// <see cref="TabOrientation"/> is such that the tabs are displayed on the left or right.</p>
		/// </remarks>
		/// <seealso cref="TabOrientation"/>
		/// <seealso cref="TextOrientation"/>
		/// <seealso cref="MdiTabGroupSettings.TabHeight"/>
		public int TabHeight
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeTabHeight())
					return tabGroup.Settings.TabHeight;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeTabHeight())
					return manager.TabGroupSettings.TabHeight;

				return -1;
			}
		}

		#endregion //TabHeight

		#region TabsPerRow
		/// <summary>
		/// Readonly. Returns the number of <see cref="MdiTab"/> instances visible at a time when the 'TabStyle' is set to TabsPerRow.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabsPerRow</b> property is only used when the <see cref="TabStyle"/> resolves to <b>TabsPerRow</b>. When that 
		/// style is used, this property determines the number of fully visible tabs that will equally share the available tab area. For example, 
		/// if there is 100 pixels of available area and <b>TabsPerRow</b> is set to 4, each tab will have a logical width of 25 pixels.</p>
		/// </remarks>
		/// <seealso cref="TabStyle"/>
		/// <seealso cref="MdiTabGroupSettings.TabsPerRow"/>
		public int TabsPerRow
		{
			get
			{
				// AS 6/1/06 BR13248
				//if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeTabsPerRow())
				if (this.tabGroup.HasSettings && tabGroup.Settings.TabsPerRow > 0)
					return tabGroup.Settings.TabsPerRow;

				UltraTabbedMdiManager manager = this.Manager;

				// AS 6/1/06 BR13248
				//if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeTabsPerRow())
				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.TabsPerRow > 0)
					return manager.TabGroupSettings.TabsPerRow;

				return 5;
			}
		}

		#endregion //TabsPerRow

		#region ButtonStyle
		/// <summary>
		/// Readonly. Returns the style of button used to display the scroll and close buttons.
		/// </summary>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="ShowCloseButton"/>
		/// <seealso cref="MdiTabGroupSettings.ButtonStyle"/>
		public UIElementButtonStyle ButtonStyle
		{
			get
			{
				
					object val;

				if (false == StyleUtils.GetCachedPropertyVal(this.tabGroup, StyleUtils.TabGroupCachedProperty.ButtonStyle, out val))
				{
					UltraTabbedMdiManager manager = this.Manager;
					UIElementButtonStyle ctrlValue, fallback;
					ViewStyle viewStyle = manager != null ? manager.ViewStyleResolved : ViewStyle.Standard;

					#region Control Value
					if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeButtonStyle())
						ctrlValue = tabGroup.Settings.ButtonStyle;
					else if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeButtonStyle())
						ctrlValue = manager.TabGroupSettings.ButtonStyle;
					else
						ctrlValue = UIElementButtonStyle.Default;
					#endregion //Control Value

					#region Fallback
					fallback = UIElementButtonStyle.Default;
					// when not set and using an xp style scroll arrow, use an xp
					// style button
                    if (this.ScrollArrowStyle == ScrollArrowStyle.WindowsXP)
                        fallback = UIElementButtonStyle.OfficeXPToolbarButton;
                    else if (viewStyle == ViewStyle.Office2003)
                        fallback = UIElementButtonStyle.Office2003ToolbarButton;
                    else if (viewStyle == ViewStyle.VisualStudio2005)
                        fallback = UIElementButtonStyle.VisualStudio2005Button;
                    // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
                    else if (viewStyle == ViewStyle.Office2007)
                        fallback = UIElementButtonStyle.Office2007RibbonButton;
                    else
                        fallback = UIElementButtonStyle.Default;

					#endregion //Fallback

					#region Cache
					val = StyleUtils.CachePropertyValue(
						this.tabGroup,
						StyleUtils.TabGroupCachedProperty.ButtonStyle,
						StyleUtils.GetButtonStyle(manager),
						ctrlValue, 
						UIElementButtonStyle.Default,
						fallback );
					#endregion //Cache
				}

				return (UIElementButtonStyle)val;			}
		}
		#endregion //ButtonStyle

		#region TabButtonStyle
		/// <summary>
		/// Readonly. Returns the style of button used to display a state button style tab item.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <see cref="TabStyle"/> is resolved to <b>StateButtons</b>, the tabs 
		/// are displayed as state buttons where the selected tab is in the pressed state. The <b>TabButtonStyle</b> 
		/// determines the style of buttons displayed including the tab border style.</p>
		/// </remarks>
		/// <seealso cref="TabStyle"/>
		/// <seealso cref="ShowButtonSeparators"/>
		/// <seealso cref="InterTabSpacing"/>
		/// <seealso cref="MdiTabGroupSettings.TabButtonStyle"/>
		public UIElementButtonStyle TabButtonStyle
		{
			get 
			{
				
				object val;

				if (false == StyleUtils.GetCachedPropertyVal(this.tabGroup, StyleUtils.TabGroupCachedProperty.TabButtonStyle, out val))
				{
					UltraTabbedMdiManager manager = this.Manager;
					UIElementButtonStyle ctrlValue, fallback;

					#region Control Value
					if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeTabButtonStyle())
						 ctrlValue = tabGroup.Settings.TabButtonStyle;
					else if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeTabButtonStyle())
						ctrlValue = manager.TabGroupSettings.TabButtonStyle;
					else
						ctrlValue = UIElementButtonStyle.Default;
					#endregion //Control Value

					#region Fallback
					fallback = UIElementButtonStyle.Default;
					#endregion //Fallback

					#region Cache
					val = StyleUtils.CachePropertyValue(this.tabGroup,
						StyleUtils.TabGroupCachedProperty.TabButtonStyle,
						this.tabGroup.UIRoleTabItemDefault.GetButtonStyle(StyleUtils.GetComponentRole(manager)), 
						ctrlValue, UIElementButtonStyle.Default, fallback );
					#endregion //Cache
				}

				return (UIElementButtonStyle)val;			
			}
		}

		#endregion //TabButtonStyle

		#region ScrollArrowStyle
		/// <summary>
		/// Readonly. Returns the style of scroll buttons displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ScrollArrowStyle</b> determines the display of the arrows in the first/last, next/previous and next 
		/// page / previous page buttons. The <see cref="ScrollButtonTypes"/> property is used to determine which scroll buttons should be displayed. 
		/// The <see cref="ScrollButtons"/> property determines when scroll buttons should be displayed. If scroll buttons are set to always be displayed, 
		/// they will appear disabled when no scrolling can occur. The <b>WindowsXP</b> style uses the Windows Xp style chevrons seen in the spin and scroll buttons. 
		/// The <b>Standard</b> and <b>VisualStudio</b> settings are displayed as triangles indicating the direction of the scroll. The difference being that 
		/// <b>VisualStudio</b> style scroll arrows are not filled when they are disabled as they appear in Visual Studio .Net.</p>
		/// </remarks>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="ButtonStyle"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollArrowStyle"/>
		public ScrollArrowStyle ScrollArrowStyle
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeScrollArrowStyle())
					return tabGroup.Settings.ScrollArrowStyle;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeScrollArrowStyle())
					return manager.TabGroupSettings.ScrollArrowStyle;

				return ScrollArrowStyle.VisualStudio;
			}
		}

		#endregion //ScrollArrowStyle

		#region ScrollButtons
		/// <summary>
		/// Readonly. Returns whether scroll buttons will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ScrollButtons</b> property is used to determine if and when scroll 
		/// buttons should be displayed. By default, scroll buttons are only displayed when there isn't 
		/// enough room to display all the tab items. The size of the tab items is based on multiple factors 
		/// including the <see cref="MdiTabSettings.TabWidth"/>, <see cref="TabSizing"/>, <see cref="MinTabWidth"/> and 
		/// <see cref="MaxTabWidth"/> properties.</p>
		/// </remarks>
		/// <seealso cref="ScrollArrowStyle"/>
		/// <seealso cref="ScrollButtonTypes"/>
		/// <seealso cref="ButtonStyle"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtons"/>
		public TabScrollButtons ScrollButtons
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeScrollButtons())
					return Utilities.ToTabScrollButtons(tabGroup.Settings.ScrollButtons);

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeScrollButtons())
					return Utilities.ToTabScrollButtons(manager.TabGroupSettings.ScrollButtons);

				// AS 5/2/06 AppStyling
				ViewStyle viewStyle = manager != null ? manager.ViewStyleResolved : ViewStyle.Standard;

				// MBS 3/27/06 BR11033
                // Do not show scroll buttons if not explicitly requested with VS2005 viewstyle
                if (viewStyle == ViewStyle.VisualStudio2005)
                    return TabScrollButtons.None;
                
				return TabScrollButtons.Automatic;
			}
		}

		#endregion //ScrollButtons

		#region ScrollButtonTypes
		/// <summary>
		/// ReadOnly. Returns the types of scroll buttons to display.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>ScrollButtonTypes</b> is a flag type enumeration (multiple values may be combined using an OR operator) that determines which scroll buttons are displayed. When the scroll buttons are displayed is based 
		/// on the <see cref="ScrollButtons"/> property.</p>
		/// <list type="table">
		/// <item><term>Default</term><description>The value is not set and will be resolved</description></item>
		/// <item><term>None</term><description>No scroll buttons will be displayed</description></item>
		/// <item><term>NextPrevious</term><description>Buttons to navigate to the next and previous item will be displayed</description></item>
		/// <item><term>NextPagePreviousPage</term><description>Buttons to scroll forward and backward by one page will be displayed</description></item>
		/// <item><term>FirstLast</term><description>Buttons to scroll the first and last item into view will be displayed</description></item>
		/// <item><term>Thumb</term><description>A scroll track and scroll thumb are displayed. This is similar to a scroll bar without any scroll buttons.</description></item>
		/// </list>
		/// </remarks>
		/// <seealso cref="ScrollButtons"/>
		/// <seealso cref="ScrollArrowStyle"/>
		/// <seealso cref="ButtonStyle"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtonTypes"/>
		public ScrollButtonTypes ScrollButtonTypes
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeScrollButtonTypes())
					return tabGroup.Settings.ScrollButtonTypes;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeScrollButtonTypes())
					return manager.TabGroupSettings.ScrollButtonTypes;

				return ScrollButtonTypes.Default;
			}
		}

		#endregion //ScrollButtonTypes

		#region TabOrientation
		/// <summary>
		/// Readonly. Returns the orientation of the tabs in the <see cref="MdiTabGroup"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabOrientation</b> property determines the alignment and order in which the tabs are layed out. By default, the value 
		/// resolves to TopLeft which means that the tabs are aligned on the top and are laid out from left to right.</p>
		/// </remarks>
		/// <seealso cref="TextOrientation"/>
		/// <seealso cref="MdiTabGroupSettings.TabOrientation"/>
		public TabOrientation TabOrientation
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeTabOrientation())
					return tabGroup.Settings.TabOrientation;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeTabOrientation())
					return manager.TabGroupSettings.TabOrientation;

				return TabOrientation.TopLeft;
			}
		}

		#endregion //TabLocation

		#region TextOrientation
		/// <summary>
		/// Readonly. Indicates how the tab text is aligned.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TextOrientation</b> determines the orientation of the text in the tab. The orientation may be specified 
		/// relative to the <see cref="TabOrientation"/> or to an absolute value. By default, the text orientation is resolved based on 
		/// the resolved <b>TabOrientation</b> to keep the text parallel with the tab orientation.</p>
		/// </remarks>
		/// <seealso cref="TabOrientation"/>
		/// <seealso cref="MdiTabGroupSettings.TextOrientation"/>
		public TextOrientation TextOrientation
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeTextOrientation())
					return tabGroup.Settings.TextOrientation;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeTextOrientation())
					return manager.TabGroupSettings.TextOrientation;

				return TextOrientation.Default;
			}
		}

		#endregion //TextOrientation

		#region TabPadding
		/// <summary>
		/// Readonly. Returns the amount of padding around the image and text of the <see cref="MdiTab"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>TabPadding</b> is the amount of space between the contents of the tab (the image and text) and the borders of the tab.</p>
		/// <p class="body">The Width and Height may be set on different levels. In other words, you may set the Width of the <b>TabPadding</b> of the 
		/// <see cref="MdiTabGroup.Settings"/> to 3 but leave the Height set to -1 and the Height of the <see cref="UltraTabbedMdiManager.TabGroupSettings"/> 
		/// <b>TabPadding</b> will be used if specified.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.TabPadding"/>
		public Size TabPadding
		{
			get
			{
				int width = -1;
				int height = -1;

				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeTabPadding())
				{
					Size size = tabGroup.Settings.TabPadding;

					if (size.Width >= 0)
						width = size.Width;

					if (size.Height >= 0)
						height = size.Height;
				}

				if (width < 0 || height < 0)
				{					

					UltraTabbedMdiManager manager = this.Manager;

					if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeTabPadding())
					{
						Size size = manager.TabGroupSettings.TabPadding;

						if (width < 0 && size.Width >= 0)
							width = size.Width;

						if (height < 0 && size.Height >= 0)
							height = size.Height;
					}
				}

				if (width < 0)
				{
					// AS 9/17/04 WTB1648
					if (this.TabStyle == TabStyle.PropertyPage2003)
						width = 0;
					else
						width = 1;
				}

				if (height < 0)
				{
					// AS 9/17/04 WTB1648
					if (this.TabStyle == TabStyle.PropertyPage2003)
						height = 0;
					else
						height = 1;
				}

				return new Size(width,height);
			}
		}

		#endregion //TabPadding

		#region TabSizing
		/// <summary>
		/// ReadOnly. Returns the resolved tab sizing mode used to display the tab items.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>TabSizing</b> determines how the size of the tab items are calculated. </p>	
		/// <list type="table">
		/// <item><term>AutoSize</term><description>The tab size is based on the size required to show the image and text.</description></item>
		/// <item><term>Fixed</term><description>The tab size is based on the <see cref="MdiTabSettings.TabWidth"/> if resolved to a value, 
		/// otherwise the <b>AutoSize</b> value is used.</description></item>
		/// <item><term>Justified</term><description>This sizing mode only affects the tabs when there is not enough room to display all the tab items. Each tab item's size is reduced so that all the tab items are in view.</description></item>
		/// <item><term>SizeToFit</term><description>Increases or decreases the size of the tab so that they occupy the entire available area.</description></item>
		/// <item><term>Compress</term><description>The non selected tabs are sized to just enough space to display the associated image. The selected tab is sized based on the largest image and text area.</description></item>
		/// <item><term>TabsPerRow</term><description>The size of each tab is a percentage of the available area based on the <see cref="TabsPerRow"/> setting.</description></item>
		/// </list>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.TabSizing"/>
		/// <seealso cref="TabsPerRow"/>
		/// <seealso cref="MdiTabSettings.TabWidth"/>
		public TabSizing TabSizing
		{
			get
			{
				if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeTabSizing())
					return tabGroup.Settings.TabSizing;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeTabSizing())
					return manager.TabGroupSettings.TabSizing;

				return TabSizing.AutoSize;
			}
		}
		#endregion //TabSizing

		#region TabStyle
		/// <summary>
		/// ReadOnly. Returns the resolved tab style used to display the tab items.
		/// </summary>
		/// <remarks>
		/// <p class="body">Determines the display style for the tab items. The default resolved value is VisualStudio so that the tabs appear as 
		/// the mdi tabs appear in Visual Studio .Net.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.TabStyle"/>
		public TabStyle TabStyle
		{
			get
			{
				
				// AS 4/28/06
				// Reorganized property caching.
				//
				object val;

				if (false == StyleUtils.GetCachedPropertyVal(this.tabGroup, StyleUtils.TabGroupCachedProperty.TabStyle, out val))
				{
					UltraTabbedMdiManager manager = this.Manager;
					TabStyle ctrlValue, fallback;

					#region Control Value
					if (this.tabGroup.HasSettings && tabGroup.Settings.ShouldSerializeTabStyle())
						ctrlValue = tabGroup.Settings.TabStyle;
					else if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeTabStyle())
						ctrlValue = manager.TabGroupSettings.TabStyle;
					else
						ctrlValue = TabStyle.Default;
					#endregion //Control Value

					#region Fallback
                    if (manager != null && manager.ViewStyleResolved == ViewStyle.VisualStudio2005)
                        fallback = TabStyle.PropertyPage2003;
                    // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
                    else if (manager != null && manager.ViewStyleResolved == ViewStyle.Office2007)
                        fallback = TabStyle.Office2007Ribbon;
                    else
                        fallback = TabStyle.VisualStudio;
					#endregion //Fallback

					#region Cache
					val = StyleUtils.CachePropertyValue(this.tabGroup, 
						StyleUtils.TabGroupCachedProperty.TabStyle,
						StyleUtils.CustomProperty.TabStyle,
						ctrlValue, TabStyle.Default, fallback );
					#endregion //Cache
				}

				return (TabStyle)val;
			}
		}
		#endregion //TabStyle

		// JAS 12/20/04 Tab Spacing
		#region SpaceAfterTabs

		/// <summary>
		/// Returns the amount of space between the edge of the control and the first tab.
		/// If a non-default value has been specified then that value is returned, else DefaultableInteger.Default is returned.
		/// This property does not fully resolve the value due to lack of context.
		/// </summary>
		public DefaultableInteger SpaceAfterTabs
		{
			get
			{
				if( this.tabGroup.HasSettings && this.tabGroup.Settings.ShouldSerializeSpaceAfterTabs() )
					return this.tabGroup.Settings.SpaceAfterTabs;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeSpaceAfterTabs())
					return manager.TabGroupSettings.SpaceAfterTabs;

				return DefaultableInteger.Default;
			}
		}

		#endregion // SpaceAfterTabs

		// JAS 12/20/04 Tab Spacing
		#region SpaceBeforeTabs
		
		/// <summary>
		/// Returns the minimum amount of space between the edge of the control and the last tab.
		/// If a non-default value has been specified then that value is returned, else DefaultableInteger.Default is returned.
		/// This property does not fully resolve the value due to lack of context.
		/// </summary>
		public DefaultableInteger SpaceBeforeTabs
		{
			get
			{
				if( this.tabGroup.HasSettings && this.tabGroup.Settings.ShouldSerializeSpaceBeforeTabs() )
					return this.tabGroup.Settings.SpaceBeforeTabs;

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeSpaceBeforeTabs())
					return manager.TabGroupSettings.SpaceBeforeTabs;

				return DefaultableInteger.Default;
			}
		}

		#endregion // SpaceBeforeTabs

		// AS 2/6/04 WTB1314
		#region Margins
		/// <summary>
		/// ReadOnly. Returns the amount of space between the edges of the <see cref="MdiTabGroupControl"/> and its contents.
		/// </summary>
		/// <seealso cref="MdiTabGroupSettings.TabAreaMargins"/>
		public Infragistics.Win.Margins TabAreaMargins
		{
			get
			{
				if (this.tabAreaMargins == null)
					this.tabAreaMargins = new Margins(-1,-1,-1,-1,false);
				else
					this.tabAreaMargins.Reset();

				if (this.tabGroup.HasSettings &&
					this.tabGroup.Settings.ShouldSerializeTabAreaMargins())
					MergeMargins(this.tabAreaMargins, this.tabGroup.Settings.TabAreaMargins);

				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null &&
					manager.HasTabGroupSettings &&
					manager.TabGroupSettings.ShouldSerializeTabAreaMargins())
					MergeMargins(this.tabAreaMargins, manager.TabGroupSettings.TabAreaMargins);

				// see which side gets the extra height by default
				TabOrientation tabOrientation = this.tabGroup.SettingsResolved.TabOrientation;

				TabStyle tabStyle = this.tabGroup.SettingsResolved.TabStyle;

				#region Defaults
				if (!this.tabAreaMargins.ShouldSerializeLeft())
				{
					if ((tabOrientation == TabOrientation.LeftBottom ||
						tabOrientation == TabOrientation.LeftTop) &&
						tabStyle != TabStyle.Wizard)
						this.tabAreaMargins.Left = TabGroupInfoManager.DefaultTabGroupPadding;
					else
						this.tabAreaMargins.Left = 0;
				}

				if (!this.tabAreaMargins.ShouldSerializeRight())
				{
					if ((tabOrientation == TabOrientation.RightBottom ||
						tabOrientation == TabOrientation.RightTop) &&
						tabStyle != TabStyle.Wizard)
						this.tabAreaMargins.Right = TabGroupInfoManager.DefaultTabGroupPadding;
					else
						this.tabAreaMargins.Right = 0;
				}

				if (!this.tabAreaMargins.ShouldSerializeTop())
				{
					if ((tabOrientation == TabOrientation.TopLeft ||
						tabOrientation == TabOrientation.TopRight) &&
						tabStyle != TabStyle.Wizard)
						this.tabAreaMargins.Top = TabGroupInfoManager.DefaultTabGroupPadding;
					else
						this.tabAreaMargins.Top = 0;
				}

				if (!this.tabAreaMargins.ShouldSerializeBottom())
				{
					if ((tabOrientation == TabOrientation.BottomLeft||
						tabOrientation == TabOrientation.BottomRight) &&
						tabStyle != TabStyle.Wizard)
						this.tabAreaMargins.Bottom = TabGroupInfoManager.DefaultTabGroupPadding;
					else
						this.tabAreaMargins.Bottom = 0;
				}
				#endregion //Defaults

				return this.tabAreaMargins;
			}
		}
		#endregion //Margins

		// MD 11/7/07 - NA 2008 Vol 1
		#region CloseButtonLocation

		/// <summary>
		/// Gets the value which determines where the close button is displayed.
		/// </summary>
		public TabCloseButtonLocation CloseButtonLocation
		{
			get
			{
				if ( this.tabGroup.HasSettings && this.tabGroup.Settings.ShouldSerializeCloseButtonLocation() )
					return this.tabGroup.Settings.CloseButtonLocation;

				UltraTabbedMdiManager manager = this.Manager;

				if ( manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.ShouldSerializeCloseButtonLocation() )
					return manager.TabGroupSettings.CloseButtonLocation;

				return TabCloseButtonLocation.HeaderArea;
			}
		}

		#endregion CloseButtonLocation

		#endregion //Properties

		#region Methods

		// AS 2/6/04 WTB1314
		#region MergeMargins
		private static void MergeMargins(Infragistics.Win.Margins destination, Infragistics.Win.Margins source)
		{
			if (!destination.ShouldSerializeLeft() && source.ShouldSerializeLeft())
				destination.Left = source.Left;

			if (!destination.ShouldSerializeRight() && source.ShouldSerializeRight())
				destination.Right = source.Right;

			if (!destination.ShouldSerializeTop() && source.ShouldSerializeTop())
				destination.Top = source.Top;

			if (!destination.ShouldSerializeBottom() && source.ShouldSerializeBottom())
				destination.Bottom = source.Bottom;
		}
		#endregion //MergeMargins

		#endregion //Methods
	}

}
