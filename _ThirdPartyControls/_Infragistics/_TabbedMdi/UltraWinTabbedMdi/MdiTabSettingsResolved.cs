#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using Infragistics.Win.UltraWinTabs;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Represents the resolved tab item settings for an <see cref="MdiTab"/>.
	/// </summary>
	/// <remarks>
	/// <p class="body">All of the properties of the <see cref="MdiTabSettings"/> object have default values 
	/// that need to be resolved to determine the actual value. This allows the properties to be set for a 
	/// single tab (<see cref="MdiTab.Settings"/>), all the tabs in a particular <see cref="MdiTabGroup"/> (<see cref="MdiTabGroup.TabSettings"/>), 
	/// or for all tabs (<see cref="UltraTabbedMdiManager.TabSettings"/>).  The <b>MdiTabSettingsResolved</b> is responsible 
	/// for calculating the resolved values based on which properties are explicitly set on the <b>MdiTabSettings</b> 
	/// object used by the tab, containing tab group and UltraTabbedMdiManager.</p>
	/// </remarks>
	/// <seealso cref="MdiTabSettings"/>
	/// <seealso cref="UltraTabbedMdiManager.TabSettings"/>
	/// <seealso cref="MdiTabGroup.Settings"/>
	/// <seealso cref="MdiTab.Settings"/>
	/// <seealso cref="MdiTab.SettingsResolved"/>
	public class MdiTabSettingsResolved
	{
		#region Member Variables

		private MdiTab			tab = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabSettingsResolved"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> whose settings will be resolved.</param>
		public MdiTabSettingsResolved(MdiTab tab)
		{
			if (tab == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_28"), Shared.SR.GetString(null, "LE_V2_Exception_29"));

			this.tab = tab;
		}
		#endregion //Constructor

		#region Properties

		#region TabGroup
		private MdiTabGroup TabGroup
		{
			get	{ return this.tab.TabGroup;	}
		}
		#endregion //TabGroup

		#region AllowClose
		/// <summary>
		/// Readonly. Indicates whether the <see cref="MdiTab"/> may be closed.
		/// </summary>
		/// <remarks>
		/// <p class="body">An <see cref="MdiTab"/> can be closed 2 ways using the user interface of the <see cref="UltraTabbedMdiManager"/>. 
		/// One way is using the close button (appears as an "X" in the <see cref="MdiTabGroup"/>) and the other is via the context menu displayed when 
		/// right clicking on a tab. The close button displayed in the MdiTabGroup may be hidden by setting the <see cref="MdiTabGroupSettings.ShowCloseButton"/> to false but
		/// the option in the context menu will still be available. If the <b>AllowClose</b> resolves to false (true is the default value) the option in 
		/// the context menu will not be displayed and the close button (if displayed) will be disabled when the tab is the selected tab. Regardless of the 
		/// <b>AllowClose</b> setting, the tab may be closed using the <see cref="MdiTab.Close()"/> method.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowClose"/>
		/// <seealso cref="TabCloseAction"/>
		/// <seealso cref="MdiTabGroupSettings.ShowCloseButton"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosing"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosed"/>
		public bool AllowClose
		{
			get 
			{
				if (this.tab.HasSettings && this.tab.Settings.ShouldSerializeAllowClose())
					return this.tab.Settings.AllowClose == DefaultableBoolean.True;

				MdiTabGroup tabGroup = this.TabGroup;

				// AS 4/20/05 BR03471
				//if (tabGroup != null && tabGroup.HasSettings && tabGroup.TabSettings.ShouldSerializeAllowClose())
				if (tabGroup != null && tabGroup.HasTabSettings && tabGroup.TabSettings.ShouldSerializeAllowClose())
					return tabGroup.TabSettings.AllowClose == DefaultableBoolean.True;

				UltraTabbedMdiManager manager = tabGroup == null ? null : tabGroup.Manager;

				if (manager != null && manager.HasTabSettings && manager.TabSettings.ShouldSerializeAllowClose())
					return manager.TabSettings.AllowClose == DefaultableBoolean.True;

				// let the custom tab have the final say
				if (tab.IsCustomTab)
					return tab.CustomMdiTab.AllowClose;

				return true;
			}
		}
		#endregion //AllowClose

		#region HotTrack
		/// <summary>
		/// Readonly. Indicates whether the <see cref="MdiTab"/> will display using the <see cref="MdiTabSettings.HotTrackTabAppearance"/> when the mouse is over the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>HotTrack</b> property determines if the <see cref="MdiTabSettings.HotTrackTabAppearance"/> is applied to the tab 
		/// when the mouse enters the bounds of the tab item. By default, this value resolves to false when the <see cref="UltraTabbedMdiManager.ViewStyle"/> property is set to 'Standard' or 'VisualStudio2005', and true when it is set to 'Office2003'.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.HotTrack"/>
		public bool HotTrack
		{
			get 
			{
				#region Old Code
				
				#endregion //Old Code

				#region Setup

				MdiTabGroup tabGroup = this.TabGroup;
				UltraTabbedMdiManager manager = tabGroup == null ? null : tabGroup.Manager;
				AppStyling.ResolutionOrder order = StyleUtils.GetResolutionOrder(manager);
				DefaultableBoolean ctrlValue, fallback;
				object val;

				#endregion //Setup

				#region Control Value
				if (this.tab.HasSettings && this.tab.Settings.ShouldSerializeHotTrack())
					ctrlValue = this.tab.Settings.HotTrack;
				else if (tabGroup != null && tabGroup.HasTabSettings && tabGroup.TabSettings.ShouldSerializeHotTrack())
					ctrlValue = tabGroup.TabSettings.HotTrack;
				else if (manager != null && manager.HasTabSettings && manager.TabSettings.ShouldSerializeHotTrack())
					ctrlValue = manager.TabSettings.HotTrack;
				else if (this.tab.IsCustomTab && this.tab.CustomMdiTab.HotTrack != DefaultableBoolean.Default)
					ctrlValue = this.tab.CustomMdiTab.HotTrack;
				else
					ctrlValue = DefaultableBoolean.Default;
				#endregion //Control Value

				#region Fallback
				// AS 5/2/06 AppStyling
				ViewStyle viewStyle = manager != null ? manager.ViewStyleResolved : ViewStyle.Standard;

                if (viewStyle == ViewStyle.Office2003)
                    fallback = DefaultableBoolean.True;
                else if (viewStyle == ViewStyle.VisualStudio2005)
                    fallback = DefaultableBoolean.False;
                // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
                else if (viewStyle == ViewStyle.Office2007)
                    fallback = DefaultableBoolean.True;
                else
                    fallback = DefaultableBoolean.False;
				#endregion //Fallback

				#region Cache Value
				if ( false == StyleUtils.GetCachedPropertyVal( manager, StyleUtils.CustomProperty.HotTrack, out val ) )
				{
					val = StyleUtils.CacheUnresolvedPropertyValue( manager, 
						StyleUtils.CustomProperty.HotTrack,
						DefaultableBoolean.Default
						);
				}
				#endregion //Cache Value

				#region Calculate Resolved Value
				DefaultableBoolean resolvedVal = (DefaultableBoolean)AppStyling.StyleUtilities.CalculateResolvedValue(
					order,
					val,
					ctrlValue,
					DefaultableBoolean.Default,
					fallback);
				#endregion //Calculate Resolved Value

				return resolvedVal == DefaultableBoolean.True;
			}
		}
		#endregion //HotTrack

		#region AllowDrag
		/// <summary>
		/// Readonly. Indicates whether the <see cref="MdiTab"/> may be repositioned.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>AllowDrag</b> property determines whether an <see cref="MdiTab"/> may be repositioned. By default, an MdiTab may be 
		/// repositioned within its containing <see cref="MdiTab.TabGroup"/> and also to the other <see cref="MdiTabGroup"/> objects in the 
		/// <see cref="UltraTabbedMdiManager.TabGroups"/>. When set to <b>WithinTabGroup</b>, the MdiTab may only be repositioned within its 
		/// containing TabGroup; it cannot be dragged to another TabGroup and cannot be used to create a new 
		/// <see cref="MdiTabGroup"/>. When set to <b>None</b>, the MdiTab cannot be dragged.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDragging"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDragOver"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDropped"/>
		public MdiTabDragStyle AllowDrag
		{
			get
			{
				if (this.tab.HasSettings && this.tab.Settings.ShouldSerializeAllowDrag())
					return this.tab.Settings.AllowDrag;

				MdiTabGroup tabGroup = this.TabGroup;

				// AS 4/20/05 BR03471
				//if (tabGroup != null && tabGroup.HasSettings && tabGroup.TabSettings.ShouldSerializeAllowDrag())
				if (tabGroup != null && tabGroup.HasTabSettings && tabGroup.TabSettings.ShouldSerializeAllowDrag())
					return tabGroup.TabSettings.AllowDrag;

				UltraTabbedMdiManager manager = tabGroup == null ? null : tabGroup.Manager;

				if (manager != null && manager.HasTabSettings && manager.TabSettings.ShouldSerializeAllowDrag())
					return manager.TabSettings.AllowDrag;

				// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
				// When maximized, we will only allow dragging within the 
				// current group.
				//
				if (manager != null	&& manager.IsActiveTabGroupMaximized)
					return MdiTabDragStyle.WithinGroup;

				return MdiTabDragStyle.WithinAndAcrossGroups;
			}
		}

		#endregion //AllowDrag

		#region TabCloseAction
		/// <summary>
		/// Readonly. Indicates what happens to the associated <see cref="System.Windows.Forms.Form"/> when the <see cref="MdiTab"/> is closed.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a tab is closed, using the <see cref="MdiTab.Close()"/> method or via the user interface (using the 
		/// close button or context menu), the <b>TabCloseAction</b> determines the action that will be taken on the <see cref="MdiTab.Form"/> associated 
		/// with the tab. By default the associated Form is closed. When a form is closed, the tab associated with the form is also released. If the form is 
		/// only hidden, the MdiTab is removed from the <see cref="MdiTab.TabGroup"/> it is associated with and added to the <see cref="UltraTabbedMdiManager.HiddenTabs"/> collection of 
		/// the <see cref="UltraTabbedMdiManager"/>. When set to <b>None</b>, no action is taken but the <see cref="UltraTabbedMdiManager.TabClosing"/> and 
		/// <see cref="UltraTabbedMdiManager.TabClosed"/> events are still invoked.</p>
		/// </remarks>
		/// <seealso cref="AllowClose"/>
		/// <seealso cref="MdiTabGroupSettings.ShowCloseButton"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosing"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosed"/>
		/// <seealso cref="MdiTabSettings.TabCloseAction"/>
		public MdiTabCloseAction TabCloseAction
		{
			get 
			{
				// the close behavior for a custom tab is ALWAYS left 
				// completely up to the custom tab
				if (this.tab.IsCustomTab)
					return MdiTabCloseAction.None;

				if (this.tab.HasSettings && this.tab.Settings.ShouldSerializeTabCloseAction())
					return this.tab.Settings.TabCloseAction;

				MdiTabGroup tabGroup = this.TabGroup;

				// AS 4/20/05 BR03471
				//if (tabGroup != null && tabGroup.HasSettings && tabGroup.TabSettings.ShouldSerializeTabCloseAction())
				if (tabGroup != null && tabGroup.HasTabSettings && tabGroup.TabSettings.ShouldSerializeTabCloseAction())
					return tabGroup.TabSettings.TabCloseAction;

				UltraTabbedMdiManager manager = tabGroup == null ? null : tabGroup.Manager;

				if (manager != null && manager.HasTabSettings && manager.TabSettings.ShouldSerializeTabCloseAction())
					return manager.TabSettings.TabCloseAction;

				return MdiTabCloseAction.Close;
			}
		}
		#endregion //TabCloseAction

		#region TabWidth
		/// <summary>
		/// Readonly. Indicates the <see cref="MdiTab"/> width when displayed as a fixed width tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabWidth</b> property is used to control the width of a tab item when 
		/// the <see cref="MdiTabGroupSettings.TabSizing"/> is set to either <b>Fixed</b> or <b>AutoSize</b>. 
		/// If the property does not resolve to a specific value and the <b>TabSizing</b> of the containing group 
		/// is set to <b>Fixed</b>, the tab defaults to the size required to show the image and text. The tab size is 
		/// also limited by the <see cref="MdiTabGroupSettings.MinTabWidth"/> and <see cref="MdiTabGroupSettings.MaxTabWidth"/>.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.TabSizing"/>
		/// <seealso cref="MdiTabGroupSettings.MinTabWidth"/>
		/// <seealso cref="MdiTabGroupSettings.MaxTabWidth"/>
		/// <seealso cref="MdiTabSettings.TabWidth"/>
		public int TabWidth
		{
			get
			{
				if (this.tab.HasSettings && this.tab.Settings.ShouldSerializeTabWidth())
					return this.tab.Settings.TabWidth;

				MdiTabGroup tabGroup = this.TabGroup;

				// AS 4/20/05 BR03471
				//if (tabGroup != null && tabGroup.HasSettings && tabGroup.TabSettings.ShouldSerializeTabWidth())
				if (tabGroup != null && tabGroup.HasTabSettings && tabGroup.TabSettings.ShouldSerializeTabWidth())
					return tabGroup.TabSettings.TabWidth;

				UltraTabbedMdiManager manager = tabGroup == null ? null : tabGroup.Manager;

				if (manager != null && manager.HasTabSettings && manager.TabSettings.ShouldSerializeTabWidth())
					return manager.TabSettings.TabWidth;

				if (this.tab.IsCustomTab)
					return this.tab.CustomMdiTab.TabWidth;

				return -1;
			}
		}

		#endregion //TabWidth

		#region DisplayFormIcon
		/// <summary>
		/// ReadOnly. Returns whether the <see cref="MdiTab"/> will display the associated <see cref="MdiTab.Form"/> icon if no other image is assigned to the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, an <see cref="MdiTab"/> will only display an image if one is specified on one of the tab appearances 
		/// (<see cref="MdiTabSettings.TabAppearance"/>, <see cref="MdiTabSettings.SelectedTabAppearance"/>, etc.). To have the tab display the Icon of the associated 
		/// <see cref="MdiTab.Form"/>, set the <b>DisplayForm</b> icon property to true.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.DisplayFormIcon"/>
		public bool DisplayFormIcon
		{
			get
			{
				if (this.tab.HasSettings && this.tab.Settings.ShouldSerializeDisplayFormIcon())
					return this.tab.Settings.DisplayFormIcon == DefaultableBoolean.True;

				MdiTabGroup tabGroup = this.TabGroup;

				// AS 4/20/05 BR03471
				//if (tabGroup != null && tabGroup.HasSettings && tabGroup.TabSettings.ShouldSerializeDisplayFormIcon())
				if (tabGroup != null && tabGroup.HasTabSettings && tabGroup.TabSettings.ShouldSerializeDisplayFormIcon())
					return tabGroup.TabSettings.DisplayFormIcon == DefaultableBoolean.True;

				UltraTabbedMdiManager manager = tabGroup == null ? null : tabGroup.Manager;

				if (manager != null && manager.HasTabSettings && manager.TabSettings.ShouldSerializeDisplayFormIcon())
					return manager.TabSettings.DisplayFormIcon == DefaultableBoolean.True;

				return false;
			}
		}
		#endregion //DisplayFormIcon

		// MD 11/7/07 - NA 2008 Vol 1
		#region CloseButtonAlignment

		/// <summary>
		/// Gets the value which determines how the close button is positioned.
		/// </summary>
		public TabCloseButtonAlignment CloseButtonAlignment
		{
			get
			{
				if ( this.tab.HasSettings && this.tab.Settings.ShouldSerializeCloseButtonAlignment() )
					return this.tab.Settings.CloseButtonAlignment;

				MdiTabGroup tabGroup = this.TabGroup;

				if ( tabGroup != null && tabGroup.HasTabSettings && tabGroup.TabSettings.ShouldSerializeCloseButtonAlignment() )
					return tabGroup.TabSettings.CloseButtonAlignment;

				UltraTabbedMdiManager manager = tabGroup == null ? null : tabGroup.Manager;

				if ( manager != null && manager.HasTabSettings && manager.TabSettings.ShouldSerializeCloseButtonAlignment() )
					return manager.TabSettings.CloseButtonAlignment;

				return TabCloseButtonAlignment.AfterContent;
			}
		}

		#endregion CloseButtonAlignment

		#region CloseButtonVisibility

		/// <summary>
		/// Gets the value indicating whether the tab should display a close button.
		/// </summary>
		public TabCloseButtonVisibility CloseButtonVisibility
		{
			get
			{
				if ( this.tab.HasSettings && this.tab.Settings.ShouldSerializeCloseButtonVisibility() )
					return this.tab.Settings.CloseButtonVisibility;

				MdiTabGroup tabGroup = this.TabGroup;

				if ( tabGroup != null && tabGroup.HasTabSettings && tabGroup.TabSettings.ShouldSerializeCloseButtonVisibility() )
					return tabGroup.TabSettings.CloseButtonVisibility;

				UltraTabbedMdiManager manager = tabGroup == null ? null : tabGroup.Manager;

				if ( manager != null && manager.HasTabSettings && manager.TabSettings.ShouldSerializeCloseButtonVisibility() )
					return manager.TabSettings.CloseButtonVisibility;

				return TabCloseButtonVisibility.Always;
			}
		}

		#endregion CloseButtonVisibility

		#endregion // Properties
	}
}
