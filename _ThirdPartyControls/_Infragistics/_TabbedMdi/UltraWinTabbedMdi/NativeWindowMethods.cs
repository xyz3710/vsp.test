#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi
{

	[System.Security.SuppressUnmanagedCodeSecurityAttribute()]
	[ComVisible(false)]
	internal class NativeWindowMethods
	{
		#region Static Member Variables

		private static IntPtr patternBrush = IntPtr.Zero;

		#endregion Static Member Variables

		#region Constants

		// MD 10/31/06 - 64-Bit Support
		private const int PtrSizeOn32Bit = 4;
		private const int PtrSizeOn64Bit = 8;

		private const int PatInvert = 0x5A0049;

		private const int WM_SETREDRAW = 0x000B;

		// MD 1/4/07 - FxCop
		internal const int WM_MDIACTIVATE = 0x0222;

		#region RedrawWindow Constants

		// AS 5/1/03 WTB880
		// Changed to internal
		//
		internal const int RDW_INVALIDATE     	=	0x0001;
		internal const int RDW_INTERNALPAINT  	=	0x0002;
		internal const int RDW_ERASE          	=	0x0004;
		internal const int RDW_VALIDATE       	=	0x0008;
		internal const int RDW_NOINTERNALPAINT	=	0x0010;
		internal const int RDW_NOERASE        	=	0x0020;
		internal const int RDW_NOCHILDREN     	=	0x0040;
		internal const int RDW_ALLCHILDREN    	=	0x0080;
		internal const int RDW_UPDATENOW      	=	0x0100;
		internal const int RDW_ERASENOW       	=	0x0200;
		internal const int RDW_FRAME          	=	0x0400;
		internal const int RDW_NOFRAME        	=	0x0800;

		#endregion RedrawWindow Constants

		internal const int	WmKeyFirst = 0x100;
		internal const int	WmKeyLast = 0x108;
		internal const int	WmKeyDown = 0x100;
		internal const int	VkEscape = 0x1B;

		// AS 4/29/03 WTB880
		private const int WS_BORDER	= 0x00800000;
		private const int WS_EX_CLIENTEDGE = 0x00000200;

		private const int GWL_STYLE = (-16);
		private const int GWL_EXSTYLE = (-20);

		// AS 4/30/03 WTB880
		internal const int DCX_WINDOW = 0x1;
		internal const int DCX_INTERSECTRGN = 0x80;

		#endregion //Constants

		#region Constructor
		private NativeWindowMethods() {}
		#endregion //Constructor

		#region Structures

		// AS 4/30/03 WTB880
		#region RECT structure

		[ StructLayout( LayoutKind.Sequential )]
		internal struct RECT 
		{
			public int left;
			public int top;
			public int right;
			public int bottom;

			public RECT(int left, int top, int right, int bottom) 
			{
				this.left = left;
				this.top = top;
				this.right = right;
				this.bottom = bottom;
			}

			public Rectangle Rect { get { return new Rectangle( this.left, this.top, this.right - this.left, this.bottom - this.top ); } }

			public static RECT FromXYWH(int x, int y, int width, int height) 
			{
				return new RECT(x,
					y,
					x + width,
					y + height);
			}

			public static RECT FromRectangle( Rectangle rect ) 
			{
				return new RECT( rect.Left,
					rect.Top,
					rect.Right,
					rect.Bottom );
			}
		}

		#endregion RECT structure

		#region NCCALCSIZE_PARAMS
		[StructLayout(LayoutKind.Sequential)]
			internal struct NCCALCSIZE_PARAMS
		{
			internal RECT rectProposed;
			internal RECT rectBeforeMove;
			internal RECT rectClientBeforeMove;
			// JJD 8/14/06
			// This is a pointer to a WINDOWPOS structure which need to be marshalled separately
			// e.g.
			// NativeWindowMethods.NCCALCSIZE_PARAMS ncParams = (NativeWindowMethods.NCCALCSIZE_PARAMS)m.GetLParam(typeof(NativeWindowMethods.NCCALCSIZE_PARAMS));
			// NativeWindowMethods.WINDOWPOS wpos = (NativeWindowMethods.WINDOWPOS)Marshal.PtrToStructure(ncParams.ptrWindowPos, typeof(NativeWindowMethods.WINDOWPOS));
			internal IntPtr ptrWindowPos;
			//internal WINDOWPOS lpPos;
		}
		#endregion //NCCALCSIZE_PARAMS

		#region WINDOWPOS
		[StructLayout(LayoutKind.Sequential)]
			internal struct WINDOWPOS
		{
			internal IntPtr hwnd;
			internal IntPtr hWndInsertAfter;
			internal int x;
			internal int y;
			internal int cx;
			internal int cy;
			internal uint flags;
		}
		#endregion //WINDOWPOS
		
		// AS 3/8/05 BR02542
		#region MDINEXTMENU
		[StructLayout(LayoutKind.Sequential)]
		internal struct MDINEXTMENU
		{
			public IntPtr hmenuIn;
			public IntPtr hmenuNext;
			public IntPtr hwndNext;
		};
		#endregion //MDINEXTMENU
		
		// JJD 8/29/06 - NA 2006 vol 3
		#region WINDOWPLACEMENT structure

		[StructLayout(LayoutKind.Sequential)]
		public struct WINDOWPLACEMENT
		{
			public int length;
			public int flags;
			public int showCmd;
			public POINT ptMinPosition;
			public POINT ptMaxPosition;
			public RECT rcNormalPosition;
		}

		#endregion //WINDOWPLACEMENT structure	
    
		// JJD 8/29/06 - NA 2006 vol 3
		#region POINT structure

		[StructLayout(LayoutKind.Sequential)]
		internal struct POINT
		{
			internal int X;
			internal int Y;

			internal POINT(int x, int y)
			{
				this.X = x;
				this.Y = y;
			}

			internal Point Point { get { return new Point(this.X, this.Y); } }

			internal static POINT FromPoint(Point point)
			{
				return new POINT(point.X,
								  point.Y);
			}
		}

		#endregion POINT structure

		#endregion //Structures

		#region API Declarations 

		[DllImport("gdi32.dll", SetLastError=true)]
		private static extern int PatBlt(IntPtr hdc, int x, int y, int width, int height, int rop );

		[DllImport("user32.dll", SetLastError=true)]
		private static extern IntPtr GetDC( IntPtr hwnd );

		// AS 4/30/03 WTB880
		// Changed from private to internal
		[DllImport("user32.dll", SetLastError=true)]
		internal static extern int ReleaseDC( IntPtr hwnd, IntPtr hdc );
		
		[DllImport("gdi32", ExactSpelling=true, CharSet=System.Runtime.InteropServices.CharSet.Auto)]
		private static extern IntPtr CreateBitmap(int width, int height, int nPlanes, 
			int bitCount,
			short[] lpBits);

		[DllImport("gdi32", ExactSpelling=true, CharSet=System.Runtime.InteropServices.CharSet.Auto)]
		private static extern IntPtr CreatePatternBrush(IntPtr hBitmap);

		[DllImport("gdi32", ExactSpelling=true, CharSet=System.Runtime.InteropServices.CharSet.Auto)]
		private static extern int DeleteObject(IntPtr hObject);

		[DllImport("gdi32", ExactSpelling=true, CharSet=System.Runtime.InteropServices.CharSet.Auto)]
		private static extern IntPtr SelectObject(IntPtr hdc, IntPtr hObject);

		#region SendMessage

		[DllImport("user32", CharSet=System.Runtime.InteropServices.CharSet.Auto)]
		// MD 10/31/06 - 64-Bit Support
		//private static extern int SendMessage(IntPtr hWnd, int msg, IntPtr wparam, IntPtr lparam);
		private static extern IntPtr SendMessage( IntPtr hWnd, int msg, IntPtr wparam, IntPtr lparam );

		#endregion SendMessage

		#region RedrawWindow

		// MD 10/31/06 - 64-Bit Support
		[return: MarshalAs( UnmanagedType.Bool )]
		[DllImport("user32", ExactSpelling=true, CharSet=System.Runtime.InteropServices.CharSet.Auto)]
		private static extern bool RedrawWindow(IntPtr hWnd, IntPtr rectUpdate, IntPtr hrgnUpdate, uint flags);

		#endregion RedrawWindow

		// AS 4/29/03 WTB880
		#region SetWindowLong
		// MD 10/31/06 - 64-Bit Support
		//[DllImport("user32.dll")]
		//private static extern int SetWindowLong(IntPtr hwnd, int nIndex, int dwNewLong);
		[DllImport( "user32.dll", EntryPoint = "SetWindowLong", CharSet = CharSet.Auto )]
		private static extern IntPtr SetWindowLong32( IntPtr hWnd, int nIndex, IntPtr dwNewLong );

		[DllImport( "user32.dll", EntryPoint = "SetWindowLongPtr", CharSet = CharSet.Auto )]
		private static extern IntPtr SetWindowLongPtr64( IntPtr hWnd, int nIndex, IntPtr dwNewLong );

		internal static IntPtr SetWindowLong( IntPtr hwnd, int nIndex, IntPtr dwNewLong )
		{
			if ( IntPtr.Size == PtrSizeOn32Bit )
				return SetWindowLong32( hwnd, nIndex, dwNewLong );
			else
				return SetWindowLongPtr64( hwnd, nIndex, dwNewLong );
		}
		#endregion //SetWindowLong

		#region GetWindowLong
		// MD 10/31/06 - 64-Bit Support
		//[DllImport("user32.dll")]
		//private static extern int GetWindowLong(IntPtr hwnd, int nIndex);
		[DllImport( "user32.dll", EntryPoint = "GetWindowLong", CharSet = CharSet.Auto )]
		public static extern IntPtr GetWindowLong32( IntPtr hWnd, int nIndex );

		[DllImport( "user32.dll", EntryPoint = "GetWindowLongPtr", CharSet = CharSet.Auto )]
		public static extern IntPtr GetWindowLongPtr64( IntPtr hWnd, int nIndex );

		internal static IntPtr GetWindowLong( IntPtr hwnd, int nIndex )
		{
			if ( IntPtr.Size == PtrSizeOn32Bit )
				return GetWindowLong32( hwnd, nIndex );
			else
				return GetWindowLongPtr64( hwnd, nIndex );
		}
		#endregion //GetWindowLong

		#region SetWindowPos

		// MD 10/31/06 - 64-Bit Support
		[return: MarshalAs( UnmanagedType.Bool )]
		[DllImport("user32", ExactSpelling=true, CharSet=System.Runtime.InteropServices.CharSet.Auto)]
		private static extern bool SetWindowPos( IntPtr hWnd, IntPtr hWndInsertAfter,
			int x, int y, int cx, int cy, SetWindowPosEnum flags);

		#endregion SetWindowPos

		// AS 4/30/03 WTB880
		#region GetWindowDC
		[DllImport("user32")]
		internal static extern IntPtr GetWindowDC(IntPtr hwnd);
		#endregion // GetWindowDC

		// JJD 8/29/06 - NA 2006 vol 3
		#region  GetWindowPlacement

		static internal bool GetWindowPlacementApi(IntPtr hWnd, ref WINDOWPLACEMENT lpwndpl)
		{
			return GetWindowPlacement(hWnd, ref lpwndpl);
		}

		// MD 10/31/06 - 64-Bit Support
		[return: MarshalAs( UnmanagedType.Bool )]
		[DllImport("user32.dll")]
		static extern private bool GetWindowPlacement(IntPtr hWnd,
		   ref WINDOWPLACEMENT lpwndpl);

		#endregion // GetWindowPlacement	
    

		#region GetWindowRect
		[DllImport("user32.dll")]
		internal static extern int GetWindowRect(IntPtr hwnd, ref RECT lpRect);
		#endregion //GetWindowRect

		#region GetDCEx
		[DllImport("user32")]
		internal static extern IntPtr GetDCEx(IntPtr hwnd, IntPtr hrgnclip, int fdwOptions);
		#endregion //GetDCEx

		// AS 3/8/05 BR02542
		#region GetSystemMenu
		[DllImport("user32", ExactSpelling=true, CharSet=System.Runtime.InteropServices.CharSet.Auto)]
		// MD 10/31/06 - 64-Bit Support
		//internal static extern IntPtr GetSystemMenu(IntPtr hWnd, bool revert);
		internal static extern IntPtr GetSystemMenu( IntPtr hWnd, [MarshalAs( UnmanagedType.Bool )] bool revert );
		#endregion //GetSystemMenu

		// MD 1/4/07 - FxCop
		[DllImport( "user32" )]
		private static extern IntPtr SetFocus( IntPtr hWnd );

		[DllImport( "user32" )]
		internal static extern bool SetForegroundWindow( IntPtr hWnd );

		#endregion //API Declarations 

		#region Properties

		#region PatternBrush

		private static IntPtr PatternBrush
		{
			get
			{
				if (NativeWindowMethods.patternBrush == IntPtr.Zero)
				{
					Application.ApplicationExit += new EventHandler( NativeWindowMethods.OnApplicationExit );

					IntPtr patternBitmap = NativeWindowMethods.CreateBitmap(8,8,1,1, new short[] {0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA});

					NativeWindowMethods.patternBrush = NativeWindowMethods.CreatePatternBrush(patternBitmap);
					 
					NativeWindowMethods.DeleteObject(patternBitmap);
				}

				return patternBrush;
			}
		}

		#endregion PatternBrush

		#endregion //Properties

		#region Enumerations

		#region SetWindowPosEnum
		[Flags]
		private enum SetWindowPosEnum
		{
			SWP_NOSIZE = 0x0001,
			SWP_NOMOVE = 0x0002,
			SWP_NOZORDER = 0x0004,
			SWP_NOREDRAW = 0x0008,
			SWP_NOACTIVATE = 0x0010,
			SWP_FRAMECHANGED = 0x0020, 
			SWP_SHOWWINDOW = 0x0040,
			SWP_HIDEWINDOW = 0x0080,
			SWP_NOCOPYBITS = 0x0100,
			SWP_NOOWNERZORDER  = 0x0200, 
			SWP_NOSENDCHANGING = 0x0400, 
			SWP_DRAWFRAME = SWP_FRAMECHANGED,
			SWP_NOREPOSITION = SWP_NOOWNERZORDER,
			SWP_DEFERERASE = 0x2000,
			SWP_ASYNCWINDOWPOS = 0x4000
		}
		#endregion //SetWindowPosEnum

		#endregion //Enumerations

		#region Functions

		#region Internal

		#region DrawReversibleFrame (Rectangle)

		internal static void DrawReversibleFrame( Rectangle rect )
		{
			NativeWindowMethods.DrawReversibleFrame( rect, Rectangle.Empty );
		}
		#endregion DrawReversibleFrame (Rectangle)

		#region DrawReversibleFrame (Rectangle, Rectangle)
		internal static void DrawReversibleFrame( Rectangle rect, Rectangle previousRect )
		{
			int width = SystemInformation.FrameBorderSize.Width;

			NativeWindowMethods.DrawReversibleFrame( rect, width, previousRect );
		}
		#endregion DrawReversibleFrame

		#region DrawReversibleFrame (Rectangle, int, Rectangle)
		internal static void DrawReversibleFrame( Rectangle rect, int width, Rectangle previousRect )
		{
			if (width == 0 || rect == previousRect)
				return;

			try
			{
				// MD 1/4/07 - FxCop
				// This is not needed due to the SuppressUnmanagedCodeSecurityAttribute on the NativeWindowMethods class
				//SecurityPermission perm = new SecurityPermission( SecurityPermissionFlag.UnmanagedCode );
				//
				//perm.Assert();

				if (!previousRect.IsEmpty)
					NativeWindowMethods.DrawReversibleFrameAPI( previousRect, width );

				if (!rect.IsEmpty)
					NativeWindowMethods.DrawReversibleFrameAPI( rect, width );
			}
			// AS 4/30/03 FxCop Change
			// Catch only the specific exception
			//
			//catch
			catch (System.Security.SecurityException)
			{
				// if a previous rect was supplied, clear that first
				if (!previousRect.IsEmpty)
				{
					Rectangle[] rects = NativeWindowMethods.GetRects(previousRect, width);

					//ControlPaint.DrawReversibleFrame( previousRect, SystemColors.Control, FrameStyle.Thick );
					foreach (Rectangle rectItem in rects)
						ControlPaint.FillReversibleRectangle(rectItem, SystemColors.Control);
				}

				// if a new rect was supplied, draw that one
				if (!rect.IsEmpty)
				{
					Rectangle[] rects = NativeWindowMethods.GetRects(rect, width);

					//ControlPaint.DrawReversibleFrame( rect, SystemColors.Control, FrameStyle.Thick );
					foreach (Rectangle rectItem in rects)
						ControlPaint.FillReversibleRectangle(rectItem, SystemColors.Control);
				}
			}
		}
		#endregion DrawReversibleFrame

		#region FillReversibleFrame (Rectangle)

		internal static void FillReversibleFrame( Rectangle rect )
		{
			int width = Math.Max(rect.Width,rect.Height);

			NativeWindowMethods.DrawReversibleFrame( rect, width, Rectangle.Empty );
		}
		#endregion FillReversibleFrame (Rectangle)

		// MD 1/4/07 - FxCop
		#region SetFocus

		internal static IntPtr SetFocusApi( IntPtr hWnd )
		{
			return SetFocus( hWnd );
		}

		#endregion SetFocus

		#region SetForegroundWindow

		internal static bool SetForegroundWindowApi( IntPtr hWnd )
		{
			return SetForegroundWindow( hWnd );
		}

		#endregion SetForegroundWindow

		#region SetRedraw
		internal static void SetRedraw( Control control, bool newValue, bool redraw )
		{
			if (control == null)
				return;

			try
			{
				// MD 1/4/07 - FxCop
				// This is not needed due to the SuppressUnmanagedCodeSecurityAttribute on the NativeWindowMethods class
				//// create a security permission for unmanaged code
				//SecurityPermission sp = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
				//
				//// assert the permission
				//sp.Assert();

				IntPtr handle = control.Handle;

				SendMessageApi( handle, 
					NativeWindowMethods.WM_SETREDRAW, 
					newValue ? new IntPtr(1) : IntPtr.Zero, 
					IntPtr.Zero);
				
				if (newValue && redraw)
				{
					RedrawWindowApi( handle,
						IntPtr.Zero, 
						IntPtr.Zero,
						NativeWindowMethods.RDW_INVALIDATE
						| NativeWindowMethods.RDW_INTERNALPAINT
						| NativeWindowMethods.RDW_ERASE
						| NativeWindowMethods.RDW_ALLCHILDREN
						| NativeWindowMethods.RDW_ERASENOW
						| NativeWindowMethods.RDW_FRAME );
				}
			}
			// AS 4/30/03 FxCop Change
			// Catch only the specific exception
			//
			//catch {}
			catch (System.Security.SecurityException) {}
		}
		#endregion //SetRedraw

		#region UpdateBorderStyle
		
		internal static void UpdateBorderStyle( IntPtr hwnd, BorderStyle borderStyle )
		{
			if (hwnd == IntPtr.Zero)
				return;

			try
			{
				// MD 1/4/07 - FxCop
				// This is not needed due to the SuppressUnmanagedCodeSecurityAttribute on the NativeWindowMethods class
				//SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
				//
				//perm.Assert();

				// AS 4/30/03 WTB880
				//UpdateBorderStyleAPI(mdiClient.Handle, borderStyle);
				UpdateBorderStyleAPI(hwnd, borderStyle);
			}
			catch (SecurityException)
			{
			}
		}
		#endregion //UpdateBorderStyle

		#endregion Internal

		#region Private

		#region GetRects
		private static Rectangle[] GetRects( Rectangle rect, int width )
		{
			// if the rect is going to be filled in entirely, just return
			// the rect passed in
			if (rect.Width <= width * 2 || rect.Height <= width * 2)
				return new Rectangle[] { rect };

			Rectangle[] rects = new Rectangle[] {new Rectangle( rect.X, rect.Y, rect.Width, width ),
													new Rectangle(rect.Right - width, rect.Y + width, width, rect.Height - width * 2),
													new Rectangle(rect.X, rect.Bottom - width, rect.Width, width),
													new Rectangle(rect.X, rect.Y + width, width, rect.Height - width * 2) };

			return rects;
		}
		#endregion GetRects

		#region DrawReversibleFrameAPI( Rectangle[] )
		private static bool DrawReversibleFrameAPI(Rectangle[] rects)
		{
			if (rects == null)
				return false;

			IntPtr hwnd = IntPtr.Zero;
			IntPtr hBrush = IntPtr.Zero;

			// get the screen dc
			IntPtr hdc = NativeWindowMethods.GetDC(hwnd);

			// if its null, throw an exception - the control paint method will be used
			if (hdc == IntPtr.Zero)
				throw new ArgumentOutOfRangeException();

			try
			{
				// select our pattern brush into the dc
				hBrush = SelectObject( hdc, NativeWindowMethods.PatternBrush );

				// perform the blt's...
				foreach (Rectangle rect in rects)
					NativeWindowMethods.PatBlt(hdc, rect.X, rect.Y, rect.Width, rect.Height, NativeWindowMethods.PatInvert);
			}
			// AS 4/30/03 FxCop Change
			// Catch only the specific exception
			//
			//catch
			catch (Exception ex)
			{
				if (ex is OutOfMemoryException)
					throw;

				return false;
			}
			finally
			{
				// select the previous brush back in
				SelectObject( hdc, hBrush );

				// make sure we release the dc
				NativeWindowMethods.ReleaseDC( hwnd, hdc );
			}

			return true;
		}
		#endregion //DrawReversibleFrameAPI( Rectangle[], width )

		#region DrawReversibleFrameAPI (Rectangle, width)
		private static void DrawReversibleFrameAPI(Rectangle rect, int width)
		{
			Rectangle[] rects = NativeWindowMethods.GetRects(rect, width);

			NativeWindowMethods.DrawReversibleFrameAPI( rects );
		}
		#endregion DrawReversibleFrameAPI

		#region OnApplicationExit
		private static void OnApplicationExit( object sender, EventArgs e )
		{
			// clean up the bitmap brush we created
			if (NativeWindowMethods.patternBrush != IntPtr.Zero)
				NativeWindowMethods.DeleteObject( NativeWindowMethods.patternBrush );
		}
		#endregion //OnApplicationExit

		#region SendMessageApi
		// MD 10/31/06 - 64-Bit Support
		//private static int SendMessageApi(IntPtr hWnd, int msg, IntPtr wparam, IntPtr lparam)
		// MD 1/4/07 - FxCop
		// Made internal
		//private static IntPtr SendMessageApi( IntPtr hWnd, int msg, IntPtr wparam, IntPtr lparam )
		internal static IntPtr SendMessageApi( IntPtr hWnd, int msg, IntPtr wparam, IntPtr lparam )
		{
			return SendMessage( hWnd, msg, wparam, lparam);
		}
		#endregion //SendMessageApi

		// AS 4/30/03 WTB880
		// Changed from private to internal
		#region RedrawWindowApi
		internal static bool RedrawWindowApi(IntPtr hWnd, IntPtr rectUpdate, IntPtr hrgnUpdate, uint flags)
		{
			return RedrawWindow( hWnd, rectUpdate, hrgnUpdate, flags);
		}
		#endregion //RedrawWindowApi

		// AS 4/29/03 WTB880
		#region AdjustStylesForBorder
		// MD 10/31/06 - 64-Bit Support
		//private static void AdjustStylesForBorder( ref int currentStyle, ref int currentExStyle, BorderStyle borderStyle )
		private static void AdjustStylesForBorder( ref IntPtr currentStyle, ref IntPtr currentExStyle, BorderStyle borderStyle )
		{
			switch (borderStyle)
			{
				case BorderStyle.None:
					// MD 10/31/06 - 64-Bit Support
					//currentExStyle &= ~(int)NativeWindowMethods.WS_EX_CLIENTEDGE;
					//currentStyle &= ~(int)NativeWindowMethods.WS_BORDER;
					currentExStyle = RemoveBits( currentExStyle, NativeWindowMethods.WS_EX_CLIENTEDGE );
					currentStyle = RemoveBits( currentStyle, NativeWindowMethods.WS_BORDER );
					break;
				case BorderStyle.Fixed3D:
					// MD 10/31/06 - 64-Bit Support
					//currentExStyle |= (int)NativeWindowMethods.WS_EX_CLIENTEDGE;
					//currentStyle &= ~(int)NativeWindowMethods.WS_BORDER;
					currentExStyle = AddBits( currentExStyle, NativeWindowMethods.WS_EX_CLIENTEDGE );
					currentStyle = RemoveBits( currentStyle, NativeWindowMethods.WS_BORDER );
					break;
				case BorderStyle.FixedSingle:
					// MD 10/31/06 - 64-Bit Support
					//currentExStyle &= ~(int)NativeWindowMethods.WS_EX_CLIENTEDGE;
					//currentStyle |= (int)NativeWindowMethods.WS_BORDER;
					currentExStyle = RemoveBits( currentExStyle, NativeWindowMethods.WS_EX_CLIENTEDGE );
					currentStyle = AddBits( currentStyle, NativeWindowMethods.WS_BORDER );
					break;
			}
		}
		#endregion //AdjustStylesForBorder

		#region UpdateBorderStyleAPI
		// AS 4/30/03 WTB880
		// Changed signature since our border style
		// may not match up with the system styles for 
		// borders. Also, we want to notify about a 
		// frame change regardless of a style bit change
		// since we want the nc calcsize message
		//
		private static void UpdateBorderStyleAPI( IntPtr handle, BorderStyle borderStyle )
		{
			// get the current styles
			// MD 10/31/06 - 64-Bit Support
			//int currentStyle = NativeWindowMethods.GetWindowLong( handle, NativeWindowMethods.GWL_STYLE );
			//int currentStyleEx = NativeWindowMethods.GetWindowLong( handle, NativeWindowMethods.GWL_EXSTYLE );
			//
			//int oldStyle = currentStyle;
			//int oldStyleEx = currentStyleEx;
			IntPtr currentStyle = NativeWindowMethods.GetWindowLong( handle, NativeWindowMethods.GWL_STYLE );
			IntPtr currentStyleEx = NativeWindowMethods.GetWindowLong( handle, NativeWindowMethods.GWL_EXSTYLE );

			IntPtr oldStyle = currentStyle;
			IntPtr oldStyleEx = currentStyleEx;

			// modify them based on the specified style
			AdjustStylesForBorder( ref currentStyle, ref currentStyleEx, borderStyle );

			// update the styles
			if (oldStyle != currentStyle)
				NativeWindowMethods.SetWindowLong( handle, NativeWindowMethods.GWL_STYLE, currentStyle );
			
			if (oldStyleEx != currentStyleEx)
				NativeWindowMethods.SetWindowLong( handle, NativeWindowMethods.GWL_EXSTYLE, currentStyleEx );

			// notify about the frame change
			NativeWindowMethods.SetWindowPos(handle, IntPtr.Zero,
				0, 0, 0, 0,
				NativeWindowMethods.SetWindowPosEnum.SWP_FRAMECHANGED	| 
				NativeWindowMethods.SetWindowPosEnum.SWP_NOACTIVATE |
				NativeWindowMethods.SetWindowPosEnum.SWP_NOMOVE |
				NativeWindowMethods.SetWindowPosEnum.SWP_NOOWNERZORDER |
				NativeWindowMethods.SetWindowPosEnum.SWP_NOREPOSITION |
				NativeWindowMethods.SetWindowPosEnum.SWP_NOSIZE |
				NativeWindowMethods.SetWindowPosEnum.SWP_NOZORDER);
		}
		#endregion //UpdateBorderStyleAPI

		// MD 10/31/06 - 64-Bit Support
		#region AddBits

		internal static IntPtr AddBits( IntPtr ptr, int bitsToRemove )
		{
			if ( IntPtr.Size == NativeWindowMethods.PtrSizeOn32Bit )
				return new IntPtr( ptr.ToInt32() | bitsToRemove );
			else
				return new IntPtr( ptr.ToInt64() | (uint)bitsToRemove );
		}

		#endregion AddBits

		#region RemoveBits

		internal static IntPtr RemoveBits( IntPtr ptr, int bitsToRemove )
		{
			if ( IntPtr.Size == NativeWindowMethods.PtrSizeOn32Bit )
				return new IntPtr( ptr.ToInt32() & ~bitsToRemove );
			else
				return new IntPtr( ptr.ToInt64() & ~bitsToRemove );
		}

		#endregion RemoveBits

		#endregion //Private

		#endregion //Functions
	}
}
