#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Win.UltraWinTabs;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	// Dirty Actions
	// =============
	// Create tab group			=> Create TabGroupInfo					=> Affects All Other TabGroup Sizes
	// Remove tab group			=> Remove TabGroupInfo					=> Affects All Other TabGroup Sizes
	// Change SplitterWidth		=> Dirty TabGroupInfo for all			=> Affects Form Area for Each
	// TabGroup Dirtied			=> Affects Height of TabGroup Control	=> Affects Form Area
	// All TabGroups Dirtied	=> Affects Height of All TabGroupControls
	// MdiClient Resized		=> Affects Size of All TabGroups
	// MdiClient Repositioned	=> Affects Offset of All TabGroups
	// 
	// Types of Change
	// ===============
	// Change TabGroupControl and SplitterBarControl Location
	// Change TabGroupControl and SplitterBarControl Size
	// Change Selected Tab of a TabGroup's Form Size
	// 
	internal class TabGroupInfoManager
	{
		#region Member Variables

		private Point						mdiClientLocation = Point.Empty;
		private bool						mdiClientLocationDirty = true;
		private bool						tabGroupCollectionDirty = true;

		private HybridDictionary			tabGroupInfos = null;

		private UltraTabbedMdiManager		manager = null;

		// AS 2/6/04 WTB1314
		//private const int					TabGroupPadding = 2;
		internal const int					DefaultTabGroupPadding = 2;

		internal const UIElementBorderStyle	TabGroupControlBorderStyle = UIElementBorderStyle.RaisedSoft;

		#endregion //Member Variables

		#region Constructor
		internal TabGroupInfoManager(UltraTabbedMdiManager manager)
		{
			this.manager = manager;
		}
		#endregion //Constructor

		#region Properties

		#region Private

		#region TabGroupInfos
		private HybridDictionary TabGroupInfos
		{
			get
			{
				if (this.tabGroupInfos == null)
					this.tabGroupInfos = new HybridDictionary(3);

				return this.tabGroupInfos;
			}
		}
		#endregion //TabGroupInfos

		#region IsVerticalGroups
		
		#endregion //IsVerticalGroups

		#region MdiClientLocation
		private Point MdiClientLocation
		{
			get
			{
				if (this.mdiClientLocationDirty)
					this.mdiClientLocation = this.manager.MdiClientLocation;

				return this.mdiClientLocation;
			}
		}
		#endregion //MdiClientLocation

		#endregion //Private

		#endregion //Properties

		#region Methods

		#region Private

		#region VerifyMetrics
		private void VerifyMetrics()
		{
			if (this.tabGroupCollectionDirty)
			{
				this.tabGroupCollectionDirty = false;

				// make sure the collections are in sync
				this.ResyncTabGroupCollection();

				// recalculate all the tab group infos
				this.RecalculateTabGroupInfos();
			}
			else
			{
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup tabGroup in this.manager.TabGroups)
				foreach(MdiTabGroup tabGroup in this.manager.TabGroups.GetNestedTabGroups(false))
				{
					TabGroupInfo tabGroupInfo = this.TabGroupInfos[tabGroup] as TabGroupInfo;

					Debug.Assert(tabGroupInfo != null, "Unable to locate a 'TabGroupInfo' for the specified tab group!");

					if (!tabGroupInfo.Dirty)
						continue;

					this.RecalculateTabGroupInfo(tabGroup);
				}
			}
		}
		#endregion //VerifyMetrics

		#region VerifyTabGroupMetrics
		private void VerifyTabGroupMetrics( MdiTabGroup tabGroup )
		{
			if (this.tabGroupInfos == null || !this.TabGroupInfos.Contains(tabGroup))
				this.ResyncTabGroupCollection();

			TabGroupInfo tabGroupInfo = this.TabGroupInfos[tabGroup] as TabGroupInfo;

			if (tabGroupInfo.Dirty)
				this.RecalculateTabGroupInfo(tabGroup);
		}
		#endregion //VerifyTabGroupMetrics

		#region ResyncTabGroupCollection
		private bool ResyncTabGroupCollection()
		{
			if (!this.IsResyncNeeded())
				return false;

			TabGroupInfo[] oldInfos = null;
			
			if (this.tabGroupInfos != null && this.tabGroupInfos.Count > 0)
			{
				// store the tab group infos so we can reuse them
				oldInfos = new TabGroupInfo[this.TabGroupInfos.Count];
			
				this.TabGroupInfos.Values.CopyTo(oldInfos, 0);

				this.TabGroupInfos.Clear();
			}

			int nextIndex = 0;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.manager.TabGroups)
			foreach(MdiTabGroup tabGroup in this.manager.TabGroups.GetNestedTabGroups(false))
			{
				TabGroupInfo tabGroupInfo = null;

				if (oldInfos != null && nextIndex < oldInfos.Length)
				{
					tabGroupInfo = oldInfos[nextIndex];
					tabGroupInfo.Dirty = true;
					nextIndex++;
				}
				else
					tabGroupInfo = new TabGroupInfo();

				this.TabGroupInfos.Add(tabGroup, tabGroupInfo);
			}

			return true;
		}
		#endregion //ResyncTabGroupCollection

		#region IsResyncNeeded
		private bool IsResyncNeeded()
		{
			int tabGroupInfoCount = this.tabGroupInfos == null ? 0 : this.tabGroupInfos.Count;
			int tabGroupCount = !this.manager.HasTabGroups ? 0 : this.manager.TabGroups.Count;

			// check the counts first
			if (tabGroupCount != tabGroupInfoCount)
				return true;

			// make sure we have one TabGroupInfo for each tab group
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.manager.TabGroups)
			foreach(MdiTabGroup tabGroup in this.manager.TabGroups.GetNestedTabGroups(false))
			{
				if (!this.TabGroupInfos.Contains(tabGroup))
					return true;
			}

			return false;
		}
		#endregion //IsResyncNeeded

		#region RecalculateTabGroupInfos
		private void RecalculateTabGroupInfos()
		{
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.manager.TabGroups)
			foreach(MdiTabGroup tabGroup in this.manager.TabGroups.GetNestedTabGroups(false))
				this.RecalculateTabGroupInfo(tabGroup);
		}
		#endregion //RecalculateTabGroupInfos

		#region RecalculateTabGroupInfo
		private void RecalculateTabGroupInfo( MdiTabGroup tabGroup )
		{
			TabGroupInfo info = this.TabGroupInfos[tabGroup] as TabGroupInfo;

			Debug.Assert(info != null, "There is no 'TabGroupInfo' for the associated 'MdiTabGroup'");

			// store whether we are dealing with vertical groups
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//bool isVerticalGroup = this.IsVerticalGroups;
			bool isVerticalGroup = this.IsInVerticalGroup(tabGroup);

			// store the extent that is common to all tab groups - i.e.
			// if they are displayed vertically, it is the height of the mdiclient
			//
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//int commonExtent = this.manager.CommonMdiClientExtent;
			int commonExtent = this.manager.CalculateCommonExtent(tabGroup.Parent);

			// get the orientation of the tab group
			TabOrientation tabOrientation = tabGroup.SettingsResolved.TabOrientation;
			bool isVerticalTabs = IsVerticalTabOrientation(tabOrientation);

			// get the height of the tab area
			int tabRowHeight;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			if (tabGroup.HasTabGroups)
				tabRowHeight = 0;
			else
			if (isVerticalTabs)
				tabRowHeight = tabGroup.TabManager.TabAreaSize.Width;
			else
				tabRowHeight = tabGroup.TabManager.TabAreaSize.Height;

			// AS 4/23/03
			// Don't add in the padding for wizard style tabs.
			// 
			// AS 2/6/04 WTB1314
			// We don't need to do this now that we have a margins property.
			//
			//if (tabGroup.SettingsResolved.TabStyle != TabStyle.Wizard)
			//	tabRowHeight += TabGroupInfoManager.TabGroupPadding;

			int tabGroupExtent = tabGroup.Extent;	// the extent of the tab group

			// find the relative origin of the tab group - that is the
			// location of the tab group with respect to the upper left 
			// of the mdi client area
			Point relativeOrigin = this.GetRelativeTabGroupOrigin(tabGroup);

			// now get the total size for the mdi tab group
			Size tabGroupAreaSize;

			if (isVerticalGroup)
				tabGroupAreaSize = new Size(tabGroupExtent, commonExtent);
			else
				tabGroupAreaSize = new Size(commonExtent, tabGroupExtent);

			// now we have the location of the tab group area
			Rectangle tabGroupArea = new Rectangle(relativeOrigin, tabGroupAreaSize);

			bool needsSplitter = this.NeedsSplitter(tabGroup);

			// the width of the splitter bar
			int splitterWidth = needsSplitter ? this.manager.SplitterWidth : 0;

			// now we have enough info to calculate the rect of the mdi tab group
			//
			Rectangle formRect;
			Rectangle splitterRect = Rectangle.Empty;
			Rectangle tabGroupControlRect;

			// store the working area (area available) in a separate rect
			Rectangle workingArea = tabGroupArea;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//bool allocateSplitterFirst = AllocateSplitterRectBeforeTabGroup(tabOrientation, isVerticalGroup);
			bool allocateSplitterFirst = AllocateSplitterRectBeforeTabGroup(tabOrientation, isVerticalGroup, this.manager);

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			// Changed the calls to CalcSplitterRect and CalcTabGroupControlSize to
			// pass along whether we are dealing with a vertical tabgroup.
			//
			if (isVerticalGroup)
			{
				if (needsSplitter && allocateSplitterFirst)	// position the splitter on the right
					splitterRect = this.CalcSplitterRect(splitterWidth, ref workingArea, isVerticalGroup);

				// now allocate the tab group size
				Size tabGroupSize = this.CalcTabGroupControlSize(tabRowHeight, workingArea.Width, workingArea.Height, tabOrientation, isVerticalGroup);

				// get the tab group control rect and have it
				// reduce the working rect
				tabGroupControlRect = this.CalcTabGroupControlRectangle(tabOrientation, tabGroupSize, ref workingArea);

				if (needsSplitter && !allocateSplitterFirst)	// position the splitter on the right
					splitterRect = this.CalcSplitterRect(splitterWidth, ref workingArea, isVerticalGroup);
			}
			else
			{
				if (needsSplitter && allocateSplitterFirst)	// position the splitter at the bottom
					splitterRect = this.CalcSplitterRect(splitterWidth, ref workingArea, isVerticalGroup);

				// then position the tab group above it
				Size tabGroupSize = this.CalcTabGroupControlSize(tabRowHeight, workingArea.Height, workingArea.Width, tabOrientation, isVerticalGroup);

				// remove the size needed for the border
				tabGroupSize = this.RemoveTabGroupBorder(tabGroup, tabGroupSize);

				// get the tab group control rect and have it
				// reduce the working rect
				tabGroupControlRect = this.CalcTabGroupControlRectangle(tabOrientation, tabGroupSize, ref workingArea);

				if (needsSplitter && !allocateSplitterFirst)	// position the splitter at the bottom
					splitterRect = this.CalcSplitterRect(splitterWidth, ref workingArea, isVerticalGroup);
			}

			// the area available for the forms is the remaining area
			formRect = workingArea;

			// now store it all in the TabGroupInfo
			info.FormRectangle = formRect;
			info.SplitterRectangle = splitterRect;
			info.TabGroupControlRectangle = tabGroupControlRect;
			info.TabGroupArea = tabGroupArea;

			// its no longer dirty so update its state
			info.Dirty = false;
		}
		#endregion //RecalculateTabGroupInfo

		#region AllocateSplitterRectBeforeTabGroup
		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		//private static bool AllocateSplitterRectBeforeTabGroup( TabOrientation tabOrientation, bool isVerticalGroup )
		private static bool AllocateSplitterRectBeforeTabGroup( TabOrientation tabOrientation, bool isVerticalGroup, UltraTabbedMdiManager manager )
		{
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			// When using nested tab groups, we want the splitter to
			// always be outside the tabgroup. Otherwise, you can
			// get some weird scenarios where one tabgroup has tabs and 
			// its sibling has nested tab groups but the splitter only 
			// goes up under the tab group and honestly it doesn't look
			// very nice.
			//
			if (manager.AllowNestedTabGroupsResolved || manager.HasNestedTabGroups)
				return true;

			// anytime the splitter is parallel to the orientation of
			// the tabs, we should allocate the splitter first since
			// in visual studio, it gets preference for space.
			switch(tabOrientation)
			{
				default:
				case TabOrientation.TopLeft:
				case TabOrientation.TopRight:
				case TabOrientation.BottomLeft:
				case TabOrientation.BottomRight:
					// when the splitter is vertical, we have to
					// allocate the tab group first but when it
					// is horizontal, we can allocate the splitter first
					// since it is parallel to the tabs
					return !isVerticalGroup;
				case TabOrientation.LeftBottom:
				case TabOrientation.LeftTop:
				case TabOrientation.RightTop:
				case TabOrientation.RightBottom:
					// when the splitter is horizontal, it is in
					// conflict with the tabs so the tab group
					// must be allocated first but when its 
					// vertical, its perpendicular with the
					// tab groups and should be given preference
					return isVerticalGroup;
			}
		}
		#endregion //AllocateSplitterRectBeforeTabGroup

		#region GetRelativeTabGroupOrigin
		private Point GetRelativeTabGroupOrigin( MdiTabGroup tabGroup )
		{
			Point origin = Point.Empty;

			
			MdiTabGroup tempTabGroup = tabGroup;

			do
			{
				bool isVerticalGroup = this.IsInVerticalGroup(tempTabGroup);

				foreach(MdiTabGroup group in tempTabGroup.ParentCollection)
				{
					if (group == tempTabGroup)
						break;

					if (isVerticalGroup)
						origin.X += group.Extent;
					else
						origin.Y += group.Extent;
				}

				// walk up the parent chain
				tempTabGroup = tempTabGroup.Parent;

			} while(tempTabGroup != null);

			return origin;
		}
		#endregion //GetRelativeTabGroupOrigin

		#region GetActualTabGroupOrigin
		private Point GetActualTabGroupOrigin(Point relativeOrigin)
		{
			Point actualOrigin = this.MdiClientLocation;

			actualOrigin.Offset(relativeOrigin.X, relativeOrigin.Y);

			return actualOrigin;
		}
		#endregion //GetActualTabGroupOrigin

		#region CalcTabGroupControlSize
		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		//private Size CalcTabGroupControlSize(int tabRowHeight, int tabGroupExtent, int commonExtent, TabOrientation tabOrientation)
		private Size CalcTabGroupControlSize(int tabRowHeight, int tabGroupExtent, int commonExtent, TabOrientation tabOrientation, bool isVerticalTabGroup)
		{
			switch(tabOrientation)
			{
				default:
				case TabOrientation.BottomLeft:
				case TabOrientation.BottomRight:
				case TabOrientation.TopLeft:
				case TabOrientation.TopRight:
					// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
					//if (this.IsVerticalGroups)
					if (isVerticalTabGroup)
					{
						// since this contradicts the orientation of the
						// the tab group, the tab group can only be as wide
						// as its extent. its as tall as the row height
						return new Size(tabGroupExtent, tabRowHeight );
					}
					else	// Horizontal
					{
						// since the orientation is in line with the tab
						// orientation, its as wide as the mdi client and
						// as tall as the tab row height
						return new Size(commonExtent, tabRowHeight);
					}

				case TabOrientation.LeftBottom:
				case TabOrientation.LeftTop:
				case TabOrientation.RightBottom:
				case TabOrientation.RightTop:
					// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
					//if (this.IsVerticalGroups)
					if (isVerticalTabGroup)
					{
						// its as wide as the tab row height and as tall 
						// as the mdi client area since the orientation of
						// the tab group is in line with the orientation
						// of the splits
						return new Size(tabRowHeight, commonExtent);
					}
					else	// Horizontal
					{
						// since the tab orientation contradicts the orientation
						// of the splits, the tab group can only be as high as the
						// extent of the group. its as wide as the tab row height
						return new Size(tabRowHeight, tabGroupExtent);
					}
			}
		}
		#endregion //CalcTabGroupControlSize

		#region CalcTabGroupControlRectangle
		private Rectangle CalcTabGroupControlRectangle( TabOrientation tabOrientation, Size tabGroupControlSize, ref Rectangle tabGroupArea )
		{
			// store the total starting tab group area so we can manipulate
			// the tabGroupArea
			Rectangle totalTabGroupArea = tabGroupArea;

			// return its position within the provided display rect
			switch(tabOrientation)
			{
				default:
				case TabOrientation.BottomLeft:
				case TabOrientation.BottomRight:
					// reduce the available work area
					tabGroupArea.Height -= tabGroupControlSize.Height;

					return new Rectangle(totalTabGroupArea.Left,
						totalTabGroupArea.Bottom - tabGroupControlSize.Height,
						totalTabGroupArea.Width,
						tabGroupControlSize.Height);

				case TabOrientation.TopLeft:
				case TabOrientation.TopRight:
					// shift the remaining work area down
					tabGroupArea.Y += tabGroupControlSize.Height;
					tabGroupArea.Height -= tabGroupControlSize.Height;

					return new Rectangle(totalTabGroupArea.Left,
						totalTabGroupArea.Top,
						totalTabGroupArea.Width,
						tabGroupControlSize.Height);

				case TabOrientation.LeftBottom:
				case TabOrientation.LeftTop:
					// shift the remaining work area right
					tabGroupArea.X += tabGroupControlSize.Width;
					tabGroupArea.Width -= tabGroupControlSize.Width;

					return new Rectangle(totalTabGroupArea.Left,
						totalTabGroupArea.Top,
						tabGroupControlSize.Width,
						totalTabGroupArea.Height);

				case TabOrientation.RightBottom:
				case TabOrientation.RightTop:
					// reduce the remaining work area
					tabGroupArea.Width -= tabGroupControlSize.Width;

					return new Rectangle(totalTabGroupArea.Right - tabGroupControlSize.Width,
						totalTabGroupArea.Top,
						tabGroupControlSize.Width,
						totalTabGroupArea.Height);
			}
		}
		#endregion //CalcTabGroupControlRectangle

		#region CalcSplitterRect
		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		//private Rectangle CalcSplitterRect( int splitterWidth, ref Rectangle tabGroupArea )
		private Rectangle CalcSplitterRect( int splitterWidth, ref Rectangle tabGroupArea, bool isVerticalTabGroup )
		{
			if (splitterWidth == 0)
				return Rectangle.Empty;

			Rectangle totalAvailableArea = tabGroupArea;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.IsVerticalGroups)
			if (isVerticalTabGroup)
			{
				// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
				if (tabGroupArea.Width == 0)
					return Rectangle.Empty;

				tabGroupArea.Width -= splitterWidth;

				return new Rectangle(totalAvailableArea.Right - splitterWidth,
					totalAvailableArea.Top,
					splitterWidth,
					totalAvailableArea.Height);
			}
			else
			{	
				// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
				if (tabGroupArea.Height == 0)
					return Rectangle.Empty;

				tabGroupArea.Height -= splitterWidth;

				return new Rectangle(totalAvailableArea.Left,
					totalAvailableArea.Bottom - splitterWidth,
					totalAvailableArea.Width,
					splitterWidth);
			}
		}
		#endregion //CalcSplitterRect

		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		#region IsInVerticalGroup
		private bool IsInVerticalGroup(MdiTabGroup tabGroup)
		{
			Debug.Assert(tabGroup != null, "Cannot determine the orientation of a null tabgroup");

			Orientation orientation = tabGroup == null ? this.manager.Orientation : tabGroup.ParentOrientation;

			return orientation == Orientation.Vertical;
		}
		#endregion //IsInVerticalGroup

		#endregion //Private

		#region Internal

		#region Dirty Methods
		internal void DirtyMdiClientLocation()
		{
			this.mdiClientLocationDirty = true;
		}

		internal void DirtyTabGroupExtent()
		{
			this.DirtyAllTabGroups();
		}

		internal void DirtyAllTabGroups()
		{
			this.tabGroupCollectionDirty = true;

			foreach(TabGroupInfo groupInfo in this.TabGroupInfos.Values)
				groupInfo.Dirty = true;
		}

		internal void DirtyTabGroup( MdiTabGroup tabGroup )
		{
			if (this.tabGroupInfos == null)
				return;

			if (!this.tabGroupInfos.Contains(tabGroup))
			{
				this.DirtyAllTabGroups();
				return;
			}

			TabGroupInfo tgi = this.TabGroupInfos[tabGroup] as TabGroupInfo;

			tgi.Dirty = true;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			if (tabGroup.HasTabGroups)
			{
				foreach(MdiTabGroup nestedTabGroup in tabGroup.TabGroups)
					this.DirtyTabGroup(nestedTabGroup);
			}
		}
		#endregion //Dirty Methods

		#region CalcMaxAvailableTabGroupSize
		internal Size CalcMaxAvailableTabGroupSize( MdiTabGroup tabGroup )
		{
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//return this.CalcMaxAvailableTabGroupSize(tabGroup, this.manager.CommonMdiClientExtent);
			return this.CalcMaxAvailableTabGroupSize(tabGroup, this.manager.CalculateCommonExtent(tabGroup.Parent) );
		}	

		internal Size CalcMaxAvailableTabGroupSize( MdiTabGroup tabGroup, int commonExtent )
		{
			int tabGroupExtent = tabGroup.Extent;
			TabOrientation tabOrientation = tabGroup.SettingsResolved.TabOrientation;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//bool isVerticalGroup = this.IsVerticalGroups;
			bool isVerticalGroup = this.IsInVerticalGroup(tabGroup);

			Size tabGroupSize;

			// get the total size of the tab group
			if (isVerticalGroup)
				tabGroupSize = new Size(tabGroupExtent, commonExtent);
			else
				tabGroupSize = new Size(commonExtent, tabGroupExtent);

			// remove the splitter width if it gets preference
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.NeedsSplitter(tabGroup) && AllocateSplitterRectBeforeTabGroup(tabOrientation, isVerticalGroup))
			if (this.NeedsSplitter(tabGroup) && AllocateSplitterRectBeforeTabGroup(tabOrientation, isVerticalGroup, this.manager))
			{
				int splitterWidth = this.manager.SplitterWidth;

				// if the splitter is vertical reduce the available width
				if (isVerticalGroup)
				{
					// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
					//tabGroupSize.Width -= splitterWidth;
					tabGroupSize.Width = Math.Max(0, tabGroupSize.Width - splitterWidth);
				}
				else
				{
					// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
					//tabGroupSize.Height -= splitterWidth;
					tabGroupSize.Height = Math.Max(0, tabGroupSize.Height - splitterWidth);
				}
			}

			tabGroupSize = this.RemoveTabGroupBorder(tabGroup, tabGroupSize);

			return tabGroupSize;
		}
		#endregion //CalcMaxAvailableTabGroupSize

		#region RemoveTabGroupBorder
		private Size RemoveTabGroupBorder( MdiTabGroup tabGroup, Size tabGroupSize )
		{
			Border3DSide side = this.GetTabGroupBorderSide(tabGroup);
			
			UIElementBorderWidths borderWidths = DrawUtility.CalculateBorderWidths(TabGroupControlBorderStyle,
				side);

			tabGroupSize.Width -= borderWidths.Left + borderWidths.Right;
			tabGroupSize.Height -= borderWidths.Top + borderWidths.Bottom;

			return tabGroupSize;
		}
		#endregion //RemoveTabGroupBorder

		#region NeedsSplitter
		internal bool NeedsSplitter( MdiTabGroup tabGroup )
		{
			if (this.manager.SplitterWidth == 0)
				return false;

			Debug.Assert(this.manager.HasTabGroups, "The specified tab group is not in the TabGroups collection.");

			if (!this.manager.HasTabGroups)
				return false;

			// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
			if (this.manager.MaximizedTabGroup != null &&
				this.manager.MaximizedTabGroupDisplayStyle == MaximizedMdiTabGroupDisplayStyle.ShowMaximizedGroupOnly)
				return false;

			// as long as it is not the last tab group
			//return this.manager.TabGroups[this.manager.TabGroups.Count - 1] != tabGroup;
			Debug.Assert(tabGroup.ParentCollection != null, "Cannot access the ParentCollection of the specified tab group!");

			return tabGroup.ParentCollection[tabGroup.ParentCollection.Count - 1] != tabGroup;
		}
		#endregion //NeedsSplitter

		#region GetTabGroupControlRectangle
		internal Rectangle GetTabGroupControlRectangle( MdiTabGroup tabGroup )
		{
			this.VerifyTabGroupMetrics(tabGroup);

			TabGroupInfo groupInfo = this.TabGroupInfos[tabGroup] as TabGroupInfo;

			Rectangle tabGroupRect = groupInfo.TabGroupControlRectangle;

			// adjust by the actual positon of the mdi client
			tabGroupRect.Offset(this.MdiClientLocation);

			return tabGroupRect;
		}
		#endregion //GetTabGroupControlRectangle

		#region GetSplitterControlRectangle
		internal Rectangle GetSplitterControlRectangle( MdiTabGroup tabGroup )
		{
			this.VerifyTabGroupMetrics(tabGroup);

			TabGroupInfo groupInfo = this.TabGroupInfos[tabGroup] as TabGroupInfo;

			Rectangle splitterRect = groupInfo.SplitterRectangle;

			// adjust by the actual positon of the mdi client
			splitterRect.Offset(this.MdiClientLocation);

			return splitterRect;
		}
		#endregion //GetSplitterControlRectangle

		#region GetFormPosition
		internal Rectangle GetFormPosition( MdiTabGroup tabGroup )
		{
			this.VerifyTabGroupMetrics(tabGroup);

			TabGroupInfo groupInfo = this.TabGroupInfos[tabGroup] as TabGroupInfo;

			// return this as it is since the rect should be
			// relative to the upper left of the mdi client area
			return groupInfo.FormRectangle;
		}
		#endregion //GetFormPosition

		#region IsVerticalTabOrientation
		internal static bool IsVerticalTabOrientation(TabOrientation tabOrientation)
		{
			switch(tabOrientation)
			{
				case TabOrientation.LeftBottom:
				case TabOrientation.LeftTop:
				case TabOrientation.RightBottom:
				case TabOrientation.RightTop:
					return true;
				default:
					return false;
			}
		}
		#endregion //IsVerticalTabOrientation

		#region GetTabGroupArea
		internal Rectangle GetTabGroupArea( MdiTabGroup tabGroup )
		{
			this.VerifyTabGroupMetrics(tabGroup);

			TabGroupInfo groupInfo = this.TabGroupInfos[tabGroup] as TabGroupInfo;

			Rectangle tabGroupArea = groupInfo.TabGroupArea;

			// adjust by the actual positon of the mdi client
			tabGroupArea.Offset(this.MdiClientLocation);

			return tabGroupArea;
		}
		#endregion //GetTabGroupArea

		#region GetTabGroupBorderSide
		internal Border3DSide GetTabGroupBorderSide( MdiTabGroup tabGroup )
		{
			if (tabGroup == null)
				return 0;

			// if this is the last tab group, it doesn't need any border
			TabOrientation tabOrientation = tabGroup.SettingsResolved.TabOrientation;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//bool isVerticalGroup = this.IsVerticalGroups;
			bool isVerticalGroup = this.IsInVerticalGroup(tabGroup);

			// if the splitter will be allocated first, then it
			// will completely cover the side of the tabgroup between
			// it and the next tab group
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.NeedsSplitter(tabGroup) && AllocateSplitterRectBeforeTabGroup(tabOrientation, isVerticalGroup))
			if (this.NeedsSplitter(tabGroup) && AllocateSplitterRectBeforeTabGroup(tabOrientation, isVerticalGroup, this.manager))
				return 0;

			// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
			// Don't draw the borders when maximizing the active tab group.
			//
			if (this.manager.IsActiveTabGroupMaximized)
				return 0;

			// if its the last group, it doesn't need any
			// AS 7/16/04 WTB1534
			//if (this.manager.TabGroups[this.manager.TabGroups.Count -1] == tabGroup)
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.manager.TabGroups.Count == 0 || this.manager.TabGroups[this.manager.TabGroups.Count -1] == tabGroup)
			if (tabGroup.ParentCollection == null ||
				tabGroup.ParentCollection.Count == 0 ||
				tabGroup.ParentCollection[tabGroup.ParentCollection.Count - 1] == tabGroup)
				return 0;

			bool isVerticalOrientation = Utilities.IsVerticalTabOrientation(tabOrientation);

			if (isVerticalGroup && !isVerticalOrientation)
				return Border3DSide.Right;		// vertical tab groups need a border on the right edge
			else if (!isVerticalGroup && isVerticalOrientation)
				return Border3DSide.Bottom;		// horizontal groups need a border on the bottom
			else
				return 0;
		}
		#endregion //GetTabGroupBorderSide

		#region GetTabGroupDropRect
		internal Rectangle GetTabGroupDropRect( MdiTabGroup tabGroup )
		{
			Rectangle formRect = this.GetFormPosition(tabGroup);

			// adjust by the actual positon of the mdi client
			formRect.Offset(this.MdiClientLocation);

			if (!this.NeedsSplitter(tabGroup))
				return formRect;

			if (formRect.IsEmpty)
				return this.GetSplitterControlRectangle(tabGroup);

			// if the splitter is outside the tabgroup then don't union that area in
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//if (AllocateSplitterRectBeforeTabGroup(tabGroup.SettingsResolved.TabOrientation, this.IsVerticalGroups))
			if (AllocateSplitterRectBeforeTabGroup(tabGroup.SettingsResolved.TabOrientation, this.IsInVerticalGroup(tabGroup), this.manager))
				return formRect;

			return Rectangle.Union(formRect, this.GetSplitterControlRectangle(tabGroup));
		}
		#endregion //GetTabGroupDropRect

		#region GetTabGroupFormRect
		internal Rectangle GetTabGroupFormRect( MdiTabGroup tabGroup )
		{
			Rectangle formRect = this.GetFormPosition(tabGroup);

			// adjust by the actual positon of the mdi client
			formRect.Offset(this.MdiClientLocation);

			return formRect;
		}
		#endregion // GetTabGroupFormRect

		// AS 1/5/04 WTB1271
		#region RemoveUnusedTabGroups
		internal void RemoveUnusedTabGroups()
		{
			this.ResyncTabGroupCollection();
		}
		#endregion //RemoveUnusedTabGroups

		#endregion //Internal

		#endregion //Methods
	}

	// Information To Track
	// ====================
	// Location of the TabGroup - where it begins within the mdi client area
	// Size of the TabGroupArea - based on the tab group extent and shared mdi client size (width or height based on orientation)
	// Location of TabGroupControl
	// Size of TabGroupControl
	// Location of SplitterBar
	// Size of SplitterBar
	// Location of Form area
	// Size of Form area
	internal class TabGroupInfo
	{
		#region Member Variables

		private bool		dirty = true;

		private Rectangle	tabGroupArea = Rectangle.Empty;
		private Rectangle	tabGroupControlRect = Rectangle.Empty;
		private Rectangle	splitterRect = Rectangle.Empty;
		private Rectangle	formRect = Rectangle.Empty;

		#endregion //Member Variables

		#region TabGroupInfo
		internal TabGroupInfo()
		{
		}
		#endregion //TabGroupInfo

		#region Properties

		internal Rectangle TabGroupArea
		{
			get { return this.tabGroupArea; }
			set { this.tabGroupArea = value; }
		}

		internal Rectangle TabGroupControlRectangle
		{
			get { return this.tabGroupControlRect; }
			set { this.tabGroupControlRect = value; }
		}

		internal Rectangle SplitterRectangle
		{
			get { return this.splitterRect; }
			set { this.splitterRect = value; }
		}

		internal Rectangle FormRectangle
		{
			get { return this.formRect; }
			set { this.formRect = value; }
		}

		#region Dirty
		internal bool Dirty
		{
			get { return this.dirty; }
			set { this.dirty = value; }
		}
		#endregion //Dirty

		#endregion //Properties
	}
}
