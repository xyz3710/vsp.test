#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTabs;
using System.Security;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	internal class Utilities
	{
		#region Constructor
		private Utilities(){}
		#endregion //Constructor

		#region ToTabScrollButtons
		internal static TabScrollButtons ToTabScrollButtons(DefaultableTabScrollButtons buttons)
		{
			switch(buttons)
			{
				case DefaultableTabScrollButtons.None:
					return TabScrollButtons.None;
				case DefaultableTabScrollButtons.Always:
					return TabScrollButtons.Always;
				default:
				case DefaultableTabScrollButtons.Default:
				case DefaultableTabScrollButtons.Automatic:
					return TabScrollButtons.Automatic;
			}
		}
		#endregion //ToTabScrollButtons

		#region ShouldSetAppearanceProperty
		internal static bool ShouldSetAppearanceProperty( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, AppearancePropFlags appearanceProperty )
		{
			if (appearance.HasPropertyBeenSet(appearanceProperty))
				return false;

			return (requestedProps & appearanceProperty) == appearanceProperty;
		}
		#endregion //ShouldSetAppearanceProperty

		// MD 12/15/06 - FxCop
		#region ActivateForm

		internal static void ActivateForm( Form form )
		{
			try
			{
				form.Activate();
			}
			catch ( SecurityException )
			{
				if ( form.Visible == false ||
					form.IsHandleCreated == false )
					return;

				if ( form.IsMdiChild )
				{
					MdiClient mdiClient = GetParent( form ) as MdiClient;

					if ( mdiClient != null )
					{
						try
						{
							NativeWindowMethods.SendMessageApi(
								mdiClient.Handle,
								NativeWindowMethods.WM_MDIACTIVATE,
								form.Handle,
								IntPtr.Zero );
						}
						catch ( SecurityException ) { }
					}
				}
				else
				{
					try
					{
						NativeWindowMethods.SetForegroundWindowApi( form.Handle );
					}
					catch ( SecurityException ) { }
				}
			}
		}

		#endregion // ActivateForm

		#region Commented Out Code

		// SSP 11/4/05 BR07555
		// Moved the CreateLargestFont to the Win's Utilities. This is being used from Misc
		// as well.
		// 
		
		#endregion // Commented Out Code

		// MD 12/18/06 - FxCop
		#region FocusControl

		internal static bool FocusControl( Control control )
		{
			try
			{
				return control.Focus();
			}
			catch ( SecurityException )
			{
				try
				{
					if ( control.CanFocus )
						NativeWindowMethods.SetFocusApi( control.Handle );
				}
				catch ( SecurityException ) { }
			}

			return control.Focused;
		}

		#endregion FocusControl

		// MD 12/15/06 - FxCop
		#region GetMdiParent

		internal static Form GetMdiParent( Form form )
		{
			try
			{
				UIPermission perm = new UIPermission( UIPermissionWindow.AllWindows );
				perm.Assert();

				return form.MdiParent;
			}
			catch ( SecurityException )
			{
				return null;
			}
		}

		#endregion // GetMdiParent

		// MD 12/15/06 - FxCop
		#region GetParent

		internal static Control GetParent( Control control )
		{
			try
			{
				UIPermission perm = new UIPermission( UIPermissionWindow.AllWindows );
				perm.Assert();

				return control.Parent;
			}
			catch ( SecurityException )
			{
				return null;
			}
		}

		#endregion // GetParent

		#region IsVerticalTabOrientation
		internal static bool IsVerticalTabOrientation( TabOrientation tabOrientation )
		{
			switch(tabOrientation)
			{
				default:
				case TabOrientation.BottomLeft:
				case TabOrientation.BottomRight:
				case TabOrientation.TopLeft:
				case TabOrientation.TopRight:
					return false;
				case TabOrientation.LeftBottom:
				case TabOrientation.LeftTop:
				case TabOrientation.RightBottom:
				case TabOrientation.RightTop:
					return true;
			}

		}
		#endregion //IsVerticalTabOrientation
	}
}
