#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win.UltraWinTabs;
using Infragistics.Win.UltraWinToolbars;

// AS 4/21/03 WTB818
using Infragistics.Win.IGControls;

// serialization
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using Infragistics.Shared.Serialization;

using System.Collections;
using System.Collections.Specialized;
using System.Threading;

using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// A component for displaying mdi child forms as tab items.
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>UltraTabbedMdiManager</b> provides a Visual Studio .Net style tabbed interface. Each mdi <see cref="System.Windows.Forms.Form"/> in the 
	/// <see cref="System.Windows.Forms.Form.MdiChildren"/> of the <see cref="MdiParent"/> is represented by a tab.</p>
	/// <p class="body">An <see cref="MdiTab"/> instance is created for each mdi child form and provides the information for the display of the tab. By default the text 
	/// displayed in the tab item is based on the text of the associated <see cref="MdiTab.Form"/> but can be overriden using the <see cref="MdiTab.Text"/> property. Also, the form's
	/// icon may be displayed in the tab using the <see cref="MdiTabSettings.DisplayFormIcon"/> property.</p>
	/// <p class="body">The MdiTab objects for visible forms are moved into an <see cref="MdiTabGroup"/> when they are displayed and may be 
	/// repositioned to other <see cref="TabGroups"/> or to a new <b>MdiTabGroup</b> (<see cref="MoveToNewGroup(MdiTab)"/>). MdiTab objects whose 
	/// associated forms are not visible are maintained in the <see cref="HiddenTabs"/> collection. When a hidden form/tab is made visible, 
	/// the <see cref="TabDisplaying"/> event is invoked when the tab is about to displayed.</p>
	/// <p class="body">The <see cref="TabSettings"/> is used to set default values for the appearance and behavior of the <see cref="MdiTab"/> objects. 
	/// The values can then be overriden by the containing <see cref="MdiTab.TabGroup"/> using the <see cref="MdiTabGroup.TabSettings"/> and can be 
	/// further controlled at the specific tab using its <see cref="MdiTab.Settings"/>. The appearances (e.g. <see cref="MdiTabSettings.TabAppearance"/>) of the 
	/// <see cref="TabSettings"/> is used to control the colors and images displayed by the tab items. Similarly, the <see cref="TabGroupSettings"/> 
	/// is used to set the default values for all the <see cref="TabGroups"/>. These values can be overriden for specific <see cref="MdiTabGroup"/> objects 
	/// using the <see cref="MdiTabGroup.Settings"/> property. The <b>TabGroupSettings</b> property is useful to initialize the 
	/// <see cref="MdiTabGroupSettings.TabStyle"/> and <see cref="MdiTabGroupSettings.TabSizing"/> as well as other appearance settings. See the 
	/// <see cref="MdiTabGroupSettings"/> class and <see cref="MdiTabSettings"/> class for more information on what may be affected using these properties.</p>
	/// </remarks>
	[ToolboxBitmap(typeof(UltraTabbedMdiManager), AssemblyVersion.ToolBoxBitmapFolder + "UltraTabbedMdiManager.bmp")] // MRS 11/13/05
	
	
	[ToolboxItemFilter("System.Windows.Forms",ToolboxItemFilterType.Allow)]
	[DefaultProperty("TabGroups")]
	[DefaultEvent("InitializeTab")]
	[Designer(typeof(Design.UltraTabbedMdiManagerDesigner), typeof(IDesigner))]
	[LocalizedDescription("LD_UltraTabbedMdiManager")] // JAS BR06191 10/10/05
	public class UltraTabbedMdiManager : Infragistics.Win.UltraComponentControlManagerBase,
		IUltraLicensedComponent,
		IImageListProvider,
		ISupportInitialize,
		ITabbedMdiManager
        // MD 10/15/04 - Default Store Support
        , System.Configuration.IPersistComponentSettings
	{
		#region Member Variables

		#region Public Property Members

		private TabbedMdiEventManager				eventManager = null;
		private AppearancesCollection				appearances = null;
		private ImageList							imageList = null;
		private MdiTabGroupSettings					tabGroupSettings = null;
		private MdiTabSettings						tabSettings = null;
		private MdiTabGroupsCollection				tabGroups = null;

		private const bool							AllowHorizontalTabGroupsDefault = true;
		private const bool							AllowVerticalTabGroupsDefault = true;
		private const bool							ShowToolTipsDefault = true;
		private const bool							UseMnemonicsDefault = false;
		private bool								allowHorizontalTabGroups = AllowHorizontalTabGroupsDefault;
		private bool								allowVerticalTabGroups = AllowVerticalTabGroupsDefault;
		private bool								showToolTips = ShowToolTipsDefault;
		private bool								useMnemonics = UseMnemonicsDefault;

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		//private const Orientation					OrientationDefault = Orientation.Horizontal;
		internal const Orientation					OrientationDefault = Orientation.Horizontal;

		private Orientation							orientation = OrientationDefault;
		private UIElementBorderStyle				splitterBorderStyle = UIElementBorderStyle.Default;

		private AppearanceHolder					appearanceHolder = null;
		private AppearanceHolder					splitterAppearanceHolder = null;

		private Color								imageTransparentColor = Color.Transparent;

		// AS 5/18/06 BR10993
		//private static readonly Size				ImageSizeDefault = new System.Drawing.Size(16,16);
		internal static readonly Size ImageSizeDefault = new System.Drawing.Size(16, 16);
		private Size imageSize = ImageSizeDefault;

		// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
		// Changed default MaxTabGroups to 0 (unlimited).
		//
		//private const int							MaxTabGroupsDefault = 5;
		private const int							MaxTabGroupsDefault = 0;
		private int									maxTabGroups = MaxTabGroupsDefault;
		private const int							SplitterWidthDefault = 5;
		private int									splitterWidth = SplitterWidthDefault;

		private Form								mdiParent = null;

		private HiddenMdiTabsCollection				hiddenTabs = null;

		// AS 4/29/03 WTB880
		private const MdiClientBorderStyle			BorderStyleDefault = MdiClientBorderStyle.Solid;
		private MdiClientBorderStyle				borderStyle = BorderStyleDefault;

		// AS 5/1/03 WTB880
		private Color								borderColor = Color.Empty;

		// AS 5/2/03 WTB884
		private bool								allowExternalDrop = false;

		//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
		// AS 5/2/06 AppStyling
		//private ViewStyle							viewStyle = ViewStyle.Standard;
		private ViewStyle							viewStyle = ViewStyle.Default;

		// AS 3/7/05 NA 2005 Vol 2 - Nested TabGroups
		private DefaultableBoolean					allowNestedTabGroups = DefaultableBoolean.Default;

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		private const MaximizedMdiTabGroupDisplayStyle	
			MaximizedTabGroupDisplayStyleDefault = MaximizedMdiTabGroupDisplayStyle.CompressUnmaximizedGroups;
		private MaximizedMdiTabGroupDisplayStyle	maximizedTabGroupDisplayStyle = MaximizedTabGroupDisplayStyleDefault;

		private const bool							IsActiveTabGroupMaximizedDefault = false;
		private bool								isActiveTabGroupMaximized = IsActiveTabGroupMaximizedDefault;

		private const bool							AllowMaximizeDefault = false;
		private bool								allowMaximize = AllowMaximizeDefault;

		// AS 5/22/07 BR23151 - MdiTabNavigationMode 
		private const MdiTabNavigationMode			TabNavigationModeDefault = MdiTabNavigationMode.ActivationOrder;
		private MdiTabNavigationMode				tabNavigationMode = TabNavigationModeDefault;

		#endregion //Public Property Members

		#region Internal/Protected Members

		private SubObjectPropChangeEventHandler		subObjectPropChangeHandler = null;
		private bool								isInitializing = false;
		private UltraLicense						license = null;

		private bool								hookedMdiParent = false;
		private bool								hookedMdiClient = false;

		// AS 5/1/03 WTB880
		//private object								mdiClientSubclasser = null;
		private IMdiClientWindow					mdiClientSubclasser = null;

		// AS 3/8/05 BR02542
		private IMdiParentWindow					mdiParentSubclasser = null;

		private bool								isActivatingMdiChild = false;

		private bool								isRestoringTabs = false;
		private MdiClient							mdiClient = null;
		private bool								isDisposing = false;
		private bool								isSubclasserActivatingMdiChild = false;

		private HybridDictionary					tabGroupControls = null;
		private HybridDictionary					splitterControls = null;

		private TabGroupInfoManager					tabGroupManager = null;

		// AS 5/5/06 AppStyling
		//private ResolveAppearanceCallback			splitterAppearanceCallback = null;
		private ResolveAppearanceCallback			splitterAppearanceCallbackVert = null;
		private ResolveAppearanceCallback			splitterAppearanceCallbackHorz = null;

		private MdiTabGroup							lastActiveTabGroup = null;

		private int									suspendLayoutRequests = 0;
		private int									currentTabGroupExtentVersion = int.MinValue;
		private bool								invokeTabGroupResized = false;
		private bool								suspendExtentInitialization = false;

		private Size								lastMdiClientSize = Size.Empty;

		private MdiTab								cachedActiveTab = null;

		private int									minDelta = 0;
		private int									maxDelta = 0;

		// menu related

		// AS 4/21/03 WTB818
		private IGContextMenu						tabContextMenu = null;
		private MenuItem[]							menuItems = null;
		private EventHandler						menuItemClickHandler = null;
		private EventHandler						menuItemDisposedHandler = null;
		private MdiTab								tabForContextMenu = null;

		private bool								isReleasingTabs = false;

		private bool								ignoreContextMenuPopup = false;

		// AS 4/12/03 WTB796
		private bool								hookedIdle = false;
		private IAsyncResult						delayedRedraw;

		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		internal const int							NoActiveTabGroupIndex = -1;

		// AS 4/14/03 WTB801
		private int									serializedActiveTabGroupIndex = 0;

		// AS 4/21/03 WTB818
		private Stream								newTabGroupHorzImgStream = null;
		private Stream								newTabGroupVertImgStream = null;
		// JDN 8/16/04 NAS2004 Vol3 - VisualStudio2005 style
		private Stream								newTabGroupVertImgStreamVS2005 = null;
		private Bitmap								newTabGroupHorzImage = null;
		private Bitmap								newTabGroupVertImage = null;
		// JDN 8/16/04 NAS2004 Vol3 - VisualStudio2005 style
		private Bitmap								newTabGroupVertImageVS2005 = null;

		// AS 4/23/03
		private bool								ensuringTabIsSelected = false;

		// AS 4/28/03 WTB873
		private bool								positioningControls = false;

		// AS 4/29/03 WTB880
		private bool								hasUnmanagedCodeRights = true;

		// AS 5/9/03 WTB884
		private DragDropHelper						dragHelper = null;

		// AS 6/11/03
		// We need a flag since we don't want to create a tab
		// for a form if it does not yet exist.
		//
		private bool								restoringTabGroup = false;

		// AS 1/5/04 WTB1271
		private IContainer							container;

        internal const ControlStyles                DoubleBufferControlStyle = ControlStyles.OptimizedDoubleBuffer;

        // MD 10/15/04 - Default Store Support
        private bool								saveSettings = false;
        private string								settingsKey = "";
        private UltraTabbedMdiManager.UltraTabbedMdiManagerApplicationSettings 
													settings;
        private SaveSettingsFormat					saveSettingsFormat = SaveSettingsFormat.Binary;

		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		internal const int							RootTabGroupLevel = 0;

		// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
		private MdiTab								lastActivatedTab = null;

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		private MdiTabGroup							maximizedTabGroup = null;

		// AS 3/25/05
		private bool								isDraggingTab = false;

		// JJD 8/22/06 - NA 2006 vol 3
		// Added support for a preferred view style
		private IToolbarsManager toolbarsManager;

		#endregion //Internal/Protected Members

 		#endregion //Member Variables
  
		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="UltraTabbedMdiManager"/>
		/// </summary>
		public UltraTabbedMdiManager()
		{
			// verify and cache the license
			//
			// AS 3/5/03 DNF37
			// Wrapped in a try/catch for a FileNotFoundException.
			// When the assembly is loaded dynamically, VS seems 
			// to be trying to reload a copy of Shared even though 
			// one is in memory. This generates a FileNotFoundException
			// when the dll is not in the gac and not in the AppBase
			// for the AppDomain.
			//
			try
			{
				this.license = LicenseManager.Validate( typeof(UltraTabbedMdiManager), this ) as UltraLicense;
			}
			catch (System.IO.FileNotFoundException)	{}

			// JJD 8/23/06 - NA2006 vol 3
			// Hook into the Offixe 2007 view style change event
			Office2007ColorTable.ColorSchemeChanged += new EventHandler(OnOffice2007ColorSchemeChanged);
		}

		// AS 1/5/04 WTB1271
		/// <summary>
		/// Initializes a new <see cref="UltraTabbedMdiManager"/> component
		/// </summary>
		/// <param name="container">An IContainer representing the container of the UltraTabbedMdiManager</param>
		public UltraTabbedMdiManager(IContainer container) : this()
		{
			this.container = container;

			if ( this.container != null )
				this.container.Add( this );
		}

		#endregion //Constructor

		#region Properties

		#region Public

		#region EventManager
		/// <summary>
		/// The object that enables, disables and controls firing of <b>UltraTabbedMdiManager</b> specific events.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public TabbedMdiEventManager EventManager
		{
			get
			{
				if (this.eventManager == null)
					this.eventManager = new TabbedMdiEventManager(this);

				return this.eventManager;
			}
		}
		#endregion //EventManager

		#region Appearances
		/// <summary>
		/// Returns a collection of user created <see cref="Infragistics.Win.Appearance"/> objects.
		/// </summary>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_Appearances")]
		[LocalizedCategory("LC_Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public AppearancesCollection Appearances
		{
			get
			{
				if (null == this.appearances)
				{
					this.appearances = new AppearancesCollection(this);
					this.appearances.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.appearances;
			}
		}

		/// <summary>
		/// Indicates whether an <see cref="AppearancesCollection"/> object has been created.
		/// </summary>
		/// <value>Returns True when the <b>AppearancesCollection</b> object for the <see cref="Appearances"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearancesCollection"/> object has been created.  <b>AppearancesCollection</b> objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="Appearances"/>
		/// <seealso cref="AppearancesCollection"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasAppearances
		{
			get { return this.appearances != null; }
		}

		/// <summary>
		/// <p class="body">Returns a Boolean value that determines whether the <see cref="UltraTabbedMdiManager.Appearances"/> property is set to its default value.</p>
		/// </summary>
		/// <remarks>
		/// <p class="body">Returns True if the <see cref="UltraTabbedMdiManager.Appearances"/> property is not set to its default value; otherwise, it returns False.</p>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// <p class="body">Invoke the <see cref="UltraTabbedMdiManager.ResetAppearances"/> method to reset this property to its default value.</p>
		/// </remarks>
		[EditorBrowsable( EditorBrowsableState.Never)]
		protected bool ShouldSerializeAppearances()
		{
			return (this.appearances != null && this.appearances.Count > 0);
		}

		/// <summary>
		/// Resets the <see cref="UltraTabbedMdiManager.Appearances"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to reset the <see cref="UltraTabbedMdiManager.Appearances"/> property to its default value.</p>
		/// <p class="body">Once this method is invoked, the <see cref="UltraTabbedMdiManager.ShouldSerializeAppearances"/> method will return False until the <see cref="UltraTabbedMdiManager.Appearances"/> property is set again.</p>
		/// </remarks>
		[EditorBrowsable( EditorBrowsableState.Never)]
		public void ResetAppearances()
		{
			if (this.appearances != null)
			{
				// unsubscribe from old appearance notifications
				this.appearances.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.appearances = null;

				// Notify listeners that the property has changed.
				this.NotifyPropChange(TabbedMdiPropertyIds.Appearances);
			}
		}
		#endregion //Appearances

		#region ImageList
		/// <summary>
		/// Returns or sets the <b>ImageList</b> providing the images for the component.
		/// </summary>
		/// <remarks>
		/// <p class="body">When an <b>ImageList</b> has been assigned, the <see cref="AppearanceBase.Image"/> property of 
		/// the various tab appearances (<see cref="MdiTabSettings.TabAppearance"/>, <see cref="MdiTabSettings.SelectedTabAppearance"/>, etc.) 
		/// may be set to an integer value indicating the index of the image in the <b>ImageList</b>. Alternatively, tabs can 
		/// display the <see cref="Form.Icon"/> of the associated <see cref="MdiTab.Form"/> using the 
		/// <see cref="MdiTabSettings.DisplayFormIcon"/> property.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_ImageList")]
		[LocalizedCategory("LC_Appearance")]
		[DefaultValue(null)]
		public ImageList ImageList
		{
			get	{ return this.imageList; }
			set
			{
				if (this.imageList == value)
					return;

				this.imageList = value;

				// dirty the tab group's tabmanager as needed...
				if (!this.IsInitializing)
					this.DirtyTabGroups(TabbedMdiPropertyIds.ImageList);

				// AS 4/21/03 WTB818
				if (this.tabContextMenu != null)
					this.tabContextMenu.ImageList = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ImageList );
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="ImageList"/> property needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ImageList"/> property is not null.</returns>
		/// <seealso cref="UltraTabbedMdiManager.ImageList"/>
		/// <seealso cref="System.Windows.Forms.ImageList"/>
		protected bool ShouldSerializeImageList()
		{
			return this.imageList != null;
		}

		/// <summary>
		/// Resets the <see cref="ImageList"/> property to its default value.
		/// </summary>
		/// <seealso cref="UltraTabbedMdiManager.ImageList"/>
		/// <seealso cref="System.Windows.Forms.ImageList"/>
		public void ResetImageList()
		{
			this.ImageList = null;
		}
		#endregion //ImageList

		#region ActiveTab
		/// <summary>
		/// Returns the <see cref="MdiTab"/> instance representing the current <see cref="System.Windows.Forms.Form.ActiveMdiChild"/> of the <see cref="MdiParent"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ActiveTab</b> is the <see cref="MdiTab"/> object representing the 
		/// <see cref="Form.ActiveMdiChild"/> of the <see cref="MdiParent"/>. You can access the tabs for 
		/// specific <see cref="Form"/> objects using the <see cref="TabFromForm"/> method.</p>
		/// <p class="body">Since the <b>ActiveTab</b> represents the active mdi tab and mdi child form activation is 
		/// maintained by their zorder, the <b>ActiveTab</b> must be the <see cref="MdiTabGroup.SelectedTab"/> of its 
		/// <see cref="MdiTab.TabGroup"/>. Since the ActiveTab must be selected, as the <b>ActiveTab</b> changes, the 
		/// <see cref="TabSelecting"/> event will be invoked.</p>
		/// <p class="note">Note: The <see cref="TabSelecting"/> event may not always be cancellable since the 
		/// activation may occur as a result of an external action - e.g. when the mdi child is first created.</p>
		/// </remarks>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public MdiTab ActiveTab
		{
			get
			{
				if (this.MdiParent == null)
					return null;

				Form activeMdiChild = this.MdiParent.ActiveMdiChild;

				if (activeMdiChild == null)
					return null;

				bool allowCreate = !this.IsInitializing && 
					!this.IsReleasingTabs				&& 
					!this.IsRestoringTabs				&& 
					!this.IsDisposing;

				// todo - should this create if null?
				return this.GetTabFromForm(activeMdiChild, true, false, allowCreate);
			}
		}
		#endregion //ActiveTab

		#region TabSettings
		/// <summary>
		/// Returns an <see cref="MdiTabSettings"/> instance used to control the default settings for the <see cref="MdiTab"/> objects.
		/// </summary>
		/// <seealso cref="MdiTab.Settings"/>
		/// <seealso cref="MdiTabGroup.TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_TabSettings")]
		public MdiTabSettings TabSettings
		{
			get
			{
				if (this.tabSettings == null)
				{
					this.tabSettings = new MdiTabSettings(this);
					this.tabSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.tabSettings;
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="TabSettings"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabSettings"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>MdiTabSettings</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		protected bool ShouldSerializeTabSettings()
		{
			// JJD 5/19/03 - WTB947
			// Return true also if ForceSerialization returns true
			//return this.tabSettings != null && this.tabSettings.ShouldSerialize();
			return this.TabSettings.ShouldSerialize() || this.tabSettings.ForceSerialization;
		}

		/// <summary>
		/// Indicates whether an <see cref="MdiTabSettings"/> object has been created.
		/// </summary>
		/// <value>Returns True when the <b>MdiTabSettings</b> object for the <see cref="TabSettings"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="MdiTabSettings"/> object has been created.  <b>MdiTabSettings</b> objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabSettings
		{
			get { return this.tabSettings != null; }
		}

		/// <summary>
		/// Resets the <see cref="TabSettings"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the properties of the <see cref="MdiTabSettings"/> maintained by the <see cref="TabSettings"/> property.</p>
		/// </remarks>
		/// <seealso cref="TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		/// <seealso cref="MdiTabSettings.Reset"/>
		public void ResetTabSettings()
		{
			if (this.HasTabSettings)
				this.TabSettings.Reset();
		}
		#endregion //TabSettings

		#region TabGroupSettings
		/// <summary>
		/// Returns an <see cref="MdiTabGroupSettings"/> instance used to control the default settings for the <see cref="TabGroups"/>.
		/// </summary>
		/// <seealso cref="MdiTabGroup.Settings"/>
		/// <seealso cref="MdiTabGroupSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_TabGroupSettings")]
		public MdiTabGroupSettings TabGroupSettings
		{
			get
			{
				if (this.tabGroupSettings == null)
				{
					this.tabGroupSettings = new MdiTabGroupSettings(this);
					this.tabGroupSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.tabGroupSettings;
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="TabGroupSettings"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabGroupSettings"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>MdiTabGroupSettings</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="TabGroupSettings"/>
		/// <seealso cref="MdiTabGroupSettings"/>
		protected bool ShouldSerializeTabGroupSettings()
		{
			// JJD 5/19/03 - WTB947
			// Return true also if ForceSerialization returns true
			//return this.tabGroupSettings != null && this.tabGroupSettings.ShouldSerialize();
			return this.TabGroupSettings.ShouldSerialize() || this.tabGroupSettings.ForceSerialization;
		}

		/// <summary>
		/// Indicates whether an <see cref="MdiTabGroupSettings"/> object has been created.
		/// </summary>
		/// <value>Returns True when the <b>MdiTabGroupSettings</b> object for the <see cref="TabGroupSettings"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="MdiTabGroupSettings"/> object has been created.  <b>MdiTabGroupSettings</b> objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="TabGroupSettings"/>
		/// <seealso cref="MdiTabGroupSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabGroupSettings
		{
			get { return this.tabGroupSettings != null; }
		}

		/// <summary>
		/// Resets the <see cref="TabGroupSettings"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the properties of the <see cref="MdiTabGroupSettings"/> maintained by the <see cref="TabGroupSettings"/> property.</p>
		/// </remarks>
		/// <seealso cref="TabGroupSettings"/>
		/// <seealso cref="MdiTabGroupSettings"/>
		/// <seealso cref="MdiTabGroupSettings.Reset"/>
		public void ResetTabGroupSettings()
		{
			if (this.HasTabGroupSettings)
				this.TabGroupSettings.Reset();
		}
		#endregion //TabGroupSettings

		#region TabGroups
		/// <summary>
		/// Returns a collection of <see cref="MdiTabGroup"/> instances
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>MdiTabGroup</b> objects are created and destroyed as needed. When all of 
		/// the <see cref="MdiTab"/> objects in a tab group's <see cref="MdiTabGroup.Tabs"/> collection have 
		/// been closed, hidden or moved to other tab groups, the tab group is removed from the collection and 
		/// released. When there are no tab groups and a tab is being displayed or when a tab is explicitly moved 
		/// to a new tab group (e.g. using the <see cref="MoveToNewGroup(MdiTab)"/> method), a new 
		/// <b>MdiTabGroup</b> is created and the <see cref="InitializeTabGroup"/> event is invoked to 
		/// allow the tab group settings to be initialized.</p>
		/// <p class="note"><b>Note:</b> When an MdiTabGroup that contains child MdiTabGroups in its 
		/// <see cref="MdiTabGroup.TabGroups"/> only has 1 MdiTabGroup, the children of that tab group 
		/// are promoted and the tab group that contained only the single child tab group is removed.</p>
		/// </remarks>
		// AS 4/29/03 This was previously removed and stripped using the designer 
		// at design time so it can be accessed using a property grid at runtime if 
		// desired. It is not exposed at design time because no tab groups exist at 
		// design time.
		//
		//[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public MdiTabGroupsCollection TabGroups
		{
			get
			{
				if (this.tabGroups == null)
				{
					this.tabGroups = new MdiTabGroupsCollection();
					this.tabGroups.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.tabGroups;
			}
		}

		/// <summary>
		/// Indicates if any of the <see cref="MdiTabGroup"/> objects contains data to serialize.
		/// </summary>
		/// <returns>True if any of the properties of the <see cref="MdiTabGroup"/> objects in the collection differ from their default values.</returns>
		/// <seealso cref="TabGroups"/>
		protected bool ShouldSerializeTabGroups()
		{
			if (!this.HasTabGroups)
				return false;

			foreach(MdiTabGroup tabGroup in this.TabGroups)
			{
				if (tabGroup.ShouldSerialize())
					return true;
			}

			return false;
		}

		/// <summary>
		/// Resets the properties of all the <see cref="MdiTabGroup"/> objects in the <see cref="TabGroups"/> collection to their default values.
		/// </summary>
		/// <seealso cref="TabGroups"/>
		public void ResetTabGroups()
		{
			if (!this.HasTabGroups)
				return;

			foreach(MdiTabGroup tabGroup in this.TabGroups)
			{
				tabGroup.Reset();

				// initialize the extents to empty so they will all be the same size
				tabGroup.InitializeExtent(0);
			}

			

			// AS 4/16/03
			this.InitializeTabGroupExtents(true);
		}

		/// <summary>
		/// Indicates if the <see cref="TabGroups"/> collection has been created and contains at least one <see cref="MdiTabGroup"/> instance
		/// </summary>
		/// <seealso cref="TabGroups"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabGroups
		{
			get { return this.tabGroups != null && this.tabGroups.Count > 0; }
		}
		#endregion //TabGroups

		#region AllowHorizontalTabGroups
		/// <summary>
		/// Returns or sets whether the end user may create horizontally oriented tab groups.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <see cref="Orientation"/> property determines how the 
		/// <see cref="MdiTabGroup"/> objects in the <see cref="TabGroups"/> collection are 
		/// laid out. When there is only 1 tab group and the <see cref="MaxTabGroups"/> is 
		/// greater than 1, the end user may create either a <b>Horizontal</b> or <b>Vertical</b> 
		/// tab group by either dragging a tab near the edges of the mdi client or using the 
		/// context menu. Setting the <b>AllowHorizontalTabGroups</b> to false (true is the default 
		/// value), will prevent the user from being able to create a horizontal tab group. Also, if 
		/// this property is set to false, the user will not be able to create additional horizontal 
		/// tab groups.</p>
		/// <p class="note"><b>Note:</b> This property only affects the ability to create horizontal root 
		/// tab groups. When true and <see cref="AllowNestedTabGroups"/> is true and the Orientation is 
		/// vertical, dragging and releasing a tab within a few pixels of the outside top or bottom mdi client 
		/// edge will create a new tab group for the tab and another that will be used to nest the tabgroups that 
		/// existed in the <see cref="TabGroups"/> collection. The orientation will then be toggled to 
		/// horizontal.</p>
		/// </remarks>
		/// <seealso cref="AllowVerticalTabGroups"/>
		/// <seealso cref="MaxTabGroups"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_AllowHorizontalTabGroups")]
		[LocalizedCategory("LC_Behavior")]
		public bool AllowHorizontalTabGroups
		{
			get { return this.allowHorizontalTabGroups; }
			set
			{
				if (this.allowHorizontalTabGroups == value)
					return;

				this.allowHorizontalTabGroups = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.AllowHorizontalTabGroups );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AllowHorizontalTabGroups"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AllowHorizontalTabGroups"/> differs from the default value.</returns>
		/// <seealso cref="AllowHorizontalTabGroups"/>
		public bool ShouldSerializeAllowHorizontalTabGroups()
		{
			return this.AllowHorizontalTabGroups != AllowHorizontalTabGroupsDefault;
		}	

		/// <summary>
		/// Resets the <see cref="AllowHorizontalTabGroups"/> property to its default value.
		/// </summary>
		/// <seealso cref="AllowHorizontalTabGroups"/>
		public void ResetAllowHorizontalTabGroups()
		{
			this.AllowHorizontalTabGroups = AllowHorizontalTabGroupsDefault;
		}
		#endregion //AllowHorizontalTabGroups

		#region AllowVerticalTabGroups
		/// <summary>
		/// Returns or sets whether the end user may create vertically oriented tab groups.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <see cref="Orientation"/> property determines how the 
		/// <see cref="MdiTabGroup"/> objects in the <see cref="TabGroups"/> collection are 
		/// laid out. When there is only 1 tab group and the <see cref="MaxTabGroups"/> is 
		/// greater than 1, the end user may create either a <b>Horizontal</b> or <b>Vertical</b> 
		/// tab group by either dragging a tab near the edges of the mdi client or using the 
		/// context menu. Setting the <b>AllowVerticalTabGroups</b> to false (true is the default 
		/// value), will prevent the user from being able to create a horizontal tab group. Also, if 
		/// this property is set to false, the user will not be able to create additional vertical 
		/// tab groups.</p>
		/// <p class="note"><b>Note:</b> This property only affects the ability to create vertical root 
		/// tab groups. When true and <see cref="AllowNestedTabGroups"/> is true and the Orientation is 
		/// horizontal, dragging and releasing a tab within a few pixels outside of the left or right mdi client 
		/// edge will create a new tab group for the tab and another that will be used to nest the tabgroups that 
		/// existed in the <see cref="TabGroups"/> collection. The orientation will then be toggled to 
		/// vertical.</p>
		/// </remarks>
		/// <seealso cref="AllowHorizontalTabGroups"/>
		/// <seealso cref="MaxTabGroups"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_AllowVerticalTabGroups")]
		[LocalizedCategory("LC_Behavior")]
		public bool AllowVerticalTabGroups
		{
			get { return this.allowVerticalTabGroups; }
			set
			{
				if (this.allowVerticalTabGroups == value)
					return;

				this.allowVerticalTabGroups = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.AllowVerticalTabGroups );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AllowVerticalTabGroups"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AllowVerticalTabGroups"/> differs from the default value.</returns>
		public bool ShouldSerializeAllowVerticalTabGroups()
		{
			return this.AllowVerticalTabGroups != AllowVerticalTabGroupsDefault;
		}	

		/// <summary>
		/// Resets the <see cref="AllowVerticalTabGroups"/> property to its default value.
		/// </summary>
		public void ResetAllowVerticalTabGroups()
		{
			this.AllowVerticalTabGroups = AllowVerticalTabGroupsDefault;
		}
		#endregion //AllowVerticalTabGroups

		// AS 3/7/05 NA 2005 Vol 2 - Nested TabGroups
		#region AllowNestedTabGroups
		/// <summary>
		/// Returns or sets whether the end user may create nested groups.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, <b>AllowNestedTabGroups</b> resolves to false. When this property 
		/// resolves to true, the end user may drag tabs (depending upon their <see cref="MdiTabSettings.AllowDrag"/>) 
		/// to create nested tab groups. A nested tab group is an <see cref="MdiTabGroup"/> that contains MdiTabGroups 
		/// instead of directly containing <see cref="MdiTab"/> instances. Nested tab groups allow for more control 
		/// over the layout of the tabs.</p>
		/// </remarks>
		/// <seealso cref="AllowHorizontalTabGroups"/>
		/// <seealso cref="AllowVerticalTabGroups"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_AllowNestedTabGroups")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean AllowNestedTabGroups
		{
			get { return this.allowNestedTabGroups; }
			set
			{
				if (this.allowNestedTabGroups == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.allowNestedTabGroups = value;

				// since this affects the splitter position, dirty the tab groups
				if (!this.IsInitializing)
					this.TabGroupManager.DirtyAllTabGroups();

				this.NotifyPropChange( TabbedMdiPropertyIds.AllowNestedTabGroups );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AllowNestedTabGroups"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AllowNestedTabGroups"/> differs from the default value.</returns>
		public bool ShouldSerializeAllowNestedTabGroups()
		{
			return this.AllowNestedTabGroups != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="AllowNestedTabGroups"/> property to its default value.
		/// </summary>
		public void ResetAllowNestedTabGroups()
		{
			this.AllowNestedTabGroups = DefaultableBoolean.Default;
		}

		/// <summary>
		/// Indicates if the end-user can create nested tab groups.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool AllowNestedTabGroupsResolved
		{
			get { return this.allowNestedTabGroups == DefaultableBoolean.True; }
		}
		#endregion //AllowNestedTabGroups

		#region ShowToolTips
		/// <summary>
		/// Returns or sets whether tooltips are displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, <b>ShowToolTips</b> is true. When true, tooltips are displayed 
		/// for tabs and buttons (scroll buttons and close button). Tooltips are only displayed for 
		/// tabs when either the <see cref="MdiTab.ToolTip"/> property is set or there is not enough 
		/// room to display the <see cref="MdiTab.TextResolved"/> for the tab.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_ShowToolTips")]
		[LocalizedCategory("LC_Behavior")]
		public bool ShowToolTips
		{
			get { return this.showToolTips; }
			set
			{
				if (this.showToolTips == value)
					return;

				this.showToolTips = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ShowToolTips );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ShowToolTips"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ShowToolTips"/> differs from the default value.</returns>
		/// <seealso cref="ShowToolTips"/>
		public bool ShouldSerializeShowToolTips()
		{
			return this.ShowToolTips != ShowToolTipsDefault;
		}	

		/// <summary>
		/// Resets the <see cref="ShowToolTips"/> property to its default value.
		/// </summary>
		/// <seealso cref="ShowToolTips"/>
		public void ResetShowToolTips()
		{
			this.ShowToolTips = ShowToolTipsDefault;
		}
		#endregion //ShowToolTips

		#region UseMnemonics
		/// <summary>
		/// Returns or sets whether the control interprets an ampersand character (&amp;) in the MdiTabs <see cref="MdiTab.Text"/> property to be an access key prefix character.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, ampersands are not interpretted as mnemonic characters. When set to true, 
		/// pressing Alt and the mnemonic character (the character following the &amp;) will activate the associated 
		/// form.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_UseMnemonics")]
		[LocalizedCategory("LC_Behavior")]
		public bool UseMnemonics
		{
			get { return this.useMnemonics; }
			set
			{
				if (this.useMnemonics == value)
					return;

				this.useMnemonics = value;

				// dirty the tab group's tabmanager as needed...
				this.DirtyTabGroups(TabbedMdiPropertyIds.UseMnemonics);

				this.NotifyPropChange( TabbedMdiPropertyIds.UseMnemonics );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="UseMnemonics"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="UseMnemonics"/> differs from the default value.</returns>
		/// <seealso cref="UseMnemonics"/>
		public bool ShouldSerializeUseMnemonics()
		{
			return this.UseMnemonics != UseMnemonicsDefault;
		}	

		/// <summary>
		/// Resets the <see cref="UseMnemonics"/> property to its default value.
		/// </summary>
		/// <seealso cref="UseMnemonics"/>
		public void ResetUseMnemonics()
		{
			this.UseMnemonics = UseMnemonicsDefault;
		}
		#endregion //UseMnemonics

		#region Orientation
		/// <summary>
		/// Returns or sets the orientation of the tab groups.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Orientation</b> determines how the <see cref="TabGroups"/> are laid out. 
		/// The tab groups are either displayed with a <b>Horizontal</b> split and laid out from 
		/// top to bottom or they are displayed with a <b>Vertical</b> split and laid out from left to 
		/// right. When there is only 1 <see cref="MdiTabGroup"/> in the <see cref="TabGroups"/> collection, the 
		/// end user may create a new <b>MdiTabGroup</b> in either orientation unless the 
		/// <see cref="AllowHorizontalTabGroups"/> or <see cref="AllowVerticalTabGroups"/> is set to false.</p>
		/// <p class="body">The <b>Orientation</b> may also be changed programatically to switch between the layout styles. 
		/// This will affect the <see cref="MdiTabGroup.Extent"/> of tab groups in the <see cref="TabGroups"/> collection 
		/// since the sum of the extents must match the extent of the relative client area. In other words, when 
		/// the <b>Orientation</b> is set to Horizontal, the sum of the extents must equal the height 
		/// of the client size of the mdi client. If this is not the case, the tab groups are resized proportionally 
		/// based on their current extents to adjust for the difference.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_Orientation")]
		[LocalizedCategory("LC_Appearance")]
		public Orientation Orientation
		{
			get { return this.orientation; }
			set
			{
				if (this.orientation == value)
					return;

				if (!Enum.IsDefined(typeof(Orientation), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(Orientation));

				this.orientation = value;

				// synchronize the values of the splitter bars
				this.SynchronizeSplitterProperties();

				// reinitialize the tab group extents to align with the
				// altered orientation
				this.InitializeTabGroupExtents(true);

				this.NotifyPropChange( TabbedMdiPropertyIds.Orientation );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="Orientation"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="Orientation"/> differs from the default value.</returns>
		/// <seealso cref="Orientation"/>
		public bool ShouldSerializeOrientation()
		{
			return this.Orientation != OrientationDefault;
		}	

		/// <summary>
		/// Resets the <see cref="Orientation"/> property to its default value.
		/// </summary>
		/// <seealso cref="Orientation"/>
		public void ResetOrientation()
		{
			this.Orientation = OrientationDefault;
		}
		#endregion //Orientation

		#region SplitterBorderStyle
		/// <summary>
		/// Returns or sets the border style for the splitter bars.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SplitterBorderStyle</b> determines the style of the border for 
		/// the splitter bar displayed between <see cref="MdiTabGroup"/> instances. Each <b>MdiTabGroup</b> 
		/// except the last item in the <see cref="TabGroups"/> will have a splitter that can be used by 
		/// the end user to modify the <see cref="MdiTabGroup.Extent"/> of the tab group. Depending upon 
		/// the <see cref="Orientation"/> of the tab groups, the splitter will be displayed either 
		/// vertically or horizontally. When the <b>Orientation</b> is set to horizontal, the splitter 
		/// bar is displayed horizontally and displays the top and bottom borders using the 
		/// <b>SplitterBorderStyle</b>.</p>
		/// <p class="note">Note: The <b>SplitterBorderStyle</b> does not affect the width of the splitter 
		/// so the <see cref="SplitterWidth"/> must be wide enough to accomodate the <b>SplitterBorderStyle.</b></p>
		/// </remarks>
		/// <seealso cref="Orientation"/>
		/// <seealso cref="SplitterWidth"/>
		/// <seealso cref="SplitterBorderStyleResolved"/>
		/// <seealso cref="SplitterDragging"/>
		/// <seealso cref="SplitterDragged"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_SplitterBorderStyle")]
		[LocalizedCategory("LC_Appearance")]
		public UIElementBorderStyle SplitterBorderStyle
		{
			get { return this.splitterBorderStyle; }
			set
			{
				if (this.splitterBorderStyle == value)
					return;

				if (!Enum.IsDefined(typeof(UIElementBorderStyle), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(UIElementBorderStyle));

				this.splitterBorderStyle = value;

				this.SynchronizeSplitterProperties();

				if (!this.IsInitializing)
				{
					// AS 6/6/06
					// Moved from OnPropertyChanged so the cache is dirty
					// when the dirtytabgroups is called.
					//
					StyleUtils.DirtyCachedPropertyVals(this);

					// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
					this.RefreshSplitterIntersections();

					this.DirtyTabGroups(TabbedMdiPropertyIds.SplitterBorderStyle);
					this.TabGroupManager.DirtyAllTabGroups();
				}

				this.NotifyPropChange( TabbedMdiPropertyIds.SplitterBorderStyle );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="SplitterBorderStyle"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="SplitterBorderStyle"/> differs from the default value.</returns>
		/// <seealso cref="SplitterBorderStyle"/>
		public bool ShouldSerializeSplitterBorderStyle()
		{
			return this.SplitterBorderStyle != UIElementBorderStyle.Default;
		}	

		/// <summary>
		/// Resets the <see cref="SplitterBorderStyle"/> property to its default value.
		/// </summary>
		/// <seealso cref="SplitterBorderStyle"/>
		public void ResetSplitterBorderStyle()
		{
			this.SplitterBorderStyle = UIElementBorderStyle.Default;
		}

		/// <summary>
		/// Returns the resolved borderstyle used to display the splitter bars.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SplitterBorderStyle</b> determines the style of the border for 
		/// the splitter bar displayed between <see cref="MdiTabGroup"/> instances. Each <b>MdiTabGroup</b> 
		/// except the last item in the <see cref="TabGroups"/> will have a splitter that can be used by 
		/// the end user to modify the <see cref="MdiTabGroup.Extent"/> of the tab group. Depending upon 
		/// the <see cref="Orientation"/> of the tab groups, the splitter will be displayed either 
		/// vertically or horizontally. When the <b>Orientation</b> is set to horizontal, the splitter 
		/// bar is displayed horizontally and displays the top and bottom borders using the 
		/// <b>SplitterBorderStyle</b>.</p>
		/// <p class="note">Note: The <b>SplitterBorderStyle</b> does not affect the width of the splitter 
		/// so the <see cref="SplitterWidth"/> must be wide enough to accomodate the <b>SplitterBorderStyle.</b></p>
		/// </remarks>
		/// <seealso cref="SplitterBorderStyle"/>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UIElementBorderStyle SplitterBorderStyleResolved
		{
			get
			{
				
				object val;

				if (false == StyleUtils.GetCachedPropertyVal(this, StyleUtils.CachedProperty.BorderSplitter, out val))
				{
					#region fallback
					UIElementBorderStyle fallBackValue;
					// AS 5/2/06 AppStyling
					ViewStyle viewStyle = this.ViewStyleResolved;
					
					if (viewStyle == ViewStyle.Office2003 || viewStyle == ViewStyle.VisualStudio2005 )
						fallBackValue = UIElementBorderStyle.Solid;
					else
						fallBackValue = UIElementBorderStyle.RaisedSoft;
					#endregion //fallback

					val = StyleUtils.CachePropertyValue(this,
						StyleUtils.CachedProperty.BorderSplitter,
						StyleUtils.GetRole(this, StyleUtils.Role.SplitterBar).BorderStyle,
						this.SplitterBorderStyle,
						UIElementBorderStyle.Default,
						fallBackValue);
				}

				return (UIElementBorderStyle)val;
			}
		}
		#endregion //SplitterBorderStyle

		#region MaxTabGroups
		// AS 3/15/05 Old comment
		//		/// <p class="body">The <b>MaxTabGroups</b> determines the maximum number of 
		//		/// <see cref="MdiTabGroup"/> objects that may be created in the <see cref="TabGroups"/> collection.</p>
		//		/// <p class="note">Note: This limitation is imposed both programatically and via the 
		//		/// ui. In other words, the options to move a tab to a "New Horizontal Tab Group" or 
		//		/// "New Vertical Tab Group" will not be included in the context menu if there are 
		//		/// already the <b>MaxTabGroups</b> number of MdiTabGroup objects in the 
		//		/// <see cref="TabGroups"/>. Also, an exception will be raised if an attempt to 
		//		/// programmatically move a tab to a new tab group 
		//		/// (e.g. <see cref="MoveToNewGroup(MdiTab)"/>) is made.</p>

		/// <summary>
		/// Returns or sets the maximum number of <see cref="MdiTabGroup"/> instances that may be created.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MaxTabGroups</b> determines the maximum number of <see cref="MdiTabGroup"/> 
		/// objects that contain <see cref="MdiTab"/> object can be created.</p>
		/// <p class="note">Note: This limitation is imposed both programatically and via the 
		/// ui. In other words, the options to move a tab to a "New Horizontal Tab Group" or 
		/// "New Vertical Tab Group" will not be included in the context menu if there are 
		/// already the <b>MaxTabGroups</b> number of MdiTabGroup objects (in the 
		/// <see cref="TabGroups"/> collection or within the <see cref="MdiTabGroup.TabGroups"/> of nested 
		/// <b>MdiTabGroups</b>) with <see cref="MdiTab"/> objects in their <see cref="MdiTabGroup.Tabs"/> 
		/// collection. Also, an exception will be raised if an attempt to 
		/// programmatically move a tab to a new tab group 
		/// (e.g. <see cref="MoveToNewGroup(MdiTab)"/>) is made.</p>
		/// </remarks>
		/// <seealso cref="AllowHorizontalTabGroups"/>
		/// <seealso cref="AllowVerticalTabGroups"/>
		/// <seealso cref="TabGroups"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_MaxTabGroups")]
		[LocalizedCategory("LC_Behavior")]
		public int MaxTabGroups
		{
			get { return this.maxTabGroups; }
			set
			{
				if (this.maxTabGroups == value)
					return;

				// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
				// Changed default MaxTabGroups to 0 (unlimited).
				//
				//if (value < 1)
				if (value < 0)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_33"));

				this.maxTabGroups = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.MaxTabGroups );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="MaxTabGroups"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="MaxTabGroups"/> differs from the default value.</returns>
		/// <seealso cref="MaxTabGroups"/>
		public bool ShouldSerializeMaxTabGroups()
		{
			return this.MaxTabGroups != MaxTabGroupsDefault;
		}	

		/// <summary>
		/// Resets the <see cref="MaxTabGroups"/> property to its default value.
		/// </summary>
		/// <seealso cref="MaxTabGroups"/>
		public void ResetMaxTabGroups()
		{
			this.MaxTabGroups = MaxTabGroupsDefault;
		}
		#endregion //MaxTabGroups

		#region SplitterWidth
		/// <summary>
		/// Returns or sets the width of the splitter bar displayed between tab groups.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SplitterWidth</b> determines the width of the splitter 
		/// bar in a tab group. All <see cref="MdiTabGroup"/> objects in the <see cref="TabGroups"/> 
		/// collection have a splitter bar except the last one. The splitter bar is used 
		/// by the end user to modify the <see cref="MdiTabGroup.Extent"/> of a tab group.</p>
		/// <p class="body">When the user presses the logical left mouse button on the splitter 
		/// bar, the <see cref="SplitterDragging"/> event is invoked to determine whether 
		/// to allow the splitter bar to be repositioned. If the event is not cancelled, 
		/// the user may move the splitter bar. Once the splitter bar is released, the 
		/// <see cref="MdiTabGroup.Extent"/> is updated (as well as that of the adjacent 
		/// tab group) and the <see cref="SplitterDragged"/> event is invoked.</p>
		/// </remarks>
		/// <seealso cref="SplitterBorderStyle"/>
		/// <seealso cref="SplitterAppearance"/>
		/// <seealso cref="SplitterDragging"/>
		/// <seealso cref="SplitterDragged"/>
		/// <seealso cref="MdiTabGroup.Extent"/>
		/// <seealso cref="MdiTabGroup.ClientExtent"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_SplitterWidth")]
		[LocalizedCategory("LC_Appearance")]
		public int SplitterWidth
		{
			get { return this.splitterWidth; }
			set
			{
				if (this.splitterWidth == value)
					return;

				if (value < 0)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_34"));

				this.splitterWidth = value;

				if (!this.IsInitializing)
					this.DirtyTabGroups(TabbedMdiPropertyIds.SplitterWidth);

				// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
				if (this.MaximizedTabGroupDisplayStyle == MaximizedMdiTabGroupDisplayStyle.CompressUnmaximizedGroups)
					this.RefreshMaximizedTabGroupExtents();

				this.NotifyPropChange( TabbedMdiPropertyIds.SplitterWidth );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="SplitterWidth"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="SplitterWidth"/> differs from the default value.</returns>
		/// <seealso cref="SplitterWidth"/>
		public bool ShouldSerializeSplitterWidth()
		{
			return this.SplitterWidth != SplitterWidthDefault;
		}	

		/// <summary>
		/// Resets the <see cref="SplitterWidth"/> property to its default value.
		/// </summary>
		/// <seealso cref="SplitterWidth"/>
		public void ResetSplitterWidth()
		{
			this.SplitterWidth = SplitterWidthDefault;
		}
		#endregion //SplitterWidth

		#region ImageSize
		/// <summary>
		/// Returns or sets the size of the images in the tabs.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ImageSize</b> property determines the 
		/// size of the image displayed in an <see cref="MdiTab"/>. All tab images 
		/// are scaled using this property. The default value is 16x16.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.DisplayFormIcon"/>
		/// <seealso cref="MdiTabSettings"/>
		/// <seealso cref="ImageList"/>
		/// <seealso cref="ImageTransparentColor"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_ImageSize")]
		[LocalizedCategory("LC_Appearance")]
		public Size ImageSize
		{
			get { return this.imageSize; }
			set
			{
				if (this.imageSize == value)
					return;

				if (value.Width < 0 || value.Height < 0)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, "The 'ImageSize' must be at least (0,0).");

				this.imageSize = value;

				// dirty the tab group's tabmanager as needed...
				this.DirtyTabGroups(TabbedMdiPropertyIds.ImageSize);

				this.NotifyPropChange( TabbedMdiPropertyIds.ImageSize );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ImageSize"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ImageSize"/> differs from the default value.</returns>
		/// <seealso cref="ImageSize"/>
		public bool ShouldSerializeImageSize()
		{
			return this.ImageSize != ImageSizeDefault;
		}	

		/// <summary>
		/// Resets the <see cref="ImageSize"/> property to its default value.
		/// </summary>
		/// <seealso cref="ImageSize"/>
		public void ResetImageSize()
		{
			this.ImageSize = ImageSizeDefault;
		}
		#endregion //ImageSize

		#region ImageTransparentColor
		/// <summary>
		/// Gets/sets the color displayed as transparent in a image.
		/// </summary>
		/// <remarks>
		/// <p class="body">When set to a color other than Color.Transparent (the default), all 
		/// occurrences of the color in the image will be made transparent.</p>
		/// <p class="body">If an image is supplied by setting an Appearance.Image property
		/// to an Imagelist index, the ImageLists TransparentColor property is 
		/// looked at first.  If that property is set to Color.TransparentColor,
		/// then the component's ImageTransparentColor is used.  If it is set 
		/// to Color.TransparentColor, then no color masking is done.</p>
		/// <p class="body">If an image is supplied by setting the Appearance.Image property to
		/// an image, the component's ImageTransparentColor is used.  If it is
		/// set to Color.TransparentColor, then no color masking is done.</p>
		/// </remarks>
		/// <seealso cref="ImageList"/>
		/// <seealso cref="ImageSize"/>
		/// <seealso cref="MdiTabSettings.DisplayFormIcon"/>
		/// <seealso cref="MdiTabSettings"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_ImageTransparentColor")]
		[LocalizedCategory("LC_Appearance")]
		public Color ImageTransparentColor
		{
			get { return this.imageTransparentColor; }
			set
			{
				if (this.imageTransparentColor == value)
					return;

				this.imageTransparentColor = value;

				if (!this.IsInitializing)
					this.DirtyTabGroups(TabbedMdiPropertyIds.ImageTransparentColor);

				this.NotifyPropChange( TabbedMdiPropertyIds.ImageTransparentColor );
			}
		}

		/// <summary>
		/// <p class="body">Returns a Boolean value that determines whether the <see cref="UltraTabbedMdiManager.ImageTransparentColor"/> property is set to its default value.</p>
		/// </summary>
		/// <remarks>
		/// <p class="body">Returns True if the <see cref="UltraTabbedMdiManager.ImageTransparentColor"/> property is not set to its default value; otherwise, it returns False.</p>
		/// <p class="body">Invoke the <see cref="UltraTabbedMdiManager.ResetImageTransparentColor"/> method to reset this property to its default value.</p>
		/// </remarks>
		/// <seealso cref="ImageTransparentColor"/>
		protected bool ShouldSerializeImageTransparentColor()
		{
			return this.ImageTransparentColor != Color.Transparent;
		}

		/// <summary>
		/// Resets the <see cref="UltraTabbedMdiManager.ImageTransparentColor"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to reset the <see cref="UltraTabbedMdiManager.ImageTransparentColor"/> property to its default value.</p>
		/// <p class="body">Once this method is invoked, the <see cref="UltraTabbedMdiManager.ShouldSerializeImageTransparentColor"/> method will return False until the <see cref="UltraTabbedMdiManager.ImageTransparentColor"/> property is set again.</p>
		/// </remarks>
		/// <seealso cref="ImageTransparentColor"/>
		public void ResetImageTransparentColor()
		{
			this.ImageTransparentColor = Color.Transparent;
		}
		#endregion //ImageTransparentColor

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		#region IsActiveTabGroupMaximized
		/// <summary>
		/// Returns or sets whether only the <see cref="MdiTabGroup.Tabs"/> of the MdiTabGroup containing the <see cref="UltraTabbedMdiManager.ActiveTab"/> is displayed.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note:</b> As the MdiTabGroup that contains the <see cref="ActiveTab"/> changes 
		/// (because either the ActiveTab is moved to a different group or because a different MdiTab/Form is 
		/// activated) the MdiTabGroup that is displayed will automatically change.</p>
		/// </remarks>
		/// <seealso cref="MaximizedMdiTabGroupDisplayStyle"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_IsActiveTabGroupMaximized")]
		[LocalizedCategory("LC_Behavior")]
		public bool IsActiveTabGroupMaximized
		{
			get { return this.isActiveTabGroupMaximized;}
			set
			{
				if (value == this.isActiveTabGroupMaximized)
					return;

				this.isActiveTabGroupMaximized = value;

				this.VerifyMaximizedTabGroup();

				this.NotifyPropChange(TabbedMdiPropertyIds.IsActiveTabGroupMaximized);
			}
		}

		/// <summary>
		/// Indicates if the <see cref="IsActiveTabGroupMaximized"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="IsActiveTabGroupMaximized"/> differs from the default value.</returns>
		/// <seealso cref="IsActiveTabGroupMaximized"/>
		public bool ShouldSerializeIsActiveTabGroupMaximized()
		{
			return this.IsActiveTabGroupMaximized != IsActiveTabGroupMaximizedDefault;
		}	

		/// <summary>
		/// Resets the <see cref="IsActiveTabGroupMaximized"/> property to its default value.
		/// </summary>
		/// <seealso cref="IsActiveTabGroupMaximized"/>
		public void ResetIsActiveTabGroupMaximized()
		{
			this.IsActiveTabGroupMaximized = IsActiveTabGroupMaximizedDefault;
		}
		#endregion //IsActiveTabGroupMaximized

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		#region MaximizedTabGroupDisplayStyle
		/// <summary>
		/// Returns or sets how the non-maximized MdiTabGroups are displayed when the <see cref="IsActiveTabGroupMaximized"/> is set to true.
		/// </summary>
		/// <seealso cref="IsActiveTabGroupMaximized"/>
		[LocalizedCategory("LC_Behavior")]
		public MaximizedMdiTabGroupDisplayStyle MaximizedTabGroupDisplayStyle
		{
			get { return this.maximizedTabGroupDisplayStyle;}
			set
			{
				if (value == this.maximizedTabGroupDisplayStyle)
					return;

				if (!Enum.IsDefined(typeof(MaximizedMdiTabGroupDisplayStyle), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(MaximizedMdiTabGroupDisplayStyle));

				this.maximizedTabGroupDisplayStyle = value;

				// make sure the layout is updated
				this.RefreshMaximizedTabGroupExtents();

				this.NotifyPropChange(TabbedMdiPropertyIds.MaximizedTabGroupDisplayStyle);
			}
		}

		/// <summary>
		/// Indicates if the <see cref="MaximizedTabGroupDisplayStyle"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="MaximizedTabGroupDisplayStyle"/> differs from the default value.</returns>
		/// <seealso cref="MaximizedTabGroupDisplayStyle"/>
		public bool ShouldSerializeMaximizedTabGroupDisplayStyle()
		{
			return this.MaximizedTabGroupDisplayStyle != MaximizedTabGroupDisplayStyleDefault;
		}	

		/// <summary>
		/// Resets the <see cref="MaximizedTabGroupDisplayStyle"/> property to its default value.
		/// </summary>
		/// <seealso cref="MaximizedTabGroupDisplayStyle"/>
		public void ResetMaximizedTabGroupDisplayStyle()
		{
			this.MaximizedTabGroupDisplayStyle = MaximizedTabGroupDisplayStyleDefault;
		}
		#endregion //MaximizedTabGroupDisplayStyle

		// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
		#region AllowMaximize
		/// <summary>
		/// Returns or sets whether the end user can change the <see cref="IsActiveTabGroupMaximized"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property defaults to false. When set to true, the tab context menu that is 
		/// displayed when you right click on a tab will include an additional option - Maximize. This menu 
		/// item is used to toggle the <see cref="IsActiveTabGroupMaximized"/> property.</p>
		/// </remarks>
		/// <seealso cref="MaximizedMdiTabGroupDisplayStyle"/>
		/// <seealso cref="IsActiveTabGroupMaximized"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_AllowMaximize")]
		[LocalizedCategory("LC_Behavior")]
		public bool AllowMaximize
		{
			get { return this.allowMaximize;}
			set
			{
				if (value == this.allowMaximize)
					return;

				this.allowMaximize = value;

				this.VerifyMaximizedTabGroup();

				this.NotifyPropChange(TabbedMdiPropertyIds.AllowMaximize);
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AllowMaximize"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AllowMaximize"/> differs from the default value.</returns>
		/// <seealso cref="AllowMaximize"/>
		public bool ShouldSerializeAllowMaximize()
		{
			return this.AllowMaximize != AllowMaximizeDefault;
		}	

		/// <summary>
		/// Resets the <see cref="AllowMaximize"/> property to its default value.
		/// </summary>
		/// <seealso cref="AllowMaximize"/>
		public void ResetAllowMaximize()
		{
			this.AllowMaximize = AllowMaximizeDefault;
		}
		#endregion //AllowMaximize

		#region Appearance
		/// <summary>
		/// Gets/sets the default appearance for the component.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Appearance</b> property is the default appearance for all the elements 
		/// used by the <see cref="UltraTabbedMdiManager"/>. It is used when resolving the appearance of the tabs, tab area, 
		/// splitter bar, tab list button, close button and scroll buttons. If you want to control those items individually, you may use the 
		/// specific appearance properties for those items. For example, to affect only the splitter bar you can use the <see cref="SplitterAppearance"/>.</p>
		/// </remarks>
		/// <seealso cref="SplitterAppearance"/>
		/// <seealso cref="MdiTabSettings.TabAppearance"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettings.CloseButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettings.TabListButtonAppearance"/>
		/// <seealso cref="MdiTabGroupSettings.TabAreaAppearance"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_Appearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase Appearance
		{
			get
			{
				if (this.appearanceHolder == null)
				{
					this.appearanceHolder = new AppearanceHolder();
					this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				this.appearanceHolder.Collection = this.Appearances;

				return this.appearanceHolder.Appearance;
			}

			set
			{
				if (this.appearanceHolder == null				|| 
					!this.appearanceHolder.HasAppearance		||
					value != this.appearanceHolder.Appearance)
				{
					if (null == this.appearanceHolder)
					{
						this.appearanceHolder = new Infragistics.Win.AppearanceHolder();
						this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					this.appearanceHolder.Collection = this.Appearances;

					this.appearanceHolder.Appearance = value;

					// dirty the tab group's tabmanager as needed...
					this.DirtyTabGroups(TabbedMdiPropertyIds.Appearance);

					this.NotifyPropChange( TabbedMdiPropertyIds.Appearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns true when the Appearance object for the <see cref="Appearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="Appearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasAppearance
		{
			get
			{
				return (null != this.appearanceHolder &&
					this.appearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="Appearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="Appearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="Appearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeAppearance()
		{
			return this.HasAppearance && this.appearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="Appearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="Appearance"/> property. If the <see cref="Appearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="Appearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="Appearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetAppearance()
		{
			if ( this.HasAppearance )
			{
				this.appearanceHolder.Reset();

				// dirty the tab group's tabmanager as needed...
				this.DirtyTabGroups(TabbedMdiPropertyIds.Appearance);

				this.NotifyPropChange( TabbedMdiPropertyIds.Appearance );
			}
		}
		#endregion //Appearance

		#region SplitterAppearance
		/// <summary>
		/// Gets/sets the appearance for the splitter bars.
		/// </summary>
		/// <remarks>
		/// <p class="body">All <see cref="MdiTabGroup"/> items in the <see cref="TabGroups"/> collection except the 
		/// last item have a splitter bar that may be used to adjust the <see cref="MdiTabGroup.Extent"/> at runtime 
		/// as long as the <see cref="SplitterWidth"/> is at least 0. The <b>SplitterAppearance</b> determines the 
		/// colors used to display the splitter bar.</p>
		/// </remarks>
		/// <seealso cref="SplitterWidth"/>
		/// <seealso cref="SplitterBorderStyle"/>
		/// <seealso cref="MdiTabGroup.Extent"/>
		/// <seealso cref="Appearance"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_SplitterAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase SplitterAppearance
		{
			get
			{
				if (this.splitterAppearanceHolder == null)
				{
					this.splitterAppearanceHolder = new AppearanceHolder();
					this.splitterAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				this.splitterAppearanceHolder.Collection = this.Appearances;

				return this.splitterAppearanceHolder.Appearance;
			}

			set
			{
				if (this.splitterAppearanceHolder == null				|| 
					!this.splitterAppearanceHolder.HasAppearance		||
					value != this.splitterAppearanceHolder.Appearance)
				{
					if (null == this.splitterAppearanceHolder)
					{
						this.splitterAppearanceHolder = new Infragistics.Win.AppearanceHolder();
						this.splitterAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					this.splitterAppearanceHolder.Collection = this.Appearances;

					this.splitterAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.SplitterAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns true when the Appearance object for the <see cref="SplitterAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="SplitterAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasSplitterAppearance
		{
			get
			{
				return (null != this.splitterAppearanceHolder &&
					this.splitterAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="SplitterAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="SplitterAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="SplitterAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeSplitterAppearance()
		{
			return this.HasSplitterAppearance && this.splitterAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="SplitterAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="SplitterAppearance"/> property. If the <see cref="SplitterAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="SplitterAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="SplitterAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetSplitterAppearance()
		{
			if ( this.HasSplitterAppearance )
			{
				this.splitterAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.SplitterAppearance );
			}
		}
		#endregion //SplitterAppearance

		#region MdiParent
		/// <summary>
		/// Returns or sets the <see cref="System.Windows.Forms.Form"/> whose mdi child forms are managed by the <b>UltraTabbedMdiManager</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MdiParent</b> is the <see cref="System.Windows.Forms.Form"/> whose 
		/// <see cref="Form.MdiChildren"/> will be managed by the <b>UltraTabbedMdiManager</b>. As mdi child 
		/// forms are created, <see cref="MdiTab"/> objects are created for each form. The <b>MdiTab</b> 
		/// for a specific Form may be obtained using the <see cref="TabFromForm"/> method.</p>
		/// <p class="note">Note: Setting the property to null (Nothing in VB) will unhook the UltraTabbedMdiManager from 
		/// the Form and release all <see cref="MdiTab"/> objects and <see cref="TabGroups"/>. To temporarily disable the 
		/// mdi interface, use the <see cref="Infragistics.Win.UltraComponentControlManagerBase.Enabled"/> property.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraComponentControlManagerBase.Enabled"/>
		/// <seealso cref="TabGroups"/>
		[Browsable(false)]
		[DefaultValue(null)]	// AS 4/25/03
		public Form MdiParent
		{
			get { return this.mdiParent; }
			set
			{
				if (this.mdiParent == value)
					return;

				// first make sure that there isn't an mdi manager already associated
				// with the form
				this.EnsureOnlyOneTabbedMdiManager(value);

				// AS 10/30/03 WTB1216
				// When we disable, we perform a cascade layout.
				// When we unhook, we should do the same thing.
				//
				MdiClient oldMdiClient = this.mdiClient;

				this.UnhookMdiParent();

				this.mdiParent = value;

				this.HookMdiParent();

				// AS 10/30/03 WTB1216
				// Perform a layout of the old mdi client if we were
				// enabled and assuming the mdi client is still valid.
				//
				if (oldMdiClient != null		&&
					this.Enabled				&&
					!oldMdiClient.Disposing		&&
					!oldMdiClient.IsDisposed)
				{
					oldMdiClient.LayoutMdi(MdiLayout.Cascade);
				}
			}
		}
		#endregion //MdiParent

		#region HiddenTabs
		/// <summary>
		/// Returns a set of cached <see cref="MdiTab"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>HiddenTabs</b> is a collection of <see cref="MdiTab"/> objects 
		/// representing <see cref="MdiTab.Form"/> objects that are not yet visible. When the associated 
		/// form is made visible, the associated <b>MdiTab</b> is removed from the <b>HiddenTabs</b> and 
		/// moved into the <see cref="MdiTabGroup.Tabs"/> collection of an <see cref="MdiTabGroup"/> and the 
		/// <see cref="TabDisplaying"/> event is invoked. When the form is hidden, the associated 
		/// <b>MdiTab</b> is removed from its containing <see cref="MdiTab.TabGroup"/> and added to the 
		/// <b>HiddenTabs</b> collection.</p>
		/// </remarks>
		/// <seealso cref="TabDisplaying"/>
		/// <seealso cref="TabDisplayed"/>
		/// <seealso cref="MdiTabSettings.TabCloseAction"/>
		/// <seealso cref="MdiTabSettings.AllowClose"/>
		// AS 4/29/03 This was previously removed and stripped using the designer 
		// at design time so it can be accessed using a property grid at runtime if 
		// desired. It is not exposed at design time because no tab groups exist at 
		// design time.
		//
		//[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public HiddenMdiTabsCollection HiddenTabs
		{
			get
			{
				if (this.hiddenTabs == null)
				{
					this.hiddenTabs = new HiddenMdiTabsCollection();
					this.hiddenTabs.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.hiddenTabs;
			}
		}

		/// <summary>
		/// Indicates if the <see cref="HiddenTabs"/> collection contains any items
		/// </summary>
		/// <seealso cref="HiddenTabs"/>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool HasHiddenTabs
		{
			get
			{
				return this.hiddenTabs != null && this.hiddenTabs.Count > 0;
			}
		}
		#endregion //HiddenTabs

		#region CanCreateNewGroup
		/// <summary>
		/// Indicates if a new <see cref="MdiTabGroup"/> can be created.
		/// </summary>
		/// <remarks>Returns true if the current number of <see cref="TabGroups"/> is less than the <see cref="MaxTabGroups"/></remarks>
		/// <seealso cref="TabGroups"/>
		/// <seealso cref="MaxTabGroups"/>
		/// <seealso cref="InitializeTabGroup"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool CanCreateNewGroup
		{
			get
			{
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				// Since we have the potential for nesting tab groups,
				// we want to compare the maxtabgroups count to the
				// actual number of tabgroups with tabs.
				//
				//return this.TabGroups.Count < this.MaxTabGroups;
				// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
				// Changed default MaxTabGroups to 0 (unlimited).
				//
				//return this.TabGroupsWithTabsCount < this.MaxTabGroups;
				return this.MaxTabGroups == 0 || this.TabGroupsWithTabsCount < this.MaxTabGroups;
			}
		}
		#endregion //CanCreateNewGroup

		#region IsDragging
		/// <summary>
		/// Indicates if an <see cref="MdiTab"/> is being dragged
		/// </summary>
		/// <remarks>Returns true if an <see cref="MdiTab"/> is currently being dragged</remarks>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		/// <seealso cref="TabDragging"/>
		/// <seealso cref="TabDropped"/>
		/// <seealso cref="TabDragOver"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool IsDragging
		{
			get
			{
				if (!this.HasTabGroups)
					return false;

				// check each tab group to see if we are in a drag operation
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup tabGroup in this.TabGroups)
				foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
				{
					if (tabGroup.HasTabManager && tabGroup.TabManager.IsDragging)
						return true;
				}

				return false;
			}
		}
		#endregion //IsDragging

		// AS 4/29/03 WTB880
		#region BorderStyle
		/// <summary>
		/// Returns or sets the borderstyle of the mdi client area when the UltraTabbedMdiManager is enabled.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>BorderStyle</b> property is used to modify the border 
		/// style of the mdi client area when the <see cref="Infragistics.Win.UltraComponentControlManagerBase.Enabled"/> 
		/// property is set to true. By default the mdi client in .Net displays a three dimensional sunken border. To 
		/// allow the mdi client to more closely resemble the appearance of Visual Studio .Net, the <b>BorderStyle</b> 
		/// defaults to <b>Solid</b>.</p>
		/// <p class="note">Note: Since there is no mechanism available on the mdi client to modify its 
		/// border, this is done using unmanaged code and therefore will require that the assembly have 
		/// unmanaged code rights in order to be able to adjust the border style of the mdi client.</p>
		/// </remarks>
		/// <seealso cref="BorderColor"/>
		[DefaultValue(BorderStyleDefault)]
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_BorderStyle")]
		[LocalizedCategory("LC_Appearance")]
		public MdiClientBorderStyle BorderStyle
		{
			get { return this.borderStyle; }
			set
			{
				if (this.borderStyle == value)
					return;

				if (!Enum.IsDefined(typeof(MdiClientBorderStyle), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(MdiClientBorderStyle));

				this.borderStyle = value;

				this.UpdateBorderStyle();

				this.NotifyPropChange( TabbedMdiPropertyIds.BorderStyle );
			}
		}
		#endregion //BorderStyle

		// AS 5/1/03 WTB880
		#region BorderColor
		/// <summary>
		/// Returns or sets the color used for the mdi client area border.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>BorderColor</b> property determines the color of the border 
		/// of the mdi client area when the <see cref="Infragistics.Win.UltraComponentControlManagerBase.Enabled"/> 
		/// property is set to true. When the <see cref="BorderStyle"/> is set to a three dimensional style border 
		/// the <b>BorderColor</b> is used as the base color.</p>
		/// <p class="note">Note: Since there is no mechanism available on the mdi client to modify its 
		/// border, this is done using unmanaged code and therefore will require that the assembly have 
		/// unmanaged code rights in order to be able to adjust the border style and border color of the mdi client.</p>
		/// </remarks>
		/// <seealso cref="BorderStyle"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_BorderColor")]
		[LocalizedCategory("LC_Appearance")]
		public Color BorderColor
		{
			get { return this.borderColor;}
			set 
			{
				if (this.borderColor == value)
					return;

				this.borderColor = value;

				if (this.IsMdiClientSubclassed)
					this.mdiClientSubclasser.BorderColorChanged();

				this.NotifyPropChange( TabbedMdiPropertyIds.BorderColor );
			}
		}

		/// <summary>
		/// <p class="body">Returns a Boolean value that determines whether the <see cref="UltraTabbedMdiManager.BorderColor"/> property is set to its default value.</p>
		/// </summary>
		/// <remarks>
		/// <p class="body">Returns True if the <see cref="UltraTabbedMdiManager.BorderColor"/> property is not set to its default value; otherwise, it returns False.</p>
		/// <p class="body">Invoke the <see cref="UltraTabbedMdiManager.ResetBorderColor"/> method to reset this property to its default value.</p>
		/// </remarks>
		/// <seealso cref="BorderColor"/>
		protected bool ShouldSerializeBorderColor()
		{
			return this.BorderColor != Color.Empty;
		}

		/// <summary>
		/// Resets the <see cref="UltraTabbedMdiManager.BorderColor"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to reset the <see cref="UltraTabbedMdiManager.BorderColor"/> property to its default value.</p>
		/// <p class="body">Once this method is invoked, the <see cref="UltraTabbedMdiManager.ShouldSerializeBorderColor"/> method will return False until the <see cref="UltraTabbedMdiManager.BorderColor"/> property is set again.</p>
		/// </remarks>
		/// <seealso cref="BorderColor"/>
		public void ResetBorderColor()
		{
			this.BorderColor = Color.Empty;
		}
		#endregion //BorderColor

		//	BF 7.19.04	NAS2004 Vol 3 - Office2003 look & feel
		#region ViewStyle
		/// <summary>
		/// Gets/sets the view style for the control.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedCategory("LC_Appearance")]
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_ViewStyle")]
		public ViewStyle ViewStyle
		{
			get
			{
				return this.viewStyle;
			}

			set
			{
				if ( value != this.viewStyle )
				{
					this.viewStyle = value;

					// AS 3/15/05
					// For consistency sake, we should validate the enum value.
					//
					if (!Enum.IsDefined(typeof(ViewStyle), value))
						throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(ViewStyle));

					// AS 3/15/05
					// We should really dirty/invalidate first and then
					// send a notification in case a customer is looking at
					// the PropertyChanged event for this property.
					//
					//this.NotifyPropChange( TabbedMdiPropertyIds.ViewStyle );

					// JJD 8/22/06 - NA 2006 vol 3
					// Moved logic to ViewStyleChangeDirtyHelper method
					//// AS 6/6/06 BR11811
					//StyleUtils.DirtyCachedPropertyVals(this);

					//// AS 5/19/06 BR11811
					////this.DirtyTabGroups(Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup.TabManagerChangeType.AllMetrics);
					//this.DirtyTabGroups(Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup.TabManagerChangeType.AllMetrics 
					//    | MdiTabGroup.TabManagerChangeType.Font);
					//this.InvalidateControls();

					//// AS 3/15/05 Moved from above
					//this.NotifyPropChange( TabbedMdiPropertyIds.ViewStyle );
					this.ViewStyleChangeDirtyHelper();
				}
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="UltraTabbedMdiManager.ViewStyle"/> property needs to be serialized.
		/// </summary>
		/// <returns>A boolean indicating whether the <see cref="UltraTabbedMdiManager.ViewStyle"/> property needs to be serialized.</returns>
		/// <seealso cref="UltraTabbedMdiManager.ViewStyle"/>
		protected bool ShouldSerializeViewStyle()
		{
			// JJD 8/22/06 check against default not standard
			//return this.viewStyle != ViewStyle.Standard;
			return this.viewStyle != ViewStyle.Default;
		}

		/// <summary>
		/// Resets the <see cref="UltraTabbedMdiManager.ViewStyle"/> property to its default value.
		/// </summary>
		/// <seealso cref="UltraTabbedMdiManager.ViewStyle"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetViewStyle()
		{
			// JJD 8/22/06 reset to default not standard
			//this.ViewStyle = ViewStyle.Standard;
			this.ViewStyle = ViewStyle.Default;
		}

		// AS 5/2/06 AppStyling
		/// <summary>
		/// Returns the resolved <see cref="Infragistics.Win.UltraWinTabbedMdi.ViewStyle"/>
		/// </summary>
		/// <seealso cref="UltraTabbedMdiManager.ViewStyle"/>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ViewStyle ViewStyleResolved
		{
			get
			{
				object val;

				if (false == StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.ViewStyle, out val))
				{
					val = StyleUtils.CachePropertyValue(this, StyleUtils.CustomProperty.ViewStyle,
						StyleUtils.GetViewStyle(this, StyleUtils.CustomProperty.ViewStyle),
						this.ViewStyle, ViewStyle.Default, ViewStyle.Standard);
				}

				return (ViewStyle)val;
			}
		}
		#endregion //ViewStyle

		#region CLR2
        // MD 10/15/04 - Default Store Support
		#region SaveSettingsFormat
        /// <summary>
        /// Gets or sets the format in which the user customizations settings will be saved and loaded
        /// </summary>
        /// <value></value>
        [DefaultValue(SaveSettingsFormat.Binary)]
        // JAS v5.3 SmartTags 7/13/05 - Added LocalizedDescription & LocalizedCategory
        [LocalizedDescription( "LD_UltraTabbedMdiManager_P_SaveSettingsFormat" )]
        [LocalizedCategory("LC_Serialization")]
        public SaveSettingsFormat SaveSettingsFormat
        {
            get { return this.saveSettingsFormat; }
            set { this.saveSettingsFormat = value; }
        }
		#endregion
		#endregion //CLR2

		// AS 5/22/07 BR23151 - MdiTabNavigationMode 
		#region TabNavigationMode
		/// <summary>
		/// Gets/sets the setting that controls which tab is activated when navigating using the keyboard.
		/// </summary>
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraTabbedMdiManager_P_TabNavigationMode")]
		[DefaultValue(TabNavigationModeDefault)]
		public MdiTabNavigationMode TabNavigationMode
		{
			get { return this.tabNavigationMode; }
			set
			{
				if (this.tabNavigationMode == value)
					return;

				if (!Enum.IsDefined(typeof(MdiTabNavigationMode), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(MdiTabNavigationMode));

				this.tabNavigationMode = value;

				this.NotifyPropChange(TabbedMdiPropertyIds.TabNavigationMode);
			}
		} 
		#endregion //TabNavigationMode

		#endregion //Public

		#region Protected

		#region SubObjectPropChangeHandler
		/// <summary>
		/// Returns the event handler that for the <see cref="OnSubObjectPropChanged"/> method that is used to receive notifications when a property on a sub-object has changed.
		/// </summary>
		protected SubObjectPropChangeEventHandler SubObjectPropChangeHandler
		{
			get
			{
				if (this.subObjectPropChangeHandler == null)
					this.subObjectPropChangeHandler = new SubObjectPropChangeEventHandler(this.OnSubObjectPropChanged);

				return this.subObjectPropChangeHandler;
			}
		}
		#endregion //SubObjectPropChangeHandler

		#endregion //Protected

		#region Internal

		#region ActiveTabGroup
		internal MdiTabGroup ActiveTabGroup
		{
			get
			{
				// if we are not hooked up, there is nothing to do
				if (this.MdiParent == null)
					return null;

				// get the tab associated with the active mdi child
				MdiTab tab = this.ActiveTab;

				if (tab == null)
					return null;

				// get its tab group
				return tab.TabGroup;
			}
		}
		#endregion //ActiveTabGroup

		#region PreviousActiveTabGroup
		internal MdiTabGroup PreviousActiveTabGroup
		{
			get
			{
				// make sure that the tab group wasn't removed
				if (this.lastActiveTabGroup != null  &&
					// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
					//!this.TabGroups.Contains(this.lastActiveTabGroup))
					(this.lastActiveTabGroup.Manager == null || this.lastActiveTabGroup.Index < 0))
				{
					this.lastActiveTabGroup = null;
				}

				return this.lastActiveTabGroup;
			}
		}
		#endregion //PreviousActiveTabGroup

		#region IsInitializing
		internal bool IsInitializing
		{
			get { return this.isInitializing; }
		}
		#endregion //IsInitializing

		#region IsDisposing
		internal bool IsDisposing
		{
			get { return this.isDisposing; }
		}	
		#endregion //IsDisposing

		#region SubclasserActivatingMdiChild
		internal bool SubclasserActivatingMdiChild
		{
			get { return this.isSubclasserActivatingMdiChild; }
			set { this.isSubclasserActivatingMdiChild = value; }
		}
		#endregion //SubclasserActivatingMdiChild

		#region MdiClientLocation
		internal Point MdiClientLocation
		{
			get
			{
				if (this.mdiClient == null)
					return Point.Empty;

				Point pt = this.mdiClient.Location;

				if (mdiClient.IsHandleCreated	&& 
					this.MdiParent != null		&&
					this.MdiParent.IsHandleCreated)
				{
					// since the mdi client has a border but does not
					// provide the client rect with respect to its upper
					// left, get the offset by finding out the location
					// of the upper left of the mdi client in relation it
					// its upper left of the client rect
					//
					Point location = mdiClient.Location;
					location = this.MdiParent.PointToScreen(location);
					Point offset = this.mdiClient.PointToClient(location);

					if (!offset.IsEmpty)
					{
						// AS 12/29/05 BR08034
						// There seems to be an os bug whereby mapwindowpoints is
						// returning the incorrect value when the mdi parent is maximized
						// and an mdi child is creating in the constructor of the mdi parent.
						// If the mdi parent is not created, we'll have to assume that the 
						// non client area is evenly distributed.
						// 
						//pt.Offset(-offset.X, -offset.Y);
						if (this.MdiParent.Created)
							pt.Offset(-offset.X, -offset.Y);
						else
							pt.Offset((this.mdiClient.Width - this.mdiClient.ClientSize.Width) / 2, (this.mdiClient.Height - this.mdiClient.ClientSize.Height) / 2);
					}
				}

				return pt;
			}
		}
		#endregion //MdiClientLocation

		#region CommonMdiClientExtent
		internal int CommonMdiClientExtent
		{
			get
			{
				if (this.Orientation == Orientation.Vertical)
					return this.MdiClientSize.Height;
				else
					return this.MdiClientSize.Width;
			}
		}
		#endregion //CommonMdiClientExtent

		#region TabGroupManager
		internal TabGroupInfoManager TabGroupManager
		{
			get
			{
				if (this.tabGroupManager == null)
					this.tabGroupManager = new TabGroupInfoManager(this);

				return this.tabGroupManager;
			}
		}
		#endregion //TabGroupManager

		#region MdiClientSize
		private Size MdiClientSize
		{
			get
			{
				if (this.mdiClient == null)
					return Size.Empty;

				if (this.MdiParent != null && this.MdiParent.WindowState == FormWindowState.Minimized)
					return this.lastMdiClientSize;

				return this.mdiClient.ClientSize;
			}
		}
		#endregion //MdiClientSize

		#region MdiClientScreenRect
		internal Rectangle MdiClientScreenRect
		{
			get
			{
				if (this.mdiClient == null || !this.mdiClient.IsHandleCreated)
					return Rectangle.Empty;

				Rectangle rect = this.mdiClient.ClientRectangle;

				return this.mdiClient.RectangleToScreen(rect);
			}
		}
		#endregion //MdiClientScreenRect

		// AS 4/23/03
		#region AllowSelectedTabActivation
		internal bool AllowSelectedTabActivation
		{
			get { return !this.ensuringTabIsSelected; }
		}
		#endregion //AllowSelectedTabActivation

		#region IsDesignMode
		internal bool IsDesignMode
		{
			get { return this.DesignMode; }
		}
		#endregion //IsDesignMode

		#region MSColors

		// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
		internal MicrosoftColorTable MSColors
		{
			get
			{
				// AS 5/2/06 AppStyling
				ViewStyle viewStyle = this.ViewStyleResolved;

				if ( viewStyle == UltraWinTabbedMdi.ViewStyle.Office2003 )
					return Office2003ColorTable.Colors;
				else
					if ( viewStyle == UltraWinTabbedMdi.ViewStyle.VisualStudio2005 )
					return VisualStudio2005ColorTable.Colors;
				else
					return null;
			}
		}

				#endregion //MSColors

		#region ThemesActive
		// JDN 8/18/04 Added ThemesActive property for VS2005 style support
		internal bool ThemesActive
		{
			get
			{
				// AS 3/20/06 AppStyling
				//return this.SupportThemes			&&
				return this.UseOsThemesResolved		&&
					XPThemes.ThemesSupported		&&
					XPThemes.IsThemeActive			&&
					XPThemes.AppControlsThemeThemed;
			}
		}

				#endregion //ThemesActive

		// AS 3/10/05 NA 2005 Vol 2 - Nested TabGroups
		#region HasNestedTabGroups
		internal bool HasNestedTabGroups
		{
			get
			{
				if (this.HasTabGroups)
				{
					foreach(MdiTabGroup tabGroup in this.TabGroups)
					{
						if (tabGroup.HasTabGroups)
							return true;
					}
				}

				return false;
			}
		}
		#endregion //HasNestedTabGroups

		// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
		#region CanCreateHorizontalGroup
		internal bool CanCreateHorizontalGroup
		{
			get
			{
				if (!this.CanCreateNewGroup)
					return false;

				
				if (this.AllowHorizontalTabGroups == false)
					return false;

				return this.AllowNestedTabGroupsResolved
					|| this.Orientation == Orientation.Horizontal
					|| this.TabGroups.Count == 1;
			}
		}

		internal bool CanCreateRootHorizontalGroup
		{
			get { return this.CanCreateHorizontalGroup && (this.AllowHorizontalTabGroups || this.Orientation == Orientation.Horizontal); }
		}
		#endregion //CanCreateHorizontalGroup

		// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
		#region CanCreateVerticalGroup
		internal bool CanCreateVerticalGroup
		{
			get
			{
				if (!this.CanCreateNewGroup)
					return false;

				
				if (this.AllowVerticalTabGroups == false)
					return false;

				return this.AllowNestedTabGroupsResolved
					|| this.Orientation == Orientation.Vertical
					|| this.TabGroups.Count == 1;
			}
		}

		internal bool CanCreateRootVerticalGroup
		{
			get { return this.CanCreateVerticalGroup && (this.AllowVerticalTabGroups || this.Orientation == Orientation.Vertical); }
		}
		#endregion //CanCreateVerticalGroup

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		#region MaximizedTabGroup
		internal MdiTabGroup MaximizedTabGroup
		{
			get { return this.maximizedTabGroup; }
		}
		#endregion //MaximizedTabGroup

		// AS 3/25/05
		#region IsDraggingInternal
		internal bool IsDraggingInternal
		{
			get { return this.isDraggingTab; }
		}
		#endregion //IsDraggingInternal
		
		// JJD 8/31/06 - NA 2006 vol 3
		#region ToolbarsManager

		internal IToolbarsManager ToolbarsManager { get { return this.toolbarsManager; } }

		#endregion //ToolbarsManager	
    
		#endregion //Internal

		#region Private

		#region IsMdiClientSubclassed
		private bool IsMdiClientSubclassed
		{
			get 
			{ 
				// AS 10/30/03 WTB1216
				// We don't release the mdi client subclasser
				// when we detach from the mdi parent in case
				// we are reattached with it.
				//
				//return this.mdiClientSubclasser != null; 
				if (this.mdiClientSubclasser == null)
					return false;

				return this.mdiClientSubclasser.IsActive;
			}
		}
		#endregion //IsMdiClientSubclassed

		// AS 3/8/05 BR02542
		#region IsMdiParentSubclassed
		private bool IsMdiParentSubclassed
		{
			get 
			{ 
				if (this.mdiParentSubclasser == null)
					return false;

				return this.mdiParentSubclasser.IsActive;
			}
		}
		#endregion //IsMdiParentSubclassed

		#region IsActivatingMdiChild
		private bool IsActivatingMdiChild
		{
			get { return this.isActivatingMdiChild; }
		}	
		#endregion //IsActivatingMdiChild

		#region IsRestoringTabs
		private bool IsRestoringTabs
		{
			get { return this.isRestoringTabs; }
		}
		#endregion //IsRestoringTabs

		#region IsRestoringTabGroup
		private bool IsRestoringTabGroup
		{
			get { return this.restoringTabGroup; }
		}
		#endregion //IsRestoringTabGroup

		#region IsReleasingTabs
		private bool IsReleasingTabs
		{
			get { return this.isReleasingTabs; }
		}
		#endregion //IsReleasingTabs

		#region HasMdiTabs

		private bool HasMdiTabs
		{
			get
			{
				if (this.HasTabGroups)
				{
					foreach(MdiTabGroup tabGroup in this.tabGroups)
					{
						// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
						//if (tabGroup.HasTabs)
						if (tabGroup.HasTabsResolved)
							return true;
					}
				}

				return this.HasHiddenTabs;
			}
		}
		#endregion //HasMdiTabs

		#region AvailableMdiClientExtent
		private int AvailableMdiClientExtent
		{
			get
			{
				if (this.Orientation == Orientation.Vertical)
					return this.MdiClientSize.Width;
				else
					return this.MdiClientSize.Height;
			}
		}
		#endregion //AvailableMdiClientExtent

		#region TabGroupControls
		private HybridDictionary TabGroupControls
		{
			get
			{
				if (this.tabGroupControls == null)
					this.tabGroupControls = new HybridDictionary(3);

				return this.tabGroupControls;
			}
		}
		#endregion //TabGroupControls

		#region SplitterControls
		private HybridDictionary SplitterControls
		{
			get
			{
				if (this.splitterControls == null)
					this.splitterControls = new HybridDictionary(3);

				return this.splitterControls;
			}
		}
		#endregion //SplitterControls

		#region IsLayoutSuspended
		private bool IsLayoutSuspended
		{
			get { return this.suspendLayoutRequests > 0; }
		}
		#endregion //IsLayoutSuspended

		#region IsLayoutSuspendedResolved
		private bool IsLayoutSuspendedResolved
		{
			get { return this.IsLayoutSuspended || this.IsUpdating; }
		}
		#endregion //IsLayoutSuspendedResolved

		#region SplitterAppearanceCallback
		
		private ResolveAppearanceCallback SplitterAppearanceCallbackVert
		{
			get
			{
				if (this.splitterAppearanceCallbackVert == null)
					this.splitterAppearanceCallbackVert = new ResolveAppearanceCallback(this.ResolveSplitterAppearanceVert);

				return this.splitterAppearanceCallbackVert;
			}
		}

		private ResolveAppearanceCallback SplitterAppearanceCallbackHorz
		{
			get
			{
				if (this.splitterAppearanceCallbackHorz == null)
					this.splitterAppearanceCallbackHorz = new ResolveAppearanceCallback(this.ResolveSplitterAppearanceHorz);

				return this.splitterAppearanceCallbackHorz;
			}
		}
		#endregion //SplitterAppearanceCallback

        #region TabContextMenu
		// MBS 4/5/06 BR11210
		//private IGContextMenu TabContextMenu
		internal IGContextMenu TabContextMenu
		{
			get 
			{
				if (this.tabContextMenu == null)
				{
					this.tabContextMenu = new IGContextMenu();

					//	BF 7.21.04	NAS2004 Vol3 - Office2003 look & feel
					//this.tabContextMenu.Style = MenuStyle.OfficeXP;

					this.tabContextMenu.ImageList = this.ImageList;
					this.tabContextMenu.Popup += new EventHandler(this.OnContextMenuPopup);
				}

				// AS 5/2/06 AppStyling
				ViewStyle viewStyle = this.ViewStyleResolved;

				//	BF 7.21.04	NAS2004 Vol3 - Office2003 look & feel
				if ( viewStyle == ViewStyle.Standard )
					this.tabContextMenu.Style = MenuStyle.OfficeXP;
				else
					if ( viewStyle == ViewStyle.Office2003 )
					this.tabContextMenu.Style = MenuStyle.Office2003;
					// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
				else
					if ( viewStyle == ViewStyle.VisualStudio2005 )
					this.tabContextMenu.Style = MenuStyle.VisualStudio2005;
                // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
                else
                    if( viewStyle == ViewStyle.Office2007 )
                        this.tabContextMenu.Style = MenuStyle.Office2007;

				return this.tabContextMenu;
			}
		}
		#endregion //TabContextMenu

		#region MenuItem Event Handlers
		private EventHandler MenuItemClickHandler
		{
			get
			{
				if (this.menuItemClickHandler == null)
					this.menuItemClickHandler = new EventHandler(this.OnMenuItemClick);

				return this.menuItemClickHandler;
			}
		}
		
		private EventHandler MenuItemDisposedHandler
		{
			get
			{
				if (this.menuItemDisposedHandler == null)
					this.menuItemDisposedHandler = new EventHandler(this.OnMenuItemDisposed);

				return this.menuItemDisposedHandler;
			}
		}
		#endregion //MenuItem Event Handlers

		#region Images
		// AS 4/21/03 WTB818
		private Bitmap NewHorizontalGroupImage
		{
			get
			{
				if (this.newTabGroupHorzImage == null)
				{
					Type type = typeof(UltraTabbedMdiManager);
					this.newTabGroupHorzImgStream = type.Assembly.GetManifestResourceStream(type, "TabGroupHorizontal.bmp");
					this.newTabGroupHorzImage = Image.FromStream(this.newTabGroupHorzImgStream) as Bitmap;
				}

				return this.newTabGroupHorzImage;
			}
		}

		private Bitmap NewVerticalGroupImage
		{
			get
			{
				if (this.newTabGroupVertImage == null)
				{
					Type type = typeof(UltraTabbedMdiManager);
					this.newTabGroupVertImgStream = type.Assembly.GetManifestResourceStream(type, "TabGroupVertical.bmp");
					this.newTabGroupVertImage = Image.FromStream(this.newTabGroupVertImgStream) as Bitmap;
				}

				return this.newTabGroupVertImage;
			}
		}

		// JDN 8/16/04 NAS2004 Vol3 - VisualStudio2005 style
		private Bitmap NewVerticalGroupImageVS2005
		{
			get
			{
				if (this.newTabGroupVertImageVS2005 == null)
				{
					Type type = typeof(UltraTabbedMdiManager);
					this.newTabGroupVertImgStreamVS2005 = type.Assembly.GetManifestResourceStream(type, "TabGroupVerticalVS2005.bmp");
					this.newTabGroupVertImageVS2005 = Image.FromStream(this.newTabGroupVertImgStreamVS2005) as Bitmap;
				}

				return this.newTabGroupVertImageVS2005;
			}
		}

		#endregion //Images

		// AS 4/28/03 WTB873
		#region PositioningControls
		private bool PositioningControls
		{
			get { return this.positioningControls; }
		}
		#endregion //PositioningControls

		// AS 5/9/03 WTB884
		#region DragDropHelper
		private DragDropHelper DragDropHelper
		{
			get
			{
				if (this.dragHelper == null)
					this.dragHelper = new DragDropHelper(this);

				return this.dragHelper;
			}
		}
		#endregion //DragDropHelper

		// AS 5/29/03 WTB1012
		// Try to prevent flicker when showing a hidden mdi child form. 
		// For some reason, the mdi child handle is created and destroyed
		// several times and if we don't turn off mdiclient redrawing, you'll
		// see the title bar of the form appear and disappear.
		//
		#region WaitingForDelayedRedraw
		private bool WaitingForDelayedRedraw
		{
			get
			{
				if (this.hookedIdle)
					return true;

				if (this.delayedRedraw != null && !delayedRedraw.IsCompleted)
					return true;

				return false;
			}
		}
		#endregion //WaitingForDelayedRedraw

		#region SuppressSplitterLeadingEdge
		
		private bool SuppressSplitterLeadingEdge
		{
			get
			{
				// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
				if (this.AllowNestedTabGroupsResolved || this.HasNestedTabGroups)
					return false;

				// AS 5/2/06 AppStyling
				//if ( this.ViewStyle == ViewStyle.VisualStudio2005  &&
				if ( this.ViewStyleResolved == ViewStyle.VisualStudio2005  &&
					this.SplitterBorderStyle == UIElementBorderStyle.Default )
					return true;
				else
					return false;
			}
		}

		#endregion //SuppressSplitterLeadingEdge

		#region CLR2
        // MD 10/15/04 - Default Store Support
		#region Settings
        /// <summary>
        /// Gets the UltraToolbarsManagerApplicationSettings instance which saves and loads
        /// user customizations from the default store
        /// </summary>
        /// <value></value>
        private UltraTabbedMdiManager.UltraTabbedMdiManagerApplicationSettings Settings
        {
            get
            {
                if (this.settings == null)
                    this.settings = new UltraTabbedMdiManagerApplicationSettings(this, this.settingsKey);

                return this.settings;
            }
        }
		#endregion
		#endregion //CLR2

		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		#region TabGroupsWithTabsCount
		private int TabGroupsWithTabsCount
		{
			get
			{
				int count = 0;

				if (this.HasTabGroups)
				{
					// count up the tabgroups with tabs
					foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
					{
						if (tabGroup.HasTabs)
							count++;
					}
				}

				return count;
			}
		}
		#endregion //TabGroupsWithTabsCount
    
		#endregion //Private

		#endregion //Properties

		#region Methods

		#region Public

		#region MoveToNewGroup
		/// <summary>
		/// Creates a new <see cref="MdiTabGroup"/> and moves the specified tab to that group.
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <seealso cref="MdiTab.MoveToNewGroup()"/>
		/// <seealso cref="MoveToGroup(MdiTab,MdiTabGroup)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public MdiTabGroup MoveToNewGroup( MdiTab tab )
		{
			// create a new tab group at the end
			return this.MoveToNewGroup(tab, MdiTabGroupPosition.Last);
		}

		/// <summary>
		/// Creates a new <see cref="MdiTabGroup"/> and moves the specified tab to that group.
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <param name="relativeGroup">Group relative to the currently containing tab group</param>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <seealso cref="MdiTab.MoveToNewGroup()"/>
		/// <seealso cref="MoveToGroup(MdiTab,MdiTabGroup)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public MdiTabGroup MoveToNewGroup( MdiTab tab, MdiTabGroupPosition relativeGroup )
		{
			// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
			Orientation orientation = this.Orientation;

			// by default, do not split the group
			if (tab.TabGroup != null && tab.TabGroup.Parent != null)
				orientation = tab.TabGroup.TabGroupOrientation;

			return this.MoveToNewGroup(tab, relativeGroup, orientation);
		}

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		// Added an overload that would allow the creation of a split group - i.e. a group that would
		// contain both the specified tab group and the new tab group that will contain the specified tab.
		//
		/// <summary>
		/// Creates a new <see cref="MdiTabGroup"/> and moves the specified tab to that group.
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <param name="relativeGroup">Group relative to the currently containing tab group</param>
		/// <param name="orientation">Indicates the orientation of the new mdi tabgroup. If this value differs from the orientation of the <paramref name="MdiTab.TabGroup"/> 
		/// then a new group that contains both the <paramref name="relativeGroup"/> and the group to contain the specified <paramref name="tab"/> will also be created.</param>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <seealso cref="MdiTab.MoveToNewGroup()"/>
		/// <seealso cref="MoveToGroup(MdiTab,MdiTabGroup)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public MdiTabGroup MoveToNewGroup( MdiTab tab, MdiTabGroupPosition relativeGroup, Orientation orientation )
		{
			if (tab == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_28"), Shared.SR.GetString(null, "LE_V2_Exception_29"));

			MdiTabGroup relativeTabGroup;

			if (tab.TabGroup == null)
				relativeTabGroup = null;
			else
				relativeTabGroup = this.TabGroupFromPosition(tab.TabGroup, relativeGroup);

			// AS 3/9/05
			// If the relative group is next, we don't want to position the new group
			// after the next group, we want the new group to be after the current
			// group. I think the only time that we want to position the item after the
			// relative group is if we are trying to create the new group at the end.
			//
			RelativePosition relativePosition;

			switch(relativeGroup)
			{
				case MdiTabGroupPosition.First:
					relativePosition = RelativePosition.Before;
					break;
				default:
				case MdiTabGroupPosition.Last:
					relativePosition = RelativePosition.After;
					break;
				case MdiTabGroupPosition.Previous:
					relativeTabGroup = tab.TabGroup;
					relativePosition = RelativePosition.Before;
					break;
				case MdiTabGroupPosition.Next:
					relativeTabGroup = tab.TabGroup;
					relativePosition = RelativePosition.After;
					break;
			}

			//return this.MoveToNewGroup(tab, relativeTabGroup, RelativePosition.After);
			return this.MoveToNewGroup(tab, relativeTabGroup, relativePosition, orientation);
		}

		/// <summary>
		/// Creates a new <see cref="MdiTabGroup"/> and moves the specified tab to that group.
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <param name="relativeGroup">Relative group used to determine where the new group should be positioned</param>
		/// <param name="relativePosition">Determines whether the new group should be positioned before or after the relative group specified.</param>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <remarks>
		/// <p class="note">Derived classes that override this method should instead override the new overload (<see cref="MoveToNewGroup(MdiTab, MdiTabGroup, RelativePosition, Orientation)"/>).</p>
		/// </remarks>
		/// <seealso cref="MdiTab.MoveToNewGroup()"/>
		/// <seealso cref="MoveToGroup(MdiTab,MdiTabGroup)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public virtual MdiTabGroup MoveToNewGroup( MdiTab tab, MdiTabGroup relativeGroup, RelativePosition relativePosition )
		{
			// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
			Orientation orientation = this.Orientation;

			// by default, do not split the group
			if (relativeGroup != null && relativeGroup.Parent != null)
				orientation = relativeGroup.TabGroupOrientation;

			return this.MoveToNewGroup(tab, relativeGroup, relativePosition, orientation);
		}

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		// Added an overload that would allow the creation of a split group - i.e. a group that would
		// contain both the specified tab group and the new tab group that will contain the specified tab.
		//
		/// <summary>
		/// Creates a new <see cref="MdiTabGroup"/> and moves the specified tab to that group.
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <param name="relativeGroup">Relative group used to determine where the new group should be positioned</param>
		/// <param name="relativePosition">Determines whether the new group should be positioned before or after the relative group specified.</param>
		/// <param name="orientation">Indicates the orientation of the new mdi tabgroup. If this value differs from the orientation of the <paramref name="relativeGroup"/> 
		/// then a new group that contains both the <paramref name="relativeGroup"/> and the group to contain the specified <paramref name="tab"/> will also be created.</param>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <seealso cref="MdiTab.MoveToNewGroup()"/>
		/// <seealso cref="MoveToGroup(MdiTab,MdiTabGroup)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public virtual MdiTabGroup MoveToNewGroup( MdiTab tab, MdiTabGroup relativeGroup, RelativePosition relativePosition, Orientation orientation )
		{
			if (tab == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_28"), Shared.SR.GetString(null, "LE_V2_Exception_29"));

			if (!tab.IsFormVisible)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_35"));

			if (this.EventManager.InProgress(TabbedMdiEventIds.InitializeTab))
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_36"));

			if (this.EventManager.InProgress(TabbedMdiEventIds.InitializeTabGroup))
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_37"));

			// AS 4/14/03
			if (this.EventManager.InProgress(TabbedMdiEventIds.RestoreTab))
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_64"));

			// if there are no tab groups, just create a new one at the end
			if (this.TabGroups.Count == 0)
			{
				// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
				// If the new group should have a different orientation and there
				// are no groups, then change the root orientation.
				//
				if (this.Orientation != orientation)
					this.Orientation = orientation;

				// AS 3/9/05 Not much use in having an assert that is never thrown.
				//Debug.Assert(true, "A 'MoveToNewGroup' was invoked when there were no MdiTabGroups but outside the InitializeTab/InitializeTabGroup event");

				this.AddTabToTabGroup(tab);
			}
			else	// otherwise split the tabgroup
			{
				// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
				//this.SplitTabGroup(tab, relativeGroup, relativePosition);
				this.SplitTabGroup(tab, relativeGroup, relativePosition, orientation);
			}

			return tab.TabGroup;
		}
		#endregion //MoveToNewGroup

		#region MoveToGroup
		/// <summary>
		/// Moves the specified <see cref="MdiTab"/> to the specified <see cref="MdiTabGroup"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <param name="tabGroup"><b>MdiTabGroup</b> to contain the specified tab</param>
		/// <returns>Returns <b>True</b> if the tab was repositioned. Otherwise <b>False</b> is returned.</returns>
		/// <seealso cref="MdiTab.MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="MoveToNewGroup(MdiTab)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public bool MoveToGroup( MdiTab tab, MdiTabGroup tabGroup )
		{
			if (tab == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_28"), Shared.SR.GetString(null, "LE_V2_Exception_29"));

			if (tabGroup == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_13"), Shared.SR.GetString(null, "LE_V2_Exception_24"));

			if (tabGroup == tab.TabGroup)
				return false;

			return this.MoveToGroup(tab, tabGroup, tabGroup.Tabs.Count);
		}

		/// <summary>
		/// Moves the specified <see cref="MdiTab"/> to the specified <see cref="MdiTabGroup"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <param name="tabGroup"><b>MdiTabGroup</b> to contain the specified tab</param>
		/// <param name="tabIndex">Index that the tab should be positioned within the specified group</param>
		/// <returns>Returns <b>True</b> if the tab was repositioned. Otherwise <b>False</b> is returned.</returns>
		/// <seealso cref="MdiTab.MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="MoveToNewGroup(MdiTab)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public virtual bool MoveToGroup( MdiTab tab, MdiTabGroup tabGroup, int tabIndex )
		{
			if (tab == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_28"), Shared.SR.GetString(null, "LE_V2_Exception_29"));

			if (tabGroup == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_13"), Shared.SR.GetString(null, "LE_V2_Exception_24"));

			if (!tab.IsFormVisible)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_35"));

			if (this.EventManager.InProgress(TabbedMdiEventIds.InitializeTab))
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_36"));

			if (this.EventManager.InProgress(TabbedMdiEventIds.InitializeTabGroup))
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_37"));

			// AS 4/14/03
			if (this.EventManager.InProgress(TabbedMdiEventIds.RestoreTab))
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_64"));

			// AS 4/21/05 BR03516
			// A tab cannot be moved to a tab group that contains other tab groups.
			if (tabGroup.HasTabGroups)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_66"));

			
			return this.MoveTab(tab, tabGroup, tabIndex, false, true);
		}

		/// <summary>
		/// Moves the specified <see cref="MdiTab"/> to an <see cref="MdiTabGroup"/> relative to the currently containing <see cref="MdiTab.TabGroup"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <param name="position">Relative <see cref="MdiTabGroup"/> to which the tab should be repositioned</param>
		/// <returns>Returns <b>True</b> if the tab was repositioned. Otherwise <b>False</b> is returned.</returns>
		/// <seealso cref="MdiTab.MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="MoveToNewGroup(MdiTab)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public bool MoveToGroup( MdiTab tab, MdiTabGroupPosition position )
		{
			if (tab == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_28"), Shared.SR.GetString(null, "LE_V2_Exception_29"));

			MdiTabGroup newGroup = this.TabGroupFromPosition(tab.TabGroup, position);

			return this.MoveToGroup(tab, newGroup);
		}

		/// <summary>
		/// Moves the specified <see cref="MdiTab"/> to an <see cref="MdiTabGroup"/> relative to the currently containing <see cref="MdiTab.TabGroup"/>
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to reposition</param>
		/// <param name="position">Relative <see cref="MdiTabGroup"/> to which the tab should be repositioned</param>
		/// <param name="tabIndex">Index that the tab should be positioned within the specified group</param>
		/// <returns>Returns <b>True</b> if the tab was repositioned. Otherwise <b>False</b> is returned.</returns>
		/// <seealso cref="MdiTab.MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="MoveToNewGroup(MdiTab)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="TabMoving"/>
		/// <seealso cref="TabMoved"/>
		public bool MoveToGroup( MdiTab tab, MdiTabGroupPosition position, int tabIndex )
		{
			if (tab == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_28"), Shared.SR.GetString(null, "LE_V2_Exception_29"));

			MdiTabGroup newGroup = this.TabGroupFromPosition(tab.TabGroup, position);

			return this.MoveToGroup(tab, newGroup, tabIndex);
		}
		#endregion //MoveToGroup

		#region Save/Load Binary/Xml

		#region LoadFrom
		/// <summary>
		/// Loads saved layout information from a stream containing the binary layout.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>LoadFromBinary</b> method is used in conjunction with the 
		/// <see cref="SaveAsBinary(System.IO.Stream)"/> method to persist the property settings and layout of the 
		/// <b>UltraTabbedMdiManager</b>. All property settings (except the <see cref="ImageList"/>)
		/// are deserialized including the <see cref="MdiTab"/> objects and <see cref="TabGroups"/>.
		/// After the information has been deserialized, the <see cref="RestoreTab"/> event is invoked 
		/// for each serialized <see cref="MdiTab"/> so that the appropriate <see cref="MdiTab.Form"/> 
		/// may be associated with the tab. If the tab is not associated with a Form in this event, 
		/// the tab will be discarded.</p>
		/// </remarks>
		/// <param name="stream">Stream containing the serialized <see cref="UltraTabbedMdiManager"/> information</param>
		/// <seealso cref="LoadFromXml(System.IO.Stream)"/>
		/// <seealso cref="RestoreTab"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="SaveAsBinary(System.IO.Stream)"/>
		/// <seealso cref="MdiTab.PersistedInfo"/>
		public void LoadFromBinary( System.IO.Stream stream )
		{
			this.Load(stream, false);
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Loads saved layout information from a file containing the binary layout.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>LoadFromBinary</b> method is used in conjunction with the 
		/// <see cref="SaveAsBinary(string)"/> method to persist the property settings and layout of the 
		/// <b>UltraTabbedMdiManager</b>. All property settings (except the <see cref="ImageList"/>)
		/// are deserialized including the <see cref="MdiTab"/> objects and <see cref="TabGroups"/>.
		/// After the information has been deserialized, the <see cref="RestoreTab"/> event is invoked 
		/// for each serialized <see cref="MdiTab"/> so that the appropriate <see cref="MdiTab.Form"/> 
		/// may be associated with the tab. If the tab is not associated with a Form in this event, 
		/// the tab will be discarded.</p>
		/// </remarks>
		/// <param name="filename">The name of the file containing the serialized <see cref="UltraTabbedMdiManager"/> information.</param>
		/// <seealso cref="LoadFromXml(string)"/>
		/// <seealso cref="RestoreTab"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="SaveAsBinary(string)"/>
		/// <seealso cref="MdiTab.PersistedInfo"/>
		public void LoadFromBinary(string filename)
		{
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Open, FileAccess.Read );

			try
			{
				this.Load(fileStream, false);
			}
			finally
			{
				fileStream.Close();
			}
		}

		/// <summary>
		/// Loads saved layout information from a stream containing the xml/soap layout.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>LoadFromXml</b> method is used in conjunction with the 
		/// <see cref="SaveAsXml(System.IO.Stream)"/> method to persist the property settings and layout of the 
		/// <b>UltraTabbedMdiManager</b>. All property settings (except the <see cref="ImageList"/>)
		/// are deserialized including the <see cref="MdiTab"/> objects and <see cref="TabGroups"/>.
		/// After the information has been deserialized, the <see cref="RestoreTab"/> event is invoked 
		/// for each serialized <see cref="MdiTab"/> so that the appropriate <see cref="MdiTab.Form"/> 
		/// may be associated with the tab. If the tab is not associated with a Form in this event, 
		/// the tab will be discarded.</p>
		/// </remarks>
		/// <param name="stream">Stream containing the serialized <see cref="UltraTabbedMdiManager"/> information</param>
		/// <seealso cref="LoadFromBinary(System.IO.Stream)"/>
		/// <seealso cref="RestoreTab"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="SaveAsXml(System.IO.Stream)"/>
		/// <seealso cref="MdiTab.PersistedInfo"/>
		public void LoadFromXml( System.IO.Stream stream )
		{
			this.Load(stream, true);
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Loads saved layout information from a file containing the xml/soap layout.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>LoadFromXml</b> method is used in conjunction with the 
		/// <see cref="SaveAsXml(string)"/> method to persist the property settings and layout of the 
		/// <b>UltraTabbedMdiManager</b>. All property settings (except the <see cref="ImageList"/>)
		/// are deserialized including the <see cref="MdiTab"/> objects and <see cref="TabGroups"/>.
		/// After the information has been deserialized, the <see cref="RestoreTab"/> event is invoked 
		/// for each serialized <see cref="MdiTab"/> so that the appropriate <see cref="MdiTab.Form"/> 
		/// may be associated with the tab. If the tab is not associated with a Form in this event, 
		/// the tab will be discarded.</p>
		/// </remarks>
		/// <param name="filename">File containing the serialized <see cref="UltraTabbedMdiManager"/> information</param>
		/// <seealso cref="LoadFromBinary(string)"/>
		/// <seealso cref="RestoreTab"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="SaveAsXml(string)"/>
		/// <seealso cref="MdiTab.PersistedInfo"/>
		public void LoadFromXml(string filename)
		{
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Open, FileAccess.Read );

			try
			{
				this.Load(fileStream, true);
			}
			finally
			{
				fileStream.Close();
			}
		}

		#endregion //LoadFrom

		#region SaveAs
		/// <summary>
		/// Saves layout information to a binary stream.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SaveAsBinary</b> method is used in conjunction with the 
		/// <see cref="LoadFromBinary(System.IO.Stream)"/> method to persist the property settings and layout of the 
		/// <b>UltraTabbedMdiManager</b>. All property settings (except the <see cref="ImageList"/>)
		/// are serialized including the <see cref="MdiTab"/> objects and <see cref="TabGroups"/>.
		/// When the method is invoked, the <see cref="StoreTab"/> event is invoked for each 
		/// <b>MdiTab</b> so that the <see cref="MdiTab.PersistedInfo"/> may be updated. This 
		/// property will be available upon deserialization and should be set to a value that 
		/// can be used to create the appropriate form in the <see cref="RestoreTab"/> event.</p>
		/// </remarks>
		/// <param name="stream">Stream containing the serialized <see cref="UltraTabbedMdiManager"/> information</param>
		/// <seealso cref="SaveAsXml(System.IO.Stream)"/>
		/// <seealso cref="RestoreTab"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="LoadFromBinary(System.IO.Stream)"/>
		public void SaveAsBinary( System.IO.Stream stream )
		{
			this.Save(stream, false);
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Saves layout information to a file in binary format.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SaveAsBinary</b> method is used in conjunction with the 
		/// <see cref="LoadFromBinary(string)"/> method to persist the property settings and layout of the 
		/// <b>UltraTabbedMdiManager</b>. All property settings (except the <see cref="ImageList"/>)
		/// are serialized including the <see cref="MdiTab"/> objects and <see cref="TabGroups"/>.
		/// When the method is invoked, the <see cref="StoreTab"/> event is invoked for each 
		/// <b>MdiTab</b> so that the <see cref="MdiTab.PersistedInfo"/> may be updated. This 
		/// property will be available upon deserialization and should be set to a value that 
		/// can be used to create the appropriate form in the <see cref="RestoreTab"/> event.</p>
		/// </remarks>
		/// <param name="filename">The file to receive the serialized <see cref="UltraTabbedMdiManager"/> information</param>		
		/// <seealso cref="SaveAsXml(string)"/>
		/// <seealso cref="RestoreTab"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="LoadFromBinary(string)"/>
		public void SaveAsBinary(string filename)
		{
			//MD 5/23/06 - BR12872
			// If the binary file already existed and wsa longer than the newly saved file,
			// the old file contents would still appear at the bottom of the file.
			// Now the file created each time so no old contents will remain.
			//
			//System.IO.FileStream fileStream = new FileStream( filename, FileMode.OpenOrCreate, FileAccess.Write );
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Create, FileAccess.Write );

			try
			{
				this.Save(fileStream, false);
			}
			finally
			{
				fileStream.Close();
			}
		}

		/// <summary>
		/// Saves layout information to an xml/soap stream.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SaveAsXml</b> method is used in conjunction with the 
		/// <see cref="LoadFromXml(System.IO.Stream)"/> method to persist the property settings and layout of the 
		/// <b>UltraTabbedMdiManager</b>. All property settings (except the <see cref="ImageList"/>)
		/// are serialized including the <see cref="MdiTab"/> objects and <see cref="TabGroups"/>.
		/// When the method is invoked, the <see cref="StoreTab"/> event is invoked for each 
		/// <b>MdiTab</b> so that the <see cref="MdiTab.PersistedInfo"/> may be updated. This 
		/// property will be available upon deserialization and should be set to a value that 
		/// can be used to create the appropriate form in the <see cref="RestoreTab"/> event.</p>
		/// </remarks>
		/// <param name="stream">Stream to receive the serialized <see cref="UltraTabbedMdiManager"/> information</param>
		/// <seealso cref="SaveAsBinary(System.IO.Stream)"/>
		/// <seealso cref="RestoreTab"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="LoadFromXml(System.IO.Stream)"/>
		/// <seealso cref="MdiTab.PersistedInfo"/>
		public void SaveAsXml( System.IO.Stream stream )
		{
			this.Save(stream, true);
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Saves layout information to a file in xml/soap format.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SaveAsXml</b> method is used in conjunction with the 
		/// <see cref="LoadFromXml(string)"/> method to persist the property settings and layout of the 
		/// <b>UltraTabbedMdiManager</b>. All property settings (except the <see cref="ImageList"/>)
		/// are serialized including the <see cref="MdiTab"/> objects and <see cref="TabGroups"/>.
		/// When the method is invoked, the <see cref="StoreTab"/> event is invoked for each 
		/// <b>MdiTab</b> so that the <see cref="MdiTab.PersistedInfo"/> may be updated. This 
		/// property will be available upon deserialization and should be set to a value that 
		/// can be used to create the appropriate form in the <see cref="RestoreTab"/> event.</p>
		/// </remarks>
		/// <param name="filename">The file to receive the serialized <see cref="UltraTabbedMdiManager"/> information</param>		
		/// <seealso cref="SaveAsXml(string)"/>
		/// <seealso cref="SaveAsBinary(string)"/>
		/// <seealso cref="RestoreTab"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="LoadFromXml(string)"/>
		/// <seealso cref="MdiTab.PersistedInfo"/>
		public void SaveAsXml(string filename)
		{
			//MD 5/23/06 - BR12872
			// If the xml file already existed and was longer than the newly saved file,
			// the old file contents would still appear at the bottom of the file.
			// Now the file created each time so no old contents will remain.
			//
			//System.IO.FileStream fileStream = new FileStream( filename, FileMode.OpenOrCreate, FileAccess.Write );
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Create, FileAccess.Write );

			try
			{
				this.Save(fileStream, true);
			}
			finally
			{
				fileStream.Close();
			}
		}

		#endregion //SaveAs

		#endregion //Save/Load Binary/Xml

		#region TabGroupFromPoint
		/// <summary>
		/// Returns an <see cref="MdiTabGroup"/> located at the specified screen coordinates or null, if a tab group does not exist at the specified point.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabGroupFromPoint</b> method returns the <see cref="MdiTabGroup"/> that exists at the 
		/// specified screen coordinates or null (Nothing in VB) if no <b>MdiTabGroup</b> occupies that area.</p>
		/// <p class="note">Note: This method checks for an <see cref="MdiTabGroup"/> that occupies the area and may not be 
		/// over an <see cref="MdiTabGroupControl"/> or <see cref="SplitterBarControl"/>.</p>
		/// </remarks>
		/// <param name="point">Location in screen coordinates</param>
		/// <returns>An <see cref="MdiTabGroup"/> located at the specified coordinates or null (Nothing in VB)</returns>
		/// <seealso cref="MdiTabGroup"/>
		/// <seealso cref="TabFromPoint"/>
		public MdiTabGroup TabGroupFromPoint( Point point )
		{
			// Note: This checks for a tab group that occupies an AREA - it does not return
			// a tab group based on the tab group control that would be/is positioned. This
			// should not be changed since this routine is also used for dragging.
			// If it becomes important to have a method that returns a TabGroupControlFromPoint
			// then implement that as such.
			//
			if (this.mdiClient == null || this.mdiClient.IsDisposed || this.mdiClient.Disposing)
				return null;

			// get the screen point in mdi client coordinates
			Point mdiClientPoint = this.mdiClient.PointToClient(point);

			// if the point is not in the mdi client area, return null
			if (!this.mdiClient.ClientRectangle.Contains(mdiClientPoint))
				return null;

			if (this.MdiParent == null || this.MdiParent.IsDisposed || this.MdiParent.Disposing)
				return null;

			// get the coordinates in form coordinates
			Point formPoint = this.MdiParent.PointToClient(point);

			
			return this.TabGroupFromPointHelper(this.TabGroups, formPoint);
		}

		// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
		private MdiTabGroup TabGroupFromPointHelper( MdiTabGroupsCollection tabGroups, Point formPoint )
		{
			foreach(MdiTabGroup tabGroup in tabGroups)
			{
				Rectangle tabGroupArea = this.TabGroupManager.GetTabGroupArea(tabGroup);

				if (tabGroupArea.Contains(formPoint))
				{
					// if the tabgroup at this location has nested groups,
					// see if the point is within one of those groups
					if (tabGroup.HasTabGroups)
					{
						MdiTabGroup tempGroup = this.TabGroupFromPointHelper(tabGroup.TabGroups, formPoint);

						return tempGroup != null ? tempGroup : tabGroup;
					}
					else
						return tabGroup;
				}
			}

			return null;
		}
		#endregion //TabGroupFromPoint

		#region TabFromPoint
		/// <summary>
		/// Returns an <see cref="MdiTab"/> located at the specified screen coordinates or null, if a tab does not exist at the specified location.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabFromPoint</b> method returns the <see cref="MdiTab"/> that exists at the 
		/// specified screen coordinates or null (Nothing in VB) if no <b>MdiTab</b> exists at that point.</p>
		/// </remarks>
		/// <param name="point">Location in screen coordinates</param>
		/// <returns>A <see cref="MdiTab"/> located at the specified coordinates or null (Nothing in VB)</returns>
		/// <seealso cref="MdiTab"/>
		/// <seealso cref="TabGroupFromPoint"/>
		public MdiTab TabFromPoint( Point point )
		{
			// get the tab group that contains the point
			MdiTabGroup tabGroup = this.TabGroupFromPoint(point);

			if (tabGroup == null)
				return null;

			// get the associated tab group control
			MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;

			if (tabGroupControl == null || tabGroupControl.IsDisposed || tabGroupControl.Disposing)
				return null;

			// get the coordinates with respect to the tab group control
			Point clientPt = tabGroupControl.PointToClient(point);
			UIElement mainElement = ((IUltraControlElement)tabGroupControl).MainUIElement;

			if (mainElement == null)
				return null;

			// get the element at the coordinates
			UIElement elementFromPt = mainElement.ElementFromPoint(clientPt);

			// if we got an element, return its mdi tab context
			if (elementFromPt != null)
				return elementFromPt.GetContext(typeof(MdiTab), true) as MdiTab;

			return null;
		}
		#endregion //TabFromPoint

		#region TabFromForm
		/// <summary>
		/// Returns the <see cref="MdiTab"/> object associated with the specified <see cref="System.Windows.Forms.Form"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabFromForm</b> method returns the <see cref="MdiTab"/> associated 
		/// with a particular <see cref="MdiTab.Form"/> or null (Nothing in VB) if there is no 
		/// <b>MdiTab</b> associated with the form.</p>
		/// </remarks>
		/// <param name="form">Form whose associated <see cref="MdiTab"/> should be returned.</param>
		/// <returns>The <see cref="MdiTab"/> associated with the <paramref name="form"/> or null.</returns>
		/// <seealso cref="MdiTab"/>
		/// <seealso cref="TabFromPoint"/>
		/// <seealso cref="ActiveTab"/>
		public MdiTab TabFromForm( Form form )
		{
			return this.GetTabFromForm(form, true, true, true);
		}
		#endregion //TabFromForm

		#region TabFromKey
		/// <summary>
		/// Returns the <see cref="MdiTab"/> with the specified <see cref="KeyedSubObjectBase.Key"/>
		/// </summary>
		/// <param name="key">Key of the <b>MdiTab</b> to locate</param>
		/// <returns>An <b>MdiTab</b> or null (Nothing in VB) if no tab with the specified <paramref name="key"/> exists.</returns>
		public MdiTab TabFromKey( string key )
		{
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.TabGroups)
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
			{
				foreach(MdiTab tab in tabGroup.Tabs)
				{
					if (tab.Key == key)
						return tab;
				}
			}

			foreach(MdiTab tab in this.HiddenTabs)
			{
				if (tab.Key == key)
					return tab;
			}

			return null;
		}

		// AS 5/13/03
		// Since keys are not required to be unique, there can be multiple
		// tabs with the same key. Therefore we need an additional method.
		//
		/// <summary>
		/// Returns an array of <see cref="MdiTab"/> objects with the specified <see cref="KeyedSubObjectBase.Key"/>
		/// </summary>
		/// <param name="key">Key of the <b>MdiTab</b> to locate</param>
		/// <returns>An array of <b>MdiTab</b> objects or null (Nothing in VB) if no tab with the specified <paramref name="key"/> exists.</returns>
		public MdiTab[] TabsFromKey( string key )
		{
			ArrayList list = null;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.TabGroups)
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
			{
				foreach(MdiTab tab in tabGroup.Tabs)
				{
					if (tab.Key == key)
					{
						if (list == null)
							list = new ArrayList();

						list.Add( tab );
					}
				}
			}

			foreach(MdiTab tab in this.HiddenTabs)
			{
				if (tab.Key == key)
				{
					if (list == null)
						list = new ArrayList();

					list.Add( tab );
				}
			}

			if (list == null)
				return null;

			list.TrimToSize();

			return (MdiTab[])list.ToArray(typeof(MdiTab));
		}
		#endregion //TabFromKey

		#region Reset
		/// <summary>
		/// Resets the properties of the component to their default values.
		/// </summary>
		public void Reset()
		{
			this.ResetComponent(false);
		}
		#endregion //Reset

		#endregion //Public

		#region Protected Methods

		#region OnSubObjectPropChanged
		/// <summary>
		/// Called when a property on a sub object has changed.
		/// </summary>
        /// <param name="propChangeInfo">A structure containing the property change information.</param>
		protected virtual void OnSubObjectPropChanged(PropChangeInfo propChangeInfo) 
		{
			if (this.HasAppearances && propChangeInfo.Source == this.appearances)
			{
				this.NotifyPropChange(TabbedMdiPropertyIds.Appearances, propChangeInfo);
			}
			else if (this.HasTabSettings && propChangeInfo.Source == this.tabSettings)
			{
				this.DirtyTabGroups(TabbedMdiPropertyIds.TabSettings,propChangeInfo);
				this.NotifyPropChange(TabbedMdiPropertyIds.TabSettings, propChangeInfo);
			}
			else if (this.HasTabGroupSettings && propChangeInfo.Source == this.tabGroupSettings)
			{
				// AS 6/6/06
				// Moved from OnPropertyChanged so the cache is dirty
				// when the dirtytabgroups is called.
				//
				StyleUtils.DirtyCachedPropertyVals(this);

				this.DirtyTabGroups(TabbedMdiPropertyIds.TabGroupSettings,propChangeInfo);
				this.NotifyPropChange(TabbedMdiPropertyIds.TabGroupSettings, propChangeInfo);
			}
			else if (this.TabGroups == propChangeInfo.Source)
			{
				this.NotifyPropChange(TabbedMdiPropertyIds.TabGroups, propChangeInfo);
			}
			else if (this.HasAppearance && propChangeInfo.Source == this.appearanceHolder.RootAppearance)
			{
				this.DirtyTabGroups(TabbedMdiPropertyIds.Appearance,propChangeInfo);
				this.NotifyPropChange(TabbedMdiPropertyIds.Appearance, propChangeInfo);
			}
			else if (this.HasSplitterAppearance && propChangeInfo.Source == this.splitterAppearanceHolder.RootAppearance)
			{
				this.NotifyPropChange(TabbedMdiPropertyIds.SplitterAppearance, propChangeInfo);
			}
			else if (propChangeInfo.Source == this.hiddenTabs)
			{
				this.NotifyPropChange(TabbedMdiPropertyIds.HiddenTabs, propChangeInfo);
			}
			else
				// AS 4/30/03 FxCop Change
				// Explicitly call the overload that takes an IFormatProvider
				//
				//Debug.Assert(false, string.Format("Unaccounted for sub object property change - {0}", propChangeInfo.ToString()));
				Debug.Assert(false, string.Format(null, "Unaccounted for sub object property change - {0}", propChangeInfo.ToString()));
		}
		#endregion //OnSubObjectPropChanged

		#region CreateMdiTab
		/// <summary>
		/// Creates a new <see cref="MdiTab"/> instance for the specified form.
		/// </summary>
		/// <param name="form">Associated <see cref="System.Windows.Forms.Form"/></param>
		/// <returns>A new <see cref="MdiTab"/> for the specified <paramref name="form"/></returns>
		protected virtual MdiTab CreateMdiTab( Form form )
		{
			if (form == null)
				return null;

			return new MdiTab(form);
		}
		#endregion //CreateMdiTab

		#region CreateMdiTabGroup
		/// <summary>
		/// Creates a new <see cref="MdiTabGroup"/>
		/// </summary>
		/// <returns>A new <see cref="MdiTabGroup"/></returns>
		protected virtual MdiTabGroup CreateMdiTabGroup()
		{
			return new MdiTabGroup(this);
		}
		#endregion //CreateMdiTabGroup

		#region CreateMdiTabGroupControl
		/// <summary>
		/// Creates an <see cref="MdiTabGroupControl"/> to contain the specified <see cref="MdiTabGroup"/>
		/// </summary>
		/// <param name="tabGroup">MdiTabGroup to be displayed by the <b>MdiTabGroupControl</b></param>
		/// <returns>A new <b>MdiTabGroupControl</b> created to display the specified <b>MdiTabGroup</b></returns>
		protected virtual MdiTabGroupControl CreateMdiTabGroupControl(MdiTabGroup tabGroup)
		{
			// create an MdiTabGroupControl to house the MdiTabGroup
			return new MdiTabGroupControl(tabGroup, this);
		}
		#endregion //CreateMdiTabGroupControl

		#region CreateSplitterBarControl
		/// <summary>
		/// Creates an <see cref="SplitterBarControl"/>
		/// </summary>
		/// <returns>A new <b>SplitterBarControl</b> used to allow resizing of <see cref="MdiTabGroup"/> objects</returns>
		protected virtual SplitterBarControl CreateSplitterBarControl()
		{
			// create an SplitterBarControl
			// return new SplitterBarControl(this);
			// JDN 8/12/04 NAS2004 Vol3 - Support for VisualStudio2005 style
			// Suppress leading edge of splitter bar for all styles of tabbed mdi
			// since there is no leading edge in any VS or VS2005
			SplitterBarControl splitterBarControl = new SplitterBarControl(this);
			splitterBarControl.SuppressLeadingEdge = this.SuppressSplitterLeadingEdge;
			return splitterBarControl;
		}
		#endregion //CreateSplitterBarControl

		#region Dispose
		/// <summary>
		/// Invoked when the component must release the resources it is using.
		/// </summary>
		/// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.isDisposing = true;

                // MD 10/15/04 - Default Store Support
                if (this.saveSettings)
                    this.SaveComponentSettings();

				

				// AS 4/30/03
				this.UnsubclassMdiClient();

				// AS 4/6/04 Moved up from below
				this.UnhookMdiParent();

				if (this.appearanceHolder != null)
				{
					this.appearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.appearanceHolder.Reset();
					this.appearanceHolder = null;
				}

				if (this.appearances != null && !this.appearances.Disposed)
				{
					this.appearances.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.appearances.Dispose();
					this.appearances = null;
				}

				if (this.tabGroups != null && !this.tabGroups.Disposed)
				{
					this.tabGroups.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.tabGroups.Dispose();
					this.tabGroups = null;
				}

				if (this.tabSettings != null && !this.tabSettings.Disposed)
				{
					this.tabSettings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.tabSettings.Dispose();
					this.tabSettings = null;
				}

				if (this.tabGroupSettings != null && !this.tabGroupSettings.Disposed)
				{
					this.tabGroupSettings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.tabGroupSettings.Dispose();
					this.tabGroupSettings = null;
				}

				// AS 1/21/04 WTB1303
				// The MenuItem class seems to have a bug where
				// they null out the field that they use to manage
				// the menu item data before raising the event
				// so unhook the event before disposing the
				// menu item or context menu.
				//
				if (this.menuItems != null)
				{
					foreach(MenuItem mi in this.menuItems)
					{
						if (mi != null)
							mi.Click -= this.MenuItemClickHandler;
					}
				}

				if (this.tabContextMenu != null)
				{
					// make sure no one is referencing it first
					foreach(MdiTabGroupControl ctrl in this.TabGroupControls.Values)
					{
						if (ctrl.Disposing || ctrl.IsDisposed)
							continue;

						ctrl.ContextMenu = null;
					}

					// AS 4/30/03
					this.tabContextMenu.Popup -= new EventHandler(this.OnContextMenuPopup);

					this.tabContextMenu.Dispose();
					this.tabContextMenu = null;
				}

				if (this.menuItems != null)
				{
					foreach(MenuItem mi in this.menuItems)
					{
						// AS 1/21/04
						if (mi != null)
							mi.Dispose();
					}

					this.menuItems = null;
				}
				
				// AS 4/21/03 WTB818
				if (this.newTabGroupHorzImage != null)
				{
					this.newTabGroupHorzImage.Dispose();
					this.newTabGroupHorzImage = null;
				}

				if (this.newTabGroupHorzImgStream != null)
				{
					this.newTabGroupHorzImgStream.Close();
					this.newTabGroupHorzImgStream = null;
				}

				if (this.newTabGroupVertImage != null)
				{
					// AS 1/21/04
					// We were disposing the member that
					// was disposed above.
					//
					//this.newTabGroupHorzImage.Dispose();
					//this.newTabGroupHorzImage = null;
					this.newTabGroupVertImage.Dispose();
					this.newTabGroupVertImage = null;
				}

				if (this.newTabGroupVertImgStream != null)
				{
					this.newTabGroupVertImgStream.Close();
					this.newTabGroupVertImgStream = null;
				}

				// JDN 8/16/04 NAS2004 Vol3 - VisualStudio2005 style
				if (this.newTabGroupVertImageVS2005 != null)
				{
					this.newTabGroupVertImageVS2005.Dispose();
					this.newTabGroupVertImageVS2005 = null;
				}

				// JDN 8/16/04 NAS2004 Vol3 - VisualStudio2005 style
				if (this.newTabGroupVertImgStreamVS2005 != null)
				{
					this.newTabGroupVertImgStreamVS2005.Close();
					this.newTabGroupVertImgStreamVS2005 = null;
				}

				// JJD 8/23/06 - NA2006 vol 3
				// Unhook from the Offixe 2007 view style change event
				Office2007ColorTable.ColorSchemeChanged -= new EventHandler(OnOffice2007ColorSchemeChanged);


				// AS 4/6/04 Moved up
				// AS 4/2/04 WTB1402
				//this.UnhookMdiParent();
			}

			base.Dispose(disposing);
		}
		#endregion //Dispose

		// AS 3/5/03 UWG2003
		#region OnBeginInit/OnEndInit
		/// <summary>
		/// Invoked during the <see cref="ISupportInitialize.BeginInit"/> of the component.
		/// </summary>
		protected virtual void OnBeginInit()
		{
		}

		/// <summary>
		/// Invoked during the <see cref="ISupportInitialize.EndInit"/> of the component.
		/// </summary>
		protected virtual void OnEndInit()
		{
		}
		#endregion //OnBeginInit/OnEndInit

		#region ResolveSplitterAppearance
		// AS 5/5/06 AppStyling
		// Added orientation specific methods for the splitter bars.
		//
		private void ResolveSplitterAppearanceVert(ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			this.ResolveSplitterAppearance(ref appearance, ref requestedProps, Orientation.Vertical);
		}

		private void ResolveSplitterAppearanceHorz(ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			this.ResolveSplitterAppearance(ref appearance, ref requestedProps, Orientation.Horizontal);
		}

		/// <summary>
		/// Invoked when the appearance for the splitter should be resolved.
		/// </summary>
		/// <param name="appearance">AppearanceData structure to update</param>
		/// <param name="requestedProps">Appearance properties to resolve</param>
		/// <param name="orientation">Orientation of the splitter bar being resolved</param>
		// AS 5/5/06
		//internal protected virtual void ResolveSplitterAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		internal protected virtual void ResolveSplitterAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps, Orientation orientation )
		{
			// AS 4/27/06 AppStyling
			AppStyling.ResolutionOrderInfo order;
			// AS 5/5/06 Added orientation specific roles
			//AppStyling.UIRole role = StyleUtils.GetRole(this, StyleUtils.Role.SplitterBar, out order);
			AppStyling.UIRole role = StyleUtils.GetRole(this, 
				orientation == Orientation.Vertical
				? StyleUtils.Role.SplitterBarVert
				: StyleUtils.Role.SplitterBarHorz, out order);

			#region Splitter - Normal
			if (null != role && order.UseStyleBefore)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);

			if (order.UseControlInfo)
			{
				if (this.HasSplitterAppearance)
					this.SplitterAppearance.MergeData(ref appearance, ref requestedProps);
			}

			if (null != role && order.UseStyleAfter)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);
			#endregion //Splitter - Normal

			if (order.UseControlInfo)
			{
				if (this.HasAppearance)
					this.Appearance.MergeData(ref appearance, ref requestedProps);
			}

			#region Defaults

			// AS 5/2/06 AppStyling
			ViewStyle viewStyle = this.ViewStyleResolved;

			#region BackColor
			if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BackColor))
			{
                // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
                //
				//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
                //Color backColor = viewStyle == ViewStyle.Office2003 ?
                //    this.MSColors.OutlookNavPaneLabelItemGradientLight :
                //    SystemColors.Control;
                Color backColor;
                if (viewStyle == ViewStyle.Office2003)
                    backColor = this.MSColors.OutlookNavPaneLabelItemGradientLight;
                else if (viewStyle == ViewStyle.Office2007)
                    backColor = Office2007ColorTable.Colors.TabBorderColor;
                else
                    backColor = SystemColors.Control;

				appearance.BackColor = backColor;
				requestedProps &= ~AppearancePropFlags.BackColor;
			}
			#endregion //BackColor

			#region BorderColor3DBase
			if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BorderColor3DBase))
			{
				// base it on the backcolor
				//appearance.BorderColor3DBase = appearance.BackColor;
				// JDN 8/12/04 NAS2004 Vol3 - VisualStudio2005 style
				
				appearance.BorderColor3DBase = appearance.BackColor;
				requestedProps &= ~AppearancePropFlags.BorderColor3DBase;
			}			
			#endregion //BorderColor3DBase

			#region BorderColor
			if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BorderColor))
			{
				//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
				//Color borderColor = this.ViewStyle ==	ViewStyle.Office2003 ?
				//Office2003Colors.ToolBorder :
				// just darken the backcolor
				//Infragistics.Win.DrawUtility.Dark(appearance.BackColor);
				// JDN 8/12/04 NAS2004 Vol3 - VisualStudio2005 style
				Color borderColor;
				if ( viewStyle == ViewStyle.Office2003 )
					borderColor = this.MSColors.ToolBorder;
				else
					if ( viewStyle == ViewStyle.VisualStudio2005 )
					borderColor = SystemColors.AppWorkspace;
				else
					borderColor = Infragistics.Win.DrawUtility.Dark(appearance.BackColor);

				appearance.BorderColor = borderColor;
				requestedProps &= ~AppearancePropFlags.BorderColor;
			}			
			#endregion //BorderColor

			#endregion //Defaults
		}

		#endregion //ResolveSplitterAppearance

		#endregion //Protected Methods

		#region Internal

		#region Activation Related

		#region ActivateMdiChild
		internal void ActivateMdiChild( MdiTab tab, bool invokeTabSelecting, bool invokeTabSelected, bool allowCancel, bool activate )
		{
			Debug.Assert( !this.IsActivatingMdiChild, "Another 'MdiTab' is already being activated." );

			// if the component is activating the mdi child, we don't want to do anything here
			//
			if (this.isActivatingMdiChild)
				return;

			// if there is no tab or the tab is not associated with a form, there is nothing to do
			if (tab == null || tab.Form == null)
				return;

			// keep a flag to indicating that the component is activating a form
			//
			this.isActivatingMdiChild = true;

			try
			{
				// if we are supposed to invoke the selection events, fire the selecting first
				if (invokeTabSelecting)
				{
					// raise the TabSelecting
					CancelableMdiTabEventArgs args = new CancelableMdiTabEventArgs(tab);

					this.InvokeEvent( TabbedMdiEventIds.TabSelecting, args );

					// if we cancel and the user cancelled, exit
					if (allowCancel && args.Cancel)
						return;
				}

				// now activate the form
				if (activate)
				{
					// AS 6/27/03 WTB1073
					// If the form is already the ActiveMdiChild, calling Activate
					// doesn't seem to do anything. This causes a problem when a control
					// on the mdi parent had focus and we try to "activate" the already
					// active mdi child. When this happens, we'll try to get the furthest
					// down active control and give it focus.
					//
					if (tab.IsFormActive)
					{
						// make sure we have access to the mdi parent and that our form
						// is not already its active control - which it seems to do when
						// the form has the input focus
						if (this.MdiParent != null && this.MdiParent.ActiveControl != tab.Form)
						{
							// get the active control on the tab's mdi child form
							Control activeControl = tab.Form.ActiveControl;

							// walk down the ActiveControl chain until we get
							// the deepest descendant
							while (activeControl is IContainerControl)
								activeControl = ((IContainerControl)activeControl).ActiveControl;

							// if we got to a control
							// AND it does not already have focus, which it should not if the mdi child was not the active control of the mdi parent, 
							// AND it can take focus,
							// then give it focus
							if (activeControl != null			&& 
								!activeControl.ContainsFocus	&&
								activeControl.CanFocus)
							{
								// MD 1/4/07 - FxCop
								// Moved this code into a centralized helper method
								//try
								//{
								//    activeControl.Focus();
								//}
								//catch (System.Security.SecurityException)
								//{
								//    // we'll get a security exception if we don't have
								//    // the rights to give it focus. i don't think
								//    // we want to assert for permissions since its
								//    // not necessarily our control that we would be
								//    // giving focus and we don't want to open a security
								//    // hole
								//}
								Utilities.FocusControl( activeControl );
							}
						}
					}
					else
					{
						// MD 1/4/07 - FxCop
						// Use the safe version of Activate
						//tab.Form.Activate();
						Utilities.ActivateForm( tab.Form );
					}
				}

				// make sure that the tab is the selected tab in the tabgroup
				// but do so in a way that will not invoke the selection events
				// again.
				// AS 7/30/07 BR25281
				if (null != tab.TabGroup)
					tab.TabGroup.InitializeSelectedTab(tab);

				// AS 10/9/03 WTB1192
				// When a tab is activated, it should be brought into view.
				tab.EnsureTabInView();

				// raise the TabSelected...
				if (invokeTabSelected)
					this.InvokeEvent( TabbedMdiEventIds.TabSelected, new MdiTabEventArgs(tab) );
			}
			finally
			{
				this.isActivatingMdiChild = false;

				// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
				this.InvokeTabActivatedEvent();
			}
		}
		#endregion //ActivateMdiChild

		#region ShouldSubclasserAllowFormActivation
		internal bool ShouldSubclasserAllowFormActivation( Form form )
		{
			// if we're not involved in the process, allow it
			if (this.IsInitializing)
				return true;

			// if we are the ones activating this then we have already checked
			// with the programmer to see if this should be selected.
			if (this.IsActivatingMdiChild)
				return true;

			
			MdiTab tab = this.GetTabFromForm(form, false, false, false);

			// apparantly this happens under odd scenarios like
			// changing the MdiParent of Form already displayed as
			// a non-mdichid form
			//Debug.Assert( tab != null, "An MdiChild without an associated 'MdiTab' is being activated");

			if (tab == null)
				return true;

			CancelableMdiTabEventArgs args = new CancelableMdiTabEventArgs(tab);

			this.InvokeEvent(TabbedMdiEventIds.TabSelecting, args);

			return !args.Cancel;
		}
		#endregion //ShouldSubclasserAllowFormActivation

		#region ShouldSubclasserNotifyFormActivation
		internal bool ShouldSubclasserNotifyFormActivation( Form form )
		{
			MdiTab tab = this.GetTabFromForm(form, true, true, true);

			// if there is no tab associated with it yet, then let it happen
			if (tab != null)
				return !tab.IsSelected;

			return true;
		}
		#endregion //ShouldSubclasserNotifyFormActivation

		#region AfterSubclasserFormActivation
		internal void AfterSubclasserFormActivation(Form form)
		{
			// if we're not involved in the process, allow it
			if (this.IsInitializing)
				return;

			// if we are the ones activating this then we don't need
			// to do anything with this info
			if (this.IsActivatingMdiChild)
				return;

			MdiTab tab = this.TabFromForm(form);

			// apparantly this happens under odd scenarios like
			// changing the MdiParent of Form already displayed as
			// a non-mdichid form
			//Debug.Assert( tab != null, "An MdiChild without an associated 'MdiTab' is being activated");

			if (tab == null)
				return;

			MdiTabGroup tabGroup = tab.TabGroup;

			if (tabGroup != null)
				tabGroup.InitializeSelectedTab(tab);
			
			// AS 10/9/03 WTB1192
			// When a tab is activated, it should be brought into view.
			tab.EnsureTabInView();

			MdiTabEventArgs args = new MdiTabEventArgs(tab);

			this.InvokeEvent(TabbedMdiEventIds.TabSelected, args);

			// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
			this.InvokeTabActivatedEvent();
		}
		#endregion //AfterSubclasserFormActivation

		#region CanAllowSelectionCancel
		internal bool CanAllowSelectionCancel( MdiTab tab )
		{
			// AS 4/16/03
			// only allow cancelling if we can cancel it - i.e.
			// if its already active, we can't prevent it from being activated.
			if (tab == null)
				return true;

			if (tab == this.ActiveTab)
				return false;

			// if the tab is topmost in its group, then we cannot
			// 
			if (this.GetTopMostTab(tab.TabGroup) == tab)
				return false;

			return true;
		}
		#endregion //CanAllowSelectionCancel
		
		// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
		#region InvokeTabActivatedEvent
		private void InvokeTabActivatedEvent()
		{
			Form activeMdiChild = this.MdiParent == null ? null : this.MdiParent.ActiveMdiChild;
			MdiTab activeTab = activeMdiChild == null ? null : this.GetTabFromForm(activeMdiChild, true, false, false);

			Debug.Assert(!this.IsActivatingMdiChild, "We are currently in the process of activating the tab and should wait until that is complete.");
			Debug.Assert(!this.SubclasserActivatingMdiChild, "The subclasser is activating the form so we should wait for that to complete.");

			if (!this.IsActivatingMdiChild			&&
				!this.SubclasserActivatingMdiChild	&&
				activeTab != this.lastActivatedTab	&&
				activeTab != null)
			{
				this.lastActivatedTab = activeTab;

				// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
				this.VerifyMaximizedTabGroup();
					
				this.InvokeEvent(TabbedMdiEventIds.TabActivated, new MdiTabEventArgs(activeTab));
			}
			else
			{
				this.lastActivatedTab = activeTab;

				// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
				this.VerifyMaximizedTabGroup();
			}
		}
		#endregion //InvokeTabActivatedEvent

		#endregion //Activation Related

		#region TabGroup/Tab related

		#region DirtyGroupPosition
		internal void DirtyGroupPosition( MdiTabGroup tabGroup )
		{
			if (this.IsInitializing )
				return;

			this.TabGroupManager.DirtyTabGroup(tabGroup);

			this.PositionAndSizeTabGroups();
		}
		#endregion //DirtyGroupPosition

		#region InitializeTabFormSettings
		internal void InitializeTabFormSettings( MdiTab tab )
		{
			if (tab == null || tab.IsFormInitialized || !this.Enabled)
				return;

			if (tab.MdiChild == null)
				return;

			if (tab.IsCustomTab)
				tab.CustomMdiTab.IsMdiTab = true;

			// MD 1/4/07 - FxCop
			// See comment on assert below
			//SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);

			try
			{
				// MD 1/4/07 - FxCop
				// This is not needed due to the SuppressUnmanagedCodeSecurityAttribute on the NativeWindowMethods class
				//perm.Assert();

				// AS 10/29/03 WTB1214
				// Go through a single point to turn redraw on/off.
				//NativeWindowMethods.SetRedraw(this.mdiClient, false, false);
				this.SetRedraw(false);

				tab.InitializeTabFormSettings();

				
				
				this.DelayedRedraw();
			}
			catch( System.Security.SecurityException )
			{
				// make sure we initialized the settings
				tab.InitializeTabFormSettings();
			}
		}
		#endregion //InitializeTabFormSettings

		// AS 4/12/03 WTB796
		#region DelayedMdiClientRedraw
		private void DelayedMdiClientRedraw()
		{
			// AS 10/29/03 WTB1214
			// Go through a single point to turn redraw on/off.
			//
			//			SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
			//
			//			try
			//			{
			//				perm.Assert();
			//
			//				NativeWindowMethods.SetRedraw(this.mdiClient, true, true);
			//			}
			//			catch (System.Security.SecurityException)
			//			{
			//			}
			this.SetRedraw(true);
		}
		#endregion //DelayedMdiClientRedraw

		#region OnApplicationIdle
		private void OnApplicationIdle(object sender, EventArgs e)
		{
			Application.Idle -= new EventHandler(this.OnApplicationIdle);
			this.hookedIdle = false;

			// AS 10/29/03 WTB1214
			// Go through a single point to turn redraw on/off.
			//
			//			SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
			//
			//			try
			//			{
			//				perm.Assert();
			//
			//				NativeWindowMethods.SetRedraw(this.mdiClient, true, true);
			//			}
			//			catch (System.Security.SecurityException)
			//			{
			//			}
			this.SetRedraw(true);
		}
		#endregion //OnApplicationIdle

		#region MdiChildVisibilityChanged
		internal void MdiChildVisibilityChanged( MdiTab tab )
		{
			// if the form is being made visible...
			if (tab.IsFormVisible)
			{
				if (tab.TabGroup != null)
				{
					tab.TabGroup.DirtyTabManager(MdiTabGroup.TabManagerChangeType.Collection | MdiTabGroup.TabManagerChangeType.Invalidate);
					return;
				}

				// add it to a tabgroup
				this.AddTabToTabGroup(tab);
			}
			else // otherwise if its being hidden
			{
				// remove it from its tab group
				this.RemoveTabFromTabGroup(tab);
			}
		}
		#endregion //MdiChildVisibilityChanged

		#region MoveAllTabs
		internal void MoveAllTabs(MdiTabGroup tabGroup, MdiTabGroupPosition position)
		{
			// AS 4/14/03
			if (this.EventManager.InProgress(TabbedMdiEventIds.RestoreTab))
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_64"));

			// get the tabgroup we'll be moving the tabs to
			MdiTabGroup newTabGroup = this.TabGroupFromPosition(tabGroup, position);

			if (newTabGroup == tabGroup)
				return;

			// suspend any processing
			this.SuspendLayout();

			try
			{
				// AS 5/13/03
				// Should be iterating backwards.
				// assuming its different, move them
				//for( int i = tabGroup.Tabs.Count - 1; i >= 0; i++ )
				for( int i = tabGroup.Tabs.Count - 1; i >= 0; i-- )
				{
					MdiTab tab = tabGroup.Tabs[i];

					// AS 3/18/05 NA 2005 Vol 2
					// It does not make sense to be firing the TabDisplaying
					// since these tabs are already in a tab group. We should
					// fire the tabmoving.
					//
					//this.MoveTab(tab, newTabGroup, 0, true);
					this.MoveTab(tab, newTabGroup, 0, false, true);
				}
			}
			finally
			{
				this.ResumeLayout(true);
			}
		}
		#endregion //MoveAllTabs

		#region RepositionTab
		internal void RepositionTab( MdiTab tabToReposition, MdiTab relativeTab, MdiTabPosition relativePosition )
		{
			// AS 4/14/03
			if (this.EventManager.InProgress(TabbedMdiEventIds.RestoreTab))
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_64"));

			Debug.Assert(tabToReposition != null, "A null 'MdiTab' cannot be repositioned.");
			Debug.Assert(relativeTab != null, "A null 'MdiTab' relative tab was specified.");

			if (!tabToReposition.IsFormVisible)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_35"));

			if (!relativeTab.IsFormVisible)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_38"));

			// AS 3/18/05 NA 2005 Vol 2
			// It does not make sense to be firing the TabDisplaying
			// since these tabs are already in a tab group. We should
			// fire the tabmoving.
			//
			bool invokeTabDisplayEvents = tabToReposition.TabGroup == null;
			bool invokeTabMoveEvents = !invokeTabDisplayEvents;

			switch(relativePosition)
			{
				case MdiTabPosition.First:
					// AS 3/18/05 NA 2005 Vol 2 - See above
					//this.MoveTab(tabToReposition, relativeTab.TabGroup, 0, true);
					this.MoveTab(tabToReposition, relativeTab.TabGroup, 0, invokeTabDisplayEvents, invokeTabMoveEvents);
					break;
				case MdiTabPosition.Last:
					// AS 3/18/05 NA 2005 Vol 2 - See above
					//this.MoveTab(tabToReposition, relativeTab.TabGroup, relativeTab.TabGroup.Tabs.Count, true);
					// MD 12/15/08 - TFS11576
					// When the tab being repositioned is already in the same tab group at the relative tab, it will be removed from the collection
					// and then inserted at the specified index. Therefore, the index will be too high and an index out of range exception will be 
					// thrown when trying to do the insert. In this case, decrement the index by one.
					//this.MoveTab(tabToReposition, relativeTab.TabGroup, relativeTab.TabGroup.Tabs.Count, invokeTabDisplayEvents, invokeTabMoveEvents);
					int index = relativeTab.TabGroup.Tabs.Count;

					if ( relativeTab.TabGroup.Tabs.Contains( tabToReposition ) )
						index--;

					this.MoveTab( tabToReposition, relativeTab.TabGroup, index, invokeTabDisplayEvents, invokeTabMoveEvents );
					break;
				case MdiTabPosition.Previous:
					if (tabToReposition == relativeTab)
						return;

					// if its a different tab group, move it at the point where the relative is
					if (relativeTab.TabGroup != tabToReposition.TabGroup)
						// AS 3/18/05 NA 2005 Vol 2 - See above
						//this.MoveTab(tabToReposition, relativeTab.TabGroup, ((ITabItem)relativeTab).Index, true);
						this.MoveTab(tabToReposition, relativeTab.TabGroup, ((ITabItem)relativeTab).Index, invokeTabDisplayEvents, invokeTabMoveEvents);
					else
					{
						int relativeIndex = ((ITabItem)relativeTab).Index;
						int tabIndex = ((ITabItem)tabToReposition).Index;

						int newTabIndex;

						
						if (tabIndex < relativeIndex)
							newTabIndex = relativeIndex - 1;
						else //if (tabIndex >= relativeIndex)
							newTabIndex = relativeIndex;

						// AS 3/18/05 NA 2005 Vol 2 - See above
						//this.MoveTab(tabToReposition, relativeTab.TabGroup, newTabIndex, true);
						this.MoveTab(tabToReposition, relativeTab.TabGroup, newTabIndex, invokeTabDisplayEvents, invokeTabMoveEvents);
					}
					break;
				case MdiTabPosition.Next:
					if (tabToReposition == relativeTab)
						return;

					// if its a different tab group, move it just after the relative
					if (relativeTab.TabGroup != tabToReposition.TabGroup)
						// AS 3/18/05 NA 2005 Vol 2 - See above
						//this.MoveTab(tabToReposition, relativeTab.TabGroup, ((ITabItem)relativeTab).Index + 1, true);
						this.MoveTab(tabToReposition, relativeTab.TabGroup, ((ITabItem)relativeTab).Index + 1, invokeTabDisplayEvents, invokeTabMoveEvents);
					else
					{
						int relativeIndex = ((ITabItem)relativeTab).Index;
						int tabIndex = ((ITabItem)tabToReposition).Index;

						int newTabIndex;

						
						if (tabIndex > relativeIndex)
							newTabIndex = relativeIndex + 1;
						else
							newTabIndex = relativeIndex;

						// AS 3/18/05 NA 2005 Vol 2 - See above
						//this.MoveTab(tabToReposition, relativeTab.TabGroup, newTabIndex, true);
						this.MoveTab(tabToReposition, relativeTab.TabGroup, newTabIndex, invokeTabDisplayEvents, invokeTabMoveEvents);
					}
					break;
			}
		}
		#endregion //RepositionTab

		#region GetTabGroupControl(MdiTabGroup mdiTabGroup)
		internal MdiTabGroupControl GetTabGroupControl(MdiTabGroup mdiTabGroup)
        {
			return this.TabGroupControls[mdiTabGroup] as MdiTabGroupControl;			
		}
		#endregion //GetTabGroupControl(MdiTabGroup mdiTabGroup)

		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		#region GetTabGroupIndex
		internal int GetTabGroupIndex(MdiTab tab, MdiTabGroup parentTabGroupToFind)
		{
			MdiTabGroup previousGroup = tab == null ? null : tab.TabGroup;
			MdiTabGroup currentGroup = previousGroup == null ? null : previousGroup.Parent;

			while (currentGroup != parentTabGroupToFind)
			{
				previousGroup = currentGroup;
				currentGroup = currentGroup.Parent;
			}

			if (currentGroup != parentTabGroupToFind)
				return -1;

			return previousGroup.Index;
		}
		#endregion //GetTabGroupIndex

		#region CalculateCommonExtent
		internal int CalculateCommonExtent( MdiTabGroup parentTabGroup )
		{
			Debug.Assert(parentTabGroup == null || parentTabGroup.HasTabGroups, "Trying to determine the common extent of a leaf tabgroup!");

			// when null, the common extent for the root groups is requested
			// otherwise, its the common (i.e. non sizable) extent for the
			// specified group that is requested
			if (parentTabGroup == null)
				return this.CommonMdiClientExtent;
			else
				return Math.Max(0, parentTabGroup.ClientExtent);
		}
		#endregion //CalculateCommonExtent

		#region CalculateSizableExtent
		internal int CalculateSizableExtent( MdiTabGroup parentTabGroup )
		{
			if (parentTabGroup == null)
				return this.AvailableMdiClientExtent;
			else if (parentTabGroup.Parent == null)
				return this.CommonMdiClientExtent;
			else
				return parentTabGroup.Parent.ClientExtent;
		}
		#endregion //CalculateSizableExtent

		#region GetFirstLeafTabGroup
		internal static MdiTabGroup GetFirstLeafTabGroup(MdiTabGroupsCollection tabGroups)
		{
			if (tabGroups.Count == 0)
				return null;

			MdiTabGroup tabGroup = tabGroups[0];

			return !tabGroup.HasTabGroups ? tabGroup : GetFirstLeafTabGroup(tabGroup.TabGroups);
		}
		#endregion //GetFirstLeafTabGroup

		#region GetLastLeafTabGroup
		internal static MdiTabGroup GetLastLeafTabGroup(MdiTabGroupsCollection tabGroups)
		{
			if (tabGroups.Count == 0)
				return null;

			MdiTabGroup tabGroup = tabGroups[tabGroups.Count - 1];

			return !tabGroup.HasTabGroups ? tabGroup : GetLastLeafTabGroup(tabGroup.TabGroups);
		}
		#endregion //GetLastLeafTabGroup

		#region CalculateMaxTabGroupRows
		internal int CalculateMaxTabGroupRows()
		{
			return this.CalculateTabGroupRowsColsHelper(null, Orientation.Horizontal);
		}
		#endregion //CalculateMaxTabGroupRows

		#region CalculateMaxTabGroupCols
		internal int CalculateMaxTabGroupCols()
		{
			return this.CalculateTabGroupRowsColsHelper(null, Orientation.Vertical);
		}
		#endregion //CalculateMaxTabGroupCols

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		#region VerifyMaximizedTabGroup
		private void VerifyMaximizedTabGroup()
		{
			// If the active tab is in a different group then maximize that group instead.
			if (this.IsActiveTabGroupMaximized)
				this.MaximizeTabGroup(this.lastActivatedTab == null ? null : this.lastActivatedTab.TabGroup, true);
			else
				this.MaximizeTabGroup(null, true);
		}

		#endregion //VerifyMaximizedTabGroup

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		#region MaximizeTabGroup
		private void MaximizeTabGroup(MdiTabGroup tabGroupToMaximize, bool cachePreMaximizedExtent)
		{
			if (tabGroupToMaximize == this.maximizedTabGroup)
				return;

			// when loading from a layout, wait until the end
			if (this.IsInitializing || this.IsRestoringTabs || this.IsRestoringTabGroup)
				return;

			MdiTabGroup oldValue = this.maximizedTabGroup;
			MdiTabGroup newValue = tabGroupToMaximize;

			this.SuspendLayout();

			try
			{
				// if we're unmaximizing, then restore the premaximized extents
				if (newValue == null)
				{
					foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
						tabGroup.InitializeExtent(tabGroup.PreMaximizedExtent);
				}
				else
				{
					// when starting from scratch, we need to catch the 
					// pre maximized extent
					if (oldValue == null && cachePreMaximizedExtent)
					{
						// make sure the extents are all allocated before we cache them
						this.VerifyTabGroupExtents();

						// now save them so we can restore things later
						foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
							tabGroup.CachePreMaximizedExtent();
					}
				}

				// store the new maximized group...
				this.maximizedTabGroup = tabGroupToMaximize;

				if (newValue != null)
				{
					// now change the extents of all minimized groups
					// to a value that will require reallocation
					foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
						tabGroup.InitializeExtent(-1);
				}

				this.DirtyTabGroups(TabbedMdiPropertyIds.Extent);

				// ensure the splitter properties are properly set
				this.SynchronizeSplitterProperties();
			}
			finally
			{
				// reallocate the extents so the maximized groups
				// get the space used by the other now minimized groups
				this.ResumeLayout(true);
			}
		}
		#endregion //MaximizeTabGroup

		// AS 3/25/05
		#region OnDragTab(Start|End)
		internal void OnDragTabStart()
		{
			this.isDraggingTab = true;
		}

		internal void OnDragTabEnd()
		{
			this.isDraggingTab = false;
		}
		#endregion //OnDragTab(Start|End)

		#endregion // TabGroup/Tab related

		#region Miscellaneous

		#region CreateTabMetricsFont
		internal Font CreateTabMetricsFont(MdiTabGroup tabGroup)
		{
			Font baseFont = this.GetBaseFont(tabGroup);
			
			MdiTabSettings groupSettings = tabGroup.HasTabSettings ? tabGroup.TabSettings : null;
			MdiTabSettings managerSettings = this.HasTabSettings ? this.TabSettings : null;

			// JDN 8/17/04 NAS2004 Vol3 - VisualStudio2005 style
			// This appears to be a bad assumption
			//if (groupSettings == null && managerSettings == null)
				//return baseFont.Clone() as Font;

			AppearanceData[] appearances = new AppearanceData[6];
			appearances[0] = MdiTab.ResolveTabFontAppearance(tabGroup, TabState.Selected | TabState.Hot | TabState.Active, groupSettings, managerSettings);
			appearances[1] = MdiTab.ResolveTabFontAppearance(tabGroup, TabState.Selected | TabState.Active, groupSettings, managerSettings);

			// not needed since an active tab must be selected
			//AppearanceData activeAppearance = MdiTab.ResolveTabFontAppearance(tabGroup, TabState.Normal, true, groupSettings, managerSettings);
			//AppearanceData activeHotAppearance = MdiTab.ResolveTabFontAppearance(tabGroup, TabState.Hot, true, groupSettings, managerSettings);

			appearances[2] = MdiTab.ResolveTabFontAppearance(tabGroup, TabState.Selected | TabState.Hot, groupSettings, managerSettings);
			appearances[3] = MdiTab.ResolveTabFontAppearance(tabGroup, TabState.Selected, groupSettings, managerSettings);
			appearances[4] = MdiTab.ResolveTabFontAppearance(tabGroup, TabState.Hot, groupSettings, managerSettings);
			appearances[5] = MdiTab.ResolveTabFontAppearance(tabGroup, TabState.Selected, groupSettings, managerSettings);

			// SSP 11/4/05 BR07555
			// Added TextRenderingMode property to UltraControlBase and UltraComponentBase. 
			// We need to pass in the gdi draw string flags according to that property setting.
			// Also Moved the CreateLargestFont to the Win's Utilities. This is being used from 
			// Misc as well.
			// 
			//return Utilities.CreateLargestFont(appearances, baseFont);
			return Infragistics.Win.Utilities.CreateLargestFont( appearances, baseFont
				, DrawUtility.GetGdiDrawStringFlags( this )
				);
		}
		#endregion //CreateTabMetricsFont

		#region GetUIElement
		internal UIElement GetUIElement( MdiTab tab )
		{
			if (tab == null)
				return null;

			MdiTabGroup tabGroup = tab.TabGroup;

			// if the tab is not yet associated with a tab group or
			// the tab group does not yet have a TabManager, return null
			if (tabGroup == null || !tabGroup.HasTabManager)
				return null;

			// otherwise get the element from the tabmanager
			return tabGroup.TabManager.GetUIElement(tab);
		}
		#endregion //GetUIElement

		#region ProcessMnemonic (char, MdiTabGroup)
		internal bool ProcessMnemonic( char character, MdiTabGroup tabGroup )
		{
			// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.TabGroups.Count == 0 || tabGroup != this.TabGroups[0])
			if (!this.HasTabGroups || tabGroup != UltraTabbedMdiManager.GetFirstLeafTabGroup(this.TabGroups))
				return false;

			// start with the tab after the active tab
			MdiTab activeTab = this.ActiveTab;

			// if there is no active tab, return false
			if (activeTab == null)
				return false;

			MdiTabGroup activeTabGroup = activeTab.TabGroup;
			// AS 6/3/03
			// Use the new Index property
			//int activeTabGroupIndex = this.TabGroups.IndexOf(activeTabGroup);
			// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
			// Don't need this anymore.
			//
			//int activeTabGroupIndex = activeTabGroup.Index;
			int activeTabIndex = ((ITabItem)activeTab).Index + 1;

			int startTabIndex = activeTabIndex;

			// start with the activetab's group and move forward
			// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
			//for(int groupIndex = activeTabGroupIndex; groupIndex < this.TabGroups.Count; groupIndex++)
			//{
			//	MdiTabGroup currentTabGroup = this.TabGroups[groupIndex];
			MdiTabGroup currentTabGroup = activeTabGroup;

			// first pass is from the active tab group to the last group forward
			while(currentTabGroup != null)
			{
				for(int tabIndex = startTabIndex; tabIndex < currentTabGroup.Tabs.Count; tabIndex++)
				{
					if (this.ProcessMnemonic(character, currentTabGroup.Tabs[tabIndex]))
						return true;
				}

				startTabIndex = 0;

				// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
				currentTabGroup = currentTabGroup.NextLeafTabGroup;
			}

			// if nothing was found, then start from the first tab group
			// and move forward
			// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
			//if (activeTabGroupIndex > 0)
			//{
			//	for(int groupIndex = 0; groupIndex < activeTabGroupIndex; groupIndex++)
			//	{
			//		MdiTabGroup currentTabGroup = this.TabGroups[groupIndex];
			// now start with the first leaf tabgroup
			currentTabGroup = UltraTabbedMdiManager.GetFirstLeafTabGroup(this.TabGroups);

			// go forward until we hit the active tab group
			while (currentTabGroup != activeTabGroup)
			{
					for(int tabIndex = 0; tabIndex < currentTabGroup.Tabs.Count; tabIndex++)
					{
						if (this.ProcessMnemonic(character, currentTabGroup.Tabs[tabIndex]))
							return true;
					}
				// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
				//}
				currentTabGroup = currentTabGroup.NextLeafTabGroup;
			}

			// lastly start with the beginning of the active tab group
			for(int tabIndex = 0; tabIndex < activeTabIndex; tabIndex++)
			{
				if (this.ProcessMnemonic(character, activeTabGroup.Tabs[tabIndex]))
					return true;
			}

			return false;
		}
		#endregion //ProcessMnemonic (char, MdiTabGroup)

		#region ProcessMnemonic (char,MdiTab)
		private bool ProcessMnemonic(char character, MdiTab tab)
		{
			// skip disabled or invisible tabs
			if (!tab.IsFormEnabled || !tab.IsFormVisible)
				return false;
			
			// AS 6/9/03
			// We have to actually do something with the tab
			// if it is its mnemonic.
			//
			// get the text displayed by the tab
			//return Control.IsMnemonic(character, tab.TextResolved);
			if (!Control.IsMnemonic(character, tab.TextResolved))
				return false;

			tab.Activate();
			return true;
		}
		#endregion //ProcessMnemonic (char,MdiTab)

		#region PointToClient
		internal Point PointToClient( MdiTabGroup tabGroup, Point screenPt )
		{
			MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;

			if (tabGroupControl == null)
				return Point.Empty;

			return tabGroupControl.PointToClient(screenPt);
		}
		#endregion //PointToClient

		#region PointToScreen
		internal Point PointToScreen( MdiTabGroup tabGroup, Point clientPt )
		{
			MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;

			if (tabGroupControl == null)
				return Point.Empty;

			return tabGroupControl.PointToScreen(clientPt);
		}
		#endregion //PointToScreen

		#region RectangleToClient
		internal Rectangle RectangleToClient( MdiTabGroup tabGroup, Rectangle screenRect )
		{
			MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;

			if (tabGroupControl == null)
				return Rectangle.Empty;

			return tabGroupControl.RectangleToClient(screenRect);
		}
		#endregion //RectangleToClient

		#region RectangleToScreen
		internal Rectangle RectangleToScreen( MdiTabGroup tabGroup, Rectangle clientRect )
		{
			MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;

			if (tabGroupControl == null)
				return Rectangle.Empty;

			return tabGroupControl.RectangleToScreen(clientRect);
		}
		#endregion //RectangleToScreen

		#region ShowDragDropContextMenu
		internal void ShowDragDropContextMenu( MdiTab tab, Point screenPt )
		{
			// initialize the context menu contents
			this.InitializeTabContextMenu(tab, MdiTabContextMenu.Reposition);

			// store the tab that we are displaying the context menu for
			this.tabForContextMenu = tab;

			MdiTabGroup tabGroup = tab.TabGroup;

			// allow the programmer to modify the context menu
			MdiTabContextMenuEventArgs args = new MdiTabContextMenuEventArgs(this, tab, this.TabContextMenu, MdiTabContextMenu.Reposition);

			this.InvokeEvent(TabbedMdiEventIds.InitializeContextMenu, args);
			
			// if its cancelled, clear the menu so it doesn't get shown
			if (args.Cancel)
				this.TabContextMenu.MenuItems.Clear();
			else
			{
				Control tabGroupControl = this.TabGroupControls[tabGroup] as Control;

				Debug.Assert(tabGroupControl != null, "No MdiTabGroupControl was associated with the specified 'MdiTabGroup'");

				if (tabGroupControl == null)
					this.TabContextMenu.MenuItems.Clear();
				else
				{
					Point clientPt = tabGroupControl.PointToClient(screenPt);

					this.ignoreContextMenuPopup = true;

					try
					{
						this.TabContextMenu.Show(tabGroupControl, clientPt);
					}
					finally
					{
						this.ignoreContextMenuPopup = false;
					}
				}
			}
		}
		#endregion //ShowDragDropContextMenu

		// AS 4/28/03 WTB873
		#region ResetFormPosition
		internal void ResetFormPosition( MdiTab tab )
		{
			// AS 9/18/03 WTB1118
			// Moved from below
			Form form = tab.Form;

			// AS 9/18/03 WTB1118
			// As long as the form has been initialized to
			// be an mdi tab, keep its window state as 
			// normal.
			if (tab.IsFormInitialized	&&
				form != null			&& 
				form.WindowState != FormWindowState.Normal)
			{
				form.WindowState = FormWindowState.Normal;
			}

			// if we're:
			// (a) positioning the controls or
			// (b) the layout is suspended or 
			// (c) we're within a BeginUpdate/EndUpdate or
			// (d) the component is not enabled
			// =================
			// don't do anything
			if (this.PositioningControls	|| 
				this.IsLayoutSuspended		|| 
				this.IsUpdating				||
				!this.Enabled)
				return;

			// AS 9/18/03 WTB1118
			// Moved up
			// Form form = tab.Form;

			// if the form is disposed or being disposed, don't do anything
			if (form == null || form.IsDisposed || form.Disposing)
				return;

			MdiTabGroup tabGroup = tab.TabGroup;

			// if its not part of a tab group yet, bail
			if (tabGroup == null)
				return;

			// get the position for the associated forms
			Rectangle formRect = this.TabGroupManager.GetFormPosition(tabGroup);

			// position the form
			form.Bounds = formRect;
		}
		#endregion //ResetFormPosition

		// AS 5/29/03 WTB1012
		// Try to prevent flicker when showing a hidden mdi child form. 
		// For some reason, the mdi child handle is created and destroyed
		// several times and if we don't turn off mdiclient redrawing, you'll
		// see the title bar of the form appear and disappear.
		//
		#region MdiClientCreateNotification
		internal void MdiClientCreateNotification()
		{
			if (this.WaitingForDelayedRedraw)
				return;

			// temporarily disable redrawing in the mdi client
			this.DisableMdiClientRedraw();

			// set up for a delayed re-enabling of the mdi client redrawing
			this.DelayedRedraw();
		}
		#endregion //MdiClientCreateNotification

		// AS 4/27/06 AppStyling
		#region AppStyling
		internal void OnAppStyleChanged()
		{
			StyleUtils.DirtyCachedPropertyVals(this);

			// dirty the tabs...
			this.DirtyTabGroups(
				MdiTabGroup.TabManagerChangeType.AllMetrics |
				MdiTabGroup.TabManagerChangeType.Font |
				MdiTabGroup.TabManagerChangeType.Invalidate);

			this.SynchronizeSplitterProperties();

			// invalidate the controls
			this.InvalidateControls(true, true, true, true);

			// dirty the tab group manager sizes
			this.TabGroupManager.DirtyAllTabGroups();

			// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
			this.RefreshSplitterIntersections();

			// then force a reposition
			this.PositionAndSizeTabGroups();
		}
		#endregion //AppStyling

		#endregion //Miscellaneous

		#endregion // Internal

		#region Private

		#region DirtyTabGroups

		#region DirtyTabGroups (TabbedMdiPropertyIds)
		private void DirtyTabGroups( TabbedMdiPropertyIds property )
		{
			if (!this.HasTabGroups)
				return;

			this.DirtyTabGroups( MdiTabGroup.GetTabChangeType(property) );
		}
		#endregion //DirtyTabGroups (TabbedMdiPropertyIds)

		#region DirtyTabGroups (TabbedMdiPropertyIds,PropChangeInfo)
		private void DirtyTabGroups( TabbedMdiPropertyIds property, PropChangeInfo propChangeInfo )
		{
			if (!this.HasTabGroups)
				return;

			this.DirtyTabGroups( MdiTabGroup.GetTabChangeType(property, propChangeInfo) );
		}
		#endregion //DirtyTabGroups (TabbedMdiPropertyIds,PropChangeInfo)

		#region DirtyTabGroups (MdiTabGroup.TabManagerChangeType)
		private void DirtyTabGroups( MdiTabGroup.TabManagerChangeType changeType )
		{
			if (changeType == MdiTabGroup.TabManagerChangeType.None)
				return;

			if (this.IsInitializing )
				return;

			// suspend the layout so only reposition the groups once
			bool needsSuspend = !this.IsLayoutSuspended;

			this.SuspendLayout();

			try
			{
				// have each tab group dirty its tabmanager
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup tabGroup in this.TabGroups)
				foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
					tabGroup.DirtyTabManager(changeType);
			}
			finally
			{
				// assuming all the metrics have changed, we should update the position
				// of the tab groups
				bool processLayout = (changeType & MdiTabGroup.TabManagerChangeType.AllMetrics) != 0 ||
					(changeType & MdiTabGroup.TabManagerChangeType.Collection) != 0;

				this.ResumeLayout(processLayout);
			}
		}
		#endregion //DirtyTabGroups (MdiTabGroup.TabManagerChangeType)

		#endregion //DirtyTabGroups

		#region Load/Save related

		#region Save
		private void Save( Stream stream, bool xml )
		{
			// make sure we have a stream
			if (stream == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_39"), Shared.SR.GetString(null, "LE_V2_Exception_40"));

			// AS 4/14/03 WTB800
			if (this.HasMdiTabs)
			{
				StoreTabEventArgs args = new StoreTabEventArgs(null);

				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup tabGroup in this.TabGroups)
				foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
				{
					foreach(MdiTab tab in tabGroup.Tabs)
					{
						args.Initialize(tab);
						this.InvokeEvent(TabbedMdiEventIds.StoreTab, args);
					}
				}

				foreach(MdiTab hiddenTab in this.HiddenTabs)
				{
					args.Initialize(hiddenTab);
					this.InvokeEvent(TabbedMdiEventIds.StoreTab, args);
				}
			}

			// create a formatter
			IFormatter formatter = this.CreateFormatter(xml);

			// create the object containing the tabbed mdi properties
			ObjectStreamer streamer = this.CreateObjectStreamer();

			// serialize it
			formatter.Serialize(stream, streamer);
		}
		#endregion //Save

		#region CreateFormatter
		private IFormatter CreateFormatter( bool xml )
		{
			if (xml)
			{
				SoapFormatter soapFormatter = new SoapFormatter();

				soapFormatter.AssemblyFormat = FormatterAssemblyStyle.Simple;

				return soapFormatter;
			}
			else
			{
				BinaryFormatter binaryFormatter = new BinaryFormatter();

				binaryFormatter.AssemblyFormat = FormatterAssemblyStyle.Simple;

				return binaryFormatter;
			}
		}
		#endregion //CreateFormatter

		#region Load
		private void Load( Stream stream, bool xml )
		{
			if (stream == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_39"), Shared.SR.GetString(null, "LE_V2_Exception_40"));

			// create the appropriate formatter
			IFormatter formatter = this.CreateFormatter(xml);

			// create a binder to hook up the simple serialized objects
			formatter.Binder = new Infragistics.Win.UltraWinTabbedMdi.Serialization.Binder();
			
			// deserialize the object streamer
			ObjectStreamer streamer = formatter.Deserialize(stream) as ObjectStreamer;

			try
			{
				if (this.MdiParent != null)
					this.MdiParent.SuspendLayout();

				if (this.mdiClient != null)
					this.mdiClient.SuspendLayout();

				this.SuspendLayout();

				this.LoadHelper(streamer);
			}
			finally
			{
				if (this.MdiParent != null)
					this.MdiParent.ResumeLayout(false);

				if (this.mdiClient != null)
					this.mdiClient.ResumeLayout(false);

				this.ResumeLayout(true);

			}
		}
		#endregion //Load

		#region LoadHelper
		private void LoadHelper( ObjectStreamer streamer  )
		{
			try
			{
				// release all the current mdi tabs and tab groups
				this.ReleaseMdiTabsAndForms();

				// keep track that we are initializing
				this.isInitializing = true;

				// initialize the props
				this.InitializeFrom( streamer );
			}
			finally
			{
				// end init
				this.isInitializing = false;
			}

			// AS 12/8/06 BR18188
			StyleUtils.DirtyCachedPropertyVals(this);

			// dirty the cached settings
			this.TabGroupManager.DirtyAllTabGroups();
			this.lastActiveTabGroup = null;
			this.cachedActiveTab = null;

			// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
			this.lastActivatedTab = null;

			// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
			this.maximizedTabGroup = null;

			if (this.MdiParent == null)
			{
				// if we are not associated with an mdi parent,
				// release any tabs and tab groups
				this.ReleaseMdiTabsAndForms();
			}
			else
			{
				this.InitializeDeserializedTabs();
			}

			// AS 4/29/03 WTB880
			if (this.IsMdiClientSubclassed)
			{
				this.UpdateBorderStyle();

				// AS 5/1/03 WTB880
				if (this.mdiClientSubclasser != null)
					this.mdiClientSubclasser.BorderColorChanged();
			}
		}
		#endregion //LoadHelper

		#region InitializeDeserializedTabs
		private void InitializeDeserializedTabs()
		{
			try
			{
				// if we have tabs, we need to initialize and hook them up
				if (this.HasMdiTabs)
				{
					// first iterate over the tabs and fire the restoretab event
					#region Fire RestoreTab for all Tabs

					// AS 4/14/03
					// Do this for the hidden tabs first since 
					// creating them last could mess up the
					// active mdi child order.
					//
					if (this.HasHiddenTabs)
						this.RestoreTabs(this.HiddenTabs);
				
					// AS 4/14/03 WTB801
					// Use a separate method that can store the
					// serialized selected tab and first displayed tab
					// and restore them after restoring the tabs.
					//
					//foreach(MdiTabGroup tabGroup in this.TabGroups)
					//	this.RestoreTabs(tabGroup.Tabs);
					this.RestoreTabGroups();

					#endregion //Fire RestoreTab for all Tabs

					// then remove any tabs not associated with a form
					#region Remove Unassociated Tabs
					if (this.HasTabGroups)
					{
						// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
						//foreach(MdiTabGroup tabGroup in this.TabGroups)
						foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
						{
							for(int i = tabGroup.Tabs.Count - 1; i >= 0; i--)
							{
								if (tabGroup.Tabs[i].Form == null)
								{
									// AS 6/12/03
									// Reset the owner.
									//
									tabGroup.Tabs[i].InitializeOwner(null);

									tabGroup.Tabs.RemoveAt(i);
								}
							}
						}
					}

					if (this.HasHiddenTabs)
					{
						for(int i = this.HiddenTabs.Count - 1; i >= 0; i--)
						{
							if (this.HiddenTabs[i].Form == null)
							{
								// AS 6/12/03
								// Reset the owner.
								//
								this.HiddenTabs[i].InitializeOwner(null);

								this.HiddenTabs.RemoveAt(i);
							}
						}
					}
					#endregion //Remove Unassociated Tabs

					// move any tabs for visible forms from hidden tabs to 
					// a tab group and any hidden forms in tab groups to hidden tabs
					//
					#region Verify Tab Collections
					if (this.HasTabGroups)
					{
						// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
						//foreach(MdiTabGroup tabGroup in this.TabGroups)
						foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
						{
							for(int i = tabGroup.Tabs.Count - 1; i >= 0; i--)
							{
								MdiTab tab = tabGroup.Tabs[i];

								if (!tab.IsFormVisible)
								{
									tabGroup.Tabs.RemoveAt(i);

									this.HiddenTabs.Add(tab);

									// AS 6/11/03
									// The tab needs to be associated with the
									// tab manager not the tab group since it is
									// no longer part of the tab group.
									//
									tab.InitializeOwner(this);
								}
									
							}
						}
					}

					if (this.HasHiddenTabs)
					{
						for(int i = this.HiddenTabs.Count - 1; i >= 0; i--)
						{
							MdiTab tab = this.HiddenTabs[i];

							if (tab.IsFormVisible)
							{
								//this.HiddenTabs.RemoveAt(i);
								this.AddTabToTabGroup(tab);
							}
						}
					}
					#endregion // Verify Tab Collections

					// remove any tab groups that do not have tabs
					this.RemoveEmptyTabGroups();
				}

				// then create new tabs for all forms not associated with an mditab
				#region Create Tabs for Unassociated Forms
				if (this.MdiParent.MdiChildren.Length > 0)
				{
					ArrayList list = new ArrayList();

					// check the mdi children to see if any don't have a tab associated with it
					foreach(Form form in this.MdiParent.MdiChildren)
					{
						if (this.GetTabFromForm(form, false, false, false) == null)
							list.Add(form);
					}

					// if there were unaccounted forms...
					if (list.Count > 0)
					{
						// create tabs for these forms...
						this.CreateTabsForForms( (Form[])list.ToArray(typeof(Form)) );
					}
				}
				#endregion //Create Tabs for Unassociated Forms
			}
			finally
			{
				// update the cache tabgroup and tab info
				this.CacheLastActiveTabGroup();
				this.cachedActiveTab = this.ActiveTab;

				// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
				this.lastActivatedTab = this.cachedActiveTab;

				// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
				if (this.IsActiveTabGroupMaximized)
					this.MaximizeTabGroup(this.lastActivatedTab == null ? null : this.lastActivatedTab.TabGroup, false);

				// AS 11/24/03
				// Be sure to dirty all the metrics.
				//
				this.DirtyTabGroups(MdiTabGroup.TabManagerChangeType.AllMetrics);

				this.InitializeTabGroupExtents(true);

				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup tabGroup in this.TabGroups)
				foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
					this.EnsureTabIsSelected(tabGroup, false);
			}
		}
		#endregion //InitializeDeserializedTabs

		// AS 4/14/03 WTB801
		#region RestoreTabGroups
		private void RestoreTabGroups()
		{
			if (this.TabGroups.Count == 0)
				return;

			
			this.RestoreTabGroups(this.TabGroups, this.serializedActiveTabGroupIndex);
		}

		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		// Created helper method since we need to restore the active tab group
		// last regardless of the level.
		//
		private void RestoreTabGroups( MdiTabGroupsCollection tabGroups, int serializedActiveTabGroupIndex )
		{
			// get the tab group index that contained the active tab
			int lastTabGroup = serializedActiveTabGroupIndex;

			int count = tabGroups.Count;

			// if the info was somehow corrupted, skip it
			if (lastTabGroup < 0 || lastTabGroup >= count)
				lastTabGroup = -1;

			for (int i = 0; i < count; i++)
			{
				if (i == lastTabGroup)
					continue;

				this.RestoreTabGroup( tabGroups[i] );
			}

			// we want to restore this tab group first since
			// we want to activate this tab group's tab last
			// so it is the active mdi tab
			if (lastTabGroup >= 0 && lastTabGroup < tabGroups.Count)
				this.RestoreTabGroup(tabGroups[lastTabGroup]);
		}
		#endregion //RestoreTabGroups

		// AS 4/14/03 WTB801
		#region RestoreTabGroup
		private void RestoreTabGroup( MdiTabGroup tabGroup )
		{
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			// If a tab group has nested tab group, we just 
			// need to process those and leave.
			//
			if (tabGroup.HasTabGroups)
			{
				this.RestoreTabGroups(tabGroup.TabGroups, tabGroup.SerializedActiveTabGroupIndex);
				return;
			}

			// AS 6/11/03
			// We need a flag since we don't want to create a tab
			// for a form if it does not yet exist.
			//
			this.restoringTabGroup = true;

			try
			{
				// store the serialized first displayed tab
				// and selected tab
				MdiTab selectedTab = tabGroup.SerializedSelectedTab;
				MdiTab firstTab = tabGroup.SerializedFirstDisplayedTab;

				this.RestoreTabs(tabGroup.Tabs);

				// restore the first displayed tab
				if (firstTab != null				&& 
					firstTab.Form != null			&&
					((ITabItem)firstTab).Visible	&&
					tabGroup.Tabs.Contains(firstTab))
					tabGroup.FirstDisplayedTab = firstTab;

				// activate the selected tab
				if (selectedTab != null				&& 
					selectedTab.Form != null		&&
					((ITabItem)selectedTab).Visible	&&
					tabGroup.Tabs.Contains(selectedTab))
					selectedTab.Activate();
			}
			finally
			{
				// AS 6/11/03
				// We need a flag since we don't want to create a tab
				// for a form if it does not yet exist.
				//
				this.restoringTabGroup = false;
			}
		}
		#endregion //RestoreTabGroup

		#region InitializeFrom
		private void InitializeFrom( ObjectStreamer streamer )
		{
			if (streamer.AssemblyName == null || streamer.AssemblyName.IndexOf("Infragistics.Win.UltraWinTabbedMdi") < 0)
				throw new InvalidCastException(Shared.SR.GetString(null, "LE_V2_Exception_41"));

			// first reset to the default values
			this.ResetComponent(true);

			foreach(System.Collections.DictionaryEntry dictionaryEntry in streamer.Dictionary)
			{
				if ( !(dictionaryEntry.Value is SerializationEntry) )
					continue;

				SerializationEntry entry = (SerializationEntry)dictionaryEntry.Value;

				switch(entry.Name)
				{
					case "AllowHorizontalTabGroups":
						this.allowHorizontalTabGroups = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.allowHorizontalTabGroups);
						break;
				
					case "AllowVerticalTabGroups":
						this.allowVerticalTabGroups = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.allowVerticalTabGroups);
						break;
				
					case "Appearance":
					{
						// it should be safe to use the InitializeFrom since we're not in the
						// deserialization constructor - the object value has already been fully
						// deserialized.
						AppearanceHolder holder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.appearanceHolder);

						if (this.appearanceHolder != null)
							this.appearanceHolder.InitializeFrom(holder);
						else
						{
							this.appearanceHolder = holder;

							if (this.appearanceHolder != null)
								this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
						}
						break;
					}
					case "SplitterAppearance":
					{
						// it should be safe to use the InitializeFrom since we're not in the
						// deserialization constructor - the object value has already been fully
						// deserialized.
						AppearanceHolder holder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.appearanceHolder);

						if (this.splitterAppearanceHolder != null)
							this.splitterAppearanceHolder.InitializeFrom(holder);
						else
						{
							this.splitterAppearanceHolder = holder;

							if (this.splitterAppearanceHolder != null)
								this.splitterAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
						}
						break;
					}
					case "Appearances":
					{
						// we need to unhook from an existing one if we have one
						if (this.appearances != null)
						{
							this.appearances.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
							this.appearances = null;
						}
						
						this.appearances = (AppearancesCollection)Utils.DeserializeProperty(entry, typeof(AppearancesCollection), this.appearances);

						// assuming it is non-null, initialize the imagelist provider and hook into the notifications
						if (this.appearances != null)
						{
							this.appearances.Initialize(this);
							this.appearances.SubObjectPropChanged += this.SubObjectPropChangeHandler;
						}
						break;
					}
					case "TabSettings":
					{
						MdiTabSettings settings = (MdiTabSettings)Utils.DeserializeProperty(entry, typeof(MdiTabSettings), this.tabSettings);

						// if we don't have on, take this one
						if (this.tabSettings == null)
						{
							this.tabSettings = settings;

							// be sure to hook the property change notifications
							if (this.tabSettings != null)
							{
								this.tabSettings.InitializeOwner(this);
								this.tabSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
							}
						}
						else // otherwise just initialize the one we are hooked into
							this.tabSettings.InitializeFrom(settings);
						break;
					}
					case "TabGroupSettings":
					{
						MdiTabGroupSettings settings = (MdiTabGroupSettings)Utils.DeserializeProperty(entry, typeof(MdiTabGroupSettings), this.tabGroupSettings);

						// if we don't have on, take this one
						if (this.tabGroupSettings == null)
						{
							this.tabGroupSettings = settings;

							// be sure to hook the property change notifications
							if (this.tabGroupSettings != null)
							{
								this.tabGroupSettings.InitializeOwner(this);
								this.tabGroupSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
							}
						}
						else // otherwise just initialize the one we are hooked into
							this.tabGroupSettings.InitializeFrom(settings);
						break;
					}
					case "TabGroups":
					{
						// unhook from an existing one
						if (this.tabGroups != null)
						{
							Debug.Assert( this.tabGroups.Count == 0, "The 'TabGroups' collection already contains tabs");

							this.tabGroups.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
							this.tabGroups = null;
						}

						this.tabGroups = (MdiTabGroupsCollection)Utils.DeserializeProperty(entry, typeof(MdiTabGroupsCollection), this.tabGroups);

						// hook into the new one
						if (this.tabGroups != null)
						{
							this.tabGroups.SubObjectPropChanged += this.SubObjectPropChangeHandler;

							foreach(MdiTabGroup tabGroup in this.tabGroups)
								tabGroup.InitializeTabbedMdiManager(this);
						}

						break;
					}
					case "ShowToolTips":
						this.showToolTips = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.showToolTips);
						break;
				
					case "UseMnemonics":
						this.useMnemonics = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.useMnemonics);
						break;
				
					case "Orientation":
						this.orientation = (Orientation)Utils.DeserializeProperty(entry, typeof(Orientation), this.orientation);
						break;

					case "SplitterBorderStyle":
						this.splitterBorderStyle = (UIElementBorderStyle)Utils.DeserializeProperty(entry, typeof(UIElementBorderStyle), this.splitterBorderStyle);
						break;
				
					case "MaxTabGroups":
						this.maxTabGroups = (int)Utils.DeserializeProperty(entry, typeof(int), this.maxTabGroups);
						break;
				
					case "ImageSize":
						this.imageSize = (Size)Utils.DeserializeProperty(entry, typeof(Size), this.imageSize);
						break;
				
					case "ImageTransparentColor":
						this.imageTransparentColor = (Color)Utils.DeserializeProperty(entry, typeof(Color), this.imageTransparentColor);
						break;

					case "AlphaBlendMode":
						this.AlphaBlendMode = (AlphaBlendMode)Utils.DeserializeProperty(entry, typeof(AlphaBlendMode), this.AlphaBlendMode);
						break;
				
					case "Enabled":
						this.Enabled = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.Enabled);
						break;
				
					case "FlatMode":
						// AS 5/2/06 AppStyling
						//this.FlatMode = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.FlatMode);
						this.UseFlatMode = true == (bool)Utils.DeserializeProperty(entry, typeof(bool), false)
							? DefaultableBoolean.True
							: DefaultableBoolean.Default;
						break;
				
					case "SupportThemes":
						// AS 5/2/06 AppStyling
						//this.SupportThemes = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.SupportThemes);
						this.UseOsThemes = false == (bool)Utils.DeserializeProperty(entry, typeof(bool), true)
							? DefaultableBoolean.False
							: DefaultableBoolean.Default;
						break;

					case "UseFlatMode":
						this.UseFlatMode = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.UseFlatMode);
						break;
				
					case "UseOsThemes":
						this.UseOsThemes = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.UseOsThemes);
						break;

					case "HiddenTabs":
					{
						// unhook from an existing one
						if (this.hiddenTabs != null)
						{
							this.hiddenTabs.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
							this.hiddenTabs = null;
						}

						this.hiddenTabs = (HiddenMdiTabsCollection)Utils.DeserializeProperty(entry, typeof(HiddenMdiTabsCollection), this.hiddenTabs);

						// hook into the new one
						if (this.hiddenTabs != null)
						{
							this.hiddenTabs.SubObjectPropChanged += this.SubObjectPropChangeHandler;

							foreach(MdiTab tab in this.hiddenTabs)
								tab.InitializeOwner(this);
						}

						break;
					}
					case "SplitterWidth":
						this.splitterWidth = (int)Utils.DeserializeProperty(entry, typeof(int), this.SplitterWidth);
						break;

						// AS 4/14/03 WTB801
					case "ActiveTabGroupIndex":
						// use -1 as the default since we could load a layout that did not serialize
						// the active tab, in which case we don't want to muck with the active tab
						//
						// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
						//this.serializedActiveTabGroupIndex = (int)Utils.DeserializeProperty(entry, typeof(int), -1);
						this.serializedActiveTabGroupIndex = (int)Utils.DeserializeProperty(entry, typeof(int), NoActiveTabGroupIndex);
						break;

						// AS 4/29/03 WTB880
					case "BorderStyle":
					{
						this.borderStyle = (MdiClientBorderStyle)Utils.DeserializeProperty(entry, typeof(MdiClientBorderStyle), this.borderStyle);
						break;
					}
						// AS 5/1/03 WTB880
					case "BorderColor":
					{
						this.borderColor = (Color)Utils.DeserializeProperty(entry, typeof(Color), Color.Empty);
						break;
					}

					//	BF 7.27.04	NAS2004 Vol3 - Office2003 look & feel
					case "ViewStyle":
					{
						this.viewStyle = (ViewStyle)Utils.DeserializeProperty(entry, typeof(ViewStyle), ViewStyle.Standard);
						break;
					}

					// AS 3/7/05 NA 2005 Vol 2 - Nested TabGroups
					case "AllowNestedTabGroups":
					{
						this.allowNestedTabGroups = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), DefaultableBoolean.Default);
						break;
					}

					// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
					case "MaximizedTabGroupDisplayStyle":
					{
						this.maximizedTabGroupDisplayStyle = (MaximizedMdiTabGroupDisplayStyle)Utils.DeserializeProperty(entry, typeof(MaximizedMdiTabGroupDisplayStyle), MaximizedTabGroupDisplayStyleDefault);
						break;
					}

					// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
					case "IsActiveTabGroupMaximized":
					{
						this.isActiveTabGroupMaximized = (bool)Utils.DeserializeProperty(entry, typeof(bool), IsActiveTabGroupMaximizedDefault);
						break;
					}

					// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
					case "AllowMaximize":
					{
						this.allowMaximize = (bool)Utils.DeserializeProperty(entry, typeof(bool), AllowMaximizeDefault);
						break;
					}

					// AS 5/22/07 BR23151 - MdiTabNavigationMode 
					case "TabNavigationMode":
					{
						this.tabNavigationMode = (MdiTabNavigationMode)Utils.DeserializeProperty(entry, typeof(MdiTabNavigationMode), TabNavigationModeDefault);
						break;
					}

					default:
						// AS 4/30/03 FxCop Change
						// Explicitly call the overload that takes an IFormatProvider
						//
						//Debug.Assert(false, string.Format("Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						Debug.Assert(false, string.Format(null, "Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						break;
				}
			}
		}
		#endregion //InitializeFrom

		#region CreateObjectStreamer
		private ObjectStreamer CreateObjectStreamer()
		{
			ObjectStreamer streamer = new ObjectStreamer("Infragistics.Win.UltraWinTabbedMdi", 25);

			System.Collections.IDictionary dictionary = streamer.Dictionary;

			if (this.ShouldSerializeAllowHorizontalTabGroups())
				dictionary.Add("AllowHorizontalTabGroups", this.AllowHorizontalTabGroups);

			if (this.ShouldSerializeAllowVerticalTabGroups())
				dictionary.Add("AllowVerticalTabGroups", this.AllowVerticalTabGroups);

			if (this.ShouldSerializeAppearance())
				dictionary.Add("Appearance", this.appearanceHolder);

			if (this.ShouldSerializeSplitterAppearance())
				dictionary.Add("SplitterAppearance", this.splitterAppearanceHolder);

			if (this.ShouldSerializeAppearances())
				dictionary.Add("Appearances", this.Appearances);

			if (this.ShouldSerializeTabSettings())
				dictionary.Add("TabSettings", this.TabSettings);

			if (this.ShouldSerializeTabGroupSettings())
				dictionary.Add("TabGroupSettings", this.TabGroupSettings);

			if (this.ShouldSerializeTabGroups())
				dictionary.Add("TabGroups", this.TabGroups);

			if (this.ShouldSerializeShowToolTips())
				dictionary.Add("ShowToolTips", this.ShowToolTips);

			if (this.ShouldSerializeUseMnemonics())
				dictionary.Add("UseMnemonics", this.UseMnemonics);

			if (this.ShouldSerializeOrientation())
				dictionary.Add("Orientation", this.Orientation);

			if (this.ShouldSerializeSplitterBorderStyle())
				dictionary.Add("SplitterBorderStyle", this.SplitterBorderStyle);

			if (this.ShouldSerializeMaxTabGroups())
				dictionary.Add("MaxTabGroups", this.MaxTabGroups);

			if (this.ShouldSerializeSplitterWidth())
				dictionary.Add("SplitterWidth", this.SplitterWidth);

			if (this.ShouldSerializeImageSize())
				dictionary.Add("ImageSize", this.ImageSize);

			if (this.ShouldSerializeImageTransparentColor())
				dictionary.Add("ImageTransparentColor", this.ImageTransparentColor);

			// save the base class properties...
			if (this.AlphaBlendMode != AlphaBlendMode.Optimized)
				dictionary.Add("AlphaBlendMode", this.AlphaBlendMode);

			if (!this.Enabled)
				dictionary.Add("Enabled", this.Enabled);

			// AS 3/20/06 AppStyling
			//if (this.FlatMode)
			//	dictionary.Add("FlatMode", this.FlatMode);
			if (DefaultableBoolean.Default != this.UseFlatMode)
				dictionary.Add("UseFlatMode", this.UseFlatMode);

			// AS 3/20/06 AppStyling
			//if (!this.SupportThemes)
			//	dictionary.Add("SupportThemes", this.SupportThemes);
			if (DefaultableBoolean.Default != this.UseOsThemes)
				dictionary.Add("UseOsThemes", this.UseOsThemes);

			// we have to serialize the unassigned tabs separately since
			// they are not part of a group that can be serialized
			if (this.HasHiddenTabs)
				dictionary.Add("HiddenTabs", this.HiddenTabs);

			// AS 4/14/03 WTB801
			// Save the index of the tab group containing the active tab.
			//
			MdiTab activeTab = this.ActiveTab;

			if (activeTab != null && activeTab.TabGroup != null)
			{
				// AS 6/3/03
				// Use the new Index property
				//dictionary.Add("ActiveTabGroupIndex", this.TabGroups.IndexOf(activeTab.TabGroup));
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				// The actual active group may be nested so we just need to
				// store the index of the ancestor group in our tabgroups collection.
				//
				//dictionary.Add("ActiveTabGroupIndex", activeTab.TabGroup.Index);
				dictionary.Add("ActiveTabGroupIndex", this.GetTabGroupIndex(this.ActiveTab, null));
			}

			// AS 4/29/03 WTB880
			if (this.BorderStyle != BorderStyleDefault)
				dictionary.Add("BorderStyle", this.BorderStyle);

			// AS 5/1/03 WTB880
			if (!this.BorderColor.IsEmpty)
				dictionary.Add("BorderColor", this.BorderColor);

			//	BF 7.27.04	NAS2004 Vol3 - Office2003 look & feel
			if ( this.ShouldSerializeViewStyle() )
				dictionary.Add( "ViewStyle", this.ViewStyle );

			// AS 3/7/05 NA 2005 Vol 2 - Nested TabGroups
			if ( this.ShouldSerializeAllowNestedTabGroups() )
				dictionary.Add( "AllowNestedTabGroups", this.AllowNestedTabGroups );

			// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
			if ( this.ShouldSerializeMaximizedTabGroupDisplayStyle() )
				dictionary.Add( "MaximizedTabGroupDisplayStyle", this.MaximizedTabGroupDisplayStyle );

			// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
			if ( this.ShouldSerializeIsActiveTabGroupMaximized() )
				dictionary.Add( "IsActiveTabGroupMaximized", this.IsActiveTabGroupMaximized );

			// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
			if ( this.ShouldSerializeAllowMaximize() )
				dictionary.Add( "AllowMaximize", this.AllowMaximize );

			// AS 5/22/07 BR23151 - MdiTabNavigationMode 
			if (this.TabNavigationMode != TabNavigationModeDefault)
				dictionary.Add( "TabNavigationMode", this.TabNavigationMode );

			return streamer;
		}
		#endregion //CreateObjectStreamer

		#region RestoreTabs
		private void RestoreTabs( MdiTabsBaseCollection tabs )
		{
			this.isRestoringTabs = true;

			try
			{
				// if the tab was in a tab groups' tabs collection
				// then it was visible
				bool wasVisible = tabs is MdiTabsCollection;

				foreach(MdiTab tab in tabs)
				{
					RestoreTabEventArgs args = new RestoreTabEventArgs(tab, wasVisible);

					// try to find a suggested form to initialize it with
					if (tab.PersistedInfo != null)
						args.Form = this.FindMatchingForm(tab);

					this.InvokeEvent( TabbedMdiEventIds.RestoreTab, args );

					if (args.Form == null)
						continue;

					// make sure there wasn't another form already associated
					// with that tab
					MdiTab oldTab = this.GetTabFromForm(args.Form, false, false, false);

					// if there was, unhook that one
					if (oldTab != null)
						oldTab.MdiChild = null;

					// associate the tab with the form
					tab.MdiChild = args.Form;

					// initialize the form settings
					this.InitializeTabFormSettings(tab);
				}
			}
			finally
			{
				this.isRestoringTabs = false;
			}
		}

		#endregion //RestoreTabs

		#region FindMatchingForm
		private Form FindMatchingForm( MdiTab tab )
		{
			if (tab == null || tab.PersistedInfo == null || this.mdiParent == null)
				return null;

			foreach(Form mdiChild in this.MdiParent.MdiChildren)
			{
				if ( !(mdiChild is ICustomMdiTab) )
					continue;

				if ( Object.Equals( ((ICustomMdiTab)mdiChild).UniqueId, tab.PersistedInfo ) )
				{
					// make sure its not associated with an mdi tab already
					if (this.GetTabFromForm(mdiChild, false, false, false) == null)
						return mdiChild;
				}
			}

			return null;
		}
		#endregion //FindMatchingForm

		#endregion //Load/Save related

		#region Form/MdiClient Hookup related

		#region EnsureOnlyOneTabbedMdiManager
		private void EnsureOnlyOneTabbedMdiManager(Form form)
		{
			// at design time, we don't deal with the form - we only store it
			// for use at runtime
			if (this.DesignMode)
				return;

			// if we're just unhooking, then we don't have to check
			if (form == null)
				return;

			ITabbedMdiManager manager = MdiParentManager.GetTabbedMdiManager(form);

			Debug.Assert( manager != this, "The specified 'Form' is already associated with this 'UltraTabbedMdiManager' instance");

			// AS 3/21/05 BR02938
			//if (manager != null)
			if (manager != null && manager != this)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_42"));
		}
		#endregion //EnsureOnlyOneTabbedMdiManager

		#region UnhookMdiParent
		private void UnhookMdiParent()
		{
			// at design time, we don't deal with the form - we only store it
			// for use at runtime
			if (this.DesignMode)
				return;

			// if there was no previous form being manager, exit
			if (this.mdiParent == null)
				return;

			Debug.Assert( !this.IsInitializing, "An 'MdiParent' was associated with the 'UltraTabbedMdiManager' but the manager is currently initializing");

			Debug.Assert( this.hookedMdiParent, "The previous 'MdiParent' was not hooked" );

			if (!this.hookedMdiParent)
				return;

			// unregister the form with the tabbed mdi manager so we always know
			// if a form is already associated with a manager.
			MdiParentManager.UnregisterTabbedMdiManager(this, this.mdiParent);

			// unhook the control added/removed event
			this.mdiParent.ControlAdded -= new ControlEventHandler(this.OnMdiParentFormControlAdded);
			this.mdiParent.ControlRemoved -= new ControlEventHandler(this.OnMdiParentFormControlRemoved);
			this.mdiParent.MdiChildActivate -= new EventHandler(this.OnMdiParentMdiChildActivated);

			// AS 3/8/05 BR02542
			this.mdiParent.HandleCreated -= new EventHandler( this.OnMdiParentHandleCreated );
			this.UnsubclassMdiParent();

			this.hookedMdiParent = false;

			// unhook the mdi client
			this.UnhookMdiClient(this.mdiClient);

			// release all the controls and tab objects
			this.ReleaseMdiTabsAndForms();
		}
		#endregion //UnhookMdiParent

		#region HookMdiParent
		private void HookMdiParent()
		{
			// at design time, we don't deal with the form - we only store it
			// for use at runtime
			if (this.DesignMode)
				return;

			// if there is no mdi parent, there's nothing to do
			if (this.mdiParent == null)
				return;

			// during initialization, we don't want to hook up to the form until
			// the end init
			if (this.IsInitializing)
				return;

			// first make sure that there isn't an mdi manager already associated
			// with the form
			this.EnsureOnlyOneTabbedMdiManager(this.mdiParent);

			Debug.Assert( !this.hookedMdiParent, "The 'MdiParent' has already been hooked" );

			if (this.hookedMdiParent)
				return;

			// register the form with the tabbed mdi manager so we always know
			// if a form is already associated with a manager.
			MdiParentManager.RegisterTabbedMdiManager(this, this.mdiParent);

			// hook the control added/remove events
			this.mdiParent.ControlAdded += new ControlEventHandler(this.OnMdiParentFormControlAdded);
			this.mdiParent.ControlRemoved += new ControlEventHandler(this.OnMdiParentFormControlRemoved);
			this.mdiParent.MdiChildActivate += new EventHandler(this.OnMdiParentMdiChildActivated);

			this.hookedMdiParent = true;

			// AS 3/8/05 BR02542
			if (!this.mdiParent.IsHandleCreated)
				this.mdiParent.HandleCreated += new EventHandler( this.OnMdiParentHandleCreated );
			else
				this.SubclassMdiParent(this.mdiParent);

			// hook the mdi client if possible
			this.HookMdiClient(null);

			// create the tabgroups and tabs for any existing forms
			if (this.mdiClient != null)
				this.CreateTabsForForms(this.MdiParent.MdiChildren);
		}

		#endregion //HookMdiParent

		#region HookMdiClient
		private void HookMdiClient(MdiClient mdiClient)
		{
			if (this.DesignMode || this.IsInitializing)
				return;

			Debug.Assert( !this.hookedMdiClient, "Already hooked the events of the mdi client area");

			if (this.hookedMdiClient)
				return;

			Debug.Assert( this.mdiParent != null, "There is no 'MdiParent' to locate an 'MdiClient'");

			if (this.mdiParent == null)
				return;

			// find it if it was not specified
			if (mdiClient == null)
			{
				foreach(Control control in this.mdiParent.Controls)
				{
					if (control is MdiClient)
					{
						mdiClient = control as MdiClient;
						break;
					}
				}
			}

			// bail out if there is no mdi client yet
			if (mdiClient == null)
				return;

			// store the mdi client reference
			this.mdiClient = mdiClient;

			mdiClient.ControlAdded += new ControlEventHandler( this.OnMdiClientControlAdded );
			mdiClient.ControlRemoved += new ControlEventHandler( this.OnMdiClientControlRemoved );
			mdiClient.Resize += new EventHandler( this.OnMdiClientResize );
			mdiClient.LocationChanged += new EventHandler( this.OnMdiClientLocationChanged );
			mdiClient.Layout += new LayoutEventHandler( this.OnMdiClientLayout );

			if (!mdiClient.IsHandleCreated)
				mdiClient.HandleCreated += new EventHandler( this.OnMdiClientHandleCreated );

			this.hookedMdiClient = true;

			if (mdiClient.IsHandleCreated)
				this.SubclassMdiClient(mdiClient);
		}
		#endregion //HookMdiClient

		#region UnhookMdiClient
		private void UnhookMdiClient(MdiClient mdiClient)
		{
			Debug.Assert( this.hookedMdiClient, "Not currently hooked into an MdiClient control.");
			Debug.Assert( mdiClient != null, "There is no 'MdiClient' to hook");

			// find it if it was not specified
			if (mdiClient == null)
			{
				foreach(Control control in this.mdiParent.Controls)
				{
					if (control is MdiClient)
					{
						mdiClient = control as MdiClient;
						break;
					}
				}
			}

			if (mdiClient == null)
				return;

			// AS 4/29/03 WTB880
			// Don't do this yet.
			//
			// // store the mdi client reference
			//this.mdiClient = null;

			mdiClient.HandleCreated -= new EventHandler( this.OnMdiClientHandleCreated );
			mdiClient.ControlAdded -= new ControlEventHandler( this.OnMdiClientControlAdded );
			mdiClient.ControlRemoved -= new ControlEventHandler( this.OnMdiClientControlRemoved );
			mdiClient.Resize -= new EventHandler( this.OnMdiClientResize );
			mdiClient.LocationChanged -= new EventHandler( this.OnMdiClientLocationChanged );
			mdiClient.Layout -= new LayoutEventHandler( this.OnMdiClientLayout );

			this.hookedMdiClient = false;

			this.UnsubclassMdiClient();

			// store the mdi client reference
			this.mdiClient = null;
		}
		#endregion //UnhookMdiClient

		#region Subclass/Unsubclass MdiClient
		private void SubclassMdiClient(MdiClient mdiClient)
		{
			// if we've already subclassed it, exit
			if (this.IsMdiClientSubclassed)
				return;

			Debug.Assert( mdiClient != null, "A null 'MdiClient' cannot be subclassed");

			// if the handle is not created, wait until it is
			if (!mdiClient.IsHandleCreated)
				return;

			try
			{
				// MD 1/4/07 - FxCop
				// This is not needed
				//SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
				//
				//perm.Assert();

				// AS 10/30/03 WTB1216
				// If we have an mdi client subclasser, try to reuse it.
				//
				if (this.mdiClientSubclasser != null &&
					this.mdiClientSubclasser.Activate(this, mdiClient))
						return;

				this.SubclassMdiClientUnsafe(mdiClient);
			}			
			catch (System.Security.SecurityException)	// AS 4/29/03 Only catch security exceptions
			{
				this.hasUnmanagedCodeRights = false;
			}
		}

		private void SubclassMdiClientUnsafe(MdiClient mdiClient)
		{
			MdiClientSubclasser nativeWindow = new MdiClientSubclasser(this);

			nativeWindow.AssignHandle(mdiClient.Handle);

			this.mdiClientSubclasser = nativeWindow;
		}
		#endregion // Subclass MdiClient

		#region Unsubclass MdiClient
		private void UnsubclassMdiClient()
		{
			if (this.mdiClientSubclasser == null)
				return;

			
			this.mdiClientSubclasser.Unsubclass();
		}

		
		#endregion // Unsubclass MdiClient

		// AS 3/8/05 BR02542
		#region Subclass/Unsubclass MdiParent
		private void SubclassMdiParent(Form mdiParent)
		{
			// if we've already subclassed it, exit
			if (this.IsMdiParentSubclassed)
				return;

			Debug.Assert( mdiParent != null, "A null mdi parent cannot be subclassed");

			// if the handle is not created, wait until it is
			if (!mdiParent.IsHandleCreated)
				return;

			try
			{
				// MD 1/4/07 - FxCop
				// This is not needed
				//SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
				//
				//perm.Assert();

				// AS 10/30/03 WTB1216
				// If we have an mdi client subclasser, try to reuse it.
				//
				if (this.mdiParentSubclasser != null &&
					this.mdiParentSubclasser.Activate(this, mdiParent))
					return;

				this.SubclassMdiParentUnsafe(mdiParent);
			}			
			catch (System.Security.SecurityException)	// AS 4/29/03 Only catch security exceptions
			{
				this.hasUnmanagedCodeRights = false;
			}
		}

		private void SubclassMdiParentUnsafe(Form mdiParent)
		{
			MdiParentSubclasser nativeWindow = new MdiParentSubclasser(this);

			nativeWindow.AssignHandle(mdiParent.Handle);

			this.mdiParentSubclasser = nativeWindow;
		}

		private void UnsubclassMdiParent()
		{
			if (this.mdiParentSubclasser == null)
				return;

			this.mdiParentSubclasser.Unsubclass();
		}

		#endregion // Unsubclass MdiParent

		#endregion //Form/MdiClient Hookup related

		#region MdiParent / MdiClient events

		#region MdiParent ControlAdded/ControlRemoved
		private void OnMdiParentFormControlAdded(object sender, ControlEventArgs e)
		{
			if ( !(e.Control is MdiClient) )
				return;

			// Hook the mdiclient events
			this.HookMdiClient(e.Control as MdiClient);
		}

		private void OnMdiParentFormControlRemoved(object sender, ControlEventArgs e)
		{
			if ( !(e.Control is MdiClient) )
				return;
			
			// Unhook the mdiclient events and destroy any tabgroups
			this.UnhookMdiClient(e.Control as MdiClient);
		}
		#endregion //MdiParent ControlAdded/ControlRemoved

		#region MdiParent MdiChildActivated
		private void OnMdiParentMdiChildActivated(object sender, EventArgs e)
		{
			// if we are restoring from a stream or releasing our tabs, do
			// not proceed
			if (this.IsRestoringTabs || this.IsReleasingTabs)
				return;

			// don't do anything during initialization - shouldn't happen since
			// we don't hook the events but just in case...
			if (this.IsInitializing)
				return;

			// cache the last active tab group
			this.CacheLastActiveTabGroup();

			// invalidate the previous active tab
			if (this.cachedActiveTab != null && this.cachedActiveTab.TabGroup != null)
				this.cachedActiveTab.TabGroup.DirtyTabItem(this.cachedActiveTab, true, false, false);

			// store the new one
			this.cachedActiveTab = this.ActiveTab;

			// invalidate the new active tab
			if (this.cachedActiveTab != null && this.cachedActiveTab.TabGroup != null)
				this.cachedActiveTab.TabGroup.DirtyTabItem(this.cachedActiveTab, true, false, false);

			// if we are activating the mdi child, we're handling the events
			if (this.IsActivatingMdiChild)
				return;

			// if we are already processing the activation of the form with
			// our mdi client subclasser, exit and wait for its notification
			if (this.SubclasserActivatingMdiChild)
				return;

			// this seems to happen when a new mdichild is activated. we can't
			// prevent the selection
			this.EnsureActiveTabIsSelected(true);

			// AS 10/9/03 WTB1192
			// When a tab is activated, it should be brought into view.
			if (this.ActiveTab != null)
				this.ActiveTab.EnsureTabInView();

			// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
			this.InvokeTabActivatedEvent();
		}
		#endregion //MdiParent MdiChildActivated

		// AS 3/8/05 BR02542
		#region MdiParent HandleCreated
		private void OnMdiParentHandleCreated(object sender, EventArgs e)
		{
			// subclass the mdiclient
			if (!this.IsMdiParentSubclassed)
				this.SubclassMdiParent(sender as Form);

			Debug.Assert(this.IsMdiParentSubclassed, "Attempted to subclass the mdiparent but it was not successful.");

			if (!this.IsMdiParentSubclassed)
				return;

			Control control = sender as Control;

			// unhook now
			if (control != null)
			{
				// AS 12/29/05
				// We were unhooking the wrong event.
				//
				//control.HandleCreated -= new EventHandler(this.OnMdiClientHandleCreated);
				control.HandleCreated -= new EventHandler(this.OnMdiParentHandleCreated);
			}
		}
		#endregion //MdiParent HandleCreated

		#region MdiClient LocationChanged
		private void OnMdiClientLocationChanged(object sender, EventArgs e)
		{
			
			// going to fire

			// reposition the forms and associated tabgroups
			this.PositionAndSizeTabGroups();
		}
		#endregion //MdiClient LocationChanged

		#region MdiClient HandleCreated
		private void OnMdiClientHandleCreated(object sender, EventArgs e)
		{
			// subclass the mdiclient
			if (!this.IsMdiClientSubclassed)
				this.SubclassMdiClient(sender as MdiClient);

			Debug.Assert(this.IsMdiClientSubclassed, "Attempted to subclass the mdiclient but it was not successful.");

			if (!this.IsMdiClientSubclassed)
				return;

			Control control = sender as Control;

			// unhook now
			if (control != null)
				control.HandleCreated -= new EventHandler(this.OnMdiClientHandleCreated);
		}
		#endregion //MdiClient HandleCreated

		#region OnMdiClientLayout
		private void OnMdiClientLayout(object sender, LayoutEventArgs e)
		{
			
			//			// initialize the tab group extents if needed
			//			this.InitializeTabGroupExtents(true);
			//
			//			// reposition the tab groups and controls
			//			this.PositionAndSizeTabGroups();
		}
		#endregion //OnMdiClientLayout

		#region MdiClient ControlAdded/ControlRemoved
		private void OnMdiClientControlRemoved(object sender, ControlEventArgs e)
		{
			// don't do anything while initializing
			if (this.IsInitializing)
				return;

			// AS 4/30/03 FxCop Change
			// Explicitly call the overload that takes an IFormatProvider
			//
			//Debug.Assert( !this.IsRestoringTabs, string.Format("A control [{0}] is being removed while deserializing.", e.Control));
			Debug.Assert( !this.IsRestoringTabs, string.Format(null, "A control [{0}] is being removed while deserializing.", e.Control));

			// find out which mditab is associated with that form
			if (e.Control is Form)
			{
				MdiTab tab = this.GetTabFromForm(e.Control as Form, false, false, false);

				this.RemoveTabFromTabGroup(tab);
			}
		}

		private void OnMdiClientControlAdded(object sender, ControlEventArgs e)
		{
			// don't do anything while initializing
			if (this.IsInitializing)
				return;

			// don't add/remove any mdi tab's while restoring from a serialized tab manager
			if (this.IsRestoringTabs)
				return;

			MdiTab tab = this.GetTabFromForm(e.Control as Form, false, false, false);

			// don't do anything if we already created a tab for it
			if (tab != null)
				return;

			// create the 'MdiTab' object for the specified form
			this.CreateTabsForForms( new Form[] { e.Control as Form } );
		}

		#endregion //MdiClient ControlAdded/ControlRemoved

		#region OnMdiClientResize
		private void OnMdiClientResize(object sender, EventArgs e)
		{
			// AS 3/24/05
			// While disposing the component, we really don't need
			// to be repositioning the tab group controls, etc since
			// we will be disposing them shortly.
			//
			if (this.IsDisposing)
				return;

			// cache the mdi client size
			this.CachedMdiClientSize();

			// reposition the forms and associated tabgroups
			if (!this.HasTabGroups)
				return;

			

			// first update the extents of the tab groups
			this.InitializeTabGroupExtents(true);

			// make sure everything gets dirtied
			this.DirtyTabGroups(MdiTabGroup.TabManagerChangeType.AllMetrics | MdiTabGroup.TabManagerChangeType.Invalidate);
			this.TabGroupManager.DirtyAllTabGroups();

			
			// now position and size the tab groups
			this.PositionAndSizeTabGroups();
		}
		#endregion //OnMdiClientResize

		#endregion //MdiParent / MdiClient events

		#region Create/Remove Owned Controls

		#region RemoveTabGroupControls
		private void RemoveTabGroupControls()
		{
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			// Note, even though a tabgroup that doesn't have tabs
			// will not have a tabgroup control, we'll check them just
			// in case they don't currently have tabs.
			//
			//foreach(MdiTabGroup tabGroup in this.TabGroups)
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
				this.RemoveTabGroupControl(tabGroup);

			Debug.Assert(this.TabGroupControls.Count == 0, "The 'MdiTabGroupControl' objects for the known 'MdiTabGroup' objects were disposed but the 'TabGroupControls' collection is not empty.");
		}
		#endregion //RemoveTabGroupControls

		#region RemoveTabGroupControl
		private void RemoveTabGroupControl( MdiTabGroup tabGroup )
		{
			// get its associated control
			MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;

			if (tabGroupControl == null)
				return;

			// unhook from the font changed event
			tabGroupControl.FontChanged -= new EventHandler( this.OnTabGroupControlFontChanged );

			

			// remove it from the tab group controls
			this.TabGroupControls.Remove(tabGroup);

			// dispose the tab group control
			tabGroupControl.Dispose();
		}
		#endregion //RemoveTabGroupControl

		#region CreateTabGroupControls
		private void CreateTabGroupControls()
		{
			if (!this.Enabled)
				return;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.TabGroups)
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
			{
				// see if we have a tabgroup for the specified tab group
				MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;
						
				// if the tab group has a control, there is nothing to do
				if (tabGroupControl != null)
					continue;
						
				// create an MdiTabGroupControl to house the MdiTabGroup
				this.CreateTabGroupControl(tabGroup);
			}
		}
		#endregion //CreateTabGroupControls

		#region CreateTabGroupControl
		private void CreateTabGroupControl(MdiTabGroup tabGroup)
		{
			if (!this.Enabled)
				return;

			Debug.Assert( !this.TabGroupControls.Contains(tabGroup), "The specified 'MdiTabGroup' already contains an associated 'MdiTabGroupControl'");

			// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
			Debug.Assert( !tabGroup.HasTabGroups, "An MdiTabGroup that contains other tab groups should not have an associated MdiTabGroupControl!");

			// create an MdiTabGroupControl to house the MdiTabGroup
			MdiTabGroupControl tabGroupControl = this.CreateMdiTabGroupControl(tabGroup);

			if (tabGroupControl == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_43"));

			// start it off as invisible
			tabGroupControl.Visible = false;

			// hook into the font changed notification
			tabGroupControl.FontChanged += new EventHandler( this.OnTabGroupControlFontChanged );

			// AS 5/2/03 WTB884
			tabGroupControl.DragDrop += new DragEventHandler( this.OnTabGroupControlDragDrop );
			tabGroupControl.DragEnter += new DragEventHandler( this.OnTabGroupControlDragEnter );
			tabGroupControl.DragLeave += new EventHandler( this.OnTabGroupControlDragLeave );
			tabGroupControl.DragOver += new DragEventHandler( this.OnTabGroupControlDragOver );

			// AS 5/2/03 WTB884
			if (this.CanSetAllowDrop && this.allowExternalDrop)
				tabGroupControl.AllowDrop = true;

            // MBS 4/5/06
			// initialize the context menu with the manager's
			//tabGroupControl.ContextMenu = this.TabContextMenu;

			// store it using the tab group as its key
			this.TabGroupControls.Add(tabGroup, tabGroupControl);

			// add it to the parent's controls collection
			this.MdiParent.Controls.Add(tabGroupControl);

			// make sure its above the other controls - esp the mdiclient
			this.MdiParent.Controls.SetChildIndex(tabGroupControl, 0);
		}
		#endregion //CreateTabGroupControl

		#region CreateSplitterControls
		private void CreateSplitterControls()
		{
			if (!this.Enabled)
				return;

			
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
			{
				// see if we have a splitter for the specified tab group
				SplitterBarControl tabGroupControl = this.SplitterControls[tabGroup] as SplitterBarControl;
						
				bool needsSplitter = this.TabGroupManager.NeedsSplitter(tabGroup);

				// if the tab group has a control, there is nothing to do
				if (tabGroupControl == null && !needsSplitter)
					continue;
				else if (tabGroupControl != null && needsSplitter)
					continue;
						
				// create an SplitterBarControl to allow resizing the tab group
				if (needsSplitter)
					this.CreateSplitterControl(tabGroup);
				else
					this.RemoveSplitterControl(tabGroup);
			}
		}
		#endregion //CreateSplitterControls

		#region CreateSplitterControl
		private void CreateSplitterControl(MdiTabGroup tabGroup)
		{
			if (!this.Enabled)
				return;

			Debug.Assert( !this.SplitterControls.Contains(tabGroup), "The specified 'MdiTabGroup' already contains an associated 'SplitterBarControl'");
			// AS 6/3/03
			// Use the new Index property
			//Debug.Assert( this.TabGroups.IndexOf(tabGroup) < this.TabGroups.Count - 1, "The last 'MdiTabGroup' should not have a 'SplitterBarControl'");
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//Debug.Assert( tabGroup.Index < this.TabGroups.Count - 1, "The last 'MdiTabGroup' should not have a 'SplitterBarControl'");
			Debug.Assert( tabGroup.ParentCollection == null || tabGroup.Index < tabGroup.ParentCollection.Count - 1, "The last 'MdiTabGroup' should not have a 'SplitterBarControl'");

			// create an SplitterBarControl for the tab group
			SplitterBarControl splitterControl = this.CreateSplitterBarControl();

			if (splitterControl == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_44"));

			// start it off as invisible
			splitterControl.Visible = false;

			// initialize the border style & orientation
			splitterControl.BorderStyle = this.SplitterBorderStyleResolved;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//splitterControl.Orientation = this.Orientation;
			splitterControl.Orientation = tabGroup.ParentOrientation;

			// AS 5/5/06 AppStyling
			//splitterControl.ResolveAppearanceCallback = this.SplitterAppearanceCallback;
			splitterControl.ResolveAppearanceCallback = splitterControl.Orientation == Orientation.Vertical
				? this.SplitterAppearanceCallbackVert
				: this.SplitterAppearanceCallbackHorz;

			// hook into the drag notifications
			splitterControl.SplitterDragging += new CancelEventHandler( this.OnSplitterBarDragging );
			splitterControl.SplitterDragged += new SplitterDraggedEventHandler( this.OnSplitterBarDragged );
			splitterControl.SplitterMove += new SplitterMoveEventHandler( this.OnSplitterBarMove );

			// store it using the tab group as its key
			this.SplitterControls.Add(tabGroup, splitterControl);

			// add it to the parent's controls collection
			this.MdiParent.Controls.Add(splitterControl);

			// make sure its above the other controls - esp the mdiclient
			this.MdiParent.Controls.SetChildIndex(splitterControl, 0);
		}
		#endregion //CreateSplitterControl

		#region RemoveSplitterControls
		private void RemoveSplitterControls()
		{
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.TabGroups)
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
				this.RemoveSplitterControl(tabGroup);

			Debug.Assert(this.SplitterControls.Count == 0, "The 'SplitterBarControl' objects for the known 'MdiTabGroup' objects were disposed but the 'SplitterControls' collection is not empty.");
		}
		#endregion //RemoveSplitterControls

		#region RemoveSplitterControl
		private void RemoveSplitterControl( MdiTabGroup tabGroup )
		{
			// get its associated control
			SplitterBarControl splitterControl = this.SplitterControls[tabGroup] as SplitterBarControl;

			if (splitterControl == null)
				return;

			// unhook from the drag events
			splitterControl.SplitterDragging -= new CancelEventHandler( this.OnSplitterBarDragging );
			splitterControl.SplitterDragged -= new SplitterDraggedEventHandler( this.OnSplitterBarDragged );
			splitterControl.SplitterMove -= new SplitterMoveEventHandler( this.OnSplitterBarMove );

			// remove it from the tab group controls
			this.SplitterControls.Remove(tabGroup);

			// dispose the tab group control
			splitterControl.Dispose();
		}
		#endregion //RemoveSplitterControl

		#endregion //Create/Remove Controls

		#region Splitter & TabGroupControl Event sinks

		#region OnTabGroupControlFontChanged
		private void OnTabGroupControlFontChanged(object sender, EventArgs e)
		{
			MdiTabGroupControl tabGroupControl = sender as MdiTabGroupControl;

			// dirty the tabmanager font info
			if (tabGroupControl != null && tabGroupControl.TabGroup != null)
				tabGroupControl.TabGroup.DirtyTabManager(MdiTabGroup.TabManagerChangeType.Font);
		}
		#endregion //OnTabGroupControlFontChanged

		#region Splitter events
		private void OnSplitterBarDragging(object sender, CancelEventArgs e)
		{
			MdiTabGroup tabGroup = this.GetTabGroupFromSplitter(sender as SplitterBarControl);

			if (tabGroup == null)
			{
				e.Cancel = true;
				return;
			}

			MdiSplitterDraggingEventArgs args = new MdiSplitterDraggingEventArgs(tabGroup);

			this.InvokeEvent(TabbedMdiEventIds.SplitterDragging, args);

			if (args.Cancel)
				e.Cancel = true;

			// store the min and max delta
			// AS 3/16/05
			// Based the range on the minimum size. We don't currently expose one
			// but this will make it easier to implement later.
			//
			//this.minDelta = -tabGroup.ClientExtent;
			this.minDelta = -Math.Max( 0, (tabGroup.Extent - this.GetMinimumTabGroupExtent(tabGroup)));

			// AS 6/3/03
			// Use the new Index property
			//int tabGroupIndex = this.TabGroups.IndexOf(tabGroup) + 1;
			int tabGroupIndex = tabGroup.Index + 1;
			// AS 3/16/05
			// Based the range on the minimum size. We don't currently expose one
			// but this will make it easier to implement later.
			//
			//this.maxDelta = this.TabGroups[tabGroupIndex].ClientExtent;
			this.maxDelta = Math.Max( 0, (tabGroup.ParentCollection[tabGroupIndex].Extent - this.GetMinimumTabGroupExtent(tabGroup.ParentCollection[tabGroupIndex])));
		}

		private void OnSplitterBarDragged(object sender, SplitterDraggedEventArgs e)
		{
			// if the drag was cancelled, don't do anything
			if (e.Cancelled)
				return;

			MdiTabGroup tabGroup = this.GetTabGroupFromSplitter(sender as SplitterBarControl);

			if (tabGroup == null)
				return;

			int delta;

			// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.Orientation == Orientation.Horizontal)
			if (tabGroup.ParentOrientation == Orientation.Horizontal)
				delta = e.MousePosition.Y - e.OriginalMousePosition.Y;
			else
				delta = e.MousePosition.X - e.OriginalMousePosition.X;

			// update the size of the tab group
			this.UpdateTabGroupExtent(tabGroup, tabGroup.Extent + delta);

			MdiTabGroupEventArgs args = new MdiTabGroupEventArgs(tabGroup);

			this.InvokeEvent(TabbedMdiEventIds.SplitterDragged, args);
		}

		private void OnSplitterBarMove(object sender, SplitterMoveEventArgs e)
		{
			// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
			//bool horizontal = this.Orientation == Orientation.Horizontal;
			bool horizontal = ((SplitterBarControl)sender).Orientation == Orientation.Horizontal;

			int delta;

			// restrict based on group size
			if (horizontal)
				delta = e.MousePosition.Y - e.OriginalMousePosition.Y;
			else
				delta = e.MousePosition.X - e.OriginalMousePosition.X;

			if (this.minDelta > delta)
			{
				Rectangle rect = e.SplitRectangle;

				if (horizontal)
					rect.Offset(0, minDelta - delta);
				else
					rect.Offset(minDelta - delta, 0);

				e.SplitRectangle = rect;
			}
			else if (this.maxDelta < delta)
			{
				Rectangle rect = e.SplitRectangle;

				if (horizontal)
					rect.Offset(0, maxDelta - delta);
				else
					rect.Offset(maxDelta - delta, 0);

				e.SplitRectangle = rect;
			}

			if (this.HasSplitterAppearance && 
				this.SplitterAppearance.HasPropertyBeenSet(AppearancePropFlags.Cursor))
			{
				e.DragCursor = this.SplitterAppearance.Cursor;
			}
		}
		#endregion //Splitter events

		#region Tab Group Drag Drop notifications

		private void OnTabGroupControlDragEnter(object sender, System.Windows.Forms.DragEventArgs e)
		{
			this.DragDropHelper.DragEnter(sender as MdiTabGroupControl, e);
		}

		private void OnTabGroupControlDragLeave(object sender, System.EventArgs e)
		{
			this.DragDropHelper.DragLeave(sender as MdiTabGroupControl);
		}

		private void OnTabGroupControlDragOver(object sender, System.Windows.Forms.DragEventArgs e)
		{
			this.DragDropHelper.DragOver(sender as MdiTabGroupControl, e);
		}

		private void OnTabGroupControlDragDrop(object sender, System.Windows.Forms.DragEventArgs e)
		{
			this.DragDropHelper.DragDrop(sender as MdiTabGroupControl, e);
		}
		#endregion //Drag Drop notifications

		#endregion //Splitter & TabGroupControl Event sinks

		#region TabGroup related

		#region PositionAndSizeTabGroups
		private void PositionAndSizeTabGroups()
		{
			// do not perform any sizing operations if the layout is suspended
			// or we are in a 'beginupdate' mode - i.e. not painting
			if (this.IsLayoutSuspendedResolved)
				return;

			if ( this.DesignMode )
				return;

			MdiClient mdiClient = this.mdiClient;
			Form mdiParent = this.MdiParent;

			Debug.Assert( mdiClient != null, "The 'mdiClient' member is null");

			if (mdiClient == null)
				return;

			// AS 4/23/03
			if (this.mdiClient.Disposing	|| 
				this.mdiClient.IsDisposed)
				return;

			// if the tab group size was changed while IsUpdating was true...
			if (this.invokeTabGroupResized)
			{
				// reset the flag
				this.invokeTabGroupResized = false;

				// see if everything is completely allocated
				
				if (this.AreTabGroupExtentsAllocated())
				{
					// if it is then fire the after resize
					this.InvokeEvent( TabbedMdiEventIds.TabGroupResized, new MdiTabGroupResizedEventArgs(this.TabGroups) );
				}
				else
				{
					// otherwise initialize the tab group extents
					this.InitializeTabGroupExtents(true);
					return;
				}
			}
			else
			{
				// ensure everything is allocated
				this.VerifyTabGroupExtents();
			}

			// if there's nothing to do, just leave
			if (!this.Enabled || !this.HasTabGroups)
				return;

			mdiParent.SuspendLayout();
			mdiClient.SuspendLayout();

			// AS 4/28/03 WTB873
			this.positioningControls = true;

			try
			{
				// make sure the controls are created
				this.CreateTabGroupControls();
				this.CreateSplitterControls();

				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				// Note, this is just the initialize count but there
				// should be about 2 controls per tabgroup with tabs.
				// Of course a tabgroup without tabs might have a splitter
				// but will never had a tabgroup control.
				//
				//ArrayList controls = new ArrayList(this.TabGroups.Count * 2);
				ArrayList controls = new ArrayList(this.TabGroupsWithTabsCount * 2);

				// then apply the position
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup tabGroup in this.TabGroups)
				foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
				{
					// get and position the splitter first
					SplitterBarControl splitterControl = this.SplitterControls[tabGroup] as SplitterBarControl;

					if (splitterControl != null)
					{
						// Update property
						splitterControl.SuppressLeadingEdge = this.SuppressSplitterLeadingEdge;

						// get the location of the splitter
						Rectangle splitterRect = this.TabGroupManager.GetSplitterControlRectangle(tabGroup);

						// set the bounds of the splitter control
						splitterControl.Bounds = splitterRect;

						// verify the orientation of the splitter
						splitterControl.Orientation = tabGroup.ParentOrientation;

						// AS 5/5/06 AppStyling
						splitterControl.ResolveAppearanceCallback = splitterControl.Orientation == Orientation.Vertical
							? this.SplitterAppearanceCallbackVert
							: this.SplitterAppearanceCallbackHorz;

						// store the control - we'll make them all visible at once
						controls.Add(splitterControl);
					}

					// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
					// Nesting groups won't need a MdiTabGroupControl
					//
					if (!tabGroup.HasTabs)
						continue;

					// get the associated tab group control
					MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;

					// size and position it
					Rectangle tabGroupRect = this.TabGroupManager.GetTabGroupControlRectangle(tabGroup);

					// size it based on the calculated location
					tabGroupControl.Bounds = tabGroupRect;

					// store the control - we'll make them all visible at once
					controls.Add(tabGroupControl);

					// get the position for the associated forms
					Rectangle formRect = this.TabGroupManager.GetFormPosition(tabGroup);

					// position each of the forms managed by the tab group
					foreach(MdiTab tab in tabGroup.Tabs)
						tab.Form.Bounds = formRect;
				}

				// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
				this.RefreshSplitterIntersections();

				// make all the associated tab group controls visible
				foreach(Control control in controls)
					control.Visible = true;
			}
			finally
			{
				// AS 4/28/03 WTB873
				this.positioningControls = false;

				// resume any pending layout processing
				mdiClient.ResumeLayout();
				mdiParent.ResumeLayout();
			}
		}
		#endregion //PositionAndSizeTabGroups

		#region ReleaseMdiTabsAndForms
		private void ReleaseMdiTabsAndForms()
		{
			this.isReleasingTabs = true;

			try
			{
				// get the mdi parent and client
				Form mdiParent = this.MdiParent;
				Control mdiClient = this.mdiClient;

				// suspend their layouts
				if (mdiParent != null)
					mdiParent.SuspendLayout();
				if (mdiClient != null)
					mdiClient.SuspendLayout();

				// destroy the associated controls
				this.RemoveSplitterControls();
				this.RemoveTabGroupControls();

				// restore the forms to their previous settings
				//
				this.InitializeMdiTabForms(true);

				// detach all the mdi tabs and tab groups...
				
				foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
				{
					// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
					// Don't force the tabs collection to be created
					// for a nested tab group.
					//
					if (tabGroup.HasTabs)
					{
						foreach(MdiTab tab in tabGroup.Tabs)
							tab.InitializeOwner(null);
					}

					tabGroup.InitializeTabbedMdiManager(null);

					tabGroup.Tabs.Clear();
				}

				// clear the tab groups
				this.TabGroups.Clear();

				// detach all the closed tabs
				foreach(MdiTab tab in this.HiddenTabs)
					tab.InitializeOwner(null);

				// clear the closed tabs
				this.HiddenTabs.Clear();

				// AS 1/5/04 WTB1271
				this.TabGroupManager.RemoveUnusedTabGroups();

				if (mdiParent != null)
					mdiParent.ResumeLayout();
				if (mdiClient != null)
					mdiClient.ResumeLayout();
			}
			finally
			{
				this.isReleasingTabs = false;
			}
		}
		#endregion //ReleaseMdiTabsAndForms

		#region EnsureActiveTabIsSelected
		private void EnsureActiveTabIsSelected(bool addToGroupIfNecessary)
		{
			MdiTab activeTab = this.ActiveTab;

			if (activeTab != null)
			{
				if (activeTab.TabGroup == null)
				{
					// AS 5/12/03
					// Make sure the tab is visible
					//
					if (addToGroupIfNecessary && activeTab.Form != null && activeTab.Form.Visible)
						this.AddTabToTabGroup(activeTab);
				}
				else if (activeTab.TabGroup.SelectedTab != activeTab)
				{
					// make sure it is the selected tab
					this.ActivateMdiChild(activeTab, true, true, false, false);
				}
			}
		}
		#endregion //EnsureActiveTabIsSelected

		#region EnsureTabIsSelected
		private void EnsureTabIsSelected( MdiTabGroup tabGroup, bool allowEventNotifications )
		{
			if (tabGroup == null)
				return;

			// make sure there is a selected tab and that's its in sync
			MdiTab selectedTab = tabGroup.SelectedTab;
			MdiTab activeTab = this.ActiveTab;

			// AS 4/22/05 BR03551
			// We cannot use the SelectedTab of the tab group
			// since the form of the tab in the old group may have
			// a higher zorder then that of the selected tab
			// in the newgroup and as a result must be selected.
			// Unfortunately we cannot change the zorder of the
			// child forms to maintain the group's selected tab
			// since changing the zorder will activate the form
			// which is not the desired behavior either.
			//
			//MdiTab tabToSelect = selectedTab;
			MdiTab tabToSelect;

			// if the tab group contains the active mdi child, that should
			// be the selected tab
			if (activeTab != null && activeTab.TabGroup == tabGroup)
				tabToSelect = activeTab;
			// AS 4/22/05 BR03551
			//else if (selectedTab == null)
			else
				tabToSelect = this.GetTopMostTab(tabGroup);

			if (selectedTab != tabToSelect)
			{
				// AS 4/23/03
				// We shouldn't be changing the active tab
				// while ensuring that a tab is selected.
				//
				this.ensuringTabIsSelected = true;

				try
				{
					if (allowEventNotifications)
					{
						tabGroup.SelectedTab = tabToSelect;
					}
					else
						tabGroup.InitializeSelectedTab(tabToSelect);
				}
				finally
				{
					this.ensuringTabIsSelected = false;
				}

				this.TabGroupManager.DirtyTabGroup(tabGroup);
			}
		}
		#endregion //EnsureTabIsSelected

		#region GetTopMostTab
		private MdiTab GetTopMostTab(MdiTabGroup tabGroup)
		{
			if (tabGroup == null)
				return null;

			if (this.mdiClient == null)
				return tabGroup.TabManager.FirstVisibleTabItem as MdiTab;

			MdiTab topMostTab = null;
			int topMostIndex = int.MaxValue;

			foreach(MdiTab tab in tabGroup.Tabs)
			{
				int formIndex = this.mdiClient.Controls.GetChildIndex(tab.Form, false);

				if (formIndex >= 0 && formIndex < topMostIndex)
				{
					topMostTab = tab;
					topMostIndex = formIndex;
				}
			}

			return topMostTab;
		}
		#endregion //GetTopMostTab

		#endregion // TabGroup related

		#region Create/Remove/Move Tabs & TabGroups

		#region AddTabToTabGroup
		private void AddTabToTabGroup( MdiTab tab )
		{
			Debug.Assert( tab != null && tab.TabGroup == null, "This method should only be invoked with an 'MdiTab' that is not associated with a TabGroup.");

			// add it to the active mdi tab group
			MdiTabGroup tabGroup = this.ActiveTabGroup;

			if (tabGroup == null)
				tabGroup = this.PreviousActiveTabGroup;

			// if there was no active mdi tab group, get a group
			if (tabGroup == null)
			{
				if (this.TabGroups.Count > 0)
				{
					// if there was no active mdi tab group, add it to the first tab group
					// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
					// We need a tab group with tabs
					//tabGroup = this.TabGroups[0];
					tabGroup = UltraTabbedMdiManager.GetFirstLeafTabGroup(this.TabGroups);
				}
				else
				{
					// if there are none, create one
					// AS 3/10/05 NA 2005 Vol 2 - Nested TabGroups
					// Add the new group to the component's collection
					//
					//tabGroup = this.CreateNewTabGroup(0, this.AvailableMdiClientExtent);
					tabGroup = this.CreateNewTabGroup(0, this.AvailableMdiClientExtent, null);
				}
			}

			// add the tab to the end
			// AS 3/18/05 NA 2005 Vol 2
			// It does not make sense to be firing the TabDisplaying
			// since these tabs are already in a tab group. We should
			// fire the tabmoving.
			//
			//this.MoveTab(tab, tabGroup, tabGroup.Tabs.Count, true);
			this.MoveTab(tab, tabGroup, tabGroup.Tabs.Count, tab.TabGroup == null, tab.TabGroup != null);

			// MRS 4/13/04 - Accessibility
			MdiTabGroupControl groupControl = tabGroup.Manager.GetTabGroupControl(tabGroup);
			if (groupControl != null)
				groupControl.AccessibilityNotifyClientsInternal(AccessibleEvents.Reorder, -1);
		}
		#endregion //AddMdiTabToTabGroup

		#region CreateNewTabGroup
		// AS 3/10/05 NA 2005 Vol 2 - Nested TabGroups
		//private MdiTabGroup CreateNewTabGroup(int index, int initialExtent)
		private MdiTabGroup CreateNewTabGroup(int index, int initialExtent, MdiTabGroup owningTabGroup)
		{
			// if there are none, create one
			MdiTabGroup tabGroup = this.CreateMdiTabGroup();

			// insert it into to our collection
			// AS 3/10/05 NA 2005 Vol 2 - Nested TabGroups
			//this.TabGroups.Insert(index, tabGroup);
			MdiTabGroupsCollection tabGroups = owningTabGroup == null ? this.TabGroups : owningTabGroup.TabGroups;
			tabGroups.Insert(index, tabGroup);

			// initialize its extent
			tabGroup.InitializeExtent(initialExtent);
			tabGroup.DirtyTabManager(MdiTabGroup.TabManagerChangeType.AllMetrics);

			// AS 3/10/05 NA 2005 Vol 2 - Nested TabGroups
			tabGroup.InitializeParentTabGroup(owningTabGroup);

			// dirty the info for all
			this.TabGroupManager.DirtyAllTabGroups();

			// fire the InitializeTabGroup event
			this.InvokeEvent( TabbedMdiEventIds.InitializeTabGroup, new MdiTabGroupEventArgs(tabGroup) );

			return tabGroup;
		}
		#endregion //CreateNewTabGroup

		// AS 5/29/03
		// Moving the first displayed tab from one group to another 
		// can cause a crash if the tab is part of another tabs collection 
		// and returns an index greater than the number of tabs currently
		// in the old collection.
		//
		#region RemoveTabGroupTabItem
		private void RemoveTabGroupTabItem( MdiTabGroup tabGroup, MdiTab tab )
		{
			// if the tab is not the first displayed tab, then just remove it
			if (tabGroup.FirstDisplayedTab != tab)
				tabGroup.Tabs.Remove(tab);
			else
			{
				// otherwise, get the index of the tab at the first displayed
				// tab index
				int index = ((ITabItem)tab).Index;

				tabGroup.Tabs.Remove(tab);

				// AS 5/30/03
				if (tabGroup.Tabs.Count == 0)
					return;

				// Can't do this since that will reverify the 
				// FirstDisplayedTab.
				//
				// // make sure someone didn't change it already
				//if (tabGroup.FirstDisplayedTab == tab)
				//{
				// make sure we make it one within the collection
				int newIndex = (int)Math.Max( 0, Math.Min( tabGroup.Tabs.Count - 1, index ) );

				// reset the first displayed tab
				tabGroup.FirstDisplayedTab = tabGroup.Tabs[newIndex];
				//}
			}
			
			// MRS 4/13/04 - Accessibility
			MdiTabGroupControl groupControl = tabGroup.Manager.GetTabGroupControl(tabGroup);
			if (groupControl != null)
				groupControl.AccessibilityNotifyClientsInternal(AccessibleEvents.Reorder, -1);
		}
		#endregion //RemoveTabGroupTabItem

		#region MoveTab
		// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
		// Added an overload that would invoke the TabMoving/TabMoved events.
		// Also changed to return a boolean indicating whether it was moved.
		//
		//private void MoveTab( MdiTab tab, MdiTabGroup newTabGroup, int tabIndex, bool invokeDisplayEvents )
		private bool MoveTab( MdiTab tab, MdiTabGroup newTabGroup, int tabIndex, bool invokeDisplayEvents, bool invokeTabMoveEvents )
		{
			Debug.Assert(tab.TabGroup == null || !invokeDisplayEvents, "'invokeDisplayEvents' is true but the tab is already part of a tab group!");

			// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
			// This seems like an existing bug where some calls to MoveTab are
			// preceeded by invoking TabMoving and others are not. We should
			// always do this if the tab was already displayed.
			//
			Debug.Assert(invokeDisplayEvents != invokeTabMoveEvents || invokeDisplayEvents == false, "We should probably be invoking one or the other. Review this to ensure this is correct.");

			// AS 4/21/05 BR03516
			// Added a check here in case someone does try to call this with an invalid tab group.
			//
			Debug.Assert(newTabGroup == null || !newTabGroup.HasTabGroups, "Invalid tabgroup. A valid tab group that does not contain other tab groups must be specified!");

			if (invokeTabMoveEvents)
			{
				// fire the tabmoving
				MdiTabMovingEventArgs movingArgs = new MdiTabMovingEventArgs(tab, newTabGroup, tabIndex);

				this.InvokeEvent(TabbedMdiEventIds.TabMoving, movingArgs);

				// just remove the empty tab group and leave
				if (movingArgs.Cancel)
					return false;
			}

			MdiTabGroup oldTabGroup = tab.TabGroup;

			this.SuspendLayout();

			MdiTabEventArgs displayArgs = null;

			try
			{
				if (oldTabGroup != null && oldTabGroup == newTabGroup)
				{
					this.RepositionTabInTabGroup(tab, tabIndex);
				}
				else
				{
					Debug.Assert( newTabGroup != null, "Cannot reposition a tab to a null 'MdiTabGroup'");

					if (oldTabGroup == null)	// remove it from the closed tabs
						this.HiddenTabs.Remove(tab);
					else	// or its containing tab group
					{
						// AS 5/29/03
						// Use a separate method to try and maintain
						// the index of the first displayed tab.
						//
						//oldTabGroup.Tabs.Remove(tab);
						this.RemoveTabGroupTabItem(oldTabGroup, tab);
					}

					// add it to the tab group
					// AS 4/18/05 BR03444
					//newTabGroup.Tabs.Add(tab);
					newTabGroup.Tabs.Insert(tabIndex, tab);

					// initialize the TabGroup member of the MdiTab
					tab.InitializeOwner(newTabGroup);

					// make sure there is a selected tab
					// AS 4/16/03
					// Allow the events to go through. I added a check
					// in the TabSelecting to not honor a cancel when
					// the associated tab is already active.
					//
					//this.EnsureTabIsSelected(newTabGroup, false);
					//this.EnsureTabIsSelected(oldTabGroup, false);
					this.EnsureTabIsSelected(newTabGroup, true);
					this.EnsureTabIsSelected(oldTabGroup, true);

					// now fire the display event if needed
					if (invokeDisplayEvents)
					{
						displayArgs = new MdiTabEventArgs(tab);

						this.InvokeEvent(TabbedMdiEventIds.TabDisplaying, displayArgs);
					}

					// remove any empty tab groups
					this.RemoveEmptyTabGroups();
				}
			}
			finally
			{
				// update the last active tab group
				this.CacheLastActiveTabGroup();

				// AS 5/25/05
				// If the active tab is moved we may need to change the maximized tab group.
				//
				this.VerifyMaximizedTabGroup();

				this.ResumeLayout(true);
			}

			// if we fired the before, fire the after
			if (displayArgs != null)
				this.InvokeEvent(TabbedMdiEventIds.TabDisplayed, displayArgs);

			// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
			// This seems like an existing bug where some calls to MoveTab are
			// preceeded by invoking TabMoving and others are not. We should
			// always do this.
			//
			if (invokeTabMoveEvents)
			{
				// invoke the tab moved
				MdiTabEventArgs movedArgs = new MdiTabEventArgs(tab);

				this.InvokeEvent(TabbedMdiEventIds.TabMoved, movedArgs);
			}

			return true;
		}
		#endregion //MoveTab

		#region RemoveTabFromTabGroup
		private void RemoveTabFromTabGroup( MdiTab tab )
		{
			// if there wasn't one there's nothing to do
			if (tab == null)
				return;

			// get the owning tab group - if there is one
			MdiTabGroup tabGroup = tab.TabGroup;

			// remove the tab from its tab group
			if (tabGroup != null)
			{
				// AS 5/29/03
				// Use a separate method to try and maintain
				// the index of the first displayed tab.
				//
				//tabGroup.Tabs.Remove(tab);
				this.RemoveTabGroupTabItem(tabGroup, tab);
			}

			// if the tab is disposed or the mdi child is disposed or removed,
			// discard the tab
			if (tab.Disposed				|| 
				tab.MdiChild == null		|| 
				!tab.MdiChild.IsMdiChild	||
				tab.MdiChild.IsDisposed		||
				tab.MdiChild.Disposing)
			{
				// unhook from the form
				tab.MdiChild = null;

				// remove the tab
				if (this.HiddenTabs.Contains(tab))
					this.HiddenTabs.Remove(tab);

				// remove the associated with its owner
				tab.InitializeOwner(null);

				// AS 1/5/04 WTB1271
				if (this.cachedActiveTab == tab)
					this.cachedActiveTab = null;

				// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
				if (this.lastActivatedTab == tab)
					this.lastActivatedTab = null;
			}
			else
			{
				// disassociate the tab from the tabgroup and associate
				// it with the tab manager
				tab.InitializeOwner(this);

				// add it to the closed tabs collection
				if (!this.HiddenTabs.Contains(tab))
					this.HiddenTabs.Add(tab);
			}

			if (tabGroup != null)
			{
				if (tabGroup.HasTabs)
				{
					// make sure that the tab group has a selected tab
					if (tabGroup.SelectedTab == null || tabGroup.SelectedTab == tab)
						this.EnsureTabIsSelected(tabGroup, false);
				}
				else	// it was in a tab group and that tab group doesn't have any more tabs
					this.RemoveEmptyTabGroups();
			}

			// store the new active tab group
			this.CacheLastActiveTabGroup();
		}
		#endregion //RemoveTabFromTabGroup

		#region CreateTabsForForms
		private void CreateTabsForForms( Form[] forms )
		{
			// create an mdi tab for each form
			MdiTab[] tabs = this.CreateMdiTabs(forms);

			if (tabs == null || tabs.Length == 0)
				return;

			// add the tabs to the collection
			for(int i = 0; i < tabs.Length; i++)
			{
				MdiTab tab = tabs[i];

				// if this is a custom tab, initialize the persisted info
				// with the serialized value
				if (tab.IsCustomTab)
					tab.PersistedInfo = tab.CustomMdiTab.UniqueId;

				// initialize the TabGroup member of the MdiTab
				tab.InitializeOwner(this);

				// add it to the closed tabs first
				this.HiddenTabs.Add(tab);

				// get the form it is referencing
				Form mdiChild = tab.MdiChild;
			}

			// now, fire the InitializeTab event.
			// we want to fire the event after adding all the tabs
			// since we don't want a programmer asking for the active tab in
			// the InitializeTab event causing the tab to get created again
			// while we are adding the tabs
			for(int i = 0; i < tabs.Length; i++)
			{
				// invoke the initialize tab event
				MdiTabEventArgs args = new MdiTabEventArgs(tabs[i]);

				this.InvokeEvent( TabbedMdiEventIds.InitializeTab, args );
			}

			// initialize the tab form settings
			if (this.Enabled)
			{
				foreach(MdiTab tab in tabs)
					this.InitializeTabFormSettings(tab);
			}

			// now add any visible tabs to groups
			foreach(MdiTab tab in tabs)
			{
				// also check to see if the tab group is still null
				// since it may have been made visible during
				// the settings initialization
				if (tab.IsFormVisible && tab.TabGroup == null)
					this.AddTabToTabGroup(tab);
			}
		}
		#endregion //CreateTabsForForms

		#region RemoveEmptyTabGroups
		private void RemoveEmptyTabGroups()
		{
			if (!this.HasTabGroups)
				return;

			bool groupWasRemoved = false;

			try
			{
				
				bool wereGroupsMoved = false;

				do
				{
					#region RemoveEmptyTabGroups
					MdiTabGroup tabGroup = UltraTabbedMdiManager.GetLastLeafTabGroup(this.TabGroups);

				while (tabGroup != null)
				{
					if (tabGroup.HasTabs)
						tabGroup = tabGroup.PreviousLeafTabGroup;
					else
					{
						// we have a leaf tab group without tabs
						// walk up until we find a tab group that
						// has tabs or null
						MdiTabGroup groupToRemove = tabGroup;
						MdiTabGroup parentGroup = groupToRemove.Parent;

						while(parentGroup != null)
						{
							if (parentGroup.HasTabsResolved)
								break;

							groupToRemove = parentGroup;
							parentGroup = parentGroup.Parent;
						}

						// if this is the first group we are removing, then suspend the layout
						if (!groupWasRemoved)
						{
							groupWasRemoved = true;
							this.SuspendLayout();
						}

						wereGroupsMoved = true;

						// start the next iteration with the previous leaf
						tabGroup = groupToRemove.PreviousLeafTabGroup;

						// remove the topmost group we can
						this.RemoveEmptyTabGroup(groupToRemove);
					}
				}
					#endregion //RemoveEmptyTabGroups

					wereGroupsMoved = this.RemoveUnnecessaryGroupLevels();

					// keep removing while we find unnecessary groups
				} while(wereGroupsMoved);
					

				if (groupWasRemoved)
				{
					// make sure the active tab is selected
					this.EnsureActiveTabIsSelected(true);

					// make sure the extents are synchronized
					this.InitializeTabGroupExtents(true);
				}
			}
			finally
			{
				if (groupWasRemoved)
					this.ResumeLayout(true);
			}
		}
		#endregion //RemoveEmptyTabGroups

		#region RemoveEmptyTabGroup
		private void RemoveEmptyTabGroup( MdiTabGroup tabGroup )
		{
			// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
			this.RemoveEmptyTabGroup(tabGroup, false);
		}

		// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
		// Added an overload since there is no point in updating the extent
		// of a tabgroup when it and all its siblings will be removed.
		//
		private void RemoveEmptyTabGroup( MdiTabGroup tabGroup, bool isParentBeingRemoved )
		{
			if (tabGroup == null)
				return;

			// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
			//Debug.Assert( !tabGroup.HasTabs, "An 'MdiTabGroup' that has tabs is about to be removed");
			Debug.Assert( !tabGroup.HasTabsResolved, "An 'MdiTabGroup' that has tabs is about to be removed");

			// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
			if (tabGroup.HasTabGroups)
			{
				// remove the children first
				foreach(MdiTabGroup child in tabGroup.TabGroups)
					RemoveEmptyTabGroup(child, true);
			}

			// try to update the extent of the previous tab group
			// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.TabGroups.Count > 1)
			if (!isParentBeingRemoved && tabGroup.ParentCollection.Count > 1)
			{
				// try to use the previous tab group first
				MdiTabGroup adjacentTabGroup = this.TabGroupFromPosition(tabGroup, MdiTabGroupPosition.Previous);

				// otherwise try to use the next
				if (adjacentTabGroup == null || adjacentTabGroup == tabGroup)
					adjacentTabGroup = this.TabGroupFromPosition(tabGroup, MdiTabGroupPosition.Next);

				// if we got one, give it the additional extent
				if (adjacentTabGroup != null && adjacentTabGroup != tabGroup)
				{
					// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
					//adjacentTabGroup.InitializeExtent(tabGroup.Extent + adjacentTabGroup.Extent);
					adjacentTabGroup.InitializeExtent(tabGroup.ExtentForAdjustments + adjacentTabGroup.ExtentForAdjustments);
				}
			}

			// remove the tab group
			// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
			//this.TabGroups.Remove(tabGroup);
			tabGroup.ParentCollection.Remove(tabGroup);

			// AS 6/12/03
			// Make sure that the tab group is no longer associated with the 
			// tabbed mdi manager.
			//
			tabGroup.InitializeTabbedMdiManager(null);

			// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
			tabGroup.InitializeParentTabGroup(null);

			// dispose the associated tabgroup control...
			this.RemoveTabGroupControl(tabGroup);

			// dispose the associated splitter control...
			this.RemoveSplitterControl(tabGroup);

			// AS 1/5/04 WTB1271
			// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
			// Only do this for the parent being removed.
			//
			if (!isParentBeingRemoved)
				this.TabGroupManager.RemoveUnusedTabGroups();
		}
		#endregion //RemoveEmptyTabGroup
		
		// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
		#region RemoveUnnecessaryGroupLevels
		private bool RemoveUnnecessaryGroupLevels()
		{
			this.SuspendLayout();

			try
			{
				bool moveRootGroups = false;

				// we need to account for when there is only 1 root tab group
				// and that tab group has nested groups...
				if (this.TabGroups.Count == 1 && this.TabGroups[0].HasTabGroups)
				{
					MdiTabGroup oldParent = this.TabGroups[0];
					int itemsToMove = oldParent.TabGroups.Count;

					// we're going to move all the children of this one group up to the manager
					for(int j = itemsToMove - 1; j >= 0; j--)
					{
						// reparent the tabgroup
						this.ReparentTabGroup(oldParent.TabGroups[j], null, 1);
					}

					// clear the extent of the old root group since it is
					// still in the collection
					oldParent.InitializeExtent(0);

					// toggle the orientation to account for promoting to the root level
					this.Orientation = this.Orientation == Orientation.Vertical ? Orientation.Horizontal : Orientation.Vertical;

					// keep track that groups were moved
					moveRootGroups = true;
				}

				// make sure to call the helper routine first so we will
				// remove any unnecessary groups within even if we promoted
				// the root groups
				return this.RemoveUnnecessaryGroupLevelsHelper(null) || moveRootGroups;
			}
			finally
			{
				this.ResumeLayout(true);
			}
		}

		#region RemoveUnnecessaryGroupLevelsHelper
		private bool RemoveUnnecessaryGroupLevelsHelper(MdiTabGroup parent)
		{
			MdiTabGroupsCollection tabGroups = parent == null ? this.TabGroups : parent.TabGroups;

			int index = 0;

			bool wereGroupsMoved = false;

			while (index < tabGroups.Count)
			{
				MdiTabGroup tabGroup = tabGroups[index++];

				if (tabGroup.HasTabs)
					continue;

				if (tabGroup.HasTabGroups && tabGroup.TabGroups.Count == 1)
				{
					// the children of this group need to be promoted to here
					// and this group should be removed
					if (!tabGroup.TabGroups[0].HasTabGroups)
					{
						// if this group only contains a leaf group
						// then we need to just move this group up
						// and update its extent
						MdiTabGroup groupToMove = tabGroup.TabGroups[0];

						// reparent the tabgroup
						this.ReparentTabGroup(groupToMove, parent, index);

						// give it the extent of the new tabgroup
						groupToMove.InitializeExtent(tabGroup.Extent);

						// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
						if (this.IsActiveTabGroupMaximized && this.MaximizedTabGroup != null)
							groupToMove.CachePreMaximizedExtent(tabGroup.ExtentForAdjustments);
					}
					else
					{
						int itemsToMove = tabGroup.TabGroups[0].TabGroups.Count;

						for(int j = itemsToMove - 1; j >= 0; j--)
						{
							MdiTabGroup groupToMove = tabGroup.TabGroups[0].TabGroups[j];

							// if the tabgroup to which we are promoting this one shows
							// a splitter (and therefore excludes that width from the
							// extent of its children) then we need to update the extent of
							// the last tabgroup so that it includes this extra space
							// since it will be displaying a splitter after the move
							bool addSplitterWidth = this.TabGroupManager.NeedsSplitter(tabGroup)	&&
								!this.TabGroupManager.NeedsSplitter(groupToMove)					&&
								j == itemsToMove - 1;

							// reparent the tabgroup
							this.ReparentTabGroup(groupToMove, parent, index);

							if (addSplitterWidth)
							{
								groupToMove.InitializeExtent(groupToMove.Extent + this.SplitterWidth);

								// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
								if (this.IsActiveTabGroupMaximized && this.MaximizedTabGroup != null)
									groupToMove.CachePreMaximizedExtent(groupToMove.ExtentForAdjustments + this.SplitterWidth);
							}
						}
					}

					// keep track that things were changed
					wereGroupsMoved = true;

					// clear the extent for the tabgroup to be removed
					tabGroup.InitializeExtent(0);

				}
				else
				{
					// see if descendants need to be removed and if so, update
					// the flag that they were
					if (RemoveUnnecessaryGroupLevelsHelper(tabGroup))
						wereGroupsMoved = true;
				}
			}

			return wereGroupsMoved;
		}

		#endregion //RemoveUnnecessaryGroupLevelsHelper

		#endregion //RemoveUnnecessaryGroupLevels

		// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
		#region ReparentTabGroup
		private void ReparentTabGroup(MdiTabGroup groupToMove, MdiTabGroup newParent, int newIndex)
		{
			// remove the group being split from its current location
			if (groupToMove.ParentCollection != null)
				groupToMove.ParentCollection.Remove(groupToMove);

			MdiTabGroupsCollection tabGroups = newParent == null ? this.TabGroups : newParent.TabGroups;

			// add it to the nested tab group's collection
			tabGroups.Insert(newIndex, groupToMove);

			// set its new parent
			groupToMove.InitializeParentTabGroup(newParent);
		}
		#endregion //ReparentTabGroup

		#region RepositionTabInTabGroup
		private void RepositionTabInTabGroup( MdiTab tab, int newIndex )
		{
			Debug.Assert( tab != null, "A  null 'MdiTab' cannot be repositioned");

			if (tab == null)
				return;

			// if its not in a tab group...
			if (tab.TabGroup == null)
				return;

			// if its already there, leave
			if ( ((ITabItem)tab).Index == newIndex )
				return;

			// AS 5/29/03
			// Instead of removing the tab and using the new index
			// we should really use the Reposition method of the 
			// collection so we don't get the same issue as WTB968.
			//
			// // remove it from the tab group
			//tab.TabGroup.Tabs.Remove(tab);
			//
			// // reinsert it at the appropriate spot
			//tab.TabGroup.Tabs.Insert(newIndex, tab);
			tab.TabGroup.Tabs.Reposition(tab, newIndex);

			// MRS 4/13/04 - Accessibility
			MdiTabGroupControl groupControl = tab.TabGroup.Manager.GetTabGroupControl(tab.TabGroup);
			if (groupControl != null)
				groupControl.AccessibilityNotifyClientsInternal(AccessibleEvents.Reorder, -1);
		}
		#endregion //RepositionTabInTabGroup

		#region SplitTabGroup
		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		// Allow the orientation to be specified, which is then used to determine
		// if we need to create a nested tab group.
		//
		//private void SplitTabGroup( MdiTab tabToAdd, MdiTabGroup groupToSplit, RelativePosition relation )
		private void SplitTabGroup( MdiTab tabToAdd, MdiTabGroup groupToSplit, RelativePosition relation, Orientation orientation )
		{
			if (!tabToAdd.IsFormVisible)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_35"));

			if (this.TabGroups.Count == 0)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_46"));

			if (!this.CanCreateNewGroup)
			{
				// AS 3/15/05 
				// We should not throw an exception if the tabgroup only contains
				// one tab and therefore will be removed because it doesn't 
				// contain any after the split.
				//
				if (tabToAdd.TabGroup == null || tabToAdd.TabGroup.Tabs.Count != 1)
					throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_47"));
			}

			#region Setup
			// first figure out which tab group we will be splitting
			// AS 6/3/03
			// Use the new Index property
			//int newGroupIndex = this.TabGroups.IndexOf(groupToSplit);
			// AS 3/10/05 NA 2005 Vol 2 - Nested TabGroups
			//int newGroupIndex = groupToSplit.Index;
			int newGroupIndex = groupToSplit == null ? 0 : groupToSplit.Index;

			// if the new group should be after the original, up the index
			if (relation == RelativePosition.After)
				newGroupIndex += 1;

			// AS 3/10/05 NA 2005 Vol 2 - Nested TabGroups [Start]

			// There are 3 things that can happen here:
			// (1) We could just create a new group for tabToAdd that is a sibling to the groupToSplit
			// (2) We could create a new group to contain the group for tabToAdd and the groupToSplit
			// (3) We could create a new group to contain all the root TabGroups and then make
			//	that a sibling of the newgroup created for tabToAdd.
			//
			// For #1, it would be what we did before and would happen if 
			// the orientation is not different from the siblings orientation and
			// there was a sibling specified (groupToSplit != null).
			//
			// For #2, groupToSplit != null but the orientation indicating
			// how grouptosplit is arranged differs from the new orientation.
			// We need to create a new nesting group that will be put into groupToSplit's 
			// place and move groupToSplit and the group for tabToAdd into the TabGroups
			// collection of that new nesting group.
			//
			// For #3, groupToSplit is null and the orientation is different
			// then the orientation specified. In this case, we need to create a
			// new nesting tabgroup and move all the TabGroups into that. This
			// nesting group would be added to the component's tabgroups
			// collection along with the tabgroup for tabToAdd.
			//

			// figure out the current orientation of the new sibling group
			Orientation parentOrientation = groupToSplit == null ? this.Orientation : groupToSplit.ParentOrientation;

			// a nested group is needed when the orientations differ
			// when a nested group is needed, we will create 2 new groups -
			// one to host the new tabgroup created for the tab being moved
			// and another to host the sibling tab(s)
			bool isNestedGroupNeeded = parentOrientation != orientation;

			if (isNestedGroupNeeded && this.TabGroups.Count == 1 && groupToSplit == this.TabGroups[0])
			{
				this.Orientation = orientation;
				isNestedGroupNeeded = false;
			}

			// if the group to split is null and the orientation is the same as
			// the current orientation then we should add this group at the beginning or
			// end of the component tabgroups
			//
			if (groupToSplit == null && !isNestedGroupNeeded)
			{
				// no nesting since the orientations are the same
				isNestedGroupNeeded = false;

				// just store the new index for the group
				if (relation == RelativePosition.Before)
					newGroupIndex = 0;
				else // after
					newGroupIndex = this.TabGroups.Count;
			}
			else if (isNestedGroupNeeded)
			{
				// if we're told to split a group and that group
				// contains nested groups, we will just add to that 
				// tab group since it doesn't make sense to split
				// that - also since the orientation is implied
				// based on the parent orientation, this would in 
				// essence toggle the orientation of the descendants
				if (groupToSplit != null && groupToSplit.HasTabGroups)
				{
					// don't try to create another nested group
					isNestedGroupNeeded = false;

					// just position it at the beginning or end of the group
					if (relation == RelativePosition.Before)
					{
						groupToSplit = groupToSplit.TabGroups[0];
						newGroupIndex = 0;
					}
					else
					{
						groupToSplit = groupToSplit.TabGroups[groupToSplit.TabGroups.Count - 1];
						newGroupIndex = groupToSplit.Index + 1;
					}
				}
				else
				{
					// when splitting a group, the nested group will be
					// either before or after the new group since the new group
					// will contain just the two groups
					newGroupIndex = relation == RelativePosition.Before ? 0 : 1;
				}
			}
			// AS 3/10/05 NA 2005 Vol 2 - Nested TabGroups [End]
			#endregion //Setup

			this.SuspendLayout();

			try
			{
				#region Create New MdiTabGroups

				
				MdiTabGroup newTabGroup;
				MdiTabGroup newNestedTabGroup = null; 

				if (isNestedGroupNeeded)
				{
					// first we need to create the group - depending on the groupToSplit
					// this will either contain the groupToSplit and the new group or
					// will contain the root level tabgroups (because we're introducing
					// a new root level.
					int newIndex = groupToSplit == null ? 0 : groupToSplit.Index;
					int newExtent = groupToSplit == null ? 
						// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
						//this.CommonMdiClientExtent : groupToSplit.Extent;
						this.CommonMdiClientExtent : groupToSplit.ExtentForAdjustments;

					// create the new nested group
					newNestedTabGroup = this.CreateNewTabGroup(newIndex, newExtent, 
						groupToSplit == null ? null : groupToSplit.Parent);

					// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
					if (this.IsActiveTabGroupMaximized && this.MaximizedTabGroup != null)
						newNestedTabGroup.CachePreMaximizedExtent(newExtent);

					// if we're introducing a new layer, then we need to change the orientation
					if (groupToSplit == null)
						this.Orientation = orientation;

					// move the other tab group(s)
					if (groupToSplit == null)
					{
						// we're reparenting the root groups. only 
						// go back to 1 since 0 is the new nested group
						for(int i = this.TabGroups.Count - 1; i >= 1; i--)
						{
							MdiTabGroup tabGroup = this.TabGroups[i];

							Debug.Assert(tabGroup != newNestedTabGroup, "Attempting to reparent the nested tab group to itself. The tab group must have been added in the wrong spot");

							if (tabGroup == newNestedTabGroup)
								continue;

							// reparent the group
							this.ReparentTabGroup(tabGroup, newNestedTabGroup, newNestedTabGroup.Index);
						}
					}
					else
					{
						// this nested group is going to contain the group to split
						// and the group for the tabToAdd so move the group to split now...

						this.ReparentTabGroup(groupToSplit, newNestedTabGroup, newNestedTabGroup.TabGroups.Count);
					}

					// now create the tabgroup for the new tab - it will either
					// be parented by the new nested group or by the component's
					// tab groups depending on whether groupToSplit was null or not
					newTabGroup = this.CreateNewTabGroup(newGroupIndex, 0, 
						groupToSplit == null ? null : newNestedTabGroup );
				}
				else if (groupToSplit == null)
					newTabGroup = this.CreateNewTabGroup(newGroupIndex, 0, null);
				else
					newTabGroup = this.CreateNewTabGroup(newGroupIndex, 0, groupToSplit.Parent);

				#endregion //Create New MdiTabGroups

				// store the current extent of the tab group
				// AS 3/24/05 NA 2005 Vol 2 - Nested TabGroups
				//int oldTabGroupExtent = groupToSplit.Extent;
				// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
				//int oldTabGroupExtent = groupToSplit == null ? 0 : groupToSplit.Extent;
				int oldTabGroupExtent = groupToSplit == null ? 0 : groupToSplit.ExtentForAdjustments;

				// fire the tab moving
				int tabIndex = 0;

				MdiTabMovingEventArgs movingArgs = new MdiTabMovingEventArgs(tabToAdd, newTabGroup, tabIndex);

				this.InvokeEvent(TabbedMdiEventIds.TabMoving, movingArgs);

				// just remove the empty tab group and leave
				if (movingArgs.Cancel)
				{
					this.RemoveEmptyTabGroups();
					return;
				}

				
				// account for a null group to split
				if (groupToSplit == null)
				{
					// if the group to split was null - and therefore we are either
					// splitting or adding to the root tab groups, we need to handle
					// the extents slightly differently
					if (isNestedGroupNeeded)
					{
						// if we created a new nested group to contain the
						// previous root tab groups...
						int newGroupExtent = this.AvailableMdiClientExtent / this.CalculateTabGroupRowsColsHelper(null, this.Orientation);

						MdiTabGroupsCollection tabGroups = new MdiTabGroupsCollection( new MdiTabGroup[] { newNestedTabGroup, newTabGroup } );
						int[] newExtents = new int[] { this.AvailableMdiClientExtent - newGroupExtent, newGroupExtent };
					
						// update the sizes
						this.UpdateTabGroupExtentsHelper(tabGroups, newExtents, true);
					}
					else
					{
						// we're adding to the root groups but we're giving it a proportion
						// of the entire area (i.e. we're not splitting a particular group)

						// if we created a new nested group to contain the
						// previous root tab groups...
						int newGroupExtent = this.AvailableMdiClientExtent / (this.CalculateTabGroupRowsColsHelper(null, this.Orientation) - 1);

						newTabGroup.InitializeExtent(newGroupExtent);

						// update the sizes
						this.InitializeTabGroupExtents(true);
					}
				}
				else
				// if the tab group that originally contained the tab was removed
				// give that item's space to the new group
				// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
				//if (!this.TabGroups.Contains(groupToSplit))
				if (groupToSplit.Manager == null)
				{
					MdiTabGroupsCollection tabGroups = new MdiTabGroupsCollection( new MdiTabGroup[] { newTabGroup } );
					int[] newExtents = new int[] { oldTabGroupExtent };

					// update the sizes
					this.UpdateTabGroupExtentsHelper(tabGroups, newExtents, true);
				}
				else
				{
					// figure out the extent of the new group - give it half the extent
					int newGroupExtent = oldTabGroupExtent / 2;

					MdiTabGroupsCollection tabGroups = new MdiTabGroupsCollection( new MdiTabGroup[] { groupToSplit, newTabGroup } );
					int[] newExtents = new int[] { oldTabGroupExtent - newGroupExtent, newGroupExtent };
					
					// update the sizes
					this.UpdateTabGroupExtentsHelper(tabGroups, newExtents, true);
				}

				this.MoveTab(tabToAdd, newTabGroup, tabIndex, false, false);

				// invoke the tab moved
				MdiTabEventArgs movedArgs = new MdiTabEventArgs(tabToAdd);

				this.InvokeEvent(TabbedMdiEventIds.TabMoved, movedArgs);
			}
			finally
			{
				// resume the layout and reposition the tab groups
				this.ResumeLayout(true);
			}
		}
		#endregion //SplitTabGroup

		#endregion //Create/Remove/Move Tabs & TabGroups

		#region TabGroup Extent related

		#region GetMinimumTabGroupExtent
		private int GetMinimumTabGroupExtent( MdiTabGroup tabGroup )
		{
			// AS 5/6/05
			// I noticed this while using the testproject for BR03666.
			// Basically, I'm enforcing a minimum extent of a group
			// to be based on the splitter width but that is not really
			// correct since that tab group could contain other tab groups
			// which themselves have splitters so we need to take
			// that into account as well.
			//
			//return this.SplitterWidth;
			int rowCols = 1;

			if (tabGroup != null && tabGroup.HasTabGroups)
				rowCols = this.CalculateTabGroupRowsColsHelper(tabGroup, tabGroup.ParentOrientation);

			return this.SplitterWidth * rowCols;
		}
		#endregion //GetMinimumTabGroupExtent

		#region UpdateTabGroupExtent
		internal void UpdateTabGroupExtent( MdiTabGroup tabGroup, int newExtent )
		{
			// if the user is doing this while in a begin update, we'll
			// take the value they specify and keep a flag that we need to fire the
			// after tab group resized
			if (this.IsUpdating)
			{
				this.invokeTabGroupResized = true;
				tabGroup.InitializeExtent(newExtent);
				return;
			}

			// keep a counter so we can tell when the tab group extent has been
			// modified
			this.currentTabGroupExtentVersion++;

			// get the index of the tab group that we are updating
			// AS 6/3/03
			// Use the new Index property
			//int index = this.TabGroups.IndexOf(tabGroup);
			int index = tabGroup.Index;

			// AS 5/27/05 BR04323
			//int groupCount = this.TabGroups.Count;
			int groupCount = tabGroup.ParentCollection.Count;

			Debug.Assert( groupCount >= 1, "There are no tab groups in the TabGroups collection" );
			Debug.Assert( index >= 0, "The tab group being modified is not in the collection." );

			if (groupCount == 1)
			{
				Debug.Assert(!this.HasNestedTabGroups, "There should never be only 1 root tab group that contains other nested tab groups!");

				// if there's only one, you can't change its extent...
				//tabGroup.InitializeExtent(this.AvailableMdiClientExtent);

				// get the info for the event
				int[] newExtents = new int[] {this.AvailableMdiClientExtent};
				MdiTabGroupsCollection tabGroups = new MdiTabGroupsCollection( new MdiTabGroup[] {tabGroup} );

				// update the new extents
				this.UpdateTabGroupExtentsHelper(tabGroups, newExtents, true);
			}
			else
			{
				// get the minimum for the tab group being modified
				int minTabGroupExtent = this.GetMinimumTabGroupExtent(tabGroup);

				// enforce minimum extent
				if (newExtent <= minTabGroupExtent)
					newExtent = minTabGroupExtent;

				// find out the change
				// e.g.
				// Previous		New		Delta
				// ========		====	=====
				// 50			100		50
				// 100			50		-50
				// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
				//int delta = newExtent - tabGroup.Extent;
				int delta = newExtent - tabGroup.ExtentForAdjustments;

				// reduce the adjacent group - either the group immediately
				// after this group... or the previous group when dealing with 
				// the last group
				// AS 6/3/03
				// Use the new Index property
				//int tabGroupIndex = this.TabGroups.IndexOf(tabGroup);
				int tabGroupIndex = tabGroup.Index;

				int affectedGroupIndex = tabGroupIndex == groupCount - 1 ? tabGroupIndex - 1 : tabGroupIndex + 1;

				// get that adjacent tab group
				// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
				//MdiTabGroup adjacentTabGroup = this.TabGroups[affectedGroupIndex];
				MdiTabGroup adjacentTabGroup = tabGroup.ParentCollection[affectedGroupIndex];

				// get that tab group's minimum extent
				int minAdjacentTabGroupExtent = this.GetMinimumTabGroupExtent(adjacentTabGroup);

				// get the extent of the adjacent tab group
				// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
				//int adjacentExtent = adjacentTabGroup.Extent;
				int adjacentExtent = adjacentTabGroup.ExtentForAdjustments;

				// change the adjacent tab group extent by the delta
				adjacentExtent -= delta;

				// account for minimum tab group extent of the adjacent tab group...
				if (adjacentExtent <= minAdjacentTabGroupExtent)
				{
					// put it back in
					// e.g if the resulting change to the adjacent group
					// results in a -20 extent, add -20 back to the newExtent
					newExtent += adjacentExtent;

					// set to the minimum
					adjacentExtent = minAdjacentTabGroupExtent;
				}

				// get the info for the event
				int[] newExtents = new int[] {newExtent, adjacentExtent};
				MdiTabGroupsCollection tabGroups = new MdiTabGroupsCollection( new MdiTabGroup[] {tabGroup, adjacentTabGroup} );
				
				// update the new extents
				this.UpdateTabGroupExtentsHelper(tabGroups, newExtents, true);
			}
		}
		#endregion //UpdateTabGroupExtent

		#region UpdateTabGroupExtentsHelper
		private bool UpdateTabGroupExtentsHelper( MdiTabGroupsCollection tabGroups, int[] newExtents, bool invokeEvents )
		{
			// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
			// Moved the bulk of the implementation here to a
			// new overload that would take a tabgroupextentinfocollection.
			// This overload should contain only siblings and will
			// be walked down to see if their extents are changing.
			//
			TabGroupExtentInfoCollection extentInfos = new TabGroupExtentInfoCollection(10);

			for(int i = 0; i < tabGroups.Count; i++)
				extentInfos.Add(tabGroups[i], newExtents[i]);

			if (this.IsActiveTabGroupMaximized && this.MaximizedTabGroup != null)
			{
				for(int i = 0; i < tabGroups.Count; i++)
					tabGroups[i].CachePreMaximizedExtent(newExtents[i]);
			}


			// then iterate the group and add in any child groups 
			// that have changed
			for(int i = 0; i < tabGroups.Count; i++)
			{
				TabGroupExtentInfo extentInfo = extentInfos[i];

				if (!extentInfo.TabGroup.HasTabGroups)
					continue;

				int childCommonExtent = extentInfo.NewExtent;

				// the sizable area for the 
				if (this.TabGroupManager.NeedsSplitter(extentInfo.TabGroup))
					childCommonExtent -= this.SplitterWidth;

				this.InitializeTabGroupExtentsHelper(
					extentInfos, 
					extentInfo.TabGroup.TabGroups, 
					this.CalculateCommonExtent(extentInfo.TabGroup.Parent), 
					childCommonExtent);
			}

			return this.UpdateTabGroupExtentsHelper(extentInfos, invokeEvents);
		}

		// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
		// Much of this implementation is the what used to be
		// in the other overload that took a collection and an array.
		//
		private bool UpdateTabGroupExtentsHelper( TabGroupExtentInfoCollection extentInfos, bool invokeEvents )
		{
			bool isResizeNeeded = false;

			// verify that a resize operation is in order
			// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
			//for(int i = 0; i < newExtents.Length; i++)
			foreach(TabGroupExtentInfo extentInfo in extentInfos)
			{
				// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
				//if (tabGroups[i].Extent != newExtents[i])
				if (extentInfo.IsExtentDifferent)
				{
					isResizeNeeded = true;
					break;
				}
			}

			if (!isResizeNeeded)
				return false;

			int tabGroupExtentVersion = this.currentTabGroupExtentVersion;
			bool cancelled = false;

			if (invokeEvents)
			{
				// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
				//MdiTabGroupResizingEventArgs beforeArgs = new MdiTabGroupResizingEventArgs(tabGroups, newExtents);
				MdiTabGroupResizingEventArgs beforeArgs = extentInfos.CreateTabGroupResizingArgs();

				// fire the before event
				this.InvokeEvent( TabbedMdiEventIds.TabGroupResizing, beforeArgs );

				cancelled = beforeArgs.Cancel;
			}

			// if its cancelled or the tab group extents have changed during
			// the before, don't change anything
			if (cancelled || this.currentTabGroupExtentVersion != tabGroupExtentVersion)
				return false;

			// suspend the layout while initializing the extents
			this.SuspendLayout();

			try
			{
				
				foreach(TabGroupExtentInfo extentInfo in extentInfos)
					extentInfo.TabGroup.InitializeExtent(extentInfo.NewExtent);

				// make sure that the tab groups are sized to fill
				// the available area
				this.VerifyTabGroupExtents();
			}
			finally
			{
				// update the size and position
				this.ResumeLayout(true);
			}

			if (invokeEvents)
			{
				MdiTabGroupResizedEventArgs afterArgs = new MdiTabGroupResizedEventArgs(tabGroups);

				this.InvokeEvent( TabbedMdiEventIds.TabGroupResized, afterArgs );
			}

			return true;
		}
		#endregion //UpdateTabGroupExtentsHelper

		#region InitializeTabGroupExtents
		private void InitializeTabGroupExtents(bool invokeResizeEvents)
		{
			this.InitializeTabGroupExtents(invokeResizeEvents, false);
		}

		private void InitializeTabGroupExtents(bool invokeResizeEvents, bool force)
		{
			

			if (this.suspendExtentInitialization)
				return;

			// if there are no tab groups or we are initializing, wait til later
			if (!this.HasTabGroups || this.IsInitializing)
				return;

			
			// if everything is allocated properly, then just exit
			if (!force && this.AreTabGroupExtentsAllocated())
				return;

			// we need a hashtable for the groups that have changed
			TabGroupExtentInfoCollection tabGroupExtentList = new TabGroupExtentInfoCollection();

			// start from the base tabgroups collection even
			// though the only change may be within a nested group
			this.InitializeTabGroupExtentsHelper(tabGroupExtentList, this.TabGroups, this.AvailableMdiClientExtent, this.CommonMdiClientExtent);

			this.UpdateTabGroupExtentsHelper( tabGroupExtentList, invokeResizeEvents);
		}

		// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
		private void InitializeTabGroupExtentsHelper(TabGroupExtentInfoCollection tabGroupExtentList, MdiTabGroupsCollection tabGroups, int sizableExtent, int commonExtent)
		{
			int groupCount = tabGroups.Count;
			int[] extents = new int[groupCount];
			int totalExtent = 0;

			// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
			int minimizedGroups = 0;
			int minimizedGroupsWithSplitter = 0;

			// AS 4/24/03 WTB861
			int actualTotalExtent = 0;

			// store all the extents
			for(int i = 0; i < groupCount; i++)
			{
				// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
				if (tabGroups[i].IsMinimized)
				{
					minimizedGroups++;

					if (this.TabGroupManager.NeedsSplitter(tabGroups[i]))
						minimizedGroupsWithSplitter++;
				}

				// AS 4/24/03 WTB861
				// When a tab group has an extent of 0,
				// it will never get allocated any of the 
				// available space when there is another with an extent 
				// greater than 0 so give everyone 1 extra pixel.
				//
				extents[i] = tabGroups[i].Extent + 1;
				totalExtent += extents[i];

				// AS 4/24/03 WTB861
				// keep track of the real total extent
				actualTotalExtent += tabGroups[i].Extent;
			}

			// find out how much area is available
			int mdiClientExtent = sizableExtent;

			// if something in this collection has changed...
			if (actualTotalExtent != mdiClientExtent)
			{
				// keep another member so we know we how much we have left to allocate
				int remainingExtent = mdiClientExtent;

				// we need to store the calculated new extents
				int[] newExtents = new int[groupCount];

				// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
				if (minimizedGroups > 0 && minimizedGroups < tabGroups.Count)
				{
					Debug.Assert(tabGroups.Count - minimizedGroups == 1, "If there are any minimized groups and not all are minimized, there should be only one non-minimized group.");

					int maximizedExtent = remainingExtent - (minimizedGroupsWithSplitter * this.SplitterWidth );
					int maximizedGroups = tabGroups.Count - minimizedGroups;

					// calculate that new extents
					for(int i = 0; i < groupCount; i++)
					{
						// do not allocate space to the minimized group that does not show a splitter
						if (tabGroups[i].IsMinimized)
							newExtents[i] = minimizedGroupsWithSplitter == 0 || !this.TabGroupManager.NeedsSplitter(tabGroups[i]) ? 0 : this.SplitterWidth;
						else
							newExtents[i] = maximizedExtent / maximizedGroups;
					}
				}
				else if (minimizedGroups > 0 && minimizedGroups == tabGroups.Count)
				{
					// if every sibling is minimized then no part of any will be shown
					newExtents.Initialize();
				}
				else
				{

					// calculate that new extents
					for(int i = 0; i < groupCount; i++)
					{
						int extent;

						if (totalExtent == 0)
							extent = 0;
						else
						{
							

							// find out how much to give to this tab group
							extent = (int)Math.Round(((extents[i] / (decimal)totalExtent) * mdiClientExtent));

							// if when processing the last group, it exceeds
							// the remaining, give it the remaining
							if (i == groupCount - 1 && extent > remainingExtent)
								extent = remainingExtent;
						}

						// initialize the extent
						newExtents[i] = extent;

						// remove the portion just allocated
						remainingExtent -= extent;
					}

					// AS 4/16/03 WTB817
					// If there's any remaining amount to allocate then dole it out here.
					// 
					if (remainingExtent > 0)
					{
						int percent = remainingExtent / groupCount;

						for(int i = 0; i < groupCount; i++)
						{
							int extent;

							if (i == groupCount - 1)
								extent = remainingExtent;
							else
								extent = percent;

							// initialize the extent
							newExtents[i] += extent;

							// remove the portion just allocated
							remainingExtent -= extent;
						}
					}
				}
	
				// store any tabgroup whose size has changed
				for(int i = 0; i < newExtents.Length; i++)
				{
					if (newExtents[i] != tabGroups[i].Extent)
						tabGroupExtentList.Add( tabGroups[i], newExtents[i] );
				}
			}

			foreach(MdiTabGroup tabGroup in tabGroups)
			{
				if (tabGroup.HasTabGroups)
				{
					TabGroupExtentInfo extentInfo = tabGroupExtentList[tabGroup];

					int childCommonExtent = extentInfo != null ?
						extentInfo.NewExtent : tabGroup.Extent;

					// the sizable area for the 
					if (this.TabGroupManager.NeedsSplitter(tabGroup))
						childCommonExtent -= this.SplitterWidth;

					int childSizableExtent = commonExtent;

					this.InitializeTabGroupExtentsHelper(tabGroupExtentList, tabGroup.TabGroups,
						childSizableExtent, childCommonExtent);
				}
			}
		}
		#endregion //InitializeTabGroupExtents

		#region VerifyTabGroupExtents
		/// <summary>
		/// Verifies that the sum of the extents of the tab group equals the available mdi client extent.
		/// </summary>
		private void VerifyTabGroupExtents()
		{
			
			if (this.AreTabGroupExtentsAllocated())
				return;

			this.SuspendLayout();

			try
			{
				this.InitializeTabGroupExtents(false);
			}
			finally
			{
				// resume the layout but do NOT process the sizing and positioning
				// this is left up to the caller - esp since this could be called
				// from within the PositionAndSizeTabGroups
				//
				this.ResumeLayout(false);
			}
		}
		#endregion //VerifyTabGroupExtents

		#region CalculateRemainingAvailableExtent
		
		#endregion //CalculateRemainingAvailableExtent

		// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
		#region AreTabGroupExtentsAllocated
		private bool AreTabGroupExtentsAllocated()
		{
			return this.AreTabGroupExtentsAllocatedHelper(this.TabGroups, this.AvailableMdiClientExtent);
		}

		private bool AreTabGroupExtentsAllocatedHelper(MdiTabGroupsCollection tabGroups, int availableExtent)
		{
			// first make a pass on this groups extent
			int totalExtent = 0;

			foreach(MdiTabGroup tabGroup in tabGroups)
				totalExtent += tabGroup.Extent;

			if (availableExtent != totalExtent)
				return false;

			// if it matches with the available extent,
			// then have any nested groups check their values
			foreach(MdiTabGroup tabGroup in tabGroups)
			{
				// if a tab group contains nested tab groups, make sure they are
				// completely allocated as well
				if (tabGroup.HasTabGroups)
				{
					if (!this.AreTabGroupExtentsAllocatedHelper(tabGroup.TabGroups, this.CalculateSizableExtent(tabGroup)))
						return false;
				}
			}

			return true;
		}
		#endregion //AreTabGroupExtentsAllocated

		#region CalculateTabGroupRowsColsHelper
		private int CalculateTabGroupRowsColsHelper(MdiTabGroup tabGroup, Orientation orientation)
		{
			if (tabGroup != null && !tabGroup.HasTabGroups)
				return 1;

			MdiTabGroupsCollection tabGroups = tabGroup == null ? this.TabGroups : tabGroup.TabGroups;

			int count = 0;

			foreach(MdiTabGroup child in tabGroups)
			{
				if (child.ParentOrientation == orientation)
					count += this.CalculateTabGroupRowsColsHelper(child, orientation);
				else
					count = Math.Max(count, this.CalculateTabGroupRowsColsHelper(child, orientation));
			}

			return count;
		}
		#endregion //CalculateTabGroupRowsColsHelper

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		#region RefreshMaximizedTabGroupExtents
		private void RefreshMaximizedTabGroupExtents()
		{
			if (this.MaximizedTabGroup != null	&&
				!this.IsInitializing)
			{
				this.SuspendLayout();

				try
				{
					// clear the extent of each group
					foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
						tabGroup.InitializeExtent(-1);

					this.DirtyTabGroups(TabbedMdiPropertyIds.Extent);
					this.InitializeTabGroupExtents(false, true);
				}
				finally
				{
					this.ResumeLayout(true);
				}
			}
		}
		#endregion //RefreshMaximizedTabGroupExtents

		#endregion //TabGroup Extent related

		#region Miscellaneous

		#region CreateMdiTabs
		private MdiTab[] CreateMdiTabs( ICollection forms )
		{
			if (forms == null || forms.Count == 0)
				return null;

			int count = 0;

			// first find out how many we're adding
			foreach( object item in forms )
			{
				// increment the counter
				if ( item is Form )
					count++;
			}

			// if there are no forms, just bail
			if (count == 0)
				return null;

			// then create a tab array of that size
			MdiTab[] tabs = new MdiTab[count];

			count = 0;

			// now create an 'MdiTab' for each form
			foreach( object item in forms )
			{
				if ( !(item is Form) )
					continue;

				// use the protected virtual method so a programmer
				// can use their derived one
				tabs[count] = this.CreateMdiTab(item as Form);

				count++;
			}

			return tabs;
		}
		#endregion //CreateMdiTabs

		#region GetBaseFont
		private Font GetBaseFont(MdiTabGroup tabGroup)
		{
			MdiTabGroupControl tabGroupControl = this.TabGroupControls[tabGroup] as MdiTabGroupControl;

			if (tabGroupControl != null)
				return tabGroupControl.Font;

			if (this.MdiParent != null)
				return this.MdiParent.Font;

			if (this.mdiClient != null)
				return this.mdiClient.Font;

			return Control.DefaultFont;
		}
		#endregion //GetBaseFont

		#region InitializeMdiTabForms
		private void InitializeMdiTabForms( bool restore )
		{
			// AS 1/7/05 BR01537
			// Suspend the layout while initializing since
			// we don't want to process size changes, etc.
			//
			try
			{
				this.SuspendLayout();

				this.InitializeMdiTabFormsHelper(restore);
			}
			finally
			{
				this.ResumeLayout(false);
			}
		}

		// AS 1/7/05 BR01537
		// Moved from InitializeMdiTabForms to a helper method
		// so it could be more easily called from a try finally
		//
		private void InitializeMdiTabFormsHelper(bool restore)
		{
			if (restore)
			{
				this.SetRedraw(false);

				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup tabGroup in this.TabGroups)
				foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
				{
					foreach(MdiTab tab in tabGroup.Tabs)
					{
						if (tab.IsCustomTab)
							tab.CustomMdiTab.IsMdiTab = false;

						tab.RestoreTabFormSettings();
					}
				}

				foreach(MdiTab tab in this.HiddenTabs)
				{
					if (tab.IsCustomTab)
						tab.CustomMdiTab.IsMdiTab = false;

					tab.RestoreTabFormSettings();
				}

				this.SetRedraw(true);
			}
			else
			{
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup tabGroup in this.TabGroups)
				foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
				{
					foreach(MdiTab tab in tabGroup.Tabs)
					{
						this.InitializeTabFormSettings(tab);
					}
				}

				foreach(MdiTab tab in this.HiddenTabs)
					this.InitializeTabFormSettings(tab);
			}
		}
		#endregion //InitializeMdiTabForms

		#region SynchronizeSplitterProperties
		private void SynchronizeSplitterProperties()
		{
			if (this.IsInitializing || !this.Enabled)
				return;

			
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
			{
				SplitterBarControl splitter = this.SplitterControls[tabGroup] as SplitterBarControl;

				if (splitter != null)
				{
					splitter.BorderStyle = this.SplitterBorderStyleResolved;
					splitter.Orientation = tabGroup.ParentOrientation;
					// AS 5/5/06 AppStyling
					//splitter.ResolveAppearanceCallback = this.SplitterAppearanceCallback;
					splitter.ResolveAppearanceCallback = splitter.Orientation == Orientation.Vertical
						? this.SplitterAppearanceCallbackVert
						: this.SplitterAppearanceCallbackHorz;

					// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
					splitter.Enabled = this.MaximizedTabGroup == null;
				}
			}
		}
		#endregion //SynchronizeSplitterProperties

		#region TabGroupFromPosition
		private MdiTabGroup TabGroupFromPosition( MdiTabGroup tabGroup, MdiTabGroupPosition relativePosition )
		{
			MdiTabGroup newGroup = null;

			if (this.HasTabGroups)
			{
				Debug.Assert(tabGroup.ParentCollection != null, "Unable to access the parent collectioN!");

				switch(relativePosition)
				{
					case MdiTabGroupPosition.First:
						// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
						//newGroup = this.TabGroups[0];
						newGroup = tabGroup.ParentCollection[0];
						break;
					case MdiTabGroupPosition.Last:
						// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
						//newGroup = this.TabGroups[this.TabGroups.Count - 1];
						newGroup = tabGroup.ParentCollection[tabGroup.ParentCollection.Count - 1];
						break;
					case MdiTabGroupPosition.Next:
					case MdiTabGroupPosition.Previous:
						int currentTabGroupIndex = 0;

						// first we need to get the index of the current tab group
						if (tabGroup != null)
						{
							// AS 6/3/03
							// Use the new Index property
							//currentTabGroupIndex = this.TabGroups.IndexOf(tabGroup);
							currentTabGroupIndex = tabGroup.Index;
						}

						int newTabGroupIndex;

						if (relativePosition == MdiTabGroupPosition.Next)
							newTabGroupIndex = currentTabGroupIndex + 1;
						else
							newTabGroupIndex = currentTabGroupIndex - 1;
						
						if (newTabGroupIndex < 0)
							newTabGroupIndex = 0;
						// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
						//else if (newTabGroupIndex > this.TabGroups.Count - 1)
						//	newTabGroupIndex = this.TabGroups.Count - 1;
						else if (newTabGroupIndex > tabGroup.ParentCollection.Count - 1)
							newTabGroupIndex = tabGroup.ParentCollection.Count - 1;

						// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
						//newGroup = this.TabGroups[newTabGroupIndex];
						newGroup = tabGroup.ParentCollection[newTabGroupIndex];
						break;
				}
			}

			return newGroup;
		}
		#endregion //TabGroupFromPosition

		#region GetTabFromForm
		private MdiTab GetTabFromForm(Form form, bool verifyFormIsInitialized, bool verifyTabGroup , bool createIfNull)
		{
			MdiTab tab = this.GetTabFromForm(form);

			if (tab != null)
			{
				if (verifyFormIsInitialized && !tab.IsFormInitialized)
					this.InitializeTabFormSettings(tab);

				bool initializingTabs = this.EventManager.InProgress(TabbedMdiEventIds.InitializeTab);

				if (verifyTabGroup && !initializingTabs)
				{
					if (tab.IsFormVisible && tab.TabGroup == null)
						this.AddTabToTabGroup(tab);
					else if (!tab.IsFormVisible && tab.TabGroup != null)
						this.RemoveTabFromTabGroup(tab);
				}		
			}
			else if (createIfNull && this.MdiParent != null)
			{
				// AS 6/11/03
				// We need a flag since we don't want to create a tab
				// for a form if it does not yet exist while we are
				// restoring the tabs.
				//
				// AS 4/30/03
				// We shouldn't be creating mdi tab objects
				// during the RestoreTab event.
				//
				//if (!this.IsRestoringTabs)
				if (!this.IsRestoringTabs && !restoringTabGroup)
				{
					// as long as this is child of the mdi parent
					// MD 1/4/07 - FxCop
					// Use the safe MdiParent getter
					//if (form != null && form.IsMdiChild && form.MdiParent == this.MdiParent &&
					if ( form != null && form.IsMdiChild && Utilities.GetMdiParent( form ) == this.MdiParent &&
						!form.IsDisposed && !form.Disposing && !this.IsDisposing)
					{
						this.CreateTabsForForms( new Form[] {form} );
					}
				}
			}

			return tab;
		}

		private MdiTab GetTabFromForm(Form form)
		{
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.TabGroups)
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
			{
				foreach(MdiTab tab in tabGroup.Tabs)
				{
					if (tab.Form == form)
						return tab;
				}
			}

			foreach(MdiTab tab in this.HiddenTabs)
			{
				if (tab.Form == form)
					return tab;
			}

			return null;
		}
		#endregion //GetTabFromForm

		#region CacheLastActiveTabGroup
		private void CacheLastActiveTabGroup()
		{
			MdiTab tab = this.ActiveTab;

			// cache the last active tab group
			if (tab != null				&& 
				tab.TabGroup != null	&& 
				tab.TabGroup != this.lastActiveTabGroup)
				this.lastActiveTabGroup = tab.TabGroup;
			else // AS 1/5/04 WTB1271
				if (tab == null)
			{
				if (this.lastActiveTabGroup != null  &&
					// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
					//!this.TabGroups.Contains(this.lastActiveTabGroup))
					(this.lastActiveTabGroup.Manager == null || this.lastActiveTabGroup.Index < 0))
				{
					this.lastActiveTabGroup = null;
				}
			}
		}
		#endregion //CacheLastActiveTabGroup

		#region SuspendLayout / ResumeLayout
		private int SuspendLayout()
		{
			// increment and return
			return ++this.suspendLayoutRequests;
		}

		private int ResumeLayout(bool positionControls)
		{
			Debug.Assert(this.suspendLayoutRequests != 0, "A resume layout was invoked without previously invoking a SuspendLayout.");

			if (this.suspendLayoutRequests == 0)
				return 0;

			this.suspendLayoutRequests--;

			// only position the controls when we have gotten the
			// suspend requests back down to 0
			if (this.suspendLayoutRequests == 0 && positionControls)
				this.PositionAndSizeTabGroups();

			return this.suspendLayoutRequests;
		}
		#endregion //SuspendLayout / ResumeLayout

		#region GetTabGroupFromSplitter
		private MdiTabGroup GetTabGroupFromSplitter(SplitterBarControl splitter)
		{
			if (splitter != null)
			{
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				//foreach(MdiTabGroup item in this.TabGroups)
				foreach(MdiTabGroup item in this.TabGroups.GetNestedTabGroups(false))
				{
					if (this.SplitterControls[item] == splitter)
						return item;
				}
			}

			return null;
		}
		#endregion //GetTabGroupFromSplitter

		#region CachedMdiClientSize
		private void CachedMdiClientSize()
		{
			this.lastMdiClientSize = this.MdiClientSize;
		}
		#endregion //CachedMdiClientSize

		#region ResetComponent
		private void ResetComponent( bool deserializing )
		{
			this.SuspendLayout();

			try
			{
				this.ResetAllowHorizontalTabGroups();
				this.ResetAllowVerticalTabGroups();
				this.ResetAppearance();
				this.ResetAppearances();
				this.ResetCursor();

				if (!deserializing)
					this.ResetImageList();

				this.ResetImageSize();
				this.ResetImageTransparentColor();
				this.ResetMaxTabGroups();
				this.ResetOrientation();
				this.ResetShowToolTips();
				this.ResetSplitterAppearance();
				this.ResetSplitterBorderStyle();
				this.ResetSplitterWidth();
				this.ResetTabGroups();
				this.ResetTabGroupSettings();
				this.ResetTabSettings();
				this.ResetUseMnemonics();

				// AS 5/1/03 WTB880
				this.ResetBorderColor();
				this.BorderStyle = BorderStyleDefault;

				// AS 3/7/05 NA 2005 Vol 2 - Nested TabGroups
				this.ResetAllowNestedTabGroups();

				// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
				this.ResetMaximizedTabGroupDisplayStyle();
				this.ResetIsActiveTabGroupMaximized();
				this.ResetAllowMaximize();

				// reset the base properties as well
				base.AlphaBlendMode = AlphaBlendMode.Optimized;
				// AS 3/20/06 AppStyling
				//base.FlatMode = false;
				//base.SupportThemes = true;
				base.UseFlatMode = DefaultableBoolean.Default;
				base.UseOsThemes = DefaultableBoolean.Default;

				// MD 11/30/07 - Bug 1271
				// These should have been reset here
				this.ResetViewStyle();
				this.TabNavigationMode = TabNavigationModeDefault;

				// do this last since it means we activate the ui
				base.Enabled = true;
			}
			finally
			{
				this.ResumeLayout(true);
			}
		}
		#endregion //ResetComponent

		// AS 10/30/03 WTB1214
		// See the SetRedraw method for a full explanation of
		// why this is needed. Essentially this method will
		// tell the focused control that it doesn't have focus
		// and then that it does.
		//
		#region ReactivateFocusedControl
		private void ReactivateFocusedControl(Control control)
		{
			if (control == null || !control.ContainsFocus)
				return;

			if (control.Focused)
			{
				using (FocusHelper fh = new FocusHelper())
					fh.ToggleFocus(control);
			}

			foreach(Control child in control.Controls)
			{
				if (child.Focused || child.ContainsFocus)
				{
					ReactivateFocusedControl(child);
					break;
				}
			}
		}
		#endregion //ReactivateFocusedControl

		#region SetRedraw
		private void SetRedraw( bool redrawOn )
		{
			if (this.mdiClient == null || !this.hasUnmanagedCodeRights)
				return;

			// MD 1/4/07 - FxCop
			// See comment on assert below
			//SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);

			try
			{
				// MD 1/4/07 - FxCop
				// This is not needed due to the SuppressUnmanagedCodeSecurityAttribute on the NativeWindowMethods class
				//perm.Assert();

				NativeWindowMethods.SetRedraw(this.mdiClient, redrawOn, redrawOn);

				// AS 10/30/03 WTB1214
				// The editors use the Enter event to put themselves into
				// edit mode. The problem is that we set the WM_REDRAW of 
				// the mdi client to false to prevent the titlebar from 
				// displaying and hiding (this is a bug that occurs when
				// manipulating the formborderstyle). So, redraw is 
				// false when the editor gets its Enter event and it 
				// cannot force itself into edit mode (or assumes it
				// can't because the textbox does not take focus).
				// The only thing I can do to not break the flicker
				// issue is to use the protected InvokeGotFocus/
				// InvokeLostFocus to "toggle" the focus state of 
				// the focused control when we turn redraw back on.
				// I think we're safe in doing this in general esp
				// since the .net docs say not to rely upon those
				// events.
				//
				if (redrawOn && this.mdiClient != null)
					this.ReactivateFocusedControl(this.mdiClient);
			}
			catch( System.Security.SecurityException )
			{
				this.hasUnmanagedCodeRights = false;
			}
		}
		#endregion //SetRedraw

		#region UpdateBorderStyle
		private void UpdateBorderStyle()
		{
			if (!this.IsMdiClientSubclassed)
				return;

			
			this.mdiClientSubclasser.VerifyBorderStyle();
		}

		
		#endregion //UpdateBorderStyle

		// AS 5/29/03 WTB1012
		// Try to prevent flicker when showing a hidden mdi child form. 
		// For some reason, the mdi child handle is created and destroyed
		// several times and if we don't turn off mdiclient redrawing, you'll
		// see the title bar of the form appear and disappear.
		//
		#region DisableMdiClientRedraw
		private void DisableMdiClientRedraw()
		{
			if (this.mdiClient != null			&&
				this.mdiClient.IsHandleCreated	&&
				!this.mdiClient.IsDisposed		&&
				!this.mdiClient.Disposing)
			{
				NativeWindowMethods.SetRedraw(this.mdiClient, false, false);
			}
		}
		#endregion //DisableMdiClientRedraw

		#region DelayedRedraw
		private void DelayedRedraw()
		{
			if ( !this.WaitingForDelayedRedraw )
			{
				if (this.MdiParent == null || !this.MdiParent.IsHandleCreated )
				{
					this.hookedIdle = true;
					Application.Idle += new EventHandler(this.OnApplicationIdle);
				}
				else
					this.delayedRedraw = this.MdiParent.BeginInvoke(new MethodInvoker(this.DelayedMdiClientRedraw));
			}
		}
		#endregion //DelayedRedraw

		// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
		#region RefreshSplitterIntersections
		private void RefreshSplitterIntersections()
		{
			// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
			UIElementBorderStyle borderStyle = this.SplitterBorderStyleResolved;
			int splitterBorderWidth = DrawUtility.CalculateBorderWidths(borderStyle, Border3DSide.All, null).Left;
			bool isSplitterIntersectionNeeded = this.HasNestedTabGroups && splitterBorderWidth > 0;
			int splitterInset;

			switch(borderStyle)
			{
				case UIElementBorderStyle.Solid:
				case UIElementBorderStyle.Dashed:
				case UIElementBorderStyle.Dotted:
					splitterInset = -splitterBorderWidth;
					break;
				default:
					splitterInset = 0;
					break;
			}

			// then apply the position
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//foreach(MdiTabGroup tabGroup in this.TabGroups)
			foreach(MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(false))
			{
				// get and position the splitter first
				SplitterBarControl splitterControl = this.SplitterControls[tabGroup] as SplitterBarControl;

				if (splitterControl != null)
				{
					splitterControl.RemoveIntersections();

					if (!isSplitterIntersectionNeeded)
						continue;

					// get the location of the splitter
					Rectangle splitterRect = this.TabGroupManager.GetSplitterControlRectangle(tabGroup);

					// we need to expand the splitterRect to figure out the intersection
					if (splitterControl.Orientation == Orientation.Vertical)
						splitterRect.Inflate( splitterInset, splitterBorderWidth);
					else
						splitterRect.Inflate( splitterBorderWidth, splitterInset );

					MdiTabGroup parent = tabGroup.Parent;
					MdiTabGroup previous = null;
					MdiTabGroup next = null;

					// Walk up the parent chain and notify any parent splitters
					// that this might intersect with about the intersection.
					while (parent != null && (previous == null || next == null))
					{
						if (parent.ParentOrientation != splitterControl.Orientation)
						{
							if (previous == null)
								previous = this.AddSplitterIntersection(parent, RelativePosition.Before, splitterRect);

							if (next == null)
								next = this.AddSplitterIntersection(parent, RelativePosition.After, splitterRect);
						}

						parent = parent.Parent;
					}
				}
			}
		}
		#endregion //RefreshSplitterIntersections

		// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
		#region AddSplitterIntersection
		private MdiTabGroup AddSplitterIntersection(MdiTabGroup tabGroup, RelativePosition relativePosition, Rectangle splitterRect)
		{
			MdiTabGroup relative;
			
			if (relativePosition == RelativePosition.Before)
			{
				relative = this.TabGroupFromPosition(tabGroup, MdiTabGroupPosition.Previous);

				if (relative == tabGroup)
					return null;
			}
			else
			{
				if (!this.TabGroupManager.NeedsSplitter(tabGroup))
					return null;

				relative = tabGroup;
			}

			Rectangle relativeSplitterRect = this.TabGroupManager.GetSplitterControlRectangle( relative );

			if (!relativeSplitterRect.IsEmpty && splitterRect.IntersectsWith(relativeSplitterRect))
			{
				SplitterBarControl relativeSplitter = this.SplitterControls[relative] as SplitterBarControl;

				relativeSplitter.AddIntersection(splitterRect);
			}

			return relative;
		}
		#endregion //AddSplitterIntersection

		// AS 5/6/05 BR03666
		#region InvalidateControls
		private void InvalidateControls(bool dirtyChildElements, bool invalidate, bool splitterControls, bool tabGroupControls)
		{
			if (tabGroupControls)
			{
				foreach(MdiTabGroupControl tabGroupControl in this.TabGroupControls.Values)
				{
					UIElement element = ((IUltraControlElement)tabGroupControl).MainUIElement;

					if (element != null)
					{
						if (dirtyChildElements)
							element.DirtyChildElements(invalidate && element.Control.IsHandleCreated && !element.IsDrawing);
						else if (invalidate && element.Control.IsHandleCreated)
							element.Invalidate(false);
					}
				}
			}

			if (splitterControls)
			{
				foreach(SplitterBarControl splitterBarControl in this.SplitterControls.Values)
				{
					UIElement element = ((IUltraControlElement)splitterBarControl).MainUIElement;

					if (element != null)
					{
						if (dirtyChildElements)
							element.DirtyChildElements(invalidate && element.Control.IsHandleCreated && !element.IsDrawing);
						else if (invalidate && element.Control.IsHandleCreated)
							element.Invalidate(false);
					}
				}
			}
		}
		#endregion //InvalidateControls

		#endregion //Miscellaneous

		#region Menu related

		#region InitializeTabContextMenu
		private void InitializeTabContextMenu( MdiTab tab, MdiTabContextMenu menuType )
		{
			// clear the context menu before starting
			this.TabContextMenu.MenuItems.Clear();

			if (tab == null || tab.TabGroup == null)
				return;

			#region Setup

			

			// start by getting the groups to which it may move
			MdiTabGroup tabGroup = tab.TabGroup;
			MdiTabGroup prevTabGroup = tabGroup.PreviousLeafTabGroup;
			MdiTabGroup nextTabGroup = tabGroup.NextLeafTabGroup;
			
			bool canCreateHorizontalGroup = this.CanCreateHorizontalGroup;
			bool canCreateVerticalGroup = this.CanCreateVerticalGroup;

			bool canMoveNext = nextTabGroup != null;
			bool canMovePrevious = prevTabGroup != null;
			bool canClose = tab.SettingsResolved.AllowClose;

			// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
			bool canMaximize = this.AllowMaximize;

			// AS 4/1/03
			// Changed so that this condition is used for the reposition context
			// menu as well since that is what visual studio seems to do.
			//
			// the default menu type will not allow you to 
			// create a new group if the tab group only has 1 tab
			//if (menuType == MdiTabContextMenu.Default && tabGroup.Tabs.Count == 1)
			if (tabGroup.Tabs.Count == 1)
			{
				canCreateVerticalGroup = false;
				canCreateHorizontalGroup = false;
			}

			// added an additional check to use the same criteria as used in a drag
			// operation

			// if the tab cannot be dragged to another tab group, you should not
			// be able to do so with the context menu
			if (tab.SettingsResolved.AllowDrag != MdiTabDragStyle.WithinAndAcrossGroups)
			{
				canMoveNext = false;
				canMovePrevious = false;
				canCreateVerticalGroup = false;
				canCreateHorizontalGroup = false;
			}

			// if we had determined that the tab can be moved to the next group
			// make sure the next group allows drops
			if (canMoveNext && !nextTabGroup.SettingsResolved.AllowDrop)
				canMoveNext = false;

			// if we had determined that the tab can be moved to the previous group
			// make sure the previous group allows drops
			if (canMovePrevious && !prevTabGroup.SettingsResolved.AllowDrop)
				canMovePrevious = false;

			#endregion //Setup

			switch(menuType)
			{
				case MdiTabContextMenu.Default:
				{
					// Menu Structure:
					// ===============
					// Close
					// -------------
					// New Horizontal Tab Group
					// New Vertical Tab Group
					// Move To Next Tab Group
					// Move To Previous Tab Group
					// --------------
					// Maximize
					//
					if (canClose)
					{
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.Close) );

						MenuItem menuItemSep = this.GetMenuItem(MdiTabMenuItems.CloseSeparator);

						if (canCreateHorizontalGroup || canCreateVerticalGroup || canMoveNext || canMovePrevious)
							this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.CloseSeparator) );
					}

					if (canCreateHorizontalGroup)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.NewHorizontalGroup) );

					if (canCreateVerticalGroup)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.NewVerticalGroup) );

					if (canMoveNext)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.MoveToNextGroup) );

					if (canMovePrevious)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.MoveToPreviousGroup) );

					// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
					if (canMaximize)
					{
						if (this.TabContextMenu.MenuItems.Count > 0)
							this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.MaximizeSeparator) );

						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.Maximize) );
					}

					break;
				}
				case MdiTabContextMenu.Reposition:
				{
					// Menu Structure:
					// ===============
					// New Horizontal Tab Group
					// New Vertical Tab Group
					// Move To Next Tab Group
					// Move To Previous Tab Group
					// --------------
					// Cancel
					//

					if (canCreateHorizontalGroup)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.NewHorizontalGroup) );

					if (canCreateVerticalGroup)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.NewVerticalGroup) );

					if (canMoveNext)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.MoveToNextGroup) );

					if (canMovePrevious)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.MoveToPreviousGroup) );

					if (canCreateHorizontalGroup || canCreateVerticalGroup || canMoveNext || canMovePrevious)
						this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.CancelSeparator) );

					this.TabContextMenu.MenuItems.Add( this.GetMenuItem(MdiTabMenuItems.Cancel) );
					break;
				}
			}
		}
		#endregion //InitializeTabContextMenu

		#region GetMenuType
		internal MdiTabMenuItems GetMenuType( MenuItem menuItem )
		{
			if (this.menuItems != null && menuItem != null)
			{
				for(int i = 0; i < this.menuItems.Length; i++)
				{
					if (this.menuItems[i] == menuItem)
						return (MdiTabMenuItems)i;
				}
			}

			// this shouldn't happen
			return ((MdiTabMenuItems)(-1));
		}
		#endregion //GetMenuType

		#region GetMenuItem
		private MenuItem GetMenuItem( MdiTabMenuItems menuItem )
		{
			if (this.menuItems == null)
				this.menuItems = new MenuItem[ Enum.GetValues(typeof(MdiTabMenuItems)).Length ];

			int index = (int)menuItem;

			if (this.menuItems[index] == null)
			{
				switch(menuItem)
				{
					case MdiTabMenuItems.Close:
						this.menuItems[index] = new IGMenuItem(SR.GetString(null, "MenuItemClose"), this.MenuItemClickHandler);
						break;
					case MdiTabMenuItems.CloseSeparator:
						this.menuItems[index] = new IGMenuItem("-");
						break;
					case MdiTabMenuItems.NewHorizontalGroup:
						this.menuItems[index] = new IGMenuItem(SR.GetString(null, "MenuItemNewHorizontalGroup"), this.MenuItemClickHandler);

						// AS 4/21/03 WTB818
						((IGMenuItem)this.menuItems[index]).Image = this.NewHorizontalGroupImage;
						break;
					case MdiTabMenuItems.NewVerticalGroup:
						this.menuItems[index] = new IGMenuItem(SR.GetString(null, "MenuItemNewVerticalGroup"), this.MenuItemClickHandler);

						// AS 4/21/03 WTB818
						// JDN 8/16/04 NAS2004 Vol3 - VisualStudio2005 style
						// AS 5/2/06 AppStyling
						//if ( this.ViewStyle == ViewStyle.VisualStudio2005 )
						if ( this.ViewStyleResolved == ViewStyle.VisualStudio2005 )
							((IGMenuItem)this.menuItems[index]).Image = this.NewVerticalGroupImageVS2005;
						else
							((IGMenuItem)this.menuItems[index]).Image = this.NewVerticalGroupImage;
						break;
					case MdiTabMenuItems.MoveToNextGroup:
						this.menuItems[index] = new IGMenuItem(SR.GetString(null, "MenuItemMoveToNextGroup"), this.MenuItemClickHandler);
						break;
					case MdiTabMenuItems.MoveToPreviousGroup:
						this.menuItems[index] = new IGMenuItem(SR.GetString(null, "MenuItemMoveToPreviousGroup"), this.MenuItemClickHandler);
						break;
					case MdiTabMenuItems.CancelSeparator:
						this.menuItems[index] = new IGMenuItem("-");
						break;
					case MdiTabMenuItems.Cancel:
						this.menuItems[index] = new IGMenuItem(SR.GetString(null, "MenuItemCancel"), this.MenuItemClickHandler);
						break;

					// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
					case MdiTabMenuItems.Maximize:
						this.menuItems[index] = new IGMenuItem(SR.GetString(null, "MenuItemMaximize"), this.MenuItemClickHandler);

						this.menuItems[index].Checked = this.IsActiveTabGroupMaximized;
						break;
					case MdiTabMenuItems.MaximizeSeparator:
						this.menuItems[index] = new IGMenuItem("-");
						break;
				}

				// when creating a menu item, hook its disposed event
				this.menuItems[index].Disposed += this.MenuItemDisposedHandler;
			}
			else
			{
				// otherwise, reinitialize the menu text in case the thread's
				// locale changed
				switch(menuItem)
				{
					case MdiTabMenuItems.Close:
						this.menuItems[index].Text = SR.GetString(null, "MenuItemClose");
						break;
					case MdiTabMenuItems.NewHorizontalGroup:
						this.menuItems[index].Text = SR.GetString(null, "MenuItemNewHorizontalGroup");
						break;
					case MdiTabMenuItems.NewVerticalGroup:
					{
						// AS 8/25/04 NA 2004 Vol 3
						// Since the view style can change, we need to reinitialize the Image property too.
						// AS 5/2/06 AppStyling
						//if ( this.ViewStyle == ViewStyle.VisualStudio2005 )
						if ( this.ViewStyleResolved == ViewStyle.VisualStudio2005 )
							((IGMenuItem)this.menuItems[index]).Image = this.NewVerticalGroupImageVS2005;
						else
							((IGMenuItem)this.menuItems[index]).Image = this.NewVerticalGroupImage;

						this.menuItems[index].Text = SR.GetString(null, "MenuItemNewVerticalGroup");
						break;
					}
					case MdiTabMenuItems.MoveToNextGroup:
						this.menuItems[index].Text = SR.GetString(null, "MenuItemMoveToNextGroup");
						break;
					case MdiTabMenuItems.MoveToPreviousGroup:
						this.menuItems[index].Text = SR.GetString(null, "MenuItemMoveToPreviousGroup");
						break;
					case MdiTabMenuItems.Cancel:
						this.menuItems[index].Text = SR.GetString(null, "MenuItemCancel");
						break;
					// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
					case MdiTabMenuItems.Maximize:
						this.menuItems[index].Text = SR.GetString(null, "MenuItemMaximize");
						this.menuItems[index].Checked = this.IsActiveTabGroupMaximized;
						break;

				}
			}

			return this.menuItems[index];
		}
		#endregion // GetMenuItem

		#region OnContextMenuPopup
		private void OnContextMenuPopup(object sender, EventArgs e)
		{
			if (this.ignoreContextMenuPopup)
				return;

			if (sender == this.tabContextMenu)
			{
				// figure out which tab based on the position
				MdiTab tab = this.TabFromPoint(Control.MousePosition);

				// AS 4/16/03 WTB815
				if (tab == null)
				{
					this.tabContextMenu.MenuItems.Clear();
					return;
				}

				// initialize the context menu contents
				this.InitializeTabContextMenu(tab, MdiTabContextMenu.Default);

				ICustomMdiTab customTab = tab.CustomMdiTab;

				if (customTab != null)
				{
					// give the custom tab a chance to initialize the menu
					customTab.InitializeMenu(this.TabContextMenu);
				}

				// store the tab that we are displaying the context menu for
				this.tabForContextMenu = tab;

				// allow the programmer to modify the context menu
				MdiTabContextMenuEventArgs args = new MdiTabContextMenuEventArgs(this, tab, this.TabContextMenu, MdiTabContextMenu.Default);

				this.InvokeEvent(TabbedMdiEventIds.InitializeContextMenu, args);

				// if its cancelled, clear the menu so it doesn't get shown
				if (args.Cancel)
					this.TabContextMenu.MenuItems.Clear();
			}
			else
				this.tabForContextMenu = null;
		}
		#endregion //OnContextMenuPopup

		#region OnMenuItemClick
		private void OnMenuItemClick(object sender, EventArgs e)
		{
			if (this.tabForContextMenu == null)
				return;

			// find out which menu item was clicked
			MdiTabMenuItems menuType = this.GetMenuType(sender as MenuItem);

			MdiTab tab = this.tabForContextMenu;

			if (tab == null)
				return;

			switch(menuType)
			{
				case MdiTabMenuItems.Cancel:
					return;
				case MdiTabMenuItems.Close:
					// MD 1/7/08 - BR29406
					// Added a close reason so the handler of the TabClosing and TabClosed events can determine how the tab closed.
					//tab.Close();
					tab.Close( MdiTabCloseReason.CloseMenuItem );
					return;
				case MdiTabMenuItems.MoveToNextGroup:
					// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
					//this.MoveToGroup(tab, MdiTabGroupPosition.Next);
					this.MoveToGroup(tab, tab.TabGroup.NextLeafTabGroup);

					// AS 4/22/03 WTB843
					if (tab.CanActivate)
						tab.Activate();
					break;
				case MdiTabMenuItems.MoveToPreviousGroup:
					// AS 3/17/05 NA 2005 Vol 2 - Nested TabGroups
					//this.MoveToGroup(tab, MdiTabGroupPosition.Previous);
					this.MoveToGroup(tab, tab.TabGroup.PreviousLeafTabGroup);

					// AS 4/22/03 WTB843
					if (tab.CanActivate)
						tab.Activate();
					break;
				case MdiTabMenuItems.NewHorizontalGroup:
					
					this.MoveToNewGroup(tab, tab.TabGroup, RelativePosition.After, Orientation.Horizontal);

					// AS 4/22/03 WTB843
					if (tab.CanActivate)
						tab.Activate();
					break;
				case MdiTabMenuItems.NewVerticalGroup:
					
					this.MoveToNewGroup(tab, tab.TabGroup, RelativePosition.After, Orientation.Vertical);

					// AS 4/22/03 WTB843
					if (tab.CanActivate)
						tab.Activate();
					break;
				// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
				case MdiTabMenuItems.Maximize:
					this.IsActiveTabGroupMaximized = !this.IsActiveTabGroupMaximized;
					break;
			}
		}
		#endregion //OnMenuItemClick

		#region OnMenuItemDisposed
		private void OnMenuItemDisposed(object sender, EventArgs e)
		{
			MenuItem menu = sender as MenuItem;

			if (menu == null)
				return;

			// unhook the disposed and click event
			menu.Disposed -= this.MenuItemDisposedHandler;

			// AS 1/21/04 WTB1303
			// The MenuItem class seems to have a bug where
			// they null out the field that they use to manage
			// the menu item data before raising the event
			// so we're going to unhook from the click event
			// when we are being disposed before disposing
			// the menu items. Therefore, we only need
			// to try to unhook from this event if the
			// menu item is being disposed when we are not the
			// one disposing it.
			if (!this.IsDisposing)
			{
				try
				{
					menu.Click -= this.MenuItemClickHandler;
				}
				catch (NullReferenceException)
				{
				}
			}

			MdiTabMenuItems menuType = this.GetMenuType(menu);

			// null out the member - assuming we found a match
			if (Enum.IsDefined(typeof(MdiTabMenuItems), menuType))
				this.menuItems[(int)menuType] = null;
		}
		#endregion //OnMenuItemDisposed

		#endregion //Menu related

		// AS 5/2/03 WTB884
		#region DragDrop related

		#region CanSetAllowDrop
		private bool CanSetAllowDrop
		{
			get
			{
				// at runtime is this is a single threaded apt
				return !this.DesignMode &&
                    Thread.CurrentThread.GetApartmentState() == ApartmentState.STA;
            }
        }
		#endregion //CanSetAllowDrop

		#region InitializeAllowDrop
		private void InitializeAllowDrop( bool value )
		{
			if (this.IsInitializing || this.DesignMode)
				return;

			if (!this.CanSetAllowDrop)
				return;

			foreach(MdiTabGroupControl tabGroup in this.TabGroupControls.Values)
				tabGroup.AllowDrop = value;
		}
		#endregion //InitializeAllowDrop

		#endregion // DragDrop related

		// JJD 8/22/06 - NA 2006 vol 3
		// Added khelper method
		#region ViewStyleChangeDirtyHelper

		private void ViewStyleChangeDirtyHelper()
		{
			// AS 6/6/06 BR11811
			StyleUtils.DirtyCachedPropertyVals(this);

			// AS 5/19/06 BR11811
			//this.DirtyTabGroups(Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup.TabManagerChangeType.AllMetrics);
			this.DirtyTabGroups(Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup.TabManagerChangeType.AllMetrics
				| MdiTabGroup.TabManagerChangeType.Font);
			this.InvalidateControls();

			// AS 3/15/05 Moved from above
			this.NotifyPropChange(TabbedMdiPropertyIds.ViewStyle);
		}

		#endregion //ViewStyleChangeDirtyHelper	

		// JJD 8/23/06 - NA2006 vol 3
		#region OnOffice2007ColorSchemeChanged

		private void OnOffice2007ColorSchemeChanged(object sender, EventArgs e)
		{
			// JJD 8/23/06 - NA2006 vol 3
			// if the view style is Office 2007 then dirty everytning 
			// when the view style changes
			if ( this.ViewStyleResolved == ViewStyle.Office2007 )
				this.ViewStyleChangeDirtyHelper();
		}

		#endregion //OnOffice2007ColorSchemeChanged	
    
		#endregion //Private

		#endregion //Methods

		#region Interfaces

		#region IImageListProvider interface

		/// <summary>
		/// Returns the <see cref="ImageList"/> property.
		/// </summary>
		System.Windows.Forms.ImageList IImageListProvider.ImageList
		{
			get
			{
				return this.imageList;
			}
		}

		#endregion // IImageListProvider

		#region ISupportInitialize
		void ISupportInitialize.BeginInit()
		{
			// keep a flag so we know that we are initializing
			this.isInitializing = true;

			// AS 3/5/03 UWG2003
			this.OnBeginInit();
		}

		void ISupportInitialize.EndInit()
		{
			// turn off the flag so we know that we are no longer initializing
			this.isInitializing = false;

			// try to hook the mdi parent now...
			this.HookMdiParent();

			// AS 3/5/03 UWG2003
			this.OnEndInit();
		}
		#endregion //ISupportInitialize

		#region About Dialog and Licensing Interface

		/// <summary>
		/// Displays the About dialog for the control.
		/// </summary>
		[DesignOnly(true)]
		[LocalizedDescription("LD_MenuItemInfo_P_About")]
		[LocalizedCategory("LC_Design")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[ParenthesizePropertyName(true)]
		[Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor))]
		[MergableProperty(false)]
		public object About { get { return null; } }

		/// <summary>
		/// Return the license we cached inside the constructor
		/// </summary>
		UltraLicense IUltraLicensedComponent.License { get { return this.license; } }

		#endregion

		#region ITabbedMdiManager
		bool ITabbedMdiManager.Enabled
		{
			get { return this.Enabled; }
			set { this.Enabled = value; }
		}

		Form ITabbedMdiManager.MdiParent
		{
			get { return this.MdiParent; }
		}

        // MRS v6.2 "Navigator"
        // Added GetFormImage, GetFormText, and GetFormToolTipText to 
        // ITabbedMdiManager and implemented them here. 
        Image ITabbedMdiManager.GetFormImage(Form mdiChildForm)
        {
            MdiTab tab = this.GetTabFromForm(mdiChildForm);
            Debug.Assert(tab != null, "Tab could not be found");
            if (tab == null) 
                return null;

            return tab.GetResolvedImage();
        }

        string ITabbedMdiManager.GetFormText(Form mdiChildForm)
        {
            MdiTab tab = this.GetTabFromForm(mdiChildForm);
            Debug.Assert(tab != null, "Tab could not be found");
            if (tab == null)
                return string.Empty;

            return tab.TextResolved;            
        }

        string ITabbedMdiManager.GetFormToolTipText(Form mdiChildForm)
        {
            MdiTab tab = this.GetTabFromForm(mdiChildForm);
            Debug.Assert(tab != null, "Tab could not be found");
            if (tab == null)
                return string.Empty;
                        
            return tab.ToolTipResolved;
        }
    
		// JJD 8/24/06 - NA 2006 vol 3
		#region ToolbarsManager

		/// <summary>
		/// Gets/sets the toolbars manager.
		/// </summary>
		IToolbarsManager ITabbedMdiManager.ToolbarsManager
		{
			get
			{
				return this.toolbarsManager;
			}
			set
			{
				if (this.toolbarsManager != value)
				{
					this.toolbarsManager = value;
				}
			}
		}

		#endregion //ToolbarsManager	

		// JJD 8/22/06 - NA 2006 vol 3
		// Added EnabledChanging event to ITabbedMdiManager interface
		#region EnabledChanging event

		event EventHandler ITabbedMdiManager.EnabledChanging
		{
			add { this.Events.AddHandler(UltraTabbedMdiManager.EventEnabledChanging, value); }
			remove { this.Events.RemoveHandler(UltraTabbedMdiManager.EventEnabledChanging, value); }
		}

		#endregion //EnabledChanging event	
        
		#region EnabledChanged event

		// JJD 8/22/06 - NA 2006 vol 3
		// Added EnabledChanged event to ITabbedMdiManager interface
		event EventHandler ITabbedMdiManager.EnabledChanged
		{
			add { this.Events.AddHandler(UltraTabbedMdiManager.EventEnabledChanged, value); }
			remove { this.Events.RemoveHandler(UltraTabbedMdiManager.EventEnabledChanged, value); }
		}

		#endregion //EnabledChanged event	
    
		#endregion // ITabbedMdiManager

        // MD 10/15/04 - Default Store Support
        #region IPersistComponentSettings Members

        #region LoadComponentSettings
		// MD 11/20/06 - BR17859
		// Updated the docs to reflect where settings are stored.
		///// <summary>
		///// Loads the user configuration settings from the default store
		///// </summary>
		/// <summary>
		/// Loads the user configuration settings for this component.
		/// </summary>
		/// <remarks>
		/// <p class="note">
		/// <b>Note:</b> The location where the settings are stored varies depending on the configuration of the system running 
		/// the application. Information about where these locations are is currently unavailable in Microsoft's MSDN Library.
		/// </p>
		/// </remarks>
        public void LoadComponentSettings()
        {
            Debug.WriteLine("Loading Configuration");

            bool loadSuccessful = false;
            bool triedBinary = false;
            bool triedXml = false;

            switch (this.saveSettingsFormat)
            {
                case SaveSettingsFormat.Binary:
                    triedBinary = true;

                    byte[] binaryData = this.Settings.BinaryData;

                    if (binaryData != null && binaryData.Length != 0)
                    {
                        // Create a stream to read the binary configuration
						// AS 10/28/05
						// Changed to a using statement since the stream is a disposable resource.
						//
						//MemoryStream ms = new MemoryStream()
						using (MemoryStream ms = new MemoryStream())
						{

							// Write the xml configuration to the stream
							ms.Write(binaryData, 0, binaryData.Length);

							// Move back to the beginning of the stream
							ms.Position = 0;

							// Load the tabbed mdi configuration from the data
							try
							{
								this.LoadFromBinary(ms);
								loadSuccessful = true;
							}
							catch { }
						}
                    }

                    if (!loadSuccessful && !triedXml)
                        goto case SaveSettingsFormat.Xml;
                    else
                        break;

                case SaveSettingsFormat.Xml:
                    triedXml = true;
                    string xmlData = this.Settings.XmlData;

                    if (xmlData != null && xmlData.Length != 0)
                    {
                        // Create a stream to read the xml configuration
						// AS 10/28/05
						// Changed to a using statement since the stream is a disposable resource.
						//
						//MemoryStream ms = new MemoryStream()
						using (MemoryStream ms = new MemoryStream())
						{
							StreamWriter sw = new StreamWriter(ms);

							// Write the xml configuration to the stream
							sw.Write(xmlData);
							sw.Flush();

							// Move back to the beginning of the stream
							ms.Position = 0;

							// Load the tabbed mdi configuration from the data
							try
							{
								this.LoadFromXml(ms);
								loadSuccessful = true;
							}
							catch { }
						}
                    }

                    if (!loadSuccessful && !triedBinary)
                        goto case SaveSettingsFormat.Binary;
                    else
                        break;
            }
        }
        #endregion //LoadComponentSettings

        #region ResetComponentSettings
		// MD 11/20/06 - BR17859
		// Updated the docs to reflect where settings are stored.
		///// <summary>
		///// Resets the user configurations in the default store
		///// </summary>
		/// <summary>
		/// Resets the user configuration settings for this component.
		/// </summary>
		/// <remarks>
		/// <p class="note">
		/// <b>Note:</b> The location where the settings are stored varies depending on the configuration of the system running 
		/// the application. Information about where these locations are is currently unavailable in Microsoft's MSDN Library.
		/// </p>
		/// </remarks>
        public void ResetComponentSettings()
        {
            this.Settings.Reset();
        }
        #endregion //ResetComponentSettings

        #region SaveComponentSettings
		// MD 11/20/06 - BR17859
		// Updated the docs to reflect where settings are stored.
		///// <summary>
		///// Saves the user configuration settings to the default store
		///// </summary>
		/// <summary>
		/// Saves the user configuration settings for this component.
		/// </summary>
		/// <remarks>
		/// <p class="note">
		/// <b>Note:</b> The location where the settings are stored varies depending on the configuration of the system running 
		/// the application. Information about where these locations are is currently unavailable in Microsoft's MSDN Library.
		/// </p>
		/// </remarks>
        public void SaveComponentSettings()
        {
            Debug.WriteLine("Saving Configuration");

            // Create a stream
			// AS 10/28/05
			// Changed to a using statement since the stream is a disposable resource.
			//
			//MemoryStream ms = new MemoryStream()
			using (MemoryStream ms = new MemoryStream())
			{

				switch (this.saveSettingsFormat)
				{
					case SaveSettingsFormat.Binary:
						// Save the tabbed mdi configuration data in the stream
						this.SaveAsBinary(ms);

						// Move back to the beginning of the stream
						ms.Position = 0;

						// Store the data
						byte[] binaryData = new byte[ms.Length];
						ms.Read(binaryData, 0, (int)ms.Length);
						this.Settings.BinaryData = binaryData;

						// Save the settings object
						this.Settings.Save();

						break;

					case SaveSettingsFormat.Xml:
						// Save the tabbed mdi configuration data in the stream
						this.SaveAsXml(ms);

						// Move back to the beginning of the stream
						ms.Position = 0;

						// Store the data and save the settings object
						StreamReader sr = new StreamReader(ms);
						this.Settings.XmlData = sr.ReadToEnd();
						this.Settings.Save();

						break;
				}
			}
        }
        #endregion //SaveComponentSettings

        #region SaveSettings
		// MD 11/20/06 - BR17859
		// Updated the docs to reflect where settings are stored.
		///// <summary>
		///// Indicates whether this control should automatically save its user
		///// customizations to the default store
		///// </summary>
		/// <summary>
		/// Indicates whether this component should automatically save its user configuration settings.
		/// </summary>
		/// <remarks>
		/// <p class="note">
		/// <b>Note:</b> The location where the settings are stored varies depending on the configuration of the system running 
		/// the application. Information about where these locations are is currently unavailable in Microsoft's MSDN Library.
		/// </p>
		/// </remarks>
		/// <value></value>
        [DefaultValue(false)]
        // JAS v5.3 SmartTags 7/13/05 - Added LocalizedDescription & LocalizedCategory
        [LocalizedDescription( "LD_UltraTabbedMdiManager_P_SaveSettings" )]
        [LocalizedCategory( "LC_Serialization" )]
        public bool SaveSettings
        {
            get { return this.saveSettings; }
            set { this.saveSettings = value; }
        }
        #endregion //SaveSettings

        #region SettingsKey
		// MD 11/20/06 - BR17859
		// Updated the docs to reflect where settings are stored.
		///// <summary>
		///// A unique string that distinguishes this UltraTabbedMdiManager form others
		///// in the solution it is being used in.  This is needed to store user customizations
		///// </summary>
		/// <summary>
		/// A unique string that distinguishes this UltraTabbedMdiManager form others in the solution it is 
		/// being used in.  This is needed to store user configuration settings.
		/// </summary>
		/// <remarks>
		/// <p class="note">
		/// <b>Note:</b> The location where the settings are stored varies depending on the configuration of the system running 
		/// the application. Information about where these locations are is currently unavailable in Microsoft's MSDN Library.
		/// </p>
		/// </remarks>
		/// <value></value>
        // JAS v5.3 SmartTags 7/13/05 - Added LocalizedDescription & LocalizedCategory
        [LocalizedDescription( "LD_UltraTabbedMdiManager_P_SettingsKey" )]
        [LocalizedCategory( "LC_Serialization" )]
        public string SettingsKey
        {
            get { return this.settingsKey; }
            set { this.settingsKey = value; }
        }

		
        #endregion //SettingsKey

        #endregion //IPersistComponentSettings Members

		#endregion //Interfaces

		#region Base Class overrides

		#region Visible
		/// <summary>
		/// Overriden. This property is not used by the <b>UltraTabbedMdiManager</b>
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool Visible
		{
			get { return base.Visible; }
			set
			{
				throw new NotSupportedException(Shared.SR.GetString(null, "LE_V2_Exception_48"));
			}
		}
		#endregion //Visible

		#region InvalidateControls
		/// <summary>
		/// This method is invoked when the controls should be invalidated. A control
		/// should only be invalidated if its IsHandleCreated property returns true.
		/// </summary>
		protected override void InvalidateControls()
		{
			
			this.InvalidateControls(false, true, true, true);
		}
		#endregion //InvalidateControls

		#region DirtyChildElements
		/// <summary>
		/// This method is invoked when the uielements for the managed controls should
		/// be dirtied. A control should only be dirtied if its IsHandleCreated 
		/// property returns true.
		/// </summary>
		protected override void DirtyChildElements()
		{
			
			this.InvalidateControls(true, true, true, true);
		}
		#endregion //DirtyChildElements

		#region ChangeControlsVisibleState
		/// <summary>
		/// This method is invoked when the visible property of the managed controls
		/// should be changed. A control should only be shown/hidden if its IsHandleCreated 
		/// property returns true.
		/// </summary>
		protected override void ChangeControlsVisibleState()
		{
			// nothing to do since we hide the visible property
			throw new NotSupportedException(Shared.SR.GetString(null, "LE_V2_Exception_48"));
		}
		#endregion //ChangeControlsVisibleState

		#region ChangeControlsEnabledState
		/// <summary>
		/// This method is invoked when the enabled property of the managed controls
		/// should be changed.
		/// </summary>
		protected override void ChangeControlsEnabledState()
		{
			// don't do anything during initialization
			if (this.IsInitializing)
				return;

			// JJD 8/22/06 - NA 2006 vol 3
			// Added EnabledChanging event to ITabbedMdiManager interface
			EventHandler eDelegate = (EventHandler)Events[UltraTabbedMdiManager.EventEnabledChanging];

			if (eDelegate != null)
				eDelegate(this, new EventArgs());

			if (this.Enabled)
			{
				// show the tab ui
				//

				// set up the forms by storing and initializing
				// their properties
				this.InitializeMdiTabForms(false);

				// first make sure all the controls are created...
				this.CreateTabGroupControls();
				this.CreateSplitterControls();

				// AS 4/29/03 WTB880
				this.UpdateBorderStyle();

				// make sure that the controls are sized and positioned correctly
				this.PositionAndSizeTabGroups();
			}
			else
			{
				// hide the tab ui
				//

				// remove the controls
				this.RemoveSplitterControls();
				this.RemoveTabGroupControls();

				// reset the form settings to their previous value
				this.InitializeMdiTabForms(true);

				// AS 4/29/03 WTB880
				this.UpdateBorderStyle();

				// cascade the forms
				// JJD 8/24/06 - NA 2006 vol 3
				// Moved code logic after EnabledChanged event below
				//this.mdiClient.LayoutMdi(MdiLayout.Cascade);
			}

			// JJD 8/22/06 - NA 2006 vol 3
			// Added EnabledChanged event to ITabbedMdiManager interface
			eDelegate = (EventHandler)Events[UltraTabbedMdiManager.EventEnabledChanged];

			if (eDelegate != null)
				eDelegate(this, new EventArgs());

			// JJD 8/24/06 - NA 2006 vol 3
			// Moved logic to cascade the forms from above EnabledChanged event
			if (!this.Enabled)
				this.mdiClient.LayoutMdi(MdiLayout.Cascade);
		}
		#endregion //ChangeControlsEnabledState

		#region EndUpdate
		/// <summary>
		/// Resets the IsUpdating flag to false and invalidates the control
		/// </summary>
		/// <param name="invalidate">True to invalidate the control and dirty the child elements; otherwise false.</param>
		/// <remarks>This method must be called after BeginUpdate. If BeginUpdate was called without a subsequent call to EndUpdate the control will not draw itself.</remarks>
		// AS 4/6/05
		// Added an overload that takes a boolean indicating whether to invalidate.
		//public override void EndUpdate()
		public override void EndUpdate(bool invalidate)
		{
			// AS 4/6/05
			//base.EndUpdate();
			base.EndUpdate(invalidate);

			this.PositionAndSizeTabGroups();
		}
		#endregion ////EndUpdate

		#region OnPropertyChanged
		/// <summary>
		/// Used to invoke the <see cref="UltraControlBase.PropertyChanged"/> event.
		/// </summary>
		/// <param name="e">Event arguments for the <see cref="UltraControlBase.PropertyChanged"/> event</param>
		protected override void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			if (e.ChangeInfo.PropId is TabbedMdiPropertyIds)
			{
				TabbedMdiPropertyIds propId = (TabbedMdiPropertyIds)e.ChangeInfo.PropId;

				switch(propId)
				{
					case TabbedMdiPropertyIds.SplitterAppearance:
						// AS 5/6/05 BR03666
						// Invalidate the splitter controls when the appearance changes.
						//
						this.InvalidateControls(false, true, true, false);
						break;
					case TabbedMdiPropertyIds.ImageSize:
						// AS 5/18/06 BR10993
						// When the image size changes, dispose the icon
						// image since its size is based on the image size.
						//
						if (this.HasTabGroups)
						{
							foreach (MdiTabGroup tabGroup in this.TabGroups.GetNestedTabGroups(true))
							{
								for (int i = 0; i < tabGroup.Tabs.Count; i++)
									tabGroup.Tabs[i].DisposeIconImage();
							}
						}

						foreach (MdiTab hiddenTab in this.HiddenTabs)
							hiddenTab.DisposeIconImage();

						break;

					
				}
			}

			base.OnPropertyChanged(e);
		}
		#endregion //OnPropertyChanged

		// AS 4/27/06 AppStyling
		#region CreateComponentRole

		/// <summary>
		/// Factory method used to create the component role that provides the style information for the control.
		/// </summary>
		protected override AppStyling.ComponentRole CreateComponentRole()
		{
			return new UltraTabbedMdiManagerRole( this );
		}

		#endregion // CreateComponentRole

		#endregion //Base Class overrides

		#region Events

		#region Event objects

		private static readonly object			EventInitializeContextMenu = new object();
		private static readonly object			EventInitializeTab = new object();
		private static readonly object			EventInitializeTabGroup = new object();
		private static readonly object			EventRestoreTab = new object();
		private static readonly object			EventTabClosing = new object();
		private static readonly object			EventTabClosed = new object();
		private static readonly object			EventTabDragging = new object();
		private static readonly object			EventTabDragOver = new object();
		private static readonly object			EventTabDropped = new object();
		private static readonly object			EventTabGroupResizing = new object();
		private static readonly object			EventTabGroupResized = new object();
		private static readonly object			EventTabGroupScrolling = new object();
		private static readonly object			EventTabGroupScrolled = new object();
		private static readonly object			EventTabSelecting = new object();
		private static readonly object			EventTabSelected = new object();
		private static readonly object			EventTabDisplaying = new object();
		private static readonly object			EventTabDisplayed = new object();

		private static readonly object			EventSplitterDragging = new object();
		private static readonly object			EventSplitterDragged = new object();
		private static readonly object			EventTabMoving = new object();
		private static readonly object			EventTabMoved = new object();

		// AS 4/14/03 WTB800
		private static readonly object			EventStoreTab = new object();

		// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
		private static readonly object			EventTabActivated = new object();
	
		// JJD 8/22/06 - NA 2006 vol 3
		// Added EnabledChanged event to ITabbedMdiManager interface
		private static readonly object			EventEnabledChanging = new object();
		private static readonly object			EventEnabledChanged = new object();

		#endregion //Event objects

		#region Event declarations
		/// <summary>
		/// Cancelable event that occurs when a <see cref="System.Windows.Forms.ContextMenu"/> will be displayed for an <see cref="MdiTab"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>InitializeContextMenu</b> is invoked when the end user right clicks on an 
		/// <see cref="MdiTab"/> and when a tab is released within the mdi client area in an area that 
		/// would not result in a new tab group or tab move. The <see cref="MdiTabContextMenuEventArgs.ContextMenuType"/> 
		/// indicates the reason that the context menu is being displayed. The <b>Cancel</b> parameter can 
		/// be set to true to prevent the context menu from being displayed.</p>
		/// </remarks>
		/// <seealso cref="MdiTabContextMenuEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_InitializeContextMenu")]
		[LocalizedCategory("LC_Behavior")]
		public event MdiTabContextMenuEventHandler InitializeContextMenu
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventInitializeContextMenu, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventInitializeContextMenu, value ); }
		}

		/// <summary>
		/// Event that occurs when a new <see cref="MdiTab"/> is created for an mdichild form.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>InitializeTab</b> event is invoked when a new <see cref="MdiTab"/> is 
		/// created for an mdi child form before it has been associated with an <see cref="MdiTabGroup"/>. 
		/// This event provides an opportunity to initialize the tab before it is displayed.</p>
		/// <p class="note">Note: Invoking one of the move methods (e.g. <see cref="MoveToNewGroup(MdiTab)"/>) on the <see cref="MdiTabEventArgs.Tab"/> will result in 
		/// an exception.</p>
		/// </remarks>
		/// <seealso cref="MdiTabEventArgs"/>
		/// <seealso cref="MdiTab"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_InitializeTab")]
		[LocalizedCategory("LC_Appearance")]
		public event MdiTabEventHandler InitializeTab
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventInitializeTab, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventInitializeTab, value ); }
		}

		/// <summary>
		/// Event that occurs when a new <see cref="MdiTabGroup"/> is created.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>InitializeTabGroup</b> event is invoked when an <see cref="MdiTabGroup"/> is 
		/// created and provides an opportunity to initialize its settings before it is displayed.</p>
		/// <p class="note">Note: Attempting to move a tab into the <see cref="MdiTabGroup"/> during the event 
		/// will generate an exception.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupEventArgs"/>
		/// <seealso cref="MdiTabGroup"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_InitializeTabGroup")]
		[LocalizedCategory("LC_Appearance")]
		public event MdiTabGroupEventHandler InitializeTabGroup
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventInitializeTabGroup, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventInitializeTabGroup, value ); }
		}

		/// <summary>
		/// Event that occurs when an <see cref="MdiTab"/> is deserialized so that it may be associated with a <see cref="System.Windows.Forms.Form"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>RestoreTab</b> event is invoked when the <see cref="LoadFromBinary(System.IO.Stream)"/> or 
		/// <see cref="LoadFromXml(System.IO.Stream)"/> method is called and there are serialized <see cref="MdiTab"/> objects. 
		/// The event is invoked so that the deserialized tab can be associated with an mdi child form. This 
		/// is done by setting the <see cref="RestoreTabEventArgs.Form"/> property. If this is not specified, 
		/// the <b>MdiTab</b> will be discarded. You can use the <see cref="MdiTab.PersistedInfo"/> property 
		/// of the <b>MdiTab</b> to store information about the form it should be associated with during 
		/// deserialization (e.g. store a file name).</p>
		/// <p class="note">Note: Any forms created during this event will not have an <see cref="MdiTab"/> 
		/// associated with it so that a form can be created in this event and used to set the <see cref="RestoreTabEventArgs.Form"/> 
		/// property.</p>
		/// </remarks>
		/// <seealso cref="MdiTab.PersistedInfo"/>
		/// <seealso cref="LoadFromXml(System.IO.Stream)"/>
		/// <seealso cref="LoadFromBinary(System.IO.Stream)"/>
		/// <seealso cref="StoreTab"/>
		/// <seealso cref="RestoreTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_RestoreTab")]
		[LocalizedCategory("LC_Data")]
		public event RestoreTabEventHandler RestoreTab
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventRestoreTab, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventRestoreTab, value ); }
		}

		/// <summary>
		/// Cancelable event that occurs before an <see cref="MdiTab"/> is closed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabClosing</b> event is invoked when <see cref="MdiTab.Close()"/> method is 
		/// invoked, the close button is pressed or the "Close" menu option in the tab's context menu is selected. If 
		/// the event is cancelled, no action is taken. Otherwise, the action specified in the <see cref="MdiTabSettingsResolved.TabCloseAction"/> 
		/// is taken and the <see cref="TabClosed"/> event is invoked.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowClose"/>
		/// <seealso cref="MdiTabGroupSettings.ShowCloseButton"/>
		/// <seealso cref="MdiTab.Close()"/>
		/// <seealso cref="TabClosed"/>
		/// <seealso cref="CancelableMdiTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabClosing")]
		[LocalizedCategory("LC_Behavior")]
		public event CancelableMdiTabEventHandler TabClosing
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabClosing, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabClosing, value ); }
		}

		/// <summary>
		/// Event that occurs after an <see cref="MdiTab"/> is closed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabClosed</b> event is invoked when <see cref="MdiTab.Close()"/> method is 
		/// invoked, the close button is pressed or the "Close" menu option in the tab's context menu is selected after
		/// the action specified in the <see cref="MdiTabSettingsResolved.TabCloseAction"/> is taken.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowClose"/>
		/// <seealso cref="MdiTabGroupSettings.ShowCloseButton"/>
		/// <seealso cref="MdiTab.Close()"/>
		/// <seealso cref="TabClosing"/>
		/// <seealso cref="MdiTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabClosed")]
		[LocalizedCategory("LC_Behavior")]
		public event MdiTabEventHandler TabClosed
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabClosed, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabClosed, value ); }
		}

		/// <summary>
		/// Cancelable event that occurs when an <see cref="MdiTab"/> is about to be dragged.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabDragging</b> event is invoked before a drag operation for a tab 
		/// begins. If the event is cancelled, the drag operation will be cancelled. Otherwise, the drag 
		/// will commence and the <see cref="TabDragOver"/> event as the tab is dragged. When the drag operation 
		/// completes successfully (i.e. it is not cancelled using the escape key), the <see cref="TabDropped"/> event will 
		/// be invoked.</p>
		/// <p class="note">Note: This event will not be invoked if the resolved <see cref="MdiTabSettings.AllowDrag"/> 
		/// for the tab is set to <b>None</b> since that tab is not considered draggable.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		/// <seealso cref="TabDropped"/>
		/// <seealso cref="TabDragOver"/>
		/// <seealso cref="CancelableMdiTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabDragging")]
		[LocalizedCategory("LC_DragDrop")]
		public event CancelableMdiTabEventHandler TabDragging
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabDragging, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabDragging, value ); }
		}

		/// <summary>
		/// Event that occurs after an <see cref="MdiTab"/> drag operation is completed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabDropped</b> event is invoked when a drag operation has 
		/// completed. The event will be invoked after any related move operations have occurred.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		/// <seealso cref="TabDragging"/>
		/// <seealso cref="TabDragOver"/>
		/// <seealso cref="MdiTabDroppedEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabDropped")]
		[LocalizedCategory("LC_DragDrop")]
		public event MdiTabDroppedEventHandler TabDropped
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabDropped, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabDropped, value ); }
		}

		/// <summary>
		/// Event that occurs during the drag of an <see cref="MdiTab"/> to determine if the specified location is a valid drop point.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabDragOver</b> event is invoked during a <see cref="MdiTabDragOverEventArgs.Tab"/> 
		/// drag operation when the mouse is positioned over a valid drop location. The 
		/// <see cref="MdiTabDragOverEventArgs.AllowDrop"/> can be set to true to indicate to the end user that this 
		/// location is not a valid drop point. Also, the cursor displayed may be initialized using the 
		/// <see cref="MdiTabDragOverEventArgs.DragCursor"/>. The <see cref="MdiTabDragOverEventArgs.DropAction"/> 
		/// indicates the action that will be taken if the tab is released at this 
		/// <see cref="MdiTabDragOverEventArgs.Point"/>.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		/// <seealso cref="TabDragging"/>
		/// <seealso cref="TabDropped"/>
		/// <seealso cref="MdiTabDragOverEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabDragOver")]
		[LocalizedCategory("LC_DragDrop")]
		public event MdiTabDragOverEventHandler TabDragOver
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabDragOver, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabDragOver, value ); }
		}

		/// <summary>
		/// Cancelable event that occurs before an <see cref="MdiTabGroup"/> is resized.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabGroupResizing</b> is invoked when one or 
		/// more <see cref="MdiTabGroup"/> objects are about to have their <see cref="MdiTabGroup.Extent"/> 
		/// modified. This could be as a result of a splitter drag (<see cref="SplitterDragging"/>), resizing 
		/// the mdi client, creating a new <b>MdiTabGroup</b>, removing an existing <b>MdiTabGroup</b> or 
		/// explicitly setting the <see cref="MdiTabGroup.Extent"/> or <see cref="MdiTabGroup.ClientExtent"/>.</p>
		/// <p class="body">The sum of the extents must always equal the extent of the client area of the 
		/// mdi client. Whether they must equal the width or height is dependant upon the <see cref="Orientation"/>. 
		/// If the event is cancelled, the sum of the extents is calculated. If the sum does not equal the 
		/// extent of the mdi client area, the extents are adjusted.</p>
		/// <p class="note">Note: Setting the <see cref="MdiTabGroup.Extent"/> will implicitly cancel the 
		/// event and the tab groups extents will be verified as described above.</p>
		/// </remarks>
		/// <seealso cref="TabGroupResized"/>
		/// <seealso cref="Orientation"/>
		/// <seealso cref="MdiTabGroup.Extent"/>
		/// <seealso cref="MdiTabGroup.ClientExtent"/>
		/// <seealso cref="MdiTabGroupResizingEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabGroupResizing")]
		[LocalizedCategory("LC_Layout")]
		public event MdiTabGroupResizingEventHandler TabGroupResizing
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabGroupResizing, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabGroupResizing, value ); }
		}

		/// <summary>
		/// Event that occurs after an <see cref="MdiTabGroup"/> is resized.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabGroupResized</b> is invoked after the <see cref="MdiTabGroup.Extent"/> of 
		/// one or more <see cref="MdiTabGroup"/> objects have been modified. This could be as a result of a splitter drag 
		/// (<see cref="SplitterDragging"/>), resizing the mdi client, creating a new <b>MdiTabGroup</b>, removing an 
		/// existing <b>MdiTabGroup</b> or explicitly setting the <see cref="MdiTabGroup.Extent"/> or 
		/// <see cref="MdiTabGroup.ClientExtent"/>.</p>
		/// </remarks>
		/// <seealso cref="TabGroupResizing"/>
		/// <seealso cref="Orientation"/>
		/// <seealso cref="MdiTabGroup.Extent"/>
		/// <seealso cref="MdiTabGroup.ClientExtent"/>
		/// <seealso cref="MdiTabGroupResizedEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabGroupResized")]
		[LocalizedCategory("LC_Layout")]
		public event MdiTabGroupResizedEventHandler TabGroupResized
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabGroupResized, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabGroupResized, value ); }
		}

		/// <summary>
		/// Cancelable event that occurs before an <see cref="MdiTabGroup"/> is scrolled.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabGroupScrolling</b> is invoked when the tabs in an <see cref="MdiTabGroup"/> are 
		/// about to scroll. This could be the result of clicking on a the <see cref="MdiTabGroupSettings.ScrollButtons"/>,
		/// invoking the <see cref="MdiTabGroup.Scroll(Infragistics.Win.UltraWinTabs.ScrollType)"/> method, etc. The <see cref="CancelEventArgs.Cancel"/> may 
		/// be set to true to prevent the scroll operation.</p>
		/// </remarks>
		/// <seealso cref="TabGroupScrolled"/>
        /// <seealso cref="MdiTabGroup.Scroll(Infragistics.Win.UltraWinTabs.ScrollType)"/>
        /// <seealso cref="MdiTabGroup.FirstDisplayedTab"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtons"/>
		/// <seealso cref="MdiTabGroupScrollingEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabGroupScrolling")]
		[LocalizedCategory("LC_Layout")]
		public event MdiTabGroupScrollingEventHandler TabGroupScrolling
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabGroupScrolling, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabGroupScrolling, value ); }
		}

		/// <summary>
		/// Event that occurs after an <see cref="MdiTabGroup"/> is scrolled.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabGroupScrolled</b> is invoked after the tabs in an <see cref="MdiTabGroup"/> have 
		/// been scrolled. This could be the result of clicking on a the <see cref="MdiTabGroupSettings.ScrollButtons"/>,
        /// invoking the <see cref="MdiTabGroup.Scroll(Infragistics.Win.UltraWinTabs.ScrollType)"/> method, etc.</p>
        /// </remarks>
		/// <seealso cref="TabGroupScrolling"/>
        /// <seealso cref="MdiTabGroup.Scroll(Infragistics.Win.UltraWinTabs.ScrollType)"/>
        /// <seealso cref="MdiTabGroup.FirstDisplayedTab"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtons"/>
		/// <seealso cref="MdiTabGroupScrolledEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabGroupScrolled")]
		[LocalizedCategory("LC_Layout")]
		public event MdiTabGroupScrolledEventHandler TabGroupScrolled
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabGroupScrolled, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabGroupScrolled, value ); }
		}

		/// <summary>
		/// Cancelable event that occurs before an <see cref="MdiTab"/> is selected.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabSelecting</b> is invoked when the <see cref="MdiTabGroup.SelectedTab"/> is 
		/// about to change. Selecting an <see cref="MdiTab"/> will activate the <see cref="MdiTab.Form"/> with which it 
		/// is associated.</p>
		/// <p class="body">Since the <b>ActiveTab</b> represents the active mdi tab and mdi child form activation is 
		/// maintained by their zorder, the <b>ActiveTab</b> must be the <see cref="MdiTabGroup.SelectedTab"/> of its 
		/// <see cref="MdiTab.TabGroup"/>. Since the ActiveTab must be selected, as the <b>ActiveTab</b> changes, the 
		/// <b>TabSelecting</b> event will be invoked.</p>
		/// <p class="note">Note: The <b>TabSelecting</b> event may not always be cancellable since the 
		/// activation/selection may occur as a result of an external action - e.g. when the mdi child is first created.</p>
		/// </remarks>
		/// <seealso cref="ActiveTab"/>
		/// <seealso cref="MdiTabGroup.SelectedTab"/>
		/// <seealso cref="TabSelected"/>
		/// <seealso cref="CancelableMdiTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabSelecting")]
		[LocalizedCategory("LC_Appearance")]
		public event CancelableMdiTabEventHandler TabSelecting
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabSelecting, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabSelecting, value ); }
		}

		/// <summary>
		/// Event that occurs after an <see cref="MdiTab"/> has been selected.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabSelected</b> is invoked after a tab has been selected.</p>
		/// </remarks>
		/// <seealso cref="ActiveTab"/>
		/// <seealso cref="MdiTabGroup.SelectedTab"/>
		/// <seealso cref="TabSelecting"/>
		/// <seealso cref="CancelableMdiTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabSelected")]
		[LocalizedCategory("LC_Appearance")]
		public event MdiTabEventHandler TabSelected
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabSelected, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabSelected, value ); }
		}

		/// <summary>
		/// Event that occurs when the visibility of the <see cref="MdiTab.Form"/> of an <see cref="MdiTab"/> is changed to true and the <b>MdiTab</b> is being displayed in an <see cref="MdiTabGroup"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body"><see cref="MdiTab"/> objects associated with non-visible forms are stored in the 
		/// <see cref="HiddenTabs"/> collection and are not associated with a particular <see cref="MdiTabGroup"/>. 
		/// When the associated <see cref="MdiTab.Form"/> is made visible, the tab is removed from the <b>HiddenTabs</b> 
		/// and added to a <b>MdiTabGroup</b>. The <b>TabDisplaying</b> event is invoked when the tab has 
		/// been added to a <b>MdiTabGroup</b> but before the display has been updated.</p>
		/// </remarks>
		/// <seealso cref="HiddenTabs"/>
		/// <seealso cref="MdiTabSettings.TabCloseAction"/>
		/// <seealso cref="TabDisplayed"/>
		/// <seealso cref="MdiTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabDisplaying")]
		[LocalizedCategory("LC_Appearance")]
		public event MdiTabEventHandler TabDisplaying
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabDisplaying, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabDisplaying, value ); }
		}

		/// <summary>
		/// Event that occurs after the visibility of the <see cref="MdiTab.Form"/> of an <see cref="MdiTab"/> is changed to true and the <b>MdiTab</b> is displayed in an <see cref="MdiTabGroup"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body"><see cref="MdiTab"/> objects associated with non-visible forms are stored in the 
		/// <see cref="HiddenTabs"/> collection and are not associated with a particular <see cref="MdiTabGroup"/>. 
		/// When the associated <see cref="MdiTab.Form"/> is made visible, the tab is removed from the <b>HiddenTabs</b> 
		/// and added to a <b>MdiTabGroup</b>. The <b>TabDisplayed</b> event is invoked after the tab has 
		/// been added to a <b>MdiTabGroup</b>.</p>
		/// </remarks>
		/// <seealso cref="HiddenTabs"/>
		/// <seealso cref="MdiTabSettings.TabCloseAction"/>
		/// <seealso cref="TabDisplaying"/>
		/// <seealso cref="MdiTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabDisplayed")]
		[LocalizedCategory("LC_Appearance")]
		public event MdiTabEventHandler TabDisplayed
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabDisplayed, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabDisplayed, value ); }
		}

		/// <summary>
		/// Event that occurs when an <see cref="MdiTab"/> is about to be repositioned.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabMoving</b> event is invoked before an <see cref="MdiTab"/> is moved 
		/// from one position to another. This may be a reposition in the <see cref="MdiTabGroup.Tabs"/> of the 
		/// containing <see cref="MdiTab.TabGroup"/> or as a result of being moved to a different <see cref="MdiTabGroup"/>. 
		/// The end user may reposition a tab by either dragging the tab or via the right click context menu. Where 
		/// the user may reposition the tabs is dependant on many settings including the tab's <see cref="MdiTabSettingsResolved.AllowDrag"/>, the 
		/// <see cref="MdiTabGroupSettings.AllowDrop"/> of the tab groups, as well as the <see cref="MaxTabGroups"/>, 
		/// <see cref="AllowHorizontalTabGroups"/> and <see cref="AllowVerticalTabGroups"/>. Programatically, the tab 
		/// may be repositioned to a specific <b>MdiTabGroup</b> using the <see cref="MdiTab.MoveToGroup(MdiTabGroup)"/> or to 
		/// a new <b>MdiTabGroup</b> using the <see cref="MoveToNewGroup(MdiTab)"/> method. A tab may also be repositioned using its 
		/// <see cref="MdiTab.Reposition"/> method or it may be repositioned by its containing group using the 
		/// <see cref="MdiTabGroup.MoveAllTabs"/>.</p>
		/// </remarks>
		/// <seealso cref="MoveToGroup(MdiTab, MdiTabGroup)"/>
		/// <seealso cref="MoveToNewGroup(MdiTab)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="MdiTabGroup.MoveAllTabs"/>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		/// <seealso cref="MdiTabMovingEventArgs"/>
		/// <seealso cref="TabMoved"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabMoving")]
		[LocalizedCategory("LC_Behavior")]
		public event MdiTabMovingEventHandler TabMoving
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabMoving, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabMoving, value ); }
		}

		/// <summary>
		/// Event that occurs after an <see cref="MdiTab"/> has been repositioned.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabMoved</b> event is invoked after an <see cref="MdiTab"/> is moved 
		/// from one position to another. This may be a reposition in the <see cref="MdiTabGroup.Tabs"/> of the 
		/// containing <see cref="MdiTab.TabGroup"/> or as a result of being moved to a different <see cref="MdiTabGroup"/>. 
		/// The end user may reposition a tab by either dragging the tab or via the right click context menu. Where 
		/// the user may reposition the tabs is dependant on many settings including the tab's <see cref="MdiTabSettingsResolved.AllowDrag"/>, the 
		/// <see cref="MdiTabGroupSettings.AllowDrop"/> of the tab groups, as well as the <see cref="MaxTabGroups"/>, 
		/// <see cref="AllowHorizontalTabGroups"/> and <see cref="AllowVerticalTabGroups"/>. Programatically, the tab 
		/// may be repositioned to a specific <b>MdiTabGroup</b> using the <see cref="MdiTab.MoveToGroup(MdiTabGroup)"/> or to 
		/// a new <b>MdiTabGroup</b> using the <see cref="MoveToNewGroup(MdiTab)"/> method. A tab may also be repositioned using its 
		/// <see cref="MdiTab.Reposition"/> method or it may be repositioned by its containing group using the 
		/// <see cref="MdiTabGroup.MoveAllTabs"/>.</p>
		/// </remarks>
		/// <seealso cref="MoveToGroup(MdiTab,MdiTabGroup)"/>
		/// <seealso cref="MoveToNewGroup(MdiTab)"/>
		/// <seealso cref="MdiTab.Reposition"/>
		/// <seealso cref="MdiTabGroup.MoveAllTabs"/>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		/// <seealso cref="MdiTabMovingEventArgs"/>
		/// <seealso cref="TabMoving"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabMoved")]
		[LocalizedCategory("LC_Behavior")]
		public event MdiTabEventHandler TabMoved
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabMoved, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabMoved, value ); }
		}

		/// <summary>
		/// Event that occurs when the splitter bar for an <see cref="MdiTabGroup"/> is about to be repositioned.
		/// </summary>
		/// <remarks>
		/// <p class="body">All <see cref="MdiTabGroup"/> objects in the <see cref="TabGroups"/> 
		/// collection have a splitter bar except the last one. The splitter bar is used 
		/// by the end user to modify the <see cref="MdiTabGroup.Extent"/> of a tab group.
		/// When the user presses the logical left mouse button on the splitter 
		/// bar, the <see cref="SplitterDragging"/> event is invoked to determine whether 
		/// to allow the splitter bar to be repositioned. If the event is not cancelled, 
		/// the user may move the splitter bar. Once the splitter bar is released, the 
		/// <see cref="MdiTabGroup.Extent"/> is updated (as well as that of the adjacent 
		/// tab group) and the <see cref="SplitterDragged"/> event is invoked.</p>
		/// </remarks>
		/// <seealso cref="SplitterDragged"/>
		/// <seealso cref="SplitterWidth"/>
		/// <seealso cref="Orientation"/>
		/// <seealso cref="SplitterBorderStyle"/>
		/// <seealso cref="MdiSplitterDraggingEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_SplitterDragging")]
		[LocalizedCategory("LC_Behavior")]
		public event MdiSplitterDraggingEventHandler SplitterDragging
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventSplitterDragging, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventSplitterDragging, value ); }
		}

		/// <summary>
		/// Event that occurs after the splitter bar for an <see cref="MdiTabGroup"/> has been repositioned.
		/// </summary>
		/// <remarks>
		/// <p class="body">All <see cref="MdiTabGroup"/> objects in the <see cref="TabGroups"/> 
		/// collection have a splitter bar except the last one. The splitter bar is used 
		/// by the end user to modify the <see cref="MdiTabGroup.Extent"/> of a tab group.
		/// When the user presses the logical left mouse button on the splitter 
		/// bar, the <see cref="SplitterDragging"/> event is invoked to determine whether 
		/// to allow the splitter bar to be repositioned. If the event is not cancelled, 
		/// the user may move the splitter bar. Once the splitter bar is released, the 
		/// <see cref="MdiTabGroup.Extent"/> is updated (as well as that of the adjacent 
		/// tab group) and the <see cref="SplitterDragged"/> event is invoked.</p>
		/// </remarks>
		/// <seealso cref="SplitterDragging"/>
		/// <seealso cref="SplitterWidth"/>
		/// <seealso cref="Orientation"/>
		/// <seealso cref="SplitterBorderStyle"/>
		/// <seealso cref="MdiTabGroupEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_SplitterDragged")]
		[LocalizedCategory("LC_Behavior")]
		public event MdiTabGroupEventHandler SplitterDragged
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventSplitterDragged, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventSplitterDragged, value ); }
		}

		// AS 4/14/03 WTB800
		/// <summary>
		/// Event that occurs when the <see cref="UltraTabbedMdiManager.SaveAsBinary(System.IO.Stream)"/> or <see cref="UltraTabbedMdiManager.SaveAsXml(System.IO.Stream)"/> method is invoked so that each tab's <see cref="MdiTab.PersistedInfo"/> may be initialized.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>StoreTab</b> event is invoked for each <see cref="MdiTab"/> when the 
		/// <see cref="SaveAsXml(System.IO.Stream)"/> or <see cref="SaveAsBinary(System.IO.Stream)"/> method is called. The purpose of the 
		/// event is to provide a place to initialize the <see cref="MdiTab.PersistedInfo"/> property 
		/// before serializing the tabs. The <b>PersistedInfo</b> property can be set outside of this event 
		/// but often the criteria you will need to create the appropriate form during deserialization will 
		/// change during the life of the application. This event provides a centralized point to initialize 
		/// the tabs immediately before serialization without having to iterate through the <see cref="MdiTabGroup.Tabs"/> 
		/// of all the <see cref="TabGroups"/> and the <see cref="HiddenTabs"/>.</p>
		/// </remarks>
		/// <seealso cref="MdiTab.PersistedInfo"/>
		/// <seealso cref="SaveAsXml(System.IO.Stream)"/>
		/// <seealso cref="SaveAsBinary(System.IO.Stream)"/>
		/// <seealso cref="StoreTabEventArgs"/>
		/// <seealso cref="RestoreTab"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_StoreTab")]
		[LocalizedCategory("LC_Behavior")]
		public event StoreTabEventHandler StoreTab
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventStoreTab, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventStoreTab, value ); }
		}

		// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
		/// <summary>
		/// Event that occurs after the <see cref="ActiveTab"/> has changed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabActivated</b> event is invoked when when an <see cref="MdiTab"/> 
		/// is activated.</p>
		/// </remarks>
		/// <seealso cref="UltraTabbedMdiManager.ActiveTab"/>
		/// <seealso cref="MdiTab.Activate"/>
		/// <seealso cref="MdiTab.IsFormActive"/>
		/// <seealso cref="MdiTabEventArgs"/>
		[LocalizedDescription("LD_UltraTabbedMdiManager_E_TabActivated")]
		[LocalizedCategory("LC_Behavior")]
		public event MdiTabEventHandler TabActivated
		{
			add { this.Events.AddHandler( UltraTabbedMdiManager.EventTabActivated, value ); }
			remove { this.Events.RemoveHandler( UltraTabbedMdiManager.EventTabActivated, value ); }
		}
		#endregion //Event declarations

		#region Protected virtual OnXXX

		#region OnMouseEnter/Leave
		/// <summary>
		/// Raises the <see cref="UltraComponentControlManagerBase.MouseEnterElement"/> event when the mouse is moved over a UIElement.
		/// </summary>
		/// <param name="e">A <see cref="UIElementEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnMouseEnterElement</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnMouseEnterElement</b> in a derived class, be sure to call the base class's <b>OnMouseEnterElement</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="UIElementEventArgs"/>
		/// <seealso cref="UltraComponentControlManagerBase.MouseEnterElement"/>
		protected override void OnMouseEnterElement( UIElementEventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( TabbedMdiEventIds.MouseEnterElement ) )
				return;

			if ( e == null )
				throw new ArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_50"));

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( TabbedMdiEventIds.MouseEnterElement );

			try
			{
				base.OnMouseEnterElement(e);
			}
			catch
			{
				throw;
			}
			finally
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( TabbedMdiEventIds.MouseEnterElement );
			}
		}

		/// <summary>
		/// Raises the <see cref="UltraComponentControlManagerBase.MouseLeaveElement"/> event when the mouse is moved out of a UIElement.
		/// </summary>
		/// <param name="e">A <see cref="UIElementEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnMouseLeaveElement</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnMouseLeaveElement</b> in a derived class, be sure to call the base class's <b>OnMouseLeaveElement</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="UIElementEventArgs"/>
		/// <seealso cref="UltraComponentControlManagerBase.MouseLeaveElement"/>
		protected override void OnMouseLeaveElement( UIElementEventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( TabbedMdiEventIds.MouseLeaveElement ) )
				return;

			if ( e == null )
				throw new ArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_50"));

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( TabbedMdiEventIds.MouseLeaveElement );

			try
			{
				base.OnMouseLeaveElement(e);
			}
			catch
			{
				throw;
			}
			finally
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( TabbedMdiEventIds.MouseLeaveElement );
			}

		}
		#endregion //OnMouseEnter/Leave

		#region OnInitializeContextMenu
		/// <summary>
		/// Raises the <see cref="InitializeContextMenu"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabContextMenuEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnInitializeContextMenu</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnInitializeContextMenu</b> in a derived class, be sure to call the base class's <b>OnInitializeContextMenu</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabContextMenuEventArgs"/>
		/// <seealso cref="InitializeContextMenu"/>
		protected virtual void OnInitializeContextMenu( MdiTabContextMenuEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_49"));

			MdiTabContextMenuEventHandler eDelegate = (MdiTabContextMenuEventHandler)Events[UltraTabbedMdiManager.EventInitializeContextMenu];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnInitializeContextMenu

		#region OnInitializeTab
		/// <summary>
		/// Raises the <see cref="InitializeTab"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnInitializeTab</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnInitializeTab</b> in a derived class, be sure to call the base class's <b>OnInitializeTab</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabEventArgs"/>
		/// <seealso cref="InitializeTab"/>
		protected virtual void OnInitializeTab( MdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_50"));

			MdiTabEventHandler eDelegate = (MdiTabEventHandler)Events[UltraTabbedMdiManager.EventInitializeTab];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnInitializeTab

		#region OnInitializeTabGroup
		/// <summary>
		/// Raises the <see cref="InitializeTabGroup"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabGroupEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnInitializeTabGroup</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnInitializeTabGroup</b> in a derived class, be sure to call the base class's <b>OnInitializeTabGroup</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupEventArgs"/>
		/// <seealso cref="InitializeTabGroup"/>
		protected virtual void OnInitializeTabGroup( MdiTabGroupEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_51"));

			MdiTabGroupEventHandler eDelegate = (MdiTabGroupEventHandler)Events[UltraTabbedMdiManager.EventInitializeTabGroup];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnInitializeTabGroup

		#region OnRestoreTab
		/// <summary>
		/// Raises the <see cref="RestoreTab"/> event.
		/// </summary>
		/// <param name="e">A <see cref="RestoreTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnRestoreTab</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnRestoreTab</b> in a derived class, be sure to call the base class's <b>OnRestoreTab</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="RestoreTabEventArgs"/>
		/// <seealso cref="RestoreTab"/>
		protected virtual void OnRestoreTab( RestoreTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_52"));

			RestoreTabEventHandler eDelegate = (RestoreTabEventHandler)Events[UltraTabbedMdiManager.EventRestoreTab];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnRestoreTab

		#region OnTabClosing
		/// <summary>
		/// Raises the <see cref="TabClosing"/> event.
		/// </summary>
		/// <param name="e">A <see cref="CancelableMdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabClosing</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabClosing</b> in a derived class, be sure to call the base class's <b>OnTabClosing</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="CancelableMdiTabEventArgs"/>
		/// <seealso cref="TabClosing"/>
		protected virtual void OnTabClosing( CancelableMdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_53"));

			CancelableMdiTabEventHandler eDelegate = (CancelableMdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabClosing];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabClosing

		#region OnTabClosed
		/// <summary>
		/// Raises the <see cref="TabClosed"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabClosed</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabClosed</b> in a derived class, be sure to call the base class's <b>OnTabClosed</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabEventArgs"/>
		/// <seealso cref="TabClosed"/>
		protected virtual void OnTabClosed( MdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_50"));

			MdiTabEventHandler eDelegate = (MdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabClosed];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabClosed

		#region OnTabDragging
		/// <summary>
		/// Raises the <see cref="TabDragging"/> event.
		/// </summary>
		/// <param name="e">A <see cref="CancelableMdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabDragging</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabDragging</b> in a derived class, be sure to call the base class's <b>OnTabDragging</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="CancelableMdiTabEventArgs"/>
		/// <seealso cref="TabDragging"/>
		protected virtual void OnTabDragging( CancelableMdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_53"));

			CancelableMdiTabEventHandler eDelegate = (CancelableMdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabDragging];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabDragging

		#region OnTabDropped
		/// <summary>
		/// Raises the <see cref="TabDropped"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabDroppedEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabDropped</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabDropped</b> in a derived class, be sure to call the base class's <b>OnTabDropped</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabDroppedEventArgs"/>
		/// <seealso cref="TabDropped"/>
		protected virtual void OnTabDropped( MdiTabDroppedEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_54"));

			MdiTabDroppedEventHandler eDelegate = (MdiTabDroppedEventHandler)Events[UltraTabbedMdiManager.EventTabDropped];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabDropped

		#region OnTabDragOver
		/// <summary>
		/// Raises the <see cref="TabDragOver"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabDragOverEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabDragOver</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabDragOver</b> in a derived class, be sure to call the base class's <b>OnTabDragOver</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabDragOverEventArgs"/>
		/// <seealso cref="TabDragOver"/>
		protected virtual void OnTabDragOver( MdiTabDragOverEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_55"));

			MdiTabDragOverEventHandler eDelegate = (MdiTabDragOverEventHandler)Events[UltraTabbedMdiManager.EventTabDragOver];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabDragOver

		#region OnTabGroupResizing
		/// <summary>
		/// Raises the <see cref="TabGroupResizing"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabGroupResizingEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabGroupResizing</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabGroupResizing</b> in a derived class, be sure to call the base class's <b>OnTabGroupResizing</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupResizingEventArgs"/>
		/// <seealso cref="TabGroupResizing"/>
		protected virtual void OnTabGroupResizing( MdiTabGroupResizingEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_56"));

			MdiTabGroupResizingEventHandler eDelegate = (MdiTabGroupResizingEventHandler)Events[UltraTabbedMdiManager.EventTabGroupResizing];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabGroupResizing

		#region OnTabGroupResized
		/// <summary>
		/// Raises the <see cref="TabGroupResized"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabGroupResizedEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabGroupResized</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabGroupResized</b> in a derived class, be sure to call the base class's <b>OnTabGroupResized</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupResizedEventArgs"/>
		/// <seealso cref="TabGroupResized"/>
		protected virtual void OnTabGroupResized( MdiTabGroupResizedEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_57"));

			MdiTabGroupResizedEventHandler eDelegate = (MdiTabGroupResizedEventHandler)Events[UltraTabbedMdiManager.EventTabGroupResized];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabGroupResized

		#region OnTabGroupScrolling
		/// <summary>
		/// Raises the <see cref="TabGroupScrolling"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabGroupScrollingEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabGroupScrolling</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabGroupScrolling</b> in a derived class, be sure to call the base class's <b>OnTabGroupScrolling</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupScrollingEventArgs"/>
		/// <seealso cref="TabGroupScrolling"/>
		protected virtual void OnTabGroupScrolling( MdiTabGroupScrollingEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_58"));

			MdiTabGroupScrollingEventHandler eDelegate = (MdiTabGroupScrollingEventHandler)Events[UltraTabbedMdiManager.EventTabGroupScrolling];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabGroupScrolling

		#region OnTabGroupScrolled
		/// <summary>
		/// Raises the <see cref="TabGroupScrolled"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabGroupScrolledEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabGroupScrolled</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabGroupScrolled</b> in a derived class, be sure to call the base class's <b>OnTabGroupScrolled</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupScrolledEventArgs"/>
		/// <seealso cref="TabGroupScrolled"/>
		protected virtual void OnTabGroupScrolled( MdiTabGroupScrolledEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_59"));

			MdiTabGroupScrolledEventHandler eDelegate = (MdiTabGroupScrolledEventHandler)Events[UltraTabbedMdiManager.EventTabGroupScrolled];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabGroupScrolled

		#region OnTabSelecting
		/// <summary>
		/// Raises the <see cref="TabSelecting"/> event.
		/// </summary>
		/// <param name="e">A <see cref="CancelableMdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabSelecting</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabSelecting</b> in a derived class, be sure to call the base class's <b>OnTabSelecting</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="CancelableMdiTabEventArgs"/>
		/// <seealso cref="TabSelecting"/>
		protected virtual void OnTabSelecting( CancelableMdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_53"));

			CancelableMdiTabEventHandler eDelegate = (CancelableMdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabSelecting];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabSelecting

		#region OnTabSelected
		/// <summary>
		/// Raises the <see cref="TabSelected"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabSelected</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabSelected</b> in a derived class, be sure to call the base class's <b>OnTabSelected</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabEventArgs"/>
		/// <seealso cref="TabSelected"/>
		protected virtual void OnTabSelected( MdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_50"));

			MdiTabEventHandler eDelegate = (MdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabSelected];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabSelected

		#region OnTabDisplayed
		/// <summary>
		/// Raises the <see cref="TabDisplayed"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabDisplayed</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabDisplayed</b> in a derived class, be sure to call the base class's <b>OnTabDisplayed</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabEventArgs"/>
		/// <seealso cref="TabDisplayed"/>
		protected virtual void OnTabDisplayed( MdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_50"));

			MdiTabEventHandler eDelegate = (MdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabDisplayed];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabDisplayed

		#region OnTabDisplaying
		/// <summary>
		/// Raises the <see cref="TabDisplaying"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabDisplaying</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabDisplaying</b> in a derived class, be sure to call the base class's <b>OnTabDisplaying</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabEventArgs"/>
		/// <seealso cref="TabDisplaying"/>
		protected virtual void OnTabDisplaying( MdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_50"));

			MdiTabEventHandler eDelegate = (MdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabDisplaying];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabDisplaying

		#region OnTabMoving
		/// <summary>
		/// Raises the <see cref="TabMoving"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabMovingEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabMoving</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabMoving</b> in a derived class, be sure to call the base class's <b>OnTabMoving</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabMovingEventArgs"/>
		/// <seealso cref="TabMoving"/>
		protected virtual void OnTabMoving( MdiTabMovingEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_60"));

			MdiTabMovingEventHandler eDelegate = (MdiTabMovingEventHandler)Events[UltraTabbedMdiManager.EventTabMoving];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabMoving

		#region OnTabMoved
		/// <summary>
		/// Raises the <see cref="TabMoved"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabMoved</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabMoved</b> in a derived class, be sure to call the base class's <b>OnTabMoved</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabEventArgs"/>
		/// <seealso cref="TabMoved"/>
		protected virtual void OnTabMoved( MdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_50"));

			MdiTabEventHandler eDelegate = (MdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabMoved];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabMoved

		#region OnSplitterDragged
		/// <summary>
		/// Raises the <see cref="SplitterDragged"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabGroupEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnSplitterDragged</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnSplitterDragged</b> in a derived class, be sure to call the base class's <b>OnSplitterDragged</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupEventArgs"/>
		/// <seealso cref="SplitterDragged"/>
		protected virtual void OnSplitterDragged( MdiTabGroupEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_51"));

			MdiTabGroupEventHandler eDelegate = (MdiTabGroupEventHandler)Events[UltraTabbedMdiManager.EventSplitterDragged];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnSplitterDragged

		#region OnSplitterDragging
		/// <summary>
		/// Raises the <see cref="SplitterDragging"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiSplitterDraggingEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnSplitterDragging</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnSplitterDragging</b> in a derived class, be sure to call the base class's <b>OnSplitterDragging</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiSplitterDraggingEventArgs"/>
		/// <seealso cref="SplitterDragging"/>
		protected virtual void OnSplitterDragging( MdiSplitterDraggingEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_61"));

			MdiSplitterDraggingEventHandler eDelegate = (MdiSplitterDraggingEventHandler)Events[UltraTabbedMdiManager.EventSplitterDragging];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnSplitterDragging

		// AS 4/14/03 WTB800
		#region OnStoreTab
		/// <summary>
		/// Raises the <see cref="StoreTab"/> event.
		/// </summary>
		/// <param name="e">A <see cref="StoreTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnStoreTab</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnStoreTab</b> in a derived class, be sure to call the base class's <b>OnStoreTab</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="StoreTabEventArgs"/>
		/// <seealso cref="StoreTab"/>
		protected virtual void OnStoreTab( StoreTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException("e", "A valid 'StoreTabEventArgs' instance must be specified.");

			StoreTabEventHandler eDelegate = (StoreTabEventHandler)Events[UltraTabbedMdiManager.EventStoreTab];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnStoreTab

		// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
		#region OnTabActivated
		/// <summary>
		/// Raises the <see cref="TabActivated"/> event.
		/// </summary>
		/// <param name="e">A <see cref="MdiTabEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnTabActivated</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnTabActivated</b> in a derived class, be sure to call the base class's <b>OnTabActivated</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="MdiTabEventArgs"/>
		/// <seealso cref="TabActivated"/>
		protected virtual void OnTabActivated( MdiTabEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_50"));

			MdiTabEventHandler eDelegate = (MdiTabEventHandler)Events[UltraTabbedMdiManager.EventTabActivated];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnTabActivated

		#endregion //Protected virtual OnXXX

		#region InvokeEvent
		internal void InvokeEvent( TabbedMdiEventIds eventId, EventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( eventId ) )
				return;

			if ( !Enum.IsDefined( typeof( TabbedMdiEventIds ), eventId ) )
			{
				// AS 4/30/03 FxCop Change
				// Explicitly call the overload that takes an IFormatProvider
				//
				//Debug.Fail( string.Format("Invalid eventId in InvokeEvent, id: {0}", eventId) );
				Debug.Fail( string.Format(null, "Invalid eventId in InvokeEvent, id: {0}", eventId) );
				return;
			}

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( eventId );
			
			try
			{
				switch(eventId)
				{
					case TabbedMdiEventIds.InitializeContextMenu:
						this.OnInitializeContextMenu(e as MdiTabContextMenuEventArgs);
						break;
					case TabbedMdiEventIds.InitializeTab:
						this.OnInitializeTab(e as MdiTabEventArgs);
						break;
					case TabbedMdiEventIds.InitializeTabGroup:
						this.OnInitializeTabGroup(e as MdiTabGroupEventArgs);
						break;
					case TabbedMdiEventIds.RestoreTab:
						this.OnRestoreTab(e as RestoreTabEventArgs);
						break;
					case TabbedMdiEventIds.TabClosing:

						// MD 1/7/08 - BR29406
						// Make sure the event args are the new derived event args, even though OnTabClosing only takes the base.
						Debug.Assert( e is MdiTabClosingEventArgs );

						this.OnTabClosing(e as CancelableMdiTabEventArgs);
						break;
					case TabbedMdiEventIds.TabClosed:

						// MD 1/7/08 - BR29406
						// Make sure the event args are the new derived event args, even though OnTabClosed only takes the base.
						Debug.Assert( e is MdiTabClosedEventArgs );

						this.OnTabClosed(e as MdiTabEventArgs);
						break;
					case TabbedMdiEventIds.TabDragging:
						this.OnTabDragging(e as CancelableMdiTabEventArgs);
						break;
					case TabbedMdiEventIds.TabDragOver:
						this.OnTabDragOver(e as MdiTabDragOverEventArgs);
						break;
					case TabbedMdiEventIds.TabDropped:
						this.OnTabDropped(e as MdiTabDroppedEventArgs);
						break;
					case TabbedMdiEventIds.TabGroupResizing:
						this.OnTabGroupResizing(e as MdiTabGroupResizingEventArgs);
						break;
					case TabbedMdiEventIds.TabGroupResized:
						this.OnTabGroupResized(e as MdiTabGroupResizedEventArgs);
						break;
					case TabbedMdiEventIds.TabGroupScrolling:
						this.OnTabGroupScrolling(e as MdiTabGroupScrollingEventArgs);
						break;
					case TabbedMdiEventIds.TabGroupScrolled:
						this.OnTabGroupScrolled(e as MdiTabGroupScrolledEventArgs);
						break;
					case TabbedMdiEventIds.TabSelecting:
						this.OnTabSelecting(e as CancelableMdiTabEventArgs);
						break;
					case TabbedMdiEventIds.TabSelected:
						this.OnTabSelected(e as MdiTabEventArgs);
						break;
					case TabbedMdiEventIds.TabDisplayed:
						this.OnTabDisplayed(e as MdiTabEventArgs);
						break;
					case TabbedMdiEventIds.TabDisplaying:
						this.OnTabDisplaying(e as MdiTabEventArgs);
						break;
					case TabbedMdiEventIds.SplitterDragged:
						this.OnSplitterDragged(e as MdiTabGroupEventArgs);
						break;
					case TabbedMdiEventIds.SplitterDragging:
						this.OnSplitterDragging(e as MdiSplitterDraggingEventArgs);
						break;
					case TabbedMdiEventIds.TabMoving:
						this.OnTabMoving(e as MdiTabMovingEventArgs);
						break;
					case TabbedMdiEventIds.TabMoved:
						this.OnTabMoved(e as MdiTabEventArgs);
						break;
						// AS 4/14/03 WTB800
					case TabbedMdiEventIds.StoreTab:
						this.OnStoreTab(e as StoreTabEventArgs);
						break;
					// AS 3/24/05 NA 2005 Vol 2 - TabActivated event
					case TabbedMdiEventIds.TabActivated:
						this.OnTabActivated(e as MdiTabEventArgs);
						break;
				}


			}
			finally
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( eventId );
			}

		}
		#endregion //InvokeEvent

		#endregion // Events
	
		#region Nested classes

		// AS 10/30/03 WTB1214
		#region FocusHelper
		internal class FocusHelper : Control
		{
			internal FocusHelper()
			{
			}

			internal void ToggleFocus(Control control)
			{
				if (!control.Focused)
					return;

				if (control is Infragistics.Win.EmbeddableTextBox)
					return;

				this.InvokeLostFocus(control, EventArgs.Empty);
				this.InvokeGotFocus(control, EventArgs.Empty);
			}
		}
		#endregion //FocusHelper

		#region UltraTabbedMdiManagerApplicationSettings
        // MD 10/15/04 - Default Store Support
		#region Public Class UltraTabbedMdiManagerApplicationSettings
        /// <summary>
        /// Helps with the saving and loading of data from the default store
        /// </summary>
		// AS 10/28/05 BR07306
		// VS uses the class name as the attribute tag name and since this class
		// is nested, it contains a + and instead of filtering that, vs just 
		// throws an exception when it writes the config file. To get around this
		// we are explicitly telling vs the start of the attribute name using the
		// SettingsGroupName tag. The convention we will use is the class name 
		// of the component it will be serializing.
		//
		[System.Configuration.SettingsGroupName("Infragistics.Win.UltraWinTabbedMdi.UltraTabbedMdiManager")]
		// MD 2/3/09 - TFS13426
		// Use the new base class created to work around a VS error.
		//public class UltraTabbedMdiManagerApplicationSettings : System.Configuration.ApplicationSettingsBase
		public class UltraTabbedMdiManagerApplicationSettings : UltraApplicationSettingsBase
        {
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="owner">The owner of this settings instance</param>
            /// <param name="settingsKey">The settingsKey of the owner</param>
            public UltraTabbedMdiManagerApplicationSettings(IComponent owner, string settingsKey)
                : base(owner, settingsKey) { }

            /// <summary>
            /// Gets or sets the binary data which contains the tabbed mdi configuration settings
            /// </summary>
            /// <value></value>
            [System.Configuration.UserScopedSetting]
            public byte[] BinaryData
            {
                get { return (byte[])this["BinaryData"]; }
                set { this["BinaryData"] = value; }
            }

            /// <summary>
            /// Gets or sets the xml string which contains the tabbed mdi configuration settings
            /// </summary>
            /// <value></value>
            [System.Configuration.UserScopedSetting]
            public string XmlData
            {
                get { return (string)this["XmlData"]; }
                set { this["XmlData"] = value; }
            }
        }
		#endregion //Public Class UltraToolbarsManagerApplicationSettings
		#endregion //UltraTabbedMdiManagerApplicationSettings

		// AS 3/18/05 NA 2005 Vol 2 - Nested TabGroups
		#region TabGroupExtentInfo
		private class TabGroupExtentInfo
		{
			#region Member Variables

			private MdiTabGroup		tabGroup;
			private int				newExtent;

			#endregion //Member Variables

			#region TabGroupExtentInfo
			internal TabGroupExtentInfo(MdiTabGroup tabGroup, int newExtent)
			{
				Debug.Assert(tabGroup != null, "Invalid tab group!");

				this.tabGroup = tabGroup;
				this.newExtent = newExtent;
			}
			#endregion //TabGroupExtentInfo

			#region Properties

			#region TabGroup
			internal MdiTabGroup TabGroup
			{
				get { return this.tabGroup; }
			}
			#endregion //TabGroup

			#region NewExtent
			internal int NewExtent
			{
				get { return this.newExtent; }
				set { this.newExtent = value; }
			}
			#endregion //NewExtent

			#region IsExtentDifferent
			internal bool IsExtentDifferent
			{
				get { return this.tabGroup.Extent != this.newExtent; }
			}
			#endregion // IsExtentDifferent

			#endregion //Properties
		}
		#endregion //TabGroupExtentInfo

		#region TabGroupExtentInfoCollection
		private class TabGroupExtentInfoCollection : ICollection
		{
			#region Member Variables

			private ArrayList			list;

			#endregion //Member Variables

			#region Constructor
			internal TabGroupExtentInfoCollection() : this(5)
			{
			}

			internal TabGroupExtentInfoCollection(int capacity)
			{
				this.list = new ArrayList(capacity);
			}
			#endregion //Constructor

			#region Members

			#region Add
			internal TabGroupExtentInfo Add(MdiTabGroup tabGroup, int newExtent)
			{
				TabGroupExtentInfo extentInfo = new TabGroupExtentInfo(tabGroup, newExtent);

				this.list.Add(extentInfo);

				return extentInfo;
			}
			#endregion //Add

			#region ICollection
			public int Count
			{
				get { return this.list.Count; }
			}

			bool ICollection.IsSynchronized
			{
				get { return this.list.IsSynchronized; }
			}

			void ICollection.CopyTo(System.Array array, int index)
			{
				this.list.CopyTo(array, index);
			}

			object ICollection.SyncRoot
			{
				get { return this.list.SyncRoot; }
			}
			#endregion //ICollection

			#region IEnumerable
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.list.GetEnumerator();
			}
			#endregion //IEnumerable

			#region Indexers
			internal TabGroupExtentInfo this[int index]
			{
				get 
				{
					return this.list[index] as TabGroupExtentInfo;
				}
			}

			internal TabGroupExtentInfo this[MdiTabGroup tabGroup]
			{
				get 
				{
					foreach(TabGroupExtentInfo info in this.list)
					{
						if (info.TabGroup == tabGroup)
							return info;
					}

					return null;
				}
			}
			#endregion //Indexers

			#region CreateTabGroupResizingArgs
			internal MdiTabGroupResizingEventArgs CreateTabGroupResizingArgs()
			{
				MdiTabGroup[] tabGroups = new MdiTabGroup[this.list.Count];
				int[] newExtents = new int[this.list.Count];

				for(int i = 0; i < this.list.Count; i++)
				{
					TabGroupExtentInfo extentInfo = this[i];
					tabGroups[i] = extentInfo.TabGroup;
					newExtents[i] = extentInfo.NewExtent;
				}

				return new MdiTabGroupResizingEventArgs( new MdiTabGroupsCollection(tabGroups), newExtents);
			}
			#endregion //CreateTabGroupResizingArgs

			#endregion //Members
		}
		#endregion //TabGroupExtentInfoCollection

		#endregion //Nested classes			

	}
}
