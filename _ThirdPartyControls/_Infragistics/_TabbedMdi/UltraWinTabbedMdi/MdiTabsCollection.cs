#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections;
using Infragistics.Shared;
using System.Diagnostics;

using System.Runtime.Serialization;
using Infragistics.Shared.Serialization;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Abstract base class for a collection of <see cref="MdiTab"/> objects.
	/// </summary>
	/// <remarks>
	/// <p class="body">Each <b>MdiTab</b> object in the collection may be accessed using either the index or <b>Key</b>.</p>
	/// </remarks>
	/// <seealso cref="MdiTabGroup.Tabs"/>
	/// <seealso cref="UltraTabbedMdiManager.HiddenTabs"/>
	/// <seealso cref="HiddenMdiTabsCollection"/>
	/// <seealso cref="MdiTabsCollection"/>
	[Serializable()]
	[System.ComponentModel.TypeConverter(typeof(Infragistics.Shared.ExpandableCollectionConverter))]
	public abstract class MdiTabsBaseCollection : KeyedSubObjectsCollectionBase,
		ISerializable,
		IDeserializationCallback
	{
		#region Member Variables

		private int				initialCapacity = 4;

		#endregion //Member Variables

		#region Constructor

		#region Public
		/// <summary>
		/// Initializes a new <see cref="MdiTabsBaseCollection"/>
		/// </summary>
		protected MdiTabsBaseCollection()
		{
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabsBaseCollection"/> with the specified initial capacity
		/// </summary>
        /// <param name="initialCapacity">The number of elements that the new collection can initially store. </param>
		protected MdiTabsBaseCollection(int initialCapacity)
		{
			this.initialCapacity = initialCapacity;
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabsBaseCollection"/>
		/// </summary>
        /// <param name="tabs">An array of MdiTab objects with which to populate the collection.</param>
		protected MdiTabsBaseCollection(MdiTab[] tabs) : this()
		{
			if (tabs == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_25"), Shared.SR.GetString(null, "LE_V2_Exception_26"));

			foreach(MdiTab tab in tabs)
			{
				if (tab == null)
					throw new ArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_17"));
			}

			this.AddRange(tabs);
		}
		#endregion //Public

		#region Serialization Constructor
		/// <summary>
		/// Constructor used during deserialization to initialize a new <see cref="MdiTabsCollection"/> object
		/// with the serialized property values.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected MdiTabsBaseCollection( SerializationInfo info, StreamingContext context )
		{
			foreach( SerializationEntry entry in info )
			{
				if ( entry.Value is MdiTab )
				{
					MdiTab tab = (MdiTab)Utils.DeserializeProperty(entry, typeof(MdiTab), null);
			
					if (tab != null)
						this.InternalAdd( tab );			
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							this.initialCapacity =  Math.Max( this.initialCapacity, (int)Utils.DeserializeProperty(entry, typeof(int), this.initialCapacity));
							break;
						}

						// De-serialize the tag property
						//
						case MdiTabsCollection.TagSerializationName:
							this.DeserializeTag(entry);
							break;

						default:
						{
							// AS 4/30/03 FxCop Change
							// Explicitly call the overload that takes an IFormatProvider
							//
							//Debug.Assert( false, string.Format("Invalid entry in {0} de-serialization ctor", this.GetType().Name) );
							Debug.Assert( false, string.Format(null, "Invalid entry in {0} de-serialization ctor", this.GetType().Name) );
							break;
						}
					}
				}
			}
		}
		#endregion //Serialization Constructor

		#endregion //Constructor

		#region Properties

		#region Internal
		internal IList InternalTabList
		{
			get { return this.List; }
		}
		#endregion Internal

		#region Indexers
		/// <summary>
		/// Returns the <see cref="MdiTab"/> at the specified index.
		/// </summary>
		public MdiTab this[int index]
		{
			get
			{
				return this.GetItem(index) as MdiTab;
			}
		}

		/// <summary>
		/// Returns the <see cref="MdiTab"/> with the specified <see cref="KeyedSubObjectBase.Key"/>
		/// </summary>
		public MdiTab this[string key]
		{
			get
			{
				return this.GetItem(key) as MdiTab;
			}
		}
		#endregion //Indexers

		// AS 5/13/03
		// Do not enforce uniqueness within a tabs collection.
		//
		#region AllowDuplicateKeys
		/// <summary>
		/// Returns true if the collection allows 2 or more items to have the same key value.
		/// </summary>
		/// <remarks>
		/// This does not apply to items whose keys are null or empty. There can always be multiple items with null or empty keys unless the <see cref="KeyedSubObjectsCollectionBase.AllowEmptyKeys"/> property returns false.
		/// </remarks>
		public override bool AllowDuplicateKeys{ get { return true; } }
		#endregion //AllowDuplicateKeys

		#endregion // Properties

		#region Methods

		#region Public

		#region Contains
		/// <summary>
		/// Indicates if the specified <see cref="MdiTab"/> is a member of the collection.
		/// </summary>
		/// <param name="tab"><b>MdiTab</b> to evaluate</param>
		/// <returns><b>True</b> if the tab is a member of the collection; otherwise <b>false</b> is returned.</returns>
		public bool Contains( MdiTab tab )
		{
			return base.Contains(tab);
		}
		#endregion //Contains

		#region GetEnumerator

		/// <summary>
		/// Returns the type-safe enumerator for iterating through the <see cref="MdiTab"/> instances in the collection.
		/// </summary>
		/// <returns>A type safe enumerator for iterating through the <see cref="MdiTab"/> objects in the collection.</returns>
		/// <seealso cref="MdiTabEnumerator"/>
		public MdiTabEnumerator GetEnumerator() // non-IEnumerable version
		{
			return new MdiTabEnumerator(this);
		}

		#endregion GetEnumerator

		#endregion //Public

		#region Protected

		#region CreateArray (override)
		/// <summary>
		/// Virtual method used by the All 'get' method to create the array it returns.
		/// </summary>
		/// <returns>The newly created object array</returns>
		/// <remarks>This is normally overridden in a derived class to allocate a type safe array.</remarks>
		protected override object[] CreateArray()
		{
			return new MdiTab[ this.Count ];
		}
		#endregion CreateArray

		#region OnSubObjectPropChanged
		/// <summary>
		/// Invoked when a property has changed on a subobject.
		/// </summary>
		/// <param name="propChange">Object containing information about the change.</param>
		protected override void OnSubObjectPropChanged(Infragistics.Shared.PropChangeInfo propChange)
		{
			if (propChange.Source is MdiTab)
			{
				// pass along any property change notifications
				this.NotifyPropChange( TabbedMdiPropertyIds.MdiTab, propChange );
			}

			base.OnSubObjectPropChanged(propChange);
		}
		#endregion OnSubObjectPropChanged

		// AS 10/15/03 DNF116
		#region GetObjectData
		/// <summary>
		/// Invoked during the serialization of the object.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		// MD 1/4/07 - FxCop
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
		}
		#endregion //GetObjectData

		// AS 10/15/03 DNF116
		#region OnDeserialization
		/// <summary>
		/// Invoked after the entire object graph has been deserialized.
		/// </summary>
		/// <param name="sender">The object that initiated the callback. The functionality for the this parameter is not currently implemented.</param>
		protected virtual void OnDeserialization(object sender)
		{
		}
		#endregion //OnDeserialization

		#endregion Protected

		#region Internal
		internal void Add( MdiTab tab )
		{
			this.Insert(this.Count, tab);
		}

		internal void AddRange( MdiTab[] tabs )
		{
			this.InsertRange(this.Count, tabs);
		}

		internal void Insert( int index, MdiTab tab )
		{
			Debug.Assert( !this.Contains(tab), "The specified 'MdiTab' is already contained by the collection.");

			tab.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if (index < 0)
				index = 0;
			else if (index > this.Count)
				index = this.Count;

			this.InternalInsert(index, tab);

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTab );
		}

		internal void InsertRange( int index, MdiTab[] tabs )
		{
			foreach(MdiTab tab in tabs)
				Debug.Assert( !this.Contains(tab), "The specified 'MdiTab' is already contained by the collection.");

			if (index < 0)
				index = 0;
			else if (index > this.Count)
				index = this.Count;

			foreach(MdiTab tab in tabs)
			{
				tab.SubObjectPropChanged += this.SubObjectPropChangeHandler;

				this.InternalInsert(index, tab);

				index++;
			}

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTab );
		}

		internal void Remove( MdiTab tab )
		{
			Debug.Assert( this.Contains(tab), "The specified 'MdiTab' is not contained by the collection.");

			tab.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalRemove(tab);

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTab );
		}

		internal void RemoveAt( int index )
		{
			this.Remove( this[index] );
		}

		internal void Clear()
		{
			foreach(MdiTab tab in this)
				tab.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalClear();

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTab );
		}

		// AS 5/19/03 WTB968
		// Added a separate method to allow a tab to be
		// repositioned within the group followed by a
		// notification.
		//
		internal void Reposition(MdiTab tab, int newIndex)
		{
			int index = this.IndexOf(tab);

			if (index == -1)
				return;

			this.List.RemoveAt(index);
			this.List.Insert(newIndex, tab);

			this.NotifyPropChange( TabbedMdiPropertyIds.MdiTab );
		}
		#endregion //Internal

		#endregion //Methods

		#region Base class overrides
		/// <summary>
		/// Returns false to indicate that the collection may not be modified.
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// Returns the initial capacity for the collection.
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return this.initialCapacity;
			}
		}
		#endregion //Base class overrides

		#region Interfaces

		#region ISerializable
		/// <summary>
		/// Gets serialization information with all of the non-default information
		/// required to reinstantiate the object.
		/// </summary>
		// MD 1/4/07 - FxCop
		// We do not need a full demand here, just a link demand
		//[System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags=System.Security.Permissions.SecurityPermissionFlag.SerializationFormatter)]
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinTabbedMdi";

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			Utils.SerializeProperty(info, "Count", this.Count);
			
			// add each item in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				Utils.SerializeProperty(info, i.ToString(), this[i]);
			}

			// Serialize the tag property
			//
			if (this.ShouldSerializeTag())
				this.SerializeTag(info);

			// AS 10/15/03 DNF116
			// Call the virtual method so derived classes
			// can serialize additional info.
			//
			this.GetObjectData(info, context);
		}
		#endregion ISerializable

		#region IDeserializationCallback
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			foreach(MdiTab tab in this)
				tab.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// AS 10/15/03 DNF116
			// Call the virtual method so a derived class
			// could get the call.
			this.OnDeserialization(sender);
		}
		#endregion //IDeserializationCallback

		#endregion Interfaces

		#region MdiTabEnumerator class
		/// <summary>
		/// Type-specific enumerator class for enumerating over the <see cref="MdiTab"/> objects of the
		/// <see cref="MdiTabsBaseCollection"/>.
		/// </summary>
		/// <seealso cref="MdiTabsBaseCollection"/>
		/// <seealso cref="MdiTab"/>
		/// <seealso cref="MdiTabsBaseCollection.GetEnumerator()"/>
		public class MdiTabEnumerator : DisposableObjectEnumeratorBase
		{
			/// <summary>
			/// Initializes a new <see cref="MdiTabEnumerator"/>
			/// </summary>
			/// <param name="collection">Collection to iterate</param>
			public MdiTabEnumerator(MdiTabsBaseCollection collection) : base(collection)
			{
			}

			/// <summary>
			/// Returns the current <see cref="MdiTab"/> object in the enumerator.
			/// </summary>
			public MdiTab Current
			{
				get { return ((IEnumerator)this).Current as MdiTab; }
			}
		}
		#endregion //MdiTabEnumerator class
	}

	/// <summary>
	/// Read-only collection of <see cref="MdiTab"/> objects whose associated Forms are 
	/// not visible.
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>HiddenMdiTabsCollection</b> is a collection of <see cref="MdiTab"/> objects 
	/// whose associated Form is not visible. When an <b>MdiTab</b> is hidden or if it is not visible when it is made into an mdi child
	/// form, it will be added to the <see cref="UltraTabbedMdiManager.HiddenTabs"/> collection of the UltraTabbedMdiManager. 
	/// When the associated <see cref="MdiTab.Form"/> is displayed, the tab is moved from the <b>HiddenTabs</b> to an 
	/// <see cref="MdiTabGroup"/> and the <see cref="UltraTabbedMdiManager.TabDisplaying"/> and <see cref="UltraTabbedMdiManager.TabDisplayed"/> 
	/// events are invoked. <b>MdiTab</b> objects in the collection will not be associated with a particular <b>MdiTabGroup</b> 
	/// and therefore return null (Nothing in VB) for the <see cref="MdiTab.TabGroup"/> property.</p>
	/// <p class="body">Each <b>MdiTab</b> object in the collection may be accessed using either the index or <b>Key</b>.</p>
	/// </remarks>
	/// <seealso cref="UltraTabbedMdiManager.HiddenTabs"/>
	/// <seealso cref="MdiTabSettings.TabCloseAction"/>
	/// <seealso cref="MdiTab.Close()"/>
	/// <seealso cref="MdiTabsCollection"/>
	[Serializable()]
	public class HiddenMdiTabsCollection : MdiTabsBaseCollection,
		ISerializable,
		IDeserializationCallback
	{
		#region Constructor

		#region Public
		/// <summary>
		/// Initializes a new <see cref="HiddenMdiTabsCollection"/>
		/// </summary>
		public HiddenMdiTabsCollection()
		{
		}

		/// <summary>
		/// Initializes a new <see cref="HiddenMdiTabsCollection"/> with the specified initial capacity
		/// </summary>
        /// <param name="initialCapacity">The number of elements that the new collection can initially store. </param>
		public HiddenMdiTabsCollection(int initialCapacity) : base(initialCapacity)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabsCollection"/>
		/// </summary>
        /// <param name="tabs">An array of MdiTab objects with which to populate the collection.</param>
		public HiddenMdiTabsCollection(MdiTab[] tabs) : base(tabs)
		{
		}
		#endregion //Public

		#region Serialization Constructor
		/// <summary>
		/// Constructor used during deserialization to initialize a new <see cref="HiddenMdiTabsCollection"/> object
		/// with the serialized property values.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected HiddenMdiTabsCollection( SerializationInfo info, StreamingContext context ) : base(info, context)
		{
		}
		#endregion //Serialization Constructor

		#endregion //Constructor
	}

	/// <summary>
	/// Collection of <see cref="MdiTab"/> objects
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>MdiTabsCollection</b> is a collection of <see cref="MdiTab"/> objects. 
	/// Each <b>MdiTab</b> object in the collection may be accessed using either the index or <b>Key</b>.</p>
	/// <p class="body">The <b>MdiTabsCollection</b> is used by the <see cref="MdiTabGroup"/> to manage its 
	/// collection of <b>MdiTab</b> objects. The order of the items in the collection is the order in which 
	/// the items are displayed in the corresponding <see cref="MdiTabGroupControl"/>.</p>
	/// </remarks>
	/// <seealso cref="MdiTabGroup.Tabs"/>
	/// <seealso cref="HiddenMdiTabsCollection"/>
	[Serializable()]
	public class MdiTabsCollection : MdiTabsBaseCollection,
		ISerializable,
		IDeserializationCallback
	{
		#region Constructor

		#region Public
		/// <summary>
		/// Initializes a new <see cref="MdiTabsCollection"/>
		/// </summary>
		public MdiTabsCollection()
		{
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabsCollection"/> with the specified initial capacity
		/// </summary>
		public MdiTabsCollection(int initialCapacity) : base(initialCapacity)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabsCollection"/>
		/// </summary>
        /// <param name="tabs">An array of MdiTab objects with which to populate the collection.</param>
		public MdiTabsCollection(MdiTab[] tabs) : base(tabs)
		{
		}
		#endregion //Public

		#region Serialization Constructor
		/// <summary>
		/// Constructor used during deserialization to initialize a new <see cref="MdiTabsCollection"/> object
		/// with the serialized property values.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected MdiTabsCollection( SerializationInfo info, StreamingContext context ) : base(info, context)
		{
		}
		#endregion //Serialization Constructor

		#endregion //Constructor
	}
}
