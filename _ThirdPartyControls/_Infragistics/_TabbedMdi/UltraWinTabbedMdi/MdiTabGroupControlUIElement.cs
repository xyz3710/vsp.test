#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinTabs;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Main UIElement for the <see cref="MdiTabGroupControl"/>
	/// </summary>
	public class MdiTabGroupControlUIElement : ControlUIElementBase
	{
		#region Member Variables

		private UIElement				tabGroupElement = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupControlUIElement"/>
		/// </summary>
		/// <param name="tabGroupControl">Owning <b>MdiTabGroupControl</b></param>
		public MdiTabGroupControlUIElement(MdiTabGroupControl tabGroupControl) : this(tabGroupControl, tabGroupControl)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupControlUIElement"/>
		/// </summary>
		/// <param name="tabGroupControl">Owning <b>MdiTabGroupControl</b></param>
		/// <param name="ultraControl">Owning <b>IUltraControl</b></param>
		public MdiTabGroupControlUIElement(MdiTabGroupControl tabGroupControl, IUltraControl ultraControl) : base(tabGroupControl, ultraControl)
		{
		}
		#endregion //Constructor

		#region Properties

		private MdiTabGroupControl TabGroupControl
		{
			get { return this.Control as MdiTabGroupControl; }
		}

		private UIElement TabGroupElement
		{
			get
			{
				if (this.tabGroupElement == null && this.TabGroupControl.TabGroup != null)
					this.tabGroupElement = this.TabGroupControl.TabGroup.TabManager.CreateTabAreaUIElement(this);

				return this.tabGroupElement;
			}
		}

		#region BorderStyle
		/// <summary>
		/// BorderStyle of the element.
		/// <see cref="UIElementBorderStyle"/>
		/// </summary>
		public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return TabGroupInfoManager.TabGroupControlBorderStyle;
			}
			set 
			{
				base.BorderStyle = value;
			}
		}
		#endregion //BorderStyle

		#region BorderSides
		/// <summary>
		/// Flags indicating which sides of the element to
		/// draw borders.
		/// </summary>
		public override System.Windows.Forms.Border3DSide BorderSides
		{
			get
			{
				MdiTabGroup tabGroup =  this.TabGroupControl.TabGroup;

				if (tabGroup == null || tabGroup.Manager == null)
					return 0;

				return tabGroup.Manager.TabGroupManager.GetTabGroupBorderSide(tabGroup);
			}
		}
		#endregion //BorderSides

		#endregion //Properties

		#region Methods

		#region DrawBorders

		// JDN 8/12/04 NAS2004 Vol3 - Support for VisualStudio2005 style
		/// <summary>
		/// Invoked when the element should render its borders.
		/// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBorders(ref Infragistics.Win.UIElementDrawParams drawParams)
		{
			AppearanceData appearanceData = new AppearanceData();
			AppearancePropFlags propFlags = AppearancePropFlags.BorderColor | AppearancePropFlags.BorderColor3DBase;
			// AS 5/5/06 Added orientation specific roles.
			//
			//this.TabGroupControl.TabGroup.Manager.ResolveSplitterAppearance( ref appearanceData, ref propFlags );
			//drawParams.DrawBorders( this.BorderStyle, this.BorderSides, appearanceData.BorderColor,
			//	appearanceData.BorderColor3DBase, this.Rect, drawParams.InvalidRect );
			Border3DSide sides = this.BorderSides;

			if (sides != 0)
			{
				this.TabGroupControl.TabGroup.Manager.ResolveSplitterAppearance( ref appearanceData, ref propFlags,
					0 != (sides & (Border3DSide.Left | Border3DSide.Right))
					? Orientation.Vertical 
					: Orientation.Horizontal);

				drawParams.DrawBorders( this.BorderStyle, sides, appearanceData.BorderColor,
					appearanceData.BorderColor3DBase, this.Rect, drawParams.InvalidRect );
			}
		}

		#endregion //DrawBorders

		#region PositionChildElements (protected override)
		/// <summary>
		/// Handles positioning of the child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			this.ChildElements.Clear();

			if (this.TabGroupElement == null)
				return;

			this.TabGroupElement.Rect = this.RectInsideBorders;

			this.ChildElements.Add( this.TabGroupElement );
		}
		#endregion //PositionChildElements (protected override)

		// AS 10/10/03 WTB1074
		#region OnMouseDown
		/// <summary>
		/// Called when the mouse down message is received over the element. 
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		/// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
		/// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
		/// <returns>If true then bypass default processing</returns>
		protected override bool OnMouseDown( System.Windows.Forms.MouseEventArgs e, 
			bool adjustableArea,
			ref UIElement captureMouseForElement )
		{
			bool returnValue = base.OnMouseDown(e, adjustableArea, ref captureMouseForElement);

			// if we are enabled and the 
			// left mouse button is used, try to active the 
			// selected tab in the tabgroup if it is not actived.
			// 
			if (this.Enabled	&&
				e.Button == System.Windows.Forms.MouseButtons.Left)
			{
				// get the tab group and manager
				MdiTabGroup tabGroup = 	this.TabGroupControl == null ? null : this.TabGroupControl.TabGroup;
				UltraTabbedMdiManager mdiManager = tabGroup == null ? null : tabGroup.Manager;

				// assuming its not active and has a selected tab,
				// lets activate the tab
				if (mdiManager != null						&& 
					mdiManager.ActiveTabGroup != tabGroup	&&
					tabGroup.HasTabs						&&
					tabGroup.SelectedTab != null			&&
					tabGroup.SelectedTab.CanActivate)
				{
					tabGroup.SelectedTab.Activate();
				}
			}

			return returnValue;
		}
		#endregion OnMouseDown

		#endregion //Methods
	}
}
