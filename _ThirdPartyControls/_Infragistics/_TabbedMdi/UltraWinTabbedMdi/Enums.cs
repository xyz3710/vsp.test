#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using Infragistics.Win.UltraWinTabs;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	#region TabbedMdiPropertyIds
	/// <summary>
	/// Enumeration of property ids for the <see cref="UltraTabbedMdiManager"/> and its associated classes.
	/// </summary>
	public enum TabbedMdiPropertyIds
	{
		/// <summary>
		/// <see cref="MdiTabSettings.ActiveTabAppearance"/> property
		/// </summary>
		ActiveTabAppearance,

		/// <summary>
		/// <see cref="MdiTabSettings.AllowClose"/> property
		/// </summary>
		AllowClose,

		/// <summary>
		/// <see cref="MdiTabSettings.AllowDrag"/> property
		/// </summary>
		AllowDrag,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.AllowDrop"/> property
		/// </summary>
		AllowDrop,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.AllowHorizontalTabGroups"/> property
		/// </summary>
		AllowHorizontalTabGroups,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.AllowVerticalTabGroups"/> property
		/// </summary>
		AllowVerticalTabGroups,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.Appearance"/> property
		/// </summary>
		Appearance,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.AutoSelect"/> property
		/// </summary>
		AutoSelect,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.AutoSelectDelay"/> property
		/// </summary>
		AutoSelectDelay,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.Appearances"/> property
		/// </summary>
		Appearances,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ButtonStyle"/> property
		/// </summary>
		ButtonStyle,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.CloseButtonAppearance"/> property
		/// </summary>
		CloseButtonAppearance,

		/// <summary>
		/// <see cref="MdiTabSettings.DisplayFormIcon"/> property
		/// </summary>
		DisplayFormIcon,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.HiddenTabs"/> property
		/// </summary>
		HiddenTabs,

		/// <summary>
		/// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup.Extent"/> property
		/// </summary>
		Extent,

		/// <summary>
        /// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabSettings.HotTrack"/> property
        /// </summary>
		HotTrack,

		/// <summary>
		/// <see cref="MdiTabSettings.HotTrackTabAppearance"/> property
		/// </summary>
		HotTrackTabAppearance,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.ImageList"/> property
		/// </summary>
		ImageList,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.ImageSize"/> property
		/// </summary>
		ImageSize,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.ImageTransparentColor"/> property
		/// </summary>
		ImageTransparentColor,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.InterTabSpacing"/> property
		/// </summary>
		InterTabSpacing,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.MaxTabGroups"/> property
		/// </summary>
		MaxTabGroups,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.MaxTabWidth"/> property
		/// </summary>
		MaxTabWidth,

		/// <summary>
		/// <see cref="MdiTab"/>
		/// </summary>
		MdiTab,

		/// <summary>
		/// <see cref="MdiTabGroup"/>
		/// </summary>
		MdiTabGroup,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.MinTabWidth"/> property
		/// </summary>
		MinTabWidth,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.Orientation"/> property
		/// </summary>
		Orientation,

		/// <summary>
        /// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTab.PersistedInfo"/> property
        /// </summary>
		PersistedInfo,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ScrollArrowStyle"/> property
		/// </summary>
		ScrollArrowStyle,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ScrollButtonAppearance"/> property
		/// </summary>
		ScrollButtonAppearance,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ScrollButtons"/> property
		/// </summary>
		ScrollButtons,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ScrollButtonTypes"/> property
		/// </summary>
		ScrollButtonTypes,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ScrollTrackAppearance"/> property
		/// </summary>
		ScrollTrackAppearance,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ScrollTrackExtent"/> property
		/// </summary>
		ScrollTrackExtent,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ShowButtonSeparators"/> property
		/// </summary>
		ShowButtonSeparators,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.ShowCloseButton"/> property
		/// </summary>
		ShowCloseButton,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.ShowToolTips"/> property
		/// </summary>
		ShowToolTips,

		/// <summary>
		/// <see cref="MdiTabSettings.SelectedTabAppearance"/> property
		/// </summary>
		SelectedTabAppearance,

		/// <summary>
        /// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTab.Settings"/> and <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup.Settings"/> properties
        /// </summary>
		Settings,
	
		/// <summary>
		/// <see cref="UltraTabbedMdiManager.SplitterAppearance"/> property
		/// </summary>
		SplitterAppearance,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.SplitterBorderStyle"/> property
		/// </summary>
		SplitterBorderStyle,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.SplitterWidth"/> property
		/// </summary>
		SplitterWidth,

		/// <summary>
		/// <see cref="MdiTabSettings.TabAppearance"/> property
		/// </summary>
		TabAppearance,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabAreaAppearance"/> property
		/// </summary>
		TabAreaAppearance,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabButtonStyle"/> property
		/// </summary>
		TabButtonStyle,

		/// <summary>
		/// <see cref="MdiTabSettings.TabCloseAction"/> property
		/// </summary>
		TabCloseAction,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.TabGroups"/> property
		/// </summary>
		TabGroups,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.TabGroupSettings"/> property
		/// </summary>
		TabGroupSettings,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabHeight"/> property
		/// </summary>
		TabHeight,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabPadding"/> property
		/// </summary>
		TabPadding,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabOrientation"/> property
		/// </summary>
		TabOrientation,

		/// <summary>
        /// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup.Tabs"/> property
        /// </summary>
		Tabs,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabSizing"/> property
		/// </summary>
		TabSizing,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabStyle"/> property
		/// </summary>
		TabStyle,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabsPerRow"/> property
		/// </summary>
		TabsPerRow,

		/// <summary>
        /// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabGroup.TabSettings"/> and <see cref="UltraTabbedMdiManager.TabSettings"/> properties
        /// </summary>
		TabSettings,

		/// <summary>
        /// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabSettings.TabWidth"/> property
        /// </summary>
		TabWidth,

		/// <summary>
        /// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTab.Text"/> property
        /// </summary>
		Text,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.TextOrientation"/> property
		/// </summary>
		TextOrientation,

		/// <summary>
        /// <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTab.ToolTip"/> property
        /// </summary>
		ToolTip,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.UseMnemonics"/> property
		/// </summary>
		UseMnemonics,

		// AS 4/29/03 WTB880
		/// <summary>
		/// <see cref="UltraTabbedMdiManager.BorderStyle"/> property
		/// </summary>
		BorderStyle,

		// AS 5/1/03 WTB880
		/// <summary>
		/// <see cref="UltraTabbedMdiManager.BorderColor"/> property
		/// </summary>
		BorderColor,

		// AS 2/6/04 WTB1314
		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabAreaMargins"/> property
		/// </summary>
		TabAreaMargins,

		//	BF 7.20.04
		/// <summary>
		/// The <see cref="UltraTabbedMdiManager.ViewStyle"/> property
		/// </summary>
		ViewStyle,

		// JDN 8/18/04 NAS2004 Vol3
		/// <summary>
		/// <see cref="MdiTabGroupSettings.ShowPartialTabs"/> property
		/// </summary>
		ShowPartialTabs,

		// JDN 8/18/04 NAS2004 Vol3
		/// <summary>
		/// <see cref="MdiTabGroupSettings.ShowTabListButton"/> property
		/// </summary>
		ShowTabListButton,

		// JDN 8/18/04 NAS2004 Vol3
		/// <summary>
		/// <see cref="MdiTabGroupSettings.TabListButtonAppearance"/> property
		/// </summary>
		TabListButtonAppearance,

		// JAS 12/20/04 Tab Spacing
		/// <summary>
		/// <see cref="MdiTabGroupSettings.SpaceAfterTabs"/> property
		/// </summary>
		SpaceAfterTabs,

		// JAS 12/20/04 Tab Spacing
		/// <summary>
		/// <see cref="MdiTabGroupSettings.SpaceBeforeTabs"/> property
		/// </summary>
		SpaceBeforeTabs,

		// AS 3/7/05 NA 2005 Vol 2 - Nested TabGroups
		/// <summary>
		/// <see cref="UltraTabbedMdiManager.AllowNestedTabGroups"/> property
		/// </summary>
		AllowNestedTabGroups,

		// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
		/// <summary>
		/// <see cref="UltraTabbedMdiManager.IsActiveTabGroupMaximized"/> property
		/// </summary>
		IsActiveTabGroupMaximized,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.MaximizedTabGroupDisplayStyle"/> property
		/// </summary>
		MaximizedTabGroupDisplayStyle,

		/// <summary>
		/// <see cref="UltraTabbedMdiManager.AllowMaximize"/> property
		/// </summary>
		AllowMaximize,

		// AS 5/22/07 BR23151 - MdiTabNavigationMode 
		/// <summary>
		/// <see cref="UltraTabbedMdiManager.TabNavigationMode"/> property
		/// </summary>
		TabNavigationMode,

		// MD 11/8/07 - NA 2008 Vol 1
		/// <summary>
		/// <see cref="MdiTabSettings.CloseButtonAlignment"/> property
		/// </summary>
		CloseButtonAlignment,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.CloseButtonLocation"/> property
		/// </summary>
		CloseButtonLocation,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.HotTrackCloseButtonAppearance"/> property
		/// </summary>
		HotTrackCloseButtonAppearance,

		/// <summary>
		/// <see cref="MdiTabGroupSettings.PressedCloseButtonAppearance"/> property
		/// </summary>
		PressedCloseButtonAppearance,

		/// <summary>
		/// <see cref="MdiTabSettings.CloseButtonVisibility"/> property
		/// </summary>
		CloseButtonVisibility,
	}
	#endregion //TabbedMdiPropertyIds

	#region MdiTabCloseAction
	/// <summary>
	/// Enumeration of possible actions to take when the a tab is closed.
	/// </summary>
	/// <seealso cref="MdiTabSettings.TabCloseAction"/>
	public enum MdiTabCloseAction
	{
		/// <summary>
		/// Default
		/// </summary>
		Default = 0,	

		/// <summary>
		/// Invokes the close method of the form.
		/// </summary>
		Close = 1,

		/// <summary>
		/// Invokes the hide method of the form.
		/// </summary>
		Hide = 2,

		/// <summary>
		/// The form is not affected.
		/// </summary>
		None = 3
	}
	#endregion //MdiTabCloseAction

	#region MdiTabDragStyle

	/// <summary>
	/// Enumeration for where an <see cref="MdiTab"/> can be repositioned.
	/// </summary>
	/// <seealso cref="MdiTabSettings.AllowDrag"/>
	public enum MdiTabDragStyle
	{
		/// <summary>
		/// Default
		/// </summary>
		Default = 0,

		/// <summary>
		/// The tab may not be dragged
		/// </summary>
		None = 1,

		/// <summary>
		/// The tab may be repositioned within the tab group.
		/// </summary>
		WithinGroup = 2,
		
		/// <summary>
		/// The tab may be repositioned within the tab group or dragged to other tab groups.
		/// </summary>
		WithinAndAcrossGroups = 3
	}
	#endregion //MdiTabDragStyle

	// AS 5/22/07 BR23151 - MdiTabNavigationMode 
	#region MdiTabNavigationMode
	/// <summary>
	/// Enumeration used to determine which form will be activated when using the keyboard to select the next or previous tab.
	/// </summary>
	public enum MdiTabNavigationMode
	{
		/// <summary>
		/// The tabs are navigated to based on the order in which they have been activated. This model follows implementations such as Visual Studio.
		/// </summary>
		ActivationOrder,

		/// <summary>
		/// The tabs are navigated to based on their visual order.
		/// </summary>
		VisibleOrder,
	} 
	#endregion //MdiTabNavigationMode

	#region MdiTabPosition
	/// <summary>
	/// Enumeration of <see cref="MdiTab"/> positions
	/// </summary>
	/// <seealso cref="MdiTab.Reposition"/>
	public enum MdiTabPosition
	{
		/// <summary>
		/// First visible tab in the same TabGroup as the specified tab
		/// </summary>
		First = 0,

		/// <summary>
		/// Last visible tab in the same TabGroup as the specified tab.
		/// </summary>
		Last = 1,
		
		/// <summary>
		/// The tab is positioned immediately after the specified tab
		/// </summary>
		Next = 2,

		/// <summary>
		/// The tab is positioned immediately before the specified tab.
		/// </summary>
		Previous = 3,
	}
	#endregion //MdiTabPosition

	#region MdiTabGroupPosition
	/// <summary>
	/// Enumeration of <see cref="MdiTabGroup"/> positions
	/// </summary>
	/// <seealso cref="MdiTab.MoveToGroup(MdiTabGroup)"/>
	/// <seealso cref="MdiTabGroup.MoveAllTabs"/>
	/// <seealso cref="UltraTabbedMdiManager.MoveToGroup(MdiTab, MdiTabGroupPosition, int)"/>
	public enum MdiTabGroupPosition
	{
		/// <summary>
		/// First TabGroup
		/// </summary>
		First = 0,

		/// <summary>
		/// Last TabGroup
		/// </summary>
		Last = 1,

		/// <summary>
		/// Next TabGroup
		/// </summary>
		Next = 2,

		/// <summary>
		/// Previous TabGroup
		/// </summary>
		Previous = 3,
	}
	#endregion //MdiTabGroupPosition

	// MD 1/7/08 - BR29406
	/// <summary>
	/// Represents the reason why an mdi tab is closing or has closed.
	/// </summary>
	public enum MdiTabCloseReason
	{
		/// <summary>
		/// The close button was clicked.
		/// </summary>
		CloseButton = TabCloseReason.CloseButton,

		/// <summary>
		/// The <see cref="MdiTab.Close()"/> method was called on the tab.
		/// </summary>
		CloseMethod = TabCloseReason.CloseMethod,

		/// <summary>
		/// The middle mouse button was clicked on the tab.
		/// </summary>
		MiddleMouseButton = TabCloseReason.MiddleMouseButton,

		/// <summary>
		/// The mdi child form was closed.
		/// </summary>
		FormClosed = 3,

		/// <summary>
		/// The Close menu item was clicked from the tab's context menu.
		/// </summary>
		CloseMenuItem = 4,
	}

	#region MdiTabContextMenu
	/// <summary>
	/// Enumeration of <see cref="System.Windows.Forms.ContextMenu"/> types displayed for the <see cref="UltraTabbedMdiManager.InitializeContextMenu"/> event
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager.InitializeContextMenu"/>
	/// <seealso cref="MdiTabContextMenuEventArgs.ContextMenuType"/>
	public enum MdiTabContextMenu
	{
		/// <summary>
		/// Default tab context menu that appears when right clicking on a tab
		/// </summary>
		Default = 0,

		/// <summary>
		/// Context menu displayed when dragging a tab into the main area of the MdiClient.
		/// </summary>
		Reposition = 1,
	}
	#endregion //MdiTabContextMenu

	#region DefaultableTabScrollButtons
	/// <summary>
	/// Defaultable enumeration of scroll buttons
	/// </summary>
	/// <seealso cref="MdiTabGroupSettings.ScrollButtons"/>
	public enum DefaultableTabScrollButtons
	{
		/// <summary>
		/// Default
		/// </summary>
		Default,

		/// <summary>
		/// Scroll buttons are never displayed
		/// </summary>
		None,

		/// <summary>
		/// Scroll buttons are always displayed
		/// </summary>
		Always,

		/// <summary>
		/// Scroll buttons are only displayed when there is not enough room to display all the visible tabs.
		/// </summary>
		Automatic,
	}
	#endregion //DefaultableTabScrollButtons

	#region MdiTabDropAction
	/// <summary>
	/// Enumerations of <see cref="MdiTab"/> drag drop actions
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager.TabDragOver"/>
	/// <seealso cref="MdiTabDragOverEventArgs.DropAction"/>
	public enum MdiTabDropAction
	{
		/// <summary>
		/// The tab is being repositioned in an <see cref="MdiTabGroup"/>.
		/// </summary>
		TabGroup,

		/// <summary>
		/// The tab will be positioned in a new horizontal group
		/// </summary>
		NewHorizontalGroup,

		/// <summary>
		/// The tab will be positioned in a new vertical group
		/// </summary>
		NewVerticalGroup,

		/// <summary>
		/// A context menu will be displayed to determine the action taken.
		/// </summary>
		ContextMenu,

		/// <summary>
		/// No action is being taken. Occurs when the mouse is released over an invalid location.
		/// </summary>
		None,
	}
	#endregion //MdiTabDropAction

	#region MdiTabMenuItems
	/// <summary>
	/// Enumeration of the menu items displayed by the <see cref="UltraTabbedMdiManager"/>
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager.InitializeContextMenu"/>
	/// <seealso cref="MdiTabContextMenuEventArgs.GetMenuType"/>
	public enum MdiTabMenuItems
	{
		/// <summary>
		/// Close. Menu item to close the active tab
		/// </summary>
		Close = 0,

		/// <summary>
		/// Separator
		/// </summary>
		CloseSeparator = 1,

		/// <summary>
		/// New Horizontal TabGroup. Menu item to create a new Horizontal <see cref="MdiTabGroup"/>
		/// </summary>
		NewHorizontalGroup = 2,

		/// <summary>
		///  New Vertical TabGroup. Menu item to create a new Vertical <see cref="MdiTabGroup"/>
		/// </summary>
		NewVerticalGroup = 3,

		/// <summary>
		/// Move to Next Tab Group. Menu item to move the active tab to the next <see cref="MdiTabGroup"/>
		/// </summary>
		MoveToNextGroup = 4,

		/// <summary>
		/// Move to Previous Tab Group. Menu item to move the active tab to the previous <see cref="MdiTabGroup"/>
		/// </summary>
		MoveToPreviousGroup = 5,

		/// <summary>
		/// Separator
		/// </summary>
		CancelSeparator = 6,

		/// <summary>
		/// Cancel. Used during a drag operation context menu to cancel the drag action.
		/// </summary>
		Cancel = 7,

		/// <summary>
		/// Separator that preceeds the Maximize menu entry
		/// </summary>
		MaximizeSeparator = 8,

		// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
		/// <summary>
		/// Maximized. Used to enabled or disable the <see cref="UltraTabbedMdiManager.IsActiveTabGroupMaximized"/>
		/// </summary>
		Maximize = 9,
	}
	#endregion //MdiTabMenuItems

	// AS 4/29/03 WTB880
	#region MdiClientBorderStyle
	/// <summary>
	/// Specifies the border style of the <b>MdiClient</b>
	/// </summary>
	/// <seealso cref="UltraTabbedMdiManager.BorderStyle"/>
	public enum MdiClientBorderStyle
	{
		/// <summary>
		/// A single line border
		/// </summary>
		// AS 5/1/03 Changed to match our border style
		//FixedSingle,
		Solid,

		/// <summary>
		/// A three-dimensional sunken border
		/// </summary>
		// AS 5/1/03 Changed to match our border style
		//Fixed3D,
		Inset,

		/// <summary>
		/// No border
		/// </summary>
		None,
	}
	#endregion //MdiClientBorderStyle

	#region ViewStyle

	/// <summary>
	/// Determines the view style for the control.
	/// </summary>
	public enum ViewStyle
	{
		// AS 5/2/06 AppStyling
		// Added default and moved Standard to the end.
		//
		/// <summary>
		/// Default. When otherwise unspecified, the default view style is <b>Standard</b>.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Controls will emulate the visual appearance seen in Office2003 applications.
		/// </summary>
		Office2003 = 1,

		/// <summary>
		/// Controls will emulate the visual appearance seen in Visual Studio 2005.
		/// </summary>
		// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
		VisualStudio2005 = 2,

		// AS 5/2/06 AppStyling
		// Moved to allow a default to be added.
		//
		/// <summary>
		/// Controls will appear as they did in previous versions.
		/// </summary>
		Standard = 3,

        // MBS 8/2/06 <Office2007 look & feel>
        /// <summary>
        /// Controls will emulate the visual appearance seen in Office2007.
        /// </summary>
        Office2007,
}
	
	#endregion //ViewStyle

	// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
	#region MaximizedMdiTabGroupDisplayStyle
	/// <summary>
	/// Enumeration indicating how the unmaximized groups are displayed while <see cref="UltraTabbedMdiManager.IsActiveTabGroupMaximized"/> is set to true.
	/// </summary>
	public enum MaximizedMdiTabGroupDisplayStyle
	{
		/// <summary>
		/// The splitter bars of sibling and ancestor groups will be displayed.
		/// </summary>
		CompressUnmaximizedGroups,

		/// <summary>
		/// All of the mdi client area is allocated to display the maximized group. No portion of the non-maximized groups is displayed.
		/// </summary>
		ShowMaximizedGroupOnly,
	}
	#endregion //MaximizedMdiTabGroupDisplayStyle
}
