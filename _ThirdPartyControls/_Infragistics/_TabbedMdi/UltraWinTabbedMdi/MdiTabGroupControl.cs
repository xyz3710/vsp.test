#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Win.UltraWinTabs;
using Infragistics.Win.Layout;		// AS 6/14/04 DNF190

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Control use to manage the display of the <see cref="MdiTabGroup.Tabs"/> of an <see cref="MdiTabGroup"/>
	/// </summary>
	/// <remarks>
	/// <p class="body">Instances of this control are created by the <see cref="UltraTabbedMdiManager"/> 
	/// for each <see cref="MdiTabGroup"/> using the <see cref="UltraTabbedMdiManager.CreateMdiTabGroupControl"/>. 
	/// The control displays the uielements for the tabs as well as the scroll and close buttons. The size and 
	/// position of the control is managed by the owning UltraTabbedMdiManager.</p>
	/// </remarks>
	[ToolboxItem(false)]
	[DefaultProperty("TabGroup")]
	public class MdiTabGroupControl : UltraControlBase,
		ILayoutItem			// AS 6/14/04 DNF190
	{
		#region Member Variables

		private MdiTabGroup						tabGroup = null;
		private IUltraControl					ultraControl = null;
		private MdiTabGroupControlUIElement		controlUIElement = null;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupControl"/>
		/// </summary>
		/// <param name="tabGroup">Class providing the tab information</param>
		public MdiTabGroupControl(MdiTabGroup tabGroup) : this(tabGroup, null)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="MdiTabGroupControl"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">This constructor is used by the <see cref="UltraTabbedMdiManager"/> when 
		/// creating an instance of the control so that the <b>MdiTabGroupControl</b> will use the 
		/// <b>IUltraControl</b> of the <b>UltraTabbedMdiManager</b>.</p>
		/// </remarks>
		/// <param name="tabGroup">Class providing the tab information</param>
		/// <param name="ultraControl">Owning <b>IUltraControl</b></param>
		public MdiTabGroupControl(MdiTabGroup tabGroup, IUltraControl ultraControl)
		{
			this.tabGroup = tabGroup;
			this.ultraControl = ultraControl;

			// the control does not take focus
			this.SetStyle(ControlStyles.Selectable, false);
			this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(UltraTabbedMdiManager.DoubleBufferControlStyle, false);
        }
		#endregion //Constructor

		#region Properties

		#region ControlUIElement
		/// <summary>
		/// Returns the main uielement representing the control
		/// </summary>
		protected override Infragistics.Win.ControlUIElementBase ControlUIElement
		{
			get
			{
				if (this.controlUIElement == null)
				{
					if (this.ultraControl != null)
						this.controlUIElement = new MdiTabGroupControlUIElement(this,this.ultraControl);
					else
						this.controlUIElement = new MdiTabGroupControlUIElement(this);
				}

				return this.controlUIElement;
			}
		}
		#endregion ControlUIElement

		#region TabGroup
		/// <summary>
		/// Returns the associated <see cref="MdiTabGroup"/>
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public MdiTabGroup TabGroup
		{
			get { return this.tabGroup; }
		}
		#endregion //TabGroup

		#endregion //Properties

		#region Methods

		#region ProcessMnemonic

		/// <summary>
		/// Processes a mnemonic character.
		/// </summary>
		/// <param name="character">The character to process.</param>
		/// <returns><b>true</b> if the character was processed as a mnemonic by the control; otherwise, <b>false</b>.</returns>
		protected override bool ProcessMnemonic ( char character )
		{
			if (!this.Enabled || !this.Visible)
				return false;

			MdiTabGroup tabGroup = this.TabGroup;

			if (tabGroup == null			||
				tabGroup.Disposed			|| 
				tabGroup.Manager == null	||
				!tabGroup.Manager.UseMnemonics)
				return false;

			return tabGroup.Manager.ProcessMnemonic(character, tabGroup);
		}

		#endregion ProcessMnemonic

        // MBS 4/5/06 BR11210
        // Manually show a context menu and invalidate the control so that the rectangle
        // of the context menu will not show on the tab
        #region OnMouseUp
        /// <summary>
        /// Shows a context menu at the specified point on a right-click
        /// </summary>
        /// <param name="e"><see cref="MouseEventArgs"/></param>
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (e.Button == MouseButtons.Right && this.ContextMenu == null)
            {
                UltraTabbedMdiManager manager = this.TabGroup == null ? null : this.TabGroup.Manager;
                if (manager != null)
                {
					System.Drawing.Point point = new System.Drawing.Point(e.X, e.Y);

					// MD 9/23/08 - TFS7665
					// The mnemonic characters will not work with owner draw menu items when the context menu is shown manully. To get around this,
					// we need to temporarily set the ContextMenu property of this control to the menu being shown.
					//manager.TabContextMenu.Show( this, point );
					try
					{
						this.ContextMenu = manager.TabContextMenu;
						manager.TabContextMenu.Show( this, point );
					}
					finally
					{
						this.ContextMenu = null;
					}

                    this.Invalidate();
                }
            }
        }
        #endregion //OnMouseUp

		#region OnPaint
		/// <summary>
		/// Calls the ControlUIElement's draw method
		/// </summary>
		/// <param name="pe"><see cref="PaintEventArgs"/></param>
		protected override void OnPaint(PaintEventArgs pe)
		{
			if (this.IsUpdating)
				return;

			UltraTabbedMdiManager manager = this.TabGroup == null ? null : this.TabGroup.Manager;

			if (manager != null)
			{
				if (manager.IsUpdating)
					return;

				AlphaBlendMode alphaMode = manager.AlphaBlendMode;

				this.ControlUIElement.Draw( pe.Graphics, 
    				pe.ClipRectangle,
	   			    !this.GetStyle( UltraTabbedMdiManager.DoubleBufferControlStyle ),
					alphaMode);

				return;
			}

			base.OnPaint(pe);
		}
		#endregion //OnPaint

		#region Dispose
		/// <summary>
		/// Invoked when the component must release the resources it is using.
		/// </summary>
		/// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				// AS 1/5/04 WTB1271
				if (this.controlUIElement != null)
				{
					this.controlUIElement.Dispose();
					this.controlUIElement = null;
				}
			}

			base.Dispose(disposing);
		}
		#endregion //Dispose

		// MRS 04/12/04 - Added Accessibility support
		#region CreateAccessibilityInstance

		/// <summary>
		/// Creates a new accessibility object for the control.
		/// </summary>
		/// <returns>A new <see cref="AccessibleObject"/> for the control.</returns>
		protected override System.Windows.Forms.AccessibleObject CreateAccessibilityInstance()
		{
			return new MdiTabGroupControl.MdiTabGroupControlAccessibleObject(this);
		}

		#endregion CreateAccessibilityInstance

		// MRS 04/12/04 - Added Accessibility support
		#region AccessibilityNotifyClientsInternal
		internal void AccessibilityNotifyClientsInternal(AccessibleEvents accEvent, int childID)
		{
			// Get out if everything isn't in order
			if (this.IsDisposed ||
				this.Disposing	||
				!this.IsHandleCreated )
				return;

			// If this is a focus notification, make sure the control has focus
			if (accEvent == AccessibleEvents.Focus &&
				!this.Focused )
				return;

			this.AccessibilityNotifyClients( accEvent, childID );
		}

		#endregion AccessibilityNotifyClientsInternal

		#endregion //Methods

		// MRS 04/12/04 - Added Accessibility support
		#region Public Class MdiTabGroupAccessibleObject

		/// <summary>
		/// The Accessible object for a mdiTabGroupControl.
		/// </summary>
		/// <seealso cref="MdiTabGroup"/>
		protected class MdiTabGroupControlAccessibleObject : UltraControlBase.UltraControlAccessibleObject
		{
			#region Private Members

			private MdiTabGroupControl mdiTabGroupControl;

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor
			/// </summary>
            /// <param name="mdiTabGroupControl">The MdiTabGroupControl</param>
			public MdiTabGroupControlAccessibleObject ( MdiTabGroupControl mdiTabGroupControl ) : base( mdiTabGroupControl )
			{
				this.mdiTabGroupControl = mdiTabGroupControl;
			}

			#endregion Constructor

			#region Base Class Overrides

				#region Bounds

			/// <summary>
			/// Gets the location and size of the accessible object.
			/// </summary>
			public override System.Drawing.Rectangle Bounds
			{
				get
				{
					return base.Bounds;
				}
			}

				#endregion Bounds

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				
				if ( index < 0 || index >= this.GetChildCount())
					return null;

				if (index < this.MdiTabGroupControl.TabGroup.Tabs.Count )
				{
					MdiTab tab = this.MdiTabGroupControl.TabGroup.Tabs[index] as MdiTab;

					if ( tab == null )
						return null;

					return tab.AccessibilityObject;

				}
				else 
					index -= this.MdiTabGroupControl.TabGroup.Tabs.Count;					

				if ( index < base.GetChildCount() )
				{
					return base.GetChild(index);
				}				
				else
					index -= base.GetChildCount();

				ScrollButtonTypes scrollButtonTypes = this.mdiTabGroupControl.TabGroup.TabManager.ScrollButtonTypesResolved;
				if ( scrollButtonTypes != ScrollButtonTypes.None )
				{
					if ( index == 0 )
					{
						TabScrollButtonUIElement tabScrollButtonUIElement = (TabScrollButtonUIElement)this.MdiTabGroupControl.ControlUIElement.GetDescendant(typeof(TabScrollButtonUIElement));
						if ( tabScrollButtonUIElement != null)							
							return tabScrollButtonUIElement.AccessibilityInstance;
					}
					else if ( index == 1 )
					{
						TabCloseButtonUIElement tabCloseButtonUIElement = (TabCloseButtonUIElement)this.mdiTabGroupControl.controlUIElement.GetDescendant(typeof(TabCloseButtonUIElement));
						if ( tabCloseButtonUIElement != null)							
							return tabCloseButtonUIElement.AccessibilityInstance;
					}
				}
				else
				{
					// MD 12/3/07 - NA 2008 Vol 1
					// Only return the header area close button accessibility object if the close button location 
					// is HeaderArea.
					if ( this.MdiTabGroupControl.TabGroup.SettingsResolved.CloseButtonLocation == TabCloseButtonLocation.HeaderArea )
					{
						if ( index == 0 )
						{
							TabCloseButtonUIElement tabCloseButtonUIElement = (TabCloseButtonUIElement)this.mdiTabGroupControl.controlUIElement.GetDescendant( typeof( TabCloseButtonUIElement ) );
							if ( tabCloseButtonUIElement != null )
								return tabCloseButtonUIElement.AccessibilityInstance;
						}

						// MD 12/3/07 - NA 2008 Vol 1
						// Always decrement the index if the close button is showing but nothing was returned.
						index--;
					}
				}				
				
				return null;
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{	
				int count = this.MdiTabGroupControl.TabGroup.Tabs.Count					
					+ base.GetChildCount();
				
				ScrollButtonTypes scrollButtonTypes = this.mdiTabGroupControl.TabGroup.TabManager.ScrollButtonTypesResolved;
				if (scrollButtonTypes != ScrollButtonTypes.None)
					count += 1;

				// MD 12/3/07 - NA 2008 Vol 1
				// ShowCloseButton is now obsolete and has been repaced by the CloseButtonLocation property.
				//if (this.MdiTabGroupControl.TabGroup.SettingsResolved.ShowCloseButton)
				if ( this.MdiTabGroupControl.TabGroup.SettingsResolved.CloseButtonLocation == TabCloseButtonLocation.HeaderArea )
					count += 1;

				return count;				
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				if ( this.MdiTabGroupControl.Focused )
					return this.MdiTabGroupControl.TabGroup.Manager.ActiveTab.AccessibilityObject;

				return null;
			}

				#endregion GetFocused

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				if ( this.mdiTabGroupControl.TabGroup.TabManager.SelectedTabItem  != null )
					return this.mdiTabGroupControl.TabGroup.TabManager.SelectedTabItem.AccessibilityObject;
				else
					return null;				
			}

				#endregion GetSelected

				#region HitTest

			/// <summary>
			/// Retrieves the child object at the specified screen coordinates.
			/// </summary>
			/// <param name="x">The horizontal screen coordinate.</param>
			/// <param name="y">The vertical screen coordinate.</param>
			/// <returns>An AccessibleObject that represents the child object at the given screen coordinates. This method returns the calling object if the object itself is at the location specified. Returns a null reference (Nothing in Visual Basic) if no object is at the tested location.</returns>
			public override AccessibleObject HitTest(int x, int y)
			{				
				AccessibleObject accObject = base.HitTest(x,y);

				if (accObject != this)
					return accObject;

				UIElement mainElement = this.mdiTabGroupControl.ControlUIElement;

				if ( mainElement == null )
					return null;

				return mainElement.HitTest(x,y);
			}

				#endregion HitTest

				#region Navigate

			/// <summary>
			/// Navigates to another accessible object.
			/// </summary>
			/// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
			/// <returns></returns>
			public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{
				return base.Navigate( navdir );
			}

				#endregion Navigate

				#region Role

			/// <summary>
			/// Gets the role of this accessible object.
			/// </summary>
			public override AccessibleRole Role
			{
				get
				{
					if ( this.mdiTabGroupControl.AccessibleRole != AccessibleRole.Default )
						return base.Role;

					return AccessibleRole.PageTabList;
				}
			}

				#endregion Role

				#region State

			/// <summary>
			/// Gets the state of this accessible object.
			/// </summary>
			public override AccessibleStates State
			{
				get
				{
					AccessibleStates state = AccessibleStates.None;

					if ( this.mdiTabGroupControl.Enabled == false )
						state |= AccessibleStates.Unavailable;
					else
					{
						if ( this.mdiTabGroupControl.Visible == true )
						{
							state |= AccessibleStates.Focusable;

							if ( this.mdiTabGroupControl.Focused )
							{
								state |= AccessibleStates.Focused;
							}
						}
						else
							state |= AccessibleStates.Invisible;
					}

					if ( this.Bounds.IsEmpty )
						state |= AccessibleStates.Offscreen;

					return state;
				}
			}

				#endregion State

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region MdiTabGroupControl

			/// <summary>
			/// Returns the associated mdiTabGroupControl control.
			/// </summary>
			/// <seealso cref="MdiTabGroupControl"/>
			public MdiTabGroupControl MdiTabGroupControl 
			{ 
				get 
				{
					return this.mdiTabGroupControl; 
				} 
			}

					#endregion MdiTabGroupControl

				#endregion Public Properties

			#endregion Properties

		}

		#endregion Public Class MdiTabGroupControlAccessibleObject

		#region Interfaces

		// AS 6/14/04 DNF190
		#region ILayoutItem
		bool ILayoutItem.IsVisible
		{
			get { return false; }
		}

		System.Drawing.Size ILayoutItem.MinimumSize
		{
			get { return System.Drawing.Size.Empty; }
		}

		System.Drawing.Size ILayoutItem.PreferredSize
		{
			get { return System.Drawing.Size.Empty; }
		}
		#endregion //ILayoutItem

		#endregion //Interfaces
	}
}
