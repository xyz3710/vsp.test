#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using Infragistics.Win.Design;
using System.Windows.Forms;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi.Design
{
	/// <summary>
	/// UltraTabbedMdiManagerDesigner
	/// </summary>
    // JAS v5.3 SmartTags 7/13/05
	// MD 1/4/07 - FxCop
	[PermissionSet( SecurityAction.LinkDemand, Name = "FullTrust" )]
	[PermissionSet( SecurityAction.InheritanceDemand, Name = "FullTrust" )]
    public partial class UltraTabbedMdiManagerDesigner : UltraComponentManagerDesigner
	{
		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="UltraTabbedMdiManagerDesigner"/>
		/// </summary>
		public UltraTabbedMdiManagerDesigner()
		{
		}
		#endregion //Constructor

		#region General Properties

		#region Manager
		private UltraTabbedMdiManager Manager
		{
			get { return this.Component as UltraTabbedMdiManager; }
		}
		#endregion //Manager

		#region DesignerHost
		private IDesignerHost DesignerHost
		{
			get { return this.GetService( typeof(IDesignerHost) ) as IDesignerHost; }
		}
		#endregion //DesignerHost

		#endregion //General Properties

		#region Initialization/Dispose related

		#region Initialize
		/// <summary>
		/// Prepares the designer to view, edit, and design the specified component.
		/// </summary>
		/// <param name="component">Component to be managed by the designer</param>
		public override void Initialize(System.ComponentModel.IComponent component)
		{
			if ( !(component is UltraTabbedMdiManager) )
				throw new NotSupportedException(Shared.SR.GetString(null, "LE_V2_Exception_4"));

			base.Initialize(component);

			this.EnsureOnlyOneInstance();
		}
		#endregion Initialize

		#region EnsureOnlyOneInstance
		private void EnsureOnlyOneInstance()
		{
			IDesignerHost host = this.DesignerHost;

			if (host == null)
				return;

			Form rootContainer = host.RootComponent as Form;

			// AS 4/25/03 WTB866
			// Allow it adding it to a service, etc.
			//
			//if (rootContainer == null)
			//	throw new NotSupportedException(Shared.SR.GetString(null, "LE_V2_Exception_62"));

			if (host.Container == null)
				return;

			foreach (object component in host.Container.Components)
			{
				if (component is UltraTabbedMdiManager && component != this.Manager)
					throw new NotSupportedException(Shared.SR.GetString(null, "LE_V2_Exception_63"));
			}
		}
		#endregion //EnsureOnlyOneInstance

    #region [Moved Code]
        // JAS v5.3 SmartTags 7/13/05 - Moved this code to UltraTabbedMdiManagerDesigner_CLR2.cs

        //#region InitializeNewComponent
        ///// <summary>
        ///// Invoked when the designer is first initialized so it may set default values on the component its managing.
        ///// </summary>
        //public override void InitializeNewComponent(System.Collections.IDictionary defaultValues)
        //{
        //    base.InitializeNewComponent(defaultValues);

        //    this.SetHostControl();

           
        //    // MD 10/14/04 - Default Store Support
        //    ISite site = this.Component.Site;

        //    if (site != null)
        //    {
        //        IComponent host = ((IDesignerHost)this.GetService(typeof(IDesignerHost))).RootComponent;

        //        if (host != null && host.Site != null)
        //            TypeDescriptor.GetProperties(this.Component)["SettingsKey"].SetValue(this.Component, host.Site.Name + "." + site.Name);
        //        //base.ShadowProperties["SettingsKey"] = defaultValues["SettingsKey"] = host.Site.Name + "." + site.Name;
        //        else
        //            TypeDescriptor.GetProperties(this.Component)["SettingsKey"].SetValue(this.Component, site.Name);
        //    }
        //}
        //#endregion //InitializeNewComponent
    #endregion // [Moved Code]

        #region SetHostControl
        private void SetHostControl()
		{
			// set the HostControl of the manager to the root component
			IDesignerHost host = this.DesignerHost;

			if (host != null)
			{
				Form form = host.RootComponent as Form;

				if (form == null)
				{
					// AS 4/25/03 WTB866
					// Allow it adding it to a service, etc..
					//
					//throw new NotSupportedException(Shared.SR.GetString(null, "LE_V2_Exception_62"));
					return;
				}

				this.Manager.MdiParent = form;
			}
		}
		#endregion //SetHostControl

    #region [Moved Code]
    // JAS v5.3 SmartTags 7/13/05 - Moved this code to UltraTabbedMdiManagerDesigner_CLR2.cs

        //#region InitializeExistingComponent
        ///// <summary>
        ///// Invoked when the designer is first initialized so it may set default values on the component its managing.
        ///// </summary>
        //public override void InitializeExistingComponent(System.Collections.IDictionary defaultValues)
        //{
        //    base.InitializeExistingComponent(defaultValues);

        //    this.EnsureOnlyOneInstance();

        //    this.SetHostControl();
        //}
        //#endregion //InitializeExistingComponent

    #endregion // [Moved Code]

        #region PostFilterProperties
        /// <summary>
		/// Used to filter the properties listed for the component.
		/// </summary>
		/// <param name="properties">Dictionary of properties</param>
		protected override void PostFilterProperties(System.Collections.IDictionary properties)
		{
			base.PostFilterProperties(properties);

			properties.Remove("TabGroups");
			properties.Remove("HiddenTabs");
		}
		#endregion //PostFilterProperties

		#endregion //Initialization/Dispose related
	}
}
