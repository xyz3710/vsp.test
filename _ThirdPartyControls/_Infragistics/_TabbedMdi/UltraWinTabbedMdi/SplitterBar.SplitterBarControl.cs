#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using System.Drawing;
using Infragistics.Win.Layout;		// AS 6/14/04 DNF190
using System.Collections;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Control for handling the display of a splitter bar.
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>SplitterBarControl</b> is a simple non-selectable control for displaying a 
	/// splitter bar. The <b>SplitterBarControl</b> differs from the <see cref="System.Windows.Forms.Splitter"/> 
	/// in that it does not actually perform any resize operation. It only provides the ui for a draggable splitter 
	/// bar.</p>
	/// <p class="body">When the mouse is pressed down on the splitter bar, the <see cref="SplitterDragging"/> 
	/// event is invoked. If the event is cancelled, the drag operation does not begin. Otherwise, the drag operation 
	/// begins and the <see cref="SplitterMove"/> event is invoked as the mouse is 
	/// repositioned. The range of the drag may be controlled using the <see cref="SplitterMove"/> event. 
	/// When the mouse is released and the drag operation is completed, the <see cref="SplitterDragged"/> event is invoked.</p>
	/// </remarks>
	[ToolboxItem(false)]
	[DefaultEvent("SplitterMove")]
	[DefaultProperty("Orientation")]
	public class SplitterBarControl : UltraControlBase,
		ILayoutItem			// AS 6/14/04 DNF190
	{
		#region Member Variables

		private IUltraControl						ultraControl = null;
		private AppearanceHolder					appearanceHolder = null;
		private UIElementBorderStyle				borderStyle = UIElementBorderStyle.Default;

		private const Orientation					OrientationDefault = Orientation.Vertical;
		private Orientation							orientation = OrientationDefault;

		private SplitterControlUIElement			controlUIElement = null;

		private SubObjectPropChangeEventHandler		subObjectPropChangeHandler = null;

		private ResolveAppearanceCallback			resolveAppearanceCallback = null;

		// AS 4/11/03 WTB798
		private System.IO.Stream					cursorStream = null;
		private Cursor								currentCursor = null;

		// JDN 8/12/04 NAS2004 Vol3 - Support for VisualStudio2005 style
		private bool								suppressLeadingEdge = false;

		// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
		private ArrayList							intersectionList = null;

		#endregion //Member Variables

		#region Constructors
		/// <summary>
		/// Initializes a new <see cref="SplitterBarControl"/>
		/// </summary>
		public SplitterBarControl()
		{
			this.SetStyle(ControlStyles.StandardDoubleClick, false);
			this.SetStyle(ControlStyles.UserMouse, true);
			this.SetStyle(ControlStyles.ResizeRedraw, true);
			this.SetStyle(ControlStyles.Selectable, false);
		}

		/// <summary>
		/// Initializes a new <see cref="SplitterBarControl"/>
		/// </summary>
		/// <param name="ultraControl">The managing IUltraControl</param>
		public SplitterBarControl(IUltraControl ultraControl) : this()
		{
			this.ultraControl = ultraControl;
		}
		#endregion //Constructors

		#region Properties

		#region IsDragging
		/// <summary>
		/// Indicates if the splitter is currently being dragged.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool IsDragging
		{
			get 
			{ 
				if (this.controlUIElement == null)
					return false;

				return this.controlUIElement.IsDragging; 
			}
		}
		#endregion //IsDragging

		#region Orientation
		/// <summary>
		/// Returns or sets the orientation of the splitter bar.
		/// </summary>
		[LocalizedDescription("LD_SplitterBarControl_P_Orientation")]
		[LocalizedCategory("LC_Behavior")]
		[DefaultValue(SplitterBarControl.OrientationDefault)]
		public Orientation Orientation
		{
			get { return this.orientation; }
			set
			{
				if (this.orientation == value)
					return;

				if (!Enum.IsDefined( typeof(Orientation), value ))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(Orientation));

				this.orientation = value;

				this.DestroyCursor();

				this.NotifyPropChange( SplitterBarPropertyIds.Orientation );
			}
		}
		#endregion //Orientation

		#region BorderStyle
		/// <summary>
		/// Gets/sets the border style for the splitter bar.
		/// </summary>
		[LocalizedDescription("LD_SplitterBarControl_P_BorderStyle")]
		[LocalizedCategory("LC_Appearance")]
		[DefaultValue(UIElementBorderStyle.Default)]
		public UIElementBorderStyle BorderStyle
		{
			get { return this.borderStyle; }
			set
			{
				if (this.borderStyle == value)
					return;

				if (!Enum.IsDefined( typeof(UIElementBorderStyle), value ))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(UIElementBorderStyle));

				this.borderStyle = value;

				this.DirtyChildElements();
				
				this.NotifyPropChange( SplitterBarPropertyIds.BorderStyle);
			}
		}
		#endregion //BorderStyle

		#region ControlUIElement
		/// <summary>
		/// Returns the main uielement representing the control
		/// </summary>
		protected override Infragistics.Win.ControlUIElementBase ControlUIElement
		{
			get
			{
				if (this.controlUIElement == null)
				{
					if (this.ultraControl != null)
						this.controlUIElement = new SplitterControlUIElement(this,this.ultraControl);
					else
						this.controlUIElement = new SplitterControlUIElement(this);
				}

				return this.controlUIElement;
			}
		}
		#endregion ControlUIElement

		#region Appearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> object used to format the display the splitter.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_SplitterBarControl_P_Appearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase Appearance
		{
			get
			{
				if (this.appearanceHolder == null)
				{
					this.appearanceHolder = new AppearanceHolder();

					this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.appearanceHolder.Appearance;
			}

			set
			{
				if (this.appearanceHolder == null				|| 
					!this.appearanceHolder.HasAppearance		||
					value != this.appearanceHolder.Appearance)
				{
					if (null == this.appearanceHolder)
					{
						this.appearanceHolder = new Infragistics.Win.AppearanceHolder();

							this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					this.appearanceHolder.Appearance = value;

					this.NotifyPropChange( SplitterBarPropertyIds.Appearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="Appearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="Appearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasAppearance
		{
			get
			{
				return (null != this.appearanceHolder &&
					this.appearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="Appearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="Appearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="Appearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeAppearance()
		{
			// only serialize unowned appearances
			return this.HasAppearance && 
				this.appearanceHolder.ShouldSerialize() &&
				this.appearanceHolder.Collection == null;
		}

		/// <summary>
		/// Resets the <see cref="Appearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="Appearance"/> property. If the <see cref="Appearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="Appearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="Appearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetAppearance()
		{
			if ( this.HasAppearance )
			{
				this.appearanceHolder.Reset();
				this.NotifyPropChange( SplitterBarPropertyIds.Appearance );
			}
		}
		#endregion Appearance

		#region SubObjectPropChangeHandler
		/// <summary>
		/// Returns the event handler that notifies OnSubObjectPropChanged
		/// </summary>
		protected SubObjectPropChangeEventHandler SubObjectPropChangeHandler
		{
			get
			{
				if (this.subObjectPropChangeHandler == null)
					this.subObjectPropChangeHandler = new SubObjectPropChangeEventHandler(this.OnSubObjectPropChanged);

				return this.subObjectPropChangeHandler;
			}
		}
		#endregion //SubObjectPropChangeHandler

		#region ResolveAppearanceCallback
		/// <summary>
		/// Returns or sets a delegate used to resolve the appearance for the splitter bar.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public ResolveAppearanceCallback ResolveAppearanceCallback
		{
			get { return this.resolveAppearanceCallback; }
			set { this.resolveAppearanceCallback = value; }
		}
		#endregion //ResolveAppearanceCallback

		// AS 4/11/03 WTB798
		#region CurrentSplitterCursor (internal)
		internal Cursor CurrentSplitterCursor
		{
			get
			{
				if (this.currentCursor == null)
				{
					Type type = typeof(UltraTabbedMdiManager);
					string cursorName;

					if (this.Orientation == Orientation.Vertical)
						cursorName = "CursorSplitEW.cur";
					else
						cursorName = "CursorSplitNS.cur";

					this.cursorStream = type.Assembly.GetManifestResourceStream(type, cursorName);
					this.currentCursor = new Cursor(this.cursorStream);
				}

				return this.currentCursor;
			}
		}
		#endregion //CurrentSplitterCursor

		// JDN 8/12/04 NAS2004 Vol3 - Support for VisualStudio2005 style
		#region SuppressLeadingEdge (internal)
		internal bool SuppressLeadingEdge
		{
			get { return this.suppressLeadingEdge;  }
			set { this.suppressLeadingEdge = value; }
		}
		#endregion SuppressLeadingEdge

		// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
		#region IntersectionList
		internal ArrayList IntersectionList
		{
			get 
			{ 
				if (this.intersectionList == null)
					this.intersectionList = new ArrayList(3);

				return this.intersectionList; 
			}
		}
		#endregion //IntersectionList

		#region HasIntersections
		internal bool HasIntersections
		{
			get { return this.intersectionList != null && this.intersectionList.Count > 0; }
		}
		#endregion //HasIntersections

		#endregion //Properties

		#region Methods

		#region DirtyChildElements
		private void DirtyChildElements()
		{
			if (this.controlUIElement != null)
				this.controlUIElement.DirtyChildElements(this.IsHandleCreated);
		}
		#endregion //DirtyChildElements

		#region OnSubObjectPropChanged
		/// <summary>
		/// Called when a property on a sub object has changed.
		/// </summary>
        /// <param name="propChangeInfo">A structure containing the property change information.</param>
		protected virtual void OnSubObjectPropChanged(PropChangeInfo propChangeInfo) 
		{
			if (this.HasAppearance && propChangeInfo.Source == this.appearanceHolder.RootAppearance)
			{
				if (this.IsHandleCreated)
					this.Invalidate();

				this.NotifyPropChange(SplitterBarPropertyIds.Appearance, propChangeInfo);
			}
		}
		#endregion //OnSubObjectPropChanged

		#region StartDrag
		/// <summary>
		/// Initiates a drag of the splitter bar based on the current mouse location.
		/// </summary>
		/// <remarks>
		/// <p class="note">Note: Since the control relies upon mouse capture to receive mouse notifications, the 
		/// mouse must be down in order for the mouse messages to be routed to the control.</p>
		/// </remarks>
		public void StartDrag()
		{
			this.StartDrag(System.Windows.Forms.Control.MousePosition);
		}

		/// <summary>
		/// Initiates a drag of the splitter bar based on the specified screen coordinates
		/// </summary>
		/// <remarks>
		/// <p class="note">Note: Since the control relies upon mouse capture to receive mouse notifications, the 
		/// mouse must be down in order for the mouse messages to be routed to the control.</p>
		/// </remarks>
		/// <param name="point">The location to start the drag in screen coordinates</param>
		public void StartDrag(System.Drawing.Point point)
		{
			if (this.controlUIElement == null)
				return;
					
			this.controlUIElement.StartDrag(point);				  
		}
		#endregion //StartDrag

		#region EndDrag
		/// <summary>
		/// Ends a drag operation.
		/// </summary>
		/// <param name="cancel">True to cancel the drag, otherwise false.</param>
		public void EndDrag(bool cancel)
		{
			if (this.controlUIElement == null)
				return;

			this.controlUIElement.EndDrag(cancel);
		}
		#endregion //EndDrag

		#region ResolveAppearance
		internal void ResolveAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			if (this.ResolveAppearanceCallback != null)
				this.ResolveAppearanceCallback(ref appearance, ref requestedProps);

			if (requestedProps == 0)
				return;

			if (this.HasAppearance)
				this.Appearance.MergeData(ref appearance, ref requestedProps);

			if (requestedProps == 0)
				return;

			if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BackColor))
			{
				appearance.BackColor = SystemColors.Control;
				requestedProps &= ~AppearancePropFlags.BackColor;
			}

			if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BorderColor3DBase))
			{
				// base it on the backcolor
				appearance.BorderColor3DBase = appearance.BackColor;
				requestedProps &= ~AppearancePropFlags.BorderColor3DBase;
			}			

			if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BorderColor))
			{
				// just darken the backcolor
				appearance.BorderColor = Infragistics.Win.DrawUtility.Dark(appearance.BackColor);
				requestedProps &= ~AppearancePropFlags.BorderColor;
			}			
		}
		#endregion //ResolveAppearance

		#region OnPaint
		/// <summary>
		/// Calls the ControlUIElement's draw method
		/// </summary>
		/// <param name="pe"><see cref="PaintEventArgs"/></param>
		protected override void OnPaint(PaintEventArgs pe)
		{
			if (this.IsUpdating)
				return;

			if (this.ultraControl != null)
			{
				UltraComponentControlManagerBase ultraComponent = this.ultraControl as UltraComponentControlManagerBase;
				UltraControlBase ultraControlBase = this.ultraControl as UltraControlBase;

				if ( ultraComponent != null || ultraControlBase != null)
				{
					if ( (ultraComponent != null && ultraComponent.IsUpdating) ||
						(ultraControlBase != null && ultraControlBase.IsUpdating) )
						return;

					AlphaBlendMode alphaMode = ultraComponent != null ? ultraComponent.AlphaBlendMode : ultraControlBase.AlphaBlendMode;

					this.ControlUIElement.Draw( pe.Graphics, 
						pe.ClipRectangle,
                        !this.GetStyle(UltraTabbedMdiManager.DoubleBufferControlStyle),
                        alphaMode);

					return;
				}
			}

			base.OnPaint(pe);
		}
		#endregion //OnPaint

		#region Dispose
		/// <summary>
		/// Invoked when the component must release the resources it is using.
		/// </summary>
		/// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.IsDragging)
					this.EndDrag(true);

				this.DestroyCursor();
			}

			base.Dispose(disposing);
		}
		#endregion //Dispose

		// AS 4/11/03 WTB798
		#region DestroyCursor (private)
		private void DestroyCursor()
		{
			if (this.currentCursor != null)
			{
				this.currentCursor.Dispose();
				this.currentCursor = null;
			}

			if (this.cursorStream != null)
			{
				this.cursorStream.Close();
				this.cursorStream = null;
			}
		}
		#endregion //DestroyCursor

		// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
		#region RemoveIntersections
		internal void RemoveIntersections()
		{
			if (this.HasIntersections)
			{
				this.Invalidate();
				this.IntersectionList.Clear();
			}
		}
		#endregion //RemoveIntersections

		#region AddIntersection
		internal void AddIntersection(Rectangle siblingSplitterRect)
		{
			Rectangle intersection = Rectangle.Intersect(siblingSplitterRect, this.Bounds);
			intersection.Offset(-this.Left, -this.Top);
			this.IntersectionList.Add( intersection );

			this.Invalidate(intersection);
		}
		#endregion //AddIntersection

		#endregion //Methods

		#region Events

		#region Event objects

		private static readonly object			EventSplitterDragging = new object();
		private static readonly object			EventSplitterDragged = new object();
		private static readonly object			EventSplitterMove = new object();

		#endregion //Event objects

		#region Event declarations
		/// <summary>
		/// Cancelable event that occurs when the splitter bar is about to be repositioned.
		/// </summary>
		[LocalizedDescription("LD_SplitterBarControl_E_SplitterDragging")]
		[LocalizedCategory("LC_Action")]
		public event System.ComponentModel.CancelEventHandler SplitterDragging
		{
			add { this.Events.AddHandler( SplitterBarControl.EventSplitterDragging, value ); }
			remove { this.Events.RemoveHandler( SplitterBarControl.EventSplitterDragging, value ); }
		}

		/// <summary>
		/// Occurs when the drag of the splitter bar has ended.
		/// </summary>
		[LocalizedDescription("LD_SplitterBarControl_E_SplitterDragged")]
		[LocalizedCategory("LC_Action")]
		public event SplitterDraggedEventHandler SplitterDragged
		{
			add { this.Events.AddHandler( SplitterBarControl.EventSplitterDragged, value ); }
			remove { this.Events.RemoveHandler( SplitterBarControl.EventSplitterDragged, value ); }
		}

		/// <summary>
		/// Occurs during the drag of the splitter bar.
		/// </summary>
		/// <remarks>
		/// <p class="body">The event is invoked during a drag operation as the mouse changes position. 
		/// This event may be used to limit the range that the control will render the splitter bar during 
		/// the drag.</p>
		/// </remarks>
		[LocalizedDescription("LD_SplitterBarControl_E_SplitterMove")]
		[LocalizedCategory("LC_Action")]
		public event SplitterMoveEventHandler SplitterMove
		{
			add { this.Events.AddHandler( SplitterBarControl.EventSplitterMove, value ); }
			remove { this.Events.RemoveHandler( SplitterBarControl.EventSplitterMove, value ); }
		}
		#endregion //Event declarations

		#region Protected virtual OnXXX

		#region OnSplitterDragging
		/// <summary>
		/// Raises the <see cref="SplitterDragging"/> event when the splitter bar is about to be dragged.
		/// </summary>
		/// <param name="e">A <see cref="CancelEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnSplitterDragging</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnSplitterDragging</b> in a derived class, be sure to call the base class's <b>OnSplitterDragging</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="CancelEventArgs"/>
		/// <seealso cref="SplitterDragging"/>
		protected virtual void OnSplitterDragging( System.ComponentModel.CancelEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_31"));

			CancelEventHandler eDelegate = (CancelEventHandler)Events[SplitterBarControl.EventSplitterDragging];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnSplitterDragging

		#region OnSplitterDragged
		/// <summary>
		/// Raises the <see cref="SplitterDragged"/> event after the splitter bar has been repositioned.
		/// </summary>
		/// <param name="e">A <see cref="SplitterDraggedEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnSplitterDragged</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnSplitterDragged</b> in a derived class, be sure to call the base class's <b>OnSplitterDragged</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="SplitterDraggedEventArgs"/>
		/// <seealso cref="SplitterDragged"/>
		protected virtual void OnSplitterDragged( SplitterDraggedEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_32"));

			SplitterDraggedEventHandler eDelegate = (SplitterDraggedEventHandler)Events[SplitterBarControl.EventSplitterDragged];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnSplitterDragged

		#region OnSplitterMove
		/// <summary>
		/// Raises the <see cref="SplitterMove"/> event during the drag operation of a splitter bar.
		/// </summary>
		/// <param name="e">A <see cref="SplitterMoveEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnSplitterMove</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnSplitterMove</b> in a derived class, be sure to call the base class's <b>OnSplitterMove</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="SplitterMoveEventArgs"/>
		/// <seealso cref="SplitterMove"/>
		protected virtual void OnSplitterMove( SplitterMoveEventArgs e )
		{
			if ( e == null )
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_30"), Shared.SR.GetString(null, "LE_V2_Exception_32"));

			SplitterMoveEventHandler eDelegate = (SplitterMoveEventHandler)Events[SplitterBarControl.EventSplitterMove];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion //OnSplitterMove

		#endregion //Protected virtual OnXXX

		#region InvokeEvent
		internal void InvokeEvent( SplitterBarEventIds eventId, EventArgs e )
		{
			switch(eventId)
			{
				case SplitterBarEventIds.SplitterDragged:
					this.OnSplitterDragged(e as SplitterDraggedEventArgs);
					break;
				case SplitterBarEventIds.SplitterMove:
					this.OnSplitterMove(e as SplitterMoveEventArgs);
					break;
				case SplitterBarEventIds.SplitterDragging:
					this.OnSplitterDragging(e as CancelEventArgs);
					break;
			}
		}
		#endregion //InvokeEvent

		#endregion //Events

		#region Interfaces

		// AS 6/14/04 DNF190
		#region ILayoutItem
		bool ILayoutItem.IsVisible
		{
			get { return false; }
		}

		Size ILayoutItem.MinimumSize
		{
			get { return Size.Empty; }
		}

		Size ILayoutItem.PreferredSize
		{
			get { return Size.Empty; }
		}
		#endregion //ILayoutItem

		#endregion //Interfaces
	}
}
