#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.Drawing;
using Infragistics.Shared;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;
using Infragistics.Win.IGControls;
using Infragistics.Win.UltraWinTabs;
using System.Collections;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Windows.Forms;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Class representing a group of <see cref="MdiTab"/> objects in an <see cref="UltraTabbedMdiManager"/>
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>MdiTabGroup</b> is a container for a group of visible <see cref="MdiTab"/> instances.</p>
	/// <p class="body">The <see cref="MdiTabGroup.Settings"/> returns an instance of a <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabGroupSettings"/> object. 
	/// This object has properties that affect the appearance and behavior of the group. The <see cref="MdiTabGroup.TabSettings"/> returns a 
	/// <see cref="MdiTabSettings"/> instance that affects the default appearance and behavior of the <see cref="MdiTabGroup.Tabs"/>. Properties not explicitly set on the <see cref="MdiTab.Settings"/> 
	/// of the contained tabs are resolved using this object.</p>
	/// <p class="body">The <see cref="Extent"/> (and <see cref="ClientExtent"/> property) can be used to manage the size 
	/// of the MdiTabGroup. Depending upon the <see cref="UltraTabbedMdiManager.Orientation"/> of the owning <see cref="Manager"/>, 
	/// this value will affect the width (for vertically oriented tab groups) or height (for horizontally oriented tab groups).</p>
	/// </remarks>
	/// <seealso cref="Tabs"/>
	/// <seealso cref="MdiTab"/>
	/// <seealso cref="UltraTabbedMdiManager.TabGroups"/>
	[Serializable()]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MdiTabGroup : KeyedSubObjectBase, 
		ISerializable,
		IDeserializationCallback,
		ITabProvider
	{
		#region Member Variables

		private UltraTabbedMdiManager			manager = null;
		private MdiTabGroupSettings				settings = null;
		private MdiTabSettings					tabSettings = null;
		private MdiTabGroupSettingsResolved		settingsResolved = null;
		private MdiTabsCollection				tabs = null;
		private int								extent = 0;

		private TabManager						tabManager = null;

		private bool							isInitializingSelectedTab = false;

		// AS 4/14/03 WTB801
		private int								serializedFirstDisplayedTabIndex = 0;
		private int								serializedSelectedTabIndex = 0;

		// AS 6/3/03
		private int								cachedIndex = -1;

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		private MdiTabGroup						parentGroup;
		private MdiTabGroupsCollection			tabGroups;

		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups / WTB801
		private int								serializedActiveTabGroupIndex = UltraTabbedMdiManager.NoActiveTabGroupIndex;

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		private int								preMaximizeExtent = -1;

		// AS 4/28/06 AppStyling
		private AppStyling.StylePropertyCache	propertyCache = new AppStyling.StylePropertyCache( StyleUtils.TabGroupCachePropertyCount );

        // MBS 10/16/06 BR16790
        private bool isDefaultNonSelectedHotTrackedAppearance = false;

		#endregion //Member Variables

		#region Constructor
		// AS 4/22/03
		// The constructor does not need to be public and may
		// cause some confusion. I'll leave it protected so someone
		// can derive from it.

		/// <summary>
		/// Initializes a new <see cref="MdiTabGroup"/>
		/// </summary>
		/// <param name="manager">Owning <see cref="MdiTabGroup"/></param>
		internal protected MdiTabGroup(UltraTabbedMdiManager manager)
		{
			if (manager == null)
				throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_3"), Shared.SR.GetString(null, "LE_V2_Exception_7"));

			this.manager = manager;
		}

		/// <summary>
		/// Constructor used during deserialization to initialize a new <see cref="MdiTabGroup"/> object
		/// with the serialized property values.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected MdiTabGroup(SerializationInfo info, StreamingContext context)
		{
			foreach( SerializationEntry entry in info )
			{
				switch (entry.Name)
				{
					case "Key":
						this.Key = (string)Utils.DeserializeProperty(entry, typeof(string), this.Key);
						break;
					case "Settings":
						this.settings = (MdiTabGroupSettings)Utils.DeserializeProperty(entry, typeof(MdiTabGroupSettings), null);
						break;
					case "TabSettings":
						this.tabSettings = (MdiTabSettings)Utils.DeserializeProperty(entry, typeof(MdiTabSettings), null);
						break;
					case "Tabs":
						this.tabs = (MdiTabsCollection)Utils.DeserializeProperty(entry, typeof(MdiTabsCollection), null);
						break;
					case "Extent":
						this.extent = (int)Utils.DeserializeProperty(entry, typeof(int), this.extent);
						break;
					case MdiTab.TagSerializationName:
						this.DeserializeTag(entry);
						break;
					// AS 4/14/03 WTB801
					case "FirstDisplayTabIndex":
						this.serializedFirstDisplayedTabIndex = (int)Utils.DeserializeProperty(entry, typeof(int), this.serializedFirstDisplayedTabIndex);
						break;
					case "SelectedTabIndex":
						this.serializedSelectedTabIndex = (int)Utils.DeserializeProperty(entry, typeof(int), this.serializedSelectedTabIndex);
						break;

					// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
					case "TabGroups":
						this.tabGroups = (MdiTabGroupsCollection)Utils.DeserializeProperty(entry, typeof(MdiTabGroupsCollection), null);
						break;

					case "ActiveTabGroupIndex":
						// use -1 as the default since we could load a layout that did not serialize
						// the active tab, in which case we don't want to muck with the active tab
						//
						this.serializedActiveTabGroupIndex = (int)Utils.DeserializeProperty(entry, typeof(int), UltraTabbedMdiManager.NoActiveTabGroupIndex);
						break;

					// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
					case "PreMaximizedExtent":
						this.preMaximizeExtent = (int)Utils.DeserializeProperty(entry, typeof(int), this.preMaximizeExtent);
						break;

					default:
						// AS 4/30/03 FxCop Change
						// Explicitly call the overload that takes an IFormatProvider
						//
						//System.Diagnostics.Debug.Assert(false, string.Format("Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						System.Diagnostics.Debug.Assert(false, string.Format(null, "Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						break;
				}
			}
		}

		#endregion //Constructor

		#region Properties

		#region Manager
		/// <summary>
		/// Returns the owning <see cref="UltraTabbedMdiManager"/>
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraTabbedMdiManager Manager
		{
			get { return this.manager; }
		}
		#endregion //Manager

		#region Settings
		/// <summary>
		/// Returns an <see cref="MdiTabGroupSettings"/> instance used to control the appearance and behavior for this <b>MdiTabGroup</b>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Settings</b> property returns an instance of an <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabGroupSettings"/> object. 
		/// This object has properties that affect the appearance and behavior of the group including the <see cref="MdiTabGroupSettings.TabStyle"/>, 
		/// <see cref="MdiTabGroupSettings.TabSizing"/>, etc. The properties of this object are initialized with default values that need to be resolved to determine 
		/// the values that will actually be used. The resolution for the <b>Settings</b> begins with this property. Any settings not explicitly set on this object 
		/// are then checked on the <see cref="UltraTabbedMdiManager.TabGroupSettings"/> of the owning <see cref="Manager"/>.</p>
		/// <p class="note"><b>Note:</b> This property will only affect the <see cref="MdiTabGroup.Tabs"/> of this MdiTabGroup. If the MdiTabGroup does not contain any tabs, this property 
		/// will have no effect on any tabs within the <see cref="MdiTabGroup.TabGroups"/> of this MdiTabGroup.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroupSettings"/>
		/// <seealso cref="SettingsResolved"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public MdiTabGroupSettings Settings
		{
			get
			{
				if (this.settings == null)
				{
					this.settings = new MdiTabGroupSettings(this);
					this.settings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.settings;
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="Settings"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="Settings"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>MdiTabGroupSettings</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabGroupSettings"/>
		protected bool ShouldSerializeSettings()
		{
			return this.settings != null && this.settings.ShouldSerialize();
		}

		/// <summary>
		/// Indicates whether an <see cref="MdiTabGroupSettings"/> object has been created.
		/// </summary>
		/// <value>Returns True when the <b>MdiTabGroupSettings</b> object for the <see cref="Settings"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="MdiTabGroupSettings"/> object has been created.  <b>MdiTabGroupSettings</b> objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabGroupSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasSettings
		{
			get { return this.settings != null; }
		}

		/// <summary>
		/// Resets the <see cref="Settings"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the properties of the <see cref="MdiTabGroupSettings"/> maintained by the <see cref="Settings"/> property.</p>
		/// </remarks>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabGroupSettings"/>
		/// <seealso cref="MdiTabGroupSettings.Reset"/>
		public void ResetSettings()
		{
			if (this.HasSettings)
				this.Settings.Reset();
		}
		#endregion //Settings

		#region TabSettings
		/// <summary>
		/// Returns an <see cref="MdiTabSettings"/> instance used to control the tab appearance and behavior for the <b>MdiTab</b> objects contained within the <b>MdiTabGroup</b>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabSettings</b> property returns an instance of an <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabSettings"/> object. 
		/// This object has properties that affect the appearance and behavior of the <see cref="Tabs"/>. Properties not explicitly set on the <see cref="MdiTab.Settings"/> 
		/// of the contained tabs are resolved using this object.</p>
		/// <p class="note"><b>Note:</b> This property will only affect the <see cref="MdiTabGroup.Tabs"/> of this MdiTabGroup. If the MdiTabGroup does not contain any tabs, this property 
		/// will have no effect on any tabs within the <see cref="MdiTabGroup.TabGroups"/> of this MdiTabGroup.</p>
		/// </remarks>
		/// <seealso cref="MdiTab.Settings"/>
		/// <seealso cref="MdiTab.SettingsResolved"/>
		/// <seealso cref="UltraTabbedMdiManager.TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public MdiTabSettings TabSettings
		{
			get
			{
				if (this.tabSettings == null)
				{
					this.tabSettings = new MdiTabSettings(this);
					this.tabSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.tabSettings;
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="TabSettings"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabSettings"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>MdiTabSettings</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		protected bool ShouldSerializeTabSettings()
		{
			return this.tabSettings != null && this.tabSettings.ShouldSerialize();
		}

		/// <summary>
		/// Indicates whether an <see cref="MdiTabSettings"/> object has been created.
		/// </summary>
		/// <value>Returns True when the <b>MdiTabSettings</b> object for the <see cref="TabSettings"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="MdiTabSettings"/> object has been created.  <b>MdiTabSettings</b> objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabSettings
		{
			get { return this.tabSettings != null; }
		}

		/// <summary>
		/// Resets the <see cref="TabSettings"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the properties of the <see cref="MdiTabSettings"/> maintained by the <see cref="TabSettings"/> property.</p>
		/// </remarks>
		/// <seealso cref="TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		/// <seealso cref="MdiTabSettings.Reset"/>
		public void ResetTabSettings()
		{
			if (this.HasTabSettings)
				this.TabSettings.Reset();
		}
		#endregion //TabSettings

		#region SettingsResolved
		/// <summary>
		/// Returns an object providing the resolved values for the <see cref="Settings"/> property.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SettingsResolved</b> is used to determine the resolved values for the <see cref="Settings"/> that should be used by this <see cref="MdiTabGroup"/> instance. 
		/// The <b>Settings</b> property returns an instance of an <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabGroupSettings"/> object. 
		/// That object has properties that affect the appearance and behavior of the group including the <see cref="MdiTabGroupSettings.TabStyle"/>, 
		/// <see cref="MdiTabGroupSettings.TabSizing"/>, etc. The properties of this object are initialized with default values that need to be resolved to determine 
		/// the values that will actually be used. Any settings not explicitly set on the <b>Settings</b> property are then checked on the 
		/// <see cref="UltraTabbedMdiManager.TabGroupSettings"/> of the owning <see cref="Manager"/>.</p>
		/// </remarks>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabGroupSettingsResolved"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroupSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public MdiTabGroupSettingsResolved SettingsResolved
		{
			get
			{
				if (this.settingsResolved == null)
					this.settingsResolved = new MdiTabGroupSettingsResolved(this);

				return this.settingsResolved;
			}
		}
		#endregion //SettingsResolved

		#region Extent
		/// <summary>
		/// Returns or sets the extent of the tab group.
		/// </summary>
		/// <remarks>
		/// <p class="body"> When the <see cref="UltraTabbedMdiManager.Orientation"/> of the tab groups is 
		/// <b>Horizontal</b>, this value indicates the height of the group; otherwise the value indicates 
		/// the width of the group. The <b>Extent</b> includes the area occupied by the splitter, if there is 
		/// one for the <see cref="MdiTabGroup"/>. A splitter is allocated for all <see cref="MdiTabGroup"/> instances 
		/// in the <see cref="UltraTabbedMdiManager.TabGroups"/> except the last one. The <see cref="ClientExtent"/> 
		/// property can be used to initialize the <b>Extent</b> without determining whether the tab group displays a 
		/// splitter bar.</p>
		/// <p class="note">Note: Changing the extent of a tab group after a call BeginUpdate and before the call to <see cref="UltraTabbedMdiManager.EndUpdate"/>,
		/// will not affect the extent of the other MdiTabGroup objects in the <see cref="UltraTabbedMdiManager.TabGroups"/> until 
		/// the EndUpdate is invoked. At that point, the sum of the extents is verified against the extent of the mdi client area. If the sum 
		/// does not match the extent of the mdi client area, the extents are adjusted.</p>
		/// </remarks>
		/// <seealso cref="ClientExtent"/>
		/// <seealso cref="UltraTabbedMdiManager.Orientation"/>
		/// <seealso cref="UltraTabbedMdiManager.SplitterWidth"/>
		public int Extent
		{
			get
			{ 
				return this.extent;
			}
			set 
			{
				// AS 5/13/03
				// Impose a minimum extent.
				//
				if (value < 0)
					throw new ArgumentOutOfRangeException("value", value, SR.GetString("LE_V2_Exception_65")); 

				if (this.Manager == null)
					this.extent = value;
				else
					this.Manager.UpdateTabGroupExtent(this, value);

				this.NotifyPropChange(TabbedMdiPropertyIds.Extent);
			}
		}

		/// <summary>
		/// Indicates if the <see cref="Extent"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="Extent"/> differs from the default value.</returns>
		/// <seealso cref="Extent"/>
		protected bool ShouldSerializeExtent()
		{
			return this.extent > 0;
		}
		#endregion //Extent

		#region FirstDisplayedTab
		/// <summary>
		/// Returns or sets the first displayed <see cref="MdiTab"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>FirstDisplayedTab</b> is the <see cref="MdiTab"/> that is 
		/// first tab in the displayed <see cref="Tabs"/>. The tabs are laid out starting with the 
		/// <b>FirstDisplayedTab</b> and continuing until there are no more visible tabs or there isn't 
		/// enough room to display any more tab items. The direction in which the tabs are laid out will 
		/// depend upon the resolved <see cref="MdiTabGroupSettings.TabOrientation"/>. When the 
		/// <see cref="MdiTabGroupSettings.ScrollButtons"/> are displayed and the <b>FirstDisplayedTab</b> is 
		/// not the first visible tab, the scroll previous (first, previous, previous page) will be enabled.</p>
		/// <p class="note"><b>Note:</b> This property will return null if the <see cref="MdiTabGroup.Tabs"/> 
		/// is empty or for an MdiTabGroup that contains other MdiTabGroups instances in its <see cref="MdiTabGroup.TabGroups"/>.</p>
		/// <p class="note"><b>Note:</b> When this property is set, an exception is generated if the specified 
		/// <see cref="MdiTab"/> is not in the <see cref="MdiTabGroup.Tabs"/> collection.</p>
		/// </remarks>
		public MdiTab FirstDisplayedTab
		{
			get
			{
				// AS 4/8/05 BR03275
				if (!this.HasTabs)
					return null;

				return this.TabManager.FirstDisplayedTabItem as MdiTab;
			}
			set
			{
				if (!this.Tabs.Contains(value))
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_9"));

				this.TabManager.FirstDisplayedTabItem = value;
			}
		}
		#endregion //FirstDisplayedTab

		#region SelectedTab
		/// <summary>
		/// Returns or sets the currently selected <see cref="MdiTab"/> in the group
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>SelectedTab</b> represents the tab that currently 
		/// appears as selected in the tab group. The different in appearance will 
		/// depend upon the resolved <see cref="MdiTabGroupSettings.TabStyle"/> as well 
		/// as the appearance settings of the <see cref="MdiTabSettings"/>. The 
		/// <see cref="TabSettings"/> can be used to initialize the default appearances 
		/// for the <see cref="Tabs"/>. The appearance of the <b>SelectedTab</b> will be 
		/// resolved using the <see cref="MdiTabSettings.SelectedTabAppearance"/>.</p>
		/// <p class="body">The <b>SelectedTab</b> may also represent the <see cref="UltraTabbedMdiManager.ActiveTab"/> 
		/// since the <b>ActiveTab</b> must be the <b>SelectedTab</b> of the tab group that 
		/// contains it. Setting the <b>SelectedTab</b> will cause the <see cref="UltraTabbedMdiManager.TabSelecting"/>
		/// event to be invoked. If not cancelled, the tab will be selected and the <see cref="UltraTabbedMdiManager.TabSelected"/>
		/// event will be invoked.</p>
		/// <p class="note"><b>Note:</b> This property will return null if the <see cref="MdiTabGroup.Tabs"/> 
		/// is empty or for an MdiTabGroup that contains other MdiTabGroups instances in its <see cref="MdiTabGroup.TabGroups"/>.</p>
		/// <p class="note"><b>Note:</b> When this property is set, an exception is generated if the specified 
		/// <see cref="MdiTab"/> is not in the <see cref="MdiTabGroup.Tabs"/> collection.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.SelectedTabAppearance"/>
		/// <seealso cref="UltraTabbedMdiManager.TabSelecting"/>
		/// <seealso cref="UltraTabbedMdiManager.TabSelected"/>
		public MdiTab SelectedTab
		{
			get
			{
				// AS 4/8/05 BR03275
				if (!this.HasTabs)
					return null;

				MdiTab tab = this.TabManager.SelectedTabItem as MdiTab;

				//Debug.Assert( tab != null, "The 'MdiTabGroup' has a null SelectedTab" );

				return tab;
			}
			set
			{
				if (value == null)
					throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_8"), Shared.SR.GetString(null, "LE_V2_Exception_10"));

				if (!this.Tabs.Contains(value))
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_11"));

				this.TabManager.SelectedTabItem = value;
			}
		}
		#endregion //SelectedTab

		#region Tabs
		/// <summary>
		/// Returns a read only collection of <see cref="MdiTab"/> instances contained by the tabgroup.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Tabs</b> collection is a collection of the <see cref="MdiTab"/> objects 
		/// displayed by the MdiTabGroup. The order of the items in the collection determines the 
		/// order in which the tabs are displayed. The tab positions, sizes and display is managed by the 
		/// <see cref="MdiTabGroup.TabManager"/>.</p>
		/// <p class="body">The collection returned by the property is read only. There are several methods 
		/// on the MdiTabGroup, MdiTab and UltraTabbedMdiManager for rearranging and reposition MdiTab objects.
		/// The <see cref="MoveAllTabs"/> method may be used to move all the items in the <b>Tabs</b> collection 
		/// to the Tabs collection of a different MdiTabGroup. To move individual items, the <see cref="MdiTab.MoveToGroup(MdiTabGroup)"/> and 
		/// <see cref="MdiTab.MoveToNewGroup()"/> methods may be used. Alternatively, there are similar methods on the 
		/// <see cref="UltraTabbedMdiManager"/> itself that can change the position or tab group that contains particular 
		/// MdiTab objects.</p>
		/// </remarks>
		/// <seealso cref="MoveAllTabs"/>
		/// <seealso cref="MdiTab"/>
		/// <seealso cref="MdiTab.MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.MoveToGroup(MdiTab,MdiTabGroup)"/>
		public MdiTabsCollection Tabs
		{
			get
			{
				if (this.tabs == null)
				{
					this.tabs = new MdiTabsCollection();
					this.tabs.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.tabs;
			}
		}

		/// <summary>
		/// Indicates if the <see cref="Tabs"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="Tabs"/> differs from the default value.</returns>
		/// <seealso cref="Tabs"/>
		protected bool ShouldSerializeTabs()
		{
			return this.HasTabs;
		}

		/// <summary>
		/// Resets the properties of the contained 'MdiTab' objects to their default values.
		/// </summary>
		/// <seealso cref="Tabs"/>
		public void ResetTabs()
		{
			if (this.HasTabs)
			{
				foreach(MdiTab tab in this.Tabs)
				{
					tab.Reset();
				}
			}
		}

		/// <summary>
		/// Indicates if the <see cref="Tabs"/> collection has been created and contains at least one <see cref="MdiTab"/> instance
		/// </summary>
		/// <seealso cref="Tabs"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabs
		{
			get { return this.tabs != null && this.tabs.Count > 0; }
		}

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		internal bool HasTabsResolved
		{
			get 
			{ 
				if (this.HasTabs)
					return true;
				
				if (this.HasTabGroups)
				{
					foreach(MdiTabGroup tabGroup in this.TabGroups)
					{
						if (tabGroup.HasTabsResolved)
							return true;
					}
				}

				return false;
			}
		}
		#endregion //Tabs

		#region TabManager (internal protected)
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.UltraWinTabs.TabManager"/> class used 
		/// to manage the positioning and size of the <see cref="MdiTab"/> objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabManager</b> object is created using the 
		/// <see cref="MdiTabGroup.CreateTabManager"/> method and is used to 
		/// manage the positioning, size and display of the <see cref="MdiTab"/> objects 
		/// in the <see cref="Tabs"/> collection.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupManager"/>
		/// <seealso cref="CreateTabManager"/>
		internal protected Infragistics.Win.UltraWinTabs.TabManager TabManager
		{
			get
			{
				if (this.tabManager == null || this.tabManager.Disposed)
				{
					// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
					Debug.Assert(!this.HasTabGroups, "Attempting to create a tabmanager for an MdiTabGroup that contains nested tab groups.");

					this.tabManager = this.CreateTabManager();

					this.tabManager.SelectedTabItemChanging += new TabManager.SelectedTabItemChangingEventHandler( this.OnTabManagerSelectedTabItemChanging );
					this.tabManager.SelectedTabItemChanged += new TabManager.SelectedTabItemChangedEventHandler( this.OnTabManagerSelectedTabItemChanged );
					this.tabManager.TabScrolling += new TabScrollingEventHandler( this.OnTabManagerTabScrolling );
					this.tabManager.TabScrolled += new TabScrolledEventHandler( this.OnTabManagerTabScrolled );

					this.tabManager.TabItemMoving += new TabManager.TabItemMovingEventHandler(this.OnTabManagerTabMoving);
					this.tabManager.TabItemMoved += new TabManager.TabItemMovedEventHandler(this.OnTabManagerTabMoved);
				}

				return this.tabManager;
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabManager"/> object that manages the 
		/// <see cref="Tabs"/> collection has been created.
		/// </summary>
		internal protected bool HasTabManager
		{
			get { return this.tabManager != null; }
		}
		#endregion //TabManager (internal protected)

		#region HotTrackTab (internal)
		internal MdiTab HotTrackTab
		{
			get
			{
				if (this.HasTabManager)
					return this.TabManager.HotTrackTabItem as MdiTab;

				return null;
			}
		}
		#endregion //HotTrackTab (internal)

		#region ContainsActiveTab (private)
		private bool ContainsActiveTab
		{
			get
			{
				
				MdiTab activeTab = this.Manager == null ? null : this.Manager.ActiveTab;

				return activeTab == null ? false : activeTab.IsDescendantOf(this);
			}
		}
		#endregion //ContainsActiveTab

		#region ClientExtent
		/// <summary>
		/// Returns or sets the extent of the tabgroup not including the splitter width.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ClientExtent</b> is the <see cref="Extent"/> of the tab group excluding the 
		/// <see cref="UltraTabbedMdiManager.SplitterWidth"/>, if the tab group displays a splitter bar. When the 
		/// <see cref="UltraTabbedMdiManager.Orientation"/> of the tab groups is <b>Horizontal</b>, this value 
		/// indicates the height of the group excluding the area for the splitter bar; otherwise the value indicates 
		/// the width of the group excluding the splitter bar. A splitter is allocated 
		/// for all <see cref="MdiTabGroup"/> instances in the <see cref="UltraTabbedMdiManager.TabGroups"/> except 
		/// the last one.</p>
		/// <p class="note">Note: Changing the extent of a tab group after a call BeginUpdate and before the call to <see cref="UltraTabbedMdiManager.EndUpdate"/>,
		/// will not affect the extent of the other MdiTabGroup objects in the <see cref="UltraTabbedMdiManager.TabGroups"/> until 
		/// the EndUpdate is invoked. At that point, the sum of the extents is verified against the extent of the mdi client area. If the sum 
		/// does not match the extent of the mdi client area, the extents are adjusted.</p>
		/// </remarks>
		/// <seealso cref="Extent"/>
		/// <seealso cref="UltraTabbedMdiManager.Orientation"/>
		/// <seealso cref="UltraTabbedMdiManager.SplitterWidth"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.Repaint)]
		public int ClientExtent
		{
			get 
			{
				// get the current extent
				int extent = this.Extent;

				if (this.Manager != null)
				{
					// if we can get to the tab manager, see if we need a splitter
					bool needsSplitter = this.Manager.TabGroupManager.NeedsSplitter(this);

					// if we do, then remove the splitter width and return that value
					if (needsSplitter)
						extent -= this.Manager.SplitterWidth;
				}

				return extent;
			}
			set
			{
				// if we can get to the tab manager, add in the splitter width
				// if the tab group needs one
				if (this.Manager != null)
				{
					bool needsSplitter = this.Manager.TabGroupManager.NeedsSplitter(this);

					if (needsSplitter)
						value += this.Manager.SplitterWidth;
				}

				// then just set the extent
				this.Extent = value;
			}
		}
		#endregion //ClientExtent

		// AS 4/14/03 WTB801
		#region Serialized Tabs
		internal MdiTab SerializedFirstDisplayedTab
		{
			get
			{
				if (this.HasTabs)
				{
					int index = this.serializedFirstDisplayedTabIndex;

					if (index >= 0 && index < this.Tabs.Count)
						return this.Tabs[index];
				}

				return null;
				
			}
		}

		// AS 4/14/03 WTB801
		internal MdiTab SerializedSelectedTab
		{
			get
			{
				if (this.HasTabs)
				{
					int index = this.serializedSelectedTabIndex;

					if (index >= 0 && index < this.Tabs.Count)
						return this.Tabs[index];
				}

				return null;
			}
		}
		#endregion //Serialized Tabs

		// AS 6/3/03
		// There should be an easier way to get the index of a tab.
		//
		#region Index
		/// <summary>
		/// Returns the index of the <b>MdiTabGroup</b> in its parent collection.
		/// </summary>
		/// <seealso cref="KeyedSubObjectsCollectionBase.IndexOf(string)"/>
		/// <seealso cref="MdiTabGroupsCollection"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroups"/>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int Index
		{
			get 
			{
				
				MdiTabGroupsCollection collection = this.ParentCollection;

				if (collection == null)
					this.cachedIndex = -1;
				else if (this.cachedIndex < 0				||
					this.cachedIndex >= collection.Count	||
					collection[this.cachedIndex] != this)
				{
					this.cachedIndex = collection.IndexOf(this);
				}

				return this.cachedIndex;
			}
		}
		#endregion //Index

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		#region Parent
		/// <summary>
		/// Returns the containing <see cref="MdiTabGroup"/> or null if the group is owned by the <see cref="Manager"/>.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public MdiTabGroup Parent
		{
			get { return this.parentGroup; }
		}
		#endregion //Parent

		#region ParentCollection
		internal MdiTabGroupsCollection ParentCollection
		{
			get
			{
				if (this.Parent != null)
					return this.Parent.TabGroups;

				if (this.Manager != null)
					return this.Manager.TabGroups;

				return null;
			}
		}
		#endregion //ParentCollection

		#region ParentOrientation
		internal Orientation ParentOrientation
		{
			get
			{
				if (this.Parent != null)
					return this.Parent.TabGroupOrientation;

				if (this.Manager != null)
					return this.Manager.Orientation;

				Debug.Assert(false, "The Orientation is being requested for a tabgroup not parented to a group or tabbedmdimanager.");

				return UltraTabbedMdiManager.OrientationDefault;
			}
		}
		#endregion //ParentOrientation

		#region TabGroups
		/// <summary>
		/// Returns a collection of <see cref="MdiTabGroup"/> instances
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>MdiTabGroup</b> objects are created and destroyed as needed. When all of 
		/// the <see cref="MdiTab"/> objects in a tab group's <see cref="MdiTabGroup.Tabs"/> collection have 
		/// been closed, hidden or moved to other tab groups, the tab group is removed from the collection and 
		/// released. When there are no tab groups and a tab is being displayed or when a tab is explicitly moved 
		/// to a new tab group (e.g. using the <see cref="UltraTabbedMdiManager.MoveToNewGroup(MdiTab)"/> method), a new 
		/// <b>MdiTabGroup</b> is created and the <see cref="UltraTabbedMdiManager.InitializeTabGroup"/> event is invoked to 
		/// allow the tab group settings to be initialized.</p>
		/// <p class="note"><b>Note:</b> When an MdiTabGroup that contains child MdiTabGroups in its 
		/// <see cref="MdiTabGroup.TabGroups"/> only has 1 MdiTabGroup, the children of that tab group 
		/// are promoted and the tab group that contained only the single child tab group is removed.</p>
		/// </remarks>		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public MdiTabGroupsCollection TabGroups
		{
			get
			{
				if (this.tabGroups == null)
				{
					this.tabGroups = new MdiTabGroupsCollection();
					this.tabGroups.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.tabGroups;
			}
		}

		/// <summary>
		/// Indicates if any of the <see cref="MdiTabGroup"/> objects contains data to serialize.
		/// </summary>
		/// <returns>True if any of the properties of the <see cref="MdiTabGroup"/> objects in the collection differ from their default values.</returns>
		/// <seealso cref="TabGroups"/>
		protected bool ShouldSerializeTabGroups()
		{
			if (!this.HasTabGroups)
				return false;

			foreach(MdiTabGroup tabGroup in this.TabGroups)
			{
				if (tabGroup.ShouldSerialize())
					return true;
			}

			return false;
		}

		/// <summary>
		/// Resets the properties of all the <see cref="MdiTabGroup"/> objects in the <see cref="TabGroups"/> collection to their default values.
		/// </summary>
		/// <seealso cref="TabGroups"/>
		public void ResetTabGroups()
		{
			if (!this.HasTabGroups)
				return;

			foreach(MdiTabGroup tabGroup in this.TabGroups)
				tabGroup.Reset();

			
		}

		/// <summary>
		/// Indicates if the <see cref="TabGroups"/> collection has been created and contains at least one <see cref="MdiTabGroup"/> instance
		/// </summary>
		/// <seealso cref="TabGroups"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabGroups
		{
			get { return this.tabGroups != null && this.tabGroups.Count > 0; }
		}
		#endregion //TabGroups

		#region TabGroupOrientation
		/// <summary>
		/// Returns the <see cref="Orientation"/> indicating how the child <see cref="TabGroups"/> are arranged.
		/// </summary>
		/// <remarks>
		/// <p class="body">The orientation of the child tab group is based on the <see cref="MdiTabGroup.TabGroupOrientation"/> 
		/// of the <see cref="MdiTabGroup.Parent"/>. When the <see cref="Parent"/> is non-null, the orientation is 
		/// opposite that of the Parent's <b>TabGroupOrientation</b>. When the <b>Parent</b> is null, the orientation 
		/// is the value of the <see cref="UltraTabbedMdiManager.Orientation"/> of the associated <see cref="Manager"/>.</p>
		/// </remarks>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Orientation TabGroupOrientation
		{
			get
			{
				Orientation baseOrientation;

				// if the group is contained within another tab group,
				// its orientation is opposite that of the parent
				if (this.Parent != null)
					baseOrientation = this.Parent.TabGroupOrientation;
				else if (this.Manager != null)
					baseOrientation = this.Manager.Orientation;
				else
				{
					Debug.Assert(false, "Attempting to access the tab group's Orientation while not associated with a tab group or ultratabbedmdimanager!");

					baseOrientation = UltraTabbedMdiManager.OrientationDefault;
				}

				return baseOrientation == Orientation.Vertical ? Orientation.Horizontal : Orientation.Vertical;
			}
		}
		#endregion //TabGroupOrientation

		#region Next/Previous TabGroup
		internal MdiTabGroup NextLeafTabGroup
		{
			get 
			{
				return GetLeafTabGroup(this, false);
			}
		}

		internal MdiTabGroup PreviousLeafTabGroup
		{
			get
			{
				return GetLeafTabGroup(this, true);
			}
		}
		#endregion //Next/Previous TabGroup

		#region Level
		internal int Level
		{
			get
			{
				MdiTabGroup tabGroup = this.Parent;
				int level = UltraTabbedMdiManager.RootTabGroupLevel;

				while (tabGroup != null)
				{
					level++;
					tabGroup = tabGroup.Parent;
				}

				return level;
			}
		}
		#endregion //Level

		#region SerializedActiveTabGroupIndex
		internal int SerializedActiveTabGroupIndex
		{
			get { return this.serializedActiveTabGroupIndex; }
		}
		#endregion //SerializedActiveTabGroupIndex

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		#region IsMaximized
		/// <summary>
		/// Indicates if this group is currently maximized.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool IsMaximized
		{
			get
			{

				if (this.Manager == null)
					return false;

				return this.Manager.MaximizedTabGroup == this;
			}
		}
		#endregion //IsMaximized

		#region IsMinimized
		internal bool IsMinimized
		{
			get 
			{
				Debug.Assert(this.Manager != null, "Unable to determine minimized state while not associated with a tabbedmdimanager!");

				if (this.Manager == null)
					return false;

				if (this.IsMaximized || this.Manager.MaximizedTabGroup == null)
					return false;

				// it is not minimized if its anywhere in the chain
				// of the maximized group
				return !this.IsDescendantOf(this.Manager.MaximizedTabGroup) &&
					!this.Manager.MaximizedTabGroup.IsDescendantOf(this);
			}
		}
		#endregion //IsMinimized

		#region PreMaximizedExtent
		internal int PreMaximizedExtent
		{
			get { return this.preMaximizeExtent; }
		}
		#endregion //PreMaximizedExtent
		
		// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
		#region ExtentForAdjustments
		internal int ExtentForAdjustments
		{
			get
			{
				if (this.Manager != null && this.Manager.MaximizedTabGroup != null)
					return this.PreMaximizedExtent;

				return this.Extent;
			}
		}
		#endregion //ExtentForAdjustments

		// AS 4/27/06 AppStyling
		#region UIRoles
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> that represents the tab area.
		/// </summary>
		internal protected virtual Infragistics.Win.AppStyling.UIRole UIRoleTabArea
		{
			get 
			{
				StyleUtils.Role roleID;

				switch (this.SettingsResolved.TabOrientation)
				{
					default:
					case TabOrientation.TopLeft:
					case TabOrientation.TopRight:
						roleID = StyleUtils.Role.TabItemAreaHorzTop;
						break;
					case TabOrientation.BottomLeft:
					case TabOrientation.BottomRight:
						roleID = StyleUtils.Role.TabItemAreaHorzBottom;
						break;
					case TabOrientation.LeftTop:
					case TabOrientation.LeftBottom:
						roleID = StyleUtils.Role.TabItemAreaVertLeft;
						break;
					case TabOrientation.RightTop:
					case TabOrientation.RightBottom:
						roleID = StyleUtils.Role.TabItemAreaVertRight;
						break;
				}

				return StyleUtils.GetRole(this.Manager, roleID);
			}
		}

		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> that represents the close button element.
		/// </summary>
		internal protected virtual Infragistics.Win.AppStyling.UIRole UIRoleCloseButton
		{
			get 
			{ 
				// MD 12/18/07
				// Added new roles so appearance of header area and tab close buttons can be styled separately
				//return StyleUtils.GetRole(this.Manager, StyleUtils.Role.CloseButton); 
				StyleUtils.Role roleID;
				switch ( this.SettingsResolved.CloseButtonLocation )
				{
					case TabCloseButtonLocation.HeaderArea:
						roleID = StyleUtils.Role.HeaderAreaCloseButton;
						break;

					case TabCloseButtonLocation.Tab:
						roleID = StyleUtils.Role.TabItemCloseButton;
						break;

					default:
					case TabCloseButtonLocation.Default:
					case TabCloseButtonLocation.None:
						roleID = StyleUtils.Role.CloseButton;
						break;
				}

				return StyleUtils.GetRole( this.Manager, roleID );
			}
		}

		// MD 12/18/07
		// Added new virtual to get the ui role of the selected tab item close button
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> that represents the close button element with the selected tab.
		/// </summary>
		internal protected virtual Infragistics.Win.AppStyling.UIRole UIRoleSelectedTabItemCloseButton
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Manager, StyleUtils.Role.SelectedTabItemCloseButton ); 
			}
		}

		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> that represents the close tab list button element.
		/// </summary>
		internal protected virtual Infragistics.Win.AppStyling.UIRole UIRoleTabListButton
		{
			get 
			{ 
				return StyleUtils.GetRole(this.Manager, StyleUtils.Role.TabListButton); 
			}
		}

		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> that represents the tab client area.
		/// </summary>
		internal protected virtual Infragistics.Win.AppStyling.UIRole UIRoleTabClientArea
		{
			get 
			{ 
				return StyleUtils.GetRole(this.Manager, 
					this.IsVertical == false ? StyleUtils.Role.TabClientAreaHorz : StyleUtils.Role.TabClientAreaVert); 
			}
		}

		
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> that represents a tab item when resolving tab group appearances.
		/// </summary>
		internal protected virtual Infragistics.Win.AppStyling.UIRole UIRoleTabItemDefault
		{
			get 
			{
				StyleUtils.Role roleID;

				switch (this.SettingsResolved.TabOrientation)
				{
					default:
					case TabOrientation.TopLeft:
					case TabOrientation.TopRight:
						roleID = StyleUtils.Role.TabItemHorzTop;
						break;
					case TabOrientation.BottomLeft:
					case TabOrientation.BottomRight:
						roleID = StyleUtils.Role.TabItemHorzBottom;
						break;
					case TabOrientation.LeftTop:
					case TabOrientation.LeftBottom:
						roleID = StyleUtils.Role.TabItemVertLeft;
						break;
					case TabOrientation.RightTop:
					case TabOrientation.RightBottom:
						roleID = StyleUtils.Role.TabItemVertRight;
						break;
				}

				return StyleUtils.GetRole(this.Manager, roleID);
			}
		}
		#endregion //UIRoles

		#region IsVertical
		internal bool IsVertical
		{
			get { return Utilities.IsVerticalTabOrientation(this.SettingsResolved.TabOrientation); }
		}
		#endregion //IsVertical

		#region PropertyCache
		internal AppStyling.StylePropertyCache PropertyCache
		{
			get { return this.propertyCache; }
		}
		#endregion //PropertyCache

        // MBS 10/16/06 BR16790
        #region IsDefaultNonSelectedHotTrackedAppearance

        internal bool IsDefaultNonSelectedHotTrackedAppearance
        {
            get { return this.isDefaultNonSelectedHotTrackedAppearance; }
        }

        #endregion //IsDefaultNonSelectedHotTrackedAppearance

		#endregion //Properties

		#region Methods

		#region Public

		#region MoveAllTabs
		/// <summary>
		/// Moves all of the tabs in the group to the relative TabGroup.
		/// </summary>
		/// <param name="relation">Relative <see cref="MdiTabGroup"/> that should contain the tabs contained by the group</param>
		/// <remarks>
		/// <p class="body">The <b>MoveAllTabs</b> method may be used to move all the <see cref="Tabs"/> to another <see cref="MdiTabGroup"/>. The 
		/// <i>relation</i> is used to indicate which MdiTabGroup should receive the new <see cref="MdiTab"/> objects and is based on the order of 
		/// the <see cref="MdiTabGroup"/> objects in the <see cref="UltraTabbedMdiManager.TabGroups"/> of the owning <see cref="Manager"/>.</p>
		/// <p class="note">Note: Depending upon the <b>relation</b> specified, the tabs may not be moved. For example, if the tab group is the first 
		/// item in the <see cref="UltraTabbedMdiManager.TabGroups"/> collection and a relation of <b>First</b> is specified, then nothing will change 
		/// because the relation points to the same <see cref="MdiTabGroup"/>.</p>
		/// </remarks>
		public void MoveAllTabs( MdiTabGroupPosition relation )
		{
			

			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_12"));

			manager.MoveAllTabs(this, relation);
		}
		#endregion //MoveAllTabs

		#region Reset
		/// <summary>
		/// Resets the properties of the <see cref="MdiTabGroup"/> to their default values.
		/// </summary>
		/// <remarks>
		/// <p class="note">Since the <see cref="Extent"/> affects other <see cref="MdiTabGroup"/> instances, this property is not resettable.</p>
		/// </remarks>
		public void Reset()
		{
			this.ResetKey();
			this.ResetSettings();
			this.ResetTabs();
			this.ResetTabSettings();
			this.ResetTag();

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			this.ResetTabGroups();
		}
		#endregion //Reset

		#region ShouldSerialize
		/// <summary>
		/// Indicates if the <see cref="MdiTabGroup"/> should be serialized.
		/// </summary>
		/// <returns>Returns <b>true</b> if any of the properties of the object differ from their default values.</returns>
		public bool ShouldSerialize()
		{
			return this.ShouldSerializeExtent()		||
				this.ShouldSerializeKey()			||
				this.ShouldSerializeSettings()		||
				this.ShouldSerializeTabSettings()	||
				this.ShouldSerializeTabs()			||
				// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
				this.ShouldSerializeTabGroups()		||
				this.ShouldSerializeTag();
		}
		#endregion //ShouldSerialize

		#region ResolveTabAreaAppearance
		/// <summary>
		/// Resolves the appearance for the area behind the tabs
		/// </summary>
		/// <param name="appearance">Appearance structure to update</param>
		/// <param name="requestedProps">Appearance properties to resolve</param>
		public void ResolveTabAreaAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			// AS 4/27/06 AppStyling
			UltraTabbedMdiManager manager = this.Manager;
			AppStyling.ResolutionOrderInfo order = StyleUtils.GetResolutionOrderInfo(manager);
			AppStyling.UIRole role = this.UIRoleTabArea;

			if (null != role && order.UseStyleBefore)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);

			if (order.UseControlInfo)
			{
				if (this.HasSettings && this.Settings.HasTabAreaAppearance)
					this.Settings.TabAreaAppearance.MergeData(ref appearance, ref requestedProps);

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.HasTabAreaAppearance)
					manager.TabGroupSettings.TabAreaAppearance.MergeData(ref appearance, ref requestedProps);
			}

			if (null != role && order.UseStyleAfter)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);

			if (order.UseControlInfo)
			{
				if (manager != null && manager.HasAppearance)
					manager.Appearance.MergeData(ref appearance, ref requestedProps);
			}

			if (requestedProps == 0)
				return;

			#region TabStyle Defaults
			switch(this.SettingsResolved.TabStyle)
			{
				case TabStyle.Wizard:
					break;

				default:
				case TabStyle.Excel:
				case TabStyle.PropertyPage:
				case TabStyle.PropertyPageSelected:
				case TabStyle.StateButtons:
					
					break;

				case TabStyle.Flat:
				case TabStyle.VisualStudio:
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BackColor))
					{
						Color color = SystemColors.Control;

						// increase the luminosity of the control color by 13 percent
						float luminance = (float)Math.Min(color.GetBrightness() * ( 1f + 16f/255 ), 1f);
						color = XPItemColor.ColorFromHLS( color.GetHue(), luminance, color.GetSaturation() );

						appearance.BackColor = color;
						requestedProps &= ~AppearancePropFlags.BackColor;
					}
					break;

                // MBS 8/4/06 NAS2006 Vol3 Office2007 look & feel
                case TabStyle.Office2007Ribbon:
                    if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BackColor))
                    {
                        appearance.BackColor = Office2007ColorTable.Colors.TabHeaderAreaBackColor;
                        requestedProps &= ~AppearancePropFlags.BackColor;
                    }
                    break;
			}
			#endregion //TabStyle Defaults

			// AS 2/6/04 WTB1314
			// This isn't related to the change but I noticed it
			// while testing the fix.
			requestedProps &= ~AppearancePropFlags.FontData;
		}
		#endregion //ResolveTabAreaAppearance

		#region ToString
		/// <summary>
		/// Returns the <see cref="KeyedSubObjectBase.Key"/> and number of <see cref="MdiTab"/> objects in the <see cref="Tabs"/> collection.
		/// </summary>
        /// <returns>The <see cref="KeyedSubObjectBase.Key"/> and number of <see cref="MdiTab"/> objects in the <see cref="Tabs"/> collection</returns>
		public override string ToString()
		{
			bool hasKey = this.Key != null && this.Key.Length > 0;
			int tabCount = this.Tabs.Count;

			// AS 4/30/03 FxCop Change
			// Explicitly call the overload that takes an IFormatProvider
			//
			if (hasKey)
				return string.Format(null, "MdiTabGroup Key='{0}', Tab Count={1}", this.Key, tabCount);
			else
				return string.Format(null, "MdiTabGroup Tab Count={0}", tabCount);
		}
		#endregion //ToString

		#region Scroll
		/// <summary>
		/// Scrolls the tabs using the specified ScrollType and increment.
		/// </summary>
		/// <param name="scrollType">Type of scroll operation to perform</param>
		/// <seealso cref="FirstDisplayedTab"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroupScrolling"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroupScrolled"/>
        public void Scroll(Infragistics.Win.UltraWinTabs.ScrollType scrollType)
        {
			this.Scroll(scrollType, 1);
		}

		/// <summary>
		/// Scrolls the tabs using the specified ScrollType and increment.
		/// </summary>
		/// <param name="scrollType">Type of scroll operation to perform</param>
		/// <param name="increment">Number of units to scroll</param>
		/// <seealso cref="FirstDisplayedTab"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroupScrolling"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroupScrolled"/>
        public void Scroll(Infragistics.Win.UltraWinTabs.ScrollType scrollType, int increment)
        {
			this.TabManager.ScrollTabs(scrollType, increment);
		}
		#endregion //Scroll

		#region ResolveScrollTrackAppearance
		/// <summary>
		/// Resolves the appearance for the scroll track
		/// </summary>
		/// <param name="appearance">Appearance structure to update</param>
		/// <param name="requestedProps">Appearance properties to resolve</param>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtonTypes"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollButtons"/>
		/// <seealso cref="MdiTabGroupSettings.ScrollTrackExtent"/>
		public void ResolveScrollTrackAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			if (this.HasSettings && this.Settings.HasScrollTrackAppearance)
				this.Settings.ScrollTrackAppearance.MergeData(ref appearance, ref requestedProps);
		}
		#endregion //ResolveScrollTrackAppearance

		#endregion //Public

		#region Protected Methods

		#region OnSubObjectPropChanged
		/// <summary>
		/// Called when a property on a sub object has changed.
		/// </summary>
        /// <param name="propChangeInfo">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged(PropChangeInfo propChangeInfo) 
		{
			if (this.HasSettings && propChangeInfo.Source == this.settings)
			{
				// AS 4/28/06 AppStyling
				this.propertyCache.ClearCachedPropertyValues();

				this.NotifyTabManager(TabbedMdiPropertyIds.Settings, propChangeInfo);
				this.NotifyPropChange(TabbedMdiPropertyIds.Settings, propChangeInfo);
			}
			else if (this.HasTabSettings && propChangeInfo.Source == this.tabSettings)
			{
				this.NotifyTabManager(TabbedMdiPropertyIds.TabSettings, propChangeInfo);
				this.NotifyPropChange(TabbedMdiPropertyIds.TabSettings, propChangeInfo);
			}
			//else if (this.Tabs == propChangeInfo.Source)
			else if (this.tabs != null && this.Tabs == propChangeInfo.Source)
			{
				this.NotifyTabManager(TabbedMdiPropertyIds.Tabs, propChangeInfo);
				this.NotifyPropChange(TabbedMdiPropertyIds.Tabs, propChangeInfo);
			}
			// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
			else if (this.tabGroups != null && this.tabGroups == propChangeInfo.Source)
				this.NotifyPropChange(TabbedMdiPropertyIds.TabGroups, propChangeInfo);
			else
				System.Diagnostics.Debug.Fail("An unhandled 'PropChangeInfo' notification was received", propChangeInfo.ToString());

			base.OnSubObjectPropChanged(propChangeInfo);
		}
		#endregion //OnSubObjectPropChanged

		#region CreateTabManager
		/// <summary>
		/// Creates the <see cref="Infragistics.Win.UltraWinTabs.TabManager"/> used to manage the display of the <see cref="Tabs"/>
		/// </summary>
		/// <returns>A new <see cref="Infragistics.Win.UltraWinTabs.TabManager"/></returns>
		protected virtual TabManager CreateTabManager()
		{
			return new MdiTabGroupManager(this);
		}
		#endregion //CreateTabManager

		// AS 10/15/03 DNF116
		#region GetObjectData
		/// <summary>
		/// Invoked during the serialization of the object.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		// MD 1/4/07 - FxCop
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
		}
		#endregion //GetObjectData

		// AS 10/15/03 DNF116
		#region OnDeserialization
		/// <summary>
		/// Invoked after the entire object graph has been deserialized.
		/// </summary>
		/// <param name="sender">The object that initiated the callback. The functionality for the this parameter is not currently implemented.</param>
		protected virtual void OnDeserialization(object sender)
		{
		}
		#endregion //OnDeserialization

		#region OnDispose (protected override)
		/// <summary>
		/// Invoked when the element is being disposed to allow the element to free
		/// any resources being used.
		/// </summary>
		protected override void OnDispose()
		{
			// AS 1/5/04 WTB1271
			this.DisposeTabManager();

			// AS 1/5/04 WTB1271
			if (this.settings != null)
			{
				this.settings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.settings.Dispose();
				this.settings = null;
			}

			base.OnDispose();
		}
		#endregion //OnDispose (protected override)

		#endregion //Protected Methods

		#region Internal

		#region DirtyTabManager
		/// <summary>
		/// Notifies the tab group that some of the tab settings have changed.
		/// </summary>
		/// <param name="changeType">Flagged enumeration indicating the types of changes.</param>
		internal protected void DirtyTabManager(TabManagerChangeType changeType)
		{
			
			if (changeType == TabManagerChangeType.None)
				return;

			bool invalidate = (changeType & TabManagerChangeType.Invalidate) != 0;
			bool collection = (changeType & TabManagerChangeType.Collection) != 0;
			bool allMetrics = (changeType & TabManagerChangeType.AllMetrics) != 0;

			if (this.HasTabManager)
			{
				if ((changeType & TabManagerChangeType.Font) != 0)
					this.TabManager.DirtyFont();

				if (invalidate || collection || allMetrics)
					this.TabManager.Dirty(invalidate, collection, allMetrics);
			}

			if ( (collection || allMetrics) && this.Manager != null )
				this.Manager.DirtyGroupPosition(this);
		}
		#endregion //DirtyTabManager

		#region DirtyTabItem
		internal void DirtyTabItem( MdiTab tab, bool invalidate, bool textChanged, bool imageChanged )
		{
			if (!this.HasTabManager)
				return;

			this.TabManager.DirtyTabItem(tab, invalidate, textChanged, imageChanged);

			// AS 4/24/03 WTB860
			// Changing the text while the text orientation is perpendicular to the
			// the tab orientation could cause the group to resize so dirty the 
			// tab collection metrics.
			//
			if (textChanged)
			{
				TextOrientation orientation = this.TabManager.TextOrientationResolved;

				switch(orientation)
				{
					case TextOrientation.HorizontalPlus270:
					case TextOrientation.HorizontalPlus90:
						this.DirtyTabManager(TabManagerChangeType.Invalidate | TabManagerChangeType.Collection);
						break;
				}
			}
		}
		#endregion //DirtyTabItem

		#region ResolveDefaultTabPhaseAppearance
		internal void ResolveDefaultTabPhaseAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, MdiTab.AppearanceResolutionPhase phase, TabState tabState )
		{
			

			// resolve the defaults for the phase depending upon the tab style
			//

            // MBS 10/16/06 BR16790
            // Reset flag
            this.isDefaultNonSelectedHotTrackedAppearance = false;

			switch(phase)
			{
				case MdiTab.AppearanceResolutionPhase.Active:
					break;
				case MdiTab.AppearanceResolutionPhase.Final:
					bool isSelected = (tabState & TabState.Selected) != 0;
					bool isHotTrack = (tabState & TabState.Hot) != 0;
					bool isActive = (tabState & TabState.Active) != 0;

					TabStyle style = this.SettingsResolved.TabStyle;

					// AS 5/2/06 AppStyling
					ViewStyle viewStyle = this.manager.ViewStyleResolved;

					//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
					bool isViewStyleOffice2003 = viewStyle == ViewStyle.Office2003;
					// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
					bool isViewStyleVS2005	   = viewStyle == ViewStyle.VisualStudio2005;
					bool isOffice2003orVS2005  = viewStyle == ViewStyle.Office2003     ||
												 viewStyle == ViewStyle.VisualStudio2005;
                    // MBS 8/2/06 NAS2006 Vol3 - Office2007 look & feel
                    bool isViewStyleOffice2007Ribbon = viewStyle == ViewStyle.Office2007;

					#region Office2003 / VisualStudio2005 ViewStyle
					//
					//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
					//
					//	This block handles BackColor, BackColor2, and BackGradientStyle,
					//	since the ultimate defaults we provide are dependent on the
					//	tab orientation.
					//
					// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
					//if ( isViewStyleOffice2003 )
					if ( isOffice2003orVS2005 )
					{
						//Color backColor = Office2003Colors.ToolbarGradientLight;
						//Color backColor2 = Office2003Colors.ToolbarGradientDark;
						Color backColor  = Color.Empty;
						Color backColor2 = Color.Empty;

						// AS 10/18/05 BR06737
						bool skipBackColorCode = false;

						if ( isViewStyleOffice2003 )
						{
							backColor  = this.Manager.MSColors.ToolbarGradientLight;
							backColor2 = this.Manager.MSColors.ToolbarGradientDark;
						}
						else if ( isViewStyleVS2005 )
						{
							if ( !this.Manager.ThemesActive && isSelected )
							{
								backColor = SystemColors.Window;

								if ( (requestedProps & AppearancePropFlags.BackColor) == AppearancePropFlags.BackColor &&
									! appearance.HasPropertyBeenSet(AppearancePropFlags.BackColor) )
								{
									appearance.BackColor = backColor;
									requestedProps		&= ~AppearancePropFlags.BackColor;
								}
								// AS 10/18/05 BR06737
								// We don't want to return. We just want to skip the
								// code below.
								//
								//return;
								skipBackColorCode = true;
							}
                            // MBS 4/3/06
                            // Set background of non-selected tabs to be the same color as the control background
                            // so that the selected tab will be more visually obvious
                            //
                            // This change is relevant to how VS2005 displayed its tabs
                            // i.e. 'Error List', 'Find Results', and 'Watch', while pinned 
                            if (style == TabStyle.VisualStudio2005 && !isSelected)
                                backColor = SystemColors.Control;
                            else if (isSelected)
                                backColor = SystemColors.Window;
                            else
                            {
                                backColor = VisualStudio2005ColorTable.Colors.TabGradientLight;
                                backColor2 = VisualStudio2005ColorTable.Colors.TabGradientDark;
                            }
						}
						
						// AS 10/18/05 BR06737
						// We were incorrectly returning above but we really wanted 
						// to let the code below continue - only skipping the setting of
						// the backcolor/backcolor2/backgradient.
						//
						if (!skipBackColorCode)
						{
							if ( isHotTrack )
							{
								//backColor =	isSelected ?
										//	Office2003Colors.HighlightMouseDownGradientDark :
										//	Office2003Colors.HighlightNotPressedGradientLight;
								backColor =	isSelected ?
									this.Manager.MSColors.HighlightMouseDownGradientDark :
									this.Manager.MSColors.HighlightNotPressedGradientLight;


								//backColor2 = isSelected ?
										//	Office2003Colors.HighlightMouseDownGradientLight :
										//	Office2003Colors.HighlightNotPressedGradientDark;
								backColor2 = isSelected ?
									this.Manager.MSColors.HighlightMouseDownGradientLight :
									this.Manager.MSColors.HighlightNotPressedGradientDark;
							}

							this.AdjustAppearancePropertiesForTabOrientation( backColor,
																			backColor2,
																			ref appearance,
																			ref requestedProps );
						}
					}
					#endregion Office2003 / VisualStudio2005 ViewStyle

                    #region Office2007Ribbon

                    if (isViewStyleOffice2007Ribbon)
                    {
                        if (isSelected)
                        {
                            this.AdjustAppearancePropertiesForTabOrientation(
                                Office2007ColorTable.Colors.TabSelectedGradientLight,
                                Office2007ColorTable.Colors.TabSelectedGradientDark,
                                ref appearance,
                                ref requestedProps);
                        }
                        else if (isHotTrack)
                        {
                            Color backColor = Office2007ColorTable.Colors.TabNonSelectedHotTrackedBackColor;
                            Color backColor2 = Office2007ColorTable.Colors.TabNonSelectedHotTrackedBackColor2;

                            // MBS 10/16/06 BR16790
                            // If the user has set BackColor, BackColor2, or the BackGradientStyle, we're not going to draw the glow
                            // since it's possible, even likely, that the glow color wouldn't match up with the user settings.
                            this.isDefaultNonSelectedHotTrackedAppearance =
                                !appearance.HasPropertyBeenSet(AppearancePropFlags.BackColor) && (requestedProps & AppearancePropFlags.BackColor) != 0 &&
                                !appearance.HasPropertyBeenSet(AppearancePropFlags.BackColor2) && (requestedProps & AppearancePropFlags.BackColor2) != 0 &&
                                !appearance.HasPropertyBeenSet(AppearancePropFlags.BackGradientStyle) && (requestedProps & AppearancePropFlags.BackGradientStyle) != 0;

                            this.AdjustAppearancePropertiesForTabOrientation(backColor,
                                                                             backColor2,
                                                                             ref appearance,
                                                                             ref requestedProps);

                            #region BackGradientStyle

                            if ((requestedProps & AppearancePropFlags.BackGradientStyle) == AppearancePropFlags.BackGradientStyle &&
                                !appearance.HasPropertyBeenSet(AppearancePropFlags.BackGradientStyle))
                            {
                                switch (this.SettingsResolved.TabOrientation)
                                {
                                    case TabOrientation.LeftBottom:
                                    case TabOrientation.LeftTop:
                                        if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Black)
                                            appearance.BackGradientStyle = GradientStyle.GlassLeft20;
                                        else
                                            appearance.BackGradientStyle = GradientStyle.Horizontal;
                                        break;

                                    case TabOrientation.RightBottom:
                                    case TabOrientation.RightTop:
										if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Black)
                                            appearance.BackGradientStyle = GradientStyle.GlassRight20;
                                        else
                                            appearance.BackGradientStyle = GradientStyle.Horizontal;
                                        break;

                                    case TabOrientation.BottomLeft:
                                    case TabOrientation.BottomRight:
										if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Black)
                                            appearance.BackGradientStyle = GradientStyle.GlassBottom20;
                                        else
                                            appearance.BackGradientStyle = GradientStyle.Vertical;
                                        break;

                                    case TabOrientation.TopLeft:
                                    case TabOrientation.TopRight:
										if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Black)
                                            appearance.BackGradientStyle = GradientStyle.GlassTop20;
                                        else
                                            appearance.BackGradientStyle = GradientStyle.Vertical;
                                        break;
                                }
                                requestedProps &= ~AppearancePropFlags.BackGradientStyle;
                            }

                            #endregion //BackGradientStyle
                        }
                        else
                        {
                            if ((requestedProps & AppearancePropFlags.BackColor) != 0 &&
                                !appearance.HasPropertyBeenSet(AppearancePropFlags.BackColor))
                            {
                                appearance.BackColor = Color.Transparent;
                                requestedProps &= ~AppearancePropFlags.BackColor;
                            }
                        }

                        // Used for the 'glow' effect
                        #region BorderColor2
                        if ((requestedProps & AppearancePropFlags.BorderColor2) == AppearancePropFlags.BorderColor2 &&
                         !appearance.HasPropertyBeenSet(AppearancePropFlags.BorderColor2))
                        {
                            // MBS 9/19/06
                            if (isHotTrack)
                                appearance.BorderColor2 = Office2007ColorTable.Colors.TabGlowBorderColor;
                            else
                                appearance.BorderColor2 = Office2007ColorTable.Colors.TabSelectedInnerBorderColor;

                            requestedProps &= ~AppearancePropFlags.BorderColor2;
                        }
                        #endregion //BorderColor2
                    }

                    #endregion //Office2007Ribbon

                    #region BackColor
                    // BackColor
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BackColor))
					{
						if (isSelected)
						{
							if (style == TabStyle.Excel)
								appearance.BackColor = SystemColors.Window;
							else
								appearance.BackColor = SystemColors.Control;
						}
						else
						{
							switch(style)
							{
								default:
								case TabStyle.Wizard:
								case TabStyle.Flat:
								case TabStyle.Excel:
								case TabStyle.PropertyPage:
								case TabStyle.PropertyPageSelected:
								case TabStyle.StateButtons:
									appearance.BackColor = SystemColors.Control;
									break;
								case TabStyle.VisualStudio:
									appearance.BackColor = Color.Transparent;
									break;
							}
						}

						requestedProps &= ~AppearancePropFlags.BackColor;
					}
					#endregion //BackColor

					#region ForeColor
					// ForeColor
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.ForeColor))
					{
						//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
						//	GrayText looks too dim when the background has the
						//	Office2003-style gradient, so we'll use ControlText
						//	when not hot tracking, regardless of other states.
						// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
						//if ( isViewStyleOffice2003 )
						if ( isOffice2003orVS2005 )
						{
							if ( isHotTrack )
								//appearance.ForeColor = Office2003Colors.ToolBorder;
								appearance.ForeColor = this.Manager.MSColors.ToolBorder;
                            // MBS 4/3/06
                            // Make non-selected tabs appear 'disabled', per the VS2005 way of displaying tabs
                            // i.e. 'Error List', 'Find Results', and 'Watch', while pinned
                            else if (style == TabStyle.VisualStudio2005 && !isSelected)
                                appearance.ForeColor = SystemColors.ControlDarkDark;
							else
							{
								appearance.ForeColor = SystemColors.ControlText;
							}
						}
                        // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
                        else if (isViewStyleOffice2007Ribbon)
                        {
                            if (isSelected)
                                appearance.ForeColor = Office2007ColorTable.Colors.TabSelectedForeColor;
                            else
                                appearance.ForeColor = Office2007ColorTable.Colors.TabNonSelectedForeColor;
                        }
                        else
                        {
                            if (isHotTrack)
                            {
                                appearance.ForeColor = SystemColors.HotTrack;
                            }
                            else if (isSelected)
                            {
                                if (style == TabStyle.VisualStudio)
                                {
                                    if (isActive)
                                        appearance.ForeColor = SystemColors.WindowText;
                                    else
                                        appearance.ForeColor = SystemColors.GrayText;
                                }
                                else if (style == TabStyle.Excel)
                                    appearance.ForeColor = SystemColors.WindowText;
                                else
                                    appearance.ForeColor = SystemColors.ControlText;
                            }
                            else
                            {
                                switch (style)
                                {
                                    default:
                                    case TabStyle.Wizard:
                                    case TabStyle.Flat:
                                    case TabStyle.Excel:
                                    case TabStyle.PropertyPage:
                                    case TabStyle.PropertyPageSelected:
                                    case TabStyle.StateButtons:
                                        appearance.ForeColor = SystemColors.ControlText;
                                        break;
                                    case TabStyle.VisualStudio:
                                        appearance.ForeColor = SystemColors.GrayText;
                                        break;
                                }
                            }
                        }

						requestedProps &= ~AppearancePropFlags.ForeColor;
					}
					#endregion //ForeColor

					#region BorderColor
					// BorderColor
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BorderColor))
					{
						//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
						// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
						if ( isViewStyleOffice2003 )
						{
							// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
							//appearance.BorderColor = Office2003Colors.ToolBorder;
							appearance.BorderColor = this.Manager.MSColors.ToolBorder;
						}
						else if ( isViewStyleVS2005 )
						{
							if ( isSelected )
								appearance.BorderColor = VisualStudio2005ColorTable.Colors.TabSelectedBorderColor;
							else
								appearance.BorderColor = VisualStudio2005ColorTable.Colors.TabBorderColor;
						}
                        // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
                        else if (isViewStyleOffice2007Ribbon)
                        {
                            // MBS 9/20/06
                            if (isHotTrack && !isSelected)
                                appearance.BorderColor = Office2007ColorTable.Colors.TabNonSelectedHotTrackedBorderColor;
                            else
                                appearance.BorderColor = Office2007ColorTable.Colors.TabBorderColor;

                            requestedProps &= ~AppearancePropFlags.BorderColor;
                        }
                        else
                        {
                            Color backColor;

                            if (appearance.BackColor == Color.Transparent || appearance.BackColor.IsEmpty)
                                backColor = SystemColors.Control;
                            else
                                backColor = appearance.BackColor;

                            appearance.BorderColor = DrawUtility.DarkDark(backColor);
                        }

						requestedProps &= ~AppearancePropFlags.BorderColor;
					}
					#endregion //BorderColor

					#region BorderColor3DBase
					// BorderColor3DBase
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BorderColor3DBase))
					{
						Color backColor;
					
						if (appearance.BackColor == Color.Transparent || appearance.BackColor.IsEmpty)
							backColor = SystemColors.Control;
						else
							backColor = appearance.BackColor;

						appearance.BorderColor3DBase = backColor;
						requestedProps &= ~AppearancePropFlags.BorderColor3DBase;
					}
					#endregion //BorderColor3DBase

				//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
					#region ThemedElementAlpha

					//JDN 8/04/04 NAS2004 Vol3 - Added support for VisualStudio2005 style
					//if ( isViewStyleOffice2003 )
					if ( isOffice2003orVS2005 )
					{
						if ( (requestedProps & AppearancePropFlags.ThemedElementAlpha) == AppearancePropFlags.ThemedElementAlpha &&
							! appearance.HasPropertyBeenSet(AppearancePropFlags.ThemedElementAlpha) )
						{
							appearance.ThemedElementAlpha = Alpha.Transparent;
							requestedProps		&= ~AppearancePropFlags.ThemedElementAlpha;
						}
					}
					#endregion ThemedElementAlpha
		
					#region Image & Text Aligns

					// TextHAlign
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.TextHAlign))
					{
						// MD 11/8/07 - NA 2008 Vol 1
						// If the close buttons can possible show in the tabs, we want to resolve the horizontal alignment
						// to left. Otherwise, showing and hiding the close button when hot tracking makes the test shift
						// awkwardly.
						//appearance.TextHAlign = HAlign.Center;
						appearance.TextHAlign = this.SettingsResolved.CloseButtonLocation == TabCloseButtonLocation.Tab 
							? HAlign.Left 
							: HAlign.Center;

						requestedProps &= ~AppearancePropFlags.TextHAlign;
					}

					// TextVAlign
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.TextVAlign))
					{
						appearance.TextVAlign = VAlign.Middle;
						requestedProps &= ~AppearancePropFlags.TextVAlign;
					}

					// ImageHAlign
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.ImageHAlign))
					{
						appearance.ImageHAlign = HAlign.Left;
						requestedProps &= ~AppearancePropFlags.ImageHAlign;
					}

					// ImageVAlign
					if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.ImageVAlign))
					{
						appearance.ImageVAlign = VAlign.Middle;
						requestedProps &= ~AppearancePropFlags.ImageVAlign;
					}

					#endregion //Image & Text Aligns

					#region FontData
					// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
					// The default behavior of Visual Studio 2005 is to set the selected tab font to bold
					if ( Utilities.ShouldSetAppearanceProperty( ref appearance, ref requestedProps, AppearancePropFlags.FontBold )  &&
                        viewStyle == ViewStyle.VisualStudio2005 && isActive &&
                        // MBS 4/4/06 
                        // Do not bold the font of the selected tab if the TabStyle is VS2005, since VS2005 itself does not
                        // bold this style (i.e. 'Watch', 'Find Symbol', etc. when pinned)
                        this.Manager.TabGroupSettings.TabStyle != TabStyle.VisualStudio2005)
					{
						appearance.FontData.Bold = DefaultableBoolean.True;
						requestedProps &= ~AppearancePropFlags.FontBold;
					}

					// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
					// The default font name for Visual Studio 2005 is "Tahoma"
					if ( Utilities.ShouldSetAppearanceProperty( ref appearance, ref requestedProps, AppearancePropFlags.FontName )  &&
						appearance.FontData.Name != "Tahoma"  &&
						viewStyle == ViewStyle.VisualStudio2005  )
					{
						appearance.FontData.Name = "Tahoma";
						requestedProps &= ~AppearancePropFlags.FontName;
					}
					#endregion FontData

					break;
				case MdiTab.AppearanceResolutionPhase.HotTrack:
					break;
				case MdiTab.AppearanceResolutionPhase.Selected:
					break;
				case MdiTab.AppearanceResolutionPhase.Tab:
					break;
			}
		}
		#endregion //ResolveDefaultTabPhaseAppearance

		#region InitializeSelectedTab
		internal void InitializeSelectedTab( MdiTab tab )
		{
			Debug.Assert( !this.isInitializingSelectedTab, "The 'TabGroup' is already in the process of initializing a selected tab.");

			this.isInitializingSelectedTab = true;

			try
			{
				this.TabManager.SelectedTabItem = tab;

				// make sure that the control gets dirtied
				this.TabManager.Dirty(true, false, false);
			}
			finally
			{
				this.isInitializingSelectedTab = false;
			}
		}
		#endregion //InitializeSelectedTab

		#region InitializeExtent
		internal void InitializeExtent(int extent)
		{
			if (this.extent == extent)
				return;

			this.extent = extent;

			this.DirtyTabManager( MdiTabGroup.GetTabChangeType(TabbedMdiPropertyIds.Extent) );
		}
		#endregion //InitializeExtent

		#region InitializeTabbedMdiManager
		internal void InitializeTabbedMdiManager( UltraTabbedMdiManager manager )
		{
			this.manager = manager;

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			if (this.HasTabGroups)
			{
				foreach(MdiTabGroup tabGroup in this.TabGroups)
					tabGroup.InitializeTabbedMdiManager(manager);
			}

			// AS 1/5/04 WTB1271
			if (this.manager == null)
				this.DisposeTabManager();
		}
		#endregion //InitializeTabbedMdiManager

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		#region InitializeParentTabGroup
		internal void InitializeParentTabGroup( MdiTabGroup parentGroup )
		{
			this.parentGroup = parentGroup;
		}
		#endregion //InitializeParentTabGroup

		#region GetTabChangeType
		internal static TabManagerChangeType GetTabChangeType(TabbedMdiPropertyIds property)
		{
			switch(property)
			{
					// these just invalidate the group...
				case TabbedMdiPropertyIds.CloseButtonAppearance:
				case TabbedMdiPropertyIds.ScrollButtonAppearance:
					// JDN 8/18/04 NAS2004 Vol3
				case TabbedMdiPropertyIds.TabListButtonAppearance: 
				case TabbedMdiPropertyIds.ScrollTrackAppearance:
				case TabbedMdiPropertyIds.TabAreaAppearance:
				// MD 11/8/07 - NA 2008 Vol 1
				case TabbedMdiPropertyIds.HotTrackCloseButtonAppearance:
				case TabbedMdiPropertyIds.PressedCloseButtonAppearance:
				case TabbedMdiPropertyIds.CloseButtonAlignment:
					return TabManagerChangeType.Invalidate;

					// these global changes require a dirty of the font
					// and the tab group metrics
				case TabbedMdiPropertyIds.ActiveTabAppearance:
				case TabbedMdiPropertyIds.HotTrackTabAppearance:
				case TabbedMdiPropertyIds.SelectedTabAppearance:
				case TabbedMdiPropertyIds.TabAppearance:
				case TabbedMdiPropertyIds.Appearance:
					return TabManagerChangeType.Invalidate	|
						TabManagerChangeType.AllMetrics		|
						TabManagerChangeType.Font;

					// these require an invalidation...
				case TabbedMdiPropertyIds.AllowClose:
				case TabbedMdiPropertyIds.ButtonStyle:
				case TabbedMdiPropertyIds.HotTrack:
				case TabbedMdiPropertyIds.ImageTransparentColor:
				case TabbedMdiPropertyIds.ScrollArrowStyle:
				// JDN 8/16/04
				case TabbedMdiPropertyIds.ShowPartialTabs:
					return TabManagerChangeType.Invalidate;

					// who cares...
					

					// the tab manager could care but i don't think it does :-)
					
				default:
					return TabManagerChangeType.None;

					// not sure yet...
				case TabbedMdiPropertyIds.Settings:
				case TabbedMdiPropertyIds.TabGroupSettings:
				case TabbedMdiPropertyIds.TabSettings:
				case TabbedMdiPropertyIds.Text:
					return TabManagerChangeType.None;

					// dirties all the tab item sizes
				case TabbedMdiPropertyIds.TabPadding:
				case TabbedMdiPropertyIds.ImageSize:
				case TabbedMdiPropertyIds.TabHeight:
				case TabbedMdiPropertyIds.UseMnemonics:
				case TabbedMdiPropertyIds.DisplayFormIcon:
				case TabbedMdiPropertyIds.MaxTabWidth:
				case TabbedMdiPropertyIds.MinTabWidth:
				case TabbedMdiPropertyIds.TextOrientation:
				case TabbedMdiPropertyIds.TabOrientation:
				case TabbedMdiPropertyIds.TabSizing:
				case TabbedMdiPropertyIds.TabWidth:
				case TabbedMdiPropertyIds.TabStyle:
				case TabbedMdiPropertyIds.TabButtonStyle:	// this affects the tab border widths
				case TabbedMdiPropertyIds.ImageList:		// this affects whether an image is displayed
				// JAS 12/20/04 Tab Spacing
				case TabbedMdiPropertyIds.SpaceAfterTabs:
				case TabbedMdiPropertyIds.SpaceBeforeTabs:
				// MD 11/8/07 - NA 2008 Vol 1
				case TabbedMdiPropertyIds.CloseButtonLocation:
				case TabbedMdiPropertyIds.CloseButtonVisibility:
					return TabManagerChangeType.Invalidate	|
						TabManagerChangeType.AllMetrics;

				case TabbedMdiPropertyIds.InterTabSpacing:
				case TabbedMdiPropertyIds.ShowButtonSeparators:
				case TabbedMdiPropertyIds.ScrollButtons:
				// MD 12/17/07 - BR29137
				// Uncommented the ShowCloseButton prop id, we need to keep this here just in case the user sets the 
				// ShowCloseButton property we will still dirty the ui
				//// MD 11/8/07 - NA 2008 Vol 1
				//// This property id has a different purpose now
				case TabbedMdiPropertyIds.ShowCloseButton:
				case TabbedMdiPropertyIds.Orientation:
				case TabbedMdiPropertyIds.ScrollButtonTypes:
				case TabbedMdiPropertyIds.ScrollTrackExtent:
				case TabbedMdiPropertyIds.Extent:
				case TabbedMdiPropertyIds.SplitterWidth:
				case TabbedMdiPropertyIds.TabsPerRow:
					// JDN 8/17/04 NAS2004 Vol3
				case TabbedMdiPropertyIds.ShowTabListButton:

					// AS 2/6/04 WTB1314
				case TabbedMdiPropertyIds.TabAreaMargins:

					// AS 4/8/03 Changed to collection type change so we don't 
					// dirty the metrics of all the tabs.
					return TabManagerChangeType.Invalidate	|
						TabManagerChangeType.Collection;

					// a tabs collection level change means the list has changed...
				case TabbedMdiPropertyIds.Tabs:
					return TabManagerChangeType.Invalidate	|
						TabManagerChangeType.Collection;

				case TabbedMdiPropertyIds.MdiTab:
					return TabManagerChangeType.Invalidate	|
						TabManagerChangeType.Collection;
			}
		}
		#endregion //GetTabChangeType

		#region GetTabChangeType
		internal static TabManagerChangeType GetTabChangeType(TabbedMdiPropertyIds propertyChanged, PropChangeInfo propChangeInfo)
		{
			if (propChangeInfo == null)
				return TabManagerChangeType.None;

			if (propChangeInfo.Source is MdiTabGroupSettings ||
				propChangeInfo.Source is MdiTabSettings		 ||
				propChangeInfo.Source is MdiTabsCollection)
			{
				if ( !(propChangeInfo.PropId is TabbedMdiPropertyIds) )
					return TabManagerChangeType.None;

				TabbedMdiPropertyIds property = (TabbedMdiPropertyIds)propChangeInfo.PropId;

				// if the change is directly from the tabgroupsettings, tabsettings
				// or tabs object, pass it along and handle it
				if (propChangeInfo.Trigger == null)
					return GetTabChangeType(property);
				else	// otherwise walk up the chain...
					return GetTabChangeType(property, propChangeInfo.Trigger);
			}
			else if (propChangeInfo.Source is AppearanceBase)
			{
				// based on property and prop id
				switch(propertyChanged)
				{
					case TabbedMdiPropertyIds.CloseButtonAppearance:
					case TabbedMdiPropertyIds.ScrollButtonAppearance:
						// JDN 8/18/04 NAS2004 Vol3
					case TabbedMdiPropertyIds.TabListButtonAppearance:
						return GetTabChangeType(propertyChanged);

						// these global changes require a dirty of the font
						// and the tab group metrics
					case TabbedMdiPropertyIds.ActiveTabAppearance:
					case TabbedMdiPropertyIds.HotTrackTabAppearance:
					case TabbedMdiPropertyIds.SelectedTabAppearance:
					case TabbedMdiPropertyIds.TabAppearance:
					case TabbedMdiPropertyIds.TabAreaAppearance:
					case TabbedMdiPropertyIds.Appearance:
					{
						// if this change involves the font, then it needs
						// to be invalidated and dirtied .. as well as dirtying
						// the font.
						

						if (IsFontDataChange(propChangeInfo))
						{
							return TabManagerChangeType.Invalidate	|
								TabManagerChangeType.AllMetrics		|
								TabManagerChangeType.Font;
						}

						// the tab size is at least partially based on the image...
						if (propChangeInfo.PropId is AppearancePropIds)
						{
							AppearancePropIds appPropId = ((AppearancePropIds)propChangeInfo.PropId);
							
							switch(appPropId)
							{
								case AppearancePropIds.Appearance:
								case AppearancePropIds.ImageHAlign:
								case AppearancePropIds.ImageVAlign:
								case AppearancePropIds.TextHAlign:
								case AppearancePropIds.TextVAlign:
								case AppearancePropIds.Image:
									return TabManagerChangeType.Invalidate | TabManagerChangeType.AllMetrics;
							}
						}

						
						// the metrics - e.g. image? alignment?
						return TabManagerChangeType.Invalidate;
					}
				}
			}
			// AS 2/6/04 WTB1314
			else if (propChangeInfo.Source is Margins)
			{
				// just pass the property
				return GetTabChangeType(propertyChanged);
			}

			return TabManagerChangeType.None;
		}
		#endregion //GetTabChangeType

		//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
		#region AdjustAppearancePropertiesForOrientation
		internal void AdjustAppearancePropertiesForTabOrientation( Color topColor,
																   Color bottomColor,
																   ref AppearanceData appearance,
																   ref AppearancePropFlags requestedProps )
		{
			GradientStyle backGradientStyle = GradientStyle.None;
			Color backColor = topColor;
			Color backColor2 = bottomColor;

			switch ( this.SettingsResolved.TabOrientation )
			{
				case TabOrientation.Default:
				case TabOrientation.TopLeft:
				case TabOrientation.TopRight:
				{
					backGradientStyle = GradientStyle.Vertical;
					backColor = topColor;
					backColor2 = bottomColor;
				}
				break;

				case TabOrientation.BottomLeft:
				case TabOrientation.BottomRight:
				{
                    // MBS 4/4/06
                    // With the VS2005 TabStyle, we will not have a backColor2 since we aren't drawing a gradient
                    if (!backColor2.IsEmpty)
                    {
                        backGradientStyle = GradientStyle.Vertical;
                        backColor = bottomColor;
                        backColor2 = topColor;
                    }
				}
				break;

				case TabOrientation.LeftTop:
				case TabOrientation.LeftBottom:
				{
					backGradientStyle = GradientStyle.Horizontal;
					backColor = topColor;
					backColor2 = bottomColor;
				}
				break;

				case TabOrientation.RightTop:
				case TabOrientation.RightBottom:
				{
                     // MBS 4/4/06
                    // With the VS2005 TabStyle, we will not have a backColor2 since we aren't drawing a gradient
                    if (!backColor2.IsEmpty)
                    {
                        backGradientStyle = GradientStyle.Horizontal;
                        backColor = bottomColor;
                        backColor2 = topColor;
                    }
				}
				break;

			}

			#region BackColor
			if ( (requestedProps & AppearancePropFlags.BackColor) == AppearancePropFlags.BackColor &&
				! appearance.HasPropertyBeenSet(AppearancePropFlags.BackColor) )
			{
				appearance.BackColor = backColor;
				requestedProps		&= ~AppearancePropFlags.BackColor;
			}
			#endregion BackColor

			#region BackColor2
			if ( (requestedProps & AppearancePropFlags.BackColor2) == AppearancePropFlags.BackColor2 &&
				! appearance.HasPropertyBeenSet(AppearancePropFlags.BackColor2) )
			{
				appearance.BackColor2 = backColor2;
				requestedProps		&= ~AppearancePropFlags.BackColor2;
			}
			#endregion BackColor2

			#region BackGradientStyle
			if ( (requestedProps & AppearancePropFlags.BackGradientStyle) == AppearancePropFlags.BackGradientStyle &&
				! appearance.HasPropertyBeenSet(AppearancePropFlags.BackGradientStyle))
			{
				appearance.BackGradientStyle = backGradientStyle;
				requestedProps		&= ~AppearancePropFlags.BackGradientStyle;
			}
			#endregion BackGradientStyle
			

		}
		#endregion AdjustAppearancePropertiesForOrientation

		// AS 3/24/05 NA 2005 Vol 2 - Nested TabGroups
		#region IsDescendantOf
		internal bool IsDescendantOf(MdiTabGroup tabGroup)
		{
			MdiTabGroup tempGroup = this;

			while (tempGroup != null)
			{
				if (tempGroup == tabGroup)
					return true;

				tempGroup = tempGroup.Parent;
			}

			return false;
		}
		#endregion //IsDescendantOf

		// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
		#region CachePreMaximizedExtent
		internal void CachePreMaximizedExtent()
		{
			this.CachePreMaximizedExtent(this.extent);
		}

		internal void CachePreMaximizedExtent(int extent)
		{
			this.preMaximizeExtent = extent;
		}
		#endregion //CachePreMaximizedExtent

        #endregion //Internal

        #region Private

        #region TabManager events

        #region Selecting
        private void OnTabManagerSelectedTabItemChanging(object sender, TabManager.SelectedTabItemChangingEventArgs e)
		{
			// if we are just initializing the selected tab, we don't want the
			// events to fire
			if (this.isInitializingSelectedTab)
				return;

			if (this.Manager == null)
				return;

			CancelableMdiTabEventArgs args = new CancelableMdiTabEventArgs(e.TabItem as MdiTab);

			this.Manager.InvokeEvent( TabbedMdiEventIds.TabSelecting, args );

			// AS 4/16/03
			// only allow cancelling if we can cancel it - i.e.
			// if its already active, we can't prevent it from being activated.
			bool allowCancel = this.Manager.CanAllowSelectionCancel(e.TabItem as MdiTab);

			if (args.Cancel && allowCancel)
				e.Cancel = true;
		}
		#endregion //Selecting

		#region Selected
		private void OnTabManagerSelectedTabItemChanged(object sender, TabManager.SelectedTabItemChangedEventArgs e)
		{
			// if we are just initializing the selected tab, we don't want the
			// events to fire
			if (this.isInitializingSelectedTab)
				return;

			if (this.Manager == null)
				return;

			// AS 4/23/03
			// If the tab manager is just fixing the tab groups to have an
			// initialized tab, we should not be activating the associated form.
			//
			if (this.Manager.AllowSelectedTabActivation)
				this.Manager.ActivateMdiChild(e.TabItem as MdiTab, false, false, false, true);

			MdiTabEventArgs args = new MdiTabEventArgs(e.TabItem as MdiTab);

			this.Manager.InvokeEvent( TabbedMdiEventIds.TabSelected, args );

			// MRS 4/14/04 - Accessibililty
			if ( this.SelectedTab != null )
			{
				MdiTabGroupControl groupControl = this.SelectedTab.TabGroup.Manager.GetTabGroupControl(this.SelectedTab.TabGroup);
				if (groupControl != null)
					groupControl.AccessibilityNotifyClientsInternal(AccessibleEvents.Selection, this.SelectedTab.Index);
			}
		}
		#endregion //Selected

		#region Scrolling / Scrolled
		private void OnTabManagerTabScrolling(object sender, TabScrollingEventArgs e)
		{
			if (this.Manager == null)
				return;

			MdiTabGroupScrollingEventArgs args = new MdiTabGroupScrollingEventArgs(this, e.ScrollType, e.ScrollIncrement);

			this.Manager.InvokeEvent( TabbedMdiEventIds.TabGroupScrolling, args );

			if (args.Cancel)
				e.Cancel = true;
		}

		private void OnTabManagerTabScrolled(object sender, TabScrolledEventArgs e)
		{
			if (this.Manager == null)
				return;

			MdiTabGroupScrolledEventArgs args = new MdiTabGroupScrolledEventArgs(this, e.ScrollType, e.ScrollIncrement);

			this.Manager.InvokeEvent( TabbedMdiEventIds.TabGroupScrolled, args );
		}
		#endregion //Scrolling / Scrolled

		#region Moving / Moved

		private void OnTabManagerTabMoving(object sender, TabManager.TabItemMovingEventArgs e)
		{

		}

		private void OnTabManagerTabMoved(object sender, TabManager.TabItemMovedEventArgs e)
		{
		}

		#endregion // Moving / Moved

		#endregion //TabManager events

		#region ResolveScrollButtonAppearance
		private void ResolveScrollButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			

			UltraTabbedMdiManager manager = this.Manager;

			// first use the specific scroll button appearance
			if (this.HasSettings && this.Settings.HasScrollButtonAppearance)
				this.Settings.ScrollButtonAppearance.MergeData(ref appearance, ref requestedProps);

			if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.HasScrollButtonAppearance)
				manager.TabGroupSettings.ScrollButtonAppearance.MergeData(ref appearance, ref requestedProps);

			if (requestedProps == 0)
				return;

			bool initBackColor = Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BackColor);

			// strip out some properties before resolving the tab area appearance
			requestedProps &= ~(AppearancePropFlags.BackGradientStyle |
				AppearancePropFlags.BackHatchStyle	|
				AppearancePropFlags.ImageBackground	|
				AppearancePropFlags.ImageBackgroundDisabled | // AS 4/7/06 ImageBackgroundDisabled
				AppearancePropFlags.Image | 
				AppearancePropFlags.BackColor);

			// then use the tab area appearance
			if (this.HasSettings && this.Settings.HasTabAreaAppearance)
				this.Settings.TabAreaAppearance.MergeData(ref appearance, ref requestedProps);

			if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.HasTabAreaAppearance)
				manager.TabGroupSettings.TabAreaAppearance.MergeData(ref appearance, ref requestedProps);

			if (requestedProps == 0)
				return;

			// lastly use the manager's appearance
			if (manager != null && manager.HasAppearance)
				manager.Appearance.MergeData(ref appearance, ref requestedProps);

			if (initBackColor)
			{
				// always use a transparent backcolor
				appearance.BackColor = Color.Transparent;
				requestedProps &= ~AppearancePropFlags.BackColor;
			}
		}
		#endregion //ResolveScrollButtonAppearance

		#region ResolveCloseButtonAppearance
		// AS 4/27/06 AppStyling
		//private void ResolveCloseButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		// MD 12/20/07 - Task 1619
		// Added a parameter to the ResolveCloseButtonAppearance method
		//private void ResolveCloseButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, UIElementButtonState buttonState )
		private void ResolveCloseButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, ITabItem tabItem, UIElementButtonState buttonState )
		{
			UltraTabbedMdiManager manager = this.Manager;

			// AS 4/27/06 AppStyling
			AppStyling.ResolutionOrderInfo order = StyleUtils.GetResolutionOrderInfo(manager);

			// MD 12/20/07 - Task 1619
			// Use a different role if the close button is in the selected tab.
			//AppStyling.UIRole role = this.UIRoleCloseButton;
			bool isSelectedTabItem = tabItem != null && tabItem == this.SelectedTab;

			AppStyling.UIRole role = isSelectedTabItem 
				? this.UIRoleSelectedTabItemCloseButton 
				: this.UIRoleCloseButton;

			// MD 11/7/07 - NA 2008 Vol 1
			// We now have apearance properties for the hot tracked and pressed states, 
			// so the resolution is a little different
			//if (null != role && (order.UseStyleBefore || order.UseStyleAfter))
			//{
			//    if (Infragistics.Win.Utilities.IsPressed(buttonState))
			//        role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Pressed);
			//
			//    if (Infragistics.Win.Utilities.IsHotTracked(buttonState))
			//        role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.HotTracked);
			//}
			if ( Infragistics.Win.Utilities.IsPressed( buttonState ) )
			{
				if ( null != role && order.UseStyleBefore )
					role.ResolveAppearance( ref appearance, ref requestedProps, AppStyling.RoleState.Pressed );

				if ( order.UseControlInfo )
				{
					// first use the specific scroll button appearance
					if ( this.HasSettings && this.Settings.HasPressedCloseButtonAppearance )
						this.Settings.PressedCloseButtonAppearance.MergeData( ref appearance, ref requestedProps );

					if ( manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.HasPressedCloseButtonAppearance )
						manager.TabGroupSettings.PressedCloseButtonAppearance.MergeData( ref appearance, ref requestedProps );
				}

				if ( null != role && order.UseStyleAfter )
					role.ResolveAppearance( ref appearance, ref requestedProps, AppStyling.RoleState.Pressed );
			}

			if ( Infragistics.Win.Utilities.IsHotTracked( buttonState ) )
			{
				if ( null != role && order.UseStyleBefore )
					role.ResolveAppearance( ref appearance, ref requestedProps, AppStyling.RoleState.HotTracked );

				if ( order.UseControlInfo )
				{
					// first use the specific scroll button appearance
					if ( this.HasSettings && this.Settings.HasHotTrackCloseButtonAppearance )
						this.Settings.HotTrackCloseButtonAppearance.MergeData( ref appearance, ref requestedProps );

					if ( manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.HasHotTrackCloseButtonAppearance )
						manager.TabGroupSettings.HotTrackCloseButtonAppearance.MergeData( ref appearance, ref requestedProps );
				}

				if ( null != role && order.UseStyleAfter )
					role.ResolveAppearance( ref appearance, ref requestedProps, AppStyling.RoleState.HotTracked );
			}

			if (null != role && order.UseStyleBefore)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);

			if (order.UseControlInfo)
			{
				// first use the specific scroll button appearance
				if (this.HasSettings && this.Settings.HasCloseButtonAppearance)
					this.Settings.CloseButtonAppearance.MergeData(ref appearance, ref requestedProps);

				if (manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.HasCloseButtonAppearance)
					manager.TabGroupSettings.CloseButtonAppearance.MergeData(ref appearance, ref requestedProps);
			}

			if (null != role && order.UseStyleAfter)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);

			#region Commented Out
			
			#endregion //Commented Out
		}
		#endregion //ResolveCloseButtonAppearance

		#region ResolveTabListButtonAppearance
		// JDN 8/18/04 NAS2004 Vol3
		// AS 4/27/06 AppStyling
		//private void ResolveTabListButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		private void ResolveTabListButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, UIElementButtonState buttonState )
		{
			UltraTabbedMdiManager manager = this.Manager;

			// AS 4/27/06 AppStyling
			AppStyling.ResolutionOrderInfo order = StyleUtils.GetResolutionOrderInfo(manager);
			// MD 12/20/07
			// Noticed while implementing Task 1619
			// We were using the wrong role here
			//AppStyling.UIRole role = this.UIRoleCloseButton;
			AppStyling.UIRole role = this.UIRoleTabListButton;

			if (null != role && (order.UseStyleBefore || order.UseStyleAfter))
			{
				if (Infragistics.Win.Utilities.IsPressed(buttonState))
					role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Pressed);

				if (Infragistics.Win.Utilities.IsHotTracked(buttonState))
					role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.HotTracked);
			}

			if (null != role && order.UseStyleBefore)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);

			if (order.UseControlInfo)
			{
				// first use the specific tab list button appearance
				if ( this.HasSettings && this.Settings.HasTabListButtonAppearance )
					this.Settings.TabListButtonAppearance.MergeData( ref appearance, ref requestedProps );

				if ( manager != null && manager.HasTabGroupSettings && manager.TabGroupSettings.HasTabListButtonAppearance )
					manager.TabGroupSettings.TabListButtonAppearance.MergeData( ref appearance, ref requestedProps );
			}

			if (null != role && order.UseStyleAfter)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);
		}
		#endregion ResolveTabListButtonAppearance

		#region IsFontDataChange
		private static bool IsFontDataChange( PropChangeInfo propChangeInfo )
		{
			if (propChangeInfo == null)
				return false;

			if (propChangeInfo.PropId is FontDataPropIds)
				return true;

			if (propChangeInfo.Source is AppearanceBase && 
				propChangeInfo.Trigger != null)
				return IsFontDataChange(propChangeInfo.Trigger);

			if ( !(propChangeInfo.PropId is AppearancePropIds) )
				return false;

			AppearancePropIds appearanceProp = (AppearancePropIds)propChangeInfo.PropId;

			return appearanceProp == AppearancePropIds.FontData ||
				appearanceProp == AppearancePropIds.Appearance;
		}
		#endregion //IsFontDataChange

		#region NotifyTabManager
		private void NotifyTabManager(TabbedMdiPropertyIds propertyChanged, PropChangeInfo propChangeInfo)
		{
			if (propChangeInfo == null)
				return;

			if ( !(propChangeInfo.PropId is TabbedMdiPropertyIds) )
				return;

			// AS 3/16/05 NA 2005 Vol 2 - Nested TabGroups
			// Since a nesting group doesn't present a ui, it
			// shouldn't need to do anything when a descendant
			// group changes.
			//
			if (propChangeInfo.Source == this.tabGroups)
				return;

			TabbedMdiPropertyIds property = (TabbedMdiPropertyIds)propChangeInfo.PropId;

			// we only need to specially handle the tabs collection changes
			// that involve a single tab
			if (propChangeInfo.Source == this.tabs && propChangeInfo.Trigger != null)
			{
				if (this.ProcessTabChangeNotification(propChangeInfo.Trigger))
					return;
			}

			if (propChangeInfo.Trigger == null)
				this.DirtyTabManager(GetTabChangeType(property));
			else if (propChangeInfo.Source != this.tabs)
				this.DirtyTabManager(GetTabChangeType(property, propChangeInfo.Trigger));
		}
		#endregion //NotifyTabManager

		#region ProcessTabChangeNotification
		private bool ProcessTabChangeNotification( PropChangeInfo propChangeInfo )
		{
			if ( !(propChangeInfo.Source is MdiTab) )
				return false;

			MdiTab tab = propChangeInfo.Source as MdiTab;

			PropChangeInfo rootTrigger = propChangeInfo.FindTrigger(null);

			if ( rootTrigger.PropId is TabbedMdiPropertyIds &&
				((TabbedMdiPropertyIds)rootTrigger.PropId) == TabbedMdiPropertyIds.Text)
			{
				this.DirtyTabItem(tab, true, true, false);
			}
			else if (rootTrigger.PropId is AppearancePropIds)
			{
				AppearancePropIds appPropId = (AppearancePropIds)rootTrigger.PropId;
				
				switch(appPropId)
				{
					case AppearancePropIds.Appearance:
					{
						this.DirtyTabItem(tab, true, true, true);
						break;
					}
					case AppearancePropIds.TextHAlign:
					case AppearancePropIds.TextVAlign:
					{
						this.DirtyTabItem(tab, true, true, false);
						break;
					}
					case AppearancePropIds.ImageHAlign:
					case AppearancePropIds.ImageVAlign:
					case AppearancePropIds.Image:
					{
						this.DirtyTabItem(tab, true, false, true);
						break;
					}
					default:
					{
						this.DirtyTabItem(tab, true, false, false);
						break;
					}
				}
			}
			else if ( rootTrigger.PropId is TabbedMdiPropertyIds )
			{
				TabbedMdiPropertyIds mdiProp = ((TabbedMdiPropertyIds)rootTrigger.PropId);
				
				if (mdiProp == TabbedMdiPropertyIds.DisplayFormIcon)
					this.DirtyTabItem(tab, true, false, true);
				else if (mdiProp == TabbedMdiPropertyIds.AllowClose)
				{
					// AS 6/20/05 BR04672
					// The AllowClose affects more then the tab item itself.
					// It also affects the display of the close button and
					// therefore we may need to invalidate the entire
					// tabmanager area.
					//
					if (tab == this.SelectedTab)
						this.DirtyTabManager(TabManagerChangeType.Invalidate);
				}
				else
				{
					TabManagerChangeType changeType = GetTabChangeType(mdiProp);

					// AS 4/16/03 WTB812
					// If we get an all metrics type change, dirty the text and image.
					//
					bool dirty = (changeType & TabManagerChangeType.AllMetrics) != 0;

					bool invalidate = (changeType & TabManagerChangeType.Invalidate) != 0;

					if (dirty || invalidate)
						this.DirtyTabItem(tab, invalidate, dirty, dirty);
				}
			}
			return true;

		}
		#endregion //ProcessTabChangeNotification

		// AS 1/5/04 WTB1271
		#region DisposeTabManager
		private void DisposeTabManager()
		{
			if (this.tabManager != null)
			{
				this.tabManager.SelectedTabItemChanging -= new TabManager.SelectedTabItemChangingEventHandler( this.OnTabManagerSelectedTabItemChanging );
				this.tabManager.SelectedTabItemChanged -= new TabManager.SelectedTabItemChangedEventHandler( this.OnTabManagerSelectedTabItemChanged );
				this.tabManager.TabScrolling -= new TabScrollingEventHandler( this.OnTabManagerTabScrolling );
				this.tabManager.TabScrolled -= new TabScrolledEventHandler( this.OnTabManagerTabScrolled );

				this.tabManager.TabItemMoving -= new TabManager.TabItemMovingEventHandler(this.OnTabManagerTabMoving);
				this.tabManager.TabItemMoved -= new TabManager.TabItemMovedEventHandler(this.OnTabManagerTabMoved);

				this.tabManager.Dispose();
				this.tabManager = null;
			}
		}
		#endregion //DisposeTabManager

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		#region GetLeafTabGroup
		private static MdiTabGroup GetLeafTabGroup(MdiTabGroup startTabGroup, bool previous)
		{
			// if we're going forward and this group has 
			// tab groups, return its first leaf group
			if (!previous && startTabGroup.HasTabGroups)
				return UltraTabbedMdiManager.GetFirstLeafTabGroup(startTabGroup.TabGroups);

			return GetLeafTabGroupHelper(startTabGroup, previous);
		}

		private static MdiTabGroup GetLeafTabGroupHelper(MdiTabGroup startTabGroup, bool previous)
		{
			// ok, we're either want the previous group or 
			// the start tabgroup has tabs...
			MdiTabGroupsCollection tabGroups = startTabGroup.ParentCollection;
			int offset = previous ? -1 : +1;
			int index = startTabGroup.Index + offset;

			// if there is a subsequent sibling tab group, find a tab group
			if (index >= 0 && index < tabGroups.Count)
			{
				MdiTabGroup group = tabGroups[index];

				if (!group.HasTabGroups)
					return group;

				// either return the first or last leaf group
				if (previous)
					return UltraTabbedMdiManager.GetLastLeafTabGroup(group.TabGroups);
				else
					return UltraTabbedMdiManager.GetFirstLeafTabGroup(group.TabGroups);
			}
			
			// there are no siblings after the tab group so go up the parent chain...

			// if there is no parent group, there is nothing left to check
			if (startTabGroup.Parent == null)
				return null;

			return GetLeafTabGroupHelper(startTabGroup.Parent, previous);
		}
		#endregion //GetLeafTabGroup

		#endregion //Private

		#endregion //Methods

		#region Interfaces

		#region ISerializable
		// MD 1/4/07 - FxCop
		// We do not need a full demand here, just a link demand
		//[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinTabbedMdi";

			if (this.ShouldSerializeKey())
				Utils.SerializeProperty(info, "Key", this.Key);

			if (this.ShouldSerializeSettings())
				Utils.SerializeProperty(info, "Settings", this.Settings);

			if (this.ShouldSerializeTabSettings())
				Utils.SerializeProperty(info, "TabSettings", this.TabSettings);

			if (this.ShouldSerializeTabs())
				Utils.SerializeProperty(info, "Tabs", this.Tabs);

			if (this.ShouldSerializeExtent())
				Utils.SerializeProperty(info, "Extent", this.Extent);

			if (this.ShouldSerializeTag())
				this.SerializeTag(info);

			// AS 4/14/03 WTB801
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.FirstDisplayedTab != null)
			if (!this.HasTabGroups && this.FirstDisplayedTab != null)
				Utils.SerializeProperty(info, "FirstDisplayTabIndex", ((ITabItem)this.FirstDisplayedTab).Index);

			// AS 4/14/03 WTB801
			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			//if (this.SelectedTab != null)
			if (!this.HasTabGroups && this.SelectedTab != null)
				Utils.SerializeProperty(info, "SelectedTabIndex", ((ITabItem)this.SelectedTab).Index);

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			if (this.ShouldSerializeTabGroups())
				Utils.SerializeProperty(info, "TabGroups", this.TabGroups);

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			// If we contain the tab group but not directly, 
			if (this.HasTabGroups && this.ContainsActiveTab)
				Utils.SerializeProperty(info, "ActiveTabGroupIndex", this.Manager.GetTabGroupIndex(this.Manager.ActiveTab, this));

			// AS 3/24/05 NA 2005 Vol 2 - Maximized TabGroup
			if (this.Manager != null && this.Manager.MaximizedTabGroup != null)
				Utils.SerializeProperty(info, "PreMaximizedExtent", this.PreMaximizedExtent);

			// AS 10/15/03 DNF116
			// Call the virtual method so derived classes
			// can serialize additional info.
			//
			this.GetObjectData(info, context);
		}
		#endregion // ISerializable

		#region IDeserializationCallback
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			if (this.settings != null)
			{
				this.settings.InitializeOwner(this);
				this.settings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}

			if (this.tabSettings != null)
			{
				this.tabSettings.InitializeOwner(this);
				this.tabSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}

			if (this.tabs != null)
			{
				this.tabs.SubObjectPropChanged += this.SubObjectPropChangeHandler;

				foreach(MdiTab tab in this.Tabs)
					tab.InitializeOwner(this);
			}

			// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
			if (this.tabGroups != null)
			{
				this.tabGroups.SubObjectPropChanged += this.SubObjectPropChangeHandler;

				foreach(MdiTabGroup tabGroup in this.TabGroups)
					tabGroup.InitializeParentTabGroup(this);
			}

			// AS 10/15/03 DNF116
			// Call the virtual method so a derived class
			// could get the call.
			this.OnDeserialization(sender);
		}
		#endregion //IDeserializationCallback

		#region ITabProvider

		#region Methods

		#region CreateMetricsFont
		System.Drawing.Font ITabProvider.CreateMetricsFont()
		{
			if (this.Manager != null)
				return this.Manager.CreateTabMetricsFont(this);

			// otherwise just use the default font
			return System.Windows.Forms.Control.DefaultFont.Clone() as Font;
		}
		#endregion // CreateMetricsFont

		#region ResolveDefaultAppearance
		void ITabProvider.ResolveDefaultAppearance(ref Infragistics.Win.AppearanceData appearance, ref Infragistics.Win.AppearancePropFlags requestedProps)
		{
			this.ResolveTabAreaAppearance(ref appearance, ref requestedProps);
		}
		#endregion // ResolveDefaultAppearance

		#region ResolveClientAreaAppearance
		void ITabProvider.ResolveClientAreaAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			UltraTabbedMdiManager manager = this.Manager;
			AppStyling.ResolutionOrderInfo order = StyleUtils.GetResolutionOrderInfo(manager);
			AppStyling.UIRole role = this.UIRoleTabClientArea;
			// AS 5/2/06 AppStyling
			ViewStyle viewStyle = manager != null ? manager.ViewStyleResolved : ViewStyle.Standard;

			if (null != role && (order.UseStyleBefore || order.UseStyleAfter))
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);

			if (null != role && order.UseStyleBefore)
				role.ResolveAppearance(ref appearance, ref requestedProps, AppStyling.RoleState.Normal);

			//	BF 7.20.04	NAS2004 Vol3 - Office2003 look & feel
			if ( manager != null && viewStyle == ViewStyle.Office2003 )
				return;
			else 
				if ( viewStyle == ViewStyle.VisualStudio2005 ) 
			{
				// JDN 8/11/04 NAS2004 Vol3 - VisualStudio2005 style
				// Apply tab border style to selected tab's top client border
				if ( Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.BorderColor) )
					appearance.BorderColor = VisualStudio2005ColorTable.Colors.TabSelectedBorderColor;
				else 
					return;
			}
			else
			{
				// AS 5/8/03
				// This was added to the ITabProvider interface.
				// We should probably just use the info of the selected tab.
				//
				ITabItem tab = this.SelectedTab;

				if (tab != null)
					tab.ResolveAppearance( ((ITabProvider)this).GetTabItemState(tab), ref appearance, ref requestedProps );
			}
		}
		#endregion // ResolveClientAreaAppearance

		#region ResolveScrollButtonAppearance
		void ITabProvider.ResolveScrollButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			this.ResolveScrollButtonAppearance(ref appearance, ref requestedProps);
		}
		#endregion // ResolveScrollButtonAppearance

		#region ResolveScrollTrackAppearance
		void ITabProvider.ResolveScrollTrackAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			this.ResolveScrollTrackAppearance(ref appearance, ref requestedProps);
		}
		#endregion // ResolveScrollTrackAppearance

		#region GetTabItemState
		TabState ITabProvider.GetTabItemState(ITabItem tab)
		{
			TabState state = TabState.Normal;

			if (tab != null)
			{
				if (this.TabManager.SelectedTabItem == tab)
					state |= TabState.Selected;

				if (!this.TabManager.IsDragging				&&	// no hottracking during a drag
					this.TabManager.HotTrackTabItem == tab	&&	// has to be the hottrack item
					this.Manager != null					&&
					!this.Manager.IsDraggingInternal		&&	// AS 3/25/05 Make sure we're not in a drag operation anywhere.
					tab.HotTrack)								// tab has to allow hottracking
					state |= TabState.Hot;

				if (tab is MdiTab && ((MdiTab)tab).IsFormActive)
					state |= TabState.Active;
			}

			return state;
		}
		#endregion //GetTabItemState

		#endregion //Methods

		#region Properties

		#region AutoSelect
		bool ITabProvider.AutoSelect
		{
			get
			{
				return this.SettingsResolved.AutoSelect;
			}
		}
		#endregion // AutoSelect

		#region AutoSelectDelay
		int ITabProvider.AutoSelectDelay
		{
			get
			{
				return this.SettingsResolved.AutoSelectDelay;
			}
		}
		#endregion // AutoSelectDelay

		#region AllowTabMoving
		bool ITabProvider.AllowTabMoving
		{
			get
			{
				return false;
			}
		}
		#endregion // AllowTabMoving

		#region HotkeyPrefix
		System.Drawing.Text.HotkeyPrefix ITabProvider.HotkeyPrefix
		{
			get
			{
				UltraTabbedMdiManager manager = this.Manager;

				// AS 6/9/03
				// Base it on the tab manager's settings.
				//
				//bool useMnemonics = manager == null ? true : false;
				bool useMnemonics = manager == null ? true : manager.UseMnemonics;

				if (useMnemonics)
					return System.Drawing.Text.HotkeyPrefix.Show;
				else
					return System.Drawing.Text.HotkeyPrefix.None;
			}
		}
		#endregion // HotkeyPrefix

		#region HotTrack
		bool ITabProvider.HotTrack
		{
			get
			{
				// in general we allow hottracking, whether a particular
				// item can or not is based on the resolved settings
				return true;
			}
		}
		#endregion // HotTrack

		#region ImageList
		System.Windows.Forms.ImageList ITabProvider.ImageList
		{
			get
			{
				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null)
					return manager.ImageList;

				return null;
			}
		}
		#endregion // ImageList

		#region ImageSize
		System.Drawing.Size ITabProvider.ImageSize
		{
			get
			{
				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null)
					return manager.ImageSize;

				return new Size(16,16);
			}
		}
		#endregion // ImageSize

		#region ImageTransparentColor
		Color ITabProvider.ImageTransparentColor
		{
			get
			{
				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null)
					return manager.ImageTransparentColor;

				return Color.Transparent;
			}
		}
		#endregion // ImageTransparentColor

		#region InterRowSpacing
		DefaultableInteger ITabProvider.InterRowSpacing
		{
			get
			{
				return DefaultableInteger.Default;
			}
		}
		#endregion // InterRowSpacing

		#region InterTabSpacing
		DefaultableInteger ITabProvider.InterTabSpacing
		{
			get
			{
				return this.SettingsResolved.InterTabSpacing;
			}
		}
		#endregion // InterTabSpacing

		#region MaxAvailableTabAreaSize
		System.Drawing.Size ITabProvider.MaxAvailableTabAreaSize
		{
			get
			{
				if (this.Manager == null)
					return Size.Empty;

				// get it from the manager
				return this.Manager.TabGroupManager.CalcMaxAvailableTabGroupSize(this);
			}
		}
		#endregion // MaxAvailableTabAreaSize

		#region MaxTabWidth
		int ITabProvider.MaxTabWidth
		{
			get
			{
				return this.SettingsResolved.MaxTabWidth;
			}
		}
		#endregion // MaxTabWidth

		#region MinTabWidth
		int ITabProvider.MinTabWidth
		{
			get
			{
				return this.SettingsResolved.MinTabWidth;
			}
		}
		#endregion // MinTabWidth

		#region MaxVisibleTabRows
		int ITabProvider.MaxVisibleTabRows
		{
			get
			{
				return 1;
			}
		}
		#endregion // MaxVisibleTabRows

		#region MultiRowSelectionStyle
		Infragistics.Win.UltraWinTabs.MultiRowSelectionStyle ITabProvider.MultiRowSelectionStyle
		{
			get
			{
				return MultiRowSelectionStyle.DefaultForStyle;
			}
		}
		#endregion // MultiRowSelectionStyle

		#region ScaleImages
		Infragistics.Win.ScaleImage ITabProvider.ScaleImages
		{
			get
			{
				return Infragistics.Win.ScaleImage.OnlyWhenNeeded;
			}
		}
		#endregion // ScaleImages

		#region ScrollArrowStyle

		ScrollArrowStyle ITabProvider.ScrollArrowStyle
		{
			get { return this.SettingsResolved.ScrollArrowStyle; }
		}

		#endregion //ScrollArrowStyle

		#region ScrollButtons
		Infragistics.Win.UltraWinTabs.TabScrollButtons ITabProvider.ScrollButtons
		{
			get
			{
				return this.SettingsResolved.ScrollButtons;
			}
		}
		#endregion // ScrollButtons

		#region ScrollButtonTypes
		Infragistics.Win.UltraWinTabs.ScrollButtonTypes ITabProvider.ScrollButtonTypes
		{
			get
			{
				return this.SettingsResolved.ScrollButtonTypes;
			}
		}
		#endregion // ScrollButtonTypes

		#region ScrollButtonStyle

		/// <summary>
		/// Determines the visual style of the scroll buttons.
		/// </summary>
		UIElementButtonStyle ITabProvider.ScrollButtonStyle 
		{ 
			get 
			{ 
				return this.SettingsResolved.ButtonStyle;
			}
		}

		#endregion ScrollButtonStyle

		#region ScrollTrackExtent
		int ITabProvider.ScrollTrackExtent
		{
			get
			{
				return this.SettingsResolved.ScrollTrackExtent;
			}
		}
		#endregion // ScrollTrackExtent

		#region ShowButtonSeparators
		bool ITabProvider.ShowButtonSeparators
		{
			get
			{
				return this.SettingsResolved.ShowButtonSeparators;
			}
		}
		#endregion // ShowButtonSeparators

		#region ShowCloseButton
		bool ITabProvider.ShowCloseButton
		{
			get
			{
				// MD 11/8/07 - NA 2008 Vol 1
				// The ShowCloseButton property is now obsolete
				//return this.SettingsResolved.ShowCloseButton;
				return this.SettingsResolved.CloseButtonLocation != TabCloseButtonLocation.None;
			}
		}
		#endregion // ShowCloseButton

		#region ShowFocus
		bool ITabProvider.ShowFocus
		{
			get	{ return false; }
		}
		#endregion // ShowFocus

		#region ShowToolTips
		bool ITabProvider.ShowToolTips
		{
			get
			{
				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null)
					return manager.ShowToolTips;

				return true;
			}
		}
		#endregion // ShowToolTips

		#region Style
		Infragistics.Win.UltraWinTabs.TabStyle ITabProvider.Style
		{
			get
			{
				return this.SettingsResolved.TabStyle;
			}
		}
		#endregion // Style

		#region TabButtonStyle
		Infragistics.Win.UIElementButtonStyle ITabProvider.TabButtonStyle
		{
			get
			{
				return this.SettingsResolved.TabButtonStyle;
			}
		}
		#endregion // TabButtonStyle

		#region TabLayoutStyle
		Infragistics.Win.UltraWinTabs.TabLayoutStyle ITabProvider.TabLayoutStyle
		{
			get
			{
				TabLayoutStyle tabLayoutStyle;

				switch ( this.SettingsResolved.TabSizing )
				{
					default:
					case TabSizing.AutoSize:
						tabLayoutStyle = TabLayoutStyle.SingleRowAutoSize;
						break;
					case TabSizing.Compressed:
						tabLayoutStyle = TabLayoutStyle.SingleRowCompressed;
						break;
					case TabSizing.Fixed:
						tabLayoutStyle = TabLayoutStyle.SingleRowFixed;
						break;
					case TabSizing.Justified:
						tabLayoutStyle = TabLayoutStyle.SingleRowJustified;
						break;
					case TabSizing.SizeToFit:
						tabLayoutStyle = TabLayoutStyle.SingleRowSizeToFit;
						break;
					case TabSizing.TabsPerRow:
						tabLayoutStyle = TabLayoutStyle.SingleRowTabsPerRow;
						break;
				}

				return tabLayoutStyle;				
			}
		}
		#endregion // TabLayoutStyle

		#region Tabs
		IList ITabProvider.Tabs
		{
			get 
			{ 
				return this.Tabs.InternalTabList;
			}
		}
		#endregion // Tabs

		#region TabOrientation
		Infragistics.Win.UltraWinTabs.TabOrientation ITabProvider.TabOrientation
		{
			get
			{
				return this.SettingsResolved.TabOrientation; 
			}
		}
		#endregion // TabOrientation

		#region TabPadding
		System.Drawing.Size ITabProvider.TabPadding
		{
			get
			{
				return this.SettingsResolved.TabPadding;
			}
		}
		#endregion // TabPadding

		#region TabSize
		System.Drawing.Size ITabProvider.TabSize
		{
			get
			{
				int width = -1;

			    if (this.HasTabSettings)
					width = this.TabSettings.TabWidth;

				if (width < 0 && this.Manager != null && this.Manager.HasTabSettings)
					width = this.Manager.TabSettings.TabWidth;

				int height = this.SettingsResolved.TabHeight;

				return new Size(width, height);
			}
		}
		#endregion // TabSize

		#region TabsPerRow
		int ITabProvider.TabsPerRow
		{
			get
			{
				return this.SettingsResolved.TabsPerRow;
			}
		}
		#endregion // TabsPerRow

		#region TextOrientation
		Infragistics.Win.UltraWinTabs.TextOrientation ITabProvider.TextOrientation
		{
			get
			{
				return this.SettingsResolved.TextOrientation;
			}
		}
		#endregion // TextOrientation

		#region VisibleTabsCount
		int ITabProvider.VisibleTabsCount
		{
			get
			{
				int count = this.Tabs.Count;

				// when the form is being disposed, we may still
				// have our tabs but they may not be visible
				//
				if (count > 0 && !((ITabItem)this.Tabs[0]).Visible)
				{
					int total = 0;

					foreach(ITabItem tab in this.Tabs)
					{
						if (tab.Visible)
							total++;
					}

					return total;
				}

				return count;
			}
		}
		#endregion // VisibleTabsCount


		#endregion //Properties

		// MRS 4/12/04 - Added for Accessibility Support
		#region Accessibility
			
			#region GetMarshallingControl

			/// <summary>
			/// Returns the control used to synchronize accessibility calls.
			/// </summary>
			/// <returns>A control to be used to synchronize accessibility calls.</returns>
			Control ITabProvider.GetMarshallingControl()
			{				
				return (Control)this.Manager.GetTabGroupControl(this);
			}

			#endregion GetMarshallingControl

			#region GetTabParentAccessibleObject

			/// <summary>
			/// Gets the Accessible Object of the parent of the tabs.
			/// </summary>
			/// <returns>Returns the Accessible Object of that parent of the tabs. </returns>
			AccessibleObject ITabProvider.GetTabParentAccessibleObject()
			{
				return this.Manager.GetTabGroupControl(this).AccessibilityObject;								
			}

			#endregion GetTabParentAccessibleObject

			#endregion Accessibility

		#endregion //ITabProvider

		#endregion //Interfaces

		#region Nested classes

		#region MdiTabGroupManager
		/// <summary>
		/// Class used to manage the placement and display of the <see cref="MdiTab"/> objects
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MdiTabGroupManager</b> is a customized <see cref="Infragistics.Win.UltraWinTabs.TabManager"/> 
		/// class used to manage the size, display and positioning of <see cref="MdiTab"/> objects. An instance is created by 
		/// an <see cref="MdiTabGroup"/> using the <see cref="MdiTabGroup.CreateTabManager"/> method.</p>
		/// </remarks>
		protected class MdiTabGroupManager : Infragistics.Win.UltraWinTabs.TabManager
		{
			#region Constructor
			/// <summary>
			/// Initializes a new <see cref="MdiTabGroupManager"/>
			/// </summary>
			/// <param name="tabGroup"><b>MdiTabGroup</b> providing the tabs and settings</param>
			public MdiTabGroupManager(MdiTabGroup tabGroup) : base(tabGroup)
			{
			}
			#endregion //Constructor

			#region Properties

			#region TabGroup
			/// <summary>
			/// Returns the <see cref="MdiTabGroup"/> providing the tabs and settings.
			/// </summary>
			public MdiTabGroup TabGroup
			{
				get { return this.TabProvider as MdiTabGroup; }
			}
			#endregion //TabGroup

			#region Manager
			/// <summary>
			/// Returns the owning <see cref="UltraTabbedMdiManager"/>
			/// </summary>
			[Browsable(false)]
			[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
			public UltraTabbedMdiManager Manager
			{
				get 
				{ 
					if (this.TabGroup != null)
						return this.TabGroup.Manager;

					return null; 
				}
			}
			#endregion //Manager

			#region AllowClose
			
			#endregion //AllowClose

			// AS 10/10/03 WTB1177
			#region ButtonAlignment
			/// <summary>
			/// Returns the alignment of the buttons with respect to the <see cref="Infragistics.Win.UltraWinTabs.TabManager.TabOrientationResolved"/>
			/// </summary>
			/// <remarks>
			/// <p class="body">The tabbed mdi buttons are centered within the tab group area.</p>
			/// </remarks>
			protected override VAlign ButtonAlignment
			{
				get { return VAlign.Middle; }
			}
			#endregion //ButtonAlignment

			#region ShowPartialTabs
			/// <summary>
			/// Determines if partial tabs should be displayed.
			/// </summary>
			/// <remarks>
			/// <p class="body">The default value is true unless the ViewStyle
			///  is set to VisualStudio2005.</p>
			/// </remarks>
			protected override bool ShowPartialTabs
			{
				get { return this.TabGroup.SettingsResolved.ShowPartialTabs; }
			}
			#endregion ShowPartialTabs

			#region ShowTabListButton
			// JDN 8/17/04 NAS2004 Vol3
			/// <summary>
			/// Determines if the tab list button should be displayed.
			/// </summary>
			/// <remarks>
			/// <p class="body">The default value is false unless the ViewStyle
			///  is set to VisualStudio2005.</p>
			/// </remarks>
			protected override bool ShowTabListButton
			{
				get { return this.TabGroup.SettingsResolved.ShowTabListButton; }
			}
			#endregion ShowTabListButton			

			// JAS 12/20/04 Tab Spacing
			#region SpaceAfterTabs

			/// <summary>
			/// Returns the minimum amount of space in the visible area after the tabs.
			/// </summary>
			protected override int SpaceAfterTabs
			{
				get
				{
					if( ! this.TabGroup.SettingsResolved.SpaceAfterTabs.IsDefault )
						return this.TabGroup.SettingsResolved.SpaceAfterTabs.Value;

					return base.SpaceAfterTabs;
				}
			}

			#endregion // SpaceAfterTabs

			// JAS 12/20/04 Tab Spacing
			#region SpaceBeforeTabs

			/// <summary>
			/// Returns the amount of space in the visible area before the tabs.
			/// </summary>
			protected override int SpaceBeforeTabs
			{
				get
				{
					if( ! this.TabGroup.SettingsResolved.SpaceBeforeTabs.IsDefault )
						return this.TabGroup.SettingsResolved.SpaceBeforeTabs.Value;

					return base.SpaceBeforeTabs;
				}
			}

			#endregion // SpaceBeforeTabs

			#region TabListContextMenuStyle
			// JDN 8/17/04 NAS2004 Vol3
			/// <summary>
			/// Determines the style of the tab list context menu.
			/// </summary>
			/// <remarks>
			/// <p class="body">The default value is true unless the ViewStyle
			///  is set to VisualStudio2005.</p>
			/// </remarks>
			protected override MenuStyle TabListContextMenuStyle
			{
				get	
				{
					// AS 5/2/06 AppStyling
					ViewStyle viewStyle = this.Manager != null ? this.Manager.ViewStyleResolved : ViewStyle.Standard;

                    if (viewStyle == ViewStyle.VisualStudio2005)
                        return MenuStyle.VisualStudio2005;
                    else if (viewStyle == ViewStyle.Office2003)
                        return MenuStyle.Office2003;
                    // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
                    else if (viewStyle == ViewStyle.Office2007)
                        return MenuStyle.Office2007;
                    else //Standard
                        return MenuStyle.OfficeXP;
				}
			}
			#endregion TabListContextMenuStyle

			// AS 2/6/04 WTB1314
			#region TabAreaMargins
			/// <summary>
			/// Returns a <see cref="Margins"/> instance that indicates the amount of space between the edge of the tab area and the elements within it.
			/// </summary>
			protected override Infragistics.Win.Margins TabAreaMargins
			{
				get { return this.TabGroup.SettingsResolved.TabAreaMargins; }
			}
			#endregion //TabAreaMargins

			#region CLR2
			// AS 11/9/05
			// Add a TextRenderingMode on the TabManager to get the 
			// flags used to determine if the text should be measured/drawn
			// using gdi or gdi+. This should delegate off to the 
			// ultracontrolbase impl that we recently added.
			//
			#region TextRenderingMode
			/// <summary>
			/// Returns the text rendering mode for the tab items.
			/// </summary>
			protected override TextRenderingMode TextRenderingMode
			{
				get { return this.Manager == null ? base.TextRenderingMode : this.Manager.TextRenderingMode; }
			}
			#endregion //TextRenderingMode
			#endregion //CLR2

			// AS 4/27/06 AppStyling
			#region ComponentRole
			/// <summary>
			/// Returns the <see cref="AppStyling.ComponentRole"/> associated with the tab area.
			/// </summary>
			protected override AppStyling.ComponentRole ComponentRole
			{
				get { return this.Manager != null ? this.Manager.ComponentRole : null;}
			}
			#endregion //ComponentRole

			// MD 11/8/07 - NA 2008 Vol 1
			#region CloseButtonLocation

			/// <summary>
			/// Determines where the close button(s) will be shown when the <see cref="ITabProvider.ShowCloseButton"/> returns true.
			/// </summary>
			protected override TabCloseButtonLocation CloseButtonLocation
			{
				get
				{
					if ( this.TabGroup != null )
						return this.TabGroup.SettingsResolved.CloseButtonLocation;

					return base.CloseButtonLocation;
				}
			}

			#endregion CloseButtonLocation

			#endregion //Properties

			#region Methods

			#region CreateDragManager
			/// <summary>
			/// Invoked when a <see cref="TabDragManager"/> must be created to manage a drag operation
			/// </summary>
			/// <remarks>
			/// <p class="note">The <b>MdiTabDragManager</b> returned is responsible for invoking the drag related 
			/// events as it receives mouse notifications from the <b>TabManager</b>. If a different 
			/// TabDragManager is returned, the drag events will not be invoked.</p>
			/// </remarks>
			/// <returns>An <see cref="MdiTabDragManager"/> to be used to manage drag operations</returns>
			protected override TabDragManager CreateDragManager()
			{
				return new MdiTabDragManager(this.TabGroup, this);
			}
			#endregion //CreateDragManager

			#region ResolveCloseButtonAppearance

			/// <summary>
			/// Resolves the appearance for the tab close button.
			/// </summary>
			/// <param name="appearance">Appearance structure to update</param>
			/// <param name="requestedProps">Appearance properties to resolve</param>
			/// <param name="tabItem">The tab which contains the close button or null if the close butotn is in the header area.</param>
			/// <param name="buttonState">Current state of the button element</param>
			// MD 12/20/07 - Task 1619
			// Added a parameter to the ResolveCloseButtonAppearance method
			//protected override void ResolveCloseButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, UIElementButtonState buttonState )
			protected override void ResolveCloseButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, ITabItem tabItem, UIElementButtonState buttonState )
			{
				// AS 4/27/06 AppStyling
				//this.TabGroup.ResolveCloseButtonAppearance(ref appearance, ref requestedProps);
				// MD 12/20/07 - Task 1619
				// Added a parameter to the ResolveCloseButtonAppearance method
				//this.TabGroup.ResolveCloseButtonAppearance(ref appearance, ref requestedProps, buttonState);
				this.TabGroup.ResolveCloseButtonAppearance( ref appearance, ref requestedProps, tabItem, buttonState );
			}

			#endregion //ResolveCloseButtonAppearance

			#region ResolveTabListButtonAppearance
			// JDN 8/18/04 NAS2004 Vol3
			/// <summary>
			/// Resolves the appearance for the tab list button.
			/// </summary>
			/// <param name="appearance">Appearance structure to update</param>
			/// <param name="requestedProps">Appearance properties to resolve</param>
			/// <param name="buttonState">Current state of the button element</param>
			protected override void ResolveTabListButtonAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, UIElementButtonState buttonState )
			{
				// AS 4/27/06 AppStyling
				//this.TabGroup.ResolveTabListButtonAppearance( ref appearance, ref requestedProps );
				this.TabGroup.ResolveTabListButtonAppearance( ref appearance, ref requestedProps, buttonState );
			}

			#endregion ResolveTabListButtonAppearance

			#region CloseButtonClick
			/// <summary>
			/// Invoked when the close button is clicked.
			/// </summary>
			/// <param name="tab">The tab for which the close button was clicked or null if the close button within the tab header area is clicked.</param>
			/// <param name="reason">The reason the tab is closing.</param>
			// MD 11/7/07 - NA 2008 Vol 1
			// This is a breaking change, the virtual member has been changed on the base
			//protected override void CloseButtonClick()
			// MD 12/6/07 - BR28964
			// We need a way to determine how the tab was closed.
			//protected override void CloseButtonClick( ITabItem tab )
			protected override void CloseButtonClick( ITabItem tab, TabCloseReason reason )
			{
				// MD 12/6/07 - NA 2008 Vol 1
				// At design time we should not close tabs
				if ( this.TabGroup != null && 
					this.TabGroup.Manager != null &&
					this.TabGroup.Manager.IsDesignMode )
					return;

				// MD 11/8/07 - NA 2008 Vol 1
				// Don't always close the selected tab, the tab may be passed in now
				//// close the selected tab...
				//if (this.TabGroup != null)
				//{
				//    MdiTab tab = this.TabGroup.SelectedTab as MdiTab;
				//
				//    if (tab != null)
				//        tab.Close();
				//}
				MdiTab mdiTab = tab as MdiTab ?? this.SelectedTabItem as MdiTab;

				if ( mdiTab != null )
				{
					// MD 1/7/08 - BR29406
					// Added a close reason so the handler of the TabClosing and TabClosed events can determine how the tab closed.
					//mdiTab.Close();
					mdiTab.Close( (MdiTabCloseReason)reason );
				}
			}
			#endregion CloseButtonClick

			#region MouseDownTab
			/// <summary>
			/// Invoked when the mouse is pressed on the tab element.
			/// </summary>
			/// <param name="element">Element representing the tab item</param>
			/// <param name="tab">Associated tab</param>
			/// <param name="e">Mouse event arguments</param>
            /// <returns>true if the mouse message was handled; otherwise, false.</returns>
			protected override bool MouseDownTab( UIElement element, ITabItem tab, MouseEventArgs e )
			{
				// when the right button is pressed, select the tab and exit
				if (e.Button == MouseButtons.Right)
				{
					// select the tab and exit
					this.TabClick(tab);
					return true;
				}

				return base.MouseDownTab(element, tab, e);
			}
			#endregion //MouseDownTab

			#region TabClick
			/// <summary>
			/// Invoked when the tab has been clicked
			/// </summary>
			/// <param name="tab">Associated tab</param>
			protected override void TabClick( ITabItem tab )
			{
				base.TabClick(tab);

				MdiTab mdiTab = tab as MdiTab;

				// AS 9/18/03 WTB1159
				//if (mdiTab != null)
				if (mdiTab != null && mdiTab.IsSelected)
					mdiTab.Activate();
			}
			#endregion //TabClick

			// AS 6/2/03
			// Need a way to get a tab item based on an imagelist.
			//
			#region GetImageList
			/// <summary>
			/// Returns the imagelist used to retreive the image for the specified tab
			/// </summary>
			/// <param name="tab">Tab whose associated imagelist should be returned</param>
			/// <returns></returns>
			protected override ImageList GetImageList( ITabItem tab )
			{
				MdiTab mdiTab = tab as MdiTab;

				// when dealing with a custom mdi tab, we need
				// to use its imagelist if it has a tab image
				if (mdiTab != null		&& 
					mdiTab.IsCustomTab	&& 
					mdiTab.CustomMdiTab.HasTabImage)
					return mdiTab.CustomMdiTab.ImageList;

				return base.GetImageList(tab);
			}
			#endregion //GetImageList

			#region GetImageColorMapTable
		
			/// <summary>
			/// Returns the color map used to remap colors in the tab images
			/// </summary>
			/// <param name="tab">Tab item for which the image</param>
			/// <param name="appearanceImage">Image property returned from the resolved Appearance data for the tab</param>
			/// <returns>A color map array of colors to remap or null</returns>
			protected override System.Drawing.Imaging.ColorMap[] GetImageColorMapTable( ITabItem tab, object appearanceImage )
			{
				if ( appearanceImage == null )
					return null;

				MdiTab mdiTab = tab as MdiTab;

				ICustomMdiTab customTab = mdiTab == null ? null : mdiTab.CustomMdiTab;

				// when dealing with a custom mdi tab, we need
				// to use its imagelist if it has a tab image
				if (customTab != null && customTab.HasTabImage)
				{
					// get the ImageTransparentColor color
					Color transparentColor = customTab.ImageTransparentColor;

					// if the image is from an imagelist (i.e. not set as an image object)
					// use the imagelist' transparent color if it isn't Color.Transparent
					if (customTab.ImageList != null &&
						!(appearanceImage is Image))
					{
						if ( customTab.ImageList.TransparentColor != Color.Transparent )
							transparentColor = customTab.ImageList.TransparentColor;
					}

					// Only return a remap table if the transparentColor is
					// not Color.Transparent
					if ( transparentColor != Color.Transparent )
					{
						System.Drawing.Imaging.ColorMap colorMap = new System.Drawing.Imaging.ColorMap();

						colorMap.NewColor = Color.Transparent;
						colorMap.OldColor = transparentColor;

						return new System.Drawing.Imaging.ColorMap[] { colorMap };
					}

					return null;
				}

				return base.GetImageColorMapTable(tab, appearanceImage);
			}

			#endregion //GetImageColorMapTable

			#region GetTabListItemImage
			// JDN 8/18/04 NAS2004 Vol3
			/// <summary>
			/// Returns the image for the specified tab
			/// </summary>
			///<param name="tab">Tab whose associated image should be returned</param>
			/// <returns>Image</returns>
			protected override Image GetTabListItemImage(ITabItem tab)
			{
				Image image = 	base.GetTabListItemImage( tab );

				// Currently returns tab's Appearance Image property if set, 
				// otherwise null.  Uncommenting the code below will return the Form's
				// Icon as the tab list item image if the tab's appearance image prop has
				// not been set.  JDN TODO:  Possibly add a public property that allows one
				// to specify if the Form Icon should be used.

				//if ( image != null )
					return image;
				 
				//MdiTab mdiTab = tab as MdiTab;
				//if ( mdiTab != null )
					//return mdiTab.FormIcon;
				
				//return null;
			}
			#endregion GetTabListItemImage

			// AS 4/27/06 AppStyling
			#region AppStyling

			#region GetRole(TabRole)
			/// <summary>
			/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with the specified area.
			/// </summary>
			/// <param name="role">Enumeration indicating the type of uirole to obtain.</param>
			/// <returns>The UIRole associated with the specified area</returns>
			protected override AppStyling.UIRole GetRole(TabRole role)
			{
				AppStyling.UIRole uiRole;

				if (this.TabGroup != null)
				{
					switch(role)
					{
						case TabRole.ClientArea:
							uiRole = this.TabGroup.UIRoleTabClientArea;
							break;
						// MD 12/20/07 - Task 1619
						// Added a new TabRole to distinguish regular close buttons from close buttons in selected tabs.
						case TabRole.SelectedTabItemCloseButton:
							uiRole = this.TabGroup.UIRoleSelectedTabItemCloseButton;
							break;
						case TabRole.CloseButton:
							uiRole = this.TabGroup.UIRoleCloseButton;
							break;
						case TabRole.TabListButton:
							uiRole = this.TabGroup.UIRoleTabListButton;
							break;
						case TabRole.ItemArea:
							uiRole = this.TabGroup.UIRoleTabArea;
							break;
						default:
							System.Diagnostics.Debug.Assert(false, "A tab role that is not specific to UltraDockManager is being requested:" + role.ToString());
							return base.GetRole(role);
					}
				}
				else
					uiRole = base.GetRole(role);

				return uiRole;
			}
			#endregion //GetRole(TabRole)

			#region GetTabItemRole
			/// <summary>
			/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with the specified tab item.
			/// </summary>
            /// <param name="item">The tab item for which to retrieve a role.</param>
            /// <returns>The <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with the specified tab item.</returns>
			protected override AppStyling.UIRole GetTabItemRole(ITabItem item)
			{
				MdiTab tab = item as MdiTab;

				return tab != null ? tab.UIRole : base.GetTabItemRole(item);
			}
			#endregion //GetTabItemRole

			#endregion //AppStyling

            // MBS 8/4/06 NAS2006 Vol3 - Office2007 look & feel
            #region ShouldDrawGlow
            /// <summary>
            /// Whether a glow effect should be drawn on the tab item.
            /// </summary>
            /// <param name="tab">The tab in question.</param>
            /// <param name="shouldDraw">Set to true to draw the glow.</param>
            /// <param name="glowColor">The base color of the glow.</param>
            /// <param name="glowOrientation">The location of the glow.</param>
            /// <param name="circleExtentPercentage">The percentage of the height/width, depending on the orientation, of the 
            /// <see cref="Rectangle"/> to draw the top of the glow.</param>
            /// <param name="firstFalloff">The percentage used to draw the inner glow, based on the distance from the edge of the tab,
            /// or the middle of the circle, whichever is closer, to the edge of the circle.</param>
            /// <param name="secondFalloff">The percentage used to draw the outer glow, based on the distance from the edge of the tab,
            /// or the middle of the circle, whichever is closer, to the edge of the circle.</param>
            /// <param name="drawAmbientGlow">Whether to draw an ambient glow gradient with the glow color.</param>
            // MBS 9/20/06
            //protected override void ShouldDrawGlow(ITabItem tab, out bool shouldDraw, out Color glowColor, out GlowOrientation glowOrientation, out int circleExtentPercentage, out int firstFalloff, out int secondFalloff)
            protected override void ShouldDrawGlow(ITabItem tab, out bool shouldDraw, out Color glowColor, out GlowOrientation glowOrientation, out int circleExtentPercentage, out int firstFalloff, out int secondFalloff, out bool drawAmbientGlow)
            {
                if (this.Manager.ViewStyleResolved == ViewStyle.Office2007 && this.TabGroup.SettingsResolved.TabStyle == TabStyle.Office2007Ribbon &&
                    tab.HotTrack && this.TabGroup.HotTrackTab == tab &&
                    this.TabGroup.SelectedTab != tab &&
                    // MBS 10/16/06 BR16790
                    this.TabGroup.IsDefaultNonSelectedHotTrackedAppearance)
                {
                    shouldDraw = true;
                    glowColor = Office2007ColorTable.Colors.TabNonSelectedHotTrackedGlowColor;
                    drawAmbientGlow = false; // MBS 9/20/06

					if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Black)
                    {
                        circleExtentPercentage = 40;
                        firstFalloff = 5;
                        secondFalloff = 100;

                        // MBS 9/20/06
                        drawAmbientGlow = true;
                    }
                    else
                    {
                        circleExtentPercentage = 80;
                        firstFalloff = 50;
                        secondFalloff = 90;
                    }

                    #region GlowOrientation
                    switch (this.TabGroup.SettingsResolved.TabOrientation)
                    {
                        case (TabOrientation.BottomLeft):
                        case (TabOrientation.BottomRight):
                            if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Blue ||
                                 Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Silver)
                                glowOrientation = GlowOrientation.Bottom;
                            else
                                glowOrientation = GlowOrientation.Top;
                            break;

                        case (TabOrientation.LeftBottom):
                        case (TabOrientation.LeftTop):
                            if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Blue ||
                                 Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Silver)
                                glowOrientation = GlowOrientation.Left;
                            else
                                glowOrientation = GlowOrientation.Right;
                            break;

                        case (TabOrientation.RightBottom):
                        case (TabOrientation.RightTop):
                            if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Blue ||
                                 Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Silver)
                                glowOrientation = GlowOrientation.Right;
                            else
                                glowOrientation = GlowOrientation.Left;
                            break;

                        default:
                        case (TabOrientation.TopLeft):
                        case (TabOrientation.TopRight):
                            if (Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Blue ||
                                 Office2007ColorTable.ColorSchemeResolved == Office2007ColorScheme.Silver)
                                glowOrientation = GlowOrientation.Top;
                            else
                                glowOrientation = GlowOrientation.Bottom;
                            break;
                    }
                    #endregion //GlowOrientation
                }
                else
                    base.ShouldDrawGlow(tab, out shouldDraw, out glowColor, out glowOrientation, out circleExtentPercentage, out firstFalloff, out secondFalloff, out drawAmbientGlow);
            }

            #endregion //ShouldDrawGlow

			// AS 10/30/07 NA 2008 Vol 1
			#region CanCloseTab

			/// <summary>
			/// Used to determine if the close button for the specified tab is enabled.
			/// </summary>
			/// <param name="tab">The tab whose close button state is being queried or null if the close button is displayed within the header area.</param>
			protected override bool CanCloseTab( ITabItem tab )
			{
				MdiTab mdiTab = tab as MdiTab ?? this.SelectedTabItem as MdiTab;

				if ( mdiTab != null )
					return mdiTab.SettingsResolved.AllowClose;

				return false;
			}

			#endregion //CanCloseTab

			// MD 11/8/07 - NA 2008 Vol 1
			#region GetCloseButtonAlignment

			/// <summary>
			/// Returns a value indicating whether a tab item's close button is visible in the tab.
			/// </summary>
			/// <param name="tab">The tab for which the close button will be displayed.</param>
			/// <returns></returns>
			protected override TabCloseButtonAlignment GetCloseButtonAlignment( ITabItem tab )
			{
				MdiTab mdiTab = tab as MdiTab;

				if ( mdiTab == null )
					return base.GetCloseButtonAlignment( tab );

				return mdiTab.SettingsResolved.CloseButtonAlignment;
			}

			#endregion GetCloseButtonAlignment

			#region GetCloseButtonVisibility

			/// <summary>
			/// Returns a value indicating whether a tab item's close button is visible in the tab.
			/// </summary>
			/// <param name="tab">
			/// The tab for which the close button will be displayed or null if the close button is to be displayed 
			/// within the tab header area.
			/// </param>
			protected override TabCloseButtonVisibility GetCloseButtonVisibility( ITabItem tab )
			{
				MdiTab mdiTab = tab as MdiTab;

				if ( mdiTab == null )
					return base.GetCloseButtonVisibility( tab );

				return mdiTab.SettingsResolved.CloseButtonVisibility;
			}

			#endregion GetCloseButtonVisibility

            #endregion //Methods
        }
		#endregion //MdiTabGroupManager

		#region MdiTabDragManager
		/// <summary>
		/// Class used to manage a tab drag operation for an <see cref="MdiTabGroup"/>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>MdiTabDragManager</b> is used by the <see cref="MdiTabGroup"/> to 
		/// manage a tab drag operation. The drag manager is created by the <see cref="MdiTabGroupManager"/> via 
		/// the <see cref="MdiTabGroupManager.CreateDragManager"/> and handles the initiation and processing 
		/// of the tab repositioning.</p>
		/// <p class="body">The MdiTabDragManager behaves differently than the base TabManager. It mimics the 
		/// drag behavior displayed by Visual Studio when repositioning tabs in a tab group. When a tab is dragged 
		/// within its tab group, it is immediately repositioned within the <see cref="MdiTabGroup.Tabs"/> collection 
		/// but the corresponding move events are not invoked during the drag operation. If the drag operation is 
		/// cancelled, the tab is returned to its original position. When the drag operation is completed the 
		/// tab is repositioned if needed or the drag context menu is displayed. The drag behavior differs slightly in 
		/// that scrolling is supported when dragging near the inner edges of the tab group that contains the tab being 
		/// dragged.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettings.AllowDrag"/>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		protected class MdiTabDragManager : Infragistics.Win.UltraWinTabs.TabDragManager, IMessageFilter
		{
			#region Member Variables

			// owning mdi tab group
			private MdiTabGroup			owningTabGroup = null;

			// cursors
			private Cursor				dragCursor = null;
			private System.IO.Stream	dragCursorStream = null;
			private Cursor				noDragCursor = null;
			private System.IO.Stream	noDragCursorStream = null;

			// flags
			private bool				messageFilterAdded = false;
			private bool				isProcessingDrop = false;

			// drop point info
			private Point				lastScreenPt = Point.Empty;
			private MdiTabDropAction	dropAction = MdiTabDropAction.None;
			private MdiTabGroup			dropTabGroup = null;
			private int					dropTabIndex = 0;
			private DragScrollDirection	scrollDirection;

			// original mdi tab indexes
			private HybridDictionary	originalIndexes = null;

			// info about the tab being dragged
			private MdiTabGroup			originalSrcTabGroup = null;
			private int					originalTabIndex = 0;
			private MdiTab				originalFirstDisplayedTab = null;

			// previous drop rect displayed
			private Rectangle			previousDropRect = Rectangle.Empty;

			// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
			private TabGroupDropArea	dropNewGroupArea = TabGroupDropArea.Middle;

			#endregion //Member Variables

			#region Constructor
			/// <summary>
			/// Initializes a new <see cref="MdiTabDragManager"/>
			/// </summary>
			/// <param name="tabGroup">Owning <b>MdiTabGroup</b></param>
			/// <param name="tabManager">Associated <b>TabManager</b></param>
			public MdiTabDragManager(MdiTabGroup tabGroup, TabManager tabManager) : base(tabManager)
			{
				if (tabGroup == null)
					throw new ArgumentNullException(Shared.SR.GetString(null, "LE_V2_Exception_13"), Shared.SR.GetString(null, "LE_V2_Exception_14"));

				this.owningTabGroup = tabGroup;
			}
			#endregion //Constructor

			#region Properties

			#region MdiTabGroup
			/// <summary>
			/// Returns the owning <see cref="MdiTabGroup"/>
			/// </summary>
			public MdiTabGroup TabGroup
			{
				get { return this.owningTabGroup; }
			}
			#endregion // MdiTabGroup

			#region DragCursor
			private Cursor DragCursor
			{
				get
				{
					if (this.dragCursor == null)
					{
						Type type = typeof(MdiTabGroup);
						this.dragCursorStream = type.Assembly.GetManifestResourceStream(type, "CursorDragMono.cur");
						this.dragCursor = new Cursor(this.dragCursorStream);
					}

					return this.dragCursor;
				}
			}
			#endregion //DragCursor

			#region NoDragCursor
			private Cursor NoDragCursor
			{
				get
				{
					if (this.noDragCursor == null)
					{
						Type type = typeof(MdiTabGroup);
						this.noDragCursorStream = type.Assembly.GetManifestResourceStream(type, "CursorNoDragMono.cur");
						this.noDragCursor = new Cursor(this.noDragCursorStream);
					}

					return this.noDragCursor;
				}
			}
			#endregion //NoDragCursor

			#region IsDragging
			/// <summary>
			/// Indicates if there is a drag operation in progress
			/// </summary>
			public override bool IsDragging
			{
				get { return base.IsDragging || this.isProcessingDrop; }
			}
			#endregion //IsDragging

			#region AllowScroll
			/// <summary>
			/// Indicates whether the tabs should be allowed to scroll during a drag operation.
			/// </summary>
			protected virtual bool AllowScroll
			{
				get { return true; }
			}
			#endregion //AllowScroll

			#region Scroll Properties
			/// <summary>
			/// The minimum distance between the mouse and the edge of the tab row area.
			/// </summary>
			/// <remarks>The <b>ScrollDistanceMaximum</b> and <see cref="ScrollDistanceMinimum"/> are used to determine the range of distance
			/// between the mouse position and edge of the tab row area in which a scroll operation will occur.</remarks>
			protected virtual int ScrollDistanceMinimum
			{
				get { return -20; }
			}

			/// <summary>
			/// The maximum distance between the mouse and the edge of the tab row area.
			/// </summary>
			/// <remarks>The <b>ScrollDistanceMaximum</b> and <see cref="ScrollDistanceMinimum"/> are used to determine the range of distance
			/// between the mouse position and edge of the tab row area in which a scroll operation will occur.</remarks>
			protected virtual int ScrollDistanceMaximum
			{
				get { return 0; }
			}

			/// <summary>
			/// Returns the number of milliseconds to pause during a drag operation between scrolls.
			/// </summary>
			/// <param name="distance">Distance between the mouse pointer and the edge of the tab row area.</param>
			/// <returns>The number of milliseconds to pause during a drag operation.</returns>
			protected virtual int GetScrollInterval(int distance)
			{
				return 500;
			}
			#endregion //Scroll Properties

			#region TabDragStyle
			private MdiTabDragStyle TabDragStyle
			{
				get
				{
					MdiTab tab = this.Tab as MdiTab;

					if (tab == null || tab.Manager == null)
						return MdiTabDragStyle.None;

					MdiTabDragStyle dragStyle = tab.SettingsResolved.AllowDrag;

					// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
					// When maximized, we will not allow them to drag 
					// to other tab groups. Note, the context menu 
					// will still allow it though - if they set the
					// AllowDrag specifically to WithinAndAcrossGroups.
					//
					if (dragStyle == MdiTabDragStyle.WithinAndAcrossGroups &&
						tab.Manager.IsActiveTabGroupMaximized)
						return MdiTabDragStyle.WithinGroup;

					return dragStyle;
				}
			}
			#endregion //TabDragStyle

			#endregion //Properties

			#region Methods

			#region Base Class Overrides

			#region InitiateDrag
			/// <summary>
			/// Invoked during the <see cref="TabDragManager.StartDrag"/> when a drag should be initiated.
			/// </summary>
			/// <param name="tabOffset">Amount of offset between the mouse location when pressed down on the tab and the upper left of the tab element</param>
			/// <param name="tabToDrag">Tab item to drag</param>
			/// <param name="tabElement">Element associated with the tab. This element should not be stored and may be reused.</param>
			/// <returns>True if the drag operation was started</returns>
			protected override bool InitiateDrag( Point tabOffset, ITabItem tabToDrag, UIElement tabElement )
			{
				UltraTabbedMdiManager mdiManager = this.TabGroup.Manager;

				if (mdiManager == null)
					return false;

				// if there is only 1 visible tab in all the tab groups, there
				// is nothing to do
				if (this.TabGroup.Tabs.Count == 1 && mdiManager.TabGroups.Count == 1)
				{
					Debug.Assert(!mdiManager.HasNestedTabGroups, "There is a single root nested tab group!");
					return false;
				}

				// invoke the before event to see if this is allowed
				CancelableMdiTabEventArgs beforeArgs = new CancelableMdiTabEventArgs(tabToDrag as MdiTab);

				mdiManager.InvokeEvent(TabbedMdiEventIds.TabDragging, beforeArgs);

				// cancel the drag if the TabDragging is cancelled
				if (beforeArgs.Cancel)
					return false;

				// try to put in a message filter
				this.HookMessageFilter();

				// store the original tab info
				this.originalSrcTabGroup = ((MdiTab)tabToDrag).TabGroup;
				this.originalTabIndex = tabToDrag.Index;
				this.originalFirstDisplayedTab = this.TabGroup.FirstDisplayedTab;

				// initialize the last screen point with an unreal value so
				// we never skip the first point
				this.lastScreenPt = new Point(int.MinValue, int.MinValue);

				// AS 3/25/05
				mdiManager.OnDragTabStart();

				// otherwise let the drag operation begin
				return true;
			}
			#endregion //InitiateDrag

			#region ProcessMouseMove
			/// <summary>
			/// Invoked during a drag operation as the mouse is repositioned
			/// </summary>
			/// <param name="e">Mouse event arguments</param>
			public override void ProcessMouseMove(MouseEventArgs e)
			{
				// Cursors				Cause
				// ================		==============================
				// * Cursors.Arrow		Repositioning Tab in TabGroup
				//
				// * DragCursor 		Over Client Area
				//						Over SplitterBar
				//						Over Other TabGroup Tab Area
				//						Over Scroll Buttons in Current Group
				//
				// * NoDragCursor		Outside MdiClient Area
				//						Over Other Group and AllowDrag == WithinGroup
				//						Over Other Group and Group.AllowDrop == False
				//

				#region Verifications
				UltraTabbedMdiManager mdiManager = this.TabGroup.Manager;

				// we need an mdi manager to procedure
				if (mdiManager == null)
				{
					// cancel the drag operation and leave
					this.TabGroup.TabManager.CancelDrag();
					return;
				}
				#endregion //Verifications

				// coordinates relative to the mditabgroupcontrol that represents
				// this dragmanager' mditabgroup
				Point pt = new Point(e.X, e.Y);

				// convert the coordinates to screen coordinates
				Point screenPt = this.DragSourceControl.PointToScreen(pt);

				if (screenPt == this.lastScreenPt)
					return;

				this.scrollDirection = DragScrollDirection.None;

				// store the screen point - this will be used by the various helper
				// routines so be sure to set it before calling any
				this.lastScreenPt = screenPt;

				// get the mdi client's client rect in screen coordinates
				Rectangle mdiRect = mdiManager.MdiClientScreenRect;

				// make sure we're within the mdi client area since that is
				// where all of our controls are positioned
				if (!mdiRect.Contains(screenPt))
				{
					MdiTab dragTab = this.Tab as MdiTab;

					if (dragTab != null &&
						// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
						//dragTab.SettingsResolved.AllowDrag == MdiTabDragStyle.WithinAndAcrossGroups)
						this.TabDragStyle == MdiTabDragStyle.WithinAndAcrossGroups)
					{
						#region Create New Root Group
						// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
						// check if this should result in a root level group
						//
						const int EdgePadding = 24;

						// calculate how far the mouse is from every edge
						// AS 5/7/05
						// We want positive values indicating how far
						// we are from the edge since we know we are outside
						// the mdi client area.
						//
						//int offsetTop = screenPt.Y - mdiRect.Top;
						//int offsetBottom = mdiRect.Bottom - screenPt.Y;
						//int offsetLeft = screenPt.X - mdiRect.Left;
						//int offsetRight = mdiRect.Right - screenPt.X;
						int offsetTop = mdiRect.Top - screenPt.Y;
						int offsetBottom = screenPt.Y - mdiRect.Bottom;
						int offsetLeft = mdiRect.Left - screenPt.X;
						int offsetRight = screenPt.X - mdiRect.Right;

						// AS 5/7/05
						// We don't care about negative values - e.g.
						// if you are between the upper and bottom edge
						// of the mdi client area. We are only concerned
						// with which edge you are outside of.
						//
						if (offsetTop < 0)
							offsetTop = int.MaxValue;
						if (offsetBottom < 0)
							offsetBottom = int.MaxValue;
						if (offsetLeft < 0)
							offsetLeft = int.MaxValue;
						if (offsetRight < 0)
							offsetRight = int.MaxValue;

						// assume it won't be creating a new group
						TabGroupDropArea dropArea = TabGroupDropArea.Middle;
				
						// now figure out the drop area
						if (mdiManager.CanCreateRootVerticalGroup && Math.Min(offsetLeft, offsetRight) < EdgePadding)
							dropArea = offsetLeft < offsetRight ? TabGroupDropArea.LeftEdge : TabGroupDropArea.RightEdge;

						if (mdiManager.CanCreateRootHorizontalGroup && Math.Min(offsetTop, offsetBottom) < EdgePadding)
						{
							// if we either decided not to drop it for a vertical group or were closer
							// to an edge that would create a horizontal group...
							if (dropArea == TabGroupDropArea.Middle ||
								Math.Min(offsetTop, offsetBottom) < Math.Min(offsetLeft, offsetRight))
							{
								dropArea = offsetTop < offsetBottom ? TabGroupDropArea.TopEdge : TabGroupDropArea.BottomEdge;
							}
						}

						if (dropArea != TabGroupDropArea.Middle)
						{
							// from the drop location, determine the action 
							// that will take place
							MdiTabDropAction dropAction = (dropArea == TabGroupDropArea.LeftEdge || dropArea == TabGroupDropArea.RightEdge) ? 
								MdiTabDropAction.NewVerticalGroup : MdiTabDropAction.NewHorizontalGroup;

							if (this.InvokeDragOver( dropAction, screenPt, null, null, dropArea))
							{
								Rectangle dropRect = mdiRect;

								// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
								// Only offset, if this will be added after this group.
								//
								if (dropAction == MdiTabDropAction.NewVerticalGroup)
								{
									dropRect.Width = mdiRect.Width / (mdiManager.CalculateMaxTabGroupCols() + 1);

									if (dropArea == TabGroupDropArea.RightEdge)
										dropRect.X = mdiRect.Right - dropRect.Width;
								}
								else
								{
									dropRect.Height = mdiRect.Height / (mdiManager.CalculateMaxTabGroupRows() + 1);

									if (dropArea == TabGroupDropArea.BottomEdge)
										dropRect.Y = mdiRect.Bottom - dropRect.Height;
								}

								this.ShowDropRect(dropRect);
								return;
							}
						}
						#endregion //Create New Root Group
					}

					this.ProcessInvalidDropLocation();
					return;
				}

				// we're within the mdi client area
				//

				// get the tab group that contains the specified point
				MdiTabGroup containingTabGroup = mdiManager.TabGroupFromPoint(screenPt);

				Debug.Assert(containingTabGroup != null, "The mouse is within the mdi client area but there is no associated containing tab group.");

				if (containingTabGroup == null)
				{
					// if we're in the mdiclient area but for some reason, can't get
					// to a tabgroup that contains that point, consider it
					// an invalid drop point. this really shouldn't happen
					this.ProcessInvalidDropLocation();
					return;
				}

				// find the tab group area (the area of an mdi tab group)
				// that contains the screen coordinates
				TabGroupArea groupArea = this.GetTabGroupArea(containingTabGroup, screenPt);

				switch(groupArea)
				{
					case TabGroupArea.None:
					{
						Debug.Assert(false, "The screen coordinates were part of an MdiTabGroup but none of the areas of the tab group contained the specified screen point.");

						// shouldn't happen if we're in a tab group's area
						this.ProcessContextMenuDrop(containingTabGroup);
						break;
					}
					case TabGroupArea.TabGroupControl:
					{
						MdiTab dragTab = this.Tab as MdiTab;

						if (containingTabGroup != this.TabGroup && // dragged over a different tab group
							// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
							//(dragTab.SettingsResolved.AllowDrag != MdiTabDragStyle.WithinAndAcrossGroups	||
							(this.TabDragStyle != MdiTabDragStyle.WithinAndAcrossGroups	||
							!containingTabGroup.SettingsResolved.AllowDrop) )
						{
							// if the tab is being dragged over a different tab group
							// AND either it does not allow being dropped on a different
							// tab group OR the tab group it is now over does not allow
							// external tabs to be dropped on it, then process this as a
							// context menu drop
							//
							this.ProcessContextMenuDrop(containingTabGroup);
							break;
						}
						else
						{
							// get the screen point in coordinates relative to the
							// mdi tab group control
							Point tabGroupPt = containingTabGroup == this.TabGroup ? pt : mdiManager.PointToClient(containingTabGroup, screenPt);

							// hit test the containing group
							TabHitTestInfo hitInfo = containingTabGroup.TabManager.HitTest(tabGroupPt);
				
							// process that hit test info
							this.ProcessTabRowAreaDrop(containingTabGroup, hitInfo, tabGroupPt);
						}
						break;
					}
					default:
					case TabGroupArea.Form:
					case TabGroupArea.Splitter:
					{
						// potentially show a drop rectangle or show a context menu
						this.ProcessTabGroupClientArea(containingTabGroup);
						break;
					}
				}
			}
			#endregion //ProcessMouseMove

			#region DragEnded
			/// <summary>
			/// Invoked when the drag has ended so resources can be released.
			/// </summary>
			protected override void DragEnded()
			{
				// let the base clean up first
				base.DragEnded();

				// clean up any screen rects
				this.ShowDropRect(Rectangle.Empty);

				// release the cursors we created
				if (this.dragCursor != null)
				{
					this.dragCursor.Dispose();
					this.dragCursor = null;
				}

				if (this.dragCursorStream != null)
				{
					this.dragCursorStream.Close();
					this.dragCursorStream = null;
				}

				if (this.noDragCursor != null)
				{
					this.noDragCursor.Dispose();
					this.noDragCursor = null;
				}

				if (this.noDragCursorStream != null)
				{
					this.noDragCursorStream.Close();
					this.noDragCursorStream = null;
				}

				// reset the cached info
				this.originalIndexes = null;
				this.originalSrcTabGroup = null;
				this.originalTabIndex = 0;
				this.originalFirstDisplayedTab = null;
				this.dropAction = MdiTabDropAction.None;
				this.dropTabGroup = null;

				// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
				this.dropNewGroupArea = TabGroupDropArea.Middle;

				// unhook our message filter
				this.UnhookMessageFilter();

				// AS 3/25/05
				if (this.TabGroup.Manager != null)
					this.TabGroup.Manager.OnDragTabEnd();
			}
			#endregion //DragEnded

			#region ProcessDrop
			/// <summary>
			/// Invoked during a drag operation when the mouse button has been released.
			/// </summary>
			public override void ProcessDrop()
			{
				this.isProcessingDrop = true;

				try
				{
					MdiTab tab = this.Tab as MdiTab;

					// store the values we will need
					MdiTabDropAction	dropAction				= this.dropAction;
					MdiTabGroup			dropTabGroup			= this.dropTabGroup;
					int					dropTabIndex			= this.dropTabIndex;
					int					newTabIndex				= this.dropTabIndex;
					Point				screenPt				= this.lastScreenPt;
					int					firstDisplayedTabIndex	= ((ITabItem)this.originalSrcTabGroup.FirstDisplayedTab).Index;
					MdiTabGroup			originalTabGroup		= this.originalSrcTabGroup;
					bool				wasFirstDisplayedTab	= this.originalSrcTabGroup.FirstDisplayedTab == this.Tab;
					int					originalTabIndex		= this.originalTabIndex;

					// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
					TabGroupDropArea	dropNewGroupArea		= this.dropNewGroupArea;

					// get the current index at which the item is positioned
					int currentIndex = this.Tab.Index;

					// first restore the tab position since we want the user to see
					// the tab as being at its original position in the TabMoving event
					//
					this.RestoreTabPosition();

					int				curFirstDisplayedTabIndex	= ((ITabItem)originalTabGroup.FirstDisplayedTab).Index;
					bool			resetFirstDisplayedTab		= curFirstDisplayedTabIndex != firstDisplayedTabIndex;

					UltraTabbedMdiManager mdiManager = this.TabGroup.Manager;

					// clean up after the drag
					this.DragEnded();

					if (mdiManager != null)
					{
						// then perform the action required
						switch(dropAction)
						{
							case MdiTabDropAction.ContextMenu:
								#region ContextMenu
								// if the tab was moved before going out to a context menu area...
								if ( ((ITabItem)tab).Index != currentIndex )
								{
									// reposition in a tab group
									if (mdiManager.MoveToGroup(tab, originalTabGroup, currentIndex))
									{
										// AS 4/24/03
										// Maintain the first displayed tab index if the 
										// move was successful.
										//
										// reset the first displayed tab if the move was allowed
										//if (mdiManager.TabGroups.Contains(originalTabGroup))
										//	originalTabGroup.FirstDisplayedTab = originalTabGroup.Tabs[ Math.Min(originalTabGroup.Tabs.Count - 1, firstDisplayedTabIndex)];
									}

									if ((wasFirstDisplayedTab || resetFirstDisplayedTab)		&&	// was the first displayed tab
										// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
										//mdiManager.TabGroups.Contains(originalTabGroup)			&&	// the original group still exists
										originalTabGroup.ParentCollection != null				&&	// the original group still exists
										originalTabGroup.Tabs.Count > firstDisplayedTabIndex	&&	// there are more tabs then the original first displayed tab index
										originalTabGroup.Tabs.Contains(tab)						&&	// the original source group still contains the tab
										originalTabGroup.FirstDisplayedTab != null				&&	// there is a first displayed tab
										((ITabItem)originalTabGroup.FirstDisplayedTab).Index 
										!= firstDisplayedTabIndex)								// the index has not changed
									{
										originalTabGroup.FirstDisplayedTab = originalTabGroup.Tabs[firstDisplayedTabIndex];
									}

									// AS 4/24/03 WTB843
									if (tab.CanActivate)
										tab.Activate();
								}

								// then show a context menu
								mdiManager.ShowDragDropContextMenu(tab, screenPt);
								#endregion //ContextMenu
								break;

							case MdiTabDropAction.NewHorizontalGroup:
							case MdiTabDropAction.NewVerticalGroup:
								#region New(Horizontal|Vertical)Group

								
								if (true)
								{
									//if (mdiManager.MoveToNewGroup(tab, dropTabGroup, RelativePosition.After) != null)
									//{
									//	// reset the first displayed tab if the move was allowed
									//	if (mdiManager.TabGroups.Contains(originalTabGroup))
									//		originalTabGroup.FirstDisplayedTab = originalTabGroup.Tabs[ Math.Min(originalTabGroup.Tabs.Count - 1, firstDisplayedTabIndex)];
									//}
									// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
									//mdiManager.MoveToNewGroup(tab, dropTabGroup, RelativePosition.After);
									Orientation orientation;
									RelativePosition relativePosition;

									switch(dropNewGroupArea)
									{
										case TabGroupDropArea.BottomEdge:
											orientation = Orientation.Horizontal;
											relativePosition = RelativePosition.After;
											break;
										case TabGroupDropArea.TopEdge:
											orientation = Orientation.Horizontal;
											relativePosition = RelativePosition.Before;
											break;
										case TabGroupDropArea.LeftEdge:
											orientation = Orientation.Vertical;
											relativePosition = RelativePosition.Before;
											break;
										case TabGroupDropArea.RightEdge:
											orientation = Orientation.Vertical;
											relativePosition = RelativePosition.After;
											break;
										default:
										case TabGroupDropArea.Middle:
											Debug.Assert(false, "Unexpected drop location!");
											orientation = dropTabGroup.ParentOrientation;
											relativePosition = RelativePosition.After;
											break;
									}

									mdiManager.MoveToNewGroup(tab, dropTabGroup, relativePosition, orientation);

									// AS 4/24/03
									if ((wasFirstDisplayedTab || resetFirstDisplayedTab)		&&	// was the first displayed tab
										// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
										//mdiManager.TabGroups.Contains(originalTabGroup)			&&	// the original group still exists
										originalTabGroup.ParentCollection != null				&&	// the original group still exists
										originalTabGroup.Tabs.Count > firstDisplayedTabIndex	&&	// there are more tabs then the original first displayed tab index
										originalTabGroup.Tabs.Contains(tab)						&&	// the original source group still contains the tab
										originalTabGroup.FirstDisplayedTab != null				&&	// there is a first displayed tab
										((ITabItem)originalTabGroup.FirstDisplayedTab).Index 
										!= firstDisplayedTabIndex)								// the index has not changed
									{
										originalTabGroup.FirstDisplayedTab = originalTabGroup.Tabs[firstDisplayedTabIndex];
									}

									// AS 4/22/03 WTB843
									if (tab.CanActivate)
										tab.Activate();
								}
								#endregion //New(Horizontal|Vertical)Group
								break;

							case MdiTabDropAction.None:
								#region None
								// if the tab was repositioned within the tab group...
								if (originalTabIndex != currentIndex)
								{
									// reposition in a tab group
									mdiManager.MoveToGroup(tab, originalTabGroup, currentIndex);

									// AS 4/24/03
									if ((wasFirstDisplayedTab || resetFirstDisplayedTab)		&&	// was the first displayed tab
										// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
										//mdiManager.TabGroups.Contains(originalTabGroup)			&&	// the original group still exists
										originalTabGroup.ParentCollection != null				&&	// the original group still exists
										originalTabGroup.Tabs.Count > firstDisplayedTabIndex	&&	// there are more tabs then the original first displayed tab index
										originalTabGroup.Tabs.Contains(tab)						&&	// the original source group still contains the tab
										originalTabGroup.FirstDisplayedTab != null				&&	// there is a first displayed tab
										((ITabItem)originalTabGroup.FirstDisplayedTab).Index 
										!= firstDisplayedTabIndex)								// the index has not changed
									{
										originalTabGroup.FirstDisplayedTab = originalTabGroup.Tabs[firstDisplayedTabIndex];
									}

									// AS 4/22/03 WTB843
									if (tab.CanActivate)
										tab.Activate();
								}
								#endregion //None
								break;

							case MdiTabDropAction.TabGroup:
								#region TabGroup
								// AS 4/24/03
								// Just make sure its active if it hasn't been
								// repositioned.
								//
								if (newTabIndex == originalTabIndex &&
									dropTabGroup == originalTabGroup)
								{
									// no operation
								}
								else
									// reposition in a tab group
									if (mdiManager.MoveToGroup(tab, dropTabGroup, dropTabIndex))
								{
									// reset the first displayed tab if the move was allowed
									//if (mdiManager.TabGroups.Contains(originalTabGroup))
									//	originalTabGroup.FirstDisplayedTab = originalTabGroup.Tabs[ Math.Min(originalTabGroup.Tabs.Count - 1, firstDisplayedTabIndex)];
								}

								// AS 4/24/03
								if ((wasFirstDisplayedTab || resetFirstDisplayedTab)		&&	// was the first displayed tab
									// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
									//mdiManager.TabGroups.Contains(originalTabGroup)			&&	// the original group still exists
									originalTabGroup.ParentCollection != null				&&	// the original group still exists
									originalTabGroup.Tabs.Count > firstDisplayedTabIndex	&&	// there are more tabs then the original first displayed tab index
									originalTabGroup.Tabs.Contains(tab)						&&	// the original source group still contains the tab
									originalTabGroup.FirstDisplayedTab != null				&&	// there is a first displayed tab
									((ITabItem)originalTabGroup.FirstDisplayedTab).Index 
									!= firstDisplayedTabIndex)								// the index has not changed
								{
									originalTabGroup.FirstDisplayedTab = originalTabGroup.Tabs[firstDisplayedTabIndex];
								}

								// AS 4/22/03 WTB843
								if (tab.CanActivate)
									tab.Activate();

								#endregion //TabGroup
								break;
						}
					}

					// fire the TabDragged event
					if (mdiManager != null)
					{
						MdiTabDroppedEventArgs draggedArgs = new MdiTabDroppedEventArgs(tab);

						mdiManager.InvokeEvent( TabbedMdiEventIds.TabDropped, draggedArgs );
					}
				}
				finally
				{
					this.isProcessingDrop = false;
				}
			}
			#endregion //ProcessDrop

			#region CancelDrag
			/// <summary>
			/// Invoked if the drag operation was cancelled.
			/// </summary>
			public override void CancelDrag()
			{
				// we keep the IsDragging flag set while processing the drop
				// so make sure we don't do anything while processing the drop
				if (this.isProcessingDrop)
					return;

				if (base.IsDragging)
				{
					// restore the position of the tab were dragging
					this.RestoreTabPosition();
				}

				base.CancelDrag();
			}
			#endregion //CancelDrag

			#region OnScrollTimerTick

            // MBS 12/14/06 - FxCop
            // Removed parameters due to security concerns
			/// <summary>
			/// Invoked when the interval has elapsed for the scroll timer
			/// </summary>
            ///// <param name="sender">Timer invoking the tick</param>
            ///// <param name="e">Event arguments</param>
            //protected override void OnScrollTimerTick(object sender, EventArgs e)
            protected override void OnScrollTimerTick()
			{
				if (base.IsDragging && 
					this.dropAction == MdiTabDropAction.TabGroup &&
					this.scrollDirection != DragScrollDirection.None)
				{
                    Infragistics.Win.UltraWinTabs.ScrollType scrollType = 
                        this.scrollDirection == DragScrollDirection.Next ?
                        Infragistics.Win.UltraWinTabs.ScrollType.Next :
                        Infragistics.Win.UltraWinTabs.ScrollType.Previous;

                    this.dropTabGroup.TabManager.ScrollTabs(scrollType, 1);
				}
			}
			#endregion //OnScrollTimerTick

			#endregion //Base Class Overrides

			#region Moving related

			#region ProcessTabGroupClientArea
			private void ProcessTabGroupClientArea(MdiTabGroup destTabGroup)
			{
				UltraTabbedMdiManager mdiManager = this.TabGroup.Manager;

				// if we can't create new groups, then there are no valid drop
				// areas to test
				//
				// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
				// Moving the tab is ok when cancreatenewgroup is false if
				// the tabgroup will be removed when the tab is moved.
				//
				//if (mdiManager == null || !mdiManager.CanCreateNewGroup)
				if (mdiManager == null || (!mdiManager.CanCreateNewGroup && this.TabGroup.Tabs.Count > 1) )
				{
					// treat it like a context menu drop
					this.ProcessContextMenuDrop(destTabGroup);
					return;
				}

				// setup
				
				bool canCreateVerticalGroup = mdiManager.CanCreateVerticalGroup;
				bool canCreateHorizontalGroup = mdiManager.CanCreateHorizontalGroup;

				// get the tab group drop area (comes back in mdiparent coordinates)
				Rectangle tabGroupClientArea = mdiManager.TabGroupManager.GetTabGroupDropRect(destTabGroup);

				// get the mouse point in mdi parent coordinates
				Point mdiParentPt = mdiManager.MdiParent.PointToClient(this.lastScreenPt);

				if (destTabGroup == this.originalSrcTabGroup && destTabGroup.Tabs.Count == 1)
				{
					canCreateVerticalGroup = false;
					canCreateHorizontalGroup = false;
				}
				else
				{
					// AS 4/28/03
					// If the tab being dragged only supports being dragged in its
					// current group then we shouldn't allow it to
					// create a new group.
					//
					MdiTab dragTab = this.Tab as MdiTab;

					if (dragTab != null &&
						// AS 3/25/05 NA 2005 Vol 2 - Maximized TabGroup
						//dragTab.SettingsResolved.AllowDrag != MdiTabDragStyle.WithinAndAcrossGroups)
						this.TabDragStyle != MdiTabDragStyle.WithinAndAcrossGroups)
					{
						canCreateVerticalGroup = false;
						canCreateHorizontalGroup = false;
					}
				}

				

				// calculate how far the mouse is from every edge
				int offsetTop = mdiParentPt.Y - tabGroupClientArea.Top;
				int offsetBottom = tabGroupClientArea.Bottom - mdiParentPt.Y;
				int offsetLeft = mdiParentPt.X - tabGroupClientArea.Left;
				int offsetRight = tabGroupClientArea.Right - mdiParentPt.X;

				const int EdgePadding = 24;

				// assume it won't be creating a new group
				TabGroupDropArea dropArea = TabGroupDropArea.Middle;
				
				// now figure out the drop area
				if (canCreateVerticalGroup && Math.Min(offsetLeft, offsetRight) < EdgePadding)
					dropArea = offsetLeft < offsetRight ? TabGroupDropArea.LeftEdge : TabGroupDropArea.RightEdge;

				if (canCreateHorizontalGroup && Math.Min(offsetTop, offsetBottom) < EdgePadding)
				{
					// if we either decided not to drop it for a vertical group or were closer
					// to an edge that would create a horizontal group...
					if (dropArea == TabGroupDropArea.Middle ||
						Math.Min(offsetTop, offsetBottom) < Math.Min(offsetLeft, offsetRight))
					{
						dropArea = offsetTop < offsetBottom ? TabGroupDropArea.TopEdge : TabGroupDropArea.BottomEdge;
					}
				}

				// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
				//if (canCreateVerticalGroup && tabGroupClientArea.Right - 24 < mdiParentPt.X)
				if (dropArea == TabGroupDropArea.LeftEdge || dropArea == TabGroupDropArea.RightEdge)
				{
					// see if this is allowed
					// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
					//if (!this.InvokeDragOver(MdiTabDropAction.NewVerticalGroup, this.lastScreenPt, destTabGroup, null))
					if (!this.InvokeDragOver(MdiTabDropAction.NewVerticalGroup, this.lastScreenPt, destTabGroup, null, dropArea))
					{
						// clear any screen rects
						this.ShowDropRect(Rectangle.Empty);
					}
					else
					{
						// show a split group rect
						//

						// get the entire tab group area rect in mdiparent coordinates
						// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
						//Rectangle tabGroupRect = mdiManager.TabGroupManager.GetTabGroupArea(destTabGroup);
						Rectangle tabGroupRect;

						if (destTabGroup.ParentOrientation == Orientation.Vertical)
							tabGroupRect = mdiManager.TabGroupManager.GetTabGroupArea(destTabGroup);
						else
						{
							tabGroupRect = mdiManager.TabGroupManager.GetTabGroupFormRect(destTabGroup);

							Rectangle groupCtrlRect = mdiManager.TabGroupManager.GetTabGroupControlRectangle(destTabGroup);

							if (!groupCtrlRect.IsEmpty)
								tabGroupRect = Rectangle.Union(tabGroupRect, groupCtrlRect);
						}

						// convert that to screen coordinates
						Rectangle screenRect = mdiManager.MdiParent.RectangleToScreen(tabGroupRect);

						// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
						// Only offset, if this will be added after this group.
						//
						if (dropArea == TabGroupDropArea.RightEdge)
							screenRect.X += screenRect.Width / 2;

						screenRect.Width -= screenRect.Width / 2;

						this.ShowDropRect(screenRect);
					}
				}
				// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
				//else if (canCreateHorizontalGroup && tabGroupClientArea.Bottom - 24 < mdiParentPt.Y)
				else if (dropArea == TabGroupDropArea.TopEdge || dropArea == TabGroupDropArea.BottomEdge)
				{
					// see if this is allowed
					// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
					//if (!this.InvokeDragOver(MdiTabDropAction.NewHorizontalGroup, this.lastScreenPt, destTabGroup, null))
					if (!this.InvokeDragOver(MdiTabDropAction.NewHorizontalGroup, this.lastScreenPt, destTabGroup, null, dropArea))
					{
						// clear any screen rects
						this.ShowDropRect(Rectangle.Empty);
					}
					else
					{
						// show a split group rect
						//
						// get the entire tab group area rect in mdiparent coordinates
						// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
						//Rectangle tabGroupRect = mdiManager.TabGroupManager.GetTabGroupArea(destTabGroup);
						Rectangle tabGroupRect;

						if (destTabGroup.ParentOrientation == Orientation.Horizontal)
							tabGroupRect = mdiManager.TabGroupManager.GetTabGroupArea(destTabGroup);
						else
						{
							tabGroupRect = mdiManager.TabGroupManager.GetTabGroupFormRect(destTabGroup);

							Rectangle groupCtrlRect = mdiManager.TabGroupManager.GetTabGroupControlRectangle(destTabGroup);

							if (!groupCtrlRect.IsEmpty)
								tabGroupRect = Rectangle.Union(tabGroupRect, groupCtrlRect);
						}

						// convert that to screen coordinates
						Rectangle screenRect = mdiManager.MdiParent.RectangleToScreen(tabGroupRect);

						// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
						// Only offset, if this will be added after this group.
						//
						if (dropArea == TabGroupDropArea.BottomEdge)
							screenRect.Y += screenRect.Height / 2;

						screenRect.Height -= screenRect.Height / 2;

						this.ShowDropRect(screenRect);
					}
				}
				else
				{
					// treat it like a context menu drop
					this.ProcessContextMenuDrop(destTabGroup);
				}
			}
			#endregion //ProcessTabGroupClientArea

			#region ProcessInvalidDropLocation
			private void ProcessInvalidDropLocation()
			{
				// clean up any screen rects
				this.ShowDropRect(Rectangle.Empty);

				// invoke the drag over event?
				this.InvokeDragOver(MdiTabDropAction.None, this.lastScreenPt, null, null);
			}
			#endregion //ProcessInvalidDropLocation

			#region ProcessContextMenuDrop
			private void ProcessContextMenuDrop(MdiTabGroup containingTabGroup)
			{
				// clean up any screen rects
				this.ShowDropRect(Rectangle.Empty);

				// invoke the drag over event
				this.InvokeDragOver(MdiTabDropAction.ContextMenu, this.lastScreenPt, containingTabGroup, null);
			}
			#endregion //ProcessContextMenuDrop

			#region ProcessTabRowAreaDrop
			private void ProcessTabRowAreaDrop(MdiTabGroup destTabGroup, TabHitTestInfo hitInfo, Point tabGroupPt)
			{
				
				// either clone the tab for use in other tabgroups and then manage that
				// or make sure that tab groups are not removed during a drag when there are
				// no tabs left in them. probably better to use a clone tab and resolve
				// the appearance of the original differently?
				// Another option is to use the drop indicators on other tab groups
				//

				UltraTabbedMdiManager mdiManager = this.TabGroup.Manager;

				// if we're dragging over a different tabgroup then we should just
				// show the drop rect like visual studio does
				if (destTabGroup != this.TabGroup)
				{
					if (!this.InvokeDragOver(MdiTabDropAction.TabGroup, this.lastScreenPt, destTabGroup, null))
					{
						this.ShowDropRect(Rectangle.Empty);
					}
					else
					{
						// get the tab group drop area (comes back in mdiparent coordinates)
						Rectangle tabGroupClientArea = mdiManager.TabGroupManager.GetTabGroupDropRect(destTabGroup);

						Rectangle screenRect = mdiManager.MdiParent.RectangleToScreen(tabGroupClientArea);

						this.ShowDropRect(screenRect);

						// it will be positioned at the end of the group
						this.dropTabIndex = destTabGroup.Tabs.Count;
					}

					return;
				}

				// we must be dragging over the tab group control that originally contained
				// the tab being dragged
				//

				// cache the original tab indexes
				this.CacheOriginalIndexes(destTabGroup);

 				// clean up any screen rects
				this.ShowDropRect(Rectangle.Empty);

				if (hitInfo.Tab == null)
				{
					// if we're not over any tabs, there is no action to take - well
					// at least that's what visual studio says
					this.ProcessContextMenuDrop(destTabGroup);
					return;
				}

				// get the source and destination info
				MdiTab destTab = hitInfo.Tab as MdiTab;
				MdiTab srcTab = this.Tab as MdiTab;
				MdiTabGroup srcTabGroup = this.originalSrcTabGroup;

				// get their current indexes
				int srcCurIndex = ((ITabItem)srcTab).Index;
				int destCurIndex = ((ITabItem)destTab).Index;

				// get their original indexes for the destination tab group
				int srcOrigIndex = this.GetOriginalTabIndex(srcTab, destTabGroup);
				int destOrigIndex = this.GetOriginalTabIndex(destTab, destTabGroup);

				// store the index of the first displayed tab
				int firstTabIndex = ((ITabItem)destTabGroup.FirstDisplayedTab).Index;

				Debug.WriteLine( string.Format("TabRowAreaDrop: Src={0}, Dest={1}, SrcOrig={2}, DestOrig{3}, First={4}", srcCurIndex, destCurIndex, srcOrigIndex, destOrigIndex, firstTabIndex) );

				// need a variable to hold which tab index we will place the tab at.
				// this value will need to be the index that the tab should be added
				// assuming that it has been removed from the collection.
				//
				int newTabIndex = -1;

				if (srcCurIndex == srcOrigIndex)
				{
					Debug.WriteLine("Source Tab is at its original location");

					#region Source Tab At Original Location

					if (destTab == srcTab)
					{
						// if the drag tab has not been repositioned before and
						// we're over that tab, do nothing
					}
					else if (srcCurIndex < destCurIndex)
					{
						// if the drag tab has not been repositioned before and
						// we're over the tab on the right, shift it right
						//destTabGroup.Tabs.RemoveAt(srcCurIndex);
						//destTabGroup.Tabs.Insert(destCurIndex, srcTab);

						newTabIndex = destCurIndex;
					}
					else if (srcCurIndex > destCurIndex)
					{
						// if the drag tab has not been repositioned before and
						// we're over that tab, do nothing
						//destTabGroup.Tabs.RemoveAt(srcCurIndex);
						//destTabGroup.Tabs.Insert(destCurIndex, srcTab);

						newTabIndex = destCurIndex;
					}

					#endregion //Source Tab At Original Location
				}
				else if (destTab == srcTab)
				{
					Debug.WriteLine("Mouse Over Source Tab That Has Been Repositioned");

					#region Mouse Over Src Tab That Was Repositioned

					// get the element's rect - this is the source tab's rect
					Rectangle elementRect = hitInfo.Element.Rect;

					if (srcCurIndex < srcOrigIndex)
					{
						// if the tab was repositioned left, see if it needs to
						// be shifted back to the right

						MdiTab nextTab = destTabGroup.TabManager.GetNextVisibleTabItem(srcTab, false) as MdiTab;

						if (nextTab != null)
						{
							// get the size of the next tab
							Size nextTabSize = destTabGroup.TabManager.GetTabItemSize(nextTab);

							int interTabSpacing = destTabGroup.TabManager.InterTabSpacingResolved;

							// shift the rect to the right
							switch(destTabGroup.SettingsResolved.TabOrientation)
							{
								case TabOrientation.RightTop:
								case TabOrientation.LeftTop:
									if (interTabSpacing > 0)
										elementRect.Y += nextTabSize.Height + interTabSpacing;
									else
										elementRect.Y += nextTabSize.Height;
									break;
								case TabOrientation.LeftBottom:
								case TabOrientation.RightBottom:
									if (interTabSpacing > 0)
										elementRect.Y -= nextTabSize.Height + interTabSpacing;
									else
										elementRect.Y -= nextTabSize.Height;
									break;
								case TabOrientation.BottomRight:
								case TabOrientation.TopRight:
									if (interTabSpacing > 0)
										elementRect.X -= nextTabSize.Width + interTabSpacing;
									else
										elementRect.X -= nextTabSize.Width;
									break;
								default:
								case TabOrientation.BottomLeft:
								case TabOrientation.TopLeft:
									if (interTabSpacing > 0)
										elementRect.X += nextTabSize.Width + interTabSpacing;
									else
										elementRect.X += nextTabSize.Width;
									break;
							}

							if (elementRect.Contains(tabGroupPt))
							{
								//destTabGroup.Tabs.RemoveAt(srcCurIndex);
								//destTabGroup.Tabs.Insert(srcCurIndex + 1, srcTab);
								//Debug.WriteLine("Shift the Tab to the Right");

								newTabIndex = srcCurIndex + 1;
							}
						}
					}
					else
					{
						// if the tab was repositioned right, see if it needs to
						// be shifted back to the left

						MdiTab previousTab = destTabGroup.TabManager.GetPreviousVisibleTabItem(srcTab, false) as MdiTab;

						if (previousTab != null)
						{
							// get its size
							Size previousTabSize = destTabGroup.TabManager.GetTabItemSize(previousTab);

							int interTabSpacing = destTabGroup.TabManager.InterTabSpacingResolved;

							// shift the rect to the left
							switch(destTabGroup.SettingsResolved.TabOrientation)
							{
								case TabOrientation.RightBottom:
								case TabOrientation.LeftBottom:
									if (interTabSpacing > 0)
										elementRect.Y += previousTabSize.Height + interTabSpacing;
									else
										elementRect.Y += previousTabSize.Height;
									break;
								case TabOrientation.LeftTop:
								case TabOrientation.RightTop:
									if (interTabSpacing > 0)
										elementRect.Y -= previousTabSize.Height + interTabSpacing;
									else
										elementRect.Y -= previousTabSize.Height;
									break;
								default:
								case TabOrientation.TopLeft:
								case TabOrientation.BottomLeft:
									if (interTabSpacing > 0)
										elementRect.X -= previousTabSize.Width + interTabSpacing;
									else
										elementRect.X -= previousTabSize.Width;
									break;
								case TabOrientation.BottomRight:
								case TabOrientation.TopRight:
									if (interTabSpacing > 0)
										elementRect.X += previousTabSize.Width + interTabSpacing;
									else
										elementRect.X += previousTabSize.Width;
									break;
							}

							if (elementRect.Contains(tabGroupPt))
							{
								//destTabGroup.Tabs.RemoveAt(srcCurIndex);
								//destTabGroup.Tabs.Insert(srcCurIndex - 1, srcTab);

								//Debug.WriteLine("Shift the Tab to the Left");

								newTabIndex = srcCurIndex - 1;
							}
						}
					}

					#endregion //Mouse Over Src Tab That Was Repositioned
				}
				else if ( (destCurIndex > srcCurIndex && destOrigIndex < srcOrigIndex) ||
					(destCurIndex > srcCurIndex && destOrigIndex > srcOrigIndex) )
				{
					// Destination is Right of Source but was Left Of Source
					// OR
					// Destination is Right of Source AND was Right Of Source
					Debug.WriteLine("Destination is After Source");

					// the mouse is over a tab that was on the left originally but
					// is now to the right - see if we need to shift it right
					#region Dest was Left, Now On Right of Src

					Rectangle elementRect = hitInfo.Element.Rect;

					// get its size
					Size srcTabSize = destTabGroup.TabManager.GetTabItemSize(srcTab);

					int interTabSpacing = destTabGroup.TabManager.InterTabSpacingResolved;

					Rectangle testSrcRect;
					
					switch(destTabGroup.SettingsResolved.TabOrientation)
					{
						case TabOrientation.LeftTop:
						case TabOrientation.RightTop:
							testSrcRect = new Rectangle(elementRect.X,
								elementRect.Bottom - srcTabSize.Height, elementRect.Width, srcTabSize.Height);
							break;
						case TabOrientation.RightBottom:
						case TabOrientation.LeftBottom:
							testSrcRect = new Rectangle(elementRect.X,
								elementRect.Y, elementRect.Width, srcTabSize.Height);
							break;
						default:
						case TabOrientation.TopLeft:
						case TabOrientation.BottomLeft:
							testSrcRect = new Rectangle(elementRect.Right - srcTabSize.Width,
								elementRect.Y, srcTabSize.Width, elementRect.Height);
							break;
						case TabOrientation.BottomRight:
						case TabOrientation.TopRight:
							testSrcRect = new Rectangle(elementRect.X,
								elementRect.Y, srcTabSize.Width, elementRect.Height);
							break;
					}

					

					// Destination is Right of Source AND was Right Of Source
					if (testSrcRect.Contains(tabGroupPt))
					{
						//destTabGroup.Tabs.RemoveAt(srcCurIndex);
						//destTabGroup.Tabs.Insert(destCurIndex, srcTab);
						//Debug.WriteLine("Move the tab to the right of the destination tab");

						newTabIndex = destCurIndex;
					}
					else if (srcCurIndex != destCurIndex - 1)
					{
						// make sure its left of the destination
						//destTabGroup.Tabs.RemoveAt(srcCurIndex);
						//destTabGroup.Tabs.Insert(destCurIndex - 1, srcTab);

						newTabIndex = destCurIndex - 1;
					}


					#endregion //Dest was Left, Now On Right of Src
				}
				else if ( (destCurIndex < srcCurIndex && destOrigIndex > srcOrigIndex) ||
					(destCurIndex < srcCurIndex && destOrigIndex < srcOrigIndex) )
				{
					// Destination is Left of Source but was Right Of Source
					// OR
					// Destination is Left of Source AND was Left Of Source
					Debug.WriteLine("Destination is Before Source");

					// the mouse is over a tab that was on the right originally but
					// is now to the left
					#region Dest was Right, Now On Right of Left

					Rectangle elementRect = hitInfo.Element.Rect;

					// get its size
					Size srcTabSize = destTabGroup.TabManager.GetTabItemSize(srcTab);

					int interTabSpacing = destTabGroup.TabManager.InterTabSpacingResolved;

					Rectangle testSrcRect;

					switch(destTabGroup.SettingsResolved.TabOrientation)
					{
						case TabOrientation.LeftTop:
						case TabOrientation.RightTop:
							testSrcRect = new Rectangle(elementRect.X,
								elementRect.Y, elementRect.Width, srcTabSize.Height);
							break;
						case TabOrientation.RightBottom:
						case TabOrientation.LeftBottom:
							testSrcRect = new Rectangle(elementRect.X,
								elementRect.Bottom - srcTabSize.Height, elementRect.Width, srcTabSize.Height);
							break;
						default:
						case TabOrientation.TopLeft:
						case TabOrientation.BottomLeft:
							testSrcRect = new Rectangle(elementRect.X,
								elementRect.Y, srcTabSize.Width, elementRect.Height);
							break;
						case TabOrientation.TopRight:
						case TabOrientation.BottomRight:
							testSrcRect = new Rectangle(elementRect.Right - srcTabSize.Width,
								elementRect.Y, srcTabSize.Width, elementRect.Height);


							break;
					}

					

					// Destination is Left of Source AND was Left Of Source
					if (testSrcRect.Contains(tabGroupPt))
					{
						//destTabGroup.Tabs.RemoveAt(srcCurIndex);
						//destTabGroup.Tabs.Insert(destCurIndex, srcTab);
						//Debug.WriteLine("Move the tab to the left of the destination tab");

						newTabIndex = destCurIndex;
					}
					else if (srcCurIndex != destCurIndex + 1)
					{
						// make sure its right of the destination
						//destTabGroup.Tabs.RemoveAt(srcCurIndex);
						//destTabGroup.Tabs.Insert(destCurIndex + 1, srcTab);

						newTabIndex = destCurIndex + 1;
					}

					#endregion //Dest was Right, Now On Left of Src
				}
				else
				{
					// AS 4/30/03 FxCop Change
					// Explicitly call the overload that takes an IFormatProvider
					//
					//Debug.WriteLine( string.Format("DestTab==SrcTab? {0}; SrcCurIndex={1}; SrcOrigIndex={2}; DestCurIndex={3}; DestOrigIndex={4}", destTab == srcTab, srcCurIndex, srcOrigIndex, destCurIndex, destOrigIndex) );
					Debug.WriteLine( string.Format(null, "DestTab==SrcTab? {0}; SrcCurIndex={1}; SrcOrigIndex={2}; DestCurIndex={3}; DestOrigIndex={4}", destTab == srcTab, srcCurIndex, srcOrigIndex, destCurIndex, destOrigIndex) );
				}

				// potentially start scroll timer?
				if (!this.AllowScroll									||
					hitInfo.ScrollDirection == DragScrollDirection.None ||
					hitInfo.Distance >= this.ScrollDistanceMaximum		||
					hitInfo.Distance <= this.ScrollDistanceMinimum)
				{
					if (this.HasScrollTimer && this.ScrollTimer.Enabled)
						this.ScrollTimer.Enabled = false;
				}
				else
				{
					int interval = this.GetScrollInterval(hitInfo.Distance);

					if (!this.HasScrollTimer		|| 
						!this.ScrollTimer.Enabled	||
						this.ScrollTimer.Interval != interval)
					{
						if (interval >= 0)
						{
							this.ScrollTimer.Interval = 500;
							this.ScrollTimer.Enabled = true;
						}
						else if (this.ScrollTimer.Enabled)
							this.ScrollTimer.Enabled = false;
					}

					this.scrollDirection = hitInfo.ScrollDirection;
				}


				if (newTabIndex != -1 || this.dropAction != MdiTabDropAction.TabGroup)
				{
					if (this.InvokeDragOver(MdiTabDropAction.TabGroup, this.lastScreenPt, destTabGroup, Cursors.Arrow))
					{
						// reposition the tab
						if (newTabIndex != -1)
						{
							// AS 4/30/03 FxCop Change
							// Explicitly call the overload that takes an IFormatProvider
							//
							//Debug.WriteLine( string.Format("Current Info: DestTab==SrcTab? {0}; SrcCurIndex={1}; SrcOrigIndex={2}; DestCurIndex={3}; DestOrigIndex={4}", destTab == srcTab, srcCurIndex, srcOrigIndex, destCurIndex, destOrigIndex) );
							//Debug.WriteLine( string.Format("Repositioning Tab: {0} to {1}", srcCurIndex, newTabIndex) );
							Debug.WriteLine( string.Format(null, "Current Info: DestTab==SrcTab? {0}; SrcCurIndex={1}; SrcOrigIndex={2}; DestCurIndex={3}; DestOrigIndex={4}", destTab == srcTab, srcCurIndex, srcOrigIndex, destCurIndex, destOrigIndex) );
							Debug.WriteLine( string.Format(null, "Repositioning Tab: {0} to {1}", srcCurIndex, newTabIndex) );

							// AS 5/19/03 WTB968
							// Use a single method since doing it separately is causing
							// us to verify the SelectedTabItem while repositioning.
							//
							//destTabGroup.Tabs.RemoveAt(srcCurIndex);
							//destTabGroup.Tabs.Insert(newTabIndex, srcTab);
							destTabGroup.Tabs.Reposition(srcTab, newTabIndex);
						}

						// make sure if the tab that was the first displayed
						// was changed, that we reset it properly
						if (destTabGroup.Tabs[firstTabIndex] != destTabGroup.FirstDisplayedTab)
						{
							if (destTabGroup.Tabs[firstTabIndex] == null)
								destTabGroup.TabManager.FirstDisplayedTabItem = destTabGroup.TabManager.LastVisibleTabItem;
							else
								destTabGroup.FirstDisplayedTab = destTabGroup.Tabs[firstTabIndex];
						}

						// AS 4/24/03
						// if there was no change, then the new tab index is the current index
						if (newTabIndex == -1)
							// AS 5/29/03 WTB1011
							// The DropTabIndex needs to be initialized and not the local
							// variable. If this doesn't happen and you drag and drop a tab 
							// onto itself, it will be repositioned incorrectly instead of 
							// remaining where it was installed.
							//
							//newTabIndex = srcCurIndex;
							this.dropTabIndex = srcCurIndex;
						else
							this.dropTabIndex = newTabIndex;
					}
					else
					{
						// AS 4/24/03
						// the drop tab index is its current index
						//
						//this.dropTabIndex = -1;
						this.dropTabIndex = srcCurIndex;
					}
				}
			}
			#endregion //ProcessTabRowAreaDrop

			#region InvokeDragOver
			private bool InvokeDragOver(MdiTabDropAction dropAction, Point screenPt, MdiTabGroup tabGroup, Cursor defaultCursor)
			{
				return this.InvokeDragOver(dropAction, screenPt, tabGroup, defaultCursor, TabGroupDropArea.Middle);
			}

			// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
			// Added an overload that can be used when creating a new group so we know
			// if it will be vertical or horizontal and before/after the destination tab group.
			//
			private bool InvokeDragOver(MdiTabDropAction dropAction, Point screenPt, MdiTabGroup tabGroup, Cursor defaultCursor, TabGroupDropArea newTabGroupDropArea)
			{
				// store the drop action and tab group
				this.dropAction = dropAction;
				this.dropTabGroup = tabGroup;

				// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
				this.dropNewGroupArea = newTabGroupDropArea;

				UltraTabbedMdiManager mdiManager = this.TabGroup.Manager;

				if (mdiManager == null)
					return false;

				#region Invalid Drop Location
				// for now we will not pass along an invalid point. this imposes a limitation
				// that the programmer cannot specify a cursor for the no-drop area but it avoids
				// certain issues - e.g. what do we do if we considered a point invalid but they
				// mark it as valid?
				//
				if (dropAction == MdiTabDropAction.None)
				{
					Cursor cursor = defaultCursor;

					if (cursor == null)
						cursor = this.NoDragCursor;

					if (Cursor.Current != cursor)
						Cursor.Current = cursor;

					return false;
				}
				#endregion //Invalid Drop Location

				// create the args for the TabDragOver event
				// AS 3/24/05 NA 2005 Vol 2 - Nested TabGroups
				//MdiTabDragOverEventArgs e = new MdiTabDragOverEventArgs(this.Tab as MdiTab, screenPt, dropAction, tabGroup);
				RelativePosition relativePos = (newTabGroupDropArea == TabGroupDropArea.LeftEdge || newTabGroupDropArea == TabGroupDropArea.TopEdge) 
					? RelativePosition.Before : RelativePosition.After;
				Orientation orientation = (newTabGroupDropArea == TabGroupDropArea.LeftEdge || newTabGroupDropArea == TabGroupDropArea.RightEdge) 
					? Orientation.Vertical : Orientation.Horizontal;

				MdiTabDragOverEventArgs e = new MdiTabDragOverEventArgs(this.Tab as MdiTab, screenPt, dropAction, tabGroup, relativePos, orientation);

				// invoke the event
				mdiManager.InvokeEvent(TabbedMdiEventIds.TabDragOver, e);

				// now initialize the cursor
				//

				// if the programmer specified one, use that. otherwise use the one passed
				// into the method
				Cursor cursorToUse = e.DragCursor != null ? e.DragCursor : defaultCursor;

				// if they're both null, use the drag cursor or no drag cursor depending
				// on whether the drop point is valid
				if (cursorToUse == null)
					cursorToUse = e.AllowDrop ? this.DragCursor : this.NoDragCursor;

				// if the cursor needs to change, change it now
				if (Cursor.Current != cursorToUse)
					Cursor.Current = cursorToUse;

				// if the programmer indicates that this is not a valid drop location
				if (!e.AllowDrop)
					this.dropAction = MdiTabDropAction.None;

				// return the allow drop
				return e.AllowDrop;
			}
			#endregion //InvokeDragOver

			#endregion // Moving related

			#region Helper routines

			#region CacheOriginalIndexes
			private void CacheOriginalIndexes(MdiTabGroup tabGroup)
			{
				if (this.originalIndexes == null)
					this.originalIndexes = new HybridDictionary(this.TabGroup.Manager.TabGroups.Count);

				if (this.originalIndexes[tabGroup] == null)
				{
					HybridDictionary tabIndex = new HybridDictionary(tabGroup.Tabs.Count);

					this.originalIndexes.Add(tabGroup, tabIndex);

					foreach(ITabItem tab in tabGroup.Tabs)
					{
						tabIndex.Add(tab, tab.Index);
					}
				}
			}
			#endregion //CacheOriginalIndexes

			#region GetOriginalTabIndex
			private int GetOriginalTabIndex( MdiTab tab, MdiTabGroup tabGroup)
			{
				HybridDictionary tabGroupIndexes = this.originalIndexes[tabGroup] as HybridDictionary;

				Debug.Assert( tabGroupIndexes != null, "An original index was retreived before it was cached.");

				if (tabGroupIndexes[tab] != null)
					return (int)tabGroupIndexes[tab];
				else
					return int.MaxValue;
			}
			#endregion //GetOriginalTabIndex

			#region GetTabGroupArea
			private TabGroupArea GetTabGroupArea( MdiTabGroup tabGroup, Point screenPt )
			{
				UltraTabbedMdiManager mdiManager = this.TabGroup.Manager;

				if (mdiManager == null || mdiManager.MdiParent == null)
					return TabGroupArea.None;

				// convert the coordinates to those relative to the mdi parent
				// since that is how the coordinates are returned from the 
				// TabGroupInfoManager (although the GetFormRect method is in mdiclient coordinates)
				//
				Point clientPt = mdiManager.MdiParent.PointToClient(screenPt);

				TabGroupInfoManager groupManager = mdiManager.TabGroupManager;

				if (groupManager.GetTabGroupControlRectangle(tabGroup).Contains(clientPt))
					return TabGroupArea.TabGroupControl;

				if (groupManager.GetSplitterControlRectangle(tabGroup).Contains(clientPt))
					return TabGroupArea.Splitter;

				if (groupManager.GetTabGroupFormRect(tabGroup).Contains(clientPt))
					return TabGroupArea.Form;

				return TabGroupArea.None;
			}
			#endregion //GetTabGroupArea

			#region Hook/UnhookMessageFilter
			private void HookMessageFilter()
			{
				if (this.messageFilterAdded)
					return;

				try
				{
					SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);

					perm.Assert();

					// AS 11/10/03 WTB538
					//Application.AddMessageFilter(this);
					Infragistics.Win.Utilities.AddMessageFilter(this);

					this.messageFilterAdded = true;
				}
				catch (System.Security.SecurityException)
				{
				}
			}

			private void UnhookMessageFilter()
			{
				if (!this.messageFilterAdded)
					return;

				try
				{
					SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);

					perm.Assert();

					// AS 11/10/03 WTB538
					//Application.RemoveMessageFilter(this);
					Infragistics.Win.Utilities.RemoveMessageFilter(this);

					this.messageFilterAdded = false;
				}
				catch (System.Security.SecurityException)
				{
				}
			}
			#endregion //Hook/UnhookMessageFilter

			#region RestoreTabPosition
			private void RestoreTabPosition()
			{
				if (this.originalSrcTabGroup != null)
				{
					// AS 4/24/03
					// Get the index of the first displayed tab.
					//
					int currentFirstDisplayedIndex = ((ITabItem)this.originalSrcTabGroup.FirstDisplayedTab).Index;

					// put the tab back where it was
					if (this.originalTabIndex != this.Tab.Index)
					{
						// AS 5/19/03 WTB968
						// Use a single method since doing it separately is causing
						// us to verify the SelectedTabItem while repositioning.
						//
						//this.originalSrcTabGroup.Tabs.RemoveAt(this.Tab.Index);
						//this.originalSrcTabGroup.Tabs.Insert(this.originalTabIndex, this.Tab as MdiTab);
						this.originalSrcTabGroup.Tabs.Reposition(this.Tab as MdiTab, this.originalTabIndex);
					}

					// AS 4/24/03
					// Don't reset the first displayed tab to the original
					// index. Keep the index the same as it was during the
					// scroll operation.
					//
					// reset the first displayed tab item if necessary
					//if (this.originalSrcTabGroup.FirstDisplayedTab != this.originalFirstDisplayedTab)
					//	this.originalSrcTabGroup.FirstDisplayedTab = this.originalFirstDisplayedTab;
					if (this.originalSrcTabGroup.FirstDisplayedTab != null && 
						((ITabItem)this.originalSrcTabGroup.FirstDisplayedTab).Index != currentFirstDisplayedIndex)
						this.originalSrcTabGroup.FirstDisplayedTab = this.originalSrcTabGroup.Tabs[currentFirstDisplayedIndex];
				}
			}
			#endregion //RestoreTabPosition

			#region ShowDropRect
			private void ShowDropRect( Rectangle screenRect )
			{
				// if we're just drawing the rect in the
				// same place and size as last time, don't bother
				if (screenRect == this.previousDropRect)
					return;

				// if the previous drop rect was not empty then clear it
				if (!this.previousDropRect.IsEmpty)
					NativeWindowMethods.DrawReversibleFrame(this.previousDropRect);

				// if the new rect is not empty then draw it
				if (!screenRect.IsEmpty)
					NativeWindowMethods.DrawReversibleFrame(screenRect);

				// store the last rect
				this.previousDropRect = screenRect;
			}
			#endregion // ShowDropRect

			#endregion // Helper routines

			#endregion //Methods

			#region Enums

			#region TabGroupArea enum
			private enum TabGroupArea
			{
				TabGroupControl,

				Splitter,

				Form,

				None
			}
			#endregion //TabGroupArea enum

			// AS 3/21/05 NA 2005 Vol 2 - Nested TabGroups
			#region TabGroupDropArea
			private enum TabGroupDropArea
			{
				LeftEdge,
				TopEdge,
				RightEdge,
				BottomEdge,
				Middle,
			}
			#endregion //TabGroupDropArea

			#endregion //Enums

			#region Interfaces

			#region IMessageFilter
			// MD 1/4/07 - FxCop
			[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode )]
			bool IMessageFilter.PreFilterMessage(ref Message m)
			{
				if (!this.IsDragging)
					return false;

				if (m.Msg >= NativeWindowMethods.WmKeyFirst && m.Msg <= NativeWindowMethods.WmKeyLast)
				{
					// MD 10/31/06 - 64-Bit Support
					//if (m.Msg == NativeWindowMethods.WmKeyDown && m.WParam.ToInt32() == NativeWindowMethods.VkEscape)
					if ( m.Msg == NativeWindowMethods.WmKeyDown && m.WParam.ToInt64() == NativeWindowMethods.VkEscape )
						this.TabManager.CancelDrag();

					return true;
				}

				return false;
			}
			#endregion //IMessageFilter

			#endregion //Interfaces
		}
		#endregion //MdiTabDragManager

		#endregion // Nested classes

		#region TabManagerChangeType enum
		/// <summary>
		/// Enumeration of changes to the <see cref="Infragistics.Win.UltraWinTabs.TabManager"/> of an <see cref="MdiTabGroup"/>
		/// </summary>
		[Flags()]
		internal protected enum TabManagerChangeType
		{
			/// <summary>
			/// No change is required
			/// </summary>
			None			= 0x00,

			/// <summary>
			/// The collection has changed
			/// </summary>
			Collection		= 0x01,

			/// <summary>
			/// All metrics have changed
			/// </summary>
			AllMetrics		= 0x02,

			/// <summary>
			/// The tab area should be invalidated
			/// </summary>
			Invalidate		= 0x04,

			/// <summary>
			/// The font information has changed
			/// </summary>
			Font			= 0x08,
		}
		#endregion //TabManagerChangeType

		// MRS 04/12/04 - Added Accessibility support
		#region CreateTabAccessibilityInstance
		/// <summary>
		/// Creates an accessible object for a tab.
		/// </summary>
		/// <param name="tab">The related tab.</param>
		/// <returns>A new <see cref="AccessibleObject"/> object for the tab.</returns>
		internal protected virtual System.Windows.Forms.AccessibleObject CreateTabAccessibilityInstance( MdiTab tab )
		{
			return new TabManager.TabItemAccessibleObject( tab, this.TabManager ).AccessibleObject;
		}

		#endregion CreateTabAccessibilityInstance
	}
}
