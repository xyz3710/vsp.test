#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTabs;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	internal class DragDropHelper
	{
		#region Member Variables

		private UltraTabbedMdiManager		manager = null;
		private MdiTabGroup					tabGroup = null;

		private DateTime					scrollStartTime = DateTime.MaxValue;
		private DragScrollDirection			scrollDirection = DragScrollDirection.Next;
		private DateTime					lastScrollTime = DateTime.MaxValue;

		private static readonly TimeSpan	InitialScrollDelay = new TimeSpan(0,0,0,0,500);
		private static readonly TimeSpan	ScrollModeDelay = new TimeSpan(0,0,0,0,100);
		private TimeSpan					scrollDelay = InitialScrollDelay;

		private const int					ScrollThreshold = -20;

		private ITabItem					dragOverTab = null;
		private DateTime					tabEnterTime = DateTime.MaxValue;

		private static readonly TimeSpan	InitialActivateDelay = new TimeSpan(0,0,0,1,0);
		private static readonly TimeSpan	ActivateModeDelay = new TimeSpan(0,0,0,0,100);

		private TimeSpan					activateDelay = InitialActivateDelay;

		#endregion //Member Variables

		#region Constructor
		internal DragDropHelper(UltraTabbedMdiManager manager)
		{
			this.manager = manager;
		}
		#endregion //Constructor

		#region Methods

		#region Internal
		internal void DragEnter( MdiTabGroupControl tabGroupControl, DragEventArgs e )
		{
			this.ProcessDragOver(tabGroupControl, e);
		}

		internal void DragDrop( MdiTabGroupControl tabGroupControl, DragEventArgs e )
		{
			
			this.ResetScrollInfo();
			this.ResetActivateInfo(true);
			this.tabGroup = null;
		}

		internal void DragOver( MdiTabGroupControl tabGroupControl, DragEventArgs e )
		{
			this.ProcessDragOver(tabGroupControl, e);
		}

		internal void DragLeave( MdiTabGroupControl tabGroupControl )
		{
			// reset any variables
			this.ResetScrollInfo();
			this.ResetActivateInfo(true);
			this.tabGroup = null;
		}
		#endregion //Internal

		#region Private

		#region ProcessDragOver
		private void ProcessDragOver( MdiTabGroupControl tabGroupControl, DragEventArgs e )
		{
			if (tabGroupControl == null)
				return;

			this.tabGroup = tabGroupControl.TabGroup;

			if (this.tabGroup == null		|| 
				tabGroupControl.IsDisposed	|| 
				tabGroupControl.Disposing)
			{
				this.ResetScrollInfo();
				this.ResetActivateInfo(true);
				return;
			}

			// get the point from the drag info
			Point screenPt = new Point(e.X, e.Y);

			// convert it to client coordinates
			Point clientPt = tabGroupControl.PointToClient(screenPt);

			// do a hit test to see where the mouse is in 
			// relation to the tabs
			TabHitTestInfo hitInfo = this.tabGroup.TabManager.HitTest( clientPt );

			// if the mouse is not within the bounds
			// of the row area, leave
			if (!hitInfo.RowAreaRect.Contains(clientPt))
			{
				this.ResetScrollInfo();
				this.ResetActivateInfo(true);
				return;
			}

			// see if we need to perform a scroll operation
			this.ProcessDragScroll(hitInfo);

			// see if we need to activate a tab
			this.ProcessTabActivate(hitInfo);
		}
		#endregion //ProcessDragOver

		#region ProcessTabActivate
		private void ProcessTabActivate( TabHitTestInfo hitInfo )
		{
			// if we're not over a tab, then reset the tab activate info
			if (hitInfo.Tab == null)
			{
				this.ResetActivateInfo(false);
				return;
			}

			DateTime currentTime = DateTime.Now;

			// if we're over a different tab, just store the tab and time
			if (hitInfo.Tab != this.dragOverTab)
			{
				this.dragOverTab = hitInfo.Tab;
				this.tabEnterTime = currentTime;
				return;
			}

			// we're over a tab that we were previously over
			//

			// find out how long we have been over the tab
			TimeSpan ts = currentTime.Subtract(this.tabEnterTime);

			// if we've been over it long enough, then activate it
			if (ts > this.activateDelay)
			{
				if (this.lastScrollTime != DateTime.MaxValue && 
					currentTime.Subtract(this.lastScrollTime) < new TimeSpan(0,0,0,0,200))
					return;

				ITabItem firstDisplayedTab = this.tabGroup.FirstDisplayedTab;

				this.tabGroup.SelectedTab = this.dragOverTab as MdiTab;

				// if we caused a scroll, reset the scroll time
				if (firstDisplayedTab != this.tabGroup.FirstDisplayedTab)
				{
					this.scrollStartTime = currentTime;
					this.activateDelay = new TimeSpan(0,0,0,0,500);
					this.dragOverTab = null;
				}
				else
					this.activateDelay = ActivateModeDelay;
			}
		}
		#endregion //ProcessTabActivate

		#region ProcessDragScroll
		private bool ProcessDragScroll( TabHitTestInfo hitInfo )
		{
			// if this will not be a scroll op - OR -
			// we're more than our threshold away from the edge
			// 
			if (hitInfo.ScrollDirection == DragScrollDirection.None ||
				hitInfo.Distance < ScrollThreshold)
			{
				this.ResetScrollInfo();
				return false;
			}

			DateTime currentTime = DateTime.Now;

			// if we weren't in the scroll area before and 
			// we are now or we were holding a different 
			// scroll direction, just store the direction and time
			if (this.scrollDirection != hitInfo.ScrollDirection)
			{
				this.scrollDelay = InitialScrollDelay;
				this.scrollDirection = hitInfo.ScrollDirection;
				this.scrollStartTime = currentTime;
				return false;
			}

			// we're in the same scroll direction we were before
			// so see if we have been there long enough since
			// the last scroll operation
			//

			// find out how long we have been over the area
			TimeSpan ts = currentTime.Subtract(this.scrollStartTime);

			if (ts >= this.scrollDelay)
			{
				// scroll if we have been in this zone long enough
				//

				// find out the direction
                Infragistics.Win.UltraWinTabs.ScrollType scrollType = 
                    hitInfo.ScrollDirection == DragScrollDirection.Previous ?
                    Infragistics.Win.UltraWinTabs.ScrollType.Previous :
                    Infragistics.Win.UltraWinTabs.ScrollType.Next;

                // tell the tab group to scroll
				this.tabGroup.Scroll(scrollType);

				// update the start of the scroll time
				this.scrollStartTime = currentTime;

				// store when we did the last scroll operation
				this.lastScrollTime = currentTime;

				this.scrollDelay = ScrollModeDelay;

				// indicate that we performed a scroll
				return true;
			}

			return false;
		}
		#endregion //ProcessDragScroll

		#region ResetActivateInfo
		private void ResetActivateInfo(bool resetDelayToInitial)
		{
			this.dragOverTab = null;
			this.tabEnterTime = DateTime.MaxValue;

			if (resetDelayToInitial)
				this.activateDelay = InitialActivateDelay;
		}
		#endregion //ResetActivateInfo

		#region ResetScrollInfo
		private void ResetScrollInfo()
		{
			this.scrollStartTime = DateTime.MaxValue;
			this.scrollDirection = DragScrollDirection.None;
			this.lastScrollTime = DateTime.MaxValue;
			this.scrollDelay = InitialScrollDelay;
		}
		#endregion //ResetScrollInfo

		#endregion //Private

		#endregion //Methods
	}
}
