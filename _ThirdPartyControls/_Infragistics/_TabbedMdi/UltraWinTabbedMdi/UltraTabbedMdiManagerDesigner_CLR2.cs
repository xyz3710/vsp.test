#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel.Design;
using System.ComponentModel;
using Infragistics.Win.Design;
using Infragistics.Shared;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi.Design
{
    // JAS v5.3 SmartTags 7/13/05
    public partial class UltraTabbedMdiManagerDesigner : UltraComponentManagerDesigner
    {
        #region Data

        private DesignerActionListCollection actionLists;

        #endregion // Data

        #region ActionLists

        /// <summary>
        /// Gets the <see cref="DesignerActionListCollection"/> for this designer's control/component.
        /// </summary>
        public override DesignerActionListCollection ActionLists
        {
            get
            {
                if( this.actionLists == null )
                {
                    this.actionLists = new DesignerActionListCollection();
                    this.actionLists.Add( new UltraTabbedMdiManagerActionList( this.Component as UltraTabbedMdiManager ) );
                }
                return this.actionLists;
            }
        }

        #endregion // ActionLists

        // JAS v5.3 SmartTags 7/13/05 - Moved these methods over from
        // UltraTabbedMdiManager.cs because they are CLR2-specific.
        // --------------------------------------------------------
        #region InitializeExistingComponent
        /// <summary>
        /// Invoked when the designer is first initialized so it may set default values on the component its managing.
        /// </summary>
        public override void InitializeExistingComponent( System.Collections.IDictionary defaultValues )
        {
            base.InitializeExistingComponent( defaultValues );

            this.EnsureOnlyOneInstance();

            this.SetHostControl();
        }
        #endregion //InitializeExistingComponent

        #region InitializeNewComponent
        /// <summary>
        /// Invoked when the designer is first initialized so it may set default values on the component its managing.
        /// </summary>
        public override void InitializeNewComponent( System.Collections.IDictionary defaultValues )
        {
            base.InitializeNewComponent( defaultValues );

            this.SetHostControl();

			
        }
        #endregion //InitializeNewComponent
        // --------------------------------------------------------

        #region UltraTabbedMdiManagerActionList [nested class]

        /// <summary>
        /// Provides DesignerActionItems for the SmartTag associated with the UltraTabbedMdiManager.
        /// </summary>
        // MD 1/4/07 - FxCop
		[PermissionSet( SecurityAction.LinkDemand, Name = "FullTrust" )]
		[PermissionSet( SecurityAction.InheritanceDemand, Name = "FullTrust" )]
		public class UltraTabbedMdiManagerActionList : UltraActionListBase<UltraTabbedMdiManager>
        {
            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            public UltraTabbedMdiManagerActionList( UltraTabbedMdiManager product ) : base( product ) { }

            #endregion // Constructor

            #region CreateActionItems

            /// <summary>
            /// Overrides CreateActionItems.
            /// </summary>
            /// <param name="actionItems">The list to populate.</param>
            protected override void CreateActionItems( DesignerActionItemCollection actionItems )
            {
                // MBS 6/29/06 BR13853
                // Replaces all calls to actionItems.Add with new helper methods
                #region ViewStyle

                //actionItems.Add( new DesignerActionPropertyItem(
                //    "ViewStyle",
                //    SR.GetString( "UltraTabbedMdiManagerActionList_P_ViewStyle_DisplayName" ),
                //    this.GetCategory( "ViewStyle" ),
                //    this.GetDescription( "ViewStyle" ) )
                //);
                this.AddActionPropertyItem(
                    actionItems,
                    "ViewStyle",
                    SR.GetString("UltraTabbedMdiManagerActionList_P_ViewStyle_DisplayName"),
                    "ViewStyle"
                );

                #endregion // ViewStyle

                #region TabGroupSettings_TextOrientation

                //actionItems.Add( new DesignerActionPropertyItem(
                //    "TabGroupSettings_TextOrientation",
                //    SR.GetString( "UltraTabbedMdiManagerActionList_P_TabGroupSettings_TextOrientation_DisplayName" ),
                //    this.GetCategory( "TabGroupSettings_TextOrientation" ),
                //    this.GetDescription( "TabGroupSettings_TextOrientation" ) )
                //);
                this.AddActionPropertyItem(
                    actionItems,
                    "TabGroupSettings_TextOrientation",
                    SR.GetString("UltraTabbedMdiManagerActionList_P_TabGroupSettings_TextOrientation_DisplayName"),
                    "TabGroupSettings_TextOrientation"
                );

                #endregion // TabGroupSettings_TextOrientation

                #region AllowHorizontalTabGroups

                //actionItems.Add( new DesignerActionPropertyItem(
                //    "AllowHorizontalTabGroups",
                //    SR.GetString( "UltraTabbedMdiManagerActionList_P_AllowHorizontalTabGroups_DisplayName" ),
                //    this.GetCategory( "AllowHorizontalTabGroups" ),
                //    this.GetDescription( "AllowHorizontalTabGroups" ) )
                //);
                this.AddActionPropertyItem(
                    actionItems,
                    "AllowHorizontalTabGroups",
                    SR.GetString("UltraTabbedMdiManagerActionList_P_AllowHorizontalTabGroups_DisplayName"),
                    "AllowHorizontalTabGroups"
                );

                #endregion // AllowHorizontalTabGroups

                #region AllowVerticalTabGroups

                //actionItems.Add( new DesignerActionPropertyItem(
                //    "AllowVerticalTabGroups",
                //    SR.GetString( "UltraTabbedMdiManagerActionList_P_AllowVerticalTabGroups_DisplayName" ),
                //    this.GetCategory( "AllowVerticalTabGroups" ),
                //    this.GetDescription( "AllowVerticalTabGroups" ) )
                //);
                this.AddActionPropertyItem(
                    actionItems,
                    "AllowVerticalTabGroups",
                    SR.GetString("UltraTabbedMdiManagerActionList_P_AllowVerticalTabGroups_DisplayName"),
                    "AllowVerticalTabGroups"
                );

                #endregion // AllowVerticalTabGroups

                #region AllowNestedTabGroups

                //actionItems.Add( new DesignerActionPropertyItem(
                //    "AllowNestedTabGroups",
                //    SR.GetString( "UltraTabbedMdiManagerActionList_P_AllowNestedTabGroups_DisplayName" ),
                //    this.GetCategory( "AllowNestedTabGroups" ),
                //    this.GetDescription( "AllowNestedTabGroups" ) )
                //);
                this.AddActionPropertyItem(
                    actionItems,
                    "AllowNestedTabGroups",
                    SR.GetString("UltraTabbedMdiManagerActionList_P_AllowNestedTabGroups_DisplayName"),
                    "AllowNestedTabGroups"
                );

                #endregion // AllowNestedTabGroups

                #region AllowMaximize

                //actionItems.Add( new DesignerActionPropertyItem(
                //    "AllowMaximize",
                //    SR.GetString( "UltraTabbedMdiManagerActionList_P_AllowMaximize_DisplayName" ),
                //    this.GetCategory( "AllowMaximize" ),
                //    this.GetDescription( "AllowMaximize" ) )
                //);
                this.AddActionPropertyItem(
                    actionItems,
                    "AllowMaximize",
                    SR.GetString("UltraTabbedMdiManagerActionList_P_AllowMaximize_DisplayName"),
                    "AllowMaximize"
                );

                #endregion // AllowMaximize
            }

            #endregion // CreateActionItems

            #region Proxies

                #region ViewStyle

            /// <summary>
            /// Used by SmartTag Panel.
            /// </summary>
            public ViewStyle ViewStyle
            {
                get { return this.GetValueTypeValue<ViewStyle>( "ViewStyle" ); }
                set { this.SetValue( "ViewStyle", value ); }
            }

                #endregion // ViewStyle

                #region TabGroupSettings_TextOrientation

            /// <summary>
            /// Used by SmartTag Panel.
            /// </summary>
            public Infragistics.Win.UltraWinTabs.TextOrientation TabGroupSettings_TextOrientation
            {
                get { return this.GetValueTypeValue<Infragistics.Win.UltraWinTabs.TextOrientation>( "TabGroupSettings_TextOrientation" ); }
                set { this.SetValue( "TabGroupSettings_TextOrientation", value ); }
            }

                #endregion // TabGroupSettings_TextOrientation

                #region AllowHorizontalTabGroups

            /// <summary>
            /// Used by SmartTag Panel.
            /// </summary>
            public bool AllowHorizontalTabGroups
            {
                get { return this.GetValueTypeValue<bool>( "AllowHorizontalTabGroups" ); }
                set { this.SetValue( "AllowHorizontalTabGroups", value ); }
            }

                #endregion // AllowHorizontalTabGroups

                #region AllowVerticalTabGroups

            /// <summary>
            /// Used by SmartTag Panel.
            /// </summary>
            public bool AllowVerticalTabGroups
            {
                get { return this.GetValueTypeValue<bool>( "AllowVerticalTabGroups" ); }
                set { this.SetValue( "AllowVerticalTabGroups", value ); }
            }

                #endregion // AllowVerticalTabGroups

                #region AllowNestedTabGroups

            /// <summary>
            /// Used by SmartTag Panel.
            /// </summary>
            public DefaultableBoolean AllowNestedTabGroups
            {
                get { return this.GetValueTypeValue<DefaultableBoolean>( "AllowNestedTabGroups" ); }
                set { this.SetValue( "AllowNestedTabGroups", value ); }
            }

                #endregion // AllowNestedTabGroups

                #region AllowMaximize

            /// <summary>
            /// Used by SmartTag Panel.
            /// </summary>
            public bool AllowMaximize
            {
                get { return this.GetValueTypeValue<bool>( "AllowMaximize" ); }
                set { this.SetValue( "AllowMaximize", value ); }
            }

                #endregion // AllowMaximize

            #endregion // Proxies
        }

        #endregion // UltraTabbedMdiManagerActionList [nested class]
    }
}
