#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Diagnostics;
using System.Drawing;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Shared.Serialization;
using System.Security.Permissions;
using Infragistics.Win.UltraWinTabs;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Maintains defaultable property settings for an <see cref="MdiTab"/>.
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>MdiTabSettings</b> object is used to affect the appearance and behavior of an <see cref="MdiTab"/> object. 
	/// The values are initialized with default values. These default values are resolved by a 
	/// <see cref="MdiTabSettingsResolved"/> instance that may be accessed from the <b>MdiTab</b> using the 
	/// <see cref="MdiTab.SettingsResolved"/> property.</p>
	/// <p class="body">The most commonly used properties of the class are the appearance properties. These properties 
	/// are used to control the look of the tab items. The class exposes 4 tab appearances. The <see cref="TabAppearance"/> property 
	/// is used to determine the default appearance for the tab. Usually the Image property is set on this property so that a 
	/// particular image is displayed for a tab regardless of its state. The <see cref="SelectedTabAppearance"/> is used to augment the 
	/// appearance of the tab when it is the <see cref="MdiTabGroup.SelectedTab"/> in its containing <see cref="MdiTabGroup"/>. When the tab 
	/// is not selected, the values on this appearance are not used. The <see cref="ActiveTabAppearance"/> is similar to that of the 
	/// <b>SelectedTabAppearance</b> in that the tab must be selected. However, just as there can be only one active mdi child, there is 
	/// only one <see cref="UltraTabbedMdiManager.ActiveTab"/>. If there is only one tab group, then the active tab will be the 
	/// selected tab since the active tab must be selected. However, when there is more than one tab group in the <see cref="UltraTabbedMdiManager.TabGroups"/> 
	/// collection, only one tab group will contain the active tab. The appearance of that active tab will be affected by the ActiveTabAppearance. The 
	/// last appearance property is the <see cref="HotTrackTabAppearance"/>. This appearance is used in the appearance resolution process when the 
	/// mouse is over the tab item if the <see cref="HotTrack"/> property resolves to true.</p>
	/// <p class="body">The class contains several properties that affect the behavior of the tab. The <see cref="AllowDrag"/> property 
	/// is used to determine whether a tab may be repositioned and to where. The <see cref="AllowClose"/> determines whether the 
	/// close button is enabled when the tab is selected while the <see cref="TabCloseAction"/> determines the action that will 
	/// be taken when the tab is closed.</p>
	/// <p class="note">Note: Only the font information for the tab appearances of the <see cref="UltraTabbedMdiManager.TabSettings"/> and 
	/// <see cref="MdiTabGroup.TabSettings"/> are used to determine the size of the tab items. The font information set on the 
	/// <see cref="MdiTab.Settings"/> will affect the display but will not be used to determine the size of the <see cref="MdiTabGroup"/>.</p>
	/// </remarks>
	/// <seealso cref="MdiTabSettingsResolved"/>
	/// <seealso cref="UltraTabbedMdiManager.TabSettings"/>
	/// <seealso cref="MdiTabGroup.TabSettings"/>
	/// <seealso cref="MdiTab.Settings"/>
	/// <seealso cref="MdiTab.SettingsResolved"/>
	/// <seealso cref="MdiTabSettingsResolved"/>
	[Serializable()]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MdiTabSettings : SubObjectBase, 
		IDeserializationCallback,
		ISerializable
	{
		#region Member Variables

		private AppearanceHolder			activeTabAppearanceHolder = null;
		private AppearanceHolder			hotTrackTabAppearanceHolder = null;
		private AppearanceHolder			selectedTabAppearanceHolder = null;
		private AppearanceHolder			tabAppearanceHolder = null;

		private DefaultableBoolean			allowClose = DefaultableBoolean.Default;
		private DefaultableBoolean			hotTrack = DefaultableBoolean.Default;
		private MdiTabDragStyle				allowDrag = MdiTabDragStyle.Default;
		private MdiTabCloseAction			tabCloseAction = MdiTabCloseAction.Default;

		private const int					TabWidthDefault = -1;
		private int							tabWidth = TabWidthDefault;

		private DefaultableBoolean			displayFormIcon = DefaultableBoolean.Default;

		private object						owner = null;

		// MD 11/7/07 - NA 2008 Vol 1
		private TabCloseButtonAlignment		closeButtonAlignment = TabCloseButtonAlignment.Default;
		private TabCloseButtonVisibility	closeButtonVisibility = TabCloseButtonVisibility.Default;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <b>MdiTabSettings</b> instance
		/// </summary>
		public MdiTabSettings(object owner)
		{
			this.owner = owner;
		}

		/// <summary>
		/// Constructor used during deserialization to initialize a new <see cref="MdiTabSettings"/> object
		/// with the serialized property values.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected MdiTabSettings( SerializationInfo info, StreamingContext context )
		{
			foreach( SerializationEntry entry in info )
			{
				switch (entry.Name)
				{
					case "ActiveTabAppearance":
						this.activeTabAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.activeTabAppearanceHolder);
						break;
					case "AllowClose":
						this.allowClose = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.allowClose);
						break;
					case "AllowDrag":
						this.allowDrag = (MdiTabDragStyle)Utils.DeserializeProperty(entry, typeof(MdiTabDragStyle), this.allowDrag);
						break;
					case "HotTrack":
						this.hotTrack = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.hotTrack);
						break;
					case "HotTrackTabAppearance":
						this.hotTrackTabAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.hotTrackTabAppearanceHolder);
						break;
					case "SelectedTabAppearance":
						this.selectedTabAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.selectedTabAppearanceHolder);
						break;
					case "TabAppearance":
						this.tabAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty(entry, typeof(AppearanceHolder), this.tabAppearanceHolder);
						break;
					case "TabCloseAction":
						this.tabCloseAction = (MdiTabCloseAction)Utils.DeserializeProperty(entry, typeof(MdiTabCloseAction), this.tabCloseAction);
						break;
					case "TabWidth":
						this.tabWidth = (int)Utils.DeserializeProperty(entry, typeof(int), this.tabWidth);
						break;
					case "DisplayFormIcon":
						this.displayFormIcon = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.displayFormIcon);
						break;
					case MdiTabSettings.TagSerializationName:
						this.DeserializeTag(entry);
						break;

					// MD 11/7/07 - NA 2008 Vol 1
					case "CloseButtonAlignment":
						this.closeButtonAlignment = (TabCloseButtonAlignment)Utils.DeserializeProperty( entry, typeof( TabCloseButtonAlignment ), this.closeButtonAlignment );
						break;

					case "CloseButtonVisibility":
						this.closeButtonVisibility = (TabCloseButtonVisibility)Utils.DeserializeProperty( entry, typeof( TabCloseButtonVisibility ), this.closeButtonVisibility );
						break;

					default:
						// AS 4/30/03 FxCop Change
						// Explicitly call the overload that takes an IFormatProvider
						//
						//System.Diagnostics.Debug.Assert(false, string.Format("Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						System.Diagnostics.Debug.Assert(false, string.Format(null, "Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						break;
				}
			}
		}
		#endregion //Constructor

		#region Properties

		#region Manager
		/// <summary>
		/// Returns the associated <b>UltraTabbedMdiManager</b>.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UltraTabbedMdiManager Manager
		{
			get
			{
				if (this.owner is UltraTabbedMdiManager)
					return this.owner as UltraTabbedMdiManager;

				if (this.owner is MdiTabGroup)
					return ((MdiTabGroup)this.owner).Manager;

				if (this.owner is MdiTab)
				{
					MdiTab tab = this.owner as MdiTab;

					if (tab.TabGroup != null)
						return tab.TabGroup.Manager;
				}

				return null;
			}
		}
		#endregion //Manager

		#region ActiveTabAppearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> for the <see cref="UltraTabbedMdiManager.ActiveTab"/>.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabSettings_P_ActiveTabAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase ActiveTabAppearance
		{
			get
			{
				if (this.activeTabAppearanceHolder == null)
				{
					this.activeTabAppearanceHolder = new AppearanceHolder();

					this.activeTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.activeTabAppearanceHolder.Collection = this.Manager.Appearances;

				return this.activeTabAppearanceHolder.Appearance;
			}

			set
			{
				if (this.activeTabAppearanceHolder == null				|| 
					!this.activeTabAppearanceHolder.HasAppearance		||
					value != this.activeTabAppearanceHolder.Appearance)
				{
					if (null == this.activeTabAppearanceHolder)
					{
						this.activeTabAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.activeTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.activeTabAppearanceHolder.Collection = this.Manager.Appearances;

					this.activeTabAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.ActiveTabAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="ActiveTabAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="ActiveTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasActiveTabAppearance
		{
			get
			{
				return (null != this.activeTabAppearanceHolder &&
					this.activeTabAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="ActiveTabAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ActiveTabAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="ActiveTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeActiveTabAppearance()
		{
			return this.HasActiveTabAppearance && this.activeTabAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="ActiveTabAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="ActiveTabAppearance"/> property. If the <see cref="ActiveTabAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="ActiveTabAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="ActiveTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetActiveTabAppearance()
		{
			if ( this.HasActiveTabAppearance )
			{
				this.activeTabAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.ActiveTabAppearance );
			}
		}
		#endregion ActiveTabAppearance

		#region HotTrackTabAppearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> object used to format the tab when the mouse is over the tab item.
		/// </summary>
		/// <remarks>
		/// <p class="note">The <b>HotTrackTabAppearance</b> is only used if the <see cref="HotTrack"/> property resolves to true. By default, hot tracking 
		/// is not enabled.</p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabSettings_P_HotTrackTabAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase HotTrackTabAppearance
		{
			get
			{
				if (this.hotTrackTabAppearanceHolder == null)
				{
					this.hotTrackTabAppearanceHolder = new AppearanceHolder();

					this.hotTrackTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.hotTrackTabAppearanceHolder.Collection = this.Manager.Appearances;

				return this.hotTrackTabAppearanceHolder.Appearance;
			}

			set
			{
				if (this.hotTrackTabAppearanceHolder == null				|| 
					!this.hotTrackTabAppearanceHolder.HasAppearance		||
					value != this.hotTrackTabAppearanceHolder.Appearance)
				{
					if (null == this.hotTrackTabAppearanceHolder)
					{
						this.hotTrackTabAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.hotTrackTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.hotTrackTabAppearanceHolder.Collection = this.Manager.Appearances;

					this.hotTrackTabAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.HotTrackTabAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="HotTrackTabAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="HotTrackTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasHotTrackTabAppearance
		{
			get
			{
				return (null != this.hotTrackTabAppearanceHolder &&
					this.hotTrackTabAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="HotTrackTabAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="HotTrackTabAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="HotTrackTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeHotTrackTabAppearance()
		{
			return this.HasHotTrackTabAppearance && this.hotTrackTabAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="HotTrackTabAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="HotTrackTabAppearance"/> property. If the <see cref="HotTrackTabAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="HotTrackTabAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="HotTrackTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetHotTrackTabAppearance()
		{
			if ( this.HasHotTrackTabAppearance )
			{
				this.hotTrackTabAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.HotTrackTabAppearance );
			}
		}
		#endregion HotTrackTabAppearance

		#region SelectedTabAppearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> for a selected tab.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabSettings_P_SelectedTabAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase SelectedTabAppearance
		{
			get
			{
				if (this.selectedTabAppearanceHolder == null)
				{
					this.selectedTabAppearanceHolder = new AppearanceHolder();

					this.selectedTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.selectedTabAppearanceHolder.Collection = this.Manager.Appearances;

				return this.selectedTabAppearanceHolder.Appearance;
			}

			set
			{
				if (this.selectedTabAppearanceHolder == null				|| 
					!this.selectedTabAppearanceHolder.HasAppearance		||
					value != this.selectedTabAppearanceHolder.Appearance)
				{
					if (null == this.selectedTabAppearanceHolder)
					{
						this.selectedTabAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.selectedTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.selectedTabAppearanceHolder.Collection = this.Manager.Appearances;

					this.selectedTabAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.SelectedTabAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="SelectedTabAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="SelectedTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasSelectedTabAppearance
		{
			get
			{
				return (null != this.selectedTabAppearanceHolder &&
					this.selectedTabAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="SelectedTabAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="SelectedTabAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="SelectedTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeSelectedTabAppearance()
		{
			return this.HasSelectedTabAppearance && this.selectedTabAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="SelectedTabAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="SelectedTabAppearance"/> property. If the <see cref="SelectedTabAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="SelectedTabAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="SelectedTabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetSelectedTabAppearance()
		{
			if ( this.HasSelectedTabAppearance )
			{
				this.selectedTabAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.SelectedTabAppearance );
			}
		}
		#endregion SelectedTabAppearance

		#region TabAppearance
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.Appearance"/> object used to format the tab.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedDescription("LD_MdiTabSettings_P_TabAppearance")]
		[LocalizedCategory("LC_Appearance")]
		public AppearanceBase TabAppearance
		{
			get
			{
				if (this.tabAppearanceHolder == null)
				{
					this.tabAppearanceHolder = new AppearanceHolder();

					this.tabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if (this.Manager != null)
					this.tabAppearanceHolder.Collection = this.Manager.Appearances;

				return this.tabAppearanceHolder.Appearance;
			}

			set
			{
				if (this.tabAppearanceHolder == null				|| 
					!this.tabAppearanceHolder.HasAppearance		||
					value != this.tabAppearanceHolder.Appearance)
				{
					if (null == this.tabAppearanceHolder)
					{
						this.tabAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						this.tabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if (this.Manager != null)
						this.tabAppearanceHolder.Collection = this.Manager.Appearances;

					this.tabAppearanceHolder.Appearance = value;

					this.NotifyPropChange( TabbedMdiPropertyIds.TabAppearance );

				}
			}

		}

		/// <summary>
		/// Indicates whether an <see cref="AppearanceBase"/> object has been created.
		/// </summary>
		/// <value>Returns True when the Appearance object for the <see cref="TabAppearance"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="AppearanceBase"/> object has been created.  Appearance objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="TabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasTabAppearance
		{
			get
			{
				return (null != this.tabAppearanceHolder &&
					this.tabAppearanceHolder.HasAppearance);
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="TabAppearance"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabAppearance"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>Appearance</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="TabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		protected bool ShouldSerializeTabAppearance()
		{
			return this.HasTabAppearance && this.tabAppearanceHolder.ShouldSerialize();
		}

		/// <summary>
		/// Resets the <see cref="TabAppearance"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the <see cref="TabAppearance"/> property. If the <see cref="TabAppearance"/> property was set to an Appearance from the <see cref="UltraTabbedMdiManager.Appearances"/> collection, the properties on the <see cref="Infragistics.Win.AppearanceBase"/> object in the collection will remain the same but the <see cref="TabAppearance"/> property will no longer be attached to that <see cref="Infragistics.Win.AppearanceBase"/>. To reset the properties of the associated <b>Appearance</b> object, the <see cref="Infragistics.Win.Appearance.Reset"/> method of the <b>Appearance</b> object itself should be invoked instead.</p>
		/// </remarks>
		/// <seealso cref="TabAppearance"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase"/>
		/// <seealso cref="Infragistics.Win.AppearanceBase.Reset"/>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetTabAppearance()
		{
			if ( this.HasTabAppearance )
			{
				this.tabAppearanceHolder.Reset();
				this.NotifyPropChange( TabbedMdiPropertyIds.TabAppearance );
			}
		}
		#endregion TabAppearance

		#region AllowClose
		/// <summary>
		/// Returns or sets whether the <see cref="MdiTab"/> may be closed.
		/// </summary>
		/// <remarks>
		/// <p class="body">An <see cref="MdiTab"/> can be closed 2 ways using the user interface of the <see cref="UltraTabbedMdiManager"/>. 
		/// One way is using the close button (appears as an "X" in the <see cref="MdiTabGroup"/>) and the other is via the context menu displayed when 
		/// right clicking on a tab. The close button displayed in the MdiTabGroup may be hidden by setting the <see cref="MdiTabGroupSettings.ShowCloseButton"/> to false but
		/// the option in the context menu will still be available. If the <b>AllowClose</b> resolves to false (true is the default value) the option in 
		/// the context menu will not be displayed and the close button (if displayed) will be disabled when the tab is the selected tab. Regardless of the 
		/// <b>AllowClose</b> setting, the tab may be closed using the <see cref="MdiTab.Close()"/> method.</p>
		/// </remarks>
		/// <seealso cref="TabCloseAction"/>
		/// <seealso cref="MdiTabSettingsResolved.AllowClose"/>
		/// <seealso cref="MdiTabGroupSettings.ShowCloseButton"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosing"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosed"/>
		[LocalizedDescription("LD_MdiTabSettings_P_AllowClose")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean AllowClose
		{
			get { return this.allowClose; }
			set
			{
				if (this.allowClose == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.allowClose = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.AllowClose );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AllowClose"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AllowClose"/> differs from the default value.</returns>
		/// <seealso cref="AllowClose"/>
		public bool ShouldSerializeAllowClose()
		{
			return this.AllowClose != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="AllowClose"/> property to its default value.
		/// </summary>
		/// <seealso cref="AllowClose"/>
		public void ResetAllowClose()
		{
			this.AllowClose = DefaultableBoolean.Default;
		}
		#endregion //AllowClose

		#region HotTrack
		/// <summary>
		/// Returns or sets whether the <see cref="MdiTab"/> will display using the <see cref="HotTrackTabAppearance"/> when the mouse is over the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>HotTrack</b> property determines if the <see cref="HotTrackTabAppearance"/> is applied to the tab 
		/// when the mouse enters the bounds of the tab item. By default, this value resolves to false.</p>
		/// </remarks>
		/// <seealso cref="HotTrackTabAppearance"/>
		/// <seealso cref="MdiTabSettingsResolved.HotTrack"/>
		[LocalizedDescription("LD_MdiTabSettings_P_HotTrack")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean HotTrack
		{
			get { return this.hotTrack; }
			set
			{
				if (this.hotTrack == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.hotTrack = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.HotTrack );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="HotTrack"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="HotTrack"/> differs from the default value.</returns>
		/// <seealso cref="HotTrack"/>
		public bool ShouldSerializeHotTrack()
		{
			return this.HotTrack != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="HotTrack"/> property to its default value.
		/// </summary>
		/// <seealso cref="HotTrack"/>
		public void ResetHotTrack()
		{
			this.HotTrack = DefaultableBoolean.Default;
		}
		#endregion //HotTrack

		#region AllowDrag
		/// <summary>
		/// Returns or sets whether the <see cref="MdiTab"/> may be repositioned.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>AllowDrag</b> property determines whether an <see cref="MdiTab"/> may be repositioned. By default, an MdiTab may be 
		/// repositioned within its containing <see cref="MdiTab.TabGroup"/> and also to the other <see cref="MdiTabGroup"/> objects in the 
		/// <see cref="UltraTabbedMdiManager.TabGroups"/>. When set to <b>WithinTabGroup</b>, the MdiTab may only be repositioned within its 
		/// containing TabGroup; it cannot be dragged to another TabGroup and cannot be used to create a new 
		/// <see cref="MdiTabGroup"/>. When set to <b>None</b>, the MdiTab cannot be dragged.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.AllowDrop"/>
		/// <seealso cref="MdiTabSettingsResolved.AllowDrag"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDragging"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDragOver"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDropped"/>
		[LocalizedDescription("LD_MdiTabSettings_P_AllowDrag")]
		[LocalizedCategory("LC_Behavior")]
		public MdiTabDragStyle AllowDrag
		{
			get { return this.allowDrag; }
			set
			{
				if (this.allowDrag == value)
					return;

				if (!Enum.IsDefined(typeof(MdiTabDragStyle), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(MdiTabDragStyle));

				this.allowDrag = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.AllowDrag );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="AllowDrag"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="AllowDrag"/> differs from the default value.</returns>
		/// <seealso cref="AllowDrag"/>
		public bool ShouldSerializeAllowDrag()
		{
			return this.AllowDrag != MdiTabDragStyle.Default;
		}	

		/// <summary>
		/// Resets the <see cref="AllowDrag"/> property to its default value.
		/// </summary>
		/// <seealso cref="AllowDrag"/>
		public void ResetAllowDrag()
		{
			this.AllowDrag = MdiTabDragStyle.Default;
		}
		#endregion //AllowDrag

		#region TabCloseAction
		/// <summary>
		/// Returns or sets what happens to the associated <see cref="System.Windows.Forms.Form"/> when the <see cref="MdiTab"/> is closed.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a tab is closed, using the <see cref="MdiTab.Close()"/> method or via the user interface (using the 
		/// close button or context menu), the <b>TabCloseAction</b> determines the action that will be taken on the <see cref="MdiTab.Form"/> associated 
		/// with the tab. By default the associated Form is closed. When a form is closed, the tab associated with the form is also released. If the form is 
		/// only hidden, the MdiTab is removed from the <see cref="MdiTab.TabGroup"/> it is associated with and added to the <see cref="UltraTabbedMdiManager.HiddenTabs"/> collection of 
		/// the <see cref="UltraTabbedMdiManager"/>. When set to <b>None</b>, no action is taken but the <see cref="UltraTabbedMdiManager.TabClosing"/> and 
		/// <see cref="UltraTabbedMdiManager.TabClosed"/> events are still invoked.</p>
		/// </remarks>
		/// <seealso cref="AllowClose"/>
		/// <seealso cref="MdiTabGroupSettings.ShowCloseButton"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosing"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosed"/>
		/// <seealso cref="MdiTabSettingsResolved.TabCloseAction"/>
		[LocalizedDescription("LD_MdiTabSettings_P_TabCloseAction")]
		[LocalizedCategory("LC_Behavior")]
		public MdiTabCloseAction TabCloseAction
		{
			get { return this.tabCloseAction; }
			set
			{
				if (this.tabCloseAction == value)
					return;

				if (!Enum.IsDefined(typeof(MdiTabCloseAction), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(MdiTabCloseAction));

				this.tabCloseAction = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabCloseAction );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabCloseAction"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabCloseAction"/> differs from the default value.</returns>
		/// <seealso cref="TabCloseAction"/>
		public bool ShouldSerializeTabCloseAction()
		{
			return this.TabCloseAction != MdiTabCloseAction.Default;
		}	

		/// <summary>
		/// Resets the <see cref="TabCloseAction"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabCloseAction"/>
		public void ResetTabCloseAction()
		{
			this.TabCloseAction = MdiTabCloseAction.Default;
		}
		#endregion //TabCloseAction

		#region TabWidth
		/// <summary>
		/// Returns or sets the <see cref="MdiTab"/> width when displayed as a fixed width tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>TabWidth</b> property is used to control the width of a tab item when 
		/// the <see cref="MdiTabGroupSettings.TabSizing"/> is set to either <b>Fixed</b> or <b>AutoSize</b>. 
		/// If the property does not resolve to a specific value and the <b>TabSizing</b> of the containing group 
		/// is set to <b>Fixed</b>, the tab defaults to the size required to show the image and text. The tab size is 
		/// also limited by the <see cref="MdiTabGroupSettings.MinTabWidth"/> and <see cref="MdiTabGroupSettings.MaxTabWidth"/>.</p>
		/// </remarks>
		/// <seealso cref="MdiTabGroupSettings.TabSizing"/>
		/// <seealso cref="MdiTabGroupSettings.MinTabWidth"/>
		/// <seealso cref="MdiTabGroupSettings.MaxTabWidth"/>
		/// <seealso cref="MdiTabSettingsResolved.TabWidth"/>
		[LocalizedDescription("LD_MdiTabSettings_P_TabWidth")]
		[LocalizedCategory("LC_Appearance")]
		public int TabWidth
		{
			get { return this.tabWidth; }
			set
			{
				if (this.tabWidth == value)
					return;

				if (value < -1)
					throw new ArgumentOutOfRangeException(Shared.SR.GetString(null, "LE_V2_Exception_8"), value, Shared.SR.GetString(null, "LE_V2_Exception_27"));

				this.tabWidth = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.TabWidth );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="TabWidth"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="TabWidth"/> differs from the default value.</returns>
		/// <seealso cref="TabWidth"/>
		public bool ShouldSerializeTabWidth()
		{
			return this.TabWidth != TabWidthDefault;
		}	

		/// <summary>
		/// Resets the <see cref="TabWidth"/> property to its default value.
		/// </summary>
		/// <seealso cref="TabWidth"/>
		public void ResetTabWidth()
		{
			this.TabWidth = TabWidthDefault;
		}
		#endregion //TabWidth

		#region DisplayFormIcon
		/// <summary>
		/// Returns or sets whether the <see cref="MdiTab"/> will display the associated <see cref="MdiTab.Form"/> icon if no other image is assigned to the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, an <see cref="MdiTab"/> will only display an image if one is specified on one of the tab appearances 
		/// (<see cref="TabAppearance"/>, <see cref="SelectedTabAppearance"/>, etc.). To have the tab display the Icon of the associated 
		/// <see cref="MdiTab.Form"/>, set the <b>DisplayForm</b> icon property to true.</p>
		/// </remarks>
		/// <seealso cref="TabAppearance"/>
		/// <seealso cref="SelectedTabAppearance"/>
		/// <seealso cref="ActiveTabAppearance"/>
		/// <seealso cref="HotTrackTabAppearance"/>
		/// <seealso cref="MdiTabSettingsResolved.DisplayFormIcon"/>
		[LocalizedDescription("LD_MdiTabSettings_P_DisplayFormIcon")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean DisplayFormIcon
		{
			get { return this.displayFormIcon; }
			set
			{
				if (this.displayFormIcon == value)
					return;

				if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
					throw new InvalidEnumArgumentException(Shared.SR.GetString(null, "LE_V2_Exception_8"), (int)value, typeof(DefaultableBoolean));

				this.displayFormIcon = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.DisplayFormIcon );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="DisplayFormIcon"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="DisplayFormIcon"/> differs from the default value.</returns>
		public bool ShouldSerializeDisplayFormIcon()
		{
			return this.DisplayFormIcon != DefaultableBoolean.Default;
		}	

		/// <summary>
		/// Resets the <see cref="DisplayFormIcon"/> property to its default value.
		/// </summary>
		public void ResetDisplayFormIcon()
		{
			this.DisplayFormIcon = DefaultableBoolean.Default;
		}
		#endregion //DisplayFormIcon
	
		// JJD 5/19/03 - WTB947
		// Added ForceSerialization as a workaround for a serialization
		// bug MS introdiced in V1.1 of Visual Studio
		#region ForceSerialization

		/// <summary>
		/// Internal property.
		/// </summary>
		/// <remarks>
		/// This property is used internally as a workaround for a serialization bug in Visual Studio that was introduced in version 1.1.
		/// </remarks>
		[ Browsable(false) ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		[ DefaultValue(false) ]
		public bool ForceSerialization
		{
			get	
			{ 
				// JJD 5/19/03 - WTB947
				// Return true if the toolbars manager is inherited
				// to prevent MS's v1.1 serialization bug from surfacing
				if ( this.Manager != null &&
					 this.Manager.IsDesignMode )
				{
					if ( this == this.Manager.TabSettings )
					{
						InheritanceAttribute inherit = Infragistics.Win.Utilities.GetInheritanceAttribute( this.Manager );

						if ( inherit != null &&
							 inherit.InheritanceLevel == InheritanceLevel.Inherited )
							return !this.ShouldSerialize();
					}
				}

				return false; 
			}

			set { } // Don't do anything on a set
		}

		#endregion ForceSerialization

		// MD 11/7/07 - NA 2008 Vol 1
		#region CloseButtonAlignment

		/// <summary>
		/// Gets or sets the alignment of the close button within the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This only applies if the <see cref="MdiTabGroupSettings.CloseButtonLocation"/> of the owning tab group is set to Tab. 
		/// </p>
		/// </remarks>
		/// <exception cref="InvalidEnumArgumentException">
		/// The value assigned is not defined in the <see cref="TabCloseButtonAlignment"/> enumeration.
		/// </exception>
		/// <seealso cref="MdiTabGroupSettings.CloseButtonLocation"/>
		[LocalizedDescription( "LD_MdiTabSettings_P_CloseButtonAlignment" )]
		[LocalizedCategory( "LC_Behavior" )]
		public TabCloseButtonAlignment CloseButtonAlignment
		{
			get { return this.closeButtonAlignment; }
			set
			{
				if ( this.closeButtonAlignment != value )
				{
					if ( Enum.IsDefined( typeof( TabCloseButtonAlignment ), value ) == false )
						throw new InvalidEnumArgumentException( "value", (int)value, typeof( TabCloseButtonAlignment ) );

					this.closeButtonAlignment = value;
					this.NotifyPropChange( TabbedMdiPropertyIds.CloseButtonAlignment );
				}
			}
		}

		/// <summary>
		/// Resets the <see cref="CloseButtonAlignment"/> property to its default value.
		/// </summary>
		/// <seealso cref="CloseButtonAlignment"/>
		public void ResetCloseButtonAlignment()
		{
			this.CloseButtonAlignment = TabCloseButtonAlignment.Default;
		}

		/// <summary>
		/// Indicates if the <see cref="CloseButtonAlignment"/> property should be serialized.
		/// </summary>
		/// <returns>Returns True if the <see cref="CloseButtonAlignment"/> differs from the default value.</returns>
		/// <seealso cref="CloseButtonAlignment"/>
		public bool ShouldSerializeCloseButtonAlignment()
		{
			return this.closeButtonAlignment != TabCloseButtonAlignment.Default;
		}

		#endregion TabCloseButtonAlignment

		#region CloseButtonVisibility

		/// <summary>
		/// Gets or sets the visibility of the close button within the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This only applies if the <see cref="MdiTabGroupSettings.CloseButtonLocation"/> of the owning tab group is set to Tab. 
		/// </p>
		/// </remarks>
		/// <exception cref="InvalidEnumArgumentException">
		/// The value assigned is not defined in the <see cref="TabCloseButtonVisibility"/> enumeration.
		/// </exception>
		/// <seealso cref="MdiTabGroupSettings.CloseButtonLocation"/>
		[LocalizedDescription( "LD_MdiTabSettings_P_CloseButtonVisibility" )]
		[LocalizedCategory( "LC_Behavior" )]
		public TabCloseButtonVisibility CloseButtonVisibility
		{
			get { return this.closeButtonVisibility; }
			set
			{
				if ( this.closeButtonVisibility != value )
				{
					if ( Enum.IsDefined( typeof( TabCloseButtonVisibility ), value ) == false )
						throw new InvalidEnumArgumentException( "value", (int)value, typeof( TabCloseButtonVisibility ) );

					this.closeButtonVisibility = value;
					this.NotifyPropChange( TabbedMdiPropertyIds.CloseButtonVisibility );
				}
			}
		}

		/// <summary>
		/// Indicates if the <see cref="CloseButtonVisibility"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="CloseButtonVisibility"/> differs from the default value.</returns>
		public bool ShouldSerializeCloseButtonVisibility()
		{
			return this.CloseButtonVisibility != TabCloseButtonVisibility.Default;
		}

		/// <summary>
		/// Resets the <see cref="CloseButtonVisibility"/> property to its default value.
		/// </summary>
		public void ResetCloseButtonVisibility()
		{
			this.CloseButtonVisibility = TabCloseButtonVisibility.Default;
		}
		#endregion CloseButtonVisibility

		#endregion //Properties

		#region Methods

		#region Public

		#region ShouldSerialize
		/// <summary>
		/// Indicates if any of the property values differ from the default value.
		/// </summary>
		/// <returns>True if the object needs to be serialized.</returns>
		public bool ShouldSerialize()
		{
			return this.ShouldSerializeTag()							||
				this.ShouldSerializeAllowClose()						||
				this.ShouldSerializeAllowDrag()							||
				this.ShouldSerializeHotTrack()							||
				this.ShouldSerializeTabCloseAction()					||
				this.ShouldSerializeTabWidth()							||
				this.ShouldSerializeActiveTabAppearance()				||
				this.ShouldSerializeHotTrackTabAppearance()				||
				this.ShouldSerializeTabAppearance()						||
				this.ShouldSerializeDisplayFormIcon()					||
				this.ShouldSerializeSelectedTabAppearance()				||

				// MD 11/7/07 - NA 2008 Vol 1
				this.ShouldSerializeCloseButtonAlignment()				||
				this.ShouldSerializeCloseButtonVisibility();
		}
		#endregion ShouldSerialize

		#region Reset
		/// <summary>
		/// Resets the object to its default state.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void Reset()
		{
			this.ResetActiveTabAppearance();
			this.ResetHotTrackTabAppearance();
			this.ResetTabAppearance();
			this.ResetSelectedTabAppearance();
			this.ResetTag();
			this.ResetAllowClose();
			this.ResetAllowDrag();
			this.ResetHotTrack();
			this.ResetTabCloseAction();
			this.ResetTabWidth();
			this.ResetDisplayFormIcon();

			// MD 11/7/07 - NA 2008 Vol 1
			this.ResetCloseButtonAlignment();
			this.ResetCloseButtonVisibility();
		}
		#endregion Reset

		#region ToString
		/// <summary>
		/// The string representation of the object.
		/// </summary>
		public override string ToString()
		{
			return string.Empty;
		}
		#endregion //ToString

		#endregion Public

		#region Internal

		#region InitializeOwner
		internal void InitializeOwner( object owner )
		{
			if (owner == this.owner)
				return;

			if (this.owner != null)
				Debug.Fail("An owner was already specified");

			this.owner = owner;
		}
		#endregion InitializeOwner

		#region InitializeFrom
		internal void InitializeFrom( MdiTabSettings source )
		{
			this.Reset();

			if (source == null)
				return;

			if (source.HasActiveTabAppearance)
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				Infragistics.Win.AppearanceBase app = this.ActiveTabAppearance;
				
				// initialize the appearance holder...
				this.activeTabAppearanceHolder.InitializeFrom( source.activeTabAppearanceHolder );
			}
			else if (this.HasActiveTabAppearance)
				this.activeTabAppearanceHolder.Reset();

			if (source.HasHotTrackTabAppearance)
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				Infragistics.Win.AppearanceBase app = this.HotTrackTabAppearance;
				
				// initialize the appearance holder...
				this.hotTrackTabAppearanceHolder.InitializeFrom( source.hotTrackTabAppearanceHolder );
			}
			else if (this.HasHotTrackTabAppearance)
				this.hotTrackTabAppearanceHolder.Reset();

			if (source.HasSelectedTabAppearance)
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				Infragistics.Win.AppearanceBase app = this.SelectedTabAppearance;
				
				// initialize the appearance holder...
				this.selectedTabAppearanceHolder.InitializeFrom( source.selectedTabAppearanceHolder );
			}
			else if (this.HasSelectedTabAppearance)
				this.selectedTabAppearanceHolder.Reset();

			if (source.HasTabAppearance)
			{
				// get the appearance property via the public interface to ensure
				// the holder is created and hooked up
				//
				Infragistics.Win.AppearanceBase app = this.TabAppearance;
				
				// initialize the appearance holder...
				this.tabAppearanceHolder.InitializeFrom( source.tabAppearanceHolder );
			}
			else if (this.HasTabAppearance)
				this.tabAppearanceHolder.Reset();

			this.allowClose = source.AllowClose;
			this.allowDrag = source.AllowDrag;
			this.hotTrack = source.HotTrack;
			this.tabWidth = source.TabWidth;
			this.tabCloseAction = source.TabCloseAction;
			this.displayFormIcon = source.displayFormIcon;

			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;
			else
				this.tagValue = null;

			// MD 11/7/07 - NA 2008 Vol 1
			this.closeButtonAlignment = source.closeButtonAlignment;
			this.closeButtonVisibility = source.closeButtonVisibility;
		}
		#endregion //InitializeFrom

		#endregion Internal

		#region Protected

		#region OnSubObjectPropChanged
		/// <summary>
		/// Invoked when a property on a subobject has changed.
		/// </summary>
		/// <param name="propChange">Object providing information about the property change</param>
		protected override void OnSubObjectPropChanged(Infragistics.Shared.PropChangeInfo propChange)
		{
			// pass along property change notifications.

			if (this.HasActiveTabAppearance && propChange.Source == this.activeTabAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.ActiveTabAppearance, propChange);
			else if (this.HasHotTrackTabAppearance && propChange.Source == this.hotTrackTabAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.HotTrackTabAppearance, propChange);
			else if (this.HasTabAppearance && propChange.Source == this.tabAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.TabAppearance, propChange);
			else if (this.HasSelectedTabAppearance && propChange.Source == this.selectedTabAppearanceHolder.RootAppearance)
				this.NotifyPropChange(TabbedMdiPropertyIds.SelectedTabAppearance, propChange);

			base.OnSubObjectPropChanged( propChange );
		}
		#endregion OnSubObjectPropChanged

		#region OnDispose
		/// <summary>
		/// Invoked when the resources utilized by the object are released and the object is being disposed.
		/// </summary>
		protected override void OnDispose()
		{
			this.owner = null;

			if (this.activeTabAppearanceHolder != null)
			{
				this.activeTabAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.activeTabAppearanceHolder.Reset();
				this.activeTabAppearanceHolder = null;
			}

			if (this.hotTrackTabAppearanceHolder != null)
			{
				this.hotTrackTabAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.hotTrackTabAppearanceHolder.Reset();
				this.hotTrackTabAppearanceHolder = null;
			}

			if (this.tabAppearanceHolder != null)
			{
				this.tabAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.tabAppearanceHolder.Reset();
				this.tabAppearanceHolder = null;
			}

			if (this.selectedTabAppearanceHolder != null)
			{
				this.selectedTabAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.selectedTabAppearanceHolder.Reset();
				this.selectedTabAppearanceHolder = null;
			}

			base.Dispose();
		}
		#endregion OnDispose

		// AS 10/15/03 DNF116
		#region GetObjectData
		/// <summary>
		/// Invoked during the serialization of the object.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		// MD 1/4/07 - FxCop
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
		}
		#endregion //GetObjectData

		// AS 10/15/03 DNF116
		#region OnDeserialization
		/// <summary>
		/// Invoked after the entire object graph has been deserialized.
		/// </summary>
		/// <param name="sender">The object that initiated the callback. The functionality for the this parameter is not currently implemented.</param>
		protected virtual void OnDeserialization(object sender)
		{
		}
		#endregion //OnDeserialization

		#endregion Protected

		#endregion Methods

		#region Interfaces

		#region IDeserializationCallback
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			if (this.activeTabAppearanceHolder!= null)
				this.activeTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if (this.hotTrackTabAppearanceHolder != null)
				this.hotTrackTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if (this.tabAppearanceHolder != null)
				this.tabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if (this.selectedTabAppearanceHolder != null)
				this.selectedTabAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// AS 10/15/03 DNF116
			// Call the virtual method so a derived class
			// could get the call.
			this.OnDeserialization(sender);
		}
		#endregion //IDeserializationCallback

		#region ISerializable
		// MD 1/4/07 - FxCop
		// We do not need a full demand here, just a link demand
		//[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinTabbedMdi";

			if (this.ShouldSerializeActiveTabAppearance())
				Utils.SerializeProperty(info, "ActiveTabAppearance", this.activeTabAppearanceHolder);

			if (this.ShouldSerializeAllowClose())
				Utils.SerializeProperty(info, "AllowClose", this.allowClose);

			if (this.ShouldSerializeAllowDrag())
				Utils.SerializeProperty(info, "AllowDrag", this.allowDrag);

			if (this.ShouldSerializeDisplayFormIcon())
				Utils.SerializeProperty(info, "DisplayFormIcon", this.displayFormIcon);

			if (this.ShouldSerializeHotTrack())
				Utils.SerializeProperty(info, "HotTrack", this.hotTrack);

			if (this.ShouldSerializeHotTrackTabAppearance())
				Utils.SerializeProperty(info, "HotTrackTabAppearance", this.hotTrackTabAppearanceHolder);

			if (this.ShouldSerializeSelectedTabAppearance())
				Utils.SerializeProperty(info, "SelectedTabAppearance", this.selectedTabAppearanceHolder);

			if (this.ShouldSerializeTabAppearance())
				Utils.SerializeProperty(info, "TabAppearance", this.tabAppearanceHolder);

			if (this.ShouldSerializeTabCloseAction())
				Utils.SerializeProperty(info, "TabCloseAction", this.tabCloseAction);

			if (this.ShouldSerializeTabWidth())
				Utils.SerializeProperty(info, "TabWidth", this.tabWidth);

			if (this.ShouldSerializeTag())
				this.SerializeTag(info);

			// MD 11/7/07 - NA 2008 Vol 1
			if ( this.ShouldSerializeCloseButtonAlignment() )
				Utils.SerializeProperty( info, "CloseButtonAlignment", this.closeButtonAlignment );

			if ( this.ShouldSerializeCloseButtonVisibility() )
				Utils.SerializeProperty( info, "CloseButtonVisibility", this.closeButtonVisibility );

			// AS 10/15/03 DNF116
			// Call the virtual method so derived classes
			// can serialize additional info.
			//
			this.GetObjectData(info, context);
		}
		#endregion //ISerializable

		#endregion //Interfaces
	}
}
