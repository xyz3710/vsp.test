#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Win.UltraWinTabs;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	internal class MdiChildSettings
	{
		#region Member Variables

		private Form				form = null;
		private bool				isInitialized = false;

		// JJD 8/31/06 - NA 2006 vol 3
		// Added manager backward reference
		private UltraTabbedMdiManager manager;
		
		// JJD 8/31/06 - NA 2006 vol 3
		// Added flag to keep track of whether the caption settings were cleared
		private bool				captionSettingsWereCleared;

		private bool				controlBox = false;
		private FormBorderStyle		formBorderStyle = FormBorderStyle.Sizable;
		private bool				maximizeBox = false;
		private bool				minimizeBox = false;
		private bool				helpButton = false;
		private Size				maximumSize = Size.Empty;
		private Size				minimumSize = Size.Empty;
		private Size				size = Size.Empty;
		// JJD 8/24/06 NA 2006 vol 3
		// save original location as well
		private Point				location = Point.Empty;
		private SizeGripStyle		sizeGripStyle = SizeGripStyle.Auto;
		private FormWindowState		windowState = FormWindowState.Normal;

		// AS 1/4/07 BR18569
		private Size				autoScrollMinSize = Size.Empty;

		#endregion //Member Variables

		#region Constructor

		// JJD 8/31/06 - NA 2006 vol 3
		// Added manager backward reference
		//internal MdiChildSettings(Form form)
		internal MdiChildSettings(Form form, UltraTabbedMdiManager manager)
		{
			this.form = form;
			this.manager = manager;
		}
		#endregion //Constructor
 
		#region Properties
    
		#region IsInitialized
		internal bool IsInitialized
		{
			get { return this.isInitialized; }
		}
		#endregion //IsInitialized

		// JJD 8/31/06 NA 2006 vol 3
		#region IsRoundedForm
		internal bool IsRoundedForm
		{
			get 
			{
				return ( this.manager != null &&
						 this.manager.ToolbarsManager != null &&
						 this.manager.ToolbarsManager.GetFormDisplayStyle(this.form) != Infragistics.Win.UltraWinToolbars.FormDisplayStyle.Standard);
			}
		}
		#endregion //IsRoundedForm

		#endregion //Properties

		#region Methods

		#region Initialize
		internal void Initialize()
		{
			if (this.isInitialized)
				return;

			this.controlBox = this.form.ControlBox;
			this.formBorderStyle = this.form.FormBorderStyle;
			this.maximizeBox = this.form.MaximizeBox;
			this.minimizeBox = this.form.MinimizeBox;

			this.helpButton = this.form.HelpButton;
			this.maximumSize = this.form.MaximumSize;
			this.minimumSize = this.form.MinimumSize;

			// AS 1/4/07 BR18569
			this.autoScrollMinSize = this.form.AutoScrollMinSize;

			this.size = this.form.Size;

			// JJD 8/29/06 - NA 2006 vol 3
			// save original location as well
			this.location = this.form.Location;

			// JJD 8/29/06 - NA 2006 vol 3
			// If the form is minimized or maximized get its restored size
			// by calling the GetWindowPlacement api.
			if (this.form.WindowState != FormWindowState.Normal && form.IsHandleCreated)
			{
				// Assert for the necessary permissions
				System.Security.PermissionSet pset = new System.Security.PermissionSet(null);

				pset.AddPermission(new SecurityPermission(SecurityPermissionFlag.UnmanagedCode));
				pset.AddPermission(new UIPermission(UIPermissionWindow.SafeTopLevelWindows));

				try
				{
					// 1st assert the rights
					pset.Assert();
					// Demand the rights since the Assert always succeeds
					pset.Demand();
					
					NativeWindowMethods.WINDOWPLACEMENT placement = new NativeWindowMethods.WINDOWPLACEMENT();

					if (NativeWindowMethods.GetWindowPlacementApi(form.Handle, ref placement))
					{
						Rectangle rect = placement.rcNormalPosition.Rect;

						this.location = rect.Location;
						this.size = rect.Size;
					}

					SecurityPermission.RevertAssert();
					UIPermission.RevertAssert();
				}
				catch (System.Security.SecurityException)
				{
				}
			}

			this.sizeGripStyle = this.form.SizeGripStyle;
			this.windowState = this.form.WindowState;

			this.isInitialized = true;

			this.form.SuspendLayout();

			// initialize the form settings
			if (this.form.StartPosition != FormStartPosition.Manual)
				this.form.StartPosition = FormStartPosition.Manual;

			// AS 1/4/07 BR18569
			// If the form had a minimum size set, then we need to clear
			// it out and set the autoscrollminsize based on the minimumsize
			//
			if (this.minimumSize.IsEmpty == false)
			{
				this.form.MinimumSize = Size.Empty;
				Size autoScrollMinSize;

				Size minSize = this.minimumSize;

				// we need to remove the non client area since that won't be there
				minSize.Width -= this.form.Width - this.form.ClientSize.Width;
				minSize.Height -= this.form.Height - this.form.ClientSize.Height;

				minSize.Width = Math.Max(0, minSize.Width);
				minSize.Height = Math.Max(0, minSize.Height);

				if (minSize.Height > 0 || minSize.Width > 0)
				{
					if (this.autoScrollMinSize.IsEmpty)
						autoScrollMinSize = minSize;
					else
					{
						int width = Math.Min(minSize.Width, this.autoScrollMinSize.Width);
						int height = Math.Min(minSize.Height, this.autoScrollMinSize.Height);
						autoScrollMinSize = new Size(width, height);
					}

					this.form.AutoScrollMinSize = autoScrollMinSize;
				}
			}
			
			// JJD 8/31/06 NA 2006 vol 3
			// Bypass updating the form properties on rounded forms
			// since that is already handled by the rounded form logic in
			if (!this.IsRoundedForm)
			{
				this.captionSettingsWereCleared = true;
				this.form.FormBorderStyle = FormBorderStyle.None;
				// AS 1/30/07 BR19113
				// We don't really have to change this setting and by changing
				// it, we're preventing from being closable using Ctrl-F4.
				//
				//this.form.ControlBox = false;
				this.form.HelpButton = false;
				this.form.MaximizeBox = false;
				this.form.MinimizeBox = false;
			}
			else
				this.captionSettingsWereCleared = false;

			// AS 3/2/05 BR02652
			// Setting this property seems to interfere with the mouse enter/leave
			// infrastructure. Since we don't really need to set this (it defaults to
			// true on a form and doesn't seem to affect things), we can just skip it.
			//
			//this.form.ShowInTaskbar = false;

			//			//this.form.MaximumSize = Size.Empty;
			//			//this.form.MinimumSize = Size.Empty;
			//			//this.form.Size = this.form.MinimumSize;
			this.form.SizeGripStyle = SizeGripStyle.Hide;

			if (this.form.WindowState != FormWindowState.Normal)
				this.form.WindowState = FormWindowState.Normal;

			// AS 7/1/03 WTB1080
			// We need to let the mdi child form 
			// process the pending layout changes.
			//
			//this.form.ResumeLayout(false);
			this.form.ResumeLayout(true);
		}
		#endregion //Initialize

		#region Restore
		internal void Restore()
		{
			if (!this.isInitialized)
				return;

			// AS 4/16/03 WTB799
			// Don't reset the settings when the form is being disposed.
			// Apparantly this causes repaint issues in Everett.
			//
			if (this.form.IsDisposed || this.form.Disposing)
				return;

			this.isInitialized = false;

			this.form.SuspendLayout();

			// JJD 8/31/06 NA 2006 vol 3
			// Bypass updating the form properties on rounded forms
			// since that is already handled by the rounded form logic in
			if (this.captionSettingsWereCleared)
			{
				this.captionSettingsWereCleared = false;
				this.form.ControlBox = this.controlBox;
				this.form.FormBorderStyle = this.formBorderStyle;
				this.form.MaximizeBox = this.maximizeBox;
				this.form.MinimizeBox = this.minimizeBox;
				this.form.HelpButton = this.helpButton;
			}

			this.form.MaximumSize = this.maximumSize;
			this.form.MinimumSize = this.minimumSize;
			this.form.Size = this.size;
			this.form.SizeGripStyle = this.sizeGripStyle;

			// AS 1/4/07 BR18569
			// If there was a minimum size then we would have set the AutoScrollMinSize
			// so restore that value.
			//
			if (this.minimumSize.IsEmpty == false)
				this.form.AutoScrollMinSize = this.autoScrollMinSize;

			// JJD 8/29/06 - NA 2006 vol 3
			// restore the original location
			this.form.Location = this.location;

			this.form.WindowState = this.windowState;

			this.form.ResumeLayout(true);
		}
		#endregion //Restore

		#endregion //Methods
	}
}
