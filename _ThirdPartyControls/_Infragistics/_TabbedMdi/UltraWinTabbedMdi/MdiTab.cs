#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;

using System.Runtime.Serialization;
using Infragistics.Shared.Serialization;
using System.Security.Permissions;

using Infragistics.Win.UltraWinTabs;

using System.Diagnostics;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// A tab class used to represent an mdi child form in a tabbed mdi environment.
	/// </summary>
	/// <remarks>
	/// <p class="body">The <see cref="UltraTabbedMdiManager"/> creates an <b>MdiTab</b> for each 
	/// mdi child form in the <see cref="UltraTabbedMdiManager.MdiParent"/>. <b>MdiTab</b> objects 
	/// for hidden forms are maintained in the <see cref="UltraTabbedMdiManager.HiddenTabs"/> collection. 
	/// As they are displayed, the <b>MdiTab</b> objects are added to <see cref="MdiTabGroup"/> instances that
	/// will display the tabs.</p>
	/// <p class="body">The <see cref="Settings"/> property contains defaultable properties that can be used to 
	/// control the appearance and behavior of the tab. If the properties are not set on the tab's Settings, the 
	/// <see cref="MdiTabGroup.TabSettings"/> of the containing <see cref="TabGroup"/> are checked. Any properties 
	/// that are not specified at that level at resolved using the <see cref="UltraTabbedMdiManager.TabSettings"/>
	/// of the owning <see cref="Manager"/>.</p>
	/// <p class="body">By default, the tab will display the text from the associated <see cref="Form"/> and no image. The text can be 
	/// overriden using the <see cref="Text"/> property. The <see cref="TextResolved"/> property will return the text 
	/// that is displayed by the tab. An image can be assigned using the appearances of the <see cref="Settings"/> 
	/// or using the <see cref="MdiTabSettings.DisplayFormIcon"/>. The <b>DisplayFormIcon</b> is only used if no image is 
	/// specified when the appearance is resolved for the state of the tab.</p>
	/// </remarks>
	[Serializable()]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class MdiTab : KeyedSubObjectBase, 
		ISerializable, 
		IDeserializationCallback,
		ITabItem
	{
		#region Member Variables

		// members
		private MdiTabSettings			settings = null;
		private MdiTabSettingsResolved	settingsResolved = null;
		private Form					form = null;
		private object					persistedInfo = null;
		private string					text = null;
		private string					tooltip = null;

		// opaque data used by the TabManager via the ITabItem interface
		private object					tabItemTag = null;

		// owning tab group or manager
		private object					owner = null;

		// previous form property values
		private MdiChildSettings		formSettings = null;

		private Icon					icon = null;
		private Image					iconImage = null;
		private System.IO.Stream		iconStream = null;

		// AS 6/3/03
		private int						cachedIndex = -1;

		// AS 7/25/03 WTB1108
		private bool					isClosingTab = false;

		// MRS 4/12/04
		private AccessibleObject		accessibilityObject = null;

		// MD 1/7/08 - BR29406
		// We need to store the close reason on the class (instead of passing into a helper Close method that does the closing)
		// for backwards compatibility reasons.
		private MdiTabCloseReason currentCloseReason = MdiTabCloseReason.CloseMethod;

		#endregion //Member Variables

		#region Constructor
		// AS 4/22/03
		// The constructor does not need to be public and may
		// cause some confusion. I'll leave it protected so someone
		// can derive from it.

		/// <summary>
		/// Initializes a new <see cref="MdiTab"/>
		/// </summary>
		/// <param name="form">Associated <b>Form</b></param>
		internal protected MdiTab(Form form)
		{
			this.MdiChild = form;
		}

		/// <summary>
		/// Constructor used during deserialization to initialize a new <see cref="MdiTab"/> object
		/// with the serialized property values.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected MdiTab(SerializationInfo info, StreamingContext context)
		{
			foreach( SerializationEntry entry in info )
			{
				switch (entry.Name)
				{
					case "Key":
						this.Key = (string)Utils.DeserializeProperty(entry, typeof(string), this.Key);
						break;
					case "Settings":
						this.settings = (MdiTabSettings)Utils.DeserializeProperty(entry, typeof(MdiTabSettings), null);
						break;
					case "PersistedInfo":
						this.persistedInfo = Utils.DeserializeObjectProperty(entry);
						break;
					case "ToolTip":
						this.tooltip = (string)Utils.DeserializeProperty(entry, typeof(string), this.tooltip);
						break;
					case "Text":
						this.text = (string)Utils.DeserializeProperty(entry, typeof(string), this.text);
						break;
					case MdiTab.TagSerializationName:
						this.DeserializeTag(entry);
						break;
					default:
						// AS 4/30/03 FxCop Change
						// Explicitly call the overload that takes an IFormatProvider
						//
						//System.Diagnostics.Debug.Assert(false, string.Format("Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						System.Diagnostics.Debug.Assert(false, string.Format(null, "Unaccounted for serialization entry - {0} [{1}]", entry.Name, entry.Value));
						break;
				}
			}
		}
		#endregion //Constructor

		#region Properties

		#region Public

		#region TabGroup
		/// <summary>
		/// Returns the owning <see cref="MdiTabGroup"/> or null (Nothing in VB), if the tab is not associated with a particular <b>MdiTabGroup</b>.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public MdiTabGroup TabGroup
		{
			get 
			{ 
				return this.owner as MdiTabGroup;
			}
		}
		#endregion //TabGroup

		#region Settings
		/// <summary>
		/// Returns an <see cref="MdiTabSettings"/> instance used to control the settings for this <b>MdiTab</b>
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Settings</b> property returns an instance of an <see cref="Infragistics.Win.UltraWinTabbedMdi.MdiTabSettings"/> object. 
		/// This object has properties that affect the appearance and behavior of the tab. Properties not explicitly set on the <b>Settings</b> 
		/// are resolved using the <see cref="MdiTabGroup.TabSettings"/> of the containing <see cref="TabGroup"/>. Any remaining unresolved 
		/// settings are resolved using the <see cref="UltraTabbedMdiManager.TabSettings"/> of the owning <see cref="Manager"/>.</p>
		/// </remarks>
		/// <seealso cref="SettingsResolved"/>
		/// <seealso cref="MdiTabGroup.Settings"/>
		/// <seealso cref="UltraTabbedMdiManager.TabSettings"/>
		/// <seealso cref="MdiTabSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public MdiTabSettings Settings
		{
			get
			{
				if (this.settings == null)
				{
					this.settings = new MdiTabSettings(this);
					this.settings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.settings;
			}
		}

		/// <summary>
		/// Indicates whether the <see cref="Settings"/> property contains data that needs to be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="Settings"/> property needs to be serialized.</returns>
		/// <remarks>
		/// <p class="body">Use this method to determine if an <b>MdiTabSettings</b> object has been created and contains data that needs to be serialized.</p>
		/// </remarks>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabSettings"/>
		protected bool ShouldSerializeSettings()
		{
			return this.settings != null && this.settings.ShouldSerialize();
		}

		/// <summary>
		/// Indicates whether an <see cref="MdiTabSettings"/> object has been created.
		/// </summary>
		/// <value>Returns True when the <b>MdiTabSettings</b> object for the <see cref="Settings"/> property has been created.</value>
		/// <remarks>
		/// <p class="body">Use this property to determine if an <see cref="MdiTabSettings"/> object has been created.  <b>MdiTabSettings</b> objects in general are not created until the properties associated with them are accessed.</p>
		/// </remarks>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public bool HasSettings
		{
			get { return this.settings != null; }
		}

		/// <summary>
		/// Resets the <see cref="Settings"/> property to its default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to reset the properties of the <see cref="MdiTabSettings"/> maintained by the <see cref="Settings"/> property.</p>
		/// </remarks>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabSettings"/>
		/// <seealso cref="MdiTabSettings.Reset"/>
		public void ResetSettings()
		{
			if (this.HasSettings)
				this.Settings.Reset();
		}
		#endregion //Settings

		#region SettingsResolved
		/// <summary>
		/// Returns an object providing the resolved values for the <see cref="Settings"/> property.
		/// </summary>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabGroup.TabSettings"/>
		/// <seealso cref="UltraTabbedMdiManager.TabSettings"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public MdiTabSettingsResolved SettingsResolved
		{
			get
			{
				if (this.settingsResolved == null)
					this.settingsResolved = new MdiTabSettingsResolved(this);

				return this.settingsResolved;
			}
		}
		#endregion //SettingsResolved

		#region Form
		/// <summary>
		/// Returns the associated <see cref="Form"/>
		/// </summary>
		[ReadOnly(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public Form Form
		{
			get { return this.form; }
		}
		#endregion //Form

		#region Manager
		/// <summary>
		/// Returns the owning <see cref="UltraTabbedMdiManager"/>
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public UltraTabbedMdiManager Manager
		{
			get 
			{ 
				if (this.owner is MdiTabGroup)
					return ((MdiTabGroup)this.owner).Manager;

				return this.owner as UltraTabbedMdiManager;
			}
		}
		#endregion //Manager

		#region IsFormActive
		/// <summary>
		/// Returns true if the <see cref="MdiTab"/> is associated with the active form
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsFormActive
		{
			get 
			{ 
				UltraTabbedMdiManager manager = this.Manager;

				return manager == null ? false : this == manager.ActiveTab;
			}
		}
		#endregion //IsFormActive

		#region IsFormEnabled
		/// <summary>
		/// Returns true if the associated <see cref="Form"/> is enabled.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsFormEnabled
		{
			get 
			{ 
				System.Windows.Forms.Form form = this.Form;

				return form == null ? false : form.Enabled;
			}
		}
		#endregion //IsFormEnabled

		#region IsFormVisible
		/// <summary>
		/// Returns true if the associated <see cref="Form"/> is visible.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsFormVisible
		{
			get 
			{ 
				System.Windows.Forms.Form form = this.Form;

				return form == null ? false : form.Visible && !form.IsDisposed;
			}
		}
		#endregion //IsFormVisible

		#region PersistedInfo
		/// <summary>
		/// Returns or sets additional information to persist when the MdiTab object is persisted.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <see cref="UltraTabbedMdiManager.SaveAsXml(System.IO.Stream)"/> or the 
		/// <see cref="UltraTabbedMdiManager.SaveAsBinary(System.IO.Stream)"/> method is invoked to save the layout 
		/// and properties of the tabs and tabgroups, the <see cref="UltraTabbedMdiManager.StoreTab"/> 
		/// is invoked for each tab managed by the <b>UltraTabbedMdiManager</b>. It is recommended that 
		/// you set this property in that event to a serializable object (e.g. string) that contains 
		/// enough information to create the appropriate form when deserialized. When the <see cref="UltraTabbedMdiManager.LoadFromXml(System.IO.Stream)"/> or 
		/// <see cref="UltraTabbedMdiManager.LoadFromBinary(System.IO.Stream)"/> method is invoked, the <see cref="UltraTabbedMdiManager.RestoreTab"/> 
		/// event is invoked. The event provides the deserialized <see cref="MdiTab"/> so you may 
		/// access the serialized <b>PersistedInfo</b> and create the appropriate Form.</p>
		/// <p class="note">Note: This property is not used by the MdiTab but exists to aid in deserialization. If set, 
		/// the value must be serializable (see <see cref="Type.IsSerializable"/>).</p>
		/// </remarks>
		/// <seealso cref="UltraTabbedMdiManager.StoreTab"/>
		/// <seealso cref="UltraTabbedMdiManager.RestoreTab"/>
		/// <seealso cref="UltraTabbedMdiManager.SaveAsBinary(System.IO.Stream)"/>
		/// <seealso cref="UltraTabbedMdiManager.SaveAsXml(System.IO.Stream)"/>
		/// <seealso cref="UltraTabbedMdiManager.LoadFromBinary(System.IO.Stream)"/>
		/// <seealso cref="UltraTabbedMdiManager.LoadFromXml(System.IO.Stream)"/>
		/// <seealso cref="Type.IsSerializable"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[Browsable(false)]
		public object PersistedInfo
		{
			get 
			{
				return this.persistedInfo; 
			}
			set 
			{ 
				if (value == this.persistedInfo)
					return;

				if (value != null)
					System.Diagnostics.Debug.Assert(value.GetType().IsSerializable, "The specified 'PersistedInfo' is not serializable.");

				this.persistedInfo = value; 

				this.NotifyPropChange( TabbedMdiPropertyIds.PersistedInfo );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="PersistedInfo"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="PersistedInfo"/> differs from the default value.</returns>
		/// <seealso cref="PersistedInfo"/>
		protected bool ShouldSerializePersistedInfo()
		{
			return this.PersistedInfo != null;
		}	

		/// <summary>
		/// Resets the <see cref="PersistedInfo"/> property to its default value.
		/// </summary>
		/// <seealso cref="PersistedInfo"/>
		public void ResetPersistedInfo()
		{
			this.PersistedInfo = null;
		}
		#endregion //PersistedInfo

		#region IsSelected
		/// <summary>
		/// Returns true if the tab is the currently selected tab in the associated <see cref="TabGroup"/>.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsSelected
		{
			get 
			{ 
				MdiTabGroup tabGroup = this.TabGroup;

				if (tabGroup == null)
					return false;

				return tabGroup.SelectedTab == this;
			}
		}
		#endregion //IsSelected

		#region Text
		/// <summary>
		/// Returns or sets the caption for the tab. If set to null, the forms caption is displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, the tab displays the <see cref="Control.Text"/> of the associated <see cref="Form"/>. If the 
		/// <b>Text</b> property is set, that value is used instead. The <see cref="TextResolved"/> value can be used to 
		/// determine the resolved text that is displayed by the tab.</p>
		/// </remarks>
		/// <seealso cref="TextResolved"/>
		public string Text
		{
			get { return this.text; }
			set
			{
				if (value == this.text)
					return;

				this.text = value;

				// AS 4/28/03
				// When we are associated with a tab group, we should
				// dirty the text info.
				//
				MdiTabGroup tabGroup = this.TabGroup;

				// update tab text & dirty text size
				if (tabGroup != null)
					tabGroup.DirtyTabItem(this, true, true, false);

				this.NotifyPropChange( TabbedMdiPropertyIds.Text );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="Text"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="Text"/> differs from the default value.</returns>
		/// <seealso cref="Text"/>
		protected bool ShouldSerializeText()
		{
			return this.Text != null;
		}

		/// <summary>
		/// Resets the <see cref="Text"/> property to its default value.
		/// </summary>
		/// <seealso cref="Text"/>
		public void ResetText()
		{
			this.Text = null;
		}

		/// <summary>
		/// Returns the text to display in the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <see cref="Text"/> property has been specified for the 
		/// tab, that value is used. If it is not set and the <see cref="Form"/> implements 
		/// the <see cref="ICustomMdiTab"/> interface, then the <see cref="ICustomMdiTab.Text"/> 
		/// value is used. Lastly, the <see cref="Control.Text"/> of the <b>Form</b> is used.</p>
		/// </remarks>
		/// <seealso cref="Text"/>
		/// <seealso cref="ICustomMdiTab"/>
		/// <seealso cref="ICustomMdiTab.Text"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public string TextResolved
		{
			get
			{
				if (this.Text != null)
					return this.Text;

				string text = null;

				if (this.IsCustomTab)
					text = this.CustomMdiTab.Text;

				if (text != null)
					return text;

				return this.Form == null ? string.Empty : this.Form.Text;
			}
		}
		#endregion //Text

		#region ToolTip
		/// <summary>
		/// Returns or sets the tooltip text associated with the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default, there is no tooltip assigned for a tab. However, when there isn't 
		/// enough room to completely display the text for the tab, the <see cref="TextResolved"/> will be 
		/// displayed in a tooltip. The <see cref="UltraTabbedMdiManager.ShowToolTips"/> is used to 
		/// determine whether any tooltips are displayed.</p>
		/// </remarks>
		/// <seealso cref="UltraTabbedMdiManager.ShowToolTips"/>
		/// <seealso cref="TextResolved"/>
		/// <seealso cref="ToolTipResolved"/>
		public string ToolTip
		{
			get 
			{ 
				return this.tooltip; 
			}
			set
			{
				if (this.tooltip == value)
					return;

				this.tooltip = value;

				this.NotifyPropChange( TabbedMdiPropertyIds.ToolTip );
			}
		}

		/// <summary>
		/// Indicates if the <see cref="ToolTip"/> property should be serialized.
		/// </summary>
		/// <returns>Returns true if the <see cref="ToolTip"/> differs from the default value.</returns>
		/// <seealso cref="ToolTip"/>
		protected bool ShouldSerializeToolTip()
		{
			return this.ToolTip != null;
		}

		/// <summary>
		/// Resets the <see cref="ToolTip"/> property to its default value.
		/// </summary>
		/// <seealso cref="ToolTip"/>
		public void ResetToolTip()
		{
			this.ToolTip = null;
		}

		/// <summary>
		/// Returns the resolved tooltip used for the tab.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <see cref="ToolTip"/> property has been specified for the 
		/// tab, that value is used. If it is not set and the <see cref="Form"/> implements 
		/// the <see cref="ICustomMdiTab"/> interface, then the <see cref="ICustomMdiTab.ToolTip"/> 
		/// value is used. Lastly, if there is not enough room in the tab to display the 
		/// <see cref="TextResolved"/>, that value is displayed when the mouse hovers over the tab.</p>
		/// </remarks>
		/// <seealso cref="ToolTip"/>
		/// <seealso cref="UltraTabbedMdiManager.ShowToolTips"/>
		/// <seealso cref="ICustomMdiTab"/>
		/// <seealso cref="ICustomMdiTab.ToolTip"/>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public string ToolTipResolved
		{
			get
			{
				if (this.ToolTip != null)
					return this.ToolTip;

				if (this.IsCustomTab)
					return this.CustomMdiTab.ToolTip;

				return null;
			}
		}
		#endregion //ToolTip

		#region UIElement
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.UIElement"/> representing the tab or null if the element is not displayed.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public UIElement UIElement
		{
			get
			{
				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null)
					return manager.GetUIElement(this);

				return null;
			}
		}
		#endregion //UIElement

		#region IsCustomTab
		/// <summary>
		/// Indicates if the form implements <see cref="ICustomMdiTab"/>
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool IsCustomTab
		{
			get { return this.CustomMdiTab != null; }
		}
		#endregion //IsCustomTab

		// AS 6/3/03
		// There should be an easier way to get the index of a tab.
		//
		#region Index
		/// <summary>
		/// Returns the index of the <b>MdiTab</b> in its parent collection.
		/// </summary>
		/// <seealso cref="KeyedSubObjectsCollectionBase.IndexOf(string)"/>
		/// <seealso cref="MdiTabGroup.Tabs"/>
		/// <seealso cref="UltraTabbedMdiManager.HiddenTabs"/>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int Index
		{
			get 
			{
				MdiTabsBaseCollection collection = null;

				if (this.TabGroup != null)
					collection = this.TabGroup.Tabs;
				else if (this.Manager != null)
					collection = this.Manager.HiddenTabs;

				if (collection == null)
					this.cachedIndex = -1;
				else if (this.cachedIndex < 0				||
					this.cachedIndex >= collection.Count	||
					collection[this.cachedIndex] != this)
				{
					this.cachedIndex = collection.IndexOf(this);
				}

				return this.cachedIndex;
			}
		}
		#endregion //Index

		#endregion //Public

		#region Internal

		#region MdiChild
		internal Form MdiChild
		{
			get { return this.form; }
			set 
			{
				if (value == this.form)
					return;

				this.UnhookForm();

				this.form = value;

				// AS 1/5/04 WTB1271
				if (this.form == null)
					this.formSettings = null;

				this.HookForm();
			}
		}
		#endregion //MdiChild

		#region IsFormInitialized
		internal bool IsFormInitialized
		{
			get { return this.formSettings != null && this.formSettings.IsInitialized; }
		}
		#endregion //IsFormInitialized

		#region CustomMdiTab
		internal ICustomMdiTab CustomMdiTab
		{
			get { return this.Form as ICustomMdiTab; }
		}
		#endregion //CustomMdiTab

		#region CanActivate
		internal bool CanActivate
		{
			get
			{
				return this.form != null	&& 
					this.form.Visible		&&
					!this.form.Disposing	&&
					!this.form.IsDisposed;
			}
		}
		#endregion //CanActivate

		#endregion //Internal

		#region Protected

		#region UIRole
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> that represents the tab item.
		/// </summary>
		internal protected Infragistics.Win.AppStyling.UIRole UIRole
		{
			get 
			{
				StyleUtils.Role roleID;

				if (this.TabGroup == null)
					roleID = StyleUtils.Role.TabItemHorzTop;
				else
				{
					switch (this.TabGroup.SettingsResolved.TabOrientation)
					{
						default:
						case TabOrientation.TopLeft:
						case TabOrientation.TopRight:
							roleID = StyleUtils.Role.TabItemHorzTop;
							break;
						case TabOrientation.BottomLeft:
						case TabOrientation.BottomRight:
							roleID = StyleUtils.Role.TabItemHorzBottom;
							break;
						case TabOrientation.LeftTop:
						case TabOrientation.LeftBottom:
							roleID = StyleUtils.Role.TabItemVertLeft;
							break;
						case TabOrientation.RightTop:
						case TabOrientation.RightBottom:
							roleID = StyleUtils.Role.TabItemVertRight;
							break;
					}
				}

				return StyleUtils.GetRole(this.Manager, roleID );
			}
		}
		#endregion //UIRole

		#endregion //Protected

		#region Private

		#region IsHotTrackTab
		private bool IsHotTrackTab
		{
			get
			{
				if (this.TabGroup != null)
					return this.TabGroup.HotTrackTab == this;

				return false;
			}
		}
		#endregion //IsHotTrackTab

		#region DoesFormHaveIcon
		private bool DoesFormHaveIcon
		{
			get { return this.form != null && this.form.Icon != null; }
		}
		#endregion //DoesFormHaveIcon

		#region FormIcon
		internal Image FormIcon
		{
			get
			{
				this.CreateFormIcon();

				return this.iconImage;
			}
		}
		#endregion //FormIcon
 
		#region IsVertical
		private bool IsVertical
		{
			get { return this.TabGroup != null && this.TabGroup.IsVertical; }
		}
		#endregion //IsVertical

		#endregion //Private

		#endregion //Properties

		#region Methods

		#region Public

		#region Activate
		/// <summary>
		/// Activates the associated Mdi child <see cref="Form"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body">Activating an <see cref="MdiTab"/> will cause it to be selected and will 
		/// update the <see cref="MdiTabGroup.SelectedTab"/> of the owning <see cref="TabGroup"/>.</p>
		/// <p class="note">Note: A tab's <see cref="Form"/> must be visible in order to activate the tab since the <b>Activate</b>
		/// method results in activating the <b>Form</b></p>
		/// </remarks>
		public virtual void Activate()
		{
			// AS 6/27/03 WTB1073
			// Don't check the is active flag since the form may be logically the
			// active mdi child but may not have the input focus. If someone calls
			// this method we should active the mdi child again.
			//
			// if its already active, exit
			//if (this.IsFormActive || this.Form == null || this.Manager == null)
			if (this.Form == null || this.Manager == null)
				return;

			// let the manager activate the form so we cancel if needed
			bool isSelected = this.IsSelected;

			this.Manager.ActivateMdiChild(this, !isSelected, !isSelected, true, true);
		}
		#endregion //Activate

		#region Close
		/// <summary>
		/// Closes the tab and associated Mdi child form.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoking the <b>Close</b> method will first invoke the 
		/// <see cref="UltraTabbedMdiManager.TabClosing"/> event. If this event is not cancelled, 
		/// the action specified by the <see cref="MdiTabSettings.TabCloseAction"/> is performed and 
		/// then the <see cref="UltraTabbedMdiManager.TabClosed"/> event is invoked.</p>
		/// </remarks>
		/// <seealso cref="MdiTabSettingsResolved.TabCloseAction"/>
		/// <seealso cref="MdiTabSettings.AllowClose"/>
		/// <seealso cref="MdiTabGroupSettings.ShowCloseButton"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosing"/>
		/// <seealso cref="UltraTabbedMdiManager.TabClosed"/>
		public virtual void Close()
		{
			if (this.Form == null)
				return;

			// AS 7/25/03 WTB1108
			// Keep a flag so we know we're trying to close the tab.
			this.isClosingTab = true;

			try
			{
				UltraTabbedMdiManager manager = this.Manager;

				Debug.Assert(manager != null, "An 'MdiTab' should always have an associated UltraTabbedMdiManager if it is associated with a form.");

				// invoke the before event assuming we have access to a tab manager
				if (manager != null)
				{
					// MD 1/7/08 - BR29406
					// Use the derived event args instead
					//CancelableMdiTabEventArgs cancelArgs = new CancelableMdiTabEventArgs(this);
					MdiTabClosingEventArgs cancelArgs = new MdiTabClosingEventArgs( this, this.currentCloseReason );

					manager.InvokeEvent(TabbedMdiEventIds.TabClosing, cancelArgs);

					if (cancelArgs.Cancel)
						return;
				}

				// AS 11/11/05 BR07018
				bool ensureClosed = false;

				if (this.IsCustomTab)
					this.CustomMdiTab.Close();
				else
				{
					switch(this.SettingsResolved.TabCloseAction)
					{
						default:
						case MdiTabCloseAction.Default:
						case MdiTabCloseAction.Close:
							// AS 11/11/05 BR07018
							ensureClosed = true;

							// AS 11/9/07 BR26741/BR28363
							// Its possible that the tab is closed/removed from within the Closing event
							// so make sure we have a form reference.
							//
							if (null != this.Form)
								this.Form.Close();
							break;
						case MdiTabCloseAction.Hide:

							// AS 11/9/07 BR26741/BR28363
							// Its possible that the tab is closed/removed from within the Closing event
							// so make sure we have a form reference.
							//
							if (null != this.Form)
								this.Form.Hide();
							break;
						case MdiTabCloseAction.None:
							// nothing to do
							break;
					}
				}

				// invoke the after event
				// AS 10/18/05 BR07018
				// We cannot assume that the form was actually closed. If we don't 
				// have a reference to the form or the form has been disposed then
				// we can assume we can fire the event.
				//
				//if (manager != null)
				// AS 11/11/05 BR07018
				// Actually my initial fix was incorrect. We could be firing the close but 
				// are just hiding the form in which case the following check is incorrect
				// as we'd only have the form disposed or unreferenced if we called the Close
				// method.
				//
				//if (manager != null && (this.Form == null || this.Form.IsDisposed))
				if (manager != null && (this.Form == null || this.Form.IsDisposed || ensureClosed == false))
				{
					// MD 1/7/08 - BR29406
					// Use the derived event args instead
					//MdiTabEventArgs args = new MdiTabEventArgs(this);
					MdiTabClosedEventArgs args = new MdiTabClosedEventArgs( this, this.currentCloseReason );

					manager.InvokeEvent(TabbedMdiEventIds.TabClosed, args);
				}
			}
			finally
			{
				// AS 7/25/03 WTB1108
				// Reset the flag.
				this.isClosingTab = false;
			}
		}
		#endregion //Close

		#region EnsureTabInView
		/// <summary>
		/// Scrolls the <see cref="MdiTab"/> into view in the containing <see cref="TabGroup"/>
		/// </summary>
		/// <seealso cref="MdiTabGroup.Scroll(Infragistics.Win.UltraWinTabs.ScrollType)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroupScrolling"/>
		/// <seealso cref="UltraTabbedMdiManager.TabGroupScrolled"/>
		public virtual void EnsureTabInView()
		{
			if (this.TabGroup != null)
				this.TabGroup.TabManager.EnsureTabItemInView(this);
		}
		#endregion //EnsureTabInView

		// AS 5/22/07 BR23151 - MdiTabNavigationMode 
		#region GetNextTab
		/// <summary>
		/// Returns the next visible tab. If there are no more visible tabs within the <see cref="TabGroup"/>, the first tab from the next <see cref="MdiTabGroup"/> will be returned.
		/// </summary>
		/// <param name="wrap">If true and this is the last visible tab in the last <see cref="MdiTabGroup"/>, the first <see cref="MdiTab"/> in the first MdiTabGroup will be returned; otherwise null is returned.</param>
		/// <returns>The next visible tab, either within this <see cref="TabGroup"/> or within the next <see cref="MdiTabGroup"/>.</returns>
		public MdiTab GetNextTab(bool wrap)
		{
			return GetNextPreviousTab(wrap, true);
		} 
		#endregion //GetNextTab

		// AS 5/22/07 BR23151 - MdiTabNavigationMode 
		#region GetPreviousTab
		/// <summary>
		/// Returns the previous visible tab. If there are no more visible tabs within the <see cref="TabGroup"/>, the last tab from the previous <see cref="MdiTabGroup"/> will be returned.
		/// </summary>
		/// <param name="wrap">If true and this is the first visible tab in the first <see cref="MdiTabGroup"/>, the last <see cref="MdiTab"/> in the last MdiTabGroup will be returned; otherwise null is returned.</param>
		/// <returns>The previous visible tab, either within this <see cref="TabGroup"/> or within the previous <see cref="MdiTabGroup"/>.</returns>
		public MdiTab GetPreviousTab(bool wrap)
		{
			return GetNextPreviousTab(wrap, false);
		} 
		#endregion //GetPreviousTab

		#region MoveToGroup
		/// <summary>
		/// Moves the tab to the end of the specified <see cref="MdiTabGroup"/>
		/// </summary>
		/// <param name="relativePosition">Relative position used to determine which tab group to which the tab should be moved.</param>
		/// <returns>Returns <b>True</b> if the tab was repositioned. Otherwise <b>False</b> is returned.</returns>
		/// <seealso cref="MoveToNewGroup()"/>
		/// <seealso cref="UltraTabbedMdiManager.MoveToGroup(MdiTab, MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoved"/>
		public bool MoveToGroup(MdiTabGroupPosition relativePosition)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_5"));

			return manager.MoveToGroup(this, relativePosition);
		}

		/// <summary>
		/// Moves the tab to the end of the specified <see cref="MdiTabGroup"/>
		/// </summary>
		/// <param name="group">TabGroup to contain the tab</param>
		/// <returns>Returns <b>True</b> if the tab was repositioned. Otherwise <b>False</b> is returned.</returns>
		/// <seealso cref="MoveToNewGroup()"/>
		/// <seealso cref="UltraTabbedMdiManager.MoveToGroup(MdiTab, MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoved"/>
		public bool MoveToGroup(MdiTabGroup group)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_5"));

			return manager.MoveToGroup(this, group);
		}

		/// <summary>
		/// Moves the tab to the end of the specified <see cref="MdiTabGroup"/>
		/// </summary>
		/// <param name="group">TabGroup to contain the tab</param>
		/// <param name="tabIndex">Index at which the tab should be positioned in the tab group.</param>
		/// <returns>Returns <b>True</b> if the tab was repositioned. Otherwise <b>False</b> is returned.</returns>
		/// <seealso cref="MoveToNewGroup()"/>
		/// <seealso cref="UltraTabbedMdiManager.MoveToGroup(MdiTab, MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoved"/>
		public bool MoveToGroup(MdiTabGroup group, int tabIndex)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_5"));

			return manager.MoveToGroup(this, group, tabIndex);
		}
		#endregion //MoveToGroup

		#region MoveToNewGroup
		/// <summary>
		/// Moves the tab to a new <see cref="MdiTabGroup"/> created at the end of the <see cref="UltraTabbedMdiManager.TabGroups"/>
		/// </summary>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <seealso cref="MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.MoveToNewGroup(MdiTab)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoved"/>
		public MdiTabGroup MoveToNewGroup()
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_5"));

			return manager.MoveToNewGroup(this);
		}

		/// <summary>
		/// Moves the tab to a new <see cref="MdiTabGroup"/> that is created based on the specified relative position
		/// </summary>
		/// <param name="relativePosition">Enumeration indicating where the new <see cref="MdiTabGroup"/> should be inserted.</param>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <seealso cref="MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.MoveToNewGroup(MdiTab)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoved"/>
		public MdiTabGroup MoveToNewGroup(MdiTabGroupPosition relativePosition)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_5"));

			return manager.MoveToNewGroup(this, relativePosition);
		}

		// AS 3/9/05 NA 2005 Vol 2 - Nested TabGroups
		/// <summary>
		/// Moves the tab to a new <see cref="MdiTabGroup"/> that is created based on the specified relative position
		/// </summary>
		/// <param name="relativeTabGroup">The MdiTabGroup to which the new MdiTabGroup should be a sibling.</param>
		/// <param name="relativePosition">Enumeration indicating where the new <see cref="MdiTabGroup"/> should be inserted in relation to the <paramref name="relativeTabGroup"/>.</param>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <seealso cref="MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoved"/>
		public MdiTabGroup MoveToNewGroup(MdiTabGroup relativeTabGroup, RelativePosition relativePosition)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_5"));

			return manager.MoveToNewGroup(this, relativeTabGroup, relativePosition);
		}

		/// <summary>
		/// Moves the tab to a new <see cref="MdiTabGroup"/> that is created based on the specified relative position
		/// </summary>
		/// <param name="relativeTabGroup">The MdiTabGroup to which the new MdiTabGroup should be a sibling.</param>
		/// <param name="relativePosition">Enumeration indicating where the new <see cref="MdiTabGroup"/> should be inserted in relation to the <paramref name="relativeTabGroup"/>.</param>
		/// <param name="orientation">Indicates the orientation of the new mdi tabgroup. If this value differs from the orientation of the <paramref name="relativeGroup"/> 
		/// then a new group that contains both the <paramref name="relativeGroup"/> and the group to contain the specified <paramref name="tab"/> will also be created.</param>
		/// <returns>The new <see cref="MdiTabGroup"/></returns>
		/// <seealso cref="MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoved"/>
		public MdiTabGroup MoveToNewGroup(MdiTabGroup relativeTabGroup, RelativePosition relativePosition, Orientation orientation)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_5"));

			return manager.MoveToNewGroup(this, relativeTabGroup, relativePosition, orientation);
		}
		#endregion //MoveToNewGroup

		#region Reposition
		/// <summary>
		/// Repositions the tab relative to the specified <see cref="MdiTab"/>
		/// </summary>
		/// <param name="relativeTab">Tab used to determine where this tab will be repositioned with respect to.</param>
		/// <param name="relativePosition">Position relative to the <paramref name="relativeTab"/></param>
		/// <seealso cref="MoveToGroup(MdiTabGroup)"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoving"/>
		/// <seealso cref="UltraTabbedMdiManager.TabMoved"/>
		public void Reposition( MdiTab relativeTab, MdiTabPosition relativePosition )
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager == null)
				throw new InvalidOperationException(Shared.SR.GetString(null, "LE_V2_Exception_5"));

			manager.RepositionTab(this, relativeTab, relativePosition);
		}
		#endregion //Reposition

		#region Show
		/// <summary>
		/// Ensures that the tab and associated <see cref="Form"/> are not hidden.
		/// </summary>
		/// <seealso cref="UltraTabbedMdiManager.TabDisplaying"/>
		/// <seealso cref="UltraTabbedMdiManager.TabDisplayed"/>
		/// <seealso cref="UltraTabbedMdiManager.HiddenTabs"/>
		public virtual void Show()
		{
			if (this.Form == null)
				throw new NotSupportedException(Shared.SR.GetString(null, "LE_V2_Exception_6"));

			this.Form.Show();
		}
		#endregion //Show

		#region Reset
		/// <summary>
		/// Resets the properties of the <see cref="MdiTab"/> to their default values.
		/// </summary>
		public void Reset()
		{
			this.ResetKey();
			this.ResetSettings();
			this.ResetTag();
			this.ResetPersistedInfo();
			this.ResetText();
			this.ResetToolTip();
		}
		#endregion //Reset

		#region ShouldSerialize
		/// <summary>
		/// Indicates if the <see cref="MdiTab"/> should be serialized.
		/// </summary>
		/// <returns>Returns <b>true</b> if any of the properties of the object differ from their default values.</returns>
		public bool ShouldSerialize()
		{
			return this.ShouldSerializePersistedInfo()	||
				this.ShouldSerializeKey()				||
				this.ShouldSerializeSettings()			||
				this.ShouldSerializeText()				||
				this.ShouldSerializeToolTip()			||
				this.ShouldSerializeTag();
		}
		#endregion //ShouldSerialize

		#region ToString
		/// <summary>
		/// Returns the <see cref="TextResolved"/> and <see cref="KeyedSubObjectBase.Key"/>
		/// </summary>
        /// <returns>The <see cref="TextResolved"/> and <see cref="KeyedSubObjectBase.Key"/></returns>
		public override string ToString()
		{
			string text = this.TextResolved;

			bool hasKey = this.Key != null && this.Key.Length > 0;
			bool hasText = text != null && text.Length > 0;

			// AS 4/30/03 FxCop Change
			// Explicitly call the overload that takes an IFormatProvider
			//
			if (hasKey && hasText)
				return string.Format(null, "MdiTab Key='{0}', Text='{1}'", this.Key, this.TextResolved);
			else if (hasKey)
				return string.Format(null, "MdiTab Key='{0}'", this.Key);
			else if (hasText)
				return string.Format(null, "MdiTab Text='{0}'", this.TextResolved);
			else
				return "MdiTab";
		}
		#endregion //ToString

		#region ResolveAppearance (public)
		/// <summary>
		/// Resolves the current appearance for the <see cref="MdiTab"/>
		/// </summary>
		/// <param name="appearance">Appearance structure to update with the appearance information</param>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabSettings"/>
		public void ResolveAppearance( ref AppearanceData appearance )
		{
			AppearancePropFlags requestedProps = AppearancePropFlags.AllRenderAndCursor;

			this.ResolveAppearance(ref appearance, ref requestedProps);
		}

		/// <summary>
		/// Resolves the current appearance for the <see cref="MdiTab"/>
		/// </summary>
		/// <param name="appearance">Appearance structure to update with the appearance information</param>
		/// <param name="requestedProps">Appearance properties to resolve</param>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabSettings"/>
		public void ResolveAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			
			TabState tabState = this.TabGroup == null ? TabState.Normal : ((ITabProvider)this.TabGroup).GetTabItemState(this);

			this.ResolveAppearance(ref appearance, ref requestedProps, tabState);
		}
		#endregion //ResolveAppearance (public)

		#endregion //Public

		#region Protected Methods

		#region ResolveAppearance (protected)
		/// <summary>
		/// Resolves the appearance for the specified phase
		/// </summary>
		/// <param name="appearance">Appearance structure to update with the appearance information</param>
		/// <param name="requestedProps">Appearance properties to resolve</param>
		/// <param name="tabState">TabState to resolve</param>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabSettings"/>        
		protected void ResolveAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, TabState tabState )
		{
			MdiTabGroup tabGroup = this.TabGroup;

			bool hasSettings = this.HasSettings;
			bool hasGroupSettings = tabGroup != null ? tabGroup.HasTabSettings : false;
			bool hasManagerSettings = this.Manager != null ? this.Manager.HasTabSettings : false;

			MdiTabSettings settings = hasSettings ? this.Settings : null;
			MdiTabSettings groupSettings = hasGroupSettings ? tabGroup.TabSettings : null;
			MdiTabSettings managerSettings = hasManagerSettings ? this.Manager.TabSettings : null;
			
			bool isImageRequested = (requestedProps & AppearancePropFlags.Image) != 0;

			bool isCustomTab = this.IsCustomTab;
			bool isActive = (tabState & TabState.Active) != 0;

			// AS 4/21/06 AppStyling
			AppStyling.UIRole role = this.UIRole;
			AppStyling.ResolutionOrder order = StyleUtils.GetResolutionOrder(this.Manager);
			AppStyling.ResolutionOrderInfo orderInfo = new AppStyling.ResolutionOrderInfo(order);

			// resolve the active appearance
			if (isActive && requestedProps != 0)
			{
				if (isCustomTab)
				{
					// AS 4/21/06 AppStyling
					//this.CustomMdiTab.ResolveAppearance(TabState.Active, ref appearance, ref requestedProps);
					this.CustomMdiTab.ResolveAppearance(TabState.Active, ref appearance, ref requestedProps, role, order);
				}

				ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.Active, settings, groupSettings, managerSettings, tabGroup, tabState
					, role, orderInfo);
			}

			// resolve the hot track appearance
			if ( requestedProps != 0 && (tabState & TabState.Hot) != 0 )
			{
				if (isCustomTab)
				{
					// AS 4/21/06 AppStyling
					//this.CustomMdiTab.ResolveAppearance(TabState.Hot, ref appearance, ref requestedProps);
					this.CustomMdiTab.ResolveAppearance(TabState.Hot, ref appearance, ref requestedProps, role, order);
				}

				ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.HotTrack, settings, groupSettings, managerSettings, tabGroup, tabState
					, role, orderInfo);
			}

			// resolve the tab state appearance
			if ( requestedProps != 0 && (tabState & TabState.Selected) != 0 )
			{
				if (isCustomTab)
				{
					// AS 4/21/06 AppStyling
					//this.CustomMdiTab.ResolveAppearance(TabState.Selected, ref appearance, ref requestedProps);
					this.CustomMdiTab.ResolveAppearance(TabState.Selected, ref appearance, ref requestedProps, role, order);
				}

				ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.Selected, settings, groupSettings, managerSettings, tabGroup, tabState
					, role, orderInfo);
			}

			// resolve the default tab appearance
			if (requestedProps != 0)
			{
				if (isCustomTab)
				{
					// AS 4/21/06 AppStyling
					//this.CustomMdiTab.ResolveAppearance(TabState.Normal, ref appearance, ref requestedProps);
					this.CustomMdiTab.ResolveAppearance(TabState.Normal, ref appearance, ref requestedProps, role, order);
				}

				ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.Tab, settings, groupSettings, managerSettings, tabGroup, tabState
					, role, orderInfo);
			}

			// fill in gaps using "control" level defaults
			if (requestedProps != 0)
			{
				// there is no analogous phase for the final so just skip the style info
				orderInfo.UseStyleAfter = orderInfo.UseStyleBefore = false;

				ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.Final, settings, groupSettings, managerSettings, tabGroup, tabState
					, role, orderInfo);
			}

			// if we don't have an image but one is requested and we should
			// display the form image, do so now
			if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.Image)	&&
				this.SettingsResolved.DisplayFormIcon)
			{
				appearance.Image = this.FormIcon;

				if (appearance.Image != null)
					requestedProps &= ~AppearancePropFlags.Image;
			}
			else if (isImageRequested && this.iconImage != null)
			{
				// if an image was requested but we're not using the form
				// icon, be sure to dispose it
				this.DisposeIconImage();
			}

//			// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
//			// The default behavior of Visual Studio 2005 is to set the selected tab font to bold
//			if ( Utilities.ShouldSetAppearanceProperty( ref appearance, ref requestedProps, AppearancePropFlags.FontBold )  &&
//				this.Manager.ViewStyle == ViewStyle.VisualStudio2005  &&
//				(tabState & TabState.Selected) != 0 )
//
//			{
//				appearance.FontData.Bold = DefaultableBoolean.True;
//				requestedProps &= ~AppearancePropFlags.FontBold;
//			}
//
//			// JDN 8/09/04 NAS2004 Vol3 - VisualStudio2005 style
//			// The default font name for Visual Studio 2005 is "Tahoma"
//			if ( Utilities.ShouldSetAppearanceProperty( ref appearance, ref requestedProps, AppearancePropFlags.FontName )  &&
//				appearance.FontData.Name != "Tahoma"  &&
//				this.Manager.ViewStyle == ViewStyle.VisualStudio2005  )
//			{
//				appearance.FontData.Name = "Tahoma";
//				requestedProps &= ~AppearancePropFlags.FontName;
//			}

		}

		#endregion //ResolveAppearance (protected)

		#region ResolveAppearancePhase
		/// <summary>
		/// Resolves the appearance for the specified phase
		/// </summary>
		/// <param name="appearance">Appearance structure to update with the appearance information</param>
		/// <param name="requestedProps">Appearance properties to resolve</param>
		/// <param name="phase">Appearance phase to resolve</param>
		/// <param name="settings">Settings object to use during the resolution process</param>
		/// <seealso cref="Settings"/>
		/// <seealso cref="MdiTabSettings"/>
		protected static void ResolveAppearancePhase( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, AppearanceResolutionPhase phase, MdiTabSettings settings )
		{
			switch( phase )
			{
				case AppearanceResolutionPhase.Active:
					if (settings != null && settings.HasActiveTabAppearance)
						settings.ActiveTabAppearance.MergeData(ref appearance, ref requestedProps);
					break;

				case AppearanceResolutionPhase.HotTrack:
					if (settings != null && settings.HasHotTrackTabAppearance)
						settings.HotTrackTabAppearance.MergeData(ref appearance, ref requestedProps);
					break;
				case AppearanceResolutionPhase.Selected:
					if (settings != null && settings.HasSelectedTabAppearance)
						settings.SelectedTabAppearance.MergeData(ref appearance, ref requestedProps);
					break;
				case AppearanceResolutionPhase.Tab:
					if (settings != null && settings.HasTabAppearance)
						settings.TabAppearance.MergeData(ref appearance, ref requestedProps);
					break;
				case AppearanceResolutionPhase.Final:
					UltraTabbedMdiManager manager = settings == null ? null : settings.Manager;

					if ( manager != null && manager.HasAppearance )
						manager.Appearance.MergeData(ref appearance, ref requestedProps);
					break;
			}
		}
		#endregion //ResolveAppearancePhase

		#region OnSubObjectPropChanged
		/// <summary>
		/// Called when a property on a sub object has changed.
		/// </summary>
        /// <param name="propChangeInfo">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged(PropChangeInfo propChangeInfo) 
		{
			if (this.HasSettings && propChangeInfo.Source == this.settings)
				this.NotifyPropChange(TabbedMdiPropertyIds.Settings, propChangeInfo);

			base.OnSubObjectPropChanged(propChangeInfo);
		}
		#endregion //OnSubObjectPropChanged

		#region OnDispose (protected override)
		/// <summary>
		/// Invoked when the element is being disposed to allow the element to free
		/// any resources being used.
		/// </summary>
		protected override void OnDispose()
		{
			this.DisposeIconImage();

			// AS 1/5/04 WTB1271
			if (this.settings != null)
			{
				this.settings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.settings.Dispose();
				this.settings = null;
			}

			base.OnDispose();
		}
		#endregion //OnDispose (protected override)

		// AS 10/15/03 DNF116
		#region GetObjectData
		/// <summary>
		/// Invoked during the serialization of the object.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		// MD 1/4/07 - FxCop
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		protected virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
		}
		#endregion //GetObjectData

		// AS 10/15/03 DNF116
		#region OnDeserialization
		/// <summary>
		/// Invoked after the entire object graph has been deserialized.
		/// </summary>
		/// <param name="sender">The object that initiated the callback. The functionality for the this parameter is not currently implemented.</param>
		protected virtual void OnDeserialization(object sender)
		{
		}
		#endregion //OnDeserialization

		#endregion //Protected Methods

		#region Private

		#region Hook/Unhook Form
		private void HookForm()
		{
			if (this.form == null)
				return;

			this.form.EnabledChanged += new EventHandler( this.OnMdiChildEnabledChanged );
			this.form.VisibleChanged += new EventHandler( this.OnMdiChildVisibleChanged );
			this.form.TextChanged += new EventHandler( this.OnMdiChildTextChanged );

			// AS 4/28/03 WTB873
			this.form.LocationChanged += new EventHandler( this.OnMdiChildLocationChanged );
			this.form.SizeChanged += new EventHandler( this.OnMdiChildSizeChanged );

			// AS 7/25/03 WTB1108
			this.form.Closing += new CancelEventHandler( this.OnMdiChildClosing );
			this.form.Closed += new EventHandler( this.OnMdiChildClosed );

			// AS 10/9/03 WTB1193
			// There's a .net bug that if you right click on a control,
			// the control gets focus but the form does not become the
			// active mdi child.
			//
			this.form.Enter += new EventHandler( this.OnMdiChildEnter );
		}

		private void UnhookForm()
		{
			if (this.form == null)
				return;

			this.form.EnabledChanged -= new EventHandler( this.OnMdiChildEnabledChanged );
			this.form.VisibleChanged -= new EventHandler( this.OnMdiChildVisibleChanged );
			this.form.TextChanged -= new EventHandler( this.OnMdiChildTextChanged );

			// AS 4/28/03 WTB873
			this.form.LocationChanged -= new EventHandler( this.OnMdiChildLocationChanged );
			this.form.SizeChanged -= new EventHandler( this.OnMdiChildSizeChanged );

			// AS 7/25/03 WTB1108
			this.form.Closing -= new CancelEventHandler( this.OnMdiChildClosing );
			this.form.Closed -= new EventHandler( this.OnMdiChildClosed );

			// AS 10/9/03 WTB1193
			// There's a .net bug that if you right click on a control,
			// the control gets focus but the form does not become the
			// active mdi child.
			//
			this.form.Enter -= new EventHandler( this.OnMdiChildEnter );

			this.RestoreTabFormSettings();

			this.DisposeIconImage();
		}
		#endregion //Hook/Unhook Form

		#region MdiChild Form Events
		private void OnMdiChildEnabledChanged(object sender, EventArgs e)
		{
			MdiTabGroup tabGroup = this.TabGroup;

			// dirty the tab item so we pick up the enabled/disabled dirty state
			if (tabGroup != null)
				tabGroup.DirtyTabItem(this, true, false, false);
		} 

		private void OnMdiChildVisibleChanged(object sender, EventArgs e)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager != null)
				manager.MdiChildVisibilityChanged(this);
		}

		private void OnMdiChildTextChanged(object sender, EventArgs e)
		{
			MdiTabGroup tabGroup = this.TabGroup;

			// update tab text & dirty text size
			if (tabGroup != null)
				tabGroup.DirtyTabItem(this, true, true, false);
		}

		// AS 4/28/03 WTB873
		private void OnMdiChildLocationChanged(object sender, EventArgs e)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager != null)
				manager.ResetFormPosition(this);
		}

		// AS 4/28/03 WTB873
		private void OnMdiChildSizeChanged(object sender, EventArgs e)
		{
			UltraTabbedMdiManager manager = this.Manager;

			if (manager != null)
				manager.ResetFormPosition(this);
		}

		// AS 7/25/03 WTB1108
		private void OnMdiChildClosing(object sender, CancelEventArgs e)
		{
			// If we are closing the tab then we don't want to fire the 
			// TabClosing event again.
			if (this.isClosingTab)
				return;

			UltraTabbedMdiManager manager = this.Manager;

			// invoke the before event assuming we have access to a tab manager
			if (manager != null)
			{
				// MD 1/7/08 - BR29406
				// Use the derived event args instead
				//CancelableMdiTabEventArgs cancelArgs = new CancelableMdiTabEventArgs(this);
				MdiTabClosingEventArgs cancelArgs = new MdiTabClosingEventArgs( this, MdiTabCloseReason.FormClosed );

				manager.InvokeEvent(TabbedMdiEventIds.TabClosing, cancelArgs);

				// if the user cancels the tabclosing, lets cancel the 
				// mdi child closing
				if (cancelArgs.Cancel)
					e.Cancel = true;
			}
		}

		// AS 7/25/03 WTB1108
		private void OnMdiChildClosed(object sender, EventArgs e)
		{
			// If we are closing the tab then we don't want to fire the 
			// TabClosed event again.
			if (this.isClosingTab)
				return;

			// AS 11/11/05 BR07212
			// instead of firing the close now, we need to wait until
			// the form has been disposed. however, since we will unhook
			// from all events before that point and we won't have a 
			// reference to the manager any more, we'll need a separate
			// way to fire the tabclosed. we could waste space on each
			// mdi tab with extra variables but instead I decided to
			// create a helper class that will wait for the dispose
			// and fire the closed when needed.
			// MD 3/25/09 - TFS15825
			// This null check was incorrect.
			//if (this.Form == null && this.Form.IsDisposed)
			if ( this.Form != null && this.Form.IsDisposed )
			{
				UltraTabbedMdiManager manager = this.Manager;

				// invoke the after event
				if (manager != null)
				{
					// MD 1/7/08 - BR29406
					// Use the derived event args instead
					//MdiTabEventArgs args = new MdiTabEventArgs(this);
					MdiTabClosedEventArgs args = new MdiTabClosedEventArgs( this, MdiTabCloseReason.FormClosed );

					manager.InvokeEvent(TabbedMdiEventIds.TabClosed, args);
				}
			}
			else if (this.Manager != null)
			{
				new TabClosedHelper(this, this.Manager, sender as Form);
			}
		}

		// AS 10/9/03 WTB1193
		private void OnMdiChildEnter(object sender, EventArgs e)
		{
			System.Windows.Forms.Form f = sender as Form;

			// There's a .net bug that if you right click on a control,
			// the control gets focus but the form does not become the
			// active mdi child. If the form has focus but isn't the
			// active mdi child form of the mdi parent, then it should
			// be active. Technically .net should be doing this but 
			// they're not.
			//
			// MD 1/4/07 - FxCop
			// Refactored to use safe MdiParent getter
			//if (this.CanActivate		&&
			//    f.ContainsFocus			&& 
			//    f.IsMdiChild			&& 
			//    this.Manager != null	&&
			//    f.MdiParent.ActiveMdiChild != null && // AS 10/20/05 BR07203 - This could cause the TabSelecting/ed to fire twice for the initial mdi child.
			//    f != f.MdiParent.ActiveMdiChild)
			//{
			if ( this.CanActivate &&
				f.ContainsFocus &&
				f.IsMdiChild &&
				this.Manager != null )
			{
				Form mdiParent = Utilities.GetMdiParent( f );

				if ( mdiParent.ActiveMdiChild != null && f != mdiParent.ActiveMdiChild )
				{
					// don't go through the normal activate method
					// since technically the programmer cannot cancel
					// this - the form already has focus
					this.Manager.ActivateMdiChild(this, true, true, false, true);
				}
			}
		}
		#endregion //MdiChild Form Events

		#region ResolveAppearanceHelper
		// AS 4/27/06 AppStyling
		//private static void ResolveAppearanceHelper( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, AppearanceResolutionPhase phase, MdiTabSettings tabSettings, MdiTabSettings groupSettings, MdiTabSettings managerSettings, MdiTabGroup tabGroup, TabState tabState )
		private static void ResolveAppearanceHelper( ref AppearanceData appearance, ref AppearancePropFlags requestedProps, AppearanceResolutionPhase phase, MdiTabSettings tabSettings, MdiTabSettings groupSettings, MdiTabSettings managerSettings, MdiTabGroup tabGroup, TabState tabState, AppStyling.UIRole role, AppStyling.ResolutionOrderInfo order )
		{
			AppStyling.RoleState state = StyleUtils.GetRoleState(phase);

			if (order.UseStyleBefore && role != null)
				role.ResolveAppearance(ref appearance, ref requestedProps, state);

			if (order.UseControlInfo)
			{
				if (tabSettings != null)
					ResolveAppearancePhase(ref appearance, ref requestedProps, phase, tabSettings);

				if (groupSettings != null)
					ResolveAppearancePhase(ref appearance, ref requestedProps, phase, groupSettings);

				if (managerSettings != null)
					ResolveAppearancePhase(ref appearance, ref requestedProps, phase, managerSettings);
			}

			if (order.UseStyleAfter && role != null)
				role.ResolveAppearance(ref appearance, ref requestedProps, state);

			if (tabGroup != null)
				tabGroup.ResolveDefaultTabPhaseAppearance(ref appearance, ref requestedProps, phase, tabState);
		}
		#endregion //ResolveAppearanceHelper

		#region HasTabImage
		private bool HasTabImage( MdiTabSettings tabSettings )
		{
			if (tabSettings == null)
				return false;

			// AS 4/27/06 AppStyling
			// 
			AppStyling.ResolutionOrderInfo order = StyleUtils.GetResolutionOrderInfo(this.Manager);

			// we only want to consider the tab property images if 
			// we are allowed to use the appearance properties
			if (order.UseControlInfo)
			{
				// AS 6/2/03
				// If this is a custom tab, we need to check with the
				// custom tab info provider as well.
				if (this.IsCustomTab && this.CustomMdiTab.HasTabImage)
					return true;

				AppearancePropFlags imageFlag = AppearancePropFlags.Image;

				if (tabSettings.HasActiveTabAppearance && tabSettings.ActiveTabAppearance.HasPropertyBeenSet(imageFlag))
					return true;
				if (tabSettings.HasSelectedTabAppearance && tabSettings.SelectedTabAppearance.HasPropertyBeenSet(imageFlag))
					return true;
				if (tabSettings.HasHotTrackTabAppearance && tabSettings.HotTrackTabAppearance.HasPropertyBeenSet(imageFlag))
					return true;
				if (tabSettings.HasTabAppearance && tabSettings.TabAppearance.HasPropertyBeenSet(imageFlag))
					return true;
			}

			// if there is none, then check with the role if we are allowed to
			// use app stying info
			if (order.UseStyleBefore || order.UseStyleAfter)
			{
				AppStyling.UIRole role = this.UIRole;

				if (null != role)
				{
					if (role.HasAppearancePropertyBeenSet(AppStyling.RoleState.Normal, AppearancePropFlags.Image) ||
						role.HasAppearancePropertyBeenSet(AppStyling.RoleState.Active, AppearancePropFlags.Image) ||
						role.HasAppearancePropertyBeenSet(AppStyling.RoleState.HotTracked, AppearancePropFlags.Image) ||
						role.HasAppearancePropertyBeenSet(AppStyling.RoleState.Selected, AppearancePropFlags.Image))
						return true;
				}
			}

			return false;
		}
		#endregion //HasTabImage

		#region DisposeIconImage
		// AS 5/18/06 BR10993
		//private void DisposeIconImage()
		internal void DisposeIconImage()
		{
			this.icon = null;
			if (this.iconImage != null)
			{
				this.iconImage.Dispose();
				this.iconImage = null;
			}

			if (this.iconStream != null)
			{
				this.iconStream.Close();
				this.iconStream = null;
			}
		}
		#endregion //DisposeIconImage

		#region CreateFormIcon
		private void CreateFormIcon()
		{
			System.Windows.Forms.Form frm = this.form;

			// if we don't have a form, the icon is null, or the form is disposing/ed
			// then destroy the icon
			if (frm == null || frm.Icon == null || 
				frm.Disposing || frm.IsDisposed)
			{
				this.DisposeIconImage();
				return;
			}

			// if the icon is the same, leave the image alone
			if (frm.Icon == this.icon)
				return;

			// if the icon has changed, dispose the old one
			this.DisposeIconImage();

			this.icon = frm.Icon;

			// AS 5/18/06 BR10993
			// We may need to get the right size icon first.
			// I changed the references below from frm.Icon
			// to frmIcon.
			//
			Icon frmIcon = frm.Icon;

			Size imageSize = this.Manager != null ? this.Manager.ImageSize : UltraTabbedMdiManager.ImageSizeDefault;

			try
			{
				if (imageSize.Width > 0 && imageSize.Height > 0)
					frmIcon = new Icon(frmIcon, imageSize);
			}
			catch (Exception ex)
			{
				Debug.Fail("We were unable to create an icon from the form's image of the same size as the ImageSize.\n" + ex.Message);
			}

			// AS 8/2/02 WTB232
			// When doing a ToBitmap, we are getting back
			// a 32x32 icon (system icon size) so the image
			// is scaled down later/ to 16x16. When the
			// image editor opens an image it does it from
			// a stream. Lo and behold - it comes in at
			// 16x16.
			//
			//this.image = frm.Icon.ToBitmap();
			this.iconStream = new System.IO.MemoryStream();
			frmIcon.Save(this.iconStream);
			this.iconStream.Position = 0;

			// AS 1/2/03 WTB545
			// When the icon is created using Icon.FromHandle, it appears
			// that you cannot then save that icon to a stream and get the
			// image from that stream. In this case, we'll use ToBitmap
			// and scale the image as needed.
			//
			try
			{
				this.iconImage = Image.FromStream(this.iconStream);
			}
			catch (Exception ex)
			{
				// AS 4/30/03 FxCop Change
				// Eat argument exceptions since there was a problem
				// as mentioned above.
				//
				if ( !(ex is ArgumentException) )
					throw;

				// close the stream since we will not be using it anymore
				// 
				this.iconStream.Close();
				this.iconStream = null;

				// convert the icon to a bitmap
				Bitmap bitmap = frmIcon.ToBitmap();

				try
				{
					// if the size matches, just use the bitmap
					if (bitmap.Size == frmIcon.Size)
					{
						this.iconImage = bitmap;

						// AS 7/16/04 WTB1545
						// Since we are referencing the image we don't want to dispose 
						// it in the finally block below. We still want to dispose it
						// if we create it in the else block.
						//
						bitmap = null;
					}
					else
					{
						// otherwise, scale the image down based on the size
						// from the icon
						this.iconImage = DrawUtility.CreateScaledImage(bitmap, frmIcon.Size, new Rectangle(Point.Empty, bitmap.Size) );
					}
				}
				finally
				{
					// and dispose the temporary bitmap
					if (bitmap != null)
						bitmap.Dispose();
				}
			}
			finally
			{
				// AS 5/18/06 BR10993
				// If we created an icon with a different size
				// then dispose it now.
				//
				if (frmIcon != frm.Icon)
					frmIcon.Dispose();
			}
		}
		#endregion //CreateFormIcon

		#endregion //Private

		#region Internal

		// MD 1/7/08 - BR29406
		#region Close

		internal void Close( MdiTabCloseReason closeReason )
		{
			MdiTabCloseReason oldCloseReason = this.currentCloseReason;

			try
			{
				this.currentCloseReason = closeReason;
				this.Close();
			}
			finally
			{
				this.currentCloseReason = oldCloseReason;
			}
		}

		#endregion Close

		// AS 5/22/07 BR23151 - MdiTabNavigationMode 
		#region GetNextPreviousTab
		internal MdiTab GetNextPreviousTab(bool wrap, bool next)
		{
			MdiTab adjacentTab = null;
			MdiTabGroup tabGroup = this.TabGroup;

			if (null != tabGroup)
			{
				int tabIndex = this.Index + (next ? 1 : -1);

				if (tabIndex >= 0 && tabIndex < tabGroup.Tabs.Count)
					adjacentTab = tabGroup.Tabs[tabIndex];
				else if (next)
				{
					tabGroup = tabGroup.NextLeafTabGroup;

					if (tabGroup == null && wrap && this.Manager != null)
						tabGroup = UltraTabbedMdiManager.GetFirstLeafTabGroup(this.Manager.TabGroups);

					if (null != tabGroup)
						adjacentTab = tabGroup.Tabs[0];
				}
				else
				{
					tabGroup = tabGroup.PreviousLeafTabGroup;

					if (tabGroup == null && wrap && this.Manager != null)
						tabGroup = UltraTabbedMdiManager.GetLastLeafTabGroup(this.Manager.TabGroups);

					if (null != tabGroup)
						adjacentTab = tabGroup.Tabs[tabGroup.Tabs.Count - 1];
				}
			}

			return adjacentTab;
		}
		#endregion //GetNextPreviousTab

		#region ResolveTabFontAppearance
		internal static AppearanceData ResolveTabFontAppearance( MdiTabGroup tabGroup, TabState tabState, 
			MdiTabSettings groupSettings, MdiTabSettings managerSettings )
		{
			AppearanceData appearance = new AppearanceData();
			AppearancePropFlags requestedProps = AppearancePropFlags.FontData;

			Debug.Assert(tabGroup != null, "There should always be a tab group specified!");

			// AS 4/27/06 AppStyling
			AppStyling.UIRole role = tabGroup.UIRoleTabItemDefault;
			AppStyling.ResolutionOrderInfo order = StyleUtils.GetResolutionOrderInfo(tabGroup.Manager);

			// resolve the active appearance
			if ((tabState & TabState.Active) != 0)
			{
				ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.Active, null, groupSettings, managerSettings, tabGroup, tabState,
					role, order);
			}

			// resolve the hot track appearance
			if ( (tabState & TabState.Hot) != 0 )
			{
				ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.HotTrack, null, groupSettings, managerSettings, tabGroup, tabState,
					role, order);
			}

			// resolve the tab state appearance
			if ( (tabState & TabState.Selected) != 0 )
			{
				ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.Selected, null, groupSettings, managerSettings, tabGroup, tabState,
					role, order);
			}

			// resolve the default tab appearance
			ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.Tab, null, groupSettings, managerSettings, tabGroup, tabState,
				role, order);

			// there is no state associated with the final phase so skip the style info
			order.UseStyleAfter = order.UseStyleBefore = false;

			// fill in gaps using "control" level defaults
			ResolveAppearanceHelper(ref appearance, ref requestedProps, AppearanceResolutionPhase.Final, null, groupSettings, managerSettings, tabGroup, tabState,
				role, order);

			return appearance;
		}
		#endregion //ResolveTabFontAppearance

		#region InitializeOwner
		internal void InitializeOwner( object owner )
		{
			this.owner = owner;
		}
		#endregion //InitializeOwner

		#region InitializeTabFormSettings
		internal void InitializeTabFormSettings()
		{
			if (this.IsFormInitialized)
				return;

			System.Windows.Forms.Form mdiChild = this.MdiChild;

			Debug.Assert(mdiChild != null, "Attempting to initialize a null mdichild form.");

			if (mdiChild == null)
				return;

			Debug.Assert( this.Manager != null && this.Manager.Enabled, "A Disabled UltraTabbedMdiManager should not initialize the form settings for an mdichild until it is enabled.");

			if (this.formSettings == null)
				this.formSettings = new MdiChildSettings(mdiChild, this.Manager);

			this.formSettings.Initialize();
		}
		#endregion //InitializeTabFormSettings

		#region RestoreTabFormSettings
		internal void RestoreTabFormSettings()
		{
			if (!this.IsFormInitialized)
				return;

			// AS 11/3/06 BR17249
			// If the tabbed mdi manager is in the process of being disposed
			// we will not restore the form settings so if someone wanted to
			// dispose the tabbed mdimanager but not want to get rid of the 
			// mdi children then they would have to set the enabled property
			// of the mdi manager to false first.
			//
			if (this.Manager != null && this.Manager.IsDisposing)
				return;

			this.formSettings.Restore();
		}
		#endregion //RestoreTabFormSettings

		// AS 3/15/05 NA 2005 Vol 2 - Nested TabGroups
		#region IsDescendantOf
		internal bool IsDescendantOf(MdiTabGroup tabGroup)
		{
			MdiTabGroup tempGroup = this.TabGroup;

			while (tempGroup != null)
			{
				if (tempGroup == tabGroup)
					return true;

				tempGroup = tempGroup.Parent;
			}

			return false;
		}
		#endregion //IsDescendantOf

        // MRS v6.1 "Navigator"
        #region GetResolvedImage
        internal Image GetResolvedImage()
        {
            AppearanceData appData = new AppearanceData();
            AppearancePropFlags propFlags = AppearancePropFlags.Image;
            this.ResolveAppearance(ref appData, ref propFlags, TabState.Normal );

            return appData.GetImage(this.Manager.ImageList);
        }
        #endregion //GetResolvedImage

        #endregion //Internal

        #endregion //Methods

        #region Interfaces

        #region ISerializable
        // MD 1/4/07 - FxCop
		// We do not need a full demand here, just a link demand
		//[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinTabbedMdi";

			if (this.ShouldSerializeKey())
				Utils.SerializeProperty(info, "Key", this.Key);

			if (this.ShouldSerializeSettings())
				Utils.SerializeProperty(info, "Settings", this.Settings);

			// be sure to get it from the property get...
			if (this.ShouldSerializePersistedInfo())
				Utils.SerializeObjectProperty(info, "PersistedInfo", this.PersistedInfo);

			if (this.ShouldSerializeToolTip())
				Utils.SerializeProperty(info, "ToolTip", this.ToolTip);

			if (this.ShouldSerializeText())
				Utils.SerializeProperty(info, "Text", this.Text);

			if (this.ShouldSerializeTag())
				this.SerializeTag(info);

			// AS 10/15/03 DNF116
			// Call the virtual method so derived classes
			// can serialize additional info.
			//
			this.GetObjectData(info, context);
		}
		#endregion // ISerializable

		#region IDeserializationCallback
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			if (this.settings != null)
			{
				this.settings.InitializeOwner(this);
				this.settings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}

			// AS 10/15/03 DNF116
			// Call the virtual method so a derived class
			// could get the call.
			this.OnDeserialization(sender);
		}
		#endregion //IDeserializationCallback

		#region ITabItem

		void ITabItem.ResolveAppearance(Infragistics.Win.UltraWinTabs.TabState state, ref Infragistics.Win.AppearanceData appearance, ref Infragistics.Win.AppearancePropFlags requestedProps)
		{
			this.ResolveAppearance(ref appearance, ref requestedProps, state);
		}

		string ITabItem.ToolTipText
		{
			get
			{
				return this.ToolTipResolved;
			}
		}

		bool ITabItem.Visible
		{
			get
			{
				return this.IsFormVisible;
			}
		}

		object ITabItem.Tag
		{
			get
			{
				return this.tabItemTag;
			}
			set
			{
				this.tabItemTag = value;
			}
		}

		bool ITabItem.AllowMoving
		{
			get
			{
				return this.SettingsResolved.AllowDrag != MdiTabDragStyle.None;
			}
		}

		bool ITabItem.Enabled
		{
			get
			{
				return this.IsFormEnabled;
			}
		}

		Size ITabItem.ImageSize
		{
			get
			{
				UltraTabbedMdiManager manager = this.Manager;

				if (manager != null)
				{
					MdiTabGroup tabGroup = this.TabGroup;
					Size imageSize = manager.ImageSize;

					// AS 6/2/03
					// We should return the ImageSize if this is a custom
					// tab that will provide its own image.
					if (this.IsCustomTab && this.CustomMdiTab.HasTabImage)
						return imageSize;

					if (this.HasSettings && this.HasTabImage(this.Settings))
						return imageSize;
					else if (tabGroup != null && tabGroup.HasTabSettings && this.HasTabImage(tabGroup.TabSettings))
						return imageSize;
					else if (manager.HasTabSettings && this.HasTabImage(manager.TabSettings))
						return imageSize;

					// if we are displaying the form icon, then we have an image
					if (this.SettingsResolved.DisplayFormIcon && this.DoesFormHaveIcon)
						return imageSize;
				}

				return Size.Empty;
			}
		}

		int ITabItem.Index
		{
			get
			{
				
				return this.Index;
			}
		}

		bool ITabItem.HotTrack
		{
			get
			{
				return this.SettingsResolved.HotTrack;
			}
		}

		int ITabItem.FixedWidth
		{
			get
			{
				return this.SettingsResolved.TabWidth;
			}
		}

		string ITabItem.Text
		{
			get { return this.TextResolved; }
		}



		#endregion ITabItem

		#endregion //Interfaces

		#region AppearanceResolutionPhase enum
		/// <summary>
		/// Enumeration of phases during the resolution of the <see cref="MdiTab"/> object's appearance
		/// </summary>
		internal protected enum AppearanceResolutionPhase
		{
			/// <summary>
			/// For the <see cref="MdiTab"/> associated with the <see cref="System.Windows.Forms.Form.ActiveMdiChild"/>.
			/// </summary>
			Active,

			/// <summary>
			/// For the selected <see cref="MdiTab"/>
			/// </summary>
			Selected,

			/// <summary>
			/// For an <see cref="MdiTab"/> when the mouse is over the tab.
			/// </summary>
			HotTrack,

			/// <summary>
			/// Default appearance for the <see cref="MdiTab"/>
			/// </summary>
			Tab,

			/// <summary>
			/// Defaults for the <see cref="MdiTabGroup"/>
			/// </summary>
			Final,
		}
		#endregion //AppearanceResolutionPhase			

		// MRS 04/01/04 - Added Accessibility support
		#region AccessibilityObject property

		/// <summary>
		/// Returns the accessible object representing the tab.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public AccessibleObject AccessibilityObject
		{
			get
			{
				if ( this.accessibilityObject == null )
				{
					MdiTabGroup mdiTabGroup = this.TabGroup;

					if ( mdiTabGroup != null )
						this.accessibilityObject = mdiTabGroup.CreateTabAccessibilityInstance( this );
				}

				return this.accessibilityObject;
			}
		}

		#endregion AccessibilityObject property
		
		// AS 11/11/05 BR07212
		#region TabClosedHelper class
		private class TabClosedHelper
		{
			private MdiTab tab;
			private UltraTabbedMdiManager manager;
			private Form form;

			internal TabClosedHelper(MdiTab tab, UltraTabbedMdiManager manager, Form form)
			{
				this.tab = tab;
				this.manager = manager;
				this.form = form;

				this.form.Disposed += new EventHandler(this.OnMdiChildDisposed);
			}

			private void OnMdiChildDisposed(object sender, EventArgs e)
			{
				this.form.Disposed -= new EventHandler(this.OnMdiChildDisposed);

				// MD 1/7/08 - BR29406
				// Use the derived event args instead
				//this.manager.InvokeEvent(TabbedMdiEventIds.TabClosed, new MdiTabEventArgs(this.tab));
				this.manager.InvokeEvent( TabbedMdiEventIds.TabClosed, new MdiTabClosedEventArgs( this.tab, MdiTabCloseReason.FormClosed ) );
			}
		}
		#endregion //TabClosedHelper class
	}
}
