#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Win;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	/// <summary>
	/// Main UIElement for the <see cref="SplitterBarControl"/>
	/// </summary>
	public class SplitterControlUIElement : ControlUIElementBase, IMessageFilter
	{
		#region Member Variables

		private Cursor			lastDragCursor = null;
		private Rectangle		lastDragRect = Rectangle.Empty;
		private Point			offset = Point.Empty;
		private Point			lastPoint = Point.Empty;
		private Point			startPoint = Point.Empty;
		private bool			isDragging = false;
		private bool			messageFilterAdded = false;

		#endregion //Member Variables

		#region Constructors
		/// <summary>
		/// Initializes a new <see cref="SplitterControlUIElement"/>
		/// </summary>
		/// <param name="splitterBarControl">Associated <b>SplitterBarControl</b></param>
		public SplitterControlUIElement(SplitterBarControl splitterBarControl) : this(splitterBarControl, splitterBarControl)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="SplitterControlUIElement"/>
		/// </summary>
		/// <param name="splitterBarControl">Associated <b>SplitterBarControl</b></param>
		/// <param name="ultraControl">Owning <b>IUltraControl</b></param>
		public SplitterControlUIElement(SplitterBarControl splitterBarControl, IUltraControl ultraControl) : base(splitterBarControl, ultraControl)
		{
		}
		#endregion //Constructors

		#region Base class overrides

		#region DrawBorders

		/// <summary>
		/// Invoked when the border for the element should be drawn
		/// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBorders( ref UIElementDrawParams drawParams )
		{
			// AS 3/28/05 NA 2005 Vol 2 - Nested TabGroups
			// Modified logic to handle intersections. I originally tried
			// excluding the intersection from the region but gdi+ 
			// didn't properly split all the lines - particularly
			// when dealing with a 2 pixel wide border style.
			//
			if (this.SplitterControl.HasIntersections)
			{
				// handle special borderstyles...
				switch(this.BorderStyle)
				{
					case UIElementBorderStyle.Etched:
					case UIElementBorderStyle.Inset:
					case UIElementBorderStyle.InsetSoft:
					case UIElementBorderStyle.Raised:
					case UIElementBorderStyle.RaisedSoft:
						this.DrawComplexSplitterBorders(ref drawParams);
						return;

					// AS 4/14/05 
					// As a result of a change for BR03223
					// the borders on the left edge are not drawn
					// when we have a splitter intersection
					// and use the default draw borders impl.
					//
					case UIElementBorderStyle.Dotted:
					case UIElementBorderStyle.Dashed:
					case UIElementBorderStyle.Solid:
						this.DrawSimpleIntersectedSplitterBorders(ref drawParams);
						return;
				}
			}

			// handle basic border styles or when there are no
			// intersections...
			this.DrawSimpleSplitterBorders(ref drawParams);
		}
		#endregion //DrawBorders

		#region GetAdjustableCursor
		/// <summary>
		/// The adjustable cursor is used by any element that can be moved or
		/// resized. Returning null means the element can not be adjusted
		/// by clicking on the passed in point.
		/// </summary>
        /// <param name="point">The point that should be used to determine if the area is adjustable.</param>
        /// <returns>The cursor that should be used to represent an adjustable region.</returns>
		public override System.Windows.Forms.Cursor GetAdjustableCursor( System.Drawing.Point point )
		{
			if (this.isDragging && this.lastDragCursor != null)
				return this.lastDragCursor;

			return null;
		}
		#endregion //GetAdjustableCursor
		
		#region BorderStyle
			/// <summary>
			/// BorderStyle of the element.
			/// <see cref="UIElementBorderStyle"/>
			/// </summary>
			public override UIElementBorderStyle BorderStyle
		{
			get
			{
				if (this.SplitterControl != null)
					return this.SplitterControl.BorderStyle;

				return base.BorderStyle;
			}
			set 
			{
				base.BorderStyle = value;
			}
		}
		#endregion //BorderStyle

		#region BorderSides
		/// <summary>
		/// Flags indicating which sides of the element to
		/// draw borders.
		/// </summary>
		public override System.Windows.Forms.Border3DSide BorderSides
		{
			get
			{
				// JDN 8/12/04 NAS2004 Vol3 - Support for VisualStudio2005 style
				if ( this.SplitterControl.SuppressLeadingEdge )
				{
					if ( this.IsVertical )
						return Border3DSide.Right;
					else
						return Border3DSide.Bottom;
				}

				if ( this.IsVertical )
					return Border3DSide.Left | Border3DSide.Right;
				else
					return Border3DSide.Top | Border3DSide.Bottom;
			}
		}
		#endregion //BorderSides

		#region Adjustable
		/// <summary>
		/// Returns true if this element can be moved or resized by the mouse 
		/// </summary>
		public override bool Adjustable
		{
			get { return this.Enabled; }
		}
		#endregion //Adjustable

		#region PointInAdjustableArea
		/// <summary>
		/// Returns true if the passed in point is over an area of this
		/// element that would require the AdjustableCursor to be displayed
		/// </summary>
		/// <param name="point">In client coordinates</param>
        /// <returns>true if the passed in point is over an area of this element that would require the AdjustableCursor to be displayed.</returns>
		public override bool PointInAdjustableArea( System.Drawing.Point point )
		{
			return this.Rect.Contains(point);    
		}
		#endregion //PointInAdjustableArea
		
		#region OnMouseDown
		/// <summary>
		/// Called when the mouse down message is received over the element. 
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		/// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
		/// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
		/// <returns>If true then bypass default processing</returns>
		protected override bool OnMouseDown( MouseEventArgs e, 
			bool adjustableArea,
			ref UIElement captureMouseForElement )
		{
			if (e.Button == MouseButtons.Left)
			{
				if (this.InitiateDrag( new Point(e.X, e.Y), false ))
				{
					captureMouseForElement = this;
					return true;
				}
			}

			return base.OnMouseDown(e, adjustableArea, ref captureMouseForElement);
		}
		#endregion //OnMouseDown

		#region OnCaptureAborted
		/// <summary>
		/// Called when mouse capture has been aborted, for example, when the
		/// 'escape' key is pressed during a drag operation.
		/// </summary>
		protected override void OnCaptureAborted( )
		{
			base.OnCaptureAborted();
			
			this.ProcessEndDrag(true);
		}
		#endregion //OnCaptureAborted

		#region OnMouseMove
		/// <summary>
		/// Called on a mouse move message
		/// </summary>
		/// <param name="e">Mouse event arguments </param>
		protected override void OnMouseMove( MouseEventArgs e )
		{
			if (this.HasCapture)
				this.ProcessDragPoint( new Point(e.X, e.Y) );

			base.OnMouseMove(e);
		}
		#endregion //OnMouseMove

		#region OnMouseUp
		/// <summary>
		/// Called when the mouse is released over an element
		/// </summary>
		/// <param name="e">Mouse event arguments </param>
		/// <returns>Returning true will ignore the next click event</returns>
		protected override bool OnMouseUp( MouseEventArgs e )
		{
			if (this.isDragging)
				this.ProcessEndDrag(false);

			return base.OnMouseUp(e);
		}
		#endregion //OnMouseUp

		#region InitAppearance
		/// <summary>
		/// Initializes the appearance of splitter bar.
		/// </summary>
		/// <param name="appearance">Appearance struct to update</param>
		/// <param name="requestedProps">Appearance properties to resolve</param>
		protected override void InitAppearance( ref AppearanceData appearance, ref AppearancePropFlags requestedProps )
		{
			this.SplitterControl.ResolveAppearance(ref appearance, ref requestedProps);

			if (Utilities.ShouldSetAppearanceProperty(ref appearance, ref requestedProps, AppearancePropFlags.Cursor))
			{
				// AS 4/11/03 WTB798
				// Instead of using the .Net EW and NS cursors, we'll use
				// our own cursor resources that match what visual studio uses.
				//
				appearance.Cursor = this.SplitterControl.CurrentSplitterCursor;
				requestedProps &= ~AppearancePropFlags.Cursor;
			}

			base.InitAppearance(ref appearance, ref requestedProps);
		}
		#endregion //InitAppearance

		#endregion //Base class overrides

		#region Properties

		#region SplitterControl
		/// <summary>
		/// Returns the owning <see cref="SplitterBarControl"/>
		/// </summary>
		public SplitterBarControl SplitterControl
		{
			get { return this.Control as SplitterBarControl; }
		}
		#endregion //SplitterControl

		#region IsDragging
		internal bool IsDragging
		{
			get { return this.isDragging; }
		}
		#endregion //IsDragging

		#region IsVertical
		private bool IsVertical
		{
			get { return this.SplitterControl.Orientation == Orientation.Vertical; }
		}
		#endregion //IsVertical

		#region UIRole
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				// AS 5/5/06
				//return StyleUtils.GetRole(this.UltraControl, StyleUtils.Role.SplitterBar);
				return StyleUtils.GetRole(this.UltraControl, this.IsVertical
					? StyleUtils.Role.SplitterBarVert
					: StyleUtils.Role.SplitterBarHorz);
			}
		}
		#endregion //UIRole

		#endregion //Properties

		#region Methods

		#region Private

		#region InitiateDrag
		private bool InitiateDrag( Point clientPt, bool initializeCursorPosition )
		{
			if (this.isDragging)
				return false;

			CancelEventArgs args = new CancelEventArgs();

			this.SplitterControl.InvokeEvent(SplitterBarEventIds.SplitterDragging, args);

			if (args.Cancel)
				return false;

			if (initializeCursorPosition)
			{
				Rectangle rect = this.Rect;
				this.offset = new Point(rect.Width / 2, rect.Height / 2);

				System.Windows.Forms.Cursor.Position = this.Control.PointToScreen(clientPt);
			}
			else
			{
				Rectangle rect = this.Rect;
				this.offset = new Point(clientPt.X - rect.X, clientPt.Y - rect.Y);
			}

			// store the starting point in screen coordinates
			this.startPoint = this.Control.PointToScreen(clientPt);

			this.Control.Update();

			this.ProcessDragPoint(clientPt);

			this.isDragging = true;

			this.HookMessageFilter();

			return true;
		}
		#endregion //InitiateDrag

		#region Hook/UnhookMessageFilter
		private void HookMessageFilter()
		{
			if (this.messageFilterAdded)
				return;

			try
			{
				SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);

				perm.Assert();

				// AS 11/10/03 WTB538
				//Application.AddMessageFilter(this);
				Infragistics.Win.Utilities.AddMessageFilter(this);

				this.messageFilterAdded = true;
			}
			// AS 4/30/03 FxCop Change
			// Catch only the specific exception
			//
			//catch {}
			catch (System.Security.SecurityException) {}
		}

		private void UnhookMessageFilter()
		{
			if (!this.messageFilterAdded)
				return;

			try
			{
				SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);

				perm.Assert();

				// AS 11/10/03 WTB538
				//Application.RemoveMessageFilter(this);
				Infragistics.Win.Utilities.RemoveMessageFilter(this);

				this.messageFilterAdded = false;
			}
			// AS 4/30/03 FxCop Change
			// Catch only the specific exception
			//
			//catch {}
			catch (System.Security.SecurityException) {}
		}
		#endregion //Hook/UnhookMessageFilter

		#region ProcessDragPoint
		private void ProcessDragPoint( Point clientPt )
		{
			Point screenPt = this.Control.PointToScreen(clientPt);

			if (screenPt == this.lastPoint)
				return;
			
			Rectangle rect = this.Control.RectangleToScreen(this.Rect);

			if (this.IsVertical)
				rect.X += clientPt.X - this.offset.X;
			else
				rect.Y += clientPt.Y - this.offset.Y;

			SplitterMoveEventArgs moveArgs = new SplitterMoveEventArgs(rect, screenPt, this.startPoint);

			this.SplitterControl.InvokeEvent(SplitterBarEventIds.SplitterMove, moveArgs);

			this.lastDragCursor = moveArgs.DragCursor;

			this.InvertScreenRect(moveArgs.SplitRectangle);

			this.lastPoint = screenPt;
		}
		#endregion //ProcessDragPoint

		#region ProcessEndDrag
		private void ProcessEndDrag( bool cancelled )
		{
			if (!this.isDragging)
				return;

			this.isDragging = false;

			this.UnhookMessageFilter();

			this.lastDragCursor = null;

			this.InvertScreenRect(Rectangle.Empty);

			SplitterDraggedEventArgs e = new SplitterDraggedEventArgs(this.lastPoint, this.startPoint, cancelled);

			this.SplitterControl.InvokeEvent(SplitterBarEventIds.SplitterDragged, e);
		}
		#endregion //ProcessEndDrag

		#region InvertScreenRect
		private void InvertScreenRect( Rectangle screenRect )
		{
			if (screenRect == this.lastDragRect)
				return;

			if (!this.lastDragRect.IsEmpty)
				NativeWindowMethods.FillReversibleFrame(this.lastDragRect);

			if (screenRect.Size.IsEmpty)
				this.lastDragRect = Rectangle.Empty;
			else
			{
				NativeWindowMethods.FillReversibleFrame(screenRect);
				this.lastDragRect = screenRect;
			}
		}
		#endregion //InvertScreenRect

		// AS 3/28/05 NA 2005 Vol 2 - Nested TabGroups
		#region DrawSimpleSplitterBorders
		private void DrawSimpleSplitterBorders(ref UIElementDrawParams drawParams)
		{
			System.Drawing.Region oldRegion = null;
			UIElementBorderStyle borderStyle = this.BorderStyle;

			// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
			if (this.SplitterControl.HasIntersections)
			{
				oldRegion = drawParams.Graphics.Clip;

				foreach(Rectangle intersection in this.SplitterControl.IntersectionList)
					drawParams.Graphics.ExcludeClip(intersection);
			}

			try
			{
				Rectangle rect = this.Rect;

				if (this.IsVertical)
					rect.Inflate(0, 2);
				else
					rect.Inflate(2, 0);

				drawParams.DrawBorders(borderStyle, this.BorderSides, rect);
			}
			finally
			{
				// AS 3/25/05 NA 2005 Vol 2 - Nested TabGroups
				if (oldRegion != null)
				{
					drawParams.Graphics.Clip = oldRegion;
					oldRegion.Dispose();
				}
			}
		}
		#endregion //DrawSimpleSplitterBorders

		// AS 4/14/05 
		#region DrawSimpleIntersectedSplitterBorders
		private void DrawSimpleIntersectedSplitterBorders( ref UIElementDrawParams drawParams )
		{
			System.Diagnostics.Debug.Assert(this.SplitterControl.HasIntersections, "This method is intended to be used when there are intersections in a 3d based border style.");

			#region Setup

			// get the intersections
			Rectangle[] intersections = !this.SplitterControl.HasIntersections ? null : (Rectangle[])this.SplitterControl.IntersectionList.ToArray(typeof(Rectangle));

			Array.Sort(intersections, new RectangleComparer());

			Color borderColor = drawParams.AppearanceData.BorderColor;

			if (borderColor.IsEmpty)
				borderColor = drawParams.AppearanceData.ForeColor;

			Graphics g = drawParams.Graphics;
			Pen pen;
			Rectangle rect = this.Rect;
			bool isVertical = this.SplitterControl.Orientation == Orientation.Vertical;
			UIElementBorderStyle borderStyle = this.BorderStyle;

			#endregion //Setup

			#region Get Pens for Border
			switch(borderStyle)
			{
				case UIElementBorderStyle.Dotted:
					pen = drawParams.GetPen(DrawCache.PenType.Dotted, borderColor);
					break;
				case UIElementBorderStyle.Dashed:
					pen = drawParams.GetPen(DrawCache.PenType.Dashed, borderColor);
					break;
				case UIElementBorderStyle.Solid:
					pen = drawParams.GetPen(DrawCache.PenType.Solid, borderColor);
					break;
				default:
				{
					System.Diagnostics.Debug.Assert(false, "Unexpected borderstyle!");
					return;
				}
			}
			#endregion //Get Pens for Border

			#region Draw Borders skipping intersections
			// iterate the intersections and draw the borders
			const int NearIndex = 0;

			// do 2 passes - one for the near (left/top) and one for far (right/bottom)
			for(int i = 0; i < 2; i++)
			{
				Rectangle startRect = rect;
				int sidePos = isVertical ? 
					(i == NearIndex ? startRect.Left : startRect.Right) :
					(i == NearIndex ? startRect.Top : startRect.Bottom);

				foreach(Rectangle intersection in intersections)
				{
					int comparisonPos = isVertical ? 
						(i == NearIndex ? intersection.Left : intersection.Right) :
						(i == NearIndex ? intersection.Top : intersection.Bottom);

					// if the intersection is not on the side being processed, skip it
					if (comparisonPos != sidePos)
						continue;

					Rectangle tempRect = startRect;

					if (isVertical)
					{
						tempRect.Height = intersection.Top - startRect.Top;
						startRect.Height -= tempRect.Height + intersection.Height;
						startRect.Y = intersection.Bottom;
					}
					else
					{
						tempRect.Width = intersection.Left - startRect.Left;
						startRect.Width -= tempRect.Width + intersection.Width;
						startRect.X = intersection.Right;
					}

					// draw the line
					DrawBorderLineHelper(g, tempRect,
						pen, null, i == NearIndex, isVertical);
				}

				// draw from the rest of the line
				DrawBorderLineHelper(g, startRect,
					pen, null, i == NearIndex, isVertical);
			}
			#endregion //Draw Borders skipping intersections
		}
		#endregion //DrawSimpleIntersectedSplitterBorders

		#region DrawComplexSplitterBorders
		private void DrawComplexSplitterBorders( ref UIElementDrawParams drawParams )
		{
			System.Diagnostics.Debug.Assert(this.SplitterControl.HasIntersections, "This method is intended to be used when there are intersections in a 3d based border style.");

			#region Setup

			// get the intersections
			Rectangle[] intersections = !this.SplitterControl.HasIntersections ? null : (Rectangle[])this.SplitterControl.IntersectionList.ToArray(typeof(Rectangle));

			Array.Sort(intersections, new RectangleComparer());

			Color borderColor3DBase = drawParams.AppearanceData.BorderColor3DBase;

			if (borderColor3DBase.IsEmpty)
				borderColor3DBase = drawParams.AppearanceData.BackColor;

			Graphics g = drawParams.Graphics;
			Pen dPen, ddPen, lPen, llPen;
			Pen outerLeftTopPen, outerRightBottomPen, innerLeftTopPen, innerRightBottomPen;

			outerLeftTopPen = outerRightBottomPen = innerLeftTopPen = innerRightBottomPen = null;

			// get the pen(s) we will use to draw the border
			if ( borderColor3DBase == SystemColors.Control )
			{
				dPen = drawParams.GetPen( DrawCache.PenType.ControlDark, borderColor3DBase );
				llPen = drawParams.GetPen( DrawCache.PenType.ControlLightLight, borderColor3DBase );
				ddPen = drawParams.GetPen( DrawCache.PenType.ControlDarkDark, borderColor3DBase );
				lPen = drawParams.GetPen( DrawCache.PenType.ControlLight, borderColor3DBase );
			}
			else
			{
				dPen = drawParams.GetPen( DrawCache.PenType.Dark, borderColor3DBase );
				llPen = drawParams.GetPen( DrawCache.PenType.LightLight, borderColor3DBase );
				ddPen = drawParams.GetPen( DrawCache.PenType.Dark, borderColor3DBase );
				lPen = drawParams.GetPen( DrawCache.PenType.LightLight, borderColor3DBase );
			}	
	
			Rectangle rect = this.Rect;
			bool isVertical = this.SplitterControl.Orientation == Orientation.Vertical;
			UIElementBorderStyle borderStyle = this.BorderStyle;

			#endregion //Setup

			#region Get Pens for Border
			switch(borderStyle)
			{
				case UIElementBorderStyle.Etched:
				{
					
					outerLeftTopPen = dPen;
					outerRightBottomPen = llPen;
					innerLeftTopPen = llPen;
					innerRightBottomPen = dPen;
					break;
				}
				case UIElementBorderStyle.Inset:
				{
					
					outerLeftTopPen = dPen;
					outerRightBottomPen = llPen;
					innerLeftTopPen = ddPen;
					innerRightBottomPen = lPen;
					break;
				}
				case UIElementBorderStyle.InsetSoft:
				{
					
					outerLeftTopPen = dPen;
					outerRightBottomPen = llPen;
					break;
				}
				case UIElementBorderStyle.Raised:
				{
					
					outerLeftTopPen = lPen;
					outerRightBottomPen = ddPen;
					innerLeftTopPen = llPen;
					innerRightBottomPen = dPen;
					break;
				}
				case UIElementBorderStyle.RaisedSoft:
				{
					
					outerLeftTopPen = llPen;
					outerRightBottomPen = dPen;
					break;
				}
				default:
				{
					System.Diagnostics.Debug.Assert(false, "Unexpected borderstyle!");
					return;
				}
			}
			#endregion //Get Pens for Border

			#region Draw Borders skipping intersections
			// iterate the intersections and draw the borders
			const int NearIndex = 0;

			// do 2 passes - one for the near (left/top) and one for far (right/bottom)
			for(int i = 0; i < 2; i++)
			{
				Rectangle startRect = rect;
				int sidePos = isVertical ? 
					(i == NearIndex ? startRect.Left : startRect.Right) :
					(i == NearIndex ? startRect.Top : startRect.Bottom);

				foreach(Rectangle intersection in intersections)
				{
					int comparisonPos = isVertical ? 
						(i == NearIndex ? intersection.Left : intersection.Right) :
						(i == NearIndex ? intersection.Top : intersection.Bottom);

					// if the intersection is not on the side being processed, skip it
					if (comparisonPos != sidePos)
						continue;

					Rectangle tempRect = startRect;

					if (isVertical)
					{
						tempRect.Height = intersection.Top - startRect.Top;
						startRect.Height -= tempRect.Height + intersection.Height;
						startRect.Y = intersection.Bottom;
					}
					else
					{
						tempRect.Width = intersection.Left - startRect.Left;
						startRect.Width -= tempRect.Width + intersection.Width;
						startRect.X = intersection.Right;
					}

					// draw the line
					DrawBorderLineHelper(g, tempRect,
						i == NearIndex ? outerLeftTopPen : outerRightBottomPen,
						i == NearIndex ? innerLeftTopPen : innerRightBottomPen,
						i == NearIndex, isVertical);
				}

				// draw from the rest of the line
				DrawBorderLineHelper(g, startRect,
					i == NearIndex ? outerLeftTopPen : outerRightBottomPen,
					i == NearIndex ? innerLeftTopPen : innerRightBottomPen,
					i == NearIndex, isVertical);
			}
			#endregion //Draw Borders skipping intersections

			#region Draw Intersections

			foreach(Rectangle intersection in intersections)
			{
				if ( (isVertical && intersection.Left == rect.Left) ||
					(!isVertical && intersection.Top == rect.Top))
					this.DrawCornerHelper(g, Corners.TopLeft, intersection, outerLeftTopPen, innerLeftTopPen, outerRightBottomPen, innerRightBottomPen);

				if ( (isVertical && intersection.Right == rect.Right) ||
					(!isVertical && intersection.Bottom == rect.Bottom))
					this.DrawCornerHelper(g, Corners.BottomRight, intersection, outerLeftTopPen, innerLeftTopPen, outerRightBottomPen, innerRightBottomPen);

				if ( (isVertical && intersection.Left == rect.Left) ||
					(!isVertical && intersection.Bottom == rect.Bottom))
					this.DrawCornerHelper(g, Corners.BottomLeft, intersection, outerLeftTopPen, innerLeftTopPen, outerRightBottomPen, innerRightBottomPen);

				if ( (isVertical && intersection.Right == rect.Right) ||
					(!isVertical && intersection.Top == rect.Top))
					this.DrawCornerHelper(g, Corners.TopRight, intersection, outerLeftTopPen, innerLeftTopPen, outerRightBottomPen, innerRightBottomPen);
			}
			#endregion //Draw Intersections
		}
		#endregion //DrawComplexSplitterBorders

		#region DrawCornerHelper
		private void DrawCornerHelper(Graphics g, Corners corner, Rectangle intersection, 
			Pen outerLeftTopPen, Pen innerLeftTopPen, Pen outerRightBottomPen, Pen innerRightBottomPen )
		{
			#region TopLeft
			if ( (corner & Corners.TopLeft) == Corners.TopLeft)
			{
				g.DrawLine(outerLeftTopPen, intersection.X - 1, intersection.Top,
					intersection.X, intersection.Top);

				// top left of intersection
				if (innerLeftTopPen != null)
				{
					g.DrawLine(innerLeftTopPen, intersection.X + 1, intersection.Top - 1,
						intersection.X + 1, intersection.Top);

					g.DrawLine(innerLeftTopPen, intersection.X, intersection.Top + 1,
						intersection.X + 1, intersection.Top + 1);
				}
			}
			#endregion //TopLeft

			#region BottomRight
			if ( (corner & Corners.BottomRight) == Corners.BottomRight)
			{
				g.DrawLine(outerRightBottomPen, intersection.Right - 1, intersection.Bottom - 1,
					intersection.Right, intersection.Bottom - 1);

				// top left of intersection
				if (innerRightBottomPen != null)
				{
					g.DrawLine(innerRightBottomPen, intersection.Right - 2, intersection.Bottom - 1,
						intersection.Right - 2, intersection.Bottom);

					g.DrawLine(innerRightBottomPen, intersection.Right - 2, intersection.Bottom - 2,
						intersection.Right - 1, intersection.Bottom - 2);
				}
			}
			#endregion //BottomRight

			#region BottomLeft
			if ( (corner & Corners.BottomLeft) == Corners.BottomLeft)
			{
				g.DrawLine(outerLeftTopPen, intersection.X, intersection.Bottom - 1,
					intersection.X, intersection.Bottom);

				// top left of intersection
				if (innerRightBottomPen != null)
				{
					g.DrawLine(innerRightBottomPen, intersection.X - 1, intersection.Bottom - 2,
						intersection.X, intersection.Bottom - 2);

					g.DrawLine(innerLeftTopPen, intersection.X + 1, intersection.Bottom - 2,
						intersection.X + 1, intersection.Bottom - 1);
				}
			}
			#endregion //BottomLeft

			#region TopRight
			if ( (corner & Corners.TopRight) == Corners.TopRight)
			{
				g.DrawLine(outerLeftTopPen, intersection.Right - 1, intersection.Top,
					intersection.Right, intersection.Top);

				// top left of intersection
				if (innerRightBottomPen != null)
				{
					g.DrawLine(innerLeftTopPen, intersection.Right - 2, intersection.Top + 1,
						intersection.Right, intersection.Top + 1);

					g.DrawLine(innerRightBottomPen, intersection.Right - 2, intersection.Top - 1,
						intersection.Right - 2, intersection.Top);
				}
			}
			#endregion //BottomLeft
		}
		#endregion //DrawCornerHelper

		#region DrawBorderLineHelper
		private static void DrawBorderLineHelper( Graphics g, Rectangle rect, 
			Pen outerPen, Pen innerPen, bool near, bool isVertical)
		{
			Point pt1 = rect.Location;
			Point pt2 = new Point(rect.Right - 1, rect.Bottom - 1);

			if (isVertical && near)
				pt2.X = pt1.X;
			else if (isVertical && !near)
				pt1.X = pt2.X;
			else if (!isVertical && near)
				pt2.Y = pt1.Y;
			else // !isVertical && !near
				pt1.Y = pt2.Y;

			g.DrawLine(outerPen, pt1, pt2);

			if (innerPen != null)
			{
				if (isVertical)
					pt1.X = pt2.X = pt1.X + (near ? 1 : -1);
				else // !isVertical
					pt1.Y = pt2.Y = pt1.Y + (near ? 1 : -1);

				g.DrawLine(innerPen, pt1, pt2);
			}
		}
		#endregion //DrawBorderLineHelper

		#endregion //Private

		#region Internal

		#region StartDrag
		internal void StartDrag(Point pt)
		{
			Point clientPt = this.Control.PointToClient(pt);

			if (this.InitiateDrag(clientPt, true))
				this.CaptureMouseForElement(this);
		}
		#endregion //StartDrag

		#region EndDrag
		internal void EndDrag( bool cancel )
		{
			this.ProcessEndDrag(cancel);

			if (this.HasCapture)
				this.ControlElement.TerminateCapture();
		}
		#endregion //EndDrag

		#endregion //Internal

		#endregion //Methods

		#region IMessageFilter
		// MD 1/4/07 - FxCop
		[SecurityPermission( SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.UnmanagedCode )]
		bool IMessageFilter.PreFilterMessage(ref Message m)
		{
			if (!this.isDragging)
				return false;

			if (m.Msg >= NativeWindowMethods.WmKeyFirst && m.Msg <= NativeWindowMethods.WmKeyLast)
			{
				// MD 10/31/06 - 64-Bit Support
				//if (m.Msg == NativeWindowMethods.WmKeyDown && m.WParam.ToInt32() == NativeWindowMethods.VkEscape)
				if ( m.Msg == NativeWindowMethods.WmKeyDown && m.WParam.ToInt64() == NativeWindowMethods.VkEscape )
					this.EndDrag(true);

				return true;
			}

			return false;
		}
		#endregion //IMessageFilter

		// AS 3/28/05 NA 2005 Vol 2 - Nested TabGroups
		#region RectangleComparer nested class
		private class RectangleComparer : System.Collections.IComparer
		{
			int System.Collections.IComparer.Compare(object x, object y)
			{
				Rectangle rectX = (Rectangle)x;
				Rectangle rectY = (Rectangle)y;

				if (rectX.X < rectY.X)
					return -1;
				else if (rectX.X > rectY.X)
					return 1;
				else if (rectX.Y < rectY.Y)
					return -1;
				else if (rectX.Y > rectY.Y)
					return 1;
				else
					return 0;
			}
		}
		#endregion //RectangleComparer nested class
	}
}
