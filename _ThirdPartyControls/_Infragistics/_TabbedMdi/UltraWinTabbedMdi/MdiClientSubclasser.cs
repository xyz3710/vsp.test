#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Diagnostics;
using System.Windows.Forms;

// AS 4/30/03 WTB880
using System.Drawing;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinTabbedMdi
{
	[ System.Security.SuppressUnmanagedCodeSecurityAttribute() ]
	internal class MdiClientSubclasser : System.Windows.Forms.NativeWindow,
		IMdiClientWindow // AS 5/1/03 WTB880
	{
		#region Constants

		private const int 		WM_MDICASCADE = 0x227;
		private const int 		WM_MDIICONARRANGE = 0x228;
		private const int 		WM_MDIRESTORE = 0x223;
		private const int 		WM_MDITILE = 0x226;
		private const int 		WM_MDIMAXIMIZE = 0x225;

		private const int 		WM_MDIACTIVATE = 0x222;
		private const int 		WM_MDINEXT = 0x224;

		// AS 4/30/03 WTB880
		private const int 		WM_ERASEBKGND = 0x14;

		// AS 5/29/03 WTB1012
		private const int		WM_PARENTNOTIFY = 0x210;
		private const int		WM_CREATE = 0x1;

		

		// AS 4/30/03 WTB880
		private const int		WM_NCPAINT		= 0x0085;
		private const int		WM_NCCALCSIZE	= 0x0083;

		#endregion //Constants

		#region Member Variables

		private UltraTabbedMdiManager			manager = null;

		// AS 4/30/03 WTB880
		private Rectangle						displayRect = Rectangle.Empty;

		#endregion //Member Variables

		#region Constructor
		internal MdiClientSubclasser(UltraTabbedMdiManager mdiManager)
		{
			this.manager = mdiManager;

			// AS 4/30/03 WTB880
			// When the subclasser is created, adjust the
			// mdi client border style.
			//
			this.UpdateBorderStyle();
		}
		#endregion //Constructor

		#region Methods

		#region WndProc
		protected override void WndProc(ref System.Windows.Forms.Message m)
		{
			UltraTabbedMdiManager manager = this.manager;

			// check if unhooked.
			if (manager == null)
			{
				base.WndProc(ref m);
				return;
			}

			switch(m.Msg)
			{
				case WM_MDICASCADE:
				case WM_MDIRESTORE:
				case WM_MDITILE:
				case WM_MDIICONARRANGE:
				case WM_MDIMAXIMIZE:
				{
					// prevent these actions when we are hooked up
					if (manager.Enabled)
					{
						m.Result = IntPtr.Zero;
						return;
					}
					break;
				}
				case WM_MDINEXT:
				{
					#region WM_MDINEXT
					// need a variable to hold the starting point
					//
					Form mdiChild = null;

					// find out which window will be activated
					//

					// if they did not pass in a window handle, it
					// starts with the active mdi child
					if (m.WParam == IntPtr.Zero)
					{
						// so get the mdi form
						Control mdiParent = Control.FromChildHandle(this.Handle);

						// assuming it is a form, get the active mdi child
						if (mdiParent is Form)
							mdiChild = ((Form)mdiParent).ActiveMdiChild;
					}
					else
					{
						// otherwise the starting point is the handle they specified
						mdiChild = Control.FromHandle(m.WParam) as Form;
					}

					// see if this is going forward or backward
					bool forward = m.LParam == IntPtr.Zero;

					// get the next/previous form
					Form nextForm = this.GetNextWindow(mdiChild, forward);

					// AS 5/22/07 BR23151 - MdiTabNavigationMode 
					// When navigating through the visible order, we will just 
					// explicitly activate the form and not let the mdi client
					// handle the MDINEXT message.
					//
					if (manager.TabNavigationMode == MdiTabNavigationMode.VisibleOrder)
					{
						if (null != nextForm)
							nextForm.Activate();

						m.Result = IntPtr.Zero;
						return;
					}

					// if the tab is already selected, we don't need to
					// ask permission - we don't want the selection events
					// to fire in this case since its already selected
					if (!this.SendActivationNotification(nextForm))
						break;
					else if (this.AllowMdiChildActivate(nextForm))
					{
						// if we are allowed to activate it, then let
						// the message go through to the wndproc

						// let the manager know that we are handling the activation
						// of the form
						manager.SubclasserActivatingMdiChild = true;

						try
						{
							base.WndProc(ref m);
						}
						finally
						{
							manager.SubclasserActivatingMdiChild = false;

							manager.AfterSubclasserFormActivation(nextForm);
						}
					}
					else
					{
						// otherwise, eat it
						m.Result = IntPtr.Zero;
					}

					return;
					#endregion //WM_MDINEXT
				}
				case WM_MDIACTIVATE:
				{
					#region WM_MDIACTIVATE
					// get the form that will be activated.
					Form form = Control.FromHandle(m.WParam) as Form;

					// if the tab is already selected, we don't need to
					// ask permission - we don't want the selection events
					// to fire in this case since its already selected
					if (!this.SendActivationNotification(form))
						break;
					else if (this.AllowMdiChildActivate(form))
					{
						// if we are allowed to activate it, then let
						// the message go through to the wndproc

						// let the manager know that we are handling the activation
						// of the form
						manager.SubclasserActivatingMdiChild = true;

						try
						{
							base.WndProc(ref m);
						}
						finally
						{
							manager.SubclasserActivatingMdiChild = false;

							manager.AfterSubclasserFormActivation(form);
						}
					}
					else
					{
						// otherwise, eat it
						m.Result = IntPtr.Zero;
					}

					return;
					#endregion //WM_MDIACTIVATE
				}
				// AS 4/30/03 WTB880
				case WM_NCPAINT:
				{
					#region WM_NCPAINT

					base.WndProc(ref m);

					if (!this.IsManagerEnabled)
						return;

					IntPtr hdc = IntPtr.Zero;
					Graphics g = null;
					IntPtr hwnd = m.HWnd;

					try
					{
						Rectangle rect = new Rectangle(Point.Empty, this.DisplayRectangle.Size);

						// paint the borders
						// MD 10/31/06 - 64-Bit Support
						//if (m.WParam.ToInt32() == 1 || m.WParam == IntPtr.Zero)
						if ( m.WParam == IntPtr.Zero || m.WParam.ToInt64() == 1 )
						{
							// paint everything
							hdc = NativeWindowMethods.GetWindowDC(hwnd);
						}
						else
						{
							// get dc with the specified region
							hdc = NativeWindowMethods.GetDCEx(hwnd, 
								m.WParam, 
								NativeWindowMethods.DCX_WINDOW | 
								NativeWindowMethods.DCX_INTERSECTRGN);
								
							// use the dc from the window
							if (hdc == IntPtr.Zero)
								hdc = NativeWindowMethods.GetWindowDC(m.HWnd);
						}

						if (hdc != IntPtr.Zero)
						{
							// MD 1/4/07 - FxCop
							// Graphics.FromHdc does a security demand for unmanaged code
							SecurityPermission perm = new SecurityPermission( SecurityPermissionFlag.UnmanagedCode );
							perm.Assert();

							g = Graphics.FromHdc(hdc);

							// MD 1/4/07 - FxCop
							SecurityPermission.RevertAssert();

							this.DrawBorder(g, rect, rect);
	
							m.Result = IntPtr.Zero;
						}
					}
					finally
					{
						if (g != null)
							g.Dispose();

						if (hdc != IntPtr.Zero)
							NativeWindowMethods.ReleaseDC(hwnd, hdc);
					}

					return;

					#endregion //WM_NCPAINT
				}
				// AS 4/30/03 WTB880
				case WM_NCCALCSIZE:
				{
					#region WM_NCCALCSIZE
					if (this.IsManagerEnabled)
					{
						// adjust the non client area based on how much borders
						// we want on the menus
						// MD 10/31/06 - 64-Bit Support
						//if (m.WParam.ToInt32() == 1)
						if ( m.WParam.ToInt64() == 1 )
						{
							NativeWindowMethods.NCCALCSIZE_PARAMS ncParams = (NativeWindowMethods.NCCALCSIZE_PARAMS)m.GetLParam(typeof(NativeWindowMethods.NCCALCSIZE_PARAMS));

							UIElementBorderWidths borders = this.GetBorderWidth();

							// store the old rect
							this.displayRect = ncParams.rectProposed.Rect;

							ncParams.rectProposed.left += borders.Left;
							ncParams.rectProposed.right -= borders.Right;
							ncParams.rectProposed.top += borders.Top;
							ncParams.rectProposed.bottom -= borders.Bottom;

							Marshal.StructureToPtr(ncParams, m.LParam, false);
						}
					}
					break;
					#endregion //WM_NCCALCSIZE
				}
				// AS 4/30/03 WTB880
				case WM_ERASEBKGND:
				{
					#region WM_ERASEBKGND
					base.WndProc(ref m);

					if (this.IsManagerEnabled)
						m.Result = new IntPtr(1);
					return;
					#endregion //WM_ERASEBKGND
				}
				// AS 5/29/03 WTB1012
				// Try to prevent flicker when showing a hidden mdi child form. 
				// For some reason, the mdi child handle is created and destroyed
				// several times and if we don't turn off mdiclient redrawing, you'll
				// see the title bar of the form appear and disappear.
				//
				case WM_PARENTNOTIFY:
				{
					#region WM_PARENTNOTIFY
					if ( (m.WParam.ToInt32() & 0xffff) == WM_CREATE )
						this.manager.MdiClientCreateNotification();
					break;
					#endregion //WM_PARENTNOTIFY
				}
			}

			base.WndProc(ref m);
		}
		#endregion //WndProc

		#region SendActivationNotification
		private bool SendActivationNotification( Form form )
		{
			if (this.manager == null)
				return false;

			return this.manager.ShouldSubclasserNotifyFormActivation(form);
		}
		#endregion //SendActivationNotification

		#region AllowMdiChildActivate
		private bool AllowMdiChildActivate( Form form )
		{
			// check if this is still active...
			if (this.manager != null)
				return this.manager.ShouldSubclasserAllowFormActivation(form);

			return true;
		}
		#endregion //AllowMdiChildActivate

		#region GetNextWindow
		private Form GetNextWindow( Form mdiChild, bool forward )
		{
			// make sure we have a form to start with
			if (mdiChild == null)
				return null;

			// AS 5/22/07 BR23151 - MdiTabNavigationMode 
			#region MdiTabNavigationMode.VisibleOrder
			if (this.manager != null && this.manager.TabNavigationMode == MdiTabNavigationMode.VisibleOrder)
			{
				MdiTab tab = this.manager.TabFromForm(mdiChild);
				MdiTab tabToActivate = null;

				if (tab != null)
					tabToActivate = tab.GetNextPreviousTab(true, forward);

				return tabToActivate != null ? tabToActivate.Form : null;
			} 
			#endregion //MdiTabNavigationMode.VisibleOrder

			// get the mdi client area that we are subclassing
			MdiClient mdiClient = Control.FromHandle(this.Handle) as MdiClient;

			// it would be nice if the GetNextControl method actually worked here
			// but it doesn't so we must do it ourselves.
			//

			// get the index of the mdi child is our base
			int index = mdiClient.Controls.GetChildIndex(mdiChild,false);

			// if its not in the collection, bail
			if (index < 0)
				return null;

			// find out how many controls there are in total
			int count = mdiClient.Controls.Count;

			// need a variable to hold the next control/form
			Control nextControl = null;

			// if we are moving forward...
			if (forward)
			{
				// start with the next index and wrap if necessary
				for(int i = index + 1; i < count * 2; i++)
				{
					// get the control (using the modulus)
					Control c = mdiClient.Controls[i % count];

					// if this form cannot be selected, skip it
					if (!c.CanSelect)
						continue;

					// otherwise, we've got it
					nextControl = c;
					break;
				}
			}
			else // otherwise we are moving backward
			{
				// start with the previous index and wrap if necessary
				for(int i = index -1 + count; i > index; i--)
				{
					// get the control (using the modulus)
					Control c = mdiClient.Controls[i % count];

					// if this form cannot be selected, skip it
					if (!c.CanSelect)
						continue;

					// otherwise, we've got it
					nextControl = c;
					break;
				}
			}

			return nextControl as Form;
		}
		#endregion //GetNextWindow

		#region Unhook
		// AS 5/1/03 WTB880
		// Changed from internal to private - the manager
		// can more easily communicate with the class
		// via the interface
		//
		private void Unhook()
		{
			this.manager = null;

			// AS 4/30/03 WTB880
			// Update the border style when unhooking
			//
			this.UpdateBorderStyle();
		}
		#endregion //Unhook

		// AS 5/1/03 WTB880
		//
		#region DrawBorder
		private void DrawBorder( Graphics g, Rectangle rect, Rectangle clipRect )
		{
			if (!this.IsManagerEnabled)
				return;

			Debug.WriteLine("Border:" + displayRect.ToString() + ":" + clipRect.ToString());

			Color color = this.manager.BorderColor;

			Pen pen = null;

			switch(this.manager.BorderStyle)
			{
				case MdiClientBorderStyle.None:
					// no border
					break;

				case MdiClientBorderStyle.Inset:
					if (color.IsEmpty)
						color = SystemColors.Control;

					//ControlPaint.DrawBorder(g, displayRect, Color.Cyan,
					//	ButtonBorderStyle.Inset);
					if (color == SystemColors.Control)
					{
						this.DrawBorderHelper(g, rect, clipRect, Border3DSide.Left | Border3DSide.Top, SystemColors.ControlDark, ref pen);
						this.DrawBorderHelper(g, rect, clipRect, Border3DSide.Right | Border3DSide.Bottom, SystemColors.ControlLightLight, ref pen);
						rect.Inflate(-1,-1);
						this.DrawBorderHelper(g, rect, clipRect, Border3DSide.Left | Border3DSide.Top, SystemColors.ControlDarkDark, ref pen);
						this.DrawBorderHelper(g, rect, clipRect, Border3DSide.Right | Border3DSide.Bottom, SystemColors.ControlLight, ref pen);
					}
					else
					{
						this.DrawBorderHelper(g, rect, clipRect, Border3DSide.Left | Border3DSide.Top, DrawUtility.Dark(color), ref pen);
						this.DrawBorderHelper(g, rect, clipRect, Border3DSide.Right | Border3DSide.Bottom, DrawUtility.LightLight(color), ref pen);
						rect.Inflate(-1,-1);
						this.DrawBorderHelper(g, rect, clipRect, Border3DSide.Left | Border3DSide.Top, DrawUtility.DarkDark(color), ref pen);
						this.DrawBorderHelper(g, rect, clipRect, Border3DSide.Right | Border3DSide.Bottom, DrawUtility.Light(color), ref pen);
					}
					break;

				case MdiClientBorderStyle.Solid:
					if (color.IsEmpty)
						color = SystemColors.ControlDark;

					//ControlPaint.DrawBorder(g, displayRect, SystemColors.ControlDark,
					//	ButtonBorderStyle.Solid);
					this.DrawBorderHelper(g, rect, clipRect, Border3DSide.All, color, ref pen);
					break;
			}

			if (pen != null)
				pen.Dispose();
		}
		#endregion //DrawBorder

		#region DrawBorderHelper
		private void DrawBorderHelper(Graphics g, Rectangle rect, Rectangle invalidRect, Border3DSide sides, Color color, ref Pen pen)
		{
			if (sides == 0)
				return;

			Rectangle rcWork = rect;

			rcWork.Width--;
			rcWork.Height--;

			if (rcWork.Height <= 0 || rcWork.Width <= 0)
				return;

			// JJD 1/23/04 - WTR530
			// Use CreatePen and CreateSolidBrush utils to prevent memory leaks caused by creating pen using 
			if (pen == null)
				//pen = new Pen(color);
				pen = Infragistics.Win.Utilities.CreatePen(color);
			else if (pen.Color != color)
				//pen.Color = color;
				pen.Color = Infragistics.Win.Utilities.GetNonSystemColor( color );
			
			if ((sides & Border3DSide.Left) != 0 && invalidRect.Left <= rcWork.Left )
				g.DrawLine( pen, rcWork.Left, rcWork.Bottom, rcWork.Left, rcWork.Top );

			if ((sides & Border3DSide.Right) != 0 && invalidRect.Right >= rcWork.Right - 1 )
				g.DrawLine( pen, rcWork.Right, rcWork.Top, rcWork.Right, rcWork.Bottom );

			if ((sides & Border3DSide.Top) != 0 && invalidRect.Top <= rcWork.Top )
				g.DrawLine( pen, rcWork.Left, rcWork.Top, rcWork.Right, rcWork.Top );
			
			if ((sides & Border3DSide.Bottom) != 0 && invalidRect.Bottom >= rcWork.Bottom - 1 )
				g.DrawLine( pen, rcWork.Right, rcWork.Bottom, rcWork.Left, rcWork.Bottom );
		}
		#endregion //DrawBorderHelper

		#region GetBorderWidth
		private UIElementBorderWidths GetBorderWidth()
		{
			UIElementBorderWidths borders = new UIElementBorderWidths();

			if (this.IsManagerEnabled)
			{
				switch(this.manager.BorderStyle)
				{
					case MdiClientBorderStyle.None:
						// no border
						break;

					case MdiClientBorderStyle.Inset:
						borders.Left = borders.Right = borders.Top = borders.Bottom = 2;
						break;

					case MdiClientBorderStyle.Solid:
						borders.Left = borders.Right = borders.Top = borders.Bottom = 1;
						break;
				}
			}

			return borders;
		}
		#endregion //GetBorderWidth

		#region IsManagerEnabled
		private bool IsManagerEnabled
		{
			get { return this.manager != null && this.manager.Enabled; }
		}
		#endregion //IsManagerEnabled

		#region OnHandleChange
		protected override void OnHandleChange()
		{
			base.OnHandleChange();

			// update the border
			if (this.IsManagerEnabled)
				this.UpdateBorderStyle();
		}
		#endregion //OnHandleChange

		#region UpdateBorderStyle
		private void UpdateBorderStyle()
		{
			if (!this.IsManagerEnabled)
				NativeWindowMethods.UpdateBorderStyle(this.Handle, BorderStyle.Fixed3D);
			else
				NativeWindowMethods.UpdateBorderStyle(this.Handle, BorderStyle.None);
		}
		#endregion //UpdateBorderStyle

		#region DisplayRectangle
		private Rectangle DisplayRectangle
		{
			get
			{
				return this.displayRect;
			}
		}
		#endregion //DisplayRectangle

		#endregion //Methods

		// AS 5/1/03 WTB880
		// The manager can more easily communicate with 
		// the class via the interface
		//
		#region IMdiClientWindow
		void IMdiClientWindow.BorderColorChanged()
		{
			if (!this.IsManagerEnabled)
				return;

			// invalidate the border?
			SecurityPermission perm = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);

			try
			{
				perm.Assert();

				NativeWindowMethods.RedrawWindowApi(this.Handle, IntPtr.Zero, IntPtr.Zero, 
					NativeWindowMethods.RDW_FRAME			|
					NativeWindowMethods.RDW_INVALIDATE		|
					NativeWindowMethods.RDW_NOCHILDREN);
			}
			catch (System.Security.SecurityException){}
		}

		void IMdiClientWindow.VerifyBorderStyle()
		{
			this.UpdateBorderStyle();
		}

		void IMdiClientWindow.Unsubclass()
		{
			this.Unhook();
		}

		// AS 10/30/03 WTB1216
		// Setting the MdiParent to null and then back to an mdi parent does
		// not subclass the new mdi parent's mdi client. This is partially 
		// because we maintain a reference to the mdi client window and 
		// partially because there is no way to reassociate it with the
		// mdi parent. Since in theory someone could try to associate the 
		// tabbed mdi manager with a different mdi parent, i'll expose the 
		// activation state, associated handle and a way to reactivate.
		//
		bool IMdiClientWindow.IsActive { get { return this.manager != null; } }

		IntPtr IMdiClientWindow.Handle { get { return this.Handle; } }

		bool IMdiClientWindow.Activate( UltraTabbedMdiManager manager, MdiClient mdiClient )
		{
			if (mdiClient == null || manager == null)
				return false;

			if (this.manager == manager && mdiClient.Handle == this.Handle)
				return true;

			// do not associate with a different mdi client
			if (this.Handle != mdiClient.Handle)
				return false;

			this.manager = manager;

			this.UpdateBorderStyle();

			return true;
		}

		#endregion IMdiClientWindow
	}

	// AS 5/1/03 WTB880
	#region IMdiClientWindow
	internal interface IMdiClientWindow
	{
		void Unsubclass();

		void VerifyBorderStyle();

		void BorderColorChanged();

		// AS 10/30/03 WTB1216
		// Setting the MdiParent to null and then back to an mdi parent does
		// not subclass the new mdi parent's mdi client. This is partially 
		// because we maintain a reference to the mdi client window and 
		// partially because there is no way to reassociate it with the
		// mdi parent. Since in theory someone could try to associate the 
		// tabbed mdi manager with a different mdi parent, i'll expose the 
		// activation state, associated handle and a way to reactivate.
		//
		bool IsActive { get; } 

		IntPtr Handle { get; }

		bool Activate( UltraTabbedMdiManager manager, MdiClient mdiClient );
	}
	#endregion //IMdiClientWindow

	// AS 3/8/05 BR02542
	#region MdiParentSubclasser
	[ System.Security.SuppressUnmanagedCodeSecurityAttribute() ]
	internal class MdiParentSubclasser : System.Windows.Forms.NativeWindow,
		IMdiParentWindow
	{
		#region Constants

		private const int WM_NEXTMENU = 0x0213;

		#endregion //Constants

		#region Member Variables

		private UltraTabbedMdiManager			manager = null;

		#endregion //Member Variables

		#region Constructor
		internal MdiParentSubclasser(UltraTabbedMdiManager mdiManager)
		{
			this.manager = mdiManager;
		}
		#endregion //Constructor

		#region Methods

		#region WndProc
		protected override void WndProc(ref System.Windows.Forms.Message m)
		{
			UltraTabbedMdiManager manager = this.manager;

			// check if unhooked.
			if (manager == null)
			{
				base.WndProc(ref m);
				return;
			}

			switch(m.Msg)
			{
				case WM_NEXTMENU:
				{
					#region WM_NEXTMENU

					if (!manager.Enabled			||
						manager.MdiParent == null	||
						manager.MdiParent.ActiveMdiChild == null)
						break;

					Form activeMdiChild = manager.MdiParent.ActiveMdiChild;

					if (activeMdiChild == null || !activeMdiChild.IsHandleCreated)
						break;

					// keep a copy of the original message
					Message originalMsg = m;

					// let the base process this message
					base.WndProc(ref m);

					IntPtr mdiChildHwnd = activeMdiChild.Handle;

					// in a tabbed mdi, we cannot let the child's
					// menu be displayed
					NativeWindowMethods.MDINEXTMENU nextMenuInfo = (NativeWindowMethods.MDINEXTMENU)Marshal.PtrToStructure(m.LParam, typeof(NativeWindowMethods.MDINEXTMENU));

					// if its the system menu for the mdi child
					// then we need to process this message again
					// starting with this menu in case there
					// is another menu to navigate to after this
					if (nextMenuInfo.hwndNext == mdiChildHwnd &&
						nextMenuInfo.hmenuNext != IntPtr.Zero &&
						NativeWindowMethods.GetSystemMenu(mdiChildHwnd, false) == nextMenuInfo.hmenuNext)
					{
						nextMenuInfo.hmenuIn = nextMenuInfo.hmenuNext;
						nextMenuInfo.hwndNext = IntPtr.Zero;

						// update the ptr with the updated struct
						Marshal.StructureToPtr(nextMenuInfo, m.LParam, true);

						base.WndProc(ref m);
					}

					return;
					#endregion //WM_NEXTMENU
				}			
			}

			base.WndProc(ref m);
		}
		#endregion //WndProc

		#region Unhook
		private void Unhook()
		{
			this.manager = null;
		}
		#endregion //Unhook

		#region IsManagerEnabled
		private bool IsManagerEnabled
		{
			get { return this.manager != null && this.manager.Enabled; }
		}
		#endregion //IsManagerEnabled

		#endregion //Methods

		#region IMdiParentWindow
		void IMdiParentWindow.Unsubclass()
		{
			this.Unhook();
		}

		// AS 10/30/03 WTB1216
		// Setting the MdiParent to null and then back to an mdi parent does
		// not subclass the new mdi parent's mdi client. This is partially 
		// because we maintain a reference to the mdi client window and 
		// partially because there is no way to reassociate it with the
		// mdi parent. Since in theory someone could try to associate the 
		// tabbed mdi manager with a different mdi parent, i'll expose the 
		// activation state, associated handle and a way to reactivate.
		//
		bool IMdiParentWindow.IsActive { get { return this.manager != null; } }

		IntPtr IMdiParentWindow.Handle { get { return this.Handle; } }

		bool IMdiParentWindow.Activate( UltraTabbedMdiManager manager, Form mdiParent)
		{
			if (mdiParent == null || manager == null)
				return false;

			if (this.manager == manager && mdiParent.Handle == this.Handle)
				return true;

			// do not associate with a different mdi client
			if (this.Handle != mdiParent.Handle)
				return false;

			this.manager = manager;

			return true;
		}

		#endregion //IMdiParentWindow
	}
	#endregion //MdiParentSubclasser

	// AS 3/8/05 BR02542
	#region IMdiParentWindow
	internal interface IMdiParentWindow
	{
		void Unsubclass();

		// AS 10/30/03 WTB1216
		// Setting the MdiParent to null and then back to an mdi parent does
		// not subclass the new mdi parent's mdi client. This is partially 
		// because we maintain a reference to the mdi client window and 
		// partially because there is no way to reassociate it with the
		// mdi parent. Since in theory someone could try to associate the 
		// tabbed mdi manager with a different mdi parent, i'll expose the 
		// activation state, associated handle and a way to reactivate.
		//
		bool IsActive { get; } 

		IntPtr Handle { get; }

		bool Activate( UltraTabbedMdiManager manager, Form mdiParent );
	}
	#endregion //IMdiParentWindow
}
