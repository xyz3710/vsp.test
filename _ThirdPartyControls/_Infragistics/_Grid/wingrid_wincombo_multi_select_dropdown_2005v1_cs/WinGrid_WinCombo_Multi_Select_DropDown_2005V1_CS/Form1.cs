using System.Diagnostics;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;

namespace WinCombo_Selection_Event_2005V1_VB
{
	public class Form1 : System.Windows.Forms.Form
	{
		[STAThread]
		static void Main()
		{
			Application.Run(new Form1());
		}
		
		#region " Windows Form Designer generated code "
		
		public Form1() {
			
			//This call is required by the Windows Form Designer.
			InitializeComponent();
			
			//Add any initialization after the InitializeComponent() call
			
		}
		
		//Form overrides dispose to clean up the component list.
		protected override void Dispose (bool disposing)
		{
			if (disposing)
			{
				if (!(components == null))
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		private System.ComponentModel.IContainer components;
		
//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		internal Infragistics.Win.UltraWinEditors.UltraTextEditor UltraTextEditor1;
		internal Infragistics.Win.UltraWinGrid.UltraGrid UltraGrid1;
		internal Infragistics.Win.UltraWinGrid.UltraGrid theMultiSelectGrid;
		internal System.Windows.Forms.Label Label1;
		internal Infragistics.Win.Misc.UltraPopupControlContainer UltraPopupControlContainer1;
		[System.Diagnostics.DebuggerStepThrough()]private void InitializeComponent ()
		{
			this.components = new System.ComponentModel.Container();
			this.UltraTextEditor1 = new Infragistics.Win.UltraWinEditors.UltraTextEditor();
			this.UltraGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.theMultiSelectGrid = new Infragistics.Win.UltraWinGrid.UltraGrid();
			this.Label1 = new System.Windows.Forms.Label();
			this.UltraPopupControlContainer1 = new Infragistics.Win.Misc.UltraPopupControlContainer(this.components);
			((System.ComponentModel.ISupportInitialize)(this.UltraTextEditor1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.UltraGrid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.theMultiSelectGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// UltraTextEditor1
			// 
			this.UltraTextEditor1.Location = new System.Drawing.Point(298, 276);
			this.UltraTextEditor1.Name = "UltraTextEditor1";
			this.UltraTextEditor1.Size = new System.Drawing.Size(259, 21);
			this.UltraTextEditor1.TabIndex = 0;
			this.UltraTextEditor1.AfterEditorButtonCloseUp += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.UltraTextEditor1_AfterEditorButtonCloseUp);
			this.UltraTextEditor1.BeforeEditorButtonDropDown += new Infragistics.Win.UltraWinEditors.BeforeEditorButtonDropDownEventHandler(this.UltraTextEditor1_BeforeEditorButtonDropDown);
			// 
			// UltraGrid1
			// 
			this.UltraGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
			this.UltraGrid1.Location = new System.Drawing.Point(10, 9);
			this.UltraGrid1.Name = "UltraGrid1";
			this.UltraGrid1.Size = new System.Drawing.Size(480, 181);
			this.UltraGrid1.TabIndex = 1;
			this.UltraGrid1.Text = "UltraGrid1";
			this.UltraGrid1.BeforeEnterEditMode += new System.ComponentModel.CancelEventHandler(this.UltraGrid1_BeforeEnterEditMode);
			this.UltraGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.UltraGrid1_InitializeLayout);
			// 
			// theMultiSelectGrid
			// 
			this.theMultiSelectGrid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
			this.theMultiSelectGrid.Location = new System.Drawing.Point(84, 302);
			this.theMultiSelectGrid.Name = "theMultiSelectGrid";
			this.theMultiSelectGrid.Size = new System.Drawing.Size(473, 189);
			this.theMultiSelectGrid.TabIndex = 2;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(374, 198);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(173, 26);
			this.Label1.TabIndex = 3;
			this.Label1.Text = "Resize This Form!";
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 14);
			this.ClientSize = new System.Drawing.Size(830, 594);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.theMultiSelectGrid);
			this.Controls.Add(this.UltraGrid1);
			this.Controls.Add(this.UltraTextEditor1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.UltraTextEditor1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.UltraGrid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.theMultiSelectGrid)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		
		#endregion
		
		#region "  Data  "
		
		private DataTable MakeDataTable()
		{
			DataTable objTable = new DataTable();
			DataRow objRow;
			
			DataTable with_1 = objTable;
			with_1.Columns.Add("Col_1", Type.GetType("System.Boolean"));
			with_1.Columns.Add("Col_2", Type.GetType("System.String"));
			with_1.Columns.Add("Col_3", Type.GetType("System.String"));
			with_1.Columns.Add("Col_4", Type.GetType("System.Int32"));
			
			for (int i = 1; i <= 20; i++)
			{
				
				objRow = with_1.NewRow();
				
				objRow[0] = (i % 2 == 0);
				objRow[1] = "GoodBye " + i.ToString();
				objRow[2] = "Sayonara " + i.ToString();
				objRow[3] = Interaction.IIf((i % 2 == 0), -i, i);
				
				with_1.Rows.Add(objRow);
			}
			
			return objTable;
		}
		
		private DataTable MakeAnotherDataTable()
		{
			DataTable objTable = new DataTable();
			DataRow objRow;
			
			DataTable with_1 = objTable;
			with_1.Columns.Add("Col_1", Type.GetType("System.Boolean"));
			with_1.Columns.Add("Col_2", Type.GetType("System.String"));
			with_1.Columns.Add("Col_3", Type.GetType("System.String"));
			with_1.Columns.Add("Col_4", Type.GetType("System.Int32"));
			
			for (int i = 1; i <= 20; i++)
			{
				
				objRow = with_1.NewRow();
				
				objRow[0] = (i % 2 == 0);
				objRow[1] = "HEY " + i.ToString();
				objRow[2] = "HEY " + i.ToString();
				objRow[3] = Interaction.IIf((i % 2 == 0), -i, i);
				
				with_1.Rows.Add(objRow);
			}
			
			return objTable;
		}
		
		#endregion
		
		private void Form1_Load (System.Object sender, System.EventArgs e)
		{
			
			//Set up the "Custom Control"
			Infragistics.Win.UltraWinEditors.DropDownEditorButton b = new Infragistics.Win.UltraWinEditors.DropDownEditorButton(); //Dropdown Editor Button
			b.Control = this.theMultiSelectGrid; //The Multi Select "Combo"
			this.UltraTextEditor1.ButtonsRight.Add(b); //add the button to the editor control
			this.theMultiSelectGrid.DataSource = this.MakeAnotherDataTable(); //Fill the "Combo" with Data
			
			//'''''' The real grid that is used for the UI
			this.UltraGrid1.DataSource = this.MakeDataTable(); //set the data source
			this.UltraGrid1.DisplayLayout.Bands[0].Columns[2].EditorControl = this.UltraTextEditor1; //connect the "Custom Editor"
			
		}
		
		private void UltraTextEditor1_AfterEditorButtonCloseUp (object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
		{
			if (this.theMultiSelectGrid.ActiveRow == null)return;

			this.UltraGrid1.ActiveCell.Value = this.theMultiSelectGrid.ActiveRow.Cells[1].Value;
		}
		
		private void UltraGrid1_InitializeLayout (System.Object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
		{
			//Show the buttons at ALL times
			e.Layout.Bands[0].Columns[2].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
		}
		
		private void UltraGrid1_BeforeEnterEditMode (object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (this.UltraGrid1.ActiveCell.Column.Key == "Col_3")
			{
				this.UltraGrid1.ActiveCell.Column.CellActivation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
			}
			else
			{
				this.UltraGrid1.ActiveCell.Column.CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
			}
		}

		private void UltraTextEditor1_BeforeEditorButtonDropDown(object sender, Infragistics.Win.UltraWinEditors.BeforeEditorButtonDropDownEventArgs e)
		{
		
		}
		
	}
	
}
