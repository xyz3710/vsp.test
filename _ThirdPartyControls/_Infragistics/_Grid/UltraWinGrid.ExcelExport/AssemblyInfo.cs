#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


#region XPS License
/*----------------------------------------------------------------------*
* Note: To the best of our knowledge, Microsoft® doesn’t forbid         *
* third-party companies from developing a product for the XPS document  *
* generation. However, according to the XML Paper Specification Patent  *
* License, we need to provide the following notice for your information:* 
*                                                                       *
* This product may incorporate intellectual property owned by Microsoft *
* Corporation. The terms and conditions upon which Microsoft is         *
* licensing such intellectual property may be found at                  *
* http://go.microsoft.com/fwlink/?LinkId=52369.;                        *
* ----------------------------------------------------------------------*
*/		
#endregion XPS License

using System.Reflection;
using System.Runtime.CompilerServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyConfiguration(AssemblyRef.Configuration)]
[assembly: AssemblyDescription(AssemblyRef.AssemblyDescriptionBase + " - " + AssemblyRef.Configuration + " Version")]
[assembly: AssemblyTitle(AssemblyRef.AssemblyName + AssemblyRef.ProductTitleSuffix)]
[assembly: AssemblyProduct(AssemblyRef.AssemblyProduct + AssemblyRef.ProductTitleSuffix)]
[assembly: AssemblyCompany(Infragistics.Shared.AssemblyVersion.CompanyName)]
[assembly: AssemblyCopyright("Copyright(c) 2003-"+Infragistics.Shared.AssemblyVersion.EndCopyrightYear+ " Infragistics, Inc.")]
[assembly: AssemblyTrademark("UltraWinGrid")]
[assembly: AssemblyCulture("")]		
[assembly: System.CLSCompliant(true)]		
[assembly: System.Security.AllowPartiallyTrustedCallers()]		 

// JJD 12/16/02 - FxCop
// Added ComVisible attribute to avoid fxcop violation
[assembly: System.Runtime.InteropServices.ComVisible(true)]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Revision
//      Build Number
//
// You can specify all the value or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

// this needs to be bumped after we release anything to the public
[assembly: AssemblyVersion(Infragistics.Shared.AssemblyVersion.Version)]
[assembly: AssemblyFileVersion(Infragistics.Shared.AssemblyVersion.Version)] // AS 7/29/08

//
// In order to sign your assembly you must specify a key to use. Refer to the 
// Microsoft .NET Framework documentation for more information on assembly signing.
//
// Use the attributes below to control which key is used for signing. 
//
// Notes: 
//   (*) If no key is specified - the assembly cannot be signed.
//   (*) KeyName refers to a key that has been installed in the Crypto Service
//       Provider (CSP) on your machine. 
//   (*) If the key file and a key name attributes are both specified, the 
//       following processing occurs:
//       (1) If the KeyName can be found in the CSP - that key is used.
//       (2) If the KeyName does not exist and the KeyFile does exist, the key 
//           in the file is installed into the CSP and used.
//   (*) Delay Signing is an advanced option - see the Microsoft .NET Framework
//       documentation for more information on this.
//
// MBS 8/9/07 
// We're now always delay-signing through the project settings
//
//#if DEBUG
//    [assembly: AssemblyDelaySign(false)]
//#else
//    [assembly: AssemblyDelaySign(true)]
//#endif


// JJD 3/19/04 - DNF159 added attribute to prevent looking for satellite
// assemblies when the language is en-US 
[assembly: System.Resources.NeutralResourcesLanguageAttribute("en-US")]

[assembly: System.Resources.SatelliteContractVersion(Infragistics.Shared.AssemblyVersion.SatelliteContractVersion)]

class AssemblyRef
{
	public const string BaseResourceName = "Infragistics.Win.UltraWinGrid.ExcelExport.strings";

	private const string AssemblyPrefix = "Infragistics2";

	internal const string AssemblyName = AssemblyPrefix+".Win.UltraWinGrid.ExcelExport.v" + Infragistics.Shared.AssemblyVersion.MajorMinor;
	internal const string AssemblyProduct = AssemblyPrefix+".Win.UltraWinGrid.ExcelExport";
	internal const string AssemblyDescriptionBase = "Infragistics UltraWinGrid Excel Export class library";

	internal const string Configuration = Infragistics.Shared.AssemblyVersion.Configuration;

	internal const string ProductTitleSuffix = "";
}

