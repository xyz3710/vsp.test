#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


#region XPS License
/*----------------------------------------------------------------------*
* Note: To the best of our knowledge, Microsoft® doesn’t forbid         *
* third-party companies from developing a product for the XPS document  *
* generation. However, according to the XML Paper Specification Patent  *
* License, we need to provide the following notice for your information:* 
*                                                                       *
* This product may incorporate intellectual property owned by Microsoft *
* Corporation. The terms and conditions upon which Microsoft is         *
* licensing such intellectual property may be found at                  *
* http://go.microsoft.com/fwlink/?LinkId=52369.;                        *
* ----------------------------------------------------------------------*
*/		
#endregion XPS License

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Drawing;
using System.Diagnostics;

using Infragistics.Win.Layout;
using System.IO;
using Infragistics.Win.UltraWinGrid.Design;

namespace Infragistics.Win.UltraWinGrid
{
    internal class GridExportCache : IDisposable
    {
        #region Private Members

		// MD 1/14/09 - Groups in RowLayout
		// This is no longer needed.
		//// UWG2786
		//private Hashtable dataSchema = new Hashtable();

        // BR12912
        // We need to cache the modified GridBagLayouts of each Band for efficiency. 
        private Hashtable columnHeaderGridBagLayoutManagersTable = null;
        private Hashtable valuesGridBagLayoutManagersTable = null;
        private Hashtable summariesGridBagLayoutManagersTable = null;

        // Keeps a table of Bands (key) so DocumentColumnInfo (value) objects
        private Dictionary<UltraGridBand, DocumentBandInfo> documentBandInfoTable = new Dictionary<UltraGridBand, DocumentBandInfo>();
        private Dictionary<UltraGridBand, DocumentBandInfo> documentBandHeaderInfoTable = new Dictionary<UltraGridBand, DocumentBandInfo>();
        private Dictionary<UltraGridBand, DocumentBandInfo> documentSummaryInfoTable = new Dictionary<UltraGridBand, DocumentBandInfo>();

        // MRS 5/20/2009 - TFS17800
        private Dictionary<SummaryDisplayAreaContext, DocumentBandInfo> documentSummaryRowInfoTable = new Dictionary<SummaryDisplayAreaContext, DocumentBandInfo>(new SummaryDisplayAreaContext.EqualityComparerWithoutGroupByRowContext());        

        private CacheType cacheType;

        private UltraGridLayout exportLayout;

        // MRS 7/31/2009 - TFS20051
        private Dictionary<RowsCollection, bool> rowIslandsWithHeaders;

        #endregion // Private Members

        #region Enums
        internal enum PlaceholderType { ColumnHeaders, Values, Summaries }

		// MD 1/12/09 - Groups in RowLayout
		// Renamed to DocumentHeaderInfoType
        //internal enum DocumentColumnInfoType
		internal enum DocumentHeaderInfoType
        {
            Cell,
            Header,
            Blank,
            Summary
        }

        internal enum CacheType
        {
            Excel, 
            Document
        }
        #endregion Enums

        #region Constructor 
        internal GridExportCache(CacheType cacheType)
        {
            this.cacheType = cacheType;
        }
        #endregion Constructor
        
        #region Properties

        #region ColumnHeaderGridBagLayoutManagersTable
        // BR12912
        internal Hashtable ColumnHeaderGridBagLayoutManagersTable
        {
            get
            {
                if (this.columnHeaderGridBagLayoutManagersTable == null)
                    this.columnHeaderGridBagLayoutManagersTable = new Hashtable();

                return this.columnHeaderGridBagLayoutManagersTable;
            }
        }
        #endregion ColumnHeaderGridBagLayoutManagersTable

		// MD 1/14/09 - Groups in RowLayout
		// This is no longer needed.
		

        #region Layout
        public UltraGridLayout Layout
        {
            get { return this.exportLayout;  }
            set { this.exportLayout = value; }
        }
        #endregion //Layout

        #region SummariesGridBagLayoutManagersTable
        // BR12912        
        internal Hashtable SummariesGridBagLayoutManagersTable
        {
            get
            {
                if (this.summariesGridBagLayoutManagersTable == null)
                    this.summariesGridBagLayoutManagersTable = new Hashtable();

                return this.summariesGridBagLayoutManagersTable;
            }
        }
        #endregion SummariesGridBagLayoutManagersTable

        #region ValuesGridBagLayoutManagersTable
        // BR12912
        internal Hashtable ValuesGridBagLayoutManagersTable
        {
            get
            {
                if (this.valuesGridBagLayoutManagersTable == null)
                    this.valuesGridBagLayoutManagersTable = new Hashtable();

                return this.valuesGridBagLayoutManagersTable;
            }
        }
        #endregion ValuesGridBagLayoutManagersTable

        #endregion // Properties

        #region Methods

		// MD 1/14/09 - Groups in RowLayout
		// This is no longer needed.
		#region Not Used

		

		#endregion Not Used

        #region BuildReportInfoCache
        internal void BuildReportInfoCache(UltraGridLayout layout)
        {
            this.ClearGridBagLayoutManagerCache();

            this.exportLayout = layout;
            foreach (UltraGridBand band in layout.Bands)
            {
                this.BuildReportInfoCache(band);
            }
        }

        private void BuildReportInfoCache(UltraGridBand band)
        {
            if (band.Hidden)
                return;

#pragma warning disable 0618
            if (band.UseRowLayout )
#pragma warning restore 0618
				this.BuildReportInfoCacheForRowLayoutRow(band);
            else if (band.Groups.Count > 0)
                this.BuildReportInfoCacheForGroupsAndLevelsRow(band);
            else
                this.BuildReportInfoCacheForNormalRow(band);
        }

        #region BuildReportInfoCacheForGroupsAndLevelsRow
        private void BuildReportInfoCacheForGroupsAndLevelsRow(UltraGridBand band)
        {
            // If all the columns in the band are hidden, then skip this band
            UltraGridColumn column = band.GetFirstVisibleCol(null, true);
            if (column == null)
                return;

            List<int> colDimsList = new List<int>(band.Columns.Count);
            colDimsList.Add(0);

            // In order to build the ColDims list and figure out the OriginX and SpanX of each 
            // column, we have to build a list of columns for each level. 

            int levelCount = band.LevelCount;
            column = column.GetRelatedVisibleColumn(VisibleRelation.First);

            // This is an array of lists. Each list contains the GroupLevelColumnInfo objects for
            // one level. The index into the array is the level. 
            GroupLevelColumnList[] groupLevelColumnLists = new GroupLevelColumnList[levelCount];
            GroupLevelColumnList groupLevelColumnList;

            for (int level = 0; level < levelCount; level++)
            {
                groupLevelColumnList = new GroupLevelColumnList(band, level);
                groupLevelColumnLists[level] = groupLevelColumnList;

                for (int i = 0; i < groupLevelColumnList.Count; i++)
                {
                    GroupLevelColumnInfo groupLevelColumnInfo = groupLevelColumnList[i];
                    if (colDimsList.Contains(groupLevelColumnInfo.RightEdge) == false)
                        colDimsList.Add(groupLevelColumnInfo.RightEdge);
                }
            }

            colDimsList.Sort();

            // MRS 8/7/2008 - BR35168
            //DocumentBandInfo documentBandInfo = new DocumentBandInfo(band);
            //DocumentBandInfo documentBandHeaderInfo = new DocumentBandInfo(band);
            //DocumentBandInfo documentBandSummaryInfo = new DocumentBandInfo(band);
            DocumentBandInfo documentBandInfo = new DocumentBandInfo(band, PlaceholderType.Values);
            DocumentBandInfo documentBandHeaderInfo = new DocumentBandInfo(band, PlaceholderType.ColumnHeaders);
            DocumentBandInfo documentBandSummaryInfo = new DocumentBandInfo(band, PlaceholderType.Summaries);

            for (int level = 0; level < groupLevelColumnLists.Length; level++)
            {
                int originX = 0;
                int originY = level;
                int spanY = 1;
                int spanX;

                groupLevelColumnList = groupLevelColumnLists[level];

                for (int i = 0; i < groupLevelColumnList.Count; i++)
                {
                    GroupLevelColumnInfo groupLevelColumnInfo = groupLevelColumnList[i];
                    int startingIndex = originX + 1;
                    int index = startingIndex;

                    while (colDimsList[index] != groupLevelColumnInfo.RightEdge)
                    {
                        index++;
                    }

                    spanX = (index - startingIndex) + 1;

                    DocumentHeaderInfo documentColumnInfo = new DocumentHeaderInfo(
						// MD 1/12/09 - Groups in RowLayout
						// The header is now used in the info.
                        //groupLevelColumnInfo.Column,
						groupLevelColumnInfo.Column.Header,
                        originX,                        //originX, 
                        originY,                        //originY
                        spanX,                          //spanX
                        spanY,                          //spanY
                        DocumentHeaderInfoType.Cell,
                        null                            //layoutItem
                        );

                    DocumentHeaderInfo documentHeaderInfo = new DocumentHeaderInfo(
						// MD 1/12/09 - Groups in RowLayout
						// The header is now used in the info.
                        //groupLevelColumnInfo.Column,
						groupLevelColumnInfo.Column.Header,
                        originX,                        //originX, 
                        originY,                        //originY
                        spanX,                          //spanX
                        spanY,                          //spanY
                        DocumentHeaderInfoType.Header,
                        null                            //layoutItem
                        );

                    DocumentHeaderInfo documentSummaryInfo = new DocumentHeaderInfo(
						// MD 1/12/09 - Groups in RowLayout
						// The header is now used in the info.
                        //groupLevelColumnInfo.Column,
						groupLevelColumnInfo.Column.Header,
                        originX,                        //originX, 
                        originY,                        //originY
                        spanX,                          //spanX
                        spanY,                          //spanY
                        DocumentHeaderInfoType.Summary,
                        null                            //layoutItem
                        );

                    documentBandInfo.Add(documentColumnInfo);
                    documentBandHeaderInfo.Add(documentHeaderInfo);
                    documentBandSummaryInfo.Add(documentSummaryInfo);

                    originX = index;
                }
            }

            int[] colDims = colDimsList.ToArray();

            // Rows can be variable-height, so we can't calculate the RowDims up front. 
            documentBandInfo.InitializeDims(colDims, null);
            documentBandSummaryInfo.InitializeDims(colDims, null);

            List<int> rowDimsList = new List<int>(band.LevelCount);
            int rowHeight = band.GetColumnHeaderHeight() / band.LevelCount;
            int rowDim = 0;
            rowDimsList.Add(rowDim);
            for (int i = 0; i < band.LevelCount; i++)
            {
                rowDim += rowHeight;
                rowDimsList.Add(rowDim);
            }

            documentBandHeaderInfo.InitializeDims(colDims, rowDimsList.ToArray());

            this.documentBandInfoTable[band] = documentBandInfo;
            this.documentBandHeaderInfoTable[band] = documentBandHeaderInfo;
            this.documentSummaryInfoTable[band] = documentBandSummaryInfo;
        }
        #endregion //BuildReportInfoCacheForGroupsAndLevelsRow

        #region BuildReportInfoCacheForRowLayoutRow
        private void BuildReportInfoCacheForRowLayoutRow(UltraGridBand band)
        {
            // Cells
            GridBagLayoutManager gridBagLayoutManager = this.GetCachedGridBagLayoutManager(band, PlaceholderType.Values);
            GridBagLayoutItemDimensionsCollection dims = this.GetLayoutItemDimensions(band, PlaceholderType.Values);

            // MRS 8/7/2008 - BR35168
            //DocumentBandInfo documentBandInfo = new DocumentBandInfo(band);
            DocumentBandInfo documentBandInfo = new DocumentBandInfo(band, PlaceholderType.Values);

            foreach (ILayoutItem layoutItem in dims)
            {
                DocumentHeaderInfoType documentColumnInfoType;
                if (layoutItem is UltraGridColumn)
                    documentColumnInfoType = DocumentHeaderInfoType.Cell;
				// MD 1/12/09 - Groups in RowLayout
				// GroupHeaders are now allowed too and they should also use the Header info type.
                //else if (layoutItem is ColumnHeader)
				else if ( layoutItem is HeaderBase )
					documentColumnInfoType = DocumentHeaderInfoType.Header;
                else                 
                {
                    Debug.Fail("Unknown LayoutItem type");
                    documentColumnInfoType = DocumentHeaderInfoType.Cell;
                }

				// MD 1/12/09 - Groups in RowLayout
				// The header is now used in the info.
                //UltraGridColumn column = RowLayoutColumnInfo.GetColumnFromLayoutItem(layoutItem);
				HeaderBase header = RowLayoutColumnInfo.GetHeaderFromLayoutItem( layoutItem );

                GridBagConstraint constraint = gridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;
                DocumentHeaderInfo documentColumnInfo = new DocumentHeaderInfo(
					// MD 1/12/09 - Groups in RowLayout
					// The header is now used in the info.
                    //column,
					header,

                    constraint.OriginX,
                    constraint.OriginY,
                    constraint.SpanX,
                    constraint.SpanY,
                    documentColumnInfoType,
                    layoutItem
                );

                documentBandInfo.Add(documentColumnInfo);
            }

            int rowCount = dims.RowDims.Length - 1;
            int colCount = dims.ColumnDims.Length - 1;

            this.FillInBlanks(documentBandInfo, colCount, rowCount);

            // Since the row height can change, we can't use the RowDims here. We have to get them from the
            // GridBagLayoutManager on every row, so we will use GetRowDims.
            // Make sure to call InitializeDims last, because this will trigger a sort. 
            documentBandInfo.InitializeDims(dims.ColumnDims, null);

            this.documentBandInfoTable[band] = documentBandInfo;     


            // Headers
            // MRS 5/29/07 - BR23087
            //if (band.RowLayoutLabelStyle == RowLayoutLabelStyle.Separate)
            if (band.RowLayoutLabelStyle == RowLayoutLabelStyle.Separate
                && band.ColHeadersVisible == true)
            {
                gridBagLayoutManager = this.GetCachedGridBagLayoutManager(band, PlaceholderType.ColumnHeaders);
                dims = this.GetLayoutItemDimensions(band, PlaceholderType.ColumnHeaders);

                // MRS 8/7/2008 - BR35168
                //documentBandInfo = new DocumentBandInfo(band);
                documentBandInfo = new DocumentBandInfo(band, PlaceholderType.ColumnHeaders);

                foreach (ILayoutItem layoutItem in dims)
                {
					// MD 1/12/09 - Groups in RowLayout
					// The header is now used in the info.
                    //UltraGridColumn column = RowLayoutColumnInfo.GetColumnFromLayoutItem(layoutItem);
					HeaderBase header = RowLayoutColumnInfo.GetHeaderFromLayoutItem( layoutItem );

                    GridBagConstraint constraint = gridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;
                    DocumentHeaderInfo documentColumnInfo = new DocumentHeaderInfo(
						// MD 1/12/09 - Groups in RowLayout
						// The header is now used in the info.
                        //column,
						header,

                        constraint.OriginX,
                        constraint.OriginY,
                        constraint.SpanX,
                        constraint.SpanY,
                        DocumentHeaderInfoType.Header, 
                        layoutItem
                        );

                    documentBandInfo.Add(documentColumnInfo);
                }

                rowCount = dims.RowDims.Length - 1;
                colCount = dims.ColumnDims.Length - 1;

                this.FillInBlanks(documentBandInfo, colCount, rowCount );

                // Make sure to call InitializeDims last, because this will trigger a sort. 
                documentBandInfo.InitializeDims(dims.ColumnDims, dims.RowDims);

                this.documentBandHeaderInfoTable[band] = documentBandInfo;
            }

            // MRS 5/20/2009 - TFS17800            
            // Can't use a single DocumentBandInfo for all of the summaries, because the 
            // Summaries can vary depending on location
            //
            #region Old Code
            //if (band.Summaries.Count > 0)
            //{
            //    gridBagLayoutManager = this.GetCachedGridBagLayoutManager(band, PlaceholderType.Summaries);
            //    dims = this.GetLayoutItemDimensions(band, PlaceholderType.Summaries);

            //    // MRS 8/7/2008 - BR35168
            //    //documentBandInfo = new DocumentBandInfo(band);
            //    documentBandInfo = new DocumentBandInfo(band, PlaceholderType.Summaries);

                //foreach (ILayoutItem layoutItem in dims)
                //{
                //    DocumentHeaderInfoType documentColumnInfoType;

                //    // MD 1/12/09 - Groups in RowLayout
                //    // GroupHeaders are now allowed too and they should also use the Header info type.
                //    //if (layoutItem is ColumnHeader)
                //    if ( layoutItem is HeaderBase )
                //        documentColumnInfoType = DocumentHeaderInfoType.Header;
                //    else
                //        documentColumnInfoType = DocumentHeaderInfoType.Summary;

                //    // MD 1/12/09 - Groups in RowLayout
                //    // The header is now used in the info.
                //    //UltraGridColumn column = RowLayoutColumnInfo.GetColumnFromLayoutItem(layoutItem);
                //    HeaderBase header = RowLayoutColumnInfo.GetHeaderFromLayoutItem( layoutItem );

                //    GridBagConstraint constraint = gridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;
                //    DocumentHeaderInfo documentColumnInfo = new DocumentHeaderInfo(
                //        // MD 1/12/09 - Groups in RowLayout
                //        // The header is now used in the info.
                //        //column,
                //        header,

                //        constraint.OriginX,
                //        constraint.OriginY,
                //        constraint.SpanX,
                //        constraint.SpanY,
                //        documentColumnInfoType,
                //        layoutItem
                //    );

            //        documentBandInfo.Add(documentColumnInfo);
            //    }

            //    rowCount = dims.RowDims.Length - 1;
            //    colCount = dims.ColumnDims.Length - 1;

            //    this.FillInBlanks(documentBandInfo, colCount, rowCount);

            //    // Since the row height can change, we can't use the RowDims here. We have to get them from the
            //    // GridBagLayoutManager on every row, so we will use GetRowDims.
            //    // Make sure to call InitializeDims last, because this will trigger a sort. 
            //    documentBandInfo.InitializeDims(dims.ColumnDims, null);

            //    this.documentSummaryInfoTable[band] = documentBandInfo;
            //}
            #endregion // Old Code
        }        

        #endregion //BuildReportInfoCacheForRowLayoutRow

        #region BuildReportInfoCacheForNormalRow
        private void BuildReportInfoCacheForNormalRow(UltraGridBand band)
        {
            // If all the columns in the band are hidden, then skip this band
            UltraGridColumn column = band.GetFirstVisibleCol(null, true);

            // MRS 5/23/07 - BR23084
            //if (column == null)
            //    return;

            List<int> colDimsList = new List<int>(band.Columns.Count);

            colDimsList.Add(0);
            int columnRightEdge = 0;

            // MRS 8/7/2008 - BR35168
            //DocumentBandInfo documentBandInfo = new DocumentBandInfo(band);
            //DocumentBandInfo documentBandHeaderInfo = new DocumentBandInfo(band);
            //DocumentBandInfo documentBandSummaryInfo = new DocumentBandInfo(band);
            DocumentBandInfo documentBandInfo = new DocumentBandInfo(band, PlaceholderType.Values);
            DocumentBandInfo documentBandHeaderInfo = new DocumentBandInfo(band, PlaceholderType.ColumnHeaders);
            DocumentBandInfo documentBandSummaryInfo = new DocumentBandInfo(band, PlaceholderType.Summaries);

            int[] colDims = null;

            // MRS 6/26/07 - BR24370
            // Can't use the VisiblePosition property of the column, since there may be hidden 
            // columns in the grid before it. 
            int columnIndex = 0;

            // MRS 5/23/07 - BR23084
            // Added 'if' block
            if (column != null)
            {
                column = column.GetRelatedVisibleColumn(VisibleRelation.First);

                do
                {
                    DocumentHeaderInfo documentColumnInfo = new DocumentHeaderInfo(
						// MD 1/12/09 - Groups in RowLayout
						// The header is now used in the info.
                        //column,
						column.Header,

                        // MRS 6/26/07 - BR24370
                        //column.Header.VisiblePosition,  //originX, 
                        columnIndex,  //originX, 
                        0,                              //originY
                        1,                              //spanX
                        1,                              //spanY
                        DocumentHeaderInfoType.Cell,
                        null                            //layoutItem
                        );

                    DocumentHeaderInfo documentHeaderInfo = new DocumentHeaderInfo(
						// MD 1/12/09 - Groups in RowLayout
						// The header is now used in the info.
                        //column,
						column.Header,

                        // MRS 6/26/07 - BR24370
                        //column.Header.VisiblePosition,  //originX, 
                        columnIndex,  //originX, 
                        0,                              //originY
                        1,                              //spanX
                        1,                              //spanY
                        DocumentHeaderInfoType.Header,
                        null                            //layoutItem
                        );

                    DocumentHeaderInfo documentSummaryInfo = new DocumentHeaderInfo(
						// MD 1/12/09 - Groups in RowLayout
						// The header is now used in the info.
                        //column,
						column.Header,

                        // MRS 6/26/07 - BR24370
                        //column.Header.VisiblePosition,  //originX, 
                        columnIndex,  //originX, 
                        0,                              //originY
                        1,                              //spanX
                        1,                              //spanY
                        DocumentHeaderInfoType.Summary,
                        null                            //layoutItem
                        );
                                        
                    columnRightEdge += column.CellSizeResolved.Width;
                    colDimsList.Add(columnRightEdge);

                    documentBandInfo.Add(documentColumnInfo);
                    documentBandHeaderInfo.Add(documentHeaderInfo);
                    documentBandSummaryInfo.Add(documentSummaryInfo);

                    column = column.GetRelatedVisibleColumn(VisibleRelation.Next);

                    // MRS 6/26/07 - BR24370
                    columnIndex++;

                } while (column != null);

                colDims = colDimsList.ToArray();

                // Rows can be variable-height, so we can't calculate the RowDims up front. 
                documentBandInfo.InitializeDims(colDims, null);
                documentBandSummaryInfo.InitializeDims(colDims, null);
            }

            int[] rowDims = new int[] { 0, band.GetColumnHeaderHeight() };
            documentBandHeaderInfo.InitializeDims(colDims, rowDims);

            this.documentBandInfoTable[band] = documentBandInfo;
            this.documentBandHeaderInfoTable[band] = documentBandHeaderInfo;
            this.documentSummaryInfoTable[band] = documentBandSummaryInfo;
        }
        #endregion //BuildReportInfoCacheForNormalRow

        // MRS 5/20/2009 - TFS17800        
        #region BuildReportInfoCacheForRowLayoutSummaryRow
        private void BuildReportInfoCacheForRowLayoutSummaryRow(UltraGridSummaryRow summaryRow)
        {
            UltraGridBand band = summaryRow.Band;
            Infragistics.Win.UltraWinGrid.Design.IGridDesignInfo designInfo = band.Layout.Grid;

            GridBagLayoutManager gridBagLayoutManager = designInfo.GetSummaryRowLayoutManager(summaryRow);
            GridBagLayoutManager compressedGridBagLayoutManager = this.GetCompressedGridBagLayoutManager(gridBagLayoutManager, PlaceholderType.Summaries);

            int width = band.GetExtent(BandOrigin.RowCellArea);
            GridBagLayoutItemDimensionsCollection dims = this.GetLayoutItemDimensions(compressedGridBagLayoutManager, width);

            // MRS 8/7/2008 - BR35168
            //documentBandInfo = new DocumentBandInfo(band);
            DocumentBandInfo documentBandInfo = new DocumentBandInfo(band, PlaceholderType.Summaries);

            foreach (ILayoutItem layoutItem in dims)
                {
                    DocumentHeaderInfoType documentColumnInfoType;

					// MD 1/12/09 - Groups in RowLayout
					// GroupHeaders are now allowed too and they should also use the Header info type.
                    //if (layoutItem is ColumnHeader)
					if ( layoutItem is HeaderBase )
                        documentColumnInfoType = DocumentHeaderInfoType.Header;
                    else
                        documentColumnInfoType = DocumentHeaderInfoType.Summary;

					// MD 1/12/09 - Groups in RowLayout
					// The header is now used in the info.
                    //UltraGridColumn column = RowLayoutColumnInfo.GetColumnFromLayoutItem(layoutItem);
					HeaderBase header = RowLayoutColumnInfo.GetHeaderFromLayoutItem( layoutItem );

                    GridBagConstraint constraint = compressedGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;
                    DocumentHeaderInfo documentColumnInfo = new DocumentHeaderInfo(
						// MD 1/12/09 - Groups in RowLayout
						// The header is now used in the info.
                        //column,
						header,

                        constraint.OriginX,
                        constraint.OriginY,
                        constraint.SpanX,
                        constraint.SpanY,
                        documentColumnInfoType,
                        layoutItem
                    );

                documentBandInfo.Add(documentColumnInfo);
            }

            int rowCount = dims.RowDims.Length - 1;
            int colCount = dims.ColumnDims.Length - 1;

            this.FillInBlanks(documentBandInfo, colCount, rowCount);

            // Since the row height can change, we can't use the RowDims here. We have to get them from the
            // GridBagLayoutManager on every row, so we will use GetRowDims.
            // Make sure to call InitializeDims last, because this will trigger a sort. 
            documentBandInfo.InitializeDims(dims.ColumnDims, null);

            this.documentSummaryRowInfoTable[summaryRow.SummaryDisplayAreaContext] = documentBandInfo;
        }
        #endregion //BuildReportInfoCacheForRowLayoutSummaryRow

        #endregion //BuildReportRowInfoCache

        #region FillInBlanks
        private void FillInBlanks(DocumentBandInfo documentBandInfo, int colCount, int rowCount)
        {
            bool[,] filledSpaces = new bool[colCount, rowCount];

            foreach (DocumentHeaderInfo documentColumnInfo in documentBandInfo)
            {
                int originX = documentColumnInfo.OriginX;
                int endingX = documentColumnInfo.OriginX + documentColumnInfo.SpanX;
                int originY = documentColumnInfo.OriginY;
                int endingY = documentColumnInfo.OriginY + documentColumnInfo.SpanY;

                for (int x = originX; x < endingX; x++)
                {
                    for (int y = originY; y < endingY; y++)
                        filledSpaces[x, y] = true;
                }
            }

            for (int x = 0; x < colCount; x++)
            {
                for (int y = 0; y < rowCount; y++)
                {
                    if (filledSpaces[x, y] == false)
                    {
                        DocumentHeaderInfo documentColumnInfo = new DocumentHeaderInfo(
                            null,
                            x,
                            y,
                            1,
                            1,
                            DocumentHeaderInfoType.Blank,
                            null                            //layoutItem
                            );

                        documentBandInfo.Add(documentColumnInfo);
                    }
                }
            }
        }
        #endregion //FillInBlanks

		// MD 1/14/09 - Groups in RowLayout
		// This is no longer needed.
		#region Not Used

		

		#endregion Not Used

        #region GetDocumentBandInfo
        public DocumentBandInfo GetDocumentBandInfo(UltraGridBand band, PlaceholderType placeHolderType)
        {
            switch (placeHolderType)
            {
                case PlaceholderType.Values:
                    return this.documentBandInfoTable[band];
                case PlaceholderType.ColumnHeaders:
                    return this.documentBandHeaderInfoTable[band];
                case PlaceholderType.Summaries:
                    // MRS 5/20/2009 - TFS17800
                    Debug.Assert(band.RowLayoutStyle == RowLayoutStyle.None,  "This should not be called. It is inherently innacurate since each SummaryRow may have some summaries and not others depending on it's location. Call GetDocumentBandInfo(UltraGridSummaryRow), instead.");
                    return this.documentSummaryInfoTable[band];
                default:
                    Debug.Fail("Unknown PlaceholderType");
                    return this.documentBandInfoTable[band];
            }
        }

        // MRS 5/20/2009 - TFS17800
        public DocumentBandInfo GetDocumentBandInfo(UltraGridSummaryRow summaryRow)
        {
            UltraGridBand band = summaryRow.Band;

            if (band.RowLayoutStyle == RowLayoutStyle.None)
                return this.GetDocumentBandInfo(band, PlaceholderType.Summaries);

            DocumentBandInfo documentBandInfo;
            bool success = this.documentSummaryRowInfoTable.TryGetValue(summaryRow.SummaryDisplayAreaContext, out documentBandInfo);
            if (success)
                return documentBandInfo;
            
            this.BuildReportInfoCacheForRowLayoutSummaryRow(summaryRow);

            success = this.documentSummaryRowInfoTable.TryGetValue(summaryRow.SummaryDisplayAreaContext, out documentBandInfo);
            if (success)
                return documentBandInfo;

            Debug.Fail("Should never get here. The call to BuildReportInfoCacheForRowLayoutSummaryRow should cause the documentSummaryRowInfoTable to contain the LayoutManager and get a DocumentBandInfo");

            return null;
        }
        #endregion //GetDocumentBandInfo

        #region GetLayoutItemDimensions
        private GridBagLayoutItemDimensionsCollection GetLayoutItemDimensions(UltraGridBand band, PlaceholderType placeholderType)
        {            
            GridBagLayoutManager gridBagLayoutManager = GetCachedGridBagLayoutManager(band, placeholderType);

            int width = band.GetExtent(BandOrigin.RowCellArea);

            // MRS 5/20/2009 - TFS17800
            //int height = 0;
            //foreach (int rowHeight in gridBagLayoutManager.RowHeights)
            //    height += rowHeight;

            //Size rowSize = new Size(width, height);

            //return this.GetLayoutItemDimensions(gridBagLayoutManager, rowSize);
            //
            return this.GetLayoutItemDimensions(gridBagLayoutManager, width);
        }

        // MRS 5/20/2009 - TFS17800
        private GridBagLayoutItemDimensionsCollection GetLayoutItemDimensions(GridBagLayoutManager gridBagLayoutManager, int bandWidth)
        {            
            int height = 0;
            foreach (int rowHeight in gridBagLayoutManager.RowHeights)
                height += rowHeight;

            Size rowSize = new Size(bandWidth, height);

            return this.GetLayoutItemDimensions(gridBagLayoutManager, rowSize);
        }

        private GridBagLayoutItemDimensionsCollection GetLayoutItemDimensions(UltraGridRow row)
        {
            UltraGridBand band = row.Band;
            int width = band.GetExtent(BandOrigin.RowCellArea);
            int height = row.Height;
            Size rowSize = new Size(width, height);

            GridBagLayoutManager gridBagLayoutManager = this.GetGridBagLayoutManager(row);
            return this.GetLayoutItemDimensions(gridBagLayoutManager, rowSize);            
        }

        private GridBagLayoutItemDimensionsCollection GetLayoutItemDimensions(GridBagLayoutManager gridBagLayoutManager, Size rowSize)
        {
            GridBagLayoutItemDimensionsCollection layoutItemDimensions = gridBagLayoutManager.GetLayoutItemDimensions(new LayoutContainerCalcSize(rowSize), null);
            return layoutItemDimensions;
        }
        #endregion GetLayoutItemDimensions

        #region GetRowDims
        internal int[] GetRowDimsForRow(UltraGridRow row)
        {
            UltraGridBand band = row.Band;
#pragma warning disable 0618
			if ( band.UseRowLayout )
#pragma warning restore 0618
            {
                GridBagLayoutItemDimensionsCollection layoutItemDimensionsCellArea = this.GetLayoutItemDimensions(row);
                return layoutItemDimensionsCellArea.RowDims;
            }            
            else if (band.Groups.Count > 0)
            {
                List<int> rowDimsList = new List<int>(band.LevelCount);
                int rowHeight = row.Height / band.LevelCount;
                int rowDim = 0;                
                rowDimsList.Add(rowDim);
                for (int i = 0; i < band.LevelCount; i++)
                {
                    rowDim += rowHeight;
                    rowDimsList.Add(rowDim);
                }

                return rowDimsList.ToArray();
            }
            else if (row.IsSummaryRow)
            {
                UltraGridSummaryRow summaryRow = (UltraGridSummaryRow)row;
                SummaryValuesCollection summaryValues = summaryRow.ParentCollection.SummaryValues;
                int fixedFooterAreaHeight = summaryValues.GetFixedFooterAreaHeight(summaryRow);
                return new int[] { 0, fixedFooterAreaHeight };             
            }
            else
            {
                return new int[] { 0, row.Height };
            }
        }

        internal int[] GetRowDimsForHeader(UltraGridBand band)
        {
#pragma warning disable 0618
			if ( band.UseRowLayout )
#pragma warning restore 0618
            {
                GridBagLayoutItemDimensionsCollection layoutItemDimensionsHeaderArea = this.GetLayoutItemDimensions(band,  PlaceholderType.ColumnHeaders);
                return layoutItemDimensionsHeaderArea.RowDims;
            }
            else
            {
                DocumentBandInfo documentBandInfo = this.GetDocumentBandInfo(band, PlaceholderType.ColumnHeaders);
                return documentBandInfo.HeaderRowDims;
            }
        }
        #endregion //GetRowDims

        #region GridBagLayoutCaching

        #region ClearGridBagLayoutManagerCache
        internal void ClearGridBagLayoutManagerCache()
        {
            this.columnHeaderGridBagLayoutManagersTable = null;
            this.valuesGridBagLayoutManagersTable = null;
            this.summariesGridBagLayoutManagersTable = null;

            this.documentBandInfoTable.Clear();
            this.documentBandHeaderInfoTable.Clear();
            this.documentSummaryInfoTable.Clear();

            // MRS 5/20/2009 - TFS17800
            this.documentSummaryRowInfoTable.Clear();

            // MRS 7/31/2009 - TFS20051
            if (rowIslandsWithHeaders != null)
                this.rowIslandsWithHeaders.Clear();            

            this.exportLayout = null;
        }
        #endregion ClearGridBagLayoutManagerCache

        #region GetCachedGridBagLayoutManager
        internal GridBagLayoutManager GetCachedGridBagLayoutManager(UltraGridBand band, GridExportCache.PlaceholderType placeholderType)
        {
            Hashtable table = null;
            switch (placeholderType)
            {
                case GridExportCache.PlaceholderType.ColumnHeaders:
                    table = this.ColumnHeaderGridBagLayoutManagersTable;
                    break;
                case GridExportCache.PlaceholderType.Values:
                    table = this.ValuesGridBagLayoutManagersTable;
                    break;
                case GridExportCache.PlaceholderType.Summaries:
                    // MRS 5/20/2009 - TFS17800
                    Debug.Fail("This should not be called. It is inherently innacurate since each SummaryRow may have some summaries and not others depending on it's location. Call GetCachedGridBagLayoutManager(UltraGridSummaryRow), instead.");
                    table = this.SummariesGridBagLayoutManagersTable;
                    break;
                default:
                    throw new Exception("Internal: Unknown GridExportCache.PlaceholderType");
            }

            GridBagLayoutManager gridBagLayoutManager = table[band] as GridBagLayoutManager;
            if (gridBagLayoutManager == null)
            {
                gridBagLayoutManager = GetGridBagLayoutManager(band, placeholderType);
                table[band] = gridBagLayoutManager;
            }

            return gridBagLayoutManager;
        }

        // MRS 8/17/2009 - TFS20658 (see also TFS17800)
        // We need to be able to get the GridBagLayoutManager from a summaryRow. 
        //
        internal GridBagLayoutManager GetCachedGridBagLayoutManager(UltraGridSummaryRow summaryRow)
        {
            Hashtable table = this.SummariesGridBagLayoutManagersTable;

            GridBagLayoutManager gridBagLayoutManager = table[summaryRow] as GridBagLayoutManager;
            if (gridBagLayoutManager == null)
            {
                gridBagLayoutManager = GetGridBagLayoutManager(summaryRow);
                table[summaryRow] = gridBagLayoutManager;
            }

            return gridBagLayoutManager;
        }
        
        #endregion GetCachedGridBagLayoutManager

        #region GetGridBagLayoutManager
        internal GridBagLayoutManager GetGridBagLayoutManager(UltraGridBand band, GridExportCache.PlaceholderType placeholderType)
        {
            Infragistics.Win.UltraWinGrid.Design.IGridDesignInfo designInfo = band.Layout.Grid;

            // Store the LayoutManager we get from the grid in a variable.
            // We will take this layout manager and build a new one without any 0-size rows
            // and columns. 
            GridBagLayoutManager gridBagLayoutManager = null;

            switch (placeholderType)
            {
                case GridExportCache.PlaceholderType.ColumnHeaders:
                    switch (band.RowLayoutLabelStyle)
                    {
                        case RowLayoutLabelStyle.Separate:
                            gridBagLayoutManager = designInfo.GetHeaderAreaGridBagLayoutManager(band);
                            break;
                        case RowLayoutLabelStyle.WithCellData:
                            gridBagLayoutManager = designInfo.GetCellAreaGridBagLayoutManager(band);
                            break;
                        default:
                            throw new Exception("Internal: RowLayoutLabelStyle");
                    }
                    break;

                case GridExportCache.PlaceholderType.Values:
                    gridBagLayoutManager = designInfo.GetCellAreaGridBagLayoutManager(band);
                    break;

                case GridExportCache.PlaceholderType.Summaries:
                    // MRS 5/20/2009 - TFS17800
                    Debug.Fail("This should not be called. It is inherently innacurate since each SummaryRow may have some summaries and not others depending on it's location. Call GetDocumentBandInfo(UltraGridSummaryRow), instead.");
                    gridBagLayoutManager = designInfo.GetSummaryAreaGridBagLayoutManager(band);
                    break;

                default:
                    throw new Exception("Internal: unknown GridExportCache.PlaceholderType");
            }

            gridBagLayoutManager = this.GetCompressedGridBagLayoutManager(gridBagLayoutManager, placeholderType);            

            return gridBagLayoutManager;
        }

        internal GridBagLayoutManager GetGridBagLayoutManager(UltraGridRow row)
        {
            UltraGridBand band = row.Band;
            Infragistics.Win.UltraWinGrid.Design.IGridDesignInfo designInfo = band.Layout.Grid;

            // Store the LayoutManager we get from the grid in a variable.
            // We will take this layout manager and build a new one without any 0-size rows
            // and columns. 
            GridBagLayoutManager gridBagLayoutManager = null;

            gridBagLayoutManager = designInfo.GetCellAreaGridBagLayoutManager(row);

            gridBagLayoutManager = this.GetCompressedGridBagLayoutManager(gridBagLayoutManager, PlaceholderType.Values);            

            return gridBagLayoutManager;
        }

        // MRS 8/17/2009 - TFS20658 (see also TFS17800)
        // We need to be able to get the GridBagLayoutManager from a Summary Row
        internal GridBagLayoutManager GetGridBagLayoutManager(UltraGridSummaryRow summaryRow)
        {
            UltraGridBand band = summaryRow.Band;
            Infragistics.Win.UltraWinGrid.Design.IGridDesignInfo designInfo = band.Layout.Grid;

            // Store the LayoutManager we get from the grid in a variable.
            // We will take this layout manager and build a new one without any 0-size rows
            // and columns. 
            GridBagLayoutManager gridBagLayoutManager = null;

            gridBagLayoutManager = designInfo.GetSummaryRowLayoutManager(summaryRow);

            gridBagLayoutManager = this.GetCompressedGridBagLayoutManager(gridBagLayoutManager, PlaceholderType.Summaries);

            return gridBagLayoutManager;
        }
        #endregion GetGridBagLayoutManager

        #region GetCompressedGridBagLayoutManager
        // This method takes a GridBagLayoutManager and creates a new one without
        // any 0-sized rows or columns. 
        private GridBagLayoutManager GetCompressedGridBagLayoutManager(GridBagLayoutManager gridBagLayoutManager, GridExportCache.PlaceholderType placeholderType)        
        {
            // MRS 6/27/07 - BR24452
            if (gridBagLayoutManager == null)
                return null;

            Hashtable originalContraints = new Hashtable(gridBagLayoutManager.LayoutItems.Count);
            foreach (ILayoutItem layoutItem in gridBagLayoutManager.LayoutItems)
            {
                IGridBagConstraint iGridBagConstraint = gridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as IGridBagConstraint;
                GridBagConstraint gridBagConstraint = new GridBagConstraint(iGridBagConstraint.OriginX, iGridBagConstraint.OriginY, iGridBagConstraint.SpanX, iGridBagConstraint.SpanY, iGridBagConstraint.WeightX, iGridBagConstraint.WeightY, iGridBagConstraint.Anchor, iGridBagConstraint.Fill, iGridBagConstraint.Insets);
                originalContraints[layoutItem] = gridBagConstraint;
            }

            GridBagLayoutManager exportGridBagLayoutManager = new GridBagLayoutManager();
            foreach (ILayoutItem layoutItem in originalContraints.Keys)
            {
                ILayoutItem key = layoutItem;
                object constraint = originalContraints[layoutItem];

                // If the PlaceHolderType is a a summary, the grid will use a SummaryLayoutItem
                // as the LayoutItem. This is an internal class, so we can't use it here. So we 
                // will just use the grid column, instead. 
                // MRS v7.2 - Document Exporter
                //
                //if (placeholderType == GridExportCache.PlaceholderType.Summaries)
                if (this.cacheType == CacheType.Excel &&
                    placeholderType == GridExportCache.PlaceholderType.Summaries)
                {
					// MD 1/12/09 - Groups in RowLayout
					// The header is now used in the info, but if the column is valid, we need to use it.
                    //key = RowLayoutColumnInfo.GetColumnFromLayoutItem(key);
					ILayoutItem originalKey = key;
					bool keyIsHeader = ( originalKey is HeaderBase );

					if ( keyIsHeader == false )
						key = RowLayoutColumnInfo.GetColumnFromLayoutItem( originalKey );

					if ( key == null || keyIsHeader )
						key = RowLayoutColumnInfo.GetHeaderFromLayoutItem( originalKey );
                }

                exportGridBagLayoutManager.LayoutItems.Add(key, constraint);
            }

            int currentRow = 0;

            exportGridBagLayoutManager.InvalidateLayout();

            while (currentRow < exportGridBagLayoutManager.RowHeights.Length
                // MRS 2/23/07 - BR20309
                // This was getting into an infinite loop if there is only one row in the layout
                // and it's height is already 0 (like when ColHeadersVisible is false). 
                // If there's only one row, there's nothing to collapse, so bail out. 
                //
                && exportGridBagLayoutManager.RowHeights.Length > 1)
            {
                if (exportGridBagLayoutManager.RowHeights[currentRow] == 0)
                {
                    // MRS 3/11/2008 - BR31054
                    // Keep a list of LayoutItems whose SpanY has been reduced to 0.
                    ArrayList listOfLayoutItemsToRemove = null;

                    foreach (ILayoutItem layoutItem in exportGridBagLayoutManager.LayoutItems)
                    {
                        GridBagConstraint gridBagConstraint = exportGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;

                        // See if this contraint intersect with the current (0-height) row.                             
                        if (gridBagConstraint.OriginY <= currentRow &&
                            currentRow < (gridBagConstraint.OriginY + gridBagConstraint.SpanY))
                            gridBagConstraint.SpanY--;

                        // MRS 3/11/2008 - BR31054
                        // Keep a list of LayoutItems whose SpanY has been reduced to 0.
                        if (gridBagConstraint.SpanY == 0)
                        {
                            if (listOfLayoutItemsToRemove == null)
                                listOfLayoutItemsToRemove = new ArrayList();

                            listOfLayoutItemsToRemove.Add(layoutItem);
                        }
                    }

                    foreach (ILayoutItem layoutItem in exportGridBagLayoutManager.LayoutItems)
                    {
                        GridBagConstraint gridBagConstraint = exportGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;
                        if (gridBagConstraint.OriginY > currentRow)
                            gridBagConstraint.OriginY--;
                    }

                    // MRS 3/11/2008 - BR31054
                    // Remove any Layout items flagged for removal. 
                    if (listOfLayoutItemsToRemove != null)
                    {
                        while (listOfLayoutItemsToRemove.Count > 0)
                        {
                            exportGridBagLayoutManager.LayoutItems.Remove(listOfLayoutItemsToRemove[0] as ILayoutItem);
                            listOfLayoutItemsToRemove.Remove(listOfLayoutItemsToRemove[0]);
                        }
                        listOfLayoutItemsToRemove = null;
                    }

                    exportGridBagLayoutManager.InvalidateLayout();
                }
                else
                    currentRow++;
            }

            int currentColumn = 0;
            exportGridBagLayoutManager.InvalidateLayout();
            while (currentColumn < exportGridBagLayoutManager.ColumnWidths.Length)
            {
                if (exportGridBagLayoutManager.ColumnWidths[currentColumn] == 0)
                {
                    // MRS 4/24/07 - BR22016
                    // I added a double-fix for this issue. We will keep track of whether anything changed. 
                    // This will avoid infinite loops in the future. We will also keep a list of 0-width
                    // items so that we can remove them from the LayoutItems collection. 
                    //
                    bool somethingChanged = false;
                    ArrayList layoutItemsToRemove = new ArrayList();

                    foreach (ILayoutItem layoutItem in exportGridBagLayoutManager.LayoutItems)
                    {
                        GridBagConstraint gridBagConstraint = exportGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;

                        // See if this contraint intersect with the current (0-height) row.                             
                        if (gridBagConstraint.OriginX <= currentColumn &&
                            currentColumn < (gridBagConstraint.OriginX + gridBagConstraint.SpanX))
                        {
                            gridBagConstraint.SpanX--;

                            // MRS 4/24/07 - BR22016                            
                            somethingChanged = true;
                            if (gridBagConstraint.SpanX == 0)
                            {
                                // If the SpanX of the item was set to 0, we want this to mean
                                // that the item is 0-width. Normally, 0 means fill the remaining space. 
                                // So we will remove the item from the list. We can't do it inside the foreach
                                // loop, though. So store the items that need to be removed in a list.                                 
                                if (layoutItemsToRemove.Contains(layoutItem) == false)
                                    layoutItemsToRemove.Add(layoutItem);
                            }
                        }
                    }

                    // MRS 4/24/07 - BR22016
                    // Remove any items from the list that need to be removed. See comment above. 
                    foreach (ILayoutItem layoutItemToRemove in layoutItemsToRemove)
                        exportGridBagLayoutManager.LayoutItems.Remove(layoutItemToRemove);

                    foreach (ILayoutItem layoutItem in exportGridBagLayoutManager.LayoutItems)
                    {
                        GridBagConstraint gridBagConstraint = exportGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;
                        if (gridBagConstraint.OriginX > currentColumn)
                        {
                            gridBagConstraint.OriginX--;

                            // MRS 4/24/07 - BR22016
                            // Mark that something changed. This will help us prevent infinite loops. 
                            somethingChanged = true;
                        }
                    }

                    // MRS 4/24/07 - BR22016                    
                    // If nothing changed, don't invalidate the layout, just skip ahead to the next column. 
                    //exportGridBagLayoutManager.InvalidateLayout();
                    //                    
                    if (somethingChanged)
                        exportGridBagLayoutManager.InvalidateLayout();
                    else
                        currentColumn++;
                }
                else
                    currentColumn++;
            }

            return exportGridBagLayoutManager;
        }
        #endregion GetCompressedGridBagLayoutManager

        #endregion GridBagLayoutCaching

        // MRS 6/3/2009 - TFS17908
        // Moved this from UltraGridDocumentExporter and made it an internal static method
        // so that it can be used in the ExcelExporter. 
        //
        #region GetImageRendererImage

        internal static System.Drawing.Image GetImageRendererImage(UltraGridRow gridRow, UltraGridColumn gridColumn, object exportValue)
        {
            EmbeddableImageRenderer embeddableImageRenderer = gridRow.GetEditorResolved(gridColumn) as EmbeddableImageRenderer;
            if (embeddableImageRenderer == null)
            {
                Debug.Fail("GetImageRendererImage is only valid for cells that are using an EmbeddableImageRenderer");
                return null;
            }

            EmbeddableEditorOwnerBase editorOwner;
            object ownerContext;
            GridExportCache.GetOwnerInfo(gridRow, gridColumn, out editorOwner, out ownerContext);
            Stream imageStream;
            bool disposeImage;
            return embeddableImageRenderer.GetImage(exportValue, editorOwner, ownerContext, out imageStream, out disposeImage);
        }

        #endregion //GetImageRendererImage

        // MRS 6/3/2009 - TFS17908
        // Moved this from the UltraGridDocumentExporter and made it internal so it can be 
        // used by the ExcelExporter.
        //
        #region GetOwnerInfo
        internal static void GetOwnerInfo(UltraGridRow gridRow, UltraGridColumn gridColumn, out EmbeddableEditorOwnerBase editorOwner, out object ownerContext)
        {
            IGridDesignInfo iGridDesignInfo = gridRow.Band.Layout.Grid as IGridDesignInfo;
            editorOwner = iGridDesignInfo.GetEditorOwner(gridRow, gridColumn);
            ownerContext = gridRow;
        }
        #endregion //GetOwnerInfo

        #endregion // Methods

        #region IDisposable Members

        public void Dispose()
        {
            this.ClearGridBagLayoutManagerCache();
        }

        #endregion

        #region LayoutContainerCalcSize Class

        internal class LayoutContainerCalcSize : ILayoutContainer
        {
            private Rectangle containerRect = Rectangle.Empty;

            #region Constructor

			// MD 1/14/09 - Groups in RowLayout
			// This is no longer needed.
			#region Not Used

			

			#endregion Not Used

            internal LayoutContainerCalcSize(Size size)
            {
                this.Initialize(size);
            }

			// MD 1/14/09 - Groups in RowLayout
			// This is no longer needed.
			#region Not Used

			

			#endregion Not Used

            #endregion // Constructor

            #region Initialize

			// MD 1/14/09 - Groups in RowLayout
			// This is no longer needed.
			#region Not Used

			

			#endregion Not Used

            internal void Initialize(Size size)
            {
                this.Initialize(new Rectangle(new Point(0, 0), size));
            }

            internal void Initialize(Rectangle rect)
            {
                this.containerRect = rect;
            }

            #endregion // Initialize

            #region Implementation of ILayoutContainer

            #region ILayoutContainer.PositionItem

            void ILayoutContainer.PositionItem(ILayoutItem item, System.Drawing.Rectangle rect, object containerContext)
            {
            }

            #endregion // ILayoutContainer.PositionItem

            #region ILayoutContainer.GetBounds

            System.Drawing.Rectangle ILayoutContainer.GetBounds(object containerContext)
            {
                return this.containerRect;
            }

            #endregion // ILayoutContainer.GetBounds

            #endregion // Implementation of ILayoutContainer
        }

        #endregion // LayoutContainerCalcSize Class

        #region DocumentBandInfo class
        internal class DocumentBandInfo : List<DocumentHeaderInfo>
        {
            #region Private Members

            private int[] colDims;
            private int[] headerRowDims;
            private UltraGridBand band;
            private int bandWidth;

			// MD 1/12/09 - Groups in RowLayout
			// The columnDictionary is no logner needed, so we don't have to do this anymore.
			//// MRS 6/20/07 - BR24108
			////private Dictionary<UltraGridColumn, DocumentColumnInfo> columnDictionary;
			//private Dictionary<DocumentColumnInfoId, DocumentHeaderInfo> columnDictionary;

            private List<UltraGridGroup> groupList;
            private GroupSortComparer groupSortComparer;

            // MRS 8/7/2008 - BR35168
            private int? groupByConnectorsExtent = null;
            PlaceholderType placeholderType;

            #endregion //Private Members

            // MRS 8/7/2008 - BR35168
            //public DocumentBandInfo(UltraGridBand band)
            public DocumentBandInfo(UltraGridBand band, PlaceholderType placeholderType)
            {
                this.band = band;

                // MRS 8/7/2008 - BR35168
                this.placeholderType = placeholderType;
            }
                        
            public void InitializeDims(int[] colDims, int[] headerRowDims)
            {                
                this.colDims = colDims;
                this.headerRowDims = headerRowDims;

                this.CalculateBandWidth();
                this.Sort();

                this.BuildGroupList();

				// MD 1/12/09 - Groups in RowLayout
				// The columnDictionary is no logner needed, so we don't have to do this anymore.
				//// MRS 6/20/07 - BR24108
				//this.BuildColumnDictionary();
            }

            private void BuildGroupList()
            {
                this.groupList = new List<UltraGridGroup>(this.band.Groups.Count);
                foreach (UltraGridGroup group in this.band.Groups)
                {
                    // MRS 2/3/2009 - TFS13371
                    // Added check to exclude hidden groups. 
                    //
                    if (group.Hidden == false)
                        this.groupList.Add(group);
                }

                this.groupList.Sort(this.MyGroupSortComparer);
            }            

            private void CalculateBandWidth()
            {                
                this.bandWidth = this.band.GetExtent(BandOrigin.RowCellArea);

                // MRS 8/28/2008 - BR35168
                this.bandWidth += this.GroupByConnectorsExtent;
            }

            public int[] ColDims
            {
                get { return this.colDims; }
            }

            public int[] HeaderRowDims
            {
                get { return this.headerRowDims; }
            }

            public UltraGridBand Band
            {
                get { return this.band; }
            }

            public int BandWidth
            {
                get { return this.bandWidth; }
            }

            public bool HasGroups
            {
                get { return this.groupList != null && this.groupList.Count > 0; }
            }

            public List<UltraGridGroup> GroupList
            {
                get 
                { 
                    return this.groupList; 
                }
                set
                {
                    this.groupList = value;
                }
            }

            private GroupSortComparer MyGroupSortComparer
            {
                get
                {
                    if (this.groupSortComparer == null)
                        this.groupSortComparer = new GroupSortComparer();

                    return this.groupSortComparer;
                }
            }

			// MD 1/12/09 - Groups in RowLayout
			// The columnDictionary is no logner needed, so we don't have to do this anymore.
			#region Removed

			

			#endregion Removed

			// MD 1/12/09 - Groups in RowLayout
			// Not Used
			#region Not Used

			

			#endregion Not Used

            // MRS 6/20/07 - BR24108
            #region GetColumnWidth

			// MD 1/12/09 - Groups in RowLayout
			// Not Used
			

            public int GetColumnWidth(DocumentHeaderInfo documentColumnInfo)
            {
                if (documentColumnInfo == null)
                    return 0;

                int start = this.colDims[documentColumnInfo.OriginX];
                int end = this.colDims[documentColumnInfo.OriginX + documentColumnInfo.SpanX];
                return end - start;
            }
            #endregion //GetColumnWidth

            #region GroupSortComparer
            private class GroupSortComparer : IComparer<UltraGridGroup>
            {
                #region IComparer<UltraGridGroup> Members

                int IComparer<UltraGridGroup>.Compare(UltraGridGroup x, UltraGridGroup y)
                {
                    return x.Header.VisiblePosition.CompareTo(y.Header.VisiblePosition);
                }

                #endregion
            }
            #endregion //GroupSortComparer

            // MRS 8/7/2008 - BR35168
            #region GroupByConnectorsExtent
            public int GroupByConnectorsExtent
            {
                get
                {
                    if (this.groupByConnectorsExtent == null)
                    {
                        if (this.placeholderType == PlaceholderType.ColumnHeaders &&
                            // We are currently not supporting exporting the headers this way. We 
                            // end up essentially using HeaderPlacement.RepeatOnBreak. So we don't
                            // want to indent.
                            this.band.HeaderPlacementResolved != HeaderPlacement.OncePerGroupedRowIsland)
                        {
                            this.groupByConnectorsExtent = this.band.CalcGroupByConnectorsExtent();
                        }
                        else
                            this.groupByConnectorsExtent = 0;
                    }

                    return (int)this.groupByConnectorsExtent;
                }
            }
            #endregion //GroupByConnectorsExtent

        }
        #endregion //DocumentBandInfo class

		#region DocumentHeaderInfo class

		// MD 1/12/09 - Groups in RowLayout
		// Renamed to DocumentHeaderInfo
        //internal class DocumentColumnInfo :IComparable<DocumentColumnInfo>
		internal class DocumentHeaderInfo : IComparable<DocumentHeaderInfo>
        {
            private int originX;
            private int originY;
            private int spanX;
            private int spanY;
            private DocumentHeaderInfoType documentColumnInfoType;
            private ILayoutItem layoutItem;

			// MD 1/12/09 - Groups in RowLayout
			// The header is now used in the info.
            //private UltraGridColumn column;
			private HeaderBase header;

			// MD 1/14/09 - Groups in RowLayout
			private Rectangle relativeRect;

			public DocumentHeaderInfo(
				// MD 1/12/09 - Groups in RowLayout
				// The header is now used in the info.
				//UltraGridColumn column,
				HeaderBase header,

				int originX,
				int originY,
				int spanX,
				int spanY,
				DocumentHeaderInfoType documentColumnInfoType,
				ILayoutItem layoutItem )
			{
                this.originX = originX;
                this.originY = originY;
                this.spanX = spanX;
                this.spanY = spanY;

				// MD 1/12/09 - Groups in RowLayout
				// The header is now used in the info.
                //this.column = column;

                this.documentColumnInfoType = documentColumnInfoType;
                this.layoutItem = layoutItem;

				// MD 1/12/09 - Groups in RowLayout
				// The header is now used in the info.
				this.header = header;

				Debug.Assert( 
					( this.header is GroupHeader ) == false || 
					this.documentColumnInfoType == DocumentHeaderInfoType.Header, 
					"A type of Header must be specified for group headers." );

				// MD 1/14/09 - Groups in RowLayout
				this.relativeRect = new Rectangle( this.originX, this.originY, this.spanX, this.spanY );
            }

            public int OriginX
            {
                get { return this.originX; }                
            }

            public int OriginY
            {
                get { return this.originY; }                
            }

            public int SpanX
            {
                get { return this.spanX; }
            }

            public int SpanY
            {
                get { return this.spanY; }
            }

			// MD 1/14/09 - Groups in RowLayout
			public Rectangle RelativeRect
			{
				get { return this.relativeRect; }
			}

            public UltraGridColumn Column
            {
				// MD 1/12/09 - Groups in RowLayout
				// The header is now used but we will keep the Column property for the code that needs to use it.
                //get { return this.column; }
				get 
				{
					if ( this.header == null )
						return null;

					return this.header.Column; 
				}
            }

			// MD 1/14/09 - Groups in RowLayout
			// Renamed because the enum was renamed.
            //public DocumentHeaderInfoType DocumentColumnInfoType
			public DocumentHeaderInfoType DocumentHeaderInfoType
            {
                get { return this.documentColumnInfoType; }                
            }

            public ILayoutItem LayoutItem
            {
                get { return this.layoutItem; }
            }

			// MD 1/12/09 - Groups in RowLayout
			public HeaderBase Header
			{
				get { return this.header; }
			}

			#region IComparable<DocumentHeaderInfo> Members

			public int CompareTo(DocumentHeaderInfo other)
            {
                if (this.OriginY != other.OriginY)
                    return this.OriginY.CompareTo(other.OriginY);

                if (this.OriginX != other.originX)
                    return this.OriginX.CompareTo(other.originX);

				// MD 1/12/09 - Groups in RowLayout
				// The header is now used in the info.
                //if (this.column == null || other.column == null)
				if ( this.header == null || other.header == null )
                    return 0;

				// MD 1/12/09 - Groups in RowLayout
				// The header is now used in the info.
                //return this.Column.Header.VisiblePosition.CompareTo(other.Column.Header.VisiblePosition);                
				return this.Header.VisiblePosition.CompareTo( other.Header.VisiblePosition );
			}

            #endregion
		}
		#endregion //DocumentHeaderInfo class

		// These two helper classes are for dealing with Groups and Levels
        #region GroupLevelColumnList
        private class GroupLevelColumnList : List<GroupLevelColumnInfo>
        {
            private UltraGridBand band;
            private int level;

            internal GroupLevelColumnList(UltraGridBand band, int level)
            {
                this.band = band;
                this.level = level;

                this.BuildList();
            }

            private void BuildList()
            {
                int rightEdge = 0;

                // If all the columns in the band are hidden, then skip this band
                UltraGridColumn column = band.GetFirstVisibleCol(null, true);

                // Keep track of the amount of unused space in each group. 
                Dictionary<UltraGridGroup, int> remainingGroupSpace = new Dictionary<UltraGridGroup, int>(this.band.Groups.Count);
                foreach (UltraGridGroup group in this.band.Groups)
                    remainingGroupSpace[group] = group.Width;

                while (column != null)
                {
                    UltraGridColumn nextColumn = column.GetRelatedVisibleColumn(VisibleRelation.Next);

                    if (column.Level == this.level)
                    {
                        UltraGridGroup group = column.Group;

                        if (nextColumn != null
                            && nextColumn.Group == group
                            && nextColumn.Level == column.Level)
                        {
                            remainingGroupSpace[group] -= column.CellSizeResolved.Width;
                            rightEdge += column.CellSizeResolved.Width;
                        }
                        else
                        {
                            // If this is the last item in the group, we need to fill all remaining space in the group.
                            rightEdge += remainingGroupSpace[column.Group];
                        }

                        this.Add(new GroupLevelColumnInfo(column, rightEdge));
                    }                    

                    column = nextColumn;
                }

                this.Sort();
            }
        }
        #endregion //GroupLevelColumnList

        #region GroupLevelColumnInfo
        private class GroupLevelColumnInfo
            : IComparable<GroupLevelColumnInfo>
        {
            private UltraGridColumn column;
            private int rightEdge;            

            public GroupLevelColumnInfo(UltraGridColumn column, int rightEdge)
            {
                this.column = column;
                this.rightEdge = rightEdge;
            }

            public UltraGridColumn Column
            {
                get { return this.column; }
            }

            public int RightEdge
            {
                get { return this.rightEdge; }
            }
            
            

            #region IComparable<GroupLevelColumnInfo> Members

			// MD 1/24/08
			// Made changes to allow for VS2008 style unit test accessors
            //int IComparable<GroupLevelColumnInfo>.CompareTo(GroupLevelColumnInfo other)
			public int CompareTo( GroupLevelColumnInfo other )
            {
                return this.RightEdge.CompareTo(other.RightEdge);
            }

            #endregion
        }
        #endregion //GroupLevelColumnInfo

		// MD 1/12/09 - Groups in RowLayout
		// This is no longer needed
		#region Removed

		

		#endregion Removed

        // MRS 7/31/2009 - TFS20051
        #region DoesIslandHaveHeader

        internal bool DoesIslandHaveHeader(RowsCollection rows)
        {
            if (this.rowIslandsWithHeaders == null)
                this.rowIslandsWithHeaders = new Dictionary<RowsCollection, bool>();
            else
            {
                bool islandHasHeader = false;
                bool success = this.rowIslandsWithHeaders.TryGetValue(rows, out islandHasHeader);

                if (success && islandHasHeader)
                    return true;
            }

            this.rowIslandsWithHeaders[rows] = true;
            return false;
        }

        #endregion // DoesIslandHaveHeader
    }   
}
