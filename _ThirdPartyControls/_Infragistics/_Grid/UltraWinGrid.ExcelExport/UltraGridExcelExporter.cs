#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


#region XPS License
/*----------------------------------------------------------------------*
* Note: To the best of our knowledge, Microsoft® doesn’t forbid         *
* third-party companies from developing a product for the XPS document  *
* generation. However, according to the XML Paper Specification Patent  *
* License, we need to provide the following notice for your information:* 
*                                                                       *
* This product may incorporate intellectual property owned by Microsoft *
* Corporation. The terms and conditions upon which Microsoft is         *
* licensing such intellectual property may be found at                  *
* http://go.microsoft.com/fwlink/?LinkId=52369.;                        *
* ----------------------------------------------------------------------*
*/		
#endregion XPS License

using System;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Security.Permissions;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Collections.Specialized;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win;
using Infragistics.Excel;
using Infragistics.Win.Layout;
using Infragistics.Win.UltraWinGrid.ExcelExport.FormulaExporting;
using Infragistics.Win.CalcEngine;
using System.Text;
using System.Collections.Generic;

namespace Infragistics.Win.UltraWinGrid.ExcelExport
{
	/// <summary>
	/// UltraGridExporterHelper implements <see cref="IUltraGridExporter"/> and serves 
	/// as communication between grid and <see cref="UltraGridExcelExporter"/>. 
	/// Class is internal so user can't create helper and call 
	/// <see cref="UltraGrid.Export"/> method on the grid directly.
	/// </summary>
	internal class UltraGridExporterHelper: IUltraGridExporter
	{
		#region Member Variables

		private UltraGridExcelExporter excelExporter;

		public Worksheet CurrentWorksheet;
		public int HierarchyLevel;

		public Point gridStartPos;
		public Point currentPos;
		public Rectangle extentOfCurrentGridObject;

        // MRS 4/18/07
        // I discovered while implementing the DocumentExporter that this variable is never actually used.         
		//public BitArray skipSiblings;

		public Hashtable columnExcelFormats;

		public UltraGridBand lastBand;

		// MRS 2/22/05 - BR00432
		private int currentIndentationLevel = 0;

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        public Dictionary<SummarySettings, string> summaryExcelFormats;
		#endregion Member Variables

		#region Constructor

		public UltraGridExporterHelper(UltraGridExcelExporter excelExporter, 
			Worksheet currentWorksheet, int startRow, int startColumn)
		{
			this.excelExporter = excelExporter;

			this.CurrentWorksheet = currentWorksheet;
			this.HierarchyLevel = 0;

			this.gridStartPos = new Point(startColumn, startRow);
			this.currentPos = this.gridStartPos;

			this.extentOfCurrentGridObject = new Rectangle(-1, -1, -1, -1);

			this.columnExcelFormats = null;

            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            this.summaryExcelFormats = null;

			this.lastBand = null;
		}

		#endregion Constructor

		#region Interface Implementation

		public void BeginExport(UltraGridLayout exportLayout, RowsCollection rows)
		{
			this.excelExporter.BeginExportInternal(this, exportLayout, rows);
		}

		public void ProcessRow(UltraGridRow row, ProcessRowParams processRowParams)
		{
			this.excelExporter.ProcessGridRowInternal(this, row, processRowParams);
		}

		public void EndExport(bool canceled)
		{
			this.excelExporter.EndExportInternal(this, canceled);
		}

		#endregion

		// MRS 2/22/05 - BR00432
		internal int CurrentIndentationLevel
		{
			get { return this.currentIndentationLevel; }
			set { this.currentIndentationLevel = value; }
		}       
	}

	/// <summary>
	/// UltraGridExcelExporter component provides excel exporting functionality to UltraGrid.
	/// </summary>	
	// MBS 6/8/09 - TFS18160
    //[ToolboxBitmap(typeof(UltraGridExcelExporter), AssemblyVersion.ToolBoxBitmapFolder + "UltraGridExcelExporter.bmp")] // MRS 11/13/05
   [ToolboxBitmap(typeof(UltraGridExcelExporter), AssemblyVersion.ToolBoxBitmapFolder + "UltraGridExcelExporter20.bmp")]
	
	
	[DefaultEvent("BeginExport")]
	[Designer(typeof(Infragistics.Shared.UltraComponentDesigner))] 
	[ToolboxItemFilter("System.Windows.Forms",ToolboxItemFilterType.Allow)]
	[LocalizedDescription("LD_UltraGridExcelExporter")]
	public class UltraGridExcelExporter : Infragistics.Win.UltraComponentBase,
		IUltraLicensedComponent
	{
		#region Private Members

		private UltraLicense license = null;

		// MD 1/14/09 - Groups in RowLayout
		// The twipsInPixelX member is never used and the other members have been moved to their own line.
		//private float twipsInPixelX, twipsInPixelY, columnWidthUnitsInPixelX;
		private float twipsInPixelY;
		private float columnWidthUnitsInPixelX;

		private FileLimitBehaviour fileLimitBehaviour = FileLimitBehaviour.ThrowException;
        
        // MRS v7.2 - Document Exporter
        // Moved to the GridExportCache.cs file so this code can be shared with
        // the document exporter
        #region Moved to GridExportCache.cs
        //// MRS 9/27/04 - UWG2786
        //private Hashtable dataSchema = new Hashtable();
        #endregion // Moved to GridExportCache.cs

        // MRS 6/28/05 - BR04756
		private WorkbookPaletteMode defaultWorkbookPaletteMode = WorkbookPaletteMode.CustomPalette;

        // MRS v7.2 - Document Exporter
        // Moved to the GridExportCache.cs file so this code can be shared with
        // the document exporter
        #region Moved to GridExportCache.cs
        //// MRS 5/31/06 - BR12912
        //// We need to cache the modified GridBagLayouts of each Band for efficiency. 
        //private Hashtable columnHeaderGridBagLayoutManagersTable = null;
        //private Hashtable valuesGridBagLayoutManagersTable = null;
        //private Hashtable summariesGridBagLayoutManagersTable = null;
        #endregion //Moved to GridExportCache.cs

        // MRS v7.2 - Document Exporter
        private GridExportCache gridExportCache;

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        private MappingManager mappingManager;
        private Workbook currentWorkbook;
        private bool exportFormulas = true;
        private bool currentExportFormulas = true;

        // MRS 6/3/2009 - TFS17908
        private Dictionary<WorksheetImage, Rectangle> unpositionedImages;

        // MBS 6/11/09 - TFS18312/FR04462/FR08690
        private const BandSpacing DefaultBandSpacing = BandSpacing.SingleRow;
        private BandSpacing bandSpacing = DefaultBandSpacing;

        #endregion Private Members

        #region Constructors

        /// <summary>
		/// UltraGridExcelExporter constructor.
		/// </summary>
		public UltraGridExcelExporter()
		{
			try
			{
				this.license = LicenseManager.Validate( typeof(UltraGridExcelExporter), this ) as UltraLicense;
			}
			catch (System.IO.FileNotFoundException)	{}

			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			//Graphics tmpGraphics = DrawUtility.CreateReferenceGraphics(null);
			Graphics tmpGraphics = DrawUtility.GetCachedGraphics(null);

			try
			{
				// MD 1/14/09 - Groups in RowLayout
				// The twipsInPixelX member is never used.
				//this.twipsInPixelX = 1440 / tmpGraphics.DpiX;

				this.twipsInPixelY = 1440 / tmpGraphics.DpiY;

				TEXTMETRIC textMetric;

				IntPtr hdc = tmpGraphics.GetHdc();

                // MRS 12/22/06 - fxCop
                //SecurityPermission perm = new SecurityPermission( SecurityPermissionFlag.UnmanagedCode );

				try
				{
                    // MRS 12/22/06 - fxCop
                    //perm.Assert();

					UnsafeGetTextMetrics(hdc, out textMetric);
				}
				catch
				{
					textMetric = new TEXTMETRIC();
					textMetric.tmAveCharWidth = 7;
				}

				tmpGraphics.ReleaseHdc(hdc);

				if(textMetric.tmAveCharWidth>0)
					this.columnWidthUnitsInPixelX = 256.0f / textMetric.tmAveCharWidth;
				else
					this.columnWidthUnitsInPixelX = 256.0f / 7;
			}
			finally
			{
				// AS 8/12/03 optimization - Use the new caching mechanism.
				//
				//tmpGraphics.Dispose();
				DrawUtility.ReleaseCachedGraphics(tmpGraphics);
			}
		}

		// MRS 11/15/05 - Added a constructor that takes an IContainer
		/// <summary>
		/// UltraGridExcelExporter constructor.
		/// </summary>
		public UltraGridExcelExporter(IContainer container) : this()
		{
			if (container != null)
				container.Add(this);
		}

		#endregion Constructors

		#region Public Methods

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        // Overload for backward compatibility
        /// <summary>
        /// Exports the passed in grid to specified file. 
        /// </summary>
        /// <param name="grid">Grid to export.</param>
        /// <param name="fileName">Name and path of resulting excel file.</param>
        /// <returns>Created <see cref="Infragistics.Excel.Workbook"/>.</returns>
        public Workbook Export(UltraGrid grid, string fileName)
        {
            WorkbookFormat? workbookFormat = Workbook.GetWorkbookFormat(fileName);
            if (workbookFormat == null)
                workbookFormat = WorkbookFormat.Excel97To2003;

            return this.Export(grid, fileName, (WorkbookFormat)workbookFormat);
        }

		/// <summary>
		/// Exports the passed in grid to specified file. 
		/// </summary>
		/// <param name="grid">Grid to export.</param>
		/// <param name="fileName">Name and path of resulting excel file.</param>
        /// <param name="workbookFormat">Specifies the format for the new workbook (Excel2007 or Excel97To2003) .</param>
		/// <returns>Created <see cref="Infragistics.Excel.Workbook"/>.</returns>
        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
		//public Workbook Export(UltraGrid grid, string fileName)
        public Workbook Export(UltraGrid grid, string fileName, WorkbookFormat workbookFormat)
		{
            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
			//Workbook wb = this.Export(grid);
            Workbook wb = this.Export(grid, workbookFormat);

			// MD 8/10/07
			// The BIFF8Writer class is now obsolete
			//BIFF8Writer.WriteWorkbookToFile(wb, fileName);
			wb.Save( fileName );

			// ZS 2/17/04 - UWG2935
			//return null;
			return wb;
		}

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        // Overload for backward compatibility
        /// <summary>
        /// Creates a new workbook with empty <see cref="Infragistics.Excel.Worksheet"/> 
        /// and exports the passed in grid to it. 
        /// </summary>
        /// <param name="grid">Grid to export.</param>
        /// <returns>Created <see cref="Infragistics.Excel.Workbook"/>.</returns>
        public Workbook Export(UltraGrid grid)    
        {
            return this.Export(grid, WorkbookFormat.Excel97To2003);
        }

		/// <summary>
		/// Creates a new workbook with empty <see cref="Infragistics.Excel.Worksheet"/> 
		/// and exports the passed in grid to it. 
		/// </summary>
		/// <param name="grid">Grid to export.</param>
        /// <param name="workbookFormat">Specifies the format for the new workbook (Excel2007 or Excel97To2003) .</param>
		/// <returns>Created <see cref="Infragistics.Excel.Workbook"/>.</returns>
        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
		//public Workbook Export(UltraGrid grid)
        public Workbook Export(UltraGrid grid, WorkbookFormat workbookFormat)
		{
			// MRS 6/28/05 - BR04756
			//Workbook wb = new Workbook();
            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
			//Workbook wb = new Workbook(this.DefaultWorkbookPaletteMode);
            Workbook wb = new Workbook(workbookFormat, this.DefaultWorkbookPaletteMode);

			wb.Worksheets.Add("Sheet1");

			// MD 8/10/07
			// The ActiveWorksheet property is now obsolete
			//return Export(grid, wb.ActiveWorksheet, 0, 0);
			return Export( grid, wb.WindowOptions.SelectedWorksheet, 0, 0 );
		}

		/// <summary>
		/// Exports the passed in ultragrid to the specified workbook in the active 
		/// worksheet. If there are no worksheets in workbook, new worksheet is created. 
		/// </summary>
		/// <param name="grid">Grid to export.</param>
		/// <param name="workbook">Destination <see cref="Infragistics.Excel.Workbook"/>.</param>
		/// <returns>Destination <see cref="Infragistics.Excel.Workbook"/>.</returns>
		public Workbook Export(UltraGrid grid, Workbook workbook)
		{
			// MD 8/10/07
			// The ActiveWorksheet property is now obsolete
			//if(workbook.ActiveWorksheet==null)
			//    workbook.Worksheets.Add("Sheet1");
			//
			//return Export(grid, workbook.ActiveWorksheet, 0, 0);
			if ( workbook.Worksheets.Count == 0 )
				workbook.Worksheets.Add( "Sheet1" );

			return Export( grid, workbook.WindowOptions.SelectedWorksheet, 0, 0 );
		}
		
		/// <summary>
		/// Exports the passed in ultragrid to the specified worksheet.
		/// </summary>
		/// <param name="grid">Grid to export.</param>
		/// <param name="worksheet">Destination <see cref="Infragistics.Excel.Worksheet"/>.</param>
		/// <returns>Parent workbook of passed in worksheet.</returns>
		public Workbook Export(UltraGrid grid, Worksheet worksheet)
		{
			return Export(grid, worksheet, 0, 0);
		}

		/// <summary>
		/// Exports the passed in ultragrid to the specified workbook in the active 
		/// worksheet starting at the specified row and column. If there are no 
		/// worksheets in workbook, new worksheet is created. It returns the passed 
		/// in workbook.
		/// </summary>
		/// <param name="grid">Grid to export.</param>
		/// <param name="workbook">Destination <see cref="Infragistics.Excel.Workbook"/>.</param>
		/// <param name="startRow">Start row for exported data (zero based).</param>
		/// <param name="startColumn">Start column for exported data (zero based).</param>
		/// <returns>Destination <see cref="Infragistics.Excel.Workbook"/>.</returns>
		public Workbook Export(UltraGrid grid, Workbook workbook, int startRow, 
			int startColumn)
		{
			// MD 8/10/07
			// The ActiveWorksheet property is now obsolete
			//if(workbook.ActiveWorksheet==null)
			//    workbook.Worksheets.Add("Sheet1");
			//
			//return Export(grid, workbook.ActiveWorksheet, startRow, startColumn);
			if ( workbook.Worksheets.Count == 0 )
				workbook.Worksheets.Add( "Sheet1" );

			return Export( grid, workbook.WindowOptions.SelectedWorksheet, startRow, startColumn );
		}

		/// <summary>
		/// Exports the passed in ultragrid to the specified worksheet starting at 
		/// the specified row and column.
		/// </summary>
		/// <param name="grid">Grid to export.</param>
		/// <param name="worksheet">Destination <see cref="Infragistics.Excel.Worksheet"/>.</param>
		/// <param name="startRow">Start row for exported data (zero based).</param>
		/// <param name="startColumn">Start column for exported data (zero based).</param>
		/// <returns>Parent workbook of passed in worksheet.</returns>
		public Workbook Export(UltraGrid grid, Worksheet worksheet, 
			int startRow, int startColumn)
		{
			if(grid==null)
				throw new ArgumentNullException("grid");

			if(worksheet==null)
				throw new ArgumentNullException("worksheet");

            // MBS 4/29/09 - TFS17050
            // We shouldn't be looking at the hard-coded value for the number of supported rows, since we
            // support different workbook formats now
            //
			//if(startRow<0 || startRow>=Workbook.MaxExcelRowCount)
            if (startRow < 0 || startRow >= Workbook.GetMaxRowCount(worksheet.Workbook.CurrentFormat))
				throw new ArgumentOutOfRangeException("row", startRow, 
					Shared.SR.GetString("LER_ArgumentOutOfRangeException_1") );

            // MBS 4/29/09 - TFS17050
            // We shouldn't be looking at the hard-coded value for the number of supported columns, since we
            // support different workbook formats now
            //
			//if(startColumn<0 || startColumn>=Workbook.MaxExcelColumnCount)
            if (startColumn < 0 || startColumn >= Workbook.GetMaxColumnCount(worksheet.Workbook.CurrentFormat))
				throw new ArgumentOutOfRangeException("column", startColumn, 
					Shared.SR.GetString("LER_ArgumentOutOfRangeException_2") );

			UltraGridExporterHelper exporterHelper = new UltraGridExporterHelper(
				this, worksheet, startRow, startColumn);

            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            this.currentWorkbook = worksheet.Workbook;

			grid.Export(exporterHelper);

            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            this.currentWorkbook = null;

			return worksheet.Workbook;
		}

		/// <summary>
		/// Relevant appearance properties used in excel exporting. All other 
		/// properties are ignored in exporting process.
		/// </summary>
		public const AppearancePropFlags ExcelRelevantPropFlags = 
			AppearancePropFlags.BackColor |
			AppearancePropFlags.BackColorDisabled |
			AppearancePropFlags.BorderColor |
			AppearancePropFlags.FontData |
			AppearancePropFlags.ForeColor |
			AppearancePropFlags.ForeColorDisabled |
			AppearancePropFlags.TextHAlign |
			AppearancePropFlags.TextVAlign;

		#endregion Public Methods

		#region Public Properties

		#region FileLimitBehaviour
		/// <summary>
		/// Specifies behavior during the exporting process. Use this if you want exporter to behave 
		/// differently when it reaches one of excel file limits.
		/// </summary>
		[DefaultValue(FileLimitBehaviour.ThrowException), 
		LocalizedDescription("LD_UltraGridExcelExporter_P_FileLimitBehaviour"),
		LocalizedCategory("LC_Behaviour")]
		public FileLimitBehaviour FileLimitBehaviour
		{
			get
			{
				return this.fileLimitBehaviour;
			}
			set
			{
				this.fileLimitBehaviour = value;
			}
		}
		#endregion FileLimitBehaviour

		// MRS 6/28/05 - BR04756
		#region DefaultWorkbookPaletteMode

		/// <summary>
		/// Determines the default <see cref="Infragistics.Excel.WorkbookPaletteMode"/> to use when exporting.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property determines whether a custom palette will be created or whether to use the standard Excel palette of colors when exporting.</p>
		/// <p class="body">A custom palette will make the exported Excel Workbook appear exactly like the exported grid. However, using a custom palette can cause problems when copying and pasting from one Excel Workbook to another. Since the two workbooks are unlikely to share exactly the same palette, colors may change.</p>
		/// <p class="body">Using the standard Excel palette means that the colors in the Excel Workbook will not exactly match those in the grid. For each color used by the grid, the closest matching color in the standard palette will be used in Excel. Copy and Paste using a Workbook creating using the Standard palette will not result in any color changes - unless the pasting is done onto a Workbook with a custom palette.</p>
		/// <p class="note">Note that the palette mode is determined by the WorkBook. So this property will only be used when the ExcelExport creates a new workbook. If a workbook is passed in to the Export method, then the palette mode of the Workbook is used. To change the palette mode of a Workbook created in code, pass a <see cref="Infragistics.Excel.WorkbookPaletteMode"/> into the Workbook constructor.</p>
		/// </remarks>
		[DefaultValue(WorkbookPaletteMode.CustomPalette), 
		LocalizedDescription("LD_UltraGridExcelExporter_P_DefaultWorkbookPaletteMode"),
		LocalizedCategory("LC_Behaviour")]
		public WorkbookPaletteMode DefaultWorkbookPaletteMode
		{
			get
			{
				return this.defaultWorkbookPaletteMode;
			}
			set
			{
				if (this.defaultWorkbookPaletteMode == value)
					return;
                
				this.defaultWorkbookPaletteMode = value;				
			}
		}

		#endregion DefaultWorkbookPaletteMode

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        #region ExportFormulas

        /// <summary>
        /// Determines whether formulas in the grid will be exported to Excel.
        /// </summary>
        /// <remarks>
        /// <p class="body">When true (the default), the UltraGridExcelExport will attempt to convert grid formulas into equivalent formulas in the exported worksheet.</p>
        /// <p class="body">When false, the value of the cell in the grid at the time of the export will be exported directly to the value of the cells in the worksheet.</p>
        /// <p class="body">The exporting of formulas is done after all of the data in the grid has been exported. So when this property is set to true, the normal export process of exporting the values of the grid cells to the worksheet cells will still occur as normal, and the exporting of formulas will occur after all of the raw data exporting has completed.</p>
        /// <p class="body">Due to limitations of Excel and differences between Excel and UltraCalcManager behavior, some formulas may require the creation of a hidden worksheet which will point back to the main worksheet for it's data.</p>
        /// <p class="body">If the exporter fails to convert a formula for any reason, the <see cref="FormulaExportError"/> event will fire. By default, if the event is not handled, the error message will be written to the worksheet cell.</p>
        /// </remarks>
        /// <seealso cref="FormulaExporting"/>
        /// <seealso cref="FormulaExported"/>
        /// <seealso cref="FormulaExportError"/>
        [DefaultValue(true),
        LocalizedDescription("LD_UltraGridExcelExporter_P_ExportFormulas"),
        LocalizedCategory("LC_Behaviour")]
        public bool ExportFormulas
        {
            get
            {
                return this.exportFormulas;
            }
            set
            {
                if (this.exportFormulas == value)
                    return;

                this.exportFormulas = value;
            }
        }

        #endregion ExportFormulas

        // MBS 6/11/09 - TFS18312/FR04462/FR08690
        #region BandSpacing

        /// <summary>
        /// Gets or sets the amount of empty rows used as padding around a set of child rows.
        /// </summary>
        [DefaultValue(DefaultBandSpacing)]
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridExcelExporter_P_BandSpacing")]
        public BandSpacing BandSpacing
        {
            get { return this.bandSpacing; }
            set { this.bandSpacing = value; }
        }
        #endregion //BandSpacing

        #endregion

        #region Internal Properties

        // MRS v7.2 - Document Exporter
        // Moved to the GridExportCache.cs file so this code can be shared with
        // the document exporter
        #region Moved to GridExportCache.cs
        //// MRS 5/31/06 - BR12912
        //// We need to cache the modified GridBagLayouts of each Band for efficiency. 
        //internal Hashtable ColumnHeaderGridBagLayoutManagersTable
        //{
        //    get
        //    {
        //        if (this.columnHeaderGridBagLayoutManagersTable == null)
        //            this.columnHeaderGridBagLayoutManagersTable = new Hashtable();

        //        return this.columnHeaderGridBagLayoutManagersTable;
        //    }
        //}

        //// MRS 5/31/06 - BR12912
        //// We need to cache the modified GridBagLayouts of each Band for efficiency. 
        //internal Hashtable ValuesGridBagLayoutManagersTable
        //{
        //    get
        //    {
        //        if (this.valuesGridBagLayoutManagersTable == null)
        //            this.valuesGridBagLayoutManagersTable = new Hashtable();

        //        return this.valuesGridBagLayoutManagersTable;
        //    }
        //}

        //// MRS 5/31/06 - BR12912
        //// We need to cache the modified GridBagLayouts of each Band for efficiency. 
        //internal Hashtable SummariesGridBagLayoutManagersTable
        //{
        //    get
        //    {
        //        if (this.summariesGridBagLayoutManagersTable == null)
        //            this.summariesGridBagLayoutManagersTable = new Hashtable();

        //        return this.summariesGridBagLayoutManagersTable;
        //    }
        //}
        #endregion // Moved to GridExportCache.cs

        // MRS v7.2 - Document Exporter
        #region GridExportCache
        private GridExportCache GridExportCache
        {
            get
            {
                if (this.gridExportCache == null)
                    this.gridExportCache = new GridExportCache( GridExportCache.CacheType.Excel );

                return this.gridExportCache;
            }
        }
        #endregion GridExportCache

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        #region MappingManager
        private MappingManager MappingManager
        {
            get
            {
                if (this.mappingManager == null)
                    this.mappingManager = new MappingManager(this.GridExportCache.Layout, this.currentWorkbook);                

                return this.mappingManager;
            }
        }
        #endregion //MappingManager

        // MRS 6/3/2009 - TFS17908
        #region UnpositionedImages
        private Dictionary<WorksheetImage, Rectangle> UnpositionedImages
        {
            get
            {
                if (this.unpositionedImages == null)
                    this.unpositionedImages = new Dictionary<WorksheetImage, Rectangle>();

                return this.unpositionedImages;
            }
        }
        #endregion // UnpositionedImages

        #endregion Internal Properties

        #region Internal Methods Called By Interface

        /// <summary>
		/// BeginExportInternal is indirectly called by grid (via UltraGridExporterHelper).
		/// </summary>
		/// <param name="exportHelper"></param>
		/// <param name="exportLayout"></param>
		/// <param name="rows"></param>
		internal void BeginExportInternal(UltraGridExporterHelper exportHelper, 
			UltraGridLayout exportLayout, RowsCollection rows)
		{
            // MRS 5/31/06 - BR12912
            // Clear the cache in case there is anything in there from the last export. 
            // MRS v7.2 - Document Exporter
            //
            //this.ClearGridBagLayoutManagerCache();
            this.GridExportCache.ClearGridBagLayoutManagerCache();

            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            this.currentExportFormulas = this.ExportFormulas;

			// SSP 11/18/04 BR00530
			// Max hierarchy level is not necessarily the max band depth because that doesn't take into
			// account the group-by levels. Use the new GetMaxHierarchyLevel method to figure out the
			// max possible hierarchy level.
			//
			//exportHelper.skipSiblings = new BitArray(exportLayout.MaxBandDepth, false);
            // MRS 4/18/07
            // I discovered while implementing the DocumentExporter that this variable is never actually used.         
            //exportHelper.skipSiblings = new BitArray(this.GetMaxHierarchyLevel(exportLayout), false);

			exportHelper.columnExcelFormats = new Hashtable(64);

            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            exportHelper.summaryExcelFormats = new Dictionary<SummarySettings, string>();

			exportHelper.lastBand = exportLayout.Bands[0];

			this.OnBeginExport(	new BeginExportEventArgs(exportHelper, exportLayout, rows) );

			// MRS 1/6/05 - BR00256
			// make sure the columns don't have exclusive regions set since
			// this doesn't make sense for an export layout
			//
			foreach ( UltraGridBand band in exportLayout.Bands )
			{
				foreach ( UltraGridColumn column in band.Columns )
				{
					column.Header.ExclusiveColScrollRegion = null;
				}
			}

            // MRS 7/17/2008 - BR34498
            this.GridExportCache.Layout = exportLayout;

			// MRS 9/27/04 - UWG2786
            // MRS v7.2 - Document Exporter
            //
			//this.BuildDataSchema(exportLayout);
			// MD 1/14/09 - Groups in RowLayout
			// We are now going to use the new caching mechanism.
            //this.GridExportCache.BuildDataSchema(exportLayout);
			this.GridExportCache.BuildReportInfoCache( exportLayout );

			// MRS 8/1/05 - BR05002
			//if(rows.Count==0 
			if(rows.VisibleRowCount==0 
				&& exportLayout.Bands[0].GetFirstVisibleCol(null,true)!=null)
			{
                // MRS 5/31/06 - BR12912
				//this.SetCurrentGridObjectSpan(exportHelper, exportLayout.Bands[0], null);
                this.SetCurrentGridObjectSpan(exportHelper, exportLayout.Bands[0], null,GridExportCache.PlaceholderType.ColumnHeaders);

				this.ProcessHeaderRows(exportHelper, null, exportLayout.Bands[0]);
                
                // MRS 5/31/06 - BR12912
                this.SetCurrentGridObjectSpan(exportHelper, exportLayout.Bands[0], null, GridExportCache.PlaceholderType.Summaries);

				this.ProcessSummaryRows(exportHelper, null, exportLayout.Bands[0], rows);
			}
		}

		/// <summary>
		/// ProcessGridRowInternal is indirectly called by grid (via UltraGridExporterHelper).
		/// </summary>
		/// <param name="exportHelper"></param>
		/// <param name="row"></param>
		/// <param name="processRowParams"></param>
		internal void ProcessGridRowInternal(UltraGridExporterHelper exportHelper, 
			UltraGridRow row, ProcessRowParams processRowParams)
		{
			this.AdjustPositionForRowsCollection(exportHelper, row);

			exportHelper.lastBand = row.Band;

			// if this is the first row in rows collection AND is NOT UltraGridGroupByRow
			// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
			// Export summaries displayed on top. Summary rows have index of -1 so the check
			// for Index == 0 won't work for summary row on top. Instead check if there is
			// no previous row using the GetSibling method.
			// 
			// ------------------------------------------------------------------------------
			//if(row.Index==0 && !(row is UltraGridGroupByRow))						
			// MRS 8/10/06 - BR14855
			// Don't process the header again if the VisibleRowCount is 0. The headers will already have been processed
			// inside the Export method of the grid. 
			//if ( ! row.IsGroupByRow && null == row.GetSibling( SiblingRow.Previous, false, false, true ) )
			if ( ! row.IsGroupByRow && null == row.GetSibling( SiblingRow.Previous, false, false, true ) 

				// MD 1/14/09 - Groups in RowLayout
				// Don't write out headers when we are in row layout mode and the labels are with the cells. They will then be written out with each row.
#pragma warning disable 0618
				&& ( row.Band.UseRowLayout == false || row.Band.RowLayoutLabelStyle == RowLayoutLabelStyle.Separate )
#pragma warning restore 0618

				&& row.ParentCollection.VisibleRowCount > 0 )
			{
                // MRS 5/1/2008 - BR32400
                // Copied this fix from BR04874 and applied it to fixed headers.
                if (row != null &&
                    row.ParentCollection.Count > 0 &&
                    row.IsSummaryRow)
                {
                    UltraGridGroupByRow firstRow = row.ParentCollection[0] as UltraGridGroupByRow;
                    int groupByDescendantLevelCount = this.GetGroupByDescendantLevelCount(firstRow);
                    exportHelper.currentPos.X += groupByDescendantLevelCount;
                }

                // MRS 5/31/06 - BR12912
				//this.SetCurrentGridObjectSpan(exportHelper, row.Band, null);
                this.SetCurrentGridObjectSpan(exportHelper, row.Band, null, GridExportCache.PlaceholderType.ColumnHeaders);
				this.ProcessHeaderRows(exportHelper, row, row.Band);
			}
			// ------------------------------------------------------------------------------

            if (row is UltraGridGroupByRow)
            {
                exportHelper.extentOfCurrentGridObject.Height = 1;
                exportHelper.extentOfCurrentGridObject.Width = 1;
            }
            else
            {
                // MRS 5/31/06 - BR12912
                //this.SetCurrentGridObjectSpan(exportHelper, row.Band, row);
                this.SetCurrentGridObjectSpan(exportHelper, row.Band, row, 
                    row.IsSummaryRow ? GridExportCache.PlaceholderType.Summaries : GridExportCache.PlaceholderType.Values);                

            }

			// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
			// Added the following if block.
			//
			if ( row.IsSummaryRow )
			{
				this.ProcessSummaryRows( exportHelper, row, row.Band, row.ParentCollection );
				return;
			}

			// fire InitializeRow
			ExcelExportInitializeRowEventArgs initRowEA = new 
				ExcelExportInitializeRowEventArgs(exportHelper, processRowParams, row);

			if(row.HiddenResolved)
			{
				initRowEA.SkipRow = true;
				initRowEA.SkipDescendants = true;
			}

			this.OnInitializeRow(initRowEA);

			if(initRowEA.TerminateExport==true)
				return;

			// if NOT skipRow
			if(!initRowEA.SkipRow)			
			{
				this.ProcessSingleRow(exportHelper, row);
			}

            // MRS 4/18/07
            // I discovered while implementing the DocumentExporter that this variable is never actually used.         
            //exportHelper.skipSiblings[exportHelper.HierarchyLevel] = initRowEA.SkipSiblings;

			// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
			// Commented out the following code. Instead added block above. Now the 
			// IUltraGridExporter will get called with UltraGridSummaryRow instances.
			//
			
		}

		/// <summary>
		/// EndExportInternal is indirectly called by grid (via UltraGridExporterHelper).
		/// </summary>
		/// <param name="exportHelper"></param>
		/// <param name="canceled"></param>
		internal void EndExportInternal(UltraGridExporterHelper exportHelper, 
			bool canceled)
		{
            if (!canceled)
                this.ProcessFormulas();

			this.OnEndExport( new EndExportEventArgs(exportHelper, canceled) );

            // MRS 5/31/06 - BR12912
            // Clear the cache - no reason to hold on to this stuff after the export is complete
            // and we need a clean slate for the next export. 
            // MRS v7.2 - Document Exporter
            //
            //this.ClearGridBagLayoutManagerCache();
            this.GridExportCache.ClearGridBagLayoutManagerCache();
		}        

		#endregion Internal Methods Called By Interface

		#region Private Helper Methods

		internal bool HasChildHelper(UltraGridRow row)
		{
			if(row.ChildBands!=null)
				foreach(UltraGridChildBand childBand in row.ChildBands)
					if(!childBand.Band.HiddenResolved)
					{
						// MRS 3/24/05 - BR02960 
						//return true;
						// The band is not hidden, but it still might have no rows or no visible rows. 
						if (childBand.Rows.VisibleRowCount > 0)
							return true;
					}

			return false;
		}

		internal void AdjustPositionForRowsCollection(UltraGridExporterHelper exportHelper, 
			UltraGridRow row)
		{
			int newHierarchyLevel = GetHierarchyLevel(row);
			
			// MRS 2/22/05 - BR00432
			exportHelper.CurrentIndentationLevel = this.GetIndentationLevel(row);

			if(newHierarchyLevel != exportHelper.HierarchyLevel)
			{
				// MRS 2/22/05 - BR00432
				// Indentation is not neccessarily the same as hierarachy level. 
				//exportHelper.currentPos.X = exportHelper.gridStartPos.X + newHierarchyLevel;
				exportHelper.currentPos.X = exportHelper.gridStartPos.X + exportHelper.CurrentIndentationLevel;

				while(exportHelper.HierarchyLevel != newHierarchyLevel)
				{
					if(exportHelper.HierarchyLevel < newHierarchyLevel)
						exportHelper.HierarchyLevel++;
					else
						exportHelper.HierarchyLevel--;					
					
					this.SetCollapsedRow( exportHelper.CurrentWorksheet,
						exportHelper.currentPos.Y, exportHelper.HierarchyLevel );

                    // MBS 6/11/09 - TFS18312/FR04462/FR08690
                    // Only do this if the user wants the spacing
                    if(this.BandSpacing == BandSpacing.SingleRow)
					    this.IncreaseCurrentRow(exportHelper, 1);
				}
			}
			else
			{
				if(row.Band != exportHelper.lastBand)
				{
					this.SetCollapsedRow( exportHelper.CurrentWorksheet,
						exportHelper.currentPos.Y, exportHelper.HierarchyLevel );

                    // MBS 6/11/09 - TFS18312/FR04462/FR08690
                    // Only do this if the user wants the spacing
                    if (this.BandSpacing == BandSpacing.SingleRow)
					    this.IncreaseCurrentRow(exportHelper, 1);
				}
			}
		}

		// SSP 11/18/04 BR00530
		// Max hierarchy level is not necessarily the max band depth because that doesn't take into
		// account the group-by levels. Added GetMaxHierarchyLevel method.
		//
		private int GetMaxHierarchyLevel( UltraGridLayout layout )
		{
			int maxHierarchyLevel = 0;
			foreach ( UltraGridBand band in layout.Bands )
				// Add 1 for the band level and the count of columns for the number of group-by
				// levels possible. We don't have to return an exact number here.
				//
				maxHierarchyLevel += 1 + band.Columns.Count;

			return maxHierarchyLevel;
		}

		private int GetHierarchyLevel(UltraGridRow row)
		{
			if(row.ParentRow==null) 
				return 0;
			else
				return GetHierarchyLevel(row.ParentRow) + 1;
		}

		// MRS 2/22/05 - BR00432
		// If the Band indentation is 0, then don't indent for that band
        private int GetIndentationLevel(UltraGridRow row)
        {
            if (row.ParentRow == null)
                return 0;
            else
            {
                int bandIndent = (row.Band.Indentation == 0) ? 0 : 1;
                // MRS 10/18/05 - BR06862
                //return GetHierarchyLevel(row.ParentRow) + bandIndent;
                return GetIndentationLevel(row.ParentRow) + bandIndent;
            }
        }

        // MRS v7.2 - Document Exporter
        // Moved to the GridExportCache.cs file so this code can be shared with
        // the document exporter
        #region Moved to GridExportCache.cs
        
        //#region MRS 5/31/06 - BR12912

        ////MRS 5/31/06 - BR12912
        
        //#region ClearGridBagLayoutManagerCache
        //private void ClearGridBagLayoutManagerCache()
        //{
        //    this.columnHeaderGridBagLayoutManagersTable = null;
        //    this.valuesGridBagLayoutManagersTable = null;
        //    this.summariesGridBagLayoutManagersTable = null;
        //}
        //#endregion ClearGridBagLayoutManagerCache

        //#region GetCachedGridBagLayoutManager
        //    internal GridBagLayoutManager GetCachedGridBagLayoutManager(UltraGridBand band, GridExportCache.PlaceholderType placeholderType)
        //{
        //    Hashtable table = null;
        //    switch (placeholderType)
        //    {
        //        case GridExportCache.PlaceholderType.ColumnHeaders:
        //            table = this.ColumnHeaderGridBagLayoutManagersTable;
        //            break;
        //        case GridExportCache.PlaceholderType.Values:
        //            table = this.ValuesGridBagLayoutManagersTable;
        //            break;
        //        case GridExportCache.PlaceholderType.Summaries:
        //            table = this.SummariesGridBagLayoutManagersTable;
        //            break;
        //        default:
        //            throw new Exception("Internal: Unknown GridExportCache.PlaceholderType");
        //    }

        //    GridBagLayoutManager gridBagLayoutManager = table[band] as GridBagLayoutManager;
        //    if (gridBagLayoutManager == null)
        //    {
        //        gridBagLayoutManager = UltraGridExcelExporter.GetGridBagLayoutManager(band, placeholderType);
        //        table[band] = gridBagLayoutManager;
        //    }

        //    return gridBagLayoutManager;
        //}
        //#endregion GetCachedGridBagLayoutManager
                
        //#region GetGridBagLayoutManager
        //internal static GridBagLayoutManager GetGridBagLayoutManager(UltraGridBand band, GridExportCache.PlaceholderType placeholderType)
        //{
        //    Design.IGridDesignInfo designInfo = band.Layout.Grid;

        //    // Store the LayoutManager we get from the grid in a variable.
        //    // We will take this layout manager and build a new one without any 0-size rows
        //    // and columns. 
        //    Layout.GridBagLayoutManager gridBagLayoutManager = null;

        //    switch (placeholderType)
        //    {
        //        case GridExportCache.PlaceholderType.ColumnHeaders:
        //            switch (band.RowLayoutLabelStyle)
        //            {
        //                case RowLayoutLabelStyle.Separate:
        //                    gridBagLayoutManager = designInfo.GetHeaderAreaGridBagLayoutManager(band);
        //                    break;
        //                case RowLayoutLabelStyle.WithCellData:
        //                    gridBagLayoutManager = designInfo.GetCellAreaGridBagLayoutManager(band);
        //                    break;
        //                default:
        //                    throw new Exception("Internal: RowLayoutLabelStyle");
        //            }
        //            break;

        //        case GridExportCache.PlaceholderType.Values:
        //            gridBagLayoutManager = designInfo.GetCellAreaGridBagLayoutManager(band);
        //            break;

        //        case GridExportCache.PlaceholderType.Summaries:
        //            gridBagLayoutManager = designInfo.GetSummaryAreaGridBagLayoutManager(band);
        //            break;

        //        default:
        //            throw new Exception("Internal: unknown GridExportCache.PlaceholderType");
        //    }

        //    gridBagLayoutManager = UltraGridExcelExporter.GetCompressedGridBagLayoutManager(gridBagLayoutManager, placeholderType);

        //    return gridBagLayoutManager;
        //}
        //#endregion GetGridBagLayoutManager
                
        //#region GetCompressedGridBagLayoutManager
        //// This method takes a GridBagLayoutManager and creates a new one without
        //// any 0-sized rows or columns. 
        //private static GridBagLayoutManager GetCompressedGridBagLayoutManager(GridBagLayoutManager gridBagLayoutManager, GridExportCache.PlaceholderType placeholderType)
        //{
        //    Hashtable originalContraints = new Hashtable(gridBagLayoutManager.LayoutItems.Count);
        //    foreach (ILayoutItem layoutItem in gridBagLayoutManager.LayoutItems)
        //    {
        //        IGridBagConstraint iGridBagConstraint = gridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as IGridBagConstraint;
        //        GridBagConstraint gridBagConstraint = new Layout.GridBagConstraint(iGridBagConstraint.OriginX, iGridBagConstraint.OriginY, iGridBagConstraint.SpanX, iGridBagConstraint.SpanY, iGridBagConstraint.WeightX, iGridBagConstraint.WeightY, iGridBagConstraint.Anchor, iGridBagConstraint.Fill, iGridBagConstraint.Insets);
        //        originalContraints[layoutItem] = gridBagConstraint;
        //    }

        //    GridBagLayoutManager exportGridBagLayoutManager = new Layout.GridBagLayoutManager();
        //    foreach (ILayoutItem layoutItem in originalContraints.Keys)
        //    {
        //        ILayoutItem key = layoutItem;
        //        object constraint = originalContraints[layoutItem];

        //        // If the PlaceHolderType is a a summary, the grid will use a SummaryLayoutItem
        //        // as the LayoutItem. This is an internal class, so we can't use it here. So we 
        //        // will just use the grid column, instead. 
        //        if (placeholderType == GridExportCache.PlaceholderType.Summaries)
        //            key = RowLayoutColumnInfo.GetColumnFromLayoutItem(key);

        //        exportGridBagLayoutManager.LayoutItems.Add(key, constraint);
        //    }

        //    int currentRow = 0;

        //    exportGridBagLayoutManager.InvalidateLayout();

        //    while (currentRow < exportGridBagLayoutManager.RowHeights.Length
        //        // MRS 2/23/07 - BR20309
        //        // This was getting into an infinite loop if there is only one row in the layout
        //        // and it's height is already 0 (like when ColHeadersVisible is false). 
        //        // If there's only one row, there's nothing to collapse, so bail out. 
        //        //
        //        && exportGridBagLayoutManager.RowHeights.Length > 1) 
        //    {
        //        if (exportGridBagLayoutManager.RowHeights[currentRow] == 0)
        //        {
        //            foreach (ILayoutItem layoutItem in exportGridBagLayoutManager.LayoutItems)
        //            {
        //                GridBagConstraint gridBagConstraint = exportGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;

        //                // See if this contraint intersect with the current (0-height) row.                             
        //                if (gridBagConstraint.OriginY <= currentRow &&
        //                    currentRow < (gridBagConstraint.OriginY + gridBagConstraint.SpanY))
        //                    gridBagConstraint.SpanY--;
        //            }

        //            foreach (ILayoutItem layoutItem in exportGridBagLayoutManager.LayoutItems)
        //            {
        //                GridBagConstraint gridBagConstraint = exportGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;
        //                if (gridBagConstraint.OriginY > currentRow)
        //                    gridBagConstraint.OriginY--;
        //            }

        //            exportGridBagLayoutManager.InvalidateLayout();
        //        }
        //        else
        //            currentRow++;
        //    }

        //    int currentColumn = 0;
        //    exportGridBagLayoutManager.InvalidateLayout();
        //    while (currentColumn < exportGridBagLayoutManager.ColumnWidths.Length)
        //    {
        //        if (exportGridBagLayoutManager.ColumnWidths[currentColumn] == 0)
        //        {
        //            foreach (ILayoutItem layoutItem in exportGridBagLayoutManager.LayoutItems)
        //            {
        //                GridBagConstraint gridBagConstraint = exportGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;

        //                // See if this contraint intersect with the current (0-height) row.                             
        //                if (gridBagConstraint.OriginX <= currentColumn &&
        //                    currentColumn < (gridBagConstraint.OriginX + gridBagConstraint.SpanX))
        //                    gridBagConstraint.SpanX--;
        //            }

        //            foreach (ILayoutItem layoutItem in exportGridBagLayoutManager.LayoutItems)
        //            {
        //                GridBagConstraint gridBagConstraint = exportGridBagLayoutManager.LayoutItems.GetConstraint(layoutItem) as GridBagConstraint;
        //                if (gridBagConstraint.OriginX > currentColumn)
        //                    gridBagConstraint.OriginX--;
        //            }

        //            exportGridBagLayoutManager.InvalidateLayout();
        //        }
        //        else
        //            currentColumn++;
        //    }

        //    return exportGridBagLayoutManager;
        //}
        //#endregion GetCompressedGridBagLayoutManager

        //#endregion // MRS 5/31/06 - BR12912

        #endregion // Moved to GridExportCache.cs

        internal void IncreaseCurrentRow(UltraGridExporterHelper exportHelper, int amount)
		{
			exportHelper.currentPos.Y += amount;

			// MRS 2/22/05 - BR00432			
			//exportHelper.currentPos.X = exportHelper.gridStartPos.X + exportHelper.HierarchyLevel;
			exportHelper.currentPos.X = exportHelper.gridStartPos.X + exportHelper.CurrentIndentationLevel;
		}

		internal void SetCollapsedRow(Worksheet worksheet, int row, int hierarchyLevel)
		{
			// ZS 1/27/04 - Updated UWG2752
			// ZS 1/23/04 - UWG2752
            //
            // MBS 4/29/09 - TFS17050
            // We shouldn't be looking at the hard-coded value for the number of supported rows, since we
            // support different workbook formats now
			//
            //if( row >= Workbook.MaxExcelRowCount )
            if (row >= Workbook.GetMaxRowCount(worksheet.Workbook.CurrentFormat))
			{
				if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
					return;
				else
					throw new Exception( Shared.SR.GetString("LER_MaxExcelRowCountLimit") );
			}
			
			WorksheetRow tmRow = worksheet.Rows[row];

			tmRow.OutlineLevel = hierarchyLevel;

			// MD 8/10/07
			// The Expanded property is now obsolete
			//if(hierarchyLevel!=0)
			//    tmRow.Expanded = false;
			//else
			//    tmRow.Expanded = true;
			if ( hierarchyLevel != 0 )
				tmRow.Hidden = true;
			else
				tmRow.Hidden = false;
		}

		internal void SetCollapsedRows(Worksheet worksheet, int firstRow, int lastRow, 
			int hierarchyLevel)
		{
			for(int i=firstRow ; i<=lastRow ; i++)
				this.SetCollapsedRow(worksheet, i, hierarchyLevel);
		}

        // MRS 5/31/06 - BR12912
        //private void SetCurrentGridObjectSpan(UltraGridExporterHelper exportHelper, 
        //    UltraGridBand band, UltraGridRow row)
        private void SetCurrentGridObjectSpan(UltraGridExporterHelper exportHelper,
            UltraGridBand band, UltraGridRow row, GridExportCache.PlaceholderType placeHolderType)
		{
			// MRS 9/27/04 - UWG2786
			//			WorksheetPlaceholderCollection places = new WorksheetPlaceholderCollection(
			//				band, GridExportCache.PlaceholderType.Values, row );
            // MRS 5/31/06 - BR12912
            //WorksheetPlaceholderCollection places = new WorksheetPlaceholderCollection(
            //    band, GridExportCache.PlaceholderType.Values, row, this );
			// MD 1/14/09 - Groups in RowLayout
			// We have to use the new caching mechanism.
			//WorksheetPlaceholderCollection places = new WorksheetPlaceholderCollection(
			//    band, placeHolderType, row, this);
			//
			//exportHelper.extentOfCurrentGridObject.Height = places.SpanY;
			//exportHelper.extentOfCurrentGridObject.Width = places.SpanX;

            // MRS 8/19/2009 - TFS20658
			//GridExportCache.DocumentBandInfo documentBandInfo = this.GridExportCache.GetDocumentBandInfo( band, placeHolderType );
            //
            UltraGridSummaryRow summaryRow = row as UltraGridSummaryRow;
            GridExportCache.DocumentBandInfo documentBandInfo = (summaryRow == null)
                ? this.GridExportCache.GetDocumentBandInfo(band, placeHolderType)
                : this.GridExportCache.GetDocumentBandInfo(summaryRow);

			int spanX = 0;
			int spanY = 0;

			foreach ( GridExportCache.DocumentHeaderInfo headerInfo in documentBandInfo )
			{
				switch ( headerInfo.DocumentHeaderInfoType )
				{
					case GridExportCache.DocumentHeaderInfoType.Cell:
					case GridExportCache.DocumentHeaderInfoType.Header:
					case GridExportCache.DocumentHeaderInfoType.Summary:
						spanX = Math.Max( spanX, headerInfo.OriginX + headerInfo.SpanX );
						spanY = Math.Max( spanY, headerInfo.OriginY + headerInfo.SpanY );
						break;

					case GridExportCache.DocumentHeaderInfoType.Blank:
						break;

					default:
						Debug.Fail( "Unknown DocumentHeaderInfoType" );
						break;
				}
			}

            // MRS 4/21/2009 - TFS16876
            // Add an extra spanY for the AutoPreview row if this is a row and the 
            // AutoPreviewHeight is > 0.
            //
            if (row != null && row.AutoPreviewHeight > 0)
                spanY++;

			exportHelper.extentOfCurrentGridObject.Height = spanY;
			exportHelper.extentOfCurrentGridObject.Width = spanX;
		}

		private void ProcessHeaderRows(UltraGridExporterHelper exportHelper,
			UltraGridRow row, UltraGridBand band)
		{
			// export band header
			if(band.HeaderVisible)
			{
				this.ExportBandHeaderRow(exportHelper, row, band);
			}

			// export column headers

			// don't export if headers are together with cells
			if(
#pragma warning disable 0618
				band.UseRowLayout
#pragma warning restore 0618
				&& band.RowLayoutLabelStyle == RowLayoutLabelStyle.WithCellData )
				return;

			// don't export if ColHeadersVisible is false
			if( !band.ColHeadersVisible )
				return;

			// fire HeaderRowExporting
			HeaderRowExportingEventArgs headerExportingArgs =
				new HeaderRowExportingEventArgs(exportHelper, band, row,
				HeaderTypes.ColumnHeader);

			this.OnHeaderRowExporting(headerExportingArgs);

			// header export is not canceled
			if(!headerExportingArgs.Cancel)
			{
				exportHelper.extentOfCurrentGridObject.Location = exportHelper.currentPos;

				this.ExportColumnHeaderCells(exportHelper, row, band, true);

				this.SetCollapsedRows(
					exportHelper.CurrentWorksheet, 
					exportHelper.extentOfCurrentGridObject.Y, 
					exportHelper.extentOfCurrentGridObject.Bottom-1,
					exportHelper.HierarchyLevel);

				exportHelper.currentPos = exportHelper.extentOfCurrentGridObject.Location;
				this.IncreaseCurrentRow(exportHelper, 
					exportHelper.extentOfCurrentGridObject.Height);

				// fire HeaderRowExported
				HeaderRowExportedEventArgs headerExportedArgs =
					new HeaderRowExportedEventArgs(exportHelper, band, row, 
					HeaderTypes.ColumnHeader);

				this.OnHeaderRowExported(headerExportedArgs);
			}
		}

		private void ExportBandHeaderRow(UltraGridExporterHelper exportHelper,
			UltraGridRow row, UltraGridBand band)
		{
			// fire HeaderRowExporting
			HeaderRowExportingEventArgs headerExportingArgs =
				new HeaderRowExportingEventArgs(exportHelper, band, row, 
				HeaderTypes.BandHeader);

			this.OnHeaderRowExporting(headerExportingArgs);

			// header export is not canceled
			if(!headerExportingArgs.Cancel)
			{
				// fire header cell exporting
				HeaderCellExportingEventArgs headerCellExportingArgs = new
					HeaderCellExportingEventArgs(exportHelper, band.Header, row, 
					HeaderTypes.BandHeader);

				this.OnHeaderCellExporting( headerCellExportingArgs );

				// ZS 1/27/04 - Updated UWG2752
				// ZS 1/23/04 - UWG2752
                //
                // MBS 4/29/09 - TFS17050
                // We shouldn't be looking at the hard-coded value for the number of supported rows, since we
                // support different workbook formats now
                //
				//if(exportHelper.currentPos.Y >= Workbook.MaxExcelRowCount)
                if (exportHelper.currentPos.Y >= Workbook.GetMaxRowCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
				{
					if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
						headerCellExportingArgs.Cancel = true;
					else
						throw new Exception( Shared.SR.GetString("LER_MaxExcelRowCountLimit") );
				}

                // MBS 4/29/09 - TFS17050
                // We shouldn't be looking at the hard-coded value for the number of supported columns, since we
                // support different workbook formats now
                //
				//if(exportHelper.currentPos.X + exportHelper.extentOfCurrentGridObject.Width - 1 >= Workbook.MaxExcelColumnCount)
                if (exportHelper.currentPos.X + exportHelper.extentOfCurrentGridObject.Width - 1 >= Workbook.GetMaxColumnCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
				{
					if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
						headerCellExportingArgs.Cancel = true;
					else
						throw new Exception( Shared.SR.GetString("LER_MaxExcelColumnCountLimit") );
				}

				if(!headerCellExportingArgs.Cancel)
				{
					// write band header
					WorksheetCell excelCell = exportHelper.CurrentWorksheet.Rows[ 
						exportHelper.currentPos.Y ].Cells[ exportHelper.currentPos.X ];

					// MD 11/16/07 - BR28404
					// Moved below, the format string is now resolved based on the value at the time it set (date formatting).
					// If we set the value then set the formatting, the resolved format string will be over written.
					//excelCell.Value = band.Header.Caption;

					excelCell.CellFormat.SetFormatting( 
						this.ResolvedHeader(exportHelper, band.Header) );

					// MD 11/16/07 - BR28404
					// Moved from above, see comment on original line.
					excelCell.Value = band.Header.Caption;

					exportHelper.CurrentWorksheet.MergedCellsRegions.Add(
						exportHelper.currentPos.Y, 
						exportHelper.currentPos.X,
						exportHelper.currentPos.Y, 
						exportHelper.currentPos.X + 
						exportHelper.extentOfCurrentGridObject.Width - 1 );

					// fire header cell exported
					HeaderCellExportedEventArgs headerCellExportedArgs = new
						HeaderCellExportedEventArgs(exportHelper, band.Header, row,
						HeaderTypes.BandHeader);

					this.OnHeaderCellExported( headerCellExportedArgs );
				}

				this.SetCollapsedRow(exportHelper.CurrentWorksheet, 
					exportHelper.currentPos.Y, exportHelper.HierarchyLevel);

				this.IncreaseCurrentRow(exportHelper, 1);

				// fire HeaderRowExported
				HeaderRowExportedEventArgs headerExportedArgs =
					new HeaderRowExportedEventArgs(exportHelper, band, row,
					HeaderTypes.BandHeader);

				this.OnHeaderRowExported(headerExportedArgs);
			}
		}

		private void ExportColumnHeaderCells(UltraGridExporterHelper exportHelper,
			UltraGridRow row, UltraGridBand band, bool setRowHeights)
		{
			// MD 1/14/09 - Groups in RowLayout
			// We are now using the new caching mechanism
			#region Old Code

			

			#endregion Old Code

			GridExportCache.DocumentBandInfo documentBandInfo = this.GridExportCache.GetDocumentBandInfo( band, GridExportCache.PlaceholderType.ColumnHeaders );

			#region Group Headers

			if ( band.GroupHeadersVisible &&
				documentBandInfo.HasGroups &&
#pragma warning disable 0618
				band.UseRowLayout == false )
#pragma warning restore 0618
			{
				this.SetColumnWidthsAndRowHeights( exportHelper, row, null, new int[] { documentBandInfo.GroupList[ 0 ].Header.Height } );

				int currentX = 0;
				foreach ( UltraGridGroup group in documentBandInfo.GroupList )
				{
					int endPositionX = 0;
					foreach ( GridExportCache.DocumentHeaderInfo headerInfo in documentBandInfo )
					{
						if ( headerInfo.DocumentHeaderInfoType != GridExportCache.DocumentHeaderInfoType.Header )
							continue;

						UltraGridColumn column = headerInfo.Column;

						if ( column == null || column.Group != group )
							continue;

						endPositionX = Math.Max( endPositionX, headerInfo.OriginX + headerInfo.SpanX );
					}

					int spanX = endPositionX - currentX;
					GridExportCache.DocumentHeaderInfo documentColumnInfo = new GridExportCache.DocumentHeaderInfo( 
						group.Header, 
						currentX, 
						0, 
						spanX, 
						1, 
						GridExportCache.DocumentHeaderInfoType.Header, 
						null );

					this.ExportHeaderCell( exportHelper, row, documentColumnInfo );

					currentX = endPositionX;
				}

                // MRS 3/10/2009 - TFS14661
                this.SetCollapsedRow(exportHelper.CurrentWorksheet,
                    exportHelper.currentPos.Y, 
                    exportHelper.HierarchyLevel);

				Point position = exportHelper.currentPos;
				position.Y++;
				exportHelper.currentPos = position;

				Rectangle extent = exportHelper.extentOfCurrentGridObject;
				extent.Y++;
				exportHelper.extentOfCurrentGridObject = extent;                
			}

			#endregion // Group Headers

			if ( setRowHeights )
			{
				int[] logicalRowHeights = UltraGridExcelExporter.GetDimensionsFromAbsolutePositions( documentBandInfo.HeaderRowDims );
				this.SetColumnWidthsAndRowHeights( exportHelper, row, null, logicalRowHeights );
			}

			foreach ( GridExportCache.DocumentHeaderInfo documentColumnInfo in documentBandInfo )
			{
				switch ( documentColumnInfo.DocumentHeaderInfoType )
				{
					case GridExportCache.DocumentHeaderInfoType.Header:
						this.ExportHeaderCell( exportHelper, row, documentColumnInfo );
						break;

					case GridExportCache.DocumentHeaderInfoType.Blank:
						break;

					case GridExportCache.DocumentHeaderInfoType.Summary:
					case GridExportCache.DocumentHeaderInfoType.Cell:
						Debug.Fail( "This was not expected." );
						break;

					default:
						Debug.Fail( "Unknown DocumentColumnInfoType" );
						break;
				}
			}            
		}

		// MD 1/14/09 - Groups in RowLayout
		// A helper method was created from this code to allow it to be called from multiple places.
		private void ExportHeaderCell( UltraGridExporterHelper exportHelper, UltraGridRow row, GridExportCache.DocumentHeaderInfo documentColumnInfo )
		{
			HeaderBase gridColumnHeader = documentColumnInfo.Header;

			// fire header cell exporting
			exportHelper.currentPos.X = exportHelper.extentOfCurrentGridObject.X + documentColumnInfo.OriginX;
			exportHelper.currentPos.Y = exportHelper.extentOfCurrentGridObject.Y + documentColumnInfo.OriginY;

			HeaderCellExportingEventArgs headerCellExportingArgs = new
				HeaderCellExportingEventArgs( exportHelper, gridColumnHeader,
				row, HeaderTypes.ColumnHeader );

			this.OnHeaderCellExporting( headerCellExportingArgs );

			if ( !headerCellExportingArgs.Cancel )
			{
				// write header cell
				SetRegionRelativeToOrigin( exportHelper, documentColumnInfo.RelativeRect,
					gridColumnHeader.Caption,
					this.ResolvedHeader( exportHelper, gridColumnHeader ) );

				// fire header cell exported
				HeaderCellExportedEventArgs headerCellExportedArgs =
					new HeaderCellExportedEventArgs( exportHelper, gridColumnHeader, row, HeaderTypes.ColumnHeader );

				this.OnHeaderCellExported( headerCellExportedArgs );
			}
		}

		private void SetColumnWidthsAndRowHeights(UltraGridExporterHelper exportHelper,
			UltraGridRow row, int[] columnWidths, int[] rowHeights)
		{
			int i, size;

			if(columnWidths!=null)
				for(i=0 ; i<columnWidths.Length ; i++)
				{
					// ZS 1/27/04 - Updated UWG2752
					// ZS 1/23/04 - UWG2752
                    //
                    // MBS 4/29/09 - TFS17050
                    // We shouldn't be looking at the hard-coded value for the number of supported columns, since we
                    // support different workbook formats now
                    //
					//if( exportHelper.currentPos.X+i >= Workbook.MaxExcelColumnCount )
                    if (exportHelper.currentPos.X + i >= Workbook.GetMaxColumnCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
					{
						if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
							break;
						else
							throw new Exception( Shared.SR.GetString("LER_MaxExcelColumnCountLimit") );
					}

					WorksheetColumn wsCol = exportHelper.CurrentWorksheet.Columns[exportHelper.currentPos.X+i];

					size = (int) (columnWidths[i] * this.columnWidthUnitsInPixelX);

					if(wsCol.Width < size)
						wsCol.Width = size;
				}

			if(rowHeights!=null)
				for(i=0; i<rowHeights.Length ; i++)
				{
					// ZS 1/27/04 - Updated UWG2752
					// ZS 1/23/04 - UWG2752
                    //
                    // MBS 4/29/09 - TFS17050
                    // We shouldn't be looking at the hard-coded value for the number of supported rows, since we
                    // support different workbook formats now
                    //
					//if( exportHelper.currentPos.Y+i >= Workbook.MaxExcelRowCount )
                    if (exportHelper.currentPos.Y + i >= Workbook.GetMaxRowCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
					{
						if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
							break;
						else
							throw new Exception( Shared.SR.GetString("LER_MaxExcelRowCountLimit") );
					}

					WorksheetRow wsRow = exportHelper.CurrentWorksheet.Rows[exportHelper.currentPos.Y+i];

					size = (int)(rowHeights[i] * this.twipsInPixelY);

					if(wsRow.Height < size)
						wsRow.Height = size;
				}		
		}

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        // Changed this to return the WorkSheetCell that was written to. 
        //
		//private void SetRegionRelativeToOrigin(UltraGridExporterHelper exportHelper, 
        private WorksheetCell SetRegionRelativeToOrigin(UltraGridExporterHelper exportHelper, 
			Rectangle rect, object val, IWorksheetCellFormat cellFormat)
		{
			if(rect.Width<1 || rect.Height<1)
                // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
				//return;
                return null;

			exportHelper.currentPos.X = exportHelper.extentOfCurrentGridObject.X + rect.X;
			exportHelper.currentPos.Y = exportHelper.extentOfCurrentGridObject.Y + rect.Y;

			// ZS 1/27/04 - Updated UWG2752
			// ZS 1/23/04 - UWG2752

			bool cancel = false;

            // MBS 4/29/09 - TFS17050
            // We shouldn't be looking at the hard-coded value for the number of supported rows, since we
            // support different workbook formats now
            //
			//if(exportHelper.currentPos.Y + rect.Height - 1 >= Workbook.MaxExcelRowCount)
            if (exportHelper.currentPos.Y + rect.Height - 1 >= Workbook.GetMaxRowCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
			{
				if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
					cancel = true;
				else
					throw new Exception( Shared.SR.GetString("LER_MaxExcelRowCountLimit") );
			}

            // MBS 4/29/09 - TFS17050
            // We shouldn't be looking at the hard-coded value for the number of supported columns, since we
            // support different workbook formats now
            //
			//if(exportHelper.currentPos.X + rect.Width - 1 >= Workbook.MaxExcelColumnCount)
            if (exportHelper.currentPos.X + rect.Width - 1 >= Workbook.GetMaxColumnCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
			{
				if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
					cancel = true;
				else
					throw new Exception( Shared.SR.GetString("LER_MaxExcelColumnCountLimit") );
			}

			if(!cancel)
			{
                if (exportHelper.CurrentWorksheet.MergedCellsRegions.IsOverlappingWithMergedRegion(
                    exportHelper.currentPos.Y,
                    exportHelper.currentPos.X,
                    exportHelper.currentPos.Y + rect.Height - 1,
                    exportHelper.currentPos.X + rect.Width - 1))
                {
                    // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
                    //return;
                    return null;
                }
				
				if(rect.Width>1 || rect.Height>1)
				{
					exportHelper.CurrentWorksheet.MergedCellsRegions.Add(
						exportHelper.currentPos.Y, 
						exportHelper.currentPos.X,
						exportHelper.currentPos.Y + rect.Height - 1, 
						exportHelper.currentPos.X + rect.Width - 1 );	
				}

				WorksheetCell excelCell = exportHelper.CurrentWorksheet.Rows[ 
					exportHelper.currentPos.Y ].Cells[ exportHelper.currentPos.X ];

				// MD 11/16/07 - BR28404
				// Moved below, the format string is now resolved based on the value at the time it set (date formatting).
				// If we set the value then set the formatting, the resolved format string will be over written.
				//excelCell.Value = val;

				excelCell.CellFormat.SetFormatting( cellFormat );

				// MD 11/16/07 - BR28404
				// Moved from above, see comment on original line.
                // MRS 6/3/2009 - TFS17908
				//excelCell.Value = val;
                if (val is Image)
                {
                    Image image = (Image)val;

                    // Create a WorksheetImage
                    WorksheetImage worksheetImage = new WorksheetImage(image);

                    // Position the image. This position will be wrong, since the height of 
                    // the row has not yet been set. But we have to set the positions to 
                    // something in order to add the image. 
                    //
                    UltraGridExcelExporter.PositionWorksheetImage(excelCell.Worksheet, worksheetImage, rect);

                    // Add the image into the workbook
                    excelCell.Worksheet.Shapes.Add(worksheetImage);
                    Rectangle imageRect = new Rectangle(
                        exportHelper.currentPos.X,
                        exportHelper.currentPos.Y,
                        rect.Width - 1,
                        rect.Height - 1
                        );

                    // Store the worksheetImage and it's rect so that we can set it's bounds
                    // after the Row is exported. We have to do this because the row height's
                    // have not been established, yet, and they won't be until after the
                    // row is exported. 
                    //
                    this.UnpositionedImages[worksheetImage] = imageRect;
                }
                else
                {
                    excelCell.Value = val;
                }

                // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
                return excelCell;
			}

            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            return null;
		}

		private void ProcessSingleRow(UltraGridExporterHelper exportHelper, 
			UltraGridRow row)
		{
			// fire RowExporting
			RowExportingEventArgs rowExportingArgs = 
				new RowExportingEventArgs(exportHelper, row);

			this.OnRowExporting(rowExportingArgs);

			// row is not canceled
			if(!rowExportingArgs.Cancel)
			{
				exportHelper.extentOfCurrentGridObject.Location = exportHelper.currentPos;

				if(row is UltraGridGroupByRow)
					this.WriteGroupByRow(exportHelper, (UltraGridGroupByRow)row);
				else
					this.WriteSingleRow(exportHelper, row);

				this.SetCollapsedRows(
					exportHelper.CurrentWorksheet, 
					exportHelper.extentOfCurrentGridObject.Y, 
					exportHelper.extentOfCurrentGridObject.Bottom-1,
					exportHelper.HierarchyLevel);

				exportHelper.currentPos = exportHelper.extentOfCurrentGridObject.Location;
				this.IncreaseCurrentRow(exportHelper, 
					exportHelper.extentOfCurrentGridObject.Height);

                // MRS 6/3/2009 - TFS17908
                this.PositionWorksheetImages();

				// fire Row Exported
				RowExportedEventArgs rowExportedArgs = 
					new RowExportedEventArgs(exportHelper, row);

				this.OnRowExported(rowExportedArgs);
			}
		}

        // MRS 6/3/2009 - TFS17908
        #region PositionWorksheetImages
        private void PositionWorksheetImages()
        {
            if (this.unpositionedImages == null)
                return;

            foreach (WorksheetImage worksheetImage in this.UnpositionedImages.Keys)
            {
                Rectangle rect = this.UnpositionedImages[worksheetImage];
                UltraGridExcelExporter.PositionWorksheetImage(worksheetImage.Worksheet, worksheetImage, rect);
            }

            this.unpositionedImages.Clear();
        }
        #endregion // PositionWorksheetImages

        // MRS 6/3/2009 - TFS17908
        #region PositionWorksheetImage
        private static void PositionWorksheetImage(Worksheet worksheet, WorksheetImage worksheetImage, Rectangle rect)
        {
            // We will want to maintain the aspect ratio of the image so it looks the
            // same as it does in the grid cell. So determine the available size in the excel
            // worksheet. We do this by creating a region contain all three Excel cells, then 
            // getting the bounds.                    
            WorksheetRegion worksheetRegion = new WorksheetRegion(
                worksheet,
                rect.Top,
                rect.Left,
                rect.Bottom,
                rect.Right);

            Rectangle availableRect = worksheetRegion.GetBoundsInTwips();

            // Set the bounds of the image. This method automatically fits the image into the
            // bounds and maintains the image aspect ratio. Note that the aspect ratio may be
            // slightly off due to some differences between the units used for width in 
            // different versions of Excel. 
            worksheetImage.SetBoundsInTwips(worksheet, availableRect, true);
        }
        #endregion // PositionWorksheetImage

        private void WriteGroupByRow(UltraGridExporterHelper exportHelper, 
			UltraGridGroupByRow row)
		{
			// ZS 1/27/04 - Updated UWG2752
			// ZS 1/23/04 - UWG2752
            //
            // MBS 4/29/09 - TFS17050
            // We shouldn't be looking at the hard-coded value for the number of supported rows, since we
            // support different workbook formats now
            //
			//if(exportHelper.currentPos.Y >= Workbook.MaxExcelRowCount)
            if(exportHelper.currentPos.Y >= Workbook.GetMaxRowCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
			{
				if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
					return;
				else
					throw new Exception( Shared.SR.GetString("LER_MaxExcelRowCountLimit") );
			}

            // MBS 4/29/09 - TFS17050
            // We shouldn't be looking at the hard-coded value for the number of supported columns, since we
            // support different workbook formats now
            //
			//if(exportHelper.currentPos.X >= Workbook.MaxExcelColumnCount)
            if (exportHelper.currentPos.X >= Workbook.GetMaxColumnCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
			{
				if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
					return;
				else
					throw new Exception( Shared.SR.GetString("LER_MaxExcelColumnCountLimit") );
			}

			// write cell
			WorksheetCell excelCell = exportHelper.CurrentWorksheet.Rows[ 
				exportHelper.currentPos.Y ].Cells[ exportHelper.currentPos.X ];

			// MRS 11/7/05 - BR07250
			//
			//excelCell.Value = row.Description;
			// MD 11/16/07 - BR28404
			// Moved below, the format string is now resolved based on the value at the time it set (date formatting).
			// If we set the value then set the formatting, the resolved format string will be over written.
			//excelCell.Value = row.DescriptionWithSummaries;

			excelCell.CellFormat.SetFormatting( 
				this.ResolvedGroupByRow(exportHelper, row) );

			// MD 11/16/07 - BR28404
			// Moved from above, see comment on original line.
			excelCell.Value = row.DescriptionWithSummaries;

			WorksheetColumn wsCol = exportHelper.CurrentWorksheet.Columns[ exportHelper.currentPos.X ];
			// MRS 11/7/05 - BR07250
			//
			//int newSize = row.Description.Length * 256; 
			int newSize = row.DescriptionWithSummaries.Length * 256; 

			if(wsCol.Width < newSize)
				wsCol.Width = newSize;

			// SSP 10/26/06 BR16917
			// 
			// --------------------------------------------------------------------
			UltraGridSummaryRow summariesInsideGroupByRow = UltraGridSummaryRow.GetInGroupByRowSummaryContext( row );
			if ( null != summariesInsideGroupByRow )
			{
				exportHelper.currentPos.X++;

				if ( GroupBySummaryDisplayStyle.SummaryCellsAlwaysBelowDescription == summariesInsideGroupByRow.Band.GroupBySummaryDisplayStyleResolved )
					exportHelper.currentPos.Y++;

				this.ProcessSummaryRows( exportHelper, summariesInsideGroupByRow, summariesInsideGroupByRow.Band,
					summariesInsideGroupByRow.ParentCollection );
			}
			// --------------------------------------------------------------------
		}

		private void WriteSingleRow(UltraGridExporterHelper exportHelper, UltraGridRow row)
		{
			// MD 1/14/09 - Groups in RowLayout
			// Each row being written out will write out the header cells now, so we don't have to do this anymore.
			//if( row.Band.UseRowLayout && 
			//    row.Band.RowLayoutLabelStyle==RowLayoutLabelStyle.WithCellData )
			//{
			//    this.ExportColumnHeaderCells(exportHelper, row, row.Band, false);
			//
			//    exportHelper.currentPos = exportHelper.extentOfCurrentGridObject.Location;
			//}

			this.ExportRowValuesCells(exportHelper, row);

			if( row.AutoPreviewHeight>0 )
				this.ExportAutoPreviewRow(exportHelper, row);
		}

		private void ExportAutoPreviewRow(UltraGridExporterHelper exportHelper, 
			UltraGridRow row)
		{
			// fire CellExporting
			CellExportingEventArgs cellExportingArgs = new 
				CellExportingEventArgs(exportHelper, row, null);

			this.OnCellExporting(cellExportingArgs);

			if(!cellExportingArgs.Cancel)
			{
				// write cell
				Rectangle apRect = new Rectangle( 0, 
					exportHelper.extentOfCurrentGridObject.Height-1,
					exportHelper.extentOfCurrentGridObject.Width, 1 );

				IWorksheetCellFormat tmpCellFmt = 
					this.ResolvedRowAppearance(exportHelper, row);

				this.SetRegionRelativeToOrigin(exportHelper, apRect, row.Description, tmpCellFmt);

				// fire CellExported
				CellExportedEventArgs cellExportedArgs = new
					CellExportedEventArgs(exportHelper, row, null, row.Description);

				this.OnCellExported(cellExportedArgs);
			}
		}

		private void ExportRowValuesCells(UltraGridExporterHelper exportHelper, 
			UltraGridRow row)
		{
			// MD 1/14/09 - Groups in RowLayout
			// We are now using the new caching mechanism
			#region Old Code

			

			#endregion Old Code
			GridExportCache.DocumentBandInfo documentBandInfo = this.GridExportCache.GetDocumentBandInfo( row.Band, GridExportCache.PlaceholderType.Values );

			int[] logicalColumnWidths = UltraGridExcelExporter.GetDimensionsFromAbsolutePositions( documentBandInfo.ColDims );
			int[] logicalRowHeights = UltraGridExcelExporter.GetDimensionsFromAbsolutePositions( documentBandInfo.HeaderRowDims );

			this.SetColumnWidthsAndRowHeights( exportHelper, row, logicalColumnWidths, logicalRowHeights );

			foreach ( GridExportCache.DocumentHeaderInfo documentColumnInfo in documentBandInfo )
			{
				UltraGridColumn gridColumn = documentColumnInfo.Column;

				switch ( documentColumnInfo.DocumentHeaderInfoType )
				{
					case GridExportCache.DocumentHeaderInfoType.Cell:
						{
                            // MRS 4/21/2009 - TFS16877
                            // This got ripped out when the RowLayout Groups feature was 
                            // implemented above. 
                            //
                            // skip hidden cells
                            if (row.HasCell(gridColumn) && row.Cells[gridColumn].Hidden)
                                continue;

							// fire CellExporting
							exportHelper.currentPos.X = exportHelper.extentOfCurrentGridObject.X + documentColumnInfo.OriginX;
							exportHelper.currentPos.Y = exportHelper.extentOfCurrentGridObject.Y + documentColumnInfo.OriginY;

							CellExportingEventArgs cellExportingArgs = new
								CellExportingEventArgs( exportHelper, row, gridColumn );

							this.OnCellExporting( cellExportingArgs );

							if ( !cellExportingArgs.Cancel )
							{
								// write cell
								object resolvedVal = WriteValueCell( exportHelper, row, gridColumn, documentColumnInfo.RelativeRect );

								// fire CellExported
								CellExportedEventArgs cellExportedArgs = new
									CellExportedEventArgs( exportHelper, row, gridColumn, resolvedVal );

								this.OnCellExported( cellExportedArgs );
							}
						}
						break;

					case GridExportCache.DocumentHeaderInfoType.Header:
						this.ExportHeaderCell( exportHelper, row, documentColumnInfo );
						break;

					case GridExportCache.DocumentHeaderInfoType.Blank:					
						break;

					case GridExportCache.DocumentHeaderInfoType.Summary:
						Debug.Fail( "This was not expected." );
						break;

					default:
						Debug.Fail( "Unknown DocumentColumnInfoType" );
						break;
				}
			}
		}

		// MD 1/14/09 - Groups in RowLayout
		private static int[] GetDimensionsFromAbsolutePositions( int[] positions )
		{
			if ( positions == null )
				return null;

			int[] logicalColumnWidths = new int[ positions.Length - 1 ];

			for ( int i = 0; i < positions.Length - 1; i++ )
				logicalColumnWidths[ i ] = positions[ i + 1 ] - positions[ i ];

			return logicalColumnWidths;
		}

		private static bool IsValueListCell(UltraGridRow row, UltraGridColumn ugColumn)
		{
			if(row.HasCell(ugColumn))
				return (row.Cells[ugColumn].ValueListResolved!=null);
			else
				return (ugColumn.ValueList!=null);
		}

		private object WriteValueCell(UltraGridExporterHelper exportHelper, 
			UltraGridRow row, UltraGridColumn ugColumn, Rectangle valueCellRect)
		{
			// MRS 7/11/05 - BR04865
			// Added a GetExportValue method to the grid row. This will account
			// for when the cell is using an editor that has a ValueList, as well
			// as when the column/cell has a ValueList
			//-----------------------------------------------------------
			//			object val = null, cellValue = row.GetCellValue(ugColumn);
			//			
			//			if(IsValueListCell(row,ugColumn) || ugColumn.DataType.IsEnum)
			//			{
			//				val = row.GetCellText(ugColumn);
			//			}
			//			else
			//			{
			//				//~ ZS 5/6/04 UWG3229 - If cell type is not supported, we will call GetCellText. 
			//				//if( cellValue!=null && WorksheetCell.IsCellTypeSupported(cellValue.GetType()) )
			//				if( cellValue!=null) 
			//				{
			//					if( WorksheetCell.IsCellTypeSupported(cellValue.GetType()) )
			//						val = cellValue;
			//					else
			//						val = row.GetCellText(ugColumn);
			////				}
			//			}
			object val = null, cellValue = row.GetExportValue(ugColumn);
			if( cellValue!=null) 
			{
                // MRS 6/3/2009 - TFS17908
                //
                //if (WorksheetCell.IsCellTypeSupported(cellValue.GetType()))
                //    val = cellValue;
                //else
                //    val = row.GetCellText(ugColumn);
                //
                // ---------------------------------------------------------                
                EmbeddableEditorBase editor = (row != null && ugColumn != null && row.IsSummaryRow == false)
                    ? row.GetEditorResolved(ugColumn)
                    : null;

                EmbeddableImageRenderer embeddableImageRenderer = editor as EmbeddableImageRenderer;
                if (embeddableImageRenderer != null)
                    val = GridExportCache.GetImageRendererImage(row, ugColumn, cellValue);
                else
                {
                    if (WorksheetCell.IsCellTypeSupported(cellValue.GetType()))
                        val = cellValue;
                    else
                        val = row.GetCellText(ugColumn);
                }
                // ---------------------------------------------------------
			}
			//-----------------------------------------------------------	

			if(val is string) 
				// MRS 4/8/05 - Replaced Case with CharacterCasing
				//				switch(ugColumn.Case)
				//				{
				//					case Case.Lower:
				//						val = ((string)val).ToLower();
				//						break;
				//
				//					case Case.Upper:
				//						val = ((string)val).ToUpper();
				//						break;
				//
				//					case Case.Unchanged:
				//						break;
				//				}
				switch(ugColumn.CharacterCasing)
				{
					case CharacterCasing.Lower:
						val = ((string)val).ToLower();
						break;

					case CharacterCasing.Upper:
						val = ((string)val).ToUpper();
						break;

					case CharacterCasing.Normal:
						break;
				}

			IWorksheetCellFormat tmpCellFmt = 
				this.ResolvedXFCell(exportHelper, row, ugColumn);

			object objFmt;

			if(!exportHelper.columnExcelFormats.ContainsKey(ugColumn))
			{
				InitializeColumnEventArgs convertArgs = 
					new InitializeColumnEventArgs(ugColumn.Format, null, ugColumn);

				this.OnInitializeColumn(convertArgs);

				if(convertArgs.ExcelFormatStr=="")
					convertArgs.ExcelFormatStr = null;
				
				exportHelper.columnExcelFormats.Add(ugColumn, convertArgs.ExcelFormatStr);

				objFmt = convertArgs.ExcelFormatStr;
			}
			else
				objFmt = exportHelper.columnExcelFormats[ugColumn];
			
			if(objFmt!=null)
				tmpCellFmt.FormatString = (string) objFmt;

            // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
            //// cell
            //SetRegionRelativeToOrigin(exportHelper, valueCellRect, val, tmpCellFmt);
            WorksheetCell worksheetCell = SetRegionRelativeToOrigin(exportHelper, valueCellRect, val, tmpCellFmt);
            if (this.currentExportFormulas &&
                worksheetCell != null)
            {
                this.MappingManager.MapWorksheetCell(ugColumn, row, worksheetCell);
            }

			return val;
		}

		private IWorksheetCellFormat ResolvedXFCell(UltraGridExporterHelper exportHelper, 
			UltraGridRow row, UltraGridColumn column)
		{
			AppearanceData apData = new AppearanceData();

			row.ResolveCellAppearance(column, ref apData, ExcelRelevantPropFlags);

            // MRS 1/11/07 - BR19086
            // If the cell resolved it's BackColor to Transparent, we want to use the 
            // RowAppearance for the cell, so the row appearnce comes through, just
            // like it does in in the on-screen grid.
            // -------------------------------------------------------------------------
            AppearancePropFlags rowAppearanceFlags = 0;
            if (apData.BackColor == Color.Transparent)
            {
                rowAppearanceFlags |= AppearancePropFlags.BackColor;
                apData.BackColor = Color.Empty;
            }

            if (apData.BackColor2 == Color.Transparent)
            {
                rowAppearanceFlags |= AppearancePropFlags.BackColor2;
                apData.BackColor2 = Color.Empty;
            }

            if (apData.BackColorDisabled == Color.Transparent)
            {
                rowAppearanceFlags |= AppearancePropFlags.BackColorDisabled;
                apData.BackColorDisabled = Color.Empty;
            }

            rowAppearanceFlags &= ExcelRelevantPropFlags;

            row.ResolveAppearance(ref apData, rowAppearanceFlags);
            // -------------------------------------------------------------------------

			if(row.GetCellActivationResolved(column)==Activation.Disabled)
			{
				if(!apData.BackColorDisabled.IsEmpty)
					apData.BackColor = apData.BackColorDisabled;

				if(!apData.ForeColorDisabled.IsEmpty)
					apData.ForeColor = apData.ForeColorDisabled;
				else
					apData.ForeColor = SystemColors.GrayText;
			}

			// MRS BR01870
			// If this is a multiline cell, we need to set WrapText
			//return this.GetCellFormatFromAppearanceData(exportHelper, apData);
			IWorksheetCellFormat cellFormat = this.GetCellFormatFromAppearanceData(exportHelper, apData);
			
			if (column.CellMultiLineResolved == DefaultableBoolean.True)
				cellFormat.WrapText = ExcelDefaultableBoolean.True;

			return cellFormat;
		}

		private IWorksheetCellFormat ResolvedRowAppearance(
			UltraGridExporterHelper exportHelper, UltraGridRow row)
		{
			AppearanceData apData = new AppearanceData();

			row.ResolveAppearance(ref apData, ExcelRelevantPropFlags, true);

			return this.GetCellFormatFromAppearanceData(exportHelper, apData);
		}

		private IWorksheetCellFormat ResolvedHeader(UltraGridExporterHelper exportHelper, 
			HeaderBase header)
		{
			AppearanceData apData = new AppearanceData();

			header.ResolveAppearance(ref apData, ExcelRelevantPropFlags);

			// MRS BR01870
			// If this is a multiline header, we need to set WrapText
			//return this.GetCellFormatFromAppearanceData(exportHelper, apData);
			IWorksheetCellFormat cellFormat = this.GetCellFormatFromAppearanceData(exportHelper, apData);
			
			if (header.Band != null
				&& header.Band.ColHeaderLines > 1)
			{
				cellFormat.WrapText = ExcelDefaultableBoolean.True;
			}

			// MD 4/24/08 - 8.2 - Rotated Column Headers
			// Sync the header rotation on the excel cell.
			int rotation = UltraGridExcelExporter.GetExcelRotationValue( header.TextOrientationResolved );

			if ( rotation >= 0 )
				cellFormat.Rotation = rotation;

			return cellFormat;			
		}

		// MD 4/24/08 - 8.2 - Rotated Column Headers
		private static int GetExcelRotationValue( TextOrientationInfo textOrientation )
		{
			if ( textOrientation == null )
				return -1;

			if ( textOrientation.FlowDirection == TextFlowDirection.Vertical )
				return 255;

			int degrees = textOrientation.Degrees;

			// Return the appropriate excel rotation value depending on whether it is rotated up or down.
			if ( 0 <= degrees && degrees <= 90 )
				return degrees;

			if ( -90 <= degrees && degrees <= -1 )
				return 90 - degrees;

			Debug.Fail( "We didn't resolve the angle correctly." );
			return -1;
		}

		private IWorksheetCellFormat ResolvedSummary(UltraGridExporterHelper exportHelper, 
			SummaryValue summary)
		{
			AppearanceData apData = new AppearanceData();

			AppearancePropFlags tmpFlags = ExcelRelevantPropFlags;
			summary.ResolveAppearance(ref apData, ref tmpFlags);

			return this.GetCellFormatFromAppearanceData(exportHelper, apData);
		}

		// MRS 8/28/06 - BR14930
		private IWorksheetCellFormat ResolvedSummaryFooterCaption(UltraGridExporterHelper exportHelper, 
			SummaryValuesCollection summaries)
		{
			AppearanceData apData = new AppearanceData();

			AppearancePropFlags tmpFlags = ExcelRelevantPropFlags;
			summaries.ResolveSummaryFooterCaptionAppearance(ref apData, ref tmpFlags);

			return this.GetCellFormatFromAppearanceData(exportHelper, apData);
		}

		private IWorksheetCellFormat ResolvedGroupByRow(UltraGridExporterHelper exportHelper, 
			UltraGridGroupByRow row)
		{
			AppearanceData apData = new AppearanceData();

			row.ResolveAppearance(ref apData, ExcelRelevantPropFlags);

			return this.GetCellFormatFromAppearanceData(exportHelper, apData);
		}

		private IWorksheetCellFormat GetCellFormatFromAppearanceData(
			UltraGridExporterHelper exportHelper, AppearanceData apData)
		{
			Workbook wb = exportHelper.CurrentWorksheet.Workbook;

			IWorksheetCellFormat cellFormat = wb.CreateNewWorksheetCellFormat();

			if(apData.BackColor!=Color.Empty &&
				apData.BackColor.ToArgb()!=Color.White.ToArgb())
			{
				cellFormat.FillPatternForegroundColor = apData.BackColor;
				cellFormat.FillPattern = FillPatternStyle.Solid;
			}

			if(apData.BorderColor!=Color.Empty)
			{
				cellFormat.LeftBorderColor = 
					cellFormat.RightBorderColor = 
					cellFormat.TopBorderColor = 
					cellFormat.BottomBorderColor = apData.BorderColor;

				cellFormat.LeftBorderStyle = 
					cellFormat.RightBorderStyle = 
					cellFormat.TopBorderStyle =
					cellFormat.BottomBorderStyle = CellBorderLineStyle.Thin;
			}

            // MRS 7/17/2008 - BR34498
            // This code is making the incorrect assumption that the grid's default font and 
            // Excel's default font are the same. They are not the same by default. So it doesn't 
            // matter if the font or forecolor are being set, we need to apply the font from the 
            // grid anyway. I commented out the "if" statement so this code always gets called. 
            // Also, it's basing everything on the FontData instead of an actual Font object. 
            //
            #region Old Code
            //if (apData.HavePropertiesBeenSet(AppearancePropFlags.FontData |
            //    AppearancePropFlags.ForeColor))
            //{
            //    IWorkbookFont wbFont = wb.CreateNewWorkbookFont();

            //    if(apData.FontData.Name!=null)
            //        wbFont.Name = apData.FontData.Name;

            //    if( ((int)(apData.FontData.SizeInPoints * 20)) != 0 )
            //        wbFont.Height = (int)(apData.FontData.SizeInPoints * 20);
            //    else
            //        wbFont.Height = (int)(Control.DefaultFont.SizeInPoints * 20);

            //    wbFont.Color = apData.ForeColor;

            //    if(apData.FontData.Bold==DefaultableBoolean.True)
            //        wbFont.Bold = ExcelDefaultableBoolean.True;

            //    if(apData.FontData.Italic==DefaultableBoolean.True)
            //        wbFont.Italic = ExcelDefaultableBoolean.True;

            //    if(apData.FontData.Strikeout==DefaultableBoolean.True)
            //        wbFont.Strikeout = ExcelDefaultableBoolean.True;

            //    if(apData.FontData.Underline==DefaultableBoolean.True)
            //        wbFont.UnderlineStyle = FontUnderlineStyle.Single;

            //    cellFormat.Font.SetFontFormatting(wbFont);
            //}
            #endregion //Old Code
            //
            // -----------------------------------------------------------
            IWorkbookFont wbFont = wb.CreateNewWorkbookFont();

            UltraGrid grid = this.GridExportCache.Layout.Grid as UltraGrid;
            Font font = grid.Font;
            Font createdFont = apData.CreateFont(font);
            if (null != createdFont)
                font = createdFont;

            wbFont.Name = font.Name;
            int sizeInPoints = (int)Math.Round(((double)font.SizeInPoints * 2d), MidpointRounding.AwayFromZero) / 2;
            int twips = sizeInPoints * 20;
            wbFont.Height = twips;
            wbFont.Color = apData.ForeColor;

            if (font.Bold)
                wbFont.Bold = ExcelDefaultableBoolean.True;

            if (font.Italic)
                wbFont.Italic = ExcelDefaultableBoolean.True;

            if (font.Strikeout)
                wbFont.Strikeout = ExcelDefaultableBoolean.True;

            if (font.Underline)
                wbFont.UnderlineStyle = FontUnderlineStyle.Single;

            cellFormat.Font.SetFontFormatting(wbFont);

            if (createdFont != null)
                createdFont.Dispose();
            // -----------------------------------------------------------

			switch(apData.TextHAlign)
			{
				case HAlign.Left:
					cellFormat.Alignment = HorizontalCellAlignment.Left;
					break;

				case HAlign.Center:
					cellFormat.Alignment = HorizontalCellAlignment.Center;
					break;

				case HAlign.Right:
					cellFormat.Alignment = HorizontalCellAlignment.Right;
					break;

				default:
					cellFormat.Alignment = HorizontalCellAlignment.General;
					break;
			}

			switch(apData.TextVAlign)
			{
				case VAlign.Top:
					cellFormat.VerticalAlignment = VerticalCellAlignment.Top;
					break;

				case VAlign.Middle:
					cellFormat.VerticalAlignment = VerticalCellAlignment.Center;
					break;

				case VAlign.Bottom:
					goto default;

				default:
					cellFormat.VerticalAlignment = VerticalCellAlignment.Bottom;
					break;
			}

			return cellFormat;
		}

		private bool ColumnSummariesExist(
			// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
			// Export summaries displayed on top. Added summaryRowContext parameter.
			// 
			// MD 1/14/09 - Groups in RowLayout
			// Changed the parameters. This will now take the collection of layout items instead of the first visible column.
			//UltraGridSummaryRow summaryRowContext,
			//SummaryValuesCollection summaries, 
			//UltraGridColumn firstColumn, int level)
			UltraGridSummaryRow summaryRowContext,
			SummaryValuesCollection summaries,
			GridExportCache.DocumentBandInfo documentBandInfo, 
			int level )
		{
			// MD 1/14/09 - Groups in RowLayout
			// We are now using the new caching mechanism
			#region Old Code

			

			#endregion Old Code
			foreach ( GridExportCache.DocumentHeaderInfo headerInfo in documentBandInfo )
			{
				switch ( headerInfo.DocumentHeaderInfoType )
				{
					case GridExportCache.DocumentHeaderInfoType.Header:
					case GridExportCache.DocumentHeaderInfoType.Summary:
						{
							UltraGridColumn gridColumn = headerInfo.Column;

							if ( gridColumn == null )
								break;

							SummaryValue summary = summaries.GetSummaryValueFromPosition(
								SummaryPosition.UseSummaryPositionColumn,
								gridColumn,
								level,
								summaryRowContext );

							if ( summary != null )
								return true;
						}
						break;

					
					case GridExportCache.DocumentHeaderInfoType.Cell:
						Debug.Fail( "This was not expected." );
						break;

					case GridExportCache.DocumentHeaderInfoType.Blank:
						break;

					default:
						Debug.Fail( "Unknown DocumentHeaderInfoType" );
						break;
				}
			}

			return false;
		}

		// MRS 11/1/05 - BR07222
		// Added this method to get all the Summaries on a particular 
		// level in a RowLayout.
		// MD 1/14/09 - Groups in RowLayout
		// Redefined the parameters because this now takes the collection of layout items instead of the first visible column.
		//private SummaryValue[] GetSummariesForLevel(SummaryValuesCollection summaries,
		//    UltraGridColumn firstColumn, int level,
		//    UltraGridSummaryRow summmaryRow) // MRS 7/5/07 - BR24588
		private SummaryValue[] GetSummariesForLevel( 
			SummaryValuesCollection summaries,
			GridExportCache.DocumentBandInfo documentBandInfo, 
			int level,
			UltraGridSummaryRow summmaryRow ) // MRS 7/5/07 - BR24588
		{
			// MD 1/14/09 - Groups in RowLayout
			// We are now using the new caching mechanism
			#region Old Code

			

			#endregion Old Code
			List<SummaryValue> list = new List<SummaryValue>();

			foreach ( GridExportCache.DocumentHeaderInfo headerInfo in documentBandInfo )
			{
				switch ( headerInfo.DocumentHeaderInfoType )
				{
					case GridExportCache.DocumentHeaderInfoType.Header:
					case GridExportCache.DocumentHeaderInfoType.Summary:
						{
							UltraGridColumn gridColumn = headerInfo.Column;

							if ( gridColumn == null )
								break;

							SummaryValue summary = summaries.GetSummaryValueFromPosition(
								SummaryPosition.UseSummaryPositionColumn,
								gridColumn,
								level,
								summmaryRow );

							if ( summary != null )
								list.Add( summary );
						}
						break;
					
					case GridExportCache.DocumentHeaderInfoType.Cell:
						Debug.Fail( "This was not expected." );
						break;

					case GridExportCache.DocumentHeaderInfoType.Blank:
						break;

					default:
						Debug.Fail( "Unknown DocumentHeaderInfoType" );
						break;
				}
			}

			return list.ToArray();
		}

		private bool FreeformSummariesExist(
			// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
			// Export summaries displayed on top. Added summaryRowContext parameter.
			// 
			UltraGridSummaryRow summaryRowContext,
			SummaryValuesCollection summaries, 
			int level, out SummaryValue leftSV, out SummaryValue centerSV, out SummaryValue rightSV)
		{
			// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
			// Pass around the new summaryRowContext parameter.
			// 
			
			leftSV = summaries.GetSummaryValueFromPosition(SummaryPosition.Left, null, level, summaryRowContext );
			centerSV = summaries.GetSummaryValueFromPosition(SummaryPosition.Center, null, level, summaryRowContext );
			rightSV = summaries.GetSummaryValueFromPosition(SummaryPosition.Right, null, level, summaryRowContext );

			return (leftSV!=null || centerSV!=null || rightSV!=null);
		}

		// MRS 7/12/05 - BR04874
		//		private void ExportSummaryCaptionRow(UltraGridExporterHelper exportHelper, 
		//			SummaryValuesCollection summaries, IWorksheetCellFormat defaultSummaryFormatting)
		private void ExportSummaryCaptionRow(UltraGridExporterHelper exportHelper, 
			SummaryValuesCollection summaries, IWorksheetCellFormat defaultSummaryFormatting, UltraGridSummaryRow summaryRow)
		{				
			
			// MRS 7/12/05 - BR04874
			// MRS 8/1/05 - added a check for null and 0 row count
			if (summaryRow != null && 
				summaryRow.ParentCollection.Count > 0 )
			{
				UltraGridGroupByRow firstRow = summaryRow.ParentCollection[0] as UltraGridGroupByRow;
				int groupByDescendantLevelCount  = this.GetGroupByDescendantLevelCount(firstRow);
				exportHelper.currentPos.X += groupByDescendantLevelCount;
			}

			// fire SummaryRowExporting
			SummaryRowExportingEventArgs summaryExportingArgs = 
				new SummaryRowExportingEventArgs(exportHelper, summaries, -1);

			this.OnSummaryRowExporting(summaryExportingArgs);

			// summary is not canceled
			if(!summaryExportingArgs.Cancel)
			{
				// fire SummaryCellExporting
				SummaryCellExportingEventArgs summaryCellExportingEA = new 
					SummaryCellExportingEventArgs(exportHelper, null, -1);

				this.OnSummaryCellExporting(summaryCellExportingEA);

				// ZS 1/27/04 - Updated UWG2752
				// ZS 1/23/04 - UWG2752
                //
                // MBS 4/29/09 - TFS17050
                // We shouldn't be looking at the hard-coded value for the number of supported rows, since we
                // support different workbook formats now
                //
				//if(exportHelper.currentPos.Y >= Workbook.MaxExcelRowCount)
                if (exportHelper.currentPos.Y >= Workbook.GetMaxRowCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
				{
					if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
						summaryCellExportingEA.Cancel = true;
					else
						throw new Exception( Shared.SR.GetString("LER_MaxExcelRowCountLimit") );
				}

                // MBS 4/29/09 - TFS17050
                // We shouldn't be looking at the hard-coded value for the number of supported columns, since we
                // support different workbook formats now
                //
				//if(exportHelper.currentPos.X + exportHelper.extentOfCurrentGridObject.Width - 1 >= Workbook.MaxExcelColumnCount)
                if (exportHelper.currentPos.X + exportHelper.extentOfCurrentGridObject.Width - 1 >= Workbook.GetMaxColumnCount(exportHelper.CurrentWorksheet.Workbook.CurrentFormat))
				{
					if(this.fileLimitBehaviour==FileLimitBehaviour.TruncateData)
						summaryCellExportingEA.Cancel = true;
					else
						throw new Exception( Shared.SR.GetString("LER_MaxExcelColumnCountLimit") );
				}
				
				if(!summaryCellExportingEA.Cancel)
				{
					WorksheetCell excelCell = exportHelper.CurrentWorksheet.Rows[ 
						exportHelper.currentPos.Y ].Cells[ exportHelper.currentPos.X ];

					exportHelper.CurrentWorksheet.MergedCellsRegions.Add(
						exportHelper.currentPos.Y, 
						exportHelper.currentPos.X,
						exportHelper.currentPos.Y, 
						exportHelper.currentPos.X + 
						exportHelper.extentOfCurrentGridObject.Width - 1 );

					// MD 11/16/07 - BR28404
					// Moved below, the format string is now resolved based on the value at the time it set (date formatting).
					// If we set the value then set the formatting, the resolved format string will be over written.
					//excelCell.Value = summaries.SummaryFooterCaptionResolved;

					excelCell.CellFormat.SetFormatting( defaultSummaryFormatting );

					// MD 11/16/07 - BR28404
					// Moved from above, see comment on original line.
					excelCell.Value = summaries.SummaryFooterCaptionResolved;

					// fire SummaryCellExported
					SummaryCellExportedEventArgs summaryCellExportedEA = new
						SummaryCellExportedEventArgs(exportHelper, null, -1);

					this.OnSummaryCellExported(summaryCellExportedEA);
				}

				this.SetCollapsedRow(exportHelper.CurrentWorksheet, 
					exportHelper.currentPos.Y, exportHelper.HierarchyLevel);

				this.IncreaseCurrentRow(exportHelper, 1);

				// fire SummaryRowExported
				SummaryRowExportedEventArgs summaryExportedArgs =
					new SummaryRowExportedEventArgs(exportHelper, summaries, -1);

				this.OnSummaryRowExported(summaryExportedArgs);
			}
		}

		// MRS 7/12/05 - BR04874
//		private void ExportSummaryCell(UltraGridExporterHelper exportHelper, 
//			SummaryValue summary, int summaryLevel, 
//			IWorksheetCellFormat defaultSummaryFormatting, Rectangle summaryRect)
		private void ExportSummaryCell(UltraGridExporterHelper exportHelper, 
			SummaryValue summary, int summaryLevel, 
			IWorksheetCellFormat defaultSummaryFormatting, Rectangle summaryRect, 
			UltraGridSummaryRow summaryRow)
		{
			// MRS 7/12/05 - BR04874
			// MRS 8/1/05 - Added a check for null and 0 row count
			if (summaryRow != null && 
				summaryRow.ParentCollection.Count >0)
			{
				UltraGridGroupByRow firstRow = summaryRow.ParentCollection[0] as UltraGridGroupByRow;
				int groupByDescendantLevelCount  = this.GetGroupByDescendantLevelCount(firstRow);
				summaryRect.X += groupByDescendantLevelCount;
			}

			// fire SummaryCellExporting
			exportHelper.currentPos.X = exportHelper.extentOfCurrentGridObject.X + summaryRect.X;
			exportHelper.currentPos.Y = exportHelper.extentOfCurrentGridObject.Y + summaryRect.Y;

			SummaryCellExportingEventArgs summaryCellExportingEA = new 
				SummaryCellExportingEventArgs(exportHelper, summary, summaryLevel);

			this.OnSummaryCellExporting(summaryCellExportingEA);

			if(!summaryCellExportingEA.Cancel)
			{
				object val;
				IWorksheetCellFormat summaryFmt;

				if(summary!=null)
				{
					val = summary.SummaryText;
					summaryFmt = this.ResolvedSummary(exportHelper, summary);
				}
				else
				{
					val = null;
					summaryFmt = defaultSummaryFormatting;
				}
                // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
                if (summary != null)
                {
                    SummarySettings summarySettings = summary.SummarySettings;
                    string formatString;
                    bool success = exportHelper.summaryExcelFormats.TryGetValue(summarySettings, out formatString);
                    if (!success)
                    {
                        InitializeSummaryEventArgs convertArgs =
                            new InitializeSummaryEventArgs(summarySettings.DisplayFormatResolved, null, summarySettings);

                        this.OnInitializeSummary(convertArgs);

                        exportHelper.summaryExcelFormats.Add(summarySettings, convertArgs.ExcelFormatStr);

                        formatString = convertArgs.ExcelFormatStr;
                    }

                    if (formatString != null && formatString.Length > 0)
                        summaryFmt.FormatString = formatString;
                }

                // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
                //// cell
                //this.SetRegionRelativeToOrigin(exportHelper, summaryRect, val, summaryFmt);
                WorksheetCell worksheetCell = this.SetRegionRelativeToOrigin(exportHelper, summaryRect, val, summaryFmt);                
                if (this.currentExportFormulas &&
                    summary != null && worksheetCell != null)
                {
                    this.MappingManager.MapWorksheetCell(new SummaryInfo(summary, summaryRow), worksheetCell);
                }                

				// fire SummaryCellExported
				SummaryCellExportedEventArgs summaryCellExportedEA = new
					SummaryCellExportedEventArgs(exportHelper, summary, summaryLevel);

				this.OnSummaryCellExported(summaryCellExportedEA);
			}
		}        

		private void ProcessSummaryRows(UltraGridExporterHelper exportHelper, 
			UltraGridRow row, UltraGridBand band, RowsCollection rowsCollection)
		{
			SummaryValuesCollection summaries;
			AppearanceData apData;

			summaries = rowsCollection.SummaryValues;	
			
			apData = new AppearanceData();

			AppearancePropFlags tmpFlags = ExcelRelevantPropFlags;
			summaries.ResolveSummaryFooterAppearance(ref apData, ref tmpFlags);

			IWorksheetCellFormat defaultSummaryFormatting = 
				this.GetCellFormatFromAppearanceData(exportHelper, apData);
			
			// MRS 8/28/06 - BR14930
			IWorksheetCellFormat summaryFooterCaptionFormatting = this.ResolvedSummaryFooterCaption(exportHelper, summaries);

			// MRS 9/27/04 - UWG2786
			//			UltraGridColumn firstColumn = band.GetFirstVisibleCol(null, 
			//				true).GetRelatedVisibleColumn( VisibleRelation.First );
            // MRS v7.2 - Document Exporter
            //
			//UltraGridColumn firstColumn = this.dataSchema[band] as UltraGridColumn;

			// MD 1/14/09 - Groups in RowLayout
			// We are now using the new caching mechanism so we need the collection of layout items instead of the first visible column.
            //UltraGridColumn firstColumn = this.GridExportCache.GetFirstVisibleColumn(band);
            // MRS 8/19/2009 - TFS20658
			//GridExportCache.DocumentBandInfo documentBandInfo = this.GridExportCache.GetDocumentBandInfo( band, GridExportCache.PlaceholderType.Summaries );
            //
            UltraGridSummaryRow summaryRow = row as UltraGridSummaryRow;
            GridExportCache.DocumentBandInfo documentBandInfo = summaryRow == null
                ? this.GridExportCache.GetDocumentBandInfo(band, GridExportCache.PlaceholderType.Summaries)
                : this.GridExportCache.GetDocumentBandInfo(summaryRow);

			bool summaryCaptionExported = false;

			// MRS 11/1/05 - BR07222
			// If this band is using a RowLayout, store the SummaryDisplayArea property of 
			// all summaries, so we can reset them later. 
			Hashtable hiddenSummariesTable = null;

#pragma warning disable 0618
			bool useRowLayout = band.UseRowLayout;
#pragma warning restore 0618

			if (useRowLayout)
			{
				hiddenSummariesTable = new Hashtable(summaries.Count);
				foreach (SummaryValue summary in summaries)
					hiddenSummariesTable.Add(summary, summary.SummarySettings.SummaryDisplayArea);
			}

            // MBS 6/30/09 - TFS18248
            // When we're exporting summary rows, we should always be starting at the same X position as
            // we do for the first row.  The IncreaseCurrentRow method will reset the X position, which is
            // wrong in this particular case because all of the summaries are specified in relative positions
            // to the original column.  We will keep track of the X position here to ensure that all summary
            // positions are calculated relative to the same position
            int currentXPos = exportHelper.currentPos.X;

			// column summaries loop
			for(int summaryLevel=0 ; 
				// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
				// Export summaries displayed on top. Pass the summaryRowContext parameter.
				// 
				//ColumnSummariesExist(summaries, firstColumn, summaryLevel) ; 
				// MD 1/14/09 - Groups in RowLayout
				// We are now using the new caching mechanism so we need to pass the collection of layout items instead of the first visible column.
				//ColumnSummariesExist( row as UltraGridSummaryRow, summaries, firstColumn, summaryLevel ); 
				ColumnSummariesExist( row as UltraGridSummaryRow, summaries, documentBandInfo, summaryLevel ); 
				summaryLevel++)
			{
				// summary caption
				if(summaryLevel==0 && summaries.SummaryFooterCaptionVisibleResolved
					// SSP 10/27/06 BR16917
					// 
					&& ( ! ( row is UltraGridSummaryRow ) || ((UltraGridSummaryRow)row).ShouldDisplaySummaryFooterCaption )
					)
				{

					// MRS 7/12/05 - BR04874
					//
//					this.ExportSummaryCaptionRow(exportHelper, 
//						summaries, defaultSummaryFormatting);
					// MRS 8/28/06 - BR14930
					//
//					this.ExportSummaryCaptionRow(exportHelper, 
//						summaries, defaultSummaryFormatting, row as UltraGridSummaryRow);
					this.ExportSummaryCaptionRow(exportHelper, 
						summaries, summaryFooterCaptionFormatting, row as UltraGridSummaryRow);

					summaryCaptionExported = true;
				}

				// fire SummaryRowExporting
				SummaryRowExportingEventArgs summaryExportingArgs = 
					new SummaryRowExportingEventArgs(exportHelper, summaries, summaryLevel);

				this.OnSummaryRowExporting(summaryExportingArgs);

				// summary is not canceled
				if(!summaryExportingArgs.Cancel)
				{

					exportHelper.extentOfCurrentGridObject.Location = exportHelper.currentPos;

					// MD 1/14/09 - Groups in RowLayout
					// Each row being written out will write out the header cells now, so we don't have to do this anymore.
					//if(band.UseRowLayout && 
					//    band.RowLayoutLabelStyle==RowLayoutLabelStyle.WithCellData )
					//{
					//    this.ExportColumnHeaderCells(exportHelper, row, band, false);
					//
					//    exportHelper.currentPos = exportHelper.extentOfCurrentGridObject.Location;
					//}

					// MRS 11/1/05 - BR07222
					// If this band is using a RowLayout, hide all summaries
					// except those on the current level					
					if (useRowLayout)
					{
                        // MRS 7/5/07 - BR24588
                        //SummaryValue[] summariesOnCurrentLevel = this.GetSummariesForLevel(summaries, firstColumn, summaryLevel);
						// MD 1/14/09 - Groups in RowLayout
						// We are now using the new caching mechanism so we need to pass the collection of layout items instead of the first visible column.
                        //SummaryValue[] summariesOnCurrentLevel = this.GetSummariesForLevel(summaries, firstColumn, summaryLevel, row as UltraGridSummaryRow);
						SummaryValue[] summariesOnCurrentLevel = this.GetSummariesForLevel( summaries, documentBandInfo, summaryLevel, row as UltraGridSummaryRow );
                        
                        foreach (SummaryValue summary in summaries)
						{							
							if (Array.IndexOf(summariesOnCurrentLevel, summary) >= 0)
								summary.SummarySettings.SummaryDisplayArea = (SummaryDisplayAreas)hiddenSummariesTable[summary];
							else							
								summary.SummarySettings.SummaryDisplayArea = SummaryDisplayAreas.None;							
						}							
					}

					// MD 1/14/09 - Groups in RowLayout
					// We are now using the new caching mechanism
					#region Old Code

					

					#endregion Old Code

					int[] logicalRowHeights = UltraGridExcelExporter.GetDimensionsFromAbsolutePositions( documentBandInfo.HeaderRowDims );
					this.SetColumnWidthsAndRowHeights( exportHelper, row, null, logicalRowHeights );

					foreach ( GridExportCache.DocumentHeaderInfo headerInfo in documentBandInfo )
					{
						switch ( headerInfo.DocumentHeaderInfoType )
						{
							case GridExportCache.DocumentHeaderInfoType.Summary:
								{
									// MRS 11/1/05 - BR07222
									// If this band is using a RowLayout, then each row of 
									// the layout will be broken up. So always use a summaryLevel
									// of 0.
									//						SummaryValue summary = summaries.GetSummaryValueFromPosition(
									//							SummaryPosition.UseSummaryPositionColumn, place.Column, summaryLevel,
									//							// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
									//							// Export summaries displayed on top. Pass the summaryRowContext parameter.
									//							// 
									//							row as UltraGridSummaryRow );										
									SummaryValue summary = summaries.GetSummaryValueFromPosition(
										SummaryPosition.UseSummaryPositionColumn, headerInfo.Column, ( useRowLayout ) ? 0 : summaryLevel,
										// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
										// Export summaries displayed on top. Pass the summaryRowContext parameter.
										// 
										row as UltraGridSummaryRow );

									// MRS 7/12/05 - BR04874
									//						this.ExportSummaryCell(exportHelper, summary, summaryLevel, 
									//							defaultSummaryFormatting, place.Relative);
									this.ExportSummaryCell( 
										exportHelper, 
										summary, 
										summaryLevel,
										defaultSummaryFormatting, 
										headerInfo.RelativeRect, 
										row as UltraGridSummaryRow );
								}
								break;

							case GridExportCache.DocumentHeaderInfoType.Header:
								this.ExportHeaderCell( exportHelper, row, headerInfo );
								break;

							case GridExportCache.DocumentHeaderInfoType.Cell:
								Debug.Fail( "This was not expected." );
								break;

							case GridExportCache.DocumentHeaderInfoType.Blank:
								break;

							default:
								Debug.Fail( "Unknown DocumentHeaderInfoType" );
								break;
						}
					}

					this.SetCollapsedRows(
						exportHelper.CurrentWorksheet, 
						exportHelper.extentOfCurrentGridObject.Y, 
						exportHelper.extentOfCurrentGridObject.Bottom-1,
						exportHelper.HierarchyLevel);

					exportHelper.currentPos = exportHelper.extentOfCurrentGridObject.Location;
					this.IncreaseCurrentRow(exportHelper, 
						exportHelper.extentOfCurrentGridObject.Height);

                    // MBS 6/30/09 - TFS18248
                    // See comments above
                    exportHelper.currentPos.X = currentXPos;

					// fire SummaryRowExported
					SummaryRowExportedEventArgs summaryExportedArgs =
						new SummaryRowExportedEventArgs(exportHelper, summaries, summaryLevel);

					this.OnSummaryRowExported(summaryExportedArgs);
				}

				// MRS 11/1/05 - BR07222
				// If this band is using a RowLayout, reset al the hidden 
				// properties back to their original values
				if (useRowLayout)
				{			
					foreach (SummaryValue summary in summaries)
						summary.SummarySettings.SummaryDisplayArea = (SummaryDisplayAreas)hiddenSummariesTable[summary];
				}

				// MRS 11/1/05 - BR07222
				// Took this out. We should not just be exporting the first summary. 
//				// in the case of layout summaries, only first level is exported
//				if(summaryLevel==0 && band.UseRowLayout)
//					break;
			}

			// free-form summaries loop
			SummaryValue leftSV, centerSV, rightSV;

			exportHelper.extentOfCurrentGridObject.Height = 1;

			for(int summaryLevel=0 ; 
				// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
				// Export summaries displayed on top. Pass the summaryRowContext parameter.
				// 
				//FreeformSummariesExist(summaries, summaryLevel, out leftSV, out centerSV, out rightSV) ; 
				FreeformSummariesExist( row as UltraGridSummaryRow, summaries, summaryLevel, out leftSV, out centerSV, out rightSV) ; 
				summaryLevel++)
			{
				// summary caption
				if(!summaryCaptionExported && summaryLevel==0 && summaries.SummaryFooterCaptionVisibleResolved
					// SSP 10/27/06 BR16917
					// 
					&& ( ! ( row is UltraGridSummaryRow ) || ((UltraGridSummaryRow)row).ShouldDisplaySummaryFooterCaption )
					)
				{
					// MRS 7/12/05 - BR04874
					//
					//					this.ExportSummaryCaptionRow(exportHelper, 
					//						summaries, defaultSummaryFormatting);
					// MRS 8/28/06 - BR14930
					//
//					this.ExportSummaryCaptionRow(exportHelper, 
//						summaries, defaultSummaryFormatting, row as UltraGridSummaryRow);
					this.ExportSummaryCaptionRow(exportHelper, 
						summaries, summaryFooterCaptionFormatting, row as UltraGridSummaryRow);

					summaryCaptionExported = true;
				}

				// fire SummaryRowExporting
				SummaryRowExportingEventArgs summaryExportingArgs = 
					new SummaryRowExportingEventArgs(exportHelper, summaries, summaryLevel);

				this.OnSummaryRowExporting(summaryExportingArgs);

				// summary is not canceled
				if(!summaryExportingArgs.Cancel)
				{
					// write free-form summaries

					exportHelper.extentOfCurrentGridObject.Location = exportHelper.currentPos;

					int newWidth = Math.Max(1, exportHelper.extentOfCurrentGridObject.Width/3);

					Rectangle leftRect = new Rectangle(0, 0, newWidth, 1);

					Rectangle centerRect = new Rectangle(newWidth, 0, newWidth, 1);

					Rectangle rightRect = new Rectangle(newWidth*2, 0, 
						exportHelper.extentOfCurrentGridObject.Width - newWidth*2, 1);

					// MRS 7/12/05 - BR04874
//					this.ExportSummaryCell(exportHelper, leftSV, summaryLevel, 
//						defaultSummaryFormatting, leftRect);
					this.ExportSummaryCell(exportHelper, leftSV, summaryLevel, 
						defaultSummaryFormatting, leftRect, row as UltraGridSummaryRow);

					// MRS 7/12/05 - BR04874
//					this.ExportSummaryCell(exportHelper, centerSV, summaryLevel, 
//						defaultSummaryFormatting, centerRect);
					this.ExportSummaryCell(exportHelper, centerSV, summaryLevel, 
						defaultSummaryFormatting, centerRect, row as UltraGridSummaryRow);

					// MRS 7/12/05 - BR04874
//					this.ExportSummaryCell(exportHelper, rightSV, summaryLevel, 
//						defaultSummaryFormatting, rightRect);
					this.ExportSummaryCell(exportHelper, rightSV, summaryLevel, 
						defaultSummaryFormatting, rightRect, row as UltraGridSummaryRow);

					this.SetCollapsedRows(
						exportHelper.CurrentWorksheet, 
						exportHelper.extentOfCurrentGridObject.Y, 
						exportHelper.extentOfCurrentGridObject.Bottom-1,
						exportHelper.HierarchyLevel);

					exportHelper.currentPos = exportHelper.extentOfCurrentGridObject.Location;
					this.IncreaseCurrentRow(exportHelper, 
						exportHelper.extentOfCurrentGridObject.Height);

					// fire SummaryRowExported
					SummaryRowExportedEventArgs summaryExportedArgs =
						new SummaryRowExportedEventArgs(exportHelper, summaries, summaryLevel);

					this.OnSummaryRowExported(summaryExportedArgs);
				}
			}
		}

		private static bool UnsafeGetTextMetrics(IntPtr hdc, out TEXTMETRIC textmetric)
		{
			return GetTextMetrics(hdc, out textmetric);
		}

		// importing GetTextMetrics(...) from Win32 API
		[DllImport("Gdi32", CharSet=CharSet.Auto)]
		// MD 10/31/06 - 64-Bit Support
		[return: MarshalAs( UnmanagedType.Bool )]
		private static extern bool GetTextMetrics(IntPtr hdc, out TEXTMETRIC textmetric);

        // MRS v7.2 - Document Exporter
        // Moved to the GridExportCache.cs file so this code can be shared with
        // the document exporter
        #region Moved to GridExportCache.cs
//        // MRS 9/27/04 - UWG2786
//        #region BuildDataSchema
//#if DEBUG
//        /// <summary>
//        /// Stores the data Schema in a HashTable. This is to prevent multiple calls
//        /// to GetRelatedVisibleColumn in the grid and improve performance.
//        /// </summary>
//#endif
//        private void BuildDataSchema(UltraGridLayout layout)
//        {
//            this.dataSchema.Clear();
//            foreach ( UltraGridBand band in layout.Bands )
//            {
//                // JAS 1/6/05 BR01282 ***START***
//                //
//                // If all the columns in the band are hidden, then we need to skip this iteration.
//                UltraGridColumn firstCol = band.GetFirstVisibleCol(null, true);
//                if( firstCol == null )
//                    continue;
//                firstCol = firstCol.GetRelatedVisibleColumn(VisibleRelation.First);
//                //				UltraGridColumn firstCol = band.GetFirstVisibleCol(null, true).GetRelatedVisibleColumn(VisibleRelation.First);
//                //
//                // JAS 1/6/05 BR01282 ***END***
//                dataSchema.Add(band, firstCol);
//                UltraGridColumn nextCol = firstCol.GetRelatedVisibleColumn(VisibleRelation.Next);
				
//                while ( nextCol != null )
//                {
//                    this.dataSchema.Add(firstCol, nextCol);
//                    firstCol = nextCol;
//                    nextCol = firstCol.GetRelatedVisibleColumn(VisibleRelation.Next);					
//                }			
				
//            }
//        }
//        #endregion BuildDataSchema
        #endregion Moved to GridExportCache.cs

        // MRS 7/12/05 - BR04874
		#region GetGroupByDescendantLevelCount

				private int GetGroupByDescendantLevelCount(UltraGridGroupByRow row)
		{
			if (row == null) return 0;
		
			UltraGridGroupByRow childRow = row.Rows[0] as UltraGridGroupByRow;
			return 1 + this.GetGroupByDescendantLevelCount(childRow);
		}
		#endregion GetGroupByDescendantLevelCount

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        #region MRS NAS v8.3 - Exporting CalcManager Formulas to Excel

        #region ProcessFormulas
        private void ProcessFormulas()
        {
            // The code below which exports the formulas might encounter hidden columns that need
            // to be exported to a hidden worksheet and these hidden columns might have formulas, 
            // which will then be added to these lists. So we need to make multiple passes through 
            // this process until the lists are empty.
            while (this.MappingManager.HasFormulaTargetCells || this.MappingManager.HasFormulaTargetSummaryCells)
            {
                // Get all the cells and summaries that have formulas. 
                CellInfo[] formulaTargetCells = this.MappingManager.GetFormulaTargetCells();
                SummaryInfo[] summaryInfos = this.MappingManager.GetFormulaTargetSummaryCells();

                // Clear the lists, since the formula in these lists will already have been processed
                // the next time we come through the loop. 
                this.mappingManager.ClearFormulaTargetCells();
                this.mappingManager.ClearFormulaTargetSummaryCells();

                foreach (CellInfo formulaTargetCell in formulaTargetCells)
                {
                    object context = formulaTargetCell.GridRow.Cells[formulaTargetCell.GridColumn];
                    string gridFormula = formulaTargetCell.FormulaString;
                    WorksheetCell workSheetCell = this.MappingManager.GetWorksheetCell(formulaTargetCell);

                    ProcessFormula(formulaTargetCell, context, gridFormula, workSheetCell);
                }

                foreach (SummaryInfo summaryInfo in summaryInfos)
                {
                    object context = summaryInfo.SummaryValue;
                    string gridFormula = summaryInfo.FormulaString;
                    WorksheetCell workSheetCell = this.MappingManager.GetWorksheetCell(summaryInfo);

                    this.ProcessFormula(summaryInfo, context, gridFormula, workSheetCell);
                }
            }

            this.mappingManager = null;
        }

        #endregion //ProcessFormulas

        #region ProcessFormula
        private void ProcessFormula(FormulaTargetInfo formulaTargetInfo, object context, string gridFormula, WorksheetCell workSheetCell)
        {
            if (this.MappingManager.GetIsFormulaCancelled(formulaTargetInfo))
                return;

            FormulaExportingEventArgs formulaExportingEventArgs = this.OnFormulaExporting(
                context,
                gridFormula,
                workSheetCell,
                FormulaExportAction.ExportFormula);

            switch (formulaExportingEventArgs.Action)
            {
                case FormulaExportAction.ExportFormula:
                    string convertedFormula = null;
                    try
                    {
                        // Attempt to convert the formula. 
                        convertedFormula = this.ConvertGridFormulaToExcel(formulaTargetInfo, workSheetCell);
                                               
                        // If the converted formula is null, it probably means that the formula was
                        // something like "DBNull" or "Null". In this case, we can't set a formula
                        // on the WorksheetCell. So don't fire the FormulaExported event. This is 
                        // like an implicit cancel. 
                        if (convertedFormula == null)
                            return;

                        // Attempt to apply the formula. 
                        UltraGridExcelExporter.ApplyFormula(workSheetCell, convertedFormula);
                    }
                    catch (Exception ex)
                    {
                        FormulaExportException formulaExportException = ex as FormulaExportException;
                        if (formulaExportException != null)
                            convertedFormula = formulaExportException.InvalidFormula;                        

                        // If the formula failed to convert or apply, raise the FormulaExportError
                        // event. 
                        FormulaExportErrorEventArgs formulaExportErrorEventArgs = this.OnFormulaExportError(
                            context, 
                            gridFormula, 
                            workSheetCell,
                            FormulaExportErrorAction.WriteErrorMessageToWorksheetCell, 
                            ex,
                            convertedFormula);

                        // Use the converted formula from the event args - the user may 
                        // have modified it. 
                        convertedFormula = formulaExportErrorEventArgs.ExcelFormula;
                        
                        if (formulaExportErrorEventArgs.ApplyInvalidFormulaIfPossible)
                        {
                            // The user wants to try to apply the formula again, so let's try 
                            // it.
                            bool applySucceeded = true;
                            try
                            {
                                UltraGridExcelExporter.ApplyFormula(workSheetCell, convertedFormula);                                                                
                            }
                            catch 
                            { 
                                applySucceeded = false; 
                            }

                            // If no exception was raised, assume that the formula applied okay. 
                            // Raise the FormulaExported event and return;
                            if (applySucceeded)
                            {
                                this.OnFormulaExported(context, gridFormula, workSheetCell);
                                return;
                            }
                        }

                        // If we got here, either the user chose not to try to export the formula
                        // or else they tried to apply it and it failed. 
                        switch (formulaExportErrorEventArgs.Action)
                        {
                            case FormulaExportErrorAction.WriteErrorMessageToWorksheetCell:
                                workSheetCell.Value = formulaExportErrorEventArgs.ErrorText;
                                break;
                            case FormulaExportErrorAction.CancelOne:
                                break;
                            case FormulaExportErrorAction.CancelAll:
                                this.MappingManager.SetIsFormulaCancelled(formulaTargetInfo);
                                break;
                            default:
                                Debug.Fail("Unknown FormulaExportErrorAction");
                                break;
                        }                            

                        return;
                    }

                    this.OnFormulaExported(context, gridFormula, workSheetCell);

                    break;
                case FormulaExportAction.CancelOne:
                    break;
                case FormulaExportAction.CancelAll:
                    this.MappingManager.SetIsFormulaCancelled(formulaTargetInfo);                    
                    break;
                default:
                    Debug.Fail("Unknown FormulaExportAction");
                    break;
            }
        }
        #endregion //ProcessFormula

        #region ConvertGridFormulaToExcel
        private string ConvertGridFormulaToExcel(FormulaTargetInfo formulaTarget, WorksheetCell targetWorksheetCell)
        {
            CellInfo cellInfo = formulaTarget as CellInfo;
            if (cellInfo != null)
                return this.ConvertGridFormulaToExcel(cellInfo, targetWorksheetCell);

            SummaryInfo summaryInfo = formulaTarget as SummaryInfo;
            if (summaryInfo != null)
                return this.ConvertGridFormulaToExcel(summaryInfo, targetWorksheetCell);

            return null;
        }
        private string ConvertGridFormulaToExcel(CellInfo formulaTargetCell, WorksheetCell targetWorksheetCell)
        {
            // Get the real grid column from the export column. 
            UltraGridColumn realGridColumn = this.MappingManager.GetRealGridColumn(formulaTargetCell.GridColumn);
            // Get the column reference
            IUltraCalcReference columnReference = UltraGridBase.GetReferenceFromContext(realGridColumn);
            // Get the formula from the column reference. 
            IUltraCalcFormula formula = columnReference.Formula;

            // The formula is on the column, but we need to resolve it based on the cell. So
            // get the export layout cell. 
            UltraGridCell exportGridCell = formulaTargetCell.GridRow.Cells[formulaTargetCell.GridColumn];            
            // Now get the real grid cell from the export layou cell. 
            UltraGridCell realGridCell = this.MappingManager.GetRealGridCell(exportGridCell);
            
            // Use the real grid cell to get the cell reference. The formula must be 
            // evaluated based on the real reference of the real grid cell. The export
            // cell reference won't work. 
            IUltraCalcReference cellReference = formulaTargetCell.GridColumn.GetReferenceFromContext(realGridCell);

            return this.ConvertGridFormulaToExcel(targetWorksheetCell, formula, cellReference);
        }

        private string ConvertGridFormulaToExcel(SummaryInfo summaryInfo, WorksheetCell targetWorksheetCell)
        {
            SummaryValue summaryValue = summaryInfo.SummaryValue;

            // Get the real SummaryValue from the actual grid. The export SummaryValue will
            // not work here. 
            SummaryValue realGridSummaryValue = this.MappingManager.GetRealGridSummaryValue(summaryValue);
            
            // Get the SummarySettings - that's where the formula is. 
            SummarySettings realGridSummarySettings = realGridSummaryValue.SummarySettings;
            IUltraCalcReference summarySettingsReference = UltraGridBase.GetReferenceFromContext(realGridSummarySettings);
            IUltraCalcFormula formula = summarySettingsReference.Formula;

            // Get the SummaryValue reference. The formula needs to be evaluated against
            // this reference, not the Summary Settings and not the summaryValue in the export
            // layout. 
            IUltraCalcReference summaryReference = UltraGridBase.GetReferenceFromContext(realGridSummaryValue);

            return this.ConvertGridFormulaToExcel(targetWorksheetCell, formula, summaryReference);
        }        

        private string ConvertGridFormulaToExcel(WorksheetCell targetWorksheetCell, IUltraCalcFormula formula, IUltraCalcReference targetReference)
        {
            Exception ex = null;
            IUltraCalcFormulaToken[] tokens = this.MappingManager.GetFormulaTokens(formula);

            Stack<ArgumentInfo> argumentStack = new Stack<ArgumentInfo>();
            ArgumentInfo argumentInfo;

            IUltraCalcValueToken valueToken;
            IUltraCalcFunctionToken functionToken;

            foreach (IUltraCalcFormulaToken token in tokens)
            {
                switch (token.Type)
                {
                    case UltraCalcFormulaTokenType.Value:
                        valueToken = token as IUltraCalcValueToken;
                        if (valueToken == null)
                        {
                            Debug.Fail("Invalid token");
                            return null;
                        }

                        UltraCalcValue calcValue = valueToken.Value;

                        if (calcValue.IsReference)
                            calcValue = new UltraCalcValue(targetReference.ResolveReference(calcValue.ToReference(), ResolveReferenceType.RightHandSide));

                        argumentInfo = new ArgumentInfo(this.GetArgumentString(calcValue, targetWorksheetCell), null);
                        argumentStack.Push(argumentInfo);

                        break;
                    case UltraCalcFormulaTokenType.Function:
                        functionToken = token as IUltraCalcFunctionToken;
                        if (functionToken == null)
                        {
                            Debug.Fail("Invalid token");
                            return null;
                        }

                        if (ex == null)
                        {
                            bool isValidFunctionName = UltraGridExcelExporter.IsValidFunctionName(this.currentWorkbook, functionToken);
                            if (false == isValidFunctionName)
                            {
                                ex = new FormulaExportException(
                                    FormulaExportErrorType.UnrecognizedFunction,
                                    SR.GetString("FormulaError_UnrecognizedFunction", functionToken.FunctionName), 
                                    null);
                            }
                        }

                        string functionString = this.BuildFunctionString(functionToken, argumentStack, targetWorksheetCell, out ex);                       
                        argumentInfo = new ArgumentInfo(functionString, functionToken.FunctionOperator);
                        argumentStack.Push(argumentInfo);
                        break;
                    default:
                        Debug.Fail("Unknown Token Type");
                        return null;
                }
            }

            // If there is no function, we will end up here one (and only one) arg.
            if (argumentStack.Count != 1)
            {
                Debug.Fail("There is more than one argument left and no function; unexpected");
                return null;                
            }

            argumentInfo = argumentStack.Pop();
            string text = argumentInfo.Text;
            if (text == null ||
                text.Length == 0 ||
                text == "DBNULL()" ||
                text == "NULL()")
            {
                return null;
            }

            string formulaString = string.Format("={0}", argumentInfo.Text);
            
            if (ex == null)
                return formulaString;

            FormulaExportException formulaExportException = ex as FormulaExportException;
            if (formulaExportException != null)
                formulaExportException.InvalidFormula = formulaString;

            throw ex;            
        }        
        
        #endregion //ConvertGridFormulaToExcel

        #region BuildFunctionString
        private string BuildFunctionString(IUltraCalcFunctionToken functionToken, Stack<ArgumentInfo> argumentStack, WorksheetCell targetWorksheetCell, out Exception ex)
        {
            ex = null;

            int argumentCount = functionToken.ArgumentCount;
            if (argumentStack.Count < argumentCount)
            {
                Debug.Fail("There are not enough arguments on the stack; unexpected");
                return "#REF!";
            }

            string functionName = functionToken.FunctionName.ToUpper();

            ArgumentInfo argumentInfo;
            string argText;
            UltraCalcOperatorFunction? functionOperator = functionToken.FunctionOperator;

            UltraGridExcelExporter.CoerceFunction(argumentStack, ref argumentCount, ref functionName, ref functionOperator);
                        
            switch (functionOperator)
            {
                case UltraCalcOperatorFunction.Add:
                case UltraCalcOperatorFunction.Concatenate:
                case UltraCalcOperatorFunction.Divide:
                case UltraCalcOperatorFunction.Equal:                
                case UltraCalcOperatorFunction.GreaterThan:
                case UltraCalcOperatorFunction.GreaterThanOrEqual:
                case UltraCalcOperatorFunction.LessThan:
                case UltraCalcOperatorFunction.LessThanOrEqual:
                case UltraCalcOperatorFunction.Multiply:
                case UltraCalcOperatorFunction.NotEqual:                
                case UltraCalcOperatorFunction.Subtract:
                case UltraCalcOperatorFunction.Exponent:

                    ArgumentInfo argumentInfo2 = argumentStack.Pop();
                    ArgumentInfo argumentInfo1 = argumentStack.Pop();
                    // MRS 10/8/2008 - TFS8760
                    //string arg2 = argumentInfo2.GetArgumentText(functionOperator);
                    //string arg1 = argumentInfo1.GetArgumentText(functionOperator);
                    string arg2 = argumentInfo2.GetArgumentText(functionOperator, 2);
                    string arg1 = argumentInfo1.GetArgumentText(functionOperator, 1);

                    return string.Format("{0} {1} {2}", arg1, functionName, arg2);
                case UltraCalcOperatorFunction.UnaryMinus:
                case UltraCalcOperatorFunction.UnaryPlus:
                    argumentInfo = argumentStack.Pop();
                    argText = argumentInfo.GetArgumentText(functionOperator);
                    functionName = functionName.Substring(0, 1);
                    return string.Format("{0}{1}", functionName, argText);
                case UltraCalcOperatorFunction.Percent:
                    argumentInfo = argumentStack.Pop();
                    argText = argumentInfo.GetArgumentText(functionOperator);
                    return string.Format("{0}{1}", argText, functionName);
                default:                   
                                        
                    string coercedFunctionString;                    
                    bool functionWasCoerced = this.CoerceFunction(argumentStack, functionToken, out coercedFunctionString, out ex);

                    if (functionWasCoerced)
                        return coercedFunctionString;

                    return BuildFunctionString(argumentStack, argumentCount, functionName, functionOperator);
            }
        }

        private static string BuildFunctionString(Stack<ArgumentInfo> argumentStack, IUltraCalcFunctionToken functionToken)
        {
            int argumentCount = functionToken.ArgumentCount;
            string functionName = functionToken.FunctionName.ToUpper();
            UltraCalcOperatorFunction? functionOperator = functionToken.FunctionOperator;

            return BuildFunctionString(argumentStack, argumentCount, functionName, functionOperator);
        }

        private static string BuildFunctionString(Stack<ArgumentInfo> argumentStack, int argumentCount, string functionName, UltraCalcOperatorFunction? functionOperator)
        {
            ArgumentInfo argumentInfo;
            string argText;

            string argList = null;
            for (int i = 0; i < argumentCount; i++)
            {
                if (argList != null)
                    argList = string.Format(", {0}", argList);

                argumentInfo = argumentStack.Pop();                
                argText = argumentInfo.GetArgumentText(functionOperator);
                argList = string.Format("{0}{1}", argText, argList);
            }

            return string.Format("{0}({1})", functionName, argList);
        }        

        #endregion //BuildFunctionString

        #region GetArgumentString
        private string GetArgumentString(UltraCalcValue calcValue, WorksheetCell targetWorksheetCell)
        {
            if (calcValue.IsReference)
            {
                if (calcValue.IsError)
                {
                    UltraCalcErrorValue ultraCalcErrorValue = calcValue.ToErrorValue();
                    if (ultraCalcErrorValue.Code == UltraCalcErrorCode.Reference)
                        return "#REF!";
                }

                IUltraCalcReference reference = calcValue.ToReference();                
                UltraGridCell cellContext = reference.Context as UltraGridCell;
                if (cellContext != null)
                {
                    WorksheetCell worksheetCell = this.MappingManager.GetWorksheetCell(cellContext);
                    bool includeWorksheetName = worksheetCell.Worksheet != targetWorksheetCell.Worksheet;
                    return worksheetCell.ToString(CellReferenceMode.A1, includeWorksheetName, true, true);
                }

                UltraGridColumn columnContext = reference.Context as UltraGridColumn;
                if (columnContext != null)
                {
                    if (reference.IsEnumerable == false)
                    {
                        Debug.Fail("Reference has a context of a column but is not enumerable; unexpected");
                        return null;
                    }                    

                    WorksheetCellRangeInfo worksheetCellRangeInfo = this.MappingManager.GetWorksheetCells(reference);
                    WorksheetRegion worksheetRegion = worksheetCellRangeInfo.GetWorksheetRegion();

                    // This could be null if there are no cells (if all of the rows were cancelled, 
                    // for example). 
                    if (worksheetRegion != null)
                    {
                        bool includeWorksheetName = worksheetCellRangeInfo.Worksheet != targetWorksheetCell.Worksheet;
                        return worksheetRegion.ToString(CellReferenceMode.A1, includeWorksheetName, true, true);
                    }
                    else
                        return "#REF!";
                }

                // MRS 5/21/2009 - TFS17817
                SummaryValue summaryValueContext = reference.Context as SummaryValue;
                if (summaryValueContext != null)
                {   
                    WorksheetCell worksheetCell = this.MappingManager.GetWorksheetCell(summaryValueContext);
                    bool includeWorksheetName = worksheetCell.Worksheet != targetWorksheetCell.Worksheet;
                    return worksheetCell.ToString(CellReferenceMode.A1, includeWorksheetName, true, true);
                }

                Debug.Fail(string.Format("Unknown reference context type: {0}", reference.Context.GetType().ToString()));
                return null;
            }

            if (calcValue.Value is string)
                return string.Format("\"{0}\"", calcValue.ToString());

            return calcValue.ToString();
        }
        #endregion //GetArgumentString

        #region ApplyFormula
        private static void ApplyFormula(WorksheetCell workSheetCell, string convertedFormula)
        {
            if (convertedFormula != null && convertedFormula.Length > 0)
            {
                Formula formula = Formula.Parse(convertedFormula, CellReferenceMode.A1);
                formula.ApplyTo(workSheetCell);
            }
        }
        #endregion //ApplyFormula

        #region CoerceFunction
        private static void CoerceFunction(Stack<ArgumentInfo> argumentStack, ref int argumentCount, ref string functionName, ref UltraCalcOperatorFunction? functionOperator)
        {
            switch (functionName)
            {
                // In CalcManager, ceiling and floor have one required argument and one 
                // optional argument. Excel requires both args. So if there is only one 
                // argument specified, fill in the second argument with a one. 
                case "CEILING":
                case "FLOOR":
                    if (argumentCount == 1)
                    {
                        argumentCount = 2;
                        argumentStack.Push(new ArgumentInfo("1", null));
                    }
                    break;

                // The second and third aguments (start and num_chars) are not optional in Excel.
                case "MID":
                    if (argumentCount == 1)
                    {
                        argumentCount = 3;
                        argumentStack.Push(new ArgumentInfo("1", null));
                        argumentStack.Push(new ArgumentInfo("0", null));
                    }
                    else if (argumentCount == 2)
                    {
                        argumentCount = 3;
                        argumentStack.Push(new ArgumentInfo("0", null));
                    }
                    break;
                // Excel doesn't have an option for arithmetic vs. banker's rounding. 
                // So strip off the third argument, if it was specified. 
                case "ROUND":                    
                    if (argumentCount == 3)
                    {
                        argumentCount = 2;
                        argumentStack.Pop();
                    }
                    break;
                case "ERRORTYPE":
                    functionName = "ERROR.TYPE";
                    break;
                case "ISDBNULL":
                case "ISNULL":
                    functionName = "ISBLANK";
                    break;                
            }
        }

        private bool CoerceFunction(Stack<ArgumentInfo> argumentStack, IUltraCalcFunctionToken functionToken, out string functionString, out Exception ex)
        {
            ArgumentInfo intervalInfo;
            string interval;
            string functionName = functionToken.FunctionName.ToUpper();
            ex = null;

            switch (functionName)
            {
                case "DATEADD":
                    ArgumentInfo dateInfo = argumentStack.Pop();
                    ArgumentInfo numberInfo = argumentStack.Pop();
                    intervalInfo = argumentStack.Pop();

                    interval = intervalInfo.Text.Replace("\"", string.Empty).Replace("\'", string.Empty).ToLower();
                    string[] intervals = new string[] { dateInfo.ToString(), string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty };

                    switch (interval)
                    {
                        case "yyyy": // year
                            intervals[1] = string.Format(" + {0}", numberInfo.Text);
                            break;
                        case "m": // month
                            intervals[2] = string.Format(" + {0}", numberInfo.Text);
                            break;
                        case "q": // quarter = 3 months
                            intervals[2] = string.Format(" + ({0} * 3)", numberInfo.Text);
                            break;
                        case "d": // day
                        case "y": // day (not year)
                            intervals[3] = string.Format(" + {0}", numberInfo.Text);
                            break;
                        case "w": // week
                        case "ww": // calendar week (same as week)
                            intervals[3] = string.Format(" + ({0} * 7)", numberInfo.Text);
                            break;
                        case "h": // hours
                            intervals[4] = string.Format(" + {0}", numberInfo.Text);
                            break;
                        case "n": // minute
                            intervals[5] = string.Format(" + {0}", numberInfo.Text);
                            break;
                        case "s": // second
                            intervals[6] = string.Format(" + {0}", numberInfo.Text);
                            break;
                        default:
                            functionString = null;                            
                            return true;
                    }

                    functionString = string.Format("DATE(YEAR({0}){1},MONTH({0}){2},DAY({0}){3})+TIME(HOUR({0}){4},MINUTE({0}){5},SECOND({0}){6})", intervals);
                    return true;

                case "DATEDIFF":
                    ArgumentInfo dateInfo1 = argumentStack.Pop();
                    ArgumentInfo dateInfo2 = argumentStack.Pop();
                    intervalInfo = argumentStack.Pop();
                    interval = intervalInfo.Text.Replace("\"", string.Empty).Replace("\'", string.Empty).ToLower();
                    string multiplier;

                    switch (interval)
                    {
                        case "d": // day
                        case "y": // day (not year)
                            multiplier = string.Empty;
                            break;
                        case "w": // week
                        case "ww": // calendar week (same as week)                             
                            multiplier = this.MappingManager.GetNamedReference(NamedReferenceConstant.DaysPerWeek).Name;
                            break;
                        case "h": // hours
                            multiplier = this.MappingManager.GetNamedReference(NamedReferenceConstant.HoursPerDay).Name;
                            break;
                        case "n": // minute
                            multiplier = this.MappingManager.GetNamedReference(NamedReferenceConstant.MinutesPerDay).Name;
                            break;
                        case "s": // second                            
                            multiplier = this.MappingManager.GetNamedReference(NamedReferenceConstant.SecondsPerDay).Name;
                            break;
                        case "yyyy": // year                                   
                        case "m": // month                                    
                        case "q": // quarter = 3 months
                        default:

                            argumentStack.Push(intervalInfo);                            
                            argumentStack.Push(dateInfo2);
                            argumentStack.Push(dateInfo1);
                            
                            functionString = UltraGridExcelExporter.BuildFunctionString(argumentStack, functionToken);

                            ex = new FormulaExportException(
                                FormulaExportErrorType.UnsupportedFunction,                                
                                SR.GetString("FormulaError_UnsupportedFunction_DateDiff", interval), 
                                functionString);

                            return true;
                    }

                    string multiplierText = multiplier == null || multiplier.Length == 0 
                        ? string.Empty 
                        : string.Format(" * {0}", multiplier.ToString());

                    functionString = string.Format("({0} - {1}){2}", dateInfo1.Text, dateInfo2.Text, multiplierText);
                    return true;
            }

            functionString = null;
            return false;
        }
        #endregion //CoerceFunction

        #region IsValidFunctionName
        private static bool IsValidFunctionName(Workbook workbook, IUltraCalcFunctionToken functionToken)
        {
            // If it's a known operator, return true. 
            if (functionToken.FunctionOperator != null)
                return true;

            // Check if the name is valid. 
            string functionName = functionToken.FunctionName;
            if (workbook.IsValidFunctionName(functionName))
                return true;

            // Check the coercable function names
            switch (functionName.ToUpper())
            {
                case "DATEADD":
                case "DATEDIFF":
                    return true;
            }
            return false;
        }
        #endregion //IsValidFunctionName        

        #endregion //MRS NAS v8.3 - Exporting CalcManager Formulas to Excel

        #endregion Private Helper Methods

		// MD 1/14/09 - Groups in RowLayout
		// These classes are no logner needed
		#region Not Used

		

		#endregion Not Used

		#region Events

		/// <summary>
		/// Occurs before grid export starts.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Layout</i> argument returns a reference to a cloned UltraGridLayout used for excel exporting. Any changes you make to this layout will be reflected in exported file and not in the original grid layout.</p>
		/// <p class="body">The <i>Rows</i> argument returns a reference to a original rows collection in top band.</p>
		/// <p class="body">Additionaly this event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired before all other events. You can use it to do any preprocessing before exporting process starts. For example, you can set properties on cloned layout or write some custom header to Excel workbook.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_BeginExport")]		
		public event BeginExportEventHandler BeginExport;

		/// <summary>
		/// Occurs when a row is initialized.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Row</i> argument returns a reference to a grid row which is being exported.</p>
		/// <p class="body">The <i>SkipRow</i> argument specifies whether to skip the current row. This argument doesn't affect exporting of descendant rows.</p>
		/// <p class="body">The <i>SkipDescendants</i> argument specifies whether to skip the descendats of the current row.</p>
		/// <p class="body">The <i>SkipSiblings</i> argument specifies whether to skip sibling rows of the current row.</p>
		/// <p class="body">The <i>TerminateExport</i> argument specifies whether to terminate the export process. Current row will not be processed.</p>
		/// <p class="body">Additionaly this event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired for every grid row which is being exported, and before <see cref="RowExporting"/> and <see cref="RowExported"/> are fired. Use this event to set grid row specific properties and to control exporting process.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_InitializeRow")]		
		public event InitializeRowEventHandler InitializeRow;

		/// <summary>
		/// Occurs before grid row is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>GridRow</i> argument returns a reference to row being exported.</p>
		/// <p class="body">Additionaly this event has Cancel, Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportCancelEventArgs"/>.</p>
		/// <p class="body">This event is fired before excel row with grid data is processed. Use Cancel argument to cancel row exporting. If grid rows spans multiple Excel rows (like in row layout mode), this event is fired only for first row.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_RowExporting")]
		public event RowExportingEventHandler RowExporting;

		/// <summary>
		/// Occurs after grid row is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>GridRow</i> argument returns a reference to row being exported.</p>
		/// <p class="body">Additionaly this event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired after excel row with grid data is processed. Use it to apply any additional formatting to excel row. If grid rows spans multiple Excel rows (like in row layout mode), this event is fired only for last row.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_RowExported")]
		public event RowExportedEventHandler RowExported;

		/// <summary>
		/// Occurs before summary row is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Summaries</i> argument returns a reference to summary values.</p>
		/// <p class="body">The <i>SummaryLevel</i> argument returns a current summary level.</p>
		/// <p class="body">Additionaly this event has Cancel, Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportCancelEventArgs"/>.</p>
		/// <p class="body">This event is fired before excel row with summary values for specific level is processed. Use Cancel argument to cancel row exporting. If summaries span multiple Excel rows (like in row layout mode), this event is fired only for first row of summaries.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_SummaryRowExporting")]
		public event SummaryRowExportingEventHandler SummaryRowExporting;

		/// <summary>
		/// Occurs after summary row is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Summaries</i> argument returns a reference to summary values.</p>
		/// <p class="body">The <i>SummaryLevel</i> argument returns a current summary level.</p>
		/// <p class="body">Additionaly this event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired after excel row with summary values for specific level is processed. Use it to apply any additional formatting to excel row. If summaries span multiple Excel rows (like in row layout mode), this event is fired only for last row of summaries.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_SummaryRowExported")]
		public event SummaryRowExportedEventHandler SummaryRowExported;

		/// <summary>
		/// Occurs before header row is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Band</i> argument returns a reference to UltraGridBand associated with headers.</p>
		/// <p class="body">The <i>GridRow</i> argument returns a reference to UltraGridRow associated with headers.</p>
		/// <p class="body">The <i>HeaderType</i> argument returns type of header.</p>
		/// <p class="body">Additionaly this event has Cancel, Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportCancelEventArgs"/>.</p>
		/// <p class="body">This event is fired before excel row with header values is processed. Use Cancel argument to cancel row exporting. If headers span multiple Excel rows (like in row layout mode), this event is fired only for first header row.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_HeaderRowExporting")]
		public event HeaderRowExportingEventHandler HeaderRowExporting;

		/// <summary>
		/// Occurs after header row is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Band</i> argument returns a reference to UltraGridBand associated with headers.</p>
		/// <p class="body">The <i>GridRow</i> argument returns a reference to UltraGridRow associated with headers.</p>
		/// <p class="body">The <i>HeaderType</i> argument returns type of header.</p>
		/// <p class="body">Additionaly this event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired after excel row with header values is processed. Use it to apply any additional formatting to excel row. If headers span multiple Excel rows (like in row layout mode), this event is fired only for last header row.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_HeaderRowExported")]
		public event HeaderRowExportedEventHandler HeaderRowExported;

		/// <summary>
		/// Occurs before grid cell is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>GridRow</i> argument returns a reference to a grid row containing the cell.</p>
		/// <p class="body">The <i>GridColumn</i> argument returns a reference to associated grid column.</p>
		/// <p class="body">The <i>Value</i> argument returns a grid cell value.</p>
		/// <p class="body">Additionaly this event has Cancel, Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportCancelEventArgs"/>.</p>
		/// <p class="body">This event is fired before excel cell with grid data is processed. Use Cancel argument to cancel cell exporting.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_CellExporting")]
		public event CellExportingEventHandler CellExporting;

		/// <summary>
		/// Occurs after grid cell is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>GridRow</i> argument returns a reference to a grid row containing the cell.</p>
		/// <p class="body">The <i>GridColumn</i> argument returns a reference to associated grid column.</p>
		/// <p class="body">The <i>Value</i> argument returns a grid cell value.</p>
		/// <p class="body">Additionaly this event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired after excel cell with grid data is processed. Use it to apply any additional formatting to excel cell.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_CellExported")]
		public event CellExportedEventHandler CellExported;

		/// <summary>
		/// Occurs before header cell is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>GridHeader</i> argument returns a reference to grid header.</p>
		/// <p class="body">The <i>GridRow</i> argument returns a reference to associated grid row.</p>
		/// <p class="body">The <i>HeaderType</i> argument returns header type.</p>
		/// <p class="body">Additionaly this event has Cancel, Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportCancelEventArgs"/>.</p>
		/// <p class="body">This event is fired before excel cell with header value is processed. Use Cancel argument to cancel cell exporting.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_HeaderCellExporting")]
		public event HeaderCellExportingEventHandler HeaderCellExporting;

		/// <summary>
		/// Occurs after header cell is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>GridHeader</i> argument returns a reference to grid header.</p>
		/// <p class="body">The <i>GridRow</i> argument returns a reference to associated grid row.</p>
		/// <p class="body">The <i>HeaderType</i> argument returns header type.</p>
		/// <p class="body">Additionaly this event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired after excel cell with header value is processed. Use it to apply any additional formatting to excel cell.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_HeaderCellExported")]
		public event HeaderCellExportedEventHandler HeaderCellExported;

		/// <summary>
		/// Occurs before summary cell is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Summary</i> argument returns a reference to a summary value.</p>
		/// <p class="body">The <i>SummaryLevel</i> argument returns current summary level.</p>
		/// <p class="body">Additionaly this event has Cancel, Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportCancelEventArgs"/>.</p>
		/// <p class="body">This event is fired before excel cell with summary value is processed. Use Cancel argument to cancel cell exporting.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_SummaryCellExporting")]
		public event SummaryCellExportingEventHandler SummaryCellExporting;

		/// <summary>
		/// Occurs after summary cell is exported to excel.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Summary</i> argument returns a reference to a summary value.</p>
		/// <p class="body">The <i>SummaryLevel</i> argument returns current summary level.</p>
		/// <p class="body">Additionaly this event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired after excel cell with summary value is processed. Use it to apply any additional formatting to excel cell.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_SummaryCellExported")]
		public event SummaryCellExportedEventHandler SummaryCellExported;

		/// <summary>
		/// Occurs after grid export is finished.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>Canceled</i> argument is true if exporting process was been canceled.</p>
		/// <p class="body">This event has Workbook, CurrentWorksheet, CurrentRowIndex, CurrentColumnIndex, CurrentOutlineLevel arguments inherited from <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.ExcelExportEventArgs"/>.</p>
		/// <p class="body">This event is fired after exporting process is finished and is fired after all other events. Use this event for any post processing of excel workbook.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_EndExport")]
		public event EndExportEventHandler EndExport;

		/// <summary>
		/// Occurs when a grid column is initialized. 
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>FrameworkFormatStr</i> argument has value only if column has format string assigned. Value represents format string in .NET Framework format.</p>
		/// <p class="body">If needed, the <i>ExcelFormatStr</i> argument should be set to excel specific format string. If not set, cells in column will have no specific format.</p>
		/// <p class="body">This event is fired when a grid column is initialized. You should use this event to convert format strings from .NET Framework format to Excel format. See .NET Framework documentation and Excel help file for more information about differences in format strings.</p>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGridExcelExporter_E_InitializeColumn")]
		public event InitializeColumnEventHandler InitializeColumn;

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        #region NAS v8.3 - Exporting CalcManager Formulas to Excel

        /// <summary>
        /// Fires before exporting a formula to a worksheet cell.
        /// </summary>
        /// <remarks>
        /// <para class="body">The <see cref="FormulaEventArgs.Context"/> property will return the object in the grid whose formula is being exported. This will be either an <see cref="Infragistics.Win.UltraWinGrid.UltraGridCell"/> or a <see cref="Infragistics.Win.UltraWinGrid.SummaryValue"/>.</para>
        /// <para class="body">The <see cref="FormulaEventArgs.GridFormula"/> property will return the formula applied to the object in the grid.</para>
        /// <para class="body">This event will fire after the <see cref="UltraGridExcelExporter"/> has attempted to convert a grid formula into an equivalent Excel formula and is export to apply the formula (or an error message if the conversion failed) to the worksheet cell.</para>
        /// </remarks>
        [LocalizedDescription("LDR_UltraGridExcelExporter_E_FormulaExporting")]
        public event EventHandler<FormulaExportingEventArgs> FormulaExporting;

        /// <summary>
        /// Fires after exporting a formula (or an error message if the conversion from a grid formula to an Excel formula failed) to a worksheet cell.
        /// </summary>
        /// <remarks>
        /// <para class="body">The <see cref="FormulaEventArgs.Context"/> property will return the object in the grid whose formula is being exported. This will be either an <see cref="Infragistics.Win.UltraWinGrid.UltraGridCell"/> or a <see cref="Infragistics.Win.UltraWinGrid.SummaryValue"/>.</para>
        /// <para class="body">The <see cref="FormulaEventArgs.GridFormula"/> property will return the formula applied to the object in the grid.</para>
        /// </remarks>
        [LocalizedDescription("LDR_UltraGridExcelExporter_E_FormulaExported")]
        public event EventHandler<FormulaExportedEventArgs> FormulaExported;

        /// <summary>
        /// Fires after exporting a formula (or an error message if the conversion from a grid formula to an Excel formula failed) to a worksheet cell.
        /// </summary>
        /// <remarks>
        /// <para class="body">The <see cref="FormulaEventArgs.Context"/> property will return the object in the grid whose formula is being exported. This will be either an <see cref="Infragistics.Win.UltraWinGrid.UltraGridCell"/> or a <see cref="Infragistics.Win.UltraWinGrid.SummaryValue"/>.</para>
        /// <para class="body">The <see cref="FormulaEventArgs.GridFormula"/> property will return the formula applied to the object in the grid.</para>
        /// <para class="body">The <see cref="FormulaExportErrorEventArgs.Error"/> property will indicate the type of error that occurred and the <see cref="FormulaExportErrorEventArgs.ErrorText"/> provides details of the error.</para>
        /// </remarks>
        [LocalizedDescription("LDR_UltraGridExcelExporter_E_FormulaExportError")]
        public event EventHandler<FormulaExportErrorEventArgs> FormulaExportError;

        /// <summary>
        /// Occurs when a summaryis initialized. 
        /// </summary>
        /// <remarks>
        /// <p class="body">The <i>FrameworkFormatStr</i> argument has value only if summary has a DisplayFormat string assigned. This represents the format string in the .NET Framework format.</p>
        /// <p class="body">If needed, the <i>ExcelFormatStr</i> argument should be set to an Excel-specific format string. If not set, exported summaries will have no specific format.</p>
        /// <p class="body">This event is fired when a SummarySettings is initialized. You should use this event to convert format strings from .NET Framework format to Excel format. See .NET Framework documentation and Excel help file for more information about differences in format strings.</p>
        /// </remarks>
        [LocalizedDescription("LDR_UltraGridExcelExporter_E_InitializeSummary")]
        public event EventHandler<InitializeSummaryEventArgs> InitializeSummary;

        #endregion //NAS v8.3 - Exporting CalcManager Formulas to Excel

		#endregion Events

		#region OnEvent Methods

		/// <summary>
		/// Called before grid export starts.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnBeginExport(BeginExportEventArgs e)
		{
			if(this.BeginExport!=null)
				this.BeginExport(this, e);
		}

		/// <summary>
		/// Called when a row is initialized.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializeRow(ExcelExportInitializeRowEventArgs e)
		{
			if(this.InitializeRow!=null)
				this.InitializeRow(this, e);
		}

		/// <summary>
		/// Called before grid row is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnRowExporting(RowExportingEventArgs e)
		{
			if(this.RowExporting!=null)
				this.RowExporting(this, e);
		}

		/// <summary>
		/// Called after grid row is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnRowExported(RowExportedEventArgs e)
		{
			if(this.RowExported!=null)
				this.RowExported(this, e);
		}

		/// <summary>
		/// Called before summary row is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnSummaryRowExporting(SummaryRowExportingEventArgs e)
		{
			if(this.SummaryRowExporting!=null)
				this.SummaryRowExporting(this, e);
		}

		/// <summary>
		/// Called after summary row is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnSummaryRowExported(SummaryRowExportedEventArgs e)
		{
			if(this.SummaryRowExported!=null)
				this.SummaryRowExported(this, e);
		}

		/// <summary>
		/// Called before header row is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnHeaderRowExporting(HeaderRowExportingEventArgs e)
		{
			if(this.HeaderRowExporting!=null)
				this.HeaderRowExporting(this, e);
		}

		/// <summary>
		/// Called after header row is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnHeaderRowExported(HeaderRowExportedEventArgs e)
		{
			if(this.HeaderRowExported!=null)
				this.HeaderRowExported(this, e);
		}

		/// <summary>
		/// Called before grid cell is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnCellExporting(CellExportingEventArgs e)
		{
			if(this.CellExporting!=null)
				this.CellExporting(this, e);
		}

		/// <summary>
		/// Called after grid cell is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnCellExported(CellExportedEventArgs e)
		{
			if(this.CellExported!=null)
				this.CellExported(this, e);
		}

		/// <summary>
		/// Called before header cell is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnHeaderCellExporting(HeaderCellExportingEventArgs e)
		{
			if(this.HeaderCellExporting!=null)
				this.HeaderCellExporting(this, e);
		}

		/// <summary>
		/// Called after header cell is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnHeaderCellExported(HeaderCellExportedEventArgs e)
		{
			if(this.HeaderCellExported!=null)
				this.HeaderCellExported(this, e);
		}

		/// <summary>
		/// Called before summary cell is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnSummaryCellExporting(SummaryCellExportingEventArgs e)
		{
			if(this.SummaryCellExporting!=null)
				this.SummaryCellExporting(this, e);
		}

		/// <summary>
		/// Called after summary cell is exported to excel.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnSummaryCellExported(SummaryCellExportedEventArgs e)
		{
			if(this.SummaryCellExported!=null)
				this.SummaryCellExported(this, e);
		}

		/// <summary>
		/// Called after grid export is finished.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnEndExport(EndExportEventArgs e)
		{
			if(this.EndExport!=null)
				this.EndExport(this, e);
		}

		/// <summary>
		/// Occurs when a grid column is initialized. 
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializeColumn(InitializeColumnEventArgs e)
		{
			if(this.InitializeColumn!=null)
				this.InitializeColumn(this, e);		
		}

        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
        #region NAS v8.3 - Exporting CalcManager Formulas to Excel

        #region OnFormulaExporting
        private FormulaExportingEventArgs OnFormulaExporting(object context, string gridFormula, WorksheetCell worksheetCell, 
            FormulaExportAction action)
        {
            FormulaExportingEventArgs formulaExportingEventArgs = new FormulaExportingEventArgs(
                context,
                gridFormula,
                worksheetCell,
                action);

            this.OnFormulaExporting(formulaExportingEventArgs);

            return formulaExportingEventArgs;
        }

        /// <summary>
        /// Occurs before exporting a formula to a worksheet cell.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        virtual protected void OnFormulaExporting(FormulaExportingEventArgs e)
        {
            if (this.FormulaExporting != null)
                this.FormulaExporting(this, e);
        }
        
        #endregion //OnFormulaExporting

        #region OnFormulaExported
        private FormulaExportedEventArgs OnFormulaExported(object context, string gridFormula, WorksheetCell worksheetCell)
        {
            FormulaExportedEventArgs formulaExportedEventArgs = new FormulaExportedEventArgs(
                context, 
                gridFormula, 
                worksheetCell);

            this.OnFormulaExported(formulaExportedEventArgs);

            return formulaExportedEventArgs;
        }

        /// <summary>
        /// Occurs after exporting a formula (or an error message if the formula could not be converted to Excel) to a worksheet cell.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        virtual protected void OnFormulaExported(FormulaExportedEventArgs e)
        {
            if (this.FormulaExported != null)
                this.FormulaExported(this, e);
        }
        #endregion //OnFormulaExported

        #region FormulaExportError

        private FormulaExportErrorEventArgs OnFormulaExportError(object context, string gridFormula, WorksheetCell worksheetCell, FormulaExportErrorAction action, Exception exception, string excelFormula)
        {
            FormulaExportErrorEventArgs formulaExportErrorEventArgs = new FormulaExportErrorEventArgs(
                context,
                gridFormula,
                worksheetCell,
                action,
                exception, 
                excelFormula);

            this.OnFormulaExportError(formulaExportErrorEventArgs);

            return formulaExportErrorEventArgs;
        }

        /// <summary>
        /// Occurs after exporting a formula (or an error message if the formula could not be converted to Excel) to a worksheet cell.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        virtual protected void OnFormulaExportError(FormulaExportErrorEventArgs e)
        {
            if (this.FormulaExportError != null)
                this.FormulaExportError(this, e);
        }

        #endregion //FormulaExportError

        #region OnInitializeSummary
        /// <summary>
        /// Occurs when a summary is initialized. 
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        virtual protected void OnInitializeSummary(InitializeSummaryEventArgs e)
        {
            if (this.InitializeSummary != null)
                this.InitializeSummary(this, e);
        }
        #endregion //OnInitializeSummary        

        #endregion //NAS v8.3 - Exporting CalcManager Formulas to Excel

        #endregion OnEvent Methods

        #region About Dialog and Licensing Interface

        /// <summary>
		/// Displays the About dialog for the control.
		/// </summary>
		[DesignOnly(true)]
		[LocalizedDescription("LD_MenuItemInfo_P_About")]
		[LocalizedCategory("LC_Design")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[ParenthesizePropertyName(true)]
		[Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor))]
		[MergableProperty(false)]
		public object About { get { return null; } }

		/// <summary>
		/// Return the license we cached inside the constructor
		/// </summary>
		UltraLicense IUltraLicensedComponent.License { get { return this.license; } }

		#endregion
	}

	// AS 7/28/03
	// Since the structure has characters representing tchar's
	// we need to specify that the character set for the structure
	// itself is dependant upon the os.
	//
	//[StructLayout(LayoutKind.Sequential)]
	[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Auto)]
	internal struct TEXTMETRIC 
	{ 
		public int tmHeight; 
		public int tmAscent; 
		public int tmDescent; 
		public int tmInternalLeading; 
		public int tmExternalLeading; 
		public int tmAveCharWidth; 
		public int tmMaxCharWidth; 
		public int tmWeight; 
		public int tmOverhang; 
		public int tmDigitizedAspectX; 
		public int tmDigitizedAspectY; 

		public char tmFirstChar; 
		public char tmLastChar; 
		public char tmDefaultChar; 
		public char tmBreakChar; 

		public byte tmItalic; 
		public byte tmUnderlined; 
		public byte tmStruckOut; 
		public byte tmPitchAndFamily; 
		public byte tmCharSet; 
	}
}
