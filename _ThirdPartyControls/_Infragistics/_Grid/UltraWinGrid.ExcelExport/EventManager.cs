#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


#region XPS License
/*----------------------------------------------------------------------*
* Note: To the best of our knowledge, Microsoft® doesn’t forbid         *
* third-party companies from developing a product for the XPS document  *
* generation. However, according to the XML Paper Specification Patent  *
* License, we need to provide the following notice for your information:* 
*                                                                       *
* This product may incorporate intellectual property owned by Microsoft *
* Corporation. The terms and conditions upon which Microsoft is         *
* licensing such intellectual property may be found at                  *
* http://go.microsoft.com/fwlink/?LinkId=52369.;                        *
* ----------------------------------------------------------------------*
*/		
#endregion XPS License

using System;
using System.ComponentModel;
using System.Collections;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid.ExcelExport
{

//	/// <summary>
//	/// Class for managing an UltraExplorerBar's events. It maintains an enabled flag for each event as well as a nested 'in progress' count and it exposes events for firing each event.
//	/// </summary>
//	public class UltraExplorerBarEventManager : EventManagerBase
//	{
//		#region Member Variables
//
//		private UltraExplorerBar			explorerBar;
//		private static int []				beforeEventIndexes = null;
//		private static int []				afterEventIndexes = null;
//
//		#endregion Member Variables
//
//		#region Constructor
//
//		/// <summary>
//		/// contructor
//		/// </summary>
//		/// <param name="explorerBar">The UltraExplorerBarManager that this event manager will be asociated with.</param>
//		public UltraExplorerBarEventManager(UltraExplorerBar explorerBar) : base( (int)UltraExplorerBarEventIds.LastEventId + 1 )
//		{
//			this.explorerBar = explorerBar;
//		}
//
//		#endregion Constructor
//
//		#region Properties
//
//			#region Public Properties
//
//				#region ExplorerBar
//
//		internal UltraExplorerBar ExplorerBar
//		{
//			get	{ return this.explorerBar; }
//		}
//
//				#endregion ExplorerBar
//
//			#endregion Public Properties		
//
//			#region Private Properties
//
//				#region AfterEventIndexes
//
//#if DEBUG
//		/// <summary>
//		/// Returns a lazily loaded array of indexes for 
//		/// all the 'After' events
//		/// </summary>
//#endif
//		private int [] AfterEventIndexes
//		{
//			get
//			{
//				if ( null == afterEventIndexes )
//				{
//					afterEventIndexes =  new int [] 
//					{
//						(int)UltraExplorerBarEventIds.ActiveGroupChanged,
//						(int)UltraExplorerBarEventIds.ActiveItemChanged,
//						(int)UltraExplorerBarEventIds.GroupAdded,
//						(int)UltraExplorerBarEventIds.GroupCollapsed,
//						(int)UltraExplorerBarEventIds.GroupDropped,
//						(int)UltraExplorerBarEventIds.GroupEnteredEditMode,
//						(int)UltraExplorerBarEventIds.GroupExitedEditMode,
//						(int)UltraExplorerBarEventIds.GroupExpanded,
//						(int)UltraExplorerBarEventIds.GroupRemoved,
//						(int)UltraExplorerBarEventIds.ItemAdded,
//						(int)UltraExplorerBarEventIds.ItemCheckStateChanged,
//						(int)UltraExplorerBarEventIds.ItemDropped,
//						(int)UltraExplorerBarEventIds.ItemEnteredEditMode,
//						(int)UltraExplorerBarEventIds.ItemExitedEditMode,
//						(int)UltraExplorerBarEventIds.ItemRemoved,
//						(int)UltraExplorerBarEventIds.SelectedGroupChanged
//					};
//				}
//
//				return afterEventIndexes;
//
//			}
//		}
//
//				#endregion AfterEventIndexes
//
//				#region BeforeEventIndexes
//
//#if DEBUG
//		/// <summary>
//		/// Returns a lazily loaded array of indexes for 
//		/// all the 'Before' events
//		/// </summary>
//#endif
//		private int [] BeforeEventIndexes
//		{
//			get
//			{
//				if ( null == beforeEventIndexes )
//				{
//					beforeEventIndexes =  new int [] 
//					{
//						(int)UltraExplorerBarEventIds.ActiveGroupChanging,
//						(int)UltraExplorerBarEventIds.ActiveItemChanging,
//						(int)UltraExplorerBarEventIds.ContextMenuInitializing,
//						(int)UltraExplorerBarEventIds.GroupAdding,
//						(int)UltraExplorerBarEventIds.GroupCollapsing,
//						(int)UltraExplorerBarEventIds.GroupDragging,
//						(int)UltraExplorerBarEventIds.GroupEnteringEditMode,
//						(int)UltraExplorerBarEventIds.GroupExitingEditMode,
//						(int)UltraExplorerBarEventIds.GroupExpanding,
//						(int)UltraExplorerBarEventIds.GroupRemoving,
//						(int)UltraExplorerBarEventIds.ItemAdding,
//						(int)UltraExplorerBarEventIds.ItemCheckStateChanging,
//						(int)UltraExplorerBarEventIds.ItemDragging,
//						(int)UltraExplorerBarEventIds.ItemEnteringEditMode,
//						(int)UltraExplorerBarEventIds.ItemExitingEditMode,
//						(int)UltraExplorerBarEventIds.ItemRemoving,
//						(int)UltraExplorerBarEventIds.SelectedGroupChanging
//					};
//				}
//
//				return beforeEventIndexes;
//
//			}
//		}
//
//				#endregion BeforeEventIndexes
//
//			#endregion Private Properties
//
//		#endregion Properties
//
//		#region Methods
//
//			#region Public Methods
//
//				#region InProgress
//
//		/// <summary>
//		/// Returns True if the event is in progress (in progress count > 0).
//		/// </summary>
//		/// <remarks>
//		/// <p class="body">Use this method to determine whether the code of an event is currently being executed. You may want to know whether an event is occurring before disabling it.</p>
//		/// <p class="body">The "in progress" count is used to account for recursive calls to events. You can use it to determine how many nested calls to an event have been made and which level of recursion is now executing.</p>
//		/// </remarks>
//		public bool InProgress(UltraExplorerBarEventIds eventid)
//		{
//			return base.InProgress((int)eventid);
//		}
//
//				#endregion InProgress
//
//				#region IsEnabled (eventid)
//
//		/// <summary>
//		/// Returns True if the event is enabled.
//		/// </summary>
//		public bool IsEnabled(UltraExplorerBarEventIds eventid)
//		{
//			return base.IsEnabled( (int)eventid );
//		}
//
//				#endregion IsEnabled (eventid)
//
//				#region SetEnabled (eventid, enabled)
//
//		/// <summary>
//		/// Sets a specific event to enabled or disabled.
//		/// </summary>
//		/// <remarks>
//		/// <p class="body">Before setting an event to disabled, you should use <see cref="InProgress"/> to check whether the event is currently occurring.</p>
//		/// </remarks>
//		public void SetEnabled(UltraExplorerBarEventIds eventid, bool enabled)
//		{
//			base.SetEnabled((int)eventid, enabled);
//		}
//
//				#endregion SetEnabled (eventid, enabled)
//
//				#region IsEnabled (group)
//
//		/// <summary>
//		/// Returns true if all events in the group are enabled.
//		/// </summary>
//		public bool IsEnabled(UltraExplorerBarEventGroups group)
//		{
//			switch (group)
//			{
//				case UltraExplorerBarEventGroups.AllEvents:
//					return this.AllEventsEnabled;
//
//				case UltraExplorerBarEventGroups.BeforeEvents:
//					return base.IsEnabled(this.BeforeEventIndexes);
//
//				case UltraExplorerBarEventGroups.AfterEvents:
//					return base.IsEnabled(this.AfterEventIndexes);
//
//			}
//
//			return false;
//		}
//
//				#endregion IsEnabled (group)
//
//				#region SetEnabled (group, enabled)
//
//		/// <summary>
//		/// Sets all events in the group to enabled or disabled.
//		/// </summary>
//		public void SetEnabled(UltraExplorerBarEventGroups group, bool enabled)
//		{
//			switch (group)
//			{
//				case UltraExplorerBarEventGroups.AllEvents:
//					base.AllEventsEnabled = enabled;
//					break;
//
//				case UltraExplorerBarEventGroups.BeforeEvents:
//					base.SetEnabled(this.BeforeEventIndexes, enabled);
//					break;
//
//				case UltraExplorerBarEventGroups.AfterEvents:
//					base.SetEnabled(this.AfterEventIndexes, enabled);
//					break;
//
//			}
//		}
//				#endregion SetEnabled (group, enabled)
//
//			#endregion Public Methods
//
//			#region Internal Methods
//
//				#region CanFireEvent
//
//#if DEBUG
//		/// <summary>
//		/// Returns true if the event can be fired.
//		/// </summary>
//#endif
//		internal bool CanFireEvent(UltraExplorerBarEventIds eventid)
//		{
//			return this.IsEnabled(eventid);
//		}
//
//				#endregion CanFireEvent
//
//				#region DecrementInProgress
//
//#if DEBUG
//		/// <summary>
//		/// Decrements the "in progress" count.
//		/// </summary>
//		/// <remarks>
//		/// <p class="body">The "in progress" count is used to account for recursive calls to events. You can use it to determine how many nested calls to an event have been made and which level of recursion is now executing.</p>
//		/// </remarks>
//#endif
//		internal void DecrementInProgress(UltraExplorerBarEventIds eventid)
//		{
//			base.DecrementInProgress((int)eventid);
//		}
//
//				#endregion DecrementInProgress
//
//				#region IncrementInProgress
//
//#if DEBUG
//		/// <summary>
//		/// Increments the "in progress" count.
//		/// </summary>
//		/// <remarks>
//		/// <p class="body">The "in progress" count is used to account for recursive calls to events. You can use it to determine how many nested calls to an event have been made and which level of recursion is now executing.</p>
//		/// </remarks>
//#endif
//		internal void IncrementInProgress(UltraExplorerBarEventIds eventid)
//		{
//			base.IncrementInProgress((int)eventid);
//		}
//
//				#endregion IncrementInProgress
//
//			#endregion Internal Methods
//
//		#endregion Methods
//	}
}
