#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


#region XPS License
/*----------------------------------------------------------------------*
* Note: To the best of our knowledge, Microsoft® doesn’t forbid         *
* third-party companies from developing a product for the XPS document  *
* generation. However, according to the XML Paper Specification Patent  *
* License, we need to provide the following notice for your information:* 
*                                                                       *
* This product may incorporate intellectual property owned by Microsoft *
* Corporation. The terms and conditions upon which Microsoft is         *
* licensing such intellectual property may be found at                  *
* http://go.microsoft.com/fwlink/?LinkId=52369.;                        *
* ----------------------------------------------------------------------*
*/		
#endregion XPS License

namespace Infragistics.Win.UltraWinGrid.ExcelExport
{
	/// <summary>
	/// An enumerator for different header types used in header export related events.
	/// </summary>
	public enum HeaderTypes
	{
		/// <summary>
		/// Currently exporting band header.
		/// </summary>
		BandHeader		= 0,

		/// <summary>
		/// Currently exporting group header.
		/// </summary>
		GroupHeader		= 1,

		/// <summary>
		/// Currently exporting column header.
		/// </summary>
		ColumnHeader	= 2
	}

	/// <summary>
	/// An enumeration specifying different behaviors if excel file limit (maximum column number, maximum row number etc.) is reached during the export.
	/// </summary>
	public enum FileLimitBehaviour
	{
		/// <summary>
		/// Throw exception if excel file limit is reached.
		/// </summary>
		ThrowException	= 0,

		/// <summary>
		/// Truncate extra data if excel file limit is reached.
		/// </summary>
		TruncateData	= 1
	}

    // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
    #region NAS v8.3 - Exporting CalcManager Formulas to Excel

    #region FormulaExportErrorAction
    /// <summary>
    /// An enum listing possible actions to take when exporting a formula to Excel.
    /// </summary>
    public enum FormulaExportErrorAction
    {
        /// <summary>
        /// Write the <see cref="FormulaExportErrorEventArgs.ErrorText"/> into the cell.
        /// </summary>
        WriteErrorMessageToWorksheetCell,

        /// <summary>
        /// Cancel the exporting of the formula to the cell. The cell value will remain unchanged. 
        /// </summary>
        CancelOne,

        /// <summary>
        /// Cancels the exporting of all instances of this formula. The cell value will remain unchanged. No further events will be fired for this formula.
        /// </summary>
        CancelAll
    }
    #endregion //FormulaExportErrorAction

    #region FormulaExportError
    /// <summary>
    /// An enum listing possible errors when converting a function from the grid into Excel.
    /// </summary>
    public enum FormulaExportErrorType
    {
        /// <summary>
        /// The formula references a function that Excel does not recognize.
        /// </summary>
        UnrecognizedFunction,

        /// <summary>
        /// This function cannot be converted into an Excel function. 
        /// </summary>
        UnsupportedFunction,

        /// <summary>
        /// The Excel engine reported that the formula was invalid. 
        /// </summary>
        InvalidFormula,
    }
    #endregion //FormulaExportError

    #region FormulaExportAction
    /// <summary>
    /// An enum listing possible actions to take when exporting a formula to Excel.
    /// </summary>
    public enum FormulaExportAction
    {
        /// <summary>
        /// Attempts to convert the formula from the grid into an equivalent Excel formula and exports the formula to the WorksheetCell.
        /// </summary>
        ExportFormula,

        /// <summary>
        /// Cancels the exporting of the formula to the individual WorksheetCell.
        /// </summary>
        CancelOne,

        /// <summary>
        /// Cancels the exporting of the formula for all cells in the column or all occurrances of the summary.
        /// </summary>
        CancelAll
    }
    #endregion //FormulaExportAction

    #region NamedReference
    internal enum NamedReferenceConstant
    {
        DaysPerWeek = 7,
        HoursPerDay = 24,
        MinutesPerDay = 24 * 60,
        SecondsPerDay = 24 * 60 * 60
    }
    #endregion //NamedReference

    #endregion //NAS v8.3 - Exporting CalcManager Formulas to Excel

    // MBS 6/11/09 - TFS18312/FR04462/FR08690
    #region BandSpacing

    /// <summary>
    /// An enumeration specifying the amount of empty rows used as padding around a set of child rows.
    /// </summary>
    public enum BandSpacing
    {
        /// <summary>
        /// Specifies that no empty rows will be placed around a child rows collection.
        /// </summary>
        None = 0,

        /// <summary>
        /// A single empty row will be placed on either side of a child rows collection.
        /// </summary>
        SingleRow = 1
    }
    #endregion //BandSpacing
}
