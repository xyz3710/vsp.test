#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


#region XPS License
/*----------------------------------------------------------------------*
* Note: To the best of our knowledge, Microsoft® doesn’t forbid         *
* third-party companies from developing a product for the XPS document  *
* generation. However, according to the XML Paper Specification Patent  *
* License, we need to provide the following notice for your information:* 
*                                                                       *
* This product may incorporate intellectual property owned by Microsoft *
* Corporation. The terms and conditions upon which Microsoft is         *
* licensing such intellectual property may be found at                  *
* http://go.microsoft.com/fwlink/?LinkId=52369.;                        *
* ----------------------------------------------------------------------*
*/		
#endregion XPS License

namespace Infragistics.Win.UltraWinGrid.ExcelExport
{
    using System;

//    /// <summary>
//    ///    Values that uniquely identify each control property. 
//    /// </summary>
//	public enum PropertyIds 
//	{
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ActiveGroup"/> property.
//		/// </summary>
//		ActiveGroup					= 1,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ActiveItem"/> property.
//		/// </summary>
//		ActiveItem					= 2,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.AnimationEnabled"/> property.
//		/// </summary>
//		AnimationEnabled			= 3,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.AnimationSpeed"/> property.
//		/// </summary>
//		AnimationSpeed				= 4,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.Appearance"/> property.
//		/// </summary>
//		Appearance					= 5,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.Appearances"/> property.
//		/// </summary>
//		Appearances					= 6,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.BorderStyle"/> property.
//		/// </summary>
//		BorderStyle					= 7,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.CheckedItem"/> property.
//		/// </summary>
//		CheckedItem					= 8,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ColumnCount"/> property.
//		/// </summary>
//		ColumnCount					= 9,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ColumnSpacing"/> property.
//		/// </summary>
//		ColumnSpacing				= 10,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.CurrentState"/> property.
//		/// </summary>
//		CurrentState				= 11,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.Groups"/> property.
//		/// </summary>
//		Groups						= 12,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.GroupSettings"/> property.
//		/// </summary>
//		GroupSettings				= 13,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.GroupSpacing"/> property.
//		/// </summary>
//		GroupSpacing				= 14,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ImageListSmall"/> property.
//		/// </summary>
//		ImageListSmall				= 15,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ImageListLarge"/> property.
//		/// </summary>
//		ImageListLarge				= 16,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ImageSizeSmall"/> property.
//		/// </summary>
//		ImageSizeSmall				= 17,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ImageSizeLarge"/> property.
//		/// </summary>
//		ImageSizeLarge				= 18,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ImageTransparentColor"/> property.
//		/// </summary>
//		ImageTransparentColor		= 19,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ItemSettings"/> property.
//		/// </summary>
//		ItemSettings				= 20,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.KeyActionMappings"/> property.
//		/// </summary>
//		KeyActionMappings			= 21,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.Margins"/> property.
//		/// </summary>
//		Margins						= 22,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.Scrollbars"/> property.
//		/// </summary>
//		Scrollbars					= 23,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.SelectedGroup"/> property.
//		/// </summary>
//		SelectedGroup				= 24,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ShowDefaultContextMenu"/> property.
//		/// </summary>
//		ShowDefaultContextMenu		= 25,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.StateButtonCheckStyle"/> property.
//		/// </summary>
//		StateButtonCheckStyle		= 26,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.Style"/> property.
//		/// </summary>
//		Style						= 27,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.UseLargeGroupHeaderImages"/> property.
//		/// </summary>
//		UseLargeGroupHeaderImages	= 28,
//
//		/// <summary>
//		/// The <see cref="Infragistics.Win.UltraWinExplorerBar.UltraExplorerBar.ViewStyle"/> property.
//		/// </summary>
//		ViewStyle					= 29,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarSettingsBase.AllowEdit"/> property.
//		/// </summary>
//		AllowEdit = 33,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.AllowDrag"/> property.
//		/// </summary>
//		AllowDrag = 34,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.AllowItemDrop"/> property.
//		/// </summary>
//		AllowItemDrop = 35,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.AllowItemUncheck"/> property.
//		/// </summary>
//		AllowItemUncheck = 36,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.HeaderButtonStyle"/> property.
//		/// </summary>
//		HeaderButtonStyle = 37,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.BorderStyleItemArea"/> property.
//		/// </summary>
//		BorderStyleItemArea = 38,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.HeaderVisible"/> property.
//		/// </summary>
//		HeaderVisible = 39,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.ItemAreaOuterMargins"/> property.
//		/// </summary>
//		ItemAreaOuterMargins = 40,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.ItemSort"/> property.
//		/// </summary>
//		ItemSort = 41,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.ItemSortComparer"/> property.
//		/// </summary>
//		ItemSortComparer = 42,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarSettingsBase.Editor"/> property.
//		/// </summary>
//		Editor = 43,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarSettingsBase.EditorControl"/> property.
//		/// </summary>
//		EditorControl = 44,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarSettingsBase.HotTracking"/> property.
//		/// </summary>
//		HotTracking = 45,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarSettingsBase.MaxLines"/> property.
//		/// </summary>
//		MaxLines = 46,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarSettingsBase.ShowToolTips"/> property.
//		/// </summary>
//		ShowToolTips = 47,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.ActiveAppearance"/> property.
//		/// </summary>
//		ActiveAppearance = 48,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.ActiveHeaderAppearance"/> property.
//		/// </summary>
//		ActiveHeaderAppearance = 49,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.EditAppearance"/> property.
//		/// </summary>
//		EditAppearance = 50,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.HeaderAppearance"/> property.
//		/// </summary>
//		HeaderAppearance = 51,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.HeaderHotTrackAppearance"/> property.
//		/// </summary>
//		HeaderHotTrackAppearance = 52,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.HotTrackAppearance"/> property.
//		/// </summary>
//		HotTrackAppearance = 53,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.SelectedAppearance"/> property.
//		/// </summary>
//		SelectedAppearance = 54,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.SelectedHeaderAppearance"/> property.
//		/// </summary>
//		SelectedHeaderAppearance = 55,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.AppearancesLarge"/> property.
//		/// </summary>
//		AppearancesSmall = 56,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.AppearancesLarge"/> property.
//		/// </summary>
//		AppearancesLarge = 57,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.ColumnsSpanned"/> property.
//		/// </summary>
//		ColumnsSpanned = 58,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Container"/> property.
//		/// </summary>
//		Container = 59,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Enabled"/> and <see cref="UltraExplorerBarItemSettings.Enabled"/> properties.
//		/// </summary>
//		Enabled = 60,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Expanded"/> property.
//		/// </summary>
//		Expanded = 61,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.ExplorerBar"/> and <see cref="UltraExplorerBarSettingsBase.ExplorerBar"/> properties.
//		/// </summary>
//		ExplorerBar = 62,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Index"/> and <see cref="UltraExplorerBarItem.Index"/> properties.
//		/// </summary>
//		Index = 63,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Active"/> and <see cref="UltraExplorerBarItem.Active"/> properties.
//		/// </summary>
//		Active = 64,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.IsInView"/> and <see cref="UltraExplorerBarItem.IsInView"/> properties.
//		/// </summary>
//		IsInView = 65,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Items"/> property.
//		/// </summary>
//		Items = 66,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Settings"/> and <see cref="UltraExplorerBarItem.Settings"/> properties.
//		/// </summary>
//		Settings = 67,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.SettingsResolved"/> and <see cref="UltraExplorerBarItem.SettingsResolved"/> properties.
//		/// </summary>
//		SettingsResolved = 68,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Text"/> and <see cref="UltraExplorerBarItem.Text"/> properties.
//		/// </summary>
//		Text = 69,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.ToolTipText"/> and <see cref="UltraExplorerBarItem.ToolTipText"/> properties.
//		/// </summary>
//		ToolTipText = 70,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBar.UIElement"/>, <see cref="UltraExplorerBarGroup.UIElement"/> and <see cref="UltraExplorerBarItem.UIElement"/> properties.
//		/// </summary>
//		UIElement = 71,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Visible"/> and <see cref="UltraExplorerBarItem.Visible"/> properties.
//		/// </summary>
//		Visible = 72,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemAppearances.CheckedAppearance"/>property.
//		/// </summary>
//		CheckedAppearance = 73,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemSettings.AllowDragCopy"/>property.
//		/// </summary>
//		AllowDragCopy = 74,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemSettings.AllowDragMove"/>property.
//		/// </summary>
//		AllowDragMove = 75,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemSettings.Height"/>property.
//		/// </summary>
//		Height = 76,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemSettings.HotTrackStyle"/>property.
//		/// </summary>
//		HotTrackStyle = 77,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemSettings.HotTrackBorderStyle"/>property.
//		/// </summary>
//		HotTrackBorderStyle = 78,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemSettings.Indent"/>property.
//		/// </summary>
//		Indent = 79,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemSettings.UseDefaultImage"/>property.
//		/// </summary>
//		UseDefaultImage = 80,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItem.Checked"/>property.
//		/// </summary>
//		Checked = 81,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItem.Group"/>property.
//		/// </summary>
//		Group = 82,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroup.Selected"/>property.
//		/// </summary>
//		Selected = 83,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.ScrollButtonAppearance"/>property.
//		/// </summary>
//		ScrollButtonAppearance = 84,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.ScrollButtonHotTrackAppearance"/>property.
//		/// </summary>
//		ScrollButtonHotTrackAppearance = 85,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupAppearances.ItemAreaAppearance"/>property.
//		/// </summary>
//		ItemAreaAppearance = 86,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBar.AcceptsFocus"/>property.
//		/// </summary>
//		AcceptsFocus = 87,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.ContainerHeight"/>property.
//		/// </summary>
//		ContainerHeight = 88,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarItemSettings.SeparatorStyle"/>property.
//		/// </summary>
//		SeparatorStyle = 89,
//
//		/// <summary>
//		/// The <see cref="UltraExplorerBarSettingsBase.UseMnemonics"/>property.
//		/// </summary>
//		UseMnemonics = 90,
//
//		//	BF 5.9.03
//		/// <summary>
//		/// The <see cref="UltraExplorerBarGroupSettings.ItemAreaInnerMargins"/> property.
//		/// </summary>
//		ItemAreaInnerMargins = 40,
//	}
}
