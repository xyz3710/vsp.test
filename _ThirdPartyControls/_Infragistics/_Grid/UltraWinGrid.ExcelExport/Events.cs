#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


#region XPS License
/*----------------------------------------------------------------------*
* Note: To the best of our knowledge, Microsoft® doesn’t forbid         *
* third-party companies from developing a product for the XPS document  *
* generation. However, according to the XML Paper Specification Patent  *
* License, we need to provide the following notice for your information:* 
*                                                                       *
* This product may incorporate intellectual property owned by Microsoft *
* Corporation. The terms and conditions upon which Microsoft is         *
* licensing such intellectual property may be found at                  *
* http://go.microsoft.com/fwlink/?LinkId=52369.;                        *
* ----------------------------------------------------------------------*
*/		
#endregion XPS License

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;

using Infragistics.Excel;
using Infragistics.Win.UltraWinGrid.ExcelExport.FormulaExporting;

namespace Infragistics.Win.UltraWinGrid.ExcelExport
{
	#region Event Handlers (delegates)

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.BeginExport"/> event.
	/// </summary>
	public delegate void BeginExportEventHandler(object sender, BeginExportEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.InitializeRow"/> event.
	/// </summary>
	public delegate void InitializeRowEventHandler(object sender, ExcelExportInitializeRowEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.RowExporting"/> event.
	/// </summary>
	public delegate void RowExportingEventHandler(object sender, RowExportingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.RowExported"/> event.
	/// </summary>
	public delegate void RowExportedEventHandler(object sender, RowExportedEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.SummaryRowExporting"/> event.
	/// </summary>
	public delegate void SummaryRowExportingEventHandler(object sender, SummaryRowExportingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.SummaryRowExported"/> event.
	/// </summary>
	public delegate void SummaryRowExportedEventHandler(object sender, SummaryRowExportedEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.HeaderRowExporting"/> event.
	/// </summary>
	public delegate void HeaderRowExportingEventHandler(object sender, HeaderRowExportingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.HeaderRowExported"/> event.
	/// </summary>
	public delegate void HeaderRowExportedEventHandler(object sender, HeaderRowExportedEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.CellExporting"/> event.
	/// </summary>
	public delegate void CellExportingEventHandler(object sender, CellExportingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.CellExported"/> event.
	/// </summary>
	public delegate void CellExportedEventHandler(object sender, CellExportedEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.HeaderCellExporting"/> event.
	/// </summary>
	public delegate void HeaderCellExportingEventHandler(object sender, HeaderCellExportingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.HeaderCellExported"/> event.
	/// </summary>
	public delegate void HeaderCellExportedEventHandler(object sender, HeaderCellExportedEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.SummaryCellExporting"/> event.
	/// </summary>
	public delegate void SummaryCellExportingEventHandler(object sender, SummaryCellExportingEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.SummaryCellExported"/> event.
	/// </summary>
	public delegate void SummaryCellExportedEventHandler(object sender, SummaryCellExportedEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.EndExport"/> event.
	/// </summary>
	public delegate void EndExportEventHandler(object sender, EndExportEventArgs e);

	/// <summary>
	/// Delegate for handling the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.InitializeColumn"/> event.
	/// </summary>
	public delegate void InitializeColumnEventHandler(object sender, 
		InitializeColumnEventArgs e);

	#endregion Event Handlers (delegates)
	
	#region Event Args

    #region ExcelExportEventArgs
    /// <summary>
	/// ExcelExportEventArgs is a base class for non-cancelable event argument classes.
	/// </summary>
	public class ExcelExportEventArgs: System.EventArgs
	{
		#region Private Members

		private UltraGridExporterHelper exporterHelper;

		#endregion Private Members
		
		#region Constructor

		internal ExcelExportEventArgs(UltraGridExporterHelper exporterHelper)
		{
			this.exporterHelper = exporterHelper;
		}

		#endregion Constructor

		#region Public Properties

		/// <summary>
		/// Exporting workbook.
		/// </summary>
		public Workbook Workbook
		{
			get { return this.exporterHelper.CurrentWorksheet.Workbook; }
		}

		/// <summary>
		/// Current exporting worksheet.
		/// </summary>
		public Worksheet CurrentWorksheet
		{
			get { return this.exporterHelper.CurrentWorksheet; }
			set { this.exporterHelper.CurrentWorksheet = value; }
		}

		/// <summary>
		/// Zero-based index of current exporting row in excel worksheet.
		/// </summary>
		public int CurrentRowIndex
		{
			get { return this.exporterHelper.currentPos.Y; }
			set { this.exporterHelper.currentPos.Y = value; }
		}

		/// <summary>
		/// Zero-based index of current exporting column in excel worksheet.
		/// </summary>			
		public int CurrentColumnIndex
		{
			get { return this.exporterHelper.currentPos.X; }
			set { this.exporterHelper.currentPos.X = value; }
		}

		/// <summary>
		/// Current outline level used for grouping.
		/// </summary>
		public int CurrentOutlineLevel
		{
			get { return this.exporterHelper.HierarchyLevel; }
		}

		#endregion Public Properties
    }
    #endregion //ExcelExportEventArgs

    #region ExcelExportCancelEventArgs
    /// <summary>
	/// ExcelExportCancelEventArgs is a base class for cancelable event argument classes.
	/// </summary>
	public class ExcelExportCancelEventArgs: System.ComponentModel.CancelEventArgs
	{
		#region Private Members

		private UltraGridExporterHelper exporterHelper;

		#endregion Private Members
		
		#region Constructor

		internal ExcelExportCancelEventArgs(UltraGridExporterHelper exporterHelper)
		{
			this.exporterHelper = exporterHelper;
		}

		#endregion Constructor

		#region Public Properties

		/// <summary>
		/// Exporting workbook.
		/// </summary>
		public Workbook Workbook
		{
			get { return this.exporterHelper.CurrentWorksheet.Workbook; }
		}

		/// <summary>
		/// Current exporting worksheet.
		/// </summary>
		public Worksheet CurrentWorksheet
		{
			get { return this.exporterHelper.CurrentWorksheet; }
			set { this.exporterHelper.CurrentWorksheet = value; }
		}

		/// <summary>
		/// Zero-based index of current exporting row in excel worksheet.
		/// </summary>
		public int CurrentRowIndex
		{
			get { return this.exporterHelper.currentPos.Y; }
			set { this.exporterHelper.currentPos.Y = value; }
		}

		/// <summary>
		/// Zero-based index of current exporting column in excel worksheet..
		/// </summary>			
		public int CurrentColumnIndex
		{
			get { return this.exporterHelper.currentPos.X; }
			set { this.exporterHelper.currentPos.X = value; }
		}

		/// <summary>
		/// Current outline level used for grouping.
		/// </summary>
		public int CurrentOutlineLevel
		{
			get { return this.exporterHelper.HierarchyLevel; }
		}

		#endregion Public Properties
    }
    #endregion //ExcelExportCancelEventArgs

    #region BeginExportEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.BeginExport"/> event.
	/// </summary>
	public class BeginExportEventArgs: ExcelExportEventArgs
	{
		private UltraGridLayout layout;
		private RowsCollection rows;

		internal BeginExportEventArgs(UltraGridExporterHelper exporterHelper, 
			UltraGridLayout layout, RowsCollection rows): base(exporterHelper)
		{
			this.layout = layout;
			this.rows = rows;
		}

		/// <summary>
		/// Grid layout.
		/// </summary>
		public UltraGridLayout Layout
		{
			get { return this.layout; }
		}

		/// <summary>
		/// Top band rows collection.
		/// </summary>
		public RowsCollection Rows
		{
			get { return this.rows; }
		}
    }
    #endregion //BeginExportEventArgs

    #region ExcelExportInitializeRowEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.InitializeRow"/> event.
	/// </summary>
	public class ExcelExportInitializeRowEventArgs: ExcelExportEventArgs
	{
		private UltraGridRow row;
		private ProcessRowParams processRowParams;
		private bool skipRow;

		internal ExcelExportInitializeRowEventArgs(UltraGridExporterHelper exporterHelper,
			ProcessRowParams processRowParams, UltraGridRow row): base(exporterHelper)
		{
			this.row = row;
			this.processRowParams = processRowParams;
			this.skipRow = false;
		}

		/// <summary>
		/// Grid row.
		/// </summary>
		public UltraGridRow Row
		{
			get { return this.row; }
		}

		/// <summary>
		/// Specifies whether to skip the current row.
		/// </summary>
		public bool SkipRow
		{
			get { return this.skipRow; }
			set { this.skipRow = value; }
		}

		/// <summary>
		/// Specifies whether to skip the descendats of the current row.
		/// </summary>
		public bool SkipDescendants
		{
			get { return this.processRowParams.SkipDescendants; }
			set { this.processRowParams.SkipDescendants = value; }
		}

		/// <summary>
		/// Specifies whether to skip sibling rows of the current row.
		/// </summary>
		public bool SkipSiblings
		{
			get { return this.processRowParams.SkipSiblings; }
			set { this.processRowParams.SkipSiblings = value; }
		}

		/// <summary>
		/// Specifies whether to terminate the export process. Current row will 
		/// not be processed.
		/// </summary>
		public bool TerminateExport
		{
			get { return this.processRowParams.TerminateExport; }
			set { this.processRowParams.TerminateExport = value; }
		}
    }
    #endregion //ExcelExportInitializeRowEventArgs

    #region RowExportingEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.RowExporting"/> event.
	/// </summary>
	public class RowExportingEventArgs: ExcelExportCancelEventArgs
	{
		private UltraGridRow gridRow;

		internal RowExportingEventArgs(UltraGridExporterHelper exporterHelper, 
			UltraGridRow gridRow): base(exporterHelper)
		{
			this.gridRow = gridRow;
		}

		/// <summary>
		/// Reference to row being exported.
		/// </summary>
		public UltraGridRow GridRow
		{
			get { return this.gridRow; }
		}
    }
    #endregion //RowExportingEventArgs

    #region RowExportedEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.RowExported"/> event.
	/// </summary>
	public class RowExportedEventArgs: ExcelExportEventArgs
	{
		private UltraGridRow gridRow;

		internal RowExportedEventArgs(UltraGridExporterHelper exporterHelper,
			UltraGridRow gridRow): base(exporterHelper)
		{
			this.gridRow = gridRow;
		}

		/// <summary>
		/// Reference to row being exported.
		/// </summary>
		public UltraGridRow GridRow
		{
			get { return this.gridRow; }
		}
    }
    #endregion //RowExportedEventArgs

    #region SummaryRowExportingEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.SummaryRowExporting"/> event.
	/// </summary>
	public class SummaryRowExportingEventArgs: ExcelExportCancelEventArgs
	{
		private SummaryValuesCollection summaries;
		private int summaryLevel;

		internal SummaryRowExportingEventArgs(UltraGridExporterHelper exporterHelper, 
			SummaryValuesCollection summaries, int summaryLevel): base(exporterHelper)
		{
			this.summaries = summaries;
			this.summaryLevel = summaryLevel;
		}

		/// <summary>
		/// Summary values.
		/// </summary>
		public SummaryValuesCollection Summaries
		{
			get { return this.summaries; }
		}

		/// <summary>
		/// Summary level.
		/// </summary>
		public int SummaryLevel
		{
			get { return this.summaryLevel; }
		}
    }
    #endregion //SummaryRowExportingEventArgs

    #region SummaryRowExportedEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.SummaryRowExported"/> event.
	/// </summary>
	public class SummaryRowExportedEventArgs: ExcelExportEventArgs
	{
		private SummaryValuesCollection summaries;
		private int summaryLevel;

		internal SummaryRowExportedEventArgs(UltraGridExporterHelper exporterHelper, 
			SummaryValuesCollection summaries, int summaryLevel):	base(exporterHelper)
		{
			this.summaries = summaries;
			this.summaryLevel = summaryLevel;
		}

		/// <summary>
		/// Summary values.
		/// </summary>
		public SummaryValuesCollection Summaries
		{
			get { return this.summaries; }
		}

		/// <summary>
		/// Summary level.
		/// </summary>
		public int SummaryLevel
		{
			get { return this.summaryLevel; }
		}
    }
    #endregion //SummaryRowExportedEventArgs

    #region HeaderRowExportingEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.HeaderRowExporting"/> event.
	/// </summary>
	public class HeaderRowExportingEventArgs: ExcelExportCancelEventArgs
	{
		private UltraGridBand band;
		private UltraGridRow gridRow;
		private HeaderTypes headerType;

		internal HeaderRowExportingEventArgs(UltraGridExporterHelper exporterHelper,
			UltraGridBand band, UltraGridRow gridRow, HeaderTypes headerType): 
			base(exporterHelper)
		{
			this.band = band;
			this.gridRow = gridRow;
			this.headerType = headerType;
		}

		/// <summary>
		/// UltraGridBand associated with headers.
		/// </summary>
		public UltraGridBand Band
		{
			get { return this.band; }
		}

		/// <summary>
		/// UltraGridRow associated with headers.
		/// </summary>
		public UltraGridRow GridRow		
		{
			get { return this.gridRow; }
		}

		/// <summary>
		/// Type of header.
		/// </summary>
		public HeaderTypes HeaderType
		{
			get { return this.headerType; }
		}
    }
    #endregion //HeaderRowExportingEventArgs

    #region HeaderRowExportedEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.HeaderRowExported"/> event.
	/// </summary>
	public class HeaderRowExportedEventArgs: ExcelExportEventArgs
	{
		private UltraGridBand band;
		private UltraGridRow gridRow;
		private HeaderTypes headerType;

		internal HeaderRowExportedEventArgs(UltraGridExporterHelper exporterHelper,
			UltraGridBand band, UltraGridRow gridRow, HeaderTypes headerType): 
			base(exporterHelper)
		{
			this.band = band;
			this.gridRow = gridRow;
			this.headerType = headerType;
		}

		/// <summary>
		/// UltraGrid band associated with headers.
		/// </summary>
		public UltraGridBand Band
		{
			get { return this.band; }
		}

		/// <summary>
		/// UltraGrid row associated with headers.
		/// </summary>
		public UltraGridRow GridRow		
		{
			get { return this.gridRow; }
		}

		/// <summary>
		/// Type of header.
		/// </summary>
		public HeaderTypes HeaderType
		{
			get { return this.headerType; }
		}
    }
    #endregion //HeaderRowExportedEventArgs

    #region CellExportingEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.CellExporting"/> event.
	/// </summary>
	public class CellExportingEventArgs: ExcelExportCancelEventArgs
	{
		private UltraGridRow gridRow;
		private UltraGridColumn gridColumn;

		internal CellExportingEventArgs(UltraGridExporterHelper exporterHelper,
			UltraGridRow gridRow, UltraGridColumn gridColumn): 
			base(exporterHelper)
		{
			this.gridRow = gridRow;
			this.gridColumn = gridColumn;
		}

		/// <summary>
		/// Grid row containing the cell.
		/// </summary>
		public UltraGridRow GridRow
		{
			get { return this.gridRow; }
		}

		/// <summary>
		/// Associated grid column.
		/// </summary>
		public UltraGridColumn GridColumn
		{
			get { return this.gridColumn; }
		}

		/// <summary>
		/// Grid cell value.
		/// </summary>
		public object Value
		{
			get { return this.gridRow.GetCellValue(this.gridColumn); }
			set { this.gridRow.Cells[this.gridColumn].Value = value; }
		}
    }
    #endregion //CellExportingEventArgs

    #region CellExportedEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.CellExported"/> event.
	/// </summary>
	public class CellExportedEventArgs: ExcelExportEventArgs
	{
		private UltraGridRow gridRow;
		private UltraGridColumn gridColumn;
		private object valueMbr;

		internal CellExportedEventArgs(UltraGridExporterHelper exporterHelper,
			UltraGridRow gridRow, UltraGridColumn gridColumn, object valueMbr): 
			base(exporterHelper)
		{
			this.gridRow = gridRow;
			this.gridColumn = gridColumn;
			this.valueMbr = valueMbr;
		}

		/// <summary>
		/// Grid row containing the cell.
		/// </summary>
		public UltraGridRow GridRow
		{
			get { return this.gridRow; }
		}

		/// <summary>
		/// Associated grid column.
		/// </summary>
		public UltraGridColumn GridColumn
		{
			get { return this.gridColumn; }
		}

		/// <summary>
		/// Grid cell value.
		/// </summary>
		public object Value
		{
			get { return this.valueMbr; }
		}
    }
    #endregion //CellExportedEventArgs

    #region HeaderCellExportingEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.HeaderCellExporting"/> event.
	/// </summary>
	public class HeaderCellExportingEventArgs: ExcelExportCancelEventArgs
	{
		private HeaderBase gridHeader;
		private UltraGridRow gridRow;
		private HeaderTypes headerType;

		internal HeaderCellExportingEventArgs(UltraGridExporterHelper exporterHelper,
			HeaderBase gridHeader, UltraGridRow gridRow, HeaderTypes headerType): 
			base(exporterHelper)
		{
			this.gridHeader = gridHeader;
			this.gridRow = gridRow;
			this.headerType = headerType;
		}

		/// <summary>
		/// Grid header.
		/// </summary>
		public HeaderBase GridHeader
		{
			get { return this.gridHeader; }
		}

		/// <summary>
		/// Associated grid row.
		/// </summary>
		public UltraGridRow GridRow
		{
			get { return this.gridRow; }
		}

		/// <summary>
		/// Type of header.
		/// </summary>
		public HeaderTypes HeaderType
		{
			get { return this.headerType; }
		}
    }
    #endregion //HeaderCellExportingEventArgs

    #region HeaderCellExportedEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.HeaderCellExported"/> event.
	/// </summary>
	public class HeaderCellExportedEventArgs: ExcelExportEventArgs
	{
		private HeaderBase gridHeader;
		private UltraGridRow gridRow;
		private HeaderTypes headerType;

		internal HeaderCellExportedEventArgs(UltraGridExporterHelper exporterHelper,
			HeaderBase gridHeader, UltraGridRow gridRow, HeaderTypes headerType): 
			base(exporterHelper)
		{
			this.gridHeader = gridHeader;
			this.gridRow = gridRow;
			this.headerType = headerType;
		}

		/// <summary>
		/// Grid header.
		/// </summary>
		public HeaderBase GridHeader
		{
			get { return this.gridHeader; }
		}

		/// <summary>
		/// Associated grid row.
		/// </summary>
		public UltraGridRow GridRow
		{
			get { return this.gridRow; }
		}	

		/// <summary>
		/// Type of header.
		/// </summary>
		public HeaderTypes HeaderType
		{
			get { return this.headerType; }
		}
    }
    #endregion //HeaderCellExportedEventArgs

    #region SummaryCellExportingEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.SummaryCellExporting"/> event.
	/// </summary>
	public class SummaryCellExportingEventArgs: ExcelExportCancelEventArgs
	{
		private SummaryValue summary;
		private int summaryLevel;

		internal SummaryCellExportingEventArgs(UltraGridExporterHelper exporterHelper,
			SummaryValue summary, int summaryLevel): base(exporterHelper)
		{
			this.summary = summary;
			this.summaryLevel = summaryLevel;
		}

		/// <summary>
		/// Summary value.
		/// </summary>
		public SummaryValue Summary
		{
			get { return this.summary; }
		}

		/// <summary>
		/// Summary level.
		/// </summary>
		public int SummaryLevel
		{
			get { return this.summaryLevel; }
		}
    }
    #endregion //SummaryCellExportingEventArgs

    #region SummaryCellExportedEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.SummaryCellExported"/> event.
	/// </summary>
	public class SummaryCellExportedEventArgs: ExcelExportEventArgs
	{
		private SummaryValue summary;
		private int summaryLevel;

		internal SummaryCellExportedEventArgs(UltraGridExporterHelper exporterHelper,
			SummaryValue summary, int summaryLevel): base(exporterHelper)
		{
			this.summary = summary;
			this.summaryLevel = summaryLevel;
		}

		/// <summary>
		/// Summary value.
		/// </summary>
		public SummaryValue Summary
		{
			get { return this.summary; }
		}

		/// <summary>
		/// Summary level.
		/// </summary>
		public int SummaryLevel
		{
			get { return this.summaryLevel; }
		}
    }
    #endregion //SummaryCellExportedEventArgs

    #region EndExportEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.EndExport"/> event.
	/// </summary>
	public class EndExportEventArgs: ExcelExportEventArgs
	{
		private bool canceled;

		internal EndExportEventArgs(UltraGridExporterHelper exporterHelper, bool canceled): 
			base(exporterHelper)
		{
			this.canceled = canceled;
		}

		/// <summary>
		/// True if exporting process was been canceled.
		/// </summary>
		public bool Canceled
		{
			get { return this.canceled; }
		}
    }
    #endregion //EndExportEventArgs

    #region InitializeColumnEventArgs
    /// <summary>
	/// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.InitializeColumn"/> event.
	/// </summary>
	public class InitializeColumnEventArgs: System.EventArgs
	{
		private string frameworkFormatStr, excelFormatStr;
		UltraGridColumn column;

		internal InitializeColumnEventArgs(string frameworkFormatStr, 
			string excelFormatStr, UltraGridColumn column)
		{
			this.frameworkFormatStr = frameworkFormatStr;
			this.excelFormatStr = excelFormatStr;
			this.column = column;
		}

		/// <summary>
		/// Format string used in .NET Framework.
		/// </summary>
		public string FrameworkFormatStr
		{
			get { return this.frameworkFormatStr; }
		}

		/// <summary>
		/// Excel specific format string.
		/// </summary>
		public string ExcelFormatStr
		{
			get { return this.excelFormatStr; }
			set { this.excelFormatStr = value; }
		}

		/// <summary>
		/// Ultra grid column.
		/// </summary>
		public UltraGridColumn Column
		{
			get { return this.column; }
		}
    }
    #endregion //InitializeColumnEventArgs

    // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
    #region NAS v8.3 - Exporting CalcManager Formulas to Excel

    #region FormulaEventArgs
    /// <summary>
    /// Base class for event arguments for events dealing with the exporting of formulas to excel.
    /// </summary>
    public abstract class FormulaEventArgs : EventArgs
    {
        #region Private Members

        private object context;
        private string gridFormula;
        private WorksheetCell worksheetCell;        

        #endregion //Private Members

        #region Constructor

        /// <summary>
        /// Creates a new FormulaEventArgs instance. 
        /// </summary>
        /// <param name="context">The object in the UltraGrid whose formula is being exported. </param>
        /// <param name="gridFormula">The formula of the object in the grid being exported. </param>
        /// <param name="worksheetCell">The worksheet cell to which the formula is being exported.</param>
        public FormulaEventArgs(object context, string gridFormula, WorksheetCell worksheetCell)
        {
            this.context = context;
            this.gridFormula = gridFormula;
            this.worksheetCell = worksheetCell;
        }

        #endregion //Constructor

        #region Public Properties

        #region Context
        /// <summary>
        /// The object in the UltraGrid whose formula is being exported. 
        /// </summary>
        /// <remarks>
        /// <para class="body">This will either be an <see cref="Infragistics.Win.UltraWinGrid.UltraGridCell"/> or an <see cref="Infragistics.Win.UltraWinGrid.SummaryValue"/> object.</para>
        /// </remarks>
        public object Context
        {
            get { return this.context; }
        }
        #endregion //Context

        #region GridFormula
        /// <summary>
        /// The formula of the object in the grid being exported. 
        /// </summary>
        /// <remarks>
        /// <para class="body">This property returns the formula string applied to the column or summary in the grid being exported.</para>
        /// </remarks>
        public string GridFormula
        {
            get { return this.gridFormula; }
        }
        #endregion //GridFormula

        #region WorksheetCell
        /// <summary>
        /// The worksheet cell to which the formula is being exported.
        /// </summary>
        public WorksheetCell WorksheetCell
        {
            get { return this.worksheetCell; }
        }
        #endregion //WorksheetCell

        #endregion //Public Properties
    }
    #endregion //FormulaEventArgs
       

    #region FormulaExportingEventArgs
    /// <summary>
    /// Event arguments for the FormulaExporting event.
    /// </summary>
    public class FormulaExportingEventArgs : FormulaEventArgs
    {
        #region Private Members

        private FormulaExportAction action;

        #endregion //Private Members

        #region Constructor

        /// <summary>
        /// Creates a new FormulaEventArgs instance. 
        /// </summary>
        /// <param name="context">The object in the UltraGrid whose formula is being exported. </param>
        /// <param name="gridFormula">The formula of the object in the grid being exported.</param>
        /// <param name="worksheetCell">The worksheet cell to which the formula is being exported.</param>
        /// <param name="action">Determines what action to take when exporting the formula.</param>
        public FormulaExportingEventArgs(object context, string gridFormula, WorksheetCell worksheetCell, FormulaExportAction action) 
        : base(context, gridFormula, worksheetCell)
        {
            this.action = action;
        }

        #endregion //Constructor

        #region Public Properties

        #region Action
        /// <summary>
        /// Determines what action to take when exporting a formula. 
        /// </summary>
        public FormulaExportAction Action
        {
            get { return this.action; }
            set { this.action = value; }
        }
        #endregion //Action        

        #endregion //Public Properties
    }
    #endregion //FormulaExportingEventArgs

    #region FormulaExportedEventArgs
    /// <summary>
    /// Event args for the FormulaExported event.
    /// </summary>
    public class FormulaExportedEventArgs : FormulaEventArgs
    {
        #region Constructor

        /// <summary>
        /// Creates a new FormulaEventArgs instance. 
        /// </summary>
        /// <param name="context">The object in the UltraGrid whose formula is being exported. </param>
        /// <param name="gridFormula">The formula of the object in the grid being exported.</param>
        /// <param name="worksheetCell">The worksheet cell to which the formula is being exported.</param>
        public FormulaExportedEventArgs(object context, string gridFormula, WorksheetCell worksheetCell)
            : base(context, gridFormula, worksheetCell)
        {
        }

        #endregion //Constructor
    }
    #endregion //FormulaExportedEventArgs

    #region FormulaExportErrorEventArgs
    /// <summary>
    /// Event arguments for the FormulaExportingError event.
    /// </summary>
    public class FormulaExportErrorEventArgs : FormulaEventArgs
    {
        #region Private Members

        private FormulaExportErrorAction action;
        private FormulaExportErrorType error;
        private string errorText;
        private string excelFormula;
        private bool applyInvalidFormulaIfPossible = false;

        #endregion //Private Members

        #region Constructor

        internal FormulaExportErrorEventArgs(object context, string gridFormula, WorksheetCell worksheetCell,
            FormulaExportErrorAction action, Exception exception, string excelformula )
            : base(context, gridFormula, worksheetCell)
        {
            this.action = action;
            FormulaExportException formulaExportException = exception as FormulaExportException;
            if (formulaExportException != null)
                this.error = formulaExportException.Error;
            else
                this.error = FormulaExportErrorType.InvalidFormula;

            this.errorText = exception.Message;

            this.excelFormula = excelformula;            
        }

        /// <summary>
        /// Creates a new FormulaEventArgs instance. 
        /// </summary>
        /// <param name="context">The object in the UltraGrid whose formula is being exported. </param>
        /// <param name="gridFormula">The formula of the object in the grid being exported.</param>
        /// <param name="worksheetCell">The worksheet cell to which the formula is being exported.</param>
        /// <param name="action">Determines what action to take when exporting the formula to the cell.</param>
        /// <param name="error">An enumeration value that describes the error.</param>
        /// <param name="errorText">A string with details about the error.</param>
        /// <param name="excelFormula">The converted formula to apply to the worksheet cell. May be null.</param>
        public FormulaExportErrorEventArgs(object context, string gridFormula, WorksheetCell worksheetCell,
            FormulaExportErrorAction action, FormulaExportErrorType error, string errorText, string excelFormula)
            : base(context, gridFormula, worksheetCell)
        {
            this.action = action;
            this.error = error;
            this.errorText = errorText;
            this.excelFormula = excelFormula;
        }

        #endregion //Constructor

        #region Public Properties

        #region Action
        /// <summary>
        /// Determines what action to take when exporting a formula. 
        /// </summary>
        public FormulaExportErrorAction Action
        {
            get { return this.action; }
            set { this.action = value; }
        }
        #endregion //Action        

        #region Error
        /// <summary>
        /// An enum returning the type of error that occurred when converting the formula from the grid to Excel. 
        /// </summary>
        /// <remarks>
        /// <para class="body">For details on the error, see the ErrorText property.</para>
        /// </remarks>
        /// <see cref="ErrorText"/>
        public FormulaExportErrorType Error
        {
            get
            {
                return this.error;
            }
        }
        #endregion //Error

        #region ErrorText
        /// <summary>
        /// Gets or sets the error message if there was an error when converting a grid formula to an equivalent Excel formula. 
        /// </summary>
        /// <remarks>
        /// <para class="body">This property will return a string with a description of any errors that may have occurred when attempting to convert a grid formula to an equivalent Excel formula. To determine the type of the error, see the <see cref="Error"/> property.</para>
        /// <para class="body">This property is settable so that when the Action is set to WriteErrorMessageToWorksheetCell, you can specify custom error text.</para>
        /// </remarks>
        /// <see cref="Error"/>
        public string ErrorText
        {
            get { return this.errorText; }
            set { this.errorText = value; }
        }
        #endregion //ErrorText

        #region ExcelFormula
        /// <summary>
        /// Gets or sets the converted formula to apply to the worksheet cell. May be null.
        /// </summary>
        /// <remarks>
        /// <para class="body">This property returns the formula converted to an equivalent invalid formula in Excel. This property may return null if the formula could not be converted.</para>
        /// <para class="body">This property may be set so that you can correct the invalid formula. It will not be applied to the cell, however, unless <see cref="ApplyInvalidFormulaIfPossible"/> is set ot true.</para>
        /// </remarks>
        public string ExcelFormula
        {
            get { return this.excelFormula; }
            set { this.excelFormula = value; }
        }
        #endregion //ExcelFormula

        #region ApplyInvalidFormulaIfPossible
        /// <summary>
        /// When set to true, the UltraGridExcelExporter will attempt to write the <see cref="ExcelFormula"/> to the worksheet cell, even though it may be invalid.
        /// </summary>
        /// <remarks>
        /// <para class="body">If this property is set to true and then formula fails to apply to the cell, the <see cref="Action"/> property will be used to determine how to proceed.</para>
        /// </remarks>
        public bool ApplyInvalidFormulaIfPossible
        {
            get { return this.applyInvalidFormulaIfPossible; }
            set { this.applyInvalidFormulaIfPossible = value; }
        }
        #endregion //ApplyInvalidFormulaIfPossible

        #endregion //Public Properties
    }
    #endregion //FormulaExportErrorEventArgs


    #region InitializeSummaryEventArgs
    /// <summary>
    /// Event parameters used for the <see cref="Infragistics.Win.UltraWinGrid.ExcelExport.UltraGridExcelExporter.InitializeSummary"/> event.
    /// </summary>
    public class InitializeSummaryEventArgs : System.EventArgs
    {
        private string frameworkFormatStr, excelFormatStr;
        SummarySettings summary;

        internal InitializeSummaryEventArgs(string frameworkFormatStr,
            string excelFormatStr, SummarySettings summary)
        {
            this.frameworkFormatStr = frameworkFormatStr;
            this.excelFormatStr = excelFormatStr;
            this.summary = summary;
        }

        /// <summary>
        /// Format string used in .NET Framework.
        /// </summary>
        public string FrameworkFormatStr
        {
            get { return this.frameworkFormatStr; }
        }

        /// <summary>
        /// Excel specific format string.
        /// </summary>
        public string ExcelFormatStr
        {
            get { return this.excelFormatStr; }
            set { this.excelFormatStr = value; }
        }

        /// <summary>
        /// Ultra grid summary settings.
        /// </summary>
        public SummarySettings Summary
        {
            get { return this.summary; }
        }
    }
    #endregion //InitializeSummaryEventArgs

    #endregion //NAS v8.3 - Exporting CalcManager Formulas to Excel

    #endregion Event Args
}
