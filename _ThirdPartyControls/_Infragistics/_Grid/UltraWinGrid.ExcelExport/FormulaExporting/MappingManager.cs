#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved


#region XPS License
/*----------------------------------------------------------------------*
* Note: To the best of our knowledge, Microsoft® doesn’t forbid         *
* third-party companies from developing a product for the XPS document  *
* generation. However, according to the XML Paper Specification Patent  *
* License, we need to provide the following notice for your information:* 
*                                                                       *
* This product may incorporate intellectual property owned by Microsoft *
* Corporation. The terms and conditions upon which Microsoft is         *
* licensing such intellectual property may be found at                  *
* http://go.microsoft.com/fwlink/?LinkId=52369.;                        *
* ----------------------------------------------------------------------*
*/		
#endregion XPS License

using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Excel;
using Infragistics.Win.CalcEngine;
using System.Diagnostics;
using System.Collections;

namespace Infragistics.Win.UltraWinGrid.ExcelExport.FormulaExporting
{
    #region MappingManager class
    internal class MappingManager
    {
        #region Private Members
        private UltraGridLayout exportLayout;
        private Dictionary<CellInfo, WorksheetCell> cellMappings;
        private Dictionary<SummaryInfo, WorksheetCell> summaryMappings;
        private List<CellInfo> formulaTargetCells;
        private List<SummaryInfo> formulaTargetSummaryCells;
        private Dictionary<IUltraCalcFormula, IUltraCalcFormulaToken[]> formulaTokens;
        private Dictionary<UltraGridRow, UltraGridRow> realRowsToExportRows;
        private Dictionary<UltraGridRow, UltraGridRow> exportRowToRealRows;

        private Dictionary<UltraGridColumn, UltraGridColumn> exportColumnsToRealColumns;
        private Dictionary<SummaryValue, SummaryValue> exportSummaryValueToRealSummaryValue;

        private static WorksheetCellComparer worksheetCellComparer;

        private Workbook workbook;
        private HiddenWorksheetManager hiddenWorksheetManager;
        
        private Dictionary<UltraGridRow, bool> exportedRows;

        private Dictionary<FormulaTargetInfo, bool> cancelledFormulas;
        private Dictionary<NamedReferenceConstant, NamedReference> namedReferences;        

        public const string HIDDEN_WORKSHEET_NAME = "Calc_Helper";

        // MRS 5/21/2009 - TFS17817
        private Dictionary<SummaryValue, WorksheetCell> summaryValueMappings;                

        #endregion //Private Members

        #region Constructor
        public MappingManager(UltraGridLayout exportLayout, Workbook workbook)
        {
            this.exportLayout = exportLayout;
            this.workbook = workbook;
        }
        #endregion //Constructor

        #region Private Properties

        #region CancelledFormulas
        // Keeps track of all formulas whose action was set to CancelAll.
        private Dictionary<FormulaTargetInfo, bool> CancelledFormulas
        {
            get
            {
                if (this.cancelledFormulas == null)
                    this.cancelledFormulas = new Dictionary<FormulaTargetInfo, bool>();

                return this.cancelledFormulas;
            }
        }
        #endregion //CancelledFormulas

        #region CellMappings
        private Dictionary<CellInfo, WorksheetCell> CellMappings
        {
            get
            {
                if (this.cellMappings == null)
                    this.cellMappings = new Dictionary<CellInfo, WorksheetCell>();

                return this.cellMappings;
            }
        }
        #endregion //CellMappings        

        #region ExportedRows
        // Keeps track of all rows that exist in the exported excel file. 
        // This is so when we build column data in a hidden work sheet, we can exclude
        // rows that were cancelled. 
        private Dictionary<UltraGridRow, bool> ExportedRows
        {
            get
            {
                if (this.exportedRows == null)
                    this.exportedRows = new Dictionary<UltraGridRow, bool>();

                return this.exportedRows;
            }
        }
        #endregion //ExportedRows

        #region ExportRowToRealRows
        private Dictionary<UltraGridRow, UltraGridRow> ExportRowToRealRows
        {
            get
            {
                if (this.exportRowToRealRows == null)
                    this.exportRowToRealRows = new Dictionary<UltraGridRow, UltraGridRow>();

                return this.exportRowToRealRows;
            }
        }
        #endregion //ExportRowToRealRows

        #region ExportColumnsToRealColumns
        private Dictionary<UltraGridColumn, UltraGridColumn> ExportColumnsToRealColumns
        {
            get
            {
                if (this.exportColumnsToRealColumns == null)
                    this.exportColumnsToRealColumns = new Dictionary<UltraGridColumn, UltraGridColumn>();

                return this.exportColumnsToRealColumns;
            }
        }
        #endregion //ExportColumnsToRealColumns

        #region ExportSummaryValueToRealSummaryValue
        private Dictionary<SummaryValue, SummaryValue> ExportSummaryValueToRealSummaryValue
        {
            get
            {
                if (this.exportSummaryValueToRealSummaryValue == null)
                    this.exportSummaryValueToRealSummaryValue = new Dictionary<SummaryValue, SummaryValue>();

                return this.exportSummaryValueToRealSummaryValue;
            }
        }
        #endregion //ExportSummaryValueToRealSummaryValue

        #region FormulaTargetCells
        internal List<CellInfo> FormulaTargetCells
        {
            get
            {
                if (this.formulaTargetCells == null)
                    this.formulaTargetCells = new List<CellInfo>();

                return this.formulaTargetCells;
            }
        }
        #endregion //FormulaTargetCells

        #region FormulaTargetSummaryCells
        private List<SummaryInfo> FormulaTargetSummaryCells
        {
            get
            {
                if (this.formulaTargetSummaryCells == null)
                    this.formulaTargetSummaryCells = new List<SummaryInfo>();

                return this.formulaTargetSummaryCells;
            }
        }
        #endregion //FormulaTargetSummaryCells

        #region FormulaTokens
        private Dictionary<IUltraCalcFormula, IUltraCalcFormulaToken[]> FormulaTokens
        {
            get
            {
                if (this.formulaTokens == null)
                    this.formulaTokens = new Dictionary<IUltraCalcFormula, IUltraCalcFormulaToken[]>();

                return this.formulaTokens;
            }
        }
        #endregion //FormulaTokens        

        #region HasFormulaTargetCells
        public bool HasFormulaTargetCells
        {
            get
            {
                return this.formulaTargetCells != null &&
                    this.formulaTargetCells.Count > 0;
            }
        }
        #endregion //HasFormulaTargetCells

        #region HasFormulaTargetSummaryCells
        public bool HasFormulaTargetSummaryCells
        {
            get
            {
                return this.formulaTargetSummaryCells != null &&
                    this.formulaTargetSummaryCells.Count > 0;
            }
        }
        #endregion //HasFormulaTargetSummaryCells

        #region HiddenWorksheetManager
        private HiddenWorksheetManager HiddenWorksheetManager
        {
            get 
            {
                if (this.hiddenWorksheetManager == null)
                    this.hiddenWorksheetManager = new HiddenWorksheetManager(this, this.workbook);

                return this.hiddenWorksheetManager;
            }
        }
        #endregion //HiddenWorksheetManager        

        #region NamedReferences
        // Keeps track of all the NamedReferences created by the exporter.
        private Dictionary<NamedReferenceConstant, NamedReference> NamedReferences
        {
            get
            {
                if (this.namedReferences == null)
                    this.namedReferences = new Dictionary<NamedReferenceConstant, NamedReference>();

                return this.namedReferences;
            }
        }
        #endregion //NamedReferences

        #region RealRowsToExportRows
        private Dictionary<UltraGridRow, UltraGridRow> RealRowsToExportRows
        {
            get
            {
                if (this.realRowsToExportRows == null)
                    this.realRowsToExportRows = new Dictionary<UltraGridRow, UltraGridRow>();

                return this.realRowsToExportRows;
            }
        }
        #endregion //RealRowsToExportRows

        #region SummaryMappings
        private Dictionary<SummaryInfo, WorksheetCell> SummaryMappings
        {
            get
            {
                if (this.summaryMappings == null)
                    this.summaryMappings = new Dictionary<SummaryInfo, WorksheetCell>();

                return this.summaryMappings;
            }
        }
        #endregion //SummaryMappings

        #region SummaryValueMappings
        private Dictionary<SummaryValue, WorksheetCell> SummaryValueMappings
        {
            get
            {
                if (this.summaryValueMappings == null)
                    this.summaryValueMappings = new Dictionary<SummaryValue, WorksheetCell>();

                return this.summaryValueMappings;
            }
        }
        #endregion //SummaryValueMappings

        #region WorksheetCellComparer
        internal static WorksheetCellComparer WorksheetCellComparer
        {
            get
            {
                if (MappingManager.worksheetCellComparer == null)
                    MappingManager.worksheetCellComparer = new WorksheetCellComparer();

                return MappingManager.worksheetCellComparer;
            }
        }
        #endregion //WorksheetCellComparer

        #endregion //Private Properties

        #region Private Methods

        #region GetGrid
        public static UltraGrid GetGrid(UltraGridCell cell)
        {
            if (cell == null)
                return null;

            return GetGrid(cell.Column);
        }

        public static UltraGrid GetGrid(UltraGridColumn column)
        {
            if (column == null)
                return null;

            return GetGrid(column.Layout);
        }

        public static UltraGrid GetGrid(UltraGridRow row)
        {
            if (row == null)
                return null;

            return GetGrid(row.Band);
        }

        private UltraGrid GetGrid(SummaryValue exportSummaryValue)
        {
            return GetGrid(exportSummaryValue.ParentRows);
        }

        private UltraGrid GetGrid(RowsCollection rowsCollection)
        {
            return GetGrid(rowsCollection.Band);
        }

        public static UltraGrid GetGrid(UltraGridBand band)
        {
            return GetGrid(band.Layout);
        }

        public static UltraGrid GetGrid(UltraGridLayout ultraGridLayout)
        {
            if (ultraGridLayout == null)
                return null;

            return GetGrid(ultraGridLayout.Grid);
        }

        public static UltraGrid GetGrid(UltraGridBase ultraGridBase)
        {
            return ultraGridBase as UltraGrid;
        }
        #endregion //GetGrid

        // MRS 5/21/2009 - TFS17817
        #region GetSummaryValueFromExportSummaryValue
        private static SummaryValue GetSummaryValueFromExportSummaryValue(SummaryValue exportSummaryValue)
        {
            SummarySettings exportSummarySettings = exportSummaryValue.SummarySettings;
            UltraGrid grid = (UltraGrid)exportSummarySettings.Band.Layout.Grid;
            SummarySettings realSummarySettings = grid.GetSummarySettingsFromPrintSummarySettings(exportSummarySettings);
            RowsCollection realRows = grid.GetRowsFromPrintRows(exportSummaryValue.ParentRows, true);
            SummaryValue realSummaryValue = null;
            foreach (SummaryValue summaryValue in realRows.SummaryValues)
            {
                if (summaryValue.SummarySettings == realSummarySettings)
                {
                    realSummaryValue = summaryValue;
                    break;
                }
            }
            return realSummaryValue;
        }
        #endregion // GetSummaryValueFromExportSummaryValue

        #endregion //Private Methods

        #region Public Methods

        #region ClearFormulaTargetCells
        public void ClearFormulaTargetCells()
        {
            this.FormulaTargetCells.Clear();
        }
        #endregion //ClearFormulaTargetCells

        #region ClearFormulaTargetSummaryCells
        public void ClearFormulaTargetSummaryCells()
        {
            this.FormulaTargetSummaryCells.Clear();
        }
        #endregion //ClearFormulaTargetSummaryCells

        #region GetExportCell
        public UltraGridCell GetExportCell(UltraGridCell realGridCell)
        {
            UltraGridRow exportRow = this.GetExportRow(realGridCell.Row);            
            return exportRow.Cells[realGridCell.Column.Key];            
        }
        #endregion //GetExportCell

        #region GetExportRow
        public UltraGridRow GetExportRow(UltraGridRow realGridRow)
        {
            UltraGridRow exportRow;
            bool success = this.RealRowsToExportRows.TryGetValue(realGridRow, out exportRow);

            if (success)
                return exportRow;

            UltraGrid grid = GetGrid(realGridRow);
            if (grid == null)
            {
                Debug.Fail("Failed to get the grid from the row; unexpected");
                return null;
            }

            exportRow = grid.GetPrintRowFromRow(realGridRow, this.exportLayout, true);                        

            this.ExportRowToRealRows[exportRow] = realGridRow;
            this.RealRowsToExportRows[realGridRow] = exportRow;

            return exportRow;
        }
        #endregion //GetExportRow        

        #region GetIsFormulaCancelled

        public bool GetIsFormulaCancelled(FormulaTargetInfo formulaTargetInfo)
        {
            bool cancelled = false;
            bool success = this.CancelledFormulas.TryGetValue(formulaTargetInfo, out cancelled);

            return cancelled;
        }

        #endregion //GetIsFormulaCancelled

        #region GetFormulaTargetCells
        public CellInfo[] GetFormulaTargetCells()
        {
            return this.FormulaTargetCells.ToArray();
        }
        #endregion //GetFormulaTargetCells

        #region GetFormulaTargetSummaryCells
        public SummaryInfo[] GetFormulaTargetSummaryCells()
        {
            return this.FormulaTargetSummaryCells.ToArray();
        }
        #endregion //GetFormulaTargetSummaryCells

        #region GetFormulaTokens
        public IUltraCalcFormulaToken[] GetFormulaTokens(IUltraCalcFormula ultraCalcFormula)
        {
            IUltraCalcFormulaToken[] ultraCalcFormulaTokens;
            bool success = this.FormulaTokens.TryGetValue(ultraCalcFormula, out ultraCalcFormulaTokens);
            if (success)
                return ultraCalcFormulaTokens;

            ultraCalcFormulaTokens = ultraCalcFormula.GetTokens();
            this.FormulaTokens[ultraCalcFormula] = ultraCalcFormulaTokens;
            return ultraCalcFormulaTokens;
        }
        #endregion //GetFormulaTokens

        #region GetNamedReference
        public NamedReference GetNamedReference(NamedReferenceConstant namedReferenceConstant)
        {
            NamedReference namedReference;
            bool success = this.NamedReferences.TryGetValue(namedReferenceConstant, out namedReference);
            if (success)
                return namedReference;

            string namedReferenceName = namedReferenceConstant.ToString();
            int i = 0;
            while(this.workbook.NamedReferences.Find(namedReferenceName) != null)
            {
                namedReferenceName = string.Format("{0} {1}", namedReferenceConstant.ToString(), i.ToString());
                i++;
            }
            int value = ((int)namedReferenceConstant);
            string formula = string.Format("={0}", value.ToString());
            namedReference = this.workbook.NamedReferences.Add(namedReferenceName, formula);
            this.NamedReferences[namedReferenceConstant] = namedReference;

            return namedReference;
        }
        #endregion //GetNamedReference

        #region GetRealGridCell
        public UltraGridCell GetRealGridCell(UltraGridCell exportGridCell)
        {
            UltraGridRow realGridRow = this.GetRealGridRow(exportGridCell.Row);
            return realGridRow.Cells[exportGridCell.Column.Key];
        }

        #endregion //GetRealGridCell

        #region GetRealGridColumn
        public UltraGridColumn GetRealGridColumn(UltraGridColumn exportLayoutColumn)
        {
            UltraGridColumn realGridColumn;
            bool success = this.ExportColumnsToRealColumns.TryGetValue(exportLayoutColumn, out realGridColumn);
            if (success)
                return realGridColumn;

            UltraGrid grid = GetGrid(exportLayoutColumn);
            if (grid == null)
            {
                Debug.Fail("Failed to get the grid from the column in the export layout; unexpected");
                return null;
            }

            realGridColumn = grid.GetColumnFromPrintColumn(exportLayoutColumn);
            this.ExportColumnsToRealColumns[exportLayoutColumn] = realGridColumn;
            return realGridColumn;
        }
        #endregion //GetRealGridColumn       

        #region GetRealGridRow
        public UltraGridRow GetRealGridRow(UltraGridRow exportGridRow)
        {
            UltraGridRow realGridRow;
            bool success = this.ExportRowToRealRows.TryGetValue(exportGridRow, out realGridRow);
            if (success)
                return realGridRow;

            UltraGrid grid = GetGrid(exportGridRow);
            if (grid == null)
            {
                Debug.Fail("Failed to get the grid from the row in the export layout; unexpected");
                return null;
            }

            realGridRow = grid.GetRowFromPrintRow(exportGridRow);            
            this.ExportRowToRealRows[exportGridRow] = realGridRow;

            this.RealRowsToExportRows[realGridRow] = exportGridRow;
            return realGridRow;
        }

        #endregion //GetRealGridRow

        #region GetRealGridSummaryValue
        public SummaryValue GetRealGridSummaryValue(SummaryValue exportSummaryValue)
        {
            SummaryValue realGridSummaryValue;
            bool success = this.ExportSummaryValueToRealSummaryValue.TryGetValue(exportSummaryValue, out realGridSummaryValue);
            if (success)
                return realGridSummaryValue;

            UltraGrid grid = GetGrid(exportSummaryValue);
            if (grid == null)
            {
                Debug.Fail("Failed to get the grid from the column in the export layout; unexpected");
                return null;
            }

            RowsCollection exportRowsCollection = exportSummaryValue.ParentRows;
            RowsCollection realGridRowsCollection = grid.GetRowsFromPrintRows(exportRowsCollection, true);
            int index = exportRowsCollection.SummaryValues.IndexOf(exportSummaryValue);
            realGridSummaryValue = realGridRowsCollection.SummaryValues[index];
            this.ExportSummaryValueToRealSummaryValue[exportSummaryValue] = realGridSummaryValue;
            return realGridSummaryValue;
        }        
        #endregion //GetRealGridSummaryValue

        #region GetWorksheetCell(s)

        public WorksheetCell GetWorksheetCell(UltraGridCell cell)
        {
            // The excel cells are mapped to the export cell (not the real grid cells). 
            // So if the cell passed in here is a "real" grid cell, get the export cell. 
            if (cell.Band.Layout.IsExportLayout == false)
                cell = GetExportCell(cell);

            return this.GetWorksheetCell(cell.Column, cell.Row);
        }

        public WorksheetCell GetWorksheetCell(UltraGridColumn gridColumn, UltraGridRow gridRow)
        {
            return this.GetWorksheetCell(new CellInfo(gridColumn, gridRow));
        }

        public WorksheetCell GetWorksheetCell(CellInfo cellInfo)
        {
            WorksheetCell worksheetCell;            
            bool success = this.CellMappings.TryGetValue(cellInfo, out worksheetCell);
            if (success)
                return worksheetCell;

            // Maybe the column is hidden
            if (cellInfo.GridColumn.Hidden == true)
            {
                worksheetCell = this.HiddenWorksheetManager.GetHiddenWorksheetCell(cellInfo);
            }
            else
            {
                Debug.Fail("Failed to find the worksheet cell; unexpected");
                worksheetCell = null;
            }

            return worksheetCell;
        }

        public WorksheetCellRangeInfo GetWorksheetCells(IUltraCalcReference reference)            
        {
            if (reference.IsEnumerable == false)
            {
                Debug.Fail("GetWorksheetCells called with a non-enumerable reference; unexpected");
                return null;
            }

            // Build a list of the real grid cells that make up the reference. 
            List<UltraGridCell> cells = new List<UltraGridCell>();            
            foreach (IUltraCalcReference subReference in reference.References)
            {
                UltraGridCell gridCell = subReference.Context as UltraGridCell;
                if (gridCell == null)
                    Debug.Fail("Enumerable reference contains a reference that is not a cell; unexpected.");
                else
                {
                    if (this.WasRowExported(gridCell.Row))                    
                        cells.Add(gridCell);
                }
            }

            return GetWorksheetCells(cells);

        }
        public WorksheetCellRangeInfo GetWorksheetCells(List<UltraGridCell> cells)
        {
            // Determine if the list is contiguous in the excel sheet. 
            bool isContiguous = true;
                        
            // Get all of the WorksheetCells that correspond to the grid cells.
            UltraGridColumn column = null;
            List<WorksheetCell> worksheetCells = new List<WorksheetCell>();
            foreach (UltraGridCell cell in cells)
            {                
                WorksheetCell worksheetCell = this.GetWorksheetCell(cell);
                worksheetCells.Add(worksheetCell);                

                if (isContiguous)
                {
                    // If we think the list might be contiguous at this point, check to make sure
                    // that every cell is in the same column. 
                    if (column == null)
                        column = cell.Column;
                    else if (column != cell.Column)
                        isContiguous = false;
                }
            }
        
            // If we still think the data is contiguous, sort the Worksheet cells by row index
            // and make sure there are no gaps. 
            if (isContiguous && worksheetCells.Count > 0)
            {
                worksheetCells.Sort(MappingManager.WorksheetCellComparer);
                int rowIndex = worksheetCells[0].RowIndex;
                for (int i = 1; i < worksheetCells.Count; i++)
                {
                    WorksheetCell worksheetCell = worksheetCells[i];
                    if (worksheetCell.RowIndex != rowIndex + 1)
                    {
                        isContiguous = false;
                        break;
                    }

                    rowIndex = worksheetCell.RowIndex;
                }
            }

            // Build the WorksheetCellRangeInfo
            WorksheetCellRangeInfo worksheetCellRangeInfo;
            if (isContiguous)
            {
                // If the cells are contiguous, we can use the real cells in the worksheet.
                worksheetCellRangeInfo = new WorksheetCellRangeInfo(cells.Count);
                foreach (WorksheetCell worksheetCell in worksheetCells)
                {
                    worksheetCellRangeInfo.AddWorksheetCell(worksheetCell);
                }
            }
            else
            {
                // If the cells are not contiguous, we need to use a hidden worksheet
                // and refer to a contiguous range of cells there. 
                worksheetCellRangeInfo = this.HiddenWorksheetManager.GetHiddenWorksheetCells(column, worksheetCells);
            }

            return worksheetCellRangeInfo;
        }

        internal WorksheetCell GetWorksheetCell(SummaryInfo summaryInfo)
        {
            WorksheetCell worksheetCell;
            bool success = this.SummaryMappings.TryGetValue(summaryInfo, out worksheetCell);

            return (success)
                ? worksheetCell
                : null;
        }

        // MRS 5/21/2009 - TFS17817
        internal WorksheetCell GetWorksheetCell(SummaryValue summaryValue)
        {
            WorksheetCell worksheetCell;
            bool success = this.SummaryValueMappings.TryGetValue(summaryValue, out worksheetCell);

            return (success)
                ? worksheetCell
                : null;
        }
        #endregion //GetWorksheetCell(s)

        #region MapWorksheetCell
        public void MapWorksheetCell(UltraGridColumn gridColumn, UltraGridRow gridRow, WorksheetCell workSheetCell)
        {
            if (gridColumn.Band.Layout.IsExportLayout == false)
                Debug.Fail("The mapping should be to an export row, not a real grid row");

            CellInfo cellInfo = new CellInfo(gridColumn, gridRow);
            this.CellMappings[cellInfo] = workSheetCell;

            if (gridColumn.Formula != null &&
                gridColumn.Formula.Length > 0)
            {
                this.FormulaTargetCells.Add(cellInfo);
            }

            this.ExportedRows[gridRow] = true;
        }

        public void MapWorksheetCell(SummaryInfo summaryInfo, WorksheetCell worksheetCell)
        {
            if (summaryInfo.SummaryValue.SummarySettings.Band.Layout.IsExportLayout == false)
                Debug.Fail("The mapping should be to an export row, not a real grid row");

            this.SummaryMappings[summaryInfo] = worksheetCell;

            // MRS 5/21/2009 - TFS17817
            // Map the SummaryValue to the WorksheetCell. We don't need a SummaryRow context
            // here, because the location of the summary is irrelevant - they will all have the 
            // same value. 
            // Note that we need to map the REAL SummaryValue here, not the export one, because the 
            // CalcReference context only has the real one and there's no way to get from the 
            // real one to the export one. 
            //
            SummaryValue exportSummaryValue = summaryInfo.SummaryValue;
            SummaryValue realSummaryValue = MappingManager.GetSummaryValueFromExportSummaryValue(exportSummaryValue);

            // MRS 8/18/2009 - TFS20929
            // Added null check. The export layout might have summaries that the real layout does not;
            //
            if (realSummaryValue != null)
                this.SummaryValueMappings[realSummaryValue] = worksheetCell;
            
            SummarySettings summarySettings = summaryInfo.SummaryValue.SummarySettings;
            if (summarySettings.Formula != null &&
                summarySettings.Formula.Length > 0)
            {
                this.FormulaTargetSummaryCells.Add(summaryInfo);
            }
        }
               
        #endregion //MapWorksheetCell

        #region SetIsFormulaCancelled

        public void SetIsFormulaCancelled(FormulaTargetInfo formulaTargetInfo)
        {
            this.CancelledFormulas[formulaTargetInfo] = true;
        }
        #endregion //SetIsFormulaCancelled

        #region WasRowExported

        // Returns whether or not the row was actually exported to Excel (not cancelled).
        public bool WasRowExported(UltraGridRow row)
        {
            bool wasRowExported;
            if (row.Band.Layout.IsExportLayout == false)
                row = this.GetExportRow(row);            

            bool success = this.ExportedRows.TryGetValue(row, out wasRowExported);
            return success && wasRowExported;
        }
        #endregion //WasRowExported

        #endregion //Public Methods

    }
    #endregion //MappingManager class

    #region FormulaTargetInfo class
    internal abstract class FormulaTargetInfo
    {
        #region Constructor
        public FormulaTargetInfo()
        {
        }
        #endregion //Constructor

        #region Public Properties        
        public abstract string FormulaString { get; }
        #endregion //Public Properties
    }
    #endregion //CellInfo class

    #region CellInfo class
    internal class CellInfo : FormulaTargetInfo
    {
        #region Private Members
        private UltraGridColumn gridColumn;
        private UltraGridRow gridRow;
        #endregion //Private Members

        #region Constructor
        public CellInfo(UltraGridColumn gridColumn, UltraGridRow gridRow)
        {
            this.gridColumn = gridColumn;
            this.gridRow = gridRow;
        }
        #endregion //Constructor

        #region Public Properties
        public UltraGridColumn GridColumn { get { return this.gridColumn; } }
        public UltraGridRow GridRow { get { return this.gridRow; } }        
        #endregion //Public Properties

        #region Methods

        #region GetCell
        public UltraGridCell GetCell()
        {
            return this.gridRow.Cells[this.gridColumn];
        }
        #endregion //GetCell

        #endregion //Methods

        #region Overrides

        #region FormulaString
        public override string FormulaString { get { return this.gridColumn.Formula; } }
        #endregion //FormulaString

        #region Equals
        public override bool Equals(object obj)
        {
            CellInfo cellMappingInfo = obj as CellInfo;
            if (cellMappingInfo == null) 
                return false;

            return (this.gridColumn == cellMappingInfo.gridColumn &&
                this.gridRow == cellMappingInfo.gridRow);
        }
        #endregion //Equals

        #region GetHashCode
        public override int GetHashCode()
        {
            return this.GridColumn.GetHashCode() |
                this.gridRow.GetHashCode() << 16;
        }
        #endregion //GetHashCode

        #endregion //Overrides        
    }
    #endregion //CellInfo class

    #region SummaryInfo class
    internal class SummaryInfo : FormulaTargetInfo
    {
        #region Private Members
        private SummaryValue summaryValue;
        private UltraGridSummaryRow summaryRow;
        #endregion //Private Members

        #region Constructor
        public SummaryInfo(SummaryValue summaryValue, UltraGridSummaryRow summaryRow)
        {
            this.summaryValue = summaryValue;
            this.summaryRow = summaryRow;
        }
        #endregion //Constructor

        #region Public Properties
        public SummaryValue SummaryValue { get { return this.summaryValue; } }
        public UltraGridSummaryRow SummaryRow { get { return this.summaryRow; } }        
        #endregion //Public Properties

        #region Methods

        #endregion //Methods

        #region Overrides

        #region FormulaString

        public override string FormulaString { get { return this.summaryValue.SummarySettings.Formula; } }

        #endregion //FormulaString

        #region Equals
        public override bool Equals(object obj)
        {
            SummaryInfo summaryMappingInfo = obj as SummaryInfo;
            if (summaryMappingInfo == null)
                return false;

            return (this.summaryValue == summaryMappingInfo.summaryValue &&
                this.summaryRow == summaryMappingInfo.summaryRow);
        }
        #endregion //Equals

        #region GetHashCode
        public override int GetHashCode()
        {
            return this.summaryValue.GetHashCode() |
                this.summaryRow.GetHashCode() << 16;
        }
        #endregion //GetHashCode

        #endregion //Overrides
    }
    #endregion //SummaryInfo class

    #region WorksheetCellRangeInfo class
    internal class WorksheetCellRangeInfo 
    {
        #region Private Members
        private List<WorksheetCell> worksheetCells;
        private bool hasCellsInMultipleWorksheets = false;
        private bool hasCellsInMultipleColumns = false;
        private Worksheet worksheet;
        private int columnIndex = -1;
        #endregion //Private Members

        #region Constructors
        public WorksheetCellRangeInfo() : this(0) { }

        public WorksheetCellRangeInfo(int initialCapacity)
        {
            if (initialCapacity != 0)
                this.worksheetCells = new List<WorksheetCell>(initialCapacity);
        }
        #endregion //Constructors

        #region Private Properties
        
        #region WorksheetCells
        private List<WorksheetCell> WorksheetCells
        {
            get 
            {
                if (this.worksheetCells == null)
                    this.worksheetCells = new List<WorksheetCell>();

                return this.worksheetCells; 
            }
        }
        #endregion //WorksheetCells

        #endregion //Private Properties

        #region Public Properties

        #region HasCellsInMultipleColumns
        public bool HasCellsInMultipleColumns
        {
            get { return this.hasCellsInMultipleColumns; }
        }
        #endregion //HasCellsInMultipleColumns

        #region HasCellsInMultipleWorksheets
        public bool HasCellsInMultipleWorksheets
        {
            get { return this.hasCellsInMultipleWorksheets; }
        }
        #endregion //HasCellsInMultipleWorksheets

        #region IsContiguous
        public bool IsContiguous
        {
            get 
            {
                if (this.hasCellsInMultipleColumns)
                    return false;

                if (this.worksheetCells != null && this.worksheetCells.Count > 0)
                {
                    this.worksheetCells.Sort(MappingManager.WorksheetCellComparer);
                    int rowIndex = this.worksheetCells[0].RowIndex;
                    for (int i = 1; i < this.worksheetCells.Count; i++)
                    {
                        WorksheetCell worksheetCell = this.worksheetCells[i];
                        if (worksheetCell.RowIndex != rowIndex + 1)
                        {
                            return false;                            
                        }

                        rowIndex = worksheetCell.RowIndex;
                    }
                }

                return true; 
            }
        }
        #endregion //IsContiguous

        #region Worksheet
        public Worksheet Worksheet
        {
            get { return this.worksheet; }
        }
        #endregion //Worksheet
        
        #endregion //Public Properties

        #region Public Methods

        #region AddWorksheetCell
        public void AddWorksheetCell(WorksheetCell worksheetCell)
        {
            if (hasCellsInMultipleWorksheets == false)
            {
                if (this.worksheet == null)
                    this.worksheet = worksheetCell.Worksheet;
                else if (worksheetCell.Worksheet != this.Worksheet)
                {
                    this.hasCellsInMultipleWorksheets = true;

                    // If there are cell in multiple worksheets, then there are certainly 
                    // cells in multiple column.
                    this.hasCellsInMultipleColumns = true;
                }
            }

            if (hasCellsInMultipleColumns == false)
            {
                if (this.columnIndex == -1)
                    this.columnIndex = worksheetCell.ColumnIndex;
                else if (worksheetCell.ColumnIndex != this.columnIndex)
                {                    
                    this.hasCellsInMultipleColumns = true;
                }
            }

            this.WorksheetCells.Add(worksheetCell);
        }
        #endregion //AddWorksheetCell

        #region GetWorksheetCells
        public WorksheetCell[] GetWorksheetCells()
        {
            return this.WorksheetCells.ToArray();
        }
        #endregion //GetWorksheetCells        

        #region GetWorksheetRegion
        public WorksheetRegion GetWorksheetRegion()
        {
            if (this.IsContiguous == false)
                throw new InvalidOperationException("Cannot create a WorksheetRegion for non-contiguous cells");

            if (this.WorksheetCells.Count == 0)
                return null;

            this.WorksheetCells.Sort(MappingManager.WorksheetCellComparer);
            WorksheetCell firstWorksheetCell = this.WorksheetCells[0];
            WorksheetCell lastWorksheetCell = this.WorksheetCells[this.WorksheetCells.Count - 1];
            
            WorksheetRegion worksheetRegion = new WorksheetRegion(
                this.Worksheet,
                firstWorksheetCell.RowIndex,
                firstWorksheetCell.ColumnIndex,
                lastWorksheetCell.RowIndex,
                lastWorksheetCell.ColumnIndex);

            return worksheetRegion;
        }
        #endregion //GetWorksheetRegion

        #endregion //Public Methods
    }
    #endregion //WorksheetCellRangeInfo class    

    #region WorksheetCellComparer class
    internal class WorksheetCellComparer : IComparer<WorksheetCell>
    {
        #region IComparer<WorksheetCell> Members

        public int Compare(WorksheetCell x, WorksheetCell y)
        {
            return x.RowIndex.CompareTo(y.RowIndex);
        }

        #endregion
    }
    #endregion //WorksheetCellComparer class

    #region GridRowComparer class
    internal class GridRowComparer : IComparer<UltraGridRow>
    {
        private FormulaRowIndexSource formulaRowIndexSource = FormulaRowIndexSource.VisibleIndex;

        public FormulaRowIndexSource FormulaRowIndexSource
        {
            get { return this.formulaRowIndexSource; }
            set { this.formulaRowIndexSource = value; }
        }

        #region IComparer<UltraGridRow> Members

        public int Compare(UltraGridRow x, UltraGridRow y)
        {
            int xInt;
            int yInt;

            FormulaRowIndexSource formulaRowIndexSource = this.FormulaRowIndexSource;

            while (x.ParentRow != y.ParentRow)
            {
                formulaRowIndexSource = FormulaRowIndexSource.VisibleIndex;

                x = x.ParentRow;
                y = y.ParentRow;
            }

            switch (formulaRowIndexSource)
            {
                case FormulaRowIndexSource.VisibleIndex:
                    xInt = x.VisibleIndex;
                    yInt = y.VisibleIndex;
                    break;
                case FormulaRowIndexSource.RowIndex:
                    xInt = x.Index;
                    yInt = y.Index;
                    break;
                case FormulaRowIndexSource.ListIndex:
                    xInt = x.ListIndex;
                    yInt = y.ListIndex;
                    break;
                case FormulaRowIndexSource.Default:
                    Debug.Fail("The FormulaRowIndexSource cannot be set to default when sorting.");
                    xInt = x.VisibleIndex;
                    yInt = y.VisibleIndex;
                    break;
                default:
                    Debug.Fail("Unknown FormulaRowIndexSource.");
                    xInt = x.VisibleIndex;
                    yInt = y.VisibleIndex;
                    break;
            }            

            return xInt.CompareTo(yInt);
        }

        #endregion        
    }
    #endregion //GridRowComparer class

    #region HiddenWorksheetManager class
    internal class HiddenWorksheetManager
    {
        #region Private Members
        private Workbook workbook;
        private Worksheet hiddenWorkSheet;
        private int lastUsedColumn = -1;
        private Dictionary<UltraGridColumn, HiddenWorksheetColumnInfo> columnToHiddenWorksheetInfo;
        private MappingManager mappingManager;
        private GridRowComparer gridRowComparer;
        #endregion //Private Members

        #region Constructor
        public HiddenWorksheetManager(MappingManager mappingManager, Workbook workbook)
        {
            this.mappingManager = mappingManager;
            this.workbook = workbook;
        }
        #endregion //Constructor        

        #region Private Properties

        #region ColumnToHiddenWorksheetInfo
        private Dictionary<UltraGridColumn, HiddenWorksheetColumnInfo> ColumnToHiddenWorksheetInfo
        {
            get
            {
                if (this.columnToHiddenWorksheetInfo == null)
                    this.columnToHiddenWorksheetInfo = new Dictionary<UltraGridColumn, HiddenWorksheetColumnInfo>();

                return this.columnToHiddenWorksheetInfo;
            }
        }
        #endregion //ColumnToHiddenWorksheetInfo

        #region GridRowComparer
        private GridRowComparer GridRowComparer
        {
            get
            {
                if (this.gridRowComparer == null)
                    this.gridRowComparer = new GridRowComparer();

                return this.gridRowComparer;
            }
        }
        #endregion //GridRowComparer

        #region HiddenWorkSheet
        private Worksheet HiddenWorkSheet
        {
            get
            {
                return this.hiddenWorkSheet;
            }
        }
        #endregion //HiddenWorkSheet        

        #endregion //Private Properties

        #region Private Methods

        #region GetHiddenWorksheetColumnInfo

        private HiddenWorksheetColumnInfo GetHiddenWorksheetColumnInfo(UltraGridColumn column)
        {
            HiddenWorksheetColumnInfo hiddenWorksheetColumnInfo;
            bool success = this.ColumnToHiddenWorksheetInfo.TryGetValue(column, out hiddenWorksheetColumnInfo);
            if (success)
                return hiddenWorksheetColumnInfo;

            hiddenWorksheetColumnInfo = this.BuildHiddenWorksheetColumnInfo(column);
            this.ColumnToHiddenWorksheetInfo[column] = hiddenWorksheetColumnInfo;

            return hiddenWorksheetColumnInfo;
        }

        private HiddenWorksheetColumnInfo BuildHiddenWorksheetColumnInfo(UltraGridColumn column)
        {
            if (this.hiddenWorkSheet == null)
            {
                this.hiddenWorkSheet = this.workbook.Worksheets.Add(MappingManager.HIDDEN_WORKSHEET_NAME);               
                this.HiddenWorkSheet.DisplayOptions.Visibility = WorksheetVisibility.Hidden;
            }

            UltraGridBand band = column.Band;
            this.GridRowComparer.FormulaRowIndexSource = band.FormulaRowIndexSourceResolved;
            UltraGridLayout layout = band.Layout;

            this.lastUsedColumn += 1;

            HiddenWorksheetColumnInfo hiddenWorksheetColumnInfo = new HiddenWorksheetColumnInfo();

            List<UltraGridRow> rows = new List<UltraGridRow>();
            UltraGridBand parentBand = band.ParentBand;
            if (parentBand == null)
            {
                foreach (UltraGridRow row in layout.Rows.GetAllNonGroupByRows())
                {   
                    if (this.mappingManager.WasRowExported(row))
                        rows.Add(row);
                }
                rows.Sort(this.GridRowComparer);

                this.BuildHiddenWorksheetColumnInfo(column, hiddenWorksheetColumnInfo, rows, 0);
            }
            else
            {
                int startRow = 0;
                foreach (UltraGridRow parentRow in layout.Rows.GetRowEnumerator(GridRowType.DataRow, parentBand, parentBand))
                {                    
                    rows = new List<UltraGridRow>();
                    foreach (UltraGridRow row in parentRow.ChildBands[band.Key].Rows)
                    {
                        if (this.mappingManager.WasRowExported(row))
                            rows.Add(row);
                    }
                    rows.Sort(this.GridRowComparer);

                    startRow = this.BuildHiddenWorksheetColumnInfo(column, hiddenWorksheetColumnInfo, rows, startRow);
                }
            }

            return hiddenWorksheetColumnInfo;
        }

        private int BuildHiddenWorksheetColumnInfo(UltraGridColumn column, HiddenWorksheetColumnInfo hiddenWorksheetColumnInfo, List<UltraGridRow> rows, int startRow)
        {
            int endRow = startRow + rows.Count;
            for (int i = startRow; i < endRow; i++)
            {
                UltraGridRow row = rows[i - startRow];
                WorksheetCell hiddenworkSheetCell = this.hiddenWorkSheet.Rows[i].Cells[this.lastUsedColumn];

                if (false == column.Hidden)
                {
                    UltraGridCell cell = row.Cells[column];
                    WorksheetCell realWorksheetCell = this.mappingManager.GetWorksheetCell(cell);
                    string formulaString = string.Format("={0}", realWorksheetCell.ToString(CellReferenceMode.A1, true));
                    Formula formula = Formula.Parse(formulaString, CellReferenceMode.A1);                    
                    formula.ApplyTo(hiddenworkSheetCell);
                    hiddenWorksheetColumnInfo.MapWorkSheetCell(realWorksheetCell, hiddenworkSheetCell);
                }
                else
                {
                    CellInfo cellInfo = new CellInfo(column, row);
                    
                    // If this column has a formula, add the cell to the MappingManagers 
                    // FormulaTargetCells list so that the Formula gets exported. 
                    if (column.Formula != null &&
                        column.Formula.Length > 0)
                    {
                        this.mappingManager.FormulaTargetCells.Add(cellInfo);
                    }                    

                    hiddenworkSheetCell.Value = row.GetExportValue(column);
                    hiddenWorksheetColumnInfo.MapWorkSheetCell(cellInfo, hiddenworkSheetCell);
                }                
            }

            return endRow;
        }
        #endregion //GetHiddenWorksheetColumnInfo

        #endregion //Private Methods

        #region Public Methods

        #region GetHiddenWorksheetCell(s)

        internal WorksheetCell GetHiddenWorksheetCell(CellInfo cellInfo)
        {
            HiddenWorksheetColumnInfo hiddenWorksheetColumnInfo = this.GetHiddenWorksheetColumnInfo(cellInfo.GridColumn);
            return hiddenWorksheetColumnInfo.GetHiddenWorksheetCell(cellInfo);
        }

        public WorksheetCellRangeInfo GetHiddenWorksheetCells(UltraGridColumn column, List<WorksheetCell> realworkSheetCells)
        {
            Worksheet worksheet = realworkSheetCells[0].Worksheet;
            HiddenWorksheetColumnInfo hiddenWorksheetColumnInfo = this.GetHiddenWorksheetColumnInfo(column);

            WorksheetCellRangeInfo worksheetCellRangeInfo = new WorksheetCellRangeInfo(realworkSheetCells.Count);
            foreach (WorksheetCell realWorksheetCell in realworkSheetCells)
            {
                WorksheetCell hiddenWorkSheetCell = hiddenWorksheetColumnInfo.GetHiddenWorksheetCell(realWorksheetCell);
                worksheetCellRangeInfo.AddWorksheetCell(hiddenWorkSheetCell);
            }

            return worksheetCellRangeInfo;
        }
        #endregion //GetHiddenWorksheetCell(s)

        #endregion //Public Methods

        #region HiddenWorksheetColumnInfo class
        private class HiddenWorksheetColumnInfo
        {
            #region Private Members
            private Dictionary<WorksheetCell, WorksheetCell> realWorksheetCellToHiddenWorksheetCell;
            private Dictionary<CellInfo, WorksheetCell> gridCellToHiddenWorksheetCell;
            
            #endregion //Private Members

            #region Private Properties
            
            #region GridCellToHiddenWorksheetCell
            private Dictionary<CellInfo, WorksheetCell> GridCellToHiddenWorksheetCell
            {
                get
                {
                    if (this.gridCellToHiddenWorksheetCell == null)
                        this.gridCellToHiddenWorksheetCell = new Dictionary<CellInfo, WorksheetCell>();

                    return this.gridCellToHiddenWorksheetCell;
                }
            }
            #endregion //GridCellToHiddenWorksheetCell

            #region RealWorksheetCellToHiddenWorksheetCell
            private Dictionary<WorksheetCell, WorksheetCell> RealWorksheetCellToHiddenWorksheetCell
            {
                get
                {
                    if (this.realWorksheetCellToHiddenWorksheetCell == null)
                        this.realWorksheetCellToHiddenWorksheetCell = new Dictionary<WorksheetCell, WorksheetCell>();

                    return this.realWorksheetCellToHiddenWorksheetCell;
                }
            }
            #endregion //RealWorksheetCellToHiddenWorksheetCell

            #endregion //Private Properties

            #region Public Methods

            #region GetHiddenWorksheetCell

            public WorksheetCell GetHiddenWorksheetCell(CellInfo cellInfo)
            {                
                WorksheetCell hiddenWorksheetCell;                
                bool success = this.GridCellToHiddenWorksheetCell.TryGetValue(cellInfo, out hiddenWorksheetCell);

                return success
                    ? hiddenWorksheetCell
                    : null;
            }

            public WorksheetCell GetHiddenWorksheetCell(WorksheetCell realWorksheetCell)
            {
                WorksheetCell hiddenWorksheetCell;
                bool success = this.RealWorksheetCellToHiddenWorksheetCell.TryGetValue(realWorksheetCell, out hiddenWorksheetCell);

                return success
                    ? hiddenWorksheetCell
                    : null;
            }
            #endregion //GetHiddenWorksheetCell

            #region MapWorkSheetCell
            public void MapWorkSheetCell(WorksheetCell realWorksheetCell, WorksheetCell hiddenWorkSheetCell)
            {
                this.RealWorksheetCellToHiddenWorksheetCell[realWorksheetCell] = hiddenWorkSheetCell;
            }

            internal void MapWorkSheetCell(CellInfo cellInfo, WorksheetCell hiddenworkSheetCell)
            {
                this.GridCellToHiddenWorksheetCell[cellInfo] = hiddenworkSheetCell;
            }
            #endregion //MapWorkSheetCell

            #endregion //Public Methods       
            
        }
        #endregion //HiddenWorksheetColumnInfo class        
    }
    #endregion //HiddenWorksheetManager class

    #region ArgumentInfo class
    internal class ArgumentInfo
    {
        #region Private Members
        private string text;
        private UltraCalcOperatorFunction? functionOperator;
        private int? orderOfPrecedence;
        #endregion //Private Members

        #region Constructor
        public ArgumentInfo(string text, UltraCalcOperatorFunction? functionOperator)
        {
            this.text = text;
            this.functionOperator = functionOperator;
        }
        #endregion //Constructor

        #region Public Properties

        #region OperatorFunction
        public UltraCalcOperatorFunction? FunctionOperator
        {
            get { return this.functionOperator; }
        }
        #endregion //OperatorFunction

        #region OrderOfPrecedence
        public int OrderOfPrecedence
        {
            get
            {
                if (this.orderOfPrecedence == null)
                    this.orderOfPrecedence = GetOrderOfPrecedence(this.FunctionOperator);

                return (int)this.orderOfPrecedence;
            }
        }
        #endregion //OrderOfPrecedence

        #region Text
        public string Text
        {
            get { return this.text; }
        }
        #endregion //Text
        
        #endregion //Public Properties

        #region Public Methods

        #region GetArgumentText

        /// <summary>
        /// Returns the text of the argument. Automatically surrounds the text with parens if the needed (as determined by the order of precedence of the next operator).
        /// </summary>
        /// <param name="nextFunctionOperator"></param>        
        /// <returns></returns>
        // MRS 10/8/2008 - TFS8760
        // Overload for backward compatibility
        public string GetArgumentText(UltraCalcOperatorFunction? nextFunctionOperator)        
        {
            return this.GetArgumentText(nextFunctionOperator, 1);
        }

        /// <summary>
        /// Returns the text of the argument. Automatically surrounds the text with parens if the needed (as determined by the order of precedence of the next operator).
        /// </summary>
        /// <param name="nextFunctionOperator"></param>
        /// <param name="argumentIndex">An integer indicating whether the argument is the first or second arg in an operation.</param>
        /// <returns></returns>
        // MRS 10/8/2008 - TFS8760
        public string GetArgumentText(UltraCalcOperatorFunction? nextFunctionOperator, int argumentIndex)
        {
            string text = this.Text;
            if (text == "DBNULL()" ||
                text == "NULL()")
            {
                switch (nextFunctionOperator)
                {
                    case UltraCalcOperatorFunction.Add:                    
                    case UltraCalcOperatorFunction.Divide:
                    case UltraCalcOperatorFunction.Equal:
                    case UltraCalcOperatorFunction.GreaterThan:
                    case UltraCalcOperatorFunction.GreaterThanOrEqual:
                    case UltraCalcOperatorFunction.LessThan:
                    case UltraCalcOperatorFunction.LessThanOrEqual:
                    case UltraCalcOperatorFunction.Multiply:
                    case UltraCalcOperatorFunction.NotEqual:
                    case UltraCalcOperatorFunction.Subtract:
                    case UltraCalcOperatorFunction.Exponent:
                    case UltraCalcOperatorFunction.UnaryMinus:
                    case UltraCalcOperatorFunction.UnaryPlus:
                    case UltraCalcOperatorFunction.Percent:
                        text = "0";
                        break;
                    case UltraCalcOperatorFunction.Concatenate:
                        text = "\"\"";
                        break;
                    default:
                        Debug.Fail("This methods was called with a null operator; unexpected");
                        return string.Empty;
                }
            }

            // MRS 10/8/2008 - TFS8760
            //if (this.HasLowerOrderOfPrecedence(nextFunctionOperator))
            if (this.HasLowerOrderOfPrecedence(nextFunctionOperator, argumentIndex))
            {
                return string.Format("({0})", text);
            }

            return text;
        }
        #endregion //GetArgumentText

        #region GetOrderOfPrecedence
        public static int GetOrderOfPrecedence(UltraCalcOperatorFunction? functionOperator)
        {
            switch (functionOperator)
            {
                case UltraCalcOperatorFunction.UnaryMinus:
                case UltraCalcOperatorFunction.UnaryPlus:
                case UltraCalcOperatorFunction.Percent:
                    return 6;
                case UltraCalcOperatorFunction.Exponent:
                    return 5;
                case UltraCalcOperatorFunction.Multiply:
                case UltraCalcOperatorFunction.Divide:
                    return 4;
                case UltraCalcOperatorFunction.Add:
                case UltraCalcOperatorFunction.Subtract:
                    return 3;
                case UltraCalcOperatorFunction.Concatenate:
                    return 2;
                case UltraCalcOperatorFunction.Equal:
                case UltraCalcOperatorFunction.GreaterThan:
                case UltraCalcOperatorFunction.GreaterThanOrEqual:
                case UltraCalcOperatorFunction.LessThan:
                case UltraCalcOperatorFunction.LessThanOrEqual:
                case UltraCalcOperatorFunction.NotEqual:
                    return 1;
                default:
                    return int.MaxValue;
            }
        }
        #endregion //GetOrderOfPrecedence

        #region HasLowerOrderOfPrecedence
        // MRS 10/8/2008 - TFS8760
        //public bool HasLowerOrderOfPrecedence(UltraCalcOperatorFunction? operatorFunction)
        public bool HasLowerOrderOfPrecedence(UltraCalcOperatorFunction? operatorFunction, int argumentIndex)
        {
            // MRS 10/8/2008 - TFS8760
            //return this.OrderOfPrecedence < ArgumentInfo.GetOrderOfPrecedence(operatorFunction);

            int functionOrderOfPrecedence = ArgumentInfo.GetOrderOfPrecedence(operatorFunction);
            if (this.OrderOfPrecedence < functionOrderOfPrecedence)
                return true;

            if (argumentIndex == 2 && this.OrderOfPrecedence == functionOrderOfPrecedence)
                return true;

            return false;
        }
        #endregion //HasLowerOrderOfPrecedence

        #endregion //Pubilc Methods

        #region Overrides

        #region ToString
        public override string ToString()
        {
            return this.Text;
        }
        #endregion //ToString

        #endregion //Overrides
    }
    #endregion //ArgumentInfo class
}
