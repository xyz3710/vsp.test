#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Diagnostics;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// The element that appears to the top of the vertical
    /// scrollbar that is used fro spliiting RowScrollRegions
    /// regions.
    /// </summary>
    public class RowSplitterBarUIElement : SplitterUIElement
    {

        private RowScrollRegion rowScrollRegion;

		/// <summary>
		/// ctor
		/// </summary>
        public RowSplitterBarUIElement( UIElement parent )
            : base( parent, false )
        {			
        }
		internal void initialize( RowScrollRegion rowScrollRegion )
		{
			this.rowScrollRegion    = rowScrollRegion;
			this.PrimaryContext     = rowScrollRegion;
		}

		// SSP 1/5/04 UWG1606
		// Added appearances for the splitter bars.
		//
        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			UltraGridLayout layout = null != this.RowScrollRegion ? this.RowScrollRegion.Layout : null;
			if ( null != layout )
			{
				// SSP 3/13/06 - App Styling
				// 
				// ----------------------------------------------------------------------------------------------------
				//if ( layout.GetHasAppearance( UltraGridLayout.LayoutAppearanceIndex.SplitterBarHorizontalAppearance ) )
				//	layout.SplitterBarHorizontalAppearance.MergeData( ref appearance, ref requestedProps );	
				layout.MergeLayoutAppearances( ref appearance, ref requestedProps, 
					UltraGridLayout.LayoutAppearanceIndex.SplitterBarHorizontalAppearance , 
					StyleUtils.Role.RowScrollRegionSplitterBar, AppStyling.RoleState.Normal );
				// ----------------------------------------------------------------------------------------------------
			}
		}

		/// <summary>
		/// Row Splitter bars row scroll region
		/// </summary>
        public RowScrollRegion RowScrollRegion
        {
            get
            {
                return this.rowScrollRegion;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>The border style of cells, rows, and headers can be set by the BorderStyleCell, BorderStyleRow, and BorderStyleHeader properties respectively.</para>
        /// <para>The border style of the AddNew box buttons can be set by the ButtonBorderStyle property.</para>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.rowScrollRegion.Layout.BorderStyleSplitter;
			}
		}

		/// <summary>
        /// Returns true if the element can be moved or resized vertically by clicking on the passed in mouse point.                
        /// </summary>
        /// <param name="point">In client coordinates</param>
        /// <returns>true if the element supports up / down adjustsments from the specified point.</returns>
        public override bool SupportsUpDownAdjustmentsFromPoint( System.Drawing.Point point )
        {
			//RobA UWG524 10/15/01 
			//implemented this method

			int count = this.RowScrollRegion.Layout.RowScrollRegions.Count;
			int index = this.RowScrollRegion.Index;

			//if this RowScrollRegion and the RowScrollRegion under it are both 
			//fixed, return false
			//
			if ( this.RowScrollRegion.SizingMode == SizingMode.Fixed &&
				 this.RowScrollRegion.Layout.RowScrollRegions[index + 1].SizingMode == SizingMode.Fixed )
				return false;

			//if any of the RowScrollRegions under this one are free
			//and there is a RowScrollRegion on top that is free
			//return true, otherwise return false
			//
			for ( int i = index + 1; i < count ; ++i )
			{
				if ( this.RowScrollRegion.Layout.RowScrollRegions[i].SizingMode == SizingMode.Free )					
				{
					for ( int j = index; j >= 0; --j )
						if ( this.RowScrollRegion.Layout.RowScrollRegions[j].SizingMode == SizingMode.Free )
							return true;				
				}
			}

			return false;
		}

        /// <summary>
        /// Called after a move/resize operation. 
        /// </summary>
        /// <param name="delta">The delta</param>
        public override void ApplyAdjustment(Point delta)
        {
            // call OnResize 
            //
            if ( 0 != delta.Y )
			{
				//RobA UWG524 10/16/01 
				//rewrote this method

				//if this region is fixed then walk up and resize each region
				//until you reach a free sizing region
				//
				if ( this.rowScrollRegion.SizingMode == SizingMode.Fixed )
				{
					for ( int i = this.rowScrollRegion.Index; i >= 0; --i )
					{
						if ( this.rowScrollRegion.Layout.RowScrollRegions[i].SizingMode == SizingMode.Free )
						{			
							this.rowScrollRegion.Layout.RowScrollRegions.OnResize( this.rowScrollRegion.Layout.RowScrollRegions[i], delta.X );
							break;
						}
						this.rowScrollRegion.Layout.RowScrollRegions.OnResize( this.rowScrollRegion.Layout.RowScrollRegions[i], delta.X );					
					}
				}

				else
					//RobA UWG572 10/19/01
	                //this.rowScrollRegion.Layout.RowScrollRegions.OnResize( this.rowScrollRegion, delta.X );
					this.rowScrollRegion.Layout.RowScrollRegions.OnResize( this.rowScrollRegion, delta.Y );
			
				//walk down resizing each region
				//until you reach a free sizing rowScrollRegion. 
				//
				for ( int i = this.rowScrollRegion.Index + 1; i < this.rowScrollRegion.Layout.RowScrollRegions.Count; ++i )
				{
					if ( this.rowScrollRegion.Layout.RowScrollRegions[i].SizingMode == SizingMode.Free )
						return;
					
					this.rowScrollRegion.Layout.RowScrollRegions.OnResize( this.rowScrollRegion.Layout.RowScrollRegions[i], delta.X );
				}					
			}                  
        }


		// SSP 8/16/01 UWG133
		// We need to exit the edit mode when user pressed the mouse on the
		// splitter bar to resize the region
        /// <summary>
        /// Called when the mouse down message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown( MouseEventArgs e, 
										 bool adjustableArea,
										 ref UIElement captureMouseForElement )
		{
			bool ret = base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );

			if ( null != this.RowScrollRegion )
			{
				// exit edit mode when user clicks on the splitter bar
				if ( null != this.RowScrollRegion.Layout.ActiveCell )
				{				
					this.RowScrollRegion.Layout.ActiveCell.ExitEditMode();
				}
			}

			return ret;
		}

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.RowScrollRegion.Layout, StyleUtils.Role.RowScrollRegionSplitterBar );
			}
		}

		#endregion // UIRole
    }
}
