#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;

using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{

	/// <summary>
	/// Created at designtime only - used by the UltraGridDesigner to display prompts at designtime.
	/// </summary>
	public class DesignerPromptAreaUIElement : UIElement
	{
		#region Constructor

		internal DesignerPromptAreaUIElement(UIElement parent) : base( parent)
		{
			//
			
			//
		}

		#endregion Constructor

		#region Base Class Overrides

			#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
												ref AppearancePropFlags	requestedProps)
		{
			appearance.BackColor			 = Office2003Colors.OutlookNavPaneCurrentGroupHeaderGradientLight;
			requestedProps					&= ~AppearancePropFlags.BackColor;

			appearance.BackColor2			 = Office2003Colors.OutlookNavPaneCurrentGroupHeaderGradientDark;
			requestedProps					&= ~AppearancePropFlags.BackColor2;

			appearance.BackGradientStyle	 = GradientStyle.Vertical;
			requestedProps					&= ~AppearancePropFlags.BackGradientStyle;

			appearance.ForeColor			 = Office2003Colors.OutlookNavPaneCurrentGroupHeaderForecolor;
			requestedProps					&= ~AppearancePropFlags.ForeColor;

			appearance.FontData.Name		 = "Tahoma";
			requestedProps					&= ~AppearancePropFlags.FontName;

			appearance.FontData.SizeInPoints = 10f;
			requestedProps					&= ~AppearancePropFlags.FontSize;


			//	Call the base class implementation
			base.InitAppearance(ref appearance, ref requestedProps);
		}

			#endregion	InitAppearance

		#endregion Base Class Overrides

		#region PositionChildElements

		/// <summary>
		/// Positions child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// Do not add child elements here.  UltraGridDesigner will add the necessary child elements
			// using a CreationFilter.
		}

		#endregion PositionChildElements
	}
}
