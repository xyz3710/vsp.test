#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	// SSP 2/22/02
	// File created.
	// Added the CustomRowFiltersDialog form for prompting the user to enter a
	// custom dialog for inputting custom row filter conditions.
	//

	// AS 6/22/04 Moved to DialogHelperClasses.cs
	#region Moved
	
	#endregion //Moved

	/// <summary>
	/// Dialog for letting users enter custom row filters.
	/// </summary>
	public class CustomRowFiltersDialog : System.Windows.Forms.Form
	{
		// SSP 5/16/05 - NAS 5.2 Filter Row
		//
		
		
		private ArrayList regexExamples = null;
		private Image	  rowSelectorImage = null;
		
		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Support Filtering in Combo and DropDown
		//private UltraGrid ownerGrid = null;
		private UltraGridBase ownerGrid = null;

		private UltraGrid	grid = null;
		private ValueList   operatorsValueList = null;
		private ValueList   operandsValueList  = null;
		private System.Windows.Forms.Button addConditionButton;
		private System.Windows.Forms.Panel gridPanel;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.RadioButton andRadioButton;
		private System.Windows.Forms.RadioButton orRadioButton;
		private bool cancellingDialog = true;
		
		// Only valid while visible
		//
		private UltraGridColumn internalColumn = null;

		// SSP 8/5/03
		// Changed the custom row filter dialog's Operator field to display all the cell values.
		// Added rows member variable.
		//
		private RowsCollection rows = null;
		private System.ComponentModel.IContainer components;

		// SSP 8/6/03
		// Implemented code to show the cell values in the operand column of the row filter dialog.
		//
		private ValueListItem[] cachedAllCellValues = null;
		private System.Windows.Forms.Button deleteButton;
		private ValueListItem[] cachedFilteredInCellValues = null;
		// SSP 3/14/06 BR10846
		// Changed from the Label to StatusBar because the text in the label needs to be single
		// line and there is no way to force the label to not wrap.
		// 
		//private System.Windows.Forms.Label conditionsLabel;
		private System.Windows.Forms.StatusBar conditionsLabel;
		private System.Windows.Forms.StatusBarPanel statusBarPanel1;
		private Infragistics.Win.AppStyling.Runtime.InboxControlStyler inboxControlStyler1;

		// SSP 10/8/03 UWG2459
		// Added columnFilter variable.
		//
		private ColumnFilter columnFilter = null;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="ownerGrid">The owning UltraGridBase control.</param>
		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Support Filtering in Combo and DropDown
		//public CustomRowFiltersDialog( UltraGrid ownerGrid )
		public CustomRowFiltersDialog( UltraGridBase ownerGrid )
		{			
			this.ownerGrid = ownerGrid;

			// SSP 1/6/04
			// Reget the strings from resources.
			//
			// SSP 5/16/05 - NAS 5.2 Filter Row
			// Now we are regetting the strings every place we need it.
			//
			

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// AS 1/29/07 NA 2007 Vol 1
			if (null != ownerGrid)
				ownerGrid.InitializeInboxControlStyler(this.inboxControlStyler1);

			this.addConditionButton.Text = SR.GetString( "FilterDialogAddConditionButtonText" ); // "&Add a conditio&n";
			this.cancelButton.Text = SR.GetString( "FilterDialogCancelButtonText" ); //"&Cancel";
			this.okButton.Text = SR.GetString( "FilterDialogOkButtonText" ); // "&Ok";
			this.orRadioButton.Text = SR.GetString( "FilterDialogOrRadioText" ); //"Or conditions";
			this.andRadioButton.Text = SR.GetString( "FilterDialogAndRadioText" ); //"And conditions";

			// SSP 8/6/03 UWG2106
			// Added Delete button to the dialog for deleting the item.
			//
			this.deleteButton.Text = SR.GetString( "FilterDialogDeleteButtonText" ); // "&Ok";

			// MD 11/15/06
			// Dialog Accessibility
			this.AccessibleName = this.Text;
			this.addConditionButton.AccessibleName = this.addConditionButton.Text;
			this.cancelButton.AccessibleName = this.cancelButton.Text;
			this.okButton.AccessibleName = this.okButton.Text;
			this.orRadioButton.AccessibleName = this.orRadioButton.Text;
			this.orRadioButton.AccessibleDescription = SR.GetString( "FilterDialogOrRadio_AccessibleDescription" );
			this.andRadioButton.AccessibleName = this.andRadioButton.Text;
			this.andRadioButton.AccessibleDescription = SR.GetString( "FilterDialogAndRadio_AccessibleDescription" );
			this.deleteButton.AccessibleName = this.deleteButton.Text;
			this.deleteButton.AccessibleDescription = SR.GetString( "FilterDialogDeleteButton_AccessibleDescription" );

			//
			// Add any constructor code after InitializeComponent call
			//
			if ( !this.DesignMode )
				this.Hide( );
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
				
				if ( null != this.grid )
				{
					this.grid.AfterRowInsert -= new Infragistics.Win.UltraWinGrid.RowEventHandler( this.OnGrid_AfterRowInsert );
					this.grid.AfterRowsDeleted -= new EventHandler( this.OnGrid_AfterRowsDeleted  );
					this.grid.BeforeSelectChange -= new Infragistics.Win.UltraWinGrid.BeforeSelectChangeEventHandler( this.OnGrid_BeforeSelectChange );
					this.grid.BeforeRowsDeleted -= new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler( this.OnGrid_BeforeRowsDeleted );
					this.grid.CellListSelect -=  new Infragistics.Win.UltraWinGrid.CellEventHandler( this.OnGrid_CellListSelect );
					this.grid.AfterRowUpdate -= new Infragistics.Win.UltraWinGrid.RowEventHandler( this.OnGrid_AfterRowUpdate );
					this.grid.BeforeCellListDropDown -= new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler( this.OnGrid_BeforeCellDropDownList );
					this.grid.CellChange -= new Infragistics.Win.UltraWinGrid.CellEventHandler( this.OnGrid_OnCellChange );

					this.grid.Dispose( );
					this.grid = null;
					this.rowSelectorImage.Dispose( );
					this.rowSelectorImage = null;
				}

                // MBS 7/9/08 - BR33993
                if (this.operandsValueList != null)
                    this.operandsValueList.Dispose();
                //
                if (this.operatorsValueList != null)
                    this.operatorsValueList.Dispose();
			}

			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.gridPanel = new System.Windows.Forms.Panel();
			this.addConditionButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.okButton = new System.Windows.Forms.Button();
			this.andRadioButton = new System.Windows.Forms.RadioButton();
			this.orRadioButton = new System.Windows.Forms.RadioButton();
			this.deleteButton = new System.Windows.Forms.Button();
			this.conditionsLabel = new System.Windows.Forms.StatusBar();
			this.statusBarPanel1 = new System.Windows.Forms.StatusBarPanel();
			this.inboxControlStyler1 = new Infragistics.Win.AppStyling.Runtime.InboxControlStyler(this.components);
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.inboxControlStyler1)).BeginInit();
			this.SuspendLayout();
			// 
			// gridPanel
			// 
			this.gridPanel.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.gridPanel.Location = new System.Drawing.Point(144, 8);
			this.gridPanel.Name = "gridPanel";
			this.gridPanel.Size = new System.Drawing.Size(400, 288);
			this.inboxControlStyler1.SetStyleSettings(this.gridPanel, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.gridPanel.TabIndex = 1;
			this.gridPanel.Tag = "";
			// 
			// addConditionButton
			// 
			this.addConditionButton.Location = new System.Drawing.Point(8, 80);
			this.addConditionButton.Name = "addConditionButton";
			this.addConditionButton.Size = new System.Drawing.Size(128, 32);
			this.inboxControlStyler1.SetStyleSettings(this.addConditionButton, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.addConditionButton.TabIndex = 2;
			this.addConditionButton.Click += new System.EventHandler(this.addConditionButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(8, 200);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(128, 32);
			this.inboxControlStyler1.SetStyleSettings(this.cancelButton, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.cancelButton.TabIndex = 5;
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Enabled = false;
			this.okButton.Location = new System.Drawing.Point(8, 160);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(128, 32);
			this.inboxControlStyler1.SetStyleSettings(this.okButton, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.okButton.TabIndex = 4;
			this.okButton.Click += new System.EventHandler(this.okButton_Click);
			// 
			// andRadioButton
			// 
			this.andRadioButton.Checked = true;
			this.andRadioButton.Location = new System.Drawing.Point(8, 24);
			this.andRadioButton.Name = "andRadioButton";
			this.andRadioButton.Size = new System.Drawing.Size(128, 24);
			this.inboxControlStyler1.SetStyleSettings(this.andRadioButton, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.andRadioButton.TabIndex = 6;
			this.andRadioButton.TabStop = true;
			this.andRadioButton.CheckedChanged += new System.EventHandler(this.andRadioButton_CheckedChanged);
			// 
			// orRadioButton
			// 
			this.orRadioButton.Location = new System.Drawing.Point(8, 48);
			this.orRadioButton.Name = "orRadioButton";
			this.orRadioButton.Size = new System.Drawing.Size(128, 24);
			this.inboxControlStyler1.SetStyleSettings(this.orRadioButton, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.orRadioButton.TabIndex = 7;
			this.orRadioButton.CheckedChanged += new System.EventHandler(this.andRadioButton_CheckedChanged);
			// 
			// deleteButton
			// 
			this.deleteButton.Location = new System.Drawing.Point(8, 120);
			this.deleteButton.Name = "deleteButton";
			this.deleteButton.Size = new System.Drawing.Size(128, 32);
			this.inboxControlStyler1.SetStyleSettings(this.deleteButton, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.deleteButton.TabIndex = 3;
			this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
			// 
			// conditionsLabel
			// 
			this.conditionsLabel.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.conditionsLabel.Dock = System.Windows.Forms.DockStyle.None;
			this.conditionsLabel.Location = new System.Drawing.Point(8, 304);
			this.conditionsLabel.Name = "conditionsLabel";
			this.conditionsLabel.Panels.AddRange(new System.Windows.Forms.StatusBarPanel[] {
																							   this.statusBarPanel1});
			this.conditionsLabel.ShowPanels = true;
			this.conditionsLabel.Size = new System.Drawing.Size(536, 24);
			this.conditionsLabel.SizingGrip = false;
			this.inboxControlStyler1.SetStyleSettings(this.conditionsLabel, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.conditionsLabel.TabIndex = 8;
			// 
			// statusBarPanel1
			// 
			this.statusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
			this.statusBarPanel1.Text = "statusBarPanel1";
			this.statusBarPanel1.Width = 536;
			// 
			// CustomRowFiltersDialog
			// 
			this.AcceptButton = this.okButton;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(554, 334);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.conditionsLabel,
																		  this.deleteButton,
																		  this.okButton,
																		  this.cancelButton,
																		  this.addConditionButton,
																		  this.gridPanel,
																		  this.andRadioButton,
																		  this.orRadioButton});
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(360, 296);
			this.Name = "CustomRowFiltersDialog";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.inboxControlStyler1.SetStyleSettings(this, new Infragistics.Win.AppStyling.Runtime.InboxControlStyleSettings("", Infragistics.Win.DefaultableBoolean.Default));
			this.Closing += new System.ComponentModel.CancelEventHandler(this.CustomRowFiltersDialog_Closing);
			this.Load += new System.EventHandler(this.CustomRowFiltersDialog_Load);
			((System.ComponentModel.ISupportInitialize)(this.statusBarPanel1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.inboxControlStyler1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		// MD 8/7/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal ColumnFilter ColumnFilterBeingEdited
		//{
		//    get
		//    {
		//        return this.columnFilter;
		//    }
		//}

		#endregion Not Used

		private void CustomRowFiltersDialog_Load(object sender, System.EventArgs e)
		{
			// SSP 2/21/06 BR10268
			// Previously when the dialog came up, the first row was active however no cell
			// was activated and put into edit mode. This change is to put the first cell
			// into edit mode.
			// 
			this.DelayedEnterEditMode( );
		}

		// SSP 2/21/06 BR10268
		// 
		private void DelayedEnterEditMode( )
		{
			if ( ! this.IsDisposed && null != this.grid && ! this.grid.IsDisposed )
				this.BeginInvoke( new MethodInvoker( this.EnterEditMode ) );
		}

		// SSP 2/21/06 BR10268
		// 
		private void EnterEditMode( )
		{
			if ( null != this.grid && ! this.grid.IsDisposed )
			{
				this.Grid.EnsureTempActiveRowAssigned( );

				UltraGridCell activeCell = this.grid.ActiveCell;
				if ( null == activeCell )
				{
					UltraGridRow activeRow = this.Grid.ActiveRow;
					if ( null != activeRow && activeRow.Band.Columns.Exists( "Operator" ) )
					{
						activeRow.Cells[ "Operator" ].Activate( );
						activeCell = this.grid.ActiveCell;
					}
				}

				if ( null != activeCell && ! activeCell.IsInEditMode )
					this.grid.PerformAction( UltraGridAction.EnterEditMode );
			}
		}

		// SSP 2/21/06 BR10268
		// Enter the first cell into edit when tabbing into the grid.
		// 
		/// <summary>
        /// Processes a dialog key.
		/// </summary>
        /// <param name="keyData">One of the System.Windows.Forms.Keys values that represents the key to process.</param>
        /// <returns>true if the keystroke was processed and consumed by the control; otherwise, false to allow further processing.</returns>
        // MRS 12/19/06 - fxCop
        [System.Security.Permissions.UIPermission(System.Security.Permissions.SecurityAction.LinkDemand, Window = System.Security.Permissions.UIPermissionWindow.AllWindows)]
        protected override bool ProcessDialogKey(Keys keyData)
		{
			bool ret = base.ProcessDialogKey( keyData );

			// SSP 5/23/06 BR12792
			// Apparently certain keys are supersets of certain other keys.
			// 
			//if ( Keys.Tab == ( Keys.Tab & keyData ) && this.grid.ContainsFocus )
			if ( ( Keys.Tab == keyData || ( Keys.Shift | Keys.Tab ) == keyData ) && this.grid.ContainsFocus )
			{
				if ( null != this.grid && ! this.grid.IsDisposed && this.grid.Rows.Count > 0 )
				{
					RowsCollection rows = this.grid.Rows;
					UltraGridCell cell;

					if ( Keys.Shift == ( Keys.Shift & keyData ) )
						cell = rows[ rows.Count - 1 ].Cells[ "Operand" ];
					else
						cell = rows[0].Cells[ "Operator" ];

					this.grid.ActiveCell = cell;
					this.EnterEditMode( );
				}
			}

			return ret;
		}

		// SSP 10/8/03 UWG2459
		// Made the Grid property public.
		//
		//private UltraGrid Grid
		/// <summary>
		/// Returns the UltraGrid instance used for inputting conditions on the dialog.
		/// </summary>
		public UltraGrid Grid
		{
			get
			{
				if ( null == this.grid )
				{
					this.grid = new UltraGrid( );
					this.gridPanel.Controls.Add( this.grid );
					this.grid.Dock = DockStyle.Fill;
					this.grid.DisplayLayout.ViewStyleBand = ViewStyleBand.Vertical;
					this.grid.DisplayLayout.ViewStyle = ViewStyle.SingleBand;
					this.grid.Visible = true;

					// SSP 10/8/03 UWG2459
					// Set the TabIndex to 1.
					//
					this.grid.TabIndex = 1;

					// AS 1/29/07 NA 2007 Vol 1
					if (null != ownerGrid)
					{
						this.grid.StyleLibraryName = ownerGrid.StyleLibraryName;
						this.grid.StyleSetName = ownerGrid.StyleSetName;
						this.grid.UseAppStyling = ownerGrid.UseAppStyling;
					}
			
					this.grid.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler( this.OnGrid_AfterRowInsert );
					this.grid.AfterRowsDeleted += new EventHandler( this.OnGrid_AfterRowsDeleted  );
					this.grid.BeforeSelectChange += new Infragistics.Win.UltraWinGrid.BeforeSelectChangeEventHandler( this.OnGrid_BeforeSelectChange );
					this.grid.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler( this.OnGrid_BeforeRowsDeleted );
					this.grid.CellListSelect +=  new Infragistics.Win.UltraWinGrid.CellEventHandler( this.OnGrid_CellListSelect );
					this.grid.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler( this.OnGrid_AfterRowUpdate );
					this.grid.BeforeCellListDropDown += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler( this.OnGrid_BeforeCellDropDownList );
					this.grid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler( this.OnGrid_OnCellChange );

					// SSP 8/6/03 UWG2106
					// Added Delete button to the dialog for deleting the item.
					//
					this.grid.AfterSelectChange += new Infragistics.Win.UltraWinGrid.AfterSelectChangeEventHandler( this.OnGrid_AfterSelectChange );

					// SSP 2/21/06 BR10268
					// We need to populate the operands drop down list when the cell enters edit mode, nost
					// just right before it drops down. This way the arrow keys can be used to navigate
					// the items.
					// 
					this.grid.BeforeEnterEditMode += new CancelEventHandler( this.OnGrid_BeforeEnterEditMode );
				}

				return this.grid;
			}
		}

		// SSP 8/6/03 UWG2106
		// Added Delete button to the dialog fro deleting the item.
		//
		private void OnGrid_AfterSelectChange( object sender, AfterSelectChangeEventArgs e )
		{
			this.RefreshDeleteButtonEnabled( );
		}

		// SSP 8/6/03 UWG2106
		// Added Delete button to the dialog fro deleting the item.
		//
		private void RefreshDeleteButtonEnabled( )
		{
			bool enableDeleteButton = false;

			if ( null != this.Grid )
			{
				for ( int i = 0; ! enableDeleteButton && i < this.Grid.Selected.Rows.Count; i++ )
					enableDeleteButton = this.Grid.Selected.Rows[i].IsStillValid;
			}
				
			this.deleteButton.Enabled = enableDeleteButton;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//// SSP 2/25/03 UWG1975
		////
		//private void OnGrid_BeforeRowUpdate( object sender, CancelableRowEventArgs e )
		//{
		//}

		#endregion Not Used

		private object GetCellValue( UltraGridCell cell )
		{
			object cellVal = cell.Value;

			if ( cell == this.Grid.ActiveCell && cell.IsInEditMode && cell.DataChanged )
			{
				try
				{
					object tmp = cell.Column.Editor.Value;
				
					
				
					cellVal = tmp;
				}
				catch ( Exception )
				{
				}
			}

			// SSP 2/24/03 UWG2010
			// For some reason the embeddable editor returns a string rather than the associated
			// value in the value list. So I'm putting a workaround here because I am not sure as
			// to what should be the correct behaviour.
			//
			// MD 7/27/07 - 7.3 Performance
			// Refactored - Prevent calling expensive getters multiple times
			//if ( cellVal is string && null != cell.Column.ValueList )
			//{
			//    int index = -1;
			//    object val = cell.Column.ValueList.GetValue( (string)cellVal, ref index );
			//
			//    // SSP 5/16/05
			//    // Editor also sometimes returns the value as a string rather than the actual
			//    // instance of the value (DataValue in the value list).
			//    //
			//    if ( index < 0 )
			//    {
			//        cell.Column.ValueList.GetText( cellVal, ref index );
			//        if ( index >= 0 )
			//            val = cell.Column.ValueList.GetValue( index );
			//    }
			//
			//    if ( null != val )
			//        cellVal = val;
			//}
			if ( cellVal is string )
			{
				IValueList valueList = cell.Column.ValueList;

				if ( null != valueList )
				{
					int index = -1;
					object val = valueList.GetValue( (string)cellVal, ref index );

					// SSP 5/16/05
					// Editor also sometimes returns the value as a string rather than the actual
					// instance of the value (DataValue in the value list).
					//
					if ( index < 0 )
					{
						valueList.GetText( cellVal, ref index );

						if ( index >= 0 )
							val = valueList.GetValue( index );
					}

					if ( null != val )
						cellVal = val;
				}
			}

			return cellVal;
		}


		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private ValueListItem FindValueListItem( ValueList valueList, object dataValue )
		private static ValueListItem FindValueListItem( ValueList valueList, object dataValue )
		{
			if ( null == valueList || null == valueList.ValueListItems ||
				valueList.ValueListItems.Count <= 0 )
				return null;

			for ( int i = 0; i < valueList.ValueListItems.Count; i++ )
			{
				if ( dataValue == valueList.ValueListItems[i].DataValue )
					return valueList.ValueListItems[i];
			}

			return null;
		}

		private void AddColumnOperands( ValueList valueList )
		{
			if ( null != this.internalColumn && null != internalColumn.Band &&
				null != this.internalColumn.Band.Columns )
			{
				for ( int i = 0; i < this.internalColumn.Band.Columns.Count; i++ )
				{
					UltraGridColumn column = internalColumn.Band.Columns[i];

					if ( column == this.internalColumn )
						continue;

					// SSP 8/29/03 UWG2608
					// Skip the hidden columns.
					//
                    // MRS 2/23/2009 - Found while fixing TFS14354
					//if ( column.Hidden )
                    if (column.HiddenResolved)
						continue;

					// SSP 4/7/05 BR03272
					//
					//if ( null == this.FindValueListItem( valueList, column ) )
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//if ( null == this.FindValueListItem( valueList, new ColumnValueListDataValue( column ) ) )
					if ( null == CustomRowFiltersDialog.FindValueListItem( valueList, new ColumnValueListDataValue( column ) ) )
					{
						// SSP 2/4/04 UWG2927
						// Use the column header's caption instead of the key because that's what the 
						// end user sees.
						//
						//valueList.ValueListItems.Add( internalColumn.Band.Columns[i], "[" + internalColumn.Band.Columns[i].Key + "]" );

						// SSP 4/7/05 BR03272
						//
						
						valueList.ValueListItems.Add( new ColumnValueListDataValue( column ) );

						valueList.BumpContentsVersion( );
					}
				}
			}
		}

		// SSP 8/6/03
		// Implemented code to show the cell values in the operator column of the row filter dialog.
		// Added the new PupulateOperandsValueList method and commented out RemoveColumnOperands method
		// since we don't need it anymore.
		//
		

		// SSP 8/6/03
		// Implemented code to show the cell values in the operator column of the row filter dialog.
		// Added the new PupulateOperandsValueList method and commented out RemoveRegexExamples method
		// since we don't need it anymore.
		//
		

		private void AddRegexExamples( ValueList valueList )
		{
			if ( null == this.regexExamples )
			{
				this.regexExamples = new ArrayList( );

				string[] exampleRegex = new string[]
					{
						"^[A-Z]",
						"^[a-z]",
						"^[0-9]",
						"^[A-Za-z]",
						"^[^A-Za-z]",
						"^[^0-9]+$",
						"^[A-Za-z0-9][A-Za-z0-9_]*$"
					};

				for ( int i = 0; i < exampleRegex.Length; i++ )
				{
					this.regexExamples.Add( new SpecialOperandClass( SpecialOperandClass.SpecialOperandType.Regex, exampleRegex[i] ) );
				}				
			}

			for ( int i = 0; i < this.regexExamples.Count; i++ )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( null == this.FindValueListItem( valueList, this.regexExamples[i] ) )
				if ( null == CustomRowFiltersDialog.FindValueListItem( valueList, this.regexExamples[ i ] ) )
				{
					valueList.ValueListItems.Add( this.regexExamples[i], this.regexExamples[i].ToString( ) );
					valueList.BumpContentsVersion( );
				}
			}			
		}

		// SSP 8/6/03
		// Implemented code to show the cell values in the operator column of the row filter dialog.
		// Added the new PupulateOperandsValueList method and commented out AddRemoveBlankItems method
		// since we don't need it anymore.
		//
		

		private void OnGrid_OnCellChange( object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e )
		{
			if ( null != e.Cell )
			{
				if ( "Operand".Equals( e.Cell.Column.Key ) && e.Cell.IsInEditMode )
				{
					UltraGrid gridCtrl = e.Cell.Layout.Grid as UltraGrid;

					if ( null != gridCtrl )
					{
						EmbeddableEditorBase editor = e.Cell.Column.Editor;

						if ( null != editor && editor.IsInEditMode )
						{
							string text = null;

							try
							{
								text = editor.CurrentEditText;
							}
							catch ( Exception )
							{
							}

							// SSP 5/16/05 - NAS 5.2 Filter Row
							//
							//if ( null != text && text.Length > 0 && !BLANKS_STRING.Equals( text ) )
							if ( null != text && text.Length > 0 && BlanksClass.Value.ToString( ) != text )
							{
								if ( !this.addConditionButton.Enabled )
								{
									this.addConditionButton.Enabled = true;
								}
							}
						}
					}
				}
			}

			this.UpdateConditionLabelText( );
		}

		// SSP 2/21/06 BR10268
		// We need to populate the operands drop down list when the cell enters edit mode, nost
		// just right before it drops down. This way the arrow keys can be used to navigate
		// the items.
		// 
		private void OnGrid_BeforeEnterEditMode( object sender, CancelEventArgs e )
		{
			if ( null != this.grid && ! this.grid.IsDisposed )
				this.PopulateOperandsCellHelper( this.grid.ActiveCell );
		}

		private void OnGrid_BeforeCellDropDownList( object sender, Infragistics.Win.UltraWinGrid.CancelableCellEventArgs e )
		{
			// SSP 7/27/07 BR25080
			// Since we are populating the drop down list in the BeforeEnterEditMode event, we don't
			// need to do it in BeforeCellDropDownList anymore. Not only that it causes an issue 
			// where the editor ends up clearing its value when the value list cleared in the process
			// of populating it with operands.
			// 
			//this.PopulateOperandsCellHelper( e.Cell );
		}

		private void PopulateOperandsCellHelper( UltraGridCell operandsCell )
		{
			UltraGridCell cell = operandsCell;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridColumn operandColumn = this.Grid.DisplayLayout.Bands[0].Columns[ "Operand" ];
			//UltraGridColumn operatorColumn = this.Grid.DisplayLayout.Bands[0].Columns[ "Operator" ];

			if ( null != cell && "Operand".Equals( cell.Column.Key ) )
			{
				//object cellVal = e.Cell.Row.Cells[ "Operator" ].Value;
				object cellVal = this.GetCellValue( cell.Row.Cells[ "Operator" ] );

				ValueList valueList = cell.Column.ValueList as ValueList;

				if ( null == valueList )
					return;

				// If we have an operator that does not make sense for us to have
				// columns in the operands drop down list, then we should remove
				// them.
				//
				bool hasColumnsOperands  = false;
				bool hasRegexExamples    = false;
				bool hasBlanksItem		 = false;
				bool hasEmptyTextItem	 = false;
				bool hasDBNullItem		 = false;

				// SSP 8/6/03
				// Implemented code to show the cell values in the operator column of the row filter dialog.
				//
				bool hasCellValues		 = false;

                // MBS 6/22/09 - TFS18639
                bool hasErrors           = false;
                FilterOperandDropDownItems items = this.columnFilter != null && this.columnFilter.Column != null ?
                    this.columnFilter.Column.FilterOperandDropDownItemsResolved : UltraGridColumn.DefaultFilterOperandDropDownItems;

				if ( cellVal is FilterComparisionOperator &&
					Enum.IsDefined( typeof( FilterComparisionOperator ), cellVal ) )
				{
					FilterComparisionOperator filterOperator = (FilterComparisionOperator)cellVal;

					switch ( filterOperator )
					{
						case FilterComparisionOperator.Like:
							break;
						case FilterComparisionOperator.Match:
						// SSP 5/16/05 - NAS 5.2 Filter Row
						// Added DoesNotMatch entry.
						//
						case FilterComparisionOperator.DoesNotMatch:
							hasRegexExamples = true;
							break;
						case FilterComparisionOperator.Equals:
							hasBlanksItem = true;
							hasEmptyTextItem = true;
							hasDBNullItem = true;
							hasColumnsOperands = true;

							// SSP 8/6/03
							// Implemented code to show the cell values in the operator column of the row filter dialog.
							//
							hasCellValues = true;

                            // MBS 6/22/09 - TFS18639
                            hasErrors = (items & FilterOperandDropDownItems.Errors) == FilterOperandDropDownItems.Errors;

							break;
						case FilterComparisionOperator.NotEquals:
							hasBlanksItem = true;
							hasEmptyTextItem = true;
							hasDBNullItem = true;
							hasColumnsOperands = true;

							// SSP 8/6/03
							// Implemented code to show the cell values in the operator column of the row filter dialog.
							//
							hasCellValues = true;

                            // MBS 6/22/09 - TFS18639
                            hasErrors = (items & FilterOperandDropDownItems.Errors) == FilterOperandDropDownItems.Errors;

							break;
						case FilterComparisionOperator.LessThan:
						case FilterComparisionOperator.LessThanOrEqualTo:
							hasColumnsOperands = true;

							// SSP 8/6/03
							// Implemented code to show the cell values in the operator column of the row filter dialog.
							//
							hasCellValues = true;
							break;
						case FilterComparisionOperator.GreaterThan:
							hasBlanksItem = true;
							hasDBNullItem = true;
							hasEmptyTextItem = true;
							hasColumnsOperands = true;

							// SSP 8/6/03
							// Implemented code to show the cell values in the operator column of the row filter dialog.
							//
							hasCellValues = true;
							break;
						case FilterComparisionOperator.GreaterThanOrEqualTo:
							hasEmptyTextItem = true;
							hasColumnsOperands = true;

							// SSP 8/6/03
							// Implemented code to show the cell values in the operator column of the row filter dialog.
							//
							hasCellValues = true;
							break;
					}
				}

                // MBS 6/22/09 - TFS18639
                // We should respect this new enum when populating the default values in the custom dialog as well                
                hasBlanksItem &= (items & FilterOperandDropDownItems.Blanks) == FilterOperandDropDownItems.Blanks;
                hasCellValues &= (items & FilterOperandDropDownItems.CellValues) == FilterOperandDropDownItems.CellValues;

				// SSP 4/25/03 UWG2174
				// If we already have a condition with blanks as the operand, then don't add the Blanks again 
				// since it dosn't make sense to have two conditions with the Blanks as the operand.
				//
				if ( hasBlanksItem )
				{
					for ( int i = 0; i < this.Grid.Rows.Count; i++ )
					{
						if ( cell.Row != this.Grid.Rows[i] && 
							this.Grid.Rows[i].Cells["Operand"].Value is BlanksClass )
						{
							hasBlanksItem = false;
							break;
						}
					}
				}

				// SSP 8/6/03
				// Implemented code to show the cell values in the operator column of the row filter dialog.
				// Use the new PupulateOperandsValueList method instead.
				// Commented out the original code and added the new code.
				//
				// ------------------------------------------------------------------------------------------
				this.PopulateOperandsValueList( hasBlanksItem, hasEmptyTextItem, hasDBNullItem, 
					hasColumnsOperands, hasRegexExamples, hasCellValues,
                    // MBS 6/22/09 - TFS18639
                    hasErrors);
				
				// ------------------------------------------------------------------------------------------
			}
		}

		// SSP 8/6/03
		// Implemented code to show the cell values in the operator column of the row filter dialog.
		// Added PupulateOperandsValueList method.
		//
		private void PopulateOperandsValueList(
			bool hasBlanksItem, bool hasEmptyTextItem, bool hasDBNullItem,
			bool hasColumns, bool hasRegExExamples, bool hasCellValues,
            // MBS 6/22/09 - TFS18639
            bool hasErrors)
		{
			ValueList valueList = this.GetOperandsValueLists( );

			if ( hasCellValues )
			{
				if ( ( null == this.cachedAllCellValues || null == this.cachedFilteredInCellValues ) 
					&& null != this.internalColumn && null != this.internalColumn.Band )
				{
					UltraGridBand band = this.internalColumn.Band;
					this.internalColumn.Band.LoadFilterValueListHelper( valueList,
						RowFilterMode.AllRowsInBand == band.RowFilterModeResolved ? null : this.rows,
                        // MBS 1/14/09 - TFS12274
						//this.internalColumn, true, true );
                        this.internalColumn, true, FilterValueListOptions.CellValuesOnly);

					// Cache the cell values value list items.
					//
					ValueListItemsCollection cellValues = valueList.ValueListItems;
					this.cachedAllCellValues = new ValueListItem[ cellValues.Count ];

					for ( int i = 0; i < cellValues.Count; i++ )
						this.cachedAllCellValues[ i ] = cellValues[i];

					// Clear the items because we need to add other operands (like blank items) first.
					//
					valueList.ValueListItems.Clear( );

					this.internalColumn.Band.LoadFilterValueListHelper( valueList,
						RowFilterMode.AllRowsInBand == band.RowFilterModeResolved ? null : this.rows,
                        // MBS 1/14/09 - TFS12274
						//this.internalColumn, false, true );
                        this.internalColumn, false, FilterValueListOptions.CellValuesOnly);

					cellValues = valueList.ValueListItems;
					this.cachedFilteredInCellValues = new ValueListItem[ cellValues.Count ];

					for ( int i = 0; i < cellValues.Count; i++ )
						this.cachedFilteredInCellValues[ i ] = cellValues[i];

					// Clear the items because we need to add other operands (like blank items) first.
					//
					valueList.ValueListItems.Clear( );
				}
			}
            
            // MBS 6/22/09 - TFS18639
            if (hasErrors)
                valueList.ValueListItems.Insert(0, ErrorsClass.Value, ErrorsClass.Value.ToString());

			if ( hasEmptyTextItem )
				// SSP 5/16/05 - NAS 5.2 Filter Row
				// Now the EmptyStringClass overrides ToString and returns the text.
				//
				//valueList.ValueListItems.Insert( 0, new EmptyStringClass( ), EMPTY_STRING );
				valueList.ValueListItems.Insert( 0, EmptyStringClass.Value, EmptyStringClass.Value.ToString( ));

			if ( hasDBNullItem )
				// SSP 5/16/05 - NAS 5.2 Filter Row
				// Now the DBNullClass overrides ToString and returns the text.
				//
				//valueList.ValueListItems.Insert( 0, new DBNullClass( ), DBNULL_STRING );
				valueList.ValueListItems.Insert( 0, DBNullClass.Value, DBNullClass.Value.ToString( ) );

			if ( hasBlanksItem )
				// SSP 5/16/05 - NAS 5.2 Filter Row
				// Now the BlanksClass overrides ToString and returns the text.
				//
				//valueList.ValueListItems.Insert( 0, new BlanksClass( ), BLANKS_STRING );
				valueList.ValueListItems.Insert( 0, BlanksClass.Value, BlanksClass.Value.ToString( ) );

			if ( hasColumns )
				this.AddColumnOperands( valueList );

			if ( hasRegExExamples )
				this.AddRegexExamples( valueList );

			if ( hasCellValues )
			{
				ValueListItemsCollection valueListItems = valueList.ValueListItems;
				object[] cellValues = Keys.Shift == Control.ModifierKeys ? this.cachedAllCellValues : this.cachedFilteredInCellValues;

				if ( null != cellValues )
				{
					for ( int i = 0; i < cellValues.Length; i++ )
						valueListItems.Add( cellValues[i] );
				}
			}
		}

		private void OnGrid_BeforeRowsDeleted( object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e )
		{
			e.DisplayPromptMsg = false;
		}

		private void OnGrid_BeforeSelectChange( object sender, Infragistics.Win.UltraWinGrid.BeforeSelectChangeEventArgs e )
		{
			// If the user is trying to select a column, then cancel that.
			//
			if ( null != e && 
				null != e.NewSelections.Columns &&
				e.NewSelections.Columns.Count > 0 )
			{
				e.Cancel = true;
			}
		}

		private void UpdateConditionLabelText( )
		{
			string labelText = "";

			if ( null != this.internalColumn && this.Grid.Rows.Count > 0 )
			{
				for ( int i = 0; i < this.Grid.Rows.Count; i++ )
				{
					UltraGridRow row = this.Grid.Rows[i];
					
					object val = this.GetCellValue( row.Cells[ "Operator" ] );
					
					// AS 1/8/03 - fxcop
					// Do not compare against string.empty - test the length instead
					//if ( null == val || val is System.DBNull || ( val is string && "".Equals( val.ToString( ) ) ) )
					if ( null == val || val is System.DBNull || ( val is string && ((string)val).Length == 0 ) )
					{
						continue;
					}

					// SSP 5/16/05 - NAS 5.2 Filter Row
					// Use the new method of the FilterCondition for getting a string representation
					// of a filter condition.
					//
					// --------------------------------------------------------------------------------
					object operandVal = this.GetCellValue( row.Cells[ "Operand" ] );
					
					// --------------------------------------------------------------------------------

					if ( labelText.Length > 0 )
						// SSP 5/16/06
						// Localized AND and OR.
						//
						//labelText += ( this.andRadioButton.Checked ? " AND " : " OR " );
						labelText += " " + ColumnFilter.GetLogicalOperatorText( this.andRadioButton.Checked ? FilterLogicalOperator.And : FilterLogicalOperator.Or ) + " ";

					// SSP 5/16/05 - NAS 5.2 Filter Row
					// Use the new method of the FilterCondition for getting a string representation
					// of a filter condition.
					//
					// ----------------------------------------------------------------------------------
					System.Text.StringBuilder sb = new System.Text.StringBuilder( );
					FilterCondition.ToString( this.internalColumn, sb, true, val, operandVal );
					labelText += sb.ToString( );
					
					// ----------------------------------------------------------------------------------
				}
			}

			// SSP 3/14/06 BR10846
			// Show a tooltip if the text gets truncated.
			// 
			//this.conditionsLabel.Text = labelText;
			this.statusBarPanel1.Text = labelText;
			this.statusBarPanel1.ToolTipText = labelText;

			this.okButton.Enabled = labelText.Length > 0;

			if ( this.Grid.Rows.Count <=  0 )
			{
				this.okButton.Text = SR.GetString( "FilterDialogOkButtonNoFiltersText" ); //"N&o filters";
				this.okButton.Enabled = true;
			}
			else
			{
				this.okButton.Text = SR.GetString( "FilterDialogOkButtonText" ); // "&Ok";
			}
			

			if ( this.Grid.Rows.Count <= 1 )
			{
				// If there is only one row or no row then no need to have
				// the logical operator radio buttons enabled.
				//
				this.andRadioButton.Enabled = false;
				this.orRadioButton.Enabled = false;

				// We do want them to be able to add a row if there are no rows.
				// Below is where we decide whether we will allow them to add a
				// new row if already existing row has Blanks as the operand.
				// (Remember, we can have only one condition with Blanks as operand.)
				//
				this.addConditionButton.Enabled = true;

				if ( 1 == this.Grid.Rows.Count )
				{
					UltraGridColumn operandColumn = this.Grid.DisplayLayout.SortedBands[0].Columns[ "Operand" ];
					UltraGridColumn operatorColumn = this.Grid.DisplayLayout.SortedBands[0].Columns[ "Operator" ];

					object filterOperatorValue = this.GetCellValue( this.Grid.Rows[0].Cells[ operatorColumn ] );
					object operandVal = this.GetCellValue( this.Grid.Rows[0].Cells[ operandColumn ] );
				
					if ( filterOperatorValue is FilterComparisionOperator && 
						Enum.IsDefined( typeof( FilterComparisionOperator ), filterOperatorValue ) )
					{
						FilterComparisionOperator filterOperator = (FilterComparisionOperator)filterOperatorValue;

						if ( operandVal is BlanksClass )
						{
							if ( FilterComparisionOperator.Equals    != filterOperator && 
								FilterComparisionOperator.NotEquals != filterOperator )
							{
								this.Grid.Rows[0].Cells[ operandColumn ].Value = new DBNullClass( );
							}
							else
							{
								// SSP 10/31/02 UWG1803
								//
								//this.addConditionButton.Enabled = false;
								this.addConditionButton.Enabled = true;

								if ( FilterComparisionOperator.Equals == filterOperator )
								{
									this.andRadioButton.Checked = false;
									this.orRadioButton.Checked = true;
								}
								else if ( FilterComparisionOperator.NotEquals == filterOperator )
								{
									this.orRadioButton.Checked = false;
									this.andRadioButton.Checked = true;
								}
							}
						}
					}
				}

				
			}
			else
			{
				this.addConditionButton.Enabled = true;
				this.andRadioButton.Enabled = true;
				this.orRadioButton.Enabled = true;

				// SSP 4/25/03 UWG2174
				// If one of the conditions is Equals Blanks, then Anding that condition with any other
				// condition doesn't make sense. So Change the condition to and.
				//
				// ------------------------------------------------------------------------------------
				for ( int i = 0; i < this.Grid.Rows.Count; i++ )
				{
					if ( this.Grid.Rows[i].Cells["Operand"].Value is BlanksClass )
					{
						this.andRadioButton.Enabled = false;
						this.orRadioButton.Enabled = false;

						object operatorValue = (FilterComparisionOperator)this.Grid.Rows[i].Cells["Operator"].Value;
						if ( operatorValue is FilterComparisionOperator )
						{
							if ( FilterComparisionOperator.Equals == ((FilterComparisionOperator)operatorValue) )
							{
								this.orRadioButton.Checked = true;
							}
							else if ( FilterComparisionOperator.NotEquals == ((FilterComparisionOperator)operatorValue) )
							{
								this.andRadioButton.Checked = true;
							}
						}

						break;
					}
				}
				// ------------------------------------------------------------------------------------
			}

			// SSP 8/6/03 UWG2106
			// Added Delete button to the dialog fro deleting the item.
			//
			this.RefreshDeleteButtonEnabled( );
		}

		private void OnGrid_CellListSelect( object sender, CellEventArgs e )
		{
			// Only do so if the data has changed
			//
			if ( e.Cell.DataChanged && e.Cell.IsActiveCell && e.Cell.IsInEditMode )
			{
				//e.Cell.ExitEditMode( false, false );
			}

			this.UpdateConditionLabelText( );
		}

		private void OnGrid_AfterRowUpdate( object sender, RowEventArgs e )
		{
			this.UpdateConditionLabelText( );
		}

		private void OnGrid_AfterRowInsert( object sender, RowEventArgs e )
		{
			this.UpdateConditionLabelText( );
		}

		private void OnGrid_AfterRowsDeleted( object sender, EventArgs e )
		{
			this.UpdateConditionLabelText( );
		}

		private ValueList GetOperatorsValueList( )
		{
			if ( null == this.operatorsValueList )
			{
				// SSP 5/9/05 - NAS 5.2 Filter Row
				// Use the same code to generate the operators value list as the filter row code.
				//
				// ------------------------------------------------------------------------------
				
				// SSP 10/19/05 BR06776
				// 
				//this.operatorsValueList = (ValueList)this.internalColumn.FilterOperatorEditorOwnerInfo.GetValueList( null );
				this.operatorsValueList = new ValueList( );
				this.internalColumn.Layout.LoadFilterOperatorValueList( this.operatorsValueList, this.internalColumn.FilterOperatorDropDownItemsResolved );
				// ------------------------------------------------------------------------------
			}

			return this.operatorsValueList;
		}

		// SSP 8/6/03
		// Removed columns and column parameter.
		//
		//private ValueList GetOperandsValueLists( ColumnsCollection columns, UltraGridColumn column )
		private ValueList GetOperandsValueLists( )
		{
			if ( null == this.operandsValueList )
				this.operandsValueList = new ValueList( );

			this.operandsValueList.ValueListItems.Clear( );

			// SSP 8/6/03
			// Implemented code to show the cell values in the operator column of the row filter dialog.
			// Added the new PupulateOperandsValueList method and commented out below code. 
			// PupulateOperandsValueList method re-populates the list so we don't need to do
			// anything here.
			//
			

			return this.operandsValueList;
		}

		internal void PopulateGrid( ColumnFilter columnFilter )
		{
			ArrayList list = this.Grid.DataSource as ArrayList;

			Debug.Assert( null != list, "The grid must have been bount to an arraylist by now !" );

			if ( null == list )
				list = new ArrayList( );

			list.Clear( );

			if ( FilterLogicalOperator.And == columnFilter.LogicalOperator )
			{
				this.orRadioButton.Checked = false;
				this.andRadioButton.Checked = true;				
			}
			else
			{
				this.andRadioButton.Checked = false;
				this.orRadioButton.Checked = true;				
			}

			// SSP 4/24/03 UWG1803
			// We are allowing a Blanks condition (Equals Blanks) and other conditions with it.
			// So try to find the Blanks conditions in more than 3 conditions and not just 3 
			// conditions. (Blanks condition, condition like Equal to (Blanks) comprise of
			// 3 conditions. EqualTo "" Or EqualTo DBNull Or EqualTo null.
			// Commented out below code and added new code below it.
			//
			

			int[] blanksConditions = new int[3] { -1, -1, -1 };

			for ( int i = 0; i < columnFilter.FilterConditions.Count; i++ )
			{
				FilterCondition fc = columnFilter.FilterConditions[i];

				if ( FilterLogicalOperator.And == columnFilter.LogicalOperator )
				{
					if ( FilterComparisionOperator.NotEquals == fc.ComparisionOperator )
					{
						if ( null == fc.CompareValue )
							blanksConditions[0] = i;
						else if ( fc.CompareValue is DBNull )
							blanksConditions[1] = i;
						else if ( fc.CompareValue.ToString( ).Length <= 0 )
							blanksConditions[2] = i;
					}
				}
				else
				{

					if ( FilterComparisionOperator.Equals == fc.ComparisionOperator )
					{
						if ( null == fc.CompareValue )
							blanksConditions[0] = i;
						else if ( fc.CompareValue is DBNull )
							blanksConditions[1] = i;
						else if ( fc.CompareValue.ToString( ).Length <= 0 )
							blanksConditions[2] = i;
					}
				}
			}

			// If we found all the conditions necessary for a Blanks condition, then
			// add that condition.
			//
			if ( blanksConditions[0] >= 0 && blanksConditions[1] >= 0  && blanksConditions[2] >= 0 )
			{
				FilterConditionRow row = new FilterConditionRow( );

				row.Operator = FilterLogicalOperator.Or == columnFilter.LogicalOperator 
					? FilterComparisionOperator.Equals 
					: FilterComparisionOperator.NotEquals;

				row.Operand  = new BlanksClass( );
				list.Add( row );
			}
			else
			{
				blanksConditions[0] = blanksConditions[1] = blanksConditions[2] = -1;
			}

			// SSP 4/25/03 UWG2174
			// Commented out the if condition.
			//
			//if ( list.Count <= 0 )
			//{
				for ( int i = 0; i < columnFilter.FilterConditions.Count; i++ )
				{
					// SSP 4/25/03 UWG2174
					// Skip the blanks condition since we added the row for blanks above.
					//
					if ( Array.IndexOf( blanksConditions, i ) >= 0 )
						continue;

					FilterCondition filterCondition = columnFilter.FilterConditions[i];

					FilterConditionRow row = new FilterConditionRow( );

					row.Operator = filterCondition.ComparisionOperator;
					
					object compareVal = filterCondition.CompareValue;

					// AS 1/8/03 - fxcop
					// Do not compare against string.empty - test the length instead
					//if ( null != compareVal && "".Equals( compareVal ) )
					if ( null != compareVal && compareVal is string && ((string)compareVal).Length == 0 )
						compareVal = new EmptyStringClass( );
					else if ( null != compareVal && compareVal is System.DBNull )
						compareVal = new DBNullClass( );
                    // MBS 2/6/09 - TFS13644
                    // We decided that if the user decides to show the CustomFilterDialog when there
                    // is a SpecialFilterOperand applied to the column, we should just show an empty
                    // slate to start with, since we only support the UI given by the 
                    // UltraGridFilerUIProvider for modifying these
                    else if (compareVal is SpecialFilterOperand)
                    {
                        list.Clear();
                        break;
                    }

					row.Operand  = compareVal;

					list.Add( row );
				}
			//}
			

			if ( list.Count <= 0 )
			{
				list.Add( new FilterConditionRow( ) ); 
			}

			this.Grid.DataSource = list;

			if ( null != this.Grid.Rows )
				this.Grid.Rows.Refresh( RefreshRow.RefreshDisplay, false );
		}

		private void GetColumnFilters( ColumnFilter columnFilter )
		{
			ArrayList list = this.Grid.DataSource as ArrayList;

			Debug.Assert( null != list, "The row filter grid must have been bound to an array list !" );

			columnFilter.ClearFilterConditions( );

			if ( null != list && list.Count > 0 )
			{
				for ( int i = 0; i < list.Count; i++ )
				{
					FilterConditionRow row = (FilterConditionRow)list[ i ];					

					object operatorValue = row.Operator;

					if ( operatorValue is FilterComparisionOperator &&
						Enum.IsDefined( typeof( FilterComparisionOperator ), operatorValue ) )
					{
						FilterComparisionOperator comparisionOperator = (FilterComparisionOperator)operatorValue;

						object operandValue = row.Operand;

						if ( operandValue is BlanksClass )
						{
							columnFilter.FilterConditions.Add( comparisionOperator, System.DBNull.Value );
							columnFilter.FilterConditions.Add( comparisionOperator, "" );
							columnFilter.FilterConditions.Add( comparisionOperator, null );
						}
						else if ( operandValue is SpecialOperandClass )
						{
							SpecialOperandClass sc = (SpecialOperandClass)operandValue;

							if ( SpecialOperandClass.SpecialOperandType.Regex == sc.OperandType )
							{
								columnFilter.FilterConditions.Add( comparisionOperator, sc.ToString( ) );
							}
							else if ( SpecialOperandClass.SpecialOperandType.Wildcard == sc.OperandType )
							{
								columnFilter.FilterConditions.Add( comparisionOperator, sc.ToString( ) );
							}
							else
							{
								Debug.Assert( false, "Unknown special operand !" );
							}
						}
                        // MBS 6/22/09 - TFS18639
                        else if (operandValue is ErrorsClass)
                        {
                            Debug.Assert(comparisionOperator == FilterComparisionOperator.Equals || comparisionOperator == FilterComparisionOperator.NotEquals);
                            columnFilter.FilterConditions.Add(new DataErrorFilterCondition(comparisionOperator == FilterComparisionOperator.Equals));
                        }
                        else
                        {
                            if (operandValue is EmptyStringClass)
                                operandValue = "";
                            else if (operandValue is DBNullClass)
                                operandValue = System.DBNull.Value;
                            // SSP 4/7/05 BR03272
                            // Added ColumnValueListDataValue class. Used by custom filter dialog to
                            // display the caption of a column into the cell instead of the key.
                            //
                            else if (operandValue is ColumnValueListDataValue)
                                operandValue = ((ColumnValueListDataValue)operandValue).column;

                            columnFilter.FilterConditions.Add(comparisionOperator, operandValue);
                        }
					}
				}
			}

			columnFilter.LogicalOperator = 
				this.andRadioButton.Checked
				? FilterLogicalOperator.And 
				: FilterLogicalOperator.Or;
		}

		private Image GetRowSelectorImage( int width, int height )
		{
			if ( null != this.rowSelectorImage )
			{
				return this.rowSelectorImage;
			}
			
			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			//Graphics gr = DrawUtility.CreateReferenceGraphics( this.Grid );
			Graphics gr = DrawUtility.GetCachedGraphics( this.Grid );

			Bitmap bmp = new Bitmap( width, height, gr );

			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			//gr.Dispose( );
			DrawUtility.ReleaseCachedGraphics(gr);

			gr = Graphics.FromImage( bmp );

			ControlPaint.DrawButton( gr, 0, 0, bmp.Size.Width, bmp.Size.Height, ButtonState.Normal );

			gr.Dispose( );

			this.rowSelectorImage = bmp;
			
			return this.rowSelectorImage;
		}

		// SSP 10/8/03 UWG2459
		// Split the logic in ShowDialog in two mehtods. Added InitializeDialog method below to
		// do that.
		//
		internal bool InitializeDialog( UltraGridColumn column, ColumnFilter columnFilter, RowsCollection rows )
		{
			// SSP 8/6/03
			// Implemented code to show the cell values in the operator column of the row filter dialog.
			// Clear out the cached cell values.
			//
			this.cachedAllCellValues = this.cachedFilteredInCellValues = null;

			Debug.Assert( null != column, "Null column passed in." );

			if ( null == column )
				return false;

			Debug.Assert( null != column.Band, "No band associated with the column !" );
			Debug.Assert( column.Band.Columns.Contains( column ), "Invalid column !" );

			if ( null == column.Band || !column.Band.Columns.Contains( column ) )
				return false;

			this.internalColumn = column;

			// SSP 8/5/03
			// Changed the custom row filter dialog's Operator field to display all the cell values.
			// Added rows parameter to the Show method.
			//
			this.rows = rows;

			// JAS 11/4/04 UWG3775 - Multiline column captions must be converted to singleline.
			// this.Text = SR.GetString( "RowFilterDialogTitlePrefix" ) + " " + column.Header.Caption;
			// SSP 4/7/05 BR03272
			// Use ColumnValueListDataValue.GetCaptionAsSingleLine instead of duplicating code.
			//
			//string columnName = column.Header.Caption == null ? String.Empty : column.Header.Caption.Replace( Environment.NewLine, " " );
			string columnName = ColumnValueListDataValue.GetCaptionAsSingleLine( column );
			this.Text = SR.GetString( "RowFilterDialogTitlePrefix" ) + " " + columnName;


			ArrayList list = new ArrayList( );
			list.Add( new FilterConditionRow( ) );
			this.Grid.DataSource = list;

			this.PopulateGrid( columnFilter );

			this.okButton.Enabled = false;
			
			// Hide the grid's caption. When set to null, the grid won't add the
			// caption element.
			//
			this.Grid.Text = string.Empty;
			this.Grid.UpdateMode = UpdateMode.OnCellChangeOrLostFocus;
			UltraGridLayout layout = this.grid.DisplayLayout;
			layout.Override.AllowColSizing = AllowColSizing.None;
			layout.Override.HeaderClickAction = HeaderClickAction.Select;
			layout.CaptionAppearance.TextHAlign = Infragistics.Win.HAlign.Left;
			layout.Override.BorderStyleCell = UIElementBorderStyle.Inset;
			layout.Override.RowSpacingAfter = 2;
			layout.MaxColScrollRegions = 1;
			layout.MaxRowScrollRegions = 1;
			UltraGridColumn operatorColumn = layout.SortedBands[0].Columns["Operator"];
			operatorColumn.Header.Caption = SR.GetString( "RowFilterDialogOperatorHeaderCaption" );

            // MRS 1/11/2008 - BR29592
            //operatorColumn.Header.Caption = SR.GetString( "RowFilterDialogOperandHeaderCaption" );

			operatorColumn.ValueList = this.GetOperatorsValueList( );
			operatorColumn.Style = ColumnStyle.DropDownList;
			operatorColumn.ButtonDisplayStyle = ButtonDisplayStyle.Always;				
			// SSP 8/6/03
			// Removed columns and column parameter.
			//
			//this.Grid.DisplayLayout.Bands[0].Columns[ "Operand"  ].ValueList = this.GetOperandsValueLists( column.Band.Columns, column );
			UltraGridColumn operandColumn = layout.SortedBands[0].Columns["Operand"];
			operandColumn.ValueList = this.GetOperandsValueLists( );

            // MRS 1/11/2008 - BR29592
            operandColumn.Header.Caption = SR.GetString("RowFilterDialogOperandHeaderCaption");

			// SSP 8/6/07 BR25080
			// If there's a data filter on the editor, we have to display the cell value in the operand cell with
			// the data filter conversions applied to it. However since the 
			// --------------------------------------------------------------------------------------------------
			UltraGridColumn columnFilterColumn = null != columnFilter ? columnFilter.Column : null;
			EmbeddableEditorBase columnFilterColumnEditor = null != columnFilterColumn ? columnFilterColumn.Editor : null;
			if ( null != columnFilterColumnEditor && null != columnFilterColumnEditor.DataFilter )
                // MBS 6/22/09 - TFS18639
				//this.PopulateOperandsValueList( false, false, false, false, false, true );
                this.PopulateOperandsValueList(false, false, false, false, false, true, false);
			// --------------------------------------------------------------------------------------------------
            //
            // MBS 9/12/08 - TFS6442
            // If we're showing the dialog when we've already filtered by a value, particularly in a column
            // that has a ValueList applied to it, we need to make sure that we build the operands list
            // now, otherwise we'll show the underlying value (i.e. if the user has a VL with a
            // display value of "Sunday" and an underlying value of "1", we'd show the "1" until we
            // properly build this list).
            else if (this.Grid.Rows.Count > 0)
                this.PopulateOperandsCellHelper(this.Grid.Rows[0].Cells[operandColumn]);

			// SSP 5/14/04 UWG3230
			// We needed to do this to fix the problem caused by the fix for UWG2512.
			// Basically when the column's data type is object which it is in this case,
			// the EmbeddableEditorOwner's implementation of GetType will end up returning
			// typeof( string ). We don't want string, we want object as the data type
			// for the proper working of the value list assigned to the column. To do that
			// we have to create an owner that will return typeof( object ) from the 
			// GetType.
			//
			// ----------------------------------------------------------------------------
			Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings editorSettings = 
				new Infragistics.Win.UltraWinEditors.DefaultEditorOwnerSettings( );
			editorSettings.DataType = typeof( object );
			operandColumn.Editor = new EditorWithCombo( new Infragistics.Win.UltraWinEditors.DefaultEditorOwner( editorSettings ) );
			// ----------------------------------------------------------------------------
			operandColumn.ButtonDisplayStyle = ButtonDisplayStyle.Always;

            // MBS 2/1/07 BR19707
            // We should be using the localized string for this dialog for DBNull
            //this.Grid.DisplayLayout.Bands[0].Columns[ "Operand" ].NullText = "(DBNull)";
			operandColumn.NullText = String.Format( "({0})", SR.GetString( "RowFilterDialogDBNullItem" ) );

			// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
			// Obsolted AutoFitColumns and added AutoFitStyle property.
			//
			//this.Grid.DisplayLayout.AutoFitColumns = true;
			layout.AutoFitStyle = AutoFitStyle.ResizeAllColumns;

			layout.BorderStyle = UIElementBorderStyle.None;
			layout.BorderStyleCaption = UIElementBorderStyle.None;
			layout.Override.BorderStyleRow = UIElementBorderStyle.None;
			layout.Override.BorderStyleCell = UIElementBorderStyle.InsetSoft;
			layout.Override.BorderStyleHeader = UIElementBorderStyle.None;
			layout.Override.RowSizing = RowSizing.Fixed;

			layout.Override.RowSelectorAppearance.Image = this.GetRowSelectorImage( UltraGridBand.PRE_ROW_SELECTOR_WIDTH - 1, this.Grid.DisplayLayout.SortedBands[0].RowHeightResolved - 1 );
				
			// SSP 8/6/03 UWG2073
			//
			layout.TabNavigation = TabNavigation.NextControlOnLastCell;

			this.columnFilter = columnFilter;

			return true;
		}

		internal DialogResult InternalShowDialog( )
		{
			try
			{
				IWin32Window ownerWindow = null;

				try
				{
					if ( null != this.ownerGrid )
						ownerWindow = this.ownerGrid.FindForm( );
				}
				catch ( Exception )
				{
				}

				this.UpdateConditionLabelText( );

				DialogResult result;
				if ( null != ownerWindow )
					result =  this.ShowDialog( ownerWindow );
				else
					result = this.ShowDialog( );

				this.internalColumn = null;

				if ( DialogResult.OK == result )
				{
					try
					{
						if ( null != this.Grid )
						{
							UltraGridCell cell = this.Grid.ActiveCell;

							if ( null != cell && cell.IsInEditMode )
							{
								cell.ExitEditMode( false, false );
							}
						}
					} 
					catch ( Exception exc )
					{
						Debug.Assert( false, exc.Message );
					}

					this.GetColumnFilters( this.columnFilter );
				}

				return result;
			}
			catch ( Exception e )
			{
				Debug.Assert( false, "Exception thrown while trying to initialize the grid.\n" + e.Message );
			}

			return DialogResult.None;			
		}

		// SSP 10/19/05 BR06776
		// Added public ShowDialog method.
		// 
		/// <summary>
		/// Initializes the dialog with the specified parameters and displays it.
		/// </summary>
		/// <param name="columnFilter">The filter conditions of this columnFilter will be 
		/// displayed in the dialog. When the user okays the dialog, the columnFilter will 
		/// be modified to reflect any changes.</param>
		/// <param name="rows">The operand fields typically display a list of unique cell values.
		/// These cell values will be retrieved from this rows collection. This parameter is
		/// optional. You can specify it as null.</param>
		/// <returns>Returns the dialog result the indicates whether the user clicked Ok or Cancel.</returns>
		public DialogResult ShowDialog( ColumnFilter columnFilter, RowsCollection rows )
		{
			this.InitializeDialog( columnFilter.Column, columnFilter, rows );
			return this.InternalShowDialog( );
		}

		private void addConditionButton_Click(object sender, System.EventArgs e)
		{
			ArrayList list = this.Grid.DataSource as ArrayList;

			Debug.Assert( null != list, "At this point the grid must have been bound to an array list !" );

			if ( null != list )
			{
				list.Add( new FilterConditionRow( ) );

				this.Grid.Rows.Refresh( RefreshRow.RefreshDisplay, false );
			}
			else
			{
				list = new ArrayList( );

				list.Add( new FilterConditionRow( ) );

				this.Grid.DataSource = list;
			}
		}

		private void andRadioButton_CheckedChanged(object sender, System.EventArgs e)
		{
			this.UpdateConditionLabelText( );
		}

		// SSP 2/25/03 UWG1975
		// Added ValidateInput method.
		//
		private bool ValidateInput( )
		{
            // JDN 12/20/04 BR00867 - Make Sure we are out of Edit Mode so the operand is commited if the Enter key
            // is used instead of clicking the OK button
            this.Grid.PerformAction( UltraGridAction.ExitEditMode, false, false );

			ArrayList list = this.Grid.DataSource as ArrayList;

			for ( int i = 0; null != list && i < list.Count; i++ )
			{
				FilterConditionRow row = list[ i ] as FilterConditionRow;

				if ( null == row || !( row.Operator is FilterComparisionOperator ) )
					continue;

				FilterComparisionOperator filterOperator = (FilterComparisionOperator)row.Operator;

				if ( FilterComparisionOperator.Match == filterOperator )
				{
					string exp = null != row.Operand ? row.Operand.ToString( ) : "";
					try
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Remove unused locals
						//System.Text.RegularExpressions.Regex rx = 
							new System.Text.RegularExpressions.Regex( row.Operand.ToString( ) );
					}
					catch ( Exception exc )
					{
						this.Grid.ClearSelectedCells( );
						this.Grid.Rows[i].Cells["Operand"].Selected = true;

						MessageBox.Show( this, 
							SR.GetString( "RowFilterRegexError", exp ) + "\n" + exc.Message,
							SR.GetString( "RowFilterRegexErrorCaption" ),
							MessageBoxButtons.OK,
							MessageBoxIcon.Error );

						return false;
					}
				}

                // JDN 12/20/04 BR00867
                // Ensure the search pattern is valid
                if ( FilterComparisionOperator.Like == filterOperator )
                {
                    string exp = null != row.Operand ? row.Operand.ToString( ) : "";
                    try
                    {
                        Microsoft.VisualBasic.CompilerServices.StringType.StrLike( "testString", row.Operand.ToString( ), Microsoft.VisualBasic.CompareMethod.Text );
                    }
                    catch ( Exception exc )
                    {
                        this.Grid.ClearSelectedCells( );
                        this.Grid.Rows[i].Cells["Operand"].Selected = true;

                        MessageBox.Show( this, 
                            SR.GetString( "RowFilterPatternError", exp ) + "\n" + exc.Message,
                            SR.GetString( "RowFilterPatternCaption" ),
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error );

                        return false;
                    }
                }
			}

			return true;
		}

		// SSP 2/25/03 UWG1975
		// Hook into Closing to perform input validation. Regex constructor throws an exception when parsing
		// expressions in which "*" follows nothing (like in "*test") rather then treating it as a literal.
		//
		private void CustomRowFiltersDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if ( ! this.cancellingDialog && ! this.ValidateInput( ) )
			{
				this.cancellingDialog = true;
				e.Cancel = true;
			}
		}

		// SSP 2/25/03 UWG1975
		//
		private void okButton_Click(object sender, System.EventArgs e)
		{
			this.cancellingDialog = false;
		}

		// SSP 2/25/03 UWG1975
		//
		private void cancelButton_Click(object sender, System.EventArgs e)
		{
			this.cancellingDialog = true;
		}

		// SSP 8/6/03 UWG2106
		// Added Delete button to the dialog fro deleting the item.
		//
		private void deleteButton_Click(object sender, System.EventArgs e)
		{
			this.Grid.DeleteSelectedRows( false );
			this.Grid.ClearSelectedRows( );
			this.RefreshDeleteButtonEnabled( );
		}
	}
}
