#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	///	Used as the control for the drop down portion
	///	of the UltraCombo control.
	/// </summary>
	[ ToolboxItem(false) ]
	internal class ComboDropDownControl : Control, 
		IUltraControlElement	// AS 12/5/02
	{
		private UltraCombo	combo;
		
		/// <summary>
		/// Constructor.
		/// </summary>
		internal ComboDropDownControl( UltraCombo combo )
		{
			this.combo			= combo;
           
			// Set the appropriate control styles
			//
			this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
			this.SetStyle( ControlStyles.ResizeRedraw, true );
			this.SetStyle( ControlStyles.Selectable, false );

			// AS 2/9/04 DNF148
			//this.SetStyle( ControlStyles.DoubleBuffer, false );
            this.SetStyle(UltraGridBase.DoubleBufferControlStyle, true);

            this.SetStyle( ControlStyles.StandardClick, true );
			this.SetStyle( ControlStyles.StandardDoubleClick, true );
		}

        // JAS 10/11/04 - Since ComboDropDownControl is not derived from UltraControlBase, we 
        // needed a way to suppress the call to Refresh that Control.ScrollWindow causes.  Normally
        // UltraControlBase would handle this suppression of Refresh() for us.  If
        // we are scrolling, then we need to ignore the call to Refresh because at the time that
        // ShowWindow() calls this method, the elements are not ready to be drawn.
        public override void Refresh()
        {
            // If we are scrolling, then ignore the call to Refresh.
            if( this.combo != null && 
                this.combo.DisplayLayout != null && 
                !this.combo.DisplayLayout.IsScrolling )
                base.Refresh();
        }


        /// <summary>
		/// Called when grid itself resizes.
		/// </summary>
        /// <param name="eventArgs">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnResize(System.EventArgs eventArgs )
		{
			// SSP 10/15/06 BR15686
			// Enclosed the existing code into the if block. Don't dirty the metrics
			// if we are currently in the process of initializing the metrics.
			// 
			UltraGridLayout layout = this.combo.DisplayLayout;
			if ( null != layout && ! layout.Disposed && ! layout.ColScrollRegions.InitializingMetrics )
			{
				//dirty grid and both column and row scroll regions
				layout.DirtyGridElement();
				layout.ColScrollRegions.DirtyMetrics( );
				layout.RowScrollRegions.DirtyAllVisibleRows( );
			}

			base.OnResize( eventArgs );
		}

        /// <summary>
        /// Calls the ControlUIElement's draw method
        /// </summary>
        protected override void OnPaint(PaintEventArgs pe)
        {
            if ( null != this.combo.DisplayLayout.UIElement )
            {
				// JJD 10/31/01
				// Set drawing state on layout which is needed to perform
				// cell appearance cacahing optimizations
				//
				this.combo.DisplayLayout.InitializeDrawingFlag( true );

				try
				{
					
					this.combo.DisplayLayout.UIElement.Draw( pe.Graphics, 
						pe.ClipRectangle,
						// only double buffer if the control is not
						// already double buffered
                        !this.GetStyle(UltraGridBase.DoubleBufferControlStyle),
                        this.combo.AlphaBlendMode );
				}
				finally
				{
					// JJD 10/31/01
					// Reset drawing state on layout
					//
					this.combo.DisplayLayout.InitializeDrawingFlag( false );
				}
           }
        }
 
        /// <summary>
        /// Does nothing since the background is drawn in OnPaint
        /// </summary>
        protected override void OnPaintBackground(PaintEventArgs pe)
        {
        }
        
		/// <summary>
		/// Returns the cursor that is to be used for this control.
		/// </summary>
		public override Cursor Cursor
		{
			get
			{
				// SSP 1/10/02 UWG702
				// Overrode the cursor property for this control (which is the dropdown
				// portion of the combo).
				//

				if ( null != this.combo && 
					 null != this.combo.DisplayLayout &&
					 null != this.combo.DisplayLayout.UIElement )
				{
					AppearanceData appData = new AppearanceData( );

					ResolveAppearanceContext context = 
						new ResolveAppearanceContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridLayout ), AppearancePropFlags.Cursor );
					
					this.combo.DisplayLayout.ResolveAppearance( ref appData, ref context );

					if ( null != appData.Cursor )
						return appData.Cursor;
				}

				// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support HeaderClickAction in Combo and DropDown
//				// Use the base cursor if not set on the appearance
//				//
//				return base.Cursor;
				return ((IUltraControlElement)this).MainUIElement.CurrentCursor;
			}
		}

		// AS 12/5/02
		// Exposed the uielement so the UIElementViewer could recognize the control.
		#region IUltraControlElement
		ControlUIElementBase IUltraControlElement.MainUIElement
		{
			get 
			{ 
				if (this.combo == null || this.combo.DisplayLayout == null)
					return null;

				return this.combo.DisplayLayout.UIElement; 
			}
		}
		#endregion //IUltraControlElement

		// AS 1/7/04 Accessibility
		#region CreateAccessibilityInstance

		/// <summary>
		/// Creates a new accessibility object for the control.
		/// </summary>
		/// <returns>A new <see cref="AccessibleObject"/> for the control.</returns>
		protected override System.Windows.Forms.AccessibleObject CreateAccessibilityInstance()
		{
			return this.combo.CreateAccessibilityInstance(this);
		}
		#endregion CreateAccessibilityInstance

	}
}
