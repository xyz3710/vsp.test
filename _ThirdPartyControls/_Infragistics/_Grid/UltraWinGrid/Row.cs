#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Collections;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Security.Permissions;
    using System.Collections.Generic;

	/// <summary>
	/// Represents a row from the attached data source.
	/// </summary>
	

	public class UltraGridRow : ActivateableGridItemBase,		
		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//	
		//IScrollableRowCountManagerOwner, 
		IUIElementTextProvider,
		// SSP 4/6/04 - Virtual Mode related
		// Implemented Infragistics.Shared.ISparseArrayMultiItem interface on the rows collection.
		//
		Infragistics.Shared.ISparseArrayMultiItem
	{
		private UltraGridBand    band;

		// JJD 12/15/03 - Added Accessibility support
		private AccessibleObject accessibilityObject;

		// JJD 1/06/04 - Accessibility
		private AccessibleObject colHeadersAccessibilityObject;
		
		// NOTE: listObject member var is used only for caching. Should not
		// be relied upon as far as retrieving and setting data goes.
		// Use the ListObject property.
		//
		private object  listObject  = null;

		// SSP 1/3/06 BR08526
		// Whidbey introduced an issue with data filtering the data source. When the data filter
		// is changed, the DataRow of the DataRowView objects change without any notifications
		// and therefore we can't rely on them for the purposes of reusing of the old rows. As a
		// result we have to store the DataRow objects and use them for the purposes of reusing 
		// the rows.
		//
		// SSP 11/16/06 BR16418
		// Do this for CLR1.x versions as well since one could use CLR1.x assemblies with CLR2 
		// version (VS2005).
		// 
//#if CLR2
		internal object listObject_ForComparison = null;
//#endif // CLR2

		// SSP 4/5/04 - Virtual Binding Related Optimizations
		// Commented out cachedListIndex declaration.
		//
		//private int		cachedListIndex	= -1;

		private int		lastVerifiedVersion = 0;
		private Infragistics.Win.UltraWinGrid.RowsCollection parentCollection = null;
		private Infragistics.Win.UltraWinGrid.CellsCollection cells = null;
		internal Infragistics.Win.UltraWinGrid.ChildBandsCollection	childBands = null;
		private int     rowSpacingBefore = -1;
		private int     rowSpacingAfter = -1;

		// SSP 4/5/04 - Virtual Binding Related Optimizations
		//
		//private int     index = 0;

		private int     synchronizedRowHeightVersion = 0;

		private Activation		activation = 0;	//AllowEdit;
		private Infragistics.Win.Appearance	appearance = null;
		private Infragistics.Win.Appearance	cellAppearance = null;
    
		private bool            hidden = false;
		internal bool			deleted = false;
		private bool	        autoPreviewHidden = false;
		private bool            resizing = false;
		
		private bool			dataChanged = false;

		// SSP 11/17/05 BR07772
		// If the column is bound then we need to honor the UpdateData method
		// call even if modifications were made via the calc manager.
		// 
		internal bool dataChangedViaCalcManager = false;

		private string			description = string.Empty;
		private string			cardCaption	= string.Empty;
		private bool			expanded = false;
		
		// SSP 2/19/04 - Virtual Mode - Optimization
		//
		//private int				cachedScrollRowPositionVersion = -1;
		//private int				cachedScrollRowPosition = 0;

		// JJD 12/17/01
		// Added logic to cache a row's card height during a 
		// drawing operation
		//
		private int				drawSequence = 0;
		private int				cachedCardHeight = 0;

		//		private ExpandOnLoad	expandChildRowsOnLoad = 0;
		private ShowExpansionIndicator	expansionIndicator = 0;	//Default;
		private DefaultableBoolean		fixedHeight = DefaultableBoolean.Default;	//Default;
		private Infragistics.Win.Appearance	rowSelectorAppearance = null;
		private Infragistics.Win.Appearance	previewAppearance = null;
		//		private bool			selected = false;
		internal bool			inInitializeRowEvent = false;
		internal bool			initializeRowEventFired = false;
		private bool			synchronizing = false;
		//private bool            autoSized = false;

		private int             rootHeight = 0;

		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//	
		//private ScrollableRowCountManager scrollableRowCountManager = null;

		private bool			isAutoSized = false;

		private bool			isAddRow = false;
		
		// SSP 1/22/02 UWG962
		// A flag used to keep track of whether EndEdit has been called
		// on the row or not.
		// 
		private bool			endEditCalled = false;

		//	BF 8.12.02	UWG1429
		// MRS 5/21/04 - UWG2915
		// Made this interal so we can use it in CopyRowPropertiesFrom
		//private bool heightSetManually = false;
		internal bool heightSetManually = false;

		// SSP 3/21/02
		// Added vars for row filtering code
		//
		// SSP 11/22/04 BR00930
		// 0 means filtered out, 1 means not filtered out, -1 means filter has
		// never been evaluated on this row.
		// 
		//private bool cachedIsFilteredOut = false;
		private const sbyte FILTERED_OUT = 0, FILTERED_IN = 1, FILTERS_NEVER_EVALUATED = -1;
		private sbyte cachedIsFilteredOut = FILTERS_NEVER_EVALUATED;

		// SSP 10/27/04 - UltraCalc
		// Moved the filter verification logic into the row collection. This is so
		// that we can evaluate filters at once on all the rows of a row collection
		// and fire rows synced event to the calc manager instead of having to fire
		// row deleted/added event for every row whose hidden state changes.
		// Commented out following variables.
		//
		

		// SSP 6/27/02
		// Compressed card view.
		//
		private bool isCardCompressed = true;

		// MRS 5/6/05 - Moved this into DataBindingUtils in Win so it can be
		// used by the tree, also. 
//		// SSP 7/17/02 UWG1377
//		// A table to store sample data for column data types.
//		//
//		private static Hashtable sampleData = null;

		// SSP 4/7/04 - UltraDataSource - Virtual Mode related
		//
		internal object unsortedRows_SparseArrayOwnerData = null;
		internal object sortedRows_SparseArrayOwnerData = null;

		// SSP 6/30/04 - UltraCalc
		//
		private RowReferenceBase calcReference = null;

		// SSP 2/20/04 - Virtual Mode 
		//
		

		// SSP 8/16/04 - Optimizations
		// Indicates how many visible rows are currently associated with the row.
		// 0 means none and that means that the row is not visible in any of the
		// scroll regions. If it's 1 then it means it's visible in 1 scroll region
		// and 2 means it's visible in 2 scroll regions etc.
		//
		private byte visibleRowCounter = 0;

		// SSP 12/22/04 - IDataErrorInfo Support
		//
		private const AppearancePropFlags DATA_ERROR_APPEARANCE_EXCLUDE_PROPS = 
			AppearancePropFlags.Image | AppearancePropFlags.ImageHAlign 
			| AppearancePropFlags.ImageVAlign | AppearancePropFlags.ImageAlpha;

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		private DefaultableBoolean allowFixing = DefaultableBoolean.Default;

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		private string toolTipText = null;

		// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// 
		private RowAutoSizeLayoutManagerHolder rowAutoSizeLayoutManagerHolder = null;

        // MBS 3/20/08 - RowEditTemplate NA2008 V2
        // We need to know if the active row was changed due to a change in the position
        // of the currency manager so that we can force the template to close
        internal bool forceTemplateCloseOnActionRowChanged;

		// SSP 8/17/01 UWG132
		// We have duplicate properties DataChanged and IsRowDirty for
		// keeping track if the data has changed and not committed to the
		// backend. So I took this out.
		//private bool            isRowDirty = false;

		internal UltraGridRow( UltraGridBand band,
			//object listObject, 
			// SSP 4/5/04 - Virtual Binding Related Optimizations
			// Took out listIndex parameter.
			//
			//int listIndex,
			Infragistics.Win.UltraWinGrid.RowsCollection parentCollection )
		{
			//this.autoSized = false;
			this.isAutoSized = false;

			if ( null == band )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_225"));

			this.band = band;
			//this.listObject        = listObject;

			// SSP 4/5/04 - Virtual Binding Related Optimizations
			//
			

			this.parentCollection  = parentCollection;

			// JJD 8/21/01 - UWG188
			// Call InitRootHeight to calculate the height of the row
			//
			// SSP 4/5/04 - Virtual Binding Related Optimizations
			// We should be initialzing height when we need it.
			// Commented out the call to InitRootHeight method.
			//
			//this.InitRootHeight( );

			//this.Reset();
		}

		internal void InitRowsCollection( RowsCollection parentCollection )
		{
			this.parentCollection = parentCollection;

			// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
			//
			//if ( this.scrollableRowCountManager != null )
			//	this.scrollableRowCountManager.ParentManager = parentCollection.ScrollableRowCountManager;
		}


		

		

		/// <summary>
		/// Returns the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand"/> that the object belongs to, if any. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Band</b> property of an object refers to a specific band in the grid as defined by an UltraGridBand object. You use the <b>Band</b> property to access the properties of a specified UltraGridBand object, or to return a reference to the UltraGridBand object that is associated with the current object.</p>
		/// <p class="body">UltraGridBand objects are the foundation of the hierarchical data structure used by UltraWinGrid. Any row or cell in the grid must be accessed through its UltraGridBand object. Bands are also used to apply consistent formatting and behavior to the rows that they comprise. An UltraGridBand object is used to display all the data rows from a single level of a data hierarchy. UltraGridBand objects contain multiple sets of child UltraGridRow objects that actually display the data of the recordset. All of the rows that are drawn from a single Command in the DataEnvironment make up a band.</p>
		/// <p class="body">The rows of a band are generally displayed in groups of one more in order to show rows from subsequent bands that are linked to rows in the current band via the structure of the data hierarchy. For example, if a hierarchical recordset has Commands that display Customer, Order and Order Detail data, each one of these Commands maps to its own UltraGridBand in the UltraWinGrid. The rows in the Customer band will appear separated by any Order data rows that exist for the customers. By the same token, rows in the Order band will be appear separated to make room for Order Detail rows. How this looks depends on the ViewStyle settings selected for the grid, but the concept of visual separation is readily apparent when the UltraWinGrid is used with any hierarchical recordset.</p>
		/// <p class="body">Although the rows in a band may appear to be separated, they are treated contiguously. When selecting a column in a band, you will see that the cells of that column become selected in all rows for the band, regardless of any intervening rows. Also, it is possible to collapse the hierarchical display so that any children of the rows in the current band are hidden.</p>
		/// </remarks>
		public override Infragistics.Win.UltraWinGrid.UltraGridBand Band
		{
			get
			{				
				return this.band;
			}
		}

		// SSP 8/14/03 - Optimization
		//
		internal UltraGridBand BandInternal
		{
			get
			{
				return this.band;
			}
		}

		internal override bool PivotOnRowScrollRegion { get { return true; } 
		}
		internal override bool PivotOnColScrollRegion { get { return false; } 
		}

		/// <summary>
		/// True only if this row and all of its ancestor rows are still valid (read-only).
		/// </summary>
		public virtual bool IsStillValid 
		{ 
			get 
			{
				try
				{
					// SSP 4/30/05 - NAS 5.2 Fixed Rows_
					// Return false if the row is disposed.
					//
					if ( this.Disposed )
						return false;

					// If the index is less than 0 then the row
					// doesn't exist in its parent collection any more
					// so return false
					//
					// SSP 11/11/03 Add Row Feature
					// A template row's index will be -1.
					//
					//if ( this.Index < 0  || this.IsDeleted )
					// SSP 4/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					// Use the GetRowIndex instead of Index which takes into account special
					// rows as well.
					//
					//if ( ( this.Index < 0 && ! this.IsTemplateAddRow ) || this.IsDeleted )
					int index = null != this.ParentCollection ? this.ParentCollection.GetRowIndex( this, true ) : -1;
					// SSP 8/26/06 BR05797
					// If template-add row feature is not enabled then GetRowIndex will return -1 for a template
					// add-row. Therefore we still need to check for it.
					// 
					//if ( this.IsDeleted || index < 0 )
                    //
                    // MBS 11/13/07 - BR28423
                    // We should also be checking whether we're working with a filter row, since a filter row
                    // should always be considered as valid, regardless of whether it's hidden, unless disposed
                    //
                    //if ( ( index < 0 && ! this.IsTemplateAddRow) || this.IsDeleted )
                    if ((index < 0 && !this.IsTemplateAddRow && !this.IsFilterRow) || this.IsDeleted)
						return false;

					// If the row has a parent return this property
					// from the parent. This effectively walks up the
					// parent chain.
					//
					// SSP 4/22/03 - Optimizations
					// Parent row does verification against group-by version numbers.
					//
					//if ( this.ParentRow != null )
					//	return this.ParentRow.IsStillValid;
					UltraGridRow parentRow = this.ParentRow;
					if ( parentRow != null )
						return parentRow.IsStillValid;

					// Since this is a top level row only return true
					// if the parentcollection is the main rows
					// collection off the layout
					//
					return ( this.parentCollection == this.Layout.Rows );
				}
				catch(Exception)
				{
					// return false if an error was thrown
					//
					return false;
				}
			} 
		}

		/// <summary>
		/// Returns whether this row is being displayed in Card View mode. This property is read-only.
		/// </summary>
		/// <remarks>
		/// <p class="body">If the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand "/> object containing this mode has its <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand.CardView"/> property set to True, then the band is in Card View mode and this property will return True.</p>
		/// <seealso cref="UltraGridBand.CardView"/>
		/// <seealso cref="UltraGridBand.CardSettings"/>
		/// </remarks>
		public virtual bool IsCard 
		{ 
			get 
			{ 
				// SSP 8/28/04
				//
				//return this.band.CardView;
				return null != this.band && this.band.CardView;
			} 
		}

		#region Compressed Card View

		#region IsCardStyleCompressed

		internal bool IsCardStyleCompressed
		{
			get
			{
				if ( null != this.Band && 
					// SSP 8/28/04
					// Added a check for IsCard so that we don't have to do it everywhere.
					//
					this.IsCard &&
					// SSP 2/28/03 - Row Layout Functionality
					// Added StyleResolved property because in row layout mode we need to resolve VariableHeight
					// to StandardLabels card style so use the StyleResolved instead of the Style property.
					//
					CardStyle.Compressed == this.Band.CardSettings.StyleResolved					
					)
					return true;

				return false;
			}
		}

		#endregion // IsCardStyleCompressed

		#region IsCardStyleVariableHeight

		// SSP 8/29/04 UWG3610
		// Added IsCardStyleVariableHeight property.
		//
		internal bool IsCardStyleVariableHeight
		{
			get
			{
				return this.IsCard && this.band.HasCardSettings 
					&& CardStyle.VariableHeight == this.band.CardSettings.StyleResolved;
			}
		}

		#endregion // IsCardStyleVariableHeight

		#region IsCardCompressed

		/// <summary>
		/// Indicates whether the card is compressed or expanded.
		/// </summary>
		/// <remarks>
		/// <p>This property only applies when the band is in card view (IsCard property returns true for this row) and the <see cref="UltraGridCardSettings.Style"/> is set to <b>Compressed</b>. When these conditions are not met, returned value is undefined.</p>
		/// <p>Also this does not apply to group by rows and for group by rows returned value will be undefined.</p>
		/// <seealso cref="UltraGridRow.IsCard"/>
		/// <seealso cref="UltraGridBand.CardView"/>
		/// <seealso cref="UltraGridBand.CardSettings"/>
		/// <seealso cref="UltraGridCardSettings.Style"/>
		/// <seealso cref="CardStyle"/>
		/// </remarks>
		public virtual bool IsCardCompressed
		{
			get
			{
				// SSP 4/3/03 - Row Layout Functionality
				// In design mode, always show the cards as expanded.
				//
				UltraGridBand band = this.Band;
				if ( null != band && null != band.Layout && band.Layout.Grid.DesignMode )
					return false;

				if ( this.IsCard && this.IsCardStyleCompressed )
					return this.isCardCompressed;

				return false;
			}
			set
			{
				if ( this.isCardCompressed != value )
				{
					// Exit the edit mode before toggling the card's expansion state.
					//
					Debug.Assert( null != this.Band && null != this.Band.Layout, "No layout !" );

					if ( null != this.Band && null != this.Band.Layout )
					{
						UltraGridCell activeCell = this.Band.Layout.ActiveCell;

						if ( null != activeCell && activeCell.IsInEditMode )
						{
							activeCell.ExitEditMode( );

							// If exitting the edit mode was cancelled, then return without
							// doing anything because we don't want to expand or collapse a
							// card while a cell is in edit mode.
							//
							if ( activeCell.IsInEditMode )
								return;
						}
					}

					this.isCardCompressed = value;
                    
					// Dirty the cached card height
					//
					this.cachedCardHeight = -1;
                    
					if ( null != this.Layout )
						this.Layout.DirtyGridElement( );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.IsCardCompressed );
				}
			}
		}

		#endregion // IsCardCompressed

		#region ToggleCardCompressedState

		// SSP 7/30/03 UWG2430
		// This method gets called when the card caption is double clicked and also when
		// the card expansion indicator is clicked. We should only select when the card
		// caption is double clicked and not select when the expansion indicator is
		// clicked.
		// Added an overload ToggleCardCompressedState that takes in a bool parameter 
		// indicating whether we should select the card or not.
		//
		//internal void ToggleCardCompressedState( )
		internal void ToggleCardCompressedState( bool selectCard )
		{
			UltraGrid grid =
				null != this.Band.Layout 
				? this.Band.Layout.Grid as UltraGrid
				: null;

			// JDN 11/17/04 CardCompressedStateChanged events
			CardCompressedState newCardCompressedState;
			if ( this.IsCardCompressed )
				newCardCompressedState = CardCompressedState.Expanded;
			else
				newCardCompressedState = CardCompressedState.Compressed;

			// Fire BeforeCardCompressedStateChanged event
			if ( grid != null )
			{              
				BeforeCardCompressedStateChangedEventArgs beforeArgs = 
					new BeforeCardCompressedStateChangedEventArgs( this, newCardCompressedState );

				grid.FireEvent( GridEventIds.BeforeCardCompressedStateChanged, beforeArgs );
                
				if ( beforeArgs.Cancel )
					return;
			}

			this.IsCardCompressed = !this.IsCardCompressed;

			// Now scroll the card into view because as a result of expanding
			// the row, it may get scrolled out of view.
			//
			RowScrollRegion rsr = this.Band.Layout.ActiveRowScrollRegion;

			Debug.Assert( null != rsr, "No row scroll region !" );
					
			if ( null != rsr )
			{
				rsr.ScrollRowIntoView( this );						
			}

			// SSP 7/30/03 UWG2430
			// This method gets called when the card caption is double clicked and also when
			// the card expansion indicator is clicked. We should only select when the card
			// caption is double clicked and not select when the expansion indicator is
			// clicked.
			// Added an overload ToggleCardCompressedState that takes in a bool parameter 
			// indicating whether we should select the card or not.
			//
			if ( selectCard )
			{
				// Select the row as well.
				//
				ISelectionStrategyProvider selectionStrategyProvider = this.Band;
				SelectionStrategyBase rowSelectionStrategy = selectionStrategyProvider.SelectionStrategyRow;

				// JDN 11/17/04 CardCompressedStateChanged events
				// moved code to beginning of method
				// UltraGrid grid =
				//      null != this.Band.Layout 
				//      ? this.Band.Layout.Grid as UltraGrid
				//      : null;

				Debug.Assert( null != rowSelectionStrategy, "No row selection strategy !" );
				Debug.Assert( null != grid, "No grid !" );

				if ( null != grid && null != selectionStrategyProvider && null != rowSelectionStrategy &&
					( rowSelectionStrategy.IsSingleSelect || rowSelectionStrategy.IsMultiSelect ) )
				{
					grid.InternalSelectItem( this, true, true );
				}
			}

			// JDN 11/17/04 CardCompressedStateChanged events
			// Fire AfterCardCompressedStateChanged event
			if ( grid != null )
			{
				grid.FireEvent( GridEventIds.AfterCardCompressedStateChanged,
					new AfterCardCompressedStateChangedEventArgs( this, newCardCompressedState ) );
			}
		}

		#endregion // ToggleCardCompressedState

		#endregion // Compressed Card View
		
		// SSP 8/17/01 UWG132
		// We have duplicate properties DataChanged and IsRowDirty for
		// keeping track if the data has changed and not committed to the
		// backend. So I took IsRowDirty out.
		

		// SSP 12/17/01
		// Not used anywhre and don't think needed anymore.
		

		internal bool AreAllAncestorsCurrentRows
		{
			get
			{
				Infragistics.Win.UltraWinGrid.UltraGridRow parentRow = this.ParentRow;

				// top level band row
				if ( null != parentRow )
					if ( !parentRow.AreAllAncestorsCurrentRows )
						return false;


				// we don't have to worry about group by rows because they
				// are not associated with binding list
				//
				if ( this.IsGroupByRow )
					return true;
				
				BindingManagerBase bindingManager = this.BandInternal.BindingManager;

				return null != bindingManager && 
					bindingManager.Position == this.ListIndex;
			}
		}

		internal UltraGridRow FindActualAncestorRow
		{
			get
			{
				// SSP 5/8/03 - Optimizations
				//
				
				UltraGridRow parentRow = this.ParentRow;
				if ( parentRow is UltraGridGroupByRow )
					return parentRow.FindActualAncestorRow;

				return parentRow;
			}
		}

		// SSP 5/8/03 UWG2249
		// Instead of clearing the list object, decrement the verified version number. Either of
		// these will cause the row to reget the list object when the ListObject property is
		// accessed.
		// 
		
		internal void InternalDecrementVerifiedVersion( )
		{
			this.lastVerifiedVersion--;
		}

		// SSP 4/29/03 UWG1981
		// Added InternalCachedListObject property to get and set the list object without
		// verifying against the version number.
		//
		internal object InternalCachedListObject
		{
			get
			{
				return this.listObject;
			}
			set
			{
				this.listObject = value;

				// SSP 1/3/06 BR08526
				// See comments above listObject_ForComparison member declaration for more info.
				//
				// SSP 11/16/06 BR16418
				// Do this for CLR1.x versions as well since one could use CLR1.x assemblies with CLR2 
				// version (VS2005).
				// 
//#if CLR2
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.listObject_ForComparison = null != this.parentCollection
				//    ? this.parentCollection.GetObjectForRowComparision( this.listObject ) : null;
				this.listObject_ForComparison = RowsCollection.GetObjectForRowComparision( this.listObject );
//#endif // CLR2
			}
		}

		/// <summary>
		/// Returns the object corresponding to this row from the IList that the control is bound to. Returns Null if this is an UltraGridGroupByRow.
		/// </summary>
		public object ListObject
		{
			get
			{				
				// SSP 11/19/03 Add Row Feature
				// Commented out the original code and added the new code below. They do the same thing
				// (at least they should do the same thing) except that the new code is more compact
				// and it makes use of the new helper method RowsCollection.ResolvedDataList property.
				//
				// ------------------------------------------------------------------------------------
				// SSP 10/4/01 UWG483
				// See note above Rows.VerifyVersion
				//
				//if ( null == this.listObject || this.lastVerifiedVersion != this.parentCollection.veryfyVersion  )
				if ( null == this.listObject || this.lastVerifiedVersion != this.parentCollection.VerifyVersion  )
				{
					// SSP 2/1/02 UWG1017
					// If we are bound to a list that's not IBindingList, then
					// don't use the caching mechanism to cache the list objects.
					// Just fetch them from the list every time.
					//
					if ( null == this.band.BindingList )
					{
						this.listObject = null;
						try
						{
							int listIndex = this.ListIndex;

							// SSP 5/6/04 UWG3233
							// If the row has been deleted or is a group-by row then simply return null
							// without dirtying the parent collection.
							//
							if ( listIndex < 0 )
								return null;

							// SSP 11/19/04 BR00601
							// Don't reset the list object to null because the SyncRows makes use of list 
							// objects to match the rows.
							//
							// ------------------------------------------------------------------------------
							//return this.ParentCollection.Data_List[ listIndex  ];
							// SSP 1/4/05 BR08526
							// Use the InternalCachedListObject instead of the member var which takes some
							// additional actions.
							// 
							//return this.listObject = this.ParentCollection.Data_List[ listIndex  ];
							return this.InternalCachedListObject = this.ParentCollection.Data_List[ listIndex ];
							// ------------------------------------------------------------------------------
						}
						catch ( Exception exception1 )
						{
							// SSP 10/28/02 UWG1787
							// Dirty the rows if we have a valid row with an invalid list index. This is
							// a good indication that we are out of sync with the underlying binding list.
							// This is not neccessary so you can take it out if it causes problems.
							//
							if ( null != this.parentCollection && this.IsStillValid )
								this.parentCollection.DirtyRows( );

							// SSP 9/15/06 - NAS 6.3
							// Raise Error event.
							// 
							UltraGridLayout layout = this.Layout;
							if ( null != layout )
								layout.Grid.InternalHandleDataError( SR.GetString( "DataError_RowListObjectRetrieval" ), exception1, true );
						}
					
						return null;
					}

					// If this is a child band, then it must have a parent column and parent propery descriptor
					// associated with it.
					//
					Debug.Assert( null == this.Band.ParentBand || null != this.Band.ParentColumn && null != this.Band.ParentColumn.PropertyDescriptor );

					try
					{
						int listIndex = this.ListIndex;

						// SSP 5/6/04 UWG3233
						// If the row has been deleted or is a group-by row then simply return null
						// without dirtying the parent collection.
						//
						if ( listIndex < 0 )
							return null;

						IList list = this.parentCollection.ResolvedDataList;

						// SSP 1/4/05 BR08526
						// Use the InternalCachedListObject instead of the member var which takes some
						// additional actions.
						// 
						//this.listObject = list[ listIndex ];
						this.InternalCachedListObject = list[ listIndex ];

						// SSP 10/4/01 UWG483
						// See note above Rows.VerifyVersion
						//
						//this.lastVerifiedVersion = this.parentCollection.veryfyVersion;
						this.lastVerifiedVersion = this.parentCollection.VerifyVersion;
					}
					catch ( Exception exception2 )
					{
						//this.listObject = null;

							
						// SSP 10/28/02 UWG1787
						// Dirty the rows if we have a valid row with an invalid list index. This is
						// a good indication that we are out of sync with the underlying binding list.
						// This is not neccessary so you can take it out if it causes problems.
						//
						if ( null != this.parentCollection && this.IsStillValid )
							this.parentCollection.DirtyRows( );

						// SSP 9/15/06 - NAS 6.3
						// Raise Error event.
						// 
						UltraGridLayout layout = this.Layout;
						if ( null != layout )
							layout.Grid.InternalHandleDataError( SR.GetString( "DataError_RowListObjectRetrieval" ), exception2, true );
					}
				}

				return this.listObject;

				
				// ------------------------------------------------------------------------------------
			}
		}


		/// <summary>
		/// Returns the index corresponding to this row from the IList that the control is bound to. Return value of -1 indicates that a row has been deleted or doesn't exist anymore.
		/// </summary>
		// SSP 8/9/02
		// Made this public due to a customer request.
		//
		//internal int ListIndex
		public int ListIndex
		{
			get
			{
				// SSP 2/17/04 - Virtual Mode - Optimization
				// Commented out the original code and added the new code below it.
				//
				//this.ParentCollection.GetRowListIndex( this, ref this.cachedListIndex );				
				//return this.cachedListIndex;
				return this.ParentCollection.UnSortedActualRows.IndexOf( this );
			}
			// SSP 8/16/02
			// Since this property was made public, created an internal method
			// for setting the cahcedListIndex since we don not want the user
			// to do this.
			//
						
		}

		// SSP 4/5/04 - Virtual Binding Related Optimizations
		// Commented out InternalListIndex.
		//
		
		
		

		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//
		#region Commented out IScrollableRowCountManagerOwner implementation

		

		#endregion // Commented out IScrollableRowCountManagerOwner implementation

		/// <summary>
		/// Returns the UltraGridUIElement object that is associated with an object.
		/// </summary>
        /// <param name="csr">The ColScrollRegion</param>
        /// <param name="rsr">The RowScrollRegion</param>
        /// <param name="verifyElements">Indicates whether to VerifyChildElements</param>
        /// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body">The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to an UIElement object residing at specific coordinates.</p>
		/// <p class="body"><b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement.</p>
		/// <p class="body">The <b>GetUIElement</b> method does not take into account the presence of a pop-up edit window or the drop-down portion of a combo if these elements are present, and will never return a UIElement that corresponds to one of these elements. The <b>GetUIElementPopup</b> method can be invoked to return a reference to a popup window's UIElement.</p>
		/// </remarks>
        /// <returns>The UIElement associated with the object, in the specified row and column scrolling regions.</returns>
		public override UIElement GetUIElement( RowScrollRegion rsr, ColScrollRegion csr, bool verifyElements )
		{
			// SSP 8/16/04 - Optimizations
			// IsPotentiallyVisibleOnScreen returns true if the row has any associated visible
			// rows in any of the scroll region's visible rows collections.
			// ----------------------------------------------------------------------------------
			if ( ! this.IsPotentiallyVisibleOnScreen )
				return null;
			// ----------------------------------------------------------------------------------

			// SSP 12/10/03 UWG2787
			// We shouldn't really throw an exception here. Just return if the rsr or csr is null.
			// Commented out below code.
			//
			

			// get the grid's main element
			//
			UIElement element = this.band.Layout.GetUIElement( verifyElements );

			// SSP 12/16/02
			// Optimizations.
			// Get the data area element since rows are always in data area element.
			//
			// MD 8/14/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( element is UltraGridUIElement )
			//    element = ((UltraGridUIElement)element).DataAreaElement;
			UltraGridUIElement gridElement = element as UltraGridUIElement;

			if ( gridElement != null )
				element = gridElement.DataAreaElement;

			if ( null != element )
			{
				element = element.GetDescendant( typeof( RowUIElementBase ), new object[] { this, rsr, csr } );
			}

			return element;
		}

		// SSP 8/16/04 - Optimizations
		// Added VisibleRowCounter, IncrementVisibleRowCounter and DecrementVisibleRowCounter.
		//
		internal byte VisibleRowCounter
		{
			get
			{
				return this.visibleRowCounter;
			}
		}

		internal bool IsPotentiallyVisibleOnScreen
		{
			get
			{
				return this.VisibleRowCounter > 0
					// SSP 8/28/04
					// For card-view return true because we don't use visible rows in card-view.
					//
					|| this.IsCard;
			}
		}

		internal void IncrementVisibleRowCounter( )
		{
			Debug.Assert( this.visibleRowCounter < byte.MaxValue );
			this.visibleRowCounter++;
		}

		internal void DecrementVisibleRowCounter( )
		{
			Debug.Assert( this.visibleRowCounter > 0 );
			this.visibleRowCounter--;			
		}

		internal void BeginEdit()
		{
			// SSP 11/20/03 - Optimizations
			// Replaced multiple calls to ListObject property with a single call.
			//
			IEditableObject editableObject = this.ListObject as IEditableObject;
			if ( null != editableObject )
				editableObject.BeginEdit( );
		}

		#region CancelEditHelper_ActivateNearestRow

		// SSP 2/25/07 BR18124
		// 
		/// <summary>
		/// Cancels edit on the row by calling CancelEdit. However in addition, it activates the nearest data
		/// row if this row gets deleted (which would happen for example if this row were an add-row).
		/// </summary>
		internal void CancelEditHelper_ActivateNearestRow( )
		{
			UltraGridLayout layout = this.Layout;
			UltraGridRow nearestDataRow = GridUtils.GetNearestDataRow( this, false );
			if ( null == nearestDataRow )
				nearestDataRow = GridUtils.GetNearestDataRow( this, true );

			this.CancelEdit( );

			if ( null != nearestDataRow && nearestDataRow.IsStillValid 
				&& ! this.IsStillValid && null != layout 
				// Also only activate the nearest row if a different row wasn't activated
				// in the meantime.
				// 
				&& ( null == layout.ActiveRowInternal || this == layout.ActiveRowInternal ) )
			{
				nearestDataRow.Activate( );
			}
		}

		#endregion // CancelEditHelper_ActivateNearestRow

        // MBS 8/7/09 - TFS18607
        // Refactored logic from the CancelEdit so that we can call this method in a different order
        // depending on developer preference
        #region CancelEdit Interface Helpers

        private bool CancelEditOnIEditableObject(IEditableObject editableObject)
        {
            if (editableObject == null)
                return false;

            editableObject.CancelEdit();

            // MBS 4/16/08 - RowEditTemplate NA2008 V2
            // Let the template know that we're cancelling the edit so that
            // anything bound to the template can be refreshed
            UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
            if (template != null)
                template.OnRowCancelEdit();

            // SSP 8/17/01 UWG132
            // We have duplicate properties DataChanged and IsRowDirty for
            // keeping track if the data has changed and not committed to the
            // backend. So I took this out and called SetDataChanged instead
            //this.IsRowDirty = false;
            this.SetDataChanged(false);

            // SSP 3/17/05 BR02858
            // When edit is cancelled the ItemChanged notification may not fire.
            // So manually notify the calc manager as well as merged cell logic
            // to merge/unmerge cells.
            //
            this.ParentCollection.NotifyCalcManager_ValueChanged(this);

            // SSP 4/7/05 BR03209
            // Also dirty the summaries for the same reasons listed above.
            //
            this.ParentCollection.DataChanged_DirtySummaries();

            return true;
        }
        //
        private bool CancelEditOnICancelAddNew(ICancelAddNew cancelAddNew, IList dataList)
        {
            // Only cancel the added row if end edit has not been called on
            // a row (this is the situation where grid is not bound to
            // IBindingList, so we need to delete the added row upon two
            // escape keys manually
            //
            if (!this.endEditCalled && this.IsAddRow)
            {
                int listIndex = this.ListIndex;
                if (null != cancelAddNew && listIndex >= 0 && listIndex < dataList.Count)
                {
                    cancelAddNew.CancelNew(listIndex);
                    return true;
                }
            }
            return false;
        }
        //
        private void CancelEditDefault()
        {
            // Only cancel the added row if end edit has not been called on
            // a row (this is the situation where grid is not bound to
            // IBindingList, so we need to delete the added row upon two
            // escape keys manually
            //
            if (!this.endEditCalled && this.IsAddRow)
            {
                this.DeleteHelper();
            }
        }
        #endregion //CancelEdit Interface Helpers

        internal void CancelEdit( )
		{
            // MBS 8/7/09 - TFS18607
            // Refactored code into separate methods so that we can use the interfaces specified by the user
            // to cancel the edit operation.  Note that the default behavior is still preserved where if this
            // isn't an AddRow, the only method that would ever be called is CancelEditOnIEditableObject, since
            // ICancelAddNew only applies to new rows.
            //
            #region Refactored
//            // SSP 11/20/03 - Optimizations
//            // Replaced multiple calls to ListObject property with a single call.
//            //
//            IEditableObject editableObject = this.ListObject as IEditableObject;
//            if (null != editableObject)
//            {
//                editableObject.CancelEdit();

//                // MBS 4/16/08 - RowEditTemplate NA2008 V2
//                // Let the template know that we're cancelling the edit so that
//                // anything bound to the template can be refreshed
//                UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
//                if (template != null)
//                    template.OnRowCancelEdit();

//                // SSP 8/17/01 UWG132
//                // We have duplicate properties DataChanged and IsRowDirty for
//                // keeping track if the data has changed and not committed to the
//                // backend. So I took this out and called SetDataChanged instead
//                //this.IsRowDirty = false;
//                this.SetDataChanged(false);

//                // SSP 3/17/05 BR02858
//                // When edit is cancelled the ItemChanged notification may not fire.
//                // So manually notify the calc manager as well as merged cell logic
//                // to merge/unmerge cells.
//                //
//                this.ParentCollection.NotifyCalcManager_ValueChanged(this);

//                // SSP 4/7/05 BR03209
//                // Also dirty the summaries for the same reasons listed above.
//                //
//                this.ParentCollection.DataChanged_DirtySummaries();
//            }
//            // SSP 1/22/02 UWG962
//            // If we are bound to a IList (and not IBindingList), and we
//            // and we are using our own property descriptor to 
//            //
//            else
//            {
//                // Only cancel the added row if end edit has not been called on
//                // a row (this is the situation where grid is not bound to
//                // IBindingList, so we need to delete the added row upon two
//                // escape keys manually
//                //
//                if (!this.endEditCalled && this.IsAddRow)
//                {
//                    // SSP 4/9/07 BR21502
//                    // Added the if block and enclosed the existing code into the else block.
//                    // 
//#if CLR2
//                    IList dataList = this.ParentCollection.Data_List;
//                    ICancelAddNew cancelAddNew = dataList as ICancelAddNew;
//                    int listIndex = this.ListIndex;
//                    if (null != cancelAddNew && listIndex >= 0 && listIndex < dataList.Count)
//                    {
//                        cancelAddNew.CancelNew(listIndex);
//                    }
//                    else
//#endif
//                    {
//                        // SSP 8/19/05 BR05644
//                        // Delete the row if the list object does not implement IEditableObject
//                        // when an add-row is canceled.
//                        // 
//                        // ----------------------------------------------------------------------
//                        this.DeleteHelper();
//                        /*
//                        if ( null != this.Band && null != this.Band.Columns &&
//                            1 == this.Band.Columns.BoundColumnsCount )
//                        {
//                            // Unbound columns are always at the end
//                            //
//                            UltraGridColumn column = this.Band.Columns[0];

//                            if ( column.PropertyDescriptor is Infragistics.Win.UltraWinGrid.ValuePropertyDescriptor )
//                            {
//                                this.DeleteHelper( );
//                            }
//                        }
//                        */
//                        // ----------------------------------------------------------------------
//                    }
//                }
//            }
            #endregion //Refactored
            //
            bool callDefaultDelete = false;
            IList dataList = this.ParentCollection.Data_List;
            IEditableObject editableObject = this.ListObject as IEditableObject;
            ICancelAddNew cancelAddNew = dataList as ICancelAddNew;
            AddRowEditNotificationInterface notificationInterfaces = this.Band.EndEditNotificationInterfacesResolved;
            switch (notificationInterfaces)
            {
                case AddRowEditNotificationInterface.ICancelAddNewAndIEditableObject:
                    callDefaultDelete = !this.CancelEditOnICancelAddNew(cancelAddNew, dataList);
                    callDefaultDelete |= !this.CancelEditOnIEditableObject(editableObject);

                    // If neither method could be applied to the row, then take the default action of deleting the row manually
                    if (callDefaultDelete)
                        this.CancelEditDefault();

                    break;

                case AddRowEditNotificationInterface.ICancelAddNewOrIEditableObject:
                    // Only call the IEditableObject implementation if the ICancelAddNew fails
                    if (!(this.CancelEditOnICancelAddNew(cancelAddNew, dataList) || this.CancelEditOnIEditableObject(editableObject)))
                        this.CancelEditDefault();

                    break;

                case AddRowEditNotificationInterface.IEditableObjectAndICancelAddNew:
                    callDefaultDelete = !this.CancelEditOnIEditableObject(editableObject);
                    callDefaultDelete |= !this.CancelEditOnICancelAddNew(cancelAddNew, dataList);                    

                    // If neither method could be applied to the row, then take the default action of deleting the row manually
                    if (callDefaultDelete)
                        this.CancelEditDefault();

                    break;

                case AddRowEditNotificationInterface.IEditableObjectOrICancelAddNew:
                    // Only call the IEditableObject implementation if the ICancelAddNew fails
                    if (!(this.CancelEditOnIEditableObject(editableObject) || this.CancelEditOnICancelAddNew(cancelAddNew, dataList)))
                        this.CancelEditDefault();

                    break;
            }
        }

        // MBS 8/7/09 - TFS18607
        // Refactored from EndEdit
        #region EndEdit Interface Helpers

        private bool EndEditOnICancelAddNew(ICancelAddNew cancelAddNew, IList dataList)
        {
            if (!this.IsAddRow || cancelAddNew == null)
                return false;

            int listIndex = this.ListIndex;
            if (listIndex >= 0 && listIndex < dataList.Count)
            {
                cancelAddNew.EndNew(listIndex);
                return true;
            }

            return false;
        }

        private bool EndEditOnIEditableObject(IEditableObject editableObject)
        {
            if (editableObject == null)
                return false;

            // MBS 4/16/08 - RowEditTemplate NA2008 V2
            // Let the template know that we're cancelling the edit so that
            // anything bound to the template can be refreshed
            UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
            if (template != null)
                template.OnRowEndEdit();

            editableObject.EndEdit();

            return true;
        }

        #endregion //EndEdit Interface Helpers

        internal void EndEdit()
		{
            // MBS 8/7/09 - TFS18607
            // Refactored code into separate methods so that we can use the interfaces specified by the user
            // to cancel the edit operation.  Note that the default behavior is still preserved where if this
            // isn't an AddRow, the only method that would ever be called is CancelEditOnIEditableObject, since
            // ICancelAddNew only applies to new rows.
            //
            #region Refactored

            //// SSP 11/20/03 - Optimizations
            //// Replaced multiple calls to ListObject property with a single call.
            ////
            //IEditableObject editableObject = this.ListObject as IEditableObject;
            //if ( null != editableObject )
            //{
            //    // MBS 4/16/08 - RowEditTemplate NA2008 V2
            //    // Let the template know that we're cancelling the edit so that
            //    // anything bound to the template can be refreshed
            //    UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
            //    if (template != null)
            //        template.OnRowEndEdit();

            //    editableObject.EndEdit();
				
            //    // SSP 12/21/01
            //    // 
            //    /*// SSP 8/17/01 UWG132
            //    // We have duplicate properties DataChanged and IsRowDirty for
            //    // keeping track if the data has changed and not committed to the
            //    // backend. So I took this out and called SetDataChanged instead
            //    //this.IsRowDirty = false;
            //    this.SetDataChanged( false );
            //    */
            //}
            //// SSP 10/12/07 BR26397
            //// If the list object doesn't implement IEditableObject then see if the
            //// list implements ICancelAddNew. If so then call EndNew on that if this
            //// row is an add-row (ICancelAddNew is only meant for add-rows as its
            //// name implies).
            //// 
            //else if ( this.IsAddRow )
            //{
            //    IList dataList = this.ParentCollection.Data_List;
            //    ICancelAddNew cancelAddNew = dataList as ICancelAddNew;
            //    if ( null != cancelAddNew )
            //    {
            //        int listIndex = this.ListIndex;
            //        if ( listIndex >= 0 && listIndex < dataList.Count )
            //            cancelAddNew.EndNew( listIndex );
            //    }
            //}
            #endregion //Refactored
            //            
            IList dataList = this.ParentCollection.Data_List;
            IEditableObject editableObject = this.ListObject as IEditableObject;
            ICancelAddNew cancelAddNew = dataList as ICancelAddNew;
            AddRowEditNotificationInterface notificationInterfaces = this.Band.EndEditNotificationInterfacesResolved;
            switch (notificationInterfaces)
            {
                case AddRowEditNotificationInterface.ICancelAddNewAndIEditableObject:
                    this.EndEditOnICancelAddNew(cancelAddNew, dataList);
                    this.EndEditOnIEditableObject(editableObject);
                    break;

                case AddRowEditNotificationInterface.ICancelAddNewOrIEditableObject:
                    // Only call the IEditableObject implementation if the ICancelAddNew fails
                    if (!this.EndEditOnICancelAddNew(cancelAddNew, dataList))
                        this.EndEditOnIEditableObject(editableObject);                        

                    break;

                case AddRowEditNotificationInterface.IEditableObjectAndICancelAddNew:
                    this.EndEditOnIEditableObject(editableObject);
                    this.EndEditOnICancelAddNew(cancelAddNew, dataList);                    
                    break;

                case AddRowEditNotificationInterface.IEditableObjectOrICancelAddNew:
                    // Only call the ICancelAddNew implementation if the IEditableObject fails
                    if (!this.EndEditOnIEditableObject(editableObject))
                        this.EndEditOnICancelAddNew(cancelAddNew, dataList);     

                    break;
            }

            // SSP 1/22/02 UWG962
			// Just set this flag to true if EndEdit is called.
			//
			this.endEditCalled = true;


			// SSP 11/12/03 Add Row Feature
			// Reset the CurrentAddRow of the parent collection since this row
			// is not an add row any more.
			//
			if ( this.ParentCollection.CurrentAddRow == this )
				this.ParentCollection.CurrentAddRow = null;
			
			// SSP 12/21/01
			// moved this here from within above if statement because
			// we want to set the DataChanged to false regardless whether
			// the list object is IEditableObject or not.
			// SSP 8/17/01 UWG132
			// We have duplicate properties DataChanged and IsRowDirty for
			// keeping track if the data has changed and not committed to the
			// backend. So I took this out and called SetDataChanged instead
			//this.IsRowDirty = false;
			this.SetDataChanged( false );
		}

		// MD 8/14/07 - 7.3 Performance
		// Reimplemented index caching
		private int cachedIndex = -1;

		/// <summary>
		/// The index of this row in its parent collection
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Index</b> property is set by default to the order of the creation of objects in a collection. The index for the first object in a collection will always be zero.</p>
		/// <p class="body">The value of the <b>Index</b> property of an object can change when objects in the collection are reordered, such as when objects are added to or deleted from the collection. Since the <b>Index</b> property may change dynamically, it may be more useful to refer to objects in a collection by using its <b>Key</b> property.</p>
		/// <p class="body">Not all objects in a collection support an <b>Index</b> property. For certain objects, the <b>Index</b> value is essentially meaningless except as an internal placeholder within the collection. In these cases, the object will not have an <b>Index</b> property, even though it is in a collection. You can use other mechanisms, such as the <b>Key</b> property or the <code class="code">For Each...</code> loop structure, to access the objects in the collection.</p>
		/// </remarks>
		public int Index
		{
			get
			{
				// SSP 4/5/04 - Virtual Binding Related Optimizations
				// Commented out the original code and added the new code.
				//
				// SSP 6/10/04 UWG3406
				// If the row collection is marked dirty then sync the row collection.
				// To do that simply call IndexOf on the parent collection.
				//
				//return this.ParentCollection.SparseArray.IndexOf( this );
				// MD 8/14/07 - 7.3 Performance
				// Reimplemented index caching
				//return this.ParentCollection.IndexOf( this );
                //
                // MBS 10/29/07 - BR27940
                // Keep a stack variable of the parent collection for the logic below
                RowsCollection parentCollection = this.ParentCollection;
				if ( 0 <= this.cachedIndex && this.cachedIndex < this.ParentCollection.Count )
				{
					// MD 11/9/07 - BR28147
					// Getting the Count in an UltraCombo will regenerate each row, but the parent collection 
					// will not change. In this case, the cached index was being set back to -1, but we were
					// never reevaluating whether to use the cached index because the parent collection hadn't
					// changed. Removed the condition that the parent collection had to change and added another
					// check: to make sure cachedIndex is positive.
					//// MBS 10/29/07 - BR27940
					//// In the process of requesting the Count of the parent collection, if we have 
					//// recently done a GroupBy operation, the ParentCollection could have actually 
					//// changed through the EnsureNotDirty call, which would change the value of 
					//// the Count property.  Instead of always calling EnsureNotDirty, which has
					//// associated overhead, we'll selectively re-acquire the Count of the parent collection
					//// and bail out of trying to access an invalid index.
					////
					////if (this.ParentCollection[ this.cachedIndex ] == this )
					//bool shouldUseCache = true;
					//if (parentCollection != this.ParentCollection)
					//    shouldUseCache = this.cachedIndex < this.ParentCollection.Count;
					bool shouldUseCache =
						0 <= this.cachedIndex &&
						this.cachedIndex < this.ParentCollection.Count;

                    if (shouldUseCache && this.ParentCollection[this.cachedIndex] == this)
                        return this.cachedIndex;
				}

				this.cachedIndex = this.ParentCollection.IndexOf( this );
				return this.cachedIndex;

				
			}
		}
		
		/// <summary>
		/// Returns the index of this row in its parent collection, relative to all other visible rows. Hidden rows are not counted.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This property will return -1 if the UltraGridRow's <b>Hidden</b> property is set.
		/// </p>
		/// <seealso cref="RowsCollection.VisibleRowCount"/>
		/// <seealso cref="RowsCollection.GetRowAtVisibleIndex(int)"/>
		/// </remarks>
		public int VisibleIndex
		{
			get
			{
				// SSP 2/20/04 - Virtual Mode 
				// Commented out the original code and added the new code below.
				//
				return null != this.parentCollection 
					? this.parentCollection.GetVisibleIndex( this )
					: -1;

				

				
			}
		}

		// SSP 5/29/02 UWG1137
		// Added OverallSelectionPosition property.
		//
		internal long OverallSelectionPosition
		{
			get
			{
				return  ( (long)this.ScrollPosition << 32 ) + this.Index;
			}
		}

		/// <summary>
		/// Returns True if this row's parent row and all its ancestors are expanded. This property is read-only.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// If the row doesn't have any parent row then it this property will return true.
		/// </p>
		/// </remarks>
		public bool AllAncestorsExpanded
		{
			get
			{
				UltraGridRow parent = this.ParentRow;

				// if we don't have a parent return true
				//
				if ( parent == null )
					return true;

				// if the parent isn't expanded return false
				//
				if ( !parent.Expanded )
					return false;

				// Return this property of the parent. This effectively
				// walks up the parent chain
				//
				return parent.AllAncestorsExpanded;
			}
		}

		/// <summary>
		/// This property returns True if the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeRow"/> event is in process for the current row. It will return False outside of InitializeRow.
		/// </summary>
		public bool InInitializeRowEvent
		{
			get 
			{
				return this.inInitializeRowEvent;
			}
		}

		internal bool Synchronizing
		{
			get
			{
				return this.synchronizing;
			}
		}

		internal virtual void FireInitializeRow( )
		{
			// SSP 8/11/05 BR05398
			// 
			UltraGridLayout layout = this.Layout;
			bool initializeRowWasAlreadySuspended = layout.SuspendInitializeRowFor( this );
			if ( initializeRowWasAlreadySuspended )
				return;

			this.inInitializeRowEvent = true;

			try 
			{
				// fire the initialize row event
				//
				layout.Grid.FireInitializeRow( new InitializeRowEventArgs( this, this.initializeRowEventFired ) );
			}
			finally
			{
				this.initializeRowEventFired = true;
				this.inInitializeRowEvent = false;

				// SSP 8/11/05 BR05398
				// 
				layout.ResumeInitializeRowFor( this );
			}
		}

		/// <summary>
		/// The read-only collection of UltraGridChildBand objects that contain the child rows for this row.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridChildBand"/>
		/// <seealso cref="UltraGridChildBand.Rows"/>
		/// </remarks>
		public virtual Infragistics.Win.UltraWinGrid.ChildBandsCollection ChildBands
		{
			get
			{
				if ( null == this.childBands )
				{
					// SSP 3/24/03 - Row Layout Functionality
					// HasChildBands overload with no parameter checks to see if there are any
					// visible child bands. Here we want to see if there are any child bands, visible
					// or non-visible. So pass in false for visibleOnly parameter.
					//
					//if ( this.band.HasChildBands() )
					if ( this.band.HasChildBands( false ) )
					{
						this.childBands = new ChildBandsCollection( this );

						// SSP 3/10/04 - Virtual Binding Related
						// Added code for synchronizing child band collections against the bands collection
						// in case a band is added or removed. We don't need to add the child bands
						// here as that synchronization logic will take care it for us.
						// Commented below code out.
						//
						
					}
				}

				return this.childBands;
			} 
		}

		// SSP 10/14/03 UWG2703
		// Added BumpChildRowsVerifyVersion method.
		//
		internal void BumpChildRowsVerifyVersion( )
		{
			if ( null != this.childBands )
			{
				for ( int i = 0; i < this.childBands.Count; i++ )
				{
					UltraGridChildBand childBand = this.childBands[i];
					if ( null != childBand && childBand.HaveRowsBeenInitialized )
					{
						childBand.Rows.BumpVerifyVersion( );
					}
				}
			}
		}

		// SSP 12/27/01
		// Added this helper function.
		//
		internal bool HaveChildRowsBeenCreated( UltraGridBand band )
		{
			
			
			
			if ( null != this.childBands )
			{
				int count = this.childBands.Count;
				for ( int i = 0; i < count; i++ )
				{
					UltraGridChildBand childBand = this.childBands[i];
					if ( band == childBand.Band )
						return childBand.HaveRowsBeenInitialized;
				}
			}

			
			

			return false;
		}

		// JJD 10/03/01 - UWG476
		// This should be marked internal
		internal bool HasChildRows
		{
			get
			{
				return this.HasChildRowsInternal( IncludeRowTypes.DataRowsOnly );
			}
		}

		// SSP 11/14/03 Add Row Feature
		// Added HasChildRowsInternal method.
		//
		internal bool HasChildRowsInternal( IncludeRowTypes includeRowTypes )
		{
			// if the band doesn't have any child bands
			// then return false so we don't needlessly 
			// allocate the childBands collection
			//
			if ( null == this.childBands &&
				this.band.GetChildBandCount() == 0 )
				return false;

			return this.ChildBands != null && 
				// SSP 11/14/03 Add Row Feature
				// Also take into account template add-rows.
				//
				//this.ChildBands.HasChildRows;
				this.ChildBands.HasChildRowsInternal( includeRowTypes );
		}

		// SSP 2/5/02
		// Made this a virtual method so that it can be overridden in 
		// the GroupByRow class.
		//
		// The collection of cells for this row. The cells
		// are created lazily as they are referenced.
		/// <summary>
		/// Returns a reference to a collection of UltraGridCell objects.  This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a collection of UltraGridCell objects that can be used to retrieve references to the UltraGridCell objects that belong to a row. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		/// <p class="body">For the UltraGridRow object, the returned collection provides a way to work with the cells that constitute the row.</p>
		/// <p class="body">The <b>Count</b> property of the returned collection can be used to determine the number of cells that belong to a row.</p>
		/// </remarks>
		//public CellsCollection Cells 
		public virtual CellsCollection Cells
		{
			get
			{
				if ( null == this.cells )
					this.cells = new CellsCollection( this );

				return this.cells;
			}
		}

		// SSP 6/21/04 UWG3398
		// Added HasCells property.
		//
		internal bool HasCells
		{
			get
			{
				return null != this.cells;
			}
		}

		// SSP 7/29/02
		// Added HasCell off the column. Before we were accessing the Cells property of the
		// row and calling HasCell off the returned CellsCollection forcing creation of
		// a cells collection if it wasn't already created. So added below method which will
		// not create cells collection.
		// 
		/// <summary>
		/// Indicates if cell associated with the specified column has been created.
		/// </summary>
		/// <param name="column">The associated <b>column</b>.</param>
		/// <returns>
		/// <p class="body">
		/// <b>HasCell</b> method indicates whether the cell for a column has been allocated yet. 
		/// UltraGrid allocates cells and cell collections lazily - as they are accessed. This is 
		/// useful for example if you want to perform an operation on a cell, such as resetting 
		/// previously set appearance settings, that would only be necessary if the cell has been 
		/// allocated previously. In such a case you can use this method to find out if the cell 
		/// has been allocated and only perform the operation if it is. This ensures that the cell
		/// doesn't get unnecessarily allocated.
		/// </p>
		/// </returns>
		// SSP 7/31/03
		// Changed the access modifier of HasCell method from internal to public.
		// The reason for doing this is that if the user wants to find if a cell has
		// been allocated then the only way he can do that is by calling HasCell method
		// off the cells collection however that leads to allocation of the cells 
		// collection. Exposing this method solves that problem.
		//
		//internal bool HasCell( UltraGridColumn column )
		public bool HasCell( UltraGridColumn column )
		{
			if ( null != this.cells )
				// SSP 2/20/04 - Optimization
				// Cells' a virtual property.
				//
				//return this.Cells.HasCell( column );
				return this.cells.HasCell( column );

			// Since the cells collection hasn't been created yet, there aren't any cells.
			// So return false.
			//
			return false;
		}


		// SSP 8/26/06 - NAS 6.3
		// Added HasCell overloads that take in column index and column key as well.
		// 
		/// <summary>
		/// Indicates if cell associated with the specified column index has been created.
		/// </summary>
		/// <param name="columnIndex">The index indicating the column.</param>
		/// <returns>
		/// <p class="body">
		/// <b>HasCell</b> method indicates whether the cell for a column has been allocated yet. 
		/// UltraGrid allocates cells and cell collections lazily - as they are accessed. This is 
		/// useful for example if you want to perform an operation on a cell, such as resetting 
		/// previously set appearance settings, that would only be necessary if the cell has been 
		/// allocated previously. In such a case you can use this method to find out if the cell 
		/// has been allocated and only perform the operation if it is. This ensures that the cell
		/// doesn't get unnecessarily allocated.
		/// </p>
		/// </returns>
		public bool HasCell( int columnIndex )
		{
			return null != this.cells && this.cells.HasCell( columnIndex );
		}

		// SSP 8/26/06 - NAS 6.3
		// Added HasCell overloads that take in column index and column key as well.
		// 
		/// <summary>
		/// Indicates if cell associated with the specified column key has been created.
		/// </summary>
		/// <param name="columnKey">The key indicating the column.</param>
		/// <returns>
		/// <p class="body">
		/// <b>HasCell</b> method indicates whether the cell for a column has been allocated yet. 
		/// UltraGrid allocates cells and cell collections lazily - as they are accessed. This is 
		/// useful for example if you want to perform an operation on a cell, such as resetting 
		/// previously set appearance settings, that would only be necessary if the cell has been 
		/// allocated previously. In such a case you can use this method to find out if the cell 
		/// has been allocated and only perform the operation if it is. This ensures that the cell
		/// doesn't get unnecessarily allocated.
		/// </p>
		/// <p class="body">
		/// Note that there must be a column with the specified key in the associated band's 
		/// columns collection otherwise this method will throw an exception. If the intention is
		/// to find out if a column with the specified key exists then use the CellCollection's
		/// <b>Exists</b> method instead.
		/// </p>
		/// <p class="body">
		/// <b>Note</b> that for better efficiency you may want to use HasCell overloads that take 
		/// column index or a column object as they do not require searching the collection for
		/// the matching column key.
		/// </p>
		/// </returns>
		public bool HasCell( string columnKey )
		{
			return null != this.cells && this.cells.HasCell( columnKey );
		}

		// SSP 5/5/03 - Optimizations
		// Added a method to get the cell if it was allocated.
		//
		internal UltraGridCell GetCellIfAllocated( UltraGridColumn column )
		{
			if ( null != this.cells )
				return this.cells.GetCellIfAllocated( column );
			
			return null;
		}

		internal void OnDeleted()
		{
			this.deleted = true;
			this.hidden = true;
		}

		/// <summary>
		/// Returns True if the row has been deleted but the UltraGridRow object has not yet been destroyed.
		/// </summary>
		public bool IsDeleted
		{
			get
			{ 
				return this.deleted;
			}
		}

		// This needed to be an internal method so that it didn't conflict 
		// with the Cell's public Row property
		//
		internal override Infragistics.Win.UltraWinGrid.UltraGridRow GetRow() 
		{ 
			return this;
		}

		internal void GetCellRect( UltraGridColumn column, ref Rectangle rect, ref AppearanceData appData )
		{
			RowScrollRegion rsr = this.Layout.ActiveRowScrollRegion;
			ColScrollRegion csr = this.Layout.GetActiveColScrollRegion( false );

			this.GetCellRect( column, ref rect, ref appData, rsr, csr );
		}

		internal void GetCellRect( UltraGridColumn column, 
			ref Rectangle rect, 
			ref AppearanceData appData,
			RowScrollRegion rsr,
			ColScrollRegion csr )
		{
			if ( rsr == null )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_226"), Shared.SR.GetString("LE_ArgumentNullException_339") );

			if ( csr == null )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_228"), Shared.SR.GetString("LE_ArgumentNullException_340") );
 
			VisibleRow visibleRow = null;
			if ( null != rsr )			
				visibleRow = rsr.GetVisibleRow( this );

			if ( null == visibleRow || null == csr || null == rsr )
			{
				rect = Rectangle.Empty;				
				return;
			}

			this.ResolveCellAppearance( column, ref appData, AppearancePropFlags.AllRender );
			
			Rectangle pictureRect = Rectangle.Empty;
			
			// Use the new GetDimensions method to get the proper cell rect
			//
			column.Header.GetDimensions( visibleRow,				
				PositionDimensions.CellTextAndPicture,
				DimOriginBase.OnScreen,
				out rect,
				csr,
				ref appData,
				ref pictureRect );

			// MD 1/21/09 - Groups in RowLayouts
			// Use LevelCountResolved because it checks the band's row layout style
			//int levelCount = this.Band.LevelCount;
			int levelCount = this.Band.LevelCountResolved;
			
			if ( levelCount > 1)
			{
				int left = rect.Left;
				int right = rect.Right;
				int level = column.GetDisplayLevel();

				int offset = this.GetLevelOffset( level, true );

				rect.Y += offset;
				
				int height = rect.Bottom - rect.Top;

				int heightPerLevel = ( height + (levelCount /2) ) / levelCount;
				rect.Height = (rect.Bottom - rect.Y ) + rect.Top + heightPerLevel - 1;

				column.AdjustForRowBorders( ref rect );				
				column.Header.AdjustForCellPadding( ref rect );				

				rect.X = left;
				rect.Width = (rect.Right - rect.X ) + right;
			}
		}


		/// <summary>
		/// Returns the text for a cell.
		/// </summary>
		/// <param name="column">The text of the cell associated with this column object will be returned.</param>
		/// <returns>Retruns the text that's displayed in the cell.</returns>
		/// <remarks>
		/// <seealso cref="UltraGridRow.GetCellValue(UltraGridColumn)"/>
		/// <seealso cref="UltraGridCell.Value"/>
		/// <seealso cref="UltraGridCell.Text"/>
		/// </remarks>
		public string GetCellText( Infragistics.Win.UltraWinGrid.UltraGridColumn column )
		{
			if ( this.deleted )
				return string.Empty;

			return column.GetCellText( this );
		}

		// SSP 7/17/02 UWG1377
		// Added GetSampleDataForType method.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal object GetSampleDataForType( Type type )
		internal static object GetSampleDataForType( Type type )
		{
			// MRS 5/6/05 - Moved this into a static method in Win so it can be
			// used by the tree, also. 
			return DataBindingUtils.GetSampleDataForType(type);
//			if ( null == UltraGridRow.sampleData )
//			{
//				UltraGridRow.sampleData = new Hashtable( 20 );
//				// For dates return January 1st of this year.
//				//
//				DateTime currDate = DateTime.Now;
//				Random random = new Random( );
//
//				UltraGridRow.sampleData[ typeof( DateTime ) ] = new DateTime( currDate.Year, 1, 1, 0, 0, 0 );
//				UltraGridRow.sampleData[ typeof( System.DayOfWeek ) ] = currDate.DayOfWeek;
//				UltraGridRow.sampleData[ typeof( int ) ]			= random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( uint ) ]		= random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( short ) ]		= random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( ushort ) ]		= random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( long ) ]		= random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( ulong ) ]		= random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( float ) ]		= 0.5 * random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( double ) ]		= 0.5 * random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( decimal ) ]	= 0.5 * random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( byte ) ]		= random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( float ) ]		= random.Next( 100 );
//				UltraGridRow.sampleData[ typeof( sbyte ) ]		= random.Next( 100 );
//				// MRS 7/22/04 - Localization fixes for Grape City
//				//UltraGridRow.sampleData[ typeof( string ) ]		= "Text";
//				UltraGridRow.sampleData[ typeof( string ) ]		= SR.GetString("DesignTime_SampleData_String"); //"Text"
//
//				UltraGridRow.sampleData[ typeof( System.Drawing.Color ) ] = Color.FromArgb( 0, 0, 0 );
//				// SSP 8/19/02 UWG1592
//				// Added below entry for boolean data type.
//				//
//				UltraGridRow.sampleData[ typeof( bool ) ]		= 0 == ( random.Next( 100 ) % 2 );
//			}
//
//			if ( UltraGridRow.sampleData.ContainsKey( type ) )
//				return UltraGridRow.sampleData[ type ];
//			
//			// For rest of the data type return DBNull as we don't know what they
//			// are expecting and all the editors should be able to handle DBNull.
//			//
//			return DBNull.Value;
		}

        // MBS 12/23/08
        // Added an overload that takes the key of the column
        /// <summary>
        /// Returns the data value for a cell from the database
        /// </summary>
        /// <param name="columnKey">The key of the column intersecting the row from which the value will be retrieved.</param>
        /// <returns>The data value for a cell from the database</returns>
        public object GetCellValue(string columnKey)
        {
            UltraGridColumn column = this.Band.Columns[columnKey];
            return this.GetCellValue(column);
        }

		/// <summary>
		/// Returns the data value for a cell from the database
		/// </summary>
		/// <param name="column">The column intersecting the row from which the value will be retrieved.</param>
        /// <returns>The data value for a cell from the database</returns>
		// SSP 4/2/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Made GetCellValue and SetCellValue virtual so the UltraGridFilterRow can override it. Also
		// as an optimization got rid of designMode parameter.
		//
		//internal object GetCellValue( Infragistics.Win.UltraWinGrid.UltraGridColumn column, bool designMode )
		public virtual object GetCellValue( Infragistics.Win.UltraWinGrid.UltraGridColumn column )
		{
			// SSP 11/10/03 Add Row Feature
			//
			// ------------------------------------------------------------------------------
			if ( this.IsTemplateAddRow )
			{
				return this.parentCollection.GetTemplateAddRowValue( column );
			}
			// ------------------------------------------------------------------------------

			// SSP 12/26/01
			// If unbound, use the Value off the cell.
			//
			if ( !column.IsBound )
				return this.Cells[column].Value;

			// AS - 10/25/01
			// At design time, the value for a cell should be the datatype for
			// the column.
			//
			// SSP 2/18/04 - Virtual Mode - Optimization
			//
			//if ( this.band.Layout.Grid.DesignMode )
			// SSP 3/10/04 - Designer Usability Improvements Related
			// If binding manager is null return sample data as well. This is for the design
			// time extract data structure functionality.
			//
			//if ( designMode )
			// SSP 4/2/05 - Optimizations
			// Got rid of designMode parameter. Instead if listObject has been assigned a non-null
			// value then it has to be at run time and there has to be a binding manager.
			//
			//if ( designMode || null == this.band.BindingManager )
			// SSP 6/8/05 BR03609
			// Use the new ShouldDisplayDataFromDataSource property instead.
			// 
			//if ( null == this.listObject && ( this.Layout.Grid.DesignMode || null == this.band.BindingManager ) )
			if ( null == this.listObject && ! this.Layout.ShouldDisplayDataFromDataSource )
			{
				// SSP 7/17/02 UWG1377
				// Since the embeddable editors are expecting the data of the supported
				// data type, we can't just pass in the string.
				// So pass in a sample data instead.
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//return this.GetSampleDataForType( column.DataType );
				return UltraGridRow.GetSampleDataForType( column.DataType );
				
			}

			if ( this.deleted )
				
				
				
				
				
				return null;

			try
			{
				return column.PropertyDescriptor.GetValue( this.ListObject );
			}
			catch ( Exception exception )
			{
				// SSP 9/15/06 - NAS 6.3
				// Raise Error event.
				// 
				UltraGridLayout layout = this.Layout;
				if ( null != layout )
					layout.Grid.InternalHandleDataError( SR.GetString( "DataError_CellValueRetrieval" ), exception, true );
			}

			return null;
		}

		// SSP 10/6/04 - UltraCalc
		// Added an overload of SetCellValue that takes suppressErrorMessagePrompt parameter.
		// 		
		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Changed the return type from void to bool. We need to know if the action was canceled or not.
		// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
		// 
		//internal void SetCellValue( Infragistics.Win.UltraWinGrid.UltraGridColumn column, object val )
		internal bool SetCellValue( Infragistics.Win.UltraWinGrid.UltraGridColumn column, object val )
		{
			return this.SetCellValue( column, val, false );
		}
		
		// SSP 10/6/04 - UltraCalc
		// Added suppressErrorMessagePrompt parameter to the method. We want to supress the 
		// error message box in the case a formula's evaluation results do not match the 
		// column's data type and set value on the list object fails. That's what the 
		// suppressErrorMessagePrompt parameter is for.
		//
		//internal void SetCellValue( Infragistics.Win.UltraWinGrid.UltraGridColumn column, object val )
		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Made GetCellValue and SetCellValue virtual so the UltraGridFilterRow can override it.
		//
		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Changed the return type from void to bool. We need to know if the action was canceled or not.
		// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
		// 
		//internal virtual void SetCellValue( 
		internal bool SetCellValue( 
			Infragistics.Win.UltraWinGrid.UltraGridColumn column, 
			object val, bool suppressErrorMessagePrompt )
		{
			return this.SetCellValue( column, val, suppressErrorMessagePrompt, false );
		}

        //  BF NA 9.1 - UltraCombo MultiSelect
        //  I added another overload so the caller can specify whether the
        //  RowsCollection should fire the DataChanged event.

		// SSP 2/21/06 BR10249
		// Added an overload that takes in throwExceptionOnError parameter. Pass that along.
		// 
		internal virtual bool SetCellValue( 
			Infragistics.Win.UltraWinGrid.UltraGridColumn column, 
			object val, bool suppressErrorMessagePrompt,
			bool throwExceptionOnError )
		{
            bool fireDataChanged = true;
            return this.SetCellValue( column, val, suppressErrorMessagePrompt, throwExceptionOnError, fireDataChanged );
        }

        // CDS 6/18/09 TFS17819 Added an overload for backward compatibility
		internal bool SetCellValue( 
			Infragistics.Win.UltraWinGrid.UltraGridColumn column, 
			object val, bool suppressErrorMessagePrompt,
			bool throwExceptionOnError,
            bool fireDataChanged )
		{
            bool fireCellUpdateEvents = false;
            return this.SetCellValue(column, val, suppressErrorMessagePrompt, throwExceptionOnError, fireDataChanged, fireCellUpdateEvents);
        }

        // CDS 6/18/09 TFS17819 Added an overload for backward compatibility
        internal bool SetCellValue(
            Infragistics.Win.UltraWinGrid.UltraGridColumn column,
            object val, bool suppressErrorMessagePrompt,
            bool throwExceptionOnError,
            bool fireDataChanged,
            bool fireCellUpdateEvents )
        {
            //  BF 2/10/09  TFS13739 (see below)
            UltraGridLayout layout = this.Layout;

            try
            {
                //  BF 2/10/09  TFS13739
                //  Flag this method as in progress so we can prevent multiple
                //  DataChanged notifications.
                if ( layout != null )
                    layout.IsInSetCellValue = true;

			    if ( this.deleted )
				    // SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				    // Changed the return type from void to bool. We need to know if the action was canceled or not.
				    // Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
				    // 
				    //return;
				    return false;

			    if ( null == column )
				    // SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				    // Changed the return type from void to bool. We need to know if the action was canceled or not.
				    // Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
				    // 
				    //return;
				    return false;

			    // SSP 11/10/03 Add Row Feature
			    //
			    // ------------------------------------------------------------------------------
			    if ( this.IsTemplateAddRow )
			    {
				    this.parentCollection.SetTemplateAddRowValue( column, val );
				    // SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				    // Changed the return type from void to bool. We need to know if the action was canceled or not.
				    // Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
				    // 
				    //return;
				    return true;
			    }
			    // ------------------------------------------------------------------------------

			    this.BeginEdit();

			    object valueToAssign = null;

                //  BF 3/24/09  TFS15870
                bool conversionWasSuccessful = false;

			    // SSP 2/7/02 UWG1000
			    // Moved this code into a separate method UltraGridColumn.ConvertValueToDataType
			    //
			    // SSP 11/3/03 UWG2700
			    // Added Nothing to Nullable enum. When it's Nothing, use null value and don't try
			    // to convert null into anything.
			    //
			    //valueToAssign = column.ConvertValueToDataType( val );
			    if ( null != val || Nullable.Nothing != column.Nullable )
                //  BF 3/24/09  TFS15870
                //  Nullable<T> types can of course be converted to null.
                //valueToAssign = column.ConvertValueToDataType( val );				
                conversionWasSuccessful = column.ConvertValueToDataType( val, out valueToAssign );

                #region Old code
                
                #endregion Old code
    			
			    // If unable to conver the value to the underlying type,
			    // then fire the Error event.
			    //
			    // SSP 11/3/03 UWG2700
			    // Added Nothing to Nullable enum. When it's Nothing, use null value.
			    //
			    //if ( null == valueToAssign )

                //  BF 3/24/09  TFS15870
			    //if ( null == valueToAssign && ( null != val || Nullable.Nothing != column.Nullable ) )
			    if ( conversionWasSuccessful == false &&
                     null == valueToAssign && ( null != val || Nullable.Nothing != column.Nullable ) )
			    {
				    // Added code to fire Error Event
				    // SSP 9/26/03 UWG2679
				    // If we are being called from Cell.CommitEditValue, then let Cell.CommitEditValue
				    // handle the error. To do that simply throw an exception and Cell.CommitEditValue
				    // will handle it and fire the CellDataError/Error events.
				    //
				    // ----------------------------------------------------------------------------------
				    UltraGridCell cell = this.HasCell( column ) ? this.Cells[ column ] : null;
    				
				    if ( null != cell && cell.InCommitEditValue 
					    // SSP 2/21/06 BR10249
					    // Added an overload that takes in throwExceptionOnError parameter.
					    // 
					    || throwExceptionOnError
					    )
				    {
					    throw new Exception( SR.GetString("DataErrorCellUpdateUnableToConvert",
                            // MBS 1/30/09 - TFS13247
                            //val.GetType(), column.PropertyDescriptor.PropertyType) );
                            val != null ? val.GetType().ToString() : "null", column.PropertyDescriptor.PropertyType));
				    }
					    // SSP 1/14/05 BR01756
					    // Took out the restriction we had that didn't allow setting of cell values in
					    // UltraDropDownBase. Now we can get here when the grid is not an UltraGrid so
					    // check to make sure that grid is an UltraGrid.
					    //
					    //else
				    else if ( this.Layout.Grid is UltraGrid )
				    {
					    DataErrorInfo e = new DataErrorInfo(null, val, this, this.Cells[column],
						    DataErrorSource.CellUpdate,
						    SR.GetString("DataErrorCellUpdateUnableToConvert",
						    // SSP 11/13/03 Add Row Feature
						    // If the val is null, then pass in (null) string. I don't know what else we can do here.
						    //
						    //val.GetType(), 
						    null == val ? (object)"(null)" : (object)val.GetType(), 
						    column.PropertyDescriptor.PropertyType) ); //"Unable to convert from: " + val.GetType() + " to " + column.PropertyDescriptor.PropertyType );
    				
					    // SSP 10/6/04 - UltraCalc
					    // Added suppressErrorMessagePrompt parameter to the method. We want to supress the 
					    // error message box in the case a formula's evaluation results do not match the 
					    // column's data type and set value on the list object fails. That's what the 
					    // suppressErrorMessagePrompt parameter is for.
					    //
					    //((UltraGrid)this.Layout.Grid).InternalHandleDataError( e );
					    ((UltraGrid)this.Layout.Grid).InternalHandleDataError( e, suppressErrorMessagePrompt );
				    }

				    // SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				    // Changed the return type from void to bool. We need to know if the action was canceled or not.
				    // Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
				    // 
				    //return;
				    return false;
				    // ----------------------------------------------------------------------------------
			    }

			    Debug.Assert( null != column.PropertyDescriptor, 
				    "This method should only be called with a column that has a PropertyDescriptor." );

			    // SSP 2/7/02
			    // Added below if condition. We should not be called if
			    // column is unbound.
			    //
			    if ( null != column.PropertyDescriptor )
			    {
                    // CDS 6/18/09 TFS17819 Fire the BeforeCellUpdate event
                    UltraGrid grid = (layout != null) ? this.Layout.Grid as UltraGrid : null;
                    if (fireCellUpdateEvents &&
                        grid != null)
                    {
                        BeforeCellUpdateEventArgs e = new BeforeCellUpdateEventArgs(this, column, valueToAssign);
                        grid.FireEvent(GridEventIds.BeforeCellUpdate, e);
                        if (e.Cancel)
                        {
                            return false;
                        }
                    }

				    // SSP 12/19/01
				    // If the property descriptor is Infragistics.Win.UltraWinGrid.ValuePropertyDescriptor 
				    // then pass in the ourself instead of ListObject
				    //
    			
				    if ( column.PropertyDescriptor is Infragistics.Win.UltraWinGrid.ValuePropertyDescriptor )
				    {
					    column.PropertyDescriptor.SetValue( this, valueToAssign );
				    }
				    else
				    {
					    column.PropertyDescriptor.SetValue( this.ListObject, valueToAssign );

					    // SSP 8/17/01 UWG132
					    // Read comment below
					    this.SetDataChanged( true );
				    }

				    // SSP 8/15/02 UWG1553
				    // If we are not bound to an IBindingList, then we won't get any messages letting
				    // us know that data has changed and the summaries won't reflect the changed values.
				    // So here we need to refresh the summaries if the data changes of a cell.
				    //
				    // SSP 3/26/04 UWG3101
				    // Dirty the summaries even if we are bound to a binding list because some implementations
				    // do not fire ListChanged.ItemChanged until EndEdit is called on the row. What this means 
				    // is that when the user modifies a cell and stays in the same row and thus EndEdit is not
				    // called on the row object, we won't get ListChanged.ItemChanged and summaries will reflect
				    // the wrong summary value.
				    //
				    //if ( null != this.Band && !this.Band.IsBoundToIBindingList && null != this.ParentCollection )
				    if ( null != this.ParentCollection )
				    {
					    this.ParentCollection.DataChanged_DirtySummaries( );

                        //  BF NA 9.1 - UltraCombo MultiSelect
                        //  Since this does not trigger a ListChanged notification, we need
                        //  to send something out so that the CheckedRowsCollection et al.
                        //  know that the "check state" of the row might have changed.
                        if ( fireDataChanged )
                        {
                            RowsCollection rowsCollection = this.ParentCollection;
                            string columnKey = column != null ? column.Key : string.Empty;
                            CheckedListSettings.DataChangedEventArgs eventArgs = new CheckedListSettings.DataChangedEventArgs( this, columnKey, null );
                            rowsCollection.FireDataChanged( eventArgs );
                        }
				    }

				    // SSP 8/8/02 UWG1278
				    // When the value of a cell is changed and we have auto preview enabled and 
				    // if the auto preview field is the column being update, we need to regenerate
				    // the visible rows and dirty the grid element. When the auto preview field
				    // cell is emptied out or is filled with text and was previously empty, then
				    // the auto preview area will get hidden or show up. However we need to adjust
				    // the positions of rows after it. So we need to regenerate visible rows.
				    //
				    if ( null != this.Band && null != this.Band.Layout &&
					    this.Band.AutoPreviewEnabled && 
					    null != this.Band.AutoPreviewField && null != column.Key  )
				    {
					    // AS 1/8/03 - fxcop
					    // Must supply an explicit CultureInfo to the String.Compare method.
					    //if ( 0 == string.Compare( this.Band.AutoPreviewField, column.Key, true ) )
					    if ( 0 == string.Compare( this.Band.AutoPreviewField, column.Key, true, System.Globalization.CultureInfo.CurrentCulture ) )
						    this.Band.Layout.DirtyGridElement( true, true );
				    }

                    // CDS 6/18/09 TFS17819 Fire the AfterCellUpdate event
                    if (fireCellUpdateEvents &&
                        grid != null)
                    {
                        CellEventArgs e2 = new CellEventArgs(this, column);
                        grid.FireEvent(GridEventIds.AfterCellUpdate, e2);
                    }

				    // SSP 7/14/04 - UltraCalc
				    // Notify the calc manager of the change in the cell's value.
				    //
				    this.ParentCollection.NotifyCalcManager_ValueChanged( this, column );
			    }

    			
			    // SSP 8/17/01 UWG132
			    // We have duplicate properties DataChanged and IsRowDirty for
			    // keeping track if the data has changed and not committed to the
			    // backend. So I took this out.
			    // set the row dirty flag
			    //this.IsRowDirty = true;

			    // SSP 11/28/05 - NAS 6.1 Multi-cell Operations
			    // Changed the return type from void to bool. We need to know if the action was canceled or not.
			    // Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
			    // 
			    return true;
            }
            finally
            {
                //  BF 2/10/09  TFS13739
                if ( layout != null )
                    layout.IsInSetCellValue = false;
            }
        }

        #region SetCellValueConditionally

        // CDS NAS v9.1 Header CheckBox
        internal bool SetCellValueConditionally(Infragistics.Win.UltraWinGrid.UltraGridColumn column, object val)
        {
            // CDS 2/24/09 TFS12452 - Its valid for GetCellValue() to return null which would cause NullReferenceException
            //if (this.GetCellValue(column).Equals(val))
            object currentValue = this.GetCellValue(column);

            // CDS 2/26/09 TFS14684 GetCellValue() returns an Int32 for enums, 
            // so attempt to convert it back to the enum 
			currentValue = UltraGridRow.ResolveEnumValue( column, currentValue );

            if (currentValue != null &&
                currentValue.Equals(val))            
                return false;
            this.InvalidateCellAllRegions(column, true, false);

            // CDS 6/18/09 TFS17819 Make sure the Update and InitializeRow events are fired when calling SetCellValue().
            //// CDS 1/16/08 TFS12490 SetCellValue() does not operate on unbound columns. Set the value on the Cell directly in this case.
            ////return SetCellValue(column, val);
            //if (null != column.PropertyDescriptor)
            //    return SetCellValue(column, val);
            //else
            //    return this.Cells[column].SetValueInternal(val);
            bool originalSuppressInitializeRow = this.Layout.IsInitializeRowSuspendedFor(this);
            this.Layout.SuspendInitializeRowFor(this);
            try
            {
                if (null != column.PropertyDescriptor)
                    return this.SetCellValue(column, val, false, false, true, true);
                else
                    return this.Cells[column].SetValueInternal(val);
            }
            finally
            {
                if (!originalSuppressInitializeRow)
                    this.Layout.ResumeInitializeRowFor(this);
                this.FireInitializeRow();
            }
        }

        #endregion SetCellValueConditionally

		// MD 2/27/09 - TFS14684
		#region ResolveEnumValue

		private static object ResolveEnumValue( Infragistics.Win.UltraWinGrid.UltraGridColumn column, object currentValue )
		{
			if ( column.DataType != null && column.DataType.IsEnum )
			{
				try
				{
					return Enum.ToObject( column.DataType, currentValue );
				}
				catch ( Exception exc )
				{
					Debug.Fail( "Error converting the enum: " + exc.ToString() );
				}
			}

			return currentValue;
		} 

		#endregion ResolveEnumValue

        /// <summary>
		/// Returns a sibling row (if any exist)
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to the first, last, next, or previous sibling row of a row. If a sibling row does not exist, this method returns Nothing.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">The <b>HasNextSibling</b> and <b>HasPrevSibling</b> methods can be invoked to determine whether a row has a sibling row after it and before it, respectively. </p>
		/// <p class="body">The <b>GetChild</b> method and <b>HasParent</b> property can be used to return references to a child row and a parent row, respectively.</p>
		/// </remarks>
        /// <param name="sibling">specifies which sibling to return</param>
        /// <returns>A sibling row (if any exist)</returns>
		public Infragistics.Win.UltraWinGrid.UltraGridRow GetSibling(SiblingRow sibling)
		{
			return this.GetSibling( sibling, false );						
		}

		/// <summary>
		/// Returns a sibling row of this row. A sibling row is defined as a row with the same parent row. Note: Band zero rows don't have parent rows but all band zero rows are considered siblings.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to the first, last, next, or previous sibling row of a row. If a sibling row does not exist, this method returns Nothing.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">The <b>HasNextSibling</b> and <b>HasPrevSibling</b> methods can be invoked to determine whether a row has a sibling row after it and before it, respectively. </p>
		/// <p class="body">The <b>GetChild</b> method and <b>HasParent</b> property can be used to return references to a child row and a parent row, respectively.</p>
		/// </remarks>
		/// <param name="sibling">specifies which sibling to return</param>
		/// <param name="spanBands">ignored for band zero rows</param>
        /// <returns>A sibling row (if any exist)</returns>
		public Infragistics.Win.UltraWinGrid.UltraGridRow GetSibling( SiblingRow sibling, bool spanBands )
		{
			return this.GetSibling( sibling, spanBands, false );
		}

		/// <summary>
		/// Returns a sibling row of this row. A sibling row is defined as a row with the same parent row. Note: Band zero rows don't have parent rows but all band zero rows are considered siblings.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to the first, last, next, or previous sibling row of a row. If a sibling row does not exist, this method returns Nothing.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">The <b>HasNextSibling</b> and <b>HasPrevSibling</b> methods can be invoked to determine whether a row has a sibling row after it and before it, respectively. </p>
		/// <p class="body">The <b>GetChild</b> method and <b>HasParent</b> property can be used to return references to a child row and a parent row, respectively.</p>
		/// </remarks>
		/// <param name="sibling">specifies which sibling to return</param>
		/// <param name="spanBands">ignored for band zero rows</param>
		/// <param name="excludeCardBands">ignored if spanBands is false.</param>
        /// <returns>A sibling row (if any exist)</returns>
		public Infragistics.Win.UltraWinGrid.UltraGridRow GetSibling( SiblingRow sibling, bool spanBands, bool excludeCardBands )
		{
			return this.GetSibling( sibling, spanBands, excludeCardBands, IncludeRowTypes.DataRowsOnly );
		}

		// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
		// Added GetSibling overload that takes in includeSpecialRows.
		//
		/// <summary>
		/// Returns a sibling row of this row. A sibling row is defined as a row with the same parent row. Note: Band zero rows don't have parent rows but all band zero rows are considered siblings.
		/// </summary>
        /// <param name="sibling">specifies which sibling to return</param>
        /// <param name="spanBands">ignored for band zero rows</param>
        /// <param name="excludeCardBands">ignored if spanBands is false.</param>
        /// <param name="includeSpecialRows">specifies whether to include special rows</param>
        /// <returns>A sibling row (if any exist)</returns>		
		public Infragistics.Win.UltraWinGrid.UltraGridRow GetSibling( SiblingRow sibling, bool spanBands, bool excludeCardBands, bool includeSpecialRows )
		{
			return this.GetSibling( sibling, spanBands, excludeCardBands, ! includeSpecialRows ? IncludeRowTypes.DataRowsOnly : IncludeRowTypes.SpecialRows );
		}

		// SSP 11/11/03 Add Row Feature
		// Added includeRowTypes parameter.
		//
		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetSibling( SiblingRow sibling, bool spanBands, bool excludeCardBands, IncludeRowTypes includeRowTypes )
		{
			Infragistics.Win.UltraWinGrid.UltraGridRow row = null;

			// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// We have to take into account filter row, summary row etc...
			// Replaced TemplateAddRow enum member with SpecialRows.
			// Commented out the following code.
			// 
			

			// MD 8/2/07 - 7.3 Performance
			// Cache parent row - Prevent calling expensive getters multiple times
			UltraGridRow parentRow = this.ParentRow;

			switch ( sibling )
			{
				case SiblingRow.First:
				{
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( spanBands && null != this.ParentRow )
					if ( spanBands && null != parentRow )
					{
						// JJD 1/19/02 - UWG940
						// Pass excludeCardBands flag into GetFirstRow
						//
						// SSP 11/11/03 Add Row Feature
						// Pass along the includeRowTypes parameter.
						//
						//row = this.ParentRow.ChildBands.GetFirstRow( excludeCardBands );
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//row = this.ParentRow.ChildBands.GetFirstRow( excludeCardBands, includeRowTypes );
						row = parentRow.ChildBands.GetFirstRow( excludeCardBands, includeRowTypes );
						break;
					}

					// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Replaced TemplateAddRow enum member with SpecialRows.
					// Commented out the original code and added the new code below.
					// 
					// --------------------------------------------------------------------------------------
					row = this.parentCollection.GetFirstRow( includeRowTypes );
					
					// --------------------------------------------------------------------------------------
				}
					break;

				case SiblingRow.Next:									
				{
					// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Replaced TemplateAddRow enum member with SpecialRows.
					// Commented out the original code and added the new code below.
					// 
					// --------------------------------------------------------------------------------------
					row = this.parentCollection.GetRowAtIndexOffset( this, 1, includeRowTypes );

					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( null == row && spanBands && null != this.ParentRow )
					if ( null == row && spanBands && null != parentRow )
					{
						UltraGridChildBand childBand = this.ParentChildBand;

						do
						{
							// MD 8/2/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//childBand = this.ParentRow.ChildBands.GetNextChildBand( childBand, excludeCardBands );
							childBand = parentRow.ChildBands.GetNextChildBand( childBand, excludeCardBands );

							if ( null != childBand )
								row = childBand.Rows.GetFirstRow( includeRowTypes );
						}
						while ( null == row && null != childBand );
					}
					
					// --------------------------------------------------------------------------------------
				}
					break;

				case SiblingRow.Previous:
				{
					// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Replaced TemplateAddRow enum member with SpecialRows.
					// Commented out the original code and added the new code below.
					// 
					// --------------------------------------------------------------------------------------
					row = this.parentCollection.GetRowAtIndexOffset( this, -1, includeRowTypes );

					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( null == row && spanBands && null != this.ParentRow )
					if ( null == row && spanBands && null != parentRow )
					{
						UltraGridChildBand childBand = this.ParentChildBand;

						do
						{
							// MD 8/2/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//childBand = this.ParentRow.ChildBands.GetPreviousChildBand( childBand, excludeCardBands );
							childBand = parentRow.ChildBands.GetPreviousChildBand( childBand, excludeCardBands );

							if ( null != childBand )
								row = childBand.Rows.GetLastRow( includeRowTypes );
						}
						while ( null == row && null != childBand );
					}
					
					// --------------------------------------------------------------------------------------
				}
					break;

				case SiblingRow.Last:
				{
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( spanBands && null != this.ParentRow )
					if ( spanBands && null != parentRow )
					{
						// JJD 1/19/02 - UWG940
						// Pass excludeCardBands flag into GetLastRow
						//
						// SSP 11/11/03 Add Row Feature
						// Pass along the includeRowTypes parameter.
						//
						//row = this.ParentRow.ChildBands.GetLastRow( excludeCardBands );
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//row = this.ParentRow.ChildBands.GetLastRow( excludeCardBands, includeRowTypes );
						row = parentRow.ChildBands.GetLastRow( excludeCardBands, includeRowTypes );
						break;
					}
                
					// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Replaced TemplateAddRow enum member with SpecialRows.
					// Commented out the original code and added the new code below.
					// 
					// --------------------------------------------------------------------------------------
					row = this.parentCollection.GetLastRow( includeRowTypes );
					
					// --------------------------------------------------------------------------------------
				}
					break;				
			}

			return row;
		}

		internal UltraGridChildBand ParentChildBand
		{
			get
			{
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null == this.ParentRow )
				//    return null;
				//
				//return this.ParentRow.ChildBands[ this.band ];
				UltraGridRow parentRow = this.ParentRow;

				if ( null == parentRow )
					return null;

				return parentRow.ChildBands[ this.band ];
			}
		}    


		/// <summary>
		/// Returns the rows collection this row belongs to
		/// </summary>
		public RowsCollection ParentCollection 
		{
			get
			{
				return this.parentCollection;
			}
		}

		// SSP 9/12/02 UWG1637
		//
		internal bool IsDescendantOf( UltraGridRow row )
		{
			if ( null != row )
			{
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.ParentRow == row )
				//    return true;
				//
				//if ( null != this.ParentRow )
				//    return this.ParentRow.IsDescendantOf( row );
				UltraGridRow parentRow = this.ParentRow;

				if ( parentRow == row )
					return true;

				if ( null != parentRow )
					return parentRow.IsDescendantOf( row );
			}

			return false;
		}

		// SSP 10/15/04 - UltraCalc
		// Added IsTrivialDescendantOf method.
		//
		internal bool IsTrivialDescendantOf( UltraGridRow row )
		{
			return this == row || this.IsDescendantOf( row );
		}

		internal bool IsDescendantOf( RowsCollection rows )
		{
			UltraGridRow row = this;
			while ( null != row && row.ParentCollection != rows )
				row = row.ParentRow;

			return null != row;
		}

		/// <summary>
		/// Returns this row's parent row which could be an UltraGridRow instance
		/// or an UltraGridGroupByRow instance.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow ParentRow
		{
			get
			{
				// use the parent collection's internally exposed
				// ParentRow property
				if ( null != this.parentCollection )
					this.parentCollection.TopLevelRowsCollection.VerifyGroupByVersionHelper( );

				if ( null != this.parentCollection )
					return this.parentCollection.ParentRow;

				return null;
			}
		}

		internal Infragistics.Win.UltraWinGrid.RowsCollection ContainedByRows
		{
			get
			{
				return this.parentCollection;
			}
		}

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered before a row.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <b>RowSpacingBefore</b> property to specify the space that precedes a row in the band or the grid. Space between rows allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that follows a row, use the <b>RowSpacingAfter</b> property.</p>
		/// <p class="body">You can use the <b>RowSpacingAfter</b> property to specify the space that follows a row in the band or the grid. Space between rows allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that precedes a row, use the <b>RowSpacingBefore</b> property.</p>
		/// <p class="body">
		/// To specify the row spacing of all rows in a grid or band, use the Override's 
		/// <see cref="UltraGridOverride.RowSpacingAfter"/> and <see cref="UltraGridOverride.RowSpacingBefore"/>
		/// properties.
		/// </p>
		/// <seealso cref="UltraGridOverride.RowSpacingAfter"/>
		/// <seealso cref="UltraGridOverride.RowSpacingBefore"/>
		/// </remarks>
		public int RowSpacingBefore
		{
			get
			{
				return this.rowSpacingBefore;
			}
			set 
			{
				if ( value != this.rowSpacingBefore )
				{
					this.rowSpacingBefore = value;

					// SSP 1/19/04 UWG2902
					// Bump the grand verify version because changing the row spacing affects
					// the max scroll position in ScrollBounds mode of ScrollToFill.
					//
					if ( null != this.Layout )
						this.Layout.BumpGrandVerifyVersion( );

					this.NotifyPropChange( PropertyIds.RowSpacingBefore, null );
				}
			}
		}

		

 
		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered before a row.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <b>RowSpacingAfter</b> property to specify the space that follows a row in the band or the grid. Space between rows allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that precedes a row, use the <b>RowSpacingBefore</b> property.</p>
		/// <p class="body">
		/// To specify the row spacing of all rows in a grid or band, use the Override's 
		/// <see cref="UltraGridOverride.RowSpacingAfter"/> and <see cref="UltraGridOverride.RowSpacingBefore"/>
		/// properties.
		/// </p>
		/// <seealso cref="UltraGridOverride.RowSpacingAfter"/>
		/// <seealso cref="UltraGridOverride.RowSpacingBefore"/>
		/// </remarks>
		public int RowSpacingAfter
		{
			get
			{
				return this.rowSpacingAfter;
			}
			set 
			{
				if ( value != this.rowSpacingAfter )
				{
					this.rowSpacingAfter = value;

					// SSP 1/19/04 UWG2902
					// Bump the grand verify version because changing the row spacing affects
					// the max scroll position in ScrollBounds mode of ScrollToFill.
					//
					if ( null != this.Layout )
						this.Layout.BumpGrandVerifyVersion( );

					this.NotifyPropChange( PropertyIds.RowSpacingAfter, null );
				}
			}
		}

		


		/// <summary>
		/// Returns the actual value used for RowSpacingBefore for this row.
		/// </summary>
		public virtual int RowSpacingBeforeResolved 
		{
			get 
			{
                // MBS 4/24/09 - TFS12665
                // Similarly to the optimizations made elsewhere, when we're performing
                // operations where we're reusing info that will not change (i.e.
                // when recreating all of the visible rows), we can cache some of the
                // more expensive operations.  In this case, we don't need to keep walking
                // up the parent chain every time that we need to ask for the height
                UltraGridLayout.CachedRowInfo rowInfo = null;
                Dictionary<UltraGridRow, UltraGridLayout.CachedRowInfo> cachedInfo = null;
                if (this.Layout != null && this.Layout.IsCachingBandInfo)
                {
                    cachedInfo = this.Layout.CachedRowData;
                    if (cachedInfo.TryGetValue(this, out rowInfo) && rowInfo.RowSpacingBefore > -1)
                        return rowInfo.RowSpacingBefore;
                }

				// SSP 5/3/05 - NAS 5.2 Fixed Rows/Filter Row/Fixed Add-Row/Extension of Summaries Functionality
				// Moved the code into the new RowSpacingBeforeResolvedBase. Use that instead. Also added code to
				// add the space for special row separator.
				// 
				// ----------------------------------------------------------------------------------------------
				int rowSpacingBefore = this.RowSpacingBeforeResolvedBase;

				if ( this.HasSpecialRowSeparatorBefore )
					rowSpacingBefore += this.BandInternal.SpecialRowSeparatorHeightResolved;
				
				// ----------------------------------------------------------------------------------------------

				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.ParentRow != null )
				UltraGridRow parentRow = this.ParentRow;

				if ( parentRow != null )
				{
					ViewStyleBase viewStyle = this.band.Layout.ViewStyleImpl;

					// if there are multiple rows on a tier (horizontal view style)
					// and we are the first sibling row (no previous sibling) then
					// make sure this rows height is at least as big as any of
					// its ancestors
					//
					if ( viewStyle.HasMultiRowTiers && 
						// SSP 11/11/03 Add Row Feature
						//
						//!this.HasPrevSibling(false, true) &&
						!this.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) &&
						!this.HasSiblingInPrevSiblingBand(true) )
					{
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//rowSpacingBefore = System.Math.Max( rowSpacingBefore, this.ParentRow.RowSpacingBeforeResolved );
						rowSpacingBefore = System.Math.Max( rowSpacingBefore, parentRow.RowSpacingBeforeResolved );
					}
				}

                // MBS 4/24/09 - TFS12665
                if (cachedInfo != null)
                {
                    if (rowInfo != null)
                        rowInfo.RowSpacingBefore = rowSpacingBefore;
                    else
                    {
                        rowInfo = new UltraGridLayout.CachedRowInfo();
                        rowInfo.RowSpacingBefore = rowSpacingBefore;

                        // MBS 5/13/09 - TFS17523
                        //cachedInfo.Add(this, rowInfo);
                        cachedInfo[this] = rowInfo;
                    }
                }

				return rowSpacingBefore;
			}
		}

		/// <summary>
		/// Returns the actual value used for RowSpacingAfter for this row.
		/// </summary>
		public virtual int RowSpacingAfterResolved
		{
			get 
			{
                // MBS 4/24/09 - TFS12665
                // Similarly to the optimizations made elsewhere, when we're performing
                // operations where we're reusing info that will not change (i.e.
                // when recreating all of the visible rows), we can cache some of the
                // more expensive operations.  In this case, we don't need to keep walking
                // up the parent chain every time that we need to ask for the height
                UltraGridLayout.CachedRowInfo rowInfo = null;
                Dictionary<UltraGridRow, UltraGridLayout.CachedRowInfo> cachedInfo = null;
                if (this.Layout != null && this.Layout.IsCachingBandInfo)
                {
                    cachedInfo = this.Layout.CachedRowData;
                    if (cachedInfo.TryGetValue(this, out rowInfo) && rowInfo.RowSpacingAfter > -1)
                        return rowInfo.RowSpacingAfter;
                }

				// SSP 5/3/05 - NAS 5.2 Fixed Rows/Filter Row/Fixed Add-Row/Extension of Summaries Functionality
				// Moved the code into the new RowSpacingAfterResolvedBase. Use that instead. Also added code to
				// add the space for special row separator.
				// 
				// ----------------------------------------------------------------------------------------------
				int rowSpacingAfter = this.RowSpacingAfterResolvedBase;

				if ( this.HasSpecialRowSeparatorAfter )
					rowSpacingAfter += this.BandInternal.SpecialRowSeparatorHeightResolved;
				
				// ----------------------------------------------------------------------------------------------

				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.ParentRow != null )
				UltraGridRow parentRow = this.ParentRow;

				if ( parentRow != null )
				{
					ViewStyleBase viewStyle = this.band.Layout.ViewStyleImpl;

					// if there are multiple rows on a tier (horizontal view style)
					// and we are the first sibling row (no previous sibling) then
					// make sure this rows height is at least as big as any of
					// its ancestors
					//
					if ( viewStyle.HasMultiRowTiers && 
						// SSP 11/11/03 Add Row Feature
						//
						//!this.HasPrevSibling(false, true) &&
						!this.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) &&
						!this.HasSiblingInPrevSiblingBand(true) )
					{
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//rowSpacingAfter = System.Math.Max( rowSpacingAfter, this.ParentRow.RowSpacingAfterResolved );
						rowSpacingAfter = System.Math.Max( rowSpacingAfter, parentRow.RowSpacingAfterResolved );
					}
				}

                // MBS 4/24/09 - TFS12665
                if (cachedInfo != null)
                {
                    if (rowInfo != null)
                        rowInfo.RowSpacingAfter = rowSpacingAfter;
                    else
                    {
                        rowInfo = new UltraGridLayout.CachedRowInfo();
                        rowInfo.RowSpacingAfter = rowSpacingAfter;

                        // MBS 5/13/09 - TFS17523
                        //cachedInfo.Add(this, rowInfo);
                        cachedInfo[this] = rowInfo;
                    }
                }

				return rowSpacingAfter;
			}
                                                
		}

		/// <summary>
		/// Returns or sets the height of the row (excluding spacing).
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Height</b> property is used to determine the vertical dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">The Override's <see cref="UltraGridOverride.DefaultRowHeight"/> can be used to 
		/// specify the default height of all rows associated with a grid or a band.</p>
		/// <seealso cref="UltraGridOverride.DefaultRowHeight"/>
		/// <seealso cref="UltraGridOverride.RowSizing"/>
		/// </remarks>
		public virtual int Height
		{
			get
			{
				return this.BaseHeight;
			}
			set
			{
				// call the band's resizerow method
				//
				this.band.ResizeRow( this, value, true );
			}
		}

		// JJD 10/04/01
		// This was calling itself (stack fault) so I guess no one
		// ws using it
		//
		

		// SSP 11/14/03 Add Row Feature
		// Added an overload of HiddenStateChanged that takes in dirtySummaries parameter.
		//
		internal void HiddenStateChanged( )
		{
			this.HiddenStateChanged( true );
		}

		// SSP 11/3/03 
		// Added HiddenStateChanged method. Code in this method was moved from the set of the 
		// Hidden property.
		//
		// SSP 11/14/03 Add Row Feature
		// Added dirtySummaries parameter.
		//
		internal void HiddenStateChanged( bool dirtySummaries )
		{
			// SSP 9/4/02 UWG1630
			// When a row is hidden, make sure any active cell belonging to this row exits
			// the edit mode. Otherwise we will end up in a weird state where the cell is
			// not visible but it's in edit mode.
			//
			// ----------------------------------------------------------------------------
			if ( this.HiddenInternal )
			{
				UltraGridCell activeCell = 
					null != this.band && null != this.band.Layout
					// SSP 3/9/07 - Optimizations
					// 
					//? this.band.Layout.ActiveCell 
					? this.band.Layout.ActiveCellInternal 
					: null;

				if ( null != activeCell && activeCell.IsInEditMode && this == activeCell.Row )
					activeCell.ExitEditMode( false, true );
			}
			// ----------------------------------------------------------------------------

			// SSP 3/20/02
			// Dirty the scrollable row count and the grid element.
			//
			// ------------------------------------------------------
			// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
			// Commented out the original code and added call to NotifyScrollCountChanged.
			//
			
			this.NotifyScrollCountChanged( );

			// SSP 3/9/07 BR20933
			// Commented out the following code. This is not necessary since the above call
			// to NotifyScrollCountChanged will do the necessary invalidation. Not only that
			// it's more efficient in that it will do invalidations more intelligently.
			// Besides this code causes BR20933.
			// 
			
					
			// SSP 10/18/04
			// If the formula row index type is VisibleIndex and a row's visibility changes then we
			// need to notify the calc manager.
			//
			this.ParentCollection.NotifyCalcManager_HiddenStateChanged( this );

			// SSP 6/5/02
			// If a row is hidden or unhidden, we need to dirty the summaries so
			// they get recalculated.
			//
			// SSP 11/14/03 Add Row Feature
			// Added dirtySummaries parameter.
			//
			//if ( null != this.parentCollection )
			if ( null != this.parentCollection && dirtySummaries )
				this.parentCollection.DataChanged_DirtySummaries( );
			// ------------------------------------------------------
		}

		/// <summary>
		/// Determines whether the object will be displayed. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Hidden</b> property determines whether an object is visible. Hiding an object may have have effects that go beyond simply removing it from view. For example, hiding a band also hides all the rows in that band. Also, changing the <b>Hidden</b> property of an object affects all instances of that object. For example, a hidden column or row is hidden in all scrolling regions.</p>
		/// <p class="body">The bahavior of <b>Hidden</b> property with Row Filtering functionality has changed in UltaGrid Version 3.0. In previous version the <b>Hidden</b> property of a row returned false if the row was filtered out where as in Version 3.x the <b>Hidden</b> property will return the value it was set to regardless of whether the row is filtered out or not. Instead in Version 3.x <see cref="IsFilteredOut"/> property is added that indicates whether the row is filtered out or not. <see cref="HiddenResolved"/> behaves the same and takes into account whether the row is filtered out or not. Note though this only applies when the <see cref="UltraGridOverride.RowFilterAction"/> is set to <b>HideFilteredOutRows</b>.</p>
		/// </remarks>
		// SSP 3/27/02
		// Made this property virtual so that the group by row can override
		// it.
		//
		//public bool Hidden
		public virtual bool Hidden
		{
			get
			{
				// SSP 8/1/03 UWG1654 - Filter Action
				// Implemented Filter action (ShowEnabled, ShowDisabled, Hide).
				// Added IsFilteredOut proeprty. In V2 we used to set the hidden member variable
				// to true whenever the row get filtered out and reset it to false when it was
				// unfiltered. In V3 we won't do that any more. Instead we added IsRowFilteredOut
				// which indicates whether the row is filtered out or not. This property should
				// simply retrun the value it was set to.
				// Commented out below code.
				//
				
				
				return this.hidden || this.deleted;
			}
			set
			{
				if ( value != this.hidden && !this.deleted )
				{
					this.hidden = value;
					
					// SSP 11/3/03 
					// Added HiddenStateChanged method. Code that was here was moved into that new method'
					// so that it could be called from filter code as well.
					//
					this.HiddenStateChanged( );

					this.NotifyPropChange( PropertyIds.Hidden, null );
				}
			}
		}

		


		// SSP 3/21/02
		// Added code for row filtering code
		//
		#region "Filter code"

		private bool AreFiltersActiveOnChildRowsAndHasChildRows( )
		{
			// If filters are active, then return true because we do want to show
			// the band header any ways and allow the user to be able to expand the
			// parent row whose children are all filtered out.
			//
			ChildBandsCollection childBands = this.ChildBands;
			if ( null != childBands )
			{
				for ( int i = 0; i < childBands.Count; i++ )
				{
					UltraGridChildBand childBand = childBands[i];

					if ( childBand.Rows.Count > 0 )
					{
						if ( childBand.HaveRowsBeenInitialized )
						{
							if ( childBand.Rows.AreAnyFiltersActive )
								return true;
						}
						else
						{
							if ( childBand.Band.AreAnyBandFiltersActive ) 
								return true;
						}
					}
				}
			}

			return false;
		}

		#region ApplyFilters

		// SSP 10/27/04 - UltraCalc
		// Moved the filter verification logic into the row collection. This is so
		// that we can evaluate filters at once on all the rows of a row collection
		// and fire rows synced event to the calc manager instead of having to fire
		// row deleted/added event for every row whose hidden state changes.
		// Commented out EnsureFiltersEvaluated and added ApplyFilters method.
		//
		// SSP 5/24/05 BR04243
		// Made the method virtual so it can be overridden by group-by row.
		// 
		//internal void ApplyFilters( ColumnFiltersCollection columnFilters )
		internal virtual void ApplyFilters( ColumnFiltersCollection columnFilters )
		{
			// SSP 7/18/02 UWG1396
			// If the row is an add new row, then don't filter it out.
			// What happens is that when a new row is added it will contain DNUlls in the 
			// fields (unless ofcource default values for the fiels are set up in which 
			// case the cell values would be those default values, however most of the time
			// they are going to be DNBulls). And filters will filter them out because they
			// don't match the criteria. The user will have no way of updating the row
			// without unfiltering. And also this will cause an DataError to be raised as
			// clicking somewhere else in the grid could potentially cause the EndEdit
			// on the row called and any number of data constraint errors may get raised.)
			// So don't filter the row if the row is an add new row. IsAddRow will get reset
			// as soon as the row is moved off.
			//
			// SSP 4/14/05 - NAS 5.2 Fixed Rows
			// Use the new IsDataRow which checks for IsTemplateAddRow as well as other special
			// rows like the filter row.
			//
			//if ( this.IsAddRow || this.IsTemplateAddRow )
			// SSP 3/9/07 - Optimizations
			// Non-data rows never get filtered out. However to prevent them from calling 
			// ApplyFilters method repeatedly, set their InternalIsFilteredOut to false.
			// 
			// ----------------------------------------------------------------------------
			//if ( this.IsAddRow || ! this.IsDataRow )
			//	return;
			if ( this.IsAddRow )
				return;

			if ( ! this.IsDataRow )
			{
				this.InternalIsFilteredOut = false;
				return;
			}
			// ----------------------------------------------------------------------------

			// 0 means filtered out, 1 means not filtered out, -1 means filter has
			// never been evaluated on this row.
			// 
			this.InternalIsFilteredOut = null != columnFilters && ! columnFilters.DoesRowPassFilters( this );

			UltraGridBase grid = this.Layout.Grid;
			if ( null != grid )
			{
				FilterRowEventArgs e = new FilterRowEventArgs( this, this.InternalIsFilteredOut );
				// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
				//grid.FireFilterRow( e );
				grid.FireCommonEvent(CommonEventIds.FilterRow,  e );

				// Added RowFilteredOut off the event args which the user can modify.
				// In case the user has modified it, then set the cachedIsFilteredOut
				// to that value.
				//
				this.InternalIsFilteredOut = e.RowFilteredOut;
			}
		}

		// SSP 11/19/04 BR00655 BR00930
		//
		internal bool InternalIsFilteredOut
		{
			get
			{
				return FILTERED_OUT == this.cachedIsFilteredOut;
			}
			set
			{
				this.cachedIsFilteredOut = value ? FILTERED_OUT : FILTERED_IN;
			}
		}

		

		#endregion // ApplyFilters

		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide).
		// Added IsFilteredOut property.
		//
		#region IsFilteredOut

		/// <summary>
		/// Indicates whether the row is filtered out or not. A filtered out row is a row that does not pass the filter criteria. Filter criteria can be sepcified by <see cref="UltraGridBand.ColumnFilters"/> or <see cref="RowsCollection.ColumnFilters"/> property depending on the <see cref="UltraGridOverride.RowFilterMode"/> setting.
		/// </summary>
		/// <remarks>
		/// <p class="body">Indicates whether the row is filtered out or not. A filtered out row is a row that does not pass the filter criteria. Filter criteria can be sepcified by <see cref="UltraGridBand.ColumnFilters"/> or <see cref="RowsCollection.ColumnFilters"/> property depending on the <see cref="UltraGridOverride.RowFilterMode"/> setting.</p>
		/// <seealso cref="UltraGridOverride.AllowRowFiltering"/> <seealso cref="UltraGridOverride.RowFilterMode"/> <seealso cref="RowsCollection.ColumnFilters"/> <seealso cref="UltraGridBand.ColumnFilters"/>
		/// </remarks>
		public bool IsFilteredOut
		{
			get
			{
				// SSP 10/27/04 - UltraCalc
				// Moved the filter verification logic into the row collection. This is so
				// that we can evaluate filters at once on all the rows of a row collection
				// and fire rows synced event to the calc manager instead of having to fire
				// row deleted/added event for every row whose hidden state changes.
				//
				//this.EnsureFiltersEvaluated( );
				if ( null != this.parentCollection )
				{
					this.parentCollection.EnsureFiltersEvaluated( );

					// SSP 11/24/04 BR00930
					// Added following if block.
					//
					if ( FILTERS_NEVER_EVALUATED == this.cachedIsFilteredOut )
					{
						this.ApplyFilters( this.parentCollection.ActiveColumnFilters );
						if ( this.InternalIsFilteredOut )
							this.HiddenStateChanged( );
					}
				}

				// NOTE: 0 means filtered out.
				//
				return FILTERED_OUT == this.cachedIsFilteredOut;
			}
		}

		#endregion // IsFilteredOut

		// SSP 8/1/03 UWG1654 - Filter Action
		// Added a way for the user to be able to construct filter conditions and evaluate
		// it themselves by calling the new UltraGridRow.MeetsCriteria method.
		// Added the MeetsCriteria method below.
		//
		#region MeetsCriteria

		private void ValidateNotGroupByRow( )
		{
			if ( this is UltraGridGroupByRow )
				throw new NotSupportedException( SR.GetString( "LER_NotSupportedException_347" ) );
		}
		
		/// <summary>
		/// Returns true if the row meets the criteria specified by the passed in filter condition.
		/// </summary>
		/// <param name="filterCondition">An instance of <see cref="FilterCondition"/> containing information on the criteria.</param>
		/// <returns></returns>
		/// <remarks>
		/// <p class="body">Returns true if the row meets the criteria specified by the passed in filter condition.</p>
		/// <seealso cref="FilterCondition"/>
		/// </remarks>
        /// <returns>true if the row meets the criteria of the FilterCondition, false if it does not.</returns>
		public bool MeetsCriteria( FilterCondition filterCondition )
		{
			this.ValidateNotGroupByRow( );

			if ( null == filterCondition )
				throw new ArgumentNullException( "filterCondition" );

			if ( filterCondition.Column.Band != this.Band )
				throw new ArgumentException( SR.GetString( "LER_ArgumentException_11" ), "filterCondition" );

			return filterCondition.MeetsCriteria( this );
		}

		/// <summary>
		/// Returns true if the row meets the criteria specified by the passed in filter conditions and the logical operator. If the logical operator is <b>And</b> and the row meets all the conditions in the filterConditions array then this method returns true. If the logical operator is <b>Or</b> and the row meets at least one filter condition then this method returns true. Otherwise it returns false.
		/// </summary>
		/// <param name="filterConditions">Filter conditions.</param>
		/// <param name="logicalOperator">Logical operator to combine filter conditions in the filter conditions array.</param>
		/// <returns></returns>
		/// <remarks>
		/// <p class="body">Returns true if the row meets the criteria specified by the passed in filter conditions and the logical operator. If the logical operator is <b>And</b> and the row meets all the conditions in the filterConditions array then this method returns true. If the logical operator is <b>Or</b> and the row meets at least one filter condition then this method returns true. Otherwise it returns false.</p>
		/// <seealso cref="FilterCondition"/> <seealso cref="FilterLogicalOperator"/>
		/// </remarks>
        /// <returns>true if the row meets the criteria of the FilterCondition, false if it does not.</returns>
		public bool MeetsCriteria( FilterCondition[] filterConditions, FilterLogicalOperator logicalOperator )
		{
			this.ValidateNotGroupByRow( );

			if ( ! Enum.IsDefined( typeof( FilterLogicalOperator ), logicalOperator ) )
				throw new InvalidEnumArgumentException( "logicalOperator", (int)logicalOperator, typeof( FilterComparisionOperator ) );

			if ( null == filterConditions )
				throw new ArgumentNullException( "filterConditions" );

			if ( filterConditions.Length <= 0 )
				throw new ArgumentException( SR.GetString( "LER_ArgumentException_12" ), "filterConditions" );

			for ( int i = 0; i < filterConditions.Length; i++ )
			{
				if ( FilterLogicalOperator.And == logicalOperator )
				{
					// If the operator is And then if any one of the conditions fails then return false.
					//
					if ( ! this.MeetsCriteria( filterConditions[i] ) )
						return false;
				}
				else if ( FilterLogicalOperator.Or == logicalOperator )
				{
					// If the operator is Or then if any one of the conditions succeeds then return true.
					//
					if ( this.MeetsCriteria( filterConditions[i] ) )
						return true;
				}
				else
				{
					Debug.Assert( false, "Unknown type of FilterLogicalOperator !" );
				}
			}

			// If the operator is And, then the only way for the above loop to have finished 
			// is for all the conditions to have been true.
			//
			if ( FilterLogicalOperator.And == logicalOperator )
				return true;
				// If it was Or and loop finished, then none of the conditions succeeded so in 
				// the case of the Or return false.
			else 
				return false;
		}

		/// <summary>
		/// Returns true if the row meets the criteria specified by the passed in column filter which contains the collection of filter conditions.
		/// </summary>
		/// <param name="columnFilter">An instance of <see cref="ColumnFilter"/> containing information on the criteria.</param>
		/// <returns></returns>
		/// <remarks>
		/// <p class="body">Returns true if the row meets the criteria specified by the passed in column filter.</p>
		/// <seealso cref="ColumnFilter"/> <seealso cref="ColumnFilter.FilterConditions"/>
		/// </remarks>
        /// <returns>true if the row meets the criteria of the FilterCondition, false if it does not.</returns>
		public bool MeetsCriteria( ColumnFilter columnFilter )
		{
			this.ValidateNotGroupByRow( );

			if ( null == columnFilter )
				throw new ArgumentNullException( "columnFilter" );

			if ( columnFilter.Column.Band != this.Band )
				throw new ArgumentException( SR.GetString( "LER_ArgumentException_13" ), "columnFilter" );

			if ( columnFilter.FilterConditions.Count <= 0 )
				throw new ArgumentException( SR.GetString( "LER_ArgumentException_14" ), "columnFilter" );

			FilterCondition[] filterConditions = new FilterCondition[ columnFilter.FilterConditions.Count ];

			for ( int i = 0; i < columnFilter.FilterConditions.Count; i++ )
				filterConditions[i] = columnFilter.FilterConditions[i];

			return this.MeetsCriteria( filterConditions, columnFilter.LogicalOperator );
		}

		/// <summary>
		/// Returns true if the row meets the criteria specified by the passed in column filters and the logical operator. If the logical operator is <b>And</b> and the row meets all the conditions in the column filters array then this method returns true. If the logical operator is <b>Or</b> and the row meets at least one of the column filter then this method returns true. Otherwise it returns false.
		/// </summary>
		/// <param name="columnFilters">Filter conditions.</param>
		/// <param name="logicalOperator">Logical operator to combine column filters in the column filters array.</param>
		/// <returns></returns>
		/// <remarks>
		/// <p class="body">Returns true if the row meets the criteria specified by the passed in column filters and the logical operator. If the logical operator is <b>And</b> and the row meets all the conditions in the column filters array then this method returns true. If the logical operator is <b>Or</b> and the row meets at least one of the column filter then this method returns true. Otherwise it returns false.</p>
		/// <seealso cref="ColumnFilter"/> <seealso cref="FilterLogicalOperator"/>
		/// </remarks>
        /// <returns>true if the row meets the criteria of the FilterCondition, false if it does not.</returns>
		public bool MeetsCriteria( ColumnFilter[] columnFilters, FilterLogicalOperator logicalOperator )
		{
			this.ValidateNotGroupByRow( );

			if ( null == columnFilters )
				throw new ArgumentNullException( "columnFilters" );

			if ( columnFilters.Length <= 0 )
				throw new ArgumentException( "ColumnFilters must contain at least one value ColumnFilter.", "columnFilters" );

			if ( ! Enum.IsDefined( typeof( FilterLogicalOperator ), logicalOperator ) )
				throw new InvalidEnumArgumentException( "logicalOperator", (int)logicalOperator, typeof( FilterComparisionOperator ) );

			for ( int i = 0; i < columnFilters.Length; i++ )
			{
				if ( FilterLogicalOperator.And == logicalOperator )
				{
					// If the operator is And then if any one of the conditions fails then return false.
					//
					if ( ! this.MeetsCriteria( columnFilters[i] ) )
						return false;
				}
				else if ( FilterLogicalOperator.Or == logicalOperator )
				{
					// If the operator is Or then if any one of the conditions succeeds then return true.
					//
					if ( this.MeetsCriteria( columnFilters[i] ) )
						return true;
				}
				else
				{
					Debug.Assert( false, "Unknown type of FilterLogicalOperator !" );
				}
			}

			// If the operator is And, then the only way for the above loop to have finished 
			// is for all the conditions to have been true.
			//
			if ( FilterLogicalOperator.And == logicalOperator )
				return true;
				// If it was Or and loop finished, then none of the conditions succeeded so in 
				// the case of the Or return false.
			else 
				return false;
		}

		#endregion // MeetsCriteria
        
		#region HasFilters

		// SSP 8/1/03 UWG1654 - Filter Action
		// Added HasFilters property.
		//
		internal bool HasFilters
		{
			get
			{
				return null != this.ParentCollection && this.ParentCollection.AreAnyFiltersActive;
			}
		}

		#endregion // HasFilters

		#region HiddenInternal

		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide).
		// Added IsFilteredOut and HiddenInternal properties. In V2 we used to set the 
		// hidden member variable to true whenever the row get filtered out and reset 
		// it to false when it was unfiltered. In V3 we won't do that any more. 
		// Instead we added IsFilteredOut which indicates whether the row is filtered 
		// out or not. However internally, we want to use take into account both the
		// Hidde property setting and IsFilteredOut state. To do that added HiddenInternal
		// property that will return what the Hidden property in V2 would have returned
		// when the row was filtered out (Hidden or'd with IsFilteredOut).
		//
		internal bool HiddenInternal
		{
			get
			{
				return this.Hidden || ( this.IsFilteredOut && RowFilterAction.HideFilteredOutRows == this.BandInternal.RowFilterActionResolved ) 
					// SSP 11/11/03 Add Row Feature
					// If the row is a template add-row and template add-rows are not visible, then return true for hidden.
					//
					|| ( this.IsTemplateAddRow && ! this.ParentCollection.IsTemplateAddRowVisible );
			}
		}

		#endregion // HiddenInternal

		#endregion // End of Filter code region


		/// <summary>
		/// Returns true if the hidden property for this row is set to
		/// true or any of this row's ancestor rows have their hidden
		/// property set to true.
		/// </summary>
		// SSP 3/27/02
		// Made this property virtual so that the group by row can override
		// it.
		//
		//public bool HiddenResolved
		public virtual bool HiddenResolved
		{
			get
			{
				// SSP 2/7/02
				// code for row filters
				//
				// SSP 8/1/03 UWG1654 - Filter Action
				// Implemented Filter action (ShowEnabled, ShowDisabled, Hide).
				// Added IsFilteredOut property.
				// Commented out the original code and added the new code below.
				//
				// ----------------------------------------------------------------------------
				
				if ( this.HiddenInternal )
					return true;
				// ----------------------------------------------------------------------------

				// SSP 10/31/01 UWG614
				// If the band is hidden, then return true as well.
				//
				if ( this.BandInternal.HiddenResolved )
					return true;

				// if this is not a row in band 0 then return the
				// hidden status of our parent row.
				// This effectively walks up the parent
				// chain.
				//
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null != this.ParentRow )
				//    return this.ParentRow.HiddenResolved;
				UltraGridRow parentRow = this.ParentRow;

				if ( null != parentRow )
					return parentRow.HiddenResolved;

				// since we got to the band 0 row without hidden
				// being set we can return false
				//
				return false;
			}
		}

		// SSP 2/20/04 - Virtual Mode
		//
		

		/// <summary>
		/// Returns whether the current row is an alternate row. Alternate rows can have their own appearance settings.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowAppearance"/> and <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowAlternateAppearance"/> properties to provide different appearance settings for odd and even rows in the grid. The <b>IsAlternate</b> property indicates which appearance the row will use. (Note that if no alternate appearance is specified, all rows will use the standard appearance and there will be no visible difference between alternate  and non-alternate rows.)</p>
		/// </remarks>
		public bool IsAlternate
		{
			get
			{
				// SSP 4/8/03 UWG2104
				// Use the VisibleIndex rather than index for applying the alternate color.
				// Added a mechanism for caching the visible indexes.
				//
				// SSP 3/29/05 - NAS 5.2 Filter Row
				// Filter row and other special rows should never considered alternate.
				//
				//return 0 != this.VisibleIndex % 2;
				int visibleIndex = this.ParentCollection.GetVisibleIndex( this, false );
				return visibleIndex >= 0 && 0 != visibleIndex % 2;

				
			}
		}

		 
		/// <summary>  
		/// Returns or sets a value that determines how an object will behave when it is  activated.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>Activation</b> property of the UltraGridCell object is subordinate to the settings of the Activation properties of the UltraGridRow and UltraGridColumn objects that contain the cell. If either the cell's row or column has its <b>Activation</b> property set to Disabled, the cell cannot be activated, regardless of its own setting for <b>Activation</b>. The setting of the other type of parent also has no effect; setting <b>Activation</b> to False on a cell's row makes the cell inactive regardless of the setting of its column.</p>
		/// <seealso cref="UltraGridOverride.CellClickAction"/>
		/// <seealso cref="UltraGridColumn.CellActivation"/>
		/// <seealso cref="UltraGridCell.Activation"/>
		/// </remarks>
		public Activation Activation
		{
			get
			{
				return this.activation;
			}
			set
			{
				if( this.activation != value )
				{
					if ( !Enum.IsDefined( typeof(Activation), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_32") );

					this.activation = value;

					// SSP 11/26/02 UWG1834
					// Cause the row to reposition its child elements (cell elements) which will
					// cause it to enable or disable cell according to the new value of Activation.
					//
					this.InvalidateItemAllRegions( true );

					this.NotifyPropChange( PropertyIds.Activation, null );
				}
			}
		}
		


		/// <summary>  
		/// Returns or sets the Appearance object that controls the object's formatting.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>Appearance</b> property of an object is used to associate the object with an Appearance object that will determine its appearance. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For many of the objects in the UltraWinGrid, you do not set formatting properties directly. Instead, you set the properties of an Appearance object, which controls the formatting of the object it is attached to.</p>
		/// <p class="body">There are two ways of working with the <b>Appearance</b> property and assigning the attributes of an Appearance object to other objects. One way is to create a new Appearance object, adding it directly to the Appearances collection. Then you assign the new Appearance object  to the <b>Appearance</b> property of the object you want to format. This method uses a "named" Appearance object that you must explicitly create (and to which you must assign property settings) before it can be used. For instance, you could create an object in the grid's Appearances collection and assign it some values as follows:</p>
		/// <p class="code">UltraWinGrid1.Appearances.Add "New1"</p>
		/// <p class="code">UltraWinGrid1.Appearances("New1").BorderColor = System.Drawing.Color.Blue</p>
		/// <p class="code">UltraWinGrid1.Appearances("New1").ForeColor = System.Drawing.Color.Red</p>
		/// <p class="body">Creating the object in this way does not apply formatting to any visible part of the grid. The object simply exists in the collection with its property values, waiting to be used. To actually use the object, you must assign it to the grid's (or another object's) <b>Appearance</b> property:</p>
		/// <p class="code">UltraWinGrid1.Appearance = UltraWinGrid1.Appearances("New1")</p>
		/// <p class="body">In this case, only one Appearance object exists. The grid's appearance is governed by the settings of the "New1" object in the collection. Any changes you make to the object in the collection will immediately be reflected in the grid.</p>
		/// <p class="body">The second way of working with the <b>Appearance</b> property is to use it to set property values directly, such as:</p>
		/// <p class="code">UltraWinGrid1.Appearance.ForeColor = System.Drawing.Color.Blue</p>
		/// <p class="body">In this case, an Appearance object is automatically created by the control. This Appearance object is not a member of an Appearances collection and it does not have a name. It is specific to the object for which it was created; it is an "intrinsic" Appearance object. Changes to the properties of an intrinsic Appearance object are reflected only in the object to which it is attached.</p>
		/// <p class="body">Note that you can assign properties from a named Appearance object to an intrinsic Appearance object without creating a dependency relationship. For example, the following code...</p>
		/// <p class="code">UltraWinGrid1.Appearance.ForeColor = UltraWinGrid1.Appearances("New1").ForeColor</p>
		/// <p class="body">...does <i>not</i> establish a relationship between the foreground color of the intrinsic object and that of the named object. It is simply a one-time assignment of the named object's value to that of the intrinsic object. In this case, two Appearance objects exist - one in the collection and one attached to the grid - and they operate independently of one another.</p>
		/// <p class="body">If you wish to assign all the properties of a named object to an intrinsic object at once without creating a dependency relationship, you can use the <b>Clone</b> method of the Appearance object to duplicate its settings and apply them. So if you wanted to apply all the property settings of the named Appearance object "New1" to the grid's intrinsic Appearance object, but you did not want changes made to "New1" automatically reflected in the grid, you would use the following code:</p>
		/// <p class="code">UltraWinGrid1.Appearance = UltraWinGrid1.Appearances("New1").Clone</p>
		/// <p class="body">Note that the properties of an Appearance object can also operate in a hierarchical fashion. Certain properties can be set to a "use default" value, which indicates to the control that the property should take its setting from the object's parent. This functionality is enabled by default, so that unless you specify otherwise, child objects resemble their parents, and formatting set at higher levels of the grid hierarchy is inherited by objects lower in the hierarchy.</p>
		/// </remarks>
		public Infragistics.Win.Appearance Appearance
		{
			get
			{
				if ( null == this.appearance )
				{
				    
					this.appearance = new Infragistics.Win.Appearance();
				
					// hook up the prop change notifications
					//
					this.appearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.appearance;
			}
			set
			{
				if( this.appearance != value )
				{
					UltraGridLayout layout = this.Layout;

					// SSP 12/3/01 UWG825
					// Unhook from the old appearance object
					//
					if ( null != this.appearance )
					{
						this.appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

						// SSP 10/21/04 UWG3665
						// This is to fix a performance problem where if the same appearance instance is
						// assigned to a lot of cells or rows then all these cells/rows hook into that
						// appearance instance. When it comes the time for the cells to unhook from this
						// appearance (like when the grid is being disposed or the datasource is being
						// reset) it takes a long time because the .NET multicast events use linked list to
						// store the delegates. This requires performing a linear search and if there are a
						// lot of cells/rows that are hooked into the same appearence then it can end up
						// taking a very long time. The solution as implemented by this fix is for the
						// layout to hook into such appearances that are shared by multiple cells/rows once
						// and then manage the cells/rows that refer to these appearances and redirect the
						// sub object prop change to them.
						//
						if ( null != layout )
							layout.RemoveMulticastAppearance( this.appearance, this );
					}

					this.appearance = value;

					// SSP 12/3/01 UWG825
					// hook up the prop change notifications
					//
					// SSP 10/21/04 UWG3665
					// This is to fix a performance problem where if the same appearance instance is
					// assigned to a lot of cells or rows then all these cells/rows hook into that
					// appearance instance. When it comes the time for the cells to unhook from this
					// appearance (like when the grid is being disposed or the datasource is being
					// reset) it takes a long time because the .NET multicast events use linked list to
					// store the delegates. This requires performing a linear search and if there are a
					// lot of cells/rows that are hooked into the same appearence then it can end up
					// taking a very long time. The solution as implemented by this fix is for the
					// layout to hook into such appearances that are shared by multiple cells/rows once
					// and then manage the cells/rows that refer to these appearances and redirect the
					// sub object prop change to them.
					//
					//if ( null != this.appearance )
					//	this.appearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					if ( null != this.appearance && null != layout )
						layout.SetMulticastAppearance( this.appearance, this );

					this.NotifyPropChange( PropertyIds.Appearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the appearance object has been allocated for the <see cref="UltraGridRow.Appearance"/> property.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridRow.Appearance"/>
		/// </remarks>
		public bool HasAppearance
		{
			get
			{
				return ( null != this.appearance );
			}
		}

		


		/// <summary>  
		/// Determines the formatting attributes that will be applied to the cells in a row.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>CellAppearance</b> property is used to specify the appearance of all the cells in a row. When you assign an Appearance object to the <b>CellAppearance</b> property, the properties of that object will be applied to all the cells belonging to the row. You can use the <b>CellAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the cells, for example:</p> 
		/// <p class="code">UltraWinGrid1.Override.CellAppearance.BackColor = vbYellow</p>
		/// <p class="body">You can override the <b>CellAppearance</b> setting for specific cells by setting the <b>Appearance</b> property of the UltraGridCell object directly. The cell will always use the values of its own Appearance object before it will use the values inherited from the Appearance object specified by the <b>CellAppearance</b> property of the row or band it occupies.</p>
		/// <p class="body">If any of the properties of the Appearance object specified for the <b>CellAppearance</b> property are set to default values, the properties from the Appearance object of the row containing the cell are used.</p>
		/// </remarks>
		public Infragistics.Win.Appearance CellAppearance
		{
			get
			{
				if ( null == this.cellAppearance )
				{
				    
					this.cellAppearance = new Infragistics.Win.Appearance();
				
					// hook up the prop change notifications
					//
					this.cellAppearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.cellAppearance;
			}
			set
			{
				if( this.cellAppearance != value )
				{
					UltraGridLayout layout = this.Layout;

					// SSP 12/3/01 UWG825
					// Unhook from the old appearance object
					//
					if ( null != this.cellAppearance )
					{
						this.cellAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

						// SSP 10/21/04 UWG3665
						// This is to fix a performance problem where if the same appearance instance is
						// assigned to a lot of cells or rows then all these cells/rows hook into that
						// appearance instance. When it comes the time for the cells to unhook from this
						// appearance (like when the grid is being disposed or the datasource is being
						// reset) it takes a long time because the .NET multicast events use linked list to
						// store the delegates. This requires performing a linear search and if there are a
						// lot of cells/rows that are hooked into the same appearence then it can end up
						// taking a very long time. The solution as implemented by this fix is for the
						// layout to hook into such appearances that are shared by multiple cells/rows once
						// and then manage the cells/rows that refer to these appearances and redirect the
						// sub object prop change to them.
						//
						if ( null != layout )
							layout.RemoveMulticastAppearance( this.cellAppearance, this );
					}

					this.cellAppearance = value;

					// SSP 12/3/01 UWG825
					// hook up the prop change notifications
					//
					// SSP 10/21/04 UWG3665
					// This is to fix a performance problem where if the same appearance instance is
					// assigned to a lot of cells or rows then all these cells/rows hook into that
					// appearance instance. When it comes the time for the cells to unhook from this
					// appearance (like when the grid is being disposed or the datasource is being
					// reset) it takes a long time because the .NET multicast events use linked list to
					// store the delegates. This requires performing a linear search and if there are a
					// lot of cells/rows that are hooked into the same appearence then it can end up
					// taking a very long time. The solution as implemented by this fix is for the
					// layout to hook into such appearances that are shared by multiple cells/rows once
					// and then manage the cells/rows that refer to these appearances and redirect the
					// sub object prop change to them.
					//
					//if ( null != this.cellAppearance )
					//	this.cellAppearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					if ( null != this.cellAppearance && null != layout )
						layout.SetMulticastAppearance( this.cellAppearance, this );

					this.NotifyPropChange( PropertyIds.CellAppearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the appearance object has been allocated for the <see cref="UltraGridRow.CellAppearance"/> property.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridRow.CellAppearance"/>
		/// </remarks>
		public bool HasCellAppearance
		{
			get
			{
				return ( null != this.cellAppearance );
			}
		}

		


		/// <summary>  
		/// Determines if the <B>Description</B> will be displayed in the AutoPreview area for this row. This property is not available at design-time.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The auto preview area of a row is a blank area that appears at the bottom of a row across the row's entire width. This area can be used to display the text of the row's description, as determined by the <b>Description</b> property of the UltraGridRow object.</p>
		/// <p class="body">The <b>AutoPreviewEnabled</b> property determines whether the AutoPreview area can be displayed for rows in the specified band. Once AutoPreview has been enabled, it can be displayed for any row by setting the UltraGridRow object's <b>AutoPreviewHidden</b> property to False.</p>
		/// </remarks>
		public bool AutoPreviewHidden
		{
			get
			{
				return this.autoPreviewHidden;
			}
			set
			{
				if( this.autoPreviewHidden != value )
				{
					this.autoPreviewHidden = value;
					this.NotifyPropChange( PropertyIds.AutoPreviewHidden, null );
				}
			}
		}

		

		

		/// <summary>  
		/// Returns a value that determines whether the data in a cell or row has been changed, but not committed to the data source. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns True when a cell or row's data has changed, but has not yet been committed to the data source; otherwise, it returns False.</p>
		/// <p class="body">When the value of a cell is changed, either programmatically by setting ;its <b>Value</b> property, or by user interaction, this property is set to True and the <b>BeforeCellUpdate</b> and <b>AfterCellUpdate</b> events are generated. Note that the cell's new value is not necessarily committed to the data source at this time, however, since various factors such as the type of record locking employed by the data source, as well as the value of the <b>UpdateMode</b> property, can affect when the actual update occurs. Once the data source is actually updated, the <b>BeforeRowUpdate</b> and <b>AfterRowUpdate</b> events are generated and this property is set back to False.</p>
		/// <p class="body">The <b>OriginalValue</b> property of the cell can be used to determine a cell's value before it was changed.</p>
		/// </remarks>
		public bool DataChanged
		{
			get
			{
				// SSP 6/30/03 UWG2271
				// Now we are not setting the DataChanged to true in ValueChanged of the editor. Instead
				// we will wait till the value is commited to the backend to set this property. Until 
				// then we will have editValueChanged flag set to true. Look at UWG2271 for more info on
				// why this was done.
				//
				// --------------------------------------------------------------------------------------
				UltraGridCell activeCell = null != this.Layout ? this.Layout.ActiveCell : null;
				if ( null != activeCell && this == activeCell.Row && activeCell.EditValueChanged )
					return true;
				// --------------------------------------------------------------------------------------

				return this.dataChanged;
			}
		}

		/// <summary>  
		/// Returns or sets the text that will be displayed as the card's caption.
		/// </summary>
		/// <remarks>
		/// <p class="body">When the band containing the Row is in Card View mode, the <b>CardCaption</b> property specifies the text that will be displayed in the caption area of the card that contains the row's data. This property will be read-only if the <b>CaptionField</b> property has been set to a valid field or column name in the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand.CardSettings"/> object of the band that contains the row.</p>
		/// <p class="body">Typically, you would set the value of the <b>CardCaption</b> property in the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeRow"/> event.</p>		
		/// </remarks>
		public virtual string CardCaption
		{
			get
			{
				if ( this.cardCaption != null &&
					this.cardCaption.Length > 0 )
					return this.cardCaption;

				string field = this.band.CardSettings.CaptionField;

				// With a blank card caption, use Caption field only if it itself is not blank
				//
				if ( field != null &&
					field.Length > 0 )
				{
					Infragistics.Win.UltraWinGrid.UltraGridColumn column = null;

					try
					{ 
						// SSP 8/24/01 UWG238
						// NOTE: This bug only happens when a new row is added and the autopreviw
						// is enabled with auto preview field set to a non-existing column name.
						//
						// If the field does not exit in the Columns collection, then 
						// exception gets thrown and caught multiple times every time grid 
						// is scrolled and throwing and catching of exceptions extremely slows
						// down the process. So I added check to see if the field exists before.
						// calling the indexer.
						//
						// NOTE: I am not sure if this thing should get called so many times
						// when scrolling ( and it only gets called after a row is added, and
						// thus UWG238 says that after adding columns it's slow ).
						//
						if ( this.band.Columns.Exists( field ) )
							column = this.band.Columns[field];
					}
					catch( Exception )
					{
						throw new ArgumentException( SR.GetString("LER_Exception_315", field) );
					}

					if ( column != null )
						return this.GetCellText( column );
				}

				return null;
			}
			set
			{
				if( this.cardCaption != value )
				{
					this.cardCaption = value;
					this.NotifyPropChange( PropertyIds.CardCaption );
				}
			}
		}

		internal int CardHeight
		{
			get
			{
				// Merged and standard label cards always have the same height
				// as all other cards in the band.
				//
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				switch ( this.band.CardSettings.StyleResolved )
				{
					case CardStyle.MergedLabels:
					case CardStyle.StandardLabels:
						return this.band.CardHeight;				
						// SSP 6/28/02
						// Compressed card view.
						// Added the case for Compressed card view style.
						//
					case CardStyle.Compressed:
						// SSP 6/28/02
						// Compressed card view.
						// If the card is not compressed, then use the CardHeight off the band
						// becuase when the card is expanded, it's the same calcualtions.
						// Added below if block while put the already existing code
						// in the else block.
						//
						if ( !this.IsCardCompressed )
						{
							return this.Band.CardHeight;
						}
						else
						{
							// If the card is compressed, then return the card caption height
							// and the borders.
							//
							return this.Band.CardHeightCompressed;
						}
				}

				

				// JJD 12/17/01
				// Use the cachedCardHeight so we don't run through this
				// every time. 
				//
				if ( this.cachedCardHeight < 1 ||
					this.drawSequence		!= this.Layout.DrawSequence )
				{
					int totalCellHeight = 0;

					// Add up the height of all the columns that will be displayed in the card
					//
					for ( int i = 0; i < this.band.OrderedHeaders.Count; i++ )
					{
						// MD 8/3/07 - 7.3 Performance
						// Refactored - Prevent calling expensive getters multiple times
						// Dont access the same array index multiple times in a loop iteration
						// Dont cast multiple times
						#region Refactored

						//if ( this.band.OrderedHeaders[i].Hidden )
						//    continue;
						//
						//if ( this.band.OrderedHeaders[i] is GroupHeader )
						//{
						//    UltraGridGroup group = ((GroupHeader )this.band.OrderedHeaders[i]).Group;
						//
						//    for ( int j = 0; j < group.Columns.Count; j++ )
						//    {
						//        if ( group.Columns[j].Hidden )
						//            continue;
						//
						//        if ( group.Columns[j].ShouldCellValueBeDisplayedInCard( this ) ) 
						//            totalCellHeight += group.Columns[j].CardCellHeight;
						//    }
						//}
						//else
						//    if ( this.band.OrderedHeaders[i] is ColumnHeader )
						//{
						//    if ( ((ColumnHeader)(this.band.OrderedHeaders[i])).Column.ShouldCellValueBeDisplayedInCard( this ) ) 
						//        totalCellHeight += ((ColumnHeader)this.band.OrderedHeaders[i]).Column.CardCellHeight;
						//}

						#endregion Refactored
						HeaderBase orderedHeader = this.band.OrderedHeaders[ i ];

						if ( orderedHeader.Hidden )
							continue;

						GroupHeader groupHeader = orderedHeader as GroupHeader;

						if ( groupHeader != null )
						{
							UltraGridGroup group = groupHeader.Group;

							// MD 1/21/09 - Groups in RowLayouts
							// Use the resolved column because in GroupLayout style, it will return a different enumarator which returns the columns 
							// actually visible in the group
							//for ( int j = 0; j < group.Columns.Count; j++ )
							//{
							//    UltraGridColumn groupColumn = group.Columns[ j ];
							foreach ( UltraGridColumn groupColumn in group.ColumnsResolved )
							{
								// MD 1/21/09 - Groups in RowLayouts
								// Use HiddenResolved instead of Hidden because it checks the group's hidden value.
								//if ( groupColumn.Hidden )
								if ( groupColumn.HiddenResolved )
									continue;

								if ( groupColumn.ShouldCellValueBeDisplayedInCard( this ) )
									totalCellHeight += groupColumn.CardCellHeight;
							}
						}
						else
						{
							ColumnHeader columnHeader = orderedHeader as ColumnHeader;

							if ( columnHeader != null )
							{
								UltraGridColumn columnHeaderColumn = columnHeader.Column;

								if ( columnHeaderColumn.ShouldCellValueBeDisplayedInCard( this ) )
									totalCellHeight += columnHeaderColumn.CardCellHeight;
							}
						}
					}

					//JM 01-16-02 now using BorderStyleRow for cards
					//int borderThickness	= this.Layout.GetBorderThickness( this.band.BorderStyleCardResolved );
					int borderThickness	= this.Layout.GetBorderThickness( this.band.BorderStyleRowResolved );

					this.cachedCardHeight = totalCellHeight 
						+ this.band.CardCaptionHeight
						+ ( 2 * borderThickness );
				}

				this.drawSequence = this.Layout.DrawSequence;

				return this.cachedCardHeight;
			}
		}
		

		/// <summary>  
		/// Returns or sets the text that will be displayed as a description.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Description</b> property determines the text that will be displayed in the AutoPreview area for a row. This property will be read-only if its band's <b>AutoPreviewField</b> property is set to a valid field or column name.</p>
		///	<p>
		/// The following sample code enables the row previews in the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeLayout"/> event and sets descriptions of rows in <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeRow"/> event.
		/// </p>
		/// <p></p>
		/// <p>C#:</p>
		/// <p></p>
		/// <code>
		/// private void ultraGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
		/// {
		/// 	// Enable the row previews.
		/// 	//
		/// 	this.ultraGrid1.DisplayLayout.Bands[0].AutoPreviewEnabled = true;
		/// }
		/// 
		/// private void ultraGrid1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
		/// {
		/// 	Infragistics.Win.UltraWinGrid.UltraGridBand band;
		/// 	band = this.ultraGrid1.DisplayLayout.Bands[0];
		/// 
		/// 	// Check for the row being from the same band.
		/// 	//
		/// 	if ( e.Row.Band == band )
		/// 	{
		/// 		// Create a custom description that we want shown in the row's
		/// 		// row preview area.
		/// 		//
		/// 		System.Text.StringBuilder description = new System.Text.StringBuilder( );
		/// 
		/// 		description.Append( e.Row.GetCellText( band.Columns["ContactName"] ) );
		/// 		description.Append( "\n" );
		/// 		description.Append( e.Row.GetCellText( band.Columns["City"] ) );
		/// 		description.Append( ", " );
		/// 		description.Append( e.Row.GetCellText( band.Columns["Country"] ) );
		/// 
		/// 		// Set the row's Description to our custom description text.
		/// 		//
		/// 		e.Row.Description = description.ToString( );
		/// 	}
		/// }
		/// </code>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewField"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewMaxLines"/>
		/// </remarks>
		public virtual string Description
		{
			get
			{
				if ( this.description != null &&
					this.description.Length > 0 )
					return this.description;

				string field = this.band.AutoPreviewField;

				// With a blank description, use AutoPreviewField only if it itself is not blank
				//
				if ( field != null &&
					field.Length > 0 )
				{
					Infragistics.Win.UltraWinGrid.UltraGridColumn column = null;

					try
					{ 
						// SSP 8/24/01 UWG238
						// NOTE: This bug only happens when a new row is added and the autopreviw
						// is enabled with auto preview field set to a non-existing column name.
						//
						// If the field does not exit in the Columns collection, then 
						// exception gets thrown and caught multiple times every time grid 
						// is scrolled and throwing and catching of exceptions extremely slows
						// down the process. So I added check to see if the field exists before.
						// calling the indexer.
						//
						// NOTE: I am not sure if this thing should get called so many times
						// when scrolling ( and it only gets called after a row is added, and
						// thus UWG238 says that after adding columns it's slow ).
						//
						if ( this.band.Columns.Exists( field ) )
							column = this.band.Columns[field];
					}
					catch( Exception )
					{
						throw new ArgumentException( SR.GetString("LER_Exception_315", field) );
					}

					if ( column != null )
						return this.GetCellText( column );
				}

				return null;
			}
			set
			{
				if( this.description != value )
				{
					this.description = value;

					// SSP 9/16/02 UWG1495
					// If the row is in view, then dirty the grid element as well
					// as the visible rows. Because if the description is set when
					// there was none or if it's cleared when there was a description,
					// then row's height would change and we would need to regenerate
					// the visible rows as a result.
					//
					// -------------------------------------------------------------
					if ( null != this.Layout )
					{
						UIElement mainElem = this.Layout.UIElement;
						if ( null != mainElem )
						{
							UIElement rowElem = mainElem.GetDescendant( typeof( RowUIElementBase ), this );

							if ( null != rowElem )
								this.Layout.DirtyGridElement( true );
						}
					}
					// -------------------------------------------------------------

					this.NotifyPropChange( PropertyIds.Description );
				}
			}
		}

		/// <summary>  
		/// Returns or sets whether the row is expanded. This property is not available at design-time.
		/// </summary>  
		/// <remarks>
		/// <p class="body">If set to False, the row will be collapsed but child row expand/collapse information will not be discarded. An error occurs if this property is set to True and the <b>Expandable</b> property of the <b>UltraGridBand</b> object is False.</p>
		/// <p class="body">
		/// To expand or collapse all the rows of a grid or a specific row collection
		/// use the RowsCollection's <see cref="RowsCollection.ExpandAll"/> and
		/// <see cref="RowsCollection.CollapseAll"/> methods.
		/// </p>
		/// <seealso cref="IsExpanded"/>
		/// <seealso cref="ExpandAll"/> 
		/// <seealso cref="CollapseAll"/>
		/// <seealso cref="RowsCollection.ExpandAll"/>
		/// <seealso cref="RowsCollection.CollapseAll"/>
		/// <seealso cref="ExpansionIndicator"/>
		/// <seealso cref="IsExpandable"/>
		/// </remarks>
		public bool Expanded
		{
			get
			{
				return this.expanded;
			}
			set
			{
				UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

				if ( null == grid )
					throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_230"));

				if( this.expanded != value )
				{
					CancelableRowEventArgs beforeArgs = new CancelableRowEventArgs( this );

					// fire either the BeforeRowExpanded or BeforeRowCollapsed
					// event
					//
					if ( value )
						grid.FireEvent(GridEventIds.BeforeRowExpanded, beforeArgs );
					else
						grid.FireEvent(GridEventIds.BeforeRowCollapsed, beforeArgs );

					// if the event was cancelled then exit
					//
					if ( beforeArgs.Cancel )
						return;

					// set the member variable
					//
					this.expanded = value;

					// JJD 8/7/01
					// keep the scrollableRowCountManager's expanded property in
					// sync
					//
					// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
					// Commented out the original code and added call to NotifyScrollCountChanged.
					//
					//if ( this.scrollableRowCountManager != null )
					//	this.scrollableRowCountManager.Expanded = value;
					this.NotifyScrollCountChanged( );

					// notify all listeners
					//
					this.NotifyPropChange( PropertyIds.Expanded, null );

					RowEventArgs afterArgs = new RowEventArgs( this );

					// fire either the BeforeRowExpanded or BeforeRowCollapsed
					// event
					//
					if ( value )
						grid.FireEvent(GridEventIds.AfterRowExpanded, afterArgs );
					else
						grid.FireEvent(GridEventIds.AfterRowCollapsed, afterArgs );
				}
			}
		}

		
		/// <summary>
		/// Returns true if the band's <b>Expandable</b> property is true and the band has child bands that aren't hidden.
		/// </summary>
		/// <remarks>
		/// <seealso cref="IsExpanded"/>
		/// <seealso cref="ExpandAll"/>
		/// <seealso cref="CollapseAll"/>
		/// <seealso cref="ExpansionIndicator"/>
		/// <seealso cref="IsExpandable"/>
		/// </remarks>
		public virtual bool IsExpandable
		{
			get
			{
				if ( !this.Band.Expandable ||
					// JJD 1/14/01
					// The row cannot expand if it is a card 
					//
					this.IsCard ||
					!this.Band.HasChildBands( true ) )
					return false;

				// JJD 1/14/02
				// If we are checking shild rows on expand return true if we
				// haven't checked yet.
				//
				if ( this.Band.ExpansionIndicatorResolved == ShowExpansionIndicator.CheckOnExpand )
				{
					if ( this.childBands == null )
						return true;

					for ( int i = 0; i < this.childBands.Count; i++ )
					{
						if ( !this.childBands[i].HaveRowsBeenInitialized &&
							!this.childBands[i].Band.HiddenResolved ) 
							return true;
					}
				}

				// SSP 3/18/02
				// For filtering, we also have to check if all the rows are
				// filtered out in which case hidden property of all the rows
				// will be true, but we still want the user to be able to
				// expand the parent row.
				// Removed HasChild call and added below code
				//
				// return this.HasChild( true );
				// -----------------------------------------------------------
				// SSP 11/11/03 Add Row Feature
				//
				//bool hasChildren = this.HasChild( true );
				bool hasChildren = this.HasChild( true, IncludeRowTypes.SpecialRows );

				if ( hasChildren )
					return true;


				// If filters are active, then return true because we do want to show
				// the band header any ways and allow the user to be able to expand the
				// parent row whose children are all filtered out.
				//
				return this.AreFiltersActiveOnChildRowsAndHasChildRows( );

				// -----------------------------------------------------------
			}
		}

		/// <summary>
		/// Returns true if the row should display an expansion indicator
		/// </summary>
		/// <remarks>
		/// <seealso cref="IsExpanded"/>
		/// <seealso cref="Expanded"/>
		/// <seealso cref="ExpansionIndicator"/>
		/// <seealso cref="IsExpandable"/>
		/// </remarks>
		public virtual bool HasExpansionIndicator
		{
			get
			{
				// SSP 11/11/03 Add Row Feature
				// Template add-rows should never show expansion indicators.
				//
				// --------------------------------------------------------------
				// SSP 4/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// Also check for filter row, summary row and fixed add-row on bottom.
				//
				//if ( this.IsTemplateAddRow || this.IsUnModifiedAddRowFromTemplate )
				if ( this.IsTemplateAddRow || this.IsUnModifiedAddRowFromTemplate 
					// Following line doesn't allow expansion of the add-row that's fixed at the
					// bottom. Doing so would mean the row would have to be unfixed and the unfixed
					// row may jump to some other position.
					//
					|| this.IsAddRowFromTemplate && this.FixedResolved
					|| this.IsFilterRow || this.IsSummaryRow )
					return false;
				// --------------------------------------------------------------

				// JJD 11/07/01
				// Rewrote this property get completely
				//
				ShowExpansionIndicator expIndicator;

				// use either this row's setting or the band's default
				//
				if ( this.expansionIndicator == ShowExpansionIndicator.Default )
					expIndicator = this.Band.ExpansionIndicatorResolved;
				else
					expIndicator = this.expansionIndicator;

				if ( expIndicator == ShowExpansionIndicator.Default )
				{
					// the default for multi-band view styles with child bands 
					// is to CheckOnExpand 
					if ( this.Layout.ViewStyleImpl.IsMultiBandDisplay )
						expIndicator = ShowExpansionIndicator.CheckOnExpand;
					else
						return false;
				}

				switch ( expIndicator )
				{
						// If this row's expansionIndicator is set to never then return false
						//
					case ShowExpansionIndicator.Never:
						return false;

						// If this row's expansionIndicator is set to always then return true
						//
					case ShowExpansionIndicator.Always:
						return true;

					case ShowExpansionIndicator.CheckOnExpand:
						
						if ( this.band.GetChildBandCount( true ) < 1 )
							return false;

						if ( !this.expanded )
							return true;

						break;
				}

				// SSP 3/18/02
				// For filtering, we also have to check if all the rows are
				// filtered out in which case hidden property of all the rows
				// will be true, but we still want the user to be able to
				// expand the parent row.
				// Took out call to HasChild and added code below
				//
				// return this.HasChild( true );
				// SSP 11/11/03 Add Row Feature
				//
				//bool hasChildren = this.HasChild( true );
				bool hasChildren = this.HasChild( true, IncludeRowTypes.SpecialRows );

				if ( hasChildren )
					return true;


				// If filters are active, then return true because we do want to show
				// the band header any ways and allow the user to be able to expand the
				// parent row whose children are all filtered out.
				//
				return this.AreFiltersActiveOnChildRowsAndHasChildRows( );
			}
		}

		/// <summary>
		/// Returns true if the row is expanded in the display.
		/// </summary>
		/// <remarks>
		/// <seealso cref="IsExpanded"/>
		/// <seealso cref="ExpandAll"/>
		/// <seealso cref="CollapseAll"/>
		/// <seealso cref="ExpansionIndicator"/>
		/// <seealso cref="IsExpandable"/>
		/// </remarks>
		public bool IsExpanded
		{
			get
			{
				// JAS 2005 v2 GroupBy Row Extensions
				// The current expansion state does not depend on whether the row can be expanded.
				//
				return this.expanded;
				//				return this.expanded && this.IsExpandable; 
			}
		}

		/// <summary>
		/// Returns true if the Row's expansion indicator indicates that the row is expanded (appears as a minus sign.) This property is read-only.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// To expand or collapse a row, use the row's <see cref="UltraGridRow.Expanded"/>
		/// property. To expand or collapse all the rows of a grid or a specific row collection
		/// use the RowsCollection's <see cref="RowsCollection.ExpandAll"/> and
		/// <see cref="RowsCollection.CollapseAll"/> methods.
		/// </p>
		/// <seealso cref="UltraGridRow.Expanded"/>
		/// <seealso cref="RowsCollection.ExpandAll"/>
		/// <seealso cref="RowsCollection.CollapseAll"/>
		/// </remarks>
		public bool ShowAsExpanded
		{
			get
			{
				// JJD 9/26/00 
				// If HasExpansionIndicator returns true then just return the stete's
				// m_bExpanded value
				//
				if ( this.HasExpansionIndicator )
					return this.expanded;

				// Otherwise, forward to IsExpanded
				//
				return this.IsExpanded;

			}
		}
		#region code commented out
		
		#endregion
		
		/// <summary>  
		/// Returns or sets a value that determines whether row expansion (plus/minus) indicators are displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property can be used to show expansion indicators for a row that has no children or hide them for a row that does.</p>
		/// <p class="body">The <b>Expanded</b> property can be used to indicate whether the expansion indicator appears expanded (minus) or collapsed (plus).</p>
		/// <p class="body">The <b>BeforeRowExpanded</b> and <b>BeforeRowCollapsed</b> events are generated when the user expands or collapses a row by clicking an expansion indicator.</p>
		/// <seealso cref="IsExpanded"/>
		/// <seealso cref="ExpandAll"/>
		/// <seealso cref="CollapseAll"/>
		/// <seealso cref="ExpansionIndicator"/>
		/// <seealso cref="IsExpandable"/>
		/// </remarks>
		public ShowExpansionIndicator ExpansionIndicator
		{
			get
			{
				return this.expansionIndicator;
			}
			set
			{
				if( this.expansionIndicator != value )
				{
					if ( !Enum.IsDefined( typeof(ShowExpansionIndicator), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_231") );

					this.expansionIndicator = value;
					this.NotifyPropChange( PropertyIds.ExpansionIndicator, null );
				}
			}
		}


		/// <summary>  
		/// Returns or sets a value that determines whether a row can be sized by the user. This property is not available at design-time.
		/// </summary>  
		/// <remarks>
		/// <p class="body">This property can be used to indicate a specific row should not be resized, regardless of whether the <b>RowSizing</b> property enables the user to resize rows.</p>
		/// <p class="body">If this property is set to 1 (FixedHeightTrue) for a particular row, the user may still indirectly resize that row if the <b>RowSizing</b> property is set to 3, since the row's size may be affected by the sizing of another row.</p>
		/// <p class="body">This property only affects whether the user can resize a row. A row can be sized programmatically, regardless of the value of this property, by setting its <b>Height</b> property.</p>
		/// </remarks>
		public DefaultableBoolean FixedHeight
		{
			get
			{
				return this.fixedHeight;
			}
			set
			{
				if( this.fixedHeight != value )
				{
					if ( !Enum.IsDefined( typeof(DefaultableBoolean), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_232") );

					this.fixedHeight = value;
					this.NotifyPropChange( PropertyIds.FixedHeight, null );
				}
			}
		}
		
		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// Made virtual.
		// 
		//internal bool IsFixedHeight
		internal virtual bool IsFixedHeight
		{
			get
			{
				// As per DCR changed FixedHeight from a boolean to an enumerator
				//
				// At design time all row's are fixed 
				//
				if ( this.Layout.Grid.DesignMode )
					return true;

				bool fixedHeight = true;
    
				switch ( this.fixedHeight )
				{
					case Infragistics.Win.DefaultableBoolean.Default:
						// Get the band's default
						//
						fixedHeight = this.Band.IsRowSizingFixed;
						break;

					case Infragistics.Win.DefaultableBoolean.True:
						break;
    
					case Infragistics.Win.DefaultableBoolean.False:
						fixedHeight = false;
						break;

					default:
						Debug.Fail("Bad FixedHeight enum value in Row.IsFixedHeight");
						break;
				}

				return fixedHeight;
			}
		}		


		/// <summary>  
		/// Determines how the row selectors will look.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>RowSelectorAppearance</b> property is used to specify the appearance of the row selectors. When you assign an Appearance object to the <b>RowSelectorAppearance</b> property, the properties of that object will be applied to the row selectors of all all rows in the band or the rid, depending on where the UltraGridOverride object is being used. You can use the <b>RowSelectorAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the active cell, for example:</p> 
		/// <p class="code">UltraWinGrid1.Override.RowSelectorAppearance.ForeColor = vbBlack</p>
		/// <p class="body">Because you may want the row selectors to look different at different levels of a hierarchical record set, <b>RowSelectorAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different row selector appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the row selectors will use the grid-level setting of <b>RowSelectorAppearance</b>.</p>
		/// <p class="body">To display row selectors set the <seealso cref="UltraGridOverride.RowSelectors"/> property.</p>
		/// <seealso cref="UltraGridOverride.RowSelectors"/>
		/// <seealso cref="UltraGridOverride.RowSelectorWidth"/>
		/// </remarks>
		public Infragistics.Win.Appearance RowSelectorAppearance
		{
			get
			{
				if ( null == this.rowSelectorAppearance )
				{
				    
					this.rowSelectorAppearance = new Infragistics.Win.Appearance();
				
					// hook up the prop change notifications
					//
					this.rowSelectorAppearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.rowSelectorAppearance;
			}
			set
			{
				if( this.rowSelectorAppearance != value )
				{
					// SSP 10/21/04 UWG3665
					// Before we weren't hooking into the set row selector appearence at all.
					// Added the following if block.
					//
					if ( null != this.rowSelectorAppearance )
					{
						this.rowSelectorAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

						if ( null != this.Layout )
							this.Layout.RemoveMulticastAppearance( this.rowSelectorAppearance, this );
					}

					this.rowSelectorAppearance = value;

					// SSP 10/21/04 UWG3665
					// Before we weren't hooking into the set row selector appearence at all.
					// Added the following if block.
					//
					if ( null != this.rowSelectorAppearance && null != this.Layout )
						this.Layout.SetMulticastAppearance( this.rowSelectorAppearance, this );

					this.NotifyPropChange( PropertyIds.RowSelectorAppearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the appearance object has been allocated for the <see cref="UltraGridRow.RowSelectorAppearance"/> property.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridRow.RowSelectorAppearance"/>
		/// </remarks>
		public bool HasRowSelectorAppearance
		{
			get
			{
				return ( null != this.rowSelectorAppearance );
			}
		}

		#region code commented out
		
		#endregion
		/// <summary>  
		/// Returns or sets the Appearance object that controls the formatting of row's AutoPreview area. 
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>PreviewAppearance</b> property provides access to the Appearance object being used to control the preview area of the UltraGridRow object. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For more information on how to use properties that end in "Appearance", consult the topic for the <b>Appearance</b> property.</p>
		/// <p class="body">You can also use the <b>RowPreviewAppearance</b> property of the UltraGridOverride object to control the settings of the row preview area. To determine the settings for a given row, use the <b>ResolvePreviewAppearance</b> method.</p>
		/// <seealso cref="UltraGridBand.AutoPreviewEnabled"/>
		/// <seealso cref="UltraGridRow.AutoPreviewHidden"/>
		/// <seealso cref="UltraGridOverride.RowPreviewAppearance"/>
		/// </remarks>
		public Infragistics.Win.Appearance PreviewAppearance
		{
			get
			{
				if ( null == this.previewAppearance )
				{
				    
					this.previewAppearance = new Infragistics.Win.Appearance();
				
					// hook up the prop change notifications
					//
					this.previewAppearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.previewAppearance;
			}
			set
			{
				if( this.previewAppearance != value )
				{
					// SSP 10/21/04 UWG3665
					// Before we weren't hooking into the set preview appearence at all.
					// Added the following if block.
					//
					if ( null != this.previewAppearance )
					{
						this.previewAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

						if ( null != this.Layout )
							this.Layout.RemoveMulticastAppearance( this.previewAppearance, this );
					}

					this.previewAppearance = value;

					// SSP 10/21/04 UWG3665
					// Before we weren't hooking into the set preview appearence at all.
					// Added the following if block.
					//
					if ( null != this.previewAppearance && null != this.Layout )
						this.Layout.SetMulticastAppearance( this.previewAppearance, this );

					this.NotifyPropChange( PropertyIds.RowPreviewAppearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the appearance object has been allocated for the <see cref="UltraGridRow.PreviewAppearance"/> property.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridRow.PreviewAppearance"/>
		/// </remarks>
		public bool HasPreviewAppearance
		{
			get
			{
				return ( null != this.previewAppearance );
			}
		}

		#region code commented out
		
		#endregion

		/// <summary>
		/// Indicates whether this is the active row.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>Activated</b> property returns true if this row is the active row. You
		/// can get or set the active row using the UltraGrid's 
		/// <see cref="UltraGridBase.ActiveRow"/> property.
		/// </p>
		/// <seealso cref="UltraGridBase.ActiveRow"/>
		/// <seealso cref="UltraGrid.ActiveCell"/>
		/// <seealso cref="UltraGridRow.Activate"/>
		/// </remarks>
		public override bool Activated
		{ 
			get 
			{ 
				return this == this.band.Layout.ActiveRow; 
			}
			
			set
			{
				// SSP 1/27/05 BR02053
				// Only do this for the display layout. Print and export layouts should not be
				// modifying the ActiveRow of the grid.
				//
				if( ! this.Layout.IsDisplayLayout )
					throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_37"));

				if ( this.IsDisabled )
					throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_233"));
				
				if ( value )
					this.band.Layout.Grid.ActiveRow = this;
				else
					if ( this.Activated )
					this.band.Layout.Grid.ActiveRow = null;
			} 
		}

		#region code commented out
		// AS 10/24/01 Moved to GridItemBase
		//		/// <summary>
		//		/// Indicates whether this row can be selected (not disabled)
		//		/// </summary>
		//		protected override bool Selectable
		//		{ 
		//			get
		//			{
		//				return !this.IsDisabled; 
		//			}
		//		}
		#endregion

		internal override void InternalSelect( bool value )
		{
			if( this.selectedValue != value )
			{
				this.selectedValue = value;

				// SSP 12/6/04 - Merged Cell Feature
				//
				RowsCollection.MergedCell_RowStateChanged( this, MergedCell.State.Selection );
		
				this.NotifyPropChange( PropertyIds.Selected, null );
			}
		}

		/// <summary>  
		/// Property: gets/sets whether the row is selected.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The UltraGrid maintains selected rows, cells and columns in 
		/// <see cref="Infragistics.Win.UltraWinGrid.Selected"/> object. The 
		/// <b>Selected</b> object exposes <see cref="Infragistics.Win.UltraWinGrid.Selected.Rows"/>,
		/// <see cref="Infragistics.Win.UltraWinGrid.Selected.Cells"/> and
		/// <see cref="Infragistics.Win.UltraWinGrid.Selected.Columns"/> properties that
		/// return selected rows, cells and columns respectively. When you set the 
		/// <see cref="UltraGridRow.Selected"/> property of a row, that row gets added
		/// to the selected rows collection of the Selected object. You can also select
		/// a row by calling the <see cref="SelectedRowsCollection.Add"/> method of the
		/// selected rows collection.
		/// </p>
		/// <p class="body">
		/// <b>NOTE:</b> If you want to select a lot of rows at once then use the
		/// <see cref="SelectedRowsCollection.AddRange(UltraGridRow[])"/> method of the 
		/// <see cref="SelectedRowsCollection"/> for greater efficiency.
		/// </p>
		/// <seealso cref="UltraGrid.Selected"/>
        /// <seealso cref="SelectedRowsCollection.AddRange(UltraGridRow[])"/>
		/// </remarks>
		public override bool Selected
		{
			get
			{
				return this.selectedValue;
			}
			set
			{
				// RobA UWG662 11/20/01 
				// removed this code and put in below code that calls InternalSelectItem
				//
				

				if( this.selectedValue != value )
				{
					UltraGrid grid = this.Layout.Grid as UltraGrid;

					if( null != grid )
					{
						// SSP 4/10/02 UWG1072
						// Don't clear the existing selection when selected
						// property is set. Look at the selection strategy to
						// determine whether to clear existing selected
						// items.
						//
						//grid.InternalSelectItem( this, true, value );
						
						bool clearExistingItems = true;

						SelectionStrategyBase selectionStrategy = 
							((ISelectionStrategyProvider)this.Band).SelectionStrategyRow;

						if ( null != selectionStrategy )
							clearExistingItems = selectionStrategy.IsSingleSelect;

						// Also clear the selected cells as rows and cells are mutually 
						// exclusive for selection. If the user cancels the selection
						// then return without selecting the row.
						//
						if ( grid.ClearSelectedCells( ) )
							return;

						grid.InternalSelectItem( this, clearExistingItems, value );
					}
					
						// if this is not an UltraGrid just set the selected property
						//
					else
					{
						// SSP 1/8/04 UWG2801
						// If the user is setting the Selected property of a row in the 
						// ultra drop down directly, then make sure we also update the 
						// SelectedRow property of the UltraDropDown.
						// 
						// ------------------------------------------------------------------------
						UltraDropDownBase dropDown = this.Layout.Grid as UltraDropDownBase;
						// MRS 10/28/04 - UWG2996
						//if ( null != dropDown && ! dropDown.inSelectedRow && dropDown.SelectedRow != this )
						if ( null != dropDown && dropDown.SelectedRow != this )
						{
							dropDown.SelectedRow = this;
							return;
						}
						// ------------------------------------------------------------------------

						this.selectedValue = value;
						this.NotifyPropChange( PropertyIds.Selected, null );
					}
				}
			}
		}

		#region Selectable

		// SSP 4/20/05 - NAS 5.2 Filter Row
		// Overrode Selectable. Moved the code from CanSelectRow into here and commented 
		// out CanSelectRow.
		//
		/// <summary>
		/// Property: Returns true only if selectable
		/// </summary>
		internal protected override bool Selectable
		{ 
			get 
			{ 
				// SSP 4/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// Use the new IsDataRow which checks for IsTemplateAddRow as well as other special
				// rows like the filter row.
				//
				//if ( this.IsTemplateAddRow )
				if ( ! this.IsDataRow && ! this.IsGroupByRow )
					return false;

				// SSP 1/6/04 - Accessiblity
				// If the selection strategy is None then retrun false.
				//
				// --------------------------------------------------------------------
				ISelectionStrategy strategy = this.SelectionStrategyDefault;
				if ( null == strategy || strategy is SelectionStrategyNone )
					return false;

				// Also check if the Activation is set to Disabled.
				//
				if ( this.IsDisabled )
					return false;
				// --------------------------------------------------------------------

				return base.Selectable;
			} 
		}

		#endregion // Selectable

		#region code commented out

		// SSP 4/20/05 - NAS 5.2 Filter Row
		// Moved the logic into Selectable property. Selectable is defined as virtual in
		// GridItemBase. Commented out CanSelectRow.
		//
		

		
		#endregion
		
		/// <summary>
		/// Collapses every row in the band and discards all of the expand/collapse information for the child rows.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>CollapseAll</b> method collapses the child rows of a band and discards any information about which children were themselves expanded.</p> 
		/// <p class="body">When you invoke the <b>CollapseAll</b> method, the control fires the <b>BeforeRowCollapsed</b> event for every row in the band. In that event, you have the opportunity to cancel the collapse of any row. For all rows except those for which the event was cancelled, the control then collapses the row and any of its children. If those children have children, they are also collapsed, and so on down to the bottom level of the hierarchy. Any context information that was previously accumulated as the result of the user expanding and collapsing child rows is discarded.</p>
		/// <seealso cref="ExpandAll"/> 
		/// <seealso cref="RowsCollection.ExpandAll"/>
		/// <seealso cref="RowsCollection.CollapseAll"/>
		/// <seealso cref="IsExpanded"/>
		/// <seealso cref="ExpansionIndicator"/>
		/// <seealso cref="IsExpandable"/>
		/// </remarks>
		public void CollapseAll()
		{
			// SSP 10/23/03 UWG2334
			// Expanding or collapsing a row could lead to the cell in edit mode being scrolled 
			// out of view which we don't want happen. So exit the edit mode and if that fails 
			// then return without expanding any rows.
			//
			if ( null != this.Band && null != this.Band.Layout && ! this.Band.Layout.ExitEditModeHelper( ) )
				return;

			// JJD 8/21/01
			// Implemented CollapseAll functionality
			//

			// first collapse the child rows
			//
			if ( this.HasChildRows )
			{
				foreach ( UltraGridChildBand childBand in this.ChildBands )
				{
					foreach ( Infragistics.Win.UltraWinGrid.UltraGridRow childRow in childBand.Rows )
					{
						childRow.CollapseAll();
					}
				}
			}

			// then collapse this row
			//
			this.Expanded = false;

		}

		/// <summary>
		/// Expands all rows (and bands, if applicable) in the object. Ignores the existing expanded/collapsed state of any rows or bands.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>ExpandAll</b> method expands all the child rows of a band. If those rows have any children, they are also expanded, and so on until the bottom level of the hierarchy is reached.</p> 
		/// <p class="body">When you invoke the <b>ExpandAll</b> method, the control fires the <b>BeforeRowExpanded</b> event for every row in the band. In that event, you have the opportunity to cancel the expansion of any row. For all rows except those for which the event was cancelled, the control then expands the row and any of its children. If those children have children, they are also expanded, and so on down to the bottom level of the hierarchy.  Any context information that was previously accumulated as the result of the user expanding and collapsing child rows is discarded.</p>
		/// <p class="body">The <see cref="Expanded"/> property can be used to expand or collapse a row without expanding its descendants.</p>
		/// <p class="body">The <see cref="RowsCollection.ExpandAll"/> method which takes in a <b>recursive</b> paramater can be used to expand only the immediate children of the rows collecition or all the descendants depending on the value of the recursive paramter.</p>
		/// <p class="body">The <see cref="CollapseAll"/> method can be invoked to collapse all descendent rows.</p>
		/// <seealso cref="CollapseAll"/>
		/// <seealso cref="RowsCollection.ExpandAll"/>
		/// <seealso cref="RowsCollection.CollapseAll"/>
		/// <seealso cref="ExpansionIndicator"/>
		/// <seealso cref="IsExpandable"/>
		/// <seealso cref="IsExpanded"/>
		/// </remarks>
		public void ExpandAll()
		{
			// SSP 10/23/03 UWG2334
			// Expanding or collapsing a row could lead to the cell in edit mode being scrolled 
			// out of view which we don't want happen. So exit the edit mode and if that fails 
			// then return without expanding any rows.
			//
			if ( null != this.Band && null != this.Band.Layout && ! this.Band.Layout.ExitEditModeHelper( ) )
				return;

			// JJD 8/21/01
			// Implemented Expandall functionality
			//

			// first expand this row
			//
			this.Expanded = true;

			// then expand all of its children
			//
			if ( this.HasChildRows )
			{
				foreach ( UltraGridChildBand childBand in this.ChildBands )
				{
					foreach ( Infragistics.Win.UltraWinGrid.UltraGridRow childRow in childBand.Rows )
					{
						childRow.ExpandAll();
					}
				}
			}
		}

		/// <summary>
		/// Returns a reference to the first or last child row of a row.
		/// </summary>
		/// <param name="child">Indicates which child row to retrieve.</param>
        /// <returns>A reference to the first or last child row of a row.</returns>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to either the first or last child row of a parent row. If a child row does not exist, this method returns Nothing.</p>
		/// <p class="body">The <i>band</i> argument can be used to specify a child rowset in which a child row should be sought. If <i>band</i> is not specified, this method attempts to find a child row in all child rowsets.</p>
		/// <p class="body">The <b>GetChild</b> method can be invoked to determine whether a row has a child row. </p>
		/// <p class="body">The <b>HasParent</b> property and <b>GetSibling</b> method can be used to return references to a parent row and a sibling row, respectively.</p>
		/// <seealso cref="UltraGridRow.ChildBands"/>
		/// <seealso cref="UltraGridChildBand.Rows"/>
		/// </remarks>
		public UltraGridRow GetChild( ChildRow child )
		{
			return this.GetChild( child, null );			
		}

		/// <summary>
		/// Returns a reference to the first or last child row of a row.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to either the first or last child row of a parent row. If a child row does not exist, this method returns Nothing.</p>
		/// <p class="body">The <i>band</i> argument can be used to specify a child rowset in which a child row should be sought. If <i>band</i> is not specified, this method attempts to find a child row in all child rowsets.</p>
		/// <p class="body">The <b>HasChild</b> method can be invoked to determine whether a row has a child row. </p>
		/// <p class="body">The <b>HasParent</b> and <b>GetSibling</b> methods can be invoked to return references to a parent row and a sibling row, respectively.</p>
		/// </remarks>
        /// <param name="child">Indicates which child row to retrieve.</param>        
		/// <param name="band">The band in which the child row exists.</param>
        /// <returns>A reference to the first or last child row of a row.</returns>        
		public Infragistics.Win.UltraWinGrid.UltraGridRow GetChild( ChildRow child, UltraGridBand band )
		{
			return this.GetChild( child, band, IncludeRowTypes.DataRowsOnly );
		}

		// SSP 4/30/05 - NAS 5.2 Fixed Rows_
		// 
		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetChild( ChildRow child, IncludeRowTypes includeRowTypes )
		{
			return this.GetChild( child, null, includeRowTypes );
		}

		// SSP 11/11/03 Add Row Feature
		// Added IncudeRowTypes parameter. Internally we need to take into account the template add-rows however
		// when the user calls this method, it should not return a template add-row.
		//
		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetChild( ChildRow child, UltraGridBand band, IncludeRowTypes includeRowTypes )
		{
			if ( !this.HasChildRowsInternal( IncludeRowTypes.SpecialRows ) )
				return null;

			Infragistics.Win.UltraWinGrid.UltraGridRow row = null;
			UltraGridChildBand childBand;

			switch ( child )
			{
				case ChildRow.First:
					// JJD 8/2/01 - UWG18 and 19
					// Test should be == instead of !=
					//if ( null != band )
					if ( null == band )
					{
						// SSP 11/11/03 Add Row Feature
						//
						//row = this.ChildBands.FirstRow;
						row = this.ChildBands.GetFirstRow( false, includeRowTypes );
						break;
					}
                    
					childBand = this.ChildBands[ band ];

					// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Replaced TemplateAddRow enum member with SpecialRows.
					// Commented out the original code and added the new code below.
					// 
					// --------------------------------------------------------------------------------------
					if ( null != childBand )
						row = childBand.Rows.GetFirstRow( includeRowTypes );
					
					// --------------------------------------------------------------------------------------

					break;

				case ChildRow.Last:
					// JJD 8/2/01 - UWG18 and 19
					// Test should be == instead of !=
					//if ( null != band )
					if ( null == band )
					{
						// SSP 11/11/03 Add Row Feature
						//
						//row = this.ChildBands.LastRow;
						row = this.ChildBands.GetLastRow( false, includeRowTypes );
						break;
					}
                    
					childBand = this.ChildBands[ band ];

					// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Replaced TemplateAddRow enum member with SpecialRows.
					// Commented out the original code and added the new code below.
					// 
					// --------------------------------------------------------------------------------------
					if ( null != childBand )
						row = childBand.Rows.GetLastRow( includeRowTypes );
					
					// --------------------------------------------------------------------------------------

					break;
			}
                
			return row;			
		}

		// SSP 11/10/03 - Add Row Feature
		// Why not just use the ParentRow property instead of GetParent. Only one place GetParent was
		// used so took it out in an effort to follow .NET conventions.
		//
		
		

		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a child row.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has at least one child row. If a child row exists, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>band</i> argument can be used to specify a child band in which a child row should be sought. If <i>band</i> is not specified, this method attempts to find a child row in all child bands.</p>
		/// <p class="body">A reference to the first or last child row can be returned by invoking the <b>GetChild</b> method.</p>
		/// <p class="body">The <b>HasParent</b>, <b>HasNextSibling</b>, and <b>HasPrevSibling</b> methods can be invoked to determine if a row has a parent row, sibling row above it, and sibling row below it, respectively.</p>
		/// </remarks>
        /// <returns>true if a row has a child row.</returns>
		public bool HasChild()
		{
			// JJD 10/03/01 - UWG476
			// Implemented
			//
			return this.HasChild( null );			
		}

		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a child row.
		/// </summary>
        /// <param name="band">If null will check all bands.</param>
		/// <remarks>        
		/// <p class="body">Invoke this method to determine whether a row has at least one child row. If a child row exists, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>band</i> argument can be used to specify a child band in which a child row should be sought. If <i>band</i> is not specified, this method attempts to find a child row in all child bands.</p>
		/// <p class="body">A reference to the first or last child row can be returned by invoking the <b>GetChild</b> method.</p>
		/// <p class="body">The <b>HasParent</b>, <b>HasNextSibling</b>, and <b>HasPrevSibling</b> methods can be invoked to determine if a row has a parent row, sibling row above it, and sibling row below it, respectively.</p>
		/// </remarks>
        /// <returns>true if a row has a child row.</returns>
		public bool HasChild( UltraGridBand band )
		{
			// JJD 10/03/01 - UWG476
			// Implemented
			//
			return this.HasChild( band, false );			
		}

		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a child row.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has at least one child row. If a child row exists, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>band</i> argument can be used to specify a child band in which a child row should be sought. If <i>band</i> is not specified, this method attempts to find a child row in all child bands.</p>
		/// <p class="body">A reference to the first or last child row can be returned by invoking the <b>GetChild</b> method.</p>
		/// <p class="body">The <b>HasParent</b>, <b>HasNextSibling</b>, and <b>HasPrevSibling</b> methods can be invoked to determine if a row has a parent row, sibling row above it, and sibling row below it, respectively.</p>
		/// </remarks>
		/// <param name="excludeHidden">If true only look for visible rows.</param>
        /// <returns>true if a row has a child row.</returns>
		public bool HasChild( bool excludeHidden )
		{
			return this.HasChild( null, excludeHidden );
		}

		// SSP 11/11/03 Add Row Feature
		//
		internal bool HasChild( bool excludeHidden, IncludeRowTypes includeRowTypes )
		{
			return this.HasChild( null, excludeHidden, includeRowTypes );
		}

		// SSP 11/11/03 Add Row Feature
		//
		internal bool HasChild( UltraGridBand band, IncludeRowTypes includeRowTypes )
		{
			return this.HasChild( band, false, includeRowTypes );
		}

		// JJD 10/03/01 - UWG476
		// Added band parameter
		//

		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a child row.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has at least one child row. If a child row exists, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>band</i> argument can be used to specify a child band in which a child row should be sought. If <i>band</i> is not specified, this method attempts to find a child row in all child bands.</p>
		/// <p class="body">A reference to the first or last child row can be returned by invoking the <b>GetChild</b> method.</p>
		/// <p class="body">The <b>HasParent</b>, <b>HasNextSibling</b>, and <b>HasPrevSibling</b> methods can be invoked to determine if a row has a parent row, sibling row above it, and sibling row below it, respectively.</p>
		/// </remarks>
		/// <param name="band">If null will check all bands.</param>
		/// <param name="excludeHidden">If true only look for visible rows.</param>
        /// <returns>true if a row has a child row.</returns>
		public bool HasChild( UltraGridBand band, bool excludeHidden )
		{
			return this.HasChild( band, excludeHidden, IncludeRowTypes.DataRowsOnly );
		}

		// SSP 11/11/03 Add Row Feature
		// Added includeRowTypes parameter.
		//
		//internal bool HasChild( UltraGridBand band, bool excludeHidden )
		internal bool HasChild( UltraGridBand band, bool excludeHidden, IncludeRowTypes includeRowTypes )
		{
			// SSP 8/24/01  UWG230

			if ( this.ChildBands == null )
				return false;

			for ( int i = 0; i < this.ChildBands.Count; i++ )
			{
				UltraGridChildBand childBand = this.ChildBands[i];

				// JJD 10/03/01 - UWG476
				// If a band was passed in and it doesn't match
				// this child band then continue.
				//
				if ( band != null		&&
					band != childBand.Band )
					continue;

				// SSP 9/11/03 UWG2062
				// If excludeHidden is true then skip hidden child bands.
				//
				// --------------------------------------------------------------------------
				if ( excludeHidden && childBand.Band.HiddenResolved )
					continue;
				// --------------------------------------------------------------------------

				// SSP 4/19/04 - Virtual Binding Related Optimizations
				//
				// ------------------------------------------------------------------------------
								
				if ( ! excludeHidden && childBand.Rows.Count > 0 )
					return true;

				// SSP 4/25/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
				//
				//if ( childBand.Rows.VisibleRowCount > 0 )
				if ( childBand.Rows.GetVisibleRowCount( includeRowTypes ) > 0 )
					return true;				
				// ------------------------------------------------------------------------------

				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// This is not necessary since the VisibleRowCount above includes template add-rows.
				//
				
			}

			return false;			
		}


		// SSP 9/12/03 UWG2648
		// Added HasParent overload that doesn't take any paramters.
		//
		/// <summary>
		/// Returns true if the row has a parent row. If the ParentRow is null then returns false.
		/// </summary>
        /// <returns>true if the row has a parent row. If the ParentRow is null then returns false.</returns>
		public bool HasParent( )
		{
			return this.HasParent( null );
		}

		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a parent row.
		/// </summary>
        /// <param name="band">If null will check all bands.</param>
		/// <remarks>        
		/// <p class="body">Invoke this method to determine whether a row has a parent row. If a parent row exists, this method returns True; otherwise, this method returns False. If you specify an UltraGridBand for the <i>band</i> parameter, the control will determine whether the row has an ancestor row in that band.</p>
		/// <p class="body">If a parent row exists, a reference to it can be returned by invoking the <b>HasParent</b> method.</p>
		/// <p class="body">The <b>HasChild</b>, <b>HasNextSibling</b>, and <b>HasPrevSibling</b>  methods can be invoked to determine whether a row has a child row, sibling row above it, and sibling row below it, respectively.</p>
		/// </remarks>
        /// <returns>true if the row has a parent row. If the ParentRow is null then returns false.</returns>
		public bool HasParent(UltraGridBand band)
		{
			//RobA 11/5/01 implemented
			
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.ParentRow != null )
			UltraGridRow parentRow = this.ParentRow;

			if ( parentRow != null )
			{
				if ( band != null )
				{
					// If a band was specified return true if it matches our band
					//
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//bool bandMatches = ( band == this.ParentRow.Band );
					bool bandMatches = ( band == parentRow.Band );

					if ( bandMatches )
					{
						return true;
					}
					
					else
					{
						// Otherwise, walk up the parent chain looking for a band match
						//
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//return this.ParentRow.HasParent( band );						
						return parentRow.HasParent( band );						
					}

				}

				else
				{
					// If a band wasn't specified then return true since we do have
					// a parent row
					//
					return true;
				}
			}
			
			return false;		
		}

		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a sibling row below it.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has a sibling row below it. If a sibling row exists below the row, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">A reference to sibling row can be returned by invoking the <b>GetSibling</b> method.</p>
		/// <p class="body">The <b>HasChild</b>, <b>HasParent</b>, and <b>HasPrevSibling</b>  methods can be invoked to determine whether a row has a child row, a parent row, and sibling row below it, respectively.</p>
		/// </remarks>
		/// <returns></returns>
		public bool HasNextSibling()
		{
			return this.HasNextSibling(false);
		}
		
		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a sibling row below it.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has a sibling row below it. If a sibling row exists below the row, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">A reference to sibling row can be returned by invoking the <b>GetSibling</b> method.</p>
		/// <p class="body">The <b>HasChild</b>, <b>HasParent</b>, and <b>HasPrevSibling</b>  methods can be invoked to determine whether a row has a child row, a parent row, and sibling row below it, respectively.</p>
		/// </remarks>
		/// <param name="spanBands">If true will look for rows in following sibling bands.</param>
		/// <returns></returns>
		public bool HasNextSibling(bool spanBands)
		{
			return this.HasNextSibling( spanBands, true );
		}

		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a sibling row below it.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has a sibling row below it. If a sibling row exists below the row, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">A reference to sibling row can be returned by invoking the <b>GetSibling</b> method.</p>
		/// <p class="body">The <b>HasChild</b>, <b>HasParent</b>, and <b>HasPrevSibling</b>  methods can be invoked to determine whether a row has a child row, a parent row, and sibling row below it, respectively.</p>
		/// </remarks>
		/// <param name="spanBands">If true will look for rows in following sibling bands.</param>
		/// <param name="excludeHidden">If true will not consider rows that are hidden.</param>
		/// <returns></returns>
		public bool HasNextSibling( bool spanBands, bool excludeHidden )
		{
			return this.HasNextSibling( spanBands, excludeHidden, IncludeRowTypes.DataRowsOnly );
		}

		// SSP 11/11/03 Add Row Feature
		// Added includeRowTypes parameter.
		//
		internal bool HasNextSibling(bool spanBands, IncludeRowTypes includeRowTypes )
		{
			return this.HasNextSibling( spanBands, true, includeRowTypes );
		}

		// SSP 11/11/03 Add Row Feature
		// Added includeRowTypes parameter.
		//
		internal bool HasNextSibling( bool spanBands, bool excludeHidden, IncludeRowTypes includeRowTypes )
		{
			// SSP 4/19/04 - Virtual Binding Related Optimizations
			// If a lot of rows are filtered out after this row then doing a linear search
			// will take a long time. So instead make use of the visible row indeces to
			// get the next visible sibling.
			//
			// ------------------------------------------------------------------------------
			if ( excludeHidden )
				return null != this.GetVisibleSiblingRow( true, spanBands, includeRowTypes );
			// ------------------------------------------------------------------------------

			UltraGridRow row = this;
			while ( true ) 
			{
				// SSP 11/11/03 Add Row Feature
				//
				//row = row.GetSibling( Infragistics.Win.UltraWinGrid.SiblingRow.Next, spanBands );
				row = row.GetSibling( Infragistics.Win.UltraWinGrid.SiblingRow.Next, spanBands, false, includeRowTypes );
				if ( !excludeHidden || null == row || !row.HiddenResolved )
					break;
			}
            
			return null != row;				
		}


		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a sibling row above it.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has a sibling row above it. If a sibling row exists above the row, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">A reference to a sibling row can be returned by invoking the <b>GetSibling</b> method.</p>
		/// <p class="body">The <b>HasChild</b>, <b>HasNextSibling</b>, and <b>HasParent</b>  methods can be invoked to determine whether a row has a child row, a sibling row after it, and a parent row, respectively.</p>
		/// </remarks>
        /// <returns>true if a row has a sibling row above it.</returns>
		public bool HasPrevSibling()
		{
			return this.HasPrevSibling(false);		
		}
		
		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a sibling row above it.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has a sibling row above it. If a sibling row exists above the row, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">A reference to a sibling row can be returned by invoking the <b>GetSibling</b> method.</p>
		/// <p class="body">The <b>HasChild</b>, <b>HasNextSibling</b>, and <b>HasParent</b>  methods can be invoked to determine whether a row has a child row, a sibling row after it, and a parent row, respectively.</p>
		/// </remarks>
        /// <param name="spanBands">True will look for rows in previous sibling bands.</param>
        /// <returns>true if a row has a sibling row above it.</returns>
		public bool HasPrevSibling(bool spanBands)
		{
			return this.HasPrevSibling( spanBands, true );
		}

		/// <summary>
		/// Returns a Boolean expression indicating whether a row has a sibling row above it.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to determine whether a row has a sibling row above it. If a sibling row exists above the row, this method returns True; otherwise, this method returns False.</p>
		/// <p class="body">The <i>spanbands</i> argument can be used to indicate whether rows in other bands are considered siblings.</p>
		/// <p class="body">A reference to a sibling row can be returned by invoking the <b>GetSibling</b> method.</p>
		/// <p class="body">The <b>HasChild</b>, <b>HasNextSibling</b>, and <b>HasParent</b>  methods can be invoked to determine whether a row has a child row, a sibling row after it, and a parent row, respectively.</p>
		/// </remarks>
		/// <param name="spanBands">True will look for rows in previous sibling bands.</param>
		/// <param name="excludeHidden">If true will not consider rows that are hidden.</param>
        /// <returns>true if a row has a sibling row above it.</returns>		
		public bool HasPrevSibling( bool spanBands, bool excludeHidden )
		{
			return this.HasPrevSibling( spanBands, excludeHidden, IncludeRowTypes.DataRowsOnly );
		}

		// SSP 11/11/03 Add Row Feature
		//
		internal bool HasPrevSibling( bool spanBands, IncludeRowTypes includeRowTypes )
		{
			return this.HasPrevSibling( spanBands, true, includeRowTypes );
		}

		// SSP 11/11/03 Add Row Feature
		// Added includeRowTypes parameter.
		//
		internal bool HasPrevSibling( bool spanBands, bool excludeHidden, IncludeRowTypes includeRowTypes )
		{
			// SSP 4/19/04 - Virtual Binding Related Optimizations
			// If a lot of rows are filtered out after this row then doing a linear search
			// will take a long time. So instead make use of the visible row indeces to
			// get the next visible sibling.
			//
			// ------------------------------------------------------------------------------
			if ( excludeHidden )
				return null != this.GetVisibleSiblingRow( false, spanBands, includeRowTypes );
			// ------------------------------------------------------------------------------

			UltraGridRow row = this;
			while ( true ) 
			{
				// SSP 11/11/03 Add Row Feature
				//
				//row = row.GetSibling( Infragistics.Win.UltraWinGrid.SiblingRow.Previous, spanBands );
				row = row.GetSibling( Infragistics.Win.UltraWinGrid.SiblingRow.Previous, spanBands, false, includeRowTypes );
				if ( !excludeHidden || null == row || !row.HiddenResolved )
					break;
			}
			return null != row;
		}

		/// <summary>
		///  Refresh the display and/or refetch the data with or without events.
		/// </summary>
		/// <remarks>
		/// <p class="body">Generally, painting a control is handled automatically while no events are occurring. However, there may be situations where you want the form or control updated immediately, for example, after some external event has caused a change to the form. In such a case, you would use the <b>Refresh</b> method.</p>
		/// <p class="body">The <b>Refresh</b> method can also be used to ensure that the user is viewing the latest copy of the data from the record source.</p>
		/// </remarks>
		public void Refresh()
		{
			this.Refresh( Infragistics.Win.UltraWinGrid.RefreshRow.RefreshDisplay );
		}

		/// <summary>
		///  Refresh the display and/or refetch the data with or without events.
		/// </summary>
        /// <param name="action">The refresh action to perform.</param>
		/// <remarks>
		/// <p class="body">Generally, painting a control is handled automatically while no events are occurring. However, there may be situations where you want the form or control updated immediately, for example, after some external event has caused a change to the form. In such a case, you would use the <b>Refresh</b> method.</p>
		/// <p class="body">The <b>Refresh</b> method can also be used to ensure that the user is viewing the latest copy of the data from the record source.</p>
		/// </remarks>
		public void Refresh( RefreshRow action )
		{
			this.Refresh( action, false );
		}

		/// <summary>
		///  Refresh the display and/or refetch the data with or without events.
		/// </summary>
        /// <param name="action">The refresh action to perform.</param>
        /// <param name="refreshDescendantRows">true to refresh all descendant rows recursively.</param>
		/// <remarks>
		/// <p class="body">Generally, painting a control is handled automatically while no events are occurring. However, there may be situations where you want the form or control updated immediately, for example, after some external event has caused a change to the form. In such a case, you would use the <b>Refresh</b> method.</p>
		/// <p class="body">The <b>Refresh</b> method can also be used to ensure that the user is viewing the latest copy of the data from the record source.</p>
		/// </remarks>
		public void Refresh( RefreshRow action, bool refreshDescendantRows )
		{
			switch ( action )
			{
				case Infragistics.Win.UltraWinGrid.RefreshRow.RefreshDisplay:
					this.InvalidateItemAllRegions();
					break;
        
				case Infragistics.Win.UltraWinGrid.RefreshRow.FireInitializeRow:
            
					this.InvalidateItemAllRegions();

					if ( null != this.Layout.Grid )
					{
						// fire initialize row
						// SSP 1/6/04 UWG2811
						// If this is a template add-row then fire the InitializeTemplateAddRow instead
						// of InitializeRow event.
						//
						if ( ! this.IsTemplateAddRow )
							// SSP 10/17/04
							// We had FireInitializeRow and InitializeRow methods that did almost the
							// same things. Got rid of InitializeRow because it's getting called from
							// only two places.
							//
							//this.InitializeRow( true );
							this.FireInitializeRow( true );
						else
							this.ParentCollection.FireInitializeTemplateAddRow( );
					}

					break;
			};
	
			// If the refresh flag was passed in then call this method on
			// the child bands collection
			//
			if ( refreshDescendantRows && 
				this.childBands != null )
				this.childBands.Refresh( action, true );

		}

		// SSP 3/4/02
		// Added this method to position the row according to the current
		// sort criteria without having to resort the whole rows collection.
		//
		/// <summary>
		/// If the row is not at correct sort position, this method will
		/// reposition the row in the rows collection based on the current
		/// sort criteria. Also if it's in the wrong group, it will put it
		/// under appropriate group by row.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This method can be useful in situations where a new row is added
		/// to the rows collection (which by default is appended at the end 
		/// of the collection) and you want to ensure the row is positioned
		/// at the right position in the collection based on the sort criteria
		/// without having to resort the whole collection. This method should
		/// not be used if the sort criteria itself changes which effects the
		/// whole rows collection.
		/// </p>
		/// <p class="body">
		/// To resort all the rows of a band, call 
		/// <see cref="SortedColumnsCollection.RefreshSort"/> method of the 
		/// <see cref="SortedColumnsCollection"/> object.
		/// </p>
		/// <seealso cref="SortedColumnsCollection.RefreshSort"/>
		/// </remarks>
		public virtual void RefreshSortPosition( )
		{
			// If this row is not in the correct group, then reposition it
			// in the correct group.
			//
			// MD 8/2/07 - 7.3 Performance
			// Refactored - Prevent calling expensive getters multiple times
			#region Refactored

			//if ( this.IsStillValid && null != this.ParentRow &&
			//    this.ParentRow is UltraGridGroupByRow )
			//{
			//    UltraGridGroupByRow groupByRow = (UltraGridGroupByRow)this.ParentRow;

			//    // If the row does not belong to the group it's currently in
			//    // then remove it from it and readd it to appropriate group.
			//    //
			//    if ( !groupByRow.DoesRowMatchGroup( this, true ) )
			//    {
			//        groupByRow.Rows.SyncRows_Remove( this, false );

			//        // SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
			//        // Commented out the following line of code. ScrollCountManagerSparseArray itself 
			//        // should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
			//        // method.
			//        //
			//        //groupByRow.Rows.DirtyScrollableRowCountHelper( );

			//        groupByRow.Rows.TopLevelRowsCollection.SyncRows_Add( this );

			//        // SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
			//        // Commented out the following line of code. ScrollCountManagerSparseArray itself 
			//        // should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
			//        // method.
			//        //
			//        //this.ParentCollection.DirtyScrollableRowCountHelper( );					
			//    }
			//}

			#endregion Refactored
			if ( this.IsStillValid )
			{
				UltraGridGroupByRow groupByRow = this.ParentRow as UltraGridGroupByRow;

				if ( null != groupByRow )
				{
					// If the row does not belong to the group it's currently in
					// then remove it from it and readd it to appropriate group.
					//
					if ( !groupByRow.DoesRowMatchGroup( this, true ) )
					{
						groupByRow.Rows.SyncRows_Remove( this, false );

						// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
						// Commented out the following line of code. ScrollCountManagerSparseArray itself 
						// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
						// method.
						//
						//groupByRow.Rows.DirtyScrollableRowCountHelper( );

						groupByRow.Rows.TopLevelRowsCollection.SyncRows_Add( this );

						// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
						// Commented out the following line of code. ScrollCountManagerSparseArray itself 
						// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
						// method.
						//
						//this.ParentCollection.DirtyScrollableRowCountHelper( );					
					}
				}
			}

			// Call EnsureCorrectSortPosition on the parent collection.
			//
			this.ParentCollection.EnsureCorrectSortPosition( this );

			// Dirty the visible rows and dirty the grid element.
			//
			this.Layout.RowScrollRegions.DirtyAllVisibleRows( );
			this.Layout.DirtyGridElement( );
		}

		#region RefreshFilters

		// SSP 4/11/07 BR21578
		// Added RefreshFilter method.
		// 

		/// <summary>
		/// Re-evaluates the filter conditions on this row.
		/// </summary>
		public void RefreshFilters( )
		{
			sbyte origIsFilteredOut = this.cachedIsFilteredOut;
			this.cachedIsFilteredOut = FILTERS_NEVER_EVALUATED;

			RowsCollection parentCollection = this.ParentCollection;
			if ( null == parentCollection || parentCollection.FiltersNeedToBeEvaluated )
				return;

			this.ApplyFilters( parentCollection.ActiveColumnFilters );

			if ( origIsFilteredOut != this.cachedIsFilteredOut )
				this.HiddenStateChanged( );
		}

		#endregion // RefreshFilters

		// SSP 10/17/04
		// We had FireInitializeRow and InitializeRow methods that did almost the
		// same things. Got rid of InitializeRow because it's getting called from
		// only two places. The only difference between the two was that 
		// InitializeRow saved original values of unbound cells.
		//
		internal void FireInitializeRow( bool saveOriginalValForUnboundCells )
		{
			this.FireInitializeRow( );

			if ( saveOriginalValForUnboundCells )
				this.SaveOriginalValForUnboundCells( );
		}

		

		internal void InternalPositionToRow( )
		{
			this.InternalPositionToRow( null );
		}


		internal virtual void InternalPositionToRow( System.Collections.Stack oldPositions )
		{
			// JJD 10/01/01
			// Only do this for the UltraGrid. UltraCombo and UltraDropDown
			// should not be synced with the binding manager's position 
			//
			// SSP 3/31/04
			// If the binding manager is null, return.
			//
			//if ( !( this.Layout.Grid is UltraGrid ) )
			// SSP 5/18/05 BR03434
			// Added SyncWithCurrencyManager to prevent the UltraGrid from synchronizing with
			// the currency manager. When there are a lot of deep nested bands activating a 
			// different ancestor row caues a lot of slowdown.
			// 
			//if ( !( this.Layout.Grid is UltraGrid ) || null == this.Band.BindingManager )
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			// SSP 6/8/05 BR03609
			// Allow for null bindingManager on bands.
			// 
			//if ( null == grid || null == this.Band.BindingManager )
			if ( null == grid )
				return;

			// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
			// This is taken care of in derived class.
			//
			

			// if an oldPositions stack was passed in then save the old position at 
			// this band level
			//
			// SSP 6/8/05 BR03609
			// Allow for null bindingManager on bands.
			// 
			//if ( oldPositions != null )
			if ( oldPositions != null && null != this.band.BindingManager )
			{
				oldPositions.Push( this.band.BindingManager.Position );
			}

			// call this method on the parent row first since that has to
			// be set first to give the proper context
			//
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.ParentRow != null )
			UltraGridRow parentRow = this.ParentRow;

			if ( parentRow != null )
			{
				//RobA 11/7/01
				//Set alreadySet based on the IgnoreDataSourePositionChange flag,
				//this we we do not reset IgnoreDataSourePositionChange to false before 
				//we exit out of this recursive call.
				//			
				bool alreadySet = this.Layout.IgnoreDataSourePositionChange;


				//RobA UWG326 9/21/01 ignore data source position changed event
				//so that we don't set the active row
				//
				this.Layout.IgnoreDataSourePositionChange = true;

				try
				{
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//this.ParentRow.InternalPositionToRow( oldPositions );
					parentRow.InternalPositionToRow( oldPositions );
				}
				
				catch (Exception)
				{
					if ( !alreadySet )
						this.Layout.IgnoreDataSourePositionChange = false;
					throw;
				}

				if ( !alreadySet )
					this.Layout.IgnoreDataSourePositionChange = false;
			}

			// set the position to this row
			//
			// SSP 9/5/01 - We need to use the ListIndex not the row index
			// because the row index could be different if the rows are
			// sorted. They won't match the binding manager.
			// Also do not change the position in BindingManager if the 
			// row is a group by row
			//if ( this.band.BindingManager.Position != this.Index )
			//    this.band.BindingManager.Position = this.Index;
			// SSP 5/18/05 BR03434
			// Added SyncWithCurrencyManager to prevent the UltraGrid from synchronizing with
			// the currency manager. When there are a lot of deep nested bands activating a 
			// different ancestor row caues a lot of slowdown.
			// 
			//if ( !( this is UltraGridGroupByRow ) && this.band.BindingManager.Position != this.ListIndex )
			// SSP 6/8/05 BR03609
			// Allow for null bindingManager on bands. Check for null BindingManager.
			// 
			//if ( grid.SyncWithCurrencyManager && this.band.BindingManager.Position != this.ListIndex )
			if ( grid.SyncWithCurrencyManager && null != this.band.BindingManager 
				 && this.band.BindingManager.Position != this.ListIndex )
			{
				// SSP 11/20/01 UWG671
				// Use IgnoreDataSourePositionChange flag to make sure data source
				// events are ignored when we are setting the position.
				//
				//this.band.BindingManager.Position = this.ListIndex;
	
				// If IgnoreDataSourePositionChange is already set then,
				// set this flag to to true so that we know that the 
				// IgnoreDataSourePositionChange flag was set before this call 
				// (since this is a recursive method, it could have been set by 
				//  the call that called this call) so we don't reset it
				// to false at the end.
				bool flagAlreadySet = this.Layout.IgnoreDataSourePositionChange;
				try
				{				
					if ( !flagAlreadySet )
						this.Layout.IgnoreDataSourePositionChange = true;

					this.band.BindingManager.Position = this.ListIndex;
				}
				finally
				{
					if ( !flagAlreadySet )
						this.Layout.IgnoreDataSourePositionChange = false;
				}
			}
		}

		internal void restoreOldPositions( System.Collections.Stack oldPositions )
		{

			// call this method on the parent row first since that has to
			// be restored first to give the proper context
			//
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.ParentRow != null )
			//    this.ParentRow.restoreOldPositions( oldPositions );
			UltraGridRow parentRow = this.ParentRow;

			if ( parentRow != null )
				parentRow.restoreOldPositions( oldPositions );

			// pop the old position off the stack
			//
			int oldPosition = (int)oldPositions.Pop();

			// restore the old position
			//
			// SSP 3/31/04
			// Check for binding manager being null.
			//
			//if ( this.band.BindingManager.Position != oldPosition )
			if ( null != this.band.BindingManager && this.band.BindingManager.Position != oldPosition )
				this.band.BindingManager.Position = oldPosition;
		}

		
		/// <summary>
		///  Updates the data source with any modified information from the grid.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Update</b> method updates any modified information in the grid, sending it to the data provider. When the update is complete, any rows that were marked as having modified data will have that mark cleared. The <b>DataChanged</b> property will be set to False.</p>
		/// <p class="body">Normally, the grid handles the updating of data automatically, so there will be few situations in which you will need to invoke this method. The major exception is when you have set the <b>UpdateMode</b> property to 'OnUpdate'. When using that setting, the grid will not send any data to the data provider until you invoke the <b>Update</b> method. You must use the method to manually update the data provider whenever data has been changed and you are ready to commit the changes.</p>
		/// </remarks>
		public virtual bool Update()
		{
			// SSP 11/29/05 - NAS 6.1 Multi-cell Operations
			// Added an overload that doesn't actually fire the error event, it just returns the
			// data error that needs to be fired. This way the cop & paste logic can make use of this
			// method.
			// 

			DataErrorInfo dataErrorInfo;
			bool ret = this.Update( out dataErrorInfo );

			if ( null != dataErrorInfo )
			{
				UltraGrid grid = this.Layout.Grid as UltraGrid;
				if ( null != grid )
					grid.InternalHandleDataError( dataErrorInfo );
			}

			return ret;
		}

		// SSP 11/29/05 - NAS 6.1 Multi-cell Operations
		// Added an overload that doesn't actually fire the error event, it just returns the
		// data error that needs to be fired. This way the cop & paste logic can make use of this
		// method.
		// 
		internal bool Update( out DataErrorInfo error )
		{
			error = null;
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_234"));

			// SSP 8/17/01 UWG132
			// Only update if the data has changed
			// SSP 11/17/05 BR07772
			// If the column is bound then we need to honor the UpdateData method
			// call even if modifications were made via the calc manager.
			// 
			//if ( !this.DataChanged )
			if ( ! this.DataChanged && ! this.dataChangedViaCalcManager )
				return true;				

			if ( null != grid )
			{
                // MRS 12/8/2008 - TFS11256
                // We should not fire events for a row in an export
                // or print layout, because we copy the values from every unbound cell
                // in the grid into the new cloned export/print layout and this causes
                // a whole lot of unneccessary event firings.
                // Added 'if' block 
                //
                if (false == this.Layout.IsPrintLayout &&
                    false == this.Layout.IsExportLayout)
                {
                    CancelableRowEventArgs e = new CancelableRowEventArgs(this);

                    grid.FireEvent(GridEventIds.BeforeRowUpdate, e);

                    if (e.Cancel)
                    {
                        // SSP 1/14/02 UWG1928
                        // Rather than calling canceledit on the row object always, look at the newly
                        // added RowUpdateCancelAction property off the grid and decide whether to
                        // call canceledit or not.
                        //
                        
                        if (RowUpdateCancelAction.CancelUpdate == grid.RowUpdateCancelAction)
                            this.CancelEdit();

                        // SSP 7/26/02 UWG1436 UWG1365
                        // Return false when cancelled. If the operation this method was called
                        // to perform does not succeed, then retrun false.
                        //
                        //return true;
                        return false;
                    }
                }					
			}
			
			// SSP 10/16/02 UWG1116
			// If a cell in the row is in edit mode, then get its value and commit it
			// to the binding list.
			//
			// ----------------------------------------------------------------------------
			if ( null != this.Layout && null != this.Layout.ActiveCell && 
				this == this.Layout.ActiveCell.Row && this.Layout.ActiveCell.IsInEditMode )
			{
				// SSP 10/24/02 UWG1782
				// Don't call CommitEditValue if we are being called from within it.
				// This isn't necessary because the CommitEditValue checks if the old value
				// and new value are the same if they are it doesn't call this method back
				// breaking the recursion right there. However flawed embeddable editor
				// implementations may cause weird behaviour (like the case that caused me to
				// do this in the first place was the CheckEditor when its Value
				// property was set to true, it unchecked itself and in CommitEditValue,
				// the orignal value and the new value came out to be different).
				// Enclosed the code in the if block.
				if ( !this.Layout.ActiveCell.InCommitEditValue )
				{
					bool discard = true;
					this.Layout.ActiveCell.CommitEditValue( ref discard, false );
				}
			}
			// ----------------------------------------------------------------------------

			try
			{
				this.EndEdit();
			}
			catch ( Exception exception )
			{			
				error = new DataErrorInfo( exception, null,
					this, null, DataErrorSource.RowUpdate, 
					SR.GetString("DataErrorRowUpdateUnableToUpdateRow", exception.Message) ); // "Unable to update the row:\n" + exception.Message );

				// SSP 11/29/05 - NAS 6.1 Multi-cell Operations
				// Added an overload that doesn't actually fire the error event, it just returns the
				// data error info that needs to be fired. This way the Multi-cell Operations logic can make 
				// use of this method.
				// 
				//if ( null != this.Layout.Grid )
				//	grid.InternalHandleDataError( e );

				return false;
			}

			if ( null != grid )
			{
                // MRS 12/8/2008 - TFS11256
                // We should not fire events for a row in an export
                // or print layout, because we copy the values from every unbound cell
                // in the grid into the new cloned export/print layout and this causes
                // a whole lot of unneccessary event firings.
                // Added 'if' block 
                //
                if (false == this.Layout.IsPrintLayout &&
                    false == this.Layout.IsExportLayout)
                {
                    RowEventArgs e = new RowEventArgs(this);

                    grid.FireEvent(GridEventIds.AfterRowUpdate, e);
                }
			}

            // MBS 4/16/08 - RowEditTemplate NA2008 V2
            // The user might want to call Update on the row without closing the template.  However, the template
            // will now be in a state where no further changes can be made to the underlying row, so we should
            // call BeginEdit again; the template will call Cancel/EndEdit when it is closed.
            UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
            if (template != null && template.IsShown)
                this.BeginEdit();

			return true;
		}

		/// <summary>
		/// Cancels the update of the row or cell when data has been changed (similar to pressing ESC).
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <b>CancelUpdate</b> method is invoked for a row, any changes made to the cells of the active row are removed. The cells display their original values, and the row is taken out of edit mode. The row selector picture changes from the "Write" image back to the "Current" image. The <b>DataChanged</b> property will be set to false.</p>
		/// </remarks>
		public void CancelUpdate()
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			// Only perform the cancel if
			// data had been changed.
			if ( !this.DataChanged )
				return;

			// SSP 8/21/01 UWG174
			if ( null != grid )
			{
				CancelableRowEventArgs e = new CancelableRowEventArgs( this );
				grid.FireEvent( GridEventIds.BeforeRowCancelUpdate, e );

				if ( e.Cancel )
					return;
			}
			

			this.CancelEdit();

			
			
			//set the data changed to false			
			this.SetDataChanged( false );

			// SSP 8/21/01 UWG174
			if ( null != grid )
			{
				grid.FireEvent( GridEventIds.AfterRowCancelUpdate, new RowEventArgs( this ) );
			}

			this.InvalidateItemAllRegions();

            // MBS 4/16/08 - RowEditTemplate NA2008 V2
            // If the template is currently shown, the user might want to have a "Reset" button that will
            // change all the cells back to the original values.  However, by calling this method, the template
            // will now be in a state where no further changes can be made to the underlying row, so we should
            // call BeginEdit again; the template will call Cancel/EndEdit when it is closed.
            UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
            if (template != null && template.IsShown)
                this.BeginEdit();
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool EnsureAllAncestorsCurrentRow( UltraGridRow row )
		internal static bool EnsureAllAncestorsCurrentRow( UltraGridRow row )
		{
			UltraGridRow parentRow = row.FindActualAncestorRow;

			if ( null != parentRow )
			{			
				// If the making the parentRow's ancestors current row failed
				// then return.
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( !parentRow.EnsureAllAncestorsCurrentRow( parentRow ) )
				if ( !UltraGridRow.EnsureAllAncestorsCurrentRow( parentRow ) )
					return false;
			}
			
			BindingManagerBase bm = row.Band.BindingManager;

			Debug.Assert( null != bm, "Null binding manager on a band !" );

			if ( null == bm )
				return false;

			Debug.Assert( row.ListIndex >= 0, "Invalid row ListIndex !" );

			if ( row.ListIndex < 0 )
				return false;

			if ( bm.Position != row.ListIndex )
			{
				bm.Position = row.ListIndex;

				return bm.Position == row.ListIndex;
			}

			return true;
		}

		internal void DeleteHelper( )
		{
			BindingManagerBase bm = this.band.BindingManager;

			// SSP 3/31/04
			// If the binding manager is null that means we are in the mode where
			// DataSource is not set. We are just displaying the data structure.
			// 
            // MBS 9/16/08 - TFS6776
            // We can have a null binding manager in more cases than this, such as a 
            // recursive structure within a BindingSource, so we shouldn't bail out 
            // just on this condition
            //
            //if (null == bm)                                
            //    return;            
			
			bool ignoreDataSourePositionChangeAlreadySet =
				this.Band.Layout.IgnoreDataSourePositionChange;
			
			// SSP 8/6/04 UWG3553
			// If the cell in edit mode is from this row then exit the edit mode before 
			// deleting the row.
			//
			// ------------------------------------------------------------------------------
			UltraGridCell activeCell = this.Layout.ActiveCell;
			if ( null != activeCell && this == activeCell.Row && activeCell.IsInEditMode )
				activeCell.ExitEditMode( true, true );
			// ------------------------------------------------------------------------------

            // MBS 4/15/08 - RowEditTemplate NA2008 v2
            // Make sure that we cause the editor of a proxy to exit edit mode as well
            UltraGridCellProxy activeProxy = this.Layout.ActiveProxy;
            if (null != activeProxy && activeProxy.Editor.IsInEditMode)
                activeProxy.Editor.ExitEditMode(true, true);

			try
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.Band.inDeleteRow = true;
				//this.Band.rowBeingDeleted = this;

				
				// SSP 11/27/01 UWG717
				// Set this flag so that we ignore the position changed event
				// because a valid active row will be set in the SyncRows.
				//
				if ( null != this.ParentCollection.TopLevelRowsCollection )
					this.Band.Layout.IgnoreDataSourePositionChange = true;

                // MBS 4/16/08 - RowEditTemplate NA2008 V2
                UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
                if (template != null && template.IsShown)
                    template.Close(false, true);

				
				
				
				
				

				// SSP 1/22/02 UWG961
				// Enclosed EndCurrentEdit into a try-catch block because the
				// user may be trying to delete a row that's not have proper
				// input and calling EndCurrentEdit will throw exception in 
				// that case.
				// 
				try
				{
                    // MBS 9/16/08 - TFS6776
                    // We need to check for null now
                    if(bm != null)
					    bm.EndCurrentEdit( );
				}
				catch ( Exception )
				{
					Debug.WriteLine( "Unable to EndCurrentEdit before proceeding deleting the row !" );
				}

				// SSP 12/20/01
				// For some reason, when we are bound to IList (and not an IBindingList)
				// the binding manager does not make sure the position falls within the
				// range of count (especially when last row is deleted, position stays 
				// unchanged to the last row, which leads to an exception being thrown 
				// later on.
				//
				try
				{
					if ( !this.Band.IsBoundToIBindingList &&
                        // MBS 9/16/08 - TFS6776
                        // We need to check for null now
                        bm != null &&
						bm.Position >= bm.Count - 1 &&
						bm.Count - 2 >= 0 )
						bm.Position = bm.Count - 2;
				}
				catch ( Exception )
				{
					Debug.WriteLine( "Unable to set the position in Row.DeleteHelper( ) !" );
				}

				// SSP 12/27/05 BR08328
				// 
				int listIndex = this.ListIndex;

				// SSP 1/17/02
				// Only call RemoveAt on the binding manager if all the ancestor
				// rows of the row are current row otherwise call RemoveAt on the
				// IBindingList associated with the rows collection.
				//
				UltraGridRow parentRow = this.FindActualAncestorRow;
				if ( null == parentRow || 
					parentRow.AreAllAncestorsCurrentRows || 
					// If the parentCollection's binding list is null, that probably means
					// that we are not bound to the binding list.
					//
					null == this.ParentCollection.BindingList )
				{
					// SSP 8/14/02 UWG1433
					// Use the listIndex local variable instead of the property. We are storing the listIndex
					// before calling EndCurrentEdit and accessing properties off the rows collection that 
					// could potentially sync the rows causing this row's ListIndex to change.
					//
					//bm.RemoveAt( this.ListIndex );
                    //
                    // MBS 9/16/08 - TFS6776
                    // We need to check for null now
                    if(bm != null)
					    bm.RemoveAt( listIndex );
				}
				else
					// SSP 8/14/02 UWG1433
					// Use the listIndex local variable instead of the property. We are storing the listIndex
					// before calling EndCurrentEdit and accessing properties off the rows collection that 
					// could potentially sync the rows causing this row's ListIndex to change.
					//
					//this.ParentCollection.BindingList.RemoveAt( this.ListIndex );
					this.ParentCollection.BindingList.RemoveAt( listIndex );

				// Only reset if it wasn't already set
				//
				if ( !ignoreDataSourePositionChangeAlreadySet )
					this.Band.Layout.IgnoreDataSourePositionChange = false;

				// SSP 11/27/01 UWG717
				// Call SyncRows to sync the rows so that SyncRows method makes
				// use of inDeleteRow and rowBeingDeleted flags to handle a row
				// deletion appropriately.
				//
				// Only sync the rows if we aren't hooked into the binding list.
				// because if we are, then we do get messages when rows are
				// deleted.
				//
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( !this.Band.HookIntoList && 
				//    null != this.ParentCollection.TopLevelRowsCollection )
				//    this.ParentCollection.TopLevelRowsCollection.SyncRows( );
				if ( !this.Band.HookIntoList )
				{
					RowsCollection topLevelRowsCollection = this.ParentCollection.TopLevelRowsCollection;

					if ( null != topLevelRowsCollection )
						topLevelRowsCollection.SyncRows();
				}
			}
			finally
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.Band.inDeleteRow = false;
				//this.Band.rowBeingDeleted = null;

				if ( !ignoreDataSourePositionChangeAlreadySet )
					this.Band.Layout.IgnoreDataSourePositionChange = false;
			}

			// JJD 8/21/01
			// Dirty the parent row count manager
			//
			// SSP 1/22/02 UWG958
			// Dirty the row count on the parent ChildBand instead of the ParentRow
			//
			
			// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
			// Commented out the following code. We don't need this here since the syncing
			// logic in the rows collection is doing this.
			//
			//if ( this.ParentChildBand != null )
			//	((IScrollableRowCountManagerOwner)this.ParentChildBand).ScrollableRowCountManager.DirtyRowCount();
			//else
			//	((IScrollableRowCountManagerOwner)this.Layout).ScrollableRowCountManager.DirtyRowCount();

			//cm.List.RemoveAt( cm.List.IndexOf( this.listObject ) );
		}


		// SSP 4/25/03 UWG2185
		// Added an overload of Delete that takes in a boolean parameter indicating whether to display
		// the dialog or not.
		// 
		/// <summary>
		/// Deletes the row.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a row is deleted, the <b>BeforeRowsDeleted</b> event is generated. Afterwards, the row is removed from the control and its corresponding record is deleted from the data source. If the record cannot be removed from the data source, the <b>Error</b> event is generated.</p>
		/// <p class="body">The <b>DeleteSelectedRows</b> method of the control can be invoked to delete all selected rows.</p>
		/// </remarks>
		public bool Delete( )
		{
			return this.Delete( true );
		}

		/// <summary>
		/// Deletes the row.
		/// </summary>
		/// <param name="displayPrompt">Specifies whether to display the delete confirmation prompt.</param>
		/// <returns></returns>
		/// <remarks>
		/// <p class="body">When a row is deleted, the <b>BeforeRowsDeleted</b> event is generated. Afterwards, the row is removed from the control and its corresponding record is deleted from the data source. If the record cannot be removed from the data source, the <b>Error</b> event is generated.</p>
		/// <p class="body">The <b>DeleteSelectedRows</b> method of the control can be invoked to delete all selected rows.</p>
		/// </remarks>
		public bool Delete( bool displayPrompt )
		{
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_235"));


			// If row has already been deleted, then do nothing
			if ( !this.parentCollection.Contains( this ) )
				return false;

            // MBS 9/16/08 - TFS6776
            // It is possible to have a null binding list now but still allow deletes, such
            // as if we have a recursive data structure within a BindingSource
            //
			//RobA UWG296 9/24/01
			//IBindingList bl = this.band.BindingList;
            //IList bl = this.band.List;                        
            //if ( bl == null )
            //    return false;

			// SSP 9/21/06 BR15379
			// Honor MinRows setting of the band.
			// 
			if ( this.Band.MinRows > 0 && this.ParentCollection.TopLevelRowsCollection.UnSortedActualRows.Count <= this.Band.MinRows )
			{
				this.Layout.RaiseMinRowsViolatedError( this, this.Band.MinRows );
				return false;
			}

			// fire the BeforeRowsDeleted event
			//
			BeforeRowsDeletedEventArgs eventArgs = new BeforeRowsDeletedEventArgs( new UltraGridRow[] {this}, true );

			// SSP 4/25/03 UWG2185
			// Added an overload of Delete that takes in a boolean parameter indicating whether to display
			// the dialog or not.
			// 
			eventArgs.DisplayPromptMsg = displayPrompt;
            
			grid.FireEvent( GridEventIds.BeforeRowsDeleted, eventArgs );
			
			if ( eventArgs.Cancel )
				return false;
			
			// SSP 1/18/02 UWG947
			// Use the DisplayPromptMsg off the event args to determine if we
			// should display the prompt message or not.
			//
			if ( eventArgs.DisplayPromptMsg )
			{
				DialogResult result;
				result = MessageBox.Show(
					SR.GetString("DeleteSingleRowPrompt"), // "You have selected 1 row for deletion.\nChoose Yes to delete the row or No to exit.", 
					SR.GetString("DeleteRowsMessageTitle"), //"Delete Rows", 
					MessageBoxButtons.YesNo, MessageBoxIcon.Question  );
				if ( result != DialogResult.Yes )
					return false;
			}

			//RobA UWG296 9/24/01
			//if ( bl == null )
			//	throw new NotSupportedException("IBindlist not implemented, can't delete rows.");

			// SSP 4/25/03 UWG2185
			// This is something that the user calls. I don't think we should be checkinf if the AllowDelete
			// has been set here and throwing an exception because this is something the developer calls from
			// the code.
			//
			

			try
			{
                // MBS 11/18/08 - TFS10399
                // Fire the BeforeSelectChange if the row is selected
                bool isSelected = null != grid && grid.HasSelectedBeenAllocated && null != grid.Selected.Rows && grid.Selected.Rows.Contains(this);
                if (isSelected)
                {
                    SelectionStrategyBase selectionStrategy =
                        ((ISelectionStrategyProvider)this.Band).SelectionStrategyRow;

                    bool clearExistingItems = true;
                    if (null != selectionStrategy)
                        clearExistingItems = selectionStrategy.IsSingleSelect;

                    Selected selected = grid.CalcNewSelection(this, clearExistingItems, false);
                    grid.FireEvent(GridEventIds.BeforeSelectChange, new BeforeSelectChangeEventArgs(typeof(UltraGridRow), selected));
                }

				this.DeleteHelper( );

                // MBS 11/18/08 - TFS10399
                // Moved this check into a variable since we now want to fire the Before/AfterSelectChange
                //
                //// SSP 7/22/02 UWG1344
                //// Also make sure the deleted row is not in the Selected rows collection any more.
                ////
                //if ( null != grid && grid.HasSelectedBeenAllocated && null != grid.Selected.Rows &&
                //    grid.Selected.Rows.Contains( this ) )
                //
                if (isSelected)
                {
                    grid.Selected.Rows.Remove( this );

                    // MBS 11/18/08 - TFS10399
                    grid.FireEvent(GridEventIds.AfterSelectChange, new AfterSelectChangeEventArgs(typeof(UltraGridRow)));
                }
			}
			catch ( Exception e )
			{
				DataErrorInfo dataError = new DataErrorInfo( e, null, this,
					null, DataErrorSource.RowDelete, 
					SR.GetString("DataErrorDeleteRowUnableToDelete", e.Message) ); //"Unable to delete the row:\n" + e.Message );

				grid.InternalHandleDataError( dataError );

				return false;
			}
			
			//this.parentCollection.OnRowDeleted( this.Index );

			// if we actually deleted rows fire the AfterRowsDeleted event
			//
			grid.FireEvent( GridEventIds.AfterRowsDeleted, null );
			
			this.Layout.RowScrollRegions.DirtyAllVisibleRows( false );
			this.Layout.DirtyGridElement( );
			
			return true;
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetBand_0_Row( ) 
		{ 
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//return this.ParentRow != null
			//    ? this.ParentRow.GetBand_0_Row() 
			//    : this; 
			UltraGridRow parentRow = this.ParentRow;

			return parentRow != null
				? parentRow.GetBand_0_Row()
				: this; 
		}

		/// <summary>
		/// Returns True if the current row is the active row.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridBase.ActiveRow"/>
		/// </remarks>
		public bool IsActiveRow
		{
			get
			{
				return ( this.band.Layout.ActiveRow == this );
			}
		}

		#region IsActiveRowInternal

		// SSP 4/1/05 - Optimizations
		// Added IsActiveRowInternal.
		//
		internal bool IsActiveRowInternal
		{
			get
			{
				return this == this.Layout.ActiveRowInternal;
			}
		}

		#endregion // IsActiveRowInternal
 
		/// <summary>
		/// Resolves all of the appearance properties for the row selector
		/// </summary>
		/// <param name="appData">The structure to contain the resolved appearance.</param>
		public void ResolveRowSelectorAppearance( ref AppearanceData appData )
		{
			// SSP 10/8/03
			// This looks like a typo.
			//
			//this.ResolveAppearance( ref appData, AppearancePropFlags.AllRenderAndCursor );
			this.ResolveRowSelectorAppearance( ref appData, AppearancePropFlags.AllRenderAndCursor );
		}

		/// <summary>
		/// Resolves selected properties of row selector's appearance
		/// </summary>
		/// <param name="appData">The structure to contain the resolved appearance.</param>
		/// <param name="requestedProps">Bit flags indicating which properties to resolve.</param>
		public void ResolveRowSelectorAppearance( ref AppearanceData appData, 
			AppearancePropFlags requestedProps )
		{
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingRow parameter.
			// 
			//this.ResolveRowSelectorAppearance( ref appData, ref requestedProps, true );
			this.ResolveRowSelectorAppearance( ref appData, ref requestedProps, true, false );
		}

		// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
		// Added an overload of ResolveRowSelectorAppearance that takes in resolveDefaultColors.
		// 
		internal void ResolveRowSelectorAppearance( 
			ref AppearanceData appData, 
			ref AppearancePropFlags requestedProps,
			bool resolveDefaultColors,
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingRow parameter.
			// 
			bool hotTrackingRow )
		{
			UltraGridBand band = this.band;
			UltraGridLayout layout = band.Layout;

			ResolveAppearanceContext context = new ResolveAppearanceContext( this.GetType(), requestedProps );

			// SSP 3/12/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( layout, StyleUtils.Role.RowSelector, out context.ResolutionOrder );

			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Added an overload of ResolveRowSelectorAppearance that takes in resolveDefaultColors.
			// 
			context.ResolveDefaultColors = resolveDefaultColors;
			
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingRow parameter.
			// 
			if ( hotTrackingRow )
			{
				band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.HotTrackRowSelectorAppearance 
					// SSP 3/13/06 - App Styling
					// Use the new overload that takes in the app styling related params.
					// 
					, AppStyling.RoleState.RowHotTracked );
			}

			// SSP 12/15/04 - IDataErrorInfo Support
			// 
			// ----------------------------------------------------------------------------
			if ( this.HasDataError( ) )
			{
				// Do not resolve the image on the data error appearances. We don't want 
				// the editors to display this image. The DataErrorTooltipUIElement which
				// would be a sibling of the editor element in the cell will resolve and 
				// position the data error image itself.
				//
				AppearancePropFlags imageFlag = DATA_ERROR_APPEARANCE_EXCLUDE_PROPS & context.UnresolvedProps;
				context.UnresolvedProps ^= imageFlag;

				band.MergeBLOverrideAppearances( ref appData, ref context, 
					UltraGridOverride.OverrideAppearanceIndex.DataErrorRowSelectorAppearance
					// SSP 3/13/06 - App Styling
					// Use the new overload that takes in the app styling related params.
					// 
					, AppStyling.RoleState.DataError );

				context.UnresolvedProps ^= imageFlag;
			}
			// ----------------------------------------------------------------------------



			// merge in the row's rowSelectorAppearance values (if any)
			//
			if ( null != this.rowSelectorAppearance )
			{
				// SSP 3/8/06 - App Styling
				// Added if condition to the existing code.
				// 
				if ( context.ResolutionOrder.UseControlInfo )
				{
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//AppearanceData.MergeAppearance( ref appData, 
					//	this.rowSelectorAppearance.Data,
					//	ref context.UnresolvedProps );
					this.rowSelectorAppearance.MergeData( ref appData, ref context.UnresolvedProps );

					if ( 0 == context.UnresolvedProps )
						return;
				}
			}

			// SSP 3/29/05 - NAS 5.2 Fixed Rows/Filter Row
			//
			// ------------------------------------------------------------------------------------------------
			bool shouldApplyFixedAppearance = this.ShouldApplyFixedAppearance( );
			if ( shouldApplyFixedAppearance )
			{
				band.MergeBLOverrideAppearances( ref appData, ref context,
					UltraGridOverride.OverrideAppearanceIndex.FixedRowSelectorAppearance
					// SSP 3/13/06 - App Styling
					// Use the new overload that takes in the app styling related params.
					// 
					, AppStyling.RoleState.FixedRow );
			}

			bool isFilterRow = this.IsFilterRow;
			if ( isFilterRow )
			{
				band.MergeBLOverrideAppearances( ref appData, ref context,
					UltraGridOverride.OverrideAppearanceIndex.FilterRowSelectorAppearance 
					// SSP 3/13/06 - App Styling
					// Use the new overload that takes in the app styling related params.
					// 
					, AppStyling.RoleState.FilterRow );
			}
			// ------------------------------------------------------------------------------------------------

			// SSP 7/11/05 - NAS 5.3 Empty Rows
			// 
			// ------------------------------------------------------------------------------------------------
			if ( this.IsEmptyRow )
			{
				// SSP 3/9/06 - App Styling
				// 
				//layout.EmptyRowSettings.ResolveRowSelectorAppearance( ref appData, ref context.UnresolvedProps );
				layout.EmptyRowSettings.ResolveRowSelectorAppearance( ref appData, ref context );
			}
			// ------------------------------------------------------------------------------------------------

            // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
            if (layout.ActiveCellInternal != null && layout.ActiveCellInternal.Row == this)
            {
                band.MergeBLOverrideAppearances(ref appData, ref context, 
                    UltraGridOverride.OverrideAppearanceIndex.ActiveCellRowSelectorAppearance,
                    Infragistics.Win.AppStyling.RoleState.ActiveCell);
            }

			context.IsRowSelector = true;

			band.ResolveAppearance( ref appData, ref context );

			// JJD 4/17/01
			// Merge in the header appearances as a default 
			//
			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Added an overload of ResolveRowSelectorAppearance that takes in resolveDefaultColors.
			// 
			//if ( context.UnresolvedProps != 0 )
			if ( context.UnresolvedProps != 0 && context.ResolveDefaultColors )
			{
				// JJD 11/26/01 - UWG84
				// Instead of using header appearance as the default, use the 
				// default system colors
				//
				//ResolveAppearanceContext headercontext = new ResolveAppearanceContext( typeof( Infragistics.Win.UltraWinGrid.HeaderBase ), context.UnresolvedProps );
                
				//this.band.ResolveAppearance( ref appData, ref headercontext );

				// SSP 3/30/05 - NAS 5.2 Row Numbers
				// By default trim the row numbers and show ellipsis.
				//
				if ( 0 != ( AppearancePropFlags.TextTrimming & context.UnresolvedProps ) )
				{
					context.UnresolvedProps ^= AppearancePropFlags.TextTrimming;
					appData.TextTrimming = TextTrimming.EllipsisCharacter;
				}

				layout.ResolveColors( ref appData, 
					ref context, 
					SystemColors.Control, 
					SystemColors.WindowFrame,
					SystemColors.ControlText,
					false );
			}
			
		}

		/// <summary>
		/// Resolves the row object's appearance and sets the resolved values on the specified appearance data structure.
		/// </summary>
		/// <remarks>
		/// <p class="body">Examining the value of the <b>Appearance</b> property that has not been set will return the "use default" value, not the internal value that is actually being used to display the object affected by the Appearance object. In order to find out what values are being used, you must use the <b>ResolveAppearance</b> method. This method initializes the specified AppearanceData structur with values that can be used to determine how the object will look.</p>
		/// <p class="body">When you change the properties of an Appearance object, you are not required to specify a value for every property that object supports. Whether the Appearance object is a stand-alone object you are creating from scratch, or an intrinsic object that is already attached to some other object, you can set certain properties and ignore others. The properties you do not explicitly set are given a "use default" value that indicates there is no specific setting for that property.</p>
		/// <p class="body">Properties that are set to the "use default" value derive their settings from other objects by following an appearance hierarchy. In the appearance hierarchy, each object has a parent object from which it can inherit the actual numeric values to use in place of the "use default" values. The "use default" value should not be confused with the initial setting of the property, which is generally referred to as the default value. In many cases, the default setting of an object's property will be "use default"; this means that the property is initially set not to use a specific value. The "use default" value will be 0 for an enumerated property (usually indicated by a constant ending in the word "default", such as AlignDefault) or -1 (0xFFFFFFFF) for a numeric setting, such as that used by color-related properties.</p>
		/// <p class="body">So for example, if the Appearance object of a cell has its <b>BackColor</b> property set to -1, the control will use the setting of the row's <b>BackColor</b> property for the cell, because the row is above the cell in the appearance hierarchy. The top level of the appearance hierarchy is the UltraGrid control itself. If any of the UltraWinGrid's Appearance object properties are set to their "use default" values, the control uses built-in values (the "factory presets") for those properties. For example, the factory preset of the <b>BackColor</b> property of the grid's Appearance object is the system button face color (0x8000000F). This is the value that will be used for the grid's background color when the <b>BackColor</b> property of the grid's Appearance object is set to the "use default" value.</p>
		/// <p class="body">The <b>ResolveAppearance</b> method will initialize the specified AppearanceData structure with all of its "use default" settings converted into actual values. It does this by navigating the appearance hierarchy for each property until an explicit setting or a factory preset is encountered. If you simply place a grid on a form, run the project, and examine the setting of the <b>BackColor</b> property of the grid's intrinsic Appearance object:</p>
		/// <p class="code">MsgBox Hex(UltraWinGrid1.Rows(0).Appearance.BackColor)</p>
		/// <p class="body">...you will see that it is set to the "use default" value (0xFFFFFFFF). However, if you use the <b>ResolveAppearance</b> method to display the same value:</p>
		/// <p class="code">Dim appData as AppearanceData = new AppearanceData()</p>
		/// <p class="code">UltraWinGrid1.Rows(0).ResolveAppearance(ByRef appData)</p>
		/// <p class="code">MsgBox Hex(appData.BackColor)</p>  
		/// <p class="body">...you will see that it is set to the system button face color (0x8000000F).</p>
		/// </remarks>
		/// <param name="appData">The structure to contain the resolved appearance.</param>
		public void ResolveAppearance( ref AppearanceData appData )
		{
			this.ResolveAppearance( ref appData, AppearancePropFlags.AllRenderAndCursor );
		}

		/// <summary>
		/// Resolves the row object's appearance and sets the resolved values on the specified appearance data structure.
		/// </summary>
		/// <remarks>
		/// <p class="body">Examining the value of the <b>Appearance</b> property that has not been set will return the "use default" value, not the internal value that is actually being used to display the object affected by the Appearance object. In order to find out what values are being used, you must use the <b>ResolveAppearance</b> method. This method initializes the specified AppearanceData structur with values that can be used to determine how the object will look.</p>
		/// <p class="body">When you change the properties of an Appearance object, you are not required to specify a value for every property that object supports. Whether the Appearance object is a stand-alone object you are creating from scratch, or an intrinsic object that is already attached to some other object, you can set certain properties and ignore others. The properties you do not explicitly set are given a "use default" value that indicates there is no specific setting for that property.</p>
		/// <p class="body">Properties that are set to the "use default" value derive their settings from other objects by following an appearance hierarchy. In the appearance hierarchy, each object has a parent object from which it can inherit the actual numeric values to use in place of the "use default" values. The "use default" value should not be confused with the initial setting of the property, which is generally referred to as the default value. In many cases, the default setting of an object's property will be "use default"; this means that the property is initially set not to use a specific value. The "use default" value will be 0 for an enumerated property (usually indicated by a constant ending in the word "default", such as AlignDefault) or -1 (0xFFFFFFFF) for a numeric setting, such as that used by color-related properties.</p>
		/// <p class="body">So for example, if the Appearance object of a cell has its <b>BackColor</b> property set to -1, the control will use the setting of the row's <b>BackColor</b> property for the cell, because the row is above the cell in the appearance hierarchy. The top level of the appearance hierarchy is the UltraGrid control itself. If any of the UltraWinGrid's Appearance object properties are set to their "use default" values, the control uses built-in values (the "factory presets") for those properties. For example, the factory preset of the <b>BackColor</b> property of the grid's Appearance object is the system button face color (0x8000000F). This is the value that will be used for the grid's background color when the <b>BackColor</b> property of the grid's Appearance object is set to the "use default" value.</p>
		/// <p class="body">The <b>ResolveAppearance</b> method will initialize the specified AppearanceData structure with all of its "use default" settings converted into actual values. It does this by navigating the appearance hierarchy for each property until an explicit setting or a factory preset is encountered. If you simply place a grid on a form, run the project, and examine the setting of the <b>BackColor</b> property of the grid's intrinsic Appearance object:</p>
		/// <p class="code">MsgBox Hex(UltraWinGrid1.Rows(0).Appearance.BackColor)</p>
		/// <p class="body">...you will see that it is set to the "use default" value (0xFFFFFFFF). However, if you use the <b>ResolveAppearance</b> method to display the same value:</p>
		/// <p class="code">Dim appData as AppearanceData = new AppearanceData()</p>
		/// <p class="code">UltraWinGrid1.Rows(0).ResolveAppearance(ByRef appData)</p>
		/// <p class="code">MsgBox Hex(appData.BackColor)</p>  
		/// <p class="body">...you will see that it is set to the system button face color (0x8000000F).</p>
		/// </remarks>
		/// <param name="appData">The structure to contain the resolved appearance.</param>
		/// <param name="requestedProps">Bit flags indicating which properties to resolve.</param>
		public virtual void ResolveAppearance( ref AppearanceData appData,
			AppearancePropFlags requestedProps )
		{
			this.ResolveAppearance( ref appData, requestedProps, false );
		}
        
		/// <summary>
		/// Resolves the row object's appearance and sets the resolved values on the specified appearance data structure.
		/// </summary>
		/// <remarks>
		/// <p class="body">Examining the value of the <b>Appearance</b> property that has not been set will return the "use default" value, not the internal value that is actually being used to display the object affected by the Appearance object. In order to find out what values are being used, you must use the <b>ResolveAppearance</b> method. This method initializes the specified AppearanceData structur with values that can be used to determine how the object will look.</p>
		/// <p class="body">When you change the properties of an Appearance object, you are not required to specify a value for every property that object supports. Whether the Appearance object is a stand-alone object you are creating from scratch, or an intrinsic object that is already attached to some other object, you can set certain properties and ignore others. The properties you do not explicitly set are given a "use default" value that indicates there is no specific setting for that property.</p>
		/// <p class="body">Properties that are set to the "use default" value derive their settings from other objects by following an appearance hierarchy. In the appearance hierarchy, each object has a parent object from which it can inherit the actual numeric values to use in place of the "use default" values. The "use default" value should not be confused with the initial setting of the property, which is generally referred to as the default value. In many cases, the default setting of an object's property will be "use default"; this means that the property is initially set not to use a specific value. The "use default" value will be 0 for an enumerated property (usually indicated by a constant ending in the word "default", such as AlignDefault) or -1 (0xFFFFFFFF) for a numeric setting, such as that used by color-related properties.</p>
		/// <p class="body">So for example, if the Appearance object of a cell has its <b>BackColor</b> property set to -1, the control will use the setting of the row's <b>BackColor</b> property for the cell, because the row is above the cell in the appearance hierarchy. The top level of the appearance hierarchy is the UltraGrid control itself. If any of the UltraWinGrid's Appearance object properties are set to their "use default" values, the control uses built-in values (the "factory presets") for those properties. For example, the factory preset of the <b>BackColor</b> property of the grid's Appearance object is the system button face color (0x8000000F). This is the value that will be used for the grid's background color when the <b>BackColor</b> property of the grid's Appearance object is set to the "use default" value.</p>
		/// <p class="body">The <b>ResolveAppearance</b> method will initialize the specified AppearanceData structure with all of its "use default" settings converted into actual values. It does this by navigating the appearance hierarchy for each property until an explicit setting or a factory preset is encountered. If you simply place a grid on a form, run the project, and examine the setting of the <b>BackColor</b> property of the grid's intrinsic Appearance object:</p>
		/// <p class="code">MsgBox Hex(UltraWinGrid1.Rows(0).Appearance.BackColor)</p>
		/// <p class="body">...you will see that it is set to the "use default" value (0xFFFFFFFF). However, if you use the <b>ResolveAppearance</b> method to display the same value:</p>
		/// <p class="code">Dim appData as AppearanceData = new AppearanceData()</p>
		/// <p class="code">UltraWinGrid1.Rows(0).ResolveAppearance(ByRef appData)</p>
		/// <p class="code">MsgBox Hex(appData.BackColor)</p>  
		/// <p class="body">...you will see that it is set to the system button face color (0x8000000F).</p>
		/// </remarks>
		/// <param name="appData">The structure to contain the resolved appearance.</param>
		/// <param name="requestedProps">Bit flags indicating which properties to resolve.</param>
		/// <param name="previewArea">True to resolve the row preview area appearance.</param>
		public void ResolveAppearance( ref AppearanceData appData,
			AppearancePropFlags requestedProps,
			bool previewArea )
		{
			this.ResolveAppearance( ref appData, ref requestedProps, previewArea, false );
		}

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// Added an overload of ResolveAppearance that takes in hotTrackingRow parameter.
		// 
		internal void ResolveAppearance( ref AppearanceData appData,
			ref AppearancePropFlags requestedProps,
			bool previewArea,
			bool hotTrackingRow )
		{
			this.ResolveAppearance( ref appData, ref requestedProps, previewArea, hotTrackingRow, false );
		}

		// SSP 5/18/06 - App Styling
		// Added an overload that takes in resolveOnlyRowAppearances.
		// 
		internal void ResolveAppearance( ref AppearanceData appData,
			ref AppearancePropFlags requestedProps,
			bool previewArea,
			bool hotTrackingRow,
            bool resolveOnlyRowAppearances )
		{
			UltraGridBand band = this.band;
			UltraGridLayout layout = this.band.Layout;

			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.UltraGridRow), requestedProps );

			// SSP 3/12/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( layout, previewArea ? StyleUtils.Role.RowPreview : StyleUtils.Role.Row, out context.ResolutionOrder );

			// JJD 1/10/02
			// Set the isCard flag on the context
			//
			if ( this.IsCard )
				context.IsCard = true;

			// SSP 5/18/06 - App Styling
			// Enclosed the existing code into try-finally so we can assign the ref requestedProps param 
			// the correct value at the end.
			// 
			try
			{
				// resolve active row apperances first
				//

                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled)
                //// SSP 4/1/05 - Optimizations
                //// Use the IsActiveRowInternal which does not go through the process of verifying 
                //// that the active row is valid.
                ////
                ////if ( this.IsActiveRow )
                //if ( this.IsActiveRowInternal )
                if (band.ActiveAppearancesEnabledResolved && this.IsActiveRowInternal )
				{
					context.IsActiveRow = true;

					this.band.ResolveAppearance( ref appData, ref context );

					if ( 0 == context.UnresolvedProps )
						return;

					context.IsActiveRow = false;
				}

				UltraGrid grid = layout.Grid as UltraGrid;

				// SSP 1/6/03 Optimization.
				// Don't check to see if the row is in the selected rows collection. Checking Selected
				// propery should be enough because they are synchronized everytime the selection changes.
				// So there is no need to check for the row being in the selected rows collection if the
				// row is not selected.
				//
                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (SelectedAppearancesEnabled)
                ////if (  this.Selected || ( null != grid && grid.Selected.Rows.Contains( this ) ) )
                //if ( this.Selected )
                if ( band.SelectedAppearancesEnabledResolved && this.Selected )
                {
					context.IsSelected = true;
					band.ResolveAppearance( ref appData, ref context );

					if (context.UnresolvedProps == 0)
						return;

					context.IsSelected = false;
				}

				if ( previewArea )
				{
					// merge in the row's PreviewAppearance values (if any)
					//
					if ( this.previewAppearance != null )
					{
						// SSP 3/12/06 - App Styling
						// Added if condition to the existing code.
						// 
						if ( context.ResolutionOrder.UseControlInfo )
						{
							// JJD 12/12/02 - Optimization
							// Call the Appearance object's MergeData method instead so
							// we don't make unnecessary copies of the data structure
							//AppearanceData.MergeAppearance( ref appData, this.previewAppearance.Data, 
							//	ref context.UnresolvedProps );
							this.previewAppearance.MergeData( ref appData, ref context.UnresolvedProps );

							if ( context.UnresolvedProps == 0 )
								return;					
						}
					}

					// apply any override settings
					//
					context.IsRowPreview = true;
					this.band.ResolveAppearance( ref appData,ref context );
				
					if ( context.UnresolvedProps == 0 )
						return;

					context.IsRowPreview = false;
				}

				// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
				// 
				if ( hotTrackingRow )
				{
					band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.HotTrackRowAppearance
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						, AppStyling.RoleState.HotTracked );
				}
			
				// SSP 12/15/04 - IDataErrorInfo Support
				// 
				// ----------------------------------------------------------------------------
				if ( this.HasDataError( ) )
				{
					// Do not resolve the image on the data error appearances. We don't want 
					// the editors to display this image. The DataErrorTooltipUIElement which
					// would be a sibling of the editor element in the cell will resolve and 
					// position the data error image itself.
					//
					AppearancePropFlags imageFlag = DATA_ERROR_APPEARANCE_EXCLUDE_PROPS & context.UnresolvedProps;
					context.UnresolvedProps ^= imageFlag;

					band.MergeBLOverrideAppearances( ref appData, ref context, 
						UltraGridOverride.OverrideAppearanceIndex.DataErrorRowAppearance
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						, AppStyling.RoleState.DataError );

					context.UnresolvedProps ^= imageFlag;
				}
				// ----------------------------------------------------------------------------
			
				//	if fPreviewOnly is true, get out here, since
				//	we do not want the Row.Appearance in this case
				

				// merge in the row's Appearance values (if any)
				//
				if ( null != this.appearance )
				{
					// SSP 3/12/06 - App Styling
					// Added if condition to the existing code.
					// 
					if ( context.ResolutionOrder.UseControlInfo )
					{
						// JJD 12/12/02 - Optimization
						// Call the Appearance object's MergeData method instead so
						// we don't make unnecessary copies of the data structure
						//AppearanceData.MergeAppearance( ref appData, 
						//	this.appearance.Data,
						//	ref context.UnresolvedProps );
						this.appearance.MergeData( ref appData, ref context.UnresolvedProps );

						if ( 0 == context.UnresolvedProps )
							return;
					}
				}

				// SSP 11/11/03 Add Row Feature
				// Apply template add-row appearance.
				//
				// ----------------------------------------------------------------------------------
				bool shouldApplyTemplateAddRowAppearance = this.ShouldApplyTemplateAddRowAppearance( );
				bool shouldApplyAddRowAppearance = this.ShouldApplyAddRowAppearance( );
				bool addRowFeature_morphBackColor = false;
				if ( shouldApplyTemplateAddRowAppearance )
				{
					band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.TemplateAddRowAppearance
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						, AppStyling.RoleState.TemplateAddRow );

					addRowFeature_morphBackColor = ! appData.HasPropertyBeenSet( AppearancePropFlags.BackColor );
				}
				else if ( shouldApplyAddRowAppearance )
				{
					band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.AddRowAppearance
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						, AppStyling.RoleState.AddRow );

					// SSP 4/2/04 UWG2985
					// Changed the behavior of AddRowAppearance. Shade the cell back color to use with the
					// add-row only if template add-row functionality is turned on. When add-row feature 
					// is not turned on and the user adds a new row from outside, we don't want to all of
					// a sudded start showing the added row as gray where as in version 3.0 we used to
					// show it the same back color as the regular rows.
					//
					//addRowFeature_morphBackColor = ! appData.HasPropertyBeenSet( AppearancePropFlags.BackColor );
					if ( TemplateAddRowLocation.None != this.ParentCollection.TemplateRowLocationDefault )
						addRowFeature_morphBackColor = ! appData.HasPropertyBeenSet( AppearancePropFlags.BackColor );
				}
				// ----------------------------------------------------------------------------------
			
				// SSP 8/1/03 UWG1654 - Filter Action
				// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell  appearances.
				// Resolve the filter related appearances.
				// Resolve filter row appearance if we have filters.
				//
				// ------------------------------------------------------------------------------------------------
				// SSP 11/11/03 Add Row Feature
				// Don't apply filter related appearances to template add-rows since they do not qualify
				// for filter evaluations and thus they are neither filtered in nor filtered out.
				//
				//if ( this.HasFilters )
				// SSP 7/11/05 - NAS 5.3 Empty Rows
				// 
				//bool hasFilters = this.HasFilters;
				bool isEmptyRow = this.IsEmptyRow;
				bool hasFilters = this.HasFilters && ! isEmptyRow;

				if ( hasFilters && ! shouldApplyTemplateAddRowAppearance && ! shouldApplyAddRowAppearance )
				{
					bool isFilteredOut = this.IsFilteredOut;

					UltraGridOverride.OverrideAppearanceIndex filterAppIndex = 
						isFilteredOut
						? UltraGridOverride.OverrideAppearanceIndex.FilteredOutRowAppearance 
						: UltraGridOverride.OverrideAppearanceIndex.FilteredInRowAppearance;

					band.MergeBLOverrideAppearances( ref appData, ref context, filterAppIndex
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						, isFilteredOut ? AppStyling.RoleState.FilteredOut : AppStyling.RoleState.FilteredIn );
				}
				// ------------------------------------------------------------------------------------------------

				// SSP 3/29/05 - NAS 5.2 Fixed Rows/Filter Row
				//
				// ------------------------------------------------------------------------------------------------
				bool shouldApplyFixedAppearance = this.ShouldApplyFixedAppearance( );
				if ( shouldApplyFixedAppearance )
				{
					band.MergeBLOverrideAppearances( ref appData, ref context,
						UltraGridOverride.OverrideAppearanceIndex.FixedRowAppearance 
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						, AppStyling.RoleState.FixedRow );
				}

				bool isFilterRow = this.IsFilterRow;
				if ( isFilterRow )
				{
					// Apply the active filter row appearance if there are any filters.
					//
					if ( hasFilters )
					{
						band.MergeBLOverrideAppearances( ref appData, ref context,
							UltraGridOverride.OverrideAppearanceIndex.FilterRowAppearanceActive
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, AppStyling.RoleState.HasActiveFilters );
					}

					band.MergeBLOverrideAppearances( ref appData, ref context,
						UltraGridOverride.OverrideAppearanceIndex.FilterRowAppearance
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						, AppStyling.RoleState.FilterRow );

				}
				// ------------------------------------------------------------------------------------------------

				// Instead of calling the merge overrides for the bands cell and row appearances
				// followed by a call to the layout's resolve appearance, just call the
				// band's ResolveAppearance method first with the alternate row flag set
				// (for alternate rows) then without the flag set
				// SSP 5/10/04 - Optimizations
				// Added HasAlternateAppearances property. We don't want to go through the
				// visible index calculations to figure out if a row is an alternate row 
				// or not if there are no alternate row appearances.
				//
				//if ( this.IsAlternate )
				// SSP 5/18/06 - App Styling
				// Also take into account if the role has any AlternateItem state appearance settings.
				// 
				//if ( band.HasAlternateAppearances && this.IsAlternate )
				bool applyAlternateAppearances = band.HasBLOverrideAppearance( 
						UltraGridOverride.OverrideAppearanceIndex.RowAlternate, 
						context.Role, context.ResolutionOrder, AppStyling.RoleState.AlternateItem )
					&& this.IsAlternate;
				if ( applyAlternateAppearances )
				{
					context.IsAlternate = true;

					this.band.ResolveAppearance(ref appData, ref context );
					if ( 0 == context.UnresolvedProps )
						return;

					context.IsAlternate = false;
				}

				// SSP 7/11/05 - NAS 5.3 Empty Rows
				// 
				// ------------------------------------------------------------------------------------------------
				if ( isEmptyRow )
				{
					// SSP 3/9/06 - App Styling
					// 
					//layout.EmptyRowSettings.ResolveRowAppearance( ref appData, ref context.UnresolvedProps );
					layout.EmptyRowSettings.ResolveRowAppearance( ref appData, ref context );
				}
				// ------------------------------------------------------------------------------------------------

				// SSP 5/4/06 - App Styling
				// Resolve the CardView state appearances.
				// 
				if ( context.IsCard )
					StyleUtils.ResolveAppearance( AppStyling.RoleState.CardView, ref appData, ref context );

				// SSP 5/18/06 - App Styling
				// Added an overload that takes in resolveOnlyRowAppearances.
				// 
				// ------------------------------------------------------------------------
				if ( resolveOnlyRowAppearances )
				{
					band.MergeBLOverrideAppearances( ref appData, ref context, 
						UltraGridOverride.OverrideAppearanceIndex.Row, AppStyling.RoleState.Normal );

					return;
				}
				// ------------------------------------------------------------------------

				band.ResolveAppearance (ref appData, ref context );

				// finally call ResolveColors to make sure we have a valid
				// fore and back color
				//
				// AS - 11/21/01 UWG579
				// Should resolve to window backcolor and not control by default.
				// 11/27/01 Nevermind. :-)
				//			this.band.Layout.ResolveColors( ref appData, ref context, SystemColors.Control, 
				//				SystemColors.WindowFrame, SystemColors.WindowText, false );
				layout.ResolveColors( ref appData, ref context, SystemColors.Control, 
					SystemColors.WindowFrame, SystemColors.WindowText, false );

				// SSP 11/11/03 Add Row Feature
				//
				// --------------------------------------------------------------------------------------------
				if ( addRowFeature_morphBackColor && appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
					appData.BackColor = UltraGridLayout.GetColorShade( appData.BackColor, shouldApplyAddRowAppearance ? 25 : 50 );
				// --------------------------------------------------------------------------------------------
			}
			finally
			{
				// SSP 5/18/06 - App Styling
				// 
				requestedProps = context.UnresolvedProps;
			}
		}
		

		/// <summary>
		/// Resolves all of a cell's appearance properties
		/// </summary>
		/// <param name="column">The associated column.</param>
		/// <param name="appData">The structure to contain the resolved appearance.</param>
		public void ResolveCellAppearance( Infragistics.Win.UltraWinGrid.UltraGridColumn column, 
			ref AppearanceData appData )
		{
			this.ResolveCellAppearance( column, ref appData, AppearancePropFlags.AllRender );
		}
		
		/// <summary>
		/// Resolves selected properties of a cell's appearance
		/// </summary>
		/// <param name="column">The associated column.</param>
		/// <param name="appData">The structure to contain the resolved appearance.</param>
		/// <param name="requestedProps">Bit flags indicating which properties to resolve.</param>
		public void ResolveCellAppearance( Infragistics.Win.UltraWinGrid.UltraGridColumn column,
			ref AppearanceData appData,												 
			AppearancePropFlags requestedProps)
		{
			this.ResolveCellAppearance( column, ref appData, requestedProps, false );
		}

		internal void ResolveCellAppearance( Infragistics.Win.UltraWinGrid.UltraGridColumn column,
			ref AppearanceData appData,												 
			AppearancePropFlags requestedProps,
			bool cellAppearanceOnly )
		{
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingCell and hotTrackingRow parameters.
			// 
			//this.ResolveCellAppearance( column, ref appData, ref requestedProps, cellAppearanceOnly, false, false );
			// SSP 8/18/05 BR05562
			// Added forceActive parameter.
			// 
			//this.ResolveCellAppearance( column, ref appData, ref requestedProps, cellAppearanceOnly, false, false, false, false );
            //
            // MBS 5/5/08 - RowEditTemplate NA2008 V2
            // Added isProxyResolution parameter
            //
			//this.ResolveCellAppearance( column, ref appData, ref requestedProps, cellAppearanceOnly, false, ForceActive.None, false, false, false );
            this.ResolveCellAppearance(column, ref appData, ref requestedProps, cellAppearanceOnly, false, ForceActive.None, false, false, false, false);
		}
		
		// SSP 11/3/04 - Merged Cell Feature
		//
		/// <summary>
		/// Resolves appearance for the merged cell associated with this cell.
		/// </summary>
		/// <param name="column"></param>
        /// <param name="appData">The structure to contain the resolved apperance.</param>
        /// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
        public void ResolveMergedCellAppearance( 
			UltraGridColumn column, ref AppearanceData appData, ref AppearancePropFlags requestedProps )
		{
			this.ResolveMergedCellAppearance( column, ref appData, ref requestedProps, CellState.None );
		}

		// SSP 11/3/04 - Merged Cell Feature
		//
		/// <summary>
		/// Resolves appearance for the merged cell associated with this cell.
		/// </summary>
		/// <param name="column">The associated column.</param>
        /// <param name="appData">The structure to contain the resolved apperance.</param>
        /// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
        /// <param name="cellState"></param>
		internal void ResolveMergedCellAppearance( UltraGridColumn column, ref AppearanceData appData, 
			ref AppearancePropFlags requestedProps,
			// SSP 9/14/05 BR05562
			// Added cellState parameter.
			// 
			//bool forceSelected,
			CellState cellState )
		{
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingCell and hotTrackingRow parameters.
			// 
			//this.ResolveCellAppearance( column, ref appData, ref requestedProps, false, forceSelected, true );
			// SSP 8/18/05 BR05562
			// Added forceActive parameter.
			// 
			//this.ResolveCellAppearance( column, ref appData, ref requestedProps, false, forceSelected, true, false, false );
			ForceActive forceActive = ForceActive.None;
			if ( 0 != ( CellState.ActiveCell & cellState ) )
				forceActive |= ForceActive.Cell;

			if ( 0 != ( CellState.ActiveRow & cellState ) )
				forceActive |= ForceActive.Row;

            // MBS 5/5/08 - RowEditTemplate NA2008 V2
            // Added isProxyResolution parameter
            //
            //this.ResolveCellAppearance( column, ref appData, ref requestedProps, false, 
            //    0 != ( CellState.Selected & cellState ),
            //    forceActive, 
            //    true,
            //    0 != ( CellState.HotTrackedCell & cellState ),
            //    0 != ( CellState.HotTrackedRow & cellState ) 
            //    );
            this.ResolveCellAppearance(column, ref appData, ref requestedProps, false,
                0 != (CellState.Selected & cellState),
                forceActive,
                true,
                0 != (CellState.HotTrackedCell & cellState),
                0 != (CellState.HotTrackedRow & cellState),
                false
                );
		}

		#region ResolveEditorCellValueAppearance

		// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
		// Added ResolveEditorCellValueAppearance helper method. Code in there was moved from the
		// ResolveCellAppearance method.
		// 
		internal void ResolveEditorCellValueAppearance( UltraGridColumn column, 
			ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			// SSP 4/29/02
			// EM Embeddable editors.
			// Value list appearances will be resolved by the editors.
			//
			// SSP 10/15/02 UWG1754
			// Apply the value list item appearance as well.
			// ----------------------------------------------------------------------
			// SSP 10/16/02 
			// Commenting out the code for now until we can optimize
			//
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			EmbeddableEditorBase editor = column.GetEditor( this );
			// SSP 7/11/05 - NAS 5.3 Empty Rows
			// 
			//if ( null != editor && editor.SupportsValueList )
			if ( null != editor && editor.SupportsValueList )
			{
				object dataVal = this.GetCellValue( column );

				UltraGridCell cell = this.GetCellIfAllocated( column );

				if ( null != cell && cell.IsInEditMode && editor.IsInEditMode  )
				{
					try
					{
						// JAS 3/31/05 BR02713 - Added the if() to prevent a first chance exception.
						//
						if( editor.IsValid )
							dataVal = editor.Value;
					}
					catch ( Exception )
					{
						//Debug.Assert( false, "Unexpected exception thrown: " + e.Message );
					}
				}

				editor.ApplyValueAppearance( 
					// SSP 4/18/05 - NAS 5.2 Filter Row
					// Use the new EditorOwnerInfo property of the cell because filter row cells 
					// use different owners.
					//
					//ref appData, ref context.UnresolvedProps, dataVal, column.EditorOwnerInfo, this );
					ref appData, ref flags, dataVal, this.GetEditorOwnerInfo( column ), this );
			}
			// ----------------------------------------------------------------------
		}

		#endregion // ResolveEditorCellValueAppearance

		// SSP 4/1/02
		// Added an overload that takes in the requestedProps as a reference rather than by
		// value. This is necessary for the embeddable editors as they rely on requestedProps
		// to determine what was resolved by the owner and what properties they need to provide
		// default values for.
		//
		internal void ResolveCellAppearance( Infragistics.Win.UltraWinGrid.UltraGridColumn column,
			ref AppearanceData appData,												 
			ref AppearancePropFlags requestedProps,
			bool cellAppearanceOnly,
			bool forceSelected,
			// SSP 8/18/05 BR05562
			// Added forceActive parameter.
			// 
			ForceActive forceActive,
			// SSP 11/3/04 - Merged Cell Feature
			// Added isMergedCell parameter.
			//
			bool isMergedCell,
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingCell and hotTrackingRow parameters.
			// 
			bool hotTrackingCell, bool hotTrackingRow,
            // MBS 5/5/08 - NA2008 V2 RowEditTemplate
            // Added additional parameter so that we can distinguish between proxies
            // so that we can ignore certain states
            bool isProxyResolution)
		{
			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridCell ), requestedProps );
			UltraGridBand band = this.band;
			UltraGridLayout layout = band.Layout;

			// SSP 3/8/06 - App Styling
			// 
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( layout, StyleUtils.Role.Cell, out order );
			AppStyling.UIRole rowRole = StyleUtils.GetRole( layout, StyleUtils.Role.Row );
			context.Role = role;
			context.ResolutionOrder = order;

			// SSP 4/17/03
			// Before returning we have to clear the resolved prop flags in the requestedProps
			// ref parameter. Enclosed in the try-finally block and in the finally block
			// put the code to do so.
			//
			try
			{
				// JJD 1/10/02
				// Set the isCard flag on the context
				//
				if ( this.IsCard )
					context.IsCard = true;

				// SSP 7/25/03
				// Commented this code out. We need to do multiple passes to resolve cell appearance.
				// (For example, first we resolved the active cell appearance, then active row, then
				// selected row then group-by column and then fixed cell appearance etc...). So setting
				// the IsGroupByColumn is incorrect because then Band.ResolveAppearance and 
				// Layout.ResolveAppearance methods will not look at any other appearances if IsGroupBy
				// is true. Look in those methods for clarification (series of if-else-if blocks).
				//
				

				// SSP 7/11/05 - NAS 5.3 Empty Rows
				// 
				bool isEmptyRow = this.IsEmptyRow;

				// JJD 10/31/01
				// Added appearance cache off the columns to cache standard and selected
				// appearances during a draw operation. Only cache if the cellAppearanceOnly
				// and forceSelected flags are false and the requestor is asking for allrender.
				//
				bool cacheThisAppearanceForColumn = !cellAppearanceOnly	&& 
					!forceSelected		&&
					requestedProps == AppearancePropFlags.AllRender
					// SSP 11/4/04 Merged Cell Feature
					// If we are resolving the appearance for a merged cell then don't cache 
					// the appearance.
					//
					&& ! isMergedCell
					// SSP 7/11/05 - NAS 5.3 Empty Rows
					// 
					&& ! isEmptyRow
					// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
					// 
					&& ! hotTrackingRow && ! hotTrackingCell;

				UltraGridCell cell = null;

				// See if the cell has been created. If so, get the
				// cell reference for use below
				//
				// SSP 7/22/03 - Optimizations
				// Use the GetCellIfAllocated method instead for greater effieciency.
				//
				// --------------------------------------------------------------------------
				
				cell = null != column ? this.GetCellIfAllocated( column ) : null;
				// --------------------------------------------------------------------------

				// if the cell is in edit mode resolve the edit cell
				// appearance settings
				//
				// SSP 11/4/04 Merged Cell Feature
				// Don't resolve edit cell appearance for merged cells.
				//
				//if ( null != cell && cell.IsInEditMode )
				if ( null != cell && cell.IsInEditMode && ! isMergedCell )
				{
					context.IsInEdit = true;
				
					band.ResolveAppearance( ref appData, ref context );

					if ( context.UnresolvedProps == 0)
						return;
				
					context.IsInEdit = false;
				
					// JJD 11/29/01 - UWG724
					// Don't allow gradient fills if we are in edit mode
					// since we can't support it in the edit control.
					// 
					// SSP 1/2/02 UWG725
					// Only clear the gradient style if the requestedProps specifies
					// to resolve BackGradientStyle. (ValueList drop down actually
					// calls this method after resolving value list item and value appearance
					// to resolve the active cell's appearance. This always set the 
					// backgradientstyle to none which we don't want.)
					//
					// Added if condition 
					if ( 0 != ( AppearancePropFlags.BackGradientStyle & context.UnresolvedProps ) )
					{
						appData.BackGradientStyle = GradientStyle.None;
						context.UnresolvedProps &= ~AppearancePropFlags.BackGradientStyle; 
					}

					// JJD 10/31/01
					// We don't want to use the cached column appearance
					// if we are in edit mode
					//
					cacheThisAppearanceForColumn = false;
				}

				// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
				// Added hotTrackingCell and hotTrackingRow parameters.
				// 
				// ----------------------------------------------------------------------------
				if ( hotTrackingCell )
				{
					band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.HotTrackCellAppearance,
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						AppStyling.RoleState.HotTracked );
				}

				if ( hotTrackingRow )
				{
					band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.HotTrackRowCellAppearance,
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						AppStyling.RoleState.RowHotTracked );
				}
				// ----------------------------------------------------------------------------

                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
                bool allowActiveAppearances = band.ActiveAppearancesEnabledResolved;
                bool allowSelectedAppearances = band.SelectedAppearancesEnabledResolved;                

				// if the Cell is active resolve the active Cell appearances
				//
				// SSP 11/4/04 Merged Cell Feature
				// Don't apply active cell/row appearance to merged cells.
				//
				//if ( ( null != cell && cell.IsActiveCell ) || this.IsActiveRow )
				// SSP 4/1/05 - Optimizations
				// In order for a cell to be active its row must be active. Also use the 
				// IsActiveRowInternal which does not go through the process of verifying that the
				// active row is valid.
				//
				//if ( ! isMergedCell && ( ( null != cell && cell.IsActiveCell ) || this.IsActiveRow ) )                
				bool isActiveRow = this.IsActiveRowInternal;

                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled)
                //// SSP 8/18/05 BR05562
                //// Added forceActive parameter.
                //// 
                ////if ( ! isMergedCell && isActiveRow )
                //if ( ! isMergedCell && isActiveRow || ForceActive.None != forceActive )
                if ( allowActiveAppearances && (! isMergedCell && isActiveRow || ForceActive.None != forceActive ))
				{
					// SSP 8/18/05 BR05562
					// Added forceActive parameter.
					// 
					//context.IsActiveCell = null != cell && cell.IsActiveCell;
					context.IsActiveCell = null != cell && cell.IsActiveCell || 0 != ( ForceActive.Cell & forceActive );

					if( cellAppearanceOnly )
					{
						context.IsCellOnly = true;
					}

					else
					{
						// Don't merge the row appearance on a cell only pass
						//
						// SSP 4/1/05 - Optimizations
						//
						//context.IsActiveRow = this.IsActiveRow;

						// SSP 8/18/05 BR05562
						// Added forceActive parameter.
						// 
						//context.IsActiveRow = isActiveRow;
                        //
                        // MBS 5/5/08 - RowEditTemplate NA2008 V2
                        // We don't want to have the active row appearance resolved for the proxies
                        //
						//context.IsActiveRow = isActiveRow || 0 != ( ForceActive.Row & forceActive );
                        if(!isProxyResolution)
                            context.IsActiveRow = isActiveRow || 0 != (ForceActive.Row & forceActive);

						context.IsCellOnly =  false;
					}

                    // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearance)
                    // Added ActiveAppearance property on the cell object.
                    // 
                    if (null != cell && cell.HasActiveAppearance && context.IsActiveCell)
                        cell.ActiveAppearance.MergeData(ref appData, ref context.UnresolvedProps);


					band.ResolveAppearance( ref appData, ref context);

					if( context.UnresolvedProps ==0 )
					{
						return;
					}

					context.IsActiveCell = false;
					context.IsActiveRow  = false;				

					// JJD 10/31/01
					// We don't want to use the cached column appearance
					// for the active row
					//
					cacheThisAppearanceForColumn = false;
				
				}

				bool selected = false;

                //// SSP 11/4/04 Merged Cell Feature
                //// Don't apply selected cell/row appearance to merged cells.
                ////
                ////if ( forceSelected || ( null != cell && cell.Selected ) || this.Selected || column.Header.Selected)
                //// SSP 8/10/05 - NAS 5.3 Empty Rows
                //// Don't apply the selected appearance to cells of empty rows.
                //// 
                ////if ( forceSelected || ! isMergedCell && ( null != cell && cell.Selected || this.Selected ) || column.Header.Selected )
                ////
                //// MBS 5/5/08 - RowEditTemplate NA2008 V2
                //// Don't apply the selected appearqance to the proxies
                ////
                ////if ( ( forceSelected || ! isMergedCell && ( null != cell && cell.Selected || this.Selected ) || column.Header.Selected ) && ! isEmptyRow )                
                ////
                //if ((forceSelected || !isMergedCell && (null != cell && cell.Selected || this.Selected) || column.Header.Selected) && !isEmptyRow && !isProxyResolution &&
                if (allowSelectedAppearances && ((forceSelected || !isMergedCell && (null != cell && cell.Selected || this.Selected) || column.Header.Selected) && !isEmptyRow && !isProxyResolution &&
                    // MBS 4/14/09 - NA9.2 Selection Overlay
                    // We don't want to resolve the selected appearance if we're going to be drawing a selection overlay.
                    // Note that we only need to check the resolved border color, since this takes into account whether
                    // or not the SelectionOverlayColor has been set and will return a color if so, or if it has been set itself.
                    layout.SelectionOverlayBorderColorResolved == Color.Empty))
				{
					selected = true;

					// SSP 8/26/06 - NAS 6.3
					// Added SelectedAppearance property on the cell object.
					// 
					if ( null != cell && cell.HasSelectedAppearance )
						cell.SelectedAppearance.MergeData( ref appData, ref context.UnresolvedProps );

					context.IsSelected = true;
				
					band.ResolveAppearance ( ref appData, ref context );

					if (context.UnresolvedProps == 0 )
					{
						return;
					}

					context.IsSelected = false;

				}

				// SSP 12/15/04 - IDataErrorInfo Support
				// 
				// ----------------------------------------------------------------------------
				// SSP 1/20/05 BR01805
				//
				bool rowHasDataError = this.HasDataError( );
				if ( rowHasDataError )
					cacheThisAppearanceForColumn = false;

				if ( ! isMergedCell && this.HasDataError( column ) )
				{
					// Do not resolve the image on the data error appearances. We don't want 
					// the editors to display this image. The DataErrorTooltipUIElement which
					// would be a sibling of the editor element in the cell will resolve and 
					// position the data error image itself.
					//
					AppearancePropFlags imageFlag = DATA_ERROR_APPEARANCE_EXCLUDE_PROPS & context.UnresolvedProps;
					context.UnresolvedProps ^= imageFlag;

					band.MergeBLOverrideAppearances( ref appData, ref context, 
						UltraGridOverride.OverrideAppearanceIndex.DataErrorCellAppearance,
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						AppStyling.RoleState.DataError );

					context.UnresolvedProps ^= imageFlag;

					cacheThisAppearanceForColumn = false;
				}
				// ----------------------------------------------------------------------------

				// merge in the cell's Appearance values (if any)
				//
				if ( null != cell && cell.HasAppearance )
				{
					// SSP 3/8/06 - App Styling
					// Added if condition to the existing line of code.
					// 
					if ( order.UseControlInfo )
					{
						// JJD 12/12/02 - Optimization
						// Call the Appearance object's MergeData method instead so
						// we don't make unnecessary copies of the data structure
						//AppearanceData.MergeAppearance( ref appData, 
						//	cell.Appearance.Data,
						//	ref context.UnresolvedProps );
						cell.Appearance.MergeData( ref appData, ref context.UnresolvedProps );

						if ( 0 == context.UnresolvedProps )
							return;

						// JJD 10/31/01
						// We don't want to use the cached column appearance
						// if the cell has an override appearance
						//
						cacheThisAppearanceForColumn = false;
					}
				}

				// SSP 12/6/01
				// If column has a value list, then get the cell because
				// we need the cell to get the value list item for resolving
				// image.
				// 
				if ( null == cell && column.HasValueList )
				{
					cell = this.Cells[column];
				}

				// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
				// Added ResolveEditorCellValueAppearance helper method. The commented out code was moved
				// into that method.
				// 
				// ------------------------------------------------------------------------------------------
				if ( ! isEmptyRow )
				{
					AppearancePropFlags oldFlags = context.UnresolvedProps;

					this.ResolveEditorCellValueAppearance( column, ref appData, ref context.UnresolvedProps );

					// If a value list item had appearance set, then don't cache it.
					//
					if ( oldFlags != context.UnresolvedProps )
						cacheThisAppearanceForColumn = false;
				}
				
				// ------------------------------------------------------------------------------------------

				
			

				// SSP 7/24/04 - UltraCalc
				// Resolve CellErrorAppearance.
				//
				// ----------------------------------------------------------------------
				// SSP 7/11/05 - NAS 5.3 Empty Rows
				// 
				//if ( null != column && column.HasActiveFormula )
				if ( null != column && column.HasActiveFormula && ! isEmptyRow )
				{
					cell = this.Cells[ column ];
					if ( cell.HasFormulaCalcError )
					{
						cacheThisAppearanceForColumn = false;

						context.IsFormulaError = true;

						// SSP 3/8/06 - App Styling
						// 
						//if ( column.HasFormulaErrorAppearance )
						if ( column.HasFormulaErrorAppearance && order.UseControlInfo )
							column.FormulaErrorAppearance.MergeData( ref appData, ref context.UnresolvedProps );

						band.ResolveAppearance( ref appData, ref context );

						context.IsFormulaError = false;

						// SSP 1/20/05
						//
						cacheThisAppearanceForColumn = false;
					}
				}
				// ----------------------------------------------------------------------

				// merge in the row's CellAppearance values (if any)
				//
				if ( this.HasCellAppearance )
				{
					// SSP 3/8/06 - App Styling
					// Added if condition to the existing line of code.
					// 
					if ( order.UseControlInfo )
					{
						// JJD 12/12/02 - Optimization
						// Call the Appearance object's MergeData method instead so
						// we don't make unnecessary copies of the data structure
						//AppearanceData.MergeAppearance( ref appData, 
						//	this.cellAppearance.Data,
						//	ref context.UnresolvedProps );
						this.cellAppearance.MergeData( ref appData, ref context.UnresolvedProps );

						if ( 0 == context.UnresolvedProps )
							return;

						// JJD 10/31/01
						// We don't want to use the cached column appearance
						// if the row has an override appearance
						//
						cacheThisAppearanceForColumn = false;
					}
				}

                // MBS 11/15/06 - NAS7.1 Conditional Formatting
                if (column.ValueBasedAppearance != null && IsDataRow)
                {
                    // MBS 12/21/06
                    CellContextProvider cellContextProvider = new CellContextProvider(this, column);

                    // MBS 1/25/07 
                    // Pull in the appropriate filter or valuelist value to use for evaluating matches
                    //column.ValueBasedAppearance.ResolveAppearance(ref appData, ref context.UnresolvedProps, this.GetCellValue(column), cellContextProvider);
                    object comparisonValue = UltraGridBand.GetValueForFilterComparision(this, column);
                    column.ValueBasedAppearance.ResolveAppearance(ref appData, ref context.UnresolvedProps, comparisonValue, cellContextProvider);
                    
                    cacheThisAppearanceForColumn = false;
                }
			
				// JJD 10/31/01
				// We don't want to use the cached column appearance
				// if the row has override appearance
				//
				if ( cacheThisAppearanceForColumn && this.HasAppearance )
					cacheThisAppearanceForColumn = false;

				// SSP 8/1/03 UWG1654 - Filter Action
				// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell  appearances.
				// If the row is filtered out and there are filter row/cell appearances, then don't cache
				// the appearance.
				// Resolve filter row appearance if we have filters and filters evaluate to true for this
				// row (meaning the row is not filtered out).
				// ----------------------------------------------------------------------------------------------
				// SSP 7/11/05 - NAS 5.3 Empty Rows
				// 
				//bool hasFilters = this.HasFilters;
				bool hasFilters = this.HasFilters && ! isEmptyRow;
				bool applyFilterAppearance = hasFilters;
				// SSP 11/11/03 Add Row Feature
				// Don't apply filter related appearances to template add-rows since they do not qualify
				// for filter evaluations and thus they are neither filtered in nor filtered out.
				//
				// ----------------------------------------------------------------------------
				bool shouldApplyTemplateAddRowAppearance = this.ShouldApplyTemplateAddRowAppearance( );
				bool shouldApplyAddRowAppearance = this.ShouldApplyAddRowAppearance( );

				if ( shouldApplyTemplateAddRowAppearance || shouldApplyAddRowAppearance )
					cacheThisAppearanceForColumn = applyFilterAppearance = false;
				// ----------------------------------------------------------------------------
				bool isFilteredOut = this.IsFilteredOut;
				if ( applyFilterAppearance && cacheThisAppearanceForColumn )
				{
					cacheThisAppearanceForColumn = 
						! band.HasBLOverrideAppearance( 
							isFilteredOut 
							? UltraGridOverride.OverrideAppearanceIndex.FilteredOutRowAppearance 
							: UltraGridOverride.OverrideAppearanceIndex.FilteredInRowAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, rowRole, order, isFilteredOut ? AppStyling.RoleState.FilteredOut : AppStyling.RoleState.FilteredIn
							)
						&& ! band.HasBLOverrideAppearance( 
							isFilteredOut 
							? UltraGridOverride.OverrideAppearanceIndex.FilteredOutCellAppearance 
							: UltraGridOverride.OverrideAppearanceIndex.FilteredInCellAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, role, order, isFilteredOut ? AppStyling.RoleState.FilteredOut : AppStyling.RoleState.FilteredIn
							);
				}
				// ----------------------------------------------------------------------------------------------

				// SSP 3/29/05 - NAS 5.2 Fixed Rows/Filter Row
				//
				// ------------------------------------------------------------------------------------------------
				bool shouldApplyFixedRowAppearance = this.ShouldApplyFixedAppearance( );
				if ( shouldApplyFixedRowAppearance && cacheThisAppearanceForColumn )
				{
					cacheThisAppearanceForColumn = ! band.HasBLOverrideAppearance( 
							UltraGridOverride.OverrideAppearanceIndex.FixedRowAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, rowRole, order, AppStyling.RoleState.FixedRow
						)
						// SSP 8/16/05 BR05670
						// 
						&& ! band.HasBLOverrideAppearance( 
							UltraGridOverride.OverrideAppearanceIndex.FixedRowCellAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, role, order, AppStyling.RoleState.FixedRow 
						);
				}

				bool isFilterRow = this.IsFilterRow;
				if ( isFilterRow && cacheThisAppearanceForColumn )
				{
					if ( hasFilters )
					{
						// Apply the active filter row appearance if there are any filters.
						//
						cacheThisAppearanceForColumn = ! band.HasBLOverrideAppearance( 
								UltraGridOverride.OverrideAppearanceIndex.FilterRowAppearanceActive
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, rowRole, order, AppStyling.RoleState.HasActiveFilters
							)
							// SSP 8/16/05 BR05670
							// 
							&& ! band.HasBLOverrideAppearance( 
								UltraGridOverride.OverrideAppearanceIndex.FilterCellAppearanceActive
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, role, order, AppStyling.RoleState.HasActiveFilters
							);
					}

					cacheThisAppearanceForColumn = cacheThisAppearanceForColumn
						&& ! band.HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex.FilterRowAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, rowRole, order, AppStyling.RoleState.FilterRow
						)
						// SSP 8/16/05 BR05670
						// 
						&& ! band.HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex.FilterCellAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, role, order, AppStyling.RoleState.FilterRow
					    )
						// SSP 5/19/05 BR04157
						//
						// SSP 3/13/06 - App Styling
						// Take into account the UseControlInfo setting.
						// 
						//&& ! column.HasFilterCellAppearance;
						&& ( ! column.HasFilterCellAppearance || ! order.UseControlInfo );
				}
				// ------------------------------------------------------------------------------------------------

				// JJD 11/06/01
				// If this is an alternate row and there are apperance
				// overrides set for the alternate rows then we can use
				// or set the cache.
				//
				// SSP 5/10/04 - Optimizations
				// Added HasAlternateAppearances property. We don't want to go through the
				// visible index calculations to figure out if a row is an alternate row 
				// or not if there are no alternate row appearances. Also store it into
				// a local variable so we don't have to access the property multiple times.
				//
				//bool isAlternate = this.IsAlternate;
				// SSP 3/13/06 - App Styling
				// 
				//bool isAlternate = band.HasAlternateAppearances && this.IsAlternate;
				bool isAlternate = 
					band.HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex.RowAlternate,
						rowRole, order, AppStyling.RoleState.AlternateItem )
					&& this.IsAlternate;

				// SSP 3/29/05 - Optimizations
				// HasAlternateAppearances method call above already checks for what the
				// commented out code checks for.
				//
				// --------------------------------------------------------------------------
				if ( cacheThisAppearanceForColumn && isAlternate )
					cacheThisAppearanceForColumn = false;
				
				// --------------------------------------------------------------------------

				// SSP 8/26/06 - NAS 6.3
				// Added ReadOnlyCellAppearance to the override.
				// 
				// --------------------------------------------------------------------
				bool applyReadOnlyCellAppearance = band.HasBLOverrideAppearance( 
					UltraGridOverride.OverrideAppearanceIndex.ReadOnlyCellAppearance, role, order, AppStyling.RoleState.ReadOnly )
					&& ! column.CanBeModified( this )
					// Only apply to data rows as it doesn't make sense to apply to empty rows.
					// 
					&& this.IsDataRow;

				if ( applyReadOnlyCellAppearance )
				{
					cacheThisAppearanceForColumn = false;

					band.MergeBLOverrideAppearances( ref appData, ref context, 
						UltraGridOverride.OverrideAppearanceIndex.ReadOnlyCellAppearance, AppStyling.RoleState.ReadOnly );
				}
				// --------------------------------------------------------------------

				// JJD 10/31/01
				// Use the previously cached appearanceData instead of rebuilding
				// it - optimization.
				//
				if ( cacheThisAppearanceForColumn )
				{
                    if (column.GetCachedCellAppearance(selected, ref appData, ref context))
                        return;
				}

				try 
				{
					// SSP 11/4/04 - Merged Cell Feature
					// Merge the merged cell appearance from the column.
					//
					if ( isMergedCell )
					{
						// SSP 3/8/06 - App Styling
						// Added if condition to the existing line of code.
						// 
						if ( order.UseControlInfo )
						{
							if ( column.HasMergedCellAppearance )
								column.MergedCellAppearance.MergeData( ref appData, ref context.UnresolvedProps );
						}
					}

					// SSP 10/2/06 BR16416
					// Merge in the FilterCellAppearance before the CellAppearance. Moved this here from below.
					// 
					if ( isFilterRow && column.HasFilterCellAppearance )
					{
						// SSP 3/8/06 - App Styling
						// Added if condition to the existing line of code.
						// 
						if ( order.UseControlInfo )
							column.FilterCellAppearance.MergeData( ref appData, ref context.UnresolvedProps );
					}
                    
					// merge in the column's appearance
					//
					column.ResolveAppearance ( ref appData, ref context );

					if ( context.UnresolvedProps == 0)
						return;

					// SSP 7/25/03
					// Added below code.
					//
					// --------------------------------------------------------------------------------------
					if ( column.Header.FixedResolved )
					{
						context.IsFixedColumn = true;
						band.ResolveAppearance( ref appData, ref context );
						context.IsFixedColumn = false;
					}

					if ( column.IsGroupByColumn && ViewStyleBand.OutlookGroupBy == layout.ViewStyleBand )
					{
						context.IsGroupByColumn = true;
						band.ResolveAppearance( ref appData, ref context );
						context.IsGroupByColumn = false;
					}
					// --------------------------------------------------------------------------------------

					// SSP 8/1/03 UWG1654 - Filter Action
					// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell  appearances.
					// Resolve the filter related appearances.
					//
					// ------------------------------------------------------------------------------------------------
					if ( applyFilterAppearance )
					{
						band.MergeBLOverrideAppearances( ref appData, ref context,
							isFilteredOut 
							? UltraGridOverride.OverrideAppearanceIndex.FilteredOutCellAppearance 
							: UltraGridOverride.OverrideAppearanceIndex.FilteredInCellAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, isFilteredOut ? AppStyling.RoleState.FilteredOut : AppStyling.RoleState.FilteredIn
							);

						if(context.UnresolvedProps == 0)
							return;
					}
					// ------------------------------------------------------------------------------------------------

					// SSP 12/11/03 Add Row Feature - UWG2766
					// Added AddRowCellAppearance and TemplateAddRowCellAppearance properties.
					//
					// ----------------------------------------------------------------------------------
					// -1 means positively don't morph, 1 means positively morph.
					// SSP 8/4/05 BR04897
					// Only shade the back color if it comes from the RowAppearance or CellAppearance
					// or ResolveColors. If someone specifies the DataErrorCellAppearance.BackColor then
					// we don't want to shade that if the add-row happens to have the data error.
					// 
					//int addRowFeature_morphBackColorFlag = 0;
					int addRowFeature_morphBackColorFlag = -1;
					if ( shouldApplyTemplateAddRowAppearance || shouldApplyAddRowAppearance )
					{
						// SSP 8/4/05 BR04897
						// Only shade the back color if it comes from the RowAppearance or CellAppearance
						// or ResolveColors. If someone specifies the DataErrorCellAppearance.BackColor then
						// we don't want to shade that if the add-row happens to have the data error.
						// 
						addRowFeature_morphBackColorFlag = 0;

						band.MergeBLOverrideAppearances( ref appData, ref context, 
							shouldApplyTemplateAddRowAppearance 
							? UltraGridOverride.OverrideAppearanceIndex.TemplateAddRowCellAppearance
							: UltraGridOverride.OverrideAppearanceIndex.AddRowCellAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, shouldApplyAddRowAppearance ?  AppStyling.RoleState.AddRow : AppStyling.RoleState.TemplateAddRow
							);

						// SSP 8/4/05 BR04897
						// Only shade the back color if it comes from the RowAppearance or CellAppearance
						// or ResolveColors. If someone specifies the DataErrorCellAppearance.BackColor then
						// we don't want to shade that if the add-row happens to have the data error.
						// 
						//if ( appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
						//	addRowFeature_morphBackColorFlag = -1;
					}
					// ----------------------------------------------------------------------------------

					// SSP 3/29/05 - NAS 5.2 Fixed Rows/Filter Row
					//
					// ------------------------------------------------------------------------------------------------
					if ( shouldApplyFixedRowAppearance )
					{
						band.MergeBLOverrideAppearances( ref appData, ref context,
							UltraGridOverride.OverrideAppearanceIndex.FixedRowCellAppearance 
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, AppStyling.RoleState.FixedRow );
					}

					if ( isFilterRow )
					{
						// Apply the active filter row appearance if there are any filters.
						//
						if ( hasFilters && ((UltraGridFilterRow)this).HasActiveFilters( column ) )
						{
							band.MergeBLOverrideAppearances( ref appData, ref context,
								UltraGridOverride.OverrideAppearanceIndex.FilterCellAppearanceActive
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, AppStyling.RoleState.HasActiveFilters );
						}

						// SSP 10/2/06 BR16416
						// Moved this above.
						// 
						

						band.MergeBLOverrideAppearances( ref appData, ref context,
							UltraGridOverride.OverrideAppearanceIndex.FilterCellAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, AppStyling.RoleState.FilterRow
							);
					}
					// ------------------------------------------------------------------------------------------------

					// SSP 7/11/05 - NAS 5.3 Empty Rows
					// 
					// ------------------------------------------------------------------------------------------------
					if ( isEmptyRow )
					{
						// SSP 3/8/06 - App Styling
						// 
						//layout.EmptyRowSettings.ResolveCellAppearance( ref appData, ref context.UnresolvedProps );
						layout.EmptyRowSettings.ResolveCellAppearance( ref appData, ref context );
					}
					// ------------------------------------------------------------------------------------------------

					if( !cellAppearanceOnly )
					{
						// SSP 3/8/06 - App Styling
						// Check for UseControlInfo.
						// 
						//if ( this.HasAppearance )
						if ( this.HasAppearance && order.UseControlInfo )
						{
							
							AppearancePropFlags byPassImgProps = context.UnresolvedProps & GridUtils.ImageBackgroundProps;
							context.UnresolvedProps &= ~GridUtils.ImageBackgroundProps;

							// JJD 12/12/02 - Optimization
							// Call the Appearance object's MergeData method instead so
							// we don't make unnecessary copies of the data structure
							//AppearanceData.MergeAppearance( ref appData,
							//	this.Appearance.Data,
							//	ref context.UnresolvedProps );
							this.Appearance.MergeData( ref appData, ref context.UnresolvedProps );

							// turn the imageBackround bit back on if it was set above
							//
							context.UnresolvedProps |= byPassImgProps;

							if ( context.UnresolvedProps == 0)
							{						
								return;
							}
						}
					}

					// SSP 11/4/04 - Merged Cell Feature
					//
					if ( isMergedCell )
					{
						band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.MergedCellAppearance
							// SSP 3/13/06 - App Styling
							// Use the new overload that takes in the app styling related params.
							// 
							, AppStyling.RoleState.MergedCell );
					}

					// SSP 8/4/05 BR04897
					// Only shade the back color if it comes from the RowAppearance or CellAppearance
					// or ResolveColors. If someone specifies the DataErrorCellAppearance.BackColor then
					// we don't want to shade that if the add-row happens to have the data error.
					// 
					if ( 0 == addRowFeature_morphBackColorFlag
						&& appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
						addRowFeature_morphBackColorFlag = -1;

					// SSP 5/4/06 - App Styling
					// Resolve the CardView state appearances.
					// 
					if ( context.IsCard )
						StyleUtils.ResolveAppearance( AppStyling.RoleState.CardView, ref appData, ref context );

					band.MergeBLOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Cell
						// SSP 3/13/06 - App Styling
						// Use the new overload that takes in the app styling related params.
						// 
						, AppStyling.RoleState.Normal );

					// SSP 8/4/05 BR04897
					// Only shade the back color if it comes from the RowAppearance or CellAppearance
					// or ResolveColors. If someone specifies the DataErrorCellAppearance.BackColor then
					// we don't want to shade that if the add-row happens to have the data error.
					// 
					if ( 0 == addRowFeature_morphBackColorFlag
						&& appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
						addRowFeature_morphBackColorFlag = 1;

					if( !cellAppearanceOnly )
					{
						// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
						// Added hotTrackingCell and hotTrackingRow parameters.
						// 
						if ( hotTrackingRow )
						{
							band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.HotTrackRowAppearance
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, rowRole, AppStyling.RoleState.HotTracked );
						}
						
						// SSP 1/20/05 BR01805 - IDataErrorInfo Support
						// Apply the DataErrorRowAppearance as well.
						//
						// ----------------------------------------------------------------------------
						if ( rowHasDataError )
						{
							// Do not resolve the image on the data error appearances. We don't want 
							// the editors to display this image. The DataErrorTooltipUIElement which
							// would be a sibling of the editor element in the cell will resolve and 
							// position the data error image itself.
							//
							AppearancePropFlags imageFlag = DATA_ERROR_APPEARANCE_EXCLUDE_PROPS & context.UnresolvedProps;
							context.UnresolvedProps ^= imageFlag;

							band.MergeBLOverrideAppearances( ref appData, ref context, 
								UltraGridOverride.OverrideAppearanceIndex.DataErrorRowAppearance
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, rowRole, AppStyling.RoleState.DataError );

							context.UnresolvedProps ^= imageFlag;
						}
						// ----------------------------------------------------------------------------

						// SSP 8/1/03 UWG1654 - Filter Action
						// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell  appearances.
						// Resolve the filter related appearances.
						//
						// ------------------------------------------------------------------------------------------------
						if ( applyFilterAppearance )
						{
							band.MergeBLOverrideAppearances( ref appData, ref context,
								isFilteredOut 
								? UltraGridOverride.OverrideAppearanceIndex.FilteredOutRowAppearance 
								: UltraGridOverride.OverrideAppearanceIndex.FilteredInRowAppearance
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, rowRole, isFilteredOut ? AppStyling.RoleState.FilteredOut : AppStyling.RoleState.FilteredIn );

							if(context.UnresolvedProps == 0)
								return;
						}
						// ------------------------------------------------------------------------------------------------

						// SSP 11/11/03 Add Row Feature
						// Apply template add-row appearance.
						//
						// ----------------------------------------------------------------------------------
						if ( shouldApplyTemplateAddRowAppearance || shouldApplyAddRowAppearance )
						{
							// SSP 8/4/05 BR04897
							// Only shade the back color if it comes from the RowAppearance or CellAppearance
							// or ResolveColors. If someone specifies the DataErrorCellAppearance.BackColor then
							// we don't want to shade that if the add-row happens to have the data error.
							// 
							

							band.MergeBLOverrideAppearances( ref appData, ref context, 
								shouldApplyTemplateAddRowAppearance 
									? UltraGridOverride.OverrideAppearanceIndex.TemplateAddRowAppearance
									: UltraGridOverride.OverrideAppearanceIndex.AddRowAppearance
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, rowRole, shouldApplyTemplateAddRowAppearance 
									? AppStyling.RoleState.TemplateAddRow 
									: AppStyling.RoleState.AddRow
								);

							if ( 0 == addRowFeature_morphBackColorFlag 
								&& ! appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
								addRowFeature_morphBackColorFlag = 1;
						}
						// ----------------------------------------------------------------------------------

						// SSP 3/29/05 - NAS 5.2 Fixed Rows/Filter Row
						//
						// ------------------------------------------------------------------------------------------------
						bool shouldApplyFixedAppearance = this.ShouldApplyFixedAppearance( );
						if ( shouldApplyFixedAppearance )
						{
							band.MergeBLOverrideAppearances( ref appData, ref context,
								UltraGridOverride.OverrideAppearanceIndex.FixedRowAppearance
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, rowRole, AppStyling.RoleState.FixedRow );
						}

						if ( isFilterRow )
						{
							if ( hasFilters )
							{
								// Apply the active filter row appearance if there are any filters.
								//
								band.MergeBLOverrideAppearances( ref appData, ref context,
									UltraGridOverride.OverrideAppearanceIndex.FilterRowAppearanceActive
									// SSP 3/13/06 - App Styling
									// Use the new overload that takes in the app styling related params.
									// 
									, rowRole, AppStyling.RoleState.HasActiveFilters );
							}

							band.MergeBLOverrideAppearances( ref appData, ref context,
								UltraGridOverride.OverrideAppearanceIndex.FilterRowAppearance
								// SSP 3/13/06 - App Styling
								// Use the new overload that takes in the app styling related params.
								// 
								, rowRole, AppStyling.RoleState.FilterRow );
						}
						// ------------------------------------------------------------------------------------------------

						// SSP 7/11/05 - NAS 5.3 Empty Rows
						// 
						// ------------------------------------------------------------------------------------------------
						if ( isEmptyRow )
						{
							// SSP 3/9/06 - App Styling
							// 
							//layout.EmptyRowSettings.ResolveRowAppearance( ref appData, ref context.UnresolvedProps );
							layout.EmptyRowSettings.ResolveRowAppearance( ref appData, ref context );
						}
						// ------------------------------------------------------------------------------------------------

						if ( isAlternate )
						{
							context.IsAlternate = true;
						
							band.ResolveAppearance( ref appData, ref context );

							if(context.UnresolvedProps == 0)
							{
								return;
							}

							context.IsAlternate = false;
						}

						// SSP 5/4/06 - App Styling
						// Resolve the CardView state appearances.
						// 
						if ( context.IsCard )
							StyleUtils.ResolveAppearance( rowRole, AppStyling.RoleState.CardView, ref appData, ref context );

						// Call the band's resolve appearance which will use band level
						// overrides for the row appareance
						//
						band.ResolveAppearance( ref appData, ref context );
						// finally call ResolveColors to make sure we have a valid
						// fore and back color
						layout.ResolveColors( ref appData, ref context, SystemColors.Window,
							SystemColors.WindowFrame, SystemColors.WindowText, false);

						// SSP 11/11/03 Add Row Feature
						//
						// --------------------------------------------------------------------------------------------
						if ( 1 == addRowFeature_morphBackColorFlag 
							// SSP 4/2/04 UWG2985
							// Changed the behavior of AddRowAppearance. Shade the cell back color to use with the
							// add-row only if template add-row functionality is turned on. When add-row feature 
							// is not turned on and the user adds a new row from outside, we don't want to all of
							// a sudden start showing the added row as gray where as in version 3.0 we used to
							// show it the same back color as the regular rows.
							// Added below condition.
							//
							&& TemplateAddRowLocation.None != this.ParentCollection.TemplateRowLocationDefault
							&& appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
							appData.BackColor = UltraGridLayout.GetColorShade( appData.BackColor, shouldApplyAddRowAppearance ? 25 : 50 );
						// --------------------------------------------------------------------------------------------
					}
				}
				finally
				{
					// JJD 10/31/01
					// Cache the apperance so we can use it for another cell in
					// this column.
					//
					if ( cacheThisAppearanceForColumn )
                    {
                        // MRS 11/21/06 - BR17614
                        // We need to pass in flags so we know what was actually resolved. 
                        //column.SetCachedCellAppearance( selected, ref appData );
                        AppearancePropFlags unresolvedProps = requestedProps;
                        column.SetCachedCellAppearance(selected, ref appData, ref unresolvedProps);
                    }
                }
			}
			finally
			{
				// SSP 4/17/03
				// Clear any resolved properties from the requestedProps flags.
				//
				requestedProps = context.UnresolvedProps;
			}
		}

		// SSP 12/1/03 UWG2085
		// Added ButtonAppearance property to cell object and moved the following 
		// ResolveCellButtonAppearance method from Column to here and added a column 
		// parameter to it so we can resolve the cell's button appearance.
		//
		internal void ResolveCellButtonAppearance( UltraGridColumn column, ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			UltraGridBand band = this.band;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridLayout layout = band.Layout;

			// SSP 12/1/03 UWG2085
			// Added button appearance property to row object.
			//
			// ----------------------------------------------------------------------
			UltraGridCell cell = this.GetCellIfAllocated( column );
			if ( null != cell && cell.HasButtonAppearance )
				cell.ButtonAppearance.MergeData( ref appData, ref flags );
			// ----------------------------------------------------------------------

			if ( column.HasCellButtonAppearance )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( 
				//	ref appData, 
				//	this.CellButtonAppearance.Data,
				//	ref flags );
				column.CellButtonAppearance.MergeData( ref appData, ref flags );
			}

			// SSP 3/29/05 - Optimizations
			//
			// ----------------------------------------------------------------------------

			band.MergeBLOverrideAppearances( ref appData, ref flags, UltraGridOverride.OverrideAppearanceIndex.CellButton );

			
			// ----------------------------------------------------------------------------
		}

		// SSP 8/23/02 UWG1434
		// Made this public.
		//
		/// <summary>
		/// Indicates whether this row is an add-row.
		/// </summary>
		/// <remarks>
		/// <para class="body">
		/// There can only be one add-row at a time. When a new row is added, this property for that 
		/// row will return True. Any previously added rows will return False from this property. 
		/// When the add-row is updated, it will cease being an add-row and return False from this 
		/// property. Also note that regardless of the 
		/// <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.UpdateMode"/> settings, the add-row 
		/// will not be updated until another row is activated. This means that even if you set 
		/// the <b>UpdateMode</b> to <b>OnCellChange</b> or <b>OnCellChangeOrLostFocus</b>,
		/// the <b>IsAddRow</b> property will return True until the row loses focus. You can 
		/// explicitly update an add-row by using the row's <see cref="UltraGridRow.Update()"/> method.
		/// </para>
		/// </remarks>
		//internal bool IsAddRow
		public bool IsAddRow
		{
			get
			{
				// SSP 4/28/03 UWG1980
				// Only return true if this row is the active row (in other words, if the binding
				// manager's Position matches this row's ListIndex.
				// Commented out the original code and added new code below.
				//
				// --------------------------------------------------------------------------------
				//return this.isAddRow;
				if ( ! this.isAddRow )
					return false;

				// SSP 7/29/03 UWG2396
				// If the row has been updated, then return false.
				//
				if ( this.endEditCalled )
					return false;

				// SSP 9/26/03 UWG2673
				// When a row is added, the binding manager's Position is out of sync momentarily. 
				// It doesn't match the add new row while we are in the RowsCollection.OnListChanged
				// event handler. This is probably because the binding manager hasn't finished 
				// adding the row. So while we are in the AddNew method of the band, don't check
				// for the Position of the binding manager because it might not have been set
				// to the new row position yet by the binding manager while it's still processing
				// the add row command.
				// Enclosed the code in the if block.
				//
				if ( null != this.Band && ! this.Band.inAddNew )
				{
					if ( ! this.AreAllAncestorsCurrentRows )
					{
						this.InternalSetIsAddRow( false );
						return false;
					}
				}
				
				return true;
				// --------------------------------------------------------------------------------
			}
			// SSP 8/23/02 UWG1434
			// We want this to be read-only.
			//
			
		}

		
		// SSP 7/29/03 UWG2396
		// Added InternalIsAddRow property.
		//
		#region InternalIsAddRow
		
		internal bool InternalIsAddRow
		{
			get
			{
				return this.isAddRow;
			}
		}

		#endregion // InternalIsAddRow

		// SSP 8/23/02 UWG1434
		// Made IsAddRow public. However since it has to be read-only, added InternalSetIsAddRow
		// internal method for setting it. Code was copied from the set of the IsAddRow.
		//
		internal void InternalSetIsAddRow( bool val )
		{
			this.isAddRow = val;

			if ( this.isAddRow )
			{
				this.Band.InternalSetLastAddedRow( this );
			}
		}

		// SSP 11/7/03 - Add Row Feature
		//
		#region Add Row Feature

		/// <summary>
		/// Indicates whether this row is a template add-row.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The Override's <see cref="UltraGridOverride.AllowAddNew"/> can be used to
		/// enabled add-row functionality.
		/// </p>
		/// <seealso cref="UltraGridOverride.AllowAddNew"/> 
		/// <seealso cref="RowsCollection.TemplateAddRow"/> 
		/// <seealso cref="UltraGridOverride.AllowAddNew"/>
		/// </remarks>
		public bool IsTemplateAddRow
		{
			get
			{
				return null != this.parentCollection 
					&& this == this.parentCollection.TemplateAddRowInternal;
			}
		}

		internal bool IsAddRowFromTemplate
		{
			get
			{
				return null != this.parentCollection && this == this.parentCollection.CurrentAddRow;
			}
		}

		internal bool IsModifiedAddRowFromTemplate
		{
			get
			{
				return this.IsAddRowFromTemplate && this.parentCollection.CurrentAddRowModifiedByUser;
			}
		}

		internal bool IsUnModifiedAddRowFromTemplate
		{
			get
			{
				return this.IsAddRowFromTemplate && ! this.parentCollection.CurrentAddRowModifiedByUser;
			}
		}

		// MRS 1/14/05 - BR00792
		// Publicly exposed this property (with a slightly better name).
		#region IsUnmodifiedTemplateAddRow		

		/// <summary>
		/// Indicates whether this row is an add-row that was added via template-add row and hasn't been 
		/// modified by the user yet. If this property returns true then it won't show any child template 
		/// add-row.
		/// </summary>
		public bool IsUnmodifiedTemplateAddRow
		{
			get
			{
				return this.IsUnModifiedAddRowFromTemplate;
			}
		}

		#endregion IsUnmodifiedTemplateAddRow

		internal bool ShouldApplyAddRowAppearance( )
		{
			// SSP 4/2/04 UWG2985
			// Changed the behavior of AddRowAppearance. Shade the cell back color to use with the
			// add-row only if template add-row functionality is turned on. When add-row feature 
			// is not turned on and the user adds a new row from outside, we don't want to all of
			// a sudded start showing the added row as gray where as in version 3.0 we used to
			// show it the same back color as the regular rows. Also don't apply the add-row
			// appearance to rows that are added from outside.
			//
			//return this.IsAddRow && ! this.ShouldApplyTemplateAddRowAppearance( );
			if ( this.IsAddRow && ! this.ShouldApplyTemplateAddRowAppearance( ) )
			{
				if ( null != this.BandInternal && this.BandInternal.lastAddedRowAddedThroughGrid )
					return true;
			}

			return false;
		}

		internal bool ShouldApplyTemplateAddRowAppearance( )
		{
			return this.IsTemplateAddRow || this.IsUnModifiedAddRowFromTemplate;
		}

		internal void EnterFirstCellInEditMode( )
		{
			UltraGrid grid = null != this.Layout ? this.Layout.Grid as UltraGrid : null;

			if ( null == grid )
				return;			

			UltraGridColumn firstColumn = this.Band.GetFirstVisibleCol( this.Layout.ActiveColScrollRegion, false );

			if ( firstColumn != null && null != this.Cells )
			{
				UltraGridCell firstEditableCell = this.Cells[ firstColumn ];

				// set the active cell to this cell
				//
				grid.ActiveCell = firstEditableCell;

				// if the activation wasn't cancelled then enter edit mode
				//
				if ( firstEditableCell == this.Layout.ActiveCell )
					firstEditableCell.EnterEditMode();
			}
		}

		#endregion // Add Row Feature

		#region IsGroupByRow

		// SSP 2/13/04
		// Added IsGroupByRow property.
		//
		/// <summary>
		/// Indicates whether this is a group-by row.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>IsGroupByRow</b> can be used to check if a row is a group-by row or a non-group-by row.</p>
		/// <seealso cref="UltraGridGroupByRow"/>
		/// </remarks>
		public virtual bool IsGroupByRow
		{
			get
			{
				return false;
			}
		}

		#endregion // IsGroupByRow

		/// <summary>
		/// Called when columns are added or removed from the band's column
		/// collection.
		/// </summary>
		internal void OnColumnsChanged()
		{

			// JJD 10/17/01
			// If we are firing InitializeLayout ignore this
			// and return
			//
			if ( this.Layout.Grid.InInitializeLayout )
				return;

			if ( !this.Layout.InitializingAllSubItems )
			{
				// fire initialize row again
				// SSP 10/17/04
				// We had FireInitializeRow and InitializeRow methods that did almost the
				// same things. Got rid of InitializeRow because it's getting called from
				// only two places.
				//
				//this.InitializeRow(true);
				this.FireInitializeRow( true );
			}
		}

		// SSP 10/21/04 UWG3665
		// Added InternalOnSubObjectPropChanged because we need to call this from layout.
		//
		internal void InternalOnSubObjectPropChanged( PropChangeInfo propChange )
		{
			this.OnSubObjectPropChanged( propChange );
		}

		/// <summary>
		/// Called when a property has changed on a sub object		
        /// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{

			// let other people know about changes to our apperance object or 
			// cells collection
			//
			if ( propChange.Source == this.cells )
			{
				this.NotifyPropChange( PropertyIds.Cells, propChange );
				return;
			}
				// Perform the same dirtying logic for CellAppearance that we were
				// doing for Appearance
				//
			else if ( (propChange.Source == this.appearance) ||
				(propChange.Source == this.cellAppearance) )
			{								
				// SSP 12/3/01 UWG825
				// If cell appearance changes, incremenet this child elements cache version
				// so that cell elements recreate all the child elements.
				//
				this.Band.Layout.BumpCellChildElementsCacheVersion( );


				if ( ( null != propChange.FindPropId( AppearancePropIds.FontData ) )||
					( null != propChange.FindPropId( AppearancePropIds.Image ) ))
				{
					this.band.Layout.DirtyGridElement( true );
				}

				else if ( ( null != propChange.FindPropId( AppearancePropIds.ImageHAlign ) )||
					(	null != propChange.FindPropId( AppearancePropIds.ImageVAlign ) )||
					(	null != propChange.FindPropId( AppearancePropIds.TextVAlign  ) )||
					(   null != propChange.FindPropId( AppearancePropIds.TextHAlign  ) ) )
				{
					// SSP 12/3/01 UWG825
					// Why are we redrawing the band's header when appearances in a row
					// changes?
					//
					//this.Band.Header.InvalidateItemAllRegions ( true );
					// SSP 12/7/04 - Merged Cell Feature
					// Dirty associated merged cells as well.
					//
					//this.InvalidateItemAllRegions( true );
					this.InvalidateItemAllRegions( true, true );
				}

                // MRS 3/18/2009 - TFS15302                
                UltraDropDownBase ultraDropDownBase = this.GetGrid() as UltraDropDownBase;
                if (ultraDropDownBase != null)
                    ultraDropDownBase.BumpItemImageSizeVersion();

				if ( this.cellAppearance == propChange.Source )
				{
					// FUTURE: If we auto recalc height based on cell font changes
					//         we can try to dirty grid element. See GetMinRowHeight off Row

					this.NotifyPropChange( PropertyIds.CellAppearance, propChange );
				}
				else
					this.NotifyPropChange( PropertyIds.Appearance, propChange );

				return;
			}

			else if( this.rowSelectorAppearance == propChange.Source )
			{
				this.NotifyPropChange( PropertyIds.RowSelectorAppearance, propChange );
				return;
			}

			else if ( this.previewAppearance == propChange.Source )
			{
				this.NotifyPropChange( PropertyIds.RowPreviewAppearance, propChange );
				return;
			}			
			
			Debug.Assert( false, "Unknown sub object in Row.OnSubObjectPropChanged" );
		}
		internal int GetLevelOffset( int nLevel, bool fromInsideTopBorder )
		{
			int nOffset = 0;

			// MD 1/20/09 - Groups in RowLayout
			// More checks were added to ActualColDisplayLevels which should also be added here, so just use the property instead.
			//int nLevelCount =	this.Band.LevelCount;
			int nLevelCount = this.Band.ActualColDisplayLevels;

			UIElementBorderStyle borderStyle = this.Band.BorderStyleRowResolved;

			if ( !fromInsideTopBorder )
				nOffset += Layout.GetBorderThickness ( borderStyle );

			if ( nLevel >= nLevelCount )
			{
				System.Diagnostics.Debug.Assert( false, "Invalid level number in Row.GetLevelOffset" );
				return nOffset;
			}

			// MD 1/20/09 - Groups in RowLayout
			// This check is no longer needed because the member is now obtained from ActualColDisplayLevels, which checks for GroupsDisplayed.
			//if ( nLevelCount > 1  && this.Band.GroupsDisplayed )
			if ( nLevelCount > 1 )
			{
				// Adjusted calculation to make non evenly divided numbers look better
				//
				// Use new GetRawHeightPerLevel method instead
				//
				// Changed level height calc to be more accurate (spreads out
				// odd remainders more evenly over the levels when there are
				// more than 2 levels). For example, if there are 10 levels and
				// dividing the height by 10 left a 9 pixel remainder, the old
				// would make the last level stand out (all 9 extra pixels) but
				// this way 9 of the levels would get 1 extra pixel)
				//
				nOffset +=	this.GetOffsetPerLevelHelper( nLevel );
			}

			return nOffset;
		}				
		//	BF 10.22.04	UWG3454
		//	Changed scope so this could be called from UltraGridColumn.CalculateCellTextWidth
		//private int HeightInsideRowBorders
		internal int HeightInsideRowBorders
		{
			get
			{
				int nHeight = this.BaseHeight;
				UIElementBorderStyle borderStyle = this.Band.BorderStyleRowResolved;

				int nBorderThickness = Layout.GetBorderThickness ( borderStyle );

				// adjust the height for the top and bottom borders
				//
				nHeight -= nBorderThickness * 2;

				return nHeight;
			}
		}

		internal int GetLevelHeight( int level )
		{
			int nHeight = this.HeightInsideRowBorders;

			//RobA UWG685 11/7/01 
			//HeightInsideRowBorders includes the autopreview area
			//
			if ( this.AutoPreviewEnabled )
				nHeight -= this.AutoPreviewHeight;

			// MD 1/20/09 - Groups in RowLayout
			// More checks were added to ActualColDisplayLevels which should also be added here, so just use the property instead.
			//int nLevelCount =	this.Band.LevelCount;
			int nLevelCount = this.Band.ActualColDisplayLevels;

			if ( level >= nLevelCount )
			{
				System.Diagnostics.Debug.Assert( false, "Invalid level number in Row.GetLevelHeight" );
				return nHeight;
			}

			// MD 1/20/09 - Groups in RowLayout
			// This check is no longer needed because the member is now obtained from ActualColDisplayLevels, which checks for GroupsDisplayed.
			//if ( nLevelCount > 1  && this.Band.GroupsDisplayed )
			if ( nLevelCount > 1 )
			{
				// calculate the height per level.
				//
				// JJD 9/16/00 - ult1617
				// Use new GetRawHeightPerLevel method instead so we
				// are consistent with GetLevelOffset
				//
				//        int nHeightPerLevel = GetRawHeightPerLevel( );
				// on the last level we want to compensate for any
				// pixel truncations caused by the division above
				//
				//        if ( nLevel == nLevelCount - 1 )
				//            nHeight -= nHeightPerLevel * ( nLevelCount - 1 );
				//        else
				//            nHeight = nHeightPerLevel;
				//
				// JJD 9/18/00 
				// Changed level height calc to be more accurate (spreads out
				// odd remainders more evenly over the levels when there are
				// more than 2 levels). For example, if there are 10 levels and
				// dividing the height by 10 left a 9 pixel remainder, the old
				// would make the last level stand out (all 9 extra pixels) but
				// this way 9 of the levels would get 1 extra pixel)
				//
				
				int nOffsetThisLevel = GetOffsetPerLevelHelper( level );

				// on the last level just subtract the offset from the height
				//
				if ( level == nLevelCount - 1 )
					nHeight -= nOffsetThisLevel;
				else
				{
					// JJD 9/18/00
					// Otherwise, get the offset for the next level and subtract
					// this level's offset
					//
					int nOffsetNextLevel = GetOffsetPerLevelHelper( level + 1 );

					nHeight = nOffsetNextLevel - nOffsetThisLevel;

					// If cellspacing is zero and cell borders can be merged then
					// adjust the height by adding 1 so that this cell's bottom border will
					// overlap the top border of the cell below it
					//
					//	
					//	We were checking the level and level offset here, which was not necessary;
					//	it is already being done (see above)
					//
					// JJD 1/15/02
					// Check the borderstylecell from the band
					//
					if ( this.band.CellSpacingResolved == 0   &&
						this.Layout.CanMergeAdjacentBorders( this.band.BorderStyleCellResolved ) )
					{
						nHeight++;
					}
				}
			}

			return nHeight;
		}		

		int GetOffsetPerLevelHelper( int nLevel )
		{
			// Changed level height calc to be more accurate (spreads out
			// odd remainders more evenly over the levels when there are
			// more than 2 levels). For example, if there are 10 levels and
			// dividing the height by 10 left a 9 pixel remainder, the old
			// would make the last level stand out (all 9 extra pixels) but
			// this way 9 of the levels would get 1 extra pixel)
			//
			if ( nLevel < 1 )
				return 0;

			// MD 1/21/09 - Groups in RowLayouts
			Debug.Assert( this.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout, "Column levels should not be used in GroupLayout style" );

			// MD 1/21/09 - Groups in RowLayouts
			// Use LevelCountResolved because it checks the band's row layout style
			//int nLevelCount =	this.Band.LevelCount;
			int nLevelCount = this.Band.LevelCountResolved;

			//RobA UWG685 11/7/01 
			//HeightInsideRowBorders includes the autopreview area
			//
			int height = this.HeightInsideRowBorders;			
			
			if ( this.AutoPreviewEnabled )
				height -= this.AutoPreviewHeight;

			// JJD 9/18/00 
			// Muliplying by the level number and dividing by the
			// level count will speard any remainder across all the
			// levels more evenly
			//
			//int nLevelOffset = ( this.HeightInsideRowBorders * nLevel ) / nLevelCount;
			int nLevelOffset = ( height * nLevel ) / nLevelCount;

			// If cellspacing is zero and cell borders can be merged then
			// adjust the offset by -1 so that this cell's top border will
			// overlap the bottom border of the cell above it
			//
			// JJD 1/15/02
			// Check the borderstylecell from the band
			//
			if ( nLevel > 0 && nLevelOffset > 1           &&
				this.band.CellSpacingResolved == 0   &&
				Layout.CanMergeAdjacentBorders( this.band.BorderStyleCellResolved ) )
			{
				nLevelOffset--;
			}

			return nLevelOffset;
		}

		internal UltraGridLayout  Layout
		{
			get
			{
				return this.band.Layout;
			}
		}	
		
		internal virtual int TotalHeight
		{
			get
			{
				return 
					this.RowSpacingBeforeResolved
					+ this.BaseHeight
					+ this.RowSpacingAfterResolved;
			}
		}

		internal int ScrollPosition
		{ 
			get
			{
				// SSP 2/13/04 - Virtual Mode - Optimization
				// Updated the logic that calculates the scroll positions/counts.
				//
				int scrollPos = this.ParentCollection.GetScrollIndex( this );

				// Either the row is an invalid row or is hidden.
				//
				if ( scrollPos < 0 )
					return -1;

				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null != this.ParentRow )
				//{
				//    ChildBandsCollection childBandsColl = this.ParentRow.ChildBands;
				UltraGridRow parentRow = this.ParentRow;

				if ( null != parentRow )
				{
					ChildBandsCollection childBandsColl = parentRow.ChildBands;

					if ( null != childBandsColl )
					{
						// MD 7/27/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						// Cache the value so we don't have to call it on each iteration of the form loop
						UltraGridChildBand parentChildBand = this.ParentChildBand;

						for ( int i = 0; i < childBandsColl.Count; i++ )
						{
							UltraGridChildBand childBand = childBandsColl[i];

							// MD 7/27/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//if ( childBand == this.ParentChildBand )
							if ( childBand == parentChildBand )
								break;

							if ( null != childBand && ! childBand.Band.HiddenResolved )
								scrollPos += childBand.Rows.ScrollCount;
						}
					}

					// Also take into account the fact that a child row's scroll position should 
					// be one more than the parent row's scroll position.
					//
					scrollPos++;

					// MD 7/27/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//int parentRowScrollPosition = this.ParentRow.ScrollPosition;
					int parentRowScrollPosition = parentRow.ScrollPosition;

					// If the parent row's scroll position is valid then add it to this row's 
					// relative scroll position. If it's invalid then return -1.
					//
					if ( parentRowScrollPosition >= 0 )
						scrollPos += parentRowScrollPosition;
					else
						scrollPos = -1;
				}

				return scrollPos;
				
				
				// --------------------------------------------------------------------------------
			}			
		}

		/// <summary>
		/// Returns true if the row height needs to be re-initialized
		/// </summary>
		protected bool IsRowHeightDirty
		{
			get
			{
				// JJD 9/8/00 - ult1619
				// Added MajorRowHeightVersion to determine if something basic changed
				// since a row was sized to force it to resize (e.g. level count changed)
				//
				// This applies even if rows aren't synchronized
				//
				if ( this.band.MajorRowHeightVersion > this.synchronizedRowHeightVersion )
					return true;

				// Check the synchronized version only if that is the row sizing mode
				//
				// SSP 4/24/03 - Optimizations
				//
				RowSizing rowSizingResolved = this.band.RowSizingResolved;
				if ( rowSizingResolved == RowSizing.Sychronized &&
					this.band.SynchronizedRowHeightVersion != this.synchronizedRowHeightVersion )
					return true;

				//	BF 11.2.00	ULT1528, ULT1828
				//	(ULT1828) If row sizing is synchronized and the default row height
				//	changes, the row is considered dirty because otherwise the visible rows will be
				//	be a different height than rows that are scrolled into view after the fact.
				//	We don't want this because synchronized implies that all rows should be the same size
				if( this.band.IsRowHeightDirty &&
					rowSizingResolved == RowSizing.Sychronized )
					return true;

				return false;
				
			}
		}

		#region InternalDirtyRowHeight

		// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// 
		internal void InternalDirtyRowHeight( )
		{
			this.synchronizedRowHeightVersion--;

			// Make sure the potential change in the height of the row is reflected on screen
			// if the row is currently visible on screen.
			// 
			if ( this.IsPotentiallyVisibleOnScreen )
				this.Layout.DirtyGridElement( true );
		}

		#endregion // InternalDirtyRowHeight

		private void InitRootHeight( )
		{
								
			//	BF 8.12.02	UWG1429
			//
			//	If the override's RowSizing property is set to anything
			//	other than Synchronized, check to see if the row's height
			//	was set manually; if it was, we don't want to reset the
			//	rootHeight member variable, because this member is the only
			//	means we use to cache each row's individual height.
			//
			bool setRootHeight = true;
			// SSP 4/24/03 - Optimizations
			// 
			// --------------------------------------------------------------------------------
			
			if ( this.heightSetManually )
			{
				if ( this.BandInternal.RowSizingResolved != RowSizing.Sychronized )
					setRootHeight = false;
				else
					this.heightSetManually = false;
			}
			// --------------------------------------------------------------------------------

			if ( setRootHeight )
			{
				// JJD 3/14/00 - ult34
				// Changed from storing the m_nHeight (which included cell padding and cell
				// spacing to storing m_nRootHeight which does not
				//
				// SSP 11/26/03 - Optimizations
				// Let rootHeight contain the cell spacing and cell padding. Otherwise we have
				// to keep substracting these two entities all the time.
				//
				//this.rootHeight = this.band.RowHeightResolved
				//	- ( 2 * ( this.band.CellSpacingResolved + this.band.CellPaddingResolved ) );
				// SSP 8/29/05 BR05818
				// 
				// ----------------------------------------------------------------------------------
				//this.rootHeight = this.band.RowHeightResolved;
				if ( null != this.rowAutoSizeLayoutManagerHolder && this.band.UseRowLayoutResolved 
					&& RowSizing.Sychronized != this.band.RowSizingResolved )
				{
					this.rootHeight = this.RowAutoSizeLayoutManagerHolder.RowLayoutPreferredSize.Height;
				}
				else
				{
					this.rootHeight = this.band.RowHeightResolved;
				}
				// ----------------------------------------------------------------------------------

				
			}

			// NickC 10/5/00 : ult 1341/1677 Removed code that subtracted AutoPreviewHeight 
			// from root height. Since we haven't added it yet, it was not correct to 
			// subtract it from root height. Ult 1341 was fixed another way.

			// JJD 9/13/00 
			// Cache m_dwSynchronizedRowHeightVersion so that we can go a
			// lazy approach for synchronizing row heights. This is because
			// if we have a lot of rows preloaded it can take too int to
			// process a size change
			//    
			this.synchronizedRowHeightVersion = this.band.SynchronizedRowHeightVersion;
		}

		// SSP 11/26/03 UWG2344
		// Added PerformAutoSize to the UltraGridRow.
		//
		#region PerformAutoSize

		/// <summary>
		/// Resizes the row based on its contents.
		/// </summary>
		/// <remarks>
		/// <p class="body">Resizes the row based on its contents. This method 
		/// does nothing when the <see cref="UltraGridOverride.RowSizing"/> resolves 
		/// to Synchronized which it does by default. Set it to a value other than 
		/// <b>Synchronized</b> to have this method perform as expected.
		/// </p>
		/// <seealso cref="UltraGridOverride.RowSizing"/>
		/// <seealso cref="UltraGridRow.Height"/>
		/// </remarks>
		public void PerformAutoSize( )
		{
			// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// 
			//if ( RowSizing.Sychronized == this.Band.RowSizingResolved || this.Band.UseRowLayoutResolved )
			if ( RowSizing.Sychronized == this.Band.RowSizingResolved )
			{
				this.synchronizedRowHeightVersion--;
				this.InvalidateItemAllRegions( );
			}
			else
			{
				// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// If the user had previously resized any cells then clear their stored resize values.
				// They should now be based on the cell contents.
				//
				if ( null != this.rowAutoSizeLayoutManagerHolder )
					// SSP 2/6/08 BR27135
					// Changed the implementation and renamed it to better represent the new implementation.
					// 
					//this.rowAutoSizeLayoutManagerHolder.ClearManuallySetItemPreferredSizes( );
					this.rowAutoSizeLayoutManagerHolder.PrepareForExplicitPerformAutoSize( );

				this.Height = this.CalculateAutoHeight( );

				// SSP 5/20/04 UWG3256
				// Setting the Height sets the heightSetManually flag to true. This flag will
				// prevent the row from using the auto-size height next time it initializes the
				// root height (as a result of row height version numbers being bumped). So
				// set the flag to false.
				//
				this.heightSetManually = false;
			}
		}

		#endregion // PerformAutoSize

		#region RowAutoSizeLayoutManagerHolder

		// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// 
		internal Infragistics.Win.UltraWinGrid.RowAutoSizeLayoutManagerHolder RowAutoSizeLayoutManagerHolder
		{
			get
			{
				if ( null == this.rowAutoSizeLayoutManagerHolder )
					this.rowAutoSizeLayoutManagerHolder = new RowAutoSizeLayoutManagerHolder( this );

				this.BandInternal.VerifyRowLayoutCache( );

				return this.rowAutoSizeLayoutManagerHolder;
			}
		}

		#endregion // RowAutoSizeLayoutManagerHolder

		
		// SSP 4/1/02 UWG1060
		// Made this a virtual method so we can override it for group by rows.
		//
		//int CalculateAutoHeight()
		internal virtual int CalculateAutoHeight( )
		{
			// SSP 4/6/02
			// Modified for embeddable editors.
			//

			// SSP 4/1/02 UWG1060
			// Implemented this method. Before it was just returning 0.
			//

			if ( null == this.Band || null == this.Band.Columns )
				return 0;

			int maxHeight = 0;

			// SSP 11/20/03 UWG2745
			// Commented below code out. We should be using editors for calculating the ideal cell sizes.
			//
			

			// AS 8/12/03 optimization
			// Cache a bunch of the things we would be calculating or asking
			// for multiple times that is all band or row based and therefore should
			// never change within the loop.
			//
			UltraGridBand band = this.Band;
			UltraGridLayout layout = band.Layout;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridBase grid = layout.Grid;
			
			// SSP 11/20/03 UWG2745
			// Commented below code out. We should be using editors for calculating the ideal cell sizes.
			//
			
			int rowBorderThickness = 2 * layout.GetBorderThickness( band.BorderStyleRowResolved );
			int cellBorderThickness = 2 * layout.GetBorderThickness( band.BorderStyleCellResolved );
			// SSP 8/30/05 BR05846
			// 
			bool cellBordersMergeable = layout.CanMergeAdjacentBorders( band.BorderStyleCellResolved );

			// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// 
			// --------------------------------------------------------------------------------------
			if ( band.UseRowLayoutResolved )
			{
				this.RowAutoSizeLayoutManagerHolder.DirtyCachedSize( );
				int height = this.RowAutoSizeLayoutManagerHolder.RowLayoutPreferredSize.Height;
				height += rowBorderThickness;

				// SSP 8/30/05 BR05846
				// Take into account the merged cell borders.
				// 
				if ( rowBorderThickness > 0 && cellBordersMergeable )
					height -= cellBorderThickness;

				return height;
			}

			RowAutoSizeLayoutManagerHolder.RowAutoSizeCalculator autoHeightCalculator = 
				new RowAutoSizeLayoutManagerHolder.RowAutoSizeCalculator( this );
			
			// --------------------------------------------------------------------------------------

            // MRS 1/3/2008 - BR29247
            // This is not neccessary. We will account for cell spacing along with the borders inside
            // of RowAutoSizeCalculator.GetIdealCellSize. 
            // This is a better implementation, because it works in both RowLayout mode and non-RowLayout
            // mode. The fix as it is here only works when not using RowLayouts. 
            //int cellSpacing = 2 * band.CellSpacingResolved;

			ColumnsCollection columns = band.Columns;
			for ( int i = 0, count = columns.Count; i < count; i++ )
			{
				// AS 8/12/03 optimization - The Band doesn't change through the
				// loop so use the member retrieved outside the loop.
				//
				//UltraGridColumn column = this.Band.Columns[i];
				UltraGridColumn column = columns[i];

                // MRS 2/23/2009 - Found while fixing TFS14354
				//if ( null == column || column.Hidden )
                if (null == column || column.HiddenResolved)
					continue;

				// SSP 11/20/03 UWG2745
				//
				

				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout - Optimizations
				// Use the new RowAutoHeightCalculator instead which is more efficient.
				// Commented out the following block.
				// 
				// ----------------------------------------------------------------------------------
				
				int height = autoHeightCalculator.GetIdealCellSize( column ).Height;
				// ----------------------------------------------------------------------------------

				// SSP 11/20/03 UWG2745
				// 
				

				// Adjust the height to account for row borders and cell borders
				//
				// AS 8/12/03 optimization - The Band doesn't change through the
				// loop so use the member retrieved outside the loop.
				//
				//height += 2 * this.Band.Layout.GetBorderThickness( this.Band.BorderStyleRowResolved );
				//height += 2 * this.Band.Layout.GetBorderThickness( this.Band.BorderStyleCellResolved );
				height += rowBorderThickness;

				// SSP 8/30/05 BR05846
				// Take into account the merged cell borders.
				// 
				if ( rowBorderThickness > 0 && cellBordersMergeable )
					height -= cellBorderThickness;

				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout - Optimizations
				// CellBorderThickness is taken into account above by the call to 
				// autoHeightCalculator.GetIdealCellSize.
				// 
				//height += cellBorderThickness;

				//	BF 1.18.01	ULT2226
				//	We need to honor the RowSizingAutoMaxLines property
				//	and also, if RowSizing is auto, make sure the row is
				//	no higher than it needs to be

				//	OLD	----------------------------------------------------------
				//            if ( nMaxRowHeight != 0 )
				//                nHeight = max ( nHeight, nMaxRowHeight );
				//	----------------------------------------------------------------

				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout - Optimizations
				// This is taken into account above by the call to autoHeightCalculator.GetIdealCellSize.
				// 
				
            
				if ( maxHeight < height )
					maxHeight = height;
			}
			// SSP 11/20/03 UWG2745
			// Commented below code out. We should be using editors for calculating the ideal cell sizes.
			//
			

            // MRS 1/3/2008 - BR29247
            // This is not neccessary. We will account for cell spacing along with the borders inside
            // of RowAutoSizeCalculator.GetIdealCellSize. 
            // This is a better implementation, because it works in both RowLayout mode and non-RowLayout
            // mode. The fix as it is here only works when not using RowLayouts. 
            //// MRS 10/28/04 - UWG3671
            //// Account for cellSpacing. We are calculating this above, but never using it.
            //maxHeight += cellSpacing;

			// MRS 9/20/04 - UWG3671
			// Account for multiple levels
			// MD 1/21/09 - Groups in RowLayouts
			// Use LevelCountResolved because it checks the band's row layout style
			//maxHeight *= this.band.LevelCount;
			maxHeight *= this.band.LevelCountResolved;

			return maxHeight;
		}


		// SSP 4/26/02
		// EM Embeddable editors.
		// Look at the modified version above.
		//
		

		/// <summary>
		/// Returns a value that determines whether the AutoPreview area will be displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">The AutoPreview area appears under a row and provides a way to display multiple lines of text associated with that row. You can specify how many lines of text should be displayed, and choose to either display the value from a cell in the row or a custom text string that you specify. One common use might be to display the contents of a memo field that initially appears off-screen when the grid is loaded.</p>
		/// <p class="body">The <b>AutoPreviewEnabled</b> property determines whether the AutoPreview area can be displayed for rows in the specified band. Once AutoPreview has been enabled, it can be displayed for any row by setting the UltraGridRow object's <b>AutoPreviewHidden</b> property to False.</p>
		///	<remarsk>
		///	<seealso cref="UltraGridBand.AutoPreviewEnabled"/>
		///	<seealso cref="UltraGridBand.AutoPreviewField"/>
		///	<seealso cref="UltraGridRow.AutoPreviewHidden"/>
		///	</remarsk>
		/// </remarks>
		public bool AutoPreviewEnabled
		{
			get
			{
				if ( this.autoPreviewHidden )
					return false;

				return this.band.AutoPreviewEnabled;
			}
		}

		// SSP 4/20/05 - NAS 5.2 Filter Row
		// Changed from private to internal.
		//
		//private int HeightIncludingPadding
		internal int HeightIncludingPadding
		{
			get
			{
				
				// Cache m_dwSynchronizedRowHeightVersion so that we can go a
				// lazy approach for synchronizing row heights. This is because
				// if we have a lot of rows preloaded it can take too int to
				// process a size change
				//    				
				bool rootHeightWasIntialized = false;
				if ( this.IsRowHeightDirty )
				{
					this.InitRootHeight();					
					//	flag this function as having been called, so we don't
					//	add the AutoPreview height to the row height twice
					rootHeightWasIntialized=true;
				}

				//  For Auto sized row-heights, check lazy autoht flag
				//  and determine auto height based on cell multiline, content, and font
				if (
					// SSP 4/9/02 UWG1060
					// If root height was initialized above, we need to recalc the 
					// auto height
					//
					rootHeightWasIntialized ||
					!this.isAutoSized )
				{
					RowSizing rowSizing = this.band.RowSizingResolved;
					if ( rowSizing == RowSizing.AutoFree || rowSizing == RowSizing.AutoFixed )
					{
						int autoHt =  this.CalculateAutoHeight();						
            
						// A return of 0 means no multiline cells, default logic will work fine
						//
						if ( autoHt > 0 )
						{
							//	BF 8.12.02	UWG1429
							//	If the value of the rootHeight member variable is greater
							//	than the row's auto height, we will use that
							//
							//this.rootHeight = autoHt;
							this.rootHeight = System.Math.Max( this.rootHeight, autoHt );
						}

						this.isAutoSized = true;
					}
					// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
					// If RowSizing is switched from something other than Sychronized to Sychronized 
					// then null out the rowAutoSizeLayoutManagerHolder since it's not needed anymore.
					// 
					else if ( RowSizing.Sychronized == rowSizing )
					{
						// SSP 3/14/08 BR31270
						// Filte row always makes use of it's own row layout manager so don't
						// null it out in that case.
						// 
						//this.rowAutoSizeLayoutManagerHolder = null;
						if ( null != this.rowAutoSizeLayoutManagerHolder && ! this.IsFilterRow )
							this.rowAutoSizeLayoutManagerHolder = null;
					}
				}

				// SSP 11/26/03 - Optimizations
				// Changed the code so that rootHeight will contain the cell spacing and cell padding.
				//
				//int ret = this.rootHeight + ( 2 * ( this.band.CellSpacingResolved + this.band.CellPaddingResolved ) );
				int ret = this.rootHeight;

				//	The fix for ULT1828 was causing the AutoPreview height
				//	to be added to the row height twice; this flag will prevent that
				// SSP 5/22/06 BR12611
				// We should always add the auto-preview height regardless of whether rootHeightWasIntialized
				// was set or not.
				// 
				//if ( this.AutoPreviewEnabled && !rootHeightWasIntialized)
				if ( this.AutoPreviewEnabled )
				{
					ret += this.AutoPreviewHeight;
				}

				return ret;
			}
		}

		internal virtual int MinRowHeight
		{
			get
			{		
				// SSP 11/25/03 UWG2013 UWG2553
				// Added MinRowHeight property to Override to allow for unrestricted control over the row heights.
				//
				// ------------------------------------------------------------------
				int overrideMinRowHeight = this.BandInternal.OverrideMinRowHeight;
				if ( overrideMinRowHeight > 0 )
					return overrideMinRowHeight;
				// ------------------------------------------------------------------

				Infragistics.Win.UltraWinGrid.RowSizing rowSizing =  this.BandInternal.RowSizingResolved;

				// Code to consider GetAutoPreviewHeight() in min row height calc
				//
				int height =  0;

				AppearanceData appData;
				switch ( rowSizing )
				{
					case Infragistics.Win.UltraWinGrid.RowSizing.Free:
					case Infragistics.Win.UltraWinGrid.RowSizing.AutoFree:
					case Infragistics.Win.UltraWinGrid.RowSizing.AutoFixed:
						appData = new AppearanceData();												
						
						this.ResolveAppearance(ref appData, Infragistics.Win.AppearancePropFlags.FontData);										
						
						// get the base font' height
						//
						int lineHeight = this.band.Layout.Grid.Font.Height;
						
						// if we are overriding the size then adjust the 
						// height based on the oveeride size
						//
						if ( appData.HasFontData  &&
							appData.FontData.SizeInPoints > 0.0f )
						{
							lineHeight = (int)((float)lineHeight * appData.FontData.SizeInPoints / this.band.Layout.Grid.Font.SizeInPoints);
						}

						lineHeight = System.Math.Max( lineHeight, this.BandInternal.GetMinRowHeight() );
						
						height = lineHeight;
						
						break;

					case Infragistics.Win.UltraWinGrid.RowSizing.Default:
					case Infragistics.Win.UltraWinGrid.RowSizing.Fixed:
					case Infragistics.Win.UltraWinGrid.RowSizing.Sychronized:					
					default:
						height = this.BandInternal.GetMinRowHeight();
						break;
				}

				height += this.AutoPreviewHeight;

				return height;
			}
		}

		// SSP 11/10/04 - Merged Cell Feature
		// Added AutoPreviewDisplayed property.
		//
		internal bool AutoPreviewDisplayed
		{
			get
			{
				if ( this.AutoPreviewEnabled )
				{
					string description = this.Description;
					return null != description && description.Length > 0;
				}

				return false;
			}
		}

		/// <summary>
		/// Returns the height, in pixels, of the AutoPreview area for the row
		/// </summary>
		public virtual int AutoPreviewHeight 
		{
			get
			{
				// SSP 3/11/05 BR02735
				// Check the row's AutoPreviewEnabled instead of the band's.
				//
				// ------------------------------------------------------------------
				//if ( !this.band.AutoPreviewEnabled )					
				//	return 0;
				if ( ! this.AutoPreviewEnabled )
					return 0;
				// ------------------------------------------------------------------

				// get the description for this row
				// 
				string autoPreviewText = this.Description;

				
				//
				if ( autoPreviewText != null &&
					autoPreviewText.Length > 0 )
				{
					AppearanceData appData = new AppearanceData();

					// resolve the font
					//
					this.ResolveAppearance( ref appData, AppearancePropFlags.FontData, true );

					UltraGridBand band = this.band;
					UltraGridLayout layout = band.Layout;

					// get the control's font
					//
					Font font = layout.Grid.Font;
					
					Font createdFont = null;
					
					// if there is a font override then create a temp font
					//
					if ( appData.HasFontData )
					{
						createdFont = appData.CreateFont( font );

						if ( createdFont != null )
							font = createdFont;
					}

					// create a graphics object
					//
					// JJD 10/23/01
					// Call CreateReferenceGraphics which will create the least 
					// disruptive graphics object based on the the permissions
					// of the assembly
					//
					// AS 8/12/03 optimization - Use the new caching mechanism.
					//
					//Graphics gr = DrawUtility.CreateReferenceGraphics( this.Layout.Grid );
					//					Graphics gr = Graphics.FromHwnd( this.band.Layout.Grid.Handle );
					// MRS 6/1/04 - UWG2915
					//Graphics gr = DrawUtility.GetCachedGraphics( this.Layout.Grid );
					Graphics gr = layout.GetCachedGraphics();
					
					// calculate the size of the string
					//
					//  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
					//Size size = gr.MeasureString( autoPreviewText, font, this.band.GetExtent( BandOrigin.RowCellArea ) ).ToSize();
					// SSP 8/9/05 BR05390
					// Take into account the left and the right row borders as well as AutoPreviewIndentation.
					// 
					// ----------------------------------------------------------------------------------------
					//Size size = DrawUtility.MeasureString( gr, autoPreviewText, font, this.band.GetExtent( BandOrigin.RowCellArea ) ).ToSize();

					UIElementBorderStyle rowBorderStyle = this.BorderStyleResolved;
					int rowBorderWidth = layout.GetBorderThickness( rowBorderStyle );
					bool rowBordersMergeable = layout.CanMergeAdjacentBorders( rowBorderStyle );

					int availableWidth = band.GetExtent( BandOrigin.RowCellArea );
					availableWidth -= 2 * rowBorderWidth;

					// RowUIElement merges the solid row border with the row selector by extending it 1 pixel left.
					// 
					if ( rowBordersMergeable && band.RowSelectorExtent > 0 )
						availableWidth++;

					availableWidth -= band.AutoPreviewIndentation;

					// SSP 11/4/05 BR07555
					// Need to pass printing param when measuring in Whidbey. When printing,
					// in Whidbey we need use the GDI+ measuring.
					// 
					//Size size = DrawUtility.MeasureString( gr, autoPreviewText, font, availableWidth ).ToSize();
					Size size = layout.MeasureString( gr, autoPreviewText, font, availableWidth );

					// ----------------------------------------------------------------------------------------
					
					//  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
					//Size sizeMin = gr.MeasureString( "lg", font ).ToSize();
					// SSP 11/4/05 BR07555
					// Need to pass printing param when measuring in Whidbey. When printing,
					// in Whidbey we need use the GDI+ measuring.
					// 
					//Size sizeMin = DrawUtility.MeasureString( gr, "lg", font ).ToSize();
					Size sizeMin = layout.MeasureString( gr, "lg", font );

					//RobA UWG682 11/13/01
					int maxHeight = sizeMin.Height * band.AutoPreviewMaxLines;
					
					// AS 8/12/03 optimization - Use the new caching mechanism.
					//
					//gr.Dispose();
					// MRS 6/1/04 - UWG2915
					//DrawUtility.ReleaseCachedGraphics(gr);
					layout.ReleaseCachedGraphics(gr);

					if ( createdFont != null )
						createdFont.Dispose();

					int apHeight = System.Math.Max( sizeMin.Height, size.Height );

					// RobA UWG682 11/13/01 commented below code out because we are doing this
					// in RowAutoPreviewUlElement.PositionChildElements
					apHeight = System.Math.Min( maxHeight, apHeight );
					
					// add enough extra space for the the cell spacing and borders
					//
					//apHeight += this.band.CellSpacingResolved;// we adjust for cell spacing below the autopreview area
					//UIElementBorderStyle borderStyle = this.Band.BorderStyleRowResolved;
					//apHeight += this.Layout.GetBorderThickness( borderStyle );

					// SSP 9/26/02 UWG1425
					// I don't know why the above code that adds border thickness is commented out,
					// but we need to add the border thickness to the auto preview height because
					// auto preview area has its own border. I don't see the border thickness being
					// taken into account when calculating the auto preview height anywhere.
					// Also use a cell padding of 1 on top and bottom.
					//
					// SSP 8/9/05 - Optimization
					// 
					//apHeight += 2 + 2 * this.Layout.GetBorderThickness( this.BandInternal.BorderStyleRowResolved );
					apHeight += 2 + 2 * rowBorderWidth;
					
					return apHeight;
				}

				else
				{
					return 0;
				}
			}
		}
				   
		internal virtual int BaseHeight
		{
			get
			{
                // MBS 4/24/09 - TFS12665
                // Similarly to the optimizations made elsewhere, when we're performing
                // operations where we're reusing info that will not change (i.e.
                // when recreating all of the visible rows), we can cache some of the
                // more expensive operations.  In this case, we don't need to keep walking
                // up the parent chain every time that we need to ask for the height
                UltraGridLayout.CachedRowInfo rowInfo = null;
                Dictionary<UltraGridRow, UltraGridLayout.CachedRowInfo> cachedInfo = null;
                if (this.Layout != null && this.Layout.IsCachingBandInfo)
                {
                    cachedInfo = this.Layout.CachedRowData;
                    if (cachedInfo.TryGetValue(this, out rowInfo) && rowInfo.BaseHeight > -1)
                        return rowInfo.BaseHeight;
                }

				
				
				
				
				int temp  = this.HeightIncludingPadding;
				int temp2 = this.MinRowHeight;

				int baseHeight = System.Math.Max( temp, temp2 );

				UltraGridRow parentRow = this.ParentRow;
				if ( null != parentRow )
				{
					ViewStyleBase viewStyleBase = this.band.Layout.ViewStyleImpl;

					// if there are multiple rows on a tier (horizontal view style)
					// and we are the first sibling row (no previous sibling) then
					// make sure this rows height is at least as big as any of
					// its ancestors
					//
					if ( null != viewStyleBase &&
						viewStyleBase.HasMultiRowTiers && 
						// SSP 11/11/03 Add Row Feature
						//
						//!this.HasPrevSibling(true) &&
						!this.HasPrevSibling( true, IncludeRowTypes.SpecialRows ) 
						// SSP 3/24/08 BR28869
						// Commented out the following condition. We shouldn't be checking if there's
						// a next sibling. Basically we want to syncrhonize first visible row's height
						// with parent row in horizontal view (so the same tier rows have the same
						// height for scrolling purposes).
						// 
						//&& !this.HasSiblingInNextSiblingBand(true)
						// SSP 9/11/06 BR14110
						// Disable same tier height syncrhonization in row-layout mode. 
						// We were doing this only if there was only one child row anyways
						// so the impact of this change should be minimal. The same
						// applies to groups.
						// 
						&& ! this.band.UseRowLayoutResolved && ! this.band.GroupsDisplayed
						&& ! parentRow.band.UseRowLayoutResolved && ! parentRow.band.GroupsDisplayed 
						)
					{
						baseHeight = System.Math.Max( baseHeight, parentRow.BaseHeight );
					}
				}

                // MBS 4/24/09 - TFS12665
                if (cachedInfo != null)
                {
                    if (rowInfo != null)
                        rowInfo.BaseHeight = baseHeight;
                    else
                    {
                        rowInfo = new UltraGridLayout.CachedRowInfo();
                        rowInfo.BaseHeight = baseHeight;

                        // MBS 5/13/09 - TFS17523
                        //cachedInfo.Add(this, rowInfo);
                        cachedInfo[this] = rowInfo;
                    }
                }

				return baseHeight;
			}
		}

		internal void SynchSameTierParentRows( int newHeight )
		{
			ViewStyleBase viewStyle = this.band.Layout.ViewStyleImpl;

			// if there are multiple rows on a tier (horizontal view style)
			// and we are the first sibling row (no previous sibling) then
			// make sure this rows height is at least as big as any of
			// its ancestors
			//
			if ( !viewStyle.HasMultiRowTiers )
				return;

			// Don't resync if we are already doing that
			//
			if ( !this.synchronizing && 
				this.HeightIncludingPadding != newHeight )
			{

				this.synchronizing = true;

				// Wra the code below in a try/catch so we are sure
				// to reset the flag
				//
				try
				{
					this.band.ResizeRow( this, newHeight, false ); 
				}
				catch(Exception)
				{
					Debug.Assert( false, "Try caught in Row.SynchSameTierParentRows");

					throw;
				}

				this.synchronizing = false;
			}

			// MD 7/27/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.ParentRow != null     &&
			//    // SSP 11/11/03 Add Row Feature
			//    //
			//    //!HasPrevSibling(false, true)   &&
			//    !HasPrevSibling( false, true, IncludeRowTypes.SpecialRows )   &&
			//    !HasSiblingInPrevSiblingBand(true) )
			//    this.ParentRow.SynchSameTierParentRows( newHeight );
			UltraGridRow parentRow = this.ParentRow;

			if ( parentRow != null &&
				// SSP 11/11/03 Add Row Feature
				//
				//!HasPrevSibling(false, true)   &&
				!HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) &&
				!HasSiblingInPrevSiblingBand( true ) )
				parentRow.SynchSameTierParentRows( newHeight );
		}
		internal void SynchSameTierChildRows( int newHeight )
		{
			ViewStyleBase viewStyle = this.band.Layout.ViewStyleImpl;

			// if there are multiple rows on a tier (horizontal view style)
			// and we are the first sibling row (no previous sibling) then
			// make sure this rows height is at least as big as any of
			// its ancestors
			//
			if ( !viewStyle.HasMultiRowTiers )
				return;

			// Don't resize this row if we are already in a synchronizing
			// operation
			//
			if ( !this.synchronizing && 
				this.HeightIncludingPadding != newHeight )
			{
				this.synchronizing = true;

				// Wra the code below in a try/catch so we are sure
				// to reset the flag
				//
				try
				{
					this.band.ResizeRow( this, newHeight, false ); 
				}
				catch(Exception)
				{
					Debug.Assert( false, "Try caught in CRow::SynchSameTierChildRows");

					throw;
				}

				this.synchronizing = false;
			}

			if ( this.IsExpanded && this.HasChildRowsInternal( IncludeRowTypes.SpecialRows ) )
			{
				// SSP 11/11/03 Add Row Feature
				//
				//UltraGridRow firstChild = this.GetChild( ChildRow.First );
				UltraGridRow firstChild = this.GetChild( ChildRow.First, null, IncludeRowTypes.SpecialRows );

				if ( firstChild != null )
				{
					firstChild.SynchSameTierChildRows( newHeight );
				}
			}

		}

		/// <summary>
		/// Ensures that all ancestor rows are expanded.
		/// </summary>
		/// <returns>true if all ancestors have been expanded</returns>
		public bool ExpandAncestors()
		{
			// MD 7/27/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//// if we don't have a parent row (band 0) then return true
			////
			//if ( this.ParentRow == null )
			//    return true;
			//
			//// call this method on our parent row which will walk up the parent
			//// chain until it reaches the top so that the highest level parent
			//// row will be expanded first
			////
			//if ( !this.ParentRow.ExpandAncestors() )
			//    return false;
			//
			//// attempt to expand the parent row
			////
			//if ( !this.ParentRow.IsExpanded )
			//    this.ParentRow.Expanded = true;
			//
			//return this.ParentRow.IsExpanded;
			UltraGridRow parentRow = this.ParentRow;

			// if we don't have a parent row (band 0) then return true
			//
			if ( parentRow == null )
				return true;

			// call this method on our parent row which will walk up the parent
			// chain until it reaches the top so that the highest level parent
			// row will be expanded first
			//
			if ( !parentRow.ExpandAncestors() )
				return false;

			// attempt to expand the parent row
			//
			if ( !parentRow.IsExpanded )
				parentRow.Expanded = true;

			return parentRow.IsExpanded;
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetCommonParent( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{

			Infragistics.Win.UltraWinGrid.UltraGridRow testRow;
			Infragistics.Win.UltraWinGrid.UltraGridRow thisRow = this;

			while ( thisRow != null )
			{
				testRow = row;

				while ( testRow != null )
				{
					if ( testRow == thisRow )
						return thisRow;

					testRow = testRow.ParentRow;
				}

				thisRow = thisRow.ParentRow;
			}

			return null;
		}
		internal void SetHeight( int newHeight )
		{
			this.SetHeight( newHeight, true );
		}

		internal void SetHeight( int newHeight, bool notify )
		{
			// In order to resize rows correctly in all synchronize row situations
			// changed the logic to always call the synchronize same tier methods
			// below and added a flag to prevent recursion
			//
			// check resizing flag to prevent recursion
			//
			if ( this.resizing )
				return;

			// set the resizing flag to true to prevent recursion
			//
			this.resizing = true;

			//	BF 8.12.02	UWG1429
			//
			//	Set a flag when the row's height is manually set, so that
			//	this height takes precedence over the DefaultRowHeight
			//
			if ( this.Band.RowSizingResolved != RowSizing.Sychronized )
				this.heightSetManually = true;

			// SSP 8/14/07 BR25122
			// 
			//if ( newHeight != this.HeightIncludingPadding )
			int heightIncludingPadding = this.HeightIncludingPadding;
			if ( newHeight != heightIncludingPadding )
			{
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( newHeight < this.MinRowHeight )
				//    newHeight = this.MinRowHeight;
				int minRowHeight = this.MinRowHeight;

				if ( newHeight < minRowHeight )
					newHeight = minRowHeight;

				// SSP 8/14/07 BR25122
				// Since the newHeight can potentially change above where we apply the min row height 
				// constraint, re-check this condition again. Enclosed the existing code inside the if
				// block.
				// 
				if ( newHeight != heightIncludingPadding )
				{
					// Changed from storing the m_nHeight (which included cell padding and cell
					// spacing to storing m_nRootHeight which does not
					//
					// SSP 11/26/03 - Optimizations
					// Changed the code so that rootHeight will contain the cell spacing and cell padding.
					//
					//this.rootHeight = newHeight - ( 2 * ( this.band.CellSpacingResolved + this.band.CellPaddingResolved ) );
					this.rootHeight = newHeight;

					// Take AutoPreview into account (bug 1341)
					if ( this.AutoPreviewEnabled )
					{
						this.rootHeight -= this.AutoPreviewHeight;
					}

					// SSP 11/24/03 - Scrolling Till Last Row Visible
					//
					if ( null != this.Layout )
						this.Layout.BumpGrandVerifyVersion( );

					// SSP 11/3/04 - Merged Cell Feature
					// Added BumpRowHeightVersion method. This is used for caching the height of the 
					// merged cells.
					//
					if ( null != this.ParentCollection )
						this.ParentCollection.BumpRowHeightVersion( );

					if ( notify )
					{
						// notify any interested parties
						//
						this.NotifyPropChange( PropertyIds.Height );
					}
				}
			}

			// Don't bother calling SynchSameTierParentRows if this is 
			// a band 0 row
			//
			if ( this.ParentRow != null )
				this.SynchSameTierParentRows( this.HeightIncludingPadding );

			this.SynchSameTierChildRows( this.HeightIncludingPadding );

			// reset the resizing flag
			//
			this.resizing = false;
		}


		internal override SelectChange SelectChangeType 
		{
			get
			{
				return SelectChange.Row;
			}
		}

		internal bool HasSiblingInNextSiblingBand( bool excludeHidden )
		{
			return this.HasSiblingInNextSiblingBand( excludeHidden, false );
		}

		internal bool HasSiblingInNextSiblingBand( bool excludeHidden, bool excludeCardBands )
		{
			UltraGridRow row = this;

			// SSP 4/21/04 - Virtual Binding - Optimizations
			//
			// ----------------------------------------------------------------------------------------
			if ( excludeHidden )
			{
				UltraGridRow tmpRow = row.ParentCollection.GetRowAtVisibleIndexOffset( row, row.HiddenInternal ? 0 : 1 );

				// SSP 7/1/04 UWG3452
				// If the row is hidden due to the band or the parent row being hidden, then don't
				// return true. GetRowAtVisibleIndexOffset method does not take into account the band
				// and the parent row being hidden. It only looks at the HiddenInternal.
				//
				//if ( null != tmpRow )
				if ( null != tmpRow && ! tmpRow.HiddenResolved )
					return true;

				if ( null != row.ParentRow && null != row.ParentRow.ChildBands && null != row.ParentChildBand )
				{
					UltraGridChildBand childBand = row.ParentRow.ChildBands.GetNextChildBand( row.ParentChildBand, excludeCardBands ); 
					while ( null != childBand )
					{
						tmpRow = childBand.Rows.GetFirstVisibleRow( );

						// SSP 7/1/04 UWG3452
						// If the row is hidden due to the band or the parent row being hidden, then don't
						// return true. GetRowAtVisibleIndexOffset method does not take into account the band
						// and the parent row being hidden. It only looks at the HiddenInternal.
						//
						//if ( null != tmpRow )
						if ( null != tmpRow && ! tmpRow.HiddenResolved )
							return true;

						childBand = childBand.ParentRow.ChildBands.GetNextChildBand( childBand, excludeCardBands );
					}
				}

				return false;
			}
			// ----------------------------------------------------------------------------------------

			while ( true ) 
			{
				// SSP 11/11/03 Add Row Feature
				//
				//row = row.GetSibling( Infragistics.Win.UltraWinGrid.SiblingRow.Next, true, excludeCardBands );
				row = row.GetSibling( Infragistics.Win.UltraWinGrid.SiblingRow.Next, true, excludeCardBands, IncludeRowTypes.SpecialRows );
				if ( !excludeHidden || null == row || !row.HiddenResolved )
					break;
			}

			return null != row;
		}


		internal bool HasSiblingInPrevSiblingBand( bool excludeHidden )
		{
			return this.HasSiblingInPrevSiblingBand( excludeHidden, false );
		}

		internal bool HasSiblingInPrevSiblingBand( bool excludeHidden, bool excludeCardBands )
		{
			UltraGridRow row = this;
			while ( true ) 
			{
				// SSP 11/11/03 Add Row Feature
				//
				//row = row.GetSibling( Infragistics.Win.UltraWinGrid.SiblingRow.Previous, true, excludeCardBands );
				row = row.GetSibling( Infragistics.Win.UltraWinGrid.SiblingRow.Previous, true, excludeCardBands, IncludeRowTypes.SpecialRows );
				if ( !excludeHidden || null == row || !row.HiddenResolved )
					break;
			}

			return null != row;
		}
		

		internal UltraGridRow GetPrevVisibleRelative(  )
		{
			return this.GetPrevVisibleRelative( true, false, true );
		}

		internal UltraGridRow GetNextVisibleRelative( bool spanBands, bool skipThisRow, bool spanParents )
		{

			// JJD 9/7/00 - ult1195
			// If this row is hidden check to make sure all 
			// of the rows are not hidden. Otherwise, we will
			// keep calling this method on every paint and really 
			// things down.
			//
			// MD 8/14/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.HiddenResolved )
			bool hiddenResolved = this.HiddenResolved;

			if ( hiddenResolved )
			{
				// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
				//
				// ----------------------------------------------------------------------------
				
				if ( null == this.ParentCollection || this.ParentCollection.VisibleRowCount <= 0 )
					return null;
				// ----------------------------------------------------------------------------
			}

			// if we aren't hidden then return this row
			//
			// MD 8/14/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( !skipThisRow && !this.HiddenResolved )
			if ( !skipThisRow && !hiddenResolved )
				return this;

			// if we don't have a parent (band 0) or our parent is
			// visible then attempt to get the next visible sibling
			// row
			//
			// JJD 8/4/00
			// Check to make sure the band is not hidden before trying
			// to get another row
			//
			if ( !this.band.HiddenResolved && 
				( this.ParentRow == null || 
				!this.ParentRow.HiddenResolved ) )
			{
				Infragistics.Win.UltraWinGrid.UltraGridRow   currentRow    = this;

				bool fSpanBands = ( this.ParentRow != null && spanBands );

				// JJD 9/7/00 - ult1195
				// Instead of calling this method recursively which can blow the stack
				// if there is a large number of consecutive hidden rows, use a while loop
				//
				while ( currentRow != null )
				{
					// get the next sibling row
					//
					// SSP 11/11/03 Add Row Feature
					//
					//currentRow = currentRow.GetSibling( SiblingRow.Next, fSpanBands );
					currentRow = currentRow.GetSibling( SiblingRow.Next, fSpanBands, false, IncludeRowTypes.SpecialRows );

					if ( currentRow != null && !currentRow.HiddenResolved )
						return currentRow;
				}
			}

			// If we have a parent row and the span bands parameter is true
			// then call this function on our parent
			//
			// JJD 8/4/00
			// Check to make sure the band is not hidden before trying
			// to get another row
			//
			// MD 7/27/07 - 7.3 Performance
			// Refactored Prevent calling expensive getters multiple times
			//if ( this.ParentRow != null && 
			//    spanBands && spanParents && 
			//    !this.ParentRow.Band.HiddenResolved )
			//    return this.ParentRow.GetNextVisibleRelative( spanBands, false, spanParents );
			if ( spanBands && spanParents )
			{
				UltraGridRow parentRow = this.ParentRow;

				if ( parentRow != null &&
					!parentRow.Band.HiddenResolved )
				{
					return parentRow.GetNextVisibleRelative( spanBands, false, spanParents );
				}
			}

			return null;
		}

		// SSP 9/18/02 UWG1120
		// Moved IsAbsoluteFirstRow and IsAbsoluteLastRow methods here from VisibleRow
		// because they don't rely on visible row in any way. I left the methods there
		// which just delegate the call to these methods.
		//
		internal bool IsAbsoluteFirstRow( )
		{
			// the first row must be in band 0 (no parent row)
			// and it cannot have any poervious sibling rows
			//
			// SSP 11/11/03 Add Row Feature
			//
			//if ( null != this.ParentRow  ||	this.HasPrevSibling( false, true ) )
			if ( null != this.ParentRow  ||	this.HasPrevSibling( false, true, IncludeRowTypes.SpecialRows ) )
				return false;

			return true;		
		}

		// SSP 4/23/04 - Virtual Binding Related Optimizations
		// Adde GetVisibleSiblingRow method.
		//
		internal UltraGridRow GetVisibleSiblingRow( bool next, bool acrossSiblingBands, IncludeRowTypes includeRowTypes )
		{
			if ( null == this.ParentCollection )
				return null;

			// SSP 5/10/05 BR03761
			// Pass along the includeRowTypes.
			//
			//UltraGridRow row = next ? this.ParentCollection.GetNextVisibleRow( this ) : this.ParentCollection.GetPrevVisibleRow( this );
			UltraGridRow row = next 
				? this.ParentCollection.GetNextVisibleRow( this, includeRowTypes )
				: this.ParentCollection.GetPrevVisibleRow( this, includeRowTypes );
		
			if ( null == row && acrossSiblingBands && null != this.ParentRow 
				&& null != this.ParentRow.ChildBands && null != this.ParentChildBand )
			{
				UltraGridChildBand siblingChildBand = this.ParentChildBand;

				while ( null == row && null != siblingChildBand )
                {
					// SSP 1/27/05 BR00167
					// Depending on the next parameter get the next or the previous sibling band.
					//
					//siblingChildBand = siblingChildBand.ParentRow.ChildBands.GetNextChildBand( siblingChildBand, false );
					siblingChildBand = next
						? siblingChildBand.ParentRow.ChildBands.GetNextChildBand( siblingChildBand, false )
						: siblingChildBand.ParentRow.ChildBands.GetPreviousChildBand( siblingChildBand, false );

					if ( null != siblingChildBand && ! siblingChildBand.Band.HiddenResolved )
					{
						// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
						// Added overloads that take in includeRowTypes and includeSpecialRows.
						//
						// --------------------------------------------------------------------------------------
						row = next
							? siblingChildBand.Rows.GetFirstVisibleRow( includeRowTypes )
							: siblingChildBand.Rows.GetLastVisibleRow( includeRowTypes );
						
						// --------------------------------------------------------------------------------------
					}
				}
			}

			return row;
		}

		// SSP 9/19/02 UWG1678
		// Added GetNextVisibleRow
		//
		internal UltraGridRow GetNextVisibleRow( )
		{
			return this.GetNextVisibleRow( false );
		}
		
		internal UltraGridRow GetNextVisibleRow( bool skipThisRowDescendants )
		{
			UltraGridRow row = null;

			// If a child is visible, then return that.
			//
			if ( !skipThisRowDescendants && this.Expanded && !this.HiddenResolved )
			{
				// SSP 4/21/04 - Virtual Binding Related Optimizations
				// Instead of looping through the child rows, use the GetItemAtScrollIndex
				// method to get the visible row.
				//
				// --------------------------------------------------------------------------------
				
				if ( null != this.ChildBands )
					row = this.ChildBands.GetItemAtScrollIndex( 0 );
				// --------------------------------------------------------------------------------
			}

			if ( null == row || row.HiddenResolved )
			{
				// SSP 4/21/04 - Virtual Binding Related Optimizations
				// Instead of looping through the child rows, use the GetRowAtVisibleIndexOffset
				// method to get the visible row.
				//
				// --------------------------------------------------------------------------------
				
				row = this.GetVisibleSiblingRow( true, true, IncludeRowTypes.SpecialRows );
				// --------------------------------------------------------------------------------
			}
            
			if ( null == row || row.HiddenResolved )
			{
				// Get the parent row or it's visible sibling if the parent row is hidden
				//
				// SSP 4/21/04 - Virtual Binding Related Optimizations
				// Instead of looping through the child rows, use the GetRowAtVisibleIndexOffset
				// method to get the visible row.
				//
				// --------------------------------------------------------------------------------
				
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null != this.ParentRow )
				//    row = this.ParentRow.GetVisibleSiblingRow( true, true, IncludeRowTypes.SpecialRows );
				UltraGridRow parentRow = this.ParentRow;

				if ( null != parentRow )
					row = parentRow.GetVisibleSiblingRow( true, true, IncludeRowTypes.SpecialRows );
				// --------------------------------------------------------------------------------
			}

			if ( null != row && !row.HiddenResolved )
				return row;

			return  null;
		}

		// SSP 9/19/02 UWG1678
		// Added GetVisibleDescendant
		//
		private UltraGridRow GetVisibleDescendant( bool last, bool includeThisRow )
		{
			if ( !this.HiddenResolved )
			{
				if ( this.Expanded )
				{
					// SSP 11/11/03 Add Row Feature
					//
					//UltraGridRow row = this.GetChild( !last ? ChildRow.First : ChildRow.Last );
					UltraGridRow row = this.GetChild( !last ? ChildRow.First : ChildRow.Last, null, IncludeRowTypes.SpecialRows );

					while ( null != row && row.HiddenResolved )
						// SSP 11/11/03 Add Row Feature
						//
						//row = row.GetSibling( !last ? SiblingRow.Next : SiblingRow.Previous, true, false );
						row = row.GetSibling( !last ? SiblingRow.Next : SiblingRow.Previous, true, false, IncludeRowTypes.SpecialRows );

					if ( null != row && !row.HiddenResolved )
					{
						row = row.GetVisibleDescendant( last, true );

						if ( null != row && !row.HiddenResolved )
							return row;
					}
				}

				if ( includeThisRow )
					return this;
			}

			return null;
		}

		// SSP 9/19/02 UWG1678
		// Added GetPrevVisibleRow
		//
		internal UltraGridRow GetPrevVisibleRow( )
		{
			UltraGridRow row = null;
			
			// If a prev sibling is visible, then return that.
			//
			// SSP 11/11/03 Add Row Feature
			//
			//row = this.GetSibling( SiblingRow.Previous, true, false );
			row = this.GetSibling( SiblingRow.Previous, true, false, IncludeRowTypes.SpecialRows );

			while ( null != row && row.HiddenResolved )
				// SSP 11/11/03 Add Row Feature
				//
				//row = row.GetSibling( SiblingRow.Previous, true, false );
				row = row.GetSibling( SiblingRow.Previous, true, false, IncludeRowTypes.SpecialRows );
			
			if ( null != row && !row.HiddenResolved )
				return row.GetVisibleDescendant( true, true );

			// If the parent is visible, then return that or else return its
			// prev visible siblings.
			//
			// MD 8/2/07 - 7.3 Performance
			// Refactored - Prevent calling expensive getters multiple times
			//if ( null != this.ParentRow && !this.ParentRow.HiddenResolved )
			//    return this.ParentRow;
			//
			//row = this.ParentRow;
			row = this.ParentRow;

			if ( null != row && !row.HiddenResolved )
				return row;

			while ( null != row && row.HiddenResolved )
				// SSP 11/11/03 Add Row Feature
				//
				//row = row.GetSibling( SiblingRow.Previous, true, false );
				row = row.GetSibling( SiblingRow.Previous, true, false, IncludeRowTypes.SpecialRows );

			if ( null != row && !row.HiddenResolved )
				return row.GetVisibleDescendant( true, true );

			return  null;
		}
	    
		// SSP 9/18/02 UWG1120
		// Moved IsAbsoluteFirstRow and IsAbsoluteLastRow methods here from VisibleRow
		// because they don't rely on visible row in any way. I left the methods there
		// which just delegate the call to these methods.
		//
		internal bool IsAbsoluteLastRow( bool expandedOnly )
		{			
			// the last row cannot have any next siblings
			//
			// SSP 11/11/03 Add Row Feature
			//
			//if ( this.HasNextSibling( false, true ) ||
			if ( this.HasNextSibling( false, true, IncludeRowTypes.SpecialRows ) ||
				this.HasSiblingInNextSiblingBand(true) )
				return false;

			// SSP 9/18/02 UWG1120
			// If any one of the ancestore row has a next sibling, then this is not
			// the absolute last row.
			//
			// --------------------------------------------------------------------
			UltraGridRow parentRow = this.ParentRow;

			while ( null != parentRow )
			{
				// SSP 11/11/03 Add Row Feature
				//
				//if ( parentRow.HasNextSibling( true, true ) )
				if ( parentRow.HasNextSibling( true, true, IncludeRowTypes.SpecialRows ) )
					return false;

				parentRow = parentRow.ParentRow;
			}
			// --------------------------------------------------------------------

			// if the row passed the above text and it's band
			// doesn't have any child bands then it is the last
			// row so return true
			//
			if ( !this.Band.HasChildBands() )
				return true;

			// we are interested in expanded rows only or the
			// row is expanded then return false if the row
			// has any child rows
			// 
			if ( !expandedOnly || this.Expanded )
				// SSP 11/11/03 Add Row Feature
				//
				//return !this.HasChild(true); /* .GetHasChildren(true); */
				return !this.HasChild( true, IncludeRowTypes.SpecialRows ); 

			return true;
		}

		internal UltraGridRow GetPrevVisibleRelative( bool spanBands, bool skipThisRow, bool spanParents )
		{

			// JJD 9/7/00 - ult1195
			// If this row is hidden check to make sure all 
			// of the rows are not hidden. Otherwise, we will
			// keep calling this method on every paint and really 
			// things down.
			//
			// MD 8/14/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.HiddenResolved )
			bool hiddenResolved = this.HiddenResolved;

			if ( hiddenResolved )
			{
				// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
				//
				// ----------------------------------------------------------------------------
				
				if ( null == this.ParentCollection || this.ParentCollection.VisibleRowCount <= 0 )
					return null;
				// ----------------------------------------------------------------------------
			}

			// if we aren't hidden then return this row
			//
			// MD 8/14/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( !skipThisRow && !this.HiddenResolved )
			if ( !skipThisRow && !hiddenResolved )
				return this;

			// if we don't have a parent (band 0) or our parent is
			// visible then attempt to get the next visible sibling
			// row
			//
			// JJD 8/4/00
			// Check to make sure the band is not hidden before trying
			// to get another row
			//
			if ( !this.band.HiddenResolved && 
				( this.ParentRow == null || 
				!this.ParentRow.HiddenResolved ) )
			{
				Infragistics.Win.UltraWinGrid.UltraGridRow   currentRow    = this;

				bool fSpanBands = ( this.ParentRow != null && spanBands );

				// JJD 9/7/00 - ult1195
				// Instead of calling this method recursively which can blow the stack
				// if there is a large number of consecutive hidden rows, use a while loop
				//
				while ( currentRow != null )
				{
					// get the next sibling row
					//
					// SSP 11/11/03 Add Row Feature
					//
					//currentRow = currentRow.GetSibling( SiblingRow.Previous, fSpanBands );
					currentRow = currentRow.GetSibling( SiblingRow.Previous, fSpanBands, false, IncludeRowTypes.SpecialRows );

					if ( currentRow != null && !currentRow.HiddenResolved )
						return currentRow;
				}
			}

			// If we have a parent row and the span bands parameter is true
			// then call this function on our parent
			//
			// JJD 8/4/00
			// Check to make sure the band is not hidden before trying
			// to get another row
			//
			// MD 8/2/07 - 7.3 Performance
			// Refactored - Prevent calling expensive getters multiple times
			//if ( this.ParentRow != null && 
			//    spanBands && spanParents && 
			//    !this.ParentRow.Band.HiddenResolved )
			//    return this.ParentRow.GetPrevVisibleRelative( spanBands, false, spanParents );
			if ( spanBands && spanParents )
			{
				UltraGridRow parentRow = this.ParentRow;

				if ( parentRow != null &&
					!parentRow.Band.HiddenResolved )
				{
					return parentRow.GetPrevVisibleRelative( spanBands, false, spanParents );
				}
			}

			return null;
		}
		
		internal virtual Activation GetResolvedCellActivation( UltraGridColumn column )
		{
			// Use a stack variable to hold the temp value instead of
			// of placing the method call inside the 'max' macro
			// below since that causes the method to be called twice
			// 

			// SSP 8/1/03 UWG1654 - Filter Action
			// Implemented Filter action (ShowEnabled, ShowDisabled, Hide).
			// If the row filter action is to disable the filtered out rows and the row is filtered out
			// then return Disabled.
			//
			// ------------------------------------------------------------------------------------------------
			if ( this.IsFilteredOut && RowFilterAction.DisableFilteredOutRows == this.BandInternal.RowFilterActionResolved )
				return Activation.Disabled; 
			// ------------------------------------------------------------------------------------------------
			
			Activation aTemp = column.ActivationResolved;

			// SSP 4/20/05 - NAS 5.2 Extension of Summaries Functionality
			// Use the ActivationResolved instead of the member var.
			//
			//return (Activation)System.Math.Max( (int)this.activation, (int)aTemp );
			// SSP 5/2/05 BR04321
			// After discussing with Joe, the behavior was changed so Disabled takes precedence 
			// over the NoEdit. Use the new MaxActivation method instead.
			// 
			//return (Activation)System.Math.Max( (int)this.ActivationResolved, (int)aTemp );
			return GridUtils.MaxActivation( this.ActivationResolved, aTemp );
		}

		internal override Activation ActivationResolved
		{
			get
			{
				return this.activation;
			}
		}

		// SSP 8/19/03 UWG2589
		// Added GetCellActivationResolved method.
		//
		/// <summary>
		/// Returns the resolved activation of the cell associated with this row and the column.
		/// </summary>
		/// <param name="column">Associated <b>Column</b></param>
        /// <returns>The resolved activation of the cell associated with this row and the column.</returns>
		public Activation GetCellActivationResolved( UltraGridColumn column )
		{
			UltraGridCell cell = this.GetCellIfAllocated( column );
			return null != cell ? cell.ActivationResolved : this.GetResolvedCellActivation( column );
		}

		// SSP 7/19/05
		// TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
		// need to resolve the TipStyleScroll differently based on whether the scrolling logic 
		// or the row connector logic is calling it.
		// 
		//internal string GetScrollTip( ref int lineCount  )
		internal string GetScrollTip( ref int lineCount, bool forRowConnectorTooltip )
		{
			string tipString = null;
			// if we have a parent row 

			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( null != this.ParentRow )
			//    // SSP 7/19/05
			//    // TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
			//    // need to resolve the TipStyleScroll differently based on whether the scrolling logic 
			//    // or the row connector logic is calling it.
			//    // 
			//    //tipString = this.ParentRow.GetScrollTip( ref lineCount );
			//    tipString = this.ParentRow.GetScrollTip( ref lineCount, forRowConnectorTooltip );
			UltraGridRow parentRow = this.ParentRow;

			if ( null != parentRow )
				// SSP 7/19/05
				// TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
				// need to resolve the TipStyleScroll differently based on whether the scrolling logic 
				// or the row connector logic is calling it.
				// 
				//tipString = this.ParentRow.GetScrollTip( ref lineCount );
				tipString = parentRow.GetScrollTip( ref lineCount, forRowConnectorTooltip );

			// SSP 7/19/05
			// TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
			// need to resolve the TipStyleScroll differently based on whether the scrolling logic 
			// or the row connector logic is calling it.
			// 
			//if ( TipStyle.Hide == this.Band.TipStyleScrollResolved )
			if ( TipStyle.Hide == this.Band.TipStyleScrollResolved( forRowConnectorTooltip ) )
				return tipString;

			// get the band's scroll tip column
			//
			UltraGridColumn scrollTipCol = this.Band.ScrollTipCol;

			if ( null == scrollTipCol )
				return tipString;

			string strWork = string.Empty;

			for ( int i = 0; i < lineCount; i++ )
				strWork += "    ";

			// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries
			// Added RowScrollTip virtual property on the row which is overridden by 
			// UltraGridGroupByRow so the following block is not necessary anymore.
			// 
			

			// SSP 1/10/02
			// Instead of forcing the creation of the cell, we can get the text
			// by calling GetCellText off the row. 
			//			
			//UltraGridCell cell = null;
			

			// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries
			// Added RowScrollTip property. Use that instead. Code from here was moved into it.
			// 
			// --------------------------------------------------------------------------------
			
			string rowScrollTip = this.RowScrollTip;
			if ( null != rowScrollTip )
				strWork += this.RowScrollTip;
			// --------------------------------------------------------------------------------

			if ( null == tipString )
				tipString = string.Empty;

			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( null != this.ParentRow && tipString.Length > 0 )
			if ( null != parentRow && tipString.Length > 0 )
				tipString += "\r\n";

			tipString += strWork;

			lineCount++;

			return tipString;
		}

		internal override bool SetFocusAndActivate()
		{
			return this.SetFocusAndActivate ( true, false, false );
		}

		internal override bool SetFocusAndActivate(bool byMouse)
		{
			return this.SetFocusAndActivate ( byMouse, false, false );
		}

		internal override bool SetFocusAndActivate( bool byMouse, bool enterEditMode )
		{
			return this.SetFocusAndActivate ( byMouse, enterEditMode, false );
		}

		internal override bool SetFocusAndActivate( bool byMouse, bool enterEditMode, bool byTabKey )
		{
			if ( this.Activate() )
				return true;
			return false;
		}

		/// <summary>
		/// Activates row. Returns true if the operation was cancelled.
		/// </summary>
		/// <returns>Retruns false if successful and true otherwise.</returns>
		/// <remarks>
		/// <seealso cref="UltraGridBase.ActiveRow"/>
		/// <seealso cref="UltraGrid.ActiveCell"/>
		/// <seealso cref="UltraGridRow.Activated"/>
		/// </remarks>
		public bool Activate()
		{
			Debug.Assert(this.Layout.Grid != null, "No grid ptr in Row::Activate");

			// SSP 1/27/05 BR02053
			// Only do this for the display layout. Print and export layouts should not be
			// modifying the ActiveRow of the grid.
			//
			if ( ! this.Layout.IsDisplayLayout )
				return true;

			// if is disabled then return true
			//
			//	if ( this.IsDisabled )
			//		return true;

			if ( this.Layout.ActiveRow == this )
			{
				UltraGrid grid = this.Layout.Grid as UltraGrid;

				// SSP 2/24/05 BR02492
				// Clear the temp active row.
				//
				if ( null != this.Layout.Grid )
					this.Layout.Grid.SetTempActiveRow( null, false );

				if ( null != grid )
				{
					// first clear out the active cell (if there is one)
					//
					grid.ActiveCell = null;

					// SSP 9/23/03 UWG2674
					// If the clearing of the active cell fails then return true which means canceled. 
					// The reason for doing this is that if the user cancels exit edit mode or some 
					// other event then cell is still active then the row activation from the selection
					// strategy point of view has failed. Remember you can't select rows while there 
					// is a cell that's active or in edit mode.
					//
					if ( null != grid.ActiveCell )
						return true;
				}

				return false;
			}

			// call the ActiveRow method off the grid (this will fire
			// the necessary events)
			//
			this.Layout.Grid.ActiveRow = this;

			// return true if the operation was cancelled
			//
			if ( this.Layout.ActiveRow != this )
				return true;

			// if not cancelled, scroll new row into view

			this.Layout.Grid.ActiveRowScrollRegion.ScrollRowIntoView(this);
			return false;
		}

		internal void SetDataChanged( bool newValue )
		{
			if ( newValue != this.dataChanged )
			{
				// SSP 7/26/04 - UltraCalc
				// If SetDataChanged gets called due to us setting this cell's value to
				// it's formula evaluation result, don't show the pencil icon on the row
				// selector. What happens is that if you have a column with a formula,
				// when the grid comes up all the row selectors are showing the pencil
				// icon because all the cells in the formula column got their values set 
				// to the formula results.
				//
				// ----------------------------------------------------------------------
				if ( newValue && this.HasCalcReference 
					&& ((RowReference)this.CalcReference).inCellReferenceValueSet )
					return;
				// ----------------------------------------------------------------------

				this.dataChanged = newValue;
 
				// notify any interested parties
				//
				// SSP 11/14/03 Add Row Feature
				// Check for the row being disposed. If the row is disposed, then calling NotifyPropChange
				// will cause an exception since it verifies that the subobject is not disposed off
				// and throws an exception if it is.
				//
				if ( ! this.Disposed )
					this.NotifyPropChange( PropertyIds.DataChanged );

				// SSP 8/17/01 
				// We are not keeping track of rows that are dirty in an update
				// row list any more. So the code below is not needed.
				
			}

			// Only set all the cell's datachanged
			// properties if the row is being set to false.
			// Otherwise, you dirty one cell, and all their 
			// data changed properties were being set.
			//if ( newValue )
			// SSP 8/17/01 UWG132
			// Above comment says it all
			if ( !newValue )
			{
				// SSP 11/17/05 BR07772
				// If the column is bound then we need to honor the UpdateData method
				// call even if modifications were made via the calc manager.
				// 
				this.dataChangedViaCalcManager = false;

				// Set all the Cells DataChanged Properties				
				CellsCollection cellsColl = this.Cells;

				if ( null != cellsColl )
				{
					for ( int i = 0; i < cellsColl.Count; i++ )
					{							
						// The Cells Collection supports lazy creation of 
						// cell objects, with NULL placeholders. Check to
						// ensure we are dealing with a real object.
						if ( cellsColl.HasCell( i ) )
							cellsColl[i].SetDataChanged( newValue );
					}					
				}
			}
		}

		/// <summary>
		/// This is used by the DependentTextUIElement class to get necessary information for rendering text
		/// </summary>	
		/// These functions will not be used, so they are just for sake of providing
		/// implementation for the interface
		UIElementBorderStyle IUIElementProvider.GetBorderStyle( UIElement element )
		{
			return UIElementBorderStyle.None;
		}
		Border3DSide IUIElementProvider.GetBorderSides( UIElement element )
		{
			return (Border3DSide)0;
		}		
		System.Windows.Forms.Cursor IUIElementProvider.GetCursor( UIElement element )
		{
			return element.Parent.Cursor;
		}
		void IUIElementProvider.InitElementAppearance ( UIElement element,
			ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
		}
		String IUIElementTextProvider.GetText( DependentTextUIElement element )
		{
			Infragistics.Win.UltraWinGrid.UltraGridColumn column = 
				(Infragistics.Win.UltraWinGrid.UltraGridColumn)element.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridColumn ), true );

			Debug.Assert( null != column, "null column in IUIElementTextProvider.GetText" );
		
			string text = null;

			// SSP 8/14/01 - UWG36 
			// added code for applying mask to the string
			if ( null != column )
			{
				text = this.GetCellText( column );
				
				if ( null != text && null != column )
					text = column.FormatWithMaskMode( text, column.MaskDisplayMode );

				if ( null == text )
					text = string.Empty;

				return text;
			}

			return string.Empty;
		}

		bool IUIElementTextProvider.IsMultiLine( DependentTextUIElement element )
		{
			UltraGridColumn column = (Infragistics.Win.UltraWinGrid.UltraGridColumn)element.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridColumn ), true );

			Debug.Assert( null != column, "null column in IUIElementTextProvider.GetText" );

			if ( null != column )
			{
				return column.IsCellMultiLine;
			}
			return true;
		}

		// SSP 2/5/02 
		// Added.
		bool IUIElementTextProvider.WrapText( DependentTextUIElement element )
		{
			return ((IUIElementTextProvider)this).IsMultiLine( element );
		}

		void IUIElementTextProvider.GetTextPadding( DependentTextUIElement element, ref Size padding )
		{
		}
		void IUIElementTextProvider.AdjustTextDisplayRect( DependentTextUIElement element, ref Rectangle displayRect )
		{
			UltraGridColumn column = (Infragistics.Win.UltraWinGrid.UltraGridColumn)element.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridColumn ), true );

			Debug.Assert( null != column, "null column in IUIElementTextProvider.GetText" );

			if ( null != column )
			{			
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//if ( Infragistics.Win.UltraWinGrid.ColumnStyle.Button == column.StyleResolved )
				if ( Infragistics.Win.UltraWinGrid.ColumnStyle.Button == column.GetStyleResolved( this ) )
				{
					CellButtonUIElement btnCell = (CellButtonUIElement)
						element.GetAncestor( typeof (CellButtonUIElement) );
    
					if ( null != btnCell && btnCell.IsPressed )
						displayRect.Offset( 2, 2 );
				}
			}
		}
		bool IUIElementTextProvider.IsVertical( DependentTextUIElement element )
		{				
			return false;
		}	
	
		// AS - 1/21/02 UWG847
		/// <summary>
		/// Indicates whether the element renders mnemonics in the text.
		/// </summary>
		System.Drawing.Text.HotkeyPrefix IUIElementTextProvider.HotkeyPrefix( DependentTextUIElement element )
		{
			return System.Drawing.Text.HotkeyPrefix.None;
		}

		internal UltraGridRow GetCrossParentSibling(bool previous, bool spanBands )
		{
			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.ParentRow == null )
			UltraGridRow parentRow = this.ParentRow;

			if ( parentRow == null )
			{
				//Debug.Fail("ParentRow == null in Row::GetCrossParentSibling");
				return null;
			}

			// MD 8/2/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//UltraGridRow siblingParent = this.ParentRow;
			UltraGridRow siblingParent = parentRow;

			UltraGridRow nextSiblingParent = null;
					
			while ( siblingParent != null )
			{
				// get the next/prev sibling of our parent that has children
				// in our band
				//
							
				nextSiblingParent = siblingParent.GetSiblingWithChildrenInBand( (spanBands) ? null : band, previous );
				
				// if successful we can stop here
				//
				if ( nextSiblingParent != null )
				{
					siblingParent = nextSiblingParent;
					break;
				}

				// get a cros parent sibling of the parent (this recursively
				// walks up the parent chain)
				//
				nextSiblingParent = siblingParent.GetCrossParentSibling( previous, spanBands );
						
				siblingParent  = nextSiblingParent;

				if ( siblingParent != null )
				{
					bool hasChild = false;

					// if that parent has children we can stop here
					//
					// Changed to use optional variant
					// SSP 11/11/03 Add Row Feature
					//
					//hasChild = siblingParent.HasChild( spanBands ? null : this.band);
					hasChild = siblingParent.HasChild( spanBands ? null : this.band, IncludeRowTypes.SpecialRows );
					if ( hasChild )
						break;
				}
			}

			// JJD 10/03/01 - UWG460
			// If the siblingParent is null return null.
			// Otherwise we blow up below.
			//
			if ( siblingParent == null )
				return null;
		
			// get the first child row in this band
			//	
			// SSP 11/11/03 Add Row Feature
			//
			//return  siblingParent.GetChild( previous ? ChildRow.Last : ChildRow.First, (spanBands) ? null : this.band );			
			return  siblingParent.GetChild( previous ? ChildRow.Last : ChildRow.First, (spanBands) ? null : this.band, IncludeRowTypes.SpecialRows );
		}
		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetSiblingWithChildrenInBand( UltraGridBand  childBand, bool previous )
		{
			UltraGridRow siblingRow = this;
			
			// SSP 9/25/02 UWG1375
			// This is not valid anymore because with the group by rows, child band doesn't necessarily
			// have to be child band of this row's band since the child rows of a group by row are
			// from the same band.
			// Commented out below block of code.
			//
			

			SiblingRow siblingRowAction = previous ? SiblingRow.Previous : SiblingRow.Next;
			int chapteredCol = childBand.ParentBand != null ? childBand.ParentColumn.Index : -1;
			
			// stop when we have found a sibling with children
			//
			bool hasChild = false;

			// keep getting the next or previous sibling until we either
			// find one with child rows (in the passed in band) or we reach
			// eof
			//
			while ( !hasChild )
			{
				UltraGridRow nextSiblingRow = null;
				// SSP 11/11/03 Add Row Feature
				//
				//nextSiblingRow = siblingRow.GetSibling( siblingRowAction, (chapteredCol < 0) );
				nextSiblingRow = siblingRow.GetSibling( siblingRowAction, (chapteredCol < 0), false, IncludeRowTypes.SpecialRows );
				
				if ( nextSiblingRow == null )
					// SSP 5/29/02 UWG1137
					// Instead of returning the siblingRow return null if we haven't found 
					// a sibling with children yet (we woudn't be in this loop if we have
					// found one).
					//
					//return siblingRow;
					return null;

				siblingRow = nextSiblingRow;

				// see if this sibling has any children in the requested band
				//			
				if ( chapteredCol < 0 )
					// SSP 11/11/03 Add Row Feature
					//
					//hasChild = siblingRow.HasChild(false);
					hasChild = siblingRow.HasChild( false, IncludeRowTypes.SpecialRows );
				else
				{
					// JJD 10/04/01
					// Pass the child band in since that is the whole point
					//
					//					hasChild = siblingRow.HasChild( );
					// SSP 11/11/03 Add Row Feature
					//
					//hasChild = siblingRow.HasChild( childBand );
					hasChild = siblingRow.HasChild( childBand, IncludeRowTypes.SpecialRows );
				}
				
			}

			
			return siblingRow;

		}

		
		internal void RestoreAllUnboundCells()
		{
			// Only need to check if we have a cells collection.
			// 
			if ( this.cells != null )
			{    
				for( int index = 0; index < this.cells.Count; index++ )
				{
					if ( this.cells[index] != null )
						this.cells[index].RestoreUnboundData();
				}
			}
		}


		internal void SaveOriginalValForUnboundCells()
		{
			// Only need to check if we have a cells collection.
			// 
			if ( this.cells != null )
			{    
				for( int index = 0; index < this.cells.Count; index++ )
				{
					// SSP 10/17/04
					// Accessing the indexer will end up creating the cell even if it wasn't 
					// allocated. Use HasCell instead to find out if it was allocated or not.
					//
					//if ( this.cells[index] != null )
					if ( this.cells.HasCell( index ) )
						this.cells[index].SaveOriginalVal();
				}
			}

		}

		internal override ISelectionStrategy SelectionStrategyDefault
		{
			get
			{
				return ((ISelectionStrategyProvider)(Band)).SelectionStrategyRow;
			}
		}

		internal override void AddToCollection( Selected selected )
		{
			if ( selected == null )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_237"));

			selected.Rows.InternalAdd( this );
		}
		internal override void RemoveFromCollection( Selected selected )
		{
			Debug.Assert( selected.Rows != null, "No collection to remove from!" );

			selected.Rows.Remove( this );
		}


		internal SelectedRowsCollection GetRowsForSelection( bool select )
		{
			// SSP 10/4/01
			// Selection should be using SelectedRowsCollection, not
			// RowsCollection
			//RowsCollection rows = new RowsCollection( this.Band, null );
			SelectedRowsCollection rows = new SelectedRowsCollection( );
			GridItemBase pivotItem = this.Layout.Grid.ActiveRowScrollRegion.GetPivotItem();

			//Row pivotRow = pivotItem.GetRow();
			UltraGridRow pivotRow = pivotItem as UltraGridRow;
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added UltraGridFilterCell class.
			//
			//if ( pivotRow == null && typeof(UltraGridCell) == pivotItem.GetType() )
			if ( pivotRow == null && pivotItem is UltraGridCell )
				pivotRow = ((UltraGridCell)pivotItem).Row;

			if ( pivotRow == null ) return rows;

			// JJD 1/11/2002
			// Use Index as well as scroll position. We need to do this for
			// card view but it should work for all views.
			//
			//			int position = this.ScrollPosition;
			//			int positionPivotItem = pivotRow.ScrollPosition;
			// SSP 5/29/02 UWG1137
			// Use the new OverallSelectionPosition property instead of doing
			// the addition of scroll position and index here.
			//
			//int position = this.ScrollPosition + this.Index;
			//int positionPivotItem = pivotRow.ScrollPosition + pivotRow.Index;
			long position = this.OverallSelectionPosition;
			long positionPivotItem = pivotRow.OverallSelectionPosition;


			//			if ( position == positionPivotItem )
			//				rows.Add( this );

			// Select the range between position and positionPivotItem.  If positions 
			// are the same, we only want to select the pivot item below.
			// If item is selected and we are toggling (unselecting), there is nothing to
			// do here.  We simply want to add the initial selection, which
			// is outside the range of position to positionPivotItem.
			if ( position != positionPivotItem && select )
			{
				//				SiblingRow sibling;
				NavigateType navType;

				// if position of clicked-on item is > than pivot item, we want to
				// start at the pivot item and get the next item until we reach the
				// clicked on item, otherwise we get the previous item
				if ( position > positionPivotItem )
				{
					//					sibling = SiblingRow.Next;
					navType = NavigateType.Next;
				}
				else
				{
					//					sibling = SiblingRow.Previous;
					navType = NavigateType.Prev;
				}

				// get next/previous item

				// JJD 1/11/02
				// Call GetRelatedVisibleRow instead since it will handle cross parent 
				// siblings correctly
				//
				//UltraGridRow siblingRow = pivotRow.GetSibling( sibling, false );
				UltraGridRow siblingRow = this.Layout.GetRelatedVisibleRow( pivotRow, navType, false, true, true );

				// while we have items...
				while ( siblingRow != null )
				{
					// .. add them to the collection
					//newSelection.Rows.InternalAdd( siblingRow );
					
					// SSP 10/4/01
					// Change from using RowsCollection to SelectedRowsCollection
					// necessiated below change
					//
					//rows.Add( siblingRow );
					// SSP 11/13/03 Add Row Feature
					// Only add the row if its CanSelectRow returns true.
					// Added if condition to the existing line of code.
					//
					// SSP 4/20/05 - NAS 5.2 Filter Row
					// Use the Selectable property instead.
					//
					//if ( siblingRow.CanSelectRow )
					if ( siblingRow.Selectable )
						rows.InternalAdd( siblingRow );
					
					// when we reach the clicked-on item (ourself), break
					if ( siblingRow == this )
						break;
					// get next/previous item

					// JJD 1/11/02
					// Call GetRelatedVisibleRow instead since it will handle cross parent 
					// siblings correctly
					//
					//					siblingRow = siblingRow.GetSibling( sibling, false );

					siblingRow = this.Layout.GetRelatedVisibleRow( siblingRow, navType, false, true, true );
				}
			}

			//			if ( pivotRow.selected && select )
			if ( select )
				//rows.Insert( 0, pivotRow );
				rows.InternalInsert( 0, pivotRow );
				

			return rows;
		}


		internal override void CalcSelectionRange( Infragistics.Win.UltraWinGrid.Selected newSelection, 
			bool clearExistingSelection,
			bool select,
			Selected initialSelection )
		{
			GridItemBase pivotItem = this.Layout.Grid.ActiveRowScrollRegion.GetPivotItem();

			// SSP 7/22/02 UWG1380
			// If the pivotItem is null then do the same. Make it the pivot
			// item and select it. We don't need the below assert anymore due to
			// the changes made for this bug.
			//
			//Debug.Assert( pivotItem != null, "pivotItem null!" );
			
			// if the pivot item is a cell, change pivot item to row and then select
			// that row (same as clicking row)
			if (
				// SSP 7/22/02 UWG1380
				// If the pivotItem is null then do the same. Make it the pivot
				// item and select it.
				// Added null == pivotItem clause.
				//
				null == pivotItem ||
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Added UltraGridFilterCell class.
				//
				//pivotItem.GetType() == typeof(UltraGridCell) 
				pivotItem is UltraGridCell )
			{
				this.Layout.Grid.ActiveRowScrollRegion.SetPivotItem( this );
				CalcSelectionRange( newSelection, 
					clearExistingSelection, 
					select,
					initialSelection );
				return;
			}

			// bands must be the same
			Debug.Assert( pivotItem.Band == this.band, "Bands don't match!" );

			UltraGridRow pivotRow = pivotItem as UltraGridRow;

			SelectedRowsCollection rows = this.GetRowsForSelection( select );
			foreach ( UltraGridRow row in rows )
				newSelection.Rows.InternalAdd( row, false );

			rows = null;


			if ( ! clearExistingSelection )
			{
				// SSP 5/29/02 UWG1137
				//int positionToTest;
				long positionToTest;

				// JJD 1/11/2002
				// Use Index as well as scroll position. We need to do this for
				// card view but it should work for all views.
				//
				//				int position = this.ScrollPosition;
				//				int positionPivotItem = pivotRow.ScrollPosition;
				// SSP 5/29/02 UWG1137
				// Use the new OverallSelectionPosition property instead of doing
				// the addition of scroll position and index here.
				//
				//int position = this.ScrollPosition + this.Index;
				//int positionPivotItem = pivotRow.ScrollPosition + pivotRow.Index;
				long position = this.OverallSelectionPosition;
				long positionPivotItem = pivotRow.OverallSelectionPosition;

				//MergeSelectionWithInitialSelection();
				foreach ( UltraGridRow row in initialSelection.Rows )
				{
					// JJD 10/15/01
					// Get the position once to avoid unnecessary
					// overhead
					//
					// JJD 1/11/2002
					// Use Index as well as scroll position. We need to do this for
					// card view but it should work for all views.
					//
					//					positionToTest = row.ScrollPosition;
					// SSP 5/29/02 UWG1137
					// Use the new OverallSelectionPosition property instead of doing
					// the addition of scroll position and index here.
					//
					//positionToTest = row.ScrollPosition + row.Index;
					positionToTest = row.OverallSelectionPosition;


					// JJD 10/15/01 - UWG523
					// Don't re-add the if the position is equal to the pivot
					//
					
					if ( position > positionPivotItem )
					{
						if ( positionToTest < positionPivotItem ||
							positionToTest > position )							
							newSelection.Rows.InternalAdd( row );
					}
					else 
					{
						if ( positionToTest < position ||
							positionToTest > positionPivotItem )							
							newSelection.Rows.InternalAdd( row );							
					}
				}

				// JJD 10/12/01 - UWG522
				// Copy over the selected columns also
				//
				UltraGrid grid = this.Layout.Grid as UltraGrid;

				if ( grid != null )
				{
					foreach ( ColumnHeader header in grid.Selected.Columns )
					{
						newSelection.Columns.InternalAdd( header );
					}
				}
			}
		}
		internal override void SetInitialSelection( Selected currentSelection, 
			Selected initialSelection )
		{
			if ( currentSelection.Rows != null )
			{
				// JJD 10/09/01
				// You no longer have to create the rows collection like
				// this since the property get will create it lazily
				//
				//if ( initialSelection.Rows == null )
				//	initialSelection.Rows = new SelectedRowsCollection();

				// add each cell currently selected into the initial selection collection
				foreach( UltraGridRow row in currentSelection.Rows )
					initialSelection.Rows.InternalAdd( row );
			}
		}

		#region ISparseArrayItem Interface Implementation

		#region GetOwnerData

		object ISparseArrayItem.GetOwnerData( SparseArray context )
		{
			if ( context == this.ParentCollection.SparseArray )
				return this.sortedRows_SparseArrayOwnerData;
			else
				return this.unsortedRows_SparseArrayOwnerData;
		}

		#endregion // GetOwnerData

		#region SetOwnerData

		void ISparseArrayItem.SetOwnerData( object ownerData, SparseArray context )
		{
			if ( context == this.ParentCollection.SparseArray )
				this.sortedRows_SparseArrayOwnerData = ownerData;
			else
				this.unsortedRows_SparseArrayOwnerData = ownerData;
		}

		#endregion // SetOwnerData

		#region ScrollCount
		
		int ISparseArrayMultiItem.ScrollCount
		{
			get
			{
				// SSP 3/22/05 - NAS 5.2 Fixed Rows
				// Moved the code from here into the new ScrollCountInternal property.
				//
				return this.ScrollCountInternal;
			}
		}

		#endregion // ScrollCount

		#region ScrollCountInternal

		// SSP 3/22/05 - NAS 5.2 Fixed Rows
		// Added ScrollCountInternal. Code in there is moved from ISparseArrayMultiItem.ScrollCount.
		//
		internal int ScrollCountInternal
		{
			get
			{
                // MBS 4/24/09 - TFS12665
                // Similarly to the optimizations made elsewhere, when we're performing
                // operations where we're reusing info that will not change (i.e.
                // when recreating all of the visible rows), we can cache some of the
                // more expensive operations.  In this case, we don't need to keep walking
                // up the parent chain every time that we need to ask for the height
                UltraGridLayout.CachedRowInfo rowInfo = null;
                Dictionary<UltraGridRow, UltraGridLayout.CachedRowInfo> cachedInfo = null;
                if (this.Layout != null && this.Layout.IsCachingBandInfo)
                {
                    cachedInfo = this.Layout.CachedRowData;
                    if (cachedInfo.TryGetValue(this, out rowInfo) && rowInfo.ScrollCount > -1)
                        return rowInfo.ScrollCount;
                }

				int count = 0;

				if ( ! this.HiddenInternal )
				{
					// SSP 5/2/05 - NAS 5.2 Fixed Rows/Fixed Add Row
					// This is to make fixed template add-row work. If this row is an add-row from
					// a template add-row then keep the row fixed where the template was. This means 
					// that it will have to be included in the RowsCollection.GetSpecialRows however 
					// not included in the SparseArray scroll count logic.
					//
					if (  this.parentCollection.IsFixedAddRow( this ) )
						return count;

					count++;

					// SSP 5/10/04 - Optimizations
					//
					//if ( this.Expanded && ( this.Band.HasChildBands( true ) || this.IsGroupByRow ) )
					if ( this.Expanded )
					{
						ChildBandsCollection childBands = this.ChildBands;
						if ( null != childBands )
							count += childBands.ScrollCount;
					}
				}

                // MBS 4/24/09 - TFS12665
                if (cachedInfo != null)
                {
                    if (rowInfo != null)
                        rowInfo.ScrollCount = count;
                    else
                    {
                        // We need to check to see if we've cached this during the process, since accessing
                        // the ScrollCount on the child bands may cause this to happen
                        if (!cachedInfo.TryGetValue(this, out rowInfo))
                        {
                            rowInfo = new UltraGridLayout.CachedRowInfo();
                            cachedInfo.Add(this, rowInfo);
                        }
                        rowInfo.ScrollCount = count;
                    }
                }

				return count;
			}
		}

		#endregion // ScrollCountInternal

		#region GetItemAtScrollIndex
		
		object ISparseArrayMultiItem.GetItemAtScrollIndex( int scrollIndex )
		{
			if ( 0 == scrollIndex )
				return this;
			else 
				return this.ChildBands.GetItemAtScrollIndex( scrollIndex - 1 );
		}

		#endregion // GetItemAtScrollIndex

		#endregion // ISparseArrayItem Interface Implementation

		#region NotifyScrollCountChanged

		internal void NotifyScrollCountChanged( )
		{
			if ( null != this.ParentCollection )
				this.ParentCollection.NotifyRowScrollCountChanged( this );
		}

		#endregion // NotifyScrollCountChanged

		#region CopyRowPropertiesFrom

		// SSP 1/14/03 UWG1892
		// Added retainRowPropertyCategories parameter to Print and PrintPreview methods for
		// making WYSIWIG style printing. 
		//
		internal void CopyRowPropertiesFrom( UltraGridRow source, RowPropertyCategories rowProperties )
		{
			if ( ( RowPropertyCategories.Appearances & rowProperties ) == RowPropertyCategories.Appearances )
			{
				if ( source.HasAppearance )
					this.Appearance = (Infragistics.Win.Appearance)source.Appearance.Clone( );

				if ( source.HasCellAppearance )
					this.CellAppearance = (Infragistics.Win.Appearance)source.CellAppearance.Clone( );

				if ( source.HasPreviewAppearance )
					this.PreviewAppearance = (Infragistics.Win.Appearance)source.PreviewAppearance.Clone( );

				if ( source.HasRowSelectorAppearance )
					this.RowSelectorAppearance = (Infragistics.Win.Appearance)source.RowSelectorAppearance.Clone( );
			}
				
			if ( ( RowPropertyCategories.Expanded & rowProperties )	== RowPropertyCategories.Expanded )
			{
				this.expanded = source.expanded;
			}

            // MRS NAS v8.2 - CardView Printing
            if (this.IsCardStyleCompressed &&
                ((RowPropertyCategories.CompressedCardExpansionState & rowProperties) == RowPropertyCategories.CompressedCardExpansionState))
            {
                this.IsCardCompressed = source.IsCardCompressed;
            }

			if ( ( RowPropertyCategories.Hidden & rowProperties ) == RowPropertyCategories.Hidden )
			{
				this.hidden = source.Hidden;
			}
			
			// SSP 12/16/03 UWG2800
			// Also copy the row spacing before and after settings.
			//
			if ( ( RowPropertyCategories.RowSpacing & rowProperties ) == RowPropertyCategories.RowSpacing )
			{
				this.rowSpacingBefore = source.rowSpacingBefore;
				this.rowSpacingAfter = source.rowSpacingAfter;				
			}

			// SSP 8/8/03 UWG2578
			// We need to copy row heights as well. Added Height member.
			//
			// MRS 5/21/04 - UWG2915 - Check to see if the row height was set manually
			// before copying it. Otherwise, when printing, we 
			// will copy the row height from the grid on the screen
			// and it will print incorrectly. 
			//			if ( RowPropertyCategories.Height == ( RowPropertyCategories.Height & rowProperties ) )
			//				this.Height = source.Height;
			if ( source.heightSetManually )
			{
				if ( RowPropertyCategories.Height == ( RowPropertyCategories.Height & rowProperties ) )
					this.Height = source.Height;
			}

			if ( ( ( RowPropertyCategories.UnboundData & rowProperties ) == RowPropertyCategories.UnboundData 
				// SSP 12/27/05 BR08445
				// Added CellEditorSettings to RowPropertyCategories.
				// 
				|| ( RowPropertyCategories.CellEditorSettings & rowProperties ) == RowPropertyCategories.CellEditorSettings
				// SSP 1/2/07 BR18034
				// Added Tag to RowPropertyCategories.
				// 
				|| ( RowPropertyCategories.Tag & rowProperties ) == RowPropertyCategories.Tag 
				)
				&& ! this.IsGroupByRow )
			{
				// SSP 1/2/07 BR18034
				// Added Tag to RowPropertyCategories.
				// 
				if ( ( RowPropertyCategories.Tag & rowProperties ) == RowPropertyCategories.Tag )
					this.Tag = source.Tag;

				ColumnsCollection columns = null != source.BandInternal ? source.BandInternal.Columns : null;
				if ( null != columns )
				{
					for ( int i = 0, count = columns.Count; i < count; i++ )
					{
						UltraGridColumn column = columns[i];

						// SSP 12/27/05 BR08445
						// Added CellEditorSettings to RowPropertyCategories.
						// 
						// --------------------------------------------------------------------------------------
						
						UltraGridCell sourceCell = null != column ? source.GetCellIfAllocated( column ) : null;
						if ( null == sourceCell )
							continue;

						UltraGridCell cell = this.BandInternal.Columns.Exists( column.Key ) ? this.Cells[ column.Key ] : null;
						if ( null == cell )
							continue;

						if ( ( RowPropertyCategories.UnboundData & rowProperties ) == RowPropertyCategories.UnboundData 
							&& ! column.IsBound )
							cell.Value = sourceCell.Value;

						if ( ( RowPropertyCategories.CellEditorSettings & rowProperties ) == RowPropertyCategories.CellEditorSettings )
						{
							cell.Editor = sourceCell.Editor;
							cell.ValueList = sourceCell.ValueList;
						}

						// SSP 1/2/07 BR18034
						// Added Tag to RowPropertyCategories.
						// 
						if ( ( RowPropertyCategories.Tag & rowProperties ) == RowPropertyCategories.Tag )
							cell.Tag = sourceCell.Tag;
						// --------------------------------------------------------------------------------------
					}
				}
			}

			// SSP 1/17/05 - BR01753
			// When printing or exporting to excel, copy over the formula evaluation results from the display 
			// layout to the print/export layout.
			//
			// ----------------------------------------------------------------------------------------------------
			if ( ! this.IsGroupByRow && null != source.BandInternal && null != source.Layout 
				&& null != source.Layout.CalcManager )
			{
				ColumnsCollection columns = source.BandInternal.Columns;
				if ( null != columns )
				{
					for ( int i = 0, count = columns.Count; i < count; i++ )
					{
						UltraGridColumn column = columns[i];
						if ( null != column && column.HasActiveFormula && this.Band.Columns.Exists( column.Key ) )
						{
							FormulaTargetRefBase destRef = this.Cells[ column.Key ].CalcReference;
							FormulaTargetRefBase sourceRef = source.Cells[ column ].CalcReference;
							if ( null != destRef && null != sourceRef )
								destRef.InternalCopyValueFrom( sourceRef );
						}
					}
				}
			}
			// ----------------------------------------------------------------------------------------------------

			// AS 5/30/03
			// Copy of the Appearance of the cells if specified.
			// The checks are based on the same checks we do above for
			// unbound data except we just care about if it has an appearance.
			//
			if ( ( RowPropertyCategories.CellAppearance & rowProperties ) == RowPropertyCategories.CellAppearance 
				&& ! this.IsGroupByRow )
			{
				// SSP 1/27/04 UWG2921
				// Copy over the activation settings.
				//
				this.activation = source.activation;

				if ( null != source.Band && null != source.Band.Columns )
				{
					for ( int i = 0; i < source.Band.Columns.Count; i++ )
					{
						UltraGridColumn column = source.Band.Columns[i];

						if ( null != column && source.HasCell( column ) )
						{
							UltraGridCell srcCell = source.Cells[column];
							
							// SSP 1/27/04 UWG2921
							//
							// ------------------------------------------------------------------
							UltraGridCell destCell = this.Band.Columns.Exists( column.Key )
								? this.Cells[ column.Key ] : null;

							if ( null == destCell )
								continue;
							// ------------------------------------------------------------------

							// we only need to clone it if it has at least
							// one property set
							if (srcCell.HasAppearance && srcCell.Appearance.HavePropertiesBeenSet(AppearancePropFlags.AllRender))
							{
								// SSP 10/17/03
								// Check to see if the destination row's band has a column with that key.
								// Added the if condition to the already existing line of code.
								//
								// SSP 1/27/04 UWG2921
								// Use the local var instead.
								//
								//if ( this.Band.Columns.Exists( column.Key ) )
								//	this.Cells[ column.Key ].Appearance = (Infragistics.Win.Appearance)srcCell.Appearance.Clone();
								destCell.Appearance = (Infragistics.Win.Appearance)srcCell.Appearance.Clone();
							}

							// SSP 10/17/03 UWG2661
							// Also copy the Hidden property.
							//
							if ( srcCell.Hidden )
							{
								// SSP 10/17/03
								// Check to see if the destination row's band has a column with that key.
								// Added the if condition to the already existing line of code.
								//
								// SSP 1/27/04 UWG2921
								// Use the local var instead.
								//
								//if ( this.Band.Columns.Exists( column.Key ) )
								//	this.Cells[ column.Key ].Hidden = true;
								destCell.Hidden = true;
							}

							// SSP 1/27/04 UWG2921
							// Copy over the activation settings.
							//
							// --------------------------------------------------------------------------
							destCell.Activation = srcCell.Activation;
							destCell.IgnoreRowColActivation = srcCell.IgnoreRowColActivation;
							// --------------------------------------------------------------------------
						}
					}
				}
			}

			// SSP 8/14/03 UWG2589 - Filter Action
			// When Filter Action functionality was added, the Hidden property's behavior was changed.
			// Now the Hidden property won't get set when the filter is evaluated. So we need to copy
			// the column filters on every rows collection when printing or exporting to excel.
			//
			// ----------------------------------------------------------------------------------------------------
			if ( null != source.childBands && null != this.ChildBands )
			{
				for ( int i = 0; i < source.ChildBands.Count; i++ )
				{
					UltraGridChildBand sourceChildBand = source.ChildBands[ i ];

					// SSP 1/18/05 BR01753
					// Only enable calculations in display layout. Print and export layouts will
					// copy over the calculated values from the display layout.
					//
					// --------------------------------------------------------------------------------
					//if ( null == sourceChildBand || ! sourceChildBand.HaveRowsBeenInitialized || ! sourceChildBand.Rows.HasColumnFilters )
					//	continue;
					if ( null == sourceChildBand || ! sourceChildBand.HaveRowsBeenInitialized )
						continue;
					// --------------------------------------------------------------------------------

					if ( this.ChildBands.Exists( sourceChildBand.Band.Key ) )
					{
						UltraGridChildBand thisChildBand = this.ChildBands[ sourceChildBand.Band.Key ];

						// SSP 1/18/05 BR01753
						// Only enable calculations in display layout. Print and export layouts will
						// copy over the calculated values from the display layout.
						//
						//UltraGridRow.CopyColumnFilters( sourceChildBand.Rows, thisChildBand.Rows );
						UltraGridRow.CopyPrintRelatedSettings( sourceChildBand.Rows, thisChildBand.Rows );
					}
				}
			}
			// ----------------------------------------------------------------------------------------------------

			// SSP 9/5/03 UWG2630
			// Copy the description as well.
			//
			// ----------------------------------------------------------------------------------------------------
			if ( RowPropertyCategories.Description == ( RowPropertyCategories.Description & rowProperties ) )
			{
				this.description = source.description;
			}
			// ----------------------------------------------------------------------------------------------------
		}

		internal static void CopyPrintRelatedSettings( RowsCollection source, RowsCollection dest )
		{
			UltraGridRow.CopyColumnFilters( source, dest );
			UltraGridRow.CopyFormulaSummaryValues( source, dest );

			// SSP 5/18/05 BR04116 - NAS 5.2 Fixed Rows
			//
			UltraGridRow.CopyFixedRows( source, dest );
		}

		// SSP 5/18/05 BR04116 - NAS 5.2 Fixed Rows
		// Added CopyFixedRows method.
		//
		internal static void CopyFixedRows( RowsCollection source, RowsCollection dest )
		{
			// First clear fixed rows on the collection.
			//
			if ( dest.HasFixedRowsBeenAllocated )
				dest.FixedRows.Clear( );

			if ( source.HasFixedRowsBeenAllocated )
			{
				foreach ( UltraGridRow printRow in source.FixedRows )
				{
					UltraGridRow row = dest.GetRowFromPrintRow( printRow );
					if ( null != row )
						dest.FixedRows.Add( row );
				}
			}
		}

		private static void CopyColumnFilters( RowsCollection source, RowsCollection dest )
		{
			if ( null != source && null != dest && source.HasColumnFilters )
				dest.ColumnFilters.InitializeFrom( source.ColumnFilters, PropertyCategories.All );
		}

		// SSP 1/18/05 BR01753
		// Only enable calculations in display layout. Print and export layouts will
		// copy over the calculated values from the display layout.
		//
		private static void CopyFormulaSummaryValues( RowsCollection source, RowsCollection dest )
		{
			if ( null != source && null != dest && source.Band.HasSummaries )
			{
				SummaryValuesCollection sourceVals = source.SummaryValues;
				SummaryValuesCollection destVals = dest.SummaryValues;

				for ( int i = 0, count = destVals.Count; i < count; i++ )
				{
					SummaryValue destSummary = destVals[i];
					SummaryValue sourceSummary = sourceVals.GetFormulaSummaryFromPrintFormulaSummary( destSummary );
					if ( null != sourceSummary )
					{
						FormulaTargetRefBase sourceRef = sourceSummary.CalcReference;
						FormulaTargetRefBase destRef = destSummary.CalcReference;
                        if (null != destRef && null != sourceRef)
                        {
                            // MRS 4/8/2008 - BR31725
                            //destRef.InternalCopyValueFrom(sourceRef);
                            object oldValue = destSummary.Value;
                            destRef.InternalCopyValueFrom(sourceRef);
                            destSummary.OnSummaryValueChanged(oldValue, destSummary.Value);
                        }
					}
				}
			}
		}

		#endregion // CopyRowPropertiesFrom

		#region OnDispose

		// SSP 9/9/03 UWG2308
		// Overrode OnDispose.
		//
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnDispose( )
		{
			UltraGridLayout layout = this.Layout;

			// MRS 9/21/04 - UWG3670
			// If the row is selected, un-select it
			if ( this.selectedValue )
			{
				UltraGrid grid = layout.Grid as UltraGrid;
				// AS 9/28/04 UWG3442
				// Setting the Selected property will lead to a prop change
				// notification which will cause an exception since the
				// object is disposed. Just remove the object from the 
				// selected collection. Also, don't bother if the layout is
				// disposed.
				//
				//if ( grid != null && grid.Selected.Cells.Contains(this) )
				//	this.Selected = false;
				if ( ! layout.Disposed && grid != null )
					grid.Selected.Rows.Remove(this);
			}

			if ( null != this.appearance )
			{
				// SSP 4/29/06 BR11431
				// Make sure the layout doesn't hold onto the appearance reference.
				// 
				// ------------------------------------------------------------------
				if ( null != layout )
					layout.RemoveMulticastAppearance( this.appearance, this );
				// ------------------------------------------------------------------

				this.appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.appearance = null;
			}

			if ( null != this.cellAppearance )
			{
				// SSP 4/29/06 BR11431
				// Make sure the layout doesn't hold onto the appearance reference.
				// 
				// ------------------------------------------------------------------
				if ( null != layout )
					layout.RemoveMulticastAppearance( this.cellAppearance, this );
				// ------------------------------------------------------------------

				this.cellAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.cellAppearance = null;
			}

			if ( null != this.rowSelectorAppearance )
			{
				// SSP 4/29/06 BR11431
				// Make sure the layout doesn't hold onto the appearance reference.
				// 
				// ------------------------------------------------------------------
				if ( null != layout )
					layout.RemoveMulticastAppearance( this.rowSelectorAppearance, this );
				// ------------------------------------------------------------------

				this.rowSelectorAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.rowSelectorAppearance = null;
			}

			if ( null != this.previewAppearance )
			{
				// SSP 4/29/06 BR11431
				// Make sure the layout doesn't hold onto the appearance reference.
				// 
				// ------------------------------------------------------------------
				if ( null != layout )
					layout.RemoveMulticastAppearance( this.previewAppearance, this );
				// ------------------------------------------------------------------

				this.previewAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.previewAppearance = null;
			}

			// SSP 5/19/04 UWG3088
			// Dispose the cells as well. When an appearance is assigned to a cell object, 
			// the cell object hooks into the appearance and thus the appearance object 
			// will keep a reference to the cell. When the row is diposed of, the row's 
			// cells will still be referenced by the appearance and thus there could be a 
			// memory leak. So we need to dispose of the cells when the row gets disposed 
			// off.
			//
			if ( null != cells )
				this.cells.Dispose( );
			this.cells = null;
			
			if ( null != this.childBands )
				this.childBands.Dispose( );
			// SSP 12/11/03 UWG2791
			// Don't set the childBands to null because if something is holding a reference
			// to this row even though it's disposed off, accessing some property that assumes
			// non-null childBands will crash. Although nothing should be holding a reference
			// to a disposed row, not setting it null won't cause any problems either. So
			// taking a safe approach here and not setting the childBands to null.
			// Commneted out the below line.
			//
			//this.childBands = null;

			// SSP 11/2/04 UWG3736
			// Null out the list object so the row doesn't keep holding a reference back
			// to the data row even when the grid's DataSource is reset to null.
			//
			this.listObject = null;

			// SSP 1/4/05 BR08526
			// 
			// SSP 11/16/06 BR16418
			// Do this for CLR1.x versions as well since one could use CLR1.x assemblies with CLR2 
			// version (VS2005).
			// 
//#if CLR2
			this.listObject_ForComparison = null;
//#endif // CLR2

			// SSP 11/4/03 UWG2736
			// Typo. Call OnDispose instead of Dispose.
			//
			//base.Dispose( );
			base.OnDispose( );
		}

		#endregion // OnDispose

		

		// JJD 12/12/03 - Added Accessibility support
		#region AccessibilityObject property

		/// <summary>
		/// Returns the accesible object representing the data area of the grid.
		/// </summary>
		public AccessibleObject AccessibilityObject
		{
			get
			{
				if ( this.accessibilityObject == null )
				{
					UltraGridBase grid = this.band.Layout.Grid;

					if ( grid != null )
						this.accessibilityObject = grid.CreateAccessibilityInstance	( this );
				}

				return this.accessibilityObject;
			}
		}

		#endregion AccessibilityObject property

		// JJD 1/06/04 - Accessibility
		//
		#region ColHeadersAccessibilityObject

		internal System.Windows.Forms.AccessibleObject ColHeadersAccessibilityObject
		{
			get
			{
				// return null if the row is hidden 
				if ( this.HiddenResolved == true )
					return null;

				// only return true if the row has a header attached
				foreach ( VisibleRow vr in this.Layout.ActiveRowScrollRegion.VisibleRows )
				{
					if ( vr.Row == this )
					{
						if ( vr.HasHeader == true )
						{
							if ( this.colHeadersAccessibilityObject == null )
								this.colHeadersAccessibilityObject = this.Layout.Grid.CreateAccessibilityInstance( new HeadersCollection.HeadersAccessibleObject( this ) );

							return this.colHeadersAccessibilityObject;
						}
					}
				}

				return null;
			}
		}

		#endregion ColHeadersAccessibilityObject

		// JJD 12/12/03 - Added Accessibility support
		#region Public Class RowAccessibleObject

		/// <summary>
		/// The Accessible object for a row.
		/// </summary>
		public class RowAccessibleObject : AccessibleObjectWrapper
		{
			#region Private Members

			private UltraGridRow row;

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor
			/// </summary>
            /// <param name="row">The row which this accessible object represents.</param>
			public RowAccessibleObject ( UltraGridRow row )
			{
				if ( null == row )
					throw new ArgumentNullException( "row" );

				this.row = row;
			}

			#endregion Constructor

			#region Base Class Overrides

				#region Bounds

			/// <summary>
			/// Gets the location and size of the accessible object.
			/// </summary>
			public override System.Drawing.Rectangle Bounds
			{
				get
				{

					UIElement element = this.row.GetUIElement( this.row.Band.Layout.ActiveRowScrollRegion, this.row.Band.Layout.ActiveColScrollRegion, true );

					if ( element == null )
						return Rectangle.Empty;

					Control control = element.Control;
	
					Debug.WriteLine("Get accessible row bounds " + control.InvokeRequired.ToString() );

					if ( control == null )
						return Rectangle.Empty;

					return control.RectangleToScreen( element.ClipRect );
				}
			}

				#endregion Bounds

				#region DefaultAction

			/// <summary>
			/// Gets a string that describes the default action of the object.
			/// </summary>
			public override string DefaultAction
			{
				get
				{

					if ( this.row.HasExpansionIndicator == true )
					{
						if ( this.row.Expanded == true )
							return Shared.SR.GetString("DefaultAction_Collapse");
						else 
							return Shared.SR.GetString("DefaultAction_Expand");
					}

					if ( this.row.IsCard && this.row.IsCardStyleCompressed )
					{
						if ( this.row.IsCardCompressed == true )
							return Shared.SR.GetString("DefaultAction_Expand");
						else 
							return Shared.SR.GetString("DefaultAction_Collapse");
					}

					if ( !this.row.IsDisabled )
					{
						if ( !this.row.IsActiveRow || this.row.Layout.ActiveCell != null)
							return Shared.SR.GetString("DefaultAction_Row");
					}

					return null;
				}
			}

				#endregion DefaultAction

				#region Description

			/// <summary>
			/// Gets a string that describes the visual appearance of the specified object. Not all objects have a description.
			/// </summary>
			public override string Description
			{
				get
				{
					return null;
				}
			}

				#endregion Description

				#region DoDefaultAction

			/// <summary>
			/// Performs the default action associated with this accessible object.
			/// </summary>
			public override void DoDefaultAction()
			{
				if ( this.row.HasExpansionIndicator == true )
					this.row.Expanded = ! this.row.Expanded;
				else if ( this.row.IsCard && this.row.IsCardStyleCompressed )
					this.row.IsCardCompressed = !this.row.IsCardCompressed;
				else if ( !this.row.IsActiveRow || this.row.Layout.ActiveCell != null)
				{
					if ( !this.row.IsDisabled )
					{
						// set the active row
						this.row.Layout.Grid.ActiveRow = this.row;

						// clear the active cell
						if ( this.row.Layout.ActiveRow == this.row &&
							this.row.Layout.ActiveCell != null &&
							this.row.Layout.Grid is UltraGrid )
							((UltraGrid)(this.row.Layout.Grid)).ActiveCell = null;
					}
				}
			}

				#endregion DoDefaultAction

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{

				UltraGridGroupByRow groupbyRow = this.row as UltraGridGroupByRow;

				if ( groupbyRow != null )
				{
					if ( this.row.Expanded &&
						index >= 0 &&
						index < groupbyRow.Rows.Count)
						return groupbyRow.Rows[index].AccessibilityObject;
					
					return null;
				}

				if ( !this.row.band.CardView )
				{
					if ( this.row.ColHeadersAccessibilityObject != null )
					{
						if ( index == 0 )
							return this.row.ColHeadersAccessibilityObject;

						index--;
					}
				}

				HeadersCollection headers;

				if ( this.row.Band.UseRowLayoutResolved )
					headers = this.row.Band.LayoutOrderedVisibleColumnHeaders;
				else
					headers = this.row.Band.OrderedColumnHeaders;

				if ( index >= 0 && index < headers.Count )
				{
					UltraGridColumn column = ((ColumnHeader)(headers[index])).Column;

					if ( column != null )
						return this.row.Cells[ column ].AccessibilityObject;
				}

				index -= headers.Count;

				if ( !this.row.band.CardView )
				{
					if ( index >= 0 && this.row.Expanded && 
						index < this.row.ChildBands.GetAccessibleChildCount() &&
						this.row.HasChildRowsInternal( IncludeRowTypes.SpecialRows ) )
						return this.row.ChildBands.GetAccessibleChild(index);
				}

				return null;
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{

				int count = 0;

				UltraGridGroupByRow groupbyRow = this.row as UltraGridGroupByRow;

				if ( groupbyRow != null )
				{
					if ( this.row.Expanded )
						count = groupbyRow.Rows.Count;
				}
				else
				{
					if ( this.row.Band.UseRowLayoutResolved )
						count = this.row.Band.LayoutOrderedVisibleColumnHeaders.Count;
					else
						count = this.row.Band.OrderedColumnHeaders.Count;

					if ( !this.row.band.CardView )
					{
						if ( this.row.Expanded && 
							!this.row.band.CardView &&
							this.row.HasChildRowsInternal( IncludeRowTypes.SpecialRows ) )
							count += this.row.ChildBands.GetAccessibleChildCount();

						if ( this.row.ColHeadersAccessibilityObject != null )
							count++;
					}
				}

				return count;
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				return null;
			}

				#endregion GetFocused

			//JJD 12/17/03
				#region GetMarshallingControl

			/// <summary>
			/// Returns the control used to synchronize accessibility calls.
			/// </summary>
			/// <returns>A control to be used to synchronize accessibility calls.</returns>
			protected override Control GetMarshallingControl()
			{
				return this.row.Layout.Grid;
			}

				#endregion GetMarshallingControl

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				
				return null;
			}

				#endregion GetSelected

				#region Help

			/// <summary>
			/// Gets a description of what the object does or how the object is used.
			/// </summary>
			public override string Help
			{
				get
				{
					return null;
				}
			}

				#endregion Help

				#region HitTest

			/// <summary>
			/// Retrieves the child object at the specified screen coordinates.
			/// </summary>
			/// <param name="x">The horizontal screen coordinate.</param>
			/// <param name="y">The vertical screen coordinate.</param>
			/// <returns>An AccessibleObject that represents the child object at the given screen coordinates. This method returns the calling object if the object itself is at the location specified. Returns a null reference (Nothing in Visual Basic) if no object is at the tested location.</returns>
			public override AccessibleObject HitTest(int x, int y)
			{
				
				return base.HitTest( x, y );
			}

				#endregion HitTest

				#region Name

			/// <summary>
			/// The accessible name for the data area.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if ( name != null && name.Length > 0 )
						return name;

					if ( this.row.IsTemplateAddRow )
						return Shared.SR.GetString("AccessibleName_TemplateAddRow");

					if ( this.row.IsAddRow )
						return Shared.SR.GetString("AccessibleName_AddRow");

					// SSP 4/14/05 - NAS 5.2 Filter Row
					//
					if ( this.row.IsFilterRow )
						return Shared.SR.GetString("AccessibleName_FilterRow");

					string bandCaption = this.row.band.Header.Caption;

					if ( bandCaption == null )
						bandCaption = string.Empty;
					
					name = Shared.SR.GetString("AccessibleName_Row");

					try 
					{
						return (string.Format( name, bandCaption, this.row.Index + 1 )).Trim();
					}
					catch(Exception)
					{
						return name;
					}
				}
				set
				{
					base.Name = value;
				}
			}

				#endregion Name

				#region Navigate

            /// <summary>
            /// Navigates to another accessible object.
            /// </summary>
            /// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
            /// <returns>An AccessibleObject relative to this object.</returns>
            public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{

				UltraGridRow sibling = null;

				switch ( navdir )
				{
						#region case FirstChild/LastChild

					case AccessibleNavigation.FirstChild:
					case AccessibleNavigation.LastChild:
					{
						if ( !(this.row is UltraGridGroupByRow ) )
						{
							UltraGridColumn column; 

							if ( navdir == AccessibleNavigation.FirstChild )
								column = this.row.Band.GetFirstVisibleCol( this.row.Layout.ActiveColScrollRegion, true );
							else
								column = this.row.Band.GetLastVisibleCol( this.row.Layout.ActiveColScrollRegion, true );

							if ( column != null )
								return this.row.Cells[ column ].AccessibilityObject;
						}

						return base.Navigate( navdir );
					}

						#endregion case FirstChild/LastChild

						#region case Down

					case AccessibleNavigation.Down:
						sibling = this.row.GetNextVisibleRelative( false, true, false );
						break;

						#endregion case Down

						#region case Up

					case AccessibleNavigation.Up:
						sibling = this.row.GetPrevVisibleRelative( false, true, false );
						break;

						#endregion case Up

						#region case Next

					case AccessibleNavigation.Next:
						sibling = this.row.GetNextVisibleRelative( false, true, false );
						break;

						#endregion case Next

						#region case Previous

					case AccessibleNavigation.Previous:
						sibling = this.row.GetPrevVisibleRelative( false, true, false );
						break;

						#endregion case Previous

						
						#region case Right

					case AccessibleNavigation.Right:
						break;

						#endregion case Right

						#region case Left

						
					case AccessibleNavigation.Left:
						break;

						#endregion case Left
				}

				if ( sibling != null )
					return sibling.AccessibilityObject;

				return null;
			}

				#endregion Navigate

				#region Parent

			/// <summary>
			/// Gets the parent of an accessible object.
			/// </summary>
			public override AccessibleObject Parent
			{
				get
				{
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( this.row.ParentRow == null )
					//    return this.row.band.Layout.Grid.AccessibilityObject;
					//
					//return this.row.ParentRow.AccessibilityObject;
					UltraGridRow parentRow = this.row.ParentRow;

					if ( parentRow == null )
						return this.row.band.Layout.Grid.AccessibilityObject;

					return parentRow.AccessibilityObject;
				}
			}

				#endregion Parent

				#region Role

			/// <summary>
			/// Gets the role of this accessible object.
			/// </summary>
			public override AccessibleRole Role
			{
				get
				{
					return AccessibleRole.Row;
				}
			}

				#endregion Role

				#region Select

			/// <summary>
			/// Modifies the selection or moves the keyboard focus of the accessible object.
			/// </summary>
			/// <param name="flags">One of the <see cref="System.Windows.Forms.AccessibleSelection"/> values.</param>
			public override void Select(System.Windows.Forms.AccessibleSelection flags)
			{
				UltraGridRow item = this.row;
				UltraGridBase gridBase = item.Layout.Grid;
				ISelectionManager selectionManager = gridBase as ISelectionManager;
				ISelectionStrategy selectionStrategy = null != selectionManager ? selectionManager.GetSelectionStrategy( item ) : null;

				Debug.Assert( null != selectionManager && null != selectionStrategy );
				if ( null == selectionManager || null == selectionStrategy )
					return;

				// We have a selection strategy that only allows contiguous selection.
				//
				bool isStrategyContiguous = 
					selectionStrategy is SelectionStrategyExtended 
					? ! ((SelectionStrategyExtended)selectionStrategy).IsDiscontiguousAllowed 
					: false;

				// NOTE: The order in which following if statements appear is important (at least for
				// some of them) because flags can contain multiple entries and thus they have to be
				// processed in a certain order. For example, TakeFocus can be combined with the 
				// ExtendSelection which necessiates processing ExtendSelection (which is supposed to
				// emulate Shift+Click on an item to range select) before processing TakeFocus.
				//

				if ( gridBase is UltraGrid )
				{
					UltraGrid grid = (UltraGrid)gridBase;

					// ExtendSelection does the range selection. It emulates the Shift + Click.
					// AddSelection adds the item to the selection. It emulates the Ctrl + Click.
					// Both of these actions do not clear the current selection.
					//
					if ( AccessibleSelection.ExtendSelection == ( AccessibleSelection.ExtendSelection & flags ) 
						|| AccessibleSelection.AddSelection == ( AccessibleSelection.AddSelection & flags ) )
					{
						// SSP 4/20/05 - NAS 5.2 Filter Row
						// Use the Selectable property instead.
						//
						//if ( item.CanSelectRow )
						if ( item.Selectable )
						{
							if ( selectionStrategy.IsSingleSelect )
							{
								// If selection strategy is single select, then clear the exisitng
								// selection.
								//
								grid.InternalSelectItem( item, true, true );
							}
								// Make sure the item can be selected with current selection. For example,
								// we don't allow seleting rows from different bands and also allow selecting
								// rows and cells at the same time.
								//
							else if ( selectionManager.IsItemSelectableWithCurrentSelection( item ) 
								&& ! selectionManager.IsMaxSelectedItemsExceeded( item ) )
							{
								if ( AccessibleSelection.ExtendSelection == ( AccessibleSelection.ExtendSelection & flags ) )
								{
									// If ExtendedSelection do range select.
									//
									grid.InternalSelectRange( item, false, true );
								}
								else 
								{
									// If AddSelection then add the item to the selection. Also if the
									// selection strategy allows only contiguous items to be selected, then
									// clear the selection by passing in true for the second param.
									//
									grid.InternalSelectItem( item, isStrategyContiguous, true );
								}
							}
						}
					}

					// TakeFocus makes the item the pivot item. For grid that means activating the
					// row as well.
					//
					if ( AccessibleSelection.TakeFocus == ( AccessibleSelection.TakeFocus & flags ) )
					{
						// Activate the item.
						//
						item.Activate( );

						// Make the item the pivot item.
						//
						if ( item.IsActiveRow )
							selectionManager.SetPivotItem( item, false );
					}
			
					// Unselect the item.
					//
					if ( AccessibleSelection.RemoveSelection == ( AccessibleSelection.RemoveSelection & flags ) )
					{
						// If the selection strategy allows only contiguous items to be selected, then
						// clear the selection by passing in true for the second param.
						//
						grid.InternalSelectItem( item, isStrategyContiguous, false );
					}

					// Select the item clearing other selected items.
					//
					if ( AccessibleSelection.TakeSelection == ( AccessibleSelection.TakeSelection & flags ) )
					{
						// SSP 4/20/05 - NAS 5.2 Filter Row
						// Use the Selectable property instead.
						//
						//if ( item.CanSelectRow )
						if ( item.Selectable )
							grid.InternalSelectItem( item, true, true );
					}
				}
				else if ( gridBase is UltraDropDownBase )
				{
					// For the UltraDropDown, following four actions should do the same. That is to select
					// the row which will have the same effect as selecting that row from the drop down.
					//
					if ( AccessibleSelection.ExtendSelection == ( AccessibleSelection.ExtendSelection & flags )
						|| AccessibleSelection.AddSelection == ( AccessibleSelection.AddSelection & flags ) 
						|| AccessibleSelection.TakeSelection == ( AccessibleSelection.TakeSelection & flags ) 
						|| AccessibleSelection.TakeFocus == ( AccessibleSelection.TakeFocus & flags ) )
					{
						((UltraDropDownBase)gridBase).SelectedRow = item;
					}

					// Unselect the item.
					//
					if ( AccessibleSelection.RemoveSelection == ( AccessibleSelection.RemoveSelection & flags ) )
						((UltraDropDownBase)gridBase).SelectedRow = null;
				}
				else
				{
					Debug.Assert( false, "Unknwon type of grid !" );
				}
			}

				#endregion Select

				#region State

			/// <summary>
			/// Gets the state of this accessible object.
			/// </summary>
			public override AccessibleStates State
			{
				get
				{
					if ( this.row.HiddenResolved == true )
						return AccessibleStates.Invisible;

					if ( this.row.ActivationResolved == Activation.Disabled )
						return AccessibleStates.Unavailable;

					AccessibleStates state = AccessibleStates.ReadOnly;

					// SSP 4/20/05 - NAS 5.2 Filter Row
					// Use the Selectable property instead.
					//
					//if ( this.row.CanSelectRow )
					if ( this.row.Selectable )
						state |= AccessibleStates.Selectable;

					if ( this.row.Selected )
						state |= AccessibleStates.Selected;

					if ( this.row.IsActiveRow == true )
					{
						UltraGridCell cell = this.row.band.Layout.ActiveCell;

						if ( cell == null )
						{
							if( this.row.band.Layout.Grid.Focused )
								state |= AccessibleStates.Focused;
						}
					}

					if ( this.row.HasExpansionIndicator == true )
					{
						if ( this.row.Expanded == true )
							state |= AccessibleStates.Expanded;
						else 
							state |= AccessibleStates.Collapsed;
					}

					if ( this.row.IsCard && this.row.IsCardStyleCompressed )
					{
						if ( this.row.IsCardCompressed == true )
							state |= AccessibleStates.Collapsed;
						else 
							state |= AccessibleStates.Expanded;
					}

					return state;
				}
			}

				#endregion State

				#region Value

			/// <summary>
			/// Returns the row's description
			/// </summary>
			public override string Value
			{
				get
				{
					string desc = this.row.Description;

					if ( desc != null && desc.Length > 0 )
						return desc;

					return null;
				}
				set
				{
				}
			}

				#endregion Value

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Row

			/// <summary>
			/// Returns the associated row.
			/// </summary>
			public UltraGridRow Row 
			{ 
				get 
				{
					return this.row; 
				} 
			}

					#endregion Row

				#endregion Public Properties

			#endregion Properties

		}

		#endregion Public Class RowAccessibleObject

		// SSP 6/30/04 - UltraCalc
		//
		#region CalcReference

		internal RowReferenceBase CalcReference
		{
			get
			{
				if ( null == this.calcReference && null != this.Layout )
				{
					if ( ! this.IsGroupByRow )
						this.calcReference = new RowReference( this );
					else
						this.calcReference = new GroupByRowReference( (UltraGridGroupByRow)this );
				}

				return this.calcReference;
			}
		}

		#endregion // CalcReference

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return null != this.calcReference;
			}
		}

		#endregion // HasCalcReference

		#region OverallHierarchyLevel
		
		internal int OverallHierarchyLevel
		{
			get
			{
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//return null != this.ParentRow ? this.ParentRow.OverallHierarchyLevel + 1 : 0;
				UltraGridRow parentRow = this.ParentRow;

				return null != parentRow ? parentRow.OverallHierarchyLevel + 1 : 0;
			}
		}

		#endregion // OverallHierarchyLevel

		#region InvalidateItemAllRegions
		
		// SSP 12/7/04 - Merged Cell Feature
		//
		internal void InvalidateItemAllRegions( bool recalcRects, bool dirtyMergedCell )
		{
			base.InvalidateItemAllRegions( recalcRects );

			if ( dirtyMergedCell )
				RowsCollection.MergedCell_RowStateChanged( this, MergedCell.State.Invalidate );
		}

		#endregion // InvalidateItemAllRegions

		#region InvalidateCellAllRegions

		// SSP 3/25/05 BR02996
		// Added InvalidateCellAllRegions method.
		//
		internal void InvalidateCellAllRegions( UltraGridColumn column, bool invalidate, bool dirtyChildElems )
		{
			if ( this.IsPotentiallyVisibleOnScreen && null != this.Layout )
			{
				RowScrollRegionsCollection rsrColl = this.Layout.RowScrollRegions;
				ColScrollRegionsCollection csrColl = this.Layout.ColScrollRegions;

				for ( int i = 0; i < csrColl.Count; i++ )
				{
					for ( int j = 0; j < rsrColl.Count; j++ )
					{
						UIElement elem = this.GetUIElement( rsrColl[ j ], csrColl[ i ], false );
						if ( null != elem )
							elem = elem.GetDescendant( typeof( CellUIElementBase ), column );

						if ( null != elem )
						{
							if ( dirtyChildElems )
								elem.DirtyChildElements( invalidate );
							else if ( invalidate )
								elem.Invalidate( );
						}
					}
				}
			}
		}

		#endregion // InvalidateCellAllRegions

		#region InvalidateElem

		// SSP 7/12/05 BR04897
		// 
		internal override void InvalidateElem( UIElement element, bool recalcRects )
		{
            // MBS 4/20/09 - NA9.2 Selection Overlay
            if (element != null)
            {
                RowColRegionIntersectionUIElement rowColRegion = element.GetAncestor(typeof(RowColRegionIntersectionUIElement)) as RowColRegionIntersectionUIElement;
                if (rowColRegion != null)
                {
                    int borderThickness = this.Layout.SelectionOverlayBorderThicknessResolved;
                    if (borderThickness > 0 && this.Layout.SelectionOverlayBorderColorResolved != Color.Empty)
                    {
                        UltraGrid grid = this.GetGrid() as UltraGrid;
                        if (grid != null)
                        {
                            Rectangle rect = element.Rect;
                            rect.Inflate(borderThickness, borderThickness);
                            grid.Invalidate(rect);

                            // If we don't have to recalculate the rects, then we can return at this point since
                            // we've already performed the invalidation
                            if (!recalcRects)
                                return;
                        }
                    }
                }
            }

			base.InvalidateElem( element, recalcRects );

			// SSP 7/12/05 BR04897
			// Also dirty the cell elems. There is an optimization in row cell area elem that
			// does not reposition cell elems unless certain conditions are met. Here we 
			// should always cause the cell elems to reposition. For that mark the new 
			// cellElemsDirty flag.
			// 
			if ( recalcRects )
			{
				RowUIElement rowElem = element as RowUIElement;
				if ( null != rowElem )
				{
					RowCellAreaUIElementBase rowCellArea = (RowCellAreaUIElementBase)GridUtils.GetChild( rowElem, typeof( RowCellAreaUIElementBase ) );
					if ( null != rowCellArea )
						rowCellArea.cellElemsDirty = true;
				}
			}
		}

		#endregion // InvalidateElem

		// SSP 12/15/04 - IDataErrorInfo Support
		//
		#region IDataErrorInfo Support

		internal bool HasDataError( )
		{
			return this.HasDataError( null );
		}
		
		internal bool HasDataError( UltraGridColumn column )
		{
			string dataError = this.GetDataError( column );
			return null != dataError && dataError.Length > 0;
		}
		
		internal string GetDataError( )
		{
			return this.GetDataError( null );
		}

		internal string GetDataError( UltraGridColumn column )
		{
			bool supportDataErrorInfo = null != column 
				? column.SupportsDataErrorInfoResolved
				: this.band.SupportsDataErrorInfoOnRowsResolved;

			if ( supportDataErrorInfo && ! this.Layout.Grid.DesignMode )
			{
				// SSP 4/7/05
				// Use the GetDataErrorInfo helper method instead.
				//
				//IDataErrorInfo dataErrorInfo = this.ListObject as IDataErrorInfo;
				IDataErrorInfo dataErrorInfo = this.GetDataErrorInfo( );
				
				if ( null != dataErrorInfo )
					// SSP 5/6/05
					// Only do this for bound columns (columns that exist as part of the 
					// underlying data source).
					//
					//return null != column ? dataErrorInfo[ column.Key ] : dataErrorInfo.Error;
					return null != column ? ( column.IsBound ? dataErrorInfo[ column.Key ] : null ) : dataErrorInfo.Error;
			}

			return null;
		}

		// SSP 4/7/05
		// Added GetDataErrorInfo method.
		//
		internal IDataErrorInfo GetDataErrorInfo( )
		{
			return this.ListObject as IDataErrorInfo;
		}

		internal Image GetDataErrorImage( out AppearanceData appData )
		{
			return this.GetDataErrorImage( null, out appData );
		}
		
		internal Image GetDataErrorImage( UltraGridColumn column, out AppearanceData appData )
		{
			ResolveAppearanceContext context = new ResolveAppearanceContext( 
				null != column 
				? typeof( Infragistics.Win.UltraWinGrid.UltraGridCell )
				: typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ), 
				AppearancePropFlags.Image | AppearancePropFlags.ImageHAlign 
				| AppearancePropFlags.ImageVAlign | AppearancePropFlags.ImageAlpha );

			appData = new AppearanceData( );
            
			if ( null != column )
			{
                // MRS 4/8/2009 - TFS15580            
                context.Role = StyleUtils.GetRole(this.Layout, StyleUtils.Role.Cell, out context.ResolutionOrder);
                
				this.band.MergeOverrideAppearances( ref appData, ref context,
                    UltraGridOverride.OverrideAppearanceIndex.DataErrorCellAppearance
                    // MRS 4/8/2009 - TFS15580
                    , Infragistics.Win.AppStyling.RoleState.DataError);

				if ( 0 != context.UnresolvedProps )
					this.Layout.MergeOverrideAppearances( ref appData, ref context,
                        UltraGridOverride.OverrideAppearanceIndex.DataErrorCellAppearance
                        // MRS 4/8/2009 - TFS15580
                        , Infragistics.Win.AppStyling.RoleState.DataError);
			}
			else
			{
                // MRS 4/8/2009 - TFS15580            
                context.Role = StyleUtils.GetRole(this.Layout, StyleUtils.Role.RowSelector, out context.ResolutionOrder);

				this.band.MergeOverrideAppearances( ref appData, ref context, 
					UltraGridOverride.OverrideAppearanceIndex.DataErrorRowSelectorAppearance
                    // MRS 4/8/2009 - TFS15580
                    , Infragistics.Win.AppStyling.RoleState.DataError );

				if ( 0 != context.UnresolvedProps )
					this.Layout.MergeOverrideAppearances( ref appData, ref context, 
						UltraGridOverride.OverrideAppearanceIndex.DataErrorRowSelectorAppearance
                        // MRS 4/8/2009 - TFS15580
                        , Infragistics.Win.AppStyling.RoleState.DataError);
			}

			if ( null == appData.Image )
				appData.Image = this.Layout.DataErrorImageFromResource;

			return appData.GetImage( null != this.Layout.Grid ? this.Layout.Grid.ImageList : null );
		}

		#endregion // IDataErrorInfo Support		

		// MRS 3/10/05 - BR02716
		#region GetLastVisibleChildRow

		// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		// We have to take into account filter row, summary row etc...
		// Got rid of IncludeRowTypes parameter from GetLastVisibleChildRow as it wasn't needed.
		// Every place we were calling it with IncludeRowTypes.TemplateAddRow as the param.
		// 
		//internal UltraGridRow GetLastVisibleChildRow( IncludeRowTypes includeRowTypes )
		internal UltraGridRow GetLastVisibleChildRow( )
		{
			if (this.ChildBands == null)
				return null;

			// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// We have to take into account filter row, summary row etc...
			// Got rid of IncludeRowTypes parameter from GetLastVisibleChildRow as it wasn't needed.
			// Every place we were calling it with IncludeRowTypes.TemplateAddRow as the param.
			// 
			//return this.ChildBands.GetLastVisibleRow(includeRowTypes);
			return this.ChildBands.GetLastVisibleRow( );
		}

		#endregion // GetLastVisibleChildRow

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		#region NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region AllowFixing

		/// <summary>
		/// Specifies whether the user is allowed to fix the row. This property disables the user interface
		/// for this row. Default value is <b>Default</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">The user interface for fixing/unfixing rows can be enabled/disabled band-wide or grid-wide
		/// using the <see cref="UltraGridOverride.FixedRowIndicator"/> property. This property must be set to one
		/// of the valid options for enabling the user interface for this property to have any effect.
		/// </p>
		/// <seealso cref="UltraGridOverride.FixedRowIndicator"/>
		/// </remarks>
		public DefaultableBoolean AllowFixing
		{
			get
			{
				return this.allowFixing;
			}
			set
			{
				if ( this.allowFixing != value )
				{
					GridUtils.ValidateEnum( "AllowFixing", typeof( DefaultableBoolean ), value );
					this.allowFixing = value;
					this.NotifyPropChange( PropertyIds.AllowFixing );
				}
			}
		}

		#endregion // AllowFixing

		#region ResetAllowFixing

		/// <summary>
		/// Resets AllowFixing property to its default value of <b>Default</b>.
		/// </summary>
		public void ResetAllowFixing( )
		{
			this.AllowFixing = DefaultableBoolean.Default;
		}

		#endregion // ResetAllowFixing

		#region Fixed

		/// <summary>
		/// Gets or sets the fixed state of the row. Setting this property to false unfixes the
		/// row if it's fixed and setting it to true fixes the row if it's unfixed. The row is
		/// fixed on top or bottom depending on the <see cref="UltraGridOverride.FixedRowStyle"/>
		/// setting.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can enable user interface for fixing and unfixing rows using the 
		/// <see cref="UltraGridOverride.FixedRowIndicator"/> property.</p>
		/// <p class="body">Setting <b>Fixed</b> property of the row to <b>true</b> has the effect of adding 
		/// the row to its parent row collection's FixedRows collection (<see cref="RowsCollection.FixedRows"/>.
		/// Likewise setting this property to false has the effect of removing it from the fixed rows collection.
		/// Note that setting this property to true does not necessarily fix the row. There are situations
		/// where fixed rows are not supported (in card-view, in child row collections,
		/// in horizontal view style). In those situations you can still add the row to the 
		/// FixedRows collection however the row will not be actually fixed. It will however move
		/// the row to either top or bottom of the parent collection (depending on the 
		/// <see cref="UltraGridOverride.FixedRowStyle"/> property settings).</p>
		/// <p class="body"><b>Note</b> that fixed rows are not supported in child row collections. Setting this 
		/// property to true on a child row will not actually fix the row. It will however add the row to the 
		/// associated row collection's FixedRows collection (<see cref="RowsCollection.FixedRows"/>) and apply 
		/// any fixed row related appearances to it. It will also move the row in the beginning of the row
		/// collection. Also note that fixed rows are not supported in card-view.</p>
		/// <p class="body">Also note that when a fixed row is expanded any sibling fixed rows occuring
		/// after it will not be fixed. They will remain non-fixed until the expanded fixed row is collapsed.
		/// They will however remain in the FixedRows collection and keep their fixed row appearances.</p>
		/// <seealso cref="UltraGridOverride.FixedRowIndicator"/> <seealso cref="UltraGridOverride.FixedRowStyle"/>
		/// </remarks>
		public bool Fixed
		{
			get
			{
				return this.ParentCollection.HasFixedRowsBeenAllocated 
					&& this.ParentCollection.FixedRows.Contains( this );
			}
			set
			{
				if ( value != this.Fixed )
				{
					FixedRowsCollection fixedRows = this.ParentCollection.FixedRows;
					if ( value )
						fixedRows.Add( this, ! fixedRows.TopFixed );
					else 
						fixedRows.Remove( this );
				}
			}
		}

		#endregion // Fixed

		#region FixedResolved

		/// <summary>
		/// Gets the actual fixed state of the row. This value can differ from the <b>Fixed</b>
		/// property's value in situations where the row is not actually fixed.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>Note</b> that fixed rows are not supported in child row collections. Setting 
		/// <b>Fixed</b> property to true on a child row will not actually fix the row. It will however add 
		/// the row to the associated row collection's FixedRows collection (<see cref="RowsCollection.FixedRows"/>) 
		/// and also apply any fixed row related appearances to it. It will also move the row to the beginning 
		/// of the row collection. Also note that fixed rows are not supported in card-view.</p>
		/// <p class="body">Also note that when a fixed row is expanded any sibling fixed rows occuring
		/// after it will not be fixed. They will remain non-fixed until the expanded fixed row is collapsed.
		/// They will however remain in the FixedRows collection and keep their fixed row appearances.</p>
		/// <seealso cref="UltraGridRow.Fixed"/>
		/// </remarks>
		public bool FixedResolved
		{
			get
			{
				return FixedRowStyle.Default != this.FixedResolvedLocation;
			}
		}

		#endregion // FixedResolved

		#region FixedResolvedLocation

		internal virtual FixedRowStyle FixedResolvedLocation
		{
			get
			{
				if ( this.ParentCollection.SupportsFixedRows )
				{
					if ( FixedRowStyle.Default != this.fixedResolvedLocationOverride )
						return this.fixedResolvedLocationOverride;

					if ( this.IsDataRow || this.IsGroupByRow )
					{
						if ( this.parentCollection.HasFixedRowsBeenAllocated 
							&& this.parentCollection.FixedRows.IsFixedResolved( this ) )
							return this.parentCollection.FixedRows.TopFixed ? FixedRowStyle.Top : FixedRowStyle.Bottom;
					}
//					else if ( ( this == this.parentCollection.TemplateAddRowInternal 
//							|| this == this.parentCollection.CurrentAddRow ) 
//						&& ( AllowAddNew.FixedAddRowOnTop == this.BandInternal.AllowAddNewResolved
//							|| AllowAddNew.FixedAddRowOnBottom == this.BandInternal.AllowAddNewResolved ) )
//					{
//						return TemplateAddRowLocation.Top == this.parentCollection.TemplateRowLocationDefault 
//							? FixedRowStyle.Top : FixedRowStyle.Bottom;
//					}
				}

				return FixedRowStyle.Default;
			}
		}

		#endregion // FixedResolvedLocation

		#region RowSpacingBeforeResolvedBase

		internal virtual int RowSpacingBeforeResolvedBase
		{
			get
			{
				return this.rowSpacingBefore >= 0
					? this.rowSpacingBefore
					// SSP 11/11/03 Add Row Feature
					//
					//: this.band.RowSpacingBeforeDefault; 
					: ( ! this.ShouldApplyTemplateAddRowAppearance( ) ? this.band.RowSpacingBeforeDefault : this.band.TemplateAddRowSpacingBeforeDefault );
			}
		}

		#endregion // RowSpacingBeforeResolvedBase

		#region RowSpacingAfterResolvedBase

		internal virtual int RowSpacingAfterResolvedBase
		{
			get
			{
				return this.rowSpacingAfter >= 0
					? this.rowSpacingAfter
					// SSP 11/11/03 Add Row Feature
					//
					//: this.band.RowSpacingAfterDefault; 
					: ( ! this.ShouldApplyTemplateAddRowAppearance( ) ? this.band.RowSpacingAfterDefault : this.band.TemplateAddRowSpacingAfterDefault );
			}
		}

		#endregion // RowSpacingAfterResolvedBase

		#region InternalSetFixedResolvedOverride

		internal FixedRowStyle fixedResolvedLocationOverride = FixedRowStyle.Default;
		internal void InternalSetFixedResolvedOverride( FixedRowStyle fixedResolved )
		{
			this.fixedResolvedLocationOverride = fixedResolved;
		}

		#endregion // InternalSetFixedResolvedOverride

		#region IsFixedTop
		
		internal bool IsFixedTop
		{
			get
			{
				return FixedRowStyle.Top == this.FixedResolvedLocation;
			}
		}

		#endregion // IsFixedTop

		#region IsFixedBottom

		internal bool IsFixedBottom
		{
			get
			{
				return FixedRowStyle.Bottom == this.FixedResolvedLocation;
			}
		}

		#endregion // IsFixedBottom

		#region ShouldTemplateAddRowHaveSeparatorHelper

		private bool ShouldTemplateAddRowHaveSeparatorHelper( )
		{
			return 
				// If this row is a template add-row or an add-row.
				//
				this.parentCollection.IsTemplateOrAddRowFromTemplate( this ) 
				// If the SpecialRowSeparator settings specify that we show a separator 
				// for the template add-row.
				//
				&& 0 != ( SpecialRowSeparator.TemplateAddRow & this.band.GetSpecialRowSeparatorResolved( this ) ) 
				// If the row is an add-row from template and the template add-row is visible then don't
				// display the separator on both the template add-row and add-row from template.
				// 
				&& ( this.IsTemplateAddRow || ! this.ParentCollection.IsTemplateAddRowVisible );
		}

		#endregion // ShouldTemplateAddRowHaveSeparatorHelper

		#region HasSpecialRowSeparatorBefore

		internal virtual bool HasSpecialRowSeparatorBefore
		{
			get
			{
				return this.ShouldTemplateAddRowHaveSeparatorHelper( )
					&& TemplateAddRowLocation.Bottom == this.ParentCollection.TemplateRowLocationDefault
				 || this.parentCollection.IsFirstFixedRowOnBottom( this ) 
					&& 0 != ( SpecialRowSeparator.FixedRows & this.band.GetSpecialRowSeparatorResolved( this ) );
			}
		}

		#endregion // HasSpecialRowSeparatorBefore

		#region HasSpecialRowSeparatorAfter

		internal virtual bool HasSpecialRowSeparatorAfter
		{
			get
			{
				return this.ShouldTemplateAddRowHaveSeparatorHelper( )
					&& TemplateAddRowLocation.Top == this.ParentCollection.TemplateRowLocationDefault
				 || this.parentCollection.IsLastFixedRowOnTop( this ) 
					&& 0 != ( SpecialRowSeparator.FixedRows & this.band.GetSpecialRowSeparatorResolved( this ) );
			}
		}

		#endregion // HasSpecialRowSeparatorAfter

		#region ShouldApplyFixedAppearance

		private bool ShouldApplyFixedAppearance( )
		{
			// SSP 5/16/05 BR03863
			//
			//return this.Fixed;
			return this.Fixed && this.ParentCollection.SupportsFixedRows;
		}

		#endregion // ShouldApplyFixedAppearance
		
		#region BorderStyleRow

		internal virtual UIElementBorderStyle BorderStyleResolved
		{
			get
			{
				return ! this.IsTemplateAddRow 
					? this.band.BorderStyleRowResolved 
					: this.band.BorderStyleTemplateAddRowResolved;
			}
		}

		#endregion // BorderStyleRow

		#region BorderStyleCell

		/// <summary>
        /// Gets the resolved border style of the cells in this row.
		/// </summary>
        // MRS v7.2 - Document Exporter
		//internal virtual UIElementBorderStyle BorderStyleCellResolved
        public virtual UIElementBorderStyle BorderStyleCellResolved
		{
			get
			{
				return this.band.BorderStyleCellResolved;
			}
		}

		#endregion // BorderStyleCell

		#region FixedRowIndicatorResolved
		
		internal FixedRowIndicator FixedRowIndicatorResolved
		{
			get
			{
				return this.parentCollection.IsRootRowsCollection
					&& this.IsDataRow 
					// Do not display the fixed row indicator icon on the template add-row
					// or add-row from template.
					//
					&& ! this.IsAddRowFromTemplate
					&& DefaultableBoolean.False != this.AllowFixing 
					? this.Band.FixedRowIndicatorResolved 
					: FixedRowIndicator.None;
			}
		}

		#endregion // FixedRowIndicatorResolved

		#region OnFixedRowIndicatorClicked

		internal void OnFixedRowIndicatorClicked( )
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			BeforeRowFixedStateChangedEventArgs e = new BeforeRowFixedStateChangedEventArgs( this, ! this.Fixed );
			if ( null != grid )
				grid.FireEvent( GridEventIds.BeforeRowFixedStateChanged, e );

			if ( ! e.Cancel )
			{
				// Call MaintainScrollPositionHelper to ensure that the row is visible after it's fixed.
				//
				// SSP 5/12/05 BR03865
				//
				int deltaScrollPos = FixedRowStyle.Top == this.FixedResolvedLocation ? -1 : 0;
				this.ParentCollection.MaintainScrollPositionHelper( deltaScrollPos );

				this.Fixed = e.NewFixedState;

				grid.FireEvent( GridEventIds.AfterRowFixedStateChanged, new AfterRowFixedStateChangedEventArgs( this ) );
			}
		}

		#endregion // OnFixedRowIndicatorClicked

		#region CreateCell
		
		internal virtual UltraGridCell CreateCell( UltraGridColumn column )
		{
			return new UltraGridCell( this.Cells, column );
		}

		#endregion // CreateCell

		#region IsDataRow

		/// <summary>
		/// Returns true if the row is associated with a row in the data source. This property returns
		/// false for group-by rows, filter rows, template add-rows, summary rows etc...
		/// </summary>
		public virtual bool IsDataRow
		{
			get
			{
				return ! this.IsTemplateAddRow;
			}
		}

		#endregion // IsDataRow

		#region IsFilterRow
		
		/// <summary>
		/// Indicates whether the row is a filter row.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// To get the filter row associated with a row colleciton, use the RowsCollection's <see cref="RowsCollection.FilterRow"/> property.
		/// </p>
		/// <p class="body">
		/// To enable the filter row functionality, set the <seealso cref="UltraGridOverride.FilterUIType"/>
		/// to <b>FilterRow</b>. Also make sure that <seealso cref="UltraGridOverride.AllowRowFiltering"/> is 
		/// set to <b>True</b>.
		/// </p>
		/// <seealso cref="UltraGridOverride.FilterUIType"/>
		/// <seealso cref="UltraGridOverride.AllowRowFiltering"/>
		/// <seealso cref="RowsCollection.FilterRow"/>
		/// <seealso cref="UltraGridFilterRow"/>
		/// </remarks>
		public virtual bool IsFilterRow
		{
			get
			{
				return false;
			}
		}

		#endregion // IsFilterRow

		#region IsSummaryRow
		
		/// <summary>
		/// Indicates whether the row is a summary row.
		/// </summary>
		public virtual bool IsSummaryRow
		{
			get
			{
				return false;
			}
		}

		#endregion // IsSummaryRow

		#region IsSpecialRow
		
		internal virtual bool IsSpecialRow
		{
			get
			{
				return false;
			}
		}

		#endregion // IsSpecialRow

		#region ResolveRowPromptAppearance

		internal void ResolveRowPromptAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			// SSP 3/12/06 - App Styling
			// 
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role;

			UltraGridLayout layout = this.Layout;

			if ( this.IsTemplateAddRow )
			{
				// SSP 3/12/06 - App Styling
				// 
				role = StyleUtils.GetRole( layout, StyleUtils.Role.TemplateAddRowPrompt, out order );

				this.band.MergeBLOverrideAppearances( ref appData, ref flags, 
					UltraGridOverride.OverrideAppearanceIndex.TemplateAddRowPromptAppearance
					// SSP 3/13/06 - App Styling
					// Use the new overload that takes in the app styling related params.
					// 
					, role, order, AppStyling.RoleState.Normal );
			}
			else if ( this.IsFilterRow )
			{
				// SSP 3/12/06 - App Styling
				// 
				role = StyleUtils.GetRole( layout, StyleUtils.Role.FilterRowPrompt, out order );
			
				this.band.MergeBLOverrideAppearances( ref appData, ref flags, 
					UltraGridOverride.OverrideAppearanceIndex.FilterRowPromptAppearance
					// SSP 3/13/06 - App Styling
					// Use the new overload that takes in the app styling related params.
					//
					, role, order, AppStyling.RoleState.Normal );
			}

			// By default resolve the back colors to transparent so the prompt elem doesn't draw 
			// any background. Do so only if the BackColor off the prompt appearance hasn't
			// been set.
			//
			if ( 0 != ( AppearancePropFlags.BackColorAlpha & flags )
				&& ! appData.HasPropertyBeenSet( AppearancePropFlags.BackColor ) )
			{
				flags ^= AppearancePropFlags.BackColorAlpha;
				appData.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
			}

			// Also resolve the text trimming to EllipsisCharacter so the user can see that
			// the text is cut off.
			//
			if ( 0 != ( AppearancePropFlags.TextTrimming & flags ) )
			{
				flags ^= AppearancePropFlags.TextTrimming;
				appData.TextTrimming = TextTrimming.EllipsisCharacter;
			}

			// Resolve the alignment to be the middle.
			//
			GridUtils.ResolveAlignments( ref appData, ref flags, HAlign.Default, VAlign.Middle, HAlign.Default, VAlign.Middle );
		}

		#endregion // ResolveRowPromptAppearance

		#region RowPromptResolved

		internal virtual string RowPromptResolved
		{
			get
			{
				if ( this.IsTemplateAddRow )
					return this.band.TemplateAddRowPromptResolved;

				return null;
			}
		}

		#endregion // RowPromptResolved

		#region GetRowPromptSize

		internal Size GetRowPromptSize( int maxWidth )
		{
			string promptText = this.RowPromptResolved;
			if ( null != promptText && promptText.Length > 0 )
			{
				AppearanceData appData = new AppearanceData( );
				AppearancePropFlags flags = AppearancePropFlags.FontData;
				this.ResolveRowPromptAppearance( ref appData, ref flags );
				this.ResolveAppearance( ref appData, flags );

				return this.Layout.CalculateFontSize( ref appData, this.RowPromptResolved, maxWidth );
			}

			return Size.Empty;
		}

		#endregion // GetRowPromptSize

		#region IsUIElementCompatible
		
		internal virtual bool IsUIElementCompatible( UIElement parent, UIElement elem )
		{
			return this.IsCard && parent is RowColRegionIntersectionUIElement
				? elem is CardAreaUIElement : elem is RowUIElement;
		}

		#endregion // IsUIElementCompatible

		#region HasSameContext

		internal virtual bool HasSameContext( UIElement parent, UIElement elem )
		{
			if ( this.IsCard && parent is RowColRegionIntersectionUIElement )
			{
				CardAreaUIElement cardAreaElem = elem as CardAreaUIElement;
				if ( null != cardAreaElem )
					return this.ParentCollection == cardAreaElem.Rows;
			}
			else
			{
				RowUIElement rowElem = elem as RowUIElement;
				if ( null != rowElem )
					return this == rowElem.contextRow;
			}

			return false;
		}

		#endregion // HasSameContext

		#region CreateRowUIElement
		
		internal virtual UIElement CreateRowUIElement( UIElement parent )
		{
			return this.IsCard && parent is RowColRegionIntersectionUIElement
				? (UIElement)( new CardAreaUIElement( parent ) )
				: (UIElement)( new RowUIElement( parent ) );
		}

		#endregion // CreateRowUIElement

		#region InitializeUIElement

		internal virtual void InitializeUIElement( UIElement elem )
		{
			RowUIElement rowElem = elem as RowUIElement;
			if ( null != rowElem )
			{
				rowElem.InitializeRow( this );
			}
			else
			{
				CardAreaUIElement cardAreaElem = elem as CardAreaUIElement;
				if ( null != cardAreaElem )
					cardAreaElem.InitializeRow( this );
				else 
					Debug.Assert( false, "Wrong type of element for the row." );
			}
		}

		#endregion // InitializeUIElement

		#region AdjustUIElementRect
		
		internal virtual void AdjustUIElementRect( ref Rectangle elemRect )
		{
			// Do nothing. Derived classes can override this there is a need to
			// adjust the rect.
		}

		#endregion // AdjustUIElementRect

		#region SetElementClipSelfRect
		
		internal virtual void SetElementClipSelfRect( UIElement elem, Rectangle clipRect )
		{
			RowUIElementBase rowElem = elem as RowUIElementBase;
			if ( null != rowElem )
				rowElem.InternalSetClipSelfRegion( clipRect );
		}

		#endregion // SetElementClipSelfRect

		#region AfterElementAdded

		#region AddSpecialRowSeparatorElemHelper

		private void AddSpecialRowSeparatorElemHelper( bool before, UIElement addedRowElem, UIElementsCollection oldElems, ref Rectangle actualRectOccupiedByRowRelatedElements )
		{
			Rectangle rect = addedRowElem.Rect;
			int spacing = before ? this.RowSpacingBeforeResolved : this.RowSpacingAfterResolved;
			int separatorHeight = Math.Min( spacing, this.BandInternal.SpecialRowSeparatorHeightResolved );

			// Rect is the rect of the row element. It does not include the row spacing before or after.
			//
			rect.Y = before ? rect.Y - spacing : rect.Bottom + spacing - separatorHeight;
			rect.Height = separatorHeight;

			if ( separatorHeight > 0 )
			{
				SpecialRowSeparatorUIElement separatorElem = (SpecialRowSeparatorUIElement)GridUtils.ExtractExistingElement( oldElems, typeof( SpecialRowSeparatorUIElement ), true );
				if ( null == separatorElem )
					separatorElem = new SpecialRowSeparatorUIElement( addedRowElem.Parent );

				separatorElem.Initialize( this.ParentCollection );

				// Since row element includes the pre row area take it out of the row separator elem.
				//
				if ( ! this.IsSummaryRow )
				{
					int preRowAreaExtent = this.BandInternal.PreRowAreaExtent;
					rect.X += preRowAreaExtent;
					rect.Width -= preRowAreaExtent;
				}

				if ( this.Layout.CanMergeAdjacentBorders( separatorElem.BorderStyle ) )
				{
					rect.Y--;
					rect.Height++;
				}

				separatorElem.Rect = rect;
				actualRectOccupiedByRowRelatedElements = Rectangle.Union( actualRectOccupiedByRowRelatedElements, rect );
				addedRowElem.Parent.ChildElements.Add( separatorElem );
			}
		}

		#endregion // AddSpecialRowSeparatorElemHelper

		internal virtual void AfterElementAdded( UIElement addedRowElem, UIElementsCollection oldElems, ref Rectangle actualRectOccupiedByRowRelatedElements )
		{
			// Add the row separator elements if any.
			//
			if ( this.HasSpecialRowSeparatorBefore )
				this.AddSpecialRowSeparatorElemHelper( true, addedRowElem, oldElems, ref actualRectOccupiedByRowRelatedElements );

			if ( this.HasSpecialRowSeparatorAfter )
				this.AddSpecialRowSeparatorElemHelper( false, addedRowElem, oldElems, ref actualRectOccupiedByRowRelatedElements );
		}

		#endregion // AfterElementAdded

		#region GetElementLastClipRect

		internal virtual Rectangle GetElementLastClipRect( UIElement elem )
		{
			RowUIElementBase rowElem = elem as RowUIElementBase;
			if ( null != rowElem )
				return rowElem.lastClipRect;

			CardAreaUIElement cardAreaElem = elem as CardAreaUIElement;
			if ( null != cardAreaElem )
				return cardAreaElem.lastClipRect;

			return Rectangle.Empty;
		}

		#endregion // GetElementLastClipRect

		#region SetElementLastClipRect

		internal virtual void SetElementLastClipRect( UIElement elem, Rectangle clipRect )
		{
			RowUIElementBase rowElem = elem as RowUIElementBase;
			if ( null != rowElem )
				rowElem.lastClipRect = clipRect;

			CardAreaUIElement cardAreaElem = elem as CardAreaUIElement;
			if ( null != cardAreaElem )
				cardAreaElem.lastClipRect = clipRect;
		}

		#endregion // SetElementLastClipRect

		#region RowSelectorExtentResolved

		internal virtual int RowSelectorExtentResolved
		{
			get
			{
				return this.band.RowSelectorExtent;
			}
		}

		#endregion // RowSelectorExtentResolved

		#region GetEditorOwnerInfo

		internal virtual GridEmbeddableEditorOwnerInfoBase GetEditorOwnerInfo( UltraGridColumn column )
		{
			return column.EditorOwnerInfo;
		}

		#endregion // GetEditorOwnerInfo

		#region GetEditorResolved

		/// <summary>
		/// Gets the resolved editor for the cell associated with this row and the specified column.
		/// </summary>
        /// <param name="column">The column intersecting the row from which the value will be retrieved.</param>
        /// <returns>The resolved editor for the cell associated with this row and the specified column.</returns>
        // MRS v7.2 - Document Exporter
		//internal virtual EmbeddableEditorBase GetEditorResolved( UltraGridColumn column )
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public virtual EmbeddableEditorBase GetEditorResolved(UltraGridColumn column)
		{
			UltraGridCell cell = this.GetCellIfAllocated( column );

			if ( null != cell )
				return cell.EditorResolved;
			else
				return column.Editor;
		}

		#endregion // GetEditorResolved

		#region GetValueListResolved
		
		internal virtual IValueList GetValueListResolved( UltraGridColumn column )
		{
			UltraGridCell cell = this.GetCellIfAllocated( column );

			if ( null != cell && null != cell.ValueList )
				return cell.ValueList;
			else
				return column.ValueList;
		}

		#endregion // GetValueListResolved

		#region OnActiveStateChanged

		internal virtual void OnActiveStateChanged( bool activated )
		{
			// Do nothing.
		}

		#endregion // OnActiveStateChanged

		#region LayoutManager
		
		internal virtual Infragistics.Win.Layout.LayoutManagerBase LayoutManager
		{
			get
			{
				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout - Optimizations
				// For efficiency reasons don't bother checking the row sizing mode if the
				// rowAutoSizeLayoutManagerHolder hasn't been allocated because if the row sizing mode
				// were one of the Auto or if PerformAutoSize were called on the row then 
				// rowAutoSizeLayoutManagerHolder would have been allocated. Non-presence of 
				// rowAutoSizeLayoutManagerHolder means the row sizing mode is not one of the Auto 
				// modes.
				// 
				// ------------------------------------------------------------------------------------
				if ( null != this.rowAutoSizeLayoutManagerHolder )
				{
					RowSizing rowSizing = this.BandInternal.RowSizingResolved;
					if ( RowSizing.Sychronized != rowSizing )
						return this.RowAutoSizeLayoutManagerHolder.RowLayoutManager;
					// If RowSizing is Sychronized then null out the rowAutoSizeLayoutManagerHolder.
					// This condition would only arise if RowSizing were something other than 
					// Synchronized and it was switched back to Synchronized. This is strictly for
					// efficiency reasons. We don't want to be holding onto an object if it's not
					// needed anymore.
					// 
					else 
						this.rowAutoSizeLayoutManagerHolder = null;
				}
				// ------------------------------------------------------------------------------------

				return this.BandInternal.RowLayoutManager;
			}
		}

		#endregion // LayoutManager

		#region RowScrollTip

		// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries
		// Added RowScrollTip property.
		// 
		internal virtual string RowScrollTip
		{
			get
			{
				UltraGridColumn scrollTipCol = this.Band.ScrollTipCol;
				if ( null != scrollTipCol )
				{
					string strWork = scrollTipCol.Header.Caption + ": ";

					string cellText = this.GetCellText( scrollTipCol );
					if ( null != cellText )
						strWork += cellText;

					return strWork;
				}

				return null;
			}
		}

		#endregion // RowScrollTip

		#region IsFromGroupByRowsParentCollection

		internal virtual bool IsFromGroupByRowsParentCollection
		{
			get
			{
				return null != this.ParentCollection && this.ParentCollection.IsGroupByRows;
			}
		}

		#endregion // IsFromGroupByRowsParentCollection

		#region IsCellHidden

		// SSP 6/10/05 BR04499
		// In a filter row a cell that's not allocated could be hidden if the FilterOperandStyle
		// of the associated column resolves to None.
		// 
		internal virtual bool IsCellHidden( UltraGridColumn column )
		{
			UltraGridCell cell = this.GetCellIfAllocated( column );
			return null != cell && cell.Hidden;
		}

		#endregion // IsCellHidden

		#region HasHeadersOverride
		
		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// 
		internal virtual DefaultableBoolean HasHeadersOverride
		{
			get
			{
				return DefaultableBoolean.Default;
			}
		}

		#endregion // HasHeadersOverride

		#endregion // NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region RowSelectorNumber

		// SSP 5/6/05 - NAS 5.2 Row Numbers
		// Added RowSelectorNumer property.
		//
		/// <summary>
		/// Returns the number that's going to be displayed in the row selector. Retruns -1 if
		/// row selector number style is set to None.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// To display row numbers in the row selectors set the Override's 
		/// <see cref="UltraGridOverride.RowSelectorNumberStyle"/> property. 
		/// Also make sure that row selectors are enabled using the Override's
		/// <see cref="UltraGridOverride.RowSelectors"/>.
		/// </p>
		/// <seealso cref="UltraGridOverride.RowSelectorNumberStyle"/>
		/// <seealso cref="UltraGridOverride.RowSelectors"/>
		/// <seealso cref="UltraGridOverride.RowSelectorWidth"/>
		/// </remarks>
		// SSP 8/5/05
		// Made the property public.
		// 
		//internal int RowSelectorNumer
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Never ) ]
		public int RowSelectorNumer
		{
			get
			{
				return this.RowSelectorNumber;
			}
		}

		// SSP 2/21/06 BR10300
		// RowSelectorNumber was mis-named as RowSelectorNumer above. So added this new version
		// with correct name. Since the property is public, left it there except marked it
		// Browsable( false ).
		// 
		/// <summary>
		/// Returns the number that's going to be displayed in the row selector. Retruns -1 if
		/// row selector number style is set to None.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// To display row numbers in the row selectors set the Override's 
		/// <see cref="UltraGridOverride.RowSelectorNumberStyle"/> property. 
		/// Also make sure that row selectors are enabled using the Override's
		/// <see cref="UltraGridOverride.RowSelectors"/>.
		/// </p>
		/// <seealso cref="UltraGridOverride.RowSelectorNumberStyle"/>
		/// <seealso cref="UltraGridOverride.RowSelectors"/>
		/// <seealso cref="UltraGridOverride.RowSelectorWidth"/>
		/// </remarks>
		public int RowSelectorNumber
		{
			get
			{
				int rowNumber = -1;
				switch ( band.RowSelectorNumberStyleResolved )
				{
					case RowSelectorNumberStyle.ListIndex:
						rowNumber = this.ListIndex;
						break;
					case RowSelectorNumberStyle.VisibleIndex:
						rowNumber = this.ParentCollection.GetVisibleIndex( this, false );
						break;
					case RowSelectorNumberStyle.RowIndex:
						rowNumber = this.Index;
						break;
				}

				// SSP 8/5/05
				// Moved this here from the row selector element's position child elements.
				// 
				const int NUMBER_OFFSET = 1;
				if ( rowNumber >= 0 )
					rowNumber += NUMBER_OFFSET;

				return rowNumber;
			}
		}

		#endregion // RowSelectorNumber

		// MRS 7/11/05 - BR04865
		// ------------------------------------
		#region GetExportValue

		/// <summary>
		/// Gets the Value of the cell that should be used for exporting. 
		/// </summary>
		/// <param name="column">Associated <b>Column</b></param>
        /// <returns>The Value of the cell that should be used for exporting. </returns>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public object GetExportValue(UltraGridColumn column)
		{
			if (this.ShouldExportText(column))
				return this.GetCellText(column);
            else
            {
                // MRS 11/28/06 - BR17952
                // Run this through the DataFilter
                //
                //return this.GetCellValue(column);
                return this.GetDataFilteredCellValue(column);
            }
		}

		#endregion GetExportValue

		#region ShouldExportText
		private bool ShouldExportText(UltraGridColumn column)
		{
            // MRS 8/14/2009 - TFS20291
            // If the editor is applying a password char, then we should export as text. 
            GridEmbeddableEditorOwnerInfoBase cellEditorOwnerInfo = this.GetEditorOwnerInfo(column);
            if (cellEditorOwnerInfo != null)
            {
                char passwordChar;
                if (cellEditorOwnerInfo.GetPasswordChar(this, out passwordChar))
                    return true;
            }

			if(this.HasCell(column))
			{
				if (this.Cells[column].ValueListResolved!=null)
					return true;                
			}
			else if (column.ValueList!=null)
				return true;

			EmbeddableEditorBase editor = column.GetEditor(this);
			return !editor.ComparesByValue(column.EditorOwnerInfo, this);
		}

		#endregion ShouldExportText
		// ------------------------------------

		// SSP 7/7/05 - NAS 5.3 Empty Rows Feature
		// 
		#region NAS 5.3 Empty Rows

		#region IsEmptyRow

		/// <summary>
		/// Returns true if this row is an instance of <see cref="UltraGridEmptyRow"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Returns true if this row is an instance of <see cref="UltraGridEmptyRow"/>.
		/// Instances of <i>UltraGridEmptyRow</i>s represent empty rows when the Empty Rows 
		/// functionality is enabled.
		/// </p>
		/// <seealso cref="UltraGridEmptyRow"/> <seealso cref="UltraGridLayout.EmptyRowSettings"/> <seealso cref="EmptyRowSettings.ShowEmptyRows"/>
		/// </remarks>
		public virtual bool IsEmptyRow
		{
			get
			{
				return false;
			}
		}

		#endregion // IsEmptyRow

		#region PreRowAreaExtentResolved

		internal virtual int PreRowAreaExtentResolved
		{
			get
			{
				return this.band.PreRowAreaExtent;
			}
		}

		#endregion // PreRowAreaExtentResolved

		#region AdjustCellUIElementRect
		
		internal virtual void AdjustCellUIElementRect( RowCellAreaUIElementBase rowCellAreaElem, UltraGridColumn column, ref Rectangle cellElemRect )
		{
			// Do nothing. Derived classes can override this there is a need to
			// adjust the rect.
		}

		#endregion // AdjustCellUIElementRect

		#endregion // NAS 5.3 Empty Rows

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		#region ToolTipText
		
		/// <summary>
		/// Specifies the tooltip to display when the user hovers the mouse over the header.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies the tooltip to display when the user hovers the mouse over the header. No tooltip is displayed if this property is set to null or empty string.</p>
		/// <seealso cref="UltraGridCell.ToolTipText"/>
		/// <seealso cref="HeaderBase.ToolTipText"/>
		/// <seealso cref="SummarySettings.ToolTipText"/>
		/// <seealso cref="SummaryValue.ToolTipText"/>
		/// </remarks>
		public string ToolTipText
		{
			get
			{
				return this.toolTipText;
			}
			set
			{
				if ( this.toolTipText != value )
				{
					this.toolTipText = value;
					this.NotifyPropChange( PropertyIds.ToolTipText );
				}
			}
		}

		#endregion // ToolTipText

		#region ResetToolTipText

		/// <summary>
		/// Resets the property to its default value of false.
		/// </summary>
		public void ResetToolTipText( )
		{
			this.ToolTipText = null;
		}

		#endregion // ResetToolTipText

		#region HasRowHotTrackingAppearances

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal bool HasRowHotTrackingAppearances
		{
			get
			{
				return this.band.HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex.HotTrackRowAppearance 
					// SSP 3/21/06 - App Styling
					// 
					, StyleUtils.Role.Row, AppStyling.RoleState.HotTracked )
					|| this.band.HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex.HotTrackRowCellAppearance 
					// SSP 3/21/06 - App Styling
					// 
					, StyleUtils.Role.Cell, AppStyling.RoleState.RowHotTracked );
			}
		}

		#endregion // HasRowHotTrackingAppearances

		#region HasRowSelectorHotTrackingAppearances

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal bool HasRowSelectorHotTrackingAppearances
		{
			get
			{
				return this.band.HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex.HotTrackRowSelectorAppearance
					// SSP 3/21/06 - App Styling
					// 
					, StyleUtils.Role.RowSelector, AppStyling.RoleState.RowHotTracked );
			}
		}

		#endregion // HasRowSelectorHotTrackingAppearances

		#region HasCellHotTrackingAppearances

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal bool HasCellHotTrackingAppearances( UltraGridColumn column )
		{
			
			
			
			bool hasAppearanceBeenAllocated = this.band.HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex.HotTrackCellAppearance
				// SSP 3/21/06 - App Styling
				// 
				, StyleUtils.Role.Cell, AppStyling.RoleState.HotTracked );

			if ( hasAppearanceBeenAllocated )
			{
				AppearancePropFlags flags = AppearancePropFlags.AllRender;
				AppearanceData appData = new AppearanceData( );
				this.band.MergeBLOverrideAppearances( ref appData, ref flags,
					UltraGridOverride.OverrideAppearanceIndex.HotTrackCellAppearance,
					StyleUtils.Role.Cell, AppStyling.RoleState.HotTracked );

				if ( 0 != appData.NonDefaultSettings )
					return true;
			}

			return false;

			
			
		}

		#endregion // HasCellHotTrackingAppearances

		#region GetAncestorRow

		// SSP 8/18/05 P856
		// 
		internal UltraGridRow GetAncestorRow( UltraGridBand ancestorBand )
		{
			if ( this.Band == ancestorBand )
				return this;

			UltraGridRow parentRow = this.ParentRow;
			return null != parentRow ? parentRow.GetAncestorRow( ancestorBand ) : null;
		}

		#endregion // GetAncestorRow

		// AS 1/26/06 BR09335
		// Added this so we could determine if we can access
		// the cells collection instead of having to check
		// if this is a group by row.
		//
		#region SupportsCells

		internal virtual bool SupportsCells
		{
			get { return true; }
		} 
		#endregion //SupportsCells

		#region UseRowLayoutResolved

		// SSP 6/8/06 BR13501
		// 
		internal virtual bool UseRowLayoutResolved
		{
			get
			{
				return null != this.band && this.band.UseRowLayoutResolved;
			}
		}

		#endregion // UseRowLayoutResolved

		#region DeallocateCells

		// SSP 8/26/06 - NAS 6.3
		// Added ClearCells method.
		// 
		/// <summary>
		/// Releases allocated cell objects. Cells are allocated lazily as they are accessed.
		/// This method is for releasing cells that have been allocated so for.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Cells are lazily allocated. They are allocated as they are accessed. However once a cell
		/// is allocated, it remains referenced until the row is removed. You can use this method to 
		/// release references to the cells that have been allocated. Note that they will be re-allocated
		/// when they are accessed next time.
		/// </p>
		/// <p class="body">
		/// One use for this method is to reset all the cells to their default settings. For example,
		/// if you have set some settings on the cells of a row and you want to clear all those settings
		/// then you can use this method to simply clear the cell collection. This will have the same
		/// effect as having reset all the cells of the row with one added benefit that the references
		/// to the cells will have been released and thus may result in improved memory usage. Note
		/// that the cells will be re-allocated when they are accessed the next time. This method is 
		/// exposed on the <see cref="RowsCollection"/> as well so this operation can be performed on 
		/// an entire row collection as well.
		/// </p>
		/// <p class="body">
		/// <b>Note:</b> If one of the cells of this row is the current active cell or is selected then
		/// it will not be cleared from the collection as the ActiveCell property of the grid (or
		/// the SelectedCellsCollection in the case of selected cells) is holding reference to the
		/// cell. Such cells will not be cleared. They will remain part of the cell collection. However
		/// their settings will be reset. In other words, the end result will be the same.
		/// </p>
		/// <p class="body">
		/// Note that any references to the cells from this row after ClearCells call should be considered
		/// invalid (except for the active cell and selected cells as noted above). Also the cell collection 
		/// itself is released, which is also lazily allocated.
		/// </p>
		/// </remarks>
		public void DeallocateCells( )
		{
			this.DeallocateCellsHelper( this.Layout.ActiveCell, false, false );
		}

		internal void DeallocateCellsHelper( UltraGridCell activeCell, bool retainUnboundData, bool recursive )
		{
			if ( null != this.cells )
			{
				if ( this.cells.DeallocateCellsHelper( activeCell, retainUnboundData ) )
				{
					this.cells.Dispose( );
					this.cells = null;
				}
			}

			if ( null != this.childBands && recursive )
			{
				ChildBandsCollection list = this.childBands;
				for ( int i = 0, count = list.Count; i < count; i++ )
				{
					UltraGridChildBand childBand = list[i];
					if ( childBand.HasRowsBeenAllocated )
						childBand.Rows.DeallocateCellsHelper( activeCell, retainUnboundData, recursive );
				}
			}
		}

		#endregion // DeallocateCells

        // MRS 11/28/06 - BR17952
        #region GetDataFilteredCellValue
        internal object GetDataFilteredCellValue(UltraGridColumn column)
		{
			// MD 2/27/09 - TFS14684
			// Moved all code to the new overload
			return this.GetDataFilteredCellValue( column, false );
		}

		// MD 2/27/09 - TFS14684
		// Added new overload
		internal object GetDataFilteredCellValue( UltraGridColumn column, bool resolveEnumValue )
        {
            object cellVal = this.GetCellValue(column);

            // CDS 2/26/09 TFS14684 GetCellValue() returns an Int32 for enums, 
            // so attempt to convert it back to the enum 
			if ( resolveEnumValue )
				cellVal = UltraGridRow.ResolveEnumValue( column, cellVal );

            EmbeddableEditorBase editor = this.GetEditorResolved(column);

            if (editor != null)
            {
                GridEmbeddableEditorOwnerInfoBase cellEditorOwnerInfo = this.GetEditorOwnerInfo(column);

                object filteredEV = editor.GetDataFilteredDestinationValue(cellVal,
                    ConversionDirection.OwnerToEditor, cellEditorOwnerInfo, this);

                return filteredEV;
            }

            return cellVal;
        }
        #endregion GetDataFilteredCellValue

        // MBS 2/20/08 - RowEditTemplate
        #region RowEditTemplate

        private UltraGridRowEditTemplate rowEditTemplate;

        /// <summary>
        /// Gets or sets the template used for editing the row.
        /// </summary>
        public UltraGridRowEditTemplate RowEditTemplate
        {
            get { return this.rowEditTemplate; }
            set
            {
                // We don't support the use of a RowEditTemplate in any of the special rows (i.e. FilterRow, GroupByRow, etc)
                if (this is UltraGridSpecialRowBase)
                    throw new InvalidOperationException(SR.GetString("LE_UltraGridBand_SpecialRow_RowEditTemplate"));

                if (this.rowEditTemplate == value)
                    return;

                // The user cannot use the same template on different bands
                if (value != null && value.Band != null && value.Band != this.Band)
                    throw new InvalidOperationException(SR.GetString("LE_UltraGridRowEditTemplate_SetToMultipleBands"));

                if (this.rowEditTemplate != null)
                {
                    if (this.rowEditTemplate.IsShown)
                        throw new InvalidOperationException(SR.GetString("LE_UltraGridRowOrBand_SetTemplateWhileShown"));

                    // If we're resetting the template, we need to clear out
                    // the template that was associated with this band previously
                    this.rowEditTemplate.Initialize(null);
                }

                this.rowEditTemplate = value;

                if (this.rowEditTemplate != null)
                    this.rowEditTemplate.Initialize(this.Band);

                this.NotifyPropChange(PropertyIds.RowEditTemplate);
            }
        }

        /// <summary>
        /// Returns the row's RowEditTemplate, if set; otherwise, returns the template associated with the band.
        /// </summary>
        public UltraGridRowEditTemplate RowEditTemplateResolved
        {
            get
            {
                // MBS 5/16/08 - BR33018
                // We only support a RET on standard rows.
                if (this is UltraGridSpecialRowBase)
                    return null;

                if (this.rowEditTemplate != null)
                    return this.rowEditTemplate;

                return this.Band.RowEditTemplate;
            }
        }
        #endregion //RowEditTemplate
        //
        #region ShowEditTemplate

        /// <summary>
        /// Shows the RowEditTemplate associated with the row, if any.
        /// </summary>
        /// <returns>True if the template was successfully shown, false otherwise.</returns>
        public bool ShowEditTemplate()
        {
            return this.ShowEditTemplate(false, TemplateDisplaySource.Manual);
        }

        internal bool ShowEditTemplate(bool preventForHiddenOrDisabledRow, TemplateDisplaySource source)
        {
            // We only support the template for an UltraGrid
            UltraGrid grid = this.Layout.Grid as UltraGrid;
            if (grid == null)
                return false;

            // Can't show a template if there isn't one associated with the row
            UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
            if (template == null || template.IsShown)
                return false;

            if (preventForHiddenOrDisabledRow && (this.HiddenResolved || this.IsDisabled))
                return false;

            if (grid.ActiveCell != null && grid.ActiveCell.IsInEditMode)
            {
                // If a cell is currently being edited before the template is shown,
                // we need to try to exit edit mode before showing the template.  If
                // this operation is unsuccessful, we can't show the template.
                // NOTE: The ExitEditMode function returns true on failure.
                if (grid.ActiveCell.ExitEditMode(false, false) == true)
                    return false;
            }

            UltraGridRow oldActiveRow = grid.ActiveRow;

            // If we can't activate this row, then do not show the template
            if (this.IsActiveRow == false && this.Activate())
                return false;

            // At this point a user might have handled the RowEditTemplateRequested event and 
            // displayed the template somewhere (i.e. a custom form).  Since we have no control
            // over the template at that point, we need to see if a template existed on the old 
            // active row.  If it did and it's the same template, we can simply update the Row; 
            // otherwise, we need to null out the row since the template is no longer valid
            if (oldActiveRow != null)
            {
                UltraGridRowEditTemplate oldTemplate = oldActiveRow.RowEditTemplateResolved;
                if (oldTemplate != null && oldTemplate.Row != null)
                {
                    // If the Row isn't null at this point, then it was never closed by our
                    // own methods (implicitly or explicitly), since we would have caught
                    // this scenario when checking the IsShown property above.
                    if (oldTemplate == template)
                        oldTemplate.Row = this;
                    else
                        oldTemplate.Row = null;
                }
            }

            // It's possible that the user has not set the template on the band and instead
            // is doing it on individual rows, or even that the user nulled out the band's
            // RowEditTemplate property after assigning it on the rows, so make sure that
            // the template is aware of its band at this point
            if (template.Band == null)
                template.Initialize(this.Band);

            // Set the row so that the template is initialized when
            // the user examines the Template property in the events            
            template.Row = this;

            // Notify the user that we're about to show the template, so they can handle it (or not)
            RowEditTemplateRequestedEventArgs e = new RowEditTemplateRequestedEventArgs(template);
            grid.FireEvent(GridEventIds.RowEditTemplateRequested, e);
            if (e.Handled == true)
                return true;

            bool ret = template.Display(grid.ActiveCell, source);
            if (!ret)
            {
                // If we weren't able to show the template, we should null out the row
                // so that any editors are unhooked.
                template.Row = null;
            }

            return ret;
        }
        #endregion //ShowEditTemplate
        //
        #region IsEditTemplateShown

        internal bool IsEditTemplateShown
        {
            get
            {
                UltraGridRowEditTemplate template = this.RowEditTemplateResolved;
                if (template == null)
                    return false;

                return template.IsShown;
            }
        }
        #endregion //IsEditTemplateShown

        // CDS NAS v9.1 Header CheckBox
        #region GetCurrentDataFilteredCellValue

        internal object GetCurrentDataFilteredCellValue(UltraGridColumn column)
        {
            UltraGridCell cell = this.GetCellIfAllocated(column);

            if (null != cell &&
                cell.IsInEditMode)
            {
                //if the cell is in edit-mode, get the editors value and pass it through the datafilter.
                EmbeddableEditorBase editor = this.GetEditorResolved(column);
                if (editor != null)
                {
                    GridEmbeddableEditorOwnerInfoBase cellEditorOwnerInfo = this.GetEditorOwnerInfo(column);
                    object filteredEV = editor.GetDataFilteredDestinationValue(editor.Value,
                        ConversionDirection.OwnerToEditor, cellEditorOwnerInfo, this);
                    return filteredEV;
                }
            }

			// MD 2/27/09 - TFS14684
			// In this situation, we want to resolve int values to their enum equivalents if the column has an 
			// enum data type, so pass in True as the second parameter.
            //return this.GetDataFilteredCellValue(column);
			return this.GetDataFilteredCellValue( column, true );
        }

        #endregion GetCurrentDataFilteredCellValue

        // MRS 3/18/2009 - TFS15302
        #region GetGrid
        private UltraGridBase GetGrid()
        {
            UltraGridBand band = this.Band;
            if (band == null)
                return null;

            UltraGridLayout layout = band.Layout;
            if (layout == null)
                return null;

            return layout.Grid;
        }
        #endregion //GetGrid

    }//end of class
}
