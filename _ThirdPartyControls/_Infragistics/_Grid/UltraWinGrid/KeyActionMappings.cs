#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    ///    Summary description for KeyActionMappings.
    /// </summary>
    public class GridKeyActionMappings : KeyActionMappingsBase
    {
		/// <summary>
		/// Constructor
		/// </summary>
        public GridKeyActionMappings( )
            : base( 75 )
        {
        }

		// SSP 10/10/03
		// This method is not used anymore. Commented out. We implemented framework-wide change
		// to fix the bug that this was added in the first to fix.
		//
		

        /// <summary>
        /// Called the first time GetActionMapping
        /// is called (enables lazy loading of mappings)
        /// </summary>
        public override void LoadDefaultMappings() 
        {
            GridKeyActionMapping [] defaultMappings =
                new GridKeyActionMapping [] 
            {
				// SSP 6/26/02
				// Due to changes in keyboard handling, I added new SwapDroppedDown state
				// and all the changes below regarding UltraGridState.SwapDroppedDown 
				// unless otherwise indicated are made by me.
				//

                //                    KeyCode          Action                              Disallowed state                Required state                  Disallowed special keys     Required special keys		skip trailing actions
                //                    ---------------  ----------------------------------  ------------------------------  ------------------------------  --------------------------  -----------------------      --------------------=
                new GridKeyActionMapping( Keys.Right,      UltraGridAction.NextCell,           UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.AltCtrl,        0 ),
                new GridKeyActionMapping( Keys.Tab,        UltraGridAction.NextCellByTab,      0,                              UltraGridState.Cell,            SpecialKeys.All,            0 ), 
                new GridKeyActionMapping( Keys.Left,       UltraGridAction.PrevCell,           UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.AltCtrl,        0 ),
                new GridKeyActionMapping( Keys.Tab,        UltraGridAction.PrevCellByTab,      0,                              UltraGridState.Cell,            SpecialKeys.AltCtrl,        SpecialKeys.Shift ),
                new GridKeyActionMapping( Keys.Up,         UltraGridAction.AboveCell,          UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Down,       UltraGridAction.BelowCell,          UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Home,       UltraGridAction.FirstRowInBand,     UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.AltCtrl,        0 ),
                new GridKeyActionMapping( Keys.End,        UltraGridAction.LastRowInBand,      UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.AltCtrl,        0 ),
                new GridKeyActionMapping( Keys.Right,      UltraGridAction.FirstRowInGrid,     UltraGridState.Row,             0,                              SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Down,       UltraGridAction.FirstRowInGrid,     UltraGridState.Row,             0,                              SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Home,       UltraGridAction.FirstRowInGrid,     UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.Alt,            SpecialKeys.Ctrl ),
                new GridKeyActionMapping( Keys.End,        UltraGridAction.LastRowInGrid,      UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.Alt,            SpecialKeys.Ctrl ),
                new GridKeyActionMapping( Keys.Right,      UltraGridAction.ExpandRow,          UltraGridState.Cell | 
                                                                                                 UltraGridState.RowExpanded,   UltraGridState.RowExpandable,   SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Left,       UltraGridAction.CollapseRow,        UltraGridState.Cell,            UltraGridState.RowExpanded |  
																															   UltraGridState.RowExpandable,   SpecialKeys.Alt,            0 ),
																								//RobA UWG398 10/4/01 - don't perform this action if the row is expandable, conflicts with expand action above				
                new GridKeyActionMapping( Keys.Right,      UltraGridAction.NextRow,            UltraGridState.Cell |
																								UltraGridState.RowExpandable,  UltraGridState.Row,			   SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Right,      UltraGridAction.NextRow,            UltraGridState.Cell,             
																																UltraGridState.Row |
																																UltraGridState.RowExpanded |
																																UltraGridState.RowExpandable,	   SpecialKeys.Alt,            0 ),
				// SSP 9/16/02 UWG1120
				// NextRowByTab. Added RowLast disallowed state. There is no next row to goto.
				// When the last row is the active row and there is no active cell, then pressing tab
				// should select the next control in the tab order.
				//
				//new GridKeyActionMapping( Keys.Tab,        UltraGridAction.NextRowByTab,       UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.All,            0 ), 
				new GridKeyActionMapping( Keys.Tab,        UltraGridAction.NextRowByTab,       UltraGridState.Cell 
																							   | UltraGridState.LastRowInGrid, UltraGridState.Row,             SpecialKeys.All,            0 ), 

																								//RobA UWG398 10/4/01 - don't perform this action if the row is expandable, conflicts with collapse action above						
				new GridKeyActionMapping( Keys.Left,       UltraGridAction.PrevRow,           UltraGridState.Cell |
																							  UltraGridState.RowExpanded,	    UltraGridState.Row,			   SpecialKeys.Alt,            0 ),

                // MBS 8/1/08 - BR34050
                // The previous KeyActionMapping will not happen if the row is expanded because
                // it is expecting a different mapping to collapse the row, but if the row
                // isn't expandable (i.e. the expansion indicators are hidden), that mapping
                // won't do anything, so keyboard navigation won't work.  Adding an additional
                // mapping here for when the row is expanded but not expandable remedies this
                new GridKeyActionMapping(Keys.Left,         UltraGridAction.PrevRow,            UltraGridState.Cell | 
                                                                                                UltraGridState.RowExpandable,   UltraGridState.Row | 
                                                                                                                                UltraGridState.RowExpanded,     SpecialKeys.All,            0),

				// JAS 2005 v2 GroupBy Row Extensions ---------------------------------------------------------->
				// **********************************
				// Note: if you are working with Keys.Left, be aware that there is another mapping toward the end
				// of this method which maps Keys.Left to UltraGridAction.PrevRow.
				// ---------------------------------------------------------------------------------------------->

				// SSP 9/16/02 UWG1120
				// PrevRowByTab. Added RowFirst disallowed state. There is no previous row to goto.
				// When the first row is the active row and there is no active cell, then pressing shift+tab
				// should select the previous control in the tab order.
				//
				//new GridKeyActionMapping( Keys.Tab,        UltraGridAction.PrevRowByTab,       UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.AltCtrl,        SpecialKeys.Shift ),
				new GridKeyActionMapping( Keys.Tab,        UltraGridAction.PrevRowByTab,       UltraGridState.Cell |
																							   UltraGridState.FirstRowInGrid,  UltraGridState.Row,             SpecialKeys.AltCtrl,        SpecialKeys.Shift ),

                new GridKeyActionMapping( Keys.Up,         UltraGridAction.AboveRow,           UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Down,       UltraGridAction.BelowRow,           UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Space,      UltraGridAction.ToggleCheckbox,     0,                              UltraGridState.IsCheckbox |
                                                                                                                                    UltraGridState.InEdit,          SpecialKeys.All,            0 ),
                new GridKeyActionMapping( Keys.Space,      UltraGridAction.ToggleCellSel,      UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.All,            0 ),
                new GridKeyActionMapping( Keys.Space,      UltraGridAction.ToggleRowSel,       UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.All,            0 ),
                new GridKeyActionMapping( Keys.Space,      UltraGridAction.DeactivateCell,     0,                              UltraGridState.Cell,            SpecialKeys.AltShift,       SpecialKeys.Ctrl ),
                new GridKeyActionMapping( Keys.Space,      UltraGridAction.ActivateCell,       UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.AltShift,       SpecialKeys.Ctrl ),
                new GridKeyActionMapping( Keys.Right,      UltraGridAction.NextCellInBand,     UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            SpecialKeys.Ctrl ),
                new GridKeyActionMapping( Keys.Left,       UltraGridAction.PrevCellInBand,     UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            SpecialKeys.Ctrl ),
                new GridKeyActionMapping( Keys.Home,       UltraGridAction.FirstCellInRow,     UltraGridState.InEdit |
                                                                                                    UltraGridState.CellFirst,       UltraGridState.Cell,            SpecialKeys.AltCtrl,        0 ),
                new GridKeyActionMapping( Keys.End,        UltraGridAction.LastCellInRow,      UltraGridState.InEdit  |
                                                                                                    UltraGridState.CellLast,        UltraGridState.Cell,            SpecialKeys.AltCtrl,        0 ),
                new GridKeyActionMapping( Keys.Home,       UltraGridAction.FirstCellInBand,    UltraGridState.InEdit,          UltraGridState.CellFirst,       SpecialKeys.AltCtrl,        0 ),
                new GridKeyActionMapping( Keys.End,        UltraGridAction.LastCellInBand,     UltraGridState.InEdit,          UltraGridState.CellLast,        SpecialKeys.AltCtrl,        0 ),
                new GridKeyActionMapping( Keys.Home,       UltraGridAction.FirstCellInGrid,    UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            SpecialKeys.Ctrl ),
                new GridKeyActionMapping( Keys.End,        UltraGridAction.LastCellInGrid,     UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            SpecialKeys.Ctrl ),
                new GridKeyActionMapping( Keys.Prior,      UltraGridAction.PageUpCell,         UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Next,       UltraGridAction.PageDownCell,       UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Prior,      UltraGridAction.PageUpRow,          UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Next,       UltraGridAction.PageDownRow,        UltraGridState.Cell,            UltraGridState.Row,             SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Escape,     UltraGridAction.UndoCell,           UltraGridState.IsDroppedDown,   UltraGridState.InEdit,          SpecialKeys.Alt,            0 ),

				// SSP 6/26/02
				// Changed the required state from Row to Row | RowDirty. (Added a new RowDirty state).
				//
                new GridKeyActionMapping( Keys.Escape,     UltraGridAction.UndoRow,            UltraGridState.InEdit,          UltraGridState.Row |
																															   UltraGridState.RowDirty,        SpecialKeys.Alt,            0 ),
                
				// SSP 6/31/02
				// Took out the key action mappings for closing down the drop downs. Editors will close
				// themselves.
				//
				//new GridKeyActionMapping( Keys.Escape,     UltraGridAction.CloseDropdown,      0,                              UltraGridState.IsDroppedDown,   SpecialKeys.Alt,            0 ),
                //new GridKeyActionMapping( Keys.Return,     UltraGridAction.CloseDropdown,      0,                              UltraGridState.IsDroppedDown,   SpecialKeys.Alt,            0 ),

				// SSP 11/14/01 UWG715				
				// When a group by row is selected and enter key is pressed, expand the row
				// if not already expanded otherwise collapse it.
				// (this is what the Outlook does).
				//
				new GridKeyActionMapping( Keys.Return,     UltraGridAction.ExpandRow,          UltraGridState.IsDroppedDown |
																							   UltraGridState.RowExpanded,     UltraGridState.GroupByRow | 
																															   UltraGridState.RowExpandable,   SpecialKeys.Alt,            0 ),

				new GridKeyActionMapping( Keys.Return,     UltraGridAction.CollapseRow,        UltraGridState.IsDroppedDown,   UltraGridState.GroupByRow |
																															   UltraGridState.RowExpanded | 
																															   UltraGridState.RowExpandable,   SpecialKeys.Alt,            0 ),


                new GridKeyActionMapping( Keys.F4,         UltraGridAction.ToggleDropdown,     0,                              UltraGridState.InEdit |
                                                                                                                                    UltraGridState.HasDropdown,     SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Up,         UltraGridAction.ToggleDropdown,     0,                              UltraGridState.InEdit |
                                                                                                                                    UltraGridState.HasDropdown,     0,                          SpecialKeys.Alt ),
                new GridKeyActionMapping( Keys.Down,       UltraGridAction.ToggleDropdown,     0,                              UltraGridState.InEdit |
                                                                                                                                    UltraGridState.HasDropdown,     0,                          SpecialKeys.Alt ),
				// SSP 8/8/02 UWG1308
				// If the swap drop down is dropped down, then don't toggle the edit mode
				//
				//new GridKeyActionMapping( Keys.F2,         UltraGridAction.ToggleEditMode,     0,                              UltraGridState.Cell,            SpecialKeys.Alt,            0 ),
				new GridKeyActionMapping( Keys.F2,         UltraGridAction.ToggleEditMode,     UltraGridState.SwapDroppedDown, UltraGridState.Cell,            SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.F4,         UltraGridAction.EnterEditModeAndDropdown, UltraGridState.InEdit,    UltraGridState.HasDropdown,     SpecialKeys.Alt,            0 ),
                new GridKeyActionMapping( Keys.Up,         UltraGridAction.EnterEditModeAndDropdown, UltraGridState.InEdit,    UltraGridState.HasDropdown,     0,                          SpecialKeys.Alt ),
                new GridKeyActionMapping( Keys.Down,       UltraGridAction.EnterEditModeAndDropdown, UltraGridState.InEdit,    UltraGridState.HasDropdown,     0,                          SpecialKeys.Alt ),

				// ZS 10/29/2003 UWG2118
				// F6 is disabled in edit mode because it is ambiguous (should we reject or accept changes and should we enter edit mode in next region?). Nothing changes for users as F6 was not functional even before the change.
                new GridKeyActionMapping( Keys.F6,         UltraGridAction.NextRegion,         UltraGridState.InEdit,                              0,                              SpecialKeys.All,            0 ),
                new GridKeyActionMapping( Keys.F6,         UltraGridAction.PrevRegion,         UltraGridState.InEdit,                              0,                              SpecialKeys.AltCtrl,        SpecialKeys.Shift ),

				// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
				// Added DeleteCells key action mapping. Note that DeleteCells needs to be before 
				// DeleteRows key action (see the last parameter).
				// 
				new GridKeyActionMapping( Keys.Delete, UltraGridAction.DeleteCells,	UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanDeleteCells, SpecialKeys.All, 0, true ),

                new GridKeyActionMapping( Keys.Delete,     UltraGridAction.DeleteRows,         UltraGridState.InEdit,          UltraGridState.Row,             SpecialKeys.All,            0 ),

				// SSP 8/8/02 UWG1308
				// We have ToggleEditMode and below two EnterEditMode and ExitEditMode for the
				// same keystroke F2. We either need below two or ToggleEditMode but not both.
				// So commented out below two action mappings.
				//
				//new GridKeyActionMapping( Keys.F2,         UltraGridAction.EnterEditMode,      UltraGridState.InEdit,          UltraGridState.Cell,            SpecialKeys.Alt,            0 ),
                //new GridKeyActionMapping( Keys.F2,         UltraGridAction.ExitEditMode,       0,                              UltraGridState.InEdit,          SpecialKeys.Alt,            0 ),
                
				// SSP 7/29/03 UWG1816
				// Added below 4 key action mappings for cells that are in edit mode with check editors
				// in them. What happens is that the check editor returns false for arrow keys and the grid
				// doesn't have any key action mappings for arrow keys when the cell is in edit mode.
				// Grid's IsInputKey returns false and we don't do anything. After discussing with Joe D and 
				// Mike we decided that in the case of a checkbox editor when the cell is in edit mode, then
				// we will exit the edit mode and navigate out of the checkbox cell.
				//
				// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
				new GridKeyActionMapping( Keys.Right,      UltraGridAction.NextCellInBand,		0,								UltraGridState.Cell 
																																| UltraGridState.InEdit 
																																| UltraGridState.IsCheckbox,            0,        0 ),
				new GridKeyActionMapping( Keys.Left,       UltraGridAction.PrevCellInBand,		0,								UltraGridState.Cell
																																| UltraGridState.InEdit 
																																| UltraGridState.IsCheckbox,            0,        0 ),
				new GridKeyActionMapping( Keys.Up,         UltraGridAction.AboveCell,           0,								UltraGridState.Cell
																																| UltraGridState.InEdit 
																																| UltraGridState.IsCheckbox,            0,        0 ),
				new GridKeyActionMapping( Keys.Down,       UltraGridAction.BelowCell,           0,								UltraGridState.Cell
																																| UltraGridState.InEdit 
																																| UltraGridState.IsCheckbox,            0,        0 ),
				// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

				// JAS 2005 v2 GroupBy Row Extensions
				// Now that we provide a way to disallow the end-user from being able to toggle the expansion state
				// of GroupBy rows, we need a new KeyActionMapping for a certain scenario.  If the GroupBy rows cannot
				// be expanded/collapsed, and all the rows are expanded (which they will be by default), and the user
				// has selected a GroupBy row, and the user hits the Left key, then we need to select the last child row
				// of the group preceding the 'active group.'
				//
				new GridKeyActionMapping( Keys.Left,       UltraGridAction.PrevRow,           UltraGridState.Cell |																							  
																							  UltraGridState.RowExpandable,	    UltraGridState.RowExpanded |
																																UltraGridState.Row |
																																UltraGridState.GroupByRow, 		SpecialKeys.Alt,            0 ),

				// SSP 4/23/05 - NAS 5.2 Filter Row/Fixed Add Row
				// Added CommitRow action. This is for applying filters when Enter key is 
				// pressed while the filter row is active depending on the 
				// FilterEvaluationTrigger settings. Also it's used for committing the
				// add-row. It can be used to commit regular rows as well however for that
				// the developer has to add a key action mapping.
				//
				// SSP 2/17/06 BR09132
				// If the cell is in edit mode and it's a multi-line cell then don't process
				// the CommitRow action since the Enter key in such scenario should cause a carriage
				// return to be entered into the cell.
				// Added CellMultiline flag into the disallowed state parameter.
				// 
				// --------------------------------------------------------------------------------------------------
				//new GridKeyActionMapping( Keys.Return,     UltraGridAction.CommitRow,      UltraGridState.IsDroppedDown,   UltraGridState.FilterRow | UltraGridState.RowDirty,          SpecialKeys.All,            0 ),
				//new GridKeyActionMapping( Keys.Return,     UltraGridAction.CommitRow,      UltraGridState.IsDroppedDown,   UltraGridState.AddRow    | UltraGridState.RowDirty,          SpecialKeys.All,            0 ),
				new GridKeyActionMapping( Keys.Return,     UltraGridAction.CommitRow,      UltraGridState.IsDroppedDown | UltraGridState.CellMultiline,   UltraGridState.FilterRow | UltraGridState.RowDirty,          SpecialKeys.All,            0 ),
				new GridKeyActionMapping( Keys.Return,     UltraGridAction.CommitRow,      UltraGridState.IsDroppedDown | UltraGridState.CellMultiline,   UltraGridState.AddRow    | UltraGridState.RowDirty,          SpecialKeys.All,            0 ),
				// --------------------------------------------------------------------------------------------------

				// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
				//									
				// ------------------------------------------------------------------------------
				new GridKeyActionMapping( Keys.C, UltraGridAction.Copy,		UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanCopy, SpecialKeys.Alt, SpecialKeys.Ctrl ),
				new GridKeyActionMapping( Keys.X, UltraGridAction.Cut,		UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanCut, SpecialKeys.Alt, SpecialKeys.Ctrl ),
				new GridKeyActionMapping( Keys.V, UltraGridAction.Paste,    UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanPaste, SpecialKeys.Alt, SpecialKeys.Ctrl ),
				new GridKeyActionMapping( Keys.Y, UltraGridAction.Redo,		UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanRedo, SpecialKeys.Alt, SpecialKeys.Ctrl ),
				new GridKeyActionMapping( Keys.Z, UltraGridAction.Undo,		UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanUndo, SpecialKeys.Alt, SpecialKeys.Ctrl ),

				new GridKeyActionMapping( Keys.Insert, UltraGridAction.Copy,		UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanCopy, SpecialKeys.AltShift, SpecialKeys.Ctrl ),
				// MRS 1/16/06 - BR08712
				//new GridKeyActionMapping( Keys.Insert, UltraGridAction.Paste,		UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanCut, SpecialKeys.AltCtrl, SpecialKeys.Shift )
				new GridKeyActionMapping( Keys.Insert, UltraGridAction.Paste,		UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanPaste, SpecialKeys.AltCtrl, SpecialKeys.Shift ),
				new GridKeyActionMapping( Keys.Delete, UltraGridAction.Cut,		UltraGridState.SwapDroppedDown | UltraGridState.FilterDroppedDown | UltraGridState.InEdit,		UltraGridState.CanCut, SpecialKeys.AltCtrl, SpecialKeys.Shift )
				// ------------------------------------------------------------------------------
            };

            // loop over the default mappings array and add each one to the
            // collection
            //
            for( int i = 0; i < defaultMappings.Length; i++ )
            {
                this.Add( defaultMappings[i] );
            }

        }

        
		/// <summary>
		/// indexer 
		/// </summary>
        public Infragistics.Win.UltraWinGrid.GridKeyActionMapping this[ int index ] 
        {
            get
            {
 				// JJD 8/15/01 - uWG153
				// verify that the mappings are loaded
				//
				this.VerifyMappingsAreLoaded();
                
				return (GridKeyActionMapping)base.GetItem( index );
            }
            set
            {
 				// JJD 8/15/01 - uWG153
				// verify that the mappings are loaded
				//
				this.VerifyMappingsAreLoaded();

				base.List[index] = value;
            }
        }
     
        
		/// <summary>
		/// IEnumerable Interface Implementation		
		/// </summary>
        /// <returns>A type safe enumerator</returns>
        public GridKeyActionMappingEnumerator GetEnumerator() // non-IEnumerable version
        {
			// JJD 8/15/01 - uWG153
			// verify that the mappings are loaded
			//
			this.VerifyMappingsAreLoaded();
            
			return new GridKeyActionMappingEnumerator(this);
        }

		//DA 1.14.03 
		//IsActionAllowed has been moved down into ActionMappingsCollection
		//which is owned by KeyActionMappingsBase.
		//This allows ensures that regardless of how the end user
		//modifies the KeyActions, that the 'performable actions'
		//are not affected.

//		/// <summary>
//		/// Returns whether or not the specified action is allowed based on
//		///	CurrentState.
//		/// </summary>
//		/// <param name="actionCode"></param>
//		/// <param name="currentState"></param>
//		/// <returns></returns>
//		public bool IsActionAllowed( UltraGridAction actionCode, UltraGridState currentState )
//		{
// 			// JJD 8/15/01 - uWG153
//			// verify that the mappings are loaded
//			//
//			this.VerifyMappingsAreLoaded();
//
//			// loop over the mappings table looking to see if this action
//			// is allowed while we are in this state
//			//
//			for ( int i =0; i<this.Count; ++i )
//			{
//				
//				if ( this[i].ActionCode == actionCode )
//				{
//					// check for the required current state bits
//					//
//					if ( ( (int)currentState & (int)(this[i].StateRequired) ) != (int)this[i].StateRequired ) 
//						continue;
//
//					// check for the disallowed current state bits
//					//
//					if ( ( (int)currentState & (int)(this[i].StateDisallowed) ) != 0 ) 
//						continue;
//
//					return true;
//				}
//			}		
//
//			return false;
//		}
 
		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.GridKeyActionMapping[] array, int index)
		{
			this.VerifyMappingsAreLoaded();

			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo

		// Inner class implements IEnumerator interface
 
		/// <summary>
		///  Summary description for KeyActionMappingEnumerator
		/// </summary>
		public class GridKeyActionMappingEnumerator: DisposableObjectEnumeratorBase
		{   
			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="mappings"></param>
			
			public GridKeyActionMappingEnumerator(GridKeyActionMappings mappings)
				: base( mappings )
			{
			}
 
			/// <summary>
			/// non-IEnumerator version: type-safe
			/// </summary>
			public GridKeyActionMapping Current 
			{
				get
				{
					return (GridKeyActionMapping)((IEnumerator)this).Current;
				}
			}
		}

				
		#region ActionStateMappingsCollection Added 1.14.03

	
		
		#region CreateActionStateMappingsCollection
		/// <summary>
		/// Creates an instance of an ActionStateMappingsCollection derived class
		/// </summary>
		/// <returns></returns>
		protected override Infragistics.Win.KeyActionMappingsBase.ActionStateMappingsCollection CreateActionStateMappingsCollection()
		{
			return new ActionStateMappingsCollection(
			new ActionStateMapping[] {

										new ActionStateMapping(UltraGridAction.NextCell,           0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.NextCellByTab,      0,                              (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.PrevCell,           0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.PrevCellByTab,      0,                              (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.AboveCell,          0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.BelowCell,          0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.FirstRowInBand,     0,            (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.LastRowInBand,      0,            (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.FirstRowInGrid,     0,             (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.LastRowInGrid,      0,            (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.ExpandRow,           (long)UltraGridState.RowExpanded,   (long)UltraGridState.RowExpandable),
										new ActionStateMapping(UltraGridAction.CollapseRow,        0,            (long)UltraGridState.RowExpanded),
										new ActionStateMapping(UltraGridAction.NextRow,            0,  (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.NextRowByTab,       (long)UltraGridState.LastRowInGrid,   (long)UltraGridState.Row),
										// JAS 2005 v2 GroupBy Row Extensions - We should not require that the row be collapsed for the previous row to be activated.
										new ActionStateMapping(UltraGridAction.PrevRow,            0,    (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.PrevRowByTab,      (long)UltraGridState.FirstRowInGrid,  (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.AboveRow,           0,        (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.BelowRow,           0,            (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.ToggleCheckbox,     0,                              (long)UltraGridState.IsCheckbox | (long)UltraGridState.InEdit),
										new ActionStateMapping(UltraGridAction.ToggleCellSel,      (long)UltraGridState.InEdit,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.ToggleRowSel,       (long)UltraGridState.Cell,            (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.DeactivateCell,     0,                              (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.ActivateCell,       (long)UltraGridState.Cell,            (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.NextCellInBand,     0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.PrevCellInBand,     0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.FirstCellInRow,      (long)UltraGridState.CellFirst,       (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.LastCellInRow,       (long)UltraGridState.CellLast,        (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.FirstCellInBand,    0,          (long)UltraGridState.CellFirst),
										new ActionStateMapping(UltraGridAction.LastCellInBand,     0,          (long)UltraGridState.CellLast),
										new ActionStateMapping(UltraGridAction.FirstCellInGrid,    0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.LastCellInGrid,     0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.PageUpCell,         0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.PageDownCell,       0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.PageUpRow,          0,            (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.PageDownRow,        0,            (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.UndoCell,           (long)UltraGridState.IsDroppedDown,   (long)UltraGridState.InEdit),
										new ActionStateMapping(UltraGridAction.UndoRow,            (long)UltraGridState.InEdit,          (long)UltraGridState.Row | (long)UltraGridState.RowDirty),
										new ActionStateMapping(UltraGridAction.ExpandRow,          (long)UltraGridState.IsDroppedDown | (long)UltraGridState.RowExpanded,     (long)UltraGridState.GroupByRow),
										new ActionStateMapping(UltraGridAction.CollapseRow,        (long)UltraGridState.IsDroppedDown,   (long)UltraGridState.GroupByRow | (long)UltraGridState.RowExpanded),
										new ActionStateMapping(UltraGridAction.ToggleDropdown,     0,                              (long)UltraGridState.InEdit | (long)UltraGridState.HasDropdown),
										new ActionStateMapping(UltraGridAction.ToggleEditMode,     (long)UltraGridState.SwapDroppedDown, (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.EnterEditModeAndDropdown, (long)UltraGridState.InEdit,    (long)UltraGridState.HasDropdown),
										new ActionStateMapping(UltraGridAction.NextRegion,         0,                              0),
										new ActionStateMapping(UltraGridAction.PrevRegion,         0,                              0),
										new ActionStateMapping(UltraGridAction.DeleteRows,         0,          (long)UltraGridState.Row),
										new ActionStateMapping(UltraGridAction.EnterEditMode,      0,          (long)UltraGridState.Cell),
										new ActionStateMapping(UltraGridAction.ExitEditMode,       0,                              (long)UltraGridState.InEdit),
										// SSP 4/23/05 - NAS 5.2 Filter Row
										// Added FilterRow state. This is for applying filters when Enter key is 
										// pressed while the filter row is active depending on the 
										// FilterEvaluationTrigger settings.
										//
										new ActionStateMapping( UltraGridAction.CommitRow,     (long)UltraGridState.IsDroppedDown,     (long)UltraGridState.RowDirty ),
										// SSP 11/22/05 - NAS 6.1 Multi-cell Operations
										//									
										// ------------------------------------------------------------------------------
										new ActionStateMapping( UltraGridAction.Copy,		0,		(long)UltraGridState.CanCopy ),
										new ActionStateMapping( UltraGridAction.Cut,		0,		(long)UltraGridState.CanCut ),
										new ActionStateMapping( UltraGridAction.Paste,		0,		(long)UltraGridState.CanPaste ),
										new ActionStateMapping( UltraGridAction.Redo,		0,		(long)UltraGridState.CanRedo ),
										new ActionStateMapping( UltraGridAction.Undo,		0,		(long)UltraGridState.CanUndo ),
										new ActionStateMapping( UltraGridAction.DeleteCells, 0,		(long)UltraGridState.CanDeleteCells ),
										// ------------------------------------------------------------------------------
									 }
			);
		}
		#endregion

		#endregion

	}
}
