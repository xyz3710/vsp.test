#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved



namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Runtime.InteropServices;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Windows.Forms.Design;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Drawing.Printing;
	using Infragistics.Win.UltraWinMaskedEdit;

	/// <summary>
	/// The UltraDropDown control provies a multi-column, grid-like dropdown list in a column of an <see cref="UltraGrid"/> control.
	/// </summary>
	/// <remarks>
	/// <p class="body">To attach an UltraDropDown to a grid, use the <see cref="UltraGridColumn.ValueList"/> property of the <see cref="UltraGridColumn"/>, or the <see cref="UltraGridCell.ValueList"/> property of an <see cref="UltraGridCell"/>.</p>
	/// <p class="body">The UltraDropDown is populated by binding it to a data source, just like the <see cref="UltraGrid"/>. So use the <see cref="UltraGridBase.SetDataBinding(object, string, bool, bool)"/> method, or the <see cref="UltraGridBase.DataSource"/> and <see cref="UltraGridBase.DataMember"/> properties.</p>
	/// <p class="body">Be sure to set the <see cref="UltraDropDownBase.ValueMember"/> property to determine which column in the dropdown is associated with the grid cell.</p>
	/// <p class="body">Optionally, you may also want to set the <see cref="UltraDropDownBase.DisplayMember"/> property to display more user-friendly text, instead of displaying the value.</p>	
	/// <p class="note">Note that the UltraDropDown control requires a container (such as a Form or UserControl) in order to function properly. Placing the control on a form (or UserControl) at design-time is sufficient for this, but if the control is created at run-time in code, be sure to add the UltraDropDown to the form's (or UserControl's) Controls collection.</p>
	/// </remarks>
	
	// AS 4/8/05
	//
	
	[ToolboxBitmap(typeof(UltraGridBase), AssemblyVersion.ToolBoxBitmapFolder + "UltraDropDown.bmp")] // MRS 11/13/05
	[ DefaultProperty( "DisplayLayout" ) ]
	[ Designer( "Infragistics.Win.UltraWinGrid.Design.UltraGridDesigner, " + AssemblyRef.Design ) ]
	[ DefaultEvent( "InitializeLayout" ) ]
	[ LocalizedDescription("LD_UltraDropDown") ] // JAS BR06191 10/10/05
	[ComplexBindingProperties("DataSource", "DataMember")]	// AS 3/28/05 Drag-Once Binding
	public class UltraDropDown : UltraDropDownBase,
								 Infragistics.Shared.IUltraLicensedComponent,
                                 // BF 4/21/08  NA 2008.2 - WinValidator
                                 IValidatorClient
	{

		#region Private Fields	

	    private UltraLicense				license = null;

		private DropDownEventManager		eventManager = null;

		// MRS 10/19/05 - BR06916
		// Moved this up to UltraDropDownBase, since the workaround
		// is good for UltraCombo, too. 
		#region bindingContext
//		// SSP 8/25/05 BR05793
//		// If someone creates an UltraDropDown in code without adding it to Controls collection
//		// of a form or container, then the BindingContext will be null and the UDD won't be 
//		// able to bind. Worse yet, if such an udd is used as ValueList of a column then the
//		// the first time it's dropped down it won't have any data however the next time it's
//		// dropped down it will because the udd gets added to the drop down form, causing it
//		// to have a BindingContext. Also when the udd is closed up after dropping down, its
//		// BindingContext will become null. This is to fix that.
//		// 
//		private BindingContext bindingContext = null;
		#endregion bindingContext

		#endregion


		#region Static Event Objects

		private static readonly object EVENT_INTIALIZELAYOUT = new object();
		private static readonly object EVENT_INTIALIZEROW = new object();
		private static readonly object EVENT_AFTERDROPDOWN = new object();
		private static readonly object EVENT_AFTERCLOSEUP = new object();
		private static readonly object EVENT_BEFOREDROPDOWN = new object();
		private static readonly object EVENT_ROWSELECTED = new object();

		#endregion

		#region Events	
		

		/// <summary>
		/// Occurs when the display layout is initialized, such as when the control is loading data from the data source.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <see cref="InitializeLayoutEventArgs.Layout"/> argument returns a reference to a <see cref="UltraGridBase.DisplayLayout"/> object that can be used to set properties of, and invoke methods on, the layout of the control. You can use this reference to access any of the returned layout's properties or methods.</p>
		///	<p class="body">Like a form's <b>Load</b> event, this event provides an opportunity to configure the control before it is displayed. It is in this event procedure that actions such as creating appearances, valuelists, and unbound columns should take place.</p>
		///	<p class="body">This event is generated when the control is first preparing to display data from the data source. This may occur when the data source changes.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraDropDown_E_InitializeLayout")]	
		public event InitializeLayoutEventHandler   InitializeLayout
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZELAYOUT, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZELAYOUT, value ); }
		}

		/// <summary>
		/// Occurs when a row is initialized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <see cref="InitializeRowEventArgs.Row"/> argument returns a reference to an <see cref="UltraGridRow"/> object that can be used to set properties of, and invoke methods on, the row being displayed. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <see cref="InitializeRowEventArgs.ReInitialize"/> argument can be used to determine if the row is being initialized for the first time, such as when the <see cref="UltraDropDown"/> is initially loading data, or whether it is being reinitialized, such as when the <see cref="RowsCollection.Refresh(RefreshRow)"/> method is called.</p>
		///	<p class="body">This event is generated once for each row being displayed or printed and provides an opportunity to perform actions on the row before it is rendered, such as populating an unbound cell or changing a cell's color based on its value.</p>
		///	<p class="body">The <see cref="UltraGridLayout.ViewStyle"/> and <see cref="UltraGridLayout.ViewStyleBand"/> properties of the <see cref="UltraGridBase.DisplayLayout"/> object are read-only in this event procedure.</p>		
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeLayout"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraDropDown_E_InitializeRow")]	
		public event InitializeRowEventHandler      InitializeRow
		{
			add { this.EventsOptimized.AddHandler( EVENT_INTIALIZEROW, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INTIALIZEROW, value ); }
		}

		/// <summary>
		/// Occurs after the list is dropped down.
		/// </summary>
		/// <remarks>
		/// <p class="body">This event is for internal infrastructure use.</p>
		/// <p class="body">To trap when a an <see cref="UltraDropDown"/> in a grid has dropped down, use the <see cref="UltraGrid.BeforeCellListDropDown"/> event of the <see cref="UltraGrid"/>.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraDropDown_E_AfterDropDown")]	
		public event DropDownEventHandler				AfterDropDown
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERDROPDOWN, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERDROPDOWN, value ); }
		}

		/// <summary>
		/// Occurs after the list is closed up.
		/// </summary>
		/// <remarks>		
		/// <p class="body">This event should not be used. To trap when a an <see cref="UltraDropDown"/> in a grid has closed, use the <see cref="UltraGrid.AfterCellListCloseUp"/> event of the <see cref="UltraGrid"/>.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraDropDown_E_AfterCloseUp")]	
		public event DropDownEventHandler				AfterCloseUp
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCLOSEUP, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCLOSEUP, value ); }
		}

		/// <summary>
		/// Occurs before the list is dropped down.
		/// </summary>
		/// <remarks>		
		/// <p class="body">This event should not be used. To trap when a an <see cref="UltraDropDown"/> in a grid has dropped down, use the <see cref="UltraGrid.BeforeCellListDropDown"/> event of the <see cref="UltraGrid"/>.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraDropDown_E_BeforeDropDown")]	
		public event CancelEventHandler			BeforeDropDown
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREDROPDOWN, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREDROPDOWN, value ); }
		}

		/// <summary>
		/// Occurs when a new row of the dropdown is selected.
		/// </summary>		
		/// <remarks>		
		/// <p class="body">This event should not be used. To trap when a a new row in the <see cref="UltraDropDown"/> in a grid is selected, use the <see cref="UltraGrid.CellListSelect"/> event of the <see cref="UltraGrid"/>.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraDropDown_E_RowSelected")]	
		public event RowSelectedEventHandler				RowSelected
		{
			add { this.EventsOptimized.AddHandler( EVENT_ROWSELECTED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_ROWSELECTED, value ); }
		}

        // CDS 02/02/08 TFS12512 - NAS v9.1 Header CheckBox - The events were moved to the UltraGridBase class, so we want to hide them here where they are not supported.
        /// <summary>
        /// Occurs before the CheckState of the Header CheckBox is changed
        /// </summary>
        [Obsolete("This event is not  supported for the UltraDropDown.", false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
#pragma warning disable 0809
#pragma warning disable 0067        
        public new event BeforeHeaderCheckStateChangedEventHandler BeforeHeaderCheckStateChanged;

        // CDS 02/02/08 TFS12512 - NAS v9.1 Header CheckBox - The events were moved to the UltraGridBase class, so we want to hide them here where they are not supported.
        /// <summary>
        /// Occurs after the CheckState of the Header CheckBox is changed
        /// </summary>
        [Obsolete("This event is not  supported for the UltraDropDown.", false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public new event AfterHeaderCheckStateChangedEventHandler AfterHeaderCheckStateChanged;
#pragma warning restore 0809
#pragma warning restore 0067
        #endregion

        /// <summary>
		/// Initializes a new instance of an <see cref="Infragistics.Win.UltraWinGrid.UltraDropDown"/> control.
		/// </summary>
		public UltraDropDown()
		{
			// verify and cache the license
			//
			// AS 3/5/03 DNF37
			// Wrapped in a try/catch for a FileNotFoundException.
			// When the assembly is loaded dynamically, VS seems 
			// to be trying to reload a copy of Shared even though 
			// one is in memory. This generates a FileNotFoundException
			// when the dll is not in the gac and not in the AppBase
			// for the AppDomain.
			//
			try
			{
				this.license = LicenseManager.Validate( typeof(UltraDropDown), this ) as UltraLicense;
			}
			catch (System.IO.FileNotFoundException)	{}
			
			if ( !this.DesignMode )
			{
				this.Visible = false;

				// SSP 10/2/02
				// Set the selectable to false so that the control doesn't take focus.
				//
				this.SetStyle( ControlStyles.Selectable, false );
			}
		}

		/// <summary>
        /// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{

			if ( disposing )
			{
				if ( this.license != null ) 
				{
					this.license.Dispose();
					this.license = null;
				}
			}

			base.Dispose( disposing );
		}

		#region About Dialog and Licensing Interface

		/// <summary>
		/// Displays the about dialog
		/// </summary>
		/// <remarks>
		/// <p class="body">The About property is a design-time only property which exists to provide a way to show the About dialog.. The property has no real value, but it will display in the property grid in Visual Studio and show an ellipsis button which will show the About dialog when clicked.</p>
		/// </remarks>
		[ DesignOnly( true ) ]
        [ LocalizedDescription("LD_UltraDropDown_P_About") ]
        [ LocalizedCategory("LC_Design") ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ ParenthesizePropertyName( true ) ]
		[ Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor) ) ]
		public object About { get { return null; } }

		/// <summary>
		/// Return the license we cached inside the constructor
		/// </summary>
		UltraLicense IUltraLicensedComponent.License { get { return this.license; } }
	
		#endregion

		// MRS 10/19/05 - BR06916
		// Moved this up to UltraDropDownBase, since the workaround
		// is good for UltraCombo, too. 
		#region BindingContext

//		// SSP 8/25/05 BR05793
//		// If someone creates an UltraDropDown in code without adding it to Controls collection
//		// of a form or container, then the BindingContext will be null and the UDD won't be 
//		// able to bind. Worse yet, if such an udd is used as ValueList of a column then the
//		// the first time it's dropped down it won't have any data however the next time it's
//		// dropped down it will because the udd gets added to the drop down form, causing it
//		// to have a BindingContext. Also when the udd is closed up after dropping down, its
//		// BindingContext will become null. This is to fix that.
//		// 
//		/// <summary>
//		/// Overridden. Gets or sets the binding context that should be used for data binding
//		/// purposes. This property must be set before data binding.
//		/// </summary>
//		public override BindingContext BindingContext
//		{
//			get
//			{
//				BindingContext val = this.bindingContext;
//
//				if ( null == val )
//				{
//					val = base.BindingContext;
//
//					if ( null == val && this.inSet_ListManager )
//						val = this.bindingContext = new BindingContext( );
//				}
//
//				return val;
//			}
//			set
//			{
//				this.bindingContext = null;
//				base.BindingContext = value;
//			}
//		}

		#endregion // BindingContext
		
		/// <summary>
		/// Called when grid itself resizes.
		/// </summary>
        /// <param name="eventArgs">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnResize(System.EventArgs eventArgs )
		{
			//call the base
			base.OnResize(eventArgs);

			// JJD 12/04/01
			// Only set the rect and invalidate if the control has
			// been created. Otherwise, ignore this message. 
			// This fixed a problem at design time where scrollbars
			// were being created too early in the process which
			// was preventing thumb tracking from working.
			//
			if ( this.Created )
			{
				this.DisplayLayout.DirtyGridElement();
				this.DisplayLayout.ColScrollRegions.DirtyMetrics();
				this.DisplayLayout.RowScrollRegions.DirtyAllVisibleRows();		
			}
		}


		/// <summary>
		/// Resets the first draw flag
		/// </summary>
		/// <param name="pe"></param>
		protected override void OnPaint(PaintEventArgs pe)
		{
			// RobA UWG690 11/15/01
			// Needed to override this method so that we can set the first draw to 
			// false after the first draw.
			//			
			if ( this.FirstDraw )
			{
				this.ResetFirstDrawFlag();
				this.DisplayLayout.GetColScrollRegions( true ).DirtyMetrics();
			}

			base.OnPaint(pe);
		}
		


		

		/// <summary>
		/// Associated Control UIElement object
		/// </summary>
		protected override ControlUIElementBase ControlUIElement 
		{ 
			get
			{
				// SSP 5/29/02 UWG1144
				// Check for DisplayLayout being null because it would be null
				// if we are in the middle of being disposed of.
				//
				if ( null != this.DisplayLayout )
					return this.DisplayLayout.UIElement; 

				return null;
			}
		}


		/// <summary>
		/// Returns the current state.
		/// </summary>
		/// <returns>The current state.</returns>
        internal protected override Int64 GetCurrentState()
		{
			return (Int64)0;
		}

		/// <summary>
		/// Return the KeyActionMappings collection.
		/// </summary>
		internal protected override KeyActionMappingsBase GetKeyActionMappings()
		{
			return null;
		}

		/// <summary>
		/// Internal property to determine if we are in the InitializeLayout event. 
		/// </summary>
		internal protected override bool InInitializeLayout 
		{ 
			get
			{
				return this.EventManager.InProgress(Infragistics.Win.UltraWinGrid.DropDownEventIds.InitializeLayout); 
			}
		}

		/// <summary>
		/// Returns true if the event is in progress
		/// </summary>
        /// <param name="eventId">Indicates the event.</param>
        /// <returns>true if the event is in progress.</returns>
		internal protected override bool IsEventInProgress( Enum eventId )
		{
			DropDownEventIds id = (Infragistics.Win.UltraWinGrid.DropDownEventIds) eventId;

			return this.EventManager.InProgress( id );
		}


		/// <summary>
		/// Called by UltraGridBase when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnBasePropChanged( PropChangeInfo propChange ) 
		{
			try 
			{
				if ( propChange.Source == this.DisplayLayout )
				{
					if ( null != propChange.FindPropId( AppearancePropIds.FontData ) )
					{
						// Clear cached metrics and dirty grid if font changes
						//				
						this.DisplayLayout.ClearCachedCaptionHeight();

						this.DisplayLayout.OnBaseFontChanged();

						this.DisplayLayout.DirtyGridElement();
						return;
					}

					switch ( (PropertyIds)propChange.PropId )
					{

						case PropertyIds.Scrollbars:
						{
							//old code = m_pLayout->DirtyGridElement();
							//this code was already here
							this.DisplayLayout.UIElement.DirtyChildElements();
							break;
						}
						case PropertyIds.ColScrollRegions:
						case PropertyIds.RowScrollRegions:
						{
							if ( null != propChange.FindPropId( PropertyIds.Hidden ) ||
								null != propChange.FindPropId( PropertyIds.Scrollbar  ) ||
								null != propChange.FindPropId( PropertyIds.Remove  ) )
								this.DisplayLayout.DirtyGridElement();
							else
								this.Invalidate();
							break;
						}
						
						// Invalidate the grid when an appearance property is set
						//
						case PropertyIds.Appearance:
						case PropertyIds.CaptionAppearance:
						{
							bool dirtyGridElement = true;
							
							//ROBA UWG116 8/17/01 
							//assume that the fontdata has changed
							//
							if ( propChange.Trigger == null )
							{
								// Clear cached metrics and dirty grid if font changes
								//				
								this.DisplayLayout.ClearCachedCaptionHeight();

								this.DisplayLayout.OnBaseFontChanged();
							}

							if ( propChange.FindPropId( AppearancePropIds.AlphaLevel ) != null )
								dirtyGridElement = false;
							if ( dirtyGridElement )
								this.DisplayLayout.DirtyGridElement( false );
							else
								this.Invalidate();

							break;
						}

						case PropertyIds.AlphaBlendEnabled:
						case PropertyIds.RowConnectorStyle:
						case PropertyIds.RowConnectorColor:
						{
							bool dirtyGridElement = true;
							
							if ( propChange.FindPropId( AppearancePropIds.AlphaLevel ) != null )
								dirtyGridElement = false;
							if ( dirtyGridElement )
								this.DisplayLayout.DirtyGridElement( false );
							else
								this.Invalidate();

							break;
						}

						case PropertyIds.Caption:
						{
							this.DisplayLayout.DirtyGridElement();
							break;
						}
						
						case PropertyIds.BorderStyle:
							this.DisplayLayout.DirtyGridElement();
							break;
					}
				}
			}
			finally
			{
				// JJD 11/06/01
				// Call the base implementation which will fire
				// the PropertyChanged notification
				//  
				base.OnBasePropChanged( propChange );
			}
		}


        /// <summary>
        /// Performs a specific key action
        /// </summary>
        ///	<param name="actionCode">An enumeration value that determines the user action to be performed.</param>
        ///	<param name="ctlKeyDown">A boolean specifies whether the action should be performed as if the control key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is maintained, as it is when the user holds down the control key and selects a row in a grid.</param>
        ///	<param name="shiftKeyDown">A boolean specifies whether the action should be performed as if the shift key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is extended, as it is when the user holds down the shift key and selects a range of rows in a grid.</param>                
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
        internal protected override bool PerformKeyAction(Enum actionCode, bool shiftKeyDown, bool ctlKeyDown)
		{
			return false;
		}

		/// <summary>
		/// The object that enables, disables and controls firing of specific control events.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>EventManager</b> gives you a high degree of control over how the component invokes event procedures. You can use it to selectively enable and disable event procedures depending on the context of your application. You can also use the event manager to return information about the state of the component's events.</p>
		/// <p class="body">The event manager's methods are used to determine the enabled state of an event (<see cref="Infragistics.Win.UltraWinGrid.DropDownEventManager.IsEnabled(DropDownEventIds)"/>), to selectively enable or disable events (<see cref="Infragistics.Win.UltraWinGrid.DropDownEventManager.SetEnabled(DropDownEventIds,bool)"/>), and to tell whether an event procedure is currently being processed (<see cref="Infragistics.Win.UltraWinGrid.DropDownEventManager.InProgress"/>). There is also an <seealso cref="Infragistics.Shared.EventManagerBase.AllEventsEnabled"/> property that you can check to quickly determine whether any events have been disabled by the event manager.</p>
		/// </remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.UltraWinGrid.DropDownEventManager EventManager
		{
			get
			{
				if ( null == this.eventManager )
				{
					this.eventManager = new Infragistics.Win.UltraWinGrid.DropDownEventManager( this );
				}

				return this.eventManager;
			}
		}

		#region Event Methods
		
		internal void FireEvent( DropDownEventIds id, EventArgs e )
		{
            
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( id ) )
				return;

			if ( !Enum.IsDefined( typeof( DropDownEventIds ), id ) )
			{
				Debug.Fail( "Invalid event id in UltraDropDown.FireEvent, id: " + ((int)id).ToString() );
				return;
			}

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( id );

			try
			{
				switch ( id )
				{
					case DropDownEventIds.BeforeDropDown:
						this.OnBeforeDropDown( e as CancelableDropDownEventArgs );
						break;

					case DropDownEventIds.AfterDropDown:
						this.OnAfterDropDown( e as DropDownEventArgs );
						break;

					case DropDownEventIds.AfterCloseUp:
						this.OnAfterCloseUp( e as DropDownEventArgs  );
						break;

					case DropDownEventIds.RowSelected:
						this.OnRowSelected( e as RowSelectedEventArgs  );
						break;

						// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.FilterRow:
						this.OnFilterRow( e as FilterRowEventArgs );
						break;
					
						// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.AfterSortChange:
						this.OnAfterSortChange( e as BandEventArgs );
						break;

						// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.BeforeSortChange:
						this.OnBeforeSortChange( e as BeforeSortChangeEventArgs );
						break;

						// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.AfterColPosChanged:
						this.OnAfterColPosChanged( e as AfterColPosChangedEventArgs);
						break;

						// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.BeforeColPosChanged:
						this.OnBeforeColPosChanged( e as BeforeColPosChangedEventArgs );
						break;

						// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.BeforeRowFilterDropDown:
						this.OnBeforeRowFilterDropDown( e as BeforeRowFilterDropDownEventArgs );
						break;

						// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.BeforeRowFilterChanged:
						this.OnBeforeRowFilterChanged( e as BeforeRowFilterChangedEventArgs );
						break;

						// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.AfterRowFilterChanged:
						this.OnAfterRowFilterChanged( e as AfterRowFilterChangedEventArgs );
						break;

						// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.BeforeCustomRowFilterDialog:
						this.OnBeforeCustomRowFilterDialog( e as BeforeCustomRowFilterDialogEventArgs );
						break;

						// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
					case DropDownEventIds.BeforeRowFilterDropDownPopulate:
						this.OnBeforeRowFilterDropDownPopulate( e as BeforeRowFilterDropDownPopulateEventArgs );
						break;
					// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					//
					// --------------------------------------------------------------------------------
					case DropDownEventIds.FilterCellValueChanged:
						this.OnFilterCellValueChanged( e as FilterCellValueChangedEventArgs );
						break;
					case DropDownEventIds.InitializeRowsCollection:
						this.OnInitializeRowsCollection( e as InitializeRowsCollectionEventArgs );
						break;
					default:
						Debug.Assert(false, "Unhandled Id passed into UltraDropDown.FireEvent");
						break;
					// --------------------------------------------------------------------------------
					// SSP 6/17/05 - NAS 5.3 Column Chooser
					//
					case DropDownEventIds.BeforeColumnChooserDisplayed:
						this.OnBeforeColumnChooserDisplayed( e as BeforeColumnChooserDisplayedEventArgs );
						break;
						// SSP 10/18/05 - NAS 5.3 Column Chooser
						// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
						// TestAdvantage.
						// 
						// ----------------------------------------------------------------------------
					case DropDownEventIds.BeforeBandHiddenChanged:
						this.OnBeforeBandHiddenChanged( e as BeforeBandHiddenChangedEventArgs );
						break;
					case DropDownEventIds.AfterBandHiddenChanged:
						this.OnAfterBandHiddenChanged( e as AfterBandHiddenChangedEventArgs );
						break;
						// ----------------------------------------------------------------------------
				}
			}
			finally
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( id );
			}
		}

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		#region FireCommonEvent

		internal override bool FireCommonEvent( CommonEventIds id, EventArgs e, bool checkInProgress )
		{
			DropDownEventIds resolvedEventID;
			
			switch (id)
			{
				case CommonEventIds.AfterSortChange:
					resolvedEventID = DropDownEventIds.AfterSortChange;
					break;
				case CommonEventIds.BeforeSortChange:
					resolvedEventID = DropDownEventIds.BeforeSortChange;
					break;
				case CommonEventIds.FilterRow:
					resolvedEventID = DropDownEventIds.FilterRow;
					break;
				case CommonEventIds.BeforeColPosChanged:
					resolvedEventID = DropDownEventIds.BeforeColPosChanged;
					break;
				case CommonEventIds.AfterColPosChanged:
					resolvedEventID = DropDownEventIds.AfterColPosChanged;
					break;
				case CommonEventIds.BeforeRowFilterDropDown:
					resolvedEventID = DropDownEventIds.BeforeRowFilterDropDown;
					break;
				case CommonEventIds.AfterRowFilterChanged:
					resolvedEventID = DropDownEventIds.AfterRowFilterChanged;
					break;
				case CommonEventIds.BeforeRowFilterChanged:
					resolvedEventID = DropDownEventIds.BeforeRowFilterChanged;
					break;
				case CommonEventIds.BeforeCustomRowFilterDialog:
					resolvedEventID = DropDownEventIds.BeforeCustomRowFilterDialog;
					break;
				case CommonEventIds.BeforeRowFilterDropDownPopulate:
					resolvedEventID = DropDownEventIds.BeforeRowFilterDropDownPopulate;
					break;
				// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// Added FilterCellValueChanged and InitializeRowsCollection events.
				//
				// ----------------------------------------------------------------------------
				case CommonEventIds.FilterCellValueChanged:
					resolvedEventID = DropDownEventIds.FilterCellValueChanged;
					break;
				case CommonEventIds.InitializeRowsCollection:
					resolvedEventID = DropDownEventIds.InitializeRowsCollection;
					break;
				// ----------------------------------------------------------------------------
				// SSP 6/17/05 - NAS 5.3 Column Chooser
				//
				case CommonEventIds.BeforeColumnChooserDisplayed:
					resolvedEventID = DropDownEventIds.BeforeColumnChooserDisplayed;
					break;
					// SSP 10/18/05 - NAS 5.3 Column Chooser
					// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
					// TestAdvantage.
					// 
					// ----------------------------------------------------------------------------
				case CommonEventIds.BeforeBandHiddenChanged:
					resolvedEventID = DropDownEventIds.BeforeBandHiddenChanged;
					break;
				case CommonEventIds.AfterBandHiddenChanged:
					resolvedEventID = DropDownEventIds.AfterBandHiddenChanged;
					break;
					// ----------------------------------------------------------------------------
				default:
					Debug.Assert(false, "Unhandled Id passed into UltraGrid.FireCommonEvent");
					return false;			
			}

			if (checkInProgress && 
				this.EventManager.InProgress(resolvedEventID))
			{
				return false;
			}

			this.FireEvent(resolvedEventID, e);
			return true;
		}
		#endregion FireCommonEvent

		/// <summary>
		/// Called when the layout is first initialized after the 
		/// datasource has been set 
		/// </summary>
		internal protected override void FireInitializeLayout( InitializeLayoutEventArgs e )
		{
         
			// JJD 12/03/01 - UWG813
			// Call base to maintain flag so we know if event was fired
			//
			base.FireInitializeLayout( e );

			InitializeLayoutEventHandler eDelegate = (InitializeLayoutEventHandler)EventsOptimized[EVENT_INTIALIZELAYOUT];

			if ( eDelegate != null )
			{				           
				// if the event can not be fired then just exit
				//
				if ( !this.EventManager.CanFireEvent( DropDownEventIds.InitializeLayout ) )
					return;

				// increment the inProgress count
				//
				this.EventManager.IncrementInProgress( DropDownEventIds.InitializeLayout );

				try
				{
					// fire the event
					//
					eDelegate( this, e );
				}
				finally
				{
					// In the case that an error was thrown we still need to
					// decrement the inProgress count
					//
					this.EventManager.DecrementInProgress( DropDownEventIds.InitializeLayout );
				}
			}
		}

		/// <summary>
		/// Called after list is closed up
		/// </summary>
		protected override void FireAfterCloseUp( )
		{
			// SSP 6/17/02 UWG1242
			// Use the right delegate type. It's DropDownEventHandler not
			// EventHandler. This results in an exception.
			//
			//EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERCLOSEUP];
			DropDownEventHandler eDelegate = (DropDownEventHandler)EventsOptimized[EVENT_AFTERCLOSEUP];

			if ( eDelegate != null )
				this.FireEvent(	DropDownEventIds.AfterCloseUp, new DropDownEventArgs( this.Owner != null ? this.Owner.Control : null ) );
		}

		/// <summary>
		/// Called after list is dropped down
		/// </summary>
		protected override void FireAfterDropDown( )
		{
			// SSP 6/17/02 UWG1242
			// Use the right delegate type. It's DropDownEventHandler not
			// EventHandler. This results in an exception.
			//
			//EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERDROPDOWN];
			DropDownEventHandler eDelegate = (DropDownEventHandler)EventsOptimized[EVENT_AFTERDROPDOWN];

			if ( eDelegate != null )
				this.FireEvent(	DropDownEventIds.AfterDropDown, new DropDownEventArgs( this.Owner != null ? this.Owner.Control : null  ) );
		}

		/// <summary>
		/// Called after list is dropped down
		/// </summary>
		/// <returns>True if cancelled.</returns>
		protected override bool FireBeforeDropDown( )
		{
			CancelEventHandler eDelegate = (CancelEventHandler)EventsOptimized[EVENT_BEFOREDROPDOWN];

			if ( eDelegate != null )
			{
				CancelableDropDownEventArgs e = new CancelableDropDownEventArgs( this.Owner != null ? this.Owner.Control : null );
				
				// fire the event
				//
				this.FireEvent(	DropDownEventIds.BeforeDropDown, e );

				return e.Cancel;
			}

			return false;
		}

		/// <summary>
		/// Called when a new row has be selected
		/// </summary>
		protected override void FireRowSelected( RowSelectedEventArgs e )
		{
			RowSelectedEventHandler eDelegate = (RowSelectedEventHandler)EventsOptimized[EVENT_ROWSELECTED];

			if ( eDelegate != null )
				this.FireEvent( DropDownEventIds.RowSelected, e );
		}


		/// <summary>
		/// Called after the list has closed
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterDropDown( DropDownEventArgs e )
		{
			// SSP 6/17/02 UWG1242
			// Use the right delegate type. It's DropDownEventHandler not
			// EventHandler. This results in an exception.
			//
			//EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERDROPDOWN];
			DropDownEventHandler eDelegate = (DropDownEventHandler)EventsOptimized[EVENT_AFTERDROPDOWN];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		/// <summary>
		/// Called after the list has closed
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterCloseUp( DropDownEventArgs e )
		{
			// SSP 6/17/02 UWG1242
			// Use the right delegate type. It's DropDownEventHandler not
			// EventHandler. This results in an exception.
			//
			//EventHandler eDelegate = (EventHandler)EventsOptimized[EVENT_AFTERCLOSEUP];
			DropDownEventHandler eDelegate = (DropDownEventHandler)EventsOptimized[EVENT_AFTERCLOSEUP];

			if ( eDelegate != null )
				eDelegate( this, e );
		}


		/// <summary>
		/// Called before the list is dropped down
		/// </summary>
		protected virtual void OnBeforeDropDown( CancelableDropDownEventArgs e )
		{
			CancelEventHandler eDelegate = (CancelEventHandler)EventsOptimized[EVENT_BEFOREDROPDOWN];

			if ( eDelegate != null )
				eDelegate( this, e );
		}


		/// <summary>
		/// Called when a new row has be selected
		/// </summary>
		protected virtual void OnRowSelected( RowSelectedEventArgs e )
		{
			RowSelectedEventHandler eDelegate = (RowSelectedEventHandler)EventsOptimized[EVENT_ROWSELECTED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}



		/// <summary>
		/// Called when an element is entered (the mouse is moved
		/// over the element)
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnMouseEnterElement( UIElementEventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( DropDownEventIds.MouseEnterElement ) )
				return;

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( DropDownEventIds.MouseEnterElement );

			try
			{
				// fire the event
				//
				base.OnMouseEnterElement( e );
			}
			catch(Exception)
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( DropDownEventIds.MouseEnterElement );

				// re-throw the error
				//
				throw;
			}

			// decrement the inProgress count
			//
			this.EventManager.DecrementInProgress( DropDownEventIds.MouseEnterElement );
		}

		/// <summary>
		/// Called when an element is left (the mouse is moved
		/// off the element)
		/// </summary>
		protected override void OnMouseLeaveElement( UIElementEventArgs e )
		{
			// if the event can not be fired then just exit
			//
			if ( !this.EventManager.CanFireEvent( DropDownEventIds.MouseLeaveElement ) )
				return;

			// increment the inProgress count
			//
			this.EventManager.IncrementInProgress( DropDownEventIds.MouseLeaveElement );

			try
			{
				// fire the event
				//
				base.OnMouseLeaveElement( e );
			}
			catch(Exception)
			{
				// In the case that an error was thrown we still need to
				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( DropDownEventIds.MouseLeaveElement );

				// re-throw the error
				//
				throw;
			}

			// decrement the inProgress count
			//
			this.EventManager.DecrementInProgress( DropDownEventIds.MouseLeaveElement );
		}

		/// <summary>
		/// Called when a row is initialized
		/// </summary>
		internal protected override void FireInitializeRow( InitializeRowEventArgs e )
		{
			InitializeRowEventHandler eDelegate = (InitializeRowEventHandler)EventsOptimized[EVENT_INTIALIZEROW];

			if ( eDelegate != null )
			{
            
				// if the event can not be fired then just exit
				//
				if ( !this.EventManager.CanFireEvent( DropDownEventIds.InitializeRow ) )
					return;

				// increment the inProgress count
				//
				this.EventManager.IncrementInProgress( DropDownEventIds.InitializeRow );

				try
				{
					// fire the event
					//
					eDelegate( this, e );
				}
				catch(Exception)
				{
					// In the case that an error was thrown we still need to
					// decrement the inProgress count
					//
					this.EventManager.DecrementInProgress( DropDownEventIds.InitializeRow );

					// re-throw the error
					//
					throw;
				}

				// decrement the inProgress count
				//
				this.EventManager.DecrementInProgress( DropDownEventIds.InitializeRow );
			}
		}


        /// <summary>
        /// Called when the active row changes
        /// </summary>
        /// <param name="newActiveRow">The new active row.</param>
        /// <param name="scrollIntoView">true to scroll the new row into view.</param>
        /// <returns>true if the row was successfully changes. false it it was cancelled.</returns>
        internal protected override bool OnActiveRowChange(Infragistics.Win.UltraWinGrid.UltraGridRow newActiveRow, bool scrollIntoView)
		{
			// SSP 6/3/05 BR04404
			// Why are we performing a no-op here. ActiveRow is being set in the UltraDropDownBase and
			// UltraDropDown code. Fix for BR03816 broke being able to select an item from an
			// UltraDropDown. This is to fix that breakage.
			// 
			// ----------------------------------------------------------------------------------------
			
			
			UltraGridRow oldActiveRow = this.currentActiveRow;
			if ( null != oldActiveRow )
				oldActiveRow.InvalidateItemAllRegions( );

			this.currentActiveRow = newActiveRow;

			if ( null != this.currentActiveRow )
				this.currentActiveRow.InvalidateItemAllRegions( );

			return true;
			// ----------------------------------------------------------------------------------------


		}

        /// <summary>
        /// Called when the active row is cleared
        /// </summary>
        ///<param name="update">true to update the row.</param>
        ///<returns>true if the row was successfully cleared. false it it was cancelled.</returns>
        internal protected override bool OnActiveRowCleared(bool update)
		{
			// SSP 6/3/05 BR04404
			// Why are we performing a no-op here. ActiveRow is being set in the UltraDropDownBase and
			// UltraDropDown code. Fix for BR03816 broke being able to select an item from an
			// UltraDropDown. This is to fix that breakage.
			// 
			// ----------------------------------------------------------------------------------------
			

			// Invalidate the current active row.
			//
			if ( null != this.currentActiveRow )
				this.currentActiveRow.InvalidateItemAllRegions( );

			this.currentActiveRow = null;

			return true;
			// ----------------------------------------------------------------------------------------

			
		}


		#endregion

        //  BF 4/21/08  NA 2008.2 - WinValidator
        #region IValidatorClient interface implementation
        //
        //  Note that the ValidationSettings extender property (see Infragistics.Win.Misc.UltraValidator)       
        //  is not extended to implementors of this interface.
        //
        /// <summary>
        /// Returns whether validation is supported through <see cref="Infragistics.Win.EmbeddableEditorBase">embeddable editors</see>.
        /// </summary>
        bool IValidatorClient.IsValidatorSupported
        {
            get { return true; }
        }
        #endregion IValidatorClient interface implementation        
	}

}
