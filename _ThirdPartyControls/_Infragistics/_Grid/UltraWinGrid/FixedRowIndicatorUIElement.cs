#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using System.Diagnostics;
using System.Drawing.Imaging;

// SSP 3/29/05 - NAS 5.2 Fixed Rows
//

namespace Infragistics.Win.UltraWinGrid
{
	#region FixedRowIndicatorUIElement Class

	/// <summary>
	/// UIElement used for fixed header indicator.
	/// </summary>
	public class FixedRowIndicatorUIElement : FixedIndicatorUIElementBase
	{
		#region Private/Internal variables

		internal const int FIXED_ROW_INDICATOR_WIDTH = 11;
		internal const int FIXED_ROW_INDICATOR_PADDING = 1;

		#endregion // Private/Internal variables

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public FixedRowIndicatorUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region InitAppearance

		// SSP 8/12/05 BR05338
		// Overrode InitAppearance so we resolve the header appearance first. Apparently the
		// ButtonUIElementBase.InitAppearance resolves the default appearances based on the
		// style of the button, overridding the parent element (which can be a header elem or
		// row selector elem) appearances.
		// 
        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			RowSelectorUIElementBase rowSelectorElem = (RowSelectorUIElementBase)this.GetAncestor( typeof( RowSelectorUIElementBase ) );
			if ( null != rowSelectorElem )
			{
				AppearancePropFlags HEADER_APP_FLAGS = AppearancePropFlags.ForeColor | AppearancePropFlags.ForeColorDisabled;
				AppearancePropFlags tmpFlags = HEADER_APP_FLAGS & flags;
				AppearancePropFlags origTmpFlags = tmpFlags;

				rowSelectorElem.InternalInitAppearance( ref appData, ref tmpFlags );

				flags ^= origTmpFlags ^ tmpFlags;
			}

			base.InitAppearance( ref appData, ref flags );
		}

		#endregion // InitAppearance

		#region Row

		/// <summary>
		/// Gets the associated Row object.
		/// </summary>
		public UltraGridRow Row
		{
			get
			{
				return (UltraGridRow)this.parentElement.GetContext( typeof( UltraGridRow ), true );
			}
		}

		#endregion // Row

		#region GetButtonImage

		internal override System.Drawing.Image GetButtonImage( out bool setTransparencyKey, out UltraGridLayout layout )
		{
			UltraGridRow row = this.Row;
			layout = null != row ? row.Layout : null;
			Debug.Assert( null != layout, "No layout !" );
			System.Drawing.Image image = null;
			setTransparencyKey = false;
			if ( null != layout )
			{
				if ( row.Fixed )
				{
					image = layout.FixedRowOnImage;
					setTransparencyKey = ! layout.ShouldSerializeFixedRowOnImage( );
				}
				else
				{
					image = layout.FixedRowOffImage;
					setTransparencyKey = ! layout.ShouldSerializeFixedRowOffImage( );
				}
			}

			return image;
		}

		#endregion // GetButtonImage

		#region ButtonClick

		/// <summary>
		/// Called when the button is clicked.
		/// </summary>
		/// <returns></returns>
		protected override bool ButtonClick( )
		{
			// If the button is disabled, then return without doing anything.
			//
			if ( ! this.Enabled )
				return false;

			UltraGridRow row = this.Row;
			Debug.Assert( null != row && null != row.Layout );
			if ( null != row && null != row.Layout )
			{
				// Exit the edit mode before proceeding.
				//
				UltraGridCell activeCell = row.Layout.ActiveCell;
				if ( null != activeCell && activeCell.IsInEditMode )
				{
					// If exitting the edit mode is cancelled, then return without
					// proceeding.
					//
					if ( activeCell.ExitEditMode( ) )
						return true;
				}

				row.OnFixedRowIndicatorClicked( );
			}

			return true;
		}

		#endregion // ButtonClick
	}

	#endregion // FixedRowIndicatorUIElement Class

}

