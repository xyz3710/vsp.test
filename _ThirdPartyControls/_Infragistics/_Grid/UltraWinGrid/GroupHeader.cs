#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
    using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Collections;
    using System.Windows.Forms;
    using System.Diagnostics;
	using System.Runtime.Serialization;
    using Infragistics.Shared;
    using Infragistics.Win;
	using System.Security.Permissions;
    using Infragistics.Win.Layout;
	using System.Collections.Generic;

    /// <summary>
    ///	The class represents the header for a specific group
    /// </summary>
    [ Serializable() ]
	public sealed class GroupHeader : HeaderBase, ISerializable,
        // MRS - NAS 9.1 - Groups in RowLayout
        ILayoutChildItem
    {
        private Infragistics.Win.UltraWinGrid.UltraGridGroup group;
		private bool firstItem = false;
		private bool lastItem = false;

        internal GroupHeader( Infragistics.Win.UltraWinGrid.UltraGridGroup group )
        {
            this.group				= group;
			this.visiblePos	= -1;
        }

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        // AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//public GroupHeader( SerializationInfo info, StreamingContext context )
		private GroupHeader( SerializationInfo info, StreamingContext context )
		{
			this.visiblePos = -1;

			base.DeserializeHelper( info, context );
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
			base.GetObjectData( info, context );
		}
 
		internal void InitGroup( Infragistics.Win.UltraWinGrid.UltraGridGroup group )
		{
			this.group = group;

			base.InitAppearanceHolder();
		}
 
		// SSP 11/21/03 UWG2746
		// Added clone method.
		//
		internal GroupHeader Clone( )
		{
			// SSP 5/20/04 UWG3097
			// Set the appearance holder on the cloned header to null so that when the 
			// cloned header is disposed off, it doesn't try to reset the appearance holder
			// that's referenced by the source header.
			//
			//return (GroupHeader)this.MemberwiseClone( );
			GroupHeader header = (GroupHeader)this.MemberwiseClone( );
			header.appearanceHolder = null;
			return header;
		}

		/// <summary>
		/// Returns the Group object that the object is associated with.  This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Group</b> property of an object refers to a specific group of columns in the grid as defined by a Group object. You use the <b>Group</b> property to access the properties of a specified Group object, or to return a reference to a Group object. A Group is a group of columns that appear together in the grid, and can be resized, moved or swapped together as a unit. Columns in the same group share a group header, and can be arranged into a multi-row layout within the group, with different columns occupying different vertical levels within a single row of data. Groups also help with the logical arrangement of columns within the grid.</p>
		/// </remarks>
        public override Infragistics.Win.UltraWinGrid.UltraGridGroup Group
        {
            get
            {
                return this.group;
            }
        }

		/// <summary>
		/// The header owner's band object. This property
		/// is read-only.
		/// </summary>
        [Browsable(false)]
		public override Infragistics.Win.UltraWinGrid.UltraGridBand Band 
		{
			get
			{
				return this.group != null ? this.group.Band : null;
			}
		}

		internal override bool PivotOnRowScrollRegion { get { return false; } }
		internal override bool PivotOnColScrollRegion { get { return true; } }
		internal override string DefaultHeaderCaption 
        { 
            get 
            {
				if ( this.group == null )
					return string.Empty;

                return this.group.Key;
            }
        }
		internal override int Extent
        {
            get
            {
				int c = 0;
				if ( this.FirstItem && this.Band != null )
				{	c  = this.Band.RowSelectorExtent;				
					return this.group.Extent + c;
				}
                return this.group.Extent;
            }
        }
		internal override UltraGridColumn GetFirstVisibleCol( bool includeNonActivateable )
        {
            
            //const COL_LIST* pColList = GetColumns();
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
			// actually visible in the group
            //GroupColumnsCollection colList = this.Group.Columns;
			IEnumerable<UltraGridColumn> colList = this.Group.ColumnsResolved;

            if ( null == colList )
                return null;			

            //COL_LIST::const_iterator ItemIterator;

            // loop thru each column to find the the first visible column
            //
			// MD 1/21/09 - Groups in RowLayouts
			// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach.
			//for ( int i = 0; i < colList.Count; i++ )
			//{
			//    //pColumn = *ItemIterator;
			//    UltraGridColumn column = colList[i];
			foreach ( UltraGridColumn column in colList )
			{
				// MD 1/21/09 - Groups in RowLayouts
				// Use HiddenResolved instead of Hidden because it checks the group's hidden value.
                //if ( column.Hidden )
				if ( column.HiddenResolved )
                    continue;

                if ( includeNonActivateable || 
                    !column.Header.IsDisabled )
                    return column;
            }

            return null;
        }

		internal override UltraGridColumn GetNextVisibleCol( UltraGridColumn column, bool includeNonActivateable )
        {
           		
            //const COL_LIST* pColList = GetColumns();
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
			// actually visible in the group
            //GroupColumnsCollection colList = this.Group.Columns;
			IEnumerable<UltraGridColumn> colList = this.Group.ColumnsResolved;

            if ( null == colList )
                return null;


            // MBS 10/25/07 - BR27345
            //ColScrollRegion exclusiveColScrollRegion = this.ExclusiveColScrollRegion;
            ColScrollRegion exclusiveColScrollRegion = this.ExclusiveColScrollRegionResolved;

			// MD 1/21/09 - Groups in RowLayouts
			// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach and since we need to find the last item,
			// the logic has to be changed slightly.
			#region Old Code

			
			
			#endregion Old Code
			bool foundSpecifiedColumn = false;
			foreach ( UltraGridColumn ctemp in colList )
			{
				// If we haven't yet hit the passed in column, check for it and skip to the next column.
				if ( foundSpecifiedColumn == false )
				{
					foundSpecifiedColumn = ( column == ctemp );
					continue;
				}

				if ( ctemp.HiddenResolved ||
					// MBS 10/25/07 - BR27345
					// We should be checking the resolved ColScrollRegion
					//
					//ctemp.Header.ExclusiveColScrollRegion != exclusiveColScrollRegion )
					ctemp.Header.ExclusiveColScrollRegionResolved != exclusiveColScrollRegion )
				{
					continue;
				}

				if ( includeNonActivateable == false && ctemp.Header.IsDisabled )
					continue;

				return ctemp;
			}

            return null;
        }
		internal override UltraGridColumn GetPrevVisibleCol( UltraGridColumn column, bool includeNonActivateable )
        {
            
            //const COL_LIST* pColList = GetColumns();
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
			// actually visible in the group
            //GroupColumnsCollection colList = this.Group.Columns;
			IEnumerable<UltraGridColumn> colList = this.Group.ColumnsResolved;

            if ( null == colList )
                return null;

			UltraGridColumn c = null;

            // MBS 10/25/07 - BR27345
            //ColScrollRegion exclusiveColScrollRegion = column.Header.ExclusiveColScrollRegion;
            ColScrollRegion exclusiveColScrollRegion = column.Header.ExclusiveColScrollRegionResolved;

            //COL_LIST::const_iterator ItemIterator;

            // loop thru each column until we find the passed in column
            //
			// MD 1/21/09 - Groups in RowLayouts
			// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach.
			//for ( int i = 0; i < colList.Count; i++ )
			//{
			//    UltraGridColumn ctemp = colList[i];
			foreach ( UltraGridColumn ctemp in colList )
			{
                // when we find the passed in column return the last visible col
                //
                if ( column == ctemp )
                    return c;

				// MD 1/21/09 - Groups in RowLayouts
				// Use HiddenResolved instead of Hidden because it checks the group's hidden value.
                //if ( ctemp.Hidden ||
				if ( ctemp.HiddenResolved ||
                    // MBS 10/25/07 - BR27345
                    // We should be checking the resolved ColScrollRegion
                    //
                    //ctemp.Header.ExclusiveColScrollRegion != exclusiveColScrollRegion )
                    ctemp.Header.ExclusiveColScrollRegionResolved != exclusiveColScrollRegion)
                    continue;

                // keep track of the last visible col
                //
                if ( includeNonActivateable || !ctemp.Header.IsDisabled )
                    c = ctemp;
            }

            return null;
        }
		internal override UltraGridColumn GetLastVisibleCol( bool includeNonActivateable )
        {
           
            //const COL_LIST* pColList = GetColumns();

			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
			// actually visible in the group
            //GroupColumnsCollection colList = this.Group.Columns;
			IEnumerable<UltraGridColumn> colList = this.Group.ColumnsResolved;

            if ( null == colList )
                return null;

			// MD 1/21/09 - Groups in RowLayouts
			// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach and since we need to find the last item,
			// the logic has to be changed slightly.
			#region Old Code

			


			#endregion Old Code
			UltraGridColumn lastVisibleColumn = null;
			foreach ( UltraGridColumn column in colList )
			{
				if ( column.HiddenResolved )
					continue;

				if ( includeNonActivateable == false && column.Header.IsDisabled )
					continue;

				lastVisibleColumn = column;
			}

			return lastVisibleColumn;
        }

       
        internal override UltraGridColumn GetVisibleColAbove ( UltraGridColumn column, bool topMost, bool includeNonActivateable ) 
        {
            // FUTURE: implement logic for multi-level groups
            //

            //return fTopMost 
            //	? ( bIncludeNonActivateable || !Column.IsDisabled() )
            //	? &Column 
            //	: NULL
            //	: NULL;
            return topMost && ( includeNonActivateable || !column.Header.IsDisabled ) 
                ? column : null;
        }		  


        internal override UltraGridColumn GetVisibleColBelow ( UltraGridColumn column, bool bottomMost, bool includeNonActivateable ) 
        {
            // FUTURE: implement logic for multi-level groups
            //
            //return fBottomMost 
            //	? ( bIncludeNonActivateable || !Column.IsDisabled() )
            //	? &Column 
            //	: NULL
            //	: NULL;
            return bottomMost && ( includeNonActivateable || !column.Header.IsDisabled )
                ? column : null;
        }	
        
        internal override bool IsGroup 
        {
            get
            {				
                return true;
            }
        }
		internal override bool Hidden 
        { 
            get 
            {
                // MRS 2/23/2009 - Found while fixing TFS14354
                //return this.group.Hidden; } }
                return this.group.HiddenResolved;
            }
        }
		

		internal override bool FirstItem
		{
			get
			{
				return this.firstItem;
			}
			set
			{	//don't check if this has been changed because when 
				//changing levels, columns in group have to be reset
				this.firstItem = value;
				this.Group.FirstItem = value;
			}
		}
		internal override bool LastItem
		{
			get
			{
				return this.lastItem;
			}
			set
			{
				//don't check if this has been changed because when 
				//changing levels, columns in group have to be reset
				this.lastItem = value;
				this.group.LastItem = value;
			}
		}

		internal override void SetOrigin( int origin )
		{
			base.SetOrigin(origin);
			
			this.Group.ReCalculateColumnOrigins( );
		}

		internal override SelectChange SelectChangeType 
		{
			get
			{
				return SelectChange.Group;
			}
		}

		internal bool StartDragNoSelection( )
		{
			UltraGridBand band = this.Band;
			UltraGrid grid = this.Group.Grid as UltraGrid;
    
			if ( null == grid || this.IsDisabled )
				return false;

            // MRS 3/14/05 - Allow drag and drop in RowLayout mode
            if (band.UseRowLayoutResolved)
            {
                // MD 7/26/07 - 7.3 Performance
                // FxCop - Remove unused locals
                //bool rowLayoutDragStarted = grid.StartGridBagLayoutDrag(this.Column, isGroupByButton);
                // MD 8/2/07 - 7.3 Performance
                // Prevent calling expensive getters multiple times
                //grid.StartGridBagLayoutDrag( this.Column, isGroupByButton );
                grid.StartGridBagLayoutDrag(group, false);                
            }

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//DragStrategyGroup  dragGroupStrategy = new DragStrategyGroup( band, grid, null );
			DragStrategyGroup dragGroupStrategy = new DragStrategyGroup( band, grid );
    
			if ( dragGroupStrategy.BeginDrag( this ) )
			{
				DragEffect dragEffect = grid.DragEffect;
            
				if ( null != dragEffect )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Avoid unused private fields
					//dragEffect.Setup( dragGroupStrategy, DragType.Bitmap );
					dragEffect.Setup( dragGroupStrategy );
                
					dragEffect.Display( );

					return true;
				}
			}

			return false;
		}


		internal bool StartDrag( )
		{
			UltraGridBand band = this.Band;
			UltraGrid grid = this.Group.Grid as UltraGrid;
    
			if ( null == grid || this.IsDisabled )
				return false;
    
			Selected selected = grid.Selected; 

			if ( null == selected )
				return false;


            // MRS 2/10/2009 - TFS13728
            if (this.Band.UseRowLayoutResolved)
            {
                // MRS 7/16/2009 - TFS19547 
                // If the group has no columns within it, then we should consider it selected
                // for the purposes of starting a drag. Otherwise, there is no way to drag
                // an empty group. So if the group is not Selectable, don't bother checking
                // Selected. 
                //
                //if (this.Selected == false)
                //
                if (this.Selectable && this.Selected == false)
                    return false;                

                grid.StartGridBagLayoutDrag(this, false);
                return true;
            }
    
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//DragStrategyGroup  dragGroupStrategy = new DragStrategyGroup( band, grid, selected );
			DragStrategyGroup dragGroupStrategy = new DragStrategyGroup( band, grid );
    
			if ( dragGroupStrategy.BeginDrag( this ) )
			{
				DragEffect dragEffect = grid.DragEffect;
            
				if ( null != dragEffect )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Avoid unused private fields
					//dragEffect.Setup( dragGroupStrategy, DragType.Bitmap );
					dragEffect.Setup( dragGroupStrategy );
                
					dragEffect.Display( );

					return true;
				}
			}

			return false;
		}

		internal override void GetResizeRange( ref int minWidth, ref int maxWidth)
		{
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//this.Group.GetResizeRange ( ref minWidth, ref maxWidth );
			UltraGridGroup group = this.Group;

			group.GetResizeRange( ref minWidth, ref maxWidth );

			//we have to adjust the minWidth because header.Extent and
			//group.Extent may differ 

			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.Group.Extent != this.Extent )
			//{
			//    minWidth += this.Extent - this.Group.Extent;
			//}
			int groupExtent = group.Extent;
			int extent = this.Extent;

			if ( groupExtent != extent )
			{
				minWidth += extent - groupExtent;
			}
		}


		internal override int GetActualWidth( bool ignoreDraggingWidth )
		{
			return this.Group.GetActualWidth( ignoreDraggingWidth );
		}



		
		/// <summary>
		/// Returns the height of the group header.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Height</b> property is used to determine the vertical dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">For a Header object, this property is read-only. In a particular band, each column header has the same height. This height is determined by taking the largest height that results from the resolution of each column's header's <b>Appearance</b> attributes and the band's <b>ColHeaderLines</b> property.</p>
		/// </remarks>
		public override int Height
		{
			get
			{
				// SSP 7/21/03 Workaround for UWG1558
				// Added an overload that takes in ignoreHidden and returns a height regardless of the header
				// is hidden or visible. If the user adds a BandHeadersUIElement and the headers are hidden
				// then we should still draw the band header ui element properly.
				//
				//return this.Band.GroupHeaderHeight;
				return this.Band.GetGroupHeaderHeight( true );
			}
		}


		internal override void InternalSetWidth ( int newWidth, bool autofit )
		{
			if ( this.FirstItem ) 
			{
				newWidth -= this.Band.RowSelectorExtent;
			}

			// JJD 1/2/02 
			// When we are doing an autofit we dont want to fire any events
			//
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//this.Group.SetWidth ( newWidth, true, !autofit, true, true);	//, true );
			UltraGridGroup group = this.Group;

			group.SetWidth( newWidth, true, !autofit, true, true );	//, true );

			// JJD 1/7/02
			// Call ReCalculateColumnOrigins during an autofit we force each of 
			// the goroup's columns to be re-calculated
			//
			if ( autofit )
			{
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//this.Group.ReCalculateColumnOrigins();
				group.ReCalculateColumnOrigins();
			}
		}


		internal override bool IsValidDropArea( DropLocationInfo dropLocationInfo, Point point )
		{
			bool valid = false;

			AllowGroupMoving allowMoving = this.Band.AllowGroupMovingResolved;

		
			//	Ignore AllowGroupMoving.NotAllowed at design time
			bool designTimeMovingAllowed = false;
			
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if( null != this.Group.Grid )
			//    designTimeMovingAllowed = ( this.Group.Grid.DesignMode &&
			//        allowMoving == AllowGroupMoving.NotAllowed );
			UltraGridBase grid = this.Group.Grid;

			if ( null != grid )
			{
				designTimeMovingAllowed = 
					( grid.DesignMode && allowMoving == AllowGroupMoving.NotAllowed );
			}

			if ( allowMoving == AllowGroupMoving.WithinBand || designTimeMovingAllowed )
				//    if (eAllowMoving == ssAllowGroupMovingWithinBand)
			{
				// To mimic the behavior of the Outlook Column Header
				// moving, only display another group header as a valid
				// drop area.
				if ( dropLocationInfo.IsDropTargetGroup )
				{
					UltraGridBand band = dropLocationInfo.GridItem.Band;
					UltraGridBand thisBand = this.Band;
 
					if ( band != thisBand )
						valid = false;
					else
					{
						valid = true;

						// SSP 7/24/03 - Fixed headers
						// If the FixedHeaderIndicator settings are as such that the fixed header
						// state cannot be changed, set valid to false if the drop will cause the 
						// Fixed state to change. (If the non-fixed header is being dropped onto
						// a fixed header or a fixed header is being dropped onto a non-fixed header).
						//
						HeaderBase dropHeader = dropLocationInfo.GroupHeader;
						if ( FixedHeaderIndicator.None == this.FixedHeaderIndicatorResolved )
							// SSP 6/2/04 UWG3357
							// 
							//valid = null != dropHeader && this.FixedResolved == dropHeader.FixedResolved;
							valid = valid && null != dropHeader && this.FixedResolved == dropHeader.FixedResolved;
					}
				}
				else
				{
					valid = false;
				}
			}

			return valid;
		}
		internal override bool AllowSizing
		{
			get
			{
				return this.group.AllowSizing;
			}			
		}

		internal override bool AllowSwapping
		{
			get
			{
				return this.Group.AllowSwapping;
			}
		}

		internal override void CreateSwapList( ValueList swapDropDown )
		{
			this.Group.CreateSwapList( swapDropDown );
		}

		internal override void Swap( HeaderBase swapItem )
		{			
			this.Group.Swap( swapItem.Group );
		}


		internal override Activation ActivationResolved 
		{
			get
			{
				// JJD 11/02/01
				// Return the group's resolved activation 
				//
				return this.group.ActivationResolved;
			}
		}
		internal override ISelectionStrategy SelectionStrategyDefault 
		{
			get
			{
				return ((ISelectionStrategyProvider)(Band)).SelectionStrategyColumn;
			}
		}

		/// <summary>  
		/// Returns or sets the selected state of the header.
		/// </summary>  
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override bool Selected
		{
			get
			{
				bool allColumnsSelected = true;
				bool visibleColumnsFound = false;

				// MD 2/25/09 - TFS14590
				// If this is group in row layout mode, we have to consider columns nested under this group and not directly under it.
				// The ColumnsResolved property only enumerates the directly owned columns.
				if ( this.Band == null ||
					this.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
				{
					// See if each column in the group is selected
					//
					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
					// actually visible in the group
					//for ( int index = 0; index < this.group.Columns.Count; index++ )
					//{
					//    Infragistics.Win.UltraWinGrid.ColumnHeader header = this.group.Columns[index].Header;
					foreach ( UltraGridColumn column in this.group.ColumnsResolved )
					{
						Infragistics.Win.UltraWinGrid.ColumnHeader header = column.Header;

						if ( header.Hidden || !header.Enabled )
							continue;

						if ( !header.Selected )
							return false;

						visibleColumnsFound = true;
					}
				}
				else
				{
					foreach ( UltraGridColumn column in this.Band.Columns )
					{
						if ( UltraGridGroup.IsDescendantOfGroup( this.group, column ) == false )
							continue;

						Infragistics.Win.UltraWinGrid.ColumnHeader header = column.Header;

						if ( header.Hidden || !header.Enabled )
							continue;

						if ( !header.Selected )
							return false;

						visibleColumnsFound = true;
					}
				}

				return visibleColumnsFound && allColumnsSelected;
			}
			set
			{
				base.Selected = value;
			}
		}

		/// <summary>
		/// Adds column to passed-in collection.
		/// </summary>
		internal override void AddToCollection( Selected selected )
		{
			if ( selected == null )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_153"));

			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
			// actually visible in the group
			////pass in each column from the group's collection
			//for ( int index = 0; index < this.group.Columns.Count; index++ )
			//    selected.Columns.InternalAdd( this.group.Columns[index].Header );
			foreach ( UltraGridColumn column in this.group.ColumnsResolved )
				selected.Columns.InternalAdd( column.Header );

            // MRS 2/10/2009 - TFS13728
            IEnumerable<UltraGridGroup> childGroups = this.group.ChildGroupsResolved;
            if (childGroups != null)
            {
                foreach (UltraGridGroup group in childGroups)
                {
                    group.Header.AddToCollection(selected);
                }
            }            

			// JJD 1/14/02
			// Call SetSelectedFlag instead. Otherwise this generates a stcak fault.
			//this.Selected = true;
			this.SetSelectedFlag( true );
		}
		internal override void RemoveFromCollection( Selected selected )
		{
			Debug.Assert( selected.Columns != null, "No collection to remove from!" );

			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
			// actually visible in the group
			//for ( int index = 0; index < this.group.Columns.Count; index++ )
			//    selected.Columns.Remove( this.group.Columns[index].Header );
			foreach ( UltraGridColumn column in this.group.ColumnsResolved )
				selected.Columns.Remove( column.Header );
		}

		internal override void CalcSelectionRange( Selected newSelection, 
												   bool clearExistingSelection,
												   bool select,
												   Selected initialSelection )
		{
			GridItemBase pivotItem = this.Band.Layout.Grid.ActiveColScrollRegion.GetPivotItem();

			Debug.Assert( pivotItem != null, "pivotItem null!" );

			// bands must be the same
			Debug.Assert( pivotItem.Band == this.Band, "Bands don't match!" );

			GroupHeader pivotCol = pivotItem as GroupHeader;

            // MRS 2/10/2009 - TFS13728
            // I copied this code from ColumnHeader.CalcSelectionRange and modified it for groups. 
            // Following is the code for header selection in row layout mode. Since headers can be laid
            // out in verious ways that may not correspond with the visible positions of the headers, we
            // need to have special selection logic.
            // 
            // ------------------------------------------------------------------------------------------
            if (this.Band.UseRowLayoutResolved)
            {
                if (!clearExistingSelection)
                {
                    UltraGrid grid = this.Band.Layout.Grid as UltraGrid;
                    Infragistics.Win.UltraWinGrid.Selected selected = null != grid && grid.HasSelectedBeenAllocated ? grid.Selected : null;

                    if (null != selected)
                    {
                        foreach (UltraGridRow row in selected.Rows)
                        {
                            newSelection.Rows.InternalAdd(row);
                        }

                        foreach (UltraGridCell cell in selected.Cells)
                        {
                            newSelection.Cells.InternalAdd(cell);
                        }
                    }

                    foreach (ColumnHeader col in initialSelection.Columns)
                    {
                        newSelection.Columns.InternalAdd(col);
                    }
                }

                if (pivotCol == this)
                {
                    List<UltraGridColumn> columns = this.Group.GetDescendantColumns(true);
                    foreach (UltraGridColumn column in columns)
                    {
                        if (!newSelection.Columns.Contains(column.Header))
                            newSelection.Columns.InternalAdd(column.Header);
                    }
                }
                else
                {
                    UltraGridBand band = this.Band;
                    GroupHeader groupHeader1 = (GroupHeader)pivotCol;
                    GroupHeader groupHeader2 = this;

                    HeadersCollection layoutOrderedGroupHeaders = band.LayoutOrderedVisibleGroupHeaders;

                    if (null != layoutOrderedGroupHeaders &&
                        layoutOrderedGroupHeaders.Contains(groupHeader1) &&
                        layoutOrderedGroupHeaders.Contains(groupHeader2))
                    {
                        Rectangle boundingRect = Rectangle.Union(
                            groupHeader1.Group.RowLayoutGroupInfo.CachedHeaderRect,
                            groupHeader2.Group.RowLayoutGroupInfo.CachedHeaderRect);

                        for (int i = 0; i < layoutOrderedGroupHeaders.Count; i++)
                        {
                            Infragistics.Win.UltraWinGrid.GroupHeader header = (GroupHeader)layoutOrderedGroupHeaders[i];

                            Rectangle itemRect = header.Group.RowLayoutGroupInfo.CachedItemRectHeader;
                            itemRect.Inflate(-1, -1);

                            List<UltraGridColumn> columns = header.group.GetDescendantColumns(true);
                            foreach (UltraGridColumn column in columns)
                            {
                                if (!column.Header.IsDisabled &&                                    
                                    boundingRect.IntersectsWith(itemRect)
                                    && !newSelection.Columns.Contains(column.Header))
                                    newSelection.Columns.InternalAdd(column.Header);                                
                            }                           
                        }

                    }
                    else
                    {
                        Debug.Assert(false);
                    }
                }

                return;
            }
            // ------------------------------------------------------------------------------------------
			

			HeadersCollection headers = this.GetHeadersForSelection( select, pivotCol );
			for ( int i = 0; i < headers.Count; i++ )
				headers[i].AddToCollection(newSelection);

			headers = null;

			if ( ! clearExistingSelection )
			{
				int positionToTest;
				int position = this.VisiblePosition;
				int positionPivotItem = pivotCol.VisiblePosition;

				//MergeSelectionWithInitialSelection();
				foreach ( ColumnHeader col in initialSelection.Columns )
				{
					// JJD 10/15/01
					// Get the position once to avoid unnecessary
					// overhead
					//
					positionToTest = col.VisiblePosition;

					// JJD 10/15/01 - UWG523
					// Don't re-add the if the position is equal to the pivot
					//

					if ( position > positionPivotItem )
					{
						if ( positionToTest < positionPivotItem ||
							 positionToTest > position )
							newSelection.Columns.InternalAdd( col );							 
					}
					else 
					{
						if ( positionToTest < position ||
							 positionToTest > positionPivotItem )
							newSelection.Columns.InternalAdd( col );
					}
				}
			}


		}

		// AS - 10/24/01
		// Checked with Vlad about the reason behind the IsSelectable
		// property of the ISelectableItem. Essentially, it was implemented
		// so that empty groups could be dragged even though they could not
		// be selected.

		/// <summary>
		/// You should be able to drag a group even though it cannot be selected.
		/// </summary>
		internal protected override bool Selectable
		{ 
			get 
			{ 
				if (this.group == null)
					return base.Selectable;
				// MD 1/21/09 - Groups in RowLayouts
				// In GroupLayout style, the Columns collection may not have the groups that are visible in the group.
				else if ( this.Band != null && this.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout )
				{
                    // MRS 2/18/2009 - TFS14194
                    //foreach (UltraGridColumn column in this.Band.Columns)
                    //{
                    //    if (column.RowLayoutColumnInfo.ParentGroup != this.group)
                    //        continue;

                    //    if (column.HiddenResolved)
                    //        continue;

                    //    return true;
                    //}                    
                    //
                    List<UltraGridColumn> columns = this.Group.GetDescendantColumns(true);
                    foreach (UltraGridColumn column in columns)
                    {
                        if (column.HiddenResolved)
                            continue;

                        return true;
                    }

					return false;
				}
				else
					return this.group.Columns.Count != 0;
			} 
		}

		/// <summary>
		/// defaults to true
		/// </summary>
		internal protected override bool Draggable 
		{ 
			get 
			{
				if ( this.Band == null )
					return false;

				if ( AllowGroupMoving.NotAllowed != this.Band.AllowGroupMovingResolved )
					return true;

				return ( this.Band.Layout != null &&
						 this.Band.Layout.Grid != null &&
						 this.Band.Layout.Grid.DesignMode ); // Only return false if not in design mode
			} 
		} 

		internal override int DefaultVisiblePosition
		{
			get 
			{
				if ( this.group == null ||
					 this.group.Band == null )
					return -1;

				return this.group.Index;
			}
		}

		/// <summary>
		/// Returns or sets the visible position of a header.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property can be used to specify the ordinal positions of groups and columns.</p>
		/// <p class="body">For group headers, this property returns or sets the position of the group within that group's band. For column headers, this property returns or sets the position of the column within its group, if the column belongs to a group, or its band, if the column belongs to a band.</p>
		/// </remarks>
		public override int VisiblePosition
		{
			get
			{
				if ( this.group			!= null &&
					 this.group.Band	!= null )
				{
					// JJD 10/16/01
					// The first time through the visible position is -1 unless
					// a different value was persisted. In this case initialize
					// the position to the group's index
					//
					int serializedPosition = this.SerializedVisiblePosition;

					int position = this.Group.InternalGetVisiblePosition();

					if ( position >= 0 )
						return position;
				}

				return base.VisiblePosition;
			}
			set
			{
				if ( this.group == null || this.group.Band == null )
					base.visiblePos = value;
				else
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( this.Group.IsClone )
					UltraGridGroup group = this.Group;

					if ( group.IsClone )
						throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_154"));

					if ( value < 0 || value >= this.Band.OrderedGroupHeaders.Count )
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_155"), value, Shared.SR.GetString("LE_ArgumentOutOfRangeException_331"));
	 
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//int position = this.Group.InternalGetVisiblePosition();
					int position = group.InternalGetVisiblePosition();

					if ( value != position )
					{
						// SSP 7/12/05 BR04913
						// We need to also update the VisiblePosition property values of other
						// groups since changing a group's visible position may affect visible
						// position of other groups. Use the SetVisiblePosition method instead.
						// 
						// --------------------------------------------------------------------
						bool posChanged = true;

						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//this.Band.SetVisiblePosition( this.Group, value, ref posChanged );
						this.Band.SetVisiblePosition( group, value, ref posChanged );

						if ( posChanged )
							this.NotifyPropChange( PropertyIds.VisiblePosition );
						
						// --------------------------------------------------------------------
					}
				}
			}
		}

		#region SizeResolved

		// SSP 6/25/03 - Row Layout Functionality
		// Added SizeResolved method.
		//
		/// <summary>
		/// Resolved header size. This property returns the actual width and the height of the header.
		/// </summary>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override Size SizeResolved 
		{
			get
			{
				UltraGridBand band = this.Band;

				Debug.Assert( null != band, "No band !" );
				if ( null == band )
					return Size.Empty;

				return new Size( this.Extent, this.Height );
			}
		}

		#endregion // SizeResolved


		#region GroupHeaderAccessibleObject Class

		/// <summary>
		/// The Accessible object for a column header.
		/// </summary>
		public class GroupHeaderAccessibleObject : HeaderBase.HeaderAccessibleObjectBase
		{
			#region Private Members

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="groupHeader">The GroupHeader.</param>
			/// <param name="parent">The parent AccessibleObject.</param>
			public GroupHeaderAccessibleObject( GroupHeader groupHeader, HeadersCollection.HeadersAccessibleObject parent ) : base( groupHeader, parent )
			{
			}

			#endregion Constructor

			#region Base Class Overrides

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				return null;
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{
				return 0;
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				return null;
			}

				#endregion GetFocused

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				
				return null;
			}

				#endregion GetSelected

				#region Navigate

            /// <summary>
            /// Navigates to another accessible object.
            /// </summary>
            /// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
            /// <returns>An AccessibleObject relative to this object.</returns>
            public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{
				switch ( navdir )
				{
						
						#region case FirstChild

					case AccessibleNavigation.FirstChild:
						return null;

						#endregion case FirstChild

						
						#region case LastChild

					case AccessibleNavigation.LastChild:
					{
						return null;
					}

						#endregion case LastChild

				}

				switch ( navdir )
				{
						#region case Down

					case AccessibleNavigation.Down:
					{
						// Navigating Down from a group header should go to the first column header of the group.
						//
						UltraGridColumn col = this.Header.GetFirstVisibleCol( true );
						return null != col 
							? this.ParentInternal.GetAccessibleObjectFor( col.Header )
							: null;
					}

						#endregion case Down

						#region case Up

					case AccessibleNavigation.Up:
					{
						// You can only go up to a band header from a group-header.
						//
						if ( ! this.Header.Band.Header.Hidden )
							return this.ParentInternal.GetAccessibleObjectFor( this.Header.Band.Header );

						break;
					}

						#endregion case Up

						#region case Next

					case AccessibleNavigation.Next:						
					{
						int index = this.ParentInternal.Headers.IndexOf( this.Header );
						return index >= 0 && 1 + index < this.ParentInternal.Headers.Count
							// MD 8/10/07 - 7.3 Performance
							// Use generics
							//? this.ParentInternal.GetAccessibleObjectFor( (HeaderBase)this.ParentInternal.Headers[ 1 + index ] ) 
							? this.ParentInternal.GetAccessibleObjectFor( this.ParentInternal.Headers[ 1 + index ] ) 
							: null;
					}

						#endregion case Next

						#region case Previous

					case AccessibleNavigation.Previous:
					{
						int index = this.ParentInternal.Headers.IndexOf( this.Header );
						return index > 0 
							// MD 8/10/07 - 7.3 Performance
							// Use generics
							//? this.ParentInternal.GetAccessibleObjectFor( (HeaderBase)this.ParentInternal.Headers[ index - 1 ] )
							? this.ParentInternal.GetAccessibleObjectFor( this.ParentInternal.Headers[ index - 1 ] )
							: null;
					}

						#endregion case Previous

						#region case Right

					case AccessibleNavigation.Right:
					{
						UltraGridGroup group = this.Group.GetRelatedVisibleGroup( VisibleRelation.Next );
						return null != group
							? this.ParentInternal.GetAccessibleObjectFor( group.Header )
							: null;
					}

						#endregion case Right

						#region case Left

					case AccessibleNavigation.Left:
					{
						UltraGridGroup group = this.Group.GetRelatedVisibleGroup( VisibleRelation.Previous );
						return null != group
							? this.ParentInternal.GetAccessibleObjectFor( group.Header )
							: null;
					}

						#endregion case Left
				}

				return null;
			}

				#endregion Navigate

				#region Role

			/// <summary>
			/// Gets the role of this accessible object.
			/// </summary>
			public override AccessibleRole Role
			{
				get
				{
					return AccessibleRole.Grouping;
				}
			}

				#endregion Role

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Group

			/// <summary>
			/// Returns the associated group.
			/// </summary>
			public UltraGridGroup Group
			{ 
				get 
				{
					return ((GroupHeader)this.Header).Group;
				} 
			}

					#endregion Group

				#endregion Public Properties

				#region Private Properties

				#endregion // Private Properties

			#endregion Properties

		}

		#endregion GroupHeaderAccessibleObject Class

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		#region DefaultTextOrientation

		internal override TextOrientationInfo DefaultTextOrientation
		{
			get
			{
				UltraGridBand band = this.Band;

				if ( band == null )
					return null;

				return band.GroupHeaderTextOrientationResolved;
			}
		} 

		#endregion DefaultTextOrientation

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

        #region VisiblePositionWithinBand
        /// <summary>
        /// The overall visible position of the groups within its band (read-only).
        /// </summary>
        /// <remarks>If groups are displayed this will be the relative position of the column within the group plus the total number of columns from all preceeding groups. The value returned includes columns that are hidden.</remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int VisiblePositionWithinBand
        {
            get
            {
                int position = -1;
                             
                UltraGridGroup group = this.Group;

                // First check if groups are displayed
                //
                if (this.Band.GroupsDisplayed && this.Band.RowLayoutStyle != RowLayoutStyle.GroupLayout)
                {
                    position = this.VisiblePosition;
                }
                else
                {
                    position = this.Band.OrderedGroupHeaders.IndexOf(group.Header);
                }

                return position;
            }
        }
        #endregion //VisiblePositionWithinBand


        #region RowLayoutColumnInfo
        /// <summary>
        /// Returns the RowLayoutColumnInfo for the object (column or group) associated with the header.
        /// </summary>
        protected internal override RowLayoutColumnInfo RowLayoutColumnInfo
        {
            get
            {
                return this.Group.RowLayoutGroupInfo;
            }
        }
        #endregion //RowLayoutColumnInfo

        #region ILayoutItem.IsVisible

        /// <summary>
        /// Indicates whether this column is visible.
        /// </summary>
        bool ILayoutItem.IsVisible
        {
            get
            {
                return this.Group.IsVisibleInLayout;
            }
        }

        #endregion // ILayoutItem.IsVisible

        #region ILayoutChildItem Members

        /// <summary>
        /// Returns the group associated with this LayoutItem, or null, if the layoutitem does not represent a group. 
        /// </summary>
        ILayoutGroup ILayoutChildItem.AssociatedGroup
        {
            get { return this.Group; }
        }

        /// <summary>
        /// Returns the group into which an item will be dropped relative to this element.
        /// </summary>
        /// <param name="dropLocation"></param>
        /// <returns></returns>
        /// <remarks>
        /// This methods takes a DropLocation and determines what group an item dropped at that location 
        /// should be dropped into. If the item is a content, then the group is the parent group
        /// of that content. If the item is a group header, then the item may be dropped inside the group
        /// or into it's parent, depending on the DropLocation relative to the position of the header. 
        /// For example, if the group header is on top, dropping an item on the bottom of the header 
        /// should place that item inside the group while dropping the item to the left, right, or top
        /// of the header should place the item as a sibling of the group. 
        /// </remarks>
        ILayoutGroup ILayoutChildItem.GetDropGroup(GridBagLayoutDragStrategy.DropLocation dropLocation)
        {
            ILayoutChildItem thisChildLayoutItem = (ILayoutChildItem)this;
            switch (this.Group.RowLayoutGroupInfo.LabelPositionResolved)
            {
                case LabelPosition.Bottom:
                    if (dropLocation == GridBagLayoutDragStrategy.DropLocation.Top)
                        return thisChildLayoutItem.AssociatedGroup;

                    break;
                case LabelPosition.Left:
                    if (dropLocation == GridBagLayoutDragStrategy.DropLocation.Right)
                        return thisChildLayoutItem.AssociatedGroup;

                    break;
                case LabelPosition.Top:
                    if (dropLocation == GridBagLayoutDragStrategy.DropLocation.Bottom)
                        return thisChildLayoutItem.AssociatedGroup;

                    break;
                case LabelPosition.Right:
                    if (dropLocation == GridBagLayoutDragStrategy.DropLocation.Left)
                        return thisChildLayoutItem.AssociatedGroup;

                    break;
                case LabelPosition.LabelOnly:
                    break;
                case LabelPosition.None:
                case LabelPosition.Default:
                default:
                    Debug.Fail("Unknown or invalid LabelPosition");
                    break;
            }

            return thisChildLayoutItem.ParentGroup;
        }

        #endregion //ILayoutChildItem Members

        #endregion // NAS 9.1 - Groups in RowLayout

		// MD 2/17/09 - TFS14116
		// With the original fix for TFS13834, columns cannot be moved to new groups by default at design-time. The has been changed 
		// to allow for different behavior at run-time and design-time.
		//#region ILayoutChildItem Members
		//
		//// MRS 2/13/2009 - TFS13834
		///// <summary>
		///// Returns whether the item is allowed to be moved to another group via drag / drop.
		///// </summary>
		//bool ILayoutChildItem.AllowParentGroupChange
		//{
		//    get 
		//    {
		//        return this.group.AllowParentGroupChangeResolved;
		//    }
		//}
		//
		//#endregion // ILayoutChildItem Members        
    }
}
