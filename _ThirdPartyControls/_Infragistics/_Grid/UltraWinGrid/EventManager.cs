#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
    using System.ComponentModel;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// Class for managing an UltraGrid's events. It maintains an
    /// enabled flag for each event as well as a nested 'in progress' 
    /// count and it exposes events for firing each event.
    /// </summary>
    public class GridEventManager : EventManagerBase
    {
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
        //private UltraGrid grid;

        private static int [] beforeEventIndexes = null;
        private static int [] afterEventIndexes = null;

		/// <summary>
		/// contructor
		/// </summary>
		/// <param name="grid">grid that this event manager will be asociated with</param>
        public GridEventManager( UltraGrid grid ) : base( (int)GridEventIds.LastEventId + 1 )
        {
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
            //this.grid = grid;
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal UltraGrid Grid
		//{
		//    get
		//    {
		//        return this.grid;
		//    }
		//}

		#endregion Not Used

        /// <summary>
        /// Returns true if the event is in progress (in progress count > 0)
        /// </summary>
        /// <param name="eventid">The id of the event</param>
        /// <returns>true if the event is in progress; otherwise, false.</returns>
        public bool InProgress(GridEventIds eventid)
        {
            return base.InProgress( (int)eventid );
        }

		/// <summary>
        /// Returns true if the event is enabled
        /// </summary>
        /// <param name="eventid">The id of the event.</param>
        /// <returns>true if the event is enabled; otherwise, false.</returns>
		public bool IsEnabled( GridEventIds eventid )
        {
            return base.IsEnabled( (int)eventid );
        }

        /// <summary>
        /// Sets a specific event to enabled or disabled
        /// </summary>
        /// <param name="eventid">The id of the event.</param>
        /// <param name="enabled">True to enable the firing of the events, false to disable.</param>
		public void SetEnabled( GridEventIds eventid, bool enabled )
        {
            base.SetEnabled( (int)eventid, enabled );
        }

        /// <summary>
        /// Returns true if all events in the group are enabled
        /// </summary>
        /// <param name="group">The id of the events.</param>
        /// <returns>true if the event group is enabled; otherwise, false.</returns>		
        public bool IsEnabled( EventGroups group )
        {
            switch( group )
            {
            case EventGroups.AllEvents:
                return this.AllEventsEnabled;

            case EventGroups.BeforeEvents:
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
                //return base.IsEnabled( this.BeforeEventIndexes );
				return base.IsEnabled( GridEventManager.BeforeEventIndexes );

            case EventGroups.AfterEvents:
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
                //return base.IsEnabled( this.AfterEventIndexes );
				return base.IsEnabled( GridEventManager.AfterEventIndexes );

            }

            return false;
        }

        /// <summary>
        /// Sets all events in the group to enabled or disabled
        /// </summary>
        /// <param name="group">The id of the event group.</param>
        /// <param name="enabled">True to enable the firing of the events, false to disable.</param>
		public void SetEnabled( EventGroups group, bool enabled )
        {
            switch( group )
            {
            case EventGroups.AllEvents:
                base.AllEventsEnabled = enabled;
                break;

            case EventGroups.BeforeEvents:
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
                //base.SetEnabled( this.BeforeEventIndexes, enabled );
				base.SetEnabled( GridEventManager.BeforeEventIndexes, enabled );
                break;

            case EventGroups.AfterEvents:
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
                //base.SetEnabled( this.AfterEventIndexes, enabled );
				base.SetEnabled( GridEventManager.AfterEventIndexes, enabled );
                break;

            }
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private int [] AfterEventIndexes
		private static int[] AfterEventIndexes
        {
            get
            {
                if ( null == afterEventIndexes )
                {
                    afterEventIndexes =  new int [] 
                    {
                        (int)GridEventIds.AfterCellActivate    ,
                        (int)GridEventIds.AfterCellListCloseUp ,
                        (int)GridEventIds.AfterCellUpdate      ,
                        (int)GridEventIds.AfterColPosChanged   ,
                        (int)GridEventIds.AfterColRegionScroll ,
                        (int)GridEventIds.AfterColRegionSize   ,
                        (int)GridEventIds.AfterEnterEditMode   ,
                        (int)GridEventIds.AfterExitEditMode    ,
                        (int)GridEventIds.AfterGroupPosChanged ,
                        (int)GridEventIds.AfterRowActivate     ,
                        (int)GridEventIds.AfterRowCollapsed    ,
                        (int)GridEventIds.AfterRowExpanded     ,
                        (int)GridEventIds.AfterRowsDeleted     ,
                        (int)GridEventIds.AfterRowInsert       ,
                        (int)GridEventIds.AfterRowRegionScroll ,
                        (int)GridEventIds.AfterRowRegionSize   ,
                        (int)GridEventIds.AfterRowResize       ,
                        (int)GridEventIds.AfterRowUpdate       ,
                        (int)GridEventIds.AfterSelectChange    ,
                        (int)GridEventIds.AfterSortChange      ,
                        (int)GridEventIds.AfterCellCancelUpdate,
                        (int)GridEventIds.AfterRowCancelUpdate
                    };
                }

                return afterEventIndexes;

            }
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private int [] BeforeEventIndexes
		private static int[] BeforeEventIndexes
        {
            get
            {
                if ( null == beforeEventIndexes )
                {
                    beforeEventIndexes =  new int [] 
                    {
                        (int)GridEventIds.BeforeAutoSizeEdit     ,
                        (int)GridEventIds.BeforeCellActivate     ,
                        (int)GridEventIds.BeforeCellDeactivate   ,
                        (int)GridEventIds.BeforeCellListDropDown ,
                        (int)GridEventIds.BeforeCellUpdate       ,
                        (int)GridEventIds.BeforeColPosChanged    ,
                        (int)GridEventIds.BeforeColRegionScroll  ,
                        (int)GridEventIds.BeforeColRegionSize    ,
                        (int)GridEventIds.BeforeColRegionSplit   ,
                        (int)GridEventIds.BeforeColRegionRemoved ,
                        (int)GridEventIds.BeforeEnterEditMode    ,
                        (int)GridEventIds.BeforeExitEditMode     ,
                        (int)GridEventIds.BeforeGroupPosChanged  ,
                        (int)GridEventIds.BeforeRowActivate      ,
                        (int)GridEventIds.BeforeRowDeactivate    ,
                        (int)GridEventIds.BeforeRowCollapsed     ,
                        (int)GridEventIds.BeforeRowsDeleted      ,
                        (int)GridEventIds.BeforeRowExpanded      ,
                        (int)GridEventIds.BeforeRowInsert        ,
                        (int)GridEventIds.BeforeRowRegionScroll  ,
                        (int)GridEventIds.BeforeRowRegionSize    ,
                        (int)GridEventIds.BeforeRowRegionSplit   ,
                        (int)GridEventIds.BeforeRowRegionRemoved ,
                        (int)GridEventIds.BeforeRowResize        ,
                        (int)GridEventIds.BeforeRowUpdate        ,
                        (int)GridEventIds.BeforeSelectChange     ,
                        (int)GridEventIds.BeforeSortChange       ,
                        (int)GridEventIds.BeforeCellCancelUpdate ,
                        (int)GridEventIds.BeforeRowCancelUpdate
                    };
                }

                return beforeEventIndexes;

            }
        }

		internal void IncrementInProgress( GridEventIds eventid )
        {
            base.IncrementInProgress( (int)eventid );
        }
		internal void DecrementInProgress( GridEventIds eventid )
        {
            base.DecrementInProgress( (int)eventid );
        }

		internal bool CanFireEvent( GridEventIds eventid )
        {
            
            //      winforms container
            //
            return this.IsEnabled( eventid );
        }
		
    }

	/// <summary>
    /// Class for managing an UltraDropDown's events. It maintains an
    /// enabled flag for each event as well as a nested 'in progress' 
    /// count and it exposes events for firing each event.
    /// </summary>
    public class DropDownEventManager : EventManagerBase
    {
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private UltraDropDown		dropDown;


		/// <summary>
		/// contructor
		/// </summary>
		/// <param name="dropDown">Drop down that this event manager will be asociated with</param>
        public DropDownEventManager( UltraDropDown dropDown ) : base( (int)DropDownEventIds.LastEventId + 1 )
        {
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
            //this.dropDown = dropDown;
        }

		internal void DecrementInProgress( DropDownEventIds eventid )
        {
            base.DecrementInProgress( (int)eventid );
        }

		internal void IncrementInProgress( DropDownEventIds eventid )
        {
            base.IncrementInProgress( (int)eventid );
        }

        /// <summary>
        /// Returns true if the event is in progress (in progress count > 0)
        /// </summary>
        /// <param name="eventid">The id of the event</param>
        /// <returns>true if the event is in progress; otherwise, false.</returns>
        public bool InProgress(DropDownEventIds eventid)
        {
            return base.InProgress( (int)eventid );
        }

		internal bool CanFireEvent( DropDownEventIds eventid )
        {
            
            //      winforms container
            //
            return this.IsEnabled( eventid );
        }

        /// <summary>
        /// Returns true if the event is enabled
        /// </summary>
        /// <param name="eventid">The id of the event.</param>
        /// <returns>true if the event is enabled; otherwise, false.</returns>
        public bool IsEnabled(DropDownEventIds eventid)
        {
            return base.IsEnabled( (int)eventid );
        }
		
		// SSP 10/29/02 UWG1797
		// Added SetEnabled method.
		//
		/// <summary>
		/// Sets a specific event to enabled or disabled
		/// </summary>
        /// <param name="eventid">The id of the event.</param>
        /// <param name="enabled">True to enable the firing of the events, false to disable.</param>
        public void SetEnabled( DropDownEventIds eventid, bool enabled )
		{
			base.SetEnabled( (int)eventid, enabled );
		}
	}

	/// <summary>
    /// Class for managing an UltraCombo's events. It maintains an
    /// enabled flag for each event as well as a nested 'in progress' 
    /// count and it exposes events for firing each event.
    /// </summary>
    public class ComboEventManager : EventManagerBase
    {
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private UltraCombo		combo;


		/// <summary>
		/// contructor
		/// </summary>
		/// <param name="combo">UltraCombo control that this event manager will be asociated with.</param>
		public ComboEventManager( UltraCombo combo ) : base( (int)ComboEventIds.LastEventId + 1 )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
            //this.combo = combo;
        }

		internal void DecrementInProgress( ComboEventIds eventid )
        {
            base.DecrementInProgress( (int)eventid );
        }

		internal void IncrementInProgress( ComboEventIds eventid )
        {
            base.IncrementInProgress( (int)eventid );
        }

        /// <summary>
        /// Returns true if the event is in progress (in progress count > 0)
        /// </summary>
        /// <param name="eventid">The id of the event</param>
        /// <returns>true if the event is in progress; otherwise, false.</returns>
        public bool InProgress(ComboEventIds eventid)
        {
            return base.InProgress( (int)eventid );
        }

		internal bool CanFireEvent( ComboEventIds eventid )
        {
            
            //      winforms container
            //
            return this.IsEnabled( eventid );
        }

        /// <summary>
        /// Returns true if the event is enabled
        /// </summary>
        /// <param name="eventid">The id of the event.</param>
        /// <returns>true if the event is enabled; otherwise, false.</returns>
        public bool IsEnabled(ComboEventIds eventid)
        {
            return base.IsEnabled( (int)eventid );
        }
		
		// SSP 10/29/02 UWG1797
		// Added SetEnabled method.
		//
		/// <summary>
		/// Sets a specific event to enabled or disabled
		/// </summary>
        /// <param name="eventid">The id of the event.</param>
        /// <param name="enabled">True to enable the firing of the events, false to disable.</param>
		public void SetEnabled( ComboEventIds eventid, bool enabled )
		{
			base.SetEnabled( (int)eventid, enabled );
		}
	}
}
