#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;
using System.Collections.Generic;
using Infragistics.Win.Layout;

namespace Infragistics.Win.UltraWinGrid
{
	#region EmptyRowSettings Class

	/// <summary>
	/// Exposes properties related to empty rows functionality.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridLayout.EmptyRowSettings"/>
	/// <seealso cref="EmptyRowSettings.ShowEmptyRows"/>
	/// </remarks>
	[ Serializable( ), TypeConverter( typeof( ExpandableObjectConverter ) ) ]
	public class EmptyRowSettings : SubObjectBase, ISerializable, ISupportPresets
	{
		#region AppearanceIndex

		// The AppearanceIndex enum specifies the index value for
		// each of the appearance objects in the appearanceHolders array
		//
		private enum AppearanceIndex
		{
			Cell = 0,
			EmptyArea = 1,
			Row = 2,
			RowSelector = 3,
			LastItem = 3
		};

		// The follwowing array lists the propertyids of the apperance
		// objects in the appearanceHolder array. These ids MUST be ordered
		// the same as the AppearanceIndex listed above
		//
		private static readonly PropertyIds [] AppearancePropIds = new PropertyIds[]
		{
			PropertyIds.CellAppearance,
			PropertyIds.EmptyAreaAppearance,			
			PropertyIds.RowAppearance,
			PropertyIds.RowSelectorAppearance
		};

		#endregion // AppearanceIndex

		#region Private Vars

		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// 
		private AppearanceManager appearanceManager = null;
		private UltraGridLayout layout = null;

		private const EmptyRowStyle DEFAULT_EMPTYROWSTYLE = EmptyRowStyle.ExtendFirstCell;
		private EmptyRowStyle emptyRowStyle = DEFAULT_EMPTYROWSTYLE;
		private bool showEmptyRows = false;
		private UltraGridEmptyRow firstEmptyRow = null;
		private UltraGridEmptyRow emptyRow = null;

		#endregion // Private Vars

		#region Constructor
		
		internal EmptyRowSettings( UltraGridLayout layout )
		{
			this.Initialize( layout );
		}

		internal EmptyRowSettings( SerializationInfo info, StreamingContext context )
		{
			// Since we're not saving properties with default values, we must set the default here.
			//
			this.Reset();

			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "Tag":
						this.DeserializeTag( entry );
						break;
					case "ShowEmptyRows":
						this.showEmptyRows = (bool)Utils.DeserializeProperty( entry, typeof( bool ), this.showEmptyRows );
						break;
					case "Style":
						this.emptyRowStyle = (EmptyRowStyle)Utils.DeserializeProperty( entry, typeof( EmptyRowStyle ), this.emptyRowStyle );
						break;
					case "AppearanceManager":
						this.appearanceManager = (AppearanceManager)Utils.DeserializeProperty( entry, typeof( AppearanceManager ), this.appearanceManager );
						break;
					default:
						Debug.Assert( false, "Unknown property serialized" );
						break;
				}
			}
		}

		#endregion // Constructor

		#region Private/Internal Properties/Methods

		#region OnDeserializationComplete

		internal void OnDeserializationComplete( UltraGridLayout layout )
		{
			this.Initialize( layout );

			if ( null != this.appearanceManager )
				this.appearanceManager.OnDeserializationComplete( this.layout, 1 + (int)AppearanceIndex.LastItem, EmptyRowSettings.AppearancePropIds );
		}

		#endregion // OnDeserializationComplete

		#region Initialize

		private void Initialize( UltraGridLayout layout )
		{
			this.layout = layout;
		}

		#endregion // Initialize

		#region Layout

		internal UltraGridLayout Layout
		{
			get
			{
				return this.layout;
			}
		}

		#endregion // Layout

		#region AppearanceManager

		internal AppearanceManager AppearanceManager
		{
			get
			{
				if ( null == this.appearanceManager )
				{
					this.appearanceManager = new AppearanceManager( this.layout, 
						1 + (int)AppearanceIndex.LastItem, EmptyRowSettings.AppearancePropIds );

					this.appearanceManager.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.appearanceManager;
			}
		}

		#endregion // AppearanceManager

		#region FirstEmptyRow

		internal UltraGridEmptyRow FirstEmptyRow
		{
			get
			{
				// SSP 12/9/05 BR07929 BR08230
				// 
				//if ( null == this.firstEmptyRow )
				if ( null == this.firstEmptyRow || this.Layout.Rows != this.firstEmptyRow.ParentCollection )
					this.firstEmptyRow = new UltraGridEmptyRow( this.Layout.Rows );

				return this.firstEmptyRow;
			}
		}

		#endregion // FirstEmptyRow

		#region EmptyRow

		internal UltraGridEmptyRow EmptyRow
		{
			get
			{
				// SSP 12/9/05 BR07929 BR08230
				// 
				//if ( null == this.emptyRow )
				if ( null == this.emptyRow || this.Layout.Rows != this.emptyRow.ParentCollection )
					this.emptyRow = new UltraGridEmptyRow( this.Layout.Rows );

				return this.emptyRow;
			}
		}

		#endregion // EmptyRow

		#region ShowEmptyRowsResolved

		internal bool ShowEmptyRowsResolved
		{
			get
			{
				return this.ShowEmptyRows && ! this.EmptyRow.IsCard;
			}
		}

		#endregion // ShowEmptyRowsResolved

		#region ResolveAppearanceHelper

		// SSP 3/13/06 - App Styling
		// Added ResolveAppearanceHelper method.
		// 
		private void ResolveAppearanceHelper( AppearanceIndex index, ref AppearanceData appData, 
			ref ResolveAppearanceContext context, AppStyling.RoleState roleState )
		{
			if ( context.ResolutionOrder.UseStyleBefore )
				StyleUtils.ResolveAppearance( roleState, ref appData, ref context );

			if ( context.ResolutionOrder.UseControlInfo )
				this.AppearanceManager.ResolveAppearance( (int)index, ref appData, ref context.UnresolvedProps );

			if ( context.ResolutionOrder.UseStyleAfter )
				StyleUtils.ResolveAppearance( roleState, ref appData, ref context );
		}

		#endregion // ResolveAppearanceHelper

		#region ResolveCellAppearance

		internal void ResolveCellAppearance( ref AppearanceData appData, ref ResolveAppearanceContext context )
		{
			this.ResolveAppearanceHelper( AppearanceIndex.Cell, ref appData, ref context, AppStyling.RoleState.EmptyRow );
		}

		#endregion // ResolveCellAppearance

		#region ResolveRowAppearance

		internal void ResolveRowAppearance( ref AppearanceData appData, ref ResolveAppearanceContext context )
		{
			this.ResolveAppearanceHelper( AppearanceIndex.Row, ref appData, ref context, AppStyling.RoleState.EmptyRow );
		}

		#endregion // ResolveRowAppearance

		#region ResolveRowSelectorAppearance

		internal void ResolveRowSelectorAppearance( ref AppearanceData appData, ref ResolveAppearanceContext context )
		{
			this.ResolveAppearanceHelper( AppearanceIndex.RowSelector, ref appData, ref context, AppStyling.RoleState.EmptyRow );
		}

		#endregion // ResolveRowSelectorAppearance

		#region ResolveEmptyAreaAppearance

		internal void ResolveEmptyAreaAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof( EmptyRowsAreaUIElement ), flags );
			context.Role = StyleUtils.GetRole( this.Layout, StyleUtils.Role.EmptyRowsArea, out context.ResolutionOrder );

			this.ResolveAppearanceHelper( AppearanceIndex.EmptyArea, ref appData, ref context, AppStyling.RoleState.Normal );

			flags = context.UnresolvedProps;

			// SSP 2/21/06 BR09319
			// Don't pick up the layout's back color setting for the empty row area. Default it to
			// Transparent.
			// 
			if ( 0 != ( AppearancePropFlags.BackColor & flags ) )
			{
				appData.BackColor = Color.Transparent;
				flags ^= AppearancePropFlags.BackColor;
			}
		}

		#endregion // ResolveEmptyAreaAppearance

		#region SSP 3/8/06 - App Styling - Commented Out Resolve methods

        

		#endregion // SSP 3/8/06 - App Styling - Commented Out Resolve methods

		#endregion // Private/Internal Properties/Methods

		#region Public Properties

		#region ShowEmptyRows

		/// <summary>
		/// Specifies whether to fill the empty area after the last row in the 
		/// UltraGrid with empty rows.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Style"/>
		/// <seealso cref="UltraGridLayout.EmptyRowSettings"/>
		/// </remarks>
		[ LocalizedDescription("LDR_EmptyRowSettings_P_ShowEmptyRows")]
		[ LocalizedCategory("LC_Appearance") ]
		public bool ShowEmptyRows
		{
			get
			{
				return this.showEmptyRows;
			}
			set
			{
				if ( value != this.showEmptyRows )
				{
					this.showEmptyRows = value;

					this.NotifyPropChange( PropertyIds.ShowEmptyRows );
				}
			}
		}

		#endregion // ShowEmptyRows

		#region ShouldSerializeShowEmptyRows

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeShowEmptyRows( )
		{
			return this.showEmptyRows;
		}

		#endregion // ShouldSerializeShowEmptyRows

		#region ResetShowEmptyRows

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetShowEmptyRows( )
		{
			this.ShowEmptyRows = false;
		}

		#endregion // ResetShowEmptyRows

		#region Style

		/// <summary>
		/// Specifies if and how to extend the empty rows all the way to the left so they 
		/// are aligned with the left edge of the scroll region (grid). The default is
		/// <b>ExtendFirstCell</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>Style</b> specifies if and how to extend the empty rows all the way to 
		/// the left so they are aligned with the left edge of the scroll region (grid).
		/// The default is <b>ExtendFirstCell</b>.
		/// </p>
		/// <seealso cref="ShowEmptyRows"/>
		/// <seealso cref="EmptyRowStyle"/>
		/// </remarks>
		[ LocalizedDescription("LDR_EmptyRowSettings_P_Style")]
		[ LocalizedCategory("LC_Appearance") ]
		public EmptyRowStyle Style
		{
			get
			{
				return this.emptyRowStyle;
			}
			set
			{
				if ( value != this.emptyRowStyle )
				{
					GridUtils.ValidateEnum( typeof( EmptyRowStyle ), value );

					this.emptyRowStyle = value;

					this.NotifyPropChange( PropertyIds.Style );
				}
			}
		}

		#endregion // Style

		#region CellAppearance

		/// <summary>
		/// Appearance applied to cells of empty rows.
		/// </summary>
		[ LocalizedDescription("LDR_EmptyRowSettings_P_CellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.AppearanceBase CellAppearance
		{
			get
			{
				return this.AppearanceManager.GetAppearance( (int)AppearanceIndex.Cell );
			}
			set
			{
				this.AppearanceManager.SetAppearance( (int)AppearanceIndex.Cell, value );
			}
		}

		#endregion // CellAppearance

		#region HasCellAppearance

		/// <summary>
		/// Returns true if an CellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCellAppearance
		{
			get
			{
				return this.AppearanceManager.HasAppearance( (int)AppearanceIndex.Cell );
			}
		}

		#endregion // HasCellAppearance

		#region EmptyAreaAppearance

		/// <summary>
		/// Appearance applied to cells of empty rows.
		/// </summary>
		[ LocalizedDescription("LDR_EmptyRowSettings_P_EmptyAreaAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.AppearanceBase EmptyAreaAppearance
		{
			get
			{
				return this.AppearanceManager.GetAppearance( (int)AppearanceIndex.EmptyArea );
			}
			set
			{
				this.AppearanceManager.SetAppearance( (int)AppearanceIndex.EmptyArea, value );
			}
		}

		#endregion // EmptyAreaAppearance

		#region HasEmptyAreaAppearance

		/// <summary>
		/// Returns true if an EmptyAreaAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasEmptyAreaAppearance
		{
			get
			{
				return this.AppearanceManager.HasAppearance( (int)AppearanceIndex.EmptyArea );
			}
		}

		#endregion // HasEmptyAreaAppearance 

		#region RowAppearance

		/// <summary>
		/// Appearance applied to empty rows.
		/// </summary>
		[ LocalizedDescription("LDR_EmptyRowSettings_P_RowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.AppearanceBase RowAppearance
		{
			get
			{
				return this.AppearanceManager.GetAppearance( (int)AppearanceIndex.Row );
			}
			set
			{
				this.AppearanceManager.SetAppearance( (int)AppearanceIndex.Row, value );
			}
		}

		#endregion // RowAppearance

		#region HasRowAppearance

		/// <summary>
		/// Returns true if an RowAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasRowAppearance
		{
			get
			{
				return this.AppearanceManager.HasAppearance( (int)AppearanceIndex.Row );
			}
		}

		#endregion // HasRowAppearance

		#region RowSelectorAppearance

		/// <summary>
		/// Appearance applied to row selectors of empty rows.
		/// </summary>
		[ LocalizedDescription("LDR_EmptyRowSettings_P_RowSelectorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.AppearanceBase RowSelectorAppearance
		{
			get
			{
				return this.AppearanceManager.GetAppearance( (int)AppearanceIndex.RowSelector );
			}
			set
			{
				this.AppearanceManager.SetAppearance( (int)AppearanceIndex.RowSelector, value );
			}
		}

		#endregion // RowSelectorAppearance

		#region HasRowSelectorAppearance

		/// <summary>
		/// Returns true if an RowSelectorAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasRowSelectorAppearance
		{
			get
			{
				return this.AppearanceManager.HasAppearance( (int)AppearanceIndex.RowSelector );
			}
		}

		#endregion // HasRowSelectorAppearance

		#endregion // Public Properties

		#region Protected/Public Methods

		#region ShouldSerializeCellAppearance

		/// <summary>
		/// Returns true if the appearance object needs to be persisted.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCellAppearance( )
		{
			return this.AppearanceManager.ShouldSerializeAppearance( (int)AppearanceIndex.Cell );
		}

		#endregion // ShouldSerializeCellAppearance

		#region ResetCellAppearance

		/// <summary>
		/// Resets the appearance.
		/// </summary>
		public void ResetCellAppearance( )
		{
			this.AppearanceManager.ResetAppearance( (int)AppearanceIndex.Cell );
		}

		#endregion // ResetCellAppearance

		#region ShouldSerializeEmptyAreaAppearance

		/// <summary>
		/// Returns true if the appearance object needs to be persisted.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeEmptyAreaAppearance( )
		{
			return this.AppearanceManager.ShouldSerializeAppearance( (int)AppearanceIndex.EmptyArea );
		}

		#endregion // ShouldSerializeEmptyAreaAppearance

		#region ResetEmptyAreaAppearance

		/// <summary>
		/// Resets the appearance.
		/// </summary>
		public void ResetEmptyAreaAppearance( )
		{
			this.AppearanceManager.ResetAppearance( (int)AppearanceIndex.EmptyArea );
		}

		#endregion // ResetEmptyAreaAppearance

		#region ShouldSerializeRowAppearance

		/// <summary>
		/// Returns true if the appearance object needs to be persisted.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowAppearance( )
		{
			return this.AppearanceManager.ShouldSerializeAppearance( (int)AppearanceIndex.Row );
		}

		#endregion // ShouldSerializeRowAppearance

		#region ResetRowAppearance

		/// <summary>
		/// Resets the appearance.
		/// </summary>
		public void ResetRowAppearance( )
		{
			this.AppearanceManager.ResetAppearance( (int)AppearanceIndex.Row );
		}

		#endregion // ResetRowAppearance

		#region ShouldSerializeRowSelectorAppearance

		/// <summary>
		/// Returns true if the appearance object needs to be persisted.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowSelectorAppearance( )
		{
			return this.AppearanceManager.ShouldSerializeAppearance( (int)AppearanceIndex.RowSelector );
		}

		#endregion // ShouldSerializeRowSelectorAppearance

		#region ResetRowSelectorAppearance

		/// <summary>
		/// Resets the appearance.
		/// </summary>
		public void ResetRowSelectorAppearance( )
		{
			this.AppearanceManager.ResetAppearance( (int)AppearanceIndex.RowSelector );
		}

		#endregion // ResetRowSelectorAppearance

		#region ShouldSerializeStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeStyle( )
		{
			return DEFAULT_EMPTYROWSTYLE != this.emptyRowStyle;
		}

		#endregion // ShouldSerializeStyle

		#region ResetStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetStyle( )
		{
			this.Style = DEFAULT_EMPTYROWSTYLE;
		}

		#endregion // ResetStyle

		#region ShouldSerialize

		/// <summary>
		/// Returns true if the object needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerialize( )
		{
			return this.ShouldSerializeShowEmptyRows( )
				|| this.ShouldSerializeStyle( )
				|| null != this.appearanceManager && this.appearanceManager.ShouldSerialize( );
		}

		#endregion // ShouldSerialize

		#region Reset

		/// <summary>
		/// Resets all the properties to their default values.
		/// </summary>
		public void Reset( )
		{
			this.ResetShowEmptyRows( );
			this.ResetStyle( );

			if ( null != this.appearanceManager )
				this.appearanceManager.Reset( );
		}

		#endregion // Reset

		#region InitializeFrom

		internal void InitializeFrom( EmptyRowSettings source )
		{
			this.showEmptyRows = source.showEmptyRows;
			this.emptyRowStyle = source.emptyRowStyle;
			
			if ( null != source.appearanceManager )
				this.AppearanceManager.InitializeFrom( source.appearanceManager );
			else if ( null != this.appearanceManager )
				this.appearanceManager.ResetAppearances( );
		}

		#endregion // InitializeFrom

		#region OnDispose

		/// <summary>
		/// Called when this object is Disposed of
		/// </summary>
		protected override void OnDispose( )
		{
			if ( null != this.appearanceManager )
			{
				this.appearanceManager.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.appearanceManager.Dispose( );
				this.appearanceManager = null;
			}
		}

		#endregion // OnDispose

		#region OnSubObjectPropChanged

		/// <summary>
		/// Called when another sub object that we are listening to notifies
		/// us that one of its properties has changed.
		/// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange )
		{
			if ( this.appearanceManager == propChange.Source )
				this.NotifyPropChange( propChange.PropId, propChange.Trigger );
			else 
				this.NotifyPropChange( PropertyIds.EmptyRowSettings, propChange );
		}

		#endregion // OnSubObjectPropChanged

		#endregion // Protected/Public Methods

		#region Implementation of ISerializable

        // MRS 12/19/06 - fxCop        
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
		{
			this.SerializeTag( info, "Tag" );

			if(	this.ShouldSerializeShowEmptyRows( ) )
			{
				Utils.SerializeProperty( info, "ShowEmptyRows", this.showEmptyRows );
			}

			if(	this.ShouldSerializeStyle( ) )
			{
				Utils.SerializeProperty( info, "Style", this.emptyRowStyle );
			}

			if ( null != this.appearanceManager && this.appearanceManager.ShouldSerializeAppearances( ) )
				Utils.SerializeProperty( info, "AppearanceManager", this.appearanceManager );
		}

		#endregion // Implementation of ISerializable

		#region Implementation of ISupportPresets

		/// <summary>
		/// Returns a list of properties which can be used in a Preset
		/// </summary>
		/// <param name="presetType">Determines which type(s) of properties are returned</param>
		/// <returns>An array of strings indicating property names</returns>
		string[] ISupportPresets.GetPresetProperties(Infragistics.Win.PresetType presetType)
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList properties = new ArrayList();
			List<string> properties = new List<string>();

			//Appearance
			if ((presetType & Infragistics.Win.PresetType.Appearance) == Infragistics.Win.PresetType.Appearance)
			{
				properties.Add( "CellAppearance" );
				properties.Add( "EmptyAreaAppearance" );
				properties.Add( "RowAppearance" );
				properties.Add( "RowSelectorAppearance" );
			}

			//Behavior
			if ((presetType & Infragistics.Win.PresetType.Behavior) == Infragistics.Win.PresetType.Behavior)
			{
				properties.Add( "ShowEmptyRows" );
				properties.Add( "Style" );
			}
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (string[])properties.ToArray(typeof(string));
			return properties.ToArray();
		}

		/// <summary>
		/// Returns the TypeName of the Preset target
		/// </summary>
		/// <returns>Returns "UltraGridOverride"</returns>
		string ISupportPresets.GetPresetTargetTypeName()
		{
			return "EmptyRowSettings";
		}

		#endregion Implementation of ISupportPresets
	}

	#endregion // EmptyRowSettings Class

	#region UltraGridEmptyRow Class

	/// <summary>
	/// Represents empty rows. <see cref="UltraGridLayout.EmptyRowSettings"/> for more information on
	/// how to enable the Empty Rows Functionality.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// <b>UltraGridEmptyRow</b> instances represent empty rows. <see cref="UltraGridLayout.EmptyRowSettings"/> 
	/// for more information on how to enable the Empty Rows Functionality.
	/// </p>
    /// <seealso cref="UltraGridLayout.EmptyRowSettings"/> <seealso cref="Infragistics.Win.UltraWinGrid.EmptyRowSettings.ShowEmptyRows"/>
	/// </remarks>
	public class UltraGridEmptyRow : UltraGridSpecialRowBase
	{
		#region Constructor
		
		internal UltraGridEmptyRow( RowsCollection parentCollection ) : base( parentCollection )
		{
		}

		#endregion // Constructor

		#region Private/Internal Properties/Methods

		#region EmptyRowSettings

		internal EmptyRowSettings EmptyRowSettings
		{
			get
			{
				return this.Layout.EmptyRowSettings;
			}
		}

		#endregion // EmptyRowSettings

		#region GetExtents

		internal void GetExtents( out int preRowAreaExtent, out int rowSelectorExtent, out int groupByRowsHierarchyIndent, out int offsetCellsBy )
		{
			this.GetExtents( this.EmptyRowSettings.Style, out preRowAreaExtent, out rowSelectorExtent, out groupByRowsHierarchyIndent, out offsetCellsBy );
		}

		internal void GetExtents( EmptyRowStyle emptyRowStyle, 
			out int preRowAreaExtent, out int rowSelectorExtent, out int groupByRowsHierarchyIndent, out int offsetCellsBy )
		{
			preRowAreaExtent = base.PreRowAreaExtentResolved;
			rowSelectorExtent = base.RowSelectorExtentResolved;
			groupByRowsHierarchyIndent = 0;
			offsetCellsBy = 0;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//bool rowBordersMergeable = this.Layout.CanMergeAdjacentBorders( this.BandInternal.BorderStyleRowResolved );

			switch ( this.EmptyRowSettings.Style )
			{
				case EmptyRowStyle.ExtendFirstCell:
				case EmptyRowStyle.ExtendRowSelector:
				case EmptyRowStyle.PrefixWithEmptyCell:
					if ( this.ParentCollection.IsGroupByRows )
						groupByRowsHierarchyIndent = - this.BandInternal.CalcGroupByRowIndent( this.ParentCollection );
					break;
			}

			switch ( emptyRowStyle )
			{
				case EmptyRowStyle.ExtendRowSelector:
					rowSelectorExtent += preRowAreaExtent + groupByRowsHierarchyIndent;
					preRowAreaExtent = 0;
					break;
				case EmptyRowStyle.HideRowSelector:
					offsetCellsBy = rowSelectorExtent;
					rowSelectorExtent = 0;
					break;
				case EmptyRowStyle.ExtendFirstCell:
				case EmptyRowStyle.PrefixWithEmptyCell:
					offsetCellsBy = preRowAreaExtent + rowSelectorExtent + groupByRowsHierarchyIndent;
					preRowAreaExtent = 0;
					rowSelectorExtent = 0;
					break;
			}
		}

		#endregion // GetExtents

		#region GroupByRowsHierarchyIndent

		internal int GroupByRowsHierarchyIndent
		{
			get
			{
				int t1, t2, indent, t3;
				this.GetExtents( out t1, out t2, out indent, out t3 );

				return indent;
			}
		}

		#endregion // GroupByRowsHierarchyIndent

		#region BeginCellsAt
		
		internal int BeginCellsAt
		{
			get
			{
				int t1, t2, t3, offsetCellsBy;
				this.GetExtents( out t1, out t2, out t3, out offsetCellsBy );

				return offsetCellsBy;
			}
		}

		#endregion // BeginCellsAt

		#endregion // Private/Internal Properties/Methods

		#region Base Overrides

		#region ActivationResolved
		
		internal override Activation ActivationResolved
		{
			get
			{
				return Activation.Disabled;
			}
		}

		#endregion // ActivationResolved

		#region IsUIElementCompatible
		
		internal override bool IsUIElementCompatible( UIElement parent, UIElement elem )
		{
			return elem is EmptyRowUIElement;
		}

		#endregion // IsUIElementCompatible

		#region HasSameContext

		internal override bool HasSameContext( UIElement parent, UIElement elem )
		{
			EmptyRowUIElement emptyRowElem = elem as EmptyRowUIElement;
			return null != emptyRowElem && this == emptyRowElem.Row;
		}

		#endregion // HasSameContext

		#region HasExpansionIndicator

		/// <summary>
		/// Overridden. Always returns false since empty rows can not be expanded.
		/// </summary>
		public override bool HasExpansionIndicator
		{
			get
			{
				return false;
			}
		}

		#endregion // HasExpansionIndicator

		#region CreateRowUIElement
		
		internal override UIElement CreateRowUIElement( UIElement parent )
		{
			return new EmptyRowUIElement( parent );
		}

		#endregion // CreateRowUIElement

		#region InitializeUIElement

		internal override void InitializeUIElement( UIElement elem )
		{
			EmptyRowUIElement rowElem = (EmptyRowUIElement)elem;
			rowElem.InitializeRow( this );
		}

		#endregion // InitializeUIElement

		#region HasHeadersOverride
		
		internal override DefaultableBoolean HasHeadersOverride
		{
			get
			{
				// Empty rows never have headers.
				// 
				return DefaultableBoolean.False;
			}
		}

		#endregion // HasHeadersOverride
		
		#region HasSpecialRowSeparatorBefore

		internal override bool HasSpecialRowSeparatorBefore
		{
			get
			{
				return 0 != ( SpecialRowSeparator.EmptyRows & this.BandInternal.GetSpecialRowSeparatorResolved( this ) )
					&& this == this.EmptyRowSettings.FirstEmptyRow;
			}
		}

		#endregion // HasSpecialRowSeparatorBefore

		#region HasSpecialRowSeparatorAfter

		internal override bool HasSpecialRowSeparatorAfter
		{
			get
			{
				return false;
			}
		}

		#endregion // HasSpecialRowSeparatorAfter
		
		#region AfterElementAdded

		internal override void AfterElementAdded( UIElement addedRowElem, UIElementsCollection oldElems, ref Rectangle actualRectOccupiedByRowRelatedElements )
		{
			// Call base implementation to add the row separators.
			//
			base.AfterElementAdded( addedRowElem, oldElems, ref actualRectOccupiedByRowRelatedElements );
		}

		#endregion // AfterElementAdded

		#region IsFixedHeight

		internal override bool IsFixedHeight
		{
			get
			{
				return true;
			}
		}

		#endregion // IsFixedHeight
		
		#region IsEmptyRow

		// SSP 7/7/05 - NAS 5.3 Empty Rows Feature
		// 
		/// <summary>
		/// Overridden. Always returns true since this is <b>UltraGridEmptyRow</b>.
		/// </summary>
		/// <remarks>
        /// <seealso cref="UltraGridEmptyRow"/> <seealso cref="UltraGridLayout.EmptyRowSettings"/> <seealso cref="Infragistics.Win.UltraWinGrid.EmptyRowSettings.ShowEmptyRows"/>
		/// </remarks>
		public override bool IsEmptyRow
		{
			get
			{
				return true;
			}
		}

		#endregion // IsEmptyRow

		#region AdjustUIElementRect
		
		internal override void AdjustUIElementRect( ref Rectangle elemRect )
		{
			int indent = this.GroupByRowsHierarchyIndent;
			if ( 0 != indent )
			{
				elemRect.X -= indent;
				elemRect.Width += indent;
			}
		}

		#endregion // AdjustUIElementRect

		#region AdjustCellUIElementRect
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool IsFirstItemHelper( UltraGridColumn column )
		private static bool IsFirstItemHelper( UltraGridColumn column )
		{
			bool firstItem;
			if ( column.Band.GroupsDisplayed )
			{
				// SSP 10/20/05 BR06693
				// 
				//firstItem = column.FirstItemInLevel;
				// MD 1/21/09 - Groups in RowLayouts
				// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
				// Also, the logic has to be changed slightly, because a column with a null group can be first in GroupLayout style.
				//firstItem = column.FirstItem && null != column.Group && column.Group.FirstItem;
				firstItem = column.FirstItem;
				if ( firstItem )
				{
					UltraGridGroup group = column.GroupResolved;

					if ( group == null )
					{
						if ( column.Band.RowLayoutStyle == RowLayoutStyle.None )
							firstItem = false;
					}
					else
					{
						firstItem = group.FirstItem;
					}
				}
			}
			else if ( column.Band.UseRowLayoutResolved )
				firstItem = 0 == column.RowLayoutColumnInfo.OriginXResolved;
			else 
				firstItem = column.FirstItem;

			return firstItem;
		}
		
		internal override void AdjustCellUIElementRect( RowCellAreaUIElementBase rowCellAreaElem, UltraGridColumn column, ref Rectangle cellElemRect )
		{
			EmptyRowStyle emptyRowStyle =  this.EmptyRowSettings.Style;
			if ( EmptyRowStyle.ExtendFirstCell == emptyRowStyle || EmptyRowStyle.HideRowSelector == emptyRowStyle )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( this.IsFirstItemHelper( column ) )
				if ( UltraGridEmptyRow.IsFirstItemHelper( column ) )
				{
					// Extend leftmost cells according to the empty row style.
					// 
					int delta = cellElemRect.X - rowCellAreaElem.Rect.X;
					if ( delta > 0 )
					{
						cellElemRect.X -= delta;
						cellElemRect.Width += delta;
					}
				}
			}
			else if ( EmptyRowStyle.PrefixWithEmptyCell == emptyRowStyle )
			{
				// Make sure that the left border of the first cell is aligned with the left borders
				// of the first cells of the data rows.
				// 
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( this.IsFirstItemHelper( column ) 
				if ( UltraGridEmptyRow.IsFirstItemHelper( column ) 
					&& ! this.Band.UseRowLayoutResolved 
					&& this.Layout.CanMergeAdjacentBorders( this.BandInternal.BorderStyleRowResolved ) )
				{
					int delta = 1;
					cellElemRect.X -= delta;
					cellElemRect.Width += delta;
				}
			}
		}

		#endregion // AdjustCellUIElementRect

		#region PreRowAreaExtentResolved

		internal override int PreRowAreaExtentResolved
		{
			get
			{
				int preRowAreaExtent, t1, t2, t3;
				this.GetExtents( out preRowAreaExtent, out t1, out t2, out t3 );

				return preRowAreaExtent;
			}
		}

		#endregion // PreRowAreaExtentResolved

		#region RowSelectorExtentResolved

		internal override int RowSelectorExtentResolved
		{
			get
			{
				int t1, rowSelectorExtent, t2, t3;
				this.GetExtents( out t1, out rowSelectorExtent, out t2, out t3 );

				return rowSelectorExtent;

			}
		}

		#endregion // RowSelectorExtentResolved

		#endregion // Base Overrides

	}

	#endregion // UltraGridEmptyRow Class
}
