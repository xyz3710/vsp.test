#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    ///    Summary description for KeyActionMappings.
    /// </summary>
    public class ComboKeyActionMappings : KeyActionMappingsBase
    {
		/// <summary>
		/// Constructor
		/// </summary>
        public ComboKeyActionMappings( )
            : base( 75 )
        {
        }

		/// <summary>
		/// Loads this key action mappings collection with all the actions so that we 
		/// </summary>
		internal void InternalLoadAllActions( )
		{
			ComboKeyActionMapping [] defaultMappings =
				new ComboKeyActionMapping [] 
			{
				//							KeyCode          Action                             Disallowed state                Required state                  Disallowed special keys     Required special keys
				//							---------------  ---------------------------------  ------------------------------  ------------------------------  --------------------------  -----------------------
				new ComboKeyActionMapping( Keys.Up,          UltraComboAction.PrevRow,			0,								UltraComboState.Row,			0,            0 ),
				new ComboKeyActionMapping( Keys.Down,        UltraComboAction.NextRow,			0,								UltraComboState.Row,            0,            0 ),
				new ComboKeyActionMapping( Keys.Up,          UltraComboAction.FirstRow,			UltraComboState.Row,			0,								0,            0 ),
				//	BF 8.12.02	UWG1283
				//	A required state of  'Row' (there must be an ActiveRow) is actually
				//	not what we want here;when selecting the
				//	first row with the right or left arrow key, it is actually required
				//	that there is NO ActiveRow.
				//
				//new ComboKeyActionMapping( Keys.Right,       UltraComboAction.FirstRow,			UltraComboState.HasEdit | UltraComboState.Row,		UltraComboState.Row,            SpecialKeys.Alt,            0 ),
				//new ComboKeyActionMapping( Keys.Left,        UltraComboAction.FirstRow,			UltraComboState.HasEdit | UltraComboState.Row,		UltraComboState.Row,            SpecialKeys.Alt,            0 ),
				new ComboKeyActionMapping( Keys.Right,       UltraComboAction.FirstRow,			UltraComboState.HasEdit | UltraComboState.Row,		0,          0,            0 ),

				new ComboKeyActionMapping( Keys.Home,        UltraComboAction.FirstRow,			0,								UltraComboState.IsDroppedDown,	0,            0 ),
				new ComboKeyActionMapping( Keys.End,         UltraComboAction.LastRow,			0,								UltraComboState.IsDroppedDown,	0,            0 ),
				new ComboKeyActionMapping( Keys.Right,       UltraComboAction.NextRow,			UltraComboState.HasEdit,		UltraComboState.Row,            0,            0 ),
				new ComboKeyActionMapping( Keys.Left,        UltraComboAction.PrevRow,			UltraComboState.HasEdit,		UltraComboState.Row,            0,            0 ),
				new ComboKeyActionMapping( Keys.Prior,       UltraComboAction.PageUp,			0,								UltraComboState.Row,            0,            0 ),
				new ComboKeyActionMapping( Keys.Next,        UltraComboAction.PageDown,			0,								UltraComboState.Row,            0,            0 ),
				new ComboKeyActionMapping( Keys.Escape,      UltraComboAction.CloseDropdown,	0,                              UltraComboState.IsDroppedDown,  0,            0 ),
				new ComboKeyActionMapping( Keys.F4,          UltraComboAction.ToggleDropdown,	0,                              0,								0,            0 )
			};

			this.Clear( );

			// loop over the default mappings array and add each one to the
			// collection
			//
			for( int i = 0; i < defaultMappings.Length; i++ )
			{
				this.Add( defaultMappings[i] );
			}
		}

        /// <summary>
        /// Called the first time GetActionMapping
        /// is called (enables lazy loading of mappings)
        /// </summary>
        public override void LoadDefaultMappings() 
        {
            ComboKeyActionMapping [] defaultMappings =
                new ComboKeyActionMapping [] 
            {
                //							KeyCode          Action                             Disallowed state                Required state                  Disallowed special keys     Required special keys
                //							---------------  ---------------------------------  ------------------------------  ------------------------------  --------------------------  -----------------------
                
                //  BF NA 9.1 - UltraCombo MultiSelect
                //  Added 'ValueProvidedByCheckedItemList' as a disallowed state.
                //new ComboKeyActionMapping( Keys.Up,          UltraComboAction.PrevRow,			0,								UltraComboState.Row,			SpecialKeys.Alt,            0 ),
                //new ComboKeyActionMapping( Keys.Down,        UltraComboAction.NextRow,			0,								UltraComboState.Row,            SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Up,          UltraComboAction.PrevRow,			UltraComboState.ValueProvidedByCheckedItemList,								UltraComboState.Row,			SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Down,        UltraComboAction.NextRow,			UltraComboState.ValueProvidedByCheckedItemList,								UltraComboState.Row,            SpecialKeys.Alt,            0 ),

                new ComboKeyActionMapping( Keys.Up,          UltraComboAction.FirstRow,			UltraComboState.Row,			0,								SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Down,        UltraComboAction.FirstRow,			UltraComboState.Row,			0,					            SpecialKeys.Alt,            0 ),
				//	BF 8.12.02	UWG1283
				//	A required state of  'Row' (there must be an ActiveRow) is actually
				//	not what we want here;when selecting the
				//	first row with the right or left arrow key, it is actually required
				//	that there is NO ActiveRow.
				//
                //new ComboKeyActionMapping( Keys.Right,       UltraComboAction.FirstRow,			UltraComboState.HasEdit | UltraComboState.Row,		UltraComboState.Row,            SpecialKeys.Alt,            0 ),
                //new ComboKeyActionMapping( Keys.Left,        UltraComboAction.FirstRow,			UltraComboState.HasEdit | UltraComboState.Row,		UltraComboState.Row,            SpecialKeys.Alt,            0 ),
				new ComboKeyActionMapping( Keys.Right,       UltraComboAction.FirstRow,			UltraComboState.HasEdit | UltraComboState.Row,		0,            SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Left,        UltraComboAction.FirstRow,			UltraComboState.HasEdit | UltraComboState.Row,		0,            SpecialKeys.Alt,            0 ),

                new ComboKeyActionMapping( Keys.Home,        UltraComboAction.FirstRow,			0,								UltraComboState.IsDroppedDown,	SpecialKeys.AltCtrl,        0 ),
                new ComboKeyActionMapping( Keys.End,         UltraComboAction.LastRow,			0,								UltraComboState.IsDroppedDown,	SpecialKeys.AltCtrl,        0 ),
                new ComboKeyActionMapping( Keys.Right,       UltraComboAction.NextRow,			UltraComboState.HasEdit,		UltraComboState.Row,            SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Left,        UltraComboAction.PrevRow,			UltraComboState.HasEdit,		UltraComboState.Row,            SpecialKeys.Alt,            0 ),
                
                //  BF NA 9.1 - UltraCombo MultiSelect
                //  Added 'ValueProvidedByCheckedItemList' as a disallowed state.
                //new ComboKeyActionMapping( Keys.Prior,       UltraComboAction.PageUp,			0,								UltraComboState.Row,            SpecialKeys.Alt,            0 ),
                //new ComboKeyActionMapping( Keys.Next,        UltraComboAction.PageDown,			0,								UltraComboState.Row,            SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Prior,       UltraComboAction.PageUp,			UltraComboState.ValueProvidedByCheckedItemList,								UltraComboState.Row,            SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Next,        UltraComboAction.PageDown,			UltraComboState.ValueProvidedByCheckedItemList,								UltraComboState.Row,            SpecialKeys.Alt,            0 ),
                
                new ComboKeyActionMapping( Keys.Escape,      UltraComboAction.CloseDropdown,	0,                              UltraComboState.IsDroppedDown,  SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Return,      UltraComboAction.CloseDropdown,	0,                              UltraComboState.IsDroppedDown,  SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.F4,          UltraComboAction.ToggleDropdown,	0,                              0,								SpecialKeys.Alt,            0 ),
                new ComboKeyActionMapping( Keys.Up,          UltraComboAction.ToggleDropdown,	0,                              0,								0,                          SpecialKeys.Alt ),
                new ComboKeyActionMapping( Keys.Down,        UltraComboAction.ToggleDropdown,	0,                              0,								0,                          SpecialKeys.Alt ),
            };

            // loop over the default mappings array and add each one to the
            // collection
            //
            for( int i = 0; i < defaultMappings.Length; i++ )
            {
                this.Add( defaultMappings[i] );
            }

        }

        
		/// <summary>
		/// indexer 
		/// </summary>
        public Infragistics.Win.UltraWinGrid.ComboKeyActionMapping this[ int index ] 
        {
            get
            {
 				// JJD 8/15/01 - uWG153
				// verify that the mappings are loaded
				//
				this.VerifyMappingsAreLoaded();
                
				return (ComboKeyActionMapping)base.GetItem( index );
            }
            set
            {
 				// JJD 8/15/01 - uWG153
				// verify that the mappings are loaded
				//
				this.VerifyMappingsAreLoaded();

				base.List[index] = value;
            }
        }
     
        
		/// <summary>
		/// IEnumerable Interface Implementation
		/// </summary>
        /// <returns>A type safe enumerator</returns>
        public ComboKeyActionMappingEnumerator GetEnumerator() // non-IEnumerable version
        {
			// JJD 8/15/01 - uWG153
			// verify that the mappings are loaded
			//
			this.VerifyMappingsAreLoaded();
            
			return new ComboKeyActionMappingEnumerator(this);
        }

		//DA 1.14.03 
		//IsActionAllowed has been moved down into ActionMappingsCollection
		//which is owned by KeyActionMappingsBase.
		//This allows ensures that regardless of how the end user
		//modifies the KeyActions, that the 'performable actions'
		//are not affected.

//		/// <summary>
//		/// Returns whether or not the specified action is allowed based on
//		///	CurrentState.
//		/// </summary>
//		/// <param name="actionCode"></param>
//		/// <param name="currentState"></param>
//		/// <returns></returns>
//		public bool IsActionAllowed( UltraComboAction actionCode, UltraComboState currentState )
//		{
// 			// JJD 8/15/01 - uWG153
//			// verify that the mappings are loaded
//			//
//			this.VerifyMappingsAreLoaded();
//
//			// loop over the mappings table looking to see if this action
//			// is allowed while we are in this state
//			//
//			for ( int i =0; i<this.Count; ++i )
//			{
//				
//				if ( this[i].ActionCode == actionCode )
//				{
//					// check for the required current state bits
//					//
//					if ( ( (int)currentState & (int)(this[i].StateRequired) ) != (int)this[i].StateRequired ) 
//						continue;
//
//					// check for the disallowed current state bits
//					//
//					if ( ( (int)currentState & (int)(this[i].StateDisallowed) ) != 0 ) 
//						continue;
//
//					return true;
//				}
//			}		
//
//			return false;
//		}

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.ComboKeyActionMapping[] array, int index)
		{
			this.VerifyMappingsAreLoaded();

			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo

 
		// Inner class implements IEnumerator interface
 
		/// <summary>
		///  Summary description for KeyActionMappingEnumerator
		/// </summary>
		public class ComboKeyActionMappingEnumerator: DisposableObjectEnumeratorBase
		{   
			/// <summary>
			/// Constructor
			/// </summary>
            /// <param name="mappings">The ComboKeyActionMappings collection to enumerate.</param>			
			public ComboKeyActionMappingEnumerator(ComboKeyActionMappings mappings)
				: base( mappings )
			{
			}
 
			/// <summary>
			/// non-IEnumerator version: type-safe
			/// </summary>
			public ComboKeyActionMapping Current 
			{
				get
				{
					return (ComboKeyActionMapping)((IEnumerator)this).Current;
				}
			}
		}


		
		#region ActionStateMappingsCollection Added 1.14.03

		
		/// <summary>
		/// Creates an instance of an ActionStateMappingsCollection derived class
		/// </summary>
        /// <returns>an ActionStateMappingsCollection</returns>
		protected override Infragistics.Win.KeyActionMappingsBase.ActionStateMappingsCollection CreateActionStateMappingsCollection()
		{
			return new ActionStateMappingsCollection(
			new ActionStateMapping[] {
										new ActionStateMapping(UltraComboAction.PrevRow, 0, (long)UltraComboState.Row),
										new ActionStateMapping(UltraComboAction.NextRow, 0, (long)UltraComboState.Row),
										// AS 5/21/04 UWG2990
									    // First and Last Row should not have any requirements.
										//
										//new ActionStateMapping(UltraComboAction.FirstRow, (long)UltraComboState.Row, 0),
										//new ActionStateMapping(UltraComboAction.LastRow, 0, (long)UltraComboState.IsDroppedDown),
										new ActionStateMapping(UltraComboAction.FirstRow, 0, 0),
										new ActionStateMapping(UltraComboAction.LastRow, 0, 0),
										
										
										new ActionStateMapping(UltraComboAction.PageUp, 0, (long)UltraComboState.Row),
										new ActionStateMapping(UltraComboAction.PageDown, 0, (long)UltraComboState.Row),
										new ActionStateMapping(UltraComboAction.CloseDropdown, 0, (long)UltraComboState.IsDroppedDown),
										new ActionStateMapping(UltraComboAction.ToggleDropdown, 0, 0)
										
										 //DA 1.31.03 Removed the following unnecesary mappings
										 //new ActionStateMapping(UltraComboAction.PrevRow, (long)UltraComboState.HasEdit, (long)UltraComboState.Row),
										//new ActionStateMapping(UltraComboAction.NextRow, (long)UltraComboState.HasEdit, (long)UltraComboState.Row),
									 }

			
			);

		}
		

		#endregion

	
	}
}
