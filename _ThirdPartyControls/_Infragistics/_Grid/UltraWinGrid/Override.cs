#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Security.Permissions;
	using Infragistics.Shared.Serialization;
	using System.Collections;
	using System.Collections.Generic;

	///	<summary>
	///	The UltraGridOverride class allows the setting of properties on multiple levels of the grid. 
	///	</summary>
	///	<remarks>
	///	<p class="body">The <b>Override</b> object has properties which define the look and behavior of Bands in the grid. </p>
	///	<p class="body">The <b>Override</b> property exists both on the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBase.DisplayLayout"/> and on the <see cref="UltraGridBand"/> object. This allows you to set properties that apply to all bands, and then override those property settings on each individual band. Some properties on the override also exist on the column and can be overriden on a finer level. For example:</p>
	///	<p class="body"><b>In VB:</b></p>
	///	<p class="code">Me.UltraGrid1.DisplayLayout.Override.CellAppearance.BackColor = Color.Blue</p>
	///	<p class="code">Me.UltraGrid1.DisplayLayout.Bands(1).Override.CellAppearance.BackColor = Color.White</p>
	///	<p class="code">Me.UltraGrid1.DisplayLayout.Bands(1).Columns(0).CellAppearance.BackColor = Color.Red</p>
	///	<p class="body"><b>In C#:</b></p>
	///	<p class="code">this.ultraGrid1.DisplayLayout.Override.CellAppearance.BackColor = Color.Blue;</p>
	///	<p class="code">this.ultraGrid1.DisplayLayout.Bands[1].Override.CellAppearance.BackColor = Color.White;</p>
	///	<p class="code">this.ultraGrid1.DisplayLayout.Bands[1].Columns[0].CellAppearance.BackColor = Color.Red;</p>
	///	<p class="body">In this case, the first line of code will affect all cells in the grid and set their BackColor to Blue. The second line set the BackColor of all cells in Band 1 to White. The third line sets all the cells of Column 0 in Band 1 to Red. In general, the smaller object will take precedence over a larger object. So since the column is the smallest (most limited) object, it's property settings take precedence over the Band Override. The band is smaller (more limited) that the DisplayLayout. So the Band Override settings take precedence over the DisplayLayout Override settings.</p>
	///	</remarks>
	[ 
	TypeConverter( typeof( UltraGridOverride.UltraGridOverrideTypeConverter ) ), 
	Serializable() 
	]
	public class UltraGridOverride : KeyedSubObjectBase,
		ISelectionStrategyProvider,
		ISerializable, 

		// MRS 2/23/04
		//Add ISupportPresets
		ISupportPresets
	{

		#region Private Members
		private Infragistics.Win.AppearanceHolder[]    appearanceHolders = null;

		private Infragistics.Win.UltraWinGrid.UltraGridLayout   layout;

		private AllowAddNew						allowAddNew;
		private AllowColMoving                  allowColMoving;
		private AllowColSizing                  allowColSizing;
		private AllowColSwapping                allowColSwapping;
		private DefaultableBoolean              allowDelete;
		private AllowGroupMoving                allowGroupMoving;
		private AllowGroupSwapping              allowGroupSwapping;
		private DefaultableBoolean              allowUpdate;
		private UIElementBorderStyle            borderStyleRow;
		private UIElementBorderStyle            borderStyleCell;
		//JM 01-16-02 The following is no longer needed - now using
		//			  BorderStyleRow for cards
		//private UIElementBorderStyle            borderStyleCard;
		private UIElementBorderStyle            borderStyleCardArea;
		private UIElementBorderStyle            borderStyleHeader;
		private CellClickAction                 cellClickAction;
		private DefaultableBoolean              cellMultiLine;
		private int                             cellPadding;
		private int                             cellSpacing;
		private int                             cardSpacing;
		private int                             defaultColWidth;
		private int                             defaultRowHeight;
		private ShowExpansionIndicator          expansionIndicator;
		//        private ExpandOnLoad                    expandRowsOnLoad;
		private HeaderClickAction               headerClickAction;
		private int                             maxSelectedCells;
		private int                             maxSelectedRows;
		private DefaultableBoolean              rowSelectors;
		private RowSizing                       rowSizing;
		private RowSizingArea                   rowSizingArea;
		private int                             rowSizingAutoMaxLines;
		private int                             rowSpacingBefore;
		private int                             rowSpacingAfter;
		private SelectType                      selectTypeCell;
		private SelectType                      selectTypeCol;
		private SelectType                      selectTypeRow;
		private TipStyle                        tipStyleCell;
		private TipStyle                        tipStyleRowConnector;
		private TipStyle                        tipStyleScroll;

		// SSP 3/3/06 BR10430
		// Added TipStyleHeader property.
		// 
		private TipStyle                        tipStyleHeader;

		private SelectionStrategyBase			selectionStrategyCell = null;
		private SelectionStrategyBase			selectionStrategyRow = null;
		private SelectionStrategyBase			selectionStrategyColumn = null;
		private SelectionStrategyBase			selectionStrategyGroupByRow = null;
		
		private DefaultableBoolean				allowGroupBy;
		private DefaultableBoolean				groupByColumnsHidden;		
		private	int								groupByRowPadding = -1;		
		private string							groupByRowDescriptionMask = null;
		private SelectType						selectTypeGroupByRow;

		// JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
		//
		private string							nullText = null;

		// SSP 3/21/02
		// Added vars for row filtering feature
		//
		private RowFilterMode			rowFilterMode;
		private DefaultableBoolean	allowRowFiltering;

		// SSP 4/23/02
		// Row summaries feature additions.
		//
		private AllowRowSummaries			allowRowSummaries;
		private UIElementBorderStyle		borderStyleSummaryFooter;
		private UIElementBorderStyle		borderStyleSummaryValue;
		private UIElementBorderStyle		borderStyleSummaryFooterCaption;

		// SSP 7/9/02 UWG1369
		// Moved SummaryFooterCaptionVisible property from the band to the override.
		//
		private DefaultableBoolean			summaryFooterCaptionVisible;

		// SSP 2/14/03 - RowSelectorWidth property
		// Added RowSelectorWidth property to allow the user to control the widths
		// of the row selectors.
		//
		private int rowSelectorWidth = 0;

		// SSP 2/28/03 - Row Layout Functionality
		// Added row selector border style because now since the column headers appear
		// inside rows with cells, they may want to change the header border style without
		// effecting the row selectors.
		//
		private UIElementBorderStyle borderStyleRowSelector;
		private RowLayoutSizing allowRowLayoutCellSizing;
		private RowLayoutSizing allowRowLayoutLabelSizing;
    
		// SSP 4/14/03
		// Added ColumnAutoSizeMode property.
		//
		private ColumnAutoSizeMode columnAutoSizeMode;

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		private Infragistics.Win.UltraWinGrid.CellDisplayStyle cellDisplayStyle;

		// SSP 5/20/03 - Fixed headers
		//
		private Color fixedCellSeparatorColor;
		private Infragistics.Win.UltraWinGrid.FixedHeaderIndicator fixedHeaderIndicator;

		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
		// appearances.
		//
		private RowFilterAction rowFilterAction;

		// SSP 11/7/03 - Add Row Feature
		//
		private int templateAddRowSpacingBefore;
		private int templateAddRowSpacingAfter;
		private UIElementBorderStyle borderStyleTemplateAddRow;

		// SSP 11/25/03 UWG2013 UWG2553
		// Added MinRowHeight property to Override to allow for unrestricted control over the 
		// row heights.
		//
		int minRowHeight;

		// SSP 12/12/03 DNF135
		// Added a way to control whether ink buttons get shown.
		// Added ShowInkButton property.
		//
		private ShowInkButton showInkButton;

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		// SSP 10/21/04
		// Initialize the var to Default, not False.
		//
		//private DefaultableBoolean showCalculatingText = DefaultableBoolean.False;
		private DefaultableBoolean showCalculatingText = DefaultableBoolean.Default;

		// SSP 10/17/04
		// Added FormulaRowIndexSource property on the override.
		//
		private FormulaRowIndexSource formulaRowIndexSource;

		// SSP 11/3/04 - Merged Cell Feature
		//
		private MergedCellStyle mergedCellStyle = MergedCellStyle.Default;
		private MergedCellContentArea mergedCellContentArea = MergedCellContentArea.Default;

		// SSP 11/16/04
		// Implemented column sizing using cells in non-rowlayout mode. 
		// Added ColumnSizingArea property.
		//
		private Infragistics.Win.UltraWinGrid.ColumnSizingArea columnSizingArea = ColumnSizingArea.Default;

		//JDN 11/19/04 Added RowSelectorHeader
		private RowSelectorHeaderStyle          rowSelectorHeaderStyle;

		// SSP 12/14/04 - IDataErrorInfo Support
		//
		private Infragistics.Win.UltraWinGrid.SupportDataErrorInfo supportDataErrorInfo = SupportDataErrorInfo.Default;
    
		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		private int                             groupByRowSpacingBefore;
		private int                             groupByRowSpacingAfter;

		// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		// ----------------------------------------------------------------------------------
		private UIElementBorderStyle borderStyleFilterCell = UIElementBorderStyle.Default;
		private UIElementBorderStyle borderStyleFilterOperator = UIElementBorderStyle.Default;
		private UIElementBorderStyle borderStyleFilterRow = UIElementBorderStyle.Default;
		private UIElementBorderStyle borderStyleSpecialRowSeparator = UIElementBorderStyle.Default;
		private FilterClearButtonLocation filterClearButtonLocation = FilterClearButtonLocation.Default;
		private FilterEvaluationTrigger filterEvaluationTrigger = FilterEvaluationTrigger.Default;
		private FilterOperandStyle filterOperandStyle = FilterOperandStyle.Default;
		private FilterOperatorDefaultValue filterOperatorDefaultValue = FilterOperatorDefaultValue.Default;
		private FilterOperatorDropDownItems filterOperatorDropDownItems = FilterOperatorDropDownItems.Default;
		private FilterOperatorLocation filterOperatorLocation = FilterOperatorLocation.Default;
		private int filterRowSpacingAfter = -1;
		private int filterRowSpacingBefore = -1;
		private FilterUIType filterUIType = FilterUIType.Default;
		private FixedRowIndicator fixedRowIndicator = FixedRowIndicator.Default;
		private int fixedRowsLimit = -1;
		private FixedRowSortOrder fixedRowSortOrder = FixedRowSortOrder.Default;
		private FixedRowStyle fixedRowStyle = FixedRowStyle.Default;
		private GroupBySummaryDisplayStyle groupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.Default;
		private int sequenceFilterRow = -1;
		private int sequenceFixedAddRow  = -1;
		private int sequenceSummaryRow   = -1;
		private SpecialRowSeparator specialRowSeparator = SpecialRowSeparator.Default;
		private int specialRowSeparatorHeight = -1;
		private SummaryDisplayAreas summaryDisplayArea = SummaryDisplayAreas.Default;
		private int summaryFooterSpacingAfter = -1;
		private int summaryFooterSpacingBefore = -1;
		private string filterRowPrompt = null;
		private string templateAddRowPrompt = null;
		private FilterComparisonType filterComparisonType = FilterComparisonType.Default;
		// ----------------------------------------------------------------------------------

		// JAS 2005 v2 GroupBy Row Extensions
		//
		// ----------------------------------------------------------------------------------
		private GroupByRowExpansionStyle groupByRowExpansionStyle = GroupByRowExpansionStyle.ExpansionIndicatorAndDoubleClick;
		private GroupByRowInitialExpansionState groupByRowInitialExpansionState = GroupByRowInitialExpansionState.Default;
		// ----------------------------------------------------------------------------------

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
		//
		// ----------------------------------------------------------------------------------
		private HeaderStyle headerStyle		 = HeaderStyle.Default;
		private HeaderStyle rowSelectorStyle = HeaderStyle.Default;
		// ----------------------------------------------------------------------------------

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
		//
		private UIElementButtonStyle buttonStyle = UIElementButtonStyle.Default;

		// JAS v5.2 Wrapped Header Text 4/18/05
		//
		private DefaultableBoolean wrapHeaderText = DefaultableBoolean.Default;

		// SSP 3/30/05 NAS 5.2 Row Numbers
		//
		private RowSelectorNumberStyle rowSelectorNumberStyle = RowSelectorNumberStyle.Default;

		// MRS 4/5/05 - Added these three properties to allow the user to control column moving in RowLayout mode. 
		Layout.GridBagLayoutAllowMoving allowRowLayoutColMoving = Layout.GridBagLayoutAllowMoving.Default;
		Layout.GridBagLayoutAllowSpanSizing allowRowLayoutCellSpanSizing = Layout.GridBagLayoutAllowSpanSizing.Default;
		Layout.GridBagLayoutAllowSpanSizing allowRowLayoutLabelSpanSizing = Layout.GridBagLayoutAllowSpanSizing.Default;

		// JAS v5.2 GroupBy Break Behavior 5/4/05
		//
		private SortComparisonType sortComparisonType = SortComparisonType.Default;

		// SSP 7/14/05 - NAS 5.3 Header Placement
		// 
		private HeaderPlacement headerPlacement = HeaderPlacement.Default;

		// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
		// 
		private InvalidValueBehavior invalidValueBehavior = InvalidValueBehavior.Default;

		// SSP 8/17/05 BR05463
		// Added RowLayoutCellNavigationVertical property.
		// 
		private RowLayoutCellNavigation rowLayoutCellNavigationVertical = RowLayoutCellNavigation.Default;

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		private AllowMultiCellOperation allowMultiCellOperations = AllowMultiCellOperation.Default;

		// SSP 2/24/06 BR09715
		// Added MultiCellSelectionMode property to support snaking cell selection.
		// 
		private MultiCellSelectionMode multiCellSelectionMode = MultiCellSelectionMode.Default;
		
        // MBS 2/20/08 - RowEditTemplate NA2008 V2
        //
        private RowEditTemplateUIType rowEditTemplateUIType = RowEditTemplateUIType.Default;

		private TextOrientationInfo columnHeaderTextOrientation;
		private TextOrientationInfo groupHeaderTextOrientation;

        // MBS 5/6/08 - RowEditTemplate NA2008 V2
        private Image rowEditTemplateRowSelectorImage;

		// MD 9/23/08 - TFS6601
		private ReserveSortIndicatorSpaceWhenAutoSizing reserveSortIndicatorSpaceWhenAutoSizing;

        // MBS 12/4/08 - NA9.1 Excel Style Filtering
        private IFilterUIProvider filterUIProvider;        

		// MD 12/8/08 - 9.1 - Column Pinning Right
		private DefaultableBoolean fixHeadersOnRight;

        // CDS NAS v9.1 Header CheckBox
        private HeaderCheckBoxVisibility headerCheckBoxVisibility = HeaderCheckBoxVisibility.Default;
        private HeaderCheckBoxAlignment headerCheckBoxAlignment = HeaderCheckBoxAlignment.Default;
        private HeaderCheckBoxSynchronization headerCheckBoxSynchronization = HeaderCheckBoxSynchronization.Default;

        // CDS 9.2 Column Moving Indicators
        private DragDropIndicatorSettings dragDropIndicatorSettings = null;

        // MBS 3/31/09 - NA9.2 CellBorderColor
        //
        // MBS 5/15/09 - TFS17562
        //private int activeCellBorderThickness = 0;
        private int activeCellBorderThickness = 1;

        // MBS 6/19/09 - TFS18639
        private FilterOperandDropDownItems filterOperandDropDownItems = FilterOperandDropDownItems.Default;

        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
        private DefaultableBoolean activeAppearancesEnabled = DefaultableBoolean.Default;
        private DefaultableBoolean selectedAppearancesEnabled = DefaultableBoolean.Default;

        // MBS 8/7/09 - TFS18607
        private AddRowEditNotificationInterface addRowEditNotificationInterface = AddRowEditNotificationInterface.Default;

		#endregion // Private Members


		internal enum OverrideAppearanceIndex
		{
			ActiveRow					  = 0,
			ActiveCell					  = 1,
			Cell						  = 2,
			EditCell					  = 3,
			Header						  = 4,
			Row							  = 5,
			RowAlternate				  = 6,
			SelectedCell				  = 7,
			SelectedRow					  = 8,
			RowSelector					  = 9,
			RowPreview					  = 10,
			GroupByColumn				  = 11,
			GroupByColumnHeader			  = 12,
			GroupByRow					  = 13,
			MaskLiterals				  = 14,
			CellButton					  = 15,
			CardArea					  = 16,
			CardCaption					  = 17,
			ActiveCardCaption			  = 18,
			SelectedCardCaption			  = 19,
			SummaryFooterAppearance		  = 20,
			SummaryFooterCaptionAppearance = 21,
			SummaryValueAppearance		  = 22,
			// SSP 7/18/03 - Fixed headers
			// Added FixedHeaderAppearance and FixedCellAppearance.
			//
			FixedHeaderAppearance		  = 23,
			FixedCellAppearance			  = 24,

			// SSP 8/1/03 UWG1654 - Filter Action
			// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
			// appearances.
			//
			FilteredOutRowAppearance	  = 25,
			FilteredOutCellAppearance	  = 26,
			FilteredInRowAppearance		  = 27,
			FilteredInCellAppearance	  = 28,

			// SSP 11/7/03 - Add Row Feature
			//
			AddRowAppearance			  = 29,
			TemplateAddRowAppearance	  = 30,

			// SSP 12/11/03 UWG2766
			// Added AddRowCellAppearance and TemplateAddRowCellAppearance properties.
			//
			AddRowCellAppearance		  = 31,
			TemplateAddRowCellAppearance  = 32,

			// SSP 7/6/04 - UltraCalc
			// Added FormulaErrorAppearance.
			//
			FormulaErrorAppearance		  = 33,

			// SSP 11/3/04 - Merged Cell Feature
			//
			MergedCellAppearance		  = 34,

			//JDN 11/19/04 Added RowSelectorHeader
			RowSelectorHeaderAppearance   = 35,

			// SSP 12/14/04 - IDataErrorInfo Support
			//
			DataErrorRowAppearance		   = 36,
			DataErrorCellAppearance		   = 37,
			DataErrorRowSelectorAppearance = 38,

			// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			// ----------------------------------------------------------------------
			FilterClearButtonAppearance		= 39,
			FilterCellAppearance			= 40,
			FilterCellAppearanceActive		= 41,
			FilterOperatorAppearance		= 42,
			FilterRowAppearance				= 43,
			FilterRowAppearanceActive		= 44,
			FilterRowPromptAppearance		= 45,
			FilterRowSelectorAppearance		= 46,
			FixedRowAppearance				= 47,
			FixedRowCellAppearance			= 48,
			FixedRowSelectorAppearance		= 49,
			SpecialRowSeparatorAppearance	= 50,
			TemplateAddRowPromptAppearance	= 51,
			GroupBySummaryValueAppearance	= 52,
			// ----------------------------------------------------------------------

			// SSP 8/7/05 - NAS 5.3 HotTracking
			// 
			// ----------------------------------------------------------------------
			HotTrackCellAppearance			= 53,
			HotTrackHeaderAppearance		= 54,
			HotTrackRowAppearance			= 55,
			HotTrackRowCellAppearance		= 56,
			HotTrackRowSelectorAppearance	= 57,
			// ----------------------------------------------------------------------

			// SSP 8/26/06 - NAS 6.3
			// Added ReadOnlyCellAppearance to the override.
			// 
			ReadOnlyCellAppearance			= 58,

			// SSP 10/19/07 BR25706 BR25707
			// Added ActiveRowCellAppearance. This is to let one be able to specify the 
			// appearance for cell of the active row.
			// 
			ActiveRowCellAppearance			= 59,

            // MBS 5/8/09 - NA9.2 GroupByRowConnectorAppearance
            GroupByRowConnectorAppearance   = 60,

            // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
            ActiveCellRowSelectorAppearance = 61,
            ActiveCellColumnHeaderAppearance = 62,
		};

		private const int LastOverrideAppearanceIndex = 62;

		// the follwowing array lists the propertyids of the apperance
		// objects in the appearances array. These ids MUST be ordered
		// the same as the OverrideAppearanceIndexIndexs listed above
		//
		private static readonly PropertyIds [] AppearancePropIds = new PropertyIds[]
		{
			PropertyIds.ActiveRowAppearance,
			PropertyIds.ActiveCellAppearance,
			PropertyIds.CellAppearance,
			PropertyIds.EditCellAppearance,
			PropertyIds.HeaderAppearance,
			PropertyIds.RowAppearance,
			PropertyIds.RowAlternateAppearance,
			PropertyIds.SelectecCellAppearance,
			PropertyIds.SelectedRowAppearance,
			PropertyIds.RowSelectorAppearance,
			PropertyIds.RowPreviewAppearance,
			PropertyIds.GroupByColumnAppearance,
			PropertyIds.GroupByColumnHeaderAppearance,
			PropertyIds.GroupByRowAppearance,
			PropertyIds.MaskLiteralsAppearance,
			PropertyIds.CellButtonAppearance,
			PropertyIds.CardAreaAppearance,
			PropertyIds.CardCaptionAppearance,
			PropertyIds.ActiveCardCaptionAppearance,
			PropertyIds.SelectedCardCaptionAppearance,
			PropertyIds.SummaryFooterAppearance,
			PropertyIds.SummaryFooterCaptionAppearance,
			PropertyIds.SummaryValueAppearance,
			
			// SSP 7/18/03 - Fixed headers
			//
			PropertyIds.FixedHeaderAppearance,
			PropertyIds.FixedCellAppearance,

			// SSP 8/1/03 UWG1654 - Filter Action
			// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
			// appearances.
			//
			PropertyIds.FilteredOutRowAppearance,
			PropertyIds.FilteredOutCellAppearance,
			PropertyIds.FilteredInRowAppearance,
			PropertyIds.FilteredInCellAppearance,
			// SSP 11/7/03 - Add Row Feature
			//
			PropertyIds.AddRowAppearance,
			PropertyIds.TemplateAddRowAppearance,
			// SSP 12/11/03 UWG2766
			// Added AddRowCellAppearance and TemplateAddRowCellAppearance properties.
			//
			PropertyIds.AddRowCellAppearance,
			PropertyIds.TemplateAddRowCellAppearance,

			// SSP 7/6/04 - UltraCalc
			// Added FormulaErrorAppearance.
			//
			PropertyIds.FormulaErrorAppearance,

			// SSP 11/3/04 - Merged Cell Feature
			//
			PropertyIds.MergedCellAppearance,

			//JDN 11/19/04 Added RowSelectorHeader
			PropertyIds.RowSelectorHeaderAppearance,

			// SSP 12/14/04 - IDataErrorInfo Support
			//
			PropertyIds.DataErrorRowAppearance,
			PropertyIds.DataErrorCellAppearance,
			PropertyIds.DataErrorRowSelectorAppearance,

			// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			// ----------------------------------------------------------------------
			PropertyIds.FilterClearButtonAppearance,
			PropertyIds.FilterCellAppearance,
			PropertyIds.FilterCellAppearanceActive,
			PropertyIds.FilterOperatorAppearance,
			PropertyIds.FilterRowAppearance,
			PropertyIds.FilterRowAppearanceActive,
			PropertyIds.FilterRowPromptAppearance,
			PropertyIds.FilterRowSelectorAppearance,
			PropertyIds.FixedRowAppearance,
			PropertyIds.FixedRowCellAppearance,
			PropertyIds.FixedRowSelectorAppearance,
			PropertyIds.SpecialRowSeparatorAppearance,
			PropertyIds.TemplateAddRowPromptAppearance,
			PropertyIds.GroupBySummaryValueAppearance,
			// ----------------------------------------------------------------------

			// SSP 8/7/05 - NAS 5.3 Hottracking
			// 
			// ----------------------------------------------------------------------
			PropertyIds.HotTrackCellAppearance,
			PropertyIds.HotTrackHeaderAppearance,
			PropertyIds.HotTrackRowAppearance,
			PropertyIds.HotTrackRowCellAppearance,
			PropertyIds.HotTrackRowSelectorAppearance,
			// ----------------------------------------------------------------------

			// SSP 8/26/06 - NAS 6.3
			// Added ReadOnlyCellAppearance to the override.
			// 
			PropertyIds.ReadOnlyCellAppearance,

			// SSP 10/19/07 BR25706 BR25707
			// Added ActiveRowCellAppearance. This is to let one be able to specify the 
			// appearance for cell of the active row.
			// 
			PropertyIds.ActiveRowCellAppearance,

            // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
            PropertyIds.GroupByRowConnectorAppearance,

            // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
            PropertyIds.ActiveCellRowSelectorAppearance,
            PropertyIds.ActiveCellColumnHeaderAppearance,
		};
          

		/// <summary>
		/// Constructor used to add a valuelist within the properties
		/// table
		/// </summary>		                              
		public UltraGridOverride()
		{
			//RobA UWG577 10/18/01 had to make this contructor public 
			//with the lastest build of .Net
			this.Reset();
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="layout">The layout to which this override belongs.</param>                                 
		public UltraGridOverride( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			this.layout = layout;
			this.Reset();
		}

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="layout">The layout to which this override belongs.</param>                                 
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		public UltraGridOverride(  Infragistics.Win.UltraWinGrid.UltraGridLayout layout, String key ) : base( key )
		{
			this.layout = layout;
			this.Reset();
		}

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerialize() 
		{
            return this.ShouldSerializeAllowAddNew() ||
                this.ShouldSerializeAllowColMoving() ||
                this.ShouldSerializeAllowColSizing() ||
                this.ShouldSerializeAllowColSwapping() ||
                this.ShouldSerializeAllowDelete() ||
                this.ShouldSerializeAllowGroupMoving() ||
                this.ShouldSerializeAllowGroupSwapping() ||
                this.ShouldSerializeAllowUpdate() ||
                this.ShouldSerializeBorderStyleRow() ||
                this.ShouldSerializeBorderStyleCell() ||
                this.ShouldSerializeBorderStyleHeader() ||
                //JM 01-16-02 The following is no longer needed - now using
                //			  BorderStyleRow for cards
                //this.ShouldSerializeBorderStyleCard() ||
                this.ShouldSerializeBorderStyleCardArea() ||
                this.ShouldSerializeCellClickAction() ||
                this.ShouldSerializeCellMultiLine() ||
                this.ShouldSerializeCellPadding() ||
                this.ShouldSerializeCellSpacing() ||
                this.ShouldSerializeCardSpacing() ||
                this.ShouldSerializeDefaultColWidth() ||
                this.ShouldSerializeDefaultRowHeight() ||
                // this.ShouldSerializeExpandRowsOnLoad() ||
                this.ShouldSerializeExpansionIndicator() ||
                this.ShouldSerializeHeaderClickAction() ||
                this.ShouldSerializeMaxSelectedCells() ||
                this.ShouldSerializeMaxSelectedRows() ||
                this.ShouldSerializeRowSelectors() ||
                this.ShouldSerializeRowSizing() ||
                this.ShouldSerializeRowSizingArea() ||
                this.ShouldSerializeRowSizingAutoMaxLines() ||
                this.ShouldSerializeRowSpacingBefore() ||
                this.ShouldSerializeRowSpacingAfter() ||
                this.ShouldSerializeSelectTypeCell() ||
                this.ShouldSerializeSelectTypeCol() ||
                this.ShouldSerializeSelectTypeRow() ||
                this.ShouldSerializeTipStyleCell() ||
                this.ShouldSerializeTipStyleRowConnector() ||
                this.ShouldSerializeActiveCellAppearance() ||
                this.ShouldSerializeActiveRowAppearance() ||
                this.ShouldSerializeCellAppearance() ||
                this.ShouldSerializeEditCellAppearance() ||
                this.ShouldSerializeHeaderAppearance() ||
                this.ShouldSerializeRowAlternateAppearance() ||
                this.ShouldSerializeRowPreviewAppearance() ||
                this.ShouldSerializeRowSelectorAppearance() ||
                this.ShouldSerializeSelectedCellAppearance() ||
                this.ShouldSerializeSelectedRowAppearance() ||
                this.ShouldSerializeTipStyleScroll() ||
                //RobA 6/7/01 UWG124
                this.ShouldSerializeAppearanceHolders() ||
                this.ShouldSerializeGroupByColumnAppearance() ||
                this.ShouldSerializeGroupByColumnHeaderAppearance() ||
                this.ShouldSerializeGroupByRowAppearance() ||
                this.ShouldSerializeGroupByRowDescriptionMask() ||
                this.ShouldSerializeGroupByRowPadding() ||
                this.ShouldSerializeGroupByColumnsHidden() ||
                this.ShouldSerializeAllowGroupBy() ||
                this.ShouldSerializeSelectTypeGroupByRow() ||
                this.ShouldSerializeTag() ||
                //JM 01-16-02 The following is no longer needed - now using
                //			  BorderStyleRow for cards
                //this.ShouldSerializeBorderStyleCard() ||
                this.ShouldSerializeCardAreaAppearance() ||
                this.ShouldSerializeActiveCardCaptionAppearance() ||
                this.ShouldSerializeCardCaptionAppearance() ||
                this.ShouldSerializeSelectedCardCaptionAppearance() ||

                // JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
                //
                this.ShouldSerializeNullText() ||

                // SSP 12/7/01 UWG546
                this.ShouldSerializeCellButtonAppearance() ||

                // SSP 5/31/02
                // 
                this.ShouldSerializeAllowRowFiltering() ||
                // SSP 7/26/02 UWG1366
                //
                this.ShouldSerializeRowFilterMode() ||

                this.ShouldSerializeAllowRowSummaries() ||
                // SSP 6/11/02
                // Summary rows feature.
                //
                this.ShouldSerializeBorderStyleSummaryFooter() ||
                this.ShouldSerializeBorderStyleSummaryValue() ||
                this.ShouldSerializeBorderStyleSummaryFooterCaption() ||
                this.ShouldSerializeSummaryFooterAppearance() ||
                this.ShouldSerializeSummaryValueAppearance() ||
                // SSP 7/19/02 UWG1369
                //
                this.ShouldSerializeSummaryFooterCaptionVisible() ||
                this.ShouldSerializeSummaryFooterCaptionAppearance() ||
                // SSP 2/14/03 - RowSelectorWidth property
                // Added RowSelectorWidth property to allow the user to control the widths
                // of the row selectors.
                //
                this.ShouldSerializeRowSelectorWidth() ||

                // SSP 2/28/03 - Row Layout Functionality
                //
                this.ShouldSerializeBorderStyleRowSelector() ||
                this.ShouldSerializeAllowRowLayoutCellSizing() ||
                this.ShouldSerializeAllowRowLayoutLabelSizing() ||
                // SSP 4/14/03
                // Added ColumnAutoSizeMode property.
                //
                this.ShouldSerializeColumnAutoSizeMode() ||

                // SSP 5/12/03 - Optimizations
                // Added a way to just draw the text without having to embedd an embeddable ui element in
                // cells to speed up rendering.
                //
                this.ShouldSerializeCellDisplayStyle() ||

                // SSP 5/19/03 - Fixed headers
                //
                this.ShouldSerializeFixedHeaderIndicator() ||
                this.ShouldSerializeFixedCellSeparatorColor() ||
                this.ShouldSerializeFixedHeaderAppearance() ||
                this.ShouldSerializeFixedCellAppearance() ||

                // SSP 8/1/03 UWG1654 - Filter Action
                // Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
                // appearances.
                //
                this.ShouldSerializeRowFilterAction() ||
                this.ShouldSerializeFilteredOutRowAppearance() ||
                this.ShouldSerializeFilteredOutCellAppearance() ||
                this.ShouldSerializeFilteredInRowAppearance() ||
                this.ShouldSerializeFilteredInCellAppearance() ||
                // SSP 11/10/03 Add Row Feature
                //
                this.ShouldSerializeBorderStyleTemplateAddRow() ||
                this.ShouldSerializeTemplateAddRowSpacingAfter() ||
                this.ShouldSerializeTemplateAddRowSpacingBefore() ||
                this.ShouldSerializeTemplateAddRowAppearance() ||
                this.ShouldSerializeAddRowAppearance() ||
                // SSP 11/25/03 UWG2013 UWG2553
                // Added MinRowHeight property to Override to allow for unrestricted control over the 
                // row heights.
                //
                this.ShouldSerializeMinRowHeight() ||
                // SSP 12/12/03 DNF135
                // Added a way to control whether ink buttons get shown.
                // Added ShowInkButton property.
                //
                this.ShouldSerializeShowInkButton() ||

                // MRS 9/14/04
                // Added a property to control whether or not the grid displays 
                // a string in a cell while calculating
                //
                this.ShouldSerializeShowCalculatingText() ||

                // SSP 10/17/04
                // Added FormulaRowIndexSource property on the override.
                //
                this.ShouldSerializeFormulaRowIndexSource() ||

                // SSP 11/3/04 - Merged Cell Feature
                //
                this.ShouldSerializeMergedCellContentArea() ||
                this.ShouldSerializeMergedCellStyle() ||

                // SSP 11/16/04
                // Implemented column sizing using cells in non-rowlayout mode. 
                // Added ColumnSizingArea property to the override.
                //
                this.ShouldSerializeColumnSizingArea() ||

                //JDN 11/19/04 Added RowSelectorHeader
                this.ShouldSerializeRowSelectorHeaderAppearance() ||
                this.ShouldSerializeRowSelectorHeaderStyle() ||

                // SSP 12/15/04 - IDataErrorInfo Support
                //
                this.ShouldSerializeSupportDataErrorInfo() ||

                // SSP 12/21/04 BR01386
                // Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
                //
                this.ShouldSerializeGroupByRowSpacingBefore() ||
                this.ShouldSerializeGroupByRowSpacingAfter() ||

                // SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
                //
                // ----------------------------------------------------------------------
                this.ShouldSerializeBorderStyleFilterCell() ||
                this.ShouldSerializeBorderStyleFilterOperator() ||
                this.ShouldSerializeBorderStyleFilterRow() ||
                this.ShouldSerializeBorderStyleSpecialRowSeparator() ||
                this.ShouldSerializeFilterClearButtonLocation() ||
                this.ShouldSerializeFilterEvaluationTrigger() ||
                this.ShouldSerializeFilterOperandStyle() ||
                this.ShouldSerializeFilterOperatorDefaultValue() ||
                this.ShouldSerializeFilterOperatorDropDownItems() ||
                this.ShouldSerializeFilterOperatorLocation() ||
                this.ShouldSerializeFilterRowPrompt() ||
                this.ShouldSerializeFilterRowSpacingAfter() ||
                this.ShouldSerializeFilterRowSpacingBefore() ||
                this.ShouldSerializeFilterUIType() ||
                this.ShouldSerializeFixedRowIndicator() ||
                this.ShouldSerializeFixedRowsLimit() ||
                this.ShouldSerializeFixedRowSortOrder() ||
                this.ShouldSerializeFixedRowStyle() ||
                this.ShouldSerializeGroupBySummaryDisplayStyle() ||
                this.ShouldSerializeSequenceFilterRow() ||
                this.ShouldSerializeSequenceFixedAddRow() ||
                this.ShouldSerializeSequenceSummaryRow() ||
                this.ShouldSerializeSpecialRowSeparator() ||
                this.ShouldSerializeSpecialRowSeparatorHeight() ||
                this.ShouldSerializeSummaryDisplayArea() ||
                this.ShouldSerializeSummaryFooterSpacingAfter() ||
                this.ShouldSerializeSummaryFooterSpacingBefore() ||
                this.ShouldSerializeTemplateAddRowPrompt() ||
                this.ShouldSerializeFilterComparisonType() ||
                // ----------------------------------------------------------------------

                // JAS 2005 v2 GroupBy Row Extensions
                //
                this.ShouldSerializeGroupByRowExpansionStyle() ||
                this.ShouldSerializeGroupByRowInitialExpansionState() ||

                // JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
                //
                this.ShouldSerializeButtonStyle() ||

                // JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
                //
                this.ShouldSerializeHeaderStyle() ||
                this.ShouldSerializeRowSelectorStyle() ||

                // JAS v5.2 Wrapped Header Text 4/18/05
                //
                this.ShouldSerializeWrapHeaderText() ||

                // SSP 3/30/05 NAS 5.2 Row Numbers
                //
                this.ShouldSerializeRowSelectorNumberStyle() ||

                // MRS 4/11/05 NAS 5.2 RowLayout Colmoving and Span Resizing. 
                //
                this.ShouldSerializeAllowRowLayoutCellSizing() ||
                this.ShouldSerializeAllowRowLayoutLabelSizing() ||
                this.ShouldSerializeAllowRowLayoutColMoving() ||

                // JAS v5.2 GroupBy Break Behavior 5/4/05
                //
                this.ShouldSerializeSortComparisonType() ||
                // SSP 8/18/05
                // 
                this.ShouldSerializeInvalidValueBehavior() ||
                this.ShouldSerializeHeaderPlacement() ||
                // SSP 8/18/05 BR05463
                // 
                this.ShouldSerializeRowLayoutCellNavigationVertical() ||
                // SSP 11/15/05 - NAS 6.1 Multi-cell Operations
                // 
                this.ShouldSerializeAllowMultiCellOperations() ||
                // SSP 2/24/06 BR09715
                // Added MultiCellSelectionMode property to support snaking cell selection.
                // 
                this.ShouldSerializeMultiCellSelectionMode() ||
                // SSP 3/3/06 BR10430
                // Added TipStyleHeader property.
                // 
                this.ShouldSerializeTipStyleHeader() ||
                // MBS 2/20/08 - RowEditTemplate NA2008 V2
                //
                this.ShouldSerializeRowEditTemplateUIType() ||

                // MD 4/17/08 - 8.2 - Rotated Column Headers
                this.ShouldSerializeColumnHeaderTextOrientation() ||
                this.ShouldSerializeGroupHeaderTextOrientation() ||

                // MD 5/5/08
                // Found while making unit tests for 8.2 - Rotated Column Headers
                this.ShouldSerializeAllowRowLayoutCellSpanSizing() ||
                this.ShouldSerializeAllowRowLayoutLabelSpanSizing() ||

                // MRS 5/15/2008
                this.ShouldSerializeRowEditTemplateRowSelectorImage() ||

                // MD 9/23/08 - TFS6601
                this.ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing() ||

                // MD 12/8/08 - 9.1 - Column Pinning Right
                this.ShouldSerializeFixHeadersOnRight() ||

                // MBS 12/9/08 - NA9.1 Excel Style Filtering
                this.ShouldSerializeFilterUIProvider() ||

                // CDS NAS v9.1 Header CheckBox
                this.ShouldSerializeHeaderCheckBoxAlignment() ||
                this.ShouldSerializeHeaderCheckBoxSynchronization() ||
                this.ShouldSerializeHeaderCheckBoxVisibility() ||

                // MBS 3/31/09 - NA9.2 CellBorderColor
                this.ShouldSerializeActiveCellBorderThickness() ||

                // CDS 9.2 Column Moving Indicators
                this.ShouldSerializeDragDropIndicatorSettings() ||

                // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
                this.ShouldSerializeGroupByRowConnectorAppearance() ||

                // MBS 6/19/09 - TFS18639
                this.ShouldSerializeFilterOperandDropDownItems() ||

                // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
                this.ShouldSerializeActiveCellColumnHeaderAppearance() ||
                this.ShouldSerializeActiveCellRowSelectorAppearance() |

                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
                this.ShouldSerializeActiveAppearancesEnabled() ||
                this.ShouldSerializeSelectedAppearancesEnabled() ||        

                // MBS 8/7/09 - TFS18607
                this.ShouldSerializeAddRowEditNotificationInterface();
		}

		/// <summary>
		/// Resets all properties back to their default values
		/// </summary>
		public void Reset() 
		{
			//			this.ResetAppearanceHolders();
			this.ResetAllowAddNew();
			this.ResetAllowColMoving();
			this.ResetAllowColSizing();
			this.ResetAllowColSwapping();
			this.ResetAllowDelete();
			this.ResetAllowGroupMoving();
			this.ResetAllowGroupSwapping();
			this.ResetAllowUpdate();
			this.ResetBorderStyleRow();
			this.ResetBorderStyleCell();
			this.ResetBorderStyleHeader();
			this.ResetCellClickAction();
			this.ResetCellMultiLine();
			this.ResetCellPadding();
			this.ResetCellSpacing();
			this.ResetCardSpacing();
			this.ResetDefaultColWidth();
			this.ResetDefaultRowHeight();
			//this.ResetExpandRowsOnLoad();
			this.ResetExpansionIndicator();
			this.ResetHeaderClickAction();
			this.ResetMaxSelectedCells();
			this.ResetMaxSelectedRows();
			this.ResetRowSelectors();
			this.ResetRowSizing();
			this.ResetRowSizingArea();
			this.ResetRowSizingAutoMaxLines();
			this.ResetRowSpacingBefore();
			this.ResetRowSpacingAfter();
			this.ResetSelectTypeCell();
			this.ResetSelectTypeCol();
			this.ResetSelectTypeRow();
			this.ResetTipStyleCell();
			this.ResetTipStyleRowConnector();
			this.ResetTipStyleScroll();
			this.ResetActiveCellAppearance();
			this.ResetActiveRowAppearance();
			this.ResetCellAppearance();
			this.ResetEditCellAppearance();
			this.ResetHeaderAppearance();
			this.ResetRowAlternateAppearance();
			this.ResetRowAppearance();
			this.ResetRowPreviewAppearance();
			this.ResetRowSelectorAppearance();
			this.ResetSelectedCellAppearance();
			this.ResetSelectedRowAppearance();
			this.ResetAppearances();
			this.ResetAppearanceHolders();
			this.ResetGroupByColumnAppearance( );
			this.ResetGroupByColumnHeaderAppearance( );
			this.ResetGroupByRowAppearance( );
			this.ResetGroupByRowDescriptionMask( );
			this.ResetGroupByRowPadding( );
			this.ResetGroupByColumnsHidden( );
			this.ResetAllowGroupBy( );
			this.ResetSelectTypeGroupByRow( );
			//JM 01-16-02 The following is no longer needed - now using
			//			  BorderStyleRow for cards
			//this.ResetBorderStyleCard();
			this.ResetBorderStyleCardArea();
			this.ResetCardAreaAppearance();
			this.ResetActiveCardCaptionAppearance();
			this.ResetCardCaptionAppearance();
			this.ResetSelectedCardCaptionAppearance();

			// JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
			//
			this.ResetNullText();

			// SSP 12/7/01 UWG546
			this.ResetCellButtonAppearance( );
			
			// SSP 5/31/02
			//
			this.ResetAllowRowFiltering( );
			this.ResetRowFilterMode( );
			this.ResetAllowRowSummaries( );
			// SSP 6/11/02
			// Summary rows feature.
			//
			this.ResetBorderStyleSummaryFooter( );
			this.ResetBorderStyleSummaryValue( );
			this.ResetBorderStyleSummaryFooterCaption( );
			this.ResetSummaryFooterAppearance( );
			this.ResetSummaryValueAppearance( );
			// SSP 7/19/02 UWG1369
			//
			this.ResetSummaryFooterCaptionVisible( );
			this.ResetSummaryFooterCaptionAppearance( );

			// SSP 2/14/03 - RowSelectorWidth property
			// Added RowSelectorWidth property to allow the user to control the widths
			// of the row selectors.
			//
			this.ResetRowSelectorWidth( );

			// SSP 2/28/03 - Row Layout Functionality
			//
			this.ResetBorderStyleRowSelector( );
			this.ResetAllowRowLayoutLabelSizing( );
			this.ResetAllowRowLayoutCellSizing( );

			// SSP 4/14/03
			// Added ColumnAutoSizeMode property.
			//
			this.ResetColumnAutoSizeMode( );

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			this.ResetCellDisplayStyle( );

			// SSP 5/19/03 - Fixed headers
			//
			this.ResetFixedHeaderIndicator( );
			this.ResetFixedCellSeparatorColor( );
			this.ResetFixedHeaderAppearance( );
			this.ResetFixedCellAppearance( );

			// SSP 8/1/03 UWG1654 - Filter Action
			// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
			// appearances.
			//
			this.ResetRowFilterAction( );
			this.ResetFilteredOutRowAppearance( );
			this.ResetFilteredOutCellAppearance( );
			this.ResetFilteredInRowAppearance( );
			this.ResetFilteredInCellAppearance( );

			// SSP 11/10/03 Add Row Feature
			//
			this.ResetBorderStyleTemplateAddRow( );
			this.ResetTemplateAddRowSpacingAfter( );
			this.ResetTemplateAddRowSpacingBefore( );
			this.ResetTemplateAddRowAppearance( );
			this.ResetAddRowAppearance( );

			// SSP 11/25/03 UWG2013 UWG2553
			// Added MinRowHeight property to Override to allow for unrestricted control over the 
			// row heights.
			//
			this.ResetMinRowHeight( );

			// SSP 12/12/03 DNF135
			// Added a way to control whether ink buttons get shown.
			// Added ShowInkButton property.
			//
			this.ResetShowInkButton( );

			// MRS 9/14/04
			// Added a property to control whether or not the grid displays 
			// a string in a cell while calculating
			//
			this.ResetShowCalculatingText( );

			// SSP 10/17/04
			// Added FormulaRowIndexSource property on the override.
			//
			this.ResetFormulaRowIndexSource( );

			// SSP 11/3/04 - Merged Cell Feature
			//
			this.ResetMergedCellContentArea( );
			this.ResetMergedCellStyle( );

			// SSP 11/16/04
			// Implemented column sizing using cells in non-rowlayout mode. 
			// Added ColumnSizingArea property to the override.
			//
			this.ResetColumnSizingArea( );

			//JDN 11/19/04 Added RowSelectorHeader
			this.ResetRowSelectorHeaderAppearance( );
			this.ResetRowSelectorHeaderStyle( );

			// SSP 12/15/04 - IDataErrorInfo Support
			//
			this.ResetSupportDataErrorInfo( );

			// SSP 12/21/04 BR01386
			// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
			//
			this.ResetGroupByRowSpacingBefore( );
			this.ResetGroupByRowSpacingAfter( );

			// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			// ----------------------------------------------------------------------
			this.ResetBorderStyleFilterCell( );
			this.ResetBorderStyleFilterOperator( );
			this.ResetBorderStyleFilterRow( );
			this.ResetBorderStyleSpecialRowSeparator( );
			this.ResetFilterClearButtonLocation( );
			this.ResetFilterEvaluationTrigger( );
			this.ResetFilterOperandStyle( );
			this.ResetFilterOperatorDefaultValue( );
			this.ResetFilterOperatorDropDownItems( );
			this.ResetFilterOperatorLocation( );
			this.ResetFilterRowPrompt( );
			this.ResetFilterRowSpacingAfter( );
			this.ResetFilterRowSpacingBefore( );
			this.ResetFilterUIType( );
			this.ResetFixedRowIndicator( );
			this.ResetFixedRowsLimit( );
			this.ResetFixedRowSortOrder( );
			this.ResetFixedRowStyle( );
			this.ResetGroupBySummaryDisplayStyle( );
			this.ResetSequenceFilterRow( );
			this.ResetSequenceFixedAddRow( );
			this.ResetSequenceSummaryRow( );
			this.ResetSpecialRowSeparator( );
			this.ResetSpecialRowSeparatorHeight( );
			this.ResetSummaryDisplayArea( );
			this.ResetSummaryFooterSpacingAfter( );
			this.ResetSummaryFooterSpacingBefore( );
			this.ResetTemplateAddRowPrompt( );
			this.ResetFilterComparisonType( );
			// ----------------------------------------------------------------------

			// JAS 2005 v2 GroupBy Row Extensions
			//
			this.ResetGroupByRowExpansionStyle();
			this.ResetGroupByRowInitialExpansionState();

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
			//
			this.ResetHeaderStyle();
			this.ResetRowSelectorStyle();

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
			//
			this.ResetButtonStyle();

			// JAS v5.2 Wrapped Header Text 4/18/05
			//
			this.ResetWrapHeaderText();

			// SSP 3/30/05 NAS 5.2 Row Numbers
			//
			this.ResetRowSelectorNumberStyle( );

			// MRS 4/5/05
			this.ResetAllowRowLayoutColMoving();
			this.ResetAllowRowLayoutCellSpanSizing();
			this.ResetAllowRowLayoutLabelSpanSizing();

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			//
			this.ResetSortComparisonType();
			// SSP 8/18/05 
			// 
			this.ResetInvalidValueBehavior( );
			this.ResetHeaderPlacement( );
			// SSP 8/18/05 BR05463
			// 
			this.ResetRowLayoutCellNavigationVertical( );

			// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
			// 
			this.ResetAllowMultiCellOperations( );

			// SSP 2/24/06 BR09715
			// Added MultiCellSelectionMode property to support snaking cell selection.
			// 
			this.ResetMultiCellSelectionMode( );

			// SSP 3/3/06 BR10430
			// Added TipStyleHeader property.
			// 
			this.ResetTipStyleHeader( );

			// MD 4/17/08 - 8.2 - Rotated Column Headers
			this.ResetColumnHeaderTextOrientation();
			this.ResetGroupHeaderTextOrientation();

			// MD 5/5/08
			// Found while making unit tests for 8.2 - Rotated Column Headers
			this.ResetRowEditTemplateUIType();

            // MBS 5/6/08 - RowEditTemplate NA2008 V2
            this.ResetRowEditTemplateRowSelectorImage();

			// MD 9/23/08 - TFS6601
			this.ResetReserveSortIndicatorSpaceWhenAutoSizing();

			// MD 12/8/08 - 9.1 - Column Pinning Right
			this.ResetFixHeadersOnRight();

            // MBS 12/9/08 - NA9.1 Excel Style Filtering
            this.ResetFilterUIProvider();

            // CDS NAS v9.1 Header CheckBox
            this.ResetHeaderCheckBoxAlignment();
            this.ResetHeaderCheckBoxSynchronization();
            this.ResetHeaderCheckBoxVisibility();

            // MBS 3/31/09 - NA9.2 CellBorderColor
            this.ResetActiveCellBorderThickness();

            // CDS 9.2 Column Moving Indicators
            this.ResetDragDropIndicatorSettings();

            // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
            this.ResetGroupByRowConnectorAppearance();

            // MBS 6/19/09 - TFS18639
            this.ResetFilterOperandDropDownItems();

            // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
            this.ResetActiveCellColumnHeaderAppearance();
            this.ResetActiveCellRowSelectorAppearance();

            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
            this.ResetActiveAppearancesEnabled();
            this.ResetSelectedAppearancesEnabled();

            // MBS 8/7/09 - TFS18607
            this.ResetAddRowEditNotificationInterface();
		}

		/// <summary>
		/// Called when this object is Disposed of
		/// </summary>
		protected override void OnDispose()
		{
			// call reset appearance which will remove us a as sub object
			// prop change notify listener for all appearances 
			//
			// JJD 11/19/01
			// Unhook and call Reset to dispose the holders properly
			//
			//this.ResetAppearances();
			if ( null != this.appearanceHolders )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( null != this.appearanceHolders[ i ] )
					{
						// unhook the prop change notifications
						//
						this.appearanceHolders[ i ].SubObjectPropChanged -= this.SubObjectPropChangeHandler;
						this.appearanceHolders[ i ].Reset();
						this.appearanceHolders[ i ] = null;
					}
				}
			}

            // CDS 9.2 Column Moving Indicators
            if (this.HasDragDropIndicatorSettings)
            {
                this.dragDropIndicatorSettings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
                this.dragDropIndicatorSettings.Reset();
                this.dragDropIndicatorSettings = null;
            }

			base.OnDispose();
		}

		/// <summary>
		/// Returns an empty string.
		/// </summary>
        /// <returns>An empty string.</returns>
		public override String ToString() 
		{
			return this.Key;
		}

		/// <summary>
		/// Returns true if any appearance properties need to be
		/// persisted
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAppearanceHolders() 
		{
			if ( null != this.appearanceHolders )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( null != this.appearanceHolders[i]  &&
						this.appearanceHolders[i].ShouldSerialize() )
					{
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Resets all appearanceHolders in the appearanceHolder array
		/// </summary>
		public void ResetAppearanceHolders() 
		{
			if ( null != this.appearanceHolders  )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( null != this.appearanceHolders[i] )
						this.appearanceHolders[i].Reset();
				}
			}

		}

		/// <summary>
		/// Resets all appearances in the appearances array
		/// </summary>
		public void ResetAppearances() 
		{

			if ( null != this.appearanceHolders )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( null != this.appearanceHolders[ i ] )
						this.ResetAppearance( (OverrideAppearanceIndex) i );
				}
			}

		}
		private SelectionStrategyBase CreateSelectionStrategyHelper( SelectType selectType )
		{
			UltraGrid grid = this.layout.Grid as UltraGrid;
 
			SelectionStrategyBase strategy = null;

			if ( null != grid )
			{
				switch ( selectType )
				{
					case SelectType.None:
						strategy = new SelectionStrategyNone( grid );
						break;
					
					case SelectType.Single:
						strategy = new SelectionStrategySingle( grid );
						break;

					case SelectType.Extended:
						strategy = new SelectionStrategyExtended( grid );
						break;
					
						// JJD 1/15/02 - UWG876
						// Added support for auotdra strategies
						//
					case SelectType.SingleAutoDrag:
						strategy = new SelectionStrategySingleAutoDrag( grid );
						break;

					case SelectType.ExtendedAutoDrag:
						strategy = new SelectionStrategyExtendedAutoDrag( grid );
						break;

				}
			}

			return strategy;
		}
		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyCell
		{ 
			get
			{
				// if the selection strategy hasn't been
				// created then do it now
				//
				if ( this.selectionStrategyCell == null )
					this.selectionStrategyCell = this.CreateSelectionStrategyHelper( this.selectTypeCell );

				return this.selectionStrategyCell;
			}
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyRow
		{ 
			get
			{
				// if the selection strategy hasn't been
				// created then do it now
				//
				if ( this.selectionStrategyRow == null )
					this.selectionStrategyRow = this.CreateSelectionStrategyHelper( this.selectTypeRow );

				return this.selectionStrategyRow;
			}
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyGroupByRow
		{ 
			get
			{
				// if the selection strategy hasn't been
				// created then do it now
				//
				if ( this.selectionStrategyGroupByRow == null )
					this.selectionStrategyGroupByRow = this.CreateSelectionStrategyHelper( this.selectTypeGroupByRow );

				return this.selectionStrategyGroupByRow;
			}
		}


		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyColumn
		{ 
			get
			{
				// if the selection strategy hasn't been
				// created then do it now
				//
				if ( this.selectionStrategyColumn == null )
					this.selectionStrategyColumn = this.CreateSelectionStrategyHelper( this.selectTypeCol );

				return this.selectionStrategyColumn;
			}
		}

		[ Browsable(false), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		private Infragistics.Win.AppearanceHolder[] AppearanceHolders
		{
			get
			{
				// create the appearances on the 
				if ( null == this.appearanceHolders )
					this.appearanceHolders = new Infragistics.Win.AppearanceHolder[ LastOverrideAppearanceIndex + 1 ];

				return this.appearanceHolders;
			}
		}
		internal void InitializeFrom( UltraGridOverride source, PropertyCategories propertyCategories )
		{
			
			//OVERRIDE_SUBITEM_STATE
			this.defaultColWidth = source.defaultColWidth;
			this.defaultRowHeight = source.defaultRowHeight;
			this.cellPadding = source.cellPadding;
			this.cellSpacing = source.cellSpacing;
			this.cardSpacing = source.cardSpacing;
			this.rowSpacingBefore = source.rowSpacingBefore;
			this.rowSpacingAfter = source.rowSpacingAfter;
			this.maxSelectedCells = source.maxSelectedCells;
			this.maxSelectedRows = source.maxSelectedRows;

			// JJD 1/21/02 - UWG851 Moved from layout
			//
			this.nullText = source.nullText;

			this.rowSizingAutoMaxLines = source.rowSizingAutoMaxLines;
			this.borderStyleCell = source.borderStyleCell;
			//JM 01-16-02 The following is no longer needed - now using
			//			  BorderStyleRow for cards
			//this.borderStyleCard = source.borderStyleCard;
			this.borderStyleCardArea = source.borderStyleCardArea;
			this.borderStyleHeader = source.borderStyleHeader;
			this.borderStyleRow = source.borderStyleRow;
			this.rowSelectors = source.rowSelectors;
			this.allowAddNew = source.allowAddNew;
			this.allowColMoving = source.allowColMoving;
			this.allowColSizing = source.allowColSizing;
			this.allowColSwapping = source.allowColSwapping;
			this.allowDelete = source.allowDelete;
			this.allowGroupMoving = source.allowGroupMoving;
			//this.ExpandRowsOnLoad = source.expandRowsOnLoad;
			this.allowGroupSwapping = source.allowGroupSwapping;
			this.allowUpdate = source.allowUpdate;
			this.rowSizing = source.rowSizing;
			this.selectTypeCell = source.selectTypeCell;
			this.selectTypeCol = source.selectTypeCol;
			this.selectTypeRow = source.selectTypeRow;
			this.tipStyleCell = source.tipStyleCell;
			this.tipStyleRowConnector = source.tipStyleRowConnector;
			this.tipStyleScroll = source.tipStyleScroll;

			// SSP 3/3/06 BR10430
			// Added TipStyleHeader property.
			// 
			this.tipStyleHeader = source.tipStyleHeader;

			//FetchRows
			this.cellMultiLine = source.cellMultiLine;
			this.cellClickAction = source.cellClickAction;
			this.rowSizingArea = source.rowSizingArea;
			this.headerClickAction = source.headerClickAction;
			this.expansionIndicator = source.expansionIndicator;
			this.groupByColumnsHidden = source.groupByColumnsHidden;
			this.groupByRowDescriptionMask = source.groupByRowDescriptionMask;
			this.groupByRowPadding = source.groupByRowPadding;
			this.allowGroupBy = source.allowGroupBy;
			this.selectTypeGroupByRow = source.selectTypeGroupByRow;
			// SSP 12/21/04 BR01386
			// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
			//
			this.groupByRowSpacingBefore = source.groupByRowSpacingBefore;
			this.groupByRowSpacingAfter = source.groupByRowSpacingAfter;

			// SSP 6/11/02
			// Summay rows code.
			//
			this.allowRowFiltering = source.allowRowFiltering;
			// SSP 7/26/02
			// Also copy the row filter mode.
			//
			this.rowFilterMode = source.rowFilterMode;
			this.allowRowSummaries = source.allowRowSummaries;
			this.borderStyleSummaryFooter = source.borderStyleSummaryFooter;
			this.borderStyleSummaryValue = source.borderStyleSummaryValue;
			this.borderStyleSummaryFooterCaption = source.borderStyleSummaryFooterCaption;
			
			// SSP 7/19/02 UWG1369
			// Moved SummaryFooterCaptionVisible property from band to override.
			//
			this.summaryFooterCaptionVisible = source.summaryFooterCaptionVisible;

			// SSP 2/14/03 - RowSelectorWidth property
			// Added RowSelectorWidth property to allow the user to control the widths
			// of the row selectors.
			//
			this.rowSelectorWidth = source.rowSelectorWidth;

			// SSP 2/28/03 - Row Layout Functionality
			//
			this.allowRowLayoutCellSizing = source.allowRowLayoutCellSizing;
			this.allowRowLayoutLabelSizing = source.allowRowLayoutLabelSizing;

			// SSP 4/14/03
			// Added ColumnAutoSizeMode property.
			//
			this.columnAutoSizeMode = source.columnAutoSizeMode;

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			this.cellDisplayStyle = source.cellDisplayStyle;

			// SSP 5/19/03 - Fixed headers
			//
			// --------------------------------------------------------------------------------
			this.fixedHeaderIndicator = source.fixedHeaderIndicator;
			this.fixedCellSeparatorColor = source.fixedCellSeparatorColor;
			// --------------------------------------------------------------------------------

			// SSP 11/7/03 - Add Row Feature
			//
			// --------------------------------------------------------------------------------
			this.templateAddRowSpacingAfter = source.templateAddRowSpacingAfter;
			this.templateAddRowSpacingBefore = source.templateAddRowSpacingBefore;
			this.borderStyleTemplateAddRow = source.borderStyleTemplateAddRow;
			// --------------------------------------------------------------------------------

			// SSP 8/1/03 UWG1654 - Filter Action
			// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
			// appearances.
			//
			this.rowFilterAction = source.rowFilterAction;

			// SSP 11/25/03 UWG2013 UWG2553
			// Added MinRowHeight property to Override to allow for unrestricted control over the 
			// row heights.
			//
			this.minRowHeight = source.minRowHeight;

			// SSP 12/12/03 DNF135
			// Added a way to control whether ink buttons get shown.
			// Added ShowInkButton property.
			//
			this.showInkButton = source.showInkButton;

			// MRS 9/14/04
			// Added a property to control whether or not the grid displays 
			// a string in a cell while calculating
			//
			this.showCalculatingText = source.showCalculatingText;

			//JDN 11/19/04 Added RowSelectorHeader
			this.rowSelectorHeaderStyle = source.rowSelectorHeaderStyle;

			// SSP 11/29/04 - Merged Cell Feature
			//
			this.mergedCellContentArea = source.mergedCellContentArea;
			this.mergedCellStyle = source.mergedCellStyle;

			// SSP 12/15/04 - IDataErrorInfo Support
			//
			this.supportDataErrorInfo = source.supportDataErrorInfo;

			// SSP 3/14/05 - Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			// ------------------------------------------------------------------------------
			this.borderStyleFilterCell = source.borderStyleFilterCell;
			this.borderStyleFilterOperator = source.borderStyleFilterOperator;
			this.borderStyleFilterRow = source.borderStyleFilterRow;
			this.borderStyleSpecialRowSeparator = source.borderStyleSpecialRowSeparator;
			this.filterClearButtonLocation = source.filterClearButtonLocation;
			this.filterEvaluationTrigger = source.filterEvaluationTrigger;
			this.filterOperandStyle = source.filterOperandStyle;
			this.filterOperatorDefaultValue = source.filterOperatorDefaultValue;
			this.filterOperatorDropDownItems = source.filterOperatorDropDownItems;
			this.filterOperatorLocation = source.filterOperatorLocation;
			this.filterRowPrompt = source.filterRowPrompt;
			this.filterRowSpacingAfter = source.filterRowSpacingAfter;
			this.filterRowSpacingBefore = source.filterRowSpacingBefore;
			this.filterUIType = source.filterUIType;
			this.fixedRowIndicator = source.fixedRowIndicator;
			this.fixedRowsLimit = source.fixedRowsLimit;
			this.fixedRowSortOrder = source.fixedRowSortOrder;
			this.fixedRowStyle = source.fixedRowStyle;
			this.groupBySummaryDisplayStyle = source.groupBySummaryDisplayStyle;
			this.sequenceFilterRow = source.sequenceFilterRow;
			this.sequenceFixedAddRow = source.sequenceFixedAddRow;
			this.sequenceSummaryRow = source.sequenceSummaryRow;
			this.specialRowSeparator = source.specialRowSeparator;
			this.specialRowSeparatorHeight = source.specialRowSeparatorHeight;
			this.summaryDisplayArea = source.summaryDisplayArea;
			this.summaryFooterSpacingAfter = source.summaryFooterSpacingAfter;
			this.summaryFooterSpacingBefore = source.summaryFooterSpacingBefore;
			this.templateAddRowPrompt = source.templateAddRowPrompt;
			this.filterComparisonType = source.filterComparisonType;
			// ------------------------------------------------------------------------------

			// JAS 2005 v2 GroupBy Row Extensions
			//
			this.groupByRowExpansionStyle = source.groupByRowExpansionStyle;
			this.groupByRowInitialExpansionState = source.groupByRowInitialExpansionState;

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
			//
			this.headerStyle = source.headerStyle;
			this.rowSelectorStyle = source.rowSelectorStyle;

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
			//
			this.buttonStyle = source.buttonStyle;

			// SSP 3/30/05 NAS 5.2 Row Numbers
			//
			this.rowSelectorNumberStyle = source.rowSelectorNumberStyle;

			// MRS 4/11/05 NAS 5.2 RowLayout Colmoving and Span Resizing. 
            //
			this.allowRowLayoutCellSpanSizing = source.allowRowLayoutCellSpanSizing;
			this.allowRowLayoutLabelSpanSizing = source.allowRowLayoutLabelSpanSizing;
			this.allowRowLayoutColMoving = source.allowRowLayoutColMoving ;

			// SSP 6/27/05 BR04773
			// 
			this.sortComparisonType = source.sortComparisonType;

			// SSP 7/14/05 - NAS 5.3 Header Placement
			// 
			this.headerPlacement = source.headerPlacement;

			// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
			// 
			this.invalidValueBehavior = source.invalidValueBehavior;

			// SSP 8/17/05 BR05463
			// Added RowLayoutCellNavigationVertical property.
			// 
			this.rowLayoutCellNavigationVertical = source.rowLayoutCellNavigationVertical;

			// SSP 9/19/05 BR06405
			// 
            this.wrapHeaderText = source.wrapHeaderText;

			// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
			// 
			this.allowMultiCellOperations = source.allowMultiCellOperations;

			// SSP 2/24/06 BR09715
			// Added MultiCellSelectionMode property to support snaking cell selection.
			// 
			this.multiCellSelectionMode = source.multiCellSelectionMode;

            // MBS 2/20/08 - RowEditTemplate NA2008 V2
            //
            this.rowEditTemplateUIType = source.rowEditTemplateUIType;

			//OVERRIDE_SUBITEM_APPEARANCES:	

			if ( source.appearanceHolders != null )
			{
				
				for ( int i =0; i< source.appearanceHolders.Length; ++i )
				{
					// JJD 10/18/01
					// remove the old prop change notifications
					//
					if ( this.AppearanceHolders[i] != null )
					{
						// JJD 1/19/02 - UWG949
						// Unhook from the holder not the appearance
						//
						//						this.appearanceHolders[i].Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
						this.appearanceHolders[i].SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					}

					if ( source.appearanceHolders[i] != null )
					{
						this.AppearanceHolders[i] = source.appearanceHolders[i].Clone();

						// JJD 10/18/01
						// Add the new prop change notifications
						//
						// JJD 1/19/02 - UWG949
						// Hook the holder not the appearance
						//
						//this.appearanceHolders[i].Appearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
						this.appearanceHolders[i].SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}
					else //RA - Bug 98 7/31/01
						this.AppearanceHolders[i] = null;

					//this.appearanceHolders[i] = null;
				}
			}
			else
				this.appearanceHolders = null;

			// SSP 8/19/05 BR05669
			// Initialize the collection property for any appearance holders
			// that were serialized.
			// 
			this.InitAppearanceHolders( );

			//OVERRIDE_SUBITEM_KEY
			this.Key = source.Key;

			//OVERRIDE_SUBITEM_TAGVARIANT:

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			// MD 4/23/08 - 8.2 - Rotated Column Headers
			this.columnHeaderTextOrientation = source.columnHeaderTextOrientation;
			this.groupHeaderTextOrientation = source.groupHeaderTextOrientation;

            // MBS 6/24/08 - BR34115
            this.rowEditTemplateRowSelectorImage = source.rowEditTemplateRowSelectorImage;

			// MD 12/9/08 - 9.1 - Column Pinning Right
			this.fixHeadersOnRight = source.fixHeadersOnRight;

            // MBS 12/9/08 - NA9.1 Excel Style Filtering
            this.filterUIProvider = source.filterUIProvider;

            // CDS 5/4/09 TFS17276
            if (source.dragDropIndicatorSettings != null &&
                source.dragDropIndicatorSettings.ShouldSerialize())
            {
                if (this.dragDropIndicatorSettings != null)
                    this.dragDropIndicatorSettings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

                this.dragDropIndicatorSettings = source.dragDropIndicatorSettings;
                this.dragDropIndicatorSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                if (this.layout != null)
                    StyleUtils.DirtyCachedPropertyVals(layout);
            }

            // MBS 7/2/09 - TFS19013
            this.filterOperandDropDownItems = source.filterOperandDropDownItems;
            //
            // Noticed that I forgot to add this for the ActiveCellBorderThickness feature            
            this.activeCellBorderThickness = source.activeCellBorderThickness;

            // MBS 7/7/09 - TFS19074
            if (source.ShouldSerializeActiveCellBorderThickness())
                StyleUtils.DirtyCachedPropertyVals(this.layout);

            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
            this.activeAppearancesEnabled = source.activeAppearancesEnabled;
            this.selectedAppearancesEnabled = source.selectedAppearancesEnabled;        

            // MBS 8/7/09 - TFS18607
            this.addRowEditNotificationInterface = source.addRowEditNotificationInterface;
		}

		internal bool HasAppearance( OverrideAppearanceIndex index )
		{
			return ( null != this.appearanceHolders && 
				null != this.appearanceHolders[ (int)index ] &&
				this.appearanceHolders[ (int) index ].HasAppearance);
		}

		internal bool ShouldSerializeAppearance( OverrideAppearanceIndex index ) 
		{
			return ( this.HasAppearance( index ) && 
				this.GetAppearance( index ).ShouldSerialize() );
		}

		internal void ResetAppearance( OverrideAppearanceIndex index ) 
		{
			if ( null != this.appearanceHolders )
			{
				if ( null != this.appearanceHolders[ (int)index ] )
				{
					//					// remove the prop change notifications
					//					//
					//					this.appearanceHolders[ (int)index ].Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					//
					// JJD 11/1901
					// Reset the holder instaed of the Apperance property it exposes
					//
					this.appearanceHolders[ (int)index ].Reset();
					// AS - 10/2/01   We only need to reset the appearance data, not unhook.
					//this.appearanceHolders[ (int)index ].Appearance.Reset();

					// Notify listeners that the appearance has changed. 

					// MD 5/5/08
					// Found while making unit tests for 8.2 - Rotated Column Headers
					// We should be notifying listeners with a specific prop id, not the general Appearance
					//this.NotifyPropChange(PropertyIds.Appearance);
					this.NotifyPropChange( UltraGridOverride.AppearancePropIds[ (int)index ] );
				}
			}
		}
 
		internal Infragistics.Win.AppearanceBase GetAppearance( OverrideAppearanceIndex index )
		{
			if ( null == this.AppearanceHolders[ (int)index ] )
			{
                
				this.AppearanceHolders[ (int)index ] = new Infragistics.Win.AppearanceHolder( );
            
				// hook up the prop change notifications
				//
				this.appearanceHolders[ (int)index ].SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}

			// JJD 11/19/01
			// Set collection on each get or set
			//
			if ( this.layout != null )
				this.appearanceHolders[ (int)index ].Collection = this.layout.Appearances;

			return this.appearanceHolders[ (int)index ].Appearance;
		}

		#region GetAppearanceIfAllocated

		// SSP 3/29/05 - NAS 5.2 Optimizations
		// Added GetAppearanceIfAllocated.
		//
		internal Infragistics.Win.AppearanceBase GetAppearanceIfAllocated( OverrideAppearanceIndex index )
		{
			if ( null != this.appearanceHolders )
			{
				AppearanceHolder holder = this.appearanceHolders[ (int)index ];
				if ( null != holder && holder.HasAppearance )
					return holder.Appearance;
			}

			return null;
		}

		#endregion // GetAppearanceIfAllocated

		private void SetAppearance( OverrideAppearanceIndex index, Infragistics.Win.AppearanceBase appearance )
		{
			if ( null == this.AppearanceHolders[ (int)index ] ||
				!this.AppearanceHolders[ (int)index ].HasAppearance ||
				appearance != this.AppearanceHolders[ (int)index ].Appearance )
			{
				if ( null == this.AppearanceHolders[ (int)index ] )
				{
					this.AppearanceHolders[ (int)index ] = new Infragistics.Win.AppearanceHolder();
            
					// hook up the prop change notifications
					//
					this.appearanceHolders[ (int)index ].SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// JJD 11/19/01
				// Set collection on each get or set
				//
				if ( this.layout != null )
					this.appearanceHolders[ (int)index ].Collection = this.layout.Appearances;

				this.AppearanceHolders[ (int)index ].Appearance = appearance;

				this.NotifyPropChange( UltraGridOverride.AppearancePropIds[ (int)index ], null );
			}
		}

		/// <summary>
		/// Returns or sets the active row's Appearance object.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>ActiveRowAppearance</b> property is used to specify the appearance of the active row (as determined by the <b>ActiveRow</b> property). When you assign an Appearance object to the <b>ActiveRowAppearance</b> property, the properties of that object will be applied to any row that becomes the active row. You can use the <b>ActiveRowAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the active row, for example:</p> 
		///	<p class="code">UltraWinGrid1.Override.ActiveRowAppearance.BackColor = vbBlue</p>
		///	<p class="body">Because you may want the active row to look different at different levels of a hierarchical record set, <b>ActiveRowAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different active row appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the grid level to determine the properties for that band. In other words, any band without an override will use the grid's override, therefore the active row in that band will use the grid-level setting of <b>ActiveRowAppearance</b>.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_ActiveRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase ActiveRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.ActiveRow );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.ActiveRow, value );
			}
		}
		/// <summary>
		/// Returns true if an ActiveRowAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasActiveRowAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.ActiveRow );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeActiveRowAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.ActiveRow ); 
		}
 
		/// <summary>
		/// Resets Active Row's Appearance
		/// </summary>
		public void ResetActiveRowAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.ActiveRow ); 
		}

		/// <summary>
		/// Returns or sets the active cell's Appearance object.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>ActiveCellAppearance</b> property is used to specify the appearance of the active cell (as determined by the <b>ActiveCell</b> property). When you assign an Appearance object to the <b>ActiveCellAppearance</b> property, the properties of that object will be applied to any cell that becomes the active cell. You can use the <b>ActiveCellAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the active cell, for example:</p> 
		///	<p class="code">UltraWinGrid1.Override.ActiveCellAppearance.BackColor = vbBlue</p>
		///	<p class="body">Because you may want the active cell to look different at different levels of a hierarchical record set, <b>ActiveCellAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different active cell appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the active cell will use the grid-level setting of <b>ActiveCellAppearance</b>.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_ActiveCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase ActiveCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.ActiveCell );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.ActiveCell, value );
			}
		}
		/// <summary>
		/// Returns true if an ActiveCellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasActiveCellAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.ActiveCell );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeActiveCellAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.ActiveCell ); 
		}
 
		/// <summary>
		/// Reset Active Cell's appearance
		/// </summary>
		public void ResetActiveCellAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.ActiveCell ); 
		}


		/// <summary>
		///  Determines the formatting attributes that will be applied to the cells in a band or the grid.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>CellAppearance</b> property is used to specify the appearance of all the cells in a band or the grid. When you assign an Appearance object to the <b>CellAppearance</b> property, the properties of that object will be applied to all the cells belonging to the object specified. You can use the <b>CellAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the cells, for example:</p> 
		///	<p class="code">UltraWinGrid1.Override.CellAppearance.BackColor = vbYellow</p>
		///	<p class="body">Because you may want the cells to look different at different levels of a hierarchical record set, <b>CellAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different cell appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the cells of that band will use the grid-level setting of <b>CellAppearance</b>.</p>
		///	<p class="body">You can override the <b>CellAppearance</b> setting for specific cells by setting the <b>Appearance</b> property of the UltraGridCell object directly. The cell will always use the values of its own Appearance object before it will use the values inherited from the Appearance object specified by the <b>CellAppearance</b> property of the band it occupies.</p>
		///	<p class="body">If any of the properties of the Appearance object specified for the <b>CellAppearance</b> property are set to default values, the properties from the Appearance object of the row containing the cell are used.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_CellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase CellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.Cell );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.Cell, value );
			}
		}

		/// <summary>
		///  Determines the formatting attributes that will be applied to the buttons in cells in a band or the grid.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_CellButtonAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase CellButtonAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.CellButton );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.CellButton, value );
			}
		}

		/// <summary>
		/// Returns true if an CellButtonAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCellButtonAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.CellButton );
			}
		}

		/// <summary>
		/// Returns true if the CellButtonAppearance needs to be
		/// serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCellButtonAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.CellButton ); 
		}
 
		/// <summary>
		/// Resets the CellButtonAppearance.
		/// </summary>
		public void ResetCellButtonAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.CellButton ); 
		}




		/// <summary>
		/// Determines the formatting attributes that will be applied to the card area in a band or the grid.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_CardAreaAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase CardAreaAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.CardArea );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.CardArea, value );
			}
		}

		/// <summary>
		/// Returns true if an CardAreaAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCardAreaAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.CardArea );
			}
		}

		/// <summary>
		/// Returns true if the CardAreaAppearance needs to be
		/// serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCardAreaAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.CardArea ); 
		}
 
		/// <summary>
		/// Resets the CardAreaAppearance.
		/// </summary>
		public void ResetCardAreaAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.CardArea ); 
		}

		/// <summary>
		/// Determines the formatting attributes that will be applied to the card Label in a band or the grid.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_ActiveCardCaptionAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase ActiveCardCaptionAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.ActiveCardCaption );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.ActiveCardCaption, value );
			}
		}

		/// <summary>
		/// Returns true if an ActiveCardCaptionAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasActiveCardCaptionAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.ActiveCardCaption );
			}
		}

		/// <summary>
		/// Returns true if the ActiveCardCaptionAppearance needs to be
		/// serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeActiveCardCaptionAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.ActiveCardCaption ); 
		}
 
		/// <summary>
		/// Resets the ActiveCardCaptionAppearance.
		/// </summary>
		public void ResetActiveCardCaptionAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.ActiveCardCaption ); 
		}



		/// <summary>
		/// Determines the formatting attributes that will be applied to the card's caption in a band or the grid.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_CardCaptionAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase CardCaptionAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.CardCaption );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.CardCaption, value );
			}
		}

		/// <summary>
		/// Returns true if an CardCaptionAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCardCaptionAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.CardCaption );
			}
		}

		/// <summary>
		/// Returns true if the CardCaptionAppearance needs to be
		/// serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCardCaptionAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.CardCaption ); 
		}
 
		/// <summary>
		/// Resets the CardCaptionAppearance.
		/// </summary>
		public void ResetCardCaptionAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.CardCaption ); 
		}


		/// <summary>
		/// Determines the formatting attributes that will be applied to the selected card's caption in a band or the grid.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_SelectedCardCaptionAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SelectedCardCaptionAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.SelectedCardCaption );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.SelectedCardCaption, value );
			}
		}

		/// <summary>
		/// Returns true if an SelectedCardCaptionAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasSelectedCardCaptionAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.SelectedCardCaption );
			}
		}

		/// <summary>
		/// Returns true if the SelectedCardCaptionAppearance needs to be
		/// serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSelectedCardCaptionAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.SelectedCardCaption ); 
		}
 
		/// <summary>
		/// Resets the SelectedCardCaptionAppearance.
		/// </summary>
		public void ResetSelectedCardCaptionAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.SelectedCardCaption ); 
		}



		/// <summary>
		/// If the cell has MaskInput set or is using a UltraMaskedEdit control
		/// then this appearance will be applied to the literal chars while in
		/// edit mode.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_MaskLiteralsAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase MaskLiteralsAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.MaskLiterals );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.MaskLiterals, value );
			}
		}

		/// <summary>
		/// Returns true if an CellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasMaskLiteralsAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.MaskLiterals );
			}
		}


		/// <summary>
		/// Returns true if an CellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCellAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.Cell );
			}
		}

		// SSP 3/30/05
		// This internal Clone method is not being used anywhere so commented it out.
		//
		

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCellAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.Cell ); 
		}
 
		/// <summary>
		/// Resets the Cell's appearance
		/// </summary>
		public void ResetCellAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.Cell ); 
		}


		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMaskLiteralsAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.MaskLiterals ); 
		}
 
		/// <summary>
		/// Resets the MaskLiteralsAppearance
		/// </summary>
		public void ResetMaskLiteralsAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.MaskLiterals ); 
		}

		/// <summary>
		/// Determines the Appearance object applied to the UltraGridCell object when it is in editing mode.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>EditCellAppearance</b> property is used to specify the appearance of the cell that is in edit mode. (The <b>ActiveCell</b> property indicates which cell is currently active; a cell that is being edited is always the active cell. You can use the <b>IsInEditMode</b> property of the control to determine whether the cell is currently being edited.) When you assign an Appearance object to the <b>EditCellAppearance</b> property, the properties of that object will be applied to any cell that is in edit mode. You can use the <b>EditCellAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the cell being edited, for example:</p>
		///	<p class="code">UltraWinGrid1.Override.EditCellAppearance.BackColor = vbRed</p>
		///	<p class="body">Because you may want the edit cell to look different at different levels of a hierarchical record set, <b>EditCellAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the edit cell will use the grid-level setting of <b>EditCellAppearance</b>.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_EditCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase EditCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.EditCell );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.EditCell, value );
			}
		}

		/// <summary>
		/// Returns true if an EditCellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasEditCellAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.EditCell );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeEditCellAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.EditCell ); 
		}
 
		/// <summary>
		/// Resets the Edit Cell's Appearance object
		/// </summary>
		public void ResetEditCellAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.EditCell ); 
		}


		/// <summary>
		/// Returns or sets the Appearance object used to set the header formatting attributes.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>HeaderAppearance</b> property is used to specify the appearance of all the headers in a band or the grid. When you assign an Appearance object to the <b>HeaderAppearance</b> property, the properties of that object will be applied to all the column or group headers associated with the object that you specified. You can use the <b>HeaderAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to headers, for example:</p> 
		///	<p class="code">UltraWinGrid1.Override.HeaderAppearance.BackColor = vbBlack</p>
		///	<p class="body">Because you may want the headers to look different at different levels of a hierarchical record set, <b>HeaderAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different header appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the headers of that band will use the grid-level setting of <b>HeaderAppearance</b>.</p>
		///	<p class="body">You can override the <b>HeaderAppearance</b> setting for specific headers by setting the <b>Appearance</b> property of the Header object directly. The header will always use the values of its own Appearance object before it will use the values inherited from the Appearance object specified by the <b>HeaderAppearance</b> property of the band it occupies.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_HeaderAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase HeaderAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.Header );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.Header, value );
			}
		}
		/// <summary>
		/// Returns true if a HeaderAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasHeaderAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.Header );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeHeaderAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.Header ); 
		}
 
		/// <summary>
		/// Resets the Header Appearance
		/// </summary>
		public void ResetHeaderAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.Header ); 
		}



		/// <summary>
		/// Returns or sets the Appearance object for alternate rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>RowAlternateAppearance</b> property is used in conjunction with the <b>RowAppearance</b> property to apply different formatting options to odd and even rows in the grid. Even-numbered rows will use the Appearance specified by the <b>RowAlternateAppearance</b> property. If you do not specify a value for <b>RowAlternateAppearance</b>, the Appearance specified by <b>RowAlternateAppearance</b> will apply to all the rows in the band or the grid.</p>
		///	<p class="body">When you assign an Appearance object to the <b>RowAlternateAppearance</b> property, the properties of that object will be applied to the even-numbered rows belonging to the object specified. You can use the <b>RowAlternateAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the rows, for example:</p> 
		///	<p class="Code">UltraWinGrid1.Override.RowAppearance.ForeColor = vbRed</p>
		///	<p class="body">Because you may want the alternate rows to look different at different levels of a hierarchical record set, <b>RowAlternateAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different alternate row appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the alternate rows of that band will use the grid-level setting of <b>RowAlternateAppearance</b>.</p>
		///	<p class="body">You can override the <b>RowAlternateAppearance</b> setting for specific rows by setting the <b>Appearance</b> property of the UltraGridRow object directly. The row will always use the values of its own Appearance object before it will use the values inherited from the Appearance object specified by the <b>RowAlternateAppearance</b> property of the band it occupies.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowAlternateAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase RowAlternateAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.RowAlternate );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.RowAlternate, value );
			}
		}
		/// <summary>
		/// Returns true if a RowAlternateAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasRowAlternateAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.RowAlternate );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowAlternateAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.RowAlternate ); 
		}
 
		/// <summary>
		/// Resets the alternate Row's appearance
		/// </summary>
		public void ResetRowAlternateAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.RowAlternate ); 
		}


		/// <summary>
		/// Returns or sets the default Appearance object for the row selectors.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>RowSelectorAppearance</b> property is used to specify the appearance of the row selectors. When you assign an Appearance object to the <b>RowSelectorAppearance</b> property, the properties of that object will be applied to the row selectors of all all rows in the band or the rid, depending on where the UltraGridOverride object is being used. You can use the <b>RowSelectorAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the active cell, for example:</p> 
		///	<p class="Code">UltraWinGrid1.Override.RowSelectorAppearance.ForeColor = vbBlack</p>
		///	<p class="body">Because you may want the row selectors to look different at different levels of a hierarchical record set, <b>RowSelectorAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different row selector appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the row selectors will use the grid-level setting of <b>RowSelectorAppearance</b>.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSelectorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase RowSelectorAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.RowSelector );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.RowSelector, value );
			}
		}
		/// <summary>
		/// Returns true if an RowSelectorAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasRowSelectorAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.RowSelector );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSelectorAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.RowSelector ); 
		}
 
		/// <summary>
		/// Resets the RowSelector Appearance.
		/// </summary>
		public void ResetRowSelectorAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.RowSelector ); 
		}

		//JDN 11/19/04 Added RowSelectorHeader
		/// <summary>
		/// Returns or sets the default Appearance object for the row selector header.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>RowSelectorHeaderAppearance</b> property is used to specify the appearance of the row selector header. When you assign an Appearance object to the <b>RowSelectorHeaderAppearance</b> property, the properties of that object will be applied to the row selector header for the band or all bands in the grid, depending on where the UltraGridOverride object is being used. You can use the <b>RowSelectorHeaderAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the active cell, for example:</p> 
		///	<p class="Code">UltraWinGrid1.Override.RowSelectorHeaderAppearance.ForeColor = vbBlack</p>
		///	<p class="body">Because you may want the row selector header to look different at different levels of a hierarchical record set, <b>RowSelectorHeaderAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different row selector header appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the row selector header will use the grid-level setting of <b>RowSelectorHeaderAppearance</b>.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSelectorHeaderAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase RowSelectorHeaderAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.RowSelectorHeaderAppearance );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.RowSelectorHeaderAppearance, value );
			}
		}

		//JDN 11/19/04 Added RowSelectorHeader
		/// <summary>
		/// Returns true if an RowSelectorHeaderAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasRowSelectorHeaderAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.RowSelectorHeaderAppearance );
			}
		}

		//JDN 11/19/04 Added RowSelectorHeader
		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSelectorHeaderAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.RowSelectorHeaderAppearance ); 
		}
 
		/// <summary>
		/// Resets the RowSelectorHeaderAppearance Appearance.
		/// </summary>
		public void ResetRowSelectorHeaderAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.RowSelectorHeaderAppearance ); 
		}



		/// <summary>
		/// Returns or sets the default Appearance object for a selected cell.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>SelectedCellAppearance</b> property is used to specify the appearance of any selected cells (you can determine which cells are selected by using the <b>Cells</b> property of the Selected object). When you assign an Appearance object to the <b>SelectedCellAppearance</b> property, the properties of that object will be applied to any cell that becomes selected. You can use the <b>SelectedCellAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to selected cells, for example:</p> 
		///	<p class="code">UltraWinGrid1.Override.SelectedCellAppearance.BackColor = vbRed</p>
		///	<p class="body">Because you may want the selected cell(s) to look different at different levels of a hierarchical record set, <b>SelectedCellAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different selected cell appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the selected cell(s) will use the grid-level setting of <b>SelectedCellAppearance</b>.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SelectedCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SelectedCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.SelectedCell );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.SelectedCell, value );
			}
		}
		/// <summary>
		/// Returns true if a SelectedCellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasSelectedCellAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.SelectedCell );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSelectedCellAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.SelectedCell ); 
		}
 
		/// <summary>
		/// Resets the selected cell appearance
		/// </summary>
		public void ResetSelectedCellAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.SelectedCell ); 
		}


		/// <summary>
		/// Returns or sets the default Appearance object for a selected row.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>SelectedRowAppearance</b> property is used to specify the appearance of any selected rows (you can determine which rows are selected by using the <b>Rows</b> property of the Selected object). When you assign an Appearance object to the <b>SelectedRowAppearance</b> property, the properties of that object will be applied to any row that becomes selected. You can use the <b>SelectedRowAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to selected rows, for example:</p> 
		///	<p class="code">UltraWinGrid1.Override.SelectedRowAppearance.BackColor = vbYellow</p>
		///	<p class="body">Because you may want the selected row(s) to look different at different levels of a hierarchical record set, <b>SelectedRowAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different selected row appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the selected row(s) will use the grid-level setting of <b>SelectedRowAppearance</b>.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SelectedRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SelectedRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.SelectedRow );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.SelectedRow, value );
			}
		}

		/// <summary>
		/// Returns true if an SelectedRowAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasSelectedRowAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.SelectedRow );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSelectedRowAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.SelectedRow ); 
		}
 
		/// <summary>
		/// Resets the selected Row's Appearance.
		/// </summary>
		public void ResetSelectedRowAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.SelectedRow ); 
		}

		/// <summary>
		/// Returns or sets the Appearance object for non-alternate rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>RowAppearance</b> property is used to specify the appearance of all the rows in a band or the grid. You can use this property in combination with <b>RowAlternateAppearance</b> to apply different formatting options to odd and even rows in the grid. Odd-numbered rows will use the Appearance specified by the <b>RowAppearance</b> property. If you do not specify a value for <b>RowAlternateAppearance</b>, the Appearance specified by <b>RowAppearance</b> will apply to all the rows in the band or the grid.</p>
		///	<p class="body">When you assign an Appearance object to the <b>RowAppearance</b> property, the properties of that object will be applied to all the applicable rows belonging to the object specified. You can use the <b>RowAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the rows, for example:</p> 
		///	<p class="code">UltraWinGrid1.Override.RowAppearance.ForeColor = vbYellow</p>
		///	<p class="body">Because you may want the rows to look different at different levels of a hierarchical record set, <b>RowAppearance</b> is a property of the UltraGridOverride object. This makes it easy to specify different row appearances for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's override, and the top-level band will use the grid's override. Therefore, if the top-level band does not have its override set, the rows of that band will use the grid-level setting of <b>RowAppearance</b>.</p>
		///	<p class="body">You can override the <b>RowAppearance</b> setting for specific rows by setting the <b>Appearance</b> property of the UltraGridRow object directly. The row will always use the values of its own Appearance object before it will use the values inherited from the Appearance object specified by the <b>RowAppearance</b> property of the band it occupies.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase RowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.Row );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.Row, value );
			}
		}
		/// <summary>
		/// Returns true if a RowAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasRowAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.Row );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.Row ); 
		}
 
		/// <summary>
		/// Resets the RowAppearance
		/// </summary>
		public void ResetRowAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.Row ); 
		}

		/// <summary>
		/// Returns or sets the Appearance object that controls the formatting of row's AutoPreview area.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>RowPreviewAppearance</b> property provides access to the Appearance object being used to control the AutoPreview area of the rows in a band or the grid. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For more information on how to use properties that end in "Appearance", consult the topic for the <b>Appearance</b> property.</p>
		///	<p class="body">To determine the settings of the AutoPreview area for a given row, use the <b>ResolvePreviewAppearance</b> method of the UltraGridRow object.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowPreviewAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase RowPreviewAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.RowPreview );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.RowPreview, value );
			}
		}

		/// <summary>
		/// Returns true if an RowPreviewAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasRowPreviewAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.RowPreview );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowPreviewAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.RowPreview ); 
		}
 
		/// <summary>
		/// Resets the RowPreviewAppearance
		/// </summary>
		public void ResetRowPreviewAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.RowPreview ); 
		}

		/// <summary>
		/// Returns or sets the Appearance object that controls the formatting of summary footers.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>SummaryFooterAppearance</b> property provides access to the Appearance object being used to control the summary footers in a band or the grid. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For more information on how to use properties that end in "Appearance", consult the topic for the <b>Appearance</b> property.</p>
		///	<seealso cref="SummaryDisplayArea"/> 
		///	<seealso cref="AllowRowSummaries"/>
		///	<seealso cref="UltraGridBand.Summaries"/>
		///	<seealso cref="SummarySettingsCollection"/>
		///	<seealso cref="SummaryValueAppearance"/>
		///	<seealso cref="GroupBySummaryDisplayStyle"/>
		///	<seealso cref="GroupBySummaryValueAppearance"/>
		///	<seealso cref="SummarySettings.Appearance"/>
		///	<seealso cref="SummaryValue.Appearance"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SummaryFooterAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SummaryFooterAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.SummaryFooterAppearance );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.SummaryFooterAppearance, value );
			}
		}

		/// <summary>
		/// Returns true if an SummaryFooterAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasSummaryFooterAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.SummaryFooterAppearance );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSummaryFooterAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.SummaryFooterAppearance ); 
		}
 
		/// <summary>
		/// Resets the SummaryFooterAppearance
		/// </summary>
		public void ResetSummaryFooterAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.SummaryFooterAppearance ); 
		}

		/// <summary>
		/// Returns or sets the Appearance object that controls the formatting of captions over summary footers.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>SummaryFooterCaptionAppearance</b> property provides access to the Appearance object being used to control the captions over summary footers in a band or the grid. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For more information on how to use properties that end in "Appearance", consult the topic for the <b>Appearance</b> property.</p>
		///	<seealso cref="SummaryFooterCaptionVisible"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SummaryFooterCaptionAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SummaryFooterCaptionAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.SummaryFooterCaptionAppearance );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.SummaryFooterCaptionAppearance, value );
			}
		}

		/// <summary>
		/// Returns true if an SummaryFooterCaptionAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasSummaryFooterCaptionAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.SummaryFooterCaptionAppearance );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSummaryFooterCaptionAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.SummaryFooterCaptionAppearance ); 
		}
 
		/// <summary>
		/// Resets the SummaryFooterCaptionAppearance
		/// </summary>
		public void ResetSummaryFooterCaptionAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.SummaryFooterCaptionAppearance ); 
		}

		/// <summary>
		/// Returns or sets the Appearance object that controls the formatting of summaries in summary footers.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>SummaryValueAppearance</b> property provides access to the Appearance object being used to control the summaries in summary footers for a band or the grid. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For more information on how to use properties that end in "Appearance", consult the topic for the <b>Appearance</b> property.</p>
		///	<seealso cref="SummaryDisplayArea"/> 
		///	<seealso cref="AllowRowSummaries"/>
		///	<seealso cref="UltraGridBand.Summaries"/>
		///	<seealso cref="SummarySettingsCollection"/>
		///	<seealso cref="SummaryFooterAppearance"/>
		///	<seealso cref="GroupBySummaryDisplayStyle"/>
		///	<seealso cref="GroupBySummaryValueAppearance"/>
		///	<seealso cref="SummarySettings.Appearance"/>
		///	<seealso cref="SummaryValue.Appearance"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SummaryValueAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SummaryValueAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.SummaryValueAppearance );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.SummaryValueAppearance, value );
			}
		}

		/// <summary>
		/// Returns true if an SummaryValueAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasSummaryValueAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.SummaryValueAppearance );
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSummaryValueAppearance() 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.SummaryValueAppearance ); 
		}
 
		/// <summary>
		/// Resets the SummaryValueAppearance
		/// </summary>
		public void ResetSummaryValueAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.SummaryValueAppearance ); 
		}

		#region SummaryFooterCaptionVisible

		// SSP 7/19/02 UWG1369
		// Moved SummaryFooterCaptionVisible property to Override from the band.
		//

		/// <summary>
		/// Set this property to False to hide the summary footer captions.
		/// </summary>
		/// <remarks>
		/// <p class="body">By default summary footers have captions. Use this property to hide them for a particular band by setting it on that band's <see cref="UltraGridBand.Override"/> object or on all the bands by setting the layout's <see cref="UltraGridLayout.Override"/> object.</p>
		/// <p class="body"><see cref="UltraGridBand.SummaryFooterCaption"/> dictates the text that appears in the summary footer caption.</p>
		/// <seealso cref="UltraGridBand.SummaryFooterCaption"/>
		/// <seealso cref="SummaryFooterCaptionAppearance"/>
		/// </remarks>
		[ LocalizedDescription( "LDR_SummaryFooterCaptionVisible" ) ]
		public DefaultableBoolean SummaryFooterCaptionVisible
		{
			get
			{
				return this.summaryFooterCaptionVisible;
			}
			set
			{
				if ( this.summaryFooterCaptionVisible != value )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( Infragistics.Win.DefaultableBoolean ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LER_Exception_342"), (int)value, typeof( DefaultableBoolean ) );

					this.summaryFooterCaptionVisible = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.SummaryFooterCaptionVisible );
				}
			}
		}

		#endregion // SummaryFooterCaptionVisible

		#region ShouldSerializeSummaryFooterCaptionVisible

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSummaryFooterCaptionVisible( )
		{
			return DefaultableBoolean.Default != this.summaryFooterCaptionVisible;
		}

		#endregion //ShouldSerializeSummaryFooterCaptionVisible

		#region ResetSummaryFooterCaptionVisible

		/// <summary>
		/// Resets SummaryFooterCaptionVisible property to its default value.
		/// </summary>
		public void ResetSummaryFooterCaptionVisible( )
		{
			this.SummaryFooterCaptionVisible = DefaultableBoolean.Default;
		}

		#endregion // ResetSummaryFooterCaptionVisible

		/// <summary>
		/// Returns or sets a value that determines whether the user is allowed to add a new row of data and if so
		/// what kind of user interface is exposed for adding rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property determines whether the user can add new rows to the data in the band or the grid controlled by the specified override. 
		///	It also specifies the kind of user interface used for adding rows.</p>
		///	<p class="body">If using <b>Yes</b>, you must enable the AddNew box by setting
		///	the <i>ultraGrid1.DisplayLayout.AddNewBox.Hidden</i> to false. When the AddNew box is visible, 
		///	this property also controls the appearance of the buttons in the AddNew box. 
		///	If <b>AllowAddNew</b> is set to 2 (No) for a particular band, that 
		///	band's button will be disabled in the AddNew box. This prevents the user from 
		///	adding new data to the specified band.</p>
		///	<p class="body"><b>TabRepeat</b> provides for the rapid entry of new data.
		///	This value makes it possible to enter multiple rows of data efficiently using 
		///	only the keyboard. When the user enters data in the last cell of the AddNew row, 
		///	pressing the Tab key will automatically add a new row and 
		///	position data input to the first cell of that row. <b>TemplateOnBottom</b>, 
		///	<b>TemplateOnTopWithTabRepeat</b>, <b>FixedOnBottom</b> and 
		///	<b>FixedOnTop</b> settings also facilitate rapid entry of new data using only
		///	the keyboard. See <see cref="Infragistics.Win.UltraWinGrid.AllowAddNew"/> enum
		///	for more information on all the options available.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.AllowAddNew"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowAddNew")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.AllowAddNew AllowAddNew
		{
			get
			{
				return this.allowAddNew;
			}

			set
			{
				if ( value != this.allowAddNew )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UltraWinGrid.AllowAddNew), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_193"));

					this.allowAddNew = value;
					this.NotifyPropChange( PropertyIds.AllowAddNew, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowAddNew() 
		{
			return ( this.allowAddNew != AllowAddNew.Default );
		}
 
		/// <summary>
		/// Resets AllowAddNew to its default value (default).
		/// </summary>
		public void ResetAllowAddNew() 
		{
			this.AllowAddNew = AllowAddNew.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines whether the user is allowed to move columns.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>AllowColMoving</b> property determines how columns can be moved by the user in the band or the grid controlled by the specified override. Depending on the setting of <b>AllowColMoving</b>, users can move columns anywhere within the band, only within a group, or not at all. In order for the user to be able to move columns, column headers must be visible. If <b>AllowColMoving</b> is set to allow column moving within the band or the group, column headers become draggable, and are used to re-arrange the order of the columns via the mouse.</p>
		///	<p class="body">This property does not affect the ability of users to swap columns using the column swapping dropdown found in the column header (controlled by the <b>AllowColSwapping</b> property) or on the ability of the user to move groups within the grid (controlled by the <b>AllowGroupMoving</b> property).</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowColMoving")]
		[ LocalizedCategory("LC_Behavior") ]
		public AllowColMoving AllowColMoving
		{
			get
			{
				return this.allowColMoving;
			}

			set
			{
				if ( value != this.allowColMoving )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(AllowColMoving), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_194") );

					this.allowColMoving = value;
					this.NotifyPropChange( PropertyIds.AllowColMoving, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowColMoving() 
		{
			return ( this.allowColMoving != AllowColMoving.Default );
		}
 
		/// <summary>
		/// Resets AllowColMoving to its default value (default).
		/// </summary>
		public void ResetAllowColMoving() 
		{
			this.AllowColMoving = AllowColMoving.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines whether the user is allowed to size columns.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>AllowColSizing</b> property specifies how column resizing will be handled in the band or the grid controlled by the specified override. The <b>AllowColSizing</b> property determines not only whether columns can be resized, but how resizing columns within one band will affect the width of columns in other bands. By default, columns are aligned across multiple bands and the change in their widths is synchronized; when you resize one column, the others resize also. (You can change how columns align across bands by using the <b>ColSpan</b> property.) When <b>AllowColSizing</b> is set to 2 (AllowColSizingSync) a column resized in one band will resize all columns in other bands that occupy the same position. When <b>AllowColSizing</b> is set to 3 (AllowColSizingFree) the width of columns in the specified band can be changed independently of the widths of columns in other bands.</p>
		///	<p class="body">Due to the nature of Row-Layout functionality, <see cref="Infragistics.Win.UltraWinGrid.AllowColSizing"/>.Synchronized is not supported. If the property is set to that value, then it will resolve as <see cref="Infragistics.Win.UltraWinGrid.AllowColSizing"/>.Free.  The 'Synchronized' setting also does not work when using the Groups and Levels functionality, for similar reasons.</p>
		/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSizing"/>
		/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSizing"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowColSizing")]
		[ LocalizedCategory("LC_Behavior") ]
		public AllowColSizing AllowColSizing
		{
			get
			{
				return this.allowColSizing;
			}

			set
			{
				if ( value != this.allowColSizing )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(AllowColSizing), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_195") );

					this.allowColSizing = value;
					this.NotifyPropChange( PropertyIds.AllowColSizing, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowColSizing() 
		{
			return ( this.allowColSizing != AllowColSizing.Default );
		}
 
		/// <summary>
		/// Resets AllowColSizing to its default value (default).
		/// </summary>
		public void ResetAllowColSizing() 
		{
			this.AllowColSizing = AllowColSizing.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines whether the user is allowed to swap columns.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>AllowColSwapping</b> property determines how columns can be swapped by the user in the band or the grid controlled by the specified override. Depending on the setting of <b>AllowColSwapping</b>, users can swap columns within the band, within a group, or not at all. In order for the user to be able to swap columns, column headers must be visible. If <b>AllowColSwapping</b> is set to allow column swapping within the band or the group, the column headers will display a dropdown interface that is used to select the column that will be swapped with the current one. The contents of the dropdown list are also affected by the setting of <b>AllowColSwapping</b>.</p>
		///	<p class="body">This property does not affect the ability of users to move columns using the column moving functionality of the column headers (controlled by the <b>AllowColMoving</b> property) or on the ability of the user to swap groups within the grid (controlled by the <b>AllowGroupSwapping</b> property).</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowColSwapping")]
		[ LocalizedCategory("LC_Behavior") ]
		public AllowColSwapping AllowColSwapping
		{
			get
			{
				return this.allowColSwapping;
			}

			set
			{
				if ( value != this.allowColSwapping )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(AllowColSwapping), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_196") );

					this.allowColSwapping = value;
					this.NotifyPropChange( PropertyIds.AllowColSwapping, null );
				}
			}
		}
		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowColSwapping() 
		{
			return ( this.allowColSwapping != AllowColSwapping.Default );
		}
 
		/// <summary>
		/// Resets AllowColSwapping to its default value (default).
		/// </summary>
		public void ResetAllowColSwapping() 
		{
			this.AllowColSwapping = AllowColSwapping.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines whether the user is allowed to delete rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property determines whether the user can delete rows of data from the band or the grid controlled by the specified override. It does not control the deletion of data within individual cells (field-level deletion) only the removal of complete records from the data source (record-level deletion).</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowDelete")]
		[ LocalizedCategory("LC_Behavior") ]
		public DefaultableBoolean AllowDelete
		{
			get
			{
				return this.allowDelete;
			}

			set
			{
				if ( value != this.allowDelete )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(DefaultableBoolean), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_197") );

					this.allowDelete = value;
					this.NotifyPropChange( PropertyIds.AllowDelete, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowDelete() 
		{
			return ( this.allowDelete != DefaultableBoolean.Default );
		}
 
		/// <summary>
		/// Reset AllowDelete to its default value (default).
		/// </summary>
		public void ResetAllowDelete() 
		{
			this.AllowDelete = DefaultableBoolean.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines whether the user is allowed move groups.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>AllowGroupMoving</b> property determines whether groups can be moved by the user in the band or the grid controlled by the specified override. Depending on the setting of <b>AllowGroupMoving</b>, users can move groups anywhere within the band, or not at all. In order for the user to be able to move groups, group headers must be visible. If <b>AllowGroupMoving</b> is set to allow group moving, group headers become draggable, and are used to re-arrange the order of the groups via the mouse.</p>
		///	<p class="body">This property does not affect the ability of users to swap groups using the group swapping dropdown found in the group header (controlled by the <b>AllowGroupSwapping</b> property) or on the ability of the user to move columns within the grid (controlled by the <b>AllowColMoving</b> property).</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowGroupMoving")]
		[ LocalizedCategory("LC_Behavior") ]
		public AllowGroupMoving AllowGroupMoving
		{
			get
			{
				return this.allowGroupMoving;
			}

			set
			{
				if ( value != this.allowGroupMoving )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(AllowGroupMoving), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_198") );

					this.allowGroupMoving = value;
					this.NotifyPropChange( PropertyIds.AllowGroupMoving, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowGroupMoving() 
		{
			return ( this.allowGroupMoving != AllowGroupMoving.Default );
		}
 
		/// <summary>
		/// Resets AllowGroupMoving to its default value (default).
		/// </summary>
		public void ResetAllowGroupMoving() 
		{
			this.AllowGroupMoving = AllowGroupMoving.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines whether the user is allowed to swap groups.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>AllowGroupSwapping</b> property determines whether groups can be swapped by the user in the band or the grid controlled by the specified override. Depending on the setting of <b>AllowGroupSwapping</b>, users can swap groups within the band, or not at all. In order for the user to be able to swap groups, group headers must be visible. If <b>AllowGroupSwapping</b> is set to allow group swapping, the group headers will display a dropdown interface that is used to select the group that will be swapped with the current one.</p>
		///	<p class="body">This property does not affect the ability of users to move groups using the group moving functionality of the group headers (controlled by the <b>AllowGroupMoving</b> property) or on the ability of the user to swap columns within the grid (controlled by the <b>AllowColSwapping</b> property).</p>
        ///	<p class="body">When <see cref="UltraGridBand.RowLayoutStyle"/> is set to GroupLayout, swapping is only allowed between groups that have the same calculated SpanX and SpanY. Also, a group may never be swapped with any of it's ancestors or it's descendants.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowGroupSwapping")]
		[ LocalizedCategory("LC_Behavior") ]
		public AllowGroupSwapping AllowGroupSwapping
		{
			get
			{
				return this.allowGroupSwapping;
			}

			set
			{
				if ( value != this.allowGroupSwapping )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(AllowGroupSwapping), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_199") );

					this.allowGroupSwapping = value;
					this.NotifyPropChange( PropertyIds.AllowGroupSwapping, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowGroupSwapping() 
		{
			return ( this.allowGroupSwapping != AllowGroupSwapping.Default );
		}
 
		/// <summary>
		/// Resets AllowGroupSwapping to its default (default).
		/// </summary>
		public void ResetAllowGroupSwapping() 
		{
			this.AllowGroupSwapping = AllowGroupSwapping.Default;
		}

		/// <summary>
		/// Determines whether columns from this band can be dragged into
		/// the GroupByBox to become GroupBy columns. If resolved to default
		/// will assume 'Yes'.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowGroupBy")]
		[ LocalizedCategory("LC_Behavior") ]
		public DefaultableBoolean AllowGroupBy
		{
			get
			{
				return this.allowGroupBy;
			}
			set
			{
				if ( value != this.allowGroupBy )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_98") );

					this.allowGroupBy = value;
					this.NotifyPropChange( PropertyIds.AllowGroupBy, null );
				}
			}
		}

		/// <summary>
		/// Returns true if AllowGroupBy needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowGroupBy() 
		{
			return ( this.allowGroupBy != DefaultableBoolean.Default );
		}
 
		/// <summary>
		/// Resets AllowGroupBy to its default value (default).
		/// </summary>
		public void ResetAllowGroupBy() 
		{
			this.AllowGroupBy = DefaultableBoolean.Default;
		}



		// SSP 3/21/02
		// Added properties for Row Filter feature 
		//
		#region Row Filter feature implementation
		
		#region RowFilterMode

		// SSP 3/21/02
		// Added RowFilterMode preoprty
		// 
		/// <summary>
		/// Determines whether row filtering is done at the band level or individual rows collection level.
		/// If the view style is horizontal, this property is ignored and the RowFilterMode is always taken to be
		/// AllRowsInBand. Default is resolved to <b>AllRowsInBand</b> for the root band or if
		/// the view style is horizontal. Otherwise it's resolved to <b>SiblingRowsOnly</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property determines determines whether row filtering is done at the band level or individual rows collection level. If this property is set to <b>AllRowsInBand</b> then when the user selects a filter criteria all row islands in that band are applied that filer. If the property is set to <b>SiblingRowsOnly</b> then the filter criteria is only applied to the current row island.</p>
		/// <p class="body"><b>NOTE:</b> This property affects how you specify filter criteria in code. If this property is set to <b>AllRowsInBand</b> then you must use UltraGridBand's ColumnFilters property (<see cref="UltraGridBand.ColumnFilters"/>). If this property is set to <b>SiblingRowsOnly</b> then you must use the RowsCollection's ColumnFilters (<see cref="RowsCollection.ColumnFilters"/>) property.</p>
		/// <seealso cref="UltraGridOverride.AllowRowFiltering"/> <seealso cref="RowsCollection.ColumnFilters"/> <seealso cref="UltraGridBand.ColumnFilters"/> <seealso cref="UltraGridRow.IsFilteredOut"/> <seealso cref="RowsCollection.GetFilteredInNonGroupByRows"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowFilterMode")]
		[ LocalizedCategory("LC_Behavior") ]
		public RowFilterMode RowFilterMode
		{
			get
			{
				return this.rowFilterMode;
			}
			set
			{
				if ( value != this.rowFilterMode )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.RowFilterMode ), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_200") );

					this.rowFilterMode = value;
					this.NotifyPropChange( PropertyIds.RowFilterMode, null );
				}
			}
		}

		#endregion // RowFilterMode

		#region ShouldSerializeRowFilterMode

		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowFilterMode() 
		{
			return ( this.rowFilterMode != Infragistics.Win.UltraWinGrid.RowFilterMode.Default );
		}

		#endregion // ShouldSerializeRowFilterMode
 
		#region ResetRowFilterMode
		
		/// <summary>
		/// Resets the property to its default value
		/// </summary>
		public void ResetRowFilterMode() 
		{
			this.RowFilterMode = Infragistics.Win.UltraWinGrid.RowFilterMode.Default;
		}

		#endregion // ResetRowFilterMode

		#region AllowRowFiltering

		/// <summary>
		/// Determines whether row filtering will be enabled on column headers in this band.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property determines whether the user is allowed to filter rows. This property does not dictate whether you can specify filter criteria in code.</p>
		/// <p class="body">You can specify filter criteria in code via <see cref="RowsCollection.ColumnFilters"/> or <seealso cref="UltraGridBand.ColumnFilters"/> property depending on the <see cref="UltraGridOverride.RowFilterMode"/> property setting.</p>
		/// <seealso cref="UltraGridOverride.RowFilterMode"/> <seealso cref="RowsCollection.ColumnFilters"/> <seealso cref="UltraGridBand.ColumnFilters"/> <seealso cref="UltraGridRow.IsFilteredOut"/> <seealso cref="RowsCollection.GetFilteredInNonGroupByRows"/> <seealso cref="UltraGridOverride.FilterUIType"/> 
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowRowFiltering")]
		[ LocalizedCategory("LC_Behavior") ]
		public DefaultableBoolean AllowRowFiltering
		{
			get
			{
				return this.allowRowFiltering;
			}
			set
			{
				if ( value != this.allowRowFiltering )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_201") );

					this.allowRowFiltering = value;
					this.NotifyPropChange( PropertyIds.AllowRowFiltering, null );
				}
			}
		}

		#endregion // AllowRowFiltering

		#region ShouldSerializeAllowRowFiltering

		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowRowFiltering( ) 
		{
			return ( this.allowRowFiltering != DefaultableBoolean.Default );
		}

		#endregion // ShouldSerializeAllowRowFiltering
 
		#region ResetAllowRowFiltering

		/// <summary>
		/// Resets the property to its default value
		/// </summary>
		public void ResetAllowRowFiltering( )
		{
			this.AllowRowFiltering = DefaultableBoolean.Default;
		}

		#endregion // ResetAllowRowFiltering

		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
		// appearances.
		//

		#region RowFilterAction

		/// <summary>
		/// Specifies the action that the UltraGrid should take on rows that are filtered out.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>RowFilterAction</b> specifies what action is taken on rows that are filtered out. <b>AppearancesOnly</b> keeps the rows that are filtered out visible. <b>DisableFilteredOutRows</b> keeps the filtered out rows visible however it disables them so the user can't modify their contents. <b>HideFilteredOutRows</b> hides the rows that are filtered out.</p>
		/// <p class="body">When there are active row filters, FilteredOutRowAppearance and FilteredOutCellAppearance are applied to filtered out rows and their cells and FilteredInRowAppearance and FilteredInCellAppearance are applied to filtered in rows and their cells.</p>
		/// <seealso cref="UltraGridOverride.AllowRowFiltering"/> <seealso cref="UltraGridOverride.FilteredOutRowAppearance"/> <seealso cref="UltraGridOverride.FilteredOutCellAppearance"/> <seealso cref="UltraGridOverride.FilteredInRowAppearance"/> <seealso cref="UltraGridOverride.FilteredInCellAppearance"/> <seealso cref="UltraGridOverride.RowFilterMode"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_RowFilterAction")]
		[ LocalizedCategory("LC_Behavior") ]
		public RowFilterAction RowFilterAction
		{
			get
			{
				return this.rowFilterAction;
			}
			set
			{
				if ( value != this.rowFilterAction )
				{
					if ( ! Enum.IsDefined( typeof( RowFilterAction ), value ) )
						throw new InvalidEnumArgumentException( "RowFilterAction", (int)value, typeof( RowFilterAction ) );

					this.rowFilterAction = value;

					this.NotifyPropChange( PropertyIds.RowFilterAction );
				}
			}
		}

		#endregion // RowFilterAction

		#region ShouldSerializeRowFilterAction

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowFilterAction( )
		{
			return RowFilterAction.Default != this.rowFilterAction;
		}

		#endregion // ShouldSerializeRowFilterAction

		#region ResetRowFilterAction

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetRowFilterAction( )
		{
			this.RowFilterAction = RowFilterAction.Default;
		}

		#endregion // ResetRowFilterAction

		#region FilteredOutRowAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to rows that are filtered out.
		/// </summary>
		/// <remarks>
		/// <p class="body">When there are active row filters, <b>FilteredOutRowAppearance</b> applies to rows for which the filter conditions evaluate to false.</p>
		/// <p><seealso cref="UltraGridOverride.FilteredOutCellAppearance"/> <seealso cref="UltraGridOverride.FilteredInRowAppearance"/> <seealso cref="UltraGridOverride.FilteredInCellAppearance"/> <seealso cref="UltraGridOverride.RowFilterAction"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilteredOutRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilteredOutRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilteredOutRowAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilteredOutRowAppearance, value );
			}
		}

		#endregion // FilteredOutRowAppearance

		#region ShouldSerializeFilteredOutRowAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilteredOutRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilteredOutRowAppearance ); 
		}

		#endregion // ShouldSerializeFilteredOutRowAppearance
 
		#region ResetFilteredOutRowAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilteredOutRowAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilteredOutRowAppearance ); 
		}

		#endregion // ResetFilteredOutRowAppearance

		#region FilteredOutCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to cells of rows that are filtered out.
		/// </summary>
		/// <remarks>
		/// <p class="body">When there are active row filters, <b>FilteredOutCellAppearance</b> applies to cells of rows for which the filter conditions evaluate to false.</p>
		/// <p><seealso cref="UltraGridOverride.FilteredOutRowAppearance"/> <seealso cref="UltraGridOverride.FilteredInRowAppearance"/> <seealso cref="UltraGridOverride.FilteredInCellAppearance"/> <seealso cref="UltraGridOverride.RowFilterAction"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilteredOutCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilteredOutCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilteredOutCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilteredOutCellAppearance, value );
			}
		}

		#endregion // FilteredOutCellAppearance

		#region ShouldSerializeFilteredOutCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilteredOutCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilteredOutCellAppearance ); 
		}

		#endregion // ShouldSerializeFilteredOutCellAppearance
 
		#region ResetFilteredOutCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilteredOutCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilteredOutCellAppearance ); 
		}

		#endregion // ResetFilteredOutCellAppearance

		#region FilteredInRowAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to rows that are filtered in (not filtered out).
		/// </summary>
		/// <remarks>
		/// <p class="body">When there are active row filters, <b>FilteredInRowAppearance</b> applies to rows for which the filter conditions evaluate to true.</p>
		/// <p><seealso cref="UltraGridOverride.FilteredInCellAppearance"/> <seealso cref="UltraGridOverride.FilteredOutRowAppearance"/> <seealso cref="UltraGridOverride.FilteredOutCellAppearance"/> <seealso cref="UltraGridOverride.RowFilterAction"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilteredInRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilteredInRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilteredInRowAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilteredInRowAppearance, value );
			}
		}

		#endregion // FilteredInRowAppearance

		#region ShouldSerializeFilteredInRowAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilteredInRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilteredInRowAppearance ); 
		}

		#endregion // ShouldSerializedFilteredInRowAppearance
 
		#region ResetFilteredInRowAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilteredInRowAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilteredInRowAppearance ); 
		}

		#endregion // ResetFilteredInRowAppearance

		#region FilteredInCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to cells of rows that are filtered in (not filtered out).
		/// </summary>
		/// <remarks>
		/// <p class="body">When there are active row filters, <b>FilteredInCellAppearance</b> applies to cells of rows for which the filter conditions evaluate to true.</p>
		/// <p><seealso cref="UltraGridOverride.FilteredInRowAppearance"/> <seealso cref="UltraGridOverride.FilteredOutRowAppearance"/> <seealso cref="UltraGridOverride.FilteredOutCellAppearance"/> <seealso cref="UltraGridOverride.RowFilterAction"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilteredInCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilteredInCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilteredInCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilteredInCellAppearance, value );
			}
		}

		#endregion // FilteredInCellAppearance

		#region ShouldSerializeFilteredInCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilteredInCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilteredInCellAppearance ); 
		}

		#endregion // ShouldSerializedFilteredInCellAppearance
 
		#region ResetFilteredInCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilteredInCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilteredInCellAppearance ); 
		}

		#endregion // ResetFilteredInCellAppearance

		#endregion // Row Filter feature implementation


		/// <summary>
		/// Enables the user interface that lets the user add or remove column summaries. 
		/// By default the functionality is disabled. <b>Default</b> is resolved to <b>False</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>AllowRowSummaries</b> property determines whether the user will be allowed to 
		/// add or remove row summaries. Enabling the row summaries will display a button 
		/// in each column header that the user can use to summarize the column.
		/// By default the row summaries are disabled.
		/// </p>
		/// <p class="body">
		/// <b>Note</b> that this property controls whether the user interface for changing
		/// the summaries is enabled. It does not control whether you can display summaries
		/// or not. You can still display the summaries by adding them to the 
		/// <see cref="UltraGridBand.Summaries"/> collection. You can add summaries in code
		/// using the <see cref="SummarySettingsCollection.Add(string, SummaryType, UltraGridColumn, SummaryPosition)"/> method of the 
		/// <see cref="SummarySettingsCollection"/> object.
		/// </p>
		/// <seealso cref="UltraGridBand.Summaries"/>
		/// <seealso cref="SummarySettingsCollection"/>
        /// <seealso cref="SummarySettingsCollection.Add(string, SummaryType, UltraGridColumn, SummaryPosition)"/>
		/// <seealso cref="UltraGridOverride.SummaryFooterAppearance"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowRowSummaries")]
		[ LocalizedCategory("LC_Behavior") ]
		public AllowRowSummaries AllowRowSummaries
		{
			get
			{
				return this.allowRowSummaries;
			}
			set
			{
				if ( value != this.allowRowSummaries )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( AllowRowSummaries ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_314"), (int)value, typeof( AllowRowSummaries ) );

					this.allowRowSummaries = value;
					this.NotifyPropChange( PropertyIds.AllowRowSummaries, null );
				}
			}
		}

		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowRowSummaries() 
		{
			return ( this.allowRowSummaries != AllowRowSummaries.Default );
		}
 
		/// <summary>
		/// Resets the property to its default value
		/// </summary>
		public void ResetAllowRowSummaries() 
		{
			this.AllowRowSummaries = AllowRowSummaries.Default;
		}


		/// <summary>
		/// Determines if groupby columns will be displayed. If this resolves
		/// to 'default' then groupby columns are hidden.
		/// </summary>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.HiddenWhenGroupBy"/>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByColumnsHidden")]
		[ LocalizedCategory("LC_Display") ]
		public DefaultableBoolean GroupByColumnsHidden
		{
			get
			{
				return this.groupByColumnsHidden;
			}
			set
			{
				if ( value != this.groupByColumnsHidden )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_202") );

					this.groupByColumnsHidden = value;
					this.NotifyPropChange( PropertyIds.GroupByColumnsHidden, null );
				}
			}
		}

		/// <summary>
		/// Returns true if property needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByColumnsHidden() 
		{
			return ( this.groupByColumnsHidden != DefaultableBoolean.Default );
		}
 
		/// <summary>
		/// Resets the property to its default value
		/// </summary>
		public void ResetGroupByColumnsHidden() 
		{
			this.GroupByColumnsHidden = DefaultableBoolean.Default;
		}


		/// <summary>
		/// Returns or sets a value that determines whether the user is allowed to update the data.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>AllowUpdate</b> property determines whether to permit changes to the data displayed in the band or the grid controlled by the specified override. All data entry functionality is disabled when <b>AllowUpdate</b> is set to False. Cells may be selected and placed in edit mode, but their contents cannot be edited. Users can still view data, select all or part of it and copy it to the clipboard. They can also re-arrange the layout of the grid by moving and resizing columns, groups, rows, etc.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowUpdate")]
		[ LocalizedCategory("LC_Behavior") ]
		public DefaultableBoolean AllowUpdate
		{
			get
			{
				return this.allowUpdate;
			}

			set
			{
				if ( value != this.allowUpdate )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(DefaultableBoolean), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_203") );

					this.allowUpdate = value;
					this.NotifyPropChange( PropertyIds.AllowUpdate, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowUpdate() 
		{
			return ( this.allowUpdate != DefaultableBoolean.Default );
		}
 
		/// <summary>
		/// Resets AllowUpdate to its default value (default).
		/// </summary>
		public void ResetAllowUpdate() 
		{
			this.AllowUpdate = DefaultableBoolean.Default;
		}


		/// <summary>
		/// Returns or sets a value that determines the border style of rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of a row in the band or the grid controlled by the specified override. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleRow")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleRow
		{
			get
			{
				return this.borderStyleRow;
			}

			set
			{
				if ( value != this.borderStyleRow )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(UIElementBorderStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_204") );

					this.borderStyleRow = value;
					this.NotifyPropChange( PropertyIds.BorderStyleRow, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleRow() 
		{
			return ( this.borderStyleRow != UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets BorderStyleRow to its default value.
		/// </summary>
		public void ResetBorderStyleRow() 
		{
			this.BorderStyleRow = UIElementBorderStyle.Default;
		}


		/// <summary>
		/// Returns or sets a value that determines the border style of summary footers.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of summary footers in the band or the grid controlled by the specified override. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleSummaryFooter")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleSummaryFooter
		{
			get
			{
				return this.borderStyleSummaryFooter;
			}

			set
			{
				if ( value != this.borderStyleSummaryFooter )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(UIElementBorderStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_V2_ArgumentException_336") );

					this.borderStyleSummaryFooter = value;
					this.NotifyPropChange( PropertyIds.BorderStyleSummaryFooter, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleSummaryFooter() 
		{
			return ( this.borderStyleSummaryFooter != UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets BorderStyleSummaryFooter to its default value.
		/// </summary>
		public void ResetBorderStyleSummaryFooter() 
		{
			this.BorderStyleSummaryFooter = UIElementBorderStyle.Default;
		}


		/// <summary>
		/// Returns or sets a value that determines the border style of summaries in summary footers.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of summaries in summary footers for the band or the grid controlled by the specified override. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleSummaryValue")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleSummaryValue
		{
			get
			{
				return this.borderStyleSummaryValue;
			}

			set
			{
				if ( value != this.borderStyleSummaryValue )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(UIElementBorderStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_V2_ArgumentException_337") );

					this.borderStyleSummaryValue = value;
					this.NotifyPropChange( PropertyIds.BorderStyleSummaryValue, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleSummaryValue() 
		{
			return ( this.borderStyleSummaryValue != UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets BorderStyleSummaryValue to its default value.
		/// </summary>
		public void ResetBorderStyleSummaryValue() 
		{
			this.BorderStyleSummaryValue = UIElementBorderStyle.Default;
		}


		/// <summary>
		/// Returns or sets a value that determines the border style of summary footer caption.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of summary footer caption for the band or the grid controlled by the specified override. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.SummaryFooterCaption"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleSummaryFooterCaption")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleSummaryFooterCaption
		{
			get
			{
				return this.borderStyleSummaryFooterCaption;
			}

			set
			{
				if ( value != this.borderStyleSummaryFooterCaption )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(UIElementBorderStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_V2_ArgumentException_338") );

					this.borderStyleSummaryFooterCaption = value;
					this.NotifyPropChange( PropertyIds.BorderStyleSummaryFooterCaption, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleSummaryFooterCaption( ) 
		{
			return ( this.borderStyleSummaryFooterCaption != UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets BorderStyleSummaryFooterCaption to its default value.
		/// </summary>
		public void ResetBorderStyleSummaryFooterCaption( ) 
		{
			this.BorderStyleSummaryFooterCaption = UIElementBorderStyle.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines the border style of cells.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of cells in the band or the grid controlled by the specified override. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleCell")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleCell
		{
			get
			{
				return this.borderStyleCell;
			}

			set
			{
				if ( value != this.borderStyleCell )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(UIElementBorderStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_205") );

					this.borderStyleCell = value;
					this.NotifyPropChange( PropertyIds.BorderStyleCell, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleCell() 
		{
			return ( this.borderStyleCell != UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets BorderStyleCell to its default value.
		/// </summary>
		public void ResetBorderStyleCell() 
		{
			this.BorderStyleCell = UIElementBorderStyle.Default;
		}

		//JM 01-16-02 The following is no longer needed - now using
		//			  BorderStyleRow for cards
        
        /// <summary>
		/// Returns or sets a value that determines the border style of Cards.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of cards in the band or the grid controlled by the specified override. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleCard")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleCardArea
		{
			get
			{
				return this.borderStyleCardArea;
			}

			set
			{
				if ( value != this.borderStyleCardArea )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(UIElementBorderStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_206") );

					this.borderStyleCardArea = value;
					this.NotifyPropChange( PropertyIds.BorderStyleCardArea, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleCardArea() 
		{
			return ( this.borderStyleCardArea != UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets BorderStyleCardArea to its default value.
		/// </summary>
		public void ResetBorderStyleCardArea() 
		{
			// MD 5/5/08
			// Found while making unit tests for 8.2 - Rotated Column Headers
			// We should be setting the property so the prop change notification is fired.
			//this.borderStyleCardArea = UIElementBorderStyle.Default;
			this.BorderStyleCardArea = UIElementBorderStyle.Default;
		}


		/// <summary>
		/// An integer expression or constant that determines the appearance of the border of a header.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of a column or group header in the band or the grid controlled by the specified override. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleHeader")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleHeader
		{
			get
			{
				return this.borderStyleHeader;
			}

			set
			{
				if ( value != this.borderStyleHeader )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(UIElementBorderStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_207") );

					this.borderStyleHeader = value;
					this.NotifyPropChange( PropertyIds.BorderStyleHeader, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleHeader() 
		{
			return ( this.borderStyleHeader != UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets BorderStyleHeader to its default value.
		/// </summary>
		public void ResetBorderStyleHeader() 
		{
			this.BorderStyleHeader = UIElementBorderStyle.Default;
		}

		/// <summary>
		/// Returns or sets a value that indicates what will occur when a cell is clicked.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>CellClickAction</b> property specifies what will occur when the user navigates through the grid by clicking on cells in the band or the grid controlled by the specified override. You can choose whether cells that are clicked will put the cell into edit mode or select the cell or its row. Depending on your application, you may want to enable the user to edit any cell just by clicking on it, or you may want to require a separate action to trigger cell editing, such as double-clicking or a keystroke combination. Similarly, you can choose whether cells should be individually selectable, or if selecting the row is a sufficient response to clicking on a cell.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.CellClickAction"/>
		///	<seealso cref="UltraGridColumn.CellActivation"/>
		/// <seealso cref="UltraGridRow.Activation"/>
		/// <seealso cref="UltraGridCell.Activation"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_CellClickAction")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.CellClickAction CellClickAction
		{
			get
			{
				return this.cellClickAction;
			}

			set
			{
				if ( value != this.cellClickAction )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(CellClickAction), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_208") );

					this.cellClickAction = value;
					this.NotifyPropChange( PropertyIds.CellClickAction, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCellClickAction() 
		{
			return ( this.cellClickAction != CellClickAction.Default );
		}
 
		/// <summary>
		/// Resets CellClickAction to its default value.
		/// </summary>
		public void ResetCellClickAction() 
		{
			this.CellClickAction = CellClickAction.Default;
		}

		/// <summary>
		/// Determines if the cell's data should be displayed in a multi-line format.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property controls the display of multiple lines of text in edit cells in the band or the grid controlled by the specified override. When True, text will wrap in the area of the cell. If the <b>RowSizing</b> property is set to automatically resize the row, the row will expand in height until all lines of text are displayed (or the number of lines specified by the <b>RowSizingAutoMaxLines</b> property is reached).</p>
		///	<p class="body">The <b>CellMultiLine</b> property does not pertain to multi-line editing, only display. Also, you should note that setting a cell to multi-line mode will disable data masking. Only single-line cells can be masked (using the <b>MaskInput</b> and <b>MaskDisplayMode</b> properties.)</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_CellMultiLine")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.DefaultableBoolean CellMultiLine
		{
			get
			{
				return this.cellMultiLine;
			}

			set
			{
				if ( value != this.cellMultiLine )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(DefaultableBoolean), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_88") );

					this.cellMultiLine = value;
					this.NotifyPropChange( PropertyIds.CellMultiLine, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCellMultiLine() 
		{
			return ( this.cellMultiLine != DefaultableBoolean.Default );
		}
 
		/// <summary>
		/// Resets CellMultiline to its default value.
		/// </summary>
		public void ResetCellMultiLine() 
		{
			this.CellMultiLine = DefaultableBoolean.Default;
		}

		/// <summary>
		/// Returns or sets the amount of spacing, in pixels, between the cell's border and the cell's contents.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>CellPadding</b> property determines the amount of space between the edges of a cell and the text of the cell in the band or the grid controlled by the specified override. It is similar to an internal margin for the cell. If you want to control the amount of space that surrounds the cell itself, use the <b>CellSpacing</b> property.</p>
		///	<p class="body">Setting <b>CellPadding</b> to a value of -1 will cause it to use the value from the next highest object in the override hierarchy.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_CellPadding")]
		[ LocalizedCategory("LC_Display") ]
		public int CellPadding
		{
			get
			{
				return this.cellPadding;
			}

			set
			{
				if ( value != this.cellPadding )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.cellPadding = -1;
					else
						this.cellPadding = value;
    		    	
					this.NotifyPropChange( PropertyIds.CellPadding, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCellPadding() 
		{
			return ( this.cellPadding >= 0 );
		}
 
		/// <summary>
		/// Resets CellPadding to its default value (-1).
		/// </summary>
		public void ResetCellPadding() 
		{
			this.CellPadding = -1;
		}

		/// <summary>
		/// Returns or sets the amount of spacing, in pixels, between cells. Also determines the spacing between the cell's border and the row's border.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>CellSpacing</b> property determines the amount of empty space in a row that will surround each cell in the band or the grid controlled by the specified override. Spacing between cells allows the background of the underlying row to become visible, along with any color, transparency or background picture that was assigned to the UltraGridRow object. Cell spacing adds space equally on all sides of the cell - top, bottom, left and right.</p>
		///	<p class="body">Setting <b>CellSpacing</b> to a value of -1 will cause it to use the value from the next highest object in the override hierarchy. </p>
		///	<p class="body">This property does not have any effect on the inside of the cell. To control the cell's interior spacing, use the <b>CellPadding</b> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_CellSpacing")]
		[ LocalizedCategory("LC_Display") ]
		public int CellSpacing
		{
			get
			{
				return this.cellSpacing;
			}

			set
			{
				if ( value != this.cellSpacing )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.cellSpacing = -1;
					else
						this.cellSpacing = value;
    		    	
					this.NotifyPropChange( PropertyIds.CellSpacing, null );
				}
			}
		}

		
		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCellSpacing() 
		{
			return ( this.cellSpacing >= 0 );
		}
 
		/// <summary>
		/// Resets CellSpacing to its default value (-1).
		/// </summary>
		public void ResetCellSpacing() 
		{
			this.CellSpacing = -1;
		}

		/// <summary>
		/// Returns or sets the amount of spacing, in pixels, between cards. 
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>CardSpacing</b> property determines the amount of empty space </p>
		///	<p class="body">Setting <b>CardSpacing</b> to a value of -1 will cause it to use the value from the next highest object in the override hierarchy. </p>
		///	<p class="body">This property does not have any effect on the inside of the card. To control the card's interior spacing, use the <b>CardSpacing</b> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_CardSpacing")]
		[ LocalizedCategory("LC_Display") ]
		public int CardSpacing
		{
			get
			{
				return this.cardSpacing;
			}

			set
			{
				if ( value != this.cardSpacing )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.cardSpacing = -1;
					else
						this.cardSpacing = value;
    		    	
					this.NotifyPropChange( PropertyIds.CardSpacing, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCardSpacing() 
		{
			return ( this.cardSpacing >= 0 );
		}
 
		/// <summary>
		/// Resets CardSpacing to its default value (-1).
		/// </summary>
		public void ResetCardSpacing() 
		{
			this.CardSpacing = -1;
		}


		/// <summary>
		/// Returns or sets a value representing the default column width.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use this property to specify the width that columns will start with when the band or the grid controlled by the specified override is first displayed. Setting this property to 0 will cause the control to use the largest font size specified for the column to determine the column's width. Pictures are not taken into account by the control when calculating the default column width, so large pictures in cells may be clipped when they are displayed.</p>
		///	<p class="body">Setting <b>DefaultColWidth</b> to a value of -1 will cause it to use the value from the next highest object in the override hierarchy.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_DefaultColWidth")]
		[ LocalizedCategory("LC_Display") ]
		public int DefaultColWidth
		{
			get
			{
				return this.defaultColWidth;
			}

			set
			{
				if ( value != this.defaultColWidth )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.defaultColWidth = -1;
					else
						this.defaultColWidth = value;
    		    	
					this.NotifyPropChange( PropertyIds.DefaultColWidth, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeDefaultColWidth() 
		{
			return ( this.defaultColWidth >= 0 );
		}
 
		/// <summary>
		/// Resets DefaultColWidth to its default value (-1).
		/// </summary>
		public void ResetDefaultColWidth() 
		{
			this.DefaultColWidth = -1;
		}

		/// <summary>
		/// Returns or sets a value representing the default row height.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use this property to specify the height that rows will start with when the band or the grid controlled by the specified override is first displayed. Setting this property to 0 will cause the control to use the largest font size specified for the row to determine the row's height. Pictures are not taken into account by the control when calculating the default row height, so large pictures in cells may be clipped when they are displayed.</p>
		///	<p class="body">Setting <b>DefaultRowHeight</b> to a value of -1 will cause it to use the value from the next highest object in the override hierarchy.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_DefaultRowHeight")]
		[ LocalizedCategory("LC_Display") ]
		public int DefaultRowHeight
		{
			get
			{
				return this.defaultRowHeight;
			}

			set
			{
				if ( value != this.defaultRowHeight )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.defaultRowHeight = -1;
					else
						this.defaultRowHeight = value;
    		    	
					this.NotifyPropChange( PropertyIds.DefaultRowHeight, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeDefaultRowHeight() 
		{
			return ( this.defaultRowHeight >= 0 );
		}
 
		/// <summary>
		/// Resets DefaultRowHeight to its default value (-1).
		/// </summary>
		public void ResetDefaultRowHeight() 
		{
			this.DefaultRowHeight = -1;
		}
		
		/// <summary>
		/// Returns or sets a value that determines whether row expansion (plus/minus) indicators are displayed for "regular rows". For GroupBy rows use GroupByRowExpansionStye property.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property can be used to show expansion indicators for a row that has no children or hide them for a row that does.</p>
		///	<p class="body">The <b>Expanded</b> property can be used to indicate whether the expansion indicator appears expanded (minus) or collapsed (plus).</p>
		///	<p class="body">The <b>BeforeRowExpanded</b> and <b>BeforeRowCollapsed</b> events are generated when the user expands or collapses a row by clicking an expansion indicator.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_ExpansionIndicator")]
		[ LocalizedCategory("LC_Display") ]
		public ShowExpansionIndicator ExpansionIndicator
		{
			get
			{
				return this.expansionIndicator;
			}

			set
			{
				if ( value != this.expansionIndicator )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(ShowExpansionIndicator), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_209") );

					this.expansionIndicator = value;
					this.NotifyPropChange( PropertyIds.ExpansionIndicator, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeExpansionIndicator() 
		{
			return ( this.expansionIndicator != ShowExpansionIndicator.Default );
		}
 
		/// <summary>
		/// Resets ExpansionIndicator to its default setting.
		/// </summary>
		public void ResetExpansionIndicator() 
		{
			this.ExpansionIndicator = ShowExpansionIndicator.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines what will occur when the user clicks on a header.
		/// </summary>
		///	<remarks>
		///	<p class="body">Setting <b>HeaderClickAction</b> to enable column sorting disables selection via group headers. Group headers cannot be used for sorting; the <b>HeaderClickAction.SortSingle</b> and <b>HeaderClickAction.SortMulti</b> settings only affect column headers.</p>
		///	<p class="body">When this property is set to <b>HeaderClickAction.SortMulti</b>, the user can use the SHIFT key in combination with the mouse to select multiple columns for sorting. The order in which columns are selected is significant, determining the order in which the data will be sorted.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_HeaderClickAction")]
		[ LocalizedCategory("LC_Behavior") ]
		public HeaderClickAction HeaderClickAction
		{
			get
			{
				return this.headerClickAction;				
			}

			set
			{
				if ( value != this.headerClickAction )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(HeaderClickAction), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_210") );

					this.headerClickAction = value;
					this.NotifyPropChange( PropertyIds.HeaderClickAction, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeHeaderClickAction() 
		{
			return ( this.headerClickAction != HeaderClickAction.Default );
		}
 
		/// <summary>
		/// Resets HeaderClickAction to its default value.
		/// </summary>
		public void ResetHeaderClickAction() 
		{
			this.HeaderClickAction = HeaderClickAction.Default;
		}

		/// <summary>
		/// Determines the maximum number of cells that a user can select at any one time.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>MaxSelectedCells</b> property determines the maximum number of cells that can be selected at any one time in the band or the grid controlled by the specified override. This is an UltraGridOverride object property that can apply at either the grid level or the band level. When set at the band level, it determines how many cells may be simultaneously selected within the band. When applied at the grid level, it determines how many cells may be simultaneously selected in the entire control. The grid-level setting will override any band-level settings.</p>
		///	<p class="body">Setting <b>MaxSelectedCells</b> to 0 means there is no limit to the number of cells that may be selected simultaneously. Setting this property to a value of -1 will cause it to use the value from the next highest object in the override hierarchy.</p>
		///	<p class="body">This property operates independently of any column or row scrolling regions.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_MaxSelectedCells")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MaxSelectedCells
		{
			get
			{
				return this.maxSelectedCells;
			}

			set
			{
				if ( value != this.maxSelectedCells )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.maxSelectedCells = -1;
					else
						this.maxSelectedCells = value;
    		    	
					this.NotifyPropChange( PropertyIds.MaxSelectedCells, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMaxSelectedCells() 
		{
			return ( this.maxSelectedCells >= 0 );
		}
 
		/// <summary>
		/// Resets MaxSelectedCells to its default value (-1).
		/// </summary>
		public void ResetMaxSelectedCells() 
		{
			this.MaxSelectedCells = -1;
		}

		/// <summary>
		/// Determines the maximum number of rows that a user can select at any one time.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>MaxSelectedRows</b> property determines the maximum number of rows that can be selected at any one time in the band or the grid controlled by the specified override. This is an UltraGridOverride object property that can apply at either the grid level or the band level. When set at the band level, it determines how many rows may be simultaneously selected within the band. When applied at the grid level, it determines how many rows may be simultaneously selected in the entire control. The grid-level setting will override any band-level settings.</p>
		///	<p class="body">Setting <b>MaxSelectedRows</b> to 0 means there is no limit to the number of rows that may be selected simultaneously. Setting this property to a value of -1 will cause it to use the value from the next highest object in the override hierarchy.</p>
		///	<p class="body">This property operates independently of any row scrolling regions.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_MaxSelectedRows")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MaxSelectedRows
		{
			get
			{
				return this.maxSelectedRows;
			}

			set
			{
				if ( value != this.maxSelectedRows )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.maxSelectedRows = -1;
					else
						this.maxSelectedRows = value;
    		    	
					this.NotifyPropChange( PropertyIds.MaxSelectedRows, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMaxSelectedRows() 
		{
			return ( this.maxSelectedRows >= 0 );
		}
 
		/// <summary>
		/// Resets MaxSelectedRows to its default value (-1).
		/// </summary>
		public void ResetMaxSelectedRows() 
		{
			this.MaxSelectedRows = -1;
		}
		
		// JJD 1/21/02 - UWG815 Moved nulltext from layout to override and column
		//
		/// <summary>
		/// Returns or sets the string displayed in cells with null values.
		/// </summary>
		/// <remarks>
		/// You can use this property to customize the text displayed to the user when a null value is present in a cell. For example, you may want the cell to display "(empty)" or "no value". The default setting for this property is "null".
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_NullText")]
		[ LocalizedCategory("LC_Display") ]
		[ Localizable( true ) ]
		public string NullText
		{
			get
			{
				return this.nullText;
			}
			set			
			{			
				if ( this.nullText != value )
				{
					this.nullText = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.NullText );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeNullText() 
		{
			return ( this.nullText != null );
		}
 
		/// <summary>
		/// Resets NullText to its default value ("null").
		/// </summary>
		public void ResetNullText() 
		{
			this.NullText = null;
		}

		/// <summary>
		/// Returns or sets a value that determines whether row selectors will be displayed.
		/// </summary>
		///	<remarks>
		///	<p class="body">Row selectors are the part of the grid interface that appears at the left edge of each row. Row selectors provide information about the rows (which row is currently active, which rows have uncommitted edits) and you can click on a row selector to select the entire row at once. You can choose to display record selectors or not, either for the whole grid or on a band-by-band basis. The <b>RowSelectors</b> property specifies whether row selectors will be displayed in the band or the grid controlled by the specified override.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSelectors")]
		[ LocalizedCategory("LC_Display") ]
		public DefaultableBoolean RowSelectors
		{
			get
			{
				return this.rowSelectors;
			}

			set
			{
				if ( value != this.rowSelectors )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(DefaultableBoolean), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_211") );

					this.rowSelectors = value;
					this.NotifyPropChange( PropertyIds.RowSelectors, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSelectors() 
		{
			return ( this.rowSelectors != DefaultableBoolean.Default );
		}
 
		/// <summary>
		/// Resets RowSelectors to its default value.
		/// </summary>
		public void ResetRowSelectors() 
		{
			this.RowSelectors = DefaultableBoolean.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines the type of row sizing.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>RowSizing</b> property specifies whether the user can resize 
		///	rows using the mouse in the band or the grid controlled by the specified override 
		///	and, if they can, how that resizing is accomplished. The grid can also resize rows 
		///	automatically, based on the amount of data present in the cells that make up the 
		///	row. If one cell contains a large amount of text, the row can be resized to 
		///	accommodate all the text, or a particular number of lines of text, provided the 
		///	cell is capable of displaying multiple lines of text. The <b>RowSizing</b> 
		///	property also determines whether rows are resized independently of one another, 
		///	or whether their heights are synchronized.
		///	</p>
		///	<p class="body">When using one of the auto-sizing settings, the size of each row 
		///	is determined by the number of lines of text required to display the contents of 
		///	a cell. The cell in the row that displays the most lines of text determines the 
		///	size of the entire row. The <b>CellMultiLine</b> property is used to specify 
		///	whether the text in a cell will wrap to multiple lines. You can limit the number 
		///	of lines used by setting the <b>RowSizingAutoMaxLines</b> property.
		///	</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSizing")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.RowSizing RowSizing
		{
			get
			{
				return this.rowSizing;
			}

			set
			{
				if ( value != this.rowSizing )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(RowSizing), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_212") );

					this.rowSizing = value;
					this.NotifyPropChange( PropertyIds.RowSizing, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSizing() 
		{
			return ( this.rowSizing != RowSizing.Default );
		}
 
		/// <summary>
		/// Resets RowSizing to its default value.
		/// </summary>
		public void ResetRowSizing() 
		{
			this.RowSizing = RowSizing.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines which part of the grid's interface may be used to resize rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">If row resizing is enabled (as determined by the <b>RowSizing</b> property) the user can resize rows using the mouse. Resizing is always accomplished by clicking on the bottom edge of the row and dragging the mouse. The <b>RowSizingArea</b> property specifies which part of the row responds to the mouse pointer to initiate resizing of the row. You can choose to have just the record selectors, just the borders of the data area, or both be active forrow resizing. When the mouse pointer passes over the active area of the row, the cursor changes to a resizing cursor.</p>
		///	<p class="body">When setting a value for this property, you may want to consider whether the record selectors will remain visible at all times as your application runs, or whether they can be scrolled out of view, and what effect this will have on the ability of users to resize rows. Also, you will need to determine if having the row borders in the data area active for row resizing will interfere with other mouse operations in the grid and distract the user.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSizingArea")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.RowSizingArea RowSizingArea
		{
			get
			{
				return this.rowSizingArea;
			}

			set
			{
				if ( value != this.rowSizingArea )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(RowSizingArea), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_213") );

					this.rowSizingArea = value;
					this.NotifyPropChange( PropertyIds.RowSizingArea, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSizingArea() 
		{
			return ( this.rowSizingArea != RowSizingArea.Default );
		}
 
		/// <summary>
		/// Resets RowSizingArea to its default value.
		/// </summary>
		public void ResetRowSizingArea() 
		{
			this.RowSizingArea = RowSizingArea.Default;
		}

		#region RowSpacingBefore

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered before a row (between the top of the row and the bottom edge of the above object.)
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>RowSpacingBefore</b> property to specify the space that precedes a row in the band or the grid controlled by the specified override. Space between rows allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that follows a row, use the <b>RowSpacingAfter</b> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSpacingBefore")]
		[ LocalizedCategory("LC_Display") ]
		public int RowSpacingBefore
		{
			get
			{
				return this.rowSpacingBefore;
			}

			set
			{
				if ( value != this.rowSpacingBefore )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.rowSpacingBefore = -1;
					else
						this.rowSpacingBefore = value;
    		    	
					this.NotifyPropChange( PropertyIds.RowSpacingBefore, null );
				}
			}
		}

		#endregion // RowSpacingBefore

		#region ShouldSerializeRowSpacingBefore

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSpacingBefore() 
		{
			return ( this.rowSpacingBefore >= 0 );
		}

		#endregion // ShouldSerializeRowSpacingBefore
 
		#region ResetRowSpacingBefore

		/// <summary>
		/// Resets RowSpacingBefore to its default value (-1).
		/// </summary>
		public void ResetRowSpacingBefore() 
		{
			this.RowSpacingBefore = -1;
		}

		#endregion // ResetRowSpacingBefore

		#region RowSpacingAfter

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered after a row (between the bottom of the row and the top edge of the following object.)
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>RowSpacingAfter</b> property to specify the space that follows a row in the band or the grid controlled by the specified override. Space between rows allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that precedes a row, use the <b>RowSpacingBefore</b> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSpacingAfter")]
		[ LocalizedCategory("LC_Display") ]
		public int RowSpacingAfter
		{
			get
			{
				return this.rowSpacingAfter;
			}

			set
			{
				if ( value != this.rowSpacingAfter )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.rowSpacingAfter = -1;
					else
						this.rowSpacingAfter = value;
    		    	
					this.NotifyPropChange( PropertyIds.RowSpacingAfter, null );
				}
			}
		}

		#endregion // RowSpacingAfter

		#region ShouldSerializeRowSpacingAfter

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSpacingAfter() 
		{
			return ( this.rowSpacingAfter >= 0 );
		}
 
		#endregion // ShouldSerializeRowSpacingAfter

		#region ResetRowSpacingAfter

		/// <summary>
		/// Resets RowSpacingAfter to its default value (-1).
		/// </summary>
		public void ResetRowSpacingAfter() 
		{
			this.RowSpacingAfter = -1;
		}

		#endregion // ResetRowSpacingAfter

		#region GroupByRowSpacingBefore

		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered before a row (between the top of the row and the bottom edge of the above object.)
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>RowSpacingBefore</b> property to specify the space that precedes a row in the band or the grid controlled by the specified override. Space between rows allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that follows a row, use the <b>RowSpacingAfter</b> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByRowSpacingBefore")]
		[ LocalizedCategory("LC_Display") ]
		public int GroupByRowSpacingBefore
		{
			get
			{
				return this.groupByRowSpacingBefore;
			}

			set
			{
				if ( value != this.groupByRowSpacingBefore )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.groupByRowSpacingBefore = -1;
					else
						this.groupByRowSpacingBefore = value;
    		    	
					this.NotifyPropChange( PropertyIds.GroupByRowSpacingBefore, null );
				}
			}
		}

		#endregion // GroupByRowSpacingBefore

		#region ShouldSerializeGroupByRowSpacingBefore

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByRowSpacingBefore() 
		{
			return ( this.groupByRowSpacingBefore >= 0 );
		}

		#endregion // ShouldSerializeGroupByRowSpacingBefore
 
		#region ResetGroupByRowSpacingBefore

		/// <summary>
		/// Resets GroupByRowSpacingBefore to its default value (-1).
		/// </summary>
		public void ResetGroupByRowSpacingBefore() 
		{
			this.GroupByRowSpacingBefore = -1;
		}

		#endregion // ResetGroupByRowSpacingBefore

		#region GroupByRowSpacingAfter

		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered after a row (between the bottom of the row and the top edge of the following object.)
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>RowSpacingAfter</b> property to specify the space that follows a row in the band or the grid controlled by the specified override. Space between rows allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that precedes a row, use the <b>RowSpacingBefore</b> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByRowSpacingAfter")]
		[ LocalizedCategory("LC_Display") ]
		public int GroupByRowSpacingAfter
		{
			get
			{
				return this.groupByRowSpacingAfter;
			}

			set
			{
				if ( value != this.groupByRowSpacingAfter )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.groupByRowSpacingAfter = -1;
					else
						this.groupByRowSpacingAfter = value;
    		    	
					this.NotifyPropChange( PropertyIds.GroupByRowSpacingAfter, null );
				}
			}
		}

		#endregion // GroupByRowSpacingAfter

		#region ShouldSerializeGroupByRowSpacingAfter

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByRowSpacingAfter() 
		{
			return ( this.groupByRowSpacingAfter >= 0 );
		}
 
		#endregion // ShouldSerializeGroupByRowSpacingAfter

		#region ResetGroupByRowSpacingAfter

		/// <summary>
		/// Resets GroupByRowSpacingAfter to its default value (-1).
		/// </summary>
		public void ResetGroupByRowSpacingAfter() 
		{
			this.GroupByRowSpacingAfter = -1;
		}

		#endregion // ResetGroupByRowSpacingAfter

		/// <summary>
		/// Returns or sets the maximum number of lines a row will display when Auto-Sizing is enabled.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>RowSizing</b> property to specify that the control should automatically adjust the height of rows to accommodate multiple lines of text in the band or the grid controlled by the specified override. If a row contains one or more cells with the <b>CellMultiLine</b> property set to display more than one line of text, the row can resize itself so that all the text in the cell(s) is visible. Depending on the setting of <b>RowSizing</b>, just the row containing a multiline cell may be resized, or all the rows in the band or grid may be resized to match the one containing the multiline cell.</p>
		///	<p class="body">The <b>RowSizingAutoMaxLines</b> property is used to limit amount of row resizing the control will use to accommodate multiline cells. If one or more rows are being resized to display multiple lines of text, their height will only be increased enough to display the number of lines of text specified by this property. Use this property when you have rows that are being automatically resized and you want to display memo or long text fields in a multiline cell, but do not want rows growing too tall and disrupting the overall layout of the grid.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSizingAutoMaxLines")]
		[ LocalizedCategory("LC_Display") ]
		public int RowSizingAutoMaxLines
		{
			get
			{
				return this.rowSizingAutoMaxLines;
			}

			set
			{
				if ( value != this.rowSizingAutoMaxLines )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.rowSizingAutoMaxLines = -1;
					else
						this.rowSizingAutoMaxLines = value;
    		    	
					this.NotifyPropChange( PropertyIds.RowSizingAutoMaxLines, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSizingAutoMaxLines() 
		{
			return ( this.rowSizingAutoMaxLines >= 0 );
		}
 
		/// <summary>
		/// Resets RowSizingMaxLines to its default value (-1).
		/// </summary>
		public void ResetRowSizingAutoMaxLines() 
		{
			this.RowSizingAutoMaxLines = -1;
		}

		/// <summary>
		/// Returns or sets a value that determines the cell selection type.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to specify which selection type will be used for the cells in the band or the grid controlled by the specified override. You can choose to allow the user to have multiple cells selected, only one cell at a time selected, or to disallow the selection of cells.</p>
		///	<p class="body">You can use the <b>SelectTypeCol</b> and <b>SelectTypeRow</b> properties to specify the way in which columns and rows may be selected.</p>
		///	<p class="body">Because you may want to enable different types of selection at different levels of a hierarchical record set, <b>SelectTypeCell</b> is a property of the UltraGridOverride object. This makes it easy to specify different selection options for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's setting for <b>SelectTypeCell</b>, and the top-level band will use the grid's setting.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SelectTypeCell")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.SelectType SelectTypeCell
		{
			get
			{
				return this.selectTypeCell;
			}

			set
			{
				if ( value != this.selectTypeCell )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(SelectType), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_214") );

					this.selectTypeCell = value;

					// clear the cached selection strategy for cells
					//
					this.selectionStrategyCell = null;

					this.NotifyPropChange( PropertyIds.SelectTypeCell, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSelectTypeCell() 
		{
			return ( this.selectTypeCell != SelectType.Default );
		}
 
		/// <summary>
		/// Resets SelectTypeCell to its default value.
		/// </summary>
		public void ResetSelectTypeCell() 
		{
			this.SelectTypeCell = SelectType.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines the column selection type.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to specify which selection type will be used for the columns in the band or the grid controlled by the specified override. You can choose to allow the user to have multiple columns selected, only one column at a time selected, or to disallow the selection of columns.</p>
		///	<p class="body">You can use the <b>SelectTypeCell</b> and <b>SelectTypeRow</b> properties to specify the way in which cells and rows may be selected.</p>
		///	<p class="body">Because you may want to enable different types of selection at different levels of a hierarchical record set, <b>SelectTypeCol</b> is a property of the UltraGridOverride object. This makes it easy to specify different selection options for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's setting for <b>SelectTypeCol</b>, and the top-level band will use the grid's setting.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SelectTypeCol")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.SelectType SelectTypeCol
		{
			get
			{
				return this.selectTypeCol;
			}

			set
			{
				if ( value != this.selectTypeCol )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(SelectType), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_215") );

					this.selectTypeCol = value;

					// clear the cached selection strategy for columns
					//
					this.selectionStrategyColumn = null;

					this.NotifyPropChange( PropertyIds.SelectTypeCol, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSelectTypeCol() 
		{
			return ( this.selectTypeCol != SelectType.Default );
		}
 

		/// <summary>
		/// Reset SelectTypeCol to its default value.
		/// </summary>
		public void ResetSelectTypeCol() 
		{
			this.SelectTypeCol = SelectType.Default;
		}


		/// <summary>
		/// Returns or sets a value that determines the row selection type.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to specify which selection type will be used for the rows in the band or the grid controlled by the specified override. You can choose to allow the user to have multiple rows selected, only one row at a time selected, or to disallow the selection of rows.</p>
		///	<p class="body">You can use the <b>SelectTypeCol</b> and <b>SelectTypeCell</b> properties to specify the way in which columns and cells may be selected.</p>
		///	<p class="body">Because you may want to enable different types of selection at different levels of a hierarchical record set, <b>SelectTypeRow</b> is a property of the UltraGridOverride object. This makes it easy to specify different selection options for each band by assigning each UltraGridBand object its own UltraGridOverride object. If a band does not have an override assigned to it, the control will use the override at the next higher level of the override hierarchy to determine the properties for that band. In other words, any band without an override will use its parent band's setting for <b>SelectTypeRow</b>, and the top-level band will use the grid's setting.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SelectTypeRow")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.SelectType SelectTypeRow
		{
			get
			{
				return this.selectTypeRow;
			}

			set
			{
				if ( value != this.selectTypeRow )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(SelectType), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_216") );

					this.selectTypeRow = value;

					// clear the cached selection strategy for rows
					//
					this.selectionStrategyRow = null;

					this.NotifyPropChange( PropertyIds.SelectTypeRow, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSelectTypeRow() 
		{
			return ( this.selectTypeRow != SelectType.Default );
		}
 
		/// <summary>
		/// Resets SelectTypeRow to its default value.
		/// </summary>
		public void ResetSelectTypeRow() 
		{
			this.SelectTypeRow = SelectType.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines whether a tip will be displayed when the mouse pauses over a cell.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property determines whether the cells of the band or the grid controlled by the specified override will be capable of displaying pop-up tips. Cell tips display the contents of the cell, and generally only appear when the cell's area is not large enough to display all the data it contains, and the mouse has come to rest over the cell for a period of time.</p>
		///	<p><seealso cref="UltraGridCell.ToolTipText"/></p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_TipStyleCell")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.TipStyle TipStyleCell
		{
			get
			{
				return this.tipStyleCell;
			}

			set
			{
				if ( value != this.tipStyleCell )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(TipStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_217") );

					this.tipStyleCell = value;
					this.NotifyPropChange( PropertyIds.TipStyleCell, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeTipStyleCell() 
		{
			return ( this.tipStyleCell != TipStyle.Default );
		}
 
		/// <summary>
		/// Resets TipStyleCell to its default value.
		/// </summary>
		public void ResetTipStyleCell() 
		{
			this.TipStyleCell = TipStyle.Default;
		}

		// SSP 3/3/06 BR10430
		// Added TipStyleHeader property.
		// 
		#region TipStyleHeader

		/// <summary>
		/// Returns or sets a value that determines whether a tip will be displayed when the mouse pauses over a header.
		/// </summary>
		///	<remarks>
		///	<p class="body">
		///	This property determines whether the headers of the band or the grid controlled by this
		///	override will be capable of displaying pop-up tool-tips. By default the header tips display 
		///	the caption of the header if the caption is not fully visible. You can use the 
		///	<see cref="HeaderBase.ToolTipText"/> property of the <see cref="HeaderBase"/> to specify a 
		///	custom text to display in the header tooltip.
		///	</p>
		///	<seealso cref="HeaderBase.ToolTipText"/> <seealso cref="UltraGridCell.ToolTipText"/> 
		///	<seealso cref="UltraGridRow.ToolTipText"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_TipStyleHeader")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.TipStyle TipStyleHeader
		{
			get
			{
				return this.tipStyleHeader;
			}

			set
			{
				if ( value != this.tipStyleHeader )
				{
					// Test that the value is in range.
					//
					GridUtils.ValidateEnum( typeof( TipStyle ), value );

					this.tipStyleHeader = value;

					this.NotifyPropChange( PropertyIds.TipStyleHeader, null );
				}
			}
		}

		#endregion // TipStyleHeader

		#region ShouldSerializeTipStyleHeader

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeTipStyleHeader() 
		{
			return ( this.tipStyleHeader != TipStyle.Default );
		}

		#endregion // ShouldSerializeTipStyleHeader
 
		#region ResetTipStyleHeader

		/// <summary>
		/// Resets TipStyleHeader to its default value.
		/// </summary>
		public void ResetTipStyleHeader() 
		{
			this.TipStyleHeader = TipStyle.Default;
		}

		#endregion // ResetTipStyleHeader


		/// <summary>
		/// Returns or sets a value that determines whether a tip will be displayed when the mouse pauses over a row connector line.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property determines whether the lines connecting the rows of the band or the grid controlled by the specified override will be capable of displaying pop-up tips. When using hierarchical recordsets, often the parent record of a band will be out of view, above the top or below the bottom of the control. Row connector tips are a convenient way to discover which record is the parent of the data currently being displayed without having to scroll the control.</p>
		///	<p class="body">Row connector tips display data from a record that is attached to the connector line when the mouse has come to rest over the line for a period of time. The tip displays the name and value of one field in the record, as determined by the <b>ScrollTipField</b> property of the band in which the line is located.</p>
		///	<p class="body">When the mouse pointer passes over a connector line, it changes to a special connector line cursor that indicates the direction of the record whose data is being displayed in the pop-up tip. Normally, this cursor is an upward-pointing double arrow and the pop-up tip displays data from the previous record connected to line. But if the CTRL key on the keyboard is depressed, the mouse pointer changes to a downward-pointing double arrow and the pop-up tip displays data from the following record connected to line.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_TipStyleRowConnector")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.TipStyle TipStyleRowConnector
		{
			get
			{
				return this.tipStyleRowConnector;
			}

			set
			{
				if ( value != this.tipStyleRowConnector )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(TipStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_218") );

					this.tipStyleRowConnector = value;
					this.NotifyPropChange( PropertyIds.TipStyleRowConnector, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null)
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeTipStyleRowConnector() 
		{
			return ( this.tipStyleRowConnector != TipStyle.Default );
		}
 
		/// <summary>
		/// Resets TipStyleRowConnector to its default value.
		/// </summary>
		public void ResetTipStyleRowConnector() 
		{
			this.TipStyleRowConnector = TipStyle.Default;
		}

		/// <summary>
		/// Returns or sets a value that determines whether a tip displayed over the scrollbar when the scroll bar thumb is dragged.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property determines whether the scrollbar of the band or the grid controlled by the specified override will display pop-up tool-tips. 
		///	By default when you drag the scrollbar thumb to scroll through a recordset, the data is 
		///	not scrolled in the grid until you release the mouse button to reposition the thumb. 
		///	When <b>TipStyleScroll</b> is set to display scroll tips, a pop-up tip will appear over 
		///	the thumb indicating which record will appear at the top of the grid when the scrollbar 
		///	is released. The <b>ScrollTipField</b> property is used to specify which field from 
		///	the data record will be displayed in the pop-up tip.
		///	</p>
		///	<p class="body"><b>NOTE:</b> <see cref="UltraGridLayout.ScrollStyle"/> must be set to to <b>Deferred</b> in order for this property to have any effect.</p>
		///	<seealso cref="UltraGridLayout.ScrollStyle"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_TipStyleScroll")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.TipStyle TipStyleScroll
		{
			get
			{
				return this.tipStyleScroll;
			}

			set
			{
				if ( value != this.tipStyleScroll )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(TipStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_219") );

					this.tipStyleScroll = value;
					this.NotifyPropChange( PropertyIds.TipStyleScroll, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeTipStyleScroll() 
		{
			return ( this.tipStyleScroll != TipStyle.Default );
		}
 
		/// <summary>
		/// Resets TipStyleScroll to its default value.
		/// </summary>
		public void ResetTipStyleScroll() 
		{
			this.TipStyleScroll = TipStyle.Default;
		}

		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			if ( null != this.appearanceHolders )
			{
				// loop thru our appearanceholder object table to see if the Source was one
				// of these. If so, call NotifyPropChanged with that appearance property's
				// id from our global table
				//
				for ( int i = 0 ; i < this.appearanceHolders.Length; i++ )
				{
					// JJD 11/19/01
					// Compare against RootAppearance instead of Appearance
					//
					if ( this.appearanceHolders[i] != null &&
						this.appearanceHolders[i].HasAppearance &&
						propChange.Source == this.appearanceHolders[i].RootAppearance )
					{
                        
						this.NotifyPropChange( UltraGridOverride.AppearancePropIds[i], propChange );
						return;
					}

				}
			}

            // CDS 9.2 Column Moving Indicators
            if (propChange.PropId is DragDropIndicatorSettingsPropIds)
            {
                // if the source matches the dragDropIndicatorSettings member, then forward the notification
                // otherwise, it is being fired by a temporary DragDropIndicatorSettings object.
                if (propChange.Source == this.dragDropIndicatorSettings)
                {
                    // CDS 5/4/09 TFS17276
                    if (this.layout != null)
                        StyleUtils.DirtyCachedPropertyVals(layout);
                    this.NotifyPropChange(PropertyIds.DragDropIndicatorSettings, propChange);
                }
                return;
            }

			Debug.Assert( false, "Unknown sub object in Override.OnSubObjectPropChanged" );
			// pass the notification along to our listeners
			//
			//            this.NotifyPropChange( PropertyIds.Override, propChange );
		}

		// SSP 3/16/06 - App Styling
		// Commented out GetAppearenceFontHeight method. Instead use the band's 
		// GetBLOverrideAppearanceFontHeight method.
		// 
		

		/// <summary>
		/// Returns the default GroupByColumnAppearance
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByColumnAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase GroupByColumnAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.GroupByColumn );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.GroupByColumn, value );
			}
		}

		/// <summary>
		/// Returns true if an GroupByColumnAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasGroupByColumnAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.GroupByColumn );
			}
		}

		/// <summary>
		/// Returns true if the GroupByColumnAppearance needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByColumnAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.GroupByColumn ); 
		}
 
		/// <summary>
		/// Resets GroupByColumnAppearance
		/// </summary>
		public void ResetGroupByColumnAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.GroupByColumn ); 
		}

		/// <summary>
		/// Returns the default GroupByColumnHeaderAppearance
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByColumnHeaderAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase GroupByColumnHeaderAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.GroupByColumnHeader );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.GroupByColumnHeader, value );
			}
		}

		/// <summary>
		/// Returns true if an GroupByColumnHeaderAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasGroupByColumnHeaderAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.GroupByColumnHeader );
			}
		}

		/// <summary>
		/// Returns true if the GroupByColumnHeaderAppearance needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByColumnHeaderAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.GroupByColumnHeader ); 
		}
 
		/// <summary>
		/// Resets GroupByColumnHeaderAppearance
		/// </summary>
		public void ResetGroupByColumnHeaderAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.GroupByColumnHeader ); 
		}

		/// <summary>
		/// Returns the default GroupByRowAppearance
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase GroupByRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.GroupByRow );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.GroupByRow, value );
			}
		}

		/// <summary>
		/// Returns true if an GroupByRowAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasGroupByRowAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.GroupByRow );
			}
		}

		/// <summary>
		/// Returns true if the GroupByRowAppearance needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.GroupByRow ); 
		}
 
		/// <summary>
		/// Resets GroupByRowAppearance
		/// </summary>
		public void ResetGroupByRowAppearance() 
		{
			this.ResetAppearance( OverrideAppearanceIndex.GroupByRow ); 
		}

		/// <summary>
		/// Specifies padding in pixels used around the GroupByRow's description. If set to -1 then 1 will be used. Default is -1.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByRowPadding")]
		[ LocalizedCategory("LC_Display") ]
		public int GroupByRowPadding
		{
			get
			{
				return this.groupByRowPadding;
			}
			set
			{
				if ( value != this.groupByRowPadding )
				{
					// test that the value is in range
					//
					if ( value < -1 )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_220") );

					this.groupByRowPadding = value;
					this.NotifyPropChange( PropertyIds.GroupByRowPadding, null );
				}
			}
		}
	
		/// <summary>
		/// A string that determines what text be shown in groupby rows. It can include 
		/// special substitution strings (e.g. to specify child row count).
		/// </summary>
		/// <remarks>The default string is "[caption] : [value] ([count] [count,items,item,items])". 
		/// <p></p>
		/// <p>Note: the replaceable mask items are:</p>
		/// <p></p>
		/// <p>[caption] replaced with the groupby column headers caption.</p>
		/// <p>[value] replaced by the common cell value for this group by.</p>
		/// <p>[count] - the number of visible child rows.</p> 
		/// <p>[count,x,y,z] - If count is 0 will substitute the x string, or if count is 1 will substitute the y string otherwise will substitute the z string. </p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByRowDescriptionMask")]
		[ LocalizedCategory("LC_Display") ]
		[ Localizable( true ) ]
		public string GroupByRowDescriptionMask
		{
			get
			{
				return this.groupByRowDescriptionMask;
			}
			set
			{
				if ( value != this.groupByRowDescriptionMask )
				{
					this.groupByRowDescriptionMask = value;
					this.NotifyPropChange( PropertyIds.GroupByRowDescriptionMask, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the GroupByRowDescriptionMask needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByRowDescriptionMask( ) 
		{
			return null != this.groupByRowDescriptionMask
				// SSP 11/29/01 UWG789
				// Also check for empty string as well.
				//
				&& 0 != this.groupByRowDescriptionMask.Length;
		}
 
		/// <summary>
		/// Reset GroupByRowDescriptionMask
		/// </summary>
		public void ResetGroupByRowDescriptionMask() 
		{
			this.GroupByRowDescriptionMask = null;
		}


		/// <summary>
		/// Returns true if the GroupByRowPadding needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupByRowPadding( ) 
		{
			return -1 != this.groupByRowPadding;
		}
 
		/// <summary>
		/// Reset GroupByRowPadding
		/// </summary>
		public void ResetGroupByRowPadding() 
		{
			this.GroupByRowPadding = -1;
		}

		/// <summary>
		/// Determines how and if GroupByRows can be selected. Default is resolved to Extended.
		/// </summary>
		[ LocalizedDescription("LD_UltraGridOverride_P_SelectTypeGroupByRow")]
		[ LocalizedCategory("LC_Behavior") ]
		public SelectType SelectTypeGroupByRow
		{
			get
			{
				return this.selectTypeGroupByRow;
			}
			set
			{
				
				if ( value != this.selectTypeGroupByRow )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(SelectType), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_221") );

					this.selectTypeGroupByRow = value;

					// clear the cached selection strategy for GroupByRow's
					//
					this.selectionStrategyGroupByRow = null;

					this.NotifyPropChange( PropertyIds.SelectTypeGroupByRow, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the SelectTypeGroupByRow needs to be serialized
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSelectTypeGroupByRow( ) 
		{
			return SelectType.Default != this.selectTypeGroupByRow;
		}
 
		/// <summary>
		/// Reset SelectTypeGroupByRow
		/// </summary>
		public void ResetSelectTypeGroupByRow() 
		{
			this.SelectTypeGroupByRow = SelectType.Default;
		}


		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";
			
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//PropertyCategories propCat = context.Context is PropertyCategories
			//    ? (PropertyCategories)context.Context	: PropertyCategories.All;

			//all values that were set are now save into SerializationInfo
			if(this.ShouldSerializeAllowAddNew())
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowAddNew", this.allowAddNew );
				//info.AddValue("AllowAddNew", (int)this.allowAddNew );
			}

			if ( this.ShouldSerializeKey() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Key", this.Key );
				//info.AddValue("Key", this.Key );
			}

			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	//info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
			
			if(this.ShouldSerializeAllowColMoving())
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowColMoving", this.allowColMoving );
				//info.AddValue("AllowColMoving", (int)this.allowColMoving );
			}

			if( this.ShouldSerializeAllowColSizing() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowColSizing", this.allowColSizing );
				//info.AddValue("AllowColSizing", (int)this.allowColSizing );
			}

			if(	this.ShouldSerializeAllowColSwapping() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowColSwapping", this.allowColSwapping );
				//info.AddValue("AllowColSwapping", (int)this.allowColSwapping );
			}

			if(	this.ShouldSerializeAllowDelete() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowDelete", this.allowDelete );
				//info.AddValue("AllowDelete", (int)this.allowDelete );
			}

			if(	this.ShouldSerializeAllowGroupMoving() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowGroupMoving", this.allowGroupMoving );
				//info.AddValue("AllowGroupMoving", (int)this.allowGroupMoving );
			}

			if(	this.ShouldSerializeAllowGroupSwapping() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowGroupSwapping", this.allowGroupSwapping );
				//info.AddValue("AllowGroupSwapping", (int)this.allowGroupSwapping );
			}

			if(	this.ShouldSerializeAllowUpdate() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowUpdate", this.allowUpdate );
				//info.AddValue("AllowUpdate", (int)this.allowUpdate );
			}

			if(	this.ShouldSerializeBorderStyleRow() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyleRow", this.borderStyleRow );
				//info.AddValue("BorderStyleRow", (int)this.borderStyleRow );
			}

			//JM 01-16-02 The following is no longer needed - now using
			//			  BorderStyleRow for cards
			//if(	this.ShouldSerializeBorderStyleCard() )
			//	//info.AddValue("BorderStyleCard", (int)this.borderStyleCard );

			if(	this.ShouldSerializeBorderStyleCardArea() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyleCardArea", this.borderStyleCardArea );
				//info.AddValue("BorderStyleCardArea", (int)this.borderStyleCardArea );
			}

			if(	this.ShouldSerializeBorderStyleCell() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyleCell", this.borderStyleCell );
				//info.AddValue("BorderStyleCell", (int)this.borderStyleCell );
			}

			if(	this.ShouldSerializeBorderStyleHeader() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyleHeader", this.borderStyleHeader );
				//info.AddValue("BorderStyleHeader", (int)this.borderStyleHeader );
			}

			if(	this.ShouldSerializeCellClickAction() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info,  "CellClickAction", this.cellClickAction);
				//info.AddValue("CellClickAction", (int)this.cellClickAction );
			}

			if(	this.ShouldSerializeCellMultiLine() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CellMultiLine", this.cellMultiLine );
				//info.AddValue("CellMultiLine", (int)this.cellMultiLine );
			}

			if(	this.ShouldSerializeCellPadding() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CellPadding", this.cellPadding );
				//info.AddValue("CellPadding", this.cellPadding );
			}

			if(	this.ShouldSerializeCellSpacing() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CellSpacing", this.cellSpacing );
				//info.AddValue("CellSpacing", this.cellSpacing );
			}

			if(	this.ShouldSerializeDefaultColWidth() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "DefaultColWidth", this.defaultColWidth );
				//info.AddValue("DefaultColWidth", this.defaultColWidth );
			}

			if(	this.ShouldSerializeDefaultRowHeight() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "DefaultRowHeight", this.defaultRowHeight );
				//info.AddValue("DefaultRowHeight", this.defaultRowHeight );
			}

			//	if(	this.ShouldSerializeExpandRowsOnLoad() )
			//		//info.AddValue("ExpandRowsOnLoad", (int)this.expandRowsOnLoad );

			if(	this.ShouldSerializeExpansionIndicator() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ExpansionIndicator", this.expansionIndicator );
				//info.AddValue("ExpansionIndicator", (int)this.expansionIndicator );
			}

			if(	this.ShouldSerializeHeaderClickAction() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "HeaderClickAction", this.headerClickAction );
				//info.AddValue("HeaderClickAction", (int)this.headerClickAction );
			}

			if(	this.ShouldSerializeMaxSelectedCells() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaxSelectedCells", this.maxSelectedCells );
				//info.AddValue("MaxSelectedCells", this.maxSelectedCells );
			}

			if(	this.ShouldSerializeMaxSelectedRows() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaxSelectedRows", this.maxSelectedRows );
				//info.AddValue("MaxSelectedRows", this.maxSelectedRows );
			}

			// JJD 1/21/02 - UWG815 - Moved from layout 
			//
			if ( this.ShouldSerializeNullText() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "NullText", this.NullText );
				//info.AddValue("NullText", this.NullText );
			}

			if(	this.ShouldSerializeRowSelectors() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowSelectors", this.rowSelectors );
				//info.AddValue("RowSelectors", (int)this.rowSelectors );
			}

			if(	this.ShouldSerializeRowSizing() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowSizing", this.rowSizing );
				//info.AddValue("RowSizing", (int)this.rowSizing );
			}

			if(	this.ShouldSerializeRowSizingArea() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowSizingArea", this.rowSizingArea );
				//info.AddValue("RowSizingArea", (int)this.rowSizingArea );
			}

			if(	this.ShouldSerializeRowSizingAutoMaxLines() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowSizingAutoMaxLines", this.rowSizingAutoMaxLines );
				//info.AddValue("RowSizingAutoMaxLines", this.rowSizingAutoMaxLines );
			}

			if(	this.ShouldSerializeRowSpacingBefore() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowSpacingBefore", this.rowSpacingBefore );
				//info.AddValue("RowSpacingBefore", this.rowSpacingBefore );
			}

			if(	this.ShouldSerializeRowSpacingAfter() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowSpacingAfter", this.rowSpacingAfter );
				//info.AddValue("RowSpacingAfter", this.rowSpacingAfter );
			}

			// SSP 12/21/04 BR01386
			// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
			//
			if(	this.ShouldSerializeGroupByRowSpacingBefore() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "GroupByRowSpacingBefore", this.groupByRowSpacingBefore );
				//info.AddValue("RowSpacingBefore", this.rowSpacingBefore );
			}

			// SSP 12/21/04 BR01386
			// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
			//
			if(	this.ShouldSerializeGroupByRowSpacingAfter() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "GroupByRowSpacingAfter", this.groupByRowSpacingAfter );
				//info.AddValue("RowSpacingAfter", this.rowSpacingAfter );
			}

			if(	this.ShouldSerializeSelectTypeCell() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SelectTypeCell", this.selectTypeCell );
				//info.AddValue("SelectTypeCell", (int)this.selectTypeCell );
			}

			if(	this.ShouldSerializeSelectTypeCol() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SelectTypeCol", this.selectTypeCol );
				//info.AddValue("SelectTypeCol", (int)this.selectTypeCol );
			}

			if(	this.ShouldSerializeSelectTypeRow() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SelectTypeRow", this.selectTypeRow );
				//info.AddValue("SelectTypeRow", (int)this.selectTypeRow );
			}

			if(	this.ShouldSerializeTipStyleCell() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "TipStyleCell", this.tipStyleCell );
				//info.AddValue("TipStyleCell", (int)this.tipStyleCell );
			}

			// SSP 3/3/06 BR10430
			// Added TipStyleHeader property.
			// 
			if(	this.ShouldSerializeTipStyleHeader() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "TipStyleHeader", this.tipStyleHeader );
				//info.AddValue("TipStyleCell", (int)this.tipStyleCell );
			}

			if(	this.ShouldSerializeTipStyleRowConnector() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info,"TipStyleRowConnector", this.tipStyleRowConnector  );
				//info.AddValue("TipStyleRowConnector", (int)this.tipStyleRowConnector );
			}

			if(	this.ShouldSerializeTipStyleScroll() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "TipStyleScroll", this.tipStyleScroll );
				//info.AddValue("TipStyleScroll", (int)this.tipStyleScroll);
			}


			if ( this.ShouldSerializeGroupByRowDescriptionMask( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "GroupByRowDescriptionMask", this.GroupByRowDescriptionMask );
				//info.AddValue("GroupByRowDescriptionMask", this.GroupByRowDescriptionMask);
			}

			if ( this.ShouldSerializeGroupByRowPadding( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "GroupByRowPadding", this.GroupByRowPadding );
				//info.AddValue("GroupByRowPadding", this.GroupByRowPadding );
			}

			if ( this.ShouldSerializeGroupByColumnsHidden( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "GroupByColumnsHidden", this.GroupByColumnsHidden );
				//info.AddValue("GroupByColumnsHidden", (int)this.GroupByColumnsHidden );
			}

			if ( this.ShouldSerializeAllowGroupBy( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowGroupBy", this.AllowGroupBy );
				//info.AddValue("AllowGroupBy", (int)this.AllowGroupBy );
			}

			if ( this.ShouldSerializeSelectTypeGroupByRow( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SelectTypeGroupByRow", this.SelectTypeGroupByRow );
				//info.AddValue("SelectTypeGroupByRow", (int)this.SelectTypeGroupByRow );
			}

			// SSP 6/11/02
			// Summary rows feature.
			//
			if ( this.ShouldSerializeAllowRowFiltering( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowRowFiltering", this.allowRowFiltering );
				//info.AddValue( "AllowRowFiltering", (int)this.allowRowFiltering );
			}
			if ( this.ShouldSerializeRowFilterMode( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowFilterMode", this.rowFilterMode );
				//info.AddValue( "RowFilterMode", (int)this.rowFilterMode );
			}
			if ( this.ShouldSerializeAllowRowSummaries( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AllowRowSummaries", this.allowRowSummaries );
				//info.AddValue( "AllowRowSummaries", (int)this.allowRowSummaries );
			}
			if ( this.ShouldSerializeBorderStyleSummaryFooter( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyleSummaryFooter", this.borderStyleSummaryFooter );
				//info.AddValue( "BorderStyleSummaryFooter", (int)this.borderStyleSummaryFooter );
			}
			if ( this.ShouldSerializeBorderStyleSummaryValue( )  )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyleSummaryValue", this.borderStyleSummaryValue );
				//info.AddValue( "BorderStyleSummaryValue", (int)this.borderStyleSummaryValue );
			}
			if ( this.ShouldSerializeBorderStyleSummaryFooterCaption( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyleSummaryFooterCaption", this.borderStyleSummaryFooterCaption );
				//info.AddValue( "BorderStyleSummaryFooterCaption", (int)this.borderStyleSummaryFooterCaption );
			}

			// SSP 7/19/02 UWG1369
			// Moved SummaryFooterCaptionVisible property from band to override.
			//
			if ( this.ShouldSerializeSummaryFooterCaptionVisible( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SummaryFooterCaptionVisible", this.SummaryFooterCaptionVisible );
				//info.AddValue( "SummaryFooterCaptionVisible", (int)this.SummaryFooterCaptionVisible );
			}

			// SSP 8/20/02
			// Added an entry for card spacing. It wasn't getting serialized before.
			//
			if ( this.ShouldSerializeCardSpacing( ) )
			{
				Utils.SerializeProperty( info, "CardSpacing", this.CardSpacing );
			}

			// SSP 2/14/03 - RowSelectorWidth property
			// Added RowSelectorWidth property to allow the user to control the widths
			// of the row selectors.
			//
			if ( this.ShouldSerializeRowSelectorWidth( ) )
			{
				Utils.SerializeProperty( info, "RowSelectorWidth", this.RowSelectorWidth );
			}

			// SSP 2/28/03 - Row Layout Functionality
			//
			// ----------------------------------------------------------------------------------
			if ( this.ShouldSerializeBorderStyleRowSelector( ) )
			{
				Utils.SerializeProperty( info, "BorderStyleRowSelector", this.BorderStyleRowSelector );
			}

			if ( this.ShouldSerializeAllowRowLayoutCellSizing( ) )
			{
				Utils.SerializeProperty( info, "AllowRowLayoutCellSizing", this.AllowRowLayoutCellSizing );
			}

			if ( this.ShouldSerializeAllowRowLayoutLabelSizing( ) )
			{
				Utils.SerializeProperty( info, "AllowRowLayoutLabelSizing", this.AllowRowLayoutLabelSizing );
			}
			// ----------------------------------------------------------------------------------

			// SSP 4/14/03
			// Added ColumnAutoSizeMode property.
			//
			if ( this.ShouldSerializeColumnAutoSizeMode( ) )
			{
				Utils.SerializeProperty( info, "ColumnAutoSizeMode", this.ColumnAutoSizeMode );
			}

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			if ( this.ShouldSerializeCellDisplayStyle( ) )
			{
				Utils.SerializeProperty( info, "CellDisplayStyle", this.CellDisplayStyle );
			}

			// SSP 5/19/03 - Fixed headers
			//
			// --------------------------------------------------------------------------------------
			if ( this.ShouldSerializeFixedHeaderIndicator( ) )
			{
				Utils.SerializeProperty( info, "FixedHeaderIndicator", this.FixedHeaderIndicator );
			}

			if ( this.ShouldSerializeFixedCellSeparatorColor( ) )
			{
				Utils.SerializeProperty( info, "FixedCellSeparatorColor", this.FixedCellSeparatorColor );
			}
			// --------------------------------------------------------------------------------------

			// SSP 8/1/03 UWG1654 - Filter Action
			// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
			// appearances.
			//
			// --------------------------------------------------------------------------------------
			if ( this.ShouldSerializeRowFilterAction( ) )
			{
				Utils.SerializeProperty( info, "RowFilterAction", this.RowFilterAction );
			}
			// --------------------------------------------------------------------------------------

			// SSP 11/7/03 - Add Row Feature
			//
			// --------------------------------------------------------------------------------------
			if ( this.ShouldSerializeTemplateAddRowSpacingAfter( ) )
			{
				Utils.SerializeProperty( info, "TemplateAddRowSpacingAfter", this.TemplateAddRowSpacingAfter );
			}

			if ( this.ShouldSerializeTemplateAddRowSpacingBefore( ) )
			{
				Utils.SerializeProperty( info, "TemplateAddRowSpacingBefore", this.TemplateAddRowSpacingBefore );
			}

			if ( this.ShouldSerializeBorderStyleTemplateAddRow( ) )
			{
				Utils.SerializeProperty( info, "BorderStyleTemplateAddRow", this.BorderStyleTemplateAddRow );
			}
			// --------------------------------------------------------------------------------------

			// SSP 11/25/03 UWG2013 UWG2553
			// Added MinRowHeight property to Override to allow for unrestricted control over the 
			// row heights.
			//
			if ( this.ShouldSerializeMinRowHeight( ) )
			{
				Utils.SerializeProperty( info, "MinRowHeight", this.MinRowHeight );
			}

			// SSP 12/12/03 DNF135
			// Added a way to control whether ink buttons get shown.
			// Added ShowInkButton property.
			//
			if ( this.ShouldSerializeShowInkButton( ) )
			{
				Utils.SerializeProperty( info, "ShowInkButton", this.ShowInkButton );
			}

			// MRS 9/14/04
			// Added a property to control whether or not the grid displays 
			// a string in a cell while calculating
			//
			if ( this.ShouldSerializeShowCalculatingText( ) )
			{
				// MD 5/5/08
				// Found while making unit tests for 8.2 - Rotated Column Headers
				// This was causing a first chance exception when deserializing and the property value was never getting deserialized.
				//Utils.SerializeObjectProperty( info, "ShowCalculatingText", this.ShowCalculatingText );
				Utils.SerializeProperty( info, "ShowCalculatingText", this.ShowCalculatingText );
			}

			// SSP 10/17/04
			// Added FormulaRowIndexSource property on the override.
			//
			if ( this.ShouldSerializeFormulaRowIndexSource( ) )
			{
				Utils.SerializeProperty( info, "FormulaRowIndexSource", this.FormulaRowIndexSource );
			}

			// SSP 11/3/04 - Merged Cell Feature
			//
			if ( this.ShouldSerializeMergedCellContentArea( ) )
			{
				Utils.SerializeProperty( info, "MergedCellContentArea", this.MergedCellContentArea );
			}

			if ( this.ShouldSerializeMergedCellStyle( ) )
			{
				Utils.SerializeProperty( info, "MergedCellStyle", this.MergedCellStyle );
			}

			// SSP 11/16/04
			// Implemented column sizing using cells in non-rowlayout mode. 
			// Added ColumnSizingArea property to the override.
			//
			if ( this.ShouldSerializeColumnSizingArea( ) )
			{
				Utils.SerializeProperty( info, "ColumnSizingArea", this.ColumnSizingArea );
			}

			//JDN 11/19/04 Added RowSelectorHeader
			if ( this.ShouldSerializeRowSelectorHeaderStyle( ) )
			{
				Utils.SerializeProperty( info, "RowSelectorHeaderStyle", this.RowSelectorHeaderStyle );
			}

			// SSP 12/14/04 - IDataErrorInfo Support
			//
			if ( this.ShouldSerializeSupportDataErrorInfo( ) )
			{
				Utils.SerializeProperty( info, "SupportDataErrorInfo", this.SupportDataErrorInfo );				
			}

			// SSP 3/14/05 - Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			//
			// ----------------------------------------------------------------------------------------
			if ( this.ShouldSerializeBorderStyleFilterCell( ) )
			{
				Utils.SerializeProperty( info, "BorderStyleFilterCell", this.BorderStyleFilterCell );
			}

			if ( this.ShouldSerializeBorderStyleFilterOperator( ) )
			{
				Utils.SerializeProperty( info, "BorderStyleFilterOperator", this.BorderStyleFilterOperator );
			}

			if ( this.ShouldSerializeBorderStyleFilterRow( ) )
			{
				Utils.SerializeProperty( info, "BorderStyleFilterRow", this.BorderStyleFilterRow );
			}

			if ( this.ShouldSerializeBorderStyleSpecialRowSeparator( ) )
			{
				Utils.SerializeProperty( info, "BorderStyleSpecialRowSeparator", this.BorderStyleSpecialRowSeparator );
			}

			if ( this.ShouldSerializeFilterClearButtonLocation( ) )
			{
				Utils.SerializeProperty( info, "FilterClearButtonLocation", this.FilterClearButtonLocation );
			}

			if ( this.ShouldSerializeFilterEvaluationTrigger( ) )
			{
				Utils.SerializeProperty( info, "FilterEvaluationTrigger", this.FilterEvaluationTrigger );
			}

			if ( this.ShouldSerializeFilterOperandStyle( ) )
			{
				Utils.SerializeProperty( info, "FilterOperandStyle", this.FilterOperandStyle );
			}

			if ( this.ShouldSerializeFilterOperatorDefaultValue( ) )
			{
				Utils.SerializeProperty( info, "FilterOperatorDefaultValue", this.FilterOperatorDefaultValue );
			}

			if ( this.ShouldSerializeFilterOperatorDropDownItems( ) )
			{
				Utils.SerializeProperty( info, "FilterOperatorDropDownItems", this.FilterOperatorDropDownItems );
			}

			if ( this.ShouldSerializeFilterOperatorLocation( ) )
			{
				Utils.SerializeProperty( info, "FilterOperatorLocation", this.FilterOperatorLocation );
			}

			if ( this.ShouldSerializeFilterRowPrompt( ) )
			{
				Utils.SerializeProperty( info, "FilterRowPrompt", this.FilterRowPrompt );
			}

			if ( this.ShouldSerializeFilterRowSpacingAfter( ) )
			{
				Utils.SerializeProperty( info, "FilterRowSpacingAfter", this.FilterRowSpacingAfter );
			}

			if ( this.ShouldSerializeFilterRowSpacingBefore( ) )
			{
				Utils.SerializeProperty( info, "FilterRowSpacingBefore", this.FilterRowSpacingBefore );
			}

			if ( this.ShouldSerializeFilterUIType( ) )
			{
				Utils.SerializeProperty( info, "FilterUIType", this.FilterUIType );
			}

			if ( this.ShouldSerializeFixedRowIndicator( ) )
			{
				Utils.SerializeProperty( info, "FixedRowIndicator", this.FixedRowIndicator );
			}

			if ( this.ShouldSerializeFixedRowsLimit( ) )
			{
				Utils.SerializeProperty( info, "FixedRowsLimit", this.FixedRowsLimit );
			}

			if ( this.ShouldSerializeFixedRowSortOrder( ) )
			{
				Utils.SerializeProperty( info, "FixedRowSortOrder", this.FixedRowSortOrder );
			}

			if ( this.ShouldSerializeFixedRowStyle( ) )
			{
				Utils.SerializeProperty( info, "FixedRowStyle", this.FixedRowStyle );
			}

			if ( this.ShouldSerializeGroupBySummaryDisplayStyle( ) )
			{
				Utils.SerializeProperty( info, "GroupBySummaryDisplayStyle", this.GroupBySummaryDisplayStyle );
			}

			if ( this.ShouldSerializeSequenceFilterRow( ) )
			{
				Utils.SerializeProperty( info, "SequenceFilterRow", this.SequenceFilterRow );
			}

			if ( this.ShouldSerializeSequenceFixedAddRow( ) )
			{
				Utils.SerializeProperty( info, "SequenceFixedAddRow", this.SequenceFixedAddRow );
			}

			if ( this.ShouldSerializeSequenceSummaryRow( ) )
			{
				Utils.SerializeProperty( info, "SequenceSummaryRow", this.SequenceSummaryRow );
			}

			if ( this.ShouldSerializeSpecialRowSeparator( ) )
			{
				Utils.SerializeProperty( info, "SpecialRowSeparator", this.SpecialRowSeparator );
			}

			if ( this.ShouldSerializeSpecialRowSeparatorHeight( ) )
			{
				Utils.SerializeProperty( info, "SpecialRowSeparatorHeight", this.SpecialRowSeparatorHeight );
			}

			if ( this.ShouldSerializeSummaryDisplayArea( ) )
			{
				Utils.SerializeProperty( info, "SummaryDisplayArea", this.SummaryDisplayArea );
			}

			if ( this.ShouldSerializeSummaryFooterSpacingAfter( ) )
			{
				Utils.SerializeProperty( info, "SummaryFooterSpacingAfter", this.SummaryFooterSpacingAfter );
			}

			if ( this.ShouldSerializeSummaryFooterSpacingBefore( ) )
			{
				Utils.SerializeProperty( info, "SummaryFooterSpacingBefore", this.SummaryFooterSpacingBefore );
			}

			if ( this.ShouldSerializeTemplateAddRowPrompt( ) )
			{
				Utils.SerializeProperty( info, "TemplateAddRowPrompt", this.TemplateAddRowPrompt );
			}

			if ( this.ShouldSerializeFilterComparisonType( ) )
			{
				Utils.SerializeProperty( info, "FilterComparisonType", this.FilterComparisonType );
			}
			// ----------------------------------------------------------------------------------------

			// JAS 2005 v2 GroupBy Row Extensions
			//
			// ----------------------------------------------------------------------------------------
			if( this.ShouldSerializeGroupByRowExpansionStyle() )
			{
				Utils.SerializeProperty( info, "GroupByRowExpansionStyle", this.GroupByRowExpansionStyle );
			}

			if( this.ShouldSerializeGroupByRowInitialExpansionState() )
			{
				Utils.SerializeProperty( info, "GroupByRowInitialExpansionState", this.GroupByRowInitialExpansionState );
			}
			// ----------------------------------------------------------------------------------------

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
			//
			// ----------------------------------------------------------------------------------------
			if( this.ShouldSerializeHeaderStyle() )
			{
				Utils.SerializeProperty( info, "HeaderStyle", this.HeaderStyle );
			}

			if( this.ShouldSerializeRowSelectorStyle() )
			{
				Utils.SerializeProperty( info, "RowSelectorStyle", this.RowSelectorStyle );
			}
			// ----------------------------------------------------------------------------------------

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
			//
			if( this.ShouldSerializeButtonStyle() )
			{
				Utils.SerializeProperty( info, "ButtonStyle", this.ButtonStyle );
			}

			// JAS v5.2 Wrapped Header Text 4/18/05
			//
			if( this.ShouldSerializeWrapHeaderText() )
			{
				Utils.SerializeProperty( info, "WrapHeaderText", this.WrapHeaderText );
			}

			// SSP 3/30/05 NAS 5.2 Row Numbers
			//
			if ( this.ShouldSerializeRowSelectorNumberStyle( ) )
			{
				Utils.SerializeProperty( info, "RowSelectorNumberStyle", this.RowSelectorNumberStyle );
			}

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			//
			if( this.ShouldSerializeSortComparisonType() )
			{
				Utils.SerializeProperty( info, "SortComparisonType", this.SortComparisonType );
			}

			// SSP 7/14/05 - NAS 5.3 Header Placement
			// 
			if ( this.ShouldSerializeHeaderPlacement( ) )
			{
				Utils.SerializeProperty( info, "HeaderPlacement", this.HeaderPlacement );
			}

			// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
			// 
			if ( this.ShouldSerializeInvalidValueBehavior( ) )
			{
				Utils.SerializeProperty( info, "InvalidValueBehavior", this.InvalidValueBehavior );
			}

			// SSP 8/17/05 BR05463
			// Added RowLayoutCellNavigationVertical property.
			// 
			if ( this.ShouldSerializeRowLayoutCellNavigationVertical( ) )
			{
				Utils.SerializeProperty( info, "RowLayoutCellNavigationVertical", this.RowLayoutCellNavigationVertical );
			}

			// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
			// 
			if ( this.ShouldSerializeAllowMultiCellOperations( ) )
			{
				Utils.SerializeProperty( info, "AllowMultiCellOperations", this.allowMultiCellOperations );
			}

			// SSP 2/24/06 BR09715
			// Added MultiCellSelectionMode property to support snaking cell selection.
			// 
			if ( this.ShouldSerializeMultiCellSelectionMode( ) )
			{
				Utils.SerializeProperty( info, "MultiCellSelectionMode", this.multiCellSelectionMode );
			}

			// SSP 9/28/06 BR16312
			// 
			// ------------------------------------------------------------------------------------
			if ( this.ShouldSerializeAllowRowLayoutColMoving( ) )
			{
				Utils.SerializeProperty( info, "AllowRowLayoutColMoving", this.allowRowLayoutColMoving );
			}

			if ( this.ShouldSerializeAllowRowLayoutCellSpanSizing( ) )
			{
				Utils.SerializeProperty( info, "AllowRowLayoutCellSpanSizing", this.allowRowLayoutCellSpanSizing );
			}

			if ( this.ShouldSerializeAllowRowLayoutLabelSpanSizing( ) )
			{
				Utils.SerializeProperty( info, "AllowRowLayoutLabelSpanSizing", this.allowRowLayoutLabelSpanSizing );
			}

            // MBS 2/20/08 - RowEditTemplate NA2008 V2
            if (this.ShouldSerializeRowEditTemplateUIType())
            {
                Utils.SerializeProperty(info, "RowEditTemplateUIType", this.rowEditTemplateUIType);
            }
			// ------------------------------------------------------------------------------------

			// MD 5/5/08 - 8.2 - Rotated Column Headers
			if ( this.ShouldSerializeColumnHeaderTextOrientation() )
			{
				Utils.SerializeProperty( info, "ColumnHeaderTextOrientation", this.columnHeaderTextOrientation );
			}

			if ( this.ShouldSerializeGroupHeaderTextOrientation() )
			{
				Utils.SerializeProperty( info, "GroupHeaderTextOrientation", this.groupHeaderTextOrientation );
			}
			// -------------------------------------------------------------------------------------

            // MRS 5/15/2008
            if (this.ShouldSerializeRowEditTemplateRowSelectorImage())
            {
                Utils.SerializeProperty(info, "RowEditTemplateRowSelectorImage", this.rowEditTemplateRowSelectorImage);                
            }

			//appearances
			if ( this.appearanceHolders != null )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( this.appearanceHolders[i] != null &&
						this.appearanceHolders[i].ShouldSerialize() )
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, i.ToString(), this.appearanceHolders[i] );
						//info.AddValue( i.ToString(), this.appearanceHolders[i] );
					}
				}
			}

			// MD 9/23/08 - TFS6601
			if ( this.ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing() )
			{
				Utils.SerializeProperty( info, "ReserveSortIndicatorSpaceWhenAutoSizing", this.reserveSortIndicatorSpaceWhenAutoSizing );
			}

			// MD 12/8/08 - 9.1 - Column Pinning Right
			if ( this.ShouldSerializeFixHeadersOnRight() )
				Utils.SerializeProperty( info, "FixHeadersOnRight", this.fixHeadersOnRight );

            // MBS 12/9/08 - NA9.1 Excel Style Filtering
            if (this.ShouldSerializeFilterUIProvider())
            {
                //string providerName = this.filterUIProvider != null ? this.filterUIProvider.Name : this.filterUIProviderComponentName;
                //Debug.Assert(String.IsNullOrEmpty(templateName) == false, "We shouldn't be trying to serialize an empty template control name");
                //Utils.SerializeProperty(info, "RowEditTemplateControlName", templateName);
            }

            // CDS NAS v9.1 Header CheckBox
            if (this.ShouldSerializeHeaderCheckBoxVisibility())
            {
                Utils.SerializeProperty(info, "HeaderCheckBoxVisibility", this.HeaderCheckBoxVisibility);
            }

            // CDS NAS v9.1 Header CheckBox
            if (this.ShouldSerializeHeaderCheckBoxAlignment())
            {
                Utils.SerializeProperty(info, "HeaderCheckBoxAlignment", this.HeaderCheckBoxAlignment);
            }

            // CDS NAS v9.1 Header CheckBox
            if (this.ShouldSerializeHeaderCheckBoxSynchronization())
            {
                Utils.SerializeProperty(info, "HeaderCheckBoxSynchronization", this.HeaderCheckBoxSynchronization);
            }

            // MBS 3/31/09 - NA9.2 CellBorderColor
            if (this.ShouldSerializeActiveCellBorderThickness())
            {
                Utils.SerializeProperty(info, "ActiveCellBorderThickness", this.ActiveCellBorderThickness);
            }

            // CDS 9.2 Column Moving Indicators
            if (this.ShouldSerializeDragDropIndicatorSettings())
            {
                Utils.SerializeProperty(info, "DragDropIndicatorSettings", this.DragDropIndicatorSettings);
            }

            // MBS 6/19/09 - TFS18639
            if (this.ShouldSerializeFilterOperandDropDownItems())
            {
                Utils.SerializeProperty(info, "FilterOperandDropDownItems", this.FilterOperandDropDownItems);
            }

            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
            if (this.ShouldSerializeActiveAppearancesEnabled())
            {
                Utils.SerializeProperty(info, "ActiveAppearancesEnabled", this.activeAppearancesEnabled);
            }
            //
            if (this.ShouldSerializeSelectedAppearancesEnabled())
            {
                Utils.SerializeProperty(info, "SelectedAppearancesEnabled", this.selectedAppearancesEnabled);
            }

            // MBS 8/7/09 - TFS18607
            if (this.ShouldSerializeAddRowEditNotificationInterface())
            {
                Utils.SerializeProperty(info, "AddRowEditNotificationInterface", this.addRowEditNotificationInterface);
            }
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal UltraGridOverride( SerializationInfo info, StreamingContext context )
		protected UltraGridOverride( SerializationInfo info, StreamingContext context )
		{
			//since we're not saving properties with default values, we must set the default here
			this.Reset();

			//this will have to be set in SetLayout()
			this.layout = null;

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( Infragistics.Win.AppearanceHolder ) )
				{
					// convert the name to an int
					//
					int index = int.Parse( entry.Name );
				
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					AppearanceHolder appHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );

					if ( appHolder != null && index >= 0 && index < this.AppearanceHolders.Length )
						this.AppearanceHolders[index] = appHolder;
					else
					{
						Debug.Fail("Invalid override appearance index during de-serialization, index = " + index.ToString() );
					}
				}
				else
				{
					switch ( entry.Name )
					{
						case "Key":
							//this.Key = (string)entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.Key = (string)Utils.DeserializeProperty( entry, typeof(string), this.Key );
							//this.Key = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
							break;

						case "Tag":
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;

						case "AllowAddNew":
							//this.AllowAddNew = (Infragistics.Win.UltraWinGrid.AllowAddNew)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowAddNew = (AllowAddNew)Utils.DeserializeProperty( entry, typeof(AllowAddNew), this.AllowAddNew );
							//this.AllowAddNew = (AllowAddNew)Utils.ConvertEnum(entry.Value, this.AllowAddNew);
							break;

						case "AllowColMoving":
							//this.AllowColMoving = (Infragistics.Win.UltraWinGrid.AllowColMoving)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowColMoving = (AllowColMoving)Utils.DeserializeProperty( entry, typeof(AllowColMoving), this.AllowColMoving );
							//this.AllowColMoving = (AllowColMoving)Utils.ConvertEnum(entry.Value, this.AllowColMoving);
							break;

						case "AllowColSizing":
							//this.AllowColSizing = (Infragistics.Win.UltraWinGrid.AllowColSizing)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowColSizing = (AllowColSizing)Utils.DeserializeProperty( entry, typeof(AllowColSizing), this.AllowColSizing );
							//this.AllowColSizing = (AllowColSizing)Utils.ConvertEnum(entry.Value, this.AllowColSizing);
							break;

						case "AllowColSwapping":
							//this.AllowColSwapping = (Infragistics.Win.UltraWinGrid.AllowColSwapping )(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowColSwapping = (AllowColSwapping)Utils.DeserializeProperty( entry, typeof(AllowColSwapping), this.AllowColSwapping );
							//this.AllowColSwapping = (AllowColSwapping)Utils.ConvertEnum(entry.Value, this.AllowColSwapping);
							break;

						case "AllowDelete":
							//this.AllowDelete = (Infragistics.Win.DefaultableBoolean) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowDelete = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.AllowDelete );
							//this.AllowDelete = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.AllowDelete);
							break;

						case "AllowGroupMoving":
							//this.AllowGroupMoving = (Infragistics.Win.UltraWinGrid.AllowGroupMoving) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowGroupMoving = (AllowGroupMoving)Utils.DeserializeProperty( entry, typeof(AllowGroupMoving), this.AllowGroupMoving );
							//this.AllowGroupMoving = (AllowGroupMoving)Utils.ConvertEnum(entry.Value, this.AllowGroupMoving);
							break;

						case "AllowGroupSwapping":
							//this.AllowGroupSwapping = (Infragistics.Win.UltraWinGrid.AllowGroupSwapping) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowGroupSwapping = (AllowGroupSwapping)Utils.DeserializeProperty( entry, typeof(AllowGroupSwapping), this.AllowGroupSwapping );
							//this.AllowGroupSwapping = (AllowGroupSwapping)Utils.ConvertEnum(entry.Value, this.AllowGroupSwapping);
							break;

						case "AllowUpdate":
							//this.AllowUpdate = (Infragistics.Win.DefaultableBoolean) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowUpdate = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.AllowUpdate );
							//this.AllowUpdate = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.AllowUpdate);
							break;

						case "BorderStyleRow":
							//this.BorderStyleRow = (Infragistics.Win.UIElementBorderStyle) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/20/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.BorderStyleRow = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.BorderStyleRow );
							//this.BorderStyleRow = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.BorderStyleRow);
							break;

						case "BorderStyleCell":
							//this.BorderStyleCell = (Infragistics.Win.UIElementBorderStyle) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/20/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.BorderStyleCell = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.BorderStyleCell );
							//this.BorderStyleCell = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.BorderStyleCell);
							break;

							//JM 01-16-02 The following is no longer needed - now using
							//			  BorderStyleRow for cards
							//case "BorderStyleCard":
							//	this.BorderStyleCard = (Infragistics.Win.UIElementBorderStyle) (int)entry.Value;
							//	break;

						case "BorderStyleCardArea":
							//this.BorderStyleCardArea = (Infragistics.Win.UIElementBorderStyle) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/20/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.BorderStyleCardArea = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.BorderStyleCardArea );
							//this.BorderStyleCardArea = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.BorderStyleCardArea);
							break;

						case "BorderStyleHeader": 
							//this.BorderStyleHeader = (Infragistics.Win.UIElementBorderStyle) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/20/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.BorderStyleHeader = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.BorderStyleHeader );
							//this.BorderStyleHeader = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.BorderStyleHeader);
							break;

						case "CellClickAction": 
							//this.CellClickAction  = (Infragistics.Win.UltraWinGrid.CellClickAction) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.CellClickAction = (CellClickAction)Utils.DeserializeProperty( entry, typeof(CellClickAction), this.CellClickAction );
							//this.CellClickAction = (CellClickAction)Utils.ConvertEnum(entry.Value, this.CellClickAction);
							break;

						case "CellMultiLine": 
							//this.CellMultiLine  = (Infragistics.Win.DefaultableBoolean) (int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.CellMultiLine = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.CellMultiLine );
							//this.CellMultiLine = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.CellMultiLine);
							break;

						case "CellPadding": 
							//this.CellPadding  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.CellPadding = (int)Utils.DeserializeProperty( entry, typeof(int), this.CellPadding );
							//this.CellPadding = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

						case "CellSpacing": 
							//this.CellSpacing  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.CellSpacing = (int)Utils.DeserializeProperty( entry, typeof(int), this.CellSpacing );
							//this.CellSpacing = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

						case "DefaultColWidth": 
							//this.DefaultColWidth  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.DefaultColWidth = (int)Utils.DeserializeProperty( entry, typeof(int), this.DefaultColWidth );
							//this.DefaultColWidth = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

						case "DefaultRowHeight": 
							//this.DefaultRowHeight  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.DefaultRowHeight = (int)Utils.DeserializeProperty( entry, typeof(int), this.DefaultRowHeight );
							//this.DefaultRowHeight = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

							//	case "ExpandRowsOnLoad": 
							//		this.ExpandRowsOnLoad  = (Infragistics.Win.UltraWinGrid.ExpandOnLoad)(int) entry.Value;
							//		break;

						case "ExpansionIndicator": 
							//this.ExpansionIndicator  = (Infragistics.Win.UltraWinGrid.ShowExpansionIndicator)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.ExpansionIndicator = (ShowExpansionIndicator)Utils.DeserializeProperty( entry, typeof(ShowExpansionIndicator), this.ExpansionIndicator );
							//this.ExpansionIndicator = (ShowExpansionIndicator)Utils.ConvertEnum(entry.Value, this.ExpansionIndicator);
							break;

						case "HeaderClickAction": 
							//this.HeaderClickAction  = (Infragistics.Win.UltraWinGrid.HeaderClickAction)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.HeaderClickAction = (HeaderClickAction)Utils.DeserializeProperty( entry, typeof(HeaderClickAction), this.HeaderClickAction );
							//this.HeaderClickAction = (HeaderClickAction)Utils.ConvertEnum(entry.Value, this.HeaderClickAction);
							break;

						case "MaxSelectedCells": 
							//this.MaxSelectedCells  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.MaxSelectedCells = (int)Utils.DeserializeProperty( entry, typeof(int), this.MaxSelectedCells );
							//this.MaxSelectedCells = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

						case "MaxSelectedRows": 
							//this.MaxSelectedRows  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.MaxSelectedRows = (int)Utils.DeserializeProperty( entry, typeof(int), this.MaxSelectedRows );
							//this.MaxSelectedRows = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

							// JJD 1/21/02 - UWG851 - Moved from layout
							//
						case "NullText":
						{
							//this.NullText = (string)entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.NullText = (string)Utils.DeserializeProperty( entry, typeof(string), this.nullText );
							//this.NullText = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
							break;
						}

						case "RowSelectors": 
							//this.RowSelectors  = (Infragistics.Win.DefaultableBoolean)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.RowSelectors = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.RowSelectors );
							//this.RowSelectors = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.RowSelectors);
							break;

						case "RowSizing": 
							//this.RowSizing  = (Infragistics.Win.UltraWinGrid.RowSizing)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.RowSizing = (RowSizing)Utils.DeserializeProperty( entry, typeof(RowSizing), this.RowSizing );
							//this.RowSizing = (RowSizing)Utils.ConvertEnum(entry.Value, this.RowSizing);
							break;

						case "RowSizingArea": 
							//this.RowSizingArea  = (Infragistics.Win.UltraWinGrid.RowSizingArea)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.RowSizingArea = (RowSizingArea)Utils.DeserializeProperty( entry, typeof(RowSizingArea), this.RowSizingArea );
							//this.RowSizingArea = (RowSizingArea)Utils.ConvertEnum(entry.Value, this.RowSizingArea);
							break;

						case "RowSizingAutoMaxLines": 
							//this.RowSizingAutoMaxLines  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.RowSizingAutoMaxLines = (int)Utils.DeserializeProperty( entry, typeof(int), this.RowSizingAutoMaxLines );
							//this.RowSizingAutoMaxLines = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

						case "RowSpacingBefore": 
							//this.RowSpacingBefore  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.RowSpacingBefore = (int)Utils.DeserializeProperty( entry, typeof(int), this.RowSpacingBefore );
							//this.RowSpacingBefore = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

						case "RowSpacingAfter": 
							//this.RowSpacingAfter  = (int) entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.RowSpacingAfter = (int)Utils.DeserializeProperty( entry, typeof(int), this.RowSpacingAfter );
							//this.RowSpacingAfter = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

						case "SelectTypeCell": 
							//this.SelectTypeCell  = (Infragistics.Win.UltraWinGrid.SelectType)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.SelectTypeCell = (SelectType)Utils.DeserializeProperty( entry, typeof(SelectType), this.SelectTypeCell );
							//this.SelectTypeCell = (SelectType)Utils.ConvertEnum(entry.Value, this.SelectTypeCell);
							break;

						case "SelectTypeCol": 
							//this.SelectTypeCol  = (Infragistics.Win.UltraWinGrid.SelectType)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.SelectTypeCol = (SelectType)Utils.DeserializeProperty( entry, typeof(SelectType), this.SelectTypeCol );
							//this.SelectTypeCol = (SelectType)Utils.ConvertEnum(entry.Value, this.SelectTypeCol);
							break;

						case "SelectTypeRow": 
							//this.SelectTypeRow  = (Infragistics.Win.UltraWinGrid.SelectType)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.SelectTypeRow = (SelectType)Utils.DeserializeProperty( entry, typeof(SelectType), this.SelectTypeRow );
							//this.SelectTypeRow = (SelectType)Utils.ConvertEnum(entry.Value, this.SelectTypeRow);
							break;

						case "TipStyleCell": 
							//this.TipStyleCell  = (Infragistics.Win.UltraWinGrid.TipStyle)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.TipStyleCell = (TipStyle)Utils.DeserializeProperty( entry, typeof(TipStyle), this.TipStyleCell );
							//this.TipStyleCell = (TipStyle)Utils.ConvertEnum(entry.Value, this.TipStyleCell);
							break;

							// SSP 3/3/06 BR10430
							// Added TipStyleHeader property.
							// 
						case "TipStyleHeader": 
							this.TipStyleHeader = (TipStyle)Utils.DeserializeProperty( entry, typeof(TipStyle), this.TipStyleHeader );
							break;

						case "TipStyleRowConnector": 
							//this.TipStyleRowConnector  = (Infragistics.Win.UltraWinGrid.TipStyle)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.TipStyleRowConnector = (TipStyle)Utils.DeserializeProperty( entry, typeof(TipStyle), this.TipStyleRowConnector );
							//this.TipStyleRowConnector = (TipStyle)Utils.ConvertEnum(entry.Value, this.TipStyleRowConnector);
							break;

						case "TipStyleScroll": 
							//this.TipStyleScroll = (Infragistics.Win.UltraWinGrid.TipStyle)(int) entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.TipStyleScroll = (TipStyle)Utils.DeserializeProperty( entry, typeof(TipStyle), this.TipStyleScroll );
							//this.TipStyleScroll = (TipStyle)Utils.ConvertEnum(entry.Value, this.TipStyleScroll);
							break;

						case "GroupByRowDescriptionMask":
							//this.GroupByRowDescriptionMask = (string)entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.GroupByRowDescriptionMask = (string)Utils.DeserializeProperty( entry, typeof(string), this.GroupByRowDescriptionMask );
							//this.GroupByRowDescriptionMask = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
							break;

						case "GroupByRowPadding":
							//this.GroupByRowPadding = (int)entry.Value;
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.GroupByRowPadding = (int)Utils.DeserializeProperty( entry, typeof(int), this.GroupByRowPadding );
							//this.GroupByRowPadding = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
							break;

						case "GroupByColumnsHidden":
							//this.GroupByColumnsHidden = (DefaultableBoolean)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.GroupByColumnsHidden = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.GroupByColumnsHidden );
							//this.GroupByColumnsHidden = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.GroupByColumnsHidden);
							break;

						case "AllowGroupBy":
							//this.AllowGroupBy = (DefaultableBoolean)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.AllowGroupBy = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.AllowGroupBy );
							//this.AllowGroupBy = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.AllowGroupBy);
							break;
						
						case "SelectTypeGroupByRow":
							//this.SelectTypeGroupByRow = (SelectType)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.SelectTypeGroupByRow = (SelectType)Utils.DeserializeProperty( entry, typeof(SelectType), this.SelectTypeGroupByRow );
							//this.SelectTypeGroupByRow = (SelectType)Utils.ConvertEnum(entry.Value, this.SelectTypeGroupByRow);
							break;

							// SSP 6/11/02
							// Summary rows feature.
							//
						case "AllowRowFiltering":
							//this.allowRowFiltering = (DefaultableBoolean)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.allowRowFiltering = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.allowRowFiltering );
							//this.allowRowFiltering = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.allowRowFiltering);
							break;
						case "RowFilterMode":
							//this.rowFilterMode = (RowFilterMode)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.rowFilterMode = (RowFilterMode)Utils.DeserializeProperty( entry, typeof(RowFilterMode), this.rowFilterMode );
							//this.rowFilterMode = (RowFilterMode)Utils.ConvertEnum(entry.Value, this.rowFilterMode);
							break;
						case "AllowRowSummaries":
							//this.allowRowSummaries = (AllowRowSummaries)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.allowRowSummaries = (AllowRowSummaries)Utils.DeserializeProperty( entry, typeof(AllowRowSummaries), this.allowRowSummaries );
							//this.allowRowSummaries = (AllowRowSummaries)Utils.ConvertEnum(entry.Value, this.allowRowSummaries);
							break;
						case "BorderStyleSummaryFooter":
							//this.borderStyleSummaryFooter = (UIElementBorderStyle)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/20/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.borderStyleSummaryFooter = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.borderStyleSummaryFooter );
							//this.borderStyleSummaryFooter = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.borderStyleSummaryFooter);
							break;
						case "BorderStyleSummaryValue":
							//this.borderStyleSummaryValue = (UIElementBorderStyle)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/20/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.borderStyleSummaryValue = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.borderStyleSummaryValue );
							//this.borderStyleSummaryValue = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.borderStyleSummaryValue);
							break;
						case "BorderStyleSummaryFooterCaption":
							//this.borderStyleSummaryFooterCaption = (UIElementBorderStyle)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/20/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.borderStyleSummaryFooterCaption = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.borderStyleSummaryFooterCaption );
							//this.borderStyleSummaryFooterCaption = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.borderStyleSummaryFooterCaption);
							break;
							// SSP 7/19/02 UWG1369
							// Moved SummaryFooterCaptionVisible property from band to override.
							//
						case "SummaryFooterCaptionVisible":
							//this.summaryFooterCaptionVisible = (DefaultableBoolean)(int)entry.Value;
							// AS 8/15/02
							// Use our enum serialization method to ensure that the
							// enum value is valid.
							//
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.summaryFooterCaptionVisible = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.summaryFooterCaptionVisible );
							//this.summaryFooterCaptionVisible = (DefaultableBoolean)Utils.ConvertEnum(entry.Value, this.summaryFooterCaptionVisible);
							break;
							// SSP 8/20/02
							// Added an entry for card spacing as it wasn't being serialized and deserialized before.
							//
						case "CardSpacing":
							this.cardSpacing = (int)Utils.DeserializeProperty( entry, typeof(int), this.cardSpacing );
							break;

							// SSP 2/14/03 - RowSelectorWidth property
							// Added RowSelectorWidth property to allow the user to control the widths
							// of the row selectors.
							//
						case "RowSelectorWidth":
							this.rowSelectorWidth = (int)Utils.DeserializeProperty( entry, typeof(int), this.rowSelectorWidth );
							break;
							// SSP 3/6/03 - Row Layout Functionality
							// Added RowLayoutLabelPosition and BorderStyleRowSelector properties.
							//
						case "BorderStyleRowSelector":
							this.borderStyleRowSelector = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.borderStyleRowSelector );
							break;
						case "AllowRowLayoutCellSizing":
							this.allowRowLayoutCellSizing = (RowLayoutSizing)Utils.DeserializeProperty( entry, typeof(RowLayoutSizing), this.allowRowLayoutCellSizing );
							break;
						case "AllowRowLayoutLabelSizing":
							this.allowRowLayoutLabelSizing = (RowLayoutSizing)Utils.DeserializeProperty( entry, typeof(RowLayoutSizing), this.allowRowLayoutLabelSizing );
							break;
							// SSP 4/14/03
							// Added ColumnAutoSizeMode property.
							//
						case "ColumnAutoSizeMode":
							this.columnAutoSizeMode = (ColumnAutoSizeMode)Utils.DeserializeProperty( entry, typeof( ColumnAutoSizeMode ), this.columnAutoSizeMode );
							break;
							// SSP 5/12/03 - Optimizations
							// Added a way to just draw the text without having to embedd an embeddable ui element in
							// cells to speed up rendering.
							//
						case "CellDisplayStyle":
							this.cellDisplayStyle = (CellDisplayStyle)Utils.DeserializeProperty( entry, typeof( CellDisplayStyle ), this.cellDisplayStyle );
							break;
							// SSP 5/19/03 - Fixed headers
							//
						case "FixedHeaderIndicator":
							this.fixedHeaderIndicator = (Infragistics.Win.UltraWinGrid.FixedHeaderIndicator )Utils.DeserializeProperty( entry, typeof( Infragistics.Win.UltraWinGrid.FixedHeaderIndicator ), this.fixedHeaderIndicator );
							break;
						case "FixedCellSeparatorColor":
							this.fixedCellSeparatorColor = (Color)Utils.DeserializeProperty( entry, typeof( Color ), this.fixedCellSeparatorColor );
							break;
							// SSP 8/1/03 UWG1654 - Filter Action
							// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
							// appearances.
							//
						case "RowFilterAction":
							this.rowFilterAction = (RowFilterAction)Utils.DeserializeProperty( entry, typeof( RowFilterAction ), this.rowFilterAction );
							break;
							// SSP 11/7/03 - Add Row Feature
							//
							// ------------------------------------------------------------------------------------
						case "TemplateAddRowSpacingAfter":
							this.templateAddRowSpacingAfter = (int)Utils.DeserializeProperty( entry, typeof( int ), this.templateAddRowSpacingAfter );
							break;
						case "TemplateAddRowSpacingBefore":
							this.templateAddRowSpacingBefore = (int)Utils.DeserializeProperty( entry, typeof( int ), this.templateAddRowSpacingBefore );
							break;
						case "BorderStyleTemplateAddRow":
							this.borderStyleTemplateAddRow = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof( UIElementBorderStyle ), this.borderStyleTemplateAddRow );
							break;
							// ------------------------------------------------------------------------------------

							// SSP 11/25/03 UWG2013 UWG2553
							// Added MinRowHeight property to Override to allow for unrestricted control over the 
							// row heights.
							//
						case "MinRowHeight":
							this.minRowHeight = (int)Utils.DeserializeProperty( entry, typeof( int ), this.minRowHeight );
							break;

							// SSP 12/12/03 DNF135
							// Added a way to control whether ink buttons get shown.
							// Added ShowInkButton property.
							//
						case "ShowInkButton":
							this.showInkButton = (ShowInkButton)Utils.DeserializeProperty( entry, typeof( ShowInkButton ), this.showInkButton );
							break;

							// MRS 9/14/04
							// Added a property to control whether or not the grid displays 
							// a string in a cell while calculating
							//
						case "ShowCalculatingText":
							this.showCalculatingText = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.showCalculatingText);
							break;

							// SSP 10/17/04
							// Added FormulaRowIndexSource property on the override.
							//
						case "FormulaRowIndexSource":
							this.formulaRowIndexSource = (FormulaRowIndexSource)Utils.DeserializeProperty( entry, typeof( FormulaRowIndexSource ), this.formulaRowIndexSource );
							break;

							// SSP 11/3/04 - Merged Cell Feature
							//
						case "MergedCellContentArea":
							this.mergedCellContentArea = (MergedCellContentArea)Utils.DeserializeProperty( entry, typeof( MergedCellContentArea ), this.mergedCellContentArea );
							break;

						case "MergedCellStyle":
							this.mergedCellStyle = (MergedCellStyle)Utils.DeserializeProperty( entry, typeof( MergedCellStyle ), this.mergedCellStyle );
							break;
							// SSP 11/16/04
							// Implemented column sizing using cells in non-rowlayout mode. 
							// Added ColumnSizingArea property to the override.
							//
						case "ColumnSizingArea":
							this.columnSizingArea = (ColumnSizingArea)Utils.DeserializeProperty( entry, typeof( ColumnSizingArea ), this.columnSizingArea );
							break;  
                          
							//JDN 11/19/04 Added RowSelectorHeader
						case "RowSelectorHeaderStyle":
							this.rowSelectorHeaderStyle = (RowSelectorHeaderStyle)Utils.DeserializeProperty( entry, typeof(RowSelectorHeaderStyle), this.rowSelectorHeaderStyle );
							break;

							// SSP 12/14/04 - IDataErrorInfo Support
							//
						case "SupportDataErrorInfo":
							this.supportDataErrorInfo = (SupportDataErrorInfo)Utils.DeserializeProperty( entry, typeof( SupportDataErrorInfo ), this.supportDataErrorInfo );
							break;

							// SSP 12/21/04 BR01386
							// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
							//
						case "GroupByRowSpacingBefore": 
							this.groupByRowSpacingBefore = (int)Utils.DeserializeProperty( entry, typeof(int), this.groupByRowSpacingBefore );
							break;

							// SSP 12/21/04 BR01386
							// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
							//
						case "GroupByRowSpacingAfter": 
							this.groupByRowSpacingAfter = (int)Utils.DeserializeProperty( entry, typeof(int), this.groupByRowSpacingAfter );
							break;

							// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
							//
							// ----------------------------------------------------------------------------------------
						case "BorderStyleFilterCell": 
							this.borderStyleFilterCell = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof( UIElementBorderStyle ), this.borderStyleFilterCell );
							break;
						case "BorderStyleFilterOperator": 
							this.borderStyleFilterOperator = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof( UIElementBorderStyle ), this.borderStyleFilterOperator );
							break;
						case "BorderStyleFilterRow": 
							this.borderStyleFilterRow = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof( UIElementBorderStyle ), this.borderStyleFilterRow );
							break;
						case "BorderStyleSpecialRowSeparator": 
							this.borderStyleSpecialRowSeparator = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof( UIElementBorderStyle ), this.borderStyleSpecialRowSeparator );
							break;
						case "FilterClearButtonLocation": 
							this.filterClearButtonLocation = (FilterClearButtonLocation)Utils.DeserializeProperty( entry, typeof( FilterClearButtonLocation ), this.filterClearButtonLocation );
							break;
						case "FilterEvaluationTrigger": 
							this.filterEvaluationTrigger = (FilterEvaluationTrigger)Utils.DeserializeProperty( entry, typeof( FilterEvaluationTrigger ), this.filterEvaluationTrigger );
							break;
						case "FilterOperandStyle": 
							this.filterOperandStyle = (FilterOperandStyle)Utils.DeserializeProperty( entry, typeof( FilterOperandStyle ), this.filterOperandStyle );
							break;
						case "FilterOperatorDefaultValue": 
							this.filterOperatorDefaultValue = (FilterOperatorDefaultValue)Utils.DeserializeProperty( entry, typeof( FilterOperatorDefaultValue ), this.filterOperatorDefaultValue );
							break;
						case "FilterOperatorDropDownItems": 
							this.filterOperatorDropDownItems = (FilterOperatorDropDownItems)Utils.DeserializeProperty( entry, typeof( FilterOperatorDropDownItems ), this.filterOperatorDropDownItems );
							break;
						case "FilterOperatorLocation": 
							this.filterOperatorLocation = (FilterOperatorLocation)Utils.DeserializeProperty( entry, typeof( FilterOperatorLocation ), this.filterOperatorLocation );
							break;
						case "FilterRowPrompt": 
							this.filterRowPrompt = (string)Utils.DeserializeProperty( entry, typeof( string ), this.filterRowPrompt );
							break;
						case "FilterRowSpacingAfter": 
							this.filterRowSpacingAfter = (int)Utils.DeserializeProperty( entry, typeof( int ), this.filterRowSpacingAfter );
							break;
						case "FilterRowSpacingBefore": 
							this.filterRowSpacingBefore = (int)Utils.DeserializeProperty( entry, typeof( int ), this.filterRowSpacingBefore );
							break;
						case "FilterUIType": 
							this.filterUIType = (FilterUIType)Utils.DeserializeProperty( entry, typeof( FilterUIType ), this.filterUIType );
							break;
						case "FixedRowIndicator": 
							this.fixedRowIndicator = (FixedRowIndicator)Utils.DeserializeProperty( entry, typeof( FixedRowIndicator ), this.fixedRowIndicator );
							break;
						case "FixedRowsLimit": 
							this.fixedRowsLimit = (int)Utils.DeserializeProperty( entry, typeof( int ), this.fixedRowsLimit );
							break;
						case "FixedRowSortOrder": 
							this.fixedRowSortOrder = (FixedRowSortOrder)Utils.DeserializeProperty( entry, typeof( FixedRowSortOrder ), this.fixedRowSortOrder );
							break;
						case "FixedRowStyle": 
							this.fixedRowStyle = (FixedRowStyle)Utils.DeserializeProperty( entry, typeof( FixedRowStyle ), this.fixedRowStyle );
							break;
						case "GroupBySummaryDisplayStyle": 
							this.groupBySummaryDisplayStyle = (GroupBySummaryDisplayStyle)Utils.DeserializeProperty( entry, typeof( GroupBySummaryDisplayStyle ), this.groupBySummaryDisplayStyle );
							break;
						case "SequenceFilterRow": 
							this.sequenceFilterRow = (int)Utils.DeserializeProperty( entry, typeof( int ), this.sequenceFilterRow );
							break;
						case "SequenceFixedAddRow": 
							this.sequenceFixedAddRow = (int)Utils.DeserializeProperty( entry, typeof( int ), this.sequenceFixedAddRow );
							break;
						case "SequenceSummaryRow": 
							this.sequenceSummaryRow = (int)Utils.DeserializeProperty( entry, typeof( int ), this.sequenceSummaryRow );
							break;
						case "SpecialRowSeparator": 
							this.specialRowSeparator = (SpecialRowSeparator)Utils.DeserializeProperty( entry, typeof( SpecialRowSeparator ), this.specialRowSeparator );
							break;
						case "SpecialRowSeparatorHeight": 
							this.specialRowSeparatorHeight = (int)Utils.DeserializeProperty( entry, typeof( int ), this.specialRowSeparatorHeight );
							break;
						case "SummaryDisplayArea": 
							this.summaryDisplayArea = (SummaryDisplayAreas)Utils.DeserializeProperty( entry, typeof( SummaryDisplayAreas ), this.summaryDisplayArea );
							break;
						case "SummaryFooterSpacingAfter": 
							this.summaryFooterSpacingAfter = (int)Utils.DeserializeProperty( entry, typeof( int ), this.summaryFooterSpacingAfter );
							break;
						case "SummaryFooterSpacingBefore": 
							this.summaryFooterSpacingBefore = (int)Utils.DeserializeProperty( entry, typeof( int ), this.summaryFooterSpacingBefore );
							break;
						case "TemplateAddRowPrompt": 
							this.templateAddRowPrompt = (string)Utils.DeserializeProperty( entry, typeof( string ), this.templateAddRowPrompt );
							break;
						case "FilterComparisonType": 
							this.filterComparisonType = (FilterComparisonType)Utils.DeserializeProperty( entry, typeof( FilterComparisonType ), this.filterComparisonType );
							break;
						// ----------------------------------------------------------------------------------------

						// JAS 2005 v2 GroupBy Row Extensions
						//
						case "GroupByRowExpansionStyle":
							this.groupByRowExpansionStyle = (GroupByRowExpansionStyle)Utils.DeserializeProperty( entry, typeof(GroupByRowExpansionStyle), this.groupByRowExpansionStyle );
							break;
						case "GroupByRowInitialExpansionState":
							this.groupByRowInitialExpansionState = (GroupByRowInitialExpansionState)Utils.DeserializeProperty( entry, typeof(GroupByRowInitialExpansionState), this.groupByRowInitialExpansionState );
							break;

						// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
						//
						case "HeaderStyle":
							this.headerStyle = (HeaderStyle)Utils.DeserializeProperty( entry, typeof(HeaderStyle), this.headerStyle );
							break;
						case "RowSelectorStyle":
							this.rowSelectorStyle = (HeaderStyle)Utils.DeserializeProperty( entry, typeof(HeaderStyle), this.rowSelectorStyle );
							break;

						// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
						//
						case "ButtonStyle":
							this.buttonStyle = (UIElementButtonStyle)Utils.DeserializeProperty( entry, typeof(UIElementButtonStyle), this.buttonStyle );
							break;

						// JAS v5.2 Wrapped Header Text 4/18/05
						//
						case "WrapHeaderText":
							this.wrapHeaderText = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof(DefaultableBoolean), this.wrapHeaderText );
							break;

						// SSP 3/30/05 NAS 5.2 Row Numbers
						//
						case "RowSelectorNumberStyle":
							this.rowSelectorNumberStyle = (RowSelectorNumberStyle)Utils.DeserializeProperty( entry, typeof( RowSelectorNumberStyle ), this.rowSelectorNumberStyle );
							break;

						// JAS v5.2 GroupBy Break Behavior 5/4/05
						//
						case "SortComparisonType":
							this.sortComparisonType = (SortComparisonType)Utils.DeserializeProperty( entry, typeof(SortComparisonType), this.sortComparisonType );
							break;

						// SSP 7/14/05 - NAS 5.3 Header Placement
						// 
						case "HeaderPlacement":
							this.headerPlacement = (HeaderPlacement)Utils.DeserializeProperty( entry, typeof( HeaderPlacement ), this.headerPlacement );
							break;

						// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
						// 
						case "InvalidValueBehavior":
							this.invalidValueBehavior = (InvalidValueBehavior)Utils.DeserializeProperty( entry, typeof( InvalidValueBehavior ), this.invalidValueBehavior );
							break;

							// SSP 8/17/05 BR05463
							// Added RowLayoutCellNavigationVertical property.
							// 
						case "RowLayoutCellNavigationVertical":
							this.rowLayoutCellNavigationVertical = (RowLayoutCellNavigation)Utils.DeserializeProperty( entry, typeof( RowLayoutCellNavigation ), this.rowLayoutCellNavigationVertical );
							break;

							// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
							// 
						case "AllowMultiCellOperations":
							this.allowMultiCellOperations = (AllowMultiCellOperation)Utils.DeserializeProperty( entry, typeof( AllowMultiCellOperation ), this.allowMultiCellOperations );
							break;

							// SSP 2/24/06 BR09715
							// Added MultiCellSelectionMode property to support snaking cell selection.
							// 
						case "MultiCellSelectionMode":
							this.multiCellSelectionMode = (MultiCellSelectionMode)Utils.DeserializeProperty( entry, typeof( MultiCellSelectionMode ), this.multiCellSelectionMode );
							break;

						// SSP 9/28/06 BR16312
						// 
						// ----------------------------------------------------------------------------------------
						case "AllowRowLayoutColMoving":
							this.allowRowLayoutColMoving = (Layout.GridBagLayoutAllowMoving)Utils.DeserializeProperty( entry, typeof( Layout.GridBagLayoutAllowMoving ), this.allowRowLayoutColMoving );
							break;

						case "AllowRowLayoutCellSpanSizing":
							this.allowRowLayoutCellSpanSizing = (Layout.GridBagLayoutAllowSpanSizing)Utils.DeserializeProperty( entry, typeof( Layout.GridBagLayoutAllowSpanSizing ), this.allowRowLayoutCellSpanSizing );
							break;

						case "AllowRowLayoutLabelSpanSizing":
							this.allowRowLayoutLabelSpanSizing = (Layout.GridBagLayoutAllowSpanSizing)Utils.DeserializeProperty( entry, typeof( Layout.GridBagLayoutAllowSpanSizing ), this.allowRowLayoutLabelSpanSizing );
							break;
						// ----------------------------------------------------------------------------------------

                        // MBS 2/20/08 - RowEditTemplate NA2008 V2
                        case "RowEditTemplateUIType":
                            this.rowEditTemplateUIType = (RowEditTemplateUIType)Utils.DeserializeProperty(entry, typeof(RowEditTemplateUIType), this.rowEditTemplateUIType);
                            break;

						// MD 5/5/08 - 8.2 - Rotated Column Headers
						case "ColumnHeaderTextOrientation":
							this.columnHeaderTextOrientation = (TextOrientationInfo)Utils.DeserializeProperty( entry, typeof( TextOrientationInfo ), this.columnHeaderTextOrientation );
							break;

						case "GroupHeaderTextOrientation":
							this.groupHeaderTextOrientation = (TextOrientationInfo)Utils.DeserializeProperty( entry, typeof( TextOrientationInfo ), this.groupHeaderTextOrientation );
							break;
						// ----------------------------------------------------------------------------------------

                        // MRS 5/15/2008
                        case "RowEditTemplateRowSelectorImage":
                            this.rowEditTemplateRowSelectorImage = (Image)Utils.DeserializeProperty(entry, typeof(Image), this.rowEditTemplateRowSelectorImage);
                            break;

						// MD 9/23/08 - TFS6601
						case "ReserveSortIndicatorSpaceWhenAutoSizing":
							this.reserveSortIndicatorSpaceWhenAutoSizing = 
								(ReserveSortIndicatorSpaceWhenAutoSizing)Utils.DeserializeProperty( entry, typeof( ReserveSortIndicatorSpaceWhenAutoSizing ), this.reserveSortIndicatorSpaceWhenAutoSizing );
							break;

						// MD 12/8/08 - 9.1 - Column Pinning Right
						case "FixHeadersOnRight":
							this.fixHeadersOnRight = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.fixHeadersOnRight );
							break;

                        // CDS NAS v9.1 Header CheckBox
                        case "HeaderCheckBoxVisibility":
                            this.headerCheckBoxVisibility = (HeaderCheckBoxVisibility)Utils.DeserializeProperty(entry, typeof(HeaderCheckBoxVisibility), this.headerCheckBoxVisibility);
                            break;

                        // CDS NAS v9.1 Header CheckBox
                        case "HeaderCheckBoxAlignment":
                            this.headerCheckBoxAlignment = (HeaderCheckBoxAlignment)Utils.DeserializeProperty(entry, typeof(HeaderCheckBoxAlignment), this.headerCheckBoxAlignment);
                            break;

                        // CDS NAS v9.1 Header CheckBox
                        case "HeaderCheckBoxSynchronization":
                            this.headerCheckBoxSynchronization = (HeaderCheckBoxSynchronization)Utils.DeserializeProperty(entry, typeof(HeaderCheckBoxSynchronization), this.headerCheckBoxSynchronization);
                            break;

                        // CDS 9.2 Column Moving Indicators
                        case "DragDropIndicatorSettings":
                            this.dragDropIndicatorSettings = (DragDropIndicatorSettings)Utils.DeserializeProperty(entry, typeof(DragDropIndicatorSettings), null);
                            if (this.HasDragDropIndicatorSettings)
                                this.dragDropIndicatorSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;

                            break;

                        // MBS 3/31/09 - NA9.2 CellBorderColor
                        case "ActiveCellBorderThickness":
                            this.activeCellBorderThickness = (int)Utils.DeserializeProperty(entry, typeof(int), this.activeCellBorderThickness);
                            break;

                        case "FilterOperandDropDownItems":
                            this.filterOperandDropDownItems = (FilterOperandDropDownItems)Utils.DeserializeProperty(entry, typeof(FilterOperandDropDownItems), this.filterOperandDropDownItems);
                            break;

                        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
                        case "ActiveAppearancesEnabled":
                            this.ActiveAppearancesEnabled = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.ActiveAppearancesEnabled);
                            break;
                        //
                        case "SelectedAppearancesEnabled":
                            this.SelectedAppearancesEnabled = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.SelectedAppearancesEnabled);
                            break;

                        // MBS 8/7/09 - TFS18607
                        case "AddRowEditNotificationInterface":
                            this.addRowEditNotificationInterface = (AddRowEditNotificationInterface)Utils.DeserializeProperty(entry, typeof(AddRowEditNotificationInterface), this.addRowEditNotificationInterface);
                            break;

						default:
						{
							Debug.Assert( false, "Invalid entry in Override de-serialization ctor" );
							break;
						}
					}
				}
			}

		}
			
		internal void InitLayout( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			this.layout =  layout;

			// JJD 8/7/01 - UWG124
			// Initialize the collection property for any appearance holders
			// that were serialized
			//
			this.InitAppearanceHolders();
		}

		internal void InitAppearanceHolders()
		{
			// JJD 8/7/01 - UWG124
			// Initialize the collection property for any appearance holders
			// that were serialized
			//
			if ( this.appearanceHolders != null && this.layout != null )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( this.appearanceHolders[i] != null )
					{
						this.appearanceHolders[i].Collection = this.layout.Appearances;
					}
				}
			}
		}
 
		#region UltraGridOverrideTypeConverter

		/// <summary>
		/// UltraGridOverride type converter.
		/// </summary>
		public sealed class UltraGridOverrideTypeConverter : ExpandableObjectConverter 
		{

            /// <summary>
            /// Returns a collection of properties for the type of array specified by the
            /// value parameter, using the specified context and attributes.
            /// </summary>
            /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
            /// <param name="value">An <see cref="System.Object"/> that specifies the type of array for which to get properties.</param>
            /// <param name="attributes">An array of type <see cref="System.Attribute"/> that is used as a filter.</param>
            /// <returns>A <see cref="System.ComponentModel.PropertyDescriptorCollection"/> with the properties that are exposed for this data type, or null if there are no properties.</returns>
            public override PropertyDescriptorCollection GetProperties(
				ITypeDescriptorContext context,
				object value,
				Attribute[] attributes )
			{
				PropertyDescriptorCollection props = base.GetProperties( context, value, attributes );

				UltraGridOverride overrideObj = value as UltraGridOverride;

				// JJD 2/4/02 - UWG578
				// If this is a dropdown or combo filter out properties
				// that don't make sense
				//
				if ( props != null	&&
					overrideObj != null &&
					overrideObj.layout != null	&&
					overrideObj.layout.Grid is UltraDropDownBase )
				{
					int count = 0;

					// count up all the properties that won't be filtered out
					//
					for ( int i = 0; i < props.Count; i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.FilterOutProperty( props[i] ) )
						if ( !UltraGridOverrideTypeConverter.FilterOutProperty( props[ i ] ) )
							count++;
					}

					// allocate an array of the proper size
					//
					PropertyDescriptor [] propArray = new PropertyDescriptor[count];

					int current = 0;

					// copy the unfiltered properties into the array
					//
					for ( int i = 0; i < props.Count; i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.FilterOutProperty( props[i] ) )
						if ( !UltraGridOverrideTypeConverter.FilterOutProperty( props[ i ] ) )
						{
							propArray[current] = props[i];
							current++;
						}
					}

					// return the filtered collection
					//
					return new PropertyDescriptorCollection( propArray );
				}

				return props;
			}

			// JJD 2/4/02 - UWG578
			// Filter out properties not meaningful to combos and dropdowns
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//private bool FilterOutProperty( PropertyDescriptor pd )
			private static bool FilterOutProperty( PropertyDescriptor pd )
			{
				switch ( pd.DisplayName )
				{
					case "ActiveCardCaptionAppearance":
					case "ActiveCellAppearance":
					case "CardAreaAppearance":
					case "CardCaptionAppearance":
					case "CellButtonAppearance":
					case "EditCellAppearance":
					case "GroupByColumnAppearance":
					case "GroupByColumnHeaderAppearance":
					case "GroupByRowAppearance":
					case "MaskLiteralsAppearance":
					case "SelectedCardCaptionAppearance":
					case "SelectedCellAppearance":

					case "AllowAddNew":
					case "AllowColMoving":
					case "AllowColSizing":
					case "AllowColSwapping":
					case "AllowDelete":
					case "AllowGroupBy":
					case "AllowGroupMoving":
					case "AllowGroupSwapping":
					case "AllowUpdate":

					case "BorderStyleCardArea":
					case "CardSpacing":
					case "CellClickAction":
					case "ExpansionIndicator":
					case "GroupByColumnsHidden":
					case "GroupByRowDescriptionMask":
					case "GroupByRowPadding":
						// SSP 1/18/05 BR01609
						// Since we added support for sorting in UltraDropDown don't filter out
						// the HeaderClickAction.
						//
						//case "HeaderClickAction":
					case "MaxSelectedCells":
					case "MaxSelectedRows":
					case "RowSizing":
					case "SelectTypeCell":
					case "SelectTypeCol":
					case "SelectTypeGroupByRow":
					case "SelectTypeRow":
					case "TipStyleRowConnector":
					// SSP 5/16/05 - Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					// Added following entries.
					//
					// ------------------------------------------------------------------------
					case "FilterCellAppearance":
					case "FilterCellAppearanceActive":
					case "FilterClearButtonAppearance":
					case "FilterClearButtonLocation":
					case "FilterEvaluationTrigger":
					case "FilterOperandStyle":
					case "FilterOperatorAppearance":
					case "FilterOperatorDefaultValue":
					case "FilterOperatorDropDownItems":
					case "FilterOperatorLocation":
					case "FilterRowAppearance":
					case "FilterRowAppearanceActive":
					case "FilterRowPrompt":
					case "FilterRowPromptAppearance":
					case "FilterRowSelectorAppearance":
					case "FilterRowSpacingAfter":
					case "FilterRowSpacingBefore":
					case "FilterUIType":
					case "FixedRowIndicator":
					case "AllowRowSummaries":
					case "TemplateAddRowAppearance":
					case "TemplateAddRowCellAppearance":
					case "TemplateAddRowPrompt":
					case "TemplateAddRowPromptAppearance":
					case "TemplateAddRowSpacingAfter":
					case "TemplateAddRowSpacingBefore":
					case "AddRowAppearance":
					case "AddRowCellAppearance":
					case "BorderStyleFilterCell":
					case "BorderStyleFilterRow":
					case "BorderStyleFilterOperator":
					case "BorderStyleTemplateAddRow":
					case "GroupBySummaryDisplayStyle":
					case "GroupBySummaryValueAppearance":
					// ------------------------------------------------------------------------
					{
						return true;
					}
				}

				return false;
			}
		}
		
		#endregion UltraGridOverrideTypeConverter

		//JDN 11/19/04 Added RowSelectorHeader
        #region RowSelectorHeaderStyle
		/// <summary>
		/// Returns or sets a value that determines the style of the row selector header area.
		/// </summary>
		///	<remarks>
		///	<p class="body">The RowSelectorHeaderStyle determines the look and position of the area above the Row Selectors if Row Selectors are visible.</p>
		///	<p class="body">If the RowSelectorHeaderStyle is set to <b>None</b> there will be no visible element assigned to this area and the header of the first column will be aligned with the left edge of the column.</p>
		///	<p class="body">If the RowSelectorHeaderStyle is set to <b>ExtendFirstColumn</b> the header of the first column will extend to the left over the area above the Row Selectors.</p>
		///	<p class="body">If the RowSelectorHeaderStyle is set to <b>SeparateElement</b> a RowSelectorHeaderUIElement will be positioned above the Row Selectors and the header of the first column will be aligned with the left edge of the column.</p>
		///	<p class="body">The Default value is ExtendFirstColumn.</p>
		///	<p class="body">If the RowSelectorHeaderStyle is set to SeparateElement the <see cref="RowSelectorHeaderAppearance"/> object can be used to set the appearance of the RowSelectorHeader.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSelectorHeaderStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		public RowSelectorHeaderStyle RowSelectorHeaderStyle
		{
			get { return this.rowSelectorHeaderStyle; }			

			set
			{
				if ( value != this.rowSelectorHeaderStyle )
				{
					// test that the value is in range
					if ( !Enum.IsDefined( typeof(RowSelectorHeaderStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_354") );

					this.rowSelectorHeaderStyle = value;
					this.NotifyPropChange( PropertyIds.RowSelectorHeaderStyle, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSelectorHeaderStyle() 
		{
			return ( this.rowSelectorHeaderStyle != RowSelectorHeaderStyle.Default );
		}
 
		/// <summary>
		/// Resets RowSelectorHeaderStyle to its default value.
		/// </summary>
		public void ResetRowSelectorHeaderStyle() 
		{
			this.RowSelectorHeaderStyle = RowSelectorHeaderStyle.Default;
		}
        #endregion // RowSelectorHeaderStyle

		// SSP 2/14/03 - RowSelectorWidth property
		// Added RowSelectorWidth property to allow the user to control the widths
		// of the row selectors.
		//
		/// <summary>
		/// Gets or sets the row selector widths. Default value is 0 which means a reasonable default width will be used for row selectors.
		/// </summary>
		/// <remarks>
		/// <p>RowSelectorWidth property can be used to control the widths of the row selectors. Default value is 0 which means a reasonable default width will be used for row selectors.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_Override_P_RowSelectorWidth") ]
		[ LocalizedCategory("LC_Display") ]
		public int RowSelectorWidth
		{
			get
			{
				return this.rowSelectorWidth;
			}
			set
			{
				if ( this.rowSelectorWidth != value )
				{
					if ( value < 0 )
						throw new ArgumentException( SR.GetString( "LER_ArguementException_3" ), "RowSelectorWidth" );

					this.rowSelectorWidth = value;

					this.NotifyPropChange( PropertyIds.RowSelectorWidth );
				}
			}
		}

		/// <summary>
		/// Returns true if the RowSelectorWidth property is set to a value other than the default value of 0.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSelectorWidth( )
		{
			return 0 != this.rowSelectorWidth;
		}

		/// <summary>
		/// Resets the RowSelectorWidth property to its default value of 0.
		/// </summary>
		public void ResetRowSelectorWidth( )
		{
			this.RowSelectorWidth = 0;
		}

		// SSP 3/6/03 - Row Layout Functionality
		// Added BorderStyleRowSelector proeprty.
		//
		#region BorderStyleRowSelector

		/// <summary>
		/// Gets or sets the border style of row selectors.
		/// </summary>
		/// <remarks>
		/// <p>You can use this property to change the border style of the row selectors. If this preoprty is set to Default, then <see cref="BorderStyleHeader"/> is used for the row selectors.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_Override_P_BorderStyleRowSelector") ]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleRowSelector
		{
			get
			{
				return this.borderStyleRowSelector;
			}
			set
			{
				if ( this.borderStyleRowSelector != value )
				{
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UIElementBorderStyle ), value ) )
						throw new InvalidEnumArgumentException( "BorderStyleRowSelector", (int)value, typeof( Infragistics.Win.UIElementBorderStyle ) );

					this.borderStyleRowSelector = value;

					this.NotifyPropChange( PropertyIds.BorderStyleRowSelector );
				}
			}
		}

		#endregion // BorderStyleRowSelector

		#region ShouldSerializeBorderStyleRowSelector

		/// <summary>
		/// Returns true if the BorderStyleRowSelector property is set to a value other than the default value of <b>Default</b>.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleRowSelector( )
		{
			return UIElementBorderStyle.Default != this.borderStyleRowSelector;
		}

		#endregion // ShouldSerializeBorderStyleRowSelector

		#region ResetBorderStyleRowSelector

		/// <summary>
		/// Resets the BorderStyleRowSelector property to its default value of <b>Default</b>.
		/// </summary>
		public void ResetBorderStyleRowSelector( )
		{
			this.BorderStyleRowSelector = UIElementBorderStyle.Default;
		}

		#endregion // ResetBorderStyleRowSelectro

		#region AllowRowLayoutCellSizing

		/// <summary>
		/// Indicates whether the user is allowed to resize cells in the row-layout mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can set <see cref="RowLayoutColumnInfo.AllowCellSizing"/> and <see cref="RowLayoutColumnInfo.AllowLabelSizing"/> for individual coumns to control per column resizing behavior.</p>
		/// <p><seealso cref="UltraGridOverride.AllowRowLayoutLabelSizing"/> <seealso cref="RowLayoutColumnInfo.AllowLabelSizing"/> <seealso cref="RowLayoutColumnInfo.AllowCellSizing"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_Override_P_AllowRowLayoutCellSizing") ]
		[ LocalizedCategory("LC_Behavior") ]
		public RowLayoutSizing AllowRowLayoutCellSizing
		{
			get
			{
				return this.allowRowLayoutCellSizing;
			}
			set
			{
				if ( this.allowRowLayoutCellSizing != value )
				{
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.RowLayoutSizing ), value ) )
						throw new InvalidEnumArgumentException( "AllowRowLayoutCellSizing", (int)value, typeof( Infragistics.Win.UltraWinGrid.RowLayoutSizing ) );

					this.allowRowLayoutCellSizing = value;

					this.NotifyPropChange( PropertyIds.AllowRowLayoutCellSizing );
				}
			}
		}

		#endregion // AllowRowLayoutCellSizing

		#region ShouldSerializeAllowRowLayoutCellSizing

		/// <summary>
		/// Returns true if the AllowRowLayoutCellSizing property is set to a value other than the default value of <b>Default</b>.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowRowLayoutCellSizing( )
		{
			return Infragistics.Win.UltraWinGrid.RowLayoutSizing.Default != this.allowRowLayoutCellSizing;
		}

		#endregion // ShouldSerializeAllowRowLayoutCellSizing

		#region ResetAllowRowLayoutCellSizing

		/// <summary>
		/// Resets the AllowRowLayoutCellSizing property to its default value of <b>Default</b>.
		/// </summary>
		public void ResetAllowRowLayoutCellSizing( )
		{
			this.AllowRowLayoutCellSizing = RowLayoutSizing.Default;
		}

		#endregion // ResetAllowRowLayoutCellSizing

		#region AllowRowLayoutLabelSizing

		/// <summary>
		/// Indicates whether the user is allowed to resize cell labels (column labels associated with the cells) in the row-layout mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can set <see cref="RowLayoutColumnInfo.AllowCellSizing"/> and <see cref="RowLayoutColumnInfo.AllowLabelSizing"/> for individual coumns to control per column resizing behavior.</p>
		/// <p><seealso cref="UltraGridOverride.AllowRowLayoutCellSizing"/> <seealso cref="RowLayoutColumnInfo.AllowLabelSizing"/> <seealso cref="RowLayoutColumnInfo.AllowCellSizing"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_Override_P_AllowRowLayoutLabelSizing") ]
		[ LocalizedCategory("LC_Behavior") ]
		public RowLayoutSizing AllowRowLayoutLabelSizing
		{
			get
			{
				return this.allowRowLayoutLabelSizing;
			}
			set
			{
				if ( this.allowRowLayoutLabelSizing != value )
				{
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.RowLayoutSizing ), value ) )
						throw new InvalidEnumArgumentException( "AllowRowLayoutLabelSizing", (int)value, typeof( Infragistics.Win.UltraWinGrid.RowLayoutSizing ) );

					this.allowRowLayoutLabelSizing = value;

					this.NotifyPropChange( PropertyIds.AllowRowLayoutLabelSizing );
				}
			}
		}

		#endregion // AllowRowLayoutLabelSizing

		#region ShouldSerializeAllowRowLayoutLabelSizing

		/// <summary>
		/// Returns true if the AllowRowLayoutLabelSizing property is set to a value other than the default value of <b>Default</b>.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowRowLayoutLabelSizing( )
		{
			// SSP 6/26/03 UWG2387
			// Typo. Use allowRowLayoutLabelSizing instead of allowRowLayoutCellSizing.
			//
			//return Infragistics.Win.UltraWinGrid.RowLayoutSizing.Default != this.allowRowLayoutCellSizing;
			return Infragistics.Win.UltraWinGrid.RowLayoutSizing.Default != this.allowRowLayoutLabelSizing;
		}

		#endregion // ShouldSerializeAllowRowLayoutLabelSizing

		#region ResetAllowRowLayoutLabelSizing

		/// <summary>
		/// Resets the AllowRowLayoutLabelSizing property to its default value of <b>Default</b>.
		/// </summary>
		public void ResetAllowRowLayoutLabelSizing( )
		{
			this.AllowRowLayoutLabelSizing = RowLayoutSizing.Default;
		}

		#endregion // ResetAllowRowLayoutLabelSizing

		// SSP 4/14/03
		// Added ColumnAutoSizeMode property.
		//
		#region ColumnAutoSizeMode

		/// <summary>
		/// Specifies column autosizing mode. If <see cref="UltraGridOverride.AllowColSizing "/> is set to None, then this is ignored.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// When set the None, the user is not allowed to auto resize a column. If set to VisibleRows, then the column is resized based on the data in the visible rows. If set to SiblingRowsOnly, then the column is autoresized based on data in all the sibling rows. If set to AllRowsInBand, then the column is auto resized based on data in all rows in the band.
		/// </p>
        /// <p class="body">Note that this property determines if and how columns are auto-resized when the user auto-resizes one through the user interface by double-clicking on the right edge of the column header. It does not affect how the <see cref="UltraGridColumn.PerformAutoResize( Infragistics.Win.UltraWinGrid.PerformAutoSizeType )"/> method works. When calling that method, you will need to explicitly specify the ColumnAutoSizeMode parameter.</p>
		/// </remarks>
        [LocalizedDescription("LDR_UltraGridOverride_P_ColumnAutoSizeMode")]
        public ColumnAutoSizeMode ColumnAutoSizeMode
		{
			get
			{
				return this.columnAutoSizeMode;
			}
			set
			{
				if ( this.columnAutoSizeMode != value )
				{
					if ( ! Enum.IsDefined( typeof( ColumnAutoSizeMode ), value ) )
						throw new InvalidEnumArgumentException( "ColumnAutoSizeMode", (int)value, typeof( ColumnAutoSizeMode ) );

					this.columnAutoSizeMode = value;

					this.NotifyPropChange( PropertyIds.ColumnAutoSizeMode );
				}
			}
		}

		#endregion // ColumnAutoSizeMode

		#region ResetColumnAutoSizeMode

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetColumnAutoSizeMode( )
		{
			this.ColumnAutoSizeMode = ColumnAutoSizeMode.Default;
		}

		#endregion // ResetColumnAutoSizeMode

		#region ShouldSerializeColumnAutoSizeMode

		/// <summary>
		/// Returns true if the property is set to a value other than the default.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeColumnAutoSizeMode( )
		{
			return ColumnAutoSizeMode.Default != this.ColumnAutoSizeMode;
		}

		#endregion // ShouldSerializeColumnAutoSizeMode

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		#region CellDisplayStyle

		/// <summary>
		/// CellDisplayStyle specifies how the cells get rendered. You can use this property to speed up rendering of cells by setting it to <b>FormattedText</b> or <b>PlainText</b>. Default is resolved to <b>FullEditorDisplay</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>CellDisplayStyle</b> specifies how the cells get rendered. You can use this property to speed up rendering of cells by setting it to <b>FormattedText</b> or <b>PlainText</b>. Default is resolved to <b>FullEditorDisplay</b>.</p>
		/// <p class="body"><b>FormattedText</b> draws the formatted cell value in the cells. <b>PlainText</b> draws the cell value converted to text withought applying any formatting. It simply calls ToString on the cell value and draws the resulting text in the cell.<b>FullEditorDisplay</b> embedds an embeddable ui element which draws the cell value as well as draws any edit elements like buttons. This is a bit more performance intensive than <b>PlainText</b> and <b>FormattedText</b>. The advantage of using <b>FullEditorDisplay</b> which is the default is that any edit elements that the editor may render in cells do not get rendered until the cell enters the edit mode.</p>
		/// <seealso cref="UltraGridColumn.CellDisplayStyle"/>
		/// <seealso cref="UltraGridColumn.Style"/>
		/// </remarks>
        [LocalizedDescription("LDR_UltraGridOverride_P_CellDisplayStyle")]
        public Infragistics.Win.UltraWinGrid.CellDisplayStyle CellDisplayStyle
		{
			get
			{
				return this.cellDisplayStyle;
			}
			set
			{
				if ( this.cellDisplayStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( CellDisplayStyle ), value ) )
						throw new InvalidEnumArgumentException( "CellDisplayStyle", (int)value, typeof( CellDisplayStyle ) );

					this.cellDisplayStyle = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.CellDisplayStyle );
				}
			}
		}

		#endregion // CellDisplayStyle

		#region ResetCellDisplayStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetCellDisplayStyle( )
		{
			this.CellDisplayStyle = Infragistics.Win.UltraWinGrid.CellDisplayStyle.Default;
		}

		#endregion // ResetCellDisplayStyle

		#region ShouldSerializeCellDisplayStyle

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeCellDisplayStyle( )
		{
			return Infragistics.Win.UltraWinGrid.CellDisplayStyle.Default != this.CellDisplayStyle;
		}

		#endregion // ShouldSerializeCellDisplayStyle

		// SSP 5/19/03 - Fixed headers
		//
		#region FixedHeaderIndicator

		/// <summary>
		/// Specifies whether the user is allowed to fix or unfix headers in a band or a grid. NOTE: This property is ignored in Row-Layout mode as headers can't be fixed in Row-Layout mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">FixedHeaderIndicator property specifies whether the user is allowed to fix or unfix headers.</p>
		/// <p class="body">NOTE: This property is ignored in Row-Layout mode as headers can't be fixed in Row-Layout mode.</p>
		/// <p><seealso cref="Infragistics.Win.UltraWinGrid.FixedHeaderIndicator"/> <seealso cref="HeaderBase.FixedHeaderIndicator"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FixedHeaderIndicator")]
		[ LocalizedCategory("LC_Behavior") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.UltraWinGrid.FixedHeaderIndicator FixedHeaderIndicator
		{
			get
			{
				return this.fixedHeaderIndicator;
			}
			set
			{
				if ( value != this.fixedHeaderIndicator )
				{
					if ( ! Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.FixedHeaderIndicator ), value ) )
						throw new InvalidEnumArgumentException( "FixedHeaderIndicator", (int)value, typeof( Infragistics.Win.UltraWinGrid.FixedHeaderIndicator ) );

					this.fixedHeaderIndicator = value;
					
					this.NotifyPropChange( PropertyIds.FixedHeaderIndicator );
				}
			}
		}

		#endregion // FixedHeaderIndicator

		#region ShouldSerializeFixedHeaderIndicator

		/// <summary>
		/// Retruns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedHeaderIndicator( )
		{
			return Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default != this.fixedHeaderIndicator;
		}

		#endregion // ShouldSerializeFixedHeaderIndicator

		#region ResetFixedHeaderIndicator

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedHeaderIndicator( )
		{
			this.FixedHeaderIndicator = Infragistics.Win.UltraWinGrid.FixedHeaderIndicator.Default;
		}

		#endregion // ResetFixedHeaderIndicator

		#region FixedCellSeparatorColor

		/// <summary>
		/// Specifies the color of the separator line that separates the cells associated with fixed headers and cells associated with non-fixed headers.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridLayout.UseFixedHeaders"/> <seealso cref="HeaderBase.Fixed"/> <seealso cref="UltraGridOverride.FixedHeaderAppearance"/> <seealso cref="UltraGridOverride.FixedCellAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FixedCellSeparatorColor")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Color FixedCellSeparatorColor
		{
			get
			{
				return this.fixedCellSeparatorColor;
			}
			set
			{
				if ( value != this.fixedCellSeparatorColor )
				{
					this.fixedCellSeparatorColor = value;

					this.NotifyPropChange( PropertyIds.FixedCellSeparatorColor );
				}
			}
		}

		#endregion // FixedCellSeparatorColor

		#region ShouldSerializeFixedCellSeparatorColor

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedCellSeparatorColor( )
		{
			return Color.Empty != this.fixedCellSeparatorColor;
		}

		#endregion // ShouldSerializeFixedCellSeparatorColor

		#region ResetFixedCellSeparatorColor

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedCellSeparatorColor( )
		{
			this.FixedCellSeparatorColor = Color.Empty;
		}

		#endregion // ResetFixedCellSeparatorColor

		#region FixedHeaderAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to fixed headers in a band or the grid.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.FixedCellAppearance"/> <seealso cref="UltraGridLayout.UseFixedHeaders"/> <seealso cref="UltraGridOverride.FixedHeaderIndicator"/> <seealso cref="HeaderBase.Fixed"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FixedHeaderAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FixedHeaderAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FixedHeaderAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FixedHeaderAppearance, value );
			}
		}

		#endregion // FixedHeaderAppearance

		#region ShouldSerializeFixedHeaderAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFixedHeaderAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FixedHeaderAppearance ); 
		}

		#endregion // ShouldSerializeFixedHeaderAppearance
 
		#region ResetFixedHeaderAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedHeaderAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FixedHeaderAppearance ); 
		}

		#endregion // ResetFixedHeaderAppearance

		#region FixedCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to cells associated with fixed headers in a band or the grid.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.FixedHeaderAppearance"/> <seealso cref="UltraGridLayout.UseFixedHeaders"/> <seealso cref="UltraGridOverride.FixedHeaderIndicator"/> <seealso cref="HeaderBase.Fixed"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FixedCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FixedCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FixedCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FixedCellAppearance , value );
			}
		}

		#endregion // FixedCellAppearance

		#region ShouldSerializeFixedCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFixedCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FixedCellAppearance ); 
		}

		#endregion // ShouldSerializeFixedCellAppearance
 
		#region ResetFixedCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FixedCellAppearance ); 
		}

		#endregion // ResetFixedCellAppearance

		// SSP 11/7/03 - Add Row Feature
		//
		#region Template Add-Row Feature

		// SSP 12/11/03 UWG2766
		// Added AddRowCellAppearance and TemplateAddRowCellAppearance properties.
		//
		#region TemplateAddRowCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to cells of template add-rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_TemplateAddRowCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase TemplateAddRowCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.TemplateAddRowCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.TemplateAddRowCellAppearance, value );
			}
		}

		#endregion // TemplateAddRowCellAppearance

		#region ShouldSerializeTemplateAddRowCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeTemplateAddRowCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.TemplateAddRowCellAppearance ); 
		}

		#endregion // ShouldSerializeTemplateAddRowCellAppearance
 
		#region ResetTemplateAddRowCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetTemplateAddRowCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.TemplateAddRowCellAppearance );
		}

		#endregion // ResetTemplateAddRowCellAppearance

		#region AddRowCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to cells of add-rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_AddRowCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase AddRowCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.AddRowCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.AddRowCellAppearance, value );
			}
		}

		#endregion // AddRowCellAppearance

		#region ShouldSerializeAddRowCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAddRowCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.AddRowCellAppearance ); 
		}

		#endregion // ShouldSerializeAddRowCellAppearance
 
		#region ResetAddRowCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAddRowCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.AddRowCellAppearance );
		}

		#endregion // ResetAddRowCellAppearance

		#region TemplateAddRowAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to template add-rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_TemplateAddRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase TemplateAddRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.TemplateAddRowAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.TemplateAddRowAppearance, value );
			}
		}

		#endregion // TemplateAddRowAppearance

		#region ShouldSerializeTemplateAddRowAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeTemplateAddRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.TemplateAddRowAppearance ); 
		}

		#endregion // ShouldSerializeTemplateAddRowAppearance
 
		#region ResetTemplateAddRowAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetTemplateAddRowAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.TemplateAddRowAppearance );
		}

		#endregion // ResetTemplateAddRowAppearance

		#region AddRowAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied add-rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_AddRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase AddRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.AddRowAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.AddRowAppearance, value );
			}
		}

		#endregion // AddRowAppearance

		#region ShouldSerializeAddRowAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAddRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.AddRowAppearance ); 
		}

		#endregion // ShouldSerializeAddRowAppearance
 
		#region ResetAddRowAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAddRowAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.AddRowAppearance );
		}

		#endregion // ResetAddRowAppearance

		#region TemplateAddRowSpacingBefore

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered before a template add-row (between the top of the row and the bottom edge of the above object.)
		/// </summary>
		///	<remarks>
		///	<p class="body">TemplateAddRowSpacingBefore is set to default, <see cref="RowSpacingBefore"/> will be used.</p>
		///	<seealso cref="TemplateAddRowSpacingAfter"/> <seealso cref="RowSpacingBefore"/> <seealso cref="RowSpacingAfter"/>
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_TemplateAddRowSpacingBefore")]
		[ LocalizedCategory("LC_Display") ]
		public int TemplateAddRowSpacingBefore
		{
			get
			{
				return this.templateAddRowSpacingBefore;
			}

			set
			{
				if ( value != this.templateAddRowSpacingBefore )
				{
					// if negative then set to -1 (default)
					//
					if ( value < 0 )
						this.templateAddRowSpacingBefore = -1;
					else
						this.templateAddRowSpacingBefore = value;
    		    	
					this.NotifyPropChange( PropertyIds.TemplateAddRowSpacingBefore, null );
				}
			}
		}

		#endregion // TemplateAddRowSpacingBefore

		#region ShouldSerializeTemplateAddRowSpacingBefore

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeTemplateAddRowSpacingBefore( ) 
		{
			return this.templateAddRowSpacingBefore >= 0;
		}

		#endregion // ShouldSerializeTemplateAddRowSpacingBefore
 
		#region ResetTemplateAddRowSpacingBefore

		/// <summary>
		/// Resets TemplateAddRowSpacingBefore to its default value (-1).
		/// </summary>
		public void ResetTemplateAddRowSpacingBefore( ) 
		{
			this.TemplateAddRowSpacingBefore = -1;
		}

		#endregion // ResetTemplateAddRowSpacingBefore

		#region TemplateAddRowSpacingAfter

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered after a template add-row (between the bottom of the row and the top edge of the following object.)
		/// </summary>
		///	<remarks>
		///	<p class="body">TemplateAddRowSpacingAfter is set to default, <see cref="RowSpacingAfter"/> will be used.</p>
		///	<seealso cref="TemplateAddRowSpacingBefore"/> <seealso cref="RowSpacingBefore"/> <seealso cref="RowSpacingAfter"/>
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_TemplateAddRowSpacingAfter")]
		[ LocalizedCategory("LC_Display") ]
		public int TemplateAddRowSpacingAfter
		{
			get
			{
				return this.templateAddRowSpacingAfter;
			}

			set
			{
				if ( value != this.templateAddRowSpacingAfter )
				{
					// If negative then set to -1 (default).
					//
					if ( value < 0 )
						this.templateAddRowSpacingAfter = -1;
					else
						this.templateAddRowSpacingAfter = value;
    		    	
					this.NotifyPropChange( PropertyIds.TemplateAddRowSpacingAfter, null );
				}
			}
		}

		#endregion // TemplateAddRowSpacingAfter

		#region ShouldSerializeTemplateAddRowSpacingAfter

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeTemplateAddRowSpacingAfter( )
		{
			return this.templateAddRowSpacingAfter >= 0;
		}

		#endregion // ShouldSerializeTemplateAddRowSpacingAfter
 
		#region ResetTemplateAddRowSpacingAfter

		/// <summary>
		/// Resets TemplateAddRowSpacingAfter to its default value (-1).
		/// </summary>
		public void ResetTemplateAddRowSpacingAfter( ) 
		{
			this.TemplateAddRowSpacingAfter = -1;
		}

		#endregion // ResetTemplateAddRowSpacingAfter

		#region BorderStyleTemplateAddRow

		/// <summary>
		/// Returns or sets a value that determines the border style of template add-rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of template add-rows in the band or the grid controlled by the specified override. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_BorderStyleTemplateAddRow")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleTemplateAddRow
		{
			get
			{
				return this.borderStyleTemplateAddRow;
			}

			set
			{
				if ( value != this.borderStyleTemplateAddRow )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(UIElementBorderStyle), value ) )
						throw new InvalidEnumArgumentException( "BorderStyleTemplateAddRow", (int)value, typeof( UIElementBorderStyle ) );

					this.borderStyleTemplateAddRow = value;
					this.NotifyPropChange( PropertyIds.BorderStyleTemplateAddRow, null );
				}
			}
		}

		#endregion // BorderStyleTemplateAddRow

		#region ShouldSerializeBorderStyleTemplateAddRow

		/// <summary>
		/// Returns true if the key needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleTemplateAddRow( )
		{
			return this.borderStyleTemplateAddRow != UIElementBorderStyle.Default;
		}

		#endregion // ShouldSerializeBorderStyleTemplateAddRow
 
		#region ResetBorderStyleTemplateAddRow

		/// <summary>
		/// Resets BorderStyleTemplateAddRow to its default value.
		/// </summary>
		public void ResetBorderStyleTemplateAddRow( )
		{
			this.BorderStyleTemplateAddRow = UIElementBorderStyle.Default;
		}

		#endregion // ResetBorderStyleTemplateAddRow

		#endregion // Template Add-Row Feature

		#region MinRowHeight

		// SSP 11/25/03 UWG2013 UWG2553
		// Added MinRowHeight property to Override to allow for unrestricted control over the 
		// row heights.
		//
		/// <summary>
		/// Specifies the minimum row height. By default the row heights are restricted by the font size and any drop down buttons that the cell may have. You can use this property to prevent the UltraGrid from imposing such a limit so you can programmatically set the row height to a smaller value than what UltraGrid calculates the minimum row height to be. Default value is -1. Setting this property to 0 will throw an exception since minimum row height has to be at least 1.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>MinRowHeight</b> specifies the minimum row height. By default the row heights are restricted by the font size and any drop down buttons that the cell may have. You can use this property to prevent the UltraGrid from imposing such a limit so you can programmatically set the row height to a smaller value than what UltraGrid calculates the minimum row height to be. UltraGrid will use this as the minimum row height. Setting this property to 0 will throw an exception since minimum row height has to be at least 1.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_MinRowHeight")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MinRowHeight
		{
			get
			{
				return this.minRowHeight;
			}
			set
			{
				if ( this.minRowHeight != value )
				{
					if ( 0 == value )
						throw new ArgumentOutOfRangeException( "MinRowHeight", value, SR.GetString( "LER_ArguementOutOfRangeException13" ) );

					this.minRowHeight = Math.Max( -1, value );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.MinRowHeight );
				}
			}
		}

		#endregion // MinRowHeight

		#region ShouldSerializeMinRowHeight

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeMinRowHeight( )
		{
			return this.minRowHeight > 0;
		}

		#endregion // ShouldSerializeMinRowHeight

		#region ResetMinRowHeight

		/// <summary>
		/// Resets the <see cref="MinRowHeight"/> to its default value of -1.
		/// </summary>
		public void ResetMinRowHeight( )
		{
			this.MinRowHeight = -1;
		}

		#endregion // ResetMinRowHeight

		// SSP 12/12/03 DNF135
		// Added a way to control whether ink buttons get shown.
		// Added ShowInkButton property.
		//
		#region ShowInkButton

		/// <summary>
		/// Specifies whether ink editor buttons get shown in cells.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use <b>ShowInkButton</b> property to explicitly turn on or turn off inck buttons.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_ShowInkButton")]
		[ LocalizedCategory("LC_Appearance") ]
		public ShowInkButton ShowInkButton
		{
			get
			{
				return this.showInkButton;
			}
			set
			{
				if ( this.showInkButton != value )
				{
					if ( ! Enum.IsDefined( typeof( ShowInkButton ), value ) )
						throw new InvalidEnumArgumentException( "ShowInkButton", (int)value, typeof( ShowInkButton ) );

					this.showInkButton = value;

					this.NotifyPropChange( PropertyIds.ShowInkButton );
				}
			}
		}

		#endregion // ShowInkButton

		#region ShouldSerializeShowInkButton

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeShowInkButton( )
		{
			return ShowInkButton.Default != this.showInkButton;
		}

		#endregion // ShouldSerializeShowInkButton

		#region ResetShowInkButton

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetShowInkButton( )
		{
			this.ShowInkButton = ShowInkButton.Default;
		}

		#endregion // ResetShowInkButton

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		#region ShowCalculatingText

		/// <summary>
		/// Specifies whether cells or summaries in the grid will display "#Calculating" when calculating with an UltraCalcManager.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use <b>ShowCalculatingText</b> property to explicitly turn on or turn off the display of the text during calculations.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_ShowCalculatingText")]
		[ LocalizedCategory("LC_Appearance") ]
		public DefaultableBoolean ShowCalculatingText
		{
			get
			{
				return this.showCalculatingText;
			}
			set
			{
				if ( this.showCalculatingText != value )
				{
					if ( ! Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
						throw new InvalidEnumArgumentException( "ShowCalculatingText", (int)value, typeof( DefaultableBoolean ) );

					this.showCalculatingText = value;

					this.NotifyPropChange( PropertyIds.ShowCalculatingText );
				}
			}
		}

		#endregion // ShowCalculatingText

		#region ShouldSerializeShowCalculatingText

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeShowCalculatingText( )
		{
			return DefaultableBoolean.Default != this.showCalculatingText;
		}

		#endregion // ShouldSerializeShowCalculatingText

		#region ResetShowCalculatingText

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetShowCalculatingText( )
		{
			this.ShowCalculatingText = DefaultableBoolean.Default;
		}

		#endregion // ResetShowCalculatingText

		// MRS 2/23/04 - Added Preset support
		#region Implementation of ISupportPresets
		/// <summary>
		/// Returns a list of properties which can be used in a Preset
		/// </summary>
		/// <param name="presetType">Determines which type(s) of properties are returned</param>
		/// <returns>An array of strings indicating property names</returns>
		string[] ISupportPresets.GetPresetProperties(Infragistics.Win.PresetType presetType)
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList properties = new ArrayList();
			List<string> properties = new List<string>();

			//Appearance
			if ((presetType & Infragistics.Win.PresetType.Appearance) == Infragistics.Win.PresetType.Appearance)
			{
				properties.Add("ActiveCardCaptionAppearance");				
				properties.Add("ActiveCellAppearance");	
				properties.Add("ActiveRowAppearance");
				// SSP 10/19/07 BR25706 BR25707
				// Added ActiveRowCellAppearance. This is to let one be able to specify the 
				// appearance for cell of the active row.
				// 
				properties.Add( "ActiveRowCellAppearance" );
				properties.Add("AddRowAppearance");	
				properties.Add("AddRowCellAppearance");	
				properties.Add("BorderStyleCardArea");	
				properties.Add("BorderStyleCell");	
				properties.Add("BorderStyleHeader");	
				properties.Add("BorderStyleRow");	
				properties.Add("BorderStyleRowSelector");	
				properties.Add("BorderStyleSummaryFooter");	
				properties.Add("BorderStyleSummaryFooterCaption");	
				properties.Add("BorderStyleSummaryValue");	
				properties.Add("BorderStyleTemplateAddRow");

				// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
				//
				properties.Add("ButtonStyle");

				properties.Add("CardAreaAppearance");	
				properties.Add("CardCaptionAppearance");	
				properties.Add("CardSpacing");	
				properties.Add("CellAppearance");	
				properties.Add("CellButtonAppearance");	
				properties.Add("CellPadding");	
				properties.Add("CellSpacing");	
				properties.Add("DefaultColWidth");	
				properties.Add("DefaultRowHeight");	
				properties.Add("EditCellAppearance");					

				// SSP 5/3/05 - NAS 5.2 Fixed Row/Filter Row/Fixed Add-Row/Summaries Extention
				//
				properties.Add( "FilterCellAppearance" );
				properties.Add( "FilterCellAppearanceActive" );
				properties.Add( "FilterClearButtonAppearance" );
				properties.Add( "FilterClearButtonLocation" );

				properties.Add("FilteredInCellAppearance");	
				properties.Add("FilteredInRowAppearance");	
				properties.Add("FilteredOutCellAppearance");	
				properties.Add("FilteredOutRowAppearance");

				// SSP 5/3/05 - NAS 5.2 Fixed Row/Filter Row/Fixed Add-Row/Summaries Extention
				//
				properties.Add( "FilterOperatorAppearance" );
				properties.Add( "FilterOperatorLocation" );
				properties.Add( "FilterRowAppearance" );
				properties.Add( "FilterRowAppearanceActive" );
				properties.Add( "FilterRowPrompt" );
				properties.Add( "FilterRowPromptAppearance" );
				properties.Add( "FilterRowSelectorAppearance" );
				properties.Add( "FilterRowSpacingAfter" );
				properties.Add( "FilterRowSpacingBefore" );

				properties.Add("FixedCellAppearance");	
				properties.Add("FixedCellSeparatorColor");	
				properties.Add("FixedHeaderAppearance");				

				// SSP 5/3/05 - NAS 5.2 Fixed Row/Filter Row/Fixed Add-Row/Summaries Extention
				//
				properties.Add("FixedRowAppearance");
				properties.Add("FixedRowSelectorAppearance");

				// SSP 7/6/04 - UltraCalc
				// Added FormulaErrorAppearance property.
				//
				properties.Add( "FormulaErrorAppearance" );

				properties.Add("GroupByColumnAppearance");	
				properties.Add("GroupByColumnHeaderAppearance");					
				properties.Add("GroupByRowAppearance");	
				properties.Add("GroupByRowDescriptionMask");	
				properties.Add("GroupByRowPadding");	
				// SSP 12/21/04 BR01386
				// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
				//
				properties.Add("GroupByRowSpacingAfter");
				properties.Add("GroupByRowSpacingBefore");

				// SSP 5/3/05 - NAS 5.2 Fixed Row/Filter Row/Fixed Add-Row/Summaries Extention
				//
				properties.Add( "GroupBySummaryDisplayStyle" );
				properties.Add( "GroupBySummaryValueAppearance" );

				// SSP 7/14/05 - NAS 5.3 Header Placement
				// 
				properties.Add("HeaderPlacement");

				// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
				//
				properties.Add("HeaderStyle");
				properties.Add("RowSelectorStyle");
                
				properties.Add("HeaderAppearance");			

				// SSP 8/11/05 - NAS 5.3 Hot Track Appearances
				// 
				properties.Add("HotTrackCellAppearance");
				properties.Add("HotTrackHeaderAppearance");
				properties.Add("HotTrackRowAppearance");
				properties.Add("HotTrackRowCellAppearance");
				properties.Add("HotTrackRowSelectorAppearance");

				// SSP 8/11/05 - NAS 5.3 Invalid Value Behavior
				// 
				properties.Add("InvalidValueBehavior");

				properties.Add("MaskLiteralsAppearance");
				// SSP 11/3/04 - Merged Cell Feature
				// Added entries for MergedCellAppearance and MergedCellContentArea.
				//
				properties.Add( "MergedCellAppearance" );
				properties.Add( "MergedCellContentArea" );
				// SSP 8/26/06 - NAS 6.3
				// Added ReadOnlyCellAppearance to the override.
				// 
				properties.Add( "ReadOnlyCellAppearance" );
				// MRS 5/26/04 - Moved to Behavior
				//properties.Add("NullText");	
				properties.Add("RowAlternateAppearance");	
				properties.Add("RowAppearance");	
				properties.Add("RowPreviewAppearance");	
				properties.Add("RowSelectorAppearance");

				//JDN 11/19/04 Added RowSelectorHeader
				properties.Add("RowSelectorHeaderAppearance");
				properties.Add("RowSelectorHeaderStyle");
	
				// SSP 3/30/05 - NAS 5.2 Row Numbers
				//
				properties.Add("RowSelectorNumberStyle");

				properties.Add("RowSelectorWidth");	
				//MRS 4/27/04 - Moved RowSelectors from Behavior
				properties.Add("RowSelectors");

				properties.Add("RowSpacingAfter");	
				properties.Add("RowSpacingBefore");	
				properties.Add("SelectedCardCaptionAppearance");	
				properties.Add("SelectedCellAppearance");	
				properties.Add("SelectedRowAppearance");

				// SSP 5/3/05 - NAS 5.2 Fixed Row/Filter Row/Fixed Add-Row/Summaries Extention
				//
				properties.Add( "SpecialRowSeparator" );
				properties.Add( "SpecialRowSeparatorAppearance" );
				properties.Add( "SpecialRowSeparatorHeight" );

				properties.Add("SummaryFooterAppearance");	
				properties.Add("SummaryFooterCaptionAppearance");	
				properties.Add("SummaryFooterCaptionVisible");	
				properties.Add("SummaryValueAppearance");
				properties.Add("TemplateAddRowAppearance");	
				properties.Add("TemplateAddRowCellAppearance");	
				properties.Add("TemplateAddRowSpacingAfter");	
				properties.Add("TemplateAddRowSpacingBefore");

                // CDS 7/24/09 NAS 9.2 Cell ActiveAppearance Related properties.
                properties.Add("ActiveAppearancesEnabled");
                properties.Add("SelectedAppearancesEnabled");
            }

			//Behavior
			if ((presetType & Infragistics.Win.PresetType.Behavior) == Infragistics.Win.PresetType.Behavior)
			{
				properties.Add("ExpansionIndicator");
				properties.Add("FixedHeaderIndicator");	
				properties.Add("HeaderClickAction");
				properties.Add("GroupByColumnsHidden");	
				//MRS 4/27/04 - Moved to Appearance
				//properties.Add("RowSelectors");				
				properties.Add("AllowAddNew");
				properties.Add("AllowColMoving");
				properties.Add("AllowColSizing");
				properties.Add("AllowDelete");
				properties.Add("AllowGroupBy");
				properties.Add("AllowGroupMoving");
				properties.Add("AllowGroupSwapping");

				// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
				// 
				properties.Add("AllowMultiCellOperations");

				properties.Add("AllowRowFiltering");
				properties.Add("AllowRowLayoutCellSizing");
				properties.Add("AllowRowLayoutLabelSizing");
				// SSP 9/28/06 BR16312
				// 
				properties.Add("AllowRowLayoutColMoving");
				properties.Add("AllowRowLayoutCellSpanSizing");
				properties.Add("AllowRowLayoutLabelSpanSizing");

				properties.Add("AllowRowSummaries");
				properties.Add("AllowUpdate");
				properties.Add("CellClickAction");
				properties.Add("CellDisplayStyle");
				properties.Add("CellMultiLine");
				properties.Add("ColumnAutoSizeMode");
				// SSP 11/16/04
				// Implemented column sizing using cells in non-rowlayout mode. 
				// Added ColumnSizingArea property to the override.
				//
				properties.Add("ColumnSizingArea");

				// SSP 5/3/05 - NAS 5.2 Fixed Row/Filter Row/Fixed Add-Row/Summaries Extention
				//
				properties.Add( "FilterComparisonType" );
				properties.Add( "FilterEvaluationTrigger" );
				properties.Add( "FilterOperandStyle" );
				properties.Add( "FilterOperatorDefaultValue" );
				properties.Add( "FilterOperatorDropDownItems" );
				properties.Add( "FilterUIType" );
				properties.Add( "FixedRowIndicator" );
				properties.Add( "FixedRowsLimit" );
				properties.Add( "FixedRowSortOrder" );
				properties.Add( "FixedRowStyle" );

				// JAS v5.2 4/8/05
				properties.Add("GroupByRowExpansionStyle");
				properties.Add("GroupByRowInitialExpansionState");

				properties.Add("MaxSelectedCells");
				properties.Add("MaxSelectedRows");
				// SSP 11/3/04 - Merged Cell Feature
				// Added MergedCellStyle property.
				//
				properties.Add("MergedCellStyle");
				properties.Add("MinRowHeight");

				// SSP 2/24/06 BR09715
				// Added MultiCellSelectionMode property to support snaking cell selection.
				// 
				properties.Add( "MultiCellSelectionMode" );

				properties.Add("RowFilterAction");
				properties.Add("RowFilterMode");

				// SSP 8/17/05 BR05463
				// Added RowLayoutCellNavigationVertical property.
				// 
				properties.Add("RowLayoutCellNavigationVertical");

				properties.Add("RowSizing");
				properties.Add("RowSizingArea");
				properties.Add("RowSizingAutoMaxLines");
				properties.Add("SelectTypeCell");
				properties.Add("SelectTypeCol");
				properties.Add("SelectTypeGroupByRow");
				properties.Add("SelectTypeRow");

				// SSP 5/3/05 - NAS 5.2 Fixed Row/Filter Row/Fixed Add-Row/Summaries Extention
				//
				properties.Add( "SequenceFilterRow" );
				properties.Add( "SequenceFixedAddRow" );
				properties.Add( "SequenceSummaryRow" );

				properties.Add("ShowInkButton");
				// JAS v5.2 GroupBy Break Behavior 5/4/05
				//
				properties.Add("SortComparisonType");

				// SSP 5/3/05 - NAS 5.2 Fixed Row/Filter Row/Fixed Add-Row/Summaries Extention
				//
				properties.Add( "SummaryDisplayArea" );

				// SSP 1/4/04 - IDataErrorInfo Support
				//
				properties.Add("SupportDataErrorInfo");
				properties.Add("TipStyleCell");
				// SSP 3/3/06 BR10430
				// Added TipStyleHeader property.
				// 
				properties.Add("TipStyleHeader");
				properties.Add("TipStyleRowConnector");
				properties.Add("TipStyleScroll");

				// MRS 5/26/04 - Moved from Appearance
				properties.Add("NullText");
				// MRS 5/26/04 - Added RowSelectors back into 
				// Behavior. Note that it now appears under both look and
				// behavior. If there is a conflict, Behavior will take precedence
				// in the designer. 
				properties.Add("RowSelectors");

				//MRS 9/14/04
				properties.Add("ShowCalculatingText");

				// JAS v5.2 Wrapped Header Text 4/18/05
				//
				properties.Add("WrapHeaderText");

                // MRS 5/15/2009 - TFS17625
                properties.Add("AllowColSwapping");
			}
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (string[])properties.ToArray(typeof(string));
			return properties.ToArray();
		}

		/// <summary>
		/// Returns the TypeName of the Preset target
		/// </summary>
		/// <returns>Returns "UltraGridOverride"</returns>
		string ISupportPresets.GetPresetTargetTypeName()
		{
			return "UltraGridOverride";
		}
		#endregion Implementation of ISupportPresets

		// SSP 7/6/04 - UltraCalc
		// Added FormulaErrorAppearance.
		//
		#region FormulaErrorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells containing formula errors.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>FormulaErrorAppearance</b> property is used to specify the appearance of the cells with formula errors.</p> 
		///	<p><seealso cref="UltraGridColumn.FormulaErrorAppearance"/> <seealso cref="UltraGridColumn.Formula"/> <seealso cref="SummarySettings.Formula"/> <seealso cref="UltraGridBase.CalcManager"/></p>
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FormulaErrorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FormulaErrorAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FormulaErrorAppearance );
			}

			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FormulaErrorAppearance, value );
			}
		}

		#endregion // FormulaErrorAppearance

		#region HasFormulaErrorAppearance
		
		internal bool HasFormulaErrorAppearance
		{
			get
			{
				return this.HasAppearance( OverrideAppearanceIndex.FormulaErrorAppearance );
			}
		}

		#endregion // HasFormulaErrorAppearance

		#region ShouldSerializeFormulaErrorAppearance

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFormulaErrorAppearance( )
		{
			return this.ShouldSerializeAppearance( UltraGridOverride.OverrideAppearanceIndex.FormulaErrorAppearance );
		}

		#endregion // ShouldSerializeFormulaErrorAppearance

		#region ResetFormulaErrorAppearance

		/// <summary>
		/// Resets the <see cref="FormulaErrorAppearance"/> property to its default value.
		/// </summary>
		public void ResetFormulaErrorAppearance( )
		{
			this.ResetAppearance( UltraGridOverride.OverrideAppearanceIndex.FormulaErrorAppearance );
		}

		#endregion // ResetFormulaErrorAppearance

		// SSP 10/17/04
		// Added FormulaRowIndexSource property.
		//
		#region FormulaRowIndexSource

		/// <summary>
		/// Specifies which row index to use in formula calculations. Default is resolved to <b>VisibleIndex</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">FormulaRowIndexSource specifies which rows to use for calculations, all rows or just the visible rows. It also specifies which index to use in relative refereneces. <b>ListIndex</b> and <b>Index</b> specify that all rows should be used in calculations. <b>VisibleIndex</b> specifies that only the visible rows should be used for calculations. The summaries will be based on either all rows or just the visible ones depending on this property setting. Also if a column has a formula containing relative reference (like [column1(-1)] which references the column1 cell in the previous row), this property specifies whether to use list index, row index or the visible index for finding the relative cell. List index is the index of the data row in the underlying data list. It corrensponds to the <see cref="UltraGridRow.ListIndex"/> value of the rows. Index is the UltraGridRow's index in its parent collection. It corresponds to <see cref="UltraGridRow.Index"/> property of the rows. VisibleIndex, as the name suggests is the number of rows visible before the current row in the associated rows collection. Essentially it emulates IndexOf operation on a collection that contains only the visible rows (ones that are not filtered out or explicitly hidden). It corresponds to the <see cref="UltraGridRow.VisibleIndex"/> property.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridOverride_P_FormulaRowIndexSource")]
		[LocalizedCategory("LC_Behavior")]
		public FormulaRowIndexSource FormulaRowIndexSource
		{
			get
			{
				return this.formulaRowIndexSource;
			}
			set
			{
				if ( this.formulaRowIndexSource != value )
				{
					if ( ! Enum.IsDefined( typeof( FormulaRowIndexSource ), value ) )
						throw new InvalidEnumArgumentException( "FormulaRowIndexSource", (int)value, typeof( FormulaRowIndexSource ) );

					this.formulaRowIndexSource = value;
					this.NotifyPropChange( PropertyIds.FormulaRowIndexSource );
				}
			}
		}

		#endregion // FormulaRowIndexSource

		#region ShouldSerializeFormulaRowIndexSource

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFormulaRowIndexSource( )
		{
			return FormulaRowIndexSource.Default != this.formulaRowIndexSource;
		}

		#endregion // ShouldSerializeFormulaRowIndexSource

		#region ResetFormulaRowIndexSource

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFormulaRowIndexSource( )
		{
			this.FormulaRowIndexSource = FormulaRowIndexSource.Default;
		}

		#endregion // ResetFormulaRowIndexSource

		#region Merged Cell Feature

		// SSP 11/3/04 - Merged Cell Feature
		//
		#region MergedCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the merged cells.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.MergedCellAppearance"/> <seealso cref="UltraGridOverride.MergedCellContentArea"/> <seealso cref="UltraGridOverride.MergedCellStyle"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_MergedCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase MergedCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.MergedCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.MergedCellAppearance, value );
			}
		}

		#endregion // MergedCellAppearance

		#region ShouldSerializeMergedCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMergedCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.MergedCellAppearance ); 
		}

		#endregion // ShouldSerializeMergedCellAppearance
 
		#region ResetMergedCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMergedCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.MergedCellAppearance ); 
		}

		#endregion // ResetMergedCellAppearance

		#region MergedCellContentArea

		/// <summary>
		/// Specifies whether to position the contents of a merged cell in the entire area of the merged cell or just the visible area of the merged cell. Default is resolved to <b>VirtualRect</b>.
		/// </summary>
		/// <remarks>
		/// <b class="body">Specifies whether to position the contents of a merged cell in the entire area of the merged cell or just the visible area of the merged cell. Default is resolved to <b>VirtualRect</b>.</b>
		/// <p><seealso cref="UltraGridOverride.MergedCellStyle"/> <seealso cref="UltraGridOverride.MergedCellAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_MergedCellContentArea")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public MergedCellContentArea MergedCellContentArea
		{
			get
			{
				return this.mergedCellContentArea;
			}
			set
			{
				if ( this.mergedCellContentArea != value )
				{
					if ( ! Enum.IsDefined( typeof( MergedCellContentArea ), value ) )
						throw new InvalidEnumArgumentException( "MergedCellContentArea", (int)value, typeof( MergedCellContentArea ) );

					this.mergedCellContentArea = value;

					this.NotifyPropChange( PropertyIds.MergedCellContentArea );
				}
			}
		}

		#endregion // MergedCellContentArea

		#region ShouldSerializeMergedCellContentArea

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMergedCellContentArea( )
		{
			return MergedCellContentArea.Default != this.mergedCellContentArea;
		}

		#endregion // ShouldSerializeMergedCellContentArea

		#region ResetMergedCellContentArea

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMergedCellContentArea( )
		{
			this.MergedCellContentArea = MergedCellContentArea.Default;
		}

		#endregion // ResetMergedCellContentArea

		#region MergedCellStyle

		/// <summary>
		/// Specifies if and how cell merging is performed. Default is resolved to Never.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.MergedCellContentArea"/> <seealso cref="UltraGridOverride.MergedCellAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_MergedCellStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public MergedCellStyle MergedCellStyle
		{
			get
			{
				return this.mergedCellStyle;
			}
			set
			{
				if ( this.mergedCellStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( MergedCellStyle ), value ) )
						throw new InvalidEnumArgumentException( "MergedCellStyle", (int)value, typeof( MergedCellStyle ) );

					this.mergedCellStyle = value;

					this.NotifyPropChange( PropertyIds.MergedCellStyle );
				}
			}
		}

		#endregion // MergedCellStyle

		#region ShouldSerializeMergedCellStyle

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMergedCellStyle( )
		{
			return MergedCellStyle.Default != this.mergedCellStyle;
		}

		#endregion // ShouldSerializeMergedCellStyle

		#region ResetMergedCellStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMergedCellStyle( )
		{
			this.MergedCellStyle = MergedCellStyle.Default;
		}

		#endregion // ResetMergedCellStyle

		#endregion // Merged Cell Feature

		#region ColumnSizingArea

		// SSP 11/16/04
		// Implemented column sizing using cells in non-rowlayout mode.
		// Added ColumnSizingArea property.
		//
		/// <summary>
		/// Returns or sets a value that determines which part of the grid's interface may be used to resize columns.
		/// </summary>
		///	<remarks>
		///	<p class="body">If column resizing is enabled (as determined by the <see cref="AllowColSizing"/> property) the user can resize columns using the mouse. Resizing is always accomplished by clicking on the right edge of the column and dragging the mouse. The <b>ColumnSizingArea</b> property specifies which part of the column responds to the mouse pointer to initiate resizing of the column. You can choose to have just the column headers, just the borders of the column cells, or both be active for column resizing. When the mouse pointer passes over the active area of the column, the cursor changes to a resizing cursor.</p>
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_ColumnSizingArea")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.ColumnSizingArea ColumnSizingArea
		{
			get
			{
				return this.columnSizingArea;
			}
			set
			{
				if ( this.columnSizingArea != value )
				{
					if ( ! Enum.IsDefined( typeof( ColumnSizingArea ), value ) )
						throw new InvalidEnumArgumentException( "ColumnSizingArea", (int)value, typeof( ColumnSizingArea ) );

					this.columnSizingArea = value;

					this.NotifyPropChange( PropertyIds.ColumnSizingArea );
				}
			}
		}

		#endregion // ColumnSizingArea

		#region ShouldSerializeColumnSizingArea

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeColumnSizingArea( )
		{
			return ColumnSizingArea.Default != this.columnSizingArea;
		}

		#endregion // ShouldSerializeColumnSizingArea

		#region ResetColumnSizingArea

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetColumnSizingArea( )
		{
			this.ColumnSizingArea = ColumnSizingArea.Default;
		}

		#endregion // ResetColumnSizingArea

		#region ClearSelectionStrategies

		// SSP 11/2/04 UWG3736
		// When the data source is reset on the grid we need to ensure that the grid doesn't keep 
		// holding any references to the datasource and the list object. In order to do that
		// we need to release all the references to the rows/cells etc... The selection strategies
		// can hold a reference to the selectable item like a cell or a row. So we need to release
		// them as well. Added ClearSelectionStrategies method.
		//
		internal void ClearSelectionStrategies( )
		{
			this.selectionStrategyCell = null;
			this.selectionStrategyColumn = null;
			this.selectionStrategyGroupByRow = null;
			this.selectionStrategyRow = null;
		}

		#endregion // ClearSelectionStrategies

		#region IDataErrorInfo Support

		// SSP 12/14/04 - IDataErrorInfo Support
		//

		#region SupportDataErrorInfo

		/// <summary>
		/// Specifies whether to make use of IDataErrorInfo interface implemented on the underlying row objects to display error info in rows and cells.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies whether to make use of IDataErrorInfo interface implemented on the underlying row objects to display error info in rows and cells.</p>
		/// <p><seealso cref="UltraGridOverride.SupportDataErrorInfo"/> <seealso cref="UltraGridOverride.DataErrorRowAppearance"/> <seealso cref="UltraGridOverride.DataErrorCellAppearance"/> <seealso cref="UltraGridOverride.DataErrorRowSelectorAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_SupportDataErrorInfo")]
		[ LocalizedCategory("LC_Behavior") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.UltraWinGrid.SupportDataErrorInfo SupportDataErrorInfo
		{
			get
			{
				return this.supportDataErrorInfo;
			}
			set
			{
				if ( this.supportDataErrorInfo != value )
				{
					if ( ! Enum.IsDefined( typeof( SupportDataErrorInfo ), value ) )
						throw new InvalidEnumArgumentException( "SupportDataErrorInfo", (int)value, typeof( SupportDataErrorInfo ) );

					this.supportDataErrorInfo = value;

					this.NotifyPropChange( PropertyIds.SupportDataErrorInfo );
				}
			}
		}

		#endregion // SupportDataErrorInfo

		#region ShouldSerializeSupportDataErrorInfo

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSupportDataErrorInfo( )
		{
			return SupportDataErrorInfo.Default != this.supportDataErrorInfo;
		}

		#endregion // ShouldSerializeSupportDataErrorInfo

		#region ResetSupportDataErrorInfo

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		/// <returns></returns>
		public void ResetSupportDataErrorInfo( )
		{
			this.SupportDataErrorInfo = SupportDataErrorInfo.Default;
		}

		#endregion // ResetSupportDataErrorInfo

		#region DataErrorRowAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the rows with data errors. A row is considered to have a data error if it's underlying list object implements IDataErrorInfo interface and querying that IDataErrorInfo indicates that the row has errors.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>DataErrorRowAppearance</b> will only be applied if <see cref="SupportDataErrorInfo"/> is set to support data error info for rows.</p>
		/// <p><seealso cref="UltraGridOverride.SupportDataErrorInfo"/> <seealso cref="UltraGridOverride.DataErrorCellAppearance"/> <seealso cref="UltraGridOverride.DataErrorRowSelectorAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_DataErrorRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase DataErrorRowAppearance 
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.DataErrorRowAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.DataErrorRowAppearance, value );
			}
		}

		#endregion // DataErrorRowAppearance

		#region ShouldSerializeDataErrorRowAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDataErrorRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.DataErrorRowAppearance ); 
		}

		#endregion // ShouldSerializeDataErrorRowAppearance
 
		#region ResetDataErrorRowAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetDataErrorRowAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.DataErrorRowAppearance ); 
		}

		#endregion // ResetDataErrorRowAppearance

		#region DataErrorCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells with data errors. A cell is considered to have a data error if the associated row's underlying list object implements IDataErrorInfo interface and querying that IDataErrorInfo indicates that the cell has errors.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.SupportDataErrorInfo"/> <seealso cref="UltraGridOverride.DataErrorRowAppearance"/> <seealso cref="UltraGridOverride.DataErrorRowSelectorAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_DataErrorCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase DataErrorCellAppearance 
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.DataErrorCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.DataErrorCellAppearance, value );
			}
		}

		#endregion // DataErrorCellAppearance

		#region ShouldSerializeDataErrorCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDataErrorCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.DataErrorCellAppearance ); 
		}

		#endregion // ShouldSerializeDataErrorCellAppearance
 
		#region ResetDataErrorCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetDataErrorCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.DataErrorCellAppearance ); 
		}

		#endregion // ResetDataErrorCellAppearance

		#region DataErrorRowSelectorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the row selectors of rows with data errors. A row is considered to have a data error if it's underlying list object implements IDataErrorInfo interface and querying that IDataErrorInfo indicates that the row has errors.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="UltraGridOverride.SupportDataErrorInfo"/> <seealso cref="UltraGridOverride.DataErrorCellAppearance"/> <seealso cref="UltraGridOverride.DataErrorRowAppearance"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_DataErrorRowSelectorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase DataErrorRowSelectorAppearance 
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.DataErrorRowSelectorAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.DataErrorRowSelectorAppearance, value );
			}
		}

		#endregion // DataErrorRowSelectorAppearance

		#region ShouldSerializeDataErrorRowSelectorAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDataErrorRowSelectorAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.DataErrorRowSelectorAppearance ); 
		}

		#endregion // ShouldSerializeDataErrorRowSelectorAppearance
 
		#region ResetDataErrorRowSelectorAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetDataErrorRowSelectorAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.DataErrorRowSelectorAppearance ); 
		}

		#endregion // ResetDataErrorRowSelectorAppearance

		#endregion // IDataErrorInfo Support

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
		//
		#region Header/RowSelector Styles

			#region HeaderStyle Logic

				#region HeaderStyle

		/// <summary>
		/// Gets/sets the visual style of the column headers.  Setting this property will also determine the 
		/// visual style of the row selectors.  The visual style of the row selectors can be specified with the
		/// <see cref="RowSelectorStyle"/> property.
		/// </summary>
		[LocalizedDescription("LD_UltraGridOverride_P_HeaderStyle")]
		[LocalizedCategory("LC_Behavior")]
		public HeaderStyle HeaderStyle
		{
			get	{ return this.headerStyle; }
			set
			{
				if( this.headerStyle != value )
				{
					GridUtils.ValidateEnum( typeof(HeaderStyle), value );
				
					this.headerStyle = value;

					this.NotifyPropChange( PropertyIds.HeaderStyle );
				}
			}
		}

				#endregion // HeaderStyle

				#region ShouldSerializeHeaderStyle

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHeaderStyle() 
		{
			return this.headerStyle != HeaderStyle.Default;
		}

				#endregion // ShouldSerializeHeaderStyle
 
				#region ResetHeaderStyle

		/// <summary>
		/// Resets <see cref="HeaderStyle"/> to its default value.
		/// </summary>
		public void ResetHeaderStyle() 
		{
			this.HeaderStyle = HeaderStyle.Default;
		}		

				#endregion // ResetHeaderStyle

			#endregion // HeaderStyle Logic

			#region RowSelectorStyle Logic

				#region RowSelectorStyle

		/// <summary>
		/// Gets/sets the visual style of the row selectors.
		/// </summary>
		[LocalizedDescription("LD_UltraGridOverride_P_RowSelectorStyle")]
		[LocalizedCategory("LC_Behavior")]
		public HeaderStyle RowSelectorStyle
		{
			get	{ return this.rowSelectorStyle; }
			set
			{
				if( this.rowSelectorStyle != value )
				{
					GridUtils.ValidateEnum( typeof(HeaderStyle), value );
				
					this.rowSelectorStyle = value;

					this.NotifyPropChange( PropertyIds.RowSelectorStyle );
				}
			}
		}

				#endregion // RowSelectorStyle

				#region ShouldSerializeRowSelectorStyle

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowSelectorStyle() 
		{
			return this.rowSelectorStyle != HeaderStyle.Default;
		}

				#endregion // ShouldSerializeRowSelectorStyle
 
				#region ResetRowSelectorStyle

		/// <summary>
		/// Resets <see cref="RowSelectorStyle"/> to its default value.
		/// </summary>
		public void ResetRowSelectorStyle() 
		{
			this.RowSelectorStyle = HeaderStyle.Default;
		}		

				#endregion // ResetRowSelectorStyle

			#endregion // RowSelectorStyle Logic

		#endregion // Header/RowSelector Styles

		// JAS 2005 v2 GroupBy Row Extensions
		//
		#region GroupBy Row Extensions

			#region GroupByRowExpansionStyle Logic

				#region GroupByRowExpansionStyle

		/// <summary>
		/// Gets/sets the value which determines how GroupBy rows can be expanded/collapsed by the end-user.
		/// Setting this property to 'DoubleClick' or 'Disabled' will prevent expansion indicators from appearing
		/// in the GroupBy rows.  Note, The GroupBy rows can be expanded/collapsed via the Left and Right arrow keys
		/// as well as Enter for all settings except 'Disabled'.
		/// </summary>
		[LocalizedDescription("LD_UltraGridOverride_P_GroupByRowExpansionStyle")]
		[LocalizedCategory("LC_Behavior")]
		public GroupByRowExpansionStyle GroupByRowExpansionStyle
		{
			get	{ return this.groupByRowExpansionStyle; }
			set
			{
				if( this.groupByRowExpansionStyle != value )
				{
					GridUtils.ValidateEnum( typeof(GroupByRowExpansionStyle), value );
				
					this.groupByRowExpansionStyle = value;

					this.NotifyPropChange( PropertyIds.GroupByRowExpansionStyle );
				}
			}
		}

				#endregion // GroupByRowExpansionStyle

				#region ShouldSerializeGroupByRowExpansionStyle

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeGroupByRowExpansionStyle() 
		{
			return this.groupByRowExpansionStyle != GroupByRowExpansionStyle.Default;
		}

				#endregion // ShouldSerializeGroupByRowExpansionStyle
 
				#region ResetGroupByRowExpansionStyle

		/// <summary>
		/// Resets <see cref="GroupByRowExpansionStyle"/> to its default value.
		/// </summary>
		public void ResetGroupByRowExpansionStyle() 
		{
			this.GroupByRowExpansionStyle = GroupByRowExpansionStyle.Default;
		}		

				#endregion // ResetGroupByRowExpansionStyle

			#endregion // GroupByRowExpansionStyle Logic

			#region GroupByRowInitialExpansionState Logic

				#region GroupByRowInitialExpansionState

		/// <summary>
		/// Gets/sets the value which determines if the GroupBy rows will all be collapsed or expanded when a band groups on a column. 
		/// </summary>
		[LocalizedDescription("LD_UltraGridOverride_P_GroupByRowInitialExpansionState")]
		[LocalizedCategory("LC_Behavior")]
		public GroupByRowInitialExpansionState GroupByRowInitialExpansionState
		{
			get { return this.groupByRowInitialExpansionState; }
			set
			{
				if( this.groupByRowInitialExpansionState != value )
				{
					GridUtils.ValidateEnum( typeof(GroupByRowInitialExpansionState), value );

					this.groupByRowInitialExpansionState = value;

					this.NotifyPropChange( PropertyIds.GroupByRowInitialExpansionState );
				}
			}
		}

				#endregion // GroupByRowInitialExpansionState

				#region ShouldSerializeGroupByRowInitialExpansionState

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeGroupByRowInitialExpansionState() 
		{
			return this.groupByRowInitialExpansionState != GroupByRowInitialExpansionState.Default;
		}

				#endregion // ShouldSerializeGroupByRowInitialExpansionState
 
				#region ResetGroupByRowInitialExpansionState

		/// <summary>
		/// Resets <see cref="GroupByRowInitialExpansionState"/> to its default value.
		/// </summary>
		public void ResetGroupByRowInitialExpansionState() 
		{
			this.GroupByRowInitialExpansionState = GroupByRowInitialExpansionState.Default;
		}		

				#endregion // ResetGroupByRowInitialExpansionState

			#endregion // GroupByRowInitialExpansionState Logic

		#endregion // GroupBy Row Extensions

		#region Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region BorderStyleFilterCell

		/// <summary>
		/// Specifies the border style of the filter cells.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of cells in filter row. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	<seealso cref="UltraGridOverride.BorderStyleFilterOperator"/> <seealso cref="UltraGridOverride.BorderStyleFilterRow"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleFilterCell")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleFilterCell
		{
			get
			{
				return this.borderStyleFilterCell;
			}
			set
			{
				if ( value != this.borderStyleFilterCell )
				{
					GridUtils.ValidateEnum( "BorderStyleFilterCell", typeof( UIElementBorderStyle ), value );
					this.borderStyleFilterCell = value;
					this.NotifyPropChange( PropertyIds.BorderStyleFilterCell, null );
				}
			}
		}

		#endregion // BorderStyleFilterCell

		#region ShouldSerializeBorderStyleFilterCell

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleFilterCell( )
		{
			return UIElementBorderStyle.Default != this.borderStyleFilterCell;
		}

		#endregion // ShouldSerializeBorderStyleFilterCell

		#region ResetBorderStyleFilterCell

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetBorderStyleFilterCell( )
		{
			this.BorderStyleFilterCell = UIElementBorderStyle.Default;
		}

		#endregion // ResetBorderStyleFilterCell

		#region BorderStyleFilterOperator

		/// <summary>
		/// Specifies the border style of the operator ui elements in filter row.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of operator ui elements in filter row. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	<seealso cref="UltraGridOverride.BorderStyleFilterCell"/> <seealso cref="UltraGridOverride.BorderStyleFilterRow"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleFilterOperator")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleFilterOperator
		{
			get
			{
				return this.borderStyleFilterOperator;
			}
			set
			{
				if ( value != this.borderStyleFilterOperator )
				{
					GridUtils.ValidateEnum( "BorderStyleFilterOperator", typeof( UIElementBorderStyle ), value );
					this.borderStyleFilterOperator = value;
					this.NotifyPropChange( PropertyIds.BorderStyleFilterOperator, null );
				}
			}
		}

		#endregion // BorderStyleFilterOperator

		#region ShouldSerializeBorderStyleFilterOperator

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleFilterOperator( )
		{
			return UIElementBorderStyle.Default != this.borderStyleFilterOperator;
		}

		#endregion // ShouldSerializeBorderStyleFilterOperatorCell

		#region ResetBorderStyleFilterOperator

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetBorderStyleFilterOperator( )
		{
			this.BorderStyleFilterOperator = UIElementBorderStyle.Default;
		}

		#endregion // ResetBorderStyleFilterOperator

		#region BorderStyleFilterRow

		/// <summary>
		/// Specifies the border style of the filter rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of filter rows. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	<seealso cref="UltraGridOverride.BorderStyleFilterCell"/> <seealso cref="UltraGridOverride.BorderStyleFilterOperator"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleFilterRow")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleFilterRow
		{
			get
			{
				return this.borderStyleFilterRow;
			}
			set
			{
				if ( value != this.borderStyleFilterRow )
				{
					GridUtils.ValidateEnum( "BorderStyleFilterRow", typeof( UIElementBorderStyle ), value );
					this.borderStyleFilterRow = value;
					this.NotifyPropChange( PropertyIds.BorderStyleFilterRow, null );
				}
			}
		}

		#endregion // BorderStyleFilterRow

		#region ShouldSerializeBorderStyleFilterRow

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleFilterRow( )
		{
			return UIElementBorderStyle.Default != this.borderStyleFilterRow;
		}

		#endregion // ShouldSerializeBorderStyleFilterRow

		#region ResetBorderStyleFilterRow

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetBorderStyleFilterRow( )
		{
			this.BorderStyleFilterRow = UIElementBorderStyle.Default;
		}

		#endregion // ResetBorderStyleFilterRow

		#region BorderStyleSpecialRowSeparator

		/// <summary>
		/// Specifies the border style of the filter rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property is used to set the border appearance of filter rows. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, borders formatted with that style will be drawn using solid lines.</p>
		///	<seealso cref="UltraGridOverride.SpecialRowSeparatorHeight"/> <seealso cref="UltraGridOverride.SpecialRowSeparator"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_BorderStyleSpecialRowSeparator")]
		[ LocalizedCategory("LC_Appearance") ]
		public UIElementBorderStyle BorderStyleSpecialRowSeparator
		{
			get
			{
				return this.borderStyleSpecialRowSeparator;
			}
			set
			{
				if ( value != this.borderStyleSpecialRowSeparator )
				{
					GridUtils.ValidateEnum( "BorderStyleSpecialRowSeparator", typeof( UIElementBorderStyle ), value );
					this.borderStyleSpecialRowSeparator = value;
					this.NotifyPropChange( PropertyIds.BorderStyleSpecialRowSeparator, null );
				}
			}
		}

		#endregion // BorderStyleSpecialRowSeparator

		#region ShouldSerializeBorderStyleSpecialRowSeparator

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeBorderStyleSpecialRowSeparator( )
		{
			return UIElementBorderStyle.Default != this.borderStyleSpecialRowSeparator;
		}

		#endregion // ShouldSerializeBorderStyleSpecialRowSeparator

		#region ResetBorderStyleSpecialRowSeparator

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetBorderStyleSpecialRowSeparator( )
		{
			this.BorderStyleSpecialRowSeparator = UIElementBorderStyle.Default;
		}

		#endregion // ResetBorderStyleSpecialRowSeparator

		#region FilterClearButtonLocation

		/// <summary>
		/// Specifies if and where clear filter buttons are displayed in the filter rows. Options are to display in the filter row selecotor, filter cells or both. A filter clear button is used to clear the filter in the cell or the entire row. Default is resolved to <b>RowAndCell</b>.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies if and where clear filter buttons are displayed in the filter rows. Options are to display in the filter row selecotor, filter cells or both. A filter clear button is used to clear the filter in the cell or the entire row.</p>
		///	<p class="body">You can control the visibility of the filter clear button on an individual column by setting the <see cref="UltraGridColumn.FilterClearButtonVisible"/> property.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterClearButtonLocation"/> <seealso cref="UltraGridColumn.FilterClearButtonVisible"/> <seealso cref="UltraGridOverride.FilterClearButtonAppearance"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterClearButtonLocation")]
		[ LocalizedCategory("LC_Behavior") ]
		public FilterClearButtonLocation FilterClearButtonLocation
		{
			get
			{
				return this.filterClearButtonLocation;
			}
			set
			{
				if ( value != this.filterClearButtonLocation )
				{
					GridUtils.ValidateEnum( "FilterClearButtonLocation", typeof( FilterClearButtonLocation ), value );
					this.filterClearButtonLocation = value;
					this.NotifyPropChange( PropertyIds.FilterClearButtonLocation, null );
				}
			}
		}

		#endregion // FilterClearButtonLocation

		#region ShouldSerializeFilterClearButtonLocation

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterClearButtonLocation( )
		{
			return FilterClearButtonLocation.Default != this.filterClearButtonLocation;
		}

		#endregion // ShouldSerializeFilterClearButtonLocation

		#region ResetFilterClearButtonLocation

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterClearButtonLocation( )
		{
			this.FilterClearButtonLocation = FilterClearButtonLocation.Default;
		}

		#endregion // ResetFilterClearButtonLocation

		#region FilterComparisonType

		/// <summary>
		/// Specifies whether the filtering is performed case-sensitive. Default is to perform filtering case-insensitive.
		/// </summary>
		///	<remarks>
		///	<p class="body">By default the UltraGrid performs filtering using case-sensitive string comparison. You can set the <b>FilterComparisonType</b> property to <b>CaseSensitive</b> to perform filtering using case sensitve string comparisons.</p>
		///	<seealso cref="UltraGridColumn.FilterComparisonType"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterComparisonType") ]
		[ LocalizedCategory("LC_Behavior") ]
		public FilterComparisonType FilterComparisonType
		{
			get
			{
				return this.filterComparisonType;
			}
			set
			{
				if ( value != this.filterComparisonType )
				{
					GridUtils.ValidateEnum( "FilterComparisonType", typeof( FilterComparisonType ), value );
					this.filterComparisonType = value;
					this.NotifyPropChange( PropertyIds.FilterComparisonType, null );
				}
			}
		}

		#endregion // FilterComparisonType

		#region ShouldSerializeFilterComparisonType

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterComparisonType( )
		{
			return FilterComparisonType.Default != this.filterComparisonType;
		}

		#endregion // ShouldSerializeFilterComparisonType

		#region ResetFilterComparisonType

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterComparisonType( )
		{
			this.FilterComparisonType = FilterComparisonType.Default;
		}

		#endregion // ResetFilterComparisonType

		#region FilterEvaluationTrigger

		/// <summary>
		/// Specifies when the filter input into filter row cells is applied.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can set this property to change the default behavior of when the UltraGrid applies the filter that the user types into the cells of a filter row. Default behavior is to apply as soon as the user changes the value of a filter cell.</p>
		///	<seealso cref="UltraGridColumn.FilterEvaluationTrigger"/> <seealso cref="Infragistics.Win.UltraWinGrid.FilterEvaluationTrigger"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterEvaluationTrigger")]
		[ LocalizedCategory("LC_Appearance") ]
		public FilterEvaluationTrigger FilterEvaluationTrigger
		{
			get
			{
				return this.filterEvaluationTrigger;
			}
			set
			{
				if ( value != this.filterEvaluationTrigger )
				{
					GridUtils.ValidateEnum( "FilterEvaluationTrigger", typeof( FilterEvaluationTrigger ), value );
					this.filterEvaluationTrigger = value;
					this.NotifyPropChange( PropertyIds.FilterEvaluationTrigger, null );
				}
			}
		}

		#endregion // FilterEvaluationTrigger

		#region ShouldSerializeFilterEvaluationTrigger

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterEvaluationTrigger( )
		{
			return FilterEvaluationTrigger.Default != this.filterEvaluationTrigger;
		}

		#endregion // ShouldSerializeFilterEvaluationTrigger

		#region ResetFilterEvaluationTrigger

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterEvaluationTrigger( )
		{
			this.FilterEvaluationTrigger = FilterEvaluationTrigger.Default;
		}

		#endregion // ResetFilterEvaluationTrigger

		#region FilterOperandStyle

		/// <summary>
		/// Specifies the style of operand input in the filter row cells. Default is be resolved to <b>UseColumnEditor</b> for DateTime and Boolean columns and <b>Combo</b> for other types of columns. This can be overridden on each column using the <see cref="UltraGridColumn.FilterOperandStyle"/> property.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies the style of operand input in the filter row cells. Default will be resolved to Combo. This can be overridden on each column using the <see cref="UltraGridColumn.FilterOperandStyle"/> property.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterOperandStyle")]
		[ LocalizedCategory("LC_Appearance") ]
		public FilterOperandStyle FilterOperandStyle
		{
			get
			{
				return this.filterOperandStyle;
			}
			set
			{
				if ( value != this.filterOperandStyle )
				{
					GridUtils.ValidateEnum( "FilterOperandStyle", typeof( FilterOperandStyle ), value );
					this.filterOperandStyle = value;
					this.NotifyPropChange( PropertyIds.FilterOperandStyle, null );
				}
			}
		}

		#endregion // FilterOperandStyle

		#region ShouldSerializeFilterOperandStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterOperandStyle( )
		{
			return FilterOperandStyle.Default != this.filterOperandStyle;
		}

		#endregion // ShouldSerializeFilterOperandStyle

		#region ResetFilterOperandStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperandStyle( )
		{
			this.FilterOperandStyle = FilterOperandStyle.Default;
		}

		#endregion // ResetFilterOperandStyle

		#region FilterOperatorDefaultValue

		/// <summary>
        /// Specifies the default (initial) value of the operator cells in the filter row. If operator cells are hidden, this is used as the filter operator for values entered in the associated filter operand cells. This can be overridden on each column. Default is resolved to <b>Equals</b> for DateTime and Boolean columns and <b>StartsWith</b> for other column types.
		/// </summary>
		///	<remarks>
		///	<p class="body">Filter rows by default display user interface for selecting filter operators in addition to the filter operands. <see cref="UltraGridOverride.FilterOperatorLocation"/> property can be used to hide or disable this user interface so the user can't change the filter operators. You can specify the default filter operator to use using the <b>FilterOperatorDefaultValue</b> property. If the filter operator ui is enable then it will be initialized to the value of this property.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterOperatorDefaultValue"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/> <seealso cref="UltraGridOverride.FilterOperatorDropDownItems"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterOperatorDefaultValue")]
		[ LocalizedCategory("LC_Appearance") ]
		[ Editor( typeof( Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		public FilterOperatorDefaultValue FilterOperatorDefaultValue
		{
			get
			{
				return this.filterOperatorDefaultValue;
			}
			set
			{
				if ( value != this.filterOperatorDefaultValue )
				{
					GridUtils.ValidateEnum( "FilterOperatorDefaultValue", typeof( FilterOperatorDefaultValue ), value );
					this.filterOperatorDefaultValue = value;
					this.NotifyPropChange( PropertyIds.FilterOperatorDefaultValue, null );
				}
			}
		}

		#endregion // FilterOperatorDefaultValue

		#region ShouldSerializeFilterOperatorDefaultValue

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterOperatorDefaultValue( )
		{
			return FilterOperatorDefaultValue.Default != this.filterOperatorDefaultValue;
		}

		#endregion // ShouldSerializeFilterOperatorDefaultValue

		#region ResetFilterOperatorDefaultValue

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorDefaultValue( )
		{
			this.FilterOperatorDefaultValue = FilterOperatorDefaultValue.Default;
		}

		#endregion // ResetFilterOperatorDefaultValue

		#region FilterOperatorLocation

		/// <summary>
		/// Specifies the style of operator input in the filter row cells. This can be overridden on each column using the <see cref="UltraGridColumn.FilterOperatorLocation"/> property. <b>Default</b> is resolved to <b>WithOperand</b>.
		/// </summary>
		///	<remarks>
		///	<p class="body">Filter rows by default display user interface for selecting filter operators in addition to the filter operands. <b>FilterOperatorLocation</b> property can be used to hide this user interface so the user can't change the filter operator. You can specify what items are available in the filter operators drop down list using the <see cref="UltraGridOverride.FilterOperatorDropDownItems"/> property. You can specify the default filter operator to use using the <see cref="UltraGridOverride.FilterOperatorDefaultValue"/> property. If the filter operator ui is enabled then it's initialized to the value of the <b>FilterOperatorDefaultValue</b> property. These properties are also available on the <see cref="UltraGridColumn"/> object so these settings can be applied on a per column basis.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterOperatorLocation"/> <seealso cref="UltraGridColumn.FilterOperatorLocation"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterOperatorDefaultValue"/> <seealso cref="UltraGridColumn.FilterOperatorDefaultValue"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterOperatorLocation")]
		[ LocalizedCategory("LC_Appearance") ]
		public FilterOperatorLocation FilterOperatorLocation
		{
			get
			{
				return this.filterOperatorLocation;
			}
			set
			{
				if ( value != this.filterOperatorLocation )
				{
					GridUtils.ValidateEnum( "FilterOperatorLocation", typeof( FilterOperatorLocation ), value );
					this.filterOperatorLocation = value;
					this.NotifyPropChange( PropertyIds.FilterOperatorLocation, null );
				}
			}
		}

		#endregion // FilterOperatorLocation

		#region ShouldSerializeFilterOperatorLocation

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterOperatorLocation( )
		{
			return FilterOperatorLocation.Default != this.filterOperatorLocation;
		}

		#endregion // ShouldSerializeFilterOperatorLocation

		#region ResetFilterOperatorLocation

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorLocation( )
		{
			this.FilterOperatorLocation = FilterOperatorLocation.Default;
		}

		#endregion // ResetFilterOperatorLocation

		#region FilterOperatorDropDownItems

		/// <summary>
		/// Specifies which operators to list in the operator drop down in filter rows. Default is resolved to a value that is based on the column's data type.
		/// </summary>
		///	<remarks>
		///	<p class="body">Filter rows by default display user interface for selecting filter operators in addition to the filter operands. <see cref="UltraGridOverride.FilterOperatorLocation"/> property can be used to hide or disable this user interface so the user can't change the filter operator. You can specify the default filter operator to use using the <see cref="UltraGridOverride.FilterOperatorDefaultValue"/> property. Default is resolved to a value that is based on the column's data type.</p>
		///	<p class="body"><b>FilterOperatorDropDownItems</b> property can be used to specify which operators to include in the operator drop down list.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FilterOperatorDropDownItems"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridOverride.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/> <seealso cref="UltraGridOverride.FilterOperatorDefaultValue"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterOperatorDropDownItems")]
		[ LocalizedCategory("LC_Appearance") ]
		[ Editor( typeof( Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		public FilterOperatorDropDownItems FilterOperatorDropDownItems
		{
			get
			{
				return this.filterOperatorDropDownItems;
			}
			set
			{
				if ( value != this.filterOperatorDropDownItems )
				{
					GridUtils.ValidateEnum( "FilterOperatorDropDownItems", typeof( FilterOperatorDropDownItems ), value );
					this.filterOperatorDropDownItems = value;
					this.NotifyPropChange( PropertyIds.FilterOperatorDropDownItems, null );
				}
			}
		}

		#endregion // FilterOperatorDropDownItems

		#region ShouldSerializeFilterOperatorDropDownItems

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterOperatorDropDownItems( )
		{
			return FilterOperatorDropDownItems.Default != this.filterOperatorDropDownItems;
		}

		#endregion // ShouldSerializeFilterOperatorDropDownItems

		#region ResetFilterOperatorDropDownItems

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorDropDownItems( )
		{
			this.FilterOperatorDropDownItems = FilterOperatorDropDownItems.Default;
		}

		#endregion // ResetFilterOperatorDropDownItems

		#region FilterRowPrompt

		/// <summary>
		/// Specifies the prompt text to display in the filter row. By default no prompt is displayed. Once the filter row is activated the prompt disappears (and reappears when it�s deactivated). The text spans multiple cells unless <see cref="UltraGridBand.SpecialRowPromptField"/> has been set to a valid column key in which case the prompt is displayed in the associated cell.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies the prompt text to display in the filter row. Once the filter row is activated the prompt disappears (and reappears when it�s deactivated). The text will span multiple cells.</p>
		/// <seealso cref="UltraGridOverride.FilterRowPromptAppearance"/> <seealso cref="UltraGridOverride.FilterUIType"/> <seealso cref="UltraGridBand.SpecialRowPromptField"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterRowPrompt")]
		[ LocalizedCategory("LC_Display") ]
		[ Localizable( true ) ]
		public string FilterRowPrompt
		{
			get
			{
				return this.filterRowPrompt;
			}
			set
			{
				if ( value != this.filterRowPrompt )
				{
					this.filterRowPrompt = value;
					this.NotifyPropChange( PropertyIds.FilterRowPrompt, null );
				}
			}
		}

		#endregion // FilterRowPrompt

		#region ShouldSerializeFilterRowPrompt

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterRowPrompt( )
		{
			return null != this.filterRowPrompt && 0 != this.filterRowPrompt.Length;
		}

		#endregion // ShouldSerializeFilterRowPrompt

		#region ResetFilterRowPrompt

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterRowPrompt( )
		{
			this.FilterRowPrompt = null;
		}

		#endregion // ResetFilterRowPrompt

		#region FilterRowSpacingAfter

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered after a filter row (between the bottom of the filter row and the top edge of the following object.)
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>FilterRowSpacingAfter</b> property to specify the space that follows a filter row in the band or the grid controlled by the specified override. The space allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that precedes a filter row, use the <see cref="FilterRowSpacingBefore"/> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterRowSpacingAfter")]
		[ LocalizedCategory("LC_Display") ]
		public int FilterRowSpacingAfter
		{
			get
			{
				return this.filterRowSpacingAfter;
			}

			set
			{
				if ( value != this.filterRowSpacingAfter )
				{
					// if negative then set to -1 (default)
					//
					this.filterRowSpacingAfter = Math.Max( -1, value );
    		    	
					this.NotifyPropChange( PropertyIds.FilterRowSpacingAfter, null );
				}
			}
		}

		#endregion // FilterRowSpacingAfter

		#region ShouldSerializeFilterRowSpacingAfter

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterRowSpacingAfter( ) 
		{
			return ( this.filterRowSpacingAfter >= 0 );
		}
 
		#endregion // ShouldSerializeFilterRowSpacingAfter

		#region ResetFilterRowSpacingAfter

		/// <summary>
		/// Resets FilterRowSpacingAfter to its default value (-1).
		/// </summary>
		public void ResetFilterRowSpacingAfter( ) 
		{
			this.FilterRowSpacingAfter = -1;
		}

		#endregion // ResetFilterRowSpacingAfter

		#region FilterRowSpacingBefore

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered after a filter row (between the bottom of the filter row and the top edge of the following object.)
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>FilterRowSpacingBefore</b> property to specify the space that follows a filter row in the band or the grid controlled by the specified override. The space allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that precedes a filter row, use the <see cref="FilterRowSpacingBefore"/> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterRowSpacingBefore")]
		[ LocalizedCategory("LC_Display") ]
		public int FilterRowSpacingBefore
		{
			get
			{
				return this.filterRowSpacingBefore;
			}

			set
			{
				if ( value != this.filterRowSpacingBefore )
				{
					// if negative then set to -1 (default)
					//
					this.filterRowSpacingBefore = Math.Max( -1, value );
    		    	
					this.NotifyPropChange( PropertyIds.FilterRowSpacingBefore, null );
				}
			}
		}

		#endregion // FilterRowSpacingBefore

		#region ShouldSerializeFilterRowSpacingBefore

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterRowSpacingBefore( ) 
		{
			return this.filterRowSpacingBefore >= 0;
		}
 
		#endregion // ShouldSerializeFilterRowSpacingBefore

		#region ResetFilterRowSpacingBefore

		/// <summary>
		/// Resets FilterRowSpacingBefore to its default value (-1).
		/// </summary>
		public void ResetFilterRowSpacingBefore( ) 
		{
			this.FilterRowSpacingBefore = -1;
		}

		#endregion // ResetFilterRowSpacingBefore

		#region FilterUIType

		/// <summary>
		/// Specifies the type of user interface for filtering rows. Options are to display filter icons in headers or to display a filter row. <see cref="UltraGridOverride.AllowRowFiltering"/> property must be set to <b>True</b> in order for this property to have any effect.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies the type of user interface for filtering rows. Options are to display filter icons in headers or to display a filter row. <see cref="UltraGridOverride.AllowRowFiltering"/> property must be set to <b>True</b> in order for this property to have any effect.</p>
		///	<seealso cref="UltraGridBand.ColumnFilters"/> <seealso cref="UltraGridOverride.FilterOperandStyle"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/>  <see cref="UltraGridOverride.AllowRowFiltering"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FilterUIType")]
		[ LocalizedCategory("LC_Appearance") ]
		public FilterUIType FilterUIType
		{
			get
			{
				return this.filterUIType;
			}
			set
			{
				if ( value != this.filterUIType )
				{
					GridUtils.ValidateEnum( "FilterUIType", typeof( FilterUIType ), value );
					this.filterUIType = value;
					this.NotifyPropChange( PropertyIds.FilterUIType, null );
				}
			}
		}

		#endregion // FilterUIType

		#region ShouldSerializeFilterUIType

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFilterUIType( )
		{
			return FilterUIType.Default != this.filterUIType;
		}

		#endregion // ShouldSerializeFilterUIType

		#region ResetFilterUIType

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterUIType( )
		{
			this.FilterUIType = FilterUIType.Default;
		}

		#endregion // ResetFilterUIType

		#region FixedRowIndicator

		/// <summary>
		/// Specifies if and how the user is allowed to fix rows. Setting this to <b>Button</b> shows state buttons in the row selectors for fixing and unfixing rows. Also the default row selector width is increased to accommodate the buttons. Setting this to None shows no indicator. <b>Default</b> is resolved to <b>None</b>.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies if and how the user is allowed to fix rows. Setting this to <b>Button</b> shows state buttons in the row selectors for fixing and unfixing rows. Also the default row selector width is increased to accommodate the buttons. Setting this to None shows no indicator. Default is resolved to None.</p>
		///	<p class="body"><b>Note</b> that fixed rows are not supported in child row collections. Setting this 
		/// property on a child row will not have any effect. Also note that fixed rows are not supported in card-view.</p>
		/// <p class="body">Also note that when a fixed row is expanded, any sibling fixed rows occuring
		/// after it will not be fixed. They will remain non-fixed until the expanded fixed row is collapsed. 
		/// They will however remain in the FixedRows collection and keep their fixed row appearances.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FixedRowIndicator"/> <seealso cref="UltraGridOverride.FixedRowStyle"/> <seealso cref="UltraGridOverride.FixedRowAppearance"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FixedRowIndicator")]
		[ LocalizedCategory("LC_Appearance") ]
		public FixedRowIndicator FixedRowIndicator
		{
			get
			{
				return this.fixedRowIndicator;
			}
			set
			{
				if ( value != this.fixedRowIndicator )
				{
					GridUtils.ValidateEnum( "FixedRowIndicator", typeof( FixedRowIndicator ), value );
					this.fixedRowIndicator = value;
					this.NotifyPropChange( PropertyIds.FixedRowIndicator, null );
				}
			}
		}

		#endregion // FixedRowIndicator

		#region ShouldSerializeFixedRowIndicator

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedRowIndicator( )
		{
			return FixedRowIndicator.Default != this.fixedRowIndicator;
		}

		#endregion // ShouldSerializeFixedRowIndicator

		#region ResetFixedRowIndicator

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedRowIndicator( )
		{
			this.FixedRowIndicator = FixedRowIndicator.Default;
		}

		#endregion // ResetFixedRowIndicator

		#region FixedRowsLimit

		/// <summary>
		/// Specifies the limit on how many rows of a row collection can be fixed at a time. The default value is -1 which is resolved 0. 0 means there is no limit.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can limit the number of rows that can be fixed at a time in a row collection using the <b>FixedRowsLimit</b> property. Default value of FixedRowsLimit property is -1 which is resolved to 0. 0 specifies that there is no limit to the number of rows that can be fixed. When it�s set to a number greater than 0, the number of fixed rows is restricted to that number. Note that the limit applies to individual row collections. Importantly the a RowsCollection's FixedRowsCollection never contains more than this many rows. Adding more rows to it removes the earliest added rows, ensuring that it contains no more than the limit. Also if the FixedRowsLimit setting is changed, all the affected FixedRowsCollection instances adjust themselves to ensure they contain no more than the limit by removing the earliest added rows. The process of fixing rows via the user interface works sligltly differently. When the row collection has reached it�s fixed rows limit the user interface for fixing rows is disabled. The row selectors stop displaying the fixed row indicator buttons of the non-fixed rows and thus the user can not fix any more rows.</p>
		/// <p class="body">You can enable or disable user interface for fixing and unfixing rows by setting the <see cref="UltraGridOverride.FixedRowIndicator"/> property.</p>
		/// <seealso cref="UltraGridOverride.FixedRowIndicator"/> <seealso cref="UltraGridRow.AllowFixing"/> <seealso cref="UltraGridRow.Fixed"/> <seealso cref="RowsCollection.FixedRows"/> <seealso cref="UltraGridOverride.FixedRowStyle"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FixedRowsLimit")]
		[ LocalizedCategory("LC_Behavior") ]
		public int FixedRowsLimit
		{
			get
			{
				return this.fixedRowsLimit;
			}
			set
			{
				if ( this.fixedRowsLimit != value )
				{
					this.fixedRowsLimit = Math.Max( -1, value );
					this.NotifyPropChange( PropertyIds.FixedRowsLimit );
				}
			}
		}

		#endregion // FixedRowsLimit

		#region ShouldSerializeFixedRowsLimit

		/// <summary>
		/// Returns true if the FixedRowsLimit property is set to a non-negative value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedRowsLimit( )
		{
			return this.fixedRowsLimit >= 0;
		}

		#endregion // ShouldSerializeFixedRowsLimit

		#region ResetFixedRowsLimit

		/// <summary>
		/// Resets the FixedRowsLimit property to its default value of -1.
		/// </summary>
		public void ResetFixedRowsLimit( )
		{
			this.FixedRowsLimit = -1;
		}

		#endregion // ResetFixedRowsLimit

		#region FixedRowSortOrder

		/// <summary>
		/// Specifies whether the UltraGrid sorts the fixed rows. If set to Sorted then the fixed rows will be sorted according to the same sort criteria as non-fixed rows. If set to FixOrder then the fixed rows remain in the order they are added to the fixed rows collection. The RefreshSort method of the FixedRowsCollection can be called to explicitly sort the fixed rows this mode. Default is resolved to Sorted.
		/// </summary>
		///	<remarks>
		///	<p class="body"><b>FixedRowSortOrder</b> specifies whether the UltraGrid sorts the fixed rows. If set to Sorted then the fixed rows will be sorted according to the same sort criteria as non-fixed rows. If set to FixOrder then the fixed rows remain in the order they are added to the fixed rows collection. The RefreshSort method of the FixedRowsCollection can be called to explicitly sort the fixed rows this mode. Default is resolved to Sorted.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FixedRowSortOrder"/> <seealso cref="UltraGridOverride.FixedRowIndicator"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FixedRowSortOrder")]
		[ LocalizedCategory("LC_Behavior") ]
		public FixedRowSortOrder FixedRowSortOrder
		{
			get
			{
				return this.fixedRowSortOrder;
			}
			set
			{
				if ( value != this.fixedRowSortOrder )
				{
					GridUtils.ValidateEnum( "FixedRowSortOrder", typeof( FixedRowSortOrder ), value );
					this.fixedRowSortOrder = value;
					this.NotifyPropChange( PropertyIds.FixedRowSortOrder, null );
				}
			}
		}

		#endregion // FixedRowSortOrder

		#region ShouldSerializeFixedRowSortOrder

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedRowSortOrder( )
		{
			return FixedRowSortOrder.Default != this.fixedRowSortOrder;
		}

		#endregion // ShouldSerializeFixedRowSortOrder

		#region ResetFixedRowSortOrder

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedRowSortOrder( )
		{
			this.FixedRowSortOrder = FixedRowSortOrder.Default;
		}

		#endregion // ResetFixedRowSortOrder

		#region FixedRowStyle

		/// <summary>
		/// Specifies whether the fixed rows are display at the top or the bottom of the row collection. <b>Default</b> is resolved to <b>Top</b>.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies whether the fixed rows are display at the top or the bottom of the row collection.</p>
		///	<p class="body">You can use the row collection's <see cref="RowsCollection.FixedRows"/> property to actually fix or unfix a row in code.</p>
		///	<p class="body"><b>Node</b> that the fixed rows are not supported in certain view styles. Please see <see cref="FixedRowsCollection"/> for more information.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.FixedRowStyle"/> <seealso cref="UltraGridOverride.FixedRowIndicator"/> 
		///	<seealso cref="RowsCollection.FixedRows"/> <seealso cref="FixedRowsCollection"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_FixedRowStyle")]
		[ LocalizedCategory("LC_Display") ]
		public FixedRowStyle FixedRowStyle
		{
			get
			{
				return this.fixedRowStyle;
			}
			set
			{
				if ( value != this.fixedRowStyle )
				{
					GridUtils.ValidateEnum( "FixedRowStyle", typeof( FixedRowStyle ), value );
					this.fixedRowStyle = value;
					this.NotifyPropChange( PropertyIds.FixedRowStyle, null );
				}
			}
		}

		#endregion // FixedRowStyle

		#region ShouldSerializeFixedRowStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedRowStyle( )
		{
			return FixedRowStyle.Default != this.fixedRowStyle;
		}

		#endregion // ShouldSerializeFixedRowStyle

		#region ResetFixedRowStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedRowStyle( )
		{
			this.FixedRowStyle = FixedRowStyle.Default;
		}

		#endregion // ResetFixedRowStyle

		#region GroupBySummaryDisplayStyle

		/// <summary>
		/// Specifies how summaries in group-by rows are displayed.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies how summaries in group-by rows are displayed.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.GroupBySummaryDisplayStyle"/> <seealso cref="UltraGridOverride.SummaryDisplayArea"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupBySummaryDisplayStyle")]
		[ LocalizedCategory("LC_Display") ]
		public GroupBySummaryDisplayStyle GroupBySummaryDisplayStyle
		{
			get
			{
				return this.groupBySummaryDisplayStyle;
			}
			set
			{
				if ( value != this.groupBySummaryDisplayStyle )
				{
					GridUtils.ValidateEnum( "GroupBySummaryDisplayStyle", typeof( GroupBySummaryDisplayStyle ), value );
					this.groupBySummaryDisplayStyle = value;
					this.NotifyPropChange( PropertyIds.GroupBySummaryDisplayStyle, null );
				}
			}
		}

		#endregion // GroupBySummaryDisplayStyle

		#region ShouldSerializeGroupBySummaryDisplayStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeGroupBySummaryDisplayStyle( )
		{
			return GroupBySummaryDisplayStyle.Default != this.groupBySummaryDisplayStyle;
		}

		#endregion // ShouldSerializeGroupBySummaryDisplayStyle

		#region ResetGroupBySummaryDisplayStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetGroupBySummaryDisplayStyle( )
		{
			this.GroupBySummaryDisplayStyle = GroupBySummaryDisplayStyle.Default;
		}

		#endregion // ResetGroupBySummaryDisplayStyle

		#region SequenceFilterRow

		/// <summary>
		/// This property in conjuction with other Sequence properties determines the order in which the associated special rows are displayed. The special row with a smaller sequence number is displayed before the ones with the larger sequence numbers.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridOverride.SequenceFilterRow"/> <seealso cref="UltraGridOverride.SequenceFixedAddRow"/> <seealso cref="UltraGridOverride.SequenceSummaryRow"/> 
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SequenceFilterRow")]
		[ LocalizedCategory("LC_Display") ]
		public int SequenceFilterRow
		{
			get
			{
				return this.sequenceFilterRow;
			}
			set
			{
				if ( this.sequenceFilterRow != value )
				{
					this.sequenceFilterRow = Math.Max( -1, value );
					this.NotifyPropChange( PropertyIds.SequenceFilterRow );
				}
			}
		}

		#endregion // SequenceFilterRow

		#region ShouldSerializeSequenceFilterRow

		/// <summary>
		/// Returns true if the SequenceFilterRow property is set to a non-negative value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSequenceFilterRow( )
		{
			return this.sequenceFilterRow >= 0;
		}

		#endregion // ShouldSerializeSequenceFilterRow

		#region ResetSequenceFilterRow

		/// <summary>
		/// Resets the SequenceFilterRow property to its default value of -1.
		/// </summary>
		public void ResetSequenceFilterRow( )
		{
			this.SequenceFilterRow = -1;
		}

		#endregion // ResetSequenceFilterRow

		#region SequenceFixedAddRow

		/// <summary>
		/// This property in conjuction with other Sequence properties determines the order in which the associated special rows are displayed. The special row with a smaller sequence number is displayed before the ones with the larger sequence numbers.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridOverride.SequenceFixedAddRow"/> <seealso cref="UltraGridOverride.SequenceFixedAddRow"/> <seealso cref="UltraGridOverride.SequenceSummaryRow"/> 
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SequenceFixedAddRow")]
		[ LocalizedCategory("LC_Display") ]
		public int SequenceFixedAddRow
		{
			get
			{
				return this.sequenceFixedAddRow;
			}
			set
			{
				if ( this.sequenceFixedAddRow != value )
				{
					this.sequenceFixedAddRow = Math.Max( -1, value );
					this.NotifyPropChange( PropertyIds.SequenceFixedAddRow );
				}
			}
		}

		#endregion // SequenceFixedAddRow

		#region ShouldSerializeSequenceFixedAddRow

		/// <summary>
		/// Returns true if the SequenceFixedAddRow property is set to a non-negative value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSequenceFixedAddRow( )
		{
			return this.sequenceFixedAddRow >= 0;
		}

		#endregion // ShouldSerializeSequenceFixedAddRow

		#region ResetSequenceFixedAddRow

		/// <summary>
		/// Resets the SequenceFixedAddRow property to its default value of -1.
		/// </summary>
		public void ResetSequenceFixedAddRow( )
		{
			this.SequenceFixedAddRow = -1;
		}

		#endregion // ResetSequenceFixedAddRow

		#region SequenceSummaryRow

		/// <summary>
		/// This property in conjuction with other Sequence properties determines the order in which the associated special rows are displayed. The special row with a smaller sequence number is displayed before the ones with the larger sequence numbers.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridOverride.SequenceSummaryRow"/> <seealso cref="UltraGridOverride.SequenceSummaryRow"/> <seealso cref="UltraGridOverride.SequenceSummaryRow"/> 
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SequenceSummaryRow")]
		[ LocalizedCategory("LC_Display") ]
		public int SequenceSummaryRow
		{
			get
			{
				return this.sequenceSummaryRow;
			}
			set
			{
				if ( this.sequenceSummaryRow != value )
				{
					this.sequenceSummaryRow = Math.Max( -1, value );
					this.NotifyPropChange( PropertyIds.SequenceSummaryRow );
				}
			}
		}

		#endregion // SequenceSummaryRow

		#region ShouldSerializeSequenceSummaryRow

		/// <summary>
		/// Returns true if the SequenceSummaryRow property is set to a non-negative value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSequenceSummaryRow( )
		{
			return this.sequenceSummaryRow >= 0;
		}

		#endregion // ShouldSerializeSequenceSummaryRow

		#region ResetSequenceSummaryRow

		/// <summary>
		/// Resets the SequenceSummaryRow property to its default value of -1.
		/// </summary>
		public void ResetSequenceSummaryRow( )
		{
			this.SequenceSummaryRow = -1;
		}

		#endregion // ResetSequenceSummaryRow

		#region SpecialRowSeparator

		/// <summary>
		/// Specifies which special rows should display the separators.
		/// </summary>
		///	<remarks>
		/// <p class="body">A special row separator is a horizontal ui element that separates filter row, fixed add-row, summary row and fixed rows from each other. This ui element is 6 pixels high by default however the height can be controlled by setting the <see cref="UltraGridOverride.SpecialRowSeparatorHeight"/> property. You can specify which special rows display separator elements by setting the <b>SpecialRowSeparator</b> property.</p>
		/// <p class="body"><b>Default</b> is resolved to <b>TemplateAddRow</b> if the <see cref="UltraGridOverride.AllowAddNew"/> is set to <b>FixedAddRowOnTop</b> or <b>FixedAddRowOnBottom</b>. Otherwise it�s resolved to <b>None</b>.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.SpecialRowSeparator"/> <seealso cref="UltraGridOverride.SpecialRowSeparatorHeight"/> <seealso cref="UltraGridOverride.SpecialRowSeparatorAppearance"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SpecialRowSeparator")]
		[ LocalizedCategory("LC_Display") ]
		[ Editor( typeof( Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		public SpecialRowSeparator SpecialRowSeparator
		{
			get
			{
				return this.specialRowSeparator;
			}
			set
			{
				if ( value != this.specialRowSeparator )
				{
					GridUtils.ValidateEnum( "SpecialRowSeparator", typeof( SpecialRowSeparator ), value );
					this.specialRowSeparator = value;
					this.NotifyPropChange( PropertyIds.SpecialRowSeparator, null );
				}
			}
		}

		#endregion // SpecialRowSeparator

		#region ShouldSerializeSpecialRowSeparator

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSpecialRowSeparator( )
		{
			return SpecialRowSeparator.Default != this.specialRowSeparator;
		}

		#endregion // ShouldSerializeSpecialRowSeparator

		#region ResetSpecialRowSeparator

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSpecialRowSeparator( )
		{
			this.SpecialRowSeparator = SpecialRowSeparator.Default;
		}

		#endregion // ResetSpecialRowSeparator

		#region SpecialRowSeparatorHeight

		/// <summary>
		/// Specifies the height of the special row separator elements. This element separates filter row, fixed add-row, summary footer row, fixed rows and non-fixed rows from each other. Default value of -1 is resolved to 6. If this property is set to 0, no separator element is displayed.
		/// </summary>
		/// <remarks>
		/// <p class="body">A special row separator is a horizontal ui element that separates filter row, fixed add-row, summary row and fixed rows from each other. This ui element is 6 pixels high by default however the height can be controlled by setting the <b>SpecialRowSeparatorHeight</b> property. You can hide the special row separator elements by setting the <see cref="UltraGridOverride.SpecialRowSeparator"/> property.</p>
		/// <seealso cref="UltraGridOverride.SpecialRowSeparator"/> <seealso cref="UltraGridOverride.SpecialRowSeparatorAppearance"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SpecialRowSeparatorHeight")]
		[ LocalizedCategory("LC_Appearance") ]
		public int SpecialRowSeparatorHeight
		{
			get
			{
				return this.specialRowSeparatorHeight;
			}
			set
			{
				if ( this.specialRowSeparatorHeight != value )
				{
					this.specialRowSeparatorHeight = Math.Max( -1, value );
					this.NotifyPropChange( PropertyIds.SpecialRowSeparatorHeight );
				}
			}
		}

		#endregion // SpecialRowSeparatorHeight

		#region ShouldSerializeSpecialRowSeparatorHeight

		/// <summary>
		/// Returns true if the SpecialRowSeparatorHeight property is set to a non-negative value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSpecialRowSeparatorHeight( )
		{
			return this.specialRowSeparatorHeight >= 0;
		}

		#endregion // ShouldSerializeSpecialRowSeparatorHeight

		#region ResetSpecialRowSeparatorHeight

		/// <summary>
		/// Resets the SpecialRowSeparatorHeight property to its default value of -1.
		/// </summary>
		public void ResetSpecialRowSeparatorHeight( )
		{
			this.SpecialRowSeparatorHeight = -1;
		}

		#endregion // ResetSpecialRowSeparatorHeight

		#region SummaryDisplayArea

		/// <summary>
		/// Specifies if and which area(s) the summaries will be displayed. This property is exposed off the <see cref="SummarySettings"/> object as well. This also affects the summaries selected by the user at runtime.
		/// </summary>
		///	<remarks>
		///	<p class="body">Specifies if and which area(s) the summaries will be displayed. This property is exposed off the <see cref="SummarySettings"/> object as well. This also affects the summaries selected by the user at runtime.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.SummaryDisplayAreas"/> 
		///	<seealso cref="UltraGridOverride.GroupBySummaryDisplayStyle"/> 
		///	<seealso cref="AllowRowSummaries"/>
		///	<seealso cref="UltraGridBand.Summaries"/>
		///	<seealso cref="SummarySettingsCollection"/>
		///	<seealso cref="SummaryValueAppearance"/>
		///	<seealso cref="GroupBySummaryValueAppearance"/>
		///	<seealso cref="SummarySettings.Appearance"/>
		///	<seealso cref="SummaryValue.Appearance"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SummaryDisplayArea")]
		[ LocalizedCategory("LC_Display") ]
		[ Editor( typeof( Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		public SummaryDisplayAreas SummaryDisplayArea
		{
			get
			{
				return this.summaryDisplayArea;
			}
			set
			{
				if ( value != this.summaryDisplayArea )
				{
					GridUtils.ValidateEnum( "SummaryDisplayArea", typeof( SummaryDisplayAreas ), value );
					this.summaryDisplayArea = value;
					this.NotifyPropChange( PropertyIds.SummaryDisplayArea, null );
				}
			}
		}

		#endregion // SummaryDisplayArea

		#region ShouldSerializeSummaryDisplayArea

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSummaryDisplayArea( )
		{
			return SummaryDisplayAreas.Default != this.summaryDisplayArea;
		}

		#endregion // ShouldSerializeSummaryDisplayArea

		#region ResetSummaryDisplayArea

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSummaryDisplayArea( )
		{
			this.SummaryDisplayArea = SummaryDisplayAreas.Default;
		}

		#endregion // ResetSummaryDisplayArea

		#region SummaryFooterSpacingAfter

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered after a summary footer.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>SummaryFooterSpacingAfter</b> property to specify the space that follows a summary footer in the band or the grid controlled by the specified override. The space allows the background area of the grid to show through, and can be useful when you have specified a picture or a texture for the background. To control the space that precedes a summary footer, use the <see cref="UltraGridOverride.SummaryFooterSpacingBefore"/> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SummaryFooterSpacingAfter")]
		[ LocalizedCategory("LC_Display") ]
		public int SummaryFooterSpacingAfter
		{
			get
			{
				return this.summaryFooterSpacingAfter;
			}

			set
			{
				if ( value != this.summaryFooterSpacingAfter )
				{
					// if negative then set to -1 (default)
					//
					this.summaryFooterSpacingAfter = Math.Max( -1, value );
    		    	
					this.NotifyPropChange( PropertyIds.SummaryFooterSpacingAfter, null );
				}
			}
		}

		#endregion // SummaryFooterSpacingAfter

		#region ShouldSerializeSummaryFooterSpacingAfter

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSummaryFooterSpacingAfter( ) 
		{
			return ( this.summaryFooterSpacingAfter >= 0 );
		}
 
		#endregion // ShouldSerializeSummaryFooterSpacingAfter

		#region ResetSummaryFooterSpacingAfter

		/// <summary>
		/// Resets SummaryFooterSpacingAfter to its default value (-1).
		/// </summary>
		public void ResetSummaryFooterSpacingAfter( ) 
		{
			this.SummaryFooterSpacingAfter = -1;
		}

		#endregion // ResetSummaryFooterSpacingAfter

		#region SummaryFooterSpacingBefore

		/// <summary>
		/// Returns or sets the amount of spacing (in pixels) rendered before a summary footer.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>SummaryFooterSpacingBefore</b> property to specify the space that precedes a summary footer in the band or the grid controlled by the specified override.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SummaryFooterSpacingBefore")]
		[ LocalizedCategory("LC_Display") ]
		public int SummaryFooterSpacingBefore
		{
			get
			{
				return this.summaryFooterSpacingBefore;
			}

			set
			{
				if ( value != this.summaryFooterSpacingBefore )
				{
					// if negative then set to -1 (default)
					//
					this.summaryFooterSpacingBefore = Math.Max( -1, value );
    		    	
					this.NotifyPropChange( PropertyIds.SummaryFooterSpacingBefore, null );
				}
			}
		}

		#endregion // SummaryFooterSpacingBefore

		#region ShouldSerializeSummaryFooterSpacingBefore

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSummaryFooterSpacingBefore( ) 
		{
			return ( this.summaryFooterSpacingBefore >= 0 );
		}
 
		#endregion // ShouldSerializeSummaryFooterSpacingBefore

		#region ResetSummaryFooterSpacingBefore

		/// <summary>
		/// Resets SummaryFooterSpacingBefore to its default value (-1).
		/// </summary>
		public void ResetSummaryFooterSpacingBefore( ) 
		{
			this.SummaryFooterSpacingBefore = -1;
		}

		#endregion // ResetSummaryFooterSpacingBefore

		#region TemplateAddRowPrompt

		/// <summary>
		/// Specifies the prompt text to display in the add row. By default no prompt is displayed. Once the add row is activated the prompt disappears (and reappears when it�s deactivated). The text spans multiple cells unless <see cref="UltraGridBand.SpecialRowPromptField"/> property has been set to a valid column key in which case the prompt is displayed in the associated cell.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies the prompt text to display in the add row. Once the add row is activated the prompt disappears (and reappears when it�s deactivated). The text will span multiple cells unless <see cref="UltraGridBand.SpecialRowPromptField"/> property has been set to a valid column key.</p>
		/// <seealso cref="UltraGridOverride.TemplateAddRowPromptAppearance"/> <seealso cref="UltraGridOverride.AllowAddNew"/> <seealso cref="UltraGridBand.SpecialRowPromptField"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_TemplateAddRowPrompt")]
		[ LocalizedCategory("LC_Display") ]
		[ Localizable( true ) ]
		public string TemplateAddRowPrompt
		{
			get
			{
				return this.templateAddRowPrompt;
			}
			set
			{
				if ( value != this.templateAddRowPrompt )
				{
					this.templateAddRowPrompt = value;
					this.NotifyPropChange( PropertyIds.TemplateAddRowPrompt, null );
				}
			}
		}

		#endregion // TemplateAddRowPrompt

		#region ShouldSerializeTemplateAddRowPrompt

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeTemplateAddRowPrompt( )
		{
			return null != this.templateAddRowPrompt && 0 != this.templateAddRowPrompt.Length;
		}

		#endregion // ShouldSerializeTemplateAddRowPrompt

		#region ResetTemplateAddRowPrompt

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetTemplateAddRowPrompt( )
		{
			this.TemplateAddRowPrompt = null;
		}

		#endregion // ResetTemplateAddRowPrompt
		
		#region FilterClearButtonAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the filter clear buttons of filter rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterClearButtonAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterClearButtonAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilterClearButtonAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilterClearButtonAppearance, value );
			}
		}

		#endregion // FilterClearButtonAppearance

		#region ShouldSerializeFilterClearButtonAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterClearButtonAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilterClearButtonAppearance ); 
		}

		#endregion // ShouldSerializeFilterClearButtonAppearance
 
		#region ResetFilterClearButtonAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterClearButtonAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilterClearButtonAppearance ); 
		}

		#endregion // ResetFilterClearButtonAppearance

		#region FilterCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells of filter rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilterCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilterCellAppearance, value );
			}
		}

		#endregion // FilterCellAppearance

		#region ShouldSerializeFilterCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilterCellAppearance ); 
		}

		#endregion // ShouldSerializeFilterCellAppearance
 
		#region ResetFilterCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilterCellAppearance ); 
		}

		#endregion // ResetFilterCellAppearance

		#region FilterCellAppearanceActive

		/// <summary>
		/// Determines the formatting attributes that will be applied to the filter row cells that contains active filters.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterCellAppearanceActive")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterCellAppearanceActive
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilterCellAppearanceActive );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilterCellAppearanceActive, value );
			}
		}

		#endregion // FilterCellAppearanceActive

		#region ShouldSerializeFilterCellAppearanceActive

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterCellAppearanceActive( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilterCellAppearanceActive ); 
		}

		#endregion // ShouldSerializeFilterCellAppearanceActive
 
		#region ResetFilterCellAppearanceActive

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterCellAppearanceActive( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilterCellAppearanceActive ); 
		}

		#endregion // ResetFilterCellAppearanceActive

		#region FilterOperatorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the operator indicators in filter rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterOperatorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterOperatorAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilterOperatorAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilterOperatorAppearance, value );
			}
		}

		#endregion // FilterOperatorAppearance

		#region ShouldSerializeFilterOperatorAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterOperatorAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilterOperatorAppearance ); 
		}

		#endregion // ShouldSerializeFilterOperatorAppearance
 
		#region ResetFilterOperatorAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilterOperatorAppearance ); 
		}

		#endregion // ResetFilterOperatorAppearance

		#region FilterRowAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the filter rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilterRowAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilterRowAppearance, value );
			}
		}

		#endregion // FilterRowAppearance

		#region ShouldSerializeFilterRowAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilterRowAppearance ); 
		}

		#endregion // ShouldSerializeFilterRowAppearance
 
		#region ResetFilterRowAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterRowAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilterRowAppearance ); 
		}

		#endregion // ResetFilterRowAppearance

		#region FilterRowAppearanceActive

		/// <summary>
		/// Determines the formatting attributes that will be applied to filter rows that contain active filters.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterRowAppearanceActive")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterRowAppearanceActive
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilterRowAppearanceActive );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilterRowAppearanceActive, value );
			}
		}

		#endregion // FilterRowAppearanceActive

		#region ShouldSerializeFilterRowAppearanceActive

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterRowAppearanceActive( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilterRowAppearanceActive ); 
		}

		#endregion // ShouldSerializeFilterRowAppearanceActive
 
		#region ResetFilterRowAppearanceActive

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterRowAppearanceActive( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilterRowAppearanceActive ); 
		}

		#endregion // ResetFilterRowAppearanceActive

		#region FilterRowPromptAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the filter row prompt.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <see cref="UltraGridOverride.FilterRowPrompt"/> property to show a prompt in the filter row.</p>
		/// <see cref="UltraGridOverride.FilterRowPrompt"/> <see cref="UltraGridOverride.FilterUIType"/> <see cref="UltraGridOverride.AllowRowFiltering"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterRowPromptAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterRowPromptAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilterRowPromptAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilterRowPromptAppearance, value );
			}
		}

		#endregion // FilterRowPromptAppearance

		#region ShouldSerializeFilterRowPromptAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterRowPromptAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilterRowPromptAppearance ); 
		}

		#endregion // ShouldSerializeFilterRowPromptAppearance
 
		#region ResetFilterRowPromptAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterRowPromptAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilterRowPromptAppearance ); 
		}

		#endregion // ResetFilterRowPromptAppearance

		#region FilterRowSelectorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the row selectors of filter rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FilterRowSelectorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FilterRowSelectorAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FilterRowSelectorAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FilterRowSelectorAppearance, value );
			}
		}

		#endregion // FilterRowSelectorAppearance

		#region ShouldSerializeFilterRowSelectorAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterRowSelectorAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FilterRowSelectorAppearance ); 
		}

		#endregion // ShouldSerializeFilterRowSelectorAppearance
 
		#region ResetFilterRowSelectorAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterRowSelectorAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FilterRowSelectorAppearance ); 
		}

		#endregion // ResetFilterRowSelectorAppearance

		#region FixedRowAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the rows that are fixed.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FixedRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FixedRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FixedRowAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FixedRowAppearance, value );
			}
		}

		#endregion // FixedRowAppearance

		#region ShouldSerializeFixedRowAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFixedRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FixedRowAppearance ); 
		}

		#endregion // ShouldSerializeFixedRowAppearance
 
		#region ResetFixedRowAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedRowAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FixedRowAppearance ); 
		}

		#endregion // ResetFixedRowAppearance

		#region FixedRowCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells of fixed rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FixedRowCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FixedRowCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FixedRowCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FixedRowCellAppearance, value );
			}
		}

		#endregion // FixedRowCellAppearance

		#region ShouldSerializeFixedRowCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFixedRowCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FixedRowCellAppearance ); 
		}

		#endregion // ShouldSerializeFixedRowCellAppearance
 
		#region ResetFixedRowCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedRowCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FixedRowCellAppearance ); 
		}

		#endregion // ResetFixedRowCellAppearance

		#region FixedRowSelectorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the row selectors of fixed rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_FixedRowSelectorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase FixedRowSelectorAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.FixedRowSelectorAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.FixedRowSelectorAppearance, value );
			}
		}

		#endregion // FixedRowSelectorAppearance

		#region ShouldSerializeFixedRowSelectorAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFixedRowSelectorAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.FixedRowSelectorAppearance ); 
		}

		#endregion // ShouldSerializeFixedRowSelectorAppearance
 
		#region ResetFixedRowSelectorAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedRowSelectorAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.FixedRowSelectorAppearance ); 
		}

		#endregion // ResetFixedRowSelectorAppearance

		#region SpecialRowSeparatorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the special row separator elements.
		/// </summary>
		/// <remarks>
		/// <p class="body">A special row separator is a horizontal ui element that separates filter row, fixed add-row, summary row and fixed rows from each other. This ui element is 6 pixels high by default however the height can be controlled by setting the <see cref="UltraGridOverride.SpecialRowSeparatorHeight"/> property. You can hide the special row separator elements by setting the <see cref="UltraGridOverride.SpecialRowSeparator"/> property.</p>
		/// <seealso cref="UltraGridOverride.SpecialRowSeparator"/> <seealso cref="UltraGridOverride.SpecialRowSeparatorHeight"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_SpecialRowSeparatorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SpecialRowSeparatorAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.SpecialRowSeparatorAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.SpecialRowSeparatorAppearance, value );
			}
		}

		#endregion // SpecialRowSeparatorAppearance

		#region ShouldSerializeSpecialRowSeparatorAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSpecialRowSeparatorAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.SpecialRowSeparatorAppearance ); 
		}

		#endregion // ShouldSerializeSpecialRowSeparatorAppearance
 
		#region ResetSpecialRowSeparatorAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSpecialRowSeparatorAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.SpecialRowSeparatorAppearance ); 
		}

		#endregion // ResetSpecialRowSeparatorAppearance

		#region TemplateAddRowPromptAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the template add-row prompt.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridOverride.TemplateAddRowPrompt"/> <seealso cref="UltraGridOverride.AllowAddNew"/> <seealso cref="Infragistics.Win.UltraWinGrid.AllowAddNew"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_TemplateAddRowPromptAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase TemplateAddRowPromptAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.TemplateAddRowPromptAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.TemplateAddRowPromptAppearance, value );
			}
		}

		#endregion // TemplateAddRowPromptAppearance

		#region ShouldSerializeTemplateAddRowPromptAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeTemplateAddRowPromptAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.TemplateAddRowPromptAppearance ); 
		}

		#endregion // ShouldSerializeTemplateAddRowPromptAppearance
 
		#region ResetTemplateAddRowPromptAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetTemplateAddRowPromptAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.TemplateAddRowPromptAppearance ); 
		}

		#endregion // ResetTemplateAddRowPromptAppearance

		#region GroupBySummaryValueAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells of filter rows.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_GroupBySummaryValueAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase GroupBySummaryValueAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.GroupBySummaryValueAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.GroupBySummaryValueAppearance, value );
			}
		}

		#endregion // GroupBySummaryValueAppearance

		#region ShouldSerializeGroupBySummaryValueAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeGroupBySummaryValueAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.GroupBySummaryValueAppearance ); 
		}

		#endregion // ShouldSerializeGroupBySummaryValueAppearance
 
		#region ResetGroupBySummaryValueAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetGroupBySummaryValueAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.GroupBySummaryValueAppearance ); 
		}

		#endregion // ResetGroupBySummaryValueAppearance

		#endregion // Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region RowSelectorNumberStyle

		// SSP 4/6/05 - NAS 5.2 Row Numbers
		// Added RowSelectorNumberStyle property.
		//
		/// <summary>
		/// Specifies if and what kind of row numbers are displayed in the row selectors.
		/// </summary>
		///	<remarks>
		///	<p class="body">The row numbers are 1 based. In other words the numbers displayed in 
		///	the row selectors are not the actual index values but one plus the index value.</p>
		///	<p class="body">You can use the <b>RowSelectorNumberStyle</b> property to display 
		///	row numbers in the row selectors. If this property is set to a value for displaying 
		///	row numbers then the default widths of the row selectors is automatically enlarged 
		///	to accomodate row numbers. You can explicitly specify the width of the row selectors
		///	using <see cref="UltraGridOverride.RowSelectorWidth"/> property.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.RowSelectorNumberStyle"/> <seealso cref="UltraGridOverride.RowSelectorWidth"/> 
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_RowSelectorNumberStyle")]
		[ LocalizedCategory("LC_Display") ]
		public RowSelectorNumberStyle RowSelectorNumberStyle
		{
			get
			{
				return this.rowSelectorNumberStyle;
			}
			set
			{
				if ( value != this.rowSelectorNumberStyle )
				{
					GridUtils.ValidateEnum( "RowSelectorNumberStyle", typeof( RowSelectorNumberStyle ), value );
					this.rowSelectorNumberStyle = value;
					this.NotifyPropChange( PropertyIds.RowSelectorNumberStyle, null );
				}
			}
		}

		#endregion // RowSelectorNumberStyle

		#region ShouldSerializeRowSelectorNumberStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeRowSelectorNumberStyle( )
		{
			return RowSelectorNumberStyle.Default != this.rowSelectorNumberStyle;
		}

		#endregion // ShouldSerializeRowSelectorNumberStyle

		#region ResetRowSelectorNumberStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetRowSelectorNumberStyle( )
		{
			this.RowSelectorNumberStyle = RowSelectorNumberStyle.Default;
		}

		#endregion // ResetRowSelectorNumberStyle

		// MRS 4/5/05 - Added these three properties to allow the user to control column moving in RowLayout mode. 
		#region AllowRowLayoutColMoving

		#region AllowRowLayoutColMoving

		/// <summary>
		/// Specifies whether columns can be moved by the user at run-time in RowLayout mode.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property determines whether the user can click and drag a column header to reposition it at run-time in a RowLayout.</p>
		///	<p class="body">The <b>Default</b> setting for this property will resolve based on the AllowColMoving property.</p>
		///	<p class="note">Columns without visible headers cannot be repositioned in this way and only a single column can be dragged at a time. If multiple columns are selected, no drag will occur.</p>
		///	<p class="note">This property applies to the UltraGrid only. It is not support for UltraCombo or UltraDropDown.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowRowLayoutColMoving")]
		[ LocalizedCategory("LC_Behavior") ]
		public Layout.GridBagLayoutAllowMoving AllowRowLayoutColMoving
		{
			get
			{
				return this.allowRowLayoutColMoving;
			}
			set
			{
				if ( value != this.allowRowLayoutColMoving )
				{
					GridUtils.ValidateEnum( "AllowRowLayoutColMoving", typeof( Layout.GridBagLayoutAllowMoving ), value );
					this.allowRowLayoutColMoving = value;
					this.NotifyPropChange( PropertyIds.AllowRowLayoutColMoving, null );
				}
			}
		}

		#endregion // AllowRowLayoutColMoving

		#region ShouldSerializeAllowRowLayoutColMoving

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowRowLayoutColMoving( )
		{
			return Layout.GridBagLayoutAllowMoving.Default != this.allowRowLayoutColMoving;
		}

		#endregion // ShouldSerializeAllowRowLayoutColMoving

		#region ResetAllowRowLayoutColMoving

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAllowRowLayoutColMoving( )
		{
			this.AllowRowLayoutColMoving = Layout.GridBagLayoutAllowMoving.Default;
		}

		#endregion // ResetAllowRowLayoutColMoving

		#endregion AllowRowLayoutColMoving

		#region AllowRowLayoutCellSpanSizing

		#region AllowRowLayoutCellSpanSizing

		/// <summary>
		/// Specifies whether cells can be span resized by the user at run-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property determines whether the user can ctrl+click and drag a cell to change it's span at run-time.</p>
		///	<p class="body">Span resizing changes the SpanX or SpanY of the cell and may also change the LabelSpan.</p>
		///	<p class="note">This property applies to the UltraGrid only. It is not supported for UltraCombo or UltraDropDown.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowRowLayoutCellSpanSizing")]
		[ LocalizedCategory("LC_Behavior") ]
		public Layout.GridBagLayoutAllowSpanSizing AllowRowLayoutCellSpanSizing
		{
			get
			{
				return this.allowRowLayoutCellSpanSizing;
			}
			set
			{
				if ( value != this.allowRowLayoutCellSpanSizing )
				{
					GridUtils.ValidateEnum( "AllowRowLayoutCellSpanSizing", typeof( Layout.GridBagLayoutAllowSpanSizing ), value );
					this.allowRowLayoutCellSpanSizing = value;
					this.NotifyPropChange( PropertyIds.AllowRowLayoutCellSpanSizing, null );
				}
			}
		}

		#endregion // AllowRowLayoutCellSpanSizing

		#region ShouldSerializeAllowRowLayoutCellSpanSizing

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowRowLayoutCellSpanSizing( )
		{
			return Layout.GridBagLayoutAllowSpanSizing.Default != this.AllowRowLayoutCellSpanSizing;
		}

		#endregion // ShouldSerializeAllowRowLayoutCellSpanSizing

		#region ResetAllowRowLayoutCellSpanSizing

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAllowRowLayoutCellSpanSizing( )
		{
			this.AllowRowLayoutCellSpanSizing = Layout.GridBagLayoutAllowSpanSizing.Default;
		}

		#endregion // ResetAllowRowLayoutCellSpanSizing

		#endregion AllowRowLayoutCellSpanSizing

		#region AllowRowLayoutLabelSpanSizing

		#region AllowRowLayoutLabelSpanSizing

		/// <summary>
		/// Specifies whether labels can be span resized by the user at run-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property determines whether the user can ctrl+click and drag a label to change it's span at run-time.</p>
		///	<p class="body">Span resizing changes the SpanX or SpanY of the cell and may also change the LabelSpan.</p>
		///	<p class="note">This property applies to the UltraGrid only. It is not supported for UltraCombo or UltraDropDown.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_AllowRowLayoutLabelSpanSizing")]
		[ LocalizedCategory("LC_Behavior") ]
		public Layout.GridBagLayoutAllowSpanSizing AllowRowLayoutLabelSpanSizing
		{
			get
			{
				return this.allowRowLayoutLabelSpanSizing;
			}
			set
			{
				if ( value != this.AllowRowLayoutLabelSpanSizing )
				{
					GridUtils.ValidateEnum( "AllowRowLayoutLabelSpanSizing", typeof( Layout.GridBagLayoutAllowSpanSizing ), value );
					this.allowRowLayoutLabelSpanSizing = value;
					this.NotifyPropChange( PropertyIds.AllowRowLayoutLabelSpanSizing, null );
				}
			}
		}

		#endregion // AllowRowLayoutLabelSpanSizing

		#region ShouldSerializeAllowRowLayoutLabelSpanSizing

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeAllowRowLayoutLabelSpanSizing( )
		{
			return Layout.GridBagLayoutAllowSpanSizing.Default != this.AllowRowLayoutLabelSpanSizing;
		}

		#endregion // ShouldSerializeAllowRowLayoutLabelSpanSizing

		#region ResetAllowRowLayoutLabelSpanSizing

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAllowRowLayoutLabelSpanSizing( )
		{
			this.AllowRowLayoutLabelSpanSizing = Layout.GridBagLayoutAllowSpanSizing.Default;
		}

		#endregion // ResetAllowRowLayoutLabelSpanSizing

		#endregion AllowRowLayoutLabelSpanSizing

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
		//
		#region ButtonStyle Logic

			#region ButtonStyle

		/// <summary>
		/// Gets/sets the style of the buttons in the columns and the AddNew box.
		/// </summary>
		[LocalizedDescription("LD_UltraGridOverride_P_ButtonStyle")]
		[LocalizedCategory("LC_Display")]
		public UIElementButtonStyle ButtonStyle
		{
			get { return this.buttonStyle; }
			set
			{
				if( value != this.buttonStyle )
				{
					GridUtils.ValidateEnum( typeof(UIElementButtonStyle), value );

					this.buttonStyle = value;

					this.NotifyPropChange( PropertyIds.ButtonStyle );
				}
			}
		}

			#endregion // ButtonStyle

			#region ShouldSerializeButtonStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeButtonStyle( )
		{
			return UIElementButtonStyle.Default != this.buttonStyle;
		}

			#endregion // ShouldSerializeButtonStyle

			#region ResetButtonStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetButtonStyle( )
		{
			this.ButtonStyle = UIElementButtonStyle.Default;
		}

			#endregion // ResetButtonStyle

		#endregion // ButtonStyle Logic

		// JAS v5.2 Wrapped Header Text 4/18/05
		//
		#region WrapHeaderText Logic

			#region WrapHeaderText

		/// <summary>
		/// Gets/sets a value which indicates whether headers in the grid will automatically wrap their text and adjust their height, if necessary.
        /// Note, this property is not honored if Row Layout mode is being used.
		/// </summary>
		[LocalizedDescription("LD_UltraGridOverride_P_WrapHeaderText")]
		[LocalizedCategory("LC_Behavior")]
		public DefaultableBoolean WrapHeaderText
		{
			get
			{
				return this.wrapHeaderText;
			}
			set
			{
				if( value != this.wrapHeaderText )
				{
					GridUtils.ValidateEnum( typeof(DefaultableBoolean), value );

					this.wrapHeaderText = value;

					this.NotifyPropChange( PropertyIds.WrapHeaderText );
				}
			}
		}

			#endregion // WrapHeaderText

			#region ShouldSerializeWrapHeaderText

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeWrapHeaderText()
		{
			return DefaultableBoolean.Default != this.wrapHeaderText;
		}

			#endregion // ShouldSerializeWrapHeaderText

			#region ResetWrapHeaderText

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetWrapHeaderText()
		{
			this.WrapHeaderText = DefaultableBoolean.Default;
		}

			#endregion // ResetWrapHeaderText

		#endregion // WrapHeaderText Logic

		// JAS v5.2 GroupBy Break Behavior 5/4/05
		//
		#region SortComparisonType Logic

			#region SortComparisonType

		/// <summary>
		/// Gets/sets the type of sorting which will be performed by columns.
		/// Note, this setting can be overridden by a column via it's <b>SortComparisonType</b> property.
		/// </summary>
		[LocalizedDescription("LD_UltraGridOverride_P_SortComparisonType")]
		[LocalizedCategory("LC_Behavior")]
		public SortComparisonType SortComparisonType
		{
			get
			{
				return this.sortComparisonType;
			}
			set
			{
				if( this.SortComparisonType != value )
				{
					GridUtils.ValidateEnum( typeof(SortComparisonType), value );

					this.sortComparisonType = value;

					this.NotifyPropChange( PropertyIds.SortComparisonType );
				}
			}
		}

			#endregion // SortComparisonType

			#region ShouldSerializeSortComparisonType

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeSortComparisonType()
		{
			return SortComparisonType.Default != this.sortComparisonType;
		}

			#endregion // ShouldSerializeSortComparisonType

			#region ResetSortComparisonType

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSortComparisonType()
		{
			this.SortComparisonType = SortComparisonType.Default;
		}

			#endregion // ResetSortComparisonType

		#endregion // SortComparisonType Logic

		// SSP 7/14/05 - NAS 5.3 Header Placement
		// 
		#region NAS 5.3 Header Placement

		#region HeaderPlacement

		/// <summary>
		/// Specifies if and how headers are displayed.
		/// </summary>
		///	<remarks>
		///	<p class="body">
		///	By default the UltraGrid repeats headers whenever there is a break in the band. 
		///	In other words a visible row displays column headers if the previous visible row 
		///	is from a different band. For example in a multi band hiearchy if you expand a 
		///	row, it's sibling row from the same band also displays headers. This repeating
		///	of headers can lead to waste of screen space. This feature lets you specify 
		///	different header placement styles that minimize and even eliminate repeating of 
		///	the column headers.
		///	</p>
		///	<p class="body">
		///	Note that this property has no effect when the <see cref="UltraGridLayout.ViewStyle"/> 
		///	is set to <b>SingleBand</b> or when <see cref="UltraGridLayout.ViewStyleBand"/> is
		///	set to <b>Horizontal</b>. Due to the nature of these view styles the headers are 
		///	always non-repeating (fixed on top), and therefore this property has no effect when
		///	these view styles are in effect. By default these view styles are not in effect.
		///	</p>
		///	<p>
		///	Also note that header placement will take effect when printing however it will not
		///	have any effect when exporting to excel.
		///	</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.HeaderPlacement"/> 
		///	<seealso cref="UltraGridBand.ColHeadersVisible"/> 
		///	<seealso cref="UltraGridBand.HeaderVisible"/> 
		///	<seealso cref="UltraGridBand.GroupHeadersVisible"/> 
		///	<seealso cref="UltraGridLayout.CaptionVisible"/> 
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_HeaderPlacement")]
		[ LocalizedCategory("LC_Display") ]
		public HeaderPlacement HeaderPlacement
		{
			get
			{
				return this.headerPlacement;
			}
			set
			{
				if ( value != this.headerPlacement )
				{
					GridUtils.ValidateEnum( "HeaderPlacement", typeof( HeaderPlacement ), value );
					this.headerPlacement = value;
					this.NotifyPropChange( PropertyIds.HeaderPlacement, null );
				}
			}
		}

		#endregion // HeaderPlacement

		#region ShouldSerializeHeaderPlacement

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeHeaderPlacement( )
		{
			return HeaderPlacement.Default != this.headerPlacement;
		}

		#endregion // ShouldSerializeHeaderPlacement

		#region ResetHeaderPlacement

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetHeaderPlacement( )
		{
			this.HeaderPlacement = HeaderPlacement.Default;
		}

		#endregion // ResetHeaderPlacement

		#endregion // NAS 5.3 Header Placement

		// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
		// 
		#region NAS 5.3 Invalid Value Behavior

		#region InvalidValueBehavior

		/// <summary>
		/// Specifies the behavior when the user attempts to leave a cell after entering an invalid value. <b>Default</b> is resolved to <b>RetainValueAndFocus</b>.
		/// </summary>
		///	<remarks>
		///	<p class="body">
		///	<b>InvalidValueBehavior</b> property is used to specify what actions are taken
		///	by the UltraGrid when the user attempts to leave a cell after entering an invalid
		///	value. This property provides the default values for the properties on
		///	<see cref="CellDataErrorEventArgs"/> when it fires the <see cref="UltraGrid.CellDataError"/> 
		///	event. Therefore the same functionality can be achieved by hooking into that event
		///	and setting the properties on the event args. Also the event args
		///	exposes other properties that control other aspects of the behavior.
		///	</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.InvalidValueBehavior"/> 
		///	<seealso cref="UltraGrid.CellDataError"/> 
		///	<seealso cref="CellDataErrorEventArgs"/> 
		///	<seealso cref="UltraGrid.Error"/> 
		///	<seealso cref="UltraGridColumn.InvalidValueBehavior"/>
		///	</remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_InvalidValueBehavior")]
		[ LocalizedCategory("LC_Display") ]
		public InvalidValueBehavior InvalidValueBehavior
		{
			get
			{
				return this.invalidValueBehavior;
			}
			set
			{
				if ( value != this.invalidValueBehavior )
				{
					GridUtils.ValidateEnum( "InvalidValueBehavior", typeof( InvalidValueBehavior ), value );
					this.invalidValueBehavior = value;
					this.NotifyPropChange( PropertyIds.InvalidValueBehavior, null );
				}
			}
		}

		#endregion // InvalidValueBehavior

		#region ShouldSerializeInvalidValueBehavior

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeInvalidValueBehavior( )
		{
			return InvalidValueBehavior.Default != this.invalidValueBehavior;
		}

		#endregion // ShouldSerializeInvalidValueBehavior

		#region ResetInvalidValueBehavior

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetInvalidValueBehavior( )
		{
			this.InvalidValueBehavior = InvalidValueBehavior.Default;
		}

		#endregion // ResetInvalidValueBehavior

		#endregion // NAS 5.3 Invalid Value Behavior

		// SSP 8/7/05 - NAS 5.3 HotTracking
		// 
		#region NAS 5.3 HotTracking

		#region HotTrackCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cell that's currently hot-tracked.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_HotTrackCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase HotTrackCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.HotTrackCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.HotTrackCellAppearance, value );
			}
		}

		#endregion // HotTrackCellAppearance

		#region ShouldSerializeHotTrackCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHotTrackCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.HotTrackCellAppearance ); 
		}

		#endregion // ShouldSerializeHotTrackCellAppearance
 
		#region ResetHotTrackCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetHotTrackCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.HotTrackCellAppearance ); 
		}

		#endregion // ResetHotTrackCellAppearance

		#region HotTrackHeaderAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the header that's currently hot-tracked.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_HotTrackHeaderAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase HotTrackHeaderAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.HotTrackHeaderAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.HotTrackHeaderAppearance, value );
			}
		}

		#endregion // HotTrackHeaderAppearance

		#region ShouldSerializeHotTrackHeaderAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHotTrackHeaderAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.HotTrackHeaderAppearance ); 
		}

		#endregion // ShouldSerializeHotTrackHeaderAppearance
 
		#region ResetHotTrackHeaderAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetHotTrackHeaderAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.HotTrackHeaderAppearance ); 
		}

		#endregion // ResetHotTrackHeaderAppearance

		#region HotTrackRowAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the row that's currently hot-tracked.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_HotTrackRowAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase HotTrackRowAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.HotTrackRowAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.HotTrackRowAppearance, value );
			}
		}

		#endregion // HotTrackRowAppearance

		#region ShouldSerializeHotTrackRowAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHotTrackRowAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.HotTrackRowAppearance ); 
		}

		#endregion // ShouldSerializeHotTrackRowAppearance
 
		#region ResetHotTrackRowAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetHotTrackRowAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.HotTrackRowAppearance ); 
		}

		#endregion // ResetHotTrackRowAppearance

		#region HotTrackRowCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells of the row that's currently hot-tracked.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_HotTrackRowCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase HotTrackRowCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.HotTrackRowCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.HotTrackRowCellAppearance, value );
			}
		}

		#endregion // HotTrackRowCellAppearance

		#region ShouldSerializeHotTrackRowCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHotTrackRowCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.HotTrackRowCellAppearance ); 
		}

		#endregion // ShouldSerializeHotTrackRowCellAppearance
 
		#region ResetHotTrackRowCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetHotTrackRowCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.HotTrackRowCellAppearance ); 
		}

		#endregion // ResetHotTrackRowCellAppearance

		#region HotTrackRowSelectorAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the row selector of the row that's currently hot-tracked.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_HotTrackRowSelectorAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase HotTrackRowSelectorAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.HotTrackRowSelectorAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.HotTrackRowSelectorAppearance, value );
			}
		}

		#endregion // HotTrackRowSelectorAppearance

		#region ShouldSerializeHotTrackRowSelectorAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHotTrackRowSelectorAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.HotTrackRowSelectorAppearance ); 
		}

		#endregion // ShouldSerializeHotTrackRowSelectorAppearance
 
		#region ResetHotTrackRowSelectorAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetHotTrackRowSelectorAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.HotTrackRowSelectorAppearance ); 
		}

		#endregion // ResetHotTrackRowSelectorAppearance

		#endregion // NAS 5.3 HotTracking

		// SSP 8/26/06 - NAS 6.3
		// Added ReadOnlyCellAppearance to the override.
		// 
		#region ReadOnlyCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells that are read-only.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_ReadOnlyCellAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase ReadOnlyCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.ReadOnlyCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.ReadOnlyCellAppearance, value );
			}
		}

		#endregion // ReadOnlyCellAppearance

		#region ShouldSerializeReadOnlyCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeReadOnlyCellAppearance( ) 
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.ReadOnlyCellAppearance ); 
		}

		#endregion // ShouldSerializeReadOnlyCellAppearance
 
		#region ResetReadOnlyCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetReadOnlyCellAppearance( ) 
		{
			this.ResetAppearance( OverrideAppearanceIndex.ReadOnlyCellAppearance ); 
		}

		#endregion // ResetReadOnlyCellAppearance

		// SSP 8/17/05 BR05463
		// Added RowLayoutCellNavigationVertical property.
		// 
		#region RowLayoutCellNavigationVertical 

		#region RowLayoutCellNavigationVertical

		/// <summary>
		/// Specifies how cells are navigated when using up and down arrow keys.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridOverride_P_RowLayoutCellNavigationVertical")]
		[ LocalizedCategory("LC_Behavior") ]
		public RowLayoutCellNavigation RowLayoutCellNavigationVertical
		{
			get
			{
				return this.rowLayoutCellNavigationVertical;
			}
			set
			{
				if ( value != this.rowLayoutCellNavigationVertical )
				{
					GridUtils.ValidateEnum( typeof( RowLayoutCellNavigation ), value );

					this.rowLayoutCellNavigationVertical = value;

					this.NotifyPropChange( PropertyIds.RowLayoutCellNavigationVertical );
				}
			}
		}

		#endregion // RowLayoutCellNavigationVertical

		#region ShouldSerializeRowLayoutCellNavigationVertical

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowLayoutCellNavigationVertical( ) 
		{
			return RowLayoutCellNavigation.Default != this.rowLayoutCellNavigationVertical;
		}

		#endregion // ShouldSerializeRowLayoutCellNavigationVertical
 
		#region ResetRowLayoutCellNavigationVertical

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetRowLayoutCellNavigationVertical( ) 
		{
			this.RowLayoutCellNavigationVertical = RowLayoutCellNavigation.Default;
		}

		#endregion // ResetRowLayoutCellNavigationVertical

		#endregion // RowLayoutCellNavigationVertical 

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		#region NAS 6.1 Multi-cell Operations

		#region AllowMultiCellOperations

		/// <summary>
		/// Specifies if and which of the multi-cell operations the user is allowed to perform.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>AllowMultiCellOperations</b> specifies if and which multi-cell operation the user 
		/// is allowed to perform. See <see cref="AllowMultiCellOperation"/> enum for a full 
		/// listing of supported operations.
		/// </p>
		/// <p class="body">
		/// To perform any of these operations in code, use the control's <b>PerformAction</b> method.
		/// </p>
		/// <seealso cref="AllowMultiCellOperation"/>
		/// <seealso cref="UltraGrid.BeforeMultiCellOperation"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridOverride_P_AllowMultiCellOperations")]
		[ LocalizedCategory("LC_Behavior") ]
		[ Editor( typeof( Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		public AllowMultiCellOperation AllowMultiCellOperations
		{
			get
			{
				return this.allowMultiCellOperations;
			}
			set
			{
				if ( this.allowMultiCellOperations != value )
				{
					GridUtils.ValidateEnum( typeof( AllowMultiCellOperation ), value );

					this.allowMultiCellOperations = value;
					
					this.NotifyPropChange( PropertyIds.AllowMultiCellOperations );
				}
			}
		}

		#endregion // AllowMultiCellOperations

		#region ShouldSerializeAllowMultiCellOperations

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAllowMultiCellOperations( ) 
		{
			return AllowMultiCellOperation.Default != this.allowMultiCellOperations;
		}

		#endregion // ShouldSerializeAllowMultiCellOperations
 
		#region ResetAllowMultiCellOperations

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAllowMultiCellOperations( ) 
		{
			this.AllowMultiCellOperations = AllowMultiCellOperation.Default;
		}

		#endregion // ResetAllowMultiCellOperations

		#endregion // NAS 6.1 Multi-cell Operations

		// SSP 2/24/06 BR09715
		// Added MultiCellSelectionMode property to support snaking cell selection.
		// 
		#region MultiCellSelectionMode
		
		/// <summary>
		/// Specifies how multiple cells are range selected using mouse and keyboard.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>MultiCellSelectionMode</b> property specifies how multiple cells are range selected.
		/// </p>
		/// </remarks>
		[ LocalizedDescription( "LDR_UltraGridOverride_P_MultiCellSelectionMode" )]
		[ LocalizedCategory( "LC_Behavior" ) ]
		public MultiCellSelectionMode MultiCellSelectionMode
		{
			get
			{
				return this.multiCellSelectionMode;
			}
			set
			{
				if ( value != this.multiCellSelectionMode )
				{
					GridUtils.ValidateEnum( typeof( MultiCellSelectionMode ), value );

					this.multiCellSelectionMode = value;

					// MD 5/5/08
					// Found while making unit tests for 8.2 - Rotated Column Headers
					// We weren't firing any prop notification
					this.NotifyPropChange( PropertyIds.MultiCellSelectionMode );
				}
			}
		}

		#endregion // MultiCellSelectionMode

		#region ShouldSerializeMultiCellSelectionMode

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMultiCellSelectionMode( ) 
		{
			return MultiCellSelectionMode.Default != this.multiCellSelectionMode;
		}

		#endregion // ShouldSerializeMultiCellSelectionMode
 
		#region ResetMultiCellSelectionMode

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetMultiCellSelectionMode( ) 
		{
			this.MultiCellSelectionMode = MultiCellSelectionMode.Default;
		}

		#endregion // ResetMultiCellSelectionMode

		// SSP 10/19/07 BR25706 BR25707
		// Added ActiveRowCellAppearance. This is to let one be able to specify the 
		// appearance for cell of the active row.
		// 
		#region ActiveRowCellAppearance

		/// <summary>
		/// Determines the formatting attributes that will be applied to the cells of the active row.
		/// </summary>
		[LocalizedDescription( "LDR_UltraGridOverride_P_ActiveRowCellAppearance" )]
		[LocalizedCategory( "LC_Appearance" )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Visible )]
		public Infragistics.Win.AppearanceBase ActiveRowCellAppearance
		{
			get
			{
				return this.GetAppearance( OverrideAppearanceIndex.ActiveRowCellAppearance );
			}
			set
			{
				this.SetAppearance( OverrideAppearanceIndex.ActiveRowCellAppearance, value );
			}
		}

		#endregion // ActiveRowCellAppearance

		#region ShouldSerializeActiveRowCellAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
		/// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeActiveRowCellAppearance( )
		{
			return this.ShouldSerializeAppearance( OverrideAppearanceIndex.ActiveRowCellAppearance );
		}

		#endregion // ShouldSerializeActiveRowCellAppearance

		#region ResetActiveRowCellAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetActiveRowCellAppearance( )
		{
			this.ResetAppearance( OverrideAppearanceIndex.ActiveRowCellAppearance );
		}

		#endregion // ResetActiveRowCellAppearance

        // MBS 2/20/08 - RowEditTemplate NA2008 V2
        #region RowEditTemplateUIType

        /// <summary>
        /// Gets or sets when the grid will show the RowEditTemplate, if available.
        /// </summary>
        [Editor(typeof(Infragistics.Win.Design.DefaultableFlagsEnumerationUITypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridOverride_P_RowEditTemplateUIType")]
        public RowEditTemplateUIType RowEditTemplateUIType
        {
            get { return this.rowEditTemplateUIType; }
            set
            {
				// MD 5/5/08
				// Found while making unit tests for 8.2 - Rotated Column Headers
				// We should only process the new value if its different
				if ( this.rowEditTemplateUIType == value )
					return;

                GridUtils.ValidateEnum("RowEditTemplateUIType", typeof(RowEditTemplateUIType), value);
                this.rowEditTemplateUIType = value;
                this.NotifyPropChange(PropertyIds.RowEditTemplateUIType);
            }
        }

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeRowEditTemplateUIType()
        {
            return this.rowEditTemplateUIType != RowEditTemplateUIType.Default;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetRowEditTemplateUIType()
        {
            this.RowEditTemplateUIType = RowEditTemplateUIType.Default;
        }
        #endregion //RowEditTemplateUIType

        // MBS 5/6/08 - RowEditTemplate NA2008 V2
        #region RowEditTemplateRowSelectorImage

        /// <summary>
        /// Gets or sets the image places in the row selectors when the RowEditTemplateUIType includes RowSelectorImage.
        /// </summary>
        /// <seealso cref="RowEditTemplateUIType"/>
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridOverride_P_RowEditTemplateRowSelectorImage")]
        public Image RowEditTemplateRowSelectorImage
        {
            get { return this.rowEditTemplateRowSelectorImage; }
            set
            {
                if (this.rowEditTemplateRowSelectorImage == value)
                    return;

                this.rowEditTemplateRowSelectorImage = value;
                this.NotifyPropChange(PropertyIds.RowEditTemplateRowSelectorImage);
            }
        }

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeRowEditTemplateRowSelectorImage()
        {
            return this.rowEditTemplateRowSelectorImage != null;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetRowEditTemplateRowSelectorImage()
        {
            this.RowEditTemplateRowSelectorImage = null;
        }
        #endregion //RowEditTemplateRowSelectorImage

        // MD 4/17/08 - 8.2 - Rotated Column Headers
		#region 8.2 - Rotated Column Headers

		#region ColumnHeaderTextOrientation

		/// <summary>
		/// Gets or sets the default orientation of text in column headers.
		/// </summary>
		/// <value>The default orientation of text in the column headers, or null if no default has been set.</value>
		/// <seealso cref="GroupHeaderTextOrientation"/>
		/// <seealso cref="HeaderBase.TextOrientation"/>
		[LocalizedDescription("LD_UltraGridOverride_P_ColumnHeaderTextOrientation")]
        public TextOrientationInfo ColumnHeaderTextOrientation
		{
			get { return this.columnHeaderTextOrientation; }
			set
			{
				if ( Object.Equals( this.columnHeaderTextOrientation, value ) )
					return;

				this.columnHeaderTextOrientation = value;
				this.NotifyPropChange( PropertyIds.ColumnHeaderTextOrientation );
			}
		}

		/// <summary>
		/// Returns true if the <see cref="ColumnHeaderTextOrientation"/> property needs to be serialized.
		/// </summary>
		/// <returns>Returns true if this property is not set to its default value</returns>
		protected internal bool ShouldSerializeColumnHeaderTextOrientation()
		{
			return this.columnHeaderTextOrientation != null;
		}

		/// <summary>
		/// Resets the <see cref="ColumnHeaderTextOrientation"/> property to its default value.
		/// </summary>
		public void ResetColumnHeaderTextOrientation()
		{
			this.ColumnHeaderTextOrientation = null;
		}

		#endregion ColumnHeaderTextOrientation

		#region GroupHeaderTextOrientation

		/// <summary>
		/// Gets or sets the default orientation of text in group headers.
		/// </summary>
		/// <value>The default orientation of text in the group headers, or null if no default has been set.</value>
		/// <seealso cref="ColumnHeaderTextOrientation"/>
		/// <seealso cref="HeaderBase.TextOrientation"/>
		[LocalizedDescription("LD_UltraGridOverride_P_GroupHeaderTextOrientation")]
        public TextOrientationInfo GroupHeaderTextOrientation
		{
			get { return this.groupHeaderTextOrientation; }
			set
			{
				if ( Object.Equals( this.groupHeaderTextOrientation, value ) )
					return;

				this.groupHeaderTextOrientation = value;
				this.NotifyPropChange( PropertyIds.GroupHeaderTextOrientation );
			}
		}

		/// <summary>
		/// Returns true if the <see cref="GroupHeaderTextOrientation"/> property needs to be serialized.
		/// </summary>
		/// <returns>Returns true if this property is not set to its default value</returns>
		protected internal bool ShouldSerializeGroupHeaderTextOrientation()
		{
			return this.groupHeaderTextOrientation != null;
		}

		/// <summary>
		/// Resets the <see cref="GroupHeaderTextOrientation"/> property to its default value.
		/// </summary>
		public void ResetGroupHeaderTextOrientation()
		{
			this.GroupHeaderTextOrientation = null;
		}

		#endregion GroupHeaderTextOrientation 

		#endregion 8.2 - Rotated Column Headers

		// MD 9/23/08 - TFS6601
		#region ReserveSortIndicatorSpaceWhenAutoSizing

		/// <summary>
		/// Gets or sets the value indicating when to reserve space for the sort indicator during an auto-size operation.
		/// </summary>
		/// <seealso cref="UltraGridColumn.ReserveSortIndicatorSpaceWhenAutoSizing"/>
		[LocalizedDescription("LD_UltraGridOverride_P_ReserveSortIndicatorSpaceWhenAutoSizing")]
        public ReserveSortIndicatorSpaceWhenAutoSizing ReserveSortIndicatorSpaceWhenAutoSizing
		{
			get { return this.reserveSortIndicatorSpaceWhenAutoSizing; }
			set
			{
				if ( this.ReserveSortIndicatorSpaceWhenAutoSizing == value )
					return;

				GridUtils.ValidateEnum( typeof( ReserveSortIndicatorSpaceWhenAutoSizing ), value );
				this.reserveSortIndicatorSpaceWhenAutoSizing = value;
				this.NotifyPropChange( PropertyIds.ReserveSortIndicatorSpaceWhenAutoSizing );
			}
		}

		/// <summary>
		/// Returns true if <see cref="ReserveSortIndicatorSpaceWhenAutoSizing"/> is not set to its default value.
		/// </summary>
		/// <returns>Returns true if ReserveSortIndicatorSpaceWhenAutoSizing is not set to its default value.</returns>
		protected internal bool ShouldSerializeReserveSortIndicatorSpaceWhenAutoSizing()
		{
			return this.reserveSortIndicatorSpaceWhenAutoSizing != ReserveSortIndicatorSpaceWhenAutoSizing.Default;
		}

		/// <summary>
		/// Resets <see cref="ReserveSortIndicatorSpaceWhenAutoSizing"/> to its default value.
		/// </summary>
		[EditorBrowsable( EditorBrowsableState.Advanced )]
		public void ResetReserveSortIndicatorSpaceWhenAutoSizing()
		{
			this.ReserveSortIndicatorSpaceWhenAutoSizing = ReserveSortIndicatorSpaceWhenAutoSizing.Default;
		} 

		#endregion ReserveSortIndicatorSpaceWhenAutoSizing

        #region NAS 9.1 Header CheckBox

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxVisibility

        /// <summary>
        /// Determines whether the Header checkbox is visible.
        /// </summary>
        [LocalizedDescription("LD_HeaderCheckBoxVisibility")]
        public HeaderCheckBoxVisibility HeaderCheckBoxVisibility
        {
            get
            {
                return this.headerCheckBoxVisibility;
            }
            set
            {
                if (this.headerCheckBoxVisibility != value)
                {
                    GridUtils.ValidateEnum(typeof(HeaderCheckBoxVisibility), value);

                    this.headerCheckBoxVisibility = value;
                    // 1/14/08 CDS TFS12429 Put in a null check.
                    if (this.layout != null)
                        this.layout.DirtyColMetrics(false);
                    this.NotifyPropChange(PropertyIds.HeaderCheckBoxVisibility, null);
                }
            }
        }

        #endregion HeaderCheckBoxVisibility

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxAlignment

        /// <summary>
        /// Determines the position of the Header checkbox relative to the header caption.
        /// </summary>
        [LocalizedDescription("LD_HeaderCheckBoxAlignment")]
        public HeaderCheckBoxAlignment HeaderCheckBoxAlignment
        {
            get
            {
                return this.headerCheckBoxAlignment;
            }
            set
            {
                if (this.headerCheckBoxAlignment != value)
                {
                    GridUtils.ValidateEnum(typeof(HeaderCheckBoxAlignment), value);

                    this.headerCheckBoxAlignment = value;
                    // 1/14/08 CDS TFS12429 Put in a null check.
                    if (this.layout != null)
                        this.layout.DirtyColMetrics(false);
                    this.NotifyPropChange(PropertyIds.HeaderCheckBoxAlignment, null);
                }
            }
        }

        #endregion HeaderCheckBoxAlignment

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxSynchronization

        /// <summary>
        /// Determines which cells will be synchronized with the Header checkbox.
        /// </summary>
        [LocalizedDescription("LD_HeaderCheckBoxSynchronization")]
        public HeaderCheckBoxSynchronization HeaderCheckBoxSynchronization
        {
            get
            {
                return this.headerCheckBoxSynchronization;
            }
            set
            {
                if (this.headerCheckBoxSynchronization != value)
                {
                    GridUtils.ValidateEnum(typeof(HeaderCheckBoxSynchronization), value);

                    this.headerCheckBoxSynchronization = value;
                    this.NotifyPropChange(PropertyIds.HeaderCheckBoxSynchronization, null);
                }
            }
        }

        #endregion HeaderCheckBoxSynchronization

        // CDS NAS v9.1 Header CheckBox
        #region ShouldSerializeHeaderCheckBoxVisibility

        /// <summary>
        /// Returns true if the property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal bool ShouldSerializeHeaderCheckBoxVisibility()
        {
            return HeaderCheckBoxVisibility.Default != this.headerCheckBoxVisibility;
        }

        #endregion // ShouldSerializeHeaderCheckBoxVisibility

        // CDS NAS v9.1 Header CheckBox
        #region ResetHeaderCheckBoxVisibility

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetHeaderCheckBoxVisibility()
        {
            this.HeaderCheckBoxVisibility = HeaderCheckBoxVisibility.Default;
        }

        #endregion // ResetHeaderCheckBoxVisibility

        // CDS NAS v9.1 Header CheckBox
        #region ShouldSerializeHeaderCheckBoxAlignment

        /// <summary>
        /// Returns true if the property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal bool ShouldSerializeHeaderCheckBoxAlignment()
        {
            return HeaderCheckBoxAlignment.Default != this.headerCheckBoxAlignment;
        }

        #endregion // ShouldSerializeHeaderCheckBoxAlignment

        // CDS NAS v9.1 Header CheckBox
        #region ResetHeaderCheckBoxAlignment

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetHeaderCheckBoxAlignment()
        {
            this.HeaderCheckBoxAlignment = HeaderCheckBoxAlignment.Default;
        }

        #endregion // ResetHeaderCheckBoxAlignment

        // CDS NAS v9.1 Header CheckBox
        #region ShouldSerializeHeaderCheckBoxSynchronization

        /// <summary>
        /// Returns true if the property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal bool ShouldSerializeHeaderCheckBoxSynchronization()
        {
            return HeaderCheckBoxSynchronization.Default != this.headerCheckBoxSynchronization;
        }

        #endregion // ShouldSerializeHeaderCheckBoxSynchronization

        // CDS NAS v9.1 Header CheckBox
        #region ResetHeaderCheckBoxSynchronization

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetHeaderCheckBoxSynchronization()
        {
            this.HeaderCheckBoxSynchronization = HeaderCheckBoxSynchronization.Default;
        }

        #endregion // ResetHeaderCheckBoxSynchronization

        #endregion NAS 9.1 Header Checkbox

        // MBS 12/4/08 - NA9.1 Excel Style Filtering
        
        #region FilterUIProvider

        /// <summary>
        /// Gets or sets the UI provider that should be used for providing
        /// the drop-down UI for individual column filtering.
        /// </summary>
        [LocalizedDescription("LD_UltraGridOverride_P_FilterUIProvider")]
        public IFilterUIProvider FilterUIProvider
        {
            get { return this.filterUIProvider; }
            set
            {
                if (this.filterUIProvider == value)
                    return;

                this.filterUIProvider = value;
                this.NotifyPropChange(PropertyIds.FilterUIProvider);
            }
        }

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeFilterUIProvider()
        {
            return this.filterUIProvider != null;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetFilterUIProvider()
        {
            this.FilterUIProvider = null;
        }
        #endregion //FilterUIProvider

		// MD 12/8/08 - 9.1 - Column Pinning Right
		#region FixHeadersOnRight

		/// <summary>
		/// Gets or sets a value which indicates whether the columns or groups will be fixed to the right side of the grid when they are fixed.
		/// </summary>
		/// <exception cref="InvalidEnumArgumentException">
		/// The assigned value is not defined in the <see cref="DefaultableBoolean"/> enumeration.
		/// </exception>
		/// <seealso cref="HeaderBase.FixOnRight"/>
		/// <seealso cref="HeaderBase.FixOnRightResolved"/>
		public DefaultableBoolean FixHeadersOnRight
		{
			get { return this.fixHeadersOnRight; }
			set
			{
				if ( this.fixHeadersOnRight == value )
					return;

				if ( Enum.IsDefined( typeof( DefaultableBoolean ), value ) == false )
					throw new InvalidEnumArgumentException( "value", (int)value, typeof( DefaultableBoolean ) );

				this.fixHeadersOnRight = value;
				this.NotifyPropChange( PropertyIds.FixHeadersOnRight );
			}
		}

		/// <summary>
		/// Returns true if the <see cref="UltraGridOverride.FixHeadersOnRight"/> property needs to be serialized.
		/// </summary>
		/// <returns>Returns true if this property is not set to its default value</returns>
		protected internal bool ShouldSerializeFixHeadersOnRight()
		{
			return this.fixHeadersOnRight != DefaultableBoolean.Default;
		}

		/// <summary>
		/// Resets the <see cref="UltraGridOverride.FixHeadersOnRight"/> property to its default value.
		/// </summary>
		public void ResetFixHeadersOnRight()
		{
			this.FixHeadersOnRight = DefaultableBoolean.Default;
		} 

		#endregion FixHeadersOnRight

        #region 9.2 Drop Indicators

        // CDS 9.2 Column Moving Indicators
        #region DragDropIndicatorSettings

        /// <summary>
        /// Gets a DragDropIndicatorsSettings instance which contains the custom settings for the drag-drop indicators
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [LocalizedDescription("LD_UltraGridOverride_P_DragDropIndicatorSettings")]
        [LocalizedCategory("LC_Appearance")]
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_ColumnMovingIndicators)
        ]
        public Infragistics.Win.DragDropIndicatorSettings DragDropIndicatorSettings
        {
            get
            {
                if (null == this.dragDropIndicatorSettings)
                {
                    this.dragDropIndicatorSettings = new Infragistics.Win.DragDropIndicatorSettings();

                    // hook up the prop change notifications
                    //
                    this.dragDropIndicatorSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }

                return this.dragDropIndicatorSettings;
            }
        }

        #endregion DragDropIndicatorSettings

        #region HasDragDropIndicatorSettings

        /// <summary>
        /// Returns true if a DragDropIndicatorSettings object has been created.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasDragDropIndicatorSettings
        {
            get
            {
                return null != this.dragDropIndicatorSettings;
            }
        }

        #endregion HasDragDropIndicatorSettings

        // CDS 9.2 Column Moving Indicators
        #region DragDropIndicatorSettings serialization

        internal bool ShouldSerializeDragDropIndicatorSettings()
        {
            return (this.HasDragDropIndicatorSettings && 
                this.dragDropIndicatorSettings.ShouldSerialize());
        }

        // CDS 9.2 Column Moving Indicators
        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_ColumnMovingIndicators)
        ]
        public void ResetDragDropIndicatorSettings()
        {
            if (this.HasDragDropIndicatorSettings)
                this.dragDropIndicatorSettings.Reset();
        }

        #endregion DragDropIndicatorSettings serialization

        #endregion 9.2 Drop Indicators

        // MBS 3/31/09 - NA9.2 CellBorderColor
        #region ActiveCellBorderThickness

        /// <summary>
        /// Gets or sets the thickness of the border that is drawn over the active cell.
        /// </summary>
        /// <remarks>
        /// Setting this value will cause a border to be drawn over the existing cell borders with a solid color
        /// determined by the cell's resolved border color.  Setting this property to 1, for example, will simply 
        /// cause the <see cref="UltraGrid.ActiveCell"/>s borders to be drawn on all sides; any larger values will
        /// result in a larger rectangle being drawn around the cell.
        /// </remarks>
        [LocalizedDescription("LD_UltraGridOverride_P_ActiveCellBorderThickness")]
        public int ActiveCellBorderThickness
        {
            get { return this.activeCellBorderThickness; }
            set
            {
                if (this.activeCellBorderThickness == value)
                    return;

                if (value < 0)
                    throw new ArgumentOutOfRangeException("value", SR.GetString("LE_UltraGrid_InvalidActiveCellBorderThickness"));

                this.activeCellBorderThickness = value;

                if(this.layout != null)
                    StyleUtils.DirtyCachedPropertyVals(this.layout);

                this.NotifyPropChange(PropertyIds.ActiveCellBorderThickness);
            }
        }

        /// <summary>
        /// Returns true if the <see cref="UltraGridOverride.ActiveCellBorderThickness"/> property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected internal bool ShouldSerializeActiveCellBorderThickness()
        {
            // MBS 5/15/09 - TFS17562
            // return this.activeCellBorderThickness != 0;
            return this.activeCellBorderThickness != 1;
        }

        /// <summary>
        /// Resets the <see cref="UltraGridOverride.ActiveCellBorderThickness"/> property to its default value.
        /// </summary>
        public void ResetActiveCellBorderThickness()
        {
            // MBS 5/15/09 - TFS17562
            //this.ActiveCellBorderThickness = 0;
            this.ActiveCellBorderThickness = 1;
        } 
        #endregion //ActiveCellBorderThickness

        // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
        #region GroupByRowConnectorAppearance

        /// <summary>
        /// Determines the appearance of the area that exists to the left of the child rows of a GroupByRow.
        /// </summary>
        [LocalizedDescription("LD_UltraGridOverride_P_GroupByRowConnectorAppearance")]
        [LocalizedCategory("LC_Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Infragistics.Win.AppearanceBase GroupByRowConnectorAppearance
        {
            get
            {
                return this.GetAppearance(OverrideAppearanceIndex.GroupByRowConnectorAppearance);
            }

            set
            {
                this.SetAppearance(OverrideAppearanceIndex.GroupByRowConnectorAppearance, value);
            }
        }
        /// <summary>
        /// Returns true if an GroupByRowConnectorAppearance object has been created.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasGroupByRowConnectorAppearance
        {
            get
            {
                return this.HasAppearance(OverrideAppearanceIndex.GroupByRowConnectorAppearance);
            }
        }

        /// <summary>
        /// Returns true if the key needs to be serialized (not null )
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeGroupByRowConnectorAppearance()
        {
            return this.ShouldSerializeAppearance(OverrideAppearanceIndex.GroupByRowConnectorAppearance);
        }

        /// <summary>
        /// Resets GroupByRowConnector's Appearance
        /// </summary>
        public void ResetGroupByRowConnectorAppearance()
        {
            this.ResetAppearance(OverrideAppearanceIndex.GroupByRowConnectorAppearance);
        }
        #endregion //GroupByRowConnectorAppearance

        // MBS 6/19/09 - TFS18639
        #region FilterDropDownOperands

        /// <summary>
        /// Gets or sets the items that will be displayed by the grid when showing a dropdown filter list.
        /// </summary>        
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>The <b>Errors</b> and <b>NonErrors</b> options will only be shown in the list if the
        /// <see cref="SupportDataErrorInfo"/> property enables the error info on cells.
        /// </p>
        /// </remarks>
        /// <seealso cref="FilterUIType"/>
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridOverride_P_FilterDropDownOperands")]
        [Editor(typeof(Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public FilterOperandDropDownItems FilterOperandDropDownItems
        {
            get { return this.filterOperandDropDownItems; }
            set
            {
                if (this.filterOperandDropDownItems == value)
                    return;

                GridUtils.ValidateEnum("FilterDropDownOperands", typeof(FilterOperandDropDownItems), value);
                this.filterOperandDropDownItems = value;
                this.NotifyPropChange(PropertyIds.FilterOperandDropDownItems);
            }
        }

        /// <summary>
        /// Returns true if the FilterDropDownOperands property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeFilterOperandDropDownItems()
        {
            return this.filterOperandDropDownItems != FilterOperandDropDownItems.Default;
        }

        /// <summary>
        /// Resets the <see cref="UltraGridOverride.FilterOperandDropDownItems"/> property to its default value.
        /// </summary>
        public void ResetFilterOperandDropDownItems()
        {
            this.FilterOperandDropDownItems = FilterOperandDropDownItems.Default;
        }
        #endregion //FilterDropDownOperands

        // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
        #region ActiveCellRowSelectorAppearance

        /// <summary>
        /// Gets or sets the appearance of the row selector associated with the row of the active cell.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>If the row selectors are being drawn with themes, such as if the <see cref="RowSelectorStyle"/>
        /// is set to <b>XPThemed</b>, then this appearance will not be seen; this behavior is standard across the
        /// themed rendering of controls.  In order to get around this, the <see cref="Infragistics.Win.Appearance.ThemedElementAlpha"/>
        /// property can be set to <b>Transparent</b>.
        /// </p>
        /// </remarks>
        [LocalizedDescription("LD_UltraGridOverride_P_ActiveCellRowSelectorAppearance")]
        [LocalizedCategory("LC_Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Infragistics.Win.AppearanceBase ActiveCellRowSelectorAppearance
        {
            get
            {
                return this.GetAppearance(OverrideAppearanceIndex.ActiveCellRowSelectorAppearance);
            }

            set
            {
                this.SetAppearance(OverrideAppearanceIndex.ActiveCellRowSelectorAppearance, value);
            }
        }

        /// <summary>
        /// Returns true if an ActiveCellRowSelectorAppearance object has been created.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasActiveCellRowSelectorAppearance
        {
            get
            {
                return this.HasAppearance(OverrideAppearanceIndex.ActiveCellRowSelectorAppearance);
            }
        }

        /// <summary>
        /// Returns true if the key needs to be serialized (not null )
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeActiveCellRowSelectorAppearance()
        {
            return this.ShouldSerializeAppearance(OverrideAppearanceIndex.ActiveCellRowSelectorAppearance);
        }

        /// <summary>
        /// Resets ActiveCellRowSelectorAppearance
        /// </summary>
        public void ResetActiveCellRowSelectorAppearance()
        {
            this.ResetAppearance(OverrideAppearanceIndex.ActiveCellRowSelectorAppearance);
        }
        #endregion //ActiveCellRowSelectorAppearance
        //
        #region ActiveCellColumnHeaderAppearance

        /// <summary>
        /// Gets or sets the appearance of the column header associated with the row of the active cell.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>If the column headers are being drawn with themes, such as if the <see cref="HeaderStyle"/>
        /// is set to <b>XPThemed</b>, then this appearance will not be seen; this behavior is standard across the
        /// themed rendering of controls.  In order to get around this, the <see cref="Infragistics.Win.Appearance.ThemedElementAlpha"/>
        /// property can be set to <b>Transparent</b>.
        /// </p>
        /// </remarks>
        [LocalizedDescription("LD_UltraGridOverride_P_ActiveCellColumnHeaderAppearance")]
        [LocalizedCategory("LC_Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Infragistics.Win.AppearanceBase ActiveCellColumnHeaderAppearance
        {
            get
            {
                return this.GetAppearance(OverrideAppearanceIndex.ActiveCellColumnHeaderAppearance);
            }

            set
            {
                this.SetAppearance(OverrideAppearanceIndex.ActiveCellColumnHeaderAppearance, value);
            }
        }

        /// <summary>
        /// Returns true if an ActiveCellColumnHeaderAppearance object has been created.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasActiveCellColumnHeaderAppearance
        {
            get
            {
                return this.HasAppearance(OverrideAppearanceIndex.ActiveCellColumnHeaderAppearance);
            }
        }

        /// <summary>
        /// Returns true if the key needs to be serialized (not null )
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeActiveCellColumnHeaderAppearance()
        {
            return this.ShouldSerializeAppearance(OverrideAppearanceIndex.ActiveCellColumnHeaderAppearance);
        }

        /// <summary>
        /// Resets ActiveCellColumnHeaderAppearance
        /// </summary>
        public void ResetActiveCellColumnHeaderAppearance()
        {
            this.ResetAppearance(OverrideAppearanceIndex.ActiveCellColumnHeaderAppearance);
        }
        #endregion //ActiveCellColumnHeaderAppearance

        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
        #region ActiveAppearancesEnabled

        /// <summary>
        /// Gets or sets a value that determines whether Appearances for the active state are included in the appearance resolution order.
        /// </summary>
        [LocalizedDescription("LD_UltraGridOverride_P_ActiveAppearancesEnabled")]
        [LocalizedCategory("LC_Appearance")]
        [InfragisticsFeature(
        Version = FeatureInfo.Version_9_2,
        FeatureName = FeatureInfo.FeatureName_CellActiveAppearance)
        ]
        public DefaultableBoolean ActiveAppearancesEnabled
        {
            get
            {
                return this.activeAppearancesEnabled;
            }
            set
            {
                if (value != this.activeAppearancesEnabled)
                {
                    // test that the value is in range
                    //
                    if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
                        throw new InvalidEnumArgumentException("ActiveAppearancesEnabled", (int)value, typeof(DefaultableBoolean)); ;

                    this.activeAppearancesEnabled = value;
                    this.NotifyPropChange(PropertyIds.ActiveAppearancesEnabled, null);
                }
            }
        }

        /// <summary>
        /// Returns true if the key needs to be serialized (not null )
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeActiveAppearancesEnabled()
        {
            return (this.activeAppearancesEnabled != DefaultableBoolean.Default);
        }

        /// <summary>
        /// Reset AllowDelete to its default value (default).
        /// </summary>
        public void ResetActiveAppearancesEnabled()
        {
            this.ActiveAppearancesEnabled = DefaultableBoolean.Default;
        }

        #endregion ActiveAppearancesEnabled
        //
        #region SelectedAppearancesEnabled

        /// <summary>
        /// Gets or sets a value that determines whether Appearances for the active state are included in the appearance resolution order.
        /// </summary>
        [LocalizedDescription("LD_UltraGridOverride_P_SelectedAppearancesEnabled")]
        [LocalizedCategory("LC_Appearance")]
        [InfragisticsFeature(
        Version = FeatureInfo.Version_9_2,
        FeatureName = FeatureInfo.FeatureName_CellActiveAppearance)
        ]
        public DefaultableBoolean SelectedAppearancesEnabled
        {
            get
            {
                return this.selectedAppearancesEnabled;
            }
            set
            {
                if (value != this.selectedAppearancesEnabled)
                {
                    // test that the value is in range
                    //
                    if (!Enum.IsDefined(typeof(DefaultableBoolean), value))
                        throw new InvalidEnumArgumentException("SelectedAppearancesEnabled", (int)value, typeof(DefaultableBoolean)); ;

                    this.selectedAppearancesEnabled = value;
                    this.NotifyPropChange(PropertyIds.SelectedAppearancesEnabled, null);
                }
            }
        }

        /// <summary>
        /// Returns true if the key needs to be serialized (not null )
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeSelectedAppearancesEnabled()
        {
            return (this.selectedAppearancesEnabled != DefaultableBoolean.Default);
        }

        /// <summary>
        /// Reset AllowDelete to its default value (default).
        /// </summary>
        public void ResetSelectedAppearancesEnabled()
        {
            this.SelectedAppearancesEnabled = DefaultableBoolean.Default;
        }

        #endregion SelectedAppearancesEnabled    

        // MBS 8/7/09 - TFS18607
        #region AddRowEditNotificationInterface

        /// <summary>
        /// Gets or sets the order of the interfaces that the grid looks for when committing or cancelling the addition
        /// of a new row to the data source.
        /// </summary>
        /// <remarks>
        /// <p class="body">
        /// This property determines whether the grid will look for the <see cref="UltraGridRow.ListObject"/>'s implementation
        /// of the <see cref="IEditableObject"/> interface and/or the containing list's <see cref="ICancelAddNew"/> to detemine
        /// which interface should take priority when a new row is cancelled or committed, or if an interface implementation
        /// should be ignored entirely if the other interface implementation is present.  For example, if the property is set
        /// to <b>IEditableObjectAndICancelAddNew</b>, the default resolved behavior, 
        /// then the grid will call the <i>ICancelAddNew</i> implementation after calling the <i>IEditableObject</i> implementation.
        /// </p>        
        /// </remarks>
        public AddRowEditNotificationInterface AddRowEditNotificationInterface
        {
            get { return this.addRowEditNotificationInterface; }
            set
            {
                if (this.addRowEditNotificationInterface == value)
                    return;
                
                GridUtils.ValidateEnum(typeof(AddRowEditNotificationInterface), value);
                this.addRowEditNotificationInterface = value;
                this.NotifyPropChange(PropertyIds.AddRowEditNotificationInterface);
            }
        }

        /// <summary>
        /// Returns true if <see cref="AddRowEditNotificationInterface"/> is not set to its default value.
        /// </summary>
        /// <returns>Returns true if AddRowEditNotificationInterface is not set to its default value.</returns>
        protected internal bool ShouldSerializeAddRowEditNotificationInterface()
        {
            return this.addRowEditNotificationInterface != AddRowEditNotificationInterface.Default;
        }

        /// <summary>
        /// Resets <see cref="AddRowEditNotificationInterface"/> to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetAddRowEditNotificationInterface()
        {
            this.AddRowEditNotificationInterface = AddRowEditNotificationInterface.Default;
        } 
        #endregion //EndEditNotificationInterfaces
    }
}
