#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using Infragistics.Win;
using Infragistics.Shared;
using System.IO;
using System.Diagnostics;
using System.Drawing.Design;

namespace Infragistics.Win.UltraWinGrid
{
	internal class ColumnEditorDialog : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private Infragistics.Win.UltraWinGrid.ColumnsCollection columns = null;
		private System.Windows.Forms.Button RemoveButton;
		private System.Windows.Forms.Label PropertyLabel;
		private UltraPropPagePropertyGrid ColumnPropertyGrid;
		private System.Windows.Forms.Button AddButton;
		private UltraGridColumnListBox ColumnListBox;
		private System.Windows.Forms.Label ColumnLabel;
		private System.Windows.Forms.Button OkButton;
		private System.Windows.Forms.Button CloseButton;
		
		// MRS 10/20/04 - UWG3016 - no longer needed
		// private System.IO.MemoryStream memoryStream = null;

		private System.Windows.Forms.ContextMenu PropertyGridContextMenu;
		private System.Windows.Forms.MenuItem menuItemShowToolBar;
		private System.Windows.Forms.MenuItem menuItemShowDescription;
		private System.Windows.Forms.MenuItem menuItemReset;
		private System.Windows.Forms.MenuItem menuItemSeparator;

		private Infragistics.Shared.SubObjectPropChangeEventHandler subObjectPropChangeEventHandler = null;

		// MRS 10/20/04 - UWG3016
		private DesignerTransaction transaction = null;
		private IComponentChangeService componentService = null;
		private IDesignerHost designerHostService = null;
		private bool wasDesignerChangeNotificationsDisabled = false;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="columns">ColumnsCollection</param>
		internal ColumnEditorDialog(Infragistics.Win.UltraWinGrid.ColumnsCollection columns)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			// JJD 7/14/03
			// Localized strings
			this.OkButton.Text = SR.GetString( "LDR_ColumnEditorDialog_OK" ); // "OK";
			this.menuItemReset.Text = SR.GetString( "LDR_ColumnEditorDialog_Reset" ); // "Reset";
			this.menuItemShowToolBar.Text = SR.GetString( "LDR_ColumnEditorDialog_ShowToolBar" ); // "Show ToolBar";
			this.menuItemShowDescription.Text = SR.GetString( "LDR_ColumnEditorDialog_ShowDescription" ); // "Show Description";
			this.RemoveButton.Text = SR.GetString( "LDR_ColumnEditorDialog_Remove" ); // "&Remove";
			this.CloseButton.Text = SR.GetString( "LDR_ColumnEditorDialog_Cancel" ); // "Cancel";
			this.AddButton.Text = SR.GetString( "LDR_ColumnEditorDialog_Add" ); // "&Add";
			this.PropertyLabel.Text = SR.GetString( "LDR_ColumnEditorDialog_Properties" ); // "&Properties:";
			this.ColumnLabel.Text = SR.GetString( "LDR_ColumnEditorDialog_Members" ); // "&Members:";
			this.Text = SR.GetString( "LDR_ColumnEditorDialog_Title" ); // "Columns Collection Editor";

			// make a copy of the columns collection for the dialog's use
			this.columns = columns;

			// Initialize the property grid's component
			this.ColumnPropertyGrid.Initialize( this.columns.Band.Layout.Grid );

			// MRS 10/20/04 - UWG3016 - no longer needed
//			// save the layout to a memory stream
//			memoryStream = new MemoryStream();
//			this.columns.Band.Layout.Save(memoryStream);

			// add the items to the columns collection
			for ( int i = 0; i < this.columns.Count; i++ )
			{
				Infragistics.Win.UltraWinGrid.UltraGridColumn col = this.columns[i];

				// JAS 3/31/05 BR03039 - Don't show chaptered columns.
				// I added the if().
				//
				if( col != null && ! col.IsChaptered )
				{
					this.ColumnListBox.Add(col);

					// hook into the notifications
					col.SubObjectPropChanged += this.SubObjectPropChangeEventHandler;
				}
			}

			// hide the control box
			this.ControlBox = false;

			if (this.ColumnListBox.Items.Count > 0)
				this.ColumnListBox.SelectedIndex = 0;
				
			// MRS 10/20/04 - UWG3016
			this.BeginTransaction();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.PropertyGridContextMenu = new System.Windows.Forms.ContextMenu();
			this.menuItemReset = new System.Windows.Forms.MenuItem();
			this.menuItemSeparator = new System.Windows.Forms.MenuItem();
			this.menuItemShowToolBar = new System.Windows.Forms.MenuItem();
			this.menuItemShowDescription = new System.Windows.Forms.MenuItem();
			this.OkButton = new System.Windows.Forms.Button();
			this.RemoveButton = new System.Windows.Forms.Button();
			this.CloseButton = new System.Windows.Forms.Button();
			this.AddButton = new System.Windows.Forms.Button();
			this.PropertyLabel = new System.Windows.Forms.Label();
			this.ColumnLabel = new System.Windows.Forms.Label();
			this.ColumnPropertyGrid = new UltraPropPagePropertyGrid();
			this.ColumnListBox = new Infragistics.Win.UltraWinGrid.UltraGridColumnListBox();
			this.SuspendLayout();
			// 
			// PropertyGridContextMenu
			// 
			this.PropertyGridContextMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									this.menuItemReset,
																									this.menuItemSeparator,
																									this.menuItemShowToolBar,
																									this.menuItemShowDescription});
			this.PropertyGridContextMenu.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.PropertyGridContextMenu.Popup += new System.EventHandler(this.PropertyGridContextMenu_Popup);
			// 
			// menuItemReset
			// 
			this.menuItemReset.Enabled = false;
			this.menuItemReset.Index = 0;
			this.menuItemReset.Text = "Reset";
			this.menuItemReset.Click += new System.EventHandler(this.menuItemReset_Click);
			// 
			// menuItemSeparator
			// 
			this.menuItemSeparator.Index = 1;

			this.menuItemSeparator.Text = "-";
			// 
			// menuItemShowToolBar
			// 
			this.menuItemShowToolBar.Checked = true;
			this.menuItemShowToolBar.Index = 2;
			this.menuItemShowToolBar.Text = "Show ToolBar";
			this.menuItemShowToolBar.Click += new System.EventHandler(this.menuItemShowToolBar_Click);
			// 
			// menuItemShowDescription
			// 
			this.menuItemShowDescription.Index = 3;
			this.menuItemShowDescription.Text = "Show Description";
			this.menuItemShowDescription.Click += new System.EventHandler(this.menuItemShowDescription_Click);
			// 
			// OkButton
			// 
			this.OkButton.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.OkButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.OkButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.OkButton.Location = new System.Drawing.Point(305, 268);
			this.OkButton.Name = "OkButton";
			this.OkButton.Size = new System.Drawing.Size(76, 24);
			this.OkButton.TabIndex = 6;
			this.OkButton.Text = "OK";
			this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
			// 
			// RemoveButton
			// 
			this.RemoveButton.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.RemoveButton.Enabled = false;
			this.RemoveButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.RemoveButton.Location = new System.Drawing.Point(110, 228);
			this.RemoveButton.Name = "RemoveButton";
			this.RemoveButton.Size = new System.Drawing.Size(84, 24);
			this.RemoveButton.TabIndex = 3;

			this.RemoveButton.Text = "&Remove";
			this.RemoveButton.Click += new System.EventHandler(this.RemoveButton_Click);
			// 
			// CloseButton
			// 
			this.CloseButton.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.CloseButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CloseButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.CloseButton.Location = new System.Drawing.Point(388, 268);
			this.CloseButton.Name = "CloseButton";
			this.CloseButton.Size = new System.Drawing.Size(76, 24);
			this.CloseButton.TabIndex = 7;
			this.CloseButton.Text = "Cancel";
			this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
			// 
			// AddButton
			// 
			this.AddButton.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.AddButton.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.AddButton.Location = new System.Drawing.Point(10, 228);
			this.AddButton.Name = "AddButton";
			this.AddButton.Size = new System.Drawing.Size(84, 24);
			this.AddButton.TabIndex = 2;
			this.AddButton.Text = "&Add";
			this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
			// 
			// PropertyLabel
			// 
			this.PropertyLabel.AutoSize = true;
			this.PropertyLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.PropertyLabel.Location = new System.Drawing.Point(208, 3);
			this.PropertyLabel.Name = "PropertyLabel";
			this.PropertyLabel.Size = new System.Drawing.Size(59, 13);
			this.PropertyLabel.TabIndex = 4;
			this.PropertyLabel.Text = "&Properties:";
			// 
			// ColumnLabel
			// 
			this.ColumnLabel.AutoSize = true;
			this.ColumnLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.ColumnLabel.Location = new System.Drawing.Point(10, 3);
			this.ColumnLabel.Name = "ColumnLabel";
			this.ColumnLabel.Size = new System.Drawing.Size(55, 13);
			this.ColumnLabel.TabIndex = 0;
			this.ColumnLabel.Text = "&Members:";
			// 
			// ColumnPropertyGrid
			// 
			this.ColumnPropertyGrid.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.ColumnPropertyGrid.CommandsVisibleIfAvailable = true;
			this.ColumnPropertyGrid.ContextMenu = this.PropertyGridContextMenu;
			this.ColumnPropertyGrid.HelpVisible = false;
			this.ColumnPropertyGrid.LargeButtons = false;
			this.ColumnPropertyGrid.LineColor = System.Drawing.SystemColors.ScrollBar;
			this.ColumnPropertyGrid.Location = new System.Drawing.Point(208, 18);
			this.ColumnPropertyGrid.Name = "ColumnPropertyGrid";
			this.ColumnPropertyGrid.PropertySort = System.Windows.Forms.PropertySort.Alphabetical;
			this.ColumnPropertyGrid.Size = new System.Drawing.Size(256, 234);
			this.ColumnPropertyGrid.TabIndex = 5;
			this.ColumnPropertyGrid.Text = "propertyGrid1";
			this.ColumnPropertyGrid.ViewBackColor = System.Drawing.SystemColors.Window;
			this.ColumnPropertyGrid.ViewForeColor = System.Drawing.SystemColors.WindowText;
			// 
			// ColumnListBox
			// 
			this.ColumnListBox.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left);
			this.ColumnListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			this.ColumnListBox.IntegralHeight = false;
			this.ColumnListBox.Location = new System.Drawing.Point(10, 18);
			this.ColumnListBox.Name = "ColumnListBox";
			this.ColumnListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.ColumnListBox.Size = new System.Drawing.Size(184, 206);
			this.ColumnListBox.TabIndex = 1;
			this.ColumnListBox.SelectedValueChanged += new System.EventHandler(this.ColumnListBox_SelectedValueChanged);
			this.ColumnListBox.SelectedIndexChanged += new System.EventHandler(this.ColumnListBox_SelectedIndexChanged);
			// 
			// ColumnEditorDialog
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(471, 301);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.RemoveButton,
																		  this.PropertyLabel,
																		  this.ColumnPropertyGrid,
																		  this.AddButton,
																		  this.ColumnListBox,
																		  this.ColumnLabel,
																		  this.OkButton,
																		  this.CloseButton});
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(479, 328);
			this.Name = "ColumnEditorDialog";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Columns Collection Editor";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.ColumnEditorDialog_Closing);
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.ColumnEditorDialog_Paint);
			this.ResumeLayout(false);

		}
		#endregion

		private void ColumnEditorDialog_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			// we needed to draw the line on the form above the ok and cancel buttons
			//

			const int buffer = 7;

			if (this.WindowState == FormWindowState.Minimized || this.ClientSize.Width <=  (buffer*2))
				return;

			int y = this.ColumnPropertyGrid.Bounds.Bottom + buffer;

			Point ptFrom = new Point(buffer, y);
			Point ptTo = new Point(this.ClientSize.Width - buffer, y);

			e.Graphics.DrawLine(System.Drawing.SystemPens.ControlDark, ptFrom, ptTo);

			// move down one pixel to draw the next part of the line
			ptFrom.Y += 1;
			ptTo.Y += 1;

			e.Graphics.DrawLine(System.Drawing.SystemPens.ControlLightLight, ptFrom, ptTo);
		}

		private void RemoveButton_Click(object sender, System.EventArgs e)
		{
			// iterate through the selected items and remove them
			while (this.ColumnListBox.SelectedItems.Count > 0)
			{
				// get the selected object
				Infragistics.Win.UltraWinGrid.UltraGridColumn col = this.ColumnListBox.SelectedItems[0] as Infragistics.Win.UltraWinGrid.UltraGridColumn;

				Debug.Assert( !col.IsBound, "Attempted to remove a bound column." );

				if (col.IsBound)
					continue;

				// unhook from notification
				col.SubObjectPropChanged -= this.SubObjectPropChangeEventHandler;

				// remove the item from our copy of the collection
				this.columns.Remove(col);

				// remove the item from the listbox
				this.ColumnListBox.Remove(col);
			}
		}

		private void AddButton_Click(object sender, System.EventArgs e)
		{
			// create a new column
			Infragistics.Win.UltraWinGrid.UltraGridColumn col = this.columns.Add();

			this.ColumnListBox.ClearSelected();

			this.ColumnListBox.SelectedIndex = this.ColumnListBox.Add( col );

			// hook into the notifications
			col.SubObjectPropChanged += this.SubObjectPropChangeEventHandler;
		}

		private void OkButton_Click(object sender, System.EventArgs e)
		{
			// we've been making changes all along so just close the dialog
					
			// MRS 10/20/04 - UWG3016
			this.CommitTransaction();
		}

		private void ColumnListBox_SelectedValueChanged(object sender, System.EventArgs e)
		{
			this.VerifyRemoveEnabledState();
			this.UpdatePropertyGridCollection();
		}

		private void ColumnListBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			this.VerifyRemoveEnabledState();
			this.UpdatePropertyGridCollection();
		}

		private void UpdatePropertyGridCollection()
		{
			this.ColumnPropertyGrid.SelectedObject = null;

			if (this.ColumnListBox.SelectedItems.Count == 0)
				return;

			ArrayList objects = new ArrayList();

			foreach (object obj in this.ColumnListBox.SelectedItems)
			{
				 objects.Add(obj);
			}

			this.ColumnPropertyGrid.SelectedObjects = objects.ToArray();
		}

		private void VerifyRemoveEnabledState()
		{
			// assume the remove button will be enabled;
			bool enabled = true;

			// if there are no items, disable the remove button
			if (this.ColumnListBox.SelectedItems.Count == 0)
			{
				if (this.ColumnListBox.SelectedItem == null)
					enabled = false;
				else
				{
					Infragistics.Win.UltraWinGrid.UltraGridColumn col = this.ColumnListBox.SelectedItem as Infragistics.Win.UltraWinGrid.UltraGridColumn;

					if (col == null)
						enabled = false;
					else
						enabled = !col.IsBound;
				}
			}
			else
			{
				foreach (object obj in this.ColumnListBox.SelectedItems)
				{
					Infragistics.Win.UltraWinGrid.UltraGridColumn col = obj as Infragistics.Win.UltraWinGrid.UltraGridColumn;

					if (col == null)
						continue;

					// if the column is bound, the remove button must be disabled
					if (col.IsBound)
					{
						enabled = false;
						break;
					}
				}
			}

			this.RemoveButton.Enabled = enabled;
		}

		private void CloseButton_Click(object sender, System.EventArgs e)
		{
			// MRS 10/20/04 - UWG3016
//			Debug.Assert(memoryStream != null, "Null memory stream. Unable to restore layout.");
//
//			if (this.memoryStream != null)
//			{
//				// AS 9/19/03 UWG1733 begin
//				// The load is going to cause a lot of
//				// property change notifications which will
//				// slow things down. Disable the notifications
//				// while we are loading.
//				//
//				UltraGridBase grid = this.columns.Band.Layout.Grid;
//				bool wasNotificationDisabled = true;
//
//				if (grid != null)
//				{
//					wasNotificationDisabled = grid.DesignerChangeNotificationsDisabled;
//					grid.DesignerChangeNotificationsDisabled = true;
//				}
//
//				Cursor.Current = Cursors.WaitCursor;
//				// AS 9/19/03 UWG1733 end
//
//				try
//				{
//					memoryStream.Position = 0;
//					this.columns.Band.Layout.Reset();
//					this.columns.Band.Layout.Load(memoryStream);
//				}
//				finally
//				{
//					// AS 9/19/03 UWG1733
//					// Reset the cursor and reset the notification
//					// flag if it wasn't turned off before.
//					//
//					Cursor.Current = Cursors.Default;
//
//					if (!wasNotificationDisabled && grid != null)
//						grid.DesignerChangeNotificationsDisabled = false;
//				}
//			}
			this.CancelTransaction();	
		}

		private void ColumnEditorDialog_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			foreach( Infragistics.Win.UltraWinGrid.UltraGridColumn column in this.columns )
			{
				column.SubObjectPropChanged -= this.SubObjectPropChangeEventHandler;
			}
		}

		private SubObjectPropChangeEventHandler SubObjectPropChangeEventHandler
		{
			get
			{
				if (this.subObjectPropChangeEventHandler == null)
					this.subObjectPropChangeEventHandler = new SubObjectPropChangeEventHandler(this.OnSubObjectPropChanged);

				return this.subObjectPropChangeEventHandler;
			}
		}

		private void OnSubObjectPropChanged( PropChangeInfo propChange )
		{
			if ( !(propChange.PropId is Infragistics.Win.UltraWinGrid.PropertyIds) )
				return;

			Infragistics.Win.UltraWinGrid.PropertyIds propId = (Infragistics.Win.UltraWinGrid.PropertyIds)propChange.PropId;

			// repaint the listbox when the key or hidden property is changed.
			switch( propId )
			{
				case PropertyIds.Key:
				case PropertyIds.Hidden:
				{
					this.ColumnListBox.Refresh();
					break;
				}
			}
		}

		private void menuItemShowToolBar_Click(object sender, System.EventArgs e)
		{
			this.menuItemShowToolBar.Checked = !this.menuItemShowToolBar.Checked;
			this.ColumnPropertyGrid.ToolbarVisible = this.menuItemShowToolBar.Checked;
		}

		private void menuItemShowDescription_Click(object sender, System.EventArgs e)
		{
			this.menuItemShowDescription.Checked = !this.menuItemShowDescription.Checked;
			this.ColumnPropertyGrid.HelpVisible = this.menuItemShowDescription.Checked;
			
		}

		private void PropertyGridContextMenu_Popup(object sender, System.EventArgs e)
		{
			// check the active grid item property and see if it can be
			// reset, if it can, enable/disable the reset menu item accordingly.
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.menuItemReset.Enabled = this.CanResetSelectedItems(this.ColumnPropertyGrid);
			this.menuItemReset.Enabled = ColumnEditorDialog.CanResetSelectedItems( this.ColumnPropertyGrid );
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool CanResetSelectedItems(System.Windows.Forms.PropertyGrid propGrid)
		private static bool CanResetSelectedItems( System.Windows.Forms.PropertyGrid propGrid )
		{
			if (propGrid == null)
				return false;

			if (propGrid.SelectedObjects == null || propGrid.SelectedObjects.Length == 0 || propGrid.SelectedGridItem == null)
				return false;
			else
			{
				// iterate through each grid item and see if it can be reset
				foreach( object obj in propGrid.SelectedObjects )
				{
					try
					{
						System.Windows.Forms.GridItem gridItem = propGrid.SelectedGridItem;
						
						if (!gridItem.PropertyDescriptor.CanResetValue(obj))
							return false;
					}
					catch (InvalidCastException)
					{
						return false;
					}
					catch (Exception err)
					{
						Debug.WriteLine( err.ToString() );
					}
				}
			}
			
			return true;
		}

		private void menuItemReset_Click(object sender, System.EventArgs e)
		{
			this.ColumnPropertyGrid.ResetSelectedProperty();
		}

		// MRS 10/20/04 - UWG3016
		#region BeginTransaction
		private void BeginTransaction()
		{
			UltraGridBase grid = this.columns.Band.Layout.Grid;

			// SSP 5/20/05 BR04176
			// Check for the grid being null. The layout when added to the layouts 
			// collection may not necessarily have a pointer to the grid yet. Enclosed
			// the existing code into the if block.
			//
			if ( null != grid )
			{
				IServiceProvider serviceProvider = grid.Site;

				if (serviceProvider != null)
				{
					componentService = serviceProvider.GetService( typeof( IComponentChangeService ) ) as IComponentChangeService;			
					designerHostService = serviceProvider.GetService( typeof( IDesignerHost ) ) as IDesignerHost;
				}

				if ( null != designerHostService )
					transaction = designerHostService.CreateTransaction( SR.GetString("DesignerTransactionName") );

				if ( null != componentService )
				{
					componentService.OnComponentChanging( grid, null );
					componentService.OnComponentChanged( grid, null, null, null );
				}

				// Supress the designer change notifications while the user is designing the
				// row layout.
				//
				this.wasDesignerChangeNotificationsDisabled = grid.DesignerChangeNotificationsDisabled;
				grid.DesignerChangeNotificationsDisabled = true;
			}
		}
		#endregion BeginTransaction

		// MRS 10/20/04 - UWG3016
		#region CommitTransaction
		private void CommitTransaction()
		{	
			UltraGridBase grid = this.columns.Band.Layout.Grid;
			
			// Reset the property that suppresses the designer change notification.
			//
			if (!this.wasDesignerChangeNotificationsDisabled && grid != null)
				grid.DesignerChangeNotificationsDisabled = false;

			// SSP 12/2/04
			// Check for transaction being null because at runtime the transaction
			// wouldn't have been created.
			//
			if ( null != this.transaction )
				transaction.Commit();

			// SSP 12/2/04
			// Also null out the transaction as a good practice. We don't want to 
			// be holding a reference to it.
			//
			this.transaction = null;
		}
		#endregion CommitTransaction
		
		// MRS 10/20/04 - UWG3016
		#region CancelTransaction
		private void CancelTransaction()
		{	
			try
			{
				// Reset the property that suppresses the designer change notification.
				//
				UltraGridBase grid = this.columns.Band.Layout.Grid;
				if (!this.wasDesignerChangeNotificationsDisabled && grid != null)
					grid.DesignerChangeNotificationsDisabled = false;

				// SSP 1/31/05
				// Why are we clearing the columns collection? This causes the grid to loose
				// all its columns when the columns editor dialog is cancelled.
				// Commented out the below line of code that clears the columns.
				//
				// MRS 5/18/04] - WDS31			
				//this.columns.Clear();

				// SSP 12/2/04
				// Check for transaction being null because at runtime the transaction
				// wouldn't have been created.
				//
				if ( null != this.transaction )
					transaction.Cancel();
			}
			finally
			{
				// SSP 12/2/04
				// Also null out the transaction as a good practice. We don't want to 
				// be holding a reference to it.
				//
				this.transaction = null;
			}
		}

		#endregion CancelTransaction
	}

	// AS 6/22/04 Moved to DialogHelperClasses.cs
	#region Moved
	
	#endregion //Moved
}

