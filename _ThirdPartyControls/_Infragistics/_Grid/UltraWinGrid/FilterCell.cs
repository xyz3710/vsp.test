#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;

namespace Infragistics.Win.UltraWinGrid
{
	// SSP 4/16/05 - NAS 5.2 Filter Row
	// Added UltraGridFilterCell class.
	//

	#region UltraGridFilterCell Class

	/// <summary>
	/// Class that represents a cell in a filter row.
	/// </summary>
	public class UltraGridFilterCell : UltraGridCell
	{
		#region Private Vars

		private EmbeddableEditorBase lastHookedOperatorEditor = null;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal bool operandsDropDownLoaded = false;

		// This is used to implement Escape key (cancel update) on a filter cell.
		//
		private ColumnFilter initialCellValue = null;

		#endregion // Private Vars

		#region Constructor

		internal UltraGridFilterCell( CellsCollection cells, UltraGridColumn column )
			: base( cells, column )
		{
		}

		#endregion // Constructor

		#region FilterRow

		internal UltraGridFilterRow FilterRow
		{
			get
			{
				return (UltraGridFilterRow)this.Row;
			}
		}

		#endregion // FilterRow

		#region ColumnFilter
		
		internal ColumnFilter ColumnFilter
		{
			get
			{
				return this.FilterRow.GetColumnFilter( this.Column );
			}
		}

		#endregion // ColumnFilter

		#region ApplyFilters

		/// <summary>
		/// Applies the filters currently input into the filter row.
		/// </summary>
		public void ApplyFilters( )
		{
			this.FilterRow.ApplyFilters( this.Column, true );
		}
		
		#endregion // ApplyFilters

		#region Value

		/// <summary>  
		/// Overridden.
		/// </summary>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override object Value
		{
			get
			{
				return this.Row.GetCellValue( this.Column );
			}
			set
			{
				this.Row.SetCellValue( this.Column, value );
			}
		}

		#endregion // Value

		#region Hidden

		/// <summary>
		/// Specifies whether the cell is hidden.
		/// </summary>
		public override bool Hidden
		{
			get
			{
				if ( FilterOperandStyle.None == this.Column.FilterOperandStyleResolved )
					return true;

				return base.Hidden;
			}
			set
			{
				base.Hidden = value;
			}
		}

		#endregion // Hidden

		#region ActivationResolved
		
		internal override Activation ActivationResolved 
		{
			get
			{
				if ( FilterOperandStyle.Disabled == this.Column.FilterOperandStyle )
					return Activation.Disabled;

				return base.ActivationResolved;
			}
		}

		#endregion // ActivationResolved

		#region CanBeModified

		// SSP 8/12/05 BR05479
		// 
		internal override bool CanBeModified
		{
			get
			{
				// first check the editmode enumerator to see if this cell
				// can be edited
				//
				if ( Infragistics.Win.UltraWinGrid.Activation.AllowEdit != this.ActivationResolved )
					return false;

				return true;
			}
		}

		#endregion // CanBeModified

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#region DroppedDownValueList

		//internal IValueList DroppedDownValueList
		//{
		//    get
		//    {
		//        IValueList vl = this.ValueListResolved;
		//        if ( null != vl && vl.IsDroppedDown )
		//            return vl;

		//        vl = this.Column.FilterOperatorEditorOwnerInfo.GetValueList( this );
		//        if ( null != vl && vl.IsDroppedDown )
		//            return vl;

		//        return null;
		//    }
		//}

		//#endregion // DroppedDownValueList

		#endregion Not Used

		#region NullableResolved
		
		internal override Nullable NullableResolved
		{
			get
			{
				// Filter cells should always be nullable.
				//
				return Nullable.Nothing;
			}
		}

		#endregion // NullableResolved

		#region CellChangeHelper

		internal override void CellChangeHelper( object sender, EventArgs ev )
		{
			Debug.Assert( this.IsInEditMode );
			if ( this.IsInEditMode )
			{
				// Operator editor also hooks into the ValueChanged with this method so get
				// the operator value as well.
				//
				EmbeddableEditorBase operatorEditor = this.Column.FilterOperatorEditor;
				if ( operatorEditor.IsInEditMode && operatorEditor.IsValid )
					this.FilterRow.SetOperatorValue( this.Column, operatorEditor.Value );

				base.CellChangeHelper( sender, ev );

				this.SetDataChanged( true );

				FilterEvaluationTrigger trigger = this.Column.FilterEvaluationTriggerResolved;
				bool applyNewFilter = FilterEvaluationTrigger.OnCellValueChange == trigger;
				EmbeddableEditorBase operandEditor = this.EditorResolved;

				// SSP 5/24/06 BR12854
				// 
				// ------------------------------------------------------------------------------
				if ( null != operandEditor && operandEditor.IsInEditMode 
					&& operandEditor.SupportsDropDown && operandEditor.IsDroppedDown )
				{
					bool customItemSelected;
					this.OnOperandSelected( out customItemSelected );
					if ( customItemSelected )
						return;
				}
				// ------------------------------------------------------------------------------

				// SSP 5/27/05 BR04286
				// Fire the FilterCellValueChanged event.
				// 
				applyNewFilter = this.FilterRow.FireFilterCellValueChangedHelper( this.Column, applyNewFilter );

				if ( applyNewFilter )
				{
					bool stayInEdit = true;
					if ( null != operandEditor && operandEditor.IsInEditMode
						&& this.CommitEditValue( ref stayInEdit, false, true ) )
						this.FilterRow.ApplyFilters( false );
				}
			}
		}

		#endregion // CellChangeHelper

		#region OnAfterCellEnterEditMode
		
		internal override void OnAfterCellEnterEditMode( )
		{
			base.OnAfterCellEnterEditMode( );

			// This is used to implement Escape key (cancel update) on a filter cell so 
			// clear it once we exit the edit mode.
			//
			ColumnFilter columnFilter = this.FilterRow.GetColumnFilter( this.Column );
			Debug.Assert( null != columnFilter );
			this.initialCellValue = null != columnFilter ? columnFilter.Clone( ) : null;

			UIElement elem = this.GetUIElement( false );
			elem = null != elem ? elem.GetDescendant( typeof( FilterOperatorUIElement ) ) : null;
			elem = null != elem ? CellUIElementBase.FindEmbeddableUIElement( elem.ChildElements, false ) : null;
			EmbeddableUIElementBase embeddableElem = elem as EmbeddableUIElementBase;

			EmbeddableEditorBase operatorEditor = this.Column.FilterOperatorEditor;
			Debug.Assert( null == embeddableElem || operatorEditor == embeddableElem.Editor );
			if ( null != embeddableElem && null != operatorEditor 
				&& operatorEditor == embeddableElem.Editor && ! embeddableElem.Disposed )
			{
				GridEmbeddableEditorOwnerInfoBase owner = this.Column.FilterOperatorEditorOwnerInfo;

				owner.dontHandleOnBeforeEnterEditMode = true;
				try
				{
					operatorEditor.EnterEditMode( embeddableElem );
				}
				finally
				{
					if ( operatorEditor.IsInEditMode )
						this.HookOperatorEditor( operatorEditor );

					owner.dontHandleOnBeforeEnterEditMode = false;
				}
			}
		}

		#endregion // OnAfterCellEnterEditMode
		
		#region OnAfterCellExitEditMode
		
		internal override void OnAfterCellExitEditMode( )
		{
			base.OnAfterCellExitEditMode( );

			EmbeddableEditorBase operatorEditor = this.Column.FilterOperatorEditor;
			if ( null != operatorEditor && operatorEditor.IsInEditMode )
				operatorEditor.ExitEditMode( true, true );

            // MBS 4/8/09 - TFS16470
            ColumnFilter cf = this.FilterRow.GetColumnFilter(this.Column);

			// This is used to implement Escape key (cancel update) on a filter cell so 
			// clear it once we exit the edit mode.
			//
			//Debug.Assert( null != this.initialCellValue );
            //
			if ( null != this.initialCellValue )
			{
				// If canceling the edit operation (by hitting escape) then revert back
				// to the original filters.
				//
				if ( this.Layout.CancellingEditOperationFlag )
				{
                    // MBS 4/8/09 - TFS16470
                    // Moved this outside the 'if' blocks so we don't get it multiple times
					//ColumnFilter cf = this.FilterRow.GetColumnFilter( this.Column );

					if ( null != cf )
						cf.InitializeFrom( this.initialCellValue );
				}

				this.initialCellValue = null;
			}

            // MBS 4/8/09 - TFS16470
            // Since we're no longer in edit mode, we shouldn't be holding onto any of the edit mode flags.
            if (null != cf)
            {
                // MBS 5/13/09 - TFS17581
                // We shouldn't be clearing these flags yet if will not have applied the filter yet
                FilterEvaluationTrigger trigger = this.Column.FilterEvaluationTriggerResolved;
                if (trigger == FilterEvaluationTrigger.OnCellValueChange)
                {
                    cf.ClearEditModeInfo();
                }
            }

			// Clear the filter drop down value list once we exit the edit mode.
			//
			UltraGridBase grid = this.Layout.Grid;
			if ( grid.HasFilterDropDown )
				grid.FilterDropDown.ValueListItems.Clear( );
		}

		#endregion // OnAfterCellExitEditMode

		#region HookOperatorEditor

		internal void HookOperatorEditor( EmbeddableEditorBase editor )
		{
			this.lastHookedOperatorEditor = editor;
			this.lastHookedOperatorEditor.AfterExitEditMode += new EventHandler( this.OnOperatorEditorAfterExitEditMode );
			this.lastHookedOperatorEditor.ValueChanged += new EventHandler( this.CellChangeHelper );
			this.lastHookedOperatorEditor.AfterCloseUp += new EventHandler( this.OnOperatorDropDownCloseUp );

			Infragistics.Win.UltraWinEditors.EmbeddableEditorButtonBase editorWithButtons =
				this.lastHookedOperatorEditor as Infragistics.Win.UltraWinEditors.EmbeddableEditorButtonBase;
			if ( null != editorWithButtons )
			{
				editorWithButtons.EditorButtonClick += 
					new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler( 
							this.OnOperatorEditorButtonClicked );
			}
		}

		#endregion // HookOperatorEditor

		#region UnhookOperatorEditor

		internal void UnhookOperatorEditor( )
		{
			if ( null != this.lastHookedOperatorEditor )
			{
				this.lastHookedOperatorEditor.AfterExitEditMode -= new EventHandler( this.OnOperatorEditorAfterExitEditMode );
				this.lastHookedOperatorEditor.ValueChanged -= new EventHandler( this.CellChangeHelper );
				this.lastHookedOperatorEditor.AfterCloseUp -= new EventHandler( this.OnOperatorDropDownCloseUp );

				Infragistics.Win.UltraWinEditors.EmbeddableEditorButtonBase editorWithButtons =
					this.lastHookedOperatorEditor as Infragistics.Win.UltraWinEditors.EmbeddableEditorButtonBase;
				if ( null != editorWithButtons )
				{
					editorWithButtons.EditorButtonClick -= 
						new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler( 
						this.OnOperatorEditorButtonClicked );
				}

				this.lastHookedOperatorEditor = null;
			}
		}

		#endregion // UnhookOperatorEditor

		#region OnOperatorEditorButtonClicked

		private void OnOperatorEditorButtonClicked( 
			object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e )
		{
			if ( null != this.lastHookedOperatorEditor
				&& this.IsInEditMode 
				&& this.lastHookedOperatorEditor.IsInEditMode 
				&& this.lastHookedOperatorEditor.SupportsDropDown )
			{
				this.lastHookedOperatorEditor.DropDown( );
			}
		}

		#endregion // OnOperatorEditorButtonClicked

		#region OnOperatorEditorAfterExitEditMode

		private void OnOperatorEditorAfterExitEditMode( object sender, EventArgs e )
		{
			// Unhook from the editor. We should've already extracted the value in the ValueChange
			// event handler.
			//
			this.UnhookOperatorEditor( );
		}

		#endregion // OnOperatorEditorAfterExitEditMode

		#region OnOperatorDropDownClosUp

		private void OnOperatorDropDownCloseUp( object sender, EventArgs e )
		{
			// When the operator drop down closes up the operand editor is given focus
			// so the user can start typing the operand without having to give focus to
			// the operand editor (typically involves clicking onto it).
			//
			EmbeddableEditorBase operandEditor = this.EditorResolved;
			if ( null != operandEditor && this.IsInEditMode 
				&& operandEditor.IsInEditMode && this.Layout.Grid.ContainsFocus )
				operandEditor.Focus( );
		}

		#endregion // OnOperatorDropDownClosUp

		#region OnOperandSelectionChanged

		internal void OnOperandSelectionChanged( object sender, EventArgs e )
		{
			bool customItemSelected;
			this.OnOperandSelected( out customItemSelected );
		}

		#endregion // OnOperandSelectionChanged

		#region OnOperandSelected

		// SSP 5/24/06 BR12854
		// Added OnOperandSelected method. Code in there is moved from the existing OnOperandSelectionChanged method.
		// 
		private void OnOperandSelected( out bool customItemSelected )
		{
			customItemSelected = false;

			EmbeddableEditorBase editor = this.EditorResolved;
			if ( null == editor || ! editor.IsInEditMode )
			{
				Debug.Assert( false );
				return;
			}

			// If (Custom) is selected from the drop down then show the custom filter dialog.
			// 
			if ( UltraGridBand.FilterCustomText == editor.CurrentEditText
				&& editor.SupportsDropDown && editor.IsDroppedDown )
			{
				customItemSelected = true;

				// SSP 5/24/06 BR12854
				// Here we are trying to show the Custom Filter dialog in response to the user selecting
				// Custom from the filter drop down list. At this point we should exit the edit mode on
				// the filter cell (which is what we do below - that was existing code). However before
				// we exit the edit mode, we should make sure that the filter cell reflects the filter
				// conditions as applied currently. When ExitEditMode is called below with 
				// cancellingEditOperation parameter as true, we are trying to make sure that we don't
				// end up apply "(Custom)" value (which is what the user just selected from the filter
				// dropdown). However at the same time we don't want to revert the filters back to their
				// original state when we entered the edit mode. We want to leave the filter conditions
				// as applied currently and simply reflect that in the filter cell. The reason why
				// we are nulling out initialCellValue here is because if we don't then the
				// code in OnAfterCellExitEditMode will revert back to that value, which will be the
				// value before we entered edit mode. To see what I mean, enter edit mode in a filter
				// cell, change the filter cell value one or more times and then select Custom (while
				// being in edit mode). Without this you will notice that the filter cell reverts
				// back to the original value that it had before it entered edit mode. We don't want
				// that. For better usability, we want to keep the filter conditions applied at this
				// current moment and have the filter cell reflect that.
				// 
				// ------------------------------------------------------------------------------------
				this.initialCellValue = null;
				// ------------------------------------------------------------------------------------

				// Exit the edit mode on the filter cell first, canceling the edit operation. The
				// reason why we need to exit the edit mode is that the same value lists used by
				// the filter operator and operand may be used by the custom filter dialog.
				//
				this.ExitEditMode( true, false );

				// If exitting edit mode succeeded then show the dialog.
				//
				if ( ! this.IsInEditMode )
				{
					ColumnFilter origColumnFilter = this.FilterRow.GetColumnFilter( this.Column );
					if ( null != origColumnFilter )
					{
						ColumnFilter newColumnFilter = origColumnFilter.Clone( );
						bool applyNewFilters = ColumnHeader.RowFilterDropDownOwner.ShowCustomFilterDialogHelper( newColumnFilter, this.Row.ParentCollection );

						// SSP 3/21/06 BR11004
						// Check for applyNewFilters.
						// 
						//UltraGridFilterRow.ApplyNewFiltersHelper( origColumnFilter, newColumnFilter, this.FilterRow );
						if ( applyNewFilters )
						{
							bool ret = UltraGridFilterRow.ApplyNewFiltersHelper( origColumnFilter, newColumnFilter, this.FilterRow );

							// SSP 5/24/06 BR12924
							// Reset edit mode related info from the original column filter in case some values were
							// modified and then Custom were selected from the filter cell.
							// 
							if ( ret )
								origColumnFilter.ClearEditModeInfo( );
						}
					}
				}
			}
		}

		#endregion // OnOperandSelected

		#region OnActiveStateChanged

		internal override void OnActiveStateChanged( bool activated )
		{
			base.OnActiveStateChanged( activated );

			if ( ! activated && ! this.Disposed )
			{
				FilterEvaluationTrigger trigger = this.Column.FilterEvaluationTriggerResolved;
				if ( FilterEvaluationTrigger.OnLeaveCell == trigger
					|| FilterEvaluationTrigger.OnEnterKeyOrLeaveCell == trigger )
					this.ApplyFilters( );
			}
		}

		#endregion // OnActiveStateChanged

		#region FilterOperator

		// SSP 7/21/06 BR14307
		// Added FilterOperator property and ResetFilterOperatorToDefaultValue method.
		// 
		/// <summary>
		/// Gets/sets the value of the filter operator.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Note that this emulates user selecting the value from the filter operator. When the new filter 
		/// operator gets applied depends on the <see cref="UltraGridOverride.FilterEvaluationTrigger"/> property 
		/// setting. You can explicitly cause the new operator to take effect by calling the 
		/// <see cref="UltraGridFilterCell.ApplyFilters"/> method.
		/// </p>
		/// <seealso cref="UltraGridOverride.FilterOperatorDefaultValue"/> <seealso cref="UltraGridFilterCell.ApplyFilters"/>
		/// </remarks>
		public FilterComparisionOperator FilterOperator
		{
			get
			{
				object val = this.FilterRow.GetOperatorValue( this.Column );
				if ( val is FilterComparisionOperator )
					return (FilterComparisionOperator)val;

				Debug.Assert( false );
				return FilterComparisionOperator.Custom;
			}
			set
			{
				this.FilterRow.SetOperatorValue( this.Column, value );
			}
		}

		#endregion // FilterOperator

		#region ResetFilterOperatorToDefaultValue

		// SSP 7/21/06 BR14307
		// Added FilterOperator property and ResetFilterOperatorToDefaultValue method.
		// 
		/// <summary>
		/// Resets the filter operator to the initialize default value.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The initialize filter operator value is controlled by the 
		/// <seealso cref="UltraGridOverride.FilterOperatorDefaultValue"/> property.
		/// </p>
		/// <seealso cref="UltraGridOverride.FilterOperatorDefaultValue"/> <seealso cref="UltraGridFilterCell.ApplyFilters"/>
		/// </remarks>
		public void ResetFilterOperatorToDefaultValue( )
		{
			this.FilterOperator = this.Layout.GetAssociatedFilterComparisionOperator( this.Column.FilterOperatorDefaultValueResolved );
		}

		#endregion // ResetFilterOperatorToDefaultValue

	}

	#endregion // UltraGridFilterCell Class

}
