#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Globalization;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Shared.Serialization;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Collections;
	using System.Drawing.Design;
	using System.Security.Permissions;

	// SSP 2/11/03 - Row Layout Functionality
	//
	using Infragistics.Win.Layout;
	using System.Collections.Generic;
    using System.Drawing.Imaging;	
	
	/// <summary>
	/// An object that represents a set of related columns of data.
	/// </summary>
	/// <remarks>
	/// <p class="body">An UltraGridBand object represents a set of related columns in an <see cref="Infragistics.Win.UltraWinGrid.UltraGrid"/>, <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> or <see cref="Infragistics.Win.UltraWinGrid.UltraDropDown"/> control. In the case of flat data there is only 1 band, whereas a grid with hierarchical data will have multiple bands.</p>
	///	<p></p>
	/// <p class="body">
	/// The following sample code shows how to set some UltraGridBand properties.
	/// </p>
	/// <p></p>
	/// <code>
	/// Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
	/// 
	///     Dim band As Infragistics.Win.UltraWinGrid.UltraGridBand
	/// 
	///     band = Me.UltraGrid1.DisplayLayout.Bands(0)
	/// 
	///     band.AddButtonCaption = "Customers"
	///     band.AddButtonToolTipText = "Adds a new Customer record."
	///     band.AutoPreviewEnabled = False
	///     band.AutoPreviewField = ""
	///     band.AutoPreviewIndentation = 15
	///     band.AutoPreviewMaxLines = 3
	///     band.ColHeadersVisible = True
	///     band.ColHeaderLines = 2
	///     band.HeaderVisible = True
	///     band.ScrollTipField = "Cust_ID"
	///     band.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.InsetSoft
	/// 
	///     band.CardView = True
	///     band.CardSettings.CaptionField = "Name"
	///     band.CardSettings.AllowLabelSizing = True
	///     band.CardSettings.AllowSizing = True
	///     band.CardSettings.Style = Infragistics.Win.UltraWinGrid.CardStyle.MergedLabels
	///     band.CardSettings.ShowCaption = True
	///     band.CardSettings.AutoFit = True
	/// 
	/// End Sub
	/// </code>
	/// <p class="body"> </p>
	/// <seealso cref="UltraGridLayout.Bands"/>
	/// </remarks>
	[ TypeConverter( typeof( UltraGridBand.UltraGridBandTypeConverter ) ) ] 
	[ Serializable() ]
	public class UltraGridBand : KeyedSubObjectBase,
		ISelectionStrategyProvider,
		ISerializable,
        // MRS - NAS 9.1 - Groups in RowLayout
        ILayoutGroup
	{
		#region Member Variables

		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Localized these strings.
		//
		//internal const string DEFAULT_SUMMARY_FOOTER_CAPTION_BAND_0 = "Grand Summaries";
		//internal const string DEFAULT_SUMMARY_FOOTER_CAPTION = "Summaries for [BANDHEADER]: [SCROLLTIPFIELD]";

		internal ColumnsCollection   columns = null;
		private GroupsCollection    groups = null;
		private Infragistics.Win.UltraWinGrid.UltraGridOverride 
			overrideObj = null;
		private Infragistics.Win.UltraWinGrid.UltraGridCardSettings
			cardSettings = null;
		private UltraGridLayout              layout;
		private BindingManagerBase  bindingManager = null;
		private HeadersCollection   orderedColumns = null;
		private HeadersCollection   orderedGroups = null;
		private UltraGridBand                parentBand;
		private Infragistics.Win.UltraWinGrid.UltraGridColumn
			parentColumn;
		private Infragistics.Win.UltraWinGrid.BandHeader   
			header = null;
		private int                 origin = 0;
		private int                 extent = 0;
		private string              addButtonCaption = null;
		private string              addButtonToolTipText;
		private string              autoPreviewField;
		private string              scrollTipField;
		private string              dataMember;
		private bool                autoPreviewEnabled;
		private bool                headerVisible;
		private bool                colHeadersVisible;
		private bool                groupHeadersVisible;
		private bool                expandable;
		private bool				hidden = false;
		//private bool                rowHeightIsDirty = false;
		private bool                isSynchronizingRowHeights = false;
		private int                 autoPreviewMaxLines;
		private int                 colHeaderLines;
		private int                 groupHeaderLines;
		private int                 levelCount;
		private int                 synchronizationAdjustment;
		private int                 synchronizedRowHeight = 0;
		private int 				cachedColHdrFontHeight; 
		private int					cachedGroupHdrFontHeight;
		private int					cachedRowFontHeight;
		
		// JJD 12/13/01
		// Added caching logic for the card appearance height caching
		//
		private int					cachedCardLabelFontHeight;
		private int					cachedCardCaptionFontHeight;
		private int					cachedCardHeight;
		private int					cachedCardLabelAreaWidth = 0;
		private int					cachedCardCellAreaWidth = 0;

		private bool				isColHdrFontHeightCached; 		
		private bool				isGroupHdrFontHeightCached; 
		private bool				isRowFontHeightCached;
		private bool				isMinRowHeightCached;
		
		// JJD 12/13/01
		// Added caching logic for the card appearance height caching
		//
		private bool				isCardLabelFontHeightCached = false;
		private bool				isCardCaptionFontHeightCached = false;
		private bool				isColumnCardFontCacheDirty = false;
		
		private bool				isRowHeightDirty = false;		
		private bool				cardView = false;	
			
		private int                 indentation;
		private int					synchronizedRowHeightVersion = 0;

		private bool				orderedColumnsDirty = false;
		// SSP 7/18/03 - Fixed headers
		// Why don't we have orderedGroupsDirty analogous to orderedColumnsDirty.
		// Added orderedGroupsDirty flag.
		//
		private bool				orderedGroupsDirty = false;

		private int					autoPreviewIndentation = 0;
		// JJD 8/7/01 - UWG101
		// Major row height version must be initialized to 0 so that 
		// the free row sizing works properly
		//
		private int                 majorRowHeightVersion = 0;
		//private AddNewRowButtonUIElement addNewRowButtonUIElement;
		
		private SortedColumnsCollection sortedColumns = null;
		
		private EventHandler positionChangedHandler = null;
		private EventHandler currentChangedHandler = null;
		private ItemChangedEventHandler itemChangedHandler = null;
		private CollectionChangeEventHandler  bindingsChangedHandler = null;
		private System.ComponentModel.ListChangedEventHandler listChangedHandler = null;
		private int			parentIndex = -1;
		private int			groupByHierarchyVersion = 0;
		private int			columnsVersion = 0;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//internal static readonly int EXPANSION_INDICATOR_WIDTH = 9;
		//internal static readonly int ROW_SELECTOR_WIDTH = 19;
		internal const int EXPANSION_INDICATOR_WIDTH = 9;
		internal const int ROW_SELECTOR_WIDTH = 19;

		// SSP 3/30/05 - NAS 5.2 Row Numbers
		// Commented out ROW_SELECTOR_WIDTH_WITH_DATA_ERROR_ICONS and added ROW_SELECTOR_ROW_ICONS_WIDTH.
		//
		//		// SSP 2/4/05 - IDataErrorInfo Support
		//		// Resolve the row selector width differently based on whether row data error 
		//		// is supported so the data error icon and the row icons both have enough space.
		//		//
		//		//internal static readonly int ROW_SELECTOR_WIDTH_WITH_DATA_ERROR_ICONS = ROW_SELECTOR_WIDTH + DataErrorIconUIElement.DEFAULT_WIDTH;
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//internal static readonly int ROW_SELECTOR_ROW_ICON_WIDTH = 15;
		//
		//internal static readonly int PRE_ROW_SELECTOR_WIDTH = 19;
		//internal static readonly int MAX_BAND_DEPTH = 100;
		internal const int ROW_SELECTOR_ROW_ICON_WIDTH = 15;

		internal const int PRE_ROW_SELECTOR_WIDTH = 19;
		internal const int MAX_BAND_DEPTH = 100;

		// SSP 8/13/01 Added this flag to keep track of whether we are
		// currently in AddNew function, so DataSource event handlers know
		// how to handle events when adding a row
		internal bool inAddNew = false;
		// SSP 8/13/01 Also added this variable for the same reason as above
		internal UltraGridRow  addedRow = null; 

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal UltraGridGroupByRow parentGroupByRowToAddTheRowTo = null;
		//
		//// SSP 10/3/01
		//// These flags are used by the Rows.SyncRows when a row is being
		//// deleted in the grid.
		//internal bool inDeleteRow = false;
		//internal UltraGridRow rowBeingDeleted = null;

		private bool hookIntoList = false;

		// SSP 1/18/02
		// The List off the BindingManager is not persistant. It keeps
		// changing as the active row in the parent binding manager changes.
		// So we need to rehook into the binding list every time it changes.
		// This var is created keep track of the old binding list.
		//
		private IBindingList lastBindingList = null;
		
		// SSP 1/21/02
		// We need to reset the IsAddRow flag off the newly added rows whenever 
		// they are committed.
		//
		private UltraGridRow lastAddedRow = null;

		// SSP 4/2/04 UWG2985
		// Changed the behavior of AddRowAppearance. Shade the cell back color to use with the
		// add-row only if template add-row functionality is turned on. When add-row feature 
		// is not turned on and the user adds a new row from outside, we don't want to all of
		// a sudded start showing the added row as gray where as in version 3.0 we used to
		// show it the same back color as the regular rows. Also don't apply the add-row
		// appearance to rows that are added from outside.
		//
		internal bool lastAddedRowAddedThroughGrid = false;

		// SSP 3/21/02
		// Version 2 Row Filter Implementation
		// 
		// Version number that is bumped when filters for filtering the rows
		private int rowFiltersVersion = 0;
		private ColumnFiltersCollection columnFilters = null;

		// SSP 4/23/02
		// Summary Rows Feature
		//
		private SummarySettingsCollection			summaries = null;

		// SSP 5/28/02 UWG1141
		// Also added lastBoundList variable to keep track of the list
		//
		private IList        lastBoundList = null;


		
		// SSP 6/10/02
		// Summary rows feature.
		// Indicates the spacing between successive summary footers.
		//
		internal const int INTER_SUMMARY_FOOTER_SPACING = 4;

		private string summaryFooterCaption = null;

		// SSP 1/30/03 - Row Layout Functionality
		//
		// ------------------------------------------------------------------------------
		// MD 2/23/09 - TFS14254
		// This member variable is no longer used.
		//private bool useRowLayout = false;

		private Infragistics.Win.Layout.GridBagLayoutManager gridBagRowLayoutManager = null;
		private Infragistics.Win.Layout.GridBagLayoutManager gridBagHeaderLayoutManager = null;
		private Infragistics.Win.Layout.GridBagLayoutManager gridBagLayoutManagerHelper = null;
		private Infragistics.Win.Layout.GridBagLayoutManager gridBagSummaryLayoutManager = null;
		private int rowLayoutCacheVersion = 0;
		private int verifiedRowLayoutCacheVersion = -1;
		private GridBagLayoutItemDimensionsCollection layoutItemDimensions = null;
		private GridBagLayoutItemDimensionsCollection layoutItemDimensionsHeaderArea = null;
		private GridBagLayoutItemDimensionsCollection layoutItemDimensionsCellArea = null;
		private Size preferredRowLayoutSize = Size.Empty;
		private Size preferredHeaderLayoutSize = Size.Empty;
		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Obsoleted by the new GetSummaryLayoutPreferredSize method.
		//
		//private Size preferredSummaryLayoutSize = Size.Empty;

		// SSP 3/5/05 BR02359
		// Added minimumRowLayoutSize and minimumHeaderLayoutSize properties.
		//
		private Size minimumRowLayoutSize = Size.Empty;
		private Size minimumHeaderLayoutSize = Size.Empty;
		private HeadersCollection layoutOrderedVisibleColumnHeaders = null;
		private HeadersCollection layoutOrderedVisibleColumnHeadersVertical = null;
		private RowLayoutsCollection rowLayouts = null;
		private LabelPosition rowLayoutLabelPosition = LabelPosition.Default;
		private RowLayoutLabelStyle rowLayoutLabelStyle = RowLayoutLabelStyle.Separate;
		private bool inVerifyRowLayoutCache = false;

		// SSP 8/6/03 UWG2568
		// When we are resizing all the columns, we don't want to recalculate the row layout
		// until the operation of resizing all the columns has finished. To accomplish that
		// added dontVerifyRowLayoutCache flag.
		//
		// SSP 5/7/07 BR22392
		// 
		//private bool dontVerifyRowLayoutCache = false;
		internal bool dontVerifyRowLayoutCache = false;

		// Following variables cache layout containers.
		//
		private LayoutContainerCellArea layoutContainerCellArea = null;
		private LayoutContainerHeaderArea layoutContainerHeaderArea = null;
		private LayoutContainerCardCellArea layoutContainerCardCellArea = null;
		private LayoutContainerCardLabelArea layoutContainerCardLabelArea = null;
		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Obsoleted by the new GetSummaryLayoutManager method.
		//
		//private LayoutContainerSummaryArea layoutContainerSummaryArea = null;

		private LayoutContainerCalcSize layoutContainerCalcSize = null;
		// ------------------------------------------------------------------------------

		// SSP 7/1/03 UWG2443
		// Added new addedInBandsSerializer flag to the band object which indicates whether
		// the band has been added to to the bands serializer collection in the 
		// Layout.BandsSerializer property's get. Look in there for more info.
		//
		internal bool addedInBandsSerializer = false;

		// SSP 7/18/03 - Fixed headers
		//
		// MD 12/8/08 - 9.1 - Column Pinning Right
		// The fixed headers cache has to be broken up into two collections so they can be differentiated.
		//private HeaderBase[] fixedHeadersCached = null;
		private HeaderBase[] leftFixedHeadersCached = null;
		private HeaderBase[] rightFixedHeadersCached = null;

		private int fixedHeadersVerifyVersion = 0;
		private int verifiedFixedHeadersVersion = -1;

		// SSP 3/26/04 UWG2993
		// Added verifiedIsFilterUIEnabledVersionNumber and cachedIsFilterUIEnabled.
		//
		private int verifiedIsFilterUIEnabledVersionNumber = -1;
		private bool cachedIsFilterUIEnabled = false;

		// JJD 1/06/04 - Accessibility
		private System.Windows.Forms.AccessibleObject colHeadersAccessibilityObject;

		// SSP 6/29/04 - UltraCalc
		//
		private BandReference calcReference = null;

		// JAS 2005 v2 XSD Support
		//
		private int minRows = 0;
		private int maxRows = Int32.MaxValue;
		private XsdConstraintFlags xsdSuppliedConstraints = 0;

		// SSP 12/13/04 - Merged Cell Feature
		//
		private int mergedCellVersion = 0;

		// SSP 3/14/05 - NAS 5.2 Outlook Style Fixed Add Row
		//
		// ----------------------------------------------------------------------------
		private const int SPECIAL_ROW_SEPARATOR_HEIGHT = 6;
		private string specialRowPromptField = null;
		// ----------------------------------------------------------------------------
		
		// JAS 2005 v2 GroupBy Row Extensions
		//
		// ----------------------------------------------------------------------------
		private int indentationGroupByRow = -1;
		private int indentationGroupByRowExpansionIndicator = -1;
		// ----------------------------------------------------------------------------

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// Added ExcludeFromColumnChooser property.
		// 
		private ExcludeFromColumnChooser excludeFromColumnChooser = ExcludeFromColumnChooser.Default;

		// SSP 7/29/05 - NAS 5.3 Tab Index
		// 
		private HeadersCollection tabOrderedColumns = null;

		// SSP 9/6/05 - NAS 5.3 Wrap Header Text for Row-Layout
		// GetHeaderHeight method was using 4 as a literal. Created a constant for it 
		// because we need to use it another place as well.
		// 
		internal const int EXTRA_HEADER_HEIGHT_PADDING = 4;

		// SSP 11/10/05 BR07605
		// 
		internal int bandExtent_BeforeAutoFitExtendLastColumn = 0;

		// SSP 4/10/06 - App Styling
		// 
		internal AppStyling.StylePropertyCache propertyCache = new AppStyling.StylePropertyCache( 1 + (int)StyleUtils.CachedProperty.LastValue );

        // MBS 2/6/08 - RowEditTemplate NA2008 V2
        private UltraGridRowEditTemplate rowEditTemplate;

        // MBS 5/6/08 - RowEditTemplate NA2008 V2
        private Image rowEditTemplateRowSelectorImageDefault;

        // MBS 5/16/08 - RowEditTemplate NA2008 V2
        private string rowEditTemplateControlName;

        // MRS NAS 9.1 - Sibling Band Order
        // Added a VisiblePosition property to the band. 
        //
        private int visiblePosition;

        // MBS 11/4/08 - TFS9064
        private IList cachedList;

        // MRS - NAS 9.1 - Groups in RowLayout
        private RowLayoutStyle rowLayoutStyle = RowLayoutStyle.None;
        private int groupsVersion = -1;
        internal Infragistics.Win.Layout.GridBagLayoutManager groupedGridBagRowLayoutManager = null;
        internal Infragistics.Win.Layout.GridBagLayoutManager groupedGridBagHeaderLayoutManager = null;
        internal Infragistics.Win.Layout.GridBagLayoutManager groupedGridBagSummaryLayoutManager = null;
		private int rowLayoutItemCacheVersion;

        // MRS 2/10/2009 - TFS13728
        private HeadersCollection layoutOrderedVisibleGroupHeaders = null;

		#endregion

		#region constructors

		internal UltraGridBand( UltraGridLayout     layout, 
			UltraGridBand       parentBand,
			Infragistics.Win.UltraWinGrid.UltraGridColumn
			parentColumn )
		{
			if ( null == layout )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_2"));

			this.layout         = layout;
			this.parentBand     = parentBand;
			this.parentColumn   = parentColumn;

			// SSP 5/3/05
			// Moved the following code into the new Initialize so the other constructor can call it.
			//
			// --------------------------------------------------------------------------------------
			this.Initialize( );
			
			// --------------------------------------------------------------------------------------
		}

		/// <summary>
		/// Constructor used during de-serialization
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="parentBandIndex">The parent band index</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridBand( string key, int parentBandIndex )
		{
			// set all properties to their default settings
			//
			// SSP 5/3/05
			// We also need to initialize other variables that the above constructor does (like the
			// event handlers). For that call the new Initialize method.
			//
			// --------------------------------------------------------------------------------------
			//this.Reset();
			this.Initialize( );
			// --------------------------------------------------------------------------------------

			this.Key = key;

			this.parentIndex = parentBandIndex;

		}

		#region Initialize

		// SSP 5/3/05
		// Added Initialize method. Code in there is moved from the constructor.
		//
		private void Initialize( )
		{
			this.currentChangedHandler  = new EventHandler( this.DataSource_CurrentChanged );
			this.positionChangedHandler = new EventHandler( this.DataSource_PositionChanged );						
			this.itemChangedHandler		= new ItemChangedEventHandler( this.DataSource_ItemChanged );
			this.bindingsChangedHandler = new CollectionChangeEventHandler( this.DataSource_BindingsChanged );
			this.listChangedHandler		= new System.ComponentModel.ListChangedEventHandler( this.OnListChanged );

			// set all properties to their default settings
			//
			this.Reset();

			this.cachedColHdrFontHeight = 0; 
			this.cachedGroupHdrFontHeight = 0;
			this.isColHdrFontHeightCached = false; 		
			this.isGroupHdrFontHeightCached = false; 
			this.isRowFontHeightCached = false;
			this.isMinRowHeightCached = false;
			this.isCardCaptionFontHeightCached = false;
			this.isCardLabelFontHeightCached = false;
		}

		#endregion // Initialize
		
		#endregion


		#region FitColumnsToWidth

		internal void FitColumnsToWidth( int right )
		{
			int newExtent = right - this.GetOrigin( BandOrigin.RowSelector );
			// SSP 6/1/05 BR03774
			// Don't substract border from the right. Modified the callers to call this method
			// with a right that's relative to the scroll region instead of the grid. In other
			// words the caller now subtracts out the left border of the grid before calling
			// this method. This was done because apparently the horizontal view style was not
			// consistant in its logic for autofitting. Without the fixes for this bug try to
			// sort a column in horizontal view style with autofit enabled and notice that the
			// columns are slightly adjusted even though they weren't supposed to.
			//
			

			// if the new or the old extent is less than 1 then
			// return since we are too soon in the process to make
			// any adjustments
			//
			if ( newExtent < 1 || 
				this.extent < 1 )
				return;

			int adjustment = newExtent + this.PreRowAreaExtent - this.extent;

			if ( adjustment == 0 )
				return;

			int minWidth = 0;
			int maxWidth = 0;

			// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
			// If the auto-fit style is to extend the last column then increase the band's 
			// extent so it extends all the way to the edge of the grid.
			// 
			// ----------------------------------------------------------------------------
			if ( this.Layout.AutoFitExtendLastColumn )
			{
                // MRS 6/16/2009 - TFS18464
                // When printing, it does not make sense to extend the last column
                // unless FitWidthToPages is also set. Without FitWidthToPages, we don't
                // know what to extend the column to - extended it based on the size of the 
                // on-screen grid makes no sense and will end up clipping the column if it goes 
                // off the page.
                //
                bool extendLastColumn = true;
                //
                // MBS 7/30/09 - TFS20158
                // We can't assume that we're dealing with an UltraGrid
                //
                //PrintManager printManager = ((UltraGrid)this.layout.Grid).PrintManager;
                PrintManager printManager = this.layout.Grid is UltraGrid ? ((UltraGrid)this.layout.Grid).PrintManager : null;
                //
                if (printManager != null)
                {
                    int pages = printManager.LogicalPageLayoutInfoHolder.FitWidthToPages;
                    if (pages == 0)
                        extendLastColumn = false;
                }

				// Only extend, never reduce the width. Also do so only when there's at 
				// least one visible column (or group).
				//
				HeaderBase lastVisibleHeader = this.OrderedHeaders.LastVisibleHeader;
				if ( adjustment > 0 && null != lastVisibleHeader 
                    // MRS 6/16/2009 - TFS18464
                    && extendLastColumn)
				{
					// SSP 4/7/05 BR03245
					// Constraint the last item from resizing bigger than MaxWidth. Also 
					// do this in non-row-layout mode only since row-layout mode currently
					// doesn't support MaxWidth settings.
					//
					// ----------------------------------------------------------------------
					if ( ! this.UseRowLayoutResolved )
					{
						minWidth = 0;
						maxWidth = 0;
						lastVisibleHeader.GetResizeRange( ref minWidth, ref maxWidth );
						if ( maxWidth > 0 )
						{
							int currWidth = lastVisibleHeader.Extent - lastVisibleHeader.RowSelectorExtent;
							if ( currWidth + adjustment > maxWidth )
								adjustment = Math.Max( 0, maxWidth - currWidth );
						}

						this.extent += adjustment;
					}
						// SSP 7/12/05 BR04916
						// We need to verify the RowLayoutColumnInfo.CachedItemRectCell values when auto-fitting
						// in row layout otherwise the UltraGridColumn.CellSizeResolved won't return the correct
						// values. Added the following else block.
						// 
					else
					{
						// SSP 11/10/05 BR07605
						// Adjust the extent before calling RecalcCachedLayoutItemRects because
						// RecalcCachedLayoutItemRects uses the extent.
						// 
						this.extent += adjustment;

						this.RecalcCachedLayoutItemRects( );
					}
					// ----------------------------------------------------------------------
				}

				// SSP 11/10/05 BR07605
				// If row sizing is Auto then we need to recalc the ideal heights of the rows since
				// the widths of the columns could have potentially changed.
				// 
				this.BumpMajorRowHeightVersion( );

				return;
			}
			// ----------------------------------------------------------------------------

			// SSP 3/11/03 - Row Layout Functionality
			// In row layout mode, autofitting columns will be done through the layout manager.
			//
			if ( this.UseRowLayoutResolved )
			{
				this.extent += adjustment;

				// SSP 3/4/05 BR02359
				// When auto-fitting, honor the MinimumCellSize and MinimumLabelSize of the
				// layout items.
				//
				this.extent = Math.Max( this.extent, this.GetExtentLayout( true, BandOrigin.PreRowArea ) );

				// SSP 7/12/05 BR04916
				// We need to verify the RowLayoutColumnInfo.CachedItemRectCell values when auto-fitting
				// in row layout otherwise the UltraGridColumn.CellSizeResolved won't return the correct
				// values.
				// 
				this.RecalcCachedLayoutItemRects( );

				return;
			}

			minWidth = 0;
			maxWidth = 0;
			int originalAdjustment = adjustment;

			int numberOfAdjustableHeaders = 0;
			int widthOfAdjustableHeaders = 0;

			// count up the adjustable headers
			//
			for ( int i = 0; i < this.OrderedHeaders.Count; i++ )
			{
				if ( this.OrderedHeaders[i].Hidden )
					continue;

				// SSP 12/10/02 UWG1768
				// Skip groups and columns for which proportional resize is truned off.
				//
				// --------------------------------------------------------------------
				if ( this.OrderedHeaders[i] is GroupHeader )
				{
					UltraGridGroup group = this.OrderedHeaders[i].Group;
					
					if ( null != group && !group.ProportionalResizeResolved )
						continue;
				}

				if ( this.OrderedHeaders[i] is ColumnHeader )
				{
					UltraGridColumn column = this.OrderedHeaders[i].Column;

					if ( null != column && !column.ProportionalResizeResolved )
						continue;
				}
				// --------------------------------------------------------------------

				this.OrderedHeaders[i].GetResizeRange( ref minWidth, ref maxWidth );

				// get the old width
				//
				int oldWidth = this.OrderedHeaders[i].Extent;

				// filter out headers that can't take the adjustment
				//
				if ( adjustment < 0 )
				{
					if ( oldWidth <= minWidth )
						continue;
				}
				else
				{
					if ( maxWidth > 0 &&
						oldWidth >= maxWidth )
						continue;
				}

				// bump the count
				//
				numberOfAdjustableHeaders++;

				// SSP 3/26/04 UWG2987
				// Substract the row selector width from the header width since that shouldn't
				// be taken into account when calculating how much width to distribute to a 
				// column. It should be based on the widths of the cells in the column.
				//
				//widthOfAdjustableHeaders += oldWidth;
				widthOfAdjustableHeaders += oldWidth - this.OrderedHeaders[i].RowSelectorExtent;
			}

			if ( numberOfAdjustableHeaders < 1 )
				return;

			// compute the factor
			//
			double factor = (double)(adjustment + widthOfAdjustableHeaders) / (double)widthOfAdjustableHeaders;

			// adjust the adjustable headers based on the factor computed above
			//
			this.ApplyAdjustmentToColumns( ref adjustment, factor );

			if ( adjustment != 0 )
			{
				// do a final pass adding the remaining adjustment wherever it fits 
				//
				this.ApplyAdjustmentToColumns( ref adjustment, 0.0d );
			}

			// add in the applied adjustment
			//
			this.extent += originalAdjustment - adjustment;
			
		}
		#endregion FitColumnsToWidth

		#region ApplyAdjustmentToColumns

		// SSP 9/24/03 UWG2455
		// Don't keep applying the adjustment to the same column.
		//
		private int autoFit_columnLastAdjustmentFullyAppliedToIndex = -1;

		// SSP 6/18/04 UWG3253
		// Added ResetAutoFit_ColumnLastAdjustmentFullyAppliedToIndex method.
		// To fix an issue where repeatedly toggling AutoFitColumns property was causing
		// the auto-fit columns logic to be executed multiple times and every time the
		// logic was executed, the auto-fit slack as a result of rounding was being applied
		// to a different column based on the columnLastAdjustmentFullyAppliedToIndex 
		// counter variable causing a wave like effect (see UWG3253 sample). To fix this 
		// issue, whenever the value of AutoFitColumns is changed, we will reset this
		// columnLastAdjustmentFullyAppliedToIndex variable on all bands.
		//
		internal void ResetAutoFit_ColumnLastAdjustmentFullyAppliedToIndex( )
		{
			this.autoFit_columnLastAdjustmentFullyAppliedToIndex = -1;
		}

		private void ApplyAdjustmentToColumns( ref int adjustment, double factor )
		{
			int minWidth = 0;
			int maxWidth = 0;

			// SSP 9/24/03 UWG2455
			// Don't keep applying the adjustment to the same column.
			//
			for ( int c = 0; c < ( 0.0d != factor ? 1 : 2 ); c++ )
			{
				for ( int i = 0; i < this.OrderedHeaders.Count; i++ )
				{
					if ( adjustment == 0 )
						return;

					if ( this.OrderedHeaders[i].Hidden )
						continue;

					// SSP 12/10/02 UWG1768
					// Skip groups and columns for which proportional resize is truned off.
					//
					// --------------------------------------------------------------------
					if ( this.OrderedHeaders[i] is GroupHeader )
					{
						UltraGridGroup group = this.OrderedHeaders[i].Group;
					
						if ( null != group && !group.ProportionalResizeResolved )
							continue;
					}

					if ( this.OrderedHeaders[i] is ColumnHeader )
					{
						UltraGridColumn column = this.OrderedHeaders[i].Column;

						if ( null != column && !column.ProportionalResizeResolved )
							continue;
					}
					// --------------------------------------------------------------------

					// get the resize range
					//
					this.OrderedHeaders[i].GetResizeRange( ref minWidth, ref maxWidth );

					// get the old width
					//
					int oldWidth		= this.OrderedHeaders[i].Extent;
					int headerAdjustment;

					// if the passed in factor is 0.0 (second pass) then try to apply the
					// entire adjustment to the column
					//
					if ( factor == 0.0d )
					{
						// SSP 9/24/03 UWG2455
						// Don't keep applying the adjustment to the same column.
						//
						// ------------------------------------------------------------------
						if ( 0 == c )
						{
							if ( i <= this.autoFit_columnLastAdjustmentFullyAppliedToIndex )
								continue;
						}
						
						this.autoFit_columnLastAdjustmentFullyAppliedToIndex = i;
						// ------------------------------------------------------------------

						headerAdjustment = adjustment;
					}
					else
					{
						// calculate the adjustment based on the factor
						//
						// SSP 9/24/03 UWG2455
						// Don't keep applying the adjustment to the same column.
						//
						// ----------------------------------------------------------------------
						//headerAdjustment = (int)(factor * oldWidth) - oldWidth;
						int rowSelectorAdjustment = this.OrderedHeaders[i].RowSelectorExtent;
						int actualColWidth = oldWidth - rowSelectorAdjustment;

						// SSP 12/3/04 BR00458 BR00459 BR00551
						// Calculate the adjustment in double instead of just the new width.
						//
						//headerAdjustment = (int)(factor * actualColWidth) - actualColWidth;
						headerAdjustment = (int)( factor * actualColWidth - actualColWidth );
						// ----------------------------------------------------------------------

						// make sure it isn't greater than original adjustment
						//
						if ( adjustment < 0 )
							headerAdjustment = Math.Max( headerAdjustment, adjustment );
						else
							headerAdjustment = Math.Min( headerAdjustment, adjustment );
					}

					int newWidth = oldWidth + headerAdjustment;

					// make sure the new width is in the allowable range
					//
					if ( maxWidth > 0 )
						newWidth = Math.Min( maxWidth, newWidth );

					newWidth = Math.Max( minWidth, newWidth );

					if ( newWidth != oldWidth )
					{
						// apply the adjustment
						//
						this.OrderedHeaders[i].InternalSetWidth( newWidth, true ); 

						// recalc the adjustment from the the new extent
						//
						headerAdjustment = this.OrderedHeaders[i].Extent - oldWidth;

						// Adjust all the origins of the headers to the right to
						// acount for the adjustment to this header 
						//
						for ( int j = i + 1; j < this.OrderedHeaders.Count; j++ )
						{
							if ( this.OrderedHeaders[j].Hidden )
								continue;

							this.OrderedHeaders[j].SetOrigin( this.OrderedHeaders[j].Origin + headerAdjustment );
						}

						// reduce the adjustment amount
						//
						adjustment -= headerAdjustment;
					}
				}
			}
		}
		#endregion ApplyAdjustmentToColumns

		#region GetColumnAtOverallPosition

		internal UltraGridColumn GetColumnAtOverallPosition( int position )
		{

			if ( position < 0 )
				return null;

			// MD 1/21/09 - Groups in RowLayouts
			//if ( this.GroupsDisplayed )
			if ( this.GroupsDisplayed && this.RowLayoutStyle != RowLayoutStyle.GroupLayout )
			{
				// Lopp over the groups until we come to the group that
				// contains the column at the requested position
				//
				for ( int i = 0; i < this.OrderedGroupHeaders.Count; i++ )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( position < this.OrderedGroupHeaders[i].Group.Columns.Count )
					//    return this.OrderedGroupHeaders[i].Group.Columns[position];
					//
					//position -= this.OrderedGroupHeaders[i].Group.Columns.Count;
					GroupColumnsCollection groupColumns = this.OrderedGroupHeaders[ i ].Group.Columns;

					if ( position < groupColumns.Count )
						return groupColumns[ position ];

					position -= groupColumns.Count;
				}
				
				return null;
			}

			if ( position < this.OrderedColumnHeaders.Count )
				return this.OrderedColumnHeaders[position].Column;

			return null;
		}

		#endregion GetColumnAtOverallPosition

		internal bool HookIntoList
		{
			get
			{
				// SSP 1/9/02 UWG916
				// If in design mode, don't hook into the binding list
				//
				// SSP 7/29/03 UWG2508
				// Check for nulls.
				//
				//if ( this.Layout.Grid.DesignMode )
				if ( null == this.Layout || null == this.Layout.Grid || this.Layout.Grid.DesignMode )
					return false;

				return this.hookIntoList;
			}
			set
			{
				this.hookIntoList = value;
			}
		}

		internal void InternalSetLastAddedRow( UltraGridRow row )
		{
			// SSP 1/22/02
			// also clear the lastAddedRow's IsAddRow flag before
			// setting the new row's IsAddRow flag
			//
			if ( this.lastAddedRow != row )
			{
				if ( null != this.lastAddedRow )
					// SSP 8/23/02 UWG1434
					// Made IsAddRow public. However since it has to be read-only, added InternalSetIsAddRow
					// internal method for setting it. So use the new InternalSetIsAddRow method instead.
					//
					//this.lastAddedRow.IsAddRow = false;
					this.lastAddedRow.InternalSetIsAddRow( false );

				this.lastAddedRow = row;

				// SSP 4/2/04 UWG2985
				// Changed the behavior of AddRowAppearance. Shade the cell back color to use with the
				// add-row only if template add-row functionality is turned on. When add-row feature 
				// is not turned on and the user adds a new row from outside, we don't want to all of
				// a sudded start showing the added row as gray where as in version 3.0 we used to
				// show it the same back color as the regular rows.
				//
				if ( null != this.lastAddedRow )
					this.lastAddedRowAddedThroughGrid = this.inAddNew;
				else
					this.lastAddedRowAddedThroughGrid = false;
			}
		}

		/// <summary>
		/// Returns the key property
		/// </summary>
		public override string ToString()
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			// AS 1/8/03 - fxcop
			// Do not compare against string.empty - test the length instead
			//if (this.Key == null || this.Key == string.Empty)
			if (this.Key == null || this.Key.Length == 0)
			{
				sb.Append( "Band[" );
				sb.Append( this.Index );
				sb.Append( "]" );
			}
			else
				sb.Append( this.Key );

			// AS - 11/14/01 UWG729
			if (this.Hidden)
				sb.Append( " [hidden]" );

			return sb.ToString();
		}

		/// <summary>
		/// Called when the object is disposed of
		/// </summary>
		protected override void OnDispose()
		{
			UnWireDataSource();

			// unhook notifications and call dispose on the header object
			//
			if ( null != this.header )
			{
				this.header.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.header.Dispose();
				this.header = null;
				
			}
            
			// unhook notifications and call dispose on the Override object
			//
			if ( null != this.overrideObj )
			{
				this.overrideObj.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.overrideObj.Dispose();
				this.overrideObj = null;
			}

			// unhook notifications and call dispose on the columns collection
			//
			if ( null != this.columns )
			{
				this.columns.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.columns.Dispose();
				this.columns = null;
			}

			// unhook notifications and call dispose on the groups collection
			//
			if ( null != this.groups )
			{
				this.groups.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.groups.Dispose();
				this.groups = null;
			}

            // MRS 12/5/2008 - TFS11243
            if (null != this.rowLayouts)
            {
                this.rowLayouts.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
                this.rowLayouts.Dispose();
                this.rowLayouts = null;
            }

			this.orderedColumns = null;
			this.orderedGroups = null;

			base.OnDispose();
		}

		/// <summary>
		/// Returns true if any properties on this object or any of
		/// its sub objects is set to a non-default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerialize() 
		{
            return this.ShouldSerializeAddButtonCaption() ||
                this.ShouldSerializeAddButtonToolTipText() ||
                this.ShouldSerializeAutoPreviewField() ||
                this.ShouldSerializeAutoPreviewEnabled() ||
                this.ShouldSerializeAutoPreviewMaxLines() ||
                this.ShouldSerializeColHeaderLines() ||
                this.ShouldSerializeColHeadersVisible() ||
                this.ShouldSerializeExpandable() ||
                this.ShouldSerializeIndentation() ||
                this.ShouldSerializeGroupHeaderLines() ||
                this.ShouldSerializeGroupHeadersVisible() ||
                this.ShouldSerializeHeaderVisible() ||
                this.ShouldSerializeLevelCount() ||
                this.ShouldSerializeScrollTipField() ||
                this.ShouldSerializeOverride() ||
                this.ShouldSerializeColumns() ||
                this.ShouldSerializeGroups() ||
                this.ShouldSerializeHeader() ||
                this.ShouldSerializeHidden() ||
                this.ShouldSerializeSortedColumns() ||
                this.ShouldSerializeCardView() ||
                this.ShouldSerializeCardSettings() ||
                this.ShouldSerializeAutoPreviewIndentation() ||
                this.ShouldSerializeTag() ||
                this.ShouldSerializeSummaryFooterCaption() ||

                // SSP 7/26/02 UWG1366 UWG1367
                // Also check for column filters and summaries.
                //
                this.ShouldSerializeColumnFilters() ||
                this.ShouldSerializeSummaries() ||
                // JAS 2005 v2 XSD Support
                //
                this.ShouldSerializeMinRows() ||
                this.ShouldSerializeMaxRows() ||
                // SSP 1/30/03 - Row Layout Functionality
                //
                this.ShouldSerializeUseRowLayout() ||
                this.ShouldSerializeRowLayoutLabelPosition() ||
                this.ShouldSerializeRowLayoutLabelStyle() ||
                this.ShouldSerializeRowLayouts() ||
                // SSP 3/14/05 - NAS 5.2 Outlook Style Fixed Add Row
                //
                this.ShouldSerializeSpecialRowPromptField() ||

                // JAS 2005 v2 GroupBy Row Extensions
                //
                this.ShouldSerializeIndentationGroupByRow() ||
                this.ShouldSerializeIndentationGroupByRowExpansionIndicator() ||

                // SSP 6/17/05 - NAS 5.3 Column Chooser
                // 
                this.ShouldSerializeExcludeFromColumnChooser() ||

                // MBS 3/12/08 - RowEditTemplate NA2008 V2
                this.ShouldSerializeRowEditTemplate() ||

                // MRS - NAS 9.1 - Groups in RowLayout
                this.ShouldSerializeRowLayoutStyle();
		}
 
		/// <summary>
		/// Resets all properties on this object and all of
		/// its sub objects to their default value
		/// </summary>
		public void Reset() 
		{
			this.ResetAddButtonCaption();
			this.ResetAddButtonToolTipText();
			this.ResetAutoPreviewField();
			this.ResetAutoPreviewEnabled();
			this.ResetAutoPreviewMaxLines();
			this.ResetColHeaderLines();
			this.ResetColHeadersVisible();
			this.ResetExpandable();
			this.ResetIndentation();
			this.ResetGroupHeaderLines();
			this.ResetGroupHeadersVisible();
			this.ResetHeaderVisible();
			this.ResetLevelCount();
			this.ResetScrollTipField();
			this.ResetOverride();

			// SSP 8/6/02 UWG1481
			// Also reset the sorted columns collection.
			//
			this.ResetSortedColumns( );

			// SSP 3/30/04
			// Reset the groups before resetting the column. Before it used sometimes cause
			// series of asserts because column's level settings were reset before group's
			// were cleared. If this causes a problem, revert back to the calling 
			// ResetColumns before ResetGroups and take care of the assert some other way.
			//
			//this.ResetColumns();
			//this.ResetGroups();
			this.ResetGroups();
			this.ResetColumns();			

			this.ResetHeader();
			this.ResetHidden();
			this.ResetLevelCount();
			this.ResetAutoPreviewIndentation();
			this.ResetCardView();
			this.ResetCardSettings();

			this.ResetSummaryFooterCaption( );			

			this.orderedColumns = null;
			this.orderedGroups = null;

			// SSP 3/21/02
			// Version 2 Row Filter Implementation
			// NOTE: There is no associated ShouldSerialize method because
			// the column filters are strictly for runtime.
			//
			this.ResetColumnFilters( );
			// SSP 7/26/02 UWG1367
			// Reset the summaries as well.
			//
			this.ResetSummaries( );

			// JAS 2005 v2 XSD Support
			this.ResetMinRows();
			this.ResetMaxRows();
			this.XsdSuppliedConstraints = 0;

			// SSP 1/30/03 - Row Layout Functionality
			//
			this.ResetUseRowLayout( );
			this.ResetRowLayouts( );
			this.ResetRowLayoutLabelPosition( );
			this.ResetRowLayoutLabelStyle( );
			// SSP 3/14/05 - NAS 5.2 Outlook Style Fixed Add Row
			//
			this.ResetSpecialRowPromptField( );

			// JAS 2005 v2 GroupBy Row Extensions
			//
			// ----------------------------------------------------------------------------
			this.ResetIndentationGroupByRow();
			this.ResetIndentationGroupByRowExpansionIndicator();
			// ----------------------------------------------------------------------------

			// SSP 6/17/05 - NAS 5.3 Column Chooser
			// 
			this.ResetExcludeFromColumnChooser( );

            // MBS 3/12/08 - RowEditTemplate NA2008 V2
            this.ResetRowEditTemplate();

            // MRS NAS 9.1 - Sibling Band Order
            // Added a VisiblePosition property to the band. 
            //
            this.ResetVisiblePosition();

            // MRS - NAS 9.1 - Groups in RowLayout
            this.ResetRowLayoutLabelStyle();
		}

		/// <summary>
		/// Returns the associated <see cref="Infragistics.Win.UltraWinGrid.BandHeader"/> object which represents this band's header.
		/// </summary>
		/// <remarks>
		/// Returns the header associated with the band. It contains the Key as it's caption which can be set using <see cref="Infragistics.Win.UltraWinGrid.HeaderBase.Caption"/>. <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand.HeaderVisible"/> must be set to true to make the Header visible.
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.HeaderVisible"/>
		/// </remarks>
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ] 
		[LocalizedDescription("LD_UltraGridBand_P_Header")]
		[LocalizedCategory("LC_Display")]
		public Infragistics.Win.UltraWinGrid.BandHeader Header
		{
			get
			{
				
				if ( null == this.header )
				{
					this.header = new BandHeader( this );

					// hook up the prop change notifications
					//
					this.header.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.header;
			}
		}
 
		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHeader() 
		{
			return ( null != this.header && 
				this.header.ShouldSerialize() );
		}
 
		/// <summary>
		/// Resets Header
		/// </summary>
		public void ResetHeader() 
		{
			if ( null != this.header )
				this.header.Reset();
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyCell
		{ 
			get
			{
				SelectionStrategyBase strategy = null;

				// if there is an override object then get its strategy
				//
				if ( this.HasOverride )
					strategy = ((ISelectionStrategyProvider)(this.overrideObj)).SelectionStrategyCell;

				// if the strategy wasn't explicitly set then get the
				// default strategy from the layout
				//
				if ( strategy == null )
					strategy = ((ISelectionStrategyProvider)(this.layout)).SelectionStrategyCell;

				return strategy;
			}
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyRow
		{ 
			get
			{
				SelectionStrategyBase strategy = null;

				// if there is an override object then get its strategy
				//
				if ( this.HasOverride )
					strategy = ((ISelectionStrategyProvider)(this.overrideObj)).SelectionStrategyRow;

				// if the strategy wasn't explicitly set then get the
				// default strategy from the layout
				//
				if ( strategy == null )
					strategy = ((ISelectionStrategyProvider)(this.layout)).SelectionStrategyRow;

				return strategy;
			}
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyGroupByRow
		{ 
			get
			{
				SelectionStrategyBase strategy = null;

				// if there is an override object then get its strategy
				//
				if ( this.HasOverride )
					strategy = ((ISelectionStrategyProvider)(this.overrideObj)).SelectionStrategyGroupByRow;

				// if the strategy wasn't explicitly set then get the
				// default strategy from the layout
				//
				if ( strategy == null )
					strategy = ((ISelectionStrategyProvider)(this.layout)).SelectionStrategyGroupByRow;

				return strategy;
			}
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyColumn
		{ 
			get
			{
				SelectionStrategyBase strategy = null;

				// if there is an override object then get its strategy
				//
				if ( this.HasOverride )
					strategy = ((ISelectionStrategyProvider)(this.overrideObj)).SelectionStrategyColumn;

				// if the strategy wasn't explicitly set then get the
				// default strategy from the layout
				//
				if ( strategy == null )
					strategy = ((ISelectionStrategyProvider)(this.layout)).SelectionStrategyColumn;

				return strategy;
			}
		}

		/// <summary>
		/// Returns the associated <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout"/> object.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>UltraGridBand</b> keeps a back reference to the <see cref="UltraGridLayout"/> it belongs to.
		/// The <b>Layout</b> property returns this back reference to the <b>UltraGridLayout</b>.
		/// </p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]	
		public UltraGridLayout Layout
		{
			get 
			{
				
				return this.layout;
			}
		}

		/// <summary>
		/// The internally assigned key value for the band. The <b>Key</b> property is read-only for the <b>Band</b> object.
		/// </summary>
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public override String Key
		{
			// JJD 8/6/01 - UWG117
			// Added get routine for key property
			//
			get
			{
				return base.Key;
			}
			set
			{
				if ( this.Layout != null &&
					// SSP 3/10/06
					// Added the following line. Allow setting the band's and column's Key at design time.
					// This is useful when one creates a data structure by "Manually Define Data Schema"
					// feature of the designer and then decides to change the key of a column or a band 
					// because the runtime data source will have a different name for the band or the 
					// column.
					// 
					( null != this.Layout.Grid && null != this.Layout.Grid.DataSource ) &&
					// SSP 5/7/03 - Export Functionality
					//
					//( this.Layout.IsDisplayLayout || this.layout.IsPrintLayout ) )
					( this.Layout.IsDisplayLayout || this.layout.IsPrintLayout || this.layout.IsExportLayout ) )
					throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_3"));

				// MD 5/22/09 - TFS16438
				// Unhook the calc references before the band key changes. We can unhook and rehook them afterwards 
				// because we won't be able to find the references when the band key changes since their absolute 
				// names will have changed.
				this.RemoveCalcReferences();

				base.Key = value;

				// MD 5/22/09 - TFS16438
				// Hook the calc references in the band again. They now have different absolute names, so they are 
				// technically new references to the band.
				this.AddCalcReferences();
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value. <b>ShouldSerializeKey</b> always returns False for the <b>Band</b> object since the band key can never be changed.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		public override bool ShouldSerializeKey()
		{
			return false;
		}

		/// <summary>
		/// Resets the key to its default value. This method does nothing for the <b>Band</b> object since the band key can never be changed.
		/// </summary>
		public override void ResetKey()
		{
			// Don't do anything. Key cannot be reset
		}

		/// <summary>
		/// Returns the parent UltraGridBand object of the current band (if it is a child band) or Null for band 0. This property is read-only.
		/// </summary>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public UltraGridBand ParentBand
		{
			get 
			{
				return this.parentBand;
			}
		}

		#region IsRootBand

		// SSP 3/30/05
		//
		internal bool IsRootBand
		{
			get
			{
				return null == this.parentBand;
			}
		}

		#endregion // IsRootBand

		internal void SetParentBand( UltraGridBand band )
		{
			this.parentBand = band;
		}

		internal int ParentIndex
		{
			get
			{
				// JJD 11/21/01
				// If we have a parentBand set then return its index
				//
				if ( this.parentBand != null )
					return this.parentBand.Index;

				// Otherwise, return the serialized value
				//
				return this.parentIndex;
			}
		}

		
		
		/// <summary>
		/// Returns the UltraGridColumn object for the chaptered column in the current band's parent (if the current band is a child band) or Null for band 0. This property is read-only.
		/// </summary>
		/// <remarks>
		/// <p class="body">The chaptered column is the column which links the child band with the parent band. This column is typically invisible in the parent band.</p>
		/// </remarks>
		[ Browsable(false) ] 
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public Infragistics.Win.UltraWinGrid.UltraGridColumn ParentColumn
		{
			get 
			{
				// AS - 12/6/01
				// If this is the first time that the parent column is requested
				// after deserializing, we will need to find the parentcolumn
				// in the parent band's columns collection.
				//
				if (this.parentColumn == null && this.parentBand != null)
				{
					foreach (UltraGridColumn column in this.parentBand.Columns)
					{
						if (column.Key == this.Key)
						{
							this.parentColumn = column;
							break;
						}
					}
				}
				return this.parentColumn;
			}
		}

		private void DataSource_BindingsChanged( object sender, CollectionChangeEventArgs e )
		{
			Debug.WriteLine( "DataSource_BindingsChanged" );
		}


		private void EnsureProperlyHookedIntoBindingListListChanged( )
		{			
			// SSP 1/18/02
			// Added this method.
			// The List off the BindingManager is not persistant. It keeps
			// changing as the active row in the parent binding manager changes.
			// So we need to rehook into the binding list every time it changes.
			//
			IBindingList bindingList = this.BindingList;

			if ( this.lastBindingList != bindingList )
			{
				if ( null != this.lastBindingList )
					this.lastBindingList.ListChanged -= this.listChangedHandler;

				if ( null != bindingList )
					bindingList.ListChanged += this.listChangedHandler;
			}

			this.lastBindingList = bindingList;
		}

		// SSP 5/28/02 UWG1141
		//
		private void UnHookFromBindingList( )
		{
			if ( null != this.lastBindingList )
				this.lastBindingList.ListChanged -= this.listChangedHandler;
			this.lastBindingList = null;
		}

		// called when CurrencyManager.ItemChanged event occurs
		private void DataSource_ItemChanged(object sender, ItemChangedEventArgs ea) 
		{
			// SSP 1/18/02
			// Look at the note in the definition of below function.
			//
			this.EnsureProperlyHookedIntoBindingListListChanged( );

			if ( binding_debug ) 
			{
				Debug.WriteLine( ++this.Layout.Grid.tmp + ": Bands[" + this.Index + "].DataSource_ItemChanged: " );
				Debug.WriteLine( "   ItemChangedEventArgs.Index = " + ea.Index );
				Debug.WriteLine( "   BindingManager.Position    = " + this.BindingManager.Position );		
				Debug.WriteLine( "   BindingManager.Count       = " + this.BindingManager.Count );
				if ( null != this.ParentBand )
					Debug.WriteLine( "   ParentBindingManager.Pos   = " + this.ParentBand.BindingManager.Position );
				//Debug.WriteLine( "   Bands[0].BindingManager == sender = " + ( sender ==  this.Layout.Bands[0].BindingManager  ) );
				//Debug.WriteLine( "   Bands[1].BindingManager == sender = " + ( sender ==  this.Layout.Bands[1].BindingManager  ) );
				//Debug.WriteLine( "   Bands[2].BindingManager == sender = " + ( sender ==  this.Layout.Bands[2].BindingManager  ) );
				Debug.WriteLine( "" );
			}

			// SSP 12/27/01
			// If the current row's parent row collection has not
			// been created yet, then don't cause it to be created
			// by below calls to GetCurrentParentRows and GetCurrentRow.
			// Besides, the current row won't be visible anyways since
			// it hasn't been created yet.
			//
			if ( !this.IsCurrentRowRowsCollectionCreated( ) )
				return;

			// SSP 6/4/02
			// Summary rows feature. Call InternalRefreshSummaries so it can bump any 
			// summary calculations versions. If we are bound to
			// IBindingList, then the rows collection itself will handle it. So only
			// do so if we are not bound to IBindingList.
			//
			if ( !this.IsBoundToIBindingList )
			{
				this.InternalRefreshSummaries( );
			}
			
			// SSP 11/20/01 UWG671
			// If IgnoreDataSourePositionChange flag is set, then we 
			// are internally setting the position, so ignore this event.
			//
			// SSP 12/28/01 
			// Commented below out because it's not needed because of
			// above if statement. This was put so that when they 
			// change from rows to rows, the child rows were being
			// needlessly initialized. However, Because of the above
			// if statemenet, if the child rows collection has not
			// been created yet, then we will return above. Thus this
			// becomes unnecessary. Not only it's uncessessary, but it
			// actually may cause problems in certain circumstances.
			//
			

			// JJD 8/28/01
			// If this is not a multiband display and the band is a child band then 
			// return 
			//
			if ( this.parentBand != null &&
				!this.layout.ViewStyleImpl.IsMultiBandDisplay )
				return;


			// JJD 12/21/01
			// Ignore notifications for hidden bands
			//
			if ( this.HiddenResolved )
				return;

			// SSP 10/28/02 UWG1787
			// Pass in true for dontSyncRows. Search for UWG1787 for more info.
			//
			//RowsCollection rows = this.GetCurrentParentRows( false );
			RowsCollection rows = this.GetCurrentParentRows( false, true );

			// SSP 5/28/02 UWG1141
			// Added code to account for situations where the list off the
			// binding manager changes and this band is the top most band.
			// When this band is the top most band and the List property 
			// off the binding manager changes, then we should reload the
			// rows from the bound list.
			// NOTE: We are rehooking into the right binding list above
			// where we make the call to EnsureProperlyHookedIntoBindingListListChanged
			// method.
			//
			// SSP 9/24/03 UWG2670
			// Commented out the first if block because we are initializing the lastBoundList
			// in the WireDataSource method.
			//
			 if ( this.List != this.lastBoundList )
					{
						this.lastBoundList = this.List;

						if ( null == this.ParentBand )
						{
							// If the list has changed, then rehook and dirty the rows and return.
							//
							Debug.Assert( null != rows && null != this.Layout, "Something's wrong here." );

							if ( null != rows && null != this.Layout )
							{
								rows.DirtyRows( );

								if ( null != this.Layout )
								{
									this.Layout.DirtyGridElement( );

									// SSP 1/21/05 BR01891 
									// If this is UltraCombo invalidate the edit portion as well so it can
									// reflect the new value if the value of the selected row changes.
									//
									this.Layout.DirtyComboArea( );
								}
				
								return;
							}
						}
							// SSP 2/6/04 UWG2945 UWG2947
							// Dirty the cached data list of the rows collection.
							// Added the following else-if block.
							//
						else if ( null != rows )
						{
							rows.DirtyDataList( );
						}
					}


			// SSP 12/28/01
			//if ( null != rows )
			//	rows.BindToList( );
		
			// SSP 6/24/04 UWG3438
			// Commented below code out.
			//
			

			if ( null != this.BindingManager && null != rows &&
				// SSP 10/16/01
				// Use ActualRowsCount instead of the Count, because when we have
				// group by rows, the number of group by rows has nothing to do with the
				// number of rows in the binding list. That's why we have ActualRowsCount
				// off the rows which actually returns the count in the unsorted
				// rows array list
				//
				//( /*if adding a row*/ this.inAddNew || !rows.IsRowsDirty ) && this.BindingManager.Count != rows.Count )
				// SSP 11/18/03 Add Row Feature
				// Don't check for this.inAddNew. Now we are explicitly calling SyncRows in AddNew so
				// we don't need to check for inAddNew anymore. Not only that it causes a minor problem
				// with add-row feature by causing a premature syncing.
				//
				//( /*if adding a row*/ this.inAddNew || !rows.IsRowsDirty ) 
				! rows.IsRowsDirty
				// SSP 1/24/03 UWG1963
				// If Index of the event args is -1 and the binding manager's position is -1, then dirty the rows
				// bacause it seems that's an indication of list changing.
				//
				//&& this.BindingManager.Count != rows.ActualRowsCount )
				&& ( this.BindingManager.Count != rows.ActualRowsCount || ( ea.Index < 0 && this.BindingManager.Position < 0 ) ) )
			{	
				// SSP 6/24/04 UWG3438
				// Enclosed the existing code in the if block. Only dirty the rows if we 
				// are not hooked into IBindingList.ListChanged because if we are then we
				// will receive the ItemAdded and ItemRemoved messages.
				//
				// SSP 10/19/04 UWG3744
				// If the count of either the binding manager or the rows is 0 then dirty the rows.
				// This is to workaround the bug where if you change the relation key of a parent row
				// all the child rows are deleted and then no further notifications are fired by the
				// binding manager.
				//
				//if ( ! this.HookIntoList )
				if ( ! this.HookIntoList || 0 == this.BindingManager.Count || 0 == rows.ActualRowsCount )
				{
					//if ( null != rows.ParentRow && rows.ParentRow.Expanded)
					if ( null == rows.ParentRow || rows.ParentRow.Expanded )
						// SSP 10/8/01
						// Dirtying the rows will be more than enough as it will
						// make sure that rows are Synced whenever a property off
						// the rows collection is accessed. This way we don't resync
						// every time we get a message
						//
						//rows.SyncRows( );
						rows.DirtyRows( );
					else
						// SSP 8/13/01
						// If parent row is not expanded, then we just dirty the rows
						// and ignore this event
						//
						rows.DirtyRows( );
				}

				// RobA UWG807 11/30/01
				// Need to invalidate the grid
				//
				// SSP 8/28/04 - Optimizations
				// Calling DirtyRows above invalidates the grid via call to 
				// DirtyParentRowScrollCount.
				//
				//this.layout.Grid.Invalidate();
			}

				// 9/21/01 UWG270
				// if the row was modified outside of the grid, we may get
				// this event so invalidate the row so it draws the updated
				// text
				//
				// SSP 10/16/01
				// Use ActualRowsCount instead of the Count, because when we have
				// group by rows, the number of group by rows has nothing to do with the
				// number of rows in the binding list. That's why we have ActualRowsCount
				// off the rows which actually returns the count in the unsorted
				// rows array list
				//
				//else if ( ea.Index >= 0 && ea.Index < rows.Count )
			else 
				// SSP 12/26/01
				// Check for rows being null.
				//
				//if ( ea.Index >= 0 && ea.Index < rows.ActualRowsCount )
				if ( null != rows && ea.Index >= 0 && 
				// SSP 6/6/03 UWG2232
				// If the user has suspended the row syncronization, then don't access ActualRowsCount 
				// which will cause the rows to get synchronized if they are dirty.
				// Added below condition.
				//
				! this.Layout.Grid.RowSynchronizationSuspended &&
				// SSP 9/8/05 BR06200
				// Skip if rows collection is dirty.
				// 
				! rows.IsRowsDirty &&
				ea.Index < rows.ActualRowsCount )
			{
				// SSP 10/29/03 UWG2725
				// Commented the original code out and added new code below. Check for null
				// as well as don't call GetRowWithListIndex twice.
				//
				// --------------------------------------------------------------------------
				
				UltraGridRow row = rows.GetRowWithListIndex( ea.Index );
				if ( null != row )
				{
					// SSP 4/11/07 BR21889
					// Make sure we reget the list object.
					// 
					row.InternalDecrementVerifiedVersion( );

					// SSP 8/3/04 - UltraCalc
					// Notify the calc manager of the change the row's value.
					//
					// ------------------------------------------------------------------
					row.ParentCollection.NotifyCalcManager_ValueChanged( row );
					// ------------------------------------------------------------------

					row.FireInitializeRow( );

					// SSP 3/24/05 BR02996
					// Only invalidate if the bound list is not an ultra data source. RowsCollection's 
					// OnListChangedHelper method will do the invalidation in the case of the ultra data 
					// source. Not only that it will only invalidate the cell instead of the whole row.
					// 
					//row.InvalidateItemAllRegions( );
                    //
                    // MBS 10/6/08 - TFS8567
                    // Since we now have full use of the CLR2's addition of the PropertyDescriptor on the 
                    // ListChangedEventArgs in the RowsCollection's OnListChangedHelper (i.e. we don't
                    // need to support CLR1.x), there really isn't any reason to invalidate the entire
                    // row since if we can find the appropriate cell that changed, the RowsCollection
                    // will invalidate that, otherwise it will take care of invalidating the entire row
                    //
                    //if ( ! ( this.lastBoundList is Infragistics.Win.UltraWinDataSource.IUltraDataRowsCollection ) )
                    //    row.InvalidateItemAllRegions( );
				}
				// --------------------------------------------------------------------------
			}
			// SSP 3/11/05
			// This slows down navigation across parent rows (using arrow for example).
			//
			

			//MRS 1/7/05 - BR01573
			if (this.layout != null)
			{
				UltraCombo combo = this.layout.Grid as UltraCombo;
				if (combo != null)
					combo.UIElement.DirtyChildElements();
			}
		}


		private bool IsCurrentRowRowsCollectionCreated( )
		{
			if ( null != this.ParentBand )
			{
				if ( !this.ParentBand.IsCurrentRowRowsCollectionCreated( ) )
					return false;
			
				// SSP 10/28/02 UWG1787
				// Pass in true for dontSyncRows.
				//
				//UltraGridRow row = this.ParentBand.GetCurrentRow( false );
				UltraGridRow row = this.ParentBand.GetCurrentRow( false, true );
				if ( null != row && row.HaveChildRowsBeenCreated( this ) )
					return true;

				return false;
			}
			else
			{
				// For top most band, the Rows collection is always created
				// (Layout.Rows) so just return true.
				//
				return true;
			}
		}

		// called when BindingManger.CurrentChanged event occurs
		private void DataSource_CurrentChanged(object sender, EventArgs ea) 
		{
			if ( binding_debug )
			{
				Debug.WriteLine( ++this.Layout.Grid.tmp + ": Bands[" + this.Index + "].DataSource_CurrentChanged: " );
				Debug.WriteLine( "   BindingManager.Position = " + this.BindingManager.Position );		
				Debug.WriteLine( "   BindingManager.Count    = " + this.BindingManager.Count );
				//Debug.WriteLine( "   Bands[0].BindingManager == sender = " + ( sender ==  this.Layout.Bands[0].BindingManager  ) );
				//Debug.WriteLine( "   Bands[1].BindingManager == sender = " + ( sender ==  this.Layout.Bands[1].BindingManager  ) );
				//Debug.WriteLine( "   Bands[2].BindingManager == sender = " + ( sender ==  this.Layout.Bands[2].BindingManager  ) );
				Debug.WriteLine( "" );
			}


			// SSP 3/24/05 BR02996
			// Only invalidate if the bound list is not an ultra data source. RowsCollection's 
			// OnListChangedHelper method will do the invalidation in the case of the ultra data 
			// source. Not only that it will only invalidate the cell instead of the whole row.
			// 
			if ( this.lastBoundList is Infragistics.Win.UltraWinDataSource.IUltraDataRowsCollection )
				return;

			// SSP 12/27/01
			// If the current row's parent row collection has not
			// been created yet, then don't cause it to be created
			// by below calls to GetCurrentParentRows and GetCurrentRow.
			// Besides, the current row won't be visible anyways since
			// it hasn't been created yet.
			//
			if ( !this.IsCurrentRowRowsCollectionCreated( ) )
				return;


			// SSP 11/20/01 UWG671
			// If IgnoreDataSourePositionChange flag is set, then we 
			// are internally setting the position, so ignore this event.
			//
			if ( this.Layout.IgnoreDataSourePositionChange )
				return;
			
			
			// JJD 8/28/01
			// If this is not a multiband display and the band is a child band then 
			// return 
			//
			if ( this.parentBand != null &&
				!this.layout.ViewStyleImpl.IsMultiBandDisplay )
				return;

			// JJD 12/21/01
			// Ignore notifications for hidden bands
			//
			if ( this.HiddenResolved )
				return;

			// SSP 10/28/02 UWG1787
			// Pass in true for dontSyncRows. Search for UWG1787 for more info.
			//
			//RowsCollection rows = this.GetCurrentParentRows( false );
			RowsCollection rows = this.GetCurrentParentRows( false, true );

			// SSP 8/13/01
			// If parent row is not expanded, then we ignore this event
			//
			if ( null != rows && null != rows.ParentRow && !rows.ParentRow.Expanded )
			{
				return;
			}

			// SSP 10/28/02 UWG1787
			// Pass in true for dontSyncRows. Search for UWG1787 for more info.
			//
			//UltraGridRow row = this.GetCurrentRow( false );
			UltraGridRow row = this.GetCurrentRow( false, true );

			if ( null != row )
			{
				row.InvalidateItemAllRegions( );
			}
		}

		private void DataSource_PositionChanged(object sender, EventArgs ea) 
		{
			if ( binding_debug )
			{
				Debug.WriteLine( ++this.Layout.Grid.tmp + ": Bands[" + this.Index + "].DataSource_PositionChanged: " );
				Debug.WriteLine( "   BindingManager.Position = " + this.BindingManager.Position );			
				Debug.WriteLine( "   BindingManager.Count    = " + this.BindingManager.Count );			
				if ( null != this.ParentBand )
					Debug.WriteLine( "   ParentBindingManager.Pos   = " + this.ParentBand.BindingManager.Position );
				//Debug.WriteLine( "   Bands[0].BindingManager == sender = " + ( sender ==  this.Layout.Bands[0].BindingManager  ) );
				//Debug.WriteLine( "   Bands[1].BindingManager == sender = " + ( sender ==  this.Layout.Bands[1].BindingManager  ) );
				//Debug.WriteLine( "   Bands[2].BindingManager == sender = " + ( sender ==  this.Layout.Bands[2].BindingManager  ) );
				Debug.WriteLine( "" );
			}

			// SSP 1/21/02 UWG927
			// Whenever we get position changed event, reset the last added row's
			// IsAddRow flag to false because it's committed (the binding manager
			// commits whenever the position changes).
			//
			if ( null != this.lastAddedRow 
				// SSP 12/9/05 BR07901
				// If the new row was inserted somewhere other than at the end of the list
				// then the binding manager doesn't re-position itself to the inserted row.
				// Instead it raises PositionChanged with some other position. In such
				// a scenario, we should not be resetting the add row's IsAddRow. Therefore
				// check to see if we are currently in the process of adding a new row.
				// Added the below condition.
				// 
				&& ! this.inAddNew
				// SSP 1/22/02 UWG962
				// Do so only when the position that's being changed to is not
				// the position of the lastAddedRow because this event may get 
				// fired after we set the lastAddedRow.
				//
				&& this.lastAddedRow.ListIndex != this.BindingManager.Position )
			{
				// SSP 8/23/02 UWG1434
				// Made IsAddRow public. However since it has to be read-only, added InternalSetIsAddRow
				// internal method for setting it. So use the new InternalSetIsAddRow method instead.
				//
				//this.lastAddedRow.IsAddRow = false;
				this.lastAddedRow.InternalSetIsAddRow( false );
				this.lastAddedRow = null;
			}

			// SSP 6/2/05 BR03609
			// If the binding context indexer threw an exception while creating a child band
			// (due to lack of rows in the parent band for example) then see if we can create
			// the child band now.
			// 
			this.EnsureChildBandsLoaded( );

			// SSP 12/27/01
			// If the current row's parent row collection has not
			// been created yet, then don't cause it to be created
			// by below calls to GetCurrentParentRows and GetCurrentRow.
			// Besides, the current row won't be visible anyways since
			// it hasn't been created yet.
			//
			if ( !this.IsCurrentRowRowsCollectionCreated( ) )
				return;

			//RobA UWG326 9/21/01 we don't want to do anything here if we
			//are ignoring this event
			if ( this.Layout.IgnoreDataSourePositionChange )
				return;
			
			// JJD 8/28/01
			// If this is not a multiband display and the band is a child band then 
			// return 
			//
			if ( this.parentBand != null &&
				!this.layout.ViewStyleImpl.IsMultiBandDisplay )
				return;

			// JJD 12/21/01
			// Ignore notifications for hidden bands
			//
			if ( this.HiddenResolved )
				return;

			// SSP 10/28/02 UWG1787
			// Pass in true for dontSyncRows. Search for UWG1787 for more info.
			//
			//RowsCollection rows = this.GetCurrentParentRows( false );
			RowsCollection rows = this.GetCurrentParentRows( false, true );

			// SSP 5/13/04 - Virtual Binding Optimizations
			// Commented out following code and added the necessary code in the if block below.
			//
			


			// SSP 10/8/01
			// PositionChanged get's fired before ItemChanged when a row
			// is deleted. This creates situations where we have a row
			// in the rows collection that does not exist in the binding
			// list and we try to repaint or something.
			// So decided to also sync the rows here as well.
			//			
			if ( null != this.BindingManager && null != rows &&
				// SSP 10/16/01
				// Use ActualRowsCount instead of the Count, because when we have
				// group by rows, the number of group by rows has nothing to do with the
				// number of rows in the binding list. That's why we have ActualRowsCount
				// off the rows which actually returns the count in the unsorted
				// rows array list
				//
				//( /*if adding a row*/ this.inAddNew || !rows.IsRowsDirty ) && this.BindingManager.Count != rows.Count )
				// SSP 11/18/03 Add Row Feature
				// Don't check for this.inAddNew. Now we are explicitly calling SyncRows in AddNew so
				// we don't need to check for inAddNew anymore. Not only that it causes a minor problem
				// with add-row feature by causing a premature syncing.
				//
				//( /*if adding a row*/ this.inAddNew || !rows.IsRowsDirty ) 
				! rows.IsRowsDirty 
				&& this.BindingManager.Count != rows.ActualRowsCount )
			{	
				// SSP 5/13/04 - Virtual Binding Optimizations
				// If the underlying list is an IBindingList then don't dirty the rows 
				// collection. We will get the appropriate ListChanged notifications.
				//
				if ( ! this.HookIntoList )
				{
					//if ( null != rows.ParentRow && rows.ParentRow.Expanded)
					if ( null == rows.ParentRow || rows.ParentRow.Expanded )
						// SSP 10/8/01
						// Dirtying the rows will be more than enough as it will
						// make sure that rows are Synced whenever a property off
						// the rows collection is accessed. This way we don't resync
						// every time we get a message
						//
						//rows.SyncRows( );
						rows.DirtyRows( );
					else
						// SSP 8/13/01
						// If parent row is not expanded, then we just dirty the rows
						// and ignore this event
						//
						rows.DirtyRows( );
				}

				// RobA UWG807 11/30/01
				// Need to invalidate the grid
				//
				this.layout.Grid.Invalidate();
			}		


			// SSP 10/01/01
			// Only set the active row for UltraGrid. UltraCombo and UltraDropDown
			// should not be synced with the binding manager's position 
			//
			// SSP 5/18/05 BR03434
			// Added SyncWithCurrencyManager to prevent the UltraGrid from synchronizing with
			// the currency manager. When there are a lot of deep nested bands activating a 
			// different ancestor row caues a lot of slowdown.
			// 
			//if ( this.Layout.Grid is UltraGrid )
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			if ( null != grid && grid.SyncWithCurrencyManager )
			{
				if ( this.BindingManager != null &&
					this.BindingManager.Position >= 0 
					
					// SSP 4/12/02 UWG1085
					// Only set the active row if the rows collection is not dirty.
					// If the rows is dirty then a row is being added or deleted, and we could
					// potentially cause a premature syncing of rows (RowsCollection.SyncRows)
					// by calling GetCurrentRow. The way the active row is going to be set is
					// in RowsCollection.SyncRows or ins RowsCollection.OnListChanged. If the
					// rows.IsRowsDirty is true, then SyncRows will be called to sync the rows.
					// This is where the active row will be set.
					//
					&& ( null == rows || !rows.IsRowsDirty ) )
				{
					// get the new current row
					//
					// SSP 6/19/03 UWG2232
					// Related to SuspendRowSynchronization and ResumeRowSynchronization methods.
					// If the row syncronization is suspended, then pass in true for the dontSyncRows
					// flag.
					//
					//Infragistics.Win.UltraWinGrid.UltraGridRow currentRow = this.GetCurrentRow( false );
					Infragistics.Win.UltraWinGrid.UltraGridRow currentRow = 						
						! this.Layout.Grid.RowSynchronizationSuspended 
						? this.GetCurrentRow( false )
						: this.GetCurrentRow( false, true );

					// set the active row to it
					//
					if ( currentRow != null )
					{
                        // MBS 3/20/08 - RowEditTemplate NA2008 V2
                        // We need to force the template to close due to a change in the
                        // position in the data source
                        if (this.Layout.Grid.ActiveRow != null)
                            this.Layout.Grid.ActiveRow.forceTemplateCloseOnActionRowChanged = true;

						// SSP 10/8/01
						// Now we are doing delayed active row change. When deleting
						// multiple rows for example, we don't want to be
						// setting the active row multiple times causing the grid
						// to be repainted everytime
						//
						//this.layout.Grid.ActiveRow = currentRow;
						this.Layout.Grid.TempActiveRow = currentRow;                        
					}
				}		
			}

			// SSP 1/21/05 BR01891
			// Added SyncWithCurrencyManager property on UltraCombo. This will cause the 
			// UltraCombo to synchronize the selected row with the currency manager's Position.
			//
			if ( this.Layout.Grid is UltraCombo )
				((UltraCombo)this.Layout.Grid).SyncSelectedRowWithCurrencyManager( true );
		}		

		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetCurrentRow( bool ensureExpanded )
		{
			// By default pass in false for dontSyncRows to not the change the 
			// behaviour of the original method.
			//
			return this.GetCurrentRow( ensureExpanded, false );
		}

		// SSP 10/28/02 UWG1787
		// Added overloads of GetCurrentParentRows and GetCurrentRow that take in the new dontSyncRows 
		// paramter. DontSyncRows indicates that we don't sync the rows in the process.
		// This we needed to do because the CurrencyManager changes the value of Position property 
		// without firing thbe PositionChanged event. It does fire, however it fires after changing
		// the value of Position. For example, accessing Position property may return 1 and next time
		// you access it, it may return 4 without having fired any PositionChanged event. It does fire
		// later on, but we end up syncing the wrong rows prematurely.
		//

		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetCurrentRow( bool ensureExpanded, bool dontSyncRows )
		{
			// if the binding manager isn't set then just return null
			//
			if ( this.bindingManager == null )
				return null;

			// if we don't have a parent band then get get the layout's
			// main rows collection (band 0)
			//
			if ( this.parentBand == null )
			{
				// SSP 10/28/02 UWG1787
				// If the new parameter dontSyncRows says not to syncm the call ActualRowsCount_NoSync.
				//
				int actualRowsCount = !dontSyncRows 
					? this.layout.Rows.ActualRowsCount 
					: this.layout.Rows.ActualRowsCount_NoSync;

				//if ( this.bindingManager.Position < this.layout.Rows.ActualRowsCount )
				if ( this.bindingManager.Position < actualRowsCount )
				{
					// SSP 9/5/01 positions of rows in rows collection do not
					// necessarily correspond to the bindingManager.Position
					// (in the case where rows collection is sorted)
					//
					//return this.layout.Rows[ this.bindingManager.Position ];
					// SSP 10/28/02 UWG1787
					// Pass along the new dontSyncRows parameter.
					//
					//return this.layout.Rows.GetRowWithListIndex( this.bindingManager.Position );
					return this.layout.Rows.GetRowWithListIndexHelper( this.bindingManager.Position, dontSyncRows );
				}
				else
				{	
					// SSP 8/27/01 - All this is done in RowsCollection.SyncRows function
					
					return null;
				}
			}

			// call this method on out parent band to get its current row
			//
			// SSP 10/28/02 UWG1787
			// Pass along the new dontSyncRows parameter.
			//
			//Infragistics.Win.UltraWinGrid.UltraGridRow parentRow = this.parentBand.GetCurrentRow( ensureExpanded );
			Infragistics.Win.UltraWinGrid.UltraGridRow parentRow = this.parentBand.GetCurrentRow( ensureExpanded, dontSyncRows );

			if ( parentRow != null )
			{
				if ( ensureExpanded && !parentRow.Expanded )
				{
					// expand the parent row
					//
					parentRow.Expanded = true;

					// if the expansion was cancelled then return null
					//
					if ( !parentRow.Expanded )
						return null;
				}

				// get the childband from the parent row that manages
				// this band's child rows
				//
				UltraGridChildBand childBand = parentRow.ChildBands[ this ];

				if ( childBand != null && this.bindingManager.Position >= 0 )
				{
					// SSP 9/5/01
					// Use the actual rows, not ChildBand.Rows, because ChildBand.Rows
					// could return GroupByRows which we don't want here.
					//
					//if ( this.bindingManager.Position < childBand.Rows.Count )
					// SSP 10/28/02 UWG1787
					// If the new parameter dontSyncRows says not to syncm the call ActualRowsCount_NoSync.
					//
					// MD 7/27/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//int actualRowsCount = !dontSyncRows 
					//    ? childBand.TopLevelRows.ActualRowsCount
					//    : childBand.TopLevelRows.ActualRowsCount_NoSync;
					RowsCollection topLevelRows = childBand.TopLevelRows;

					int actualRowsCount = !dontSyncRows
						? topLevelRows.ActualRowsCount
						: topLevelRows.ActualRowsCount_NoSync;

					//if ( this.bindingManager.Position < childBand.TopLevelRows.ActualRowsCount )
					if ( this.bindingManager.Position < actualRowsCount )
					{
						// return the row at the current position
						//
						// SSP 9/5/01 positions of rows in rows collection do not
						// necessarily correspond to the bindingManager.Position
						// (in the case where rows collection is sorted)
						// And also use the actual rows, not ChildBand.Rows, because ChildBand.Rows
						// could return GroupByRows which we don't want here.
						//
						//
						//return childBand.Rows[ this.bindingManager.Position ];
						// SSP 10/28/02 UWG1787
						// Pass along the new dontSyncRows parameter.
						//
						//return childBand.TopLevelRows.GetRowWithListIndex( this.bindingManager.Position );
						// MD 7/27/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//return childBand.TopLevelRows.GetRowWithListIndexHelper( this.bindingManager.Position, dontSyncRows );
						return topLevelRows.GetRowWithListIndexHelper( this.bindingManager.Position, dontSyncRows );
					}
					else
					{
						// SSP 8/27/01 - All this is done in RowsCollection.SyncRows function
						
					}
				}
			}

			return null;
		}

		internal void UnWireDataSource() 
		{
			if ( null != this.bindingManager ) 
			{
				if ( UltraGridBand.binding_debug )
					Debug.WriteLine( "UnWiring from DataSource." );


				CurrencyManager cm = this.BindingManager as CurrencyManager;

                // MBS 11/3/08 - TFS9064
                // Check for null
                if (cm != null)
                {
                    //this.bindingManager.CurrentChanged -= this.currentChangedHandler;
                    //this.bindingManager.PositionChanged -= this.positionChangedHandler;
                    cm.CurrentChanged -= this.currentChangedHandler;
                    cm.PositionChanged -= this.positionChangedHandler;
                    cm.ItemChanged -= this.itemChangedHandler;
                }

				this.bindingManager.Bindings.CollectionChanged -= this.bindingsChangedHandler;

				// SSP 5/28/02 UWG1141
				// Use the lastHookedInBindingList to unhook instead of
				// this.BindingList
				//
				//if ( this.BindingList != null )					
				//	this.BindingList.ListChanged -= this.listChangedHandler;
				this.UnHookFromBindingList( );

				// SSP 11/2/04 UWG3736
				// Null out the binding manager so the grid doesn't keep holding a reference back
				// to the binding manager even when the grid's DataSource is set to null.
				//
				this.bindingManager = null;
				this.lastBoundList = null;

                // MBS 4/28/08 - RowEditTemplate NA2008 V2
                // When we're unwiring from the DataSource, the band is no longer being used for
                // editing so we can get rid of the reference to the template.
                if (this.rowEditTemplate != null)
                {
                    this.rowEditTemplate.Initialize(null);
                    this.rowEditTemplate = null;
                }
			}

            // MBS 11/4/08 - TFS9064
            // Ensure that we're no longer holding onto the cached list of objects
            this.cachedList = null;
		}


		private void WireDataSource() 
		{
			if ( null != this.bindingManager ) 
			{
				// SSP 3/15/04 - Designer Usability Improvements Related
				// Don't hook into the binding manager/binding list events if we are extracting 
				// the data structure.
				//
				if ( null != this.Layout && this.Layout.ExtractDataStructureOnly )
					return;

				if ( UltraGridBand.binding_debug )
					Debug.WriteLine( "Wiring To DataSource." );

				CurrencyManager cm = this.BindingManager as CurrencyManager;

                // MBS 11/3/08 - TFS9064
                // Check for null
                if (cm != null)
                {
                    //this.bindingManager.CurrentChanged += this.currentChangedHandler;
                    //this.bindingManager.PositionChanged += this.positionChangedHandler;				
                    cm.CurrentChanged += this.currentChangedHandler;
                    cm.PositionChanged += this.positionChangedHandler;
                    cm.ItemChanged += this.itemChangedHandler;
                }
				
				this.bindingManager.Bindings.CollectionChanged += this.bindingsChangedHandler;
			
				// SSP 5/28/02 UWG1141
				// Call the Ensure method which keeps track of the list that it
				// hooks into so we unhook from that list properly later on.
				//
				//if ( this.BindingList != null )
				//	this.BindingList.ListChanged += this.listChangedHandler;
				this.EnsureProperlyHookedIntoBindingListListChanged( );

				// SSP 9/24/03 UWG2670
				// Set the lastBoundList to the list. In DataSource_ItemChanged event handler we
				// compare the current list against the lastBoundList and if they are different
				// then we dirty the rows.
				//
				this.lastBoundList = this.List;

                // MBS 4/28/08 - RowEditTemplate NA2008 V2
                // Notify the template that the columns have changed (because we're wired to a DataSource)
                if (this.rowEditTemplate != null)
                    this.rowEditTemplate.Initialize(this);
			}
		}

		internal void CalculateBandMetrics( UltraGridBand priorBand )
		{
			int relativeOrigin = 0;
			int overallExtent;
			bool firstItem = true;

			// SSP 11/10/05 BR07605
			// 
			this.bandExtent_BeforeAutoFitExtendLastColumn = 0;

			// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
			// Store the original extent so we can tell if it has changed later.
			int oldExtent = this.extent;

			// JJD 6/16/00
			// make sure the sync col lists are valid
			//
			this.Layout.ValidateSynchronizedColLists();

			// JJD 6/19/00
			// Initialize the band member variable that keeps track of
			// the cumulative adjustment to the width of synchronized
			// columns
			//
			synchronizationAdjustment     = 0;

			ColScrollRegion exclusiveRegion     = null;
			HeaderBase      lastVisibleHeader   = null;

            // MRS 2/9/2009 - TFS13636
			//HeadersCollection orderedHeaders = this.OrderedHeaders;
            // MRS 2/19/2009 - TFS14274
            //HeadersCollection orderedHeaders = this.UseRowLayoutResolved ? this.LayoutOrderedVisibleColumnHeaders : this.OrderedHeaders;
            List<HeaderBase> orderedHeaders = GetAllOrderedHeaders();

			ViewStyleBase viewStyle = this.Layout.ViewStyleImpl;

            // MRS 2/19/2009 - TFS14274
            // This is no longer needed. The call to GetAllOrderedHeaders (above) will
            // verify the layout, if needed. 
            //
            //// SSP 2/25/03 - Row Layout Functionality
            ////
            //if ( this.UseRowLayoutResolved )
            //    this.VerifyRowLayoutCache( );

			// Iterate over each header item to calculate its width and offset
			//
			foreach ( HeaderBase header in orderedHeaders )
			{
				// Init synchronization on each position item
				//
				header.InitSynchronization();

				if ( header.Hidden )
				{
					header.SetOrigin( 0 );
					continue;
				}

				// SSP 2/25/03 - Row Layout Functionality
				// Added if block and enclosed the alreay existing code in the else block.
				//
				if ( this.UseRowLayoutResolved )
				{
					// SSP 6/20/03
					//header.FirstItem = false;
					//header.LastItem = false;
					// MD 1/19/09 - Groups in RowLayout
					// The header can also be a GroupHeader, so the column would be null.
					//header.SetOrigin( header.Column.RowLayoutColumnInfo.CachedHeaderRect.Left );
					header.SetOrigin( ( (IProvideRowLayoutColumnInfo)header ).RowLayoutColumnInfo.CachedHeaderRect.Left );

                    // MRS 3/10/2009 - TFS14858
                    GroupHeader groupHeader = header as GroupHeader;
                    if (groupHeader != null)
                        groupHeader.Group.SetWidth(((IProvideRowLayoutColumnInfo)header).RowLayoutColumnInfo.CachedHeaderRect.Width, false, false, false, false);
				}
				else
				{
					exclusiveRegion = header.ExclusiveColScrollRegion;

					if ( null != exclusiveRegion )
					{
						exclusiveRegion.AdjustExclusiveWidth( header, viewStyle );
					}
					else
					{
						// First get the overall width. Hidden and chaptered columns will
						// have an overall width of 0
						//
						overallExtent = header.Extent;

						// Only set first item flag if overall width > 0 ( not hidden )
						//
						header.FirstItem = firstItem;
                    
						header.SetOrigin( relativeOrigin );

						header.LastItem = false;

						// Check the overall width before updating stack variables
						//
						if ( overallExtent > 0 )
						{
							// JJD 7/13/00
							// Reget the overall width since setting the first item
							// flag could affect the width
							//
							relativeOrigin += header.Extent;
                        
							firstItem = false;

							// Keep track of the last visible position item
							//
							lastVisibleHeader = header;
						}
					}
				}
			}

			// Set the last item flag on the last visisble item
			//
			if ( null != lastVisibleHeader )
				lastVisibleHeader.LastItem = true;

			// JJD 12/27/01
			// For card view bands use a minimum extent
			//
			if ( this.CardView )
				this.extent = 1;
			else
			{
				// SSP 2/19/03 - Row Layout Functionality
				// When using a layout manager, calculate the preferred size and use the width as
				// the extent.
				// Added following if block and enclosed already existing code in the else block.
				// 
				if ( this.UseRowLayoutResolved )
				{					
					this.extent = this.GetExtentLayout( );
				}
				else
				{
					this.extent = relativeOrigin;
				}
			}

			// If we didn't encounter any visible columns or groups then
			// allow enough space for the expansion indcators and the
			// row selectors
			//
			if ( this.extent < 1 )
				this.extent += this.RowSelectorExtent + this.PreRowAreaExtent;
			else
				this.extent += this.PreRowAreaExtent;

			// adjust band origin after width has been calculated
			//
			// SSP 3/31/03 - Row Layout Functionality
			// We want the row-layout designer to be able to design hidden bands.
			// For that to work, then band.GetExtent has to return a valid value.
			// So calculate the band extent even if the band is hidden in design
			// mode and when the row-layout functionality is enabled.
			// Skip band origin adjustment in such a mode.
			//
			if ( this.HiddenResolved && this.UseRowLayoutResolved &&					
				null != this.Layout.Grid && this.Layout.Grid.DesignMode )
				return;
			
			viewStyle.AdjustBandOrigin( this, 
				priorBand,
				ref this.origin );

			// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
			// Support HeaderClickAction in Combo and DropDown
			UltraDropDownBase ultraDropDownBase = this.Layout.Grid as UltraDropDownBase;
			if ( ultraDropDownBase != null && this.extent != oldExtent
				// SSP 10/15/06 BR15686
				// Don't call ReCalculateDropDownSize while initializing the band metrics.
				// BandsCollection.CalculateBandMetrics method does this now.
				// 
				&& ! this.Layout.ColScrollRegions.InitializingMetrics )
			{
				Debug.WriteLine(DateTime.Now.ToShortTimeString());
				// SSP 10/27/05 BR07016
				// Cause the drop down to resize to the new widths of the columns.
				// 
				//ultraDropDownBase.ReCalculateDropDownSize();
				// MRS 8/29/06 - BR15111
				//ultraDropDownBase.ReCalculateDropDownSize( true );
				ultraDropDownBase.ReCalculateDropDownSize( true, false );
			}

			// SSP 11/10/05 BR07605
			// 
			this.bandExtent_BeforeAutoFitExtendLastColumn = this.extent;
        }        

		internal void ResetBandOrigins( bool adjustChildBandsAlso )
		{

			// adjust this bands origin
			//
			this.layout.ViewStyleImpl.AdjustBandOrigin( this, 
				this.parentBand,
				ref this.origin );

			if ( !adjustChildBandsAlso )
				return;

			// Adjust child band origins before proceeding
			//
			for ( int i = this.Index + 1; i < this.Layout.SortedBands.Count; i++ )
			{
				UltraGridBand childBand = this.Layout.SortedBands[i];

				if ( this == childBand.ParentBand )
					childBand.ResetBandOrigins( adjustChildBandsAlso );
			}
		}

		internal int OverallWidth
		{
			get
			{
				return this.extent;
			}
		}

		internal void SynchronizeColWidths()
		{
					
			HeadersCollection orderedHeaders = this.OrderedHeaders;			

			ViewStyleBase viewStyle = this.Layout.ViewStyleImpl;

			Debug.Assert( null != viewStyle, "ViewStyle missing in Band.CalculateBandMetrics");

			// Iterate over each CPosition item so that it'c columns can sync up 
			//
			
			
			
			for ( int i = 0, count = orderedHeaders.Count; i < count; i++ )
			{
				HeaderBase header = orderedHeaders[i];

				// JJD 6/19/00
				// Synchronized each position item
				//
				header.Synchronize();
			}

			if ( 0 != this.synchronizationAdjustment )
			{
				// adjust the width of the band to allow for any column adjustments
				//
				this.extent  += this.synchronizationAdjustment;

				// adjust descendant band origins (applies to horz view styles only)
				//
				if ( null != viewStyle )
				{
					viewStyle.AdjustDescendantBandOrigins( this, this.synchronizationAdjustment );
					
				}

				this.synchronizationAdjustment = 0;
			}
		}

		internal void UpdateExclusizeColRegionWidths()
		{
			// JJD 1/23/02 - UWG971
			// For cardview bands call UpdateExclusiveWidthWithCardBand on
			// any exlusive col scroll regions
			//
			if ( this.CardView )
			{
				foreach ( ColScrollRegion csr in this.Layout.ColScrollRegions )
				{
					if ( csr.IsExclusive )
						csr.UpdateExclusiveWidthWithCardBand( this );
				}
				return;
			}

			ColScrollRegion exclusiveRegion;			
			
			HeadersCollection orderedHeaders = this.OrderedHeaders;

			
			// Iterate over each header
			//
			for ( int i = 0; i < orderedHeaders.Count; i++ )
			{
				HeaderBase header = orderedHeaders[i];

				if ( header.Hidden )
					continue;

				exclusiveRegion = header.ExclusiveColScrollRegion;

				// JJD 10/12/00 - ult2063
				// If this position item is part of an exclusive col region
				// call that region's UpdateExclusiveWidth method so that
				// the region can possibly update its overall width
				//
				if ( null != exclusiveRegion )
					exclusiveRegion.UpdateExclusiveWidth( header );
			}
		}
		internal bool HasChildBands( ) 
		{
			return this.HasChildBands( true );
		}
		internal bool HasChildBands( bool visibleBandsOnly ) 
		{
			// SSP 4/15/05 - Optimizations
			// Use GetChildBandCount to check if the band has any child bands. Why have
			// duplicate code. Besides code in GetChildBandCount is optimized.
			// Commented out the original code below.
			//
			return this.GetChildBandCount( visibleBandsOnly ) > 0;
			
		}
		internal int PreRowAreaExtent
		{
			get
			{
				// JJD 12/26/01
				// In card view there is no pre row area
				//
				if ( this.CardView )
				{
					int preRowAreaExtent = 0;

					// SSP 10/26/06 BR15984
					// If Indentation is explicitly set then use that.
					// 
					if ( -1 != this.indentation )
						return this.indentation;

					// JJD 1/18/02 - UWG938
					// Check sibling bands to get the highest preRowAreaExtent
					// so we don't overlap their sibling row connector lines.
					//
					if ( this.parentBand != null )
					{
						// JJD 1/24/02
						// For child bands that are in cardview set a minimum
						// pre row area extext of 8 to provide some air. 
						//
						preRowAreaExtent = 8;

						for ( int i = 1; i < this.Layout.SortedBands.Count; i++ )
						{
							UltraGridBand band = this.Layout.SortedBands[i];

							// for sibling bands that are not in cardview
							// get the highest pre-row area extent
							//
							if ( band.ParentBand == this.parentBand &&
								!band.CardView )
							{
								preRowAreaExtent = Math.Max( preRowAreaExtent, band.PreRowAreaExtent );	
							}
						}
					}
					return preRowAreaExtent;
				}

				//no need for a prerowarea
				//
				if ( !( this.Layout.Grid is UltraGrid ) )
					return 0;


				int retValue = 0;

				// SSP 4/22/03 - Optimizations.
				// Instead of calling the property multiple times, call it once.
				//
				ShowExpansionIndicator expansionIndicatorResolved = this.ExpansionIndicatorResolved;

				//RobA 11/6/01 use the new enum
				//if ( this.ExpansionIndicatorResolved == DefaultableBoolean.True )
				if ( expansionIndicatorResolved == ShowExpansionIndicator.Always
					// SSP 9/24/02 UWG1699
					// Also treat CheckOnDisplay and CheckOnExpand as Always as far as the
					// pre row area extent is concerned. Because if the user explicitly sets
					// these properties, then he expects pre row area to show up.
					//
					|| ShowExpansionIndicator.CheckOnDisplay == expansionIndicatorResolved
					|| ShowExpansionIndicator.CheckOnExpand  == expansionIndicatorResolved
					)
					retValue = UltraGridBand.PRE_ROW_SELECTOR_WIDTH;
				else
					if( this.layout.ViewStyleImpl.IsMultiBandDisplay )
				{
					// SSP 9/26/01
					// Modified so that if the parent rows of rows associated with
					// this band are group by rows, then not to have any pre row
					// selector width if the rows don't have any children.
					//					
					
					if ( null != this.ParentBand && !this.HasGroupBySortColumns )
						retValue = UltraGridBand.PRE_ROW_SELECTOR_WIDTH;
					else if ( this.GetChildBandCount( true ) > 0 ) 
						retValue = UltraGridBand.PRE_ROW_SELECTOR_WIDTH;
				}

				//	get the Indentation
				int indentation = this.indentation;
				
				//	if indentation is the default, return the value of PRE_ROW_SELECTOR_WIDTH,
				//	else return indentation
				if( indentation == -1 )
					return retValue;
				else
					return indentation;
			}
		}

		// SSP 4/17/07 BR21747
		// Added RowConnectorLineOffset.
		// 
		#region RowConnectorLineOffset

		private int CalcRowConnectorLineOffsetHelper( )
		{
			int val = UltraGridBand.PRE_ROW_SELECTOR_WIDTH;
			if ( this.indentation != -1 && this.indentation < val )
				val = this.indentation;

			return val / 2;
		}

		private bool HasIndependentRowConnectorsFromSiblingBands
		{
			get
			{
				return null == this.parentBand || this.HasGroupBySortColumns || this.CardView;
			}
		}

		private int CalcRowConnectorLineOffset( )
		{
			int minOffset = this.CalcRowConnectorLineOffsetHelper( );

			if ( ! this.HasIndependentRowConnectorsFromSiblingBands )
			{
				BandsCollection bands = null != this.Layout ? this.Layout.SortedBands : null;
				int index = this.Index;
				if ( index > 0 && null != bands )
				{
					for ( int i = index - 1; i >= 0; i-- )
					{
						UltraGridBand band = bands[i];
						if ( band.parentBand == this.parentBand )
						{
							if ( band.HasIndependentRowConnectorsFromSiblingBands )
								break;

							minOffset = Math.Min( minOffset, band.CalcRowConnectorLineOffsetHelper( ) );
						}
					}

					for ( int i = index + 1, count = bands.Count; i < count; i++ )
					{
						UltraGridBand band = bands[i];
						if ( band.parentBand == this.parentBand )
						{
							if ( band.HasIndependentRowConnectorsFromSiblingBands )
								break;

							minOffset = Math.Min( minOffset, band.CalcRowConnectorLineOffsetHelper( ) );
						}
					}
				}
			}

			return minOffset;
		}

		private int cachedRowConnectorLineOffset;
		private int verifiedGrandVerifyVersion = -1;
		/// <summary>
		/// Returns the offset from the left of the pre-row area band origin where the
		/// row connectors are to be positioned.
		/// </summary>
		internal int RowConnectorLineOffset
		{
			get
			{
				if ( null == this.layout || this.verifiedGrandVerifyVersion != this.layout.GrandVerifyVersion )
				{
					this.cachedRowConnectorLineOffset = this.CalcRowConnectorLineOffset( );
					if ( null != this.layout )
						this.verifiedGrandVerifyVersion = this.layout.GrandVerifyVersion;
				}

				return this.cachedRowConnectorLineOffset;
			}
		}

		#endregion // RowConnectorLineOffset

		internal int RowSelectorExtent
		{
			get
			{
				// SSP 3/27/03 - Row Layout Functionality
				// Moved the logic into RowSelectorWidthResolved method.
				// Commented out below code and added new one.
				//
				return this.RowSelectorWidthResolved;
				
			}
		}

		// SSP 2/14/03 - RowSelectorWidth property
		// Added RowSelectorWidth property to allow the user to control the widths
		// of the row selectors.
		//
		// SSP 3/27/03 - Row Layout Functionality
		// Made RowSelectorWidthResolved public.
		//
		/// <summary>
		/// Returns the resolved row selector width. Returns 0 if the row selectors are not visible.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Advanced ), Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int RowSelectorWidthResolved
		{
			get
			{
				// Row selectors are displayed only in ultragrid.
				//
				if ( this.HasRowSelectors && this.Layout.Grid is UltraGrid )
				{
					if ( this.HasOverride && this.overrideObj.ShouldSerializeRowSelectorWidth( ) )
						return this.overrideObj.RowSelectorWidth;

					// SSP 2/4/05 - IDataErrorInfo Support
					// Resolve the row selector width differently based on whether row data error 
					// is supported so the data error icon and the row icons both have enough space.
					//
					//return this.Layout.RowSelectorWidthDefault;
					return this.Layout.GetRowSelectorWidthDefault( this );
				}

				return 0;
			}
		}

		// SSP 7/2/02
		// Compressed card view.
		//
		internal Size GetIdealCompressedCardViewButtonSize( )
		{
			bool themeSupported = this.Layout.ThemesBeingUsed;

			if ( themeSupported )
			{
				// AS 8/12/03 optimization - Use the new caching mechanism.
				//
				//Graphics graphics = DrawUtility.CreateReferenceGraphics( this.Layout.Grid );
				// MRS 6/1/04 - UWG2915
				//Graphics graphics = DrawUtility.GetCachedGraphics( this.Layout.Grid );
				Graphics graphics = this.Layout.GetCachedGraphics();

				if ( null != graphics )
				{
					Size size1 = XPThemes.Explorer.GetButtonSize(
						Infragistics.Win.ThemedExplorer.ExplorerButtonType.NormalGroupExpand,
						Infragistics.Win.ThemedExplorer.ExplorerButtonState.Normal, 
						graphics );

					Size size2 = XPThemes.Explorer.GetButtonSize(
						Infragistics.Win.ThemedExplorer.ExplorerButtonType.NormalGroupCollapse,
						Infragistics.Win.ThemedExplorer.ExplorerButtonState.Normal, 
						graphics );

					// AS 8/12/03 optimization - Use the new caching mechanism.
					//
					//graphics.Dispose( );
					// MRS 6/1/04 - UWG2915
					//DrawUtility.ReleaseCachedGraphics(graphics);
					this.Layout.ReleaseCachedGraphics( graphics );

					graphics = null;

					return new Size( Math.Max( size1.Width, size2.Width ), Math.Max( size1.Height, size2.Height ) );
				}
				else
				{					
					Debug.Assert( false, "Unable to create a graphics object !" );
					return new Size( 18, 18 );
				}
			}
			else
			{
				return new Size( 12, 12 );
			}
		}

		internal int CardCaptionHeight
		{
			get
			{
				if ( !this.CardSettings.ShowCaption )
					return 0;

				// if we haven't cached the card caption font height yet then do it
				// now
				//
				if ( !this.isCardCaptionFontHeightCached )
				{
					// See if there is an override font for headers
					//
					// JJD 12/13/01
					// Use new LowestLevelOverride property. Otherwise we might miss
					// settings off the layout's override. 
					// Note: GetAppearenceFontHeight will merge in the layout's override
					//		 settings if necessary.
					//
					int fontHeight = 0;
						
					UltraGridOverride lowestLevelOverride = this.LowestLevelOverride;
					if ( null != lowestLevelOverride )
					{
						// get the card caption appearance
						//
						// SSP 3/16/06 - App Styling
						// Use the band's GetBLOverrideAppearanceFontHeight method which also takes into account the 
						// app-style appearance settings.
						// 
						//fontHeight = lowestLevelOverride.GetAppearenceFontHeight( Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.CardCaption );
						fontHeight = this.GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex.CardCaption, 
							false, StyleUtils.Role.CardCaption, AppStyling.RoleState.Normal );

						// get the card caption selected appearance
						//
						// SSP 3/16/06 - App Styling
						// Use the band's GetBLOverrideAppearanceFontHeight method which also takes into account the 
						// app-style appearance settings.
						// 
						//int selectedFontHeight = lowestLevelOverride.GetAppearenceFontHeight( Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedCardCaption );
						int selectedFontHeight = this.GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex.SelectedCardCaption,
							false, StyleUtils.Role.CardCaption, AppStyling.RoleState.Selected );

						// use the larger of the 2 fonts
						//
						fontHeight = Math.Max( selectedFontHeight, fontHeight );

						// get the card caption selected appearance
						//
						// SSP 3/16/06 - App Styling
						// Use the band's GetBLOverrideAppearanceFontHeight method which also takes into account the 
						// app-style appearance settings.
						// 
						//int activeFontHeight = lowestLevelOverride.GetAppearenceFontHeight( Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveCardCaption );
						int activeFontHeight = this.GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex.ActiveCardCaption,
							false, StyleUtils.Role.CardCaption, AppStyling.RoleState.Active );

						// use the larger of the 2 fonts
						//
						fontHeight = Math.Max( activeFontHeight, fontHeight );
					}

					// if there is then return its height
					//
					if ( fontHeight == 0 )
					{
						// JJD 12/13/01
						// Moved logic for calculating and caching the default
						// font height into the layout
						//
						fontHeight = this.layout.DefaultFontHeight;
					}

					// SSP 7/2/02
					// Compressed card view. Also make sure the caption height is
					// as tall as the expansion button on the card caption.
					//
					fontHeight = Math.Max( fontHeight, 4 + this.GetIdealCompressedCardViewButtonSize( ).Height );

					this.cachedCardCaptionFontHeight	= fontHeight;
					this.isCardCaptionFontHeightCached	= true;
				}

				return this.cachedCardCaptionFontHeight * this.CardSettings.CaptionLines;
			}
		}

		// SSP 7/3/02
		// Compressed card view
		// Added CardHeightCompressed property.
		//
		internal int CardHeightCompressed
		{
			get
			{
				// NOTE: SInce CardCaptionHeight caches the font height, we don't
				// need to cache the height here since all we are doing is getting
				// the border thickness.
				//

				// If the card is compressed, then return the card caption height
				// and the card borders.
				//
				int borderThickness	= this.Layout.GetBorderThickness( this.BorderStyleRowResolved );

				return this.CardCaptionHeight + 2 * borderThickness;
			}
		}

		internal int CardHeight
		{
			get
			{
				// JJD 12/17/01
				// Use the cachedCardHeight so we don't run through this
				// every time. Note: it gets reset to 0 after each draw
				// operation.
				//
				if ( this.UseRowLayoutResolved )
				{
					if ( this.AreColumnHeadersInSeparateLayoutArea )
						this.cachedCardHeight = Math.Max( this.PreferredHeaderLayoutSize.Height, this.PreferredRowLayoutSize.Height );
					else 
						this.cachedCardHeight = this.PreferredRowLayoutSize.Height;
				}

				if ( this.cachedCardHeight < 1 )
				{
					int totalCellHeight = 0;

					// Add up the height of all the columns that will be displayed in the card
					//
					for ( int i = 0; i < this.OrderedHeaders.Count; i++ )
					{
						// MD 8/3/07 - 7.3 Performance
						// Refactored - Prevent accessing the same array index multiple times in the same loop
						#region Refactored

						//if ( this.OrderedHeaders[i].Hidden )
						//    continue;
						//
						//if ( this.OrderedHeaders[i] is GroupHeader )
						//{
						//    UltraGridGroup group = ((GroupHeader )this.OrderedHeaders[i]).Group;
						//
						//    for ( int j = 0; j < group.Columns.Count; j++ )
						//    {
						//        if ( group.Columns[j].Hidden )
						//            continue;
						//
						//        totalCellHeight += group.Columns[j].CardCellHeight;
						//    }
						//}
						//else
						//    if ( this.OrderedHeaders[i] is ColumnHeader )
						//{
						//    totalCellHeight += ((ColumnHeader)this.OrderedHeaders[i]).Column.CardCellHeight;
						//}

						#endregion Refactored
						HeaderBase orderedHeader = this.OrderedHeaders[ i ];

						if ( orderedHeader.Hidden )
							continue;

						GroupHeader groupHeader = orderedHeader as GroupHeader;

						if ( groupHeader != null )
						{
							UltraGridGroup group = groupHeader.Group;

							// MD 1/21/09 - Groups in RowLayouts
							// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
							// actually visible in the group
							//for ( int j = 0; j < group.Columns.Count; j++ )
							//{
							//    UltraGridColumn gridColumn = group.Columns[ j ];
							foreach ( UltraGridColumn gridColumn in group.ColumnsResolved )
							{
								// MD 1/21/09 - Groups in RowLayouts
								// Use HiddenResolved instead of Hidden because it checks the group's hidden value.
								//if ( gridColumn.Hidden )
								if ( gridColumn.HiddenResolved )
									continue;

								totalCellHeight += gridColumn.CardCellHeight;
							}
						}
						else
						{
							ColumnHeader columnHeader = orderedHeader as ColumnHeader;

							if ( columnHeader != null )
							{
								totalCellHeight += columnHeader.Column.CardCellHeight;
							}
						}
					}

					this.cachedCardHeight = totalCellHeight;
				}

				//JM 01-16-02 now using BorderStyleRow for cards
				//int borderThickness	= this.Layout.GetBorderThickness( this.BorderStyleCardResolved );
				int borderThickness	= this.Layout.GetBorderThickness( this.BorderStyleRowResolved );

				return this.cachedCardHeight 
					+ this.CardCaptionHeight
					+ ( 2 * borderThickness );

			}
		}

		#region CardLabelWidthResolvedHelper

		// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
		// Added the CardLabelWidthResolvedHelper method. The code in there was moved from the
		// existing CardLabelWidthResolved property.
		//
		internal int CardLabelWidthResolvedHelper( bool forWrapHeaderTextCalculations )
		{
			// SSP 12/16/04 BR00242
			// If the ColHeadersVisible property is set to false then don't display 
			// the card labels.
			//
			if ( ! this.ColHeadersVisible )
			{
				// MD 2/26/09 - TFS14602
				// If we are in group layout mode and group headers are still visible, we can't assume the height is 0. 
				// Otherwise, return 0.
				//return 0;
				if ( this.RowLayoutStyle != RowLayoutStyle.GroupLayout ||
                    // MRS 2/27/2009 - TFS14725
                    // The Resolved here is only concerned with whether the headers are visible on top. 
                    //
					//this.GroupHeadersVisibleResolved == false )
                    this.GroupHeadersVisible == false)
				{
					return 0;
				}
			}

			if ( this.CardSettings.LabelWidth > 0 )
				return this.CardSettings.LabelWidth;

			// SSP 2/28/03 - Row Layout Functionality
			//
			if ( this.UseRowLayoutResolved )
			{
				return this.PreferredHeaderLayoutSize.Width;
			}

			int labelAreaWidth = this.cachedCardLabelAreaWidth;
			if ( labelAreaWidth < 1 || forWrapHeaderTextCalculations )
			{
				int maxUsableCardArea	= 0;

				// If the grid is created restrict the width so that the card isn't
				// wider that the control
				//
				if ( this.Layout.Grid != null  &&
					this.Layout.Grid.ControlForGridDisplay != null &&
					this.Layout.Grid.ControlForGridDisplay.Created	&&
					this.Layout.Grid.ControlForGridDisplay.IsHandleCreated )
				{
					maxUsableCardArea = this.Layout.Grid.ControlForGridDisplay.ClientRectangle.Width - 
						( 2 * this.Layout.GetBorderThickness( this.Layout.BorderStyleResolved ) );
						
					if ( this.layout.AutoFitAllColumns )
						maxUsableCardArea -= this.GetOrigin( BandOrigin.RowSelector );

					if ( maxUsableCardArea < 10 )
						return 10;
				}

				if ( ! forWrapHeaderTextCalculations )
				{
					// Add up the height of all the columns that will be displayed in the card
					//
					for ( int i = 0; i < this.OrderedHeaders.Count; i++ )
					{
						// MD 8/3/07 - 7.3 Performance
						// Refactored - Prevent accessing the same array index multiple times in the same loop
						#region Refactored

						//if ( this.OrderedHeaders[i].Hidden )
						//    continue;
						//
						//if ( this.OrderedHeaders[i] is GroupHeader )
						//{
						//    UltraGridGroup group = ((GroupHeader )this.OrderedHeaders[i]).Group;
						//
						//    for ( int j = 0; j < group.Columns.Count; j++ )
						//    {
						//        if ( group.Columns[j].Hidden )
						//            continue;
						//
						//        labelAreaWidth = Math.Max( labelAreaWidth, group.Columns[j].CardLabelSize.Width );
						//    }
						//}
						//else
						//    if ( this.OrderedHeaders[i] is ColumnHeader )
						//{
						//    labelAreaWidth = Math.Max( labelAreaWidth, ((ColumnHeader)this.OrderedHeaders[i]).Column.CardLabelSize.Width );
						//}

						#endregion Refactored
						HeaderBase orderedHeader = this.OrderedHeaders[ i ];

						if ( orderedHeader.Hidden )
							continue;

						GroupHeader groupHeader = orderedHeader as GroupHeader;

						if ( groupHeader != null )
						{
							UltraGridGroup group = groupHeader.Group;

							// MD 1/21/09 - Groups in RowLayouts
							// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
							// actually visible in the group
							//for ( int j = 0; j < group.Columns.Count; j++ )
							//{
							//    UltraGridColumn groupColumn = group.Columns[ j ];
							foreach ( UltraGridColumn groupColumn in group.ColumnsResolved )
							{
								// MD 1/21/09 - Groups in RowLayouts
								// Use HiddenResolved instead of Hidden because it checks the group's hidden value.
								//if ( groupColumn.Hidden )
								if ( groupColumn.HiddenResolved )
									continue;

								labelAreaWidth = Math.Max( labelAreaWidth, groupColumn.CardLabelSize.Width );
							}
						}
						else
						{
							ColumnHeader columnHeader = orderedHeader as ColumnHeader;

							if ( columnHeader != null )
							{
								labelAreaWidth = Math.Max( labelAreaWidth, columnHeader.Column.CardLabelSize.Width );
							}
						}
					}
				}

				if ( maxUsableCardArea > 0 )
				{
					// only allow at most 3/4 of the usable card area to be taken up by 
					// the card labels
					//
					labelAreaWidth = Math.Min( labelAreaWidth, ( maxUsableCardArea * 3 ) / 4 );
				}

				if ( ! forWrapHeaderTextCalculations )
					this.cachedCardLabelAreaWidth = labelAreaWidth;					 
			}

			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
			// Commented out the following code because it's unnecessary. The column's CardLabelSize
			// already includes the indicator and swap elem widths. No need to add it again.
			// 
			

			return labelAreaWidth;
		}

		#endregion // CardLabelWidthResolvedHelper

		// SSP 3/19/03 - Row Layout Functionality
		// Made CardLabelWidthResolved public for row layout functionality
		//
		/// <summary>
		/// The width of the card label area
		/// </summary>
		//internal int CardLabelWidthResolved
		[ EditorBrowsable( EditorBrowsableState.Advanced ), Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int CardLabelWidthResolved
		{
			get
			{
				// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
				// Moved the code that was here into the new CardLabelWidthResolvedHelper method 
				// and modified it to accomodate the wrap header text functionality in card-view
				// non-row-layout mode.
				//
				return this.CardLabelWidthResolvedHelper( false );
			}
		}

		private int CardCellAreaWidth
		{
			get
			{
				// SSP 2/28/03 - Row Layout Functionality
				//
				if ( this.UseRowLayoutResolved )
				{
					return this.PreferredRowLayoutSize.Width;
				}

				if ( this.cachedCardCellAreaWidth < 1 )
				{
					int cellAreaWidth		= 0;
					int maxUsableCardArea	= 0;

					// If the grid is created restrict the width so that the card isn't
					// wider that the control
					//
					if ( this.Layout.Grid != null  &&
						this.Layout.Grid.ControlForGridDisplay != null &&
						this.Layout.Grid.ControlForGridDisplay.Created	&&
						this.Layout.Grid.ControlForGridDisplay.IsHandleCreated )
					{
						maxUsableCardArea = this.Layout.Grid.ControlForGridDisplay.ClientRectangle.Width - 
							( 2 * this.Layout.GetBorderThickness( this.Layout.BorderStyleResolved ) );
						
						if ( this.layout.AutoFitAllColumns )
							maxUsableCardArea -= this.GetOrigin( BandOrigin.RowSelector );

						maxUsableCardArea -= this.CardLabelWidthResolved;

						if ( maxUsableCardArea < 10 )
							return 10;
					}


					// Add up the height of all the columns that will be displayed in the card
					//
					for ( int i = 0; i < this.OrderedHeaders.Count; i++ )
					{
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						#region Refactored

						//if ( this.OrderedHeaders[i].Hidden )
						//    continue;
						//
						//if ( this.OrderedHeaders[i] is GroupHeader )
						//{
						//    UltraGridGroup group = ((GroupHeader )this.OrderedHeaders[i]).Group;
						//
						//    for ( int j = 0; j < group.Columns.Count; j++ )
						//    {
						//        if ( group.Columns[j].Hidden )
						//            continue;
						//
						//        cellAreaWidth = Math.Max( cellAreaWidth, group.Columns[j].CellWidthDefault );
						//    }
						//}
						//else
						//    if ( this.OrderedHeaders[i] is ColumnHeader )
						//{
						//    cellAreaWidth = Math.Max( cellAreaWidth, ((ColumnHeader)this.OrderedHeaders[i]).Column.CellWidthDefault );
						//}

						#endregion Refactored
						HeaderBase orderedHeader = this.OrderedHeaders[ i ];

						if ( orderedHeader.Hidden )
							continue;

						GroupHeader groupHeader = orderedHeader as GroupHeader;

						if ( groupHeader != null )
						{
							UltraGridGroup group = groupHeader.Group;

							// MD 1/21/09 - Groups in RowLayouts
							// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
							// actually visible in the group
							//for ( int j = 0; j < group.Columns.Count; j++ )
							//{
							//    UltraGridColumn groupColumn = group.Columns[ j ];
							foreach ( UltraGridColumn groupColumn in group.ColumnsResolved )
							{
								// MD 1/21/09 - Groups in RowLayouts
								// Use HiddenResolved instead of Hidden because it checks the group's hidden value.
								//if ( groupColumn.Hidden )
								if ( groupColumn.HiddenResolved )
									continue;

								cellAreaWidth = Math.Max( cellAreaWidth, groupColumn.CellWidthDefault );
							}
						}
						else
						{
							ColumnHeader columnHeader = orderedHeader as ColumnHeader;

							if ( columnHeader != null )
							{
								cellAreaWidth = Math.Max( cellAreaWidth, columnHeader.Column.CellWidthDefault );
							}
						}
					}

					if ( maxUsableCardArea > 0 )
					{
						// Restrict to the max available width
						//
						cellAreaWidth = Math.Min( cellAreaWidth, maxUsableCardArea );
					}

					this.cachedCardCellAreaWidth = Math.Max( 1, cellAreaWidth );
					 
				}

				return this.cachedCardCellAreaWidth;
			}
		}

		// SSP 3/19/03 - Row Layout Functionality
		// Made CardWidthResolved public for row layout functionality
		//
		/// <summary>
		/// The width of a single card
		/// </summary>
		//internal int CardWidthResolved
		[ EditorBrowsable( EditorBrowsableState.Advanced ), Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int CardWidthResolved
		{
			get
			{
				if ( this.CardSettings.Width > 0 )
					return this.CardSettings.Width;

				// SSP 2/28/03 - Row Layout Functionality
				// In row layout mode, don't adjust the width since in the row layout mode we
				// don't add any card label area inside of the row ui element.
				//
				// if ( this.CardSettings.Style == CardStyle.MergedLabels )
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				if ( this.UseRowLayoutResolved || this.CardSettings.StyleResolved == CardStyle.MergedLabels )
					return this.CardCellAreaWidth;

				return this.CardCellAreaWidth + this.CardLabelWidthResolved;
			}
		}

		internal int CardMergedLabelAreaWidth
		{
			get
			{
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				if (this.CardSettings.StyleResolved == CardStyle.MergedLabels)
					return this.CardLabelWidthResolved;
				else
					return 0;
			}
		}

		internal int CardSpacingVertical
		{
			get	{ return (this.CardSpacingResolved * 2); }
		}

		internal int CardSpacingHorizontal
		{
			get
			{
				return this.CardSpacingResolved;
			}
		}

		internal int GetHeightAvailableForCards(int totalCardAreaHeight)
		{
			int borderThickness		= this.Layout.GetBorderThickness(this.BorderStyleCardAreaResolved );

			return totalCardAreaHeight					-
				(2 * (this.CardAreaMarginsResolved + borderThickness))	-
				// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
				// Only add scrollbar height if scrollbar's going to be visible.
				//
				//SystemInformation.HorizontalScrollBarHeight;
				this.CardAreaHorizontalScrollbarHeightResolved;
		}

		internal int GetWidthAvailableForCards(int totalCardAreaWidth)
		{
			int borderThickness		= this.Layout.GetBorderThickness(this.BorderStyleCardAreaResolved );

			return totalCardAreaWidth					-
				(2 * (this.CardAreaMarginsResolved + borderThickness))	-
				this.CardMergedLabelAreaWidth;
		}

		internal int GetTotalRowsInCardArea(int totalCardAreaHeight)
		{
			// get the height of a single card
			//
			int cardHeight = this.CardHeight;

			// subtract that height from the total available height to find
			// out how much space is available for extra cards (more than 1).
			//
			int heightForExtraCards = this.GetHeightAvailableForCards(totalCardAreaHeight) - cardHeight;
			
			// set rows to one
			//
			int cardRows = 1;
			
			// add the number of extra cards that will fit
			//
			if ( heightForExtraCards > 0 ) 
				// SSP 1/20/05 BR01824
				// If all the columns are hidden and the card captions are hidden and the card spacing
				// is 0 then the divisor can be 0. In such a case use 1 as the divisor.
				//
				//cardRows += heightForExtraCards / (cardHeight + this.CardSpacingVertical);
				cardRows += heightForExtraCards / Math.Max( 1, cardHeight + this.CardSpacingVertical );

			// restrict to the max setting
			//
			if (this.CardSettings.MaxCardAreaRows > 0)
				cardRows = Math.Min(this.CardSettings.MaxCardAreaRows, cardRows);

			return cardRows;
		}

		internal int GetTotalColsInCardArea(int totalCardAreaWidth)
		{
			// get the width of a single card
			//
			int cardWidth = this.CardWidthResolved;

			// subtract that width from the total available width to find
			// out how much space is available for extra cards (more than 1).
			//
			int widthForExtraCards = this.GetWidthAvailableForCards(totalCardAreaWidth) - cardWidth;
			
			// set cols to one
			//
			int cols = 1;
			
			// add the number of extra cards that will fit
			//
			if ( widthForExtraCards > 0 ) 
				cols += widthForExtraCards / (cardWidth + this.CardSpacingHorizontal);

			// restrict to the max setting
			//
			if (this.CardSettings.MaxCardAreaCols > 0)
				cols = Math.Min(this.CardSettings.MaxCardAreaCols,	cols);

			return cols;
		}

			#region CalculateRequiredCardAreaWidth

		internal int CalculateRequiredCardAreaWidth( int cardColumns )
		{
			Debug.Assert( cardColumns > 0, "cardColumns less than 1" );

			// Just in case.
			cardColumns				 = Math.Max(cardColumns, 1);

			// Calculate this for convenience.
			int cardWidth			= this.CardWidthResolved;
			int cardSpacing			= this.CardSpacingHorizontal;
			int borderThickness		= this.Layout.GetBorderThickness(this.BorderStyleCardAreaResolved );

			// Calculate the width needed for the max cards we think we can hold
			// and for the width needed to hold one card.
			int widthNeeded			= (2 * (this.CardAreaMarginsResolved + borderThickness));
			
			widthNeeded				+= this.CardMergedLabelAreaWidth;
			// JM 01-16-02 Add a cardSpacing after the merged label area.
			widthNeeded				+= cardSpacing;
			widthNeeded				+= cardColumns * cardWidth;
			widthNeeded				+= (cardColumns - 1) * cardSpacing;

			return widthNeeded;
		}

			#endregion CalculateRequiredCardAreaWidth

			#region CalculateRequiredCardAreaHeight

		internal int CalculateRequiredCardAreaHeight( int cardRows )
		{
			Debug.Assert( cardRows > 0, "cardRows less than 1" );

			// Just in case.
			cardRows				 = Math.Max(cardRows, 1);

			// Calculate this for convenience.
			int cardHeight			= this.CardHeight;
			int cardSpacing			= this.CardSpacingVertical;
			int borderThickness		= this.Layout.GetBorderThickness(this.BorderStyleCardAreaResolved );

			// Calculate the height needed for the max cards we think we can hold
			// and for the height needed to hold one card.
			int heightNeeded			= (2 * (this.CardAreaMarginsResolved + borderThickness));
			
			// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
			// Only add scrollbar height if scrollbar's going to be visible.
			//
			//heightNeeded				+= SystemInformation.HorizontalScrollBarHeight;
			heightNeeded				+= this.CardAreaHorizontalScrollbarHeightResolved;

			heightNeeded				+= cardRows * cardHeight;
			heightNeeded				+= (cardRows - 1) * cardSpacing;

			return heightNeeded;
		}

			#endregion CalculateRequiredCardAreaHeight

		internal void ClearCachedHeightValues()
		{
			//	BF 8.9.02	UWG1429
			//	We need to increment the row height version number
			//	so that row heights get recalculated
			//
			this.BumpMajorRowHeightVersion();

			this.isMinRowHeightCached			= false;
			this.isColHdrFontHeightCached		= false;
			this.isGroupHdrFontHeightCached		= false;
			this.isRowFontHeightCached			= false;
			this.isCardCaptionFontHeightCached  = false;
			this.isCardLabelFontHeightCached    = false;

			// JJD 12/13/01
			// Mark the column cache dirty
			//
			this.isColumnCardFontCacheDirty		= true;

			// JAS v5.2 Wrapped Header Text 4/18/05
			//
			if( this.Layout != null )
				this.Layout.BumpColumnHeaderHeightVersionNumber();
		}

		internal void ValidateColumnCardFontCache()
		{

			if ( this.isColumnCardFontCacheDirty )
			{
				this.isColumnCardFontCacheDirty	= false;

				// JJD 1/21/02 - UWG957
				// Set back to 0 to foirce recalc of cached value
				//
				this.cachedCardHeight = 0;

				// JJD 12/13/01
				// Clear the font cache for all the columns in this band
				//
				if ( this.columns != null )
				{
					for ( int i = 0; i < this.columns.Count; i++ )
						// SSP 4/24/08 BR27115
						// Don't dirty the column metrics - pass false for dirtyColMetrics parameter.
						// This causes recursive situation where initializing column metrics causes
						// FitColumnsToWidth, which in turn calls this method which again dirties the
						// metrics. Besides it's not necessary when we call ClearFontCache from here.
						// 
						//this.columns[i].ClearFontCache();
						this.columns[i].ClearFontCache( false );
				}
			}
		}

		internal void ClearCachedHeaderFontHeights()
		{
			this.isColHdrFontHeightCached   = false; 
			this.isGroupHdrFontHeightCached = false;

			// JJD 12/13/01
			// Mark the column cache dirty
			//
			this.isColumnCardFontCacheDirty	= true;

			// SSP 6/17/02 UWG1218
			// When anything that effects the default width of a column changes,
			// we need to dirty the metrics because the widths of the columns
			// will change but the column headers won't be regenerated. To
			// cause them to be regenerated dirty the metrics.
			//
			if ( null != this.Layout && null != this.Layout.ColScrollRegions )
			{
				this.Layout.DirtyGridElement( true );
				this.Layout.ColScrollRegions.DirtyMetrics( );				
			}

			// SSP 11/24/03 - Scrolling Till Last Row Visible
			//
			if ( null != this.Layout )
				this.Layout.BumpGrandVerifyVersion( );
		}


		internal void ClearDrawCache()
		{
			// JJD 12/17/01
			// Reset the cached card values so they get recalculated 
			// on every draw
			//
			this.cachedCardHeight = 0;
			this.cachedCardLabelAreaWidth = 0;
			this.cachedCardCellAreaWidth = 0;

			// JJD 11/06/01
			// Clear each bands cache
			//
			if ( this.columns != null )
			{
				this.columns.ClearDrawCache();
			}
		}

		internal bool HasRowSelectors
		{
			get
			{
				// JJD 12/26/01
				// In card view there are no row selectors
				//
				if ( this.CardView )
					return false;

				return this.RowSelectorsResolved == DefaultableBoolean.True; 
			}
		}

		



		/// <summary>
		/// Returns the absolute coordinate of the leftmost point on the band, taking into consideration the control's entire virtual area.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>GetExtent</b> method to return leftmost point on a band, using the scale mode of the grid's container. The coordinate returned by <b>GetOrigin</b> is relative to the absolute left edge of the grid's virtual area. The grid's virtual area is the total space occupied by the grid's data, independent of any display issues. The size of the virtual area is not dependent on the size of the size of the control, it's container, or the system's display settings. How the grid is scrolled and what portion of the band is currently visible on screen will have no effect on the value returned by this method.</p>
		///	<p class="body">Note that to get the actual origin of the band in a specific column scrolling region, you must subtract the ColScrollRegion's <b>Position</b> property from the value returned by <b>GetOrigin</b></p>
		///	</remarks>
		/// <param name="area">Specifies which area to get the origin of</param>
        ///	<returns>The absolute coordinate of the leftmost point on the band, taking into consideration the control's entire virtual area.</returns>
		public int GetOrigin( BandOrigin area )
		{
			int areaOrigin = this.origin;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//RowsCollection rows = this.Layout.Rows;
	
			switch ( area )
			{
				case BandOrigin.RowSelector:
					areaOrigin += this.PreRowAreaExtent;
					break;
				case BandOrigin.RowCellArea:
					areaOrigin += this.PreRowAreaExtent + this.RowSelectorExtent;
					break;
			}

			return areaOrigin;
		}

		/// <summary>
		/// Returns the absolute coordinate of the leftmost point on the band, taking into consideration the control's entire virtual area.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>GetExtent</b> method to return leftmost point on a band, using the scale mode of the grid's container. The coordinate returned by <b>GetOrigin</b> is relative to the absolute left edge of the grid's virtual area. The grid's virtual area is the total space occupied by the grid's data, independent of any display issues. The size of the virtual area is not dependent on the size of the size of the control, it's container, or the system's display settings. How the grid is scrolled and what portion of the band is currently visible on screen will have no effect on the value returned by this method.</p>
		///	<p class="body">Note that to get the actual origin of the band in a specific column scrolling region, you must subtract the ColScrollRegion's <b>Position</b> property from the value returned by <b>GetOrigin</b></p>
		///	</remarks>
        ///	<returns>The absolute coordinate of the leftmost point on the band, taking into consideration the control's entire virtual area.</returns>
		public int GetOrigin()
		{
			return this.GetOrigin( BandOrigin.PreRowArea );
		}

		/// <summary>
		/// Returns the absolute width of the band.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>GetExtent</b> method to return the total width of a band, using the scale mode of the grid's container. This method does not take into account how much of the band is visible, the size of the ColScrollRegion, or even how much screen area is available on the system. It simply calculates the total width that would be required to display the band in its entirety, starting with the position you specify.</p>
		///	</remarks>
		/// <param name="area">Specifies which area to get the extent of</param>
		public int GetExtent( BandOrigin area )
		{
			int areaExtent = this.extent;

			switch ( area )
			{
				case BandOrigin.PreRowArea:
					break;
				case BandOrigin.RowSelector:
					areaExtent -= this.PreRowAreaExtent;;
					break;
				case BandOrigin.RowCellArea:
					areaExtent -= this.PreRowAreaExtent + this.RowSelectorExtent;
					break;				
			}

			return areaExtent;
		}
		
		internal int GetActualWidth( bool ignoreDraggingWidth )
		{
			return this.GetExtent( );
		}


		/// <summary>
		/// Returns the absolute width of the band.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>GetExtent</b> method to return the total width of a band, using the scale mode of the grid's container. This method does not take into account how much of the band is visible, the size of the ColScrollRegion, or even how much screen area is available on the system. It simply calculates the total width that would be required to display the band in its entirety.</p>
		///	</remarks>
		public int GetExtent()
		{
			return this.GetExtent( BandOrigin.PreRowArea );
		}


		internal void BumpGroupByHierarchyVersion( )
		{
			this.groupByHierarchyVersion++;

			// SSP 8/12/03 - Optimizations - Grand Verify Version Number
			//
			if ( null != this.Layout )
			{
				this.Layout.BumpGrandVerifyVersion( );

				// SSP 7/27/05
				// Since grouping row collections can lead to change in their scroll counts,
				// we need to recalc the scroll count. Without this change it would recalculate
				// the scroll count whenever the row collection recreates the group-by hierarchy.
				// However it's done lazily so the scroll count could change while for example
				// the sparse array is in the middel of GetItemAtScrollIndex etc... since the
				// row collections would be verifying their group-by hierarchies when their
				// scroll count is asked for by the sparse array.
				// 
				this.Layout.BumpScrollableRowCountVersion( );
			}
		}

		internal int GroupByHierarchyVersion
		{
			get
			{
				return this.groupByHierarchyVersion;
			}
		}

		#region AllowRowSummariesResolved

		internal AllowRowSummaries AllowRowSummariesResolved
		{
			get
			{
				if ( this.HasOverride && this.Override.ShouldSerializeAllowRowSummaries( ) )
					return this.Override.AllowRowSummaries;

				return this.Layout.AllowRowSummariesDefault;
			}
		}

		#endregion // specifiedFormat

		// SSP 3/21/02
		// Version 2 Row Filter Implementation
		//
		#region "Filter code"

		#region IsFilterUIEnabled

		// SSP 8/29/03 UWG2330
		// Added IsFilterUIEnabled helper method that tells whether the user is able
		// to modify filter criteria on this band.
		//		
		internal bool IsFilterUIEnabled
		{
			get
			{
				// SSP 3/26/04 UWG2993
				// Commented out the original code and added the new code below it.
				// If filtering is enabled on an individual column but disabled on the band, then
				// we still need to return true from this property. This property is used to 
				// decide whether a childband should display headers if all the rows in that child
				// band are filtered out so the user can unfilter them.
				//
				// ----------------------------------------------------------------------------------
				//return DefaultableBoolean.True == this.AllowRowFilteringResolved;
				if ( null != this.Layout )
				{
					if ( this.Layout.GrandVerifyVersion == this.verifiedIsFilterUIEnabledVersionNumber )
						return this.cachedIsFilterUIEnabled;

					this.verifiedIsFilterUIEnabledVersionNumber = this.Layout.GrandVerifyVersion;
				}

				this.cachedIsFilterUIEnabled = false;
				for ( int i = 0; i < this.Columns.Count; i++ )
				{
					UltraGridColumn column = this.Columns[i];

					// Skip hidden columns.
					//
					// MD 1/20/09 - Groups in RowLayout
					// Use the HiddenResolved property because it takes into account nested groups.
					//if ( column.Hidden || this.GroupsDisplayed && ( null == column.Group || column.Group.Hidden ) )
					//    continue;							
					if ( column.HiddenResolved )
						continue;

					if ( DefaultableBoolean.True == column.AllowRowFilteringResolved )
					{
						this.cachedIsFilterUIEnabled = true;
						break;
					}
				}

				return this.cachedIsFilterUIEnabled;
				// ----------------------------------------------------------------------------------
			}
		}

		#endregion // IsFilterUIEnabled

		#region ShouldAddFilteredOutFirstChildRow

		// SSP 8/29/03 UWG2330
		// Added ShouldAddFilteredOutFirstChildRow helper method.
		//
		internal bool ShouldAddFilteredOutFirstChildRow( ViewStyleBase viewStyle, RowsCollection rows, out UltraGridRow childRowToAdd )
		{
			childRowToAdd = null;

			Debug.Assert( this == rows.Band, "Band must match." );

			if ( viewStyle.ShouldApplySpecialRowFiltersCode( ) 
				&& this.IsFilterUIEnabled
				&& RowFilterAction.HideFilteredOutRows == this.RowFilterActionResolved
				// SSP 3/9/07 BR20933
				// Only do this for display layouts. Originally the idea behind this thing
				// was that when the user filtered out all rows, we still wanted to make
				// sure that we kept displaying the headers/filter-row so the user could 
				// revert the filters. Otherwise the user would get stuck since no rows
				// and thus no headers would be visible to change the filters back.
				// However this doesn't apply to print layouts.
				// 
				&& this.Layout.IsDisplayLayout )
			{
				if ( ! rows.HasAnyVisibleRows( ) )
				{
					childRowToAdd = rows.Count > 0 ? rows[0] : null;
					return null != childRowToAdd;
				}
			}

			return false;
		}

		#endregion // ShouldAddFilteredOutFirstChildRow

		#region ShouldAddFilteredOutChildRow

		// SSP 8/29/03 UWG2330
		// Added ShouldAddFilteredOutRow helper method.
		//
		internal bool ShouldAddFilteredOutRow( ViewStyleBase viewStyle, UltraGridRow row )
		{
			UltraGridRow childRowToAdd = null;
			if ( this.ShouldAddFilteredOutFirstChildRow( viewStyle, row.ParentCollection, out childRowToAdd )
				&& childRowToAdd == row )
				return true;

			return false;
		}

		#endregion // ShouldAddFilteredOutChildRow

		// MD 7/26/07 - 7.3 Performance
		// This logic in this method was changed and it always returns True now
		#region Removed

		//        #region HideGroupByRowsWithNoVisibleChildren

		//#if DEBUG
		//        /// <summary>
		//        /// Indicates whether to hide group by rows with no visible children.
		//        /// This is used in situations where if all the rows are filtered out
		//        /// in a group by row, then we may want to hide it. This property 
		//        /// indicates whether to hide the group by row or not in such cases.
		//        /// </summary>
		//#endif
		//        internal bool HideGroupByRowsWithNoVisibleChildren
		//        {
		//            get
		//            {
		//                // SSP 8/29/03 UWG2330
		//                // Commented out the existing code and added the new code below. We are going
		//                // to hide group-by rows with all their children filtered out in all bands.
		//                //
		//                // ----------------------------------------------------------------------------
		//                /*
		//                if ( ViewStyle.SingleBand == this.Layout.ViewStyle &&
		//                    RowFilterMode.AllRowsInBand == this.RowFilterModeResolved )
		//                    return true;

		//                return false;
		//                */

		//                return true;
		//                // ----------------------------------------------------------------------------
		//            }
		//        }

		//        #endregion // HideGroupByRowsWithNoVisibleChildren

		#endregion Removed

		#region ColumnFilters

		/// <summary>
		/// Column filters for filtering all the rows in this band. <b>NOTE:</b> They will apply only if the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/> is set to <b>AllRowsInBand</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/> must be set to <b>AllRowsInBand</b> in order for these column filetrs to take effect. If it's set to <b>SiblingRowsOnly</b> (which it resolves to by default) then column filers off the RowsCollection <see cref="Infragistics.Win.UltraWinGrid.RowsCollection.ColumnFilters"/> will take effect on the associated rows collection.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterCondition"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterConditionsCollection"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter.FilterConditions"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/>
		/// <seealso cref="RowsCollection.ColumnFilters"/>
		/// <seealso cref="UltraGridOverride.FilterUIType"/>
		/// </remarks>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public ColumnFiltersCollection ColumnFilters
		{
			get
			{
				if ( null == this.columnFilters )
				{
					this.columnFilters = new Infragistics.Win.UltraWinGrid.ColumnFiltersCollection( this );

					this.columnFilters.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.columnFilters;
			}
		}

		#endregion // ColumnFilters

		#region ColumnFiltersInternal

		// SSP 5/12/05 Optimizations
		// Added ColumnFiltersInternal property.
		//
		internal ColumnFiltersCollection ColumnFiltersInternal
		{
			get
			{
                //  BF 2/28/08  FR09238 - AutoCompleteMode
                //  If AutoCompleteMode is enabled for suggest mode, allow the collection
                //  to be created, and return it.
                if ( this.layout != null && this.layout.AutoCompleteFilterCondition != null )
                    return this.ColumnFilters;

				return this.columnFilters;
			}
		}

		#endregion // ColumnFiltersInternal

		#region HasColumnFilters

		internal bool HasColumnFilters
		{
			get
			{
				return null != this.columnFilters;
			}
		}

		#endregion //HasColumnFilters

		#region ShouldSerializeColumnFilters

		// SSP 7/16/02 UWG1367
		//
		internal bool ShouldSerializeColumnFilters( )
		{
			return this.HasColumnFilters && this.ColumnFilters.InternalShouldSerialize( );
		}

		#endregion // ShouldSerializeColumnFilters
	
		#region ResetColumnFilters

		internal void ResetColumnFilters( )
		{
			if ( null != this.columnFilters )
				this.columnFilters.ClearAllFilters( );
		}

		#endregion //ResetColumnFilters

		#region RowFilterModeResolved

        // MBS 2/27/09 - TFS14750
        // Made public so that the filter provider can know which ColumnFilters collection to use
//#if DEBUG
		/// <summary>
		/// For Infragistics internal use only.  Returns the resolved row filter mode based on the viewstyle
		/// and the override settings
		/// </summary>
//#endif
		//internal Infragistics.Win.UltraWinGrid.RowFilterMode RowFilterModeResolved
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Infragistics.Win.UltraWinGrid.RowFilterMode RowFilterModeResolved
		{
			get
			{
				// SSP 8/6/04 UWG3547
				// For root band always resolve the row filter mode to be AllRowsInBand
				// because for the root band it doesn't matter whether the row filter mode 
				// is AllRowsInBand or SiblingRowsOnly because filters are applied to all 
				// rows anyways (even in group-by mode due to UWG2330 fix).
				//
				// ----------------------------------------------------------------------------
				if ( null == this.ParentBand )
					return RowFilterMode.AllRowsInBand;
				// ----------------------------------------------------------------------------

				// If the view style has fixed headers (like in horizontal view style),
				// then resolve the filter mode to AllRowsInBand.
				//
				ViewStyleBase viewStyleImpl = this.Layout.ViewStyleImpl;
				if ( null != viewStyleImpl &&
					// SSP 7/18/05 - NAS 5.3 Header Placement
					// If the header placemenet is FixedOnTop then also force the row filter mode 
					// to be AllRowsInBand.
					// 
					//this.Layout.ViewStyleImpl.HasFixedHeaders )
					viewStyleImpl.BandHasFixedHeaders( this ) )
					return Infragistics.Win.UltraWinGrid.RowFilterMode.AllRowsInBand;

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowFilterMode( ) )
					return this.overrideObj.RowFilterMode;

				return this.Layout.RowFilterModeDefault;
			}
		}

		#endregion //RowFilterModeResolved

		#region AllowRowFilteringResolved

		internal DefaultableBoolean AllowRowFilteringResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowRowFiltering( ) )
					return this.overrideObj.AllowRowFiltering;
				
				// SSP 5/5/05 - NAS 5.2 Filter Row
				// Resolve AllowRowFiltering to True if the new FilterUIType property is set to
				// to a non-default value.
				//
				// ----------------------------------------------------------------------------
				//return this.Layout.AllowRowFilteringDefault;
				if ( this.layout.HasOverride &&
					this.layout.OverrideInternal.ShouldSerializeAllowRowFiltering( ) )
					return this.layout.OverrideInternal.AllowRowFiltering;

				return FilterUIType.Default != this.FilterUITypeResolvedDefault 
					? DefaultableBoolean.True : DefaultableBoolean.False;
				// ----------------------------------------------------------------------------
			}
		}

		#endregion //AllowRowFilteringResolved

		#region RowFilterActionResolved

		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
		// appearances.
		//
		/// <summary>
		/// Resolved RowFilterAction.
		/// </summary>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Advanced ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public RowFilterAction RowFilterActionResolved
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeRowFilterAction( ) )
					return this.overrideObj.RowFilterAction;

				return this.Layout.RowFilterActionDefault;
			}
		}

		#endregion // RowFilterActionResolved

		#region RowFiltersVersion

		internal int RowFiltersVersion
		{
			get
			{
				// SSP 7/31/02
				// Add the grand summaries version number off the band
				//
				//return this.rowFiltersVersion;

				int version = this.rowFiltersVersion;

				// SSP 4/24/03
				//
				if ( null != this.layout )
					version += this.layout.RowFiltersVersion;

				return version;
			}
		}

		#endregion //RowFiltersVersion

		#region BumpRowFiltersVersion

		internal void BumpRowFiltersVersion( )
		{
			this.rowFiltersVersion++;

			// SSP 8/1/03 UWG1654 - Filter Action
			// Bump cell child elements version.
			//
			if ( null != this.Layout )
			{
				this.Layout.BumpCellChildElementsCacheVersion( );

				// SSP 11/24/03 - Scrolling Till Last Row Visible
				//
				this.Layout.BumpGrandVerifyVersion( );
			}
		}

		#endregion //BumpRowFiltersVersion

		#region TraverseRowsCallback class definitition

		// MD 1/24/08
		// Made changes to allow for VS2008 style unit test accessors
		//internal class TraverseRowsCallback : RowsCollection.IRowCallback
		internal class TraverseRowsCallback : IRowCallback
		{
			private UltraGridBand band = null;
			private UltraGridColumn column = null;
			private Hashtable		hashTable = null;
			// SSP 8/1/03 UWG1654 - Filter Action
			// Added IsFilteredOut property off the UltraGridRow. We want to actually know whether
			// to include the filtered rows or not and not whether to include hidden rows or not.
			//
			//private bool includeHiddenRows;
			private bool includeFilteredOutRows;

			// SSP 4/18/05 - NAS 5.2 Added LogicalOperator to ColumnFiltersCollection.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//private RowsCollection rows = null;
			private ColumnFiltersCollection columnFilters = null;

			// SSP 3/17/06 BR10848
			// 
			internal bool hasImagesInTable = false;

			// SSP 8/1/03 UWG1654 - Filter Action
			// Added IsFilteredOut property off the UltraGridRow. We want to actually know whether
			// to include the filtered rows or not and not whether to include hidden rows or not.
			// Changed the name from includeHiddenRows to includeFilteredOutRows and any references
			// inside the constructor body.
			//
			//internal TraverseRowsCallback( UltraGridBand band, UltraGridColumn column, Hashtable hashTable, bool includeHiddenRows ) 
			// SSP 4/18/05 - NAS 5.2 Added LogicalOperator to ColumnFiltersCollection.
			// Added rows parameter.
			//
			//internal TraverseRowsCallback( UltraGridBand band, UltraGridColumn column, Hashtable hashTable, bool includeFilteredOutRows ) 
			internal TraverseRowsCallback( UltraGridBand band, UltraGridColumn column, Hashtable hashTable, bool includeFilteredOutRows, RowsCollection rows ) 
			{
				this.band = band;
				this.column = column;
				this.hashTable = hashTable;
				this.includeFilteredOutRows = includeFilteredOutRows;

				// SSP 4/18/05 - NAS 5.2 Added LogicalOperator to ColumnFiltersCollection.
				// Always include filtered out rows if we are or'ing the column filters across columns.
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.rows = rows;

				this.columnFilters = RowFilterMode.AllRowsInBand == band.RowFilterModeResolved
					? band.ColumnFilters : ( null != rows ? rows.ColumnFilters : null );
				Debug.Assert( null != this.columnFilters );
				if ( null != this.columnFilters && FilterLogicalOperator.Or == this.columnFilters.LogicalOperator )
					this.includeFilteredOutRows = true;
			}


			// MD 1/24/08
			// Made changes to allow for VS2008 style unit test accessors
			//bool RowsCollection.IRowCallback.ProcessRow( UltraGridRow row )
			bool IRowCallback.ProcessRow( UltraGridRow row )
			{
				Debug.Assert( row.Band == this.band );

				// skip the group by rows
				//
				if ( row is UltraGridGroupByRow )
					return true;
                
				// SSP 9/4/02 UWG1568
				// Now we are going to only exclude items that are filtered out by filters of columns
				// other than one the drop down is being filled for.
				//
				// -------------------------------------------------------------------------------------------
				//if ( !includeHiddenRows && row.Hidden )
				//	return true;
				// SSP 8/1/03 UWG1654 - Filter Action
				// Added IsFilteredOut property off the UltraGridRow. We want to actually know whether
				// to include the filtered rows or not and not whether to include hidden rows or not.
				// Changed the name from includeHiddenRows to includeFilteredOutRows.
				// We are changing the behavior of Hidden property. When the row is filtered out, we
				// are still returning false in Hidden unless ofcourse it was explicitly set to true
				// in which case we should not include that row in the filter drop down. Before we
				// use to return true for the Hidden if the row was filtered out.
				// Added below if statement and also changed the reference from row.Hidden to 
				// row.IsFilteredOut in the if condition below it.
				//
				if ( row.Hidden )
					return true;
				//if ( !includeHiddenRows && row.Hidden )
				if ( ! includeFilteredOutRows && row.IsFilteredOut )
				{
					// SSP 4/18/05 - Optimizations - NAS 5.2 Added LogicalOperator to ColumnFiltersCollection
					// Use the member columnFilters var.
					//
					// ------------------------------------------------------------------------------
					bool rowIsFiltered = null != this.columnFilters 
						&& ! this.columnFilters.DoesRowPassFilters( row, this.column );
					
					// ------------------------------------------------------------------------------

					if ( rowIsFiltered )
						return true;
				}
				// -------------------------------------------------------------------------------------------

				//object cellValue = row.GetCellValue( column );
				
				object cellVal = UltraGridBand.GetValueForFilterComparision( row, column );
				
				// Skip blanks
				//
				// MD 8/7/07 - 7.3 Performance
				// FxCop - Do not cast unnecessarily
				//if ( null == cellVal || cellVal is System.DBNull ||
				//    ( cellVal is string && 0 == ((string)cellVal).Length ) )
				//    return true;
				if ( null == cellVal || cellVal is System.DBNull )
					return true;

				string strCellVal = cellVal as string;
				if ( strCellVal != null && 0 == strCellVal.Length )
					return true;

				// SSP 7/19/02 UWG1397
				// The filter drop down should display what's visible in the cell. To do
				// that we have to call the editor's DataValueToText to convert the cell's
				// value into text.
				//
				// ------------------------------------------------------------------------------
				//string cellText =  cellVal.ToString( );
				string cellText = null;

				Debug.Assert( null != this.column, "Column is null !" );
				EmbeddableEditorBase editor = null;
				if ( null != this.column )
				{
                    // MRS 8/14/2009 - TFS20269
                    // If there's a PasswordChar at work, don't even bother. Skip this item
                    // because there's no point in displaying it on the filter list. 
                    //
                    char passwordChar;
                    bool hasPasswordChar = this.column.EditorOwnerInfo.GetPasswordChar(row, out passwordChar);
                    if (hasPasswordChar)
                        return true;

					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//EmbeddableEditorBase editor = column.Editor;
					editor = column.GetEditor( row );

					// MRS 1/7/05 - BR00516
					// Ignore ComparesByValue. We will always show the formatted 
					// text and if there are duplicate text entires, we will 
					// display the underlying value in parens
					//					// SSP 4/25/03 UWG2183
					//					// Only convert the value to a string via editor's DataValueToText method if the
					//					// editor compares by value.
					//					//
					//					//if ( null != editor )
					//					if ( null != editor && !editor.ComparesByValue( column.EditorOwnerInfo, row ) )
					// SSP 10/19/05 BR06887
					// Pass in the data filtered val.
					// 
					// ----------------------------------------------------------------------
					//cellText = editor.DataValueToText( row.GetCellValue( column ), this.column.EditorOwnerInfo, row );
					if ( null == editor.DataFilter )
						cellText = editor.DataValueToText( row.GetCellValue( column ), this.column.EditorOwnerInfo, row );
					else
						cellText = this.column.GetCellText( row );
					// ----------------------------------------------------------------------
				}

				if ( null == cellText )
					cellText = cellVal.ToString( );
				// ------------------------------------------------------------------------------

				// Skip blanks
				//
				if ( null == cellText || cellText.Length <= 0 )
				{
					// SSP 3/17/06 BR10848
					// If the column has a ValueList whose DisplayStyle is set to Picture then
					// the cells will display picture only. Also the cellText will be empty string
					// because that's what the EditorWithCombo returns when the DisplayStyle of
					// the underlying value list is Picture. What this means is that we have to
					// either display the image. 
					
					
					// 
					// ----------------------------------------------------------------------------
					if ( editor is EditorWithCombo )
					{
						ValueList vl = row.GetValueListResolved( this.column ) as ValueList;
						if ( null != vl && ValueListDisplayStyle.Picture == vl.DisplayStyle )
						{
							AppearanceData appData = new AppearanceData( );
							AppearancePropFlags flags = AppearancePropFlags.Image;
							row.ResolveEditorCellValueAppearance( this.column, ref appData, ref flags );
							if ( appData.Image is Image )
							{
								if ( ! this.hashTable.ContainsKey( cellVal ) )
								{
									this.hashTable.Add( cellVal, new ImageAndDisplayText( (Image)appData.Image, null ) );
									this.hasImagesInTable = true;
								}
							}
						}
					}
					// ----------------------------------------------------------------------------

					return true;
				}

				// MRS 1/7/05 - BR00516
				// There may be duplicate display values now, so use the cellVal
				// as the key into the has table. 
				//				if ( !this.hashTable.ContainsKey( cellText ) )
				//					this.hashTable.Add( cellText, cellVal );
				if ( !this.hashTable.ContainsKey( cellVal ) )
					this.hashTable.Add( cellVal, cellText );

				return true;
			}

			// SSP 3/17/06 BR10848
			// 
			internal class ImageAndDisplayText
			{
				internal Image image;
				internal string displayText;

				internal ImageAndDisplayText( Image image, string displayText )
				{
					this.image = image;
					this.displayText = displayText;
				}

				public override string ToString( )
				{
					return this.displayText;
				}
			}

			
		}

		#endregion //End of TraverseRowsCallback calss definitition

		#region GetValueForFilterComparision

		internal static object GetValueForFilterComparision( UltraGridRow row, UltraGridColumn column )
		{			
			object val = row.GetCellValue( column );

			// SSP 8/2/02 UWG1483
			// Compare by text when editor is using value list.
			//
			// ---------------------------------------------------------------------------
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//EmbeddableEditorBase editor = column.Editor;
			EmbeddableEditorBase editor = column.GetEditor( row );

			if ( null != editor )
			{
				// ZS 1/23/04 - Added data filter support.
				val = editor.GetDataFilteredDestinationValue(val, ConversionDirection.OwnerToEditor,
					column.EditorOwnerInfo, row);

				// Check to see if we should compare by value or text by calling editor's
				// CompareByValue method.
				//
				// SSP 8/21/03 - Cell Level Value List
				// Pass in the context of a row since there could be a value list off the cell.
				//
				//if ( !editor.ComparesByValue( column.EditorOwnerInfo, null ) )
				if ( !editor.ComparesByValue( column.EditorOwnerInfo, row ) )
					val = editor.DataValueToText( val, column.EditorOwnerInfo, row );
			}
			// ---------------------------------------------------------------------------

			if ( null == val )
				val = DBNull.Value;

			return val;
		}

		#endregion //GetValueForFilterComparision

		#region LoadFilterValueListHelper

        // MBS 12/9/08 - NA9.1 Excel Style Filtering
        // Removed this overload since it's not being used anymore
        //
        #region Removed
//        // SSP 8/5/03
//        // Changed the custom row filter dialog's Operator field to display all the cell values.
//        // Added an overload that takes in cellValuesOnly parameter.
//        // 
//#if DEBUG
//        /// <summary>
//        /// If rows is null, then it will use all the rows in the band. Otherwise
//        /// only the rows from the rows collection.
//        /// </summary>
//        /// <param name="valueList"></param>
//        /// <param name="rows"></param>
//        /// <param name="column"></param>
//        /// <param name="includeFilteredOutRows"></param>
//        /// <returns></returns>
//#endif
//        internal bool LoadFilterValueListHelper( ValueList valueList, RowsCollection rows, UltraGridColumn column, bool includeFilteredOutRows )
//        {
//            return this.LoadFilterValueListHelper( valueList, rows, column, includeFilteredOutRows, false );
//        }
        #endregion //Removed

        // SSP 11/25/03
		// Added following four properties.
		//
		#region FilterCustomText
		
		internal static string FilterCustomText
		{
			get
			{
				return SR.GetString( "RowFilterDropDownCustomItem" );
			}
		}

		#endregion // FilterCustomText

		#region FilterAllText
		
		internal static string FilterAllText
		{
			get
			{
				return SR.GetString( "RowFilterDropDownAllItem" );
			}
		}

		#endregion // FilterAllText

		#region FilterBlanks
		
		internal static string FilterBlanks
		{
			get
			{
				return SR.GetString( "RowFilterDropDownBlanksItem" );
			}
		}

		#endregion // FilterBlanks

		#region FilterNonBlanks

		internal static string FilterNonBlanks
		{
			get
			{
				return SR.GetString( "RowFilterDropDownNonBlanksItem" );
			}
		}

		#endregion // FilterNonBlanks

        // MBS 6/22/09 - TFS18639
        #region FilterErrors

        internal static string FilterErrors
        {
            get
            {
                return SR.GetString("RowFilterDropDownErrorsItem");
            }
        }
        #endregion //FilterErrors
        //
        #region FilterNonErrors

        internal static string FilterNonErrors
        {
            get
            {
                return SR.GetString("RowFilterDropDownNonErrorsItem");
            }
        }
        #endregion //FilterNonErrors

        // SSP 11/5/04 UWG3682 UWG3767
		// Added the else bock. If we aren't sorting by the keys then sort by the values.
		//
		#region FilterDropDownItemComparer

		// MRS 1/7/05 - BR00516
		// Re-wrote this class 
		//		private class FilterDropDownItemComparer : IComparer
		//		{
		//			private Hashtable textToValueTable = null;
		//
		//			internal FilterDropDownItemComparer( Hashtable textToValueTable )
		//			{
		//				this.textToValueTable = textToValueTable;
		//			}
		//
		//			public int Compare( object x, object y )
		//			{
		//				return RowsCollection.RowsSortComparer.DefaultCompare( 
		//					this.textToValueTable[ x ], this.textToValueTable[ y ] );
		//			}
		//		}
		private class FilterDropDownItemComparer : IComparer
		{
			private bool sortByDisplayText = false;

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			//
			private UltraGridColumn column;

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			//
			//internal FilterDropDownItemComparer( bool sortByDisplayText )
			internal FilterDropDownItemComparer( bool sortByDisplayText, UltraGridColumn column )
			{
				this.sortByDisplayText = sortByDisplayText;
				this.column = column;
			}

			public int Compare( object x, object y )
			{				
				DictionaryEntry xDic = (DictionaryEntry)x;
				DictionaryEntry yDic = (DictionaryEntry)y;

				int compResult;

				if (this.sortByDisplayText)
				{
					// JAS v5.2 GroupBy Break Behavior 5/4/05
					//
					//compResult = RowsCollection.RowsSortComparer.DefaultCompare(xDic.Value, yDic.Value);
					compResult = RowsCollection.RowsSortComparer.DefaultCompare(xDic.Value, yDic.Value, this.column);

					if ( compResult != 0)
						return compResult;
				}
				
				// JAS v5.2 GroupBy Break Behavior 5/4/05
				//
				//compResult = RowsCollection.RowsSortComparer.DefaultCompare(xDic.Key, yDic.Key);
				// SSP 12/9/05 BR07680 BR07852
				// If the RowFilterComparer is set use that.
				// 
				// --------------------------------------------------------------------
				//compResult = RowsCollection.RowsSortComparer.DefaultCompare(xDic.Key, yDic.Key, this.column);
				// SSP 5/31/06 BR13140
				// 
				
				if ( null != column.RowFilterComparer )
				{
					compResult = column.RowFilterComparer.Compare( xDic.Key, yDic.Key );
				}
				else
				{
					object xx = xDic.Key;
					object yy = yDic.Key;

					// If one is a string but not the other then convert both to strings.
					// 
					if ( ( xx is string ) ^ ( yy is string ) )
					{
						// Don't convert null's and DBNull's.

						if ( ! ( xx is string ) && null != xx && DBNull.Value != xx )
							xx = xx.ToString( );

						if ( ! ( yy is string ) && null != yy && DBNull.Value != yy )
							yy = yy.ToString( );
					}

					compResult = RowsCollection.RowsSortComparer.DefaultCompare( xx, yy, this.column );
				}
				// --------------------------------------------------------------------

				return compResult;
			}
		}

		#endregion // FilterDropDownItemComparer


		// SSP 8/1/03 UWG1654 - Filter Action
		// Added IsFilteredOut property off the UltraGridRow. We want to actually know whether
		// to include the filtered rows or not and not whether to include hidden rows or not.
		// Changed the name from includeHiddenRows to includeFilteredOutRows and any references
		// inside the function body.
		//
		//private bool LoadFilterValueListHelper( ValueList valueList, RowsCollection rows, UltraGridColumn column, bool includeHiddenRows )
		internal bool LoadFilterValueListHelper( ValueList valueList, RowsCollection rows, UltraGridColumn column, bool includeFilteredOutRows,
			// SSP 8/5/03
			// Changed the custom row filter dialog's Operator field to display all the cell values.
			// Added an overload that takes in cellValuesOnly parameter.
			// 
            // MBS 1/14/09 - TFS12274
            // Changed this to an enum so that we can have more flexibiility in what we add
			//bool cellValuesOnly ,
            FilterValueListOptions options)
		{
			valueList.ValueListItems.Clear( );

            // MBS 6/19/09 - TFS18639
            FilterOperandDropDownItems operandItems;
            if (column != null)
                operandItems = column.FilterOperandDropDownItemsResolved;
            else
            {
                Debug.Fail("Expected to have a non-null column to resolve the FilterOperandDropDownItems");
                operandItems = UltraGridColumn.DefaultFilterOperandDropDownItems;
            }

			// SSP 8/5/03
			// Changed the custom row filter dialog's Operator field to display all the cell values.
			// Added an overload that takes in cellValuesOnly parameter.
			// 
            // 1/14/09 - TFS12274
            // Changed to a switch statement 
			//if ( ! cellValuesOnly )
            switch (options)
            {
                case FilterValueListOptions.None:
                    {
                        // First add predefined choices
                        //

                        // SSP 4/18/05 - NAS 5.2 Filter Row
                        // Don't add the (All) option in filter row cells.
                        //
                        if (FilterUIType.HeaderIcons == this.FilterUITypeResolved &&
                            // MBS 6/19/09 - TFS18639
                            (operandItems & FilterOperandDropDownItems.All) == FilterOperandDropDownItems.All)
                        {
                            valueList.ValueListItems.Add(UltraGridBand.FilterAllText, UltraGridBand.FilterAllText);
                        }

                        // MBS 6/19/09 - TFS18639
                        if ((operandItems & FilterOperandDropDownItems.Custom) == FilterOperandDropDownItems.Custom)
                        {
                            // SSP 9/28/06 BR16079
                            // This is to workaround a regression issue that was introduced in the new implementation 
                            // of the ValueList. It was decided not to fix it so I had to implement a workaround in
                            // the grid. Essentially .NET ComboBox and the old ValueList implementation used to raise
                            // SelectionChangeCommitted when clicking on the same item that was previously selected.
                            // In our case, if you selected (Custom) and entered some custom conditions and okayed the
                            // dialog and then selected (Custom) again, SelectionChangeCommitted was getting fired and
                            // we displayed the dialog again. Now it doesn't fire a second time because the current value 
                            // is the same. This causes use to not show the custom filter dialog the second and successive
                            // times unless you change the value of the filter cell from (Custom) to something else (like
                            // clearing it). The workaround is to add a different value for (Custom) entry in the value list
                            // every time it drops down (this gets called everytime the filter list is dropped down).
                            // 
                            // --------------------------------------------------------------------------------------------------
                            //valueList.ValueListItems.Add( UltraGridBand.FilterCustomText, UltraGridBand.FilterCustomText );
                            if (FilterUIType.FilterRow == this.FilterUITypeResolved)
                                valueList.ValueListItems.Add(new object(), UltraGridBand.FilterCustomText);
                            else
                                valueList.ValueListItems.Add(UltraGridBand.FilterCustomText, UltraGridBand.FilterCustomText);
                            // --------------------------------------------------------------------------------------------------
                        }

                        // MBS 6/19/09 - TFS18639
                        if ((operandItems & FilterOperandDropDownItems.Blanks) == FilterOperandDropDownItems.Blanks)
                            valueList.ValueListItems.Add(UltraGridBand.FilterBlanks, UltraGridBand.FilterBlanks);

                        // MBS 6/19/09 - TFS18639
                        if ((operandItems & FilterOperandDropDownItems.NonBlanks) == FilterOperandDropDownItems.NonBlanks)
                            valueList.ValueListItems.Add(UltraGridBand.FilterNonBlanks, UltraGridBand.FilterNonBlanks);
                    }
                    break;

                // MBS 1/14/09 - TFS12274
                case FilterValueListOptions.FilterUIProvider:
                    {
                        // MBS 6/19/09 - TFS18639
                        if ((operandItems & FilterOperandDropDownItems.All) == FilterOperandDropDownItems.All)
                            valueList.ValueListItems.Add(UltraGridBand.FilterAllText, UltraGridBand.FilterAllText);

                        // MBS 6/19/09 - TFS18639
                        if ((operandItems & FilterOperandDropDownItems.Blanks) == FilterOperandDropDownItems.Blanks)
                            valueList.ValueListItems.Add(UltraGridBand.FilterBlanks, UltraGridBand.FilterBlanks);
                    }
                    break;
            }

            // MBS 6/19/09 - TFS18639
            // Add the options to filter by errors and non-errors.  Note that we respect the SupportsDataErrorInfo 
            // property even if the user has explicitly checked the Errors or NonErrors properties
            if (options != FilterValueListOptions.CellValuesOnly && this.SupportsDataErrorInfoOnCellsResolved)
            {
                if ((operandItems & FilterOperandDropDownItems.Errors) == FilterOperandDropDownItems.Errors)
                    valueList.ValueListItems.Add(UltraGridBand.FilterErrors, UltraGridBand.FilterErrors);
                //
                if ((operandItems & FilterOperandDropDownItems.NonErrors) == FilterOperandDropDownItems.NonErrors)
                    valueList.ValueListItems.Add(UltraGridBand.FilterNonErrors, UltraGridBand.FilterNonErrors);
            }
            //
            // If the user doesn't want us to populate the ValueList with the cell values, then there really isn't
            // any point in firing the BeforeRowFilterDropDownPopulate event so we can bail out here.
            if ((operandItems & FilterOperandDropDownItems.CellValues) != FilterOperandDropDownItems.CellValues)
                return true;

			// SSP 4/15/04 - Virtual Binding
			// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
			// from populating the filter drop down list. This can be useful if there are lot of rows
			// and it takes a long time for the ultragrid to populate the filter drop down list.
			// 
			// ----------------------------------------------------------------------------------------
			// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
			// Support Filtering in Combo and DropDown
			//UltraGrid grid = null != this.Layout ? this.Layout.Grid as UltraGrid : null;
			UltraGridBase grid = null != this.Layout ? this.Layout.Grid as UltraGridBase : null;
			if  ( null != grid )
			{
				BeforeRowFilterDropDownPopulateEventArgs e = 
					new BeforeRowFilterDropDownPopulateEventArgs( column, rows, valueList );

				// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support Filtering in Combo and DropDown
				//grid.FireEvent( GridEventIds.BeforeRowFilterDropDownPopulate, e );
				grid.FireCommonEvent( CommonEventIds.BeforeRowFilterDropDownPopulate, e );
				if ( e.Handled )
					return true;
			}
			// ----------------------------------------------------------------------------------------

			Hashtable hashTable = new Hashtable( );

			// SSP 3/17/06 BR10848
			// 
			TraverseRowsCallback traverseRowsCallback;

			if ( null != rows )
			{
				Debug.Assert( this == rows.Band, "rows.Band != this" );

				// SSP 9/2/03 UWG2330
				// Since we are resolving the column filters to the top most rows collection's
				// column filters, return the RowFiltersVersion off the top most rows 
				// collection.
				//
				// ----------------------------------------------------------------------------------------
				//rows.InternalTraverseRowsHelper( 
				//	this, 
				//	new TraverseRowsCallback( this, column, hashTable, includeFilteredOutRows ), false );
				rows.TopLevelRowsCollection.InternalTraverseRowsHelper( 
					this, 
					// SSP 4/18/05 - NAS 5.2 Added LogicalOperator to ColumnFiltersCollection.
					// Added rows parameter.
					//
					//new TraverseRowsCallback( this, column, hashTable, includeFilteredOutRows ), true );
					// SSP 3/17/06 BR10848
					// 
					//new TraverseRowsCallback( this, column, hashTable, includeFilteredOutRows, rows ), true );
					traverseRowsCallback = new TraverseRowsCallback( this, column, hashTable, includeFilteredOutRows, rows ), true );
				// ----------------------------------------------------------------------------------------
			}
			else
			{
				this.Layout.Rows.InternalTraverseRowsHelper( 
					this, 
					// SSP 4/18/05 - NAS 5.2 Added LogicalOperator to ColumnFiltersCollection.
					// Added rows parameter.
					//
					//new TraverseRowsCallback( this, column, hashTable, includeFilteredOutRows ), true );
					// SSP 3/17/06 BR10848
					// 
					//new TraverseRowsCallback( this, column, hashTable, includeFilteredOutRows, rows ), true );
					traverseRowsCallback = new TraverseRowsCallback( this, column, hashTable, includeFilteredOutRows, rows ), true );
			}

			// SSP 2/20/04 - Optimization
			// 
			// --------------------------------------------------------------------------------
			
			
			// MRS 1/7/05 - BR00516
			// *******************************************************************

			//			// Keys in the table are display texts. Values in the table are the actual
			//			// cell values.
			//			//			
			//			ICollection keys = hashTable.Keys;
			//			object[] uniqueDisplayTexts = new object[ keys.Count ];
			//			keys.CopyTo( uniqueDisplayTexts, 0 );
			//					
			//			// SSP 5/14/04 UWG3151
			//			// Sort by the values instead of display texts if the editor compares values
			//			// by values rather than the text. For example, when you have numbers, you
			//			// want to sort numerically and not by their text values.
			//			//
			//			// ----------------------------------------------------------------------------
			//			//Array.Sort( uniqueDisplayTexts );
			//			bool sortByDisplayText = null == column || ! column.Editor.ComparesByValue( column.EditorOwnerInfo, null );
			//			if ( sortByDisplayText )
			//			{
			//				Array.Sort( uniqueDisplayTexts );
			//			}
			//			// SSP 11/5/04 UWG3151 UWG3682 UWG3767
			//			// Added the else bock. If we aren't sorting by the text then sort by the values. Before
			//			// we were setting the SortyStyle on the value list but we don't want to do that because
			//			// that will interfere with the user manipulating the value list in 
			//			// BeforeRowFilterDropDown event.
			//			//
			//			else
			//			{
			//				Array.Sort( uniqueDisplayTexts, new UltraGridBand.FilterDropDownItemComparer( hashTable ) );
			//			}
			//			// ----------------------------------------------------------------------------
			//			
			//			ValueListItemsCollection valueListItems = valueList.ValueListItems;
			//			for ( int i = 0; i < uniqueDisplayTexts.Length; i++ )
			//				valueListItems.Add( hashTable[ uniqueDisplayTexts[i] ], uniqueDisplayTexts[i].ToString( ) );
			// *******************************************************************

			DictionaryEntry[] items = new DictionaryEntry[hashTable.Count];
			hashTable.CopyTo(items, 0);

			// Sort the Array by DisplayText first to check for duplicates.
			//
			// JAS v5.2 GroupBy Break Behavior 5/4/05 - Added the 'column' argument.
			//
			//Array.Sort(items, new UltraGridBand.FilterDropDownItemComparer( true ) );
			// SSP 3/17/06 BR10848
			// If the hash table has images as the values then don't sort by the display text, that's
			// because there is no display text. Enclosed the existing code into the if block.
			// 
			if ( ! traverseRowsCallback.hasImagesInTable )
			{
				Array.Sort(items, new UltraGridBand.FilterDropDownItemComparer( true, column ) );

				// We need to clone the items list so that we can still compare and find
				// duplicate items even as we are going through and changing them. 
				DictionaryEntry[] itemsClone = items.Clone() as DictionaryEntry[];

				//Loop through the items and if there are any duplicates, append the Value to the DisplayText
				for (int i = 0; i < items.Length; i++)
				{
					if ( i > 0 
						&& itemsClone[i].Value.ToString() == itemsClone[i-1].Value.ToString())
					{
						items[i].Value += " (" + items[i].Key + ")";
						continue;
					}

					if ( i < items.Length -1
						&& itemsClone[i].Value.ToString() == itemsClone[i+1].Value.ToString())
					{
						items[i].Value += " (" + items[i].Key + ")";
					}
				}
			}

			bool sortByDisplayText = null == column || ! column.Editor.ComparesByValue( column.EditorOwnerInfo, null );

			// SSP 3/17/06 BR10848
			// If the hash table has images as the values then don't sort by the display text, that's
			// because there is no display text.
			// 
			// ----------------------------------------------------------------------------------------
			if ( traverseRowsCallback.hasImagesInTable )
				sortByDisplayText = false;

			valueList.DisplayStyle = traverseRowsCallback.hasImagesInTable 
				? ValueListDisplayStyle.DisplayTextAndPicture : ValueListDisplayStyle.Default;
			// ----------------------------------------------------------------------------------------

			//
			// JAS v5.2 GroupBy Break Behavior 5/4/05 - Added the 'column' argument.
			//
			//Array.Sort(items, new UltraGridBand.FilterDropDownItemComparer( sortByDisplayText ) );
			Array.Sort(items, new UltraGridBand.FilterDropDownItemComparer( sortByDisplayText, column ) );

			ValueListItemsCollection valueListItems = valueList.ValueListItems;
			for ( int i = 0; i < items.Length; i++ )
			{
				// SSP 3/17/06 BR10848
				// 
				// --------------------------------------------------------------------
				//valueListItems.Add( items[i].Key, items[i].Value.ToString() );
				object val = items[i].Value;
				TraverseRowsCallback.ImageAndDisplayText imgAndText = val as TraverseRowsCallback.ImageAndDisplayText;
				ValueListItem vlItem = valueListItems.Add( items[i].Key, null == imgAndText ? val.ToString( ) : imgAndText.displayText );

				if ( null != imgAndText && null != imgAndText.image )
					vlItem.Appearance.Image = imgAndText.image;
				// --------------------------------------------------------------------
			}
            
			// SSP 11/5/04 UWG3682 UWG3767
			// Added the else bock. If we aren't sorting by the text then sort by the values. Before
			// we were setting the SortyStyle on the value list but we don't want to do that because
			// that will interfere with the user manipulating the value list in 
			// BeforeRowFilterDropDown event.
			//
			
			// --------------------------------------------------------------------------------

			// It seems that it's neccessary to BumpContentsVersion
			// on the value list. But shouldn't the value list take care 
			// of bumping any contents version itselft because it knows when the
			// value list items collection is modified.
			//
			valueList.BumpContentsVersion( );

			return true;
		}
		
		#endregion //LoadFilterValueListHelper

		#region LoadFilterValueList

		/// <summary>
		/// Loads the value list with unique values from appropriate rows
		/// depending on the RowFilterMode and the value of includeHiddenRows
		/// (which if true will include hidden rows as well).
		/// </summary>
		/// <param name="valueList"></param>
		/// <param name="headerElement"></param>
		/// <param name="includeFilteredOutRows"></param>
        /// <param name="options"></param>
		internal bool LoadFilterValueList( 
			Infragistics.Win.ValueList valueList, 
			HeaderUIElement headerElement, 
			// SSP 8/1/03 UWG1654 - Filter Action
			// Added IsFilteredOut property off the UltraGridRow. We want to actually know whether
			// to include the filtered rows or not and not whether to include hidden rows or not.
			// Changed the name from includeHiddenRows to includeFilteredOutRows and any references
			// inside the function body.
			//
			//bool includeHiddenRows )
			bool includeFilteredOutRows,
            // MBS 1/14/09 - TFS12274
            // We need to know the type of items to add to the list
            FilterValueListOptions options)
		{
			valueList.ValueListItems.Clear( );

			RowFilterMode filterMode = this.RowFilterModeResolved;

			Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader =
				(Infragistics.Win.UltraWinGrid.ColumnHeader)headerElement.GetContext( typeof( Infragistics.Win.UltraWinGrid.ColumnHeader ), true );

			Debug.Assert( null != columnHeader, "HeaderElement that has a filter drop down button must have a context of column header!" );

			if ( null == columnHeader )
				return false;

			Infragistics.Win.UltraWinGrid.UltraGridColumn column = columnHeader.Column;

			Debug.Assert( null != column, "ColumnHeader with no column !" );

			if ( null == column )			
				return false;

			bool retVal = false;

			if ( RowFilterMode.AllRowsInBand == filterMode )
			{
                // MBS 1/14/09 - TFS12274
                // We need to know the type of items to add to the list
                //
				//retVal = this.LoadFilterValueListHelper( valueList, null, column, includeFilteredOutRows );
                retVal = this.LoadFilterValueListHelper(valueList, null, column, includeFilteredOutRows, options);
			}
			else if ( RowFilterMode.SiblingRowsOnly == filterMode )
			{
				// Only rows in the associated rows collection
				Infragistics.Win.UltraWinGrid.RowsCollection rows =
					(Infragistics.Win.UltraWinGrid.RowsCollection)headerElement.GetContext( typeof( Infragistics.Win.UltraWinGrid.RowsCollection ), true );
				
				Debug.Assert( null != rows, "No RowsCollection context on the header element when Band.RowFilterModeResolved is SiblingRowsOnly." );

				if ( null != rows )
				{
                    // MBS 1/14/09 - TFS12274
                    // We need to know the type of items to add to the list
                    //
					//retVal = this.LoadFilterValueListHelper( valueList, rows, column, includeFilteredOutRows );
                    retVal = this.LoadFilterValueListHelper(valueList, rows, column, includeFilteredOutRows, options);
				}
				else
				{
					retVal = false;
				}
			}
			else
			{
				Debug.Assert( false, "Unknown value returned by Band.RowFilterModeResolved" );
				return false;
			}

			return retVal;
		}

		#endregion //LoadFilterValueList

		#region AreAnyBandFiltersActive

		internal bool AreAnyBandFiltersActive
		{
			get
			{
				RowFilterMode filterMode = this.RowFilterModeResolved;

				Debug.Assert( RowFilterMode.Default != filterMode );
				
				if ( this.HasColumnFilters && RowFilterMode.AllRowsInBand == filterMode )
				{
					return this.ColumnFilters.IsAnyColumnFilterActive;
				}

                //  BF 2/28/08  FR09238 - AutoCompleteMode
                //  If AutoCompleteMode is enabled for suggest mode, return true.
                FilterCondition autoCompleteFilterCondition = this.layout != null ? this.layout.AutoCompleteFilterCondition : null;
                if ( autoCompleteFilterCondition != null )
                    return true;

				return false;
			}
		}

		#endregion // AreAnyBandFiltersActive
		
		#endregion // End of "Filter Code" region


		// SSP 4/23/02
		// Summary Rows Feature Implementation
		//

		#region Summary Rows Feature

		#region Summaries

		/// <summary>
		/// A collection of SummarySettings objects. Use this collection to add new summaries as well as remove any existing summaries.
		/// </summary>
		[ 
		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Commented out the following attributes and added new ones below.
		//
		// Browsable( false ),
		// DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )
		DesignerSerializationVisibility( DesignerSerializationVisibility.Content ),
        LocalizedDescription("LD_UltraGridBand_P_Summaries")
		]		
        public SummarySettingsCollection Summaries
		{
			get
			{
				if ( null == this.summaries )
				{
					this.summaries = new SummarySettingsCollection( this );

					this.summaries.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.summaries;
			}
		}

		#endregion // Summaries

		#region SummaryFooterCaptionVisibleResolved
		
		// SSP 7/19/02 UWG1369
		// Moved SummaryFooterCaptionVisible to the override and made this an internal property.
		//
		internal bool SummaryFooterCaptionVisibleResolved
		{
			get
			{
				if ( this.HasOverride && this.Override.ShouldSerializeSummaryFooterCaptionVisible( ) )
					return DefaultableBoolean.False != this.Override.SummaryFooterCaptionVisible;

				return null != this.Layout 
					? this.Layout.SummaryFooterCaptionVisibleDefault 
					: true;
			}
		}

		#endregion // SummaryFooterCaptionVisibleResolved

		#region SummaryFooterCaption

		// SSP 7/26/02 UWG1416
		// Made the SummaryFooterCaption property localizable by adding Localiazable attribute.
		//
		/// <summary>
		/// Gets or sets summary footer caption substitution string.
		/// </summary>
		/// <remarks>
		/// <p>You can specify what gets shown in the summary footers' captions by setting this property to a substitution string. The format of the substition string is as follows:</p>
		/// <p>Any column names surrounded by square brakets will be substituted by that column's value. The column must be from the parent band. There are three tokens with special meaning: <pre>[BANDHEADER]</pre>, <pre>[SCROLLTIPFIELD]</pre> and <pre>[GROUPBYROWVALUE]</pre>. <pre>[BANDHEADER]</pre> will be substituted by the band header's caption <see cref="Header"/>. <pre>[SCROLLTIPFIELD]</pre> will be substitued by the value of the column associated with <see cref="ScrollTipField"/> in the parent band. <pre>GROUPBYROWVALUE</pre> will be substituted by the parent group-by row's value. This only makes sense for summary footers of rows that belong to a group-by row.</p>
		/// <p>NOTE: Columns you specify for substitution must be from the parent band. ScrollTipField used is also from parent band. If there is no parent band, they will not be substituted with anything and left as they are.</p>
		/// <p>Default value for the root rows is <pre>"Grand Summaries"</pre> and for child rows it's <pre>"Summaries for [BANDHEADER]: [SCROLLTIPFIELD]"</pre>.</p>
		/// <p>You can hide the summary footer caption by using the <see cref="UltraGridOverride.SummaryFooterCaptionVisible"/> property. Also the summary caption can be controlled on an individual summary footer by setting the <see cref="SummaryValuesCollection.SummaryFooterCaption"/> property. SummaryValuesCollection can be accessed using <see cref="RowsCollection.SummaryValues"/> property.</p>
		/// <seealso cref="UltraGridOverride.SummaryFooterCaptionVisible"/> <seealso cref="SummaryValuesCollection.SummaryFooterCaption"/>
		/// </remarks>
		[ Localizable( true ) ]
		[ LocalizedDescription( "LDR_SummaryFooterCaption" ) ]
		public string SummaryFooterCaption
		{
			get
			{
				if ( null == this.summaryFooterCaption )
				{
					if ( null == this.ParentBand )
						// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
						// Localized these strings.
						//
						//return DEFAULT_SUMMARY_FOOTER_CAPTION_BAND_0;
						return SR.GetString( "SummaryFooterCaption_RootRows" );
					else
						// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
						// Localized these strings.
						//
						//return DEFAULT_SUMMARY_FOOTER_CAPTION;
						return SR.GetString( "SummaryFooterCaption_ChildBandNonGroupByRows" );
				}

				return this.summaryFooterCaption;
			}
			set
			{
				// AS 1/8/03 - fxcop
				// Must supply an explicit CultureInfo to the String.Compare method.
				//if ( 0 != string.Compare( this.summaryFooterCaption, value ) )
				if ( 0 != string.Compare( this.summaryFooterCaption, value, false, System.Globalization.CultureInfo.CurrentCulture ) )
				{
					this.summaryFooterCaption = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.SummaryFooterCaption );
				}
			}
		}

		#endregion // SummaryFooterCaption

		#region SummaryFooterCaptionInternal

		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Added SummaryFooterCaptionInternal. This was done because we need to resolve the
		// summary footer caption differently based on whether the summary footer is for the
		// root row collection or a child row collection in group-by scenario. We can't use
		// the phrase "Grand Summaries" for group-by row child rows.
		//
		internal string SummaryFooterCaptionInternal
		{
			get
			{
				return this.summaryFooterCaption;
			}
		}

		#endregion // SummaryFooterCaptionInternal

		#region ShouldSerializeSummaryFooterCaption

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeSummaryFooterCaption( )
		{
			return null != this.summaryFooterCaption;
		}

		#endregion // ShouldSerializeSummaryFooterCaption

		#region ResetSummaryFooterCaption

		/// <summary>
		/// Resets SummaryFooterCaption property to its default value.
		/// </summary>
		public void ResetSummaryFooterCaption( )
		{
			this.SummaryFooterCaption = null;
		}

		#endregion // ResetSummaryFooterCaption

		#region HasSummaries

		internal bool HasSummaries
		{
			get
			{
				return null != this.summaries && this.summaries.Count > 0;
			}
		}

		#endregion // HasSummaries

		#region ShouldSerializeSummaries

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeSummaries()
		{
			return this.HasSummaries;
		}

		#endregion //ShouldSerializeSummaries

		#region ResetSummaries

		/// <summary>
		/// Resets Summaries property to its default value.
		/// </summary>
		public void ResetSummaries( )
		{
			if ( null != this.summaries )
				this.summaries.Clear( );			
		}

		#endregion // ResetSummaries

		#region InternalRefreshSummaries

		internal void InternalRefreshSummaries( )
		{
			// When data changes, we need to refresh the summaries to reflect the
			// changes.
			//
			UltraGridBand band = this;

			bool dirtyGridElem = false;

			while ( null != band )
			{
				if ( band.HasSummaries )
					dirtyGridElem = band.Summaries.DirtySummarySettings( this ) || dirtyGridElem;

				band = band.ParentBand;
			}

			if ( dirtyGridElem && null != this.Layout )
				this.Layout.DirtyGridElement( );
		}

		#endregion // InternalRefreshSummaries

		#endregion // Summary Rows Feature
				
		private void InitializeOrderedColumns()
		{
			//RobA 9/14/01 UWG279
			this.orderedColumnsDirty = false;			

			int capacity = this.Columns.Count;

			if ( capacity < 10 )
				capacity = 10;

			if ( null == this.orderedColumns )
				this.orderedColumns = new HeadersCollection( capacity );
			else
			{
				this.orderedColumns.InternalClear();
				this.orderedColumns.Capacity = capacity;
			}

			// add in all the columns
			//
			for( int i = 0; i < this.Columns.Count; i++ )
			{
				this.orderedColumns.InternalAdd( this.Columns[i].Header );
			}

			// JJD 10/16/01
			// Sort the columns in visible position order 
			//
			if ( this.orderedColumns.Count > 1 )
				this.orderedColumns.Sort( new HeaderComparer() );

			// SSP 7/23/03 - Fixed headers
			// Update the visible position variable values.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.SetVisiblePositionsToActual( this.orderedColumns );
			UltraGridBand.SetVisiblePositionsToActual( this.orderedColumns );

			this.orderedColumns.SetReadOnly( true );

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			this.DirtyTabOrderedColumns( );
		}


		private void InitializeOrderedGroups()
		{
			// SSP 7/18/03 - Fixed headers
			// Added orderedGroupsDirty flag. Reset it here.
			//
			this.orderedGroupsDirty = false;

			int capacity = this.Groups.Count;

			if ( capacity < 10 )
				capacity = 10;

			if ( null == this.orderedGroups )
				this.orderedGroups = new HeadersCollection( capacity );
			else
			{
				this.orderedGroups.InternalClear();
				this.orderedGroups.Capacity = capacity;
			}

			// add in all the groups
			//
			for( int i = 0; i < this.Groups.Count; i++ )
			{
				this.orderedGroups.InternalAdd( this.Groups[i].Header );
			}

			// JJD 10/16/01
			// Sort the groups in visible position order
			//
			if ( this.orderedGroups.Count > 1 )
				this.orderedGroups.Sort( new UltraGridBand.HeaderComparer() );

			// SSP 7/23/03 - Fixed headers
			// Update the visible position variable values.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.SetVisiblePositionsToActual( this.orderedGroups );
			UltraGridBand.SetVisiblePositionsToActual( this.orderedGroups );

			this.orderedGroups.SetReadOnly( true );

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			this.DirtyTabOrderedColumns( );
		}

		internal HeadersCollection OrderedColumnHeaders
		{
			get
			{
				if ( null == this.orderedColumns ||
					this.orderedColumns.Count != this.Columns.Count || 
					//RobA 9/14/01 UWG279 
					this.orderedColumnsDirty )
				{
					this.InitializeOrderedColumns();
				}
                
				return this.orderedColumns;
			}
		}

		
		internal HeadersCollection OrderedGroupHeaders
		{
			get
			{
				if ( null == this.orderedGroups ||
					//ROBA 8/8/01 UWG129
					this.orderedGroups.Count != this.Groups.Count 
					// SSP 7/18/03 - Fixed headers
					// Added orderedGroupsDirty flag.
					//
					|| this.orderedGroupsDirty )
				{
					this.InitializeOrderedGroups();
				}

				return this.orderedGroups;
			}
		}

		internal bool HasOrderedGroups
		{
			get 
			{
				// JJD 10/24/01
				// Check the groups collection instead of the
				// orderedgroups collection
				//
				return this.orderedGroups != null && 
					this.orderedGroups.Count > 0 ? true : false; 
			}
		}

		internal bool HasGroups
		{
			get 
			{
				// JJD 10/24/01
				// Check the groups collection instead of the
				// orderedgroups collection
				//
				// SSP 7/24/03 - Optmizations
				// Although I am not sure if this will have any effect whatsoever on the 
				// performance (the compiler has to be smart enough to get rid of true 
				// and false literals), however just in case.
				//
				//return this.groups != null && 
				//	this.groups.Count > 0 ? true : false; 
				return null != this.groups && this.groups.Count > 0;
			}
		}


		internal HeadersCollection OrderedHeaders
		{
			get
			{
				// if we have groups then return the ordered collection of groups
				//
				if ( GroupsDisplayed )
					return this.OrderedGroupHeaders;

				// otherwise, return the ordered collection of columns
				//
				return this.OrderedColumnHeaders;
			}
		}


		//=-----------------------------------------------------------------------------
		// FUNCTION:    CBand::GetOrderedGroupsClone
		//
		// DESCRIPTION: 
		// RETURNS:     ArrayList object with GroupHeader instances as its memebers
		//
		//=-----------------------------------------------------------------------------
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList GetOrderedGroupHeadersClone()
		internal List<HeaderBase> GetOrderedGroupHeadersClone()
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList orderedGroupsClone = new ArrayList( this.OrderedGroupHeaders.Count );
			List<HeaderBase> orderedGroupsClone = new List<HeaderBase>( this.OrderedGroupHeaders.Count );

			for ( int i = 0; i < this.OrderedGroupHeaders.Count; i++ )
			{	
				orderedGroupsClone.Add( this.OrderedGroupHeaders[ i ] );
			}

			return orderedGroupsClone;
		}

		//=-----------------------------------------------------------------------------
		// FUNCTION:    CBand::GetOrderedColsClone
		//
		// DESCRIPTION: 
		// RETURNS:     ArrayList object with ColumnHeader instances as its memebers
		//
		//=-----------------------------------------------------------------------------
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList OrderedColsClone
		internal List<HeaderBase> OrderedColsClone
		{
			get
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList orderedColsClone = new ArrayList( this.OrderedColumnHeaders.Count );
				List<HeaderBase> orderedColsClone = new List<HeaderBase>( this.OrderedColumnHeaders.Count );

				if ( null != orderedColsClone )
				{
					for ( int i = 0; i < this.OrderedColumnHeaders.Count; i++ )
					{
						ColumnHeader columnHeader = this.OrderedColumnHeaders[i] as ColumnHeader;

						Debug.Assert( null != columnHeader, "A member of OrderedColumnHeaders is not a ColumnHeader" );

						if ( null != columnHeader )
							orderedColsClone.Add( columnHeader  );
					}
				}

				return orderedColsClone;
			}
		}



		internal int GetChildBandCount()
		{
			return this.GetChildBandCount( false );		
		}
		internal int GetChildBandCount( bool excludeHidden )
		{
			if ( excludeHidden )
			{
				// if this band is hidden or the view style is single
				// then return 0
				//
				if ( this.hidden ||
					!this.layout.ViewStyleImpl.IsMultiBandDisplay )
					return 0;
			}

			int count = 0;
			
			// SSP 11/12/02 UWG1818
			// If this.Index is less than zero for whatever reason (maybe the band is disposed
			// of), then return 0 without going through the loop below.
			//
			// SSP 4/22/03 - Optimizations.
			// Commented out original code below and added new code.
			//
			
			BandsCollection bands = this.layout.SortedBands;
			int bandsCount = bands.Count;

			// If there is only one band in the collection, then no band can have any child bands.
			// In this case, just return 0.
			//
			if ( bandsCount > 1 )
			{
				// 4/15/05 - Optimizations
				//
				//int index = this.Index;
				int index = bands.IndexOf( this );

				if ( index >= 0 )
				{
					for ( int i = 1 + index; i < bandsCount; i++ )
					{
						UltraGridBand band = bands[i];
				
						if ( band.ParentBand == this )
						{
							if ( excludeHidden && band.HiddenResolved )
								continue;

							count++;
						}
					}
				}
			}

			return count;
		}

		internal IBindingList BindingList
		{
			get
			{
				// SSP 1/3/02 Optimization
				//
				
				// MD 7/26/07 - 7.3 Performance
				// Reduce duplicate code
				//if ( this.bindingManager is CurrencyManager )
				//    return ((CurrencyManager)this.bindingManager).List as IBindingList;
				return this.List as IBindingList;
			}
		}

		internal IList List
		{
			get
			{
				// SSP 1/3/02 Optimization
				//
				
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Do not cast unnecessarily
				//if ( this.bindingManager is CurrencyManager )
				//    return ((CurrencyManager)this.bindingManager).List;
				CurrencyManager currencyManager = this.bindingManager as CurrencyManager;

                if (currencyManager != null)
                {
                    // MBS 11/4/08 - TFS9064
                    // If at any point we have a currency manager and it can provide us
                    // with a list, we will take that over any list that we've already been
                    // holding onto since that should take precedence.
                    this.cachedList = null;

                    return currencyManager.List;
                }
				
                // MBS 11/4/08 - TFS9064
                //                
                // If we can't get the cached list from the currency manager, see if we can
                // get it from the actual data source, assuming it implements IEnumerable<>
                // and that we're on the root band (otherwise we will perform the same
                // check for the RowsCollection instead).                
                //
                //return null;
                if (this.cachedList == null && this.ParentBand == null && 
                    this.Layout != null && this.Layout.Grid != null)
                {
                    Type listType;
                    object dataSource = this.Layout.Grid.DataSource;
                    if (dataSource != null && DataBindingUtils.ImplementsGenericIEnumerable(dataSource.GetType(), out listType))
                    {
                        Type newList = typeof(List<>).MakeGenericType(listType);
                        this.cachedList = Activator.CreateInstance(newList, dataSource) as IList;
                    }
                }
                return this.cachedList;
			}
		}

		internal RowsCollection GetCurrentParentRows( bool initializeIfNeeded )
		{
			// By default pass in false for dontSyncRows to not the change the 
			// behaviour of the original method.
			//
			return this.GetCurrentParentRows( initializeIfNeeded, false );
		}

		// SSP 10/28/02 UWG1787
		// Added overloads of GetCurrentParentRows and GetCurrentRow that take in the new dontSyncRows 
		// paramter. DontSyncRows indicates that we don't sync the rows in the process.
		// This we needed to do because the CurrencyManager changes the value of Position property 
		// without firing thbe PositionChanged event. It does fire, however it fires after changing
		// the value of Position. For example, accessing Position property may return 1 and next time
		// you access it, it may return 4 without having fired any PositionChanged event. It does fire
		// later on, but we end up syncing the wrong rows prematurely.
		//
		internal RowsCollection GetCurrentParentRows( bool initializeIfNeeded, bool dontSyncRows )
		{
			if ( this.parentBand == null )
				return this.layout.Rows;

			// SSP 10/28/02 UWG1787
			// Pass along the dontSyncRows parameter.
			//
			//Infragistics.Win.UltraWinGrid.UltraGridRow parentRow = this.parentBand.GetCurrentRow( false );
			Infragistics.Win.UltraWinGrid.UltraGridRow parentRow = this.parentBand.GetCurrentRow( false, dontSyncRows );

			// JJD 8/28/01
			// Make sure ChildBands is not null
			//
			if ( parentRow == null ||
				parentRow.ChildBands == null )
				return null;

			if ( !initializeIfNeeded &&
				!parentRow.ChildBands[this].HaveRowsBeenInitialized )
				return null;
											
			// SSP 9/5/01
			// Get the actual associated rows, not the group by rows.
			// The child band could contain GroupByRows. We don't
			// want group by rows for this function's purpose
			//
			//return parentRow.ChildBands[this].Rows;
			return parentRow.ChildBands[this].TopLevelRows;
		}

		#region GetCurrentParentRowsForAddNew

		// SSP 8/18/05 P856
		// When SyncWithCurrencyManager is set to false the ActiveRow is not syncrhonized
		// with the Position. In that case when the band.AddNew is called, the row should
		// be added to the row collection associated with the UltraGrid's ActiveRow, not 
		// the binding maanger's Position.
		// 
		internal RowsCollection GetCurrentParentRowsForAddNew( )
		{
			UltraGrid grid = null != this.Layout ? this.Layout.Grid as UltraGrid : null;
			// SSP 2/4/08 BR26482
			// Use ActiveRow if set rather than assume that if SyncWithCurrencyManager
			// is true then the active row will necessarily correspond to the position
			// of the binding manager. Because of the way the binding manager works in
			// that when it recieves Reset notification, it maintains its Position 
			// property value and doesn't raise PositionChanged, the grid's active row
			// can get out of sync with the binding manager's position.
			// 
			//if ( null == grid || grid.SyncWithCurrencyManager || this.IsRootBand )
			if ( null == grid || this.IsRootBand )
				return this.GetCurrentParentRows( true );

			UltraGridRow activeRow = this.Layout.ActiveRow;

			// MD 8/14/07 - 7.3 Performance
			// Band is abstract, cache it so we don't make multiple virtual calls
			//if ( null != activeRow && null != activeRow.Band )
			if ( activeRow == null )
				return null;

			UltraGridBand band = activeRow.Band;

			if ( null != band )
			{
				// MD 8/14/07 - 7.3 Performance
				// Band is abstract, use the cahced value
				//if ( activeRow.Band.IsTrivialDescendantOf( this ) )
                if (band.IsTrivialDescendantOf(this))
                {
                    activeRow = activeRow.GetAncestorRow(this);
                    Debug.Assert(null != activeRow);
                    return null != activeRow ? activeRow.ParentCollection : null;
                }
                // MRS 5/27/2008 - BR33295
                #region Old Code
                //// MD 8/14/07 - 7.3 Performance
                //// Band is abstract, use the cahced value
                ////else if ( this.ParentBand == activeRow.Band )
                //else if ( this.ParentBand == band )
                //{
                //    // MD 8/14/07 - 7.3 Performance
                //    // ChildBands is virtual, cache it so we don't make multiple virtual calls
                //    //if ( null != activeRow.ChildBands && activeRow.ChildBands.Exists( this ) )
                //    //    return activeRow.ChildBands[ this ].Rows;
                //    ChildBandsCollection childBands = activeRow.ChildBands;

                //    if ( null != childBands && childBands.Exists( this ) )
                //        return childBands[ this ].Rows;
                //    else
                //        Debug.Assert( false );
                //}
                #endregion //Old Code
                //
                else
                {
                    UltraGridBand parentBand = this.ParentBand;
                    UltraGridRow row = activeRow;
                    do
                    {
                        if (row.Band == parentBand)
                        {
                            ChildBandsCollection childBands = row.ChildBands;

                            if (null != childBands && childBands.Exists(this))
                                return childBands[this].Rows;
                            else
                                Debug.Assert(false);
                        }
                        
                        row = row.ParentRow;
                    }
                    while (row != null);
                }
			}

			return null;
		}
		
		#endregion // GetCurrentParentRowsForAddNew

		internal static bool binding_debug = false;


		private void OnListChanged( object sender, System.ComponentModel.ListChangedEventArgs e )
		{			
			if ( binding_debug )
			{
				//Debug.WriteLine( ((Band)this.layout.Bands[this.Index]).Columns[0].Header.Caption );

				//Debug.WriteLine( "Has parent = " + (this.parentBand != null ).ToString() );

				//Debug.WriteLine( "Index = " + this.Index.ToString() );
		
				Debug.WriteLine( ++this.Layout.Grid.tmp + ": Bands[" + this.Index + "].OnListChanged: " );
				//Debug.WriteLine( "   Bands[0].BindingList == sender = " + ( sender ==  this.Layout.Bands[0].BindingList  ) );
				//Debug.WriteLine( "   Bands[1].BindingList == sender = " + ( sender ==  this.Layout.Bands[1].BindingList  ) );
				//Debug.WriteLine( "   Bands[2].BindingList == sender = " + ( sender ==  this.Layout.Bands[2].BindingList  ) );
				Debug.WriteLine( "   e.Type                   = " + Enum.GetName( typeof( ListChangedType ), e.ListChangedType ) );
				Debug.WriteLine( "   e.OldIndex               = " + e.OldIndex );
				Debug.WriteLine( "   e.NewIndex               = " + e.NewIndex );
				Debug.WriteLine( "   BindingManager.Position  = " + this.BindingManager.Position );
				Debug.WriteLine( "   BindingManager.Count     = " + this.BindingManager.Count );			
				if ( null != this.ParentBand )
					Debug.WriteLine( "   ParentBindingManager.Pos   = " + this.ParentBand.BindingManager.Position );
				Debug.WriteLine("");
			
				
			}

			// SSP 1/18/02
			// If we receive ItemAdded or ItemDeleted message then pass it onto
			// the current rows collection because when AddedRow is canceled
			// (by calling CancelEdit on the list object), it does no notify the
			// RowsCollection.BindingList. We do get notified though, so pass
			// them to the rows collection.
			//
			switch ( e.ListChangedType )
			{
				case ListChangedType.ItemAdded:
				case ListChangedType.ItemDeleted:
					if ( this.IsCurrentRowRowsCollectionCreated( ) )
					{
						RowsCollection rows = this.GetCurrentParentRows( false );
						if ( null != rows )
						{
							// SSP 11/19/03 Add Row Feature
							// Pass in true for calledFromBandListChanged parameter.
							//
							//rows.OnListChangedHelper( e );
							rows.OnListChangedHelper( e, true );
						}
					}
					break;
			}
			


			//RowsCollection rows = null;
			switch ( e.ListChangedType )
			{
					// SSP 8/13/01
					// Item Added, Changed, Moved, Deleted events don't get
					// fired predictably ( sometimes they get fired, other times
					// they don't, I guess it depends on the underlying iplementation, and
					// also how the data is bound, how the hierarchy was contructed etc.					
				
					
				case ListChangedType.PropertyDescriptorAdded:
				case ListChangedType.PropertyDescriptorChanged:
				case ListChangedType.PropertyDescriptorDeleted:
				{
                    // MRS 2/2/2009 - TFS13220
                    // When you rename a child band or change it's key, there are two notifications, 
                    // one on the child band that was changed and one on it's parent band. 
                    // When you use a BindingSource, these notifications pass in Null for the 
                    // PropertyDescriptor. This causes a problem because, if the child notification
                    // comes before the parent, the child band's BindingManager will not be valid
                    // and it will return the property descriptors on the parent (for some 
                    // unfathomable reason). 
                    // To make matters even more complex, we also apparently get notifications on 
                    // the child bands of the band whose key was changed. So we need to recursively
                    // walk up the entire parent chain to essentially re-verify everything from the
                    // top down.
                    // 
                    if (ListChangedType.PropertyDescriptorChanged == e.ListChangedType &&
                        this.parentColumn != null &&                        
                        e.PropertyDescriptor == null &&
                        this.parentBand != null
                        )
                    {
                        this.parentBand.OnListChanged(sender, e);
                        return;
                    }

                    // MRS 4/9/2009 - TFS16482
                    // If we get a PropertyDescriptorChanged notification, compare the 
                    // ElementName on the column's CalcReference to the column's Key.
                    // This will tell us if the column key has changed. And if it has, 
                    // we need to remove and re-add the CalcReference with the new key. 
                    //
                    if (ListChangedType.PropertyDescriptorChanged == e.ListChangedType)                    
                    {                        
                        int index = e.OldIndex;
                        if (index >= 0 && index < this.Columns.Count)
                        {
                            UltraGridColumn column = this.Columns[index];                            
                            if (column.FormulaHolder.CalcReference.ElementName != column.Key)
                            {
                                this.Columns[index].FormulaHolder.RemoveReferenceFromCalcNetwork();
                                this.Columns[index].FormulaHolder.AddReferenceToCalcNetwork();
                            }
                        }
                    }
                        
					this.ColumnAddedRemovedChangedHandler( e );
					break;				
				}
					// SSP 12/18/01
					// Moved the code for adding and removing
					// columns in ColumnAddedRemovedChangedHandler
					//
					
			}
		}

		// SSP 5/5/08 BR32208
		// With the fix to BR11351, now we could have bands without binding managers in 
		// recursive data source situations. We need to still allow adding in that case.
		// 
		//internal bool AllowAdd
		internal bool AllowAdd( IList dataList )
		{
			//get
			//{
			//RobA UWG296 9/24/01
			//return ( this.BindingList != null &&
			//	this.BindingList.AllowNew &&
			//	this.AllowAddNewResolved == Infragistics.Win.DefaultableBoolean.True );

			if ( this.AllowAddNewResolved == AllowAddNew.No )
				return false;

			// SSP 5/5/08 BR32208
			// Use the passed in list instead of this.List.
			// 
			// --------------------------------------------------------------
			IBindingList bindingList = dataList as IBindingList;
			if ( bindingList != null )
				return bindingList.AllowNew;
			//If this is not a bindinglist check the list's fixed size property
			//
			else
				return null != dataList && ! dataList.IsFixedSize;
			
			// --------------------------------------------------------------

			//}
		}

        // MBS 9/16/08 - TFS6776
        // Since we don't always create a binding list for a band (such as in the case
        // where we have a recursive structure within a BindingSource), we may need
        // to check the Rows collection's list.  We will first check the band to see
        // if we can determine whether we can delete, but if we can't, we will check 
        // the parent rows collection of a row.
        //
        #region Old Code
//#if DEBUG
//        /// <summary>
//        /// Returns a value that determines whether the user is allowed to delete rows.
//        /// </summary>
//        ///	<remarks>
//        ///	<p class="body">You can use this property to determine whether the user is permitted to delete data in the band. This property is based on the resolved value of <b>AllowDelete</b> for the override that applies to the band, and the capabilities of the data source to which the band is bound. If either of these factors are perventing the user from deleting rows from the band, the <b>AllowDelete</b> property will return False. <b>AllowDelete</b> will return True only if data can actually be deleted from the band.</p>
//        ///	</remarks>
//        //[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
//#endif
        //internal bool AllowDelete
        //{
        //    get
        //    {
        //        //RobA UWG296 9/24/01
        //        return ( this.BindingList != null &&
        //            this.BindingList.AllowRemove &&
        //            this.AllowDeleteResolved == Infragistics.Win.DefaultableBoolean.True );

        //        if ( this.AllowDeleteResolved == Infragistics.Win.DefaultableBoolean.False )
        //            return false;

        //        if ( this.BindingList != null )
        //        {
        //            // SSP 10/20/03 UWG2597
        //            // Check for AllowRemove instead of AllowNew.
        //            //
        //            //return  this.BindingList.AllowNew;
        //            return  this.BindingList.AllowRemove;
        //        }
                
        //        //If this is not a bindinglist check the list's fixed size property
        //        //
        //        else
        //        {
        //            if ( this.List == null )
        //                return false;
        //            else							
        //                return !this.List.IsFixedSize;
        //        }                
        //    }
        //}        
        #endregion //OldCode
        //
        internal bool AllowDelete(out bool canDelete)
        {
            canDelete = false;

            if (this.AllowDeleteResolved == Infragistics.Win.DefaultableBoolean.False)
                return true;

            if (this.BindingList != null)
            {
                canDelete = this.BindingList.AllowRemove;
                return true;
            }

            //If this is not a bindinglist check the list's fixed size property
            //
            else
            {
                if (this.List == null)
                    return false;
                else
                {
                    canDelete = !this.List.IsFixedSize;
                    return true;
                }
            }     
        }
        //        
        internal bool AllowDelete(UltraGridRow row)
        {
            if (row == null)
                return false;

            RowsCollection parentCollection = row.ParentCollection;
            if (parentCollection != null)
            {
                if (parentCollection.BindingList != null)
                    return parentCollection.BindingList.AllowRemove;

                if (parentCollection.Data_List != null)
                    return !parentCollection.Data_List.IsFixedSize;
            }
            return false;
        }

		// SSP 11/12/03 Add Row Feature
		// Following private method (overload of InternalAddNew that takes in an object) is not being used
		// anywhere so commented it out.
		//
		

		internal void SetGroupByValues( UltraGridGroupByRow parentGroupByRow, Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			object cellVal = parentGroupByRow.Value;
			row.SetCellValue( parentGroupByRow.Column, cellVal );
			
			UltraGridGroupByRow tmp = parentGroupByRow.ParentRow as UltraGridGroupByRow;
			if ( null != tmp )
				this.SetGroupByValues( tmp, row );
		}


		private bool GetAddNewContext( out UltraGridGroupByRow groupByRow )
		{
			groupByRow = null;

			// SSP 3/18/05 BR02633
			// Call EnsureTempActiveRowAssigned.
			//
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			if ( null != grid )
				grid.EnsureTempActiveRowAssigned( );

			Infragistics.Win.UltraWinGrid.UltraGridRow activeRow = this.Layout.ActiveRow;

			
			if ( null == activeRow && this.HasGroupBySortColumns )
				return false;

			if ( null == activeRow  &&  null == this.ParentBand )
				return true;			

			if ( null == activeRow )
				return false;

			if ( !this.HasGroupBySortColumns && 
				( activeRow.Band == this  ||
				activeRow.Band == this.ParentBand 
				// SSP 12/8/03 UWG2747
				// If the active row is in a sibling band, then we do have enough add-row
				// context and thus don't retrun false.
				//
				|| activeRow.Band.ParentBand == this.ParentBand 
				) )				
				return true;

			UltraGridRow tmpRow = activeRow;

			while ( null != tmpRow && tmpRow.Band != this )
			{
				tmpRow = tmpRow.ParentRow;
			}

			if ( null == tmpRow || tmpRow.Band != this )
				return false;

			if ( this.HasGroupBySortColumns )
			{
				if ( !( tmpRow is UltraGridGroupByRow ) )
				{
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					// Reordered to be slightly more performant while debugging
					//Debug.Assert( null != tmpRow.ParentRow && tmpRow.ParentRow is UltraGridGroupByRow );
					//groupByRow = tmpRow.ParentRow as UltraGridGroupByRow;
					groupByRow = tmpRow.ParentRow as UltraGridGroupByRow;
					Debug.Assert( null != groupByRow );
				}
				else
				{                    
					UltraGridGroupByRow gr = (UltraGridGroupByRow)tmpRow;
                    
					Debug.Assert( this.HasGroupBySortColumns );
                    
					if ( this.SortedColumns.IndexOf( gr.Column.Key ) != 
						this.SortedColumns.LastGroupByColIndex )
						return false;

					groupByRow = gr;
				}
			}

			
			return true;
		}


		internal UltraGridRow InternalAddNew( )
		{	
			UltraGrid grid = this.layout.Grid as UltraGrid;

			Debug.Assert( null != grid, "Band.InternalAddNew method should be called only " +
				"if attached to an instance of UltraGrid" );
            
			// Adding of rows only supported in UltraGrid.
			//
			if ( null == grid )
				return null;


			// SSP 12/17/03 UWG2809
			// If the user has given focus to a template add-row and thus added a new row
			// and the new row has not been modified, then don't add a new row when the
			// user clicks on the add-new button for this band.
			// 
			// ----------------------------------------------------------------------------------
			UltraGridRow currentActiveRow = grid.ActiveRow;
			if ( null != currentActiveRow && currentActiveRow.IsUnModifiedAddRowFromTemplate
				// SSP 1/27/04 UWG2928
				// Only do so if the template add-row is from the same band as the 
				// add-new-button band.
				// 
				&& this == currentActiveRow.Band )
			{
				UltraGridCell currentActiveCell = grid.ActiveCell;

				if ( null != currentActiveCell && currentActiveCell.Row != currentActiveRow )
				{
					Debug.Assert( false );
					currentActiveCell = null;
				}

				if ( null == currentActiveCell || ! currentActiveCell.IsInEditMode )
				{
					if ( null != currentActiveCell 
						&& currentActiveCell.CanEnterEditMode 
						&& currentActiveCell.CanBeModified )
						currentActiveCell.EnterEditMode( false );
					else
						currentActiveRow.EnterFirstCellInEditMode( );
				}

				// Return the add-row.
				//
				return currentActiveRow;
			}
			// ----------------------------------------------------------------------------------

			// SSP 1/2/02
			// Added this method because AddNew is public method so it needs to 
			// throw exception when AddNew row fails so the calling user can catch the 
			// exception and take apropriate actions.
			// Inside the grid, we need to catch the exception and raise DataError
			// event which should be done in an internal method (which I just added
			// by the name of InternalAddNew( ) (this one).
			//
			try
			{
				// SSP 1/15/02 UWG928
				// Before adding new row, make sure to exit the edit mode so that
				// any change in the cell gets committed to the binding list before
				// add new gets called.
				//
				UltraGridCell activeCell = null != this.Layout ? this.Layout.ActiveCell : null;
				if ( null != activeCell )
				{
					if ( activeCell.IsInEditMode )
					{
						// If exitting the edit mode was cancelled, then don't add the new row.
						//
						if ( activeCell.ExitEditMode( ) || activeCell.IsInEditMode )
							throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_4") );
					}
				}

				
				
				
				
				
				
				
				

				// SSP 9/18/06 BR14674 
				// Added AddNew overload that takes in calledFromAddNew parameter. Pass in false for it.
				// 
				//UltraGridRow newRow = this.AddNew( );
				UltraGridRow newRow = this.AddNew( false );

				// JJD 1/18/02 - UWG948
				// Attempt to enter edit mode on the first activatable cell in the new row
				//
				if ( newRow != null )
				{
					// SSP 11/25/02 UWG1858
					// Only activate the first cell if no other cell from the row has been activated.
					// Enclosed the below code into the if block.
					//
					// SSP 17/04 BR00446
					// AddNewHelper method activates the added row. If the activation fails because
					// for example BeforeRowDeactivate is canceled and as a result the previous row
					// remains active then we should not attempt to activate the added row again 
					// which is what the EnterFirstCellInEditMode would do. We should simply return
					// in that case.
					// 
					//if ( null == grid.ActiveCell || grid.ActiveCell.Row != newRow )
					if ( null == grid.ActiveCell && this.Layout.ActiveRow == newRow )
					{
						// SSP 11/17/03 Add Row Feature
						// Moved the following code into EnterFirstCellInEditMode helper method.
						//
						// ------------------------------------------------------------------------------------
						
						newRow.EnterFirstCellInEditMode( );
						// ------------------------------------------------------------------------------------
					}
				}
			}
			catch ( Exception e )
			{
			
				DataErrorInfo dataError = new DataErrorInfo( e, null, null, null,
					DataErrorSource.RowAdd, 
					SR.GetString("DataErrorRowAddMessage", e.Message) ); //"Unable to add a row:\n" + e.Message );
							
				grid.InternalHandleDataError( dataError );
			}

			// If AddNew unsucessfull, return null.
			//
			return null;
		}

		/// <summary>
		/// Displays the AddNew row for the band. If the current <b>ActiveRow</b> does not provide enough context, an error occurs.
		/// </summary>
		///	<remarks>  
		///	<p class="body">The <b>AddNew</b> method will display a data entry row for the user to enter data for a new record. When this method is invoked, the AddNew row appears at the bottom of the group of rows (in the current band) that contains the active row. If there is no active row, and the control does not have enough context to determine where the add row should appear, an error occurs.</p>
		///	<p class="body">If you attempt to invoke the <b>AddNew</b> method on a read-only data source, you will also receive an error.</p>
		///	</remarks>
		/// <returns></returns>
		public Infragistics.Win.UltraWinGrid.UltraGridRow AddNew( )
		{
			return this.AddNew( true );
		}

		// SSP 9/18/06 BR14674 
		// Added AddNew overload that takes in calledFromAddNew parameter.
		// 
		internal Infragistics.Win.UltraWinGrid.UltraGridRow AddNew( bool calledFromAddNew )
		{
			// SSP 9/18/06 BR14674
			// Moved this from the InternalAddNew so if this method is called externally then the 
			// resulting behavior will be the same.
			// 
			// ------------------------------------------------------------------------------------
			// SSP 4/11/02 UWG1045
			// Before a new row is added, update the active row explicitly
			// because it will be done either by the binding manager implicitly 
			// or by the grid later on. If it's done implicitly by the
			// binding manager, then BeforeRowUpdate and AfterRowUpdate won't
			// be fired and not only that any problems updating the row won't
			// be notified to the user through the Error event. So do all these
			// by updating the active row before adding a new row.
			//
			//-------------------------------------------------------------------
			UltraGridRow activeRow = this.Layout.ActiveRow;

			// SSP 12/27/05 BR08401 
			// Don't update the row if the UpdateMode is explicitly set to OnUpdate. Note
			// that the row is going to be updated (EndEdit'ed) by the binding manager
			// anyways (the binding manager updates the current row whenever its Position
			// is changed), however the idea is that we don't want to fire Before/After
			// Update events when the UpdateMode is set to OnUpdate. This change defeats
			// the purpose of the original bug fix (for UWG1045) when the UpdateMode is
			// OnUpdate however this is not an issue since the fact that the binding
			// manager automatically updates the row whenever it's Position is changed
			// (which happens when a row is added), the UpdateMode of OnUpdate is not
			// useful when adding a row, except to prevent Before/After Update from
			// firing.
			// 
			//if ( null != activeRow )
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			if ( null != activeRow && ( null == grid || UpdateMode.OnUpdate != grid.UpdateMode ) )
			{
				// If the active row was not updated, either because of an update 
				// error or because it's cancelled then return null without 
				// proceeding any further.
				//
				if ( !activeRow.Update( )
					// SSP 9/18/06 BR14674 
					// If called from AddNew then still proceed with adding a new row. This is to 
					// maintain previous AddNew behavior.
					// 
					&& ! calledFromAddNew )
				{
					return null;
				}
			}
			//-------------------------------------------------------------------
			// ------------------------------------------------------------------------------------

			UltraGridGroupByRow groupByRow = null;
			bool ret = this.GetAddNewContext( out groupByRow );

			if ( !ret )
				throw new InvalidOperationException( SR.GetString( "LER_Exception_328" ) );

			if ( this.HasGroupBySortColumns && null == groupByRow )
				throw new InvalidOperationException( SR.GetString( "LER_Exception_329" ) );
			
			// Make sure the rows are initialized for the child band we are adding a new row to.
			//
			// SSP 8/18/05 P856
			// When SyncWithCurrencyManager is set to false the ActiveRow is not syncrhonized
			// with the Position. In that case when the band.AddNew is called, the row should
			// be added to the row collection associated with the UltraGrid's ActiveRow, not 
			// the binding maanger's Position.
			// 
			//RowsCollection rows = this.GetCurrentParentRows( true );
			RowsCollection rows = this.GetCurrentParentRowsForAddNew( );

			Debug.Assert( null != rows, "No rows !" );
			UltraGridRow addedRow = null;
			// SSP 1/27/04 UWG2929
			// We need to go throught the template add-row logic when the template add-row
			// is hidden because currently there is an add-row. So instead of using
			// IsTemplateAddRowVisible, use TemplateRowLocationDefault.
			//
			//if ( null != rows && rows.IsTemplateAddRowVisible )
			if ( null != rows && TemplateAddRowLocation.None != rows.TemplateRowLocationDefault )
				rows.AddTemplateAddRowToDataListHelper( true, out addedRow );
			else
				addedRow = this.AddNewHelper( rows );

			return addedRow;
		}

		// SSP 11/13/03 Add Row Feature
		// Added rows parameter and made the method internal. Commented out the original method
		// and added the new AddNew which is a heavily modified version of the original.
		//
		internal Infragistics.Win.UltraWinGrid.UltraGridRow AddNewHelper( RowsCollection rows )
		{
			UltraGridLayout layout = this.layout;
			UltraGrid grid = layout.Grid as UltraGrid;
			if ( grid == null )
				throw new NotSupportedException(Shared.SR.GetString("LER_EXCEPTION_314"));

			// SSP 5/5/08 BR32208
			// Moved this below because we need to pass the context of list.
			// 
			//if ( !this.AllowAdd )
			//	throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_6"));

			if ( null != rows && rows.IsGroupByRows )
				throw new NotSupportedException( "Can't add a row to group-by rows collection." );

			// If the passed in rows is null, then use the rows collection associated with
			// current parent row.
			//
			if ( null == rows )
			{
				// SSP 8/18/05 P856
				// When SyncWithCurrencyManager is set to false the ActiveRow is not syncrhonized
				// with the Position. In that case when the band.AddNew is called, the row should
				// be added to the row collection associated with the UltraGrid's ActiveRow, not 
				// the binding maanger's Position.
				// 
				//rows = this.GetCurrentParentRows( true );
				rows = this.GetCurrentParentRowsForAddNew( );

				UltraGridRow activeRow = null != rows ? rows.GetRowWithListIndex( this.BindingManager.Position ) : null;
				Debug.Assert( null != activeRow, "There should have been a row associated with binding manager position." );
				rows = null != activeRow ? activeRow.ParentCollection : null;
			}

			UltraGridGroupByRow groupByRow = null;
			bool hasValidContext = false;
			IList list = null;

			if ( null != rows )
			{
				groupByRow = rows.ParentRow as UltraGridGroupByRow;
				list = rows.ResolvedDataList;
				hasValidContext = null != list;
			}

			if ( ! hasValidContext )
				throw new InvalidOperationException(SR.GetString("LER_Exception_328"));

			// SSP 5/5/08 BR32208
			// Moved this here from above because we needed to pass the context of list.
			// 
			if ( ! this.AllowAdd( list ) )
				throw new NotSupportedException( Shared.SR.GetString( "LE_NotSupportedException_6" ) );

			if ( this.HasGroupBySortColumns && null == groupByRow )
				throw new InvalidOperationException( SR.GetString("LER_Exception_329") );
			
			// JAS 2005 v2 XSD Support
			// If the band already contains the maximum number of allowed rows (or more)
			// then we must disallow the new row from being added.
			//
			if( rows != null && ! rows.ContainsLessThanMaxRows )
				return null;

			// Expand all the ancestors. If the user cancels BeforeRowExpanded and prevents the 
			// operation from completing successfully, then return null.
			//
			if ( null != rows.ParentRow )
			{
				if ( ! rows.ParentRow.ExpandAncestors( ) )
					return null;

				// SSP 1/27/04
				// ExpandAncestors does not expande the row on which the method is called on.
				// So expand the parent row here.
				//
				rows.ParentRow.Expanded = true;
				if ( ! rows.ParentRow.Expanded )
					return null;
			}

			// Fire BeforeRowInsert event.
			//
			BeforeRowInsertEventArgs eventArgs = new BeforeRowInsertEventArgs( this, rows.ParentRow );
	
			grid.FireEvent( GridEventIds.BeforeRowInsert, eventArgs );
			
			// If it was cancelled return null.
			//
			if ( eventArgs.Cancel )
				return null;

			Infragistics.Win.UltraWinGrid.UltraGridRow newRow = null;

			try
			{
				this.inAddNew = true;
				this.addedRow = null;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.parentGroupByRowToAddTheRowTo = groupByRow;

				// BindingManager.AddNew is only going to work if
				// we are bound to a binding list, otherwise
				// we have to add it manually.
				if ( list is IBindingList )
				{
					// SSP 2/9/04 UWG2946
					// Set the inAddingTemplateAddRow flag to false temporarily while we
					// execute the following workaround (the code in the try block below)
					// because the following code leads to binding manager adding and 
					// deleting rows which I don't know why it does but it does. Run the 
					// UWG2946 test project to see.
					// Note that in the finally block of associated with the following 
					// try block we are resetting the inAddingTemplateAddRow to its 
					// original value.
					//
					// ----------------------------------------------------------------------
					bool origInAddingTemplateAddRow = rows.inAddingTemplateAddRow;
					rows.inAddingTemplateAddRow = false;
					// ----------------------------------------------------------------------

					// SSP 1/2/02
					// end current editing on the binding manager 
					// to prevent adding of new row to a parent row that is
					// still in add new status.
					//
					try
					{
						// MD 7/27/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( null != rows.TopLevelRowsCollection && null != rows.TopLevelRowsCollection.ParentRow )
						//    rows.TopLevelRowsCollection.ParentRow.EndEdit( );
						RowsCollection topLevelRowsCollection = rows.TopLevelRowsCollection;

						if ( null != topLevelRowsCollection && null != topLevelRowsCollection.ParentRow )
							topLevelRowsCollection.ParentRow.EndEdit();

						// SSP 5/5/08 BR32208
						// With the fix to BR11351, now we could have bands without binding managers in 
						// recursive data source situations. We need to still allow adding in that case.
						// Check for BindingManager being null.
						// 
						// ------------------------------------------------------------------------------
						//if ( null != this.ParentBand )
						//	this.ParentBand.BindingManager.EndCurrentEdit( );							
						UltraGridBand parentBand = this.ParentBand;
						BindingManagerBase parentBandBindingManager = null != parentBand ? parentBand.BindingManager : null;
						if ( null != parentBandBindingManager )
							parentBandBindingManager.EndCurrentEdit( );
						// ------------------------------------------------------------------------------

						// SSP 1/21/02 UWG927
						
						// below code which basically ensures that the parent row is 
						// commited (which above EndEdit call should have done) when 
						// adding child rows to the 1st level band when the parent row 
						// (to which the child rows will be added to) is an AddRow 
						//
						//---------------------------------------------------------
						// Only for the 1st level bands
						//
						// SSP 11/25/02 UWG1820
						// Regarding comment above, a related problem occurs in lower 
						// level bands as well. The parent id doesn't get carried over.
						//
						//if ( null != this.ParentBand &&
						//	null == this.ParentBand.ParentBand )
						// SSP 5/5/08 BR32208
						// Check for BindingManager being null.
						// 
						//if ( null != this.ParentBand )
						if ( null != parentBandBindingManager )
						{
							UltraGridRow tmpRow = rows.TopLevelRowsCollection.ParentRow;
							if ( null != tmpRow &&
								// SSP 7/29/03 UWG2396
								// Changed the behavior of IsAddRow so that now it returns false if EndEdit is called
								// on it whereas before it used to wait until PositionChanged was fired on the binding 
								// manager. This change was in response to UWG2396 bug. However as a result, we
								// have to modify the condition here.
								//
								//tmpRow.IsAddRow &&
								tmpRow.InternalIsAddRow &&
								tmpRow.AreAllAncestorsCurrentRows &&
								null != tmpRow.ParentCollection )
							{
								if ( tmpRow.ParentCollection.Count > 1 )
								{
									int tmpIndex = tmpRow.ListIndex - 1;
									if ( tmpIndex < 0 )
										tmpIndex = tmpRow.ListIndex + 1;

									int oldPos = parentBandBindingManager.Position;

									bool oldIgnoreDataSourePositionChange = layout.IgnoreDataSourePositionChange;
									layout.IgnoreDataSourePositionChange = true;

									try
									{
										parentBandBindingManager.Position = tmpIndex;
									}
										// SSP 1/23/02
										// Catch any exceptions.
										//
									catch ( Exception )
									{
									}
									finally
									{
										// SSP 1/23/02
										// Enclosed this into a try-finally block because we want to
										// make sure we reset the IgnoreDataSourePositionChange 
										//
										try
										{
											parentBandBindingManager.Position = oldPos;
										}
										finally
										{
											layout.IgnoreDataSourePositionChange = oldIgnoreDataSourePositionChange;
										}
									}								
								}
									// SSP 11/25/02 UWG1820
									// If the number of rows in the parent rows collection is 1, then
									// call Refresh on the currency manager of the parent band if the parent
									// row is an add new row.
									// Added the else block.
								else
								{
									CurrencyManager cm = parentBandBindingManager as CurrencyManager;

									if ( null != cm )
										cm.Refresh( );
								}
							}
						}
						//---------------------------------------------------------
					}
					catch ( Exception exc )
					{
						throw new InvalidOperationException( 
							SR.GetString("LER_Exception_330", exc.Message ));
					}
						// SSP 2/9/04 UWG2946
						// Enclosed the following code into finally block.
						//
					finally
					{
						// SSP 1/20/04 UWG2908
						// Make sure the rows are loaded and sorted because when we add the new row
						// at the end of the rows collection. If the rows are not already sorted, then
						// when we add the new row, the rows collection will sort the rows when it goes
						// to sync and as a result the added row will also get sorted along with other
						// rows. The result will be that the added row will appear at where ever it 
						// falls in the sort order rather than at the end of the rows collection which
						// is what we want.
						// Added below line of code. Accessing Count will cause the rows to get synced
						// as well as sorted if not already done so.
						//
						// ----------------------------------------------------------------------------
						int tmpRows = rows.Count;
						// ----------------------------------------------------------------------------

						// SSP 2/9/04 UWG2946
						// Reset the flag to its original value. See comment above with the same bug
						// number.
						//
						rows.inAddingTemplateAddRow = origInAddingTemplateAddRow;
					}

					// Add a new row.
					//
					if (
						// SSP 5/5/08 BR32208
						// With the fix to BR11351, now we could have bands without binding managers in 
						// recursive data source situations. We need to still allow adding in that case.
						// Added the check for BindingManager being null.
						// 
						null != this.BindingManager &&
						( null == rows.ParentRow || rows.ParentRow.AreAllAncestorsCurrentRows ) )
					{
						this.BindingManager.AddNew( );

						// SSP 12/9/05 BR07901
						// If the new row was inserted somewhere other than at the end of the list
						// then the binding manager doesn't re-position itself to the inserted row.
						// We need to do that otherwise the added row's IsAddRow will return false.
						// 
						// ------------------------------------------------------------------------
						try
						{
							if ( null != addedRow && null != this.BindingManager
								&& this.BindingManager.Position != addedRow.ListIndex )
								this.BindingManager.Position = addedRow.ListIndex;
						}
						catch ( Exception e )
						{
							Debug.Assert( false, "Exception !", e.Message );
						}
						// ------------------------------------------------------------------------
					}
					else
					{
						// SSP 5/5/08 BR32208
						// Reget the data list in case calling cm.Refresh above could 
						// have changed the value of the chaptered property descriptor.
						// 
						// ------------------------------------------------------------
						//( (IBindingList)list ).AddNew( );
						rows.DirtyDataList( );
						list = rows.ResolvedDataList;
						if ( list is IBindingList )
							( (IBindingList)list ).AddNew( );
						else
						{
							Debug.Assert( false );
							return null;
						}
						// ------------------------------------------------------------
					}
				}
				else
				{
					bool itemAdded = false;

					if ( 1 == this.Columns.BoundColumnsCount )
					{
						UltraGridColumn column = this.Columns[0];

						if ( column.PropertyDescriptor is Infragistics.Win.UltraWinGrid.ValuePropertyDescriptor )
						{
							// ZS 3/3/04 - Common code moved to Infragistics.Win.DataBindingUtils.
							//if ( this.IsKnownType( column.PropertyDescriptor.PropertyType ) )
							if ( DataBindingUtils.IsKnownType( column.PropertyDescriptor.PropertyType ) )
							{
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//object o = this.CreateObjectOfKnownType( column.PropertyDescriptor.PropertyType );
								object o = UltraGridBand.CreateObjectOfKnownType( column.PropertyDescriptor.PropertyType );

								// SSP 5/5/08 BR32208
								// Use the list local variable.
								// 
								//this.List.Add( o );
								list.Add( o );

								rows.DirtyRows( );

								itemAdded = true;

								// SSP 12/12/03
								// If the list that we are adding a new object is not IBindingList (like 
								// in the situation where the grid is bount to an ArrayList) and we are 
								// adding directly to the list, then make sure the binding manager's 
								// position points to the new row object that we just added.
								//
								try
								{
									// SSP 5/5/08 BR32208
									// With the fix to BR11351, now we could have bands without binding managers in 
									// recursive data source situations. We need to still allow adding in that case.
									// Check for BindingManager being null and also use list local variable.
									// 
									//if ( this.BindingManager.Position != this.List.Count - 1 )
									if ( null != this.BindingManager && this.BindingManager.Position != list.Count - 1 )
										this.BindingManager.Position = list.Count - 1;
								}
								catch ( Exception e )
								{
									Debug.Assert( false, "Exception !", e.Message );
								}
							}
						}
					}

					if ( !itemAdded )
					{
						throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_7") );
					}
				}

				// SSP 11/17/03 Add Row Feature
				// Call SyncRows to sync the rows collection. SyncRows will
				// also create a new row for the new item and assign it to the addedRow
				// member var. Added call to SyncRows.
				//
				// SSP 2/17/04
				// Only SyncRows if rows is marked dirty.
				// Added the if condition to the existing SyncRows statement.
				//
				if ( rows.IsRowsDirty )
					rows.SyncRows( );
			}
			finally
			{			
				newRow = this.addedRow;
				this.inAddNew = false;
				this.addedRow = null;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.parentGroupByRowToAddTheRowTo = null;
			}

			//Debug.Assert( null != newRow, "addedRow was not set in Band.AddNew( ) !" );

			if ( null == newRow )
				newRow = rows.GetRowWithListIndex( rows.Count - 1 );
								
			if ( null != newRow )
			{
				// SSP 8/18/05 P856
				// If SyncWithCurrencyManager is false then we would not have responded to the 
				// PositionChanged notifications and the new row would not have become the active 
				// row. Therefore make the new row the active row if SyncWithCurrencyManager set 
				// to false.
				// 
				// SSP 5/5/08 BR32208
				// With the fix to BR11351, now we could have bands without binding managers in 
				// recursive data source situations. We need to still allow adding in that case.
				// Therefore also activate the row if binding manager is null.
				// 
				//if ( null != grid && ! grid.SyncWithCurrencyManager )
				if ( null != grid && ( !grid.SyncWithCurrencyManager || null == this.BindingManager ) )
					grid.SetTempActiveRow( newRow, false );

				newRow.SetDataChanged( true );

				// Now scroll the added row into view

				RowScrollRegion rsr = layout.ActiveRowScrollRegion;

				// if no active row scroll region, use the first one
				if ( null == rsr && layout.RowScrollRegions.Count > 0 )
					rsr = layout.RowScrollRegions[0];
				
				if ( null != rsr )
				{
					// SSP 9/10/02 UWG1651
					// Fix for UWG1651 is to update the grid before regenerate visible rows and
					// scroll rows into view gets called. This will trigger the active row
					// to be set to the TempActiveRow in the OnPaint. We don't want that to
					// happen in the middle of the ScrollRowIntoView operation below.
					//
					if ( null != grid )
					{
						// SSP 11/18/03 Add Row Feature
						// Instead of forcing a paint to ensure that TempActiveRow is assigned 
						// to ActiveRow, call the EnsureTempActiveRowAssigned method. This caused
						// a flicker with template add-row.
						//
						//grid.Update( );
						grid.EnsureTempActiveRowAssigned( );
					}

					// If VisibleRows collection is dirty, then regenerate visible rows
					rsr.RegenerateVisibleRows( );
					rsr.ScrollRowIntoView( newRow );
				}
			}

			//RobA UWG441 10/3/01
			//fire After event
			//
			Debug.Assert( null != newRow );
			if ( null != newRow )
			{
				RowEventArgs eArgs = new RowEventArgs( newRow );
				grid.FireEvent( GridEventIds.AfterRowInsert, eArgs );
			}

			return newRow;
		}


		

		
		internal BindingManagerBase BindingManager
		{
			get 
			{
				return this.bindingManager;
			}
		}       

		
		internal string DataMember
		{
			get
			{
				return this.dataMember;
			}
		}

		#region InitBandKey

		// SSP 1/24/05 BR00269
		// Added InitBandKey method. Code in there is moved from InitListManager method.
		//
		private void InitBandKey( )
		{
			string newKey = null;

			if ( this.parentColumn != null )
			{
				newKey = this.parentColumn.PropertyDescriptor.Name;
			}
			else if ( this.bindingManager is CurrencyManager ) 
			{
				CurrencyManager cm = (CurrencyManager)this.bindingManager;
				if (cm.List is ITypedList) 
				{
					// SSP 3/20/07 BR20741
					// 
					// ------------------------------------------------------------------
					//newKey = ((ITypedList)cm.List).GetListName(null);
					BindingSource bindingSource = cm.List as BindingSource;
					if (null != bindingSource)
					{
						newKey = bindingSource.DataMember;
					}
					// SSP 6/18/07 BR23242 
					// BindingSource.DataMember may be null in which case fall back to 
					// GetListName.
					// 
					//else
					if ( null == newKey || 0 == newKey.Length )
					{
						newKey = ((ITypedList)cm.List).GetListName(null);
					}
					// ------------------------------------------------------------------
				} 
				else 
				{
					newKey = cm.List.GetType().Name;
				}				
			} 
			else 
			{
				newKey = this.dataMember;
			}

			// MD 5/22/09 - TFS16438
			// Unhook the calc references before the band key changes. We can unhook and rehook them afterwards 
			// because we won't be able to find the references when the band key changes since their absolute 
			// names will have changed.
			this.RemoveCalcReferences();

			base.Key = newKey;

			// MD 5/22/09 - TFS16438
			// Hook the calc references in the band again. They now have different absolute names, so they are 
			// technically new references to the band.
			this.AddCalcReferences();
		}

		#endregion // InitBandKey

		internal void InitListManager( BindingManagerBase bindingManager,
			string dataMember,
			Infragistics.Win.UltraWinGrid.UltraGridBand [] oldBands ) 
		{
			// unwire the events on the old datasource
			UnWireDataSource();

			this.dataMember         = dataMember;
			this.bindingManager     = bindingManager;

			// SSP 1/24/05 BR00269
			// Added InitBandKey method. Code in there is moved from here.
			//
			this.InitBandKey( );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//bool result = this.InitColumns( oldBands );
			this.InitColumns( oldBands );

			this.hookIntoList = true;
			if ( this == this.Layout.SortedBands[0] )
			{
				this.Layout.Rows.BindToList( );
			}

			// SSP 3/15/04 - Designer Usability Improvements Related
			// Added ExtractDataStructure method to be able to extract the data structure from
			// a data source. If we are only extracting the data strcuture, don't hook into
			// the binding manager and don't keep it around as well.
			//
			// ------------------------------------------------------------------------------
			if ( null != this.Layout && this.Layout.ExtractDataStructureOnly )
			{
				this.bindingManager = null;
				return;
			}
			// ------------------------------------------------------------------------------

			// wire the events on the new datasource
			WireDataSource();
		}

		#region EnsureChildBandsLoaded

		
		
		

		// SSP 12/16/05
		// 
		
		
		
		private static void EnsureBindingManagerLoaded( UltraGridColumn chapteredColumn, UltraGridBand band )
		{
			if ( null != chapteredColumn && null != chapteredColumn.PropertyDescriptor )
			{
				// SSP 10/24/06 BR15430
				// 
				// ------------------------------------------------------------------------------------
				//chapteredColumn.InitPropertyDescriptor( chapteredColumn.PropertyDescriptor, null );
				string childBandDataMember;
				BindingManagerBase childLM = chapteredColumn.CreateChildBandBindingManagerHelper( out childBandDataMember );
				if ( null != childLM )
					chapteredColumn.InitPropertyDescriptor( chapteredColumn.PropertyDescriptor, null );
				// ------------------------------------------------------------------------------------
			}
		}

		// SSP 6/2/05 BR03609
		// Added EnsureChildBandsLoaded method.
		// 
		private void EnsureChildBandsLoaded( )
		{
			int i, count;

			UltraGridLayout layout = this.Layout;

			if ( this.GetChildBandCount( ) < this.Columns.ChapteredColumnsCount
				// SSP 5/18/07 BR22728
				// Added below condition. Don't create child bands if that will violate
				// MaxBandDepth constraint.
				// 
				&& null != layout && this.HierarchicalLevel < layout.MaxBandDepthEnforced - 1 )
			{
				for ( i = 0, count = this.Columns.Count; i < count; i++ )
				{
					UltraGridColumn column = this.columns[i];
					if ( column.IsChaptered )
					{
						UltraGridBand band = GridUtils.GetBandAssociatedWithChapteredColumn( column );

						
						
						
						if ( null == band || null == band.BindingManager )
							UltraGridBand.EnsureBindingManagerLoaded( column, band );
						
						
					}
				}
			}
			else if ( null != this.Layout && null != this.Layout.SortedBands )
			{
				BandsCollection bands = this.Layout.SortedBands;

				for ( i = 0, count = bands.Count; i < count; i++ )
				{
					UltraGridBand band = bands[i];
					if ( null != band && null == band.BindingManager )
						
						
						//band.EnsureBindingManagerLoaded( );
						UltraGridBand.EnsureBindingManagerLoaded( band.ParentColumn, band );
				}
			}
		}

		#endregion // EnsureChildBandsLoaded

		internal bool IsBoundToIBindingList
		{
			get
			{
				return null != this.BindingList;
			}
		}


		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal object CreateObjectOfKnownType( Type type )
		internal static object CreateObjectOfKnownType( Type type )
		{
			object ret = null;

			if ( typeof( System.DateTime ).Equals( type ) )
			{
				ret = System.DateTime.Now;
			}
				// SSP 1/31/03
				// Added char and bool entries
				//
			else if ( typeof( char ) == type )
			{
				return char.MinValue;
			}
			else if ( typeof( bool ) == type )
			{
				return false;
			}
				// SSP 1/24/02 UWG984
				// Added block for DayOfWeek
			else if ( typeof( System.DayOfWeek ).Equals( type ) )
			{
				ret = System.DateTime.Now.DayOfWeek;
			}
			else if ( typeof( int ) == type )
			{
				ret = (int)0;
			}
			else if ( typeof( uint ) == type )
			{
				ret = (uint)0;
			}
			else if ( typeof( short ) == type )
			{
				ret = (short)0;
			}
			else if ( typeof( ushort ) == type )
			{
				ret = (ushort)0;
			}
			else if ( typeof( long ) == type )
			{
				ret = (long)0;
			}
			else if ( typeof( ulong ) == type )
			{
				ret = (ulong)0;
			}
			else if ( typeof( float ) == type )
			{
				ret = (float)0.0f;
			}
			else if ( typeof( double ) == type )
			{
				ret = (double)0.0;
			}
			else if ( typeof( decimal ) == type )
			{
				ret = (decimal)0.00m;
			}
			else if ( typeof( byte ) == type )
			{
				ret = (byte)0;
			}
			else if ( typeof( sbyte ) == type )
			{
				ret = (sbyte)0;
			}
			else if ( typeof( string ) == type )
			{
				ret = string.Empty;
			}
				// SSP 5/7/02
				// Added support for System.Drawing.Color
				//
			else if ( typeof( System.Drawing.Color ) == type )
			{
				ret = Color.Empty;
			}

			return ret;
		}

		// ZS 3/3/04 - I moved this in Infragistics.Win.DataBindingUtils as it is common code for Grid, 
		// TabStripControl and CalendarInfo data binding.
		//		internal bool IsKnownType( Type type )
		//		{
		//			if ( typeof( System.DateTime ).Equals( type ) )
		//			{
		//			}
		//				// SSP 1/31/03
		//				// Added char and bool entries
		//				//
		//			else if ( typeof( char ) == type )
		//			{
		//			}
		//			else if ( typeof( bool ) == type )
		//			{
		//			}
		//				// SSP 1/24/02 UWG984
		//				// Typo. Use 'else if' instead of just 'if'
		//				//if ( typeof( System.DayOfWeek ).Equals( type ) )
		//			else if ( typeof( System.DayOfWeek ).Equals( type ) )
		//			{
		//			}
		//			else if ( typeof( int ) == type )
		//			{
		//			}
		//			else if ( typeof( uint ) == type )
		//			{
		//			}
		//			else if ( typeof( short ) == type )
		//			{
		//			}
		//			else if ( typeof( ushort ) == type )
		//			{
		//			}
		//			else if ( typeof( long ) == type )
		//			{
		//			}
		//			else if ( typeof( ulong ) == type )
		//			{
		//			}
		//			else if ( typeof( float ) == type )
		//			{
		//			}
		//			else if ( typeof( double ) == type )
		//			{
		//			}
		//			else if ( typeof( decimal ) == type )
		//			{
		//			}
		//			else if ( typeof( byte ) == type )
		//			{
		//			}
		//			else if ( typeof( sbyte ) == type )
		//			{
		//			}
		//			else if ( typeof( string ) == type )
		//			{
		//			}
		//				// SSP 5/7/02
		//				// Added support for System.Drawing.Color
		//				//
		//			else if ( typeof( System.Drawing.Color ) == type )
		//			{
		//			}
		//			else
		//				return false;
		//
		//			return true;
		//		}
			
		
		// ZS 3/3/04 - I moved this in Infragistics.Win.DataBindingUtils as it is common code for Grid, 
		// TabStripControl and CalendarInfo data binding.
		//		/// <summary>
		//		/// Gets the appropriate 
		//		/// </summary>
		//		/// <param name="bm"></param>
		//		internal PropertyDescriptorCollection GetItemProperties( BindingManagerBase bm )
		//		{
		//
		//			// Get the properties from the binding manager. If the bound list is
		//			// IBindingList, then this is what we want to use for constructing
		//			// columns. If the bound list is something other than a IBindingList
		//			// then we will have to get the type of elements contaned in the bound
		//			// IList.
		//			//
		//
		//
		//			CurrencyManager cm = bm as CurrencyManager;			
		//
		//			// bm should always a currency manager.
		//			//
		//			if ( null == cm )
		//				return bm.GetItemProperties( );;
		//			
		//			
		//			// Get the bound list from the currency manager.
		//			//
		//			IList list = cm.List;
		//			
		//			// If the bound list is a binding list, then use the default CurrencyManager
		//			// GetItemProperties to get the item properties.
		//			//
		//			if ( list is IBindingList )
		//				return bm.GetItemProperties( );
		//
		//			// If the bound list is not a IBindingList, then we want to use determine
		//			// the type of elements that the list contains, so we can use that to create
		//			// our own PropertyDescriptor objects for working with the bound list.
		//			//
		//
		//			PropertyDescriptorCollection props = cm.GetItemProperties( );
		//
		//			Type type = null;
		//
		//			// If the props collection has any property descriptors, then
		//			// get the ComponentType off it which should give us the type that
		//			// the properties returned by cm.GetItemProperties belong to.
		//			//
		//			if ( props.Count > 0 )
		//				type = props[0].ComponentType;
		//
		//			// If that did not return a type, then do this.
		//			// If the we are bound to an Array, try to get the element types by calling
		//			// GetElementType on the type of that array.
		//			//
		//			if ( null == type &&
		//				list is System.Array )
		//			{
		//				type = list.GetType( ).GetElementType( );
		//			}
		//
		//			// If that did not return a type, then do this.
		//			// If that didn't work, then try to get the first item and use it's
		//			// element type.
		//			//
		//			if ( null == type &&
		//				list.Count > 0 )
		//			{
		//				// Use the type of first non-null item.
		//				//
		//				foreach ( object item in list )
		//				{
		//					if ( null != item )
		//					{
		//						type = item.GetType( );
		//						break;
		//					}
		//				}
		//			}
		//
		//
		//
		//			// SSP 1/8/02 UWG912
		//			// Only use the type if it's a knwo type (like string, int, float,
		//			// DateTime). 
		//			// 
		//			//if ( null != type )
		//			if ( null != type && this.IsKnownType( type ) )
		//			{
		//				// Now we know the underlying type of elements in the bound IList
		//				// So create a property descriptor instance to handle that elemnt type.
		//				//
		//
		//				PropertyDescriptorCollection propertyDescColl = 
		//					new PropertyDescriptorCollection( 
		//					new PropertyDescriptor[] 
		//							{ new ValuePropertyDescriptor( type, this, "Value" ) }					
		//					);
		//
		//				return propertyDescColl;
		//			}
		//
		//
		//			// If all of above are unsuccessfull, then just return the default
		//			// properties that would be returned by the binding manager.
		//			//
		//			return props;
		//		}

		#region GetItemPropertiesHelper

		// SSP 6/8/05 BR03609
		// Added GetItemPropertiesHelper to fix that issue where if a parent binding manager
		// doesn't have at least one row in it and the associated IBindingList doesn't allow adding 
		// rows then the child binding manager can't be created (the BindingContext indexer throws
		// an exception).
		// 
		internal PropertyDescriptorCollection GetItemPropertiesHelper( )
		{
			if ( null != this.bindingManager )
				return DataBindingUtils.GetItemProperties( this.bindingManager, 
					new DataBindingUtils.ValuePropertyDescriptorCreator( this.GetItemPropertiesCreator ) );

			if ( null != this.ParentColumn )
			{
				UltraGridBand band = this;
				ITypedList typedList = null;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = new ArrayList( );
				List<PropertyDescriptor> list = new List<PropertyDescriptor>();

				while ( null != band )
				{
					typedList = band.List as ITypedList;
					if ( null != typedList )
						break;

					// SSP 10/31/05
					// This was a typo. It should have been band and not this.
					// 
					//PropertyDescriptor pd = null != this.ParentColumn ? this.ParentColumn.PropertyDescriptor : null;
					PropertyDescriptor pd = null != band.ParentColumn ? band.ParentColumn.PropertyDescriptor : null;

					if ( null == pd )
						break;

					list.Add( pd );
					band = band.ParentBand;
				}

				list.Reverse( );

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//PropertyDescriptor[] listAccessors = (PropertyDescriptor[])list.ToArray( typeof( PropertyDescriptor ) );
				PropertyDescriptor[] listAccessors = list.ToArray();

				if ( null != typedList && listAccessors.Length > 0 )
					return typedList.GetItemProperties( listAccessors );

                // MBS 10/31/08 - TFS9064
                // Added support for IEnumerable<> for child bands, which includes IList<>.  As per the notes
                // in the ImplementsGenericIEnumerable helper method, much of the code was adapted 
                // from the WPF databinding code, where we support this type
                PropertyDescriptor bandPD = this.ParentColumn != null ? this.ParentColumn.PropertyDescriptor : null;
                if (bandPD != null)
                {
                    Type listType;
                    if (DataBindingUtils.ImplementsGenericIEnumerable(bandPD.PropertyType, out listType))
                    {
                        if (DataBindingUtils.IsKnownType(listType))
                        {
                            return new PropertyDescriptorCollection(
                                new PropertyDescriptor[] { this.GetItemPropertiesCreator(listType, "Value") });
                        }

                        PropertyDescriptorCollection temp = TypeDescriptor.GetProperties(listType);
                        if (temp != null && temp.Count > 0)
                            return temp;
                    }
                }
			}

			return null;
        }
        #endregion //GetItemPropertiesHelper

        private PropertyDescriptor GetItemPropertiesCreator(Type type, string name)
		{
			return new ValuePropertyDescriptor(type, this, name);
		}

		internal bool InitColumns( Infragistics.Win.UltraWinGrid.UltraGridBand [] oldBands )
		{
			int boundColumnsCount = 0;

			if ( null != this.columns )
			{
				// save the number of bound columns 
				//
				boundColumnsCount = this.columns.BoundColumnsCount;
			}


			// SSP 6/8/05 BR03609
			// This is to fix that issue where if a parent binding manager doesn't have at least one row 
			// in it and the associated IBindingList doesn't allow adding rows then the child binding 
			// manager can't be created (the BindingContext indexer throws an exception).
			// Use the new GetItemPropertiesHelper helper method instead.
			// 
			// ------------------------------------------------------------------------------------------
			
			PropertyDescriptorCollection props = this.GetItemPropertiesHelper( );
			// ------------------------------------------------------------------------------------------

			if ( 
				// SSP 6/8/05 BR03609
				// This is to fix that issue where if a parent binding manager doesn't have at least one row 
				// in it and the associated IBindingList doesn't allow adding rows then the child binding 
				// manager can't be created (the BindingContext indexer throws an exception).
				// Commented out the following condition.
				// 
				//null == this.bindingManager || 

				null == props ||
				props.Count <= 0 
				// SSP 12/19/01
				// Use the newly created method GetItemProperties to get the properties.
				//
				//null == this.bindingManager.GetItemProperties() ||
				//this.bindingManager.GetItemProperties().Count < 1 
				)
			{
				
				if ( 0 == boundColumnsCount )
					return false;
			}

			ColumnsCollection oldColumns = this.columns;

			if ( null != this.columns )
			{
				// remove the prop change notifications from the existing columns collection
				//
				this.columns.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// null out the columns collection
				//
				this.columns = null;
			}

			bool columnsHaveChanged = false;

			// SSP 6/8/05 BR03609
			// This is to fix that issue where if a parent binding manager doesn't have at least one row 
			// in it and the associated IBindingList doesn't allow adding rows then the child binding 
			// manager can't be created (the BindingContext indexer throws an exception).
			// Use the new GetItemPropertiesHelper helper method instead.
			// 
			
			UltraGridColumn column;

			// SSP 12/19/01
			// We are getting the props above.
			//
			//PropertyDescriptorCollection props = this.bindingManager.GetItemProperties();

			if ( null != oldColumns )
			{
				// SSP 3/12/07 BR19515
				// Check if props is null.
				// 
				//columnsHaveChanged = (props.Count != boundColumnsCount);
				columnsHaveChanged = ( null != props ? props.Count : 0 ) != boundColumnsCount;

				if ( !columnsHaveChanged )
				{
					// loop over the old bound columns looking for any column
					// that doesn't match positionally with the new properties
					//
					for ( int i = 0; i < boundColumnsCount; i++ )
					{
						// AS 1/8/03 fxcop
						// Explicitly specify a cultureinfo
						//
						//if ( 0 != oldColumns[i].PropertyDescriptor.Name.CompareTo( props[i].Name ) )
						// SSP 5/25/04 - Extract Data Structure Related
						// If the PropertyDescriptor off the old column is null then use the column's key.
						//
						//if ( 0 != string.Compare(oldColumns[i].PropertyDescriptor.Name, props[i].Name, false, System.Globalization.CultureInfo.CurrentCulture ) )
						// MD 8/15/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//string columnPropName = null != oldColumns[i].PropertyDescriptor
						//    ? oldColumns[i].PropertyDescriptor.Name	: oldColumns[i].Key;
						UltraGridColumn oldColumn = oldColumns[ i ];

						string columnPropName = null != oldColumn.PropertyDescriptor
							? oldColumn.PropertyDescriptor.Name 
							: oldColumn.Key;

						if ( 0 != string.Compare( columnPropName, props[i].Name, false, System.Globalization.CultureInfo.CurrentCulture ) )
						{
							columnsHaveChanged = true;
							break;
						}
					}
				}

				// if none of the bound columns have changed then just
				// restore the original collection and re-init the property
				// descriptors
				//
				if ( !columnsHaveChanged )
				{
					this.columns = oldColumns;

					// remove the prop change notifications from the existing columns collection
					//
					this.columns.SubObjectPropChanged += this.SubObjectPropChangeHandler;

					for ( int i = 0; i < boundColumnsCount; i++ )
						this.columns[i].InitPropertyDescriptor( props[i], oldBands );


					// SSP 11/2/01 UWG643
					// Clear the rows regardless of whether columns have changed or not
					//
					// SSP 11/16/07 BR17904
					// Now we are maintaining old rows so there is no need to clear the active row. 
					// Also don't scroll to the beginning. Both of these behaviors are undesirable.
					// 
					// ------------------------------------------------------------------------------
					
					this.layout.RowScrollRegions.DirtyAllVisibleRows( );
					// ------------------------------------------------------------------------------

					// SSP 3/17/04 - Virtual Binding Related
					// Instead of clearing the rows, simply dirty it. Why clear it anyways ?
					//
					//this.layout.Rows.InternalClearHelper();
					this.layout.Rows.DirtyRows( );
						
					// SSP 12/14/01
					// Rebind to list.
					//
					this.hookIntoList = true;
					this.layout.Rows.BindToList( );


					return false;
				}
			}

			// SSP 3/12/07 BR19515
			// Check if props is null.
			// 
			if ( null != props )
			{
				foreach ( PropertyDescriptor prop in props )
				{
					column = null;

					if ( null != oldColumns )
					{
						// loop over the old bound columns looking for a column
						// that matches (so we can reuse it)
						//
						// SSP 6/14/04
						// Don't just match bound columns. Match unbound columns as well. This is useful when
						// the data source is not available at design time so you add a bunch of unbound columns
						// and design them and then you set the data source at runtime. In such a situation
						// you want to match up the unbound columns with the columns in the data source.
						//
						//for ( int i = 0; i < boundColumnsCount; i++ )
						for ( int i = 0; i < oldColumns.Count; i++ )
						{
							// AS 1/8/03 fxcop
							// Explicitly specify a cultureinfo
							//
							//if ( 0 == oldColumns[i].PropertyDescriptor.Name.CompareTo( prop.Name ) )
							// SSP 5/25/04 - Extract Data Structure Related
							// If the PropertyDescriptor off the old column is null then use the column's key.
							//
							//if ( 0 != string.Compare(oldColumns[i].PropertyDescriptor.Name, props[i].Name, false, System.Globalization.CultureInfo.CurrentCulture ) )
							// MD 8/15/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//string columnPropName = null != oldColumns[i].PropertyDescriptor
							//    ? oldColumns[i].PropertyDescriptor.Name : oldColumns[i].Key;
							UltraGridColumn oldColumn = oldColumns[ i ];

							string columnPropName = null != oldColumn.PropertyDescriptor
								? oldColumn.PropertyDescriptor.Name 
								: oldColumn.Key;

							//if ( 0 == string.Compare(oldColumns[i].PropertyDescriptor.Name, prop.Name, false, System.Globalization.CultureInfo.CurrentCulture ) )
							if ( 0 == string.Compare( columnPropName, prop.Name, false, System.Globalization.CultureInfo.CurrentCulture ) )
							{
								// MD 8/15/07 - 7.3 Performance
								// Prevent calling expensive getters multiple times
								//column = oldColumns[i];
								column = oldColumn;

								break;
							}
						}
					}

					// if the column didn't exit in the previous collection
					// then create a new one now
					//
					if ( null == column )
					{
						column = new UltraGridColumn( this );

						// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
						// 
						if ( null != this.Layout && NewColumnLoadStyle.Hide == this.Layout.NewColumnLoadStyleInEffect )
							column.Hidden = true;
					}

					// set the column's property descriptor
					//
					column.InitPropertyDescriptor( prop, oldBands );

					// append the column to the new collection
					//
					this.Columns.InternalAdd( column );
				}
			}
			// SSP 6/8/05 BR03609
			// 
			//}

			if ( null != oldColumns )
			{
				// SSP 3/18/04 - Virtual Binding Related
				// If a chaptered column gets removed, also remove the associated band.
				//
				// ----------------------------------------------------------------------
				if ( null != this.Layout )
				{
					for ( int i = 0; i < oldColumns.Count; i++ )
					{
						// MD 8/15/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( null != oldColumns[i] && oldColumns[i].IsChaptered )
						//{
						//    if ( ! this.Columns.Contains( oldColumns[i] ) )
						//    {
						//        UltraGridBand childBand = this.Layout.Bands.GetChildBand( oldColumns[i] );
						UltraGridColumn oldColumn = oldColumns[ i ];

						if ( null != oldColumn && oldColumn.IsChaptered )
						{
							if ( !this.Columns.Contains( oldColumn ) )
							{
								UltraGridBand childBand = this.Layout.SortedBands.GetChildBand( oldColumn );

								if ( null != childBand )
									this.Layout.SortedBands.InternalRemove( childBand );
							}
						}
					}
				}
				// ----------------------------------------------------------------------

				// Append all unbound columns to the columns collection.
				// Note: since the unbound columns always follow the
				// bound columns we can start after the first bound
				// column.
				//
				// SSP 6/17/04 UWG3359
				// Why Count - 1 ? That leaves off the last column.
				//
				//for ( int i = boundColumnsCount; i < oldColumns.Count - 1; i++ )
				for ( int i = boundColumnsCount; i < oldColumns.Count; i++ )
				{
					// append the unbound columns to the new collection
					// if its key doesn't conflict
					//
					// MD 8/15/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( !this.Columns.Exists( oldColumns[i].Key ) )
					//    this.Columns.InternalAdd( oldColumns[i] );
					UltraGridColumn oldColumn = oldColumns[ i ];

					if ( !this.Columns.Exists( oldColumn.Key ) )
						this.Columns.InternalAdd( oldColumn );
				}
			}

			//RobA 9/14/01 UWG279
			//recreate the orderedColumns
			//
			this.orderedColumnsDirty = true;

			//RobA UWG464 10/12/01
			// SSP 7/29/04
			// Use the BumpColumnsVersion instead of directly bumping the version number.
			//
			//++this.columnsVersion;
			this.BumpColumnsVersion( );

			// JJd 10/09/01 - UWG511
			// If the columns have changed we need to clear all
			// row references
			//
			// SSP 11/2/01 UWG643
			// Clear the rows regardless of whether columns have changed or not
			//
			//if ( columnsHaveChanged )
			//{
			// SSP 10/01/02 UWG1720
			// We don't want to clear the active row on the if this is a print layout.
			//
			//if ( this.layout.Grid != null )
			// SSP 5/7/03 - Exporting Functionality
			// Same applies to export layout.
			//
			//if ( this.layout.Grid != null && !this.layout.IsPrintLayout )
			// SSP 11/16/07 BR17904
			// There is no reason to clear active row. We've implemented logic to reconstruct
			// cell collection as well as deactivate the active row if it becomes invalid.
			// So we don't need to do it here explicitly. Not only that, doing so can be
			// undesirable.
			// 
			//if ( this.layout.Grid != null && !this.layout.IsPrintLayout && ! this.layout.IsExportLayout )
			//	this.layout.Grid.ClearActiveRow();

			// SSP 10/10/01 UWG343
			// When columns have changed, we need to also clear the
			// sorted columns collection because we don't want any
			// invalid columns (groupby or non-groupby) in sorted columns
			//
			// SSP 11/2/01 UWG643
			//if (  this.HasSortedColumns )
			if ( columnsHaveChanged && this.HasSortedColumns )
			{
				// JJD 10/10/01
				// Instead of clearing the sorted columns just
				// verify that the columns exist
				//
				this.SortedColumns.VerifyColumns( );
			}

			// SSP 11/16/07 BR17904
			// Now we are maintaining old rows so there is no need to scroll to the 
			// beginning.
			// 
			//this.layout.RowScrollRegions.DirtyAllVisibleRows( true );
			this.layout.RowScrollRegions.DirtyAllVisibleRows( );

			// SSP 9/5/03 UWG2634
			// Check if the rows collection has been allocated before accessing the Rows property.
			//
			if ( this.layout.IsRowsCollectionAllocated )
			{
				// SSP 3/17/04 - Virtual Binding Related
				// Instead of clearing the rows, simply dirty it. Why clear it anyways ?
				//
				//this.layout.Rows.InternalClearHelper();
				this.layout.Rows.DirtyRows( );
			}

			// SSP 12/14/01
			// If a column is added or removed, this is reasonable to
			// do.
			//---------------------------------------------
			this.hookIntoList = true;
			// SSP 9/5/03 UWG2634
			// Check if the rows collection has been allocated before accessing the Rows property.
			//
			if ( this.layout.IsRowsCollectionAllocated )
			{
				this.layout.Rows.DirtyRows( );
				this.layout.Rows.BindToList( );
			}
			this.layout.ColScrollRegions.DirtyMetrics( );

			// SSP 4/10/03 - Row Layout Functionality
			//
			this.DirtyRowLayoutCachedInfo( );
			//---------------------------------------------
			//}

			return true;			
		}

		


		internal void ColumnAddedRemovedChangedHandler( System.ComponentModel.ListChangedEventArgs e )
		{
			// SSP 6/8/05 BR03609
			// This is to fix that issue where if a parent binding manager doesn't have at least one row 
			// in it and the associated IBindingList doesn't allow adding rows then the child binding 
			// manager can't be created (the BindingContext indexer throws an exception).
			// Use the new GetItemPropertiesHelper helper method instead.
			// 
			//PropertyDescriptorCollection props = this.bindingManager.GetItemProperties();
			PropertyDescriptorCollection props = this.GetItemPropertiesHelper( );

			// SSP 1/24/05 BR00269
			// If the key of the band has changed then reget the key.
			//
			this.InitBandKey( );			

			bool columnsChanged = false;

			// SSP 3/25/04
			// Reset the interned keys because PropertyDescriptor instances could all of
			// a sudden change the Name and since the KeyedSubObjectBase caches the
			// interned strings, we need to clear that cache.
			//
			for ( int i = 0; i < this.Columns.Count; i++ )
				this.Columns[ i ].InternalResetInternedKey( );

			// Add any columns that exist in property descriptor collections and
			// not in our columns collection.
			//
			// SSP 3/12/07 BR19515
			// Check if props is null.
			// 
			if ( null != props )
			{
				for ( int i = 0; i < props.Count; i++ )
				{
					PropertyDescriptor propDesc = props[i];

					if ( !this.Columns.Exists( propDesc.Name ) )
					{
						// if the column didn't exit in the previous collection
						// then create a new one now
						//
						UltraGridColumn column = new UltraGridColumn( this );

						// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
						// 
						if ( null != this.Layout && NewColumnLoadStyle.Hide == this.Layout.NewColumnLoadStyleInEffect )
							column.Hidden = true;

						// set the column's property descriptor
						//
						column.InitPropertyDescriptor( propDesc, null );

						// append the column to the new collection
						//
						// SSP 12/11/02	UWG1813
						// Insert before any unbound columns.
						//
						//this.Columns.InternalInsert( Math.Min( i, this.columns.Count ), column );
						this.Columns.InternalInsert( Math.Min( i, this.columns.BoundColumnsCount ), column );

						columnsChanged = true;
					}
					else
					{
						// If there exists a column associated with propDesc, then
						// check if it's an unbound column. If it is, make it bound 
						// by initializing the column with propDesc.
						//
						UltraGridColumn column = this.Columns[propDesc.Name];

						// SSP 3/16/04 - Virtual Binding
						// Always initialize the property descriptor regardless of whether the column
						// was already bound. Also there is no harm in setting columnsChanged to true.
						//
						// ------------------------------------------------------------------------------
						
						column.InitPropertyDescriptor( propDesc, null );
						columnsChanged = true;
						// ------------------------------------------------------------------------------
					}
				}
			}


			// Remove any columns that exist in the columns collection but do
			// not exist in the property descriptor collection.
			//
			for ( int i = 0; i < this.Columns.Count; i++ )
			{
				UltraGridColumn column = this.Columns[i];

				// Skip all the unbound columns.
				//
				if ( !column.IsBound )
					continue;

				// SSP 3/12/07 BR19515
				// Check if props is null.
				// 
				//PropertyDescriptor propDesc = props.Find( column.Key, true );
				PropertyDescriptor propDesc = null != props ? props.Find( column.Key, true ) : null;

				if ( null == propDesc )
				{
					// If no property descriptor was found for this bound column
					// then remove it from the columns collection.
					//
					// SSP 12/11/02 UWG1813
					// Use InternalRemoveColumn rather than Remove because Remove doesn't allow
					// removing bound columns.
					//
					//this.Columns.Remove( column );
					// SSP 3/8/04 - Virtual Binding
					// If the column is a chaptered column, then delete the associated band from
					// the bands collection as well.
					//
					// ----------------------------------------------------------------------------
					if ( column.IsChaptered )
					{
						UltraGridBand chaptedColumnBand = null != this.Layout && null != this.Layout.Bands
							? this.Layout.Bands.FindBand( column ) : null;

						if ( null != chaptedColumnBand )
						{
							this.Layout.Bands.InternalRemove( chaptedColumnBand );

							// SSP 11/7/06 BR17319
							// 
							this.Layout.BumpScrollableRowCountVersion( );
						}
					}
					// ----------------------------------------------------------------------------

					this.Columns.InternalRemoveColumn( column );

					// SSP 5/18/04 UWG3310
					// Decrement i by as we have just removed a column at that position.
					//
					i--;

					columnsChanged = true;
				}
			}


			// Mark the ordered columns dirty so that ordered columns collection
			// takes into account the column that was added or removed.
			//
			this.orderedColumnsDirty = true;
			// SSP 7/29/04
			// Use the BumpColumnsVersion instead of directly bumping the version number.
			//
			//this.columnsVersion++;
			this.BumpColumnsVersion( );
            
			// If a column was removed, then also make sure that that columns
			// is not in the sorted columns collection.
			//
			if ( columnsChanged )
			{
                if(this.HasSortedColumns)
				    this.SortedColumns.VerifyColumns( );

                // MBS 4/16/08 - RowEditTemplate NA2008 V2
                // Make sure that the template will regenerate its property descriptors
                // for DataBinding the next time they are needed
                if (this.RowEditTemplate != null)
                    this.RowEditTemplate.OnBandColumnsChanged();
			}

			this.layout.SynchronizedColListsDirty = true;
			this.layout.RowScrollRegions.DirtyAllVisibleRows( );			
			this.layout.ColScrollRegions.DirtyMetrics( );

			// SSP 4/10/03 - Row Layout Functionality
			//
			this.DirtyRowLayoutCachedInfo( );
		}


		internal int ColumnsVersion
		{
			get
			{
				return this.columnsVersion;
			}
		}

		internal void BumpColumnsVersion()
		{
			this.columnsVersion++;

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			this.orderedColumnsDirty = true;
			this.DirtyTabOrderedColumns( );

			// SSP 7/21/06 BR14281
			// Clear the cached CardCellAreaWidth.
			// 
			this.ClearDrawCache( );
		}
		
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.UltraWinGrid.ColumnsCollection"/> of UltraGridColumns that make up the band. This property is read-only.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>Columns</b> property returns a collection that contains all the columns of the band.
		/// The returned collection contains <see cref="UltraGridColumn"/> instances. You can add 
		/// unbound columns using the <see cref="ColumnsCollection.Add()"/> method.
		/// </p>
		/// <see cref="ColumnsCollection"/>
		/// </remarks>
		[Editor(typeof(ColumnUITypeEditor), typeof(UITypeEditor))]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ] 
		[LocalizedDescription("LD_UltraGridBand_P_Columns")]
		[LocalizedCategory("LC_Collection")]
		public ColumnsCollection Columns
		{
			get
			{				
				if ( null == this.columns )
				{
					this.columns = new ColumnsCollection( this );

					// hook up the prop change notifications
					//
					this.columns.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.columns;
			}
		}

		internal bool HasColumnsBeenAllocated
		{
			get
			{
				return null != this.columns;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeColumns() 
		{
			if ( null != this.columns )
			{
				// SSP 5/25/04
				// We need to serialize the bands and columns when the data structure has
				// been extracted out of a data source.
				//
				// ------------------------------------------------------------------------
				if ( this.columns.Count > 0 && null != this.Layout 
					&& this.Layout.AlwaysSerializeBandsAndColumns )
					return true;
				// ------------------------------------------------------------------------

				foreach ( UltraGridColumn col in this.columns )
				{
					if ( col.ShouldSerialize() )
						return true;
				}
			}
            
			return false;
		}
 
		/// <summary>
		/// Resets the Columns collection for the band.
		/// </summary>
		public void ResetColumns() 
		{
			if ( null != this.columns )
			{
				// remove unbound columns
				//
				this.columns.ClearUnbound();

				// call Reset on each bound column
				//
				foreach ( UltraGridColumn col in this.columns )
				{
					col.Reset();

					// SSP 10/27/03 UWG2724
					// We need to reset the visible position of the column.
					//
					if ( col.IsHeaderAllocated )
					{
						col.Header.InternalResetVisiblePos( );
						this.orderedColumnsDirty = true;
					}
				}
			}
		}

		
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.UltraWinGrid.GroupsCollection"/> of UltraGridGroup objects that are present in the band. This property is read-only.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.GroupsCollection"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.GroupHeadersVisible"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.LevelCount"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Columns"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.Group"/>
		/// </remarks>
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ] 
		[LocalizedDescription("LD_UltraGridBand_P_Groups")]
		[LocalizedCategory("LC_Collection")]
		public GroupsCollection Groups
		{
			get
			{
				if ( null == this.groups )
				{
					this.groups = new GroupsCollection( this );

					// hook up the prop change notifications
					//
					this.groups.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.groups;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeGroups() 
		{
			return ( null != this.groups &&
				this.groups.Count > 0 );
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeSortedColumns() 
		{
			// SSP 10/26/01 UWG589
			// We also need to check for the ColumnSerializedIds
			// in the sorted columns not just the count because
			// if the serialization is making a second pass, this
			// will not serialize the sorted columns even though
			// they need to be because they have SerializedColumnIDs.
			// 
			//return ( this.sortedColumns != null &&
			//		   this.sortedColumns.Count > 0);
			return ( this.sortedColumns != null && 
				( this.sortedColumns.Count > 0 ||
				this.sortedColumns.HasSerializedColumnIDs ) );
		}
 
		/// <summary>
		/// Resets the Groups collection for the band.
		/// </summary>
		public void ResetGroups() 
		{
			if ( null != this.groups )
			{
				// clear all groups
				//
				this.groups.Clear();
			}
		}
 
		/// <summary>
		/// Returns or sets the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride"/> obejct that specifies the formatting and behavior of the band.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>Override</b> object contains properties for controlling the appearance and behavior of
		/// the band and objects (like rows, cells, columns, headers etc...) associated with the band. 
		/// The <see cref="UltraGridLayout"/> object also exposes <see cref="UltraGridLayout.Override"/> property
		/// which controls the behavior and appearance of objects of the entire grid, instead of just a single 
		/// band.
		/// </p>
		/// <seealso cref="UltraGridLayout.Override"/>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ] 
		[ LocalizedDescription("LD_UltraGridBand_P_Override")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.UltraGridOverride Override
		{
			get
			{
				if ( null == this.overrideObj )
				{
					this.overrideObj = new Infragistics.Win.UltraWinGrid.UltraGridOverride( this.layout );
            
					// hook up the prop change notifications
					//
					this.overrideObj.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.overrideObj;
			}

			set
			{
				if ( value != this.overrideObj )
				{
					// unhook up the old prop change notifications
					//
					if ( null != this.overrideObj )
						this.overrideObj.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

					this.overrideObj = value;

					// hook up the new prop change notifications
					//
					if ( null != this.overrideObj )
						this.overrideObj.SubObjectPropChanged += this.SubObjectPropChangeHandler;
    		    	
					this.NotifyPropChange( PropertyIds.Override, null );
				}
			}
		}

		/// <summary>
		/// Returns true if an Override object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasOverride
		{
			get
			{
				return null != this.overrideObj;
			}
		}

		/// <summary>
		/// Returns true if a CardSettings object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCardSettings
		{
			get
			{
				return null != this.cardSettings;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeOverride() 
		{
			return ( this.HasOverride && 
				this.overrideObj.ShouldSerialize() );
		}

		/// <summary>
		/// Resets the Override
		/// </summary>
		public void ResetOverride() 
		{
			if ( this.HasOverride )
				this.overrideObj.Reset();
		}

		/// <summary>
		/// Returns a <see cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings"/> object that contains information specifying how the band will appear when in card view mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// To enable the Card-View functionality set the Band's <see cref="UltraGridBand.CardView"/> property.
		/// The <see cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings"/> object exposes various properties that let you control the Card-View related aspects.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.CardView"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridCardSettings"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.UseRowLayout"/>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ] 
		[ LocalizedDescription("LD_UltraGridBand_P_CardSettings")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.UltraGridCardSettings CardSettings
		{
			get
			{
				if ( null == this.cardSettings )
				{
					this.cardSettings = new Infragistics.Win.UltraWinGrid.UltraGridCardSettings( this );
            
					// hook up the prop change notifications
					//
					this.cardSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.cardSettings;
			}
		}

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		//
		internal CardScrollbars CardScrollbarsResolved
		{
			get
			{
                // MRS NAS v8.2 - CardView Printing
                UltraGridLayout layout = this.Layout;
                if (layout != null &&
                    (layout.IsPrintLayout || layout.IsExportLayout))
                {
                    return CardScrollbars.None;
                }

				return null != this.cardSettings 
					? this.cardSettings.CardScrollbars 
					: CardScrollbars.Horizontal;
			}
		}

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		//
		internal int CardAreaHorizontalScrollbarHeightResolved
		{
			get
			{
				return CardScrollbars.Horizontal == this.CardScrollbarsResolved 
					? SystemInformation.HorizontalScrollBarHeight 
					: 0;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeCardSettings() 
		{
			return ( this.HasCardSettings && 
				this.cardSettings.ShouldSerialize() );
		}

		/// <summary>
		/// Resets the card settings 
		/// </summary>
		public void ResetCardSettings() 
		{
			if ( this.HasCardSettings )
				this.cardSettings.Reset();
		}


		internal string AddButtonCaptionResolved
		{
			get
			{	
				//RobA 8/24/01 We want to try and first use the key by calling 
				//AddButtonCaption.  If that is null then use the header's caption
				
				//if ( null != this.addButtonCaption &&
				//	this.addButtonCaption.Length > 0 )
				//	return this.addButtonCaption;

				string caption = this.AddButtonCaption;
				if ( caption != null &&
					caption.Length > 0 )
					return caption;

				// JJD 9/20/01
				// Use the key if we have one
				//
				if ( this.Key != null &&
					this.Key.Length > 0 )
					return this.Key;

				HeaderBase header = this.OrderedHeaders.FirstVisibleHeader;

				if ( null != header )
					return header.Caption;

				return string.Empty;
			}
		}

		// SSP 5/10/04 - Optimizations
		// Added HasAlternateAppearances property. We don't want to go through the
		// visible index calculations to figure out if a row is an alternate row 
		// or not if there are no alternate row appearances.
		//
		internal bool HasAlternateAppearances
		{
			get
			{
				return null != this.overrideObj && this.overrideObj.HasRowAlternateAppearance
					|| this.layout.HasOverride && this.layout.Override.HasRowAlternateAppearance;
			}
		}

		internal AllowAddNew AllowAddNewResolved
		{
			get
			{
				// SSP 1/12/04 UWG2886
				// If the binding manager is null, then return No for the AllowAddNew.
				//
				// --------------------------------------------------------------------
				// SSP 4/22/05 BR03548
				// Also retrun No if it's an UltraDropDown or UltraCombo.
				//
				//if ( null == this.bindingManager )
				// SSP 5/5/08 BR32208
				// With the fix to BR11351, now we could have bands without binding managers in 
				// recursive data source situations. We need to still allow adding in that case.
				// 
				//if ( null == this.bindingManager || ! ( this.Layout.Grid is UltraGrid ) )
                // MRS 8/8/2008 - BR35288
                // Sandip took out the check here for bindingManager == null (see note above for 
                // BR32208). This made it possible for AllowAddNewResolved to something other 
                // than No when the grid has no DataSource at all - which it should never do.
                //
                //if ( !( this.Layout.Grid is UltraGrid ) )
                if (!(this.Layout.Grid is UltraGrid) || this.Layout.Grid.DataSource == null)
					return AllowAddNew.No;
				// --------------------------------------------------------------------

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowAddNew() )
					return this.overrideObj.AllowAddNew;

				return this.layout.AllowAddNewDefault;
			}
		}

		

		internal bool AddNewBoxButtonEnabledResolved
		{
			get
			{
				if ( AllowAddNew.No == this.AllowAddNewResolved )
					return false;

				// JJD 10/05/01 - UWG353
				// If this has group by columns return false
				//
				if ( this.HasGroupBySortColumns )
					return false;

				// JJD 1/22/02
				// We can't do some of the checks below in design mode
				//
				if ( this.Layout == null		||
					this.Layout.Grid == null	||
					this.Layout.Grid.DesignMode )
					return true;

				if ( null == this.ParentBand )
				{
					if ( this.HasGroupBySortColumns )
					{
						UltraGridRow activeRow = this.Layout.ActiveRow;

						// We have to have an active row
						//
						if ( null == activeRow )
							return false;
                        
						if ( this == activeRow.Band &&
							( activeRow is UltraGridGroupByRow ) )
						{
							UltraGridGroupByRow gr = (UltraGridGroupByRow)activeRow;

							// the group by row has to be the bottom most level group by row
							// in the band
							if ( this.SortedColumns.LastGroupByColIndex != 
								this.SortedColumns.IndexOf( gr.Column.Key ) )
								return false;
						}

					}
					
				}
				else
				{
					Infragistics.Win.UltraWinGrid.UltraGridRow activeRow = this.Layout.ActiveRow;
                    
					// We have to have an active row
					//
					if ( null == activeRow )
						return false;

					// JJD 10/05/01
					// If this is the active row's band don't return false
					//
					if ( !activeRow.Band.IsDescendantOfBand( this ) &&
						this.ParentBand != activeRow.Band			&&
						this != activeRow.Band 
						// SSP 12/8/03 UWG2747
						// If the active row is in a sibling band, then we do have enough add-row
						// context and thus don't retrun false.
						//
						&& this.ParentBand != activeRow.Band.ParentBand )
						return false;

					if ( this.ParentBand == activeRow.Band &&
						this.ParentBand.HasGroupBySortColumns &&
						( activeRow is UltraGridGroupByRow ) )
					{
						return false;
					}					 
						 

					if ( this.HasSortedColumns && ( activeRow is UltraGridGroupByRow ) )
					{
						UltraGridGroupByRow gr = (UltraGridGroupByRow)activeRow;

						// the group by row has to be the bottom most level group by row
						// in the band
						if ( this.SortedColumns.LastGroupByColIndex != 
							this.SortedColumns.IndexOf( gr.Column.Key ) )
							return false;
					}
				}

				// JAS 2005 v2 XSD Support
				//
				// SSP 8/18/05 P856
				// When SyncWithCurrencyManager is set to false the ActiveRow is not syncrhonized
				// with the Position. In that case when the band.AddNew is called, the row should
				// be added to the row collection associated with the UltraGrid's ActiveRow, not 
				// the binding maanger's Position.
				// 
				//RowsCollection parentRows = this.GetCurrentParentRows( true );
				RowsCollection parentRows = this.GetCurrentParentRowsForAddNew( );

				if( parentRows != null && ! parentRows.ContainsLessThanMaxRows )
					return false;


				// SSP 11/25/02 UWG1820
				// Workaround for situations like these is to call Refresh on the
				// currency manager. Disabling adding of child rows if there is
				// only one parent row is not necessary any more.
				// Commented below code out as it's not necessary any more since 
				// fixing UWG1820 solves that problem.
				//
				


				return true;
			}
		}
		

		internal AllowColMoving AllowColMovingResolved
		{
			get
			{
				// MRS 3/14/05 - Start allowing this if it's explicitly set. 
				// Moved this down. 
				//				// SSP 3/17/03 - Row Layout Functionality
				//				// Disable column moving in row layout mode.
				//				//
				//				if ( this.UseRowLayoutResolved )
				//					return AllowColMoving.NotAllowed;

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowColMoving() )
					return this.overrideObj.AllowColMoving;

				// MRS 3/14/05 - Moved down from above. 
				// Start allowing this if it's explicitly set. 
				if ( this.UseRowLayoutResolved )
				{
					if (this.layout.HasOverride &&
						this.layout.Override.ShouldSerializeAllowColMoving())
					{
						return this.layout.Override.AllowColMoving;
					}
					else
						return AllowColMoving.NotAllowed;
				}

				return this.layout.AllowColMovingDefault;
			}
		}
		
		internal AllowColSizing AllowColSizingResolved
		{
			get
			{
				// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support HeaderClickAction in Combo and DropDown
				//				if ( !( this.Layout.Grid is UltraGrid ) )
				//					return AllowColSizing.None;

				AllowColSizing sizing;

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowColSizing() )
				{
					sizing =  this.overrideObj.AllowColSizing;
				}
				else
				{
					sizing = this.layout.AllowColSizingDefault;
				}

				// JJD 12/13/01
				// Don't support synchronizewd col sizing for
				// this band if CardView or AutoFitColumns is true
				//
				if ( sizing == AllowColSizing.Synchronized ||
					sizing == AllowColSizing.Default )
				{
					if ( this.cardView 

						// SSP 8/13/05 BR04633 BR04666
						// This is to make column synchronization work when auto-fit functionality is turned on.
						// If AllowColSizing is explicitly set to Syncrhonized then honor that when auto-fit 
						// columns is enabled. Commented out the following condition. Instead added code in the
						// layout's AllowColSizingDefault to resolve to Free instead of Synchronized when 
						// auto-fit is enabled.
						// 
						//|| this.layout.AutoFitAllColumns 

						// SSP 2/19/03 - Row Layout Functionality
						// Column synchronization is disabled in row layout functionality as well.
						//
						|| this.UseRowLayoutResolved )
						sizing = AllowColSizing.Free;
				}

				return sizing;
			}
		}
		internal AllowColSwapping AllowColSwappingResolved
		{
			get
			{
				// SSP 3/17/03 - Row Layout Functionality
				// Disable col swapping in the row layout mode.
				//
				// SSP 8/2/05 - NAS 5.3 Column Swapping in Row-Layout
				// 
				//if ( this.UseRowLayoutResolved )
				//	return AllowColSwapping.NotAllowed;

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowColSwapping() )
					return this.overrideObj.AllowColSwapping;

				return this.layout.AllowColSwappingDefault;
			}
		}
		internal DefaultableBoolean AllowDeleteResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowDelete() )
					return this.overrideObj.AllowDelete;

				return this.layout.AllowDeleteDefault;
			}
		}
		internal AllowGroupMoving AllowGroupMovingResolved
		{
			get
			{			
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowGroupMoving() )
					return this.overrideObj.AllowGroupMoving;

                // MRS 2/10/2009 - TFS13728            
                // In a GroupLayout, don't allow group moving by default. 
                if (this.RowLayoutStyle == RowLayoutStyle.GroupLayout)
                {
                    if (this.Layout.HasOverride &&
                        this.Layout.Override.ShouldSerializeAllowGroupMoving())
                    {
                        return this.Layout.Override.AllowGroupMoving;
                    }

                    return AllowGroupMoving.NotAllowed;
                }

				return this.layout.AllowGroupMovingDefault;
			}
		}
		internal AllowGroupSwapping AllowGroupSwappingResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowGroupSwapping() )
					return this.overrideObj.AllowGroupSwapping;

				return this.layout.AllowGroupSwappingDefault;
			}
		}

		internal DefaultableBoolean AllowGroupByResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowGroupBy() )
					return this.overrideObj.AllowGroupBy;

				return this.layout.AllowGroupByDefault;
			}
		}


		// SSP 6/30/05 - NAS 5.3 Column Chooser
		// Added HiddenWhenGroupByResolved on the column instead.
		// 
				

		internal int GroupByRowPaddingResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeGroupByRowPadding() )
					return this.overrideObj.GroupByRowPadding;

				return this.layout.GroupByRowPaddingDefault;
			}
		}


		/// <summary>
		/// Removes all the Group-By columns, ungrouping the rows in the band.
		/// </summary>
		/// <remarks>
		/// <p class="body">If the band is in Group-By mode, the rows in the band will appear grouped according to the columns added to the SortedColumns collection as group-by columns. The user can add columns to this collection through the grid's UI by dragging column headers into the Group-By box. As columns are added, the rows in the band are dynamically re-grouped. By clearing the contents of the collection, all rows are ungrouped and the GroupBy box is cleared.</p>
		/// </remarks>
		public void ClearGroupByColumns( )
		{
			if ( this.HasSortedColumns )
				this.SortedColumns.ClearGroupByColumns( );
		}

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Not needed anymore.
		//
		

		internal string GroupByRowDescriptionMaskResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.Override.ShouldSerializeGroupByRowDescriptionMask( ) )
					return this.Override.GroupByRowDescriptionMask;

				return this.Layout.GroupByRowDescriptionMaskDefault;
			}
		}

		internal DefaultableBoolean AllowUpdateResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowUpdate() )
					return this.overrideObj.AllowUpdate;

				return this.layout.AllowUpdateDefault;
			}
		}
		
		internal Infragistics.Win.UIElementBorderStyle BorderStyleRowResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleRow, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleRow : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleRow;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleRow,
						StyleUtils.Role.Row, style, UIElementBorderStyle.Solid );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// If the summary footer is displayed in the group-by row then the border style
		// is None. Changed from property to method so we can pass the summary display
		// area context to it.
		//
		//internal Infragistics.Win.UIElementBorderStyle BorderStyleSummaryFooterResolved
		internal Infragistics.Win.UIElementBorderStyle GetBorderStyleSummaryFooterResolved( SummaryDisplayAreaContext context )
		{
			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// If the summary footer is in group-by row then it shouldn't draw its borders and
			// background.
			//
			if ( null != context && context.IsGroupByRowSummaries )
				return UIElementBorderStyle.None;

			// SSP 3/21/06 - App Styling
			// Get the border style from the role.
			// 
			
			object val;
			if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleSummaryFooter, out val ) )
			{
				UIElementBorderStyle style = null != this.overrideObj 
					? this.overrideObj.BorderStyleSummaryFooter : UIElementBorderStyle.Default;

				if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
					style = this.layout.Override.BorderStyleSummaryFooter;

				val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleSummaryFooter,
					StyleUtils.Role.SummaryFooter, style, UIElementBorderStyle.Solid );
			}

			return (UIElementBorderStyle)val;
			
			
		}

		/// <summary>
		/// Returns the resolved value, if the band's Override value is default 
		/// it will call the Layout's BorderStyleSummaryValueDefault property.
		/// </summary>
        // MRS 7/7/2009 - TFS18285
        // Made this public for the document exporter. 
		//internal Infragistics.Win.UIElementBorderStyle BorderStyleSummaryValueResolved
        [EditorBrowsable( EditorBrowsableState.Advanced)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Infragistics.Win.UIElementBorderStyle BorderStyleSummaryValueResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleSummaryValue, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleSummaryValue : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleSummaryValue;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleSummaryValue,
						StyleUtils.Role.SummaryValue, style, UIElementBorderStyle.Solid );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		internal Infragistics.Win.UIElementBorderStyle BorderStyleSummaryFooterCaptionResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleSummaryFooterCaption, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleSummaryFooterCaption : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleSummaryFooterCaption;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleSummaryFooterCaption,
						StyleUtils.Role.SummaryFooterCaption, style, this.BorderStyleSummaryValueResolved );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		internal Infragistics.Win.UIElementBorderStyle BorderStyleCellResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleCell, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleCell : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleCell;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleCell,
						StyleUtils.Role.Cell, style, this.CardView ? UIElementBorderStyle.None : UIElementBorderStyle.Solid );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		internal Infragistics.Win.UIElementBorderStyle BorderStyleCardAreaResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleCardArea, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleCardArea : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleCardArea;

                    val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleCardArea,
						StyleUtils.Role.CardArea, style, UIElementBorderStyle.None );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		//JM 01-16-02 The following is no longer needed - now using
		//			  BorderStyleRow for cards
		

		// SSP 2/5/07 BR19011
		// Added BorderStyleHeaderDefault. We need to be able to get explicitly set BorderStyleHeader
		// setting.
		// 
		internal UIElementBorderStyle BorderStyleHeaderDefault
		{
			get
			{
				UIElementBorderStyle style = null != this.overrideObj
						? this.overrideObj.BorderStyleHeader : UIElementBorderStyle.Default;

				if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
					style = this.layout.Override.BorderStyleHeader;

				return style;
			}
		}

		/// <summary>
		/// Returns the resolved border style of the headers in this Band.
		/// </summary>
        // MRS v7.2 - Document Exporter
		//internal Infragistics.Win.UIElementBorderStyle BorderStyleHeaderResolved
        //
        // MBS 7/27/07 - BR25195
        [Browsable(false)]

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
        public Infragistics.Win.UIElementBorderStyle BorderStyleHeaderResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleHeader, out val ) )
				{
					UIElementBorderStyle style = this.BorderStyleHeaderDefault;
					UIElementBorderStyle defVal;

					// SSP 3/7/03 - Row Layout Functionality
					// When the headers are with the cells in row layout mode, return Solid
					// as the default border style.
					//
					if ( this.UseRowLayoutResolved && ! this.AreColumnHeadersInSeparateLayoutArea )
						defVal = UIElementBorderStyle.Solid;
                    else if ( this.CardView )
						defVal = UIElementBorderStyle.None;
					else
						defVal = UIElementBorderStyle.RaisedSoft;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleHeader,
						StyleUtils.Role.Header, style, defVal );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		// SSP 3/7/03 - Row Layout Functionality
		// Added BorderStyleRowSelector proeprty to the override.
		//
		internal UIElementBorderStyle BorderStyleRowSelectorResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleRowSelector, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleRowSelector : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleRowSelector;

					// SSP 2/5/07 BR19011
					// Don't default the border style of the row selector to the resolved border style
					// of the header. Instead, default it to the explicit setting of BorderStyleHeader
					// and if it's not explicitly set then default it to RaisedSoft as we used to 
					// previously.
					// 
					UIElementBorderStyle defVal = this.BorderStyleHeaderDefault;
					if ( UIElementBorderStyle.Default == defVal )
						defVal = UIElementBorderStyle.RaisedSoft;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleRowSelector,
						StyleUtils.Role.RowSelector, style,
						// SSP 2/5/07 BR19011
						// Change related to above.
						// 
						//this.BorderStyleHeaderResolved 
						defVal );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		internal CellClickAction CellClickActionResolved
		{
			get
			{
				if ( !( this.Layout.Grid is UltraGrid ) )
					return CellClickAction.RowSelect;

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCellClickAction() )
					return this.overrideObj.CellClickAction;

				return this.layout.CellClickActionDefault;
			}
		}

		internal DefaultableBoolean CellMultiLineResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCellMultiLine() )
					return this.overrideObj.CellMultiLine;

				return this.layout.CellMultiLineDefault;
			}
		}

		// SSP 5/16/06 - App Styling
		// 
		#region CellPaddingResolved

		

		internal int CellPaddingResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCellPadding() )
					return this.overrideObj.CellPadding;

				return this.layout.CellPaddingDefault;
			}
		}

		#endregion // CellPaddingResolved

		internal int CellSpacingResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCellSpacing() )
					return this.overrideObj.CellSpacing;

				return this.layout.GetCellSpacingDefault( this );
			}
		}

		internal int CardSpacingResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCardSpacing() )
					return this.overrideObj.CardSpacing;

				return this.layout.CardSpacingDefault;
			}
		}


		internal int CardAreaMarginsResolved
		{
			get
			{
				return this.CardSpacingResolved;
			}
		}

		#region CalcGroupByRowIndent

		// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
		// Added an overload of CalcGroupByRowIndent that takes in groupByRowsCollection 
		// instead of a group-by row.
		//
		internal int CalcGroupByRowIndent( RowsCollection groupByRowsCollection )
		{
			Debug.Assert( null != groupByRowsCollection, "Null group by row collection passed in" );

			if ( null == groupByRowsCollection )
				return 0;

			Debug.Assert( this == groupByRowsCollection.Band, "Passed in row collection does not belong to this band" );

			if ( this != groupByRowsCollection.Band )
			{
				if ( null != groupByRowsCollection.Band )
					return groupByRowsCollection.Band.CalcGroupByRowIndent( groupByRowsCollection );
				else
					return 0;
			}
				
			int indent = 0;

			int totalLevels = 1 + this.SortedColumns.LastGroupByColIndex;
                
			// JAS 2005 v2 GroupBy Row Extensions
			//
			//			indent = ( GroupByRowUIElement.GROUP_BY_INDENT )
			//				* ( totalLevels - groupByRow.GroupByRowLevel );
			indent = ( this.IndentationGroupByRowResolved )
				* ( totalLevels - groupByRowsCollection.GroupByRowLevel );

			// JJD 1/24/02
			// Check the IsOutlookGroupBy property instead
			//
			//if ( this.Layout.ViewStyleImpl is ViewStyleOutlookGroupBy )
			if ( this.Layout.ViewStyleImpl.IsOutlookGroupBy )
			{
				if ( this.PreRowAreaExtent > 0 )
					indent -= Math.Max( 0, ( UltraGridBand.PRE_ROW_SELECTOR_WIDTH - UltraGridBand.EXPANSION_INDICATOR_WIDTH ) / 2 );
			}

			return -indent;
		}

		internal int CalcGroupByRowIndent( UltraGridGroupByRow groupByRow )
		{
			// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
			// Added an overload of CalcGroupByRowIndent that takes in groupByRowsCollection 
			// instead of a group-by row. Commented out the original call and added code to
			// use the new overload. The code in that overload was moved from here.
			//
			return this.CalcGroupByRowIndent( null != groupByRow ? groupByRow.ParentCollection : null );
			
		}

		#endregion // CalcGroupByRowIndent

		#region CalcGroupBySummariesIndent

		// SSP 4/26/05 - NAS 5.2 Extension of Summaries Functionality
		// Added CalcGroupBySummariesIndent.
		/// <summary>
		/// Returns a value that specifies the amount by which to indent the summaries in
		/// the summary footers of the specified group-by row collection.
		/// </summary>
		/// <param name="rows"></param>
		/// <returns></returns>
        // MRS 4/24/2008 - BR32001
        // Made this public and EditorBrowsableState.Advanced. We need it for Document exporting. 
        //internal int CalcGroupBySummariesIndent( RowsCollection rows )
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public int CalcGroupBySummariesIndent(RowsCollection rows)
        {
			return null == rows || ! rows.IsGroupByRows ? 0 
				: - this.CalcGroupByRowIndent( rows ) + this.PreRowAreaExtent;
		}

		internal int CalcGroupBySummariesIndent( UltraGridGroupByRow row )
		{
			return this.CalcGroupBySummariesIndent( null != row ? row.ParentCollection : null );
		}

		#endregion // CalcGroupBySummariesIndent

        // MRS 8/7/2008 BR35168 
        #region CalcGroupByConnectorsExtent

        /// <summary>
        /// Figures out the amount by which to extend left the headers in order
        /// to have them occupy the space over the group-by row connectors.
        /// </summary>
        /// <returns></returns>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public int CalcGroupByConnectorsExtent()
        {
            UltraGridLayout layout = this.Layout;

            if (layout == null ||
                layout.ViewStyleImpl == null)
            {
                return 0;
            }

            if (HeaderPlacement.OncePerGroupedRowIsland == this.HeaderPlacementResolved
                || layout.ViewStyleImpl.IsOutlookGroupBy && layout.ViewStyleImpl.BandHasFixedHeaders(this))
            {
                return this.Layout.ViewStyleImpl.CalcGroupByConnectorsExtent(this);
            }

            return 0;
        }

        #endregion // CalcGroupByConnectorsExtent

		internal int ColWidthResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeDefaultColWidth() )
					return this.overrideObj.DefaultColWidth;

				return this.layout.ColWidthDefault;
			}
		}
		
		
		internal int RowHeightResolved
		{
			get
			{
				// if RowSizing is ssRowSizingSychronized and we have a valid m_lSynchronizedRowHeight,
				// use m_lSynchronizedRowHeight
				if ( this.RowSizingResolved == Infragistics.Win.UltraWinGrid.RowSizing.Sychronized && 0 != this.synchronizedRowHeight )
					return this.synchronizedRowHeight;

				int defaultRowHeight = -1;

				// first check the band's override object's value
				//
				if ( null != this.overrideObj )
					defaultRowHeight = this.overrideObj.DefaultRowHeight;

				// if still default then get the layout's value
				//
				if ( defaultRowHeight < 0 )
					defaultRowHeight = this.Layout.RowHeightDefault;

				// if a default row height has been specified then return it
				//
				if ( defaultRowHeight > 0 )
					return defaultRowHeight;

				// Otherwise, return the min row height
				//
				// SSP 8/29/05 BR05837
				// In row-layout return the preferred row layout size rather than the minimum.
				// This change is necessary due to a change that needed to be made in the
				// GetMinRowHeightHelper method.
				// 
				//return this.GetMinRowHeight();
				return this.UseRowLayoutResolved ? this.GetLayoutRowHeight( false ) : this.GetMinRowHeight( );
			}
		}	

		internal ShowExpansionIndicator ExpansionIndicatorResolved
		{
			get
			{
				//if this is a dropdown don't show expansion indicator
				//
				if ( !( this.Layout.Grid is UltraGrid ) )
					return ShowExpansionIndicator.Never;

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeExpansionIndicator() )
					return this.overrideObj.ExpansionIndicator;

				return this.layout.ExpansionIndicatorDefault;
			}
		}

		

		internal HeaderClickAction GetHeaderClickActionResolved( bool changeExternalSortTypesToRegular )
		{
			HeaderClickAction headerClickAction = HeaderClickAction.Default;

			if ( this.HasOverride &&
				this.overrideObj.ShouldSerializeHeaderClickAction() )
				headerClickAction = this.overrideObj.HeaderClickAction;

				// SSP 11/01/01 UWG623
				// If the override off the layout has HeaderClickAction set, then
				// use that before proceeding with the default logic which is to
				// return SortMulti if there are any sorted columns.
				//
			else if ( this.Layout.HasOverride &&
				this.Layout.Override.ShouldSerializeHeaderClickAction( ) )
				headerClickAction = this.layout.HeaderClickActionDefault;

				// SSP 10/9/01 UWG432
				// We are defaulting the header click action to 
				// SortMulti if there are any sorted columns in this
				// band
				//
			else
				headerClickAction = this.HasSortedColumns ? 
					HeaderClickAction.SortMulti :
					this.layout.HeaderClickActionDefault;

			if ( changeExternalSortTypesToRegular )
			{
				if ( HeaderClickAction.ExternalSortSingle == headerClickAction )
					headerClickAction = HeaderClickAction.SortSingle;
				else if ( HeaderClickAction.ExternalSortMulti == headerClickAction )
					headerClickAction = HeaderClickAction.SortMulti;
			}

			return headerClickAction;
		}

		internal HeaderClickAction HeaderClickActionResolved
		{
			get
			{
				// SSP 4/19/04 - Virtual Binding Related
				// Added ExternalSortSingle and ExternalSortMulti members. If the header 
				// click action is set to ExternalSortSingle or ExternalSortMulti then
				// return SortSingle and SortMulti.
				// Moved the code into GetHeaderClickActionResolved method.
				//
				return this.GetHeaderClickActionResolved( true );
			}
		}

		internal int MaxSelectedCellsResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeMaxSelectedCells() )
					return this.overrideObj.MaxSelectedCells;

				return this.layout.MaxSelectedCellsDefault;
			}
		}

		internal int MaxSelectedRowsResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeMaxSelectedRows() )
					return this.overrideObj.MaxSelectedRows;

				return this.layout.MaxSelectedRowsDefault;
			}
		}

		//JDN 11/19/04 Added RowSelectorHeader
		/// <summary>
		/// Returns the resolved value, if the band's Override value 
		/// is default it will use the Layout's RowSelectorHeaderStyleDefault 
		/// property.
		/// </summary>
		// SSP 4/21/05
		// Made RowSelectorHeaderStyleResolved public because the row-layout designer needs it.
		//
		//internal RowSelectorHeaderStyle RowSelectorHeaderStyleResolved
		[ EditorBrowsable( EditorBrowsableState.Advanced ), Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public RowSelectorHeaderStyle RowSelectorHeaderStyleResolved
		{
			get
			{
				
				
				
				
				
				object objStyle;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.RowSelectorHeaderStyle, out objStyle ) )
				{
					RowSelectorHeaderStyle style = null != this.overrideObj
						? this.overrideObj.RowSelectorHeaderStyle : RowSelectorHeaderStyle.Default;

					if ( RowSelectorHeaderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.RowSelectorHeaderStyle;

					objStyle = StyleUtils.CachePropertyValue( this, StyleUtils.CustomProperty.RowSelectorHeaderStyle, 
						style, RowSelectorHeaderStyle.Default, RowSelectorHeaderStyle.ExtendFirstColumn );
				}

				return (RowSelectorHeaderStyle)objStyle;
				
			}
		}

		internal DefaultableBoolean RowSelectorsResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSelectors() )
					return this.overrideObj.RowSelectors;

				return this.layout.RowSelectorsDefault;
			}
		}
		internal RowSizing RowSizingResolved
		{
			get
			{
				// SSP 3/17/03 - Row Layout Functionality
				// In row layout functionality, don't allow the row resizing modes of
				// Free or AutoFixed since we don't want to have different row heights.
				//
				

				RowSizing rowSizing;
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSizing() )
					rowSizing = this.overrideObj.RowSizing;
				else
					rowSizing = this.layout.RowSizingDefault;

				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// Implemented auto-sizing in row-layout mode.
				// 
				

				return rowSizing;
			}
		}
		internal RowSizingArea RowSizingAreaResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSizingArea() )
					return this.overrideObj.RowSizingArea;

				return this.layout.RowSizingAreaDefault;
			}
		}

		internal int RowSizingAutoMaxLinesResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSizingAutoMaxLines() )
					return this.overrideObj.RowSizingAutoMaxLines;

				return this.layout.RowSizingAutoMaxLinesDefault;
			}
		}

		internal int RowSpacingBeforeDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSpacingBefore() )
					return this.overrideObj.RowSpacingBefore;

				return this.layout.RowSpacingBeforeDefault;
			}
		}
		internal int RowSpacingAfterDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSpacingAfter() )
					return this.overrideObj.RowSpacingAfter;

				return this.layout.RowSpacingAfterDefault;
			}
		}

		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		internal int GroupByRowSpacingBeforeDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeGroupByRowSpacingBefore() )
					return this.overrideObj.GroupByRowSpacingBefore;

				return this.layout.GroupByRowSpacingBeforeDefault;
			}
		}

		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		internal int GroupByRowSpacingAfterDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeGroupByRowSpacingAfter() )
					return this.overrideObj.GroupByRowSpacingAfter;

				return this.layout.GroupByRowSpacingAfterDefault;
			}
		}

		internal SelectType SelectTypeCellResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSelectTypeCell() )
					return this.overrideObj.SelectTypeCell;

				return this.layout.SelectTypeCellDefault;
			}
		}

		internal SelectType SelectTypeColResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSelectTypeCol() )
					return this.overrideObj.SelectTypeCol;

				return this.layout.SelectTypeColDefault;
			}
		}
		internal SelectType SelectTypeRowResolved
		{
			get
			{
				if ( !( this.Layout.Grid is UltraGrid ) )
					return SelectType.Single;

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSelectTypeRow() )
					return this.overrideObj.SelectTypeRow;

				return this.layout.SelectTypeRowDefault;
			}
		}

		// SSP 7/8/02 UWG1154
		//
		internal SelectType SelectTypeGroupByRowResolved
		{
			get
			{
				if ( !( this.Layout.Grid is UltraGrid ) )
					return SelectType.Single;

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSelectTypeGroupByRow() )
					return this.overrideObj.SelectTypeGroupByRow;

				return this.layout.SelectTypeGroupByRowDefault;
			}
		}

		#region TipStyleCellResolved

		// SSP 7/21/06 BR14294
		// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
		// or the cell's value (clipped) will be displayed.
		// 
		//internal TipStyle TipStyleCellResolved
		internal TipStyle TipStyleCellUnResolved
		{
			get
			{
				// SSP 7/21/06 BR14294
				// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
				// or the cell's value (clipped) will be displayed.
				// 
				// ------------------------------------------------------------------------------------------------
				

				TipStyle val = this.HasOverride ? this.overrideObj.TipStyleCell : TipStyle.Default;

				if ( TipStyle.Default == val && this.layout.HasOverride )
					val = this.layout.Override.TipStyleCell;

				return val;
				// ------------------------------------------------------------------------------------------------
			}
		}

		// SSP 7/21/06 BR14294
		// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
		// or the cell's value (clipped) will be displayed.
		// 
		internal TipStyle TipStyleCellResolved( UltraGridRow row, UltraGridColumn col )
		{
			return this.TipStyleCellResolved( row.GetCellIfAllocated( col ) );
		}

		// SSP 7/21/06 BR14294
		// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
		// or the cell's value (clipped) will be displayed.
		// 
		internal TipStyle TipStyleCellResolved( UltraGridCell cell )
		{
			TipStyle val = this.TipStyleCellUnResolved;

			if ( TipStyle.Default == val )
			{
				// For grid default to Show. For UltraDropDown adn UltraCombo default to Show if ToolTipText is
				// explicitly set. Otherwise default to Hide.
				// 
				val = this.layout.Grid is UltraGrid
					|| null != cell && null != cell.ToolTipText && cell.ToolTipText.Length > 0
					? TipStyle.Show : TipStyle.Hide;
			}

			return val;
		}

		#endregion // TipStyleCellResolved

		#region TipStyleHeaderResolved

		// SSP 3/3/06 BR10430
		// Added TipStyleHeader on the Override.
		// 
		internal TipStyle TipStyleHeaderResolved
		{
			get
			{
				TipStyle val = this.HasOverride ? this.overrideObj.TipStyleHeader : TipStyle.Default;

				if ( TipStyle.Default == val && this.layout.HasOverride )
					val = this.layout.Override.TipStyleHeader;

				return TipStyle.Default == val ? TipStyle.Show : val;
			}
		}

		#endregion // TipStyleHeaderResolved

		internal TipStyle TipStyleRowConnectorResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeTipStyleRowConnector() )
					return this.overrideObj.TipStyleRowConnector;

				return this.layout.TipStyleRowConnectorDefault;
			}
		}
		// SSP 7/19/05
		// TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
		// need to resolve the TipStyleScroll differently based on whether the scrolling logic 
		// or the row connector logic is calling it.
		// 
		//internal TipStyle TipStyleScrollResolved
		internal TipStyle TipStyleScrollResolved( bool forRowConnectorTooltip )
		{
			if ( this.HasOverride &&
				this.overrideObj.ShouldSerializeTipStyleScroll() )
				return this.overrideObj.TipStyleScroll;

			// SSP 7/19/05
			// TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
			// need to resolve the TipStyleScroll differently based on whether the scrolling logic 
			// or the row connector logic is calling it.
			// 
			//return this.layout.TipStyleScrollDefault;
			if ( this.layout.HasOverride && this.layout.Override.ShouldSerializeTipStyleScroll( ) )
				return this.layout.Override.TipStyleScroll;

			if ( ! forRowConnectorTooltip && ScrollStyle.Immediate == this.layout.ScrollStyle )
				return TipStyle.Hide;
			else
				return TipStyle.Show;
		}

		/// <summary>
		/// Returns or sets a value that determines whether the band is Card View mode.
		/// </summary>
		/// <remarks>
		/// <p class="body">Card View mode is a view style of the band that presents row data as a series of note cards instead of the traditional rows and columns. This view style is similar to the card view style of the Contacts section of Microsoft Outlook. Card View mode replaces the rows and columns of the child band with a <i>card view area</i> which contains the cards. The card view area scrolls horizontally to display as many cards as are required by the data in the band.</p>
		/// <p class="body">In Card View mode, you have a number of choices that control the way the cards will appear. You can choose to display labels on cards, control the size of the card view area, set guidelines for how cards will be arranged, and specify how the data field labels (analogous to column headers) will appear. These options are set by manipulating the properties of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand.CardSettings"/> object associated with the band.</p>
		/// <p class="note">Note that when a band is in Card View mode, <i>no children of that band will be displayed.</i> In card view, there is no effective interface for displaying a child band, so the display of child bands is disabled. Child bands may still exist, but they cannot be shown by the control.</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridBand_P_CardView")]
		[ LocalizedCategory("LC_Appearance") ]
		public bool CardView
		{
			get
			{
				// SSP 4/22/03 - Optimizations.
				// If the card view is false, then return false immediately.
				//
				if ( ! this.cardView )
					return false;

                // MRS 5/15/2008
                if (this.layout == null)
                    return this.cardView;

				// JJD 12/13/01
				// We don't support card view for printing so return false
				//
				// SSP 5/7/03 - Export Functionality
				// The same applies to the export functionality as well.
				//
				//if ( this.Layout.IsPrintLayout )
                // MRS NAS v8.2 - CardView Printing
                //if ( this.Layout.IsPrintLayout || this.Layout.IsExportLayout )
                if (this.Layout.IsExportLayout)
                    return false;

                // MRS NAS v8.2 - CardView Printing
                if (this.Layout.IsPrintLayout)
                {
                    switch (this.Layout.AllowCardPrinting)
                    {
                        case AllowCardPrinting.Never:
                            return false;
                        case AllowCardPrinting.RootBandOnly:
                            if (this.IsRootBand == false)
                                return false;

                            break;
                        default:
                            Debug.Fail("Unknown AllowCardPrinting Setting");
                            break;
                    }
                }

				// JJD 12/17/01
				// We don't support card view for dropdowns and combos so return false
				//
				if ( this.layout.Grid != null &&
					!( this.layout.Grid is UltraGrid ) )
					return false;
				
				return this.cardView;
			}

			set
			{
				if ( this.cardView != value )
				{
					this.cardView   = value;
					
					// SSP 1/30/03 - Row Layout Functionality
					// In row layout functionality, hidning the column headers means that they
					// will be shown in the row cell area (according to the RowLayoutColumnInfo.LabelPosition
					// property settings. This means that we have to dirty the layout cache.
					//
					this.DirtyRowLayoutCachedInfo( );

					if ( this.layout != null )
						this.layout.BumpBandCardViewVersion();

					this.NotifyPropChange( PropertyIds.CardView );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeCardView() 
		{
			return ( this.cardView != false );
		}

		/// <summary>
		/// Resets CardView to its default value (false).
		/// </summary>
		public void ResetCardView() 
		{
			this.CardView = false;
		}


		
		// SSP 7/26/02 UWG1416
		// Made the AddButtonCaption property localizable by adding Localiazable attribute.
		//
		/// <summary>
		/// Returns or sets the caption text of the Band's Add button.
		/// </summary>
		///	<remarks>
		///	<p class="body">When the AddNew box is displayed, it contains a button for each band in the grid. The buttons are arranged in a hierarchical display that mimics the arrangement of the bands in the grid. The user can click the appropriate button to add a new row to the indicated band. The <b>AddButtonCaption</b> property determines what will be displayed on the AddNew box button for the current band.</p>
		///	<p class="body">By default, this property uses a <i>key</i>value (the name of the recordset) that it retrieves from the data provider (if it is available).If the <b>AddButtonCaption</b> property is not set (the string is empty) then the caption of the button will be this band's key. If the key is empty, then the first visible header's caption will be used.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AddButtonToolTipText"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.AddNewBox"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridBand_P_AddButtonCaption")]
		[LocalizedCategory("LC_Display")]
		[ Localizable( true ) ]
		public string AddButtonCaption
		{
			get
			{
				if ( this.addButtonCaption == null)
				{
					//if ( this.Key == null )
					//	return this.header.Caption;
					return this.Key;
				}
				
				else
					return this.addButtonCaption;
			}
			set
			{
				if ( this.addButtonCaption != value )
				{
					this.addButtonCaption   = value;
					this.NotifyPropChange( PropertyIds.AddButtonCaption );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAddButtonCaption() 
		{
			if ( null == this.addButtonCaption ||
				this.addButtonCaption.Length < 1 )
				return false;

			// JJD 11/21/01
			// Only serialize the addbuttoncaption if it is
			// different from the key
			//
			return this.addButtonCaption != this.Key;
		}
 
		/// <summary>
		/// Resets AddButtoCaption to its default value (null).
		/// </summary>
		public void ResetAddButtonCaption() 
		{
			this.AddButtonCaption = null;
		}

		/// <summary>
		/// Returns or sets the text used as the Add button's tool tip
		/// </summary>
		///	<remarks>
		///	<p class="body">When the AddNew box is displayed, it contains a button for each band in the grid. The buttons are arranged in a hierarchical display that mimics the arrangement of the bands in the grid. The user can click the appropriate button to add a new row to the indicated band. The <b>AddButtonToolTipText</b> property determines what will be displayed in the tooltip that appears when the mouse is over the AddNew box button for the current band. By default, this property is set to an empty string("") indicating that no tooltip will be displayed.</p>
		///	<p class="body">When the AddNew box is displayed, it contains a button for each band in the grid. The buttons are arranged in a hierarchical display that mimics the arrangement of the bands in the grid. The user can click the appropriate button to add a new row to the indicated band. The <b>AddButtonCaption</b> property determines what will be displayed on the AddNew box button for the current band.</p>
		///	<p class="body">By default, this property uses a <i>key</i>value (the name of the recordset) that it retrieves from the data provider (if it is available).If the <b>AddButtonCaption</b> property is not set (the string is empty) then the caption of the button will be this band's key. If the key is empty, then the first visible header's caption will be used.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AddButtonCaption"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.AddNewBox"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridBand_P_AddButtonToolTipText")]
		[LocalizedCategory("LC_Behavior")]
		[ Localizable( true ) ]
		public string AddButtonToolTipText
		{
			get
			{
				return this.addButtonToolTipText;
			}
			set
			{
				if ( this.addButtonToolTipText != value )
				{
					this.addButtonToolTipText   = value;
					this.NotifyPropChange( PropertyIds.AddButtonToolTipText );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAddButtonToolTipText() 
		{
			return ( null != this.addButtonToolTipText &&
				this.addButtonToolTipText.Length > 0 );
		}
 
		/// <summary>
		/// Resets AddButtonToolTip Text to its default value (null).
		/// </summary>
		public void ResetAddButtonToolTipText() 
		{
			this.AddButtonToolTipText = null;
		}

		/// <summary>
		/// Returns or sets the name of the field used to supply the text for the AutoPreview area.
		/// </summary>
		///	<remarks>
		///	<p class="body">The AutoPreview area appears under a row and provides a way to display multiple lines of text associated with that row. You can specify how many lines of text should be displayed, and choose to either display the value from a cell in the row or a custom text string that you specify. One common use might be to display the contents of a memo field that initially appears off-screen when the grid is loaded.</p>
		///	<p class="body">The <b>AutoPreviewField</b> property specifies the data field that will be used to populate the AutoPreview area. Whatever value is present in the specified field will be displayed in the AutoPreview area.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridRow.Description"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewEnabled"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewIndentation"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewMaxLines"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowPreviewAppearance"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridBand_P_AutoPreviewField")]
		[LocalizedCategory("LC_Behavior")]
		public string AutoPreviewField
		{
			get
			{
				return this.autoPreviewField;
			}
			set
			{
				if ( this.autoPreviewField != value )
				{
					// JJD 11/28/01 UWG780
					// If the layout is null just set the property
					//
					if ( this.layout == null )
					{
						this.autoPreviewField   = value;
					}
					else
					{
						//RobA UWG237 8/27/01 this was being check in the get of
						//Row.Description but now we are not calling that all the time
						//
						try
						{
							// JJD 11/29/01
							// If the value is null don't bother looking for it
							//
							if ( value == null || 
								value.Length < 1 ||
								this.Columns.Exists( value ) )
							{
								this.autoPreviewField   = value;
								this.NotifyPropChange( PropertyIds.AutoPreviewField );
							}
							else
								throw new ArgumentException( SR.GetString("LER_Exception_315", value) );
								
						}
						catch( Exception )
						{
							throw new ArgumentException( SR.GetString("LER_Exception_315", value) );
						}
					}
					
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAutoPreviewField() 
		{
			return ( null != this.autoPreviewField &&
				this.autoPreviewField.Length > 0 );
		}
 
		/// <summary>
		/// Resets AutoPreviewField to its default value (null).
		/// </summary>
		public void ResetAutoPreviewField() 
		{
			this.AutoPreviewField = null;
		}

		/// <summary>
		/// Returns or sets a value that determines whether the AutoPreview area will be displayed.
		/// </summary>
		///	<remarks>
		///	<p class="body">The AutoPreview area appears under a row and provides a way to display multiple lines of text associated with that row. You can specify how many lines of text should be displayed, and choose to either display the value from a cell in the row or a custom text string that you specify. One common use might be to display the contents of a memo field that initially appears off-screen when the grid is loaded.</p>
		///	<p class="body">The <b>AutoPreviewEnabled</b> property determines whether the AutoPreview area can be displayed for rows in the specified band. Once AutoPreview has been enabled, it can be displayed for any row by setting the UltraGridRow object's <b>AutoPreviewHidden</b> property to False.</p>
		///	<p class="body">The <b>Description</b> property of the row determines the text to be displayed in the AutoPreview area. <b>Description</b> can be automatically set using the field in the row specified by the <b>AutoPreviewField</b> property, or it can be explicitly set through code (for example, in the <b>InitializeRow</b> event handler).</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewField"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewIndentation"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewMaxLines"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowPreviewAppearance"/>
		/// <seealso cref="UltraGridRow.AutoPreviewHidden"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridBand_P_AutoPreviewEnabled")]
		[LocalizedCategory("LC_Behavior")]
		public bool AutoPreviewEnabled
		{
			get
			{
				return this.autoPreviewEnabled;
			}
			set
			{
				if ( this.autoPreviewEnabled != value )
				{
					this.autoPreviewEnabled   = value;
					this.NotifyPropChange( PropertyIds.AutoPreviewEnabled );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAutoPreviewEnabled() 
		{
			return this.autoPreviewEnabled;
		}
 
		/// <summary>
		/// Resets AutoPreviewEnabled to its default value (False).
		/// </summary>
		public void ResetAutoPreviewEnabled() 
		{
			this.AutoPreviewEnabled = false;
		}

		//RobA UWG540 10/16/01
		//added AutoPreviewIndentation
		/// <summary>
		/// Returns or sets the amount of horizontal indentation for a row's AutoPreview area.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewEnabled"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewField"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewMaxLines"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowPreviewAppearance"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_AutoPreviewIndentation")]
		[LocalizedCategory("LC_Behavior")]
		public int AutoPreviewIndentation
		{
			get
			{
				return this.autoPreviewIndentation;
			}

			set
			{
				if ( this.autoPreviewIndentation != value )
				{
					this.autoPreviewIndentation = value;
					this.NotifyPropChange(PropertyIds.AutoPreviewIndentation);
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAutoPreviewIndentation() 
		{
			return this.autoPreviewIndentation != 0;
		}
 
		/// <summary>
		/// Resets AutoPreviewIndentation to its default value (0).
		/// </summary>
		public void ResetAutoPreviewIndentation() 
		{
			this.AutoPreviewIndentation = 0;
		}
        

		/// <summary>
		/// Returns or sets the maximum number of lines to be auto-previewed
		/// </summary>
		///	<remarks>
		///	<p class="body">The AutoPreview area appears under a row and provides a way to display multiple lines of text associated with that row. You can specify how many lines of text should be displayed, and choose to either display the value from a cell in the row or a custom text string that you specify. One common use might be to display the contents of a memo field that initially appears off-screen when the grid is loaded.</p>
		///	<p class="body">The <b>AutoPreviewMaxLines</b> property specifies the maximum number of lines of text that will appear in the AutoPreview area. The default value is 3.</p>
		///	<p class="body">The <b>Description</b> property of the row determines the text to be displayed in the AutoPreview area. <b>Description</b> can be automatically set using the field in the row specified by the <b>AutoPreviewField</b> property, or it can be explicitly set through code (for example, in the <b>InitializeRow</b> event handler).</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewEnabled"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.AutoPreviewField"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridRow.Description"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowPreviewAppearance"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridBand_P_AutoPreviewMaxLines")]
		[LocalizedCategory("LC_Behavior")]
		public int AutoPreviewMaxLines
		{
			get
			{
				return this.autoPreviewMaxLines;
			}
			set
			{
				if ( this.autoPreviewMaxLines != value )					
				{
					//RobA UWG738 11/19/01
					if ( value <= 0 )
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_9"),Shared.SR.GetString("LE_ArgumentOutOfRangeException_303"));
 
					this.autoPreviewMaxLines   = value;
					this.NotifyPropChange( PropertyIds.AutoPreviewMaxLines );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeAutoPreviewMaxLines() 
		{
			return 3 != this.autoPreviewMaxLines;
		}
 
		/// <summary>
		/// Resets AutoPreviewMaxLines to its default value (3).
		/// </summary>
		public void ResetAutoPreviewMaxLines() 
		{
			this.AutoPreviewMaxLines = 3;
		}
        

		/// <summary>
		/// Returns or sets the number of lines to display for column headers.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>ColHeaderLines</b> property determines how many lines of text can appear inside of a column header. Setting the value of this property will change the height of the column headers to accommodate the specified number of lines, whether or not any column header actually contains enough text to fill multiple lines. The minimum value for this property is 1. The maximum value is 10.
		/// </p>
		/// <p class="body">
		/// If you want the UltraGrid to automatically wrap the header caption and also 
		/// change the height of the headers to accomodate wrapped header caption, then
		/// set the <see cref="UltraGridOverride.WrapHeaderText"/> to <b>True</b>.
		/// </p>
		/// <p class="body">
		/// Pixel level control over the heights of headers can be achieved using the
		/// Row-Layout functionality. See <see cref="UltraGridBand.UseRowLayout"/> for
		/// more information.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Columns"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.ColHeadersVisible"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.Header"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.HeaderBase.Caption"/>
		/// <seealso cref="UltraGridOverride.HeaderAppearance"/>
		/// <seealso cref="HeaderBase.Appearance"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_ColHeaderLines")]
		[LocalizedCategory("LC_Display")]
		public int ColHeaderLines
		{
            get
			{
				return this.colHeaderLines;
			}
			set
			{
				// SSP 10/31/01 UWG612
				// Throw ArgumentOutOfRangeException exception if the value
				// is out of range.
				//
				if ( value < 1 || value > 10 )
					throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_11") );

				if ( this.colHeaderLines != value )
				{
					this.colHeaderLines   = value;

					// SSP 1/30/03 - Row Layout Functionality
					// In row layout functionality, hidning the column headers means that they
					// will be shown in the row cell area (according to the RowLayoutColumnInfo.LabelPosition
					// property settings. This means that we have to dirty the layout cache.
					//
					this.DirtyRowLayoutCachedInfo( );

					this.NotifyPropChange( PropertyIds.ColHeaderLines );
				}
			}
		}

		// SSP 11/20/03 UWG2741
		// Band.CommitRowUpdateList simply works on the child rows collection associated
		// with the active row in the parent band. It doesn't take care of other child
		// rows collections that belong to non-active parent rows. Look in the 
		// CommitRowUpdateList implementation which has been commented out as a result.
		// Commented out CommitRowUpdateList and CommitRowUpdateListHelper methods as we
		// need a way to commit all rows and not just the child rows associated with
		// the current active parent row.
		//
		

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeColHeaderLines() 
		{
			return 1 != this.colHeaderLines;
		}
 
		/// <summary>
		/// Resets ColumnHeaderLines to its default value (1).
		/// </summary>
		public void ResetColHeaderLines() 
		{
			this.ColHeaderLines = 1;
		}

		// JJD 1/18/02 - UWG946
		// Added internal property to check so we don't interfere with th
		// public ColHeadersVisible property
		//
        // MRS 2/27/2009 - TFS14725
        // Renamed this property to be less misleading. 
		//internal bool ColHeadersVisibleResolved
        internal bool ColHeadersVisibleInSeparateAreaResolved
		{
			get
			{
				// JJD 12/26/01
				// Column headers are never visible in card view
				//
				if ( this.CardView )
					return false;

				// SSP 3/31/03 - Row Layout Functionality
				// If the column headers are being displayed with the cells, then return false here since we won't be displaying
				// the column headers on the top.
				//
				if ( this.UseRowLayoutResolved && Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.WithCellData == this.RowLayoutLabelStyle )
					return false;

				return this.colHeadersVisible;
			}
		}
        
		/// <summary>
		/// Determines if column headers are visible. 
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>ColHeadersVisible</b> property is used to toggle the visibility of column headers. When column headers are not visible, certain header-related functionality, such as column selection, moving and swapping, may become unavailable.
		/// </p>
		/// <p class="body">
		/// Headers of individual columns can be hidden using the Row-Layout functionality.
		/// Row-Layout functionality allows you to create flexible cell arrangements.
		/// See <see cref="UltraGridBand.UseRowLayout"/> for more information.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Columns"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.ColHeaderLines"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.Header"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.HeaderBase.Caption"/>
		/// <seealso cref="UltraGridOverride.HeaderAppearance"/>
		/// <seealso cref="UltraGridColumn.Hidden"/>
		/// <seealso cref="UltraGridBand.UseRowLayout"/>
		/// <seealso cref="RowLayoutColumnInfo.LabelPosition"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_ColHeadersVisible")]
		[LocalizedCategory("LC_Display")]
		public bool ColHeadersVisible
		{
			get
			{
				return this.colHeadersVisible;
			}
			set
			{
				if ( this.colHeadersVisible != value )
				{
					this.colHeadersVisible   = value;

					// SSP 1/30/03 - Row Layout Functionality
					// In row layout functionality, hidning the column headers means that they
					// will be shown in the row cell area (according to the RowLayoutColumnInfo.LabelPosition
					// property settings. This means that we have to dirty the layout cache.
					//
					this.DirtyRowLayoutCachedInfo( );

					this.NotifyPropChange( PropertyIds.ColHeadersVisible );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeColHeadersVisible() 
		{
			return !this.colHeadersVisible;
		}
 
		/// <summary>
		/// Resets ColumnHeaderVisible to its default value (True).
		/// </summary>
		public void ResetColHeadersVisible() 
		{
			this.ColHeadersVisible = true;
		}
        
		/// <summary>
		/// Returns or sets a value that determines if the band is expandable.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>Expandable</b> property determines whether the rows in a band can be expanded. If set to False the row expansion (plus/minus) indicators become inactive.</p>
		///	<p class="body">The <b>ExpansionIndicator</b> property can be used to hide the expansion indicators.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridBand_P_Expandable")]
		[LocalizedCategory("LC_Behavior")]
		public bool Expandable
		{
			get
			{
				return this.expandable;
			}
			set
			{
				if ( this.expandable != value )
				{
					this.expandable   = value;
					this.NotifyPropChange( PropertyIds.Expandable );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeExpandable() 
		{
			return !this.expandable;
		}
 
		/// <summary>
		/// Resets Expandable to its default value (True).
		/// </summary>
		public void ResetExpandable() 
		{
			this.Expandable = true;
		}
        
		/// <summary>
		/// Returns or sets a value that determines the amount of indenting used for this band.
		/// </summary>
		/// <remarks>
		/// You can use the <b>Indentation</b> property to specify how much indenting should be applied to bands beyond the default indenting done by the control. The default value for this property is -1, which indicates that the grid's default indenting should be used.
		/// </remarks> 
		[LocalizedDescription("LD_UltraGridBand_P_Indentation")]
		[LocalizedCategory("LC_Display")]
		public int Indentation
		{
			get
			{
				return this.indentation;
			}
			set
			{
				if ( this.indentation != value )
				{
					this.indentation   = value;
					this.NotifyPropChange( PropertyIds.Indentation );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeIndentation() 
		{
			return this.indentation >= 0;
		}
 
		/// <summary>
		/// Resets Indentation its default value (-1).
		/// </summary>
		public void ResetIndentation() 
		{
			this.Indentation = -1;
		}

		/// <summary>
		/// Returns or sets the number of lines of text to display for groups headers.
		/// </summary>
		/// <remarks>
		/// The <b>GroupHeaderLines</b> property determines how many lines of text can appear inside of a group header. Setting the value of this property will change the height of the group headers to accommodate the specified number of lines, whether or not any group header actually contains enough text to fill multiple lines. The minimum value for this property is 1. The maximum value is 10.
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Groups"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.GroupHeadersVisible"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.ColHeaderLines"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_GroupHeaderLines")]
		[LocalizedCategory("LC_Display")]
		public int GroupHeaderLines
		{
			get
			{
				// RobA UWG680 11/15/01 don't know why we were using colHeaderLines
				//
				//return this.colHeaderLines;
				return this.groupHeaderLines;
			}
			set
			{
				// RobA UWG680 11/15/01 don't know why we were using colHeaderLines
				//
				//if ( this.colHeaderLines != value )
				//{
				//	this.colHeaderLines   = value;
				//	this.NotifyPropChange( PropertyIds.GroupHeaderLines );
				//}
				if ( this.groupHeaderLines != value )
				{

					if ( value < 1 || value > 10 )
						// SSP 11/28/01
						// it should be GroupHeaderLines instead of ColheaderLines
						//throw new ArgumentOutOfRangeException( "ColHeaderLines must be between 1 and 10 inclusive." );
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_12") );

					this.groupHeaderLines = value;
					this.NotifyPropChange( PropertyIds.GroupHeaderLines );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeGroupHeaderLines() 
		{
			// RobA UWG680 11/15/01 don't know why we were using colHeaderLines
			//
			//return 1 != this.colHeaderLines;
			return 1 != this.groupHeaderLines;
		}
 
		/// <summary>
		/// Resets GroupHeaderLines to its default value (1).
		/// </summary>
		public void ResetGroupHeaderLines() 
		{
			this.GroupHeaderLines = 1;
		}


		// JJD 1/18/02 - UWG946
		// Added internal property to check so we don't interfere with th
		// public ColHeadersVisible property
		//
        // MRS 2/27/2009 - TFS14725
        // Renamed this property to be less misleading.
        //
		//internal bool GroupHeadersVisibleResolved
        internal bool GroupHeadersVisibleInSeparateAreaResolved
		{
			get
			{
				// JJD 12/26/01
				// Column headers are never visible in card view
				//
                // MRS 2/27/2009 - TFS14725
                // This does not seem right. ColHeadersVisibleResolved and GroupHeadersVisibleResolved
                // are really just concerned with whether the headers are visible on top. 
                //
                //// MD 2/25/09 - TFS14599
                //// Group labels can still be shown in card view when the layout style is GroupLayout.
                ////if ( this.CardView )
                //if ( this.CardView && this.RowLayoutStyle != RowLayoutStyle.GroupLayout )
                if (this.CardView)
					return false;

                // MRS 2/27/2009 - TFS14725
                if (this.RowLayoutStyle == RowLayoutStyle.ColumnLayout)
                    return false;

                // MRS 4/8/2009 - TFS16545
                if (this.HasGroups == false)
                    return false;

                // MRS 2/27/2009 - TFS14725
                // If the column headers are being displayed with the cells, then return false here since we won't be displaying
                // the column headers on the top.
                //
                if (this.UseRowLayoutResolved && Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.WithCellData == this.RowLayoutLabelStyle)
                    return false;

				return this.groupHeadersVisible;
			}
		}
        

		/// <summary>
		/// Determines if group headers are visible.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>GroupHeadersVisible</b> property is used to toggle the visibility of group headers. When group headers are not visible, certain header-related functionality, such as group selection, moving and swapping, may become unavailable.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.HeaderVisible"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.ColHeaderLines"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.GroupHeaderLines"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Groups"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.UseRowLayout"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_GroupHeadersVisible")]
		[LocalizedCategory("LC_Display")]
		public bool GroupHeadersVisible
		{
			get
			{
				return this.groupHeadersVisible;
			}
			set
			{
				if ( this.groupHeadersVisible != value )
				{
					this.groupHeadersVisible   = value;

					// MD 2/25/09 - TFS14599
					// When this value is changed and we are in GroupLayout style, the layout has to be recalculated.
					this.DirtyRowLayoutCachedInfo();

					this.NotifyPropChange( PropertyIds.GroupHeadersVisible );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeGroupHeadersVisible() 
		{
			return !this.groupHeadersVisible;
		}
 
		/// <summary>
		/// Resets GroupHeadersVisible to its default value (True).
		/// </summary>
		public void ResetGroupHeadersVisible() 
		{
			this.GroupHeadersVisible = true;
		}
        
		/// <summary>
		/// Determines if band headers are visible.
		/// </summary>
		/// <remarks>
		/// The <b>BandHeadersVisible</b> property is used to toggle the visibility of band headers. Band headers identify the band and appear above any column and group headers the band may contain.
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Header"/>
		/// <seealso cref="UltraGridBand.ColHeadersVisible"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_HeaderVisible")]
		[LocalizedCategory("LC_Display")]
		public bool HeaderVisible
		{
			get
			{
				return this.headerVisible;
			}
			set
			{
				if ( this.headerVisible != value )
				{
					this.headerVisible   = value;
					this.NotifyPropChange( PropertyIds.HeaderVisible );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeHeaderVisible() 
		{
			return this.headerVisible;
		}
 
		/// <summary>
		/// Resets HeaderVisible to its default value (False).
		/// </summary>
		public void ResetHeaderVisible() 
		{
			this.HeaderVisible = false;
		}
 
		/// <summary>
		/// Determines whether the object will be displayed. This property is not available at design-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>Hidden</b> property determines whether an object is visible. Hiding an object may have have effects that go beyond simply removing it from view. For example, hiding a band also hides all the rows in that band. Also, changing the <b>Hidden</b> property of an object affects all instances of that object. For example, a hidden column or row is hidden in all scrolling regions.</p>
		///	<p class="body">There may be instances where the <b>Hidden</b> property cannot be changed. For example, you cannot hide the currently active rowscrollregion or colscrollregion. If you attempt to set the <b>Hidden</b> property of the active rowscrollregion to True, an error will occur.</p>
		///	<p class="body">This property is ignored for chaptered columns; that is, columns whose <b>DataType</b> property is set to 136 (DataTypeChapter).</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridBand_P_Hidden")]
		[LocalizedCategory("LC_Display")]
		public bool Hidden
		{
			get 
			{
				
				return this.hidden;
			}
			set
			{
				if(this.hidden != value)
				{
					this.hidden = value;
					this.NotifyPropChange( PropertyIds.Hidden, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeHidden() 
		{
			return this.hidden != false;
		}
 
		/// <summary>
		/// Resets Hidden to its default value (False).
		/// </summary>
		public void ResetHidden() 
		{
			this.Hidden = false;
		}

        // MBS 4/23/09 - TFS12665
        // Refactored this from the HiddenResolved method
        private bool HiddenResolvedInternal
        {
            get
            {
                // SSP 10/25/06 BR16694
                // Moved this code from below. If Hidden is explicitly set on the band, then 
                // hide it regardless of whether it's the first band in a single band mode.
                // 
                // if this band is hidden return true
                //
                if (this.Hidden)
                    return true;

                // if view is SingleBand, then return true for bands
                // other than band first band
                //
                // SSP 11/24/03
                // Use the ViewStyleIml instead of ViewStyle to check if the view style being used
                // is a single band view style. The reason for this is that for UltraDropDown and
                // UltraCombo, we always use a VewStyleSingle even when ViewStyleBand is set to
                // Multi which it is by default. If this change causes any problem, take it out.
                // I changed it because I noticed it and not because it was causing a problem.
                //
                //if ( ViewStyle.SingleBand == this.Layout.ViewStyle )
                if (!this.layout.ViewStyleImpl.IsMultiBandDisplay)
					return this.layout.SortedBands[0] != this;

                // SSP 10/25/06 BR16694
                // Moved this above.
                // 
                

                // if this is not band 0 then return the
                // hidden status of our parent band.
                // This effectively walks up the parent
                // chain
                //
                if (null != this.parentBand)
                {
                    // JJD 12/11/01
                    // If the parent band's CardView property is true then
                    // this band is hidden automatically since there is 
                    // no way to drill down past a cardview in the UI.
                    //
                    if (this.parentBand.CardView)
                        return true;

                    return this.parentBand.HiddenResolved;
                }

                // since we got to band 0 without hidden
                // being set we can return false
                //
                return false;
            }
        }

		/// <summary>
		/// Returns true if the <b>Hidden</b> property for this band is set to
		/// true or any of this band's ancestor bands have their hidden
		/// property set to true.
		/// </summary>
		[ Browsable( false ) ]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public bool HiddenResolved
		{
			get
			{
                // MBS 4/23/09 - TFS12665
                // Moved into a helper property so that we can use the band info caching, if available
                #region Old Code
                //// SSP 10/25/06 BR16694
                //// Moved this code from below. If Hidden is explicitly set on the band, then 
                //// hide it regardless of whether it's the first band in a single band mode.
                //// 
                //// if this band is hidden return true
                ////
                //if ( this.Hidden )
                //    return true;

                //// if view is SingleBand, then return true for bands
                //// other than band first band
                ////
                //// SSP 11/24/03
                //// Use the ViewStyleIml instead of ViewStyle to check if the view style being used
                //// is a single band view style. The reason for this is that for UltraDropDown and
                //// UltraCombo, we always use a VewStyleSingle even when ViewStyleBand is set to
                //// Multi which it is by default. If this change causes any problem, take it out.
                //// I changed it because I noticed it and not because it was causing a problem.
                ////
                ////if ( ViewStyle.SingleBand == this.Layout.ViewStyle )
                //if ( ! this.layout.ViewStyleImpl.IsMultiBandDisplay )
                //    return this.layout.Bands[0] != this;

                //// SSP 10/25/06 BR16694
                //// Moved this above.
                //// 
                ////*
                //// if this band is hidden return true
                ////
                //if ( this.Hidden )
                //    return true;
                //*/

				// if view is SingleBand, then return true for bands
				// other than band first band
				//
				// SSP 11/24/03
				// Use the ViewStyleIml instead of ViewStyle to check if the view style being used
				// is a single band view style. The reason for this is that for UltraDropDown and
				// UltraCombo, we always use a VewStyleSingle even when ViewStyleBand is set to
				// Multi which it is by default. If this change causes any problem, take it out.
				// I changed it because I noticed it and not because it was causing a problem.
				//
				//if ( ViewStyle.SingleBand == this.Layout.ViewStyle )
				//if ( ! this.layout.ViewStyleImpl.IsMultiBandDisplay )
				//	return this.layout.SortedBands[0] != this;

                //    return this.parentBand.HiddenResolved;
                //}

                //// since we got to band 0 without hidden
                //// being set we can return false
                ////
                //return false;
                #endregion //Old Code
                //
                if (this.layout.IsCachingBandInfo)
                {
                    // The layout might be caching this property for efficiency so that we don't
                    // have to walk up the parent chain every time we need to see if a band is hidden
                    Dictionary<UltraGridBand, UltraGridLayout.CachedBandInfo> cachedInfo = this.layout.CachedBandData;                    
                    UltraGridLayout.CachedBandInfo info = null;
                    if (cachedInfo.TryGetValue(this, out info) && info.Hidden.HasValue)
                        return info.Hidden.Value;

                    // Cache the hidden data so that it can be reused later
                    info = new UltraGridLayout.CachedBandInfo();
                    info.Hidden = this.HiddenResolvedInternal;

                    // MBS 5/13/09 - TFS17523
                    // We need to check to see if we've cached this during the process, since accessing
                    // the HiddenResolvedInternal property can end up caching this value
                    //
                    //cachedInfo.Add(this, info);
                    UltraGridLayout.CachedBandInfo newInfo;
                    if (!cachedInfo.TryGetValue(this, out newInfo))
                        cachedInfo[this] = info;
                    else
                        newInfo.Hidden = info.Hidden;

                    return info.Hidden.Value;
                }
                else
                    return this.HiddenResolvedInternal;
            }
		}

		/// <summary>
        /// Number that specifies the band's position in the Bands collection..
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>Index</b> property is set by default to the order of the creation of objects in a collection. The index for the first object in a collection will always be zero.</p>
		///	<p class="body">The value of the <b>Index</b> property of an object can change when objects in the collection are reordered, such as when objects are added to or deleted from the collection. Since the <b>Index</b> property may change dynamically, it may be more useful to refer to objects in a collection by using its <b>Key</b> property.</p>
		///	<p class="body">Not all objects in a collection support an <b>Index</b> property. For certain objects, the <b>Index</b> value is essentially meaningless except as an internal placeholder within the collection. In these cases, the object will not have an <b>Index</b> property, even though it is in a collection. You can use other mechanisms, such as the <b>Key</b> property or the <code class="code">For Each...</code> loop structure, to access the objects in the collection.</p>
		///	</remarks>
        ///	<see cref="SortedIndex"/>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[ MergableProperty( false ) ]
		[ LocalizedDescription("LD_UltraGridBand_P_Index")]
        // MBS 3/12/08 - Hid from designer
        [Browsable(false)]
        public int Index
		{
			get
			{
				return this.layout.Bands.IndexOf(this);
			}
		}

		/// <summary>
		/// Returns or sets the number of levels that will be displayed for a single record.
		/// </summary>
		///	<remarks>
		///	<p class="body">Typically, each data record in a band occupies a single row of the grid, with all of the cells stretching from left to right. In some circumstances, you may want to have a single record occupy more than one row. For example, if you have address data stored in a record, you may want to have the first and last name fields on one level, the street address on a second level, and city, state and postal code fields on a third level. The <b>LevelCount</b> property is used to specify how many levels of data a band will display for each record in the data source.</p>
		///	<p class="body">Levels work in conjunction with groups to create blocks of fields within a band. If you do not have any groups specified for a band, the <b>LevelCount</b> property will have no effect. If one or more groups are specified (and column moving is enabled within the group or band) you can re-arrange fields vertically within the group by dragging their column headers to different levels.</p>
		///	<p class="body">The minimum value for this property is 1. The maximum value for this property is 50. When you specify a value greater than 1 for <b>LevelCount</b>, the control will automatically expand the band to create room for the number of levels you have requested, regardless of whether you actually have cells occupying those levels. Note that if you specify too high a number of levels, the user may initially see only column headers - the data itself may be pushed off the screen and may not be visible. Also, the amount of blank space allocated for each record may break up the visual continuity of the band and create confusion for the user.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Groups"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Columns"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.GroupHeadersVisible"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridBand_P_LevelCount")]
		[LocalizedCategory("LC_Display")]
		public int LevelCount
		{
			get
			{
				return this.levelCount;
			}
			set
			{
				if ( this.levelCount != value )
				{
					// AS - 10/29/01
					// We were not enforcing the min and max values.
					//
					if (value <= 0 || value > 50)
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_13") );

					this.levelCount   = value;
					this.NotifyPropChange( PropertyIds.LevelCount );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeLevelCount() 
		{
			return 1 != this.levelCount;
		}
 
		/// <summary>
		/// Resets LevelCount to its default value (1).
		/// </summary>
		public void ResetLevelCount() 
		{
			this.LevelCount = 1;
		}


		/// <summary>
		/// ScrollTipField specifies the key of the column contents of which UltraGrid will use to display the scroll tips.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>ScrollTipField</b> property specifies which field should be used to supply the text for the scroll tips. Scroll tips appear over the scrollbar as the user is scrolling through a recordset. They display the specified data as a navigational aid; the user can release the mouse button and the recordset will be positioned on the record containing the data indicated in the scroll tip.</p>
		///	<p class="body"><b>Note</b> that scroll tips are only displayed if <see cref="UltraGridLayout.ScrollStyle"/> is set to <b>Deferred</b>. Otherwise the rows are scrolled immediately making it unnecessary to display scroll tips.</p>
		///	<seealso cref="UltraGridOverride.TipStyleScroll"/>
		///	<seealso cref="UltraGridLayout.ScrollStyle"/>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridBand_P_ScrollTipField")]
		[ LocalizedCategory("LC_Behavior") ]
		public string ScrollTipField
		{
			get
			{
				return this.scrollTipField;
			}
			set
			{
				if ( this.scrollTipField != value )
				{
					this.scrollTipField   = value;
					this.NotifyPropChange( PropertyIds.ScrollTipField );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeScrollTipField() 
		{
			return ( null != this.scrollTipField &&
				this.scrollTipField.Length > 0 );
		}
 
		/// <summary>
		/// Resets ScrollTipField to its default value (null).
		/// </summary>
		public void ResetScrollTipField() 
		{
			this.ScrollTipField = null;
		}

		#region MergeOverrideAppearances
		
		internal void MergeOverrideAppearances( ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index )
		{
			// SSP 3/29/05 - NAS 5.2 Optimizations
			// Commented out the original code and added new code.
			//
			if ( null != this.overrideObj )
			{
				AppearanceBase app = this.overrideObj.GetAppearanceIfAllocated( index );
				if ( null != app )
				{
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//Infragistics.Win.AppearanceData.MergeAppearance( ref appData, overrideAppearance.Data, ref context.UnresolvedProps );
					app.MergeData( ref appData, ref context.UnresolvedProps );
				}
			}
			
		}

		// SSP 3/29/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Added an overload of MergeOverrideAppearances.
		//
		internal void MergeOverrideAppearances( 
			ref AppearanceData appData, 
			ref AppearancePropFlags flags,
			UltraGridOverride.OverrideAppearanceIndex index )
		{
			if ( null != this.overrideObj )
			{
				AppearanceBase app = this.overrideObj.GetAppearanceIfAllocated( index );
				if ( null != app )
					app.MergeData( ref appData, ref flags );
			}
		}

		// SSP 3/13/06 - App Styling
		// Added an overload that takes in roleState parameter.
		// 
		internal void MergeOverrideAppearances( ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index,
			AppStyling.RoleState roleState )
		{
			this.MergeOverrideAppearances( ref appData, ref context, index, context.Role, roleState );
		}

		// SSP 3/13/06 - App Styling
		// Added an overload that takes in role and roleState parameter.
		// 
		internal void MergeOverrideAppearances( ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index,
			AppStyling.UIRole role, AppStyling.RoleState roleState )
		{
			if ( context.ResolutionOrder.UseStyleBefore )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref context );

			if ( context.ResolutionOrder.UseControlInfo )
				this.MergeOverrideAppearances( ref appData, ref context, index );

			if ( context.ResolutionOrder.UseStyleAfter )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref context );
		}

		#endregion // MergeOverrideAppearances

		#region MergeBLOverrideAppearances
		
		// SSP 3/29/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Added MergeBLOverrideAppearances.
		//
		internal void MergeBLOverrideAppearances( 
			ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index )
		{
			this.MergeOverrideAppearances( ref appData, ref context, index );
			this.layout.MergeOverrideAppearances( ref appData, ref context, index );
		}

		internal void MergeBLOverrideAppearances( 
			ref AppearanceData appData, 
			ref AppearancePropFlags flags,
			UltraGridOverride.OverrideAppearanceIndex index )
		{
			this.MergeOverrideAppearances( ref appData, ref flags, index );
			this.layout.MergeOverrideAppearances( ref appData, ref flags, index );
		}

		// SSP 3/13/06 - App Styling
		// Added overload of MergeBLOverrideAppearances that takes in app styling related params.
		// 
		internal void MergeBLOverrideAppearances( 
			ref AppearanceData appData, 
			ref AppearancePropFlags flags,
			UltraGridOverride.OverrideAppearanceIndex index,
			StyleUtils.Role eRole, AppStyling.RoleState roleState )
		{
			AppStyling.ResolutionOrderInfo resOrderInfo;
			AppStyling.UIRole role = StyleUtils.GetRole( this, eRole, out resOrderInfo ); 

			this.MergeBLOverrideAppearances( ref appData, ref flags, index, role, resOrderInfo, roleState );
		}

		// SSP 3/13/06 - App Styling
		// Added overload of MergeBLOverrideAppearances that takes in app styling related params.
		// 
		internal void MergeBLOverrideAppearances( 
			ref AppearanceData appData, 
			ref AppearancePropFlags flags,
			UltraGridOverride.OverrideAppearanceIndex index,
			AppStyling.UIRole role, AppStyling.ResolutionOrderInfo resOrderInfo, AppStyling.RoleState roleState )
		{
			if ( resOrderInfo.UseStyleBefore )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref flags );

			if ( resOrderInfo.UseControlInfo )
				this.MergeBLOverrideAppearances( ref appData, ref flags, index );

			if ( resOrderInfo.UseStyleAfter )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref flags );
		}

		// SSP 3/13/06 - App Styling
		// Added overload of MergeBLOverrideAppearances that takes in app styling related params.
		// 
		internal void MergeBLOverrideAppearances( 
			ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index, 
			AppStyling.RoleState roleState )
		{
			this.MergeBLOverrideAppearances( ref appData, ref context, index, context.Role, roleState );
		}

		// SSP 3/13/06 - App Styling
		// Added overload of MergeBLOverrideAppearances that takes in app styling related params.
		// 
		internal void MergeBLOverrideAppearances( 
			ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index, 
			AppStyling.UIRole role, AppStyling.RoleState roleState )
		{
			if ( context.ResolutionOrder.UseStyleBefore )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref context );

			if ( context.ResolutionOrder.UseControlInfo )
				this.MergeBLOverrideAppearances( ref appData, ref context, index );

			if ( context.ResolutionOrder.UseStyleAfter )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref context );
		}

		#endregion // MergeOverrideAppearances

		#region HasBLOverrideAppearance

		// SSP 3/29/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Added HasBLOverrideAppearance method.
		//
		internal bool HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex index )
		{
			return null != this.overrideObj && this.overrideObj.HasAppearance( index )
				|| this.layout.HasOverrideAppearance( index );
		}

		// SSP 3/13/06 - App Styling
		// Added overload of MergeBLOverrideAppearances that takes in app styling related params.
		// 
		internal bool HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex index,
			StyleUtils.Role eRole, AppStyling.RoleState roleState )
		{
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( this, eRole, out order );

			return this.HasBLOverrideAppearance( index, role, order, roleState );
		}

		internal bool HasBLOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex index,
			AppStyling.UIRole role, AppStyling.ResolutionOrderInfo resOrderInfo, AppStyling.RoleState roleState )
		{
			if ( resOrderInfo.UseControlInfo )
			{
				if ( this.HasBLOverrideAppearance( index ) )
					return true;
			}

			if ( ( resOrderInfo.UseStyleBefore || resOrderInfo.UseStyleAfter )
				&& StyleUtils.HasAppearance( role, roleState ) )
				return true;

			return false;
		}

		#endregion // HasBLOverrideAppearance

		#region GetBLOverrideAppearanceFontHeight

		// SSP 4/6/05 - NAS 5.2 Filter Row
		// Added GetBLOverrideAppearanceFontHeight.
		//
		internal int GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex index
			// SSP 3/13/06 - App Styling
			// Added app-styling related params.
			// 
			, StyleUtils.Role eRole, AppStyling.RoleState roleState )
		{
			return this.GetBLOverrideAppearanceFontHeight( index, true, eRole, roleState );
		}

		// SSP 3/16/06 - App Styling
		// Added an overload that takes in resolveToDefaultFontHeight parameter.
		// 
		internal int GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex index,
			bool resolveToDefaultFontHeight, StyleUtils.Role eRole, AppStyling.RoleState roleState )
		{
			// SSP 3/16/06 - App Styling / Optimizations
			// For app-styling pass along the role related params. For optimization, don't
			// calculate the font height if there is are no font settings.
			// 
			// ------------------------------------------------------------------------------
						
			UltraGridLayout layout = this.Layout;
			if ( this.HasOverride || layout.HasOverride ) 
			{
				AppearancePropFlags flags = AppearancePropFlags.FontData;
				AppearanceData appData = new AppearanceData( );

				this.MergeBLOverrideAppearances( ref appData, ref flags, index, eRole, roleState );

				if ( appData.HasFontData )
				{
					// SSP 3/16/06
					// Also merge in the Layout's Appearance since that provides the defaults for the entire grid.
					// If we don't do this then if the override settings only had for example Italics set on the
					// appearance, then we will end up calculating the font height based on the base font (grid font)
					// with Italic attribute. We won't take into account the Layout.Appearance.FontData which may
					// have SizeInPoints set. For example, if RowAppearance has FontData.Italics set to True and
					// Layout.Appearance.FontData.SizeInPoints set to a big font size, then the row heights will
					// not take into account the Layout.Appearance.FontData.SizeInPoints setting and thus will be 
					// smaller than necessary.
					// Added the following line.
 					// 
					layout.MergeLayoutAppearances( ref appData, ref flags, UltraGridLayout.LayoutAppearanceIndex.Default );

					return layout.CalculateFontHeight( ref appData );
				}
			}

			return resolveToDefaultFontHeight ? layout.DefaultFontHeight : 0;
			// ------------------------------------------------------------------------------
		}

		#endregion // GetBLOverrideAppearanceFontHeight

		internal void ResolveAppearance( ref AppearanceData appData,
			ref ResolveAppearanceContext context )
		{
			// SSP 3/10/06 - App Styling
			// In the band's ResolveAppearance we should only do the Before phase of style appearance
			// resolution. The after must be done in the layout's ResolveAppearance which is what
			// we call at the end of this method. This is because the layout merges in its override
			// appearance settings.
			// 
			AppStyling.ResolutionOrderInfo origResolutionOrderInfo = context.ResolutionOrder;
			context.ResolutionOrder.UseStyleAfter = false;

			// Added support for RowSelectorAppearance property off the override object
			//
			// JJD 4/17/01
			// On the first pass resolving a rowselector's appearance the
			// object type is row so after we mrege in the row selector's appearance
			// we need to exit so we don't pick up any layout defaults before
			// we can do the 2nd pass to use header settings
			//
			if ( context.IsRowSelector )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.RowSelector 
					// SSP 3/8/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );
			}
				
			else if ( context.IsRowPreview )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.RowPreview 
					// SSP 3/8/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );
			}

				// First merge in the edit cell appearance if the cell is in edit
				//
			else if (context.IsInEdit)
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.EditCell 
					// SSP 3/8/06 - App Styling
					// 
					, AppStyling.RoleState.EditMode );
			}
			
			else if (context.IsActiveCell)
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveCell 
					// SSP 3/8/06 - App Styling
					// 
					, AppStyling.RoleState.Active );

				// Only merge the active row appearance if that flag is set.
				// This can happen in a cell appearance only pass
				//
				if (context.IsActiveRow)
				{
					// SSP 10/19/07 BR25706 BR25707
					// Added ActiveRowCellAppearance property.
					// 
					this.MergeOverrideAppearances( ref appData, ref context, 
						UltraGridOverride.OverrideAppearanceIndex.ActiveRowCellAppearance );

					// SSP 3/13/06 - App Styling
					// Note: Here we need to resolve on the row role.
					// 
					//this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveRow );
					AppStyling.UIRole rowRole = StyleUtils.GetRole( this, StyleUtils.Role.Row );
					this.MergeOverrideAppearances( ref appData, ref context, 
						UltraGridOverride.OverrideAppearanceIndex.ActiveRow, rowRole, AppStyling.RoleState.Active );
				}
			}
			
			else if (context.IsActiveRow)
			{
				// JJD 1/10/02
				// Use the active card caption appearance if type is CardCaptionUIElement
				//
				if (typeof(Infragistics.Win.UltraWinGrid.CardCaptionUIElement) == context.ObjectType)
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveCardCaption 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Active );
				}
				else
				{
					// SSP 10/19/07 BR25706 BR25707
					// Added ActiveRowCellAppearance property.
					// 
					if ( typeof( UltraGridCell ) == context.ObjectType )
						this.MergeOverrideAppearances( ref appData, ref context,
							UltraGridOverride.OverrideAppearanceIndex.ActiveRowCellAppearance );

					// This is the active row pass so merge the 'active row' appearance 
					//
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveRow
						// SSP 5/17/06 BR12645
						// We should be using the row role here.
						// 
						, StyleUtils.GetRole( this, StyleUtils.Role.Row ) 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Active );
				}
			}
			
			else if (context.IsSelected)
			{
				// JJD 1/10/02
				// Use the selected card caption appearance if type is CardCaptionUIElement
				//
				if (typeof(Infragistics.Win.UltraWinGrid.CardCaptionUIElement) == context.ObjectType)
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedCardCaption 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Selected );
				}
				else
				{
					// This is the selected pass so merge the selected appearances 
					//
					if (typeof(Infragistics.Win.UltraWinGrid.UltraGridCell) == context.ObjectType)
					{
						this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedCell 
							// SSP 3/13/06 - App Styling
							// 
							, AppStyling.RoleState.Selected );
					}

					// SSP 3/13/06 - App Styling
					// We need to resolve on the row role.
					// 
					//this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedRow );
					
					
					
					AppStyling.UIRole rowRole = typeof( UltraGridGroupByRow ) == context.ObjectType 
						? context.Role : StyleUtils.GetRole( this, StyleUtils.Role.Row );
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedRow, 
						rowRole, AppStyling.RoleState.Selected );
				}
			}

			else if ( context.IsGroupByColumn )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.GroupByColumn 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.GroupByColumn );
			}

			else if ( context.IsGroupByColumnHeader )
			{
				// This is the group by column pass so merge the group by column appearances 
				//
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.GroupByColumnHeader
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.GroupByColumn );
			}

				// SSP 7/25/03 - Fixed headers.
				// Added the else if blocks for IsFixedHeader and IsFixedColumn.
				//
				// ----------------------------------------------------------------------------------------------
			else if ( context.IsFixedHeader )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.FixedHeaderAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.FixedColumn );
			}

			else if ( context.IsFixedColumn )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.FixedCellAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.FixedColumn );
			}
				// ----------------------------------------------------------------------------------------------
				// SSP 7/24/04 - UltraCalc
				// Resolve CellErrorAppearance.
				//
				// ----------------------------------------------------------------------
			else if ( context.IsFormulaError )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.FormulaErrorAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.FormulaError );
			}
				// ----------------------------------------------------------------------
			else
			{
				if (typeof(Infragistics.Win.UltraWinGrid.HeaderBase) == context.ObjectType)
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Header 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Normal );
				}
                
				else if (typeof(Infragistics.Win.UltraWinGrid.UltraGridCell) == context.ObjectType)
				{
					// merge in cell appearance properties
					//
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Cell 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Normal );

					if ( !context.IsCellOnly )
					{
						// SSP 3/8/06 - App Styling
						// Note: we need to resolve on the row role here.
						// 
						AppStyling.UIRole rowRole = StyleUtils.GetRole( this, StyleUtils.Role.Row );

						//merge in row appearance properties as well
						if (context.IsAlternate)
						{
							this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.RowAlternate 
								// SSP 3/13/06 - App Styling
								// 
								, rowRole, AppStyling.RoleState.AlternateItem );
						}
						else
						{
							this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Row 
								// SSP 3/13/06 - App Styling
								// 
								, rowRole, AppStyling.RoleState.Normal );
						}
					}
				}
                
				else if (typeof(Infragistics.Win.UltraWinGrid.UltraGridRow) == context.ObjectType)
				{
					// If the alternate flag is set only merge in the alternate 
					// row appearance
					//
					if (context.IsAlternate)
					{
						this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.RowAlternate 
							// SSP 3/13/06 - App Styling
							// 
							, AppStyling.RoleState.AlternateItem );
					}
					else
					{
						this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Row 
							// SSP 3/13/06 - App Styling
							// 
							, AppStyling.RoleState.Normal );
					}
				}
				else if (typeof(Infragistics.Win.UltraWinGrid.CardAreaUIElement) == context.ObjectType)
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.CardArea 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Normal );
				}
				else if (typeof(Infragistics.Win.UltraWinGrid.CardCaptionUIElement) == context.ObjectType)
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.CardCaption 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Normal );
				}
					// SSP 5/20/02
					// Summary rows feature. Added code for resolving summary value and summary
					// footer appearances.
					// Using SummaryFooterUIElement since there is no summary footer object and
					// for the sake of consistency, I am also using the SummaryValueUIElement instead
					// of SummaryValue object.
					//
					// ---------------------------------------------------------------------------
				else if ( context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.SummaryFooterUIElement ) )
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SummaryFooterAppearance
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Normal );
				}
				else if ( context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.SummaryFooterCaptionUIElement ) )
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SummaryFooterCaptionAppearance 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Normal );
				}
				else if ( context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.SummaryValueUIElement ) )
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SummaryValueAppearance 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Normal );
				}
				// ---------------------------------------------------------------------------------------

			}
			// SSP 3/10/06 - App Styling
			// Since we already did the Before phase of style appearance resolution above, in the 
			// layout's ResolveAppearance we only need to do the after phase. Also restore the 
			// ResolutionOrder to the original as we had modified it in the beginning of this 
			// method.
			// 
			context.ResolutionOrder = origResolutionOrderInfo;
			context.ResolutionOrder.UseStyleBefore = false;

			// if we have all the appearance properties requested we can
			// exit here
			//
			if ( 0 != context.UnresolvedProps )
			{
				// Otherwise, call to layout to get the remaining properties
				//
				this.layout.ResolveAppearance( ref appData, ref context );
			}

			// SSP 3/10/06 - App Styling
			// Restore the ResolutionOrder to the original as we had modified it above.
			// 
			context.ResolutionOrder = origResolutionOrderInfo;
		}

		//JDN 11/19/04 Added RowSelectorHeader
		#region ResolveRowSelectorHeaderAppearance
		// SSP 7/20/05 - NAS 5.3 Column Chooser / HeaderStyle of WindowsXPCommand / Optimizations
		// Added defaultToHeaderAppearance and resolveDefaultColors parameters. Commented out the
		// original method and added new code.
		// 
		internal void ResolveRowSelectorHeaderAppearance( ref AppearanceData appData, 
			ref AppearancePropFlags flags, bool defaultToHeaderAppearance, bool resolveDefaultColors )
		{
			// SSP 3/13/06 - App Styling
			// 
			AppStyling.ResolutionOrderInfo resOrderInfo;
			AppStyling.UIRole role = StyleUtils.GetRole( this, StyleUtils.Role.RowSelectorHeader, out resOrderInfo );

			this.MergeBLOverrideAppearances( ref appData, ref flags, 
				UltraGridOverride.OverrideAppearanceIndex.RowSelectorHeaderAppearance 
				// SSP 3/13/06 - App Styling
				// Use the new overload that takes in the app styling related params.
				// 
				, role, resOrderInfo, AppStyling.RoleState.Normal );

			if ( defaultToHeaderAppearance )
			{
				ResolveAppearanceContext resolveAppearanceContext = 
					new ResolveAppearanceContext( typeof(HeaderBase), flags );

				// SSP 3/13/06 - App Styling
				// 
				resolveAppearanceContext.Role = role;
				resolveAppearanceContext.ResolutionOrder = resOrderInfo;

				resolveAppearanceContext.ResolveDefaultColors = resolveDefaultColors;

				this.ResolveAppearance( ref appData, ref resolveAppearanceContext ); 

				flags = resolveAppearanceContext.UnresolvedProps;
			}
		}

		

		#endregion // ResolveRowSelectorHeaderAppearance

		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{

			if ( this.layout == null )
				return;

			// SSP 8/12/03 - Optimizations
			//
			this.layout.BumpGrandVerifyVersion( );
			
			if ( propChange.Source == this.overrideObj )
			{
				if ( propChange.PropId is PropertyIds )
				{
					switch ( (PropertyIds)propChange.PropId )
					{
						case PropertyIds.HeaderAppearance:
						case PropertyIds.GroupByColumnHeaderAppearance:
						{
							this.isColHdrFontHeightCached   = false;
							this.isGroupHdrFontHeightCached = false;
							this.isCardLabelFontHeightCached = false;
							this.isColumnCardFontCacheDirty = true;
							break;
						}
						case PropertyIds.CardCaptionAppearance:
						case PropertyIds.ActiveCardCaptionAppearance:
						case PropertyIds.SelectedCardCaptionAppearance:
						{
							this.isCardCaptionFontHeightCached   = false;
							break;
						}
						case PropertyIds.CellPadding:
						case PropertyIds.CellSpacing:
						{	
							this.isMinRowHeightCached = false;
							break;
						}
                        // CDS NAS v9.1 Header CheckBox
                        case PropertyIds.HeaderCheckBoxAlignment:
                        {
                            StyleUtils.DirtyCachedPropertyVals(this);
                            // CDS 7/02/09 TFS19043 Dirty the RowLayoutCachedInfo to recalculate the header height
                            this.DirtyRowLayoutCachedInfo();
                            // CDS 7/07/09 TFS17996
                            // Location of the header checkbox may change, so dirty the cached ContainsCheckboxOnTopOrBottom value.
                            this.DirtyCachedContainsCheckboxOnTopOrBottom();
                            break;
                        }
                        // CDS 7/02/09 TFS19043 Dirty the RowLayoutCachedInfo to recalculate the header height
                        case PropertyIds.HeaderCheckBoxVisibility:
                        {
                            this.DirtyRowLayoutCachedInfo();

                            // CDS 7/07/09 TFS17996
                            // Visibility of the header checkbox may change, so dirty the cached ContainsCheckboxOnTopOrBottom value.
                            this.DirtyCachedContainsCheckboxOnTopOrBottom();

                            break;
                        }

					}
				}

				// Moved logic into separate OnOverrideChanged method
				// which can be called from the band's OnDataChanged as
				// well
				//
				this.layout.OnOverrideChanged ( propChange, this );
				
				this.NotifyPropChange( PropertyIds.Override, propChange );
				return;
			}

			else if ( propChange.Source == this.columns )
			{
				if ( propChange.PropId is PropertyIds )
				{
					switch ( (PropertyIds)propChange.PropId )
					{
						case PropertyIds.Add:
						case PropertyIds.Remove:
						case PropertyIds.ClearUnbound:
						{
							// Check the flag that lets us know if we are re-initializing
							// all sub items of the layout.
							//
							if ( !this.layout.InitializingAllSubItems )
								this.OnColumnsChanged();

							// Set the synchronized col list dirty flag on.
							//
							this.layout.SynchronizedColListsDirty = true;

							// Check the flag that lets us know if we are re-initializing
							// all sub items of the layout.
							//
							if ( !this.layout.InitializingAllSubItems )
							{
								this.layout.DirtyGridElement( false );

								if ( null != this.layout.GetColScrollRegions( false ) )
									// SSP 7/24/03
									// Don't pass in true as the first parameter because that will cause initialization
									// of the metrics and possibly lead to premature verification of some dirty state.
									//
									//this.layout.GetColScrollRegions( true ).DirtyMetrics();
									this.layout.GetColScrollRegions( false ).DirtyMetrics();
							}

							// SSP 8/27/02 UWG1611
							// Bump the child cache version if a column has been removed
							// or added.
							//
							if ( null != this.layout )
								this.layout.BumpCellChildElementsCacheVersion( );

							// SSP 2/4/03 - Row Layout Functionality
							// Added case for RowLayoutColumnInfo.
							//
							this.DirtyRowLayoutCachedInfo( );

							// SSP 7/24/03 - Fixed headers
							//
							this.BumpFixedHeadersVerifyVersion( );

                            // CDS 7/07/09 TFS17996
                            // Column may have been added or deleted, so dirty the cached ContainsCheckboxOnTopOrBottom value.
                            this.DirtyCachedContainsCheckboxOnTopOrBottom();

							break;
						}

						default:
						{
							// Dirty the metrics if certain column prperties are changed
							// 

							bool primaryMetricsDirty = false;

							// SSP 11/22/04 - Merged Cell Feature
							// Added these vars as well.
							//
							bool bumpCellElementsVersion = false;
							bool bumpGrandVerifyVersion = false;

							if( null != propChange.Trigger &&
								propChange.Trigger.PropId is PropertyIds )
							{
								switch( (PropertyIds)propChange.Trigger.PropId )
								{
									// MD 12/9/08 - 9.1 - Column Pinning Right
									case PropertyIds.FixOnRight:

									case PropertyIds.ColSpan:
									case PropertyIds.Width: //Added ***********
									case PropertyIds.Hidden:
									case PropertyIds.HiddenWhenGroupBy: // JJD 1/07/02 added case
									case PropertyIds.Columns:
									case PropertyIds.VisiblePosition:

										// SSP 11/6/01
										// When IsGroupByColumn property off a column changes
										// HiddenResolved off the column may return true or
										// false depending on the value of IsGroupByColumn.
										// So we need to dirty the metrics and synchronized 
										// col list because that column may show up or get hidden
										//
									case PropertyIds.IsGroupByColumn:
										//case PropertyIds.LevelCount:
										// SSP 7/21/03 - Fixed headers
										// Added cases for Fixed and Group properties.
										//
									case PropertyIds.Group:
									case PropertyIds.Fixed:
									case PropertyIds.Level:
									{	
										// Mark the col sync lists dirty so they get recreated
										// on the next band metrics calculation
										//
										if ( null != this.layout )
										{
											this.layout.SynchronizedColListsDirty = true;
											this.layout.GetColScrollRegions( false ).DirtyMetrics();
										}
										this.ClearCachedHeightValues();

										// SSP 4/1/02 UWG1060
										// Bump the major row height version whenever a column's
										// width changes because this may have effect on the row's
										// height.
										//
										this.BumpMajorRowHeightVersion( );


										// SSP 7/19/02 UWG1400
										// Also bump the cell child elements cache version because
										// change in the position of the column could potentially
										// lead to change in the border sides of the cells (like
										// when the border styles are single line, the first column's
										// cell's borders are merged with the row selector. When this
										// column's visible position changes, the border sides won't 
										// be merged anymore however the cell's children won't be 
										// repositioned to account for this border showing up all of
										// a sudden (RectInsideBorders of the cell elements change).
										// Added call to BumpCellChildElementsCacheVersion method so
										// the child elements of cells get repositioned.
										//
										if ( null != this.layout )
											this.layout.BumpCellChildElementsCacheVersion( );

										// SSP 2/4/03 - Row Layout Functionality
										//
										this.DirtyRowLayoutCachedInfo( );

										// SSP 7/21/03 - Fixed headers
										//
										this.BumpFixedHeadersVerifyVersion( PropertyIds.Fixed == (PropertyIds)propChange.Trigger.PropId );

										// SSP 7/29/05 - NAS 5.3 Tab Index
										// 
										this.DirtyTabOrderedColumns( );

                                        // CDS 7/07/09 TFS17996
                                        PropertyIds propId = (PropertyIds)propChange.Trigger.PropId;
                                        if (propId == PropertyIds.Hidden ||
                                            propId == PropertyIds.HiddenWhenGroupBy)
                                            this.DirtyCachedContainsCheckboxOnTopOrBottom();

										break;
									}

										// If buttondisplaystyle changes we need to dirty the column
									case PropertyIds.ButtonDisplayStyle:
										// SSP 12/5/01 UWG860
										// Added ValueList clause.
										//
									case PropertyIds.ValueList:
									case PropertyIds.Style:
									{
										// SSP 12/3/01 UWG805
										// Bump the cell cache version so that cell elements
										// reprosition their children.
										//
										this.Layout.BumpCellChildElementsCacheVersion( );
										
										primaryMetricsDirty = true;
                                        this.ClearCachedHeightValues();
                                        
                                        // CDS 04/24/09 TFS124522 
                                        // Bump the HeaderCheckBoxSynchronizationVersion to make sure the Header's Checkbox is re-synchronized
                                        this.BumpHeaderCheckBoxSynchronizationVersion();

                                        // CDS 7/07/09 TFS17996
                                        this.DirtyCachedContainsCheckboxOnTopOrBottom();

                                        break;
									}
										// SSP 5/12/03 - Optimizations
										// Added a way to just draw the text without having to embedd an embeddable ui element in
										// cells to speed up rendering.
										//
									case PropertyIds.CellDisplayStyle:
									{
										if ( null != this.Layout )
										{
											this.Layout.BumpCellChildElementsCacheVersion( );
											this.Layout.DirtyGridElement( false, true );
										}
										break;
									}
										// SSP 6/26/03 UWG2367
										// If the the a Column's Case property is set, then dirty the ui element
										// and also bump the cell version number so the cell's reposition themselves
										// in case some editor is relying on the PositionChildElement for updating
										// the case of the text.
										//
										// SSP 12/11/03 UWG2781
										// Added UseEditorMaskSettings property.
										//
									case PropertyIds.UseEditorMaskSettings:
										// SSP 12/11/03 UWG2783
										// Added a case for CellMultiLine.
										//
										// SSP 12/12/03 DNF135
										// Added a way to control whether ink buttons get shown.
										// Added ShowInkButton property.
										//
									case PropertyIds.ShowInkButton:
									case PropertyIds.CellMultiLine:
									case PropertyIds.Case:
										// MRS 4/7/05 - Added CharacterCasing
									case PropertyIds.CharacterCasing:
										// SSP 3/26/04 UWG2993
										// Added a case for AllowRowFiltering.
										// 
									case PropertyIds.AllowRowFiltering:
                                        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
                                    case PropertyIds.ActiveAppearancesEnabled:
                                    case PropertyIds.SelectedAppearancesEnabled:
										bumpGrandVerifyVersion = true;
										break;
										// SSP 11/22/04 - Merged Cell Feature
										//
									case PropertyIds.MergedCellAppearance:
									case PropertyIds.MergedCellContentArea:
									case PropertyIds.MergedCellEvaluator:
									case PropertyIds.MergedCellStyle:
									case PropertyIds.MergedCellEvaluationType:
									{
										this.BumpMergedCellVersion( );
										break;
									}
										// SSP 4/23/05 - NAS 5.2 Filter Row
										//
									case PropertyIds.FilterOperandStyle:
									case PropertyIds.FilterOperatorDefaultValue:
									case PropertyIds.FilterOperatorDropDownItems:
									case PropertyIds.FilterOperatorLocation:
									{
										this.layout.BumpVerifyEditorVersionNumber( this );
										break;
									}

										// JAS v5.2 GroupBy Break Behavior 5/3/05
										//
									case PropertyIds.GroupByMode:
									{
										this.BumpGroupByHierarchyVersion();
										if( this.layout != null )
											this.layout.DirtyGridElement( true );
										break;
									}

									case PropertyIds.SortComparisonType:
									{
										if( this.HasSortedColumns )
										{
											this.SortedColumns.BumpSortVersion();											
											primaryMetricsDirty = true;

											// JAS 5/17/05 BR04005 - If we are in GroupBy mode
											// then the groups might need to be rebuilt.
											//
											this.BumpGroupByHierarchyVersion();
										}
										break;
                                    }
                                    case PropertyIds.Editor:
                                    {
                                        // CDS 04/24/09 TFS124522 
                                        // Bump the HeaderCheckBoxSynchronizationVersion to make sure the Header's Checkbox is re-synchronized
                                        this.BumpHeaderCheckBoxSynchronizationVersion();

                                        // CDS 7/07/09 TFS17996
                                        this.DirtyCachedContainsCheckboxOnTopOrBottom();

                                        break;
                                    }
                                    // CDS 7/07/09 TFS17996
                                    case PropertyIds.Header:
                                    {
                                        PropertyIds propId = (propChange.Trigger.Trigger != null && propChange.Trigger.Trigger.PropId is PropertyIds) ? (PropertyIds)propChange.Trigger.Trigger.PropId : 0;
                                        if (propId == PropertyIds.HeaderCheckBoxAlignment ||
                                            propId == PropertyIds.HeaderCheckBoxVisibility)
                                            this.DirtyCachedContainsCheckboxOnTopOrBottom();
                                        break;
                                    }
								}
							}

							if ( null != this.layout )
							{
								this.layout.DirtyGridElement( primaryMetricsDirty );

								// SSP 11/22/04 - Merged Cell Feature
								// Added these vars as well.
								//
								if ( bumpGrandVerifyVersion )
									this.layout.BumpGrandVerifyVersion( );
								if ( bumpCellElementsVersion )
									this.layout.BumpCellChildElementsCacheVersion( );
							}
							
							break;
						}
					}
				}
				else
				{
					this.layout.DirtyGridElement( false );
				}
				
				this.NotifyPropChange( PropertyIds.Columns, propChange );
				return;
			}

			else if ( propChange.Source == this.groups )
			{
				// SSP 2/1/02 UWG1026
				// Clear the cached row heights because when switching 
				// from groups to non-groups, we need to clear the 
				// cached font heights so that they get recalculated.
				//
				this.ClearCachedHeightValues( );

				this.layout.SynchronizedColListsDirty = true;
				this.layout.DirtyGridElement( false );

				if ( this.layout.GetColScrollRegions( false ) != null )
					this.layout.GetColScrollRegions( false ).DirtyMetrics();


                switch ((PropertyIds)propChange.PropId)
                {
                    case PropertyIds.Add:
                    case PropertyIds.Remove:
                    case PropertyIds.Group: // MRS 2/18/2009 - TFS14007
                        // MRS - NAS 9.1 - Groups in RowLayout
                        this.DirtyRowLayoutCachedInfo();
                        break;
                }

				// SSP 7/24/03 - Fixed headers
				// Dirty the cached fixed headers info.
				//
				this.BumpFixedHeadersVerifyVersion( );

				this.NotifyPropChange( PropertyIds.Groups, propChange );

				// SSP 7/29/05 - NAS 5.3 Tab Index
				// 
				this.DirtyTabOrderedColumns( );

				return;
			}

				// JJD 8/28/01
				// Added logic to deal with band header change notifications
				//
			else if ( propChange.Source == this.header )
			{
				// MD 4/17/08 - 8.2 - Rotated Column Headers
				// If the caption changes and the text is rotated, the band header height may change, so we need to recalculate
				// the header height.
				//this.layout.DirtyGridElement( false );
				if ( propChange.PropId is PropertyIds &&
					(PropertyIds)propChange.PropId == PropertyIds.Caption &&
					this.header.TextOrientationResolved != null &&
					Object.Equals( this.header.TextOrientationResolved, TextOrientationInfo.Horizontal ) == false )
				{
					this.layout.ClearCachedCaptionHeight();
					this.layout.DirtyGridElement( true );
				}
				else
				{
                    // CDS 7/07/09 TFS17996
                    PropertyIds propId = (propChange.PropId is PropertyIds) ? (PropertyIds)propChange.PropId : 0;
                    if (propId == PropertyIds.HeaderCheckBoxVisibility ||
                        propId == PropertyIds.HeaderCheckBoxAlignment)
                        this.DirtyCachedContainsCheckboxOnTopOrBottom();

                    this.layout.DirtyGridElement( false );
				}

				this.NotifyPropChange( PropertyIds.Header, propChange );
				return;
			}
				// JJD 12/13/01
				// Added logic to deal with card settings
				//
			else if ( propChange.Source == this.cardSettings )
			{
				this.layout.DirtyGridElement( false );

				if ( this.layout.GetColScrollRegions( false ) != null )
					this.layout.GetColScrollRegions( false ).DirtyMetrics();

				// SSP 2/27/03 - Row Layout Functionality
				//
				if ( this.CardView )
					this.DirtyRowLayoutCachedInfo( );

				// SSP 5/5/06 - App Styling
				// 
				StyleUtils.DirtyCachedPropertyVals( this.layout );

				this.NotifyPropChange( PropertyIds.CardSettings, propChange );
				return;
			}
				// SSP 3/21/02
				// Version 2 Row Filter Implementation
				// Added below if statement to check for any change in the row filters
				//
			else if ( null != this.columnFilters && propChange.Source == this.columnFilters )
			{
				// SSP 5/13/04 UWG3260 - Virtual Binding
				// Combined the verified band and rows version numbers into one.
				// We only want to bump the filters version number on the band if 
				// band's column filters are going to be used for filtering rows.
				// Moved this into the if block below. 
				//
				//this.BumpRowFiltersVersion( );

				// Dirty row counts.
				// 
				if ( RowFilterMode.AllRowsInBand == this.RowFilterModeResolved )
				{
					// SSP 5/13/04 UWG3260 - Virtual Binding
					// Moved this here from before the if condition.
					//
					this.BumpRowFiltersVersion( );

					this.Layout.BumpScrollableRowCountVersion( );
					// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
					//
					//this.Layout.BumpScrollRowPositionVersion( );
					//((IScrollableRowCountManagerOwner)this.Layout).ScrollableRowCountManager.DirtyRowCount( );

					this.Layout.RowScrollRegions.DirtyAllVisibleRows( );
					this.Layout.DirtyGridElement( );
				}

				// SSP 4/8/03 UWG2008
				// Propagate the message up the chain.
				//
				this.NotifyPropChange( PropertyIds.ColumnFilters );

				return;
			}
				// SSP 6/4/02
				// Summary rows feature.
				//
			else if ( null != this.summaries && propChange.Source == this.summaries )
			{
				// If summaries change (either new ones are added or existing ones are deleted
				// then dirty the grid element and the visible rows (we need to regenerate
				// the visible rows because that may have effected the height of summary
				// footers.
				//
				this.Layout.RowScrollRegions.DirtyAllVisibleRows( );
				this.Layout.DirtyGridElement( );

				// SSP 2/27/03 - Row Layout Functionality
				// Dirty the row layout info so the summary layout gets recalculated.
				//
				this.DirtyRowLayoutCachedInfo( );

				// SSP 4/8/03 UWG2008
				// Propagate the message up the chain.
				//
				this.NotifyPropChange( PropertyIds.Summaries );

				return;
			}

            // MRS 12/5/2008 - TFS11243
            else if (null != this.rowLayouts && propChange.Source == this.RowLayouts)
            {
                this.NotifyPropChange(PropertyIds.Band, propChange);
                return;
            }

			Debug.Assert( false, "Unknown sub object in Band.OnSubObjectPropChanged" );
		}

		private void OnColumnsChanged()
		{
			// SSP 7/9/04 UWG3499
			// Set the orderedColumnsDirty to true so the OrderedHeaders collection gets
			// regenerated.
			//
			this.orderedColumnsDirty = true;

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			this.DirtyTabOrderedColumns( );

			// AS - 10/31/01
			// We do not want to continue if this is not a print
			// or display layout. i.e. if this is an in-memory layout
			//
			// JJD 10/17/01
			// If we are firing InitializeLayout ignore this
			// and return
			//
			//if ( this.Layout.Grid.InInitializeLayout )
			// SSP 5/7/03 - Export Functionality
			//
			//if ( (!this.Layout.IsDisplayLayout && !this.Layout.IsPrintLayout) || this.Layout.Grid.InInitializeLayout )
			if ( (!this.Layout.IsDisplayLayout && !this.Layout.IsPrintLayout && !this.Layout.IsExportLayout ) || this.Layout.Grid.InInitializeLayout )
			{
				// SSP 3/31/05 BR03055
				// Set fireInitializeRowsOnAllRow so when we sync the rows we fire InitializeRow
				// on all rows intead of just the new ones. This is necessary when the row collection
				// has been already loaded and contains rows.
				//
				if ( this.Layout.IsRowsCollectionAllocated )
					this.Layout.Rows.fireInitializeRowsOnAllRow = true;

				return;
			}

			// SSP 8/2/02 UWG1481
			// Verify that the sorted columns collection doesn't contain any columns that have
			// been removed.
			//
			if ( null != this.sortedColumns )
				this.sortedColumns.VerifyColumns( );

			//RobA 10/11/01 implemented
			//
			// SSP 4/30/04 - Virtual Binding
			// Only process the rows that are created so far.
			//
			// ------------------------------------------------------------------------
			//for ( int i=0; i < this.Layout.Rows.Count; ++i )
			//	this.layout.Rows[i].OnColumnsChanged();
			foreach ( UltraGridRow row in this.Layout.Rows.NonNullItems )
				row.OnColumnsChanged( );
			// ------------------------------------------------------------------------
		}
	
		internal int ActualColDisplayLevels
		{
			get
			{
				// MD 1/20/09 - Groups in RowLayouts
				// Levels are not used with GroupLayout style.
				//return this.GroupsDisplayed ? this.levelCount : 1;
				return ( this.GroupsDisplayed && this.RowLayoutStyle != RowLayoutStyle.GroupLayout ) 
					? this.levelCount 
					: 1;
			}
		}

		internal int GetTotalHeaderHeight ( )
		{
			return this.GetTotalHeaderHeight( true );
		}

		internal int GetTotalHeaderHeight ( bool includeOverlappedBorders  )
		{
            // MRS 7/15/2009 - TFS19376
            //// MRS - NAS 9.1 - Groups in RowLayout
            ////int totalHeight = this.GetColumnHeaderHeight ( includeOverlappedBorders ) +
            ////    this.GroupHeaderHeight + this.Header.GetBandHeaderHeight();            
            //int totalHeight = this.GetColumnHeaderHeight(includeOverlappedBorders) +
            //    this.Header.GetBandHeaderHeight();
            int totalHeight = this.Header.GetBandHeaderHeight();
            if (!this.UseRowLayoutResolved)
            {
                totalHeight += this.GetColumnHeaderHeight(includeOverlappedBorders);
            }
            else if (this.ColHeadersVisibleInSeparateAreaResolved ||
                    this.GroupHeadersVisibleInSeparateAreaResolved)
            {
                totalHeight += this.PreferredHeaderLayoutSize.Height;
            }            

            // MRS - NAS 9.1 - Groups in RowLayout
            if (false == this.CardView)
            {
                switch (this.RowLayoutStyle)
                {
                    case RowLayoutStyle.ColumnLayout:
                    case RowLayoutStyle.GroupLayout:
                        // Do not add the group height in here. We don't need it, since the 
                        // headers are laid out by the GridBagLayoutManager.
                        break;
                    case RowLayoutStyle.None:
                    default:
                        Debug.Assert(this.RowLayoutStyle == RowLayoutStyle.None, "Unknown RowLayoutStyle");
                        totalHeight += this.GroupHeaderHeight;
                        break;
                }                
            }


			// SSP 5/2/05 - NAS 5.2 Fixed Rows
			// Special row separators functionality. If we are displaying a separator 
			// after the headers then add that to the height.
			//
			if ( totalHeight > 0 && this.HasSpecialRowSeparatorAfterHeaders )
				totalHeight += this.SpecialRowSeparatorHeightResolved;

			return totalHeight;
		}

		#region HasSpecialRowSeparatorAfterHeaders

		// SSP 5/2/05 - NAS 5.2 Fixed Rows
		// Special row separators functionality. Added HasSpecialRowSeparatorAfterHeaders proeprty.
		//
		internal bool HasSpecialRowSeparatorAfterHeaders
		{
			get
			{
				return 0 != ( SpecialRowSeparator.Headers & this.GetSpecialRowSeparatorResolved( null ) );
			}
		}

		#endregion // HasSpecialRowSeparatorAfterHeaders

		// SSP 7/21/03 Workaround for UWG1558
		// Added an overload of GetColumnHeaderHeightthat ignores whether the header is
		// visible or not and returns a valid height. By default, it returns 0 if the
		// header is hidden but we want it to return a valid value.
		//
		internal int GroupHeaderHeight
		{		
			get
			{
				return this.GetGroupHeaderHeight( false );
			}
		}

		internal int GetGroupHeaderHeight( bool ignoreHidden )
		{		
			// JJD 12/26/01
			// We never show column headers in card view so return 0
			//
            // MRS - NAS 9.1 - Groups in RowLayout
            // The call to GetVisibleGroupsList below will take care of this. 
            //
            //if ( this.CardView )            
            //    return 0;
			
			// Get Group object vector and return 0 for group area height
			// if we get a null group vector
			//
			//const GROUP_VECTOR* pGroupVec = GetGroups( true );
			// SSP 7/24/03 - Optimizations
			// If the groups hasn't been allocated, then don't cause groups to be allocated
			// by accessing Groups property.
			//
			//GroupsCollection groupsColl = this.Groups;
			// SSP 3/4/04
			// Changed the behavior to give row-layout higher priority than the groups.
			// Meaning if there are groups and they turn set UseRowLayout to true, we
			// will ignore groups and use the row-layout functionality. So instead of
			// using HasGroups, use GroupsDisplayed because that's the final say in
			// whether groups are being displayed or not.
			//
			//GroupsCollection groupsColl = this.HasGroups ? this.Groups : null;
            // MRS - NAS 9.1 - Groups in RowLayout
            //GroupsCollection groupsColl = this.GroupsDisplayed ? this.Groups : null;
            //if ( null == groupsColl )
            //    return 0;
            //
            List<UltraGridGroup> groupsColl = this.GetVisibleGroupsList();
            if (null == groupsColl)
                return 0;

			// if the column headres aren't visible (or there are none defined)
			//			
			// SSP 7/21/03 Workaround for UWG1558
			// Added an overload of GetColumnHeaderHeightthat ignores whether the header is
			// visible or not and returns a valid height. By default, it returns 0 if the
			// header is hidden but we want it to return a valid value.
			// Made GroupHeaderHeight into a method that takes in a ignoreHidden parameter.
			//
			//if ( !this.groupHeadersVisible || 0 == groupsColl.Count )
			if ( ! ignoreHidden && !this.groupHeadersVisible || 0 == groupsColl.Count )            
				return 0;

			// if we haven't cached the header font height yet then do it
			// now
			//
			if ( !this.isGroupHdrFontHeightCached )
			{
				int groupsFontHeight = 0;

				// see if any of the groups have overridden the font 
				// and get back the height of the highest font 
				//
				if ( null != this.groups )
					groupsFontHeight = this.groups.MaxHeaderFontHeight;

				// get the height of the default header font for this band
				//
				// SSP 3/16/06 - App Styling
				// Use the band's GetBLOverrideAppearanceFontHeight method which takes in as params
				// the app-style role related info.
				// 
				//int defFontHeight = this.DefaultHeaderFontHeight;
				int defFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.Header, StyleUtils.Role.GroupHeader, AppStyling.RoleState.Normal );

				// cache the higher of the 2 heights we calculated
				//				
				this.cachedGroupHdrFontHeight = System.Math.Max( defFontHeight, groupsFontHeight );

				// set the cached flag so we know we don't have to recalc it
				// next time
				//				
				this.isGroupHdrFontHeightCached = true;
			}

			// use the cached font height and the number of header lines to calculate
			// and return the height of the header
			//

			// NickC 9/26/00 : (Picture+font)*headerlines needed to be replaced with 
			//                  max(Picture,font*headerlines) as height calc for header
			int headerHeight = this.cachedGroupHdrFontHeight * this.GroupHeaderLines; //m_state.m_nGroupHeaderLines;

			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Make sure there is enough height for each group with a visible header to display its text, 
			// which may be rotated.
			foreach ( UltraGridGroup group in groupsColl )
			{
                // MRS 2/23/2009 - Found while fixing TFS14354
				//if ( group.Hidden )
                if (group.HiddenResolved)
					continue;

				if ( group.Header.Hidden )
					continue;

				TextOrientationInfo headerCaptionOrientation = group.Header.TextOrientationResolved;

				// Don't measure the text if is it not rotated.
				if ( headerCaptionOrientation == null ||
					Object.Equals( headerCaptionOrientation, TextOrientationInfo.Horizontal ) )
					continue;

				AppearanceData appearanceData = new AppearanceData();
				group.Header.ResolveAppearance( ref appearanceData, AppearancePropFlags.FontData );

				int headerHeightNeeded = this.layout.CalculateFontSize(
					ref appearanceData, 
					group.Header.Caption, 
					-1,
					headerCaptionOrientation ).Height;

				// Make sure there is enough height for this group's caption to display
				headerHeight = Math.Max( headerHeight, headerHeightNeeded );
			}

			// MD 7/26/07 - 7.3 Performance
			// MaxHeaderPictureHeight always returns 0 and was removed
			//if ( null != this.groups )
			//    headerHeight = System.Math.Max ( headerHeight, this.groups.MaxHeaderPictureHeight );

			// JAS 4/27/05 - Use the HeaderBorderThickness property in case the HeaderStyle does not use
			// the conventional UIElementBorderStyle settings.
			//
			//headerHeight += 2 * Layout.GetBorderThickness( this.BorderStyleHeaderResolved );
			headerHeight += 2 * this.HeaderBorderThickness;
			headerHeight += 4; // same adjustment made for column (refer to column code for reasoning)

			return headerHeight;
		}        

		internal UltraGridOverride LowestLevelOverride
		{
			get
			{
				if ( null != this.overrideObj )
					return this.overrideObj;

				if ( this.layout.HasOverride )
					return this.layout.Override;

				return null;
			}
		}
		
		// SSP 3/16/06 - App Styling
		// Use the band's GetBLOverrideAppearanceFontHeight method which takes in as params
		// the app-style role related info. Commented out the DefaultHeaderFontHeight.
		// 
		

        /// <summary>
        /// Returns the total height of the column headers for the band.
        /// </summary>
        /// <returns></returns>
        // MRS v7.2 - Document Exporter
		//internal int GetColumnHeaderHeight( )
        public int GetColumnHeaderHeight( )
		{
			return this.GetColumnHeaderHeight( true );
		}

		// SSP 7/21/03 Workaround for UWG1558
		// Added an overload of GetColumnHeaderHeightthat ignores whether the header is
		// visible or not and returns a valid height. By default, it returns 0 if the
		// header is hidden but we want it to return a valid value.
		//
		internal int GetColumnHeaderHeightIgnoreHidden( )
		{
			return this.GetColumnHeaderHeight( true, false, true );
		}

		internal int CardLabelHeightDefault
		{
			get 
			{
				// if we are not in card view return 0
				//
				if ( !this.CardView )
					return 0;

				// if we haven't cached the card label font height yet then do it
				// now
				//
				if ( !this.isCardLabelFontHeightCached )
				{
					// See if there is an override font for headers
					//
					// JJD 12/13/01
					// Use new LowestLevelOverride property. Otherwise we might miss
					// settings off the layout's override. 
					// Note: GetAppearenceFontHeight will merge in the layout's override
					//		 settings if necessary.
					//
					int fontHeight = 0;
						
					// apply the header appearance
					//
					// SSP 3/16/06 - App Styling
					// Use the band's GetBLOverrideAppearanceFontHeight method which takes in the 
					// app-style related params
					// 
					// ----------------------------------------------------------------------------------
					fontHeight = this.GetBLOverrideAppearanceFontHeight( UltraGridOverride.OverrideAppearanceIndex.Header,
						StyleUtils.Role.ColumnHeader, AppStyling.RoleState.Normal );
					
					// ----------------------------------------------------------------------------------

					this.cachedCardLabelFontHeight		= fontHeight;
					this.isCardLabelFontHeightCached	= true;
				}

				return this.cachedCardLabelFontHeight;
			}
		}			

		// SSP 2/25/03 - Row Layout Functionality
		// Added an overload of GetColumnHeaderHeight that returns the height of a
		// single column header rather than the height of all the column headers.
		//
		internal int GetColumnHeaderHeight(bool includeOverlappedBorders )
		{
			return this.GetColumnHeaderHeight( includeOverlappedBorders, false, false );
		}

		// SSP 7/21/03 Workaround for UWG1558
		// Added an overload of GetBandHeaderHeight that ignores whether the header is
		// visible or not and returns a valid height. By default, it returns 0 if the
		// header is hidden but we want it to return a valid value.
		//
		//internal int GetColumnHeaderHeight(bool includeOverlappedBorders /* true*/, bool singleHeaderHeight )
		internal int GetColumnHeaderHeight( bool includeOverlappedBorders, bool singleHeaderHeight, bool ignoreHidden )
		{
			// if the column headers aren't visible return 0
			//
			// JJD 12/26/01
			// We never show column headers in card view so return 0
			//
			// SSP 3/03/03 - Row Layout Functionality
			// If singleHeaderHeight is true, then always return the height of the header
			// regardless of whether the col headers are visible or not. This method is being
			// called from the row layout logic which displays headers even when the col headers
			// are visible.
			//
			//if ( !this.ColHeadersVisibleResolved )
			// SSP 7/21/03 Workaround for UWG1558
			// Added an overload of GetBandHeaderHeight that ignores whether the header is
			// visible or not and returns a valid height. By default, it returns 0 if the
			// header is hidden but we want it to return a valid value.
			//
			//if ( ! singleHeaderHeight && !this.ColHeadersVisibleResolved )
			if ( ! ignoreHidden && ! singleHeaderHeight && !this.ColHeadersVisibleInSeparateAreaResolved )
			{
				// MD 2/26/09 - TFS14602
				// If we are in group layout mode and group headers are still visible, we can't assume the height is 0. 
				// Otherwise, return 0.
				//return 0;
                // MRS 2/27/2009 - TFS14725
                //if ( this.RowLayoutStyle != RowLayoutStyle.GroupLayout ||
                //    this.GroupHeadersVisibleResolved == false )
                // MRS 5/7/2009 - TFS17371
                // I have no idea why we are accounting for groups within a method that gets the 
                // column header height. It looks like this was a bad fix that was put in and 
                // the problems have since been corrected by other fixes. So I took out this 'if' 
                // check. 
                //
                //if (this.GroupHeadersVisibleInSeparateAreaResolved == false)
				{
					return 0;
				}	
			}

			// if we haven't cached the header font height yet then do it
			// now
			//
			if ( !this.isColHdrFontHeightCached )
			{
				int columnsFontHeight = 0;

				// see if any of the columns have overridden the font 
				// and get back the height of the highest font 
				//
				if ( null != this.columns )
					columnsFontHeight = this.columns.MaxHeaderFontHeight;

				// get the height of the default header font for this band
				//
				// SSP 3/16/06 - App Styling
				// Use the band's GetBLOverrideAppearanceFontHeight method which takes in as params
				// the app-style role related info.
				// 
				//int defFontHeight = this.DefaultHeaderFontHeight;
				int defFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.Header, StyleUtils.Role.ColumnHeader, AppStyling.RoleState.Normal );

				// cache the higher of the 2 heights we calculated
				//				
				this.cachedColHdrFontHeight = System.Math.Max( defFontHeight, columnsFontHeight );

				// set the cached flag so we know we don't have to recalc it
				// next time
				//				
				this.isColHdrFontHeightCached = true;
			}

			// JAS v5.2 Wrapped Header Text 4/18/05
			
			//
			// NickC 9/26/00 : (Picture+font)*headerlines needed to be replaced with 
			//                  max(Picture,font*headerlines) as height calc for header
			//int headerHeight = this.cachedColHdrFontHeight * this.colHeaderLines;
			int headerHeight = 0;
			if( this.WrapHeaderTextResolved )
			{
				foreach( UltraGridColumn col in this.Columns )
				{
					// SSP 8/1/05
					// Use the column's Hidden property instead of duplicating the code.
					// 
					
					bool alterHeight = ! col.Hidden && ! col.Header.HiddenResolved;

					if( alterHeight )
					{
						headerHeight = Math.Max( headerHeight, col.CardLabelSize.Height );
					}
				}

                // MRS 4/17/2009 - TFS16676
                // When WrapHeaderText is true and there's a ColumnChooserButton displayed
                // enforce a minimum height for the column headers of 1 column header high. 
                // To ensure the button height does not shrink to 0 or close to it
                //
                if (this.RowSelectorHeaderStyleResolved == RowSelectorHeaderStyle.ColumnChooserButtonFixedSize ||
                    this.RowSelectorHeaderStyleResolved == RowSelectorHeaderStyle.ColumnChooserButton)
                {
                    headerHeight = Math.Max(
                        headerHeight,
                        this.GetColumnHeaderHeightHelper(1));
                }
			}
			else
			{
                headerHeight = this.cachedColHdrFontHeight * this.colHeaderLines;
                
                // MD 4/17/08 - 8.2 - Rotated Column Headers
				// Make sure there is enough height for each column with a visible header to display its text, 
				// which may be rotated.
				foreach ( UltraGridColumn col in this.Columns )
				{
					if ( col.Hidden )
						continue;

					if ( col.Header.HiddenResolved )
						continue;

					TextOrientationInfo headerCaptionOrientation = col.Header.TextOrientationResolved;

					// Don't measure the text if is it not rotated.
					if ( headerCaptionOrientation == null ||
						Object.Equals( headerCaptionOrientation, TextOrientationInfo.Horizontal ) )
						continue;

					AppearanceData appearanceData = new AppearanceData();
					col.Header.ResolveAppearance( ref appearanceData, AppearancePropFlags.FontData );

					int headerHeightNeeded = this.layout.CalculateFontSize(
						ref appearanceData, 
						col.Header.Caption, 
						-1,
						headerCaptionOrientation ).Height;

					// Make sure there is enough height for this column's caption to display
					headerHeight = Math.Max( headerHeight, headerHeightNeeded );
				}

				// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
				// Moved this here from below. We should not add the border thickness if the wrap
				// header text is on since in that case the CardLabelSize includes it.
				// 
				headerHeight += 2 * this.HeaderBorderThickness;
			}

            // CDS NAS v9.1 Header CheckBox
            if (this.ContainsColumnWithVisibleHeaderCheckBoxOnTopOrBottom)
                headerHeight += UIElementDrawParams.GetGlyphSize(GlyphType.CheckBox, this.Layout.UIElement).Height + HeaderCheckBoxUIElement.HEADERCHECKBOXPADDING;

			// MD 7/26/07 - 7.3 Performance
			// MaxHeaderPictureHeight always returns 0 and has been removed
			//if ( null != this.columns )
			//    headerHeight = System.Math.Max ( headerHeight, this.columns.MaxHeaderPictureHeight );
                               
			// use the cached font height and the number of header lines to calculate
			// and return the height of the header
			//
			// JAS 4/27/05 - Use the HeaderBorderThickness property in case the HeaderStyle does not use
			// the conventional UIElementBorderStyle settings.
			//
			//headerHeight += 2 * Layout.GetBorderThickness( this.BorderStyleHeaderResolved );
			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
			// Moved this into the else block above.
			// 
			//headerHeight += 2 * this.HeaderBorderThickness;

			// NickC 9/5/00 : Since we have no padding attributes for the header, add some
			//                generous spacing to get the header to look less cramped
			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
			// 
			//headerHeight += 4; //2;
			headerHeight += EXTRA_HEADER_HEIGHT_PADDING; //2;

			// SSP 2/21/03 - Row Layout Functionality.
			//
			// --------------------------------------------------------------------
			// If we are told to return height of a single header, then do so.
			//
			if ( singleHeaderHeight )
			{
				return headerHeight;
			}

			if ( this.UseRowLayoutResolved )
			{
				return this.PreferredHeaderLayoutSize.Height;
			}
			// --------------------------------------------------------------------

			// if we have more than one level add account for that here
			//
			// MD 1/20/09 - Groups in RowLayout
			// More checks were added to ActualColDisplayLevels which should also be added here, so just use the property instead.
			//if ( this.GroupsDisplayed ) 
			//    headerHeight *= this.levelCount;
			headerHeight *= this.ActualColDisplayLevels;

			// adjust for the overlapped border if requested
			//			
			if ( !includeOverlappedBorders && this.ShareRowBorders )
				headerHeight--;

			return headerHeight;
		}

        // MRS 4/17/2009 - TFS16676
        // Extracted some code from GetColumnHeaderHeight into this little helper method. 
        private int GetColumnHeaderHeightHelper(int colHeaderLines)
        {
            int headerHeight = this.cachedColHdrFontHeight * colHeaderLines;
            headerHeight += 2 * this.HeaderBorderThickness;
            return headerHeight;
        }			
        
		internal bool IsDescendantOfBand( UltraGridBand ancestorBand )
		{
			// If the passed in band is our parent then return true
			//
			if ( ancestorBand == this.parentBand )
				return true;

			// Otherwise, if we have a parent band then call this 
			// method on the parent (effectively walking up the parent
			// chain) or return false if we don't
			//
			return this.parentBand != null 
				? this.parentBand.IsDescendantOfBand( ancestorBand )
				: false;
		}

		// SSP 10/15/04 - UltraCalc
		// Added IsTrivialDescendantOf method.
		//
		internal bool IsTrivialDescendantOf( UltraGridBand band )
		{
			return this == band || this.IsDescendantOfBand( band );
		}

		internal void AdjustOrigin( int adjustment )
		{
			this.origin += adjustment;
		}

		internal bool ShareRowBorders 
		{
			get
			{
				return this.Layout.CanMergeAdjacentBorders( this.BorderStyleRowResolved );
			}
		}

		internal bool GroupsDisplayed
		{
			get
			{
				// SSP 3/4/04
				// Changed the behavior to give row-layout higher priority than the groups.
				// Meaning if there are groups and they turn set UseRowLayout to true, we
				// will ignore groups and use the row-layout functionality.
				//
				//return null != this.groups  && this.groups.Count > 0;
				// MD 1/19/09 - Groups in RowLayout
				// Groups are now also displayed with the GroupLayout style.
				//return null != this.groups && ! this.UseRowLayoutResolved && this.groups.Count > 0;
				return
					null != this.groups
					&& this.RowLayoutStyle != RowLayoutStyle.ColumnLayout
					&& this.groups.Count > 0;
			}
		}

		internal bool HasSiblingBands
		{
			get
			{
				// SSP 11/24/03
				// Use the ViewStyleIml instead of ViewStyle to check if the view style being used
				// is a single band view style. The reason for this is that for UltraDropDown and
				// UltraCombo, we always use a VewStyleSingle even when ViewStyleBand is set to
				// Multi which it is by default. If this change causes any problem, take it out.
				// I changed it because I noticed it and not because it was causing a problem.
				//
				//if ( ViewStyle.SingleBand == this.layout.ViewStyle ||
				if ( ! this.layout.ViewStyleImpl.IsMultiBandDisplay ||
					this.parentBand == null )
					return false;
	            
				// Iterate over each band to see if any other band is a
				// child of our parent band
				//
				for ( int i = 1; i < this.layout.SortedBands.Count; i++ )
				{
					if ( this.layout.SortedBands[i] == this  ||
						this.layout.SortedBands[i].HiddenResolved )
						continue;

					if ( this.layout.SortedBands[i].ParentBand == this.parentBand )
						return true;
				}

				return false;
			}
		}


		internal int MinHeightRequiredByColumns
		{
			get
			{
				if ( this.columns != null  )
					return this.columns.MinHeightRequiredByColumns;

				return 0;
			}
		}
	
		internal void ResizeRow( Infragistics.Win.UltraWinGrid.UltraGridRow row, 
			int newHeight, 
			bool fireEvents ) 
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			// SSP 1/18/05 BR01594
			// Before setting the Height of an UltraDropDown or an UltraCombo row did not work at all.
			// This is to fix that.
			//
			//if ( grid == null )
			//	return;

			// SSP 1/18/05 BR01594
			// Related to above change. Check for grid being null.
			//
			//GridEventManager eventManager = grid.EventManager;
			//if ( fireEvents )
			if ( fireEvents && null != grid )
			{
				// fire the before event
				//
				BeforeRowResizeEventArgs e = new BeforeRowResizeEventArgs( row, newHeight );
				grid.FireEvent( GridEventIds.BeforeRowResize, e );

				// if cancelled return 
				//
				if ( e.Cancel )
					return;
			}

			// Check anti-reentrancy flag for row height synchronization
			//
			if ( !this.isSynchronizingRowHeights &&
				this.RowSizingResolved == RowSizing.Sychronized )
				// resize all
			{
				// SSP 7/18/08 BR28663
				// NewHeight that's passed in always includes the row preview in it (if it's enabled).
				// However synchronizedRowHeight should not contain the row preview height in it because
				// the way it's used (view Band.RowHeightResolved mehtod) in Row.InitRootHeight method, 
				// it's assumed to not contain the row preview height. Also RowHeightResolved property
				// initially returns a value that doesn't contain row preview height. Therefore here
				// we need to substract row preview height from the new height.
				// 
				//this.synchronizedRowHeight = newHeight;
				this.synchronizedRowHeight = newHeight - ( null != row ? row.AutoPreviewHeight : 0 );
								    
			
				// We have synchronizedRowHeightVersion so that we can go a
				// lazy approach for synchronizing row heights. This is because
				// if we have a lot of rows preloaded it can take too long to
				// process a size change.
				//  
				// Bump the version number here
				//
				this.synchronizedRowHeightVersion++;

				// Set anti-reentrancy flag for row height synchronization
				//
				this.isSynchronizingRowHeights = true;

				// Wrap the folowing code in a try/catch so we are
				// guaranteed that the isSynchronizingRowHeights flag
				// will get reset
				//
				try
				{
					// Resize all visible rows in this band
					//
					if ( null != this.Layout.RowScrollRegions )
					{
						RowScrollRegion rsr;
						VisibleRow      visibleRow;

						// iterate over the row scroll regions
						//
						for ( int i = 0; i < this.Layout.RowScrollRegions.Count; i++ )
						{
							rsr = this.Layout.RowScrollRegions[i];

							if ( null != rsr )
							{
								bool firstRow = true;

								for ( int j = 0; j < rsr.VisibleRows.Count; j++ )
								{
									visibleRow = rsr.VisibleRows[j];

									if ( null == visibleRow || 
										null == visibleRow.Row ||
										this != visibleRow.Band )
										continue;
									
									visibleRow.Row.SetHeight( newHeight );
								
									if ( firstRow )
									{
										// On the first row in each row scroll region walk
										// up its parent chain calling SynchSameTierParentRows
										// on each ancestor. This is needed for view styles
										// that has multi row tiers
										//
										// SSP 6/12/03 UWG2287
										// Don't clear the row parameter. We need it when we fire the AfterRowResize
										// at the end of this method.
										// Commented out the original code and added new code that does the same thing
										// except that it doesn't end up setting the row parameter to null.
										// NOTE: This alters the behaviour from the previous code as far as this and
										// the parent for loops are concerned however the new behaviour is the one that
										// was desired in the first place.
										//
										// --------------------------------------------------------------------------
										
										UltraGridRow tmpRow = row;
										while ( null != tmpRow )
										{
											tmpRow = tmpRow.ParentRow;
                                    
											if ( null != tmpRow )
												tmpRow.SynchSameTierParentRows( newHeight );
										}
										// --------------------------------------------------------------------------

										firstRow = false;
									}
								}
							}
						}

						// SSP 6/12/03 UWG2188
						//
						this.synchronizedRowHeightVersion++;

						// SSP 11/24/03 - Scrolling Till Last Row Visible
						//
						this.Layout.BumpGrandVerifyVersion( );

						// Dirty all visible rows
						//
						this.Layout.RowScrollRegions.DirtyAllVisibleRows(false);
						this.Layout.DirtyGridElement( false );

					}
				}
				catch( Exception )
				{
					Debug.Fail("Try caught in Band.ResizeRow");
				}
				// Reset anti-reentrancy flag for row height synchronization
				//
				this.isSynchronizingRowHeights = false;
			}
			else
			{
				// resize only this row
				//
				row.SetHeight( newHeight );
			}

			// fire the after event
			//
			// SSP 1/18/05 BR01594
			// Check for grid being null.
			//
			//if ( fireEvents )
			if ( fireEvents && null != grid )
				grid.FireEvent( GridEventIds.AfterRowResize, new RowEventArgs( row ) );
		}
		

		// SSP 11/25/03
		// Commented out below internal FontHeight method as it's not used anywhere
		//
		

		// SSP 11/25/03 UWG2013 UWG2553
		// Added MinRowHeight property to Override to allow for unrestricted control over the row heights.
		//
		/// <summary>
		/// Returns 
		/// </summary>
		internal int OverrideMinRowHeight
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeMinRowHeight( ) )
					return this.overrideObj.MinRowHeight;

				if ( this.layout.HasOverride && this.layout.Override.ShouldSerializeMinRowHeight( ) )
					return this.layout.Override.MinRowHeight;
				
				return -1;
			}
		}

		internal int GetMinRowHeight( )
		{
			return this.GetMinRowHeightHelper( true );
		}

		private int CalcMinRowHeight( )
		{
			int selectedRowFontHeight      = 0;
			int selectedCellFontHeight     = 0;
			int editCellFontHeight         = 0;
			int activeRowFontHeight        = 0;
			int activeCellFontHeight       = 0;
			int cellFontHeight             = 0;
			int rowFontHeight              = 0;
			int rowAlternateFontHeight     = 0;
			int minHeightRequiredByColumns = this.MinHeightRequiredByColumns;

			// JJD 12/13/01
			// Use new LowestLevelOverride property. Otherwise we might miss
			// settings off the layout's override. 
			// Note: GetAppearenceFontHeight will merge in the layout's override
			//		 settings if necessary.
			//
			UltraGridOverride overrideObj = this.LowestLevelOverride;
			
			if ( overrideObj != null )
			{
				// SSP 3/16/06 - App Styling
				// Use the band's GetBLOverrideAppearanceFontHeight method which takes in the 
				// app-style related params.
				// 
				// --------------------------------------------------------------------------------------
				
				selectedRowFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.SelectedRow,
					false, StyleUtils.Role.Row, AppStyling.RoleState.Selected );

				selectedCellFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.SelectedCell,
					false, StyleUtils.Role.Cell, AppStyling.RoleState.Selected );

				editCellFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.EditCell,
					false, StyleUtils.Role.Cell, AppStyling.RoleState.EditMode );

				activeRowFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.ActiveRow,
					false, StyleUtils.Role.Row, AppStyling.RoleState.Active );

				activeCellFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.ActiveCell,
					false, StyleUtils.Role.Cell, AppStyling.RoleState.Active );

				cellFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.Cell,
					false, StyleUtils.Role.Cell, AppStyling.RoleState.Normal );

				rowFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.Row,
					false, StyleUtils.Role.Row, AppStyling.RoleState.Normal );

				rowAlternateFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.RowAlternate,
					false, StyleUtils.Role.Row, AppStyling.RoleState.AlternateItem );
				// --------------------------------------------------------------------------------------
			}
    	

			// JJD 12/13/01
			// Comment out the follwing code since it is redundant. The calls
			// above using the new LowestLevelOverride property will merge in the 
			// layout's override settings if necessary.
			//
			// If the layout has override, use it's appearances.
			//
			
			// If no row font height was specified, then use the Layout.Appearance font height
			//
			if ( 0 == rowFontHeight )
			{
				// JJD 12/13/01
				// Moved logic for calculating and caching the default
				// font height into the layout
				//
				rowFontHeight = this.layout.DefaultFontHeight;
			}
			
			int maxOfHeights = 0;

			maxOfHeights = Math.Max( maxOfHeights, selectedRowFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, selectedCellFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, editCellFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, activeRowFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, activeCellFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, cellFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, rowFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, rowAlternateFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, minHeightRequiredByColumns );

			return maxOfHeights;
		}


		// SSP 1/23/02 UWG982
		// Added this method
		//
		internal int CalcMinGroupByRowHeight( )
		{
			int selectedRowFontHeight      = 0;			
			int activeRowFontHeight        = 0;
			int rowFontHeight              = 0;
			int rowAlternateFontHeight     = 0;

			// JJD 12/13/01
			// Use new LowestLevelOverride property. Otherwise we might miss
			// settings off the layout's override. 
			// Note: GetAppearenceFontHeight will merge in the layout's override
			//		 settings if necessary.
			//
			UltraGridOverride overrideObj = this.LowestLevelOverride;
			
			if ( overrideObj != null )
			{
				// SSP 3/16/06 - App Styling
				// Use the band's GetBLOverrideAppearanceFontHeight method which takes in the 
				// app-style related params.
				// 
				// --------------------------------------------------------------------------------------
				
				selectedRowFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.SelectedRow,
					false, StyleUtils.Role.Row, AppStyling.RoleState.Selected );

				activeRowFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.ActiveRow,
					false, StyleUtils.Role.Row, AppStyling.RoleState.Active );

				rowFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.Row,
					false, StyleUtils.Role.Row, AppStyling.RoleState.Normal );

				rowAlternateFontHeight = this.GetBLOverrideAppearanceFontHeight( 
					UltraGridOverride.OverrideAppearanceIndex.RowAlternate,
					false, StyleUtils.Role.Row, AppStyling.RoleState.AlternateItem );
				// --------------------------------------------------------------------------------------
			}
    	
			// If no row font height was specified, then use the Layout.Appearance font height
			//
			if ( 0 == rowFontHeight )
			{
				// JJD 12/13/01
				// Moved logic for calculating and caching the default
				// font height into the layout
				//
				rowFontHeight = this.layout.DefaultFontHeight;
			}
			
			int maxOfHeights = 0;

			maxOfHeights = Math.Max( maxOfHeights, selectedRowFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, activeRowFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, rowFontHeight );
			maxOfHeights = Math.Max( maxOfHeights, rowAlternateFontHeight );

			return maxOfHeights;
		}

		private int GetMinRowHeightHelper( bool synchronized  )
		{			
			if( synchronized )
				
				this.isRowHeightDirty = true;

			// if we haven't cached the row font height yet then do it
			// now. We need to calculate the largest font used by a row.
			//
			if ( !this.isRowFontHeightCached )
			{
				
				// SSP 11/2/01
				// Use the newly created CalcMinFontHeight( ) method which takes into
				// account various appearances font heights .
				// ( activeCell, editCell, activeRow, Row, Cell and so on ).
				//
				this.cachedRowFontHeight = this.CalcMinRowHeight( );
				this.isRowFontHeightCached = true;
				
			}

			// SSP 6/27/03 UWG2410
			// Moved this code inside of the if block below. In horizontal view style we need to
			// return the max row heights of all bands if synchronized parameter is true.
			//
			

			if ( !this.isMinRowHeightCached )
			{
				// SSP 6/27/03 UWG2410
				// Moved the code from above to here. 
				// Added the if block and enclosed the already existing code into the else block.
				//
				int minRowHeight = 0;
				if ( this.UseRowLayoutResolved )
				{
					// SSP 8/29/05 BR05837
					// GetLayoutRowHeight returned preferred row height. We want to calculate the
					// minimum row height here.
					// 
					//minRowHeight = this.GetLayoutRowHeight( );
					minRowHeight = this.GetLayoutRowHeight( true );
				}
				else
				{
					// Added logic to calculate cell border height requirements based
					// on the merging of adjacent cell borders in certain cases
					//
					int cellSpacing                        = this.CellSpacingResolved;
					int cellBorderThickness                = Layout.GetBorderThickness( this.BorderStyleCellResolved ); 
					int cellBorderAdjustmentPerExtraLevel  = 0;
					int cellBorderAdjustmentPerRow         = 0;

					if ( cellSpacing > 0       ||
						!this.Layout.CanMergeAdjacentBorders( this.BorderStyleCellResolved ) )
					{
						// each level needs 2 times the cell border thickness
						// if there is any spacing between cells or if the
						// cell border style if not mergeable
						//
						cellBorderAdjustmentPerExtraLevel  = 2 * cellBorderThickness;
						cellBorderAdjustmentPerRow         = 2 * cellBorderThickness;
					}
					else
					{
						// Otherwise each extra level needs one 1 times the cell border
						// thickness since cells that abut bottom and top will share
						// a single border. 
						//
						cellBorderAdjustmentPerExtraLevel = cellBorderThickness;

						// If the row borders can't be merged initialize the 
						// row adjustment amount. 
						//
						if ( !this.Layout.CanMergeAdjacentBorders( this.BorderStyleRowResolved ) )
							cellBorderAdjustmentPerRow = 2 * cellBorderThickness;
					}

					// use the cached font height,cell padding, and cell spacing
					// to calculate and return the default height of a row
					//		
					// SSP 6/27/03 UWG2410
					// Moved the definition of minRowHeight before the if statement associated with
					// this else block.
					//
					//int minRowHeight = 2  + ( this.cachedRowFontHeight
					minRowHeight = 2  + ( this.cachedRowFontHeight
						+ ( this.CellPaddingResolved * 2)
						+ ( cellSpacing * 2) );

					// JJD 3/28/00
					// Adjust height for multiple levels
					//
					// MD 1/20/09 - Groups in RowLayout
					// More checks were added to ActualColDisplayLevels which should also be added here, so just use the property instead.
					//if ( this.GroupsDisplayed && this.levelCount > 1 )
					//{
					//    // multiply the single level height calculated above by the 
					//    // number of levels
					//    //
					//    minRowHeight *= this.levelCount;
					//
					//    // Adjust the height for the extra levels
					//    //
					//    minRowHeight += cellBorderAdjustmentPerExtraLevel *
					//        ( this.levelCount - 1 );
					//
					//}
					int displayLevels = this.ActualColDisplayLevels;
					if ( displayLevels > 1 )
					{
						// multiply the single level height calculated above by the 
						// number of levels
						//
						minRowHeight *= displayLevels;

						// Adjust the height for the extra levels
						//
						minRowHeight += cellBorderAdjustmentPerExtraLevel *
							( displayLevels - 1 );
					}

					// Add in the row border thickness and the additonal
					// cell border adjustment as well
					//
					minRowHeight += cellBorderAdjustmentPerRow
						+ ( this.Layout.GetBorderThickness( this.BorderStyleRowResolved ) * 2 );
				}

				if ( synchronized )
				{
					ViewStyleBase viewStyle = this.Layout.ViewStyleImpl;

					// if there are multiple rows on a tier (horizontal view style)
					// we need to get the largest min row height from each
					// visible band
					//
					if ( viewStyle.HasMultiRowTiers )
					{

						// loop thru the bands collection checking for the
						// visible band with the largest min row height
						//
						for ( int i = 0; i < this.layout.SortedBands.Count; i++ )  
						{
							Infragistics.Win.UltraWinGrid.UltraGridBand band = this.layout.SortedBands[i];

							// we can skip this band and any band that is
							// hidden
							//
							if ( band == this || band.HiddenResolved )
								continue;

							minRowHeight = System.Math.Max( minRowHeight, band.GetMinRowHeightHelper( false ) );
						}
					}
				}

				this.cachedRowFontHeight = minRowHeight;
				this.isMinRowHeightCached = true;
				return minRowHeight;
			}
			else
			{
				return this.cachedRowFontHeight;
			}
			
		}

		internal bool ScrollTipsEnabled
		{
			get
			{
				// SSP 7/19/05
				// TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
				// need to resolve the TipStyleScroll differently based on whether the scrolling logic 
				// or the row connector logic is calling it.
				// 
				//if ( TipStyle.Hide != this.TipStyleScrollResolved )
				if ( TipStyle.Hide != this.TipStyleScrollResolved( false ) )
					return true;

				if ( null != this.parentBand )
					return this.parentBand.ScrollTipsEnabled;

				return false;
			}
		}        

		internal int HierarchicalLevel
		{
			get
			{
                // MBS 4/23/09 - TFS12665
                // If we're using the layout's caching mechanism, we can return that now 
                // instead of walking up the parent chain 
                UltraGridLayout.CachedBandInfo bandInfo = null;
                Dictionary<UltraGridBand, UltraGridLayout.CachedBandInfo> cachedInfo = null;
                if (this.layout.IsCachingBandInfo)
                {
                    cachedInfo = this.layout.CachedBandData;
                    if (cachedInfo.TryGetValue(this, out bandInfo) && bandInfo.HierarchicalLevel > -1)                                            
                            return bandInfo.HierarchicalLevel;                    
                }

				UltraGridBand parentBand = this.parentBand;
				int level = 0;
				while ( parentBand != null )
				{
					level++;
					parentBand = parentBand.ParentBand;
				}

                // MBS 4/23/09 - TFS12665
                // Cache the level for future use, if applicable
                if (cachedInfo != null)
                {
                    if (bandInfo != null)
                        bandInfo.HierarchicalLevel = level;
                    else
                    {
                        bandInfo = new UltraGridLayout.CachedBandInfo();
                        bandInfo.HierarchicalLevel = level;

                        // MBS 5/13/09 - TFS17581
                        //cachedInfo.Add(this, bandInfo);
                        cachedInfo[this] = bandInfo;
                    }
                }

				return level;
			}
		}
		internal int SynchronizedRowHeightVersion
		{
			get
			{
				return synchronizedRowHeightVersion;
			}
		}

		internal int MajorRowHeightVersion
		{
			get
			{
				return this.majorRowHeightVersion; 
			}
		}

		internal void BumpMajorRowHeightVersion()
		{ 
			this.synchronizedRowHeightVersion++; 
			this.majorRowHeightVersion = this.synchronizedRowHeightVersion; 

			if ( null != this.Layout )
			{
				// SSP 11/24/03 - Scrolling Till Last Row Visible
				//
				this.Layout.BumpGrandVerifyVersion( );

				// SSP 11/17/05 BR07605
				// 
				this.Layout.DirtyGridElement( true );
			}
		}
		internal bool IsRowHeightDirty
		{
			get
			{
				if( this.isRowHeightDirty )//.rowHeightIsDirty )
				{
					//this.rowHeightIsDirty = false;
					this.isRowHeightDirty = false;
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		internal UltraGridColumn ScrollTipCol
		{
			get
			{
				if ( null == this.columns ||
					this.columns.Count < 1 )
					return null;

				// use the m_strScrollTipField tp index into the column's collecction
				//
				UltraGridColumn retColumn = null != this.ScrollTipField && this.ScrollTipField.Length > 0
					? this.Columns[ this.ScrollTipField ]
					: null;

				if ( null == retColumn )
				{                
					// MRS 1/7/04 - BR00641
					// If this is a DropDown or Combo, use the DisplayMemberResolved
					UltraDropDownBase dropdown = this.layout.Grid as UltraDropDownBase;
					if (dropdown != null)
						retColumn = this.Columns[ dropdown.DisplayMemberResolved ];
					else
					{
						// get the first non-hidden column
						//
						for ( int i = 0; i < this.Columns.Count; i++ )
						{
							UltraGridColumn column = this.Columns[i];
							// Skip html columns so we don't use them as the default
							// for the scroll tip field
							//
							//if ( !column.Hidden &&
							//	Infragistics.Win.UltraWinGrid.ColumnStyle.HTML != column.StyleResolved )
                            // MRS 2/23/2009 - Found while fixing TFS14354
							//if ( !column.Hidden )
                            if (!column.HiddenResolved)
							{
								retColumn = column;
								break;
							}
						}
					}
				}

				return retColumn;
			}
		}

		internal int GetSyncPosition( UltraGridColumn column )
		{
			if ( column.Band != this )
			{
				Debug.Fail("Invalid band in Band.GetSyncPosition" );
				return -1;
			}

			int               colSyncPosition    = 0;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridGroup             group              = column.Group;

			ColScrollRegion   exclusiveRegion    = column.Header.ExclusiveColScrollRegion;
    			
			HeadersCollection orderedHeaders = this.OrderedHeaders;
			
			int i;
    
			// Iterate over each header to count the col sync positions
			//
			for ( i = 0; i < orderedHeaders.Count; i++ )
			{
				HeaderBase header = orderedHeaders[i];

				if ( header.Hidden ||
					header.ExclusiveColScrollRegion!= exclusiveRegion )
				{
					continue;
				}

				if ( header.IsGroup )
				{
					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
					//if ( column.Group == header.Group )
					if ( column.GroupResolved == header.Group )
					{
						int relativeSyncPosition = header.GetRelativeColSyncPosition( column );

						if ( relativeSyncPosition >= 0 )
							return colSyncPosition + relativeSyncPosition;
						else 
							return -1;
					}
				}
				else
				{
					if ( column.Header == header )
					{
						int relativeSyncPosition = header.GetRelativeColSyncPosition( column );

						if ( relativeSyncPosition >= 0 )
							return colSyncPosition + relativeSyncPosition;
						else 
							return -1;
					}
				}

				// since this header didn't contain the column just update
				// the running total of sync positions
				//
				colSyncPosition += header.GetTotalColSyncPositions( exclusiveRegion );
			}

			return -1;
		}	

		internal int OriginAdjustmentDuringColSynch
		{ 
			get
			{
				return this.synchronizationAdjustment;
			}			
		}
		internal void AdjustWidthDuringColSynch( int adjustment )
		{
			this.synchronizationAdjustment += adjustment;
		}

		internal Activation ActivationResolved
		{
			get
			{
				// Only return disabled if the header is marked disabled
				//
				return ( this.header != null && this.header.Enabled ) ? Activation.ActivateOnly : Activation.Disabled;
			}
			
		}
		internal bool IsRowSizingFixed
		{
			get
			{
				RowSizing rowSizing = this.RowSizingResolved;

				// if row sizing is set to ssRowSizingFixed or ssRowSizingAutoFixed, row
				// sizing is not allowed
				if ( rowSizing == RowSizing.Fixed  ||
					rowSizing == RowSizing.AutoFixed )
					return true;
				else
					return false;
			}
		}
		internal int GetVisiblePosition( UltraGridColumn column )
		{
			if ( column.Band != this )			
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_14") );
	
			for ( int count = 0; count < this.OrderedColumnHeaders.Count; count++ )
			{
				if ( this.orderedColumns[count] == column.Header )
				{
					return count;
				}
			}
			return -1;
		}
		internal void SetVisiblePosition( UltraGridColumn column, int newPos, ref bool changed )
		{
			changed = false;

			int oldPos;

			// get the column's current position
			//
			
			oldPos = GetVisiblePosition( column );

			// if it hasn't changed then just return 
			//
			if ( newPos == oldPos)
				return;

			// use uppercase OrderColumns so that they will be 
			// reinitialized if the count doesn't match the number
			// in the column collection.
			//
			//int numColumns = this.orderedColumns.Count;
			int numColumns = this.OrderedColumnHeaders.Count;

			// Check for valid value
			if ( newPos < 0 || newPos >= numColumns)
			{
				// AS - 10/31/01
				// It is possible to have more columns in the source than the destination,
				// in which case, we should just move the column to the end of the list.
				//
				//throw new ArgumentException( "Invalid newPos passed into Band.SetVisiblePosition" );
				newPos = numColumns - 1;
			}

			// First, remove the column from its current position
			//
			this.orderedColumns.InternalRemove( column.Header );

			// now re-insert it in the list at its new position
			//
			int i = newPos;
			try
			{
				// add the item to the collection at the correct spot
				//
				this.orderedColumns.InternalInsert( i, column.Header );
			}
			catch(Exception e)
			{
				throw new Exception( SR.GetString("LER_Exception_325",e.Message ));
			}

			// SSP 7/23/03 - Fixed headers
			// Ensure that the fixed headers are before non-fixed headers. Also update
			// the visiblePos member variables of all the headers to their new visible
			// positions. The reason for doing this is that with the fixed headers
			// functionality, we are re-sorting the ordered headers everytime a header
			// is fixed or unfixed. And if we don't update the visiblePos member variables,
			// then the order of the headers won't be maintained when we re-sort the
			// headers. So call SetVisiblePositionsToActual to update the visiblePos
			// member variables of the headers.
			//
			// ----------------------------------------------------------------------------------
			this.EnsureFixedHeadersBeforeNonFixed( );
			this.SetVisiblePositionsToActual( );
			// ----------------------------------------------------------------------------------

			changed = true;
		}

		/// <summary>
		/// Returns the first visible column in the passed in column scrolling region.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to determine which is the first (leftmost) visible column in a specific column scrolling region.</p>
		/// </remarks>
		/// <param name="csr">An existing column scrolling region.</param>
		/// <param name="includeNonActivateable">true to include columns that are not navigable activateable.</param>
        /// <returns>The first visible column in the passed in column scrolling region.</returns>
		public UltraGridColumn GetFirstVisibleCol( ColScrollRegion csr, bool includeNonActivateable )
		{
			return this.GetFirstVisibleCol( csr, includeNonActivateable, false );
		}

		// SSP 7/29/05 - NAS 5.3 Tab Index
		// Added an overload that takes in useTabOrderedColumns.
		// 
		/// <summary>
		/// Returns the first visible column in the passed in column scrolling region.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to determine which is the first (leftmost) visible column in a specific column scrolling region.</p>
		/// </remarks>
		/// <param name="csr">An existing column scrolling region.</param>
		/// <param name="includeNonActivateable">Specifies whether to include columns that are not activateable.</param>
		/// <param name="useTabOrderedColumns">Specifies whether to return the columns according to their TabIndex settings or their visible order.</param>
        /// <returns>The first visible column in the passed in column scrolling region.</returns>
		public UltraGridColumn GetFirstVisibleCol( ColScrollRegion csr, bool includeNonActivateable, bool useTabOrderedColumns )
		{
			// SSP 11/17/03 - Row Layout Functionality
			//
			//HeadersCollection headers = this.OrderedHeaders;
			HeadersCollection headers = this.UseRowLayoutResolved ? this.LayoutOrderedVisibleColumnHeaders : this.OrderedHeaders;

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			if ( useTabOrderedColumns )
				headers = this.TabOrderedColumns;
			
			if ( headers == null )
				return null;

			ColScrollRegion exclusiveColScrollRegion = csr != null && csr.HasExclusiveItems ? csr.ExclusiveColScrollRegion : null ;

			UltraGridColumn col = null;

			// iterate over the ordered headers looking for the first visible column
			//
			for ( int i=0; i < headers.Count; ++i)
			{
				HeaderBase headerItem = headers[i];

				// Skip over hidden position items
				//
				if ( headerItem.Hidden )
					continue;

				// if a col scroll region was passed in check if this position
				// item appears in thet scroll region
				//
				if ( csr != null && headerItem.ExclusiveColScrollRegion != exclusiveColScrollRegion )
					continue;

				// if the position item is a group get the group's first visible
				// column
				//
				if ( headerItem.IsGroup )
					col = headerItem.GetFirstVisibleCol( includeNonActivateable );
				else if ( includeNonActivateable || !headerItem.IsDisabled )
					col = headerItem.Column;
				else
					col = null ;

				// if we have a column then we can stop the iteration
				//
				if ( col != null )
					break;				
			}
			return col;
		}

		internal static NavigateType MapToNavigateType( VisibleRelation relation )
		{
			switch ( relation )
			{
				case VisibleRelation.First:
					return NavigateType.First;

				case VisibleRelation.Last:
					return NavigateType.Last;

				case VisibleRelation.Previous:
					return NavigateType.Prev;
			}

			return NavigateType.Next;
		}

		internal UltraGridColumn GetRelatedVisibleCol( UltraGridColumn column, NavigateType navType, bool includeNonActivateable )
		{
			return this.GetRelatedVisibleCol( column, navType, includeNonActivateable, false );
		}
		
		// SSP 7/29/05 - NAS 5.3 Tab Index
		// Added overload that takes in byTabKey and useTabOrderedColumns parameters.
		// 
		internal UltraGridColumn GetRelatedVisibleCol( UltraGridColumn column, NavigateType navType, 
			bool includeNonActivateable, bool byTabKey )
		{
			// SSP 3/5/03 - Row Layout Functionality
			//
			//HeadersCollection headers = this.OrderedHeaders;
			HeadersCollection headers = this.UseRowLayoutResolved ? this.LayoutOrderedVisibleColumnHeaders : this.OrderedHeaders;

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			// ----------------------------------------------------------
			bool usingTabOrderedColumns = byTabKey;
			Debug.Assert( ! byTabKey || NavigateType.First == navType || NavigateType.Last == navType
				|| NavigateType.Next == navType || NavigateType.Prev == navType );
			if ( usingTabOrderedColumns )
				headers = this.TabOrderedColumns;
			// ----------------------------------------------------------

			if ( headers == null )
				return null;

			// SSP 6/23/05 BR04630
			// In row-layout mode we could have groups however they would not be in effect.
			// 
			//UltraGridGroup group = column == null ? null : column.Group;
			// MD 1/19/09 - Groups in RowLayout
			// Since we will only have a list of column headers when using the GroupLayout style, there is no group to match on, 
			// so don't let it get set or we may get a false match.
			//bool groupsDisplayed = this.GroupsDisplayed;
			bool groupsDisplayed = 
				this.GroupsDisplayed 
				&& this.RowLayoutStyle != RowLayoutStyle.GroupLayout;

			UltraGridGroup group = null != column && groupsDisplayed ? column.Group : null;

			UltraGridColumn relatedCol = null;

            // MBS 10/25/07 - BR27345
            // We should be looking at the resolved ColScrollRegion
            //
            //ColScrollRegion exclusiveColScrollRegion = column == null ? null : column.Header.ExclusiveColScrollRegion;
            ColScrollRegion exclusiveColScrollRegion = column == null ? null : column.Header.ExclusiveColScrollRegionResolved;

			bool columnFound = false;
			
			if ( NavigateType.Above == navType ||
				NavigateType.Below == navType ||
				NavigateType.Topmost ==  navType ||
				NavigateType.Bottommost == navType )
			{
				if ( column == null )
					throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_149"), Shared.SR.GetString("LE_ArgumentNullException_304"));

				// Above or below only have meaning if we are in a band with
				// groups that have a level count > 1.
				// Otherwise just return the passed in column.
				//
				if ( group != null )
				{
					return group.Header.GetRelatedVisibleCol( column, navType, includeNonActivateable );
				}
				else
				{
					// SSP 1/13/04 - Row Layout Functionality / Accessibility
					// 
					// ----------------------------------------------------------------------------------------
					if ( this.UseRowLayoutResolved )
					{
						headers = this.LayoutOrderedVisibleColumnHeadersVertical;						
						Debug.Assert( null != headers && headers.Count > 0 );
						if ( null == headers || headers.Count <= 0 )
							return null;
				
						int headerIndex = headers.IndexOf( column.Header );
						Debug.Assert( headerIndex >= 0, "Header not in the collection !" );
						if ( headerIndex < 0 )
							return null;

						switch ( navType )
						{
							case NavigateType.Above:
								return headerIndex > 0 ? headers[ headerIndex - 1 ].Column : null;
							case NavigateType.Below:
								return 1 + headerIndex < headers.Count ? headers[ 1 + headerIndex ].Column : null;
							case NavigateType.Topmost:
								while ( headerIndex > 0 && headers[headerIndex].Column.RowLayoutColumnInfo.OriginYResolved < column.RowLayoutColumnInfo.OriginYResolved )
									headerIndex--;
								return headers[headerIndex].Column;
							case NavigateType.Bottommost:
								while ( headerIndex < headers.Count && headers[headerIndex].Column.RowLayoutColumnInfo.OriginYResolved > column.RowLayoutColumnInfo.OriginYResolved )
									headerIndex++;
								return headers[headerIndex].Column;
						}
					}
					// ----------------------------------------------------------------------------------------

					return column.Header.GetRelatedVisibleCol( column, navType, includeNonActivateable );
				}
			}

			for ( int i =0; i< headers.Count; ++i )
			{
				HeaderBase headerItem = headers[i];

				// Skip over hidden position items
				//
				if ( headerItem.Hidden ||
					headerItem.ExclusiveColScrollRegion != exclusiveColScrollRegion )
					continue;
				
				if ( NavigateType.First == navType )
				{
					// get the first visible col of this position item
					// if found we can stop here
					//
					relatedCol = headerItem.GetFirstVisibleCol( includeNonActivateable );
					if ( relatedCol != null )
						break;
				}

				else if ( NavigateType.Next == navType )
				{
					if ( columnFound )
					{
						// if we have already found the passed in column
						// then get the first visible from this position item
						// and stop if it has one
						//
						relatedCol = headerItem.GetFirstVisibleCol( includeNonActivateable );
						if ( relatedCol != null )
							break;
					}
				}

				if ( 
					// SSP 6/23/05 BR04630
					// In row-layout mode we could have groups however they would not be in effect.
					// Check for groupsDisplayed.
					// 
					//( group != null && group == headerItem.Group ) ||
					// SSP 7/29/05 - NAS 5.3 Tab Index
					// If using TabOrderedColumns collection then skip the group logic.
					// 
					//groupsDisplayed && group != null && group == headerItem.Group ||
					! usingTabOrderedColumns && groupsDisplayed && group != null && group == headerItem.Group ||
					column == headerItem.Column )
				{
					// we found the passed in column so set the stack flag
					//
					columnFound = true;

					if ( NavigateType.Next == navType )
					{
						// call get next off the position item. If foiund
						// we can stop
						//
						relatedCol = headerItem.GetNextVisibleCol(column, includeNonActivateable );
						if ( relatedCol != null )
							break;
					}
					else if ( NavigateType.Prev == navType )
					{
						// get the prev col from the position item
						//
						UltraGridColumn prevCol = headerItem.GetPrevVisibleCol( column, includeNonActivateable );

						// if it has a prev column use it. Otherwise use the
						// pRelatedCol that we have kept track of during the
						// iteration. Either way we eed to stop
						//
						if ( prevCol != null )
							relatedCol = prevCol;
						break;
					}
				}

				if ( NavigateType.Last == navType   ||
					NavigateType.Prev == navType )
				{
					// for last and prev we need to keep track of the 
					// last visible col 
					//
					UltraGridColumn lastVisibleCol = headerItem.GetLastVisibleCol( includeNonActivateable );

					if ( lastVisibleCol != null )
						relatedCol = lastVisibleCol;
				}
			}
			return relatedCol;
		}

		internal UltraGridColumn GetLastVisibleCol( ColScrollRegion csr, bool includeNonActivateable )
		{
			return this.GetLastVisibleCol( csr, includeNonActivateable, false );
		}

		// SSP 7/29/05 - NAS 5.3 Tab Index
		// Added an overload that takes in useTabOrderedColumns.
		// 
		internal UltraGridColumn GetLastVisibleCol( ColScrollRegion csr, bool includeNonActivateable, bool useTabOrderedColumns )
		{
			// SSP 11/17/03 - Row Layout Functionality
			//
			//HeadersCollection headers = this.OrderedHeaders;
			HeadersCollection headers = this.UseRowLayoutResolved ? this.LayoutOrderedVisibleColumnHeaders : this.OrderedHeaders;
			
			// SSP 7/29/05 - NAS 5.3 Tab Index
			// Added an overload that takes in useTabOrderedColumns.
			// 
			if ( useTabOrderedColumns )
				headers = this.TabOrderedColumns;

			if ( headers == null )
				return null;

			ColScrollRegion exclusiveColScrollRegion = csr != null && csr.HasExclusiveItems ? csr.ExclusiveColScrollRegion : null ;

			UltraGridColumn col = null;

			// JJD 1/14/02
			// Iterate over the ordered headers (in reverse order
			// looking for the last visible column
			//
			for ( int i = headers.Count - 1; i >= 0; i-- )
			{
				HeaderBase headerItem = headers[i];

				// Skip over hidden position items
				//
				if ( headerItem.Hidden )
					continue;

				if ( csr != null && headerItem.ExclusiveColScrollRegion != exclusiveColScrollRegion )
					continue;

				// if the position item is a group get the group's first visible
				// column
				//
				if ( headerItem .IsGroup )
					col = headerItem.GetLastVisibleCol( includeNonActivateable );

				else if ( includeNonActivateable || !headerItem.IsDisabled )
					col = headerItem.Column;
				else
					col = null;

				// if we have a column then we can stop the iteration
				//
				if ( col != null )
					break;
			}
			return col;

		}

		internal HeaderBase GetLastDisplayableCardLabel(UltraGridRow row)
		{
			HeaderBase		header = null;

			// MD 1/21/09 - Groups in RowLayouts
			// This is no longer needed
			//UltraGridColumn	column = null;

			for (int index = (this.OrderedHeaders.Count - 1); index >= 0; index--)
			{
				header = this.OrderedHeaders[index]; 
				if (header == null)
					continue;

				// If hidden, skip it.
				if (header.Hidden)
					continue;

				// If it's a group, drill down for the headers.
				if (header.IsGroup)
				{
					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
					// actually visible in the group
					//GroupColumnsCollection colList = header.Group.Columns;
					IEnumerable<UltraGridColumn> colList = header.Group.ColumnsResolved;

					if (colList != null)
					{
						// Iterate over the columns in this group and create an element for each
						// MD 1/21/09 - Groups in RowLayouts
						// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach and since we were iterating backwards,
						// the logic needs to be changed a little.
						//for ( int i = (colList.Count - 1); i >= 0; i-- )
						//{
						//    column = colList[i];
						UltraGridColumn lastColumn = null;
						foreach ( UltraGridColumn column in colList )
						{
							// If hidden, skip it.
							if (column.Header.Hidden)
								continue;

							// Make sure we should display this cell.
							if (column.ShouldCellValueBeDisplayedInCard(row) == false)
								continue;

							// MD 1/21/09 - Groups in RowLayouts
							//return column.Header;
							lastColumn = column;
						}

						// MD 1/21/09 - Groups in RowLayouts
						// MD 2/26/09 - TFS14631
						// If there are no columns in the list or the last column was never set, it will be null.
						//return lastColumn.Header;
						if ( lastColumn != null )
							return lastColumn.Header;
					}
				}
				else
				{
					// Make sure we should display this cell.
					if (header.Column.ShouldCellValueBeDisplayedInCard(row) == false)
						continue;

					return header;
				}
			}

			return null;
		}

		// SSP 4/23/04 UWG3010
		// Changed the name from IsReadOnly to IsBoundListReadOnly. When the underlying bound
		// list is read-only we should still allow the editing unbound columns. So this
		// property really indicates whether the bound columns should be read-only. And thus
		// the reason for renaming the property.
		//
		//internal bool IsReadOnly
		internal bool IsBoundListReadOnly
		{
			get
			{
				//RobA UWG296 9/24/01
				//return ( this.BindingList == null ||
				//	!this.BindingList.AllowEdit );

				if ( this.BindingList != null )
					return !this.BindingList.AllowEdit;

					//if this is not a bindinglist check the list's readonly property
					//
				else
				{
					// SSP 12/9/05 BR07902
					// Return true if List is null. We can have binding manager position
					// syncrhonization turned off or not have binding manager at all (since
					// we are making use of ITypedList now if the BindingContext fails to
					// create binding manager.
					// 
					// --------------------------------------------------------------------
					return null != this.List && this.List.IsReadOnly;
					
					// --------------------------------------------------------------------
				}				
			}
		}

		#region GetTotalColSyncPositions

		// SSP 8/15/05 BR04633 BR04666
		// Added GetTotalColSyncPositions method.
		// 
		internal int GetTotalColSyncPositions( ColScrollRegion exclusiveRegion )
		{
			int totalColSyncPositions = 0;

			HeadersCollection orderedHeaders = this.OrderedHeaders;
			if ( null != orderedHeaders )
			{
				// Iterate over each header to add their columns to the
				// sync linked lists
				for ( int index = 0; index < orderedHeaders.Count; index++ )
				{
					Infragistics.Win.UltraWinGrid.HeaderBase header = orderedHeaders[ index ];
					
					if ( ! header.Hidden )
						totalColSyncPositions += header.GetTotalColSyncPositions( exclusiveRegion );
				}
			}

			return totalColSyncPositions;
		}

		#endregion // GetTotalColSyncPositions

		// SSP 9/10/02 UWG1637
		// New implementation of BuildSynchronizedColLists. Look below it for the old
		// implementation. It's commented out.
		//
		internal void BuildSynchronizedColLists ( SynchedBandAbove pSynchedBandAbove )
		{
			// JJD 12/27/01
			// If this is a cardview or we are autofitting columns
			// we don't need to build synchronization lists
			//
			// SSP 8/13/05 BR04633 BR04666
			// This is to make column synchronization work when auto-fit functionality is turned on.
			// 
			//if ( this.CardView || this.layout.AutoFitAllColumns )
			if ( this.CardView )
				return;
            
			// SSP 6/29/06 - Optimization - 4000 cols
			// Don't syncrhonize columns if there is only one band since there is nothing to synchronize.
			// 
			BandsCollection bands = this.Layout.SortedBands;
			if ( null == bands || bands.Count <= 1 )
				return;

			ViewStyleBase vwStyle = this.layout.ViewStyleImpl;

			bool fSynchColsThisBand = (this.AllowColSizingResolved == AllowColSizing.Synchronized &&
				!this.GroupsDisplayed );

			bool fSynchColsAcrossBands = fSynchColsThisBand;

			
			
			
			HeadersCollection orderedHeaders = this.OrderedHeaders;
			if ( orderedHeaders != null && fSynchColsThisBand )
			{
				// Iterate over each header to add their columns to the
				// sync linked lists
				
				
				
				
				
				for ( int index = 0, count = orderedHeaders.Count; index < count; index++ )
				{
					Infragistics.Win.UltraWinGrid.HeaderBase header = orderedHeaders[index];
					
					if ( header.Hidden )
					{
						continue;
					}

					// call the AddColsToSyncColsList virtual method that
					// maintains the sync col linked lists
					//
					header.AddColsToSyncColsList( pSynchedBandAbove );
				}
			}

			// SSP 9/10/02 UWG1637
			// Sort them according to the band levels.
			//
			// ------------------------------------------------------------------------------
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList bandList = new ArrayList( this.Layout.Bands.Count );
			List<UltraGridBand> bandList = new List<UltraGridBand>( this.Layout.SortedBands.Count );

			for ( int i = 0; i < bands.Count; i++ )
			{
				// SSP 8/6/04 UWG3301
				// Skip card-view bands as they do not participate in the column synchronization.
				// Adding them in the bandList causes band synchronization algorithm to bail out
				// of the synchronization logic prematurely without synchronizing bands occuring
				// after the card-view band.
				//
				// ----------------------------------------------------------------------------
				if ( bands[i].CardView )
					continue;
				// ----------------------------------------------------------------------------

				if ( vwStyle.HasMultiRowTiers )
				{
					int bandLevel = bands[i].HierarchicalLevel;

					int j = 0;
					for ( j = 0; j < bandList.Count; j++ )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//UltraGridBand band = (UltraGridBand)bandList[j];
						UltraGridBand band = bandList[ j ];

						if ( band.HierarchicalLevel > bandLevel )
							break;
						else if ( band.HierarchicalLevel == bandLevel && band.Index > i )
							break;
					}

					bandList.Insert( j, bands[i] );
				}
				else
					bandList.Add( bands[i] );
			}
			// ------------------------------------------------------------------------------

			// if there are multiple bands displayed we need to call this
			// method on the bands below us
			//
			if ( vwStyle.IsMultiBandDisplay )
			{
				if ( bands != null )
				{
					bool fThisBandFound = false;

					SynchedBandAbove ThisSynchedBand = new SynchedBandAbove( this, pSynchedBandAbove );

					// loop over the bands collection to find bands below this band
					//
					// SSP 9/10/02 UWG1637
					// Use the bandList which is sorted according to band levels.
					//
					
					for ( int index = 0; index < bandList.Count; index++ )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//UltraGridBand band = (UltraGridBand)bandList[index];
						UltraGridBand band = bandList[ index ];

						// if the band is hidden then continue
						//
						if ( band.HiddenResolved )
							continue;

						// only process bands that follow this band in the bands collection
						//
						if ( !fThisBandFound )
						{
							fThisBandFound = ( this == band );
							continue;
						}

						if ( vwStyle.HasMultiRowTiers )
						{
							// for view styles that have multi-row tiers
							// (horizontal view) we only want to call this
							// method on the FIRST sibling band that follows
							// us since BuildSynchronizedColLists on that
							// band will handle any following sibling band
							//
							// If the parant band's are the same then the
							// band is a sibling of this band
							//
							if ( band.ParentBand == this.ParentBand )
							{
								band.BuildSynchronizedColLists( fSynchColsAcrossBands
									? ThisSynchedBand
									: pSynchedBandAbove );

							}
							else
							{
								// SSP 9/10/02 UWG1637
								// Pass in null if the parent bands don't match.
								//
								// if the next band isn't a sibling call this method
								// on it with the passed in band as the above band
								//
								//band.BuildSynchronizedColLists( pSynchedBandAbove );
								band.BuildSynchronizedColLists( null );
							}
						}
						else
						{
							// for vertical view styles we want to call this
							// method on next band (any next band). This will
							// effectively chain each band
							//
							band.BuildSynchronizedColLists( fSynchColsAcrossBands
								? ThisSynchedBand
								: pSynchedBandAbove );
						}

						break;
					}
				}
			}
		}

		

		internal UltraGridColumn GetColAtSyncPosition( int nSyncPosition,
			int nLevel,							// -1 means any level
			ColScrollRegion exclusiveRegion )
		{
			UltraGridColumn pColumn = null;

			int nTotSyncPositions = 0;
			int nSyncPositions    = 0;
    
			// Iterate over each header to count the col sync positions
			//
			for ( int index = 0; index < this.OrderedHeaders.Count; index++ )
			{
				HeaderBase header = this.OrderedHeaders[index];
				
				if ( header.Hidden || header.ExclusiveColScrollRegion != exclusiveRegion )
				{
					continue;
				}

				// get the total sync positions that this position item contains
				//
				nSyncPositions = header.GetTotalColSyncPositions( exclusiveRegion );

				// if the position we are looking for is contained the
				// this position item then return it
				//
				if ( nSyncPositions > 0 &&
					nSyncPositions + nTotSyncPositions > nSyncPosition )
				{
					return header.GetColAtRelativeSyncPosition( nSyncPosition - nTotSyncPositions, 
						nLevel, 
						nSyncPositions,
						exclusiveRegion );
					
				}
				// since this header didn't contain the column just update
				// the running total of sync positions
				//
				nTotSyncPositions += nSyncPositions;
			}

			return pColumn;
		}

		internal void SetSortedColumn( UltraGridColumn column, SortIndicator sortIndicator,
			bool clearExisting )
		{
			this.SortedColumns.SetSortedColumn( column, sortIndicator, column.IsGroupByColumn, clearExisting, true );
		}

		internal bool HasGroupBySortColumns
		{
			get
			{
				// SSP 4/2/04
				// Check for layout being null.
				//
				//if ( ViewStyleBand.OutlookGroupBy != this.Layout.ViewStyleBand )
				if ( null != this.Layout && ViewStyleBand.OutlookGroupBy != this.Layout.ViewStyleBand )
					return false;

				if ( null == this.sortedColumns || this.sortedColumns.Count <= 0 )
					return false;

				// SSP 12/12/01 UWG862
				// Since we are making sure that all the group by columns are in
				// the beggining of the sorted columns collection, just check for
				// the first one being a group by column.
				//				
				
				return this.sortedColumns[0].IsGroupByColumn;
			}
		}
		
		internal void SetSortedColumn( UltraGridColumn column, SortIndicator sortIndicator,
			bool isGroupBy,	bool clearExisting )
		{
			this.SortedColumns.SetSortedColumn( column, sortIndicator, isGroupBy, clearExisting, true );
		}

		
		// SSP 7/23/03 - Fixed headers
		// Added movedHeaderClones parameter which contains the cloned headers that were moved
		// and the drag stratergy passed them in the event args of Before Col/Group Pos Changed
		// event. It contains the information on whether those headers should be fixed or unfixed.
		//
		//internal void SetOrderedCols( ArrayList newOrderedColHeaders )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal void SetOrderedCols( ArrayList newOrderedColHeaders, HeaderBase[] movedHeaderClones )
		internal void SetOrderedCols( List<HeaderBase> newOrderedColHeaders, HeaderBase[] movedHeaderClones )
		{
			this.OrderedColumnHeaders.InternalClear();

			for ( int i = 0; i < newOrderedColHeaders.Count; i++ )
			{
				// orderedColumns USE lowercae var name directly
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//this.orderedColumns.InternalAdd( ((ColumnHeader)newOrderedColHeaders[i]).Column.Header );
				this.orderedColumns.InternalAdd( newOrderedColHeaders[ i ].Column.Header );
			}

			// Call notify data changed off all of the columns since all of their
			// positions may have changed
			//
			for ( int i = 0; i < this.OrderedColumnHeaders.Count; i++ )
			{
				UltraGridColumn column = this.OrderedColumnHeaders[i].Column;

				column.NotifyPropChange( PropertyIds.VisiblePosition );
			}

			// SSP 7/23/03 - Fixed headers
			// Call OnHeadersDropped which will make non-fixed headers dropped over fixed area
			// fixed and fixed headers dropped over non-fixed area non-fixed.
			//
			// ----------------------------------------------------------------------------------
			if ( this.UseFixedHeaders && null != movedHeaderClones )
				this.OnHeadersDropped( movedHeaderClones );
			else
				// Update the visiblePos member variables of all the headers to their new 
				// visible positions. The reason for doing this is that with the fixed 
				// headers functionality, we are re-sorting the ordered headers everytime 
				// a header is fixed or unfixed. And if we don't update the visiblePos member 
				// variables, then the order of the headers won't be maintained when we 
				// re-sort the headers again. So call SetVisiblePositionsToActual to update 
				// the visiblePos member variables of the headers.
				//
				this.SetVisiblePositionsToActual( );
			// ----------------------------------------------------------------------------------
		}

		// SSP 7/23/03 - Fixed headers
		// Added movedHeaderClones parameter which contains the cloned headers that were moved
		// and the drag stratergy passed them in the event args of Before Col/Group Pos Changed
		// event. It contains the information on whether those headers should be fixed or unfixed.
		//
		//internal bool SetOrderedGroups( ArrayList newOrderedGroupsHeaders )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal bool SetOrderedGroups( ArrayList newOrderedGroupsHeaders, HeaderBase[] movedHeaderClones )
		internal bool SetOrderedGroups( List<HeaderBase> newOrderedGroupsHeaders, HeaderBase[] movedHeaderClones )
		{
			this.OrderedGroupHeaders.InternalClear( );

			for ( int i = 0; i < newOrderedGroupsHeaders.Count; i++ )
			{
				// orderedGroups USE lowercae var name directly
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//this.orderedGroups.InternalAdd( ((GroupHeader)newOrderedGroupsHeaders[i]).Group.Header );
				this.orderedGroups.InternalAdd( newOrderedGroupsHeaders[ i ].Group.Header );
			}


			// Call notify data changed off all of the columns since all of their
			// positions may have changed
			//
			for ( int i = 0; i < this.OrderedGroupHeaders.Count; i++ )
			{
				this.OrderedGroupHeaders[i].NotifyPropChange( PropertyIds.VisiblePosition );
			}
    
			// SSP 7/23/03 - Fixed headers
			// Call OnHeadersDropped which will make non-fixed headers dropped over fixed area
			// fixed and fixed headers dropped over non-fixed area non-fixed.
			//
			// ----------------------------------------------------------------------------------
			if ( this.UseFixedHeaders && null != movedHeaderClones )
				this.OnHeadersDropped( movedHeaderClones );
			else
				// Update the visiblePos member variables of all the headers to their new 
				// visible positions. The reason for doing this is that with the fixed 
				// headers functionality, we are re-sorting the ordered headers everytime 
				// a header is fixed or unfixed. And if we don't update the visiblePos member 
				// variables, then the order of the headers won't be maintained when we 
				// re-sort the headers again. So call SetVisiblePositionsToActual to update 
				// the visiblePos member variables of the headers.
				//
				this.SetVisiblePositionsToActual( );
			// ----------------------------------------------------------------------------------

			return true;
		}


		/// <summary>
		/// Returns the <see cref="Infragistics.Win.UltraWinGrid.SortedColumnsCollection"/> which contains all the UltraGridColumn objects that are currently being sorted in the band.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The UltraGrid maintains sort and group-by columns in the SortedColumns collection. 
		/// You can use this property to sort and group rows by a column in code. To do that 
		/// add the column you want to sort or group rows by to the the SortedColumns collection 
		/// using the <see cref="SortedColumnsCollection.Add(UltraGridColumn, bool, bool)"/> method. The Add method takes in
		/// a parameter that specifies whether you want to just sort the rows or group rows as
		/// well. You can check the current sort state of a column using the 
		/// <see cref="UltraGridColumn.SortIndicator"/> property of a column.
		/// </p>
		/// <seealso cref="UltraGridOverride.HeaderClickAction"/>
		/// <seealso cref="UltraGridColumn.SortIndicator"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SortIndicator"/>
		/// <seealso cref="UltraGridColumn.IsGroupByColumn"/>
		/// </remarks>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public SortedColumnsCollection SortedColumns
		{
			get
			{
				if ( this.sortedColumns == null )
					this.sortedColumns = new SortedColumnsCollection( this );

				return this.sortedColumns;
			}
		}


		/// <summary>
		/// Returns true if there are any sorted columns in the band.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasSortedColumns
		{
			get
			{ 
				return this.sortedColumns != null &&
					this.sortedColumns.Count > 0; 
			}
		}

		// SSP 8/6/02 UWG1481
		// Added ResetSortedColumns.
		//
		/// <summary>
		/// Resets the sorted column's collection.
		/// </summary>
		public void ResetSortedColumns( )
		{
			if ( null != this.sortedColumns )
				this.sortedColumns.InternalClear( );
		}


		// SSP 4/15/02
		// Added IsSortedColumnsCollectionCreated.
		//
		internal bool IsSortedColumnsCollectionCreated
		{
			get
			{
				return null != this.sortedColumns;
			}
		}

		// SSP 5/13/03 - Optimizations
		//
		internal SortedColumnsCollection InternalSortedColumns
		{
			get
			{
				return this.sortedColumns;
			}
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			PropertyCategories serializeFlags = context.Context is PropertyCategories ? 
				(Infragistics.Win.UltraWinGrid.PropertyCategories)context.Context :
				PropertyCategories.All;

			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			//all values that were set are now save into SerializationInfo
			if( this.ShouldSerializeAddButtonCaption() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AddButtonCaption", this.addButtonCaption );
				//info.AddValue("AddButtonCaption", this.addButtonCaption );
			}

			if( this.ShouldSerializeAddButtonToolTipText() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info,"AddButtonToolTipText", this.addButtonToolTipText  );
				//info.AddValue("AddButtonToolTipText", this.addButtonToolTipText );
			}

			if( this.ShouldSerializeAutoPreviewField()  )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AutoPreviewField", this.autoPreviewField );
				//info.AddValue("AutoPreviewField", this.autoPreviewField );
			}

			if( this.ShouldSerializeAutoPreviewEnabled() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AutoPreviewEnabled", this.autoPreviewEnabled );
				//info.AddValue("AutoPreviewEnabled", this.autoPreviewEnabled );
			}

			if( this.ShouldSerializeAutoPreviewMaxLines() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AutoPreviewMaxLines", this.autoPreviewMaxLines );
				//info.AddValue("AutoPreviewMaxLines", this.autoPreviewMaxLines );
			}

			if ( this.ShouldSerializeCardView() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CardView", this.cardView  );
				//info.AddValue( "CardView", this.cardView );
			}

			if( this.ShouldSerializeColHeaderLines() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ColHeaderLines", this.colHeaderLines );
				//info.AddValue("ColHeaderLines", this.colHeaderLines );
			}

			if( this.ShouldSerializeColHeadersVisible() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ColHeadersVisible", this.colHeadersVisible );
				//info.AddValue("ColHeadersVisible", this.colHeadersVisible );
			}

			if( this.ShouldSerializeExpandable() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info,"Expandable", this.expandable  );
				//info.AddValue("Expandable", this.expandable );
			}

			if( this.ShouldSerializeIndentation() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Indentation", this.indentation );
				//info.AddValue("Indentation", this.indentation );
			}

			if( this.ShouldSerializeGroupHeaderLines() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "GroupHeaderLines", this.GroupHeaderLines );
				//info.AddValue("GroupHeaderLines", this.GroupHeaderLines );
			}

			if( this.ShouldSerializeGroupHeadersVisible() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "GroupHeadersVisible", this.groupHeadersVisible );
				//info.AddValue("GroupHeadersVisible", this.groupHeadersVisible );
			}

			if( this.ShouldSerializeHeaderVisible() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "HeaderVisible", this.headerVisible );
				//info.AddValue("HeaderVisible", this.headerVisible );
			}

			// JJD 10/29/01
			// Always serialize the band's key since we may use it to
			// match bands up at load time
			//
			//			if ( this.ShouldSerializeKey() )
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Key", this.Key );
			//info.AddValue("Key", this.Key);

			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			if( this.ShouldSerializeLevelCount() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "LevelCount", this.levelCount );
				//info.AddValue("LevelCount", this.levelCount );
			}

			if( this.ShouldSerializeScrollTipField() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ScrollTipField", this.scrollTipField );
				//info.AddValue("ScrollTipField", this.scrollTipField );
			}

			if( this.ShouldSerializeOverride() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Override", this.Override );
				//info.AddValue("Override", this.Override );
			}

			if( this.ShouldSerializeCardSettings() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CardSettings", this.cardSettings );
				//info.AddValue("CardSettings", this.cardSettings );
			}

			if ( this.ShouldSerializeAutoPreviewIndentation() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AutoPreviewIndentation", this.autoPreviewIndentation   );
				//info.AddValue("AutoPreviewIndentation", this.autoPreviewIndentation );
			}

			//ROBA UWG124 8/7/01
			if ( this.parentBand != null )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ParentIndex", this.parentBand.Index );
				//info.AddValue("ParentIndex", this.parentBand.Index );
			}
				// AS - 11/6/01
				// We may not have a parent band linked if this is coming from
				// an in memory layout but we may have a serialized parent index.
				// If so, we should be serializing this value. Without this,
				// we were losing the relation back to the parent band and therefore
				// only reinitializing the band 0 data.
				//
			else if ( this.parentIndex != -1 )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ParentIndex", this.parentIndex );
				//info.AddValue( "ParentIndex", this.parentIndex );
			}

			if( this.ShouldSerializeColumns() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Columns", this.columns );
				//info.AddValue("Columns", this.columns );
			}

			if( ( (serializeFlags & PropertyCategories.Groups) == PropertyCategories.Groups )
				&&	this.ShouldSerializeGroups() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info,"Groups", this.groups  );
				//info.AddValue("Groups", this.groups );
			}

			if( ( (serializeFlags & PropertyCategories.SortedColumns) == PropertyCategories.SortedColumns )
				&&	this.ShouldSerializeSortedColumns() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SortedCols", this.sortedColumns );
				//info.AddValue("SortedCols", this.sortedColumns );
			}

			// JJD 10/19/01
			// Serialize the band's header if necessary
			//
			if ( this.Header.ShouldSerialize() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Header", this.Header );
				//info.AddValue("Header", this.Header );
			}

			// SSP 11/20/01 UWG752
			// Serialize the Hidden property value as well.
			//
			if ( this.ShouldSerializeHidden( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Hidden", this.Hidden );
				//info.AddValue("Hidden", (bool)this.Hidden );
			}

			// SSP 6/11/02
			// Added code to serialize SummaryFooterCaption
			//
			if ( this.ShouldSerializeSummaryFooterCaption( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SummaryFooterCaption", this.summaryFooterCaption  );
				//info.AddValue( "SummaryFooterCaption", (string)this.summaryFooterCaption );
			}

			// SSP 7/16/02 UWG1367
			// Added serialization code for summaries and filters.
			//
            if (this.ShouldSerializeSummaries() &&
                // MRS 5/8/2008 - BR32651
                (serializeFlags & PropertyCategories.Summaries) == PropertyCategories.Summaries)
            {
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Summaries", this.Summaries );
				//info.AddValue( "Summaries", this.Summaries );
			}

            // MRS 1/4/2008 - BR29356
            // Only serialize the ColumnFilters if the PropertyCategories.ColumnFilters
            // flag is set.            
            //if ( this.ShouldSerializeColumnFilters( ) )
            if (((serializeFlags & PropertyCategories.ColumnFilters) == PropertyCategories.ColumnFilters)
             && this.ShouldSerializeColumnFilters())
            {
                // JJD 8/20/02
                // Use SerializeProperty static method to serialize properties instead.
                Utils.SerializeProperty(info, "ColumnFilters", this.ColumnFilters);
                //info.AddValue( "ColumnFilters", this.ColumnFilters );
            }

			// JAS 2005 v2 XSD Support
			//
			if( this.ShouldSerializeMinRows() )
			{
				Utils.SerializeProperty( info, "MinRows", this.MinRows );
			}

			if( this.ShouldSerializeMaxRows() )
			{
				Utils.SerializeProperty( info, "MaxRows", this.MaxRows );
			}

			if( this.XsdSuppliedConstraints != 0 )
			{
				Utils.SerializeProperty( info, "XsdSuppliedConstraints", this.XsdSuppliedConstraints );
			}

			// SSP 1/31/03 - Row Layout Functionality
			//
			// ----------------------------------------------------------------------------
			if ( this.ShouldSerializeRowLayoutLabelPosition( ) )
			{
				Utils.SerializeProperty( info, "RowLayoutLabelPosition", this.RowLayoutLabelPosition );
			}

			if ( this.ShouldSerializeRowLayoutLabelStyle( ) )
			{
				Utils.SerializeProperty( info, "RowLayoutLabelStyle", this.RowLayoutLabelStyle );
			}

			// MD 2/23/09 - TFS14254
			// This member variable is no longer used.
			//if ( this.ShouldSerializeUseRowLayout( ) )
			//{
			//    Utils.SerializeProperty( info, "UseRowLayout", (bool)this.useRowLayout );
			//}

			if ( this.ShouldSerializeRowLayouts( ) )
			{
				Utils.SerializeProperty( info, "RowLayouts", this.rowLayouts );
			}
			// ----------------------------------------------------------------------------

			// SSP 3/14/05 - NAS 5.2 Outlook Style Fixed Add Row
			//
			if ( this.ShouldSerializeScrollTipField( ) )
			{
				Utils.SerializeProperty( info, "SpecialRowPromptField", this.SpecialRowPromptField );
			}

			// JAS 2005 v2 GroupBy Row Extensions
			//
			// ----------------------------------------------------------------------------
			if( this.ShouldSerializeIndentationGroupByRow() )
			{
				Utils.SerializeProperty( info, "IndentationGroupByRow", this.IndentationGroupByRow );
			}

			if( this.ShouldSerializeIndentationGroupByRowExpansionIndicator() )
			{
				Utils.SerializeProperty( info, "IndentationGroupByRowExpansionIndicator", this.IndentationGroupByRowExpansionIndicator );
			}
			// ----------------------------------------------------------------------------

			// SSP 6/17/05 - NAS 5.3 Column Chooser
			// 
			if ( this.ShouldSerializeExcludeFromColumnChooser( ) )
			{
				Utils.SerializeProperty( info, "ExcludeFromColumnChooser", this.excludeFromColumnChooser );
			}

            // MRS 5/16/2008 - Found during unit testing
            if (this.ShouldSerializeSpecialRowPromptField())
			{
                Utils.SerializeProperty(info, "SpecialRowPromptField", this.specialRowPromptField);
			}            

            // MBS 5/16/08 - NA2008 V2 RowEditTemplate
            if (this.ShouldSerializeRowEditTemplate())
            {
                // MBS 9/22/08 - TFS6432
                // It's possible that we don't actually have an instance of the template at this point, such
                // as if we're loading and saving a layout but we don't actually hit a point where we try to
                // locate the template by its name, so we should serialize the name if we have it
                //
                //Utils.SerializeProperty(info, "RowEditTemplateControlName", this.rowEditTemplate.Name);
                string templateName = this.rowEditTemplate != null ? this.rowEditTemplate.Name : this.rowEditTemplateControlName;
                Debug.Assert(String.IsNullOrEmpty(templateName) == false, "We shouldn't be trying to serialize an empty template control name");
                Utils.SerializeProperty(info, "RowEditTemplateControlName", templateName);
            }

            // MRS NAS 9.1 - Sibling Band Order
            // Added a VisiblePosition property to the band.             
            // Serialize the VisiblePosition property
            //
            if (this.ShouldSerializeVisiblePosition())
            {   
                Utils.SerializeProperty(info, "VisiblePosition", this.VisiblePosition);                
            }

            // MRS - NAS 9.1 - Groups in RowLayout
            if (this.ShouldSerializeRowLayoutStyle())
            {
                Utils.SerializeProperty(info, "RowLayoutStyle", (RowLayoutStyle)this.RowLayoutStyle);
            }
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal UltraGridBand( SerializationInfo info, StreamingContext context )
		protected UltraGridBand( SerializationInfo info, StreamingContext context )
		{
			PropertyCategories serializeFlags = context.Context is PropertyCategories ? 
				(Infragistics.Win.UltraWinGrid.PropertyCategories)context.Context :
				PropertyCategories.All;

			//since we're not saving properties with default values, we must set the default here
			this.Reset();

			//this will have to be set in InitLayout()
			this.layout			= null;
			this.parentBand     = null;
			this.parentColumn   = null;

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "AddButtonCaption":
						//this.addButtonCaption = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.addButtonCaption = (string)Utils.DeserializeProperty( entry, typeof(string), this.addButtonCaption );
						//this.addButtonCaption = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "AddButtonToolTipText":
						//this.addButtonToolTipText = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.addButtonToolTipText = (string)Utils.DeserializeProperty( entry, typeof(string), this.addButtonToolTipText );
						//this.addButtonToolTipText = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "AutoPreviewField":
						//this.autoPreviewField = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.autoPreviewField = (string)Utils.DeserializeProperty( entry, typeof(string), this.autoPreviewField );
						//this.autoPreviewField = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "AutoPreviewEnabled":
						//this.autoPreviewEnabled = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.autoPreviewEnabled = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.autoPreviewEnabled );
						//this.autoPreviewEnabled = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "AutoPreviewIndentation":
						//this.autoPreviewIndentation = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.autoPreviewIndentation = (int)Utils.DeserializeProperty( entry, typeof(int), this.autoPreviewIndentation );
						//this.autoPreviewIndentation = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "AutoPreviewMaxLines":
						//this.autoPreviewMaxLines = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.autoPreviewMaxLines = (int)Utils.DeserializeProperty( entry, typeof(int), this.autoPreviewMaxLines );
						//this.autoPreviewMaxLines = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "CardView":
						//this.cardView = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.cardView = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.cardView );
						//this.cardView = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "ColHeaderLines":
						//this.colHeaderLines = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.colHeaderLines = (int)Utils.DeserializeProperty( entry, typeof(int), this.colHeaderLines );
						//this.colHeaderLines = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "ColHeadersVisible":
						//this.colHeadersVisible = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.colHeadersVisible = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.colHeadersVisible );
						//this.colHeadersVisible = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "Expandable":
						//this.expandable = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.expandable = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.expandable );
						//this.expandable = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "Indentation":
						//this.indentation = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.indentation = (int)Utils.DeserializeProperty( entry, typeof(int), this.indentation );
						//this.indentation = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "GroupHeaderLines":
						//this.GroupHeaderLines = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.GroupHeaderLines = (int)Utils.DeserializeProperty( entry, typeof(int), this.GroupHeaderLines );
						//this.GroupHeaderLines = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "GroupHeadersVisible":
						//this.groupHeadersVisible = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.groupHeadersVisible = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.groupHeadersVisible );
						//this.groupHeadersVisible = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "HeaderVisible":
						//this.headerVisible = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.headerVisible = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.headerVisible );
						//this.headerVisible = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "Key":
						//this.Key = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Key = (string)Utils.DeserializeProperty( entry, typeof(string), this.Key );
						//this.Key = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;

					case "LevelCount":
						//this.levelCount = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.levelCount = (int)Utils.DeserializeProperty( entry, typeof(int), this.levelCount );
						//this.levelCount = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "ScrollTipField":
						//this.scrollTipField = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.scrollTipField = (string)Utils.DeserializeProperty( entry, typeof(string), this.scrollTipField );
						//this.scrollTipField = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "Override":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.overrideObj = (UltraGridOverride)Utils.DeserializeProperty( entry, typeof(UltraGridOverride), null );
						//this.overrideObj = (Infragistics.Win.UltraWinGrid.UltraGridOverride)entry.Value;
						break;

					case "CardSettings":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.cardSettings = (UltraGridCardSettings)Utils.DeserializeProperty( entry, typeof(UltraGridCardSettings), null );
						//this.cardSettings = (Infragistics.Win.UltraWinGrid.UltraGridCardSettings)entry.Value;
						break;

						//ROBA UWG124 8/7/01
					case "ParentIndex":
						//this.parentIndex = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.parentIndex = (int)Utils.DeserializeProperty( entry, typeof(int), this.parentIndex );
						//this.parentIndex = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "Columns":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.columns = (ColumnsCollection)Utils.DeserializeProperty( entry, typeof(ColumnsCollection), null );
						//this.columns = (Infragistics.Win.UltraWinGrid.ColumnsCollection)entry.Value;
						break;

					case "Groups":
						if ( (serializeFlags & PropertyCategories.Groups) != 0 )
						{
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.groups = (GroupsCollection)Utils.DeserializeProperty( entry, typeof(GroupsCollection), null );
							//this.groups = (Infragistics.Win.UltraWinGrid.GroupsCollection)entry.Value;
						}
						break;						

					case "SortedCols":
						if ( (serializeFlags & PropertyCategories.SortedColumns) != 0 )
						{
							// JJD 8/19/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.sortedColumns = (SortedColumnsCollection)Utils.DeserializeProperty( entry, typeof(SortedColumnsCollection), null );
							//this.sortedColumns = (Infragistics.Win.UltraWinGrid.SortedColumnsCollection)entry.Value;
						}
						break;

						// JJD 10/19/01
						// De-serialize the band's header
						//
					case "Header":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.header = (BandHeader)Utils.DeserializeProperty( entry, typeof(BandHeader), null );
						//this.header = (BandHeader)entry.Value;
						break;
					}

						// SSP 11/20/01 UWG752
						// De-serialize the Hidden property value as well.
						//
					case "Hidden":
					{
						//this.hidden = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.hidden = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.hidden );
						//this.hidden = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
					}

						// SSP 6/11/02
						// Added code to serialize SummaryFooterCaption property.
						//
					case "SummaryFooterCaption":
					{
						//this.summaryFooterCaption = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.summaryFooterCaption = (string)Utils.DeserializeProperty( entry, typeof(string), this.summaryFooterCaption );
						//this.summaryFooterCaption = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					}
						// SSP 7/16/02 UWG1367
						// 
					case "Summaries":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.summaries = (SummarySettingsCollection)Utils.DeserializeProperty( entry, typeof(SummarySettingsCollection), null );
						//this.summaries = (SummarySettingsCollection)entry.Value;
						break;
					}
					case "ColumnFilters":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.columnFilters = (ColumnFiltersCollection)Utils.DeserializeProperty( entry, typeof(ColumnFiltersCollection), null );
						//this.columnFilters = (ColumnFiltersCollection)entry.Value;
						break;
					}
						// SSP 1/31/03 - Row Layout Functionality
						// Deserialize UseRowLayout property.
						// Added a case entry for UseRowLayout proeprty.
						//
					case "UseRowLayout":
					{
						// MD 2/23/09 - TFS14254
						// This member variable is no longer used.
						// Translate the value into the equivalent RowLayoutStyle value.
						//this.useRowLayout = (bool)Utils.DeserializeProperty( entry, typeof( bool ), false );
						Debug.Assert( this.rowLayoutStyle == RowLayoutStyle.None, "If UseRowLayout was serialized, RowLayoutStyle should not have also been serialized." );

						bool useRowLayout = (bool)Utils.DeserializeProperty( entry, typeof( bool ), false );

						this.rowLayoutStyle = useRowLayout
							? RowLayoutStyle.ColumnLayout
							: RowLayoutStyle.None;

						break;
					}
						// JAS 2005 v2 XSD Support
						//
					case "MinRows":
					{
						this.minRows = (Int32)Utils.DeserializeProperty( entry, typeof(Int32), 0 );
						break;
					}
					case "MaxRows":
					{
						this.maxRows = (Int32)Utils.DeserializeProperty( entry, typeof(Int32), Int32.MaxValue );
						break;
					}
					case "XsdSuppliedConstraints":
					{
						this.xsdSuppliedConstraints = (XsdConstraintFlags)Utils.DeserializeProperty( entry, typeof(XsdConstraintFlags), this.xsdSuppliedConstraints );
						break;
					}
						// SSP 3/13/03 - Row Layout Functionality
						//
					case "RowLayouts":
					{
						this.rowLayouts = (RowLayoutsCollection)Utils.DeserializeProperty( entry, typeof( RowLayoutsCollection ), null );
						break;
					}
					case "RowLayoutLabelPosition":
					{
						this.rowLayoutLabelPosition = (LabelPosition)Utils.DeserializeProperty( entry, typeof( Infragistics.Win.UltraWinGrid.LabelPosition ), this.rowLayoutLabelPosition );
						break;
					}
					case "RowLayoutLabelStyle":
					{
						this.rowLayoutLabelStyle = (RowLayoutLabelStyle)Utils.DeserializeProperty( entry, typeof( RowLayoutLabelStyle ), this.rowLayoutLabelStyle );
						break;
					}
						// SSP 3/14/05 - NAS 5.2 Outlook Style Fixed Add Row
						//
					case "SpecialRowPromptField":
					{
						this.specialRowPromptField = (string)Utils.DeserializeProperty( entry, typeof( string ), this.specialRowPromptField );                        
						break;
					}
						// JAS 2005 v2 GroupBy Row Extensions
						//
						// -------------------------------------------------------------------------
					case "IndentationGroupByRow":
					{
						this.indentationGroupByRow = (int)Utils.DeserializeProperty( entry, typeof(int), this.indentationGroupByRow );
						break;
					}
					case "IndentationGroupByRowExpansionIndicator":
					{
						this.indentationGroupByRowExpansionIndicator = (int)Utils.DeserializeProperty( entry, typeof(int), this.indentationGroupByRowExpansionIndicator );
						break;
					}
						// -------------------------------------------------------------------------
						// SSP 6/17/05 - NAS 5.3 Column Chooser
						// 
					case "ExcludeFromColumnChooser":
						this.excludeFromColumnChooser = (ExcludeFromColumnChooser)Utils.DeserializeProperty( entry, typeof( ExcludeFromColumnChooser ), this.excludeFromColumnChooser );
						break;

                    // MBS 5/16/08 - RowEditTemplate NA2008 V2
                    case "RowEditTemplateControlName":
                        this.rowEditTemplateControlName = (string)Utils.DeserializeProperty(entry, typeof(string), this.rowEditTemplateControlName);
                        break;

                    // MRS NAS 9.1 - Sibling Band Order
                    // Added a VisiblePosition property to the band. 
                    //
                    case "VisiblePosition":
                        this.visiblePosition = (int)Utils.DeserializeProperty(entry, typeof(int), this.visiblePosition);
                        break;

                    // MRS - NAS 9.1 - Groups in RowLayout
                    case "RowLayoutStyle":
                        {
                            this.rowLayoutStyle = (RowLayoutStyle)Utils.DeserializeProperty(entry, typeof(RowLayoutStyle), RowLayoutStyle.None);
                            break;
                        }

					default:
					{
						Debug.Assert( false, "Invalid entry in Band de-serialization ctor" );
						break;
					}				
				}
			}
		}

		
		
		private void InitSortedColumnsOnDeserialization( )
		{

			int totalSortedColumns = 0;
			int maxSortedIndex = 0;

			// first count the number of sorted columns
			//
			if ( this.columns != null )
			{
				foreach ( UltraGridColumn column in this.columns )
				{
					if ( column.SortIndex >= 0 )
					{
						totalSortedColumns++;

						maxSortedIndex = Math.Max( maxSortedIndex, column.SortIndex );
					}
				}
			}

			if ( totalSortedColumns < 1 )
				return;
			
			// the max index should be exactly one less than the total
			// number of sorted columns
			//
			if ( maxSortedIndex != totalSortedColumns - 1 )
			{
				Debug.Fail("Max sorted column index out of range" );
				return;
			}

			UltraGridColumn [] slots = new UltraGridColumn[ totalSortedColumns ];

			// Slot in the columns in the temporary array 
			//
			foreach ( UltraGridColumn column in this.columns )
			{
				if ( column.SortIndex >= 0 )
				{
					if ( slots[ column.SortIndex ] != null )
					{
						Debug.Fail("Duplicate sorted column index found. Index: " + column.SortIndex.ToString() );
						return;
					}

					slots[ column.SortIndex ] = column;
				}
			}

			if ( this.sortedColumns == null )
				this.sortedColumns = new SortedColumnsCollection( this );

			this.sortedColumns.InitDuringDeserialization( slots );

		}		
		
		internal void InitLayout( UltraGridLayout layout,
			bool calledFromEndInit )
		{
			this.layout         = layout;

			// JJD 1/18/02 - UWG942
			// Moved InitBand on columns to before groups so that the columns
			// are initialized before the groups try adding them to their collections.
			//
			if( this.columns != null )
				this.columns.InitBand(this, calledFromEndInit);

			// JJD 11/21/01 
			// Added flag so that we know if we are being called from inside
			// ISupportInitialize.EndInit
			//
			if( this.groups != null )
				this.groups.InitBand(this, calledFromEndInit);

            // MRS - NAS 9.1 - Groups in RowLayout
            this.InitRowLayoutColumnInfo();

			if ( calledFromEndInit )
			{
				// JJD 11/21/01
				// If we are called from inside EndInit then we don't
				// need to hook up the override and header. However,
				// we do need to initialize the sorted columns collection. 
				//
				this.InitSortedColumnsOnDeserialization();

                // MBS 2/20/08 - RowEditTemplate NA2008 V2
                if (this.rowEditTemplate != null)
                    this.rowEditTemplate.SetGrid(this, layout.Grid as UltraGrid);                
			}
			else
			{
				if ( this.sortedColumns != null )
					this.sortedColumns.InitBand( this );

				//ROBA UWG124 8/8/01
				if ( this.overrideObj != null )
					this.overrideObj.InitLayout(this.layout);

				// JJD 10/19/01
				// init the header's band also
				//
				if ( this.header != null )
					this.header.InitBand( this );

				if ( this.cardSettings != null )
					this.cardSettings.InitBand( this );

				// SSP 10/21/04 UWG3640
				// Call OnDeserializationComplete so the summaries can deseiralize the serialized
				// column ids.
				//
				if ( null != this.summaries )
					this.summaries.OnDeserializationComplete( this );
			}
		}        

		internal void InitializeFrom( UltraGridBand source, PropertyCategories propCat )
		{
			if ( ( propCat & PropertyCategories.Bands )== 0 )
				return;

			if ( this.layout == null ||
				// SSP 5/7/03 - Export Functionality
				//
				//( !this.layout.IsDisplayLayout && !this.layout.IsPrintLayout ) )
				( !this.layout.IsDisplayLayout && !this.layout.IsPrintLayout && !this.layout.IsExportLayout ) )
				this.Key = source.Key;

			
			// SSP 1/25/05 BR00268 
			// Copy member variables instead of the get of the properties because some
			// of them resolve to a non-default value in the get. Also set the member
			// variable instead of the property because we don't want to generate 
			// unnecessary prop change notifications.
			//
			// ------------------------------------------------------------------------
			
			this.addButtonCaption = source.addButtonCaption;
			this.addButtonToolTipText = source.addButtonToolTipText;
			this.autoPreviewMaxLines = source.autoPreviewMaxLines;
			this.autoPreviewEnabled = source.autoPreviewEnabled;
			// ------------------------------------------------------------------------

			// SSP 8/6/03 UWG2081
			// Set the member variable instead of the AutoPreviewField property
			// because property throws an exception if the column with such a key
			// does not exist.
			//
			//this.AutoPreviewField = source.AutoPreviewField;
			this.autoPreviewField = source.AutoPreviewField;

			this.ScrollTipField = source.ScrollTipField;

			// SSP 6/11/02
			//
			this.summaryFooterCaption = source.summaryFooterCaption;

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			this.LevelCount = source.LevelCount;
			this.cardView = source.cardView;

			// JJD 11/28/01
			// Init the following fields
			// UWG781 UWG783 UWG772, UWG785
			this.colHeaderLines = source.colHeaderLines;
			this.groupHeaderLines = source.groupHeaderLines;
			this.indentation = source.indentation;
			
			//ROBA UWG269 8/29/01			
			this.HeaderVisible = source.headerVisible;

			// SSP 11/28/01 UWG785
			// Init the following fields as well.
			//
			this.groupHeadersVisible = source.groupHeadersVisible;
			this.colHeadersVisible   = source.colHeadersVisible;
			// SSP 6/13/05
			// Typo.
			// 
			//this.expandable = expandable;
			this.expandable = source.expandable;

			this.autoPreviewIndentation = source.autoPreviewIndentation;

			// SSP 2/28/03 - Row Layout Functionality
			//
			// --------------------------------------------------------------------
			this.rowLayoutLabelPosition = source.rowLayoutLabelPosition;
			this.rowLayoutLabelStyle = source.rowLayoutLabelStyle;

			// MD 2/23/09 - TFS14254
			// This member variable is no longer used.
			//this.useRowLayout = source.useRowLayout;

            // MRS 2/19/2009 - TFS14297
            // Moved this up from below. This needs to happen before we initialize the groups
            // and columns. 
            //
            this.rowLayoutStyle = source.rowLayoutStyle;
			
			// SSP 2/23/06 BR10380
			// Moved this below after we deserialize the columns. The deserialization 
			// of RowLayout objects depends on the columns having already been
			// deserialized.
			// 
			
			// --------------------------------------------------------------------

			// SSP 6/17/05 - NAS 5.3 Column Chooser
			// 
			this.excludeFromColumnChooser = source.excludeFromColumnChooser;

			// SSP 3/14/05 - NAS 5.2 Outlook Style Fixed Add Row
			//
			this.specialRowPromptField = source.specialRowPromptField;
	
			if ( source.HasOverride )
			{
				if ( this.Override == null )
				{
					Debug.Assert(false, "Can't allocate Override");
				}
				this.Override.InitializeFrom( source.Override, propCat );
			}
				//RobA UWG248 8/27/01
			else
				this.Override.Reset();

			
			if ( source.HasCardSettings )
			{
				if ( this.CardSettings == null )
				{
					Debug.Assert(false, "Can't allocate CardSettings");
				}
				this.CardSettings.InitializeFrom( source.CardSettings, propCat );
			}				
			else
				this.CardSettings.Reset();


			this.Columns.InitializeFrom( source.Columns, propCat );

            // MRS - NAS 9.1 - Groups in RowLayout
            // Moved this down even more, since the RowLayouts now rely on the Groups, too. 
            //
            //// SSP 2/23/06 BR10380
            //// Moved this from above. The deserialization of RowLayout objects depends 
            //// on the columns having already been deserialized.
            //// 
            //if ( null != source.rowLayouts && source.rowLayouts.Count > 0 )
            //    this.RowLayouts.InitializeFrom( source.rowLayouts );
            //else if ( null != this.rowLayouts )
            //    this.rowLayouts.Clear( );

			// Since ssPropCatSortedColumns has more than one bit tunred on we need
			// to check the anded bits for equality
			//
			if( PropertyCategories.SortedColumns == (propCat & PropertyCategories.SortedColumns) )
			{
			
				
				// RobA UWG726 11/13/01 created a boolean
				//
				// AS 11/6/01
				// Added check to make sure this is a bound layout
				//
				// SSP 10/26/01
				// Added !IsPrinting clause
				//
				// SSP 9/25/02 UWG1702
				// Why are we checking for having a grid ?. We are not initializing
				// sorted columns from the source if the layout is not attached to
				// the grid which could very well be the case. As far as I can tell,
				// there doesn't seem to be a need for us to check if the grid is
				// non-null or if the grid is UltraGrid or UltraDropDown.
				//				
				//bool validGridObject = ( this.layout.Grid != null &&
				//	(  this.layout.Grid is UltraGrid /*&& 
				//	//RobA UWG868 12/26/01 Not sure why we were checking this flag.
				//	!((UltraGrid)(this.layout.Grid)).IsPrinting )*/  ||
				//	this.Layout.Grid is UltraDropDown )  );


				if (
					// SSP 9/25/02 UWG1702
					// Related to change above. Commented out validGridObject clause.
					//
					//validGridObject &&
					source.sortedColumns != null &&
					source.HasSortedColumns )
				{
					// init the collection from the source
					//
					this.SortedColumns.InitializeFrom( source.SortedColumns, propCat );
				}
				else if( source.sortedColumns != null && //source.HasSortedColumns )
					// SSP 9/25/02 UWG1702
					// Related to change above. Commented out validGridObject clause.
					//
					//validGridObject &&
					source.sortedColumns.HasSerializedColumnIDs )
				{
					// init the collection from the source
					//
					this.SortedColumns.InitializeFrom( source.SortedColumns, propCat );
				}
					// SSP 2/24/03 UWG1950
					// Added the else block.
					//
				else
				{
					// Otherwise clear any sorted and group-by columns since the source band doesn't
					// have any sort or group-by columns.
					//
					if ( null != this.sortedColumns )
						this.sortedColumns.Clear( );
				}

				// SSP 10/26/01
				// Dirty the metrics off all the col scrol regions since
				// loading of sorted columns could have potentially lead
				// to group by columns in the new sorted columns collection
				// to be hidden ( since by default group by columns are hidden ).
				// Calling this will make sure the visible headers are
				// recreated.
				//
				this.Layout.SynchronizedColListsDirty = true;
				this.Layout.ColScrollRegions.DirtyMetrics( );
			}


			// Added logic to persist the order of the columns
			//
			if ( ( (propCat & PropertyCategories.Bands) !=0 )  && !this.GroupsDisplayed )
			{
				if ( source.OrderedColumnHeaders.Count < 1 )
				{
					// If the source doesn't have a persisted column
					// order then reset back to the underlying order
					//
					//RobA 10/22/01 added call to InitializeOrderedColumns don't
					//think we need to call resetheader()
					this.InitializeOrderedColumns();
					//this.ResetHeader();
				}
				else
				{
					// SSP 10/24/03 UWG2724
					// Commented out below code. This code is incorrect. SetVisiblePosition call
					// inside the innermost loop will reorder items in OrderedColumnHeaders list
					// upsetting the whole logic.
					// I think all we need to do here is mark ordered cols dirty so they get 
					// reinitialized next time OrderedColumnHeaders is accessed. Copying over
					// the visiblePos is taken care of by UltraGridColumn.InitializeFrom.
					//
					// ------------------------------------------------------------------------
					this.orderedColumnsDirty = true;
					
					// ------------------------------------------------------------------------
				}
			}

			// Since ssPropCatGroups has more than one bit tunred on we need
			// to check the anded bits for equality
			//
			if ( PropertyCategories.Groups == ( propCat & PropertyCategories.Groups ) )
			{
				if ( source.HasGroups )
				{
					// init the collection from the source
					//
					if ( this.Groups != null )
						this.Groups.InitializeFrom(source.Groups, propCat );
				}
				else
				{
					if ( this.HasGroups )
					{
						this.Groups.Clear();
					}
				}

                // MRS - NAS 9.1 - Groups in RowLayout
                this.InitRowLayoutColumnInfo();

				// JJD 10/24/01
				// Clear the orderedGroups collection so that it will get recreated
				// (and resorted) the next time someone asks for it.
				//
				this.orderedGroups = null;
			}

            // MRS - NAS 9.1 - Groups in RowLayout
            // Moved this down even more, since the RowLayouts now rely on the Groups, too. 
            //
            // SSP 2/23/06 BR10380
            // Moved this from above. The deserialization of RowLayout objects depends 
            // on the columns having already been deserialized.
            // 
            if (null != source.rowLayouts && source.rowLayouts.Count > 0)
                this.RowLayouts.InitializeFrom(source.rowLayouts);
            else if (null != this.rowLayouts)
                this.rowLayouts.Clear();

			// JJD 10/19/01
			// Init the band's header
			//
			this.Header.InitializeFrom ( source.Header, propCat);

			// JJD 10/17/01
			// Make sure the columns and groups are ordered properly
			//
			this.InitializeOrderedColumns();
			this.InitializeOrderedGroups();

			// SSP 11/20/01 UWG752
			// Initialize the Hidden property from the source as well.
			//
			this.Hidden = source.hidden;

			// SSP 6/21/02
			// Summary Rows Feature.			
			// If both are null, then don't cause the summary settings collections
			// to be created.
			//
            // MRS 5/8/2008 - BR32651
            // PropertyCategories.Summaries includes PropertyCategories.Bands, because you 
            // can't serialize the Summaries without the bands. But you can serialize the 
            // Bands withou the Summaries. So we can't just check for != 0 here, we have to check
            // specifically for PropertyCategories.ColumnFilters only. 
            //if ( 0 != ( PropertyCategories.Summaries & propCat ) )
            if ((PropertyCategories.Summaries & propCat) == PropertyCategories.Summaries)
            {
				if ( null != this.summaries || null != source.summaries )
					this.Summaries.InitializeFrom( source.Summaries, propCat );
			}

            // MRS 1/4/2008 - BR29356
            // PropertyCategories.ColumnFilters includes PropertyCategories.ColumnFilters.Bands, because you 
            // can't serialize the ColumnFilters without the bands. But you can serialize the 
            // Bands withou the ColumnFilters. So we can't just check for != 0 here, we have to check
            // specifically for PropertyCategories.ColumnFilters only. 
            //// SSP 7/16/02 UWG1367
            //// 
            //if ( 0 != ( PropertyCategories.ColumnFilters & propCat ) )
            if (PropertyCategories.ColumnFilters == (PropertyCategories.ColumnFilters & propCat))
            {
                if (null != this.columnFilters || null != source.columnFilters)
                    this.ColumnFilters.InitializeFrom(source.ColumnFilters, propCat);
            }

			// SSP 1/28/02 UWG1007
			// Clear any cached info like font heights
			//
			this.ClearCachedHeaderFontHeights( );
			this.ClearCachedHeightValues( );
			
			// SSP 2/28/03 - Row Layout Functionality
			//
			this.DirtyRowLayoutCachedInfo( );

			// SSP 7/25/03 - Fixed headers
			//
			this.BumpFixedHeadersVerifyVersion( );

			// JAS 2005 v2 XSD Support
			//
			this.minRows = source.minRows;
			this.maxRows = source.maxRows;
			this.xsdSuppliedConstraints = source.xsdSuppliedConstraints;

			// JAS 2005 v2 GroupBy Row Extensions
			//
			this.indentationGroupByRow = source.indentationGroupByRow;
			this.indentationGroupByRowExpansionIndicator = source.indentationGroupByRowExpansionIndicator;

            // MBS 2/20/08 - RowEditTemplate NA2008 V2
            this.rowEditTemplate = source.rowEditTemplate;

            // MBS 5/23/08 - BR33274
            // In case the template didn't serialize the BandHierarchyLevel, or it was removed somehow,
            // assign the template's BandHierarchyLevel to that of this band so that it can correctly
            // locate this when initialization is complete.
            if (this.rowEditTemplate != null && this.rowEditTemplate.BandHierarchyLevel == -1)
                this.rowEditTemplate.BandHierarchyLevel = this.HierarchicalLevel;

            // MBS 5/16/08 - RowEditTemplate NA2008 V2
            this.rowEditTemplateControlName = source.rowEditTemplateControlName;

            // MRS NAS 9.1 - Sibling Band Order
            // MRS 1/30/2009 - TFS12662
            //this.visiblePosition = source.visiblePosition;
            this.VisiblePosition = source.visiblePosition;

            // MRS 2/19/2009 - TFS14297
            // Moved this up. 
            //// MRS - NAS 9.1 - Groups in RowLayout
            //this.RowLayoutStyle = source.rowLayoutStyle;
		}

		internal void SetVisiblePosition( UltraGridGroup group, int newPos, ref bool changed )
		{
			changed = false;

			// get the group's current position
			//
			int oldPos = GetVisiblePosition( group );

			// if it hasn't changed then just return 
			//
			if (newPos == oldPos )
				return;

			// Check for valid value
			// AS - 10/30/01
			// Use the upper case so that the group will be resynced
			// if it has a different count then the current groups
			// collection.
			//
			//if ( newPos < 0 || newPos >= this.orderedGroups.Count )
			if ( newPos < 0 || newPos >= this.OrderedGroupHeaders.Count )
				Debug.Fail ( "Invalid position for Bands.SetVisiblePosition" );

			// First, remove the group from its current position
			//
			this.orderedGroups.InternalRemove ( group.Header );

			// now re-insert it in the list at its new position
			//
			this.orderedGroups.InternalInsert( newPos, group.Header );

			// SSP 7/23/03 - Fixed headers
			// Ensure that the fixed headers are before non-fixed headers. Also update
			// the visiblePos member variables of all the headers to their new visible
			// positions. The reason for doing this is that with the fixed headers
			// functionality, we are re-sorting the ordered headers everytime a header
			// is fixed or unfixed. And if we don't update the visiblePos member variables,
			// then the order of the headers won't be maintained when we re-sort the
			// headers. So call SetVisiblePositionsToActual to update the visiblePos
			// member variables of the headers.
			//
			// ----------------------------------------------------------------------------------
			this.EnsureFixedHeadersBeforeNonFixed( );
			this.SetVisiblePositionsToActual( );
			// ----------------------------------------------------------------------------------

			changed = true;
		}


		internal bool CanInitializeFrom( UltraGridBand Source )
		{
			// if the keys match return true
			//
			CaseInsensitiveComparer	compareNoCase = new CaseInsensitiveComparer();
			if( compareNoCase.Compare(this.Key, Source.Key) == 0 )
				return true;

			// If both this band and the passed in source are band zeros
			// in their respective collections and if either one has
			// a null key then return true.
			//
			// This is to cover the situation where they set the
			// DataMember property to NULL string (which has the same
			// logical effect as setting it to the root command). 
			//

			// Instead of checking that the index is zero (which during the
			// load at runtime can be -1 since the band hasn't been added 
			// to the bands collection yet) check if the parent band is NULL
			// which should only occur for root bands
			if ( Source.ParentBand == null && this.parentBand == null )
			{
				// SSP 8/9/02 UWG1217
				// Always match the top-most band even if the keys are not
				// empty strings.
				// Commented out the if line so we would always return true
				//
				//if ( this.Key.Length < 1 || Source.Key.Length < 1 )
				return true;
			}

			//this.ParentColumn
			// FUTURE: we might want to add some more intelligence here
			//       to compare the columns collections
			//
			return false;
		}

		internal int GetVisiblePosition( UltraGridGroup group )
		{
			if ( group.Band != this )
			{
				Debug.Assert ( false, "Group from wrong band passed into CBand::GetVisiblePosition" );
				return -1;
			}

			for ( int index = 0; index < this.OrderedGroupHeaders.Count; index++ )
			{
				if ( this.orderedGroups[index] == group.Header )
				{
					return index;
				}
			}

			return -1;
		}

		private class HeaderComparer : IComparer
		{
			/// <summary>
			/// Compares headers in visible position order
			/// </summary>
			int IComparer.Compare( object x, object y )
			{
				Infragistics.Win.UltraWinGrid.HeaderBase header1 = x as HeaderBase;
				Infragistics.Win.UltraWinGrid.HeaderBase header2 = y as HeaderBase;

				if ( header1 == null )
					throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_18"));

				if ( header2 == null )
					throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_19"));

				if ( header1 == header2 )
					return 0;

				// SSP 7/18/03 - Fixed headers
				// All the fixed headers should appear before all the non-fixed headers.
				//
				// ----------------------------------------------------------------------------------
				//if ( header1.SerializedVisiblePosition < header2.SerializedVisiblePosition )
				//	return -1;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//int header1VisiblePos = header1.SerializedVisiblePosition;
				//int header2VisiblePos = header2.SerializedVisiblePosition;

				// MD 12/8/08 - 9.1 - Column Pinning Right
				// This logic has to be changed to allow for columns fixed on the right, which must go at the end of the 
				// ordered columns collection.
				#region Old Code

				//if ( header1.FixedResolved )
				//{
				//    if ( ! header2.FixedResolved )
				//        return -1;
				//}
				//else
				//{
				//    if ( header2.FixedResolved )
				//        return 1;
				//} 

				#endregion Old Code
				// Determine the fixed status of each header.
				//  0 = Fixed on left
				//  1 = Not fixed
				//  2 = Fixed on right
				//  3 = Chaptered columns
				int header1OrderPrecedence = HeaderComparer.GetHeaderOrderPrecedence( header1 );
				int header2OrderPrecedence = HeaderComparer.GetHeaderOrderPrecedence( header2 );

				if ( header1OrderPrecedence != header2OrderPrecedence )
					return header1OrderPrecedence - header2OrderPrecedence;

				// MD 2/19/09 - TFS14180
				// The ordered headers collections should never be using the OriginX and OriginY values.
				// These are not considered in this ordering and it was causing various issues because we
				// were re-initializing the ordered columns and groups while verifying the row layout 
				// caches or the band and groups, whichis also when the OriginX and OriginY values were
				// not stable.
				#region Removed

				//// MD 1/19/09 - Groups in RowLayout
				//// When using GroupLayout style, we need to check the flattened layout to know the visible position of the headers.
				//if ( header1.Band != null && header1.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout )
				//{
				//    GridBagLayoutManager manager = header1.Band.gridBagHeaderLayoutManager;
				//
				//    if ( manager != null )
				//    {
				//        if ( manager.LayoutItems.IndexOf( header1 ) != -1 && 
				//            manager.LayoutItems.IndexOf( header2 ) != -1 )
				//        {
				//            IGridBagConstraint constraint1 = manager.LayoutItems.GetConstraint( header1 ) as IGridBagConstraint;
				//            IGridBagConstraint constraint2 = manager.LayoutItems.GetConstraint( header2 ) as IGridBagConstraint;
				//
				//            if ( constraint1 != null && constraint2 != null )
				//            {
				//                int result = constraint1.OriginY.CompareTo( constraint2.OriginY );
				//
				//                if ( result != 0 )
				//                    return result;
				//
				//                result = constraint1.OriginX.CompareTo(constraint2.OriginX);
				//
				//                if (result != 0)
				//                    return result;
				//            }
				//        }
				//    }
				//} 

				#endregion Removed

				// SSP 10/27/03 UWG2724
				// Commented out the original code and added the new one below.
				// There can be times where the serialized visible pos on
				// two different headers can be the same in which case we should
				// sort according to their indexes.
				//
				// --------------------------------------------------------------
				//if ( header1VisiblePos < header2VisiblePos )
				//	return -1;
				int r = header1.SerializedVisiblePosition.CompareTo( header2.SerializedVisiblePosition );
				if ( 0 == r && null != header1.Band && null != header2.Band )
				{
					// MD 8/2/07 - 7.3 Performance
					// Refactored - Prevent calling expensive getters multiple times
					//int col1Index = null != header1.Column 
					//    ? header1.Column.Index : ( null != header1.Group ? header1.Group.Index : -1 );
					//int col2Index = null != header2.Column 
					//    ? header2.Column.Index : ( null != header2.Group ? header2.Group.Index : -1 );
					UltraGridColumn header1Column = header1.Column;

					int col1Index;
					if ( null != header1Column )
					{
						col1Index = header1Column.Index;
					}
					else
					{
						UltraGridGroup header1Group = header1.Group;
						col1Index = null != header1Group ? header1Group.Index : -1;
					}

					UltraGridColumn header2Column = header2.Column;

					int col2Index;
					if ( null != header2Column )
					{
						col2Index = header2Column.Index;
					}
					else
					{
						UltraGridGroup header2Group = header2.Group;
						col2Index = null != header2Group ? header2Group.Index : -1;
					}

					r = col1Index.CompareTo( col2Index );
				}

				return r;
				// --------------------------------------------------------------
				// ----------------------------------------------------------------------------------

				// SSP 10/27/03 UWG2724
				// Above change rendered this statement unreachable.
				//
				//return 1;
			}

			// MD 12/8/08 - 9.1 - Column Pinning Right
			#region GetHeaderOrderPrecedence

			private static int GetHeaderOrderPrecedence( HeaderBase header )
			{
				if ( header.Column != null && header.Column.IsChaptered )
					return 3;

				if ( header.FixedResolved == false )
					return 1;

				if ( header.FixOnRightResolved )
					return 2;

				return 0;
			} 

			#endregion GetHeaderOrderPrecedence
		}

		#region UltraGridBandTypeConverter

		/// <summary>
		/// UltraGridBand type converter.
		/// </summary>
		public sealed class UltraGridBandTypeConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if (destinationType == typeof(InstanceDescriptor)) 
				{
					return true;
				}
				
				return base.CanConvertTo(context, destinationType);
			}

            /// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) 
			{
				if (destinationType == null) 
				{
					throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_20"));
				}

				if ( destinationType == typeof(InstanceDescriptor) && 
					value is UltraGridBand ) 
				{
					UltraGridBand item = (UltraGridBand)value;
					
					ConstructorInfo ctor;
					
					ctor = typeof(UltraGridBand).GetConstructor(new Type[] { typeof( string ), typeof ( int ) } );

					if (ctor != null) 
					{
						//false as the last parameter here causes generation of a local variable for the type 
						return new InstanceDescriptor(ctor, new object[] { item.Key, item.ParentIndex }, false );
					}
				}

				return base.ConvertTo(context, culture, value, destinationType);
			}

            /// <summary>
            /// Returns a collection of properties for the type of array specified by the
            /// value parameter, using the specified context and attributes.
            /// </summary>
            /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
            /// <param name="value">An <see cref="System.Object"/> that specifies the type of array for which to get properties.</param>
            /// <param name="attributes">An array of type <see cref="System.Attribute"/> that is used as a filter.</param>
            /// <returns>A <see cref="System.ComponentModel.PropertyDescriptorCollection"/> with the properties that are exposed for this data type, or null if there are no properties.</returns>
			public override PropertyDescriptorCollection GetProperties(
				ITypeDescriptorContext context,
				object value,
				Attribute[] attributes )
			{
				PropertyDescriptorCollection props = base.GetProperties( context, value, attributes );

				UltraGridBand band = value as UltraGridBand;

				// JJD 2/4/02 - UWG578
				// If this is a dropdown or combo filter out properties
				// that don't make sense
				//
				if ( props != null	&&
					band != null	&&
					band.Layout != null &&
					band.Layout.Grid is UltraDropDownBase )
				{
					int count = 0;

					// count up all the properties that won't be filtered out
					//
					for ( int i = 0; i < props.Count; i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.FilterOutProperty( props[i] ) )
						if ( !UltraGridBandTypeConverter.FilterOutProperty( props[ i ] ) )
							count++;
					}

					// allocate an array of the proper size
					//
					PropertyDescriptor [] propArray = new PropertyDescriptor[count];

					int current = 0;

					// copy the unfiltered properties into the array
					//
					for ( int i = 0; i < props.Count; i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.FilterOutProperty( props[i] ) )
						if ( !UltraGridBandTypeConverter.FilterOutProperty( props[ i ] ) )
						{
							propArray[current] = props[i];
							current++;
						}
					}

					// return the filtered collection
					//
					return new PropertyDescriptorCollection( propArray );
				}

				return props;
			}

			// JJD 2/4/02 - UWG578
			// Filter out properties not meaningful to combos and dropdowns
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//private bool FilterOutProperty( PropertyDescriptor pd )
			private static bool FilterOutProperty( PropertyDescriptor pd )
			{
				switch ( pd.DisplayName )
				{
					case "AddButtonCaption":
					case "AddButtonToolTipText":
					case "CardSettings":
					case "CardView":
					case "Expandable":
					{
						return true;
					}
				}

				return false;
			}
		}
		
		#endregion UltraGridBandTypeConverter
		
		// SSP 7/16/02
		// Added  GetSerializedColumnId and GetMatchingColumn methods
		// for serializing and deserializing column filters as objects used in both
		// feature have references to columns so that the code for serializing columns
		// can be shared to some extent at least.
		//

		#region GetSerializedColumnId

		internal SerializedColumnID GetSerializedColumnId( UltraGridColumn column )
		{			
			Debug.Assert( null != column, "Null column passed in." );
			if ( null == column )
				return null;

			if ( this != column.Band )
			{
				Debug.Assert( false, "Column not from this band !" );
				return column.Band.GetSerializedColumnId( column );
			}

			int columnRelativeIndex = column.Index;

			Debug.Assert( columnRelativeIndex >= 0, "Invalid column." );

			if ( !column.IsBound )
				columnRelativeIndex -= this.Columns.BoundColumnsCount;

			SerializedColumnID colid = new SerializedColumnID( columnRelativeIndex, 
				column.IsBound, column.SortIndicator, column.IsGroupByColumn, column.Key );

			return colid;
		}

		#endregion // GetSerializedColumnId

		#region GetMatchingColumn
	
		internal UltraGridColumn GetMatchingColumn( SerializedColumnID serializedColumnId )
		{
			UltraGridColumn column = null;

			// first try matching via the column's key
			//
			try
			{
				if ( serializedColumnId.Key.Length > 0 &&
					this.Columns.Exists( serializedColumnId.Key ) )
					column = this.Columns[ serializedColumnId.Key ];
			}
			catch(Exception)
			{
			}

			// if key did't work then try the relative index
			//
			if ( column == null )
			{
				int index = serializedColumnId.RelativeIndex;

				// if the column is unbound add the bound columns count
				// to get the correct index into the columns collection
				//
				if ( !serializedColumnId.Bound )
					index += this.Columns.BoundColumnsCount;

				try
				{
					column = this.Columns[ index ];
				}
				catch(Exception)
				{
				}

			}

			return column;
		}

		#endregion // GetMatchingColumn

		#region ColumnAutoSizeModeResolved

		// SSP 4/14/03
		// Added ColumnAutoSizeMode off the override.
		//
		internal ColumnAutoSizeMode ColumnAutoSizeModeResolved
		{
			get
			{
				if ( this.HasOverride && this.Override.ShouldSerializeColumnAutoSizeMode( ) )
					return this.Override.ColumnAutoSizeMode;

				return this.Layout.ColumnAutoSizeModeDefault;
			}
		}

		#endregion // ColumnAutoSizeModeResolved

		// SSP 5/19/03 - Fixed headers
		// 
		#region Fixed Headers Code

		internal static readonly object SWAP_DROPDOWN_ITEM_FIX_HEADER = new object( );
		internal static readonly object SWAP_DROPDOWN_ITEM_UNFIX_HEADER = new object( );
		internal static readonly object SWAP_DROPDOWN_ITEM_UNFIX_ALL_HEADERS = new object( );

		#region UnfixAllHeaders

		/// <summary>
		/// Unfixes all the headers in this band.
		/// </summary>
		public void UnfixAllHeaders( )
		{
			int i;

			GroupsCollection groups = this.groups;
			if ( null != groups )
			{
				for ( i = 0; i < groups.Count; i++ )
					groups[i].Header.Fixed = false;
			}

			ColumnsCollection columns = this.columns;
			if ( null != columns )
			{
				for ( i = 0; i < columns.Count; i++ )
					columns[i].Header.Fixed = false;
			}
		}

		#endregion // UnfixAllHeaders

		#region BumpFixedHeadersVerifyVersion

		internal void BumpFixedHeadersVerifyVersion( )
		{
			this.BumpFixedHeadersVerifyVersion( false );
		}

		internal void BumpFixedHeadersVerifyVersion( bool dirtyOrderedHeaders )
		{
			this.fixedHeadersVerifyVersion++;

			if ( dirtyOrderedHeaders && this.UseFixedHeaders )
				this.orderedColumnsDirty = this.orderedGroupsDirty = true;

			if ( null != this.Layout )
			{
				// SSP 8/12/03 - Optimizations - Grand Verify Version Number
				//
				this.Layout.BumpGrandVerifyVersion( );

				this.Layout.DirtyGridElement( true );
				if ( null != this.Layout.ColScrollRegions )
					this.Layout.ColScrollRegions.DirtyMetrics( );
				this.Layout.SynchronizedColListsDirty = true;

				// SSP 7/12/05 BR04913
				//
				this.Layout.BumpCellChildElementsCacheVersion( );
			}

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			this.DirtyTabOrderedColumns( );
		}

		#endregion // BumpFixedHeadersVerifyVersion

		#region VerifyFixedHeadersVersion
		
		internal void VerifyFixedHeadersVersion( )
		{
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// The fixed header cache has be split into two collections now.
			//if ( this.verifiedFixedHeadersVersion == this.FixedHeadersVerifyVersion && null != this.fixedHeadersCached )
			if ( this.verifiedFixedHeadersVersion == this.FixedHeadersVerifyVersion && 
				this.leftFixedHeadersCached != null &&
				this.rightFixedHeadersCached != null )
				return;

			Debug.WriteLine( "Recreating fixed headers array." );

			this.verifiedFixedHeadersVersion = this.FixedHeadersVerifyVersion;

			HeadersCollection headers = this.OrderedHeaders;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( headers.Count );
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Renamed for clarity
			//List<HeaderBase> list = new List<HeaderBase>( headers.Count );
			List<HeaderBase> leftFixedHeadersList = new List<HeaderBase>( headers.Count );

			int i;
			for ( i = 0; i < headers.Count; i++ )
			{
				HeaderBase header = headers[i];

				// Skip hidden headers.
				//
				if ( header.Hidden )
					continue;

				// MD 12/8/08 - 9.1 - Column Pinning Right
				// Only add left fixed headers to this collection. Added another check to see which side it is fixed on.
				//if ( header.FixedResolved )
				//	list.Add( header );
				if ( header.FixedResolved && header.FixOnRightResolved == false )	
					leftFixedHeadersList.Add( header );
				else
					break;
			}

// MD 12/8/08 - 9.1 - Column Pinning Right
// We need to always do this now to skip passed the non-fixed headers
//#if DEBUG
			for ( ; i < headers.Count; i++ )
			{
				// MD 12/8/08 - 9.1 - Column Pinning Right
				// Once we hit the first fixed right column, break so we can go to the next loop
				if ( headers[ i ].FixedResolved && headers[ i ].FixOnRightResolved )
					break;

				// MD 12/8/08 - 9.1 - Column Pinning Right
				// Make sure left-fixed columns do not exist after the non-fixed headers.
				//Debug.Assert( ! headers[i].FixedResolved, "A fixed header encountered after a non-fixed header in the ordered headers collection !" );
				Debug.Assert( !headers[ i ].FixedResolved, "A left fixed header encountered after a non-fixed header in the ordered headers collection !" );
			}

// MD 12/8/08 - 9.1 - Column Pinning Right
// The if was commented out above
//#endif

			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Get the right fixed headers also
			List<HeaderBase> rightFixedHeadersList = new List<HeaderBase>( headers.Count );
			for ( ; i < headers.Count; i++ )
			{
				HeaderBase header = headers[ i ];

				// Skip hidden headers.
				if ( header.Hidden )
					continue;

				Debug.Assert( header.FixedResolved && header.FixOnRightResolved,
					"A left fixed header or non-fixed header encountered after right fixed header in the ordered headers collection!" );

				rightFixedHeadersList.Add( header );
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//this.fixedHeadersCached = (HeaderBase[])list.ToArray( typeof( HeaderBase ) );
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// The fixed header cache has be split into two collections now.
			//this.fixedHeadersCached = list.ToArray();
			this.leftFixedHeadersCached = leftFixedHeadersList.ToArray();
			this.rightFixedHeadersCached = rightFixedHeadersList.ToArray();
		}

		#endregion // VerifyFixedHeadersVersion

		#region OnHeadersDropped
		
		internal void OnHeadersDropped( HeaderBase[] cloneHeaders )
		{
			if ( ! this.UseFixedHeaders )
				return;

			int i;
			HeadersCollection headers = this.OrderedHeaders;
			
			// When the column headers or group headers are dragged and dropped, their Fixed
			// state may need to be updated. For example, if a non-fixed header was dragged
			// and dropped over a fixed header, then that non-fixed header needs to be made
			// fixed. The same applies when dragging a fixed header and dropping it over a
			// non-fixed header.
			//
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList headersToFix = new ArrayList( );
			//ArrayList headersToUnfix = new ArrayList( );			
			// MD 12/9/08 - 9.1 - Column Pinning Right
			// Split this into two lists
			//List<HeaderBase> headersToFix = new List<HeaderBase>();
			List<HeaderBase> headersToFixOnRight = new List<HeaderBase>();
			List<HeaderBase> headersToFixOnLeft = new List<HeaderBase>();

			List<HeaderBase> headersToUnfix = new List<HeaderBase>();	

			// Figure out which headers we need to fix and which headers we need to unfix.
			// CloneHeaders parameter is passed in by the drag strategy and contains the headers
			// that are dragged and dropped. Also their Fixed properties contain the appropriate
			// new value for the Fixed property.
			//
			for ( i = 0; i < headers.Count; i++ )
			{
				bool isFixed = false;
				bool found = false;

				// MD 12/9/08 - 9.1 - Column Pinning Right
				bool fixOnRight = false;

				// MD 8/2/07 - 7.3 Performance
				// Cached column - Prevent calling expensive getters multiple times
				UltraGridColumn headerColumn = headers[ i ].Column;
				UltraGridGroup headerGroup = headers[ i ].Group;

				// Find out if the current header (headers[i]) is one of the headers that was
				// dragged and dropped by looping through the cloneHeaders array.
				//
				for ( int j = 0; j < cloneHeaders.Length; j++ )
				{
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( null != cloneHeaders[j].Column )
					//{
					//    if ( cloneHeaders[j].Column == headers[i].Column 
					//        || cloneHeaders[j].Column.Header == headers[i] 
					//        || cloneHeaders[j].Column.ClonedFromColumn == headers[i].Column )
					UltraGridColumn cloneHeadersColumn = cloneHeaders[ j ].Column;

					if ( null != cloneHeadersColumn )
					{
						if ( cloneHeadersColumn == headerColumn
							|| cloneHeadersColumn.Header == headers[ i ]
							|| cloneHeadersColumn.ClonedFromColumn == headerColumn )
						{
							found = true;
							isFixed = cloneHeaders[j].Fixed;

							// MD 12/9/08 - 9.1 - Column Pinning Right
							fixOnRight = cloneHeaders[ j ].FixOnRightResolved;

							break;
						}
					}

					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( null != cloneHeaders[j].Group )
					//{
					//    if ( cloneHeaders[j].Group == headers[i].Group 
					//        || cloneHeaders[j].Group.Header == headers[i] 
					//        || cloneHeaders[j].Group.GetClonedFromGroup( ) == headers[i].Group )
					UltraGridGroup cloneHeadersGroup = cloneHeaders[ j ].Group;

					if ( null != cloneHeadersGroup )
					{
						if ( cloneHeadersGroup == headerGroup
							|| cloneHeadersGroup.Header == headers[ i ]
							|| cloneHeadersGroup.GetClonedFromGroup() == headerGroup )
						{
							found = true;
							isFixed = cloneHeaders[j].Fixed;

							// MD 12/9/08 - 9.1 - Column Pinning Right
							fixOnRight = cloneHeaders[ j ].FixOnRightResolved;

							break;
						}
					}
				}

				// If the current header (heades[i]) was dragged and dropped, figure out if it
				// needs to be fixed or unfixed and add it to appropriate headersToFix or
				// headersToUnfix list.
				//
				if ( found )
				{
					if ( isFixed )
					{
						// MD 12/9/08 - 9.1 - Column Pinning Right
						// Changed this logic to account for right-fixed columns as well as being able to move fixed columns from the 
						// right to the left.
						//if ( ! headers[i].Fixed )
						//	headersToFix.Add( headers[ i ] );
						bool isCurrentlyFixed = headers[ i ].Fixed;

						if ( fixOnRight )
						{
							if ( isCurrentlyFixed == false || headers[ i ].FixOnRightResolved == false )
								headersToFixOnRight.Add( headers[ i ] );
						}
						else
						{
							if ( isCurrentlyFixed == false || headers[ i ].FixOnRightResolved )
								headersToFixOnLeft.Add( headers[ i ] );
						}
					}
					else
					{
						if ( headers[i].Fixed )
							headersToUnfix.Add( headers[i] );
					}
				}
			}

			// Now set Fixed properties of headers to be fixed to true.
			//
			for ( i = 0; i < headersToFixOnLeft.Count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//((HeaderBase)headersToFix[i]).Fixed = true;
				headersToFixOnLeft[ i ].Fixed = true;

				// MD 12/9/08 - 9.1 - Column Pinning Right
				headersToFixOnLeft[ i ].FixOnRight = DefaultableBoolean.False;
			}

			// Likewise set the Fixed properties of headers to be unfixed to false.
			//
			for ( i = 0; i < headersToUnfix.Count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//((HeaderBase)headersToUnfix[i]).Fixed = false;
				headersToUnfix[ i ].Fixed = false;
			}

			// MD 12/9/08 - 9.1 - Column Pinning Right
			for ( i = 0; i < headersToFixOnRight.Count; i++ )
			{
				headersToFixOnRight[ i ].Fixed = true;
				headersToFixOnRight[ i ].FixOnRight = DefaultableBoolean.True;
			}

			// Make sure that all the fixed headers appear before non-fixed headers 
			// by calling EnsureFixedHeadersBeforeNonFixed method.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.EnsureFixedHeadersBeforeNonFixed( headers );
			UltraGridBand.EnsureFixedHeadersBeforeNonFixed( headers );

			// Now update the visiblePos variables of all the headers to their new 
			// positions.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.SetVisiblePositionsToActual( headers );
			UltraGridBand.SetVisiblePositionsToActual( headers );
		}

		#endregion // OnHeadersDropped

		#region EnsureFixedHeadersBeforeNonFixed
		
		internal void EnsureFixedHeadersBeforeNonFixed( )
		{
			// If fixed headers aren't enabled, return false.
			//
			if ( ! this.UseFixedHeaders )
				return;

			HeadersCollection headers = this.OrderedHeaders;

			Debug.Assert( null != headers, "Null headers." );
			if ( null != headers )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.EnsureFixedHeadersBeforeNonFixed( headers );
				UltraGridBand.EnsureFixedHeadersBeforeNonFixed( headers );
			}
		}
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void EnsureFixedHeadersBeforeNonFixed( HeadersCollection headers )
		internal static void EnsureFixedHeadersBeforeNonFixed( HeadersCollection headers )
		{
			Debug.Assert( null != headers, "Null headers passed in." );

			if ( null == headers )
				return;

			// MD 12/8/08 - 9.1 - Column Pinning Right
			// This loop has been changed to allow for right-fixed headers as well as left-fixed headers
			#region Old Code

			//int i;
			//bool needToReorder = false;
			//bool nonFixedHeaderEncountered = false;
			//for ( i = 0; i < headers.Count; i++ )
			//{
			//    HeaderBase header = headers[i];
			//
			//    if ( ! header.FixedResolved )
			//    {
			//        nonFixedHeaderEncountered = true;
			//    }
			//    else if ( nonFixedHeaderEncountered )
			//    {
			//        needToReorder = true;
			//        break;
			//    }
			//} 

			#endregion Old Code
			int i;
			bool needToReorder = false;

			bool nonFixedHeaderEncountered = false;
			bool rightFixedHeaderEncountered = false;

			for ( i = 0; i < headers.Count; i++ )
			{
				HeaderBase header = headers[ i ];

				// Fixed header
				if ( header.FixedResolved )
				{
					// Right fixed header
					if ( header.FixOnRightResolved )
					{
						rightFixedHeaderEncountered = true;
					}
					// Left fixed header
					else
					{
						// If a  left fixed header is after a non-fixed or right-fixed header, reorder
						if ( nonFixedHeaderEncountered || rightFixedHeaderEncountered )
						{
							needToReorder = true;
							break;
						}
					}
				}
				// Non-fixed header
				else
				{
					// If a non-fixed header is after a right-fixed header, reorder
					if ( rightFixedHeaderEncountered )
					{
						// MD 1/30/09 - TFS12271
						// The chaptered columns must go after the right-pinned columns
						if ( header.Column != null && header.Column.IsChaptered )
							continue;

						needToReorder = true;
						break;
					}

					nonFixedHeaderEncountered = true;
				}
			}

			if ( needToReorder )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList fixedHeaders = new ArrayList( headers.Count );
				//ArrayList nonFixedHeaders = new ArrayList( headers.Count );
				// MD 12/8/08 - 9.1 - Column Pinning Right
				// Renamed this collection for clairty and added a new collection for right-fixed headers.
				//List<HeaderBase> fixedHeaders = new List<HeaderBase>( headers.Count );
				List<HeaderBase> leftFixedHeaders = new List<HeaderBase>( headers.Count );
				List<HeaderBase> rightFixedHeaders = new List<HeaderBase>( headers.Count );

				List<HeaderBase> nonFixedHeaders = new List<HeaderBase>( headers.Count );

				// MD 1/30/09 - TFS12271
				// The chaptered columns must go after the right-pinned columns
				List<HeaderBase> chapteredHeaders = new List<HeaderBase>( headers.Count );
					
				for ( i = 0; i < headers.Count; i++ )
				{
					HeaderBase header = headers[i];

					// MD 1/30/09 - TFS12271
					// The chaptered columns must go after the right-pinned columns
					if ( header.Column != null && header.Column.IsChaptered )
					{
						chapteredHeaders.Add( header );
						continue;
					}

					if ( header.FixedResolved )
					{
						// MD 12/8/08 - 9.1 - Column Pinning Right
						// Add the header to the correct fixed header collection depending on which side it is fixed on.
						//fixedHeaders.Add( header );
						if ( header.FixOnRightResolved )
							rightFixedHeaders.Add( header );
						else
							leftFixedHeaders.Add( header );
					}
					else
						nonFixedHeaders.Add( header );
				}

				int c = 0;
				for ( i = 0; i < leftFixedHeaders.Count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//headers[c++] = (HeaderBase)fixedHeaders[i];
					headers[ c++ ] = leftFixedHeaders[ i ];
				}

				// SSP 4/15/04
				// This was a typo.
				//for ( i = 0; i < fixedHeaders.Count; i++ )
				for ( i = 0; i < nonFixedHeaders.Count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//headers[c++] = (HeaderBase)nonFixedHeaders[i];
					headers[ c++ ] = nonFixedHeaders[ i ];
				}

				// MD 12/8/08 - 9.1 - Column Pinning Right
				// Add the collection of right-fixed headers to the end.
				for ( i = 0; i < rightFixedHeaders.Count; i++ )
					headers[ c++ ] = rightFixedHeaders[ i ];

				// MD 1/30/09 - TFS12271
				// The chaptered columns must go after the right-pinned columns
				for ( i = 0; i < chapteredHeaders.Count; i++ )
					headers[ c++ ] = chapteredHeaders[ i ];
			}
		}

		#endregion // EnsureFixedHeadersBeforeNonFixed

		#region SetVisiblePositionsToActual
		
		internal void SetVisiblePositionsToActual( )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.SetVisiblePositionsToActual( this.OrderedHeaders );
			UltraGridBand.SetVisiblePositionsToActual( this.OrderedHeaders );
		}
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void SetVisiblePositionsToActual( HeadersCollection headers )
		internal static void SetVisiblePositionsToActual( HeadersCollection headers )
		{
			if ( null != headers )
			{
				for ( int i = 0; i < headers.Count; i++ )
					headers[i].InternalSetVisiblePosValue( i );
			}
		}

		#endregion // SetVisiblePositionsToActual

		#region FixedHeadersVerifyVersion

		internal int FixedHeadersVerifyVersion
		{
			get
			{
				return this.fixedHeadersVerifyVersion;
			}
		}

		#endregion // FixedHeadersVerifyVersion

		#region UseFixedHeaders

		internal bool UseFixedHeaders
		{
			get
			{
				return null != this.layout && this.layout.UseFixedHeaders 
					// SSP 8/14/03 UWG2587
					// Don't check for the layout being display layout. We want to apply the fixed
					// header and cell related appearances when printing and exporting (or at least
					// that's what the description in above bug says).
					//
					//&& this.layout.IsDisplayLayout 
					&& ! this.CardView;
			}
		}

		#endregion // UseFixedHeaders

		#region AreFixedHeadersAllowed
		
		internal bool AreFixedHeadersAllowed( ColScrollRegion csr )
		{
			return this.UseFixedHeadersResolved( csr ) && ! this.UseRowLayoutResolved
				// SSP 8/21/03 
				//
				&& this.layout.IsDisplayLayout;
		}

		#endregion // AreFixedHeadersAllowed

		#region AddSwapDropdownFixUnfixItem
		
		internal void AddSwapDropdownFixUnfixItem( ValueList swapDropDown, HeaderBase header )
		{
			ColScrollRegion activeCSR = null != this.Layout ? this.Layout.ActiveColScrollRegion : null;

			if ( null != swapDropDown && null != activeCSR 
				&& this.AreFixedHeadersAllowed( activeCSR ) 
				&& FixedHeaderIndicator.InSwapDropDown == header.FixedHeaderIndicatorResolved )
			{
				string text = null;

				if ( ! this.IsHeaderFixed( activeCSR, header ) )
				{
					text = SR.GetString( "FixedHeaders_FixHeaderSwapItem" );

					if ( null != text && text.Length > 0 )
						swapDropDown.ValueListItems.Add( UltraGridBand.SWAP_DROPDOWN_ITEM_FIX_HEADER, text );
				}
				else
				{
					text = SR.GetString( "FixedHeaders_UnfixHeaderSwapItem" );

					if ( null != text && text.Length > 0 )
						swapDropDown.ValueListItems.Add( UltraGridBand.SWAP_DROPDOWN_ITEM_UNFIX_HEADER, text );
				}

				// SSP 7/30/03 UWG2543
				// Added an Unfix All option to the swap drop down.
				//
				if ( this.HasFixedHeaders( activeCSR ) )
				{
					text = SR.GetString( "FixedHeaders_UnfixAllHeadersSwapItem" );

					if ( null != text && text.Length > 0 )
						swapDropDown.ValueListItems.Add( UltraGridBand.SWAP_DROPDOWN_ITEM_UNFIX_ALL_HEADERS, text );
				}
			}
		}

		#endregion // AddSwapDropdownFixUnfixItem

		#region HasFixedHeaders

		internal bool HasFixedHeaders( ColScrollRegion csr )
		{
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Moved all code to new overload
			return
				this.HasFixedHeaders( csr, false ) ||
				this.HasFixedHeaders( csr, true );
		}

		// MD 12/8/08 - 9.1 - Column Pinning Right
		private bool HasFixedHeaders( ColScrollRegion csr, bool checkRightFixedHeaders )
		{
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Pass the new parameter to the GetFixedHeaders method.
			//HeaderBase[] fixedHeaders = this.GetFixedHeaders( csr );
			HeaderBase[] fixedHeaders = this.GetFixedHeaders( csr, checkRightFixedHeaders );

			return null != fixedHeaders && fixedHeaders.Length > 0;
		}

		#endregion // HasFixedHeaders

		#region UseFixedHeadersResolved

		internal bool UseFixedHeadersResolved( ColScrollRegion csr )
		{
			Debug.Assert( null != csr, "Null csr passed in UseFixedHeadersResolved !" );

			return this.UseFixedHeaders && null != csr && ! csr.HasExclusiveItems;
		}

		#endregion // UseFixedHeadersResolved
	
		#region GetFixedHeaders_ExtentDelta
		
		internal int GetFixedHeaders_ExtentDelta( ColScrollRegion csr )
		{
			if ( ! this.UseFixedHeadersResolved( csr ) 
				// SSP 8/21/03
				//
				|| ! this.layout.IsDisplayLayout
				)
				return 0;

			if ( this.Layout.ViewStyleImpl.HasMultiRowTiers )
			{
				if ( null == this.ParentBand )
					return Math.Min( csr.Position, this.GetExtent( BandOrigin.RowCellArea ) - this.GetFixedHeadersExtent( csr ) );

				int scrollDelta = csr.Position;
				int parentBandOrigin, parentBandExtent;
				GetBandOriginExtent_HorizontalViewStyle( this.ParentBand, csr, 
					ref scrollDelta, out parentBandOrigin, out parentBandExtent );

				return Math.Min( scrollDelta, this.GetExtent( BandOrigin.RowCellArea ) - this.GetFixedHeadersExtent( csr ) );
			}
			else
			{
				// Make sure that the row element's width is at least as much so that it
				// can contain pre-row area and row selector elements and any fixed headers.
				//
				return Math.Min( csr.Position, this.GetExtent( BandOrigin.RowCellArea ) - this.GetFixedHeadersExtent( csr ) );
			}
		}

		#endregion // GetFixedHeaders_ExtentDelta

		#region GetFixedHeaders_BandHeader_SummaryFooter_RightDelta

		// SSP 8/8/03 UWG2551
		// Added GetBandHeader_BandHeader_SummaryFooter_RightDelta. Unlike the row ui element, the 
		// band headers and the summary footer should get scrolled all the way out.
		//
		internal int GetFixedHeaders_BandHeader_SummaryFooter_RightDelta( ColScrollRegion csr )
		{
			if ( null != csr && this.UseFixedHeadersResolved( csr ) && ! this.HasFixedHeaders( csr ) 
				// SSP 8/21/03
				//
				&& this.layout.IsDisplayLayout
				)
			{
				int csrPos = csr.Position;
				if ( null != this.ParentBand && this.Layout.ViewStyleImpl.HasMultiRowTiers )				
				{
					int scrollDelta = csr.Position;
					int parentBandOrigin, parentBandExtent;
					GetBandOriginExtent_HorizontalViewStyle( this.ParentBand, csr, 
						ref scrollDelta, out parentBandOrigin, out parentBandExtent );
					
					csrPos = scrollDelta;
				}

				int delta = this.GetFixedHeaders_ExtentDelta( csr ) - csrPos;
				if ( delta < 0 )
					return delta;
			}

			return 0;
		}

		#endregion // GetFixedHeaders_BandHeader_SummaryFooter_RightDelta

		#region GetFixedHeaders_OriginDelta
		
		internal int GetFixedHeaders_OriginDelta( ColScrollRegion csr )
		{
			if ( ! this.UseFixedHeadersResolved( csr )
				// SSP 8/21/03
				//
				|| ! this.layout.IsDisplayLayout
				)
				return 0;

			if ( this.Layout.ViewStyleImpl.HasMultiRowTiers )
			{
				if ( null == this.ParentBand )
					return csr.Position;

				int scrollDelta = csr.Position;
				int parentBandOrigin, parentBandExtent;
				GetBandOriginExtent_HorizontalViewStyle( this.ParentBand, csr, 
					ref scrollDelta, out parentBandOrigin, out parentBandExtent );

				return scrollDelta;
			}
			else
			{
				return csr.Position;
			}
		}

		#endregion // GetFixedHeaders_OriginDelta

		#region GetBandOriginExtent_HorizontalViewStyle

		// For horizontal view style, the scrolling is a lot different from the vertical view style.
		// Following helper method helps figure out the origin and extents of bands based on the
		// current position of the scroll bar.
		//
		internal static void GetBandOriginExtent_HorizontalViewStyle( UltraGridBand band, ColScrollRegion csr, 
			ref int scrollDelta, out int origin, out int extent )
		{
			origin = 0;
			extent = 0;

			int parentBandOrigin = 0, parentBandExtent = 0;

			if ( null != band.ParentBand )
				GetBandOriginExtent_HorizontalViewStyle( band.ParentBand, csr, ref scrollDelta, out parentBandOrigin, out parentBandExtent );

			int fixedHeadersAreaExtent = band.PreRowAreaExtent + band.RowSelectorExtent + band.GetFixedHeadersExtent( csr );
			int bandExtent = csr.GetBandExtent( band );
			origin = csr.GetBandOrigin( band );
			extent = Math.Max( fixedHeadersAreaExtent, bandExtent - scrollDelta );
			origin -= csr.Position - scrollDelta;
			scrollDelta -= bandExtent - extent;
		}

		#endregion // GetBandOriginExtent_HorizontalViewStyle

		#region GetFixedHeaders
		
		// MD 12/8/08 - 9.1 - Column Pinning Right
		// Added a parameter to determine which side of fixed headers to get
		//internal HeaderBase[] GetFixedHeaders( ColScrollRegion csr )
		internal HeaderBase[] GetFixedHeaders( ColScrollRegion csr, bool getRightFixedHeaders )
		{
			// In row layout mode there are no fixed headers.
			//
			//if ( ! this.UseFixedHeadersResolved( csr ) || this.UseRowLayoutResolved )
			if ( ! this.AreFixedHeadersAllowed( csr ) )
				return null;

			this.VerifyFixedHeadersVersion( );

			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Get the approproate fixed header cache, depending on the new parameter value.
			//return this.fixedHeadersCached;
			return getRightFixedHeaders
				? this.rightFixedHeadersCached
				: this.leftFixedHeadersCached;
		}

		#endregion // GetFixedHeaders

		#region GetFirstNonFixedHeader

		internal HeaderBase GetFirstNonFixedHeader( ColScrollRegion csr )
		{
			if ( ! this.UseFixedHeadersResolved( csr ) )
				return null;

			this.VerifyFixedHeadersVersion( );

			HeadersCollection headers = this.OrderedHeaders;

			for ( int i = 0; i < headers.Count; i++ )
			{
				HeaderBase tmpHeader = headers[i];

				// Skip hidden headers.
				//
				if ( tmpHeader.Hidden )
					continue;

				if ( ! tmpHeader.FixedResolved )
					return tmpHeader;
			}

			return null;
		}

		#endregion // GetFirstNonFixedHeader

		// MD 12/8/08 - 9.1 - Column Pinning Right
		#region GetFirstRightFixedHeader

		internal HeaderBase GetFirstRightFixedHeader( ColScrollRegion csr )
		{
			HeaderBase[] rightFixedHeaders = this.GetFixedHeaders( csr, true );

			return ( rightFixedHeaders != null && rightFixedHeaders.Length > 0 )
				? rightFixedHeaders[ 0 ] 
				: null;
		}

		#endregion GetFirstRightFixedHeader

		#region GetLastLeftFixedHeader

		// MD 12/8/08 - 9.1 - Column Pinning Right
		// Changed the method name for clarity
		//internal HeaderBase GetLastFixedHeader( ColScrollRegion csr )
		internal HeaderBase GetLastLeftFixedHeader( ColScrollRegion csr )
		{
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Changed the name of the local variable for clarity and called GetFixedHeaders with the new parameter.
			//HeaderBase[] fixedHeaders = this.GetFixedHeaders( csr );
			//return null != fixedHeaders && fixedHeaders.Length > 0 
			//    ? fixedHeaders[ fixedHeaders.Length - 1 ] : null;
			HeaderBase[] leftFixedHeaders = this.GetFixedHeaders( csr, false );

			return ( leftFixedHeaders != null && leftFixedHeaders.Length > 0 )
				? leftFixedHeaders[ leftFixedHeaders.Length - 1 ]
				: null;
		}

		#endregion // GetLastLeftFixedHeader

		#region GetFixedHeadersExtent

		internal int GetFixedHeadersExtent( ColScrollRegion csr )
		{
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Moved all code to the new overload
			return
				this.GetFixedHeadersExtent( csr, false ) +
				this.GetFixedHeadersExtent( csr, true );
		}

		// MD 12/8/08 - 9.1 - Column Pinning Right
		internal int GetFixedHeadersExtent( ColScrollRegion csr, bool getRightExtent )
		{
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Pass the new parameter to the GetFixedHeaders method.
			//HeaderBase[] fixedHeaders = this.GetFixedHeaders( csr );
			HeaderBase[] fixedHeaders = this.GetFixedHeaders( csr, getRightExtent );

			if ( null == fixedHeaders )
				return 0;

			int fixedHeadersExtent = 0;
			
			for ( int i = 0; i < fixedHeaders.Length; i++ )
			{
				HeaderBase header = fixedHeaders[i];
				Debug.Assert( ! header.Hidden, "All the headers in the fixedHeaders arr should be visible !" );
				
				fixedHeadersExtent += header.Extent - header.RowSelectorExtent;
			}

			return fixedHeadersExtent;
		}

		#endregion // GetFixedHeadersExtent

		#region IsHeaderFixed
		
		internal bool IsHeaderFixed( ColScrollRegion csr, HeaderBase testHeader )
		{
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Moved all code to the new overload
			return
				this.IsHeaderFixed( csr, testHeader, false ) ||
				this.IsHeaderFixed( csr, testHeader, true );
		}

		private bool IsHeaderFixed( ColScrollRegion csr, HeaderBase testHeader, bool checkRightColumns )
		{
			// MD 12/8/08 - 9.1 - Column Pinning Right
			// Pass the new parameter to the GetFixedHeaders method.
			//HeaderBase[] fixedHeaders = this.GetFixedHeaders( csr );
			HeaderBase[] fixedHeaders = this.GetFixedHeaders( csr, checkRightColumns );

			if ( null != fixedHeaders )
			{
				for ( int i = 0; i < fixedHeaders.Length; i++ )
				{
					HeaderBase header = fixedHeaders[i];

					if ( header == testHeader )
						return true;

					if ( header is GroupHeader && testHeader is ColumnHeader )
					{
						// MD 1/21/09 - Groups in RowLayouts
						// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
						//if ( header.Group == testHeader.Column.Group )
						if ( header.Group == testHeader.Column.GroupResolved )
							return true;
					}
				}
			}

			return false;
		}

		#endregion // IsHeaderFixed

		// MD 12/8/08 - 9.1 - Column Pinning Right
		// This doesn't seem to be used anymore
		#region Not Used

		

		#endregion Not Used

		#region GetLeftFixedHeadersAreaRight

		// MD 12/9/08 - 9.1 - Column Pinning Right
		// Renamed for clarity
		//internal int GetFixedHeadersAreaRight( ColScrollRegion csr )
		internal int GetLeftFixedHeadersAreaRight( ColScrollRegion csr )
		{
			int fixedHeadersAreaRight = 
				this.GetOrigin( BandOrigin.RowCellArea ) 

				// MD 12/9/08 - 9.1 - Column Pinning Right
				// This should only get the extent of the left fixed headers.
				//+ this.GetFixedHeadersExtent( csr ) 
				+ this.GetFixedHeadersExtent( csr, false ) 

				- csr.Position 
				+ this.GetFixedHeaders_OriginDelta( csr );

			return fixedHeadersAreaRight;
		}

		#endregion // GetLeftFixedHeadersAreaRight

		#region GetRightFixedHeadersAreaLeft

		internal int GetRightFixedHeadersAreaLeft( ColScrollRegion csr )
		{
			HeaderBase[] headers = this.GetFixedHeaders( csr, true );

			if ( headers == null || headers.Length == 0 )
				return Int32.MaxValue;

			return headers[ 0 ].GetOnScreenOrigin( csr, true );
		}

		#endregion // GetRightFixedHeadersAreaLeft

		#region IsHeaderScrolledUnderFixedHeaders

		internal bool IsHeaderScrolledUnderFixedHeaders( ColScrollRegion csr, HeaderBase header )
		{
			// If the header is fixed, then it can't be scrolled underneath fixed headers.
			//
			if ( this.IsHeaderFixed( csr, header ) )
				return false;

			int fixedAreaRight = this.GetLeftFixedHeadersAreaRight( csr );
			int origin = 0;
			int extent = 0;
			
			header.GetDimensions(
				// SSP 9/5/03 UWG2620
				// Use InsideRowSelectors instead of InsidePreRowSelectors because the 
				// GetFixedHeadersAreaRight call above will not include the row selectors.
				//
				//Infragistics.Win.UltraWinGrid.PositionDimensions.InsidePreRowSelectors,
				Infragistics.Win.UltraWinGrid.PositionDimensions.InsideRowSelectors,
				Infragistics.Win.UltraWinGrid.DimOriginBase.Relative,
				ref origin,
				ref extent,
				csr );

			if ( origin < fixedAreaRight )
				return true;

			return false;
		}

		#endregion // IsHeaderScrolledUnderFixedHeaders

		#region AreNonFixedHeadersScrolledUnderneathFixedHeaders

		internal bool AreNonFixedHeadersScrolledUnderneathFixedHeaders( ColScrollRegion csr )
		{
			Debug.Assert( this.UseFixedHeadersResolved( csr ), "This method should only get called when the fixed headers are turned on." );

			if ( this.UseFixedHeadersResolved( csr ) )
			{
				int extentDelta = this.GetFixedHeaders_ExtentDelta( csr );
				
				if ( 0 != extentDelta )
					return true;
			}

			return false;
		}

		#endregion // AreNonFixedHeadersScrolledUnderneathFixedHeaders

		#region FixedCellSeparatorColorResolved

		internal Color FixedCellSeparatorColorResolved
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeFixedCellSeparatorColor( ) )
					return this.overrideObj.FixedCellSeparatorColor;

				return this.Layout.FixedCellSeparatorColorDefault;
			}
		}

		#endregion // FixedCellSeparatorColorResolved

		#endregion // Fixed Headers Code

		// SSP 1/30/03 - Row Layout Functionality
		//
		#region Row Layout Code

		#region Public Properties

		#region TotalHeaderHeight

		/// <summary>
		/// Returns the total height of all the headers. (Column headers, group headers and band header). If the headers are not visible, then it returns 0.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Advanced ), Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int TotalHeaderHeight
		{
			get
			{
				return this.GetTotalHeaderHeight( );
			}
		}

		#endregion // TotalHeaderHeight

		#region UseRowLayout

		/// <summary>
		/// Enables the the row-layout functionality.
		/// </summary>
		/// <remarks>
		/// <p>Use <see cref="UltraGridColumn.RowLayoutColumnInfo"/> object to customize the cell positions. By default all the columns are visible. Set <see cref="UltraGridColumn.Hidden"/> to true on columns that you want to hide.</p>
		/// <p>Due to the nature of the row-layout functionality column swapping, and column moving are disabled.</p>
		/// <seealso cref="UltraGridColumn.RowLayoutColumnInfo"/> <seealso cref="UltraGridBand.RowLayouts"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridBand_P_UseRowLayout") ]
        // MRS - NAS 9.1 - Groups in RowLayout
        // Made this property obsolete.
        [ Obsolete("This property is obsolete. Use RowLayoutStyle, instead")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		public bool UseRowLayout
		{
			get
			{
				// MD 2/23/09 - TFS14254
				// This member variable is no longer used.
				//return this.useRowLayout;
				return this.RowLayoutStyle != RowLayoutStyle.None;
			}
			set
			{
				// MD 2/23/09 - TFS14254
				// This member variable is no longer used.
				//if ( this.useRowLayout != value )
				if ( this.UseRowLayout != value )
				{
					// MD 2/23/09 - TFS14254
					// This member variable is no longer used.
					//this.useRowLayout = value;

                    // MRS - NAS 9.1 - Groups in RowLayout
                    #region Old Code
                    //if ( this.useRowLayout )
                    //    this.DirtyRowLayoutCachedInfo( );

                    //// SSP 5/5/05
                    //// When row layout functionality is turned on or off the Hidden state of
                    //// a group-by column may change in which case we want to recreate the
                    //// ordered headers collection.
                    ////
                    //this.BumpFixedHeadersVerifyVersion( true );
                    
                    #endregion //Old Code
                    //
					// MD 2/23/09 - TFS14254
					// This member variable is no longer used.
                    //if (this.useRowLayout)
					if ( value )
                    {
                        if (this.RowLayoutStyle == RowLayoutStyle.None)
                            this.RowLayoutStyle = RowLayoutStyle.ColumnLayout;
                    }
                    else
                        this.RowLayoutStyle = RowLayoutStyle.None;

                    this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.UseRowLayout);
                }
			}
		}

		#endregion // UseRowLayout

		#region RowLayouts

		/// <summary>
		/// Returns an instance of RowLayoutsCollection to which you can add new RowLayout objects using <seealso cref="RowLayoutsCollection.Add()"/> method.
		/// </summary>
		/// <remarks>
		/// <p class="body">Set <see cref="UseRowLayout"/> property to true to enable row-layout functionality.</p>
		/// <p><seealso cref="UseRowLayout"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridBand_P_RowLayouts"), 
		DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		public RowLayoutsCollection RowLayouts
		{
			get
			{
                if (null == this.rowLayouts)
                {
                    this.rowLayouts = new RowLayoutsCollection(this);

                    // MRS 12/5/2008 - TFS11243
                    this.rowLayouts.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }

				return this.rowLayouts;
			}
		}

		#endregion // RowLayouts

		#region RowLayoutLabelStyle

		/// <summary>
		/// Gets or sets the row-layout label style. Row-layout label style specifies whether the column labels are shown in a separate area above the rows or with the cells in each row. Default is <b>Separate</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">When RowLayoutLabelStyle is set to <b>Separate</b>, the column labels are shown in a separate header area above the rows. This is the default.</p>
		/// <p class="body">When RowLayoutLabelStyle is set to <b>WithCellData</b>, the column labels are shown with cells and repeated in each row.</p>
		/// <p><seealso cref="UltraGridBand.RowLayoutLabelPosition"/></p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_RowLayoutLabelStyle")]
        public RowLayoutLabelStyle RowLayoutLabelStyle
		{
			get
			{
				return this.rowLayoutLabelStyle;
			}
			set
			{
				if ( this.rowLayoutLabelStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( RowLayoutLabelStyle ), value ) )
						throw new InvalidEnumArgumentException( "RowLayoutLabelStyle", (int)value, typeof( RowLayoutLabelStyle ) );

					this.rowLayoutLabelStyle = value;

					this.DirtyRowLayoutCachedInfo( );

                    // CDS 7/07/09 TFS17996
                    this.DirtyCachedContainsCheckboxOnTopOrBottom();

					this.NotifyPropChange( PropertyIds.RowLayoutLabelStyle );
				}
			}
		}

		#endregion // RowLayoutLabelStyle

		#region RowLayoutLabelPosition

		/// <summary>
		/// Gets or sets the label position. Label position indicates the position of the column labels in the row layout mode.
		/// </summary>
		/// <remarks>
		/// <p>Label position indicates where to position the column label in relation to the associated cell in row layout mode.</p>
		/// <p><see cref="UltraGridBand.RowLayoutLabelStyle"/> must be set to <b>WithCellData</b> for this to take effect in regular view (non-card view) mode. In card view, <see cref="UltraGridCardSettings.Style"/> must be set to a value other than <b>MergedLabels</b>.</p>
		/// <p><seealso cref="UltraGridBand.UseRowLayout"/> <seealso cref="UltraGridBand.RowLayoutLabelStyle"/> <seealso cref="RowLayoutColumnInfo.LabelPosition"/> <seealso cref="UltraGridColumn.RowLayoutColumnInfo"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridBand_P_RowLayoutLabelPosition") ]
		public LabelPosition RowLayoutLabelPosition
		{
			get
			{
				return this.rowLayoutLabelPosition;
			}
			set
			{
				if ( this.rowLayoutLabelPosition != value )
				{
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.LabelPosition ), value ) )
						throw new InvalidEnumArgumentException( "RowLayoutLabelPosition", (int)value, typeof( Infragistics.Win.UltraWinGrid.LabelPosition ) );

					this.rowLayoutLabelPosition = value;

					this.DirtyRowLayoutCachedInfo( );

					this.NotifyPropChange( PropertyIds.LabelPosition );
				}
			}
		}

		#endregion // RowLayoutLabelPosition

		#endregion // Public Properties

		#region Public Methods

		#region ResetUseRowLayout

		/// <summary>
		/// Resets UseRowLayout property to its default value.
		/// </summary>
		public void ResetUseRowLayout( )
        {
            this.SetUseRowLayoutNoWarning(false);
        }

		#endregion // ResetUseRowLayout

		#region ResetRowLayouts

		/// <summary>
		/// Resets RowLayouts property to its default value.
		/// </summary>
		public void ResetRowLayouts( )
		{
			if ( null != rowLayouts )
				this.rowLayouts.Clear( );
		}

		#endregion // ResetRowLayouts

		#region ResetRowLayoutLabelStyle

		/// <summary>
		/// Resets the property to its default value of <b>Separate</b>.
		/// </summary>
		public void ResetRowLayoutLabelStyle( )
		{
			this.RowLayoutLabelStyle = Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.Separate;
		}

		#endregion // ResetRowLayoutLabelStyle

		#region ResetRowLayoutLabelPosition

		/// <summary>
		/// Resets the RowLayoutLabelPosition property to its default value of <b>Default</b>.
		/// </summary>
		public void ResetRowLayoutLabelPosition( )
		{
			this.RowLayoutLabelPosition = LabelPosition.Default;
		}

		#endregion // ResetRowLayoutLabelPosition

		#endregion // Public Methods

		#region Protected Methods

		#region ShouldSerializeUseRowLayout

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeUseRowLayout()
		{
			// MD 2/23/09 - TFS14254
			// This member variable is no longer used.
			// Plus, this property should never be serialized anymore.
			//return this.useRowLayout;
			return false;
		}

		#endregion // ShouldSerializeUseRowLayout

		#region ShouldSerializeRowLayouts

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeRowLayouts()
		{
			return null != this.rowLayouts && this.rowLayouts.Count > 0;
		}

		#endregion // ShouldSerializeRowLayouts

		#region ShouldSerializeRowLayoutLabelPosition

		/// <summary>
		/// Returns true if the RowLayoutLabelPosition property is set to a value other than the default value of <b>Default</b>.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowLayoutLabelPosition( )
		{
			return LabelPosition.Default != this.rowLayoutLabelPosition;
		}

		#endregion // ShouldSerializeRowLayoutLabelPosition

		#region ShouldSerializeRowLayoutLabelStyle

		/// <summary>
		/// Retruns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowLayoutLabelStyle( )
		{
			return Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.Separate != this.rowLayoutLabelStyle;
		}

		#endregion // ShouldSerializeRowLayoutLabelStyle

		#endregion // Protected Methods

		#region Private/Internal Row Layout Properties/Methods

		#region UseRowLayoutResolved

		/// <summary>
		/// Whether row-layout functionality is in effect.
		/// </summary>
		internal bool UseRowLayoutResolved
		{
			get
			{
				// SSP 3/4/04
				// Changed the behavior to give row-layout higher priority than the groups.
				// Meaning if there are groups and they turn set UseRowLayout to true, we
				// will ignore groups and use the row-layout functionality.
				//
				//return this.useRowLayout && ! this.HasGroups;
				// MD 2/23/09 - TFS14254
				// This member variable is no longer used.
				//return this.useRowLayout;
#pragma warning disable 0618
				return this.UseRowLayout;
#pragma warning restore 0618
			}
		}

		#endregion // UseRowLayoutResolved

		#region RowLayoutLabelPositionResolved
		
		// MD 2/25/09 - TFS14602
		// Changed this to a method because it needs more information to get the resolved value.
		//internal LabelPosition RowLayoutLabelPositionResolved
		internal LabelPosition GetRowLayoutLabelPositionResolved( RowLayoutColumnInfoContext contextType )
		{
			// MD 2/25/09 - TFS14602
			//get
			//{
				if ( LabelPosition.Default != this.RowLayoutLabelPosition )
					return this.RowLayoutLabelPosition;

				// SSP 12/16/04 BR00242
				// If the ColHeadersVisible property is set to false then don't display 
				// the card labels. The same applies to non-card row-layout mode where 
				// the headers are with cells and ColHeadersVisible is set to false.
				//
				// MD 2/25/09 - TFS14602
				// We can't assume this is used for columns all the time anymore. It could also be called for groups.
				//if ( ! this.ColHeadersVisible )
				//    return LabelPosition.None;
				switch ( contextType )
				{
					case RowLayoutColumnInfoContext.Column:
						{
							if ( this.ColHeadersVisible == false )
								return LabelPosition.None;
						}
						break;

					case RowLayoutColumnInfoContext.Group:
						{
                            // MRS 2/27/2009 - TFS14725
                            // Put this back the way it was. The Resolved here is only concerned with
                            // whether the headers are visible on top. 
                            //
                            //// MD 2/25/09 - TFS14599
                            //// Check the resolved value.
                            ////if ( this.GroupHeadersVisible == false )
                            //if ( this.GroupHeadersVisibleResolved == false )
                            if (this.GroupHeadersVisible == false)
								return LabelPosition.None;
						}
						break;

					default:
						Debug.Fail( "Unknown RowLayoutColumnInfoContext member: " + contextType );
						break;
				}

				return this.CardView ? LabelPosition.Left : LabelPosition.Top;
			//}
		}

		#endregion // RowLayoutLabelPositionResolved

		#region AllowRowLayoutCellSizingResolved
		
		internal RowLayoutSizing AllowRowLayoutCellSizingResolved
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeAllowRowLayoutCellSizing( ) )
					return this.overrideObj.AllowRowLayoutCellSizing;

				if ( this.Layout.HasOverride && this.Layout.Override.ShouldSerializeAllowRowLayoutCellSizing( ) )
					return this.Layout.Override.AllowRowLayoutCellSizing;

				if ( AllowColSizing.None == this.AllowColSizingResolved )
					return RowLayoutSizing.None;

				// SSP 9/13/07 BR25959
				// 
				RowSizing rowSizing = this.RowSizingResolved;

				// SSP 11/16/04
				// Implemented column sizing using cells in non-rowlayout mode. Added 
				// ColumnSizingArea property to the override. If ColumnSizingArea is set to 
				// HeadersOnly then don't allow sizing cells using cell borders.
				//
				// ----------------------------------------------------------------------------------
				if ( ColumnSizingArea.HeadersOnly == this.ColumnSizingAreaResolvedDefault )
				{
					// SSP 1/6/04
					// I noticed this while debugging BR01489. If the RowSizing is set to Fixed
					// then don't allow the user to resizie the cell heights which in effect is
					// the same as resizing the row heights.
					//
					// SSP 9/13/07 BR25959
					// The same applies to AutoFixed as well.
					// 
					//if ( RowSizing.Fixed == this.RowSizingResolved )
					if ( RowSizing.Fixed == rowSizing || RowSizing.AutoFixed == rowSizing )
						return RowLayoutSizing.None;

					return RowLayoutSizing.Vertical;
				}
				// ----------------------------------------------------------------------------------

				// SSP 1/6/04
				// I noticed this while debugging BR01489. If the RowSizing is set to Fixed
				// then don't allow the user to resize the cell heights which in effect is
				// the same as resizing the row heights.
				//
				// SSP 9/13/07 BR25959
				// The same applies to AutoFixed as well.
				// 
				//if ( RowSizing.Fixed == this.RowSizingResolved )
				if ( RowSizing.Fixed == rowSizing || RowSizing.AutoFixed == rowSizing )
					return RowLayoutSizing.Horizontal;

				return RowLayoutSizing.Both;
			}
		}

		#endregion // AllowRowLayoutCellSizingResolved

		#region AllowRowLayoutLabelSizingResolved
		
		internal RowLayoutSizing AllowRowLayoutLabelSizingResolved
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeAllowRowLayoutLabelSizing( ) )
					return this.overrideObj.AllowRowLayoutLabelSizing;

				if ( this.Layout.HasOverride && this.Layout.Override.ShouldSerializeAllowRowLayoutLabelSizing( ) )
					return this.Layout.Override.AllowRowLayoutLabelSizing;

				if ( AllowColSizing.None == this.AllowColSizingResolved )
					return RowLayoutSizing.None;

				// SSP 11/16/04
				// Implemented column sizing using cells in non-rowlayout mode. Added 
				// ColumnSizingArea property to the override. If ColumnSizingArea is set to 
				// HeadersOnly then don't allow sizing cells using cell borders.
				//
				// ----------------------------------------------------------------------------------
				if ( ColumnSizingArea.CellsOnly == this.ColumnSizingAreaResolvedDefault )
					return RowLayoutSizing.Vertical;
				// ----------------------------------------------------------------------------------

				// SSP 6/26/03 UWG2387
				// Don't base the default of the RowLayoutLabelSizing to RowLayoutCellSizing settings.
				// Resolve it to Both if it's default.
				//
				//return this.AllowRowLayoutCellSizingResolved;
				return RowLayoutSizing.Both;
			}
		}

		#endregion // AllowRowLayoutLabelSizingResolved

		#region RowLayoutManager
		
		internal LayoutManagerBase RowLayoutManager
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.gridBagRowLayoutManager;
			}
		}

		#endregion // RowLayoutManager

		#region HeaderLayoutManager
		
		internal LayoutManagerBase HeaderLayoutManager
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.gridBagHeaderLayoutManager;
			}
		}

		#endregion // HeaderLayoutManager

		#region PreferredRowLayoutSize
		
		internal Size PreferredRowLayoutSize
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.preferredRowLayoutSize;		
			}
		}

		#endregion // PreferredRowLayoutSize

		#region PreferredHeaderLayoutSize
		
		internal Size PreferredHeaderLayoutSize
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.preferredHeaderLayoutSize;
			}
		}

		#endregion // PreferredHeaderLayoutSize

		#region PreferredSummaryLayoutSize
		
		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Obsoleted by the new GetSummaryLayoutPreferredSize method.
		//
		

		#endregion // PreferredSummaryLayoutSize

		#region MinimumRowLayoutSize
		
		internal Size MinimumRowLayoutSize
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.minimumRowLayoutSize;
			}
		}

		#endregion // MinimumRowLayoutSize

		#region MinimumHeaderLayoutSize
		
		internal Size MinimumHeaderLayoutSize
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.minimumHeaderLayoutSize;
			}
		}

		#endregion // MinimumHeaderLayoutSize

		#region ColumnVisiblePositionComparer
		
		/// <summary>
		/// A IComparer implementation for sorting columns by their visible positions.
		/// </summary>
		public class ColumnVisiblePositionComparer : IComparer
		{
			/// <summary>
			/// Constructor.
			/// </summary>
			public ColumnVisiblePositionComparer( )
			{
			}

			int IComparer.Compare( object x, object y )
			{
				if ( x == y )
					return 0;
				else if ( null == x )
					return -1;
				else if ( null == y )
					return 1;

				UltraGridColumn colX = (UltraGridColumn)x;
				UltraGridColumn colY = (UltraGridColumn)y;

				int visiblePosX = colX.Header.VisiblePosition;
				int visiblePosY = colY.Header.VisiblePosition;

				if ( visiblePosX == visiblePosY )
					return colX.Index.CompareTo( colY.Index );
				else
				{
					// MD 8/3/07 - 7.3 Performance
					// Use cached values - Prevent calling expensive getters multiple times
					//return colX.Header.VisiblePosition.CompareTo( colY.Header.VisiblePosition );
					return visiblePosX.CompareTo( visiblePosY );
				}
			}
		}

		#endregion // ColumnVisiblePositionComparer

		#region AreColumnsInSeparateLayoutArea
		
		internal bool AreColumnHeadersInSeparateLayoutArea
		{
			get
			{
				if ( this.CardView )
				{
					if ( CardStyle.MergedLabels == this.CardSettings.StyleResolved )
						return true;
				}
				else
				{
					if ( RowLayoutLabelStyle.Separate == this.RowLayoutLabelStyle )
						return true;
				}

				return false;
			}
		}

		#endregion // AreColumnsInSeparateLayoutArea

		#region LayoutItemDimensions
		
		internal GridBagLayoutItemDimensionsCollection LayoutItemDimensions
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.layoutItemDimensions;
			}
		}

		#endregion // LayoutItemDimensions

		#region LayoutItemDimensionsHeaderArea
		
		internal GridBagLayoutItemDimensionsCollection LayoutItemDimensionsHeaderArea
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.layoutItemDimensionsHeaderArea;
			}
		}

		#endregion // LayoutItemDimensionsHeaderArea

		#region LayoutItemDimensionsCellArea
		
		internal GridBagLayoutItemDimensionsCollection LayoutItemDimensionsCellArea
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.layoutItemDimensionsCellArea;
			}
		}

		#endregion // LayoutItemDimensionsCellArea

		#region RowLayoutCacheVersion

		internal int RowLayoutCacheVersion
		{
			get
			{
				return this.rowLayoutCacheVersion + ( null != this.layout ? this.layout.RowLayoutVersion : 0 );
			}
		}

		#endregion // RowLayoutCacheVersion

		// MD 1/23/09 - Groups in RowLayouts
		// Added another row layout version number to be used by the layout items.
		#region RowLayoutCacheVersionForLayoutItems

		internal int RowLayoutCacheVersionForLayoutItems
		{
			get
			{
				return this.RowLayoutCacheVersion + this.rowLayoutItemCacheVersion;
			}
		}

		#endregion // RowLayoutCacheVersionForLayoutItems

		#region SummaryLayoutManager

		internal LayoutManagerBase SummaryLayoutManager
		{
			get
			{
				if ( ! this.HasSummaries )
					return null;

				this.VerifyRowLayoutCache( );

				return this.gridBagSummaryLayoutManager;
			}
		}

		#endregion // SummaryLayoutManager

		#region GetSummaryLayoutManager

		// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//

		private LayoutManagerBase[] cachedSummaryLayoutManagers = null;
		private Size[] cachedSummaryLayoutManagerSizes = null;
		private const int SUMMARY_LAYOUT_MANAGER_CACHE_LENGTH = 32;
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private int GetSummaryLayoutManagerCacheIndex( SummaryDisplayAreaContext context )
		private static int GetSummaryLayoutManagerCacheIndex( SummaryDisplayAreaContext context )
		{
			Debug.Assert( null != context );
			if ( null == context )
				return -1;

			int index;
			int enumVal = (int)context.DisplayArea;
			for ( index = 0; index < SUMMARY_LAYOUT_MANAGER_CACHE_LENGTH; index++ )
			{
				if ( 0 != ( 1 & ( enumVal >> index ) ) )
					break;
			}

			if ( index >= SUMMARY_LAYOUT_MANAGER_CACHE_LENGTH )
			{
				Debug.Assert( false );
				return -1;
			}

			return index;
		}

		internal LayoutManagerBase GetSummaryLayoutManager( SummaryDisplayAreaContext context )
		{
			this.VerifyRowLayoutCache( );

			GridBagLayoutManager srcLM = (GridBagLayoutManager)this.SummaryLayoutManager;
			GridBagLayoutManager destLM = null;
			if ( null != srcLM )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//int index = this.GetSummaryLayoutManagerCacheIndex( context );
				int index = UltraGridBand.GetSummaryLayoutManagerCacheIndex( context );

				if ( index >= 0 )
				{
					if ( null == this.cachedSummaryLayoutManagers )
						this.cachedSummaryLayoutManagers = new LayoutManagerBase[ SUMMARY_LAYOUT_MANAGER_CACHE_LENGTH ];

					destLM = (GridBagLayoutManager)this.cachedSummaryLayoutManagers[ index ];
					if ( null == destLM )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.cachedSummaryLayoutManagers[ index ] = destLM = this.CreateGridBagLayoutManager( );
						this.cachedSummaryLayoutManagers[ index ] = destLM = UltraGridBand.CreateGridBagLayoutManager();
					}
				}

				// Create a new summary layout manager from the base layout manager (SummaryLayoutManager)
				// that we populated in VerifyRowLayoutCache method.
				//
				if ( null != destLM && 0 == destLM.LayoutItems.Count )
				{
                    // MRS - NAS 9.1 - Groups in RowLayout
                    // Re-factored this so we can call is recursively walking down into the groups. 
                    #region Old Code
                    //foreach ( ILayoutItem item in srcLM.LayoutItems )
                    //{
                    //    SummaryLayoutItem summaryLayoutItem = item as SummaryLayoutItem;
                    //    if ( null != summaryLayoutItem )
                    //        destLM.LayoutItems.Add( new SummaryLayoutItem( summaryLayoutItem.Column, context ), 
                    //            srcLM.LayoutItems.GetConstraint( summaryLayoutItem ) );
                    //    else
                    //        // If it's not an instance of SummaryLayoutItem then it could be a header.
                    //        //
                    //        destLM.LayoutItems.Add( item, srcLM.LayoutItems.GetConstraint( item ) );
                    //}
                    #endregion // Old Code
                    //
                    UltraGridBand.GetSummaryLayoutManagerHelper(context, srcLM, destLM);                    

                    // Make sure the widths of the summaries are the same as the widths of the cells
					// so summaries are aligned with the cells.
					//
					int[] srcArr = srcLM.ColumnWidths;
					int[] destArr = destLM.ColumnWidths;
					Array.Copy( srcArr, destArr, Math.Min( srcArr.Length, destArr.Length ) );

					// Calculate the preferred size of the summary layout.
					//
					if ( null == this.cachedSummaryLayoutManagerSizes )
						this.cachedSummaryLayoutManagerSizes = new Size[ SUMMARY_LAYOUT_MANAGER_CACHE_LENGTH ];

					this.cachedSummaryLayoutManagerSizes[ index ] = destLM.CalculatePreferredSize(
						this.GetCachedLayoutContainerCalcSize( ), null );
				}
			}

			return destLM;
		}        

		#endregion // GetSummaryLayoutManager

		#region GetSummaryLayoutPreferredSize

		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GetSummaryLayoutPreferredSize method.
		//
		internal Size GetSummaryLayoutPreferredSize( SummaryDisplayAreaContext context )
		{
			this.GetSummaryLayoutManager( context );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//int index = this.GetSummaryLayoutManagerCacheIndex( context );
			int index = UltraGridBand.GetSummaryLayoutManagerCacheIndex( context );

			return index >= 0 ? this.cachedSummaryLayoutManagerSizes[ index ] : Size.Empty;
		}

		#endregion // GetSummaryLayoutPreferredSize

		#region DirtyRowLayoutCachedInfo
		
		internal void DirtyRowLayoutCachedInfo( )
		{
			if ( null != this.layout )
			{
				// SSP 8/12/03 - Optimizations - Grand Verify Version Number
				//
				this.layout.BumpGrandVerifyVersion( );

				this.isMinRowHeightCached = false;
				this.isRowFontHeightCached = false;
				this.isRowHeightDirty = true;
				this.ClearCachedHeightValues( );
				this.layout.BumpCellChildElementsCacheVersion( );
				this.layout.DirtyGridElement( true, true );
				this.layout.SynchronizedColListsDirty = true;
				this.layout.GetColScrollRegions( false ).DirtyMetrics( );
			}

			// SSP 7/29/05 - NAS 5.3 Tab Index
			// 
			this.DirtyTabOrderedColumns( );
			
			this.rowLayoutCacheVersion++;
		}

		#endregion // DirtyRowLayoutCachedInfo

		#region GetLayoutVisibleColumns
		
		// MD 1/19/09 - Groups in RowLayout
		// Added a parameter to allow all columns to be obtained.
		//private UltraGridColumn[] GetLayoutVisibleColumns( )
		private UltraGridColumn[] GetLayoutVisibleColumns( bool getRootColumnsOnly )
		{
			ColumnsCollection columns = this.Columns;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( columns.Count );
			List<UltraGridColumn> list = new List<UltraGridColumn>( columns.Count );

			for ( int i = 0, count = columns.Count; i < count; i++ )
			{ 
				UltraGridColumn column = columns[i];

				if ( null != column && column.IsVisibleInLayout &&
                    // MRS - NAS 9.1 - Groups in RowLayout
					this.ShouldIncludeInLayout( column, getRootColumnsOnly ) )
					list.Add( column );
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (UltraGridColumn[])list.ToArray( typeof( UltraGridColumn ) );
			return list.ToArray();
		}

		#endregion // GetLayoutVisibleColumns

		#region VerifyRowLayoutCache

		internal void VerifyRowLayoutCache( )
		{
			if ( this.verifiedRowLayoutCacheVersion == this.RowLayoutCacheVersion )
				return;

			// Anti-recursion flag.
			//
			if ( this.inVerifyRowLayoutCache )
				return;

			// SSP 8/6/03 UWG2568
			// When we are resizing all the columns, we don't want to recalculate the row layout
			// until the operation of resizing all the columns has finished. To accomplish that
			// added dontVerifyRowLayoutCache flag.
			//
			if ( this.dontVerifyRowLayoutCache )
				return;

			this.inVerifyRowLayoutCache = true;

			this.verifiedRowLayoutCacheVersion = this.RowLayoutCacheVersion;

			// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			this.cachedSummaryLayoutManagers = null;
			this.cachedSummaryLayoutManagerSizes = null;

			// Get an instance of ILayoutContainer specifically for calculating the container rect.
			//
			LayoutContainerCalcSize layoutContainer = this.GetCachedLayoutContainerCalcSize( );
			layoutContainer.Initialize( );

			try
			{			
				int i;
				// Headers can be separate from the cells or they can be togather.
				//
				bool headersInSeparateArea = this.AreColumnHeadersInSeparateLayoutArea;

				// Get the visible columns array. (The columns with Hidden set to false).
				//
				// MD 1/19/09 - Groups in RowLayout
				// Added a parameter to allow all columns to be obtained.
				//UltraGridColumn[] columnsArr = this.GetLayoutVisibleColumns( );                
				// MD 2/18/09 - TFS14144
				// Moved this code to below so we sort the columns after verifying the groups. This should have been done 
				// so that the layout item dimensions cached on the groups were valid for the child columns to search for
				// themselves in the collection.
				//UltraGridColumn[] columnsArr = this.GetLayoutVisibleColumns( true );
				//
				//// Sort t`he visible columns according to the their VisiblePosition settings.
				////
				//Infragistics.Win.Utilities.SortMerge( columnsArr, new UltraGridBand.ColumnVisiblePositionComparer( ) );

                // MRS - NAS 9.1 - Groups in RowLayout
                // ---------------------------------------------------------------------
				// MD 1/19/09 - Groups in RowLayout
				// Added a parameter to allow all groups to be obtained.
                //UltraGridGroup[] groupsArr = this.GetLayoutVisibleGroups();
				UltraGridGroup[] groupsArr = this.GetLayoutVisibleGroups( true );
                
                // Sort the visible groups according to the their VisiblePosition settings.
                //
                Infragistics.Win.Utilities.SortMerge(groupsArr, new UltraGridBand.GroupVisiblePositionComparer());
                // ---------------------------------------------------------------------                

                // MRS - NAS 9.1 - Groups in RowLayout
                // ---------------------------------------------------------------------                
                for (i = 0; i < groupsArr.Length; i++)
                {
                    UltraGridGroup group = groupsArr[i];
                    group.VerifyRowLayoutCache();
                }
                // ---------------------------------------------------------------------

				// MD 2/18/09 - TFS14144
				// Moved this code to below so we sort the columns after verifying the groups. This should have been done 
				// so that the layout item dimensions cached on the groups were valid for the child columns to search for
				// themselves in the collection.
				// Sort the visible columns according to the their VisiblePosition settings.
				UltraGridColumn[] columnsArr = this.GetLayoutVisibleColumns( true );
				Infragistics.Win.Utilities.SortMerge( columnsArr, new UltraGridBand.ColumnVisiblePositionComparer() );

				// Set up the gridbag layout manager with the visible columns as its items.
				//
				if ( null == this.gridBagLayoutManagerHelper )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.gridBagLayoutManagerHelper = this.CreateGridBagLayoutManager( );
					this.gridBagLayoutManagerHelper = UltraGridBand.CreateGridBagLayoutManager();
				}

				this.gridBagLayoutManagerHelper.LayoutItems.Clear( );

                // MRS - NAS 9.1 - Groups in RowLayout
                // ---------------------------------------------------------------------
                for (i = 0; i < groupsArr.Length; i++)
                {
                    UltraGridGroup group = groupsArr[i];
                    RowLayoutColumnInfo ci = group.RowLayoutGroupInfo;
                    GridBagConstraint gridBagConstraint = new GridBagConstraint(ci.OriginX, ci.OriginY, ci.SpanXResolved, ci.SpanYResolved);
                    this.gridBagLayoutManagerHelper.LayoutItems.Add(ci, gridBagConstraint);
                }
                // ---------------------------------------------------------------------

				for ( i = 0; i < columnsArr.Length; i++ )
				{
					UltraGridColumn column = columnsArr[i];
					RowLayoutColumnInfo ci = column.RowLayoutColumnInfo;
					this.gridBagLayoutManagerHelper.LayoutItems.Add( ci, ci );
				}               

				this.layoutItemDimensions = this.gridBagLayoutManagerHelper.GetLayoutItemDimensions( layoutContainer, null );

                // For each child group, set the Origin now. 
                for (i = 0; i < groupsArr.Length; i++)
                {
                    UltraGridGroup group = groupsArr[i];
                    RowLayoutColumnInfo ci = group.RowLayoutGroupInfo;
                    ci.cachedOriginXResolved = this.layoutItemDimensions[ci].OriginX;
                    ci.cachedOriginYResolved = this.layoutItemDimensions[ci].OriginY;
                }

				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// In AutoFitStyle mode of ExtendLastColumn set the WeightX on the rightmost
				// column so that it extends to occupy the extra visible space. The extent of 
				// the band is already adjusted to include the extra space in FiltColumnsToWidth 
				// method.
				//
				// ------------------------------------------------------------------------------
				// Reset WeightXDefault we might have set previously.
				//
				for ( i = 0; i < columnsArr.Length; i++ )
					columnsArr[i].RowLayoutColumnInfo.WeightXDefault = 0.0f;

                // MRS - NAS 9.1 - Groups in RowLayout
                // ---------------------------------------------------------------------
                for (i = 0; i < groupsArr.Length; i++)
                    groupsArr[i].RowLayoutGroupInfo.WeightXDefault = 0.0f;
                // ---------------------------------------------------------------------
				
				if ( this.Layout.AutoFitExtendLastColumn && ! this.CardView )
				{
					RowLayoutColumnInfo maxOriginXItem = null;
					bool anyItemHasWeightX = false;
					GridBagLayoutItemDimensionsCollection dims = this.layoutItemDimensions;

					for ( i = 0; ! anyItemHasWeightX && i < columnsArr.Length; i++ )
					{
						RowLayoutColumnInfo ci = columnsArr[i].RowLayoutColumnInfo;
						if ( dims.Exists( ci ) )
						{
							if ( null == maxOriginXItem )
								maxOriginXItem = ci;
							else if ( dims[ ci ].OriginX > dims[ maxOriginXItem ].OriginX )
								maxOriginXItem = ci;

							anyItemHasWeightX = ci.WeightX > 0.0f;
						}
					}

                    // MRS - NAS 9.1 - Groups in RowLayout
                    // ---------------------------------------------------------------------
                    for (i = 0; !anyItemHasWeightX && i < groupsArr.Length; i++)
                    {
                        RowLayoutColumnInfo ci = groupsArr[i].RowLayoutGroupInfo;
                        if (dims.Exists(ci))
                        {
                            if (null == maxOriginXItem)
                                maxOriginXItem = ci;
                            else if (dims[ci].OriginX > dims[maxOriginXItem].OriginX)
                                maxOriginXItem = ci;

                            anyItemHasWeightX = ci.WeightX > 0.0f;
                        }
                    }
                    // ---------------------------------------------------------------------

					// Only set the WeightX on the column if none of the columns has WeightX set.
					//
					if ( ! anyItemHasWeightX && null != maxOriginXItem )
					{
						// MD 1/21/09 - Groups in RowLayouts
						// If the layout item is a group, we have to set the WeightXDefault on the right-most column under the group.
						//maxOriginXItem.WeightXDefault = 1.0f;
						if ( maxOriginXItem.Column != null || maxOriginXItem.Group == null )
						{
							maxOriginXItem.WeightXDefault = 1.0f;
						}
						else
						{
							UltraGridColumn rightMost = maxOriginXItem.Group.GetRightMostColumn();

							if ( rightMost != null )
								rightMost.RowLayoutColumnInfo.WeightXDefault = 1.0f;
							else
								maxOriginXItem.WeightXDefault = 1.0f;
						}
					}
				}
				// ------------------------------------------------------------------------------
			
				if ( null == this.gridBagRowLayoutManager )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.gridBagRowLayoutManager = this.CreateGridBagLayoutManager( );
					this.gridBagRowLayoutManager = UltraGridBand.CreateGridBagLayoutManager();
				}

				this.gridBagRowLayoutManager.LayoutItems.Clear( );

				if ( null == this.gridBagHeaderLayoutManager )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.gridBagHeaderLayoutManager = this.CreateGridBagLayoutManager( );
					this.gridBagHeaderLayoutManager = UltraGridBand.CreateGridBagLayoutManager();
				}

				this.gridBagHeaderLayoutManager.LayoutItems.Clear( );

				// If summaries are visible, then create a gridbag lm for summary area otherwise
				// dispose of it if it was created previously.
				// 
				if ( this.HasSummaries && ! this.CardView )
				{
					if ( null == this.gridBagSummaryLayoutManager )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.gridBagSummaryLayoutManager = this.CreateGridBagLayoutManager( );
						this.gridBagSummaryLayoutManager = UltraGridBand.CreateGridBagLayoutManager();
					}

					this.gridBagSummaryLayoutManager.LayoutItems.Clear( );
				}
				else
				{
					if ( null != this.gridBagSummaryLayoutManager )
						this.gridBagSummaryLayoutManager.LayoutItems.Clear( );
					this.gridBagSummaryLayoutManager = null;
				}

				if ( headersInSeparateArea )
				{
                    // MRS - NAS 9.1 - Groups in RowLayout
                    // ---------------------------------------------------------------------
                    for (i = 0; i < groupsArr.Length; i++)
                    {
                        UltraGridGroup group = groupsArr[i];                       

                        this.gridBagRowLayoutManager.LayoutItems.Add(group.GroupLayoutItemCellContent, group.GroupLayoutItemCellContent);
                                                 
                        // Even though the headers are not with the cells, we still need to add in 
                        // a group header with the cells when it's aligned to one side. If we don't, 
                        // there will be extra space in the layout and the columns / headers may not
                        // line up with the cells. 
                        //
                        bool addGroupHeaderInCellArea = UltraGridBand.ShouldAddGroupHeaderToCellArea(group);
                        if (addGroupHeaderInCellArea)
                            this.gridBagRowLayoutManager.LayoutItems.Add(group.Header, group.Header);
                        
                        this.gridBagHeaderLayoutManager.LayoutItems.Add(group.GroupLayoutItemHeaderContent, group.GroupLayoutItemHeaderContent);
                        this.gridBagHeaderLayoutManager.LayoutItems.Add(group.Header, group.Header);
                        
                        // If we have summaries then add items to summary layout manager as well.
                        //
                        if (null != this.gridBagSummaryLayoutManager)
                        {
                            this.gridBagSummaryLayoutManager.LayoutItems.Add(group.GroupLayoutItemSummaryContent, group.GroupLayoutItemSummaryContent);
                                                        
                            if (addGroupHeaderInCellArea)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(group.Header, group.Header);
                        }
                    }
                    // ---------------------------------------------------------------------

					for ( i = 0; i < columnsArr.Length; i++ )
					{
						UltraGridColumn column = columnsArr[i];

						this.gridBagRowLayoutManager.LayoutItems.Add( column, column );
						this.gridBagHeaderLayoutManager.LayoutItems.Add( column.Header, column.Header );

						// If we have summaries then add items to summary layout manager as well.
						//
						if ( null != this.gridBagSummaryLayoutManager )
							this.gridBagSummaryLayoutManager.LayoutItems.Add( new SummaryLayoutItem( column ), column );
					}

                    // MRS - NAS 9.1 - Groups in RowLayout
                    // Moved this code down until after the flattening of the layouts. 
                    #region Moved Code
                    //int[] gbCellColumnWidths = this.gridBagRowLayoutManager.ColumnWidths;
                    //int[] gbHeaderColumnWidths = this.gridBagHeaderLayoutManager.ColumnWidths;
                    //int[] gbCellRowHeights = this.gridBagRowLayoutManager.RowHeights;
                    //int[] gbHeaderRowHeights = this.gridBagHeaderLayoutManager.RowHeights;

                    //// In card-view, headers need to have the same height as the cells and
                    //// in regular view, headers need to have the same width as the cells.
                    //// 
                    //if ( null != gbCellColumnWidths && null != gbCellRowHeights
                    //    && null != gbHeaderColumnWidths && null != gbHeaderRowHeights )
                    //{
                    //    // MRS - NAS 9.1 - Groups in RowLayout
                    //    // I moved this down into the if (this.CardView) block. Only one of these
                    //    // two pairs needs to be the same.
                    //    //
                    //    //Debug.Assert( gbCellColumnWidths.Length == gbHeaderColumnWidths.Length &&
                    //    //    gbCellRowHeights.Length == gbHeaderRowHeights.Length, "Lengths should be the same." );

                    //    int[] arr1 = null;
                    //    int[] arr2 = null;

                    //    if ( this.CardView )
                    //    {
                    //        // MRS - NAS 9.1 - Groups in RowLayout
                    //        Debug.Assert( gbCellRowHeights.Length == gbHeaderRowHeights.Length, "Lengths should be the same." );

                    //        // In card view make sure the corresponding headers and cells have the same heights.
                    //        //
                    //        arr1 = gbCellRowHeights;
                    //        arr2 = gbHeaderRowHeights;
                    //    }
                    //    else
                    //    {
                    //        // MRS - NAS 9.1 - Groups in RowLayout                            
                    //        Debug.Assert( gbCellColumnWidths.Length == gbHeaderColumnWidths.Length, "Lengths should be the same." );

                    //        // In regular view make sure the corresponding headers and cells have the same widths.
                    //        //
                    //        arr1 = gbCellColumnWidths;
                    //        arr2 = gbHeaderColumnWidths;
                    //    }

                    //    int len = Math.Min( arr1.Length, arr2.Length );
                    //    for ( int j = 0; j < len; j++ )
                    //        arr1[ j ] = arr2[ j ] = Math.Max( arr1[ j ], arr2[ j ] );
                    //}
                    //else
                    //{
                    //    Debug.Assert( false, "ColumnWidths and RowHeights off the gridbag lm should have been non-null !" );
                    //}
                    #endregion // Old Code
                }
				else
				{
                    // MRS - NAS 9.1 - Groups in RowLayout
                    // ---------------------------------------------------------------------
                    for (i = 0; i < groupsArr.Length; i++)
                    {
                        UltraGridGroup group = groupsArr[i];

                        LabelPosition labelPosition = group.RowLayoutGroupInfo.LabelPositionResolved;

                        // SSP 11/17/04
                        // Added a way to hide the header or the cell of a group in the row layout mode.
                        // This way you can do things like hide a row of headers or add unbound groups
                        // and hide their cells to show grouping headers etc...
                        // Added LabelOnly member to the enum.
                        //						
                        //if ( LabelPosition.Left == labelPosition ||	LabelPosition.Top == labelPosition )
                        if (LabelPosition.LabelOnly == labelPosition
                            || LabelPosition.Left == labelPosition || LabelPosition.Top == labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add(group.Header, group.Header);                            

                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(group.Header, group.Header);
                        }

                        // SSP 11/17/04
                        // Added a way to hide the header or the cell of a group in the row layout mode.
                        // This way you can do things like hide a row of headers or add unbound groups
                        // and hide their cells to show grouping headers etc...
                        // Added LabelOnly member to the enum. Enclosed the existing code into the if
                        // block so we don't add the group if the LabelPosition is LabelOnly.
                        // 
                        if (LabelPosition.LabelOnly != labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add(group.GroupLayoutItemCellContent, group.GroupLayoutItemCellContent);

                            
                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(group.GroupLayoutItemSummaryContent, group.GroupLayoutItemSummaryContent);
                        }

                        if (LabelPosition.Right == labelPosition || LabelPosition.Bottom == labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add(group.Header, group.Header);

                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(group.Header, group.Header);
                        }
                    }
                    // ---------------------------------------------------------------------

					for ( i = 0; i < columnsArr.Length; i++ )
					{
						UltraGridColumn column = columnsArr[i];

						LabelPosition labelPosition = column.RowLayoutColumnInfo.LabelPositionResolved;

						// SSP 11/17/04
						// Added a way to hide the header or the cell of a column in the row layout mode.
						// This way you can do things like hide a row of headers or add unbound columns
						// and hide their cells to show grouping headers etc...
						// Added LabelOnly member to the enum.
						//						
						//if ( LabelPosition.Left == labelPosition ||	LabelPosition.Top == labelPosition )
						if ( LabelPosition.LabelOnly == labelPosition 
							|| LabelPosition.Left == labelPosition || LabelPosition.Top == labelPosition )
						{
							this.gridBagRowLayoutManager.LayoutItems.Add( column.Header, column.Header );

							// If we have summaries then add items to summary layout manager as well.
							//
							if ( null != this.gridBagSummaryLayoutManager )
								this.gridBagSummaryLayoutManager.LayoutItems.Add( column.Header, column.Header );
						}

						// SSP 11/17/04
						// Added a way to hide the header or the cell of a column in the row layout mode.
						// This way you can do things like hide a row of headers or add unbound columns
						// and hide their cells to show grouping headers etc...
						// Added LabelOnly member to the enum. Enclosed the existing code into the if
						// block so we don't add the column if the LabelPosition is LabelOnly.
						// 
						if ( LabelPosition.LabelOnly != labelPosition )
						{
							this.gridBagRowLayoutManager.LayoutItems.Add( column, column );

							// If we have summaries then add items to summary layout manager as well.
							//
							if ( null != this.gridBagSummaryLayoutManager )
								this.gridBagSummaryLayoutManager.LayoutItems.Add( new SummaryLayoutItem( column ), column );
						}

						if ( LabelPosition.Right == labelPosition || LabelPosition.Bottom == labelPosition )
						{
							this.gridBagRowLayoutManager.LayoutItems.Add( column.Header, column.Header );

							// If we have summaries then add items to summary layout manager as well.
							//
							if ( null != this.gridBagSummaryLayoutManager )
								this.gridBagSummaryLayoutManager.LayoutItems.Add( column.Header, column.Header );
						}
					}                    
				}

                // MRS - NAS 9.1 - Groups in RowLayout
                if (this.RowLayoutStyle == RowLayoutStyle.GroupLayout)
                {
                    // Store the grouped layout managers and replace them with flattened versions.
                    // We need to keep the grouped managers to help with drag/drop

                    this.groupedGridBagHeaderLayoutManager = this.gridBagHeaderLayoutManager;
                    this.groupedGridBagRowLayoutManager = this.gridBagRowLayoutManager;
                    this.groupedGridBagSummaryLayoutManager = this.gridBagSummaryLayoutManager;

                    // Flatten the Layouts
                    if (this.gridBagHeaderLayoutManager != null)
                        this.gridBagHeaderLayoutManager = (GridBagLayoutManager)GridUtils.GetFlattenedLayoutManager(this.groupedGridBagHeaderLayoutManager);

                    if (this.gridBagRowLayoutManager != null)
                        this.gridBagRowLayoutManager = (GridBagLayoutManager)GridUtils.GetFlattenedLayoutManager(this.groupedGridBagRowLayoutManager);

                    if (this.gridBagSummaryLayoutManager != null)
                        this.gridBagSummaryLayoutManager = (GridBagLayoutManager)GridUtils.GetFlattenedLayoutManager(this.groupedGridBagSummaryLayoutManager);

					// MD 1/19/09 - Groups in RowLayout
					// When the flattened layouts are set, the ordered header collections are no longer valid, so dirty them.
					this.orderedColumnsDirty = true;
					this.orderedGroupsDirty = true;

					// MD 1/23/09 - Groups in RowLayouts
					// When dirty the version for layout items. Now that the flattened layouts have been created, the can cache their 
					// correctly resolved origins.
					this.rowLayoutItemCacheVersion++;
                }
                else
                {
                    // Since there is no grouping, clear the grouped layout managers. 
                    this.groupedGridBagHeaderLayoutManager = null;
                    this.groupedGridBagRowLayoutManager = null;
                    this.groupedGridBagSummaryLayoutManager = null;
                }                

                // MRS - NAS 9.1 - Groups in RowLayout
                // Moved this code down from above.
                // This code synchronizes the RowHeight and ColumnWidths between the headers and cells.
                // It needs to take place after the flattening of the layout.                 
                if (headersInSeparateArea)
                {
                    int[] gbCellColumnWidths = this.gridBagRowLayoutManager.ColumnWidths;
                    int[] gbHeaderColumnWidths = this.gridBagHeaderLayoutManager.ColumnWidths;
                    int[] gbCellRowHeights = this.gridBagRowLayoutManager.RowHeights;
                    int[] gbHeaderRowHeights = this.gridBagHeaderLayoutManager.RowHeights;

                    // In card-view, headers need to have the same height as the cells and
                    // in regular view, headers need to have the same width as the cells.
                    // 
                    if (null != gbCellColumnWidths && null != gbCellRowHeights
                        && null != gbHeaderColumnWidths && null != gbHeaderRowHeights)
                    {
                        // MRS - NAS 9.1 - Groups in RowLayout
                        // I moved this down into the if (this.CardView) block. Only one of these
                        // two pairs needs to be the same.
                        //
                        //Debug.Assert( gbCellColumnWidths.Length == gbHeaderColumnWidths.Length &&
                        //    gbCellRowHeights.Length == gbHeaderRowHeights.Length, "Lengths should be the same." );

                        int[] arr1 = null;
                        int[] arr2 = null;

                        if (this.CardView)
                        {
                            // MRS - NAS 9.1 - Groups in RowLayout
                            Debug.Assert(gbCellRowHeights.Length == gbHeaderRowHeights.Length, "Lengths should be the same.");

                            // In card view make sure the corresponding headers and cells have the same heights.
                            //
                            arr1 = gbCellRowHeights;
                            arr2 = gbHeaderRowHeights;
                        }
                        else
                        {
                            // MRS - NAS 9.1 - Groups in RowLayout                            
                            Debug.Assert(gbCellColumnWidths.Length == gbHeaderColumnWidths.Length, "Lengths should be the same.");

                            // In regular view make sure the corresponding headers and cells have the same widths.
                            //
                            arr1 = gbCellColumnWidths;
                            arr2 = gbHeaderColumnWidths;
                        }

                        int len = Math.Min(arr1.Length, arr2.Length);
                        for (int j = 0; j < len; j++)
                            arr1[j] = arr2[j] = Math.Max(arr1[j], arr2[j]);
                    }
                    else
                    {
                        Debug.Assert(false, "ColumnWidths and RowHeights off the gridbag lm should have been non-null !");
                    }
                }

				if ( null != this.gridBagSummaryLayoutManager )
				{
					int[] gbCellColumnWidths = this.gridBagRowLayoutManager.ColumnWidths;
					int[] gbCellRowHeights = this.gridBagRowLayoutManager.RowHeights;
					int[] gbSummaryColumnWidths = this.gridBagSummaryLayoutManager.ColumnWidths;
					int[] gbSummaryRowHeights = this.gridBagSummaryLayoutManager.RowHeights;

					Debug.Assert( gbCellColumnWidths.Length == gbSummaryColumnWidths.Length &&
						gbCellRowHeights.Length == gbSummaryRowHeights.Length, "Lengths should be the same." );

					for ( int j = Math.Min( gbCellColumnWidths.Length, gbSummaryColumnWidths.Length ) - 1; j >= 0; j-- )
						gbSummaryColumnWidths[ j ] = gbCellColumnWidths[ j ];

					// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
					// Obsoleted by the new GetSummaryLayoutPreferredSize method.
					//
					//this.preferredSummaryLayoutSize = this.gridBagSummaryLayoutManager.CalculatePreferredSize( layoutContainer, null );
				}
				else
				{
					// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
					// Obsoleted by the new GetSummaryLayoutPreferredSize method.
					//
					//this.preferredSummaryLayoutSize = Size.Empty;
				}

                this.preferredRowLayoutSize = this.gridBagRowLayoutManager.CalculatePreferredSize(layoutContainer, null);
                this.preferredHeaderLayoutSize = headersInSeparateArea
                    ? this.gridBagHeaderLayoutManager.CalculatePreferredSize(layoutContainer, null)
                    : Size.Empty;

                // SSP 3/5/05 BR02359
                // Cache the minimum layout sizes as well.
                //
                this.minimumRowLayoutSize = this.gridBagRowLayoutManager.CalculateMinimumSize(layoutContainer, null);
                this.minimumHeaderLayoutSize = headersInSeparateArea
                    ? this.gridBagHeaderLayoutManager.CalculateMinimumSize(layoutContainer, null)
                    : Size.Empty;

				this.CalcLayoutHelper( );
			}
			finally
			{
				// Reset the anti-recursion flag.
				//
				this.inVerifyRowLayoutCache = false;
			}
		}        

		#endregion // VerifyRowLayoutCache

		#region GetExtentLayout

		internal int GetExtentLayout( )
		{
			return this.GetExtentLayout( false, BandOrigin.RowSelector );
		}

		// SSP 3/5/05 BR02359
		// Added an overload of GetExtentLayout that takes in the minimum parameter to return 
		// the extend based on the minimum size of the row layout (based on MinimumCellSize 
		// and MinimumLabelSize). Also added bandOrigin for convenience.
		//
		internal int GetExtentLayout( bool minimum, BandOrigin bandOrigin )
		{
			// SSP 3/5/05 BR02359
			// Added an overload of GetExtentLayout that takes in the minimum parameter to return 
			// the extend based on the minimum size of the row layout (based on MinimumCellSize 
			// and MinimumLabelSize). Also added bandOrigin for convenience.
			//
			//int extent = this.PreferredRowLayoutSize.Width;
			int extent = ! minimum 
				? this.PreferredRowLayoutSize.Width
				: Math.Max( this.MinimumRowLayoutSize.Width, this.MinimumHeaderLayoutSize.Width );

			// SSP 3/5/05 BR02359
			// Added bandOrigin for convenience.
			//
			if ( BandOrigin.RowCellArea == bandOrigin )
				return extent;

			// Adjust for the row borders.
			// 
			// SSP 6/26/03 UWG2413
			// Don't add the row border widths if the row borders are going to be merged with
			// the cells. Also take into account the row borders being merged with the row selectors.
			// 
			// ------------------------------------------------------------------------------------
			//extent += 2 * this.Layout.GetBorderThickness( this.BorderStyleRowResolved );
			int rowSelectorExtent = this.RowSelectorExtent;
			bool rowBorderMergeable = this.Layout.CanMergeAdjacentBorders( this.BorderStyleRowResolved );
			bool cellBorderMergeable = this.Layout.CanMergeAdjacentBorders( this.BorderStyleCellResolved );
						
			// if the row border is mergeable, then we will merge it with the row selector. So adjust
			// the extent for that.
			//
			if ( rowSelectorExtent > 0 && rowBorderMergeable )
				extent--;

			// Add the row border width if it won't get merged with the cells.
			//
			if ( ! rowBorderMergeable  || ! cellBorderMergeable || this.CellSpacingResolved > 0 )
			{
				extent += 2 * this.Layout.GetBorderThickness( this.BorderStyleRowResolved );
			}
			// ------------------------------------------------------------------------------------

			// Add row selector width since above only calculates the width of the cell area.
			//			
			extent += rowSelectorExtent;

			// SSP 3/5/05 BR02359
			// Added bandOrigin for convenience.
			//
			if ( BandOrigin.RowSelector == bandOrigin )
				return extent;

			extent += this.PreRowAreaExtent;

			return extent;
		}

		#endregion // GetExtentLayout

		#region RecalcCachedLayoutItemRects

		// SSP 7/12/05 BR04916
		// Added RecalcCachedLayoutItemRects.
		// 		
		internal void RecalcCachedLayoutItemRects( )
		{
			this.CalcLayoutHelper( this.GetExtent( BandOrigin.RowSelector ) );
		}

		#endregion // RecalcCachedLayoutItemRects

		#region GetActualHeaderLayoutContainerSize

		// SSP 8/31/06 BR13802
		// 
		internal Size GetActualHeaderLayoutContainerSize( )
		{
			if ( ! this.AreColumnHeadersInSeparateLayoutArea )
				return this.GetActualRowLayoutContainerSize( );

			Size sz = this.PreferredHeaderLayoutSize;
			if ( ! this.CardView )
				sz.Width = this.GetExtent( BandOrigin.RowSelector );
			else
				sz.Width = this.CardLabelWidthResolved;

			return sz;
		}

		#endregion // GetActualHeaderLayoutContainerSize

		#region GetActualHeaderLayoutContainerSize

		// SSP 8/31/06 BR13802
		// 
		internal Size GetActualRowLayoutContainerSize( )
		{
			Size sz = this.PreferredRowLayoutSize;
			if ( ! this.CardView )
				sz.Width = this.GetExtent( BandOrigin.RowSelector );
			else
				sz.Width = this.CardWidthResolved;

			return sz;
		}

		#endregion // GetActualHeaderLayoutContainerSize

		#region CalcLayoutHelper

		private void CalcLayoutHelper( )
		{
			this.CalcLayoutHelper( -1 );
		}

		// SSP 7/12/05 BR04916
		// Added an overload that takes in bandExtent parameter.
		//
		private void CalcLayoutHelper( int bandExtent )
		{
			this.layoutItemDimensionsCellArea = null;
			this.layoutItemDimensionsHeaderArea = null;

			// We are using differnet ILayoutContainer implementations for card view and regular view.
			// Also we are using different ILayoutContainer implementations for the headers and cells.
			//
			if ( ! this.CardView )
			{
				// SSP 7/12/05 BR04916
				// Added an overload that takes in bandExtent parameter. If bandExtent is -1 then
				// use the GetExtentLayout.
				// 
				//int bandExtent = this.GetExtentLayout( );
				if ( bandExtent < 0 )
					bandExtent = this.GetExtentLayout( );

				if ( this.AreColumnHeadersInSeparateLayoutArea )
				{
					// If the headers are separate from the cells, then calcualte the extent of the header area.
					// Following code in addition to calculating the extent of the overall row cell area, it 
					// also calculates the location and size of individual items (cells) in the row cell layout area.
					// 
					Rectangle headerAreaRect = new Rectangle( 0, 0, bandExtent, this.PreferredHeaderLayoutSize.Height );
					LayoutContainerHeaderArea headerAreaContainer = this.GetCachedLayoutContainerHeaderArea( );
					headerAreaContainer.Initialize( this, headerAreaRect );
					this.HeaderLayoutManager.LayoutContainer( headerAreaContainer, null );
					this.layoutItemDimensionsHeaderArea = this.gridBagHeaderLayoutManager.GetLayoutItemDimensions( headerAreaContainer, null );
				}

				// Calculate the extent of the row cell area. Following code in addition to calculating 
				// the extent of the overall row cell area, it also calculates the location and size of 
				// individual items (cells) in the row cell layout area.
				//
				Rectangle cellAreaRect = new Rectangle( 0, 0, bandExtent, this.PreferredRowLayoutSize.Height );
				LayoutContainerCellArea cellAreaContainer = this.GetCachedLayoutContainerCellArea( );
				cellAreaContainer.Initialize( this, cellAreaRect );
				this.RowLayoutManager.LayoutContainer( cellAreaContainer, null );
				this.layoutItemDimensionsCellArea = this.gridBagRowLayoutManager.GetLayoutItemDimensions( cellAreaContainer, null );
			}
			else
			{
				if ( this.AreColumnHeadersInSeparateLayoutArea )
				{
					// If the headers are separate from the cells, then calcualte the extent of the header area.
					// Following code in addition to calculating the extent of the overall row cell area, it 
					// also calculates the location and size of individual items (cells) in the row cell layout area.
					//
					Rectangle headerAreaRect = new Rectangle( 0, 0, this.CardLabelWidthResolved, this.PreferredHeaderLayoutSize.Height );
					LayoutContainerCardLabelArea cardLabelAreaContainer = this.GetCachedLayoutContainerCardLabelArea( );
					cardLabelAreaContainer.Initialize( this, headerAreaRect );
					this.HeaderLayoutManager.LayoutContainer( cardLabelAreaContainer, null );
					this.layoutItemDimensionsHeaderArea = this.gridBagHeaderLayoutManager.GetLayoutItemDimensions( cardLabelAreaContainer, null );
				}

				// Calculate the extent of the row cell area. Following code in addition to calculating 
				// the extent of the overall row cell area, it also calculates the location and size of 
				// individual items (cells) in the row cell layout area.
				//
				Rectangle cellAreaRect = new Rectangle( 0, 0, this.CardWidthResolved, this.PreferredRowLayoutSize.Height );
				LayoutContainerCardCellArea cardCellAreaContainer = this.GetCachedLayoutContainerCardCellArea( );
				cardCellAreaContainer.Initialize( this, cellAreaRect );
				this.RowLayoutManager.LayoutContainer( cardCellAreaContainer, null );
				this.layoutItemDimensionsCellArea = this.gridBagRowLayoutManager.GetLayoutItemDimensions( cardCellAreaContainer, null );
			}

            // MRS - TODO - NAS 9.1 - Groups in RowLayout
            // This section needs to be updated to get an array of all of the columns in every group. 
            // Right now, it will only get columns in the root band (not in a group). It also needs to 
            // get the resolved Origins of those columns and sort them correctly based on those orgins. 

			// Get the array of columns that are visible in the layout. This is usually the columns
			// with Hidden not set to true.
			//
			// MD 1/19/09 - Groups in RowLayout
			// Added a parameter to allow all columns to be obtained.
			//UltraGridColumn[] columnsArr = this.GetLayoutVisibleColumns();
			UltraGridColumn[] columnsArr = this.GetLayoutVisibleColumns( false );

			// Now we need to figure out the tabbing and arrowing orders of the columns. Since
			// in a gridbag layout manager, a cell can end up being positioned anywhere, we need
			// to create an ordered headers collection that we will use to determine in which
			// order tabbing/shift-tabbing (as well as left/right arrowing) will traverse the cells.
			//
			if ( null == this.layoutOrderedVisibleColumnHeaders )
				this.layoutOrderedVisibleColumnHeaders = new HeadersCollection( columnsArr.Length );

			if ( null == this.layoutOrderedVisibleColumnHeadersVertical )
				this.layoutOrderedVisibleColumnHeadersVertical = new HeadersCollection( columnsArr.Length );
			
			this.layoutOrderedVisibleColumnHeaders.InternalClear( );
			this.layoutOrderedVisibleColumnHeadersVertical.InternalClear( );

			int i;

			Infragistics.Win.Utilities.SortMerge( columnsArr, new UltraGridBand.LayoutColumnPositionComparer( true ) );

			for ( i = 0; i < columnsArr.Length; i++ )
			{
				this.layoutOrderedVisibleColumnHeaders.InternalAdd( columnsArr[i].Header );
			}

			// Following does the same for the arrowing down/up.
			//
			Infragistics.Win.Utilities.SortMerge( columnsArr, new UltraGridBand.LayoutColumnPositionComparer( false ) );

			for ( i = 0; i < columnsArr.Length; i++ )
			{
				this.layoutOrderedVisibleColumnHeadersVertical.InternalAdd( columnsArr[i].Header );
			}

            // MRS 2/10/2009 - TFS13728
            // We need to do the same thing for groups.
            // ---------------------------------------------------------------------            
            UltraGridGroup[] groupsArr = this.GetLayoutVisibleGroups(false);

            if (null == this.layoutOrderedVisibleGroupHeaders)
                this.layoutOrderedVisibleGroupHeaders = new HeadersCollection(groupsArr.Length);

            this.layoutOrderedVisibleGroupHeaders.InternalClear();

            Infragistics.Win.Utilities.SortMerge(groupsArr, new UltraGridBand.LayoutGroupPositionComparer(true));

            for (i = 0; i < groupsArr.Length; i++)
            {
                this.layoutOrderedVisibleGroupHeaders.InternalAdd(groupsArr[i].Header);
            }
            // ---------------------------------------------------------------------


			// SSP 11/10/04 - Merged Cell Feature
			// Added SpansEntireLayoutHeight property on the RowLayoutColumnInfo.
			// 
			// ----------------------------------------------------------------------------------
			for ( i = 0; i < this.Columns.Count; i++ )
			{
				UltraGridColumn column = this.Columns[i];
				if ( column.HasRowLayoutColumnInfo )
					column.RowLayoutColumnInfo.InternalSetSpansEntireLayoutHeight( false );
			}

            // MRS - NAS 9.1 - Groups in RowLayout
            for (i = 0; i < this.Groups.Count; i++)
            {
                UltraGridGroup group = this.Groups[i];
                if (group.HasRowLayoutGroupInfo)
                    group.RowLayoutGroupInfo.InternalSetSpansEntireLayoutHeight(false);
            }

			// SSP 9/27/07 BR26350
			// 
			//int layoutSpanY = this.layoutItemDimensionsCellArea.RowDims.Length - 1;
			int layoutSpanY = LayoutUtility.GetLogicalGridBagLayoutRect( this.layoutItemDimensionsCellArea ).Height;

			for ( i = 0; i < columnsArr.Length; i++ )
			{
				RowLayoutColumnInfo ci = columnsArr[i].RowLayoutColumnInfo;

				// SSP 9/27/07 BR26350
				// Moved this above outside the for loop.
				// 
				//int layoutSpanY = this.layoutItemDimensionsCellArea.RowDims.Length - 1;

				ci.InternalSetSpansEntireLayoutHeight( 
					ci.SpanYResolved == layoutSpanY
					 );
			}
			// ----------------------------------------------------------------------------------

			// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout/Card-View
			// This is necessary for the card-view and wrap header text. When the row-layout 
			// cache is verified it may cause the cached card label size to get dirtied in which
			// case we need to force the card label size to be verified here otherwise there is
			// no good trigger point for the verification of card label sizes.
			// 
			// ----------------------------------------------------------------------------------
			for ( i = 0; i < columnsArr.Length; i++ )
			{
				Size size = columnsArr[i].CardLabelSize;
			}
			// ----------------------------------------------------------------------------------

			// SSP 11/10/05 BR07605
			// If row sizing is Auto then we need to recalc the ideal heights of the rows since
			// the widths of the columns could have potentially changed.
			// 
			this.BumpMajorRowHeightVersion( );
		}

		#endregion // CalcLayoutHelper

		#region LayoutOrderedVisibleColumnHeaders
		
		internal HeadersCollection LayoutOrderedVisibleColumnHeaders
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.layoutOrderedVisibleColumnHeaders;
			}
		}

		#endregion // LayoutOrderedVisibleColumnHeaders

		#region LayoutOrderedVisibleColumnHeadersVertical
		
		internal HeadersCollection LayoutOrderedVisibleColumnHeadersVertical
		{
			get
			{
				this.VerifyRowLayoutCache( );

				return this.layoutOrderedVisibleColumnHeadersVertical;
			}
		}

		#endregion // LayoutOrderedVisibleColumnHeadersVertical

		#region GetLayoutRowHeight
		
		// SSP 8/29/05 BR05837
		// Added minimum parameter.
		// 
		//internal int GetLayoutRowHeight( )
		internal int GetLayoutRowHeight( bool minimum )
		{
			this.VerifyRowLayoutCache( );

			// SSP 8/29/05 BR05837
			// Added minimum parameter.
			// 
			//int height = this.PreferredRowLayoutSize.Height;
			int height = minimum ? this.MinimumRowLayoutSize.Height : this.PreferredRowLayoutSize.Height;

			// SSP 6/30/05 - NAS 5.3 Column Chooser
			// If the preferred row layout height is 0 that means all the columns are hidden.
			// Now with the column chooser the end user can hide all the columns. In such a
			// scenario make sure that we fall back to some default font height so the row 
			// selectors, expanstion indicators etc... still show. Also return 0 here can 
			// cause the RowScrollRegion's GetMaxScrollPosition logic to be very slow.
			// 
			if ( 0 == height )
			{
				// SSP 6/18/07 BR23769
				// Honor override's MinRowHeight property.
				// 
				// --------------------------------------------------------------
				//height = this.CalcMinRowHeight( );

				int overrideMinRowHeight = this.OverrideMinRowHeight;
				if ( overrideMinRowHeight > 0 )
					height = overrideMinRowHeight;
				else
					height = this.CalcMinRowHeight( );
				// --------------------------------------------------------------
			}

			// Account for row borders.
			//
			if ( this.CellSpacingResolved > 0 || ! this.Layout.CanMergeAdjacentBorders( this.BorderStyleRowResolved, this.BorderStyleCellResolved ) )
				height += 2 * this.Layout.GetBorderThickness( this.BorderStyleRowResolved );

			return height;
		}

		#endregion // GetLayoutRowHeight

		#region GetLayoutResizeItems

		internal static double ArrSum( double[] arr, int startIndex, int len )
		{
			double sum = 0.0;

			for ( int i = startIndex; null != arr && i < arr.Length && i < startIndex + len; i++ )
				sum += arr[i];

			return sum;
		}

		internal static int ArrSum( int[] arr )
		{
			return ArrSum( arr, 0, arr.Length );
		}

		internal static int ArrSum( int[] arr, int startIndex, int len )
		{
			int sum = 0;

			for ( int i = startIndex; null != arr && i < arr.Length && i < startIndex + len; i++ )
				sum += arr[i];

			return sum;
		}

		internal void GetLayoutResizeItems( 
			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Added the elem parameter and made resizeItem an out parameter. ResizeItem will be
			// determined based on the element.
			// 
			// --------------------------
			//ILayoutItem resizeItem, 
			UIElement elem,
			ref ILayoutItem resizeItem, 
			// --------------------------
			Size containerSize,
			out GridBagLayoutManager gridBagLM, 
			out Size preferredLayoutSize,
			out GridBagLayoutItemDimensionsCollection preferredItemDims,
			out GridBagLayoutItemDimensionsCollection actualItemDims,
			// SSP 8/4/06 BR13795
			// Added recalcPreferredItemDims param.
			// 
			bool recalcPreferredItemDims
			)
		{
			preferredLayoutSize = Size.Empty;
			gridBagLM = null;
			preferredItemDims = null;
			actualItemDims = null;

            // MRS - NAS 9.1 - Groups in RowLayout
			//if ( !( resizeItem is ColumnHeader ) && !( resizeItem is UltraGridColumn )
            if (!(resizeItem is HeaderBase) && !(resizeItem is UltraGridColumn)
				// SSP 5/7/07 BR22392
				// Added the following condition for checking CellLayoutItem.
				// 
				&& !( resizeItem is RowAutoSizeLayoutManagerHolder.CellLayoutItem ) 
                // MRS - NAS 9.1 - Groups in RowLayout
                && !(resizeItem is GroupLayoutItemBase))
			{
				Debug.Assert( false, "Item must be a column or a column header." );
				return;
			}

			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Added the if block and added the existing code into the else block. First try to 
			// get the layout manager from the specified element.
			// 
            ILayoutItem tmpLayoutItem = null != elem ? this.GetLayoutItemFromElem(elem, out gridBagLM, resizeItem) : null;
            if (null != tmpLayoutItem && null != gridBagLM)
            {
                resizeItem = tmpLayoutItem;
                preferredLayoutSize = gridBagLM.CalculatePreferredSize(this.GetCachedLayoutContainerCalcSize(), null);
                preferredItemDims = gridBagLM.GetLayoutItemDimensions(new LayoutContainerCalcSize(preferredLayoutSize), null);
            }
            else
            {
                // MRS - NAS 9.1 - Groups in RowLayout
                //if (resizeItem is ColumnHeader && this.AreColumnHeadersInSeparateLayoutArea)
                if (resizeItem is HeaderBase && this.AreColumnHeadersInSeparateLayoutArea)
                {
                    gridBagLM = this.gridBagHeaderLayoutManager;
                    actualItemDims = preferredItemDims = this.LayoutItemDimensionsHeaderArea;
                    preferredLayoutSize = this.PreferredHeaderLayoutSize;
                }
                else
                {
                    gridBagLM = this.gridBagRowLayoutManager;
                    actualItemDims = preferredItemDims = this.LayoutItemDimensionsCellArea;
                    preferredLayoutSize = this.PreferredRowLayoutSize;
                }
            }

			// SSP 8/4/06 BR13795
			// 
			if ( recalcPreferredItemDims && null != gridBagLM )
			{
				preferredLayoutSize = gridBagLM.CalculatePreferredSize( this.GetCachedLayoutContainerCalcSize( ), null );
				preferredItemDims = gridBagLM.GetLayoutItemDimensions( new LayoutContainerCalcSize( preferredLayoutSize ), null );
				actualItemDims = null;
			}

			if ( 0 == containerSize.Width )
				containerSize.Width = preferredLayoutSize.Width;

			if ( 0 == containerSize.Height )
				containerSize.Height = preferredLayoutSize.Height;

			if ( Math.Abs( containerSize.Width - preferredLayoutSize.Width ) > 10 ||
				Math.Abs( containerSize.Height - preferredLayoutSize.Height ) > 10 
				// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// 
				|| null == actualItemDims )
			{
				actualItemDims = gridBagLM.GetLayoutItemDimensions( new LayoutContainerCalcSize( containerSize ), null );
			}
		}

		#endregion // GetResizeHeaders

		#region GetLayoutItemMaxSize

		// SSP 10/26/04 UWG3729
		// Also calculate the max width and the max height when calculating the layout 
		// item resize range
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private Size GetLayoutItemMaxSize( ILayoutItem item )
		private static Size GetLayoutItemMaxSize( ILayoutItem item )
		{
			Size size = new Size( 0, 0 );
			if ( item is Infragistics.Win.UltraWinGrid.ColumnHeader )
				size = ((Infragistics.Win.UltraWinGrid.ColumnHeader)item).MaximumSize;
			else if ( item is UltraGridColumn )
				size = ((UltraGridColumn)item).MaximumSize;

			return size;
		}

		#endregion // GetLayoutItemMaxSize

		#region GetLayoutItemResizeRange

		internal void GetLayoutItemResizeRange( 
			ILayoutItem resizeItem,
			Size containerSize,
			ref int minWidth, ref int maxWidth,
			ref int minHeight , ref int maxHeight )
		{
			this.GetLayoutItemResizeRange( null, resizeItem, containerSize, 
				ref minWidth, ref maxWidth, ref minHeight , ref maxHeight );
		}
		
		internal void GetLayoutItemResizeRange( 
			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Added the elem parameter.
			// 
			UIElement elem,
			ILayoutItem resizeItem,
			Size containerSize,
			ref int minWidth, ref int maxWidth,
			ref int minHeight , ref int maxHeight )
		{
			Size currSize;
			this.GetLayoutItemResizeRange( elem, resizeItem, containerSize, 
				ref minWidth, ref maxWidth, ref minHeight, ref maxHeight, out currSize );
		}

		internal void GetLayoutItemResizeRange( 
			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Added the elem parameter.
			// 
			UIElement elem,
			ILayoutItem resizeItem,
			Size containerSize,
			ref int minWidth, ref int maxWidth,
			ref int minHeight , ref int maxHeight,
			// SSP 8/29/05 BR05818
			// Added an overload that takes in currSize parameter.
			// 
			out Size currSize )
		{
			// SSP 8/29/05 BR05818
			// Added an overload that takes in currSize parameter.
			// 
			currSize = Size.Empty;

			minWidth = 0;
			maxWidth = 0;
			minHeight = 0;
			maxHeight = 0;

			GridBagLayoutItemDimensionsCollection prefItemDims = null;
			GridBagLayoutItemDimensionsCollection actualItemDims = null;
			GridBagLayoutManager gblm = null;
			Size prefSize = Size.Empty;
			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Pass along the new elem paramter.
			// 
			//this.GetLayoutResizeItems( resizeItem, containerSize, out gblm, out prefSize, out prefItemDims, out actualItemDims );
			this.GetLayoutResizeItems( elem, ref resizeItem, containerSize, out gblm, out prefSize, out prefItemDims, out actualItemDims
				// SSP 8/4/06 BR13795
				// Added recalcPreferredItemDims param.
				// 
				, false
				);

			if ( null == gblm || null == prefItemDims || null == actualItemDims 
				// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// Check for resizeItem being null.
				// 
				|| null == resizeItem
				|| ! actualItemDims.Exists( resizeItem ) )
				return;

			GridBagLayoutItemDimensionsCollection itemDims = actualItemDims;

			GridBagLayoutItemDimensions gbResizeItemDim = itemDims[ resizeItem ];
			int resizeOriginX = gbResizeItemDim.OriginX;
			int resizeSpanX = gbResizeItemDim.SpanX;
			int resizeOriginY = gbResizeItemDim.OriginY;
			int resizeSpanY = gbResizeItemDim.SpanY;
			int minX = gbResizeItemDim.Bounds.Left + resizeItem.MinimumSize.Width;
			int minY = gbResizeItemDim.Bounds.Top + resizeItem.MinimumSize.Height;

			// SSP 8/29/05 BR05818
			// Added an overload that takes in currSize parameter.
			// 
			currSize = gbResizeItemDim.Bounds.Size;

			minWidth = resizeItem.MinimumSize.Width;
			minHeight = resizeItem.MinimumSize.Height;

			// SSP 10/26/04 UWG3729
			// Also calculate the max width and the max height when calculating the layout 
			// item resize range.
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//Size resizeItemMaxSize = this.GetLayoutItemMaxSize( resizeItem );
			Size resizeItemMaxSize = UltraGridBand.GetLayoutItemMaxSize( resizeItem );

			int maxX = resizeItemMaxSize.Width > 0 ? gbResizeItemDim.Bounds.Left + resizeItemMaxSize.Width : int.MaxValue;
			int maxY = resizeItemMaxSize.Height > 0 ? gbResizeItemDim.Bounds.Top + resizeItemMaxSize.Height : int.MaxValue;

			for ( int i = 0; i < gblm.LayoutItems.Count; i++ )
			{
				ILayoutItem item = gblm.LayoutItems[i];

				// Item can be a column header or a column both of which implement IGridBagConstraint.
				//
				GridBagLayoutItemDimensions gbItemDim = itemDims.Exists( item ) ? itemDims[ item ] : null;

				// Debug.Assert( null != gbItemDim );
				if ( null == gbItemDim )
					continue;

				int originX = gbItemDim.OriginX;
				int spanX = gbItemDim.SpanX;
				int originY = gbItemDim.OriginY;
				int spanY = gbItemDim.SpanY;

				// SSP 12/1/03 UWG2641
				// Added a way for the user to specify cell and label insets.
				// Here we need to take into account the insets. Commented out the original
				// code and added new code. This change simply takes into account the insets
				// when calculating the resize range.
				//
				// ----------------------------------------------------------------------------
				
				// Don't take into account insets for the resize item itself.
				//
				Insets insets = RowLayoutColumnInfo.GetItemInsetsHelper( item );

				// SSP 10/26/04 UWG3729
				// Also calculate the max width and the max height when calculating the layout 
				// item resize range.
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//Size maxSize = this.GetLayoutItemMaxSize( item );
				Size maxSize = UltraGridBand.GetLayoutItemMaxSize( item );

				if ( originX + spanX == resizeOriginX + resizeSpanX )
				{
					minX = Math.Max( minX, gbItemDim.Bounds.Left + item.MinimumSize.Width + insets.Right );

					// SSP 10/26/04 UWG3729
					// Also calculate the max width and the max height when calculating the layout 
					// item resize range.
					//
					if ( maxSize.Width > 0 )
						maxX = Math.Min( maxX, gbItemDim.Bounds.Left + maxSize.Width + insets.Right );
				}
				else if ( originX + spanX < resizeOriginX + resizeSpanX && originX + spanX > resizeOriginX )
				{
					minX = Math.Max( minX, gbItemDim.Bounds.Right + insets.Right );
				}

				if ( originY + spanY == resizeOriginY + resizeSpanY )
				{
					minY = Math.Max( minY, gbItemDim.Bounds.Top + item.MinimumSize.Height + insets.Bottom );

					// SSP 10/26/04 UWG3729
					// Also calculate the max width and the max height when calculating the layout 
					// item resize range.
					//
					if ( maxSize.Height > 0 )
						maxY = Math.Min( maxY, gbItemDim.Bounds.Top + maxSize.Height + insets.Bottom );
				}
				else if ( originY + spanY < resizeOriginY + resizeSpanY && originY + spanY > resizeOriginY )
				{
					minY = Math.Max( minY, gbItemDim.Bounds.Bottom + insets.Bottom );
				}
				// ----------------------------------------------------------------------------
			}

			// SSP 12/1/03 UWG2641
			// Added a way for the user to specify cell and label insets.
			// Here we need to take into account the insets. Commented out the original
			// code and added new code. This change simply takes into account the insets
			// when calculating the resize range.
			//
			// ----------------------------------------------------------------------------
			//minWidth = Math.Max( 0, minX - gbResizeItemDim.Bounds.Left );
			//minHeight = Math.Max( 0, minY - gbResizeItemDim.Bounds.Top );
			Insets tmpInsets = RowLayoutColumnInfo.GetItemInsetsHelper( resizeItem );
			minWidth = Math.Max( 0, minX - gbResizeItemDim.Bounds.Left - tmpInsets.Right );
			minHeight = Math.Max( 0, minY - gbResizeItemDim.Bounds.Top - tmpInsets.Bottom );
			// ----------------------------------------------------------------------------

			// SSP 10/26/04 UWG3729
			// Also calculate the max width and the max height when calculating the layout 
			// item resize range.
			//
			// ----------------------------------------------------------------------------
			if ( maxX != int.MaxValue )
				maxWidth = maxX - gbResizeItemDim.Bounds.Left - tmpInsets.Right;
			if ( maxY != int.MaxValue )
				maxHeight = maxY - gbResizeItemDim.Bounds.Top - tmpInsets.Bottom;
			// ----------------------------------------------------------------------------
		}

		#endregion // GetLayoutItemResizeRange

		#region GetLayoutItemPreferredSize

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal Size GetLayoutItemPreferredSize( ILayoutItem item )
		internal static Size GetLayoutItemPreferredSize( ILayoutItem item )
		{
			return item.PreferredSize;
		}

		#endregion // GetLayoutItemPreferredSize

		#region SetLayoutItemPreferredSize

		// SSP 12/1/03
		// Added modifyWidth, and modifyHeight parameters.
		//
		//internal void SetLayoutItemPreferredSize( ILayoutItem item, Size size )
		internal void SetLayoutItemPreferredSize( ILayoutItem item, Size size, bool modifyWidth, bool modifyHeight )
		{
			this.SetLayoutItemPreferredSize( item, size, modifyWidth, modifyHeight, false );
		}

		// SSP 8/30/06 BR13795
		// Added an overload with onlySetAutoFitAllColumnsOverrideSize.
		// 
		internal void SetLayoutItemPreferredSize( ILayoutItem item, Size size, bool modifyWidth, bool modifyHeight, bool onlySetAutoFitAllColumnsOverrideSize )
		{
			Infragistics.Win.UltraWinGrid.UltraGridColumn column = null;

            // MRS - NAS 9.1 - Groups in RowLayout
			//if ( item is ColumnHeader )
            if (item is HeaderBase)
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//column = ((ColumnHeader)item).Column;
                RowLayoutColumnInfo rowLayoutColumnInfo = ((HeaderBase)item).RowLayoutColumnInfo;
				
				// SSP 12/1/03
				// Added modifyWidth, and modifyHeight parameters.
				//
				// --------------------------------------------------------------------------
				if ( ! modifyWidth )
                    // MRS - NAS 9.1 - Groups in RowLayout
					//size.Width = column.RowLayoutColumnInfo.PreferredLabelSize.Width;
                    size.Width = rowLayoutColumnInfo.PreferredLabelSize.Width;
				if ( ! modifyHeight )
                    // MRS - NAS 9.1 - Groups in RowLayout
					//size.Height = column.RowLayoutColumnInfo.PreferredLabelSize.Height;
                    size.Height = rowLayoutColumnInfo.PreferredLabelSize.Height;
				// --------------------------------------------------------------------------

				// SSP 8/30/06 BR13795
				// Added an overload with onlySetAutoFitAllColumnsOverrideSize.
				// 
				//column.RowLayoutColumnInfo.PreferredLabelSize = size;
				if ( onlySetAutoFitAllColumnsOverrideSize )
                    // MRS - NAS 9.1 - Groups in RowLayout
					//column.RowLayoutColumnInfo.autoFitAllColumns_preferredLabelSizeOverride = size;
                    rowLayoutColumnInfo.autoFitAllColumns_preferredLabelSizeOverride = size;
				else
                    // MRS - NAS 9.1 - Groups in RowLayout
					//column.RowLayoutColumnInfo.PreferredLabelSize = size;
                    rowLayoutColumnInfo.PreferredLabelSize = size;

				// SSP 7/23/03 - UWG2376
				// If the column headers are in separate area then the cells, then we have
				// to resize the cell with the header because we use the max preferred 
				// width/height of to layout cells and headers in their separate layouts.
				// 
				if ( this.AreColumnHeadersInSeparateLayoutArea )
				{
                    // MRS - NAS 9.1 - Groups in RowLayout
					//Size cellSize = column.RowLayoutColumnInfo.PreferredCellSize;
                    Size cellSize = rowLayoutColumnInfo.PreferredCellSize;

					if ( this.CardView )
					{
						// SSP 12/1/03
						// Added modifyWidth, and modifyHeight parameters.
						//
						//if ( cellSize.Height > size.Height )
						if ( modifyHeight && cellSize.Height > size.Height )
							cellSize.Height = size.Height;
					}
					else
					{
						// SSP 12/1/03
						// Added modifyWidth, and modifyHeight parameters.
						//
						//if ( cellSize.Width > size.Width )
						if ( modifyWidth && cellSize.Width > size.Width )
							cellSize.Width = size.Width;
					}

					// SSP 8/30/06 BR13795
					// Added an overload with onlySetAutoFitAllColumnsOverrideSize.
					// 
					//column.RowLayoutColumnInfo.PreferredCellSize = cellSize;
					if ( onlySetAutoFitAllColumnsOverrideSize )
                        // MRS - NAS 9.1 - Groups in RowLayout
						//column.RowLayoutColumnInfo.autoFitAllColumns_preferredCellSizeOverride = cellSize;
                        rowLayoutColumnInfo.autoFitAllColumns_preferredCellSizeOverride = cellSize;
					else
                        // MRS - NAS 9.1 - Groups in RowLayout
						//column.RowLayoutColumnInfo.PreferredCellSize = cellSize;
                        rowLayoutColumnInfo.PreferredCellSize = cellSize;
				}
			}
				// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// 
				//else 
			else if ( item is UltraGridColumn )
			{
				column = ((UltraGridColumn)item);
				
				// SSP 12/1/03
				// Added modifyWidth, and modifyHeight parameters.
				//
				// --------------------------------------------------------------------------
				if ( ! modifyWidth )
					size.Width = column.RowLayoutColumnInfo.PreferredCellSize.Width;
				if ( ! modifyHeight )
					size.Height = column.RowLayoutColumnInfo.PreferredCellSize.Height;
				// --------------------------------------------------------------------------

				// SSP 8/30/06 BR13795
				// Added an overload with onlySetAutoFitAllColumnsOverrideSize.
				// 
				//column.RowLayoutColumnInfo.PreferredCellSize = size;
				if ( onlySetAutoFitAllColumnsOverrideSize )
					column.RowLayoutColumnInfo.autoFitAllColumns_preferredCellSizeOverride = size;
				else 
					column.RowLayoutColumnInfo.PreferredCellSize = size;

				// SSP 7/23/03 - UWG2376
				// If the column headers are in separate area then the cells, then we have
				// to resize the cell with the header because we use the max preferred 
				// width/height of to layout cells and headers in their separate layouts.
				// 
				if ( this.AreColumnHeadersInSeparateLayoutArea )
				{
					Size headerSize = column.RowLayoutColumnInfo.PreferredLabelSize;

					if ( this.CardView )
					{
						// SSP 12/1/03
						// Added modifyWidth, and modifyHeight parameters.
						//
						//if ( headerSize.Height > size.Height )
						if ( modifyHeight && headerSize.Height > size.Height )
							headerSize.Height = size.Height;
					}
					else
					{
						// SSP 12/1/03
						// Added modifyWidth, and modifyHeight parameters.
						//
						//if ( headerSize.Width > size.Width )
						if ( modifyWidth && headerSize.Width > size.Width )
							headerSize.Width = size.Width;
					}

					// SSP 8/30/06 BR13795
					// Added an overload with onlySetAutoFitAllColumnsOverrideSize.
					// 
					if ( onlySetAutoFitAllColumnsOverrideSize )
						column.RowLayoutColumnInfo.autoFitAllColumns_preferredLabelSizeOverride = headerSize;
					else 
						column.RowLayoutColumnInfo.PreferredLabelSize = headerSize;
				}
			}
			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// 
			// --------------------------------------------------------------------------------
			else if ( item is RowAutoSizeLayoutManagerHolder.CellLayoutItem )
			{
				RowAutoSizeLayoutManagerHolder.CellLayoutItem cellLayoutItem = (RowAutoSizeLayoutManagerHolder.CellLayoutItem)item;
				Size tmpSize;

				if ( modifyHeight )
				{
					tmpSize = cellLayoutItem.PreferredSizeOverride;
					tmpSize.Height = size.Height;
					cellLayoutItem.PreferredSizeOverride = tmpSize;
				}				

				// When resizing Width always set it on the column's RowLayoutColumnInfo since 
				// cell widths shouldn't be different from row to row.
				// 
				if ( modifyWidth )
				{
					tmpSize = cellLayoutItem.Column.RowLayoutColumnInfo.PreferredCellSize;
					tmpSize.Width = size.Width;
					cellLayoutItem.Column.RowLayoutColumnInfo.PreferredCellSize = tmpSize;
				}
			}
			// --------------------------------------------------------------------------------
		}

		#endregion // SetLayoutItemPreferredSize

		#region GetLayoutItemFromElem

		// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// 
		internal ILayoutItem GetLayoutItemFromElem( UIElement elem, out GridBagLayoutManager gridBagLayoutManager, ILayoutItem layoutItem )
		{
			gridBagLayoutManager = null;
			LayoutManagerBase layoutManager = null;

			if ( null == layoutManager )
			{
				RowCellAreaUIElementBase rowElem = (RowCellAreaUIElementBase)elem.GetAncestor( typeof( RowCellAreaUIElementBase ) );
				UltraGridRow row = null != rowElem ? rowElem.Row : null;
				if ( null != row )
				{
					// SSP 8/29/05 BR05818
					// When RowSizing is Free where rows can be resized independent of each other,
					// make sure that the row uses it's own layout manager for resize purposes.
					// To do that simply force allocation of RowAutoSizeLayoutManagerHolder.
					// 
					if ( RowSizing.Sychronized != row.Band.RowSizingResolved )
					{
						RowAutoSizeLayoutManagerHolder lh = row.RowAutoSizeLayoutManagerHolder;
					}

					layoutManager = row.LayoutManager;
				}
			}

			if ( null == layoutManager )
			{
				BandHeadersUIElement headersElem = (BandHeadersUIElement)elem.GetAncestor( typeof( BandHeadersUIElement ) );
				if ( null != headersElem && null != headersElem.Band )
					layoutManager = headersElem.Band.HeaderLayoutManager;
			}

			if ( null == layoutManager )
			{
				SummaryFooterUIElement summaryFooterElem = (SummaryFooterUIElement)elem.GetAncestor( typeof( SummaryFooterUIElement ) );
				if ( null != summaryFooterElem )
					layoutManager = null != summaryFooterElem.SummaryDisplayAreaContext 
						? this.GetSummaryLayoutManager( summaryFooterElem.SummaryDisplayAreaContext )
						: this.SummaryLayoutManager;
			}

			layoutItem = null != layoutManager ? LayoutUtility.GetAssociatedLayoutItem( layoutManager, elem, layoutItem ) : null;
			gridBagLayoutManager = layoutManager as GridBagLayoutManager;
			return layoutItem;
		}

		#endregion // GetLayoutItemFromElem

		#region ResizeLayoutItem

		// SSP 8/4/06 BR13795
		// Added SetAutoFitColumnsPreferredSizeOverrides method.
		// 
		private void SetAutoFitColumnsPreferredSizeOverrides( GridBagLayoutManager gblm, 
			GridBagLayoutItemDimensionsCollection itemDims, ILayoutItem exception )
		{
			foreach ( ILayoutItem item in gblm.LayoutItems )
			{
				if ( item == exception )
					continue;

				GridBagLayoutItemDimensions gbItemDim = itemDims.Exists( item ) ? itemDims[ item ] : null;
				UltraGridColumn col = RowLayoutColumnInfo.GetColumnFromLayoutItem( item );
				RowLayoutColumnInfo ci = null != col ? col.RowLayoutColumnInfo : null;
				if ( null != ci && null != gbItemDim )
					this.SetLayoutItemPreferredSize( item, gbItemDim.Bounds.Size, true, false, true );
			}
		}
		
		internal void ResizeLayoutItem( ILayoutItem resizeItem, Size containerSize, Point delta )
		{
			this.ResizeLayoutItem( null, resizeItem, containerSize, delta );
		}
	
		// SSP 8/4/06 BR13795
		// Added ResizeLayoutItemHelper.
		// 
		internal void ResizeLayoutItemHelper( UIElement elem, ILayoutItem resizeItem, Size containerSize, 
			Size itemTargetSize, Point delta, bool raiseResizeNotification, bool modifyWidthDimension, bool modifyHeightDimension )
		{
			if ( ! modifyHeightDimension && ! modifyWidthDimension )
				return;

			GridBagLayoutItemDimensionsCollection prefItemDims = null;
			GridBagLayoutItemDimensionsCollection actualItemDims = null;
			GridBagLayoutManager gblm = null;
			Size prefSize = Size.Empty;
			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Added elem parameter and took out the resizeItem parameter. Pass along the new elem paramter.
			// 
			//this.GetLayoutResizeItems( resizeItem, containerSize, out gblm, out prefSize, out prefItemDims, out actualItemDims );
			this.GetLayoutResizeItems( elem, ref resizeItem, containerSize, out gblm, out prefSize, out prefItemDims, out actualItemDims 
				// SSP 8/4/06 BR13795
				// Added recalcPreferredItemDims param.
				// 
				, false
				);

			if ( null == gblm || null == prefItemDims || null == actualItemDims 
				// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// Check for resizeItem.
				// 
				|| null == resizeItem
				|| ! actualItemDims.Exists( resizeItem ) )
				return;

			GridBagLayoutItemDimensions gbResizeItemDim = actualItemDims[ resizeItem ];
			int resizeOriginX = gbResizeItemDim.OriginX;
			int resizeSpanX = gbResizeItemDim.SpanX;
			int resizeOriginY = gbResizeItemDim.OriginY;
			int resizeSpanY = gbResizeItemDim.SpanY;

			// SSP 6/25/03 UWG2413
			// Fire the BeforeColPosChanged event.
			//
			// ------------------------------------------------------------------------
			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// Get the current size from the gbResizeItemDim instead of column's CellSizeResolved
			// or header's SizeResolved.
			// 
			Size oldSize = gbResizeItemDim.Bounds.Size;

			if ( itemTargetSize.IsEmpty )
			{
				itemTargetSize.Width = oldSize.Width + delta.X;
				itemTargetSize.Height = oldSize.Height + delta.Y;
			}

			if ( ! modifyWidthDimension )
				itemTargetSize.Width = oldSize.Width;

			if ( ! modifyHeightDimension )
				itemTargetSize.Height = oldSize.Height;

			

			UltraGridLayout layout = this.Layout;
			UltraGrid grid = layout.Grid as UltraGrid;
			if ( null != grid && raiseResizeNotification )
			{
				BeforeRowLayoutItemResizedEventArgs e = new BeforeRowLayoutItemResizedEventArgs(
					resizeItem, oldSize, itemTargetSize );

				grid.FireEvent( GridEventIds.BeforeRowLayoutItemResized, e );

				// If the user cancels the BeforeColPosChanged event, then don't 
				// continue with resizing the item.
				//
				if ( e.Cancel )
					return;

				// If the user changes the NewSize in the event handler, then make use of that.
				// 
				itemTargetSize = e.NewSize;
			}
			// ------------------------------------------------------------------------

			// SSP 8/6/03 UWG2568
			// When we are resizing all the columns, we don't want to recalculate the row layout
			// until the operation of resizing all the columns has finished. To accomplish that
			// added dontVerifyRowLayoutCache flag.
			//
			UltraGridBase gridBase = layout.Grid;
			bool origDesignerNotificationsSuppressed = null != gridBase && gridBase.DesignerChangeNotificationsDisabled;
			if ( null != gridBase )
				gridBase.DesignerChangeNotificationsDisabled = true;
			this.dontVerifyRowLayoutCache = true;
			try
			{
				// SSP 8/4/06 BR13795
				// Enclosed the existing code into the for block. Existing code is marked as such.
				// 
				Size targetSize = itemTargetSize;
				bool autoFitAllColumns = layout.AutoFitAllColumns;
				int MAX_PASSES = modifyWidthDimension && autoFitAllColumns ? 20 : 1;
				int deltaWidth = modifyWidthDimension ? targetSize.Width - gbResizeItemDim.Bounds.Width : 0;
				int deltaHeight = modifyHeightDimension ? targetSize.Height - gbResizeItemDim.Bounds.Height : 0;
				for ( int pass = 0; pass < MAX_PASSES; pass++ )
				{
					GridBagLayoutItemDimensionsCollection itemDims = actualItemDims;

					if ( pass > 0 )
					{
						gblm.InvalidateLayout( );
						this.GetLayoutResizeItems( elem, ref resizeItem, containerSize, out gblm, out prefSize, out prefItemDims, out actualItemDims, true );

						if ( autoFitAllColumns )
						{
							this.SetAutoFitColumnsPreferredSizeOverrides( gblm, actualItemDims, null );
							gblm.InvalidateLayout( );
							this.GetLayoutResizeItems( elem, ref resizeItem, containerSize, out gblm, out prefSize, out prefItemDims, out actualItemDims, true );
						}

						gbResizeItemDim = actualItemDims[ resizeItem ];

						if ( targetSize.Width == gbResizeItemDim.Bounds.Width || 1 + pass == MAX_PASSES )
							break;

						int newDeltaWidth = targetSize.Width - gbResizeItemDim.Bounds.Width;

						
						if ( newDeltaWidth == deltaWidth )
							newDeltaWidth = deltaWidth * 2;

						deltaWidth = modifyWidthDimension ? newDeltaWidth : 0;
						deltaHeight = modifyHeightDimension ? targetSize.Height - gbResizeItemDim.Bounds.Height : 0;
					}

					// SSP 8/4/06 BR13795
					// Existing code.
					// 
					// --------------------------------------------------------------------------------
					for ( int i = 0; i < gblm.LayoutItems.Count; i++ )
					{
						ILayoutItem item = gblm.LayoutItems[i];

						// Item can be a column header or a column both of which implement IGridBagConstraint.
						//
						GridBagLayoutItemDimensions gbItemDim = itemDims.Exists( item ) ? itemDims[ item ] : null;

						//Debug.Assert( null != gbItemDim );
						if ( null == gbItemDim )
							continue;

						int originX = gbItemDim.OriginX;
						int spanX = gbItemDim.SpanX;
						int originY = gbItemDim.OriginY;
						int spanY = gbItemDim.SpanY;

						float factorX = 1.0f;
						float factorY = 1.0f;

						// SSP 12/1/03 UWG2641
						// Due to insets, the row dims and the bounds of the item will be different. So use
						// the bounds off the item dims collection rather than simply the sum of widths
						// the item spans.
						//
						// ----------------------------------------------------------------------------------
						//int preferredWidth = UltraGridBand.ArrSum( gblm.ColumnWidths, originX, spanX );
						//int preferredHeight = UltraGridBand.ArrSum( gblm.RowHeights, originY, spanY );
						int preferredWidth = prefItemDims[ item ].Bounds.Width;
						int preferredHeight = prefItemDims[ item ].Bounds.Height;
						// ----------------------------------------------------------------------------------

						int actualWidth = gbItemDim.Bounds.Width;
						int actualHeight = gbItemDim.Bounds.Height;

						if ( preferredWidth != actualWidth )
							factorX = (float)preferredWidth / actualWidth;
						if ( preferredHeight != actualHeight )
							factorY = (float)preferredHeight / actualHeight;

						// SSP 12/1/03
						// If only the width is resized, then change only the widths of the preferred sizes
						// and likewise with the height.
						//
						bool modifyWidth = 0 != deltaWidth && originX + spanX == resizeOriginX + resizeSpanX;
						bool modifyHeight = 0 != deltaHeight && originY + spanY == resizeOriginY + resizeSpanY;

						if ( modifyWidth || modifyHeight )
						{
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//Size preferredSize = this.GetLayoutItemPreferredSize( item );
							Size preferredSize = UltraGridBand.GetLayoutItemPreferredSize( item );

							Size minimumSize = item.MinimumSize;							

							if ( modifyWidth )
							{
								if ( deltaWidth < 0 )
									preferredSize.Width = Math.Min( preferredSize.Width, (int)( preferredWidth + deltaWidth * factorX ) );
								else
									preferredSize.Width = Math.Max( preferredSize.Width, (int)( preferredWidth + deltaWidth * factorX ) );

								if ( preferredSize.Width < minimumSize.Width )
									preferredSize.Width = minimumSize.Width;
							}

							if ( modifyHeight )
							{
								if ( deltaHeight < 0 )
									preferredSize.Height = Math.Min( preferredSize.Height, (int)( preferredHeight + deltaHeight * factorY ) );
								else
									preferredSize.Height = Math.Max( preferredSize.Height, (int)( preferredHeight + deltaHeight * factorY ) );

								if ( preferredSize.Height < minimumSize.Height )
									preferredSize.Height = minimumSize.Height;
							}

							// SSP 12/1/03
							// Added modifyWidth, and modifyHeight parameters to SetLayoutItemPreferredSize method.
							//
							//this.SetLayoutItemPreferredSize( item, preferredSize );
							this.SetLayoutItemPreferredSize( item, preferredSize, modifyWidth, modifyHeight );
						}
					}
					// --------------------------------------------------------------------------------
				}

				// SSP 8/2/05
				// If LabelWidth is set then that's what gets used as the width of the label area.
				// What this means is that when a header is resized, the width of the label area
				// remains the same. We need to change the label width as well. The same applies to
				// row height as well.
				// 
				// --------------------------------------------------------------------------------------
				Size oldPrefLayoutSize = prefSize;
				Size newPrefLayoutSize = gblm.CalculatePreferredSize( this.GetCachedLayoutContainerCalcSize( ), null );
				if ( this.CardView )
				{
					if ( this.CardSettings.LabelWidth > 0 && this.CardSettings.AllowLabelSizing )
						this.CardSettings.LabelWidth += newPrefLayoutSize.Width - oldPrefLayoutSize.Width;
				}
				else if ( modifyHeightDimension && null != elem )
				{
					RowCellAreaUIElementBase rowElem = (RowCellAreaUIElementBase)elem.GetAncestor( typeof( RowCellAreaUIElementBase ) );
					UltraGridRow row = null != rowElem ? rowElem.Row : null;
					if ( null != row )
					{
						RowSizing rowSizing = this.RowSizingResolved;
						if ( RowSizing.Sychronized != rowSizing )
						{
							if ( ! row.heightSetManually )
								row.InternalDirtyRowHeight( );
							else if ( RowSizing.Fixed != rowSizing )
								row.Height += newPrefLayoutSize.Height - oldPrefLayoutSize.Height;
						}
					}
				}
				// --------------------------------------------------------------------------------------
			}
			finally
			{
				// Reset the flag.
				//
				this.dontVerifyRowLayoutCache = false;

				if ( null != gridBase )
					gridBase.DesignerChangeNotificationsDisabled = origDesignerNotificationsSuppressed;
			}

			// SSP 6/25/03 UWG2413
			// Fire the AfterColPosChanged event.
			//
			// ------------------------------------------------------------------------
			if ( null != grid && raiseResizeNotification )
			{
				grid.FireEvent( GridEventIds.AfterRowLayoutItemResized,
					new AfterRowLayoutItemResizedEventArgs( resizeItem ) );
			}
			// ------------------------------------------------------------------------
		}

		// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// Added the elem parameter.
		// 
		//internal void ResizeLayoutItem( ILayoutItem resizeItem, Size containerSize, Point delta )
		internal void ResizeLayoutItem( UIElement elem, ILayoutItem resizeItem, Size containerSize, Point delta )
		{
			this.ResizeLayoutItemHelper( elem, resizeItem, containerSize, Size.Empty, delta, true, 0 != delta.X, 0 != delta.Y );
		}

		
		
		
		
		

		#endregion // ResizeLayoutItem

		#region AutoResizeLayoutItem
		
		// SSP 8/31/06 BR13795 BR15249
		// Added containerSize and elem params.
		// 
		//internal void AutoResizeLayoutItem( ILayoutItem autoResizeItem, RowsCollection rowsContext )
		internal void AutoResizeLayoutItem( ILayoutItem autoResizeItem, RowsCollection rowsContext, 
			Size containerSize, UIElement elem )
		{
			if ( null == autoResizeItem || ! autoResizeItem.IsVisible )
			{
				Debug.Assert( false, "Invalid resize layout item !" );
				return;
			}

			UltraGridColumn resizeColumn = null;

			if ( autoResizeItem is UltraGridColumn )
				resizeColumn = (UltraGridColumn)autoResizeItem;
			else if ( autoResizeItem is ColumnHeader )
				resizeColumn = ((ColumnHeader)autoResizeItem).Column;

			if ( null == resizeColumn )
			{
				Debug.Assert( false, "Unknown type of layout item !" );
				return;
			}

			// Verify the row layout cache just in case it's dirty.
			//
			this.VerifyRowLayoutCache( );

			// MRS 8/30/06 - BR15249
			// Keep track of whether anything was actually resized so we know to fire the AfterRowLayoutItemResized event.
			bool resized = false;

			// Don't cause the verification of the row layout cache while we are performing the 
			// operation.
			//
			UltraGridBase gridBase = this.Layout.Grid;
			bool origDesignerNotificationsSuppressed = null != gridBase && gridBase.DesignerChangeNotificationsDisabled;
			if ( null != gridBase )
				gridBase.DesignerChangeNotificationsDisabled = true;
			this.dontVerifyRowLayoutCache = true;
			try
			{
				for ( int c = 0; c < 2; c++ )
				{
					// processingCellArea indicates whether we are currently processing items in the row-cell area
					// or the header area.
					//
					bool processingHeaderArea = 1 == c;

					// If the headers and cells are togather (WithCellData), then don't process the 
					// header area layout manager because that's would not contain any valid information
					// since we don't have any header area.
					//
					if ( processingHeaderArea && ! this.AreColumnHeadersInSeparateLayoutArea )
						continue;

					LayoutManagerBase lm = ! processingHeaderArea ? this.RowLayoutManager : this.HeaderLayoutManager;
					GridBagLayoutItemDimensionsCollection dims = ! processingHeaderArea ? this.LayoutItemDimensionsCellArea : this.LayoutItemDimensionsHeaderArea;

					Debug.Assert( null != dims, "Null dims !" );
					if ( null == dims )
						continue;

					ILayoutItem resizeItem;

					if ( this.AreColumnHeadersInSeparateLayoutArea )
					{
						if ( processingHeaderArea )
							resizeItem = resizeColumn.Header;							
						else 
							resizeItem = resizeColumn;
					}
					else
					{
						resizeItem = autoResizeItem;
					}

					// If the resize item is not in this layout area then continue.
					//
					if ( ! dims.Exists( resizeItem ) )
						continue;

					// SSP 8/31/06 BR13795
					// 
					int maxRight = int.MinValue;
					GridBagLayoutItemDimensions resizeItemDim = dims[ resizeItem ];

					for ( int i = 0; i < lm.LayoutItems.Count; i++ )
					{
						ILayoutItem item = lm.LayoutItems[i];

						// If the item is not visible, then skip it.
						//
						if ( null == item || ! dims.Exists( item ) || ! item.IsVisible )
							continue;

						// If the item is right-aligned with the resize item, then auto-resize that item
						// as well.
						//
						GridBagLayoutItemDimensions itemDim = dims[ item ];
						if ( resizeItemDim.OriginX + resizeItemDim.SpanX == itemDim.OriginX + itemDim.SpanX )
						{
							UltraGridColumn column = item is UltraGridColumn ? (UltraGridColumn)item 
								: ( item is ColumnHeader ? ((ColumnHeader)item).Column : null );

							if ( null != column )
							{
								// SSP 8/16/05 BR05387
								// If LabelPosition is LabelOnly then don't include cells in auto-resize calculations. 
								// Likewise if the LabelPosition is None then don't include header in the auto-resize
								// calculations.
								// 
								// --------------------------------------------------------------------------------------
								if ( item is UltraGridColumn && ! column.RowLayoutColumnInfo.ShouldPositionCellElement )
									continue;

								if ( item is HeaderBase && ! column.RowLayoutColumnInfo.ShouldPositionLabelElement )
									continue;
								// --------------------------------------------------------------------------------------

								bool includeCells = item is UltraGridColumn;
								bool includeHeader = item is ColumnHeader;
							
								int width = -1;
								
								ColumnAutoSizeMode autoSizeMode = resizeColumn.AutoSizeModeResolved;

								if ( ColumnAutoSizeMode.SiblingRowsOnly == autoSizeMode && null != rowsContext )
									// 0 for nRows means all rows.
									//
									width = column.PerformAutoResizeHelper( rowsContext, 0, false, includeHeader, includeCells );
								else if ( ColumnAutoSizeMode.AllRowsInBand == autoSizeMode )
									// 0 for nRows means all rows.
									//
									width = column.PerformAutoResizeHelper( null, 0, false, includeHeader, includeCells );
								else if ( ColumnAutoSizeMode.None != autoSizeMode )
									// -1 as the nRows means visible rows only.
									//
									width = column.PerformAutoResizeHelper( null, -1, false, includeHeader, includeCells );

								if ( width > 0 )
								{
									// SSP 8/31/06 BR13795
									// 
									maxRight = Math.Max( maxRight, itemDim.Bounds.Left + width );

									if ( item is ColumnHeader )
									{
										column.RowLayoutColumnInfo.PreferredLabelSize = new Size( width,
											column.RowLayoutColumnInfo.PreferredLabelSize.Height );
									}
									else if ( item is UltraGridColumn )
									{
										column.RowLayoutColumnInfo.PreferredCellSize = new Size( width,
											column.RowLayoutColumnInfo.PreferredCellSize.Height );
									}
									else
									{
										Debug.Assert( false, "Unknown type of layout item encountered." );
									}

									// MRS 8/30/06 - BR15249
									resized = true;
								}
							}
						}
					}

					// SSP 8/31/06 BR13795
					// Note that this fix is not specifically for BR13795 or BR15249. However this makes the auto-resizing
					// behavior 'more correct' when auto-fit columns are enabled. This change is not necessary at all for
					// BR13795.
					// 
					if ( resized && maxRight > int.MinValue && this.Layout.AutoFitAllColumns )
					{
						Size newSize = new Size( maxRight - resizeItemDim.Bounds.Left, resizeItemDim.Bounds.Height );
						this.ResizeLayoutItemHelper( elem, autoResizeItem, containerSize, newSize, Point.Empty, false, true, false );
					}
				}
			}
			finally
			{
				// Reset the flag.
				//
				this.dontVerifyRowLayoutCache = false;

				if ( null != gridBase )
					gridBase.DesignerChangeNotificationsDisabled = origDesignerNotificationsSuppressed;
			}

			// MRS 8/30/06 - BR15249
			if (resized)
			{
				UltraGrid grid = gridBase as UltraGrid;
				if (grid != null)
					grid.FireEvent(GridEventIds.AfterRowLayoutItemResized, new AfterRowLayoutItemResizedEventArgs(autoResizeItem));
			}
		}

		#endregion // AutoResizeLayoutItem

		#region RowLayoutCellNavigationVerticalResolved

		// SSP 8/17/05 BR05463
		// 
		internal RowLayoutCellNavigation RowLayoutCellNavigationVerticalResolved
		{
			get
			{
				RowLayoutCellNavigation ret = this.HasOverride 
					? this.overrideObj.RowLayoutCellNavigationVertical
					: RowLayoutCellNavigation.Default;

				if ( RowLayoutCellNavigation.Default == ret )
				{
					if ( this.layout.HasOverride )
						ret = this.layout.Override.RowLayoutCellNavigationVertical;

					if ( RowLayoutCellNavigation.Default == ret )
						ret = RowLayoutCellNavigation.Snaking;
				}

				return ret;
			}
		}

		#endregion // RowLayoutCellNavigationVerticalResolved

		#region Helper Methods

		#region LayoutColumnPositionComparer Class

		// SSP 2/24/06 BR09715
		// Made internal.
		// 
		//private class LayoutColumnPositionComparer : IComparer
		internal class LayoutColumnPositionComparer : IComparer
		{
			private bool yWise = true;

			internal LayoutColumnPositionComparer( bool yWise )
			{
				this.yWise = yWise;
			}

			internal LayoutColumnPositionComparer(  ) : this( true )
			{
			}
			
			public int Compare( object o1, object o2 )
			{
				UltraGridColumn col1 = (UltraGridColumn)o1;
				UltraGridColumn col2 = (UltraGridColumn)o2;

				return Compare( col1, col2, this.yWise );
			}

			// SSP 2/24/06 BR09715
			// Added a static overload.
			// 
			public static int Compare( UltraGridColumn col1, UltraGridColumn col2, bool yWise )
			{
				// MD 1/19/09 - Groups in RowLayout
				// When using GroupLayout style, we need to check the flattened layout to know the visible position of the columns.
				if ( col1.Band != null && col1.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout )
				{
					GridBagLayoutManager manager = col1.Band.gridBagRowLayoutManager;

					if ( manager != null )
					{
						if ( manager.LayoutItems.IndexOf( col1 ) != -1 &&
							manager.LayoutItems.IndexOf( col2 ) != -1 )
						{
							IGridBagConstraint constraint1 = manager.LayoutItems.GetConstraint( col1 ) as IGridBagConstraint;
							IGridBagConstraint constraint2 = manager.LayoutItems.GetConstraint( col2 ) as IGridBagConstraint;

							if ( constraint1 != null && constraint2 != null )
							{
								int xCompareResult = constraint1.OriginX.CompareTo( constraint2.OriginX );
								int yCompareResult = constraint1.OriginY.CompareTo( constraint2.OriginY );

								if ( yWise )
								{
									if ( yCompareResult != 0 )
										return yCompareResult;

									return xCompareResult;
								}
								else
								{
									if ( xCompareResult != 0 )
										return xCompareResult;

									return yCompareResult;
								}
							}
						}
					}
				}

				if ( yWise )
				{
					int r;

					r = col1.RowLayoutColumnInfo.OriginYResolved.CompareTo( col2.RowLayoutColumnInfo.OriginYResolved );

					if ( 0 != r )
						return r;

					r = col1.RowLayoutColumnInfo.OriginXResolved.CompareTo( col2.RowLayoutColumnInfo.OriginXResolved );

					return r;
				}
				else
				{
					int r;

					r = col1.RowLayoutColumnInfo.OriginXResolved.CompareTo( col2.RowLayoutColumnInfo.OriginXResolved );

					if ( 0 != r )
						return r;

					r = col1.RowLayoutColumnInfo.OriginYResolved.CompareTo( col2.RowLayoutColumnInfo.OriginYResolved );

					return r;
				}
			}
		}

		#endregion // LayoutColumnPositionComparer Class

		#region CreateGridBagLayoutManager

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal GridBagLayoutManager CreateGridBagLayoutManager( )
		internal static GridBagLayoutManager CreateGridBagLayoutManager()
		{
			GridBagLayoutManager gblm = new GridBagLayoutManager( );
			gblm.ExpandToFitWidth = true;
			gblm.ExpandToFitHeight = true;
			gblm.LayoutMode = GridBagLayoutManager.GridBagLayoutMode.LeftToRight;

			return gblm;
		}

		#endregion // CreateGridBagLayoutManager

		#region GetCachedLayoutContainerCellArea

		internal LayoutContainerCellArea GetCachedLayoutContainerCellArea( )
		{
			if ( null == this.layoutContainerCellArea )
				this.layoutContainerCellArea = LayoutContainerCellArea.CreateEmpty( );
			
			return this.layoutContainerCellArea;
		}

		#endregion // GetCachedLayoutContainerCellArea

		#region GetCachedLayoutContainerHeaderArea
		
		internal LayoutContainerHeaderArea GetCachedLayoutContainerHeaderArea( )
		{
			if ( null == this.layoutContainerHeaderArea )
				this.layoutContainerHeaderArea = LayoutContainerHeaderArea.CreateEmpty( );
			
			return this.layoutContainerHeaderArea;
		}

		#endregion // GetCachedLayoutContainerHeaderArea

		#region GetCachedLayoutContainerCardCellArea

		internal LayoutContainerCardCellArea GetCachedLayoutContainerCardCellArea( )
		{
			if ( null == this.layoutContainerCardCellArea )
				this.layoutContainerCardCellArea = LayoutContainerCardCellArea.CreateEmpty( );
			
			return this.layoutContainerCardCellArea;
		}

		#endregion // GetCachedLayoutContainerCardCellArea

		#region GetCachedLayoutContainerCardLabelArea
		
		internal LayoutContainerCardLabelArea GetCachedLayoutContainerCardLabelArea( )
		{
			if ( null == this.layoutContainerCardLabelArea )
				this.layoutContainerCardLabelArea = LayoutContainerCardLabelArea.CreateEmpty( );
			
			return this.layoutContainerCardLabelArea;
		}

		#endregion // GetCachedLayoutContainerCardLabelArea

		#region GetCachedLayoutContainerSummaryArea
		
		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Obsoleted by the new GetSummaryLayoutManager.
		//
		

		#endregion // GetCachedLayoutContainerSummaryArea

		#region GetCachedLayoutContainerCalcSize
		
		internal LayoutContainerCalcSize GetCachedLayoutContainerCalcSize( )
		{
			if ( null == this.layoutContainerCalcSize )
				this.layoutContainerCalcSize = new LayoutContainerCalcSize( );
			
			return this.layoutContainerCalcSize;
		}

		#endregion // GetCachedLayoutContainerCalcSize

		#endregion // Helper Methods

		#endregion // Row Layout Code

		#endregion // Private/Internal Row Layout Properties/Methods

		// SSP 11/11/03 Add Row Feature
		//
		#region Add Row Feature

		#region BorderStyleTemplateAddRowResolved
		
		internal UIElementBorderStyle BorderStyleTemplateAddRowResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleTemplateAddRow, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleTemplateAddRow : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleTemplateAddRow;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleTemplateAddRow,
						StyleUtils.Role.Row, style, this.BorderStyleRowResolved );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		#endregion // BorderStyleTemplateAddRowResolved

		#region TemplateAddRowSpacingAfterDefault
		
		internal int TemplateAddRowSpacingAfterDefault
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeTemplateAddRowSpacingAfter( ) )
					return this.overrideObj.TemplateAddRowSpacingAfter;

				if ( this.layout.HasOverride && this.layout.Override.ShouldSerializeTemplateAddRowSpacingAfter( ) )
					return this.layout.Override.TemplateAddRowSpacingAfter;

				// Use the row spacing after by default.
				//
				return this.RowSpacingAfterDefault;
			}
		}

		#endregion // TemplateAddRowSpacingAfterDefault

		#region TemplateAddRowSpacingBeforeDefault

		internal int TemplateAddRowSpacingBeforeDefault
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeTemplateAddRowSpacingBefore( ) )
					return this.overrideObj.TemplateAddRowSpacingBefore;

				if ( this.layout.HasOverride && this.layout.Override.ShouldSerializeTemplateAddRowSpacingBefore( ) )
					return this.layout.Override.TemplateAddRowSpacingBefore;

				// Use the row spacing before by default.
				//
				return this.RowSpacingBeforeDefault;
			}
		}

		#endregion // TemplateAddRowSpacingBeforeDefault

		#endregion // Add Row Feature

		// JJD 1/06/04 - Accessibility
		//
		#region ColHeadersAccessibilityObject

		internal System.Windows.Forms.AccessibleObject ColHeadersAccessibilityObject
		{
			get
			{
				// return null if the band is hidden 
				if ( this.HiddenResolved == true )
					return null;

				// for card view only return a band accessible object for band 0
				// with a merge label style
				if ( this.CardView )
				{
					if ( this.Index != 0 ||
						this.CardSettings.StyleResolved != CardStyle.MergedLabels )
						return null;
				}
				else
				{
					// if the column headers are not visible then return null
					if ( this.ColHeadersVisible == false )
						return null;
	
					// if the view style doeant have fixed headers return null
					// SSP 7/18/05 - NAS 5.3 Header Placement
					// 
					//if ( this.Layout.ViewStyleImpl.HasFixedHeaders == false )
					if ( ! this.Layout.ViewStyleImpl.BandHasFixedHeaders( this ) )
						return null;
				}

				if ( this.colHeadersAccessibilityObject == null )
					this.colHeadersAccessibilityObject = this.Layout.Grid.CreateAccessibilityInstance( new HeadersCollection.HeadersAccessibleObject( this ) );

				return this.colHeadersAccessibilityObject;
			}
		}

		#endregion ColHeadersAccessibilityObject

		// JJD 1/07/04 - Accessibility
		//
		#region CardScrollbarAccessibilityObject

		internal System.Windows.Forms.AccessibleObject CardScrollbarAccessibilityObject
		{
			get
			{
				// return null if the band is hidden 
				if ( this.HiddenResolved == true )
					return null;

				// for card view only return a band accessible object for band 0
				// with a merge label style
				if ( !this.CardView ||
					this.Index != 0 )
					return null;

				RowColRegionIntersectionUIElement rcr = this.Layout.UIElement.GetDescendant( typeof( RowColRegionIntersectionUIElement ), new object [] { this.Layout.ActiveRowScrollRegion, this.Layout.ActiveColScrollRegion } ) as RowColRegionIntersectionUIElement;

				if ( rcr != null )
				{
					foreach ( UIElement child in rcr.ChildElements )
					{
						CardAreaUIElement cardArea = child as CardAreaUIElement;

						if ( cardArea != null &&
							cardArea.Band == this )
						{
							return cardArea.ScrollbarAccessibilityObject;
						}
					}
				}

				return null;
			}
		}

		#endregion CardScrollbarAccessibilityObject

		// SSP 6/29/04 - UltraCalc
		//
		#region UltraCalc Related

		#region CalcReference

		internal BandReference CalcReference
		{
			get
			{
				if ( null == this.calcReference && null != this.Layout )
					this.calcReference = new BandReference( this );

				return this.calcReference;
			}
		}

		#endregion // CalcReference

		#region EnsureCalcReferenceNameInitialized

		private string cachedCalcReferenceName = null;
		private string cachedNormalizedCalcReferenceName = null;
		private string lastKey = null;

		private void EnsureCalcReferenceNameInitialized( )
		{
			if ( null != this.cachedCalcReferenceName && (object)this.lastKey == (object)this.Key )
				return;

			string key = this.Key;

			if ( null == key || 0 == key.Length )
			{
				int i = 0;
				do
				{
					key = "_Band_" + i++;
				}
				while ( null != this.Layout && this.Layout.SortedBands.Exists( key ) );
			}

			this.cachedCalcReferenceName = Infragistics.Win.CalcEngine.RefParser.EscapeString( key, false );
			this.cachedNormalizedCalcReferenceName = this.cachedCalcReferenceName.ToLower( RefUtils.UltraCalcCulture );
			this.lastKey = this.Key;
		}

		#endregion // EnsureCalcReferenceNameInitialized

		#region CalcReferenceName
		
		internal string CalcReferenceName
		{
			get
			{
				this.EnsureCalcReferenceNameInitialized( );
				return this.cachedCalcReferenceName;
			}
		}

		#endregion // CalcReferenceName

		#region NormalizedCalcReferenceName
		
		internal string NormalizedCalcReferenceName
		{
			get
			{
				this.EnsureCalcReferenceNameInitialized( );
				return this.cachedNormalizedCalcReferenceName;
			}
		}

		#endregion // NormalizedCalcReferenceName

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return null != this.calcReference;
			}
		}

		#endregion // HasCalcReference

		#region IsStillValid
        
		// SSP 9/1/04
		// Added IsStillValid property.
		//
		internal bool IsStillValid
		{
			get
			{
				return ! this.Disposed && null != this.Layout && this.Index >= 0 && ! this.Layout.Disposed;
			}
		}

		#endregion // IsStillValid

		#region FormulaRowIndexSourceResolved
		

		/// <summary>
		/// Gets the resolved formula source index.
		/// </summary>
        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel
		//internal FormulaRowIndexSource FormulaRowIndexSourceResolved
		// MD 9/19/08
		// Found while fixing TFS7822
		// This property is read-only, so make it hidden from the designer serializer.
        [Browsable(false)]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public FormulaRowIndexSource FormulaRowIndexSourceResolved
		{
			get
			{
				if ( null != this.overrideObj && this.overrideObj.ShouldSerializeFormulaRowIndexSource( ) )
					return this.overrideObj.FormulaRowIndexSource;

				return this.layout.FormulaRowIndexSourceDefault;
			}
		}

		#endregion // FormulaRowIndexSourceResolved

		#endregion // UltraCalc Related

		// SSP 11/16/04
		// Implemented column sizing using cells in non-rowlayout mode. 
		// Added ColumnSizingArea property to the override.
		//
		#region ColumnSizingAreaResolved
		
		internal ColumnSizingArea ColumnSizingAreaResolvedDefault
		{
			get
			{
				if ( this.HasOverride && this.Override.ShouldSerializeColumnSizingArea( ) )
					return this.overrideObj.ColumnSizingArea;

				return this.Layout.HasOverride ? this.Layout.Override.ColumnSizingArea : ColumnSizingArea.Default;
			}
		}
		
		internal ColumnSizingArea ColumnSizingAreaResolved
		{
			get
			{
				ColumnSizingArea area = this.ColumnSizingAreaResolvedDefault;
				return ColumnSizingArea.Default != area ? area : ColumnSizingArea.HeadersOnly;
			}
		}

		#endregion // ColumnSizingAreaResolved

		// SSP 12/13/04 - Merged Cell Feature
		//
		#region MergedCellVersion

		internal int MergedCellVersion
		{
			get
			{
				return this.mergedCellVersion;
			}
		}

		#endregion // MergedCellVersion

		#region BumpMergedCellVersion

		internal void BumpMergedCellVersion( )
		{
			this.mergedCellVersion++;

			if ( null != this.layout )
			{
				this.layout.BumpGrandVerifyVersion( );
				this.layout.BumpCellChildElementsCacheVersion( );
				this.layout.DirtyGridElement( true );
				if ( null != this.layout.ColScrollRegions )
					this.layout.ColScrollRegions.DirtyMetrics( );
			}
		}

		#endregion // BumpMergedCellVersion

		#region SupportsDataErrorInfoResolved

		internal SupportDataErrorInfo SupportsDataErrorInfoResolved
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeSupportDataErrorInfo( ) )
					return this.overrideObj.SupportDataErrorInfo;

				if ( this.layout.HasOverride && this.layout.Override.ShouldSerializeSupportDataErrorInfo( ) )
					return this.layout.Override.SupportDataErrorInfo;

				return SupportDataErrorInfo.None;
			}
		}

		internal bool SupportsDataErrorInfoOnRowsResolved
		{
			get
			{
				SupportDataErrorInfo supportDataErrorInfo = this.SupportsDataErrorInfoResolved;
				return SupportDataErrorInfo.RowsAndCells == supportDataErrorInfo 
					|| SupportDataErrorInfo.RowsOnly == supportDataErrorInfo;
			}
		}

		internal bool SupportsDataErrorInfoOnCellsResolved
		{
			get
			{
				SupportDataErrorInfo supportDataErrorInfo = this.SupportsDataErrorInfoResolved;
				return SupportDataErrorInfo.RowsAndCells == supportDataErrorInfo 
					|| SupportDataErrorInfo.CellsOnly == supportDataErrorInfo;
			}
		}

		#endregion // SupportsDataErrorInfoResolved

		#region NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region SpecialRowPromptField

		/// <summary>
		/// Specifies the key of the column whose cell to position the special row prompts in. 
		/// Default value is null which specifies that the prompt should not be positioned in 
		/// any particular cell but rather it should be overlaid on the special row spanning 
		/// multiple cells if necessary.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Specifies the key of the column whose cell to position the special row prompts in. 
		/// Default value is null which specifies that the prompt should not be positioned in 
		/// any particular cell but rather it should be overlaid on the special row spanning 
		/// multiple cells if necessary.
		/// </p>
		/// <seealso cref="UltraGridOverride.SpecialRowSeparatorHeight"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_SpecialRowPromptField")]
        public string SpecialRowPromptField
		{
			get
			{
				return this.specialRowPromptField;
			}
			set
			{
				if ( value != this.specialRowPromptField )
				{
					this.specialRowPromptField = value;

					this.NotifyPropChange( PropertyIds.SpecialRowPromptField );
				}
			}
		}

		#endregion // SpecialRowPromptField

		#region ShouldSerializeSpecialRowPromptField

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSpecialRowPromptField( )
		{
			return null != this.specialRowPromptField && this.specialRowPromptField.Length > 0;
		}

		#endregion // ShouldSerializeSpecialRowPromptField

		#region ResetSpecialRowPromptField

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSpecialRowPromptField( )
		{
			this.SpecialRowPromptField = null;
		}

		#endregion // ResetSpecialRowPromptField

		#region SpecialRowPromptFieldResolved
		
		internal UltraGridColumn SpecialRowPromptFieldResolved
		{
			get
			{
				string field = this.SpecialRowPromptField;
				return null != field && field.Length > 0 && this.Columns.Exists( field )
					? this.Columns[ field ] : null;
			}
		}

		#endregion // SpecialRowPromptFieldResolved

		#region BorderStyleFilterRowResolved
		
		internal Infragistics.Win.UIElementBorderStyle BorderStyleFilterRowResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleFilterRow, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleFilterRow : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleFilterRow;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleFilterRow,
						StyleUtils.Role.Row, style, this.BorderStyleRowResolved );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		#endregion // BorderStyleFilterRowResolved

		#region BorderStyleFilterCellResolved
		
		internal Infragistics.Win.UIElementBorderStyle BorderStyleFilterCellResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleFilterCell, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleFilterCell : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleFilterCell;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleFilterCell,
						StyleUtils.Role.Cell, style, this.BorderStyleCellResolved );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		#endregion // BorderStyleFilterCellResolved

		#region BorderStyleFilterRowSelectorResolved
		
		internal Infragistics.Win.UIElementBorderStyle BorderStyleFilterRowSelectorResolved
		{
			get
			{
				return this.BorderStyleRowSelectorResolved;
			}
		}

		#endregion // BorderStyleFilterRowSelectorResolved

		#region BorderStyleFilterOperatorResolved
		
		internal Infragistics.Win.UIElementBorderStyle BorderStyleFilterOperatorResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleFilterOperator, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleFilterOperator : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleFilterOperator;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleFilterOperator,
						StyleUtils.Role.FilterOperator, style, this.BorderStyleFilterCellResolved );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		#endregion // BorderStyleFilterOperatorResolved

		#region BorderStyleSpecialRowSeparatorResolved
		
		internal Infragistics.Win.UIElementBorderStyle BorderStyleSpecialRowSeparatorResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleSpecialRowSeparator, out val ) )
				{
					UIElementBorderStyle style = null != this.overrideObj 
						? this.overrideObj.BorderStyleSpecialRowSeparator : UIElementBorderStyle.Default;

					if ( UIElementBorderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.BorderStyleSpecialRowSeparator;

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleSpecialRowSeparator,
						StyleUtils.Role.SpecialRowSeparator, style, UIElementBorderStyle.RaisedSoft );
				}

				return (UIElementBorderStyle)val;

				
				
			}
		}

		#endregion // BorderStyleSpecialRowSeparatorResolved

		#region FilterClearButtonLocationResolved
		
		internal FilterClearButtonLocation FilterClearButtonLocationResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFilterClearButtonLocation( ) )
					return this.overrideObj.FilterClearButtonLocation;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFilterClearButtonLocation( ) )
					return this.layout.Override.FilterClearButtonLocation;

				return FilterClearButtonLocation.RowAndCell;
			}
		}

		#endregion // FilterClearButtonLocationResolved

		#region FilterRowPromptResolved
		
		internal string FilterRowPromptResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFilterRowPrompt( ) )
					return this.overrideObj.FilterRowPrompt;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFilterRowPrompt( ) )
					return this.layout.Override.FilterRowPrompt;

				return null;
			}
		}

		#endregion // FilterRowPromptResolved

		#region TemplateAddRowPromptResolved
		
		internal string TemplateAddRowPromptResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeTemplateAddRowPrompt( ) )
					return this.overrideObj.TemplateAddRowPrompt;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeTemplateAddRowPrompt( ) )
					return this.layout.Override.TemplateAddRowPrompt;

				return null;
			}
		}

		#endregion // TemplateAddRowPromptResolved

		#region FilterRowSpacingAfterDefault
		
		internal int FilterRowSpacingAfterDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFilterRowSpacingAfter( ) )
					return this.overrideObj.FilterRowSpacingAfter;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFilterRowSpacingAfter( ) )
					return this.layout.Override.FilterRowSpacingAfter;

				return this.RowSpacingAfterDefault;
			}
		}

		#endregion // FilterRowSpacingAfterDefault

		#region FilterRowSpacingBeforeDefault
		
		internal int FilterRowSpacingBeforeDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFilterRowSpacingBefore( ) )
					return this.overrideObj.FilterRowSpacingBefore;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFilterRowSpacingBefore( ) )
					return this.layout.Override.FilterRowSpacingBefore;

				return this.RowSpacingBeforeDefault;
			}
		}

		#endregion // FilterRowSpacingBeforeDefault

		#region FilterUITypeResolvedDefault
		
		private FilterUIType FilterUITypeResolvedDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFilterUIType( ) )
					return this.overrideObj.FilterUIType;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFilterUIType( ) )
					return this.layout.Override.FilterUIType;

				return FilterUIType.Default;
			}
		}

		#endregion // FilterUITypeResolvedDefault

		#region FilterUITypeResolved
		
		internal FilterUIType FilterUITypeResolved
		{
			get
			{
				FilterUIType ret = this.FilterUITypeResolvedDefault;

				// Explicitly resolve the FilterRow setting in UltraCombo to HeaderIcons.
				//
				if ( FilterUIType.FilterRow == ret && ! ( this.Layout.Grid is UltraGrid ) )
					ret = FilterUIType.HeaderIcons;

				return FilterUIType.Default != ret ? ret : FilterUIType.HeaderIcons;
			}
		}

		#endregion // FilterUITypeResolved

		#region FixedRowIndicatorResolved
		
		internal FixedRowIndicator FixedRowIndicatorResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFixedRowIndicator( ) )
					return this.overrideObj.FixedRowIndicator;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFixedRowIndicator( ) )
					return this.layout.Override.FixedRowIndicator;

				return FixedRowIndicator.None;
			}
		}

		#endregion // FixedRowIndicatorResolved

		#region FixedRowsLimitResolved
		
		internal int FixedRowsLimitResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFixedRowsLimit( ) )
					return this.overrideObj.FixedRowsLimit;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFixedRowsLimit( ) )
					return this.layout.Override.FixedRowsLimit;

				// 0 means unlimited.
				//
				return 0;
			}
		}

		#endregion // FixedRowsLimitResolved

		#region FixedRowSortOrderResolved
		
		internal FixedRowSortOrder FixedRowSortOrderResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFixedRowSortOrder( ) )
					return this.overrideObj.FixedRowSortOrder;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFixedRowSortOrder( ) )
					return this.layout.Override.FixedRowSortOrder;

				return FixedRowSortOrder.Sorted;
			}
		}

		#endregion // FixedRowSortOrderResolved

		#region FixedRowStyleResolved
		
		internal FixedRowStyle FixedRowStyleResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeFixedRowStyle( ) )
					return this.overrideObj.FixedRowStyle;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeFixedRowStyle( ) )
					return this.layout.Override.FixedRowStyle;

				return FixedRowStyle.Top;
			}
		}

		#endregion // FixedRowStyleResolved

		#region GroupBySummaryDisplayStyleResolved

		// SSP 10/27/06 BR16917
		// Made GroupBySummaryDisplayStyleResolved public for use by excel exporter.
		// 
		/// <summary>
		/// Returns the resolved GroupBySummaryDisplayStyle.
		/// </summary>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Never ) ]

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public GroupBySummaryDisplayStyle GroupBySummaryDisplayStyleResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeGroupBySummaryDisplayStyle( ) )
					return this.overrideObj.GroupBySummaryDisplayStyle;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeGroupBySummaryDisplayStyle( ) )
					return this.layout.Override.GroupBySummaryDisplayStyle;

				return GroupBySummaryDisplayStyle.Text;
			}
		}

		#endregion // GroupBySummaryDisplayStyleResolved

		#region GetSequenceSummaryRowResolved
		
		internal long GetSequenceSummaryRowResolved( bool top )
		{
			long val;

			if ( this.HasOverride &&
				this.overrideObj.ShouldSerializeSequenceSummaryRow( ) )
				val = this.overrideObj.SequenceSummaryRow;
			else if ( this.layout.HasOverride 
				&& this.layout.Override.ShouldSerializeSequenceSummaryRow( ) )
				val = this.layout.Override.SequenceSummaryRow;
			else
				val = -1;

			int DEFAULT_ORDER = top ? -3 : 3;

			return val * 256 + DEFAULT_ORDER;
		}

		#endregion // GetSequenceSummaryRowResolved

		#region GetSequenceFilterRowResolved
		
		internal long GetSequenceFilterRowResolved( bool top )
		{
			long val;

			if ( this.HasOverride &&
				this.overrideObj.ShouldSerializeSequenceFilterRow( ) )
				val = this.overrideObj.SequenceFilterRow;
			else if ( this.layout.HasOverride 
				&& this.layout.Override.ShouldSerializeSequenceFilterRow( ) )
				val = this.layout.Override.SequenceFilterRow;
			else
				val = -1;

			int DEFAULT_ORDER = top ? -2 : 2;

			return val * 256 + DEFAULT_ORDER;
		}

		#endregion // GetSequenceFilterRowResolved

		#region GetSequenceFixedAddRowResolved
		
		internal long GetSequenceFixedAddRowResolved( bool top )
		{
			long val;

			if ( this.HasOverride &&
				this.overrideObj.ShouldSerializeSequenceFixedAddRow( ) )
				val = this.overrideObj.SequenceFixedAddRow;
			else if ( this.layout.HasOverride 
				&& this.layout.Override.ShouldSerializeSequenceFixedAddRow( ) )
				val = this.layout.Override.SequenceFixedAddRow;
			else
				val = -1;

			int DEFAULT_ORDER = top ? -1 : 1;

			return val * 256 + DEFAULT_ORDER;
		}

		#endregion // GetSequenceFixedAddRowResolved

		#region SpecialRowSeparatorHeightResolved
		
		internal int SpecialRowSeparatorHeightResolved
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSpecialRowSeparatorHeight( ) )
					return this.overrideObj.SpecialRowSeparatorHeight;

				if ( this.layout.HasOverride 
					&& this.layout.Override.ShouldSerializeSpecialRowSeparatorHeight( ) )
					return this.layout.Override.SpecialRowSeparatorHeight;

				return UltraGridBand.SPECIAL_ROW_SEPARATOR_HEIGHT;
			}
		}

		#endregion // SpecialRowSeparatorHeightResolved

		#region SpecialRowSeparatorResolved

		internal SpecialRowSeparator GetSpecialRowSeparatorResolved( UltraGridRow row )
		{
			if ( this.HasOverride && this.overrideObj.ShouldSerializeSpecialRowSeparator( ) )
				return this.overrideObj.SpecialRowSeparator;

			if ( this.layout.HasOverride 
				&& this.layout.Override.ShouldSerializeSpecialRowSeparator( ) )
				return this.layout.Override.SpecialRowSeparator;

			// By default show a separator after the fixed template add-row. This is what the
			// outlook does.
			// 
			if ( null != row && row.ParentCollection.IsFixedAddRow( row ) )
			{
				AllowAddNew allowAddNew = this.AllowAddNewResolved;
				if ( AllowAddNew.FixedAddRowOnTop == allowAddNew || AllowAddNew.FixedAddRowOnBottom == allowAddNew )
					return SpecialRowSeparator.TemplateAddRow;
			}

			return SpecialRowSeparator.None;
		}

		#endregion // SpecialRowSeparatorResolved

		#region ResolveSpecialRowSeparatorAppearance

		internal void ResolveSpecialRowSeparatorAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			this.MergeBLOverrideAppearances( 
				ref appData, ref flags,
				UltraGridOverride.OverrideAppearanceIndex.SpecialRowSeparatorAppearance 
				// SSP 3/13/06 - App Styling
				// 
				, StyleUtils.Role.SpecialRowSeparator, AppStyling.RoleState.Normal );
		}

		#endregion // ResolveSpecialRowSeparatorAppearance

		#region ResolveFilterClearButtonAppearance

		internal void ResolveFilterClearButtonAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			this.MergeBLOverrideAppearances( 
				ref appData, ref flags,
				UltraGridOverride.OverrideAppearanceIndex.FilterClearButtonAppearance
				// SSP 3/13/06 - App Styling
				// Use the new overload that takes in the app styling related params.
				// 
				, StyleUtils.Role.FilterClearButton, AppStyling.RoleState.Normal );

			// Resolve the default image alignment to center.
			//
			if ( 0 != ( AppearancePropFlags.ImageHAlign & flags ) )
			{
				appData.ImageHAlign = HAlign.Center;
				flags ^= AppearancePropFlags.ImageHAlign;
			}

			if ( 0 != ( AppearancePropFlags.ImageVAlign & flags ) )
			{
				appData.ImageVAlign = VAlign.Middle;
				flags ^= AppearancePropFlags.ImageVAlign;
			}

			// SSP 6/8/05 BR04478
			// 
			if ( 0 != ( AppearancePropFlags.Image & flags ) )
			{
				appData.Image = this.Layout.FilterClearButtonImageFromResource;
				flags ^= AppearancePropFlags.Image;
			}
		}

		#endregion // ResolveFilterClearButtonAppearance

		#endregion // NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region RowSelectorNumberStyleResolved

		// SSP 3/30/05 NAS 5.2 Row Numbers
		//
		internal RowSelectorNumberStyle RowSelectorNumberStyleResolved
		{
			get
			{
				RowSelectorNumberStyle ret = RowSelectorNumberStyle.Default;
				if ( null != this.overrideObj )
					ret = this.overrideObj.RowSelectorNumberStyle;

				if ( RowSelectorNumberStyle.Default == ret && this.layout.HasOverride )
					ret = this.layout.Override.RowSelectorNumberStyle;

				return RowSelectorNumberStyle.Default != ret ? ret : RowSelectorNumberStyle.None;
			}
		}

		#endregion // RowSelectorNumberStyleResolved

		#region GetRowSelectorNumberWidth

		internal void DirtyCachedRowSelectorNumberWidth( )
		{
			this.cachedRowSelectorNumberWidth = -1;
		}

		private int cachedRowSelectorNumberWidth = -1;
		private int cachedRowSelectorNumberWidth_VerifiedVersion = -1;
		internal int GetRowSelectorNumberWidth( )
		{
			if ( this.cachedRowSelectorNumberWidth < 0 
				|| this.cachedRowSelectorNumberWidth_VerifiedVersion != this.Layout.RowLayoutVersion )
			{
				this.cachedRowSelectorNumberWidth_VerifiedVersion = this.Layout.RowLayoutVersion;

				if ( RowSelectorNumberStyle.None == this.RowSelectorNumberStyleResolved )
				{
					this.cachedRowSelectorNumberWidth = 0;
				}
				else
				{
					int n = 999;
					if ( this.IsRootBand )
						// SSP 11/28/05
						// Since the row selector indexers are 1 based, we need to use the Count
						// here instead of Count - 1.
						// 
						//n = Math.Max( n, this.Layout.Rows.Count - 1 );
						n = Math.Max( n, this.Layout.Rows.Count );

					string measureString = new String( '0', n.ToString( ).Length );

					AppearanceData appData = new AppearanceData( );
					AppearancePropFlags flags = AppearancePropFlags.FontData;
					this.MergeBLOverrideAppearances( ref appData, ref flags, UltraGridOverride.OverrideAppearanceIndex.RowSelector 
						// SSP 3/13/06 - App Styling
						// 
						, StyleUtils.Role.RowSelector, AppStyling.RoleState.Normal );

					Size size = this.Layout.CalculateFontSize( ref appData, measureString );
					int newNumberWidth = size.Width;

					if ( this.cachedRowSelectorNumberWidth != newNumberWidth )
					{
						this.cachedRowSelectorNumberWidth = newNumberWidth;
						this.DirtyRowLayoutCachedInfo( );
					}
				}
			}

			return this.cachedRowSelectorNumberWidth;
		}

		#endregion // GetRowSelectorNumberWidth

		#region CardAreaUIElement

		//	BF 1.24.05	NAS2005 Vol1 - CardView vertical scrolling
		internal CardAreaUIElement CardAreaUIElement
		{
			get
			{
				// return null if the band is hidden 
				if ( this.HiddenResolved == true )
					return null;

				if ( ! this.CardView )
					return null;

				RowColRegionIntersectionUIElement rcr = this.Layout.UIElement.GetDescendant( typeof( RowColRegionIntersectionUIElement ), new object [] { this.Layout.ActiveRowScrollRegion, this.Layout.ActiveColScrollRegion } ) as RowColRegionIntersectionUIElement;

				if ( rcr != null )
				{
					foreach ( UIElement child in rcr.ChildElements )
					{
						CardAreaUIElement cardArea = child as CardAreaUIElement;

						if ( cardArea != null && cardArea.Band == this )
							return cardArea;
					}
				}

				return null;
			}
		}

		#endregion // CardAreaUIElement

		//MRS 5/11/05 - BR02804
		#region CopyPrintRelatedProperties
		internal void CopyPrintRelatedProperties(UltraGridBand source, RowPropertyCategories rowPropertyCategories)
		{
			if (RowPropertyCategories.Height == ( RowPropertyCategories.Height & rowPropertyCategories ))
				this.synchronizedRowHeight = source.synchronizedRowHeight;
		}
		#endregion CopyPrintRelatedProperties

		// JAS 2005 v2 GroupBy Row Extensions
		//
		#region GroupBy Row Extensions
		
			#region IndentationGroupByRow Logic

				#region IndentationGroupByRow

		/// <summary>
		/// Gets/sets the number of pixels that the band's groupby rows will be offset.
		/// The default value is -1, which means that the grid will determine the offset.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use the <b>IndentationGroupByRow</b> property to specify how much indenting should be applied to the GroupBy rows in a band.  The default value for this property is -1, which indicates that the grid's default indenting should be used.</p>
		/// <p class="body">The indentation of the GroupBy rows is a function of the IndentationGroupByRow value and the row's zero-based depth-level.  In other words, the top-level GroupBy rows in a band will always be flush against the left edge of the band because the top-level GroupBy rows have a depth-level of zero.</p>
		/// <p class="body"><b>Note:</b> If this property is set to 0 then the left edge of all the GroupBy rows in the band will be at the same X coordinate.  In this situation the grid automatically determines the location of the expansion indicators in each GroupBy row such that the relationships between the groupings are visually represented via the relative location of said expansion indicators.  The indentation of the GroupBy row expansion indicators is, in this case, a function of a GroupBy row's "depth level" and the <see cref="UltraGridBand.IndentationGroupByRowExpansionIndicator"/> value of that GroupBy row's respective band.</p>
		/// </remarks> 
		[LocalizedDescription( "LD_UltraGridBand_P_IndentationGroupByRow" )]
		[LocalizedCategory("LC_Display")]
		public int IndentationGroupByRow
		{
			get	{ return this.indentationGroupByRow; }
			set
			{
				if( value < -1 )
					throw new ArgumentException( SR.GetString( "LE_Band_IndentationGroupByRow_OutOfRange" ) );

                // MRS 5/16/2008 - Found during unit testing
                if (this.indentationGroupByRow == value)
                    return;

				this.indentationGroupByRow = value;

				this.NotifyPropChange( PropertyIds.IndentationGroupByRow );
			}
		}

				#endregion // IndentationGroupByRow

				#region ShouldSerializeIndentationGroupByRow

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeIndentationGroupByRow() 
		{
			return this.indentationGroupByRow >= 0;
		}

				#endregion // ShouldSerializeIndentationGroupByRow
 
				#region ResetIndentationGroupByRow

		/// <summary>
		/// Resets IndentationGroupByRow its default value (-1).
		/// </summary>
		public void ResetIndentationGroupByRow() 
		{
			this.IndentationGroupByRow = -1;
		}		

				#endregion // ResetIndentationGroupByRow

				#region IndentationGroupByRowResolved

        // MRS 10/2/07
        // Made this public so I can get the proper indendations for DocumentExporter. 
		//internal int IndentationGroupByRowResolved
        /// <summary>
        /// Returns the resolved IndentationGroupByRow for this band.
        /// </summary>
        // MBS 3/12/08 - Hid from designer
        [Browsable(false)]

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
        public int IndentationGroupByRowResolved
		{
			get
			{
				if( this.IndentationGroupByRow == -1 )
					return GroupByRowUIElement.GROUP_BY_INDENT;

				return this.IndentationGroupByRow;
			}
		}

				#endregion // IndentationGroupByRowResolved

			#endregion // IndentationGroupByRow Logic

			#region IndentationGroupByRowExpansionIndicator Logic

				#region IndentationGroupByRowExpansionIndicator

		/// <summary>
		/// Gets/sets the number of pixels that the expansion indicators in the band's groupby rows will be offset.
		/// The default value is -1, which means that the grid will determine the offset.
		/// </summary>
		/// <remarks>
		/// You can use the <b>IndentationGroupByRowExpansionIndicator</b> property to specify how much indenting should be applied to the expansion indicators in the GroupBy rows of a band.  The default value for this property is -1, which indicates that the grid's default indenting should be used.
		/// <p class="body"><b>Note:</b> If the <see cref="UltraGridBand.IndentationGroupByRow"/> property is set to 0 then the left edge of all the GroupBy rows in the band will be at the same X coordinate.  In this situation the grid automatically determines the location of the expansion indicators in each GroupBy row such that the relationships between the groupings are visually represented via the relative location of said expansion indicators.  The indentation of the GroupBy row expansion indicators is, in this case, a function of a GroupBy row's "depth level" and the IndentationGroupByRowExpansionIndicator value of that GroupBy row's respective band.</p>
		/// </remarks> 
		[LocalizedDescription( "LD_UltraGridBand_P_IndentationGroupByRowExpansionIndicator" )]
		[LocalizedCategory("LC_Display")]
		public int IndentationGroupByRowExpansionIndicator
		{
			get	{ return this.indentationGroupByRowExpansionIndicator; }
			set
			{
				if( value < -1 )
					throw new ArgumentException( SR.GetString( "LE_Band_IndentationGroupByRowExpansionIndicator_OutOfRange" ) );

                // MRS 5/16/2008 - Found during unit testing
                if (this.indentationGroupByRowExpansionIndicator == value)
                    return;

				this.indentationGroupByRowExpansionIndicator = value;

				this.NotifyPropChange( PropertyIds.IndentationGroupByRowExpansionIndicator );
			}
		}

				#endregion // IndentationGroupByRowExpansionIndicator

				#region ShouldSerializeIndentationGroupByRowExpansionIndicator

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeIndentationGroupByRowExpansionIndicator() 
		{
			return this.indentationGroupByRowExpansionIndicator >= 0;
		}

				#endregion // ShouldSerializeIndentationGroupByRowExpansionIndicator
 
				#region ResetIndentationGroupByRowExpansionIndicator

		/// <summary>
		/// Resets IndentationGroupByRowExpansionIndicator its default value (-1).
		/// </summary>
		public void ResetIndentationGroupByRowExpansionIndicator() 
		{
			this.IndentationGroupByRowExpansionIndicator = -1;
		}		

				#endregion // ResetIndentationGroupByRowExpansionIndicator

				#region IndentationGroupByRowExpansionIndicatorResolved

		internal int IndentationGroupByRowExpansionIndicatorResolved
		{
			get
			{
				if( this.IndentationGroupByRowExpansionIndicator == -1 )
					return GroupByRowUIElement.GROUP_BY_EXPANSION_INDICATOR_SPACING;

				return this.IndentationGroupByRowExpansionIndicator;
			}
		}

				#endregion // IndentationGroupByRowExpansionIndicatorResolved

			#endregion // IndentationGroupByRowExpansionIndicator Logic

			#region GroupByRowExpansionStyleResolved

		internal GroupByRowExpansionStyle GroupByRowExpansionStyleResolved
		{
			get
			{
				if( this.HasOverride &&
					this.Override.GroupByRowExpansionStyle != GroupByRowExpansionStyle.Default )
					return this.Override.GroupByRowExpansionStyle;

				if( this.Layout.HasOverride && 
					this.Layout.Override.GroupByRowExpansionStyle != GroupByRowExpansionStyle.Default )
					return this.Layout.Override.GroupByRowExpansionStyle;

				return GroupByRowExpansionStyle.ExpansionIndicatorAndDoubleClick;
			}
		}

			#endregion // GroupByRowExpansionStyleResolved

			#region GroupByRowInitialExpansionStateResolved

		internal GroupByRowInitialExpansionState GroupByRowInitialExpansionStateResolved
		{
			get
			{
				if( this.HasOverride && 
					this.Override.GroupByRowInitialExpansionState != GroupByRowInitialExpansionState.Default )
					return this.Override.GroupByRowInitialExpansionState;

				if( this.Layout.HasOverride && 
					this.Layout.Override.GroupByRowInitialExpansionState != GroupByRowInitialExpansionState.Default )
					return this.Layout.Override.GroupByRowInitialExpansionState;

				// If the end-user is not able to toggle the expansion state of the groupby rows then 
				// the groupby rows must be expanded or else they will never be able to see the grid's data.
				//
				return 
					this.GroupByRowExpansionStyleResolved == GroupByRowExpansionStyle.Disabled ?
					GroupByRowInitialExpansionState.Expanded :
					GroupByRowInitialExpansionState.Collapsed;
			}
		}

			#endregion // GroupByRowInitialExpansionStateResolved

		#endregion // GroupBy Row Extensions

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
		//
		#region Header/RowSelector Styles

			#region HeaderStyleResolved

		internal HeaderStyle HeaderStyleResolved
		{
			get
			{
				HeaderStyle style = this.HeaderStyleUnresolved;
				if( style != HeaderStyle.Default )
					return style;

				// JAS 5/6/05 BR03741
				// The default style is different for a band in CardView than for one not in CardView.
				//
				//return HeaderStyle.XPThemed;
				return this.CardView ? HeaderStyle.Standard : HeaderStyle.XPThemed;
			}
		}

			#endregion // HeaderStyleResolved

			#region HeaderStyleUnresolved

		internal HeaderStyle HeaderStyleUnresolved
		{
			get
			{
				// SSP 3/16/06 - App Styling
				// 
				// --------------------------------------------------------------------------------
				object objStyle;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.HeaderStyleLayout, out objStyle ) )
				{
					HeaderStyle style = null != this.overrideObj
						? this.overrideObj.HeaderStyle : HeaderStyle.Default;

					if ( HeaderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.HeaderStyle;

					object defVal = HeaderStyle.Default;
					objStyle = StyleUtils.CachePropertyValue( this, StyleUtils.CachedProperty.HeaderStyleLayout,
						StyleUtils.GetComponentRoleHeaderStyle( this.layout ), style, defVal, defVal );
				}

				return (HeaderStyle)objStyle;

				
				// --------------------------------------------------------------------------------
			}
		}

			#endregion // HeaderStyleUnresolved

			#region RowSelectorStyleUnresolved

		// SSP 3/16/06 - App Styling
		// Added RowSelectorStyleUnresolved method.
		// 
		internal HeaderStyle RowSelectorStyleUnresolved
		{
			get
			{
				object objStyle;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.RowSelectorStyle, out objStyle ) )
				{
					HeaderStyle style = null != this.overrideObj
						? this.overrideObj.RowSelectorStyle : HeaderStyle.Default;

					if ( HeaderStyle.Default == style && this.layout.HasOverride )
						style = this.layout.Override.RowSelectorStyle;

					object defVal = HeaderStyle.Default;
					objStyle = StyleUtils.CachePropertyValue( this, 
						StyleUtils.CustomProperty.RowSelectorStyle, style, defVal, defVal );
				}

				return (HeaderStyle)objStyle;
			}
		}

			#endregion // RowSelectorStyleUnresolved

			#region RowSelectorStyleResolved

		internal HeaderStyle RowSelectorStyleResolved
		{
			get
			{
				// SSP 3/16/06 - App Styling
				// Added RowSelectorStyleUnresolved method.
				// 
				// ----------------------------------------------------------------------
				HeaderStyle style = this.RowSelectorStyleUnresolved;
				if ( HeaderStyle.Default != style )
					return style;

				
				// ----------------------------------------------------------------------

				// Next check to see if the HeaderStyle has a non-default value.
				// If so, return that non-default value.
				//
				style = this.HeaderStyleUnresolved;
				if( style != HeaderStyle.Default )
					return style;

				// If neither property has been set, return Standard.
				//
				return HeaderStyle.Standard;
			}
		}

			#endregion // RowSelectorStyleResolved

		// JAS 4/27/05
			#region HeaderBorderThickness
		internal int HeaderBorderThickness
		{
			get
			{
                // MRS NAS v8.1 - Windows Vista Style for UltraWinTree                
                //// The 'WindowsXPCommand' header style does not use any UIElementBorderStyle value
                //// so its border width must be calculated separately.  Note that the 'MouseOver' button state
                //// is passed into the helper method so that the "hottracking border width" is returned.
                ////
                //if( this.HeaderStyleResolved == HeaderStyle.WindowsXPCommand )
                //    return DrawUtility.CalculateButtonBorderWidths( 
                //        UIElementButtonStyle.WindowsXPCommandButton, 
                //        UIElementButtonState.MouseOver ).Left;
                //
                switch (this.HeaderStyleResolved)
                {
                    case HeaderStyle.WindowsXPCommand:
                        return DrawUtility.CalculateButtonBorderWidths(
                            UIElementButtonStyle.WindowsXPCommandButton,
                            UIElementButtonState.MouseOver).Left;
                    case HeaderStyle.WindowsVista:
                        return DrawUtility.CalculateButtonBorderWidths(
                            UIElementButtonStyle.WindowsVistaButton,
                            UIElementButtonState.MouseOver).Left;
                    case HeaderStyle.Standard:
                    case HeaderStyle.XPThemed:
                        break;
                    case HeaderStyle.Default:
                    default:
                        Debug.Fail("Unknown HeaderStyle");
                        break;
                }

                // If the 'WindowsXPCommand' header style is not being used then just return the 
				// border width of the resolved UIElementBorderStyle value.
				//
				if( this.Layout != null )
					return this.Layout.GetBorderThickness( this.BorderStyleHeaderResolved );

				// If all else fails, return 0.
				//
				return 0;
			}
		}

		#endregion // HeaderBorderThickness

		#endregion // Header/RowSelector Styles

		#region GetButtonStyleUnresolved

		// SSP 3/16/06 - App Styling
		// Added GetButtonStyleUnresolved.
		// 
		internal UIElementButtonStyle GetButtonStyleUnresolved( StyleUtils.Role eRole, StyleUtils.CachedProperty cacheProp )
		{
			object objStyle;
			if ( ! StyleUtils.GetCachedPropertyVal( this, cacheProp, out objStyle ) )
			{
				UIElementButtonStyle style = null != this.overrideObj
					? this.overrideObj.ButtonStyle : UIElementButtonStyle.Default;

				if ( UIElementButtonStyle.Default == style && this.layout.HasOverride )
					style = this.layout.Override.ButtonStyle;

				// If button style for a role other than Layout is being asked for (for example the ColumnChooserButton
				// button style), then resolve to the component role's button style. In other words in the case of the
				// column chooser button, the button style in the order of descending precendence should be the
				// column chooser button role's button style setting, then override's button style setting and then
				// the component role's button style setting.
				// 
				UIElementButtonStyle defVal = UIElementButtonStyle.Default;
				StyleUtils.Role controlAreaRole = StyleUtils.GetControlAreaRole( this.layout );
				if ( controlAreaRole != eRole )
					defVal = GetButtonStyleUnresolved( controlAreaRole, StyleUtils.CachedProperty.ButtonStyleLayout );

				objStyle = StyleUtils.CacheButtonStylePropertyValue( this, cacheProp, eRole, style, defVal );
			}

			return (UIElementButtonStyle)objStyle;
		}

		#endregion // GetButtonStyleUnresolved

		#region GetButtonStyleResolved

		// SSP 3/27/06 - App Styling
		// Commented out the ButtonStyleResolved property and instead added GetButtonStyleResolved method
		// below because we need to pass in the context of the role, since different roles could have 
		// different border style settings. Also added GetButtonStyleUnresolved method.
		// 
		

		internal UIElementButtonStyle GetButtonStyleResolved( StyleUtils.Role eRole, StyleUtils.CachedProperty cacheProp )
		{
			return this.GetButtonStyleResolved( eRole, cacheProp, true );
		}

		// AS 1/30/06
		// Moved to a helper method to allow the style to not be fully resolved
		// if no properties are explicitly set that affect the button style.
		//
		// SSP 3/16/06 - App Styling
		// Added eRole and cacheProp app-styling related parameters. We need to pass this info 
		// so we can get the ButtonStyle off the correct role.
		// 
		//internal UIElementButtonStyle GetButtonStyleResolved(bool resolveDefault)
		internal UIElementButtonStyle GetButtonStyleResolved( StyleUtils.Role eRole, StyleUtils.CachedProperty cacheProp, bool resolveDefault )
		{
			// SSP 3/16/06 - App Styling
			// Added ButtonStyleUnresolved. Code in there was moved from here and modified.
			// 
			// ----------------------------------------------------------------------------
			UIElementButtonStyle style = this.GetButtonStyleUnresolved( eRole, cacheProp );
			if ( UIElementButtonStyle.Default != style )
				return style;

			
			// ----------------------------------------------------------------------------


			// If the ButtonStyle has not been set, resolve the value against the resolved HeaderStyle.
			//

			// SSP 2/24/06 BR10400
			// Only base the button style on header style if the header style is explicitly set.
			// Otherwise default to the old behavior which was to use XPThemed/Button3D.
			// 
			//switch( this.HeaderStyleResolved )
			switch( this.HeaderStyleUnresolved )
			{
				case HeaderStyle.Default:
					if (resolveDefault)
						style = UIElementButtonStyle.Button3D;
					else
						style = UIElementButtonStyle.Default;
					break;

				case HeaderStyle.XPThemed:
					style = UIElementButtonStyle.Button3D;
					break;

				case HeaderStyle.Standard:
					style = UIElementButtonStyle.Button;
					break;

				case HeaderStyle.WindowsXPCommand:
					style = UIElementButtonStyle.WindowsXPCommandButton;
					break;

                // MRS NAS v8.1 - Windows Vista Style for UltraWinTree
                case HeaderStyle.WindowsVista:
                    style = UIElementButtonStyle.WindowsVistaButton;
                    break;

				default:
					Debug.Fail( "Unrecognized HeaderStyle value: " + this.HeaderStyleResolved.ToString() );
					style = UIElementButtonStyle.Button3D;
					break;
			}

			return style;
		}

		#endregion // GetButtonStyleResolved

		// JAS v5.2 Wrapped Header Text 4/18/05
		//
		#region WrapHeaderTextResolved

		internal bool WrapHeaderTextResolved
		{
			get
			{
				
				
				
				

				object objVal;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.WrapHeaderText, out objVal ) )
				{
					DefaultableBoolean val = null != this.overrideObj 
						? this.overrideObj.WrapHeaderText : DefaultableBoolean.Default;

					if ( DefaultableBoolean.Default == val && this.layout.HasOverride )
						val = this.layout.Override.WrapHeaderText;

                    objVal = StyleUtils.CachePropertyValue( this, 
						StyleUtils.CustomProperty.WrapHeaderText, val, DefaultableBoolean.Default, DefaultableBoolean.False );
				}

				return DefaultableBoolean.True == (DefaultableBoolean)objVal;
				
			}
		}

		#endregion // WrapHeaderTextResolved

		// JAS 2005 v2 XSD Support
		//
		#region XSD Support

			#region MinRows

		/// <summary>
		/// Gets/sets the minimum number of rows allowed in the band.  
		/// This value is inclusive.  
		/// The default is 0.
		/// If an attempt is made to delete rows from a band and the remaining number of rows would be less
		/// than MinRows, the rows are not deleted and the <see cref="UltraGrid.Error"/> event will fire.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>MaxRows</b> and <b>MinRows</b> will prevent the user from adding or removing
		/// rows if the RowsCollection will violate the MaxRows and MinRows constrants. These
		/// properties do not control what the data source can contain. It only controls whether
		/// the user is allowed to add or remove rows beyond a certain point.
		/// </p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridBand_P_MinRows")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MinRows
		{
			get { return this.minRows; }
			set
			{
				if( value < 0 )
					throw new ArgumentOutOfRangeException( "MinRows", value, "" );

                // MRS 5/16/2008 - Found during unit testing
                if (minRows == value)
                    return;

				this.minRows = value;

				// If this property had been set by the EnforceXsdConstraints method, then
				// clear the bit which represents this property so that the ClearXsdConstraints
				// method will know not to reset this value.
				//
				if( (this.XsdSuppliedConstraints & XsdConstraintFlags.MinRows) != 0 )
					this.XsdSuppliedConstraints &= ~XsdConstraintFlags.MinRows;

				this.NotifyPropChange( PropertyIds.MinRows );
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeMinRows() 
		{
			return this.minRows > 0;
		}
 
		/// <summary>
		/// Resets MinRows its default value (0).
		/// </summary>
		public void ResetMinRows() 
		{
			this.MinRows = 0;
		}

			#endregion // MinRows

			#region MaxRows

		/// <summary>
		/// Gets/sets the maximum number of rows allowed in the band.  
		/// This value is inclusive.  
		/// The default is practical infinity (Int32.MaxValue).
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>MaxRows</b> and <b>MinRows</b> will prevent the user from adding or removing
		/// rows if the RowsCollection will violate the MaxRows and MinRows constrants. These
		/// properties do not control what the data source can contain. It only controls whether
		/// the user is allowed to add or remove rows beyond a certain point.
		/// </p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridBand_P_MaxRows") ]
		[ LocalizedCategory("LC_Behavior") ]
		public int MaxRows
		{
			get { return this.maxRows; }
			set
			{
				if( value < 0 )
					throw new ArgumentOutOfRangeException( "MaxRows", value, "" );

                // MRS 5/16/2008 - Found during unit testing
                if (maxRows == value)
                    return;

				this.maxRows = value;

				// If this property had been set by the EnforceXsdConstraints method, then
				// clear the bit which represents this property so that the ClearXsdConstraints
				// method will know not to reset this value.
				//
				if( (this.XsdSuppliedConstraints & XsdConstraintFlags.MaxRows) != 0 )
					this.XsdSuppliedConstraints &= ~XsdConstraintFlags.MaxRows;

				this.NotifyPropChange( PropertyIds.MaxRows );
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeMaxRows() 
		{
			return this.maxRows < Int32.MaxValue;
		}
 
		/// <summary>
		/// Resets MaxRows its default value (Int32.MaxValue).
		/// </summary>
		public void ResetMaxRows() 
		{
			this.MaxRows = Int32.MaxValue;
		}

			#endregion // MaxRows

			#region XsdSuppliedConstraints

		internal XsdConstraintFlags XsdSuppliedConstraints
		{
			get { return this.xsdSuppliedConstraints;  }
			set { this.xsdSuppliedConstraints = value; }
		}

			#endregion // XsdSuppliedConstraints

		#endregion // XSD Support

		// MRS 4/5/05 - RowLayout Column Moving/Span Sizing
		#region AllowRowLayoutColMovingResolved

		internal Layout.GridBagLayoutAllowMoving AllowRowLayoutColMovingResolved
		{
			get
			{
				// MRS 4/11/05 - Allow Sizing at Design-time. 
				if (null != this.Layout &&
					null != this.layout.Grid &&
					this.Layout.Grid.DesignMode)
				{
					return Infragistics.Win.Layout.GridBagLayoutAllowMoving.AllowAll;
				}

				// MRS 4/21/05 - Only allow this for the grid. 
				if (null != this.Layout &&
					!(this.layout.Grid is UltraGrid)) 
				{
					return Infragistics.Win.Layout.GridBagLayoutAllowMoving.None;
				}

				Layout.GridBagLayoutAllowMoving ret = Infragistics.Win.Layout.GridBagLayoutAllowMoving.Default;
				if ( this.HasOverride )
					ret = this.overrideObj.AllowRowLayoutColMoving;

				if ( Infragistics.Win.Layout.GridBagLayoutAllowMoving.Default == ret && this.layout.HasOverride )
					ret = this.layout.Override.AllowRowLayoutColMoving;

				if ( Infragistics.Win.Layout.GridBagLayoutAllowMoving.Default == ret && this.HasOverride )
				{
					AllowColMoving allowColMoving = this.overrideObj.AllowColMoving;
					if (allowColMoving == AllowColMoving.WithinBand ||
						allowColMoving == AllowColMoving.WithinGroup)
					{
						return Infragistics.Win.Layout.GridBagLayoutAllowMoving.AllowAll;
					}
				}

				if ( Infragistics.Win.Layout.GridBagLayoutAllowMoving.Default == ret && this.layout.HasOverride )
				{
					AllowColMoving allowColMoving = this.layout.Override.AllowColMoving;
					if (allowColMoving == AllowColMoving.WithinBand ||
						allowColMoving == AllowColMoving.WithinGroup)
					{
						return Infragistics.Win.Layout.GridBagLayoutAllowMoving.AllowAll;
					}
				}

				return (Infragistics.Win.Layout.GridBagLayoutAllowMoving.Default != ret) ? ret : Infragistics.Win.Layout.GridBagLayoutAllowMoving.None;
			}			
		}		

		#endregion AllowRowLayoutColMovingResolved

		#region AllowRowLayoutCellSpanSizingResolved

		internal Layout.GridBagLayoutAllowSpanSizing AllowRowLayoutCellSpanSizingResolved
		{
			get
			{
				// MRS 4/11/05 - Allow Sizing at Design-time. 
				if (null != this.Layout &&
					null != this.layout.Grid &&
					this.Layout.Grid.DesignMode)
				{
					return Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.AllowAll;
				}
				
				// MRS 4/21/05 - Only allow this for the grid. 
				if (null != this.Layout &&
					!(this.layout.Grid is UltraGrid)) 
				{
					return Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.None;
				}

				Layout.GridBagLayoutAllowSpanSizing ret = Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.Default;
				if ( this.HasOverride )
					ret = this.overrideObj.AllowRowLayoutCellSpanSizing;

				if ( Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.Default == ret && this.layout.HasOverride )
					ret = this.layout.Override.AllowRowLayoutCellSpanSizing;

				return Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.Default != ret ? ret : Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.None;
			}			
		}

		#endregion // ResetAllowRowLayoutCellSpanSizingResolved

		#region AllowRowLayoutLabelSpanSizingResolved

		internal Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing AllowRowLayoutLabelSpanSizingResolved
		{
			get
			{
				// MRS 4/11/05 - Allow Sizing at Design-time. 
				if (null != this.Layout &&
					null != this.layout.Grid &&
					this.Layout.Grid.DesignMode)
				{
					return Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.AllowAll;
				}

				// MRS 4/21/05 - Only allow this for the grid. 
				if (null != this.Layout &&
					!(this.layout.Grid is UltraGrid)) 
				{
					return Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.None;
				}

				Layout.GridBagLayoutAllowSpanSizing ret = Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.Default;
				if ( this.HasOverride )
					ret = this.overrideObj.AllowRowLayoutLabelSpanSizing;

				if ( Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.Default == ret && this.layout.HasOverride )
					ret = this.layout.Override.AllowRowLayoutLabelSpanSizing;

				return Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.Default != ret ? ret : Infragistics.Win.Layout.GridBagLayoutAllowSpanSizing.None;
			}			
		}

		#endregion // ResetAllowRowLayoutLabelSpanSizingResolved

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// Added ExcludeFromColumnChooser property.
		// 
		#region ExcludeFromColumnChooser

		/// <summary>
		/// Forces the band to be excluded from the column chooser control. Default is resolved to <b>False</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Set the <b>ExcludeFromColumnChooser</b> property of a band to <b>True</b> if you want to
		/// exlude it from the column chooser. Excluding a band from the column chooser will cause
		/// the <see cref="UltraGridColumnChooser"/> control to not display the band and it's columns.
		/// This effectively prevents the user from hiding or unhding the band and it's columns via
		/// the column chooser.
		/// </p>
		/// <p class="body">
		/// The Column object also exposes <see cref="UltraGridColumn.ExcludeFromColumnChooser"/> property that lets
		/// you exlcude individual columns from the column chooser.
		/// </p>
		/// <seealso cref="UltraGridColumn.ExcludeFromColumnChooser"/>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// <seealso cref="ColumnChooserDialog"/>
		/// <seealso cref="UltraGridBase.ShowColumnChooser()"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBand_P_ExcludeFromColumnChooser")]
        public ExcludeFromColumnChooser ExcludeFromColumnChooser
		{
			get
			{
				return this.excludeFromColumnChooser;
			}
			set
			{
				if ( value != this.excludeFromColumnChooser )
				{
					GridUtils.ValidateEnum( typeof( ExcludeFromColumnChooser ), value );

					this.excludeFromColumnChooser = value;

					this.NotifyPropChange( PropertyIds.ExcludeFromColumnChooser );
				}
			}
		}

		#endregion // ExcludeFromColumnChooser

		#region ShouldSerializeExcludeFromColumnChooser

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeExcludeFromColumnChooser( )
		{
			return ExcludeFromColumnChooser.Default != this.excludeFromColumnChooser;
		}

		#endregion // ShouldSerializeExcludeFromColumnChooser

		#region ResetExcludeFromColumnChooser

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetExcludeFromColumnChooser( )
		{
			this.ExcludeFromColumnChooser = ExcludeFromColumnChooser.Default;
		}

		#endregion // ResetExcludeFromColumnChooser

		// SSP 7/14/05 - NAS 5.3 Header Placement
		// 
		#region NAS 5.3 Header Placement

		#region HeaderPlacementResolved

		/// <summary>
		/// Returns the resolved header placement.
		/// </summary>
        // MRS v7.2 - Document Exporter
		//internal HeaderPlacement HeaderPlacementResolved
        //
        // MBS 7/27/07 - BR25195
        [Browsable(false)]

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
        public HeaderPlacement HeaderPlacementResolved
		{
			get
			{
				// SingeBand or Horizontal view styles do not support header placement. They are
				// always fixed on top. There is no reason to support header placement in those
				// view styles due to their nature (in the case of the horizontal) and in the
				// case of the single band, the same can be accomplished using the vertical multi
				// band.
				// 
				ViewStyleBase viewStyle = null != this.Layout ? this.Layout.ViewStyleImpl : null;
				if ( null != viewStyle && ( ! viewStyle.IsMultiBandDisplay || viewStyle.HasMultiRowTiers ) )
					return HeaderPlacement.FixedOnTop;

				HeaderPlacement ret = this.HasOverride ? this.overrideObj.HeaderPlacement : HeaderPlacement.Default;

				if ( HeaderPlacement.Default == ret && this.layout.HasOverride )
					ret = this.layout.Override.HeaderPlacement;

				if ( HeaderPlacement.Default == ret )
					ret = HeaderPlacement.RepeatOnBreak;

				return ret;
			}
		}

		#endregion // HeaderPlacementResolved

		#region HeadersAreaVisible

		internal bool HeadersAreaVisible
		{
			get
			{
				return this.GetTotalHeaderHeight( ) > 0;
			}
		}

		#endregion // HeadersAreaVisible

		#endregion // NAS 5.3 Header Placement

		// SSP 7/29/05 - NAS 5.3 Tab Index
		// 
		#region NAS 5.3 Tab Index

		#region TabIndexComparer Class

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private class TabIndexComparer : IComparer
		private class TabIndexComparer : IComparer<UltraGridColumn>
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//private ArrayList defaultOrderColumns = null;
			private List<UltraGridColumn> defaultOrderColumns = null;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//internal TabIndexComparer( ArrayList defaultOrderColumns )
			internal TabIndexComparer( List<UltraGridColumn> defaultOrderColumns )
			{
				this.defaultOrderColumns = defaultOrderColumns;
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//public int Compare( object x, object y )
			//{
			//    UltraGridColumn xxCol = (UltraGridColumn)x;
			//    UltraGridColumn yyCol = (UltraGridColumn)y;
			public int Compare( UltraGridColumn xxCol, UltraGridColumn yyCol )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//UltraGridBand band = xxCol.Band;

				int xxTabIndex = xxCol.TabIndex;
				int yyTabIndex = yyCol.TabIndex;
				if ( xxTabIndex != yyTabIndex )
				{
					if ( xxTabIndex >= 0 && yyTabIndex >= 0 )
						return xxTabIndex.CompareTo( yyTabIndex );

					// Give columns with tab index specified higher priority than the columns without
					// tab index specified.
					// 
					if ( xxTabIndex >= 0 )
						return -1;

					if ( yyTabIndex >= 0 )
						return 1;
				}

				// If tab index are not specified or they are the same then fallback to the default
				// column order.
				// 
				xxTabIndex = this.defaultOrderColumns.IndexOf( xxCol );
				yyTabIndex = this.defaultOrderColumns.IndexOf( yyCol );

				// If the item is not in the list for some reason then give it the least priority.
				// 
				if ( xxTabIndex < 0 )
					xxTabIndex = int.MaxValue;

				if ( yyTabIndex < 0 )
					yyTabIndex = int.MaxValue;

				return xxTabIndex.CompareTo( yyTabIndex );
			}
		}

		#endregion // TabIndexComparer Class

		#region VerifyTabOrderedColumns

		private void VerifyTabOrderedColumns( )
		{
			if ( null == this.tabOrderedColumns )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = new ArrayList( );
				List<UltraGridColumn> list = new List<UltraGridColumn>();

				foreach ( HeaderBase header in this.OrderedColumnHeaders )
				{
					UltraGridColumn column = header.Column;

					if ( ((ILayoutItem)column).IsVisible
						// MD 1/19/09 - Groups in RowLayout
						// This logic is no logner valid in GroupLayout style. Use the IsVisibleInLayout property to determine if the column will be shown.
						//&& ( ! column.Band.GroupsDisplayed || null != column.Group && ! column.Group.Hidden ) )
						&& column.IsVisibleInLayout )
					{
						list.Add( column );
					}
				}

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList defaultOrderColumns = new ArrayList( );
				List<UltraGridColumn> defaultOrderColumns = new List<UltraGridColumn>();

				UltraGridColumn col = this.GetRelatedVisibleCol( null, NavigateType.First, true, false );
				while ( null != col )
				{
					defaultOrderColumns.Add( col );
					col = this.GetRelatedVisibleCol( col, NavigateType.Next, true, false );
				}

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//UltraGridColumn[] columnsArr = (UltraGridColumn[])list.ToArray( typeof( UltraGridColumn ) );
				UltraGridColumn[] columnsArr = list.ToArray();

				// MD 8/13/07 - 7.3 Performance
				// Use generics
				//Infragistics.Win.Utilities.SortMerge( columnsArr, new TabIndexComparer( defaultOrderColumns ) );
				Utilities.SortMergeGeneric<UltraGridColumn>( columnsArr, new TabIndexComparer( defaultOrderColumns ) );

				if ( null == this.tabOrderedColumns )
					this.tabOrderedColumns = new HeadersCollection( columnsArr.Length );

				this.tabOrderedColumns.InternalClear( );

				foreach ( UltraGridColumn column in columnsArr )
					this.tabOrderedColumns.InternalAdd( column.Header );
			}
		}

		#endregion // VerifyTabOrderedColumns

		#region DirtyTabOrderedColumns

		internal void DirtyTabOrderedColumns( )
		{
			this.tabOrderedColumns = null;
		}

		#endregion // DirtyTabOrderedColumns

		#region TabOrderedColumns

		internal HeadersCollection TabOrderedColumns
		{
			get
			{
				this.VerifyTabOrderedColumns( );
				return this.tabOrderedColumns;
			}
		}

		#endregion // TabOrderedColumns

		#endregion // NAS 5.3 Tab Index

		// SSP 11/20/05 - NAS 6.1 Multi-cell Operations
		// 
		#region NAS 6.1 Multi-cell Operations

		#region AllowMultiCellOperationsResolved

		internal AllowMultiCellOperation AllowMultiCellOperationsResolved
		{
			get
			{
				AllowMultiCellOperation ret = this.HasOverride 
					? this.overrideObj.AllowMultiCellOperations 
					: AllowMultiCellOperation.Default;

				if ( AllowMultiCellOperation.Default == ret && this.layout.HasOverride )
					ret = this.layout.Override.AllowMultiCellOperations;

				if ( AllowMultiCellOperation.Default == ret )
					ret = AllowMultiCellOperation.None;

				return ret;
			}
		}

		#endregion // AllowMultiCellOperationsResolved

		#region IsMultiCellOperationAllowed

		internal bool IsMultiCellOperationAllowed( MultiCellOperation operation )
		{
			AllowMultiCellOperation operationsAllowed = this.AllowMultiCellOperationsResolved;

			bool ret = false;

			switch ( operation )
			{
				case MultiCellOperation.Copy:
					ret = 0 != ( ( AllowMultiCellOperation.Copy | AllowMultiCellOperation.CopyWithHeaders ) & operationsAllowed );
					break;
				case MultiCellOperation.Cut:
					ret = 0 != ( AllowMultiCellOperation.Cut & operationsAllowed );
					break;
				case MultiCellOperation.Delete:
					ret = 0 != ( AllowMultiCellOperation.Delete & operationsAllowed );
					break;
				case MultiCellOperation.Paste:
					ret = 0 != ( AllowMultiCellOperation.Paste & operationsAllowed );
					break;
				case MultiCellOperation.Redo:
					ret = 0 != ( AllowMultiCellOperation.Redo & operationsAllowed );
					break;
				case MultiCellOperation.Undo:
					ret = 0 != ( AllowMultiCellOperation.Undo & operationsAllowed );
					break;
				default:
					Debug.Assert( false, "Unknown type of clipboard operation." );
					break;
			}

			return ret;
		}

		#endregion // IsMultiCellOperationAllowed

		#endregion // NAS 6.1 Multi-cell Operations

		// SSP 12/13/05 - Recursive Row Enumerator
		// 
		#region GetRowEnumerator

		/// <summary>
		/// Gets the rows associated with this band.
		/// </summary>
		/// <param name="rowTypes">Whether to get data rows or group-by rows or both.</param>
		/// <returns>Returns an IEnumerable instance that can be used to enumerate all the
		/// rows associated with this band.</returns>
		/// <remarks>
		/// <seealso cref="RowsCollection.GetRowEnumerator"/>
		/// </remarks>
		public IEnumerable GetRowEnumerator( GridRowType rowTypes )
		{
			return this.Layout.Rows.GetRowEnumerator( rowTypes, this, this );
		}

		#endregion // GetRowEnumerator

		#region MultiCellSelectionModeResolved

		// SSP 2/24/06 BR09715
		// Added MultiCellSelectionMode property to support snaking cell selection..
		// 
		internal MultiCellSelectionMode MultiCellSelectionModeResolved
		{
			get
			{
				MultiCellSelectionMode ret = this.HasOverride 
					? this.overrideObj.MultiCellSelectionMode
					: MultiCellSelectionMode.Default;

				if ( MultiCellSelectionMode.Default == ret && this.layout.HasOverride )
					ret = this.layout.Override.MultiCellSelectionMode;

				if ( MultiCellSelectionMode.Default == ret )
					ret = MultiCellSelectionMode.Standard;

				return ret;
			}
		}

		#endregion // MultiCellSelectionModeResolved

		#region SnakeCellSelectionResolved

		// SSP 2/24/06 BR09715
		// Added MultiCellSelectionMode property to support snaking cell selection..
		// 
		internal bool SnakeCellSelectionResolved
		{
			get
			{
				// If the MultiCellSelectionMode is set to Snaking then return true.
				// 
				MultiCellSelectionMode val = this.MultiCellSelectionModeResolved;
				if ( MultiCellSelectionMode.Snaking == val )
					return true;
				
				// Otherwise default to the internal Snaking property of the layout which
				// gets set as part of the selection strategy snaking logic (as well as
				// the grid itself).
				// 
				return this.Layout.Snaking;
			}
		}

		#endregion // SnakeCellSelectionResolved

        // MBS 2/6/08 - RowEditTemplate NA2008 V2
        #region RowEditTemplate

        
        //
        /// <summary>
        /// Gets or sets the template used for editing rows in the band.
        /// </summary>
        [Editor(typeof(RowEditTemplateUITypeEditor), typeof(UITypeEditor))]
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridBand_P_RowEditTemplate")]                
        public UltraGridRowEditTemplate RowEditTemplate
        {
            get 
            {
                if (this.rowEditTemplate != null)
                    return this.rowEditTemplate;

                // Check to see if we've serialized the name of the control
                if (this.rowEditTemplateControlName != null)
                {
                    UltraGridRowEditTemplate template = this.SearchForTemplateWithName(this.rowEditTemplateControlName);

                    // Only assign the template to this band if it is not already associated with another band.
                    if (template != null && (template.Band == null || template.Band == this))
                        this.rowEditTemplate = template;
                    
                    // Regardless of whether we could find the control or not, we don't want to walk up 
                    // the control chain every time that this property is requested.  If a customer
                    // loads a layout before the control exists on the form, then it is up to the customer
                    // to set the property of the band back to that template
                    this.rowEditTemplateControlName = null;
                }

                return this.rowEditTemplate;
            }
            set
            {
                if (this.rowEditTemplate == value)
                    return;

                // The user cannot use the same template on different bands
                if (value != null && value.Band != null && value.Band != this)
                    throw new InvalidOperationException(SR.GetString("LE_UltraGridRowEditTemplate_SetToMultipleBands"));

                if (this.rowEditTemplate != null)
                {
                    if (this.rowEditTemplate.IsShown)
                        throw new InvalidOperationException(SR.GetString("LE_UltraGridRowOrBand_SetTemplateWhileShown"));

                    // If we already have a different RowEditTemplate assigned to the band, we need to clear
                    // it out since it will no longer be associated with this band.
                    this.rowEditTemplate.Initialize(null);
                }

                this.rowEditTemplate = value;

                if (this.rowEditTemplate != null)
                    this.rowEditTemplate.Initialize(this);

                this.NotifyPropChange(PropertyIds.RowEditTemplate);
            }
        }

        /// <summary>
        /// Returns whether the RowEditTemplate property is set to a non-default value.
        /// </summary>
        /// <returns>True if the property is set to a non-default value.</returns>
        protected bool ShouldSerializeRowEditTemplate()
        {
            // MBS 9/22/08 - TFS6432
            // It's possible that we don't actually have an instance of the template at this point, such
            // as if we're loading and saving a layout but we don't actually hit a point where we try to
            // locate the template by its name, so we should serialize the name if we have it
            //
            //return this.rowEditTemplate != null;
            return this.rowEditTemplate != null || String.IsNullOrEmpty(this.rowEditTemplateControlName) == false;
        }

        /// <summary>
        /// Resets the RowEditTemplate property to its default value
        /// </summary>
        public void ResetRowEditTemplate()
        {
            this.RowEditTemplate = null;
        }
        #endregion //RowEditTemplate
        //
        #region RowEditTemplateUITypeResolvedDefault

        private RowEditTemplateUIType RowEditTemplateUITypeResolvedDefault
        {
            get
            {
                if (this.HasOverride &&
                    this.overrideObj.ShouldSerializeRowEditTemplateUIType())
                    return this.overrideObj.RowEditTemplateUIType;

                if (this.layout.HasOverride
                    && this.layout.Override.ShouldSerializeRowEditTemplateUIType())
                    return this.layout.Override.RowEditTemplateUIType;

                return RowEditTemplateUIType.Default;
            }
        }
        #endregion //RowEditTemplateUITypeResolvedDefault
        //
        #region RowEditTemplateUITypeResolved

        internal RowEditTemplateUIType RowEditTemplateUITypeResolved
        {
            get
            {
                RowEditTemplateUIType defaultType = this.RowEditTemplateUITypeResolvedDefault;                
                if (defaultType == RowEditTemplateUIType.Default)
                {
                    // We will only show the icons in the row selectors by default when there
                    // is a RowEditTemplate on the band.  If the user always wants to show 
                    // the buttons, then the UIType can be explicitly set.
                    if (this.RowEditTemplate != null)
                        return RowEditTemplateUIType.OnEnterEditMode | RowEditTemplateUIType.RowSelectorImage;

                    // We don't want to default to None because we still want to provide the
                    // user the option of specifying a template when the grid requests it.
                    return RowEditTemplateUIType.OnEnterEditMode;
                }
                return defaultType;                
            }
        }
        #endregion //RowEditTemplateUITypeResolved
        //
        #region SearchForTemplateWithName

        // MBS 5/16/08 - Copied methods for finding a dropdown with a specified name from Column.cs
        private UltraGridRowEditTemplate SearchForTemplateWithName(string ctrlName)
        {
            UltraGridRowEditTemplate template = null;

            try
            {
                // MRS 9/12/06 - BR14893
                // Before we search from the form down, First search from the control up.                 
                UltraGridBase gridBase = null != this.Layout ? this.Layout.Grid : null;
                if (gridBase != null)
                {
                    Control control = gridBase.Parent;
                    while (control != null)
                    {
                        if (control.Controls != null)
                            template = this.SearchForTemplateWithName(control.Controls, ctrlName, false);

                        if (template != null)
                            return template;

                        control = control.Parent;
                    }
                }

                // SSP 12/28/05 BR08374
                // If the grid is part of a user control at design time then there won't be any form.
                // Search the user control instead.
                // 
                // --------------------------------------------------------------------------------------
                //System.Windows.Forms.Form form = this.band.Layout.Grid.FindForm();
                //dropdown = this.SearchForDropDownWithName( form.Controls, ctrlName );
                Control rootCtrl = GridUtils.GetRootControlForSearching(null != this.Layout ? this.Layout.Grid : null);
                if (null != rootCtrl)
                    // MRS 9/12/06 - Added recursize param
                    //dropdown = this.SearchForDropDownWithName( rootCtrl.Controls, ctrlName );
                    template = this.SearchForTemplateWithName(rootCtrl.Controls, ctrlName, true);
                // --------------------------------------------------------------------------------------
            }
            catch (Exception)
            {
            }

            return template;
        }

        private UltraGridRowEditTemplate SearchForTemplateWithName(Control.ControlCollection controls, string name, bool recursive)
        {
            if (controls == null)
                return null;

            UltraGridRowEditTemplate template = null;

            foreach (System.Windows.Forms.Control ctrl in controls)
            {
                if (ctrl is UltraGridRowEditTemplate && ctrl.Name.Equals(name))
                    return (UltraGridRowEditTemplate)ctrl;
                else if (ctrl.Controls.Count > 0 && recursive)
                {
                    template = SearchForTemplateWithName(ctrl.Controls, name, true);

                    if (template != null)
                        break;
                }
            }

            return template;
        }
        #endregion //SearchForTemplateWithName

        // MBS 5/6/08 - RowEditTemplate NA2008 V2
        #region RowEditTemplateRowSelectorImageDefault

        private Image RowEditTemplateRowSelectorImageDefault
        {
            get
            {
                if (this.rowEditTemplateRowSelectorImageDefault == null)
                    this.rowEditTemplateRowSelectorImageDefault = UltraGridLayout.GetImageFromResourse("Images.UltraGridRowEditTemplateLauncher.png");

                return this.rowEditTemplateRowSelectorImageDefault;
            }
        }
        #endregion //RowEditTemplateRowSelectorImageDefault
        //
        #region RowEditTemplateRowSelectorImageResolved

        internal Image RowEditTemplateRowSelectorImageResolved
        {
            get
            {
                object objVal;
                if (!StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.RowEditTemplateRowSelectorImage, out objVal))
                {
                    Image val = null != this.overrideObj
                        ? this.overrideObj.RowEditTemplateRowSelectorImage : null;

                    if (null == val && this.layout.HasOverride)
                        val = this.layout.Override.RowEditTemplateRowSelectorImage;

                    objVal = StyleUtils.CachePropertyValue(this,
                        StyleUtils.CustomProperty.RowEditTemplateRowSelectorImage, val, null, this.RowEditTemplateRowSelectorImageDefault);
                }
                return (Image)objVal;
            }
        }
        #endregion //RowEditTemplateRowSelectorImageResolvedDefault

        // MRS NAS v8.2 - CardView Printing
        #region NAS v8.2 - CardView Printing

        #region PerformAutoResizeColumns

        /// <summary>
        /// Resizes all columns in the band based on cell values in either currently visible rows or all rows depending on the value of autoSizeType argument.
        /// </summary>
        /// <param name="autoSizeType"></param>
        /// <param name="sizeHiddenColumns">True to resize columns that are hidden; otherwise false.</param>
        public void PerformAutoResizeColumns(bool sizeHiddenColumns, PerformAutoSizeType autoSizeType)
        {
            this.PerformAutoResizeColumns(sizeHiddenColumns, autoSizeType, true);
        }

        /// <summary>
        /// Resizes all columns in the band based on cell values in either currently visible rows or all rows depending on the value of autoSizeType argument.
        /// </summary>
        /// <param name="autoSizeType">Specifies if and which rows to base the auto-sizing of the column.</param>
        /// <param name="includeHeader">Specifies whether to include header caption width into the auto-size.</param>
        /// <param name="sizeHiddenColumns">True to resize columns that are hidden; otherwise false.</param>
        public void PerformAutoResizeColumns(bool sizeHiddenColumns, PerformAutoSizeType autoSizeType, bool includeHeader)
        {            
            this.PerformAutoResizeHelper(sizeHiddenColumns, autoSizeType, includeHeader);
        }

        #region PerformAutoResizeHelper

        private void PerformAutoResizeHelper(bool sizeHiddenColumns, PerformAutoSizeType autoSizeType, bool includeHeader)
        {
            if (this.CardView)
                return;

            if (this.HiddenResolved && !sizeHiddenColumns)
                return;

            foreach (UltraGridColumn column in this.Columns)
            {
                if (column.IsChaptered == false &&
                    (sizeHiddenColumns || column.HiddenResolved == false))
                {
                    column.PerformAutoResize(autoSizeType, includeHeader);
                }
            }
        }
        #endregion //PerformAutoResizeHelper

        #endregion //PerformAutoResizeColumns

        #region PerformAutoResizeCards
        /// <summary>
        /// Resizes the width of all cards in the band based on cell values in all rows.
        /// </summary>
        /// <param name="includeHiddenColumns"></param>
        public void PerformAutoResizeCards(bool includeHiddenColumns)
        {
            if (this.CardView == false)
                return;

            if (this.HiddenResolved && !includeHiddenColumns)
                return;

            int maxWidth = 0;
            if (includeHiddenColumns || this.HiddenResolved == false)
            {
                foreach (UltraGridColumn column in this.Columns)
                {
                    if (column.IsChaptered == false &&
                        // MRS 2/23/2009 - Found while fixing TFS14354
                        //(includeHiddenColumns || column.Hidden == false))
                        (includeHiddenColumns || column.HiddenResolved == false))
                    {
                        // MRS 5/13/2008 - BR32739
                        // Doesn't make sense to include the header here. 
                        //int colWidth = column.CalculateAutoResizeWidth(PerformAutoSizeType.AllRowsInBand, true);
                        int colWidth = column.CalculateAutoResizeWidth(PerformAutoSizeType.AllRowsInBand, false);

                        maxWidth = Math.Max(maxWidth, colWidth);
                    }
                }
            }

            // MRS 5/13/2008 - BR32739
            if (this.CardSettings.StyleResolved != CardStyle.MergedLabels)
            {
                maxWidth += this.CardLabelWidthResolvedHelper(false);
            }

            this.CardSettings.Width = maxWidth;
        }
        #endregion //PerformAutoResizeCards

        #endregion //NAS v8.2 - CardView Printing

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		#region 8.2 - Rotated Column Headers

		#region ColumnHeaderTextOrientationResolved

		internal TextOrientationInfo ColumnHeaderTextOrientationResolved
		{
			get
			{
				if ( this.overrideObj != null && this.overrideObj.ShouldSerializeColumnHeaderTextOrientation() )
					return this.overrideObj.ColumnHeaderTextOrientation;

				if ( this.layout.HasOverride && this.layout.Override.ShouldSerializeColumnHeaderTextOrientation() )
					return this.layout.Override.ColumnHeaderTextOrientation;

				return null;
			}
		}

		#endregion ColumnHeaderTextOrientationResolved

		#region GroupHeaderTextOrientationResolved

		internal TextOrientationInfo GroupHeaderTextOrientationResolved
		{
			get
			{
				if ( this.overrideObj != null && this.overrideObj.ShouldSerializeGroupHeaderTextOrientation() )
					return this.overrideObj.GroupHeaderTextOrientation;

				if ( this.layout.HasOverride && this.layout.Override.ShouldSerializeGroupHeaderTextOrientation() )
					return this.layout.Override.GroupHeaderTextOrientation;

				return null;
			}
		}

		#endregion GroupHeaderTextOrientationResolved 

		#endregion 8.2 - Rotated Column Headers

        // MRS NAS 9.1 - Sibling Band Order
        #region NAS 9.1 - Sibling Band Order

        #region VisiblePosition
        /// <summary>
        /// Determines the position of the band relative to its siblings.
        /// </summary>
        ///	<remarks>
        ///	<p class="body">The <b>VisiblePosition</b> property determines the order in which sibling bands are displayed relative to each other. The band with the lowest VisiblePosition will be displayed first and then it's siblings will be displayed in ascending numeric order. If two bands have the same VisiblePosition, then the order will be determined by the Index of the bands in the <see cref="UltraGridLayout.Bands"/> collection.</p>
        /// <p>When RowLayoutStyle is not set to None, the VisiblePosition property will be overridden by the column�s RowLayoutColumnInfo.OriginX property. If the OriginX is not set, it will be resolved relative to the other columns in the band or group based on the VisiblePosition of the column. </p>
        ///	</remarks>
        [LocalizedDescription("LD_UltraGridBand_P_VisiblePosition")]
        [LocalizedCategory("LC_Display")]
        public int VisiblePosition
        {
            get
            {
                return this.visiblePosition;
            }
            set
            {
                if (this.visiblePosition != value)
                {
                    this.visiblePosition = value;
                    this.NotifyPropChange(PropertyIds.VisiblePosition, null);
                }
            }
        }

        /// <summary>
        /// Returns true if this property is not set to its default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeVisiblePosition()
        {
            return this.visiblePosition != 0;
        }

        /// <summary>
        /// Resets VisiblePosition to its default value (0).
        /// </summary>
        public void ResetVisiblePosition()
        {
            this.VisiblePosition = 0;
        }

        #endregion // VisiblePosition

        /// <summary>
        /// Number that specifies the band's position in the SortedBands collection.
        /// </summary>
        ///	<remarks>
        ///	<p class="body">The <b>SortedIndex</b> property returns the index of the band in the SortedBands collection.</p>
        ///	</remarks>
        ///	<see cref="Index"/>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [MergableProperty(false)]
        [LocalizedDescription("LD_UltraGridBand_P_SortedIndex")]        
        [Browsable(false)]
        public int SortedIndex
        {
            get
            {                
                return this.layout.SortedBands.IndexOf(this);
            }
        }

        #endregion //NAS 9.1 - Sibling Band Order

        // MBS 11/4/08 - TFS9064
        #region ResetCachedList

        internal void ResetCachedList()
        {
            this.cachedList = null;
        }
        #endregion //ResetCachedList

        // CDS NAS v9.1 Header CheckBox
        #region NA v9.1 Header CheckBox
        
        private Dictionary<UltraGridColumn, CheckState> checkStateCache = new Dictionary<UltraGridColumn, CheckState>();
        internal Dictionary<UltraGridColumn, bool> checkStateCacheValidHashTable = new Dictionary<UltraGridColumn, bool>();

        // CDS 7/07/09 - TFS17996
        private bool isCachedContainsCheckboxOnTopOrBottomValid = false;
        private bool cachedContainsCheckboxOnTopOrBottom = false;

        #region GetHeaderCheckState

        internal CheckState GetHeaderCheckState(UltraGridColumn column)
        {
            if (!this.IsCachedCheckStateValid(column))
            {
                //get the value and put it in the cache.
                CheckState state = column.GetHeaderCheckedStateBasedOnCellValues(null);
                // CDS 2/26/09 TFS14684 Don't want to synchronize the cells since we just obtained the value from the cells
                //this.SetCheckStateCache(column, state, false);
                this.SetCheckStateCache(column, state, false, false); 
            }
            return (CheckState)this.checkStateCache[column];
        }

        #endregion GetHeaderCheckState

        #region IsCachedCheckStateValid

        private bool IsCachedCheckStateValid(UltraGridColumn column)
        {
            if (!this.checkStateCacheValidHashTable.ContainsKey(column))
                return false;
            return (bool)this.checkStateCacheValidHashTable[column];
        }

        #endregion IsCachedCheckStateValid

        #region DirtyCheckStateCacheValidEntry

        internal void DirtyCheckStateCacheValidEntry(UltraGridColumn column)
        {
            // CDS 01/23/ 09 If column is null, clear the entire dictionary.
            if (column == null)
                this.checkStateCacheValidHashTable.Clear();
            else
                this.checkStateCacheValidHashTable[column] = false;
        }

        #endregion DirtyCheckStateCacheValidEntry

        #region SetCheckStateCache

        // CDS 2/26/09 TFS14684
        //internal void SetCheckStateCache(UltraGridColumn column, CheckState state, bool isCancelable)
        internal void SetCheckStateCache(UltraGridColumn column, CheckState state, bool isCancelable, bool synchronize)
        {
            // CDS 03/26/09 TFS14747 Only need to take the active cell out of EditMode if we are going to synchronize
            if (synchronize)
            {
                UltraGrid grid = this.Layout.Grid as UltraGrid;

                // CDS 2/02/09 TFS12512 The Grid property is not an UltraGrid for the UltraCombo, so we need an null check.
                //if ( null != grid.ActiveCell &&
                if (null != grid &&
                    null != grid.ActiveCell &&
                    grid.ActiveCell.IsInEditMode &&
                    grid.ActiveCell.ExitEditMode(false, false))
                    return;
            }
            
            System.Windows.Forms.CheckState newCheckState = state;

            // check to see if the column exists
            bool columnCheckStateExists = this.checkStateCache.ContainsKey(column);

            if (!columnCheckStateExists ||
                (CheckState)this.checkStateCache[column] != newCheckState)
            {
                // If it doesn't exist or the state is different, fire the Before event

                // CDS 2/02/09 TFS12512 Events have been moved to the base class so they will fire for the UltraCombo.
                UltraGridBase gridBase = this.Layout.Grid as UltraGridBase;

                // Fire the BeforeHeaderCheckStateChanged event
                BeforeHeaderCheckStateChangedEventArgs args = new BeforeHeaderCheckStateChangedEventArgs(column, null, state, columnCheckStateExists ? (CheckState)this.checkStateCache[column] : CheckState.Indeterminate, isCancelable);
                // CDS 2/02/09 TFS12512 Changed to use the base class
                //grid.FireEvent(GridEventIds.BeforeHeaderCheckStateChanged, args);
                gridBase.FireCommonEvent(CommonEventIds.BeforeHeaderCheckStateChanged, args);

                // Check for cancelling
                if (args.Cancel &&
                    isCancelable)
                    return;

                newCheckState = args.NewCheckState;

                this.checkStateCache[column] = newCheckState;
                // CDS 2/24/09 TFS12452 - Unrelated to actual bug, but needed fixing. Need to mark the cached CheckState 
                // as valid outside this if statement. Otherwise, we will re-synch whenever the header is painted and the synched
                // CheckState matches the one saved one (even though it was marked as not valid).
                //this.checkStateCacheValidHashTable[column] = true;

                // Force the grid to repaint, as it possible for Header on the same band but seperated by children may not be forced to repaint.
                // Optimize this at a later date.
                // CDS 3/26/08 Invalidate only the HeaderUIElements instead of the whole grid
                //// CDS 2/02/09 TFS12512 Changed to use the base class
                ////grid.Invalidate();
                //gridBase.Invalidate();
                column.DirtyHeaderUIElements(null);
                
                // CDS 6/18/09 TFS17819 Moved the synchronization to prior to the After event firing
                // Synchronize if necessary
                if (synchronize &&
                    column.HeaderCheckBoxSynchronizationResolved == HeaderCheckBoxSynchronization.Band &&
                    newCheckState != CheckState.Indeterminate)
                    column.SetCellValuesBasedOnCheckState(null, newCheckState);

                // Fire the AfterHeaderCheckStateChanged event
                // CDS 2/02/09 TFS12512 Changed to use the base class
                //grid.FireEvent(GridEventIds.AfterHeaderCheckStateChanged, new AfterHeaderCheckStateChangedEventArgs(column, null));
                gridBase.FireCommonEvent(CommonEventIds.AfterHeaderCheckStateChanged, new AfterHeaderCheckStateChangedEventArgs(column, null));
            }

            // CDS 2/24/09 TFS12452 Moved from inside the if statement, to make sure the cached value is marked valid
            this.checkStateCacheValidHashTable[column] = true;

            // CDS 6/18/09 TFS17819 Moved the synchronization to prior to the After event firing
            //// CDS 2/26/09 TFS14684 Jump out if we don't intend to synchronize
            //if (!synchronize)
            //    return;

            //// Synchronize if necessary
            //if (column.HeaderCheckBoxSynchronizationResolved == HeaderCheckBoxSynchronization.Band &&
            //    newCheckState != CheckState.Indeterminate)
            //    column.SetCellValuesBasedOnCheckState(null, newCheckState);
        }

        #endregion

        #region ContainsColumnWithVisibleHeaderCheckBoxOnTopOrBottom

        internal bool ContainsColumnWithVisibleHeaderCheckBoxOnTopOrBottom
        {
            get
            {
                // CDS 7/07/09 TFS17996 Reworked to cache the value, instead of looping through all the columns every time.
                //foreach (UltraGridColumn column in this.Columns)
                //{
                //    HeaderCheckBoxAlignment resolvedAlignment = column.HeaderCheckBoxAlignmentResolved;
                //    if (!column.HiddenResolved &&
                //        column.IsHeaderCheckBoxVisible &&
                //        (resolvedAlignment == HeaderCheckBoxAlignment.Top ||
                //        resolvedAlignment == HeaderCheckBoxAlignment.Bottom))
                //        return true;
                //}
                //return false;
                if (this.isCachedContainsCheckboxOnTopOrBottomValid == false)
                {
                    bool foundFlag = false;
                    foreach (UltraGridColumn column in this.Columns)
                    {
                        HeaderCheckBoxAlignment resolvedAlignment = column.HeaderCheckBoxAlignmentResolved;
                        if (!column.HiddenResolved &&
                            !column.Header.HiddenResolved &&
                            column.IsHeaderCheckBoxVisible &&
                            (resolvedAlignment == HeaderCheckBoxAlignment.Top ||
                            resolvedAlignment == HeaderCheckBoxAlignment.Bottom))
                        {
                            foundFlag = true;
                            break;
                        }
                    }
                    this.cachedContainsCheckboxOnTopOrBottom = foundFlag;
                    this.isCachedContainsCheckboxOnTopOrBottomValid = true;
                }
                return this.cachedContainsCheckboxOnTopOrBottom;
            }
        }

        #endregion ContainsColumnWithVisibleHeaderCheckBoxOnTopOrBottom

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxVisibilityResolved

        internal HeaderCheckBoxVisibility HeaderCheckBoxVisibilityResolved
        {
            get
            {
                if (this.HasOverride &&
                    this.Override.ShouldSerializeHeaderCheckBoxVisibility())
                    return this.Override.HeaderCheckBoxVisibility;

                if (this.Layout !=null &&
                    this.Layout.HasOverride &&
                    this.Layout.Override.ShouldSerializeHeaderCheckBoxVisibility())
                    return this.Layout.Override.HeaderCheckBoxVisibility;

                return HeaderCheckBoxVisibility.Never;
            }
        }

        #endregion HeaderCheckBoxVisibilityResolved

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxAlignmentResolved

        internal HeaderCheckBoxAlignment HeaderCheckBoxAlignmentResolved
        {
            get
            {
                HeaderCheckBoxAlignment resolvedVal = HeaderCheckBoxAlignment.Default;

                object objVal;
                if (false == StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.HeaderCheckBoxAlignment, out objVal))
                {
                    // check on the band override level
                    if (this.HasOverride &&
                        this.Override.ShouldSerializeHeaderCheckBoxAlignment())
                    {
                        resolvedVal = this.Override.HeaderCheckBoxAlignment;
                    }

                    // check on the layout override level
                    else if (this.Layout != null &&
                        this.Layout.HasOverride &&
                        this.Layout.Override.ShouldSerializeHeaderCheckBoxAlignment())
                    {
                        resolvedVal = this.Layout.Override.HeaderCheckBoxAlignment;
                    }

                    objVal = StyleUtils.CachePropertyValue(this, StyleUtils.CustomProperty.HeaderCheckBoxAlignment, resolvedVal, HeaderCheckBoxAlignment.Default, HeaderCheckBoxAlignment.Default);
                }
                return (HeaderCheckBoxAlignment)objVal;
            }
        }

        #endregion HeaderCheckBoxAlignmentResolved

        // CDS NAS v9.1 Header CheckBox
        #region HeaderCheckBoxSynchronizationResolved

        internal HeaderCheckBoxSynchronization HeaderCheckBoxSynchronizationResolved
        {
            get
            {
                if (this.HasOverride &&
                    this.Override.ShouldSerializeHeaderCheckBoxSynchronization())
                    return this.Override.HeaderCheckBoxSynchronization;
                
                if (this.Layout != null &&
                    this.Layout.HasOverride &&
                    this.Layout.Override.ShouldSerializeHeaderCheckBoxSynchronization())
                    return this.Layout.Override.HeaderCheckBoxSynchronization;

                return HeaderCheckBoxSynchronization.Default;
            }
        }

        #endregion HeaderCheckBoxSynchronizationResolved

        // CDS 02/04/08 - TFS12517 - NAS v9.1 Header CheckBox - When the synchronization changes, we need to invalidate all the cached CheckState values, so we'll using versioning.
        #region HeaderCheckboxSynchronizationVersion

        private int headerCheckBoxSynchronizationVersion = 0;

        internal int HeaderCheckBoxSynchronizationVersion
        {
            get { return this.headerCheckBoxSynchronizationVersion; }
        }

        #endregion HeaderCheckboxSynchronizationVersion

        #region BumpHeaderCheckBoxSynchronizationVersion

        internal void BumpHeaderCheckBoxSynchronizationVersion()
        {
            this.headerCheckBoxSynchronizationVersion++;
        }

        #endregion BumpHeaderCheckBoxSynchronizationVersion

        // CDS 7/07/09 - TFS17996
        #region IsCachedContainsCheckboxOnTopOrBottomValid

        internal bool IsCachedContainsCheckboxOnTopOrBottomValid
        {
            get { return this.isCachedContainsCheckboxOnTopOrBottomValid; }
        }

        #endregion IsCachedContainsCheckboxOnTopOrBottomValid

        // CDS 6/23/09 - TFS17996
        #region DirtyCachedContainsCheckboxOnTopOrBottom

        internal void DirtyCachedContainsCheckboxOnTopOrBottom()
        {
            this.isCachedContainsCheckboxOnTopOrBottomValid = false;
        }

        #endregion DirtyCachedContainsCheckboxOnTopOrBottom

        #endregion NA v9.1 Header CheckBox

        // MBS 12/04/08 - NA9.1 Excel Style Filtering
        #region FilterUIProviderResolved

        internal IFilterUIProvider FilterUIProviderResolved
        {
            get
            {
                if (this.HasOverride &&
                    this.overrideObj.ShouldSerializeFilterUIProvider())
                    return this.overrideObj.FilterUIProvider;

                if (this.layout.HasOverride
                    && this.layout.Override.ShouldSerializeFilterUIProvider())
                    return this.layout.Override.FilterUIProvider;

                return null;
            }
        }
        #endregion //FilterUIProviderResolved

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

        // MRS 2/10/2009 - TFS13728
        #region AllowRowLayoutGroupMovingResolved

        internal Layout.GridBagLayoutAllowMoving AllowRowLayoutGroupMovingResolved
        {
            get
            {
                // MRS 4/11/05 - Allow Sizing at Design-time. 
                if (null != this.Layout &&
                    null != this.layout.Grid &&
                    this.Layout.Grid.DesignMode)
                {
                    return Infragistics.Win.Layout.GridBagLayoutAllowMoving.AllowAll;
                }

                // MRS 4/21/05 - Only allow this for the grid. 
                if (null != this.Layout &&
                    !(this.layout.Grid is UltraGrid))
                {
                    return Infragistics.Win.Layout.GridBagLayoutAllowMoving.None;
                }

                switch (this.AllowGroupMovingResolved)
                {
                    case AllowGroupMoving.NotAllowed:
                        return GridBagLayoutAllowMoving.None;
                    case AllowGroupMoving.WithinBand:
                    case AllowGroupMoving.WithinGroup: // MRS 2/13/2009 - TFS13834
                        return GridBagLayoutAllowMoving.AllowAll;
                    case AllowGroupMoving.Default:
                    default:
                        Debug.Fail("Unknown or invalid AllowGroupMovingResolved setting");
                        return GridBagLayoutAllowMoving.None;
                }                
            }
        }

        #endregion AllowRowLayoutGroupMovingResolved

        #region BumpGroupsVersion
        internal void BumpGroupsVersion()
        {
            if (this.groupsVersion < int.MaxValue)
                this.groupsVersion++;
            else
                this.groupsVersion = 0;
        }
        #endregion //BumpGroupsVersion

        // MRS 2/19/2009 - TFS14274
        #region GetAllOrderedHeaders
        private List<HeaderBase> GetAllOrderedHeaders()
        {
            List<HeaderBase> orderedHeaders = new List<HeaderBase>();

            HeadersCollection columnHeaders;
            HeadersCollection groupHeaders = null;
            if (this.UseRowLayoutResolved)
            {
                columnHeaders = this.LayoutOrderedVisibleColumnHeaders;
                groupHeaders = this.LayoutOrderedVisibleGroupHeaders;
            }
            else
                columnHeaders = this.OrderedHeaders;

            if (columnHeaders != null)
            {
                foreach (HeaderBase headerBase in columnHeaders)
                {
                    orderedHeaders.Add(headerBase);
                }
            }

            if (groupHeaders != null)
            {
                foreach (HeaderBase headerBase in groupHeaders)
                {
                    orderedHeaders.Add(headerBase);
                }
            }
            return orderedHeaders;
        }
        #endregion //GetAllOrderedHeaders

        #region GetGroupsAndColumns
        internal static List<IProvideRowLayoutColumnInfo> GetGroupsAndColumns(UltraGridBand band)
        {
            // Get a list of all the groups and columns in the band
            List<IProvideRowLayoutColumnInfo> groupsAndColumns = new List<IProvideRowLayoutColumnInfo>();

            foreach (UltraGridGroup group in band.Groups)
            {
                // Exclude the group being removed.
                groupsAndColumns.Add(group);
            }

            foreach (UltraGridColumn column in band.Columns)
                groupsAndColumns.Add(column);

            return groupsAndColumns;
        }
        #endregion //GetGroupsAndColumns

        #region GetLayoutVisibleGroups

		// MD 1/19/09 - Groups in RowLayout
		// Added a parameter to allow all groups to be obtained.
		//private UltraGridGroup[] GetLayoutVisibleGroups()
		internal UltraGridGroup[] GetLayoutVisibleGroups( bool getRootGroupsOnly )
        {            
            if (this.RowLayoutStyle != RowLayoutStyle.GroupLayout)
                return new UltraGridGroup[] {};

            GroupsCollection groups = this.Groups;

            List<UltraGridGroup> list = new List<UltraGridGroup>(groups.Count);

            for (int i = 0, count = groups.Count; i < count; i++)
            {
                UltraGridGroup group = groups[i];

                if (null != group && group.IsVisibleInLayout &&
					this.ShouldIncludeInLayout( group, getRootGroupsOnly ) )
                    list.Add(group);
            }

            return list.ToArray();
        }

        #endregion // GetLayoutVisibleGroups

        #region GetMatchingGroup

        internal UltraGridGroup GetMatchingGroup(SerializedGroupID serializedGroupId)
        {
            UltraGridGroup group = null;

            // first try matching via the group's key
            //
            try
            {
                if (serializedGroupId.Key.Length > 0 &&
                    this.Groups.Exists(serializedGroupId.Key))
                    group = this.Groups[serializedGroupId.Key];
            }
            catch (Exception)
            {
            }

            // if key did't work then try the relative index
            //
            if (group == null)
            {
                int index = serializedGroupId.RelativeIndex;

                try
                {
                    group = this.Groups[index];
                }
                catch (Exception)
                {
                }

            }

            return group;
        }

        #endregion // GetMatchingGroup

        #region GetRowLayoutColumnInfo
        internal static RowLayoutColumnInfo GetRowLayoutColumnInfo(ILayoutItem layoutItem)
        {
            // MRS - NAS 9.1 - Groups in RowLayout
            //
            #region Old Code
            //UltraGridColumn column = layoutItem as UltraGridColumn;
            //if (column != null)
            //    return column.RowLayoutColumnInfo;

            //ColumnHeader header = layoutItem as ColumnHeader;
            //column = header != null
            //    ? header.Column
            //    : null;

            //if (column != null)
            //    return column.RowLayoutColumnInfo;
            #endregion //Old Code
            //
            IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = layoutItem as IProvideRowLayoutColumnInfo;
            if (iProvideRowLayoutColumnInfo == null)
            {
                GroupLayoutItemBase groupLayoutItemBase = layoutItem as GroupLayoutItemBase;
                if (groupLayoutItemBase != null)
                    iProvideRowLayoutColumnInfo = groupLayoutItemBase.Group;
            }

            if (iProvideRowLayoutColumnInfo != null)
                return iProvideRowLayoutColumnInfo.RowLayoutColumnInfo;

            HeaderBase header = layoutItem as HeaderBase;
            if (header != null)
                return header.RowLayoutColumnInfo;            

            Debug.Fail("Unknown LayoutItem type");
            return null;
        }
        #endregion GetRowLayoutColumnInfo

        #region GetSerializedGroupId

        internal SerializedGroupID GetSerializedGroupId(UltraGridGroup group)
        {
            Debug.Assert(null != group, "Null group passed in.");
            if (null == group)
                return null;

            if (this != group.Band)
            {
                Debug.Assert(false, "Group not from this band !");
                return group.Band.GetSerializedGroupId(group);
            }

            int groupRelativeIndex = group.Index;

            Debug.Assert(groupRelativeIndex >= 0, "Invalid group.");

            SerializedGroupID groupid = new SerializedGroupID(groupRelativeIndex,
                group.Key);

            return groupid;
        }

        #endregion // GetSerializedGroupId

        #region GetSummaryLayoutManagerHelper
        private static void GetSummaryLayoutManagerHelper(SummaryDisplayAreaContext context, LayoutManagerBase srcLM, LayoutManagerBase destLM)
        {
            foreach (ILayoutItem item in srcLM.LayoutItems)
            {
                if (item is SummaryLayoutItem)
                {
                    SummaryLayoutItem summaryLayoutItem = (SummaryLayoutItem)item;
                    if (null != summaryLayoutItem)
                    {
                        destLM.LayoutItems.Add(new SummaryLayoutItem(summaryLayoutItem.Column, context),
                            srcLM.LayoutItems.GetConstraint(summaryLayoutItem));
                    }
                }
                else if (item is GroupLayoutItemSummaryContent)
                {
                    // We need to walk into the group and copy all of it's layout items, just like
                    // we do on the band. Do this recursively, since the group can contain other
                    // groups. 
                    GroupLayoutItemSummaryContent groupLayoutItemSummaryContent = (GroupLayoutItemSummaryContent)item;
                    LayoutManagerBase srcGroupLM = groupLayoutItemSummaryContent.LayoutManager;
                    LayoutManagerBase destgroupLM = UltraGridBand.CreateGridBagLayoutManager();
                    UltraGridBand.GetSummaryLayoutManagerHelper(context, srcGroupLM, destgroupLM);
                    GroupLayoutItemSummaryContent cloneGroupLayoutItemSummaryContent = new GroupLayoutItemSummaryContent(groupLayoutItemSummaryContent.Group, destgroupLM);
                    object constraint = srcLM.LayoutItems.GetConstraint(item);
                    destLM.LayoutItems.Add(cloneGroupLayoutItemSummaryContent, constraint);
                }
                else
                {
                    // If it's not an instance of SummaryLayoutItem then it could be a header.
                    //
                    destLM.LayoutItems.Add(item, srcLM.LayoutItems.GetConstraint(item));
                }
            }
        }
        #endregion //GetSummaryLayoutManagerHelper

        #region GetVisibleGroupsList

        /// <summary>
        /// Returns a list of visible groups in the band, regardless of the RowLayoutStyle
        /// </summary>
        /// <returns></returns>
        private List<UltraGridGroup> GetVisibleGroupsList()
        {
            List<UltraGridGroup> groupsColl;
            switch (this.RowLayoutStyle)
            {
                case RowLayoutStyle.ColumnLayout:
                case RowLayoutStyle.GroupLayout:
					// MD 1/19/09 - Groups in RowLayout
					// Added a parameter to allow all groups to be obtained.
					//UltraGridGroup[] groupsArr = this.GetLayoutVisibleGroups();
					UltraGridGroup[] groupsArr = this.GetLayoutVisibleGroups( true );
                    groupsColl = new List<UltraGridGroup>(groupsArr);
                    break;
                case RowLayoutStyle.None:
                default:
                    Debug.Assert(this.RowLayoutStyle == RowLayoutStyle.None, "Unknown RowLayoutStyle");

                    if (this.CardView)
                        return null;

                    GroupsCollection groups = this.GroupsDisplayed ? this.Groups : null;
                    if (groups != null)
                    {
                        groupsColl = new List<UltraGridGroup>(groups.Count);
                        foreach (UltraGridGroup group in groups)
                        {
                            groupsColl.Add(group);
                        }
                    }
                    else
                        groupsColl = null;

                    break;
            }
            return groupsColl;
        }
        #endregion //GetVisibleGroupsList

        #region GroupsVersion
        internal int GroupsVersion
        {
            get
            {
                return this.groupsVersion;
            }
        }
        #endregion //GroupsVersion

        #region GroupVisiblePositionComparer

        /// <summary>
        /// A IComparer implementation for sorting groups by their visible positions.
        /// </summary>
        public class GroupVisiblePositionComparer : IComparer
        {
            /// <summary>
            /// Constructor.
            /// </summary>
            public GroupVisiblePositionComparer()
            {
            }

            int IComparer.Compare(object x, object y)
            {
                if (x == y)
                    return 0;
                else if (null == x)
                    return -1;
                else if (null == y)
                    return 1;

                UltraGridGroup groupX = (UltraGridGroup)x;
                UltraGridGroup groupY = (UltraGridGroup)y;

                int visiblePosX = groupX.Header.VisiblePosition;
                int visiblePosY = groupY.Header.VisiblePosition;

                if (visiblePosX == visiblePosY)
                    return groupX.Index.CompareTo(groupY.Index);
                else
                {
                    return visiblePosX.CompareTo(visiblePosY);
                }
            }
        }

        #endregion // GroupVisiblePositionComparer

        #region InitRowLayoutColumnInfo
        private void InitRowLayoutColumnInfo()
        {
            if (this.columns != null)
            {
                foreach (UltraGridColumn column in this.columns)
                {
                    this.InitRowLayoutColumnInfo(column.RowLayoutColumnInfo);
                }
            }

            if (this.groups != null)
            {
                foreach (UltraGridGroup group in this.groups)
                {
                    this.InitRowLayoutColumnInfo(group.RowLayoutGroupInfo);
                }
            }
        }
        #endregion //InitRowLayoutColumnInfo

        #region InitRowLayoutColumnInfo
        private void InitRowLayoutColumnInfo(RowLayoutColumnInfo rowLayoutColumnInfo)
        {
            UltraGridBand.InitRowLayoutColumnInfo(rowLayoutColumnInfo, this);
        }        

        internal static void InitRowLayoutColumnInfo(RowLayoutColumnInfo rowLayoutColumnInfo, UltraGridBand band)
        {
            // Try to match up the parent group by key
            if (rowLayoutColumnInfo.ParentGroupKey != null &&
                band.Groups.Exists(rowLayoutColumnInfo.ParentGroupKey))
            {
                rowLayoutColumnInfo.ParentGroup = band.Groups[rowLayoutColumnInfo.ParentGroupKey];
				return;
            }
            else
            {
                // If that fails, try to match it up by index.  \
                // MRS 2/9/2009 - TFS13590       
                //if (rowLayoutColumnInfo.ParentGroupIndex > 0 &&
                if (rowLayoutColumnInfo.ParentGroupIndex >= 0 &&
                    rowLayoutColumnInfo.ParentGroupIndex < band.Groups.Count)
                {
                    rowLayoutColumnInfo.ParentGroup = band.Groups[rowLayoutColumnInfo.ParentGroupIndex];
					return;
                }
            }

			// MD 1/20/09 - Groups in RowLayout
			// If we couldn't find the group from the key or index, set the group to null.
			rowLayoutColumnInfo.ParentGroup = null;
        }
        #endregion //InitRowLayoutColumnInfo

        #region LayoutOrderedVisibleGroupHeaders

        internal HeadersCollection LayoutOrderedVisibleGroupHeaders
        {
            get
            {
                this.VerifyRowLayoutCache();

                return this.layoutOrderedVisibleGroupHeaders;
            }
        }

        #endregion // LayoutOrderedVisibleGroupHeaders

		#region LevelCountResolved

		internal int LevelCountResolved
		{
			get
			{
				if ( this.RowLayoutStyle == RowLayoutStyle.GroupLayout )
					return 1;

				return this.LevelCount;
			}
		} 

		#endregion LevelCountResolved

        #region ShouldAddGroupHeaderToCellArea
        // Returns whether to add group headers to the cell and summary layout managers, even though
        // headers are not with cells. This is to fill in gaps that might exist when the group header
        // is aligned to the left/right in normal mode, or top/bottom in cardview mode. 
        internal static bool ShouldAddGroupHeaderToCellArea(UltraGridGroup group)
        {
            if (false == group.Band.CardView)
            {
                if (group.RowLayoutGroupInfo.LabelPositionResolved == LabelPosition.Left ||
                    group.RowLayoutGroupInfo.LabelPositionResolved == LabelPosition.Right)
                {
                    return true;
                }
            }
            else
            {
                if (group.RowLayoutGroupInfo.LabelPositionResolved == LabelPosition.Top ||
                    group.RowLayoutGroupInfo.LabelPositionResolved == LabelPosition.Bottom)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion //ShouldAddGroupHeaderToCellArea

        #region ShouldIncludeInRootLayout

		// MD 1/19/09 - Groups in RowLayout
		// Renamed and added a parameter so all layout items can be included in GroupLayout style if needed.
        //private bool ShouldIncludeInRootLayout(IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo)
		private bool ShouldIncludeInLayout( IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo, bool getRootItemsOnly )
        {
            switch (this.RowLayoutStyle)
            {
                case RowLayoutStyle.None:
                    return false;
                case RowLayoutStyle.ColumnLayout:
                    return iProvideRowLayoutColumnInfo is UltraGridColumn;                    
                case RowLayoutStyle.GroupLayout:
					// MD 1/19/09 - Groups in RowLayout
					// If it is not required to get only root items, just return true because any item is allowed.
					if ( getRootItemsOnly == false )
						return true;

                    return iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ParentGroup == null;
                default:
                    Debug.Fail("Unknown RowLayoutStyle");
                    return true;

            }
        }
        #endregion ShouldIncludeInRootLayout

        #region RowLayoutStyle

        /// <summary>
        /// Enables row-layout functionality in the band.
        /// </summary>
        /// <remarks>
        /// <para class="body">This property is a replacement for (now obsolete) UseRowLayout property.</para>
        /// <para class="body">The RowLayoutStyle.None setting is the equivalent of setting UseRowLayout to false. In this style, the columns are arranged based on Groups, Levels, and the VisiblePosition on the column header.</para>
        /// <para class="body">The RowLayoutStyle.ColumnLayout setting is the equivalent of setting UseRowLayout to true. In this style, the columns are arranged using a GridBagLayout. Use <see cref="UltraGridColumn.RowLayoutColumnInfo"/> object to customize the cell positions. By default all the columns are visible. Set <see cref="UltraGridColumn.Hidden"/> to true on columns that you want to hide. Groups are ignored in this style.</para>
        /// <para class="body">The RowLayoutStyle.GroupLayout setting is similar to the ColumnLayout, except that groups are included in the layout. Essentially, the RowLayoutColumnInfo of the band will contain groups and each group will contain it's own GridBagLayout. The RowLayoutColumnInfo of each column is considered to apply to the layout of that column's parent object - either the row itself or the ParentGroup if specified. This allows nested layouts within a row so that group headers can be applied to an indefinite number of levels of depth.</para>        
        /// <para class="body">Column swapping is disabled in the ColumnLayout or GroupLayout style.</para>
        /// <seealso cref="UltraGridColumn.RowLayoutColumnInfo"/> <seealso cref="UltraGridBand.RowLayouts"/>
        /// </remarks>
        [LocalizedDescription("LDR_UltraGridBand_P_RowLayoutStyle")]
        public RowLayoutStyle RowLayoutStyle
        {
            get
            {
                return this.rowLayoutStyle;
            }
            set
            {
                if (this.rowLayoutStyle != value)
                {
                    if (!Enum.IsDefined(typeof(RowLayoutStyle), value))
                        throw new InvalidEnumArgumentException("RowLayoutStyle", (int)value, typeof(RowLayoutStyle));

                    this.rowLayoutStyle = value;

                    switch (this.rowLayoutStyle)
                    {
                        case RowLayoutStyle.None:
                            this.SetUseRowLayoutNoWarning(false);
                            break;
                        case RowLayoutStyle.ColumnLayout:
                        case RowLayoutStyle.GroupLayout:
                            this.SetUseRowLayoutNoWarning(true);
                            this.DirtyRowLayoutCachedInfo();
                            break;
                        default:
                            Debug.Fail("Unknown RowLayoutStyle");
                            this.SetUseRowLayoutNoWarning(true);
                            this.DirtyRowLayoutCachedInfo();
                            break;
                    }

                    // SSP 5/5/05
                    // When row layout functionality is turned on or off the Hidden state of
                    // a group-by column may change in which case we want to recreate the
                    // ordered headers collection.
                    //
                    this.BumpFixedHeadersVerifyVersion(true);

                    // CDS 7/07/09 TFS17996
                    this.DirtyCachedContainsCheckboxOnTopOrBottom();

                    this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.RowLayoutStyle);
                }
            }
        }

        #region ResetRowLayoutStyle

        /// <summary>
        /// Resets RowLayoutStyle property to its default value.
        /// </summary>
        public void ResetRowLayoutStyle()
        {
            this.RowLayoutStyle = RowLayoutStyle.None;
        }

        #endregion // ResetRowLayoutStyle

        #region ShouldSerializeRowLayoutStyle

        /// <summary>
        /// Returns true if this property is not set to its default value
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeRowLayoutStyle()
        {
            return this.RowLayoutStyle != RowLayoutStyle.None;
        }

        #endregion // ShouldSerializeRowLayoutStyle

        #endregion // RowLayoutStyle

        #region SetUseRowLayoutNoWarning
        // Helper method that sets the UseRowLayout property without a warning, even
        // the the property is now obsolete. 
        private void SetUseRowLayoutNoWarning(bool useRowLayout)
        {
#pragma warning disable 618
            this.UseRowLayout = useRowLayout;
#pragma warning restore 618
        }
        #endregion //SetUseRowLayoutNoWarning


        #region ILayoutGroup Members

        /// <summary>
        /// Returns the visible items within the group. Does not include the group header. 
        /// </summary>
        /// <returns></returns>
        Hashtable ILayoutGroup.GetResolvedGCs(Hashtable allConstraints)
        {
            UltraGrid grid = (UltraGrid)this.Layout.Grid;
            List<ILayoutItem> layoutItems = ((ILayoutGroup)this).GetVisibleLayoutItems();

            Hashtable hash = new Hashtable();
            foreach (ILayoutItem layoutItem in layoutItems)
            {
                GridBagConstraint constraint = (GridBagConstraint)allConstraints[layoutItem];
                if (constraint != null)
                    hash[layoutItem] = constraint;
                else
                    Debug.Fail("Failed to get the constraint for the layoutItem.");
            }

            return hash;
        }

        /// <summary>
        /// Returns the resolved GridBagConstraints for the items in the header. 
        /// </summary>        
        /// <returns></returns>
        List<ILayoutItem> ILayoutGroup.GetVisibleLayoutItems()
        {
            UltraGrid grid = (UltraGrid)this.Layout.Grid;
            ILayoutItem[] layoutItems = grid.gridBagLayoutDragStrategy.GridBagLayoutDragManager.GetVisibleLayoutItems();

            List<ILayoutItem> relevantLayoutItems = new List<ILayoutItem>();
            
            foreach (ILayoutItem layoutItem in layoutItems)
            {
                ILayoutChildItem layoutChildItem = layoutItem as ILayoutChildItem;
                Debug.Assert(layoutChildItem != null, "The layoutItem is not a LayoutChildItem; unexpected");
                ILayoutGroup layoutGroup = layoutChildItem.ParentGroup;
                if (this == layoutGroup)
                    relevantLayoutItems.Add(layoutItem);
            }

            return relevantLayoutItems;
        }

        /// <summary>
        /// Returns an ILayoutItem representing the group. 
        /// </summary>
        /// <returns></returns>
        ILayoutItem ILayoutGroup.GetLayoutItem()
        {
            return null;
        }

        /// <summary>
        /// Returns the label span size of the group. 
        /// </summary>
        Size ILayoutGroup.LabelSpanSize
        {
            get { return Size.Empty; }
        }

		/// <summary>
		/// Converts a client origin relative to the group into an absolute origin.
		/// </summary>
		Point ILayoutGroup.OriginToAbsolute( Point clientOrigin, UIElement containerElement )
		{
			return clientOrigin;
		}

		/// <summary>
		/// Converts an absolute origin into a client origin relative to the group.
		/// </summary>
		Point ILayoutGroup.OriginToClient( Point absoluteOrigin, UIElement containerElement )
		{
			return absoluteOrigin;
		}

        #endregion // ILayoutGroup Members


        // MRS 2/10/2009 - TFS13728
        #region LayoutGroupPositionComparer Class


        internal class LayoutGroupPositionComparer : IComparer
        {
            private bool yWise = true;

            internal LayoutGroupPositionComparer(bool yWise)
            {
                this.yWise = yWise;
            }

            internal LayoutGroupPositionComparer()
                : this(true)
            {
            }

            public int Compare(object o1, object o2)
            {
                UltraGridGroup col1 = (UltraGridGroup)o1;
                UltraGridGroup col2 = (UltraGridGroup)o2;

                return Compare(col1, col2, this.yWise);
            }

            // SSP 2/24/06 BR09715
            // Added a static overload.
            // 
            public static int Compare(UltraGridGroup group1, UltraGridGroup group2, bool yWise)
            {
                // MD 1/19/09 - Groups in RowLayout
                // When using GroupLayout style, we need to check the flattened layout to know the visible position of the groups.
                if (group1.Band != null && group1.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout)
                {
                    GridBagLayoutManager manager = group1.Band.AreColumnHeadersInSeparateLayoutArea
                        ? (GridBagLayoutManager)group1.Band.HeaderLayoutManager
                        : (GridBagLayoutManager)group1.Band.RowLayoutManager;

                    if (manager != null)
                    {
                        if (manager.LayoutItems.IndexOf(group1.Header) != -1 &&
                            manager.LayoutItems.IndexOf(group2.Header) != -1)
                        {
                            IGridBagConstraint constraint1 = manager.LayoutItems.GetConstraint(group1.Header) as IGridBagConstraint;
                            IGridBagConstraint constraint2 = manager.LayoutItems.GetConstraint(group2.Header) as IGridBagConstraint;

                            if (constraint1 != null && constraint2 != null)
                            {
                                int xCompareResult = constraint1.OriginX.CompareTo(constraint2.OriginX);
                                int yCompareResult = constraint1.OriginY.CompareTo(constraint2.OriginY);

                                if (yWise)
                                {
                                    if (yCompareResult != 0)
                                        return yCompareResult;

                                    return xCompareResult;
                                }
                                else
                                {
                                    if (xCompareResult != 0)
                                        return xCompareResult;

                                    return yCompareResult;
                                }
                            }
                        }
                    }
                }

                if (yWise)
                {
                    int r;

                    r = group1.RowLayoutGroupInfo.OriginYResolved.CompareTo(group2.RowLayoutGroupInfo.OriginYResolved);

                    if (0 != r)
                        return r;

                    r = group1.RowLayoutGroupInfo.OriginXResolved.CompareTo(group2.RowLayoutGroupInfo.OriginXResolved);

                    return r;
                }
                else
                {
                    int r;

                    r = group1.RowLayoutGroupInfo.OriginXResolved.CompareTo(group2.RowLayoutGroupInfo.OriginXResolved);

                    if (0 != r)
                        return r;

                    r = group1.RowLayoutGroupInfo.OriginYResolved.CompareTo(group2.RowLayoutGroupInfo.OriginYResolved);

                    return r;
                }
            }
        }

        #endregion // LayoutGroupPositionComparer Class

        #endregion //NAS 9.1 - Groups in RowLayout

        // MBS 3/31/09 - NA9.2 CellBorderColor
        #region ActiveCellBorderThicknessResolved

        internal int ActiveCellBorderThicknessResolved
        {
            get
            {
                object objVal;
                if (!StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.ActiveCellBorderThickness, out objVal))
                {
                    // MBS 5/15/09 - TFS17562
                    //int val = 0;
                    int val = 1;

                    if (this.HasOverride &&
                        this.Override.ShouldSerializeActiveCellBorderThickness())
                    {
                        val = this.Override.ActiveCellBorderThickness;
                    }
                    else if (this.Layout != null &&
                        this.Layout.HasOverride &&
                        this.Layout.Override.ShouldSerializeActiveCellBorderThickness())
                    {
                        val = this.Layout.Override.ActiveCellBorderThickness;
                    }

                    objVal = StyleUtils.CachePropertyValue(this,
                        // MBS 5/15/09 - TFS17562
                        //StyleUtils.CustomProperty.ActiveCellBorderThickness, val, 0, 0);
                        StyleUtils.CustomProperty.ActiveCellBorderThickness, val, 1, 1);
                }
                return (int)objVal;
            }
        }
        #endregion //ActiveCellBorderThicknessResolved

        // CDS 9.2 Column Moving Indicators
        #region 9.2 Drop Indicators

        #region DragDropIndicatorBrushColorResolved

        [InfragisticsFeature(
        Version = FeatureInfo.Version_9_2,
        FeatureName = FeatureInfo.FeatureName_ColumnMovingIndicators)
        ]
        internal Color DragDropIndicatorBrushColorResolved
        {
            get
            {

                object objVal;
                if (!StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.DragDropIndicatorColor, out objVal))
                {
                    Color val = Infragistics.Win.DragDropIndicatorSettings.DEFAULT_COLOR;
                    if (this.HasOverride &&
                        this.Override.HasDragDropIndicatorSettings &&
                        this.Override.DragDropIndicatorSettings.ShouldSerializeColor())
                    {
                        val = this.Override.DragDropIndicatorSettings.Color;
                    }
                    else if (this.Layout != null &&
                        this.Layout.HasOverride &&
                        this.Layout.Override.HasDragDropIndicatorSettings &&
                        this.Layout.Override.DragDropIndicatorSettings.ShouldSerializeColor())
                    {
                        val = this.Layout.Override.DragDropIndicatorSettings.Color;
                    }

                    objVal = StyleUtils.CachePropertyValue(this,
                        StyleUtils.CustomProperty.DragDropIndicatorColor, val, Color.Empty, Color.Empty);
                }
                return (Color)objVal;
            }
        }


        #endregion DragDropIndicatorBrushColorResolved

        // CDS 4/2909 9.2 Column Moving Indicators
        // Added to handle the colorizable default arrow
        #region DefaultDropIndicatorArrowImageResolved

        private Image DefaultDropIndicatorArrowImageResolved(Color resolvedColor)
        {
            UltraGrid grid = (this.Layout != null) ? this.Layout.Grid as UltraGrid : null;

            // unable to resolve the default image if we cannot access the grid, so return null
            if (grid == null)
                return null;

            // if the resolvedColor is empty, then use DefaultDropIndicatorDownArrowImage
            if (resolvedColor.IsEmpty)
                return grid.DefaultDropIndicatorDownArrowImage;


            //// otherwise retrieve the ColorizableDropIndicatorDownArrowImage;
            //Image arrowImage = (Image)grid.ColorizableDropIndicatorDownArrowImage;
                        
            //// perform color replacement
            //Bitmap colorizedImage = new Bitmap(arrowImage);
           
            //using (Graphics g = Graphics.FromImage(colorizedImage)) 
            //using (ImageAttributes attr = new ImageAttributes())
            //{
            //    ColorMap[] cm = new ColorMap[1];
            //    cm[0] = new ColorMap();
            //    // used the color of the center pixel as our old color.
            //    cm[0].OldColor = colorizedImage.GetPixel(colorizedImage.Width / 2, colorizedImage.Height / 2);
            //    cm[0].NewColor = resolvedColor;

            //    attr.SetRemapTable(cm);

            //    Rectangle rect = new Rectangle(0, 0, arrowImage.Width, arrowImage.Height);
            //    g.DrawImage(arrowImage, rect, 0, 0, arrowImage.Width, arrowImage.Height, GraphicsUnit.Pixel, attr);
            //}
            //return colorizedImage;
            return grid.ColorizableDropIndicatorDownArrowImage;
        }

        #endregion DefaultDropIndicatorArrowImageResolved

        #region DragDropIndicatorSettingsResolved

        [InfragisticsFeature(
        Version = FeatureInfo.Version_9_2,
        FeatureName = FeatureInfo.FeatureName_ColumnMovingIndicators)
        ]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DragDropIndicatorSettings DragDropIndicatorSettingsResolved
        {
            get
            {
                object objValBottom, objValLeft, objValRight, objValTop;

                objValBottom =
                    objValLeft =
                    objValRight =
                    objValTop = null;

                bool imageCached = false;
                
                //see if any of the 4 sides have cached images. If so, create a new DragDropIndicatorSettings using the objVal's
                imageCached = (StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.DragDropIndicatorImageBottom, out objValBottom)) ? true : imageCached;
                imageCached = (StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.DragDropIndicatorImageLeft, out objValLeft)) ? true : imageCached;
                imageCached = (StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.DragDropIndicatorImageRight, out objValRight)) ? true : imageCached;
                imageCached = (StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.DragDropIndicatorImageTop, out objValTop)) ? true : imageCached;
                
                if (imageCached)                
                {
                    //determine if the cached image is the colorizable image
                    bool IsDefaultColorizableImage = false;

                    UltraGrid grid = (this.Layout != null) ? this.Layout.Grid as UltraGrid : null;

                    if (grid != null)
                        IsDefaultColorizableImage = Image.Equals(objValTop, grid.ColorizableDropIndicatorDownArrowImage);

                    // if any of the images are cached, used the returned objects
                    return new DragDropIndicatorSettings(objValBottom as Image, objValLeft as Image, objValRight as Image, objValTop as Image, this.DragDropIndicatorBrushColorResolved, IsDefaultColorizableImage);
                }

                DragDropIndicatorSettings defaultDragDropIndicatorSettings = new DragDropIndicatorSettings();
                DragDropIndicatorSettings styleDragDropIndicatorSettings = new DragDropIndicatorSettings();
                DragDropIndicatorSettings resolvedDragDropIndicatorSettings = null;

                // retrieve any images from application styling.
                styleDragDropIndicatorSettings.ImageBottom = StyleUtils.GetStylePropertyValue(this.Layout, StyleUtils.CustomProperty.DragDropIndicatorImageBottom) as Image;
                styleDragDropIndicatorSettings.ImageLeft = StyleUtils.GetStylePropertyValue(this.Layout, StyleUtils.CustomProperty.DragDropIndicatorImageLeft) as Image;
                styleDragDropIndicatorSettings.ImageRight = StyleUtils.GetStylePropertyValue(this.Layout, StyleUtils.CustomProperty.DragDropIndicatorImageRight) as Image;
                styleDragDropIndicatorSettings.ImageTop = StyleUtils.GetStylePropertyValue(this.Layout, StyleUtils.CustomProperty.DragDropIndicatorImageTop) as Image;

                if (this.HasOverride &&
                    this.Override.HasDragDropIndicatorSettings &&
                    this.Override.DragDropIndicatorSettings.IsAnyDropIndicatorImageSet)
                {
                    // grab the DragDropIndicatorSettings off of the Band's Override
                    resolvedDragDropIndicatorSettings = this.Override.DragDropIndicatorSettings.Clone();
                }
                else if (
                    this.Layout != null && // MBS 5/11/09
                    this.Layout.HasOverride &&
                    this.Layout.Override.HasDragDropIndicatorSettings &&
                    this.Layout.Override.DragDropIndicatorSettings.IsAnyDropIndicatorImageSet)
                {
                    // grab the DragDropIndicatorSettings off of the Layout's Override
                    resolvedDragDropIndicatorSettings = this.Layout.Override.DragDropIndicatorSettings.Clone();
                }

                // CDS 5/4/09 TFS17235
                //// resolve the DragDropIndicatorSettings based on the Application Styling's ResolutionOrder
                //resolvedDragDropIndicatorSettings = (DragDropIndicatorSettings)Infragistics.Win.AppStyling.StyleUtilities.CalculateResolvedValue(StyleUtils.GetResolutionOrderEnum(this.layout), styleDragDropIndicatorSettings, resolvedDragDropIndicatorSettings, defaultDragDropIndicatorSettings, defaultDragDropIndicatorSettings);
                resolvedDragDropIndicatorSettings = (DragDropIndicatorSettings)Infragistics.Win.AppStyling.StyleUtilities.CalculateResolvedValue(StyleUtils.GetResolutionOrderEnum(this.layout), (styleDragDropIndicatorSettings.ShouldSerialize() ? styleDragDropIndicatorSettings : null), resolvedDragDropIndicatorSettings, null, null);

                // apply the resolved brush
                if (resolvedDragDropIndicatorSettings == null)
                    // neither Layout or Band has an Override
                    resolvedDragDropIndicatorSettings = new DragDropIndicatorSettings(null, null, null, null, this.DragDropIndicatorBrushColorResolved);
                else
                    // assign the resolved brush
                    resolvedDragDropIndicatorSettings.Color = this.DragDropIndicatorBrushColorResolved;

                // cache the images 
                resolvedDragDropIndicatorSettings.ImageBottom = (Image)StyleUtils.CachePropertyValue(this, StyleUtils.CustomProperty.DragDropIndicatorImageBottom, null, resolvedDragDropIndicatorSettings.ImageBottom, null, null);
                resolvedDragDropIndicatorSettings.ImageLeft = (Image)StyleUtils.CachePropertyValue(this, StyleUtils.CustomProperty.DragDropIndicatorImageLeft, null, resolvedDragDropIndicatorSettings.ImageLeft, null, null);
                resolvedDragDropIndicatorSettings.ImageRight = (Image)StyleUtils.CachePropertyValue(this, StyleUtils.CustomProperty.DragDropIndicatorImageRight, null, resolvedDragDropIndicatorSettings.ImageRight, null, null);
                resolvedDragDropIndicatorSettings.ImageTop = (Image)StyleUtils.CachePropertyValue(this, StyleUtils.CustomProperty.DragDropIndicatorImageTop, null, resolvedDragDropIndicatorSettings.ImageTop, null, null);

                // CDS 4/29/09 9.2 Column Moving Indicators
                //// if the resolved DragDropIndicatorSettings is all default values, assign the default DropDownIndicator image.
                //if (resolvedDragDropIndicatorSettings.ShouldSerialize() == false)
                //{
                //    UltraGrid grid = (this.Layout != null) ? this.Layout.Grid as UltraGrid : null;
                //    if (grid != null)
                //        resolvedDragDropIndicatorSettings.ImageTop = (Image)StyleUtils.CachePropertyValue(this, StyleUtils.CustomProperty.DragDropIndicatorImageTop, grid.DefaultDropIndicatorDownArrowImage, null, null);
                //}
                if (resolvedDragDropIndicatorSettings.IsAnyDropIndicatorImageSet == false)
                {
                    resolvedDragDropIndicatorSettings.ImageTop = (Image)StyleUtils.CachePropertyValue(this, StyleUtils.CustomProperty.DragDropIndicatorImageTop, this.DefaultDropIndicatorArrowImageResolved(resolvedDragDropIndicatorSettings.Color), null, null);
                    // if a color has been assigned, we've dynamically created color-mapped images, so we should dispose of them later
                    resolvedDragDropIndicatorSettings.ShouldColorizeImage = resolvedDragDropIndicatorSettings.Color.IsEmpty ? false : true;
                }
                return resolvedDragDropIndicatorSettings;
            }
        }

        #endregion DragDropIndicatorSettingsResolved

        #endregion 9.2 Drop Indicators

		// MD 5/22/09 - TFS16438
		#region TFS16438 Fix

		internal void AddCalcReferences()
		{
			this.AddCalcReferencesHelper( true );
		}

		internal void RemoveCalcReferences()
		{
			this.AddCalcReferencesHelper( false );
		}

		private void AddCalcReferencesHelper( bool shouldAdd )
		{
			if ( this.Layout == null ||
				this.Layout.IsDisplayLayout == false ||
				this.Layout.IsApplyingLayout )
				return;

			UltraGridBase grid = this.Layout.Grid;

			if ( grid == null ||
				grid.CalcManager == null ||
				grid.Initializing ||
				grid.inSet_ListManager )
				return;

			foreach ( UltraGridBand band in this.Layout.BandsInternal )
			{
				if ( band.IsTrivialDescendantOf( this ) == false )
					continue;

				foreach ( UltraGridColumn column in band.Columns )
				{
					if ( column.HasFormulaHolder == false )
						continue;

					if ( shouldAdd )
					{
						column.CalcReference.ParsedReference = null;
						column.FormulaHolder.AddReferenceToCalcNetwork();
					}
					else
					{
						column.FormulaHolder.RemoveReferenceFromCalcNetwork();
						column.CalcReference.ParsedReference = null;
					}
				}

				foreach ( SummarySettings settings in band.Summaries )
				{
					if ( settings.HasFormulaHolder == false )
						continue;

					if ( shouldAdd )
					{
						settings.CalcReference.ParsedReference = null;
						settings.FormulaHolder.AddReferenceToCalcNetwork();
					}
					else
					{
						settings.FormulaHolder.RemoveReferenceFromCalcNetwork();
						settings.CalcReference.ParsedReference = null;
					}
				}
			}

			grid.CalcManager.ProcessEventQueue();
		} 

		#endregion TFS16438 Fix

        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
        #region 9.2 Cell ActiveAppearance related properties

        #region ActiveAppearancesEnabledResolved
        internal bool ActiveAppearancesEnabledResolved
        {
            get
            {
                if (this.HasOverride &&
                    this.overrideObj.ShouldSerializeActiveAppearancesEnabled())
                    return (this.overrideObj.ActiveAppearancesEnabled == DefaultableBoolean.True);

                return this.layout.ActiveAppearancesEnabledResolved;
            }
        }

        #endregion ActiveAppearancesEnabledResolved

        #region SelectedAppearancesEnabledResolved
        internal bool SelectedAppearancesEnabledResolved
        {
            get
            {
                if (this.HasOverride &&
                    this.overrideObj.ShouldSerializeSelectedAppearancesEnabled())
                    return (this.overrideObj.SelectedAppearancesEnabled == DefaultableBoolean.True);

                return this.layout.SelectedAppearancesEnabledResolved;
            }
        }

        #endregion SelectedAppearancesEnabledResolved

        #endregion 9.2 Cell ActiveAppearance related properties

        // MBS 8/7/09 - TFS18607
        #region EndEditNotificationInterfacesResolved

        internal AddRowEditNotificationInterface EndEditNotificationInterfacesResolved
        {
            get
            {
                AddRowEditNotificationInterface ret = this.HasOverride
                    ? this.overrideObj.AddRowEditNotificationInterface
                    : AddRowEditNotificationInterface.Default;

                if (AddRowEditNotificationInterface.Default == ret && this.layout.HasOverride)
                    ret = this.layout.Override.AddRowEditNotificationInterface;

                if (AddRowEditNotificationInterface.Default == ret)
                    ret = AddRowEditNotificationInterface.IEditableObjectOrICancelAddNew;

                return ret;
            }
        }
        #endregion //EndEditNotificationInterfacesResolved

    }//end of band class 


	// Added SynchedBandAbove class to keep track of a chain of
	// bands that are synchronized. It is used to pass into the
	// methods that build the synchronized col linked lists
	// 
	internal class SynchedBandAbove
	{ 
		private UltraGridBand              band = null;
		private SynchedBandAbove  synchBandAbove = null;

		public SynchedBandAbove( UltraGridBand band, SynchedBandAbove synchedBandAbove )
		{
			this.band = band;
			this.synchBandAbove = synchedBandAbove; 
		}

		public UltraGridBand Band 
		{ 
			get { return this.band; }
		}

		public SynchedBandAbove synchedBandAbove 
		{ 
			get { return this.synchBandAbove; }
		}
   
	}

    internal interface ISelectionStrategyProvider
    {
        SelectionStrategyBase SelectionStrategyCell { get; }
        SelectionStrategyBase SelectionStrategyRow { get; }
        SelectionStrategyBase SelectionStrategyColumn { get; }
        SelectionStrategyBase SelectionStrategyGroupByRow { get; }
    }

}


