#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved



namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Runtime.InteropServices;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Windows.Forms.Design;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Drawing.Printing;
	using Infragistics.Win.UltraWinMaskedEdit;
	using System.Collections.Generic;

	/// <summary>
	/// The base class for the UltraDropDown and UltraCombo
	/// control classes.
	/// </summary>
	/// <remarks>
	/// This is the base class from which <see cref="UltraCombo"/> and <see cref="UltraDropDown"/> derive.
	/// </remarks>
	// MRS 12/16/04 - BR00457
	// Changed IValueList to IFilterableValueList
//	public abstract class UltraDropDownBase : UltraGridBase, IValueList, 
//								Infragistics.Win.ISelectionManager
	public abstract class UltraDropDownBase : UltraGridBase, IFilterableValueList, 
		Infragistics.Win.ISelectionManager,
        IProvideItemImageSize // MRS 3/16/2009 - TFS15302
	{

		#region Private Fields	

		private string										displayMember = string.Empty;
		private int											dropDownWidth = -1;
		private int											maxDropDownItems = 8;
		private int											minDropDownItems = 1;
		
		// MRS 10/28/04 - UWG2996
		//private UltraGridRow								selectedRow;
		internal UltraGridRow								selectedRow;

		private string										valueMember = string.Empty;
		private IValueListOwner								owner = null;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private bool										mouseDown = false;

		private bool										dropdownEventFired = false;
		private bool										clickedOnColumnHeader = false;
		private Infragistics.Win.ToolTip					toolTip=null;
		private bool										includeVerticalScrollbar = false;
		private bool										includeHorizontalScrollbar = false;
					 
		// SSP 4/16/02 UWG1095
		// SelectedItemIndex and SelectedRow properties call each other to syncronize.
		// So I added these flags to prevent any infinite loops.
		//
		// MRS 10/28/04 - UWG2996
		// No longer needed
		//private bool inSelectedItemIndex = false;


		// SSP 1/8/04 UWG2801
		// Changed the access modifier from private to internal.
		//
		//private bool inSelectedRow = false;
		// MRS 10/28/04 - UWG2996
		// No longer needed
		//internal bool inSelectedRow = false;

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Support HeaderClickAction in Combo and DropDown
		Rectangle lastEditAreaInScreenCoords = Rectangle.Empty;

		// MRS 7/25/05 - BR04641
		// Keep an ArrayList of the items in the ValueMember field and the DisplayMember field for 
		// binary searching. 
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList sortedDisplayMemberList = null;
		//private ArrayList sortedValueMemberList = null;
		private List<UltraGridRow> sortedDisplayMemberList = null;
		private List<UltraGridRow> sortedValueMemberList = null;

		private int sortedDisplayMemberListVersion = int.MinValue;
		private int sortedValueMemberListVersion = int.MinValue;
		private string lastFailedPartialMatch = null;
		private int lastFailedPartialMatchVersion = int.MinValue;
		private bool canDoBinarySearch = true;
		
		// MRS 11/3/05 - BR07505
		// We also need to store the type of a real value in the ValueMember
		// list so that we know all the types are the same.
		//
		private Type lastValueMemberListType = null;

		// MRS 10/19/05 - BR06916
		// Moved this up from UltraDropDown, since the workaround
		// is good for UltraCombo, too. 
		// SSP 8/25/05 BR05793
		// If someone creates an UltraDropDown in code without adding it to Controls collection
		// of a form or container, then the BindingContext will be null and the UDD won't be 
		// able to bind. Worse yet, if such an udd is used as ValueList of a column then the
		// the first time it's dropped down it won't have any data however the next time it's
		// dropped down it will because the udd gets added to the drop down form, causing it
		// to have a BindingContext. Also when the udd is closed up after dropping down, its
		// BindingContext will become null. This is to fix that.
		// 
		private BindingContext bindingContext = null;

		// MRS 12/6/05 - BR07941
		private bool clickedOnDisabledRow = false;

		// AS 12/19/05 - BR08024
		private Rectangle lastRequestedRect;
		private bool isDropDownAlignedRight = false;

		//MRS MRS 4/18/06 - BR11468
		private DropDownSearchMethod dropDownSearchMethod = DropDownSearchMethod.Default;
		private EditAreaDisplayStyle editAreaDisplayStyle = EditAreaDisplayStyle.Default;

		// MRS 6/1/06 - BR12914		
		// This version number will get bumped any time ListChanged fires. 
		private int listVersion = int.MinValue;

		// MRS 6/1/06 - BR12914		
		// Any time we do a GetText / GetValue and fail to find the item, cache it, so
		// we can skip the search next time. 
		private static readonly object notSet = new object();
		private object lastFailedValueMatch = notSet;
		private int lastFailedValueMatchVersion = int.MinValue;
		private object lastFailedTextMatch = notSet;
		private int lastFailedTextMatchVersion = int.MinValue;

        //  BF 2/24/09  TFS12735
        private bool isActivatingRowByMouse = false;

        // MRS 3/16/2009 - TFS15302
        private int itemImageSizeVersion = int.MinValue;
        private int lastVerifiedItemImageSizeVersion = int.MaxValue;        
        private Size itemImageSize = Size.Empty;

		#endregion	 

		#region Events

		/// <summary>
		/// Called after list is dropped down
		/// </summary>
		/// <returns>True if cancelled.</returns>
		protected abstract bool FireBeforeDropDown( );

		/// <summary>
		/// Called after the list has closed
		/// </summary>
		protected abstract void FireAfterCloseUp( );

		/// <summary>
		/// Called after list is dropped down
		/// </summary>
		protected abstract void FireAfterDropDown( );

		/// <summary>
		/// Called when a new row has be selected
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void FireRowSelected( RowSelectedEventArgs e )
		{
		}
		
		#endregion
		private void OnCloseUp( object sender, EventArgs e )
		{
			// if we have fired the dropdown event then fire
			// the matching closeup
			//
			if ( this.dropdownEventFired )
			{
				this.dropdownEventFired = false;
				
				this.FireAfterCloseUp( );
			}

			// notify the owner of the closeup
			//
			if ( this.owner != null )
				this.owner.OnCloseUp();
		}

		internal IValueListOwner Owner { get { return this.owner; } }

		#region IValueList Events
		
		// SSP 5/16/02
		// Implemented the new ListChanged event off the IValueList.
		//
		/// <summary>
		/// Implementation of IValueList.ListChanged.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public event Infragistics.Win.ValueListChangedEventHandler ListChanged;
		
		#region ListChanged event

		// MRS 10/24/05 - BR06979
		// Made this virtual so UltraCombo can override it.
		//internal void FireListChanged( )
		internal virtual void FireListChanged( )
		{			
			// MRS 12/12/05 - BR08135
			// Dirty the binary search lists any time there's a ListChanged
			// notification. This will handle the case where the DataSource
			// is being set. 
			// MRS 6/1/06 - BR12914
			// Bump the listVersion instead of subtracting from the stored version number. 
			// This is better, because if it has to be updated in the future, it won't get missed. 
			//
//			this.DirtySortedDisplayMemberListVersion();
//			this.DirtySortedValueMemberListVersion();
			this.BumpListVersion();

			if ( this.ListChanged == null )
				return;

			this.ListChanged( this, new Infragistics.Win.ValueListChangedEventArgs( ) );
		}

		#endregion //	ListChanged event

		#endregion // IValueList Events

		#region IValueList Methods

		// SSP 5/16/02 
		// Added SortByValue property to IValueList interface.
		//
		/// <summary>
		/// Returns whether the owner should sort by values or display text. If it returns true, the owner should sort by value. Otherwise by display text.
		/// </summary>
		bool IValueList.SortByValue 
		{ 
			get
			{
				bool isDisplayMemeberSet = null != this.DisplayMember && this.DisplayMember.Length > 0;
				bool isValueMemeberSet = null != this.ValueMember && this.ValueMember.Length > 0;

				// If both are set and they are different, then and only then use the display text
				// for the ultra drop down, otherwise use the values for comparision like
				// we were doing before.
				//
				if ( isDisplayMemeberSet && isValueMemeberSet && 
					!this.DisplayMember.Equals( this.ValueMember ) )
					return false;

				return true;
			}
		}

		/// <summary>
		/// Moves to the next or previous item in the list
		/// </summary>
		/// <param name="previous">If true, moves to the previous item</param>
		void IValueList.MoveNextItemPreviousItem( bool previous )
		{
			try
			{
				// SSP 8/1/03 UWG1662
				// Exposed ReadOnly property off the UltraCombo.
				// Added IsReadOnly read-only (only get) virtual method to the UltraDropDownBase so
				// the any deriving class with a ReadOnly or a similar property can override this
				// and specify that the drop down should be read-only.
				// If the control's read-only, then return true indicating that it did not succeed.
				//
				// --------------------------------------------------------------------------------
				if ( this.IsReadOnly )
					return;
				// --------------------------------------------------------------------------------

				IValueList valueList = this;

				// SSP 11/25/02 UWG1860
				// Take into account hidden rows. Incremeneting the SelectedItemIndex can potentially
				// end up setting hte SelectedItemIndex property to an index of a hidden row.
				//
				// ----------------------------------------------------------------------------
				

				UltraGridRow currentRow = null;

				if ( valueList.SelectedItemIndex >= 0 && valueList.SelectedItemIndex < this.Rows.Count )
					currentRow = this.Rows[ valueList.SelectedItemIndex ];

				UltraGridRow row = null;

				if ( null == currentRow )
				{
					// If there is no current selected row then select the first visible row.
					row = this.Rows.GetRowAtVisibleIndex( 0 ); 
				}
				else
				{
					// Otherwise get the prevous or next row depending on the value of
					// previous.
					row = this.Rows.GetRowAtVisibleIndexOffset( currentRow, previous ? -1 : 1 );
				}

                //  BF 3/20/09  TFS15305
                //  Summary rows and anything else that cannot be navigated to
                //  should not be counted here, so that we don't clear the
                //  selection.
				//if ( null != row )
				if ( row != null && row.Index >= 0 )
					valueList.SelectedItemIndex = row.Index;
				// ----------------------------------------------------------------------------
			}
			catch ( Exception e )
			{
				Debug.Assert( false, e.Message );
			}
		}

		void IValueList.Activate( IValueListOwner owner )
		{
			if ( this.owner == owner )
				return;

			if ( this.owner != null && ((IValueList)this).IsDroppedDown )
				((IValueList)this).CloseUp();

			this.owner = owner;			

		}

		void IValueList.CloseUp()
		{
			
			if ( this.IsDroppedDown )
				Infragistics.Win.DropDownManager.CloseDropDown( this.owner != null ? this.owner.Control : null );
		}

		
		void IValueList.DeActivate( IValueListOwner owner )
		{

			if ( this.owner == owner )
			{
				if ( ((IValueList)this).IsDroppedDown )
					((IValueList)this).CloseUp();
				
				//detach owner
				this.owner = null;				
			}		

		}

		string IValueList.GetText( int index )
		{
			
			if ( index >= this.Rows.Count )
				throw new IndexOutOfRangeException( Shared.SR.GetString("LE_Exception_273") );

			// MRS 7/25/05 - BR04641
			// Replaced this with a helper method
//			// SSP 8/1/02 UWG1454
//			// Initialize the value memeber to the first column if not set.
//			// 
//			this.InitializeValueMemberIfToFirstColumn( );
//
//			// AS 1/8/03 - fxcop
//			// Do not compare against string.empty - test the length instead
//			//if ( index < 0 || this.DisplayMemberResolved == string.Empty 
//			if ( index < 0 || this.DisplayMemberResolved == null || this.DisplayMemberResolved.Length == 0 
//				
//				// SSP 7/23/02 UWG1420 UWG1345
//				// If the data source has not been initialized, then also return empty string
//				// instead of throwing key not found.
//				//
//				|| !this.IsInitializedWithDataSource 
//				)
//				return string.Empty;
//			
//			UltraGridColumn column = this.DisplayLayout.Bands[0].Columns[this.DisplayMemberResolved];
//
//			if ( column == null)
//				throw new ArgumentException( SR.GetString("LER_Exception_324", this.DisplayMemberResolved), this.DisplayMemberResolved );
			// MRS 7/25/05 - BR04641
			if ( index < 0 ||
				!this.IsInitializedWithDataSource )
			{
				return string.Empty;
			}

			UltraGridColumn column = this.GetDisplayMemberColumnResolved();

			return this.Rows[index].GetCellText(column);
		}

		void IValueList.ScrollPageUpPageDown( bool pageUp )
		{
			// SSP 8/1/03 UWG1662
			// Exposed ReadOnly property off the UltraCombo.
			// Added IsReadOnly read-only (only get) virtual method to the UltraDropDownBase so
			// the any deriving class with a ReadOnly or a similar property can override this
			// and specify that the drop down should be read-only.
			// If the control's read-only, then return true indicating that it did not succeed.
			//
			// --------------------------------------------------------------------------------
			if ( this.IsReadOnly )
				return;
			// --------------------------------------------------------------------------------

			if ( this.DisplayLayout.BandZero == null )
				return;

			// SSP 4/7/03 UWG2166
			// If the value list is not dropped down, then return without doing page up and down
			// operation.
			//
			if ( ! this.IsDroppedDown )
				return;

			UltraGridRow newRow = null;

			// SSP 4/30/05 - NAS 5.2 Fixed Rows_
			// Added PerformPageUpDownHelper method. Use that method instead which takes into
			// account the fixed rows.
			//
			// ------------------------------------------------------------------------------
			newRow = this.DisplayLayout.PerformPageUpDownHelper( 
				pageUp ? UltraGridAction.PageUpRow : UltraGridAction.PageDownRow, this.SelectedRow );
			
			// ------------------------------------------------------------------------------
		
			// SSP 4/7/03 UWG2166
			// Check for newRow being null.
			//
			//if ( this.SelectedRow != newRow )
			if ( null != newRow && this.SelectedRow != newRow )
			{
				//Set the selected row
				//
				this.SelectedRow = newRow;

				// Notify the owner that the index has changed
				//
				this.owner.OnSelectedItemChanged();
			}		
		}

		void IValueList.MoveFirstItemLastItem( bool last )
		{
			// SSP 8/1/03 UWG1662
			// Exposed ReadOnly property off the UltraCombo.
			// Added IsReadOnly read-only (only get) virtual method to the UltraDropDownBase so
			// the any deriving class with a ReadOnly or a similar property can override this
			// and specify that the drop down should be read-only.
			// If the control's read-only, then return true indicating that it did not succeed.
			//
			// --------------------------------------------------------------------------------
			if ( this.IsReadOnly )
				return;
			// --------------------------------------------------------------------------------

			//	If the list is empty, do nothing
			if ( this.Rows.Count == 0 )
				return;

			if ( last )
			{
				// AS 5/21/04 UWG3009
				//this.SelectedRow = this.ActiveRowScrollRegion.GetLastVisibleRow( this.DisplayLayout.BandZero );
				this.SelectedRow = this.Rows.GetLastVisibleRow();
			}
			else
			{
				// AS 5/21/04 UWG3009
				//this.SelectedRow = this.ActiveRowScrollRegion.GetFirstVisibleRow( this.DisplayLayout.BandZero );
				this.SelectedRow = this.Rows.GetFirstVisibleRow( );
			}
		}

		
		#region InitializeValueMemberIfToFirstColumn

		// SSP 7/19/02 UWG1383
		// Added InitializeValueMemberIfToFirstColumn because we may need to do this from
		// multiple places.
		//
		internal void InitializeValueMemberIfToFirstColumn( )
		{
			// SSP 10/8/02 UWG1736 UWG1734
			// Load the bands with the data source if not already done.
			//
			// SSP 927/07 BR26652
			// Check for Initializing before accessing BindingContext because that can cause problems
			// if the InitializeComponent hasn't finished.
			// 
			//if ( null == this.BindingManager && null != this.DataSource && null != this.BindingContext )
			if ( ! this.Initializing && null == this.BindingManager 
				&& null != this.DataSource && null != this.BindingContext )
			{
				this.DataBind( );
			}

			
		}

		#endregion // InitializeValueMemberIfToFirstColumn
	
		// SSP 7/23/02 UWG1420 UWG1345
		// 
		internal bool IsInitializedWithDataSource
		{
			get
			{
				// SSP 927/07 BR26652
				// Check for Initializing before accessing BindingContext because that can cause problems
				// if the InitializeComponent hasn't finished.
				// 
				//return null != this.BindingContext && this.DisplayLayout.Bands[0].Columns.Count > 0;
				return ! this.Initializing &&  null != this.BindingContext 
					&& this.DisplayLayout.SortedBands.Count > 0 && this.DisplayLayout.SortedBands[0].Columns.Count > 0;
			}
		}

		string IValueList.GetText( object dataValue, ref int index )
		{
			#region Old Code
			// MRS 7/25/05 - BR04641
			// Replaced this with a helper method
//			// SSP 7/19/02 UWG1383
//			// Initialize the value memeber to the first column if not set.
//			// 
//			this.InitializeValueMemberIfToFirstColumn( );
//
//			// SSP 7/23/02 UWG1420 UWG1345
//			// Don't throw an exception if value member and display member haven't been
//			// set. Use the first column as the display member and the value memebr.
//			//
//			// -------------------------------------------------------------------------
//			/*
//			//RobA UWG544 11/16/01
//			if ( this.ValueMemberResolved == string.Empty )
//			{
//				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_275"), Shared.SR.GetString("LE_ArgumentNullException_276") );					
//			}
//			
//			UltraGridColumn column = this.DisplayLayout.Bands[0].Columns[this.ValueMemberResolved];
//			
//			if ( column == null)
//				throw new ArgumentException( SR.GetString("LER_Exception_324",this.ValueMemberResolved), this.ValueMemberResolved );
//			*/
//			UltraGridColumn column = null;
//			
//			if ( this.DisplayLayout.Bands[0].Columns.Exists( this.ValueMemberResolved ) )
//				column = this.DisplayLayout.Bands[0].Columns[this.ValueMemberResolved];
//			
//			// Only throw an exception if the user had set the value member to something.
//			//
//			if ( column == null && 
//				null != this.ValueMemberResolved && this.ValueMemberResolved.Length > 0 &&
//				this.IsInitializedWithDataSource )
//				throw new ArgumentException( SR.GetString("LER_Exception_324",this.ValueMemberResolved), this.ValueMemberResolved );
//			// -----------------------------------------------------------------------------------
			#endregion Old Code

			// MRS 6/1/06 - BR12914
			// Moved the code from this method into a helper method
			string returnValue = this.GetTextHelper(dataValue, ref index);

			if (index == -1)
				this.lastFailedValueMatch = dataValue;
			else
				this.lastFailedValueMatch = notSet;

			this.lastFailedValueMatchVersion = this.GetSortedListVersion();

			return returnValue;
		}
			
		// MRS 7/25/05 - BR04641
		// Created this helper method from 'string IValueList.GetText( object dataValue, ref int index )'
		private string GetTextHelper( object dataValue, ref int index )
		{
			UltraCombo ultraCombo = this as UltraCombo;

			// SSP 10/11/06 - NAS 6.3
			// Added SetInitialValue on the UltraCombo.
			// 
			if ( null != ultraCombo && ultraCombo.dontLoadDataSource )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( this.DoesDataValueMatch( dataValue, ultraCombo.dontLoadDataSource_initialDataValue ) )
				if ( UltraDropDownBase.DoesDataValueMatch( dataValue, ultraCombo.dontLoadDataSource_initialDataValue ) )
				{
					index = -1;
					return ultraCombo.dontLoadDataSource_initialDisplayText;
				}
			}

			// MRS 4/18/06 - BR11468
			if (ultraCombo != null && 
				ultraCombo.AllowNull == DefaultableBoolean.True)
			{
				if (dataValue == null 
					|| dataValue is DBNull
					|| (dataValue is string && ((string)dataValue).Length == 0) )
				{
					index = -1;
					return null;
				}
			}

			// MRS 6/1/06 - BR12914
			// Whenever we fail to find a match, keep track of the value.  
			// If the dataValue passed in is a value we already failed to find, we know
			// won't find a match. 
			if (this.lastFailedValueMatch != notSet &&
				this.lastFailedValueMatchVersion == this.GetSortedListVersion() &&
				object.Equals(this.lastFailedValueMatch, dataValue))
			{
				index = -1;
				return null;
			}			
			this.lastFailedValueMatch = notSet;	
			
			UltraGridColumn column = this.GetValueMemberColumnResolved();

			if ( column != null )
			{
				if ( index >= 0 && index < this.Rows.Count )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//if ( this.DoesDataValueMatch( this.Rows[index].GetCellValue(column),
					if ( UltraDropDownBase.DoesDataValueMatch( this.Rows[ index ].GetCellValue( column ),
						dataValue ) )
						//RobA UWG389 9/28/01 
						//return this.Rows[index].GetCellText(column);
						return ((IValueList)this).GetText(index);
					
				}

				#region Old Code
				// MRS 11/1/05 - BR07505
				// Put this old code back in, in case Binary Searching is not available.
				// Also use the old method if the value we are searching for is
				// not IComparable, Null, or DBNull.
				//
				//				// MRS 7/25/05 - BR04641
				//				// Do a Binary Search instead. 
				//				//
				//				//				for ( int i=0; i< this.Rows.Count; ++i)
				//				//					if ( this.DoesDataValueMatch( this.Rows[i].GetCellValue(column),
				//				//						dataValue) )
				//				//					{
				//				//						index = i;
				//				//						//RobA UWG389 9/28/01 
				//				//						//return this.Rows[i].GetCellText(column);
				//				//						return ((IValueList)this).GetText(i);						
				//				//					}
				//				object displayText = this.PerformBinarySearch(dataValue, ref index, false);
				//				if (index != -1)
				//				{
				//					return (string)displayText;
				//				}
				#endregion Old Code

				if (this.CanDoBinarySearch)
				{
					// MRS 12/28/05 - If lastValueMemberListType is null, we can't do a Binary search.
					//bool canDoBinarySearch = true;
					bool canDoBinarySearch = (this.lastValueMemberListType != null);					
					
					//if (dataValue != null)
					if (dataValue != null && canDoBinarySearch)
					{
						// Try to convert the DataValue to the same type as the items in the column.
						dataValue = ValueConstraint.ValueToDataValue(dataValue, this.lastValueMemberListType, column.FormatInfo, column.Format);

						// If it comes back null, the conversion failed and
						// we cannot do a Binary search.
						if (dataValue == null)
							canDoBinarySearch = false;
					}

					if (canDoBinarySearch)
					{
						object displayText = this.PerformBinarySearch(dataValue, ref index, false);
						if (index != -1)
							return (string)displayText;
						else
						{
							index =-1;
							return null;
						}
					}
				}

				// MRS 9/28/06 - BR15784
				// Moved this block of code up. It used to be outside the 
				// 'if ( column != null )' check. But it needs to be inside, because
				// otherwise, it blows up when the column is null. 
				//
				// We could not do a binary search for whatever reason, so fall back to the old
				// search method. 
				//
				// MRS 7/25/05 - BR04641
				// Optimization
				//
				//			for ( int i=0; i< this.Rows.Count; ++i)
				//				if ( this.DoesDataValueMatch( this.Rows[i].GetCellValue(column),
				RowsCollection rows = this.Rows;
				int rowCount = rows.Count;
				for ( int i=0; i< rowCount; ++i)
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//if ( this.DoesDataValueMatch( rows[i].GetCellValue(column),
					if ( UltraDropDownBase.DoesDataValueMatch( rows[ i ].GetCellValue( column ),
						dataValue) )
					{
						index = i;
						//RobA UWG389 9/28/01 
						//return this.Rows[i].GetCellText(column);
						return ((IValueList)this).GetText(i);						
					}
			}

			// Since we didn't find a match set Index to -1
			//
			index = -1;	
			return null;
		}

		object IValueList.GetValue( int index )
		{
			if ( index >= this.Rows.Count )
				throw new IndexOutOfRangeException( Shared.SR.GetString("LE_Exception_273") );

			// MRS 7/25/05 - BR04641
			// Replaced this with a helper method
			//
//			// SSP 8/1/02 UWG1454
//			// Initialize the value memeber to the first column if not set.
//			// 
//			this.InitializeValueMemberIfToFirstColumn( );
//
//			// AS 1/8/03 - fxcop
//			// Do not compare against string.empty - test the length instead
//			//if ( index < 0 || this.ValueMemberResolved == string.Empty 
//			if ( index < 0 || this.ValueMemberResolved == null || this.ValueMemberResolved.Length == 0 
//				
//				// SSP 7/23/02 UWG1420 UWG1345
//				// If the data source has not been initialized, then also return empty string
//				// instead of throwing key not found.
//				//
//				|| !this.IsInitializedWithDataSource )
//				return null;
//			
//			UltraGridColumn column = this.DisplayLayout.Bands[0].Columns[this.ValueMemberResolved];
//
//			if ( column == null)
//				throw new ArgumentException( SR.GetString("LER_Exception_324", this.ValueMemberResolved), this.ValueMemberResolved );
			if ( index < 0  ||
				!this.IsInitializedWithDataSource )
			{
				return null;			
			}

			UltraGridColumn column = this.GetValueMemberColumnResolved();

			return this.Rows[index].GetCellValue(column);	
			
		}

		object IValueList.GetValue( string text, ref int index )
		{
			#region Old Code
			// SSP 7/23/02 UWG1420 UWG1345
			// Don't throw an exception if value member and display member haven't been
			// set. Use the first column as the display member and the value memebr.
			// Just degrade the functionality so that it doesn't do any thing.
			//
			// ----------------------------------------------------------------------------------
			
			#endregion Old Code

			// MRS 7/25/05 - BR04641
			// Moved the code into a helper method
			object returnValue = this.GetValueHelper(text, ref index);
			if (index == -1)
				this.lastFailedTextMatch = text;
			else
				this.lastFailedTextMatch = notSet;

			this.lastFailedTextMatchVersion = this.GetSortedListVersion();

			return returnValue;
		}
		
		// MRS 7/25/05 - BR04641
		// Created this helper method from 'object IValueList.GetValue( string text, ref int index )'
		private object GetValueHelper( string text, ref int index )
		{
			UltraGridColumn column = null;

			// SSP 5/24/05
			// If the control has been disposed off then return null.
			// 
			if ( null == this.DisplayLayout )
			{
				index = -1;
				return null;
			}

			#region Old Code
			// MRS 7/25/05 - BR04641
			// Replaced this with a helper method
			//
			//			// SSP 3/6/03 UWG2031
			//			// Initialize the value memeber if it's not set.
			//			//
			//			this.InitializeValueMemberIfToFirstColumn( );
			//			
			//			if ( this.DisplayLayout.Bands[0].Columns.Exists( this.DisplayMemberResolved ) )
			//				column = this.DisplayLayout.Bands[0].Columns[this.DisplayMemberResolved];
			//
			//			if ( column == null && null != this.DisplayMemberResolved && this.DisplayMemberResolved.Length > 0 &&
			//				this.IsInitializedWithDataSource )
			//				throw new ArgumentException( SR.GetString("LER_Exception_324",this.DisplayMemberResolved), this.DisplayMemberResolved );
			//			// ----------------------------------------------------------------------------------
			#endregion Old Code

			UltraCombo ultraCombo = this as UltraCombo;

			// SSP 10/11/06 - NAS 6.3
			// Added SetInitialValue on the UltraCombo.
			// 
			if ( null != ultraCombo && ultraCombo.dontLoadDataSource )
			{
				if ( this.DoesTextMatch( text, ultraCombo.dontLoadDataSource_initialDisplayText, false ) )
				{
					index = -1;
					return ultraCombo.dontLoadDataSource_initialDataValue;
				}
			}

			// MRS 8/30/06 - BR15208
			if (ultraCombo != null && 
				ultraCombo.AllowNull == DefaultableBoolean.True)
			{
				if (text == null ||
					text.Length == 0 ||
					text == ultraCombo.NullText )
				{					
					index = -1;
					return null;
				}
			}

			column = this.GetDisplayMemberColumnResolved();

			if ( column != null )
			{

				// MRS 7/25/05 - BR04641
				// Whenever we fail on a match, keep track of the failed text. 
				// If the new text starts is the same as the failed text, we know we
				// won't find a match. 
				if (this.lastFailedTextMatch != notSet &&
					this.lastFailedTextMatchVersion == this.GetSortedListVersion() &&
					object.Equals(text, this.lastFailedTextMatch))
				{
					index = -1;
					return null;
				}			
				this.lastFailedTextMatch = notSet;		

				if ( index >= 0 && index < this.Rows.Count )
				{
					if ( this.DoesTextMatch( this.Rows[index].GetCellText(column), text, false ) )
						return ((IValueList)this).GetValue(index);
				}
				
				// MRS 12/12/05 - BR08135
				// There's no reason to check for CanDoBinarySearch. 
				// We can always search on a text field. 
				//
				//				// MRS 7/25/05 - BR04641
				//				// See if we can do a Binary Search
				//				if ( this.CanDoBinarySearch )
				//				{					
				//					object dataValue = this.PerformBinarySearch(text, ref index, true);
				//					if (index != -1)
				//						return dataValue;
				//				}
				//				else
				//				{
				//					for ( int i=0; i< this.Rows.Count; ++i)
				//						if ( this.DoesTextMatch( this.Rows[i].GetCellText(column), text, false) )
				//						{
				//							index = i;
				//							return ((IValueList)this).GetValue(i);
				//						}
				//				}

				// MRS 4/18/06 - BR11468
				// Now that I have added a DropDownSearchMethod property, there are cases when we can't do a Binary Search.
				if (this.DropDownSearchMethod != DropDownSearchMethod.Linear)
				{
					// MRS 12/12/05 - BR08135
					// Since we are not calling CanDoBinarySearch, we need to 
					// verify that the sorted list is up-to-date. 				
					if (this.DoesSortedDisplayMemberListNeedRebuilding())
						this.BuildSortedDisplayMemberList();

					object dataValue = this.PerformBinarySearch(text, ref index, true);
					if (index != -1)
						return dataValue;
				}
				else
				{
					// MRS 7/25/05 - BR04641
					// Optimization
					//
					//for ( int i=0; i< this.Rows.Count; ++i)					
					//	if ( this.DoesTextMatch( this.Rows[i].GetCellText(column), text, false) )					
					RowsCollection rows = this.Rows;
					int rowCount = rows.Count;
					for ( int i=0; i< rowCount; ++i)
						if ( this.DoesTextMatch( rows[i].GetCellText(column), text, false) )
						{
							index = i;
							return ((IValueList)this).GetValue(i);
						}
				}
			}

			// Since we didn't find a match set Index to -1
			//
			index = -1;	
			return null;

		}

		object IValueList.GetValue( string text, ref int index, int beginAtIndex, bool partial )
		{
			//if not partial just do a regular getvalue
			//
			if ( !partial )
				return ((IValueList)this).GetValue( text, ref index );

			// MRS 4/18/06 - BR11468
			UltraCombo ultraCombo = this as UltraCombo;
			if (ultraCombo != null && 
				ultraCombo.AllowNull == DefaultableBoolean.True)
			{
				if (text == null || text.Length == 0 || 
					text == ultraCombo.NullText)
				{
					index = -1;
					return null;
				}
			}

			// MRS 7/25/05 - BR04641
			// Replaced this with a helper method
			//
//			// SSP 7/23/02 UWG1420 UWG1345
//			// Don't throw an exception if value member and display member haven't been
//			// set. Use the first column as the display member and the value memebr.
//			// Just degrade the functionality so that it doesn't do any thing.
//			//
//			// ---------------------------------------------------------------------------
//			/*
//			// RobA UWG544 11/16/01
//			//
//			if ( this.DisplayMemberResolved == string.Empty )
//			{
//				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_277"), Shared.SR.GetString("LE_ArgumentNullException_277") );					
//			}
//			
//			UltraGridColumn column = this.DisplayLayout.Bands[0].Columns[this.DisplayMemberResolved];
//			if ( column == null)
//
//				throw new ArgumentException( SR.GetString("LER_Exception_324",this.DisplayMemberResolved ), this.DisplayMemberResolved );
//			*/
//			UltraGridColumn column = null;
//			
//			if ( this.DisplayLayout.Bands[0].Columns.Exists( this.DisplayMemberResolved ) )
//				column = this.DisplayLayout.Bands[0].Columns[this.DisplayMemberResolved];
//
//			if ( column == null && null != this.DisplayMemberResolved && this.DisplayMemberResolved.Length > 0 &&
//				this.IsInitializedWithDataSource )
//				throw new ArgumentException( SR.GetString("LER_Exception_324",this.DisplayMemberResolved), this.DisplayMemberResolved );
//			// ---------------------------------------------------------------------------
			
			UltraGridColumn column = this.GetDisplayMemberColumnResolved();

			// SSP 4/1/02 UWG1062
			// If there are no rows to be searched, then return null as if there
			// was no match found instead of throwing the exception about beginAtIndex
			// being out of range below.
			//
			if ( this.Rows.Count <= 0 )
			{
				// Set the index to -1 since there was no match.
				//
				index = -1;
				return null;
			}

			// MRS 7/25/05 - BR04641
			// Whenever we fail on a partial match, keep track of the failed value. 
			// If the new text starts with the first failed text, we know we
			// won't find a match. 
			if (this.lastFailedPartialMatch != null &&
				this.lastFailedPartialMatchVersion == this.GetSortedListVersion() &&
				text.StartsWith(this.lastFailedPartialMatch))
			{
				index = -1;
				return null;
			}			
			this.lastFailedPartialMatch = null;			

			if ( beginAtIndex < 0 )
				beginAtIndex = 0;

			if ( beginAtIndex >= this.Rows.Count )
				throw new IndexOutOfRangeException(Shared.SR.GetString("LE_Exception_277"));
			
			// Check the last index for a match first so we can optiimize for
			// the great majority of situations
			//
			if ( index > 0 && 
				index >= beginAtIndex && 
				index < this.Rows.Count
				// SSP 4/30/03 UWG2216
				// Skip hidden rows. This method gets called to find the matching item when the user
				// presses a character. We shouldn't match ths row if it's hidden. Also only do this
				// when partial is true because this method also gets called to find the value.
				// 
				// SSP 8/1/03 UWG1654 - Filter Action
				// Use HiddenInternal instead of the public Hidden behavior of which we changed in regards to filtering.
				//
				//&& ( !partial || !this.Rows[index].Hidden )
				&& ( !partial || !this.Rows[index].HiddenInternal )
				)
			{
				string cellText = this.Rows[index].GetCellText(column);						

				// see if we match with the passed in text
				//
				if ( this.DoesTextMatch( text, cellText, partial ) )
					return ((IValueList)this).GetValue(index);
					

			}

			// loop thru the collection looking for a match
			//
			for ( int i = beginAtIndex; i < this.Rows.Count; i++ )
			{
				// SSP 4/30/03 UWG2216
				// Skip hidden rows. This method gets called to find the matching item when the user
				// presses a character. We shouldn't match ths row if it's hidden. Also only do this
				// when partial is true because this method also gets called to find the value.
				// 
				// SSP 8/1/03 UWG1654 - Filter Action
				// Use HiddenInternal instead of the public Hidden behavior of which we changed in regards to filtering.
				//
				//if ( partial && this.Rows[i].Hidden )
				if ( partial && this.Rows[i].HiddenInternal )
					continue;

				string cellText = this.Rows[i].GetCellText(column);	

				// when we find a match return the object
				//
				if ( this.DoesTextMatch( text, cellText, partial ) )
				{
					// Update the Index
					//
					index = i;
					return ((IValueList)this).GetValue(i);
 					
				}
			}

			// MRS 11/4/05 - BR07275
			// Only store the failed value when the beginAtIndex is 0, meaning
			// we searched the entire list.
			//
			if (beginAtIndex == 0)
			{
				// MRS 7/25/05 - BR04641
				// Store the text that failed to match so the next time we get
				// into this routine, we can bail if the new text starts with the
				// same thing that already failed. 
				if (text != null && text.Length == 0)
					this.lastFailedPartialMatch = null;
				else
					this.lastFailedPartialMatch = text;

				this.lastFailedPartialMatchVersion = this.GetSortedListVersion();
			}

			// Since we didn't find a match set Index to -1
			//
			index = -1;			
			return null;		
		}

		internal bool DoesTextMatch( string partialText, string fullText, bool partial )
		{
			// RobA UWG735 11/14/01
			//
			// AS 1/8/03 - fxcop
			// Do not compare against string.empty - test the length instead
			//if ( partialText == string.Empty )
			// SSP 5/24/05 BR04236
			// If the text we are looking for is empty string and the item's display text
			// is also empty string then they should match.
			//
			//if ( partialText == null || partialText.Length == 0 )
			if ( partialText == null || partial && partialText.Length == 0 )
				return false;

			// on an exact match do a no case compare 
			//
			if ( !partial )
			{
				// AS 1/8/03 - fxcop
				// Must supply an explicit CultureInfo to the String.Compare method.
				//return ( 0 == String.Compare( partialText, fullText, true ) );
				return ( 0 == String.Compare( partialText, fullText, true, System.Globalization.CultureInfo.CurrentCulture ) );
			}

			// on a partial match first get both string lengths
			//
			int partialStringLength    = partialText.Length;
			int fullLength 			   = fullText.Length;

			// if the lengths are the same call this routine with a partial
			// flag set to false
			//
			if ( partialStringLength == fullLength )
				return this.DoesTextMatch( partialText, fullText, false ); 

			// if the passed in string is shorter that our string then
			// only compare that may characters
			//			
			if ( partialStringLength < fullLength )
			{
				// AS 1/8/03
				// I don't think this should be InstalledUICulture
				//return 0 == String.Compare( fullText, 0, partialText, 0, partialStringLength, 
				//	true, System.Globalization.CultureInfo.InstalledUICulture );
				return 0 == String.Compare( fullText, 0, partialText, 0, partialStringLength, 
					true, System.Globalization.CultureInfo.CurrentCulture );
			}
 
			return false;
		}


		bool IValueList.IsDroppedDown
		{
			get
			{
				return this.IsDroppedDown;			
			}
		}


		// SSP 1/2/02 UWG860
		// Added this method because we need to be able to resolve appearance
		// for individual value list items. So added it to the IValueList 
		// interface and thus has to be imlemented here.
		//
		/// <summary>
		/// Resolves appearance for individual items in the value list.
		/// </summary>
		/// <param name="itemIndex">Index of the item to resolve the appearance for.</param>
		/// <param name="appData">AppearanceData structure where resolved appearance data is stored.</param>
		/// <param name="flags">Properties to resolve.</param>
		/// <param name="componentRole">Component role.</param>
		// SSP 4/26/06 - App Styling
		// Added componentRole parameter to the ResolveItemAppearance. We need this so we can get the app-style
		// ValueListItem role and resolve it's appearance.
		// 
		//void IValueList.ResolveItemAppearance( int itemIndex, ref AppearanceData appData, ref AppearancePropFlags flags )
		void IValueList.ResolveItemAppearance( int itemIndex, ref AppearanceData appData, ref AppearancePropFlags flags, AppStyling.ComponentRole componentRole )
		{
			RowsCollection rows = this.DisplayLayout.Rows;

			if ( itemIndex < 0 || itemIndex >= rows.Count )
				throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_278"), Shared.SR.GetString("LE_ArgumentOutOfRangeException_364") );
					
			UltraGridRow row = this.DisplayLayout.Rows[ itemIndex ];

            // MRS 3/16/2009 - TFS15302
            // We should really be using the cell appearance for the DisplayMember column here
            // instead of the row. But to minimize potential breakage, I'm 
            // leaving the row code in, but resolving the cell appearance first. 
            //
            UltraGridColumn displayMemberColumn = this.GetDisplayMemberColumnResolved();
            UltraGridCell cell = row.Cells[this.DisplayMemberResolved];
            if (cell.HasAppearance)
            {
                cell.Appearance.MergeData(ref appData, ref flags);
            }

			if ( row.HasAppearance )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, row.Appearance.Data, ref flags );
				row.Appearance.MergeData( ref appData, ref flags );
			}
		}

		int IValueList.SelectedItemIndex
		{
			get
			{				
				if ( this.selectedRow != null )
					return this.selectedRow.Index;				

				return -1;
			}

			set
			{
				// MRS 10/28/04 - UWG2996
				// Funnel this and SelectedRow through a common method
				this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.SelectedIndex, value);

                #region Obsolete code
                //				// SSP 4/16/02 UWG1095
				//				// SelectedItemIndex and SelectedRow properties call each other to syncronize.
				//				// So I added these flags to prevent any infinite loops and also enclosed the
				//				// following block of code in try-finally.
				//				//
				//				this.inSelectedItemIndex = true;
				//				
				//				try
				//				{
				//
				//				if ( value >= 0 )
				//                {
				//					if ( value >= this.Rows.Count )
				//						return;
				//
				//					// SSP 4/16/02 UWG1095
				//					// SelectedItemIndex and SelectedRow properties call each other to syncronize.
				//					// So I added these flags to prevent any infinite loops. So don't update
				//					// the selected row if selected row is the one that called us.
				//					//
				//					if ( !this.inSelectedRow )
				//					{
				//						this.SelectedRow = this.Rows[value];								
				//					}							
				//					
				//					//scroll row into view
				//					//
				//					// SSP 7/8/02 UWG1162
				//					// Don't scroll the row into view when the drop down is not
				//					// dropped down because at that point the extents of scroll regions
				//					// may not be set to correct values. When drop down is dropped down,
				//					// then we know the right size of the drop down and the right
				//					// extents of scroll regions would be set. So scroll the selected
				//					// row into view right before dropping down and here only scroll
				//					// when we are dropped down.
				//					//
				//					// AS 1/27/04 UWG2923
				//					// If the SelectedRow gets set to null in the RowSelected
				//					// that is fired above, then there is nothing to scroll
				//					// into view.
				//					//
				//					//if ( ((IValueList)this).IsDroppedDown )
				//					if ( this.selectedRow != null && ((IValueList)this).IsDroppedDown )
				//						this.ActiveRowScrollRegion.ScrollRowIntoView( this.selectedRow );
				//					
				//					// JJD 9/27/01
				//					// Notify the owner that the index has changed
				//					//
				//					if ( this.owner != null )
				//						this.owner.OnSelectedItemChanged();
				//				}
				//
				//				else
				//					this.SelectedRow = null;
				//
				//				}
				//				finally
				//				{
				//					this.inSelectedItemIndex = false;
				//				}
                #endregion Obsolete code
			}			
		}

		int IValueList.ItemCount
		{
			get
			{
				return this.Rows.Count;
			}
		}

		bool IValueList.DropDown( Rectangle editAreaInScreenCoords, int index, string text )
		{

			//Fire before event and return if cancelled
			//
			if ( this.FireBeforeDropDown( ) )
				return false;

			// JJD 1/25/02 - UWG998
			// If the ValueMember has still not been set then initialize
			// it to the first column.
			//
			// SSP 7/19/02 UWG1383
			// Moved below code into new InitializeValueMemberIfToFirstColumn method so
			// call that.
			//
			// -----------------------------------------------------------------------
			
			this.InitializeValueMemberIfToFirstColumn( );
			// -----------------------------------------------------------------------

			// SSP 7/23/02 UWG1420 UWG1345
			// Don't throw an exception if value member and display member haven't been
			// set. Use the first column as the display member and the value memebr.
			// Just degrade the functionality so that it doesn't do any thing.
			//
			

			// JJD 11/02/01
			// Make sure we rest the first draw flag which is used to
			// bypass some logic like invalidation
			//
			this.ResetFirstDrawFlag();

			// set a flag so we know that we have to fire the closeup event
			//
			this.dropdownEventFired = true;

            //  BF 3/5/08   FR09238 - AutoCompleteMode
            EditorWithCombo.AutoCompleteInfo autoCompleteInfo = this.AutoCompleteInfo;
            bool isFilteredDropDownVisible = autoCompleteInfo != null && autoCompleteInfo.IsFilteredDropDownVisible;

			// SSP 12/2/02 UWG1838
			// We need to know which row is going to be the first row in order to figure out
			// the proper height of the drop down since rows could be of variable heights.
			// Copied code from below to here and left the original code where it was since
			// I don't think it will be harmful to leave it there and may be even necessay.
			//
			// ----------------------------------------------------------------------------
			IValueList list = this as IValueList;

            //  BF 3/5/08   FR09238 - AutoCompleteMode
            //
            //  Don't set the initial selection when dropping down
            //  if we are in auto-complete suggest mode.
            //
            //if ( list != null )
			if ( list != null && isFilteredDropDownVisible == false )
			{
                //  BF 12/8/08  NA 9.1 - UltraCombo MultiSelect
                //  Externalized this so UltraCombo can do something different
                //  when its value is determined by the checked item list.
                #region Refactored
                //if ( list.GetValue( text, ref index ) != null )
                //    list.SelectedItemIndex = index;
                //else
                //    list.SelectedItemIndex = -1;
                #endregion Refactored

                this.SetSelectedIndexBeforeDroppingDown( text, ref index );
			}
			// ----------------------------------------------------------------------------

			// MRS 11/30/04 - BR00048
			// We need to copy the font over to the ControlForGridDisplay before we calculate the DropDown Size.
			// Otherwise, it will get re-parented to a control that does not have a font setting and 
			// it will not pick up the font settings from the form. 
			this.ControlForGridDisplay.Font = this.Font;

			// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
			// Support HeaderClickAction in Combo and DropDown
			// Cache this, so we can use it in SetDropDownWidth
			this.lastEditAreaInScreenCoords = editAreaInScreenCoords;

			// AS 5/21/04 UWG3134
			//Size dropDownSize = this.CalculateDropDownSize( editAreaInScreenCoords );
			bool isRecalcRequired = false;
			Size dropDownSize = this.CalculateDropDownSize( editAreaInScreenCoords, true, ref isRecalcRequired );

			// if the scrollbar visibility was determined later on to 
			// be required then we need to recalculate without resetting those
			// members
			if (isRecalcRequired)
				dropDownSize = this.CalculateDropDownSize( editAreaInScreenCoords, false, ref isRecalcRequired );

			// JJD 10/01/02 - UWG408
			// Align the drop down to the right if the drop down is narrower
			// than the edit area
			//
			bool alignRight = ( dropDownSize.Width < editAreaInScreenCoords.Width );

			Point alignPoint;

			if ( alignRight )
				alignPoint = new Point( editAreaInScreenCoords.Right, editAreaInScreenCoords.Bottom );
			else
				alignPoint = new Point( editAreaInScreenCoords.Left, editAreaInScreenCoords.Bottom );

			// AS 12/19/05 - BR08024
			this.lastRequestedRect = new Rectangle(alignPoint, dropDownSize);
			this.isDropDownAlignedRight = alignRight;

			Infragistics.Win.DropDownManager.DropDown( this.owner.Control,
				this.ControlForGridDisplay,
				this.owner.EditControl,
				new EventHandler(this.OnCloseUp),
				editAreaInScreenCoords,
				dropDownSize,
				alignPoint,
				alignRight );
			
			// Set the selected item index to its current value. You
			// need to do this after the list is dropped down in order
			// to scroll the row into view
			//

			// RobA UWG503 11/21/01 
			// if the SelectedItemIndex is -1, just scroll to the top
			//
			// SSP 6/21/02 UWG1249
			// Before scrolling the row into view, we need to verify the ui element.
			// What happens is that initially when the drop down has not been dropped
			// down, all the ui elements are not positioned and scroll regions extents
			// are 0 and the scrolling fails because the extents of the scroll regions
			// are 0. So if we just verify the grid ui element, then the extents will
			// be set and scrolling will take place correctly.
			//
			if ( null != this.DisplayLayout )
				this.DisplayLayout.GetUIElement( true, true );

			// AS 8/17/06 NA 2006 Vol 3 - Office 2007 L&F
			if (null != this.DisplayLayout && null != this.owner)
			{
				for (int i = 0; i < this.DisplayLayout.ColScrollRegions.Count; i++)
					this.DisplayLayout.ColScrollRegions[i].ScrollBarInfo.ViewStyle = this.owner.ScrollBarViewStyle;

				for (int i = 0; i < this.DisplayLayout.RowScrollRegions.Count; i++)
					this.DisplayLayout.RowScrollRegions[i].ScrollBarInfo.ViewStyle = this.owner.ScrollBarViewStyle;
			}

			//	BF 7.18.02	UWG1387
			//	Use the IValueList.GetValue method to find the row
			//	that we are going to select and activate
			//
			// SSP 12/4/02 UWG1838
			//
			//IValueList list = this as IValueList;

            //  BF 3/5/08   FR09238 - AutoCompleteMode
            //
            //  Don't set the initial selection when dropping down
            //  if we are in auto-complete suggest mode.
            //
            //if ( list != null )
			if ( list != null && isFilteredDropDownVisible == false )
			{
				// SSP 9/4/03 UWG2258
				// Enlcose the scrolling logic in BeginUpdate and EndUpdate so there is no flicker.
				// Optimizations in the scrolling logic call Update to immediately update any small
				// invalidate regions.
				//
				try
				{
					this.BeginUpdate( );

                    //  BF 12/8/08  NA 9.1 - UltraCombo MultiSelect
                    //  We do the exact same thing a few lines up, and based on
                    //  the notes for UWG1838, it is intentional, so I am leaving
                    //  that aspect as it was.
                    #region Refactored
                    //if ( list.GetValue( text, ref index ) != null )
                    //    list.SelectedItemIndex = index;
                    //else
                    //    list.SelectedItemIndex = -1;
                    #endregion Refactored

                    this.SetSelectedIndexBeforeDroppingDown( text, ref index );


					// MRS 11/16/04
					// Made a Helper Method
					//					// SSP 9/4/03 UWG2258
					//					// Make sure there is no empty space after the last visible row.
					//					//
					//					// --------------------------------------------------------------------------
					//					if ( null != this.ActiveRowScrollRegion 
					//						&& null != this.SelectedRow 
					//						&& this.ActiveRowScrollRegion.IsLastRowVisible( false ) )
					//					{
					//						this.ActiveRowScrollRegion.Scroll( RowScrollAction.Top );
					//						this.ActiveRowScrollRegion.ScrollRowIntoView( this.SelectedRow );
					//					}
					//					// --------------------------------------------------------------------------
					// MRS 1/21/05 - BR01917
					//this.ScrollSelectedRowIntoView();
					this.ScrollSelectedRowIntoView(true);

				}
				finally
				{
					this.EndUpdate( );
				}
			}

			#region Old code (BF 7.18.02: removed for UWG1387)
			
			#endregion Old code (BF 7.18.02)

			//Fire after event
			//
			this.FireAfterDropDown( );				

			return true;
		}

		/// <summary>
		/// Returns whether the selected item's text should be displayed in the edit portion
		/// </summary>
		bool IValueList.ShouldDisplayText
		{
			
			get{ return true; }
		}

		/// <summary>
		/// Returns whether the selected item's image should be displayed in the edit portion
		/// </summary>
		bool IValueList.ShouldDisplayImage
		{
			get
			{ 
				//MRS 4/18/06 - BR11468
				if (this.EditAreaDisplayStyle == EditAreaDisplayStyle.DisplayText)
					return false;

				return true; 
			}
		}

		// MRS 12/16/04 - BR00457
		// Implement IFilterableValueList
		#region IFilterableValueList implemention
		bool IFilterableValueList.IsItemVisible( int index )
		{
			if ( index >= this.Rows.Count )
				throw new IndexOutOfRangeException( Shared.SR.GetString("LE_Exception_273") );

			return ! this.Rows[index].HiddenResolved;
        }

        //  BF 2/29/08  FR09238 (EditorWithCombo AutoCompleteMode)
            #region FR09238 (EditorWithCombo AutoCompleteMode)
        /// <summary>
        /// Filters list items out based on whether they meet the filter criteria defined by the specified value.
        /// </summary>
        /// <param name="value">The value which defines the filter criteria.</param>
        /// <returns>The number of items that exist in the filtered list.</returns>
        int IFilterableValueList.ApplyFilter( object value )
        {
            return this.FilterAutoCompleteItems( value as string, true );
        }

        /// <summary>
        /// Removes any filters that might have been applied when the ApplyFilter method was called.
        /// </summary>
        void IFilterableValueList.RemoveFilter()
        {
            this.FilterAutoCompleteItems( null, false );
        }
            #endregion FR09238 (EditorWithCombo AutoCompleteMode)

		#endregion IFilterableValueList implemention

		#endregion

		#region ISelectionManager Methods

		bool ISelectionManager.ActivateItem( ISelectableItem item )
		{

		    //RobA UWG387 10/1/01
		    if ( item is Infragistics.Win.UltraWinGrid.ColumnHeader )
		    {
			    this.clickedOnColumnHeader = true;
			    // MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
			    // Support HeaderClickAction in Combo and DropDown
			    return false;
		    }

		    // SSP 8/1/03 UWG1662
		    // Exposed ReadOnly property off the UltraCombo.
		    // Added IsReadOnly read-only (only get) virtual method to the UltraDropDownBase so
		    // the any deriving class with a ReadOnly or a similar property can override this
		    // and specify that the drop down should be read-only.
		    // If the control's read-only, then return true indicating that it did not succeed.
		    //
		    // --------------------------------------------------------------------------------
		    if ( this.IsReadOnly )
			    return true;
		    // --------------------------------------------------------------------------------

		    Infragistics.Win.UltraWinGrid.UltraGridRow row = item as Infragistics.Win.UltraWinGrid.UltraGridRow;

            //  BF 2/24/09  TFS12735
            //  Wrapped this in a try/finally so we can maintain a flag
            //  that indicates that a row is being activated by mouse.
            try
            {
                this.isActivatingRowByMouse = true;

			    // MRS 1/13/06 - BR08900
			    // This can happen when the user clicks on a band header.
			    //Debug.Assert( row != null, "Invalid item passed into UltraDropDownBase's ISelectionManager.ActivateItem." );			

			    // MRS 1/13/06 - BR08900	
			    // Added a check for row == null
			    //
    //			// MRS 12/6/05 - BR07941
    //			if (row.IsDisabled)
			    if (row == null ||
				    row.IsDisabled)
			    {
				    // Set a flag so we know not to close up. 
				    this.clickedOnDisabledRow = true;
				    return true;
			    }

			    // JJD 9/26/01
			    // Set the active row
			    //
			    this.ActiveRow = row;

			    // MRS 5/18/05 - BR03816
			    // If the active row is not what we set it to, we can assume
			    // it was somehow cancelled. This will occur if the user set
			    // the ActiveRow inside the RowSelected event, for example. 
			    if (this.ActiveRow != row)
				    return true;
      
			    return false;
            }
            finally
            {
                this.isActivatingRowByMouse = false;
            }
		}

		void ISelectionManager.ClearInitialSelection()
		{
			
		}

		void ISelectionManager.DoDragScrollHorizontal( int timerInterval )
		{
			Point mouseLoc = new Point(0,0);

			if ( this.ContinueScrolling( ref mouseLoc ) )
			{
				// if the region needs scrolling, have it actually do the scrolling
				//if ( this.ActiveColScrollRegion.DoesDragNeedScroll( mouseLoc, ref timerInterval ))
				this.ActiveColScrollRegion.DoDragScroll( mouseLoc, timerInterval );
			}
		}

		bool ISelectionManager.DoesDragNeedScrollHorizontal( Point mouseLoc, ref int timerInterval )
		{
			return ( !this.clickedOnColumnHeader &&
				this.ActiveColScrollRegion.DoesDragNeedScroll( mouseLoc, ref timerInterval ) );
		}

		bool ISelectionManager.DoesDragNeedScrollVertical( Point mouseLoc, ref int timerInterval )
		{
			return ( !this.clickedOnColumnHeader &&
				this.ActiveRowScrollRegion.DoesDragNeedScroll( mouseLoc, ref timerInterval ) );
		}

		void ISelectionManager.DoDragScrollVertical( int timerInterval )
		{
			Point mouseLoc = new Point(0,0);

			if ( this.ContinueScrolling( ref mouseLoc ) )
			{
				// if the region needs scrolling, have it actually do the scrolling
				//if( this.ActiveRowScrollRegion.DoesDragNeedScroll( mouseLoc, ref timerInterval ))
				this.ActiveRowScrollRegion.DoDragScroll ( mouseLoc, timerInterval );
			}
		}

		void ISelectionManager.EnterSnakingMode( ISelectableItem item )
		{
			//Not supported by the dropdown
		}

		Control ISelectionManager.GetControl( )
		{
			return this.ControlForGridDisplay;
		}
		ISelectableItem ISelectionManager.GetPivotItem( ISelectableItem item )
		{
			return null;
		}

		ISelectionStrategy ISelectionManager.GetSelectionStrategy( ISelectableItem item )
		{
			// SSP 2/3/05
			// Allow only the row selection in ultra drop down. For headers return selection
			// strategy of None.
			//
			if ( ! ( item is UltraGridRow ) )
				return new SelectionStrategyNone( this );

			return new SelectionStrategySingle( this );			
		}


		bool ISelectionManager.IsItemSelectableWithCurrentSelection( Infragistics.Shared.ISelectableItem item )
		{
			return true;
		}
		bool ISelectionManager.IsMaxSelectedItemsExceeded( ISelectableItem item )
		{
			return false;
		}

		ISelectableItem ISelectionManager.ItemNearestPoint ( Point point, ISelectableItem lastItem )
		{
			if(lastItem == null)
				return null;

			Type itemType = lastItem.GetType();

			Infragistics.Win.UltraWinGrid.VisibleRow nearestRow = 
				new Infragistics.Win.UltraWinGrid.VisibleRow( this.ActiveRowScrollRegion );
			
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//Infragistics.Win.UltraWinGrid.VisibleHeader nearestHeader = 
			//    new Infragistics.Win.UltraWinGrid.VisibleHeader( this.ActiveColScrollRegion );
			
			
			//if ( itemType == typeof(Row) || itemType == typeof(Cell) )
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Added UltraGridFilterCell class.
			//
			//if ( itemType == typeof(UltraGridRow) || itemType.IsSubclassOf( typeof( UltraGridRow ) ) || itemType == typeof(UltraGridCell) )
			if ( GridUtils.IsObjectOfType( itemType, typeof(UltraGridRow) ) || GridUtils.IsObjectOfType( itemType, typeof(UltraGridCell) ) )
			{
				if ( this.ActiveRowScrollRegion == null )
					return lastItem;

				nearestRow = this.ActiveRowScrollRegion.GetNearestSameBandRow( point, (GridItemBase)lastItem );	//, fDraggingDown );

				if ( nearestRow == null ) 
					Debug.WriteLine ( "nearestRow is null" );
			}			

			//if ( itemType == typeof(Row) )
			// MD 8/3/07 - 7.3 Performance
			// IsAssignableFrom is faster than IsSubclassOf
			//if ( itemType == typeof(UltraGridRow) || itemType.IsSubclassOf( typeof( UltraGridRow ) ) )
			if ( itemType == typeof( UltraGridRow ) || typeof( UltraGridRow ).IsAssignableFrom( itemType ) )
			{
				return nearestRow != null ?	nearestRow.Row : lastItem;
			}

			
			return lastItem;
		}

		void ISelectionManager.OnDragEnd( bool cancelled )
		{
			
		}


		void ISelectionManager.OnDragMove( ref MouseMessageInfo msginfo )
		{	
			
		}

		// JM 01-15-02
		//bool ISelectionManager.OnDragStart(ISelectableItem item)
		bool ISelectionManager.OnDragStart(ISelectableItem item, Point mousePosition)
		{
			//RobA UWG387 10/1/01
			if ( item is Infragistics.Win.UltraWinGrid.ColumnHeader )
				return true;

			return false;
		}

		
		void ISelectionManager.OnMouseUp( ref MouseMessageInfo msginfo )
		{	
		
			// MRS 12/6/05 - BR07941
			//if ( !this.clickedOnColumnHeader )
			if ( !this.clickedOnColumnHeader && !this.clickedOnDisabledRow )
			{
				// MRS 11/16/04 - UWG2996
				// This is unneccessary and causes a problem when setting 
				// the Text property inside the ValueChanged event. 
				// If it is later determined that changing the value here
				// is neccessary, then we should call 
				// SelectedItemChangeHelper instead of 
				// OnSelectedItemChanged
				//				if ( this.owner != null )
				//					this.owner.OnSelectedItemChanged();

				//close dropdown
				//

                //  BF 12/2/08  NA 9.1 - UltraCombo MultiSelect
                //  We might not necessarily want to close the dropdown now,
                //  so delegate that decision to the derived control.
				//((IValueList)(this)).CloseUp();
                bool closeDropDown = true;
                this.ProcessMouseUp( msginfo, out closeDropDown );
                if ( closeDropDown )
				    ((IValueList)(this)).CloseUp();
			}

			//reset flag
			//
			this.clickedOnColumnHeader = false;
						
			// MRS 12/6/05 - BR07941
			this.clickedOnDisabledRow = false;

		}

		bool ISelectionManager.SelectItem( ISelectableItem item, bool clearExistingSelection )
		{
			// JJD 9/25/01
			// If the selection hasn't changed return
			//
			if ( this.SelectedRow == item as UltraGridRow )
				return false;

			// set the selected row to this row 
			//
			this.SelectedRow = item as UltraGridRow;			
			
			return false;
		}
		bool ISelectionManager.SelectRange( ISelectableItem item, bool clearExistingSelection )
		{
			Debug.Fail("Should never call ISelectionManager.SelectRange on a dropdown");

			return false;
		}
		void ISelectionManager.SetInitialSelection( ISelectableItem item )
		{
		
		}

		void ISelectionManager.SetPivotItem( ISelectableItem item, bool IsRangeSelect )
		{
		}

		void ISelectionManager.TranslateItem( ref ISelectableItem item )
		{
			item = ((UltraGridCell)(item)).Row;			
		}


		bool ISelectionManager.UnselectItem( ISelectableItem item, bool clearExistingSelection )
		{
			return false;
		}
		bool ISelectionManager.UnselectRange( ISelectableItem item, bool clearExistingSelection )
		{
			return false;
		}


		#endregion
		
		
		/// <summary>
		/// Constructor.
		/// </summary>
		protected UltraDropDownBase()
		{
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal bool IsMouseDown
		//{
		//    get
		//    {
		//        return this.mouseDown;
		//    }	

		//    set
		//    {
		//        if ( this.mouseDown != value )
		//            this.mouseDown = value;
		//    }
		//}

		#endregion Not Used

		internal bool IncludeVerticalScrollbar{ get { return this.includeVerticalScrollbar; } }
		internal bool IncludeHorizontalScrollbar{ get { return this.includeHorizontalScrollbar; } }

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Added ReCalculateDropDownSize method
		#region ReCalculateDropDownSize

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// This method will refresh the sizing of the DropDown in order to
		//        /// show scrollbars when a column is resized. 
		//        /// </summary>
		//        /// <remarks>
		//        /// If the control is not dropped down, it will do nothing. 
		//        /// </remarks>
		//#endif 
		//        internal void ReCalculateDropDownSize()
		//        {
		//            this.ReCalculateDropDownSize( false );
		//        }

		#endregion Not Used
		
		// MRS 8/29/06 - BR15111
		// Added this overload fr backward compatibility.
		internal void ReCalculateDropDownSize(bool resizeDropDownForm)
		{
			this.ReCalculateDropDownSize( resizeDropDownForm, true );
		}		

		// SSP 10/21/05 BR07016
		// Added an overload of ReCalculateDropDownSize that takes in resizeDropDownForm parameter.
		// 
		// MRS 8/29/06 - BR15111
		//internal void ReCalculateDropDownSize( bool resizeDropDownForm )
		internal void ReCalculateDropDownSize( bool resizeDropDownForm, bool dirtyMetrics )
		{
			if (!this.IsDroppedDown)
				return;
			
			bool isRecalcRequired = false;
			// MRS 8/29/06 - BR15111
			//System.Drawing.Size dropDownSize = this.CalculateDropDownSize(this.lastEditAreaInScreenCoords, true, ref isRecalcRequired);
			System.Drawing.Size dropDownSize = this.CalculateDropDownSize(this.lastEditAreaInScreenCoords, true, ref isRecalcRequired, dirtyMetrics);

			// SSP 10/21/05 BR07016
			// Added an overload of ReCalculateDropDownSize that takes in resizeDropDownForm parameter.
			// 
			if ( resizeDropDownForm )
			{
				Control ownerControl = null != this.owner ? this.owner.Control : null;
				if ( null != ownerControl )
				{
					// AS 12/19/05 - BR08024
					// We are not using the same dropdown rectangle that we passed into
					// the original dropdown method. We should use that value instead 
					// of the actual place it was dropped down. And we need to be aware
					// of whether it was originally aligned right or left since that
					// affects the rect that we pass in for the new rect.
					//
					//Rectangle oldRect = DropDownManager.GetDropDownScreenRect( ownerControl );
					//Rectangle newRect = oldRect;
					//newRect.Width = dropDownSize.Width;
					//newRect.Height = dropDownSize.Height;
					Point alignPoint = this.isDropDownAlignedRight ?
						alignPoint = new Point( this.lastEditAreaInScreenCoords.Right, this.lastEditAreaInScreenCoords.Bottom ) :
						alignPoint = new Point( this.lastEditAreaInScreenCoords.Left, this.lastEditAreaInScreenCoords.Bottom );
					
					Rectangle oldRect = this.lastRequestedRect;
					Rectangle newRect = new Rectangle(alignPoint, dropDownSize);
					this.lastRequestedRect = newRect;

					if ( newRect != oldRect && ! this.lastEditAreaInScreenCoords.IsEmpty )
					{
						DropDownManager.ResetDropDownPositionAndSize(
							ownerControl,
							newRect,
							this.lastEditAreaInScreenCoords );							
					}
				}
			}
		}
		#endregion ReCalculateDropDownSize

		// MRS 8/29/06 - BR15111
		// Added this overload for backward compatibility
		private Size CalculateDropDownSize( Rectangle editAreaInScreenCoords, bool determineScrollBarVisibility, ref bool isRecalcRequired )
		{
			return this.CalculateDropDownSize( editAreaInScreenCoords, determineScrollBarVisibility, ref isRecalcRequired, true );
		}

		// AS 5/21/04 UWG3134
		//private Size CalculateDropDownSize( Rectangle editAreaInScreenCoords )
		// MRS 8/29/06 - BR15111
		//private Size CalculateDropDownSize( Rectangle editAreaInScreenCoords, bool determineScrollBarVisibility, ref bool isRecalcRequired )
		private Size CalculateDropDownSize( Rectangle editAreaInScreenCoords, bool determineScrollBarVisibility, ref bool isRecalcRequired, bool dirtyMetrics )
		{
			// MRS 8/29/06 - BR15111
			// Added a dirtyMetrics param to this methods. Added a check before dirtying the metrics. 
			if (dirtyMetrics)
			{
				// SSP 8/16/02 UWG1478
				// InitializeMetrics call before will only initialize the metrics if
				// it has been dirtied. So dirty the metrics to ensure that InitializeMetrics
				// call below actually initializes the metrics.
				//
				this.DisplayLayout.ColScrollRegions.DirtyMetrics( );
			}

			// AS 5/21/04 UWG3134 
			// Moved down since this was forcing the extent
			// member of the band to be calculated before
			// we initialized the scrollbar visibility members
			// below.
			//
			//			//RobA UWG568 10/18/01
			//			// make sure the column metrics have been properly calculated
			//            //
			//            this.DisplayLayout.ColScrollRegions.InitializeMetrics();

			// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
			// Support HeaderClickAction in Combo and DropDown
			// ----------------------------------------------------------------
			//			// JJD 1/23/02 - UWG497
			//			// Use the passed in rect to get the correct monitor work area
			//			//
			//			Rectangle monitorWorkArea = Infragistics.Win.UIElement.GetDeskTopWorkArea( editAreaInScreenCoords );
			//			//			Rectangle monitorWorkArea = Infragistics.Win.UIElement.GetDeskTopWorkArea( this.DisplayLayout.Grid.Bounds );
			
			// SSP 10/21/05 BR07016
			// Reverting back to the old code. Commented out the original code. Now we 
			// implemented support for being able to resize the drop down form while it's
			// dropped down.
			// 
			Rectangle monitorWorkArea = Infragistics.Win.UIElement.GetDeskTopWorkArea( editAreaInScreenCoords );
			
			// ----------------------------------------------------------------

			// JJD 1/23/02 - UWG497
			// Calculate the available height which is the larger of the space above and below the
			// edit area.
			//
			int heightAvailableForDropDown = Math.Max( monitorWorkArea.Bottom - editAreaInScreenCoords.Bottom,
				editAreaInScreenCoords.Top - monitorWorkArea.Top );

			Size dropDownSize = new Size(0,0);

			// AS 5/21/04 UWG3134
			if (determineScrollBarVisibility)
			{
				this.includeVerticalScrollbar = false;
				this.includeHorizontalScrollbar = false;

				// JJD 1/23/02 - UWG497
				// Init the include flags based on the scrollbars property setting. For
				// Automatic we assume false.
				//
				switch ( this.DisplayLayout.Scrollbars )
				{
					case Scrollbars.Vertical:
					case Scrollbars.Both:
						this.includeVerticalScrollbar = true;
						break;
				}

				switch ( this.DisplayLayout.Scrollbars )
				{
					case Scrollbars.Horizontal:
					case Scrollbars.Both:
						this.includeHorizontalScrollbar = true;
						break;
				}
			}

			// JJD 10/02/01
			// Rewrite items to display calculation to correctly use
			// the Min/MaxDropDownItems property settings
			//
			int itemsToDisplay = ((IValueList)this).ItemCount;

            // MBS 9/4/08 - TFS6905
            // We need to also take into consideration the summaries that
            // are being shown
            int numNonFixedSummaries;
            UltraGridRow previousRow = null;
            List<UltraGridRow> specialRows = this.CalculateSpecialRowsInDropDown(ref previousRow, out numNonFixedSummaries);
            itemsToDisplay += numNonFixedSummaries;

			itemsToDisplay = Math.Min( itemsToDisplay, this.MaxDropDownItems );
			itemsToDisplay = Math.Max( itemsToDisplay, this.MinDropDownItems );

			// JJD 1/23/02 - UWG497
			// Only set the vert scrollbar flag if the scrollbars property setting
			// is automatic.
			//
			if ( ((IValueList)this).ItemCount > itemsToDisplay &&
				// AS 5/21/04 UWG3134
				//this.DisplayLayout.Scrollbars == Scrollbars.Automatic )
				this.DisplayLayout.Scrollbars == Scrollbars.Automatic &&
				determineScrollBarVisibility)
				this.includeVerticalScrollbar = true;

			// AS 5/21/04 UWG3134
			// Moved from above since the includeXXXScrollBar
			// will impact the metrics and therefore the overall extent.
			//
			//RobA UWG568 10/18/01
			// make sure the column metrics have been properly calculated
			//
			this.DisplayLayout.ColScrollRegions.InitializeMetrics();

			// JJD 1/28/02 - UWG1004
			// Always figue out the extent required to display all the columns
			//
			int extentNeededForColumns = this.DisplayLayout.SortedBands[0].Header.OverallExtent;
				
			if ( this.includeVerticalScrollbar )
				extentNeededForColumns += SystemInformation.VerticalScrollBarWidth;

			// RobA UWG568 Add the borderthickness
			//
			// SSP 9/19/02 UWG1682
			// Drop down's border is not the same as combo's border. This.ControlUIElement gives
			// the combo ui element in case of this being an UltraCombo which uses UltraCombo.BorderStyle.
			// The grid ui element within the drop down uses the layout's border style. So use that
			// to calcualte the width reqtured for the border.
			//
			//extentNeededForColumns += ( 2 * this.DisplayLayout.GetBorderThickness( this.ControlUIElement.BorderStyle ) );
			extentNeededForColumns += ( 2 * this.DisplayLayout.GetBorderThickness( this.DisplayLayout.BorderStyleResolved ) );

			// set the drop down width
			//
			if ( this.DropDownWidth > 0 )
			{
				//use the DropDownWidth as the width
				//
				dropDownSize.Width = this.DropDownWidth;
			}
			else if ( this.DropDownWidth == 0 )
			{
				//use ths control's width
				//
                // MRS 4/22/2009 - TFS16750
                // The width of the control would work fine for UltraCombo, but not for UltraDropDown.
                // We should be using the width of the edit area. 
                //
				//dropDownSize.Width = this.Width;
                dropDownSize.Width = editAreaInScreenCoords.Width;
			}					
			else 
			{
				dropDownSize.Width = extentNeededForColumns;

				// SSP 7/23/02 UWG1062 UWG1345
				// If there are no columns, then the extent will come out to be 0.
				// So use the width of the combo instead.
				// NOTE: 4 to counteract the the 2 * border widths added above
				//
				if ( extentNeededForColumns <= 4 && this is UltraCombo )
					dropDownSize.Width = this.Width;
			}

			// If the width set above is greater than the montior work
			// area then reduce it.
			//
			if ( dropDownSize.Width > monitorWorkArea.Width )
				dropDownSize.Width = monitorWorkArea.Width;

			// JJD 1/28/02 - UWG1004
			// If the setting was automatic see if we need a horizontal 
			// scrollbar
			//
			// AS 5/21/04 UWG3134
			//if ( this.DisplayLayout.Scrollbars == Scrollbars.Automatic )
			if ( determineScrollBarVisibility && this.DisplayLayout.Scrollbars == Scrollbars.Automatic )
			{
				if ( dropDownSize.Width < extentNeededForColumns )
				{
					this.includeHorizontalScrollbar = true;

					// AS 5/21/04 UWG3134
					isRecalcRequired = true;
				}
			}
			

			// SSP 12/2/02 UWG1838
			// Changed the way we are calculating the required height of the drop down. Now we are
			// taking into account the variable row heights in our calculations where as before
			// we were assuming fixed row heights.
			//
			// -----------------------------------------------------------------------------------------------
			

			int totalHeight = 0;
			bool doesFirstRowShareBorderWithHeaders = true;
			UIElementBorderStyle rowBorderStyle = this.DisplayLayout.SortedBands[0].BorderStyleRowResolved;
			int rowBorderThickness = this.DisplayLayout.GetBorderThickness( rowBorderStyle );
			int itemsCount = 0;

            // MBS 9/4/08 - TFS6905
            // Moved above, since we may have a summary on the top and we will need
            // to account for that border
            //UltraGridRow previousRow = null;

			// SSP 6/02/03 UWG2279
			// Take into account any hidden rows.
			//
			
			int startIndex = ((IValueList)this).SelectedItemIndex;
			if ( startIndex < 0 )
				startIndex = 0;
			else if ( startIndex >= this.Rows.Count )
				startIndex = Math.Max( 0, this.Rows.Count - 1 );

			// SSP 4/2/04 UWG3078
			// Take into account selected row being hidden. If the selected row is hidden then
			// the VisibleIndex of it will be -1 and following calculations won't produce the
			// expected results in that case.
			//
			// ------------------------------------------------------------------------------------------
			
			if ( startIndex >= 0 && startIndex < this.Rows.Count )
			{
				// If the selected row is hidden, that is it's VisibleIndex is less than 0,
				// then get the next visible row and use it's visible index to perform the
				// below calculations.
				//
				UltraGridRow tmpRow = this.Rows[ startIndex ];
				int selectedItemVisibleIndex = tmpRow.VisibleIndex;
				if ( selectedItemVisibleIndex < 0 )
				{
					if ( null != tmpRow.ParentCollection )
					{
						// Get the next visible row.
						//
						UltraGridRow nextVisibleRow = tmpRow.ParentCollection.GetRowAtVisibleIndexOffset( tmpRow, 1 );
						if ( null != nextVisibleRow )
							tmpRow = nextVisibleRow;
						else
							// Get the previous visible row if there is no next visible row.
							//
							tmpRow = tmpRow.ParentCollection.GetRowAtVisibleIndexOffset( tmpRow, -1 );
					}

					if ( null != tmpRow )
						selectedItemVisibleIndex = tmpRow.VisibleIndex;
				}

				if ( this.Rows.VisibleRowCount - selectedItemVisibleIndex < itemsToDisplay )
				{
					tmpRow = this.Rows.GetRowAtVisibleIndex( Math.Max( 0, this.Rows.VisibleRowCount - itemsToDisplay ) );

                    // MBS 4/20/09 - TFS16909
                    // It's possible for there not to be any rows, so we shouldn't ever try to index with a negative number
                    //
					//startIndex = null != tmpRow ? tmpRow.Index : this.Rows.Count - itemsToDisplay;
                    startIndex = null != tmpRow ? tmpRow.Index : Math.Max(0, this.Rows.Count - itemsToDisplay);
				}
			}
			// ------------------------------------------------------------------------------------------

			//  BF 4/20/09  TFS15304
            //
            //  a) I got a bad index exception when I tried the 'TopFixed' setting,
            //  because the logic above was causing startIndex to be -1, so I made sure
            //  startIndex was never less than zero.
            //
            //  b) The logic above is also causing startIndex to be 1 (as opposed to 0)
            //  when any item besides the first one is selected. That is what causes TFS15304;
            //  in the case where the number of items that can be displayed is exactly
            //  equal to the total number of rows, the startIndex is different when it
            //  shouldn't be. I am hesitant to mess with the logic above, so suffice to
            //  say we should always start at zero here if all rows are being displayed.
            //  
			//for ( int i = startIndex; i < this.Rows.Count && itemsCount < itemsToDisplay; i++ )

            startIndex = Math.Max(startIndex, 0);
            
            int rowCount = this.Rows.Count;
            if ( itemsToDisplay <= rowCount )
                startIndex = 0;

			for ( int i = startIndex; i < rowCount && itemsCount < itemsToDisplay; i++ )
			{
				UltraGridRow row = this.Rows[i];

				// Skip any hidden rows.
				//
				if ( row.HiddenResolved )
					continue;

				totalHeight += row.TotalHeight;
		
				if ( null != previousRow )
				{
					// Take into account merged borders.
					//
					if ( previousRow.RowSpacingAfterResolved <= 0 && row.RowSpacingBeforeResolved <= 0 &&
						this.DisplayLayout.CanMergeAdjacentBorders( rowBorderStyle ) )
					{
						totalHeight -= rowBorderThickness;
					}
				}
				else
				{
					// If this is the first row, see if it shares borders with the header.
					//
					if ( row.RowSpacingBeforeResolved > 0 )
						doesFirstRowShareBorderWithHeaders = false;
				}
				
				previousRow = row;
				itemsCount++;
			}
		
			// If somehow totalHeight is still 0, then calculate it based on the RowHeightResolved
			// property off the band.
			//
			if ( totalHeight <= 0 )
			{
				totalHeight = itemsToDisplay * this.DisplayLayout.SortedBands[0].RowHeightResolved;

				if ( this.DisplayLayout.CanMergeAdjacentBorders( rowBorderStyle ) &&
					this.DisplayLayout.SortedBands[0].RowSpacingBeforeDefault < 1 &&
					this.DisplayLayout.SortedBands[0].RowSpacingAfterDefault < 1 )
				{
					totalHeight += rowBorderThickness * ( 1 + itemsToDisplay );
				}
				else
				{
					totalHeight += 2 * rowBorderThickness * itemsToDisplay;
				}
			}
            
			// Add the header height as well.
			//
			// SSP 5/31/02 UWG1162, UWG834
			// Pass in false to GetTotalHeaderHeight so that it does not include the border 
			// shared with the row.
			//
			//dropDownSize.Height = this.DisplayLayout.Bands[0].GetTotalHeaderHeight();
			// SSP 7/23/02 UWG1345
			// Since we are allowing the combo to drop down even when there is no
			// data source set, we should only get the hear height if there indeed
			// any columns.
			// 
			if ( null != this.DisplayLayout.SortedBands[0].Columns &&	this.DisplayLayout.SortedBands[0].Columns.Count > 0 )
				totalHeight += this.DisplayLayout.SortedBands[0].GetTotalHeaderHeight( !doesFirstRowShareBorderWithHeaders );

            // MBS 9/4/08 - TFS6905
            // We need to know if either the top rows or the bottom summaries caused us to have more items
            // than we can show.  This call will also update the total height with the height of the
            // special rows, assuming that 'itemsToDisplay' isn't exceeded.
            bool hasSpecialRowsOutOfView = this.CalculateSpecialRowsSize(specialRows, itemsToDisplay, ref totalHeight, ref itemsCount);

			dropDownSize.Height = totalHeight;
			// -----------------------------------------------------------------------------------------------

			
			// RobA UWG545 11/16/01 need to add the dropdown's border thickness
			//
			// SSP 9/19/02 UWG1682
			// Drop down's border is not the same as combo's border. This.ControlUIElement gives
			// the combo ui element in case of this being an UltraCombo which uses UltraCombo.BorderStyle.
			// The grid ui element within the drop down uses the layout's border style. So use that
			// to calcualte the width reqtured for the border.
			//
			//dropDownSize.Height += 	( 2 * this.DisplayLayout.GetBorderThickness( this.ControlUIElement.BorderStyle ) ); 
			dropDownSize.Height += 	( 2 * this.DisplayLayout.GetBorderThickness( this.DisplayLayout.BorderStyleResolved ) ); 

			// RobA UWG834 12/6/01 
			// Off by 1 pixel
			//
			// SSP 5/31/02 UWG1162
			// This causes problems when the header is hidden. The reason why it was off
			// by one pixel in UWG834 was because we are not taking into account merged
			// borders between the first row and the header. I made the necessary change
			// above to take into account this merged border.
			// Commented out the line that decremenets the drop down height by 1.			
			// SSP 9/19/02 UWG1162 UWG1682
			// Do what the comment above says. Comment below line out as it causes
			// problems like the above comment says.
			//
			//--dropDownSize.Height;

			//add the height of the scollbar
			//
			if ( this.includeHorizontalScrollbar )
				dropDownSize.Height += SystemInformation.HorizontalScrollBarHeight;

			// JJD 1/23/02 - UWG497
			// If the height is > than the available height reduce it which may
			// force us to add a vertical and even a horizontal scrollbar.
			//
            if (dropDownSize.Height > heightAvailableForDropDown)
            {
                dropDownSize.Height = heightAvailableForDropDown;

                // MBS 9/4/08 - TFS6905
                // Refactored into a helper method so that we can perform the same logic
                // if we have summaries that need to be shown
                #region Refactored

                //// JJD 1/23/02 - UWG497
                //// If we don't already have a vertical scrollbar add one now.
                ////
                //if ( this.DisplayLayout.Scrollbars == Scrollbars.Automatic &&
                //    // AS 5/21/04 UWG3134
                //    //!this.includeVerticalScrollbar )
                //    !this.includeVerticalScrollbar &&
                //    determineScrollBarVisibility)
                //{
                //    dropDownSize.Width += SystemInformation.VerticalScrollBarWidth;

                //    this.includeVerticalScrollbar = true;

                //    // AS 5/21/04 UWG3134
                //    isRecalcRequired = true;

                //    // JJD 1/23/02 - UWG497
                //    // Check to make sure this didn't put us past the max width
                //    // which would force a horizonatl scrollbar
                //    //
                //    if ( dropDownSize.Width > monitorWorkArea.Width )
                //    {
                //        dropDownSize.Width = monitorWorkArea.Width;
                //        this.includeHorizontalScrollbar = true;
                //    }

                //}
                #endregion //Refactored
                //
                this.CalculateDropDownSizeAutoScrollbarHelper(determineScrollBarVisibility, monitorWorkArea, ref dropDownSize, ref isRecalcRequired);
            }
            // MBS 9/4/08 - TFS6905
            // If we have summaries to show that are beyond the number of items that
            // we're showing at once, we need to make sure that the automatic
            // scrollbar is updated accordingly
            else if (hasSpecialRowsOutOfView)
                this.CalculateDropDownSizeAutoScrollbarHelper(determineScrollBarVisibility, monitorWorkArea, ref dropDownSize, ref isRecalcRequired);
			
			return dropDownSize;
        }

        #region MBS 9/4/08 - TFS6905

        // Refactored from CalculateDropDownSize
        private void CalculateDropDownSizeAutoScrollbarHelper(bool determineScrollBarVisibility, Rectangle monitorWorkArea, ref Size dropDownSize, ref bool isRecalcRequired)
        {
            // JJD 1/23/02 - UWG497
            // If we don't already have a vertical scrollbar add one now.
            //
            if (this.DisplayLayout.Scrollbars == Scrollbars.Automatic &&
                // AS 5/21/04 UWG3134
                //!this.includeVerticalScrollbar )
                !this.includeVerticalScrollbar &&
                determineScrollBarVisibility)
            {
                dropDownSize.Width += SystemInformation.VerticalScrollBarWidth;

                this.includeVerticalScrollbar = true;

                // AS 5/21/04 UWG3134
                isRecalcRequired = true;

                // JJD 1/23/02 - UWG497
                // Check to make sure this didn't put us past the max width
                // which would force a horizonatl scrollbar
                //
                if (dropDownSize.Width > monitorWorkArea.Width)
                {
                    dropDownSize.Width = monitorWorkArea.Width;
                    this.includeHorizontalScrollbar = true;
                }
            }
        }               

        private bool CalculateSpecialRowsSize(List<UltraGridRow> specialRows, int itemsToDisplay, ref int totalHeight, ref int itemsCount)
        {
            if (specialRows == null)
                return false;

            bool hasAdditionalRows = false;

            for (int i = 0; i < specialRows.Count; i++)
            {
                UltraGridRow row = specialRows[i];

                if (itemsCount < itemsToDisplay)
                {
                    totalHeight += row.TotalHeight;
                    itemsCount++;
                }
                // If the row is fixed, we should not count it as being
                // constrained by the number of items to show in the dropdown
                else if (row.IsFixedBottom || row.IsFixedTop)
                    totalHeight += row.TotalHeight;
                else
                    // We need to let the caller know that we will have
                    // additional rows to show so that the visibility
                    // of an automatic scrollbar can be recalculated
                    hasAdditionalRows = true;
            }

            return hasAdditionalRows;
        }

        private List<UltraGridRow> CalculateSpecialRowsInDropDown(ref UltraGridRow previousRow, out int numNonFixedRows)
        {
            numNonFixedRows = 0;
            List<UltraGridRow> rows = new List<UltraGridRow>();

            CalculateSpecialRowsInDropDownHelper(this.Rows.GetBottomSpecialRows(), false, ref numNonFixedRows, ref previousRow, ref rows);
            CalculateSpecialRowsInDropDownHelper(this.Rows.GetTopSpecialRows(), true, ref numNonFixedRows, ref previousRow, ref rows);

            return rows;
        }

        private static void CalculateSpecialRowsInDropDownHelper(UltraGridRow[] specialRows, bool setPreviousRow, ref int numNonFixedRows, ref UltraGridRow previousRow, ref List<UltraGridRow> rows)
        {
            if (specialRows == null)
                return;

            foreach (UltraGridRow row in specialRows)
            {
                // Skip any hidden rows.
                //
                if (row.HiddenResolved)
                    continue;

                if (!row.IsFixedBottom && !row.IsFixedTop)
                    numNonFixedRows++;

                // We want to keep track of the last row on the top so that we can correctly calculate
                // the height with merged borders and such
                if (setPreviousRow)
                    previousRow = row;

                rows.Add(row);
            }
        }
        #endregion // MBS 9/4/08 - TFS6905

        /// <summary>
		/// Determines which column (by <see cref="UltraGridColumn.Key"/>) in the dropdown is used to determine the displayed text.
		/// </summary>
		/// <remarks>
		/// <p class="body">The DisplayMember determines which column in the dropdown is used for display purposes. If this property is not set, the <see cref="ValueMember"/> column will be used. To determine the column that is being used as the DisplayMember, you can use the <see cref="DisplayMemberResolved"/> property.</p>
		/// <p class="body">For example, if you have a column in an <see cref="UltraGrid"/> control where the user can drop down a list and select a customer, you would use an <see cref="UltraDropDown"/> control and bind it to a table of customers. The customer table likely has a primary key to uniquely identify customers.</p>
		/// <p class="body">This customer ID field would be used as the <see cref="ValueMember"/> of the <see cref="UltraDropDown"/>. But this ID value would not be very useful to the users of the application. So you would set the DisplayMember to the field on the list that contains the customer's name. The grid cell would store the ID (probably in an integer or other numeric-type field), but the display would show the user-friendly customer name.</p>
		/// <p class="body">The <see cref="UltraCombo"/> control works the same way, except that it has it's own edit portion, instead of using a grid cell. The <see cref="UltraCombo.Value"/> is determined by the <see cref="ValueMember"/> and the display, as well as the <see cref="UltraCombo.Text"/> property is determined by the DisplayMember.</p>
		/// <p class="body">DisplayMember should only be set when you want to translate data values into more user-friendly display text. If you want the value stored and the value displayed on-screen to be the same, there is no need to set DisplayMember. Setting <see cref="ValueMember"/> is sufficient.</p>
		/// </remarks>
		/// <seealso cref="ValueMember"/>
		/// <seealso cref="DisplayMemberResolved"/>
		[
		// AS 4/5/06 BR11341
		//DefaultValue(null),
		DefaultValue(""),
		LocalizedCategory("LC_Data"),		
		Editor( "System.Windows.Forms.Design.DataMemberFieldEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing" ),
		LocalizedDescription("LD_UltraDropDownBase_P_DisplayMember")
		]
		public string DisplayMember
		{
			get
			{
				return this.displayMember;					
			}

			set
			{
				//we are only interested in the last section of the passed in 
				//text
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//string newValue = this.StripMember(value);
				string newValue = UltraDropDownBase.StripMember( value );

				// MRS 7/25/05 - BR04641
//				if ( this.displayMember != newValue )
//					this.displayMember = newValue;				
//		
//				// JJD 11/06/01
//				// Notify listeners
//				//  
//				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DisplayMember );
				if ( this.displayMember != newValue )
				{
					this.displayMember = newValue;				
		
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DisplayMember );

					// MRS 6/1/06 - BR12914
					//this.DirtySortedDisplayMemberListVersion();
					this.BumpListVersion();
				}
			}
		}

		// AS 5/21/04 UWG3103/UWG3126
		// Changed to public since we're no longer setting
		// the property directly.
		//
		//internal string DisplayMemberResolved
		/// <summary>
		/// Returns the actual <see cref="DisplayMember"/> column key that indicates what is displayed in the edit area.
		/// </summary>
		/// <remarks>
		/// <p class="body">This proeprty will return the actual resolved key of the column that is being used for the display of the selected item. If the <see cref="DisplayMember"/> has been set, then that property setting will be returned. Otherwise, this will return the <see cref="ValueMemberResolved"/> property.</p>
		/// </remarks>
		/// <seealso cref="DisplayMember"/>
		/// <seealso cref="ValueMember"/>
		/// <seealso cref="ValueMemberResolved"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public string DisplayMemberResolved
		{
			get
			{
				
				if (this.displayMember != null && this.displayMember.Length > 0)
					return this.displayMember;

				return this.ValueMemberResolved;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDisplayMember() 
		{
			return ( this.displayMember != string.Empty);
		}
 
		/// <summary>
		/// Reset the <see cref="DisplayMember"/> to the default (string.Empty).
		/// </summary>		
		public void ResetDisplayMember() 
		{
			// MRS 7/25/05 - BR04641
			//this.displayMember = string.Empty;
			this.DisplayMember = string.Empty;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal bool HasTooltipTool
		//{
		//    get
		//    {
		//        return null != this.toolTip;
		//    }
		//}

		#endregion Not Used

		/// <summary>
		/// Returns the tooltip tool object, allocating it the first time
		/// </summary>
		internal protected override Infragistics.Win.ToolTip TooltipTool
		{
			get
			{
				if ( this.DesignMode )
					return null;

				if ( null == this.toolTip )
				{
					
					this.toolTip = new Infragistics.Win.ToolTip( this );

					// MRS 1/7/04 - BR00641					
					// Commented this out. This should be handled by the band's ScrollTipCol property
					//this.DisplayLayout.Bands[0].ScrollTipField = this.DisplayMemberResolved;

					//this.toolTip.Control.Visible = true;
					//this.toolTip.Control.Visible = false;			
				}
			
				return this.toolTip;
			}
		}


		/// <summary>
		/// Determines the width of the dropdown at runtime.
		/// </summary>
		/// <remarks>
		/// <p class="body">The default value is <b>-1</b>. This means that the dropdown will automatically size based on the widths of the columns it contains. It will limit itself to the size of the screen.</p>
		/// <p class="body">A setting of <b>0</b> will cause the dropdown to auto-size itself to the width of the edit portion. In the case of the <see cref="UltraCombo"/> control, this is the width of the control. In the case of the <see cref="UltraDropDown"/> control, this will be the size of the editor in the column.</p>
		/// <p class="body">A value greater than 0 will force the dropdown to be that number of pixels wide.</p>		
		/// </remarks>
		[ LocalizedDescription("LD_UltraDropDownBase_P_DropDownWidth")]
		[ LocalizedCategory("LC_Behavior") ]
		public int DropDownWidth
		{
			get
			{
				return this.dropDownWidth;
			}

			set
			{
				if ( this.dropDownWidth != value )
				{
					this.dropDownWidth = value;
		
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DropDownWidth );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDropDownWidth() 
		{
			return ( this.dropDownWidth != -1);
		}
 
		/// <summary>
		/// Reset the <see cref="DropDownWidth"/> property to the default (-1).
		/// </summary>
		public void ResetDropDownWidth() 
		{
            // MRS NAS v8.3 - Unit Testing
			//this.dropDownWidth = -1;
            this.DropDownWidth = -1;
		}

		/// <summary>
		/// Determines the maximum number of dropdown items that will display in the list at one time.
		/// </summary>
		/// <remarks>
		/// <p class="body">MaxDropDownItems determines the maximum number of items that will display on the list at one time. If there are more items on the list than can be shown, then the scrollbar will be shown. If there are less items on the list, then the dropdown will size to the exact number of items or the <see cref="MinDropDownItems"/> setting, whichever is higher. This provides a way for you to limit the height of the dropdown.</p>
		/// <p class="body">Attempting to set MaxDropDownItems to a value less than 0 or <see cref="MinDropDownItems"/> will raise an <see cref="System.ArgumentOutOfRangeException"/> exception.</p>
		/// </remarks>
		/// <seealso cref="MinDropDownItems"/>
		[ LocalizedDescription("LD_UltraDropDownBase_P_MaxDropDownItems")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MaxDropDownItems
		{
			get
			{
				return this.maxDropDownItems;
			}

			set
			{
				if ( this.maxDropDownItems != value )
				{

					// Throw an error if they try to set value to 0
					//
					if ( value == 0 )
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_281"), Shared.SR.GetString("LE_ArgumentOutOfRangeException_365"));

					//RobA UWG383 9/28/01
					if ( value >= this.minDropDownItems )
						this.maxDropDownItems = value;
					else
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_281"), Shared.SR.GetString("LE_ArgumentOutOfRangeException_283"));
		
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.MaxDropDownItems );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMaxDropDownItems() 
		{
			return ( this.maxDropDownItems != 8 );
		}
 
		/// <summary>
		/// Reset the <see cref="MaxDropDownItems"/> property to the default (8).
		/// </summary>
		public void ResetMaxDropDownItems() 
		{
            // MRS NAS v8.3 - Unit Testing
			//this.maxDropDownItems = 8;
            this.MaxDropDownItems = 8;
		}
		

		/// <summary>
		/// Determines the minimum number of dropdown items that will display in the list at one time.
		/// </summary>
		/// <remarks>
		/// <p class="body">MinDropDownItems determines the minimum height of the dropdown based on a number of items that would fit in the dropdown area. If there are fewer items in the list than this property setting, the dropdown will appear with extra space enough to fit the specified number of items.</p>
		/// <p class="body">Attempting to set MinDropDownItems to a value less than 0 or greater than <see cref="MaxDropDownItems"/> will raise an <see cref="System.ArgumentOutOfRangeException"/> exception.</p>
		/// </remarks>
		/// <seealso cref="MinDropDownItems"/>
		[ LocalizedDescription("LD_UltraDropDownBase_P_MinDropDownItems")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MinDropDownItems
		{
			get
			{
				return this.minDropDownItems;
			}

			set
			{
				if ( this.minDropDownItems != value )
				{
					// JJD 10/02/01
					// Throw an error if they try to set a negative value
					//
					if ( value < 0 )
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_284"),Shared.SR.GetString("LE_ArgumentOutOfRangeException_366"));

					//RobA UWG383 9/28/01
					if ( value <= this.maxDropDownItems )
						this.minDropDownItems = value;
					else
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_284"),Shared.SR.GetString("LE_ArgumentOutOfRangeException_286"));
		
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.MinDropDownItems );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMinDropDownItems() 
		{
			return ( this.minDropDownItems != 1 );
		}
 
		/// <summary>
		/// Reset the <see cref="MinDropDownItems"/> property to the default (1).
		/// </summary>
		public void ResetMinDropDownItems() 
		{
            // MRS NAS v8.3 - Unit Testing
			//this.minDropDownItems = 1;
            this.MinDropDownItems = 1;
		}

		/// <summary>
		/// Gets or sets the currently selected row
		/// </summary>
		/// <remarks>
		/// <p class="body">In an <see cref="UltraCombo"/> control, this property determines the currently selected row.</p>
		/// <p class="body">Since an <see cref="UltraDropDown"/> control services many grid cells at once, this property should generally not be used for the <see cref="UltraDropDown"/>. Use the <see cref="UltraGridCell.Value"/> property of the <see cref="UltraGridCell"/> instead.</p>
		/// <p class="body">The SelectedRow, <see cref="UltraCombo.Value"/>, and <seealso cref="UltraCombo.Text"/> properties are tightly linked together. Changing one will change the others (except in some cases where rows have duplicate values).</p>
		/// </remarks>
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden )]
		public UltraGridRow SelectedRow
		{
			get
			{
				return this.selectedRow;
			}

			set
			{
				// MRS 10/28/04 - UWG2996
				// Funnel this and IValueList.SelectedItemIndex through a common method
				this.SelectedItemChangeHelper(ComboSelectedItemChangeContext.SelectedRow, value);

				//				if ( this.selectedRow != value )
				//				{
				//					// SSP 4/16/02 UWG1095
				//					// SelectedItemIndex and SelectedRow properties call each other to syncronize.
				//					// So I added these flags to prevent any infinite loops and also enclosed the
				//					// following block of code in try-finally.
				//					//
				//					this.inSelectedRow = true;
				//
				//					
				//					try
				//					{
				//						int rowIndex = null != value 
				//							? this.Rows.IndexOf( value )
				//							: -1;
				//					 
				//						if ( null != value && rowIndex < 0 )
				//						{
				//							throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_287"),  Shared.SR.GetString("LE_ArgumentException_367") );
				//						}
				//
				//						if ( this.selectedRow != null )
				//							this.selectedRow.Selected = false;					
				//
				//						this.selectedRow = value;
				//
				//						// SSP 5/13/02 UWG1128 UWG1102
				//						// Also set the ActiveRow to the whatever selected row is.
				//						// 
				//						this.ActiveRow = value;
				//
				//						if ( this.selectedRow != null )
				//							this.selectedRow.Selected = true;				
				//
				//						// SSP 4/16/02 UWG1095
				//						// We have to syncronize the SelectedRow and SelectedItemIndex. However
				//						// only do so if we are not being called from within the SelectedItemIndex.
				//						//
				//						if ( !this.inSelectedItemIndex )
				//						{
				//							((IValueList)this).SelectedItemIndex = rowIndex;
				//						}
				//
				//						//Fire Event
				//						//
				//						RowSelectedEventArgs e = new RowSelectedEventArgs( this, this.selectedRow );
				//					
				//						this.FireRowSelected( e );
				//					}
				//					finally
				//					{
				//						this.inSelectedRow = false;
				//					}					
				//				}
			}

		}

		/// <summary>
		/// Returns whether the dropdown is currently dropped down.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this property if you need to determine the current state of the dropdown.</p>
		/// <p class="body">To drop down an <see cref="UltraCombo"/> control in code, use the <see cref="UltraCombo.PerformAction(UltraComboAction)"/> method and specify an <see cref="UltraComboAction"/> of <b>UltraComboAction.Dropdown.</b></p>
		/// <p class="body">To drop down an <see cref="UltraDropDown"/> control in code, use the <see cref="UltraGrid.PerformAction(UltraGridAction)"/> method of the <see cref="UltraGrid"/> control. Make sure the appropriate cell is active and use and specify an <see cref="UltraGridAction"/> of <b>UltraGridAction.EnterEditModeAndDropdown</b>.</p>
		/// </remarks>
		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden )]
		public bool IsDroppedDown
		{
			get
			{
				// AS 6/21/05 BR04702
				// We also need to check if the control is non-null
				// otherwise we will just be checking if something
				// is dropped down.
				//
				//// MRS 2/28/05 - BR02602
				////return Infragistics.Win.DropDownManager.IsDroppedDown( this.owner != null ? this.owner.Control : null );
				//return (this.owner != null) ? Infragistics.Win.DropDownManager.IsDroppedDown( this.owner.Control ) : false;
				if (this.owner != null)
				{
                    if (this.owner.Control != null)
                    {
                        // MBS 2/1/07 BR19662
                        if (this.ControlForGridDisplay != DropDownManager.GetCurrentDropDownControl(this.owner.Control))
                            return false;

                        return Infragistics.Win.DropDownManager.IsDroppedDown(this.owner.Control);
                    }

					Debug.Assert(!this.dropdownEventFired, "The dropdown event has not fired but we cannot get to a control to determine if we are currently dropped down.");
				}
				
				return false;
			}			
		}
		

		/// <summary>
		/// Determines which column (by <see cref="UltraGridColumn.Key"/>) in the dropdown is used to determine the value.
		/// </summary>
		/// <remarks>
		/// <p class="body">The ValueMember determines which column in the dropdown is used for the value. If this property is not set, the first column will be used. To determine the column that is being used as the ValueMember, you can use the <see cref="ValueMemberResolved"/> property.</p>
		/// <p class="body">For example, if you have a column in an <see cref="UltraGrid"/> control where the user can drop down a list and select a customer, you would use an <see cref="UltraDropDown"/> control and bind it to a table of customers. The customer table likely has a primary key to uniquely identify customers. This customer ID field would be used as the ValueMember of the <see cref="UltraDropDown"/>.</p> 
		/// <p class="body">This ID value would not be very useful to the users of the application. So you would set the <see cref="DisplayMember"/> to the field on the list that contains the customer's name. The grid cell would store the ID (probably in an integer or other numeric-type field), but the display would show the user-friendly customer name.</p>
		/// <p class="body">The <see cref="UltraCombo"/> control works the same way, except that it has it's own edit portion, instead of using a grid cell. The <see cref="UltraCombo.Value"/> is determined by the ValueMember and the display, and the <see cref="UltraCombo.Text"/> property is determined by the <see cref="DisplayMember"/>.</p>
		/// <p class="body"><see cref="DisplayMember"/> should only be set when you want to translate data values into more user-friendly display text. If you want the value stored and the value displayed on-screen to be the same, there is no need to set <see cref="DisplayMember"/>. Setting ValueMember is sufficient.</p>
		/// </remarks>
		/// <seealso cref="DisplayMember"/>
		/// <seealso cref="ValueMemberResolved"/>
		[
		// AS 4/5/06 BR11341
		//DefaultValue(null),
		DefaultValue(""),
		LocalizedCategory("LC_Data"),		
		Editor( "System.Windows.Forms.Design.DataMemberFieldEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing" ),
		LocalizedDescription("LD_UltraDropDownBase_P_ValueMember")
		]
		public string ValueMember
		{
			get
			{			
				return this.valueMember;
			}

			set
			{
				//we are only interested in the last section of the passed in 
				//text
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//string newValue = this.StripMember(value);
				string newValue = UltraDropDownBase.StripMember( value );

				if ( this.valueMember != newValue )
				{
                    //  BF 2/5/09   TFS13564
                    this.VerifyValueMember( newValue );

					this.valueMember = newValue;
		
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ValueMember );

					// MRS 6/1/06 - BR12914
					//
					// MRS 7/25/05 - BR04641
					//this.DirtySortedValueMemberListVersion();
					this.BumpListVersion();
				}
			}
		}

		// AS 5/21/04 UWG3103/UWG3126
		// Changed to public since we're no longer setting
		// the property directly.
		//
		//internal string ValueMemberResolved
		/// <summary>
		/// Returns the key of the column that is being used as the <see cref="ValueMember"/>.
		/// </summary>
		/// <remarks>
		/// <p class="body">If the <see cref="ValueMember"/> property has been set, then it's value is returned. If not, then the key of the first column is returned. If there are no columns, null is returned.</p>
		/// </remarks>
		/// <seealso cref="ValueMember"/>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public string ValueMemberResolved
		{
			get
			{
				
				if (this.valueMember != null && this.valueMember.Length > 0)
					return this.valueMember;

				if (this.displayMember != null && this.displayMember.Length > 0)
					return this.displayMember;

				UltraGridBand band = this.DisplayLayout.SortedBands[0];
				
				return band.Columns.Count == 0 ? null : band.Columns[0].Key;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeValueMember() 
		{
			return ( this.valueMember != string.Empty );
		}
 
		/// <summary>
		/// Reset the <see cref="ValueMember"/> property to the default (string.Empty).
		/// </summary>
		public void ResetValueMember() 
		{
			this.valueMember = string.Empty;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string StripMember ( string text )
		private static string StripMember( string text )
		{
            // MRS 3/17/06 - BR10736
            // Check for null. 
            if (text == null)
                return null;

			string[] strippedValue = text.Split('.');

			int count = strippedValue.Length - 1;
 
			return strippedValue[count];
		}


		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool DoesDataValueMatch( object dataValue1, object dataValue2 )
		internal static bool DoesDataValueMatch( object dataValue1, object dataValue2 )
		{
			// MRS 7/25/05 - BR04641
			// Replaced by CompareDataValue method
			//
			//			// SSP 8/21/02 UWG1597
			//			// Check for dataValue1 and dataValue2 being null.
			//			//
			//			// ----------------------------------------------------------
			//			if ( dataValue1 == dataValue2 )
			//				return true;
			//
			//			if ( null == dataValue1 || null == dataValue2 )
			//				return false;
			//
			//			if ( dataValue1 is DBNull || dataValue2 is DBNull )
			//				return false;
			//			// ----------------------------------------------------------
			//
			//			if ( dataValue1.GetType() == dataValue2.GetType() )
			//			{
			//				// if the types of the objects we are comparing are the same
			//				// then just do a plain comparision using Equals
			//				return dataValue1.Equals( dataValue2 );
			//			}
			//			else
			//			{
			//				// otherwise convert both of them to strings, and then compare them
			//				string s1 = dataValue1.ToString();
			//				string s2 = dataValue2.ToString();
			//
			//				return s1.Equals( s2 );
			//			}
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return this.CompareDataValue(dataValue1, dataValue2) == 0;
			return UltraDropDownBase.CompareDataValue( dataValue1, dataValue2 ) == 0;
		}


		private bool ContinueScrolling( ref Point mouseLoc )
		{
			// AS 8/31/01
			// The framework passes in the mouse buttons as logical buttons, not
			// physical ones so there is no need to swap the buttons.
			//
			//// check to see if mouse buttons have been swapped
			//MouseButtons buttons = SystemInformation.MouseButtonsSwapped ?  MouseButtons.Right : MouseButtons.Left;
			MouseButtons buttons = MouseButtons.Left;
			
			// check if the left button is not down
			//
			if ( ( Control.MouseButtons & buttons ) != buttons )
			{
				return false;
			}

			mouseLoc = this.ControlForGridDisplay.PointToClient ( System.Windows.Forms.Cursor.Position );

			return true;
		}

		// SSP 8/1/03 UWG1662
		// Exposed ReadOnly property off the UltraCombo.
		// Added IsReadOnly read-only (only get) virtual method to the UltraDropDownBase so
		// the any deriving class with a ReadOnly or a similar property can override this
		// and specify that the drop down should be read-only.
		//
		#region IsReadOnly

		/// <summary>
		/// Specifies whether the ultra drop down is read-only. Default implementation returns false.
		/// </summary>
		protected virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		#endregion // IsReadOnly

		// SSP 5/19/04 UWG3265
		// Overrode DefaultSize so when the user adds an UltraDropDown to the form
		// it is of a reasonable size since it has to show the new designer Start
		// button as well as the caption.
		//
		#region DefaultSize

		/// <summary>
		/// Returns the default size for the control.
		/// </summary>
		protected override Size DefaultSize
		{
			get 
			{ 
				return new Size( 424, 80 );
			}
		}

		#endregion // DefaultSize

		

		// MRS 11/16/04 - UWG2996
		// Made this a helper method so I can call it from several places. 
		// MRS 1/21/05 - BR01917
		//internal void ScrollSelectedRowIntoView()
		internal void ScrollSelectedRowIntoView(bool force)
		{	
			// MRS 1/21/05 - BR01917
			//if (!(this.IsDroppedDown))
			if (!(this.IsDroppedDown)
				|| force)
			{
				try
				{
					this.BeginUpdate();					
				
					// MRS 12/16/04 - BR00535
					// Even when SelectedItemIndex is -1, we still need to scroll 
					// to the top to avoid cutting off rows.
					UltraGridRow rowToKeepInView = (null != this.SelectedRow) ? this.SelectedRow : this.ActiveRowScrollRegion.FirstRow;
					
					// MRS 1/21/05 - BR01917
					//					//			if ( null != this.ActiveRowScrollRegion 
					//					//				&& null != this.SelectedRow 
					//					//				&& this.ActiveRowScrollRegion.IsLastRowVisible( false ) )
					//					//			{
					//					//				this.ActiveRowScrollRegion.Scroll( RowScrollAction.Top );
					//					//				this.ActiveRowScrollRegion.ScrollRowIntoView( this.SelectedRow );
					//					//			}
					//					if ( null != this.ActiveRowScrollRegion 
					//						&& null != rowToKeepInView
					//						&& this.ActiveRowScrollRegion.IsLastRowVisible( false ) )
					//					{
					//						this.ActiveRowScrollRegion.Scroll( RowScrollAction.Top );
					//						this.ActiveRowScrollRegion.ScrollRowIntoView( rowToKeepInView );
					//					}	
					if ( null != this.ActiveRowScrollRegion 
						&& null != rowToKeepInView )
					{
						this.ActiveRowScrollRegion.ScrollRowIntoView( rowToKeepInView );
					
						// SSP 4/28/05 - NAS 5.2 Fixed Rows
						// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
						// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
						//
						//if ( this.ActiveRowScrollRegion.IsLastRowVisible( false ) )
						if ( this.ActiveRowScrollRegion.IsLastScrollableRowVisible( false ) )
						{
							this.ActiveRowScrollRegion.Scroll( RowScrollAction.Top );						
							this.ActiveRowScrollRegion.ScrollRowIntoView( rowToKeepInView );
						}	
					}
					
				}
				finally
				{			
					this.EndUpdate();
				}
			}
		}

		// MRS 10/28/04 - UWG2996
		#region SelectedItemChangeHelper
		internal virtual void SelectedItemChangeHelper(ComboSelectedItemChangeContext context, object newData)
		{
            //  BF 3/5/08   FR09238 (EditorWithCombo AutoCompleteMode)
            EditorWithCombo.AutoCompleteInfo autoCompleteInfo = this.AutoCompleteInfo;
            if ( autoCompleteInfo != null &&
                 autoCompleteInfo.IsFilteredDropDownVisible &&
                 autoCompleteInfo.IsProcessingTextChanged )
                return;

			int newIndex = -1;
			UltraGridRow newRow = null;

			switch (context)
			{
				case ComboSelectedItemChangeContext.SelectedIndex:
					newIndex = (int)newData;

					if (newIndex == ((IValueList)this).SelectedItemIndex)
						return;
					
					if ( newIndex >=0 )
					{
						if ( newIndex < this.Rows.Count )
							newRow = this.Rows[newIndex];										
					}               					
					break;
				case ComboSelectedItemChangeContext.SelectedRow:
					newRow = newData as UltraGridRow;

					if (newRow == this.selectedRow)
						return;

					newIndex = null != newRow 
						? this.Rows.IndexOf( newRow )
						: -1;

					break;
			}

			if ( null != newRow && newIndex < 0 )
			{
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_287"),  Shared.SR.GetString("LE_ArgumentException_367") );
			}

			if ( this.selectedRow != null )
				this.selectedRow.Selected = false;

			this.selectedRow = newRow;
			
			// MRS 1/21/05 - I moved this down because when you hold down 
			// the up/down arrow key, the selection would dissappear
			//			if ( this.selectedRow != null && ((IValueList)this).IsDroppedDown )
			//				this.ActiveRowScrollRegion.ScrollRowIntoView( this.selectedRow );

			this.ActiveRow = newRow;			

			if ( this.selectedRow != null )
				this.selectedRow.Selected = true;
						
			((IValueList)this).SelectedItemIndex = newIndex;

			// MRS 1/21/05 - I moved this down because when you hold down 
			// the up/down arrow key, the selection would dissappear. Now
			// instead of disappearing, it flickers. Apparently, flicker is better.
			if ( this.selectedRow != null && ((IValueList)this).IsDroppedDown )
				this.ActiveRowScrollRegion.ScrollRowIntoView( this.selectedRow );

			if ( this.owner != null )
				this.owner.OnSelectedItemChanged();

			//Fire Event
			//
			RowSelectedEventArgs e = new RowSelectedEventArgs( this, this.selectedRow );
			this.FireRowSelected( e );
		}
		#endregion SelectedItemChangeHelper
		
		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Support HeaderClickAction in Combo and DropDown
		#region HeaderClickActionDefault
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal override HeaderClickAction HeaderClickActionDefault
		{
			get { return HeaderClickAction.Select;	}
		}
		#endregion HeaderClickActionDefault

		#region Dispose

		// SSP 6/23/05 BR04612
		// 
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{
			if ( disposing )
			{
				if ( null != this.toolTip )
				{
					this.toolTip.Dispose( );
					this.toolTip = null;
				}
			}

			base.Dispose( disposing );
		}

		#endregion // Dispose

		
		// MRS 7/25/05 - BR04641
		// Keep an ArrayList of the items in the ValueMember field and the DisplayMember field for 
		// binary searching. We will keep version numbers to know when these
		// lists need to be rebuilt. 
		#region BR04641

		#region SortedDisplayMemberList
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList SortedDisplayMemberList
		internal List<UltraGridRow> SortedDisplayMemberList
		{
			get
			{
				if (this.sortedDisplayMemberList == null ||
					this.DoesSortedDisplayMemberListNeedRebuilding())
				{
					this.BuildSortedDisplayMemberList();					
				}

				return this.sortedDisplayMemberList;
			}
		}
		#endregion SortedDisplayMemberList

		#region SortedValueMemberList
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList SortedValueMemberList
		internal List<UltraGridRow> SortedValueMemberList
		{
			get
			{
				if (this.sortedValueMemberList == null ||
					this.DoesSortedValueMemberListNeedRebuilding())
				{
					this.BuildSortedValueMemberList();					
				}

				return this.sortedValueMemberList;
			}
		}
		#endregion SortedValueMemberList

		#region BuildSortedDisplayMemberList

		private void BuildSortedDisplayMemberList()
		{
			//MRS 4/18/06 - BR11468
			Debug.Assert(this.DropDownSearchMethod != DropDownSearchMethod.Linear, "This list should not be built when doing linear searching.");

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//this.sortedDisplayMemberList = new ArrayList();
			this.sortedDisplayMemberList = new List<UltraGridRow>();

			for ( int i=0; i< this.Rows.Count; ++i)
				this.sortedDisplayMemberList.Add(this.Rows[i]);
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//Infragistics.Win.Utilities.SortMerge(this.sortedDisplayMemberList, new BinarySearchSortComparer(this, true));
			Utilities.SortMergeGeneric<UltraGridRow>( this.sortedDisplayMemberList, new BinarySearchSortComparer( this, true ) );

			this.UpdateSortedDisplayMemberListVersion();
		}

		#endregion BuildSortedDisplayMemberList

		#region BuildSortedValueMemberList

		private void BuildSortedValueMemberList()
		{			
			//MRS 4/18/06 - BR11468
			Debug.Assert(this.DropDownSearchMethod != DropDownSearchMethod.Linear, "This list should not be built when doing linear searching.");

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//this.sortedValueMemberList = new ArrayList();
			this.sortedValueMemberList = new List<UltraGridRow>();

			this.canDoBinarySearch = true;
			UltraGridColumn column = this.GetValueMemberColumnResolved();

			// MRS 11/3/05 - BR07505
			//
			this.lastValueMemberListType = null;

			for ( int i=0; i< this.Rows.Count; ++i)
			{
				UltraGridRow row = this.Rows[i];
				this.sortedValueMemberList.Add(row);
			
				if (column != null)
				{
					object o = row.GetCellValue(column);					

					// MRS 11/3/05 - BR07505
					//
					//					if ( o is IComparable ||
					//						o == null || 
					//						o is DBNull )
					//					{
					//						continue;
					//					}

					if (o == null ||
						o is DBNull)

					{
						continue;
					}

					if ( ! (o is IComparable) )
					{
						this.canDoBinarySearch = false;
						return;						
					}

					Type valueType = o.GetType();
					if ( this.lastValueMemberListType == null )					
						this.lastValueMemberListType = valueType;
					else if (this.lastValueMemberListType != valueType )
					{
						this.canDoBinarySearch = false;
						return;
					}
				}            
    
				// MRS 11/3/05 - BR07505
				//
				//				// If we reach this line of code, we hit something that is not IComparable.
				//				this.canDoBinarySearch = false;
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//Infragistics.Win.Utilities.SortMerge(this.sortedValueMemberList, new BinarySearchSortComparer(this, false));
			Utilities.SortMergeGeneric<UltraGridRow>( this.sortedValueMemberList, new BinarySearchSortComparer( this, false ) );

			this.UpdateSortedValueMemberListVersion();
		}

		#endregion BuildSortedValueMemberList

		#region UpdateSortedDisplayMemberListVersion

		private void UpdateSortedDisplayMemberListVersion()
		{
			// MRS 6/1/06 - BR12914
			//this.sortedDisplayMemberListVersion = this.Rows.VerifyVersion + this.Rows.sortVersion;
			this.sortedDisplayMemberListVersion = this.GetSortedListVersion();
		}
		#endregion UpdateSortedDisplayMemberListVersion

		#region UpdateSortedValueMemberListVersion

		private void UpdateSortedValueMemberListVersion()
		{
			// MRS 6/1/06 - BR12914
			//this.sortedValueMemberListVersion = this.Rows.VerifyVersion + this.Rows.sortVersion;
			this.sortedValueMemberListVersion = this.GetSortedListVersion();
		}
		#endregion UpdateSortedValueMemberListVersion

		#region GetSortedListVersion

		private int GetSortedListVersion()
		{
			// MRS 6/1/06 - BR12914
			//return this.Rows.VerifyVersion + this.Rows.sortVersion;
			RowsCollection rows = this.Rows;

			return rows.VerifyVersion + rows.sortVersion + this.listVersion;
		}
		#endregion GetSortedListVersion

		
		#region DirtySortedDisplayMemberListVersion
		// MRS 6/1/06 - BR12914
		// No longer used. We are using listVersion now. 
//		internal void DirtySortedDisplayMemberListVersion()
//		{
//			this.sortedDisplayMemberListVersion -= 1;
//		}
		#endregion DirtySortedDisplayMemberListVersion

		#region DirtySortedValueMemberListVersion
		// MRS 6/1/06 - BR12914
		// No longer used. We are using listVersion now. 
//		internal void DirtySortedValueMemberListVersion()
//		{
//			this.sortedValueMemberListVersion -= 1;
//		}
		#endregion DirtySortedValueMemberListVersion

		#region DoesSortedDisplayMemberListNeedRebuilding
		internal bool DoesSortedDisplayMemberListNeedRebuilding()
		{
			return this.sortedDisplayMemberListVersion != this.GetSortedListVersion();
		}
		#endregion DoesSortedDisplayMemberListNeedRebuilding

		#region DoesSortedValueMemberListNeedRebuilding
		internal bool DoesSortedValueMemberListNeedRebuilding()
		{
			return this.sortedValueMemberListVersion != this.GetSortedListVersion();
		}
		#endregion DoesSortedValueMemberListNeedRebuilding

		#region CompareDataValue
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private int CompareDataValue( object dataValue1, object dataValue2 )
		private static int CompareDataValue( object dataValue1, object dataValue2 )
		{
			// SSP 8/21/02 UWG1597
			// Check for dataValue1 and dataValue2 being null.
			//
			// ----------------------------------------------------------
			if ( dataValue1 == dataValue2 )
				return 0;

			if ( null == dataValue1 )
				return -1;
			
			if ( null == dataValue2 )
				return 1;

			if ( dataValue1 is DBNull )
				return -1;

			if ( dataValue2 is DBNull )
				return 1;
			// ----------------------------------------------------------

			if ( dataValue1.GetType() == dataValue2.GetType() )
			{
				// if the types of the objects we are comparing are the same
				// then just do a plain comparision using Equals
				return Infragistics.Win.UltraWinGrid.RowsCollection.RowsSortComparer.DefaultCompare(dataValue1, dataValue2, false , false);					
			}
			else
			{
				// otherwise convert both of them to strings, and then compare them
				string s1 = dataValue1.ToString();
				string s2 = dataValue2.ToString();

				return String.Compare( s1, s2, true, System.Globalization.CultureInfo.CurrentCulture );				
			}
		}
		#endregion CompareDataValue

		#region GetDisplayMemberColumnResolved

		internal UltraGridColumn GetDisplayMemberColumnResolved()
		{
			this.InitializeValueMemberIfToFirstColumn( );

			UltraGridColumn column = null;

			ColumnsCollection columns = this.DisplayLayout.SortedBands[0].Columns;
			string displayMemberResolved = this.DisplayMemberResolved;

			if ( columns.Exists( displayMemberResolved ) )
				column = columns[ displayMemberResolved ];

			if ( column == null && null != displayMemberResolved && displayMemberResolved.Length > 0 &&
				this.IsInitializedWithDataSource )
				throw new ArgumentException( SR.GetString( "LER_Exception_324", displayMemberResolved ), displayMemberResolved );

			return column;
		}
		#endregion GetDisplayMemberColumnResolved

		#region GetValueMemberColumnResolved

		private UltraGridColumn GetValueMemberColumnResolved()
		{
			this.InitializeValueMemberIfToFirstColumn( );

			UltraGridColumn column = null;

			ColumnsCollection columns = this.DisplayLayout.SortedBands[0].Columns;
			string valueMemberResolved = this.ValueMemberResolved;

			if ( columns.Exists( valueMemberResolved ) )
				column = columns[ valueMemberResolved ];
			
			if ( column == null && 
				null != valueMemberResolved && valueMemberResolved.Length > 0 &&
				this.IsInitializedWithDataSource )
				throw new ArgumentException( SR.GetString( "LER_Exception_324", valueMemberResolved ), valueMemberResolved );

			return column;
		}
		#endregion GetValueMemberColumnResolved

		#region PerformBinarySearch		
		private object PerformBinarySearch(object dataValue, ref int index, bool getValue)
		{			
			UltraGridColumn column = getValue
				? this.GetDisplayMemberColumnResolved()
				: this.GetValueMemberColumnResolved();

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//int binaryIndex	= getValue
			//    ? this.SortedDisplayMemberList.BinarySearch((string)dataValue, new BinarySearchSortComparer(this, getValue))
			//    : this.SortedValueMemberList.BinarySearch(dataValue, new BinarySearchSortComparer(this, getValue));
			BinarySearchSortComparer comparer = new BinarySearchSortComparer( this, getValue );

			int binaryIndex = getValue
				? comparer.BinarySearchValues( this.SortedDisplayMemberList, (string)dataValue )
				: comparer.BinarySearchValues( this.SortedValueMemberList, dataValue );

			if (binaryIndex >= 0 )
			{
				// We found a matching item. Now walk up to find the first one. 
				int previousIndex = binaryIndex - 1;	
				while (previousIndex >= 0 &&
					( getValue 
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//? this.DoesTextMatch( ((UltraGridRow)this.SortedDisplayMemberList[previousIndex]).GetCellText(column), (string)dataValue, false)
						//// MD 7/26/07 - 7.3 Performance
						//// FxCop - Mark members as static
						////: this.DoesDataValueMatch( ((UltraGridRow)this.SortedValueMemberList[previousIndex]).GetCellValue(column), dataValue) ) )
						//: UltraDropDownBase.DoesDataValueMatch( ( (UltraGridRow)this.SortedValueMemberList[ previousIndex ] ).GetCellValue( column ), dataValue ) ) )
						? this.DoesTextMatch( this.SortedDisplayMemberList[ previousIndex ].GetCellText( column ), (string)dataValue, false )
						: UltraDropDownBase.DoesDataValueMatch( this.SortedValueMemberList[ previousIndex ].GetCellValue( column ), dataValue ) ) )
				{
					binaryIndex = previousIndex;
					previousIndex = binaryIndex - 1;
				}
					
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//UltraGridRow row = getValue
				//    ? (UltraGridRow)this.SortedDisplayMemberList[binaryIndex]
				//    : (UltraGridRow)this.SortedValueMemberList[binaryIndex];
				UltraGridRow row = getValue
					? this.SortedDisplayMemberList[ binaryIndex ]
					: this.SortedValueMemberList[ binaryIndex ];

				int realIndex = row.Index;				
				index = realIndex;
				return getValue
					? ((IValueList)this).GetValue(realIndex)
					: ((IValueList)this).GetText(realIndex);
			}

			index = -1;
			return null;
		}
		#endregion PerformBinarySearch

		#region CanDoBinarySearch

		/// <summary>
		/// Determines if a Binary search can be performed based on the data type of values in the ValueMember column. This is not needed for DisplayMember, since it will always be comparing strings.
		/// </summary>
		/// <returns></returns>
		private bool CanDoBinarySearch
		{
			get
			{				
				//MRS 4/18/06 - BR11468
				if (this.DropDownSearchMethod == DropDownSearchMethod.Linear)
					return false;

				UltraGridColumn column = this.GetValueMemberColumnResolved();

				if (column == null)
					return false;


				// MRS 11/3/05 - BR07505				
				// This check is not really a good idea. The type of the column
				// may be IComparable, but the column could still contain objects 
				// of different types and thus the comparison may not be valid.
				//
//				// If the datatype of the column is IComparable, then 
//				// it's okay to do a Binary Search. 
//				if (typeof(IComparable).IsAssignableFrom(column.DataType))
//					return true;

				// Ensure that the sorted ValueMemberList is built so that the
				// canDoBinarySearch member gets set. 
				if (this.sortedValueMemberList == null ||
					this.DoesSortedValueMemberListNeedRebuilding())
				{
					this.BuildSortedValueMemberList();
				}

				return this.canDoBinarySearch;
			}
		}

		#endregion CanDoBinarySearch

		#region BinarySearchSortComparer Class
		// This SortComparer will be used to sort the list of rows and also in 
		// order to perform a Binary Search on the rows in order to find
		// a particular item in the list more efficiently than a linear loop. 
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private class BinarySearchSortComparer : IComparer
		private class BinarySearchSortComparer : IComparer<UltraGridRow>
		{	
			#region Private Members

			private bool useText = true;			
			private UltraGridColumn column = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//private UltraDropDownBase ultraDropDownBase = null;

			#endregion Private Members

			#region Constructor
			public BinarySearchSortComparer(UltraDropDownBase ultraDropDownBase, bool useText)
			{
				Debug.Assert(ultraDropDownBase != null, "ultraDropDownBase cannot be null");

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.ultraDropDownBase = ultraDropDownBase;

				this.column = useText
					? ultraDropDownBase.GetDisplayMemberColumnResolved()
					: ultraDropDownBase.GetValueMemberColumnResolved();

				this.useText = useText;
			}	
			#endregion Constructor

			#region Methods

			#region BinarySearchValues

			public int BinarySearchValues( List<UltraGridRow> rows, object value )
			{
				int start = 0;
				int end = rows.Count - 1;

				while ( start <= end )
				{
					int midPoint = start + ( ( end - start ) / 2 );

					int compareValue = this.CompareHelper( this.GetValue( rows[ midPoint ] ), value );

					if ( compareValue == 0 )
						return midPoint;

					if ( compareValue < 0 )
						start = midPoint + 1;
					else
						end = midPoint - 1;
				}

				return ~start;
			}

			#endregion BinarySearchValues

			#region GetValue

			private object GetValue( UltraGridRow x )
			{
				return this.useText ?
					   x.GetCellText( column ) :
					   x.GetCellValue( column );
			}

			#endregion GetValue

			#endregion Methods

			#region IComparer Implementation

			public int Compare( UltraGridRow x, UltraGridRow y )
			{
				if ( x == y )
					return 0;

				object xObj = null;

				if ( x != null )
					xObj = this.GetValue( x );

				object yObj = null;

				if ( y != null )
					yObj = this.GetValue( y );

				return this.CompareHelper( xObj, yObj );
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//public int Compare( object x, object y )
			private int CompareHelper( object xObj, object yObj )
			{
				// MD 8/10/07 - 7.3 Performance
				// Moved to the IComparer<UltraGridRow> version of Compare
				#region Moved

				//UltraGridRow xRow = x as UltraGridRow;
				//UltraGridRow yRow = y as UltraGridRow;
				//
				//if (xRow == yRow)
				//    return 0;
				//
				//object xObj = null;			
				//
				//if (xRow == null)
				//    xObj = x;
				//else
				//{
				//    xObj = this.useText ? 
				//        xRow.GetCellText(column) :
				//        xRow.GetCellValue(column);
				//}
				//
				//object yObj = null;
				//
				//if (yRow == null)
				//    yObj = y;
				//else
				//{
				//    yObj = this.useText ? 
				//        yRow.GetCellText(column) :
				//        yRow.GetCellValue(column);
				//}

				#endregion Moved

				int compare;
				if (this.useText)
					return String.Compare( (string)xObj, (string)yObj, true, System.Globalization.CultureInfo.CurrentCulture ) ;
				else
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//compare = this.ultraDropDownBase.CompareDataValue(xObj, yObj);
					compare = UltraDropDownBase.CompareDataValue( xObj, yObj );
				}
                				
				return compare;
			}		
			#endregion IComparer Implementation	
		}	

		#endregion BinarySearchSortComparer Class

		#endregion BR04641

		// MRS 10/19/05 - BR06916
		// Moved this up from UltraDropDown, since the workaround
		// is good for UltraCombo, too. 
		#region BindingContext

		// SSP 8/25/05 BR05793
		// If someone creates an UltraDropDown in code without adding it to Controls collection
		// of a form or container, then the BindingContext will be null and the UDD won't be 
		// able to bind. Worse yet, if such an udd is used as ValueList of a column then the
		// the first time it's dropped down it won't have any data however the next time it's
		// dropped down it will because the udd gets added to the drop down form, causing it
		// to have a BindingContext. Also when the udd is closed up after dropping down, its
		// BindingContext will become null. This is to fix that.
		// 
		/// <summary>
		/// Overridden. Gets or sets the binding context that should be used for data binding
		/// purposes. This property must be set before data binding.
		/// </summary>
		public override BindingContext BindingContext
		{
			get
			{
				BindingContext val = this.bindingContext;

				if ( null == val )
				{
					val = base.BindingContext;

					if ( null == val && this.inSet_ListManager )
						val = this.bindingContext = new BindingContext( );
				}

				return val;
			}
			set
			{
				this.bindingContext = null;
				base.BindingContext = value;                                
    		}
		}

		#endregion // BindingContext

		//MRS 4/18/06 - BR11468
		#region MRS 4/18/06 - BR11468

		#region DropDownSearchMethod
		/// <summary>
		/// Determines how the control searches the dropdown list when attempting to find a DataValue or DisplayText.
		/// </summary>
		[ LocalizedDescription("LD_UltraDropDownBase_P_DropDownSearchMethod")]
		[ LocalizedCategory("LC_Behavior") ]
		public DropDownSearchMethod DropDownSearchMethod
		{
			get
			{
				return this.dropDownSearchMethod;
			}

			set
			{
				if ( this.dropDownSearchMethod != value )
				{
                    // MRS NAS v8.3 - Unit Testing
                    GridUtils.ValidateEnum("DropDownSearchMethod", typeof(DropDownSearchMethod), value);

					this.dropDownSearchMethod = value;
		
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DropDownSearchMethod );					
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDropDownSearchMethod() 
		{
			return ( this.dropDownSearchMethod != DropDownSearchMethod.Default);
		}
 
		/// <summary>
		/// Reset the <see cref="DropDownSearchMethod"/> property to the default (DropDownSearchMethod.Default).
		/// </summary>
		public void ResetDropDownSearchMethod() 
		{
			this.DropDownSearchMethod = DropDownSearchMethod.Default;
		}
		#endregion DropDownSearchMethod

		#region EditAreaDisplayStyle
		/// <summary>
		/// Determines what is shown in the edit portion of the control when an item is selected.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property determines whether the control displays only the DisplayMember of the selected row or if it also displays the image of that cell.</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraDropDownBase_P_EditAreaDisplayStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		public EditAreaDisplayStyle EditAreaDisplayStyle
		{
			get
			{
				return this.editAreaDisplayStyle;
			}

			set
			{
				if ( this.editAreaDisplayStyle != value )
				{
                    // MRS NAS v8.3 - Unit Testing
                    GridUtils.ValidateEnum("EditAreaDisplayStyle", typeof(EditAreaDisplayStyle), value);
                    
					this.editAreaDisplayStyle = value;						
		
					if (this.IsHandleCreated)
						this.DisplayLayout.UIElement.DirtyChildElements();

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.EditAreaDisplayStyle );					
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeEditAreaDisplayStyle() 
		{
			return ( this.editAreaDisplayStyle != EditAreaDisplayStyle.Default);
		}
 
		/// <summary>
		/// Reset the <see cref="EditAreaDisplayStyle"/> property to the default (EditAreaDisplayStyle.Default).
		/// </summary>
		public void ResetEditAreaDisplayStyle() 
		{
			this.EditAreaDisplayStyle = EditAreaDisplayStyle.Default;
		}
		#endregion EditAreaDisplayStyle

		#endregion MRS 4/18/06 - BR11468

		// MRS 6/1/06 - BR12914
		#region BumpListVersion
		private void BumpListVersion()
		{
			if (this.listVersion < int.MaxValue)
				this.listVersion++;				
			else
				this.listVersion = int.MinValue;
		}
		#endregion BumpListVersion

        //  BF 2/29/08  FR09238 (EditorWithCombo AutoCompleteMode)
        #region FR09238 (EditorWithCombo AutoCompleteMode)

            #region FilterAutoCompleteItems
        internal virtual int FilterAutoCompleteItems( string text, bool apply )
        {
            return -1;
        }
            #endregion FilterAutoCompleteItems

            #region AutoCompleteInfo
        internal virtual EditorWithCombo.AutoCompleteInfo AutoCompleteInfo
        {
            get { return null; }
        }
            #endregion AutoCompleteInfo

        #endregion FR09238 (EditorWithCombo AutoCompleteMode)

        //  BF 12/2/08  NA 9.1 - UltraCombo MultiSelect
        #region NA 9.1 - UltraCombo MultiSelect

            #region ProcessMouseUp
        internal virtual void ProcessMouseUp( MouseMessageInfo msgInfo, out bool closeDropDown )
        {
            closeDropDown = true;
        }
            #endregion ProcessMouseUp

            #region SetSelectedIndexBeforeDroppingDown
        internal virtual void SetSelectedIndexBeforeDroppingDown( string text, ref int index )
        {
            IValueList valueList = this as IValueList;
            if ( valueList == null )
            {
                Debug.Fail( "SetSelectedIndexBeforeDroppingDown called on something that does not implement IValueList." );
                return;
            }

			if ( valueList.GetValue( text, ref index ) != null )
				valueList.SelectedItemIndex = index;
			else
				valueList.SelectedItemIndex = -1;
        }
            #endregion SetSelectedIndexBeforeDroppingDown

        //  BF 2/5/09   TFS13564
            #region VerifyValueMember
        internal virtual void VerifyValueMember( string newValue ){}
            #endregion VerifyValueMember

        //  BF 2/24/09   TFS12735
            #region IsActivatingRowByMouse
        internal bool IsActivatingRowByMouse
        {
            get { return this.isActivatingRowByMouse; }
        }
            #endregion IsActivatingRowByMouse

        #endregion NA 9.1 - UltraCombo MultiSelect

        // MRS 3/16/2009 - TFS15302
        #region TFS15302

        #region IProvideItemImageSize Members

        Size IProvideItemImageSize.GetItemImageSize(AppStyling.ComponentRole componentRole, ImageList imageList)
        {
            this.VerifyItemImageSize(componentRole, imageList);

            return this.itemImageSize;
        }

        #endregion //IProvideItemImageSize Members

        #region VerifyItemImageSize
        private void VerifyItemImageSize(AppStyling.ComponentRole componentRole, ImageList imageList)
        {
            if (this.lastVerifiedItemImageSizeVersion == this.itemImageSizeVersion + this.listVersion)                
                return;

            this.lastVerifiedItemImageSizeVersion = this.itemImageSizeVersion + this.listVersion;            

            IValueList list = (IValueList)this;
            int itemCount = list.ItemCount;

            Image img = null;
            Size largestImageSize = Size.Empty;
            for (int i = 0; i < itemCount; i++)
            {
                AppearanceData appData = new AppearanceData();
                AppearancePropFlags flags = AppearancePropFlags.Image;

                // The UltraDropDown's IValueList.ResolveItemAppearance does not use the 
                // componentRole so don't use it here.
                componentRole = null;

                list.ResolveItemAppearance(i, ref appData, ref flags, componentRole);

                img = appData.GetImage(imageList);
                if (img != null)
                {
                    largestImageSize.Width = System.Math.Max(largestImageSize.Width, img.Size.Width);
                    largestImageSize.Height = System.Math.Max(largestImageSize.Height, img.Size.Height);
                }
            }

            this.itemImageSize = largestImageSize;
        }
        #endregion //VerifyItemImageSize

        #region BumpItemImageSizeVersion
        internal void BumpItemImageSizeVersion()
        {
            if (this.itemImageSizeVersion == int.MaxValue)
                this.itemImageSizeVersion = int.MinValue;
            else
                this.itemImageSizeVersion++;
        }
        #endregion //BumpItemImageSizeVersion

        #endregion //TFS15302
    }
	
}
