#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// CaptionAreaUIElement. This represents the grid's caption.
    /// </summary>
	// AS - 12/14/01
	// Changed to support themes.
	//public class CaptionAreaUIElement : UIElement
	public class CaptionAreaUIElement : Infragistics.Win.HeaderUIElementBase
    {

		// JJD 12/15/03 - Added Accessibility support
		private AccessibleObject accessibilityObject;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">parent UI element</param>
		// AS - 12/14/01
		// Both the grid's caption and the other headers use the same base
		// class (which derives from adjustable item. Since this header is not 
		// adjustable, return false for both other constructor args.
		//
		//public CaptionAreaUIElement(UIElement parent ) : base( parent )
		public CaptionAreaUIElement(UIElement parent ) : base( parent, false, false )
        {
        }

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
        protected override void PositionChildElements()
        {
            TextUIElement			textElement				= null;
			ImageUIElement			imageElement			= null;

			if ( this.childElementsCollection != null )
			{
				textElement		= (TextUIElement)CaptionAreaUIElement.ExtractExistingElement( this.childElementsCollection, typeof(TextUIElement), true );
				imageElement	= (ImageUIElement)CaptionAreaUIElement.ExtractExistingElement( this.childElementsCollection, typeof(ImageUIElement), true );

				this.childElementsCollection.Clear();
			}

			if ( null == textElement )
			{
				textElement = new TextUIElement( this, this.Control.Text );
				// MRS 10/19/05 - BR06757
				textElement.MultiLine = true;
			}
			else
				textElement.Text = this.Control.Text;

			Rectangle textRect = this.RectInsideBorders;

			textRect.Inflate(-1,0);

			// JJD 08/17/01 - UWG59
			// Added support for displaying images in cells
			//
			AppearanceData appData = new AppearanceData();
			AppearancePropFlags flags = AppearancePropFlags.Image
										| AppearancePropFlags.ImageHAlign 
										| AppearancePropFlags.ImageVAlign
										| AppearancePropFlags.TextHAlign 
										| AppearancePropFlags.TextVAlign;
			
			this.InitAppearance( ref appData, ref flags );

			if ( appData.Image != null )
			{
				Image image = appData.GetImage( ((UltraGridBase)this.Control).ImageList );

				if ( image != null )
				{
					if ( null == imageElement )
					{
						imageElement = new ImageUIElement( this, image );
					}
					else
					{
						imageElement.Image = image;
					}

					imageElement.Scaled = true;

					Size size = image.Size;

					// shrink the image size if necessary
					//
					if ( textRect.Height > 2 && size.Height > textRect.Height - 2 )
					{
						// proportinally shrink the width
						//
						size.Width  = (int)((double)size.Width * ( (double)(textRect.Height - 2) / (double)size.Height ));

						// shrink the height
						//
						size.Height = textRect.Height - 2;
					}

					// make sure the width isn't too big
					//
					if ( textRect.Width > 2 && size.Width > textRect.Width - 2 )
						size.Width = textRect.Width - 2;

					Rectangle imgRect	= textRect;
					imgRect.Width		= size.Width;
					imgRect.Height		= size.Height;

					//move image based on alignment
					DrawUtility.AdjustHAlign( appData.ImageHAlign, ref imgRect, textRect ); 
					DrawUtility.AdjustVAlign( appData.ImageVAlign, ref imgRect, textRect );

					// make sure the imgae has at least a 1 pixel 
					// padding on top and bottom
					//
					if ( imgRect.Top == textRect.Top )
						imgRect.Y++;
					else
					if ( imgRect.Bottom == textRect.Bottom )
						imgRect.Y--;


					// adjust the text rect appropriately
					//
					switch ( appData.ImageHAlign )
					{
						case HAlign.Center:
							break;

						case HAlign.Right:
							imgRect.X--;
							textRect.Width -= imgRect.Width + 1;
							break;

						default:
						case HAlign.Left:
							imgRect.X++;
							textRect.Width	-= imgRect.Width + 1;
							textRect.X		+= imgRect.Width + 1;
							break;
					}

					imageElement.Rect = imgRect;

					this.ChildElements.Add( imageElement );

				}
			}

            textElement.Rect = textRect;
  
			// JJD 11/07/01
			// TextAlignment property has been removed
			//
//			textElement.TextAlignment = Win.AppearanceData.ContentAlignmentFromHVAlign( appData.TextHAlign, appData.TextVAlign );

            this.ChildElements.Add( textElement );

        }

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
                                                 ref AppearancePropFlags requestedProps )
		{
			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.UltraGridLayout),requestedProps );

            // Set the caption flag and call the layout's ResolveAppearance method
            //
            context.IsCaption = true;

			UltraGridLayout layout = ((UltraGridBase)this.Control).DisplayLayout;

			// SSP 3/13/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( layout, StyleUtils.Role.GridCaption, out context.ResolutionOrder );

			layout.ResolveAppearance( ref appearance, ref context );
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 11/6/01 UWG666
				// Use the new internal property BorderStyleCaptionResolved
				//
				//return ((UltraGridBase)this.Control).DisplayLayout.BorderStyleHeaderDefault;
				return ((UltraGridBase)this.Control).DisplayLayout.BorderStyleCaptionResolved;
			}
		}

		/// <summary>
		/// See <see cref="Infragistics.Win.UIElement.BorderSides"/>
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return Border3DSide.All;
			}
		}

		// JJD 12/23/03 - Accessibility
		#region IsAccessibleElement property

		/// <summary>
		/// Indicates if the element supports accessibility
		/// </summary>
		public override bool IsAccessibleElement { get { return true; } }

		#endregion IsAccessibleElement property

		// JJD 12/15/03 - Added Accessibility support
		#region AccessibilityInstance property

		/// <summary>
		/// Returns the accesible object representing the data area of the grid.
		/// </summary>
		public override AccessibleObject AccessibilityInstance
		{
			get
			{
				if ( this.accessibilityObject == null )
				{
					UltraGridUIElement gridElement = this.GetAncestor( typeof(UltraGridUIElement) ) as UltraGridUIElement;

					if ( gridElement != null )
					{
						UltraGridBase grid = gridElement.Grid;

						if ( grid != null )
							this.accessibilityObject = grid.CreateAccessibilityInstance	( this );
					}
				}

				return this.accessibilityObject;
			}
		}

		#endregion AccessibilityInstance property

		// JJD 12/15/03 - Added Accessibility support
		#region Public Class CaptionAreaAccessibleObject

		/// <summary>
		/// The Accessible object for the grid's caption ares.
		/// </summary>
		public class CaptionAreaAccessibleObject : UIElementAccessibleObject
		{

			#region Constructor
			
			/// <summary>
            /// Constructor.
			/// </summary>
            /// <param name="captionElement">The CaptionAreaUIElement</param>
			public CaptionAreaAccessibleObject ( CaptionAreaUIElement captionElement ) : base( captionElement, AccessibleRole.TitleBar, Shared.SR.GetString("AccessibleName_Caption") )
			{
			}

			#endregion Constructor

			#region Base Class Overrides

				#region State

			/// <summary>
			/// Gets the state of this accessible object.
			/// </summary>
			public override AccessibleStates State
			{
				get
				{
					AccessibleStates state = base.State | AccessibleStates.ReadOnly;
				
					return state;
				}
			}

				#endregion State

				#region Value

			/// <summary>
			/// Returns the caption
			/// </summary>
			public override string Value
			{
				get
				{
					return this.Grid.Text;
				}
				set
				{
				}
			}

				#endregion Value

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Grid

			/// <summary>
			/// Returns the associated grid control.
			/// </summary>
			public UltraGridBase Grid 
			{ 
				get 
				{
					UltraGridUIElement gridElement = this.UIElement.GetAncestor( typeof(UltraGridUIElement) ) as UltraGridUIElement;

					if ( gridElement != null )
						return gridElement.Grid;

					return null; 
				} 
			}

					#endregion Grid

				#endregion Public Properties

			#endregion Properties

		}

		#endregion Public Class CaptionAreaAccessibleObject

		#region Layout

		// SSP 3/20/06 - App Styling
		// 
		internal UltraGridLayout Layout
		{
			get
			{
				return ((UltraGridBase)this.Control).DisplayLayout;
			}
		}

		#endregion // Layout
		
		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Layout, StyleUtils.Role.GridCaption );
			}
		}

		#endregion // UIRole
   }

}
