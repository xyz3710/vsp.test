#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Diagnostics;
    using Infragistics.Win.Layout;

    /// <summary>
    /// HeaderUIElement
    /// </summary>
	// AS - 12/14/01 Changed to support themes.
    //public class HeaderUIElement : AdjustableUIElement
	public class HeaderUIElement : Infragistics.Win.HeaderUIElementBase
    {
		// SSP 6/29/05 - NAS 5.3 Column Chooser
		// 
		//private bool sortClickPending = false;
		internal bool sortClickPending = false;

		private Point lastClickedPoint = Point.Empty;		
		private bool dragging = false;
		private bool startDragging = false;

		// SSP 3/11/03 - UWG2047
		// When the header click action is to sort, then we return false from OnMouseDown
		// so the selection statergy doesn't take over since the click may be for changing
		// the sort indicator. In OnMouseMove, we start the drag operation ourselves
		// when the mouse moves over 4 pixels. However as a result of starting the drag
		// ourselves, the selection stratergy doesn't get involved in the mouse move and
		// scrolling and things like that so we have to do our own scrolling as necessary.
		// To do that we need a timer.
		//
		private System.Windows.Forms.Timer dragScrollTimer = null;

		// SSP 6/5/03 - Fixed headers
		//
		private Rectangle clipSelfRect = Rectangle.Empty;

		// SSP 6/5/03 - Optimizations
		//
		internal UIElementBorderStyle borderStyle = UIElementBorderStyle.None;

		// MRS 4/6/05 - Span Resizing for RowLayouts
		private bool wasShowingSpanCursor = false;

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		private bool isHotTracking = false;

		// MD 2/12/09 - TFS13812
		private bool isRightFixedHeaderSeparator;

		// MD 2/26/09 - TFS14626
		private bool forceMouseDownProcessing;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		// SSP 3/14/03 - Row Layout Functionality
		// Allow resizing both horizontally and vertically in row-layout mode.
		//
        //public HeaderUIElement( UIElement parent ) : base( parent, true, false )
		public HeaderUIElement( UIElement parent ) : base( parent, true, true )
        {
        }

		// SSP 7/21/03 Workaround for UWG1558
		// Allow the user to be able to construct the header ui element and pass 
		// in a valid header context. Added the following constructor that takes
		// in a header as the context.
		//
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		/// <param name="header"></param>
		// MD 2/12/09 - TFS13812
		// Call off to the new constructor
		//public HeaderUIElement( UIElement parent, HeaderBase header ) : this( parent )
		public HeaderUIElement( UIElement parent, HeaderBase header )
			: this( parent, header, false )
		{
			// MD 2/12/09 - TFS13812
			// The new constructor calls InitializeHeader
			//this.InitializeHeader( header );
		}

		// MD 2/12/09 - TFS13812
		// Added a parameter to specify whether the header is a right fixed header separator.
		internal HeaderUIElement( UIElement parent, HeaderBase header, bool isRightFixedHeaderSeparator )
			: this( parent )
		{
			this.InitializeHeader( header, isRightFixedHeaderSeparator );
		}

		internal void InitializeHeader( Infragistics.Win.UltraWinGrid.HeaderBase header )
		{
			// MD 2/12/09 - TFS13812
			// Moved coe to the new overload
			this.InitializeHeader( header, false );
		}

		// MD 2/12/09 - TFS13812
		// Added an extra parameter to InitializeHeader to specify whether the element is a right fixed header separator element.
		internal void InitializeHeader( Infragistics.Win.UltraWinGrid.HeaderBase header, bool isRightFixedHeaderSeparator )
        {
            this.PrimaryContext = header;

			// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
			// 
			this.clipSelfRect = Rectangle.Empty;

			// MD 2/12/09 - TFS13812
			this.isRightFixedHeaderSeparator = isRightFixedHeaderSeparator;
        }

        /// <summary>
        /// Returns an object of requested type that relates to the element or null.
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
        // AS - 10/31/01
		//public virtual object GetContext( Type type )
		public override object GetContext( Type type, bool checkParentElementContexts )
        {
			// MD 8/3/07 - 7.3 Performance
			// The logic in this section was incorrect and redundant, it has been reimplemented below
			#region Refactored

			//// since the type may be the derived class of HeaderBase or
			//// Headerbase, we need to check both
			////
			//// AS 10/16/02
			//// Make sure the type is not null.
			////
			//if ( type != null &&
			//    ( typeof( HeaderBase ) == type ||
			//     type.IsSubclassOf( typeof( HeaderBase ) ) ) )
			//{
			//    // if the context is of the passed in type return it
			//    //
			//    if ( this.PrimaryContext != null )
			//    {
			//        if ( this.PrimaryContext.GetType() == type ||
			//             this.PrimaryContext.GetType().IsSubclassOf( type ) )
			//            return this.PrimaryContext;
			//    }
			//}
			//
			//// MRS 8/2/05 - Added a Context of UltraGridColumn
			//if ( type != null &&
			//    ( typeof( UltraGridColumn ) == type ||
			//    type.IsSubclassOf( typeof( UltraGridColumn ) ) ) )
			//{
			//    HeaderBase headerBase = this.GetContext( typeof( HeaderBase ) ) as HeaderBase;
			//    if ( headerBase != null )
			//        return headerBase.Column;
			//}
			//
			//// MRS 11/1/05 - BR07432
			//// TODO - Fill in bug number
			//// Getting a cell from a header doesn't make sense. 
			////			// MRS 8/2/05 - Added a Context of UltraGridCell
			////			if (type != null &&
			////				( typeof( UltraGridCell ) == type              ||
			////				type.IsSubclassOf( typeof( UltraGridCell ) ) ) )
			////			{
			////				UltraGridRow row = this.GetContext(typeof(UltraGridRow)) as UltraGridRow;
			////				if (row != null)
			////				{
			////					UltraGridColumn column = this.GetContext(typeof(UltraGridColumn)) as UltraGridColumn;
			////					if (column != null)
			////						return row.Cells[column.Key];
			////				}
			////			}
			//
			//return base.GetContext( type, checkParentElementContexts );

			#endregion Refactored
			object result = base.GetContext( type, checkParentElementContexts );

			if ( result != null )
				return result;

			if ( type != null &&
				( typeof( UltraGridColumn ) == type ||
				type.IsAssignableFrom( typeof( UltraGridColumn ) ) ) )
			{
				HeaderBase headerBase = this.GetContext( typeof( HeaderBase ) ) as HeaderBase;

				if ( headerBase != null )
					return headerBase.Column;
			}

			return null;
        }


		/// <summary>
		/// Returns the Header object associated with the UIElement. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Header</b> property of an object refers to a column or group header, as defined by an Header object. You use the <b>Header</b> property to access the properties of a specified Header object, or to return a reference to a Header object.</p>
		/// <p class="body">A Header object represents a column or group header that specifies information about the column or group, and can also serve as the interface for functionality such as moving, swapping or sorting the column or group. Group headers have the added functionality of serving to aggregate multiple columns under a single heading.</p> 
		/// <p class="body">The <b>Header</b> property provides access to the header that is associated with an object. In some instances, the type of header may be ambiguous, such as when accessing the <b>Header</b> property of a UIElement object. You can use the <b>Type</b> property of the Header object returned by the <b>Header</b> property to determine whether the header belongs to a column or a group.</p>
		/// </remarks>
        public Infragistics.Win.UltraWinGrid.HeaderBase Header
        {
            get
            {
				// SSP 6/11/03 - Optimizations
				// Since the primary context is HeaderBase, return it without calling GetContext which is a bit less
				// efficient.
				//
                //return (Infragistics.Win.UltraWinGrid.HeaderBase)this.GetContext( typeof( Infragistics.Win.UltraWinGrid.HeaderBase ) );
				return this.PrimaryContext as HeaderBase;
            }
        }

		// SSP 5/19/03 - Fixed headers
		// Overrode DrawBorders so we can draw the left edge of the header that's partially 
		// scrolled out of view on the left side.
		//
		/// <summary>
		/// Overridden.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBorders( ref UIElementDrawParams drawParams )
		{
			UltraGridBand band = this.Header.Band;

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/12/05
			//
			//if ( band.UseFixedHeaders )
			if ( band != null && band.UseFixedHeaders )
			{
				Rectangle rect = this.Rect;

				int parentRectX = this.Parent.Rect.X;
				if ( rect.X < parentRectX )
				{
					rect.Width += rect.X - parentRectX;
					rect.X = parentRectX;

                    // MRS NAS v8.1 - Windows Vista Style for UltraWinTree
                    //// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
                    //// ----------------------------------------------------------------------
                    ////drawParams.DrawBorders( this.BorderStyle, this.BorderSides, rect );
                    //if( this.HeaderStyle == HeaderStyle.WindowsXPCommand )
                    //    drawParams.DrawButtonBorders( UIElementButtonStyle.WindowsXPCommandButton, this.HeaderState, this.BorderSides, rect, this.ClipRect );
                    //else
                    //    drawParams.DrawBorders( this.BorderStyle, this.BorderSides, rect );
                    //// ----------------------------------------------------------------------                    
                    switch (this.HeaderStyle)
                    {
                        case HeaderStyle.WindowsXPCommand:                            
                            drawParams.DrawButtonBorders(UIElementButtonStyle.WindowsXPCommandButton, this.HeaderState, this.BorderSides, rect, this.ClipRect);
                            break;
                        case HeaderStyle.Standard:
                        case HeaderStyle.WindowsVista:
                        case HeaderStyle.XPThemed:
                            drawParams.DrawBorders(this.BorderStyle, this.BorderSides, rect);
                            break;
                        case HeaderStyle.Default:
                        default:
                            Debug.Fail("Unknown HeaderStyle");
                            break;
                    }


					return;
				}
			}

			base.DrawBorders( ref drawParams );
		}

		// SSP 6/5/03 - Fixed headers
		// Overrode ClipSelf and Region properties.
		//
		#region ClipSelf

		/// <summary>
		/// Returning true causes all drawing of this element to be expicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipSelf 
		{ 
			get 		  
			{
				// MD 2/20/09 - TFS11568
				// If this is a right-fixed header separator, we want to clip it because it needs to be 3 pixels wide for the Windows XP Luna Blue theme, 
				// but we don't want it to display 3 pixels wide.
				if ( this.isRightFixedHeaderSeparator )
					return true;

				return ! this.clipSelfRect.IsEmpty;
			} 
		}
 
		#endregion // ClipSelf

		#region Region

		/// <summary>
		/// Returns the region of this element. The deafult returns the element's
		/// Rect as a region. This method can be overriden to supply an irregularly
		/// shaped region 
		/// </summary>
		public override System.Drawing.Region Region
		{
			get
			{
				// MD 2/20/09 - TFS11568
				// If this is a right-fixed header separator, we want to clip it because it needs to be 3 pixels wide for the Windows XP Luna Blue theme, 
				// but we don't want it to display 3 pixels wide.
				if ( this.isRightFixedHeaderSeparator )
				{
					Rectangle rect = this.Rect;
					rect.X = rect.Right - 1;
					rect.Width = 1;
					return new Region( rect );
				}

				// The reason why we are creating a new instance of region is because the element's
				// Draw method disposes of this after accessing it.
				//
				return ! this.clipSelfRect.IsEmpty ? new Region( this.clipSelfRect ) : base.Region;
			}
		}
 
		#endregion // Region

		#region IntersectInvalidRect

		// SSP 9/9/03 UWG2640
		// Overrode IntersectInvalidRect because theming logic doesn't honor the Region
		// and ClipSelf properties so in fixed headers non-fixed headers draw over the
		// fixed headers.
		//
		/// <summary>
		/// Overridden. Returns the intersection of the element's rect with the invalid rect for the
		/// current draw operation.
		/// </summary>
		/// <param name="invalidRect">Invalid rect</param>
		/// <returns>The intersection of the element's rect with the invalid rect.</returns>
		protected override Rectangle IntersectInvalidRect( Rectangle invalidRect )
		{
			if ( this.clipSelfRect.IsEmpty )
				return base.IntersectInvalidRect( invalidRect );
			else
				return Rectangle.Intersect( base.IntersectInvalidRect( invalidRect ), this.clipSelfRect );
		}

		#endregion IntersectInvalidRect 

		#region Contains

		// SSP 6/17/03 - Fixed headers
		// Overrode Contains method to take care of cells that are partially scrolled underneath
		// the fixed column cells.
		//
        /// <summary>
        /// Checks if the point is over the element.
        /// </summary>
        /// <param name="point">In client coordinates.</param>
        /// <param name="ignoreClipping">Specifieds if we should ignore clipping or not</param>
        /// <returns>Returns true if the point is over the element.</returns>
        public override bool Contains(System.Drawing.Point point,
			bool ignoreClipping )
		{
			if( ! this.clipSelfRect.IsEmpty && ! this.clipSelfRect.Contains( point ) )
				return false;

			return base.Contains( point, ignoreClipping );
		}

		#endregion // Contains

		#region InternalSetClipSelfRegion
		
		internal void InternalSetClipSelfRegion( Rectangle clipSelfRect )
		{
			this.clipSelfRect = clipSelfRect;
		}

		#endregion // InternalSetClipSelfRegion

		#region InternalGetClipSelfRegion
		
		// SSP 7/16/03 - Fixed headers
		// Added InternalGetClipSelfRegion method.
		//
		internal Rectangle InternalGetClipSelfRegion( )
		{
			return this.clipSelfRect;
		}

		#endregion // InternalGetClipSelfRegion

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
        protected override void PositionChildElements()
        {
			// MD 2/23/09
			// Found while fixing TFS14332
			// This has been moved below so we can get the border style set correctly for right fixed header separators.
			//// MD 2/20/09 - TFS11568
			//// If this is a right-fixed header separator, it doens't display any child elements.
			//if ( this.isRightFixedHeaderSeparator )
			//{
			//    if ( this.childElementsCollection != null )
			//        this.childElementsCollection.Clear();
			//
			//    return;
			//}

			// SSP 6/11/03 - Optimizations
			// Rather than accessing Header property all over the code, store it in a local variable and
			// use that. Calls to Header property have been replaced with header in the code of this method 
			// however these changes have not been commented as it was deemed not to be necessary.
			//
			HeaderBase header = this.Header;

			// SSP 6/5/03 - Cache the border style.
			// Cache the border style.
			//
			// ----------------------------------------------------------------------------------
			BandHeadersUIElement bandHeadersElem = this.parentElement as BandHeadersUIElement;
			if ( null != bandHeadersElem )
				this.borderStyle = bandHeadersElem.headerBorderStyle;
			else 
				this.borderStyle = header.Band.BorderStyleHeaderResolved;
			// ----------------------------------------------------------------------------------

			// SSP 6/11/03 - Fixed Headers
			// Clear the clipSelfRect if not using fixed headers. (Remember in row layout, there 
			// are no fixed headers).
			//
			if ( null == bandHeadersElem || ! bandHeadersElem.usingFixedHeaders || header.Band.UseRowLayoutResolved )
				this.clipSelfRect = Rectangle.Empty;

			// MD 2/23/09
			// Found while fixing TFS14332
			// This has been moved from above so we can get the border style set correctly for right fixed header separators.
			// MD 2/20/09 - TFS11568
			// If this is a right-fixed header separator, it doens't display any child elements.
			if ( this.isRightFixedHeaderSeparator )
			{
				if ( this.childElementsCollection != null )
					this.childElementsCollection.Clear();

				return;
			}

            TextUIElement			textElement				= null;
			SortIndicatorUIElement	sortIndicatorElement	= null;
			SwapButtonUIElement		swapButtonElement		= null;
			ImageUIElement			imageElement			= null;

			// SSP 3/21/02
			// Code for filter drop down element
			// 
			FilterDropDownButtonUIElement filterDropDownElement = null;

			// SSP 6/3/02
			// 
			RowSummariesButtonUIElement	  rowSummariesButtonElement = null;

			// SSP 7/21/03 - Fixed headers
			// Aded fixed header indicator element.
			//
			FixedHeaderIndicatorUIElement fixedHeaderIndicatorElement = null;

            // CDS NAS v9.1 Header CheckBox
            HeaderCheckBoxUIElement checkboxElement = null;

			if ( this.childElementsCollection != null )
			{
				textElement				= (TextUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof(TextUIElement), true );
				sortIndicatorElement	= (SortIndicatorUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof(SortIndicatorUIElement), true );
				swapButtonElement		= (SwapButtonUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof(SwapButtonUIElement), true );				
				imageElement			= (ImageUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof(ImageUIElement), true );

				// SSP 3/21/02
				// Code for filter drop down element
				//
				filterDropDownElement		= (FilterDropDownButtonUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof(FilterDropDownButtonUIElement), true );

				// SSP 7/21/03 - Fixed headers
				//
				fixedHeaderIndicatorElement = (FixedHeaderIndicatorUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof( FixedHeaderIndicatorUIElement ), true );

                // CDS NAS v9.1 Header CheckBox
                checkboxElement = (HeaderCheckBoxUIElement)HeaderUIElement.ExtractExistingElement(this.childElementsCollection, typeof(HeaderCheckBoxUIElement), true);

				this.childElementsCollection.Clear();
			}

			AppearanceData appData = new AppearanceData();
			if ( header != null )
			{
				// SSP 9/9/05 BR06321 - Hot Tracking Header Appearance
				// If hot-tracking appearance has image on it then resolve that.
				// 
				//header.ResolveAppearance( ref appData, AppearancePropFlags.AllRender );
				AppearancePropFlags flags = AppearancePropFlags.AllRender;
				header.ResolveAppearance( ref appData, ref flags, true, this.IsHotTracking );
			}

            // Create the text element
            //
            if ( null == textElement)
            {
                textElement = new TextUIElement( this, string.Empty );

				// SSP 2/1/02 UWG1025
				// Headers can be multiline. So make the text ui element
				// multiline.
				//
				textElement.MultiLine = true;
				// We don't want to wrap the text for headers.
				//
				// JAS v5.2 Wrapped Header Text 4/18/05
				// We need to check with the Band to see if the header's text should be wrapped.
				// This check needs to occur every time this method is called, so it must be outside this block.
				//textElement.WrapText = false;		
            }
			textElement.WrapText = header.Band.WrapHeaderTextResolved;

			Rectangle textRect = this.RectInsideBorders; //textElement.Rect;
			Rectangle swapButtonRect = textRect;
			bool addSortIndicator = false;

			// SSP 7/22/03 - Fixed headers
			//
			// ------------------------------------------------------------------------------
			if ( null != bandHeadersElem && bandHeadersElem.usingFixedHeaders && 
				FixedHeaderIndicator.Button == header.FixedHeaderIndicatorResolved )
			{
				if ( null == fixedHeaderIndicatorElement )
					fixedHeaderIndicatorElement = new FixedHeaderIndicatorUIElement( this );

				Rectangle fixedHeaderIndicatorRect = textRect;
				fixedHeaderIndicatorRect.Width = FixedHeaderIndicatorUIElement.FIXED_HEADER_INDICATOR_BUTTON_WIDTH;
				textRect.Width -= fixedHeaderIndicatorRect.Width;
				fixedHeaderIndicatorRect.X = textRect.Right;
				fixedHeaderIndicatorElement.Rect = fixedHeaderIndicatorRect;

				this.ChildElements.Add( fixedHeaderIndicatorElement );
			}
			// ------------------------------------------------------------------------------
			
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( header.AllowSwapping )
			bool allowSwapping = header.AllowSwapping;

			if ( allowSwapping )
			{								
				// SSP 11/10/03 UWG2309
				// Fixed various things with positioning of header element child elements.
				// SWAP_DROPDOWN_BUTTON_WIDTH is 9 and not 10. Use the constant. This messes
				// up the perform auto-size calculations. 1 pixel is enough to trim a whole
				// character.
				//
				//swapButtonRect.X = textRect.Right - 10;
				swapButtonRect.X = textRect.Right - SwapButtonUIElement.SWAP_DROPDOWN_BUTTON_WIDTH;

				// SSP 3/29/02
				// Use the newly defined constant instead of the literal.
				//
				//swapButtonRect.Width = 9;
				swapButtonRect.Width = SwapButtonUIElement.SWAP_DROPDOWN_BUTTON_WIDTH;
				textRect.Width = swapButtonRect.Left - textRect.Left;
			}
			           
			// SSP 3/21/02
			// Code for filter drop down element
			//
			// SSP 4/16/05 - NAS 5.2 Filter Row
			// Changed the name from AllowRowFiltering to HasFilterIcons.
			//
			//if ( header.AllowRowFiltering )
			if ( header.HasFilterIcons )
			{
				// If it wasn't extracted from the old child elements, then
				// create a new one.
				//
				if ( null == filterDropDownElement )
				{
					filterDropDownElement = new FilterDropDownButtonUIElement( this );
				}

				Rectangle filterDropDownRect = textRect;
				filterDropDownRect.X = textRect.Right - FilterDropDownButtonUIElement.FILTER_DROPDOWN_BUTTON_WIDTH;
				filterDropDownRect.Width = FilterDropDownButtonUIElement.FILTER_DROPDOWN_BUTTON_WIDTH;
				textRect.Width -= FilterDropDownButtonUIElement.FILTER_DROPDOWN_BUTTON_WIDTH;
				filterDropDownElement.Rect = filterDropDownRect;

				this.ChildElements.Add( filterDropDownElement );
			}

			// SSP 3/21/02
			// Code for row summaries
			//
			if ( header.AllowRowSummaries )
			{
				// If it wasn't extracted from the old child elements, then
				// create a new one.
				//
				if ( null == rowSummariesButtonElement )
				{
					rowSummariesButtonElement = new RowSummariesButtonUIElement( this );
				}

				Rectangle summariesButtonRect = textRect;
				summariesButtonRect.X = textRect.Right - RowSummariesButtonUIElement.ROW_SUMMARIES_BUTTON_WIDTH;
				summariesButtonRect.Width = RowSummariesButtonUIElement.ROW_SUMMARIES_BUTTON_WIDTH;
				textRect.Width -= RowSummariesButtonUIElement.ROW_SUMMARIES_BUTTON_WIDTH;
				rowSummariesButtonElement.Rect = summariesButtonRect;

				this.ChildElements.Add( rowSummariesButtonElement );
			}

			// make sure we have a reasonable amount of space for the sort indicator
			//			
			Rectangle sortIndicatorRect = textRect;

			// MD 9/9/08 - TFS6590
			// This isn't a good constant to use anymore, especially in the text is rotated vertically. We may have more than enough room
			// to display the text and sort indicator even in the column is under 40 pixels wide. Instead, if less than 40 pixels is needed
			// to display everything, use a number closer to than min width instead. Otherwise, use the normal 40 pixels.
			//if ( textRect.Width > 40 )
            //
            // MBS 9/15/08 - TFS7463
            // We should be making sure that the column isn't null at this point
            //
            //int minWidth = 
            //    this.Header.Column.CalculateHeaderTextWidth( 1000, true ) + 
            //    Infragistics.Win.UltraWinGrid.SortIndicatorUIElement.SORTIND_WIDTH;
            int minWidth = this.Header.Column == null ? 40 :
                this.Header.Column.CalculateHeaderTextWidth(1000, true) +
                Infragistics.Win.UltraWinGrid.SortIndicatorUIElement.SORTIND_WIDTH;

			if ( textRect.Width > Math.Min( 40, minWidth ) )
			{
				switch( header.SortIndicator )
				{
					case SortIndicator.Ascending:
					case SortIndicator.Descending:
				    {
						addSortIndicator = true;

						// SSP 7/9/02
						// We don't need to do take into account if we have a swap button
						// because we adjust the text rect above appropriately.
						// Commented out the if block
						//
//						if ( this.Header.AllowSwapping )
//						{
//							sortIndicatorRect = swapButtonRect;
//							sortIndicatorRect.X = swapButtonRect.Left - 10;
//							sortIndicatorRect.Width = 9;
//						}
//						else
//						{
							sortIndicatorRect = textRect;
							sortIndicatorRect.X = textRect.Right - 10;
							sortIndicatorRect.Width = 9;
//						}

						textRect.Width = sortIndicatorRect.Left - textRect.Left;
						break;
					}
				}
			}

            // CDS NAS v9.1 Header CheckBox
            UltraGridColumn column = this.Header.Column;

            // CDS 1/19/09 TFS12520 Add the Checkbox UIElement after the TextUIElement to ensure it is clickable.
            bool addCheckBoxUIElement = false;
            // CDS 6/01/09 TFS17746 We shouldn't be showing the checkbox on the column chooser.
            //if (column != null)
            if (this.IsColumnChooserHeader == false &&
                column != null)
            {
                if (column.IsHeaderCheckBoxVisible)
                {
                    //create CheckboxUIElement
                    if (null == checkboxElement)
                        checkboxElement = new HeaderCheckBoxUIElement(this);

                    //set checked state
                    HeaderCheckBoxSynchronization synchBehavior = column.HeaderCheckBoxSynchronizationResolved;
                    HeaderCheckBoxAlignment checkboxAlignment = column.HeaderCheckBoxAlignmentResolved;

                    // CDS 4/18/09 TFS16703
                    RowsCollection rows;

                    switch (synchBehavior)
                    {
                        case HeaderCheckBoxSynchronization.RowsCollection:
                            // CDS 4/18/09 TFS16703
                            //RowsCollection rows = this.GetContext(typeof(RowsCollection)) as RowsCollection;
                            rows = this.GetContext(typeof(RowsCollection)) as RowsCollection;
                            if (rows != null)
                                checkboxElement.CheckState = rows.GetHeaderCheckState(column);
                            break;
                        case HeaderCheckBoxSynchronization.Band:
                            checkboxElement.CheckState = column.Band.GetHeaderCheckState(column);
                            break;
                        case HeaderCheckBoxSynchronization.None:
                            // CDS 4/18/09 TFS16703 Need to still set the checkbox when not synching.
                            rows = this.GetContext(typeof(RowsCollection)) as RowsCollection;
                            if (rows != null)
                            {
                                CheckState state = rows.GetHeaderCheckState(column);
                                if (state != CheckState.Indeterminate)
                                    checkboxElement.CheckState = state;
                            }
                            break;
                        default:
                            break;
                    }

                    Size size = checkboxElement.CheckSize;

                    // shrink the checkbox size if necessary
                    //
                    if (textRect.Height > 2 && size.Height > textRect.Height - 2)
                    {
                        // proportinally shrink the width
                        //
                        size.Width = (int)((double)size.Width * ((double)(textRect.Height - 2) / (double)size.Height));

                        // shrink the height
                        //
                        size.Height = textRect.Height - 2;
                    }

                    // make sure the width isn't too big
                    //
                    if (textRect.Width > 2 && size.Width > textRect.Width - 2)
                        size.Width = textRect.Width - 2;

                    Rectangle checkRect = textRect;
                    checkRect.Width = size.Width;
                    checkRect.Height = size.Height;

                    switch (checkboxAlignment)
                    {
                        case HeaderCheckBoxAlignment.Left:
                            DrawUtility.AdjustHAlign(HAlign.Left, ref checkRect, textRect);
                            DrawUtility.AdjustVAlign(VAlign.Middle, ref checkRect, textRect);
                            break;
                        case HeaderCheckBoxAlignment.Right:
                            DrawUtility.AdjustHAlign(HAlign.Right, ref checkRect, textRect);
                            DrawUtility.AdjustVAlign(VAlign.Middle, ref checkRect, textRect);
                            break;
                        case HeaderCheckBoxAlignment.Center:
                            DrawUtility.AdjustHAlign(HAlign.Center, ref checkRect, textRect);
                            DrawUtility.AdjustVAlign(VAlign.Middle, ref checkRect, textRect);
                            break;
                        case HeaderCheckBoxAlignment.Top:
                            DrawUtility.AdjustHAlign(HAlign.Center, ref checkRect, textRect);
                            DrawUtility.AdjustVAlign(VAlign.Top, ref checkRect, textRect);
                            break;
                        case HeaderCheckBoxAlignment.Bottom:
                            DrawUtility.AdjustHAlign(HAlign.Center, ref checkRect, textRect);
                            DrawUtility.AdjustVAlign(VAlign.Bottom, ref checkRect, textRect);
                            break;
                    }

                    // make sure the check has at least a 1 pixel 
                    // padding on top and bottom
                    //
                    if (checkRect.Top == textRect.Top)
                        checkRect.Y++;
                    else
                        if (checkRect.Bottom == textRect.Bottom)
                            checkRect.Y--;


                    // adjust the text rect appropriately
                    //
                    switch (checkboxAlignment)
                    {
                        case HeaderCheckBoxAlignment.Left:
                            checkRect.X++;
                            textRect.Width -= checkRect.Width + 1;
                            textRect.X += checkRect.Width + 1;
                            break;
                        case HeaderCheckBoxAlignment.Center:
                            break;
                        case HeaderCheckBoxAlignment.Right:
                            checkRect.X--;
                            textRect.Width -= checkRect.Width + 1;
                            break;
                        case HeaderCheckBoxAlignment.Top:
                            checkRect.Y++;
                            textRect.Height -= checkRect.Height + 1;
                            textRect.Y += checkRect.Height + 1;
                            break;
                        case HeaderCheckBoxAlignment.Bottom:
                            checkRect.Y--;
                            textRect.Height -= checkRect.Height + 1;
                            break;
                    }

                    checkboxElement.Rect = checkRect;

                    //add to the ChildElements collection
                    // CDS 1/19/09 TFS12520 Set the addCheckBoxUIElement flag
                    //this.ChildElements.Add(checkboxElement);
                    addCheckBoxUIElement = true;
                }
            }

			// SSP 11/10/03 UWG2309
			// Fixed various things with positioning of header element child elements.
			// Why inflate by -1 ? This causes a problem with perform auto size. Even
			// 1 pixel (here it's actually 2 pixels that we are losing due to the inflate
			// operation) can be enough to trim a whole character.
			// Commented out below Inflate operation. If it turns out that this is necessary
			// then before uncommenting make sure that perform autosize compensates for this.
			//
			//textRect.Inflate(-1,0);

			// JJD 08/17/01 - UWG59
			// Added support for displaying images in headers
			//
			if ( appData.Image != null )
			{
				//	BF 12.3.02	UWG1853
				//
				//	The UltraComboDropDown control does not derive from UltraGridBase,
				//	so separate the attempt to get an ImageList from the call to GetImage
				//
				//Image image = appData.GetImage( ((UltraGridBase)this.Control).ImageList );
				ImageList imageList = null;
				// SSP 8/6/03
				// In row layout designer, this.Control is not an UltraGridBase. It's the row 
				// layout designer control. Instead use get the grid via the Header element
				// context.
				//
				
				imageList = this.Header.Band.Layout.Grid.ImageList;

				Image image = appData.GetImage( imageList );

				if ( image != null )
				{
					if ( null == imageElement )
					{
						imageElement = new ImageUIElement( this, image );
					}
					else
					{
						imageElement.Image = image;
					}

					imageElement.Scaled = true;

					Size size = image.Size;

					// shrink the image size if necessary
					//
					if ( textRect.Height > 2 && size.Height > textRect.Height - 2 )
					{
						// proportinally shrink the width
						//
						size.Width  = (int)((double)size.Width * ( (double)(textRect.Height - 2) / (double)size.Height ));

						// shrink the height
						//
						size.Height = textRect.Height - 2;
					}

					// make sure the width isn't too big
					//
					if ( textRect.Width > 2 && size.Width > textRect.Width - 2 )
						size.Width = textRect.Width - 2;

					Rectangle imgRect	= textRect;
					imgRect.Width		= size.Width;
					imgRect.Height		= size.Height;

					//move image based on alignment
					DrawUtility.AdjustHAlign( appData.ImageHAlign, ref imgRect, textRect ); 
					DrawUtility.AdjustVAlign( appData.ImageVAlign, ref imgRect, textRect );

					// make sure the imgae has at least a 1 pixel 
					// padding on top and bottom
					//
					if ( imgRect.Top == textRect.Top )
						imgRect.Y++;
					else
					if ( imgRect.Bottom == textRect.Bottom )
						imgRect.Y--;


					// adjust the text rect appropriately
					//
					switch ( appData.ImageHAlign )
					{
						case HAlign.Center:
							break;

						case HAlign.Right:
							imgRect.X--;
							textRect.Width -= imgRect.Width + 1;
							break;

						default:
						case HAlign.Left:
							imgRect.X++;
							textRect.Width	-= imgRect.Width + 1;
							textRect.X		+= imgRect.Width + 1;
							break;
					}

					imageElement.Rect = imgRect;

					this.ChildElements.Add( imageElement );

				}
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			// Removed headerLines and maxLines locals and realized the rest of this code doesn't really do anything
			#region Not Used

			//// Width of rcText is now final, so calculate top/bottom
			////
			//int headerLines = header.IsGroup ? header.Band.GroupHeaderLines:
			//    header.Band.ColHeaderLines;

			//Rectangle adjTextRect = textRect;

			//if ( header != null )
			//{

			//    //Pass in the number of ColHeaderLines.
			//    //

			//    int maxLines = 0;

			//    if ( header.Band != null && header.Band.ColHeaderLines > 0 )
			//        maxLines = header.Band.ColHeaderLines;

			//    //CalculateTextSpace ( appData, m_pPositionItem->GetHeader()->GetCaption(), rcAdjText, lMaxLines );

			//    adjTextRect.Intersect( textRect );
			//}

			//int height = textRect.Height;
			//int textHeight = adjTextRect.Height;

			//switch ( appData.TextVAlign )
			//{

			//    case Infragistics.Win.VAlign.Bottom:
			//{
			//        textRect.Height = textHeight;
			//        textRect.Y = textRect.Bottom - textHeight;
			//        break;
			//    }
			//    case Infragistics.Win.VAlign.Top:
			//{
			//        textRect.Height = textHeight;
			//        break;
			//    }

			//    default:
			//    case Infragistics.Win.VAlign.Middle:
			//{
			//        textRect.Y += ( height - textHeight ) / 2;
			//        textRect.Height = (textRect.Top + textHeight) - textRect.Y;
			//        break;
			//    }
			//}

			#endregion Not Used

			// Now that top/bottom of text rect is finalized, 
			// adjust top/bottom of rcSortIndicator and rcSwapButton
			//
			sortIndicatorRect.Y    = swapButtonRect.Y    = textRect.Top;
			sortIndicatorRect.Height = swapButtonRect.Height = textRect.Height;

			
			if ( addSortIndicator )
			{
				//-1 since sortindicator was clipping the bottom of the header's border
				//when it was printing
				//
				--sortIndicatorRect.Height;

				if ( sortIndicatorElement == null )
					sortIndicatorElement = new SortIndicatorUIElement(this, header.SortIndicator == SortIndicator.Ascending);
				else
					sortIndicatorElement.InitAscending( header.SortIndicator == SortIndicator.Ascending );
 
				sortIndicatorElement.Rect = sortIndicatorRect;

				this.ChildElements.Add( sortIndicatorElement );
			}
			

			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( header.AllowSwapping )
			if ( allowSwapping )
			{

				if ( null == swapButtonElement )				
					swapButtonElement = new SwapButtonUIElement( this );				
				
				swapButtonElement.Rect = swapButtonRect;
				
				this.ChildElements.Add( swapButtonElement );
			}


			textElement.Rect = textRect; // this.RectInsideBorders;
			textElement.Text = header.Caption;

			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Set the TextOrientation for the header on the text element
			textElement.TextOrientation = header.TextOrientationResolved;

			//ROBA 8/3/01 UWG85 needed to use appData's alignment
			//textElement.TextAlignment = ContentAlignment.MiddleCenter;
			
			// JJD 11/07/01
			// TextAlignment property has been removed
			//
			//textElement.TextAlignment = Win.AppearanceData.ContentAlignmentFromHVAlign( appData.TextHAlign, appData.TextVAlign );
			this.ChildElements.Add( textElement );

            // CDS 1/19/08 TFS12520 Add the CheckBoxUIElement here, so that it is on top of the TextUIElement when centered.
            if (addCheckBoxUIElement)
                this.ChildElements.Add(checkboxElement);

			// SSP 7/28/05 - Header, Row, Summary Tooltips
			// 
			// SSP 3/3/06 BR10430
			// Don't show the tooltip if the TipStyleHeader is set to Hide.
			// 
			// --------------------------------------------------------------------------
			//GridUtils.SetupElementToolTip( this, header.ToolTipText, true );
			if ( TipStyle.Show == header.TipStyleHeaderResolved )
				GridUtils.SetupElementToolTip( this, header.ToolTipText, true );
			else
				this.ToolTipItem = null;
			// --------------------------------------------------------------------------
        }

		//RobA UWG609 11/7/01 
		/// <summary>
		/// Gets whether this element is enabled.
		/// </summary>
		public override bool Enabled
		{
			get
			{

				// RobA UWG744 11/19/01 
				// We do not want to walk up to the parent
				//
				

				// SSP 7/22/02 UWG1390
				// Unlike what the comment above says, we do need to look at the parent's
				// enabled property because if the whole grid is disabled, Header.Enabled
				// will still return true and this ui element will look enabled.
				//
				// --------------------------------------------------------------------
				//return this.Header.Enabled;
				if ( !this.Header.Enabled )
					return false;

				return base.Enabled;
				// --------------------------------------------------------------------
			}			
		}

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
                                                 ref AppearancePropFlags requestedProps )
		{
			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Honor the colors set on the header appearances.
			// 
			HeaderBase header = this.Header;
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// 
            //header.ResolveAppearance( ref appearance, ref requestedProps, false );
			bool hotTrackingHeader = this.IsHotTracking;
			header.ResolveAppearance( ref appearance, ref requestedProps, false, hotTrackingHeader );

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
			// The HeaderUIElementBase needs to get the first opportunity to 
			// set the appearance values.
			//
			base.InitAppearance( ref appearance, ref requestedProps );

			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Honor the colors set on the header appearances.
			// 
			//this.Header.ResolveAppearance( ref appearance, requestedProps );
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// 
			//header.ResolveAppearance( ref appearance, ref requestedProps, true );
			header.ResolveAppearance( ref appearance, ref requestedProps, true, hotTrackingHeader );
		}

		#region InternalInitAppearance

		// SSP 8/12/05 BR05338
		// 
		internal void InternalInitAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			this.InitAppearance( ref appData, ref flags );
		}

		#endregion // InternalInitAppearance

		#region IsHotTracking
		
		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal bool IsHotTracking
		{
			get
			{
				return GridUtils.IsHotTracking( this, this.isHotTracking );
			}
		}

		#endregion // IsHotTracking

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 6/5/03 - Optimizations
				// Use the cached border style.
				//
				//return this.Header.Band.BorderStyleHeaderResolved;
				return this.borderStyle;
			}
		}

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return Border3DSide.All;
			}
		}

        /// <summary>
        /// Returns the cursor to use over the adjustable area of the element.
        /// </summary>
        /// <param name="point">The point that should be used to determine if the area is adjustable.</param>
        /// <returns>The cursor that should be used to represent an adjustable region.</returns>
        public override System.Windows.Forms.Cursor GetAdjustableCursor(System.Drawing.Point point)
		{
			// MRS 4/1/05 - Added a check for Span Resizing. 
			if (GridUtils.IsControlKeyDown &&
				this.SupportsSpanSizing)
			{				
				if (this.SupportsLeftRightSpanSizingFromPoint( point ) )
				{
					this.wasShowingSpanCursor = true;
					return this.SpanResizeCursorHoriz;
				}
				else if (this.SupportsUpDownSpanSizingFromPoint( point ))
				{
					this.wasShowingSpanCursor = true;
					return this.SpanResizeCursorVert;
				}
			}
			this.wasShowingSpanCursor = false;

			// SSP 3/17/03 - Row Layout Functionality
			//
			//if ( this.SupportsLeftRightAdjustmentsFromPoint( point ) )
			if ( this.SupportsLeftRightAdjustmentsFromPoint( point ) 
				&& ! this.SupportsUpDownAdjustmentsFromPoint( point ) )
			{
				// JJD 1/16/02 - UWG880
				// Added special cursor for autosizing columns
				//
				if ( this.SupportsAutoSizing )
				{
					System.Windows.Forms.Cursor cursor = this.Header.Band.Layout.AutoSizeCursor;

					if ( cursor != null )
						return cursor;
				}

				return System.Windows.Forms.Cursors.SizeWE;
			}

			// SSP 3/17/03 - Row Layout Functionality
			// Instead of returning a null cursor, call the base class' implementation
			// and return that since we do allow resizing vertically now.
			//
			//return null;
			return base.GetAdjustableCursor( point );
		}

		/// <summary>
		/// CUIElementHeader:IsMoveable
		/// Returns true header can be resized
		/// </summary>
		/// <returns></returns>
		public override bool Adjustable
		{
			get
			{
				// SSP 3/14/03 - Row Layout Functionality
				// Added below if block.
				//
				// MD 8/3/07 - 7.3 Performance
				// Refactored - Prevent calling expensive getters multiple times
				#region Refactored

				//if ( null != this.Header && null != this.Header.Column )
				//{
				//    if ( this.Header.Column.Band.UseRowLayoutResolved )
				//        // SSP 6/26/03 UWG2387
				//        // Use the AllowLabelSizingResolved instead of AllowCellSizingResolved property since this is a header.
				//        //
				//        //return RowLayoutSizing.None != this.Header.Column.RowLayoutColumnInfo.AllowCellSizingResolved;
				//        return RowLayoutSizing.None != this.Header.Column.RowLayoutColumnInfo.AllowLabelSizingResolved;
				//    // If the card-view is turned on without the row-layout mode, then return false since
				//    // in regular card-view mode, the whole card area is resized at once.
				//    //
				//    else if ( this.Header.Column.Band.CardView )
				//        return false;
				//}
				//
				//return null != this.Header && this.Header.AllowSizing
				//    // SSP 11/16/04
				//    // Implemented column sizing using cells in non-rowlayout mode.
				//    // Added ColumnSizingArea property on the override.
				//    //
				//    && ( null == this.Header.Column
				//    || ColumnSizingArea.HeadersOnly == this.Header.Band.ColumnSizingAreaResolved
				//    || ColumnSizingArea.EntireColumn == this.Header.Band.ColumnSizingAreaResolved );

				#endregion Refactored
				if ( null != this.Header )
				{
					UltraGridColumn column = this.Header.Column;

                    if (null != column)
                    {
                        if (column.Band.UseRowLayoutResolved)
                        {
                            // SSP 6/26/03 UWG2387
                            // Use the AllowLabelSizingResolved instead of AllowCellSizingResolved property since this is a header.
                            //
                            //return RowLayoutSizing.None != this.Header.Column.RowLayoutColumnInfo.AllowCellSizingResolved;
                            return RowLayoutSizing.None != column.RowLayoutColumnInfo.AllowLabelSizingResolved;
                        }
                        // If the card-view is turned on without the row-layout mode, then return false since
                        // in regular card-view mode, the whole card area is resized at once.
                        //
                        else if (column.Band.CardView)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        // MRS - NAS 9.1 - Groups in RowLayout
                        // Added this 'else' block. If the column is null, we may want to allow
                        // sizing of the group header.      
                        if (this.Header is GroupHeader)
                        {
                            UltraGridGroup group = this.Header.Group;
                            if (group != null)
                            {
                                if (group.Band.UseRowLayoutResolved)
                                {
                                    return RowLayoutSizing.None != group.RowLayoutGroupInfo.AllowLabelSizingResolved;
                                }
                            }
                        }
                    }

					if ( this.Header.AllowSizing )
					{
						if ( column == null )
							return true;

						ColumnSizingArea columnSizingAreaResolved = this.Header.Band.ColumnSizingAreaResolved;

						if ( ColumnSizingArea.HeadersOnly == columnSizingAreaResolved || 
							ColumnSizingArea.EntireColumn == columnSizingAreaResolved )
						{
							return true;
						}
					}
				}

				return false;
			}
		}

        /// <summary>
        /// Called after a move/resize operation. 
        /// </summary>
        /// <param name="delta">The delta</param>
        public override void ApplyAdjustment(Point delta)
		{
			if ( null == this.Header )
				return;

			HeaderBase header = this.Header;

			// MD 2/18/09 - TFS14163
			// We shouldn't apply an adjustment to any header which has recently been hidden.
			if ( header.Column != null )
			{
				if ( header.Column.HiddenResolved )
				{
					Debug.Fail( "Cannot apply an adjustment to hidden columns." );
					return;
				}
			}
			else if ( header.Group != null )
			{
				if ( header.Group.HiddenResolved )
				{
					Debug.Fail( "Cannot apply an adjustment to hidden groups." );
					return;
				}
			}

			// If this is a column in a group that is the last column in
			// its level within the group then adjust the group width
			// instead of the column width
			//
			// MRS 12/9/04 - BR00532
			// This if block is unneccessary. It makes it impossible to size the last
			// cell in the column. And also, it is inconsistent with the way sizing
			// works on the cell (when ColumnSizingArea = EntireColumn).
//			if ( !header.IsGroup )
//			{
//				UltraGridColumn column = this.Header.Column;
//
//				if ( null != column.Group && column.LastItemInLevel )
//				{
//					header = column.Group.Header;
//				}
//			}


			// get the layout depending on if group or column
			
			// JJD 10/03/01
			// Always use band to get thelayout since it is always
			// there
			//
			UltraGridLayout layout = this.Header.Band.Layout;

			// when resizing column/group, if we are in edit mode, then exit edit mode
			if ( layout.ActiveCell != null && layout.ActiveCell.IsInEditMode )
			{
				layout.ActiveCell.ExitEditMode( );	
			}

			int currentWidth = header.Extent;

			// MD 2/23/09 - TFS14332
			// The right fixed headers will continue to resize from the right side.
			//// MD 2/12/09 - TFS13812
			//// Right fixed headers resize from the left size, not the right side.
			////currentWidth += delta.X;
			//if ( this.IsRightFixedHeader )
			//    currentWidth -= delta.X;
			//else
			//    currentWidth += delta.X;
			currentWidth += delta.X;

			// SSP 2/26/03 - Row Layout Functionality
			// Added code for resizing headers in column layout situation.
			// Added the if block and enclosed the already existing code in the else block.
			//
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( header is ColumnHeader && header.Column.Band.UseRowLayoutResolved )
            // MRS - NAS 9.1 - Groups in RowLayout
            //UltraGridColumn headerColumn = header is ColumnHeader 
            //    ? header.Column 
            //    : null;
            UltraGridBand band = this.Header.Band;

            // MBS 10/18/07 - BR27509
            // We need to check for null here
            //
            //if ( headerColumn.Band.UseRowLayoutResolved )
            // MRS - NAS 9.1 - Groups in RowLayout
            //if (headerColumn != null && headerColumn.Band.UseRowLayoutResolved)
            if (band != null && band.UseRowLayoutResolved)
			{
				// MRS 4/4/05 - Span-Resizing for row layouts
				// MRS 4/11/05
				//if (this.Grid.gridBagLayoutDragStrategy != null)
				if (null != this.Grid &&
					this.Grid.gridBagLayoutDragStrategy != null)
				{	
					Point pointInControlCoords = this.Grid.PointToClient( Control.MousePosition );

                    // MRS - NAS 9.1 - Groups in RowLayout
                    IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = this.Header.RowLayoutColumnInfoProvider;
                    Infragistics.Win.Layout.ILayoutItem resizeItem = iProvideRowLayoutColumnInfo.ResizeLayoutItem;
                        
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times			
					//this.Grid.DragSpanEnd(pointInControlCoords, this, this.Header, this.Header.Column);
                    // MRS - NAS 9.1 - Groups in RowLayout
					//this.Grid.DragSpanEnd( pointInControlCoords, this, header, headerColumn );
                    this.Grid.DragSpanEnd(pointInControlCoords, this, header, resizeItem);

					return;
				}

				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//UltraGridBand band = header.Column.Band;
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridBand band = headerColumn.Band;

				Size containerSize = this.GetLayoutContainerSize( );

                // MRS 2/24/2009 - TFS14427
				//if ( band.AreColumnHeadersInSeparateLayoutArea )
                UIElement layoutContainerElement = GridBagLayoutDragManager.GetContainerElement(this);
                if (layoutContainerElement is BandHeadersUIElement ||
                    layoutContainerElement is CardLabelAreaUIElement)
				{
					Point horizDelta = new Point( delta.X, 0 );
					Point vertDelta = new Point( 0, delta.Y );

					if ( ! band.CardView )
					{
                        // MRS - NAS 9.1 - Groups in RowLayout
                        IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = this.Header.RowLayoutColumnInfoProvider;
                        Infragistics.Win.Layout.ILayoutItem resizeItem = iProvideRowLayoutColumnInfo.ResizeLayoutItem;

                        
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//band.ResizeLayoutItem( header.Column, new Size( containerSize.Width, 0 ), horizDelta );
                        // MRS - NAS 9.1 - Groups in RowLayout
						//band.ResizeLayoutItem( headerColumn, new Size( containerSize.Width, 0 ), horizDelta );
                        band.ResizeLayoutItem(resizeItem, new Size(containerSize.Width, 0), horizDelta);

                        // MRS 3/4/2009 - TFS14870
                        // If both the width and the height are being changed, we need to verify the layout
                        // in between. 
                        //
                        if (delta.X != 0 && delta.Y != 0)
                            band.VerifyRowLayoutCache();

						band.ResizeLayoutItem( header, containerSize, vertDelta );
					}
					else
					{
						band.ResizeLayoutItem( header, containerSize, horizDelta );

                        // MRS 3/4/2009 - TFS14870
                        // If both the width and the height are being changed, we need to verify the layout
                        // in between. 
                        //
                        if (delta.X != 0 && delta.Y != 0)
                            band.VerifyRowLayoutCache();

                        // MRS - NAS 9.1 - Groups in RowLayout
                        IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = this.Header.RowLayoutColumnInfoProvider;
                        Infragistics.Win.Layout.ILayoutItem resizeItem = iProvideRowLayoutColumnInfo.ResizeLayoutItem;

						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//band.ResizeLayoutItem( header.Column, new Size( 0, containerSize.Height ), vertDelta );
                        // MRS - NAS 9.1 - Groups in RowLayout
						//band.ResizeLayoutItem( headerColumn, new Size( 0, containerSize.Height ), vertDelta );
                        band.ResizeLayoutItem(resizeItem, new Size(0, containerSize.Height), vertDelta);
					}
				}
				else 
				{
					// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
					// Pass along the new elem parameter if the header is inside a row.
					// 
					//band.ResizeLayoutItem( this.Header, containerSize, delta );
					band.ResizeLayoutItem( this, this.Header, containerSize, delta );
				}
			}
			else
			{
				// SSP 11/16/04
				// Implemented column sizing using cells in non-rowlayout mode. Moved the code from
				// here into the new ApplyAdjustmentNonRowLayoutMode static method so the CellUIElement
				// can make use of it.
				//
				HeaderUIElement.ApplyAdjustmentNonRowLayoutMode( header, currentWidth );
			}
		}

		// SSP 11/16/04
		// Implemented column sizing using cells in non-rowlayout mode. Added 
		// ApplyAdjustmentNonRowLayoutMode method. Moved the code from ApplyAdjustment method
		// above into here so CellUIElement can make use of it also.
		//
		internal static void ApplyAdjustmentNonRowLayoutMode( HeaderBase header, int newWidth )
		{
			// SSP 12/8/03 UWG2769
			// When the user resizes a column in auto-fit columns mode, we do not want to apply
			// the extra delta width to the column that's being resized in Band.FitColumnsToWidth
			// method. Added this internal flag which is temporarily set to accomplish this.
			//
			// ------------------------------------------------------------------------------
			//header.InternalSetWidth( currentWidth, false );
			header.proportionalResizeOverride = DefaultableBoolean.False;
			try
			{
				header.InternalSetWidth( newWidth, false );

				// Initialize the metric which will cause the auto-fit columns logic to get 
				// executed.
				//
				if ( null != header.Band )
					header.Band.Layout.ColScrollRegions.InitializeMetrics( );
			}
			finally
			{
				// Reset the override flag to Default.
				//
				header.proportionalResizeOverride = DefaultableBoolean.Default;

				// Now dirty the metrics because if this header was the only header then we
				// really do want to make sure that the header auto-fits the data area. This
				// is also necessary for other cases as well.
				//
				if ( null != header.Band )
					header.Band.Layout.ColScrollRegions.DirtyMetrics( );
			}
			// ------------------------------------------------------------------------------
		}

		/// <summary>
		/// Returns the range limits for adjusting the element in either or both
		/// dimensions. It also returns the initial rects for the vertical and horizontal
		/// bars that will need to be inverted during the mouse drag operation.
		/// </summary>
		/// <param name="point">point</param>
		/// <param name="range">Returned limits</param>
		public override void GetAdjustmentRange ( System.Drawing.Point point,
			out UIElementAdjustmentRangeParams range )
		{            
			base.GetAdjustmentRange( point, out range );

			int minWidth = 0;
			int maxWidth = 0;

			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.Header is ColumnHeader && null != this.Header.Column 
			//    && this.Header.Column.Band.UseRowLayoutResolved )
			//{
			//    UltraGridBand band = this.Header.Column.Band;
            // MRS - NAS 9.1 - Groups in RowLayout
            //UltraGridColumn headerColumn = this.Header is ColumnHeader 
            //    ? this.Header.Column 
            //    : null;
            UltraGridBand band = this.Header.Band;

            // MRS - NAS 9.1 - Groups in RowLayout
			//if ( null != headerColumn && headerColumn.Band.UseRowLayoutResolved )
            if (null != band && band.UseRowLayoutResolved)
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridBand band = headerColumn.Band;

				Rectangle rect = this.Rect;

				// In row-layout mode in regular row view where the headers are at top,
				// if this item is the left most item in the header area, then it will be offset
				// by the row selector width. So adjust th rect for that.
				//
				this.AdjustForFirstItem( ref rect );

				UIElement rcrElem = this.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );
				UIElement dataAreaElem = this.GetAncestor( typeof( DataAreaUIElement ) );
				Rectangle mainElemRect = this.ControlElement.Rect;
				Rectangle dataRect = Rectangle.Intersect( mainElemRect, null != dataAreaElem ? dataAreaElem.RectInsideBorders : mainElemRect );
				// SSP 6/26/03 UWG2377
				// Don't limit the resize range to the scrolling region. Instead limit it to the desk top rect
				// like we do in the non-layout mode.
				//
				// SSP 10/26/04 UWG3729
				// Uncommented the following line out. The line following it where we define and set
				// the desktopRect was already there. We need limit the row resizing to the row col
				// region buttom but not the column because if you resized a row to be bigger than 
				// the region then you would never be able to resize it back to a smaller size.
				//
				Rectangle rowColRegionRect = null != rcrElem ? rcrElem.Rect : this.Control.ClientRectangle;

				// MD 2/5/09
				// Found while fixing TFS13341
				// The rect passed into GetDeskTopWorkArea is supposed to be in screen coordinates.
				//Rectangle desktopRect = Infragistics.Win.UIElement.GetDeskTopWorkArea( new Rectangle( rect.Right - 1, rect.Top + rect.Width / 2, 1, 1 ) );
				Rectangle tmpRect = new Rectangle( rect.Right - 1, rect.Top + rect.Width / 2, 1, 1 );
				tmpRect = this.Control.RectangleToScreen( tmpRect );
				Rectangle desktopRect = Infragistics.Win.UIElement.GetDeskTopWorkArea( tmpRect );

				int minHeight = 0;
				int maxHeight = 0;
				int tmp1 = 0, tmp2 = 0;
				Size containerSize = this.GetLayoutContainerSize( );

				if ( this.SupportsLeftRightAdjustmentsFromPoint( point ) )
				{
					if ( band.AreColumnHeadersInSeparateLayoutArea && ! band.CardView )
					{
                        // MRS - NAS 9.1 - Groups in RowLayout
                        IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = this.Header.RowLayoutColumnInfoProvider;
                        Infragistics.Win.Layout.ILayoutItem resizeItem = iProvideRowLayoutColumnInfo.HeaderLayoutItem;
                        
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//band.GetLayoutItemResizeRange( this.Header.Column, new Size( containerSize.Width, 0 ), ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
                        // MRS - NAS 9.1 - Groups in RowLayout
						//band.GetLayoutItemResizeRange( headerColumn, new Size( containerSize.Width, 0 ), ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
                        band.GetLayoutItemResizeRange(resizeItem, new Size(containerSize.Width, 0), ref minWidth, ref maxWidth, ref tmp1, ref tmp2);
					}
					else 
					{
						// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
						// 
						//band.GetLayoutItemResizeRange( this.Header, containerSize, ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
						band.GetLayoutItemResizeRange( this, this.Header, containerSize, ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
					}

					range.maxDeltaLeft = Math.Min( 0, rect.Left + minWidth - rect.Right );
						
					// SSP 6/26/03 UWG2377
					// Don't limit the resize range to the scrolling region. Instead limit it to the 
					// desk top rect like we do in the non-layout mode.
					//
					// ------------------------------------------------------------------------------
					
					if ( maxWidth > 0 )
						// SSP 10/26/04 UWG3729
						//
						//range.maxDeltaRight = Math.Max( 0, Math.Max( maxWidth - rect.Width, desktopRect.Right - rect.Right ) );
						range.maxDeltaRight = Math.Max( 0, Math.Min( maxWidth - rect.Width, desktopRect.Right - rect.Right ) );
					else 
						range.maxDeltaRight = Math.Max( 0, desktopRect.Right - rect.Right );
					// ------------------------------------------------------------------------------

					range.leftRightAdjustmentBar.Y = dataRect.Top;
					range.leftRightAdjustmentBar.Height = dataRect.Bottom - dataRect.Top;
				}

				if ( this.SupportsUpDownAdjustmentsFromPoint( point ) )
				{
					if ( band.AreColumnHeadersInSeparateLayoutArea && band.CardView )
					{
                        // MRS - NAS 9.1 - Groups in RowLayout
                        IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = this.Header.RowLayoutColumnInfoProvider;
                        Infragistics.Win.Layout.ILayoutItem resizeItem = iProvideRowLayoutColumnInfo.HeaderLayoutItem;

						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//band.GetLayoutItemResizeRange( this.Header.Column, new Size( 0, containerSize.Height ), ref tmp1, ref tmp2, ref minHeight, ref maxHeight );
                        band.GetLayoutItemResizeRange( resizeItem, new Size(0, containerSize.Height), ref tmp1, ref tmp2, ref minHeight, ref maxHeight);
					}
					else 
					{
						// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
						// 
						//band.GetLayoutItemResizeRange( this.Header, containerSize, ref tmp1, ref tmp2, ref minHeight, ref maxHeight );
						band.GetLayoutItemResizeRange( this, this.Header, containerSize, ref tmp1, ref tmp2, ref minHeight, ref maxHeight );
					}

					// MRS 5/19/05 - BR03795
					// Don't do this if this is a span resize
					//range.maxDeltaUp = Math.Min( 0, rect.Top + minHeight - rect.Bottom );
					if (this.Grid.gridBagLayoutDragStrategy == null)
						range.maxDeltaUp = Math.Min( 0, rect.Top + minHeight - rect.Bottom );
						
					// SSP 6/26/03 UWG2377
					// Don't limit the resize range to the scrolling region. Instead limit it to the 
					// desk top rect like we do in the non-layout mode.
					//
					// ------------------------------------------------------------------------------
					
					// SSP 10/26/04 UWG3729
					// We need limit the row resizing to the row col region buttom but not the 
					// column because if you resized a row to be bigger than  the region then 
					// you would never be able to resize it back to a smaller size.
					//
					
					if ( maxHeight > 0 )
						range.maxDeltaDown = Math.Max( 0, Math.Min( maxHeight - rect.Height, rowColRegionRect.Bottom - rect.Bottom ) );
					else
						range.maxDeltaDown = Math.Max( 0, rowColRegionRect.Bottom - rect.Bottom );
					// ------------------------------------------------------------------------------

					range.upDownAdjustmentBar.X = dataRect.Left;
					range.upDownAdjustmentBar.Width = dataRect.Right - dataRect.Left;
				}
					
				return;
			}

			if ( !this.SupportsLeftRightAdjustmentsFromPoint( point ) )
				return;						

			Rectangle dataAreaRect      = this.GetAncestor( typeof ( DataAreaUIElement ) ).Rect;
			Rectangle metaRegionRect    = this.GetAncestor( typeof( RowColRegionIntersectionUIElement ) ).Rect;

			// get the position item's min and max width
			//			
			this.Header.GetResizeRange( ref minWidth, ref maxWidth );
    
			if ( this.Rect.Width > minWidth )
			{
				range.maxDeltaLeft = - System.Math.Min( this.Rect.Right - metaRegionRect.Left,
					this.Rect.Width - minWidth );
			}
			else
			{
				range.maxDeltaLeft = 0;
			}

			if ( maxWidth > 0 )
			{
				if ( metaRegionRect.Right > this.Rect.Right )
				{
					// includes the column AND the row selector, so must strip off rowselector
					// width when determining nMaxRight
					int pos = -1;
					int selectorWidth = 0;					
					pos = (int)this.Header.VisiblePosition;
            
					//if the visible position is zero and grid has a row selector, get row selector's width
					if( 0 == pos && this.Header.Band.HasRowSelectors )
						selectorWidth = this.Header.Band.RowSelectorExtent;

					//subtract selector width from header rect to get an accurate nMaxRight
					range.maxDeltaRight = metaRegionRect.Right - this.Rect.Right;
					if ( maxWidth > 0 )
						range.maxDeltaRight = System.Math.Min( metaRegionRect.Right -  this.Rect.Right, 
							maxWidth - (this.Rect.Width - selectorWidth ));
				}
				else
				{
					range.maxDeltaRight = 0;
				}
			}
			else
			{
				// If a max width was not specified than allow the header to be
				// resized as large as the desktop will allow
				//
				Rectangle desktopRect;
				Rectangle currentRect  = new Rectangle(this.Rect.Right - 1, 
					this.Rect.Top - 1, 0, 1 );

				// MD 2/5/09 - TFS13341
				// The rect passed into GetDeskTopWorkArea is supposed to be in screen coordinates.
				currentRect = this.Control.RectangleToScreen( currentRect );

				desktopRect = Infragistics.Win.UIElement.GetDeskTopWorkArea( currentRect );
				
                // MBS 5/20/09 - TFS17839
                // Should also be looking at the screen rect when comparing it to the desktop
                // rect that has been converted to screen coordinates
                //
				//range.maxDeltaRight  = desktopRect.Right - this.Rect.Right;
                range.maxDeltaRight = desktopRect.Right - currentRect.Right;
			}


			// set up the vert bar from bottom to top of the meta region
			//			
			range.leftRightAdjustmentBar.Y      = dataAreaRect.Top;
			range.leftRightAdjustmentBar.Height = dataAreaRect.Height;
		}


		/// <summary>
		/// Returns true if the element can be moved or resized horizontally
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
		public override bool SupportsLeftRightAdjustmentsFromPoint( System.Drawing.Point point )
		{ 
			if ( !base.SupportsLeftRightAdjustmentsFromPoint( point ) )
				return false;

			// if the positionitem doesn't allow sizing return false
			//
			if ( !this.Adjustable )
				return false;

			// MD 2/12/09 - TFS13812
			// This check is done in the base SupportsLeftRightAdjustmentsFromPoint method and the logic is incorrect here anyway.
			// It wasn't doing any harm, but now it doesn't allow resizing from the left side for right-fixed headers, so I have 
			// removed it.
			//Rectangle workRect = this.Rect;
			//
			//// adjust the left and right of the rect to allow a 
			//// narrow area at the right edge of the header to
			//// be used for resizing
			////
			//workRect.X  = workRect.Right - 2;
			//workRect.Width++;
			//
			//if ( ! workRect.Contains( point ) )
			//	return false;

			//if min width and max width of the header are the same then return false
			//
			int minWidth = 0;
			int maxWidth = 0;
			
			// SSP 3/14/03 - Row Layout Functionality
			// Added below if block.
			//
			// ------------------------------------------------------------------------------------------
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.Header is ColumnHeader && null != this.Header.Column && this.Header.Column.Band.UseRowLayoutResolved )
			UltraGridColumn headerColumn = this.Header is ColumnHeader 
				? this.Header.Column 
				: null;

			if ( null != headerColumn && headerColumn.Band.UseRowLayoutResolved )
			{
				// In row-layout designer, if the control key is down then don't resize the header.
				// The row-layout designer has special logic for resizing the item span with control
				// key down.
				//
				if ( this.RowLayoutDesignerElement && Keys.Control == Control.ModifierKeys )
					return false;

				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//UltraGridBand band = this.Header.Column.Band;
				//
				//if ( RowLayoutSizing.Horizontal != this.Header.Column.RowLayoutColumnInfo.AllowLabelSizingResolved &&
				//    RowLayoutSizing.Both != this.Header.Column.RowLayoutColumnInfo.AllowLabelSizingResolved )
				RowLayoutSizing allowLabelSizingResolved = headerColumn.RowLayoutColumnInfo.AllowLabelSizingResolved;

				if ( RowLayoutSizing.Horizontal != allowLabelSizingResolved &&
					RowLayoutSizing.Both != allowLabelSizingResolved )
					return false;

				UltraGridBand band = headerColumn.Band;

				int tmp1 = 0, tmp2 = 0;
				Size containerSize = this.GetLayoutContainerSize( );

				if ( band.AreColumnHeadersInSeparateLayoutArea && ! band.CardView )
				{
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//band.GetLayoutItemResizeRange( this.Header.Column, new Size( containerSize.Width, 0 ), ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
					band.GetLayoutItemResizeRange( headerColumn, new Size( containerSize.Width, 0 ), ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
				}
				else 
				{
					// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
					// 
					//band.GetLayoutItemResizeRange( this.Header, containerSize, ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
					band.GetLayoutItemResizeRange( this, this.Header, containerSize, ref minWidth, ref maxWidth, ref tmp1, ref tmp2 );
				}

				if ( 0 != maxWidth && maxWidth == minWidth )
					return false;

				return true;
			}
			// ------------------------------------------------------------------------------------------

			this.Header.GetResizeRange(ref minWidth, ref maxWidth);
			if ( minWidth == maxWidth && this.Rect.Width <= minWidth)
				return false;

			return true;
		}

		// SSP 3/14/03 - Row Layout Functionality
		// Allow resizing vertically as well in row-layout mode.
		//
		/// <summary>
		/// Returns true if the element can be moved or resized vertically
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
		public override bool SupportsUpDownAdjustmentsFromPoint( System.Drawing.Point point )
		{
			// MD 8/3/07 - 7.3 Performance
			// Refactored - Prevent calling expensive getters multiple times
			//if ( !( this.Header is ColumnHeader ) || null == this.Header.Column || ! this.Header.Column.Band.UseRowLayoutResolved )
			//    return false;
            // MRS - NAS 9.1 - Groups in RowLayout
            #region NAS 9.1 - Groups in RowLayout
            //if ( !( this.Header is ColumnHeader ) )
            //    return false;

            //UltraGridColumn headerColumn = this.Header.Column;            

            //if ( null == headerColumn || !headerColumn.Band.UseRowLayoutResolved )
            //    return false;
            #endregion // NAS 9.1 - Groups in RowLayout
            //
            UltraGridBand band = this.Header.Band;
            if ( null == band || !band.UseRowLayoutResolved )
                return false;

			if ( !base.SupportsUpDownAdjustmentsFromPoint( point ) )
				return false;

			// if the positionitem doesn't allow sizing return false
			//
			if ( !this.Adjustable )
				return false;

			Rectangle workRect = this.Rect;

			// adjust the left and right of the rect to allow a 
			// narrow area at the right edge of the header to
			// be used for resizing
			//
			workRect.Y = workRect.Bottom - 2;
			workRect.Height = 4;

			if ( ! workRect.Contains( point ) )
				return false;

			//if min width and max width of the header are the same then return false
			//
			int minHeight = 0;
			int maxHeight = 0;
            
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//UltraGridBand band = this.Header.Column.Band;
			//
			//if ( RowLayoutSizing.Vertical != this.Header.Column.RowLayoutColumnInfo.AllowLabelSizingResolved &&
			//    RowLayoutSizing.Both != this.Header.Column.RowLayoutColumnInfo.AllowLabelSizingResolved )
			//    return false;
            // MRS - NAS 9.1 - Groups in RowLayout
			//RowLayoutSizing allowLabelSizingResolved = headerColumn.RowLayoutColumnInfo.AllowLabelSizingResolved;
            RowLayoutSizing allowLabelSizingResolved = this.Header.RowLayoutColumnInfo.AllowLabelSizingResolved;

			if ( RowLayoutSizing.Vertical != allowLabelSizingResolved &&
				RowLayoutSizing.Both != allowLabelSizingResolved )
				return false;

            // MRS - NAS 9.1 - Groups in RowLayout
			//UltraGridBand band = headerColumn.Band;

			// SSP 3/31/03 - Row Layout Designer
			// In row-layout designer, if the control key is down then don't resize the header.
			// The row-layout designer has special logic for resizing the item span with control
			// key down.
			//
			if ( this.RowLayoutDesignerElement && Keys.Control == Control.ModifierKeys )
				return false;
			
			int tmp1 = 0, tmp2 = 0;
			Size containerSize = this.GetLayoutContainerSize( );

			if ( band.AreColumnHeadersInSeparateLayoutArea && band.CardView )
			{
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//band.GetLayoutItemResizeRange( this.Header.Column, new Size( 0, containerSize.Height ), ref tmp1, ref tmp2, ref minHeight, ref maxHeight );
                // MRS - NAS 9.1 - Groups in RowLayout
				//band.GetLayoutItemResizeRange( headerColumn, new Size( 0, containerSize.Height ), ref tmp1, ref tmp2, ref minHeight, ref maxHeight );
                ILayoutItem layoutItem = this.Header.RowLayoutColumnInfoProvider.HeaderLayoutItem;                
                band.GetLayoutItemResizeRange(layoutItem, new Size(0, containerSize.Height), ref tmp1, ref tmp2, ref minHeight, ref maxHeight);
			}
			else 
			{
				// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// 
				//band.GetLayoutItemResizeRange( this.Header, containerSize, ref tmp1, ref tmp2, ref minHeight, ref maxHeight );
				band.GetLayoutItemResizeRange( this, this.Header, containerSize, ref tmp1, ref tmp2, ref minHeight, ref maxHeight );
			}

			if ( 0 != maxHeight && minHeight == maxHeight )
				return false;

			return true;
		}
		
		// SSP 3/19/03 - Row Layout Functionality
		// Added a property to indicate whether we are in row layout designer mode.
		//
		private bool RowLayoutDesignerElement
		{
			get
			{
				DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.GetAncestor( typeof( DataAreaUIElement ) );

				return null != dataAreaElem && dataAreaElem.RowLayoutDesignerElement;
			}
		}

		// MD 2/26/09 - TFS14626
		/// <summary>
		/// For Infragistics internal use only.
		/// </summary>
		[EditorBrowsable( EditorBrowsableState.Never )]
		protected override bool ShouldProcessMouseDown
		{
			get
			{
				if ( this.forceMouseDownProcessing )
					return true;

				return base.ShouldProcessMouseDown;
			}
		}

        /// <summary>
        /// Called when the mouse down message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown( MouseEventArgs e, 
										 bool adjustableArea,
										 ref UIElement captureMouseForElement )
		{
			// SSP 3/20/02 - Row Layout Functionality
			// In layout designer, don't process the OnMouseDown.
			//
			if ( this.RowLayoutDesignerElement )
			{
				// MD 2/26/09 - TFS14626
				// Before calling the base implementation, set a flag which will cause us to ignore the enabled state of the 
				// header element and always process the mouse down.
				//return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );
				bool oldForceMouseDownProcessing = this.forceMouseDownProcessing;
				try
				{
					this.forceMouseDownProcessing = true;
					return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );
				}
				finally
				{
					this.forceMouseDownProcessing = oldForceMouseDownProcessing;
				}
			}

			//RobA UWG457 10/3/01
			//This could be a bandheader
			//
			if ( this.Header is Infragistics.Win.UltraWinGrid.BandHeader )
			{
				//Exit edit mode
				//
				if ( this.Header.Band.Layout.ActiveCell != null )
					this.Header.Band.Layout.ActiveCell.ExitEditMode();

				return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );
			}

			// SSP 9/27/01 UWG365
			// Exit the edit mode before we potentially sort
			// or start a drag operation
			//
			// JJD 10/03/01
			// Use the Band instead of the Column since the column
			// is null for group and band headers
			//
			if ( null != this.Header.Band.Layout.ActiveCell &&
				this.Header.Band.Layout.ActiveCell.IsInEditMode )
			{
				// SSP 10/16/03 UWG2674
				// If exitting the edit mode was cancelled, then don't proceed.
				//
				// ----------------------------------------------------------------------------------
				//this.Header.Band.Layout.ActiveCell.ExitEditMode( );
				UltraGridLayout layout = this.Header.Band.Layout;
				layout.ActiveCell.ExitEditMode( );
				if ( null != layout.ActiveCell && layout.ActiveCell.IsInEditMode )
					return true;
				// ----------------------------------------------------------------------------------
			}

			this.lastClickedPoint = new Point( e.X, e.Y );

			// Reset flags
			this.startDragging = false;
			this.sortClickPending = false;
			// SSP 2/11/05
			// While dragging if the user clicks a different mouse button then end dragging.
			//
			// ------------------------------------------------------------------------------
			bool origDragging = this.dragging;
			this.dragging = false;
			ISelectionManager selectionManager = this.Header.Band.Layout.Grid as ISelectionManager;
			if ( origDragging && null != selectionManager )
			{
				selectionManager.OnDragEnd( true );
			
				// Terminate the capture.
				//
				if ( null != this.ControlElement )
					this.ControlElement.TerminateCapture( );

				// AS 1/16/02
				// we need to invalidate the header when the capture is aborted
				if (!this.IsDrawing)
					this.Invalidate();

				return true;
			}
			// ------------------------------------------------------------------------------

			if ( e.Button == System.Windows.Forms.MouseButtons.Left )
			{
				// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support HeaderClickAction in Combo and DropDown
// --------------------------------------------------------------------------
//				//ROBA UWG 8/22/01 if we are resizing we don't want to capture
//				//the mouse
//				//
//				if ( this.Header.Band.Layout.Grid is UltraGrid &&
//					!this.SupportsLeftRightAdjustmentsFromPoint( new Point(e.X,e.Y)) 
//					// SSP 3/18/03 - Row Layout Functionality
//					// Also check for up down adjustment as we support that in the row layout mode.
//					//
//					&& ! this.SupportsUpDownAdjustmentsFromPoint( new Point( e.X, e.Y ) ) )
				if ( !this.SupportsLeftRightAdjustmentsFromPoint( new Point(e.X,e.Y)) 
					&& ! this.SupportsUpDownAdjustmentsFromPoint( new Point( e.X, e.Y ) ) )
// --------------------------------------------------------------------------
				{
					HeaderClickAction action = this.Header.Band.HeaderClickActionResolved;

					if ( action == HeaderClickAction.SortMulti ||
						 action	== HeaderClickAction.SortSingle 
						// SSP 7/22/05 - NAS 5.3 Column Chooser Feature
						// Added UltraGridColumnChooser class.
						// 
						|| this.IsColumnChooserHeader )
					{		
						if ( this.Header is ColumnHeader )
							this.sortClickPending = true;

						captureMouseForElement = this;

						// this flag will start the dragging if the mouse has
						// moved more than 4 pixels
						this.startDragging = true;
						return true;
					}
				}
			}

			return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );			
		}

		private bool SupportsAutoSizing
		{
			get
			{
				// JJD 1/16/02
				// If the header is an adjustable column header (not in card view ),
				// we are not in design mode we support autosizing on a double click.
				//
				return this.Header is ColumnHeader && 
						this.Adjustable &&
						!this.Header.Band.CardView &&
						!this.Header.Band.Layout.Grid.DesignMode &&
					// SSP 4/14/03
					// Added AutoSizeMode property off the column and ColumnAutoSizeMode off the override.
					//
					ColumnAutoSizeMode.None != this.Header.Column.AutoSizeModeResolved;
			}
		}

        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{
			base.OnDoubleClick( adjustableArea );

			// Call PerformAutoResize
			//
			// JJD 1/16/02
			// If the header is an adjustable column header (not in card view ),
			// we are not in design mode and they double clicked in the adjustable area
			// then perform an autosize
			//
			if ( adjustableArea && this.SupportsAutoSizing
				// SSP 9/12/03
				// Only auto size if the mouse is in the area where the user can resize horizontally.
				// In other words only auto-resize the width if the cursor is near the right border.
				// Added below condition.
				//
				&& null != this.Control && this.SupportsLeftRightAdjustmentsFromPoint( 
						this.Control.PointToClient( System.Windows.Forms.Control.MousePosition ) ) )
			{
				// SSP 4/14/03
				// Added ColumnAutoSizeMode property.
				//
				//this.Header.Column.PerformAutoResize();
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//ColumnAutoSizeMode autoSizeMode = this.Header.Column.AutoSizeModeResolved;

				// SSP 6/24/03 UWG2385
				// Implemented the auto-resize functionality in row layout mode.
				// Enclosed the already existing code in the else statement while added the if block.
				//
				bool useRowLayout = null != this.Header && null != this.Header.Band && this.Header.Band.UseRowLayoutResolved;
				if ( useRowLayout )
				{
					RowsCollection rows = (RowsCollection)this.GetContext( typeof( RowsCollection ) );
					UltraGridBand band = this.Header.Band;

					// SSP 8/12/03 UWG2385
					// Changed the behavior of auto resizing in the row layout mode. Instead of
					// auto-resizing the item that's double clicked, auto-resize all the items that
					// are right-aligned to the item that was double-clicked.
					//
					// --------------------------------------------------------------------------------
					// SSP 8/31/06 BR13795 BR15249
					// Added containerSize and elem params.
					// 
					//band.AutoResizeLayoutItem( this.Header, rows );
					band.AutoResizeLayoutItem( this.Header, rows, this.GetLayoutContainerSize( ), this );
					
					// --------------------------------------------------------------------------------
				}
				else
				{
					// SSP 11/16/04
					// Implemented column sizing using cells in non-rowlayout mode. Moved the code that
					// was here into the new  AutoResizeNonRowLayoutMode method so the CellUIElement can
					// make use of it.
					//
					HeaderUIElement.AutoResizeNonRowLayoutMode( (ColumnHeader)this.Header, this );
				}
			}

			// JAS v5.2 DoubleClick Events 4/28/05 
			// When the header is double-clicked we need to inform the grid that the
			// DoubleClickHeader event must be fired.
			//
			UltraGrid grid = this.Grid;
			if( grid != null && this.Header != null )
				grid.FireEvent( GridEventIds.DoubleClickHeader, new DoubleClickHeaderEventArgs( this.Header ) );
		}

		// SSP 11/16/04
		// Implemented column sizing using cells in non-rowlayout mode. Added 
		// AutoResizeNonRowLayoutMode method and moved code from OnDoubleClick above into this
		// method so the CellUIElement can make use of it.
		//
		internal static void AutoResizeNonRowLayoutMode( ColumnHeader header, UIElement element )
		{
			UltraGridColumn column = header.Column;
			ColumnAutoSizeMode autoSizeMode = column.AutoSizeModeResolved;

			if ( ColumnAutoSizeMode.SiblingRowsOnly == autoSizeMode && 
				null != element.GetContext( typeof( RowsCollection ) ) )
				column.PerformAutoResizeHelper( (RowsCollection)element.GetContext( typeof( RowsCollection ), true ), 0 );
			else if ( ColumnAutoSizeMode.AllRowsInBand == autoSizeMode )
				column.PerformAutoResizeHelper( null, 0 );
			else if ( ColumnAutoSizeMode.None != autoSizeMode )
				column.PerformAutoResize( );
		}

		// SSP 3/11/03 - UWG2047
		// When the header click action is to sort, then we return false from OnMouseDown
		// so the selection statergy doesn't take over since the click may be for changing
		// the sort indicator. In OnMouseMove, we start the drag operation ourselves
		// when the mouse moves over 4 pixels. However as a result of starting the drag
		// ourselves, the selection stratergy doesn't get involved in the mouse move and
		// scrolling and things like that so we have to do our own scrolling as necessary.
		// To do that we need a timer.
		//
		private void OnTimerTick( object sender, EventArgs e )
		{
			if ( ! this.dragging || null == this.Header || null == this.Header.Band ||
				null == this.Header.Band.Layout || null == this.Header.Band.Layout.Grid )
			{
				if ( null != this.dragScrollTimer )
				{
					this.dragScrollTimer.Enabled = false;
					this.dragScrollTimer.Dispose( );
					this.dragScrollTimer = null;
				}
				return;
			}

			UltraGridBase grid = this.Header.Band.Layout.Grid;
			ISelectionManager sm =  grid as ISelectionManager;
				
			if ( null != sm )
			{
				// SSP 10/28/05 BR06561
				// Implemented scrolling vertically in card-view when dragging and dropping columns.
				// 
				// ----------------------------------------------------------------------------------
				

				int timerInterval = 0;
				if ( sm.DoesDragNeedScrollHorizontal( grid.ControlForGridDisplay.PointToClient( Control.MousePosition ), ref timerInterval ) )
					sm.DoDragScrollHorizontal( timerInterval );

                // MBS 12/12/08 - TFS8164
                // Copied the code made from the previous fix for this bug to here, since we shouldn't be 
                // scrolling headers vertically except when in card view
                //
				//if ( sm.DoesDragNeedScrollVertical( grid.ControlForGridDisplay.PointToClient( Control.MousePosition ), ref timerInterval ) )
                if (this.Header.Band.CardView && sm.DoesDragNeedScrollVertical(grid.ControlForGridDisplay.PointToClient(Control.MousePosition), ref timerInterval))
					sm.DoDragScrollVertical( timerInterval );

                // MBS 6/15/07 - BR23950
                // It is possible that we have stopped and disposed the timer as a result of DoDragScrollVertical, so we should check for that
                //
				//if ( timerInterval > 0 )
                if(this.dragScrollTimer != null && timerInterval > 0)
					this.dragScrollTimer.Interval = timerInterval;
				// ----------------------------------------------------------------------------------
			}
		}

		#region ToGridFromControlCoordinates

		// SSP 6/29/05 - NAS 5.3 Column Chooser
		// 
		private Point ToGridFromControlCoordinates( Point p )
		{
			UltraGridBase grid = null != this.Band ? this.Band.Layout.Grid : null;
			System.Windows.Forms.Control control = this.Control;

			if ( grid != control && null != control && null != grid )
			{
				p = control.PointToScreen( p );
				p = grid.PointToClient( p );
			}

			return p;
		}

		#endregion // ToGridFromControlCoordinates

		#region IsColumnChooserHeader

		// SSP 6/29/05 - NAS 5.3 Column Chooser
		// 
		internal virtual bool IsColumnChooserHeader
		{
			get
			{
				return false;
			}
		}

		#endregion // IsColumnChooserHeader

		/// <summary>
		/// Overridden method. Has logic for column dragging.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		/// <returns></returns>
		protected override void OnMouseMove( MouseEventArgs e )
		{
			Point p = new Point( e.X, e.Y );


			UltraGrid grid = null;

            // MBS 5/9/08 - BR32703 
            // Get the band here since we use it in multiple places
            //
            //if ( null != this.Header && null != this.Header.Band.Layout )
            //    grid = this.Header.Band.Layout.Grid as UltraGrid;
            UltraGridBand band = null;
            if (null != this.Header)
            {
                band = this.Header.Band;
                if (band != null && band.Layout != null)
                    grid = this.Header.Band.Layout.Grid as UltraGrid;
            }

			// SSP 6/29/05 - NAS 5.3 Column Chooser
			// Converts the point which is relative to this element's Control to a point which 
			// is relative to the associated UltraGridBase. This is meant to be used by the 
			// column chooser where the header element is in the column chooser however when 
			// dragging it we need to convert points to grid coordinates when calling 
			// DragMoveHelper and other drag related methods on the grid.
			// 
			p = this.ToGridFromControlCoordinates( p );

			// If dragging is already in pursuit
			if ( null != grid && this.dragging )
			{				
				ISelectionManager sm = grid as ISelectionManager;

                // MBS 6/10/08 - BR32703          
                // We need to be able to stop the timer and restart it in the middle of a drag
                //
                //if ( null != sm && ( null == this.dragScrollTimer || ! this.dragScrollTimer.Enabled ) )
                if (null != sm)
				{
					bool needsScrollTimer = false;
					int timerInterval = 200;

                    // MBS 5/9/08 - BR32703
                    // We shouldn't allow horizontal dragging of the header in card view
                    //
                    //if (sm.DoesDragNeedScrollHorizontal( p, ref timerInterval ) )
                    if ((band == null || !band.CardView) && sm.DoesDragNeedScrollHorizontal(p, ref timerInterval))
					{
						sm.DoDragScrollHorizontal( timerInterval );
						needsScrollTimer = true;
					}

					// SSP 10/28/05 BR06561
					// Implemented scrolling vertically in card-view when dragging and dropping columns.
					// 
                    // MBS 10/1/08 - TFS8164
                    // We should specifically be checking that we do this only in card view
                    //
					//if ( sm.DoesDragNeedScrollVertical( p, ref timerInterval ) )
                    if((band == null || band.CardView) && sm.DoesDragNeedScrollVertical( p, ref timerInterval))
					{
						sm.DoDragScrollVertical( timerInterval );
						needsScrollTimer = true;
					}

					if ( needsScrollTimer )
					{
						// SSP 3/11/03 - UWG2047
						// When the header click action is to sort, then we return false from OnMouseDown
						// so the selection statergy doesn't take over since the click may be for changing
						// the sort indicator. In OnMouseMove, we start the drag operation ourselves
						// when the mouse moves over 4 pixels. However as a result of starting the drag
						// ourselves, the selection stratergy doesn't get involved in the mouse move and
						// scrolling and things like that so we have to do our own scrolling as necessary.
						// To do that we need a timer.
						//
						if ( null == this.dragScrollTimer )
						{
							this.dragScrollTimer = Infragistics.Win.Utilities.CreateTimer( );
							this.dragScrollTimer.Tick += new EventHandler( this.OnTimerTick );
							this.dragScrollTimer.Interval = Math.Max( 500, timerInterval );
							this.dragScrollTimer.Enabled = true;
							Debug.WriteLine( ++this.Header.Band.Layout.Grid.tmp + " Header drag timer created" );
						}
						else
						{
							this.dragScrollTimer.Interval = timerInterval;

                            // MBS 6/10/08 - BR32703 
                            this.dragScrollTimer.Enabled = true;
						}
					}
                    // MBS 6/10/08 - BR32703 
                    // If we have already started a drag operation but we move the mouse
                    // into an area that shouldn't cause a drag, we should stop the timer                  
                    else if (this.dragScrollTimer != null)
                    {
                        this.dragScrollTimer.Enabled = false;
                    }  
				}

				// MRS 3/15/04 - Column Moving for RowLayouts
				//grid.DragEffect.OnDragMove( p );								
				grid.DragMoveHelper(p);
					
				return;
			}

			if ( this.startDragging )
			{
				// SSP 6/29/05 - NAS 5.3 Column Chooser
				// Related to the change in the beginning of the method that converts p.
				// 
				//int dx = p.X - this.lastClickedPoint.X;
				//int dy = p.Y - this.lastClickedPoint.Y;
				int dx = e.X - this.lastClickedPoint.X;
				int dy = e.Y - this.lastClickedPoint.Y;

				// If the distance moved is greater thatn 4 points, start the
				// draggin operation
				if ( Math.Sqrt( dx*dx + dy*dy ) > 4 )
				{
                    // MBS 5/9/08 - BR32703
                    // Replaced with stack variable
                    //
                    //HeaderClickAction action = this.Header.Band.HeaderClickActionResolved;
                    HeaderClickAction action = band.HeaderClickActionResolved;
								
					if ( null != grid && 
						// SSP 4/30/03 UWG876
						// Check for the Header.Draggable rather than IsDraggable which always returns true.
						//
						//((ISelectableItem)this.Header).IsDraggable && 
						this.Header.Draggable && 						
						( action == HeaderClickAction.SortMulti ||
						 action	== HeaderClickAction.SortSingle
						 // SSP 7/22/05 - NAS 5.3 Column Chooser Feature
						 // Added UltraGridColumnChooser class.
						 // 
						 || this.IsColumnChooserHeader ) )
					{
						// set the sortClickPending to false so on OnMouseUp sorting log is
						// skipped.
						this.sortClickPending = false;

						bool ret = false;

						if ( this.Header is ColumnHeader )
						{
							Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader = (ColumnHeader)this.Header;
								
							ret = columnHeader.StartDragNoSelection( );
						}
						else if ( this.Header is GroupHeader )
						{
							Infragistics.Win.UltraWinGrid.GroupHeader groupHeader = (GroupHeader)this.Header;

							ret = groupHeader.StartDragNoSelection( );
						}

						// if start drag operation was successful
						if ( ret )
						{
							// SSP 7/16/03 - Fixed headers
							// Set the lastPivotItem member variable which the fixed headers drag
							// scrolling logic relies on.
							//
							grid.ActiveColScrollRegion.InternalSetLastPivotItem( this.Header );

							//MRS 3/23/05 - Column Drag/Drop support for RowLayouts
							//grid.DragEffect.OnDragMove( p );				
							grid.DragMoveHelper(p);
							
							// not to start drag next time since the dragging has
							// already been started
							this.startDragging = false;
							
							// set it to true to indicate that currenty we are in drag mode
							this.dragging = true;

							// AS - 1/16/02 UWG895
							// We need to invalidate the header.
							//
							if (!this.IsDrawing)
								this.Invalidate();

							return;
						}
					}
				}
			}

			base.OnMouseMove( e );
		}


		/// <summary>
		/// Overridden method.
		/// </summary>
		protected override void OnCaptureAborted( )
		{			
			// SSP 3/11/03 - UWG2047
			// Dispose of the scroll timer.
			//
			if ( null != this.dragScrollTimer )
			{
				this.dragScrollTimer.Stop( );
				this.dragScrollTimer.Dispose( );
				this.dragScrollTimer = null;
				Debug.WriteLine( --this.Header.Band.Layout.Grid.tmp + " Header drag timer disposed" );
			}

			UltraGrid grid = null;

			if ( null != this.Header && null != this.Header.Band.Layout )
				grid = this.Header.Band.Layout.Grid as UltraGrid;

			// SSP 7/16/03 - Fixed headers
			// Reset the lastPivotItem of the active col scroll region that we set 
			// when we started the drag operation.
			//
			if ( null != grid && null != grid.ActiveColScrollRegion )
				grid.ActiveColScrollRegion.InternalSetLastPivotItem( null );

			//RobA UWG305 10/3/01
			this.startDragging = false;

			if ( this.dragging && null != grid )
			{
				// Reset flags 
				this.dragging = false;
				//this.startDragging = false;
				this.sortClickPending = false;

				((ISelectionManager)grid).OnDragEnd( true );

				// AS 1/16/02
				// we need to invalidate the header when the capture is aborted
				if (!this.IsDrawing)
					this.Invalidate();

				return;
			}

			base.OnCaptureAborted( );
		}
		
		#region OnMouseUp

		/// <summary>
        /// Called when the mouse up message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
		protected override bool OnMouseUp( MouseEventArgs e )
		{
			// SSP 5/18/06 BR12463
			// Added EndDragHelper. Code in there is moved from here. This changed should not have
			// changed any behavior.
			// 
			if ( this.EndDragHelper( false ) )
				return true;

			return base.OnMouseUp( e );
		}

		// SSP 5/18/06 BR12463
		// Added EndDragHelper. Code in there is moved from OnMouseUp. We need to call this when the
		// element gets disposed.
		// 
		private bool EndDragHelper( bool cancelled )
		{
			// SSP 3/11/03 - UWG2047
			// Dispose of the scroll timer.
			//
			if ( null != this.dragScrollTimer )
			{
				this.dragScrollTimer.Stop( );
				this.dragScrollTimer.Dispose( );
				this.dragScrollTimer = null;
				Debug.WriteLine( --this.Header.Band.Layout.Grid.tmp + " Header drag timer disposed" );
			}

			UltraGrid grid = null;

			if ( null != this.Header && null != this.Header.Band.Layout )
				grid = this.Header.Band.Layout.Grid as UltraGrid;

			// SSP 7/16/03 - Fixed headers
			// Reset the lastPivotItem of the active col scroll region that we set 
			// when we started the drag operation.
			//
			if ( null != grid && null != grid.ActiveColScrollRegion )
				grid.ActiveColScrollRegion.InternalSetLastPivotItem( null );

			//RobA UWG305 9/18/01
			this.startDragging = false;
			
			// SSP 8/27/01
			// Added code for dragging
			if ( this.dragging && null != grid )
			{
				// Reset the flags
				this.dragging = false;
				//this.startDragging = false;
				this.sortClickPending = false;	
			
				// SSP 5/18/06 BR12463
				// Pass along the cancelled.
				// 
				//((ISelectionManager)grid).OnDragEnd( false );	
				((ISelectionManager)grid).OnDragEnd( cancelled );

                // MRS 5/14/2009 - TFS17457
                // Ripped this out because it's causing performance (TFS17620) and serilalization (TFS17457) issues.
                //
                //// MBS 10/7/08 - TFS8698
                //// When we complete dragging a header, we should ensure that
                //// the metrics are properly recalculated at this point before
                //// doing a full repaint or resizing, since otherwise we will
                //// not take into account the fact that the header position 
                //// could trigger a row to be taller due to text now needing
                //// to wrap, or other effects of moving a column.
                ////
                //// Note that this will not cause a duplicate initialization of
                //// the metrics since repositioning the header dirtied the metrics
                //// and once we do the recalculation here, the metricsDirty flag
                //// will not be set and we will not unnecessarily recalculate.
                //this.Header.Layout.ColScrollRegions.InitializeMetrics();               
			
				// AS 1/16/02
				// we need to invalidate the header when the capture is aborted
				if (!this.IsDrawing)
					this.Invalidate();

				return true;
			}

			if ( this.sortClickPending )
			{
				this.sortClickPending = false;

				ColumnHeader columnHeader = this.Header as ColumnHeader;

				if ( columnHeader != null )
				{
					columnHeader.Column.ClickSortIndicator();

					this.Invalidate();

					// SSP 3/25/05 BR03003
					// RowCellAreaUIElement in OnClick activates the associated row. If the band is in 
					// headers-with-cells row-layout mode and a header in a row is clicked we will end
					// up changing the sort here and then activating the row when the row cell area gets
					// OnClick. This poses a problem because if in the AfterSortChange event handler (which
					// would get fired before the OnClick of the row cell area elem) some actions were 
					// taken like scrolling to the top or activating a different row then when the row cell
					// area elem gets OnClick, it will end up activating its associated row and thus
					// overriding the active row that the user set. So return true to prevent OnClick from 
					// being called on the parent element. Note that in the case of non-headers-with-cells 
					// mode the parent element of the header element would be BandHeadersUIElement which 
					// does not utilize OnClick nor does any of its parents so it's safe to always return 
					// true and prevent OnClick from being called whenever we perform a sort operation.
					// 
					return true;
				}
			}

			return false;
		}

		#endregion // OnMouseUp

		/// <summary>
		/// Returns a boolean indicating if the item requires the rendering a separator.
		/// </summary>
		protected override bool RequiresSeparator
		{
			get 
			{ 
				if (this.Header is BandHeader)
					return false;

				return true;
			}
		}


		// AS - 12/14/01
		#region ThemeSupport
		// Added theme support to the grid's header elements

		/// <summary>
		/// Indicates if the element supports hot tracking over the header.
		/// </summary>
		protected override bool ActiveThemeMouseTracking
		{
			get
			{
				// only if this is not a band header
				return ( !(this.Header is Infragistics.Win.UltraWinGrid.BandHeader) );
			}
		}

		/// <summary>
		/// Indicates the state of the header.
		/// </summary>
		protected override UIElementButtonState HeaderState
		{
			get
			{
				HeaderBase header = this.Header;
				if ( header is Infragistics.Win.UltraWinGrid.BandHeader )
					return 0;

				// SSP 7/22/05 - NAS 5.3 Column Chooser Feature
				// 
				//if ( header.Selected || this.HasCapture )
				if ( ( header.Selected || this.HasCapture ) && ! this.IsColumnChooserHeader )
					return UIElementButtonState.MouseDownAndOver;

				// SSP 9/11/03 UWG2389
				// Commented out the original code and added the new one. Base
				// implementation checks for design time and returns 0.
				//
				// --------------------------------------------------------------
				return base.HeaderState;
				
				// --------------------------------------------------------------
			}
		}
		#endregion ThemeSupport


		// SSP 3/11/03 - Row Layout Functionality
		// When the cells and headers are displayed together in the row area, then
		// select the associated cell when the header is clicked upon.
		// Overrode SelectableItem property.
		//
		#region SelectableItem

		/// <summary>
		/// If the context for this element is a selectable item
		/// (e.g. a grid row, cell or header) it is returned. The
		/// default implementation walks up the parent chain calling
		/// this method recursively until a selectable item is found
		/// or the control element is reached
		/// </summary>
		public override ISelectableItem SelectableItem
		{
			get
			{
				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//UltraGridBand band = 
				//    null != this.Header && null != this.Header.Column 
				//    ? this.Header.Column.Band 
				//    : null;
				UltraGridColumn column = null != this.Header 
					? this.Header.Column 
					: null;

				UltraGridBand band = null != column
					? column.Band
					: null;

				// In row layout mode if the headers are with the cells in the row cell area,
				// then return the cell for selection because when the user clicks on one
				// of these labels, we want to select the cell and not the column.
				//
				// SSP 2/3/05
				// Check if the row-layout functionality is turned on.
				//
				//if ( null != band && ! band.AreColumnHeadersInSeparateLayoutArea )
				if ( null != band && band.UseRowLayoutResolved && ! band.AreColumnHeadersInSeparateLayoutArea )
				{
					UltraGridRow row = (UltraGridRow)this.GetContext( typeof( UltraGridRow ), true );
			
					if ( null != row )
					{
						// SSP 6/27/03 UWG2371
						// If the cell click action is set to row select, then don't return the cell.
						// Return the row.
						//
						if ( CellClickAction.RowSelect == band.CellClickActionResolved 
							// SSP 2/3/05
							// In UltraDropDown and UltraCombo we want to select the row instead of 
							// the cell. Added below condition.
							// 
							|| null != band.Layout && band.Layout.Grid is UltraDropDownBase )
							return row;

						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//return row.Cells[ this.Header.Column ];
						return row.Cells[ column ];
					}
				}

				return base.SelectableItem;
			}
		}

		#endregion // SelectableItem

		// SSP 3/14/03 - Row Layout Functionality
		//
		#region AdjustForFirstItem

		private void AdjustForFirstItem( ref Rectangle rect )
		{
			UltraGridColumn column = this.Header is ColumnHeader ? this.Header.Column : null;

			if ( null != column && column.Band.AreColumnHeadersInSeparateLayoutArea && ! column.Band.CardView )
			{
				int delta = column.RowLayoutColumnInfo.CachedItemRectCell.Left - column.RowLayoutColumnInfo.CachedItemRectHeader.Left;

				rect.X += delta;
				rect.Width -= delta;
			}
		}

		#endregion // AdjustForFirstItem

		// SSP 3/14/03 - Row Layout Functionality
		//
		#region GetLayoutContainerSize

		private Size GetLayoutContainerSize( )
		{
            // MRS - NAS 9.1 - Groups in RowLayout            
            UltraGridBand band = this.Band;
            if (band != null &&
                band.RowLayoutStyle == RowLayoutStyle.GroupLayout)
            {
                UltraGridColumn column = this.GetContext(typeof(UltraGridColumn)) as UltraGridColumn;
                if (column != null && column.RowLayoutColumnInfo.ParentGroup != null)
                {
                    return column.RowLayoutColumnInfo.ParentGroup.RowLayoutGroupInfo.CachedItemRectCell.Size;
                }
            }

			BandHeadersUIElement bandHeadersElem = (BandHeadersUIElement)this.GetAncestor( typeof( BandHeadersUIElement ) );

			if ( null != bandHeadersElem )
			{
				Rectangle rect = bandHeadersElem.Rect;
				int rowSelectorExtent = bandHeadersElem.Band.RowSelectorExtent;
				int bandHeaderExtent = null != bandHeadersElem.Band ? bandHeadersElem.Band.Header.GetBandHeaderHeight( ) : 0;

				// SSP 8/6/03
				// In row layout designer, we don't put the band header element and the BandHeadersUIElement
				// is also resized by the row layout designer to not include the band header extent. So
				// don't substract the band header extent if this element is inside the row layout designer.
				//
				if ( this.RowLayoutDesignerElement )
					bandHeaderExtent = 0;

				rect.Y += bandHeaderExtent;
				rect.Height -= bandHeaderExtent;
				rect.X += rowSelectorExtent;
				rect.Width -= rowSelectorExtent;

				// SSP 8/12/03 - Fixed headers
				// Take into account the fact the row cell area element could have been shrinked in
				// the fixed headers mode. However we still need to pass in the whole row cell area
				// rect to the layout manager and not the shrunk rect.
				//
				// ------------------------------------------------------------------------------------
				if ( bandHeadersElem.usingFixedHeaders )
				{
					ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ), true );

					if ( null != csr && null != bandHeadersElem && null != bandHeadersElem.Band )
					{
						int delta = bandHeadersElem.Band.GetFixedHeaders_OriginDelta( csr );
						rect.X -= delta;
						rect.Width += delta;
					}
				}
				// ------------------------------------------------------------------------------------

				return rect.Size;
			}
			else
			{
				RowCellAreaUIElement rowCellAreaElem = (RowCellAreaUIElement)this.GetAncestor( typeof( RowCellAreaUIElement ) );

				if ( null != rowCellAreaElem )
				{
					// SSP 8/6/03
					// Take into account the auto preview area if there is one. It's not part of the layout.
					// Commented out the original code and added the new code below it.
					//
					//return rowCellAreaElem.Rect.Size;
					rowCellAreaElem.GetLayoutContainerSize( );
				}
			}

			return Size.Empty;
		}

		#endregion // GetLayoutContainerSize		

		// MRS 3/31/05 - SpanResizing support for RowLayouts
		#region SpanResizing support for RowLayouts

		#region Private / Internal Properties

		#region Band
		private UltraGridBand Band
		{			
			get
			{
				return this.Header.Band;
			}
		}
		#endregion Band

		#region Grid
		private UltraGrid Grid
		{			
			get
			{
				// MRS 4/28/05
//				if (!this.IsColumnHeader)
//					return null;
//
//				if (null == this.Header.Column ||
//					null == this.Header.Column.Layout) 
//				{
//					return null;
//				}
//
//				return this.Header.Column.Layout.Grid as UltraGrid;

				if (null == this.Band ||
					null == this.Band.Layout)
				{
					return null;
				}

				return this.Band.Layout.Grid as UltraGrid;				
			}
		}
		#endregion Grid

		#region SpanResizeCursorVert
		private Cursor SpanResizeCursorVert
		{
			get
			{
				UltraGrid grid = this.Grid;
				if (null == grid)
					return null;

				return grid.SpanResizeCursorVert;
			}
		}
		#endregion SpanResizeCursorVert

		#region SpanResizeCursorHoriz
		private Cursor SpanResizeCursorHoriz
		{
			get
			{
				UltraGrid grid = this.Grid;
				if (null == grid)
					return null;

				return grid.SpanResizeCursorHoriz;
			}
		}
		#endregion SpanResizeCursorHoriz

		#region IsColumnHeader
		private bool IsColumnHeader
		{
			get
			{
				return (this.Header is ColumnHeader);
			}
		}
		#endregion IsColumnHeader

		#endregion Private / Internal Properties

		#region Private / Internal Methods

		#region SupportsSpanSizing
		private bool SupportsSpanSizing
		{
			get
			{
                
				if (!this.IsColumnHeader 
                    // MRS - NAS 9.1 - Groups in RowLayout
                    && !this.IsGroupHeader)
                {
					return false;
                }

				if (!GridUtils.IsControlKeyDown)
					return false;
				
				if (!this.Band.UseRowLayoutResolved)
					return false;

				return true;
			}
		}

		#endregion SupportsSpanSizing

		#region SupportsLeftRightSpanSizingFromPoint
		/// <summary>
		/// Returns true if the element can be Span Sized horizontally
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
		internal bool SupportsLeftRightSpanSizingFromPoint( System.Drawing.Point point )
		{ 
			if ( ! base.SupportsLeftRightAdjustmentsFromPoint( point ) )
				return false;
			
			// MRS 4/13/05
			if (!this.IsColumnHeader)
            {
                // MRS - NAS 9.1 - Groups in RowLayout
                if (this.IsGroupHeader)
                {
                    LabelPosition labelPosition = this.Header.Group.RowLayoutGroupInfo.LabelPositionResolved;
                    if (labelPosition != LabelPosition.Left &&
                        labelPosition != LabelPosition.Right &&
                        labelPosition != LabelPosition.LabelOnly)
                    {
                        return false;
                    }
                }
                else
				    return false;
            }

			if (null == this.Band )
				return false;

			Layout.GridBagLayoutAllowSpanSizing allowSpanSizing = this.Band.AllowRowLayoutLabelSpanSizingResolved;
			
			return (Layout.GridBagLayoutAllowSpanSizing.AllowAll == allowSpanSizing ||
				Layout.GridBagLayoutAllowSpanSizing.AllowSpanXChange == allowSpanSizing);
		}
		#endregion SupportsLeftRightSpanSizingFromPoint

		#region SupportsUpDownSpanSizingFromPoint
		/// <summary>
		/// Returns true if the element can be Span Sized vertically
		/// by clicking on the passed in mouse point
		/// </summary>
		/// <param name="point">In client coordinates</param>
		internal bool SupportsUpDownSpanSizingFromPoint( System.Drawing.Point point )
		{ 
			if ( ! base.SupportsUpDownAdjustmentsFromPoint( point ) )
				return false;

            // MRS 4/13/05
            if (!this.IsColumnHeader)
            {
                // MRS - NAS 9.1 - Groups in RowLayout
                if (this.IsGroupHeader)
                {
                    LabelPosition labelPosition = this.Header.Group.RowLayoutGroupInfo.LabelPositionResolved;
                    if (labelPosition != LabelPosition.Top &&
                        labelPosition != LabelPosition.Bottom &&
                        labelPosition != LabelPosition.LabelOnly)
                    {
                        return false;
                    }
                }
                else
                    return false;
            }

			if (null == this.Band )
				return false;

			Layout.GridBagLayoutAllowSpanSizing allowSpanSizing = this.Band.AllowRowLayoutLabelSpanSizingResolved;
			
			return (Layout.GridBagLayoutAllowSpanSizing.AllowAll == allowSpanSizing ||
				Layout.GridBagLayoutAllowSpanSizing.AllowSpanYChange == allowSpanSizing);
		}
		#endregion SupportsUpDownSpanSizingFromPoint		

		#region OnKeyToggled
		internal void OnKeyToggled()
		{
			// MRS 4/11/05	
			if (null != this.Grid &&
				null != this.Band &&
				this.Band.UseRowLayoutResolved)
			{
				if (!this.Grid.IsHandleCreated)			
					return;

				Point point = this.Grid.PointToClient(Control.MousePosition);
				if (this.wasShowingSpanCursor == this.WillShowSpanCursor(point))
					return;
			}

            // MRS 12/19/06 - fxCop
            //// JJD 1/19/02
            //// Assert the UIPermission in case our assembly has access but the
            //// calling assembly doesn't. This is safe because what we are doing
            //// after this is benign.
            ////
            //System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

            //try { perm.Assert(); } 
            //catch(Exception){}

            //// JJD 1/18/02
            //// Set the cursor position to itself which will trigger an update 
            //// of the cursor but wrap it in a try/catch in case we don't have
            //// access
            ////
            //try
            //{
            //    System.Windows.Forms.Cursor.Position = System.Windows.Forms.Cursor.Position;
            //}
            //catch(Exception)
            //{
            //}
            GridUtils.SetCursorPosition(System.Windows.Forms.Cursor.Position);
		}
		#endregion OnKeyToggled
		
		#region WillShowSpanCursor
		private bool WillShowSpanCursor (Point point)
		{
			if (GridUtils.IsControlKeyDown &&
				this.SupportsSpanSizing &&
				(this.SupportsLeftRightSpanSizingFromPoint( point ) ||
				this.SupportsUpDownSpanSizingFromPoint( point ) ) )
			{
				return true;
			}
			return false;		
		}
		
		#endregion WillShowSpanCursor

		#endregion Private / Internal Methods

		#region Overrides

		#region CanStartAdjustment
		/// <summary>
		/// Invoked when the mouse is pressed down on the adjustable area 
		/// before an adjustment begins.
		/// </summary>
		/// <param name="e">Mouse event args from the MouseDown</param>
		/// <returns>True if an adjustment can be started</returns>
		protected override bool CanStartAdjustment(System.Windows.Forms.MouseEventArgs e)
		{
			Point p = new Point(e.X, e.Y);

			// MRS 4/11/05
			//if (this.SupportsSpanSizing &&
			if (null != this.Grid &&
				this.SupportsSpanSizing &&
				(this.SupportsLeftRightSpanSizingFromPoint( p ) || 
				this.SupportsUpDownSpanSizingFromPoint( p ) ) )
			{

                if (!this.IsColumnHeader
                    // MRS - NAS 9.1 - Groups in RowLayout
                    && !this.IsGroupHeader)
                {
                    return false;
                }

				bool isDragSpanHorizontal = this.SupportsLeftRightAdjustmentsFromPoint( new Point(e.X, e.Y) );
				bool isHeaderBeingDragged = true;

                // MRS - NAS 9.1 - Groups in RowLayout
				//return this.Grid.StartRowLayoutSpanResizeDrag(this.Header.Column, isDragSpanHorizontal, isHeaderBeingDragged);				
                return this.Grid.StartRowLayoutSpanResizeDrag(this.Header.RowLayoutColumnInfoProvider, isDragSpanHorizontal, isHeaderBeingDragged);
			}

			return base.CanStartAdjustment(e);
		}
		#endregion CanStartAdjustment	

		#region OnNewDeltaX
		/// <summary>
		/// Called when the the X delta has changed.
		/// </summary>
		protected override void OnNewDeltaX(int newDeltaX)
		{
			// MRS 4/11/05
			//if (this.Grid.gridBagLayoutDragStrategy != null)
			if (null != this.Grid &&
				this.Grid.gridBagLayoutDragStrategy != null)
			{					
				Point pointInControlCoords = this.Grid.PointToClient( Control.MousePosition );										
				this.Grid.DragSpanMove(pointInControlCoords, this, this.Header, this.Header.Column);
				return;
			}

			base.OnNewDeltaX(newDeltaX);
		}
		#endregion OnNewDeltaX

		#region OnNewDeltaY	
		/// <summary>
		/// Called when the the Y delta has changed.
		/// </summary>
		protected override void OnNewDeltaY(int newDeltaY)
		{
			// MRS 4/11/05
			//if (this.Grid.gridBagLayoutDragStrategy != null)
			if (null != this.Grid &&
				this.Grid.gridBagLayoutDragStrategy != null)
			{					
				Point pointInControlCoords = this.Grid.PointToClient( Control.MousePosition );										
				this.Grid.DragSpanMove(pointInControlCoords, this, this.Header, this.Header.Column);
				return;
			}

			base.OnNewDeltaY(newDeltaY);
		}
		#endregion OnNewDeltaY

		#region DrawAdjustmentBarWhileSizing
		/// <summary>
		/// True if Adjustment Bar should be drawn while sizing.
		/// </summary>
		protected override bool DrawAdjustmentBarWhileSizing
		{
			get
			{
				// MRS 4/11/05
				//if (this.Grid.gridBagLayoutDragStrategy != null)
				// MRS 4/28/05
				//if (null != this.Grid &&
				if (this.IsColumnHeader &&
					null != this.Grid &&
					this.Grid.gridBagLayoutDragStrategy != null)
					return false;

				return base.DrawAdjustmentBarWhileSizing;
			}
		}
		#endregion DrawAdjustmentBarWhileSizing

		#region OnElementAdjustmentAborted
		/// <summary>
		/// Called after a CaptureAborted is received and the adjustment is canceled. 
		/// </summary>
		public override void OnElementAdjustmentAborted()
		{
			if (null == this.Grid)
				return;

			this.Grid.ResetGridBagLayoutDragStrategy();	
		}
		#endregion OnElementAdjustmentAborted

		#endregion Overrides		

		#endregion SpanResizing support for RowLayouts

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
		//
		#region Header/RowSelector Styles

			#region HeaderStyle

		/// <summary>
		/// Returns the HeaderStyle that this column header should use.
		/// </summary>
		protected override HeaderStyle HeaderStyle
		{
			get
			{
				// If this is a column header, then get the band directly off this object.
				//
				UltraGridBand band = this.Band;

				// MRS 4/13/05
//				// If 'band' is null then this is a group header, so navigate to the band through the Header.
//				//
//				if( band					== null && 
//					this.Header				!= null && 
//					this.Header.Group		!= null && 
//					this.Header.Group.Band	!= null )
//					band = this.Header.Group.Band;			

				if( band != null )
					return band.HeaderStyleResolved;

				return HeaderStyle.XPThemed;
			}
		}

			#endregion // HeaderStyle

		#endregion // Header/RowSelector Styles

		#region Offset

		// SSP 5/18/05 - Optimizations
		// Overrode Offset so we can offset the clipSelfRect as well.
		//
		/// <summary>
		/// Overridden. Offsets this element's rect and (optionally) all of its descendant elements.
		/// </summary>
		/// <param name="deltaX">The number of pixels to offset left/right</param>
		/// <param name="deltaY">The number of pixels to offset up/down </param>
		/// <param name="recursive">If true will offset all descendant elements as well</param>
		public override void Offset( int deltaX, int deltaY, bool recursive )
		{
			if ( ! this.clipSelfRect.IsEmpty )
				this.clipSelfRect.Offset( deltaX, deltaY );

			base.Offset( deltaX, deltaY, recursive );
		}

		#endregion // Offset

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// Overrode OnMouseEnter and OnMouseLeave so we can invalidate the row if there
		// are hot tracking appearances.
		// 
		#region NAS 5.3 Row, Cell and Header Hot Tracking Appearances

		#region OnMouseEnterLeave

		private void OnMouseEnterLeave( bool enter )
		{
			this.isHotTracking = enter;

			Infragistics.Win.UltraWinGrid.HeaderBase header = this.Header;

			if ( null != header && header.HasHotTrackingAppearances )
			{
				// SSP 8/28/06 - NAS 6.3
				// Added CellHottrackInvalidationStyle property on the layout.
				// 
				// --------------------------------------------------------------
				
				CellHottrackInvalidationStyle invalidationStyle = header.Layout.CellHottrackInvalidationStyleResolved;
				if ( CellHottrackInvalidationStyle.Never != invalidationStyle )
					this.DirtyChildElements( );
				// --------------------------------------------------------------
			}
		}

		internal bool InvalidateIfHotTracked( )
		{
			if ( this.isHotTracking )
			{
				this.OnMouseEnterLeave( this.isHotTracking );
				return true;
			}

			return false;
		}

		#endregion // OnMouseEnterLeave

		#region OnMouseEnter

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseEnter( )
		{
			this.OnMouseEnterLeave( true );

			base.OnMouseEnter( );
		}

		#endregion // OnMouseEnter

		#region OnMouseLeave

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseLeave( )
		{
			this.OnMouseEnterLeave( false );

			base.OnMouseLeave( );
		}

		#endregion // OnMouseLeave

		#endregion // NAS 5.3 Row, Cell and Header Hot Tracking Appearances

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				StyleUtils.Role eRole = StyleUtils.GetHeaderRole( this.Header, false );
				
				return StyleUtils.GetRole( this.Band, eRole );
			}
		}

		#endregion // UIRole

		#region OnDispose

		// SSP 5/18/06 BR12463
		// Overrode OnDispose.
		// 
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnDispose( )
		{
			this.EndDragHelper( true );

			base.OnDispose( );
		}

		#endregion // OnDispose


        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout
        
        #region IsGroupHeader
        private bool IsGroupHeader
        {
            get
            {
                return (this.Header is GroupHeader);
            }
        }
        #endregion // IsGroupHeader

        #endregion // NAS 9.1 - Groups in RowLayout


		#region MD 2/12/09 - TFS13812

		#region IsRightFixedHeader

		private bool IsRightFixedHeader
		{
			get
			{
				if ( this.Header.FixOnRightResolved == false )
					return false;

				ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ), true );
				return this.Band.IsHeaderFixed( csr, this.Header );
			}
		} 

		#endregion IsRightFixedHeader

		#region SupportsLeftRightAdjustmentsFromLeftBorder

		/// <summary>
		/// True if this element supports left to right adjustments by grabbing the left border
		/// </summary>
		protected override bool SupportsLeftRightAdjustmentsFromLeftBorder
		{
			get
			{
				// MD 2/23/09 - TFS14332
				// The right fixed headers will continue to resize from the right side.
				//// Right-fixed headers must resize from the left side.
				//if ( this.IsRightFixedHeader )
				//    return true;

				return base.SupportsLeftRightAdjustmentsFromLeftBorder;
			}
		} 

		#endregion SupportsLeftRightAdjustmentsFromLeftBorder

		#region SupportsLeftRightAdjustmentsFromRightBorder

		/// <summary>
		/// True if this element supports left to right adjustments by grabbing the right border
		/// </summary>
		protected override bool SupportsLeftRightAdjustmentsFromRightBorder
		{
			get
			{
				// MD 2/23/09 - TFS14332
				// The right fixed headers will continue to resize from the right side.
				//// Right-fixed headers must resize from the left side.
				//if ( this.IsRightFixedHeader )
				//    return false;

				return base.SupportsLeftRightAdjustmentsFromRightBorder;
			}
		} 

		#endregion SupportsLeftRightAdjustmentsFromRightBorder

		#region WantsInputNotification

		/// <summary>
		/// Returns true if this ui element is interested in getting notificaions of type inputType
		/// at the specified location. Default implementation always returns true.
		/// </summary>
		/// <param name="inputType">The type of notification.</param>
		/// <param name="point">Point of interest.</param>
		/// <returns>True if the element wants to recieve notifications of the specified input type.</returns>
		protected override bool WantsInputNotification( UIElementInputType inputType, Point point )
		{
			// The right-fixed header separator should never repond to mouse input
			if ( this.isRightFixedHeaderSeparator )
				return false;

			// If fixed headers are used and this header is partially or fully under the right-fixed headers, do not respond to the mouse when 
			// it is over the right-fixed headers
			if ( this.Band.UseFixedHeaders && this.IsRightFixedHeader == false )
			{
				ColScrollRegion csr = (ColScrollRegion)this.GetContext( typeof( ColScrollRegion ), true );

				HeaderBase rightFixedHeader = this.Band.GetFirstRightFixedHeader( csr );

				if ( rightFixedHeader != null )
				{
					int fixedRightStart = rightFixedHeader.GetOnScreenOrigin( csr, false );

					if ( fixedRightStart < point.X )
						return false;
				}
			}

			return base.WantsInputNotification( inputType, point );
		}  

		#endregion WantsInputNotification

		#endregion MD 2/12/09 - TFS13812
    }
}
