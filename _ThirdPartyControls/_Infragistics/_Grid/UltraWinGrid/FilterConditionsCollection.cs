#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.Globalization;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Shared.Serialization;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using Infragistics.Win.UltraWinMaskedEdit;
	using System.Drawing.Design;
	using System.Windows.Forms.Design;
	using System.Text;
	using System.Text.RegularExpressions;
	using System.Security.Permissions;

	// SSP 3/21/02
	// Version 2 Row Filter Implementation
	// Added FilterConditionsCollection class
	// 


	/// <summary>
	/// Collection of FilterCondition objects.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// A <b>FilterCondition</b> object defines a single condition.
	/// Multiple FilterCondition instances can be added to the 
	/// <see cref="FilterConditionsCollection"/>. 
	/// A <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/> instance
	/// contains a <b>FilterConditionsCollection</b> instance. The <b>ColumnFilter</b>
    /// has <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter.LogicalOperator"/> property which specifies
	/// how multiple conditions contained in the ColumnFilter's FilterConditionCollection
	/// are to be combined.
	/// A <see cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/> 
	/// can contain multiple <b>ColumnFilter</b> instances. Both the
	/// <see cref="UltraGridBand"/> and <see cref="RowsCollection"/> objects expose
	/// <b>ColumnFilters</b> property. This property returns a collection of
	/// <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/> objects.
	/// UltraGrid will filter rows using either the 
	/// RowsCollection's <see cref="RowsCollection.ColumnFilters"/> or 
	/// UltraGridBand's <see cref="UltraGridBand.ColumnFilters"/> depending on the
	/// what the Override's <see cref="UltraGridOverride.RowFilterMode"/> property
	/// is set to. See <see cref="UltraGridOverride.RowFilterMode"/> for more information.
	/// </p>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterCondition"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter.FilterConditions"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/>
	/// <seealso cref="UltraGridBand.ColumnFilters"/>
	/// <seealso cref="RowsCollection.ColumnFilters"/>
	/// <seealso cref="UltraGridOverride.RowFilterMode"/>
	/// <seealso cref="UltraGridOverride.FilterUIType"/>
	/// </remarks>
	[ Serializable() ]
	public class FilterConditionsCollection : SubObjectsCollectionBase, ISerializable
	{
		#region Private Vars

		private ColumnFilter columnFilter = null;

		#endregion // Private Vars

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="columnFilter">Associated column filter.</param>
		internal FilterConditionsCollection( ColumnFilter columnFilter ) : base( )
		{
			if ( null == columnFilter )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_130"), Shared.SR.GetString("LE_ArgumentNullException_329") );

			this.columnFilter = columnFilter;
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal FilterConditionsCollection( SerializationInfo info, StreamingContext context )
		protected FilterConditionsCollection( SerializationInfo info, StreamingContext context )
		{
			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			//
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( Infragistics.Win.UltraWinGrid.FilterCondition ) 
					// SSP 8/21/03 UWG2603
					// Added a way for the users to implement their own custom filtering logic.
					// 
					// MD 8/3/07 - 7.3 Performance
					// IsAssignableFrom is faster than IsSubclassOf
					//|| entry.ObjectType.IsSubclassOf( typeof( Infragistics.Win.UltraWinGrid.FilterCondition ) ) 
					|| typeof( Infragistics.Win.UltraWinGrid.FilterCondition ).IsAssignableFrom( entry.ObjectType ) 
					)
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					FilterCondition item = (FilterCondition)Utils.DeserializeProperty( entry, typeof(FilterCondition), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd( item );
				}
				else
				{
					switch ( entry.Name )
					{
						case "Tag":
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						default:
							Debug.Assert( false, "Invalid entry in FilterConditionsCollection de-serialization ctor" );
							break;
					}
				}
			}
		}

		#endregion // Constructor

		#region Private/Internal Properties

		#region ColumnFilter

		internal ColumnFilter ColumnFilter
		{
			get
			{
				return this.columnFilter;
			}
		}

		#endregion // ColumnFilter

		#region Column

		internal UltraGridColumn Column
		{
			get
			{
				return this.ColumnFilter.Column;
			}
		}

		#endregion // Column

		#endregion // Private/Internal Properties

		#region Base Protected/Public Overrides

		#region InitialCapacity

		/// <summary>
		/// Property that specifies the initial capacity
		/// of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return 2;
			}
		}

		#endregion // InitialCapacity

		#region IsReadOnly

		/// <summary>
		/// Overridden. Returns true if the collection is read-only.
		/// </summary>
		public override bool IsReadOnly 
		{
			get
			{
				return true;
			}
		}

		#endregion // IsReadOnly
		
		#region OnSubObjectPropChanged

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
        protected override void OnSubObjectPropChanged(PropChangeInfo propChange) 
		{
			// Notify the listeners (owing ColumnFilter in this case) that 
			// FilterConditions have changed.
			//
			this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FilterConditions );
		}

		#endregion // OnSubObjectPropChanged

		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			for ( int i = 0; i < this.Count; i++ )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, i.ToString( ), this[i] );
				//info.AddValue( i.ToString( ), this[i] );
			}
		}
		
		#endregion // ISerializable.GetObjectData

		#endregion // Base Overrides

		#region Private/Internal Methods

		#region InitializeFrom

		internal void InitializeFrom( FilterConditionsCollection source )
		{
			if ( source.ShouldSerializeTag( ) )
				this.tagValue = source.tagValue;

			this.Clear( );

			for ( int i = 0; i < source.Count; i++ )
			{
				// SSP 8/21/03 UWG2603
				// Added a way for the users to implement their own custom filtering logic.
				// Commented out the original code and added the new one.
				//
				// --------------------------------------------------------------------------------------
				
				this.Add( (FilterCondition)source[i].Clone( ) );
				// --------------------------------------------------------------------------------------
			}
		}

		#endregion // InitializeFrom
		
		#endregion // Private/Internal Methods

		#region Public Methods

		#region Add

		// SSP 8/21/03 UWG2603
		// Added a way for the users to implement their own custom filtering logic.
		// Added an overload of Add that takes in a filter condition object.
		//
		/// <summary>
		/// Adds the specified filterCondition to the collection.
		/// </summary>
		/// <param name="filterCondition">The filter condition instance to add to the collection.</param>
		public void Add( FilterCondition filterCondition )
		{
			// SSP 5/5/05
			//
			//filterCondition.Initialize( this, null );
			filterCondition.Initialize( this );

			this.InternalAdd( filterCondition );
			this.FilterConditionAdded( filterCondition );
		}

        // MBS 12/3/08 - NA9.1 Excel Style Filtering
        // Added an additional overload for discoverability that the user can 
        // use a SpecialFilterOperand as the CompareValue
        //
        /// <summary>
        /// Adds a new FilterCondition object to the collection.
        /// </summary>
        /// <param name="comparisonOperator">The comparison operator of the new filter condition.</param>
        /// <param name="specialOperand">An instance of a special operand that provides advanced filtering capabilities.</param>
        /// <returns>Returns the created FilterCondition instance.</returns>
        public FilterCondition Add(FilterComparisionOperator comparisonOperator, SpecialFilterOperand specialOperand)
        {
            return this.Add(comparisonOperator, (object)specialOperand);
        }

		/// <summary>
		/// Adds a new FilterCondition object to the collection.
		/// </summary>
		/// <param name="comparisionOperator">The comparison operator of the new filter condition.</param>
		/// <param name="compareValue">The comparison value of the new filter condition.</param>
		/// <returns>Returns the created FilterCondition instance.</returns>
		public Infragistics.Win.UltraWinGrid.FilterCondition Add( 
			FilterComparisionOperator comparisionOperator,
			object compareValue )
		{
			FilterCondition filterCondition = new FilterCondition( this, comparisionOperator, compareValue );

			this.InternalAdd( filterCondition );

			// SSP 8/21/03 UWG2603
			// Added a way for the users to implement their own custom filtering logic.
			//
			// ------------------------------------------------------------------------
			this.FilterConditionAdded( filterCondition );
            
			// ------------------------------------------------------------------------

			return filterCondition;
		}

		private void FilterConditionAdded( FilterCondition filterCondition )
		{
			// Hook into the filterCondition's SubBjectPropChanged event so
			// we get notified if CompareValue or ComparisionOperator
			// changes off the FilterCondition.
			//
			filterCondition.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// Notify the listeners that an item was added.
			//
			this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.Add );
		}

		#endregion // Add

		#region Clear

		/// <summary>
		/// Empties the collection.
		/// </summary>
		public void Clear( )
		{
			// SSP 9/13/02 UWG1663
			// Only notify prop change of collection being cleared if the collection
			// actually had any items.
			// So just return if the count is 0.
			//
			if ( this.Count <= 0 )
				return;

			// Unhook from all the FilterCondition objects.
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				Infragistics.Win.UltraWinGrid.FilterCondition filterCondition =
					(FilterCondition)this[i];

				filterCondition.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			}

			// Then clear the collection.
			//
			this.InternalClear( );

			// Nofity the listeners that the collection is cleared.
			//
			this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.Clear );
		}

		#endregion // Clear

		#region RemoveAt

		/// <summary>
		/// Removes an item at index position in the collection.
		/// </summary>
		/// <param name="index">The index of the FilterCondition to remove.</param>
		public void RemoveAt( int index )
		{
			this.Remove( this[ index ] );
		}

		#endregion // RemoveAt

		#region Remove

		/// <summary>
		/// Removes filterCondition object from the condition.
		/// </summary>
		/// <param name="filterCondition">The FilterCondition instance to remove from the collection.</param>
		public void Remove( Infragistics.Win.UltraWinGrid.FilterCondition filterCondition )
		{
			// Unhook from the FilterCondition object before removing
			// it from the collection.
			//
			if ( this.Contains( filterCondition ) )
				filterCondition.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalRemove( filterCondition );

			// Nofity the listeners that an item was removed.
			//
			this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.Remove );
		}

		#endregion // Remove

		#region Indexer

		/// <summary>
		/// indexer 
		/// </summary>
		public Infragistics.Win.UltraWinGrid.FilterCondition this[ int index ] 
		{
			get
			{
				return (FilterCondition)this.GetItem( index );
			}
		}
		
		#endregion // Indexer

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.FilterCondition[] array, int index)
		{
			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo

		#endregion // Public Methods
	}

}
