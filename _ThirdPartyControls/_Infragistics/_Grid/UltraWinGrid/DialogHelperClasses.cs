#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Windows.Forms;
using Infragistics.Shared;
using System.Diagnostics;

namespace Infragistics.Win.UltraWinGrid
{
	// AS 6/22/04 Moved from ColumnEditorDialog.cs
	#region FilterConditionRow
	internal class FilterConditionRow
	{
		private object compOperator = null;
		private object compOperand = null;

		internal FilterConditionRow( )
		{
		}

		public object Operator
		{
			get
			{
				if ( null == this.compOperator )
					return DBNull.Value;

				return this.compOperator;
			}
			set
			{
				this.compOperator = value;

				// SSP 2/25/03 UWG1975
				// If passed in value is not a FilterComparisionOperator instance, then convert it
				// into a FilterComparisionOperator.
				//
				if ( null != this.compOperator && ! ( this.compOperator is FilterComparisionOperator ) )
				{
					try
					{
						this.compOperator = (FilterComparisionOperator)Enum.Parse( typeof( FilterComparisionOperator ), this.compOperator.ToString( ) );
					}
					catch ( Exception )
					{
					}
				}
			}
		}

		public object Operand
		{
			get
			{
				if ( null == this.compOperand )
					return DBNull.Value;

				return this.compOperand;
			}
			set
			{
				this.compOperand = value;

				// SSP 4/7/05 BR03272
				// Convert a column to a ColumnValueListDataValue so we can display the caption
				// of the column into the cell instead of the key.
				//
				if ( this.compOperand is UltraGridColumn )
					this.compOperand = new ColumnValueListDataValue( (UltraGridColumn)this.compOperand );
			}
		}
	}


	#endregion //FilterConditionRow

	#region EmptyStringClass
	internal class EmptyStringClass : System.Object
	{
		private static EmptyStringClass val = null;

		internal EmptyStringClass( )
		{
		}

		// SSP 5/16/05 - NAS 5.2 Filter Row
		//
		public static EmptyStringClass Value
		{
			get
			{
				if ( null == val )
					val = new EmptyStringClass( );

				return val;
			}
		}

		public override string ToString( )
		{
			// SSP 5/16/05 - NAS 5.2 Filter Row
			//
			//return CustomRowFiltersDialog.EMPTY_STRING;
			return SR.GetString( "RowFilterDialogEmptyTextItem" );			
		}
	}
	#endregion //EmptyStringClass

	#region BlanksClass

    // MBS 2/20/09 - TFS14300
    // Made the class public so that the FilterUIProvider can use it
    //
    //internal class BlanksClass : System.Object   
    /// <summary>
    /// For Infragistics internal use only.
    /// </summary>
	// SSP 8/16/05 BR05382
	// Added Serializable attribute.
	// 
	[ Serializable( ) ] 
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]    
    public class BlanksClass : System.Object
	{
		// SSP 4/20/05 - NAS 5.2 Filter Row
		//
		private static BlanksClass val = null;

		internal BlanksClass( )
		{
		}

		// SSP 4/20/05 - NAS 5.2 Filter Row
		//
        /// <summary>
        /// Returns the singleton instance of the class.
        /// </summary>
		public static BlanksClass Value
		{
			get
			{
				if ( null == val )
					val = new BlanksClass( );

				return val;
			}
		}

        /// <summary>
        /// Returns the string representation of the class.
        /// </summary>
        /// <returns>The string representation of the class.</returns>
		public override string ToString( )
		{
			// SSP 5/16/05 - NAS 5.2 Filter Row
			//
			//return CustomRowFiltersDialog.BLANKS_STRING;
			return SR.GetString( "RowFilterDialogBlanksItem" );
		}

		// SSP 6/12/06 BR13422
		// Overrode GetHashCode and Equals.
		// 
		/// <summary>
		/// Returns 0
		/// </summary>
		/// <returns>0</returns>
        public override int GetHashCode( )
		{
			return 0;
		}

		// SSP 6/12/06 BR13422
		// Overrode GetHashCode and Equals.
		// 
		/// <summary>
        /// Determines whether the specified System.Object is equal to the current System.Object.
		/// </summary>
        /// <param name="o">The System.Object to compare with the current System.Object.</param>
        /// <returns>True if the specified System.Object is equal to the current System.Object</returns>
        public override bool Equals( object o )
		{
			return o is BlanksClass;
		}
	}

	#endregion //BlanksClass

	#region NonBlanksClass 

	// SSP 4/20/05 - NAS 5.2 Filter Row
	// Added NonBlanksClass class.
	//
	// SSP 8/16/05 BR05382
	// Added Serializable attribute.
	// 
	[ Serializable( ) ]
	internal class NonBlanksClass : System.Object
	{
		private static NonBlanksClass val = null;

		internal NonBlanksClass( )
		{
		}

		// SSP 5/16/05 - NAS 5.2 Filter Row
		//
		public static NonBlanksClass Value
		{
			get
			{
				if ( null == val )
					val = new NonBlanksClass( );

				return val;
			}
		}

		public override string ToString( )
		{
			return UltraGridBand.FilterNonBlanks;
		}

		// SSP 6/12/06 BR13422
		// Overrode GetHashCode and Equals.
		// 
		public override int GetHashCode( )
		{
			return ~0;
		}

		// SSP 6/12/06 BR13422
		// Overrode GetHashCode and Equals.
		// 
		public override bool Equals( object o )
		{
			return o is NonBlanksClass;
		}
	}

	#endregion // NonBlanksClass 

	#region DBNullClass
	internal class DBNullClass : System.Object
	{
		private static DBNullClass val = null;

		internal DBNullClass( )
		{
		}

		// SSP 5/16/05 - NAS 5.2 Filter Row
		//
		public static DBNullClass Value
		{
			get
			{
				if ( null == val )
					val = new DBNullClass( );

				return val;
			}
		}

		public override string ToString( )
		{
			// SSP 5/16/05 - NAS 5.2 Filter Row
			//
			//return CustomRowFiltersDialog.DBNULL_STRING;
			return SR.GetString( "RowFilterDialogDBNullItem" );
		}
	}
	#endregion //DBNullClass

	#region SpecialOperandClass
	internal class SpecialOperandClass : System.Object
	{
		internal enum SpecialOperandType 
		{
			Regex,
			Wildcard
		}

		private SpecialOperandType operandType;
		private string val;

		internal SpecialOperandClass( SpecialOperandType operandType, string val )
		{
			this.val = val;
			this.operandType = operandType;
		}

		internal SpecialOperandType OperandType
		{
			get
			{
				return this.operandType;
			}
		}

		public override string ToString( )
		{
			return null != val ? val : "";
		}
	}
	#endregion //SpecialOperandClass

	#region ColumnValueListDataValue

	// SSP 4/7/05 BR03272
	// Added ColumnValueListDataValue class. Used by custom filter dialog to
	// display the caption of a column into the cell instead of the key.
	//
	internal class ColumnValueListDataValue
	{
		internal UltraGridColumn column = null;

		internal ColumnValueListDataValue( UltraGridColumn column )
		{
			if ( null == column )
				throw new ArgumentNullException( );

			this.column = column;
		}

		public override bool Equals( object val )
		{
			ColumnValueListDataValue test = val as ColumnValueListDataValue;
			return val == this.column || null != test && test.column == this.column;
		}

		internal static string GetCaptionAsSingleLine( UltraGridColumn column )
		{
			string caption = column.Header.Caption;
			if ( null == caption )
				caption = string.Empty;

			return GridUtils.ToSingleLine( caption );
		}

		public static string ToString( UltraGridColumn column )
		{
			return "[" + GetCaptionAsSingleLine( column ) + "]";
		}

		public override string ToString( )
		{
			return ToString( this.column );
		}

		public override int GetHashCode( )
		{
			return this.column.GetHashCode( );
		}
	}

	#endregion // ColumnValueListDataValue

	// AS 6/22/04 Moved from ColumnEditorDialog.cs
	#region IndexListBox
    [System.ComponentModel.ToolboxItem(false)] // MRS 11/30/05 - BR07621
    internal class IndexListBox : ListBox
	{
		private const int			ActionFailed = -1;
		private const int			BorderWidth = 2;

        private new const int       Padding = 2;

        private Size				lastSize = new Size(0,0);
		private Size				indexAreaSize = new Size(0,0);

		#region Constructor
		public IndexListBox()
		{
			this.IntegralHeight = false;
			this.DrawMode = DrawMode.OwnerDrawFixed;
			this.SelectionMode = SelectionMode.MultiExtended;
		}
		#endregion Constructor

		#region Public Methods
		/// <summary>
		/// Add an item to the listbox.
		/// </summary>
		/// <param name="obj">Object to add</param>
		/// <returns>Index where the item was inserted.</returns>
		public int Add( object obj )
		{
			if (obj == null)
				return ActionFailed;

			return this.InternalInsertItem(this.Items.Count, obj);

		}

		public void Remove( object obj )
		{
			// remove the item
			this.Items.Remove(obj);

			// we must now recalculate the size of all the items
			this.RefreshSizeCalculations();
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Removes the item at the specified index.
		//        /// </summary>
		//        /// <param name="index">Index of the item to remove.</param>
		//#endif
		//        public void Remove( int index )
		//        {
		//            if (index < 0 || index > this.Items.Count -1)
		//                throw new ArgumentOutOfRangeException();

		//            this.Items.RemoveAt(index);

		//            // we must now recalculate the size of all the items
		//            this.RefreshSizeCalculations();
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Inserts an item at the specified location.
		//        /// </summary>
		//        /// <param name="index">Index where the item should be inserted.</param>
		//        /// <param name="obj">Item to insert.</param>
		//#endif
		//        public void InsertAt( int index, object obj )
		//        {
		//            this.InternalInsertItem( index, obj );
		//        }

		#endregion Not Used

		#endregion Public Methods

		#region Private Helper Methods
		private int InternalInsertItem( int index, object obj )
		{
			int i = index;
			if (index >= this.Items.Count)
				i = this.Items.Add(obj);
			else
				this.Items.Insert(index, obj);

			// we must now recalculate the size of all the items
			this.RefreshSizeCalculations();

			return i;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private Size GetItemSize( string text, int index, Graphics g, Font f, out Size indexAreaSize )
		private static Size GetItemSize( string text, int index, Graphics g, Font f, out Size indexAreaSize )
		{
			if (g == null || f == null)
			{
				indexAreaSize = new Size(0,0);
				return new Size(0,0);
			}

            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
			//System.Drawing.Size buttonSize = Size.Ceiling( g.MeasureString(index.ToString(), f) );
			System.Drawing.Size buttonSize = Size.Ceiling( DrawUtility.MeasureString(g, index.ToString(), f) );

			buttonSize.Height += BorderWidth * 2;
			buttonSize.Width += BorderWidth * 2;

			indexAreaSize = buttonSize;

            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
			//System.Drawing.Size textSize = Size.Ceiling( g.MeasureString(text, f) );
			System.Drawing.Size textSize = Size.Ceiling( DrawUtility.MeasureString(g, text, f) );

			return new Size(textSize.Width + buttonSize.Width, Math.Max(buttonSize.Height, textSize.Height) );
		}

		private void RefreshSizeCalculations()
		{
			if (this.Items.Count == 0)
				return;

			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			// JJD 10/23/01
			// Call CreateReferenceGraphics which will create the least 
			// disruptive graphics object based on the the permissions
			// of the assembly
			//
			//Graphics g = DrawUtility.CreateReferenceGraphics( this );
			//Graphics g = Graphics.FromHwnd( IntPtr.Zero );
			Graphics g = DrawUtility.GetCachedGraphics( this );

			int maxHeight = 0;
			int maxWidth = 0;

			this.indexAreaSize = new Size(0,0);

			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			//using (g)
			try
			{
				for (int i = 0; i < this.Items.Count; i++)
				{
					Size indexAreaSize;

					// get the size for the item
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//System.Drawing.Size itemSize = this.GetItemSize( this.Items[i].ToString(), i, g, this.Font, out indexAreaSize );
					System.Drawing.Size itemSize = IndexListBox.GetItemSize( this.Items[ i ].ToString(), i, g, this.Font, out indexAreaSize );

					// store the highest index size
					this.indexAreaSize = new Size( Math.Max(indexAreaSize.Width, this.indexAreaSize.Width), Math.Max(indexAreaSize.Height, this.indexAreaSize.Height) );

					// store the maximum height and width
					maxHeight = Math.Max( maxHeight, itemSize.Height );
					maxWidth = Math.Max( maxWidth, itemSize.Width );
				}
			}
			finally
			{
				// AS 8/12/03 optimization - Use the new caching mechanism.
				//
				DrawUtility.ReleaseCachedGraphics(g);
			}

			this.ItemHeight = Math.Max(maxHeight,1);
			this.HorizontalExtent = maxWidth + (Padding);
		}

		private void InternalDrawItem(System.Windows.Forms.DrawItemEventArgs e)
		{
			if (this.Items.Count == 0)
				return;

			Rectangle workRect = e.Bounds;

			// draw the index
			//

			// get the rect for the index button
			Rectangle indexRect = new Rectangle( workRect.Location, this.indexAreaSize );

			// fill in the index area with the system backcolor
			e.Graphics.FillRectangle(SystemBrushes.Control, indexRect);

			// draw the border
			ControlPaint.DrawBorder3D(e.Graphics, indexRect, Border3DStyle.Raised);

			// adjust the rect to not include the border rect
			indexRect.X += BorderWidth;
			indexRect.Y += BorderWidth;
			indexRect.Width -= BorderWidth * 2;
			indexRect.Height -= BorderWidth * 2;

			// create a string format for rendering the text
			StringFormat sf = new StringFormat(StringFormatFlags.FitBlackBox | StringFormatFlags.NoWrap | StringFormatFlags.NoClip);

			// center the index text
			sf.Alignment = StringAlignment.Center;

			// render the index text
            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
			//e.Graphics.DrawString(e.Index.ToString(), e.Font, SystemBrushes.ControlText, indexRect, sf );
            using( SolidBrush solidBrush = new SolidBrush(SystemColors.ControlText) )
            {
			    DrawUtility.DrawString(e.Graphics, e.Index.ToString(), e.Font, solidBrush, indexRect, sf );
            }

			// draw the item text
			//

			// adjust the work rect to not include the area used by the index area
			workRect.Width -= this.indexAreaSize.Width; 
			workRect.X += this.indexAreaSize.Width;

			Rectangle textRect = workRect;

			textRect.Width -= Padding;
			textRect.X += Padding;

			// get the text for the item
			string text = this.Items[e.Index].ToString();

            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
			//System.Drawing.Size textSize = Size.Ceiling(e.Graphics.MeasureString(text, e.Font, text.Length, sf));
			System.Drawing.Size textSize = Size.Ceiling(DrawUtility.MeasureString(e.Graphics, text, e.Font, text.Length, sf));

			textRect.Y += (textRect.Height - textSize.Height) / 2;

			// don't continue unless there's some text to render
			// AS 1/8/03 - fxcop
			// Do not compare against string.empty - test the length instead
			//if (text == string.Empty)
			if (text == null || text.Length == 0)
				return;

			// create a brush to render the foreground text and background area
			// JJD 1/23/04 - WTR530
			// Use CreateSolidBrush util to prevent memory leaks caused by creating pen using 
			//Brush foreBrush = new SolidBrush(e.ForeColor);
			//Brush backBrush = new SolidBrush(e.BackColor);
			SolidBrush foreBrush = Utilities.CreateSolidBrush(e.ForeColor);
			SolidBrush backBrush = Utilities.CreateSolidBrush(e.BackColor);

			// draw the background for the text area of the item
			e.Graphics.FillRectangle(backBrush, workRect);

			// the item's text should be aligned to the right
			sf.Alignment = StringAlignment.Near;

			// don't allow any trimming
			sf.Trimming = StringTrimming.None;

			// render the item text
            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
			//e.Graphics.DrawString(text, e.Font, foreBrush, textRect, sf);
			DrawUtility.DrawString(e.Graphics, text, e.Font, foreBrush, textRect, sf);

			// clean up
			foreBrush.Dispose();
			backBrush.Dispose();

			// draw the focus rect if needed
			if ( (e.State & DrawItemState.Focus) == DrawItemState.Focus)
				ControlPaint.DrawFocusRectangle(e.Graphics, workRect, e.ForeColor, e.BackColor);
		}

		#endregion Private Helper Methods

		#region Control Overrides
		protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
		{
			// we'll handle owner drawn fixed
			if (this.DrawMode == DrawMode.OwnerDrawFixed)
			{
				this.InternalDrawItem( e );
			}

			base.OnDrawItem(e);
		}

		protected override void OnFontChanged(EventArgs e)
		{
			// when the font changes, we need to recalculate the metrics for the control
			this.RefreshSizeCalculations();

			// call the base implementation
			base.OnFontChanged(e);
		}

		protected override void OnResize(EventArgs e)
		{

			// if the control is less wide, recalculate the sizes
			if (this.Width < this.lastSize.Width)
				this.RefreshSizeCalculations();

			// store the last size
			this.lastSize = this.Size;

			// call the base class' implementation
			base.OnResize(e);
		}
		#endregion Control Overrides

	}
	#endregion IndexListBox

	#region UltraGridColumnListBox
    [System.ComponentModel.ToolboxItem(false)] // MRS 11/30/05 - BR07621
	internal class UltraGridColumnListBox : IndexListBox
	{
		protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
		{
			DrawItemEventArgs newArgs = e;

			if (e.Index >= 0 && e.Index < this.Items.Count)
			{
				Infragistics.Win.UltraWinGrid.UltraGridColumn col = this.Items[e.Index] as Infragistics.Win.UltraWinGrid.UltraGridColumn;

				if (col != null && col.IsBound)
				{
					// if the column is bound, the forecolor of the item should be grayed out
					newArgs = new DrawItemEventArgs(e.Graphics, e.Font, e.Bounds, e.Index, e.State, SystemColors.GrayText, e.BackColor);
				}
			}

			base.OnDrawItem( newArgs );
		}
	}
	#endregion UltraGridColumnListBox

    // MBS 6/22/09 - TFS18639
    #region ErrorsClass

    /// <summary>
    /// For Infragistics internal use only.
    /// </summary>
    [Serializable()]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]    
    public class ErrorsClass
    {
        private static ErrorsClass val = null;

        internal ErrorsClass()
		{
		}

        /// <summary>
        /// Returns the singleton instance of the class.
        /// </summary>
        public static ErrorsClass Value
		{
			get
			{
				if ( null == val )
                    val = new ErrorsClass();

				return val;
			}
		}

        /// <summary>
        /// Returns the string representation of the class.
        /// </summary>
        /// <returns>The string representation of the class.</returns>
		public override string ToString( )
		{
			return UltraGridBand.FilterErrors;
		}

        /// <summary>
        /// Returns 0
        /// </summary>
        /// <returns>0</returns>
		public override int GetHashCode( )
		{
			return ~0;
		}

        /// <summary>
        /// Determines whether the specified System.Object is equal to the current System.Object.
        /// </summary>
        /// <param name="o">The System.Object to compare with the current System.Object.</param>
        /// <returns>True if the specified System.Object is equal to the current System.Object</returns>
		public override bool Equals( object o )
		{
            return o is ErrorsClass;
		}
    }
    #endregion //ErrorsClass
    //
    #region NonErrorsClass

    /// <summary>
    /// For Infragistics internal use only.
    /// </summary>
    [Serializable()]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]    
    public class NonErrorsClass
    {
        private static NonErrorsClass val = null;

        internal NonErrorsClass()
        {
        }

        /// <summary>
        /// Returns the singleton instance of the class.
        /// </summary>
        public static NonErrorsClass Value
        {
            get
            {
                if (null == val)
                    val = new NonErrorsClass();

                return val;
            }
        }

        /// <summary>
        /// Returns the string representation of the class.
        /// </summary>
        /// <returns>The string representation of the class.</returns>
        public override string ToString()
        {
            return UltraGridBand.FilterNonErrors;
        }

        /// <summary>
        /// Returns 0
        /// </summary>
        /// <returns>0</returns>
        public override int GetHashCode()
        {
            return ~0;
        }

        /// <summary>
        /// Determines whether the specified System.Object is equal to the current System.Object.
        /// </summary>
        /// <param name="o">The System.Object to compare with the current System.Object.</param>
        /// <returns>True if the specified System.Object is equal to the current System.Object</returns>
        public override bool Equals(object o)
        {
            return o is NonErrorsClass;
        }
    }
    #endregion //NonErrorsClass
}
