#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Diagnostics;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	internal class AddNewBoxPromptProvider : TextProviderBase
	{
		protected string text;


		internal AddNewBoxPromptProvider(UIElement element): base(element)
		{
			text = SR.GetString("AddNewBoxDefaultPrompt"); //"Add ...";
		}

		// SSP 3/16/06 - App Styling
		// Overrode InitElementAppearance so we can resolve the AddNewBoxPrompt role settings to the add-new box prompt.
		// 
		public override void InitElementAppearance( UIElement elem, ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			AddNewBox addNewBox = null != elem ? (AddNewBox)elem.GetContext( typeof( AddNewBox ) ) : null;
			if ( null != addNewBox )
				addNewBox.InternalResolvePromptAppearance( ref appData, ref flags );

			base.InitElementAppearance( elem, ref appData, ref flags );
		}

		internal string Text
		{
			get
			{
				return this.text;
			}
			set
			{
				this.text = value;
			}
		}

		public override String GetText(DependentTextUIElement element)
		{
			return this.text;			
		}
	}


	/// <summary>
	///		Summary description for AddNewBoxUIElement.
	/// </summary>
	public class AddNewBoxUIElement : UIElement
	{
		#region Member Variables

		// AS 1/6/04 accessibility
		private AccessibleObject		accessibleObject;

		private AddNewBoxPromptProvider promptProvider;
		
		#endregion //Member Variables

				
		internal AddNewBoxUIElement( UIElement parent ) : base( parent )
		{
			this.promptProvider	= new AddNewBoxPromptProvider( this );
		}

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b></b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="addNewBox">Associated <b>AddNewBox</b></param>
		public AddNewBoxUIElement( UIElement parent, Infragistics.Win.UltraWinGrid.AddNewBox addNewBox ) : this( parent )
		{
			this.InitializeAddNewBox(addNewBox);
		}

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type,object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
        /// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
		protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( typeof( DataAreaUIElement ) == type ||
				typeof( GroupByBoxUIElement ) == type )
				return false;

			if ( null != contexts )
			{
				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						if ( context is UltraGridRow || 
							 context is UltraGridCell ||
							 context is RowScrollRegion ||
							 context is ColScrollRegion || 
							 context is GroupByBox )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch

		/// <summary>
		/// The AddNewBox object represents the AddNew Box interface for entering new data rows into the grid.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a grid is being used to display a flat recordset, the conventional approach for adding data has been to place an empty row at the bottom of the grid. New data is entered into this row and appended to the data source, then the row reserved for new data entry is cleared and moved down to appear below the newly added row. However, when working with a hierarchical recordset, this metaphor is no longer effective. Multiple bands of data are represented as distinct groups of rows, and which group of rows receives the new data is significant. Simply adding new data to the last row in a band will not position the new record correctly with respect to the band's parent recordset.</p>
		/// <p class="body">To effectively add new data to a hierarchical recordset, the UltraGrid implements a new interface called the "AddNew Box." The AddNew Box displays one or more buttons that are used to trigger the addition of new data. The number of buttons corresponds to the number of hierarchical bands displayed. Each band has its own AddNew button, and connecting lines link the buttons, illustrating a hierarchical relationship that mirrors that of the data.</p>
		/// <p class="body">To use the AddNew Box, you first set focus to a row or cell in the band to which you want to add data. You should determine where in the hierarchy you want the record to appear, then select a record that corresponds to that location. You then click the AddNew button for the band you want to contain the new data, and an empty data entry row appears in the band a the point you selected. For example, if you have a Customers/Orders hierarchy and you wanted to add data for a new order, you would first locate the customer to whom the order belonged, select that customer's record (or one of that customer's existing order records) and click the AddNew button for the Orders band. A blank row would appear below any existing orders that were displayed for the customer.</p>
		/// <p class="body">The AddNewBox object contains properties that control the various attributes of the AddNew Box interface. For example, you can use the <b>Hidden</b> property of the AddNewBox object to selectively display or hide the interface, thus enabling or disabling the user's ability to add new data. You can also use this object to control the appearance of the AddNew buttons, and specify other formatting features.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.AddNewBox AddNewBox
		{
			get
			{
				return ((Infragistics.Win.UltraWinGrid.UltraGridUIElement)this.parentElement).Layout.AddNewBox;
			}
		}

		internal void InitializeAddNewBox( Infragistics.Win.UltraWinGrid.AddNewBox addNewBox )
		{
			this.PrimaryContext = addNewBox;
		}



		/// <summary>
        /// Returns an object of requested type that relates to the element or null.
		/// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
		// AS - 10/31/01
		//public virtual object GetContext( Type type )
		public override object GetContext( Type type, bool checkParentElementContexts )
		{
			// Check for the AddNewBox type since this is not in the
			// Contexts collection so we can create the cell objects
			// lazily
			//
			if ( typeof(Infragistics.Win.UltraWinGrid.AddNewBox) == type )
					return this.PrimaryContext;

			// SSP 9/7/01
			if ( typeof(Infragistics.Win.UltraWinGrid.SpecialBoxBase) == type )
				return this.PrimaryContext;

			// Call the base class implementation
			//			
			return base.GetContext( type, checkParentElementContexts );
		
		}

		
        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
		/// <p class="body">The border style of the AddNew box buttons can be set by the <b>ButtonBorderStyle</b> property.</p>
		/// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
		/// </remarks>        
        public override UIElementBorderStyle BorderStyle
		{
			get
			{	
				return null != this.AddNewBox && 
					UIElementBorderStyle.Default != this.AddNewBox.BorderStyle ?
					this.AddNewBox.BorderStyle :
					UIElementBorderStyle.Inset; // Default for AddNewBox is Inset

				
			}
		}

		
		/// <summary>
        /// Initialize the appearance structure for this element
		/// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
		protected override void InitAppearance ( ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			this.AddNewBox.ResolveAppearance( ref appearance, requestedProps );			
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// get the collection 
			//
			Infragistics.Win.UIElementsCollection oldElements = this.ChildElements;
			
			this.childElementsCollection = null;

			AddNewBox addNewBox = this.AddNewBox;
			UltraGridLayout layout = null != addNewBox ? addNewBox.Layout : null;

			if ( addNewBox != null )
			{
				Rectangle rectInsideBorders = this.RectInsideBorders;
				System.Drawing.Size promptSize = addNewBox.PromptSize;
				System.Drawing.Size btnSize = new Size( addNewBox.GetButtonWidth("WWWWy"), addNewBox.ButtonHeight );

				// SSP 3/27/06 - App Styling
				// Changed the ButtonStyleResolved property into a method because we need to pass in the context
				// of a band.
				// 
				//int buttonBorderWidth	= layout.GetButtonThickness( addNewBox.ButtonStyleResolved );	
				int buttonBorderWidth	= layout.GetButtonThickness( addNewBox.GetButtonStyleResolved( null ) );

				int btnHeight = btnSize.Height + 3 + ( 2 * buttonBorderWidth );
				
				System.Drawing.Rectangle remainderRect = this.ClipRectChildren;
				if ( promptSize.Height > 0 && promptSize.Width > 0 )
				{
					System.Drawing.Rectangle promptRect = new Rectangle();
					promptRect.X = remainderRect.Left + 4;

					if ( this.AddNewBox != null &&
						this.AddNewBox.Style == AddNewBoxStyle.Full )
						promptRect.Y = remainderRect.Top + 4;
					else
						//	compact mode: situate the top closer to the data area
						promptRect.Y = remainderRect.Top + 2;

					
					promptRect.Width = promptSize.Width + 2;
					promptRect.Height = promptSize.Height + 2;
					
					if ( btnHeight > promptSize.Height )
						promptRect.Offset( 0, btnHeight - promptSize.Height );

					// add the text element
					//
					DependentTextUIElement textElement = (DependentTextUIElement)AddNewBoxUIElement.ExtractExistingElement( oldElements, typeof(DependentTextUIElement), true );

					if ( null == textElement)
					{
						textElement = new DependentTextUIElement( this, (IUIElementTextProvider)this.promptProvider );

						// SSP 3/20/06 - App Styling
						// 
						textElement.SetUIRole( StyleUtils.GetRole( layout, StyleUtils.Role.AddNewBoxPrompt ) );
					}

					this.promptProvider.Text = addNewBox.Prompt;
					textElement.Rect = promptRect;

					this.ChildElements.Add( textElement );
					remainderRect.X = promptRect.Right + 4;
				}

				if ( layout.SortedBands != null )
				{
					System.Drawing.Rectangle buttonRect = remainderRect;
					System.Drawing.Rectangle lastButtonRect = new Rectangle();
					System.Drawing.Rectangle connectorRect  = new Rectangle();
					UltraGridBand lastBand = null;
					// SSP 5/31/02 UWG1152
					// Use the new MaxBandDepthEnforced property instead of MAX_BAND_DEPTH.
					// 
					//Infragistics.Win.UIElement[] connectors = new UIElement[UltraGridBand.MAX_BAND_DEPTH];
					Infragistics.Win.UIElement[] connectors = new UIElement[ layout.MaxBandDepthEnforced ];
					int bandLevel = 0;
					
				
					if ( this.AddNewBox != null &&
						this.AddNewBox.Style == AddNewBoxStyle.Full )
						buttonRect.Y += System.Math.Max( AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET, promptSize.Height - btnHeight );
					else
						//	compact mode: situate the top closer to the data area
						//	note that with a large font for the caption, this won't matter
						buttonRect.Y += System.Math.Max( AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET_COMPACT, promptSize.Height - btnHeight );


					for ( int i=0; i < layout.SortedBands.Count; ++i )
					{
						Infragistics.Win.UltraWinGrid.UltraGridBand band = layout.SortedBands[i];
						int btnWidth;
						
						// if the band is hidden then continue
						//				
						if ( band.HiddenResolved )
							continue;

						//RobA 8/24/01 I think we should be using the resolved caption
						//string btnCaption = band.AddButtonCaption;
						string btnCaption = band.AddButtonCaptionResolved;
						
						if ( btnCaption.Length > 0 )
							btnWidth = this.AddNewBox.GetButtonWidth(btnCaption);
						else
							btnWidth = btnSize.Width;
						// set up the button rect to allow enough space for the caption
						
						buttonRect.Width = System.Math.Max( btnWidth, btnSize.Width ) + 8 + (2 * buttonBorderWidth );
						buttonRect.Height = btnHeight;

						// get the bands level which be be used to index into the
						// pConnectors array
						bandLevel = band.HierarchicalLevel;

						// add button connector
						//
						if ( lastBand != null )
						{
							System.Drawing.Rectangle parentRect;

							Debug.Assert( bandLevel > 0, "Band 0 can not have a sibling band, AddNewBoxUIElement::positionChildElements"); 
							Debug.Assert( connectors[bandLevel - 1] != null, "Band level array not initialized in AddNewBoxUIElement::positionChildElements"); 

							if ( (connectors[bandLevel - 1]) == null )
								continue;

							parentRect = connectors[bandLevel - 1].Rect;
							// check if this band is a sibling of the last band and
							// adjust the location of the button accordingly
							//
							if ( lastBand.ParentBand == band.ParentBand )
							{
								Debug.Assert( connectors[bandLevel] != null, "Band level array not initialized(2) in AddNewBoxUIElement::positionChildElements"); 

								System.Drawing.Rectangle siblingRect = connectors[bandLevel].Rect;
								if ( addNewBox != null && addNewBox.Style == AddNewBoxStyle.Full )
									buttonRect.Offset( siblingRect.Left - buttonRect.Left,
										lastButtonRect.Bottom + AddNewBox.ADDNEWBTN_MIN_SPACING_VERT - buttonRect.Top);
								else
									//	compact mode: no hierarchical look for the buttons, just straight accross
									buttonRect.Offset( lastButtonRect.Width + AddNewBox.ADDNEWBTN_MIN_SPACING_HORZ_COMPACT, 0 );
							}

								//	this band is NOT a sibling of the last band
							else if ( bandLevel < lastBand.HierarchicalLevel )
							{
								Debug.Assert( connectors[bandLevel] != null, "Band level array not initialized(3) in AddNewBoxUIElement::positionChildElements");
 
								System.Drawing.Rectangle lastAtThisLevel = connectors[bandLevel].Rect;

								if ( addNewBox != null && addNewBox.Style == AddNewBoxStyle.Full )
									buttonRect.Offset( lastAtThisLevel.Left - buttonRect.Left,
										lastButtonRect.Bottom + AddNewBox.ADDNEWBTN_MIN_SPACING_VERT - buttonRect.Top );

								else
									//	compact mode: no hierarchical look for the buttons, just straight accross
									buttonRect.Offset( lastButtonRect.Width + AddNewBox.ADDNEWBTN_MIN_SPACING_HORZ_COMPACT, 0 );
							}

							else
							{
								if ( addNewBox != null && addNewBox.Style == AddNewBoxStyle.Full )
									buttonRect.Offset( lastButtonRect.Width + AddNewBox.ADDNEWBTN_MIN_SPACING_HORZ,
										AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET );
								else
									buttonRect.Offset( lastButtonRect.Width + AddNewBox.ADDNEWBTN_MIN_SPACING_HORZ_COMPACT, 0 );
							}

							if ( addNewBox != null && addNewBox.Style == AddNewBoxStyle.Full )
							{
								connectorRect.Y		 = parentRect.Bottom;
								connectorRect.X		 = parentRect.Right - AddNewBox.ADDNEWBTN_CONNECTOR_LEFT_INDENT;
								connectorRect.Width	 = buttonRect.Left - connectorRect.Left;
								connectorRect.Height = (buttonRect.Bottom - AddNewBox.ADDNEWBTN_CONNECTOR_BOTTOM_INDENT) - connectorRect.Top;
							}

							else
							{
								//	the button connectors are drawn differently when in compact mode;
								//	there is no hierarchy, so the button connectors go from the right edge
								//	of Bands(n) to the left edge of Bands(n+1), vertically centered
								//
								//	The connectors are actually drawn as borders, and the only border we are
								//	concerned with in compact mode is the bottom
								connectorRect.Y			= buttonRect.Top + ( buttonRect.Height / 2 ) -1;
								connectorRect.X			= lastButtonRect.Right;
								connectorRect.Width		= buttonRect.Left - connectorRect.Left;
								connectorRect.Height	= 1;
							}
							// add the button connector
							//
							ButtonConnectorUIElement buttonConnectorElement = (ButtonConnectorUIElement)AddNewBoxUIElement.ExtractExistingElement( oldElements, typeof(ButtonConnectorUIElement), true );
							
							if ( null == buttonConnectorElement )
							{
								buttonConnectorElement =  new ButtonConnectorUIElement( this );
							}
							buttonConnectorElement.Rect = connectorRect;

							//RobA 8/22/01 UWG212
							//Make sure the button connectors don't overlap AddNewBox's border
							//
							if ( connectorRect.Right > rectInsideBorders.Right && connectorRect.Left < rectInsideBorders.Right )
							{
								Rectangle workRect = connectorRect;
								workRect.Width = (rectInsideBorders.Right - connectorRect.Left);
								buttonConnectorElement.Rect = workRect;
							}							

							this.ChildElements.Add( buttonConnectorElement );
						}
						// add the button and save a pointer to the uielement in the array
						//
						AddNewRowButtonUIElement rowButtonElement = (AddNewRowButtonUIElement)AddNewBoxUIElement.ExtractExistingElement( oldElements, typeof(AddNewRowButtonUIElement), true );

						if ( null == rowButtonElement )
						{
							rowButtonElement = new AddNewRowButtonUIElement( this, band );
						}
							// SSP 5/29/02 UWG1143
							// If we already had a row button element in the old child elements
							// collection, then we still need to initialze the button with the
							// right band.
							//
						else
							rowButtonElement.Initialize( band );
						

						//buttonRect.Width = buttonRect.X + ( rect.Right - rect.Left - buttonBorderWidth ); 
						//buttonRect.Width -=  buttonRect.Left - rect.Right -  ; 

						rowButtonElement.Rect = buttonRect;

						//RobA 8/22/01 UWG212
						//Make sure the buttons don't overlap AddNewBox's border
						//
						if ( buttonRect.Right > rectInsideBorders.Right && buttonRect.Left < rectInsideBorders.Right )
						{
							Rectangle workRect = buttonRect;
							workRect.Width = (rectInsideBorders.Right - buttonRect.Left);
							rowButtonElement.Rect = workRect;
						}

						//ROBA 8/22/01
						//place text in AddNewRowButtonUIElement.PostionChildElements
						//rowButtonElement.Text = band.AddButtonCaption;

						rowButtonElement.Text = band.AddButtonCaptionResolved;

						// SSP 1/2/02
						// Moved the logical to enable and disable add new buttons
						// inside the AddNewRowButtonUIElement's overridden Enabled
						// property.
						//
						


						this.ChildElements.Add( rowButtonElement );
						
						connectors[bandLevel] = rowButtonElement;

						lastBand = band;
						lastButtonRect = buttonRect;
					}
					// re-select the old font
					//
				}				
			}
				
		}
		
		// AS 1/6/04 Accessibility
		#region IsAccessibleElement
		/// <summary>
		/// Indicates if the element supports accessibility.
		/// </summary>
		public override bool IsAccessibleElement
		{
			get { return true; }
		}
		#endregion //IsAccessibleElement

		#region AccessibilityInstance
		/// <summary>
		/// Returns the accessible object associated with the element.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note</b> Derived elements that plan to return an accessible object must override 
		/// the <see cref="IsAccessibleElement"/> member.</p>
		/// </remarks>
		public override AccessibleObject AccessibilityInstance
		{
			get 
			{ 
				if (this.accessibleObject == null)
					this.accessibleObject = this.AddNewBox.Layout.Grid.CreateAccessibilityInstance(this);

				return this.accessibleObject; 
			}
		}
		#endregion //AccessibilityInstance

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.AddNewBox.Layout, StyleUtils.Role.AddNewBox );
			}
		}

		#endregion // UIRole

	}	

	
}
