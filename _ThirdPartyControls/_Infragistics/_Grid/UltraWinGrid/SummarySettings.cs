#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Reflection;
using System.Drawing.Design;

using Infragistics.Shared.Serialization;

// SSP 6/30/04 - UltraCalc
//
using Infragistics.Win.CalcEngine;
using System.Collections.Generic;

// SSP 4/23/02
// Added SummarySettings class for Summary Rows Feature
//

namespace Infragistics.Win.UltraWinGrid
{
	// SSP 8/15/02 UWG1435
	// Made SummarySettings a KeyedSubObjectBase rather than SubObjectBase
	//

	/// <summary>
	/// SummarySettings object represents a summary. Objects of this type are also referred to as summaries.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// A <b>SummarySettings</b> instance contains information on what formula to
	/// use for summary calculation. A <see cref="SummaryValue"/> instance contains
	/// the result of a summary calculation. You can access summary values of a row collection
	/// using the RowsCollection's <see cref="RowsCollection.SummaryValues"/> property.
	/// You can add summaries in code using the Band's 
	/// <see cref="UltraGridBand.Summaries"/> property.
	/// </p>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Summaries"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummaryValue"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettingsCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummaryValuesCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.SummaryValues"/>
	/// </remarks>
	[ Serializable() ]
	// SSP 8/10/04 - Implemented design time serialization of summaries
	// Added the following converter attribute.
	//
	[ TypeConverter( typeof( SummarySettings.SummarySettingsConverter ) ) ]
	// SSP 8/15/02 UWG1435
	//
	//public class SummarySettings : Infragistics.Shared.SubObjectBase, ISerializable
	public class SummarySettings : Infragistics.Shared.KeyedSubObjectBase, ISerializable
		// SSP 6/30/04 - UltraCalc
		//
		, Infragistics.Win.CalcEngine.IFormulaProvider
	{
		#region Private Vars

		private SummarySettingsCollection parentCollection = null;
		
		// SSP 8/20/02
		// Use appearance holder instead of appearance.
		//
		//private Appearance			appearance			= null;
		private AppearanceHolder	appearanceHolder    = null;

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GroupBySummaryValueAppearance.
		//
		private AppearanceHolder groupBySummaryValueAppearanceHolder = null;
		
		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
		// SummaryDisplayArea property.
		//
		//private bool				hidden				= false;
		//private bool				displayInGroupBy	= true;

		private UltraGridColumn		sourceColumn		= null;
		private SummaryType			summaryType;
		private ICustomSummaryCalculator  customSummaryCalculator = null;
		private string				displayFormat		= null;

		// SSP 1/3/06 BR08523
		// We have DisplayFormat property, but we don't have the associated format provider property.
		// So added DisplayFormatProvider property.
		// 
		private IFormatProvider		displayFormatProvider = null;

		private SummaryPosition		summaryPosition		= SummaryPosition.UseSummaryPositionColumn;
		private UltraGridColumn		summaryPositionColumn	= null;
		private int					lines				= 1;

		private int					summaryCalculationsVersion = 0;

		// SSP 7/16/02
		//
		private SerializedColumnID serializedSourceColumnId = null;
		private SerializedColumnID serializedSummaryPositionColumnId = null;
		private string			   serializedSourceColumnBandKey   = null;
		// We need index as well since the key does not uniquely define a band
		//
		private int				   serializedSourceColumnBandIndex = -1;

		// SSP 6/30/04 - UltraCalc
		//
		private FormulaHolder formulaHolder = null;

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		// SSP 10/21/04
		// Initialize the var to Default, not False.
		//
		//private DefaultableBoolean showCalculatingText = DefaultableBoolean.False;
		private DefaultableBoolean showCalculatingText = DefaultableBoolean.Default;

		// SSP 3/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		private SummaryDisplayAreas summaryDisplayArea = SummaryDisplayAreas.Default;

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		private string toolTipText = null;

        // MRS 5/21/2009 - TFS17817
        internal SummarySettings sourceSummarySettings = null;

		#endregion // Private Vars

		#region Constructor

		// SSP 8/9/04 - Implemented design time serialization of summaries
		//
        /// <summary>
        /// Constructor. For internal use only. Use the overloads of <see cref="SummarySettingsCollection.Add( 
        /// SummaryType, ICustomSummaryCalculator, UltraGridColumn, SummaryPosition, UltraGridColumn )"/> 
        /// method instead to add summaries.
        /// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="summaryType">summaryType</param>
        /// <param name="formula">formula</param>
        /// <param name="sourceColumnKey">sourceColumnKey</param>
        /// <param name="sourceColumnRelativeIndex">sourceColumnRelativeIndex</param>
        /// <param name="isSourceColumnBound">isSourceColumnBound</param>
        /// <param name="sourceColumnBandKey">sourceColumnBandKey</param>
        /// <param name="sourceColumnBandIndex">sourceColumnBandIndex</param>
        /// <param name="summaryPosition">summaryPosition</param>
        /// <param name="summaryPositionColumnKey">summaryPositionColumnKey</param>
        /// <param name="summaryPositionColumnRelativeIndex">summaryPositionColumnRelativeIndex</param>
        /// <param name="isSummaryPositionColumnBound">isSummaryPositionColumnBound</param>
        [ EditorBrowsable( EditorBrowsableState.Never ) ]
		public SummarySettings( 
			string key, 
			SummaryType summaryType, 
			string formula,
			string sourceColumnKey, 
			int sourceColumnRelativeIndex,
			bool isSourceColumnBound,
			string sourceColumnBandKey,
			int sourceColumnBandIndex,
			SummaryPosition summaryPosition,
			string summaryPositionColumnKey,
			int summaryPositionColumnRelativeIndex,
			bool isSummaryPositionColumnBound ) : base( key )
		{
			this.summaryType = summaryType;
			this.Formula = formula;
			this.serializedSourceColumnId = 
				null != sourceColumnKey && sourceColumnKey.Length > 0 || sourceColumnRelativeIndex >= 0
				? new SerializedColumnID( 
				sourceColumnRelativeIndex, isSourceColumnBound, sourceColumnKey )
				: null;
			this.serializedSourceColumnBandIndex = sourceColumnBandIndex;
			this.serializedSourceColumnBandKey = sourceColumnBandKey;
			this.summaryPosition = summaryPosition;
			this.serializedSummaryPositionColumnId = 
				null != summaryPositionColumnKey && summaryPositionColumnKey.Length > 0 || summaryPositionColumnRelativeIndex >= 0
				? new SerializedColumnID(
				summaryPositionColumnRelativeIndex, isSummaryPositionColumnBound, summaryPositionColumnKey )
				: null;
		}

		// SSP 8/10/04 - Implemented design time serialization of summaries
		//
		/// <summary>
        /// Constructor. For internal use only. Use the overloads of <see cref="SummarySettingsCollection.Add( 
        /// SummaryType, ICustomSummaryCalculator, UltraGridColumn, SummaryPosition, UltraGridColumn )"/> method instead
        /// to add summaries.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public SummarySettings( )
		{
			// SSP 8/19/04 
			// Default the summary type to Formula and summary position to Left for the parameter
			// constructor which should be used at design time. This way the Formula summaries
			// are more user friendly. Before formula summaries required the user to select the
			// summary type to Formula and set either the summary position to one of Left, Center 
			// and Right or set the SummaryPositionColumn. Now it will show up by default if we
			// default the following properties to what they are being set below.
			// 
			this.summaryType = SummaryType.Formula;
			this.summaryPosition = SummaryPosition.Left;
		}

		internal SummarySettings( 
			// SSP 8/15/02
			// Made this a keyed object.
			//
			string key,
			SummarySettingsCollection parentCollection,
			SummaryType summaryType, 
			ICustomSummaryCalculator customSummaryCalculator, 
			UltraGridColumn sourceColumn, 
			SummaryPosition summaryPosition,
			UltraGridColumn summaryPositionColumn,
			// SSP 8/2/04 - UltraCalc
			// Added formula parameter.
			//
			string formula
			) : base( key )
		{
			this.Initialize( 
				parentCollection,
				summaryType,
				customSummaryCalculator,
				sourceColumn,
				summaryPosition,
				summaryPositionColumn,
				// SSP 8/2/04 - UltraCalc
				// Added formula parameter.
				//
				formula );
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal SummarySettings( SerializationInfo info, StreamingContext context )
		protected SummarySettings( SerializationInfo info, StreamingContext context )
		{
			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			//
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;
						// SSP 8/15/02 UWG1435
						// Made this a keyed object.
						//
					case "Key":
						//this.Key = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Key = (string)Utils.DeserializeProperty( entry, typeof(string), this.Key );
						//this.Key = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					case "Hidden":
						//this.hidden = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						//this.hidden = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						// Use the DeserializeProperty static method to de-serialize properties instead.
						// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
						// Added ability to display summaries in group-by rows aligned with columns, 
						// summary footers for group-by row collections, fixed summary footers and
						// being able to display the summary on top of the row collection.
						// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
						// SummaryDisplayArea property.
						//
						//this.hidden = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.hidden );
						this.HiddenNoWarns = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.HiddenNoWarns );
						break;
					case "DisplayFormat":
						//this.displayFormat = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.displayFormat = (string)Utils.DeserializeProperty( entry, typeof(string), this.displayFormat );
						//this.displayFormat = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					case "Lines":
						//this.lines = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.lines = (int)Utils.DeserializeProperty( entry, typeof(int), this.lines );
						//this.lines = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
					case "SummaryPosition":
						//this.summaryPosition = (SummaryPosition)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.summaryPosition = (SummaryPosition)Utils.DeserializeProperty( entry, typeof(SummaryPosition), this.summaryPosition );
						//this.summaryPosition = (SummaryPosition)Utils.ConvertEnum(entry.Value, this.summaryPosition);
						break;

						// SSP 8/20/02
						// Use appearance holder instead of appearance.
						//
						
					case "AppearanceHolder":
						this.appearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						break;
					case "SummaryType":
						//this.summaryType = (SummaryType)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.summaryType = (SummaryType)Utils.DeserializeProperty( entry, typeof(SummaryType), this.summaryType );
						//this.summaryType = (SummaryType)Utils.ConvertEnum(entry.Value, this.summaryType);
						break;
					case "SourceColumn":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.serializedSourceColumnId = (SerializedColumnID)Utils.DeserializeProperty( entry, typeof(SerializedColumnID), null );
						//this.serializedSourceColumnId = (SerializedColumnID)entry.Value;
						break;
					case "SummaryPositionColumn":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.serializedSummaryPositionColumnId = (SerializedColumnID)Utils.DeserializeProperty( entry, typeof(SerializedColumnID), null );
						//this.serializedSummaryPositionColumnId = (SerializedColumnID)entry.Value;
						break;
					case "SourceColumnBandKey":
						//this.serializedSourceColumnBandKey = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.serializedSourceColumnBandKey = (string)Utils.DeserializeProperty( entry, typeof(string), this.serializedSourceColumnBandKey );
						//this.serializedSourceColumnBandKey = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					case "SourceColumnBandIndex":
						//this.serializedSourceColumnBandIndex = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.serializedSourceColumnBandIndex = (int)Utils.DeserializeProperty( entry, typeof(int), this.serializedSourceColumnBandIndex );
						//this.serializedSourceColumnBandIndex = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;
						// SSP 1/24/03 UWG1950
						// Deserialize DisplayInGroupBy property.
						//
					case "DisplayInGroupBy":
						// Use the DeserializeProperty static method to de-serialize properties instead.
						// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
						// Added ability to display summaries in group-by rows aligned with columns, 
						// summary footers for group-by row collections, fixed summary footers and
						// being able to display the summary on top of the row collection.
						// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
						// SummaryDisplayArea property.
						//
						//this.displayInGroupBy = (bool)Utils.DeserializeProperty( entry, typeof(bool), true );
						this.DisplayInGroupByNoWarns = (bool)Utils.DeserializeProperty( entry, typeof(bool), true );
						break;
						// SSP 8/2/04 - UltraCalc
						// Added Formula property.
						//
					case "Formula":
						this.Formula = (string)Utils.DeserializeProperty( entry, typeof( string ), this.Formula );
						break;
						// MRS 9/14/04
						// Added a property to control whether or not the grid displays 
						// a string in a cell while calculating
						//
					case "ShowCalculatingText":
						this.showCalculatingText = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.showCalculatingText );
						break;
						// SSP 3/14/05 - NAS 5.2 Extension of Summaries Functionality
						//
					case "SummaryDisplayArea":
						this.summaryDisplayArea = (SummaryDisplayAreas)Utils.DeserializeProperty( entry, typeof( SummaryDisplayAreas ), this.summaryDisplayArea );
						break;
					// SSP 7/27/05 - Header, Row, Summary Tooltips
					// 
					case "ToolTipText":
						this.toolTipText = (string)Utils.DeserializeProperty( entry, typeof( string ), this.toolTipText );
						break;
                    // MRS 7/30/2009 - TFS20044
                    //
                    // ---------------------------------------------------------------------
                    case "CustomSummaryCalculator":                        
                        this.customSummaryCalculator = (ICustomSummaryCalculator)Utils.DeserializeProperty(entry, typeof(ICustomSummaryCalculator), this.customSummaryCalculator);
                        break;
                    // ---------------------------------------------------------------------


					default:
						Debug.Assert( false, "Invalid entry in SummarySettings de-serialization ctor" );
						break;
				}
			}
		}

		// SSP 8/10/04 - Implemented design time serialization of summaries
		// 
		internal void Initialize( SummarySettingsCollection parentCollection )
		{
			this.parentCollection = parentCollection;
		}
		
		internal void Initialize( SummarySettingsCollection parentCollection,
			SummaryType summaryType, 
			ICustomSummaryCalculator customSummaryCalculator, 
			UltraGridColumn sourceColumn, 
			SummaryPosition summaryPosition,
			UltraGridColumn summaryPositionColumn,
			// SSP 8/2/04 - UltraCalc
			// Added formula parameter.
			//
			string formula )
		{
			if ( null == parentCollection )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_43"), Shared.SR.GetString("LE_ArgumentNullException_347") );

			if ( !Enum.IsDefined( typeof( SummaryType ), summaryType ) )
				throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_348"), (int)summaryType, typeof( SummaryType ) );

			if ( !Enum.IsDefined( typeof( SummaryPosition ), summaryPosition ) )
				throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_349"), (int)summaryPosition, typeof( SummaryPosition ) );

			if ( SummaryType.Custom == summaryType && null == customSummaryCalculator )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_350"), Shared.SR.GetString("LE_ArgumentException_351") );

			// SSP 8/2/04 - UltraCalc
			// Source column can be null if this is a formula summary.
			//
			//if ( null == sourceColumn )
			if ( null == sourceColumn && SummaryType.Formula != summaryType )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_352"), Shared.SR.GetString("LE_ArgumentNullException_103") );

			Debug.Assert( null != parentCollection.Band, "parentCollection.Band is null." );

			if (
				// SSP 8/2/04 - UltraCalc
				// Formula summaries can have null source columns so check for sourceColumn being null.
				//
				null != sourceColumn &&
				null != parentCollection.Band &&
				sourceColumn.Band != parentCollection.Band &&
				!sourceColumn.Band.IsDescendantOfBand( parentCollection.Band ) )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_352"), Shared.SR.GetString("LE_ArgumentNullException_353") );

			this.parentCollection = parentCollection;
			this.summaryType = summaryType;
			this.customSummaryCalculator = customSummaryCalculator;
			this.sourceColumn = sourceColumn;
			this.summaryPosition = summaryPosition;
			this.summaryPositionColumn = summaryPositionColumn;

			// SSP 8/2/04 - UltraCalc
			// Added formula parameter.
			//
			this.Formula = formula;
		}

		#endregion // Constructor

		#region Base Overrides

		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			// SSP 8/15/02 UWG1435
			//
			if ( this.ShouldSerializeKey( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Key", this.Key );
				//info.AddValue( "Key", (string)this.Key );
			}

			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
			// SummaryDisplayArea property.
			//
			

			if ( this.ShouldSerializeDisplayFormat( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "DisplayFormat", this.DisplayFormat );
				//info.AddValue( "DisplayFormat", (string)this.DisplayFormat );
			}

			if ( this.ShouldSerializeLines( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Lines", this.Lines );
				//info.AddValue( "Lines", (int)this.Lines );
			}

			if ( this.ShouldSerializeSummaryPosition( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SummaryPosition", this.SummaryPosition );
				//info.AddValue( "SummaryPosition", (int)this.SummaryPosition );
			}

			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
			// SummaryDisplayArea property.
			//
			

			if ( this.ShouldSerializeAppearance( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AppearanceHolder", this.appearanceHolder );
				//info.AddValue( "Appearance", this.Appearance );
			}

			// SSP 8/2/04 - UltraCalc
			// Added Formula property.
			//
			if ( this.ShouldSerializeFormula( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Formula", this.Formula );
				//info.AddValue( "Appearance", this.Appearance );
			}

			// MRS 9/14/04
			// Added a property to control whether or not the grid displays 
			// a string in a cell while calculating
			//
			if ( this.ShouldSerializeShowCalculatingText( ) )
			{
				Utils.SerializeObjectProperty( info, "ShowCalculatingText", this.ShowCalculatingText );
			}

			// SSP 3/14/05 - NAS 5.2 Extension of Summaries Functionality
			//
			if ( this.ShouldSerializeSummaryDisplayArea( ) )
			{
				Utils.SerializeObjectProperty( info, "SummaryDisplayArea", this.SummaryDisplayArea );
			}

			// SSP 7/27/05 - Header, Row, Summary Tooltips
			// 
			if ( this.ShouldSerializeToolTipText( ) )
			{
				Utils.SerializeProperty( info, "ToolTipText", this.toolTipText );
			}

			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "SummaryType", this.SummaryType );
			//info.AddValue( "SummaryType", (int)this.SummaryType );

			// JAS 5/19/05 BR04140 - It is possible that the bodies of the following two if() blocks
			// will both execute.  That causes a problem because they both try to serialize out the 
			// same two properties, SourceColumn and SourceColumnBandKey.  We need to make sure that
			// those properties only get serialized once otherwise an exception is thrown.
			//
			bool addedSourceColumnAndSourceColumnBandKey = false;

			// If this method get's called on an deserialized object (which I remember taking place
			// on SortedColumnCollection), then serialize back the serializedSourceColumnId and
			// serializedSourceColumnBandKey.
			//
			if ( null != this.serializedSourceColumnId && null != this.serializedSourceColumnBandKey )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SourceColumn", this.serializedSourceColumnId );
				Utils.SerializeProperty( info, "SourceColumnBandKey", this.serializedSourceColumnBandKey );
				//info.AddValue( "SourceColumn", this.serializedSourceColumnId );
				//info.AddValue( "SourceColumnBandKey", this.serializedSourceColumnBandKey );

				// JAS 5/19/05 BR04140 - See comments above.
				addedSourceColumnAndSourceColumnBandKey = true;
			}

			if ( null != this.SourceColumn )
			{
				Debug.Assert( null != this.SourceColumn.Band && null != this.SourceColumn.Band.Key,
					"No band off the source column !" );

				if ( null != this.SourceColumn.Band && null != this.SourceColumn.Band.Key )
				{
					SerializedColumnID colid = this.SourceColumn.Band.GetSerializedColumnId( this.SourceColumn );
				
					Debug.Assert( null != colid, "GetSerializedColumnId returned null." );
					if ( null != colid )
					{
						// JAS 5/19/05 BR04140 - See comments above.
						if( ! addedSourceColumnAndSourceColumnBandKey )
						{
							// JJD 8/20/02
							// Use SerializeProperty static method to serialize properties instead.
							Utils.SerializeProperty( info, "SourceColumn", colid );
							Utils.SerializeProperty( info, "SourceColumnBandKey", this.SourceColumn.Band.Key );
						}
						Utils.SerializeProperty( info, "SourceColumnBandIndex", this.SourceColumn.Band.Index );
						//info.AddValue( "SourceColumn", colid );					
						//info.AddValue( "SourceColumnBandKey", this.SourceColumn.Band.Key );
						//info.AddValue( "SourceColumnBandIndex", this.SourceColumn.Band.Index );
					}
				}
			}
			
			// If this method get's called on an deserialized object (which I remember taking place
			// on SortedColumnCollection), then serialize back the serializedSourceColumnId and
			// serializedSourceColumnBandKey.
			//
			if ( null != this.serializedSummaryPositionColumnId )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SummaryPositionColumn", this.serializedSummaryPositionColumnId );
				//info.AddValue( "SummaryPositionColumn", this.serializedSummaryPositionColumnId );
			}
			else if ( this.ShouldSerializeSummaryPositionColumn( ) &&
				this.SummaryPositionColumn.Band == this.Band )
			{
				Debug.Assert( null != this.Band, "No band !" );

				if ( null != this.Band )
				{
					SerializedColumnID colid = this.Band.GetSerializedColumnId( this.SummaryPositionColumn );
				
					Debug.Assert( null != colid, "GetSerializedColumnId returned null." );
					if ( null != colid )
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, "SummaryPositionColumn", colid );
						//info.AddValue( "SummaryPositionColumn", colid );
					}
				}
			}

            // MRS 7/30/2009 - TFS20044
            if (this.ShouldSerializeCustomSummaryCalculator())
            {
                Utils.SerializeProperty(info, "CustomSummaryCalculator", this.CustomSummaryCalculator);
            }
		}

		#endregion // ISerializable.GetObjectData

		#region ToString

		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Overrode ToString.
		//
		/// <summary>
		/// Returns an empty string so the property window displays nothing.
		/// </summary>
        /// <returns>An empty string so the property window displays nothing.</returns>
		public override string ToString( )
		{
			return string.Empty;
		}

		#endregion // ToString

		#region Key

		// SSP 8/31/04 UWC68
		// Overrode the Key property so when the key of the summary changes, we
		// can remove the old calc reference and add the new one into the calc network.
		// We need to do this because the changing the key changes the element name of
		// the calc refrence.
		//
		/// <summary>
		/// Returns or sets a value that uniquely identifies an object in a collection.
		/// </summary>
		[LocalizedCategory("LC_Default")]
		[MergableProperty(false)]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override String Key
		{
			get
			{
				return base.Key;
			}
			set
			{
				if ( value != this.Key )
				{
					// SSP 8/31/04 UWC68
					//
					this.FormulaHolder.RemoveReferenceFromCalcNetwork( );

					base.Key = value;

					// SSP 8/31/04 UWC68
					//
					this.FormulaHolder.AddReferenceToCalcNetwork( );
				}			
			}
		}

		#endregion // Key

		#endregion // Base Overrides

		#region Public properties
        
		#region Band

		/// <summary>
		/// Returns the associated band.
		/// </summary>
		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Added DesignerSerializationVisibility.Hidden attribute because this is serialized
		// as a parameter to the constructor. Also added Browsable( false )
		//
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ), Browsable( false ) ]
		public UltraGridBand Band
		{
			get
			{
				return null != this.ParentCollection ? this.ParentCollection.Band : null;
			}
		}

		#endregion // Band

		#region SummaryType

		/// <summary>
		/// Type of summary to calculate.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings.Formula"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings.CustomSummaryCalculator"/>
		/// </remarks>
		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Added DesignerSerializationVisibility.Hidden attribute because this is serialized
		// as a parameter to the constructor.
		//
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public SummaryType SummaryType
		{
			get
			{
				return this.summaryType;
			}
			set
			{
				if ( this.summaryType != value )
				{
					if ( !Enum.IsDefined( typeof( SummaryType ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_348"), (int)value, typeof( SummaryType ) );

					this.summaryType = value;

					this.BumpSummaryCalculationsVersion( );

					this.NotifyPropChange( PropertyIds.SummaryType, null );
				}
			}
		}

		#endregion // SummaryType

		#region CustomSummaryCalculator

		/// <summary>
		/// Gets or sets the <see cref="ICustomSummaryCalculator"/> object used for calculating custom summary. This only applies if <see cref="SummaryType"/> is set to <b>Custom</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// You can perform custom summary calculations by implementing <see cref="ICustomSummaryCalculator"/>
		/// interface. See <see cref="ICustomSummaryCalculator"/> for more information.
		/// </p>
		/// <seealso cref="ICustomSummaryCalculator"/>
		/// <seealso cref="SummarySettings.SummaryType"/>
		/// </remarks>
		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Added DesignerSerializationVisibility.Hidden attribute because this is serialized
		// as a parameter to the constructor.
		//
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ Browsable( false ) ]
		public ICustomSummaryCalculator CustomSummaryCalculator
		{
			get
			{
				return this.customSummaryCalculator;
			}
			set
			{
				if ( this.customSummaryCalculator != value )
				{
					this.customSummaryCalculator = value;

					if ( SummaryType.Custom == this.SummaryType )
						this.BumpSummaryCalculationsVersion( );

					this.NotifyPropChange( PropertyIds.CustomSummaryCalculator, null );
				}
			}
		}

		#endregion // CustomSummaryCalculator

		#region SourceColumn

		/// <summary>
		/// Gets or sets the source column. Data from the source column is what gets summarized.
		/// </summary>
		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Added DesignerSerializationVisibility.Hidden attribute because this is serialized
		// as a parameter to the constructor. Also added the Editor attribute.
		//
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ Editor( typeof( SummarySettings.ColumnsListUITypeEditor), typeof(UITypeEditor) ) ]
		public UltraGridColumn SourceColumn
		{
			get
			{
				return this.sourceColumn;
			}
			set
			{
				if ( this.sourceColumn != value )
				{
					Debug.Assert( null != this.Band, "this.Band is null." );

					// SSP 8/2/04
					// We were supposed to be looking at the new source column and not the old one.
					//
					// ----------------------------------------------------------------------------------
					
					if ( null != value &&
						null != this.Band &&
						value.Band != this.Band &&
						! value.Band.IsDescendantOfBand( this.Band ) )
						throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_352"), Shared.SR.GetString("LE_ArgumentNullException_353") );
					// ----------------------------------------------------------------------------------

					this.sourceColumn = value;

					// Bump the summary calculations version so that we recalculate
					// the summary next time it's value is accessed.
					//
					this.BumpSummaryCalculationsVersion( );

					this.NotifyPropChange( PropertyIds.SourceColumn, null );
				}
			}
		}

		#endregion // SourceColumn

		#region DisplayFormat

		/// <summary>
		/// A substitution string used for formatting the summary. For example: "{0}", "Total = {0}".
		/// </summary>
		/// <remarks>
		/// <p class="body">The format of the string is the same as string.Format method's format argument. As a matter of fact, the calculated summary value will be formatted by calling string.Format method and passing the value of this property and the calculated summary figure. In case an invalid format is specified, a default format will be used.</p>
		/// <seealso cref="SummaryValue.SummaryText"/>
		/// <seealso cref="SummaryValue.Value"/>
		/// </remarks>
		// SSP 1/11/05 BR00650
		// Added RefreshPropertiesAttribute so DisplayFormatResolved updates in the property window.
		//
		[ RefreshPropertiesAttribute( RefreshProperties.All ) ]
		public string DisplayFormat
		{
			get
			{
				return this.displayFormat;
			}
			set
			{
				if ( this.displayFormat != value )
				{
					// SSP 8/19/04 UWG3639
					// Treat empty string as a default value as well. Empty string is not a valid
					// format anyways. Commented following code out.
					//
					

					this.displayFormat = value;

					this.NotifyPropChange( PropertyIds.DisplayFormat, null );
				}
			}
		}

		#endregion // DisplayFormat

		#region DisplayFormatProvider

		// SSP 1/3/06 BR08523
		// We have DisplayFormat property, but we don't have the associated format provider property.
		// So added DisplayFormatProvider property.
		// 
		/// <summary>
		/// Gets or sets the culture specific information used to determine how values are formatted.
		/// </summary>
		/// <value>An object that implements the IFormatProvider interface, such as the CultureInfo class.</value>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.FormatInfo"/>
		/// <seealso cref="SummarySettings.DisplayFormat"/>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public IFormatProvider DisplayFormatProvider
		{
			get
			{
				return this.displayFormatProvider;
			}

			set
			{
				if ( this.displayFormatProvider != value )
				{
					this.displayFormatProvider = value;
					
					this.NotifyPropChange( PropertyIds.DisplayFormatProvider );
				}
			}
		}

		#endregion // DisplayFormatProvider

		#region Lines

		/// <summary>
		/// Gets or sets the number of text lines summary associated with this summary settings object will have.
		/// </summary>
		/// <remarks>
		/// <p class="body">If the <see cref="DisplayFormat"/> is as such that <see cref="Infragistics.Win.UltraWinGrid.SummaryValue.SummaryText"/> is going to result in a multiline text, then this property should be set to that number of linesvalue, otherwise default value of 1 will be used text will be clipped.</p>
		/// </remarks>
		public int Lines
		{
			get
			{
				return this.lines;
			}
			set
			{
				if ( value < 1 || value >= 10 )
					throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_354"), value, Shared.SR.GetString("LE_ArgumentOutOfRangeException_355") );

				if ( this.lines != value )
				{
					this.lines = value;

					this.NotifyPropChange( PropertyIds.Lines );
				}
			}
		}

		#endregion Lines

		#region DisplayInGroupBy

		/// <summary>
		/// Specifies whether to display the associated summary in group by rows if there are any. Default value is true.
		/// </summary>
		/// <remarks>
		/// <seealso cref="SummarySettings.SummaryDisplayArea"/>
		/// <seealso cref="UltraGridOverride.SummaryDisplayArea"/>
		/// </remarks>
		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
		// SummaryDisplayArea property. Added following attributes.
		//
		[ Obsolete( "Hidden and DisplayInGroupBy properties have been obsoleted. Please use the SummaryDisplayArea property instead.", false ) ]
		[ EditorBrowsable( EditorBrowsableState.Never ), Browsable( false ) ]
		public bool DisplayInGroupBy
		{
			get
			{
				// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
				// SummaryDisplayArea property.
				//
				//return this.displayInGroupBy;
				return this.DisplayInGroupByNoWarns;
			}
			set
			{
				if ( value != this.DisplayInGroupByNoWarns )
				{
					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// Added ability to display summaries in group-by rows aligned with columns, 
					// summary footers for group-by row collections, fixed summary footers and
					// being able to display the summary on top of the row collection.
					// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
					// SummaryDisplayArea property.
					//
					//this.displayInGroupBy = value;
					this.DisplayInGroupByNoWarns = value;

					this.NotifyPropChange( PropertyIds.DisplayInGroupBy, null );
				}
			}
		}

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
		// SummaryDisplayArea property.
		//
		private bool DisplayInGroupByNoWarns
		{
			get
			{
				return 0 != ( SummaryDisplayAreas.InGroupByRows & this.SummaryDisplayAreaResolved );
			}
			set
			{
				if ( value != this.DisplayInGroupByNoWarns )
				{
					this.summaryDisplayArea = this.SummaryDisplayAreaResolved ^ SummaryDisplayAreas.InGroupByRows;
				}
			}
		}

		#endregion // DisplayInGroupBy

		#region SummaryPosition

		/// <summary>
		/// Specifies where summary footers to display the summary. If its value is UseColumn, then the summary will be displayed under column returned by SummaryPositionColumn property in the summary footers. If its Left or Right they will be displayed in the free-form line of the summary footers.
		/// </summary>
		/// <remarks>
		/// <seealso cref="SummarySettings.SummaryDisplayArea"/>
		/// <seealso cref="UltraGridOverride.SummaryDisplayArea"/>
		/// </remarks>
		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Added DesignerSerializationVisibility.Hidden attribute because this is serialized
		// as a parameter to the constructor.
		//
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public SummaryPosition SummaryPosition
		{
			get
			{
				return this.summaryPosition;
			}
			set
			{
				if ( this.summaryPosition != value )
				{
					if ( !Enum.IsDefined( typeof( SummaryPosition ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_349"), (int)value, typeof( SummaryPosition ) );

					this.summaryPosition = value;

					this.NotifyPropChange( PropertyIds.SummaryPosition, null );
				}
			}
		}

		#endregion // SummaryPosition
		
		#region SummaryPositionColumn

		/// <summary>
		/// Column under which this property will be displayed. If SummaryPosition is something other than UseColumn, this will be ignored. This column must be from the band that this SummarySettings object is assigned to.
		/// </summary>
		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Added DesignerSerializationVisibility.Hidden attribute because this is serialized
		// as a parameter to the constructor. Also added Editor attribute.
		//
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ Editor( typeof( SummarySettings.ColumnsListUITypeEditor), typeof(UITypeEditor) ) ]
		public UltraGridColumn SummaryPositionColumn
		{
			get
			{
				return this.summaryPositionColumn;
			}
			set
			{
				if ( this.summaryPositionColumn != value )
				{
					if ( null != value && !this.Band.Columns.Contains( value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_356"), Shared.SR.GetString("LE_ArgumentException_357") );

					this.summaryPositionColumn = value;

					this.NotifyPropChange( PropertyIds.SummaryPositionColumn, null );
				}
			}
		}

		#endregion // SummaryPositionColumn

		#region Hidden

		/// <summary>
		/// Indicates if associated summary in the summary footers will be hidden. DisplayInGroupBy will still be honored even if this is true.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can set Hidden to true to hide the summary. This can be useful if you just want the calculated summary value but not have it show up it in the grid.</p>
		/// <p class="body">NOTE: Summaries with <see cref="SummaryPosition"/> set to <b>UseSummaryPositionColumn</b> will be hidden if <see cref="SummaryPositionColumn"/> column is hidden. This rule does not apply to free form summaries, ones with <see cref="SummaryPosition"/> set to something other than <b>UseSummaryPositionColumn</b> since they are not shown under a particular column but rather in free form area.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings.DisplayInGroupBy"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings.SummaryDisplayArea"/>
		/// </remarks>
		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
		// SummaryDisplayArea property. Added following attributes.
		//
		[ Obsolete( "Hidden and DisplayInGroupBy properties have been obsoleted. Please use the SummaryDisplayArea property instead.", false ) ]
		[ EditorBrowsable( EditorBrowsableState.Never ), Browsable( false ) ]
		public bool Hidden
		{
			get
			{	
				// SSP 10/21/04 UWG3640
				// We don't need to do check for summary position column being null here. 
				// IsValid property checks for this condition. Besides this interferes with 
				// the serialization of hidden property. Commented out below code.
				//
				

				// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
				// SummaryDisplayArea property.
				//
				//return this.hidden;
				return this.HiddenNoWarns;
			}
			set
			{
				if ( value != this.HiddenNoWarns )
				{
					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// Added ability to display summaries in group-by rows aligned with columns, 
					// summary footers for group-by row collections, fixed summary footers and
					// being able to display the summary on top of the row collection.
					// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
					// SummaryDisplayArea property.
					//
					//this.hidden = value;
					this.HiddenNoWarns = value;
					
					this.NotifyPropChange( PropertyIds.Hidden, null );
				}
			}
		}

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
		// SummaryDisplayArea property.
		//
		private bool HiddenNoWarns
		{
			get
			{
				return 0 != ( SummaryDisplayAreas.Bottom & this.SummaryDisplayAreaResolved );
			}
			set
			{
				if ( value != this.HiddenNoWarns )
				{
					this.summaryDisplayArea = this.SummaryDisplayAreaResolved ^ SummaryDisplayAreas.Bottom;
				}
			}
		}

		#endregion // Hidden

		#region Appearance

		/// <summary>
		/// Appearance that will be applied to summaries associated with this SummerySettings object.
		/// </summary>
		/// <remarks>
		/// <p class="body">Font settings off the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.SummaryValueAppearance"/> off the Override object are used to calculate the necessary height of the summary footer and the summary values. So if you are planning on modifying the size of font for individual SummarySettings or SummaryValue objects, make sure that a font just as big is assigned to SummaryValueAppearance off the Override object for the band or the layout.</p>
		/// <p class="body"><b>Note:</b> This appearance is not applied to summaries displayed in group by rows. For that use the <see cref="SummarySettings.GroupBySummaryValueAppearance"/>.</p>
		/// </remarks>
		[LocalizedCategory("LC_Appearance")]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		public Infragistics.Win.AppearanceBase Appearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.appearanceHolder )
				{
					this.appearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
					// hook up the prop change notifications
					//
					this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if ( null != this.Band && null != this.Band.Layout )
					this.appearanceHolder.Collection = this.Band.Layout.Appearances;

				return this.appearanceHolder.Appearance;
			}
			set
			{
				if ( this.appearanceHolder == null				|| 
					!this.appearanceHolder.HasAppearance		|| 
					value != this.appearanceHolder.Appearance )
				{
					if ( null == this.appearanceHolder )
					{
						this.appearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.appearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if ( null != this.Band && null != this.Band.Layout )
						this.appearanceHolder.Collection = this.Band.Layout.Appearances;

					this.appearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( PropertyIds.Appearance );
				}
			}
		}

		#endregion //Appearance

		#region HasAppearance

		/// <summary>
		/// Returns true if appearance object for Appearance property has been allocated.
		/// </summary>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public bool HasAppearance
		{
			get
			{
				return null != this.appearanceHolder && this.appearanceHolder.HasAppearance;
			}
		}

		#endregion // HasAppearance

		#region GroupBySummaryValueAppearance

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GroupBySummaryValueAppearance.
		//
		/// <summary>
		/// Appearance that will be applied to summaries in group-by rows associated with this SummerySettings object.
		/// </summary>
		/// <remarks>
		/// <p class="body">Font settings off the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.GroupBySummaryValueAppearance"/> off the Override object are used to calculate the necessary height of the summary values. So if you are planning on modifying the size of font for individual SummarySettings or SummaryValue objects, make sure that a font just as big is assigned to GroupBySummaryValueAppearance off the Override object for the band or the layout.</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_GroupByRowAppearance") ]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		public Infragistics.Win.AppearanceBase GroupBySummaryValueAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.groupBySummaryValueAppearanceHolder )
				{
					this.groupBySummaryValueAppearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
					// hook up the prop change notifications
					//
					this.groupBySummaryValueAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if ( null != this.Layout )
					this.groupBySummaryValueAppearanceHolder.Collection = this.Layout.Appearances;

				return this.groupBySummaryValueAppearanceHolder.Appearance;
			}
			set
			{
				if ( this.groupBySummaryValueAppearanceHolder == null			|| 
					!this.groupBySummaryValueAppearanceHolder.HasAppearance		|| 
					value != this.groupBySummaryValueAppearanceHolder.Appearance )
				{
					if ( null == this.groupBySummaryValueAppearanceHolder )
					{
						this.groupBySummaryValueAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.groupBySummaryValueAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					if ( null != this.Layout )
						this.groupBySummaryValueAppearanceHolder.Collection = this.Layout.Appearances;

					this.groupBySummaryValueAppearanceHolder.Appearance = value;

					// notify listeners
					//
					this.NotifyPropChange( PropertyIds.GroupBySummaryValueAppearance );
				}
			}
		}

		#endregion // GroupBySummaryValueAppearance

		#region HasGroupBySummaryValueAppearance

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GroupBySummaryValueAppearance.
		//
		/// <summary>
		/// Returns true if appearance object for GroupBySummaryValueAppearance property has been allocated.
		/// </summary>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public bool HasGroupBySummaryValueAppearance
		{
			get
			{
				return null != this.groupBySummaryValueAppearanceHolder && this.groupBySummaryValueAppearanceHolder.HasAppearance;
			}
		}

		#endregion // HasGroupBySummaryValueAppearance

		#region Formula

		// SSP 6/30/04 - UltraCalc
		// Added Formula property.
		//
		/// <summary>
		/// Specifies the formula. <see cref="UltraGridBase.CalcManager"/> property must be set to a valid instance of <see cref="IUltraCalcManager"/> for this property to have any affect. Also the <see cref="SummaryType"/> property of this summary object must be set to <b>Formula</b>.
		/// </summary>
		/// <remarks>
        /// <p class="body"> <see cref="UltraGridBase.CalcManager"/> property must be set to a valid instance of <see cref="IUltraCalcManager"/> for this property to have any affect. Also <see cref="SummaryType"/> property of this summary object must be set to <b>Formula</b>.</p>
		/// <seealso cref="UltraGridColumn.Formula"/> <seealso cref="UltraGridBase.CalcManager"/>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ Editor( "Infragistics.Win.UltraWinCalcManager.FormulaBuilder.FormulaUITypeEditor, " + AssemblyRef.FormulaBuilderDesign, typeof(UITypeEditor) ) ]
		[ MergableProperty( false ) ]
		public string Formula
		{
			get
			{
				return this.FormulaHolder.Formula;
			}
			set
			{
				// SSP 11/8/04 UWC91
				// Trim spaces from the formula string. This is so the property grid doesn't
				// accept space as a formula.
				//
				if ( null != value )
					value = value.Trim( );

				if ( this.FormulaHolder.Formula != value )
				{
					// SSP 6/13/05
					// Whenever the formula of a summary is changed, make sure that the group-by
					// level summary references use the new formula. Decrement the 
					// verifiedSummariesVersion for that.
					// 
					if ( null != this.Band && this.Band.HasCalcReference )
						this.Band.CalcReference.verifiedSummariesVersion--;

					this.FormulaHolder.Formula = value;

					this.FormulaHolder.ReaddFormulaToCalcNetwork( );

					if ( null != this.Band )
						this.NotifyPropChange( PropertyIds.Formula );
				}
			}
		}

		#endregion // Formula

		#region FormulaValueConverter 

		// SSP 11/9/04
		// Added FormulaValueConverter property.
		//
		/// <summary>
		/// Interface for providing custom logic for converting formula results and the formula source values.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>FormulaValueConverter</b> allows you to write custom logic for converting cell
		/// values to values that the formulas will use for calculations. It also allows you
		/// to write custom logic for converting results of formula calculations to cell values.
		/// </p>
		/// <seealso cref="IFormulaValueConverter"/>
		/// <seealso cref="SummarySettings.Formula"/>
		/// <seealso cref="UltraGridBase.CalcManager"/>
		/// </remarks>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public IFormulaValueConverter FormulaValueConverter 
		{
			get
			{
				return this.FormulaHolder.FormulaValueConverter;
			}
			set
			{
				this.FormulaHolder.FormulaValueConverter = value;
			}
		}

		#endregion // FormulaValueConverter 

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		#region ShowCalculatingText

		/// <summary>
		/// Specifies whether summaries will display "#Calculating" in when calculating with an UltraCalcManager.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use <b>ShowCalculatingText</b> property to explicitly turn on or turn off the display of the text during calculations.</p>
		/// <p class="body">
		/// <b>Note</b> that this applies to formula summaries only; ones that are calculating using
		/// UltraCalcManager.
		/// </p>
		/// <seealso cref="UltraGridBase.CalcManager"/>
		/// <seealso cref="SummarySettings.Formula"/>
		/// </remarks>
		[ LocalizedDescription("LDR_SummarySettings_P_ShowCalculatingText")]
		[ LocalizedCategory("LC_Appearance") ]
		public DefaultableBoolean ShowCalculatingText
		{
			get
			{
				return this.showCalculatingText;
			}
			set
			{
				if ( this.showCalculatingText != value )
				{
					if ( ! Enum.IsDefined( typeof( DefaultableBoolean ), value ) )
						throw new InvalidEnumArgumentException( "ShowCalculatingText", (int)value, typeof( DefaultableBoolean ) );

					this.showCalculatingText = value;

					this.NotifyPropChange( PropertyIds.ShowCalculatingText );
				}
			}
		}

		#endregion // ShowCalculatingText

		#region SummaryDisplayArea

		// SSP 3/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		/// <summary>
		/// Specifies if and which area(s) the summaries will be displayed in.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The Override's <see cref="UltraGridOverride.SummaryDisplayArea"/> property affects
		/// all the summaries in the grid or the band where as this property only affects this
		/// summary.
		/// </p>
		/// <seealso cref="UltraGridOverride.SummaryDisplayArea"/>
		/// <seealso cref="SummaryDisplayAreas"/>
		/// <seealso cref="SummarySettings.SummaryPosition"/>
		/// <seealso cref="SummarySettings.SummaryPositionColumn"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridOverride_P_SummaryDisplayArea")]
		[ LocalizedCategory("LC_Display") ]
		[ Editor( typeof( Infragistics.Win.UltraWinGrid.DefaultableFlagsEnumUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
		public SummaryDisplayAreas SummaryDisplayArea
		{
			get
			{
				return this.summaryDisplayArea;
			}
			set
			{
				if ( this.summaryDisplayArea != value )
				{
					this.summaryDisplayArea = value;

					this.NotifyPropChange( PropertyIds.SummaryDisplayArea );
				}
			}
		}

		#endregion // SummaryDisplayArea

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		#region ToolTipText
		
		/// <summary>
		/// Specifies the tooltip to display when the user hovers the mouse over the header.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies the tooltip to display when the user hovers the mouse over the header. No tooltip is displayed if this property is set to null or empty string.</p>
		/// </remarks>
		public string ToolTipText
		{
			get
			{
				return this.toolTipText;
			}
			set
			{
				if ( this.toolTipText != value )
				{
					this.toolTipText = value;

					this.NotifyPropChange( PropertyIds.ToolTipText );
				}
			}
		}

		#endregion // ToolTipText

		#endregion // Public properties

		#region Public methods

		#region ResetDisplayInGroupBy

		/// <summary>
		/// Resets DisplayInGroupBy property to its default value.
		/// </summary>
		public void ResetDisplayInGroupBy( )
		{
			this.DisplayInGroupByNoWarns = true;
		}

		#endregion // ResetDisplayInGroupBy

		#region ResetHidden

		/// <summary>
		/// Resets Hidden property to its default value.
		/// </summary>
		public void ResetHidden( )
		{
			this.HiddenNoWarns = false;
		}

		#endregion // ResetHidden

		#region ResetAppearance

		/// <summary>
		/// Resets Appearance property to its default value.
		/// </summary>
		public void ResetAppearance( )
		{
			if ( this.HasAppearance )
			{
				// Call reset on the holder instead of the appearance
				//
				this.appearanceHolder.Reset( );

				// Notify listeners that the appearance has changed. 
				//
				this.NotifyPropChange(PropertyIds.Appearance);
			}
		}

		#endregion // ResetAppearance
		
		#region ResetGroupBySummaryValueAppearance

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GroupBySummaryValueAppearance.
		//
		/// <summary>
		/// Resets GroupBySummaryValueAppearance property to its default value.
		/// </summary>
		public void ResetGroupBySummaryValueAppearance( )
		{
			if ( this.HasGroupBySummaryValueAppearance )
			{
				// Call reset on the holder instead of the appearance
				//
				this.groupBySummaryValueAppearanceHolder.Reset( );

				// Notify listeners that the appearance has changed. 
				//
				this.NotifyPropChange(PropertyIds.GroupBySummaryValueAppearance);
			}
		}

		#endregion // ResetGroupBySummaryValueAppearance

		#region ResetSummaryPositionColumn

		/// <summary>
		/// Resets SummaryPositionColumn property to its default value.
		/// </summary>
		public void ResetSummaryPositionColumn( )
		{
			this.SummaryPositionColumn = null;
		}

		#endregion // ResetSummaryPositionColumn
		
		#region ResetSummaryPosition

		/// <summary>
		/// Resets SummaryPosition property to its default value.
		/// </summary>
		public void ResetSummaryPosition( )
		{
			this.SummaryPosition = SummaryPosition.UseSummaryPositionColumn;
		}

		#endregion // ResetSummaryPosition

		#region ResetDisplayFormat

		/// <summary>
		/// Resets the DisplayFormat to it's default value which is null.
		/// </summary>
		public void ResetDisplayFormat( )
		{
			this.DisplayFormat = null;
		}

		#endregion // ResetDisplayFormat

		#region ResetLines

		/// <summary>
		/// Resets Lines property to it's default value of 1.
		/// </summary>
		public void ResetLines( )
		{
			this.Lines = 1;
		}

		#endregion // ResetLines

		// SSP 8/2/04 - UltraCalc
		// Added ShouldSerializeFormula.
		//
		#region ResetFormula

		/// <summary>
		/// Resets Formula property to it's default value of null.
		/// </summary>
		public void ResetFormula( )
		{
			this.Formula = null;
		}

		#endregion // ResetFormula

		#region ResetShowCalculatingText

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetShowCalculatingText( )
		{
			this.ShowCalculatingText = DefaultableBoolean.Default;
		}

		#endregion // ResetShowCalculatingText

		#region ResetSummaryDisplayArea

		// SSP 3/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSummaryDisplayArea( )
		{
			this.SummaryDisplayArea = SummaryDisplayAreas.Default;
		}

		#endregion // ResetSummaryDisplayArea

		#region Refresh

		/// <summary>
		/// Recalculates the summaries associated with this SummarySettings object.
		/// </summary>
		public void Refresh( )
		{
			this.BumpSummaryCalculationsVersion( );

			if ( null != this.Band.Layout )
				this.Band.Layout.DirtyGridElement( );
		}

		#endregion // Refresh

		#region ResetToolTipText

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		/// <summary>
		/// Resets the property to its default value of false.
		/// </summary>
		public void ResetToolTipText( )
		{
			this.ToolTipText = null;
		}

		#endregion // ResetToolTipText

		#endregion // Public methods

		#region Protected Methods

		#region ShouldSerializeDisplayFormat

		/// <summary>
		/// Retruns true if DisplayFormat property is assigned a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeDisplayFormat( )
		{
			// SSP 8/19/04 UWG3639
			// Treat empty string as a default value as well. Empty string is not a valid
			// format anyways.
			//
			//return null != this.displayFormat;
			return null != this.displayFormat && this.displayFormat.Length > 0;
		}

		#endregion // ShouldSerializeDisplayFormat
		
		#region ShouldSerializeLines

		/// <summary>
		/// Retruns true if Lines property is assigned a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeLines( )
		{
			return 1 != this.lines;
		}

		#endregion // ShouldSerializeLines

		#region ShouldSerializeHidden

		/// <summary>
		/// Returns true if Hidden property is assigned a non-default value.
		/// </summary>
		public bool ShouldSerializeHidden( )
		{
			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
			// SummaryDisplayArea property.
			//
			//return this.Hidden;
			return false;
		}

		#endregion // ShouldSerializeHidden

		#region ShouldSerializeAppearance

		/// <summary>
		/// Returns true if Appearance property is assigned a non-default value.
		/// </summary>
		public bool ShouldSerializeAppearance( )
		{
			return this.HasAppearance && this.Appearance.ShouldSerialize( );
		}

		#endregion // ShouldSerializeAppearance
		
		#region ShouldSerializeSummaryPositionColumn

		/// <summary>
		/// Returns true if SummaryPositionColumn property is assigned a non-default value.
		/// </summary>
		public bool ShouldSerializeSummaryPositionColumn( )
		{
			return null != this.SummaryPositionColumn;
		}

		#endregion // ShouldSerializeSummaryPositionColumn
		
		#region ShouldSerializeSummaryPosition

		/// <summary>
		/// Returns true if SummaryPosition property is assigned a non-default value.
		/// </summary>
		public bool ShouldSerializeSummaryPosition( )
		{
			return SummaryPosition.UseSummaryPositionColumn != this.SummaryPosition;
		}

		#endregion // ShouldSerializeSummaryPosition

		#region ShouldSerializeDisplayInGroupBy

		// SSP 1/24/03 UWG1950
		// Added ShouldSerializeDisplayInGroupBy method.
		//
		/// <summary>
		/// Returns true if DisplayInGroupBy property is assigned a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDisplayInGroupBy( )
		{
			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
			// SummaryDisplayArea property.
			//
			//return !this.displayInGroupBy;
			return false;
		}

		#endregion // ShouldSerializeDisplayInGroupBy

		#region ShouldSerializeFormula

		// SSP 8/2/04 - UltraCalc
		// Added ShouldSerializeFormula.
		//
		/// <summary>
		/// Returns true if formula property is assigned a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFormula( )
		{
			return null != this.Formula && this.Formula.Length > 0;
		}

		#endregion // ShouldSerializeFormula

		#region ShouldSerializeShowCalculatingText

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeShowCalculatingText( )
		{
			return DefaultableBoolean.Default != this.showCalculatingText;
		}

		#endregion // ShouldSerializeShowCalculatingText

		#region ShouldSerializeSummaryDisplayArea

		// SSP 3/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		/// <summary>
		/// Returns true if SummaryDisplayArea property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSummaryDisplayArea( )
		{
			return SummaryDisplayAreas.Default != this.summaryDisplayArea;
		}

		#endregion // ShouldSerializeSummaryDisplayArea

		#region OnSubObjectPropChanged

		// SSP 8/15/02
		// Overrode OnSubObjectPropChanged so we can refresh the display when any appearance
		// based settings are changed.
		//
        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			if ( this.HasAppearance &&
				propChange.Source == this.Appearance )
			{
				this.NotifyPropChange( PropertyIds.SummarySettings, propChange );
			}
		}

		#endregion // OnSubObjectPropChanged

		#region ShouldSerializeToolTipText

		// SSP 7/27/05 - Header, Row, Summary Tooltips
		// 
		/// <summary>
		/// Returns true if the property has been set to a non-default value of true.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeToolTipText( )
		{
			return null != this.toolTipText && this.toolTipText.Length > 0;
		}

		#endregion // ShouldSerializeToolTipText


        // MRS 7/30/2009 - TFS20044
        #region ShouldSerializeDisplayFormat

        /// <summary>
        /// Retruns true if DisplayFormat property is assigned a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        internal protected bool ShouldSerializeCustomSummaryCalculator()
        {
            if (null == this.customSummaryCalculator)
                return false;

            if (Utils.IsSafelyInfragisticsSerializable(this.customSummaryCalculator.GetType()))
                return true;

            return false;
        }

        #endregion // ShouldSerializeDisplayFormat

		#endregion // Protected Methods

		#region Internal/Private properties
		
		#region SummaryPositionColumnResolved

		internal UltraGridColumn SummaryPositionColumnResolved
		{
			get
			{
				if ( null != this.SummaryPositionColumn &&
					this.Band.Columns.Contains( this.SummaryPositionColumn ) )
					return this.SummaryPositionColumn;

				// If no summary position column was specified and the 
				// source column is from the same band as this summary settings
				// object, then return the source column.
				//
				// SSP 8/2/04 - UltraCalc
				// Formula summaries can have null source columns so check for sourceColumn 
				// being null.
				//
				//if ( this.Band == this.SourceColumn.Band )
				if ( null != this.SourceColumn && this.Band == this.SourceColumn.Band )
					return this.SourceColumn;

				return null;
			}
		}

		#endregion // SummaryPositionColumnResolved
	
		#region Layout
		
		internal UltraGridLayout Layout
		{
			get
			{
				return null != this.Band ? this.Band.Layout : null;
			}
		}

		#endregion // Layout

		#region SummaryCalculationsVersion
	
		internal int SummaryCalculationsVersion
		{
			get
			{
				// SSP 6/23/05 BR04577
				// Got rid of the SummaryCalculationsVersion off the SummarySettingsCollection.
				// It was being used anywhere. and it wouldn't be of much anyways since we have
				// to notify the calc manager of every summary change syncrhonously.
				// 
				
				return this.summaryCalculationsVersion;
			}
		}

		#endregion // SummaryCalculationsVersion

		#region IgnoreNulls

		internal bool IgnoreNulls
		{
			get
			{
				switch ( this.SummaryType )
				{ 
					case Infragistics.Win.UltraWinGrid.SummaryType.Minimum:
						return true;
				}

				return false;
			}
		}

		#endregion
		
		#region IsValid

		internal bool IsValid
		{
			get
			{
				// If the summary position is to use the summary position column and no
				// summary position column has been set, then retrun false.
				//
				if ( SummaryPosition.UseSummaryPositionColumn == this.SummaryPosition &&
					( null == this.SummaryPositionColumnResolved || this.SummaryPositionColumnResolved.Index < 0 ) )
					return false;

				// SSP 8/2/04 - UltraCalc
				// Formula summaries can have null source column.
				//
				// ------------------------------------------------------------------------
				//if ( null == this.SourceColumn || this.SourceColumn.Index < 0 )
				//	return false;
				if ( SummaryType.Formula != this.SummaryType 
					&& ( null == this.SourceColumn || this.SourceColumn.Index < 0 ) )
					return false;
				// ------------------------------------------------------------------------

				// If summary type is custom and no custom summary calculator has been
				// set, then return false.
				//
				if ( SummaryType.Custom == this.SummaryType &&
					null == this.CustomSummaryCalculator )
					return false;

				return true;
			}
		}

		#endregion // IsValid

		#region IsColumnDisplayed
		
		// SSP 1/25/05 BR00861
		// Added IsColumnDisplayed method.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool IsColumnDisplayed( UltraGridColumn column )
		private static bool IsColumnDisplayed( UltraGridColumn column )
		{
			// MD 1/19/09 - Groups in RowLayout
			// All this logic has been incorparted into HiddenResolved on the column.
			//if ( null == column || column.Hidden )
			//    return false;
			//
			//if ( column.Band.GroupsDisplayed && ( null == column.Group || column.Group.Hidden ) )
			//    return false;
			//
			//if ( column.Band.UseRowLayoutResolved && ! column.IsVisibleInLayout )
			//    return false;
			if ( column == null || column.HiddenResolved )
				return false;

			return true;
		}

		#endregion // IsColumnDisplayed

		#region HiddenResolvedHelper
		
		// SSP 10/21/04
		// Added HiddenResolved property.
		//
		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Changed from HiddenResolved to HiddenResolvedHelper.
		//
		//internal bool HiddenResolved
		internal bool HiddenResolvedHelper
		{
			get
			{
				return 
					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// Added ability to display summaries in group-by rows aligned with columns, 
					// summary footers for group-by row collections, fixed summary footers and
					// being able to display the summary on top of the row collection.
					// Hidden property is obsoleted now. There is no need to check for it here.
					//
					//this.Hidden ||
					! this.IsValid
					// SSP 1/24/05 BR00861
					//
					|| SummaryPosition.UseSummaryPositionColumn == this.SummaryPosition 
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//&& ! this.IsColumnDisplayed( this.SummaryPositionColumnResolved );
					&& !SummarySettings.IsColumnDisplayed( this.SummaryPositionColumnResolved );
			}
		}

		#endregion // HiddenResolvedHelper

		#region IsFixedSummary

		internal bool IsFixedSummary
		{
			get
			{
				return SummaryPosition.UseSummaryPositionColumn == this.SummaryPosition &&
					this.IsValid;
			}
		}

		#endregion // IsFixedSummary

		#region IsFreeFormSummary

		internal bool IsFreeFormSummary
		{
			get
			{
				return SummaryPosition.UseSummaryPositionColumn != this.SummaryPosition &&
					this.IsValid;					
			}
		}

		#endregion // IsFixedSummary

		#region GetSummaryTypeString

		private string GetSummaryTypeString( SummaryType summaryType )
		{
			switch ( this.SummaryType )
			{
				case SummaryType.Average:
					return SR.GetString( "SummaryTypeAverage" );					
				case SummaryType.Count:
					return SR.GetString( "SummaryTypeCount" );
				case SummaryType.Custom:
					return SR.GetString( "SummaryTypeCustom" );
				case SummaryType.Maximum:
					return SR.GetString( "SummaryTypeMaximum" );
				case SummaryType.Minimum:
					return SR.GetString( "SummaryTypeMinimum" );
				case SummaryType.Sum:
					return SR.GetString( "SummaryTypeSum" );
				default:
					Debug.Assert( false, "Unknown summary type !" );
					break;
			}

			return "";
		}

		#endregion // GetSummaryTypeString

		#region DisplayFormatResolved

		/// <summary>
		/// Returns the resolved display format. If DisplayFormat property is set to 
		/// something other than null and empty string, then it will return that 
		/// otherwise it will return a default dsiplay format.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Use the <see cref="DisplayFormat"/> property to specify the format that
		/// should be used to format the result of a summary calculation when displaying 
		/// it on the screen.
		/// </p>
		/// <seealso cref="DisplayFormat"/>
		/// </remarks>
		// SSP 8/19/04
		// Made DisplayFormatResolved public. We want to show this in the designer underneath
		// the DisplayFormat property so the users know what kind of format it will use
		// by default and what kind of strings DisplayFormat property takes.
		//
		//internal string DisplayFormatResolved
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public string DisplayFormatResolved
		{
			get
			{
				string displayFormat = this.DisplayFormat;

				// SSP 8/19/04 UWG3639
				// Treat empty string as a default value as well. Empty string is not a valid
				// format any ways.
				//
				//if ( null != displayFormat )
				if ( this.ShouldSerializeDisplayFormat( ) )
					return displayFormat;

				displayFormat =
					SummaryType.Custom != this.SummaryType 
					&& SummaryType.Formula != this.SummaryType 
					? this.GetSummaryTypeString( this.SummaryType ) + " = {0}"
					: "Summary = {0}";

				return displayFormat;
			}
		}

		#endregion // DisplayFormatResolved

		#region DisplayFormatProviderResolved

		// SSP 1/3/06 BR08523
		// We have DisplayFormat property, but we don't have the associated format provider property.
		// So added DisplayFormatProvider property.
		// 
		internal IFormatProvider DisplayFormatProviderResolved
		{
			get
			{
				IFormatProvider ret = this.DisplayFormatProvider;

				if ( null == ret && null != this.SourceColumn )
					ret = this.SourceColumn.FormatInfo;

				return ret;
			}
		}

		#endregion // DisplayFormatProviderResolved

		#region InternalSerializedSourceColumnID

		internal SerializedColumnID InternalSerializedSourceColumnID
		{
			get
			{
				return this.serializedSourceColumnId;
			}
		}

		#endregion  // InternalSerializedSourceColumnID

		#region InternalSerializedSummaryPositionColumnId

		internal SerializedColumnID InternalSerializedSummaryPositionColumnId
		{
			get
			{
				return this.serializedSummaryPositionColumnId;
			}
		}

		#endregion  // InternalSerializedSummaryPositionColumnId

		#region InternalSerializedSourceColumnBandKey

		internal string InternalSerializedSourceColumnBandKey
		{
			get
			{
				return this.serializedSourceColumnBandKey;
			}
		}

		#endregion // InternalSerializedSourceColumnBandKey

		#region InternalSerializedSourceColumnBandIndex

		internal int InternalSerializedSourceColumnBandIndex
		{
			get
			{
				return this.serializedSourceColumnBandIndex;
			}
		}

		#endregion // InternalSerializedSourceColumnBandIndex

		#region SummaryDisplayAreaResolved

		// SSP 3/14/05 - NAS 5.2 Outlook Style Fixed Add Row
		//
		internal SummaryDisplayAreas SummaryDisplayAreaResolved
		{
			get
			{

				SummaryDisplayAreas val = this.summaryDisplayArea;

				if ( SummaryDisplayAreas.Default == val )
				{
					// JAS 5/16/05 - The Band could be null, so check for null.
					//if ( this.Band.HasOverride )
					if( this.Band != null && this.Band.HasOverride )
						val = this.Band.Override.SummaryDisplayArea;

					// JAS 5/16/05 - The Layout could be null, so check for null.
					//if ( SummaryDisplayAreas.Default == val && this.Layout.HasOverride )
					if ( SummaryDisplayAreas.Default == val && this.Layout != null &&  this.Layout.HasOverride )
						val = this.Layout.Override.SummaryDisplayArea;
				}

				return SummaryDisplayAreas.Default != val ? val 
					: SummaryDisplayAreas.Bottom | SummaryDisplayAreas.InGroupByRows;
			}
		}

		#endregion // SummaryDisplayAreaResolved

        // MRS 7/30/2009 - TFS20044
        #region CanSerialize
        internal bool CanSerialize
        {
            get
            {
                if (this.summaryType != SummaryType.Custom)
                    return true;

                if (this.customSummaryCalculator == null)
                    return false;

                return Utils.IsSafelyInfragisticsSerializable(this.customSummaryCalculator.GetType());
            }
        }
        #endregion // CanSerialize

		#endregion // Internal/Private properties

		#region Internal/Private methods
	
		#region BumpSummaryCalculationsVersion

		internal void BumpSummaryCalculationsVersion( )
		{
			this.summaryCalculationsVersion++;

			// SSP 6/23/05 BR04577
			// 
			this.NotifyCalcManager_ValueChanged( );

			// SSP 7/11/06 BR14108
			// 
			UltraGridLayout layout = this.Layout;
			if ( null != layout )
				layout.BumpCellChildElementsCacheVersion( );
		}

		#endregion // BumpSummaryVersion

		#region  ParentCollection

		internal SummarySettingsCollection ParentCollection
		{
			get
			{
				return this.parentCollection;
			}
		}

		#endregion  // ParentCollection

		#region InternalInitializeFrom

		/// <summary>
		/// Initialize current object from source.
		/// </summary>
		/// <param name="source"></param>
		internal void InternalInitializeFrom( SummarySettings source )
		{
            // MRS 5/21/2009 - TFS17817
            // If this is a SummarySettings in an Export layout, store the source
            // SummarySettings, so we can map back to it. We need this for formula
            // Exporting in Excel. 
            //
            UltraGridBand band = this.Band;
            if (band != null)
            {
                UltraGridLayout layout = band.Layout;
                if (layout != null)
                {
                    if (layout.IsExportLayout ||
                        layout.IsPrintLayout)
                    {
                        this.sourceSummarySettings = source;
                    }
                }
            }

			// Serialize the tag.
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.tagValue;

			this.displayFormat = source.displayFormat;
			this.lines = source.lines;
			this.summaryType = source.summaryType;
			this.summaryPosition = source.summaryPosition;

			// SSP 1/24/03 UWG1950
			// Serialize and deserialize DisplayInGroupBy property.
			//
			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			// Obsoleted Hidden and DisplayInGroupBy properties due to the new 
			// SummaryDisplayArea property.
			//
			//this.hidden = source.hidden;
			//this.displayInGroupBy = source.displayInGroupBy;
			this.summaryDisplayArea = source.summaryDisplayArea;

			// SSP 10/21/04
			// Copy over the showCalculatingText also.
			//
			this.showCalculatingText = source.showCalculatingText;

			// SSP 7/27/05 - Header, Row, Summary Tooltips
			// 
			this.toolTipText = source.toolTipText;
				
			// SSP 1/3/06 BR08523
			// We have DisplayFormat property, but we don't have the associated format provider property.
			// So added DisplayFormatProvider property.
			// 
			this.displayFormatProvider = source.displayFormatProvider;

			// SSP 8/20/02
			// Use appearance holder rather than the appearance.
			//
			
			if ( null != source.appearanceHolder &&
				source.appearanceHolder.HasAppearance )
			{
				// Get Appearance to trigger allocation of holder
				//
				Infragistics.Win.AppearanceBase app = this.Appearance;
			
				// init the holder from the source
				//
				this.appearanceHolder.InitializeFrom( source.appearanceHolder );
			}
			else
			{
				// reset the appearance holder
				//
				if ( null != this.appearanceHolder  &&
					this.appearanceHolder.HasAppearance )
				{
					this.appearanceHolder.Reset( );
				}
			}


            // MRS 5/9/2008 - BR32722
            if (null != source.groupBySummaryValueAppearanceHolder &&
                source.groupBySummaryValueAppearanceHolder.HasAppearance)
            {
                // Get Appearance to trigger allocation of holder
                //
                Infragistics.Win.AppearanceBase app = this.GroupBySummaryValueAppearance;

                // init the holder from the source
                //
                this.groupBySummaryValueAppearanceHolder.InitializeFrom(source.groupBySummaryValueAppearanceHolder);
            }
            else
            {
                // reset the appearance holder
                //
                if (null != this.groupBySummaryValueAppearanceHolder &&
                    this.groupBySummaryValueAppearanceHolder.HasAppearance)
                {
                    this.groupBySummaryValueAppearanceHolder.Reset();
                }
            }
		}

		#endregion // InternalInitializeFrom

		#region OnDeserializationComplete
		
		// SSP 10/21/04 UWG3640
		// Added OnDeserializationComplete so the summaries can deseiralize the serialized
		// column ids when the deserialization is complete.
		//
		internal void OnDeserializationComplete( SummarySettingsCollection parentCollection )
		{
			this.Initialize( parentCollection );

			UltraGridLayout layout = this.Layout;
			if ( null != layout )
			{
				if ( null != this.serializedSourceColumnBandKey && null != this.serializedSourceColumnId )
				{
                    // MBS 6/16/09 - TFS18242
                    // Replaced SortedBands with Bands since we shouldn't be looking at that when performing deserialization
                    //
                    //UltraGridBand matchingBand = layout.SortedBands.Exists(this.serializedSourceColumnBandKey)
                    //? layout.SortedBands[this.serializedSourceColumnBandKey] : null;
                    //
                    //if (null == matchingBand && this.serializedSourceColumnBandIndex >= 0
                    //    && this.serializedSourceColumnBandIndex < layout.SortedBands.Count)
                    //    matchingBand = layout.SortedBands[this.serializedSourceColumnBandIndex];
                    //
                    UltraGridBand matchingBand = layout.Bands.Exists(this.serializedSourceColumnBandKey)
                        ? layout.Bands[this.serializedSourceColumnBandKey] : null;
                    //
                    if (null == matchingBand && this.serializedSourceColumnBandIndex >= 0
                        && this.serializedSourceColumnBandIndex < layout.Bands.Count)
                        matchingBand = layout.Bands[this.serializedSourceColumnBandIndex];

					UltraGridColumn matchingColumn = null != matchingBand 
						? matchingBand.GetMatchingColumn( this.serializedSourceColumnId ) : null;

					if ( null != matchingColumn && matchingColumn.Band.IsTrivialDescendantOf( this.Band ) )
					{
						this.sourceColumn = matchingColumn;
						this.BumpSummaryCalculationsVersion( );
					}
				}

				if ( null != this.serializedSummaryPositionColumnId )
				{
					UltraGridColumn matchingColumn = this.Band.GetMatchingColumn( this.serializedSummaryPositionColumnId );
					if ( null != matchingColumn )
					{
						this.summaryPositionColumn = matchingColumn;
						this.BumpSummaryCalculationsVersion( );
					}
				}
			}
		}

		#endregion // OnDeserializationComplete

		// SSP 6/30/04 - UltraCalc
		//
		#region UltraCalc Related

		#region FormulaHolder

		internal FormulaHolder FormulaHolder
		{
			get
			{
				if ( null == this.formulaHolder )
					this.formulaHolder = new FormulaHolder( this );

				return this.formulaHolder;
			}
		}

		// MD 5/22/09 - TFS16348
		internal bool HasFormulaHolder
		{
			get { return this.formulaHolder != null; }
		}

		#endregion // FormulaHolder

		#region CalcReference

		internal SummarySettingsReference CalcReference
		{
			get
			{
				return (SummarySettingsReference)this.FormulaHolder.CalcReference;
			}
		}

		#endregion // CalcReference

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return this.FormulaHolder.HasCalcReference;
			}
		}

		#endregion // HasCalcReference

		#region HasActiveFormula

		// SSP 7/13/04 - UltraCalc
		// Added HasActiveFormula property.
		//
		internal bool HasActiveFormula
		{
			get
			{
				return null != this.Formula 
					&& this.Formula.Length > 0 
					&& null != this.FormulaHolder.CalcManager;
			}
		}

		#endregion // HasActiveFormula

		#region IsStillValid
        
		// SSP 9/1/04
		// Added IsStillValid property.
		//
		internal bool IsStillValid
		{
			get
			{
				return ! this.Disposed && null != this.Band && this.Band.IsStillValid
					&& null != this.ParentCollection && this.ParentCollection.IndexOf( this ) >= 0;
			}
		}

		#endregion // IsStillValid

		#region ShowCalculatingTextResolved

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		internal bool ShowCalculatingTextResolved
		{
			get
			{
				DefaultableBoolean showCalculatingText = this.showCalculatingText;

				if ( DefaultableBoolean.Default == showCalculatingText && this.Band.HasOverride )
					showCalculatingText = this.Band.Override.ShowCalculatingText;

				if ( DefaultableBoolean.Default == showCalculatingText && this.Band.Layout.HasOverride )
					showCalculatingText = this.Band.Layout.Override.ShowCalculatingText;
				
				return (showCalculatingText == DefaultableBoolean.True);					
			}
		}

		#endregion // ShowCalculatingTextResolved

		#region IFormulaProvider.Participant

		IUltraCalcParticipant IFormulaProvider.Participant
		{
			get
			{
				return this.Layout.Grid;
			}
		}

		#endregion // IFormulaProvider.Participant

		#region IFormulaProvider.Reference

		IUltraCalcReference IFormulaProvider.Reference
		{
			get
			{
				return this.CalcReference;
			}
		}

		#endregion // IFormulaProvider.Reference

		#region IFormulaProvider.CalcManager

		IUltraCalcManager IFormulaProvider.CalcManager
		{
			get
			{
				return null != this.Layout ? this.Layout.CalcManager : null;
			}
		}

		#endregion // IFormulaProvider.CalcManager

		#region FormulaAbsoluteName

		// SSP 8/31/04 UWG3673
		// Added FormulaAbsoluteName.
		//
		/// <summary>
		/// Returns the absolute name of the summary settings that you can use to refer to it in formulas.
		/// </summary>
		[ Browsable( false ) ]
		public string FormulaAbsoluteName
		{
			get
			{
				return null != this.CalcReference ? this.CalcReference.AbsoluteName : null;
			}
		}

		#endregion // FormulaAbsoluteName

		#region NotifyCalcManager_ValueChanged

		// SSP 6/23/05 BR04577
		// 
		internal void NotifyCalcManager_ValueChanged( )
		{
			if ( this.HasCalcReference )
				RowsCollection.NotifyCalcManager_ValueChanged( this.CalcReference );
		}

		#endregion // NotifyCalcManager_ValueChanged

		#endregion // UltraCalc Related

        #endregion // Internal/Private methods

        #region SummarySettingsConverter Class

        // SSP 8/10/04 - Implemented design time serialization of summaries
		// Added SummarySettingsConverter class.
		//
		/// <summary>
		/// RowLayoutTypeConverter type converter.
		/// </summary>
		public sealed class SummarySettingsConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if ( destinationType == typeof( InstanceDescriptor ) ) 
				{
					return true;
				}
				
				return base.CanConvertTo( context, destinationType );
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//private int GetColumnRelativeIndex( UltraGridColumn column )
			private static int GetColumnRelativeIndex( UltraGridColumn column )
			{
				return column.IsBound 
					? column.Index
					: column.UnboundRelativePosition;
			}

            /// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
            public override object ConvertTo(ITypeDescriptorContext context, 
				System.Globalization.CultureInfo culture, object value, Type destinationType ) 
			{
				if ( destinationType == null ) 
				{
					throw new ArgumentNullException( "destinationType" );
				}

				if ( destinationType == typeof( InstanceDescriptor ) && value is SummarySettings ) 
				{
					SummarySettings item = (SummarySettings)value;

					ConstructorInfo ctor = typeof( SummarySettings ).GetConstructor( 
						new Type[] {
									   typeof( string ), // Key
									   typeof( SummaryType ), // summary type
									   typeof( string ), // formula
									   typeof( string ), // source column key
									   typeof( int ), // source column relative index
									   typeof( bool ), // is source column bound
									   typeof( string ), // source column band key
									   typeof( int ), // source column band index
									   typeof( SummaryPosition ), // summary position
									   typeof( string ), // summary position column key
									   typeof( int ), // summary position column index
									   typeof( bool ) // is summary position column bound
								   } );

					if ( null != ctor ) 
					{
						// false as the last parameter here causes generation of a local variable for the type 

												// SSP 10/21/04 UWG3640
						// If the sourceColumn/positionColumn are null off the item summary settings then
						// use serializedSourceColumnId/serializedSummaryPositionColumnId if any.
						//
						// ----------------------------------------------------------------------------------
						

						return new InstanceDescriptor( ctor, 
							new object[] { 
											 item.Key,
											 item.summaryType,
											 item.Formula,
											 null != item.sourceColumn ? item.sourceColumn.Key : ( null != item.serializedSourceColumnId ? item.serializedSourceColumnId.Key : null ),

											 // MD 7/26/07 - 7.3 Performance
											 // FxCop - Mark members as static
											 //null != item.sourceColumn ? this.GetColumnRelativeIndex( item.sourceColumn ) : ( null != item.serializedSourceColumnId ? item.serializedSourceColumnId.RelativeIndex : -1 ),
											 null != item.sourceColumn ? SummarySettingsConverter.GetColumnRelativeIndex( item.sourceColumn ) : ( null != item.serializedSourceColumnId ? item.serializedSourceColumnId.RelativeIndex : -1 ),

											 null != item.sourceColumn ? item.sourceColumn.IsBound : ( null != item.serializedSourceColumnId ? item.serializedSourceColumnId.Bound : false ),
											 null != item.sourceColumn ? item.sourceColumn.Band.Key : item.serializedSourceColumnBandKey,
											 null != item.sourceColumn ? item.sourceColumn.Band.Index : item.serializedSourceColumnBandIndex,
											 item.summaryPosition,
											 null != item.summaryPositionColumn ? item.summaryPositionColumn.Key : ( null != item.serializedSummaryPositionColumnId ? item.serializedSummaryPositionColumnId.Key : null ),

											 // MD 7/26/07 - 7.3 Performance
											 // FxCop - Mark members as static
											 //null != item.summaryPositionColumn ? this.GetColumnRelativeIndex( item.summaryPositionColumn ) : ( null != item.serializedSummaryPositionColumnId ? item.serializedSummaryPositionColumnId.RelativeIndex : -1 ),
											 null != item.summaryPositionColumn ? SummarySettingsConverter.GetColumnRelativeIndex( item.summaryPositionColumn ) : ( null != item.serializedSummaryPositionColumnId ? item.serializedSummaryPositionColumnId.RelativeIndex : -1 ),

											 null != item.summaryPositionColumn ? item.summaryPositionColumn.IsBound : ( null != item.serializedSummaryPositionColumnId ? item.serializedSummaryPositionColumnId.Bound : false )
										 },
							false );
						// ----------------------------------------------------------------------------------
					}
				}

				return base.ConvertTo( context, culture, value, destinationType );
			}		
		}
		
		#endregion // RowLayoutTypeConverter Class

		#region ColumnsListUITypeEditor

		// SSP 8/10/04 - Implemented design time serialization of summaries
		// Added ColumnsListUITypeEditor class.
		//
		/// <summary>
		/// Base class for a UITypeEditor that provides a list
		/// </summary>
		/// <remarks>
		/// <p class="body">Provides a dropdown list of items.</p>	
		/// </remarks>
		[System.Runtime.InteropServices.ComVisible(false)]
		public class ColumnsListUITypeEditor : UITypeEditorListBase 
		{
			private class BooleanIndicator
			{
				internal bool val = false;
			}

			private class ColumnItem
			{
				internal UltraGridColumn column = null;
				private BooleanIndicator includeBandKeysInNames = null;

				internal ColumnItem( UltraGridColumn column, BooleanIndicator includeBandKeysInNames )
				{
					this.column = column;
					this.includeBandKeysInNames = includeBandKeysInNames;
				}

				public override string ToString( )
				{
					// If no column was specified then display None in the drop down list.
					// This way the user can reset the property to null.
					//
					if ( null == this.column )
						return SR.GetString( "ColumnsUITypeEditorList_None_Entry" );

					string s = this.column.Key;

					if ( null == s || 0 == s.Length )
						s = string.Format( "Column# [{0}]", this.column.Index );

					if ( this.includeBandKeysInNames.val )
						s = string.Format( "{0} - {1}", this.column.Band.Key, s );

					return s;
				}

				public override int GetHashCode( )
				{
					return null != this.column 
						? this.column.GetHashCode( )
						: 0;
				}

				public override bool Equals( object value )
				{
					return value is ColumnItem && this.column == ((ColumnItem)value).column;
				}
			}

			/// <summary>
			/// Returns the set of items to show on the list.
			/// </summary>
            /// <param name="context">An ITypeDescriptorContext that can be used to gain additional context information.</param>
            /// <param name="provider">An IServiceProvider that this editor can use to obtain services.</param>
            /// <param name="value">The object to edit.</param>
			/// <param name="currentEntry">The current entry selected on the list.</param>
            /// <returns>An array of objects containing the list items.</returns>
			protected override object[] GetListItems( ITypeDescriptorContext context, 
							IServiceProvider provider, object value, ref object currentEntry )
			{                
				// MD 8/10/07 - 7.3 Performance
				// Use generics	
				//ArrayList list = new ArrayList( );
				List<ColumnItem> list = new List<ColumnItem>();

				SummarySettings summarySettings = context.Instance as SummarySettings;
				if ( null != summarySettings && null != summarySettings.Band && null != context.PropertyDescriptor )
				{
					Queue queue = new Queue( );
					queue.Enqueue( summarySettings.Band );

					// SourceColumn property can be assigned a column from any band that's a 
					// descendant of the summary's band. So process the descendant bands as well.
					//
					bool includeDescendantBands = "SourceColumn" == context.PropertyDescriptor.Name;
					BooleanIndicator includeBandKeyInColumnNames = new BooleanIndicator( );
					int includedBandCount = 0;

					while ( queue.Count > 0 )
					{
						UltraGridBand band = (UltraGridBand)queue.Dequeue( );
						int origListCount = list.Count;
						foreach ( UltraGridColumn column in band.Columns )
						{
							if ( ! column.IsChaptered )
							{
								ColumnItem ci = new ColumnItem( column, includeBandKeyInColumnNames );
								list.Add( ci );
								if ( column == value || ci.Equals( value ) )
									currentEntry = ci;
							}
						}

						// If we added at least one column from the band then incremenet the
						// included bands count.
						//
						if ( origListCount != list.Count )
							includedBandCount++;

						if ( includeDescendantBands )
						{
							foreach ( UltraGridBand b in band.Layout.SortedBands )
							{
								if ( band != b && b.IsDescendantOfBand( band ) )
									queue.Enqueue( b );
							}
						}
					}

					// If we added columns from more than one band to the list then display the
					// band keys along with the column names in the list.
					//
					includeBandKeyInColumnNames.val = includedBandCount > 1;

					// SSP 8/19/04 UWG3636
					// Also add "None" entry so the users can reset the property to a null value.
					//
					if ( list.Count > 0 )
						list.Insert( 0, new ColumnItem( null, includeBandKeyInColumnNames ) );
				}

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//return (object[])list.ToArray( typeof( ColumnItem ) );
				return list.ToArray();
			}

			/// <summary>
			/// Validates an item selected by the user on the list and returns it.
			/// </summary>
            /// <param name="context">An ITypeDescriptorContext that can be used to gain additional context information.</param>
            /// <param name="provider">An IServiceProvider that this editor can use to obtain services.</param>
            /// <param name="value">The object to edit.</param>
			/// <param name="selectedEntry">The currently selected entry.</param>
			/// <returns>The validated entry.</returns>
			protected override object ValidateEditItem ( ITypeDescriptorContext context, 
				IServiceProvider provider, object value, object selectedEntry )
			{
				if ( selectedEntry is ColumnItem )
					return ((ColumnItem)selectedEntry).column;

				return selectedEntry;
			}
		}

		#endregion // ColumnsListUITypeEditor

	}

}
