#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Collections;
    using System.Data;
    using Infragistics.Shared;
    using Infragistics.Win;

    /// <summary>
    /// Returns a reference to a collection of Cell objects. This property is 
    /// read-only at run-time. This property is not available at design-time.
    /// </summary><remarks><para>This property returns a reference to a collection of 
    /// Cell objects that can be used to retrieve references to the Cell objects that, 
    /// for the Row object, belong to a row, or for the Selected object, are 
    /// currently selected. You can use this reference to access any of the returned 
    /// collection's properties or methods, as well as the properties or methods of 
    /// the objects within the collection.</para><para>For the Row object, the 
    /// returned collection provides a way to work with the cells that constitute 
    /// the row.</para><para>For the Selected object, as cells are selected and 
    /// deselected, their corresponding Cell objects are added to and removed from 
    /// the SelectedCells collection returned by this property. When a cell is 
    /// selected or deselected, the BeforeSelectChange event is generated.</para>
    /// <para>The Count property of the returned collection can be used to determine 
    /// the number of cells that either belong to a row or are currently 
    /// selected.</para></remarks>
    public class CellsCollection : KeyedSubObjectsCollectionBase
    {
        private UltraGridRow    row;
		private int		columnsCollectionVersion;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="row">The UltraGridRow</param>
        public CellsCollection( Infragistics.Win.UltraWinGrid.UltraGridRow row )
        {
            this.row = row;
			
			// SSP 8/14/03 - Optimizations
			//
			//this.columnsCollectionVersion = row.Band.ColumnsVersion;
			this.columnsCollectionVersion = -1;
        }

		/// <summary>
		/// Returns true
		/// </summary>
        public override bool IsReadOnly { get { return true; } }

        internal UltraGridRow Row
        {
            get 
            {
                return this.row;
            }
        }

        
		/// <summary>
		/// indexer
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridCell this[ int index ] 
        {
            get
            {
                this.SynchronizeWithColumnsColection();

                return (UltraGridCell)this.GetItem( index );
            }
        }

		/// <summary>
		/// indexer (by string key)
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridCell this[ String key ] 
        {
            get
            {
                this.SynchronizeWithColumnsColection();

                return (UltraGridCell)this.GetItem( key );
            }
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// resets original value property to null for each cell that
		//        /// is created 
		//        /// </summary>
		//#endif
		//        internal void ResetOriginalValues()
		//        {
		//            for ( int i = 0; i < this.List.Count; i++ )
		//            {
		//                UltraGridCell cell = (UltraGridCell)this.List[i];
		//                if ( null != cell )
		//                {
		//                    cell.ResetOriginalValue();
		//                }
		//            }
		//        }

		#endregion Not Used

        
		/// <summary>
		/// indexer (by column)
		/// </summary>
        public UltraGridCell this[ UltraGridColumn column ] 
        {
            get
            {
                this.SynchronizeWithColumnsColection();

				// JJD 12/04/01 - UWG837
				// Use the column's index instead of its Key to
				// get the cell since it may not have a key.
				//
//                return (UltraGridCell)this.GetItem( column.Key );
                return (UltraGridCell)this.GetItem( column.Index );
            }
        }

        /// <summary>
        /// Returns true if the cell has already been created for this column
        /// </summary>
        /// <param name="index">The index of the cell</param>
        /// <returns>Returns true if the cell has already been created for this column</returns>
        public bool HasCell( int index ) 
		{
            this.SynchronizeWithColumnsColection();

            return ( null != this.List[index] );
        }

        /// <summary>
        /// Returns true if the cell has already been created for the column identified by 
        /// the specified key. The column by that key must exist in the associated band's 
        /// columns collection otherwise an exception will be thrown.
        /// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <remarks>
        /// <p class="body">
        /// <b>CellsCollection</b> creates cells lazily for efficiency reasons. It creates 
        /// cells as they are accessed via the indexer or the GetItem method. <b>HasCell</b> 
        /// can be used to find out if a cell has already been created for a column. Note that
        /// there must be a column with the specified key in the associated band's columns 
        /// collection otherwise this method will throw an exception. If the intention is
        /// to find out if a column with the specified key exists then use the <b>Exists</b> 
        /// method instead.
        /// </p>
		/// <p class="body">
		/// <b>Note</b> that for better efficiency you may want to use HasCell overloads that take 
		/// in column index or a column object as they do not require searching the collection for
		/// the matching column key.
		/// </p>
        /// </remarks>
        public bool HasCell( String key ) 
        {
            return HasCell( this.Row.Band.Columns.IndexOf( key ) );
        }

        /// <summary>
        /// Returns true if the cell has already been created for this column.
        /// </summary>
        public bool HasCell( UltraGridColumn column ) 
        {
			// SSP 12/24/02
			// Use the Index property of the column since it caches the index.
			//
			//int index = this.Row.Band.Columns.IndexOf( column );
			int index = column.Index;

            return HasCell( index );
        }

        /// <summary>
        /// Returns the index of the item in the collection that has the passed in key or -1 if key not found.
        /// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <returns>The index of the item in the collection that has the passed in key, or -1
        /// if no item is found.</returns>
        public override int IndexOf(String key) 
        {
            // get the index from the columns collection
            //
            return this.Row.Band.Columns.IndexOf( key );
        }

		/// <summary>
		/// Returns the cell of the columns that has the key
		/// </summary>
        /// <returns>Returns the cell of the columns that has the key</returns>
        protected override IKeyedSubObject GetItem( String key ) 
        {
			// SSP 6/18/02 UWG1254
			// Don't return null if the passed in key is invalid. Throw
			// an exception instead.
			// Commented out the original code and added the new code.
			// 
			// ---------------------------------------------------
			
			// Column's indexer will throw the exception if the key
			// is invalid.
			//
			UltraGridColumn column = this.Row.BandInternal.Columns[key];

			return this[column];
			// ---------------------------------------------------
        }

		/// <summary>
		/// Returns the cell at the specified index
		/// </summary>
        /// <param name="index">Index of the object to retrieve.</param>
        /// <returns>The object at the index</returns>
        public override object GetItem( int index )
        {
            UltraGridCell cell = (UltraGridCell)this.List[index];

            // if the slot for this cell is null then create it now
            //
            if ( null == cell )
            {
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Added UltraGridFilterCell class.
				//
                //cell = new UltraGridCell( this, this.Row.BandInternal.Columns[ index ] );
				cell = this.Row.CreateCell( this.Row.BandInternal.Columns[ index ] );

                // hook up the prop change notifier event
                //
                cell.SubObjectPropChanged += this.SubObjectPropChangeHandler;

                this.List[index] = cell;
            }

            return cell;
        }

		// SSP 1/24/05 BR01874
		// Added InternalRemoveAt and InternalClear methods that, unlike the base class
		// implementations, do not call GetItem and cause the unallocated cells to be
		// allocated.
		//
		#region InternalRemove

		private new void InternalRemove( int index )
		{
			ArrayList list = this.List;
			UltraGridCell cell = (UltraGridCell)list[ index ];
			if ( null != cell )
				((IKeyedSubObject)cell).OnRemovedFromCollection( this );

			list.RemoveAt( index );
		}

		#endregion // InternalRemove

		#region InternalClear

		private new void InternalClear( )
		{
			ArrayList list = this.List;

			for ( int i = 0; i < list.Count; i++ )
			{
				UltraGridCell cell = (UltraGridCell)list[ i ];
				if ( null != cell )
					((IKeyedSubObject)cell).OnRemovedFromCollection( this );
			}

			list.Clear( );
		}

		#endregion // InternalClear

		/// <summary>
		/// Set count to the columns count
		/// </summary>
        private void SynchronizeWithColumnsColection() 
        {
			// SSP 8/14/03 - Optimizations
			// Only check the version number instead of checking for both the count and the version number
			// for verification purposes.
			//
			if ( this.row.BandInternal.ColumnsVersion == this.columnsCollectionVersion )
				return;

			UltraGridBand band = this.Row.BandInternal;			
			ColumnsCollection columns = band.Columns;

			// get the number of columns
			//
			int columnCount = columns.Count;

			// SSP 8/14/03 - Optimizations
			// Only check the version number instead of checking for both the count and the version number
			// for verification purposes.
			// Related to the changed above. Commented out below block.
			//
			

			// if there are no columns then just clear the cells collection
			// and return
			//
			if ( 0 == columnCount )
			{
				// SSP 3/9/04
				// Also set the columnsCollectionVersion to prevent anti-recursion because
				// InternalClear in the base calls Count which will call this method.
				//
				this.columnsCollectionVersion = band.ColumnsVersion;

				this.InternalClear();
				return;
			}

			// SSP 2/23/04 
			// This doesn't sound right. We are supposed to be setting the capacity
			// to the number of slots we are going to have and not to the current
			// slots in the list.
			// Also added list local variable replaced this.List with it for optimization
			// reasons.
			//
			//this.List.Capacity = this.List.Count;
			ArrayList list = this.List;
			if ( list.Count < columnCount )
                list.Capacity = columnCount;

			int columnIndex;

			// JJD 10/17/01
			// loop over the cells collection and verify
			// that the indexes match the indexes in the
			// coumns collection
			//
			for ( int i = 0; i < list.Count; ++i )
			{
				// if the cell slot is null continue
				//
				if ( list[i] == null )
					continue;

				// get the index of the cell's column
				//
				columnIndex = ((UltraGridCell)list[i]).Column.Index;

				// If the cell's column's index is less than
				// zero it means that this column have been
				// removed from the collection, so remove
				// the cell and continue
				//
				if ( columnIndex < 0 )
				{
					this.InternalRemove( i );
					--i;
					continue;
				}

				// If the cell's column's index is less than
				// the cell's index then remove the prior
				// cell slot, decrement i and continue.
				// Note: this should always be a null slot
				// since non-null slots would have already
				// been verified
				//
				if ( columnIndex < i )
				{
					System.Diagnostics.Debug.Assert( list[i - 1] == null, "Cell slot filled, should be null." );
					
					this.InternalRemove( i - 1 );
					
					// Decrement i by 2 to account for this cell moving
					// up a slot and the ++ of the for loop so that
					// we can check this cell again in case multiple
					// empty slots need to be removed
					//
					i -= 2;
					
					continue;
				}

				// If the cell's column's index is greater than
				// the cell's index then add a null slot and 
				// continue.
				//
				if ( columnIndex > i )
				{
					list.Insert( i, null );
					continue;
				}
			}

			// if our count is greater then get rid of
			// excess cell slots
			//
			if ( columnCount < list.Count ) 
			{
				list.RemoveRange( columnCount, list.Count - columnCount ); 
			}
			else
			{
				// append empty slots into the collection to handle
				// every column.
				//
				// Note: we lazily create the cells in case no one
				//       asks for them.
				//
				while ( columnCount > list.Count ) 
				{
					list.Add( null );
				}
			}

			// JJD 10/17/01
			// Cache the version number of the columns collection
			//
			this.columnsCollectionVersion = band.ColumnsVersion;

        }

        /// <summary>
        /// Abstract property that specifies the initial capacity
        /// of the collection
        /// </summary>
        protected override int InitialCapacity
        {
            get
            {
                return Row.Band.Columns.Count;
            }
        }

        /// <summary>
        /// The collection as an array of objects
        /// </summary>
        public override object[] All 
        {
            get 
            {
                this.SynchronizeWithColumnsColection();

                object[] array = new object[ this.Count ];

                // we need to call getobject so that 
                // any cells that haven't been created 
                // will be created now before we
                // return the array
                //
                for ( int i = 0; i < this.Count; i++ )
                {
                    array[ i ] = this.GetItem( i );
                }

                return array;
            }
            
            set 
            {
				throw new NotSupportedException();
            }
        }

        /// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// pass the notification along to our listeners
			//
			// SSP 4/12/04 - Virtual Binding - Cell Selection Optimization
			// We need to notify the row of the change which will notify the VisibleRow
			// which will invalidate itself. Before we were invalidating the cell in the
			// InternalSelect of the cell. Doing so in there is expensive because in order
			// to do that we need to get ui element which searches through the ui element
			// hierarchy. We would be doing that even for cells that are not visible. By
			// doing it through visible row, we will be invalidating only the visible rows.
			// Commented out the following line and added code below it.
			//
			// --------------------------------------------------------------------------
			//this.NotifyPropChange( PropertyIds.Cell, propChange );
			PropChangeInfo pi = new PropChangeInfo( this, PropertyIds.Cell, propChange );
			this.NotifyPropChange( pi );
			if ( null != this.row )
				this.row.NotifyPropChange( PropertyIds.Cells, pi );
			// --------------------------------------------------------------------------
		}
     
        
		/// <summary>
		/// IEnumerable Interface Implementation
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public CellEnumerator GetEnumerator() // non-IEnumerable version
        {
            this.SynchronizeWithColumnsColection();

            return new CellEnumerator(this);
        }


		// SSP 8/23/02 UWG1317
		// Overrode Count because the base class would return 0 unless the
		// cells are synchronized with the columns collection.
		// Look at SynchronizeWithColumnsColection() for more info.
		//
		/// <summary>
		/// Overridden. Returns the number of elements in this collection.
		/// </summary>
		public override int Count
		{
			get
			{
				this.SynchronizeWithColumnsColection( );

				// After syncronizing the cells with the columns collection we
				// will have the right cells count. Note: checking for columns
				// count would also work. However might just as well syncronize.
				//
				return base.Count;
			}
		}

		// SSP 5/5/03 - Optimizations
		// Added a method to get the cell if it was allocated.
		//
		internal UltraGridCell GetCellIfAllocated( UltraGridColumn column )
		{
			this.SynchronizeWithColumnsColection( );

			// SSP 11/12/04
			// Check for column's Index being -1.
			//
			//return (UltraGridCell)this.List[ column.Index ];
			int columnIndex = column.Index;
			return columnIndex >= 0 ? (UltraGridCell)this.List[ columnIndex ] : null;
		}

		// SSP 5/19/04 UWG3088
		// Overrode OnDispose so we can dispose the cells in the collection. 
		// When an appearance is assigned to a cell object, the cell object hooks into
		// the appearance and thus the appearance object will keep a reference to the
		// cell. When the row is diposed of, the row's cells will still be referenced
		// by the appearance and thus there could be a memory leak. So we need to
		// dispose of the cells when the row gets disposed off.
		//
		/// <summary>
		/// Overridden. Disposes cells contained in the collection.
		/// </summary>
		protected override void OnDispose( )
		{
			ArrayList list = this.List;
			for ( int i = 0; i < list.Count; i++ )
			{
				UltraGridCell cell = list[i] as UltraGridCell;
				if ( null != cell )
					cell.Dispose( );
			}

			this.List.Clear( );
			this.columnsCollectionVersion--;
		}

		// SSP 6/21/04 UWG3398
		// Added RemoveCellsFromSelected method so when a row is removed, we also remove the
		// cells of the row from the selected cells collection.
		// 
		internal void RemoveCellsFromSelected( SelectedCellsCollection selectedCells )
		{
			if ( null != selectedCells && selectedCells.Count > 0 )
			{
				for ( int i = 0; i < this.List.Count; i++ )
				{
					UltraGridCell cell = this.List[i] as UltraGridCell;

					if ( null != cell )
						selectedCells.Remove( cell );
				}
			}
		}

		// SSP 7/13/05 BR04998
		// Overrode CopyTo so we can create cells if not already created.
		// 
		/// <summary>
		/// Copies the items into the array
		/// </summary>
		/// <param name="array">Target array</param>
		/// <param name="index">Index where to begin copying</param>
		public override void CopyTo(IKeyedSubObject[] array, int index) 
		{
			for ( int i = 0, count = this.Count; i < count; i++ )
				array[ index + i ] = this[ i ];
		}

		#region DeallocateCellsHelper

		// SSP 8/26/06 - NAS 6.3
		// Added ClearCells method.
		// 
		internal bool DeallocateCellsHelper( UltraGridCell activeCell, bool retainUnboundCellData )
		{
			bool allCellsCleared = true;

			ArrayList list = this.List;
			for ( int i = 0; i < list.Count; i++ )
			{
				UltraGridCell cell = list[i] as UltraGridCell;
				if ( null != cell )
				{
					if ( ! cell.Selected && cell != activeCell 
						&& ( ! retainUnboundCellData || ! cell.HasUnboundData || cell.Column.IsBound ) )
					{
						list[i] = null;
						cell.Dispose( );					
					}
					else
					{
						// Otherwise reset all the properties of the cells to their default values.
						// 
						cell.ClearSettings( retainUnboundCellData );
						allCellsCleared = false;
					}
				}
			}

			return allCellsCleared;
		}

		#endregion // DeallocateCellsHelper
	}

	#region CellEnumerator Class Definition
	
	/// <summary>
	/// Enumerator for the CellsCollection
	/// </summary>
	public class CellEnumerator : IEnumerator
	{   
		private IEnumerator enumerator = null;

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="cells">The CellsCollection to enumerate</param>
		public CellEnumerator( CellsCollection cells )
		{
			this.enumerator = ((IEnumerable)cells).GetEnumerator( );
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="cells">The SelectedCellsCollection to enumerate</param>
		public CellEnumerator( SelectedCellsCollection cells )
		{
			this.enumerator = ((IEnumerable)cells).GetEnumerator( );
		}

		void IEnumerator.Reset( )
		{
			this.enumerator.Reset( );
		}

		bool IEnumerator.MoveNext( )
		{
			return this.enumerator.MoveNext( );
		}

		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}
        
		/// <summary>
		/// Type-safe version of Current
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridCell Current 
		{
			get
			{
				return (UltraGridCell)this.enumerator.Current;
			}
		}

        /// <summary>
        /// Advances the enumerator to the next cell in the collection.
        /// </summary>
        /// <returns><b>True</b> if the enumerator was successfully advanced to the next element; <b>false</b> if the enumerator has passed the end of the collection</returns>
        public bool MoveNext()
        {
            return ((IEnumerator)this).MoveNext();
        }
    }

	

	#endregion // CellEnumerator Class Definition
}
