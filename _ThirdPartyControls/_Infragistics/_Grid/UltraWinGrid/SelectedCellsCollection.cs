#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;

namespace Infragistics.Win.UltraWinGrid
{
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	/// Summary description for SelectedCellsCollection.
	/// </summary>
	// SSP 4/12/04 - Virtual Binding Related
	// Changed the base class from SubObjectsCollectionBase to SparseCollectionBase because we
	// want a fast Contains (as well as remove but to a lesser extent).
	//
	//public class SelectedCellsCollection : SubObjectsCollectionBase
	public class SelectedCellsCollection : SparseCollectionBase
	{
		// SSP 12/11/02 UWG1835
		// Added a back reference to the selected object.
		//
		private Selected selected = null;

		// SSP 12/13/02 UWG1835
		// SSP 4/12/04 - Virtual Binding Related
		// Changed the base class from SubObjectsCollectionBase to SparseCollectionBase because we
		// want a fast Contains (as well as remove but to a lesser extent).
		// We don't need list anymore.
		//
		//private ArrayList list = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		public SelectedCellsCollection()
		{
			
		}

		/// <summary>
		/// returns whether the collection is read-only
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				// SSP 12/11/02 UWG1835
				// Added methods for clearing the selected rows, cells or columns as well as adding a
				// range of objects at the same time. We want to return true only for the main selected
				// object and not for the onces that get passed in various selectchange events.
				//
				//return true;

				return null == this.Grid || this.Selected != this.Grid.Selected || this != this.Grid.Selected.Cells;
			}
		}
			
		// SSP 3/03/03 - Row Layout Functionality
		// This change doesn't have much to do with the row layout functionality, it is an 
		// optimization for selection. Checking to see if the item being added to the list
		// is already in the list is inefficient so added an overload with which you can
		// specify not to do that.
		// 
		internal int InternalAdd( UltraGridCell selectedCell ) 
		{
			return this.InternalAdd( selectedCell, true );
		}

		internal int InternalAdd( UltraGridCell selectedCell, bool validateAlreadyExists ) 
		{
			// SSP 7/8/02 UWG1328
			// If the item is already in the collection don't add it again.
			//
			// SSP 3/03/03 - Row Layout Functionality
			// This change doesn't have much to do with the row layout functionality, it is an 
			// optimization for selection. Checking to see if the item being added to the list
			// is already in the list is inefficient so added an overload with which you can
			// specify not to do that.
			// Commented out below code and added replacement below.
			// --------------------------------------------------------------------------------
			//if ( this.Contains( selectedCell ) )
			//{
			//	Debug.Assert( false, "Cell is already in the selected cells collection. Can't added it again." );
			//	return this.IndexOf( selectedCell );
			//}
			if ( validateAlreadyExists )
			{
				int index = this.IndexOf( selectedCell );

				if ( index >= 0 )
				{
					Debug.Assert( false, "Cell is already in the selected cells collection. Can't added it again." );
					return index;
				}
			}
			// --------------------------------------------------------------------------------

			return base.InternalAdd( selectedCell );
		}

		internal int InternalAdd( object[] selectedCells ) 
		{
			int lastAdd = 0;
			
			for ( int i = 0; i < selectedCells.Length; i++ )
			{
				Debug.Assert( selectedCells[i] is UltraGridCell, "Invalid object passed into SelectedCellsCollection: " );

				if ( selectedCells[i] is UltraGridCell )
					lastAdd = base.InternalAdd( selectedCells[i] as UltraGridCell );
			}

			return lastAdd;
		}

		// SSP 1/12/05 BR0155 UWG3736
		// Added InternalClear method.
		//
		internal new void InternalClear( )
		{
			base.InternalClear( );
		}

		internal void Remove( UltraGridCell selectedCell ) 
		{
			base.InternalRemove( selectedCell );
		}
        
		/// <summary>
		/// IEnumerable Interface Implementation
		/// </summary>
        /// <returns>A type safe enumerator</returns>
		public CellEnumerator GetEnumerator() // non-IEnumerable version
		{
			return new CellEnumerator(this);
		}

		// JJD 10/04/01
		// Added the indexer
		//
		/// <summary>
		/// Index into collection
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridCell this[ int index ] 
		{
			get
			{
				// SSP 2/20/07 BR19262
				// If the grid's row collection has been dirtied (because we recieved Reset notification etc...)
				// then we need to verify that before returning Count here otherwise the SelectedRowsCollection
				// could return rows that have been deleted.
				// 
				if ( null != this.selected )
					this.selected.VerifyRowsNotDirty( );

				return (UltraGridCell)base.GetItem(index);
			}
		}

		#region Count

		// SSP 2/20/07 BR19262
		// If the grid's row collection has been dirtied (because we recieved Reset notification etc...)
		// then we need to verify that before returning Count here otherwise the SelectedRowsCollection
		// could return rows that have been deleted.
		// 
		/// <summary>
		/// Returns the number of items in the collection.
		/// </summary>
		public override int Count
		{
			get
			{
				int count = base.Count;

				if ( count > 0 && null != this.selected )
				{
					if ( this.selected.VerifyRowsNotDirty( ) )
						count = base.Count;
				}

				return count;
			}
		}

		#endregion // Count

		// SSP 12/11/02 UWG1835
		// Added methods for clearing the selected rows, cells or columns as well as adding a
		// range of objects at the same time.
		//
		#region Fix For UWG1835

		#region InternalSetList

		internal void InternalSetList( SelectedCellsCollection cells )
		{
			this.InternalClear( );
			this.InternalAddRange( cells );
		}

		#endregion // InternalSetList

		#region All

		// SSP 12/11/02 UWG1835
		// Added methods for clearing the selected rows, cells or columns as well as adding a
		// range of objects at the same time. We want to return true only for the main selected
		// object and not for the onces that get passed in various selectchange events.
		// Overrode All so we can prevent the user from calling the Set.
		//
		/// <summary>
		/// The collection as an array of objects
		/// </summary>
		[ Browsable( false ), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public override object[] All 
		{
			get 
			{
				return base.All;
			}
			set 
			{
				if ( this.IsReadOnly )
					throw new NotSupportedException();

				// Clear existing selected cells and call AddRange to add the passed in cells.
				//
				this.Clear( );

				if ( value.Length > 0 )
				{
					UltraGridCell[] cells = new UltraGridCell[ value.Length ];

					for ( int i = 0; i < value.Length; i++ )
					{
						cells[i] = (UltraGridCell)value[i];
					}

					this.AddRange( cells );
				}
			}
		}

		#endregion // All

		#region Initialize

		internal void Initialize( Selected selected )
		{
			this.selected = selected;
		}

		#endregion // Initialize

		#region Selected

		internal Infragistics.Win.UltraWinGrid.Selected Selected
		{
			get
			{
				return this.selected;
			}
		}

		#endregion // Selected

		#region Grid

		internal UltraGrid Grid
		{
			get
			{
				return null != this.selected ? this.selected.Grid : null;
			}
		}

		#endregion // Grid

		#region Clear

		/// <summary>
		/// Clears the collection unselecting any selected cells.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="AddRange"/>, <seealso cref="UltraGridCell.Selected"/></p>
		/// </remarks>
		public void Clear( )
		{			
			UltraGrid grid = this.Grid;

			if ( this.IsReadOnly )
				// SSP 6/23/03 - Localization
				//
				//throw new NotSupportedException( "Clear operation is not supported." );
				throw new NotSupportedException( SR.GetString( "LER_NotSupportedException_346" ) );

			// IsReadOnly should ensure that following condition in Debug.Assert is met before
			// returning true.
			//
			Debug.Assert( this == grid.Selected.Cells, "Grid's selected and this are not the same !" );

			// Call ClearSelectedCells to clear the selected cells. It also fires the select change
			// events.
			//
			grid.ClearSelectedCells( );
		}

		#endregion // Clear

		#region AddRange

		/// <summary>
		/// Selects cells contained in the passed in array keeping existing selected cells selected.
		/// </summary>
		/// <param name="cells"></param>
		/// <remarks>
		/// <p>This method can be used to select a range of cells. It will keep the existing selected cells and select the passed in cells.</p>
		/// <p>You can use UltraGridCell.Selected property to select or unselect individual cells.</p>
		/// <p><seealso cref="Clear"/>, <seealso cref="UltraGridCell.Selected"/></p>
		/// </remarks>		
		public void AddRange( UltraGridCell[] cells )
		{
			// If the passed in array is of length 0, then do nothing.
			//
			if ( cells.Length <= 0 )
				return;

			int i;
			UltraGrid grid = this.Grid;

			// Ensure that all the cells are from the same band. We don't allow selecting cells from
			// different bands.
			//
			UltraGridBand band = cells[0].Band;

			for ( i = 1; i < cells.Length; i++ )
			{
				if ( cells[i].Band != band )
					// SSP 6/23/03 - Localization
					//
					//throw new ArgumentException( "Cells must be from the same band.", "cells" );
					throw new ArgumentException( SR.GetString( "LER_ArgumentException_4" ), "cells" );
			}

			// Ensure that the cells are from this grid. We don't want to add cells from a different grid
			// or a different layout to the selected of this grid.
			//
			if ( null == band || band.Layout != grid.DisplayLayout )
				// SSP 6/23/03 - Localization
				//
				//throw new ArgumentException( "Cells must be from display layout of the same grid.", "cells" );
				throw new ArgumentException( SR.GetString( "LER_ArguementException_5" ), "cells" );

			SelectType selectType = band.SelectTypeCellResolved;
			
			if ( SelectType.None == selectType )
				// SSP 6/23/03 - Localization
				//
				//throw new InvalidOperationException( "Can't select cells. SelectType is None." );
				throw new InvalidOperationException( SR.GetString( "LER_InvalidOperationException1" ) );

			if ( SelectType.Single == selectType || SelectType.SingleAutoDrag == selectType )
			{
				if ( cells.Length + this.Count > 1 )
					// SSP 6/23/03 - Localization
					//
					//throw new InvalidOperationException( "Can't select more than 1 cell when SelectType is Single or SingleAutoDrag" );
					throw new InvalidOperationException( SR.GetString( "LER_InvalidOperationException2" ) );
			}
			// SSP 6/15/05
			// If select type is single and only a single row is being added to 0 count collection then
			// allow the operation. Made the if into else if block.
			// 
			//if ( SelectType.Extended != selectType &&  SelectType.ExtendedAutoDrag != selectType )
			else if ( SelectType.Extended != selectType &&  SelectType.ExtendedAutoDrag != selectType )
				// SSP 6/23/03 - Localization
				//
				//throw new InvalidOperationException( "Cell selection not allowed. Invalid SelectType settings." );
				throw new InvalidOperationException( SR.GetString( "LER_InvalidOperationException3" ) );

			// Clear the selected rows since cells and rows can't be selected at the same time. If that gets 
			// cancelled, then return.
			//
			if ( grid.ClearSelectedRows( ) )
				return;
				
			Hashtable newSelectedCells = new Hashtable( );
			object dummy = new object( );

			// Add the existing rows.
			//
			for ( i = 0; i < this.Count; i++ )
			{
				newSelectedCells[ this[i] ] = dummy;
			}

			// Add the passed in rows.
			//
			for ( i = 0; i < cells.Length; i++ )
			{
				newSelectedCells[ cells[i] ] = dummy;
			}

			object[] arr = new object[ newSelectedCells.Count ];
			newSelectedCells.Keys.CopyTo( arr, 0 );
			newSelectedCells = null;
			
			// Sort the new rows by the selected position.
			//
			Infragistics.Win.Utilities.SortMerge( arr, new Infragistics.Win.UltraWinGrid.Selected.SelectionPositionSortComparer( ) );

			Infragistics.Win.UltraWinGrid.Selected newSelection = new Selected( );
			newSelection.Cells.InternalAddRange( arr );

			// Copy the columns since columns and cells can be selected at the same time. Rows and cells
			// can't be selected at the same time.
			//
			newSelection.Columns.InternalSetList( grid.Selected.Columns );

			// Finally select the new selection by calling SelectNewSelection which fires the 
			// select change events.
			//
			grid.SelectNewSelection( typeof( UltraGridCell ), newSelection );
		}

		#endregion // AddRange

		#endregion // Fix For UWG1835

		#region Add

		// SSP 6/15/05
		// Added Add method. We had AddRange but not Add.
		// 
		/// <summary>
		/// Adds the specified cell to this selected cells collection.
		/// </summary>
		/// <param name="cell"></param>
		public void Add( UltraGridCell cell )
		{
			this.AddRange( new UltraGridCell[] { cell } );
		}

		#endregion // Add

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.UltraGridCell[] array, int index)
		{
			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo

		#region Sort

		// SSP 2/22/06 BR09673
		// Added Sort method.
		// 
		/// <summary>
		/// Sorts the items in the collection according their visible order.
		/// </summary>
		public void Sort( )
		{
			base.Sort( new Infragistics.Win.UltraWinGrid.Selected.SelectionPositionSortComparer( ) );
		}

		#endregion // Sort
	}


	
}
