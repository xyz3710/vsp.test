#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	
	/// <summary>
	/// ExpansionIndicatorUIElement
	/// </summary>
	public class ExpansionIndicatorUIElement : Infragistics.Win.ExpansionIndicatorUIElement
	{

		#region Constructor
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public ExpansionIndicatorUIElement( UIElement parent ) : base(parent)
		{
		}
		#endregion Constructor

		#region Expansion Indicator Overrides

		/// <summary>
		/// Indicates if the expansion indicator is expanded/open.
		/// </summary>
		public override bool IsOpen
		{
			get { return Row.Expanded; }
		}

		/// <summary>
		/// Handles toggling of the expansion indicator state.
		/// </summary>
		protected override void Toggle()
		{
			Infragistics.Win.UltraWinGrid.UltraGridRow row = this.Row;

			// SSP 4/11/03 UWG2189
			// Only expand/collapse if the band's expandable property is true
			// and the element is enabled.
			//
			if ( this.Enabled && row.Band.Expandable )
			{
				if ( row.Layout.ActiveCell != null )
				{
					// SSP 8/6/04 UWG3400
					// If the exitting of edit mode was canceled (may be because the cell contained 
					// invalid input) then don't proceed with expansion of the row.
					//
					//row.Layout.ActiveCell.ExitEditMode();
					if ( row.Layout.ActiveCell.ExitEditMode( ) )
						return;
				}

				FixedRowStyle origRowFixedState = row.FixedResolvedLocation;

				row.Expanded = !row.ShowAsExpanded;

				// SSP 4/30/05 - NAS 5.2 Fixed Rows_
				// 
				// --------------------------------------------------------------
				if ( FixedRowStyle.Default != origRowFixedState && row.Expanded && row.IsStillValid )
				{
					RowScrollRegion rsr = row.Layout.ActiveRowScrollRegion;
					if ( null != rsr )
					{
						bool origDontUpdateInOnScroll = rsr.dontUpdateInOnScroll;
						rsr.dontUpdateInOnScroll = true;

						try
						{
							UltraGridRow lastChildRow = row.GetChild( 
								FixedRowStyle.Top == origRowFixedState ? ChildRow.First : ChildRow.Last, IncludeRowTypes.SpecialRows );

							if ( null != lastChildRow )
								rsr.ScrollRowIntoView( lastChildRow );
							
							rsr.ScrollRowIntoView( row );
						}
						finally
						{
							rsr.dontUpdateInOnScroll = origDontUpdateInOnScroll;
						}
					}
				}
				// --------------------------------------------------------------
			}
		}

		/// <summary>
		/// Indicates whether the associated object can be expanded.
		/// </summary>
		protected override bool CanExpand
		{
			get 
			{ 
				return this.Row.Band.Expandable; 
			}
		}

		#endregion Expansion Indicator Overrides

		#region InitAppearance
        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// SSP 2/20/04 - Optimization
			// Use a local variable to to store Row instead of calling Row property which calls
			// GetContext to get the row.
			//
			UltraGridRow row = this.Row;
			UltraGridBand band = row.BandInternal;
			UltraGridLayout layout = band.Layout;

			// SSP 5/5/06 - App Styling
			// Resolve the app-style role appearance settings.
			// 
			// ----------------------------------------------------------------------------------------------
			AppStyling.ResolutionOrderInfo order = StyleUtils.GetResolutionOrder( layout );
			AppStyling.UIRole role = this.UIRole;

			if ( ( order.UseStyleBefore || order.UseStyleAfter ) && null != role )
			{
				if ( this.IsOpen )
					role.ResolveAppearance( ref appearance, ref requestedProps, AppStyling.RoleState.Expanded );

				role.ResolveAppearance( ref appearance, ref requestedProps, AppStyling.RoleState.Normal );
			}
			// ----------------------------------------------------------------------------------------------

			// SSP 5/5/06 - App Styling
			// Only set the property if it's not already been set.
			// 
			// ----------------------------------------------------------------------------------------------
			
			bool isExpandable = band.Expandable;
			if ( 0 != ( AppearancePropFlags.BackColor & requestedProps ) )
			{
				appearance.BackColor = isExpandable ? SystemColors.Window : SystemColors.Control;
				requestedProps ^= AppearancePropFlags.BackColor;
			}

			if ( 0 != ( AppearancePropFlags.ForeColor & requestedProps ) )
			{
				appearance.ForeColor = isExpandable ? SystemColors.WindowText : SystemColors.ControlDark;
				requestedProps ^= AppearancePropFlags.ForeColor;
			}
			// ----------------------------------------------------------------------------------------------

			// if the row connector style is inset ot raised use the
			// control shadoow color otherwise use the rowconnector color
			// for the bordercolor
			//
			// SSP 3/16/06 - App Styling
			// 
			//switch ( row.Layout.RowConnectorStyle )
			switch ( layout.RowConnectorStyleResolved )
			{
				case RowConnectorStyle.Inset:
				case RowConnectorStyle.Raised:
					// SSP 5/5/06 - App Styling
					// Only set the property if it's not already been set.
					// 
					if ( 0 != ( AppearancePropFlags.BorderColor & requestedProps ) )
					{
						appearance.BorderColor = SystemColors.ControlDark;
						requestedProps ^= AppearancePropFlags.BorderColor;
					}
					break;
				default:
					// SSP 3/16/06
					// Use the new RowConnectorColorResolved property.
					// 
					//appearance.BorderColor = row.Layout.RowConnectorColor;
					// SSP 5/5/06 - App Styling
					// Only set the property if it's not already been set.
					// 
					if ( 0 != ( AppearancePropFlags.BorderColor & requestedProps ) )
					{
						appearance.BorderColor = layout.RowConnectorColorResolved;
						requestedProps ^= AppearancePropFlags.BorderColor;
					}
					break;
			}

		}
		#endregion InitAppearance
 
		#region Row
		/// <summary>
		/// The UltraGridRow object represents a row of data in the grid. An UltraGridRow corresponds to a single record in an underlying data source. This property is read-only at run-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The UltraGridRow object represents a single row of data, and corresponds to a single record in the underlying recordset. Rows occupy a position in the data hierarchy of the UltraWinGrid between Cells and Bands. The UltraGridRow object is always the child of an UltraGridBand object, and its children are UltraGridCell objects.</p>
		/// <p class="body">Much of the data-binding functionality of the grid involves working with the UltraGridRow object. Whenever an UltraGridRow object is loaded by the grid, the <b>InitializeRow</b> event is fired.</p>
		/// <p class="body">UltraGridRow objects can influence the formatting of the cells they contain through the setting of the UltraGridRow's <b>CellAppearance</b> property. Rows can also be formatted independently of the cells they contain. Frequently, cells are drawn from the top of the row to the bottom and are aligned edge to edge so that they occupy the entire area of the row; the row itself is not visible because cells are always "above" the row in the grid's z-order. However it is possible to specify spacing between and around cells that lets the underlying UltraGridRow object show through. Only then will formatting applied directly to the UltraGridRow object be visible to the user.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return (Infragistics.Win.UltraWinGrid.UltraGridRow)this.Parent.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ) );
			}
		}
		#endregion Row

		#region UIRole

		// SSP 5/5/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Row.BandInternal, StyleUtils.Role.ExpansionIndicator );
			}
		}

		#endregion // UIRole

		#region BorderStyle

		// SSP 5/5/06 - App Styling - Optimization
		// Overrode BorderStyle. This is strictly for optimization purposes. The base implementation
		// returns the right border style however it does not cache.
		// 
        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				UltraGridBand band = this.Row.BandInternal;

				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( band, StyleUtils.CachedProperty.BorderStyleExpansionIndicator, out val ) )
				{
					val = StyleUtils.CacheBorderStylePropertyValue( band, StyleUtils.CachedProperty.BorderStyleExpansionIndicator,
						StyleUtils.Role.ExpansionIndicator, UIElementBorderStyle.Default, base.BorderStyle );
				}

				return (UIElementBorderStyle)val;
			}
		}
		#endregion BorderStyle
	}

}
