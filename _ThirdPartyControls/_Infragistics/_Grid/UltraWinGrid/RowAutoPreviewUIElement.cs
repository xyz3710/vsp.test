#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Diagnostics;

	/// <summary>
	///		Summary description for RowAutoPreviewUIElement.
	/// </summary>
	public class RowAutoPreviewUIElement : UIElement
	{
		internal RowAutoPreviewUIElement( UIElement parent, Rectangle rect ) : base( parent )
		{		
			this.rectValue = rect;			
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.Row.Band.BorderStyleRowResolved;				
			}
		}

		/// <summary>
		/// Returns the UltraGridRow object associated with the cell. This property is not available at design time. This property is read-only at run time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Row</b> property of an object refers to a specific row in the grid as defined by an UltraGridRow object. You use the <b>Row</b> property to access the properties of a specified UltraGridRow object, or to return a reference to the UltraGridRow object that is associated with the current object.</p>
		/// <p class="body">An UltraGridRow object represents a single row in the grid that displays the data from a single record in the underlying data source. The UltraGridRow object is one mechanism used to manage the updating of data records either singly or in batches (the other is the UltraGridCell object). When the user is interacting with the grid, one UltraGridRow object is always the active row, and determines both the input focus of the grid and the position context of the data source to which the grid is bound.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return (Infragistics.Win.UltraWinGrid.UltraGridRow)this.Parent.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ) );
			}
		}


        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			this.Row.ResolveAppearance( ref appearance, requestedProps, true );
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			UIElementsCollection oldElements = this.ChildElements;

			this.childElementsCollection = null;
			string text = null;
			text = this.Row.Description;			
						
			TextUIElement textElement = (TextUIElement)RowAutoPreviewUIElement.ExtractExistingElement( oldElements, typeof(TextUIElement), true );

			if ( null == textElement)
			{
				textElement = new TextUIElement( this, text );
			}
			else
				textElement.Text = text;

			// RobA UWG684 11/13/01
			textElement.MultiLine = true;

			//	BF 8.2.02
			textElement.WrapText = true;
					
			Rectangle workRect = this.rectValue;
			
			int border = this.Row.Layout.GetBorderThickness( this.Row.Band.BorderStyleRowResolved );

			// RobA UWG846 12/6/01
			// Not sure as to why we were taking the cellSpacing and cellPadding
			// into account here.
			//
			//int cellSpacing = ( this.Row.Band != null ) ? this.Row.Band.CellSpacingResolved : 0;
			//int cellPadding = ( this.Row.Band != null ) ? this.Row.Band.CellPaddingResolved  : 0;
			int cellSpacing = 0;
			int cellPadding = 0;

								
			//Position text element inside autopreview borders
			//				
			workRect.X += cellSpacing + cellPadding + border;
			workRect.Width -= 2*(cellSpacing + cellPadding + border);

			// RobA UWG682 11/13/01
			//workRect.Height -= border;
			workRect.Height -= 2 * border;

			// SSP 9/26/02 UWG1425
			// Take into account the border since we are using rectValue rather than
			// RectInsideBorders above. Also add in a padding of 1.
			//
			//workRect.Y += cellSpacing;
			workRect.Y += 1 + cellSpacing + border;

			// SSP 1/24/02 UWG980
			// We are supposed to be indenting the text within the auto preview area
			// not the whole auto preview area itself. So moved this code from 
			// RowCellAreaElement to here.
			//			
			int autoPreviewIndentation = 0;
			if ( this.Row != null )
				autoPreviewIndentation = this.Row.Band.AutoPreviewIndentation;

			//	if the value of the band's AutoPreviewIndentation property
			//	is greater than zero, and is less than the width of the auto preview area,
			//	adjust the rect of the AutoPreview area
			if ( autoPreviewIndentation > 0 && 
					autoPreviewIndentation < workRect.Width )
			{
				workRect.Offset( autoPreviewIndentation, 0 );
				workRect.Width -= autoPreviewIndentation;
			}
			


			textElement.Rect = workRect;

			// SSP 5/12/06 - App Styling
			// 
			// --------------------------------------------------------------------------------------
			
			// --------------------------------------------------------------------------------------

			// SSP 11/01/01 UWG628
			// Set the proper text alignment.
			//
			// JJD 11/07/01
			// TextAlignment property has been removed
			//
			//textElement.TextAlignment = Win.AppearanceData.ContentAlignmentFromHVAlign( appData.TextHAlign, appData.TextVAlign );
			
			
			// add the text element
			//
			this.ChildElements.Add( textElement );
		
		}	

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				// SSP 10/31/01 UWG611
				// Also draw the left border.
				// 
				//return Border3DSide.Bottom|Border3DSide.Top|Border3DSide.Right;
				return Border3DSide.Left | Border3DSide.Bottom|Border3DSide.Top|Border3DSide.Right;								
			}
		}

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Row.BandInternal, StyleUtils.Role.RowPreview );
			}
		}

		#endregion // UIRole

		
			
	}
}
