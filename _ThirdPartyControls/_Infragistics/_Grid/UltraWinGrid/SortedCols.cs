#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Collections;
	using System.Data;
	using Infragistics.Shared;
	using System.Diagnostics;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Runtime.InteropServices;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Security.Permissions;
	using Infragistics.Shared.Serialization;
	using System.Collections.Generic;

	/// <summary>
	/// Internal class used for serialization.
	/// </summary>
	[Serializable()]
	[EditorBrowsable( EditorBrowsableState.Never ) ]
	public class SerializedColumnID : ISerializable
	{
		private int		relativeIndex;
		private bool	bound;
		private Infragistics.Win.UltraWinGrid.SortIndicator sortIndicator;
		private bool	isGroupByColumn;
		private string	key;  

		// SSP 8/9/04 - Implemented design time serialization of summaries
		// Added following constructor.
		//
		internal SerializedColumnID( int relativeIndex, bool bound, string key ) 
			: this( relativeIndex, bound, SortIndicator.None, false, key )
		{
		}

		// Constructor
		//
		internal SerializedColumnID( int		relativeIndex,
									 bool		bound,
									 Infragistics.Win.UltraWinGrid.SortIndicator sortIndicator,
									 bool isGroupByColumn,
									 string		key  )
		{
			this.relativeIndex	  = relativeIndex;
			this.bound			  = bound;
			this.sortIndicator	  = sortIndicator;
			this.isGroupByColumn  = isGroupByColumn;
			this.key			  = key;
		}

		internal int	RelativeIndex { get { return this.relativeIndex; } }
		internal bool	Bound { get { return this.bound; } }
		internal Infragistics.Win.UltraWinGrid.SortIndicator 
						SortIndicator { get { return this.sortIndicator; } }
		internal bool   IsGroupByColumn { get { return this.isGroupByColumn; } }
		internal string	Key { get { return this.key; } }
		
  
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{

			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Key", this.key );
            //info.AddValue("Key", this.key);

			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "RelativeIndex", this.relativeIndex );
			//info.AddValue("RelativeIndex", this.relativeIndex );

			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Bound", this.bound );
			//info.AddValue("Bound", this.bound );

			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "SortIndicator", this.sortIndicator );
			//info.AddValue("SortIndicator", (int)this.sortIndicator );

			// SSP 10/26/01
			// IsGroupByColumn should not be serialized off the
			// column. They should be serialized in the SortedColumns collection.
			// So added appropriate code below
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "IsGroupByColumn", this.isGroupByColumn );
			//info.AddValue( "IsGroupByColumn", (bool)this.isGroupByColumn );
		}
        
		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//private SerializedColumnID( SerializationInfo info, StreamingContext context)
		protected SerializedColumnID( SerializationInfo info, StreamingContext context)
		{
		
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case  "Key":
						//this.key = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.key = (string)Utils.DeserializeProperty( entry, typeof(string), this.key );
						//this.key = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "RelativeIndex":
						//this.relativeIndex = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.relativeIndex = (int)Utils.DeserializeProperty( entry, typeof(int), this.relativeIndex );
						//this.relativeIndex = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "Bound":
						//this.bound = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.bound = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.bound );
						//this.bound = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case "SortIndicator":
						//this.sortIndicator = (Infragistics.Win.UltraWinGrid.SortIndicator)(int) entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.sortIndicator = (SortIndicator)Utils.DeserializeProperty( entry, typeof(SortIndicator), this.sortIndicator );
						//this.sortIndicator = (SortIndicator)Utils.ConvertEnum(entry.Value, this.sortIndicator);
						break;

						
						// SSP 10/26/01
						// IsGroupByColumn should not be serialized off the
						// column. They should be serialized in the SortedColumns collection.
						// So added appropriate code below
						//
					case "IsGroupByColumn":
						//this.isGroupByColumn = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.isGroupByColumn = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.isGroupByColumn );
						//this.isGroupByColumn = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;
				}
			}
		}

	}


	/// <summary>
	/// Returns a collection of sorted column objects. This property is not 
	/// available at design-time.
	/// </summary><remarks><para>The SortedCols property is used to access the 
	/// collection of UltraGridColumn objects associated with an Band. An UltraGridColumn 
	/// object represents a single column in the grid; it usually corresponds to a 
	/// data field in the recordset underlying the grid, and it has properties that 
	/// determine the appearance and behavior of the cells that make up the column.</para>
	/// <para>The UltraGrid can automatically sort the contents of columns 
	/// without the addition of any code, provided the control is able to preload
	/// the rows in the band. Preloading is enabled by default if the recordset
	/// bound to the band contains less than 1000 rows. 
	/// If you do not want to preload rows, but you still want to provide column 
	/// sorting in the control, you must implement column sorting yourself using 
	/// the <b>BeforeSortChange</b> and <b>AfterSortChange</b> events.</para><para>
	/// Column headers can display a sort indicator in a column's header. 
	/// When clicked and the <b>HeaderClickAction</b> property is set to  
	/// SortSingle or SortMulti, 
	/// the <b>SortIndicator</b> property is set to specify the order in which the 
	/// column will be sorted, and the column is added to a special Columns 
	/// collection just for sorted columns. This is the collection that is 
	/// accessed by the SortedCols property.</para><para>In addition to adding 
	/// the column to the UltraGridColumns collection accessed by SortedCols, the 
	/// control fires the <b>BeforeSortChange</b> and <b>AfterSortChange</b> events so that 
	/// you can examine the contents of the collection, check the value of the 
	/// <b>SortIndicator</b> property of each column, and optionally perform the sort.</para></remarks>
	
	[ Serializable() ]
	public class SortedColumnsCollection : KeyedSubObjectsCollectionBase,
		ISerializable
	{
		private UltraGridBand band;
		private bool firingBeforeSortChange = false;
		private bool isClone				= false;
		private int  sortVersion = 0;
		private SerializedColumnID[] serializedColumnIDs = null;


		internal bool inProcessingNewSortedColumns = false;

		//not sure if we still need this flag in this version
		//private bool clone = false;
		private int	 initialCapacity = 5;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="band"></param>
		public SortedColumnsCollection( UltraGridBand band )
		{
			this.band = band;
		}

		/// <summary>
		/// Band Property
		/// </summary>
		public UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}

		internal UltraGridLayout Layout
		{
			get
			{
				return this.band.Layout;
			}
		}

		internal void VerifyColumns()
		{
			// JJD 10/10/01
			// Remove any columns that don't exist in the band's
			// columns collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				if ( this.band.Columns.IndexOf( this[i] ) < 0 )
				{
					this.RemoveAt( i );
					i--;
				}
			}
		}

		internal int SortVersion
		{
			get
			{
				return this.sortVersion;
			}
		}

		internal bool HasSerializedColumnIDs
		{
			get
			{
				return this.serializedColumnIDs != null &&
					this.serializedColumnIDs.Length > 0;
			}
		}



		/// <summary>
		/// Abstract property that specifies the initial capacity
		/// of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return initialCapacity;
			}
		}
		/// <summary>
		/// indexer
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn this[ int index ] 
		{
			get
			{
				return (UltraGridColumn)base.GetItem( index );
			}
		}

		
		/// <summary>
		/// indexer (by string key)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn this[ String key ] 
		{
			get
			{
				return (UltraGridColumn)base.GetItem( key );
			}
		}
		internal int AddSortedColHelper( UltraGridColumn column )
		{
			// Add a listener for property changes.
			column.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// Add the layout and return the index
			return this.InternalAdd(column);
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
			
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			// SSP 10/26/01 UWG589
			// If this instance of sorted cols collection is already serialized
			// ( which would explain why we would have SerializedColumnIds. If
			// that's the case, then serialize those SerializedColumnIds
			//
			if ( this.HasSerializedColumnIDs && 0 == this.Count )
			{
				int cnt = 0;
				for ( int i = 0; i < this.serializedColumnIDs.Length; i++ )
				{
					SerializedColumnID colid = this.serializedColumnIDs[i];

					if ( null != colid )
						cnt++;
				}

				// first add the count so we can set the initial capacity
				// efficiently when we de-serialize
				//
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Count", cnt );
				//info.AddValue( "Count", cnt );
			
				// add each column in the collection
				//
				for ( int i = 0; i < this.serializedColumnIDs.Length; i++ )
				{
					SerializedColumnID colid = this.serializedColumnIDs[i];

					if ( null != colid )
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, i.ToString(), colid );
						//info.AddValue( i.ToString(), colid );
					}
				}

				return;
			}

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Count", this.Count );
			//info.AddValue( "Count", this.Count );
			
			// add each column in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				int relativeIndex		= this[i].Index;

				if ( !this[i].IsBound )
					relativeIndex -= this.Band.Columns.BoundColumnsCount;

				
				// SSP 10/26/01
				// Added extra parameter isGroupByColumn in SerializedColumnID contructor
				//
				//SerializedColumnID colid = new SerializedColumnID( relativeIndex, 
				//	this[i].IsBound, this[i].SortIndicator, this[i].Key );
				SerializedColumnID colid = new SerializedColumnID( relativeIndex, 
					this[i].IsBound, this[i].SortIndicator, this[i].IsGroupByColumn, this[i].Key );

				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, i.ToString(), colid );
				//info.AddValue( i.ToString(), colid );
			}
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal SortedColumnsCollection( SerializationInfo info, StreamingContext context)
		protected SortedColumnsCollection( SerializationInfo info, StreamingContext context)
		{

			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( SerializedColumnID ) )
				{
					//this.serializedColumnIDs[ int.Parse( entry.Name ) ] = (SerializedColumnID)entry.Value;
					// convert the name to an int
					//
					int index = int.Parse( entry.Name );
				
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					SerializedColumnID colID = (SerializedColumnID)Utils.DeserializeProperty( entry, typeof(SerializedColumnID), null );

					if ( colID != null && index >= 0 && index < this.serializedColumnIDs.Length )
						this.serializedColumnIDs[index] = colID;
					else
					{
						Debug.Fail("Invalid override appearance index during de-serialization, index = " + index.ToString() );
					}
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							//this.initialCapacity = Math.Max( this.initialCapacity, (int)entry.Value );
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/17/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							int deserializedValue = (int)Utils.DeserializeProperty( entry, typeof(int), 0);
							this.initialCapacity = Math.Max(this.initialCapacity, deserializedValue );
							//this.initialCapacity = Math.Max(this.initialCapacity, (int)DisposableObject.ConvertValue( entry.Value, typeof(int) ));

							//this.serializedColumnIDs = new SerializedColumnID[(int)entry.Value];
							//this.serializedColumnIDs = new SerializedColumnID[(int)DisposableObject.ConvertValue( entry.Value, typeof(int) )];
							this.serializedColumnIDs = new SerializedColumnID[deserializedValue];

							break;
						}

						// JJD 1/31/02
						// De-Serialize the tag property
						//
						case "Tag":
						{
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						}

						default:
						{
							Debug.Assert( false, "Invalid entry in SortedColumnsCollection de-serialization ctor" );
							break;
						}
					}
				}
			}
		}

		/// <summary>
		/// Remove a sorted column from the collection
		/// </summary>
		public void	Remove( UltraGridColumn column )
		{
			if ( this.isClone )
			{
				this.InternalRemove( column );
			}
			else
			{
				this.SetSortedColumn( column, SortIndicator.None, column.IsGroupByColumn, false, true );
			}
		}

		/// <summary>
		/// Returns false
		/// </summary>
		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Inserts the column into the sorted columns collection. 
		/// </summary>
        /// <param name="insertIndex">The index into which the column will be inserted. Index must be less or
        /// equal to the index of first non-group by column in the collection.
        /// (There can't be a group by column surrounded by non-group by
        /// columns in the sorted collection).</param>
        /// <param name="column">The column to insert.</param>
		/// <param name="groupBy">True to specify that the column should be grouped by, as well as sorted.</param>
		public void Insert( int insertIndex, UltraGridColumn column, bool groupBy )
		{
			if ( insertIndex > 1 + this.LastGroupByColIndex )

				throw new ArgumentOutOfRangeException( 
					SR.GetString("LER_Exception_334") );

			if ( SortIndicator.Disabled == column.SortIndicator )

				throw new InvalidOperationException( 
					SR.GetString("LER_Exception_335") );

			if ( column == null )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_268") );

			if ( column.Band != this.band )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_262") );

			if ( groupBy &&  DefaultableBoolean.False == column.AllowGroupByResolved )
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_263") );


			// If we are in the beforesortchange event prevent any modification to
			// the sorted cols collection
			// 
			if ( this.firingBeforeSortChange )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_264") );

			if ( column.SortIndicator == SortIndicator.Disabled )
				return;


			SortIndicator sortIndicator = 
				SortIndicator.None != column.SortIndicator 
				? column.SortIndicator
				: SortIndicator.Ascending;


			// Clone the existing collection
			//
			SortedColumnsCollection newSortedColumns = this.Clone();

			// Clone the column
			UltraGridColumn newColumn = column.Clone( (Infragistics.Win.UltraWinGrid.PosChanged) (-1) );
			
            
			column.TempSortIndicator = sortIndicator;

			if ( groupBy )
			{
				// SSP 8/6/02 UWG1480
				// If the key of the column is "" or null, then Exists will always return false.
				// Added GetColumnIndex which searches by key, column, and column.Index to find
				// the index so use that.
				//
				//if ( newSortedColumns.Exists( column.Key ) )
				if ( newSortedColumns.GetColumnIndex( column ) >= 0 )
				{	
					// SSP 8/6/02 UWG1480
					// If the key of the column is "" or null, then Exists will always return false
					// so also call the GetColumnIndex helper method which tries other methods of
					// finding the index.
					//
					// ------------------------------------------------------------------------------
					//int index = newSortedColumns.IndexOf( column.Key );
					int index = newSortedColumns.GetColumnIndex( column );
					// ------------------------------------------------------------------------------

					if ( index - 1 >= 0 )
					{
						UltraGridColumn col = this[ index - 1 ];
						col.BumpGroupByHierarchyVersion( );
						if ( null != col.ClonedFromColumn )
							col.ClonedFromColumn.BumpGroupByHierarchyVersion( );
					}
					else
					{
						this.Band.BumpGroupByHierarchyVersion( );
					}
				}
			}

		
			// SSP 9/7/01
			// This doesn't work as newSortedColumns contains clone of
			// the column, not the column itsekf due to above clone call
			//
			//newSortedColumns.Remove( column );			
			try
			{
				// SSP 8/6/02 UWG1480
				// If the key of the column is "" or null, then Exists will always return false.
				// Added GetColumnIndex which searches by key, column, and column.Index to find
				// the index so use that.
				//
				//if ( newSortedColumns.Exists( column.Key ) )
				if ( newSortedColumns.GetColumnIndex( column ) >= 0 )
				{
					// SSP 8/6/02 UWG1480
					// If the key of the column is "" or null, then Exists will always return false
					// so also call the GetColumnIndex helper method which tries other methods of
					// finding the index.
					//
					// ------------------------------------------------------------------------------
					//int index = newSortedColumns.IndexOf( column.Key );
					int index = newSortedColumns.GetColumnIndex( column );
					// ------------------------------------------------------------------------------

					// since we are removing an exisiting column occurring before the 
					// column being inserted in the collection, we have to decrement
					// the insertIndex so that the column will be inserted at intended
					// location
					//
					if ( index < insertIndex )
						insertIndex--;

					// SSP 8/6/02 UWG1480
					// If the key of the column is "" or null, then Remove that takes in the key
					// won't work. So use the index instead.
					//
					//newSortedColumns.Remove( column.Key );
					Debug.Assert( index >= 0, "Invalid index !" );
					if ( index >= 0 )
						newSortedColumns.Remove( index );
				}
			}
			catch ( Exception )
			{
			}
		

			// If the column is going to be in the sorted cols
			// collection if the method succeeds, then set the
			// sort indicator on the clone to it's new value.
			if (sortIndicator != SortIndicator.None &&
				sortIndicator != SortIndicator.Disabled)
			{                
				if (newColumn != null)
				{
					newColumn.InitSortIndicator( sortIndicator );
					newColumn.TempSortIndicator =  sortIndicator;
					newColumn.TempIsGroupByColumn = groupBy;
					

					// if the newColumn is not already in the list
					// add it
					//
					if ( groupBy )
					{
						Debug.Assert( !newSortedColumns.Contains( newColumn ), "Already deleted the column and still exists in the collection !" );

						

						newSortedColumns.InternalInsert( insertIndex,
							newColumn );

						if ( insertIndex - 1 >= 0 )
							newSortedColumns[ insertIndex - 1 ].BumpGroupByHierarchyVersion( );
						else
							this.Band.BumpGroupByHierarchyVersion( );
					}
					else
					{
						Debug.Assert( !newSortedColumns.Contains( newColumn ), "Already deleted the column and still exists in the collection !" );

						
						
						newSortedColumns.InternalAdd( newColumn );
					}
				}
			}

			// call ProcessNewSortedColumnsHelper which will fire
			// the before and after events and apply the new sorted
			// columns
			//
			ProcessNewSortedColumnsHelper( newSortedColumns, true, groupBy );
		}

		// SSP 8/6/02 UWG1480
		// Added GetColumnIndex helper method. When the column's key is null or "",
		// (which it can be unbound columns) then IndexOf, Remove, Exists will not
		// work. So added this method.
		//
		internal int GetColumnIndex( UltraGridColumn column )
		{
			int index = this.IndexOf( column.Key );

			if ( index < 0 )
				index = this.IndexOf( column );

			if ( index < 0 && null != column.ClonedFromColumn )
				index = this.IndexOf( column.ClonedFromColumn );

			if ( index < 0 )
			{
				int colIndex = column.Index;

				if ( colIndex < 0 && null != column.ClonedFromColumn )
					colIndex = column.ClonedFromColumn.Index;

				if ( colIndex >= 0 )
				{				
					for ( int i = 0; i < this.Count; i++ )
					{
						UltraGridColumn tmpColumn = this[i];

						if ( column.Band == tmpColumn.Band )
						{
							if ( tmpColumn.Index == colIndex || 
								( null != tmpColumn.ClonedFromColumn && tmpColumn.ClonedFromColumn.Index == colIndex ) )
							{
								index = i;
								break;
							}
						}
					}
				}
			}

			return index;
		}

		/// <summary>
		/// Inserts a column into the sorted columns collection
		/// </summary>
		/// <param name="index">The index into which the column will be inserted.</param>
		/// <param name="value">The column to insert.</param>
		public void Insert( int index, object value )
		{
			if ( value == null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_265") );
			
			Infragistics.Win.UltraWinGrid.UltraGridColumn column = value as Infragistics.Win.UltraWinGrid.UltraGridColumn;

			if ( column == null )
				throw new ArgumentException( SR.GetString("LER_Exception_323", value.GetType().Name ));

			// Add a listener for property changes.
			column.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			if ( index <= this.LastGroupByColIndex )
			{
				// SSP 1/3/02
				// Changed the property into an internal method prompting this
				// change
				//
				//column.IsGroupByColumn = true;
				column.SetGroupByColumnStatus( true );

				try
				{
					// SSP 8/6/02 UWG1480
					// If the key of the column is "" or null, then this won't work.
					// 					
					//column = this[ column.Key ];
					//this.Remove( column.Key );
					int tmpIndex = this.GetColumnIndex( column );
					if ( tmpIndex >= 0 )
						this.Remove( tmpIndex );
				}
				catch ( Exception )
				{
				}
				this.InternalInsert( index, column );
				return;
			}

			this.InternalInsert( index, column );
		}

		/// <summary>
		/// Returns false by default
		/// </summary>
		public override bool IsReadOnly { get { return false; } }


		internal int LastGroupByColIndex
		{
			get
			{
				int i = 0;
				for ( ; i < this.Count; i++ )
				{
					if ( !this[i].IsGroupByColumn )
						return i - 1;
				}

				if ( i - 1 >= 0 && this[ i - 1 ].IsGroupByColumn )
					return i - 1;

				return -1;
			}
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridColumn LastGroupByColumn
		{
			get
			{
				int i = this.LastGroupByColIndex;

				if ( i >= 0 )
					return  this[i];

				return null;
			}
		}
	

		internal void ClearGroupByColumns( )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridColumn column = this[i];

				if ( column.IsGroupByColumn )
				{
					column.InternalSetIsGroupByColumn( false );
					this.RemoveAt( i );
					i--;
				}
			}

			this.Band.BumpGroupByHierarchyVersion( );
		}


        /// <summary>
        /// Adds a column to the sorted column collection
        /// </summary>
        /// <param name="column">The column to add to the collection.</param>
        /// <param name="descending">True to sort the column in descending order. False to sort ascending.</param>
        /// <param name="groupBy">True to specify that the column should also be grouped by, as well as sorted.</param>
        /// <returns>The position into which the new element was inserted.</returns>
		public int Add( UltraGridColumn column, bool descending, bool groupBy )
		{
			if ( column == null )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_266") );

			if ( column.Band != this.band )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_267") );

			SortIndicator sortIndicator;

			if ( descending )
				sortIndicator = SortIndicator.Descending;

			else
				sortIndicator = SortIndicator.Ascending;			

			// if this is a cloned collection then just set
			// the temp sort indicator and append it to the
			// collection
			// 
			if ( this.isClone )
			{
				column.TempSortIndicator = sortIndicator;
				column.TempIsGroupByColumn = groupBy;

				// if it is already in the collection then 
				// remove it so that the same column isn't 
				// in the collection twice
				//
				if ( this.Contains( column ) )
					this.InternalRemove( column );

				if ( groupBy )
				{
					int ret =  1 + this.LastGroupByColIndex;
					this.InternalInsert( ret, column );
					return ret;
				}
				else
					return this.InternalAdd( column );
			}

			this.SetSortedColumn( column, sortIndicator, groupBy, false, true );

			if ( this.Contains( column ) )
				return this.IndexOf( column );

			return -1;

		}

        /// <summary>
        /// Adds a column to the sorted column collection
        /// </summary>
        /// <param name="column">The column to add to the collection.</param>
        /// <param name="descending">True to sort the column in descending order. False to sort ascending.</param>        
        /// <returns>The position into which the new element was inserted.</returns>		
		public int Add( UltraGridColumn column, bool descending ) 
		{
			return this.Add( column, descending, false );
		}

        /// <summary>
        /// Adds a column to the sorted column collection
        /// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="descending">True to sort the column in descending order. False to sort ascending.</param>
        /// <param name="groupBy">True to specify that the column should also be grouped by, as well as sorted.</param>
        /// <returns>The position into which the new element was inserted.</returns>
		public int Add( String key, bool descending, bool groupBy ) 
		{
			return this.Add( this.band.Columns[ key ], descending, groupBy );
		}

		/// <summary>
		/// Adds a column to the sorted column collection
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		/// <param name="descending">True to sort the column in descending order. False to sort ascending.</param>
        /// <returns>The position into which the new element was inserted.</returns>
		public int Add( String key, bool descending ) 
		{
			return this.Add( this.band.Columns[ key ], descending );
		}

		/// <summary>
		/// Remove column from collection
		/// </summary>
		public void Remove( int index ) 
		{
			this.Remove ( this[ index ] );
		}

		/// <summary>
		/// Remove column from collection
		/// </summary>
		public void Remove( String key ) 
		{
			this.Remove ( this[ key ] );
		}

		/// <summary>
		/// Remove a sorted column from the collection
		/// </summary>
		/// <param name="index">Index to be removed</param>
		public void	RemoveAt( int index )
		{
			this.Remove( this[ index ] );			
		}
		internal void InitBand( Infragistics.Win.UltraWinGrid.UltraGridBand band )
		{
			this.band =  band;

			this.ProcessSerializedColumnIDs( this.serializedColumnIDs );

			this.serializedColumnIDs = null;

			for(int i = 0; i < this.Count; i++)
			{
				this[i].InitBand(this.band, false );

				this[i].SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}
		}

		private void ProcessSerializedColumnIDs( SerializedColumnID[] colids )
		{
			// if we don't have a colid array or a band reference then
			// just return
			//
			if ( colids == null && 
				this.band != null )
				return;

			Infragistics.Win.UltraWinGrid.UltraGridColumn column = null;

			// loop over the temporary serializedColumnIDs array and
			// try to match each with the associated column. Then add
			// that column to the sorted cols collection
			//
			for( int i = 0; i < colids.Length; i++ )
			{
				column = null;
				
				// first try matching via the column's key
				//
				try
				{
					if ( colids[i].Key.Length > 0 )
						column = this.band.Columns[ colids[i].Key ];
				}
				catch(Exception)
				{
				}

				// if key did't work then try the relative index
				//
				if ( column == null )
				{
					int index = colids[i].RelativeIndex;

					// if the column is unbound add the bound columns count
					// to get the correct index into the columns collection
					//
					if ( !colids[i].Bound )
						index += this.Band.Columns.BoundColumnsCount;

					try
					{
						column = this.band.Columns[ index ];
					}
					catch(Exception)
					{
					}

				}

				if ( column != null )
				{
					// SSP 10/26/01
					// IsGroupByColumn should not be serialized off the
					// column. They should be serialized in the SortedColumns collection.
					// So added appropriate code below
					//
					column.InternalSetIsGroupByColumn( colids[i].IsGroupByColumn );

					column.InitSortIndicator( colids[i].SortIndicator ); 					
					this.InternalAdd( column );
				}
			}


			// SSP 10/26/01
			// Bump the group by hierarchy version in the band so the
			// group by rows get created since any group by columns could
			// have been added or deleted
			//
			this.Band.BumpGroupByHierarchyVersion( );

		}

		internal void InitDuringDeserialization( UltraGridColumn [] sortedColumns )
		{
			base.InternalClear();

			for ( int i = 0; i < sortedColumns.Length; i++ )
			{
				if ( sortedColumns[ i ] != null )
					base.InternalAdd( sortedColumns[ i ] );
			}
		}

		// SSP 10/26/04 UWG3681
		// Added HasGroupByColumns property.
		//
		internal bool HasGroupByColumns
		{
			get
			{
				// Group-by columns are always in the beginning of the collection.
				//
				return this.Count > 0 && this[0].IsGroupByColumn;
			}
		}

		// SSP 7/26/05 - NAS 5.3 Header Placement
		// 
		internal int GroupBySortColumnsCount
		{
			get
			{
				int i = 0;
				while ( i < this.Count && this[i].IsGroupByColumn )
					i++;

				return i;
			}
		}

		internal new void InternalClear()
		{
			// SSP 10/26/04 UWG3681
			//
			bool hadGroupByColumns = this.HasGroupByColumns;

			for ( int i=0; i < this.Count ; ++i )
			{
				UltraGridColumn column = this[i];
				Debug.Assert( null != column, "null column in SortecColumnsCollection !" );

				if ( null != column )
				{
					// SSP 5/28/03 - UWG2254
					// Don't clear the groupbycolumn flag and the sort indicator property if this sorted
					// columns collection isntance is null and the column is not a cloned column.
					// 
					if ( this.isClone && null == column.ClonedFromColumn )
						continue;

					// SSP 11/8/01 UWG688
					// Set the group by column to false before setting the sort indicator
					// because calling InternalSetSortIndicator causes the grid to redraw
					// leading to whole bunch of things that would cause problems in the
					// midst of inserting a group by column in the sorted columns collection.
					//					
					column.InternalSetIsGroupByColumn( false );
					
					
					//RobA 10/1/01 we are now using an internal set for the 
					//sort indicator
					column.InternalSetSortIndicator(SortIndicator.None);
					//column.SortIndicator = SortIndicator.None;

					// SSP 11/8/01 UWG688
					// Moved it above
					//column.InternalSetIsGroupByColumn( false );
				}
			}

			// SSP 10/26/04 UWG3681
			// Bump the group-by version number so all the row collections recreate the 
			// group-by hierarchy.
			//
			if ( hadGroupByColumns && ! this.isClone && ! this.inProcessingNewSortedColumns && null != this.Band )
				this.Band.BumpGroupByHierarchyVersion( );

			base.InternalClear();			
		}

		/// <summary>
		/// Clears the collection
		/// </summary>
		public void Clear() 
		{
			if ( this.isClone )
			{
				this.InternalClear();
				return;
			}

			// SSP 3/25/05 BR03003
			// If the collection doesn't have any items then just return. The following code
			// ends up firing Before and After SortChange events.
			//
			if ( 0 == this.Count )
				return;

			// If we are in the beforesortchange event prevent any modification to
			// the sorted cols collection
			// 
			if ( this.firingBeforeSortChange )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_264") );

			// SSP 11/26/02 UWG1852
			// If we clear any group by columns, then bump the group by version number.
			// Enclosed the below code in try-finally block and added code that calls
			// BumpGroupByHierarchyVersion method on the band.
			//
			bool hadAnyGroupByColumns = this.Count > 0 && this[0].IsGroupByColumn;

			try
			{
				// Clone the existing collection
				//
				SortedColumnsCollection newSortedColumns = this.Clone();

				// Clear the new sorted cols collection 
				//
				newSortedColumns.Clear();

				// call ProcessNewSortedColumnsHelper which will fire
				// the before and after events and apply the new sorted
				// columns
				//
				ProcessNewSortedColumnsHelper( newSortedColumns, true, false );
			}
			finally
			{
				if ( hadAnyGroupByColumns )
					this.Band.BumpGroupByHierarchyVersion( );
			}
		}

		internal void SetSortedColumn( UltraGridColumn column, 
			SortIndicator sortIndicator, bool groupBy,
			bool clearExistingNonGroupByColumns,	bool fireEvents )
		{
			if ( column == null )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_268") );

			if ( column.Band != this.band )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_262") );

			// SSP 9/27/01 UWG345, UWG349
			// We still want to allow the user to group by a column programmatically
			// even if the column.AllowGroupBy is set to false
			//
			

			// If we are in the beforesortchange event prevent any modification to
			// the sorted cols collection
			// 
			if ( this.firingBeforeSortChange )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_264") );

			//DA 8.10.02
			//uwg1243 
			//Not sure why we were doing this, essentially once the prop is set to disabled 
			//you can never set it back.

			//if ( column.SortIndicator == SortIndicator.Disabled )
			//	return;


			// Clone the existing collection
			//
			SortedColumnsCollection newSortedColumns = this.Clone();

			// Clone the column
			UltraGridColumn newColumn = column.Clone( (Infragistics.Win.UltraWinGrid.PosChanged) (-1) );
			
			column.TempSortIndicator = sortIndicator;

			// SSP 4/24/03 UWG2160
			// Also bump the version number if the column being removed was a group-by column.
			//
			//if ( groupBy )
			if ( groupBy || column.IsGroupByColumn )
			{
				// SSP 8/6/02 UWG1480
				// If the key of the column is "" or null, then Exists will always return false.
				// Added GetColumnIndex which searches by key, column, and column.Index to find
				// the index so use that.
				//
				//if ( newSortedColumns.Exists( column.Key ) )
				if ( newSortedColumns.GetColumnIndex( column ) >= 0 )
				{
					// SSP 8/6/02 UWG1480
					// If the key of the column is "" or null then IndexOf will return -1.
					// Use the new GetColumnIndex helper method instead.
					//
					//int index = newSortedColumns.IndexOf( column.Key );
					int index = newSortedColumns.GetColumnIndex( column );

					if ( index - 1 >= 0 )
					{ 
						UltraGridColumn col = this[ index - 1 ];
						col.BumpGroupByHierarchyVersion( );
						if ( null != col.ClonedFromColumn )
							col.ClonedFromColumn.BumpGroupByHierarchyVersion( );
					}
					else
					{
						this.Band.BumpGroupByHierarchyVersion( );
					}

					// SSP 8/6/02 UWG1480
					// If the key is "" or null, then Remove with the key won't work.
					// Use the index instead.
					//
					//newSortedColumns.Remove( column.Key );
					newSortedColumns.Remove( index );
				}
			}

			// SSP 9/26/01 UWG351, UWG352
			// Clear the new sorted cols collection is the fClearExisting
			// is true
			//
			if ( clearExistingNonGroupByColumns )
			{
				if ( this.LastGroupByColIndex >= 0 )
				{
					int tmp = 1 + this.LastGroupByColIndex;
					
					while ( tmp < newSortedColumns.Count )
					{
						newSortedColumns.RemoveAt( tmp );
					}

					// SSP 10/4/01
					//
					for ( int i = this.Count - 1; i > tmp; i-- )
					{
						this.RemoveAt( i );
					}

					
				}
				else
				{
					newSortedColumns.Clear();
				}
			}
			else
			{
				// SSP 9/7/01
				// This doesn't work as newSortedColumns contains clone of
				// the column, not the column itsekf due to above clone call
				//
				//newSortedColumns.Remove( column );			
				try
				{
					// SSP 8/6/02 UWG1480
					// If the key is "" or null, then Remove with the key won't work.
					// Use the index instead.
					//
					//newSortedColumns.Remove( newSortedColumns[column.Key] );
					int index = newSortedColumns.GetColumnIndex( column );
					if ( index >= 0 )
						newSortedColumns.Remove( index );
				}
				catch ( Exception )
				{
				}
			}

			// If the column is going to be in the sorted cols
			// collection if the method succeeds, then set the
			// sort indicator on the clone to it's new value.
			if (sortIndicator != SortIndicator.None &&
				sortIndicator != SortIndicator.Disabled)
			{                
				if (newColumn != null)
				{
					newColumn.InitSortIndicator( sortIndicator );
					newColumn.TempSortIndicator =  sortIndicator;
					newColumn.TempIsGroupByColumn = groupBy;
					

					// if the newColumn is not already in the list
					// add it
					//
					if ( groupBy )
					{
						if ( newSortedColumns.Contains( newColumn ) )
							newSortedColumns.Remove( newColumn );

						int lastGroupByIndex = newSortedColumns.LastGroupByColIndex;

						if ( lastGroupByIndex >= 0 )
							this[ lastGroupByIndex ].BumpGroupByHierarchyVersion( );
						else
							this.Band.BumpGroupByHierarchyVersion( );
						
						newSortedColumns.InternalInsert( 1 + lastGroupByIndex,
							newColumn );
					}
					else
					{
						if ( newSortedColumns.Contains( newColumn ) )
							newSortedColumns.Remove( newColumn );
						
						newSortedColumns.InternalAdd( newColumn );
					}
				}
			}

			// call ProcessNewSortedColumnsHelper which will fire
			// the before and after events and apply the new sorted
			// columns
			//
			if(!ProcessNewSortedColumnsHelper( newSortedColumns, fireEvents, groupBy ))
			{
                // CDS 3/24/09 15518 Do not set the sort indicator if the column being removed was placed 
                // back into the SortedColumns collection during the BeforeSortChange event
                ////DA 8.10.02
                ////uwg1243
                ////sort was not cancelled by user, if sort indicator is set to disabled or none,
                ////update our internal value
                //if (sortIndicator == SortIndicator.None || sortIndicator == SortIndicator.Disabled)
                if ((!this.Contains(column) && sortIndicator == SortIndicator.None) || 
                    sortIndicator == SortIndicator.Disabled)
                {
                    column.InitSortIndicator(sortIndicator);
                    column.TempSortIndicator = sortIndicator;
                }
			}
		}	

		private bool ProcessNewSortedColumnsHelper( SortedColumnsCollection newSortedColumns, bool fireEvents, bool groupBy )
		{
			this.inProcessingNewSortedColumns = true;

			bool sortCancelled = false;

            // JDN 11/16/04 Added Synchronous Sorting and Filtering
            ProcessMode sortProcessMode = ProcessMode.Lazy;

			try
			{
				// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// All of this has been made unneccessary by the new FireCommonEvent method
// ------------------------------------------------------------------------------
//				Infragistics.Win.UltraWinGrid.GridEventManager eventManager= null;
//
//				UltraGrid grid = this.band.Layout.Grid as UltraGrid;
//
//				if ( grid != null )
//					eventManager = grid.EventManager;
//
//				if ( eventManager == null )
//					fireEvents = false;
//
//				// Set the fFireEvents to false if we are in initialize layout event
//				//
//				if ( fireEvents )
//					fireEvents = !eventManager.InProgress( GridEventIds.InitializeLayout );
//
//
//				if ( fireEvents )
//				{
//					// fire the before event
//					//
//					firingBeforeSortChange   = true;
//
//					BeforeSortChangeEventArgs eventArgs = new BeforeSortChangeEventArgs( this.band, newSortedColumns );
//				
//					eventManager.Grid.FireEvent( GridEventIds.BeforeSortChange, eventArgs );
//				
//					if ( eventArgs.Cancel )
//						sortCancelled = true;
//
//                    // JDN 11/16/04 Added Synchronous Sorting and Filtering
//                    sortProcessMode = eventArgs.ProcessMode;
//
//					firingBeforeSortChange   = false;
//				}

				// SSP 10/27/05 BR06292
				// Make sure that IsGroupByColumn property of the new sort columns reflect
				// their new group-by states.
				// 
				// --------------------------------------------------------------------------
				for ( int i = 0; i < newSortedColumns.Count; i++ )
				{
					UltraGridColumn column = newSortedColumns[i];
					column.InternalSetIsGroupByColumn( column.TempIsGroupByColumn );
				}
				// --------------------------------------------------------------------------

				UltraGridBase gridBase = this.band.Layout.Grid as UltraGridBase;

				// MRS 1/21/05 - Check for null and if we are in InitializeLayout
				if (gridBase == null)
					fireEvents = false;
				else if (gridBase is UltraGrid)
					fireEvents = !((UltraGrid)gridBase).EventManager.InProgress( GridEventIds.InitializeLayout );
				else if (gridBase is UltraCombo)
					fireEvents = !((UltraCombo)gridBase).EventManager.InProgress( ComboEventIds.InitializeLayout );
				else if (gridBase is UltraDropDown)
					fireEvents = !((UltraDropDown)gridBase).EventManager.InProgress( DropDownEventIds.InitializeLayout );
				else
					Debug.Fail("If we reach this line of code, something is wrong.");
				
				if (fireEvents)
				{
					// fire the before event
					//
					firingBeforeSortChange   = true;

					BeforeSortChangeEventArgs eventArgs = new BeforeSortChangeEventArgs( this.band, newSortedColumns );
				
					fireEvents = gridBase.FireCommonEvent(CommonEventIds.BeforeSortChange, eventArgs, true);
				
					if ( eventArgs.Cancel )
						sortCancelled = true;

					// JDN 11/16/04 Added Synchronous Sorting and Filtering
					sortProcessMode = eventArgs.ProcessMode;

					firingBeforeSortChange   = false;
				}
// ------------------------------------------------------------------------------

				// If the Sort operation was not cancelled then apply
				// the new sorted cols collection
				//
				if ( !sortCancelled )
				{
					// SSP 1/20/05 BR01804
					// If a formula column is sorted or grouped by we need to make sure it's fully 
					// recalculated before we can sort the row collections based on its values.
					// 
					// ----------------------------------------------------------------------------------
					Infragistics.Win.CalcEngine.IUltraCalcManager calcManager = this.Layout.CalcManager;
					if ( null != calcManager )
					{
						// First readd all the formulas that are recalc deferred.
						//
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//ArrayList disableRecalcDeferredColRefs = new ArrayList( );
						List<UltraGridRefBase> disableRecalcDeferredColRefs = new List<UltraGridRefBase>();

						try
						{
							bool hasFormulaSortColumn = false;
							foreach ( UltraGridColumn columnIterator in newSortedColumns )
							{
								UltraGridColumn column = columnIterator.ClonedFromColumn;
								Debug.Assert( null != column );
								if ( null != column && column.HasActiveFormula )
								{
									hasFormulaSortColumn = true;
									ColumnReference colRef = column.CalcReference;
									if ( null != colRef && colRef.RecalcDeferredBase )
									{
										// SSP 2/4/05
										// Use the AddFormulaHelper and RemoveFormulaHelper utility methods 
										// instead. See comments above those method definitions for more info.
										//
										//calcManager.RemoveFormula( colRef.Formula );
										RefUtils.RemoveFormulaHelper( calcManager, colRef.Formula );

										Debug.Assert( ! colRef.disableRecalcDeferredOverride );
										// Set the disableRecalcDeferredOverride to true so the column reference
										// returns false from RecalcDeferred. It will also prevent the property 
										// from being set to true. We are resetting this flag below.
										//
										colRef.disableRecalcDeferredOverride = true;
										disableRecalcDeferredColRefs.Add( colRef );

										// SSP 2/4/05
										// Use the AddFormulaHelper and RemoveFormulaHelper utility methods 
										// instead. See comments above those method definitions for more info.
										//
										//calcManager.AddFormula( colRef.Formula );
										RefUtils.AddFormulaHelper( calcManager, colRef.Formula );
									}
								}
							}

							// Then make sure that everything is calculated.
							//
							if ( hasFormulaSortColumn )
								calcManager.PerformAction( Infragistics.Win.CalcEngine.UltraCalcAction.Recalc, (long)-1L );
						}
						finally
						{
							// Reset the disableRecalcDeferredOverride flag.
							//
							foreach ( UltraGridRefBase reference in disableRecalcDeferredColRefs )
								reference.disableRecalcDeferredOverride = false;
						}
					}
					// ----------------------------------------------------------------------------------

					// SSP 3/30/06 BR11065
					// Don't send any notification to the calc manager if the calc mode is syncrhonous
					// until we have completed the processing.
					// 
					// ----------------------------------------------------------------------------------------
					//this.ApplyNewSortedColumns( newSortedColumns, groupBy );
					BandReference bandCalcRef = this.Band.HasCalcReference ? this.Band.CalcReference : null;
					try
					{
						if ( null != bandCalcRef )
							bandCalcRef.SuspendVerifyGroupLevelSummaryFormulas( );

						this.ApplyNewSortedColumns( newSortedColumns, groupBy );
					}
					finally
					{
						if ( null != bandCalcRef )
							bandCalcRef.ResumeVerifyGroupLevelSummaryFormulas( false );
					}
					// ----------------------------------------------------------------------------------------
					
					// SSP 5/13/02 UWG1124
					// Set the inProcessingNewSortedColumns flag to false before firing the AfterSortChange
					// event so that the rows get sorted properly to new sorted columns collection when
					// any event handlers for AfterSortChange get the properly sorted rows. 
					// The inProcessingNewSortedColumns flag when set to true basically causes the 
					// rows collection to not to sort the rows. So if rows get accessed in AfterSortChange
					// event, they won't be sorted to correctly. So we need to set this flag to false
					// before firing the event.
					//
					this.inProcessingNewSortedColumns = false;

					if ( fireEvents )
					{
                        // JDN 11/16/04 Added Synchronous Sorting and Filtering
                        // Ensure sorting is synchronously processed for either all rows or all expanded rows
                        // if the BeforeSortChangeEventArgs.ProcessMode is set to a synchronous value
						// SSP 2/4/05
						// Use the new EnsureSortedAndFilteredHelper method instead of duplicating the code
						// in a few places. Also make sure to pass in the lowest level band otherwise it
						// will sort and filter lower level bands than this band.
						//
						gridBase.Rows.EnsureSortedAndFilteredHelper( sortProcessMode, this.Band );
						

						//Fire After event
						BandEventArgs bandEventArgs = new BandEventArgs( this.band );
						// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
						//eventManager.Grid.FireEvent( GridEventIds.AfterSortChange, bandEventArgs );		
						gridBase.FireCommonEvent( CommonEventIds.AfterSortChange, bandEventArgs );		
					}
				}
			}
			finally
			{
				this.inProcessingNewSortedColumns = false;
			}
			
			return sortCancelled;
		}

		internal void ApplyNewSortedColumns( SortedColumnsCollection newSortedColumns, bool groupBy )
		{
			// first clear the old list
			//
			this.InternalClear();

			if ( newSortedColumns.Count > 0 )
			{
				UltraGridColumn column;
				
				for ( int i=0; i<newSortedColumns.Count; ++i )
				{
					if ( newSortedColumns[i] != null )
					{
						column = ((UltraGridColumn)newSortedColumns[i]).ClonedFromColumn;

						if ( column == null )
							column = newSortedColumns[i];

						this.List.Add(column);

						// SSP 11/8/01 UWG688
						// Set the group by column to false before setting the sort indicator
						// because calling InternalSetSortIndicator causes the grid to redraw
						// leading to whole bunch of things that would cause problems in the
						// midst of inserting a group by column in the sorted columns collection.
						//
						column.InternalSetIsGroupByColumn( ((UltraGridColumn)newSortedColumns[i]).TempIsGroupByColumn );
						
						// SSP 10/1/01 UWG388
						// call InternalSetSortIndicator instead
						//
						//column.SortIndicator = ((Column)newSortedColumns[i]).TempSortIndicator;
						column.InternalSetSortIndicator( ((UltraGridColumn)newSortedColumns[i]).TempSortIndicator );
						
						// SSP 11/8/01 UWG688
						// Moved it up above
						//
						//column.InternalSetIsGroupByColumn( ((UltraGridColumn)newSortedColumns[i]).TempIsGroupByColumn );

						// SSP 9/14/04
						// Got rid of set on GroupByHierarchyVersion properties on the column and band.
						// So call BumpGroupByHierarchyVersion method instead.
						//
						// ----------------------------------------------------------------------------
						//column.GroupByHierarchyVersion = Math.Max( column.GroupByHierarchyVersion, ((UltraGridColumn)newSortedColumns[i]).GroupByHierarchyVersion );
						int maxGroupByHierarchyVersion = Math.Max( column.GroupByHierarchyVersion, ((UltraGridColumn)newSortedColumns[i]).GroupByHierarchyVersion );
						if ( maxGroupByHierarchyVersion != column.GroupByHierarchyVersion )
                            column.BumpGroupByHierarchyVersion( );
						// ----------------------------------------------------------------------------
					}
					else
						Debug.Fail("Sorted Cols list has null ptr values in SortedColumnsCollection.ApplyNewSortedColumns.");
				}
				// clear the new cols list    
				//
				newSortedColumns.Clear();

			}

			// incremenet the sortVersion if the added column was not
			// a group by column. 
			if ( !groupBy )
			{
				// JJD 10/18/01
				// Call BumpSortVersion method which also dirties the metrics
				//
				this.BumpSortVersion();

				// SSP 1/23/02 UWG963
				// Moved this here from the BumpSortVersion
				//
				this.OnSortChanged();
			}
			else
			{
				// JJD 1/19/02 - UWG953
				// Moved logic into OnSortChanged
				//				
				this.OnSortChanged();
			}
		}

		internal void OnSortChanged( )
		{
			this.OnSortChanged( null );
		}

		// SSP 1/23/02
		// Moved OnSortChanged from RowScrollRegions class to here
		//
		// JJD 1/19/02 - UWG953
		// Added internal OnSortChanged method
		//
		// SSP 2/20/03 UWG1993
		// Call OnSortChanged method off the sorted columns to fix a problem where
		// when a row's value is changed and it's sort position is refereshed, the
		// vertical scroll bar shows up even though previously it was not visible and
		// all the rows fit the row scroll region.
		// Added an overload.
		//
		//internal void OnSortChanged( )
		internal void OnSortChanged( UltraGridRow firstRow )
		{
			Debug.Assert( null != this.Band, "Null band in SortedColumnsCollection !" );

			UltraGridBand band = this.Band;
			if ( null == band )
				return;

			Debug.Assert( null != this.Band.Layout, "Attached band does not have a layout !" );

			UltraGridLayout layout = band.Layout;
			if ( null == layout )
				return;

			RowScrollRegionsCollection rsrColl = layout.RowScrollRegions;

			Debug.Assert( null != rsrColl, "Why is Layout.RowScrollRegions null ?" );

			// JM 01-24-02  This is valid - don't do the assert.
			//Debug.Assert( rsrColl.Count > 0, "Why is Layout.RowScrollRegions empty ?" );

			if ( null == rsrColl || rsrColl.Count <= 0 )
				return;
 
			UltraGridBase grid = layout.Grid;

			// SSP 3/19/04 UWG3017
			// If the ScrollBounds mode is ScrollToFill, we don't have to do all of the 
			// following. Not only that it actually interferes with the ScrollToFill mode.
			//
			// ----------------------------------------------------------------------------------
			// SSP 5/12/05 - NAS 5.2 Fixed Rows
			// Added ScrollBoundsResolved. If there are fixed rows at the bottom then
			// always resolve the scroll bounds to ScrollToFill. Added ScrollBoundsResolved
			// property.
			//
			//if ( ScrollBounds.ScrollToFill == this.Band.Layout.ScrollBounds )
			if ( 
				// SSP 6/1/07 BR23170 
				// Added the following line that checks for InInitializeLayout.
				// We don't want to access ScrollBoundsResolved which will cause the
				// rows to get synced if they are dirty. Instead if we are in 
				// initialize layout, simply dirty the grid element and return.
				// 
				null == grid || grid.InInitializeLayout 
				|| ScrollBounds.ScrollToFill == this.Band.Layout.ScrollBoundsResolved )
			{
				this.Band.Layout.DirtyGridElement( true );
				return;
			}
			// ----------------------------------------------------------------------------------

			bool original_inProcessingNewSortedColumns = 
				this.inProcessingNewSortedColumns;

			try
			{
				// SSP 9/5/03 UWG2547
				// While we are initializing the data source don't do what we are doing below.
				// Enclosed the block of code into the if block.
				//
				if ( null != this.Band.Layout.Grid && ! this.Band.Layout.Grid.InInitializeLayout )
				{
					bool[] setScrollToBeginningArr = new bool[rsrColl.Count];
					int i;

                    // MRS 3/9/2009 - TFS12415
                    // This fix caused quite a number of critical issues, so I am ripping it out and fixing
                    // TFS12415 in a different way. 
                    //
                    #region Removed
                    //// MRS 3/6/2009 - TFS14557
                    //// Added 'if' block. 
                    //// When exporting, we get in here from InitializeFrom of the layout, and it's
                    //// unneccessary, anyway, and only causes problem. 
                    ////
                    //if (this.Band.Layout.IsExportLayout == false)
                    //{
                    //    // MBS 1/16/09 - TFS14232
                    //    // Before calculating whether or not we are going to set the scroll position 
                    //    // back to the beginning, we should make sure that the filters have been
                    //    // evaluated, since this will naturally affect whether the first and 
                    //    // last scrollable rows are visible.
                    //    try
                    //    {
                    //        // Set this to false so that we don't bail out of the EnsureFiltersEvaluated prematurely
                    //        this.inProcessingNewSortedColumns = false;

                    //        // MBS 2/26/09 - TFS12415
                    //        // Instead of calling EnsureFiltersEvaluated, call VerifyGroupByVersion.  We will eventually 
                    //        // evaluate the filters later anyway, but we need to make sure at this point that the 
                    //        // GroupByColumns are processed in case the user does anything that will cause something like
                    //        // the filters to be re-evaluated before we paint (and not process the GroupByRows yet).                        
                    //        //
                    //        //this.Band.Layout.Rows.EnsureFiltersEvaluated();
                    //        this.Band.Layout.Rows.VerifyGroupByVersion();
                    //    }
                    //    finally
                    //    {
                    //        this.inProcessingNewSortedColumns = original_inProcessingNewSortedColumns;
                    //    }                    
                    // }
                    #endregion //Removed

                    for ( i = 0; i < rsrColl.Count; i++ )
					{
						RowScrollRegion rsr = rsrColl[i];

						// SSP 12/1/03
						// If first row has not been initialized which would be the case when initialzing
						// the layout, don't bother doing rest of the stuff.
						//
						// ------------------------------------------------------------------------
						if ( null == rsr.InternalFirstRow )
						{
							setScrollToBeginningArr[i] = false;
							continue;
						}
						// ------------------------------------------------------------------------

						// Set inProcessingNewSortedColumns to true to prevent the RowsCollection
						// to resort while we find out the current first and last row are visible
						// (just to see if we need a scroll bar or not after we resort).
						//
						this.inProcessingNewSortedColumns = true;

						// SSP 4/28/05 - NAS 5.2 Fixed Rows
						// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
						// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
						//
						//setScrollToBeginningArr[i] = rsr.IsFirstRowVisible() && rsr.IsLastRowVisible();
						setScrollToBeginningArr[i] = rsr.IsFirstScrollableRowVisible() && rsr.IsLastScrollableRowVisible();

						// SSP 3/7/03 - UWG1993
						// This is to solve a situation where a row is added and that row is not in
						// the visible rows collection so above calls to IsFirstRowVisible and
						// IsLastRowVisible fail.
						//
						if ( ! setScrollToBeginningArr[i] )
						{
							UltraGridRow oldFirstRow = rsr.FirstRow;

							if ( null != firstRow )
								rsr.FirstRow = firstRow;
							else
								rsr.FirstRow = this.Band.Layout.Rows.GetFirstVisibleRow( );

							rsr.SetDestroyVisibleRows( true );

							// SSP 1/20/05 BR01844
							// Don't cause the Value of the scroll bar to be changed otherwise the logic
							// for maintaining the scroll position when you sort won't work properly.
							//
							//rsr.RegenerateVisibleRows( );
							rsr.RegenerateVisibleRows( false );

							// SSP 4/28/05 - NAS 5.2 Fixed Rows
							// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
							// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
							//
							//setScrollToBeginningArr[i] = rsr.IsFirstRowVisible( ) && rsr.IsLastRowVisible( );
							setScrollToBeginningArr[i] = rsr.IsFirstScrollableRowVisible( ) && rsr.IsLastScrollableRowVisible( );

							if ( !setScrollToBeginningArr[i] )
							{
								rsr.FirstRow = oldFirstRow;
							}
						}
					}
			
					for ( i = 0; i < rsrColl.Count; i++ )
					{
						RowScrollRegion rsr = rsrColl[i];

						// JJD 1/19/02 - UWG953
						// Only reset the first row if the first and last rows are both visible
						//
						// SSP 1/23/02
						// Use the previously calculated value for setScrollToBeginning
						//
						//bool setScrollToBeginning = this[i].IsFirstRowVisible() && this[i].IsLastRowVisible();
						bool setScrollToBeginning = setScrollToBeginningArr[i];

						rsr.SetDestroyVisibleRows( true, setScrollToBeginning );

						// SSP 1/23/02 UWG963
						// If we are setting the scroll to the beggining, then set
						// the FirstRow to the first visible row.
						//
                        if (setScrollToBeginning)
                        {
                            // Set the inProcessingNewSortedColumns to true so that GetFirstVisibleRow
                            // will the cause the RowsCollection to resort and thus get the right
                            // first visible row.
                            //
                            this.inProcessingNewSortedColumns = false;

                            // SSP 2/20/03 UWG1993
                            // Call OnSortChanged method off the sorted columns to fix a problem where
                            // when a row's value is changed and it's sort position is refereshed, the
                            // vertical scroll bar shows up even though previously it was not visible and
                            // all the rows fit the row scroll region.
                            // Added an overload.
                            // Use the passed in firstRow as the first row.
                            //
                            //rsr.FirstRow = this.Band.Layout.Rows.GetFirstVisibleRow();
                            // SSP 5/19/06 BR12319
                            // Get the first scrollable row in case there are fixed rows on top of the row collection.
                            // 
                            //rsr.FirstRow = null != firstRow ? firstRow : this.Band.Layout.Rows.GetFirstVisibleRow();
                            rsr.FirstRow = null != firstRow ? firstRow : this.Band.Layout.Rows.GetFirstScrollableRow();
                        }
					}
				}

				this.Band.Layout.ColScrollRegions.DirtyMetrics( );
			}
			catch ( Exception exc )
			{
				Debug.Assert( false, exc.Message );
			}
			finally
			{
				this.inProcessingNewSortedColumns = original_inProcessingNewSortedColumns;
			}
		}



		internal void BumpSortVersion()
		{
			// JJD 10/18/01
			// Bump the sort version and dirty row and column metrics
			//
			this.sortVersion++;

			// JJD 1/19/02 - UWG953
			// Moved logic into OnSortChanged
			//
			// SSP 1/23/02 UWG963
			// This method get's called few times when setting a sorted column
			// And we don't want to call OnSortChanged from here which will
			// force the resorting of the rows prematurely.
			// Commented this out and put it in ApplySortedColumns
			//
			//this.band.Layout.RowScrollRegions.OnSortChanged();

			// SSP 7/24/03 - Fixed headers
			// If a header is grouped by, it can potentially get hidden (since we by default
			// don't show the group by columns). So bump the fixed headers version number
			// so the fixed headers cache gets recalculated.
			//
			if ( null != this.Band )
				this.Band.BumpFixedHeadersVerifyVersion( );

			// SSP 7/25/03 - Row Layout Functionality
			// Since grouping by a column can potentially lead to a column being hidden,
			// dirty the row layout info as well.
			//
			if ( null != this.Band )
				this.Band.DirtyRowLayoutCachedInfo( );
		}


		/// <summary>
		/// Re-Sorts the sorted columns collection
		/// </summary>
		/// <param name="regroupBy">
		/// True to regroup group by rows.
		/// </param>
		// SSP 1/8/02 UWG911
		// Added regroup paremeter to force the group by hierarchy to
		// be recreated.
		//
		//public void RefreshSort()
		public void RefreshSort( bool regroupBy )
		{
			if ( regroupBy )
			{
				Debug.Assert( null != this.Band );

				if ( null != this.Band )
					this.Band.BumpGroupByHierarchyVersion( );
			}

			this.BumpSortVersion();

			// SSP 8/8/02 UWG1274
			// When resorting the rows, incremeneting the sort versionis not sufficient.
			// We need to also refresh the grid as well so the display reflects the resorted
			// rows.
			//
			this.OnSortChanged( );
		}

		internal SortedColumnsCollection Clone()
		{
			SortedColumnsCollection clone = new SortedColumnsCollection(this.band);
			//SortedColumnsCollection clone = (SortedColumnsCollection)this.MemberwiseClone();

			
			if ( clone == null )
				return null;

			clone.isClone = true;
			
			try
			{
				// copy all items over into the clone's collection
				//
				for ( int i=0; i< this.Count; ++i)
				{
					object clonedCol = this[i].Clone();
					clone.List.Add( clonedCol );

					// Save the sort indicator on the column
					//

					((UltraGridColumn)this[i]).TempSortIndicator = ((UltraGridColumn)this[i]).SortIndicator;				
					((UltraGridColumn)clonedCol).TempSortIndicator = ((UltraGridColumn)this[i]).SortIndicator;
					
					((UltraGridColumn)this[i]).TempIsGroupByColumn = ((UltraGridColumn)this[i]).IsGroupByColumn;
					((UltraGridColumn)clonedCol).TempIsGroupByColumn = ((UltraGridColumn)this[i]).IsGroupByColumn;
				}
				
			}			
			catch(Exception)
			{
				Debug.Fail("SortedColumnsColl::Clone() failed");
				clone = null;
			}
            
			return clone;
		}

		
		internal void InitializeFrom( SortedColumnsCollection source, PropertyCategories categories  )
		{
			// Since ssPropCatSortedColumns has more than one bit tunred on we need
			// to check the anded bits for equality
			//
			if ( ( categories & PropertyCategories.SortedColumns ) != PropertyCategories.SortedColumns )
				return;

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			// SSP 10/26/01
			// Before calling this.Clear() below, store the columns that are
			// group by because the Clear method sets the IsGroupByColumn to
			// false for all the columns in this sorted columns collection.
			// (There is a possibility that source could contain the same
			// columns that are group by columns that are contained in this
			// collection. I which case this.Clear call is going to set 
			// the IsGroupByColumn to false on those columns which we shouldn't
			// do. We should preserver the group by column status of the columns
			// in the source. So store the group by columns into an array list
			// and after calling this.Clear() method, restore the isGroupByColumn
			// 
			//----------------------------------------------------------
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList groupByColumns = new ArrayList( Math.Min( 5, source.Count ) );
			List<UltraGridColumn> groupByColumns = new List<UltraGridColumn>( Math.Min( 5, source.Count ) );
			
			for ( int i = 0; i < source.Count; i++ )
			{
				if ( source[i].IsGroupByColumn )
					groupByColumns.Add( source[i] );
			}

			this.Clear();

			for ( int i = 0; i < groupByColumns.Count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//((UltraGridColumn)groupByColumns[i]).InternalSetIsGroupByColumn( true );
				groupByColumns[ i ].InternalSetIsGroupByColumn( true );
			}
			//----------------------------------------------------------


			if ( source.serializedColumnIDs != null )
			{
				this.ProcessSerializedColumnIDs( source.serializedColumnIDs );
				return;
			}

			// if there are no bands on the source just return
			//
			if ( source.Count < 1 )
				return;
			
			ColumnsCollection columnCollect = this.band.Columns;
			if ( columnCollect == null )
				return;
    
			for ( int i = 0; i < source.Count; i++ )
			{
				// iterate over this bands columns looking for a logical match
				//
				for ( int j = 0; j < columnCollect.Count; j++ )
				{
					// Call the column's CanInitializeFrom method to see if the column matches
					// 
					if ( columnCollect[j].CanInitializeFrom(source[i]) )
					{						
						this.SetSortedColumn ( columnCollect[j], source[i].SortIndicator, source[i].IsGroupByColumn, false, false );
					}
				}
			}

		}

		#region CopyFrom

		// SSP 12/27/05 BR07564
		// 
		/// <summary>
		/// Initializes this collection with the source collection.
		/// </summary>
		/// <param name="source">The source collection of sorted columns from which to copy.</param>
		/// <remarks>
		/// <p class="body">
		/// Columns are matched by their <see cref="UltraGridColumn.Key"/>'s. This method will clear this collection
		/// and copy sorted columns from the source collection.
		/// </p>
		/// </remarks>
		public void CopyFrom( SortedColumnsCollection source )
		{
			this.InitializeFrom( source, PropertyCategories.All );
		}

		#endregion // CopyFrom

			
	}// end of class

	/// <summary>
	/// Enumerator for the SortedColumnsCollection
	/// </summary>
	public class SortedColumnEnumerator: DisposableObjectEnumeratorBase
	{   
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="sortedColumns">The collection of sorted columns to enumerate.</param>
		public SortedColumnEnumerator( SortedColumnsCollection sortedColumns ) : base( sortedColumns )
		{
		}

		/// <summary>
		/// Type-safe version of Current 
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn Current 
		{
			get
			{
				return (UltraGridColumn)((IEnumerator)this).Current;
			}
		}

	}
}
