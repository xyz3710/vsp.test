#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Collections.Generic;

// SSP 11/9/04 - Merged Cell Feature
// Added MergedCellInfo class.
//


namespace Infragistics.Win.UltraWinGrid
{
	#region MergedCellCollection Class

	internal class MergedCellCollection : SparseArray
	{
		#region Private Vars

		private UltraGridColumn column = null;
		private MergedCellCache mergedCellCache = null;

		// This is for an optimization.
		//
		internal int likelyCellElementIndex = -1;

		// SSP 7/3/07 BR17185 - Optimizations
		// Added nonMergedRows. Existence of a row in this set indicates 
		// that the row is not merged with any neighboring rows. Non-existant
		// means the row needs to be checked for mergeability.
		// 
		private Hashtable nonMergedRows = null;

		#endregion // Private Vars

		#region Constructor
			
		internal MergedCellCollection( UltraGridColumn column, MergedCellCache mergedCellCache ) : base( 40, 0.25f, false )
		{
			if ( null == column || null == mergedCellCache )
				throw new ArgumentNullException( );

			this.column = column;
			this.mergedCellCache = mergedCellCache;
		}

		#endregion // Constructor

		#region MergedCellCache 
		
		internal MergedCellCache MergedCellCache 
		{
			get
			{
				return this.mergedCellCache;
			}
		}

		#endregion // MergedCellCache 

		#region Column
			
		internal UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		#endregion // Column

		#region Rows

		internal RowsCollection Rows
		{
			get
			{
				return this.mergedCellCache.Rows;
			}
		}

		#endregion // Rows

		#region GetMergedCell

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Gets the merged cell if any associated with the cell associated with the specified row
		//        /// and this collection's column.
		//        /// </summary>
		//        /// <param name="row"></param>
		//        /// <returns></returns>
		//#endif
		//        internal MergedCell GetMergedCell( UltraGridRow row )
		//        {
		//            return this.GetMergedCell( row, true );
		//        }

		#endregion Not Used

		internal MergedCell GetMergedCell( UltraGridRow row, bool allocate )
		{
			int index;
			return this.GetMergedCell( row, out index, allocate );
		}

		private MergedCell GetMergedCell( UltraGridRow row, out int index, bool allocate )
		{
			index = -1;

			// SSP 7/3/07 BR17185 - Optimizations
			// 
			if ( null != this.nonMergedRows && this.nonMergedRows.ContainsKey( row ) )
				return null;
			
			MergedCell mergedCell = null;
			if ( this.mergedCellCache.IsRowValid( row ) )
			{
				mergedCell = this.GetMergedCellHelper( row.Index, out index );
				Debug.Assert( index >= 0 );
				if ( null == mergedCell && allocate && index >= 0 )
				{
					mergedCell = this.CreateMergedCell( row );
					if ( null != mergedCell )
						this.Insert( index, mergedCell );
					// SSP 7/3/07 BR17185 - Optimizations
					// Added the else block.
					// 
					else
					{
						if ( null == this.nonMergedRows )
							this.nonMergedRows = new Hashtable( );

						this.nonMergedRows.Add( row, null );
					}
				}

				// If no merged cell was found and created then set the index to -1.
				//
				if ( null == mergedCell )
					index = -1;
			}

			return mergedCell;
		}

		private MergedCell GetMergedCellHelper( int rowIndex, out int index )
		{
			index = -1;
			int i = 0, j = this.Count - 1, m = 0;

			while ( i <= j )
			{
				m = ( i + j ) / 2;
				MergedCell mc = (MergedCell)this[ m ];
				if ( rowIndex < mc.StartRow.Index )
					j = m - 1;
				else if ( rowIndex > mc.EndRow.Index )
					i = m + 1;
				else
				{
					index = m;
					return mc;
				}
			}

			// If we didn't find the associated merged cell then set the index to the index at which
			// the merged cell for the row should be inserted if it were to be created.
			//
			index = i > m ? 1 + m : m;
			return null;
		}

		#endregion // GetMergedCell

		#region GetPrevSortColumn

		private UltraGridColumn GetPrevSortColumn( )
		{
			UltraGridColumn prevCol = null;
			SortedColumnsCollection sortedCols = this.Column.Band.SortedColumns;
			for ( int i = 0, count = sortedCols.Count; i < count; i++ )
			{
				UltraGridColumn col = sortedCols[i];

				if ( col == this.Column )
					return prevCol;

				if ( MergedCellStyle.OnlyWhenSorted == col.MergedCellStyleResolved 
					&& col.MergedCellEnabledResolved )
					prevCol = col;
			}

			return null;
		}

		#endregion // GetPrevSortColumn

		#region CanCellsBeMerged
			
		private bool CanCellsBeMerged( UltraGridRow row1, UltraGridRow row2 )
		{
			// SSP 4/14/05 - NAS 5.2 Filter Row/Summaries Extention
			// Only data rows can be merged.
			//
			if ( ! row1.IsDataRow || ! row2.IsDataRow )
				return false;

			UltraGridColumn column = this.Column;

			bool sameCellValues;
			if ( null != column.MergedCellEvaluator )
			{
				sameCellValues = column.MergedCellEvaluator.ShouldCellsBeMerged( row1, row2, column );
			}
			else
			{
				MergedCellEvaluationType mergeEvaluationType = column.MergedCellEvaluationTypeResolved;
				if ( MergedCellEvaluationType.MergeSameValue == mergeEvaluationType )
				{
					object val1 = row1.GetCellValue( column );
					object val2 = row2.GetCellValue( column );
					// JAS v5.2 GroupBy Break Behavior 5/4/05 - Added the 'column' argument.
					//
					//sameCellValues = 0 == RowsCollection.RowsSortComparer.DefaultCompare( val1, val2, false );
					sameCellValues = 0 == RowsCollection.RowsSortComparer.DefaultCompare( val1, val2, false, column );
				}
				else
				{
					string val1 = row1.GetCellText( column );
					string val2 = row2.GetCellText( column );
					sameCellValues = val1 == val2;
				}
			}

			if ( ! sameCellValues )
				return false;

			// If the row above is expanded we can't merge with the cell below.
			//
			if ( ((ISparseArrayMultiItem)row1).ScrollCount > 1 )
				return false;

			// Template add-row can't be merged with any other row.
			//
			// SSP 4/14/05 Filter Row/Summaries Extention
			// Added check for IsDataRow in the beginning of this method. As a 
			// result this is not necessary anymore 
			//
			//if ( row1.IsTemplateAddRow || row2.IsTemplateAddRow )
			//	return false;

			// If the cell above has auto preview then don't merge with the cell below.
			//
			if ( row1.AutoPreviewDisplayed )
				return false;

			// SSP 3/29/05 - NAS 5.2 Fixed Rows/Filter Row
			// A fixed row can't merge with a non-fixed row. Also a filter row can't merge at all.
			//
			if ( row1.Fixed != row2.Fixed || row1.IsFilterRow || row2.IsFilterRow
				// SSP 8/30/06 BR15101 BR15110
				// Also add rows shouldn't be merged.
				// 
				|| row1.IsAddRow || row2.IsAddRow )
				return false;

			// If there is row spacing between the rows we can't merge the rows.
			//
			// SSP 4/7/05 - BR03158
			// Allow merging cells even if row spacing is set. Since we allow merging
			// when cell spacing is set there is no reason to not allow when there's
			// row spacing. The user can force cells to not merge by implementing
			// IMergeCellEvaluator interface. Commented out the following code that
			// checks for row spacing.
			//
			//if ( row1.RowSpacingAfterResolved > 0 || row2.RowSpacingBeforeResolved > 0 )
			//	return false;

			// If the cells have different editors then don't merge the cells.
			//
			if ( column.GetEditor( row1 ) != column.GetEditor( row2 ) )
				return false;

			UltraGridCell cell1 = row1.GetCellIfAllocated( column );
			UltraGridCell cell2 = row2.GetCellIfAllocated( column );

			// Unmerge cells as soon as the user modifies the contents of a cell in edit mode.
			//
			if ( null != cell1 && cell1.IsInEditMode && cell1.EditValueChanged
				|| null != cell2 && cell2.IsInEditMode && cell2.EditValueChanged )
				return false;

			// If the cell merge style is OnlyWhenSorted and if cells in the prev sorted column
			// aren't merged then don't merge cells in this column either. For example let's say
			// that State and City columns are merged and A and B states each had a city with 
			// the same name of C then we should not merge C cells that cross A and B states.
			//
			if ( MergedCellStyle.OnlyWhenSorted == column.MergedCellStyleResolved )
			{
                Debug.Assert( SortIndicator.Ascending == column.SortIndicator 
					|| SortIndicator.Descending == column.SortIndicator );

				UltraGridColumn prevSortCol = this.GetPrevSortColumn( );
				if ( null != prevSortCol && ! prevSortCol.AreCellsMerged( row1, row2 ) )
					return false;
			}

			// SSP 1/20/05 BR01806
			// Cells with data errors should not be merged.
			//
            if ( row1.HasDataError( column ) || row2.HasDataError( column ) )
				return false;

			return true;
		}

		#endregion // CanCellsBeMerged

		#region GetPrevVisibleRow

		private UltraGridRow GetPrevVisibleRow( UltraGridRow row )
		{
			return this.Rows.GetPrevVisibleRow( row );
		}

		#endregion // GetPrevVisibleRow

		#region GetNextVisibleRow

		private UltraGridRow GetNextVisibleRow( UltraGridRow row )
		{
			return this.Rows.GetNextVisibleRow( row );
		}

		#endregion // GetNextVisibleRow

		#region CreateMergedCell

		private MergedCell CreateMergedCell( UltraGridRow row )
		{
			UltraGridRow startRow = row;
			UltraGridRow endRow   = row;

			for (;;)
			{
				UltraGridRow tmpRow = this.GetPrevVisibleRow( startRow );
				if ( null != tmpRow && this.CanCellsBeMerged( tmpRow, startRow ) )
					startRow = tmpRow;
				else
					break;
			}

			for (;;)
			{
				UltraGridRow tmpRow = this.GetNextVisibleRow( endRow );
				if ( null != tmpRow && this.CanCellsBeMerged( endRow, tmpRow ) )
					endRow = tmpRow;
				else
					break;
			}

			// Only create a merged cell if it actually merges two or more cells.
			//
			return startRow != endRow ? new MergedCell( startRow, endRow, this ) : null;
		}

		#endregion // CreateMergedCell

		#region RemoveMergedCellFor

		internal void RemoveMergedCellFor( UltraGridRow row, bool removeNeighbors )
		{
			if ( removeNeighbors )
			{
				UltraGridRow rowAbove = this.GetPrevVisibleRow( row );
				UltraGridRow rowBelow = this.GetNextVisibleRow( row );

				if ( null != rowAbove )
					this.RemoveMergedCellFor( rowAbove, false );

				if ( null != rowBelow )
					this.RemoveMergedCellFor( rowBelow, false );
			}

			// SSP 7/3/07 BR17185 - Optimizations
			// 
			if ( null != this.nonMergedRows )
				this.nonMergedRows.Remove( row );

			int index;
			MergedCell mergedCell = this.GetMergedCell( row, out index, false );
			if ( null != mergedCell && index >= 0 )
			{
				this.RemoveAt( index );
				this.MergedCellCache.InvalidateItemAllRegions( mergedCell, true );
			}
			else
			{
				// If no merged cell was found then we still need to invalidate the cell area
				// because this gets called in resposnse to change in the state of a cell 
				// which could lead to merged cell being unmerged or neighboring cells to be
				// merged.
				//
				this.MergedCellCache.InvalidateItemAllRegions( row, this.Column, true );
			}
		}

		#endregion // RemoveMergedCellFor
	}

	#endregion // MergedCellCollection Class

	#region MergedCellCache Class
	
	internal class MergedCellCache
	{
		#region Private Vars

		private RowsCollection rows = null;
		private int rowHeightVersion = 0;
		private MergedCellCollection[] mergedCellCollArr = null;
		private int verifiedColumnsVersion = -1;
		internal int verifiedMergedCellVersion = -1;

		#endregion // Private Vars

		#region Constructor
		
		internal MergedCellCache( RowsCollection rows )
		{
			this.rows = rows;
			this.verifiedMergedCellVersion = this.Band.MergedCellVersion;
		}

		#endregion // Constructor

		#region Rows

		internal RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}

		#endregion // Rows

		#region Band

		internal UltraGridBand Band
		{
			get
			{
				return this.rows.Band;
			}
		}

		#endregion // Band

		#region RowHeightVersion

		internal int RowHeightVersion
		{
			get
			{
				return this.rowHeightVersion 
					+ this.Band.MajorRowHeightVersion 
					+ this.Band.SynchronizedRowHeightVersion;
			}
		}

		#endregion // RowHeightVersion

		#region IsRowValid
		
		internal bool IsRowValid( UltraGridRow row )
		{
			return row.ParentCollection == this.rows && row.VisibleIndex >= 0;
		}

		#endregion // IsRowValid

		#region BumpRowHeightVersion

		internal void BumpRowHeightVersion( )
		{
			this.rowHeightVersion++;
		}

		#endregion // BumpRowHeightVersion

		#region GetMergedCellCollection

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Gets the MergedCellCollection for the specified column.
		//        /// </summary>
		//        /// <param name="column"></param>
		//        /// <returns></returns>
		//#endif
		//        internal MergedCellCollection GetMergedCellCollection( UltraGridColumn column )
		//        {
		//            return this.GetMergedCellCollection( column, true );
		//        }

		#endregion Not Used
		
		internal MergedCellCollection GetMergedCellCollection( UltraGridColumn column, bool allocate )
		{
			int index = column.Index;
			Debug.Assert( index >= 0 );
			if ( index < 0 )
				return null;

			if ( this.verifiedColumnsVersion != this.Band.ColumnsVersion )
			{
				// SSP 7/3/07 BR17185 - Optimizations
				// We need to reset the verifiedColumnsVersion otherwise we'll keep
				// resyncing everytime.
				// 
				this.verifiedColumnsVersion = this.Band.ColumnsVersion;

				ColumnsCollection cols = this.Band.Columns;
				Hashtable table = null;
				if ( null != this.mergedCellCollArr )
				{
					table = new Hashtable( cols.Count, 0.5f );
					foreach ( MergedCellCollection coll in this.mergedCellCollArr )
					{
						if ( null != coll )
							table[ coll.Column ] = coll;
					}
				}
				else if ( ! allocate )
					return null;

				int count = cols.Count;
				this.mergedCellCollArr = new MergedCellCollection[ count ];
				for ( int i = 0; i < count; i++ )
				{
					UltraGridColumn col = cols[i];
					this.mergedCellCollArr[ i ] = null != table ? (MergedCellCollection)table[ col ] : null;
				}
			}

			MergedCellCollection mergedCellsColl = this.mergedCellCollArr[ index ];
			if ( null == mergedCellsColl && allocate )
				this.mergedCellCollArr[ index ] = mergedCellsColl = new MergedCellCollection( column, this );

			return mergedCellsColl;
		}

		#endregion // GetMergedCellCollection

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region DirtyScrollRegions

		//#if DEBUG
		//        /// <summary>
		//        /// Dirties the scroll regions so they reposition their elements and repaint.
		//        /// </summary>
		//#endif
		//        internal void DirtyScrollRegions( )
		//        {
		//            UltraGridLayout layout = this.Band.Layout;
		//            if ( null != layout )
		//                layout.DirtyDataAreaElement( );
		//        }

		//        #endregion // DirtyScrollRegions

		#endregion Not Used

		#region GetMergedCell
		
		internal MergedCell GetMergedCell( UltraGridRow row, UltraGridColumn column, bool allocate )
		{
			MergedCellCollection mergedCellsColl = this.GetMergedCellCollection( column, allocate );
			return null != mergedCellsColl ? mergedCellsColl.GetMergedCell( row, allocate ) : null;
		}

		#endregion // GetMergedCell

		#region InvalidateItemAllRegions

		internal void InvalidateMergedCellAllRegions( UltraGridRow row, UltraGridColumn column )
		{
			MergedCell mergedCell = this.GetMergedCell( row, column, false );
			if ( null != mergedCell )
				this.InvalidateItemAllRegionsHelper( mergedCell, column, false );
		}
		
		internal void InvalidateItemAllRegions( UltraGridRow row, UltraGridColumn column, bool dirtyDataAreaElem )
		{
			this.InvalidateItemAllRegionsHelper( row, column, dirtyDataAreaElem );
		}

		internal void InvalidateItemAllRegions( MergedCell mergedCell, bool dirtyDataAreaElem )
		{
			this.InvalidateItemAllRegionsHelper( mergedCell, mergedCell.Column, dirtyDataAreaElem );
		}

		private void InvalidateItemAllRegionsHelper( object item, UltraGridColumn column, bool dirtyDataAreaElem )
		{
			UltraGridLayout layout = this.Rows.Layout;
			if ( this.Rows.IsPotentiallyVisibleOnScreen && null != layout && ! layout.Grid.IsUpdating )
			{
				RowScrollRegionsCollection rsrColl = layout.RowScrollRegions;
				ColScrollRegionsCollection csrColl = layout.ColScrollRegions;

				for ( int i = 0; i < rsrColl.Count; i++ )
				{
					RowScrollRegion rsr = rsrColl[i];

					for ( int j = 0; j < csrColl.Count; j++ )
					{
						ColScrollRegion csr = csrColl[j];

						UIElement elem = null;
						
						if ( item is UltraGridRow )
						{
							elem = ((UltraGridRow)item).GetUIElement( rsr, csr, false );
							//if ( null != column && null != elem )
							//	elem = elem.GetDescendant( typeof( CellUIElement ), column );
						}
						else if ( item is MergedCell )
						{
							elem = ((MergedCell)item).GetUIElement( rsr, csr );
						}
																												
						if ( null != elem )
						{
							elem.DirtyChildElements( );

							if ( dirtyDataAreaElem )
								layout.DirtyDataAreaElement( false, false );
						}
					}
				}
			}
		}

		#endregion // InvalidateItemAllRegions

		#region ShouldBreakOnStateChange

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool ShouldBreakOnStateChange( MergedCell.State state )
		private static bool ShouldBreakOnStateChange( MergedCell.State state )
		{
			return MergedCell.State.Activation != state 
				&& MergedCell.State.Selection != state
				// We don't break as soon as the cell is entered edit mode. We break as soon
				// as the user modifies the value.
				//
				&& MergedCell.State.EnterEditMode != state
				&& MergedCell.State.Invalidate != state;
		}

		#endregion // ShouldBreakOnStateChange

		#region MergedCell_RowStateChanged
		
		internal void MergedCell_RowStateChanged( UltraGridRow row, MergedCell.State state )
		{
			if ( null != this.mergedCellCollArr )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//bool shouldBreak = this.ShouldBreakOnStateChange( state );
				bool shouldBreak = MergedCellCache.ShouldBreakOnStateChange( state );

				for ( int i = 0; i < this.mergedCellCollArr.Length; i++ )
				{
					MergedCellCollection mergedCells = this.mergedCellCollArr[ i ];
					if ( null != mergedCells )
					{
						if ( ! shouldBreak )
							this.InvalidateMergedCellAllRegions( row, mergedCells.Column );
						else
							mergedCells.RemoveMergedCellFor( row, true );
					}
				}
			}
		}

		#endregion // MergedCell_RowStateChanged

		#region MergedCell_CellStateChanged
		
		internal void MergedCell_CellStateChanged( UltraGridCell cell, MergedCell.State state )
		{
			this.MergedCell_CellStateChanged( cell.Row, cell.Column, state );
		}

		internal void MergedCell_CellStateChanged( UltraGridRow row, UltraGridColumn column, MergedCell.State state )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//if ( ! this.ShouldBreakOnStateChange( state ) )
			if ( !MergedCellCache.ShouldBreakOnStateChange( state ) )
			{
				this.InvalidateMergedCellAllRegions( row, column );
				return;
			}

			if ( MergedCellStyle.OnlyWhenSorted == column.MergedCellStyleResolved )
			{
				this.MergedCell_RowStateChanged( row, state );
			}
			else
			{
				MergedCellCollection mergedCells = this.GetMergedCellCollection( column, false );
				if ( null != mergedCells )
					mergedCells.RemoveMergedCellFor( row, true );
			}
		}

		#endregion // MergedCell_CellStateChanged
	}

	#endregion // MergedCellCache Class

	#region MergedCell Class
	
	internal class MergedCell
	{
		#region State Enum

		internal enum State
		{
			Activation,
			Deleted,
			EnterEditMode,
			ExitEditMode,
			Expanded,
			Hidden,
			Invalidate,
			Inserted,
			Selection,
			Value
		}

		#endregion // State Enum

		#region Private Vars

		private MergedCellCollection mergedCellColl = null;
		private UltraGridRow startRow = null;
		private UltraGridRow endRow = null;
		private int verifiedCachedHeightVersion = -1;

		// See EnsureHeightMatrixCalculated method where this is initialized for more info.
		//
		private int[] heightMatrix = null;

		#endregion // Private Vars

		#region Constructor
		
		internal MergedCell( UltraGridRow startRow, UltraGridRow endRow, MergedCellCollection mergedCellColl )
		{
			if ( null == startRow || null == endRow || null == mergedCellColl )
				throw new ArgumentNullException( );

			if ( startRow.ParentCollection != endRow.ParentCollection )
				throw new ArgumentException( );

			this.startRow = startRow;
			this.endRow = endRow;
			this.mergedCellColl = mergedCellColl;
		}

		#endregion // Constructor

		#region Private/Internal Properties

		#region Column

		internal UltraGridColumn Column
		{
			get
			{
				return this.mergedCellColl.Column;
			}
		}

		#endregion // Column

		#region Band

		internal UltraGridBand Band
		{
			get
			{
				return this.StartRow.BandInternal;
			}
		}

		#endregion // Band

		#region StartRow

		internal UltraGridRow StartRow
		{
			get
			{
				return this.startRow;
			}
		}

		#endregion // StartRow

		#region ParentCollection

		internal MergedCellCollection ParentCollection
		{
			get
			{
				return this.mergedCellColl;
			}
		}

		#endregion // ParentCollection

		#region EndRow

		internal UltraGridRow EndRow
		{
			get
			{
				return this.endRow;
			}
		}

		#endregion // EndRow

		#region Rows

		internal RowsCollection Rows
		{
			get
			{
				return this.MergedCellCache.Rows;
			}
		}

		#endregion // Rows
        
		#region MergedCellCache

		private MergedCellCache MergedCellCache
		{
			get
			{
				return this.mergedCellColl.MergedCellCache;
			}
		}

		#endregion // MergedCellCache

		#region IsValid
		
		internal bool IsValid
		{
			get
			{
				int startRowIndex = this.StartRow.VisibleIndex;
				int endRowIndex = this.EndRow.VisibleIndex;

				Debug.Assert( startRowIndex >= 0 && startRowIndex < endRowIndex );
				return startRowIndex >= 0 && startRowIndex < endRowIndex;
			}
		}

		#endregion // IsValid

		#region CellCount
		
		private int CellCount
		{
			get
			{
				return this.IsValid ? 1 + this.EndRow.VisibleIndex - this.StartRow.VisibleIndex : -1;
			}
		}

		#endregion // CellCount

		#region Height
		
		internal int Height
		{
			get
			{
				this.EnsureHeightMatrixCalculated( );
				return null != this.heightMatrix 
					? this.heightMatrix[ this.heightMatrix.Length - 1 ] - this.heightMatrix[ 0 ]
					: -1;
			}
		}

		#endregion // Height

		#endregion // Private/Internal Properties

		#region Private/Internal Methods

		#region ContainsCell

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Returns true if this merged cell contains the specified actual cell.
		//        /// </summary>
		//        /// <param name="cell"></param>
		//        /// <returns></returns>
		//#endif
		//        internal bool ContainsCell( UltraGridCell cell )
		//        {
		//            return this.ContainsCell( cell.Row );
		//        }

		#endregion Not Used
		
		internal bool ContainsCell( UltraGridRow row )
		{
			int rowIndex = row.VisibleIndex;

			return rowIndex >= 0 
				&& rowIndex >= this.StartRow.VisibleIndex
				&& rowIndex <= this.EndRow.VisibleIndex;
		}

		#endregion // ContainsCell

		#region GetCells

		internal UltraGridCell[] GetCells( )
		{
			if ( this.IsValid )
			{
				int startIndex = this.StartRow.VisibleIndex;
				int endIndex = this.EndRow.VisibleIndex;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = new ArrayList( );
				List<UltraGridCell> list = new List<UltraGridCell>();

				for ( int i = startIndex; i <= endIndex; i++ )
				{
					UltraGridRow row = this.Rows.GetRowAtVisibleIndex( i );
					Debug.Assert( null != row );
					if ( null == row )
						return null;

					list.Add( row.Cells[ this.Column ] );
				}

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//UltraGridCell[] arr = (UltraGridCell[])list.ToArray( typeof( UltraGridCell ) );
				UltraGridCell[] arr = list.ToArray();

				// Only two or more cells can be merged.
				//
				Debug.Assert( arr.Length >= 2 );
				if ( arr.Length >= 2 )
					return arr;
			}

			return null;
		}

		#endregion // GetCells

		#region EnsureHeightMatrixCalculated

		private void EnsureHeightMatrixCalculated( )
		{
			if ( this.IsValid )
			{
				Debug.Assert( null == this.heightMatrix || 1 + this.CellCount == this.heightMatrix.Length );

				if ( null == this.heightMatrix || 1 + this.CellCount != this.heightMatrix.Length
					|| this.verifiedCachedHeightVersion != this.MergedCellCache.RowHeightVersion )
				{
					int startIndex = this.StartRow.VisibleIndex;
					int endIndex = this.EndRow.VisibleIndex;
					this.heightMatrix = new int[ 2 + endIndex - startIndex ];
					RowsCollection rows = this.Rows;
					UltraGridBand band = rows.Band;
					UIElementBorderStyle rowBorderStyle = band.BorderStyleRowResolved;
					bool rowBordersMerged = band.Layout.CanMergeAdjacentBorders( rowBorderStyle );
					bool usingRowLayout = band.UseRowLayoutResolved;
					int cellSpacing = band.CellSpacingResolved;

					int topInset = cellSpacing;
					if ( ! rowBordersMerged || cellSpacing > 0 )
						topInset += band.Layout.GetBorderThickness( rowBorderStyle );

					if ( usingRowLayout )
						topInset += this.Column.RowLayoutColumnInfo.CellInsets.Top;

					this.heightMatrix[0] = topInset;

					// Initialize the successive elements to the bottoms of the rows relative to the 
					// top of the first row.
					//
					// SSP 4/7/05 - BR03158
					// 
					UltraGridRow nextRow = rows.GetRowAtVisibleIndex( startIndex );

					for ( int i = startIndex, height = 0; i <= endIndex; i++ )
					{
						// SSP 4/7/05 - BR03158
						// Allow merging cells even if row spacing is set. Since we allow merging
						// when cell spacing is set there is no reason to not allow when there's
						// row spacing. The user can force cells to not merge by implementing
						// IMergeCellEvaluator interface. Get the next row as well so we can look 
						// at its row spacing before.
						//
						//UltraGridRow row = rows.GetRowAtVisibleIndex( i );
						UltraGridRow row = nextRow;
						nextRow = i < endIndex ? rows.GetRowAtVisibleIndex( 1 + i ) : null;

						Debug.Assert( null != row );
						if ( null == row )
						{
							this.heightMatrix = null;
							return;
						}

						// View style will reduce the heights of rows by 1 if their borders are
						// merged. Take that into account when calculating the heights of merged
						// cells. Also don't adjust the last row otherwise the height of the 
						// merged cell will end up being 1 less.
						//
						// SSP 4/7/05 - BR03158
						// Allow merging cells even if row spacing is set. Since we allow merging
						// when cell spacing is set there is no reason to not allow when there's
						// row spacing. The user can force cells to not merge by implementing
						// IMergeCellEvaluator interface.
						//
						// ------------------------------------------------------------------------
						//height += row.Height - ( rowBordersMerged && i < endIndex ? 1 : 0 );
						int currRowSpacingAfter = row.RowSpacingAfterResolved;
						int nextRowSpacingBefore = null != nextRow ? nextRow.RowSpacingBeforeResolved : 0;

						int mergedRowBordersSlack = rowBordersMerged && null != nextRow 
							&& currRowSpacingAfter <= 0 && nextRowSpacingBefore <= 0 ? 1 : 0;

						int rowSpacingSlack = null != nextRow 
							? currRowSpacingAfter + nextRowSpacingBefore : 0;

						height += row.Height - mergedRowBordersSlack + rowSpacingSlack;
						// ------------------------------------------------------------------------

						this.heightMatrix[ 1 + i - startIndex ] = height;
					}

					int bottomInset = cellSpacing;
					if ( usingRowLayout )
						bottomInset += this.Column.RowLayoutColumnInfo.CellInsets.Bottom;

					if ( ! rowBordersMerged || cellSpacing > 0 )
						bottomInset += band.Layout.GetBorderThickness( rowBorderStyle );

					this.heightMatrix[ this.heightMatrix.Length - 1 ] -= bottomInset;

					this.verifiedCachedHeightVersion = this.MergedCellCache.RowHeightVersion;
				}
			}
			else
				this.heightMatrix = null;
		}

		#endregion // EnsureHeightMatrixCalculated

		#region GetTopRelativeTo
		
		internal int GetTopRelativeTo( UltraGridRow row )
		{
			this.EnsureHeightMatrixCalculated( );
			if ( null != this.heightMatrix )
			{
				int startIndex = this.StartRow.VisibleIndex;
				int index = row.VisibleIndex;

				int i = index - startIndex;
				Debug.Assert( i >= 0 && i < this.heightMatrix.Length - 1 );
				if ( i >= 0 && i < this.heightMatrix.Length - 1 )
				{
					if ( 0 == i )
						return this.heightMatrix[ 0 ];
					else
						return this.heightMatrix[ 0 ] - this.heightMatrix[ i ];
				}
			}

			return int.MinValue;
		}

		#endregion // GetTopRelativeTo

		#region GetUIElement

		internal UIElement GetUIElement( RowScrollRegion rsr, ColScrollRegion csr )
		{
			if ( this.Rows.IsPotentiallyVisibleOnScreen )
			{
				UIElement elem = this.Rows.Layout.DataAreaElement;
				if ( null != elem )
					return elem.GetDescendant( typeof( MergedCellUIElement ), new object[] { this, rsr, csr } );
			}
			
			return null;
		}

		#endregion // GetUIElement

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region InvalidateItemAllRegions

		//#if DEBUG
		//        /// <summary>
		//        /// Invalidates the associated merged cell ui element. If a merged cell ui element was 
		//        /// found and dirtyDataAreaElem parameter is true then dirties the data area but does
		//        /// not invalidate it.
		//        /// </summary>
		//        /// <param name="dirtyDataAreaElem"></param>
		//#endif
		//        internal void InvalidateItemAllRegions( bool dirtyDataAreaElem )
		//        {
		//            if ( null != this.MergedCellCache )
		//                this.MergedCellCache.InvalidateItemAllRegions( this, dirtyDataAreaElem );
		//        }

		//        #endregion // InvalidateItemAllRegions

		#endregion Not Used

		#region ResolveAppearance

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal void ResolveAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		//{
		//    this.ResolveAppearance( ref appData, ref flags, CellState.None );
		//}

		#endregion Not Used

		internal void ResolveAppearance( ref AppearanceData appData, 
			ref AppearancePropFlags flags, 
			// SSP 9/14/05 BR06301 BR05562
			// Added forceState parameter and took out forceSelected.
			// 
			//bool forceSelected,
			CellState cellState )
		{
			// SSP 8/18/05 BR05562
			// Added forceActive parameter.
			// 
			//this.StartRow.ResolveMergedCellAppearance( this.Column, ref appData, ref flags, forceSelected );
			this.StartRow.ResolveMergedCellAppearance( this.Column, ref appData, ref flags, cellState );
		}

		#endregion // ResolveAppearance

		#endregion // Private/Internal Methods
	}

	#endregion // MergedCell Class
}
