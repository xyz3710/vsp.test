#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
    using System.Drawing;
	using System.Globalization;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
    using System.Windows.Forms;
    using System.Diagnostics;
    using Infragistics.Shared;
    using Infragistics.Shared.Serialization;
    using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Text;
	using System.Collections;
	using System.Security.Permissions;
	using System.Collections.Generic;
    using Infragistics.Win.Layout;

	internal class GroupCloneData 
	{
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//public PosChanged  posChanged;

		public UltraGridGroup	   clonedFromGroup;
		public int        width;
		public int        minWidth;
		public int        maxWidth;
		public int       visiblePosition;
	};

	internal class PositionInfo
	{
		public PositionInfo() { visiblePos = 0; level = 0; group = null; }
		public UltraGridGroup Group { get {return group;} set { group = value; } }
		public short   VisiblePos { get { return visiblePos; } set { visiblePos = value; } }
		public short   Level { get { return level; } set { level = value; } }

		private short   visiblePos;
		private short   level;
		private UltraGridGroup group;
	};



    /// <summary>
    /// UltraGrid's Column object.
    /// </summary>
	[ TypeConverter( typeof( UltraGridGroup.UltraGridGroupTypeConverter ) ) ]
	[ Serializable() ]
	public class UltraGridGroup : KeyedSubObjectBase,
									ISerializable,
        // MRS - NAS 9.1 - Groups in RowLayout        
        IProvideRowLayoutColumnInfo,
        ILayoutGroup
    {
        #region Private Members

        private int				index = 0;
        private int             width = 0;
        private Infragistics.Win.AppearanceHolder	
								cellAppearanceHolder = null;

        //private Infragistics.Win.UltraWinGrid.GroupColumnsCollection
		//						columns = null;

        private Infragistics.Win.UltraWinGrid.GroupHeader   
								header = null;

        private Infragistics.Win.UltraWinGrid.UltraGridBand	
								band = null;
        
        private bool			hidden = false;

		private const int		MIN_GROUP_WIDTH = 14;

		private GroupsCollection	groupsCollection = null;
		private GroupColumnsCollection	
								groupColumnsCollection = null;
		private int				widthDuringDrag = 0;
		private int				orderedColumnsVersion = 0;
		private GroupCloneData	cloneData = null;
		private bool	        hasBeforePosChanged = false;

		private bool			firstItem = false;
		private bool			lastItem = false;
		private int				internalID	= 0;

        // MRS - NAS 9.1 - Groups in RowLayout
        private RowLayoutColumnInfo rowLayoutGroupInfo = null;
        private int verifiedRowLayoutCacheVersion = -1;
        private bool inVerifyRowLayoutCache = false;
        private LayoutContainerCalcSize layoutContainerCalcSize = null;
        private GridBagLayoutManager gridBagLayoutManagerHelper = null;
        private GridBagLayoutItemDimensionsCollection layoutItemDimensions = null;
        private GridBagLayoutItemDimensionsCollection layoutItemDimensionsHeaderArea = null;
        private GridBagLayoutItemDimensionsCollection layoutItemDimensionsCellArea = null;
        internal GridBagLayoutManager gridBagRowLayoutManager = null;
        internal GridBagLayoutManager gridBagHeaderLayoutManager = null;
        private GridBagLayoutManager gridBagSummaryLayoutManager = null;
        private Size preferredRowLayoutSize = Size.Empty;
        private Size preferredHeaderLayoutSize = Size.Empty;
        private Size minimumRowLayoutSize = Size.Empty;
        private Size minimumHeaderLayoutSize = Size.Empty;
        private GroupLayoutItemCellContent groupLayoutItemCellContent = null;
        private GroupLayoutItemHeaderContent groupLayoutItemHeaderContent = null;
        private GroupLayoutItemSummaryContent groupLayoutItemSummaryContent = null;

        #endregion //Private Members

        /// <summary>
		/// Default constructor
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridGroup( )
		{
			this.Reset();
		}

		/// <summary>
		/// Constructor used during de-serialization
		/// </summary>
        /// <param name="id">ID</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridGroup( int id )
		{
			this.Reset();

			this.internalID = id;
		}

		/// <summary>
		/// Constructor used during de-serialization
		/// </summary>
        /// <param name="id">ID</param>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public UltraGridGroup( string key, int id ) : base ( key )
		{
			this.Reset();

			this.internalID = id;
		}

        internal UltraGridGroup( UltraGridBand band )
        {
            this.band = band;
			groupsCollection = this.band.Groups;
            this.Reset();

			// JJD 11/20/01
			// Initialize the internalId
			//
			if ( this.band != null &&
				 this.internalID == 0 )
			{
				this.internalID = this.band.Groups.GetUniqueID();
			}
        }
        
        internal UltraGridGroup( UltraGridBand band, String key ) : base( key )
        {
            this.band = band;
			groupsCollection = this.band.Groups;
            this.Reset();

			// JJD 11/20/01
			// Initialize the internalId
			//
			if ( this.band != null &&
				 this.internalID == 0 )
			{
				this.internalID = this.band.Groups.GetUniqueID();
			}
        }

		internal UltraGridGroup( UltraGridGroup group, bool byMouse )
		{
			this.band = group.Band;
			groupsCollection = this.band.Groups;

			cloneData = new GroupCloneData();

			// SSP 11/21/03 UWG2746
			// Copy the key as well as header properties.
			//
			// ----------------------------------------------------------------------
			this.Key = group.Key;
			if ( null != group.header )
			{
				this.header = group.header.Clone( );
                this.header.InitGroup( this );
			}			
			// ----------------------------------------------------------------------

			if( cloneData != null)
			{
				cloneData.clonedFromGroup = group;
				cloneData.width = group.Width;
				cloneData.visiblePosition = group.Header.VisiblePosition;
				group.GetGroupWidthRange(byMouse, true, ref cloneData.minWidth, ref cloneData.maxWidth);
			}

			if( group.groupColumnsCollection != null )
				this.groupColumnsCollection = group.groupColumnsCollection.Clone();
			else
				this.groupColumnsCollection = null;

            // MRS 2/13/2009 - TFS14007
            if (group.rowLayoutGroupInfo != null && group.ShouldSerializeRowLayoutGroupInfo())
                this.RowLayoutGroupInfo.InitializeFrom(group.RowLayoutGroupInfo);
		}

		[ Browsable(false) ] 
		[ EditorBrowsable(EditorBrowsableState.Never) ] 
		internal int InternalID
		{
			get
			{
				return this.internalID;
			}
		}
		

        /// <summary>
        /// Returns true is any of the properties have been
		/// set to non-default values
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerialize() 
        {
            return	this.ShouldSerializeCellAppearance() ||
					this.ShouldSerializeHidden() ||
					this.ShouldSerializeKey() ||
					this.ShouldSerializeTag() ||
					this.ShouldSerializeWidth();
        }
 
        /// <summary>
        /// Resets all properties back to their default values
        /// </summary>
        public void Reset() 
        {
            this.ResetHidden();
            this.ResetCellAppearance();

			// JJD 10/03/01 - UWG249
			// Call clear on the columns collection so that
			// each column's group reference will get nulled out
			//
			//this.groupColumnsCollection = null;
			if ( this.groupColumnsCollection != null )
				this.groupColumnsCollection.Clear();
        }
 
        /// <summary>
        /// Called when the object is disposed of
        /// </summary>
        protected override void OnDispose()
        {
            // unhook notifications and call dispose on the header object
            //
            if ( null != this.header )
            {
                this.header.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
                this.header.Dispose();
                this.header = null;
            }

			// It is importantant to unhook the holder and call reset on it
			// when we are disposed
			//
			if ( this.cellAppearanceHolder != null )
			{
				this.cellAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.cellAppearanceHolder.Reset();
				this.cellAppearanceHolder = null;
			}


            base.OnDispose();
        }
 
        /// <summary>
        /// The UltraGridBand that this column belongs to (read-only)
        /// </summary>
        [ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
 		[ Browsable(false) ]
        public Infragistics.Win.UltraWinGrid.UltraGridBand Band
        {
            get 
            {
                return this.band;
            }
        }

		/// <summary>
		/// Returns the key of the group
		/// </summary>
		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			// AS 1/8/03 - fxcop
			// Do not compare against string.empty - test the length instead
			//if (this.Key == null || this.Key == string.Empty)
			if (this.Key == null || this.Key.Length == 0)
			{
				// JJD 10/19/01
				// Since we had to support a public default construct to support 
				// the design time editor for collections, it is possible that
				// the band member is null. Therefore, check it before using it.
				//
				if ( this.band == null )
				{
					sb.Append( "Group" );
				}
				else
				{
					sb.Append( "Group#" );
					sb.Append( this.Index );
				}
			}
			else
			{
				sb.Append( this.Key );

			}

			if ( this.Header.Hidden )
				sb.Append( " [hidden]" );

			return sb.ToString();
		}

        /// <summary>
        /// Determines whether the object will be displayed. This property is not available at design-time.
        /// </summary>
		/// <remarks>
		/// <p class="body">The <b>Hidden</b> property determines whether an object is visible. Hiding an object may have have effects that go beyond simply removing it from view. For example, hiding a band also hides all the rows in that band. Also, changing the <b>Hidden</b> property of an object affects all instances of that object. For example, a hidden column or row is hidden in all scrolling regions.</p>
		/// <p class="body">There may be instances where the <b>Hidden</b> property cannot be changed. For example, you cannot hide the currently active rowscrollregion or colscrollregion. If you attempt to set the <b>Hidden</b> property of the active rowscrollregion to True, an error will occur.</p>
		/// </remarks>
        public bool Hidden
        {
            get 
            {
                return this.hidden;
            }
            set
            {
                if(this.hidden != value)
                {
                    this.hidden = value;

					// MD 1/19/09 - Groups in RowLayout
					// This is needed so the display will update when groups are hdiden or shown at run-time.
					if ( null != this.Band && this.Band.UseRowLayoutResolved )
						this.Band.DirtyRowLayoutCachedInfo();

					// SSP 7/18/03 - Fixed headers
					// Since hiding a column could potentially change the number of
					// fixed visible columns, bump the verify version number.
					//
					if ( null != this.Band )
						this.Band.BumpFixedHeadersVerifyVersion( );

                    this.NotifyPropChange( PropertyIds.Hidden, null );
                }
            }
        }
		/// <summary>
		///  Returns true if an bands object has been created.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeHidden() 
        {
            return this.hidden != false;
        }
 
		/// <summary>
		/// Resets Hidden to its default value (False.)
		/// </summary>
        public void ResetHidden() 
        {
            this.Hidden = false;
        }

		// MD 1/19/09 - Groups in RowLayout
		#region HiddenResolved

		/// <summary>
		/// Unlike the Hidden property, this property takes into account whether the group is a child of another hidden group. 
		/// </summary>
		[Browsable( false )]
		[EditorBrowsable( EditorBrowsableState.Advanced )]
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
		public bool HiddenResolved
		{
			get
			{
				if ( this.band == null )
					return true;

				if ( this.band.RowLayoutStyle == RowLayoutStyle.GroupLayout )
				{
					UltraGridGroup group = this.RowLayoutGroupInfo.ParentGroup;
					Debug.Assert( group != this, "A group cannot be nested within itself." );

					if ( group != null && group.HiddenResolved )
						return true;
				}

				return this.Hidden;
			}
		}

		#endregion //HiddenResolved
 
        /// <summary>
        /// The index of this group in the band's group collection
        /// </summary>
        [ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[ Browsable(false) ]
        [ MergableProperty( false ) ]
		public int Index
        {
            get
            {
                try
                {
                    // if the index hasn't changed for the last index
                    // get (the normal case then we can return it without
                    // having to call IndexOf below)
                    //
                    if ( this == this.band.Groups[this.index] )
                        return this.index;
                }
                catch(Exception)
                {
                }
            
                // cache the index before returning it so that
                // if the index doesn';t change (the normal case)
                // the test above will be more efficient
                //
                this.index = this.band.Groups.IndexOf(this);

                return this.index;
            }
        }
 
        /// <summary>
        /// Returns the Header object associated with the object. This property is read-only at run-time. This property is not available at design-time.
        /// </summary>
		/// <remarks>
		/// <p class="body">A Header object represents a column or group header that specifies information about the column or group, and can also serve as the interface for functionality such as moving, swapping or sorting the column or group. Group headers have the added functionality of serving to aggregate multiple columns under a single heading.</p> 
		/// <p class="body">The <b>Header</b> property provides access to the header that is associated with an object. The <b>Header</b> property provides access to the header that is associated with an object. In some instances, the type of header may be ambiguous, such as when accessing the <b>Header</b> property of a UIElement object. You can use the <b>Type</b> property of the Header object returned by the <b>Header</b> property to determine whether the header belongs to a column or a group.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
        public Infragistics.Win.UltraWinGrid.GroupHeader Header
        {
            get
            {
                if ( null == this.header )
                {
                    this.header = new GroupHeader( this );

                    // hook up the prop change notifications
                    //
                    this.header.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }

                return this.header;
            }
        }

		/// <summary>
		/// Returns true if the <see cref="Header"/> property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeHeader() 
		{
			return ( null != this.header && 
				this.header.ShouldSerialize() );
		}
 
		/// <summary>
		/// Resets the properties of the <see cref="Header"/>  to their default values.
		/// </summary>
		public void ResetHeader() 
		{
			if ( null != this.header )
				this.header.Reset();
		}


		/// <summary>  
		/// Determines the formatting attributes that will be applied to the cells in a group.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>CellAppearance</b> property is used to specify the appearance of all the cells in a row. When you assign an Appearance object to the <b>CellAppearance</b> property, the properties of that object will be applied to all the cells belonging to the row. You can use the <b>CellAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the cells, for example:</p> 
		/// <p class="code">UltraWinGrid1.Override.CellAppearance.BackColor = vbYellow</p>
		/// <p class="body">You can override the <b>CellAppearance</b> setting for specific cells by setting the <b>Appearance</b> property of the UltraGridCell object directly. The cell will always use the values of its own Appearance object before it will use the values inherited from the Appearance object specified by the <b>CellAppearance</b> property of the row or band it occupies.</p>
		/// <p class="body">If any of the properties of the Appearance object specified for the <b>CellAppearance</b> property are set to default values, the properties from the Appearance object of the row containing the cell are used.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		public Infragistics.Win.AppearanceBase CellAppearance
        {
            get 
            {
                // if we don't already have an appearance object then create it now
                //
                if ( null == this.cellAppearanceHolder )
                {
					// JJD 10/19/01
					// Since we had to support a public default construct to support 
					// the design time editor for collections, it is possible that
					// the band member is null. Therefore, check it before using it.
					//
					this.cellAppearanceHolder = new Infragistics.Win.AppearanceHolder();
                    
                    // hook up the prop change notifications
                    //
                    //this.cellAppearanceHolder.Appearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					// hook into holder's notifications instead of the appearanceHolder.Appearance's
					//
					this.cellAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;			
                }

				if ( this.Band != null &&
					 this.Band.Layout != null )
				{
					this.cellAppearanceHolder.Collection = this.Band.Layout.Appearances;
				}

                return this.cellAppearanceHolder.Appearance;
            }
            set
            {
				if (  this.cellAppearanceHolder == null  ||
					  !this.cellAppearanceHolder.HasAppearance || 
					  value != this.cellAppearanceHolder.Appearance )
                {
                    // remove the old prop change notifications
                    //
                    if ( null == this.cellAppearanceHolder )
					{
						this.cellAppearanceHolder = new AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.cellAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}

					// Initialize the collection
					//
					if ( this.Band != null &&
						 this.Band.Layout != null )
						this.cellAppearanceHolder.Collection = this.Band.Layout.Appearances;
                    
                    this.cellAppearanceHolder.Appearance = value;
            
                    
                    // notify listeners
                    //
                    this.NotifyPropChange( PropertyIds.CellAppearance );
                }
            }
        }

		/// <summary>
		///  Returns true if an bands object has been created.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeCellAppearance() 
        {
            return ( this.HasCellAppearance &&
                this.cellAppearanceHolder.ShouldSerialize() );
        }
 
		/// <summary>
		/// Resets CellAppearance to its default value.
		/// </summary>
        public void ResetCellAppearance() 
        {
			if ( this.HasCellAppearance )
			{
//				// remove the prop change notifications
//				//
//				this.cellAppearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
//                    
//				this.cellAppearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.cellAppearanceHolder.Appearance.Reset();

				// Call reset on the holder instead of the appearance
				//
				this.cellAppearanceHolder.Reset();

				// Notify listeners that the appearance has changed. 
				this.NotifyPropChange(PropertyIds.Appearance);
			}
        }

		/// <summary>
		/// Returns true if an CellAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCellAppearance
		{
			get
			{
				return ( null != this.cellAppearanceHolder &&
						 this.cellAppearanceHolder.HasAppearance );
			}
		}


        /// <summary>
        /// Returns or sets the width of an object in container units or pixels.
        /// </summary>
		/// <remarks>
		/// <p class="body">The <b>Width</b> property is used to determine the horizontal dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">When proportional resizing is used for the UltraGridColumn and UltraGridGroup objects, the width of the column increases or decreases proportionally as the area occupied by the column changes size, due to the resizing of adjacent columns or of the grid itself.</p>
		/// </remarks>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
        public int Width
        {
            get
            {
				return this.GetWidth();
            }
            set
            {
                if ( this.width != value)
                {
					this.SetWidth(value, false, true, true, true);
                    this.NotifyPropChange( PropertyIds.Width, null );
                }
            }
        }

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeWidth() 
        {
            return this.width > 0;
        }

        /// <summary>
        /// Property: gets group's extent
        /// </summary>
		[ Browsable(false) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
        public int Extent
        {
            get
            {
                return this.Width;
            }
        }

		// JJD 1/21/02 - UWG526 - Added
		/// <summary>
		/// Get the next/previous/first or last group in this band based on each group header's <see cref="Infragistics.Win.UltraWinGrid.GroupHeader.VisiblePosition"/>.
		/// </summary>
		/// <param name="relation">The relation the requested group has to this group.</param>
		/// <returns>The related group.</returns>
		public UltraGridGroup GetRelatedVisibleGroup( VisibleRelation relation )
		{
			if ( this.band == null )
				return null;

			HeadersCollection headers = this.band.OrderedGroupHeaders;

			if ( headers != null && headers.Count > 0 )
			{
				switch ( relation )
				{
					case VisibleRelation.First:
						return headers[0].Group;

					case VisibleRelation.Last:
						return headers[headers.Count - 1].Group;

					case VisibleRelation.Next:
					{
						if ( headers.Contains(  this.Header ) )
						{
							int index = headers.IndexOf( this.Header );

							if ( index >= 0 && index < headers.Count - 1)
								return headers[index + 1].Group;
						}
						break;
					}

					case VisibleRelation.Previous:
					{
						if ( headers.Contains(  this.Header ) )
						{
							int index = headers.IndexOf( this.Header );

							if ( index > 0 )
								return headers[index - 1].Group;
						}
						break;
					}
				}
			}

			return null;
		}

		internal bool FirstItem
		{
			get
			{
				return this.firstItem;
			}
			set
			{
				this.SetFirstItem( value );
			}
		}
		internal bool LastItem
		{
			get
			{
				return this.lastItem;
			}
			set
			{
				SetLastItem( value );
			}
		}

		internal bool HasBeforePosChanged
		{
			get
			{
				return this.hasBeforePosChanged;
			}
			set
			{
				this.hasBeforePosChanged = value;
			}
		}

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
        protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
        {
            if ( propChange.Source == this.Header )
            {
                if ( null != propChange.FindPropId( PropertyIds.CellAppearance ))
                    this.Band.ClearCachedHeaderFontHeights();

                this.NotifyPropChange( PropertyIds.Header, propChange );
                return;                
            }

            else if ( propChange.Source == this.groupColumnsCollection )	//this.columns )
            {
                this.NotifyPropChange( PropertyIds.Columns, propChange );
                // If one of the columns is hidden/shown then call
                // OnColumnsChanged so that firstitem/lastitem flags
                // will get reset on the other columns in the group
                //
				if ( null != propChange.FindPropId( PropertyIds.Hidden ) )
				{
					
					// SSP 11/9/01 UWG687
					// When a column is made visible we need to adjust the width of the
					// group to take into account previously hidden column becoming
					// visible.
					//
					this.groupColumnsCollection.AdjustGroupWidth(false, false, false);

					this.OnColumnsChanged();
				}
					// SSP 11/9/01 UWG687
					// When a column is ungrouped-by, Hidden property off that
					// column will return true, so we need to adjust the width of the
					// group to take into account previously hidden column becoming
					// visible.
					// Added else if clause.
				else if ( null != propChange.FindPropId( PropertyIds.IsGroupByColumn ) )
				{
					this.groupColumnsCollection.AdjustGroupWidth(false, false, false);
					this.OnColumnsChanged( );
				}
				else if ( null != propChange.FindPropId( PropertyIds.Level  ) )
					this.OnColumnsChanged();
				else if ( null != propChange.FindPropId( PropertyIds.VisiblePosition ) )
					this.OnColumnsChanged();
				else if ( null != propChange.FindPropId( PropertyIds.Width ) )
				{	
					this.groupColumnsCollection.AdjustGroupWidth(false, false, false);
					this.OnColumnsChanged();
				}
                return;
            }

            else if ( this.HasCellAppearance &&
					  propChange.Source == this.cellAppearanceHolder.RootAppearance )
            {
				// JJD 12/13/01
				// Clear the font cache for the band as well as all the columns
				// in this group
				//
                this.Band.ClearCachedHeaderFontHeights();

				if ( this.groupColumnsCollection != null )
				{
					for ( int i = 0; i < this.groupColumnsCollection.Count; i++ )
						this.groupColumnsCollection[i].ClearFontCache();
				}

                this.NotifyPropChange( PropertyIds.CellAppearance, propChange );
                return;
            }
            // MRS - NAS 9.1 - Groups in RowLayout
            //
            else if (this.HasRowLayoutGroupInfo && propChange.Source == this.RowLayoutGroupInfo)
            {
                if (null != this.Band)
                    this.Band.DirtyRowLayoutCachedInfo();

                this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.RowLayoutColumnInfo, propChange);
                return;
            }

            Debug.Assert( false, "Unknown sub object in Group.OnSubObjectPropChanged" );
        }

		internal void OnColumnsChanged()
        {
            // Dirty the sync list and the metrics
            //
            this.band.Layout.SynchronizedColListsDirty = false;
            this.band.Layout.GetColScrollRegions ( false ).DirtyMetrics();
        }

        internal void ResolveAppearance( ref AppearanceData appData,
							             ref ResolveAppearanceContext context )
        {
			
			// JJD 8/7/01 - UWG121
			// Only apply the header appearance if the type is header
			//
			if ( context.ObjectType == typeof(Infragistics.Win.UltraWinGrid.HeaderBase) )
			{
				// we are resolving for the header so merge in its appearance
				//
				// SSP 3/13/06 - App Styling & Optimization
				// Check UseControlInfo and also don't force allocation of appearance object.
				// 
				//if (this.Header != null && this.Header.Appearance != null )
				if ( this.Header != null && this.Header.HasAppearance && context.ResolutionOrder.UseControlInfo )
				{
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//AppearanceData.MergeAppearance( ref appData, 
					//								this.Header.Appearance.Data,
					//								ref context.UnresolvedProps );
					this.Header.Appearance.MergeData( ref appData, ref context.UnresolvedProps );
				}
			}

            else if ( this.HasCellAppearance )
            {
				// SSP 3/13/06 - App Styling
				// Enclosed the existing code into the if block.
				// 
				if ( context.ResolutionOrder.UseControlInfo )
				{
					// For rows and cells merge in our appearance
					//
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//AppearanceData.MergeAppearance( ref appData, 
					//								this.cellAppearanceHolder.Appearance.Data,
					//								ref context.UnresolvedProps );
					this.cellAppearanceHolder.Appearance.MergeData( ref appData, ref context.UnresolvedProps );
				}
            }

            // if we have all the appearance properties requested we can
            // exit here
            //
            if ( context.UnresolvedProps == 0)
            {
                return;
            }

            // we don't want to go up to the band since its okay to have unresolved 
            // properties at this point. Unless of course if this is a column header
            // resolution. In which case, we do want to get the band's input
            //
            if (context.ObjectType == typeof(Infragistics.Win.UltraWinGrid.HeaderBase))
            {
                this.Band.ResolveAppearance( ref appData, ref context );
                // finally call ResolveColors to make sure we have valid
                // fore, back and borders colors
                //
                this.Band.Layout.ResolveColors( ref appData, ref context,
                    SystemColors.Control, SystemColors.WindowFrame,
                    SystemColors.WindowText, false );

				return;

            }

			// JJD 12/13/01
			// When we are resolving cell's and the IsCellOnly flag is set
			// we want to call the band's resolve. 
			// This is called to resolve a cell's appearance in card view
			//
			if ( context.ObjectType == typeof(UltraGridCell) &&
			 	 context.UnresolvedProps != 0 && 
				 context.IsCellOnly )
			{
				this.Band.ResolveAppearance( ref appData, ref context);
			}

        }


		
		/// <summary>
		/// Returns the index of the group in the groups collection
		/// </summary>
        /// <returns>The index of the group in the groups collection.</returns>
		public int GetIndex()
		{
			Debug.Assert ( groupsCollection != null, "Group without a collection, no good.");
			return groupsCollection != null ?	groupsCollection.IndexOf(this) : -1;
		}

		internal bool CanInitializeFrom( UltraGridGroup source )
		{
			// if the keys match return true
			//
			// AS 1/8/03 - fxcop
			// Must supply an explicit CultureInfo to the String.Compare method.
			//if ( String.Compare(this.Key, source.Key, true) == 0 )
			if ( String.Compare(this.Key, source.Key, true, System.Globalization.CultureInfo.CurrentCulture) == 0 )
				return true;

			return false;
		}

		


		internal Activation ActivationResolved
		{
			get
			{
				// Only return disabled if the header is marked disabled
				//
				return ( this.Header != null && this.Header.Enabled )
					? Activation.ActivateOnly : Activation.Disabled;
			}
		}

		
		/// <summary>
		/// Returns the column at the passed in visible position
		/// </summary>
		/// <param name="position">The visible position.</param>
        /// <returns>The column at the passed in visible position.</returns>
		public UltraGridColumn GetColumn( short position )
		{
			if ( groupColumnsCollection == null )
				return null;

			return groupColumnsCollection[position];
		}


		internal void SetColumnPosition( UltraGridColumn column, short position )
		{
			// make sure the band matches.
			//
			if ( column.Band != this.band )
				throw new System.ArgumentException(Shared.SR.GetString("LE_System.ArgumentException_134"));

			UltraGridGroup oldGroup = column.Group;

			// if the group is already set to us for this column then call 
			// SetColumnPosition and return
			//
			if ( this != oldGroup )
				throw new System.ArgumentException(Shared.SR.GetString("LE_System.ArgumentException_135"));

			oldGroup.RemoveColumn( column );

			// then add it to our GroupCols collection
			//
			groupColumnsCollection.InternalInsert( position, column );
		}

		/// <summary>
		/// Returns the index of the column in the groupcols collection
		/// </summary>
		/// <param name="column">Associated <b>Column</b></param>
        /// <returns>The index of the column in the groupcols collection.</returns>
		public int GetIndex( UltraGridColumn column )
		{ 
			int position = -1;

			if ( groupColumnsCollection != null )
				position =	(short)groupColumnsCollection.IndexOf(column);

			return position;
		}


		/// <summary>
		/// Removes a column from our groupcols collection
		/// </summary>
		/// <param name="column"></param>
		public void RemoveColumn( UltraGridColumn column )
		{
			
			Debug.Assert( this.band == null || this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout, "Columns should never be removed from a group in GroupLayout style." );

			// make sure the band matches.
			//
			if ( column.Band != this.band )
				throw new System.ArgumentException(Shared.SR.GetString("LE_System.ArgumentException_136"));

			UltraGridGroup oldGroup = column.Group;

			//if group from column is null, then it's already removed, so return
			//
			if( oldGroup == null )
				return;

			// makes sure Column is part of this group
			//
			if ( this != oldGroup )
				throw new System.ArgumentException(Shared.SR.GetString("LE_System.ArgumentException_137"));

			groupColumnsCollection.Remove(column);

			// decrease group width by removed column width
			//
			this.groupColumnsCollection.AdjustGroupWidth(false, false, false);
		}

		internal void AllocateGroupColumnsCollection( )
		{
			if ( groupColumnsCollection == null )
			{
				groupColumnsCollection = new GroupColumnsCollection(this);

				if ( groupColumnsCollection != null )
					groupColumnsCollection.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}
		}

		
		/// <summary>
		/// Property: gets Group's GroupColumnsCollection [readonly]  
		/// </summary>
		//[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]   
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public GroupColumnsCollection Columns
		{
			get
			{	
				if ( groupColumnsCollection == null )
				{
					AllocateGroupColumnsCollection( );
				}
				// RobA UWG464 10/12/01
				// JJD 10/19/01
				// Make sure th band is not null (which can happen
				// during serialization
				//
				else if ( this.Band != null &&
						  this.Band.ColumnsVersion != this.orderedColumnsVersion )
				{
                    for (int i = 0; i < this.groupColumnsCollection.Count; ++i)
                    {
                        // MRS 7/7/2009 - TFS18608
                        // Doesn't make sense to check for the key here. If a column exists in the 
                        // Group and it's not the same instance as the column in the band's Columns
                        // collection, it should not be here, even if it has the same key. 
                        //
                        //if (!this.Band.Columns.Exists(this.groupColumnsCollection[i].Key) )
                        //
                        if (!this.Band.Columns.Contains(this.groupColumnsCollection[i]))
                        {
                            this.groupColumnsCollection.RemoveAt(i);
                            --i;
                        }
                    }

					this.orderedColumnsVersion  = this.Band.ColumnsVersion;
				}

				return groupColumnsCollection;
			}
		}
        
        /// <summary>
		/// Returns true if the group can be resized
		/// </summary>
		/// <returns></returns>
		[ Browsable(false) ]
		internal bool AllowSizing
		{
			get
			{

				int minWidth=0;
				int maxWidth=0;

				if( !this.Header.Enabled )
					return false;

				// If we don't have any columns then the group can not be resized
				//
				if ( groupColumnsCollection == null || groupColumnsCollection.Count < 1 )
					return false;

				this.GetResizeRange(ref minWidth, ref maxWidth );

				return ( minWidth != maxWidth );
			}
		}

		
		/// <summary>
		/// Returns the min and max width of the group.
		/// </summary>
		/// <param name="minWidth">The minimum width of the group.</param>
        /// <param name="maxWidth">The maximum width of the group.</param>        
		public void GetResizeRange( ref int minWidth, ref int maxWidth )
		{
			minWidth=0;
			maxWidth=0;
			GetGroupWidthRange( true, true, ref minWidth, ref maxWidth );
		}

    
		internal void GetColumnWidthRange( bool      byMouse,
										  bool      adjustGroupWidthAlso, 
										  UltraGridColumn	column, 
										ref int 	minWidth, 
										ref int	maxWidth )
		{
			if ( groupColumnsCollection != null )
				groupColumnsCollection.GetColumnWidthRange( byMouse, adjustGroupWidthAlso, 
							column, ref minWidth, ref maxWidth );
		}


		// Forward this to the GroupCols collection
		//
		void GetGroupWidthRange( bool byMouse, bool groupResize, ref int minWidth, ref int maxWidth )
		{
			if ( groupColumnsCollection != null )
			{
				int width = 0;
				groupColumnsCollection.GetGroupWidthRange( byMouse, groupResize, ref minWidth, ref maxWidth, ref width );
			}
		}


		// Forward this to the GroupCols collection
		//
		void AdjustGroupWidth( bool byMouse, bool groupResize, bool duringDrag )
		{
			if ( groupColumnsCollection != null )
				groupColumnsCollection.AdjustGroupWidth( byMouse, groupResize, duringDrag );
		}

		// SSP 12/10/02 UWG1768
		// This internal resolved property was added because we should not proportionally
		// resize any groups with ProportionalResize set to false on all of its columns.
		//
		/// <summary>
		/// Indicates whether this group should be proportionally reiszed when autofitting columns.
		/// </summary>
		internal bool ProportionalResizeResolved
		{
			get
			{
				// SSP 12/8/03 UWG2769
				// When the user resizes a column in auto-fit columns mode, we do not want to apply
				// the extra delta width to the column that's being resized in Band.FitColumnsToWidth
				// method. Added this internal flag which is temporarily set to accomplish this.
				//
				// ----------------------------------------------------------------------------
				if ( null != this.header && DefaultableBoolean.Default != this.header.proportionalResizeOverride )
					return DefaultableBoolean.True == this.header.proportionalResizeOverride;
				// ----------------------------------------------------------------------------

				if ( this.Band.Layout.AutoFitAllColumns )
				{
					// If any of the columns has proportional resize set to true, then return true
					// since the whole group qualifies for proportional resize as well.
					//
					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
					// actually visible in the group
					//for ( int i = 0; i < this.Columns.Count; i++ )
					//{
					//    UltraGridColumn column = this.Columns[i];
					foreach ( UltraGridColumn column in this.ColumnsResolved )
					{
						// MD 1/21/09 - Groups in RowLayouts
						// Use HiddenResolved instead of Hidden because it will determine if the column is actually visible.
						//if ( column.Hidden )
						if ( column.HiddenResolved )
							continue;

						if ( column.ProportionalResize )
							return true;
					}

					// If all of the columns have this set to false, then return true when auto fitting of 
					// columns is on.
					//
					if ( null != this.Band )
					{
						for ( int i = 0; i < this.Band.Columns.Count; i++ )
						{
							UltraGridColumn column = this.Band.Columns[i];

							// MD 1/21/09 - Groups in RowLayouts
							// Columns which are not in groups are not necessarily hidden anymore. Use HiddenResolved instead.
							//// Skip non-visible columns. When we have a group in a band, all the columns
							//// with no groups are not visible since they don't belong to any groups.
							////
							//if ( column.Hidden || this == column.Group || null == column.Group || column.Group.Hidden )
							if ( column.HiddenResolved )
								continue;
						
							if ( column.ProportionalResize )
								return false;
						}
					}
					
					return true;
				}

				return false;
			}
		}

		// Forward this to the GroupCols collection
		//
		internal void AdjustAllColumnWidths( bool	byMouse, 
											bool    groupResize,
											bool    duringDrag,
											bool    fireGroupResizeEvent, 
											bool    columnAdded,
											UltraGridColumn	column )
		{
			if ( groupColumnsCollection != null )
				groupColumnsCollection.AdjustAllColumnWidths( byMouse, groupResize, duringDrag, fireGroupResizeEvent, columnAdded, column );
		}

		internal UltraGridColumn GetColumnAtOffset( UltraGridRow row, 
								int offsetX, 
								int offsetY,
								bool daggingRight,
								bool daggingDown )
		{
			
			Debug.Assert( this.band == null || this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout, "Column levels should not be used in GroupLayout style" );

			if ( groupColumnsCollection ==null || groupColumnsCollection.Count < 1 )
				return null;

			short level      = 0;
			short levelCount =	(short)this.band.LevelCount;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//GroupsCollection groupsCollection = this.band.Groups;

			// either use the row or the band to get the height
			//
			int height         = row != null ? row.BaseHeight : band.RowHeightResolved;
			int heightPerLevel = height / levelCount;

			offsetX = Math.Max( offsetX, 0 );
			offsetX = Math.Min( offsetX, this.GetActualWidth( false ) );

			offsetY = Math.Max( offsetY, 1 );
		    
			offsetY = Math.Min( offsetY, height );

			// calculate the level based on the y offset and the height per level
			//
			if ( levelCount > 0 && offsetY > heightPerLevel )
				level  = (short)Math.Min( levelCount - 1, offsetY / heightPerLevel );

			UltraGridColumn column, firstColumn, lastColumn;
			column = firstColumn = lastColumn = null;

			for(int index = 0; index < groupColumnsCollection.Count; index++)
			{
				column = groupColumnsCollection[index];

                // MRS 2/23/2009 - Found while fixing TFS14354
				//if ( column.Hidden )
                if (column.HiddenResolved)
					continue;

				if ( firstColumn == null )
					firstColumn = column;

				lastColumn = column;

				if ( column.Level != level )
					continue;

				if ( column.RelativeOrigin <= offsetX )
				{	
					if ( column.RelativeOrigin + column.OverallWidth >= offsetX )
						return column;
				}
			}

			if ( daggingRight )
				return lastColumn;

			return null;
		}

		internal int GetActualWidth ( bool ignoreDraggingWidth  )
		{
			if ( this.widthDuringDrag > 0 && !ignoreDraggingWidth )
				return this.widthDuringDrag;

			return this.width;
		}


		//
		//
		void SetWidthEx ( int newWidth )
		{
			SetWidth ( newWidth, true, true, true, true );
		}

		// Returns the width of the group in pixels
		//
		int GetWidth()
		{
			// If the width is 0 (no visible columns) then return a minimum width
			// so the group will be displayed 
			//
			// JJD 10/22/01
			// Make sure the band is not null before proceeding
			//
			if ( this.width == 0 && this.band != null )
			{
                // MRS - NAS 9.1 - Groups in RowLayout
                #region Re-factored
                //System.Drawing.Size sizeOfCaption = new Size(0,0);

                //// Added logic to calculate the width based on the caption
                ////
                //if ( this.Header != null )
                //{
                //    string strCaption  = this.Header.Caption;
                //    int     len        = strCaption.Length;

                //    if ( len > 0 )
                //    {
                //        // Otherwise, we need to calculate a reasonable width for this column
                //        //
                //        AppearanceData appData = new AppearanceData();

                //        this.Header.ResolveAppearance( ref appData, AppearancePropFlags.FontData );

                //        // JJD 12/17/01
                //        // Moved font height logic into CalculateFontHeight
                //        //
                //        sizeOfCaption = this.band.Layout.CalculateFontSize( ref appData, strCaption );
                //    }
                //}

                //// Allow for swap btn width
                ////
                //if ( this.AllowSwapping )
                //    sizeOfCaption.Width += 10;

                //return sizeOfCaption.Width + MIN_GROUP_WIDTH;
                #endregion //Re-factored
                //
                return this.CalculatePreferredGroupHeaderSize().Width;
            }

			return this.width;
		}

		// Internal method to set the column's width
		//
		internal void SetWidth( int newWidth, 
								bool byMouse, 
								bool fireEvents, 
								bool fAdjustAllColsInGroup,
								bool fNotify )	//true
		{

			int minWidth  = 0;
			int maxWidth  = 0;

			this.GetGroupWidthRange( byMouse, true, ref minWidth, ref maxWidth );

			// adjust the width to fit within the min/max range
			//
			newWidth = Math.Max( newWidth, minWidth );

			if ( maxWidth > minWidth  )
				newWidth = Math.Min( newWidth, maxWidth );

			// make sure override width is set to zero since we can't be in 
			// an override situation when they are setting the actual width
			//
			this.widthDuringDrag = 0;

			// if the width hasn't changed then just return
			//
			if ( newWidth == this.width )
				return;

			UltraGridBase grid = this.band.Layout.Grid;

			if ( fireEvents && grid != null )
			{
				// allocate a groups collection object to pass into
				// the Before and AfterGroupPosChanged events
				//
				GroupsCollection grpsCol = new GroupsCollection(this.band);
				UltraGridGroup clone	= null;

				// make a cloned copy of this column
				//
				clone =	this.Clone(PosChanged.Sized, byMouse);

				// set the cloned copies width to the new width
				//
				clone.cloneData.width = newWidth;

				// add to the selected cols collection
				//
				grpsCol.AddClonedGroup( clone.Key );		        
				//grpsCol.Add( clone.Key );		        

				// set a flag so that we know we are in the event
				//
				this.hasBeforePosChanged = true;
		        
				//fire the BeforeColPosChanged event
				//
				BeforeGroupPosChangedEventArgs posChgEvent = 
					new BeforeGroupPosChangedEventArgs(PosChanged.Sized, DragStrategy.ToGroupHeaderArray( grpsCol.All ) );
				
				grid.FireBeforeGroupPosChanged(posChgEvent);

				// reset the flag
				//
				this.hasBeforePosChanged = false;

				// if it was canceled clean up and exit
				//
				if ( posChgEvent.Cancel )
				{
					// we just passivate the clone here since the selected cols
					// colllection will release its reference on the clone
					//
					clone.OnDispose();
					return;
				}

				// update the columns state with the new width
				//
				this.width = (int)clone.cloneData.width;

				if ( fAdjustAllColsInGroup )
					this.AdjustAllColumnWidths( true, true, false, false, false, null );

				// Moved notification before firing after event so that the uielements will
				// be dirtied and will return the new values if asked for in the event
				//
				// notify any interested parties
				//
				if ( fNotify )
					this.NotifyPropChange( PropertyIds.Width, null );

				// Clear the collection of cloned groups and add this
				// group into the collection so that the after event 
				// gets the actual group passed in instead of the clone
				//
				grpsCol.ClearClonedGroups();

				//grpsCol.Add(this.Key);
				grpsCol.AddClonedGroup( this.Key );

				// fire the after event and clean up
				//
				AfterGroupPosChangedEventArgs AfterGrpPosChgedEvtArgs = 
						new AfterGroupPosChangedEventArgs(PosChanged.Sized, 
					// SSP 11/4/04 UWG3104
					// When we fire AfterGroupPosChanged event below we need to pass in the actual group
					// and not the clone as indicated by the comment above.
					//
					//DragStrategy.ToGroupHeaderArray( grpsCol.All ) );
					DragStrategy.ToGroupHeaderArray( new UltraGridGroup[] { this } ) );
				
				grid.FireAfterGroupPosChanged(AfterGrpPosChgedEvtArgs);

				clone.OnDispose();

				grpsCol.ClearClonedGroups();

				// SSP 11/5/04 UWG3104
				// We are supposed to return here because we have already done above all of the 
				// things we do after this if block and not only that it causes the problem where
				// if the user sets the Width of the group in AfterGroupPosChanged event above
				// then we will end up resetting the width below. Column's SetWidth returns here
				// as well.
				//
				return;
			}

			// we aren't firing events so just set the newvalue in the state structure 
			//
			this.width = (int)newWidth;

			if ( fAdjustAllColsInGroup )
				this.AdjustAllColumnWidths( true, true, false, false, false, null );

			// notify any interested parties
			//
			if ( fNotify )
				this.NotifyPropChange( PropertyIds.Width, null );
		}


		internal void SetWidthDuringDrag( int newWidth )
		{
			this.widthDuringDrag = newWidth;

			// if the width during drag is being reset to 0 loop thru all of the 
			// group's columns and reset their dragging widths as well
			//
			if ( this.widthDuringDrag == 0 )
			{
				if ( groupColumnsCollection != null )
				{
					// iterate over columns collection to reset their widths as well
					//
					for( int index = 0; index < groupColumnsCollection.Count; index++ )
					{
						groupColumnsCollection[index].SetWidthDuringDrag(0);
					}
				}
			}
		}


		// Creates and returns a clone of the column object
		//
		internal UltraGridGroup Clone ( PosChanged posChanged, bool byMouse )
		{
			UltraGridGroup clone = new UltraGridGroup(this, byMouse );

			if ( clone == null )
				return null;

			
			if ( clone.cloneData == null ||
				( groupColumnsCollection != null && clone.groupColumnsCollection == null ) )
			{
				clone = null;
				return null;
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//clone.cloneData.posChanged = posChanged;
		    
			return clone;
		}

		internal UltraGridGroup GetClonedFromGroup( ) 
		{ 
			return null != this.cloneData ? this.cloneData.clonedFromGroup : null; 
		}

        // MRS 2/23/2009 - Found while fixing TFS14354
        #region Removed method that is never called
        //// Returns the default caption for this group
        ////
        //string GetDefaultCaption()
        //{
        //    return this.Key;
        //}

        //// Returns the actual caption to use. Gets it from the header 
        //// object if we have one, otherwise, it returns the key.
        ////
        //string GetCaption()
        //{
        //    return ( this.Header != null )
        //        ? this.Header.Caption
        //        : this.Key;
        //}

		
        //// DESCRIPTION: Called when the Replace method is called on an appearance object.
        ////
        //// TODO: need to implement this Notifier to get this to work
        ////*
        //bool CGroup::OnReplaced( const CUIChangeNotifier& Notifier, 
        //                         CUIChangeNotifier*       pReplacedWith, 
        //                         void *                   pContext )
        //{
        //    bool bReplaced = false;

        //    if ( m_AppearanceHolder.GetAppearance() == &Notifier )
        //    {
        //        bReplaced = m_AppearanceHolder.ReplaceWidth( (CAppearance*) pReplacedWith );
        //        NotifyDataChanged( (void*)DISPID_CELLAPPEARANCE );
        //    }
        //    else
        //        FAIL("Unknown notifier passed into CGroup::OnReplaced");

        //    return !bReplaced;
        //}
        //*/
	

        //// Delegates responsibility for selection to CBand::ClickGroup, by passing itself as a parameter.
        ////
        //void Click()
        //{
        //    Debug.Assert(this.band.Layout.Grid != null, "No Grid in Group.Click()!" );

        //    try
        //    {
        //        //TODO: implement GroupClickAction
        //        //this.band.GroupClickAction();
        //    }
        //    catch(Exception)
        //    {
        //        Debug.Assert(false, "Group.Click()");
        //    }
        //}
        
        //// Sets the internal slected state on or off and notifies any listeners of the state change 
        ////
        //void Select( bool bSelect )
        //{
        //    if ( bSelect != this.IsSelected() )
        //    {
        //        //Don't allow empty groups to be selected
        //        if ( bSelect && this.IsEmptyGroup() )
        //            return;

        //        this.NotifyPropChange( PropertyIds.Selected, null );
        //    }

        //    if ( groupColumnsCollection != null )
        //    {
        //        for(int index = 0; index < groupColumnsCollection.Count; index++)
        //        {
        //            if ( groupColumnsCollection[index].Hidden || groupColumnsCollection[index].Header.IsDisabled )
        //                continue;

        //            // If any column is not selected then update our
        //            // state variable and return false 
        //            //
        //            groupColumnsCollection[index].Header.Selected = bSelect;
        //        }
        //    }
        //}

        //// Returns true only if all columns are marked as selected
        ////
        //bool IsSelected()
        //{
        //    // Empty groups are considered not selected
        //    // MRS 2/23/2009 - Found while fixing TFS14354
        //    //if( this.Hidden || /*this.Disabled ||*/ this.IsEmptyGroup())
        //    if (this.HiddenResolved || /*this.Disabled ||*/ this.IsEmptyGroup())
        //        return false;

        //    if ( groupColumnsCollection != null )
        //    {
        //        for(int index = 0; index < groupColumnsCollection.Count; index++)
        //        {
        //            if ( groupColumnsCollection[index].Hidden || groupColumnsCollection[index].Header.IsDisabled )
        //                continue;

        //            // If any column is not selected then update our state variable and return false 
        //            //
        //            if(!groupColumnsCollection[index].Header.Selected)
        //                return false;
        //        }
        //    }
		    
        //    return true;
        //}        

        //// Returns false if the group has any visible, enabled columns, true otherwise.
        ////
        //bool IsEmptyGroup()
        //{
        //    if ( groupColumnsCollection != null )
        //    {
	
        //        if ( groupColumnsCollection.Count > 0)
        //        {
        //            // iterate over columns collection
        //            //
        //            for(int index = 0; index < groupColumnsCollection.Count; index++)
        //            {
        //                if ( groupColumnsCollection[index].Hidden || groupColumnsCollection[index].Header.IsDisabled )
        //                    continue;

        //                // if we get to a visible, enabled column, the group is not empty
        //                return false;
        //            }
        //        }
        //    }

        //    // if we didn't find a visible, enabled column, the group is empty
        //    return true;
        //}
        #endregion //Removed method that is never called

		internal Infragistics.Win.UltraWinGrid.UltraGridBase Grid
		{
			get
			{
				return this.band.Layout.Grid;
			}
		}


		// Returns whether or not drag mode is automatic.
		//
		bool IsDragAuto()
		{
			//RobA 10/23/01 implemented
			return ( this.band.AllowGroupMovingResolved != AllowGroupMoving.NotAllowed );
			
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		///// <summary>
		///// Activate the group.
		///// </summary>
		///// <returns></returns>
		//internal bool Activate()
		//{
		//    return false;
		//}

		#endregion Not Used


		// Sets the first item flag for the first visible column in every level.
		//
		void SetFirstItem( bool val )
		{
			this.firstItem = val;

			bool  firstItemInLevel = true;
			bool  firstItem = true;
			int   overallWidth;
			short lastLevel = -1;

			// MD 1/21/09 - Groups in RowLayouts
			// The group's columns collection is not valid in GroupLayout style.
			//if ( groupColumnsCollection != null )
			if ( groupColumnsCollection != null && this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
			{
				for(int index = 0; index < groupColumnsCollection.Count; index++)
				{
					// If the column is hidden it can't be the first item
					//
					if ( groupColumnsCollection[index].Hidden )
					{
						groupColumnsCollection[index].FirstItem = false;
						continue;
					}

					short level = (short)groupColumnsCollection[index].Level;

					// When the level changes reset first item flag to true
					//
					if ( level != lastLevel )
					{
						firstItem = true;
						firstItemInLevel = true;
						lastLevel = level;
					}

					// Added support for new First item in level flags
					//
					if ( firstItemInLevel )
					{
						overallWidth = groupColumnsCollection[index].OverallWidth;

						if ( overallWidth > 0 )
						{
							groupColumnsCollection[index].FirstItemInLevel = true;
							firstItemInLevel = false;
						}
						else
							groupColumnsCollection[index].FirstItemInLevel = false;
					}
					else
						groupColumnsCollection[index].FirstItemInLevel = false;


					if ( val && firstItem )
					{
						// Only set first item to true when the overall width is > 0. 
						// This correctly skips over hidden and chaptered columns.
						//
						overallWidth = groupColumnsCollection[index].OverallWidth;

						if ( overallWidth > 0 )
						{
							groupColumnsCollection[index].FirstItem = true;
							firstItem = false;
						}
						else
							groupColumnsCollection[index].FirstItem = false;
					}
					else
						groupColumnsCollection[index].FirstItem = false;
				}
			}
		}


		// Sets the last item flag for the last visible column in every level.
		//
		void SetLastItem( bool val )
		{
			this.lastItem = val;

			bool  lastItem = true;
			bool  lastItemInLevel = true;
			int  overallWidth = 0;
			short lastLevel = -1;

			// MD 1/21/09 - Groups in RowLayouts
			// The group's columns collection is not valid in GroupLayout style.
			//if ( groupColumnsCollection != null )
			if ( groupColumnsCollection != null && this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout )
			{
				for(int index = groupColumnsCollection.Count-1; index >= 0; index--)
				{
					short level = (short)groupColumnsCollection[index].Level;

					// When the level changes reset last item flag to true
					//
					if ( level != lastLevel )
					{
						lastItem = true;
						lastItemInLevel = true;
						lastLevel = level;
					}

					// Added support for new last item in level flags
					//
					if ( lastItemInLevel )
					{
						overallWidth = groupColumnsCollection[index].OverallWidth;

						if ( overallWidth > 0 )
						{
							groupColumnsCollection[index].LastItemInLevel = true;
							lastItemInLevel = false;
						}
						else
							groupColumnsCollection[index].LastItemInLevel = false;
					}
					else
						groupColumnsCollection[index].LastItemInLevel = false;

					if ( val && lastItem )
					{
						// Only set Last item to true when the overall width is > 0. 
						// This correctly skips over hidden and chaptered columns.
						//
						if ( overallWidth > 0 )
						{
							groupColumnsCollection[index].LastItem = true;
							lastItem = false;
						}
						else
							groupColumnsCollection[index].LastItem = false;
					}
					else
						groupColumnsCollection[index].LastItem = false;
				}
			}
		}


		internal void ReCalculateColumnOrigins()
		{
			// MD 1/21/09 - Groups in RowLayouts
			// Levels do not apply in GroupLayout mode.
			if ( this.band != null && this.band.RowLayoutStyle == RowLayoutStyle.GroupLayout )
				return;

			// Check to make sure we have a columns coll before proceeding
			// 
			if ( groupColumnsCollection == null )
				return;

			// Update the column position item relative origins
			//
			short lastLevel = -1;
			int pos = 0;

			for(int index = 0; index < groupColumnsCollection.Count; index++)
			{
				if ( groupColumnsCollection[index] != null )
				{
					short level =	(short)groupColumnsCollection[index].Level;

					// When the level changes, reset pos to zero
					//
					if ( level != lastLevel )
					{
						pos = 0;
						lastLevel = level;
					}

					// JJD 10/03/01 -UWG467
					// set the origon off the header
					//
					//groupColumnsCollection[index].RelativeOrigin = pos;
					groupColumnsCollection[index].Header.SetOrigin( pos );

					pos += groupColumnsCollection[index].GetActualWidth(false);

					if ( groupColumnsCollection[index].FirstItem )
						// SSP 10/20/03 UWG2707 UWG2617
						// Although above bugs are not caused by the following line of code, I noticed it 
						// when I was fixing above two bugs. Original code is wrong. We only need to add 
						// the RowSelectorExtent since the origin is relative to the group header which
						// only spans left to include row selector and not the pre row area extent.
						//
						//pos += groupColumnsCollection[index].Band.PreRowAreaExtent + groupColumnsCollection[index].Band.RowSelectorExtent;
						pos += groupColumnsCollection[index].Band.RowSelectorExtent;
				}
			}
		}

		
		/// <summary>
		/// Returns true if swap drop down button appears in the header
		/// </summary>
		[ Browsable(false) ]
		internal bool AllowSwapping
		{
			get
			{
				AllowGroupSwapping allowGroupSwapping = this.Band.AllowGroupSwappingResolved;

				Debug.Assert( allowGroupSwapping != AllowGroupSwapping.Default,
										"AllowGroupSwapping set to Default in Group.AllowSwapping");
				
				return ( allowGroupSwapping != AllowGroupSwapping.NotAllowed );
			}
		}

		// Calculates the rect for the group's header
		//
		internal void CalculateHeaderRect ( ref System.Drawing.Rectangle headerRect, ref System.Drawing.Rectangle fixedHeaderRect )	// NULL
		{
			headerRect.Location = new System.Drawing.Point(0,0);

			// use the overall width instead of the actual width
			//
			headerRect.Width = (int)this.OverallWidth;

			headerRect.Height = this.band.GroupHeaderHeight;

			// If the fixed header height is greater than the band's header height offset the 
			// rect downward so that it is displayed butted up against the row
			//
			this.AlignHeaderInFixedHeaderArea ( ref headerRect, ref fixedHeaderRect );			
		}


		internal bool SetOrderedCols( OrderedDropColumns orderedDropCols )
		{
			
			Debug.Assert( this.band == null || this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout, "The group's columns should never be set in GroupLayout style." );

			if ( null == this.groupColumnsCollection || null == orderedDropCols )
				return false;

			GroupColumnsCollection groupsColsCollClone = this.groupColumnsCollection.Clone( );

			if ( null != groupsColsCollClone )
			{
				this.groupColumnsCollection.Clear( );

				int columnIterator;
				int newPosIterator;				
				int pos = 0;
        			

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//System.Collections.ArrayList newOrderedColHeaders = orderedDropCols.GetHeaderList( );
				List<HeaderBase> newOrderedColHeaders = orderedDropCols.GetHeaderList();

				if ( null == newOrderedColHeaders )
					return false;

				for ( newPosIterator = 0; newPosIterator < newOrderedColHeaders.Count; newPosIterator++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//HeaderBase header = newOrderedColHeaders[ newPosIterator ] as HeaderBase;
					HeaderBase header = newOrderedColHeaders[ newPosIterator ];

					Debug.Assert( null != header, "All elements of newOrderedColHeaders must be HeaderBase instances" );

					Debug.Assert( !header.IsGroup, "A Group in the OrderedCols List....aaahhh!" );
					
					Debug.Assert( !( header is BandHeader ), "A Band in the OrderedCols List....aaahhh!" );

					Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader = (ColumnHeader)header;

					if ( null != columnHeader )
					{
						PositionInfo insertPosInfo = new PositionInfo( );

						insertPosInfo = orderedDropCols.GetPositionInfo( columnHeader );

						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						UltraGridColumn columnHeaderColumn = columnHeader.Column;

						// If this column is from a different group
						if ( columnHeader.Group != this )
						{
							// We shouldn't set the first itme flag here based on the
							// visible position since that column may be hidden.
							// The DISPID_VISIBLEPOSITION notification below will
							// trigger a recalc of the band metrics which will reset
							// the first item flags
							//
							//  bool fFirstItem = nPos == 0 ? true : false;
							//   
							// Reset the FirstItem Flag
							// pColumn->SetFirstItem(fFirstItem);
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//// Remove From Old Group
							//columnHeader.Column.Group = null; // ->put_Group(vEmpty);
							//
							////pColumn->AddRef();
							//// Set the New Level
							//columnHeader.Column.InternalSetLevel( insertPosInfo.Level, false );
							//// Insert into Group's Columns Collection
							//this.groupColumnsCollection.InternalAdd( columnHeader.Column /*, true */ );
							//// Set the New Group
							//columnHeader.Column.InternalSetGroup( this );
							// Remove From Old Group
							columnHeaderColumn.Group = null; // ->put_Group(vEmpty);

							//pColumn->AddRef();
							// Set the New Level
							columnHeaderColumn.InternalSetLevel( insertPosInfo.Level, false );
							// Insert into Group's Columns Collection
							this.groupColumnsCollection.InternalAdd( columnHeaderColumn  );
							// Set the New Group
							columnHeaderColumn.InternalSetGroup( this );

							// Inc the VisiblePos
							pos++;
						}
						else
						{

							// Find the Column in the existing Collection
							for ( columnIterator = 0; columnIterator < groupsColsCollClone.Count; columnIterator++ )
							{


								// MD 8/3/07 - 7.3 Performance
								// Prevent calling expensive getters multiple times	
								//if ( columnHeader.Column == groupsColsCollClone[columnIterator] )
								if ( columnHeaderColumn == groupsColsCollClone[ columnIterator ] )
								{
									// We shouldn't set the first itme flag here based on the
									// visible position since that column may be hidden.
									// The DISPID_VISIBLEPOSITION notification below will
									// trigger a recalc of the band metrics which will reset
									// the first item flags
									//
									// bool fFirstItem = nPos == 0 ? true : false;
									//
									// Reset the FirstItem Flag
									// pColumn->SetFirstItem(fFirstItem);
									// MD 8/3/07 - 7.3 Performance
									// Prevent calling expensive getters multiple times
									//// Set the New Level
									//columnHeader.Column.InternalSetLevel( insertPosInfo.Level, false );
									//// Insert into Group's Columns Collection
									//this.groupColumnsCollection.InternalInsert( pos, columnHeader.Column /*, true*/ );
									// Set the New Level
									columnHeaderColumn.InternalSetLevel( insertPosInfo.Level, false );
									// Insert into Group's Columns Collection
									this.groupColumnsCollection.InternalInsert( pos, columnHeaderColumn  );

									// Inc the VisiblePos
									pos++;
									// Our work here is done.
									break;
								}

							}
						}
					}
				}

				if ( null != newOrderedColHeaders )
				{
					newOrderedColHeaders.Clear( );
					newOrderedColHeaders = null;
				}

				this.groupColumnsCollection.AdjustGroupWidth( false, false, false );

				// Call notify data changed off all of the columns since all of their
				// positions may have changed
				//
				for ( int i = 0; i < this.groupColumnsCollection.Count; i++ )
				{
					this.groupColumnsCollection[i].NotifyPropChange( PropertyIds.VisiblePosition );
				}
        
				//groupsColsCollClone->Release();
				//pGroupsColsCollClone = NULL;
			}
    
			return true;
		}


		/// <summary>
		/// Swaps the location of this group with the specified swapGroup.
		/// </summary>
		/// <param name="swapGroup"></param>
		// SSP 9/19/05 BR06292
		// Made public.
		// 
		//internal void Swap( UltraGridGroup swapGroup )
		public void Swap( UltraGridGroup swapGroup )
		{
			// SSP 11/2/01
			// Modified to make the group swapping work
			//


			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_138"));
		    
			if ( this.band != swapGroup.band )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_139"));

			// Allow swapping at design time
			if( swapGroup != null )
			{
				// allocate a Groups collection object to pass into
				// the Before and AfterGroupPosChanged events
				UltraGridGroup[] swappedGroups = new UltraGridGroup[2];
				    				    
				// make a cloned copy of the swap columns
				UltraGridGroup clonedGroup = this.Clone(PosChanged.Swapped, false);
				UltraGridGroup cloneSwap = swapGroup.Clone(PosChanged.Swapped, false);
				    
				if ( clonedGroup == null || cloneSwap == null )
				{
					return;
				}
				        
				if ( clonedGroup.cloneData == null || cloneSwap.cloneData  == null )
				{
					return;
				}

				// get the visible positions for the columns to be swapped
				//
				int nThisVisiblePos = this.Header.VisiblePosition;
				int nSwapVisiblePos = swapGroup.Header.VisiblePosition;
			        
				// SSP 7/25/03 - Fixed headers
				// When a fixed header is swapped with a non-fixed header and vice versa,
				// make sure that we changed their fixed states.
				//
				// ------------------------------------------------------------------------------
				bool fixedStatesChanged = false;

				ColScrollRegion activeCSR = null != this.Band && null != this.Band.Layout
					? this.Band.Layout.ActiveColScrollRegion : null;
				bool thisHeaderFixed = false;
				bool swapHeaderFixed = false;

				if ( null != activeCSR && this.Band.AreFixedHeadersAllowed( activeCSR ) )
				{
					thisHeaderFixed = this.Header.FixedResolved;
					swapHeaderFixed = swapGroup.Header.FixedResolved;
 
					if ( thisHeaderFixed != swapHeaderFixed )
					{
						clonedGroup.Header.InternalSetFixed( swapHeaderFixed );
						cloneSwap.Header.InternalSetFixed( thisHeaderFixed );
						
						fixedStatesChanged = true;
					}
				}
				// ------------------------------------------------------------------------------

				// set the cloned copies visible pos if the swap operation succeeds
				//
				clonedGroup.cloneData.visiblePosition = nSwapVisiblePos;
				cloneSwap.cloneData.visiblePosition = nThisVisiblePos;

				// add the groups to be swapped into an array
				//
				swappedGroups[0] = clonedGroup;
				swappedGroups[1] = cloneSwap;

                // MRS - NAS 9.1 - Groups in RowLayout
                // In row-layout mode swap the row-layout column infos.
                // 
                if (this.Band.UseRowLayoutResolved)
                {
                    clonedGroup.RowLayoutGroupInfo.InitializeFrom(cloneSwap.RowLayoutGroupInfo);
                    cloneSwap.RowLayoutGroupInfo.InitializeFrom(this.RowLayoutGroupInfo);

                    // MRS 3/6/2009 - TFS14910
                    clonedGroup.RowLayoutGroupInfo.LabelSpan = this.RowLayoutGroupInfo.LabelSpan;
                    cloneSwap.RowLayoutGroupInfo.LabelSpan = swapGroup.RowLayoutGroupInfo.LabelSpan;
                }
    
				// set a flag so that we know we are in the event
				//
				this.hasBeforePosChanged = true;
				swapGroup.HasBeforePosChanged = true;

				//fire the BeforeGroupPosChanged event
				//
				BeforeGroupPosChangedEventArgs posChgEvent = 
					new BeforeGroupPosChangedEventArgs(PosChanged.Swapped,
					     DragStrategy.ToGroupHeaderArray( swappedGroups ) );
				
				grid.FireBeforeGroupPosChanged(posChgEvent);

				// reset the flag
				this.hasBeforePosChanged = false;
				swapGroup.HasBeforePosChanged = false;
			        
				// if it was canceled clean up and exit
				if (posChgEvent.Cancel)
				{
					// we just passivate the clone here since the selected cols
					// colllection will release its reference on the clone

					// SSP 11/2/01
					// We shouldn't call OnDispose on cloned groups since cloned
					// groups are shallow copies
					//
					//clonedGroup.OnDispose();
					//cloneSwap.OnDispose();
					return;
				}

                // MRS - NAS 9.1 - Groups in RowLayout
                // In row-layout mode swap the row-layout column infos.
                // 
                if (this.Band.UseRowLayoutResolved)
                {
                    this.RowLayoutGroupInfo.InitializeFrom(clonedGroup.RowLayoutGroupInfo);
                    swapGroup.RowLayoutGroupInfo.InitializeFrom(cloneSwap.RowLayoutGroupInfo);
                }

				// If the swapping operation wasn't cancelled then perform the actual swap. 

				// SSP 7/24/03 - Fixed headers
				// If we are swapping a fixed and non-fixed header, then exchange their fixed states.
				// Added the if block and enclosed the already existing code into the else block.
				//
				if ( fixedStatesChanged )
				{
					this.Header.InternalSetFixed( swapHeaderFixed );
					swapGroup.Header.InternalSetFixed( thisHeaderFixed );

					this.Header.InternalSetVisiblePosValue( nSwapVisiblePos );
					swapGroup.Header.InternalSetVisiblePosValue( nThisVisiblePos );

					this.Band.BumpFixedHeadersVerifyVersion( true );

					swapGroup.Header.NotifyFixedChanged( );
					this.Header.NotifyFixedChanged( );
				}
				else
				{
					// JJD 10/15/01
					// Call the headers collection Swap method to actually swap the groups
					//
					//swapGroup.InternalSetVisiblePosition( nThisVisiblePos );
					//this.InternalSetVisiblePosition( nSwapVisiblePos );
					this.Band.OrderedGroupHeaders.Swap( this.Header, swapGroup.Header );                    
				}

				// SSP 11/2/01
				//--------------------------------------------------------------------
				this.Band.Layout.SynchronizedColListsDirty = true;
				this.Band.Layout.DirtyGridElement( false );

				if ( this.Band.Layout.GetColScrollRegions( false ) != null )
					this.Band.Layout.GetColScrollRegions( false ).DirtyMetrics();
				//--------------------------------------------------------------------


				// Make sure that the after event gets the actual group passed in instead of the clone
				//
				swappedGroups[0] = this;
				swappedGroups[1] = swapGroup;
			     
				// fire the after event and clean up
				AfterGroupPosChangedEventArgs afterPosChg = 
					new AfterGroupPosChangedEventArgs( PosChanged.Swapped, 
					DragStrategy.ToGroupHeaderArray( swappedGroups ) );
				
				grid.FireAfterGroupPosChanged( afterPosChg );
			    
				// SSP 11/2/01
				// We shouldn't call OnDispose on cloned groups since cloned
				// groups are shallow copies
				//					
				//clonedGroup.OnDispose();
				//cloneSwap.OnDispose();
			}
		}


		/// <summary>
		/// Sets the swapDrown up with the list of groups that can be swapped with this
		/// group.
		/// </summary>
		/// <param name="swapDropDown"></param>
		internal void CreateSwapList( ValueList swapDropDown )
		{
			GroupsCollection groups = this.Band.Groups;

			if ( null != groups )
			{
				// SSP 7/25/03 - Fixed headers
				// If the FixedHeaderIndicator type is InSwapDropDown, then add Fix Header or Unfix Header
				// item to the swap drop down list depending on whether the header is currently not fixed
				// or fixed.
				//
				// --------------------------------------------------------------------------------------
				ColScrollRegion activeCSR = null != this.Band && null != this.Band.Layout 
					? this.Band.Layout.ActiveColScrollRegion : null;
				this.Band.AddSwapDropdownFixUnfixItem( swapDropDown, this.Header );
				// --------------------------------------------------------------------------------------
       
				// loop thru Groups collection looking for a match
				//
				for ( int i = 0; i < groups.Count; i++ )
				{
					UltraGridGroup group = groups[i];

                    // MRS 2/23/2009 - TFS14354
					//if ( this != group && !group.Hidden )					
                    if (this != group && 
                        !group.HiddenResolved)
					{
						// SSP 7/25/03 - Fixed headers
						// Skip the headers that can't be swapped because doing so would cause the Fixed
						// state of a header to change in violation of FixedHeaderIndicator settings.
						//
						// ------------------------------------------------------------------------------
						if ( null != activeCSR && this.Band.AreFixedHeadersAllowed( activeCSR ) )
						{
							if ( this.Band.IsHeaderFixed( activeCSR, this.Header ) != this.Band.IsHeaderFixed( activeCSR, group.Header ) )
							{
								if ( FixedHeaderIndicator.None == this.Header.FixedHeaderIndicatorResolved
									|| FixedHeaderIndicator.None == group.Header.FixedHeaderIndicatorResolved )
									continue;
							}
						}
						// ------------------------------------------------------------------------------

                        // MRS 2/13/2009 - TFS14007
                        // Skip any group that is a parent or child of this group.
                        if (this.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout)
                        {
                            if (UltraGridGroup.IsDescendantOfGroup(this, group))
                                continue;

                            if (UltraGridGroup.IsDescendantOfGroup(group, this))
                                continue;

                            // Only allow swapping groups that are the same Span size. 
                            // Wehn columns are swapped, the Spans are copied from one column 
                            // to the other. But since the group spans are calculated by
                            // their contents, swapping groups of different span sizes can result
                            // in overlapping layout items. 
                            // We could fix this by using EnsureItemDoesntOverlap, but this
                            // will wait for a feature request. 
                            // If we implement this, update the /// comments on Override.AllowGroupSwapping
                            //
                            if (group.RowLayoutGroupInfo.SpanXResolved != this.RowLayoutGroupInfo.SpanXResolved ||
                                group.RowLayoutGroupInfo.SpanYResolved != this.RowLayoutGroupInfo.SpanYResolved)
                                continue;
                        }

						swapDropDown.ValueListItems.Add( group, group.Header.Caption );
					}
				}
			}
		}
		

		
		internal bool IsClone
		{
			get
			{
				return ( null != this.cloneData );
			}
		}

		internal int InternalGetVisiblePosition()
		{
			// if we are a cloned group then get the value out
			// of the cloned data structure
			//
			if ( this.IsClone )
			{
				return this.cloneData.visiblePosition;
			}

			if ( this.Band.OrderedGroupHeaders.Count > 0 )
				return this.Band.OrderedGroupHeaders.IndexOf( this.Header );

			return -1;
		}

		//
		//
		internal bool SetPositionInfo(PositionInfo PosInfo)
		{
			if( this.cloneData == null )
				return false;

			cloneData.visiblePosition = PosInfo.VisiblePos;

			return true; 
		}

		//
		//
		bool BeforeGroupPosMovedHelper( GroupsCollection MovedGroups,  short newVisPos )
		{
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_140"));
    		
			if ( MovedGroups == null)
				throw new System.ArgumentException(Shared.SR.GetString("LE_System.ArgumentException_141"));

			Infragistics.Win.UltraWinGrid.GridEventManager eventManager = grid.EventManager;

			if (eventManager != null )
			{
				MovedGroups = new GroupsCollection(this.band);
		    
				// make a cloned copy of the moved group
				UltraGridGroup groupClone = this.Clone(PosChanged.Moved, false);

				if (groupClone == null || groupClone.cloneData != null )
				{
					MovedGroups = null;
						throw new System.Exception(Shared.SR.GetString("LE_System.Exception_142"));
				}

				PositionInfo NewPosInfo = new PositionInfo();
				NewPosInfo.VisiblePos =	newVisPos;

				// set the cloned copies visible pos if the move operation succeeds
				//
				groupClone.SetPositionInfo(NewPosInfo);
		        
				// Add to the selected cols collection
				//
				MovedGroups.Add(groupClone.Key);

				// set a flag so that we know we are in the event
				this.hasBeforePosChanged = true;

				//fire the BeforeGroupPosChanged event
				//
				BeforeGroupPosChangedEventArgs posChgEvtArgs 
					= new BeforeGroupPosChangedEventArgs(PosChanged.Moved, DragStrategy.ToGroupHeaderArray( MovedGroups.All ) );
				
				this.band.Layout.Grid.FireBeforeGroupPosChanged( posChgEvtArgs );
		        
				// reset the flag
				this.hasBeforePosChanged = false;      

				if ( posChgEvtArgs.Cancel )
				{
					MovedGroups = null;            
				}

				return posChgEvtArgs.Cancel;
			}

			return true;
		}

		//
		//
		void AfterGroupPosMovedHelper(GroupsCollection MovedGroups )
		{
			if( this.band.Layout.Grid != null )
			{
				// fire the after event and clean up
				AfterGroupPosChangedEventArgs afterGrpPosChgEvntArgs = 
					new AfterGroupPosChangedEventArgs(PosChanged.Moved, DragStrategy.ToGroupHeaderArray( MovedGroups.All ) );
				
				this.band.Layout.Grid.FireAfterGroupPosChanged(afterGrpPosChgEvntArgs);
			}
		}


        // MRS 2/23/2009 - Found while fixing TFS14354
        #region Removed method that is never called
        //// Returns true if any column levels were adjusted
        //// 
        //bool ValidateColumnLevels()
        //{
        //    // TODO: Remove when Groups in RowLayouts is implemented.
        //    Debug.Assert( this.band == null || this.band.RowLayoutStyle != RowLayoutStyle.GroupLayout, "Column levels should not be used in GroupLayout style" );

        //    if ( this.groupColumnsCollection == null )
        //        return false;

        //    bool  columLevelsAdjusted  = false;
        //    short levelCount           = (short)this.band.LevelCount;	


        //    for( int index = 0; index < this.groupColumnsCollection.Count; index++ )
        //    {
        //        if ( this.groupColumnsCollection[index].Level >= levelCount )
        //        {
        //            this.groupColumnsCollection[index].InternalAdjustLevel( (short)(levelCount - 1) );
        //            columLevelsAdjusted = true;
        //        }
        //    }

        //    if ( columLevelsAdjusted )
        //    {
        //        this.SetFirstItem( this.FirstItem );
        //        this.SetLastItem( this.LastItem );

        //        this.AdjustGroupWidth( false, false, false );
        //    }

        //    return columLevelsAdjusted;
        //}
        #endregion // Removed method that is never called


        // Shifts the header rect down so that headers in the fixed header
		// area always but the top row even if bands have different header heights
		//
		void AlignHeaderInFixedHeaderArea (	ref System.Drawing.Rectangle headerRect, 
											ref System.Drawing.Rectangle fixedHeaders )
		{
			// If the fixed header height is greater than the band's header height offset 
			// the rect downward so that it is displayed butted up against the row
			//
			if ( fixedHeaders.Height > this.Band.GetTotalHeaderHeight() )
			{
				headerRect.Offset( 0, fixedHeaders.Height - this.Band.Header.Height );
			}
		}

		internal int OverallWidth
		{
			get
			{
				int overallWidth = this.GetActualWidth( false );
	    
				if ( this.FirstItem )
				{
					overallWidth += this.band.RowSelectorExtent;
				}

				return overallWidth;
			}
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			//set the assembly name because of version number conflicts
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			PropertyCategories propCat = context.Context is PropertyCategories
				? (PropertyCategories)context.Context	: PropertyCategories.All;

			if( ( propCat & PropertyCategories.Groups ) == 0 )
				return;

			//all values that were set are now save into SerializationInfo
			if( this.ShouldSerializeCellAppearance() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "CellAppearanceHolder", this.cellAppearanceHolder );
				//info.AddValue("CellAppearanceHolder", this.cellAppearanceHolder );
			}

			if( ShouldSerializeWidth() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Width", this.width );
				//info.AddValue("Width", this.width );
			}

			if( ShouldSerializeHidden() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Hidden", this.hidden );
                //info.AddValue("Hidden", this.hidden );
			}

			if ( this.ShouldSerializeKey() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Key", this.Key );
				//info.AddValue("Key", this.Key );
			}

			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			//save the groupcolumnscollection
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info,"Columns", this.Columns  );
			//info.AddValue("Columns", this.Columns );

			// JJD 10/19/01
			// Serialize the group's header if necessary
			//
			if ( this.Header.ShouldSerialize() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Header", this.Header );
				//info.AddValue("Header", this.Header );
			}

            // MRS - NAS 9.1 - Groups in RowLayout
            if (this.ShouldSerializeRowLayoutGroupInfo())
            {
                Utils.SerializeProperty(info, "RowLayoutGroupInfo", this.rowLayoutGroupInfo);
            }
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal UltraGridGroup( SerializationInfo info, StreamingContext context )
		protected UltraGridGroup( SerializationInfo info, StreamingContext context )
		{
			PropertyCategories propCat = context.Context is PropertyCategories
				? (PropertyCategories)context.Context	: PropertyCategories.All;

			if( ( propCat & PropertyCategories.Groups ) == 0 )
				return;

			this.Reset();

			//this will have to be set in SetLayout()
			this.band	= null;

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "CellAppearanceHolder":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.cellAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.cellAppearanceHolder = (Infragistics.Win.AppearanceHolder)entry.Value;
						break;

					case "Width":
						//this.width = (int)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.width = (int)Utils.DeserializeProperty( entry, typeof(int), this.width );
						//this.width = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
						break;

					case "Hidden":
						//this.hidden = (bool)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.hidden = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.hidden );
						//this.hidden = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
						break;

					case"Key":
						//this. = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Key = (string)Utils.DeserializeProperty( entry, typeof(string), this.Key );
						//this.Key = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;

					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;

					case "Columns":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.groupColumnsCollection = (GroupColumnsCollection)Utils.DeserializeProperty( entry, typeof(GroupColumnsCollection), null );
						//this.groupColumnsCollection = (GroupColumnsCollection)entry.Value;
						break;

					// JJD 10/19/01
					// De-serialize the group's header
					//
					case "Header":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.header = (GroupHeader)Utils.DeserializeProperty( entry, typeof(GroupHeader), null );
						//this.header = (GroupHeader)entry.Value;
						break;
					}

                    // MRS - NAS 9.1 - Groups in RowLayout
                    case "RowLayoutGroupInfo":
                    {
                        this.rowLayoutGroupInfo = (RowLayoutColumnInfo)Utils.DeserializeProperty(entry, typeof(RowLayoutColumnInfo), null);
                        break;
                    }

					default:
					{
						Debug.Assert( false, "Invalid entry in Group de-serialization ctor" );
						break;
					}				
				}
			}
		}

		
		private void InitGroupColumnsOnDeserialization( )
		{

			if ( this.band == null )
			{
				Debug.Fail("Band not set in InitGroupColumnsOnDeserialization" );
				return;
			}

			int totalColumns = 0;
			int maxIndex = 0;

			// first count the number of columns in this group
			//
			if ( this.band.Columns != null )
			{
				foreach ( UltraGridColumn column in this.band.Columns )
				{
					if ( column.GroupId == this.internalID )
					{
						totalColumns++;

						maxIndex = Math.Max( maxIndex, column.IndexInGroup );
					}
				}
			}

			if ( totalColumns < 1 )
				return;
			
			// the max index should be exactly one less than the total
			// number of columns
			//
			if ( maxIndex != totalColumns - 1 )
			{
				Debug.Fail("Max group column index out of range" );
				return;
			}

			UltraGridColumn [] slots = new UltraGridColumn[ totalColumns ];

			// Slot in the columns in the temporary array 
			//
			foreach ( UltraGridColumn column in this.band.Columns )
			{
				if ( column.GroupId == this.internalID )
				{
					if ( slots[ column.IndexInGroup ] != null )
					{
						Debug.Fail("Duplicate group column index found. Index: " + column.IndexInGroup.ToString() );
						return;
					}

					slots[ column.IndexInGroup ] = column;
				}
			}

			if ( groupColumnsCollection == null )
				groupColumnsCollection = new GroupColumnsCollection(this);

			this.groupColumnsCollection.InitDuringDeserialization( slots );

		}		
		


		internal void InitBand(UltraGridBand band, bool calledFromEndInit )
		{
			this.band = band;

			if ( calledFromEndInit )
			{
				this.InitGroupColumnsOnDeserialization();                
			}
			else
			{
				// JJD 10/19/01
				// init the header's band also
				//
				if ( this.header != null )
				{
					// JJD 10/23/01
					// Init the group (instead of just the band)
					//
					this.header.InitGroup( this );
				}

				// JJD 10/24/01
				// Init the cellappearance with the appearances collection
				// if necessary
				//
				if ( this.band != null &&
					this.cellAppearanceHolder != null )
					this.cellAppearanceHolder.Collection = this.band.Layout.Appearances;

				// JJD 11/20/01
				// Initialize the internalId
				//
				if ( this.band != null &&
					this.internalID == 0 )
				{
					this.internalID = this.band.Groups.GetUniqueID();
				}

				if ( this.groupColumnsCollection != null )
				{
					this.groupColumnsCollection.InitBand(band, this );
				}
			}

		}

		// Initializes the state of this object from a source object of the same type
		//
		internal void InitializeFrom( UltraGridGroup source, PropertyCategories propCat )
		{
			if ( ( propCat & PropertyCategories.Groups ) == 0 )
				return;

			//copy state
			this.Key = source.Key;

			// SSP 7/9/02 UWG1153
			// We want to copy the state variables and not what the property
			// returns (because Width property will return the
			// actual width calculated based upon the font and other factors.
			// Here we are just interested in carrying over the user settings)
			// 
			//this.width = source.Width;
			//this.hidden = source.Hidden;
			this.width = source.width;
			this.hidden = source.hidden;

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			if ( this.internalID == 0 )
				this.internalID = source.InternalID;

			if ( ( propCat & PropertyCategories.AppearanceCollection ) != 0 && source.HasCellAppearance )
			{	//this.cellAppearanceHolder = source.cellAppearanceHolder.Clone();
				if( !this.HasCellAppearance )
					this.cellAppearanceHolder = new Infragistics.Win.AppearanceHolder();

				this.cellAppearanceHolder.InitializeFrom( source.cellAppearanceHolder );
			}

			// JJD 10/24/01
			// Init the header first
			//
			this.Header.InitializeFrom ( source.Header, propCat);

			if ( source.Columns != null )
			{
				// make sure we have a group cols collection
				//
				if ( this.groupColumnsCollection == null )
					this.AllocateGroupColumnsCollection();

				this.groupColumnsCollection.InitializeFrom ( source.Columns, propCat);
			}
			else
			{
				// since the source group didn't have any columns then
				// clear this group's columns collection
				//
				if ( this.groupColumnsCollection != null )
					this.groupColumnsCollection.Clear();
			}

            // MRS - NAS 9.1 - Groups in RowLayout
            if (null != source.rowLayoutGroupInfo)
                this.RowLayoutGroupInfo.InitializeFrom(source.rowLayoutGroupInfo);
            else if (null != this.rowLayoutGroupInfo)
                this.rowLayoutGroupInfo.Reset();
		}

		#region type converter

		/// <summary>
		/// UltraGridGroup type converter.
		/// </summary>
		public sealed class UltraGridGroupTypeConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if (destinationType == typeof(InstanceDescriptor)) 
				{
					return true;
				}
				
				return base.CanConvertTo(context, destinationType);
			}

            /// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) 
			{
				if (destinationType == null) 
				{
					throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_20"));
				}

				if ( destinationType == typeof(InstanceDescriptor) && 
					value is UltraGridGroup ) 
				{
					UltraGridGroup item = (UltraGridGroup)value;
					
					ConstructorInfo ctor;

					string key = item.Key;

					bool hasKey = (key != null && key.Length > 0 );
					

					if ( hasKey )
					{
						ctor = typeof(UltraGridGroup).GetConstructor(new Type[] { typeof ( string ),
																				  typeof( int ) } );
					}
					else
					{
						ctor = typeof(UltraGridGroup).GetConstructor(new Type[] { typeof( int ) } );
					}

					if (ctor != null) 
					{
						//false as the last parameter here causes generation of a local variable for the type 

						if ( hasKey )
						{
							return new InstanceDescriptor(ctor, new object[] { key, 
																				item.InternalID }, false );
						}
						else
						{
							return new InstanceDescriptor(ctor, new object[] { item.InternalID }, false );
						}
					}
				}

				return base.ConvertTo(context, culture, value, destinationType);
			}
		}
		
		#endregion


        // MRS - NAS 9.1 - Groups in RowLayout
        //
        #region NAS 9.1 - Groups in RowLayout

        // MRS 2/13/2009 - TFS13834
        #region AllowParentGroupChangeResolved
        internal bool AllowParentGroupChangeResolved
        {
            get
            {
                switch (this.Band.AllowGroupMovingResolved)
                {
                    case AllowGroupMoving.WithinBand:
                        return true;
                    case AllowGroupMoving.NotAllowed:
                    case AllowGroupMoving.WithinGroup:
                        return false;
                    case AllowGroupMoving.Default:
                    default:
                        Debug.Fail("Unknown or invalid AllowColMovingResolved");
                        return false;
                }
            }
        }
        #endregion //AllowParentGroupChangeResolved

        #region CalculatePreferredGroupHeaderSize
        private Size CalculatePreferredGroupHeaderSize()
        {
            if (this.band == null)
                return Size.Empty;

            System.Drawing.Size sizeOfCaption = new Size(0, 0);

            // Added logic to calculate the width based on the caption
            //
            if (this.Header != null)
            {
                string strCaption = this.Header.Caption;
                int len = strCaption.Length;

                if (len > 0)
                {
                    // Otherwise, we need to calculate a reasonable width for this column
                    //
                    AppearanceData appData = new AppearanceData();

                    this.Header.ResolveAppearance(ref appData, AppearancePropFlags.FontData);

                    // JJD 12/17/01
                    // Moved font height logic into CalculateFontHeight
                    //
                    sizeOfCaption = this.band.Layout.CalculateFontSize(ref appData, strCaption);
                }
            }

            // Allow for swap btn width
            //
            if (this.AllowSwapping)
                sizeOfCaption.Width += 10;

            sizeOfCaption.Width += MIN_GROUP_WIDTH;
            return sizeOfCaption;
        }
        #endregion //CalculatePreferredGroupHeaderSize

        #region CalcLayoutHelper

        private void CalcLayoutHelper()
        {
            this.CalcLayoutHelper(-1);
        }

        // SSP 7/12/05 BR04916
        // Added an overload that takes in bandExtent parameter.
        //
        private void CalcLayoutHelper(int bandExtent)
        {
            UltraGridBand band = this.Band;
            if (band == null)
                return;

            this.layoutItemDimensionsCellArea = null;
            this.layoutItemDimensionsHeaderArea = null;

            // We are using differnet ILayoutContainer implementations for card view and regular view.
            // Also we are using different ILayoutContainer implementations for the headers and cells.
            //
            if (!band.CardView)
            {
                // SSP 7/12/05 BR04916
                // Added an overload that takes in bandExtent parameter. If bandExtent is -1 then
                // use the GetExtentLayout.
                // 
                //int bandExtent = this.GetExtentLayout( );
                if (bandExtent < 0)
                    bandExtent = band.GetExtentLayout();

                if (band.AreColumnHeadersInSeparateLayoutArea)
                {
                    // If the headers are separate from the cells, then calcualte the extent of the header area.
                    // Following code in addition to calculating the extent of the overall row cell area, it 
                    // also calculates the location and size of individual items (cells) in the row cell layout area.
                    //                                         
                    LayoutContainerCalcSize headerAreaContainer = this.GetCachedLayoutContainerCalcSize();
                    headerAreaContainer.Initialize(this.PreferredHeaderLayoutSize);
                    this.HeaderLayoutManager.LayoutContainer(headerAreaContainer, null);
                    this.layoutItemDimensionsHeaderArea = this.gridBagHeaderLayoutManager.GetLayoutItemDimensions(headerAreaContainer, null);
                }

                // Calculate the extent of the row cell area. Following code in addition to calculating 
                // the extent of the overall row cell area, it also calculates the location and size of 
                // individual items (cells) in the row cell layout area.
                //                
                LayoutContainerCalcSize cellAreaContainer = this.GetCachedLayoutContainerCalcSize();
                cellAreaContainer.Initialize(this.PreferredRowLayoutSize);
                this.RowLayoutManager.LayoutContainer(cellAreaContainer, null);
                this.layoutItemDimensionsCellArea = this.gridBagRowLayoutManager.GetLayoutItemDimensions(cellAreaContainer, null);
            }
            else
            {
                if (band.AreColumnHeadersInSeparateLayoutArea)
                {
                    // If the headers are separate from the cells, then calcualte the extent of the header area.
                    // Following code in addition to calculating the extent of the overall row cell area, it 
                    // also calculates the location and size of individual items (cells) in the row cell layout area.
                    //

                    LayoutContainerCalcSize cardLabelAreaContainer = this.GetCachedLayoutContainerCalcSize();
                    cardLabelAreaContainer.Initialize();
                    this.HeaderLayoutManager.LayoutContainer(cardLabelAreaContainer, null);
                    this.layoutItemDimensionsHeaderArea = this.gridBagHeaderLayoutManager.GetLayoutItemDimensions(cardLabelAreaContainer, null);
                }

                // Calculate the extent of the row cell area. Following code in addition to calculating 
                // the extent of the overall row cell area, it also calculates the location and size of 
                // individual items (cells) in the row cell layout area.
                //
                Rectangle cellAreaRect = new Rectangle(0, 0, this.Band.CardWidthResolved, this.PreferredRowLayoutSize.Height);
                LayoutContainerCalcSize cardCellAreaContainer = this.GetCachedLayoutContainerCalcSize();
                cardCellAreaContainer.Initialize(cellAreaRect);
                this.RowLayoutManager.LayoutContainer(cardCellAreaContainer, null);
                this.layoutItemDimensionsCellArea = this.gridBagRowLayoutManager.GetLayoutItemDimensions(cardCellAreaContainer, null);
            }

            // Get the array of columns that are visible in the layout. This is usually the columns
            // with Hidden not set to true.
            //
            UltraGridColumn[] columnsArr = this.GetLayoutVisibleColumns();

            // SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout/Card-View
            // This is necessary for the card-view and wrap header text. When the row-layout 
            // cache is verified it may cause the cached card label size to get dirtied in which
            // case we need to force the card label size to be verified here otherwise there is
            // no good trigger point for the verification of card label sizes.
            // 
            // ----------------------------------------------------------------------------------
            for (int i = 0; i < columnsArr.Length; i++)
            {
                Size size = columnsArr[i].CardLabelSize;
            }
            // ----------------------------------------------------------------------------------

            // SSP 11/10/05 BR07605
            // If row sizing is Auto then we need to recalc the ideal heights of the rows since
            // the widths of the columns could have potentially changed.
            // 
            band.BumpMajorRowHeightVersion();
        }

        #endregion // CalcLayoutHelper

        #region ChildGroupsResolved
        internal IEnumerable<UltraGridGroup> ChildGroupsResolved
        {
            get
            {
                if (this.band != null &&
                    this.band.RowLayoutStyle == RowLayoutStyle.GroupLayout)
                {
                    return new GroupLayoutChildGroupsEnumerable(this);
                }

                return null;
            }
        }
        #endregion //ChildGroupsResolved

        #region ColumnsResolved
        internal IEnumerable<UltraGridColumn> ColumnsResolved
        {
            get
            {
                if (this.band != null &&
                    this.band.RowLayoutStyle == RowLayoutStyle.GroupLayout)
                {
                    return new GroupLayoutColumnsEnumerable(this);
                }

                return this.Columns;
            }
        }
        #endregion //ColumnsResolved
        
        #region FirstSelectableColumn

        internal UltraGridColumn FirstSelectableColumn
        {
            get
            {
                foreach (UltraGridColumn column in this.ColumnsResolved)
                {
                    if (column.IsSelectable == false)
                        continue;

                    return column;
                }

                return null;
            }
        }

        #endregion FirstSelectableColumn

        // MRS 2/10/2009 - TFS13728
        #region GetDescendantColumns
        internal List<UltraGridColumn> GetDescendantColumns(bool recursive)
        {
            List<UltraGridColumn> columnsList = new List<UltraGridColumn>();
            this.GetDescendantColumnsHelper(recursive, columnsList);
            return columnsList;
        }

        internal void GetDescendantColumnsHelper(bool recursive, List<UltraGridColumn> columnsList)
        {
            IEnumerable<UltraGridColumn> columnsEnumerable = this.ColumnsResolved;
            List<UltraGridColumn> columns = new List<UltraGridColumn>(columnsEnumerable);

            foreach (UltraGridColumn column in columns)
                columnsList.Add(column);

            if (recursive)
            {
                IEnumerable<UltraGridGroup> childGroups = this.ChildGroupsResolved;
                if (childGroups != null)
                {
                    foreach (UltraGridGroup childGroup in childGroups)
                    {
                        childGroup.GetDescendantColumnsHelper(recursive, columnsList);
                    }
                }
            }            
        }

        #endregion //GetDescendantColumns

        #region GetCachedLayoutContainerCalcSize

        internal LayoutContainerCalcSize GetCachedLayoutContainerCalcSize()
        {
            if (null == this.layoutContainerCalcSize)
                this.layoutContainerCalcSize = new LayoutContainerCalcSize();

            return this.layoutContainerCalcSize;
        }

        #endregion // GetCachedLayoutContainerCalcSize        

        #region GetLayoutVisibleColumns

        // MRS - NAS 9.1 - Groups in RowLayout
        //private UltraGridColumn[] GetLayoutVisibleColumns()
        internal UltraGridColumn[] GetLayoutVisibleColumns()
        {
            UltraGridBand band = this.Band;
            if (band == null)
                return new UltraGridColumn[] { };
       
            ColumnsCollection columns = band.Columns;
            List<UltraGridColumn> list = new List<UltraGridColumn>(columns.Count);

            for (int i = 0, count = columns.Count; i < count; i++)
            {
                UltraGridColumn column = columns[i];

                if (null != column && column.IsVisibleInLayout &&             
                    this.ShouldIncludeInGroupLayout(column))
                    list.Add(column);
            }

            return list.ToArray();
        }

        #endregion // GetLayoutVisibleColumns

        #region GetLayoutVisibleGroups

        // MRS - NAS 9.1 - Groups in RowLayout
        //private UltraGridGroup[] GetLayoutVisibleGroups()
        internal UltraGridGroup[] GetLayoutVisibleGroups()
        {
            UltraGridBand band = this.Band;
            if (band == null ||
                band.RowLayoutStyle != RowLayoutStyle.GroupLayout)
            {
                return new UltraGridGroup[] { };
            }

            GroupsCollection groups = band.Groups;

            List<UltraGridGroup> list = new List<UltraGridGroup>(groups.Count);

            for (int i = 0, count = groups.Count; i < count; i++)
            {
                UltraGridGroup group = groups[i];

                if (null != group && group.IsVisibleInLayout &&
                    this.ShouldIncludeInGroupLayout(group))
                    list.Add(group);
            }

            return list.ToArray();
        }

        #endregion // GetLayoutVisibleGroups

        #region GetLayoutGroup
        internal static ILayoutGroup GetLayoutGroup(UltraGridBand band, ILayoutChildItem layoutItem)
        {
            switch (band.RowLayoutStyle)
            {
                case RowLayoutStyle.GroupLayout:

                    RowLayoutColumnInfo rowLayoutColumnInfo = UltraGridBand.GetRowLayoutColumnInfo(layoutItem);
                    
                    // MRS 4/27/2009 - TFS17076
                    if (rowLayoutColumnInfo == null)
                        return null;

                    UltraGridGroup parentGroup = rowLayoutColumnInfo.ParentGroup;

                    return parentGroup != null
                        ? (ILayoutGroup)parentGroup
                        : (ILayoutGroup)band;
                case RowLayoutStyle.ColumnLayout:
                    return band;
                case RowLayoutStyle.None:
                    return null;
                default:
                    Debug.Fail("Unknown RowLayoutStyle");
                    return null;
            }
        }
        #endregion //GetLayoutGroup        

		#region GetRightMostColumn

		internal UltraGridColumn GetRightMostColumn()
		{
			GridBagLayoutItemDimensionsCollection layoutItemDimensions = this.LayoutItemDimensions;

			UltraGridColumn rightMostColumn = UltraGridGroup.GetRightMostItem( this.GetLayoutVisibleColumns(), layoutItemDimensions );
			UltraGridGroup rightMostGroup = UltraGridGroup.GetRightMostItem( this.GetLayoutVisibleGroups(), layoutItemDimensions );

			// If the group has no nested groups, just return the right-most column.
			if ( rightMostGroup == null )
				return rightMostColumn;

			// If the column and group are non-null, and the column is more right than the group, return the column.
			if ( rightMostColumn != null )
			{
				int groupOriginX = layoutItemDimensions[ rightMostGroup.RowLayoutGroupInfo ].OriginX;
				int columnOriginX = layoutItemDimensions[ rightMostColumn.RowLayoutColumnInfo ].OriginX;

				if ( groupOriginX <= columnOriginX )
					return rightMostColumn;
			}

			// Return the group's right-most column if valid. Otherwise, just return the column.
			return rightMostGroup.GetRightMostColumn() ?? rightMostColumn;
		} 

		#endregion GetRightMostColumn

		#region GetRightMostItem

		private static T GetRightMostItem<T>( T[] items, GridBagLayoutItemDimensionsCollection layoutItemDimensions ) where T : IProvideRowLayoutColumnInfo
		{
			T rightMostItem = default( T );

			foreach ( T item in items )
			{
				RowLayoutColumnInfo layoutInfo = item.RowLayoutColumnInfo;
				if ( layoutItemDimensions.Exists( layoutInfo ) == false )
					continue;

				if ( null == rightMostItem ||
					layoutItemDimensions[ layoutInfo ].OriginX > layoutItemDimensions[ rightMostItem.RowLayoutColumnInfo ].OriginX )
				{
					rightMostItem = item;
				}
			}

			return rightMostItem;
		} 

		#endregion GetRightMostItem

        #region GroupLayoutItemCellContent
        internal GroupLayoutItemCellContent GroupLayoutItemCellContent
        {
            get
            {
                if (this.groupLayoutItemCellContent == null)
                    this.groupLayoutItemCellContent = new GroupLayoutItemCellContent(this);

                return groupLayoutItemCellContent;
            }
        }
        #endregion //GroupLayoutItemCellContent

        #region GroupLayoutItemHeaderContent
        internal GroupLayoutItemHeaderContent GroupLayoutItemHeaderContent
        {
            get
            {
                if (this.groupLayoutItemHeaderContent == null)
                    this.groupLayoutItemHeaderContent = new GroupLayoutItemHeaderContent(this);

                return groupLayoutItemHeaderContent;
            }
        }
        #endregion //GroupLayoutItemHeaderContent

        #region GroupLayoutItemSummaryContent
        internal GroupLayoutItemSummaryContent GroupLayoutItemSummaryContent
        {
            get
            {
                if (this.groupLayoutItemSummaryContent == null)
                    this.groupLayoutItemSummaryContent = new GroupLayoutItemSummaryContent(this);

                return groupLayoutItemSummaryContent;
            }
        }
        #endregion //GroupLayoutItemSummaryContent

        #region HasRowLayoutColumnInfo

        internal bool HasRowLayoutGroupInfo
        {
            get
            {
                return null != this.rowLayoutGroupInfo;
            }
        }

        #endregion // HasRowLayoutColumnInfo

        #region HasSummaries

        internal bool HasSummaries
        {
            get
            {
                return (this.Band != null && this.Band.HasSummaries);
            }
        }

        #endregion // HasSummaries

        #region HeaderLayoutManager

        internal LayoutManagerBase HeaderLayoutManager
        {
            get
            {
                // MRS 2/9/2009 - TFS13725
                //this.VerifyRowLayoutCache();
                this.VerifyBandRowLayoutCache();

                return this.gridBagHeaderLayoutManager;
            }
        }

        #endregion // HeaderLayoutManager

        #region IsDescendantOfGroup
        internal static bool IsDescendantOfGroup(UltraGridGroup group, IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo)
        {
            bool isDescendantOfGroup = iProvideRowLayoutColumnInfo == group;
            UltraGridGroup parentGroup = iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ParentGroup;
            while (parentGroup != null &&
                isDescendantOfGroup == false)
            {
                if (parentGroup == group)
                    isDescendantOfGroup = true;

                parentGroup = parentGroup.RowLayoutGroupInfo.ParentGroup;
            }
            return isDescendantOfGroup;
        }
        #endregion //IsDescendantOfGroup

        #region IsVisibleInLayout

        /// <summary>
        /// Indicates whether this group is visible in row-layout mode.
        /// </summary>
        /// <remarks>
        /// <p class="body">Set <see cref="Hidden"/> to true to hide a group.</p>
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsVisibleInLayout
        {
            get
            {
				// MD 1/15/09 - Groups in RowLayouts
				// Use the resolved value, which also checks an owning group's hidden value.
                //return !this.Hidden;
				return ( this.HiddenResolved == false );
            }
        }

        #endregion // IsVisibleInLayout
                
        #region LastSelectableColumn

        internal UltraGridColumn LastSelectableColumn
        {
            get
            {
                UltraGridColumn lastColumn = null;
                foreach (UltraGridColumn column in this.ColumnsResolved)
                {
                    if (column.IsSelectable == false)
                        continue;

                    lastColumn = column;
                }

                return lastColumn;
            }
        }

        #endregion LastSelectableColumn

        #region LayoutItemDimensions

        internal GridBagLayoutItemDimensionsCollection LayoutItemDimensions
        {
            get
            {
                // MRS 2/9/2009 - TFS13725
                //this.VerifyRowLayoutCache();
                this.VerifyBandRowLayoutCache();

                return this.layoutItemDimensions;
            }
        }

        #endregion // LayoutItemDimensions

        #region LayoutItemDimensionsHeaderArea

        internal GridBagLayoutItemDimensionsCollection LayoutItemDimensionsHeaderArea
        {
            get
            {
                // MRS 2/9/2009 - TFS13725
                //this.VerifyRowLayoutCache();
                this.VerifyBandRowLayoutCache();

                return this.layoutItemDimensionsHeaderArea;
            }
        }

        #endregion // LayoutItemDimensionsHeaderArea

        #region LayoutItemDimensionsCellArea

        internal GridBagLayoutItemDimensionsCollection LayoutItemDimensionsCellArea
        {
            get
            {
                // MRS 2/9/2009 - TFS13725
                //this.VerifyRowLayoutCache();
                this.VerifyBandRowLayoutCache();

                return this.layoutItemDimensionsCellArea;
            }
        }

        #endregion // LayoutItemDimensionsCellArea

        #region MinimumSize

        internal Size MinimumSize
        {
            get
            {
                Size size = this.RowLayoutGroupInfo.MinimumLabelSize;

                return size;
            }
        }

        #endregion // MinimumSize

        #region PreferredHeaderLayoutSize

        internal Size PreferredHeaderLayoutSize
        {
            get
            {
                // MRS 2/9/2009 - TFS13725
                //this.VerifyRowLayoutCache();
                this.VerifyBandRowLayoutCache();

                return this.preferredHeaderLayoutSize;
            }
        }

        #endregion // PreferredHeaderLayoutSize

        #region PreferredRowLayoutSize

        internal Size PreferredRowLayoutSize
        {
            get
            {
                // MRS 2/9/2009 - TFS13725
                //this.VerifyRowLayoutCache();
                this.VerifyBandRowLayoutCache();

                return this.preferredRowLayoutSize;
            }
        }

        #endregion // PreferredRowLayoutSize

        #region RowLayoutGroupInfo

        /// <summary>
        /// Gets the instance of RowLayoutColumnInfo associated with this group.
        /// </summary>
        /// <remarks>
        /// <p>You can set various properties in the returned object to customize where the header associated with this group shows up.</p>
        /// <p>By default all the groups are visible. Set <see cref="Hidden"/> to true to hide the group.</p>
        /// <p>Set <see cref="UltraGridBand.UseRowLayout"/> property to true to turn on the row-layout functionality.</p>
        /// </remarks>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo RowLayoutGroupInfo
        {
            get
            {
                if (null == this.rowLayoutGroupInfo)
                {
                    this.rowLayoutGroupInfo = new GroupLayoutInfo(this);

                    this.rowLayoutGroupInfo.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }

                return this.rowLayoutGroupInfo;
            }
        }

        #region ShouldSerializeRowGroupInfo

        /// <summary>
        /// Returns true if this property is not set to its default value
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeRowGroupInfo()
        {
            return null != this.rowLayoutGroupInfo && this.rowLayoutGroupInfo.ShouldSerialize();
        }

        #endregion // ShouldSerializeRowGroupInfo

        #region ResetRowLayoutGroupInfo

        /// <summary>
        /// Resets the ResetRowLayoutGroupInfo property to its default value.
        /// </summary>
        public void ResetRowLayoutGroupInfo()
        {
            if (null != this.rowLayoutGroupInfo)
                this.rowLayoutGroupInfo.Reset();
        }

        #endregion // ResetRowLayoutGroupInfo

        #endregion // RowLayoutGroupInfo

        #region RowLayoutManager

        internal LayoutManagerBase RowLayoutManager
        {
            get
            {
                // MRS 2/9/2009 - TFS13725
                //this.VerifyRowLayoutCache();
                this.VerifyBandRowLayoutCache();

                return this.gridBagRowLayoutManager;
            }
        }

        #endregion // RowLayoutManager

        #region ShouldIncludeInGroupLayout
        private bool ShouldIncludeInGroupLayout(IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo)
        {
            switch (band.RowLayoutStyle)
            {
                case RowLayoutStyle.None:
                    Debug.Fail("This method should not be called when not using RowLayouts");
                    return false;
                case RowLayoutStyle.ColumnLayout:
                    Debug.Fail("This method should not be called when not using ColumnLayout");
                    return false;
                case RowLayoutStyle.GroupLayout:
                    return iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ParentGroup == this;
                default:
                    Debug.Fail("Unknown RowLayoutStyle");
                    return true;

            }
        }
        #endregion ShouldIncludeInGroupLayout

        #region ShouldSerializeRowLayoutColumnInfo

        /// <summary>
        /// Returns true if this property is not set to its default value
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeRowLayoutGroupInfo()
        {
            return null != this.rowLayoutGroupInfo && this.rowLayoutGroupInfo.ShouldSerialize();
        }

        #endregion // ShouldSerializeRowLayoutColumnInfo

        #region SummaryLayoutManager

        internal LayoutManagerBase SummaryLayoutManager
        {
            get
            {
                if (!this.HasSummaries)
                    return null;

                // MRS 2/9/2009 - TFS13725
                //this.VerifyRowLayoutCache();
                this.VerifyBandRowLayoutCache();

                return this.gridBagSummaryLayoutManager;
            }
        }        

        #endregion // SummaryLayoutManager

        // MRS 2/9/2009 - TFS13725
        #region VerifyBandRowLayoutCache
        private void VerifyBandRowLayoutCache()
        {
            this.Band.VerifyRowLayoutCache();
        }
        #endregion //VerifyBandRowLayoutCache

        #region VerifyRowLayoutCache

        internal void VerifyRowLayoutCache()
        {
            UltraGridBand band = this.Band;
            if (band == null)
                return;

            if (this.verifiedRowLayoutCacheVersion == band.RowLayoutCacheVersion)
                return;

            // Anti-recursion flag.
            //
            if (this.inVerifyRowLayoutCache)
                return;

            // SSP 8/6/03 UWG2568
            // When we are resizing all the columns, we don't want to recalculate the row layout
            // until the operation of resizing all the columns has finished. To accomplish that
            // added dontVerifyRowLayoutCache flag.
            //
            if (band.dontVerifyRowLayoutCache)
                return;

            this.inVerifyRowLayoutCache = true;

            this.verifiedRowLayoutCacheVersion = band.RowLayoutCacheVersion;

            // Get an instance of ILayoutContainer specifically for calculating the container rect.
            //
            LayoutContainerCalcSize layoutContainer = this.GetCachedLayoutContainerCalcSize();
            layoutContainer.Initialize();

            try
            {
                int i;
                // Headers can be separate from the cells or they can be togather.
                //
                bool headersInSeparateArea = band.AreColumnHeadersInSeparateLayoutArea;

                // Get the visible columns array. (The columns with Hidden set to false).
                //
                UltraGridColumn[] columnsArr = this.GetLayoutVisibleColumns();

                // Sort the visible columns according to the their VisiblePosition settings.
                //
                Infragistics.Win.Utilities.SortMerge(columnsArr, new UltraGridBand.ColumnVisiblePositionComparer());

                UltraGridGroup[] groupsArr = this.GetLayoutVisibleGroups();

                // Sort the visible groups according to the their VisiblePosition settings.
                //
                Infragistics.Win.Utilities.SortMerge(groupsArr, new UltraGridBand.GroupVisiblePositionComparer());

                for (i = 0; i < groupsArr.Length; i++)
                {
                    UltraGridGroup group = groupsArr[i];
                    group.VerifyRowLayoutCache();
                } 

                // Set up the gridbag layout manager with the visible columns as its items.
                //
                if (null == this.gridBagLayoutManagerHelper)
                    this.gridBagLayoutManagerHelper = UltraGridBand.CreateGridBagLayoutManager();

                this.gridBagLayoutManagerHelper.LayoutItems.Clear();

                for (i = 0; i < groupsArr.Length; i++)
                {
                    UltraGridGroup group = groupsArr[i];
                    RowLayoutColumnInfo ci = group.RowLayoutGroupInfo;
                    GridBagConstraint gridBagConstraint = new GridBagConstraint(ci.OriginX, ci.OriginY, ci.SpanXResolved, ci.SpanYResolved);
                    this.gridBagLayoutManagerHelper.LayoutItems.Add(ci, gridBagConstraint);
                }

                for (i = 0; i < columnsArr.Length; i++)
                {
                    UltraGridColumn column = columnsArr[i];
                    RowLayoutColumnInfo ci = column.RowLayoutColumnInfo;
                    this.gridBagLayoutManagerHelper.LayoutItems.Add(ci, ci);
                }                

                this.layoutItemDimensions = this.gridBagLayoutManagerHelper.GetLayoutItemDimensions(layoutContainer, null);

                // Set the cached spans of this group and the origins of it's child groups.
                this.SetCachedLayoutItemDimensions(groupsArr);

                // SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
                // In AutoFitStyle mode of ExtendLastColumn set the WeightX on the rightmost
                // column so that it extends to occupy the extra visible space. The extent of 
                // the band is already adjusted to include the extra space in FiltColumnsToWidth 
                // method.
                //
                // ------------------------------------------------------------------------------
                // Reset WeightXDefault we might have set previously.
                //
                for (i = 0; i < columnsArr.Length; i++)
                    columnsArr[i].RowLayoutColumnInfo.WeightXDefault = 0.0f;

                for (i = 0; i < groupsArr.Length; i++)
                    groupsArr[i].RowLayoutGroupInfo.WeightXDefault = 0.0f;

				// MD 1/21/09 - Groups in RowLayouts
				// This should only be done for the band, not the group.
				#region Old Code

				

				#endregion Old Code

                if (null == this.gridBagRowLayoutManager)
                    this.gridBagRowLayoutManager = UltraGridBand.CreateGridBagLayoutManager();

                this.gridBagRowLayoutManager.LayoutItems.Clear();

                if (null == this.gridBagHeaderLayoutManager)
                    this.gridBagHeaderLayoutManager = UltraGridBand.CreateGridBagLayoutManager();

                this.gridBagHeaderLayoutManager.LayoutItems.Clear();

                // If summaries are visible, then create a gridbag lm for summary area otherwise
                // dispose of it if it was created previously.
                // 
                if (band.HasSummaries && !band.CardView)
                {
                    if (null == this.gridBagSummaryLayoutManager)
                        this.gridBagSummaryLayoutManager = UltraGridBand.CreateGridBagLayoutManager();

                    this.gridBagSummaryLayoutManager.LayoutItems.Clear();
                }
                else
                {
                    if (null != this.gridBagSummaryLayoutManager)
                        this.gridBagSummaryLayoutManager.LayoutItems.Clear();
                    this.gridBagSummaryLayoutManager = null;
                }

                if (headersInSeparateArea)
                {
                    for (i = 0; i < groupsArr.Length; i++)
                    {
                        UltraGridGroup group = groupsArr[i];

                        this.gridBagRowLayoutManager.LayoutItems.Add(group.GroupLayoutItemCellContent, group.GroupLayoutItemCellContent);

                        // Even though the headers are not with the cells, we still need to add in 
                        // a group header with the cells when it's aligned to one side. If we don't, 
                        // there will be extra space in the layout and the columns / headers may not
                        // line up with the cells. 
                        //
                        // MRS 2/24/2009 - TFS14432
                        //bool addGroupHeaderInCellArea = UltraGridBand.ShouldAddGroupHeaderToCellArea(this);
                        bool addGroupHeaderInCellArea = UltraGridBand.ShouldAddGroupHeaderToCellArea(group);
                        if (addGroupHeaderInCellArea)
                            this.gridBagRowLayoutManager.LayoutItems.Add(group.Header, group.Header);

                        this.gridBagHeaderLayoutManager.LayoutItems.Add(group.GroupLayoutItemHeaderContent, group.GroupLayoutItemHeaderContent);
                        this.gridBagHeaderLayoutManager.LayoutItems.Add(group.Header, group.Header);

                        // If we have summaries then add items to summary layout manager as well.
                        //
                        if (null != this.gridBagSummaryLayoutManager)
                        {
                            this.gridBagSummaryLayoutManager.LayoutItems.Add(group.GroupLayoutItemSummaryContent, group.GroupLayoutItemSummaryContent);
                            if (addGroupHeaderInCellArea)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(group.Header, group.Header);
                        }
                    }                    

                    for (i = 0; i < columnsArr.Length; i++)
                    {
                        UltraGridColumn column = columnsArr[i];

                        this.gridBagRowLayoutManager.LayoutItems.Add(column, column);
                        this.gridBagHeaderLayoutManager.LayoutItems.Add(column.Header, column.Header);

                        // If we have summaries then add items to summary layout manager as well.
                        //
                        if (null != this.gridBagSummaryLayoutManager)
                            this.gridBagSummaryLayoutManager.LayoutItems.Add(new SummaryLayoutItem(column), column);
                    }                    
                }
                else
                {
                    // MRS - NAS 9.1 - Groups in RowLayout
                    // ---------------------------------------------------------------------
                    for (i = 0; i < groupsArr.Length; i++)
                    {
                        UltraGridGroup group = groupsArr[i];

                        LabelPosition labelPosition = group.RowLayoutGroupInfo.LabelPositionResolved;

                        // SSP 11/17/04
                        // Added a way to hide the header or the cell of a group in the row layout mode.
                        // This way you can do things like hide a row of headers or add unbound groups
                        // and hide their cells to show grouping headers etc...
                        // Added LabelOnly member to the enum.
                        //						
                        //if ( LabelPosition.Left == labelPosition ||	LabelPosition.Top == labelPosition )
                        if (LabelPosition.LabelOnly == labelPosition
                            || LabelPosition.Left == labelPosition || LabelPosition.Top == labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add(group.Header, group.Header);

                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(group.Header, group.Header);
                        }

                        // SSP 11/17/04
                        // Added a way to hide the header or the cell of a group in the row layout mode.
                        // This way you can do things like hide a row of headers or add unbound groups
                        // and hide their cells to show grouping headers etc...
                        // Added LabelOnly member to the enum. Enclosed the existing code into the if
                        // block so we don't add the group if the LabelPosition is LabelOnly.
                        // 
                        if (LabelPosition.LabelOnly != labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add( group.GroupLayoutItemCellContent, group.GroupLayoutItemCellContent );
                                                        
                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(group.GroupLayoutItemSummaryContent, group.GroupLayoutItemSummaryContent);
                        }

                        if (LabelPosition.Right == labelPosition || LabelPosition.Bottom == labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add(group.Header, group.Header);

                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(group.Header, group.Header);
                        }
                    }
                    // ---------------------------------------------------------------------

                    for (i = 0; i < columnsArr.Length; i++)
                    {
                        UltraGridColumn column = columnsArr[i];

                        LabelPosition labelPosition = column.RowLayoutColumnInfo.LabelPositionResolved;

                        // SSP 11/17/04
                        // Added a way to hide the header or the cell of a column in the row layout mode.
                        // This way you can do things like hide a row of headers or add unbound columns
                        // and hide their cells to show grouping headers etc...
                        // Added LabelOnly member to the enum.
                        //						
                        //if ( LabelPosition.Left == labelPosition ||	LabelPosition.Top == labelPosition )
                        if (LabelPosition.LabelOnly == labelPosition
                            || LabelPosition.Left == labelPosition || LabelPosition.Top == labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add(column.Header, column.Header);

                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(column.Header, column.Header);
                        }

                        // SSP 11/17/04
                        // Added a way to hide the header or the cell of a column in the row layout mode.
                        // This way you can do things like hide a row of headers or add unbound columns
                        // and hide their cells to show grouping headers etc...
                        // Added LabelOnly member to the enum. Enclosed the existing code into the if
                        // block so we don't add the column if the LabelPosition is LabelOnly.
                        // 
                        if (LabelPosition.LabelOnly != labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add(column, column);

                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(new SummaryLayoutItem(column), column);
                        }

                        if (LabelPosition.Right == labelPosition || LabelPosition.Bottom == labelPosition)
                        {
                            this.gridBagRowLayoutManager.LayoutItems.Add(column.Header, column.Header);

                            // If we have summaries then add items to summary layout manager as well.
                            //
                            if (null != this.gridBagSummaryLayoutManager)
                                this.gridBagSummaryLayoutManager.LayoutItems.Add(column.Header, column.Header);
                        }
                    }
                }

                this.preferredRowLayoutSize = this.gridBagRowLayoutManager.CalculatePreferredSize(layoutContainer, null);
                this.preferredHeaderLayoutSize = headersInSeparateArea
                    ? this.gridBagHeaderLayoutManager.CalculatePreferredSize(layoutContainer, null)
                    : Size.Empty;

                // SSP 3/5/05 BR02359
                // Cache the minimum layout sizes as well.
                //
                this.minimumRowLayoutSize = this.gridBagRowLayoutManager.CalculateMinimumSize(layoutContainer, null);
                this.minimumHeaderLayoutSize = headersInSeparateArea
                    ? this.gridBagHeaderLayoutManager.CalculateMinimumSize(layoutContainer, null)
                    : Size.Empty;

                if (headersInSeparateArea)
                {
                    int[] gbCellColumnWidths = this.gridBagRowLayoutManager.ColumnWidths;
                    int[] gbHeaderColumnWidths = this.gridBagHeaderLayoutManager.ColumnWidths;
                    int[] gbCellRowHeights = this.gridBagRowLayoutManager.RowHeights;
                    int[] gbHeaderRowHeights = this.gridBagHeaderLayoutManager.RowHeights;

                    // In card-view, headers need to have the same height as the cells and
                    // in regular view, headers need to have the same width as the cells.
                    // 
                    if (null != gbCellColumnWidths && null != gbCellRowHeights
                        && null != gbHeaderColumnWidths && null != gbHeaderRowHeights)
                    {
                        // MRS - NAS 9.1 - Groups in RowLayout
                        // I moved this down into the if (this.CardView) block. Only one of these
                        // two pairs needs to be the same.
                        //
                        //Debug.Assert(gbCellColumnWidths.Length == gbHeaderColumnWidths.Length &&
                        //    gbCellRowHeights.Length == gbHeaderRowHeights.Length, "Lengths should be the same.");

                        int[] arr1 = null;
                        int[] arr2 = null;

                        if (band.CardView)
                        {
                            // MRS - NAS 9.1 - Groups in RowLayout
                            Debug.Assert(gbCellRowHeights.Length == gbHeaderRowHeights.Length, "Lengths should be the same.");

                            // In card view make sure the corresponding headers and cells have the same heights.
                            //
                            arr1 = gbCellRowHeights;
                            arr2 = gbHeaderRowHeights;
                        }
                        else
                        {
                            // MRS - NAS 9.1 - Groups in RowLayout                            
                            Debug.Assert(gbCellColumnWidths.Length == gbHeaderColumnWidths.Length, "Lengths should be the same.");

                            // In regular view make sure the corresponding headers and cells have the same widths.
                            //
                            arr1 = gbCellColumnWidths;
                            arr2 = gbHeaderColumnWidths;
                        }

                        int len = Math.Min(arr1.Length, arr2.Length);
                        for (int j = 0; j < len; j++)
                            arr1[j] = arr2[j] = Math.Max(arr1[j], arr2[j]);
                    }
                    else
                    {
                        Debug.Assert(false, "ColumnWidths and RowHeights off the gridbag lm should have been non-null !");
                    }
                }

                if (null != this.gridBagSummaryLayoutManager)
                {
                    int[] gbCellColumnWidths = this.gridBagRowLayoutManager.ColumnWidths;
                    int[] gbCellRowHeights = this.gridBagRowLayoutManager.RowHeights;
                    int[] gbSummaryColumnWidths = this.gridBagSummaryLayoutManager.ColumnWidths;
                    int[] gbSummaryRowHeights = this.gridBagSummaryLayoutManager.RowHeights;

                    Debug.Assert(gbCellColumnWidths.Length == gbSummaryColumnWidths.Length &&
                        gbCellRowHeights.Length == gbSummaryRowHeights.Length, "Lengths should be the same.");

                    for (int j = Math.Min(gbCellColumnWidths.Length, gbSummaryColumnWidths.Length) - 1; j >= 0; j--)
                        gbSummaryColumnWidths[j] = gbCellColumnWidths[j];
                }                

                this.CalcLayoutHelper();
            }
            finally
            {
                // Reset the anti-recursion flag.
                //
                this.inVerifyRowLayoutCache = false;
            }
        }             

        #endregion // VerifyRowLayoutCache


        #region GroupLayoutColumnsEnumerable
        private class GroupLayoutColumnsEnumerable : IEnumerable<UltraGridColumn>
        {
            private UltraGridGroup parentGroup;

            public GroupLayoutColumnsEnumerable(UltraGridGroup parentGroup)
            {
                this.parentGroup = parentGroup;
            }

            #region IEnumerable Members

            IEnumerator<UltraGridColumn> IEnumerable<UltraGridColumn>.GetEnumerator()
            {
                if (parentGroup.Band != null)
                {
                    foreach (HeaderBase header in parentGroup.Band.OrderedColumnHeaders)
                    {
                        UltraGridColumn column = header.Column;

                        if (column == null || column.RowLayoutColumnInfo.ParentGroup != this.parentGroup)
                            continue;

                        yield return column;
                    }
                }
            }

            #endregion

            #region IEnumerable Members

            IEnumerator IEnumerable.GetEnumerator()
            {
                foreach (UltraGridColumn column in this)
                    yield return column;
            }

            #endregion
        }
        #endregion // GroupLayoutColumnsEnumerable

        #region GroupLayoutChildGroupsEnumerable
        private class GroupLayoutChildGroupsEnumerable : IEnumerable<UltraGridGroup>
        {
            private UltraGridGroup parentGroup;

            public GroupLayoutChildGroupsEnumerable(UltraGridGroup parentGroup)
            {
                this.parentGroup = parentGroup;
            }

            #region IEnumerable Members

            IEnumerator<UltraGridGroup> IEnumerable<UltraGridGroup>.GetEnumerator()
            {                
                if (parentGroup.Band != null)
                {
                    foreach (HeaderBase header in parentGroup.Band.OrderedGroupHeaders)
                    {
                        UltraGridGroup group = header.Group;

                        if (group == null || group.RowLayoutGroupInfo.ParentGroup != this.parentGroup)
                            continue;

                        yield return group;
                    }
                }
            }

            #endregion

            #region IEnumerable Members

            IEnumerator IEnumerable.GetEnumerator()
            {
                foreach (UltraGridGroup group in this)
                    yield return group;
            }

            #endregion
        }
        #endregion // GroupLayoutChildGroupsEnumerable

        #region GroupLayoutInfo class

        /// <summary>
        /// Class for holding row layout information for a group.
        /// </summary>
        [Serializable(), TypeConverter(typeof(ExpandableObjectConverter))]
        public class GroupLayoutInfo : RowLayoutColumnInfo
        {
            internal GroupLayoutInfo(UltraGridGroup group)
                : base(group)
            {
            }

            /// <summary>
            /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
            /// </summary>
            /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
            /// <param name="context">Context for the deserialization</param>
            protected GroupLayoutInfo(SerializationInfo info, StreamingContext context)
                : base(info, context)
            {
            }

            #region Clone

            // SSP 8/2/05 - NAS 5.3 Group Swapping in Row-Layout
            // 
            internal override RowLayoutColumnInfo Clone()
            {
                GroupLayoutInfo ci = new GroupLayoutInfo(this.Group);
                ci.InitializeFrom(this);
                return ci;
            }

            #endregion // Clone
        }

        #endregion // GroupLayoutInfo  class        

        #region IProvideRowLayoutColumnInfo Members

        #region Band
        UltraGridBand IProvideRowLayoutColumnInfo.Band
        {
            get { return this.Band; }
        }
        #endregion //Band

        #region ContentLayoutItem
        ILayoutItem IProvideRowLayoutColumnInfo.ContentLayoutItem
        {
            get { return this.GroupLayoutItemCellContent; }
        }
        #endregion //ContentLayoutItem

        #region Header
        HeaderBase IProvideRowLayoutColumnInfo.Header
        {
            get { return this.Header; }
        }
        #endregion //Header

        #region HeaderConstraint
        IGridBagConstraint IProvideRowLayoutColumnInfo.HeaderConstraint
        {
            get { return this.GroupLayoutItemHeaderContent; }
        }
        #endregion //HeaderConstraint

        #region HeaderLayoutItem
        ILayoutItem IProvideRowLayoutColumnInfo.HeaderLayoutItem
        {
            get { return this.Header; }
        }
        #endregion //HeaderLayoutItem

        #region LabelSpanSize
        internal Size LabelSpanSize
        {
            get
            {
                int x = 0;
                int y = 0;

                switch (this.RowLayoutGroupInfo.LabelPositionResolved)
                {
                    case LabelPosition.Top:
                    case LabelPosition.Bottom:
                        y = this.RowLayoutGroupInfo.LabelSpan;
                        break;
                    case LabelPosition.Left:
                    case LabelPosition.Right:
                        x = this.RowLayoutGroupInfo.LabelSpan;
                        break;
                    case LabelPosition.None:
                    case LabelPosition.LabelOnly:
                        break;
                    case LabelPosition.Default:
                    default:
                        Debug.Fail("Unexpected LabelPosition");
                        break;
                }

                return new Size(x, y);
            }
        }
        #endregion //LabelSpanSize

        #region MinimumHeaderSize
        Size IProvideRowLayoutColumnInfo.MinimumHeaderSize
        {
            get { return this.CalculatePreferredGroupHeaderSize(); }
        }
        #endregion //MinimumHeaderSize

        #region MinimumSize
        Size IProvideRowLayoutColumnInfo.MinimumSize
        {
            get { return this.MinimumSize; }
        }
        #endregion //MinimumSize

        #region MinWidth
        int IProvideRowLayoutColumnInfo.MinWidth
        {
            get
            {
                
                return 0;
            }
        }
        #endregion //MinWidth

        #region PreferredHeaderSize
        Size IProvideRowLayoutColumnInfo.PreferredHeaderSize
        {
            get
            {                
                // First, get the property setting. 
                Size size = this.RowLayoutGroupInfo.PreferredLabelSize;

                LabelPosition labelPosition = this.RowLayoutGroupInfo.LabelPositionResolved;

                // If the property setting is default, then use the calculated layout width if the
                // label it on the top of the bottom. 
                if (size.Width == 0 &&
                    (labelPosition == LabelPosition.Top || labelPosition == LabelPosition.Bottom))
                {
                    size.Width = this.PreferredHeaderLayoutSize.Width;
                }

                if (size.Height == 0 &&
                    (labelPosition == LabelPosition.Left || labelPosition == LabelPosition.Right))
                {
                    size.Height = this.PreferredHeaderLayoutSize.Height;
                }

                if (size.Width <= 0)
                    size.Width = this.CalculatePreferredGroupHeaderSize().Width;

                if (size.Height <= 0)
                {
                    size.Height = this.Header.Height;

                    if (this.Band.CardView && this.Band.AreColumnHeadersInSeparateLayoutArea)
                        size.Height = Math.Max(size.Height, this.Header.Height);
                }

                // SSP 6/30/03 UWG2375
                // Return the greater of preferred size and the minimum size.
                //
                Size minSize = this.RowLayoutGroupInfo.MinimumLabelSize;
                size.Width = Math.Max(size.Width, minSize.Width);
                size.Height = Math.Max(size.Height, minSize.Height);

                Debug.Assert(size.Width != 0 && size.Height != 0, "0 size returned from a group header.");
                return size;
            }
        }
        #endregion //PreferredHeaderSize

        #region PreferredSize
        Size IProvideRowLayoutColumnInfo.PreferredSize
        {
            get 
            {
                // First, get the property setting. 
                Size size = this.RowLayoutGroupInfo.PreferredCellSize;

                LabelPosition labelPosition = this.RowLayoutGroupInfo.LabelPositionResolved;

                // If the property setting is default, then use the calculated layout width if the
                // label it on the top of the bottom. 
                if (size.Width == 0 &&
                    (labelPosition == LabelPosition.Top || labelPosition == LabelPosition.Bottom))
                {
                    size.Width = this.PreferredRowLayoutSize.Width;                    
                }

                if (size.Width <= 0)
                    size.Width = this.CalculatePreferredGroupHeaderSize().Width;

                if (size.Height == 0 &&
                    (labelPosition == LabelPosition.Left || labelPosition == LabelPosition.Right))
                {
                    size.Height = this.PreferredRowLayoutSize.Height;
                }                

                if (size.Height <= 0)
                {
                    size.Height = this.Header.Height;

                    if (this.Band.CardView && this.Band.AreColumnHeadersInSeparateLayoutArea)
                        size.Height = Math.Max(size.Height, this.Header.Height);
                }

                // SSP 6/30/03 UWG2375
                // Return the greater of preferred size and the minimum size.
                //
                Size minSize = this.RowLayoutGroupInfo.MinimumCellSize;
                size.Width = Math.Max(size.Width, minSize.Width);
                size.Height = Math.Max(size.Height, minSize.Height);

                Debug.Assert(size.Width != 0 && size.Height != 0, "0 size returned from a group.");
                return size;
            }
        }
        #endregion //PreferredSize

        #region ResizeLayoutItem
        ILayoutItem IProvideRowLayoutColumnInfo.ResizeLayoutItem
        {
            get 
            { 
                // Find a column on the resizing edge of the group.
                UltraGridBand band = this.Band;                
                UltraGridBase grid = band.Layout.Grid;
                bool areColumnHeadersInSeparateLayoutArea = band.AreColumnHeadersInSeparateLayoutArea;

                GridBagLayoutManager gblm = areColumnHeadersInSeparateLayoutArea
                    ? ((Infragistics.Win.UltraWinGrid.Design.IGridDesignInfo)grid).GetHeaderAreaGridBagLayoutManager(band)
                    : ((Infragistics.Win.UltraWinGrid.Design.IGridDesignInfo)grid).GetCellAreaGridBagLayoutManager(band);

                IGridBagConstraint groupConstraint = (IGridBagConstraint)gblm.LayoutItems.GetConstraint(this.Header);
                
                if (groupConstraint == null)
                {
                    Debug.Fail("The group constraint could not be found. That would seem to indicate that the group header is not visible. So why is someone asking for the resize item for the group?");
                    return this.GroupLayoutItemCellContent; 
                }

                bool cardView = band.CardView;
                int groupEdge = cardView
                    ? groupConstraint.OriginY + groupConstraint.SpanY
                    : groupConstraint.OriginX + groupConstraint.SpanX;

                foreach (ILayoutItem layoutItem in gblm.LayoutItems)
                {
                    IGridBagConstraint testConstraint = (IGridBagConstraint)gblm.LayoutItems.GetConstraint(layoutItem);
                    int testEdge = cardView
                        ? testConstraint.OriginY + testConstraint.SpanY
                        : testConstraint.OriginX + testConstraint.SpanX;

                    if (testEdge == groupEdge)
                    {
                        ColumnHeader columnHeader = layoutItem as ColumnHeader;
                        if (columnHeader != null)
                            return columnHeader.Column;

                        UltraGridColumn column = layoutItem as UltraGridColumn;
                        if (column != null)
                            return column;
                    }
                }

                Debug.Fail("Failed to find a column in the same edge as the group.");
                return this.GroupLayoutItemCellContent; 
            }
        }

        #endregion //ResizeLayoutItem

        #region RowLayoutColumnInfo
        RowLayoutColumnInfo IProvideRowLayoutColumnInfo.RowLayoutColumnInfo
        {
            get { return this.RowLayoutGroupInfo; }
        }
        #endregion // RowLayoutColumnInfo                

        #region SetCachedLayoutItemDimensions
        private void SetCachedLayoutItemDimensions(UltraGridGroup[] groupsArr)
        {
            this.SetCachedSpans();

            // For each child group, set the Origin now. 
            for (int i = 0; i < groupsArr.Length; i++)
            {
                UltraGridGroup group = groupsArr[i];
                RowLayoutColumnInfo ci = group.RowLayoutGroupInfo;
                ci.cachedOriginXResolved = this.layoutItemDimensions[ci].OriginX;
                ci.cachedOriginYResolved = this.layoutItemDimensions[ci].OriginY;
            }

            this.RowLayoutGroupInfo.shouldPositionCellElement = LabelPosition.LabelOnly != this.RowLayoutGroupInfo.LabelPositionResolved;
            this.RowLayoutGroupInfo.shouldPositionLabelElement = LabelPosition.None != this.RowLayoutGroupInfo.LabelPositionResolved;
        }
        #endregion //SetCachedLayoutItemDimensions

        #region SetCachedSpans
        private void SetCachedSpans()
        {
            this.RowLayoutGroupInfo.cachedSpanXResolved = this.gridBagLayoutManagerHelper.ColumnWidths.Length;
            this.RowLayoutGroupInfo.cachedSpanYResolved = this.gridBagLayoutManagerHelper.RowHeights.Length;

            switch (this.RowLayoutGroupInfo.LabelPositionResolved)
            {
                case LabelPosition.Top:
                case LabelPosition.Bottom:
                    this.RowLayoutGroupInfo.cachedSpanYResolved += this.RowLayoutGroupInfo.LabelSpan;
                    break;
                case LabelPosition.Left:
                case LabelPosition.Right:
                    this.RowLayoutGroupInfo.cachedSpanXResolved += this.RowLayoutGroupInfo.LabelSpan;
                    break;
                case LabelPosition.None:
                    break;                
                case LabelPosition.LabelOnly:
                    // MRS 3/6/2009 - TFS15039
                    this.RowLayoutGroupInfo.cachedSpanYResolved = this.RowLayoutGroupInfo.LabelSpan;
                    this.RowLayoutGroupInfo.cachedSpanXResolved = this.RowLayoutGroupInfo.SpanX;

                    break;
                case LabelPosition.Default:
                default:
                    Debug.Fail("Unexpected LabelPosition");
                    break;
            }

            if (this.RowLayoutGroupInfo.cachedSpanXResolved <= 0)
                this.RowLayoutGroupInfo.cachedSpanXResolved = this.RowLayoutGroupInfo.SpanX;
            if (this.RowLayoutGroupInfo.cachedSpanYResolved <= 0)
                this.RowLayoutGroupInfo.cachedSpanYResolved = this.RowLayoutGroupInfo.SpanY;

            if (this.RowLayoutGroupInfo.cachedSpanXResolved <= 0)
                this.RowLayoutGroupInfo.cachedSpanXResolved = 1;
            if (this.RowLayoutGroupInfo.cachedSpanYResolved <= 0)
                this.RowLayoutGroupInfo.cachedSpanYResolved = 1;
        }
        #endregion //SetCachedSpans

        #endregion // IProvideRowLayoutColumnInfo Members

        #region ILayoutGroup Members

        /// <summary>
        /// Returns the resolved GridBagConstraints for the items in the header. 
        /// </summary>
        /// <param name="allConstraints"></param>
        /// <returns></returns>
        Hashtable ILayoutGroup.GetResolvedGCs(Hashtable allConstraints)
        {
            UltraGrid grid = (UltraGrid)this.Band.Layout.Grid;
            List<ILayoutItem> layoutItems = ((ILayoutGroup)this).GetVisibleLayoutItems();

            Hashtable hash = new Hashtable();
            foreach (ILayoutItem layoutItem in layoutItems)
            {
                GridBagConstraint constraint = (GridBagConstraint)allConstraints[layoutItem];
                if (constraint != null)
                    hash[layoutItem] = constraint;
                else
                    Debug.Fail("Failed to get the constraint for the layoutItem.");
            }

            return hash;
        }

        /// <summary>
        /// Returns the visible items within the group. Does not include the group header. 
        /// </summary>
        /// <returns></returns>
        List<ILayoutItem> ILayoutGroup.GetVisibleLayoutItems()
        {
            UltraGrid grid = (UltraGrid)this.Band.Layout.Grid;
            ILayoutItem[] layoutItems = grid.gridBagLayoutDragStrategy.GridBagLayoutDragManager.GetVisibleLayoutItems();
            
            List<ILayoutItem> relevantLayoutItems = new List<ILayoutItem>();
            
            foreach (ILayoutItem layoutItem in layoutItems)
            {
                ILayoutChildItem layoutChildItem = layoutItem as ILayoutChildItem;
                Debug.Assert(layoutChildItem != null, "The layoutItem is not a LayoutChildItem; unexpected");
                ILayoutGroup layoutGroup = layoutChildItem.ParentGroup;
                if (this == layoutGroup)
                    relevantLayoutItems.Add(layoutItem);
            }

            return relevantLayoutItems;
        }

        /// <summary>
        /// Returns an ILayoutItem representing the group. 
        /// </summary>
        /// <returns></returns>
        ILayoutItem ILayoutGroup.GetLayoutItem()
        {
            UltraGrid grid = (UltraGrid)this.Band.Layout.Grid;
            return ((GridBagLayoutDragManager)grid.gridBagLayoutDragStrategy.GridBagLayoutDragManager).GetLayoutItem(this);
        }

        /// <summary>
        /// Returns the label span size of the group. 
        /// </summary>
        Size ILayoutGroup.LabelSpanSize 
        { 
            get {return this.LabelSpanSize;}
        }

		/// <summary>
		/// Converts a client origin relative to the group into an absolute origin.
		/// </summary>
		Point ILayoutGroup.OriginToAbsolute( Point clientOrigin, UIElement containerElement )
		{
			return UltraGridGroup.ConvertOrigin( this, clientOrigin, containerElement, true );
		}

		/// <summary>
		/// Converts an absolute origin into a client origin relative to the group.
		/// </summary>
		Point ILayoutGroup.OriginToClient( Point absoluteOrigin, UIElement containerElement )
		{
			return UltraGridGroup.ConvertOrigin( this, absoluteOrigin, containerElement, false );
		}

		private static Point ConvertOrigin( UltraGridGroup group, Point originalOrigin, UIElement containerElement, bool convertToAbsolute )
		{
			// MD 2/19/09 - TFS14276
			// This flag could change with each group based on its label position, 
			// so we will reevaluate it with each group as we walk up.
			//bool includeLabelSize = UltraGridGroup.ShouldIncludeLabelSize( group, containerElement );

			int offsetX = 0;
			int offsetY = 0;

			while ( group != null )
			{
				RowLayoutColumnInfo rowLayoutColumnInfo = group.RowLayoutGroupInfo;

				offsetX += rowLayoutColumnInfo.OriginXResolved;
				offsetY += rowLayoutColumnInfo.OriginYResolved;

				// MD 2/19/09 - TFS14276
				// This flag could change with each group based on its label position, 
				// so reevaluate it with each group as we walk up.
				//if ( includeLabelSize )
				if ( UltraGridGroup.ShouldIncludeLabelSize( group, containerElement ) )
				{
					switch ( rowLayoutColumnInfo.LabelPositionResolved )
					{
						case LabelPosition.Top:
							offsetY += rowLayoutColumnInfo.LabelSpan;
							break;
						case LabelPosition.Left:
							offsetX += rowLayoutColumnInfo.LabelSpan;
							break;
					}
				}

				group = group.RowLayoutGroupInfo.ParentGroup;
			}

			if ( convertToAbsolute )
			{
				offsetX *= -1;
				offsetY *= -1;
			}

			return new Point(
				originalOrigin.X - offsetX,
				originalOrigin.Y - offsetY
				);
		}

		private static bool ShouldIncludeLabelSize( UltraGridGroup group, UIElement containerElement )
		{
			if ( containerElement is BandHeadersUIElement )
				return true;

			// MD 2/19/09 - TFS14276
			// If the container element is the card label area, we should also include the label size.
			if ( containerElement is CardLabelAreaUIElement )
				return true;

			// There is only one other type we are expecting at this point
			Debug.Assert( containerElement is RowCellAreaUIElement, "Unexpected container element." );

			// If the group header should be shown in the cell only area, return true.
			if ( UltraGridBand.ShouldAddGroupHeaderToCellArea( group ) )
				return true;

			UltraGridBand band = group.Band;

			if ( band == null )
				return true;

			UltraGridLayout layout = band.Layout;

			if ( layout == null )
				return true;

			UltraGrid grid = layout.Grid as UltraGrid;

			if ( grid == null || grid.gridBagLayoutDragStrategy == null )
				return true;

			GridBagLayoutDragManager manager = grid.gridBagLayoutDragStrategy.GridBagLayoutDragManager as GridBagLayoutDragManager;

			if ( manager == null )
				return true;

			return ( manager.AreColumnHeadersInSeparateArea == false );
		}

        #endregion ILayoutGroup Members

        #endregion // NAS 9.1 - Groups in RowLayout
    }

    #region GroupLayoutItemBase
    internal abstract class GroupLayoutItemBase : ILayoutChildItem, IGridBagConstraint
    {
        #region Private Members
        private UltraGridGroup group;
        private bool firstItem;
        #endregion //Private Members

        #region Constructor
        public GroupLayoutItemBase(UltraGridGroup group)
        {
            this.group = group;
        }
        #endregion //Constructor

        #region Abstract Properties

        internal abstract bool AdjustOriginXForLabel { get; }
        internal abstract bool AdjustOriginYForLabel { get; }
        internal abstract bool AdjustSpanXForLabel { get; }
        internal abstract bool AdjustSpanYForLabel { get; }
        internal abstract LayoutManagerBase LayoutManager { get; }

        #endregion //Abstract Properties

        #region Public Properties

        #region Band
        public UltraGridBand Band
        {
            get { return this.group.Band; }
        }
        #endregion //Band

        #region FirstItem
        public bool FirstItem
        {
            get { return this.firstItem; }
            set { this.firstItem = value; }
        }
        #endregion //FirstItem

        #region Group
        public UltraGridGroup Group
        {
            get { return this.group; }
        }
        #endregion //Group

        #region MinimumSize

        internal Size MinimumSize
        {
            get
            {
                LayoutContainerCalcSize layoutContainerCalcSize = group.GetCachedLayoutContainerCalcSize();
                return this.LayoutManager.CalculateMinimumSize(layoutContainerCalcSize, null);
            }
        }

        #endregion // MinimumSize

        #region PreferredSize

        internal Size PreferredSize
        {
            get
            {
                LayoutContainerCalcSize layoutContainerCalcSize = group.GetCachedLayoutContainerCalcSize();
                Size minSize = this.LayoutManager.CalculateMinimumSize(layoutContainerCalcSize, null);

                layoutContainerCalcSize = group.GetCachedLayoutContainerCalcSize();
                Size preferredSize = this.LayoutManager.CalculatePreferredSize(layoutContainerCalcSize, null);

                preferredSize.Width = Math.Max(preferredSize.Width, minSize.Width);
                preferredSize.Height = Math.Max(preferredSize.Height, minSize.Height);

                return preferredSize;
            }
        }

        #endregion // PreferredSize

        #region RowLayoutGroupInfo
        public RowLayoutColumnInfo RowLayoutGroupInfo
        {
            get { return this.group.RowLayoutGroupInfo; }
        }
        #endregion //RowLayoutGroupInfo

        #endregion //Public Properties

        #region Implementation of ILayoutItem

        #region ILayoutItem.MinimumSize

        Size ILayoutItem.MinimumSize
        {
            get
            {
                return this.MinimumSize;
            }
        }

        #endregion // ILayoutItem.MinimumSize

        #region ILayoutItem.PreferredSize

        Size ILayoutItem.PreferredSize
        {
            get
            {
                // SSP 11/17/04
                // Added a way to hide the header or the cell of a group in the row layout mode.
                // This way you can do things like hide a row of headers or add unbound columns
                // and hide their headers to show grouping headers etc...
                //
                if (LabelPosition.LabelOnly == this.RowLayoutGroupInfo.LabelPositionResolved)
                {
                    // SSP 8/16/05 BR05387
                    // In order to make sure that the widths are distributed among logical columns
                    // in the same manner in both the header layout manager and the cell layout
                    // manager when LabelPosition is as such that either the headers or the headers
                    // are hidden, then make sure one of the width or height dimensions are the
                    // same depending on card-view or non-card-view.
                    // 
                    //return new Size( 0, 0 );
                    return LayoutUtility.AccountForLabelPosition(this.Band, ((ILayoutItem)group.Header).PreferredSize);
                }

                return this.PreferredSize;
            }
        }

        #endregion // ILayoutItem.PreferredSize

        #region ILayoutItem.IsVisible

        /// <summary>
        /// Indicates whether this group is visible.
        /// </summary>
        bool ILayoutItem.IsVisible
        {
            get
            {
                if (!group.IsVisibleInLayout)
                    return false;

                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutGroupInfo;

                if (!this.Band.AreColumnHeadersInSeparateLayoutArea && ci.LabelSpanResolved > 0)
                {
                    LabelPosition labelPosition = ci.LabelPositionResolved;
                    if (LabelPosition.Left == labelPosition || LabelPosition.Right == labelPosition)
                    {
                        if (ci.SpanXResolved - ci.LabelSpanResolved <= 0)
                            return false;
                    }
                    else if (LabelPosition.Top == labelPosition || LabelPosition.Bottom == labelPosition)
                    {
                        if (ci.SpanYResolved - ci.LabelSpanResolved <= 0)
                            return false;
                    }
                }

                return true;
            }
        }

        #endregion // ILayoutItem.IsVisible

        #endregion

        #region IGridBagConstraint interface implementation

        #region Anchor

        /// <summary>
        /// If the display area of the item is larger than the item, this property indicates where to anchor the item.
        /// </summary>
        AnchorType IGridBagConstraint.Anchor
        {
            get
            {
                return AnchorType.Center;
            }
        }

        #endregion // Anchor

        #region Fill

        /// <summary>
        /// <p>Fill indicates whether to resize the item to fill the extra spance if the layout item's display area is larger than its size,</p> 
        /// </summary>
        FillType IGridBagConstraint.Fill
        {
            get
            {
                return Infragistics.Win.Layout.FillType.Both;
            }
        }

        #endregion // Fill

        #region Insets

        /// <summary>
        /// Indicates the padding around the layout item.
        /// </summary>
        Insets IGridBagConstraint.Insets
        {
            get
            {
                // Return an empty inset which is what the group's RowLayoutColumnInfo retruns.
                //
                // SSP 12/1/03 UWG2641
                // Added CellInsets and LabelInsets properties.
                //
                //return this.RowLayoutColumnInfo.Insets;
                return this.RowLayoutGroupInfo.CellInsets;
            }
        }

        #endregion // Insets        

        #region OriginX

        /// <summary>
        /// <p>OriginX and OriginY define where the layout item will be placed in the virtual grid of the grid-bag layout. OriginX specifies the location horizontally while specifies the location vertically. These locations are the coordinates of the headers in the virtual grid that the grid-bag layout represents.</p>
        /// <p>The leftmost cell has OriginX of 0. The constant <see cref="GridBagConstraintConstants.Relative"/> specifies that the item be placed just to the right of the item that was added to the layout manager just before this item was added. </p>
        /// <p>The default value is <see cref="GridBagConstraintConstants.Relative"/>. OriginX should be a non-negative value.</p>
        /// </summary>
        int IGridBagConstraint.OriginX
        {
            get
            {
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutGroupInfo;

                if (this.AdjustOriginXForLabel)
                    return ci.OriginXResolved + ci.LabelSpanResolved;

                return ci.OriginXResolved;
            }
        }

        #endregion // OriginX

        #region OriginY

        /// <summary>
        /// <p>OriginX and OriginY define where the layout item will be placed in the virtual grid of the grid-bag layout. OriginX specifies the location horizontally while specifies the location vertically. These locations are the coordinates of the headers in the virtual grid that the grid-bag layout represents.</p>
        /// <p>The topmost cell has OriginY of 0. The constant <see cref="GridBagConstraintConstants.Relative"/> specifies that the item be placed just below the item that was added to the layout manager just before this item was added.</p>
        /// <p>The default value is <see cref="GridBagConstraintConstants.Relative"/>. OriginY should be a non-negative value.</p>
        /// </summary>
        int IGridBagConstraint.OriginY
        {
            get
            {
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutGroupInfo;


                if (this.AdjustOriginYForLabel)
                    return ci.OriginYResolved + ci.LabelSpanResolved;

                return ci.OriginYResolved;
            }
        }

        #endregion // OriginY

        #region SpanX

        /// <summary>
        /// <p>Specifies the number of headers this item will span horizontally. The constant <see cref="GridBagConstraintConstants.Remainder"/> specifies that this item be the last one in the row and thus occupy remaining space.</p>
        /// </summary>
        int IGridBagConstraint.SpanX
        {
            get
            {
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutGroupInfo;
                
                if (this.AdjustSpanXForLabel)
                    return ci.SpanXResolved - ci.LabelSpanResolved;

                return ci.SpanXResolved;
            }
        }

        #endregion // SpanX

        #region SpanY

        /// <summary>
        /// <p>Specifies the number of headers this item will span vertically. The constant <see cref="GridBagConstraintConstants.Remainder"/> specifies that this item be the last one in the group and thus occupy remaining space.</p>
        /// </summary>
        int IGridBagConstraint.SpanY
        {
            get
            {
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutGroupInfo;

                if (this.AdjustSpanYForLabel)
                {
                    return ci.SpanYResolved - ci.LabelSpanResolved;
                }

                return ci.SpanYResolved;
            }
        }

        #endregion // SpanY

        #region WeightX

        /// <summary>
        /// Indicates how the extra horizontal space will be distributed among items. Default value is 0.0. Higher values give higher priority. The weight of the group in the virtual grid the grid-bag layout represents is the maximum WeightX of all the items in the row.
        /// </summary>
        float IGridBagConstraint.WeightX
        {
            get
            {
                return this.RowLayoutGroupInfo.WeightX;
            }
        }

        #endregion // WeightX

        #region WeightY

        /// <summary>
        /// Indicates how the extra vertical space will be distributed among items. Default value is 0.0. Higher values give higher priority. The weight of the group in the virtual grid the grid-bag layout represents is the maximum WeightY of all the items in the group.
        /// </summary>
        float IGridBagConstraint.WeightY
        {
            get
            {
                return this.RowLayoutGroupInfo.WeightY;
            }
        }

        #endregion // WeightY

        #endregion // IGridBagConstraint interface implementation

        #region ILayoutChildItem Members

		// MD 2/17/09 - TFS14116
		// With the original fix for TFS13834, columns cannot be moved to new groups by default at design-time. The has been changed 
		// to allow for different behavior at run-time and design-time.
		//// MRS 2/13/2009 - TFS13834
		///// <summary>
		///// Returns whether the item is allowed to be moved to another group via drag / drop.
		///// </summary>
		//bool ILayoutChildItem.AllowParentGroupChange
		//{
		//    get
		//    {
		//        return this.group.AllowParentGroupChangeResolved;
		//    }
		//}

        /// <summary>
        /// Returns the group associated with this LayoutItem, or null, if the layoutitem does not represent a group. 
        /// </summary>
        ILayoutGroup ILayoutChildItem.AssociatedGroup
        {
            get { return ((ILayoutChildItem)this.Group.Header).AssociatedGroup; }
        }

        /// <summary>
        /// The ILayoutGroup which contains this ILayoutChildItem, or null if it is not contained in a group. 
        /// </summary>
        ILayoutGroup ILayoutChildItem.ParentGroup
        {
            get { return ((ILayoutChildItem)this.Group.Header).ParentGroup; }
        }

        /// <summary>
        /// Returns the group into which an item will be dropped relative to this element.
        /// </summary>
        /// <param name="dropLocation"></param>
        /// <returns></returns>
        /// <remarks>
        /// This methods takes a DropLocation and determines what group an item dropped at that location 
        /// should be dropped into. If the item is a content, then the group is the parent group
        /// of that content. If the item is a group header, then the item may be dropped inside the group
        /// or into it's parent, depending on the DropLocation relative to the position of the header. 
        /// For example, if the group header is on top, dropping an item on the bottom of the header 
        /// should place that item inside the group while dropping the item to the left, right, or top
        /// of the header should place the item as a sibling of the group. 
        /// </remarks>
        ILayoutGroup ILayoutChildItem.GetDropGroup(GridBagLayoutDragStrategy.DropLocation dropLocation)
        {
            return ((ILayoutChildItem)this.Group.Header).GetDropGroup(dropLocation);
        }

        #endregion // ILayoutChildItem Members
    }
    #endregion //GroupLayoutItemBase

    #region GroupLayoutItemHeaderContent class
    internal class GroupLayoutItemHeaderContent : GroupLayoutItemBase
    {
        #region Constructor
        public GroupLayoutItemHeaderContent(UltraGridGroup group) : base(group) { }
        #endregion //Constructor               
    
        #region Overrides

        #region AdjustOriginXForLabel
        internal override bool AdjustOriginXForLabel
        {
            get 
            {                
                return LabelPosition.Left == this.RowLayoutGroupInfo.LabelPositionResolved;  
            }
        }
        #endregion //AdjustOriginXForLabel

        #region AdjustOriginYForLabel
        internal override bool AdjustOriginYForLabel
        {
            get
            {
                return LabelPosition.Top == this.RowLayoutGroupInfo.LabelPositionResolved;
            }
        }
        #endregion //AdjustOriginYForLabel

        #region AdjustSpanXForLabel
        internal override bool AdjustSpanXForLabel
        {
            get
            {
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutGroupInfo;

                return LabelPosition.Left == ci.LabelPositionResolved ||
                    LabelPosition.Right == ci.LabelPositionResolved;
            }
        }
        #endregion //AdjustSpanXForLabel

        #region AdjustSpanYForLabel
        internal override bool AdjustSpanYForLabel
        {
            get
            {
                Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo ci = this.RowLayoutGroupInfo;

                return LabelPosition.Top == ci.LabelPositionResolved ||
                    LabelPosition.Bottom == ci.LabelPositionResolved;
            }
        }
        #endregion //AdjustSpanYForLabel        

        #region LayoutManager
        internal override LayoutManagerBase LayoutManager
        {
            get { return this.Group.HeaderLayoutManager; }
        }
        #endregion //LayoutManager

        #endregion //Overrides
    }
    #endregion //GroupLayoutItemHeaderContent class

    #region GroupLayoutItemCellContent class
    internal class GroupLayoutItemCellContent : GroupLayoutItemBase, IGridBagConstraint
    {
        #region Constructor
        public GroupLayoutItemCellContent(UltraGridGroup group) : base(group) {}
        #endregion //Constructor

        #region Overrides

        #region AdjustOriginXForLabel
        internal override bool AdjustOriginXForLabel
        {
            get
            {
                bool columnHeadersAreInSeparateLayoutArea = this.Band.AreColumnHeadersInSeparateLayoutArea;
                bool cardView = this.Band.CardView;
                LabelPosition labelPosition = this.RowLayoutGroupInfo.LabelPositionResolved;
                if (columnHeadersAreInSeparateLayoutArea)
                {
                    if (false == cardView)
                    {
                        if (labelPosition == LabelPosition.Left)
                            return true;
                    }

                    return false;
                }
                else
                {
                    return labelPosition == LabelPosition.Left;
                }
            }
        }
        #endregion //AdjustOriginXForLabel

        #region AdjustOriginYForLabel
        internal override bool AdjustOriginYForLabel
        {
            get
            {
                bool columnHeadersAreInSeparateLayoutArea = this.Band.AreColumnHeadersInSeparateLayoutArea;
                bool cardView = this.Band.CardView;
                LabelPosition labelPosition = this.RowLayoutGroupInfo.LabelPositionResolved;
                if (columnHeadersAreInSeparateLayoutArea)
                {
                    if (cardView)
                    {
                        if (labelPosition == LabelPosition.Top)
                            return true;
                    }

                    return false;
                }
                else
                {
                    return labelPosition == LabelPosition.Top;
                }
            }
        }
        #endregion //AdjustOriginYForLabel

        #region AdjustSpanXForLabel
        internal override bool AdjustSpanXForLabel
        {
            get
            {
                bool columnHeadersAreInSeparateLayoutArea = this.Band.AreColumnHeadersInSeparateLayoutArea;
                bool cardView = this.Band.CardView;
                LabelPosition labelPosition = this.RowLayoutGroupInfo.LabelPositionResolved;
                if (columnHeadersAreInSeparateLayoutArea)
                {
                    if (false == cardView)
                    {
                        if (labelPosition == LabelPosition.Left ||
                            labelPosition == LabelPosition.Right)
                            return true;
                    }

                    return false;
                }
                else
                {
                    return labelPosition == LabelPosition.Left ||
                            labelPosition == LabelPosition.Right;
                }
            }
        }
        #endregion //AdjustSpanXForLabel

        #region AdjustSpanYForLabel
        internal override bool AdjustSpanYForLabel
        {
            get
            {
                bool columnHeadersAreInSeparateLayoutArea = this.Band.AreColumnHeadersInSeparateLayoutArea;
                bool cardView = this.Band.CardView;
                LabelPosition labelPosition = this.RowLayoutGroupInfo.LabelPositionResolved;
                if (columnHeadersAreInSeparateLayoutArea)
                {
                    if (cardView)
                    {
                        if (labelPosition == LabelPosition.Top ||
                            labelPosition == LabelPosition.Bottom)
                            return true;
                    }

                    return false;
                }
                else
                {
                    return labelPosition == LabelPosition.Top ||
                            labelPosition == LabelPosition.Bottom;
                }
            }
        }
        #endregion //AdjustSpanYForLabel

        #region LayoutManager
        internal override LayoutManagerBase LayoutManager
        {
            get { return this.Group.RowLayoutManager; }
        }
        #endregion //LayoutManager       

        #endregion //Overrides
    }
    #endregion //GroupLayoutItemCellContent class

    #region GroupLayoutItemSummaryContent class
    internal class GroupLayoutItemSummaryContent : GroupLayoutItemCellContent
    {
        private LayoutManagerBase layoutManager;

        #region Constructor
        public GroupLayoutItemSummaryContent(UltraGridGroup group) : base(group) 
        {
            this.layoutManager = null;
        }

        public GroupLayoutItemSummaryContent(UltraGridGroup group, LayoutManagerBase layoutManager)
            : base(group)
        {
            this.layoutManager = layoutManager;
        }
        #endregion //Constructor

        #region Overrides

        #region LayoutManager
        internal override LayoutManagerBase LayoutManager
        {
            get 
            { 
                if (this.layoutManager != null)
                    return this.layoutManager;

                return this.Group.SummaryLayoutManager;
            }
        }
        #endregion //LayoutManager

        #endregion //Overrides
    }
    #endregion //GroupLayoutItemSummaryContent class
}
