#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using Infragistics.Win.UltraWinMaskedEdit;

    /// <summary>
    /// This class represents a cell in the grid (a specific column
    /// in a specific row)
    /// </summary>
	// SSP 4/16/05 - NAS 5.2 Filter Row
	// Created UltraGridFilterCell class.
	//
	//sealed public class UltraGridCell : ActivateableGridItemBase, IKeyedSubObjectEx
	public class UltraGridCell : ActivateableGridItemBase, IKeyedSubObjectEx
	{
		/// <summary>
		/// column this cell belongs to
		/// </summary>
		private UltraGridColumn column;

		private Infragistics.Win.Appearance appearance=null;
		private bool dataChanged = false;

		// SSP 11/17/05 BR07772
		// If the column is bound then we need to honor the UpdateData method
		// call even if modifications were made via the calc manager.
		// 
		private bool dataChangedViaCalcManager = false;

		// SSP 4/26/02
		// EM Embeddable editors.
		//
		//private bool droppedDown = false;

		private bool isInEditMode;
		private Activation activation = 0;
		private DefaultableBoolean tabStop = 0;
		private object originalValue = null;
		//private object objvalue = null;
		
		// SSP 8/15/02 UWG1574
		// Implemented.
		// 
		//private int selStart = -1;
		//private int selLength = -1; 		
		//private string selText = null; 
		
		// SSP 7/18/03
		// Commented out displayText variable definition as it along with the associated 
		// property DisplayText are not being used anymore.
		//
		//private string displayText = null;		

		// MD 8/3/07 - 7.3 Performance
		// Changed type so we don't have to cast it whenever it is accessed
		//private KeyedSubObjectsCollectionBase   primaryCollection   = null;
		private CellsCollection primaryCollection = null;

		
		// SSP 4/26/02
		// EM Embeddable editors.
		// Not used anywhare anymore, so commented it out.
		//
		//bool clickingDropDown = false;
				
		// SSP 7/18/03
		// Commented out the internal method SetValueListDataValue along with valueListDataValue 
		// member variable which the method uses as they're not being used anywhere.
		//
		//internal object valueListDataValue = null;

		// SSP 7/17/03
		// Following internal property is not used anywhere so commented it out.
		//
		//private int    lastValueListIndex = -1;

		private UnBoundData unBoundData = null;

		// SSP 4/5/02
		// Embeddable editors render these unnecessary.
		//
		//------------------------------------------------------------------		
		//private bool editControl_TextChanged_Hooked = false;
		//private Control textChanged_LastHookedEditControl = null;
		private EmbeddableEditorBase lastHookedEditor = null;
		
		// SSP 7/18/03 - Optimizations
		// Instead of storing the cell changed event handler instance into a variable
		// just create it as you hook into and unhook from the cell. That way we don't
		// have to have the variable off the cell.
		//
		//private EventHandler cellChangeEventHandler = null;

		// SSP 5/6/02
		// Added this flag that is reset when going into edit mode and exiting
		// the edit mode. It's used when exiting the edit mode to find out
		// if the user has changed the value in the editor.
		//
		private bool  editValueChanged = false;
		//------------------------------------------------------------------

		// SSP 10/23/02 UWG1782
		// Added inCommitEditValue anti-recursion flag.
		private bool inCommitEditValue = false;


		// SSP 5/9/02
		// Not needed anymore since the embeddable editors take care of dropping it 
		// down when the button is clicked upon.
		

		//	BF 7.10.02
		private bool isExitingEditMode = false;

		// SSP 12/16/02 
		// Optimizations.
		// Added value list item index caching mechanism.
		//
		private object embaddableEditorContext = null;

		// SSP 5/1/03 - Cell Level Editor
		//
		private EmbeddableEditorBase editor = null;
		private IValueList valueList = null;
		private EmbeddableEditorBase lastEmbeddableEditor = null;
		
		// SSP 7/17/03 UWG2448
		// Added EditorControl and Style properties.
		//
		private Control				 editorControl = null;
		private ColumnStyle			 columnStyle = ColumnStyle.Default;
		private int verifiedCachedEditorVersion = -1;

		// SSP 5/14/03 - Hiding Individual Cells Feature
		// 
		private bool hidden = false;

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		private Infragistics.Win.UltraWinGrid.CellDisplayStyle cellDisplayStyle = CellDisplayStyle.Default;

		// SSP 8/6/03 UWG2572
		// Added IgnoreRowColActivation property to the cell.
		//
		private bool ignoreRowColActivation = false;

		// JJD 12/15/03 - Added Accessibility support
		private AccessibleObject accessibilityObject;
		private AccessibleObject editorAccessibilityObject;

		// SSP 12/1/03 UWG2085
		// Added ButtonAppearance property.
		//
		// SSP 10/21/04 UWG3665
		// Don't use appearance holder to store any appearances on the cell. Since cells never 
		// serialize there is no need to have the extra overhead of having an appearance holder.
		// Besides in order to fix UWG3665 we need to do this. In UWG3665, if the same appearance
		// instance is assigned to all the cells then every appearance holder will be hooked into
		// the same appearance instance. This can have a huge performance impact when it comes 
		// the time for the cells to unhook from the appearance (for example when getting 
		// disposed).
		//
		//private Infragistics.Win.AppearanceHolder buttonAppearanceHolder = null;
		private Infragistics.Win.AppearanceBase buttonAppearance = null;

		// SSP 8/26/06 - NAS 6.3
		// Added SelectedAppearance on the cell.
		// 
		private Infragistics.Win.AppearanceBase selectedAppearance = null;

		// SSP 8/7/04 - Added Cell.ToolTipText property
		//
		private string toolTipText = null;

		// SSP 6/30/04 - UltraCalc
		//
		private CellReference calcReference = null;


        // 
        // Added ActiveAppearance on the cell.
        // 
        private Infragistics.Win.AppearanceBase activeAppearance = null;

		internal UltraGridCell( CellsCollection cells, UltraGridColumn column )
		{
			this.column = column;

			// MD 8/3/07 - 7.3 Performance
			// The property has been removed
			//this.PrimaryCollection = cells;
			this.primaryCollection = cells;
		}

		/// <summary>
		/// Called when the object is disposed of.
		/// </summary>
		protected override void OnDispose()
		{
			// SSP 8/5/04 UWG3549 UWG3589
			// Unhook from the editor's ValueChanged if we are hooked into it. There can be
			// situations where cell will be unhooked if it got disposed in the moddle of the
			// process of exitting the edit mode.
			//
			this.UnhookValueChangedFromEditor( );

			UltraGridLayout layout = this.Layout;

			// MRS 9/21/04 - UWG3670
			// If the cell is selected, un-select it
			if ( this.selectedValue )
			{
				UltraGrid grid = layout.Grid as UltraGrid;
				// AS 9/28/04 UWG3442
				// Setting the Selected property will lead to a prop change
				// notification which will cause an exception since the
				// object is disposed. Just remove the object from the 
				// selected collection. Also, don't bother if the layout is
				// disposed.
				//
				//if ( grid != null && grid.Selected.Cells.Contains(this) )
				//	this.Selected = false;
				if ( !layout.Disposed && grid != null )
					grid.Selected.Cells.Remove(this);
			}

			// unhook notifications and call dispose on the Appearance object
			//
			if ( null != this.appearance)
			{
				// SSP 4/29/06 BR11431
				// Make sure the layout doesn't hold onto the appearance reference.
				// 
				// ------------------------------------------------------------------
				if ( null != layout )
					layout.RemoveMulticastAppearance( this.appearance, this );
				// ------------------------------------------------------------------

				this.appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// SSP 8/26/06 - NAS 6.3
				// Don't dispose the appearance as we may not necessarily have created it.
				// 
				//this.appearance.Dispose();

				this.appearance = null;
			}

			// SSP 4/29/06 BR11431
			// Make sure the layout doesn't hold onto the appearance reference.
			// 
			if ( null != layout && null != this.buttonAppearance )
				layout.RemoveMulticastAppearance( this.buttonAppearance, this );

			// SSP 8/26/06 - NAS 6.3
			// 
			if ( null != layout && null != this.selectedAppearance )
				layout.RemoveMulticastAppearance( this.selectedAppearance, this );

            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties (ActiveAppearance)
            // 
            if (null != layout && null != this.activeAppearance)
                layout.RemoveMulticastAppearance(this.activeAppearance, this);

            // MBS 7/9/08 - BR33993
            ValueList vl = this.ValueList as ValueList;
            if(vl != null)
                vl.ReleaseResources();

			this.unBoundData = null;
			base.OnDispose();
		}

		// SSP 8/26/06 - NAS 6.3
		// Added ClearSettings method.
		// 
		internal void ClearSettings( bool retainUnboundCellData )
		{
			UltraGridLayout layout = this.Layout;

			if ( null != this.appearance )
			{
				layout.RemoveMulticastAppearance( this.appearance, this );
				this.appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.appearance = null;
			}

			if ( null != this.buttonAppearance )
			{
				layout.RemoveMulticastAppearance( this.buttonAppearance, this );
				this.buttonAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.buttonAppearance = null;
			}

			if ( null != this.selectedAppearance )
			{
				layout.RemoveMulticastAppearance( this.selectedAppearance, this );
				this.selectedAppearance = null;
			}

            //
            if (null != this.activeAppearance)
            {
                layout.RemoveMulticastAppearance(this.activeAppearance, this);
                this.activeAppearance = null;
            }

			this.ResetActivation( );
			this.CellDisplayStyle = CellDisplayStyle.Default;
			this.Editor = null;

            // MRS - NAS 9.2 - Control Container Editor
#pragma warning disable 0618
            this.EditorControl = null;
#pragma warning restore 0618            
            this.EditorComponent = null;

			this.Hidden = false;
			this.IgnoreRowColActivation = false;
			this.Style = ColumnStyle.Default;
			this.TabStop = DefaultableBoolean.Default;
			this.ToolTipText = null;

			if ( ! retainUnboundCellData )
				this.unBoundData = null;

			this.ValueList = null;
		}

		// MD 8/3/07 - 7.3 Performance
		// No one calls this outside the class, use the field instead
		#region Removed

		//#if DEBUG
		//        /// <summary>
		//        /// The primary collection that owns this object
		//        /// </summary>
		//#endif		
		//        internal KeyedSubObjectsCollectionBase PrimaryCollection
		//        {
		//            get 
		//            {
		//                return this.primaryCollection;
		//            }
		//            set 
		//            {
		//                this.primaryCollection = value;
		//            }
		//        }

		#endregion Removed

		/// <summary>
		/// Returns the band to which this cell belongs
		/// </summary>        
		public override Infragistics.Win.UltraWinGrid.UltraGridBand Band 
		{
			get
			{
				// SSP 2/20/04 - Optimization
				//
				//return this.Row.Band;
				return this.Row.BandInternal;
			}
		}

		internal override bool PivotOnRowScrollRegion { get { return true; } }
		internal override bool PivotOnColScrollRegion { get { return false; } }

		

        // MRS - NAS 9.2 - Control Container Editor
        private Component editorComponent = null;

		/// <summary>
		/// Called when this object is being removed from the passed
		/// in collection. This implementation does nothing.
		/// </summary>
		void IKeyedSubObject.OnRemovedFromCollection( KeyedSubObjectsCollectionBase primaryCollection  )
		{
			// don't do anything here
		}

		/// <summary>
		/// Called when this object is being added to the passed
		/// in collection. This implementation does nothing.
		/// </summary>
		void IKeyedSubObject.OnAddedToCollection( KeyedSubObjectsCollectionBase primaryCollection  )
		{
			// don't do anything here
		}
		
		String IKeyedSubObject.Key
		{
			get
			{
				return this.column.Key;
			}
			set
			{
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_31"));
			}
		}

		// JJD 6/10/02
		// Added implemenation for KeyLowercaseInterned
		string IKeyedSubObjectEx.KeyLowercaseInterned 
		{ 
			get { return ((IKeyedSubObjectEx)(this.column)).KeyLowercaseInterned; }
		}

		/// <summary>  
		/// Returns or sets the Appearance object that controls the object's formatting.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>Appearance</b> property of an object is used to associate the object with an Appearance object that will determine its appearance. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For many of the objects in the UltraWinGrid, you do not set formatting properties directly. Instead, you set the properties of an Appearance object, which controls the formatting of the object it is attached to.</p>
		/// <p class="body">There are two ways of working with the <b>Appearance</b> property and assigning the attributes of an Appearance object to other objects. One way is to create a new Appearance object, adding it directly to the Appearances collection. Then you assign the new Appearance object  to the <b>Appearance</b> property of the object you want to format. This method uses a "named" Appearance object that you must explicitly create (and to which you must assign property settings) before it can be used. For instance, you could create an object in the grid's Appearances collection and assign it some values as follows:</p>
		/// <p class="code">UltraWinGrid1.Appearances.Add "New1"</p>
		/// <p class="code">UltraWinGrid1.Appearances("New1").BorderColor = System.Drawing.Color.Blue</p>
		/// <p class="code">UltraWinGrid1.Appearances("New1").ForeColor = System.Drawing.Color.Red</p>
		/// <p class="body">Creating the object in this way does not apply formatting to any visible part of the grid. The object simply exists in the collection with its property values, waiting to be used. To actually use the object, you must assign it to the grid's (or another object's) <b>Appearance</b> property:</p>
		/// <p class="code">UltraWinGrid1.Appearance = UltraWinGrid1.Appearances("New1")</p>
		/// <p class="body">In this case, only one Appearance object exists. The grid's appearance is governed by the settings of the "New1" object in the collection. Any changes you make to the object in the collection will immediately be reflected in the grid.</p>
		/// <p class="body">The second way of working with the <b>Appearance</b> property is to use it to set property values directly, such as:</p>
		/// <p class="code">UltraWinGrid1.Appearance.ForeColor = System.Drawing.Color.Blue</p>
		/// <p class="body">In this case, an Appearance object is automatically created by the control. This Appearance object is not a member of an Appearances collection and it does not have a name. It is specific to the object for which it was created; it is an "intrinsic" Appearance object. Changes to the properties of an intrinsic Appearance object are reflected only in the object to which it is attached.</p>
		/// <p class="body">Note that you can assign properties from a named Appearance object to an intrinsic Appearance object without creating a dependency relationship. For example, the following code...</p>
		/// <p class="code">UltraWinGrid1.Appearance.ForeColor = UltraWinGrid1.Appearances("New1").ForeColor</p>
		/// <p class="body">...does <i>not</i> establish a relationship between the foreground color of the intrinsic object and that of the named object. It is simply a one-time assignment of the named object's value to that of the intrinsic object. In this case, two Appearance objects exist - one in the collection and one attached to the grid - and they operate independently of one another.</p>
		/// <p class="body">If you wish to assign all the properties of a named object to an intrinsic object at once without creating a dependency relationship, you can use the <b>Clone</b> method of the Appearance object to duplicate its settings and apply them. So if you wanted to apply all the property settings of the named Appearance object "New1" to the grid's intrinsic Appearance object, but you did not want changes made to "New1" automatically reflected in the grid, you would use the following code:</p>
		/// <p class="code">UltraWinGrid1.Appearance = UltraWinGrid1.Appearances("New1").Clone</p>
		/// <p class="body">Note that the properties of an Appearance object can also operate in a hierarchical fashion. Certain properties can be set to a "use default" value, which indicates to the control that the property should take its setting from the object's parent. This functionality is enabled by default, so that unless you specify otherwise, child objects resemble their parents, and formatting set at higher levels of the grid hierarchy is inherited by objects lower in the hierarchy.</p>
		/// </remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.Appearance Appearance
		{
			get
			{
				if ( null == this.appearance )
				{
				    
					this.appearance = new Infragistics.Win.Appearance();
				
					// hook up the prop change notifications
					//
					this.appearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.appearance;
			}
			set
			{
				if( this.appearance != value )
				{
					UltraGridLayout layout = this.Layout;

					// remove the old prop change notifications
					//
					if ( null != this.appearance )
					{
						this.appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

						// SSP 10/21/04 UWG3665
						// This is to fix a performance problem where if the same appearance instance is
						// assigned to a lot of cells or rows then all these cells/rows hook into that
						// appearance instance. When it comes the time for the cells to unhook from this
						// appearance (like when the grid is being disposed or the datasource is being
						// reset) it takes a long time because the .NET multicast events use linked list to
						// store the delegates. This requires performing a linear search and if there are a
						// lot of cells/rows that are hooked into the same appearence then it can end up
						// taking a very long time. The solution as implemented by this fix is for the
						// layout to hook into such appearances that are shared by multiple cells/rows once
						// and then manage the cells/rows that refer to these appearances and redirect the
						// sub object prop change to them.
						//
						if ( null != layout )
							layout.RemoveMulticastAppearance( this.appearance, this );
					}
                    
					this.appearance = value;
            
					// Hook up the new prop change notifications.
					//
					// SSP 10/21/04 UWG3665
					// This is to fix a performance problem where if the same appearance instance is
					// assigned to a lot of cells or rows then all these cells/rows hook into that
					// appearance instance. When it comes the time for the cells to unhook from this
					// appearance (like when the grid is being disposed or the datasource is being
					// reset) it takes a long time because the .NET multicast events use linked list to
					// store the delegates. This requires performing a linear search and if there are a
					// lot of cells/rows that are hooked into the same appearence then it can end up
					// taking a very long time. The solution as implemented by this fix is for the
					// layout to hook into such appearances that are shared by multiple cells/rows once
					// and then manage the cells/rows that refer to these appearances and redirect the
					// sub object prop change to them.
					//
					//if ( null != this.appearance )
					//	this.appearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					if ( null != this.appearance && null != layout )
						layout.SetMulticastAppearance( this.appearance, this );

					this.NotifyPropChange( PropertyIds.Appearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the appearance object for Appearance property has been allocated.
		/// </summary>
		public bool HasAppearance
		{
			get
			{
				return ( null != this.appearance );
			}
		}

		// SSP 12/1/03 UWG2085
		// Added ButtonAppearance property.
		//
		#region ButtonAppearance

		// SSP 10/21/04 UWG3665
		// Don't use appearance holder to store any appearances on the cell. Since cells never 
		// serialize there is no need to have the extra overhead of having an appearance holder.
		// Besides in order to fix UWG3665 we need to do this. In UWG3665, if the same appearance
		// instance is assigned to all the cells then every appearance holder will be hooked into
		// the same appearance instance. This can have a huge performance impact when it comes 
		// the time for the cells to unhook from the appearance (for example when getting 
		// disposed). Rewrote ButtonAppearance property to not make use of an appearance holder.
		//
		/// <summary>
		/// Determines the formatting attributes that will be applied to the buttons in this cell.
		/// </summary>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.AppearanceBase ButtonAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.buttonAppearance )
				{
					this.buttonAppearance = new Infragistics.Win.Appearance( );
                    
					// hook up the prop change notifications
					//
					this.buttonAppearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.buttonAppearance;
			}
			set
			{
				if ( this.buttonAppearance != value )
				{
					UltraGridLayout layout = this.Layout;

					// remove the old prop change notifications
					//
					if ( null != this.buttonAppearance )
					{
						this.buttonAppearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

						if ( null != layout )
							layout.RemoveMulticastAppearance( this.buttonAppearance, this );
					}
                    
					this.buttonAppearance = value;
            
					// Hook up the new prop change notifications.
					//
					//if ( null != this.buttonAppearance )
					//	this.buttonAppearance.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					if ( null != this.buttonAppearance && null != layout )
						layout.SetMulticastAppearance( this.buttonAppearance, this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ButtonAppearance );
				}
			}
		}

		

		#endregion // ButtonAppearance

		#region HasButtonAppearance
		
		// SSP 8/26/06 - NAS 6.3
		// Made HasButtonAppearance public.
		// 
		/// <summary>
		/// Returns true if the button appearance object has been allocated.
		/// </summary>
		public bool HasButtonAppearance
		{
			get
			{
				// SSP 10/21/04 UWG3665
				// Don't use appearance holder to store any appearances on the cell. Since cells never 
				// serialize there is no need to have the extra overhead of having an appearance holder.
				//
				//return null != this.buttonAppearanceHolder && this.buttonAppearanceHolder.HasAppearance;
				return null != this.buttonAppearance;
			}
		}

		#endregion // HasButtonAppearance


		#region SelectedAppearance

		// SSP 8/26/06 - NAS 6.3
		// Added SelectedAppearance property.
		// 
		/// <summary>
		/// Determines the formatting attributes that will be applied to the cell when it's selected.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>Note:</b> To set this appearance on the grid or band level, use the Override's 
		/// <see cref="UltraGridOverride.SelectedCellAppearance"/> property.
		/// </p>
		/// </remarks>
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.AppearanceBase SelectedAppearance
		{
			get 
			{
				// if we don't already have an appearance object then create it now
				//
				if ( null == this.selectedAppearance )
				{
					this.selectedAppearance = new Infragistics.Win.Appearance( );

					// hook up the prop change notifications
					//
					UltraGridLayout layout = this.Layout;
					if ( null != layout )
						layout.SetMulticastAppearance( this.selectedAppearance, this );
				}

				return this.selectedAppearance;
			}
			set
			{
				if ( this.selectedAppearance != value )
				{
					UltraGridLayout layout = this.Layout;

					// remove the old prop change notifications
					//
					if ( null != this.selectedAppearance )
					{
						if ( null != layout )
							layout.RemoveMulticastAppearance( this.selectedAppearance, this );
					}
                    
					this.selectedAppearance = value;
            
					// Hook up the new prop change notifications.
					//
					if ( null != this.selectedAppearance && null != layout )
						layout.SetMulticastAppearance( this.selectedAppearance, this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.SelectecCellAppearance );
				}
			}
		}

		#endregion // SelectedCellAppearance

		#region HasSelectedAppearance
		
		// SSP 8/26/06 - NAS 6.3
		// Added SelectedCellAppearance property.
		// 
		/// <summary>
		/// Returns true if the selected appearance object has been allocated.
		/// </summary>
		public bool HasSelectedAppearance
		{
			get
			{
				return null != this.selectedAppearance;
			}
		}

		#endregion // HasSelectedAppearance
		 
		/// <summary>  
		/// Returns a value that determines whether the data in a cell or row has been changed, but not committed to the data source. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property returns True when a cell or row's data has changed, but has not yet been committed to the data source; otherwise, it returns False.</p>
		///	<p class="body">When the value of a cell is changed, either programmatically by setting its <b>Value</b> property, or by user interaction, this property is set to True and the <b>BeforeCellUpdate</b> and <b>AfterCellUpdate</b> events are generated. Note that the cell's new value is not necessarily committed to the data source at this time, however, since various factors such as the type of record locking employed by the data source, as well as the value of the <b>UpdateMode</b> property, can affect when the actual update occurs. Once the data source is actually updated, the <b>BeforeRowUpdate</b> and <b>AfterRowUpdate</b> events are generated and this property is set back to False.</p>
		///	<p class="body">The <b>OriginalValue</b> property of the cell can be used to determine a cell's value before it was changed.</p>
		///	</remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public bool DataChanged
		{
			get
			{
				// SSP 6/30/03 UWG2271
				// Now we are not setting the DataChanged to true in ValueChanged of the editor. Instead
				// we will wait till the value is commited to the backend to set this property. Until then
				// we will have editValueChanged flag set to true. Look at UWG2271 for more info on why
				// this was done.
				//
				//return this.dataChanged;
				return this.dataChanged || this.editValueChanged;
			}
		}

 
		/// <summary>  
		/// Returns or sets a value that determines whether a cell's dropdown list is displayed.
		/// </summary>
		///	<remarks>
		///	<p class="body">Use this property to cause a cell's dropdown list to be dropped down programmatically, or to determine whether the list is currently displayed.</p>
		///	<p class="body">When a cell's dropdown list is dropped down, the <b>BeforeCellListDropDown</b> event is generated.</p>
		///	<p class="body">When a cell's dropdown list is closed, the<b>AfterCellListCloseUp</b>event is generated.</p>
		///	</remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public bool DroppedDown
		{
			get
			{
				// SSP 4/26/02 
				// EM Embeddable editors.
				// Implemented properly
				//
				if ( this.IsInEditMode )
				{
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//EmbeddableEditorBase editor = this.Column.Editor;
					EmbeddableEditorBase editor = this.EditorResolved;

					Debug.Assert( null != editor && editor.IsInEditMode, "Editor in edit mode, bu cell in edit mode !" );

					return null != editor && editor.SupportsDropDown && 
						editor.IsInEditMode && editor.IsDroppedDown;
				}

				return false;
				
			}
			set
			{
				if ( value != this.DroppedDown )
				{
					// If this cell is not in edit mode, then try to set it in
					// edit mode.
					//
					if ( this != this.Layout.ActiveCell || !this.IsInEditMode )
					{
						this.SetFocusAndActivate( false, true );

						// If somehow entering edit mode is cancelled, then return
						// without proceeding.
						//
						if ( this != this.Layout.ActiveCell || !this.IsInEditMode )
							return;
					}

					if ( value )
						this.ShowDropDown( );
					else
						this.HideDropDown( );
				}

				// SSP 4/26/02
				// EM Embeddable editors.
				//
				
			}
		}



		// SSP 8/12/05 BR05479
		// 
		//internal bool CanBeModified
		internal virtual bool CanBeModified
		{
			get
			{
				// first check the editmode enumerator to see if this cell
				// can be edited
				//
				if ( Infragistics.Win.UltraWinGrid.Activation.AllowEdit != this.ActivationResolved )
					return false;

				//
				// All unbound columns can be modified.
				//
				//
				// SSP 12/3/04 BR00550
				// When the underlying DataColumn is read-only we still want to allow editing that
				// column cell in an add-row.
				//
				//return !this.Column.IsReadOnly;
				return !this.Column.IsReadOnly || this.Row.IsAddRow
					// SSP 11/10/05 BR07684
					// If the data column is computed, it should be read-only regardless of whether
					// the cell is from an add-row.
					// 
					&& ! this.Column.DataColumnHasExpression;
			}
		}

		/// <summary>
		/// Returns an Appearance object with its properties set to the actual values that will be used to display the object.
		/// </summary>
		///	<remarks>
		///	<p class="body">Examining the value of an Appearance object property that has not been set will return the "use default" value, not the internal value that is actually being used to display the object affected by the Appearance object. In order to find out what values are being used, you must use the <b>ResolveAppearance</b> method. This method will evaluate the property values of an Appearance object and return an Appearance object with all of its properties set to meaningful values that can be used to determine how the object will look.</p>
		///	<p class="body">When you change the properties of an Appearance object, you are not required to specify a value for every property that object supports. Whether the Appearance object is a stand-alone object you are creating from scratch, or an intrinsic object that is already attached to some other object, you can set certain properties and ignore others. The properties you do not explicitly set are given a "use default" value that indicates there is no specific setting for that property.</p>
		///	<p class="body">Properties that are set to the "use default" value derive their settings from other objects by following an appearance hierarchy. In the appearance hierarchy, each object has a parent object from which it can inherit the actual numeric values to use in place of the "use default" values. The "use default" value should not be confused with the initial setting of the property, which is generally referred to as the default value. In many cases, the default setting of an object's property will be "use default"; this means that the property is initially set not to use a specific value. The "use default" value will be 0 for an enumerated property (usually indicated by a constant ending in the word "default", such as AlignDefault) or -1 (0xFFFFFFFF) for a numeric setting, such as that used by color-related properties.</p>
		///	<p class="body">So for example, if the Appearance object of a cell has its <b>BackColor</b> property set to -1, the control will use the setting of the row's <b>BackColor</b> property for the cell, because the row is above the cell in the appearance hierarchy. The top level of the appearance hierarchy is the UltraWinGrid control itself. If any of the UltraWinGrid's Appearance object properties are set to their "use default" values, the control uses built-in values (the "factory presets") for those properties. For example, the factory preset of the <b>BackColor</b> property of the grid's Appearance object is the system button face color (0x8000000F). This is the value that will be used for the grid's background color when the <b>BackColor</b> property of the grid's Appearance object is set to the "use default" value.</p>
		///	<p class="body">The <b>ResolveAppearance</b> method will return an Appearance object with all of its "use default" settings converted into actual values. It does this by navigating the appearance hierarchy for each property until an explicit setting or a factory preset is encountered. If you simply place a grid on a form, run the project, and examine the setting of the <b>BackColor</b> property of the grid's intrinsic Appearance object:</p>
		///	<p class="code">MsgBox Hex(UltraWinGrid1.Appearance.BackColor)</p>
		///	<p class="body">...you will see that it is set to the "use default" value (0xFFFFFFFF). However, if you use the <b>ResolveAppearance</b> method to return the same value:</p>
		///	<p class="code">MsgBox Hex(UltraWinGrid1.ResolveAppearance.BackColor)</p>
		///	<p class="body">...you will see that it is set to the system button face color (0x8000000F). Note that this code takes advantage of the fact that the <b>ResolveAppearance</b> method returns an Appearance object to simplify the code that must be written. This code could be written out in a longer form as follows:</p>
		///	<p class="code">Dim objAppearance as UltraWinGrid.Appearance</p>
		///	<p class="code">Set objAppearance = UltraWinGrid1.ResolveAppearance</p>
		///	<p class="code">MsgBox Hex(objAppearance.BackColor)</p> 
		///	</remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		public void ResolveAppearance( ref AppearanceData appData )
		{
			this.ResolveAppearance( ref appData, AppearancePropFlags.AllRenderAndCursor );
		}

		/// <summary>
		/// Returns an Appearance object with its properties set to the actual values that will be used to display the object.
		/// </summary>
		///	<remarks>
		///	<p class="body">Examining the value of an Appearance object property that has not been set will return the "use default" value, not the internal value that is actually being used to display the object affected by the Appearance object. In order to find out what values are being used, you must use the <b>ResolveAppearance</b> method. This method will evaluate the property values of an Appearance object and return an Appearance object with all of its properties set to meaningful values that can be used to determine how the object will look.</p>
		///	<p class="body">When you change the properties of an Appearance object, you are not required to specify a value for every property that object supports. Whether the Appearance object is a stand-alone object you are creating from scratch, or an intrinsic object that is already attached to some other object, you can set certain properties and ignore others. The properties you do not explicitly set are given a "use default" value that indicates there is no specific setting for that property.</p>
		///	<p class="body">Properties that are set to the "use default" value derive their settings from other objects by following an appearance hierarchy. In the appearance hierarchy, each object has a parent object from which it can inherit the actual numeric values to use in place of the "use default" values. The "use default" value should not be confused with the initial setting of the property, which is generally referred to as the default value. In many cases, the default setting of an object's property will be "use default"; this means that the property is initially set not to use a specific value. The "use default" value will be 0 for an enumerated property (usually indicated by a constant ending in the word "default", such as AlignDefault) or -1 (0xFFFFFFFF) for a numeric setting, such as that used by color-related properties.</p>
		///	<p class="body">So for example, if the Appearance object of a cell has its <b>BackColor</b> property set to -1, the control will use the setting of the row's <b>BackColor</b> property for the cell, because the row is above the cell in the appearance hierarchy. The top level of the appearance hierarchy is the UltraWinGrid control itself. If any of the UltraWinGrid's Appearance object properties are set to their "use default" values, the control uses built-in values (the "factory presets") for those properties. For example, the factory preset of the <b>BackColor</b> property of the grid's Appearance object is the system button face color (0x8000000F). This is the value that will be used for the grid's background color when the <b>BackColor</b> property of the grid's Appearance object is set to the "use default" value.</p>
		///	<p class="body">The <b>ResolveAppearance</b> method will return an Appearance object with all of its "use default" settings converted into actual values. It does this by navigating the appearance hierarchy for each property until an explicit setting or a factory preset is encountered. If you simply place a grid on a form, run the project, and examine the setting of the <b>BackColor</b> property of the grid's intrinsic Appearance object:</p>
		///	<p class="code">MsgBox Hex(UltraGrid1.Appearance.BackColor)</p>
		///	<p class="body">...you will see that it is set to the "use default" value (0xFFFFFFFF). However, if you use the <b>ResolveAppearance</b> method to return the same value:</p>
		///	<p class="code">MsgBox Hex(UltraGrid1.ResolveAppearance.BackColor)</p>
		///	<p class="body">...you will see that it is set to the system button face color (0x8000000F). Note that this code takes advantage of the fact that the <b>ResolveAppearance</b> method returns an Appearance object to simplify the code that must be written. This code could be written out in a longer form as follows:</p>
		///	<p class="code">Dim objAppearance as Infragistics.Win.Appearance</p>
		///	<p class="code">Set objAppearance = UltraGrid1.ResolveAppearance</p>
		///	<p class="code">MsgBox Hex(objAppearance.BackColor)</p> 
		///	</remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		/// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
		public void ResolveAppearance( ref AppearanceData appData,
			AppearancePropFlags requestedProps )
		{
			this.Row.ResolveCellAppearance( this.column, ref appData, requestedProps );

			//get resolve appearance for the cell
			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (UltraGridCell), requestedProps );

			//get the resolved color for the cell
			this.Layout.ResolveColors( ref appData, ref context, SystemColors.Control, SystemColors.WindowFrame, 
				SystemColors.WindowText, false );
		}

		// SSP 4/26/02
		// Em Embeddable editors.
		// Commented below code out.
		


		internal void ShowDropDownHelper( bool hide )
		{			
			UltraGrid grid = this.Column.Layout.Grid as UltraGrid;

			if( null == grid )
				return;

			// If the cell is read-only, then don't proceed.
			//
			if ( !this.CanBeModified )
				return;

			UltraGridCell cellInEditMode = this.Layout.CellInEditMode;
			Debug.Assert( cellInEditMode != null, "ToggleDropDown called when there is no cell in edit mode !" );
			if ( null == cellInEditMode )				
				return;

			Debug.Assert( this == cellInEditMode, "ToogleDropDown called on a cell that's not in edit mode !" );
			if ( this != cellInEditMode )
				return;
			
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridColumn column = this.Column;

			CellUIElementBase cellElem = (CellUIElementBase)this.GetUIElement( );
			
			Debug.Assert( null != cellElem, "Cell in edit mode has no cell element associated !" );
			if ( null == cellElem )
				return;

			EmbeddableUIElementBase embeddableElemInEditMode = cellElem.FindEmbeddableUIElement( );

			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//EmbeddableEditorBase editor = column.Editor;
			EmbeddableEditorBase editor = this.EditorResolved;

			Debug.Assert( null != editor, "this.EditorResolved returned null !" );
			if ( null == editor )
				return;

			Debug.Assert( editor.IsInEditMode, "Editor is not in edit mode !" );
			if ( !editor.IsInEditMode )
				return;

			Debug.Assert( editor == embeddableElemInEditMode.Editor, "Column editor and column's cell's embeddable element's Editor don't match !" );
			if ( editor != embeddableElemInEditMode.Editor )
				return;

			Debug.Assert( editor.ElementBeingEdited == embeddableElemInEditMode, "Editor's ElementBeingEdited is not the same as embeddable element that the cell in edit mode has !" ); 
			if ( editor.ElementBeingEdited != embeddableElemInEditMode )
				return;

			Debug.Assert( editor.SupportsDropDown, "ToggleDropDown called on a column with editor that does not support dropping down !" );
			if ( editor.SupportsDropDown )
			{
				if ( !hide )
					editor.DropDown( );
				else
					editor.CloseUp( );
			}
		}
					
		internal void ShowDropDown()
		{ 
			UltraGrid grid = this.Column.Layout.Grid as UltraGrid;

			if( null == grid )
				return;

			if ( this.CanBeModified )
			{
				this.ShowDropDownHelper( false );
			}
		}
		
		internal void HideDropDown()
		{
			UltraGrid grid = this.Column.Layout.Grid as UltraGrid;

			if( null == grid )
				return;

			if ( this.CanBeModified )
			{
				this.ShowDropDownHelper( true );
			}			
		}

 
		/// <summary>  
		/// Returns or sets a value that determines how an object will behave when it is activated.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>Activation</b> property of the UltraGridCell object is subordinate to the settings of the Activation properties of the UltraGridRow and UltraGridColumn objects that contain the cell. If either the cell's row or column has its <b>Activation</b> property set to False, the cell cannot be activated, regardless of its own setting for <b>Activation</b>. The setting of the other type of parent also has no effect; setting <b>Activation</b> to False on a cell's row makes the cell inactive regardless of the setting of its column.</p>
		///	<seealso cref="UltraGridColumn.CellActivation"/>
		///	<seealso cref="UltraGridRow.Activation"/>
		///	<seealso cref="UltraGridCell.IgnoreRowColActivation"/>
		///	</remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Activation Activation
		{
			get
			{
				return this.activation;
			}
			set
			{
				if( this.activation != value )
				{
					if ( !Enum.IsDefined( typeof(Activation), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_32") );

					this.activation = value;

					// SSP 11/26/02 UWG1834
					// Cause the row to reposition its child elements (cell elements) which will
					// cause it to enable or disable cell according to the new value of Activation.
					//
					if ( null != this.Row )
						this.Row.InvalidateItemAllRegions( true );

					this.NotifyPropChange( PropertyIds.Activation, null );
				}
			}
		}

        // SSP 8/26/06 - NAS 6.3
		// 
		internal void ResetActivation( )
		{
			this.Activation = 0;
		}

		// SSP 8/6/03 UWG2572
		// Added IgnoreRowColActivation property to the cell.
		//
		#region IgnoreRowColActivation

		/// <summary>
		/// Specifies whether to ignore the activation settings of the row and column for this cell. Normally if the row or column's Activation is set to Disabled, then the cell uses that even if the cell's Activation is set to something else. Basically the cell ends up using most restrictive of the activation settings. Setting this property to true will cause the cell to use it's Activation settings and ignore the row's and column's activation settings.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies whether to ignore the activation settings of the row and column for this cell. Normally if the row or column's Activation is set to Disabled, then the cell uses that even if the cell's Activation is set to something else. Basically the cell ends up using most restrictive of the activation settings. Setting this property to true will cause the cell to use it's Activation settings and ignore the row's and column's activation settings.</p>
		/// </remarks>
		public bool IgnoreRowColActivation
		{
			get
			{
				return this.ignoreRowColActivation;
			}
			set
			{
				if ( value != this.ignoreRowColActivation )
				{
					this.ignoreRowColActivation = value;
					
					this.Layout.DirtyGridElement( false, true );
				}
			}
		}

		#endregion // IgnoreRowColActivation
 
		/// <summary>
		/// <see cref="Infragistics.Win.UltraWinGrid.HeaderBase.Height"/> 
		/// </summary> 
		public int Height
		{
			get
			{
				return this.Row.Height;
			}
		}


		/// <summary>  
		/// Returns or sets a value that determines whether the cell is selected.
		/// </summary>  
		///	<remarks>
		///	<p class="body">Setting the <b>Selected</b> property of an object causes it to become selected. This property can also be used to determine whether the object has been selected.</p>
		///	<p class="body">Depending on the settings of the selection style property that applies to this object (<b>SelectTypeCell</b>) and maximum selection property (<b>MaxSelectedCells</b>) changing the value of the <b>Selected</b> property may affect the selection of other objects within the band or the control.</p>
		///	<p class="body">For example, if <b>SelectTypeRow</b> is set to 2 (SelectTypeSingle) so that only one row may be selected at a time, when you set the <b>Selected</b> property of an UltraGridRow object to True, the <b>Selected</b> properies of all other UltraGridRow objects will be set to False. As another example, if you have set the <b>MaxSelectedRows</b> property to 3, then attempt to set the <b>Selected</b> property to True for four rows, a run-time error will occur when you set the <b>Selected</b> property to True for the fourth row.</p>
		///	<p class="body">When an object is selected or deselected, the <b>BeforeSelectChange</b> event is generated.</p>
		///	</remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override bool Selected
		{
			get
			{
				return this.selectedValue;
			}
			set
			{
				if( this.selectedValue != value )
				{
					UltraGrid grid = this.Layout.Grid as UltraGrid;

					if( null != grid )
					{
						// SSP 4/10/02 UWG1072
						// Don't clear the existing selection when selected
						// property is set. Look at the selection strategy to
						// determine whether to clear existing selected
						// items.
						//
						//grid.InternalSelectItem( this, true, value );
						
						bool clearExistingItems = true;

						SelectionStrategyBase selectionStrategy = 
							((ISelectionStrategyProvider)this.Band).SelectionStrategyCell;

						if ( null != selectionStrategy )
							clearExistingItems = selectionStrategy.IsSingleSelect;

						// Also clear the selected rows as rows and cells are mutually 
						// exclusive for selection. If the user cancels the selection
						// then return without selecting the row.
						//
						if ( grid.ClearSelectedRows( ) )
							return;

						grid.InternalSelectItem( this, clearExistingItems, value );
					}
				}
			}
		}

		#region Selectable

		// SSP 4/20/05 - NAS 5.2 Filter Row
		// Overrode Selectable. Moved the code from CanSelectCell into here and commented
		// out CanSelectCell.
		//
		/// <summary>
		/// Property: Returns true only if selectable
		/// </summary>
		internal protected override bool Selectable
		{ 
			get 
			{ 
				// SSP 4/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
				// Use the new IsDataRow which checks for IsTemplateAddRow as well as other special
				// rows like the filter row.
				//
				//if ( this.Row.IsTemplateAddRow )
				if ( ! this.Row.IsDataRow )
					return false;

				// SSP 1/6/04 - Accessiblity
				// If the selection strategy is None then retrun false.
				//
				// --------------------------------------------------------------------
				ISelectionStrategy strategy = this.SelectionStrategyDefault;
				if ( null == strategy || strategy is SelectionStrategyNone )
					return false;

				// Cells can't be selected in ultradropdown.
				//
				if ( this.Band.Layout.Grid is UltraDropDownBase )
					return false;

				// Also return false if Activation is Disabled.
				//
				if ( this.IsDisabled )
					return false;
				// --------------------------------------------------------------------

				return base.Selectable;
			}
		}

		#endregion // Selectable

		// SSP 4/20/05 - NAS 5.2 Filter Row
		// Moved the logic into Selectable property. Selectable is defined as virtual in
		// GridItemBase. Commented out CanSelectCell.
		//
		

		/// <summary>  
		/// Returns or sets a value that determines whether the user can give focus to an object by pressing the TAB key.
		/// </summary>  
		///	<remarks>
		///	<p class="body">Use this property to specify whether the user can navigate to a cell or the cells in a column by pressing the TAB key.</p>
		///	<p class="body">The <b>TabNavigation</b> property is used to specify how the control will respond when the TAB key is pressed.</p>
		///	</remarks>
		public DefaultableBoolean TabStop
		{
			get
			{
				return this.tabStop;
			}
			set
			{
				if( this.tabStop != value )
				{
					if ( !Enum.IsDefined( typeof(DefaultableBoolean), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_33") );

					this.tabStop = value;
					this.NotifyPropChange( PropertyIds.TabStop, null );
				}
			}
		}

		/// <summary>
		/// Whether this cell can be tabbed to (read-only).
		/// </summary>
		[ Browsable( false ) ] 
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public override bool IsTabStop
		{ 
			get
			{
				switch ( this.tabStop )
				{
					case DefaultableBoolean.True:
						return true;
					case DefaultableBoolean.False:
						return false;
				}

				return this.Column.TabStop;
			}
		}
 
		/// <summary>  
		/// Returns the text of the cell. If the cell is in edit mode, it returns the text in the editor being used for editing the cell.
		/// </summary>  
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public string Text
		{
			get
			{
				// SSP 10/4/02 UWG1728
				// Return the current edit text rather than what's in the underlying bound list
				// if the cell is in edit mode.
				//
				if ( this.IsInEditMode )
				{
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					
					Debug.Assert( null != this.EditorResolved && this.EditorResolved.IsInEditMode, 
						"Cell in edit mode yet the editor is not in edit mode !" );

					if ( null != this.EditorResolved && this.EditorResolved.IsInEditMode )
					{
						return this.EditorResolved.CurrentEditText;
					}
				}


				return this.column.GetCellText(this.Row);
			}
		}

		// SSP 7/18/03
		// Commented out internal property DisplayText as it's not being used anywhere.
		//
		
 
		
		/// <summary>  
		/// Returns the original value of the cell. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>  
		///	<remarks>
		///	<p class="body"><b>OriginalValue</b> returns the cell's original value before it was last set. NOTE: While in edit mode, <see cref="Value"/> property returns the value in the binding list and not the current value in the editor control being used for editing the cell. For example, if the user enters edit mode on a cell with the value of 1 and changes the value to 2, <b>Value</b> property will still retrun 1. To get to the modified value use the underlying editor's value property. <b>OriginalValue</b> property returns the value of the cell before it's value was set last time.</p>
		///	</remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public object OriginalValue
		{
			get
			{
				if ( !this.column.IsBound )
				{
					if ( this.unBoundData != null )
					{
						return this.unBoundData.Original;
					}

					return null;
				}
				else
				{
					if ( null == this.originalValue )
					{
						this.originalValue = this.Row.GetCellValue( this.column );
					}
					return this.originalValue;					
				}
			}
		}		
		internal void ResetOriginalValue()
		{
			this.originalValue = null;
		}


 
		/// <summary>  
		/// Returns or sets the underlying data value of a cell. This property is not available at design-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">Use this property to retrieve or modify a cell's value. When the value of a cell is changed, the <b>BeforeCellUpdate</b> and the <b>AfterCellUpdate</b> events are generated and the cell's <b>DataChanged</b> property is set to True. Note that the cell's new value is not necessarily committed to the data source when this property is set, however, since various factors such as the type of record locking employed by the data source, as well as the value of the <b>UpdateMode</b> property, can affect when the actual update occurs.</p>
		///	<p class="body">The <b>OriginalValue</b> property of the cell can be used to determine the cell's value before it was changed.</p>
		///	<p class="body">The <b>GetText</b> method can be invoked to return the formatted value of a cell.</p>
		///	</remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		// SSP 4/16/05 - NAS 5.2 Filter Row
		// Created UltraGridFilterCell class.
		//
		//public object Value
		public virtual object Value
		{
			get
			{
				// AS -	10/25/01
				// At design time, we want to display the column datatype in the cell.
				//
				if (this.Band.Layout.Grid.DesignMode)
				{
					// SSP 7/17/02 UWG1592
					// Since the embeddable editors are expecting the data of the supported
					// data type, we can't just pass in the string.
					// So pass in a sample data instead.
					//
					
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//return this.Row.GetSampleDataForType( this.Column.DataType );
					return UltraGridRow.GetSampleDataForType( this.Column.DataType );
				}

				// SSP 11/10/03 Add Row Feature
				// For template add-rows, use the the UltraGridRow.GetValue.
				//
				//if ( !this.column.IsBound )
				if ( ! this.Row.IsTemplateAddRow && !this.column.IsBound )
				{
					Infragistics.Win.UltraWinGrid.UnBoundData pUnboundData = this.UnboundData;

					if ( pUnboundData != null )
					{
						// SSP 4/16/02 UWG1077
						// If the unbound data is null, and the column style is a 
						// CheckBox (which is 2 state check box), and the column style is
						// boolean, then default the value to false.
						//
						//return pUnboundData.Current;
						object retVal = pUnboundData.Current;
						if ( ( null == retVal || retVal is System.DBNull ) &&
							null != this.Column && 
							typeof( System.Boolean ) == this.Column.DataType &&
							// SSP 5/1/03 - Cell Level Editor
							// Added Editor and ValueList properties off the cell so the user can set the editor
							// and value list on a per cell basis.
							//
							//ColumnStyle.CheckBox == this.Column.StyleResolved )
							ColumnStyle.CheckBox == this.StyleResolved )
						{
							retVal = false;
						}

						return retVal;
					}

					return null;
				}
				else
				{
					return this.Row.GetCellValue( this.column );
				}
			}
			set
			{
				// SSP 10/6/04 - UltraCalc
				// Added SetValue method and moved all the code from here to there. We want to 
				// supress the error message box in the case a formula's evaluation results do 
				// not match the column's data type and set value on the list object fails. 
				// That's what the suppressErrorMessagePrompt parameter is for.
				//
				// SSP 8/11/05 BR05398
				// 
				//this.SetValue( value, false );
				this.SetValueInternal( value, false, true );
			}
		}

        /// <summary>
		/// Sets the value of this cell, optionally storing the old value into the undo stack so the user
		/// can revert back to the old value via undo operation.
		/// </summary>
		/// <param name="value">The new value of the cell.</param>
		/// <param name="storeInUndoStatck">true to store the original cell value in the undo stack so the user can undo the change.</param>
		/// <remarks>
		/// <para class="body">
		/// When <b>storeInUndoStatck</b> parameter is false this method does the same thing as
		/// the <see cref="Value"/> property. When storeInUndoStatck parameter is true, the
		/// old value is stored into the undo stack so the user can undo the change in the
		/// value of the cell. To enable undo functionality, set the Override's
		/// <see cref="UltraGridOverride.AllowMultiCellOperations"/> property.
        /// </para>
		/// </remarks>
        // MRS 3/10/2008 - BR30851		
        // Overload for backward compatibility.
        public void SetValue(object value, bool storeInUndoStatck)
        {
            this.SetValue(value, storeInUndoStatck, true);
        }

		// SSP 11/17/06 BR16530
		// 
		/// <summary>
		/// Sets the value of this cell, optionally storing the old value into the undo stack so the user
		/// can revert back to the old value via undo operation.
		/// </summary>
		/// <param name="value">The new value of the cell.</param>
		/// <param name="storeInUndoStatck">true to store the original cell value in the undo stack so the user can undo the change.</param>
        /// <param name="checkForReadOnlyCells">false to specify that undo or redo actions may be performed even when the cells in the grid are read-only. Has no effect when storeInUndoStatck is false.</param>
		/// <remarks>
		/// <para class="body">
		/// When <b>storeInUndoStatck</b> parameter is false this method does the same thing as
		/// the <see cref="Value"/> property. When storeInUndoStatck parameter is true, the
		/// old value is stored into the undo stack so the user can undo the change in the
		/// value of the cell. To enable undo functionality, set the Override's
		/// <see cref="UltraGridOverride.AllowMultiCellOperations"/> property.
        /// </para>
        /// <para class="body">
        /// When the <b>checkForReadOnlyCell</b> parameter is set to false, the grid will not check
        /// if the cells are read-only before performing an undo or redo. This allows the user to 
        /// undo or redo an action in a read-only cell. This only applies when storeInUndoStatck is
        /// true.
		/// </para>
		/// </remarks>
        // MRS 3/10/2008 - BR30851
		//public void SetValue( object value, bool storeInUndoStatck )
        public void SetValue(object value, bool storeInUndoStatck, bool checkForReadOnlyCells)
		{
			if ( storeInUndoStatck )
			{
				UltraGridLayout layout = this.Layout;
				ActionFactory.SingleCellValueAction action = new ActionFactory.SingleCellValueAction( layout.ActionHistory, this, value );

                // MRS 3/10/2008 - BR30851
                action.CheckForReadOnlyCells = checkForReadOnlyCells;

				this.Layout.ActionHistory.PerformAction( null, action );
			}
			else
			{
				this.Value = value;
			}
		}

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Added an overload of SetValue that takes in just the value.
		// 
		internal bool SetValueInternal( object value )
		{
			return this.SetValueInternal( value, false );
		}

		// SSP 10/6/04 - UltraCalc
		// Added SetValue method and moved all the code from Value property's set here.
		// We want to supress the error message box in the case a formula's evaluation
		// results do not match the column's data type and set value on the list object
		// fails. That's what the suppressErrorMessagePrompt parameter is for.
		//
		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Changed the return type from void to bool. We need to know if the action was canceled or not.
		// 
		//internal void SetValue( object value, bool suppressErrorMessagePrompt )
		internal bool SetValueInternal( object value, bool suppressErrorMessagePrompt )
		{
			return this.SetValueInternal( value, suppressErrorMessagePrompt, false );
		}

		// SSP 6/7/05 BR04458
		// Added an overload of SetValue that takes in fireInitializeRow parameter.
		// 
		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// Changed the return type from void to bool. We need to know if the action was canceled or not.
		// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
		// 
		//internal void SetValue( object value, bool suppressErrorMessagePrompt, bool fireInitializeRow )
		internal bool SetValueInternal( object value, bool suppressErrorMessagePrompt, bool fireInitializeRow )
		{
			return this.SetValueInternal( value, suppressErrorMessagePrompt, fireInitializeRow, false );
		}

        //  BF NA 9.1 - UltraCombo MultiSelect
        //  I added another overload so the caller can specify whether the
        //  RowsCollection should fire the DataChanged event.

		// SSP 2/21/06 BR10249
		// Added an overload that takes in throwExceptionOnError parameter.
		// 
		internal bool SetValueInternal( object value, bool suppressErrorMessagePrompt, bool fireInitializeRow, bool throwExceptionOnError )
		{
            bool fireDataChanged = true;
            return this.SetValueInternal( value, suppressErrorMessagePrompt, fireInitializeRow, throwExceptionOnError, fireDataChanged );
        }

		internal bool SetValueInternal( object value, bool suppressErrorMessagePrompt, bool fireInitializeRow, bool throwExceptionOnError, bool fireDataChanged )
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			// SSP 4/9/02 UWG1058
			// Allow updating of cells in unbound column on combo and drop down
			//
			// SSP 10/2/02 UWG1724
			// Follow up on the above mentioned comment.
			//
			//if( null == grid )
			// SSP 1/14/05 BR01756
			// Took out the restriction we had that didn't allow setting of cell values in
			// UltraDropDownBase. Commented out the following two lines.
			//
			//if( null == grid && this.Column.IsBound )
			//	throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_34"));

			Debug.Assert( null != this.column, "no column in cell.set_Value" );				
			Debug.Assert( null != this.Layout, "no layout in cell.set_Value" );
			Debug.Assert( null != this.Layout.Grid, "no grid in cell.set_Value" );

			UltraGridRow row = this.Row;
			UltraGridColumn column = this.Column;
			UltraGridLayout layout = this.Layout;

			if ( null == column || null == layout || null == layout.Grid )
				// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				// Changed the return type from void to bool. We need to know if the action was canceled or not.
				// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
				// 
				//return;
				return false;

			// Ignore value puts on cells who's columns are chaptered
			if ( column.IsChaptered )
				// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				// Changed the return type from void to bool. We need to know if the action was canceled or not.
				// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
				// 
				//return;
				return false;

			// SSP 11/10/03 Add Row Feature
			// For template add-rows, use the the UltraGridRow.SetValue.
			//
			// ----------------------------------------------------------------------
			if ( row.IsTemplateAddRow )
			{
				// SSP 10/6/04
				// Added suppressErrorMessagePrompt parameter. Pass that along to the SetCellValue.
				//
				//this.Row.SetCellValue( this.Column, value );
				// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				// Changed the return type from void to bool. We need to know if the action was canceled or not.
				// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
				// 
				// ----------------------------------------------------
				//row.SetCellValue( column, value, suppressErrorMessagePrompt );
				//return;
				// SSP 2/21/06 BR10249
				// Added an overload that takes in throwExceptionOnError parameter. Pass that along.
				// 
				//return row.SetCellValue( column, value, suppressErrorMessagePrompt );
				return row.SetCellValue( column, value, suppressErrorMessagePrompt, throwExceptionOnError );
				// ----------------------------------------------------
			}
			// ----------------------------------------------------------------------
				
			// If we are already in the BeforeCellUpdate event prevent the value propery from
			// being changed.
			if ( 
				// SSP 4/9/02 UWG1058
				// Added null != grid condition because we do allow for updating of cells in
				// unbound columns when the grid is not an UltraGrid.
				//
				null != grid &&
				grid.EventManager.InProgress( GridEventIds.BeforeCellUpdate ) )
			{
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_35") );
			}

			object oldValue = row.GetCellValue( column );

			// SSP 8/16/01 UWG57
			// Check if the data value has actually changed. If it has not changed
			// don't go any further
			//
			// SSP 6/20/02 UWG1121
			// If the cell is in edit mode, then don't check for values being the
			// same below because the edit control could have something different
			// in it, in which case we do want to go ahead and update the edit 
			// control's text later on. Normally if the cell values are the same,
			// then we return without going through the update process. However,
			// when in edit mode, cell's edit control's value could be different.
			//
			if ( !this.IsInEditMode )
			{
				if ( null == oldValue && null == value ) 
					// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
					// Changed the return type from void to bool. We need to know if the action was canceled or not.
					// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
					// 
					//return;
					return true;
				
				if ( null != oldValue && null != value )
				{
					//RobA UWG250 8/28/01 if this is a value list the 
					//value in the database could be the displayed value not
					//the actual datavalue, so we need to use the right
					//value when we compare
					//
					if ( oldValue.Equals( value ) )
						// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
						// Changed the return type from void to bool. We need to know if the action was canceled or not.
						// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
						// 
						//return;
						return true;

					// SSP 11/3/03
					// Even if the string representation of objects are different, allow
					// setting the value. The reason for this is that if the column is of
					// a custom object type which does not implement ToString method, then
					// we will still return without setting the value eventhough the objects
					// could be different in value. Above call to Equals should be 
					// sufficient.
					// Commented out below code.
					//
					
				}
			}
					
			// SSP 4/9/02 UWG1058
			// Added null != grid condition because we do allow for updating of cells in
			// unbound columns when the grid is not an UltraGrid.
			//
			if ( null != grid )
			{
                // MRS 12/8/2008 - TFS11256
                // We should not fire events for a cell in an export
                // or print layout, because we copy the values from every unbound cell
                // in the grid into the new cloned export/print layout and this causes
                // a whole lot of unneccessary event firings.
                // Added 'if' block 
                //
                if (false == this.Layout.IsPrintLayout &&
                    false == this.Layout.IsExportLayout)
                {
                    BeforeCellUpdateEventArgs e = new BeforeCellUpdateEventArgs(this, value);

                    grid.FireEvent(GridEventIds.BeforeCellUpdate, e);
                    if (e.Cancel)
                        // SSP 11/28/05 - NAS 6.1 Multi-cell Operations
                        // Changed the return type from void to bool. We need to know if the action was canceled or not.
                        // Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
                        // 
                        //return;
                        return true;
                }
			}

			// SSP 8/11/05 BR05398
			// Enclosed the following code in try-finally.
			// 
			bool initializeRowWasAlreadySuspended = layout.SuspendInitializeRowFor( row );
			try
			{						
				// AS 1/16/06 BR08963
				// The filter row stores the value of all cells regardless
				// of whether the associated column is bound or not so we
				// need to delegate to the SetCellValue on the row for all
				// filter row cells.
				//
				//if ( column.IsBound )
				if ( column.IsBound || row.IsFilterRow )
				{	
					this.originalValue = oldValue;

					// SSP 10/6/04
					// Added suppressErrorMessagePrompt parameter. Pass that along to the SetCellValue.
					//
					//this.Row.SetCellValue( this.column, value );		
					// SSP 2/21/06 BR10249
					// Added an overload that takes in throwExceptionOnError parameter. Pass that along.
					// 
					//row.SetCellValue( column, value, suppressErrorMessagePrompt );
					row.SetCellValue( column, value, suppressErrorMessagePrompt, throwExceptionOnError );

					// SSP 10/6/04
					// Commented out the following line. This isn't being used in the if block.
					//
					//Infragistics.Win.UltraWinGrid.UnBoundData unboundData = this.UnboundData;
					
					// SSP 8/17/01 UWG132
					// We need to set the data changed to true if the value in text
					// has changed
					// If the cell's column is not bound, and
					// we are in the InitializeRow event,
					// do not dirty the cell.
					//            
					// First check if we have a grid and event handler.
					if ( null != grid && null != grid.EventManager )
					{

						// If the column is bound, or the column is unbound and the 
						// event is not in progress, dirty ourselves.
						if ( !grid.EventManager.InProgress( Infragistics.Win.UltraWinGrid.GridEventIds.InitializeRow ) )
						{
							this.SetDataChanged( true );
						}

					}
					else    // don't have a grid and eventhandler? This is an
						// odd situation, but dirty ourselves anyway.
					{
						this.SetDataChanged( true );
					}
				
				}
				else
				{
					
					Infragistics.Win.UltraWinGrid.UnBoundData unboundData = this.UnboundData;

					
					if ( null != unboundData )
					{
						//RobA UWG558 10/18/01
						//
						// SSP 12/12/01 UWG811
						// If the value is null or DBNull, then do not convert the type
						//
						//unboundData.Current = Convert.ChangeType( value, this.column.DataType );;
						if ( null == value || value is DBNull 
							// SSP 9/14/05 BR06386
							// Apparently Convert.ChangeType fails if value is Bitmap and
							// DataType is Image eventhough Bitmap derives from Image.
							// 
							|| column.DataType.IsAssignableFrom( value.GetType( ) ) )
							unboundData.Current = value;
						else
							unboundData.Current = Convert.ChangeType( value, this.column.DataType );;
						 
						
						// SSP 11/9/01 UWG629
						// Don't set the data changed to true if we are in InitializeRow
						// event.
						//
						if ( null != grid && null != grid.EventManager )
						{

							// If the column is bound, or the column is unbound and the 
							// event is not in progress, dirty ourselves.
							if ( !grid.EventManager.InProgress( Infragistics.Win.UltraWinGrid.GridEventIds.InitializeRow ) )
							{
								this.SetDataChanged( true );
							}

						}
						else
						{
							this.SetDataChanged( true );
						}

						// SSP 7/14/04 - UltraCalc
						// Notify the calc manager of the change in the cell's value. We only have
						// to do it for unbound columns because for bound columns we are doing this
						// in the row's SetCellValue method.
						//
						this.Row.ParentCollection.NotifyCalcManager_ValueChanged( this );

						// SSP 12/2/04 BR01066
						// Also recalc summaries.
						//
						this.Row.ParentCollection.DataChanged_DirtySummaries( );

                        //  BF NA 9.1 - UltraCombo MultiSelect
                        //  Since this does not trigger a ListChanged notification, we need
                        //  to send something out so that the CheckedRowsCollection et al.
                        //  know that the "check state" of the row might have changed.
                        if ( fireDataChanged )
                        {
                            RowsCollection rowsCollection = this.Row.ParentCollection;
                            string columnKey = column != null ? column.Key : string.Empty;
                            CheckedListSettings.DataChangedEventArgs eventArgs = new CheckedListSettings.DataChangedEventArgs( this.Row, columnKey, null );
                            rowsCollection.FireDataChanged( eventArgs );
                        }
					}
				}

                // MBS 4/22/08 - RowEditTemplate NA2008 V2
                // If we have a proxy associated with the cell that's been modified, we should
                // make sure to invalidate it so that it picks up the new value
                UltraGridRowEditTemplate template = row.RowEditTemplateResolved;
                if (template != null && (template.IsShown || template.Row != null))
                {
                    Debug.Assert(this.column != null, "I guess we're forcing the column to be created");

                    // If there is anything bound to the cell that's being edited, we need to make sure 
                    // that the new information is pulled in
                    template.RefreshBindings(this.Band);

                    if (template.ColumnProxyMap.ContainsKey(this.Column.Key))
                        template.ColumnProxyMap[this.Column.Key].Invalidate(true);
                }

				this.NotifyPropChange( PropertyIds.Value, null );

				// SSP 6/27/02 
				// If we're in a CardView band and the card style is Variable, dirty the data area 
				// so the cards can resize and repaint.
				//
				if ( row.IsCardStyleVariableHeight && row.IsPotentiallyVisibleOnScreen )
					layout.DirtyDataAreaElement( );

				// SSP 1/3/02 UWG905
				// When the cell's value has changed, we should not rely on
				// the binding manager or the underlying binding list sending
				// us any messages to let us know of the change. So we should
				// dirty the cell element accross all the scroll regions to 
				// reflect the change. (NOTE: we dirty the cell elements 
				// because the change in the cell's value could also lead to
				// associated image being changed. This would happen for example
				// when there is a value list for the column and value list items'
				// appearances have images set on them. )
				//
				this.InvalidateItemAllRegions( true );

				// SSP 4/9/02 UWG1058
				// If the grid is null, then don't proceed as below code is UltraGrid spcific.
				// 
				if ( null != grid )
				{
					// SSP 2/28/02 UWG1035
					// If setting a cell's value while the cell is in, we should
					// also update the associated edit control's text to the new 
					// value.
					//
					if ( this == layout.ActiveCell && this.IsInEditMode
						// SSP 3/10/06 BR10740
						// If SetValue is being called from within CommitEditValue then don't set
						// the editor's value here since it will be the same. To minimize the potential
						// for breakage, only do this for the filter cell since that's the only
						// kind of cell that typically commits edit value while remaining in edit
						// mode (when the filter evaluation trigger is on cell value change).
						// 
						&& ( ! this.IsFilterRowCell || ! this.InCommitEditValue )
						)
					{
						// SSP 4/26/02
						// EM Embeddable editors.
						// Instead of updating the text in the edit control, update the
						// Value of the editor being edited.
						//
						// SSP 5/1/03 - Cell Level Editor
						// Added Editor and ValueList properties off the cell so the user can set the editor
						// and value list on a per cell basis.
						//
						//EmbeddableEditorBase editor = null != this.Column ? this.Column.Editor : null;
						EmbeddableEditorBase editor = this.EditorResolved;

						Debug.Assert( null != editor, "this.EditorResolved null !" );
						//Debug.Assert( editor.IsInEditMode, "Cell in edit mode, but the editor not in edit mode !" );
						if ( editor.IsInEditMode )
						{
							bool orignalDontFireCellChangeEventValue = this.Layout.DontFireCellChangeEvent;

							// SSP 10/4/02 UWG1728
							// Set the DontFireCellChangeEvent flag so that we don't fire it when
							// we are internally setting the editor's value. This is so that if the
							// user is setting the Value property of the cell in CellChange event, the
							// it will get into an infinite recursion which we don't want. V1 of the
							// wingrid behaved correctly and we need to do the same.
							// Set DontFireCellChangeEvent flag and reset it and also enclosed the
							// set of the editors Value in try-finally.
							//
							this.Layout.DontFireCellChangeEvent = true;

							try
							{
								editor.Value = this.Value;
							}
							finally
							{
								this.Layout.DontFireCellChangeEvent = orignalDontFireCellChangeEventValue;
							}
						}


						
					}
				
                    // MRS 12/8/2008 - TFS11256
                    // We should not fire events for a cell in an export
                    // or print layout, because we copy the values from every unbound cell
                    // in the grid into the new cloned export/print layout and this causes
                    // a whole lot of unneccessary event firings. 
                    //
                    // Added 'if' block here 
                    if (false == this.Layout.IsPrintLayout &&
                        false == this.Layout.IsExportLayout)
                    {
                        CellEventArgs e2 = new CellEventArgs(this);
                        grid.FireEvent(GridEventIds.AfterCellUpdate, e2);
                    }
				}

                // CDS 01/29/09 TFS12785 Cell has been updated so clear the cached Header CheckState, and dirty the HeaderUIElements
                RowsCollection parentCollection = row.ParentCollection;
                if (null != column &&
                    null != parentCollection)
                {
                    parentCollection.DirtyCheckStateCacheValidEntry(column);
                    if (null != row.Band)
                        row.Band.DirtyCheckStateCacheValidEntry(column);
                    column.DirtyHeaderUIElements(parentCollection);
                }
            }
			finally
			{
				if ( ! initializeRowWasAlreadySuspended )
					layout.ResumeInitializeRowFor( row );
			}

			// SSP 6/7/05 BR04458
			// Added an overload of SetValue that takes in fireInitializeRow parameter.
			// 
			// SSP 8/11/05 BR05398
			// 
			//if ( fireInitializeRow )
			if ( fireInitializeRow && ! initializeRowWasAlreadySuspended )
				row.FireInitializeRow( );

			// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
			// Changed the return type from void to bool. We need to know if the action was canceled or not.
			// Returns TRUE if successful, FALSE if there was an error or the action was cacneled.
			// 
			return true;
		}
 
		/// <summary>  
		/// Returns or sets the width of an object in container units.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>Width</b> property is used to determine the horizontal dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		///	</remarks>  
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int Width
		{
			get
			{
				return this.column.Width;
			}
		}

 
		/// <summary>  
		/// Returns or sets the starting point of text selected or the position of the insertion point if no text is selected in a cell being edited.
		/// </summary>
		///	<remarks>
		///	<p class="body">This property, in conjunction with the <b>SelLength</b> and <b>SelText</b> properties, is useful for tasks such as setting the insertion point, establishing an insertion range, selecting substrings, or clearing text in the cell being edited.</p>
		///	<p class="body">The valid range for this property is 0 to the length of the cell text. Attempting to set this property to a value outside that range will reset this property to the highest acceptable value.</p>
		///	<p class="body">This property can only be set or retrieved when the control is in edit mode, which can be determined by using the <b>IsInEditMode</b> property. If the control is in edit mode, the <b>ActiveCell</b> property can be used to determine which cell is currently being edited. If the control is not in edit mode, attempting to use this property will generate an error.</p>
		///	<p class="body">Setting this property changes the selection to an insertion point and sets the <b>SelLength</b> property to 0.</p>
		///	</remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int SelStart
		{
			get
			{
				
				// SSP 8/15/02 UWG1574
				// Implemented.
				//
				//return this.selStart;

				if ( !this.IsInEditMode )
				{
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_343") );
				}

				EmbeddableEditorBase editor = this.Layout.EditorInEditMode;

				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//Debug.Assert( editor == this.Column.Editor, "Editor of the column of the cell in edit mode does not match the editor in edit mode !" );
				Debug.Assert( editor == this.EditorResolved, "Editor of the column of the cell in edit mode does not match the editor in edit mode !" );

				if ( !editor.SupportsSelectableText )
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_344") );

				return editor.SelectionStart;
			}
			set
			{
				// SSP 8/15/02 UWG1574
				// Implemented.
				//

				if ( !this.IsInEditMode )
				{
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_343") );
				}

				EmbeddableEditorBase editor = this.Layout.EditorInEditMode;

				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//Debug.Assert( editor == this.Column.Editor, "Editor of the column of the cell in edit mode does not match the editor in edit mode !" );
				Debug.Assert( editor == this.EditorResolved, "EditorResolved off the cell does not match the editor in edit mode !" );

				if ( !editor.SupportsSelectableText )
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_344") );

				editor.SelectionStart = value;
			}
		}

		/// <summary>
		/// Indicates whether this is the active cell
		/// </summary>
		public override bool Activated
		{ 
			get 
			{ 
				return this == this.Column.Band.Layout.ActiveCell; 
			}
			
			set
			{
				UltraGrid grid = this.column.Band.Layout.Grid as UltraGrid;

				// SSP 1/27/05 BR02053
				// Only do this for the display layout. Print and export layouts should not be
				// modifying the ActiveRow of the grid.
				//
				//if( null == grid )
				if( null == grid || ! this.Layout.IsDisplayLayout )
					throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_37"));

				if ( this.IsDisabled && value )
					throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_38"));
				
				if ( value )
					grid.ActiveCell = this;
				else
					if ( this.Activated )
					grid.ActiveCell = null;
			} 
		}

		// AS 10/24/01 Moved to GridItemBase
		//		/// <summary>
		//		/// Indicates whether this cell can be selected (not disabled)
		//		/// </summary>
		//		protected override bool Selectable
		//		{ 
		//			get
		//			{
		//				return !this.IsDisabled; 
		//			}
		//		}
 
		/// <summary>  
		/// Returns or sets the number of characters selected in a cell being edited.
		/// </summary>  
		/// <remarks>
		/// <p class="body">This property, in conjunction with the <b>SelStart</b> and <b>SelText</b> properties, is useful for tasks such as setting the insertion point, establishing an insertion range, selecting substrings, or clearing text in the cell being edited.</p>
		/// <p class="body">The valid range for this property is 0 to the length of the cell text. Attempting to set this property to a value outside that range will reset this property to the highest acceptable value.</p>
		/// <p class="body">This property can only be set or retrieved when the control is in edit mode, which can be determined by using the <b>IsInEditMode</b> property. If the control is in edit mode, the <b>ActiveCell</b> property can be used to determine which cell is currently being edited. If the control is not in edit mode, attempting to use this property will generate an error.</p>
		/// </remarks>		
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int SelLength
		{
			get
			{	
				// SSP 8/15/02 UWG1574
				// Implemented.
				//
				//return this.selLength;

				if ( !this.IsInEditMode )
				{
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_343") );
				}

				EmbeddableEditorBase editor = this.Layout.EditorInEditMode;

				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//Debug.Assert( editor == this.Column.Editor, "Editor of the column of the cell in edit mode does not match the editor in edit mode !" );
				Debug.Assert( editor == this.EditorResolved, "EditorResolved off the cell does not match the editor in edit mode !" );

				if ( !editor.SupportsSelectableText )
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_344") );

				return editor.SelectionLength;
			}
			set
			{
				// SSP 8/15/02 UWG1574
				// Implemented.
				// this.selLength = value;

				if ( !this.IsInEditMode )
				{
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_343") );
				}

				EmbeddableEditorBase editor = this.Layout.EditorInEditMode;

				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//Debug.Assert( editor == this.Column.Editor, "Editor of the column of the cell in edit mode does not match the editor in edit mode !" );
				Debug.Assert( editor == this.EditorResolved, "EditorResolved off the cell does not match the editor in edit mode !" );

				if ( !editor.SupportsSelectableText )
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_344") );

				editor.SelectionLength = value;
					
					
				// SSP 8/15/02
				// Selected text related properties should not be notifying
				// prop change.
				//
				//this.NotifyPropChange( PropertyIds.SelLength, null );
			}
		}
 
		/// <summary>  
		/// Returns or sets the selected text of the cell being edited. Note: The cell must be in edit mode and the editor being used for the cell must support selectable text. Otherwise this property will throw a NotSupportedException.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property, in conjunction with the <b>SelLength</b> and <b>SelStart</b> properties, is useful for tasks such as setting the insertion point, establishing an insertion range, selecting substrings, or clearing text in the cell being edited.</p>
		/// <p class="body">Setting this property to a new value sets the <b>SelLength</b> property to 0 and replaces the selected text with the specified text.</p>
		/// <p class="body">This property can only be set or retrieved when the control is in edit mode, which can be determined by using the <b>IsInEditMode</b> property. If the control is in edit mode, the <b>ActiveCell</b> property can be used to determine which cell is currently being edited. If the control is not in edit mode, attempting to use this property will generate an error.</p>
		/// <p class="body">Note: Accessing this property will throw an exception if the cell is not in edit mode or the underlying editor being used does not support selectable text, that is its <see cref="Infragistics.Win.EmbeddableEditorBase.SupportsSelectableText"/> must return true.</p>
		/// </remarks>
		[ Browsable( false ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public string SelText
		{
			get
			{
				// SSP 8/15/02 UWG1574
				// Implemented.
				//
				//return this.selText;

				if ( !this.IsInEditMode )
				{
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_343") );
				}

				EmbeddableEditorBase editor = this.Layout.EditorInEditMode;

				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//Debug.Assert( editor == this.Column.Editor, "Editor of the column of the cell in edit mode does not match the editor in edit mode !" );
				Debug.Assert( editor == this.EditorResolved, "EditorResolved off the cell does not match the editor in edit mode !" );

				if ( !editor.SupportsSelectableText )
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_344") );

				return editor.SelectedText;
			}
			set
			{
				// SSP 8/15/02 UWG1574
				// Implemented.
				//

				if ( !this.IsInEditMode )
				{
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_343") );
				}

				EmbeddableEditorBase editor = this.Layout.EditorInEditMode;

				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//Debug.Assert( editor == this.Column.Editor, "Editor of the column of the cell in edit mode does not match the editor in edit mode !" );
				Debug.Assert( editor == this.EditorResolved, "EditorResolved off the cell does not match the editor in edit mode !" );

				if ( !editor.SupportsSelectableText )
					throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_344") );

				editor.SelectedText = value;
			}
		}


		internal override SelectChange SelectChangeType 
		{
			get
			{
				return SelectChange.Cell;
			}
		}

		
		/// <summary>
		///  Refreshes the row cache or the display, depending on the value of the Action parameter.
		/// </summary>
		/// <remarks>
		/// <p class="body">Generally, painting a control is handled automatically while no events are occurring. However, there may be situations where you want the form or control updated immediately, for example, after some external event has caused a change to the form. In such a case, you would use the <b>Refresh</b> method.</p>
		/// <p class="body">The <b>Refresh</b> method can also be used to ensure that the user is viewing the latest copy of the data from the record source.</p>
		/// </remarks>
		public void Refresh()
		{
			this.InvalidateItemAllRegions();
		}


		/// <summary>
		/// Returns the text for the object using the specified mask mode
		/// </summary>
		/// <remarks>
		/// <p class="body">There may be times when you need to work with the text of an object in a particular format, but do not wish to change the settings of any of the masking properties (<b>MaskClipMode</b>, <b>MaskDataMode</b> or <b>MaskDisplayMode</b>). For example, if you want to retrieve the text of an object with all literals and prompt characters intact, but don't want to change the way data will be sent to the the database and don't want to use the clipboard. This is the purpose of the <b>GetText</b> method.</p>
		/// <p class="body"><b>GetText</b> returns a string value, containing the text of the object, in the format you specify. When you invoke the <b>GetText</b> method, you pass it a <i>maskmode</i> parameter that determines how the object's text will be returned. This gives you the ability to bypass the settings of the object's masking properties and determine on an ad hoc basis whether to use prompt characters, literals or just the raw text the user has entered.</p>
		/// </remarks>
		/// <param name="maskMode">The mask mode which will be used to determine the text returned.</param>
        /// <returns>The text for the object using the specified mask mode</returns>
		public string GetText(MaskMode maskMode)
		{
			//SSP 8/14/01 - UWG36
			// format the text with the maskMode
			return this.Column.FormatWithMaskMode( this.Text, maskMode );
		}

		

		/// <summary>
		/// Cancels the update of the row or cell when data has been changed (similar to pressing ESC).
		/// </summary>
		/// <remarks>
		/// <p class="body">When the <b>CancelUpdate</b> method is invoked for a cell, the cell's contents return to their original value. The <b>OriginalValue</b> property of the UltraGridCell can be used to determine what this value is before invoking the method.</p>
		/// <p class="body">When the <b>CancelUpdate</b> method is invoked for a row, any changes made to the cells of the active row are removed. The cells display their original values, and the row is taken out of edit mode. The row selector picture changes from the "Write" image back to the "Current" image.  The <b>DataChanged</b> property will be set to false.</p>
		/// </remarks>
		public void CancelUpdate()
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_39"));


			CancelableCellEventArgs e = new CancelableCellEventArgs( this ); 
			grid.FireEvent( GridEventIds.BeforeCellCancelUpdate, e );

			// if Cancel Upadate gor cancelled then return
			if ( e.Cancel )
				return;

			
			// Need to ExitEditMode before
			// we tried to reset value, otherwise
			// the OnCellChange notification handler
			// was trying to unsuccessfully duplicating 
			// what we're doing.

			// exit edit mode passing true for bCancellingEditOperation 

			// SSP 3/26/04 UWG2934
			// If we are already in the process of exitting edit mode and CancelUpdate
			// is called, simply set the CancellingEditOperationFlag flag off the layout
			// to true so the UltraGridLayout.OnBeforeExitEditMode doesn't commit the 
			// value. Added the if block and enclosed the existing code in the else
			// block.
			//
			if ( this.isExitingEditMode && null != this.Layout )
			{
				this.Layout.CancellingEditOperationFlag = true;
			}
			else
			{
				this.ExitEditMode( true );
			}

			//this.Row.CancelUpdate();

			
			// if dropdown list has focus, set it back to grid
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//if ( Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList == this.Column.StyleResolved )
			if ( Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList == this.StyleResolved )
			{
				// AS - 1/14/02
				// We need to assert putting focus on the control in order for this to work
				// when the assembly is local but the executable is running under internet
				// type restricted security access. However, to prevent a security hole, we
				// will only do so on controls that have our public key token.
				//
				try
				{
                    // MRS 12/19/06 - fxCop
                    //System.Security.Permissions.UIPermission perm = null;

                    //if ( DisposableObject.HasSamePublicKey( this.Layout.Grid.GetType() ))
                    //{
                    //    perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                    //    perm.Assert();
                    //}

                    //this.Layout.Grid.Focus();

                    //// MD 9/11/06
                    //System.Security.Permissions.UIPermission.RevertAssert();
                    if (DisposableObject.HasSamePublicKey(this.Layout.Grid.GetType()))
                        GridUtils.FocusControl(this.Layout.Grid);
                    else
                        this.Layout.Grid.Focus();
				}
				catch {}
			}
			//GetLayout().GetGrid()->SetFocus();

            // CDS NAS v9.1 Header CheckBox
            
            // CDS 02/02/09 TFS12512 Moved the UpdateHeaderCheckBox() method to the column
            //this.UpdateHeaderCheckBox();
            if (this.Column != null &&
                this.Row != null)
            this.Column.UpdateHeaderCheckBox(this.Row.ParentCollection);

			CellEventArgs e2 = new CellEventArgs( this );
			grid.FireEvent( GridEventIds.AfterCellCancelUpdate, e2 );
		}

		// SSP 10/21/04 UWG3665
		// Added InternalOnSubObjectPropChanged because we need to call this from layout.
		//
		internal void InternalOnSubObjectPropChanged( PropChangeInfo propChange )
		{
			this.OnSubObjectPropChanged( propChange );
		}

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// if we're in initialize row, don't do anything
			// SSP 9/15/03
			// Regarding the above comment - WHY ?
			// Commented below code out and moved it inside the below if statement.
			//
			

			// SSP 8/26/06 - NAS 6.3
			// 
			UltraGridRow row = this.Row;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridLayout layout = null != row ? row.Layout : null;

			bool invalidateCell = false;
			bool dirtyCellElem = false;

			if ( propChange.Source == this.appearance )
			{
				// SSP 4/26/02
				// Em Embeddable editors. With embeddable editors, all that's necessary
				// is to dirty the cell element. THe editors will get repositioned and
				// they will update the colors of any underlying edit controls.
				// Commented below code out.
				//
				
			
				// We invalidate cell and recalc rects (latter in case picture was changed)
				//
				// SSP 12/7/04 - Merged Cell Feature
				//
				//this.InvalidateItemAllRegions( true );
				this.InvalidateItemAllRegions( true, true );

				if ( row.InInitializeRowEvent )
					return;

                // MRS 3/18/2009 - TFS15302
                UltraDropDownBase ultraDropDownBase = this.GetGrid() as UltraDropDownBase;
                if (ultraDropDownBase != null)
                    ultraDropDownBase.BumpItemImageSizeVersion();

				// Pass this notifcation on to our listeners with the
				// property id of 'Appearance'
				this.NotifyPropChange( PropertyIds.Appearance, propChange );
			}
				// SSP 12/1/03 UWG2085
				// Added below else-if block.
				//
			else if ( this.HasButtonAppearance && propChange.Source == this.ButtonAppearance )
			{
				if ( null == row || row.InInitializeRowEvent )
					return;

				// SSP 8/26/06 - NAS 6.3 - Optimization
				// 
				// ----------------------------------------------------
				
				dirtyCellElem = true;
				// ----------------------------------------------------

				this.NotifyPropChange( PropertyIds.ButtonAppearance, propChange );
			}
				// SSP 8/26/06 - NAS 6.3
				// 
			else if ( propChange.Source == this.selectedAppearance )
			{
				if ( null != row && ! row.InInitializeRowEvent )
					dirtyCellElem = true;

				this.NotifyPropChange( PropertyIds.SelectecCellAppearance, propChange );
            }
            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties (ActiveAppearance)
            // 
            else if (propChange.Source == this.activeAppearance)
            {
                if (null != row && !row.InInitializeRowEvent)
                    dirtyCellElem = true;

                this.NotifyPropChange(PropertyIds.ActiveCellAppearance, propChange);
            }
			else
				Debug.Assert( false, "Unknown sub object in Row.OnSubObjectPropChanged" );

			// SSP 8/26/06 - NAS 6.3
			// 
			if ( dirtyCellElem || invalidateCell )
				this.InvalidateItemAllRegions( dirtyCellElem, false );
        }

		internal void ToggleDropDown( ) 
		{
			if ( this.CanBeModified )
			{
				if ( this.DroppedDown )
					this.HideDropDown( );
				else
					this.ShowDropDown( );
			}
		}

		// SSP 4/26/02
		// EM Embeddable editors. 
		// Look at the new one above.
		

		internal void Click()
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_40"));

			try
			{
                // MBS 7/1/08 - NA2008 V3
                // Renamed this method since it conflicts with the ClickCell event
				//grid.ClickCell( this );
                grid.ClickCellInternal(this);
			}
			catch (Exception e)
			{
				Debug.Assert( false, "Cell.Click(): " + e);
			}
		}


		internal void OnButtonClick(  )
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if( null == grid )
				return;

			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//if ( Infragistics.Win.UltraWinGrid.ColumnStyle.Button == this.Column.StyleResolved ||
			//	Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton == this.Column.StyleResolved )
			if ( Infragistics.Win.UltraWinGrid.ColumnStyle.Button == this.StyleResolved ||
				Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton == this.StyleResolved )
			{
				// now call click for cell so that it gets activated
				// Find the grid item under the specified point
				//

				this.Click();

				// if activation was cancelled, go no further
				if ( this != this.Column.Layout.ActiveCell )
				{
					// Call UpdateWindow and then reget the cell's button element
					// in case the call to pGridItem->Click() above changed the
					// uielements.
					//
					this.Column.Layout.Grid.Update();

					UIElement cellElement = this.GetUIElement();

                    // MRS 8/14/2008 - BR35547
                    // Added null check
                    if (cellElement != null)
                        cellElement.DirtyChildElements();
				}
			}

			Debug.Assert( null != grid, "No grid ptr in Cell.OnCellButtonDown()");

			Debug.Assert( this == this.Layout.ActiveCell, "Not active cell in Cell.OnCellButtonMouseDown");
			
			// fire the before event. If cancelled return true
			//
			grid.FireEvent( GridEventIds.ClickCellButton, new CellEventArgs( this ) );
		}

		// SSP 4/26/02
		// EM Embeddable editors.
		//
		


		private CheckState ToggleCheckBoxState( CheckState state )
		{
			CheckState retState;

			switch ( state )
			{
				case CheckState.Unchecked:
					retState = CheckState.Checked;
					break;
				case CheckState.Checked:
					// SSP 11/26/01 UWG514
					// If the checkbox style is TriState, then go into Intermediate
					// state when checked, otherwise go into Unchecked state.
					//
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//if ( ColumnStyle.TriStateCheckBox == this.Column.StyleResolved )
					if ( ColumnStyle.TriStateCheckBox == this.StyleResolved )
						retState = CheckState.Indeterminate;
					else
						retState = CheckState.Unchecked;
						
					break;
				case CheckState.Indeterminate:
					retState = CheckState.Unchecked;
					break;
				default:
					retState = CheckState.Unchecked;
					break;
			}

			return retState;
		}

		// SSP 4/26/02
		// EM Embeddable editors.
		// Modified for editors. Original version commented out.
		//		
		internal void ToggleCheckBox( )
		{			
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if( null == grid )
				return;

			CellUIElementBase cellElement = (CellUIElementBase)this.GetUIElement(
				this.Layout.Grid.ActiveRowScrollRegion,
				this.Layout.Grid.ActiveColScrollRegion,
				false );

			Debug.Assert( null != cellElement, "no cell ui element" );
			if ( null == cellElement )
				return;

			Debug.Assert( null != this.Column, "Cell has no column !" );
			if ( null == this.Column )
				return;

			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//EmbeddableEditorBase editor = this.Column.Editor;
			EmbeddableEditorBase editor = this.EditorResolved;

			Debug.Assert( null != editor, "Column.Editor returned null !" );
			if ( null == editor )
				return;

			Debug.Assert( editor is CheckEditor, "Column.Editor is not a CheckEditor and ToggleCheckBox called !" );
			if ( ! ( editor is CheckEditor ) )
				return;

			// If the cell is not in edit mode, then try to go into edit mode.
			// 
			if ( this != this.Layout.ActiveCell || !this.IsInEditMode )
			{
				this.SetFocusAndActivate( false, true );

				// If entering edit mode got cancelled, then return.
				//
				if ( this != this.Layout.ActiveCell || !this.IsInEditMode )
					return;
			}

			Debug.Assert( editor.IsInEditMode, "Editor not in edit mode, but the cell is in edit mode !" );

			// Above we have the check for editor being CheckEditor
			//
			CheckEditor checkEditor = (CheckEditor)editor;
			checkEditor.CheckState = this.ToggleCheckBoxState( checkEditor.CheckState );

			// SSP 4/26/02
			// EM Embeddable editors.
			// Setting the data changed and firing CellChange is not neccary as the
			// editor will fir ValueChanged when it's value is changed, and from the
			// event handler of value changed, we will do what we are doing below.
			//
			
		}

		


		// SSP 4/26/02
		// EM Embeddable editors.
		// Not used anywhare anymore, so commented it out.
		//
		

		// SSP 4/26/02
		// Em Embeddable editors.
		// Commented out OnDropDownButtonClick.
		


		/// <summary>
		/// Returns a value that determines whether a cell in the grid is currently being edited. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property indicates whether a cell is in edit mode. The <b>ActiveCell</b> property can be used to determine which cell is in edit mode.</p>
		/// <p class="body">The <b>BeforeEnterEditMode</b> event is generated before a cell enters edit mode.</p>
		/// <p class="body">The <b>BeforeExitEditMode</b> event is generated before a cell exits edit mode.</p>
		/// </remarks>
		public bool IsInEditMode
		{
			get
			{
				return this.isInEditMode;
			}
			set
			{
				if ( this.isInEditMode != value )
					this.isInEditMode = value;
			}
		}

		/// <summary>
		/// Property: Get IsActiveCell
		/// </summary>
		public bool IsActiveCell
		{
			get
			{
				// SSP 2/20/04 - Optimization
				//
				//return ( this.Row.Band.Layout.ActiveCell == this );
				return ( this.Layout.ActiveCell == this );
			}
		}

		#region IsActiveCellInternal

		// SSP 4/22/05 - Optimizations
		// Added IsActiveRowInternal.
		//
		internal bool IsActiveCellInternal
		{
			get
			{
				return this == this.Layout.ActiveCellInternal;
			}
		}

		#endregion // IsActiveCellInternal
		
		// SSP 5/14/03 - Hiding Individual Cells Feature
		// Added public Hidden method and changed this property's name from Hidden to
		// InternalHidden and also changed calls to this property appropriately.
		//
		//internal bool Hidden
		internal bool InternalHidden
		{
			get
			{
				// SSP 5/14/03 - Hiding Individual Cells Feature
				// 
				//return this.Column.Hidden || this.Row.Hidden;
				// SSP 8/1/03 UWG1654 - Filter Action
				// Use HiddenInternal instead of the public Hidden behavior of which we changed in regards to filtering.
				//
				//return this.Column.Hidden || this.Row.Hidden || this.Hidden;
				// MD 8/14/07 - 7.3 Performance
				// Rearranged so the least expensive getters are called first
				//return this.Column.Hidden || this.Row.HiddenInternal || this.Hidden;
                // MRS 2/23/2009 - Found while fixing TFS14354
				//return this.Hidden || this.Column.Hidden || this.Row.HiddenInternal;
                return this.Hidden || this.Column.HiddenResolved || this.Row.HiddenInternal;
			}
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridLayout Layout 
		{
			get
			{
				// SSP 2/20/04 - Optimization
				//
				//return this.Row.Band.Layout;
				return this.Row.Layout;
			}
			
		}

		/// <summary>
		/// Returns the UIElement object that is associated with an object.
		/// </summary>
        /// <param name="csr">The ColScrollRegion</param>
        /// <param name="rsr">The RowScrollRegion</param>
        /// <param name="verifyElements">Indicates whether to VerifyChildElements</param>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body">The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to an UIElement object residing at specific coordinates.</p>
		/// <p class="body">The <b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement. </p>
		/// <p class="body">The <b>GetUIElement</b> method does not take into account the presence of a pop-up edit window or the drop-down portion of a combo if these elements are present, and will never return a UIElement that corresponds to one of these elements. The <b>GetUIElementPopup</b> method can be invoked to return a reference to a popup window's UIElement.</p>
		/// </remarks>
        /// <returns>The UIElement associated with the object, in the specified row and column scrolling regions.</returns>
		public override UIElement GetUIElement( RowScrollRegion rsr, ColScrollRegion csr, bool verifyElements )
		{
			// SSP 8/16/04 - Optimizations
			// IsPotentiallyVisibleOnScreen returns true if the row has any associated visible
			// rows in any of the scroll region's visible rows collections.
			// ----------------------------------------------------------------------------------
			if ( ! this.Row.IsPotentiallyVisibleOnScreen )
				return null;
			// ----------------------------------------------------------------------------------

			

			// get the grid's main element
			//
			UIElement element = this.Row.Band.Layout.GetUIElement( verifyElements );

			// SSP 12/16/02
			// Optimizations.
			// Get the data area element since rows are always in data area element.
			//
			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( element is UltraGridUIElement )
			//    element = ((UltraGridUIElement)element).DataAreaElement;
			UltraGridUIElement gridElement = element as UltraGridUIElement;

			if ( gridElement != null )
				element = gridElement.DataAreaElement;

			if ( null == element )
				return null;			
			
			return element.GetDescendant( typeof( CellUIElementBase ), new object[] { this.Row, this.column, rsr, csr } );
		}

		internal void RepositionTextBoxCtl( ) 
		{
			//Debug.Assert( this.IsInEditMode, "RepositionTextBoxCtl called on a cell that's not in edit mode !" );

			if ( this.IsInEditMode )
			{
				CellUIElementBase cellElement = (CellUIElementBase)this.GetUIElement( );

				Debug.Assert( null != cellElement, "Cell in edit mode has no associated cell ui element !" );

				if ( null != cellElement )
				{
					// Just dirty the child elements causing the cell to be verified.
					// This will cause the cell to set the Rect of the embeddable element
					// to it's new position. It is at this point the editor will reposition
					// any control that it may be using for editing purposes.
					//
					cellElement.DirtyChildElements( );
				}
					// SSP 10/19/04 UWG3751
					// Exit the edit mode if the ui element can't be found. This probably means the cell
					// got scrolled out of view or got hidden. Added below else block.
					//
				else
				{
					this.ExitEditMode( false, true );
				}
			}
		}

		// SSP 4/29/02
		// EM Embeddable editors. With embeddable editors architechture, all we need to
		// do is to dirty the cell element causing it to be verified and new bounds to
		// be set on the child embeddable element. It's the editor that will actually
		// reposition the edit control whenever the bounds of the embeddable element is
		// set.
		//
		

		internal bool ExitEditMode( )
		{
			return this.ExitEditMode( false, false );
		}
		internal bool ExitEditMode( bool cancellingEditOperation )
		{
			return this.ExitEditMode( cancellingEditOperation, false );
		}

		
		// SSP 4/5/02
		// Embeddable editors lead to heavy change in the implementation of ExitEditMode
		// so it's entirely commented out and a new ExitEditMode was implemented.
		//
		

		internal void HookValueChangedIntoEditor( EmbeddableEditorBase editor )
		{
			this.UnhookValueChangedFromEditor( );

			this.lastHookedEditor = editor;

			// SSP 7/18/03 - Optimizations
			//
			//this.lastHookedEditor.ValueChanged += this.CellChangeEventHandler;
			this.lastHookedEditor.ValueChanged += new EventHandler( this.CellChangeHelper );
		}

		internal void UnhookValueChangedFromEditor( )
		{			
			if ( null != this.lastHookedEditor )
			{
				// SSP 7/18/03 - Optimizations
				//
				//this.lastHookedEditor.ValueChanged -= this.CellChangeEventHandler;
				this.lastHookedEditor.ValueChanged -= new EventHandler( this.CellChangeHelper );
			
				this.lastHookedEditor = null;
			}
		}

		// SSP 10/24/02 UWG1782
		// Added inCommitEditValue flag to indicate that we are in this method.
		// This is so UltraGridRow.Update method doesn't call us back when we
		// call it.
		/// <summary>
		/// Returns true if we are in CommitEditValue method.
		/// </summary>
		internal bool InCommitEditValue
		{
			get
			{
				return this.inCommitEditValue;
			}
		}

		// SSP 12/16/02 
		// Optimizations.
		// Added value list item index caching mechanism.
		//
		internal object EmbeddableEditorContext
		{
			get
			{
				return this.embaddableEditorContext;
			}
			set
			{
				this.embaddableEditorContext = value;
			}
		}

		// SSP 4/5/02
		// Moved the code for validating the edited value being committed to backend
		// into this separate method from ExitEditMode
		// SSP 10/16/02 UWG1116
		// Added the new parameter of fireDataError which indicates whether to throw an
		// exception or to fire DataError event. When called from UltraGridRow.Update,
		// we want to just throw the exception and not fire DataError.
		//
		internal bool CommitEditValue( ref bool stayInEdit, bool fireDataError )
		{
			return this.CommitEditValue( ref stayInEdit, fireDataError, false );
		}

		// SSP 4/23/05 - NAS 5.2 Filter Row
		// Added an overload of CommitEditValue that takes in forceDontThrowException.
		//
		internal bool CommitEditValue( ref bool stayInEdit, bool fireDataError, bool forceDontThrowException )
		{
			return this.CommitEditValue( ref stayInEdit, fireDataError, forceDontThrowException, false );
		}

		// SSP 4/1/06 BR11228
		// Added an overload that takes in dontStoreInUndoHistory parameter.
		// 
		internal bool CommitEditValue( ref bool stayInEdit, bool fireDataError, bool forceDontThrowException, bool dontStoreInUndoHistory )
		{
			// SSP 10/24/02 UWG1782
			// Added inCommitEditValue flag to indicate that we are in this method.
			// This is so UltraGridRow.Update method doesn't call us back when we
			// call it.
			bool origInCommitEditValue = this.inCommitEditValue;
			this.inCommitEditValue = true;
			try
			{

				UltraGrid grid = this.Layout.Grid as UltraGrid;

				Debug.Assert( null != grid, "UltraGridCell.CommitEditValue called on a grid that's not UltraGrid !" );

				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//EmbeddableEditorBase editor = this.Column.Editor;
				EmbeddableEditorBase editor = this.EditorResolved;

				Debug.Assert( null != editor, "No editor off the column !" );

				if ( null == editor )
					return false;

				// SSP 9/15/04 UWC117
				// If the cell has a formula and therefore it would be empty then return without
				// committing the edit value. The reason for this is that when a new row is added
				// it can end up with "#Calculating" word in one of its cells which will cause us 
				// to consider as an invalid value below and fire error event and show a message
				// box.
				//
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Use the cell's HasActiveFormula which checks for non-data rows (like a filter row).
				//
				//if ( this.Column.HasActiveFormula && ! this.Column.CanBeModified( this.Row ) )
				if ( this.HasActiveFormula && ! this.Column.CanBeModified( this.Row ) )
					return true;

				string message = null;
				bool valueValid = true;

				// SSP 6/12/02
				// OriginalValue doesn't necessarily correspond to what's in the list.
				// And we want to compare with the value in the list.
				//
				//object origVal = this.OriginalValue;
				object origVal = this.Value;
				object newVal = null;
				Exception exception = null;
			
				try
				{
					// JAS 2005 v2 XSD Support
					// Added support for detailed error messages to be retrieved from the editor
					// if the value was invalid due to one of the constraints imposed by the ValueConstraint.
					//
					//					newVal = editor.Value;
					if( editor.IsValidValue( out message ) )
					{
						newVal = editor.Value;
						valueValid = true;
					}
					else
					{
						newVal = editor.CurrentEditText;
						valueValid = false;
					}


					// JAS 2005 v2 XSD Support
					// This assertion no longer applies since we are using the IsValidValue method now
					// instead of the IsValid property.
					//
					//					Debug.Assert( editor.IsValid, "IsValid returned false while Value property returned a value. If IsValid is false, the accessing Value property should throw an exception.!" );
				
					// If the editor returned null, then use DBNull instead.
					//
					if ( null == newVal )
						newVal = DBNull.Value;
				}
					// ZS 12/24/03 - Added data filter support. DataFilterException is thrown if customer data 
					// filter code throws an exception. We want that exception to be propagated to the user
					// in every case.
				catch(DataFilterException)
				{
					throw;
				}
				catch ( Exception e )
				{
					Debug.Assert( !editor.IsValid, "Exception thrown while accessing Value property off " + editor.GetType( ).Name + " even though IsValid returned true." );

					exception = e;
					valueValid = false;
					message = "Value in editor is not valid.\n" + e.Message;

					newVal = editor.CurrentEditText;
				}

				// SSP 6/9/06 BR13443
				// In filter cell, always validate the operand against the operator. For example the operand
				// could be an invalid regular expression in which case we don't want to commit the value.
				// Typically the newVal and oldVal would be different however not when for example the
				// operator is changed to Match on a filter cell that already contained a value that's
				// invalid regex.
				// 
				bool checkIfValueIsOriginalValue = true;
				if ( this.IsFilterRowCell && this.editValueChanged )
					checkIfValueIsOriginalValue = false;

				// SSP 6/9/06 BR13443
				// Enclosed the existing code into the if block. This changed is related to above change.
				// 
				if ( checkIfValueIsOriginalValue )
				{
					// If the new value and the original values are the same then return true
					// without making any attempts to update the bound list.
					//
					if ( newVal == ( null != origVal ? origVal : DBNull.Value ) )
						return true;
					else if ( null != newVal && null != origVal )
					{
						// if the original value is DBNull and new value is empty,
						// then don't show the error
						//
						if ( newVal.Equals( this.Column.NullTextResolved ) && origVal is DBNull )
							return true;
						// if the new value is equal to the original value, then don't show the error 
						// either
						else
						{
							if ( newVal.GetType( ) == origVal.GetType( ) )
							{
								if ( newVal.Equals( origVal ) )
									return true;
							}
							else
							{
								object o1 = this.Column.ConvertValueToDataType( newVal );

								if ( null != o1 && o1.Equals( origVal ) )
								{
									return true;
								}
								// SSP 10/14/04
								// Even if the string representation of objects are different, allow
								// setting the value. The reason for this is that if the column is of
								// a custom object type which does not implement ToString method, then
								// we will still return without setting the value eventhough the objects
								// could be different in value. Above call to Equals should be 
								// sufficient.
								// Commented out below code.
								//
								
							}
						}
					}
				}
			
				// If the user has not modified the data, 
				// then return true without trying to update the cell. This handles the case
				// where user is moving from cell to cell without attempting the modify the
				// cell's value.
				//
				if ( !this.editValueChanged  )
					return true;

				if ( !valueValid || !editor.IsValid )
				{
					valueValid = false;

					// SSP 3/11/03 UWG2058 UWG2006
					// Use a better message then the exception thrown by the editor.
					// Commented out the if condition so we would always use the message
					// in the strings db.
					//
					// if ( null == message )
					//
					// JAS 2005 v2 XSD Support - Added the if() so that the error messages produced by the
					// ValueConstraint are not overwritten with the generic error message.
					//
					if ( null == message || message.Length == 0 )
						message = SR.GetString("ErrMSgEditorValNotValid");
				}

				// SSP 4/23/05 - NAS 5.2 Filter Row
				// Following code validates the filter cell operand input. For example if the operator
				// is Match (regular expression matching) then it validates that the operand is a valid
				// regular expression.
				//
				// ------------------------------------------------------------------------------------
				if ( valueValid && this.IsFilterRowCell )
				{
					UltraGridFilterRow filterRow = (UltraGridFilterRow)this.Row;
					string tmpErrorMessage;
					if ( ! filterRow.IsFilterCellInputValid( this.Column, newVal, out tmpErrorMessage ) )
					{
						valueValid = false;
						message = tmpErrorMessage;
					}
				}
				// ------------------------------------------------------------------------------------

				// If the column type is date, then validate the value against MinDate
				// and MaxDate settings.
				//
				//	We need to validate the cell's value here, to make sure it is
				//	within the range allowed by the MinDate/MaxDate properties
				if ( valueValid &&
					this.column.DataType == typeof( System.DateTime ) &&
                    // MRS 5/8/2008 - BR32612
                    false == this.IsFilterRowCell)
				{
					// If the value given is not a date, then try to convert it into
					// a date.
					//
					if ( !( newVal is DateTime ) )
					{
						// SSP 7/12/05 BR04785
						// For some reason DateTimeConverter (call to ConvertValueToDataType below 
						// eventually makes use of DateTimeConverter) converts empty string to 
						// some fixed date. This works around it.
						// 
						if ( newVal is string && 0 == ((string)newVal).Length )
							newVal = DBNull.Value;

						object convertedVal = this.Column.ConvertValueToDataType( newVal );

						if ( convertedVal is DateTime )
							newVal = convertedVal;							
					}

					// SSP 8/19/02
					// Embeddable editor will be checking for the input value being
					// in the range. So the grid shouldn't be doing that here.
					// Not only that, MinDate and MaxDate are obsoleted.
					//
					
				}
			
			
				if ( valueValid )
				{
					bool updateSucceeded = false;

					try
					{
						// SSP 12/12/01 UWG811
						// If the passed in val is empty string or DBNull, then try to
						// use the appropriate value according to Nullable property 
						// settings of the column.
						//
						// Use DBNull for bound columns and null for unbound columns.
						//							
						if ( null == newVal 
							|| ( ( newVal is string ) && 0 == ((string)newVal).Length )
							|| ( newVal is System.DBNull ) )
						{
                            // MBS 11/17/2008 - TFS10423
                            // Refactored into a helper method
                            //
                            #region Refactored

                            //// SSP 4/16/05 - NAS 5.2 Filter Row
                            //// Added NullableResolved to the cell. Also replaced calls in below code
                            //// to Nullable on column with nullableResolved local nullableResolved.
                            ////
                            //Nullable nullableResolved = this.NullableResolved;

                            //// MBS 11/29/06 BR18016
                            //// Though this would have fixed this bug, the main cause of the problem was in 
                            //// EditorWithText.  Still, after discussing it with Sandip and MikeS, we decided
                            //// that we shouldn't be checking whether the column is bound or not since we do not
                            //// do so for the other values of nullableResolved.
                            ////if ( Nullable.Automatic == nullableResolved && column.IsBound )
                            //if( Nullable.Automatic == nullableResolved )
                            //{
                            //    newVal = column.IsNullable ? System.DBNull.Value : (object)string.Empty;
							
                            //    // SSP 6/20/02 UWG1260
                            //    // If the data type of the column is boolean, there is no way
                            //    // it's going to accept a string. So ignore the Nullable.Automatic
                            //    // default setting of column.Nullable property. NOTE: The same
                            //    // applies to numberics and anything that can't coerce an empty 
                            //    // string into underlying data type. However, (Joe) decided we would
                            //    // do this for boolean for now (since the bug only mentions booleans).
                            //    //
                            //    // SSP 8/7/02 UWG1436
                            //    // For every data type except string, we should always try to
                            //    // use DBNull. IsNullable property always returns false since currently
                            //    // there is no way to find out if a column accepts nulls.
                            //    //
                            //    //if ( typeof( System.Boolean ) == column.DataType )
                            //    if ( typeof( string ) != column.DataType )							
                            //    {
                            //        newVal = DBNull.Value;
                            //    }
                            //}
                            //else if ( Nullable.EmptyString == nullableResolved )
                            //    newVal = string.Empty;
                            //else if ( Nullable.Null == nullableResolved )
                            //    newVal = System.DBNull.Value;
                            //    // SSP 11/3/03 UWG2700
                            //    // Added Nothing to Nullable. When it's Nothing, use null value.
                            //    //
                            //else if ( Nullable.Nothing == nullableResolved )
                            //    newVal = null;
                            //    // SSP 11/30/04 - BR02429 - Added Disallow to Nullable
                            //    // Don't allow empty values (null, DBNull, empty strings).
                            //    //
                            //else if ( Nullable.Disallow == nullableResolved )
                            //{
                            //    message = SR.GetString( "DataErrorCellUpdateEmptyValueNotAllowed", column.Header.Caption );
                            //    valueValid = false;
                            //}            
                            #endregion //Refactored
                            //
                            newVal = this.GetNullValue(out message, ref valueValid);
                        }


						// SSP 8/23/02
						// We don't need to apply the mask here since the EditorWithMask should give us
						// back a string with mask applied with the mode of DataMode.
						// Commented out below clock of code that applies the mask.
						//
						// -----------------------------------------------------------------------------
						
						// -----------------------------------------------------------------------------

						// SSP 11/30/04 - BR02429 - Added Disallow to Nullable
						// Related to the change above. Enclosed the existing code into the if block.
						//
						if ( valueValid )
						{
							// SSP 10/14/04 - UltraCalc
							// Store values of all the cells in a row when the user modifies a cell through UI so that
							// when we get ItemChanged notification on that row we do not notify the calc engine that
							// values of all the cells in the row changed even though only one cell's value might
							// have changed. The only way we can tell which cells' values changed is by storing the cell
							// values and comparing them.
							//
							// SSP 4/16/05 - NAS 5.2 Filter Row
							// Don't do this for a filter row.
							//
							// SSP 12/11/06 - Optimizations
							// Only do so if there is a calc manager. Stored cell values are used only when there
							// is a calc manager.
							// 
							//if ( this.Row.IsDataRow )
							if ( this.Row.IsDataRow && null != this.Layout.CalcManager )
								this.Layout.StoreCellValues( this.Row );

							// Now set the value to underlying bound list.
							//
							// SSP 11/29/05 - NAS 6.1 Multi-cell Operations
							// If Undo is enabled, then set the cell valule via the action history.
							// Added the if block and enclosed the existing code into the else block.
							// 
							// SSP 4/1/06 BR11228
							// Added an overload that takes in dontStoreInUndoHistory parameter.
							// 
							//if ( this.Band.IsMultiCellOperationAllowed( MultiCellOperation.Undo ) )
							if ( ! dontStoreInUndoHistory && this.Band.IsMultiCellOperationAllowed( MultiCellOperation.Undo ) )
							{
								ActionFactory.SingleCellValueAction setCellValueAction = 
									new ActionFactory.SingleCellValueAction( 
										this.Layout.ActionHistory, this, newVal );

								// SSP 4/1/06 BR11228
								// If the cell is going to remain in edit mode then the undo history should not
								// be updated with this cell update.
								// 
								if ( stayInEdit )
									setCellValueAction.DontExitEditMode = true;
                                
								this.Layout.ActionHistory.PerformAction( null, setCellValueAction );
							}
							else
							{
								// SSP 11/29/05 BR05398
								// Pass in false for the fireInitializeRow since we are firing the
								// InitializeRow below.
								// 
								//this.Value = newVal;
								this.SetValueInternal( newVal, false, false );
							}

							updateSucceeded = true;
						}
					}
					catch ( Exception exception1 )
					{
						updateSucceeded = false;
						exception = exception1;
						valueValid = false;
					}

					// Don't Update the row if we are moving from cell to cell
					// in the add row. This would cause an error to be
					// generated from the DB if more than one field
					// in the table was required.
					//
					if ( updateSucceeded )
					{
						// SSP 10/20/03 UWG2592
						// Reset the  editValueChanged which is set in Editor.ValueChanged.
						//
						// ----------------------------------------------------------------------
						this.editValueChanged = false;
						// ----------------------------------------------------------------------

						bool initializeRowFired = false;

						if ( null != grid &&
							( grid.UpdateMode == UpdateMode.OnCellChange  ||
							grid.UpdateMode == UpdateMode.OnCellChangeOrLostFocus ) &&
							null != this.Row && !this.Row.IsAddRow )
							this.Row.Update();
							// if we do not call Row.update we still
							// want to fire InitializeRow
							//
						else 
						{
							//RobA UWG633 11/7/01
							this.Row.FireInitializeRow();
							initializeRowFired = true;
						}

						//FireInitializeRow for unbound columns
						//
						if ( ( !this.Column.IsBound )
							&& !initializeRowFired )
						{
							//RobA UWG633 11/7/01						
							this.Row.FireInitializeRow();
						}
				

						// JM 01-02-02 - If we're in a CardView band and the card style is
						//				 Variable, dirty the data area so the cards can resize
						//				 and repaint.
						// SSP 2/28/03 - Row Layout Functionality
						// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
						// to StandardLabels card style so use the StyleResolved instead of the Style property.
						//
						// SSP 8/28/04
						// Refactoring for the better.
						//
						// ----------------------------------------------------------------------------
						
						if ( this.Row.IsCardStyleVariableHeight && null != this.Layout )
							this.Layout.DirtyDataAreaElement( );
						// ----------------------------------------------------------------------------

						// Dirty the cell in all the regions so it gets updated with the new
						// value.
						//
						this.InvalidateItemAllRegions( true );

						return true;
					}
				}

			
				if ( null != grid )
				{
					// SSP 10/16/02 UWG1116
					// Added new parameter fireDataError to the method. Only fire the DataError event
					// if that parameter is true. Otherwise just throw an exception.
					// Added if block and enclosed the already existing code into associated else
					// block.
					//
					if ( !fireDataError )
					{
						// SSP 4/23/05 - NAS 5.2 Filter Row
						// Added an overload of CommitEditValue that takes in forceDontThrowException.
						// Just return forceDontThrowException is true.
						//
						if ( forceDontThrowException )
							return false;

						if ( null != exception )
							throw exception;
						else
							throw new Exception( message );
					}
					else
					{
						// SSP 5/3/02 
						// Added new CellDataError event that we are supposed to fire
						// when the the user has typed in something invalid.
						//
						CellDataErrorEventArgs e = new CellDataErrorEventArgs( this.Layout.ForceExitEditOperationFlag );

						// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
						// 
						// ----------------------------------------------------------------------
						switch ( this.Column.InvalidValueBehaviorResolved )
						{
							case InvalidValueBehavior.RetainValueAndFocus:
								e.RestoreOriginalValue = false;
								e.StayInEditMode = true;
								break;
							case InvalidValueBehavior.RevertValue:
								e.RestoreOriginalValue = true;
								e.StayInEditMode = false;
								break;
							case InvalidValueBehavior.RevertValueAndRetainFocus:
								e.RestoreOriginalValue = true;
								e.StayInEditMode = true;
								break;
							default:
								Debug.Assert( false, "Unknown type of InvalidValueBehavior" );
								break;
						}
						// ----------------------------------------------------------------------

						grid.FireEvent( GridEventIds.CellDataError, e );

						if ( e.RaiseErrorEvent )
						{

							DataErrorInfo dataErrorInfo = new DataErrorInfo( 
								exception, 
								newVal, 
								this.Row, 
								this, 
								DataErrorSource.CellUpdate, 
								// SSP 3/11/03 UWG2058 UWG2006
								// Use a better message then the exception thrown by the editor.
								//
								// SR.GetString("DataErrorCellUpdateUnableToUpdateValue", null != exception ? exception.Message : "" ) );
								SR.GetString("DataErrorCellUpdateUnableToUpdateValue",  null != message && message.Length > 0 ? message : ( null != exception ? exception.Message : "" ) ) );

							grid.InternalHandleDataError( dataErrorInfo );
						}

						// If RestoreOriginalValue is true, then restore
						// the original value to the editor.
						//
						if ( e.RestoreOriginalValue )
						{
							editor.Value = this.Value;

							// Once we restore the value, since we now have the same value that we
							// began with, set the DataChaanged to false.
							//
							this.SetDataChanged( false );
						}
					
						stayInEdit = e.StayInEditMode;
					}
				}
			}
			finally
			{
				// SSP 10/24/02 UWG1782
				// Reset the flag to the original value in case this gets called
				// recursively, we don't want to prematurely reset it until the parent
				// CommitEditValue call finishes.
				this.inCommitEditValue = origInCommitEditValue;

				// SSP 10/14/04 - UltraCalc
				// Store values of all the cells in a row when the user modifies a cell through UI so that
				// when we get ItemChanged notification on that row we do not notify the calc engine that
				// values of all the cells in the row changed even though only one cell's value might
				// have changed. The only way we can tell which cells' values changed is by storing the cell
				// values and comparing them.
				//
				if ( null != this.Layout )
					this.Layout.NotifyCalcEngineOfChangedCellsInStoredRow( );

				// SSP 9/8/05 BR04897
				// Invalidate the row and the cell in case the data error information is changed
				// in one of the events and the underlying data row doesn't fire any notifications.
				// DataRow apparently doesn't fire notifications when it's in edit mode and it's
				// data error information is changed.
				// 
				this.Row.InvalidateItemAllRegions( true );
			}

			return false;
		}

        // MBS 11/17/08 - TFS10423
        // Refactored this from CommitEditValue so that we can get the proper 
        // value to use when doing a cut/copy/paste on a nullable field
        private object GetNullValue(out string message, ref bool valueValid)
        {
            // MRS 8/25/2009 - TFS20507
            // Moved this whole thing up to the column so that it can account for types that can accept
            // null, but not DBNull.
            //
            #region Old Code
            //object newVal = null;

            //// SSP 4/16/05 - NAS 5.2 Filter Row
            //// Added NullableResolved to the cell. Also replaced calls in below code
            //// to Nullable on column with nullableResolved local nullableResolved.
            ////
            //Nullable nullableResolved = this.NullableResolved;

            //// MBS 11/29/06 BR18016
            //// Though this would have fixed this bug, the main cause of the problem was in 
            //// EditorWithText.  Still, after discussing it with Sandip and MikeS, we decided
            //// that we shouldn't be checking whether the column is bound or not since we do not
            //// do so for the other values of nullableResolved.
            ////if ( Nullable.Automatic == nullableResolved && column.IsBound )
            //if (Nullable.Automatic == nullableResolved)
            //{
            //    newVal = column.IsNullable ? System.DBNull.Value : (object)string.Empty;

            //    // SSP 6/20/02 UWG1260
            //    // If the data type of the column is boolean, there is no way
            //    // it's going to accept a string. So ignore the Nullable.Automatic
            //    // default setting of column.Nullable property. NOTE: The same
            //    // applies to numberics and anything that can't coerce an empty 
            //    // string into underlying data type. However, (Joe) decided we would
            //    // do this for boolean for now (since the bug only mentions booleans).
            //    //
            //    // SSP 8/7/02 UWG1436
            //    // For every data type except string, we should always try to
            //    // use DBNull. IsNullable property always returns false since currently
            //    // there is no way to find out if a column accepts nulls.
            //    //
            //    //if ( typeof( System.Boolean ) == column.DataType )
            //    if (typeof(string) != column.DataType)
            //    {
            //        newVal = DBNull.Value;
            //    }
            //}
            //else if (Nullable.EmptyString == nullableResolved)
            //    newVal = string.Empty;
            //else if (Nullable.Null == nullableResolved)
            //    newVal = System.DBNull.Value;
            //// SSP 11/3/03 UWG2700
            //// Added Nothing to Nullable. When it's Nothing, use null value.
            ////
            //else if (Nullable.Nothing == nullableResolved)
            //    newVal = null;
            //// SSP 11/30/04 - BR02429 - Added Disallow to Nullable
            //// Don't allow empty values (null, DBNull, empty strings).
            ////
            //else if (Nullable.Disallow == nullableResolved)
            //{
            //    message = SR.GetString("DataErrorCellUpdateEmptyValueNotAllowed", column.Header.Caption);
            //    valueValid = false;
            //    return newVal;
            //}

            //message = null;
            //return newVal;
            #endregion // Old Code
            //
            return this.Column.GetNullValue(out message, ref valueValid);
        }

        // MBS 11/17/08 - TFS10423
        internal object GetNullValueForMultiCellOperation()
        {
            string message;
            bool isValid = true;
            object nullVal = this.GetNullValue(out message, ref isValid);
            if (isValid)
                return nullVal;
            
            // If we can't obtain a null value based on the NullableResolved property,
            // such as if the cell is set to Nullable.Disallow, try to use the 
            // DefaultCellValue on the column instead, since we need *something* to
            // put there during a Cut operation.
            return this.Column.DefaultCellValue;
        }

		internal bool ExitEditMode( bool cancellingEditOperation ,
			bool forceExit  )
		{
			//	BF 7.10.02
			if ( this.isExitingEditMode )
				return false;

			// if we are not in edit mode then return false
			// 
			if ( !this.IsInEditMode )
				return false;

			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if( null == grid )
				return false;

			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//EmbeddableEditorBase editor = this.Column.Editor;
			EmbeddableEditorBase editor = this.EditorResolved;
			Debug.Assert( null != editor, "Column.Editor returned null !" );
			if ( null == editor )
			{
				this.Layout.EmbeddableElementBeingEdited = null;
				this.IsInEditMode = false;
				return false;
			}

			
			// Let the Before/After Enter/Exit Edit Mode event handlers in the layout
			// know how we are exitting by setting these two flags.
			//
			this.Layout.ForceExitEditOperationFlag  = forceExit;
			this.Layout.CancellingEditOperationFlag = cancellingEditOperation;

			bool editorExittedEditMode = false;


			// SSP 7/25/02 UWG1407
			// When an editor exits the edit mode, its Focused property will return false.
			// So store it here to decide later on whether to give the focus to the grid
			// or not.
			//
			bool didEditorHaveFocusBeforeExittingEditMode = editor.Focused;

			//	BF 7.10.02
			//	I added an anti-recursion flag so we don't get into this method
			//	recursively; since the flag must be cleared (or the grid can never
			//	exit edit mode again) it is wrapped in a try/finally block
			try
			{
				// We are hooking into the editor's Enter/Exit edit mode events ant that's
				// where all the validating and updating of data occurs
				//
				this.isExitingEditMode = true;
				editorExittedEditMode = editor.ExitEditMode( forceExit, !cancellingEditOperation );			
			}
			finally
			{
				this.isExitingEditMode = false;
			}

			// If the editor exitted the edit mode, then set the EmbeddableElementBeingEdited
			// off the layout to null.
			//
			if ( editorExittedEditMode )
				this.Layout.EmbeddableElementBeingEdited = null;

			if ( forceExit )
				Debug.Assert( editorExittedEditMode, "ExitEditMode on editor failed even though forceExit parameter was true." );

			// If the editor did not exit the edit mode, either due to invalid input or
			// user cancaling the before exit edit mode event, return true.
			//
			if ( !editorExittedEditMode )
			{
				// Give the focus back to the editor before returning.
				//
				if ( editor.CanFocus )
				{
					// AS - 1/14/02
					// We need to assert putting focus on the control in order for this to work
					// when the assembly is local but the executable is running under internet
					// type restricted security access. However, to prevent a security hole, we
					// will only do so on controls that have our public key token.
					//
					try
					{
                        // MRS 12/19/06 - fxCop
                        // This is unneccessary and opens a security hole. The editor.Focus
                        // method will handle any neccessary permissions. 
                        //if ( DisposableObject.HasSamePublicKey( editor.GetType() ))
                        //{
                        //    System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                        //    perm.Assert();
                        //}

                        editor.Focus();
					}
					catch {}
				}
                else
                {
                    // MRS 4/15/2008 - BR32012
                    // If the editor does not take focus, then the grid should. 
                    //
                    try
                    {
                        if (this.Layout != null &&
                            this.Layout.Grid != null &&
                            this.Layout.Grid.CanFocus)
                        {
                            this.Layout.Grid.Focus();
                        }
                    }
                    catch { }
                }

				return true;
			}

			// At this point, the editor would have exitted the edit mode.
			//
			Debug.Assert( !editor.IsInEditMode, "Editor still in edit mode event though ExitEditMode returned success !" );

			// set our edit mode flag to false
			//
			this.IsInEditMode = false;

			// SSP 7/8/03 - Support devepoper calling EnterEditMode on the editor directly.
			// Added OnAfterCellEnterEditMode and OnAfterCellExitEditMode methods and moved
			// the logic in there so we have the necessary logic in centralized location
			// and handle the developer calling EnterEditMode on the editor directly.
			// Moved below code in one of the above mentioned methods.
			//
			

			// If the editor still has the focus, then set the focus back to the
			// grid.
			//
			// SSP 7/25/02 UWG1407
			// If the editor had focus, give the focus back to the grid.
			//
			//if ( editor.Focused )
			// SSP 12/6/02 UWG1877
			// Don't give foucs back to the grid if we are being called from OnLeave of the grid.
			//
			//if ( didEditorHaveFocusBeforeExittingEditMode )
			if ( !grid.inOnLeave && didEditorHaveFocusBeforeExittingEditMode )
			{
				// AS - 1/14/02
				// We need to assert putting focus on the control in order for this to work
				// when the assembly is local but the executable is running under internet
				// type restricted security access. However, to prevent a security hole, we
				// will only do so on controls that have our public key token.
				//
				//this.Layout.Grid.Focus();
				try
				{
                    // MRS 12/19/06 - fxCop
                    //if ( DisposableObject.HasSamePublicKey( this.Layout.Grid.GetType() ))
                    //{
                    //    System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                    //    perm.Assert();
                    //}

                    //// MD 11/7/06 - BR17484
                    ////this.Layout.Grid.Focus();
                    //Utilities.FocusControlWithoutScrollingIntoView( this.Layout.Grid );
                    GridUtils.FocusControlWithoutScrollingIntoView(this.Layout.Grid);
				}
				catch {}
			}
		
			this.InvalidateItemAllRegions( true );

			// SSP 7/25/02 UWG1407
			// Invalidating is enough. We don't need to update. Updating causes the focus rect
			// to be drawn for a split second on this cell when the user clicks on a differnt cell.
			//
			//if ( null != this.Layout.Grid )
			//	this.Layout.Grid.Update( );

			Debug.Assert( !editor.IsInEditMode && !this.IsInEditMode, "Still in edit mode !" );
			
			return false;
		}


		// SSP 4/29/02
		// EM Embeddable editors.
		// Commented out ResolveValueListItemAppearance because any value list item appearance
		// will be resolved by the editor and drawn properly.
		//
		


		// SSP 8/2/02
		// EM Embeddable editors.
		// Not being used anywhere. So commented it out.
		// 
		
		
		/// <summary>
		/// Activates cell. 
		/// </summary>
		/// <returns>Returns true if the operation was cancelled.</returns>
		public bool Activate()
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_37"));

			// SSP 1/27/05 BR02053
			// Only do this for the display layout. Print and export layouts should not be
			// modifying the ActiveRow of the grid.
			//
			if ( ! this.Layout.IsDisplayLayout )
				return true;

			//
			if ( this.Layout.ActiveCell == this )
				return false;
			
			// Resolve the cell's Activation property.  If ssActivationDisabled, return true.
			if ( IsDisabled )
				return true;

			// save previous row in case we must update it below
			// SSP 1/27/05
			// 
			//Infragistics.Win.UltraWinGrid.UltraGridRow previousRow = this.Layout.Grid.ActiveRow;
			Infragistics.Win.UltraWinGrid.UltraGridRow previousRow = this.Layout.ActiveRow;

            // MBS 4/2/09
            UltraGridCell oldActiveCell = this.Layout.ActiveCellInternal;

			// check to see if this cell's row is the current active row.
			//
			if ( this.Row != this.Layout.Grid.ActiveRow )
			{
				// it isn't the active row so call put_ActiveRow to activate it
				//
				//this.Layout.Grid.ActiveRow = this.Row;
				// RobA 12/28/01 Use SetActiveRow so that we can
				// set the active row without scrolling it into view
				//
				this.Layout.Grid.SetActiveRow( this.Row, false );

				// if it was cancelled then return
				//
				if ( this.Layout.Grid.ActiveRow != this.Row )
					return true;

				// clear the active cell
				grid.ActiveCell = null;
			}

			// first try to exit the edit mode of the cell
			// if cancelled return true
			//
			if ( this.Layout.ActiveCell != null && this.Layout.ActiveCell.ExitEditMode() )
				return true;

			// SSP 8/26/04 UWG3100
			// Recheck the disabled flag in case the Activation setting of this cell gets
			// changed in one of the events associated with exiting the edit mode of the
			// current active cell.
			//
			// --------------------------------------------------------------------------
			if ( this.IsDisabled )
				return true;
			// --------------------------------------------------------------------------
			
			// Clear the active cell first to allow the BeforeCellDeactivate event
			// to fire before we fire BeforeCellActivate on this cell below
			//
			grid.ActiveCell = null;

			// If the form was unloaded in the event above then return true
			// to cancel
			//
			if ( this.Layout.Grid == null )
				return true;

			// If BeforeCellDeactivate was cancelled then return true
			//
			if ( this.Layout.ActiveCell != null )
				return true;
			
			// SSP 3/18/02 UWG1052
			// Don't fire the BeforeCellActivate event here because it's fired in
			// UltraGrid.ActiveCell property.
			//
				
			
			// If the form was unloaded in the event above then return true
			// to cancel
			//
			if ( this.Layout.Grid == null )
				return true;
			

			//Don't Update the row if we are moving from cell to cell
			// in the add row. This would cause an error to be
			// generated from the DB if more than one field
			// in the table was required.

			if ( Infragistics.Win.UltraWinGrid.UpdateMode.OnCellChange == grid.UpdateMode )
			{
				if ( null != previousRow && !previousRow.IsAddRow )
					previousRow.Update();
			}
			
			grid.ActiveCell = this;

			// SSP 3/18/02 UWG1052
			// Since ActiveCell fires OnBeforeCellActivate, so if it was cancelled the
			// return.
			//
			if ( grid.ActiveCell != this )
				return true;


			// scroll the cell's row into view in the this row scroll region
			//
			this.Layout.Grid.ActiveRowScrollRegion.ScrollRowIntoView( this.Row );

			// scroll the cell's column into view in the active col scroll
			// region
			//
			this.Layout.Grid.ActiveColScrollRegion.ScrollColIntoView( this.column );

            // MBS 4/2/09 
            // As with the current active cell, mentioned below, the old active cel
            // could be diplayed simultaneously in muliple regions, so we should
            // ensure that we invalidate all of them.
            if (oldActiveCell != null)
                oldActiveCell.InvalidateItemAllRegions(false);

			// SSP 8/21/01 UWG191
			// When a cell is activated, we need to repaint the cell so that
			// any drop down button that need to be displayed get displayed
			// SSP 11/22/04 - Optimization
			// Use InvalidateItem instead.
			
            //
            // MBS 4/2/09
            // The active cell could be displayed in multiple regions at the same time, so we really 
            // need to invalidate this cell across all regions.
            //
			//this.InvalidateItem( false );
            this.InvalidateItemAllRegions(false);

			// SSP 3/18/02 UWG1052
			// Don't fire the AfterCellActivate because it gets fired in 
			// UltraGrid.ActiveCell property.
			//
			
			
			// If the form was unloaded in the event above then return true
			// to cancel
			//
			return false;

		}
		internal override bool SetFocusAndActivate() 
		{
			return this.SetFocusAndActivate ( true, false, false );
		}

		internal override bool SetFocusAndActivate(bool byMouse)
		{
			return this.SetFocusAndActivate ( byMouse, false, false );
		}

		internal override bool SetFocusAndActivate( bool byMouse, bool enterEditMode )
		{
			return this.SetFocusAndActivate ( byMouse, enterEditMode, false );
		}
		internal override bool SetFocusAndActivate( bool byMouse, bool enterEditMode, bool byTabKey )
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_37"));

			// SSP 5/9/02
			// Not needed anymore since the embeddable editors take care of dropping it 
			// down when the button is clicked upon.
			//
			

			// SSP 5/9/02
			// Not needed anymore since the embeddable editors take care of dropping it 
			// down when the button is clicked upon.
			//
			//			// SSP 11/21/01 UWG670
			//			// Also wrapped this block of code into a try-finally block
			//			// so that we can reset the above flag at the end.
			//			//
			//			try
			//			{

			if ( this.Activate() )
				return true;

			//If cell was put into edit mode as part of Activate call (by
			// PerformAction), do not continue with normal processing - otherwise
			// we could put ourselves into an illegal state: selected and
			// in edit mode 
			if ( this.IsInEditMode )
				return true;
			
			// enter edit mode if by tab key and not HTML and cellclickaction is edit
			if ( enterEditMode || ( byTabKey  ) )
			{
				this.EnterEditMode( byMouse );
								
				//Set pivotItem when entering edit mode on a cell so that
				//  shift-down arrow works as expected
				grid.SetPivotItem( this, false );

			}
			return false;

			// SSP 5/9/02
			// Look at the comment above respective try 
			//
			//			}
			//			finally
			//			{
			//				this.isCursorOverDropDownArrowOverrideFlag = DefaultableBoolean.Default;
			//			}
		}

		// SSP 5/9/02
		// Not needed anymore since the embeddable editors take care of dropping it 
		// down when the button is clicked upon. How it works is that in the OnMouseDown
		// of the embeddable editor's drop down ui elemnet, the editor raises MouseDown
		// and the grid's handler for that event puts the editor into edit mode. Once the 
		// control returns to the editor, it checks if it went into edit mode and if so
		// to drop down the drop down. And thus we have drop downs working without requiring
		// below hackery.
		//
		

		// SSP 5/2/03
		// Commented below method out because it's not being used anywhere.
		//
		

		internal bool EnterEditMode()
		{
			return this.EnterEditMode( true, false );
		}

		internal bool EnterEditMode( bool byMouse )
		{
			return this.EnterEditMode( byMouse, false );
		}

		// SSP 4/5/02
		// Look at the new implementation of EnterEditMode for embeddable editors.
		//
		

		// SSP 4/8/02
		// Added this inetranl method to set the isInEditMode flag to 
		// desired value.
		//
		internal void InternalSetInEditModeFlag( bool val )
		{
			this.isInEditMode = val; 
		}

		// SSP 7/8/03 - Support developer calling EnterEditMode on the editor directly.
		// Added OnAfterCellEnterEditMode and OnAfterCellExitEditMode methods and moved
		// the logic in there so we have the necessary logic in centralized location
		// and handle the developer calling EnterEditMode on the editor directly.
		//
		// SSP 4/16/05 - NAS 5.2 Filter Row
		// Added UltraGridFilterCell class.
		//
		//internal void OnAfterCellEnterEditMode( )
		internal virtual void OnAfterCellEnterEditMode( )
		{
			// Set the flag to true if the editor went into the edit mode
			//
			this.isInEditMode = true;

            // MBS 3/4/08 - RowEditTemplate NA2008 V2
            // We only want to perform this logic when we're not editing with a proxy,
            // since otherwise the grid's cell isn't actually being used for editing,
            // nor is it necessarily visible (and thus will not have a UIElement).
            if (this.Layout.ActiveProxy == null)
            {
                // Set the editor that's being edited so that we know to use
                // that one instead of potentially creating a new one on
                // a verification.
                //
                CellUIElementBase cellElem = (CellUIElementBase)this.GetUIElement(
                    this.Layout.ActiveRowScrollRegion, this.Layout.ActiveColScrollRegion, false);

                Debug.Assert(null != cellElem, "No cell element for the cell that's in edit mode !");

                // Get the descendat embeddable ui elemnet.
                // NOTE: GetDescendant was recently modified to look for instances of classes
                // derived from the passed in type. So this is going to work.
                //
                // SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
                // Use the FindEmbeddableUIElement instead which searches only the child elements.
                // In filter cells there can be two embeddable elements. One for the operand and 
                // the other for operator. Here we are interested in getting the operand editor 
                // element and since the operator editor element not an immediate child (unlike
                // the operand editor element which) using the FindEmbeddableUIElement gets us
                // the operand editor element.
                // 
                //EmbeddableUIElementBase embeddableElement = null != cellElem ?
                //	(EmbeddableUIElementBase)cellElem.GetDescendant( typeof( EmbeddableUIElementBase ) ) : null;
                EmbeddableUIElementBase embeddableElement = null != cellElem ? cellElem.FindEmbeddableUIElement(false) : null;

                this.Layout.EmbeddableElementBeingEdited = embeddableElement;
                this.Layout.EmbeddableElementBeingEdited_ColScrollRegion = this.Layout.ActiveColScrollRegion;
                this.Layout.EmbeddableElementBeingEdited_RowScrollRegion = this.Layout.ActiveRowScrollRegion;
            }
				
			// Reset this flag to false before going into edit mode so when we
			// are coming out of it we can check if it has been set. It gets
			// set to true whenever ValueChanged is fired by the editor so we
			// know that the user has modified the data.
			//
			this.editValueChanged = false;

			// SSP 12/6/04 Merged Cell Feature
			//
			RowsCollection.MergedCell_CellStateChanged( this, MergedCell.State.EnterEditMode );

			// Ensure that we get hooked properly from the edit control.
			// Added check for the control. Now we are keeping track of the
			// control we were last hooked into in case the control changes.
			//
			this.HookValueChangedIntoEditor( this.EditorResolved );

            // MBS 3/4/08 - RowEditTemplate NA2008 V2
            // We will have already called BeginEdit when showing a RowEditTemplate, so if there is
            // an active proxy, we don't need to call this again.  We also don't need to bother 
            // scrolling the cell into view since the template is being used for editing.
            if (this.Layout.ActiveProxy == null)
            {
                this.Row.BeginEdit();

                // Scroll the cell into view.
                //
                this.Layout.Grid.ActiveRowScrollRegion.ScrollRowIntoView(this.Row, false);
                this.Layout.Grid.ActiveColScrollRegion.ScrollColIntoView(this.Column);
            }

			// SSP 9/19/03 UWG2579
			// Moved this code from EnterEditorIntoEditMode method to here because we want
			// to set the selected text before firing AfterEnterEditMode on the grid in
			// case user wants to set different selection.
			//
			// ------------------------------------------------------------------------------

			// JAS 4/6/05 v5.2 SelectTextOnEnterEditMode
			// The following logic determines whether the text should be selected or not.
			//
			// The original implementation (i.e. default behavior) is to only select the 
			// cell's text if it entered edit mode via the Tab key.  
			bool selectAll = ! this.Layout.goingIntoEditModeByMouse;

			// Determine if the cell's text should be selected or not.
			if( this.Layout.goingIntoEditModeByMouse &&
				this.Layout.HasOverride && 
				// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
				//this.Layout.Override.CellClickAction == CellClickAction.EditAndSelectText )
				this.CellClickActionResolved == CellClickAction.EditAndSelectText )
				selectAll = true;

			// Either select or deselect the text in the cell.
			if( selectAll )
				this.SelectAll();
			
			//
			// JAS 4/6/05 v5.2 SelectTextOnEnterEditMode
			// Moved the text selection logic into the public SelectAll method.
			//
			//			EmbeddableEditorBase editor = this.EditorResolved;
			//			if ( ! this.Layout.goingIntoEditModeByMouse && null != editor && editor.SupportsSelectableText )
			//			{
			//				editor.SelectionStart = 0;
			//				editor.SelectionLength = editor.TextLength;
			//			}
			// ------------------------------------------------------------------------------

			// SSP 9/9/05 BR06315
			// Since we don't apply the hot-tracking appearance while a cell is in edit mode
			// when the cell exits the edit mode, invalidate the hot-tracked element so it 
			// redraws with the hot-tracked appearance.
			// 
			this.Layout.InvalidateHotTrackedElement( );
		}

		// JAS 4/6/05 v5.2 SelectTextOnEnterEditMode
		//
		/// <summary>
		/// Selects all of the text in the cell.  If the cell does not contain text then this method does nothing.
		/// </summary>
		public void SelectAll()
		{
			EmbeddableEditorBase editor = this.EditorResolved;
			if ( null != editor && editor.SupportsSelectableText )
			{
				// SSP 3/17/06 BR10825
				// For the EditorWithMask use the SelectAll method. Added the if block and enclosed
				// the existing code into the else block.
				// 
				if ( editor is EditorWithMask )
				{
					((EditorWithMask)editor).SelectAll( );
				}
				else
				{
					editor.SelectionStart = 0;
					editor.SelectionLength = editor.TextLength;
				}
			}
		}

		// SSP 7/8/03 - Support developer calling EnterEditMode on the editor directly.
		// Added OnAfterCellEnterEditMode and OnAfterCellExitEditMode methods and moved
		// the logic in there so we have the necessary logic in centralized location
		// and handle the developer calling EnterEditMode on the editor directly.
		//
		// SSP 4/16/05 - NAS 5.2 Filter Row
		// Added UltraGridFilterCell class.
		//
		//internal void OnAfterCellExitEditMode( )
		internal virtual void OnAfterCellExitEditMode( )
		{
			// set our edit mode flag to false
			//
			this.IsInEditMode = false;

			this.Layout.EmbeddableElementBeingEdited = null;

			// SSP 6/30/03 UWG2271
			// Reset the flag once we are out of edit mode. This flag gets set when we get ValueChanged from
			// the editor (which can only happen when the editor is in edit mode). We were already resetting 
			// this flag in EnterEditMode, however the fix for UWG2271 also requires that we reset it here.
			//
			if ( this.editValueChanged )
			{
				this.editValueChanged = false;
				
				if ( ! this.DataChanged && ! this.Row.DataChanged )
				{
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DataChanged );
					this.Row.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DataChanged );
				}
			}

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			// Since we've exitted the edit mode on the cell, reset the CellGoingIntoEditMode property
			// to null so the associated cell element doesn't put an embeddable element if the cell
			// display style is FormattedText or PlainText.
			//
			this.Layout.CellGoingIntoEditMode = null;

			// Unhook from the editor since we have exitted the edit mode.
			//
			this.Layout.UnHookFromEditor( );
			this.UnhookValueChangedFromEditor( );

			this.InvalidateItemAllRegions( true );

			// SSP 12/6/04 Merged Cell Feature
			//
			RowsCollection.MergedCell_CellStateChanged( this, MergedCell.State.ExitEditMode );

			// SSP 9/9/05 BR06315
			// Since we don't apply the hot-tracking appearance while a cell is in edit mode
			// when the cell exits the edit mode, invalidate the hot-tracked element so it 
			// redraws with the hot-tracked appearance.
			// 
			this.Layout.InvalidateHotTrackedElement( );
		}

		// SSP 4/5/02
		// Reimplementation of ShowEditCtl for embeddable editors. The original method is
		// commented out.
		//
		internal bool EnterEditorIntoEditMode( bool byMouse )
		{
			// NOTE: THis code was copied over from original ShowEditCtl method

			// SSP 11/14/01 UWG603
			// Dirty the cell element show that if EditCellAppearance has any
			// settings (like Image for example) that would possibly offset the 
			// text element effecting the position where the text box would be
			// placed.
			//
			this.InvalidateItem(
				this.Band.Layout.ActiveRowScrollRegion,
				this.Band.Layout.GetActiveColScrollRegion( false ),
				true );

			UltraGrid grid = this.Layout.Grid as UltraGrid;
			
			Debug.Assert( null != grid, "ShowEditCtrl called on a grid that's not UltraGrid." );

			if ( null == grid )
				return false;

			// Use UIElement to get the rect
			//
			// SSP 5/1/03 - Cell Level Editor
			// Added Editor and ValueList properties off the cell so the user can set the editor
			// and value list on a per cell basis.
			//
			//EmbeddableEditorBase editor = this.Column.Editor;
			EmbeddableEditorBase editor = this.EditorResolved;

			Debug.Assert( null != editor, "Column.Editor returned null !" );
			if ( null == editor )
				return false;

			// SSP 10/23/03 UWG2001
			// If the main element's Rect has not been initialized, then initialize it.
			//
			// ------------------------------------------------------------------------
			this.Layout.EnsureUIElementRectInitialized( );
			// ------------------------------------------------------------------------

			// Pass true into GetUIElement to trigger a verification of the 
			// sub element rects
			//
			Infragistics.Win.UIElement mainElement = this.Layout.GetUIElement( true );

			Debug.Assert( null != mainElement, "Layout.GetUIElement returned null !" );
			if ( null == mainElement )
				return false;
			
			// Get the cell element in active row and col scroll regions.
			//
			CellUIElementBase cellElement = (CellUIElementBase)mainElement.GetDescendant( 
				typeof( CellUIElementBase ), 
				new object[] { this.Layout.Grid.ActiveRowScrollRegion,
								 this.Layout.Grid.ActiveColScrollRegion,
								 this.Row,
								 this }  );

			Debug.Assert( null != cellElement, "Cell has no associated cell ui element in EnterEditorIntoEditMode !" );
			if ( null == cellElement )
				return false;

			// SSP 5/12/03 - Optimizations
			// Added a way to just draw the text without having to embedd an embeddable ui element in
			// cells to speed up rendering.
			//
			// ----------------------------------------------------------------------------------------
			CellDisplayStyle cellDisplayStyle = this.Column.GetCellDisplayStyleResolved( this.Row );
			this.Layout.CellGoingIntoEditMode = this;
			if ( CellDisplayStyle.FullEditorDisplay != cellDisplayStyle )
			{
				cellElement.DirtyChildElements( true );
				cellElement.VerifyChildElements( true );
			}
			// ----------------------------------------------------------------------------------------

			// Get the descendat embeddable ui elemnet.
			// NOTE: GetDescendant was recently modified to look for instances of classes
			// derived from the passed in type. So this is going to work.
			//
			// SSP 4/16/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			// Use the FindEmbeddableUIElement instead which searches only the child elements.
			// In filter cells there can be two embeddable elements. One for the operand and 
			// the other for operator. Here we are interested in getting the operand editor 
			// element and since the operator editor element not an immediate child (unlike
			// the operand editor element which) using the FindEmbeddableUIElement gets us
			// the operand editor element.
			// 
			//EmbeddableUIElementBase embeddableElement =
			//	(EmbeddableUIElementBase)cellElement.GetDescendant( typeof( EmbeddableUIElementBase ) );
			EmbeddableUIElementBase embeddableElement = cellElement.FindEmbeddableUIElement( false );

			Debug.Assert( null != embeddableElement, "Cell element has no descendant embeddale ui element in EnterEditorIntoEditMode !" );
			if ( null == embeddableElement )
				return false;

			Debug.Assert( editor == embeddableElement.Editor, "Editor off the column does not match the editor off the embeddable ui element. !" );
			if ( editor != embeddableElement.Editor )
				return false;

			// Hook into the editor' Before/After Enter/Exit edit mode events as well
			// as key press/down/up events if necessary.
			//
			this.Layout.HookIntoEditor( editor );

			// If the column is multiline, then set accepts return on the editor with text
			// so pressing enter key enters a new line.
			//
			if ( DefaultableBoolean.True == column.CellMultiLineResolved )
			{
				EditorWithText editorText = editor as EditorWithText;

				if ( null != editorText )
					editorText.AcceptsReturn = true;
			}

			// SSP 4/8/03 UWG2113
			// Reset the autosize edit event args to null.
			//
			if ( ! editor.IsInEditMode )
				this.Layout.LastAutoSizeEditEventArgs = null;

			// SSP 7/8/03 - Support devepoper calling EnterEditMode on the editor directly.
			// Added OnEditorBeforeEnterEditMode virtual method off the owner which gets
			// called when the EnterEditMode is called on the editor. However we don't
			// want to execute the code there if we are calling it ourselves because
			// we already have done all that.
			//
			// SSP 4/18/05 - NAS 5.2 Filter Row
			// Use the new EditorOwnerInfo property of the cell because filter row cells 
			// use different owners.
			//
			//this.Column.EditorOwnerInfo.dontHandleOnBeforeEnterEditMode = true;
			this.EditorOwnerInfo.dontHandleOnBeforeEnterEditMode = true;

			bool success = false;
			// SSP 10/16/03 UWG2716
			// Although this method should not get called recursively, it might if some editors
			// call Owner.EmbeddableMouseDown again from within EnterEditMode call which they should
			// not. However EditorWithText did it because it stimulated MouseDown by sending a
			// MouseDown to the textbox which in turn forwarded the MouseDown to the Owner which
			// ended up calling this method recursively. Although I fixed that in the EditorWithText,
			// I am implementing this measure just to be on the safe side.
			//
			bool origGoingIntoEditModeByMouse = this.Layout.goingIntoEditModeByMouse;
			try
			{
				// SSP 7/14/03 
				// Set the EmbeddableElementBeingEdited and EmbeddableElementBeingEdited CSR/RSR
				// before going into the edit mode in case we receive a verification during
				// editor.EnterEditMode call.
				// 
				this.Layout.EmbeddableElementBeingEdited = embeddableElement;
				this.Layout.EmbeddableElementBeingEdited_ColScrollRegion = this.Layout.ActiveColScrollRegion;
				this.Layout.EmbeddableElementBeingEdited_RowScrollRegion = this.Layout.ActiveRowScrollRegion;

				// SSP 9/19/03 UWG2579
				// 
				this.Layout.goingIntoEditModeByMouse = byMouse;
				
				success = editor.EnterEditMode( embeddableElement );
			}
			finally
			{
				// SSP 9/19/03 UWG2579
				// 
				// SSP 10/16/03 UWG2716
				//
				//this.Layout.goingIntoEditModeByMouse = false;
				this.Layout.goingIntoEditModeByMouse = origGoingIntoEditModeByMouse;

				// SSP 4/18/05 - NAS 5.2 Filter Row
				// Use the new EditorOwnerInfo property of the cell because filter row cells 
				// use different owners.
				//
				//this.Column.EditorOwnerInfo.dontHandleOnBeforeEnterEditMode = false;
				this.EditorOwnerInfo.dontHandleOnBeforeEnterEditMode = false;

				// SSP 7/14/03 
				// If entering edit mode was not successful, then reset these variables to null.
				// 
				if ( ! success )
				{
					this.Layout.EmbeddableElementBeingEdited = null;
					this.Layout.EmbeddableElementBeingEdited_ColScrollRegion = null;
					this.Layout.EmbeddableElementBeingEdited_RowScrollRegion = null;

					// SSP 10/24/06 BR16897
					// Also null out the CellGoingIntoEditMode if entering edit mode was cancelled.
					// 
					this.Layout.CellGoingIntoEditMode = null;
				}
			}

			if ( success && editor.IsInEditMode )
			{			
				// SSP 7/8/03 - Support developer calling EnterEditMode on the editor directly.
				// Added OnAfterCellEnterEditMode and OnAfterCellExitEditMode methods and moved
				// the logic in there so we have the necessary logic in centralized location
				// and handle the developer calling EnterEditMode on the editor directly.
				// Moved below code in one of the above mentioned methods.
				//
				

				// SSP 4/8/03 UWG2113
				// We have to do this before the editor enters the edit mode or at least
				// before it accesses the EmbeddableOwnerInfo.GetAutoSizeEditInfo. Here
				// we are doing all this after we are entering the edit mode above. It
				// used to work before we changed the behavior of the embeddable text box
				// to syncronously display the text box.
				// Commented out below code and moved it into the OnEditorBeforeEnterEditMode
				// off the layout.
				// 
				
		
				this.InvalidateItem( );

				// Give focus to the embeddable editor.
				//
				if ( editor.CanFocus )
				{
					// AS - 1/14/02
					// We need to assert putting focus on the control in order for this to work
					// when the assembly is local but the executable is running under internet
					// type restricted security access. However, to prevent a security hole, we
					// will only do so on controls that have our public key token.
					//
					try
					{
                        // MRS 12/19/06 - fxCop
                        // This is unneccessary and opens a security hole. The editor.Focus
                        // method will handle any neccessary permissions. 
                        //if ( DisposableObject.HasSamePublicKey( editor.GetType() ))
                        //{
                        //    System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                        //    perm.Assert();
                        //}

                        editor.Focus();
					}
					catch {}
				}

				// SSP 9/19/03 UWG2579
				// Moved this code from here to OnAfterCellEnterEditMode method because we want to
				// do this before firing AfterEnterEditMode on the grid in case user wants to set 
				// different selection.
				//
				// ----------------------------------------------------------------------------------
				
				// ----------------------------------------------------------------------------------
			
				// Call update to repaint the cell right away otherwise there will be
				// a flicker.
				//
				grid.Update( );

				// SSP 7/8/03 - Support devepoper calling EnterEditMode on the editor directly.
				// Added OnAfterCellEnterEditMode and OnAfterCellExitEditMode methods and moved
				// the logic in there so we have the necessary logic in centralized location
				// and handle the developer calling EnterEditMode on the editor directly.
				// Moved below code in one of the above mentioned methods.
				//
				//this.Row.BeginEdit( );
			}

			// Return true since we did not encounter any abnormalities.
			//
			return true;
		}


        internal bool EnterEditMode(bool byMouse, bool showingDropDown)
        {
            UltraGrid grid = this.Layout.Grid as UltraGrid;

            if (null == grid)
                throw new NotSupportedException(Shared.SR.GetString("LE_V2_NotSupportedException_309"));

            // do not allow the cell to enter edit mode while at design time
            if (grid.DesignMode)
                return true;

            // For column style button, we do not enter into edit mode.
            //
            // SSP 5/1/03 - Cell Level Editor
            // Added Editor and ValueList properties off the cell so the user can set the editor
            // and value list on a per cell basis.
            //
            //if ( ColumnStyle.Button == this.Column.StyleResolved )
            if (ColumnStyle.Button == this.StyleResolved)
                return true;

            // SSP 5/1/03 - Cell Level Editor
            // Added Editor and ValueList properties off the cell so the user can set the editor
            // and value list on a per cell basis.
            //
            //EmbeddableEditorBase editor = this.Column.Editor;
            EmbeddableEditorBase editor = this.EditorResolved;

            if (null == editor)
            {
                Debug.Assert(false, "Editor property off the column returned null.");

                return false;
            }

            // if we are already in edit mode then return false
            //
            if (this.IsInEditMode)
                return false;

            // Check if the cell can enter edit mode
            //
            if (!this.CanEnterEditMode)
                return true;


            // If the grid or one of its children doesn't have focus then
            // we can't enter edit mode
            //
            // AS - 1/14/02
            // We need to assert putting focus on the control in order for this to work
            // when the assembly is local but the executable is running under internet
            // type restricted security access. However, to prevent a security hole, we
            // will only do so on controls that have our public key token.
            //				
            // SSP 3/19/04 UWG3085
            // Instead of using Focused, use ContainsFocus. If we are already in the process
            // of entering edit mode then an editor might have focus, in which case we
            // don't want to steal the focus back from the editor.
            //
            //if (!this.Layout.Grid.Focused)
            if (!this.Layout.Grid.ContainsFocus)
            {
                try
                {
                    // MRS 12/19/06 - fxCop
                    //try
                    //{
                    //    if ( DisposableObject.HasSamePublicKey( this.Layout.Grid.GetType() ))
                    //    {
                    //        System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );

                    //        perm.Assert();
                    //    }

                    //    this.Layout.Grid.Focus();
                    //}
                    //catch {}
                    if (DisposableObject.HasSamePublicKey(this.Layout.Grid.GetType()))
                        GridUtils.FocusControl(this.Layout.Grid);
                    else
                        this.Layout.Grid.Focus();
                }
                catch { }
            }

            if (!this.Layout.Grid.Focused)
            {
                // AS 12/9/04 BR00838
                // The control itself may not be focused but it may contain the focused
                // control. In this case we should only exit if we are already
                // entering edit mode.
                //
                if (this.Layout.Grid.ContainsFocus)
                {
                    // we should only exit if we are already entering edit mode
                    // otherwise we'll exit even though we aren't in edit mode
                    if (editor.IsEnteringEditMode &&
                        editor.ElementBeingEdited != null &&
                        // SSP 4/18/05 - NAS 5.2 Filter Row
                        // Use the new EditorOwnerInfo property of the cell because filter row cells 
                        // use different owners.
                        //
                        //this.Column.EditorOwnerInfo.GetExternalContext(editor.ElementBeingEdited.OwnerContext) == this)
                        this.EditorOwnerInfo.GetExternalContext(editor.ElementBeingEdited.OwnerContext) == this)
                    {
                        return true;
                    }
                }
                else
                    return true;
            }

            //Don't go into edit mode when clicking on button portion
            //of EditButton cell
            // SSP 5/1/03 - Cell Level Editor
            // Added Editor and ValueList properties off the cell so the user can set the editor
            // and value list on a per cell basis.
            //
            //if ( this.column.StyleResolved == ColumnStyle.EditButton &&
            if (this.StyleResolved == ColumnStyle.EditButton &&
                byMouse && this.IsCursorOverEditButton)
                return true;

            // clear selection when entering edit mode
            // SSP 10/23/06 BR16793
            // Don't clear the pivot item if the pivot item is this cell. Pass in true for
            // dontClearPivotItem parameter if that's the case.
            // 
            //bool cancel = grid.ClearAllSelected();
            RowScrollRegion activeRSR = grid.ActiveRowScrollRegion;
            bool cancel = grid.ClearAllSelected(this == activeRSR.PivotItemInternal);

            if (cancel)
                return true;

            // SSP 7/8/03 - Support developer calling EnterEditMode on the editor directly.
            // Added OnAfterCellEnterEditMode and OnAfterCellExitEditMode methods and moved
            // the logic in there so we have the necessary logic in centralized location
            // and handle the developer calling EnterEditMode on the editor directly.
            // Moved below code in the OnAfterCellEnterEditMode method.
            //
            

            // SSP 11/19/03
            // We need to scroll the cell into view before entering the cell in edit mode.
            // The reason for this is that the we need to have the associated embeddable edit
            // element in order to enter the edit mode on the cell and if the cell is scrolled
            // out of view, then there won't be any cell element and embeddable ui elmenet
            // to enter the edit mode. So instead of scrolling the cell into view after
            // entering edit mode, scroll it into view before. Moved this code from below.
            //
            // we first need to scroll the cell into view in case the metaregions
            // have changed so that they partially obscure the cell
            //
            // scroll the cell's row into view in the this row scroll region
            //
            //We don't want to exit edit mode here, so pass flag to
            //override default value

            this.Layout.Grid.ActiveRowScrollRegion.ScrollRowIntoView(this.Row, false);

            // scroll the cell's column into view in the active col scroll
            // region
            //				
            this.Layout.Grid.ActiveColScrollRegion.ScrollColIntoView(this.column);

            // MBS 2/20/08 - RowEditTemplate NA2008 V2            
            if ((this.Band.RowEditTemplateUITypeResolved & RowEditTemplateUIType.OnEnterEditMode) != 0)
            {
                UltraGridRowEditTemplate template = this.Row.RowEditTemplateResolved;
                if (template != null)
                {
                    this.Row.ShowEditTemplate(true, TemplateDisplaySource.OnEnterEditMode);
                    return false;
                }
            }

            // Call EnterEditorIntoEditMode to enter the editor into edit mode.
            //
            this.EnterEditorIntoEditMode(byMouse);

            // If entering of edit mode was cancelled, then return false.
            //
            if (!editor.IsInEditMode)
            {
                // Ensure the flag is reset properly.
                //
                this.isInEditMode = false;
                return true;
            }


            this.isInEditMode = true;

            // SSP 11/14/01 UWG603
            // We need to dirty the cell element so that any EditCellAppearance
            // settings take effect.
            this.InvalidateItem(
                this.Band.Layout.ActiveRowScrollRegion,
                this.Band.Layout.GetActiveColScrollRegion(false),
                true);

            //ROBA UWG155 8/16/01 
            //We must invalidate the cell when toggling edit mode
            //so that focus rect is drawn/erased as needed.
            //this.InvalidateItem();
            //RobA UWG450 10/15/01 
            //Invalidate the row not just the cell
            //
            this.Row.InvalidateItem();

            // If the form was unloaded in the event above then return true
            // to cancel
            //
            return false;
        }
        
	
		/// <summary>
		/// Returns true if the cell can enter edit mode
		/// </summary>
		/// <returns></returns>
		public bool CanEnterEditMode
		{
			get
			{
				// SSP 6/5/02
				// EM Emdeddable editors.
				// If the editor can't edit the column's data type, then return false.

				// SSP 7/29/02 UWG1457
				// Call the new method CanCellEnterEditMode off the column because many times
				// we don't have cells created and we want to find out if the cell is editable
				// (for example to determine if we need to show the drop down button), in which
				// case we don't want to force creating of a cell. So just call the new method
				// off the column.
				//
				// ---------------------------------------------------------------------------------
				

				return this.Column.CanCellEnterEditMode( this.Row );
				// ---------------------------------------------------------------------------------
			}
		}

		internal override Activation ActivationResolved 
		{
			get
			{
				// SSP 5/16/03 - Hiding Individual Cells Feature
				// If the cell is hidden, then return Disabled for the activation because
				// we don't want this cell begin selected or going into edit mode.
				//
				if ( this.Hidden )
					return Activation.Disabled;

				// SSP 8/1/03 UWG1654 - Filter Action
				// Implemented Filter action (ShowEnabled, ShowDisabled, Hide).
				// If the row filter action is to disable the filtered out rows and the row is filtered out
				// then return Disabled.
				//
				// ------------------------------------------------------------------------------------------------
				UltraGridRow row = this.Row;
				if ( row.IsFilteredOut && RowFilterAction.DisableFilteredOutRows == this.Band.RowFilterActionResolved )
					return Activation.Disabled; 
				// ------------------------------------------------------------------------------------------------

				// SSP 8/6/03 UWG2572
				// Added IgnoreRowColActivation property to the cell.
				//
				if ( this.IgnoreRowColActivation )
					return this.Activation;

				// Use a stack variable to hold the temp value instead of
				// of placing the method call inside the 'max' macro
				// below since that causes the method to be called twice
				// 
				Activation aTemp = row.GetResolvedCellActivation( this.column );
				// SSP 5/2/05 BR04321
				// After discussing with Joe, the behavior was changed so Disabled takes precedence 
				// over the NoEdit. Use the new MaxActivation method instead.
				// 
				//return (Activation)System.Math.Max( (int)this.activation,(int) aTemp );
				return GridUtils.MaxActivation( this.activation, aTemp );
			}
		}

		internal bool IsCursorOverEditButton
		{
			get
			{
				System.Drawing.Point point = Cursor.Position;
				return this.IsPointOverEditButton( point );
			}
		}
		internal bool IsPointOverEditButton(Point point)
		{
			Infragistics.Win.UIElement element = this.Layout.GetUIElement( false );

			if ( element != null )
			{
				Infragistics.Win.UIElement subElement = element.ElementFromPoint( point );
				
				// if we get a ssUIElementButton we're clicking on the button,
				// don't post SSMSG_SIMULATE_LBUTTONDOWN message				
				// If this is our cell element then return true if
				// the point is in the clip rect
				//
				//Make sure subelement is actually ssUIElementButton
				if ( subElement != null && 
					this == subElement.GetContext( typeof(Infragistics.Win.UltraWinGrid.UltraGridCell)) &&
					subElement.GetType() == typeof(Infragistics.Win.ButtonUIElement) )
				{
					if ( subElement.ClipRect.Contains( point ) )
						return true;					
				}

			}

			return false;
		}

	
		

		// SSP 8/15/02 UWG1561
		// Moved IsCursorOverCell to UltraGridColumn because we don't always have a cell (since
		// we create cells lazily we don't want to create a cell just to check if the mouse
		// is over the cell).
		//
		

		

		

		

		

		
         
		// SSP 7/17/03
		// Following internal property is not used anywhere so commented it out.
		//
		

		

		//Don't think we need this anymore after implementing Ivaluelist
		

		
		// SSP 4/5/02
		// EM Embeddable editors. We don't really need ShowEditCtl since due to
		// embeddable editors, grid won't be managing any edit controls itself. Show
		// this is not necessary. The embeddable editors will display any necessary
		// controls.
		//
		


		internal int FieldLenResolved
		{
			get
			{
				int fieldLen = -1;

				System.Type dataType = this.column.DataType;

				if ( dataType == typeof( string ) )
					// JAS 2005 v2 XSD Support
					//					fieldLen = this.column.FieldLen;
					fieldLen = this.column.MaxLength;

				return fieldLen;
			}
		}

		// SSP 7/18/03 - Optimizations
		// Instead of storing the cell changed event handler instance into a variable
		// just create it as you hook into and unhook from the cell. That way we don't
		// have to have the variable off the cell.
		//
		
	
		// SSP 6/30/03 UWG2271
		//
		internal bool EditValueChanged
		{
			get
			{
				return this.editValueChanged;
			}
		}

		// SSP 4/16/05 - NAS 5.2 Filter Row
		// Added UltraGridFilterCell class.
		//
		//internal void CellChangeHelper( object sender, EventArgs ev )
		internal virtual void CellChangeHelper( object sender, EventArgs ev )
		{
			// SSP 6/30/03 UWG2271
			// Now we are not setting the DataChanged to true in ValueChanged of the editor. Instead
			// we will wait till the value is commited to the backend to set this property. Until then
			// we will have editValueChanged flag set to true. Look at UWG2271 for more info on why
			// this was done.
			// Commented out below code and added new code below.
			//
			// --------------------------------------------------------------------------------------
			
			if ( ! this.editValueChanged )
			{
				// SSP 2/17/05 BR00461
				// If the cell has a formula then return. We don't want to mark the cell and the
				// row modified if the cell in edit mode happens to be a formula cell and it happens
				// to be in the process of being evaluated currently. Note that formula cells are 
				// always read-only.
				//
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Use the new HasActiveFormula property of the cell which takes into account
				// filter row cells.
				//
				//if ( this.Column.HasActiveFormula )
				if ( this.HasActiveFormula )
					return;

				this.editValueChanged = true;


				// SSP 11/12/03 Add Row Feature
				// Reset the TemplateAddRowHiddenFlag flag on the associated row's parent collection
				// then so the template add-row shows up as soon as the add-row is modified.
				//
				if ( null != this.Row && null != this.Row.ParentCollection )
					this.Row.ParentCollection.OnUserModifiedCellValue( this );

				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DataChanged );
				this.Row.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DataChanged );
			}
			// --------------------------------------------------------------------------------------

            // CDS NAS v9.1 Header CheckBox             

            // CDS 02/02/09 TFS12512 Moved the UpdateHeaderCheckBox() method to the column
            //this.UpdateHeaderCheckBox();
            if (this.Column != null &&
                this.Row != null)
                this.Column.UpdateHeaderCheckBox(this.Row.ParentCollection);

			if ( this.Layout.Grid is UltraGrid )
			{
				// SSP 8/24/01 UWG231
				// Added this check before firing the event because for drop downs
				// we do not want to fire this event.
				// SSP 3/29/02
				// Took out the check for IsColumnStyleDropDown. We still want to
				// fire the cell change event regardless of the style. The bug was
				// misinterpreted. Apparently we only want to prevent the cell change
				// from being fired if a new item is selected from the drop down. But
				// we still want to fire it when the user types into the text control.
				//
				//if ( !this.column.IsColumnStyleDropDown )
				//{
				// SSP 3/29/02
				// Use the new flag DontFireCellChangeEvent off the layout to determine
				// whether to fire the cell change or not.
				//
				if ( !this.Layout.DontFireCellChangeEvent )
				{
					CellEventArgs e = new CellEventArgs( this );
					((UltraGrid)this.Layout.Grid).FireEvent( GridEventIds.CellChange, e );
				}
			}
		}

		// SSP 4/26/02
		// EM Embeddable editors.
		// Commented out UpdateEditColors.
		//
		

		internal void ToggleEditMode()
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if( null == grid )
				return;

			if ( this.isInEditMode )
			{
				this.ExitEditMode();
			
				//if ( this.column.StyleResolved == ColumnStyle.HTML )
				//	grid.Activate();				
			}
			else
				this.EnterEditMode( false );
			//We must invalidate the cell when toggling edit mode
			//so that focus rect is drawn/erased as needed.

			this.InvalidateItem();
		}	

		// SSP 7/18/03
		// Commented out the internal method SetValueListDataValue as it's not being 
		// used anywhere.
		//
		

		internal void SetDataChanged( bool newValue )
		{
			// notify any interested parties
			//
			//
			// Always notify people that this flag
			// has changed, even if it hasn't. We
			// rely on this notification to invalidate
			// ourselves.
			//
			//
			// SSP 11/14/03
			// Check for the row being disposed. If the row is disposed, then calling NotifyPropChange
			// will cause an exception since it verifies that the subobject is not disposed off
			// and throws an exception if it is.
			//
			if ( ! this.Disposed )
				this.NotifyPropChange( PropertyIds.DataChanged );

			// SSP 11/17/05 BR07772
			// If the column is bound then we need to honor the UpdateData method
			// call even if modifications were made via the calc manager.
			// 
			//if ( newValue != this.dataChanged )
			if ( newValue != this.dataChanged || newValue != this.dataChangedViaCalcManager )
			{
				// SSP 7/26/04 - UltraCalc
				// If SetDataChanged gets called due to us setting this cell's value to
				// it's formula evaluation result, don't show the pencil icon on the row
				// selector. What happens is that if you have a column with a formula,
				// when the grid comes up all the row selectors are showing the pencil
				// icon because all the cells in the formula column got their values set 
				// to the formula results.
				//
				// ----------------------------------------------------------------------
				// SSP 11/17/05 BR07772
				// If the column is bound then we need to honor the UpdateData method
				// call even if modifications were made via the calc manager.
				// 
				this.dataChangedViaCalcManager = false;

				if ( newValue && null != this.Row && this.Row.HasCalcReference 
					&& ((RowReference)this.Row.CalcReference).inCellReferenceValueSet )
				{
					// SSP 11/17/05 BR07772
					// If the column is bound then we need to honor the UpdateData method
					// call even if modifications were made via the calc manager.
					// 
					if ( this.Column.IsBound )
					{
						this.dataChangedViaCalcManager = true;
						this.Row.dataChangedViaCalcManager = true;
					}

					return;
				}
				// ----------------------------------------------------------------------

				this.dataChanged = newValue;
 
				// notify any interested parties
				//

				// If the Cell's DataChanged property is set to 
				// true, we want to pass that along to the Row. But,
				// we do not want to pass along the setting to false, which
				// only occurs via CancelUpdate methods of the Row or Grid.
				// Once cell in a row can have it's DataChanged = false, while
				// there are other cells whose DataChanged = true.
				if ( newValue )
				{
					Infragistics.Win.UltraWinGrid.UltraGridRow row = this.Row;

					if ( null != row )
					{
						row.SetDataChanged( newValue );
					}
				}
			}
		}

		// SSP 5/2/03
		// Commented below method out since it's not being used anywhere.
		//
		

			

		// SSP 5/01/03
		// This is not used anywhere. Commenting it out.
		//
		

		internal UnBoundData UnboundData
		{
			get
			{
				if (unBoundData != null)
				{
					return unBoundData;
				}
				else
				{
					this.unBoundData = new UnBoundData();
					return unBoundData;
				}
			}
		}

		// SSP 8/26/06 - NAS 6.3
		// Added HasUnboundData.
		// 
		internal bool HasUnboundData
		{
			get
			{
				return null != this.unBoundData;
			}
		}

		internal void RestoreUnboundData()
		{
			if ( this.column.IsBound && this.unBoundData != null )
				this.unBoundData.RestoreOriginal();
		}

		internal void SaveOriginalVal()
		{
			if ( this.unBoundData != null )
				this.unBoundData.SaveOriginal();
		}


		
		internal override ISelectionStrategy SelectionStrategyDefault
		{
			get
			{
				return ((ISelectionStrategyProvider)(Band)).SelectionStrategyCell;
			}
		}
		internal override void AddToCollection( Selected selected )
		{
			// create a selectedCells collection if we don't have one
			// JJD 10/09/01
			// You no longer have to create the cells collection like
			// this since the property get will create it lazily
			//
			//if ( selected.Cells == null )
			//	selected.Cells = new SelectedCellsCollection();

			selected.Cells.InternalAdd( this );
		}
		internal override void RemoveFromCollection( Selected selected )
		{
			Debug.Assert( selected.Cells != null, "No collection to remove from!" );

			selected.Cells.Remove( this );
		}
		internal override void SetInitialSelection( Selected currentSelection, 
			Selected initialSelection )
		{
			if ( currentSelection.Cells != null )
			{
				// JJD 10/09/01
				// You no longer have to create the cells collection like
				// this since the property get will create it lazily
				//
				//	if ( initialSelection.Cells == null )
				//	initialSelection.Cells = new SelectedCellsCollection();

				// add each cell currently selected into the initial selection collection
				foreach( UltraGridCell cell in currentSelection.Cells )
					initialSelection.Cells.InternalAdd( cell );
			}
		}

		internal override void CalcSelectionRange( Selected newSelection, 
			bool clearExistingSelection,
			bool select,
			Selected initialSelection )
		{
			GridItemBase pivotItem = this.Layout.Grid.ActiveRowScrollRegion.GetPivotItem();

			UltraGridCell pivotCell = pivotItem as UltraGridCell;
			if ( pivotCell == null ) return;

			SelectedRowsCollection rows = this.Row.GetRowsForSelection( select );

			// SSP 3/03/03 - Row Layout Functionality
			// Following is the code for cell selection in row layout mode. Since cells can be laid
			// out in verious ways that may not correspond with the visible positions of the associated 
			// columns, we need to have special selection logic.
			// 
			// ------------------------------------------------------------------------------------------
			if ( this.Band.UseRowLayoutResolved )
			{
				if ( ! clearExistingSelection )
				{
					UltraGrid grid = this.Band.Layout.Grid as UltraGrid;
					Infragistics.Win.UltraWinGrid.Selected selected = null != grid && grid.HasSelectedBeenAllocated ? grid.Selected : null;

					if ( null != selected )
					{
						foreach ( ColumnHeader col in grid.Selected.Columns )
						{
							newSelection.Columns.InternalAdd( col );
						}
					}
				}

				if ( pivotCell == this )
				{
					newSelection.Cells.InternalAdd( this );
				}
				else if ( pivotCell.Band == this.Band )
				{
					UltraGridBand band = this.Band;
					UltraGridCell cell1 = (UltraGridCell)pivotCell;
					UltraGridCell cell2 = this;

					// For simpler code, switch the cells if the cell2 occurs before cell1.
					// 
					if ( cell1.Row != cell2.Row && cell1.Row.OverallSelectionPosition > cell2.Row.OverallSelectionPosition 
						// SSP 2/24/06 BR09715
						// Also switch if the cells are from the same row however cell1 occurs after cell2.
						// 
						|| cell1.Row == cell2.Row && UltraGridBand.LayoutColumnPositionComparer.Compare( cell1.Column, cell2.Column, true ) > 0 )
					{
						UltraGridCell tmpCell = cell1;
						cell1 = cell2;
						cell2 = tmpCell;
					}

					HeadersCollection layoutOrderedColumnHeaders = band.LayoutOrderedVisibleColumnHeaders;

					if ( null != layoutOrderedColumnHeaders &&
						layoutOrderedColumnHeaders.Contains( cell1.Column.Header ) &&
						layoutOrderedColumnHeaders.Contains( cell2.Column.Header ) )
					{
						int x1 = Math.Min( cell1.Column.RowLayoutColumnInfo.CachedOverallCellRect.X,
							cell2.Column.RowLayoutColumnInfo.CachedOverallCellRect.X );

						int x2 = Math.Max( cell1.Column.RowLayoutColumnInfo.CachedOverallCellRect.Right,
							cell2.Column.RowLayoutColumnInfo.CachedOverallCellRect.Right );;

						int y1 = Math.Min( cell1.Column.RowLayoutColumnInfo.CachedOverallCellRect.Top,
							cell2.Column.RowLayoutColumnInfo.CachedOverallCellRect.Top );
						int y2 = Math.Max( cell1.Column.RowLayoutColumnInfo.CachedOverallCellRect.Bottom,
							cell2.Column.RowLayoutColumnInfo.CachedOverallCellRect.Bottom );

						int minY = 0;
						int maxY = 0;

						for ( int i = 0; i < layoutOrderedColumnHeaders.Count; i++ )
						{
							Infragistics.Win.UltraWinGrid.ColumnHeader header = (ColumnHeader)layoutOrderedColumnHeaders[i];

							Rectangle rect = header.Column.RowLayoutColumnInfo.CachedOverallCellRect;

							if ( 0 == i || minY > rect.Top )
								minY = rect.Top;

							if ( 0 == i || maxY < rect.Bottom )
								maxY = rect.Bottom;
						}

						for ( int j = 0; j < rows.Count; j++ )
						{						
							UltraGridRow row = rows[j];

							if ( null == row )
								continue;
							
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Remove unused locals
							//Rectangle boundingRect = Rectangle.Union( 
							//    cell1.Column.RowLayoutColumnInfo.CachedHeaderRect, 
							//    cell2.Column.RowLayoutColumnInfo.CachedHeaderRect );

							for ( int i = 0; i < layoutOrderedColumnHeaders.Count; i++ )
							{
								Infragistics.Win.UltraWinGrid.ColumnHeader header = (ColumnHeader)layoutOrderedColumnHeaders[i];

								Rectangle itemRect = header.Column.RowLayoutColumnInfo.CachedOverallCellRect;

								// Take into account merged borders. So deflate the borders by -1 otherwise
								// we will end up selecting items that are off by only 1 pixel.
								//
								itemRect.Inflate( -1, -1 );

								bool addCell = false;

								// MD 8/2/07 - 7.3 Performance
								// Cache collumn - Prevent calling expensive getters multiple times
								UltraGridColumn headerColumn = header.Column;

								// SSP 2/24/06 BR09715
								// Added MultiCellSelectionMode property to support snaking cell selection..
								// 
								// --------------------------------------------------------------------------------
								if ( band.SnakeCellSelectionResolved || band.CardView && cell1.Row != cell2.Row )
								{
									if ( row == cell1.Row && row == cell2.Row )
									{
										// If if the pivot cell and the active cell are from the same row then select
										// cells that are between the two.
										// 

										// MD 8/2/07 - 7.3 Performance
										// Prevent calling expensive getters multiple times
										//if ( UltraGridBand.LayoutColumnPositionComparer.Compare( header.Column, cell1.Column, true ) >= 0 
										//    && UltraGridBand.LayoutColumnPositionComparer.Compare( header.Column, cell2.Column, true ) <= 0 )
										if ( UltraGridBand.LayoutColumnPositionComparer.Compare( headerColumn, cell1.Column, true ) >= 0
											&& UltraGridBand.LayoutColumnPositionComparer.Compare( headerColumn, cell2.Column, true ) <= 0 )
											addCell = true;
									}
									else if ( row == cell1.Row )
									{
										// MD 8/2/07 - 7.3 Performance
										// Prevent calling expensive getters multiple times
										//if ( UltraGridBand.LayoutColumnPositionComparer.Compare( header.Column, cell1.Column, true ) >= 0 )
										if ( UltraGridBand.LayoutColumnPositionComparer.Compare( headerColumn, cell1.Column, true ) >= 0 )
											addCell = true;
									}
									else if ( row == cell2.Row )
									{
										// MD 8/2/07 - 7.3 Performance
										// Prevent calling expensive getters multiple times
										//if ( UltraGridBand.LayoutColumnPositionComparer.Compare( header.Column, cell2.Column, true ) <= 0 )
										if ( UltraGridBand.LayoutColumnPositionComparer.Compare( headerColumn, cell2.Column, true ) <= 0 )
											addCell = true;
									}
									else
									{
										// Add all cells of intervening rows in snaking mode or when we are in card-view.
										// 
										addCell = true;
									}
								}
								// --------------------------------------------------------------------------------
								else if ( itemRect.Right > x1 && itemRect.Left < x2 )
								{
									if ( row == cell1.Row && row == cell2.Row )
									{
										if ( y1 < y2 )
										{
											if ( itemRect.Bottom > y1 && itemRect.Top < y2 )
												addCell = true;
										}
										else 
										{
											if ( itemRect.Bottom > y2 && itemRect.Top < y1 )
												addCell = true;
										}
									}
									else if ( cell1.Row == row )
									{
										if ( itemRect.Bottom > cell1.Column.RowLayoutColumnInfo.CachedOverallCellRect.Top )
											addCell = true;
									}
									else if ( cell2.Row == row )
									{
										if ( itemRect.Top < cell2.Column.RowLayoutColumnInfo.CachedOverallCellRect.Bottom )
											addCell = true;
									}
									else
									{
										addCell = true;
									}
								}
									// In card view, select all the cells of the rows in between the rows associated
									// with the pivot and the this cell.
									//
								else if ( band.CardView )
								{
									if ( row != cell1.Row && row != cell2.Row )
										addCell = true;
								}

								if ( addCell )
								{
									// MD 8/2/07 - 7.3 Performance
									// Prevent calling expensive getters multiple times
									//UltraGridCell cell = row.Cells[ header.Column ];
									UltraGridCell cell = row.Cells[ headerColumn ];

									// SSP 11/13/03 Add Row Feature
									// Only add the cell if its CanSelectCell returns true.
									//
									//if ( null != cell && Activation.Disabled != cell.ActivationResolved )
									// SSP 4/20/05 - NAS 5.2 Filter Row
									// Use the Selectable property instead.
									//
									//if ( null != cell && Activation.Disabled != cell.ActivationResolved && cell.CanSelectCell )
									if ( null != cell && Activation.Disabled != cell.ActivationResolved && cell.Selectable )
										newSelection.Cells.InternalAdd( cell, false );
								}
							}		
						}
					}
					else
					{
						Debug.Assert( false );
					}
				}

				if ( ! clearExistingSelection )
				{
					foreach ( UltraGridCell cell in initialSelection.Cells )
					{
						if ( ! newSelection.Cells.Contains( cell ) )
							newSelection.Cells.InternalAdd( cell, false );
					}
				}

				return;
			}
			// ------------------------------------------------------------------------------------------

			HeadersCollection headers = this.Column.Header.GetHeadersForSelection( select, pivotCell.Column.Header );

			
			//RobA 10/29/01 implemented snaking
			// SSP 2/24/06 BR09715
			// Added MultiCellSelectionMode property to support snaking cell selection. Use the new 
			// SnakeCellSelectionResolved property off the band which takes into account the new 
			// MergedCellStyle property as well as the Snaking property of the layout.
			// 
			//if ( this.Layout.Snaking )
			if ( this.Band.SnakeCellSelectionResolved )
				AddSnakingCells( rows, headers, pivotCell, ref newSelection );
			
			else
			{
				for ( int i = 0; i < rows.Count; i++ )
				{
					for ( int j = 0; j < headers.Count; j++ )
					{
						UltraGridCell cell = rows[i].Cells[headers[j].Column];
						if ( cell != null &&
							//RobA UWG586 11/7/01
							cell.ActivationResolved != Activation.Disabled
							// SSP 11/13/03 Add Row Feature
							// Only add the cell if its CanSelectCell returns true.
							//
							// SSP 4/20/05 - NAS 5.2 Filter Row
							// Use the Selectable property instead.
							//
							//&& cell.CanSelectCell 
							&& cell.Selectable )
							newSelection.Cells.InternalAdd( cell );
					}
				}
			}

			

			headers = null;
			rows = null;

			if ( ! clearExistingSelection )
			{
				int rowpositionToTest;
				int rowposition				= this.Row.ScrollPosition;
				int rowpositionPivotItem	= pivotCell.Row.ScrollPosition;
				int colpositionToTest;
				int colposition				= this.Column.Header.VisiblePosition;
				int colpositionPivotItem	= pivotCell.Column.Header.VisiblePosition;

                // MBS 3/20/09 - TFS13782
                // When we're in group mode, we're not properly taking into account the overall visible order of the column
                // in that we're only looking at the VisiblePosition of a header, which in a group will be simply relative
                // to that group, so we should also look at the group that a column belongs to.
                int groupPosition           = this.Column.Group != null ? this.Column.Group.Header.VisiblePosition : -1;
                int groupPositionPivotItem  = pivotCell.Column.Group != null ? pivotCell.Column.Group.Header.VisiblePosition : -1;
                int groupPositionToTest;

				foreach ( UltraGridCell selectedCell in initialSelection.Cells )
				{
					// JJD 10/15/01
					// Get the row position once to avoid unnecessary
					// overhead
					//
					rowpositionToTest = selectedCell.Row.ScrollPosition;

					// JJD 10/15/01 - UWG523
					// Don't re-add the if the position is equal to the pivot
					//
					
					if ( rowposition > rowpositionPivotItem )
					{
						if ( rowpositionToTest < rowpositionPivotItem ||
							rowpositionToTest > rowposition )
						{
							newSelection.Cells.InternalAdd( selectedCell );							 
							continue;
						}
					}
					else 
					{
						if ( rowpositionToTest < rowposition ||
							rowpositionToTest > rowpositionPivotItem )
						{
							newSelection.Cells.InternalAdd( selectedCell );
							continue;
						}
					}

					// JJD 10/15/01 - UWG523
					// Get the column position once to avoid unnecessary
					// overhead
					//
					colpositionToTest = selectedCell.Column.Header.VisiblePosition;

                    // MBS 3/20/09 - TFS13782
                    groupPositionToTest = selectedCell.Column.Group != null ? selectedCell.Column.Group.Header.VisiblePosition : -1;

					// JJD 10/15/01 - UWG523
					// Don't re-add the if the position is equal to the pivot
					//
					
					if ( colposition > colpositionPivotItem ||
                        // MBS 3/20/09 - TFS13782
                        groupPosition > groupPositionPivotItem)
					{
						if ( colpositionToTest < colpositionPivotItem ||
							colpositionToTest > colposition ||
                            // MBS 3/20/09 - TFS13782
                            groupPositionToTest < groupPositionPivotItem ||
                            groupPositionToTest > groupPosition)
						{
                            // MBS 3/20/09 - TFS13782
                            // Don't try to add the cell if it's already in the collection
                            if(!newSelection.Cells.Contains(selectedCell))
							    newSelection.Cells.InternalAdd( selectedCell );							 

							continue;
						}
					}
					else 
					{
						if ( colpositionToTest < colposition ||
							colpositionToTest > colpositionPivotItem ||
                            // MBS 3/20/09 - TFS13782
                            groupPositionToTest < groupPosition ||
                            groupPositionToTest > groupPositionPivotItem)
						{
                            // MBS 3/20/09 - TFS13782
                            // Don't try to add the cell if it's already in the collection
                            if (!newSelection.Cells.Contains(selectedCell))
							    newSelection.Cells.InternalAdd( selectedCell );

							continue;
						}
					}
				}

				// JJD 10/12/01 - UWG522
				// Copy over the selected columns also
				//
				UltraGrid grid = this.Layout.Grid as UltraGrid;

				if ( grid != null )
				{
					foreach ( ColumnHeader header in grid.Selected.Columns )
					{
						newSelection.Columns.InternalAdd( header );
					}
				}
			}

		}

		/// <summary>
		/// Returns the UltraGridRow object associated with the cell. This property is not available at design time. This property is read-only at run time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Row</b> property of an object refers to a specific row in the grid as defined by an UltraGridRow object. You use the <b>Row</b> property to access the properties of a specified UltraGridRow object, or to return a reference to the UltraGridRow object that is associated with the current object.</p>
		/// <p class="body">An UltraGridRow object represents a single row in the grid that displays the data from a single record in the underlying data source. The UltraGridRow object is one mechanism used to manage the updating of data records either singly or in batches (the other is the UltraGridCell object). When the user is interacting with the grid, one UltraGridRow object is always the active row, and determines both the input focus of the grid and the position context of the data source to which the grid is bound.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row 
		{ 
			get
			{
				// get the row from the cells collection.
				// we don't keep a separate reference to the
				// row so we can conserve memory
				//
				// MD 8/3/07 - 7.3 Performance
				// The property has been removed because it is not used outside the class
				//return ((CellsCollection)this.PrimaryCollection).Row;
				return this.primaryCollection.Row;
			}
		}

		/// <summary>
		/// Returns the UltraGridColumn object associated with the cell. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
		/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}
		
		// This needed to be an internal method so that it didn't conflict 
		// with the Cell's public Column property
		//
		internal override Infragistics.Win.UltraWinGrid.UltraGridRow GetRow()
		{
			return this.Row;
		}

		// This needed to be an internal method so that it didn't conflict 
		// with the Cell's public Column property
		//
		internal override Infragistics.Win.UltraWinGrid.UltraGridColumn GetColumn()
		{
			return this.column;
		}

		internal override void InternalSelect( bool value )
		{
			if( this.selectedValue != value )
			{
				this.selectedValue = value;

				// SSP 12/6/04 - Merged Cell Feature
				//
				RowsCollection.MergedCell_CellStateChanged( this, MergedCell.State.Selection );

				this.NotifyPropChange( PropertyIds.Selected, null );

				// SSP 4/12/04 - Virtual Binding - Cell Selection Optimization
				// Instead of invalidating the cell here which does a search for the cell ui
				// element through the element hierarchy (an expensive operation), invalidate
				// it through the visible row. To do that, in the CellsCollection's 
				// OnSubObjectPropChange notify the row which which will notify the visible
				// row if a visible row is associated with it.
				// Commented below code out.
				//
				
			}
		}

		// SSP 10/4/01
		// Made changes to make use of SelectedRowsCollection instead of RowsCollection
		//
		//internal void AddSnakingCells(RowsCollection rows, HeadersCollection headers, Cell pivotCell, ref Selected newSelection )
		internal void AddSnakingCells( SelectedRowsCollection rows, HeadersCollection headers, UltraGridCell pivotCell, ref Selected newSelection )
		{
			int rowIndex = this.Row.Index;
			int pivotIndex = pivotCell.Row.Index;
	
			// if this and the pivot are the same -- no snaking to be done
			//
			// JJD 1/15/01
			// Check that the rows are the same instead of checking their index since
			// rows from different parents can have the same index
			//
			//			if (rowIndex == pivotIndex) 
			if (this.Row == pivotCell.Row) 
			{
				// JJD 1/15/02
				// Add the cells from this row only
				//
				for ( int i=0; i < headers.Count; i++ )
					newSelection.Cells.InternalAdd ( this.Row.Cells[ headers[i].Column ] );
				return;
			}
			
			// SSP 10/30/03 UWG2129
			// If the bands are different for some reason, then assert and bail out.
			//
			// ------------------------------------------------------------------------------
			Debug.Assert( this.Band == pivotCell.Band );
			if ( this.Band != pivotCell.Band )
				return;
			// ------------------------------------------------------------------------------

			bool snakingDown;

			// JJD 1/15/02
			// Use the row indexes to determine if we are snaking downward only if
			// the rows are siblings. Otherwise use the indexes of their parent rows.
			//
			if ( this.Row.ParentCollection == pivotCell.Row.ParentCollection )
				snakingDown = rowIndex > pivotIndex ? true : false;
			else
				snakingDown = this.Row.ParentRow.Index > pivotCell.Row.ParentRow.Index ? true : false;

	
			UltraGridCell[] cellsOnPivotRow = this.GetAllCellsFromPosition( snakingDown, pivotCell );
			UltraGridCell[] cellsOnThisRow = this.GetAllCellsFromPosition( !snakingDown, this );

			newSelection.Cells.InternalAdd ( cellsOnPivotRow );
			newSelection.Cells.InternalAdd ( cellsOnThisRow );

			for ( int i=0; i < rows.Count; i++ )
			{
				if ( rows[i] != this.Row && rows[i] != pivotCell.Row )
				{
					// SSP 11/13/03 Add Row Feature
					// Skip rows that return false from CanSelectRow.
					//
					// SSP 4/20/05 - NAS 5.2 Filter Row
					// Use the Selectable property instead.
					//
					//if ( null == rows[i] || ! rows[i].CanSelectRow )
					if ( null == rows[i] || ! rows[i].Selectable )
						continue;

					// JJD 1/15/02
					// We shouldn't be adding rows here we should be adding 
					// cells
					//
					//newSelection.Rows.InternalAdd ( rows[i] );
					newSelection.Cells.InternalAdd ( rows[i].Cells.All );
				}
			}

		}

		internal UltraGridCell[] GetAllCellsFromPosition( bool Right, UltraGridCell cell )
		{
			if ( cell == null )
				cell = this;

			int pivotPosition  = cell.Column.Header.VisiblePosition;

			if ( Right )
			{
				int numberOfColumns =  cell.Band.OrderedColumnHeaders.Count;
				UltraGridCell[] cells = new UltraGridCell[ numberOfColumns - pivotPosition ];
				int j = 0;
				
				for ( int i = pivotPosition; i < numberOfColumns; i++, j++ )
				{
					
					//Column col = cell.Layout.Grid.ActiveColScrollRegion.VisibleHeaders[i].Header.Column;
					UltraGridColumn col = cell.Band.OrderedColumnHeaders[i].Column;
					cells[j] = cell.Row.Cells[col];
				}
				return cells;
			}
			else
			{
				// RobA 10/29/01 pivotPosition is a zero-based index not a count
				// so allocate 1 more than the index number
				//
				UltraGridCell[] cells = new UltraGridCell[ pivotPosition + 1 ];
				
				for ( int i = 0; i <= pivotPosition; i++ )
				{
					UltraGridColumn col = cell.Band.OrderedColumnHeaders[i].Column;
					cells[i] = cell.Row.Cells[col];
				}
				return cells;
			}

		}

		// SSP 5/1/03 - Cell Level Editor
		// Added Editor and ValueList properties off the cell so the user can set the editor
		// and value list on a per cell basis.
		//
		#region Cell Level Editor/ValueList Related Code

		#region Editor

		/// <summary>
		/// Use this property to set the editor that will be used for rendering and editing the cell. This property returns null if it wasn't set to an editor before. By default, <see cref="UltraGridColumn.Editor"/> property off the associated column is used for rendering and editing the cell.
		/// </summary>
		public EmbeddableEditorBase Editor
		{
			get
			{
				return this.editor;
			}
			set
			{
				if ( this.editor != value )
				{
					// SSP 7/29/03 UWG2519
					// Validate the editor to ensure that it supports the data type of the associated
					// column.
					//
					if ( null != value && !this.Column.IsEditorAppropriate( value ) )
						throw new ArgumentException( SR.GetString("LER_Exception_336", value.GetType( ).Name ), "Editor" );

					// SSP 10/29/03 UWG2523
					// Exit the edit mode if this cell is in edit mode.
					//
					// --------------------------------------------------------------------
					if ( this.IsInEditMode )
						this.ExitEditMode( false, true );
					// --------------------------------------------------------------------

					this.editor = value;

					this.DirtyCachedEditor( );

					this.InvalidateItemAllRegions( true );
				}
			}
		}

		#endregion // Editor

		#region EditorResolved

		/// <summary>
		/// Returns the editor used for rendering and editing the cell.
		/// </summary>
		public EmbeddableEditorBase EditorResolved
		{
			get
			{
				// SSP 6/10/05 BR04524
				// Don't change the editor while the cell is in edit mode. Wait for it to exit the edit
				// mode. Otherwise ExitEditMode for example will fail to exit the edit mode because of 
				// how it assumes the cell.EditorResolved is the editor that went into edit mode. This 
				// can happen for example when the editor changes while the cell is in edit mode. This 
				// bug happened with the filter row.
				// 
				//if ( null != this.lastEmbeddableEditor && this.verifiedCachedEditorVersion == this.Column.verifyEditorVersionNumber )
				if ( null != this.lastEmbeddableEditor 
					&& ( this.verifiedCachedEditorVersion == this.Column.verifyEditorVersionNumber 
						 || this.IsInEditMode ) )
					return this.lastEmbeddableEditor;

				// First try the value of cell's Editor property.
				//
				EmbeddableEditorBase editor = this.editor;

				// If the cell's Editor property is not set, then try the cell's EditorControl property.
				//
                // MRS - NAS 9.2 - Control Container Editor
				//if ( null == editor && null != this.EditorControl )
                if (null == editor && null != this.EditorComponentResolved)
				{
                    // MRS - NAS 9.2 - Control Container Editor
					//IProvidesEmbeddableEditor editorProvider = this.EditorControl as IProvidesEmbeddableEditor;
                    // MRS 5/15/2009 - TFS17654
                    //IProvidesEmbeddableEditor editorProvider = this.EditorComponentResolved as IProvidesEmbeddableEditor;
                    IProvidesEmbeddableEditor editorProvider = this.EditorComponent as IProvidesEmbeddableEditor;

					if ( null != editorProvider && this.Column.IsEditorAppropriate( editorProvider.Editor ) )
						editor = editorProvider.Editor;
				}

				// SSP 4/6/05 - NAS 5.2 Filter Row
				//
				if ( null == editor && this.IsFilterRowCell )
					editor = this.Column.FilterOperandEditor;

				// SSP 7/18/03 - Cell Level Style property
				// Added cell level style property.
				//
				// SSP 7/29/03
				// Give higher priority to the cell level editor and editor control properties.
				//
				//if ( ColumnStyle.Default != this.Style )
				if ( null == editor && ColumnStyle.Default != this.Style )
					editor = this.Column.GetDefaultEditor( this.Row );

				// If neither Editor nor EditorControl properties of the cell are set, then get the
				// editor off the column.
				//
				if ( null == editor )
					editor = this.Column.InternalEditor;
				
				// If the user has not assigned any embeddable editor, use a default 
				// one appropriate for this column.
				//
				if ( null == editor )
				{
					// If Editor property is not set, then get the editor from column's EditorControl property.
					//
                    // MRS - NAS 9.2 - Control Container Editor
					//IProvidesEmbeddableEditor editorProvider = this.Column.EditorControl as IProvidesEmbeddableEditor;
                    IProvidesEmbeddableEditor editorProvider = this.Column.EditorComponentResolved as IProvidesEmbeddableEditor;

					if ( null != editorProvider && this.Column.IsEditorAppropriate( editorProvider.Editor ) )
						editor = editorProvider.Editor;
					else
						editor = this.Column.GetDefaultEditor( this.Row );
				}

				this.verifiedCachedEditorVersion = this.Column.verifyEditorVersionNumber;

				// JJD 12/16/03
				// Clear the cached editorAccessibilityObject if the editor has
				// changed
				if ( this.lastEmbeddableEditor != editor )
					this.editorAccessibilityObject = null;

				return this.lastEmbeddableEditor = editor;
			}
		}

		#endregion // EditorResolved

		#region ValueList

		// SSP 5/8/05 - NAS 5.2 Filter Row
		// Made ValueList virtual.
		//
		/// <summary>
		/// Gets or sets the cell specific value list. Default is to use the <see cref="UltraGridColumn.ValueList"/> off the associated column. If this property is not set to a value list, it returns null.
		/// </summary>
		public virtual IValueList ValueList
		{
			get
			{
				return this.valueList;
			}
			set
			{
				if ( this.valueList != value )
				{
					// SSP 1/13/05 BR01536
					// When a valueList is assigned to a cell, exit the edit mode on the
					// cell otherwise it will be chaotic (cell when exitting the edit mode 
					// will work on the new value list rather than the old value list that 
					// it went in edit mode with and activated).
					//
					if ( this.IsInEditMode )
						this.ExitEditMode( false, true );

					this.valueList = value;

					// Dirty the embeddable editor.
					//
					this.DirtyCachedEditor( );

					this.InvalidateItemAllRegions( true );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ValueList );
				}
			}
		}

		#endregion // ValueList

		#region ValueListResolved

		/// <summary>
		/// Retruns the resolved value list.
		/// </summary>
		public IValueList ValueListResolved
		{
			get
			{
				return this.Column.GetValueList( this.Row );
			}
		}
		
		#endregion // ValueListResolved

		#region Style

		/// <summary>
		/// Specifies the column style of the cell.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridColumn.Style"/> <seealso cref="ColumnStyle"/>
		/// </remarks>
		public ColumnStyle Style
		{
			get
			{
				return this.columnStyle;	
			}
			set
			{
				if ( this.columnStyle != value )
				{
					if ( !Enum.IsDefined( typeof(ColumnStyle), value ) )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_86") );

					this.columnStyle = value;

					this.DirtyCachedEditor( );

					this.NotifyPropChange( PropertyIds.Style, null );
				}
			}
		}

		#endregion // Style

		#region StyleResolved
		
		/// <summary>
		/// Returns the resolved style based on this value list settings of the cell and the column.
		/// </summary>
		public ColumnStyle StyleResolved
		{
			get
			{
				if ( ColumnStyle.Default != this.columnStyle )
					return this.columnStyle;

				return this.Column.GetStyleResolved( this.Row );
			}
		}

		#endregion // StyleResolved

		#region HasDropDown
		
		internal bool HasDropDown
		{
			get
			{
				// This code was copied from Column.HasDropDown.
				//
				EmbeddableEditorBase editor = this.EditorResolved;
				
				return null != editor && editor.SupportsDropDown;
			}
		}

		#endregion // HasDropDown

		#region HasValueList
		
		internal bool HasValueList
		{ 
			get
			{
				return null != this.ValueListResolved;
			}
		}

		#endregion // HasValueList

		#region EditorControl
		
		// SSP 7/17/03 UWG2448
		// Add EditorControl property.
		//
		/// <summary>
		/// Control that implements IProvidesEmbeddableEditor. Attempt to set a control that
		/// does not implement IProvidesEmbeddableEditor interface will cause an exception.
		/// </summary>
        [Obsolete("This property has been deprecated; use the EditorComponent property instead.", false)] // MRS - NAS 9.2 - Control Container Editor
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public Control EditorControl
		{
			get
            {
                // MRS 8/27/2009 - TFS21109 (NAS 9.2 - Control Container Editor)
                //
                #region Old Code
                //return this.editorControl;
                #endregion // Old Code
                //
                return this.EditorComponent as Control;
            }
			set
			{
                // MRS 8/27/2009 - TFS21109 (NAS 9.2 - Control Container Editor)
                //
                #region Old Code
                //if ( this.editorControl != value )
                //{
                //    if ( null != value && ! ( value is  IProvidesEmbeddableEditor ) )
                //        throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_316"), Shared.SR.GetString("LE_ArgumentException_317") );

                //    IProvidesEmbeddableEditor editorProvider = value as IProvidesEmbeddableEditor;

                //    if ( null != editorProvider )
                //    {
                //        EmbeddableEditorBase editor = editorProvider.Editor;

                //        if ( !this.Column.IsEditorAppropriate( editor ) )
                //            throw new ArgumentException( SR.GetString("LER_Exception_338", editorProvider.GetType( ).Name ), SR.GetString("LER_ArguementException_317") );
                //    }

                //    // Exit the edit mode if the cell is in edit mode.
                //    //
                //    if ( this.IsInEditMode )
                //        this.ExitEditMode( false, true );

                //    this.editorControl = value;

                //    this.DirtyCachedEditor( );
					
                //    // Dirty the cell element.
                //    //
                //    this.InvalidateItemAllRegions( true );
                //}
                #endregion //Old Code
                //
                this.EditorComponent = value;
			}
		}
		
		#endregion // EditorControl

		#region EditorControlResolved

		// SSP 7/17/03 UWG2448
		// Add EditorControl property.
		//
		/// <summary>
		/// Returns the resolved editor control. If the EditorControl property is set on the cell then it returns the value of that property otherwise it returns the value of the EditorControl property of the associated column.
		/// </summary>        
        [Obsolete("This property has been deprecated; use the EditorComponentResolved property instead.", false)] // MRS - NAS 9.2 - Control Container Editor
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public Control EditorControlResolved
		{
			get
			{
				if ( null != this.EditorControl )
					return this.EditorControl;

				return this.Column.EditorControl;
			}
		}

		#endregion // EditorControlResolved

		#region DirtyCachedEditor

		private void DirtyCachedEditor( )
		{
			this.verifiedCachedEditorVersion--;

			// SSP 8/7/03 UWG2564
			// Dirty the grid element and bump the cell child elements version number
			// so all the cells reposition their child elements on the next verification.
			//
			if ( null != this.Row && null != this.Row.Layout )
			{
				this.Row.Layout.DirtyGridElement( );
				this.Row.Layout.BumpCellChildElementsCacheVersion( );
			}
		}

		#endregion // DirtyCachedEditor

		#endregion // Cell Level Editor/ValueList Related Code

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		#region CellDisplayStyle

		/// <summary>
		/// Specifies what to display in cells associated with this column.
		/// </summary>
		/// <remarks>
		/// <p class="body"></p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.CellDisplayStyle CellDisplayStyle
		{
			get
			{
				return this.cellDisplayStyle;
			}
			set
			{
				if ( this.cellDisplayStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( CellDisplayStyle ), value ) )
						throw new InvalidEnumArgumentException( "CellDisplayStyle", (int)value, typeof( CellDisplayStyle ) );

					this.cellDisplayStyle = value;

					this.InvalidateItemAllRegions( true );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.CellDisplayStyle );
				}
			}
		}

		#endregion // CellDisplayStyle

		// SSP 5/14/03 - Hiding Individual Cells Feature
		// 
		#region Hidden

		/// <summary>
		/// Specifies whether the cell is hidden.
		/// </summary>
		// SSP 5/12/05 - NAS 5.2 Filter Row
		// Made Hidden property virtual.
		//
		public virtual bool Hidden
		{
			get
			{
				return this.hidden;
			}
			set
			{
				if ( value != this.hidden )
				{
					this.hidden = value;

					// Cause the row to reposition all the cell elements so this cell can
					// get hidden or shown depending on the new value of the property.
					//
					if ( null != this.Row )
					{
						this.Row.Layout.BumpCellChildElementsCacheVersion( );
						this.Row.InvalidateItemAllRegions( true );
					}

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.Hidden );
				}
			}
		}

		#endregion // Hidden

		// JJD 12/12/03 - Added Accessibility support
		#region AccessibilityObject property

		/// <summary>
		/// Returns the accessible object representing the cell.
		/// </summary>
		public AccessibleObject AccessibilityObject
		{
			get
			{
				if ( this.accessibilityObject == null )
				{
					UltraGridBase grid = this.Layout.Grid;

					if ( grid != null )
						this.accessibilityObject = grid.CreateAccessibilityInstance	( this );
				}

				return this.accessibilityObject;
			}
		}

		#endregion AccessibilityObject property

		// JJD 12/16/03 - Added Accessibility support
		#region EditorAccessibilityObject property

		internal AccessibleObject EditorAccessibilityObject
		{
			get
			{
				if ( this.EditorResolved != null && 
					this.editorAccessibilityObject == null )
				{
					// SSP 4/18/05 - NAS 5.2 Filter Row
					// Use the new EditorOwnerInfo property of the cell because filter row cells 
					// use different owners.
					//
					//this.editorAccessibilityObject = this.EditorResolved.CreateAccessibilityInstance( this.Row.AccessibilityObject, this.column.EditorOwnerInfo, this.Row ); 
					this.editorAccessibilityObject = this.EditorResolved.CreateAccessibilityInstance( this.Row.AccessibilityObject, this.EditorOwnerInfo, this.Row ); 
				}

				return this.editorAccessibilityObject;
			}
		}

		#endregion EditorAccessibilityObject property

		// JJD 12/12/03 - Added Accessibility support
		#region Public Class CellAccessibleObject

		/// <summary>
		/// The Accessible object for a cell.
		/// </summary>
		public class CellAccessibleObject : AccessibleObjectWrapper
		{
			#region Private Members

			private UltraGridCell cell;

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor
			/// </summary>
			public CellAccessibleObject ( UltraGridCell cell )
			{
				if ( null == cell )
					throw new ArgumentNullException( "cell" );

				this.cell = cell;
			}

			#endregion Constructor

			#region Base Class Overrides

				#region Bounds

			/// <summary>
			/// Gets the location and size of the accessible object.
			/// </summary>
			public override System.Drawing.Rectangle Bounds
			{
				get
				{
					UIElement element = this.cell.GetUIElement( this.cell.Layout.ActiveRowScrollRegion, this.cell.Layout.ActiveColScrollRegion, true );

					if ( element == null )
						return Rectangle.Empty;

					Control control = element.Control;

					if ( control == null )
						return Rectangle.Empty;

					return control.RectangleToScreen( element.ClipRect );
				}
			}

				#endregion Bounds

				#region DefaultAction

			/// <summary>
			/// Gets a string that describes the default action of the object.
			/// </summary>
			public override string DefaultAction
			{
				get
				{
                    // MRS 2/23/2009 - Found while fixing TFS14354
					//if ( this.cell.Column.Hidden == false )
                    if (this.cell.Column.HiddenResolved == false)
					{
						if ( this.cell.ActivationResolved != Activation.Disabled )
						{
							if ( this.cell.CanEnterEditMode )
								return Shared.SR.GetString("DefaultAction_Cell_Editable");
							else
								return Shared.SR.GetString("DefaultAction_Cell");
						}
					}
					
					return null;
				}
			}

				#endregion DefaultAction

				#region Description

			/// <summary>
			/// Gets a string that describes the visual appearance of the specified object. Not all objects have a description.
			/// </summary>
			public override string Description
			{
				get
				{
					return null;
				}
			}

				#endregion Description

				#region DoDefaultAction

			/// <summary>
			/// Performs the default action associated with this accessible object.
			/// </summary>
			public override void DoDefaultAction()
			{
                // MRS 2/23/2009 - Found while fixing TFS14354
				//if ( this.cell.Column.Hidden == false )
                if (this.cell.Column.HiddenResolved == false)
				{
					if ( this.cell.ActivationResolved != Activation.Disabled )
					{
						this.cell.Activate();

						if ( this.cell.IsActiveCell && this.cell.CanEnterEditMode )
							this.cell.EnterEditMode(false, false );
					}
				}

			}

				#endregion DoDefaultAction

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				if ( index == 0 && this.cell.IsActiveCell && this.cell.IsInEditMode )
					return this.cell.EditorAccessibilityObject;

				return null;
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{

				return this.cell.IsActiveCell && this.cell.IsInEditMode ? 1 : 0;
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				if ( (this.State & AccessibleStates.Focused) != AccessibleStates.Focused )
					return null;

				if ( this.cell.IsInEditMode )
				{
					AccessibleObject acc = this.cell.EditorAccessibilityObject.GetFocused();

					if ( acc != null )
						return acc;
				}

				return this.AccessibleObject;
			}

				#endregion GetFocused

			//JJD 12/17/03
				#region GetMarshallingControl

			/// <summary>
			/// Returns the control used to synchronize accessibility calls.
			/// </summary>
			/// <returns>A control to be used to synchronize accessibility calls.</returns>
			protected override Control GetMarshallingControl()
			{
				return this.cell.Layout.Grid;
			}

				#endregion GetMarshallingControl

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				if ( this.cell.IsInEditMode )
				{
					AccessibleObject acc = this.cell.EditorAccessibilityObject.GetSelected();

					if ( acc != null )
						return acc;
				}
		
				if ( (this.State & AccessibleStates.Selected) != AccessibleStates.Selected )
					return null;

				return this.AccessibleObject;
			}

				#endregion GetSelected

				#region Help

			/// <summary>
			/// Gets a description of what the object does or how the object is used.
			/// </summary>
			public override string Help
			{
				get
				{
					return null;
				}
			}

				#endregion Help

				#region HitTest

			/// <summary>
			/// Retrieves the child object at the specified screen coordinates.
			/// </summary>
			/// <param name="x">The horizontal screen coordinate.</param>
			/// <param name="y">The vertical screen coordinate.</param>
			/// <returns>An AccessibleObject that represents the child object at the given screen coordinates. This method returns the calling object if the object itself is at the location specified. Returns a null reference (Nothing in Visual Basic) if no object is at the tested location.</returns>
			public override AccessibleObject HitTest(int x, int y)
			{
				
				return base.HitTest( x, y );
			}

				#endregion HitTest

				#region Name

			/// <summary>
			/// The accessible name for the data area.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if ( name != null && name.Length > 0 )
						return name;

					return this.cell.Column.Header.Caption;
				}
				set
				{
					base.Name = value;
				}
			}

				#endregion Name

				#region Navigate

            /// <summary>
            /// Navigates to another accessible object.
            /// </summary>
            /// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
            /// <returns>An AccessibleObject relative to this object.</returns>
            public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{
				UltraGridCell navigateToCell = null;

				switch ( navdir )
				{
						#region case Down

					case AccessibleNavigation.FirstChild:
					case AccessibleNavigation.LastChild:
						return base.Navigate( navdir );

						#endregion case 

						#region case Down

					case AccessibleNavigation.Down:
						navigateToCell = this.cell.Layout.GetAboveBelowCell( true,//down
							this.cell.Row, // currentRow
							this.cell, // currentCell
							false,//shiftKeyDown
							false,//ctlKeyDown
							false, // bSpanBands
							true, // bSpanRows 
							true, // bSpanParents
							false ); // bSelect*/

						break;

						#endregion case Down

						#region case Up

					case AccessibleNavigation.Up:
						navigateToCell = this.cell.Layout.GetAboveBelowCell( false,//down
							this.cell.Row, // currentRow
							this.cell, // currentCell
							false,//shiftKeyDown
							false,//ctlKeyDown
							false, // bSpanBands
							true, // bSpanRows 
							true, // bSpanParents
							false ); // bSelect*/
						break;

						#endregion case Up

						#region case Next

					case AccessibleNavigation.Next:
						navigateToCell = this.cell.Layout.GetNextCell( true,//next
							this.cell.Row, // currentRow
							this.cell, // currentCell
							false,//shiftKeyDown
							false,//ctlKeyDown
							true, // bSpanBands
							true, // bSpanRows 
							true, // bSpanParents
							false ); // bSelect*/
						break;

						#endregion case Next

						#region case Previous

					case AccessibleNavigation.Previous:
						navigateToCell = this.cell.Layout.GetNextCell( false,//next
							this.cell.Row, // currentRow
							this.cell, // currentCell
							false,//shiftKeyDown
							false,//ctlKeyDown
							true, // bSpanBands
							true, // bSpanRows 
							true, // bSpanParents
							false ); // bSelect*/
						break;

						#endregion case Previous

						#region case Right

					case AccessibleNavigation.Right:
						navigateToCell = this.cell.Layout.GetNextCell( true,//next
							this.cell.Row, // currentRow
							this.cell, // currentCell
							false,//shiftKeyDown
							false,//ctlKeyDown
							true, // bSpanBands
							true, // bSpanRows 
							true, // bSpanParents
							false ); // bSelect*/
						break;

						#endregion case Right

						#region case Left

					case AccessibleNavigation.Left:
						navigateToCell = this.cell.Layout.GetNextCell( false,//next
							this.cell.Row, // currentRow
							this.cell, // currentCell
							false,//shiftKeyDown
							false,//ctlKeyDown
							true, // bSpanBands
							true, // bSpanRows 
							true, // bSpanParents
							false ); // bSelect*/
						break;

						#endregion case Left
				}

				if ( navigateToCell != null )
					return navigateToCell.AccessibilityObject;

				UIElement element = this.cell.GetUIElement( this.cell.Layout.ActiveRowScrollRegion,  this.cell.Layout.ActiveColScrollRegion, false ); 

				if ( element != null )
					return element.Navigate( navdir );

				return null;
			}

				#endregion Navigate

				#region Parent

			/// <summary>
			/// Gets the parent of an accessible object.
			/// </summary>
			public override AccessibleObject Parent
			{
				get
				{
					return this.cell.Row.AccessibilityObject;
				}
			}

				#endregion Parent

				#region Role

			/// <summary>
			/// Gets the role of this accessible object.
			/// </summary>
			public override AccessibleRole Role
			{
				get
				{
					return AccessibleRole.Cell;
				}
			}

				#endregion Role

				#region Select

			/// <summary>
			/// Modifies the selection or moves the keyboard focus of the accessible object.
			/// </summary>
			/// <param name="flags">One of the <see cref="System.Windows.Forms.AccessibleSelection"/> values.</param>
			public override void Select(System.Windows.Forms.AccessibleSelection flags)
			{
				UltraGridCell item = this.cell;
				UltraGridBase gridBase = item.Layout.Grid;
				ISelectionManager selectionManager = gridBase as ISelectionManager;
				ISelectionStrategy selectionStrategy = null != selectionManager ? selectionManager.GetSelectionStrategy( item ) : null;

				Debug.Assert( null != selectionManager && null != selectionStrategy );
				if ( null == selectionManager || null == selectionStrategy )
					return;

				// We have a selection strategy that only allows contiguous selection.
				//
				bool isStrategyContiguous = 
					selectionStrategy is SelectionStrategyExtended 
					? ! ((SelectionStrategyExtended)selectionStrategy).IsDiscontiguousAllowed 
					: false;

				// NOTE: The order in which following if statements appear is important (at least for
				// some of them) because flags can contain multiple entries and thus they have to be
				// processed in a certain order. For example, TakeFocus can be combined with the 
				// ExtendSelection which necessiates processing ExtendSelection (which is supposed to
				// emulate Shift+Click on an item to range select) before processing TakeFocus.
				//

				if ( gridBase is UltraGrid )
				{
					UltraGrid grid = (UltraGrid)gridBase;

					// ExtendSelection does the range selection. It emulates the Shift + Click.
					// AddSelection adds the item to the selection. It emulates the Ctrl + Click.
					// Both of these actions do not clear the current selection.
					//
					if ( AccessibleSelection.ExtendSelection == ( AccessibleSelection.ExtendSelection & flags ) 
						|| AccessibleSelection.AddSelection == ( AccessibleSelection.AddSelection & flags ) )
					{
						// SSP 4/20/05 - NAS 5.2 Filter Row
						// Use the Selectable property instead.
						//
						//if ( item.CanSelectCell )
						if ( item.Selectable )
						{
							if ( selectionStrategy.IsSingleSelect )
							{
								// If selection strategy is single select, then clear the exisitng
								// selection.
								//
								grid.InternalSelectItem( item, true, true );
							}
								// Make sure the item can be selected with current selection. For example,
								// we don't allow seleting cells from different bands and also allow selecting
								// rows and cells at the same time.
								//
							else if ( selectionManager.IsItemSelectableWithCurrentSelection( item ) 
								&& ! selectionManager.IsMaxSelectedItemsExceeded( item ) )
							{
								if ( AccessibleSelection.ExtendSelection == ( AccessibleSelection.ExtendSelection & flags ) )
								{
									// If ExtendedSelection do range select.
									//
									grid.InternalSelectRange( item, false, true );
								}
								else 
								{
									// If AddSelection then add the item to the selection.
									//
									grid.InternalSelectItem( item, isStrategyContiguous, true );
								}
							}
						}
					}

					// TakeFocus makes the item the pivot item. For grid that means activating the
					// cell as well.
					//
					if ( AccessibleSelection.TakeFocus == ( AccessibleSelection.TakeFocus & flags ) )
					{
						// Activate the item.
						//
						item.Activate( );

						// Make the item the pivot item.
						//
						if ( item.IsActiveCell )
							selectionManager.SetPivotItem( item, false );
					}
			
					// Unselect the item.
					//
					if ( AccessibleSelection.RemoveSelection == ( AccessibleSelection.RemoveSelection & flags ) )
					{
						grid.InternalSelectItem( item, isStrategyContiguous, false );
					}

					// Select the item clearing other selected items.
					//
					if ( AccessibleSelection.TakeSelection == ( AccessibleSelection.TakeSelection & flags ) )
					{
						// SSP 4/20/05 - NAS 5.2 Filter Row
						// Use the Selectable property instead.
						//
						//if ( item.CanSelectCell )
						if ( item.Selectable )
							grid.InternalSelectItem( item, true, true );
					}
				}
				else if ( gridBase is UltraDropDownBase )
				{
					// UltraDropDown does not support cell selection.
					//
				}
				else
				{
					Debug.Assert( false, "Unknwon type of grid !" );
				}			
			}

				#endregion Select

				#region State

			/// <summary>
			/// Gets the state of this accessible object.
			/// </summary>
			public override AccessibleStates State
			{
				get
				{
					if ( this.cell.InternalHidden == true || this.cell.Column.IsChaptered )
						return AccessibleStates.Invisible;

					if ( this.cell.ActivationResolved == Activation.Disabled )
						return AccessibleStates.Unavailable;

					AccessibleStates state = AccessibleStates.Selectable;
					
					// MRS 04/02/04 - UWG2933
					// WInEyes apparently doesn't read the text unless the
					// object is Focusable. 
					state |= AccessibleStates.Focusable;

					if ( this.cell.Selected == true )
						state |= AccessibleStates.Selected;

					if ( this.cell.IsActiveCell == true )
					{
						if( this.cell.Layout.Grid.Focused )
							state |= AccessibleStates.Focused;
					}

					return state;
				}
			}

				#endregion State

				#region Value

			/// <summary>
			/// Returns the caption
			/// </summary>
			public override string Value
			{
				get
				{
					return this.cell.Text;
				}
				set
				{
					if ( this.cell.ActivationResolved == Activation.AllowEdit &&
						// SSP 4/18/05 - NAS 5.2 Filter Row
						// Use the new EditorOwnerInfo property of the cell because filter row cells 
						// use different owners.
						//
						//!this.cell.Column.EditorOwnerInfo.IsReadOnly( this.cell.Row ) &&
						!this.cell.EditorOwnerInfo.IsReadOnly( this.cell.Row ) &&
						this.cell.CanEnterEditMode)
					{
						if ( !this.cell.IsActiveCell )
							this.cell.Activate();

						if ( !this.cell.IsActiveCell )
							return;

						if ( !this.cell.IsInEditMode )
							this.cell.EnterEditMode( false );

						if ( !this.cell.IsInEditMode )
							return;

						EmbeddableEditorBase editor = this.cell.EditorResolved;

						if ( editor != null )
						{
							try 
							{
								if ( editor.SupportsSelectableText )
								{
									editor.SelectionStart = 0;
									editor.SelectionLength = editor.TextLength;
									editor.SelectedText = value;
								}
								else
									editor.Value = value;
							}
							catch{}
						}
					}

				}
			}

				#endregion Value

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Cell

			/// <summary>
			/// Returns the associated cell.
			/// </summary>
			public UltraGridCell Cell 
			{ 
				get 
				{
					return this.cell; 
				} 
			}

					#endregion Cell

				#endregion Public Properties

			#endregion Properties

		}

		#endregion Public Class CellAccessibleObject

		// SSP 6/30/04 - UltraCalc
		//
		#region CalcReference

		internal CellReference CalcReference
		{
			get
			{
				if ( null == this.calcReference && null != this.Layout )
					this.calcReference = new CellReference( this );

				return this.calcReference;
			}
		}

		#endregion // CalcReference

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return null != this.calcReference;
			}
		}

		#endregion // HasCalcReference

		#region HasFormulaCalcError

		internal bool HasFormulaCalcError
		{
			get
			{
				string errorInfoText;
				return this.GetFormulaCalcError( out errorInfoText );
			}
		}

		internal bool GetFormulaCalcError( out string errorInfoText )
		{
			errorInfoText = null;

			// SSP 4/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			// Use the new IsDataRow which checks for IsTemplateAddRow as well as other special
			// rows like the filter row.
			//
			//if ( this.Column.HasActiveFormula && ! this.Row.IsTemplateAddRow )
			if ( this.Column.HasActiveFormula && this.Row.IsDataRow )
			{
				bool isError;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//object val = RefUtils.GetFormulaCalcValue( 
				RefUtils.GetFormulaCalcValue( 
					this.CalcReference, out isError, out errorInfoText, this.Column.FormatInfo );

				return isError;
			}

			return false;
		}

		#endregion // HasFormulaCalcError

		#region ToolTipText

		// SSP 8/7/04 - Added Cell.ToolTipText property
		//
		/// <summary>
		/// Specifies the text to display in the tooltip that's displayed when the user hovers mouse over the cell.
		/// </summary>
		/// <remarks>
		/// <p class="body">Specifies the text to display in the tooltip that's displayed when the user hovers mouse over the cell.</p>
		/// <p><seealso cref="UltraGridOverride.TipStyleCell"/></p>
		/// </remarks>
		public string ToolTipText
		{
			get
			{
				return this.toolTipText;
			}
			set
			{
				// SSP 7/29/05 - Header, Row, Summary Tooltips
				// Notify the prop change and also dirty the associated cell elems.
				// 
				//this.toolTipText = value;
				if ( this.toolTipText != value )
				{
					this.toolTipText = value;

					this.InvalidateItemAllRegions( true );

					this.NotifyPropChange( PropertyIds.ToolTipText );
				}
			}
		}

		#endregion // ToolTipText

		#region ToolTipTextResolved
		
		internal string ToolTipTextResolved
		{
			get
			{
				if ( null != this.ToolTipText && this.ToolTipText.Length > 0 )
					return this.ToolTipText;

				string formulaCalcErrorInfoText;
				if ( this.GetFormulaCalcError( out formulaCalcErrorInfoText ) )
					return formulaCalcErrorInfoText;

				return null;
			}
		}

		#endregion // ToolTipTextResolved

		// SSP 11/3/04 - Merged Cell Feature
		//
		#region Merged Cell Feature

		#region IsMergedWith

		/// <summary>
		/// Returns true if this cell is merged with the specified cell. If the specified cell is the same as this cell then returns true even if this cell is not merged with any other cell.
		/// </summary>
		/// <param name="cell">A cell</param>
        /// <returns>true if this cell is merged with the specified cell. If the specified cell is the same as this cell then returns true even if this cell is not merged with any other cell.</returns>
		public bool IsMergedWith( UltraGridCell cell )
		{
			return this == cell || this.Column.AreCellsMerged( this.Row, cell.Row );
		}

		#endregion // IsMergedWith

		#region GetMergedCells

		/// <summary>
		/// Returns the cells that are merged with this cell, excluding this cell. If this cell is not merged with any other cell then returns null.
		/// </summary>
		/// <returns></returns>
		public UltraGridCell[] GetMergedCells( )
		{
			MergedCell mergedCell = this.Column.GetMergedCell( this.Row );
			return null != mergedCell ? mergedCell.GetCells( ) : null;
		}

		#endregion // GetMergedCells

		#region InvalidateItemAllRegions

		internal void InvalidateItemAllRegions( bool recalcRects, bool dirtyMergedCell )
		{
			base.InvalidateItemAllRegions( recalcRects );

			if ( dirtyMergedCell )
				RowsCollection.MergedCell_CellStateChanged( this, MergedCell.State.Invalidate );
		}

		#endregion // InvalidateItemAllRegions

		#endregion // Merged Cell Feature

		#region NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region CellClickActionResolved
		
		internal CellClickAction CellClickActionResolved
		{
			get
			{
				if ( this.Row.IsFilterRow )
					return CellClickAction.EditAndSelectText == this.Band.CellClickActionResolved
						? CellClickAction.EditAndSelectText : CellClickAction.Edit;

				// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
				//return this.Band.CellClickActionResolved;
				return this.Column.CellClickActionResolved;
			}
		}

		#endregion // CellClickActionResolved

		#region IsDataCell

		/// <summary>
		/// Returns true if the cell is associated with a cell in the data source. 
		/// This property returns false for cells associated with unbound columns, 
		/// filter rows, template add-rows, summary rows etc...
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Returns true if the cell is associated with a cell in the data source. 
		/// This property returns false for cells associated with unbound columns, 
		/// group-by rows, filter rows, template add-rows, summary rows etc...
		/// </p>
		/// <seealso cref="UltraGridRow.IsDataRow"/>
		/// </remarks>
		public virtual bool IsDataCell
		{
			get
			{
				return this.Row.IsDataRow && this.Column.IsBound;
			}
		}

		#endregion // IsDataCell

		#region IsFilterRowCell

		/// <summary>
		/// Indicates whether the cell is from a filter row.
		/// </summary>
		public bool IsFilterRowCell
		{
			get
			{
				return this.Row.IsFilterRow;
			}
		}

		#endregion // IsFilterRowCell
		
		#region HasActiveFormula

		internal bool HasActiveFormula
		{
			get
			{
				return this.Column.HasActiveFormula && this.Row.IsDataRow;
			}
		}

		#endregion // HasActiveFormula

		#region NullableResolved
		
		internal virtual Nullable NullableResolved
		{
			get
			{
				return this.Column.Nullable;
			}
		}

		#endregion // NullableResolved

		#region EditorOwnerInfo

		internal GridEmbeddableEditorOwnerInfoBase EditorOwnerInfo
		{
			get
			{
				return this.Row.GetEditorOwnerInfo( this.column );
			}
		}

		#endregion // EditorOwnerInfo

		#region OnActiveStateChanged

		internal virtual void OnActiveStateChanged( bool activated )
		{
			// MBS 3/21/08 - RowEditTemplate NA2008 V2
            // If a template is shown for the row, we should try to activate the corresponding proxy, if any
            if (activated)
            {
                UltraGridRowEditTemplate template = this.Row.RowEditTemplateResolved;
                if (template != null && template.IsShown && this.Layout.Grid.Focused)
                    template.FocusActiveProxy(this);
            }
		}

		#endregion // OnActiveStateChanged

		#endregion // NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

        // CDS 02/02/09 TFS12512 Moved to the UltraGridColumn class.
//        // CDS NAS v9.1 Header CheckBox
//        #region NAS v9.1 Header CheckBox

//        #region UpdateHeaderCheckBox

//#if DEBUG
//        /// <summary>
//        /// Forces the HeaderCheckBox to be updated by dirtying the cached CheckState.
//        /// </summary>
//#endif
//        private void UpdateHeaderCheckBox()
//        {
//            if (column.IsColumnDataTypeRepresentableByCheckBox)
//            {
//                switch (this.column.HeaderCheckBoxSynchronizationResolved)
//                {
//                    case HeaderCheckBoxSynchronization.Band:
//                        column.Band.DirtyCheckStateCacheValidEntry(this.column);
//                        this.column.DirtyHeaderUIElements(null);
//                        break;
//                    case HeaderCheckBoxSynchronization.RowsCollection:
//                        this.Row.ParentCollection.DirtyCheckStateCacheValidEntry(this.column);
//                        this.column.DirtyHeaderUIElements(this.Row.ParentCollection);
//                        break;
//                    case HeaderCheckBoxSynchronization.None:
//                        return;
//                    default:
//                        Debug.Fail("Invalid HeaderCheckBoxSynchronization returned from the Resolved property.");
//                    break;
//                }
//            }
//        }

//        #endregion UpdateHeaderCheckBox

//        #endregion NAS v9.1 Header CheckBox

        // MRS 3/18/2009 - TFS15302
        #region GetGrid
        private UltraGridBase GetGrid()
        {
            UltraGridColumn column = this.Column;
            if (column == null)
                return null;

            UltraGridBand band = column.Band;
            if (band == null)
                return null;

            UltraGridLayout layout = band.Layout;
            if (layout == null)
                return null;

            return layout.Grid;
        }
        #endregion //GetGrid

        // MBS 3/31/09 - NA9.2 CellBorderColor
        // If we need to draw the active border around the cell area, then we should be invalidating
        // a larger area than just the cell's element         
        #region InvalidateElem

        internal override void InvalidateElem(UIElement element, bool recalcRects)
        {
            if (element != null)
            {
                RowColRegionIntersectionUIElement rowColRegion = element.GetAncestor(typeof(RowColRegionIntersectionUIElement)) as RowColRegionIntersectionUIElement;
                if(rowColRegion != null)                
                {
                    // The border thickness that we're drawing could have changed since we last draw it,
                    // such as in the case where the user enables this at run-time, or makes it larger, 
                    // so we should invalidate the largest area of the two thicknesses so that we ensure
                    // that we clear (or draw) the correct area.
                    int lastBorderThickness = rowColRegion.LastActiveCellBorderThickness;
                    int currentBorderThickness = this.Band.ActiveCellBorderThicknessResolved;
                    int borderThickness = Math.Max(lastBorderThickness, currentBorderThickness);

                    // MBS 4/16/09 - NA9.2 Selection Overlay
                    // If we're not invalidating an active cell, we may have to have to invalidate the area that accounts
                    // for the borders used by the selection overlay.  Note that we can simply check the border's resolved
                    // color and not the actual overlay color since we will resolve this property ourselves if it's the default.
                    if (this.Layout.SelectionOverlayBorderColorResolved != Color.Empty)
                        borderThickness = Math.Max(borderThickness, this.Layout.SelectionOverlayBorderThicknessResolved);

                    if (borderThickness > 0)
                    {
                        UltraGrid grid = this.GetGrid() as UltraGrid;
                        if (grid != null)
                        {
                            // Since we're drawing a border around the entire cell region, even with cell spacing, we
                            // should invalidate a larger area since the border could be drawn in the "spacing" area
                            borderThickness += this.Band.CellSpacingResolved;

                            Rectangle rect = UltraGridCell.CalculateCellOverlayRect(element, borderThickness, true);
                            grid.Invalidate(rect);

                            // If we don't have to recalculate the rects, then we can return at this point since
                            // we've already performed the invalidation
                            if (!recalcRects)
                                return;
                        }
                    }
                }
            }
            
            base.InvalidateElem(element, recalcRects);
        }
        #endregion //InvalidateElem

        // MBS 4/3/09 - NA9.2 CellBorderColor
        // Created a centralized helper method to determine the rect that the active border is drawn into
        // so that we don't have to invalidate a larger region than neccessary.
        #region CalculateCellOverlayRect

        internal static Rectangle CalculateCellOverlayRect(UIElement element, int borderWidth, bool calculateInvalidRect)
        {
            Rectangle cellRect = element.Rect;
            CellUIElementBase cellElement = element as CellUIElementBase;
            if (cellElement == null)
                return cellRect;

            UltraGridColumn column = null;
            UIElement rowCellAreaElem = cellElement.GetAncestor(typeof(RowCellAreaUIElement));
            if (rowCellAreaElem != null)
            {
                Rectangle rowRect = rowCellAreaElem.Rect;
                column = cellElement.Column;

                bool hasAdjustedHeight = false;                

                if (column.Band.CellSpacingResolved == 0)
                {
                    // We want to handle the case where the cell is positioned lower than the row element.  This should
                    // account for the border merging of the row with the element above it
                    if (!column.HasRowLayoutColumnInfo || column.RowLayoutColumnInfo.OriginYResolved <= 0)
                    {
                        int yCellOffset = cellRect.Top - rowRect.Top;
                        if (yCellOffset != 0)
                        {
                            cellRect.Y -= yCellOffset;
                            cellRect.Height += yCellOffset;

                            hasAdjustedHeight = true;
                        }
                    }

                    // Handle any remaining offsets due to border merging
                    UIElementBorderStyle borderStyle = column.Band.BorderStyleCellResolved;
                    if (column.Layout.CanMergeAdjacentBorders(borderStyle))
                    {
                        // If the element is responsible for drawing its right border, we should shrink the rect
                        // so that the border is drawn over the cell's right edge
                        if ((cellElement.BorderSides & Border3DSide.Right) != 0)
                            cellRect.Width--;

                        // In the case where we have an even-numbered border thickness and we're drawing the left
                        // border (such as in a row layout), we need to compensate for the rect's position and rounding
                        if (borderWidth % 2 == 0)
                        {
                            if ((cellElement.BorderSides & Border3DSide.Left) != 0)
                            {
                                cellRect.Width--;
                                cellRect.X++;
                            }
                        }
                        // In an odd-numbered border case, we need to shift the rect over
                        // by one when we're the first column and we're not aligned with
                        // the edge
                        else if (column.IsFirstVisibleColumnOnLevel)
                        {
                            int xCellOffset = cellRect.Left - rowRect.Left;
                            if (xCellOffset > 0)
                            {
                                Debug.Assert(xCellOffset == 1, "Expected to only be offset by 1");
                                cellRect.X -= xCellOffset;
                                cellRect.Width += xCellOffset;
                            }
                        }

                        // If the element is responsible for drawing its bottom border, we should shrink the rect
                        // so that the border is drawn over the cell's bottom edge
                        if (!hasAdjustedHeight && (cellElement.BorderSides & Border3DSide.Bottom) != 0)
                            cellRect.Height--;
                    }

                    // If we're drawing an even-numbered border thickness, we always need to offset the rect to draw over
                    // the correct borders, even if we've made adjustments above.
                    if (borderWidth % 2 == 0)
                    {
                        cellRect.Y++;
                        cellRect.Height--;
                    }
                }
                // If we have any cell spacing, we know that we're drawing all 4 borders of the cell on the grid already,
                // so all that we need to do here is just account for the rounding issue that we have with an odd thickness
                else if (borderWidth % 2 != 0)
                {
                    cellRect.Width -= 1;
                    cellRect.Height -= 1;
                }
            }            

            // If we're calculating the invalid rect, we need to actually inflate this by the full border width,
            // since the pen's width will draw outside the bounds of the rect.
            int inflate = calculateInvalidRect ? borderWidth : (int)(borderWidth / 2.0);

            cellRect.Inflate(inflate, inflate);

            return cellRect;
        }
        #endregion //CalculateCellOverlayRect

        // MRS - NAS 9.2 - Control Container Editor

        #region NAS 9.2 - Control Container Editor

        #region EditorComponent

        /// <summary>
        /// Component that implements IProvidesEmbeddableEditor. Attempt to set a component that
        /// does not implement IProvidesEmbeddableEditor interface will cause an exception.
        /// </summary>
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_ControlContainerEditor)]
        public Component EditorComponent
        {
            get
            {
                return this.editorComponent;
            }
            set
            {
                if (this.editorComponent != value)
                {
                    if (null != value && !(value is IProvidesEmbeddableEditor))
                        throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_316"), Shared.SR.GetString("LE_ArgumentException_317"));

                    IProvidesEmbeddableEditor editorProvider = value as IProvidesEmbeddableEditor;

                    if (null != editorProvider)
                    {
                        EmbeddableEditorBase editor = editorProvider.Editor;

                        if (!this.Column.IsEditorAppropriate(editor))
                            throw new ArgumentException(SR.GetString("LER_Exception_338", editorProvider.GetType().Name), SR.GetString("LER_ArguementException_317"));
                    }

                    // Exit the edit mode if the cell is in edit mode.
                    //
                    if (this.IsInEditMode)
                        this.ExitEditMode(false, true);

                    this.editorComponent = value;

                    this.DirtyCachedEditor();

                    // Dirty the cell element.
                    //
                    this.InvalidateItemAllRegions(true);
                }
            }
        }

        #endregion // EditorComponent

        #region EditorControlResolved

        /// <summary>
        /// Returns the resolved editor component. If the EditorComponent property is set on the cell then it returns the value of that property otherwise it returns the value of the EditorComponent property of the associated column.
        /// </summary>
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_ControlContainerEditor)]
        public Component EditorComponentResolved
        {
            get
            {
                if (null != this.EditorComponent)
                    return this.EditorComponent;

                return this.Column.EditorComponent;
            }
        }

        #endregion // EditorComponentResolved

        #endregion //NAS 9.2 - Control Container Editor

        #region NAS 9.2 ActiveAppearance


        #region ActiveAppearance

        // 
        // Added ActiveAppearance property.
        // 
        /// <summary>
        /// Determines the formatting attributes that will be applied to the cell when it's active.
        /// </summary>
        /// <remarks>
        /// <p class="body">
        /// <b>Note:</b> To set this appearance on the grid or band level, use the Override's 
        /// <see cref="UltraGridOverride.ActiveCellAppearance"/> property.
        /// </p>
        /// </remarks>        
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_CellActiveAppearance)
        ]
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Infragistics.Win.AppearanceBase ActiveAppearance
        {
            get
            {
                // if we don't already have an appearance object then create it now
                //
                if (null == this.activeAppearance)
                {
                    this.activeAppearance = new Infragistics.Win.Appearance();

                    // hook up the prop change notifications
                    //
                    UltraGridLayout layout = this.Layout;
                    if (null != layout)
                        layout.SetMulticastAppearance(this.activeAppearance, this);
                }

                return this.activeAppearance;
            }
            set
            {
                if (this.activeAppearance != value)
                {
                    UltraGridLayout layout = this.Layout;

                    // remove the old prop change notifications
                    //
                    if (null != this.activeAppearance)
                    {
                        if (null != layout)
                            layout.RemoveMulticastAppearance(this.activeAppearance, this);
                    }

                    this.activeAppearance = value;

                    // Hook up the new prop change notifications.
                    //
                    if (null != this.activeAppearance && null != layout)
                        layout.SetMulticastAppearance(this.activeAppearance, this);

                    this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.ActiveCellAppearance);
                }
            }
        }

        #endregion // ActiveAppearance

        #region HasActiveAppearance

        // SSP 8/26/06 - NAS 6.3
        // Added ActiveAppearance property.
        // 
        /// <summary>
        /// Returns true if the active appearance object has been allocated.
        /// </summary>
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_CellActiveAppearance)
        ]
        public bool HasActiveAppearance
        {
            get
            {
                return null != this.activeAppearance;
            }
        }

        #endregion // HasActiveAppearance

        #endregion NAS 9.2 ActiveAppearance

    }// end of class
}
