#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Permissions;
using Infragistics.Win.Layout;
using System.Collections.Generic;

// SSP 11/29/05 - NAS 6.1 Multi-cell Operations
// Added this file.
// 

namespace Infragistics.Win.UltraWinGrid
{

	#region ActionHistory Class

	internal class ActionHistory
	{
		#region ActionBase Class

		internal abstract class ActionBase
		{
			private ActionHistory actionHistory = null;

            // MRS 3/10/2008 - BR30851
            private bool checkForReadOnlyCells = true;

			internal ActionBase( ActionHistory actionHistory )
			{
				GridUtils.ValidateNull( actionHistory );

				this.actionHistory = actionHistory;
			}
			
			internal ActionHistory ActionHistory
			{
				get
				{
					return this.actionHistory;
				}
			}

			internal UltraGridLayout Layout
			{
				get
				{
					return this.ActionHistory.Layout;
				}
			}

            // MRS 3/10/2008 - BR30851
            internal bool CheckForReadOnlyCells
            {
                get { return this.checkForReadOnlyCells; }
                set { this.checkForReadOnlyCells = value; }
            }

			internal abstract bool CanPerform( object context );

			internal abstract bool Perform( object context, out ActionBase undoAction );
		}

		#endregion // UndoActionBase Class

		#region ActionsCollection Class

		private class ActionsCollection
		{
			private const int MAX_ITEMS = 100;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//private ArrayList stack;
			private List<ActionBase> stack;
			
			internal ActionsCollection( )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//this.stack = new ArrayList( );
				this.stack = new List<ActionBase>();
			}

			internal void Push( ActionBase action )
			{
				this.stack.Add( action );
				if ( this.Count > MAX_ITEMS )
					this.stack.RemoveAt( 0 );
			}

			internal ActionBase Peek( )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//return (ActionBase)this.stack[ this.Count - 1 ];
				return this.stack[ this.Count - 1 ];
			}

			internal ActionBase Pop( )
			{
				ActionBase action = this.Peek( );
				this.stack.RemoveAt( this.Count - 1 );
				return action;
			}

			internal int Count
			{
				get
				{
					return this.stack.Count;
				}
			}

			internal void Clear( )
			{
				this.stack.Clear( );
			}
		}

		#endregion // ActionsCollection Class

		#region Private Vars

		private MultiCellOperationInfo multiCellOperationInfo = null;
		private ActionsCollection undoActions = null;
		private ActionsCollection redoActions = null;
		private bool isPerformingAction = false;

		#endregion // Private Vars

		#region Constructor

		internal ActionHistory( MultiCellOperationInfo multiCellOperationInfo )
		{
			this.multiCellOperationInfo = multiCellOperationInfo;
		}

		#endregion // Constructor

		#region MultiCellOperationInfo

		internal MultiCellOperationInfo MultiCellOperationInfo
		{
			get
			{
				return this.multiCellOperationInfo;
			}
		}

		#endregion // MultiCellOperationInfo

		#region Layout

		internal UltraGridLayout Layout
		{
			get
			{
				return this.MultiCellOperationInfo.Layout;
			}
		}

		#endregion // Layout

		#region UndoActions

		private ActionsCollection UndoActions
		{
			get
			{
				if ( null == this.undoActions )
					this.undoActions = new ActionsCollection( );

				return this.undoActions;
			}
		}

		#endregion // UndoActions

		#region HasUndoActions

		private bool HasUndoActions
		{
			get
			{
				return null != this.undoActions && this.undoActions.Count > 0;
			}
		}

		#endregion // HasUndoActions

		#region RedoActions

		private ActionsCollection RedoActions
		{
			get
			{
				if ( null == this.redoActions )
					this.redoActions = new ActionsCollection( );

				return this.redoActions;
			}
		}

		#endregion // RedoActions

		#region HasRedoActions

		private bool HasRedoActions
		{
			get
			{
				return null != this.redoActions && this.redoActions.Count > 0;
			}
		}

		#endregion // HasRedoActions

		#region UndoActionOnQueue

		private ActionBase UndoActionOnQueue
		{
			get
			{
				return this.HasUndoActions ? this.UndoActions.Peek( ) : null;
			}
		}

		#endregion // UndoActionOnQueue

		#region RedoActionOnQueue

		private ActionBase RedoActionOnQueue
		{
			get
			{
				return this.HasRedoActions ? this.RedoActions.Peek( ) : null;
			}
		}

		#endregion // RedoActionOnQueue

		#region CanUndo

		internal bool CanUndo( )
		{
			ActionBase action = this.UndoActionOnQueue;
			return null != action && action.CanPerform( new MultiCellOperationInfo.MultiCellOperationContext( MultiCellOperation.Undo ) );
		}

		#endregion // CanUndo

		#region CanRedo

		internal bool CanRedo( )
		{
			ActionBase action = this.RedoActionOnQueue;
			// SSP 8/25/06
			// Andrew noticed this typo. It should have been passing in the context of Redo instead of Undo.
			// 
			//return null != action && action.CanPerform( new MultiCellOperationInfo.MultiCellOperationContext( MultiCellOperation.Undo ) );
			return null != action && action.CanPerform( new MultiCellOperationInfo.MultiCellOperationContext( MultiCellOperation.Redo ) );
		}

		#endregion // CanRedo

		#region Clear

		internal void Clear( )
		{
			if ( this.HasUndoActions )
			{
				this.UndoActions.Clear( );
				this.undoActions = null;
			}

			if ( this.HasRedoActions )
			{
				this.RedoActions.Clear( );
				this.redoActions = null;
			}
		}

		#endregion // Clear

		#region IsPerformingAction

		internal bool IsPerformingAction
		{
			get
			{
				return this.isPerformingAction;
			}
		}

		#endregion // IsPerformingAction

		#region Undo

		internal bool Undo( )
		{
			return this.HasUndoActions 
				&& this.UndoRedoHelper( 
						new MultiCellOperationInfo.MultiCellOperationContext( MultiCellOperation.Undo ),
						this.UndoActions, 
						this.RedoActions );
		}

		#endregion // Undo

		#region Redo

		internal bool Redo( )
		{
			return this.HasRedoActions
				&& this.UndoRedoHelper( 
						new MultiCellOperationInfo.MultiCellOperationContext( MultiCellOperation.Redo ),
						this.RedoActions, 
						this.UndoActions );
		}

		#endregion // Redo

		#region UndoRedoHelper
		
		private bool UndoRedoHelper( object actionContext, ActionsCollection xxStack, ActionsCollection yyStack )
		{
			ActionBase action = xxStack.Pop( );
			while ( ! action.CanPerform( actionContext ) && xxStack.Count > 0 )
				action = xxStack.Pop( );

			return this.PerformActionHelper( actionContext, action, yyStack );
		}

		#endregion // UndoRedoHelper

		#region PerformActionHelper

		private bool PerformActionHelper( object actionContext, ActionBase action, ActionsCollection undoList )
		{
			// Set the isPerformingAction flag so we can ignore the ItemChanged notifications
			// that result from the action. This is because in response to ItemChanged we end 
			// up clearing the undo history.
			// 
			bool origIsPerformingAction = this.isPerformingAction;
			this.isPerformingAction = true;

			try
			{
				ActionBase undoAction;
				bool success = action.Perform( actionContext, out undoAction );
				if ( success )
				{
					if ( null != undoAction )
						// If an undo action was returned then push onto the undo stack.
						// 
						undoList.Push( undoAction );
					else 
						// Otherwise clear the current undo/redo history since the action
						// was performed however it didn't returned an undo action and thus
						// the action can not be undone.
						//
						this.Clear( );
				}

				return success;
			}
			finally
			{
				this.isPerformingAction = origIsPerformingAction;
			}
		}

		#endregion // PerformActionHelper

		#region PerformAction

		internal bool PerformAction( object actionContext, ActionBase action )
		{
			return this.PerformActionHelper( actionContext, action, this.UndoActions );
		}

		#endregion // PerformAction

	}

	#endregion // ActionHistory Class

	#region ActionFactory Class

	internal class ActionFactory
	{
		private ActionFactory( )
		{
		}

		#region SingleCellValueAction Class
		
		internal class SingleCellValueAction : ActionHistory.ActionBase
		{
			private UltraGridCell cell = null;
			private object value = null;

			// SSP 4/1/06 BR11228
			// 
			private bool dontExitEditMode = false;

			internal SingleCellValueAction( ActionHistory actionHistory, UltraGridCell cell, object newVal )
				: base( actionHistory )
			{
				GridUtils.ValidateNull( cell );
				this.cell = cell;
				this.value = newVal;
			}

			// SSP 4/1/06 BR11228
			// 
			internal bool DontExitEditMode
			{
				get
				{
					return this.dontExitEditMode;
				}
				set
				{
					this.dontExitEditMode = value;
				}
			}

			private MultiCellValuesAction CreateMultiCellValueAction( )
			{
				UltraGridCell cell = this.cell;
				object cellVal = this.value;

				// Create a cell values collection based on the single cell and the new value 
				// that we are attempting to assign to the cell.
				// 
				MultiCellOperationInfo.CellsCollection cells = new MultiCellOperationInfo.CellsCollection( this.Layout, new UltraGridCell[] { cell }, 1, 1 );
				MultiCellOperationInfo.CellValuesCollection cellValues = new MultiCellOperationInfo.CellValuesCollection( cells );
				
				// Set the new value on the cell value holder.
				// 
				MultiCellOperationInfo.CellValueHolder cvh = cellValues[ cell ];
				cvh.Value = cellVal;
				cvh.IsValueDisplayText = false;

				return new MultiCellValuesAction( this.ActionHistory, cellValues );
			}

			internal override bool CanPerform( object context )
			{
				// AS 1/10/06 BR08747
				// See MultiCellValuesAction.CanPerform
				//
				//return ! MultiCellValuesAction.IsCellDeleted( this.cell );
				return MultiCellValuesAction.CanPerform(context, this.cell);
			}

			internal override bool Perform( object context, out ActionHistory.ActionBase undoAction )
			{
				undoAction = null;
				bool ret = false;

				if ( this.CanPerform( context ) )
				{
					undoAction = new SingleCellValueAction( this.ActionHistory, this.cell, this.cell.Value );

                    // MRS 3/10/2008 - BR30851
                    undoAction.CheckForReadOnlyCells = this.CheckForReadOnlyCells;

					// If the context is MultiCellOperation, then perform the action via the 
					// multiCellValueAction. This way it will fire the BeforeMultiCellOperation event.
					// 
					if ( context is MultiCellOperationInfo.MultiCellOperationContext )
					{
						ActionHistory.ActionBase discard;
						MultiCellValuesAction action = this.CreateMultiCellValueAction( );

                        // MRS 3/10/2008 - BR30851
                        action.CheckForReadOnlyCells = this.CheckForReadOnlyCells;

						ret = action.Perform( context, out discard );
					}
					else
					{
						ret = this.cell.SetValueInternal( this.value, false, false );

						// Also select, activate and scroll into view the cell that was undone.
						// 
						// SSP 4/1/06 BR11228
						// If DontExitEditMode is true then we want the cell to remain in edit mode. So don't
						// select the cell.
						// 
                        //if ( ret )
                        // MRS 1/8/2008 - BR29480
                        // It doesn't make sense to scroll here. I'm 99% sure we only get into this code
                        // path when we are updating the value of a cell. We don't get in here for a copy,
                        // paste, undo, redo, etc. So in this case, there's no reason to scroll. 
                        //if (ret && !this.DontExitEditMode)
                        //    this.ActionHistory.MultiCellOperationInfo.ScrollAndSelect(new UltraGridCell[] { cell }, true, true);
					}

					// SSP 4/1/06 BR11228
					// 
					// ------------------------------------------------------------------
					if ( null != this.cell && this.cell.IsFilterRowCell )
					{
						UltraGridFilterCell filterCell = (UltraGridFilterCell)this.cell;
						if ( ! filterCell.IsInEditMode )
							filterCell.ApplyFilters( );
					}
					// ------------------------------------------------------------------
				}

				return ret;
			}
		}

		#endregion // SingleCellValueAction Class

		#region MultiCellValuesAction Class

		internal class MultiCellValuesAction : ActionHistory.ActionBase
		{
			private MultiCellOperationInfo.CellValuesCollection cellValues;

			internal MultiCellValuesAction( ActionHistory actionHistory,
				MultiCellOperationInfo.CellValuesCollection cellValues ) : base( actionHistory )
			{
				this.cellValues = cellValues;
			}

			internal static bool IsCellDeleted( UltraGridCell cell )
			{
				return cell.Disposed || cell.Row.Disposed;
			}

			// AS 1/10/06 BR08747
			// I talked this over with Joe and we decided that since we cannot 
			// determine whether undo/redo is allowed based on the current 
			// selection, active cell/row, etc., we need to let the action 
			// determine the state by having it delegate to the band to which 
			// it belongs. Otherwise, the MultiCellOperationInfo is allowing
			// an undo/redo regardless of the band's MultiCellOperation value
			// as long as something exists in the undo/redo stack.
			// 
			internal static bool CanPerform( object context, UltraGridCell cell )
			{
				if ( IsCellDeleted(cell) )
					return false;

				if ( context is MultiCellOperationInfo.MultiCellOperationContext )
				{
					MultiCellOperation operation = ((MultiCellOperationInfo.MultiCellOperationContext)context).operationBeingPerformed;

					switch( operation )
					{
						case MultiCellOperation.Undo:
						case MultiCellOperation.Redo:
							return cell.Band.IsMultiCellOperationAllowed(operation);
					}
				}

				return true;
			}

			internal override bool CanPerform( object context )
			{ 
				// NOTE: The behavior that was decided was that we would undo much as possible 
				// and ignore deleted rows, even if they were part of the operation.
				// 

				foreach ( UltraGridCell cell in cellValues.Cells )
				{
					// AS 1/10/06 BR08747
					// See MultiCellValuesAction.CanPerform
					//
					//if ( ! IsCellDeleted( cell ) )
					if ( MultiCellValuesAction.CanPerform(context, cell) )
						return true;
				}

				return false;
			}

			private void FireError( MultiCellOperationErrorInfo errorInfo )
			{
				UltraGrid grid = this.Layout.Grid as UltraGrid;
				if ( null != grid )
					grid.InternalHandleMultiCellOperationError( errorInfo, false );
			}

			private bool SetCellValueHelper( MultiCellOperationInfo.MultiCellOperationContext context,
				UltraGridCell cell, object value, out MultiCellOperationErrorInfo errorInfo )
			{
				errorInfo = null;

				try
				{
					// AS 1/17/06 BR09015
					// A multicell operation is a user performed action and as such
					// we should let the rows collection know.
					//
					//return cell.SetValue( value, true, false );
					// SSP 2/21/06 BR10249
					// Added an overload of SetValue that takes in throwExceptionOnError parameter. 
					// Pass true for that parameter. This is to prevent the SetValue from raising
					// the Error event itself. We need it throw an exception instead so we can
					// raise the Error event below and pass in the proper multi-cell error info
					// context.
					// 
					//bool changed = cell.SetValue( value, true, false );
					bool changed = cell.SetValueInternal( value, true, false, true );

					// SSP 11/12/03 Add Row Feature
					// Reset the TemplateAddRowHiddenFlag flag on the associated row's parent collection
					// then so the template add-row shows up as soon as the add-row is modified.
					//
					if (changed && null != cell.Row && null != cell.Row.ParentCollection )
						cell.Row.ParentCollection.OnUserModifiedCellValue( cell );

					return changed;
				}
				catch ( Exception exception )
				{
					errorInfo = new MultiCellOperationErrorInfo( context.operationBeingPerformed, 
						cell, exception, null, ! this.IsLastCell( cell ) );
					this.FireError( errorInfo );
					return false;
				}
			}

			private bool IsLastCell( UltraGridCell cell )
			{
				return null != cell && cell == this.cellValues.Cells.LastCell;
			}

			private UltraGridRow lastRow_InitializeRowSuspended = null;

			private bool UpdateRowHelper( MultiCellOperationInfo.MultiCellOperationContext context,
				UltraGridCell lastCell, UltraGridCell currCell, UpdateMode updateMode, out MultiCellOperationErrorInfo errorInfo )
			{
				bool ret = true;

				errorInfo = null;
				UltraGridRow lastRow = null != lastCell ? lastCell.Row : null;
				UltraGridRow currRow = null != currCell ? currCell.Row : null;
				DataErrorInfo dataErrorInfo = null;
				UltraGridCell dataErrorCell = null;

				switch ( updateMode )
				{
					case UpdateMode.OnCellChange:
					case UpdateMode.OnCellChangeOrLostFocus:
						if ( null != currRow )
						{
							ret = currRow.Update( out dataErrorInfo );

							if ( null != dataErrorInfo )
								dataErrorCell = currCell;
						}
						break;
					default:
						// Update the row on row change.
						// 
						if (
							// When we move onto the next row, update the previous row.
							// 
							lastRow != currRow && null != lastRow 
							// This is for not updating the row if the operation is being performed inside
							// a single row, and that row is active.
							// 
							&& ( null != currRow || this.cellValues.Cells.RowCount >= 2 || ! lastRow.IsActiveRow )
							// Only do so if the update mode is OnRowChange or OnRowChangeOrLostFocus.
							// 
							&& ( UpdateMode.OnRowChange == updateMode || UpdateMode.OnRowChangeOrLostFocus == updateMode ) )
						{
							ret = lastRow.Update( out dataErrorInfo );

							if ( null != dataErrorInfo )
								dataErrorCell = lastCell;
						}

						// Fire the InitializeRow event for the row as well. This code is for firing the
						// InitializeRow once per row when performing multi-cell operations.
						// 
						if ( this.lastRow_InitializeRowSuspended != currRow )
						{
							// First resume the InitializeRow on the last suspended row. Also fire
							// the InitializeRow on that row.
							// 
							if ( null != this.lastRow_InitializeRowSuspended )
							{
								UltraGridRow tmpRow = this.lastRow_InitializeRowSuspended;
								this.lastRow_InitializeRowSuspended = null;

								tmpRow.Layout.ResumeInitializeRowFor( tmpRow );
								tmpRow.FireInitializeRow( );
							}

							// Suspend the InitializeRow on the new row.
							// 
							if ( null != currRow )
							{
								bool alreadySuspended = currRow.Layout.SuspendInitializeRowFor( currRow );
								if ( ! alreadySuspended )
									this.lastRow_InitializeRowSuspended = currRow;
							}
						}

						break;
				}

				if ( null != dataErrorInfo )
				{
					errorInfo = new MultiCellOperationErrorInfo( context.operationBeingPerformed, dataErrorCell, null, dataErrorInfo, null != currCell && ! this.IsLastCell( currCell ) );
					this.FireError( errorInfo );
				}

				return ret;
			}

			internal override bool Perform( object contextParam, out ActionHistory.ActionBase undoActionParam )
			{
				undoActionParam = null;
				MultiCellOperationInfo.MultiCellOperationContext context = (MultiCellOperationInfo.MultiCellOperationContext)contextParam;

				UltraGrid grid = this.Layout.Grid as UltraGrid;
				if ( null == grid || ! this.CanPerform( context ) )
					return false;

				// Fire the BeforeMultiCellOperation event and if it's cancelled, return false.
				// 
				BeforeMultiCellOperationEventArgs e = new BeforeMultiCellOperationEventArgs(
					context.operationBeingPerformed, this.cellValues );

				grid.FireEvent( GridEventIds.BeforeMultiCellOperation, e );
				if ( e.Cancel )
					return false;

				// Get the current values of the cells that we are going to be modifying. This is used
				// for undo.
				// 
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//MultiCellOperationInfo.CellValuesCollection currentCellValues = 
				//    this.ActionHistory.MultiCellOperationInfo.GetCellValues( this.cellValues.Cells );
				MultiCellOperationInfo.CellValuesCollection currentCellValues =
					MultiCellOperationInfo.GetCellValues( this.cellValues.Cells );

				// Based on the current cell values, create an action instance for undo'ing the 
				// operation.
				// 
				MultiCellValuesAction undoAction = new MultiCellValuesAction( this.ActionHistory, currentCellValues );
				undoActionParam = undoAction;

				// If necessary, convert the display texts to data values. When pasting, the values
				// are display texts. We need to conver them to the underlying column's data types, and
				// also apply editor data fitler.
				// 
				UltraGridCell stopCellExclusive;
                // MRS 3/10/2008 - BR30851
				//if ( ! this.cellValues.ConvertToDataValue( context, out stopCellExclusive, true ) )
                if (!this.cellValues.ConvertToDataValue(context, out stopCellExclusive, this.CheckForReadOnlyCells))
					return false;
				
				// Keep track of cells that are modified so we can select those cells.
				// 
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList modifiedCells = new ArrayList( );
				List<UltraGridCell> modifiedCells = new List<UltraGridCell>();

				MultiCellOperationErrorInfo errorInfo = null;
				UpdateMode updateMode = grid.UpdateMode;
				UltraGridCell lastUpdatedCell = null;

				// When performfing paste, redo or undo, activate the first cell, if the current 
				// active cell is not already part of the cells that are going to be modified.
				// 
				bool activateFirstCell = false;
				if ( MultiCellOperation.Paste == context.operationBeingPerformed 
					|| MultiCellOperation.Redo == context.operationBeingPerformed 
					|| MultiCellOperation.Undo == context.operationBeingPerformed )
				{
					UltraGridCell activeCell = grid.ActiveCell;
					if ( null == activeCell || ! this.cellValues.Exists( activeCell ) )
						activateFirstCell = true;
				}

				// If conversion is successful and the action is not canceled, set the
				// cell values to the new values.
				// 
				foreach ( UltraGridCell cell in this.cellValues.Cells )
				{
					// Only set the values upto the stopCellExclusive.
					// 
					if ( cell == stopCellExclusive )
						break;

					// Skip the cells that have been deleted. The behavior that was decided was
					// that we would undo much as possible and ignore deleted rows, even if they
					// were part of the operation.
					// 
					if ( IsCellDeleted( cell ) )
						continue;

					MultiCellOperationInfo.CellValueHolder chv = this.cellValues[ cell ];

					// Skip values that have conversion errors.
					// 
					if ( chv.Ignore || chv.HasConversionError )
						continue;

					// SSP 2/17/05 BR08864
					// Ignore cells associated with columns that have their IgnoreMultiCellOperation 
					// set to True.
					// 
					if ( cell.Column.IgnoreMultiCellOperationResolved )
						continue;

					// Set the cell's value. Pass in false for the fireInitializeRow parameter because 
					// we are firing InitializeRow in the UpdateRowHelper method below. The reason for
					// doing this is that we want to fire InitializeRow once per row.
					// 
					bool success = chv.HasSameValue || this.SetCellValueHelper( context, cell, chv.Value, out errorInfo );
					if ( success )
					{
						// Keep track of cells that changed values.
						// 
						modifiedCells.Add( cell );

						// When performfing paste, redo or undo, activate the first cell, if the current 
						// active cell is not already part of the cells that are going to be modified.
						// If this causes a problem, take it out. However make sure that undo operation
						// spanning a single row leaves the row in a modified state.
						// 
						if ( null == lastUpdatedCell && activateFirstCell )
							cell.Activate( );

						// Update the rows as we go, depending on the UpdateMode settings.
						// 
						this.UpdateRowHelper( context, lastUpdatedCell, cell, updateMode, out errorInfo );
						lastUpdatedCell = cell;
					}
					
					if ( null != errorInfo )
					{
						if ( MultiCellOperationErrorInfo.ErrorAction.ClearCellAndContinue == errorInfo.Action )
						{
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//object defaultValue = cellValues.GetColumnDefaultValue( cell );
							object defaultValue = MultiCellOperationInfo.CellValuesCollection.GetColumnDefaultValue( cell );

							// Only attempt to set the default value if it's different from the value that
							// we attempted to set above.
							// 
							if ( defaultValue != chv.Value )
							{
								MultiCellOperationErrorInfo tmpErrorInfo;
								this.SetCellValueHelper( context, cell, defaultValue, out tmpErrorInfo );

								if ( null != tmpErrorInfo )
									errorInfo = tmpErrorInfo;
							}
						}
						
						if ( MultiCellOperationErrorInfo.ErrorAction.Revert == errorInfo.Action 
							|| MultiCellOperationErrorInfo.ErrorAction.Stop == errorInfo.Action )
							break;
					}
				}

				if ( modifiedCells.Count <= 0 )
					return false;

				// Update the rows as we go, depending on the UpdateMode settings.
				// 
				if ( null != lastUpdatedCell 
					&& ( null == errorInfo || MultiCellOperationErrorInfo.ErrorAction.Revert != errorInfo.Action ) )
					this.UpdateRowHelper( context, lastUpdatedCell, null, updateMode, out errorInfo );

				// If there was an error and we fired Error event and the Action is set to Revert, then
				// revert the modified cells to their original values.
				// 
				if ( null != errorInfo && MultiCellOperationErrorInfo.ErrorAction.Revert == errorInfo.Action )
				{
					foreach ( UltraGridCell cell in modifiedCells )
					{
						MultiCellOperationInfo.CellValueHolder cvh = undoAction.cellValues[ cell ];
						cell.SetValueInternal( cvh.Value, true, true );
					}

					return false;
				}

				// Also when undo'ing this action, only undo cells that were actually modified.
				// In other words, if we skipped read-only cells, or cells that we were unable
				// to convert the value for, etc..., then don't attempt to undo such cells. For
				// this set Ignore to true on the undo action's cell values.
				// Set Ignore to true on all cells.
				// 
				foreach ( UltraGridCell cell in undoAction.cellValues.Cells )
					undoAction.cellValues[ cell ].Ignore = true;

				// Set it to false on all the modified cells, since those are the ones we want
				// to restore when undoing the operation.
				// 
				foreach ( UltraGridCell cell in modifiedCells )
					undoAction.cellValues[ cell ].Ignore = false;

				// When performfing paste, redo or undo, select the modified cells and scroll
				// them into view.
				// 
				if ( MultiCellOperation.Paste == context.operationBeingPerformed 
					|| MultiCellOperation.Redo == context.operationBeingPerformed 
					|| MultiCellOperation.Undo == context.operationBeingPerformed )
				{
					// Select the cells whose values were modified. This follows MS Excel behavior.
					// Also If a single cell is being undone then activate it. This is for better
					// usability. This way the user can enter the edit mode or manipulate the
					// value of the cell that was just undone, without having to activate it 
					// first.
					// 
					this.ActionHistory.MultiCellOperationInfo.ScrollAndSelect( modifiedCells, true, 1 == modifiedCells.Count );
				}

				return true;
			}
		}

		#endregion // MultiCellValuesAction Class

	}

	#endregion // ActionFactory Class

}
