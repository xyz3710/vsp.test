#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

// SSP 12/15/04
// Commented out EditBoxBase class as it's not being used anymore.
//
//using System;
//using System.Drawing;
//using System.Collections;
//using System.ComponentModel;
//using System.ComponentModel.Design;
//using System.Windows.Forms;
//using System.Diagnostics;
//using Infragistics.Shared;
//using Infragistics.Win;
//using System.Security.Permissions;
//
//namespace Infragistics.Win.UltraWinGrid
//{
//	/// <summary>
//	///	Used to edit text ineither the gridControl or the combo.
//	/// </summary>
//	[ ToolboxItem(false) ]
//	

//	abstract public class EditBoxBase : TextBox
//	{
//		// JJD 3/8/02
//		// Change member name from 'grid' to 'gridControl' since it is not CLSCompliant
//		// to have a member and a property whose name differs by case only
//		//
//		/// <summary></summary>
//		protected UltraGridBase	gridControl = null;
//
//
//		/// <summary></summary>
//		protected const int VK_F4			=	0x73;
//		/// <summary></summary>
//		protected const int VK_TAB			=	0x09;
//		/// <summary></summary>
//		protected const int VK_RETURN		=	0x0D;
//		/// <summary></summary>
//		protected const int VK_PRIOR		=   0x21;
//		/// <summary></summary>
//		protected const int VK_NEXT			=   0x22;
//		/// <summary></summary>
//		protected const int VK_END			=   0x23;
//		/// <summary></summary>
//		protected const int VK_HOME			=   0x24;
//		/// <summary></summary>
//		protected const int VK_UP			=   0x26;
//		/// <summary></summary>
//		protected const int VK_DOWN			=   0x28;
//		/// <summary></summary>
//		protected const int VK_ESCAPE		=	0x01B;
//		/// <summary></summary>
//		protected const int WM_KEYDOWN	=	0x0100;
//		/// <summary></summary>
//		protected const int WM_KEYUP      =	0x0101;
//		/// <summary></summary>
//		protected const int WM_CHAR       =	0x0102;
//		/// <summary></summary>
//		protected const int WM_LBUTTONDOWN		= 0x0201;
//		/// <summary></summary>
//		protected const int WM_RBUTTONDOWN		= 0x0204;
//		/// <summary></summary>
//		protected const int VK_RIGHT		= 0x27;
//		/// <summary></summary>
//		protected const	int	VK_LEFT			= 0x25;
//
//		private char pressedChar = (char)0;
//		private bool inAutoEditMode = false;
//		private bool autoSizeEdit = false;
//
//		// SSP 6/26/02
//		//
//		private char lastProcessedDialogKeyChar = '\0';
//
//		// SSP 4/29/02
//		// EM Embeddable editors.
//		// inOnTextChanged is not used anywhere anymore, so commented it out. 
//		// It was being used for auto edit feature which the embeddable editors
//		// take care of now.
//		//
//		/*// JJD 11/30/01 - UWG802
//		// Added anti-recursion flag
//		//
//		private bool inOnTextChanged = false;*/
//		
//		/// <summary>
//		/// Constructor.
//		/// </summary>
//		internal EditBoxBase( UltraGridBase gridControl )
//		{
//			if ( gridControl == null )
//				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_123"), Shared.SR.GetString("LE_ArgumentNullException_326") );
//
//			this.gridControl			= gridControl;
//			this.BorderStyle	= System.Windows.Forms.BorderStyle.None;
//			this.HideSelection	= true;
//			this.AutoSize		= false;	
//		}
//
//		/// <summary>
//		/// Called when the control receives focus
//		/// </summary>
//		protected override void OnGotFocus( EventArgs e )
//		{
//			base.OnGotFocus ( e );
//
//			this.SelectionStart		= 0;
//			this.SelectionLength	= this.Text.Length; 
//		}
//
//		// SSP 4/28/03 UWG1972
//		// UltraCombo references ComboBox whose base class EditBoxBase overrides the WndProc.
//		// This causes security problems. So added ComboTextBoxEx class and moved the WndProc
//		// in here. This will just delegate the call to a newly added helper method in
//		// the base class.
//		//
//		/*
//		/// <summary>
//		/// The control's window procedure
//		/// </summary>
//		// AS 1/8/03 - fxcop
//		// Added attribute to prevent FxCop violations
//		
//		
//		protected override void WndProc(ref System.Windows.Forms.Message msg)
//		*/
//		internal void WndProcHelper( ref System.Windows.Forms.Message msg )
//		{
//			// if the left button is pressed and we don't have focus
//			// just set the focus and eat the message
//			//
//			if ( msg.Msg == WM_LBUTTONDOWN &&
//				!this.Focused )
//			{
//				// AS - 1/14/02
//				// We need to assert putting focus on the control in order for this to work
//				// when the assembly is local but the executable is running under internet
//				// type restricted security access. However, to prevent a security hole, we
//				// will only do so on controls that have our public key token.
//				//
//				//this.Focus();
//				try
//				{
//					if ( DisposableObject.HasSamePublicKey( this.GetType() ))
//					{
//						System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );
//
//						perm.Assert();
//					}
//
//					this.Focus();
//				}
//				catch {}
//
//				return;
//			}
//
//			// if the right button is pressed check is the mouse 
//			// buttons are swapped. If so and we don't have focus
//			// just set the focus and eat the message (as above)
//			//
//			if ( msg.Msg == WM_RBUTTONDOWN &&
//				!this.Focused &&
//				SystemInformation.MouseButtonsSwapped )
//			{
//				// AS - 1/14/02
//				// We need to assert putting focus on the control in order for this to work
//				// when the assembly is local but the executable is running under internet
//				// type restricted security access. However, to prevent a security hole, we
//				// will only do so on controls that have our public key token.
//				//
//				//this.Focus();
//				try
//				{
//					if ( DisposableObject.HasSamePublicKey( this.GetType() ))
//					{
//						System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );
//
//						perm.Assert();
//					}
//
//					this.Focus();
//				}
//				catch {}
//
//				return;
//			}
//
//			base.WndProc( ref msg );
//		}
//
//		/// <summary>
//		/// Used to specify whether the textbox will automatically expand.
//		/// </summary>
//		public bool AutoSizeEdit
//		{
//			get
//			{
//				return this.autoSizeEdit;
//			}
//
//			set
//			{
//				if ( value != this.autoSizeEdit )
//				{
//					this.autoSizeEdit = value;
//				}
//			}
//		}
//
//		internal bool InAutoEditMode
//		{
//			get
//			{
//				return this.inAutoEditMode;
//			}
//
//			set
//			{
//				if ( value != this.inAutoEditMode )
//					this.inAutoEditMode = value;
//			}
//		}
//		
//		// SSP 4/29/02
//		// EM Embeddable editors.
//		// Don't need to override OnTextChanged anymore since the auto editing
//		// will be done by the editors now.
//		//
//		/*
//		/// <summary>
//		/// Overloaded
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected override void OnTextChanged( EventArgs e )
//		{
//			//RobA 10/30/01 implemented autoedit
//			//
//
//			base.OnTextChanged( e );
//
//			// JJD 11/30/01 - UWG802
//			// Prevent recursion by checking flag
//			//
//			if ( this.inOnTextChanged )
//				return;
//
//			this.inOnTextChanged = true;
//		    
//			try
//			{
//				if ( null != this.gridControl )
//				{				
//					UltraGridCell cell = this.gridControl.DisplayLayout.ActiveCell;
//					
//					// JJD 1/28/03 - UWG1003
//					// Don't bother searching for text if this change is the
//					// result of a selection from the dropdown
//					//
//					if ( null != cell && 
//						 null != cell.Column && 
//						 cell.IsInEditMode &&
//						 !cell.Column.IsUpdatingTextFromOnSelectionChange )
//					{
//						string matchedText = null;
//						string origText = this.Text;
//
//						//Make sure we have a valuelist
//						//		
//						// RobA UWG841 12/5/01
//						//
//						if (  cell.Column.HasValueList ) 
//						{
//							//If not AutoEdit
//							//Call this to clear out the selected text if there is no match
//							//if there is a match update the textbox's text
//							//
//							if ( !cell.Column.AutoEdit )
//							{
//								//we don't want to select any part of the text
//								//
//								if ( cell.SelectTextInDropdown( origText, out matchedText ) )
//									this.SelectionStart = this.Text.Length;
//							}
//
//							//AutoEdit is true
//							//
//							else						
//							{												
//								//Set inAutoEditMode so that when we call SelectTextInDropdown
//								//it doesn't eventually update the edit control
//								//
//								this.inAutoEditMode = true;
//								
//								//If we can enter auto edit mode then
//								//try to find a matching text
//								//
//								if ( this.CanEnterAutoEditMode() &&							
//									cell.SelectTextInDropdown( origText, out matchedText ) )							
//								{
//									//if we found matching text, select that text in the 
//									//text box
//									//
//									this.AutoEditHelper( matchedText );					
//								}	
//								
//								//reset flag
//								//
//								this.inAutoEditMode = false;				
//							}
//						}
//				
//				
//						// RobA 11/26/01 UWG635
//						// implemented AutoSizeEdit
//						//
//						if ( this.AutoSizeEdit )// &&
//							//( cell.Column.StyleResolved == ColumnStyle.Edit ||
//							 // cell.Column.StyleResolved == ColumnStyle.EditButton )  )
//						{
//							this.PerformAutoSizeEdit();												
//						}
//
//						// Reset the borderstyle
//						//
//						else
//							this.BorderStyle = BorderStyle.None;
//					}				
//				}
//			}
//			finally
//			{
//				// JJD 11/30/01 - UWG802
//				// Reset anti-recursion flag
//				//
//				this.inOnTextChanged = false;
//			}
//	
//		}
//		*/
//
//		
//
//		// SSP 3/1/02 UWG168
//		// Overrode OnClick and OnDoubleClick to forward those
//		// messages to the gridControl.
//		//
//		/// <summary>
//		/// Overridden.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected override void OnClick( EventArgs e )
//		{
//			UltraGrid ultraGrid = this.gridControl as UltraGrid;
//
//			if ( null != ultraGrid )
//			{
//				ultraGrid.OnClickForwarded( e );
//			}
//
//			base.OnClick( e );
//		}
//
//		/// <summary>
//		/// Overridden. Raises the DoubleClick event.
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected override void OnDoubleClick( EventArgs e )
//		{
//			UltraGrid ultraGrid = this.gridControl as UltraGrid;
//
//			if ( null != ultraGrid )
//			{
//				ultraGrid.OnDoubleClickForwarded( e );
//			}
//
//			base.OnDoubleClick( e );
//		}
//
//
//		internal void PerformAutoSizeEdit()
//		{
//
//			string text = this.Text;
//
//			// Use the active Scroll regions as our bounds
//			//
//			int right = this.gridControl.ActiveColScrollRegion.ClippedExtent + this.gridControl.ActiveColScrollRegion.Left;
//			int bottom = this.gridControl.ActiveRowScrollRegion.ClippedExtent + this.gridControl.ActiveRowScrollRegion.Top;
//
//			Rectangle rect = new Rectangle( this.Location, this.Size );
//
//			// give the textbox a border
//			//
//			this.BorderStyle = BorderStyle.FixedSingle;
//
//			// AS 8/12/03 optimization - Use the new caching mechanism.
//			//
//			//Graphics gr = DrawUtility.CreateReferenceGraphics( this.gridControl );					
//			Graphics gr = DrawUtility.GetCachedGraphics( this.gridControl );					
//
//			// borderthickness is 1 and padding after the border 1
//			// which gives a total of 4, counting both borders
//			//
//			int padding = 4;
//
//			Size textSize = this.MeasureText( text );
//
//			// include a char in the textSize so that we don't horizontal
//			// scroll when we add a new char to the textbox
//			//
//            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
//			//textSize.Width += (gr.MeasureString( "W", this.Font ).ToSize()).Width;
//			textSize.Width += (DrawUtility.MeasureString( gr, "W", this.Font ).ToSize()).Width;
//			
//			// if the width of the text is greater than the textbox's width
//			// expand the textbox
//			//
//			if ( textSize.Width > rect.Width - padding )
//			{
//                rect.Width = textSize.Width + padding;												
//			}
//
//			// Make sure we don't expand past our bounds
//			//
//			if ( rect.Right > right )
//				rect.Width = right - rect.Left;
//
//
//			// if this is multiline, then check to see if the height
//			// of the text is greater than the height of the textbox
//			//
//			if ( this.Multiline )
//			{
//                //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
//                //textSize = gr.MeasureString( text, this.Font, rect.Width ).ToSize();
//                textSize = DrawUtility.MeasureString( gr, text, this.Font, rect.Width ).ToSize();
//				
//				// include a char in the textSize so that we don't scroll vertically
//				// when we add a new char to the textbox
//				//
//                //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
//				//textSize.Height += (gr.MeasureString( "W", this.Font ).ToSize()).Height + 2;
//				textSize.Height += (DrawUtility.MeasureString( gr, "W", this.Font ).ToSize()).Height + 2;
//				
//				if ( textSize.Height > rect.Height )
//				{
//					rect.Height = textSize.Height + padding;								
//				}
//
//				// Make sure we don't expand past our bounds
//				//
//				if ( rect.Bottom > bottom )
//					rect.Height = bottom - rect.Top;
//			}	
//		
//			// Set the textbox's size and location
//			//
//			this.Location = rect.Location;
//			this.Size = rect.Size;	
//
//			// AS 8/12/03 optimization - Use the new caching mechanism.
//			//
//			DrawUtility.ReleaseCachedGraphics(gr);
//		}
//
//		
//		internal Size MeasureText( string text )
//		{
//			return this.MeasureText( text, new Rectangle( this.gridControl.Left, this.gridControl.Top, this.gridControl.Width, this.gridControl.Height ) );
//		}
//
//		internal Size MeasureText( string text, Rectangle rect )
//		{
//			// calculate the size of the string
//			//
//			//Size textSize = gr.MeasureString( text, this.Font ).ToSize();
//
//			// AS 1/8/03 - fxcop
//			// Do not compare against string.empty - test the length instead
//			//if ( text == null || text == string.Empty)
//			if ( text == null || text.Length == 0)
//				return new Size( 0,0 );
//
//			// AS 8/12/03 optimization - Use the new caching mechanism.
//			//
//			//Graphics gr = DrawUtility.CreateReferenceGraphics( this.gridControl );						
//			Graphics gr = DrawUtility.GetCachedGraphics( this.gridControl );						
//			
//			CharacterRange[] characterRange = {new CharacterRange( 0, text.Length )};
//
//			// Set string format.
//			//
//			StringFormat stringFormat = new StringFormat();
//			stringFormat.SetMeasurableCharacterRanges(characterRange);
//
//			// measure the string
//			//
//            Region[] stringRegion = gr.MeasureCharacterRanges( text, this.Font, rect, stringFormat );
//
//			RectangleF stringRect = stringRegion[0].GetBounds(gr);
//
//			stringRegion[0].Dispose();
//
//			// AS 8/12/03 optimization - Use the new caching mechanism.
//			//
//			DrawUtility.ReleaseCachedGraphics(gr);
//
//			// return a size
//			//
//			return new Size( (int)stringRect.Width, (int)stringRect.Height );			
//		}
//
//		/// <summary>
//		/// Checks to make sure that the last key that was pressed is a valid 
//		/// key for entering edit mode.
//		/// </summary>
//		/// <returns></returns>
//		protected bool CanEnterAutoEditMode()
//		{
//			return ( (char)8	!= this.pressedChar && // backspace
//					 (char)127	!= this.pressedChar && // delete
//					 '\n'		!= this.pressedChar &&
//					 '\r'		!= this.pressedChar &&
//					 '\t'		!= this.pressedChar );
//		}
//
//		/// <summary>
//		/// Selects only the passed in matched text
//		/// </summary>
//		/// <param name="matchedText"></param>
//		protected void AutoEditHelper( string matchedText )
//		{			
//			int selStart = this.SelectionStart;
//
//			// only set the text of the text box when
//			if ( selStart == this.Text.Length )
//			{						
//				try
//				{
//					this.Text = matchedText;
//
//					//Select the rest of the found string
//					//
//					this.SelectionStart = selStart;
//					this.SelectionLength = this.Text.Length - selStart;
//				}
//				catch ( Exception exception )
//				{
//					Debug.Assert( false, "Exception need investigating: " + exception.Message );
//				}									
//			}		
//		}
//
//		/// <summary>
//		/// Called when Key is down
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected override void OnKeyDown( KeyEventArgs e )
//		{
//			this.gridControl.OnKeyDownForwarded( e );
//
//			//since the delete key does not get called in
//			//OnKeyPress we need to set pressedChar here
//			//
//			if ( e.KeyCode == Keys.Delete )
//				this.pressedChar = (char)127;
//		
//			base.OnKeyDown( e );
//		}
//		
//		/// <summary>
//		/// Call when key is pressed
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected override void OnKeyPress( KeyPressEventArgs e )
//		{
//			this.gridControl.OnKeyPressForwarded( e );
//			
//			//this.inKeyPress = true;
//			this.pressedChar = e.KeyChar;
//
//			base.OnKeyPress( e );		
//		}
//
//		/// <summary>
//		/// Called when key is up
//		/// </summary>
//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
//		protected override void OnKeyUp( KeyEventArgs e )
//		{
//			this.gridControl.OnKeyUpForwarded( e );
//
//			//this.inKeyPress = false;
//			this.pressedChar = (char)0;
//
//			base.OnKeyUp( e );
//		}
//
//		internal abstract bool IsDroppedDown { get; }
//
//		// SSP 6/26/02
//		//
//#if DEBUG
//		/// <summary>
//		/// Returns true if the specified key is used by a Form as a navigational key
//		/// </summary>
//		/// <param name="key">The key to evaluate</param>
//#endif
//		private bool IsNavigationalKey( int key )
//		{
//			if ( key == VK_TAB ||				
//				key == VK_RIGHT ||
//				key == VK_LEFT || 
//				key == VK_UP || 
//				key == VK_DOWN )
//				return true;
//
//			// SSP 8/22/02 UWG1583
//			// If the combo is dropped down, then enter and escape should not
//			// be processed as dialog keys.
//			//
//			if ( this.gridControl is UltraCombo )
//			{
//				if ( VK_RETURN == key || VK_ESCAPE == key )
//				{
//					if ( ((UltraCombo)this.gridControl).IsDroppedDown )
//						return true;
//				}
//			}
//
//			return false;
//		}
//
//#if DEBUG
//		/// <summary>
//		/// Returns the Keys enumeration constant corresponding to the specified IntPtr
//		/// </summary>
//		/// <param name="wParam">The IntPtr from which to obatin the key data, passed as the wParam from the ProcessKeyMessage method</param>
//		/// <param name="keyData">(out) The corresponding key data</param>
//		/// <returns>A boolean indicating whether the 'val' parameter was recognized</returns>
//		/// <remarks>If this method returns false, we do not support the key. In that case, the overridden ProcessKeyMessage method should return the result of the base class implementation.</remarks>
//		/// <remarks>This method checks the virtual UsesInputKey method as the ultimate decision maker; if that method returns false, the key is not supported</remarks>
//#endif
//		private bool GetKeyDataFromWParam( int wParam, out Keys keyData )
//		{
//			bool ret = true;
//
//			keyData = 0;
//			switch ( wParam )
//			{
//				case VK_HOME:
//					keyData = Keys.Home;
//					break;
//				case VK_END:
//					keyData = Keys.End;
//					break;
//				case VK_UP:
//					keyData = Keys.Up;
//					break;
//				case VK_DOWN:
//					keyData = Keys.Down;
//					break;
//				case VK_ESCAPE:
//					keyData = Keys.Escape;
//					break;
//				case VK_F4:
//					keyData = Keys.F4;
//					break;
//				case VK_TAB:
//					keyData = Keys.Tab;
//					break;
//				case VK_RETURN:
//					keyData = Keys.Return;
//					break;
//				default:
//					ret = false;
//					break;
//			}
//
//			return ret;
//		}
//
//		// SSP 4/28/03 UWG1972
//		// UltraCombo references ComboBox whose base class EditBoxBase overrides the WndProc.
//		// This causes security problems. So added ComboTextBoxEx class and moved the WndProc
//		// in here. This will just delegate the call to a newly added helper method in
//		// the base class.
//		//
//		/*
//		/// <summary>
//		/// Overriden PreProcessMessage to process the tab and escape key
//		/// </summary>
//		/// <param name="msg"></param>
//		/// <returns></returns>
//		// AS 1/8/03 - fxcop
//		// Added attribute to prevent FxCop violations
//		
//		
//		public override bool PreProcessMessage( ref Message msg )
//		*/
//		internal bool PreProcessMessageHelper( ref Message msg )
//		{
//			// SSP 4/12/02 UWG1069, UWG1081
//			// If the Alt is down (and only alt, not with any other key combination),
//			// then return.
//			//
//			if ( Keys.Alt == Control.ModifierKeys )
//				return base.PreProcessMessage( ref msg );
//
//			// SSP 6/26/02
//			// Added below block of code
//			//
//			// -------------------------------------------------------------------------
//			Keys keyDataArg;
//			
//			// Only do this specialy processing for keys that we process below. For
//			// other keys, we are calling the base class' implementation anyways.
//			//
//			if ( ( msg.Msg == WM_KEYDOWN || msg.Msg == WM_CHAR || msg.Msg == WM_KEYUP ) &&
//				this.GetKeyDataFromWParam( (int)msg.WParam, out keyDataArg ) )
//			{
//				bool keyPreviewProcessed = false;
//				
//				//	WM_KEYDOWN
//				if ( msg.Msg == WM_KEYDOWN )
//				{
//					//	Clear this before we begin our processing
//					this.lastProcessedDialogKeyChar = '\0';
//
//					bool fireKeyDialog = true;
//
//					if ( null != this.gridControl && this.gridControl is UltraGrid )
//					{
//						UltraGrid grid = (UltraGrid)this.gridControl;
//
//						// If we have a key action mapping, then don't process the dialog
//						// key. Just do the key preview.
//						//
//						if ( grid.KeyActionMappings.IsKeyMapped( keyDataArg | Control.ModifierKeys, (long)grid.CurrentState ) )
//							fireKeyDialog = false;
//					}
//
//					// SSP 8/22/02 UWG1593
//					// We need to also check for the grid control being UltraCombo.
//					//
//					if ( null != this.gridControl && this.gridControl is UltraCombo )
//					{
//						UltraCombo combo = (UltraCombo)this.gridControl;
//
//						if ( combo.KeyActionMappings.IsKeyMapped( keyDataArg | Control.ModifierKeys, (long)combo.CurrentState ) )
//							fireKeyDialog = false;
//					}
//
//					// SSP 10/16/03 UWG2690
//					// Only process if it's Tab or Shift+Tab. Don't process other key combinations
//					// involving Tab like Ctrl+Tab or Alt+Tab etc.
//					// 
//					// --------------------------------------------------------------------
//					if ( fireKeyDialog && Keys.Tab == keyDataArg &&
//						 ( Keys.Control & Control.ModifierKeys ) == Keys.Control )
//						return base.PreProcessMessage( ref msg );
//					// --------------------------------------------------------------------
//
//					//	Note that the right & left arrow keys, and the TAB key
//					//	will be processed as dialog keys, which we don't want, so
//					//	if that is the case, we will not return or suppress events
//					if(	fireKeyDialog &&
//						! this.IsNavigationalKey( (int)msg.WParam ) &&
//						this.ProcessDialogKey( keyDataArg ) )
//					{
//						//	If this is the TAB key
//						//	Cache this key (yech), which will be checked in the WM_CHAR
//						//	handler. If the keyChar is the same, we will forego processing
//						//	(following the example of the TextBox control
//						this.lastProcessedDialogKeyChar = (char)(msg.WParam);
//						return true;
//					}
//
//					//	Call ProcessKeyPreview, which forwards the keystroke to the
//					//	form on which we are sited. If the key is marked as handled in
//					//	the form's KeyDown event, we do not call OnKeyDown
//					keyPreviewProcessed = this.ProcessKeyPreview( ref msg );
//
//					// If the key preview mechanism handled the key, then return true.
//					//
//					if ( keyPreviewProcessed )
//						return true;
//
//					// Otherwise fall through and let the the code way below process.
//					//
//				}
//
//				//	WM_CHAR
//				if ( msg.Msg == WM_CHAR )
//				{
//					//	If the last WM_KEYDOWN was used by the form as a dialog key,
//					//	we don't want to call ProcessKeyPreview or OnKeyPress
//					if ( this.lastProcessedDialogKeyChar == (char)msg.WParam  )
//					{
//						this.lastProcessedDialogKeyChar = '\0';
//						return true;
//					}
//
//					//	Clear it out
//					this.lastProcessedDialogKeyChar = '\0';
//
//					//	Call ProcessKeyPreview, which forwards the keystroke to the
//					//	form on which we are sited. If the key is marked as handled in
//					//	the form's KeyPress event, we do not call OnKeyPress
//					keyPreviewProcessed = this.ProcessKeyPreview( ref msg );
//
//					// If the key preview mechanism handled the key, then return true.
//					//
//					if ( keyPreviewProcessed )
//						return true;
//
//					// Otherwise fall through and let the the code way below process.
//					//
//				}
//
//				//	WM_KEYUP
//				if ( msg.Msg == WM_KEYUP )
//				{
//					//	Clear this before we begin our processing
//					this.lastProcessedDialogKeyChar = '\0';
//
//					//	Call ProcessKeyPreview, which forwards the keystroke to the
//					//	form on which we are sited. If the key is marked as handled in
//					//	the form's KeyUp event, we do not call OnKeyUp
//					keyPreviewProcessed = this.ProcessKeyPreview( ref msg );
//
//					// If the key preview mechanism handled the key, then return true.
//					//
//					if ( keyPreviewProcessed )
//						return true;
//
//					// Otherwise fall through and let the the code way below process.
//					//
//				}
//			}
//			// -------------------------------------------------------------------------
//
//			
//			if ( msg.Msg == WM_KEYDOWN)
//			{
//				switch ((int)msg.WParam)
//				{
//					case VK_HOME:
//					case VK_END:
//					{
//						// JJD 10/02/01
//						// If we are not dropped down then return
//						// false to let the edit box process the message
//						// normally
//						//
//						if ( !this.IsDroppedDown )
//							// SSP 4/12/02 UWG1069, UWG1081
//							// Instead of just returning false, call
//							// the base class' implementation and return
//							// appropriate value.
//							//
//							//return false;
//							return base.PreProcessMessage( ref msg );
//
//						// Otherwise, break to pass it on to the gridControl
//						//
//						break;
//					}
//
//					//RobA UWG418 if this text is multiline we want to be able to use
//					//the up and down keys to navigate 
//					//
//					case VK_UP:
//					case VK_DOWN:
//					{
//						if ( this.Multiline )
//							// SSP 4/12/02 UWG1069, UWG1081
//							// Instead of just returning false, call
//							// the base class' implementation and return
//							// appropriate value.
//							//
//							//return false;
//							return base.PreProcessMessage( ref msg );
//
//						break;
//					}
//
//					// we don't want the textbox to do anything with these keys but we
//					// do want to use the keys to navigvate thru the dropdown list 
//					//
//					case VK_ESCAPE:
//					case VK_F4:	
//						break;
//
//					// JJD 10/02/01 - UWG411
//					// Forward the tab key also
//					//
//					case VK_TAB:
//					{
//						// AS - 1/14/02
//						// See if we have uipermissions. If so, forward the message, otherwise let it go through.
//						//
//						try
//						{
//							System.Security.Permissions.UIPermission perm = new System.Security.Permissions.UIPermission( System.Security.Permissions.UIPermissionWindow.AllWindows );
//
//							perm.Demand();
//						}
//						catch (System.Security.SecurityException)
//						{
//							// SSP 4/12/02 UWG1069, UWG1081
//							// Instead of just returning false, call
//							// the base class' implementation and return
//							// appropriate value.
//							//
//							//return false;
//							return base.PreProcessMessage( ref msg );
//						}
//						
//						break;
//					}
//					// SSP 8/22/02 UWG1593
//					// If the ultra combo is dropped down, then we want to forward
//					// the message to it rather than let the base class process
//					// it as a dialog key.
//					//
//					case VK_RETURN:
//					{
//						if ( !( this.gridControl is UltraCombo ) ||
//							!((UltraCombo)this.gridControl).IsDroppedDown )
//							return base.PreProcessMessage( ref msg );
//						break;
//					}
//					default:
//						// SSP 4/12/02 UWG1069, UWG1081
//						// Instead of just returning false, call
//						// the base class' implementation and return
//						// appropriate value.
//						//
//						//return false;
//						return base.PreProcessMessage( ref msg );
//				}
//			
//				// JJD 9/28/01
//				// We need to forward these messages to the gridControl
//				// so that they can be used to navigate through 
//				// a dropdown list
//				//
//				Keys keyData;
//
//				switch ((int)msg.WParam)
//				{
//					case VK_HOME:
//						keyData = Keys.Home;
//						break;
//					case VK_END:
//						keyData = Keys.End;
//						break;
//					case VK_UP:
//						keyData = Keys.Up;
//						break;
//					case VK_DOWN:
//						keyData = Keys.Down;
//						break;
//					case VK_ESCAPE:
//						keyData = Keys.Escape;
//						break;
//					case VK_F4:
//						keyData = Keys.F4;
//						break;
//					// JJD 10/02/01 - UWG411
//					// Forward the tab key also
//					//
//					case VK_TAB:
//						keyData = Keys.Tab;
//						break;
//					// SSP 8/22/02 UWG1593
//					//
//					case VK_RETURN:
//						keyData = Keys.Return;
//						break;
//					default:
//						Debug.Fail("Invalid key in EditBoxBase.PreProcessMessage.");
//						// SSP 4/12/02 UWG1069, UWG1081
//						// Instead of just returning false, call
//						// the base class' implementation and return
//						// appropriate value.
//						//
//						//return false;
//						return base.PreProcessMessage( ref msg );
//				}
//
//				// add in the modifier keys
//				//
//				keyData |= Control.ModifierKeys;
//
//				KeyEventArgs e = new KeyEventArgs( keyData );
//				
//				this.gridControl.OnKeyDownForwarded( e );
//
//				return true;
//			}
//
//			if ( msg.Msg == WM_KEYUP )
//			{
//				switch ((int)msg.WParam)
//				{
//						//RobA 10/30/01 return was beeping when pressed	
//					case VK_RETURN:
//					{
//						// SSP 11/29/01 UWG645
//						// Moved this from CellTextBox to here
//						// SSP 11/8/01 UWG645
//						// The control is only goging to beep if this is a single line
//						// or multiline and AcceptsReturn is false. So if that's not
//						// the case, then let the text box do it's processing of enter
//						// key ( like inserting a line ).
//						//
//						if ( !this.Multiline || !this.AcceptsReturn )
//						{
//
//							Keys keyData = Keys.Return;
//
//							// add in the modifier keys
//							//
//							keyData |= Control.ModifierKeys;
//
//							KeyEventArgs e = new KeyEventArgs( keyData );
//
//							this.gridControl.OnKeyUpForwarded( e );
//					
//							return true;		
//						}
//						break;
//					}
//				}
//			}
//
//			else if ( msg.Msg == WM_CHAR )
//			{
//				switch ((int)msg.WParam)
//				{
//					//RobA 10/30/01 return was beeping when pressed
//					case VK_RETURN:
//					{		
//						
//						// SSP 11/29/01 UWG645
//						// Moved this from CellTextBox to here
//						// SSP 11/8/01 UWG645
//						// The control is only goging to beep if this is a single line
//						// or multiline and AcceptsReturn is false. So if that's not
//						// the case, then let the text box do it's processing of enter
//						// key ( like inserting a line ).
//						//
//						if ( !this.Multiline || !this.AcceptsReturn )
//						{
//							char keyData = '\r';
//
//							KeyPressEventArgs e = new KeyPressEventArgs( keyData );
//
//							this.gridControl.OnKeyPressForwarded( e );
//					
//							return true;
//						}
//
//						break;						
//					}
//				}
//			}
//
//
//			// SSP 4/12/02 UWG1069, UWG1081
//			// Instead of just returning false, call
//			// the base class' implementation and return
//			// appropriate value.
//			//
//			//return false;
//			return base.PreProcessMessage( ref msg );
//		}
//
//		/// <summary>
//		/// Returns true if the key is a valid input key for this
//		/// control
//		/// </summary>
//		protected override bool IsInputKey( Keys keyData )
//		{
//			// SSP 6/26/02
//			//
//			/*
//			if ( (keyData & Keys.Up )		== Keys.Up )	return true;
//			if ( (keyData & Keys.Down )		== Keys.Down )	return true;
//			if ( (keyData & Keys.Left )		== Keys.Left )	return true;
//			if ( (keyData & Keys.Right )	== Keys.Right ) return true;
//			if ( (keyData & Keys.F4 )		== Keys.F4 )	return true;
//			*/
//			if ( keyData == Keys.Up )	 return true;
//			if ( keyData == Keys.Down )	 return true;
//			if ( keyData == Keys.Left || keyData == ( Keys.Left | Keys.Shift ) )  return true;
//			if ( keyData == Keys.Right || keyData == ( Keys.Left | Keys.Shift ) ) return true;
//			if ( keyData == Keys.F4 )	 return true;
//
//
//			//otherwise let the base class handle the key
//			return base.IsInputKey( keyData );
//		}
//		
//	}
//}
