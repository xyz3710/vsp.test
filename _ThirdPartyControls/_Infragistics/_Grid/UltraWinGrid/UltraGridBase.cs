#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Runtime.InteropServices;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Windows.Forms.Design;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Drawing.Printing;
	using Infragistics.Win.UltraWinMaskedEdit;

	using Infragistics.Win.Layout;

	// SSP 5/28/03 UWG2270
	//
	using System.ComponentModel.Design.Serialization;
	using Infragistics.Shared.Serialization;
    using System.Reflection;
	using System.Collections.Generic;
    using Infragistics.Win.CalcEngine;
	using Infragistics.Win.UltraWinGrid.Design;
	

	/// <summary>
	/// Returns the UltraGrid control associated with an UltraGridLayout object. This property 
	/// is read-only at run-time. This property is not available at design-time.
	/// </summary>
	/// <remarks><para>This property returns a reference to an UltraGrid object 
	/// that can be used to set properties of, and invoke methods on, the UltraGrid 
	/// control. You can use this reference to access any of the control's properties 
	/// or methods.</para>
	/// <para>This property is used to determine which UltraGrid 
	/// control is associated with a UltraGridLayout object.</para>
	/// <para>This property returns Nothing for UltraGridLayout objects not 
	/// associated with an UltraGrid control.</para>
	/// </remarks>    
	//[ UltraPropertyPage( "Groups And Columns", "Infragistics.Win.UltraWinGrid.Design.UltraGridGroupColumnsPropControl, " + AssemblyRef.Design)]//typeof(UltraGridGroupColumnsPropControl) )]
	[ LocalizedUltraPropertyPage( "PropertyPageCaptionGroupsAndColumns", "Infragistics.Win.UltraWinGrid.Design.UltraGridGroupColumnsPropControl, " + AssemblyRef.Design)]//typeof(UltraGridGroupColumnsPropControl) )]
	// SSP 5/28/03 UWG2270
	// Added DesignerSerializer attribute. This is to fix this problem with the grid on a base
	// form and you have a derived form and properties are not retained in the derived form.
	//
    // MRS 5/17/07 - BR22994 (fixed copied from BR18054)
    //[ DesignerSerializer( typeof( UltraCodeDomSerializer ), typeof( CodeDomSerializer ) ) ]
    //
    [DesignerSerializer(typeof(UltraGridBase.GridControlCodeDomSerializer), typeof(CodeDomSerializer))]
	public abstract class UltraGridBase : Infragistics.Win.UltraControlBase,
		Infragistics.Win.IUIElementTextProvider,
		ISupportInitialize,
		// SSP 2/11/03 - Row Layout Functionality
		// Added IGridDesignInfo interface so the designers in design assembly can access
		// internal properties from the grid via this interface without having to expose them 
		// to the user.
		//
		Infragistics.Win.UltraWinGrid.Design.IGridDesignInfo,

		// SSP 5/28/03 UWG2270
		// Added DesignerSerializer attribute. This is to fix this problem with the grid on a base
		// form and you have a derived form and properties are not retained in the derived form.
		//
		ICodeDomSerializable,

		// MRS 2/19/04
		// Added ISupportPresets so the grid can support Presets
		ISupportPresets

		// SSP 6/29/04 - UltraCalc
		// Implemented IUltraCalcParticipant on the grid as required by the formula support.
		//
		, Infragistics.Win.CalcEngine.IUltraCalcParticipant
	{
		private UltraGridDisplayLayout    layout = null;
		private LayoutsCollection         layouts = null;
		private bool					  firstDraw = true;
		private bool					  initializing = false;

		// JJD 12/03/01 - UWG813
		// Maintain flag so we know when to fire initialize layout
		//
		private bool					  initializeLayoutFired = false;

		/// <summary>
		/// Protected flag that is true during a drawing operation
		/// </summary>
		// AS 1/8/03 - fxcop
		// Cannot have 2 public/protected members that differ only by case
		//protected bool						drawing = false;
		protected bool						drawingValue = false;

		private object                      dataSource = null;
		private string                      dataMember = string.Empty;
		private BindingManagerBase          bindingManager = null;

		// SSP 10/30/01 UWG395
		// Added anti-recursion flag
		//
		internal bool						inSet_ListManager = false;


		private ColScrollRegion             activeColScrollRegion = null;
		private RowScrollRegion             activeRowScrollRegion = null;
		
		// JJD 3/8/02
		// Change member name from 'activeRow' to 'currentActiveRow' since it is not CLSCompliant
		// to have a member and a property whose name differs by case only
		//
		/// <summary>
		/// The current active row
		/// </summary>
		protected Infragistics.Win.UltraWinGrid.UltraGridRow  currentActiveRow = null;

		// SSP 10/8/01
		// Now we are doing delayed active row change. When deleting
		// multiple rows for example, we don't want to be
		// setting the active row multiple times causing the grid
		// to be repainted everytime
		//
		private Infragistics.Win.UltraWinGrid.UltraGridRow    tempActiveRow = null;
		private System.Windows.Forms.Timer			 paintTimer    = null;
		// SSP 11/17/04 UWG2234 
		// Changed the behavior so that when a column is sorted we maintain the scroll position
		// instead of keeping the first row the first row. Added tempActiveRow_ScrollIntoView 
		// variable that indicates whether we should scroll the row into view when making the 
		// the temp active row the active row.
		//
		internal bool tempActiveRow_ScrollIntoView = false;

		
		private System.Windows.Forms.ImageList imageList = null; 
		
		private SubObjectPropChangeEventHandler subObjectPropChangeHandler = null;

		// SSP 1/23/02
		// Added in_getActiveRow anti-recursion flag
		//
		private bool in_getActiveRow					 = false;

		// SSP 1/6/03 UWG1902
		// Added anti-recursion flag since the get of DisplayLayout.Bands property now
		// calls this method.
		//
		private bool in_VerifyDataSourceAttached = false;

		// SSP 3/27/03 - Row Layout Functionality
		// We need to clear the loaded bands collection in the BeginInit. This flag is used to
		// prevent us from clearing it if the grid is locaed on a base form and the changed
		// are made in the derived form.
		// 
		private bool endInitDuringDesignModeRecieved = true;

		// SSP 5/28/03 UWG2270
		// Added DesignerSerializer attribute. This is to fix this problem with the grid on a base
		// form and you have a derived form and properties are not retained in the derived form.
		//
		private bool isSerializing = false;

		// SSP 6/6/03 UWG2232
		//
		private bool rowSynchronizationSuspended = false;

		// SSP 6/17/04 UWG3207
		// Added a way to suspend dirtying of summaries when we receive ItemChanged
		// notification.
		//
		private bool summaryUpdatesSuspended = false;

		// SSP 6/29/04 - UltraCalc
		// Added calcManager member var.
		//
		private Infragistics.Win.CalcEngine.IUltraCalcManager calcManager = null;
		private Infragistics.Win.CalcEngine.IUltraCalcManager lastCalcManagerGridRegisteredTo = null;

		// SSP 7/30/04 UWG3488
		// Added IGridDesignInfo.DesignMode property to force the grid to bahave
		// as if it were in design mode. This is used by the ultragrid designer dialogs
		// to show two dummy rows in the preview grid.
		//
		private bool forceDesignMode = false;

		// SSP 10/11/06 - NAS 6.3
		// Added SetInitialValue on the UltraCombo.
		// 
		internal bool dontLoadDataSource = false;

		// SSP 6/20/05 - NAS 5.3 Column Chooser Feature
		// 
		#region Column Chooser Feature Related

		internal class ColumnChooserCache
		{
			internal UltraGridColumn[] dragColumns = null;
			internal UltraGridColumnChooser draggingColumnChooser = null;
			internal bool isDraggingGroupByButton = false;
			internal bool mouseHasBeenInGridWhileDragging = false;

			// This is set when columns are successfully dropped over the group-by box (successfully
			// in the sense that the column was grouped by).
			// 
			internal DefaultableBoolean droppedOverGroupByBox = DefaultableBoolean.Default;

			// This is set when columns are successfully dropped over column headers (successfully
			// in the sense that the column was inserted at the drop location).
			// 
			internal DefaultableBoolean groupByButtonDroppedOverColumnHeaders = DefaultableBoolean.Default;
		}

		private System.Windows.Forms.Cursor hideColumnCursor = null;
		private ColumnChooserDialog cachedColumnChooserDialog = null;

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList registeredColumnChoosers = null;
		private List<UltraGridColumnChooser> registeredColumnChoosers = null;

		internal ColumnChooserCache cachedColumnChooserDragInfo = null;
		internal bool columnChooserHasBeenDispalyedAtLeastOnce = false;

		#endregion // Column Chooser Feature Related

		// SSP 8/1/03
		// Moved FilterRow event from UltraGrid to the UltraGridBase class.
		//
		private static readonly object EVENT_FILTERROW = new object( );

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved from UltraGrid
		private static readonly object EVENT_AFTERSORTCHANGE = new object();
		private static readonly object EVENT_BEFORESORTCHANGE = new object();
		private static readonly object EVENT_AFTERCOLPOSCHANGED = new object();
		private static readonly object EVENT_BEFORECOLPOSCHANGED = new object();

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved from UltraGrid
		private static readonly object EVENT_BEFOREROWFILTERCHANGED = new object( );
		private static readonly object EVENT_AFTERROWFILTERCHANGED = new object( );
		private static readonly object EVENT_BEFOREROWFILTERDROPDOWN = new object( );
		private static readonly object EVENT_BEFORECUSTOMROWFILTERDIALOG = new object();
		private static readonly object EVENT_BEFOREROWFILTERDROPDOWNPOPULATE = new object( );

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Added FilterCellValueChanged and InitializeRowsCollection events.
		//
		private static readonly object EVENT_FILTERCELLVALUECHANGED = new object( );
		private static readonly object EVENT_INITIALIZEROWSCOLLECTION = new object( );

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// 
		private static readonly object EVENT_BEFORECOLUMNCHOOSERDISPLAYED = new object( );
		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by TestAdvantage.
		// 
		private static readonly object EVENT_BEFOREBANDHIDDENCHANGED = new object( );
		private static readonly object EVENT_AFTERBANDHIDDENCHANGED = new object( );

        // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox - Moved here from the UltraGrid
        private static readonly object EVENT_BEFOREHEADERCHECKSTATECHANGED = new object();
        private static readonly object EVENT_AFTERHEADERCHECKSTATECHANGED = new object();

		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved from UltraGrid
		/// <summary>
		/// For internal infrastructure use only. 
		/// </summary>
		protected ValueList					filterDropDown = null;

        internal const ControlStyles DoubleBufferControlStyle = ControlStyles.OptimizedDoubleBuffer;

		/// <summary>
		/// UltraGridBase constructor. Initializes a new instance of UltraGridBase class.
		/// </summary>
		protected UltraGridBase()
		{
			this.layout     = new UltraGridDisplayLayout();
			this.layout.InitGrid( this );
           
			// Set the appropriate control styles
			//
			this.SetStyle( ControlStyles.AllPaintingInWmPaint, true );
			this.SetStyle( ControlStyles.ResizeRedraw, true );
			this.SetStyle( ControlStyles.Selectable, true );
			this.SetStyle( UltraGridBase.DoubleBufferControlStyle, false);
			this.SetStyle( ControlStyles.StandardClick, true );
			this.SetStyle( ControlStyles.StandardDoubleClick, true );

			// hook up the prop change notifications
			//
			this.layout.SubObjectPropChanged += new SubObjectPropChangeEventHandler( OnSubObjectPropChanged );
		}

		//RobA 9/25/01 UWG273
		#region Overriden Properties not supported


		/// <summary>
		/// BackColor property is not supported. Use DisplayLayout's
		/// <see cref="UltraGridLayout.Appearance"/> object instead to specify appearance
		/// related attributes.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>ForeColor</b> and <b>BackColor</b> control properties are not supported by
		/// the UltraGrid. Use DisplayLayout's
		/// <see cref="UltraGridLayout.Appearance"/> object instead to specify appearance
		/// related attributes.
		/// </p>
		/// <seealso cref="UltraGridBase.DisplayLayout"/>
		/// <seealso cref="UltraGridLayout.Appearance"/>
		/// <seealso cref="UltraGridLayout.Override"/>
		/// </remarks>
		[
		Browsable(false),
		EditorBrowsable( EditorBrowsableState.Never ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )
		]
		public override Color BackColor
		{
			get 
			{
				// AS - 11/26/01
				// Only overrided to hide so it doesn't hurt to
				// get/pass the info from the base class.
				//return Color.Empty;
				return base.BackColor;
			}
			set 
			{
				// AS - 11/26/01
				// Only overrided to hide so it doesn't hurt to
				// get/pass the info from the base class.
				//throw new NotSupportedException("BackColor property not supported");
				base.BackColor = value;
			}
		}

		/// <summary>
		/// ForeColor property is not supported. Use DisplayLayout's
		/// <see cref="UltraGridLayout.Appearance"/> object instead to specify appearance
		/// related attributes.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>ForeColor</b> and <b>BackColor</b> control properties are not supported by
		/// the UltraGrid. Use DisplayLayout's
		/// <see cref="UltraGridLayout.Appearance"/> object instead to specify appearance
		/// related attributes.
		/// </p>
		/// <seealso cref="UltraGridBase.DisplayLayout"/>
		/// <seealso cref="UltraGridLayout.Appearance"/>
		/// <seealso cref="UltraGridLayout.Override"/>
		/// </remarks>
		[
		Browsable(false),
		EditorBrowsable( EditorBrowsableState.Never ),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )
		]
		public override Color ForeColor
		{
			get 
			{
				// AS - 11/26/01
				// Only overrided to hide so it doesn't hurt to
				// get/pass the info from the base class.
				//return Color.Empty;
				return base.ForeColor;
			}
			set 
			{
				// AS - 11/26/01
				// Only overrided to hide so it doesn't hurt to
				// get/pass the info from the base class.
				//throw new NotSupportedException("ForeColor property not supported");
				base.ForeColor = value;
			}
		}

		/// <summary>
		/// Overrides the control's property to hide it so that the appearance can be used instead.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>BackgroundImage</b> control property are not supported by the UltraGrid. 
		/// Use DisplayLayout's <see cref="UltraGridLayout.Appearance"/> object instead to 
		/// specify appearance related attributes.
		/// </p>
		/// <seealso cref="UltraGridBase.DisplayLayout"/>
		/// <seealso cref="UltraGridLayout.Appearance"/>
		/// <seealso cref="UltraGridLayout.Override"/>
		/// </remarks>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Image BackgroundImage
		{ 
			get { return base.BackgroundImage; }
			set { base.BackgroundImage = value; }
		}

		#endregion
		internal bool Drawing
		{
			get
			{
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return this.drawing;
				return this.drawingValue;
			}
		}

		internal bool FirstDraw
		{
			get
			{
				return this.firstDraw;
			}
		}

		/// <summary>
		/// Resets the first draw flag to false
		/// </summary>
		protected void ResetFirstDrawFlag(){ this.firstDraw = false; }

		internal BindingManagerBase BindingManager
		{
			get
			{
				return this.bindingManager;
			}
		}
		
		internal int tmp = 0;

		// SSP 10/30/01 UWG595
		//
		private void BindingListChanged( object sender, System.ComponentModel.ListChangedEventArgs e )
		{
			// SSP 3/31/04
			// Check for binding manager being null.
			//
			if ( null == this.BindingManager )
				return;

			if ( UltraGridBand.binding_debug )
			{
				Debug.WriteLine( ++tmp + ": UltraGrid.OnListChanged: " );
				//Debug.WriteLine( "   Bands[0].BindingList == sender = " + ( sender ==  this.Layout.Bands[0].BindingList  ) );
				//Debug.WriteLine( "   Bands[1].BindingList == sender = " + ( sender ==  this.Layout.Bands[1].BindingList  ) );
				//Debug.WriteLine( "   Bands[2].BindingList == sender = " + ( sender ==  this.Layout.Bands[2].BindingList  ) );
				Debug.WriteLine( "   e.Type                   = " + Enum.GetName( typeof( ListChangedType ), e.ListChangedType ) );
				Debug.WriteLine( "   e.OldIndex               = " + e.OldIndex );
				Debug.WriteLine( "   e.NewIndex               = " + e.NewIndex );
				Debug.WriteLine( "   BindingManager.Position  = " + this.BindingManager.Position );			
				Debug.WriteLine( "   BindingManager.Count     = " + this.BindingManager.Count );			
				Debug.WriteLine("");
			}

			try
			{
				switch ( e.ListChangedType )
				{

					case ListChangedType.PropertyDescriptorAdded:
					case ListChangedType.PropertyDescriptorChanged:
					case ListChangedType.PropertyDescriptorDeleted:					
						// SSP 12/11/02 UWG1813
						// If a column is being deleted or added, then call ColumnAddedRemovedChangedHandler
						// which not only retains the unbound columns but is more efficient than reloading
						// the bands.
						//
						// SSP 9/4/03 UWG2629
						// Do this during runtime as well instead of just during the design time.
						//
						//if ( this.DesignMode && null != this.layout &&	
						if ( null != this.layout &&	
							this.layout.SortedBands.Count > 0 &&
							this.BindingManager == this.layout.SortedBands[0].BindingManager )
						{
							// SSP 12/5/03 UWG2775
							// If the band doesn't have any columns, then reload the whole binding list
							// by calling Set_ListManager method. Commented out the original code and 
							// added new code below.
							//
							// ----------------------------------------------------------------------------
							//this.layout.Bands[0].ColumnAddedRemovedChangedHandler( e );
							//break;
							ColumnsCollection cols = this.layout.SortedBands[0].Columns;
							int nonChapteredBoundColumns = cols.BoundColumnsCount - cols.ChapteredColumnsCount;
							if ( nonChapteredBoundColumns <= 0 )
								this.Set_ListManager( this.DataSource, this.DataMember );
							else
								break;
							// ----------------------------------------------------------------------------
						}
					
						// JJD 12/03/01 - UWG813
						// Reset flag so that IniitalizeLayout will get fired again
						//
						this.ResetInitializeLayoutFiredFlag();					
						
						goto case ListChangedType.Reset;

					case ListChangedType.Reset:
						// SSP 9/4/03 UWG2629
						// If the grid's binding manager is the same as the band's binding manager,
						// then let the band handle the Reset.
						//
						//this.Set_ListManager( this.DataSource, this.DataMember );
						if ( null == this.layout || this.layout.SortedBands.Count <= 0 ||
							this.BindingManager != this.layout.SortedBands[0].BindingManager )
						{
							this.Set_ListManager( this.DataSource, this.DataMember );
						}
					
						break;
				}
			}
			catch ( Exception exc )
			{
				Debug.WriteLine( "Exception thrown in UltraGridBase.BindingListChanged: " + exc.Message );
			}
		}


		/// <summary>
		/// Returns/Sets the active ColScrollRegion object.  This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use the <b>ActiveColScrollRegion</b> property to determine which ColScrollRegion
		/// object is currently active. If you assign a ColScrollRegion object to the 
		/// <b>ActiveColScrollRegion</b> property, it will become the active column scrolling region.</p>
		/// <p class="body">
		/// Only one column scrolling region at a time may be the active ColScrollRegion. The 
		/// active ColScrollRegion is the one that receives keyboard navigation
		/// focus. For example, if you use the left and right arrow keys to scroll columns,
		/// the columns in the column scrolling region specified by <b>ActiveColScrollRegion</b> are
		/// the ones that will move.</p>
		/// <seealso cref="UltraGridBase.ActiveRowScrollRegion"/>
		/// <seealso cref="UltraGridLayout.ColScrollRegions"/>
		/// <seealso cref="UltraGridLayout.RowScrollRegions"/>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public ColScrollRegion ActiveColScrollRegion
		{
			get
			{
				// SSP 5/29/02 UWG1145
				// In addition to checking for null, also check for col scroll region being
				// disposed.
				//
				//if ( null == this.activeColScrollRegion )
				if ( null == this.activeColScrollRegion || this.activeColScrollRegion.Disposed )
				{
					// SSP 10/9/03
					// Also check if we have the layout and colscrollregions property returns a non-null value.
					//
					if ( null != this.DisplayLayout && null != this.DisplayLayout.ColScrollRegions )
						this.activeColScrollRegion = (ColScrollRegion)(this.DisplayLayout.ColScrollRegions.FirstVisibleRegion);
					else 
						this.activeColScrollRegion = null;
				}
                
				return this.activeColScrollRegion;
			}
			set
			{
				// JJD 12/19/01
				// Only do something if the value has changed
				//
				if ( this.activeColScrollRegion != value )
				{
					if ( value.Layout != this.layout )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_293") );

					this.activeColScrollRegion = value;
	                
					// JJD 10/25/01
					// Invalidate the active row if the control contains focus
					//
					if ( this.currentActiveRow != null  &&
						this.ContainsFocus )
					{
						// SSP 8/20/02
						// Check for active row being not null because in the get of the
						// active row we make sure that the active row is a valid row and
						// if it isn't, then we return null. So this.ActiveRow may return 
						// null.
						//
						if ( null != this.ActiveRow )
							this.ActiveRow.InvalidateItemAllRegions();
					}
				}
			}
		}

		// SSP 10/22/03 UWG2442
		// Added InternalResetActiveColScrollRegion and InternalResetActiveRowScrollRegion
		// methods to set the member variables to null.
		//
		internal void InternalResetActiveColScrollRegion( )
		{
			this.activeColScrollRegion = null;
		}

		internal void InternalResetActiveRowScrollRegion( )
		{
			this.activeRowScrollRegion = null;
		}

		/// <summary>
		/// Internal property for getting the tooltip tool
		/// </summary>
		internal protected virtual Infragistics.Win.ToolTip TooltipTool
		{
			get
			{
				return null;
			}
		}

		internal int InternalScrollControl( int xAmount, 
			int yAmount, 
			Rectangle scrollRect,
			Rectangle clipRect, 
			ref Rectangle updateRect, 
			bool erase,
			bool invalidate,
			bool scrollChildren,
			short smoothScrollAmount )
		{

			// JJD 11/02/01
			// Call the protected ScrollControl method
			return ScrollControl( xAmount,
				yAmount,
				scrollRect,
				clipRect,
				ref updateRect,
				erase,
				invalidate,
				scrollChildren,
				smoothScrollAmount );
		}

 
		/// <summary>
		/// Returns or sets the active RowScrollRegion object. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use the <b>ActiveRowScrollRegion</b> property to determine which RowScrollRegion
		/// object is currently active. If you assign an RowScrollRegion object to the 
		/// <b>ActiveRowScrollRegion</b> property, it will become the active row scrolling region.</p>
		/// <p class="body">Only one row scrolling region at a time may be the active RowScrollRegion. The 
		/// active RowScrollRegion is the one that contains the active row (as specified by the 
		/// <b>ActiveRow</b> property). It is also the row scroll region that receives keyboard navigation
		/// focus. For example, if you use the up and down arrow keys to scroll rows,
		/// the rows in the row scrolling region specified by <b>ActiveRowScrollRegion</b> are
		/// the ones that will move.</p>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public RowScrollRegion ActiveRowScrollRegion
		{
			get
			{
				// SSP 5/29/02 UWG1145
				// In addition to checking for null, also check for row scroll region being
				// disposed.
				//
				//if ( null == this.activeRowScrollRegion )
				if ( null == this.activeRowScrollRegion || this.activeRowScrollRegion.Disposed )
				{
					// SSP 10/9/03
					// Also check if we have the layout and colscrollregions property returns a non-null value.
					//
					if ( null != this.DisplayLayout && null != this.DisplayLayout.RowScrollRegions )
						this.activeRowScrollRegion = (RowScrollRegion)(this.DisplayLayout.RowScrollRegions.FirstVisibleRegion);
					else
						this.activeRowScrollRegion = null;
				}
                
				return this.activeRowScrollRegion;
			}
			set
			{
				// JJD 12/19/01
				// Only do something if the value has changed
				//
				if ( this.activeRowScrollRegion != value )
				{
					if ( value.Layout != this.layout )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_293") );

					this.activeRowScrollRegion = value;
	                
					// JJD 10/25/01
					// Invalidate the active row if the control contains focus
					//
					if ( this.currentActiveRow != null  &&
						this.ContainsFocus )
					{								
						// SSP 8/20/02
						// Check for active row being not null because in the get of the
						// active row we make sure that the active row is a valid row and
						// if it isn't, then we return null. So this.ActiveRow may return 
						// null.
						//
						if ( null != this.ActiveRow )
							this.ActiveRow.InvalidateItemAllRegions();
					}
				}
			}
		}

		// SSP 9/6/02 UWG1643
		// Made this internal.
		//
		//private void PaintTimer_Tick( object source, EventArgs e )
		internal void PaintTimer_Tick( object source, EventArgs e )
		{
			// SSP 5/29/02 UWG1156
			// If we are in the process of being disposed of, then do not proceed
			// with below logic.
			//
			if ( null == this.DisplayLayout || this.DisplayLayout.Disposed )
				return;

			if ( null != this.paintTimer )
				this.paintTimer.Enabled = false;

			// SSP 12/17/03 UWG2796
			//  For ultragrid, use EnsureTempActiveRowAssigned method.
			//
			// --------------------------------------------------------------------------------
			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( this is UltraGrid )
			//{
			//    ((UltraGrid)this).EnsureTempActiveRowAssigned( );
			//    return;
			//}
			UltraGrid grid = this as UltraGrid;

			if ( grid != null )
			{
				grid.EnsureTempActiveRowAssigned();
				return;
			}
			// --------------------------------------------------------------------------------

			if ( null != this.TempActiveRow &&
				this.ActiveRow != this.TempActiveRow )
			{
				this.ActiveRow = this.TempActiveRow;
			}
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridRow TempActiveRow
		{
			get
			{
				return this.tempActiveRow;
			}
			set
			{
				// SSP 11/17/04 UWG2234 
				// Changed the behavior so that when a column is sorted we maintain the scroll position
				// instead of keeping the first row the first row. Added SetTempActiveRow method and 
				// moved the code from here into that method. We are passing in true for the 
				// scrollIntoView parameter in order to maintain the previous behavior which was to
				// scroll the row into view.
				//
				this.SetTempActiveRow( value, true );
			}
		}

		// SSP 11/17/04 UWG2234 
		// Changed the behavior so that when a column is sorted we maintain the scroll position
		// instead of keeping the first row the first row. Added SetTempActiveRow method and 
		// moved the code from the set of the TempActiveRow property into here.
		//
		internal void SetTempActiveRow( UltraGridRow tempActiveRow, bool scrollIntoView )
		{
			// SSP 6/6/05 BR04388
			// Set the tempActiveRow_ScrollIntoView only if a tempActiveRow was specified.
			// 
			//this.tempActiveRow_ScrollIntoView = scrollIntoView;
			this.tempActiveRow_ScrollIntoView = null != tempActiveRow && scrollIntoView;

			if ( this.tempActiveRow != tempActiveRow )
			{
				this.tempActiveRow = tempActiveRow;

				// SSP 6/19/03 UWG2232
				// Related to SuspendRowSynchronization and ResumeRowSynchronization methods.
				// If the row syncrhonization is suspended then don't enable the paint timer
				// as we are going to be getting a paint at the end anyways. Besides this
				// causes a problem.
				// 
				if ( this.RowSynchronizationSuspended )
					return;

				if ( null == this.paintTimer && 
					this.tempActiveRow != null )
				{
					// AS 12/10/02 UWG1884
					// Use the static method to create our internal timer to
					// workaround the 'Handle Not Initialized' exception generated
					// by Everett when setting the 'Enabled' property to false.
					//
					//this.paintTimer = new System.Windows.Forms.Timer( );
					this.paintTimer = Infragistics.Win.Utilities.CreateTimer();

					this.paintTimer.Tick += new EventHandler( this.PaintTimer_Tick );
				}

				if ( this.paintTimer != null )
				{
					this.paintTimer.Stop( );
						
					if ( this.tempActiveRow != null )
					{
						this.paintTimer.Interval = 1000;
						this.paintTimer.Start( );					
					}
				}
			}
		}

		// SSP 12/8/03 UWG2753
		// Added ActiveRowInvalidHelper method.
		//
		internal void ActiveRowInvalidHelper( )
		{
			UltraGridRow origTempActieRow = this.TempActiveRow;
			bool maintainTempActiveRow = null != origTempActieRow && origTempActieRow.IsStillValid;					
			if ( maintainTempActiveRow )
				this.tempActiveRow = null;

            // MBS 5/7/08 - BR32580
            // If we're going to be clearing the active row, we should also ensure that
            // this row is not being held onto by the selection manager (i.e. pivot items)
            if (this.currentActiveRow != null)
            {
                foreach (RowScrollRegion region in this.DisplayLayout.RowScrollRegions)
                {
                    region.VerifyPivotItem();
                }
            }

			this.ClearActiveRow( );

			if ( maintainTempActiveRow )
				this.tempActiveRow = origTempActieRow;
		}

		/// <summary>
		/// Returns or sets the active row. This property is not available at design-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">
		///	Use the <b>ActiveRow</b> property to determine which row is 
		///	currently active, or change which row is currently active. 
		///	If you assign an UltraGridRow object to the <b>ActiveRow</b> property, 
		///	the specified row will become active.</p>
		///	<p class="body">
		///	Only one row at a time may be the active row. The active row 
		///	is formatted using a special Appearance object, as specified by
		///	the <b>ActiveRowAppearance</b> property. The active row contains the
		///	active cell, which is the cell that will receive input focus when 
		///	the Grid goes into edit mode. You can determine which cell is the active
		///	cell using the <b>ActiveCell</b> property.</p>
		///	<p class="body">
		///	If no row is active, this property will return Nothing. To deactivate 
		///	the active row, set this property to Nothing.</p>
		///	<seealso cref="UltraGrid.ActiveCell"/>
		///	<seealso cref="UltraGrid.Selected"/>
		///	</remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public Infragistics.Win.UltraWinGrid.UltraGridRow ActiveRow
		{
			get
			{
				// SSP 2/24/05 BR02492
				// Moved the code from here to the new VerifyActiveRowValid so we can call
				// it from the set of this property as well.
				//
				this.VerifyActiveRowValid( );

				return this.currentActiveRow;
			}
			set
			{
				// SSP 2/24/05 BR02492
				// This reason for this change is that if the developer sets ActiveRow in code 
				// then at that point we need to sync the rows if they are marked dirty so 
				// SyncRows does not overwrite the active row at a later point. Added call 
				// to VerifyActiveRowValid.
				//
				this.VerifyActiveRowValid( );

				if ( this.currentActiveRow != value )
				{
					this.SetActiveRow( value, true );
				}
				else
				{
					// SSP 11/21/03 UWG2750
					// Reset the TempActiveRow to null. The reason for doing this is that if the
					// user sets the ActiveRow in the form's Load event and currentActiveRow and 
					// value happen to be the same, then when we receive the paint message, we
					// will end up setting the ActiveRow to TempActiveRow.
					//
					this.TempActiveRow = null;
				}
			}
		}

		// SSP 2/24/05 BR02492
		// Added VerifyActiveRowValid method. Code in there is moved from the get of the ActiveRow.
		//
        // MBS 5/7/08 - BR32576
        // Made internal
		//private void VerifyActiveRowValid( )
        internal void VerifyActiveRowValid()
		{
			// SSP 1/22/02
			// If the active row is not valid, then clear the active row
			// and active cell.
			//
			if ( null != this.currentActiveRow 
				&& !this.in_getActiveRow
				&& !this.currentActiveRow.IsStillValid )
			{
				// Set the anti-recursion flag
				//
				this.in_getActiveRow = true;
				try
				{
					UltraGrid grid = this as UltraGrid;

					// MD 8/3/07 - 7.3 Performance
					// Refactored - Prevent calling expensive getters multiple times
					//if ( null != grid &&
					//    grid.ActiveCell != null &&
					//    grid.ActiveCell.IsInEditMode )
					//{
					//    grid.ActiveCell.ExitEditMode( true, true );
					//}
					if ( null != grid )
					{
						UltraGridCell activeCell = grid.ActiveCell;

						if ( activeCell != null &&
							activeCell.IsInEditMode )
						{
							activeCell.ExitEditMode( true, true );
						}
					}
				}
				catch(Exception)
				{
				}

				// SSP 12/8/03 UWG2753
				// Maintain the tempActiveRow to what it is if it's valid. Use the new 
				// ActiveRowInvalidHelper method.
				//
				//this.ClearActiveRow( );
				this.ActiveRowInvalidHelper( );

				this.in_getActiveRow = false;
			}
		}

		internal void SetActiveRow( UltraGridRow row, bool scrollIntoView )
		{
			// JJD 1/28/02 - UWG1001
			// Make sure the row is still valid. If not set the active row to null
			//
			if ( row != null )
			{
				if ( !row.IsStillValid )
				{ 
					row = null; 
				}
			}

			// SSP 11/17/03 Add Row Feature
			// If the activation of a template add-row is disabled, then prevent the user
			// from activating the row.
			//
			// ----------------------------------------------------------------------------
			if ( null != row && row.IsTemplateAddRow && Activation.Disabled == row.ActivationResolved )
				return;
			// ----------------------------------------------------------------------------

            // MBS 2/27/08 - RowEditTemplate NA2008 V2
            // If we're changing the active row, we should be closing the template too, which will
            // try to commit the changed values to the underlying row.  
            UltraGridRowEditTemplate template = null;
            if (this.ActiveRow != null)
            {
                try
                {
                    template = this.ActiveRow.RowEditTemplateResolved;
                    if (template != null && template.IsShown)
                    {
                        // If the position has changed in the underlying DataSource, we have
                        // no control over this so we need to close the template regardless.
                        if (this.ActiveRow.forceTemplateCloseOnActionRowChanged)
                        {
                            template.Close(true, true);
                        }
                        else if (row == null || row.RowEditTemplateResolved != template ||
                            row.IsDisabled || row.HiddenResolved ||
                            // With the Modeless template, we will allow the template to remain open
                            // when the active row is changed so that the user can continue to edit, 
                            // assuming that the same template is being used.
                            template.DisplayMode != RowEditTemplateDisplayMode.Modeless)
                        {
                            // If we can't close the template, then we should not allow the active
                            // row to be changed either.
                            if (template.Close(true, false) == false)
                                return;
                        }
                    }
                }
                finally
                {
                    // Reset the flag so that we don't force subsequent templates to close
                    this.ActiveRow.forceTemplateCloseOnActionRowChanged = false;
                }
            }

			// SSP 1/22/02 UWG962
			// Set the TempActiveRow to null so that at next paint
			// we don't try to set the ActiveRow with the value in
			// TempActiveRow
			//
			this.TempActiveRow = null;


			// first clear the active row and if that isn't canceled
			// then set the new active row
			//

			// SSP 8/17/01 UWG132
			// We need to pass in true for update so that the row
			// get's updated if the update mode is OnRowChange
			//if ( this.OnActiveRowCleared( false ))
			if ( this.OnActiveRowCleared( true ))
			{
				// SSP 10/14/02 UWG1753
				// We also need to set the tempactive row to null because
				// OnActiveRowCleared call above may call EndEdit on the row which can
				// cause the binding manager to fire position changed event which will
				// lead to TempActiveRow to be set. So we need to clear it here.
				//
				this.TempActiveRow = null;

				this.OnActiveRowChange( row, scrollIntoView );

				// JJD 12/15/03 - Added accessibility support
				// Call AccessibilityNotifyClients  
                if (this.ActiveRow == row)
                {
                    // MBS 4/21/08 - RowEditTemplate NA2008 V2
                    // At this point if the template of the old row has a non-null Row property
                    // we should update it to the current row, assuming that it's the same
                    // template as the current row's template.  In most cases, we would have
                    // closed the template already, which would have nulled out the row, but we'll 
                    // make sure that the IsShown property also returns false, which in combination
                    // with the non-null Row likely means that the customer is showing the template
                    // themselves
                    if(template != null && template.Row != null && template.IsShown == false)
                    {
                        UltraGridRowEditTemplate newTemplate = row.RowEditTemplateResolved;
                        if (newTemplate == null || newTemplate != template)
                            template.Row = null;
                        else
                            template.Row = row;
                    }

                    if (this.ContainsFocus)
                        this.AccessibilityNotifyClientsInternal(AccessibleEvents.Focus, 0);
                }
			}
		}

		#region ActiveRowInternal
		
		// SSP 4/1/05 - Optimizations
		// Added ActiveRowInternal.
		//
		internal UltraGridRow ActiveRowInternal
		{
			get
			{
				return this.currentActiveRow;
			}
		}

		#endregion // ActiveRowInternal

		/// <summary>
		/// Return the KeyActionMappings collection.
		/// </summary>
		internal protected abstract KeyActionMappingsBase GetKeyActionMappings();

		/// <summary>
		/// Return the current state.
		/// </summary>
		internal protected abstract Int64 GetCurrentState();

        /// <summary>
        /// Performs a specific key action
        /// </summary>
        ///	<param name="actionCode">An enumeration value that determines the user action to be performed.</param>
        ///	<param name="ctlKeyDown">A boolean specifies whether the action should be performed as if the control key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is maintained, as it is when the user holds down the control key and selects a row in a grid.</param>
        ///	<param name="shiftKeyDown">A boolean specifies whether the action should be performed as if the shift key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is extended, as it is when the user holds down the shift key and selects a range of rows in a grid.</param>                
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
        internal protected abstract bool PerformKeyAction(Enum actionCode, bool shiftKeyDown, bool ctlKeyDown);

		/// <summary>
		/// Called when the active row changes
		/// </summary>
        /// <param name="newActiveRow">The new active row.</param>
        /// <param name="scrollIntoView">true to scroll the new row into view.</param>
        /// <returns>true if the row was successfully changes. false it it was cancelled.</returns>
		internal protected abstract bool OnActiveRowChange( Infragistics.Win.UltraWinGrid.UltraGridRow newActiveRow, bool scrollIntoView );

		/// <summary>
		/// Called when the active row is cleared
		/// </summary>
        ///<param name="update">true to update the row.</param>
        ///<returns>true if the row was successfully cleared. false it it was cancelled.</returns>
		internal protected abstract bool OnActiveRowCleared( bool update );

		/// <summary>
		/// Internal method for raising the AfterColPosChanged event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		internal protected virtual void FireAfterColPosChanged ( AfterColPosChangedEventArgs e ){}

		/// <summary>
		/// Internal method for raising the AfterGroupPosChanged event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        internal protected virtual void FireAfterGroupPosChanged(AfterGroupPosChangedEventArgs e) { }

		/// <summary>
		/// Internal method for raising the AfterRowRegionScroll event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        internal protected virtual void FireAfterRowRegionScroll(RowScrollRegionEventArgs e) { }

		/// <summary>
		/// Internal method for raising the BeforeColRegionScroll event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        internal protected virtual void FireBeforeColRegionScroll(BeforeColRegionScrollEventArgs e) { }

		/// <summary>
		/// Internal method for raising the BeforeColPosChanged event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        internal protected virtual void FireBeforeColPosChanged(BeforeColPosChangedEventArgs e) { }

		/// <summary>
		/// Internal method for raising the BeforeGroupPosChanged event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        internal protected virtual void FireBeforeGroupPosChanged(BeforeGroupPosChangedEventArgs e) { }

		/// <summary>
		/// Internal method for raising the BeforeRowRegionScroll event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        internal protected virtual void FireBeforeRowRegionScroll(BeforeRowRegionScrollEventArgs e) { }

		// JJD 12/03/01 - UWG813
		// Maintain flag so we know when to fire initialize layout
		//
		internal bool InitializeLayoutFired { get { return this.initializeLayoutFired; } }
		internal void ResetInitializeLayoutFiredFlag() { this.initializeLayoutFired = false; }


		/// <summary>
		/// Internal method for raising the InitializeLayout event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        internal protected virtual void FireInitializeLayout(InitializeLayoutEventArgs e)
		{
			// JJD 12/03/01 - UWG813
			// Maintain flag so we know if initialize layout was fired
			//
			this.initializeLayoutFired = true;

		}
		/// <summary>
		/// Internal method for raising the InitializeRow event
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        internal protected abstract void FireInitializeRow(InitializeRowEventArgs e);

		/// <summary>
		/// Internal property to determine if we are in the InitializeLayout event. 
		/// </summary>
		internal protected abstract bool InInitializeLayout { get; }

		/// <summary>
		/// Internal property that returns the current print layout. 
		/// </summary>
		internal protected virtual UltraGridLayout PrintLayout{ get { return null; } }

		/// <summary>
		/// Internal property to determine if we are printing. 
		/// </summary>
		internal protected virtual bool IsPrinting{ get { return false; } set{} }

		/// <summary>
		/// Internal property to determine if a event is in progress 
		/// </summary>
        /// <param name="eventId">Indicates the event.</param>
        /// <returns>true if the event is in progress.</returns>
		internal protected abstract bool IsEventInProgress( Enum eventId );

		/// <summary>
		/// Internal property that determines if this control supports printing. (only UltraGrid does). 
		/// </summary>
		internal protected virtual bool SupportsPrinting { get { return false; } }

		/// <summary>
		/// Returns the control that will be used for the grid display.
		/// </summary>
		/// <remarks>The default is to return this. It is overridden by the UltraCombo class to return a different control (used for the dropdown area).</remarks>
		internal protected virtual System.Windows.Forms.Control ControlForGridDisplay
		{ get { return this; } }

		//	BF 8.7.02
		/// <summary>
		/// Called right before the parent's SelectNextControl method is called
		/// so that derived classes can perform control-specific actions, such
		/// as closing open dropdowns
		/// </summary>
		internal protected virtual void OnSelectingNextControl(){	}

		/// <summary>
		/// Called when a property has changed
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        protected override void OnPropertyChanged(Infragistics.Win.PropertyChangedEventArgs e)
		{
			// AS 9/13/02 UWG1659
			// Skip property changed notifications during initialization
			//
			if (this.preventPropChangeNotification)
				return;

			// SSP 11/4/05 BR07555
			// Added TextRenderingMode property on the UltraControlBase.
			// 
			// ----------------------------------------------------------------------------
			if ( e.ChangeInfo.PropId is UltraControlPropIds
				&& (UltraControlPropIds)e.ChangeInfo.PropId == UltraControlPropIds.TextRenderingMode )
			{
				UltraGridLayout layout = this.DisplayLayout;
				if ( null != layout )
				{
					layout.DirtyGridElement( true, true, true );
					layout.ClearBandRowHeightMetrics( );
					layout.ClearCachedCaptionHeight( );
				}
			}
			// ----------------------------------------------------------------------------

			
			// AS 6/17/02 UWG1239
			// Always notify the design environment of property changes
			//
			// SSP 6/18/02 UWG1211.
			// Now we are notifying the component change service in the
			// base class in OnPropertyChanged so we don't need to do
			// it here.
			//
			//this.NotifyDesignEnvironmentOfSubObjectChange();

			// JJD 1/18/02
			// Call the base OnPropertyChanged
			//
			base.OnPropertyChanged(e);

		}

		// SSP 6/18/02
		// Moved this in the win in UltraControlBase.
		//
		
	
		/// <summary>
		/// Called by UltraGridBase when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected virtual void OnBasePropChanged( PropChangeInfo propChange )
		{
			

			// JJD 11/06/01
			// Pass the notification on
			//
			UltraGridLayout layout = this.DisplayLayout;
			if ( null != layout && propChange.Source == layout )
			{
				// SSP 3/25/06 - App Styling
				// Dirty the cached style related property values if necessary.
				// 
				bool dirtyCachedStyleProperties = false;
				if ( propChange.PropId is PropertyIds )
				{
					switch ( (PropertyIds)propChange.PropId )
					{
						case PropertyIds.BorderStyle:
						case PropertyIds.BorderStyleCaption:
							dirtyCachedStyleProperties = true;
							break;
					}

					if ( dirtyCachedStyleProperties )
						StyleUtils.DirtyCachedPropertyVals( layout );
				}

				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DisplayLayout, propChange );
			}
			else
				if ( propChange.Source == this.layouts )
			{
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.Layouts, propChange );
			}
		}
		
		/// <summary>
		/// Called when a property has changed on a sub-object
		/// </summary>
		/// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		/// <remarks>
		/// <p class="body">This property is used for sub-object event notification. You can take advantage of this feature to have your own code invoked when one or more properties of a sub-object are changed.</p>
		/// </remarks>
		public void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			this.OnBasePropChanged( propChange );
		}

		/// <summary>
		/// Called when the main font has changed 
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        protected override void OnFontChanged(EventArgs e)
		{
			// Clear cached metrics and dirty grid if font changes
			//				
			this.DisplayLayout.ClearCachedCaptionHeight();

			this.DisplayLayout.OnBaseFontChanged();

			this.DisplayLayout.DirtyGridElement();

			// AS - 12/14/01
			// We need to call the base implementation.
			base.OnFontChanged(e);
		}

		/// <summary>
		/// Called when the control is created 
		/// </summary>
		protected override void OnCreateControl()
		{
			base.OnCreateControl();

			//RobA UWG568 10/18/01
			//since OnCreateControl wasn't getting called until we actually show
			//the control, we only want to do this during design time.
			//We are doing this in EndInit during run time
			//
			// SSP 5/16/02 UWG1133 UWG1138
			// We want to call VerifyDataSourceAttached during run time as well
			// because the designer may put the code that calls EndInit on all the 
			// components in such a way that grid's EndInit gets called before the data 
			// source's EndInit gets called. So what happens is that in in EndInit, we
			// try to apply the loaded bands but at that point the data source has not
			// been initialized yet, so none of the columns get loaded by the grid and
			// we never reapply the loaded bands when the data source gets initialized
			// so none of the design time settings get carried over to runtime. So we
			// should try to reapply the loaded bands again here (at this point all
			// the components have been initialied properly).
			// Took out the check for DesignMode.
			//
			//if ( this.DesignMode )
			//{
			// JJD 10/19/01
			// Moved init logic into VerifyDataSourceAttached
			//
			this.VerifyDataSourceAttached();
			//}
			
		}

		/// <summary>
        /// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{
			// SSP 5/14/02 UWG1109
			// Dispose of the paintTimer if created.
			//
			if ( disposing && null != this.paintTimer )
			{
				this.paintTimer.Enabled = false;
				this.paintTimer.Tick -= new EventHandler( this.PaintTimer_Tick );
				this.paintTimer.Dispose( );
				this.paintTimer = null;
			}

			// SSP 7/29/04 - UltraCalc
			// Remove the grid from the calc manager if any.
			//
			// ------------------------------------------------------------------
			this.CalcManager = null;
			// ------------------------------------------------------------------

			if ( disposing && null != this.layout )
			{
				// unhook the prop change notifications
				//
				this.layout.SubObjectPropChanged -= new SubObjectPropChangeEventHandler( OnSubObjectPropChanged );
				this.layout.Dispose();
				this.layout = null;
			}

			// SSP 10/28/03 UWG2573
			// Set the activeColScrollRegion and activeRowScrollRegion to null so that if
			// for some reason some external object is holding a reference to UltraGrid
			// and preventing it from being collected by the garbase collector, then at 
			// least row bands/scrollregions/rows/cells and other grid objects get collected.
			//
			// ------------------------------------------------------------------------------
			if ( disposing )
			{
				this.activeColScrollRegion = null;
				this.activeRowScrollRegion = null;
				this.currentActiveRow = null;
				this.tempActiveRow = null;

				try
				{
					if ( this.dataSource is IComponent )
						((IComponent)this.dataSource).Disposed -= new EventHandler( this.DataSource_Disposed );
				}
				catch( Exception )
				{
				}
				this.dataSource = null;
				this.dataMember = null;

				try
				{
					this.UnhookFromBindingManager( this.bindingManager );
				}
				catch ( Exception )
				{
				}

				this.bindingManager = null;

				try
				{
					if ( null != this.layouts )
						this.layouts.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
					this.layouts = null;
				}
				catch ( Exception )
				{
				}
			}
			// ------------------------------------------------------------------------------

			base.Dispose( disposing );
		}
		

		/// <summary>
		/// Returns the event handler that notifies OnSubObjectPropChanged
		/// </summary>
		protected SubObjectPropChangeEventHandler SubObjectPropChangeHandler
		{
			get
			{
				if ( this.subObjectPropChangeHandler == null )
					this.subObjectPropChangeHandler = new SubObjectPropChangeEventHandler( OnSubObjectPropChanged );

				return this.subObjectPropChangeHandler;
			}
		}

		/// <summary>
		/// A collection used to store UltraGridLayout objects for easy retrieval.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// One way to persist UltraGridLayout objects and apply them to different objects 
		/// is to save them out to storage using the <b>SaveLayout</b> and 
		/// <b>LoadLayout</b> methods. If you wish to persist a UltraGridLayout object 
		/// without using these methods, you can also add it to the Layouts 
		/// collection for later retrieval and use.
		/// </p>
		/// <seealso cref="UltraGridBase.DisplayLayout"/>
		/// <seealso cref="UltraGridLayout"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridBase_P_Layouts")]
		[ LocalizedCategory("LC_Layout") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Content) ]
		public LayoutsCollection Layouts
		{
			get
			{
				if ( null == this.layouts )
				{
					this.layouts = new LayoutsCollection();

					// SSP 5/20/05 BR04176
					// Added backward pointer to the grid. This is necessary at deisgn time when a 
					// new layout is added to the layout's collection via the layout collection editor.
					// We need to initialize the layout with a grid so for example column collection
					// editor dialog has access to the grid as well to make the transactions work.
					// 
					this.layouts.InitGrid( this );

					// SSP 7/1/03 UWG2443
					// Hook into the layouts subobject prop changed so we can notify the
					// designer that something has changed.
					//
					this.layouts.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.layouts;
			}
		}
 
		/// <summary>
		/// Returns True is any of the properties have been
		/// set to non-default values.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeLayouts() 
		{	
			
			return null != this.layouts &&
				this.Layouts.Count > 0 ;
		}
 
		/// <summary>
		/// Clears all layouts
		/// </summary>
		public void ResetLayouts() 
		{
			if ( null != this.layouts )
				this.layouts.Clear();
		}

		void ISupportInitialize.BeginInit()
		{
			this.initializing = true;

			// SSP 3/27/03 - Row Layout Functionality
			// We need to clear the loaded bands collection in the BeginInit. This flag is used to
			// prevent us from clearing it if the grid is located on a base form and the changes
			// are made in the derived form.
			// 
			if ( this.endInitDuringDesignModeRecieved && null != this.DisplayLayout )
			{
				this.endInitDuringDesignModeRecieved = false;

				// SSP 6/3/04
				// When clearing the loaded bands and resetting the display layout, don't fire
				// designer prop change notifications.
				// Enclosed the existing code into the try-finally block.
				//
				bool origPreventPropChangeNotification = this.preventPropChangeNotification;
				this.preventPropChangeNotification = true;
				try
				{
					// Clear the loaded bands collection.
					//
					this.DisplayLayout.InternalClearLoadedBands( );

					// Also reset the display layout (and bands along with it).
					//
					this.DisplayLayout.Reset( );

					// SSP 8/12/04 - UltraCalc
					// Unregister from the calc manager when canceling transaction because all
					// the columns and bands will be cleared and re-added. We will register
					// them in VerifyDataSourceAttached which gets called from EndInit.
					//
					this.UnRegisterFromCalcManager( );

					// SSP 5/12/06 BR11947
					// EndInit calls HookDesignerHostLoaded which in certain circumstances (as in the
					// bug) ends up doing a NO-OP (doesn't end up hooking into anything). As a
					// result we should allow lazy binding after EndInit by resetting this flag.
					// 
					this.DisplayLayout.dataSourceAttachedAtLeastOnce = false;
				}
				finally
				{
					this.preventPropChangeNotification = origPreventPropChangeNotification;
				}
			}

			// AS 3/5/03 UWG2003
			this.OnBeginInit();		
		}
		
		void ISupportInitialize.EndInit()
		{			
			// SSP 3/27/03 - Row Layout Functionality
			// We need to clear the loaded bands collection in the BeginInit. This flag is used to
			// prevent us from clearing it if the grid is locaed on a base form and the changed
			// are made in the derived form.
			// 
			this.endInitDuringDesignModeRecieved = this.DesignMode;				

			// JJD 11/20/01
			// Let the layout know that deserialization is over
			//
			this.DisplayLayout.OnDeserializationComplete();

			// SSP 8/29/02 UWG1614 UWG1133 UWG1138
			// We need to do this in OnCreateControl. 
			// The reason why we need to do this is that ISupportInitialize.EndInit method may
			// get called before the data source's ISupportInitialize.EndInit gets called. This 
			// may happen if the designer writes out the code in such a way that EndInit
			// on the grid occurs before it occurs on the data table and this would lead
			// to grid prematurely initializing the layout loading the saved layout. At
			// this point since the data source has not been fully intialized, the data source
			// may not have any columns or data structure. We want to basically bind to the 
			// data source after the data source has been initialized. A good place
			// is OnCreateControl method. At that point all the components should have been
			// initialized. So do it there instead in here.
			// Moved below code in OnCreateControl and commented it out here.
			//
			// -----------------------------------------------------------------------------
			
			// -----------------------------------------------------------------------------

			// SSP 7/1/03 UWG2443
			// Copy the loaded bands to the bands in all the layouts in the layouts collection.
			// The reason for doing this is that when a layout in the layouts collection is 
			// serialized, the BandsSerializer property is the one that gets serialized and not
			// the Bands property. So when the deserialization is complete, copy the loadedBands
			// to the Bands for each layout in the layouts collection.
			// 
			// ------------------------------------------------------------------------------------------
			if ( null != this.layouts )
			{
				// SSP 6/25/04 UWG2937
				// Also initialize grid on the layouts in the collection. The reason for doing this
				// is that certain properties look to see if the grid (ViewStyleImpl is one of them)
				// is an instance of UltraGrid and return different a value based on whether grid is 
				// UltraGrid or UltraGridBase (because certain functionalities are not supported in 
				// UltraDropDown).
				//
				this.layouts.InitGrid( this );

				for ( int i = 0; i < this.layouts.Count; i++ )
				{
					BandsSerializer source = this.layouts[i].LoadedBandsInternal;
					if ( null != source && source.Count > 0 )
						// SSP 6/24/04 UWG2937
						// Pass in true for the new calledFromEndInit parameter.
						//
						//this.layouts[i].Bands.InternalCopyFromBandsSerializer( source, true );
                        // MRS 1/30/2009 - TFS12662
						//this.layouts[i].SortedBands.InternalCopyFromBandsSerializer( source, true, true );
                        this.layouts[i].Bands.InternalCopyFromBandsSerializer(source, true, true);
				}
			}
			// ------------------------------------------------------------------------------------------

            // SSP 4/29/06 BR11367
            // 
            // MRS 5/4/06
            //if ( this.DesignMode && null != this.layout && this.layout.Bands.Count > 0 && this.layout.LoadedBandsInternal.Count > 0 )
            // MRS 1/30/2009 - TFS12662
            //if (this.DesignMode && null != this.layout && this.layout.SortedBands.Count > 0 && this.layout.LoadedBandsInternal != null && this.layout.LoadedBandsInternal.Count > 0)
            if (this.DesignMode && null != this.layout && this.layout.Bands.Count > 0 && this.layout.LoadedBandsInternal != null && this.layout.LoadedBandsInternal.Count > 0)
                this.layout.ApplyLoadedBandsHelper();

			// AS 9/13/02 UWG1659
			// Wait for the designer transaction to complete
			// to verify the datasource and apply the loaded
			// bands.
			//
			this.HookDesignerHostLoaded();

			this.initializing = false;

			// AS 1/11/06 BR08714
			this.DisplayLayout.SynchGridBackColor();

			// SSP 10/25/02 UWG1780
			// If the control is already created (which may happen and which is what
			// happened with the test project in UWG1780 causing that bug), we have to
			// load the data source here because we won't be getting OnCreateControl 
			// afterwards.
			//
			// AS/SSP 3/11/04 UWG3013
			// We might have been created but only because someone
			// inadvertantly caused the control's Handle to be created.
			// Since we will do this verification in the get of the Bands
			// property, do not do it here.
			//
			//if ( !this.DesignMode && this.Created )
			//{
			//	this.VerifyDataSourceAttached( );
			//}

			// AS 3/5/03 UWG2003
			this.OnEndInit();
		}

		// AS 9/13/02 UWG1659
		#region Designer Host Transaction - UWG1659
		private void HookDesignerHostLoaded()
		{
			if (!this.DesignMode)
				return;

			IDesignerHost host = this.GetService( typeof(IDesignerHost) ) as IDesignerHost;

			if (host == null)
				return;

			if (host.InTransaction)
				host.TransactionClosing += new DesignerTransactionCloseEventHandler( this.OnDesignerTransactionClosing );
			else if (host.Loading)
				host.LoadComplete += new EventHandler( this.OnDesignerHostLoadComplete );
		}

		private void OnDesignerTransactionClosing( object sender, DesignerTransactionCloseEventArgs e )
		{
			this.DesignTimeDataSourceInitialization();

			IDesignerHost host = this.GetService( typeof(IDesignerHost) ) as IDesignerHost;

			if (host != null)
				host.TransactionClosing -= new DesignerTransactionCloseEventHandler( this.OnDesignerTransactionClosing );
		}

		private bool preventPropChangeNotification = false;

		private void DesignTimeDataSourceInitialization()
		{
			this.preventPropChangeNotification = true;

			try
			{
				this.VerifyDataSourceAttached();
			}
			finally
			{
				this.preventPropChangeNotification = false;
			}
		}

		private void OnDesignerHostLoadComplete( object sender, EventArgs e )
		{
			this.DesignTimeDataSourceInitialization();

			IDesignerHost host = this.GetService( typeof(IDesignerHost) ) as IDesignerHost;

			if (host != null)
				host.LoadComplete -= new EventHandler( this.OnDesignerHostLoadComplete );
		}
		#endregion //Designer Host Transaction - UWG1659

		// SSP 1/6/03 UWG1902
		// Made this internal.
		//
		//private void VerifyDataSourceAttached()
		internal void VerifyDataSourceAttached( )
		{
			// JJD 10/29/01
			// If the BindingContext hasn't been initialized then it would be 
			// fruitless to do anything with the data source
			//
			if ( this.BindingContext == null )
				return;

			// SSP 10/11/06 - NAS 6.3
			// Added SetInitialValue on the UltraCombo.
			// 
			if ( this.dontLoadDataSource )
				return;

			// SSP 1/6/03 UWG1902
			// Added anti-recursion flag since the get of DisplayLayout.Bands property now
			// calls this method. Enlcosed the below block of code in try-finally to ensure
			// that we reset this anti-recursion flag.
			//
			if ( this.in_VerifyDataSourceAttached )
				return;

			this.in_VerifyDataSourceAttached = true;

			try
			{
				// SSP 1/22/03 UWG1955
				// If we are currently in Set_ListManager, then don't call Set_ListManager
				// again and more importantly don't call ApplyLoadedBands again. Calling
				// ApplyLoaded bands here may lead to premature deserialization of the layout
				// since we are still in Set_ListManager trying to load the data source.
				// Enclosed the code in the if block.
				//
				if ( ! this.inSet_ListManager )
				{
					// JJD 10/16/01
					// If the bands haven't been created yet initialize the 
					// list manager
					//
                    // MRS 1/30/2009 - TFS12174
					//if ( this.DisplayLayout.SortedBands.Count < 1 ||
                    if (this.DisplayLayout.Bands.Count < 1 ||
						// SSP 10/15/02 UWG1614
						// Instead of checking if the number of columns is 0, check if
						// the binding manager is null because a data table could have
						// 0 coulmns and band could have been initialized. Also the 
						// band could have unbound columns and not have been intialized.
						// (NOTE: initialized means initailzed with a data source).
						//
						//this.DisplayLayout.Bands[0].Columns.Count < 1 )
                        // MRS 1/30/2009 - TFS12174
						//null == this.DisplayLayout.SortedBands[0].BindingManager )
                        null == this.DisplayLayout.Bands[0].BindingManager)
					{
						if ( this.dataSource != null )
						{
							this.Set_ListManager(this.dataSource, this.dataMember);
						}
							// SSP 5/25/04 - Extract Data Structure Related
							// Added else block.
							//
						else
						{
							if ( null != this.DisplayLayout && null != this.DisplayLayout.LoadedBandsInternal
								&& this.DisplayLayout.LoadedBandsInternal.Count > 0 )
							{
                                // MRS 1/30/2009 - TFS12174
								//this.DisplayLayout.SortedBands.InternalCopyFromBandsSerializer( this.DisplayLayout.LoadedBandsInternal, false );
                                this.DisplayLayout.Bands.InternalCopyFromBandsSerializer(this.DisplayLayout.LoadedBandsInternal, false);
								this.DisplayLayout.DisposeRows( );

								// SSP 6/2/04 UWG3382
								// Call the new InitializeParentBandPointers method to initialize the parent
								// band pointers instead of calling the InitializeFrom. The problem with calling
								// InitializeFrom on a bands collection with the same bands collection as the
								// argument is that InitializeFrom logic tends to clear collections before 
								// initializing from the source collection (for example destination sorted 
								// collection and groups are cleared before initializing them from the source
								// collection, however if the source and the destination happen to be the same, 
								// it will result in source collection getting cleared).
								//
								// ------------------------------------------------------------------------------
								
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//this.DisplayLayout.Bands.InitializeParentBandPointers( this.DisplayLayout.Bands );
                                // MRS 1/30/2009 - TFS12174
								//BandsCollection.InitializeParentBandPointers( this.DisplayLayout.SortedBands );
                                BandsCollection.InitializeParentBandPointers(this.DisplayLayout.Bands);
								// ------------------------------------------------------------------------------

								// When there is no data source and we are storing the data structure only, then 
								// we need to set the chaptered state of columns that are chaptered. Before we 
								// were simply checking the property descriptor's property type. However in the 
								// "Extract Data Structure Mode", we don't have any property descriptors to check.
								//
                                // MRS 1/30/2009 - TFS12174
								//foreach ( UltraGridBand band in this.DisplayLayout.SortedBands )
                                foreach (UltraGridBand band in this.DisplayLayout.Bands)
								{
									if ( null != band && null != band.ParentColumn )
										band.ParentColumn.InternalSetCachedIsChaptered( true );
								}
							}
						}
					}

					this.DisplayLayout.ApplyLoadedBands();

					//Reconnect back pointers
                    // MRS 1/30/2009 - TFS12174
                    //if ( this.DisplayLayout.SortedBands.Layout == null )
                    //    this.DisplayLayout.SortedBands.InitLayout(this.DisplayLayout);		
                    if (this.DisplayLayout.Bands.Layout == null)
                        this.DisplayLayout.Bands.InitLayout(this.DisplayLayout);		

					// SSP 8/12/04 - UltraCalc
					// Register formulas and references with the calc manager. We are waiting for
					// EndInit to compile formulas and register them.
					//
					this.RegisterWithCalcManager( );
				}
			}
			finally
			{
				// SSP 1/6/03 UWG1902
				// Added anti-recursion flag since the get of DisplayLayout.Bands property now
				// calls this method.
				//
				this.in_VerifyDataSourceAttached = false;
			}
		}
				
		internal bool Initializing
		{
			get
			{
				return this.initializing;
			}
		}

		#region InternalSetInitializing

		// SSP 2/26/07 BR18838
		// 
		internal void InternalSetInitializing( bool value )
		{
			this.initializing = value;
		}

		#endregion // InternalSetInitializing

		/// <summary>
		/// Returns or sets the ImageList component, if any, that is associated with the control.
		/// </summary>
		/// <remarks>
		/// <p class="body">For the control to use the <b>ImageList</b> property, 
		///	you must put an ImageList component on the form. Then, at design time, you can set 
		///	the <b>ImageList</b> property in the associated control's property page from the 
		///	drop down box containing the names of all the ImageList controls currently on the
		/// form. To associate an ImageList with a control at run time, set the control's 
		///	<b>ImageList</b> property to the ImageList component you want to use, as in this 
		///	example:</p>
		///	<p class="code">Set UltraWinGrid1.ImageList = ImageList1</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridBase_P_ImageList")]
		[ LocalizedCategory("LC_Appearance") ]
		public System.Windows.Forms.ImageList ImageList 
		{
			get
			{
				return this.imageList;
			}
			set
			{
				if ( value != this.imageList )
				{
					this.imageList = value;
					if ( this.Created && this.layout.UIElement != null )
						this.layout.UIElement.DirtyChildElements(true);
			
					// JJD 11/06/01
					// Notify listeners
					//  
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ImageList );
				}
			}
		}
 
		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeImageList() 
		{
			return null != this.imageList;
		}
 
		/// <summary>
		/// Resets this property to its default value.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		public void ResetImageList() 
		{
            // MRS NAS v8.2 - Unit Testing
			//this.imageList = null;
            this.ImageList = null;
		}
		

		/// <summary>
		/// Returns the DisplayLayout object that determines the layout of the control. This property is read-only at run-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>DisplayLayout</b> property of an object is used to access the DisplayLayout object that determines the settings of various properties related to the appearance and behavior of the object. The DisplayLayout object provides a simple way to maintain multiple layouts for the grid and apply them as needed. You can also save grid layouts to disk, the registry or a storage stream and restore them later.</p>
		///	<p class="body">The DisplayLayout object has properties such as <b>Appearance</b> and <b>Override</b>, so the DisplayLayout object has sub-objects of these types, and their settings are included as part of the layout. However, the information that is actually persisted depends on how the settings of these properties were assigned. If the properties were set using the DisplayLayout object's intrinsic objects, the property settings will be included as part of the layout. However, if a named object was assigned to the property from a collection, the layout will only include the reference into the collection, not the actual settings of the named object.</p>
		///	<p class="body">For example, if the DisplayLayout object's <b>Appearance</b> property is used to set values for the intrinsic Appearance object like this:</p>
		///	<p class="code">UltraWinGrid1.Layout.Appearance.ForeColor = vbBlue</p>
		///	<p class="body">Then the setting (in this case, <b>ForeColor</b>) will be included as part of the layout, and will be saved, loaded and applied along with the other layout data. However, suppose you apply the settings of a named object to the DisplayLayout's <b>Appearance</b> property in this manner:</p>
		///	<p class="code">UltraWinGrid1.Appearances.Add "New1"</p>
		///	<p class="code">UltraWinGrid1.Appearances("New1").ForeColor = vbBlue</p>
		///	<p class="code">UltraWinGrid1.DisplayLayout.Appearance = UltraWinGrid1.Appearances("New1")</p>
		///	<p class="body">In this case, the <b>ForeColor</b> setting will not be persisted as part of the layout. Instead, the layout will include a reference to the "New1" Appearance object and use whatever setting is present in that object when the layout is applied.</p>
		///	<p class="body">By default, the layout includes a copy of the entire Appearances collection, so if the layout is saved and restored using the default settings, the object should always be present in the collection when it is referred to. However, it is possible to use the <b>Load</b> and <b>Save</b> methods of the DisplayLayout object in such a way that the collection will not be re-created when the layout is applied. If this is the case, and the layout contains a reference to a nonexistent object, the default settings for that object's properties will be used.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridBase_P_DisplayLayout")]
		[ LocalizedCategory("LC_Layout") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Content) ]
		public Infragistics.Win.UltraWinGrid.UltraGridLayout DisplayLayout
		{
			get
			{
				return this.layout;
			}
		}
			
 
		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeDisplayLayout() 
		{
			// JJD 5/19/03 - WTB947
			// Also return true if ForceSerialization is true
			return this.layout.ShouldSerialize() || this.layout.ForceSerialization;
		}
 
		/// <summary>
		/// Resets the properties of the object to their default values.
		/// </summary>
		///	<remarks>
		///	<p class="body">
		///	Use this method to reset the properties of the DisplayLayout object to their default values. The appearance of any object associated with the DisplayLayout object will change accordingly.
		///	</p>
		///	</remarks>
		public void ResetDisplayLayout() 
		{
			this.layout.Reset();
		}


		/// <summary>
		/// IUIElementProvider's method to get the borderstyle of the element
		/// </summary>
		/// <param name="element"> </param>
		UIElementBorderStyle IUIElementProvider.GetBorderStyle( UIElement element )
		{
			return UIElementBorderStyle.Raised;
		}

		/// <summary>
		/// Returns cursor
		/// </summary>
		/// <param name="element"></param>
		/// <returns></returns>
		System.Windows.Forms.Cursor IUIElementProvider.GetCursor ( UIElement element )
		{
			return this.layout.UIElement.Parent.Cursor;
		}
		
		/// <summary>
		/// IUIElementProvider's method to get the sides to draw the border on
		/// </summary>
		Border3DSide IUIElementProvider.GetBorderSides( UIElement element )
		{
			return Border3DSide.All;
		}
 
		/// <summary>
		/// IUIElementProvider's method to initialize the appearance
		/// of the element during a draw
		/// </summary>
		void IUIElementProvider.InitElementAppearance ( UIElement element,
			ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps ) {}

		
		/// <summary>
		/// IUIElementTextProvider's method to get the element's text
		/// </summary>
		String IUIElementTextProvider.GetText( DependentTextUIElement element )
		{ 
			return this.Text;
		}

		/// <summary>
		/// IUIElementTextProvider's method to find out if the text
		/// is multiline.
		/// </summary>
		bool IUIElementTextProvider.IsMultiLine( DependentTextUIElement element )
		{
			return false; 
		}

		// SSP 2/5/02 
		// Added.
		/// <summary>
		/// IUIElementTextProvider's method to find out if the text
		/// is to be wrapped.
		/// </summary>
		bool IUIElementTextProvider.WrapText( DependentTextUIElement element )
		{
			return ((IUIElementTextProvider)this).IsMultiLine( element );
		}

		/// <summary>
		/// IUIElementTextProvider's method to get the padding around the text
		/// </summary>
		void IUIElementTextProvider.GetTextPadding ( DependentTextUIElement element, ref Size padding )
		{
			padding.Width = 2;
			padding.Height = 2;
		} 

		/// <summary>
		/// IUIElementTextProvider's method to find out if the text
		/// should be displayed vertically.
		/// </summary>
		bool IUIElementTextProvider.IsVertical( DependentTextUIElement element )
		{
			return false; 
		}

		/// <summary>
		/// IUIElementTextProvider's method to adjust the display rect of the
		/// text (e.g. used to shift the text right and down 1 pixel in the
		/// down state of a button)
		/// </summary>
		void IUIElementTextProvider.AdjustTextDisplayRect( DependentTextUIElement element,
			ref Rectangle displayRect ){}

		// AS - 1/21/02 UWG847
		/// <summary>
		/// Indicates whether the element renders mnemonics in the text.
		/// </summary>
		System.Drawing.Text.HotkeyPrefix IUIElementTextProvider.HotkeyPrefix( DependentTextUIElement element )
		{
			return System.Drawing.Text.HotkeyPrefix.None;
		}
		

		/// <summary>
		/// Returns a collection of the topmost level of rows in the grid. This collection will
		/// either contain all the rows in Band 0 or the top level of GroupBy rows (if GroupBy rows
		/// are being used.) This property is read-only at run-time and is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The Rows collection provides a easy way to navigate through row hierarchy of the grid, determine whether the grid contains data, and determine how rows are being displayed in GroupBy mode. When displaying data in a standard hierarchical view, the Rows collection contains an UltraGridRow object for every top-level (Band 0) row in the grid. The UltraGridRow objects in the collection expose a <b>ChildBands</b> property, which returns a collection of <b>Rows</b> collections representing the rows of the bands which are children of the current row.</p>
		/// <p class="body">The Rows colelction is also useful in GroupBy mode, when the top-level band or bands of the grid may consist of virtual "group by" rows. In this case, you use the Rows collection to obtain all the GroupBy rows that are being displayed, and the <b>ChildBand</b> properties of those rows provide a way to drill down to the actual data.</p>
		/// <seealso cref="RowsCollection"/>
		/// <seealso cref="UltraGridRow"/>
		/// <seealso cref="UltraGridRow.ChildBands"/>
		/// <seealso cref="UltraGridGroupByRow"/>
		/// </remarks>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public Infragistics.Win.UltraWinGrid.RowsCollection Rows
		{
			get
			{
				// return the rows from the main grid layout

				// SSP 8/16/02
				// Check for the layout being null. Because when we are disposed of
				// layout will be null.
				//
				if ( null != this.DisplayLayout )
					return this.DisplayLayout.Rows;

				return null;
			}
		}

		/// <summary>
		/// Indicates a sub-list of the DataSource to show in the UltraWinGrid.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>Note:</b> Setting the <b>DataSource</b> or the <b>DataMember</b> property causes
		/// the UltraGrid to load the data. Therefore to avoid loading data twice it's recommended 
		/// that you set the DataMember before setting the DataSource or as a convenience use the 
		/// <see cref="SetDataBinding(object, string, bool, bool)"/> method which lets you set the DataSource and the DataMember 
		/// in a single call.
		/// </p>
		/// <p class="body">
		/// Setting the DataSource and then setting the DataMember will cause the UltraGrid to load 
		/// data twice; once when the DataSource is set and again when the DataMember is set. 
		/// Setting the DataMember while the DataSource is null (Nothing in Visual Basic) does not 
		/// cause the UltraGrid to load the data since there is no data source to load the data from. 
		/// Also note that there is no need to call DataBind method after setting the DataSource and 
		/// DataMember.
		/// </p>
		/// <p class="body">
		/// See <see cref="UltraGridBase.DataSource"/> property for more information.
		/// </p>
		/// </remarks>
		[
		DefaultValue(""),
		LocalizedCategory("LC_Data"),		
		Editor( "System.Windows.Forms.Design.DataMemberListEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing" ),
		// JJD 1/18/02 - UWG945
		// Added RefreshProperties attribute since setting this property can change
		// the band's structure
		RefreshProperties( RefreshProperties.All),  
		LocalizedDescription("LD_UltraGridBase_P_DataMember")
		]
		public string DataMember 
		{
			get 
			{
				return this.dataMember;
			}
			set 
			{
				if (this.dataMember != null && this.dataMember.Equals(value)) 
				{
					return;
				}
                    
				//ClearAll();
				this.dataMember = value == null ? string.Empty : value;
				
				// JJD 12/03/01 - UWG813
				// Reset flag so that InitializeLayout will get fired again
				//
				this.ResetInitializeLayoutFiredFlag();

				this.Set_ListManager(this.dataSource, this.dataMember);
			
				// JJD 11/06/01
				// Notify listeners
				//  
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DataMember );
			}
		}
 
		/// <summary>
		/// Rebinds to the data source, causing InitializeLayout to be fires again. 
		/// <b>Note</b> that setting the DataSource property binds to the data source 
		/// and loads the data. This method is for forcing the UltraGrid to reload the 
		/// data.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>DataBind</b> method reloads the data. Reloading the data source will
		/// cause the InitializeLayout event to be fired. To bind to a data source, simply
		/// setting the <b>DataSource</b> property is sufficient. This method is used for 
		/// forcing the UltraGrid to reload the data.
		/// </p>
		/// <p class="body">
		/// <b>Note:</b> Setting the DataSource property and calling the DataBind after it will cause the 
		/// UltraGrid to load the data twice, and cause the InitializeLayout event to be 
		/// fired twice as well.
		/// </p>
		/// </remarks>
		public void DataBind() 
		{
			//ClearAll();
				
			// JJD 12/03/01 - UWG813
			// Reset flag so that InitializeLayout will get fired again
			//
			this.ResetInitializeLayoutFiredFlag();

			this.Set_ListManager(dataSource, dataMember);

		}

		

		private void DataSource_Disposed( object sender, EventArgs e ) 
		{
			// SSP 8/26/04
			// Set the DataSource to null before setting the DataMember. Because if you
			// set the DataMember to null before then we will reload the data structure
			// from the data source using the new data member.
			//
			//this.DataMember = null;
			//this.DataSource = null;
			this.DataSource = null;
			this.DataMember = null;
		}


		/// <summary>
		/// Specifies the source of data for the UltraWinGrid.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>DataSource</b> property is used to specify the object from which the UltraWinGrid
		/// should retrieve and display data. Possible data sources include:
		/// <list type="bullet">
		/// <item><description>DataSet</description></item>
		/// <item><description>DataTable</description></item>
		/// <item><description>DataView</description></item>
		/// <item><description>DataViewManager</description></item>
		/// <item><description>UltraDataSource</description></item>
		/// <item><description>Any object that implements IList interface, including ArrayList and Array.</description></item>
		/// <item><description>Any object</description></item>
		/// </list>
		/// </p>
		/// <p class="body">
		/// <b>Note:</b> For creating <b>custom data sources</b> you can implement the .NET <b>IList</b> interface
		/// on your custom object. However, the <i>IList</i> interface doesn't support notifications
		/// for changes in data such as changes in cell values, adding or removing of rows, etc.
		/// Therefore if you want to support these operations in your data source then it is recommended that 
		/// you implement the .NET <b>IBindingList</b> interface, instead, which has support for notifying 
		/// the bound controls of such changes. Other interfaces that may be of interest are the .NET <b>ITypedList</b>
		/// and <b>IEditableObject</b> interfaces. Please consult the .NET Framework help for more information on these 
		/// interfaces.
		/// </p>
        /// <p class="body">
        /// Setting <b>DataSource</b> or <b>DataMember</b> will cause the control to synchronously 
        /// load the data source. Therefore if you need to set both of these at run-time, to prevent binding
        /// twice, use the <see cref="SetDataBinding(object, string, bool, bool)"/> method instead of setting the individual properties. 
        /// The <b>SetDataBinding</b> method lets you specify both pieces of information in a single call. Also 
        /// note that since DataSource, DataMember and SetDataBinding all load the data source, there is no 
        /// need to call the <b>DataBind</b> method.
        /// </p>
        /// <p class="body">
        /// If you set up your grid at <b>design time</b> with bands and columns and then bind at run-time 
        /// to the actual data source, make sure that band and column keys match the names of the corresponding 
        /// bands (relations) and columns in the actual data source. Otherwise settings on the bands and columns
        /// will not be carried over from design time. UltraGrid uses the keys (Key property) of the bands
        /// and columns to find matching bands and columns at run-time to copy over the settings
        /// from design-time. Therefore it's important to have the design-time band and column keys match the 
        /// band (relation) and column names in the actual data source.
        /// </p>
        /// <p class="body">
        /// The same matching process is applied when changing the data source at run-time.
        /// The data structure of the control must match the data structure of the newly set
        /// data source. The control will automatically create all bands and columns exposed 
        /// by the new data source. The control will try to maintain the settings on the 
        /// existing columns and bands assuming that the Key of the Band is the same and that 
        /// the Key of the column is the same. Columns or bands that do not exist in the new 
        /// data structure will be destroyed and any new columns or bands in the new data source
        /// will be created. There is no way to prevent the the control from automatically picking 
        /// up the new data structure - it must synchronize it's structure with that of the data 
        /// source. However, you can make use of the <see cref="UltraGridLayout.NewBandLoadStyle"/>
        /// and <see cref="UltraGridLayout.NewColumnLoadStyle"/> properties to automatically hide
        /// new bands and columns in the new data source. Alternatively you can hide columns or bands 
        /// by using the Hidden property of the appropriate object. Unbound columns will be 
        /// maintained, so long as the band the column belongs to is not destroyed.
        /// </p>
        /// <p class="body">
        /// The <b>InitializeLayout</b> event is raised when the control binds to a DataSource.
        /// The <b>InitializeRow</b> event is raied for each row as it's created.
        /// </p>
		/// <seealso cref="DataMember"/>
		/// <seealso cref="SetDataBinding(object, string, bool, bool)"/>
		/// <see cref="UltraGridLayout.NewBandLoadStyle"/>
		/// <see cref="UltraGridLayout.NewColumnLoadStyle"/>
		/// <see cref="UltraGridLayout.NewColumnLoadStyle"/>
		/// <see cref="UltraGridBase.Rows"/>
		/// <see cref="UltraGridColumn.Hidden"/>
		/// <see cref="UltraGridBand.Hidden"/>
		/// <see cref="UltraGridRow.Hidden"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridBase_P_DataSource")]
		[DefaultValue(null)]
		[LocalizedCategory("LC_Data")]
			// JJD 1/18/02 - UWG945
			// Added RefreshProperties attribute since setting this property can change
			// the band's structure
		[RefreshProperties( RefreshProperties.All)]
        [AttributeProvider(typeof(IListSource))]
		public object DataSource 
		{
			get 
			{
				return this.dataSource;
			}

			set 
			{
				if (dataSource != null && dataSource.Equals(value)) 
				{
					return;
				}

				// SSP 4/11/05 BR03298
				// Moved the code to hook/unhook into HookDataSourceDisposed and UnhookDataSourceDisposed
				// helper methods.
				//
				//if ( null != this.dataSource && this.dataSource is IComponent )
				//	((IComponent)this.dataSource).Disposed -= new EventHandler( this.DataSource_Disposed );
				this.UnhookDataSourceDisposed( );
				
				//                ClearAll();
				this.dataSource = value;

				// SSP 4/11/05 BR03298
				// Moved the code to hook/unhook into HookDataSourceDisposed and UnhookDataSourceDisposed
				// helper methods.
				//
				//if ( this.dataSource is IComponent )
				//	((IComponent)this.dataSource).Disposed += new EventHandler( this.DataSource_Disposed );
				this.HookDataSourceDisposed( );
				
				// JJD 12/03/01 - UWG813
				// Reset flag so that InitializeLayout will get fired again
				//
				this.ResetInitializeLayoutFiredFlag();

				this.Set_ListManager(this.dataSource, this.dataMember);
			
				// JJD 11/06/01
				// Notify listeners
				//  
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.DataSource );
			}
		}

		// SSP 4/11/05 BR03298
		// Moved the code to hook/unhook into HookDataSourceDisposed and UnhookDataSourceDisposed
		// helper methods.
		//
		private void HookDataSourceDisposed( )
		{
			if ( this.dataSource is IComponent )
				((IComponent)this.dataSource).Disposed += new EventHandler( this.DataSource_Disposed );
		}

		private void UnhookDataSourceDisposed( )
		{
			if ( null != this.dataSource && this.dataSource is IComponent )
				((IComponent)this.dataSource).Disposed -= new EventHandler( this.DataSource_Disposed );
		}

		/// <summary>
		/// Sets the datasource and datamember for the control in one atomic operation.
		/// </summary>
		/// <param name="dataSource">Data source to bind to.</param>
		/// <param name="dataMember">Data member.</param>
		/// <remarks>
		/// <p class="body">
		/// See <see cref="UltraGridBase.DataSource"/> property for more information.
		/// </p>
		/// <seealso cref="UltraGridLayout.NewColumnLoadStyle"/> <seealso cref="UltraGridLayout.NewBandLoadStyle"/>
		/// </remarks>
		public void SetDataBinding( object dataSource, string dataMember )
		{
			// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
			// 
			//this.SetDataBinding( dataSource, dataMember, false );
			this.SetDataBinding( dataSource, dataMember, NewColumnLoadStyle.Hide == this.DisplayLayout.NewColumnLoadStyle );
		}

		// SSP 9/2/04
		// Added GetBandColumnString helper method.
		//
		// SSP 1/3/06 BR08519
		// Added bandsColl parameter.
		// 
		//private string GetBandColumnString( UltraGridBand band, UltraGridColumn column )
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string GetBandColumnString( BandsCollection bandsColl, UltraGridBand band, UltraGridColumn column )
		private static string GetBandColumnString( BandsCollection bandsColl, UltraGridBand band, UltraGridColumn column )
		{
			// SSP 1/3/06 BR08519
			// 
			//return ( null != band.ParentBand ? band.Key : "/" ) + "/" + column.Key;
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return this.GetBandString( bandsColl, band ) + "/" + column.Key;
			return UltraGridBase.GetBandString( bandsColl, band ) + "/" + column.Key;
		}

		// SSP 9/2/04
		// Added GetBandColumnString helper method.
		//
		// SSP 1/3/06 BR08519
		// Added bandsColl parameter.
		// 
		//private string GetBandString( UltraGridBand band )
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string GetBandString( BandsCollection bandsColl, UltraGridBand band )
		private static string GetBandString( BandsCollection bandsColl, UltraGridBand band )
		{
			// SSP 1/3/06 BR08519
			// In order to check if the band is the root band, instead of checking for ParentBand 
			// being null, check to see if the band is 0th band in the bands collection. The reason
			// for this is that parent band pointers may not have been hooked up yet.
			// 
			//return null != band.ParentBand ? band.Key : "/";
			return bandsColl.Count > 0 && band == bandsColl[0] ? "/" : band.Key;
		}

		// SSP 4/23/04 - Virtual Binding Related
		// Added hideNewColumns parameter to the SetDataBinding method.
		// 
		/// <summary>
		/// Sets the datasource and datamember for the control in one atomic operation.
		/// </summary>
		/// <param name="dataSource">Data source to bind to.</param>
		/// <param name="dataMember">Data member.</param>
		/// <param name="hideNewColumns">Whether to hide new columns in the data source.</param>
		/// <remarks>
		/// <p class="body">
		/// See <see cref="UltraGridBase.DataSource"/> and <see cref="UltraGridBase.DataMember"/>
		/// for more information.
		/// </p>
		/// <seealso cref="UltraGridLayout.NewColumnLoadStyle"/> <seealso cref="UltraGridLayout.NewBandLoadStyle"/>
		/// </remarks>
		public void SetDataBinding( object dataSource, string dataMember, bool hideNewColumns )
		{
			this.SetDataBinding( dataSource, dataMember, hideNewColumns, NewBandLoadStyle.Hide == this.DisplayLayout.NewBandLoadStyle );
		}

		// SSP 5/3/05
		// Added an overload of SetDataBinding that takes in hideNewBands parameter.
		//
		/// <summary>
		/// Sets the datasource and datamember for the control in one atomic operation.
		/// </summary>
		/// <param name="dataSource">Data source to bind to.</param>
		/// <param name="dataMember">Data member.</param>
		/// <param name="hideNewColumns">Whether to hide new columns in the data source.</param>
		/// <param name="hideNewBands">Whether to hide new bands in the data source.</param>
		/// <remarks>
		/// <p class="body">
		/// See <see cref="UltraGridBase.DataSource"/> and <see cref="UltraGridBase.DataMember"/>
		/// for more information.
		/// </p>
		/// <seealso cref="UltraGridLayout.NewColumnLoadStyle"/> <seealso cref="UltraGridLayout.NewBandLoadStyle"/>
		/// </remarks>
		public void SetDataBinding( object dataSource, string dataMember, bool hideNewColumns, bool hideNewBands )
		{
			// HideNewColumns and hideNewBands parameters should take precedence over the 
			// NewColumnLoadStyle and NewBandLoadStyle property settings. Since 
			// SetDataBindingHelper method itself hides new columns and bands set the 
			// NewColumnLoadStyle temporarily to the default value so the InitializeFrom 
			// doesn't attempt to hide any new columns and bands.
			//
			this.DisplayLayout.newColumnLoadStyleOverride = NewColumnLoadStyle.Show;
			this.DisplayLayout.newBandLoadStyleOverride = NewBandLoadStyle.Show;
			try
			{
				this.SetDataBindingHelper( dataSource, dataMember, hideNewColumns, hideNewBands );
			}
			finally
			{
				this.DisplayLayout.newColumnLoadStyleOverride = null;
				this.DisplayLayout.newBandLoadStyleOverride = null;
			}
		}

		// SSP 5/3/05
		// Added an overload of SetDataBinding that takes in hideNewBands parameter. Added 
		// SetDataBindingHelper method.
		//
		private void SetDataBindingHelper( object dataSource, string dataMember, bool hideNewColumns, bool hideNewBands )
		{
			// SSP 4/23/04 - Virtual Binding Related
			// Added hideNewColumns parameter to the SetDataBinding method.
			//
			// ----------------------------------------------------------------------------------
			Hashtable oldColumns = null;
			// SSP 5/3/05
			// Added an overload of SetDataBinding that takes in hideNewBands parameter.
			//
			//if ( hideNewColumns && null != this.layout )
			Hashtable oldBands = null;
			if ( ( hideNewColumns || hideNewBands ) && null != this.layout )
			{
				BandsCollection bands = this.layout.SortedBands;
				// If no data source had been set and thus no data structure exists, then use
				// the serialized bands.
				//
				if ( null == bands || bands.Count <= 0 || bands[0].Columns.Count <= 0 )
					bands = this.layout.LoadedBandsInternal;

				// Store all the columns that exist in the old data structure.
				//
				if ( null != bands )
				{
					oldColumns = new Hashtable( );

					// SSP 5/3/05
					// Added an overload of SetDataBinding that takes in hideNewBands parameter.
					//
					oldBands = new Hashtable( );

					foreach ( UltraGridBand band in bands )
					{
						// SSP 5/3/05
						// Added an overload of SetDataBinding that takes in hideNewBands parameter.
						//
						// SSP 1/3/06 BR08519
						// Added bandsColl parameter.
						// 
						//oldBands[ this.GetBandString( band ) ] = band;
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//oldBands[ this.GetBandString( bands, band ) ] = band;
						oldBands[ UltraGridBase.GetBandString( bands, band ) ] = band;

						if ( null != band.Columns )
						{
							foreach ( UltraGridColumn column in band.Columns )
							{
								if ( column.Key.Length > 0 )
									// SSP 9/2/04
									//
									//oldColumns[ band.Key + "/" + column.Key ] = new object( );
									// SSP 1/3/06 BR08519
									// Added bandsColl parameter.
									// 
									//oldColumns[ this.GetBandColumnString( band, column ) ] = new object( );
									// MD 7/26/07 - 7.3 Performance
									// FxCop - Mark members as static
									//oldColumns[ this.GetBandColumnString( bands, band, column ) ] = new object( );
									oldColumns[ UltraGridBase.GetBandColumnString( bands, band, column ) ] = new object();
							}
						}
					}
				}
			}
			// ----------------------------------------------------------------------------------

			// SSP 4/11/05 BR03298
			// DataSource property already hooks/unhooks from the Disposed event of the DataSource. 
			// SetDataBinding also needs to do this.
			//
			this.UnhookDataSourceDisposed( );

			this.dataSource = dataSource;
			this.dataMember = dataMember;

			// SSP 4/11/05 BR03298
			// DataSource property already hooks/unhooks from the Disposed event of the DataSource. 
			// SetDataBinding also needs to do this.
			//
			this.HookDataSourceDisposed( );

			// JJD 12/03/01 - UWG813
			// Reset flag so that InitializeLayout will get fired again
			//
			this.ResetInitializeLayoutFiredFlag();

			this.Set_ListManager(this.dataSource, this.dataMember);

			// SSP 4/23/04 - Virtual Binding Related
			// Added hideNewColumns parameter to the SetDataBinding method.
			// 
			// ----------------------------------------------------------------------------------
			// SSP 5/3/05
			// Added an overload of SetDataBinding that takes in hideNewBands parameter.
			// 
			//if ( hideNewColumns && null != oldColumns && null != this.layout && null != this.layout.Bands )
			if ( ( hideNewColumns || hideNewBands ) && null != oldColumns && null != this.layout && null != this.layout.SortedBands )
			{
				// Hide new columns that did not exist in the old data structure.
				//
				// SSP 1/3/06 BR08519
				// 
				//foreach ( UltraGridBand band in this.layout.Bands )
				BandsCollection bands = this.layout.SortedBands;
				foreach ( UltraGridBand band in bands )
				{
					if ( null != band.Columns )
					{
						bool atLeastOneColumnMatched = false;
						foreach ( UltraGridColumn column in band.Columns )
						{
							// SSP 9/2/04
							//
							//atLeastOneColumnMatched = oldColumns.ContainsKey( band.Key + "/" + column.Key );
							// SSP 1/3/06 BR08519
							// Added bandsColl parameter.
							// 
							//atLeastOneColumnMatched = oldColumns.ContainsKey( this.GetBandColumnString( band, column ) );
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//atLeastOneColumnMatched = oldColumns.ContainsKey( this.GetBandColumnString( bands, band, column ) );
							atLeastOneColumnMatched = oldColumns.ContainsKey( UltraGridBase.GetBandColumnString( bands, band, column ) );

							if ( atLeastOneColumnMatched )
								break;
						}

						// SSP 5/3/05
						// Added an overload of SetDataBinding that takes in hideNewBands parameter.
						// 
						//if ( atLeastOneColumnMatched )
						if ( atLeastOneColumnMatched || NewColumnLoadStyle.Hide == this.DisplayLayout.NewColumnLoadStyle )
						{
							foreach ( UltraGridColumn column in band.Columns )
							{
								// SSP 9/2/04
								//
								//if ( ! oldColumns.ContainsKey( band.Key + "/" + column.Key ) )
								// SSP 5/3/05
								// Added an overload of SetDataBinding that takes in hideNewBands parameter.
								// 
								//if ( ! oldColumns.ContainsKey( this.GetBandColumnString( band, column ) ) )
								// SSP 1/3/06 BR08519
								// Added bandsColl parameter.
								// 
								//if ( hideNewColumns && ! oldColumns.ContainsKey( this.GetBandColumnString( band, column ) ) )
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//if ( hideNewColumns && ! oldColumns.ContainsKey( this.GetBandColumnString( bands, band, column ) ) )
								if ( hideNewColumns && !oldColumns.ContainsKey( UltraGridBase.GetBandColumnString( bands, band, column ) ) )
									column.Hidden = true;
							}
						}

						// SSP 5/3/05
						// Added an overload of SetDataBinding that takes in hideNewBands parameter.
						// 
						// SSP 1/3/06 BR08519
						// Added bandsColl parameter.
						// 
						//if ( hideNewBands && null != oldBands && ! oldBands.ContainsKey( this.GetBandString( band ) ) )
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( hideNewBands && null != oldBands && ! oldBands.ContainsKey( this.GetBandString( bands, band ) ) )
						if ( hideNewBands && null != oldBands && !oldBands.ContainsKey( UltraGridBase.GetBandString( bands, band ) ) )
							band.Hidden = true;
					}
				}
			}
			// ----------------------------------------------------------------------------------
		}

        #region GetCurrencyManager

		// SSP 12/6/05 - Binding Source
		// Added GetCurrencyManager to DataBindingUtils for creating currency managers differently
		// for BindingSource datasources. Make use of that to create the currency managers.
		// See those methods for more info.
		// 

        internal CurrencyManager GetCurrencyManager(string dataMember)
        {
            return DataBindingUtils.GetCurrencyManager(this.BindingContext, this.DataSource, dataMember);
        }

        #endregion // GetCurrencyManager

		// SSP 10/16/02 UWG1759
		// Added InternalInSet_ListManager property
		//
		internal bool InternalInSet_ListManager
		{
			get
			{
				return this.inSet_ListManager;
			}
		}

		// SSP 10/28/03 UWG2573
		//
		private void UnhookFromBindingManager( BindingManagerBase bindingManager )
		{
			CurrencyManager cm = bindingManager  as CurrencyManager;
			if ( null != cm && cm.List is IBindingList )
			{
				IBindingList bl = (IBindingList)cm.List;

				if ( null != bl )
					bl.ListChanged -= new System.ComponentModel.ListChangedEventHandler( this.BindingListChanged );
			}
		}

		private void Set_ListManager(object newDataSource, string newDataMember ) 
		{
			// SSP 1/8/03 UWG1924
			// If we are in the middle of BeginInit and EndInit, then return. We
			// will hook up the data source when EndInit gets called. The reason why
			// we had to do this is there are situations where the control is created
			// but we are still in the begininit and endinit. In such a situation
			// we do not want to hook up the data source until endinit is called.
			//
			if ( this.Initializing )
				return;

			// SSP 10/11/06 - NAS 6.3
			// Added SetInitialValue on the UltraCombo.
			// 
			if ( this.dontLoadDataSource )
				return;

			// SSP 10/30/01 UWG595
			// Added this anti-recursion flag and enclosed the code in try
			// finally block. We are hooking into the ListChanged event of
			// the binding list and calling this method from there. However
			// the code in here could possibly lead to ListChanged event getting
			// fired and this getting called from there.
			//
			if ( this.inSet_ListManager )
				return;

			// MD 5/22/09 - TFS16348
			// Remove the calc references of the band if any have been added. We need to do this incase the band key changes
			// so the reference names in the calc manager do not still have the old band key in their names.
			UltraGridBand band = this.DisplayLayout.BandZero;
			if ( band != null )
			{
				try
				{
					// Set the anti-recursion flag so we don't get back in here while updating the calc manager.
					this.dontLoadDataSource = true;

					band.RemoveCalcReferences();
				}
				finally
				{
					this.dontLoadDataSource = false;
				}
			}

			this.inSet_ListManager = true;		

			try
			{
				// MRS 9/21/04 - UWG3670
				// Just as an optimization, clear the selections, so the OnDispose 
				// of every row and cell doesn't go looking for them
				UltraGrid grid = this as UltraGrid;
				if ( grid != null )
				{					
					grid.Selected.Rows.Clear();
					grid.Selected.Cells.Clear();

					// SSP 7/11/06 BR14276
					// Also clear the selected columns. Previously the selected columns collection
					// was cleared from ClearMainUIElement however the Selected flags of the columns
					// weren't being reset. Since now we are clearing the selected rows and cells and
					// causing the SelectChange to fire, do the same for selected columns.
					// 
					grid.Selected.Columns.Clear( );
				}

				// Exit the edit mode and end edit on the current binding manager
				//
				UpdateListManager();

				// SSP 5/26/06 - Optimization
				// 
				bool previouslyBound = null != this.bindingManager;

				// SSP 10/30/01 UWG595
				// Unhook from previously hooked in event handler
				//
				// SSP 10/28/03 UWG2573
				// Moved the code into separate method UnhookFromBindingManager.
				//
				// ------------------------------------------------------------------------
				this.UnhookFromBindingManager( this.bindingManager );
				
				// ------------------------------------------------------------------------

				if (this.DataSource != null && this.BindingContext != null) 
				{
					// Create the new BindingManagerBase
					this.bindingManager = this.BindingContext[this.DataSource, this.DataMember];
				} 
				else 
				{
					this.bindingManager = null;

                    // MRS 6/26/07 - BR24212
                    // If we got here, it means the user set the DataSource of the grid while the 
                    // BindingContext is null. Reset the flag so that the grid will rebind the 
                    // next time it needs to. 
                    this.DisplayLayout.dataSourceAttachedAtLeastOnce = false;
				}
				

				// SSP 10/30/01 UWG595
				// We need to listen into the ListChanged of the grid's
				// currency manager as well in order to get notified on
				// when the underlying dataset is reset or refilled 
				// ( possibly with a different table ).
				//
				CurrencyManager cm = this.bindingManager  as CurrencyManager;
				if ( null != cm && cm.List is IBindingList )
				{
					IBindingList bl = (IBindingList)cm.List;

					if ( null != bl )
						bl.ListChanged += new System.ComponentModel.ListChangedEventHandler( this.BindingListChanged );
				}
			
				// JJD 10/10/01 - UWG510
				// Always call ListManagerUpdated since if it is null
				// we need to clear the bands collection
				//
				//			if (bindingManager != null)

				// JJD 11/19/01
				// If we are shutting down then just exit.
				//
				if ( this.DisplayLayout == null )
					return;

				// SSP 2/25/05 BR02564
				// Moved this here before the call to ListManagerUpdated because we want 
				// to fire InitializeLayout after clearing the main element and cached editors.
				//
				// SSP 10/18/04 UWG3736
				// When the data source is cleared we need to clear the child elements of 
				// the main ui element otherwise some of the descendant ui elements will 
				// keep holding references back to row objects that may end up not releasing 
				// memory until the ui elements are verified next time. For example row 
				// elements could have references to the rows in which case the rows won't
				// be released and so won't the list objects, bands etc...
				// Added ClearMainUIElement method.
				//
				// SSP 5/26/06 - Optimization
				// 
				// ----------------------------------------------------
				//this.layout.ClearMainUIElement( );
				if ( previouslyBound )
					this.layout.ClearMainUIElement( );
				// ----------------------------------------------------

				this.DisplayLayout.ListManagerUpdated();

				

				// invalidate the grid element and mark its child elements dirty so 
				// they get repositioned/recreated on the next paint
				//
				this.layout.UIElement.DirtyChildElements();
				this.layout.SynchronizedColListsDirty = true;
				this.DisplayLayout.ColScrollRegions.DirtyMetrics();
				// SSP 5/26/06 - Optimizations
				// Don't force initialization of the metrics here. It will be done lazily next time
				// in the paint or when the elements are verified.
				// 
				this.DisplayLayout.ColScrollRegions.InitializeMetrics();

                // MBS 10/1/08 - TFS8165
                // We should make sure that we recalculate the summaries when we go to paint again.                
                this.DisplayLayout.BumpSummariesVersion();

				// SSP 2/25/05 BR02564
				// Moved this before the call to ListManagerUpdated because we want to fire 
				// InitializeLayout after clearing the main element and cached editors.
				//
				

                // CDS 7/07/09 TFS17996 
                // Whenever the datasource changes, we need to make sure the cached ContainsCheckboxOnTopOrBottom value for each band is dirtied.
                this.DisplayLayout.BandsInternal.DirtyCachedContainsCheckboxOnTopOrBottom();

				this.OnListManagerSet();
			}
			finally 
			{
				// Reset the anti-recursion flag
				//
				this.inSet_ListManager = false;

				// MD 5/22/09 - TFS16348
				// Re-add the calc referrences now after the list manager has been set.
				if ( band != null )
					band.AddCalcReferences();
			}
		}

		/// <summary>
		/// Called when the list manager is set (data source changed)
		/// </summary>
		protected virtual void OnListManagerSet()
		{
		}

		private void UpdateListManager() 
		{
			try 
			{
				// SSP 7/9/02
				// Exit the edit mode on current cell. This method get's called before
				// a new data source is set. 
				//
				// ------------------------------------------------------------------
				UltraGridCell activeCell = 
					null != this.DisplayLayout ? this.DisplayLayout.ActiveCell : null;

				if ( null != activeCell && activeCell.IsInEditMode )
				{
					// Force the exit.
					//
					activeCell.ExitEditMode( false, true );
				}
				// ------------------------------------------------------------------

				// MRS 1/14/05 - BR00792
				// If the Active row is an unmodified Template Add Row, then cancel it. 
				// ------------------------------------------------------------------
				UltraGridRow activeRow = 
					null != this.DisplayLayout ? this.DisplayLayout.ActiveRow : null;

				if (null != activeRow
					&& activeRow.IsUnModifiedAddRowFromTemplate)
				{
					activeRow.CancelUpdate();
				}
				// ------------------------------------------------------------------

				if (this.bindingManager != null) 
				{
					this.bindingManager.EndCurrentEdit();
				}
			}
			catch (Exception) 
			{
				// AS 2/20/02
				// There's no bad juju here. :-) After a couple of good laughs, I believe
				// we should keep this here for historical record but we'll eat the error.
				//
				//MessageBox.Show("Bad Juju on End Edit");
			}

		}



		internal System.Drawing.Size GetSizeOfImages()
		{				
			if ( null != this.ImageList )
				return this.ImageList.ImageSize;

			System.Drawing.Size sizImages = new System.Drawing.Size( 0, 0);			

			return sizImages;
		}

		internal UltraGridRow GetFirstLastRowHelper( bool first )
		{
			if ( this.Rows.Count == 0 )
				return null;

			if ( first )
				return this.Rows[0];
			else
				return this.Rows[this.Rows.Count-1];
		}


		// JJD 10/03/01
		// The GetRow method was marked internal but it should be public
		//
		/// <summary>
		/// Returns an UltraGridRow object for the first or last row in band 0.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>GetRow</b> method is one of the mechanisms you can use to navigate through the hierarchical structure of the grid. You can use this method to return either the first or last raow in the topmost band, then walk through the rows in that band (using the <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridRow.HasNextSibling()"/><b>HasNextSibling</b>, <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridRow.HasPrevSibling()"/><b>HasPrevSibling</b> and <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridRow.GetSibling(SiblingRow)"/><b>GetSibling</b> methods. You can also step into the child band of any row using the <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridRow.HasChild()"/><b>HasChild</b> and <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridRow.GetChild(ChildRow)"/><b>GetChild</b> methods.</p>
		/// </remarks>
		/// <param name="child">Determines whether the first or last row in Band 0 is returned.</param>
		/// <returns>The UltraGridRow object for the first or last row in Band 0</returns>
		public Infragistics.Win.UltraWinGrid.UltraGridRow GetRow( Infragistics.Win.UltraWinGrid.ChildRow child )
		{
			UltraGridRow row;

			switch ( child )
			{
				case Infragistics.Win.UltraWinGrid.ChildRow.First:					
					// JJD 9/26/01
					// Go back to calling GetFirstLastRowHelper the other 
					// call to GetFirstRowInBand made no sense
					//
					row = this.GetFirstLastRowHelper( true );
					//row = this.DisplayLayout.GetFirstRowInBand ( true, this.ActiveRow, false, false );
					break;
				case Infragistics.Win.UltraWinGrid.ChildRow.Last:
					// JJD 9/26/01
					// Go back to calling GetFirstLastRowHelper the other 
					// call to GetFirstRowInBand made no sense
					//
					row = this.GetFirstLastRowHelper( false );
					//row = this.DisplayLayout.GetFirstRowInBand ( false, this.ActiveRow, false, false );
					break;
				default:
					throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_294"));
			}	

			return row;
		}

		/// <summary>
		///  Clears the active row without firing any events
		/// </summary>
		internal protected virtual void ClearActiveRow()
		{
			this.TempActiveRow	= null;
			this.currentActiveRow		= null;
		}

		/// <summary>
		/// Returns the active ColScrollRegion object.
		/// </summary>
		/// <param name="initializeMetrics">UltraGrid lazily re-initializes the column metrics whenever
		/// something changes. This parameter specifies whether to verify the column scroll region metrics before returning the active column scroll region.</param>
		/// <returns></returns>
		/// <remarks>
		/// <p class="body">Use the <b>GetActiveColScrollRegion</b> method to determine which ColScrollRegion object is currently active.</p>
		/// <p class="body">Only one column scrolling region at a time may be the active ColScrollRegion. The active ColScrollRegion is the one that receives keyboard navigation focus. For example, if you use the left and right arrow keys to scroll columns, the columns in the column scrolling region returned by <b>GetActiveColScrollRegion</b> are the ones that will move.</p>
		/// <seealso cref="UltraGridBase.ActiveColScrollRegion"/>
		/// <seealso cref="UltraGridBase.ActiveRowScrollRegion"/>
		/// <seealso cref="UltraGridLayout.ColScrollRegions"/>
		/// <seealso cref="UltraGridLayout.RowScrollRegions"/>
		/// </remarks>
		public ColScrollRegion GetActiveColScrollRegion( bool initializeMetrics )
		{
			// check the make sure the cached active region has not
			// been passivated
			//
			if ( null != this.activeColScrollRegion )
			{
				if ( initializeMetrics )
					this.layout.GetColScrollRegions( initializeMetrics );

				// it is still around so return it
				//
				return this.activeColScrollRegion;
			}

			// otherwise we want to return the first scrolling region in 
			// the collection and set that as the active region
			//
			Debug.Assert ( null != this.layout, "No layout object" );

			if ( null == this.layout )
				return null;

			// get the layout's Col scroll region collection
			//
			ColScrollRegionsCollection csrColl = this.layout.GetColScrollRegions( initializeMetrics );

			Debug.Assert( null != csrColl , "No Col scroll region collection" );

			if ( null == csrColl )
				return null;

			Debug.Assert ( csrColl.Count > 0, "No Col scroll regions in collection" );

			if ( csrColl.Count < 1 )
				return null;

			// set the active Col scrolling region to the first one in the collection
			//			
			this.activeColScrollRegion = csrColl.DefaultActiveRegion;

			// return the new active scrolling region 
			//
			return this.activeColScrollRegion;
		}

		internal new bool DesignMode
		{
			get
			{
				// for some reason the base implementation in the 
				// Component class is protected and we need to expose
				// it for internal use
				//

				// SSP 7/30/04 UWG3488
				// Added IGridDesignInfo.DesignMode property to force the grid to bahave
				// as if it were in design mode. This is used by the ultragrid designer dialogs
				// to show two dummy rows in the preview grid.
				//
				// ----------------------------------------------------------------------------
				if ( ((Design.IGridDesignInfo)this).DesignMode )
					return true;
				// ----------------------------------------------------------------------------

				return base.DesignMode;
			}
		}

		[DllImport("user32.dll", CharSet=CharSet.Auto)]
		// MD 10/31/06 - 64-Bit Support
		//internal static extern int SendMessage( int hWnd, uint msg, IntPtr wParam, IntPtr lParam );
		internal static extern IntPtr SendMessage( IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam );

		internal void OnKeyDownForwarded( KeyEventArgs e )
		{
			// SSP 5/29/02 UWG1144
			// Check for the DisplayLayout being null, which it would be if we have been disposed of
			// or are in the process of being disposed of. If it is null, then call the
			// base class and return.
			//
			if ( null == this.DisplayLayout )
			{	
				this.OnKeyDown( e );
				return;
			}

			// SSP 3/11/02 UWG1039
			// Added TabNavigation.NextControlOnLastCell functionality
			//
			//-----------------------------------------------------------------------
			bool processTab = false;

			UltraGrid gridCtrl = this as UltraGrid;

			if ( 
				// SSP 10/16/03 UWG2690
				// Only process if it's Tab or Shift+Tab. Don't process other key combinations
				// involving Tab like Ctrl+Tab or Alt+Tab etc.
				// 
				//e.KeyCode == Keys.Tab 
				( Keys.Tab == e.KeyData || ( Keys.Tab | Keys.Shift ) == e.KeyData )
				&& null != gridCtrl &&				
				this.DisplayLayout.TabNavigation == TabNavigation.NextControlOnLastCell )
			{
				if ( e.Modifiers == Keys.Shift )
				{
					processTab = gridCtrl.IsActiveCellFirstVisibleCell( );
				}
				else
				{
					processTab = gridCtrl.IsActiveCellLastVisibleCell( );
				}
			}
			//-----------------------------------------------------------------------


			//RobA UWG504 10/11/01
			//
			// SSP 3/11/02 UWG1039
			//
			//if ( e.KeyCode == Keys.Tab && this.DisplayLayout.TabNavigation != TabNavigation.NextCell )
			// SSP 1/6/03 UWG1921
			// TabNavigation doesn't apply to ultracombo.
			//
			//if ( processTab || ( e.KeyCode == Keys.Tab && this.DisplayLayout.TabNavigation == TabNavigation.NextControl ) )
			if ( processTab || (
				// SSP 10/16/03 UWG2690
				// Only process if it's Tab or Shift+Tab. Don't process other key combinations
				// involving Tab like Ctrl+Tab or Alt+Tab etc.
				// 
				// e.KeyCode == Keys.Tab && 
				( Keys.Tab == e.KeyData || ( Keys.Tab | Keys.Shift ) == e.KeyData ) &&
				( this.DisplayLayout.TabNavigation == TabNavigation.NextControl || this is UltraCombo ) ) )
			{
				if ( this.DisplayLayout.ActiveCell != null )
					this.DisplayLayout.ActiveCell.ExitEditMode();

				// JJD 12/04/01
				// Wrap parent get in try/catch in case we don't have
				// security rights
				//
				try
				{
					// SSP 12/6/02 UWG1666
					// If the parent of the ultracombo is a user control, call ProcessDialogKey
					// for tab keys otherwise it doesn't work. I guess SelectNextControl doesn't
					// take into account user control.
					//
					// -------------------------------------------------------------------------
					// SSP 10/9/03 UWG1666
					// Instead of checking if the parent is an UserControl, check if any of the
					// ancestors is an UserControl.
					//
					Control parent = this.Parent;
					while ( null != parent && ! ( parent is UserControl ) )
						parent = parent.Parent;

					if ( ( Keys.Tab == e.KeyData || ( Keys.Shift | Keys.Tab ) == e.KeyData ) 
						&& this is UltraCombo 
						// SSP 10/9/03 UWG1666
						// Instead of checking if the parent is an UserControl, check if any of the
						// ancestors is an UserControl.
						//
						//&& this.Parent is UserControl
						&& parent is UserControl )
					{
						UltraCombo combo = (UltraCombo)this;

						bool origDontGiveFocusToTextBox = combo.dontGiveFocusToTextBox;
						combo.dontGiveFocusToTextBox = true;
						try
						{								
							combo.Focus( );
						}
						finally
						{
							combo.dontGiveFocusToTextBox = origDontGiveFocusToTextBox;
						}

						this.ProcessDialogKey( e.KeyData );	

						// SSP 1/6/03 UWG1917
						// Call the OnSelectingNextControl method to close the drop down if this is
						// an utlracombo.
						//
						//	Call the virtual OnSelectingNextControl method
						//	so that derived classes can, for example, close dropdowns
						//	that might be open
						//
						this.OnSelectingNextControl();

						return;
					}
					// -------------------------------------------------------------------------

					// SSP 2/25/03 UWG2005
					// Use the central UltraControlBase.SetFocusToNextControl method to set the focus to the
					// next control. It gets the form and calls SelectNextControl with appropriate parameters
					// to select next control.
					// Commented below code out and added new one below it.
					//
					// --------------------------------------------------------------------------------
					

					this.OnSelectingNextControl( );
					this.SetFocusToNextControl( ! e.Shift );
					// --------------------------------------------------------------------------------
				}
				catch{}

				return;
			}

			this.OnKeyDown( e );
		}

		internal void OnKeyPressForwarded( KeyPressEventArgs e )
		{
			this.OnKeyPress( e );
		}

		internal void OnKeyUpForwarded( KeyEventArgs e )
		{
			this.OnKeyUp( e );		
		}

		// MRS 2/17/06 - BR09325
		#region OnMouseWheelInternal
		internal void OnMouseWheelInternal( MouseEventArgs e )
		{
            // MRS 2/22/06 - BR10236
            // Moved this from OnMouseWheel into this separate method.
            //
 
            // JAS BR04593 7/8/05
            // In CLR v2 it is possible to prevent the MouseWheel message from entering the DefWndProc
            // by casting the MouseEventArgs to a HandledMouseEventArgs and setting the Handled property
            // to true.  This prevents the control's parent from receiving the MouseWheel message.
            HandledMouseEventArgs handledArgs = e as HandledMouseEventArgs;
            if (handledArgs != null)
            {
                if (handledArgs.Handled)
                    return;

                // MRS 10/3/06 - BR16260
                // Setting this here is bad, because if we pass off these events args to any
                // other method, they will think the scrolling has already been handled. 
                // Set this in the Finally block instead. 
                //
                //handledArgs.Handled = true;
            }
            // ---------------------------------------------------------------------------------------------------            

            // MRS 11/17/2008 - TFS10297
            // Rather than always setting handled to true on the event args, only
            // set if to truu if we actually did something. 
            bool handled = false;

            try
            {
                // AS 11/17/03 UWG2751
                //
                //UIElement element = this.DisplayLayout.UIElement.ElementFromPoint( new Point( e.X, e.Y ) );
                Control ctrl = this.ControlForGridDisplay;
                Point pt = new Point(e.X, e.Y);

                if (ctrl != this &&
                    ctrl != null &&
                    !ctrl.IsDisposed &&
                    !this.IsDisposed)
                {
                    pt = this.PointToScreen(pt);
                    pt = ctrl.PointToClient(pt);
                }

                UIElement element = this.DisplayLayout.UIElement.ElementFromPoint(pt);

                // RobA UWG739 11/19/01 
                // If this is a dropdown scroll the dropdown instead of the grid
                //
                // SSP 5/1/03 - Cell Level Editor
                //
                
                UltraGridCell activeCell = this.DisplayLayout.ActiveCell;
                // SSP 5/1/03 - Cell Level Editor
                // Added Editor and ValueList properties off the cell so the user can set the editor
                // and value list on a per cell basis.
                //
                //IValueList valueList = null != activeCell ? activeCell.Column.ValueList : null;
                IValueList valueList = null != activeCell ? activeCell.ValueListResolved : null;

                // MRS 2/17/06 - BR09325				
                // If the editor for this cell is dropped down, then there is no way
                // for us to know if it handled the mouse wheel. 
                // So if there's no ValueList on this cell and there is an editor
                // and the editor is in edit mode and dropped down, we will assume
                // that the editor did handle the mouse wheel and so we will not
                // scroll the grid. 
                if (valueList == null)
                {
                    EmbeddableEditorBase editor = null != activeCell ? activeCell.EditorResolved as EditorWithCombo : null;
                    if (editor != null &&
                        editor.IsInEditMode &&
                        editor.SupportsDropDown &&
                        editor.IsDroppedDown)
                    {
                        return;
                    }
                }

                // SSP 5/8/05 - NAS 5.2 Filter Row
                // Filter cell can drop down two, operator and operand value lists. Prevent
                // both from processing the mouse wheel.
                //
                if (activeCell is UltraGridFilterCell && activeCell.IsInEditMode)
                {
                    // JAS BR04593 7/8/05 - This flag is not needed.
                    //
                    //callBaseImplementation = false;

                    // SSP 5/19/05 BR03866
                    //
                    if ((null == valueList || !valueList.IsDroppedDown)
                        && FilterOperatorLocation.Hidden != activeCell.Column.FilterOperatorLocationResolved)
                    {
                        IValueList operatorValueList = activeCell.Column.FilterOperatorEditorOwnerInfo.GetValueList(activeCell);
                        if (null != operatorValueList && operatorValueList.IsDroppedDown)
                            valueList = operatorValueList;
                    }
                }

                if (null != valueList && valueList.IsDroppedDown)
                {
                    // SSP 10/15/03 UWG2711
                    // If the ValueList is an UltraCombo which derives from UltraDropDownBase, then following
                    // won't work. We need to check for UltraDropDownBase instead of UltraDropDown.
                    //
                    //UltraDropDown dropDown = valueList as UltraDropDown;
                    UltraDropDownBase dropDown = valueList as UltraDropDownBase;

                    if (dropDown != null)
                    {
                        // JJD 1/24/02 - UWG990
                        // Don't scroll if the scrollbar isn't visible and enabled
                        //
                        if (dropDown.ActiveRowScrollRegion.ScrollBarInfo.Visible &&
                            dropDown.ActiveRowScrollRegion.ScrollBarInfo.EnabledResolved)
                        {
                            if (e.Delta < 0)
                                dropDown.ActiveRowScrollRegion.Scroll(RowScrollAction.LineDown);
                            else
                                dropDown.ActiveRowScrollRegion.Scroll(RowScrollAction.LineUp);

                            // MRS 11/17/2008 - TFS10297
                            handled = true;
                        }
                    }

                    return;
                }

                // Don't scroll the grid if the swapdropdown is droppeddown
                //
                if (this.DisplayLayout.Grid is UltraGrid)
                {
                    UltraGrid grid = this.DisplayLayout.Grid as UltraGrid;

                    // SSP 12/17/03
                    // Instead of just returning, scroll the value list that's dropped down.
                    //
                    // --------------------------------------------------------------------------------
                    
                    if (grid.HasSwapDropDown && ((IValueList)grid.SwapDropDown).IsDroppedDown)
                    {
                        grid.SwapDropDown.ProcessOnMouseWheel(e);

                        // MRS 11/17/2008 - TFS10297
                        handled = true;

                        return;
                    }
                    else if (grid.HasFilterDropDown && ((IValueList)grid.FilterDropDown).IsDroppedDown)
                    {
                        grid.FilterDropDown.ProcessOnMouseWheel(e);

                        // MRS 11/17/2008 - TFS10297
                        handled = true;

                        return;
                    }
                    // --------------------------------------------------------------------------------
                }

                //	BF 11.8.04	NAS2005 Vol1 - CardView vertical scrolling
                //
                //	If the wheel is moved while the  cursor is positioned over
                //	a CardAreaUIElement, let the CardAreaUIElement handle it.
                //
                if (element != null)
                {
                    CardAreaUIElement cardAreaElement = element.GetAncestor(typeof(CardAreaUIElement)) as CardAreaUIElement;
                    if (cardAreaElement != null && cardAreaElement.Rect.Contains(pt))
                    {
                        //	The rest of the cases in this method return after the
                        //	wheel movement is processed, so we will return here
                        //	if the OnMouseWheel method returns true. This will cause
                        //	the wheel movement to be either processed by the CardAreaUIElement,
                        //	or the RowScrollRegion (see below), but not both.
                        //
                        if (cardAreaElement.OnMouseWheel(e))
                        {
                            // MRS 11/17/2008 - TFS10297
                            handled = true;

                            return;
                        }
                    }
                }

                // AS 11/17/03 UWG2751
                // If the mouse is elsewhere, use the active row scroll region.
                //
                //if ( element == null )
                //	return;

                //RowScrollRegion rsr = element.GetContext( typeof( RowScrollRegion ) ) as RowScrollRegion;
                RowScrollRegion rsr = null;

                if (element != null)
                    rsr = element.GetContext(typeof(RowScrollRegion)) as RowScrollRegion;

                // AS 11/17/03 UWG2751
                // If the mouse is elsewhere, use the active row scroll region.
                //
                if (rsr == null)
                    rsr = this.ActiveRowScrollRegion;

                //	If the rsr is null return
                if (rsr == null)
                    return;

                // JJD 1/24/02 - UWG990
                // Don't scroll if the scrollbar isn't visible and enabled
                //
                if (rsr.ScrollBarInfo.Visible &&
                    rsr.ScrollBarInfo.EnabledResolved &&
                    // MRS 11/17/2008 - TFS10297
                    // If there is a dropdown grid, then only scroll when it's visible. 
                    (rsr.Layout.Grid.ControlForGridDisplay == null ||
                    rsr.Layout.Grid.ControlForGridDisplay.Visible == true)
                    )
                {
                    // SSP 10/27/03 UWG2463
                    // Honor the MouseWheelScrollLines settings.
                    // Commented out the original code and added the new code below.
                    //
                    // --------------------------------------------------------------------------
                    
                    int scrollLines = Math.Max(1, SystemInformation.MouseWheelScrollLines);

                    bool origDontUpdateInOnScroll = rsr.dontUpdateInOnScroll;
                    if (scrollLines > 1)
                        rsr.dontUpdateInOnScroll = true;

                    try
                    {
						// SSP 3/21/07 BR21112
						// Also take into account Delta which encodes the number of 'notches' the mouse
						// wheel has been rotated.
						// 
						int mouseWheelIncrements = Math.Max(1, Math.Abs( e.Delta ) / SystemInformation.MouseWheelScrollDelta);
						scrollLines *= mouseWheelIncrements;

                        for (int i = 0; i < scrollLines; i++)
                        {
                            if (e.Delta < 0)
                                rsr.Scroll(RowScrollAction.LineDown);
                            else
                                rsr.Scroll(RowScrollAction.LineUp);
                        }

                        // MRS 11/17/2008 - TFS10297
                        handled = true;
                    }
                    finally
                    {
                        rsr.dontUpdateInOnScroll = origDontUpdateInOnScroll;
                    }
                    // --------------------------------------------------------------------------
                }
            }
            finally
            {
                // JAS BR04593 7/8/05 - The call to the base implementation has been 
                // moved to the beginning of this method.
                //
                #region Removed
                //				// SSP 5/8/05 - NAS 5.2 Filter Row
                //				// Filter cell can drop down two, operator and operand value lists. Prevent
                //				// both from processing the mouse wheel.
                //				//
                //				//base.OnMouseWheel( e );
                //				if ( callBaseImplementation )
                //					base.OnMouseWheel( e );
                #endregion // Removed

                // MRS 10/3/06 - BR16260
                if (handledArgs != null)
                {
                    // MRS 10/18/07 - BR27522
                    //handledArgs.Handled = false;
                    // MRS 11/17/2008 - TFS10297
                    //handledArgs.Handled = true;
                    handledArgs.Handled = handled;
                }
            }			
		}
		#endregion OnMouseWheelInternal

		// RobA UWG739 11/19/01 Moved this from UltraGrid to UltraGridBase so that 
		// the dropdown could make use of the mouse wheel.
		//
		/// <summary>
		/// Fires when the mouse wheel is used when the cursor is positioned over the control
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		protected override void OnMouseWheel( MouseEventArgs e )
		{
			// JAS BR04593 7/8/05 - Originally the base implementation was being conditionally called 
			// at the end of this method.  The base version was not called if the user scrolls a filter
			// row dropdown.  It was not being called in that situation because EditorWithCombos in filter cells 
			// are hooked into the MouseWheel event.  The problem with that lies in the fact that a filter cell
			// can contain two dropdown editors and they were both scrolling when either one received a MouseWheel
			// message.  The original fix for that bug was to scroll the dropdown which was actually dropped down
			// and not call the base implementation (hence, preventing the MouseWheel event from being fired).
			//
			// This new implementation calls the base version immediately and unconditionally.  Calling the base
			// immediately is required for the CLR2-specific HandledMouseEventArgs to be used properly, because
			// now client code can prevent further processing of the MouseWheel message.  The EditorWithCombo
			// scrolling issue has been circumvented via a new method on EmbeddabeEditorOwnerInfoBase called
			// CanProcessMouseWheel.  Before the EditorWithCombo processes a MouseWheel message it checks with its
			// owner to see if it is allowed to do so.  In the case of the FilterOperatorEmbeddableEditorOwnerInfo
			// the editor will only be sanctioned to process the message when it is dropped down.
			// ---------------------------------------------------------------------------------------------------
			#region Removed
//			// SSP 5/8/05 - NAS 5.2 Filter Row
//			// Filter cell can drop down two, operator and operand value lists. Prevent
//			// both from processing the mouse wheel.
//			//
//			bool callBaseImplementation = true;
			#endregion // Removed

		    base.OnMouseWheel( e );

            // MRS 2/22/06 - BR10236
            // Moved this into OnMouseWheelInternal
            //
            this.OnMouseWheelInternal(e);            

            #region Old code

//#if CLR2
//                // JAS BR04593 7/8/05
//                // In CLR v2 it is possible to prevent the MouseWheel message from entering the DefWndProc
//                // by casting the MouseEventArgs to a HandledMouseEventArgs and setting the Handled property
//                // to true.  This prevents the control's parent from receiving the MouseWheel message.
//                HandledMouseEventArgs handledArgs = e as HandledMouseEventArgs;
//                if( handledArgs != null )
//                {
//                    if( handledArgs.Handled )
//                        return;

//                    handledArgs.Handled = true;
//                }
//#endif // CLR2
//            // ---------------------------------------------------------------------------------------------------

//            try
//            { 
//                // AS 11/17/03 UWG2751
//                //
//                //UIElement element = this.DisplayLayout.UIElement.ElementFromPoint( new Point( e.X, e.Y ) );
//                Control ctrl = this.ControlForGridDisplay;
//                Point pt = new Point(e.X, e.Y);

//                if (ctrl != this && 
//                    ctrl != null && 
//                    !ctrl.IsDisposed &&
//                    !this.IsDisposed )
//                {
//                    pt = this.PointToScreen(pt);
//                    pt = ctrl.PointToClient(pt);
//                }

//                UIElement element = this.DisplayLayout.UIElement.ElementFromPoint( pt );

//                // RobA UWG739 11/19/01 
//                // If this is a dropdown scroll the dropdown instead of the grid
//                //
//                // SSP 5/1/03 - Cell Level Editor
//                //
//                /*
//                if ( this.DisplayLayout.ActiveCell != null && 
//                    this.DisplayLayout.ActiveCell.Column.ValueList != null &&
//                    this.DisplayLayout.ActiveCell.Column.ValueList.IsDroppedDown )
//                {
//                    UltraDropDown dropDown = this.DisplayLayout.ActiveCell.Column.ValueList as UltraDropDown;
//                */
//                UltraGridCell activeCell = this.DisplayLayout.ActiveCell;
//                // SSP 5/1/03 - Cell Level Editor
//                // Added Editor and ValueList properties off the cell so the user can set the editor
//                // and value list on a per cell basis.
//                //
//                //IValueList valueList = null != activeCell ? activeCell.Column.ValueList : null;
//                IValueList valueList = null != activeCell ? activeCell.ValueListResolved : null;

//                // MRS 2/17/06 - BR09325				
//                // If the editor for this cell is dropped down, then there is no way
//                // for us to know if it handled the mouse wheel. 
//                // So if there's no ValueList on this cell and there is an editor
//                // and the editor is in edit mode and dropped down, we will assume
//                // that the editor did handle the mouse wheel and so we will not
//                // scroll the grid. 
//                if (valueList == null)
//                {
//                    EmbeddableEditorBase editor = null != activeCell ? activeCell.EditorResolved as EditorWithCombo: null;
//                    if (editor != null &&
//                        editor.IsInEditMode &&
//                        editor.SupportsDropDown &&
//                        editor.IsDroppedDown)
//                    {
//                        return;
//                    }
//                }

//                // SSP 5/8/05 - NAS 5.2 Filter Row
//                // Filter cell can drop down two, operator and operand value lists. Prevent
//                // both from processing the mouse wheel.
//                //
//                if ( activeCell is UltraGridFilterCell && activeCell.IsInEditMode )
//                {
//                    // JAS BR04593 7/8/05 - This flag is not needed.
//                    //
//                    //callBaseImplementation = false;

//                    // SSP 5/19/05 BR03866
//                    //
//                    if ( ( null == valueList || ! valueList.IsDroppedDown ) 
//                        && FilterOperatorLocation.Hidden != activeCell.Column.FilterOperatorLocationResolved )
//                    {
//                        IValueList operatorValueList = activeCell.Column.FilterOperatorEditorOwnerInfo.GetValueList( activeCell );
//                        if ( null != operatorValueList && operatorValueList.IsDroppedDown )
//                            valueList = operatorValueList;
//                    }
//                }

//                if ( null != valueList && valueList.IsDroppedDown )
//                {
//                    // SSP 10/15/03 UWG2711
//                    // If the ValueList is an UltraCombo which derives from UltraDropDownBase, then following
//                    // won't work. We need to check for UltraDropDownBase instead of UltraDropDown.
//                    //
//                    //UltraDropDown dropDown = valueList as UltraDropDown;
//                    UltraDropDownBase dropDown = valueList as UltraDropDownBase;

//                    if ( dropDown != null )
//                    {
//                        // JJD 1/24/02 - UWG990
//                        // Don't scroll if the scrollbar isn't visible and enabled
//                        //
//                        if ( dropDown.ActiveRowScrollRegion.ScrollBarInfo.Visible &&
//                            dropDown.ActiveRowScrollRegion.ScrollBarInfo.EnabledResolved )
//                        {
//                            if ( e.Delta < 0 )
//                                dropDown.ActiveRowScrollRegion.Scroll( RowScrollAction.LineDown );
//                            else
//                                dropDown.ActiveRowScrollRegion.Scroll( RowScrollAction.LineUp );
//                        }
//                    }

//                    return;
//                }

//                // Don't scroll the grid if the swapdropdown is droppeddown
//                //
//                if ( this.DisplayLayout.Grid is UltraGrid )
//                {
//                    UltraGrid grid = this.DisplayLayout.Grid as UltraGrid;

//                    // SSP 12/17/03
//                    // Instead of just returning, scroll the value list that's dropped down.
//                    //
//                    // --------------------------------------------------------------------------------
//                    /*
//                    if ( grid.HasSwapDropDown &&
//                         ((IValueList)grid.SwapDropDown).IsDroppedDown )
//                        return;
//                    */
//                    if ( grid.HasSwapDropDown && ((IValueList)grid.SwapDropDown).IsDroppedDown )
//                    {
//                        grid.SwapDropDown.ProcessOnMouseWheel( e );
//                        return;
//                    }
//                    else if ( grid.HasFilterDropDown && ((IValueList)grid.FilterDropDown).IsDroppedDown )
//                    {
//                        grid.FilterDropDown.ProcessOnMouseWheel( e );
//                        return;
//                    }
//                    // --------------------------------------------------------------------------------
//                }

//                //	BF 11.8.04	NAS2005 Vol1 - CardView vertical scrolling
//                //
//                //	If the wheel is moved while the  cursor is positioned over
//                //	a CardAreaUIElement, let the CardAreaUIElement handle it.
//                //
//                if ( element != null )
//                {
//                    CardAreaUIElement cardAreaElement = element.GetAncestor( typeof(CardAreaUIElement) ) as CardAreaUIElement;
//                    if ( cardAreaElement != null && cardAreaElement.Rect.Contains(pt) )
//                    {
//                        //	The rest of the cases in this method return after the
//                        //	wheel movement is processed, so we will return here
//                        //	if the OnMouseWheel method returns true. This will cause
//                        //	the wheel movement to be either processed by the CardAreaUIElement,
//                        //	or the RowScrollRegion (see below), but not both.
//                        //
//                        if ( cardAreaElement.OnMouseWheel(e) )
//                            return;
//                    }
//                }

//                // AS 11/17/03 UWG2751
//                // If the mouse is elsewhere, use the active row scroll region.
//                //
//                //if ( element == null )
//                //	return;

//                //RowScrollRegion rsr = element.GetContext( typeof( RowScrollRegion ) ) as RowScrollRegion;
//                RowScrollRegion rsr = null;

//                if (element != null)
//                    rsr = element.GetContext( typeof( RowScrollRegion ) ) as RowScrollRegion;

//                // AS 11/17/03 UWG2751
//                // If the mouse is elsewhere, use the active row scroll region.
//                //
//                if (rsr == null)
//                    rsr = this.ActiveRowScrollRegion;

//                //	If the rsr is null return
//                if ( rsr == null )
//                    return;

//                // JJD 1/24/02 - UWG990
//                // Don't scroll if the scrollbar isn't visible and enabled
//                //
//                if ( rsr.ScrollBarInfo.Visible &&
//                    rsr.ScrollBarInfo.EnabledResolved )
//                {
//                    // SSP 10/27/03 UWG2463
//                    // Honor the MouseWheelScrollLines settings.
//                    // Commented out the original code and added the new code below.
//                    //
//                    // --------------------------------------------------------------------------
//                    /*
//                    if ( e.Delta < 0 )
//                        rsr.Scroll( RowScrollAction.LineDown );
//                    else
//                        rsr.Scroll( RowScrollAction.LineUp );
//                    */
//                    int scrollInes = Math.Max( 1, SystemInformation.MouseWheelScrollLines );
					
//                    bool origDontUpdateInOnScroll = rsr.dontUpdateInOnScroll;
//                    if ( scrollInes > 1 )
//                        rsr.dontUpdateInOnScroll = true;

//                    try
//                    {
//                        for ( int i = 0; i < scrollInes; i++ )
//                        {
//                            if ( e.Delta < 0 )
//                                rsr.Scroll( RowScrollAction.LineDown );
//                            else
//                                rsr.Scroll( RowScrollAction.LineUp );
//                        }
//                    }
//                    finally
//                    {
//                        rsr.dontUpdateInOnScroll = origDontUpdateInOnScroll;
//                    }
//                    // --------------------------------------------------------------------------
//                }
//            }
//            finally
//            {
//                // JAS BR04593 7/8/05 - The call to the base implementation has been 
//                // moved to the beginning of this method.
//                //
//                #region Removed
////				// SSP 5/8/05 - NAS 5.2 Filter Row
////				// Filter cell can drop down two, operator and operand value lists. Prevent
////				// both from processing the mouse wheel.
////				//
////				//base.OnMouseWheel( e );
////				if ( callBaseImplementation )
////					base.OnMouseWheel( e );
//                #endregion // Removed
            //            }
            #endregion Old Code

        }

		/// <summary>
		/// Since we are using the control's text property for our caption property
		/// we need to update the grid when a user changes the text within the property 
		/// window
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected override void OnTextChanged( System.EventArgs e)
		{
			//RobA UWG395 10/2/01
			this.DisplayLayout.DirtyGridElement();

			base.OnTextChanged(e);
		}

		

		/// <summary>
		/// Determines if the control should receive mouse notifications at design time for the specified point.
		/// </summary>
		/// <param name="pt">Point in client coordinates.</param>
		/// <returns>True if the control should receive any mouse notifications for the specified point.</returns>
		protected override bool IsPointDesignTimeActive( System.Drawing.Point pt )
		{
			UIElement element = this.DisplayLayout.UIElement.ElementFromPoint( pt );

			if (element != null)
			{
				if ( element.GetAncestor(typeof(Infragistics.Win.UltraWinGrid.BandHeadersUIElement)) != null ||
					element.GetAncestor(typeof(Infragistics.Win.UltraWinGrid.HeaderUIElement)) != null ||
					element is Infragistics.Win.UltraWinGrid.ColSplitBoxUIElement ||
					element is Infragistics.Win.UltraWinGrid.ColSplitterBarUIElement || 
					element is Infragistics.Win.UltraWinGrid.ExpansionIndicatorUIElement ||
					element.GetAncestor(typeof(Infragistics.Win.UltraWinGrid.GroupByButtonUIElement)) != null ||
					element is Infragistics.Win.UltraWinGrid.GroupByRowExpansionIndicatorUIElement ||
					element is Infragistics.Win.UltraWinGrid.RowSplitBoxUIElement ||
					element is Infragistics.Win.UltraWinGrid.RowSplitterBarUIElement ||
					element is Infragistics.Win.SplitterIntersectionUIElement ||
					element is Infragistics.Win.UltraWinGrid.SwapButtonUIElement )
				{
					return true;
				}
			
			}

			return base.IsPointDesignTimeActive(pt);
		}
	

		
		// SSP 5/3/02
		// Added this internal property mainly because GetService is protected.
		//
		#region DesignerHost

		internal IDesignerHost DesignerHost 
		{ 
			get 
			{ 
				return this.GetService( typeof( IDesignerHost ) ) as IDesignerHost; 
			} 
		}

		#endregion DesignerHost


		// SSP 8/1/02 UWG1454
		// The same applies to the combo as well. So moved this in the UltraGridBase
		// class so that it would work for all the UltraGridBase derived controls 
		// including UltraGrid and UltraCombo.
		//
		// SSP 4/8/02
		// Overrode IsInputChar to return true always because if the char
		// happens to be used for mnemonic of a button, it will call the
		// key press on the button even when the Alt key is not down.
		//
		/// <summary>
		/// Determines if a character is an input character that the control recognizes.
		/// </summary>
        /// <param name="charCode">The character to test.</param>
        /// <returns>true if the character should be sent directly to the control and not preprocessed; otherwise, false.</returns>
		protected override bool IsInputChar( char charCode )
		{
			//	JJD / AS / BF	5.28.02
			//	If the ALT key is down, don't return true. JoeD suspects that processing
			//	mnemonics when ALT is not pressed is a bug, and if it is fixed, we will be
			//	messing up the way mnemonics work by always returning true
			if ( ( Control.ModifierKeys & Keys.Alt ) == Keys.Alt )
				return false;

			return true;
		}


		// SSP 8/7/02 UWG1417
		// Overrode OnEnabledChanged so when the user enables or disables the
		// grid we can invalidate the grid.
		//
		/// <summary>
		/// Overridden. Raises the EnabledChanged event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		/// <remarks>
		/// <seealso cref="Control.OnEnabledChanged"/>
		/// </remarks>
		protected override void OnEnabledChanged( EventArgs e )
		{
			base.OnEnabledChanged( e );

			if ( null != this.layout )
			{
				// SSP 7/22/05 BR05103
				// 
				this.layout.BumpCellChildElementsCacheVersion( );

				this.layout.DirtyGridElement( );
			}
		}

		// AS 3/5/03 UWG2003
		#region OnBeginInit/OnEndInit
		/// <summary>
		/// Invoked during the <see cref="ISupportInitialize.BeginInit"/> of the component.
		/// </summary>
		protected virtual void OnBeginInit()
		{
		}

		/// <summary>
		/// Invoked during the <see cref="ISupportInitialize.EndInit"/> of the component.
		/// </summary>
		protected virtual void OnEndInit()
		{
		}
		#endregion //OnBeginInit/OnEndInit

		#region Implementation of Design.IGridDesignInfo

		// SSP 2/11/03 - Row Layout Functionality
		// Added IGridDesignInfo interface so the designers in design assembly can access
		// internal properties from the grid via this interface without having to expose them 
		// to the user.
		//
		Infragistics.Win.Layout.GridBagLayoutManager Design.IGridDesignInfo.GetCellAreaGridBagLayoutManager( UltraGridBand band )
		{
			// SSP 6/24/03 - Run-time row layout designer
			// Run-time row layout designer requres that we return a valid layout manager.
			//
			//if ( this.DesignMode )
			return band.RowLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;				

			//return null;
		}

		Infragistics.Win.Layout.GridBagLayoutManager Design.IGridDesignInfo.GetHeaderAreaGridBagLayoutManager( UltraGridBand band )
		{
			// SSP 6/24/03 - Run-time row layout designer
			// Run-time row layout designer requres that we return a valid layout manager.
			//
			//if ( this.DesignMode )
			return band.HeaderLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;            

			//return null;
		}

		// SSP 8/7/03 - Excel Exporting
		// Added GetSummaryAreaGridBagLayoutManager because the excel exporting code needs it.
		//
		Infragistics.Win.Layout.GridBagLayoutManager Design.IGridDesignInfo.GetSummaryAreaGridBagLayoutManager( UltraGridBand band )
		{
			return band.SummaryLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;
		}

		// SSP 8/6/03 UWG2567
		// Added EnsureColScrollRegionMetricsInitialized method to the interface.
		// The row layout designer needs a way to ensure that the col scroll regions'
		// metrics is initialized.
		//
		void Design.IGridDesignInfo.EnsureColScrollRegionMetricsInitialized( )
		{
			if ( null != this.layout && ! this.layout.Disposed && this.layout.ColScrollRegionsAllocated )
				this.layout.ColScrollRegions.InitializeMetrics( );
		}

		// SSP 3/10/04 - Designer Usability Improvements Related
		// Added ExtractDataStructure method to be able to extract the data structure from
		// a data source.
		//
		void Design.IGridDesignInfo.ExtractDataStructure( object dataSource, string dataMember )
		{
			this.SetDataBinding( null, null );
			this.DisplayLayout.Reset( );
			this.dataSource = dataSource;
			this.dataMember = dataMember;
			try
			{
				this.DisplayLayout.ExtractDataStructure( dataSource, dataMember );
			}
			finally
			{
				this.dataSource = null;
				this.dataMember = null;
			}
		}

		// SSP 7/30/04 UWG3488
		// Added IGridDesignInfo.DesignMode property to force the grid to bahave
		// as if it were in design mode. This is used by the ultragrid designer dialogs
		// to show two dummy rows in the preview grid.
		//
		bool Design.IGridDesignInfo.DesignMode 
		{ 
			get
			{
				return this.forceDesignMode;
			}
			set
			{
				this.forceDesignMode = value;
			}
		}

        // MRS v7.2 - Document Exporter
        /// <summary>
        /// Gets the cell area layout manager for the passed in row.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        Infragistics.Win.Layout.GridBagLayoutManager Design.IGridDesignInfo.GetCellAreaGridBagLayoutManager(UltraGridRow row)
        {
            return row.LayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;
        }

        // MRS v7.2 - Document Exporter
        /// <summary>
        /// Returns the Owner for the editor of the specified row and column.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        EmbeddableEditorOwnerBase Design.IGridDesignInfo.GetEditorOwner(UltraGridRow row, UltraGridColumn column)
        {
            return row.GetEditorOwnerInfo(column);
        }

        // MRS v7.2 - Document Exporter
        /// <summary>
        /// Gets the height of a Summary Value from it's LayoutItem. This only works for a Summary in 
        /// RowLayout mode and a summary that is positioned using a SummaryPositionColumn (not free-form).
        /// </summary>
        /// <param name="summaryLayoutItem">The SummaryLayoutItem of the SummaryPositionColumn for the summary.</param>
        /// <param name="level">The level of the summary.</param>
        /// <returns></returns>
        int Design.IGridDesignInfo.GetSummaryHeight(ILayoutItem summaryLayoutItem, int level)
        {
            SummaryLayoutItem typedSummaryLayoutItem = summaryLayoutItem as SummaryLayoutItem;
            if (typedSummaryLayoutItem == null)
                throw new ArgumentException("summaryLayoutItem must be a SummaryLayoutItem");

            return typedSummaryLayoutItem.SummaryHeights[level];
        }

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

        Infragistics.Win.Layout.GridBagLayoutManager Design.IGridDesignInfo.GetCellAreaHierarchicalGridBagLayoutManager(ILayoutGroup group)
        {
            UltraGridGroup gridGroup = group as UltraGridGroup;
            if (gridGroup != null)
                return gridGroup.RowLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;

            UltraGridBand band = group as UltraGridBand;
            if (band != null)
                return band.groupedGridBagRowLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;

            return null;
        }

        Infragistics.Win.Layout.GridBagLayoutManager Design.IGridDesignInfo.GetHeaderAreaHierarchicalGridBagLayoutManager(ILayoutGroup group)
        {
            UltraGridGroup gridGroup = group as UltraGridGroup;
            if (gridGroup != null)
                return gridGroup.HeaderLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;

            UltraGridBand band = group as UltraGridBand;
            if (band != null)
                return band.groupedGridBagHeaderLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;

            return null;
        }

        Infragistics.Win.Layout.GridBagLayoutManager Design.IGridDesignInfo.GetSummaryAreaHierarchicalGridBagLayoutManager(ILayoutGroup group)
        {
            UltraGridGroup gridGroup = group as UltraGridGroup;
            if (gridGroup != null)
                return gridGroup.SummaryLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;

            UltraGridBand band = group as UltraGridBand;
            if (band != null)
                return band.groupedGridBagSummaryLayoutManager as Infragistics.Win.Layout.GridBagLayoutManager;

            return null;
        }

        #endregion // NAS 9.1 - Groups in RowLayout
        
        #endregion // Implementation of Design.IGridDesignInfo

        // MRS 5/20/2009 - TFS17800
        #region GetSummaryLayoutManager
        GridBagLayoutManager Design.IGridDesignInfo.GetSummaryRowLayoutManager(UltraGridSummaryRow summaryRow)
        {
            return summaryRow.Band.GetSummaryLayoutManager(summaryRow.SummaryDisplayAreaContext) as GridBagLayoutManager;
        }
        #endregion // GetSummaryLayoutManager

        #region IsSerializing

        // SSP 5/28/03 UWG2270
		// Added DesignerSerializer attribute. This is to fix this problem with the grid on a base
		// form and you have a derived form and properties are not retained in the derived form.
		// Added IsSerializing proeprty.
		//
		internal bool IsSerializing
		{
			get
			{
				return this.isSerializing;
			}
		}

		#endregion // IsSerializing

		// SSP 5/28/03 UWG2270
		// Added DesignerSerializer attribute. This is to fix this problem with the grid on a base
		// form and you have a derived form and properties are not retained in the derived form.
		// Implemented ICodeDomSerializable interface.
		//
		#region Implementation of ICodeDomSerializable
		
		void ICodeDomSerializable.BeforeSerialize(Infragistics.Shared.Serialization.CodeDomSerializationInfo serializationInfo)
		{
			this.isSerializing = true;
		}

		void ICodeDomSerializable.AfterSerialize(Infragistics.Shared.Serialization.CodeDomSerializationInfo serializationInfo)
		{
			this.isSerializing = false;
		}

		void ICodeDomSerializable.AfterDeserialize(System.ComponentModel.Design.Serialization.IDesignerSerializationManager manager)
		{
		}

		#endregion // Implementation of ICodeDomSerializable


		// SSP 6/6/03 UWG2232
		// Added SuspendRowSynchronization and ResumeRowSynchronization methods.
		//
		#region SuspendRowSynchronization

		/// <summary>
		/// Suspends row syncronization.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>SuspendRowSynchronization</b> and <b>ResumeRowSynchronization</b>
		/// methods can be used to temporarily suspend UltraGrid from responding
		/// to data source change notifications. When row syncrhonization is
		/// suspended, the UltraGrid will still mark the rows dirty so it will 
		/// re-create the rows next time it gets painted.
		/// </p>
		/// <seealso cref="RowSynchronizationSuspended"/>
		/// <seealso cref="ResumeRowSynchronization"/>
		/// </remarks>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		public void SuspendRowSynchronization( )
		{
			this.rowSynchronizationSuspended = true;
		}

		#endregion // SuspendRowSynchronization

		#region ResumeRowSynchronization

		/// <summary>
		/// Resumes row syncronizations.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>SuspendRowSynchronization</b> and <b>ResumeRowSynchronization</b>
		/// methods can be used to temporarily suspend UltraGrid from responding
		/// to data source change notifications. When row syncrhonization is
		/// suspended, the UltraGrid will still mark the rows dirty so it will 
		/// re-create the rows next time it gets painted.
		/// </p>
		/// <seealso cref="SuspendRowSynchronization"/>
		/// <seealso cref="RowSynchronizationSuspended"/>
		/// </remarks>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		public void ResumeRowSynchronization( )
		{
			this.rowSynchronizationSuspended = false;
		}

		#endregion // ResumeRowSynchronization

		#region RowSynchronizationSuspended
		
		/// <summary>
		/// Returns true if the user has suspended row syncronization by calling SuspendRowSynchronization.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>SuspendRowSynchronization</b> and <b>ResumeRowSynchronization</b>
		/// methods can be used to temporarily suspend UltraGrid from responding
		/// to data source change notifications. When row syncrhonization is
		/// suspended, the UltraGrid will still mark the rows dirty so it will 
		/// re-create the rows next time it gets painted.
		/// </p>
		/// <seealso cref="SuspendRowSynchronization"/>
		/// <seealso cref="ResumeRowSynchronization"/>
		/// </remarks>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		[ Browsable( false ) ]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public bool RowSynchronizationSuspended
		{
			get
			{
				return this.rowSynchronizationSuspended;
			}
		}

		#endregion // RowSynchronizationSuspended

		// SSP 6/17/04 UWG3207
		// Added a way to suspend dirtying of summaries when we receive ItemChanged
		// notification. Added SuspendSummaryUpdates, SummaryUpdatesSuspended
		// and ResumeSummaryUpdates.
		//
		#region SuspendSummaryUpdates
		
		/// <summary>
		/// Suspends updating of summaries. Whenever UltraGrid receives notification indicating a row has changed, UltraGrid marks summaries for recalculation. Calling this method will prevent UltraGrid from marking summaries for recalculation whenever row changed notification is received. You must call <see cref="ResumeSummaryUpdates"/> to resume the default behavior of marking summaries for recalculation.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>SuspendSummaryUpdates</b> and <b>ResumeSummaryUpdates</b> methods can be used
		/// to temporarily prevent the UltraGrid from responding to cell change notifications
		/// and marking the summaries dirty. Summaries are re-calculated lazily however this
		/// method is useful when you are modifying a lots of cells and for efficiency reaons
		/// what to prevent UltraGrid from responding to every cell change notification. In
		/// that case you can call <i>SuspendSummaryUpdates</i> and change the cell values and
		/// call <i>ResumeSummaryUpdates</i>. <i>ResumeSummaryUpdates</i> method takes in
		/// <b>recalculateSummaries</b> parameter that you can use to specify whether the
		/// UltraGrid should recalculate all the summaries or not. You would specify that
		/// parameter as <b>false</b> if you know that none of the changes you made would 
		/// affect the summaries. Otherwise you would specifiy that parameter as <b>true</b>.
		/// </p>
		/// <seealso cref="ResumeSummaryUpdates"/>
		/// <seealso cref="SummaryUpdatesSuspended"/>
		/// <seealso cref="UltraControlBase.BeginUpdate"/>
		/// <seealso cref="UltraControlBase.EndUpdate(bool)"/>
		/// </remarks>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		public void SuspendSummaryUpdates( )
		{
			this.summaryUpdatesSuspended = true;
		}

		#endregion // SuspendSummaryUpdates

		#region SummaryUpdatesSuspended
		
		/// <summary>
		/// Indicates whether summary updates have been suspended.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>SuspendSummaryUpdates</b> and <b>ResumeSummaryUpdates</b> methods can be used
		/// to temporarily prevent the UltraGrid from responding to cell change notifications
		/// and marking the summaries dirty. Summaries are re-calculated lazily however this
		/// method is useful when you are modifying a lots of cells and for efficiency reaons
		/// what to prevent UltraGrid from responding to every cell change notification. In
		/// that case you can call <i>SuspendSummaryUpdates</i> and change the cell values and
		/// call <i>ResumeSummaryUpdates</i>. <i>ResumeSummaryUpdates</i> method takes in
		/// <b>recalculateSummaries</b> parameter that you can use to specify whether the
		/// UltraGrid should recalculate all the summaries or not. You would specify that
		/// parameter as <b>false</b> if you know that none of the changes you made would 
		/// affect the summaries. Otherwise you would specifiy that parameter as <b>true</b>.
		/// </p>
		/// <seealso cref="SuspendSummaryUpdates"/>
		/// <seealso cref="ResumeSummaryUpdates"/>
		/// <seealso cref="UltraControlBase.BeginUpdate"/>
        /// <seealso cref="UltraControlBase.EndUpdate(bool)"/>
		/// </remarks>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		[ Browsable( false ) ]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public bool SummaryUpdatesSuspended
		{
			get
			{
				return this.summaryUpdatesSuspended;
			}
		}

		#endregion // SummaryUpdatesSuspended

		#region ResumeSummaryUpdates

		/// <summary>
		/// Resumes the default behavior of marking summaries for recalculations whenever row changed notification is received.
		/// </summary>
		/// <param name="recalculateSummaries">
		/// Specifies whether the
		/// UltraGrid should recalculate all the summaries or not. You would specify that
		/// parameter as <b>false</b> if you know that none of the changes you made 
		/// affected the summaries. Otherwise you would specifiy that parameter as <b>true</b>.
		/// </param>
		/// <remarks>
		/// <p class="body">
		/// <b>SuspendSummaryUpdates</b> and <b>ResumeSummaryUpdates</b> methods can be used
		/// to temporarily prevent the UltraGrid from responding to cell change notifications
		/// and marking the summaries dirty. Summaries are re-calculated lazily however this
		/// method is useful when you are modifying a lots of cells and for efficiency reasons
		/// you want to prevent UltraGrid from responding to every cell change notification. In
		/// that case you can call <i>SuspendSummaryUpdates</i> and change the cell values and
		/// call <i>ResumeSummaryUpdates</i>. <i>ResumeSummaryUpdates</i> method takes in
		/// <b>recalculateSummaries</b> parameter that you can use to specify whether the
		/// UltraGrid should recalculate all the summaries or not. You would specify that
		/// parameter as <b>false</b> if you know that none of the changes you made  
		/// affected the summaries. Otherwise you would specifiy that parameter as <b>true</b>.
		/// </p>
		/// <seealso cref="SuspendSummaryUpdates"/>
		/// <seealso cref="SummaryUpdatesSuspended"/>
		/// <seealso cref="UltraControlBase.BeginUpdate"/>
		/// <seealso cref="UltraControlBase.EndUpdate(bool)"/>
		/// </remarks>

		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		public void ResumeSummaryUpdates( bool recalculateSummaries )
		{
			if ( this.summaryUpdatesSuspended )
			{
				this.summaryUpdatesSuspended = false;

				if ( recalculateSummaries && null != this.layout )
				{
					this.layout.BumpSummariesVersion( );

					// SSP 2/21/06 BR09421
					// We need to repaint the grid to reflect the newly recalculated summary values.
					// 
					this.layout.DirtyGridElement( );
				}
			}
		}

		#endregion // ResumeSummaryUpdates		

		// SSP 3/21/02
		// Added FilterRowEventHandler for row filtering feature in version 2
		//
		#region FilterRow

		/// <summary>
		/// Occurs when row filters are evaluated on a row.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>FilterRow</b> event occurs when a row filters are evaluated on a row. Hidden property of the row will be set to appropriate value according to the results of the evaluation.</p>
		///	<p class="body">You can use the <b>FilteRow</b> event to do a custom filter evaluation on the row and set it's Hidden property to appropriate value.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_FilterRow")]
		public event FilterRowEventHandler FilterRow
		{
			add { this.EventsOptimized.AddHandler( EVENT_FILTERROW, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_FILTERROW, value );} 
		}

		#endregion // FilterRow

		#region OnFilterRow

		/// <summary>
		/// Called when row filters are evaluated on a row.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnFilterRow( FilterRowEventArgs e )
		{
			FilterRowEventHandler eDelegate = (FilterRowEventHandler)EventsOptimized[EVENT_FILTERROW];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion // OnFilterRow

		// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Removed. Use FireCommonEvent instead. 
		//		#region FireFilterRow
		//		
		//#if DEBUG
		//		/// <summary>
		//		/// Fires the FilterRow event.
		//		/// </summary>
		//		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		//#endif
		//		// SSP 11/21/03 UWG2749
		//		// Made the method into virtual so UltraGrid can override it to check if the 
		//		// event is enabled.
		//		//
		//		//internal void FireFilterRow( FilterRowEventArgs e )
		//		internal virtual void FireFilterRow( FilterRowEventArgs e )
		//		{
		//			this.OnFilterRow( e );
		//		}
		//
		//		#endregion // FireFilterRow

		#region DesignerChangeNotificationsDisabled

		// SSP 8/7/03
		// Added DesignerChangeNotificationsDisabled property which just delegates the 
		// calls to the base class.
		//
		internal new bool DesignerChangeNotificationsDisabled
		{
			get
			{
				return base.DesignerChangeNotificationsDisabled;
			}
			set
			{
				base.DesignerChangeNotificationsDisabled = value;
			}
		}

		#endregion // DesignerChangeNotificationsDisabled

		// JJD 12/15/03 - Added Accessibility support
		#region CreateAccessibilityInstance

		/// <summary>
		/// Creates a new accessibility object for the control.
		/// </summary>
		/// <returns>A new <see cref="AccessibleObject"/> for the control.</returns>
		protected override System.Windows.Forms.AccessibleObject CreateAccessibilityInstance()
		{
			return this.CreateAccessibilityInstance(this);
		}

		/// <summary>
		/// Creates an accessible object for the related object.
		/// </summary>
		/// <param name="relatedObject">The logically related object (e.g. an UltraGrid, UltraGridRow, ColumnHeader etc.).</param>
		/// <returns>A new <see cref="AccessibleObject"/> object for the related object.</returns>
		internal protected virtual System.Windows.Forms.AccessibleObject CreateAccessibilityInstance( object relatedObject )
		{
			AccessibleObject accessibilityObject = null;

			if ( relatedObject is UltraGridCell )
			{
				accessibilityObject = new UltraGridCell.CellAccessibleObject( relatedObject as UltraGridCell ).AccessibleObject;
			}
			else if ( relatedObject is UltraGridRow )
			{
				accessibilityObject = new UltraGridRow.RowAccessibleObject( relatedObject as UltraGridRow ).AccessibleObject;
			}
			else if ( relatedObject is RowColRegionIntersectionUIElement )
			{
				accessibilityObject = new RowColRegionIntersectionUIElement.RowColRegionAccessibleObject( relatedObject as RowColRegionIntersectionUIElement ).AccessibleObject;
			}
			else if ( relatedObject is ColumnHeader.ColumnHeaderAccessibleObject )
			{
				accessibilityObject = (relatedObject as ColumnHeader.ColumnHeaderAccessibleObject).AccessibleObject;
			}
			else if ( relatedObject is GroupHeader.GroupHeaderAccessibleObject )
			{
				accessibilityObject = (relatedObject as GroupHeader.GroupHeaderAccessibleObject).AccessibleObject;
			}
			else if ( relatedObject is BandHeader.BandHeaderAccessibleObject )
			{
				accessibilityObject = (relatedObject as BandHeader.BandHeaderAccessibleObject).AccessibleObject;
			}
			else if ( relatedObject is HeadersCollection.HeadersAccessibleObject)
			{
				accessibilityObject = (relatedObject as HeadersCollection.HeadersAccessibleObject).AccessibleObject;
			}
			else if ( relatedObject == this )
			{
				accessibilityObject = new UltraGridBaseAccessibleObject(this);
			}
			else if ( relatedObject is CaptionAreaUIElement )
			{
				accessibilityObject = new CaptionAreaUIElement.CaptionAreaAccessibleObject( relatedObject as CaptionAreaUIElement ).AccessibleObject;
			}
			else if ( relatedObject is DataAreaUIElement )
			{
				accessibilityObject = new DataAreaUIElement.DataAreaAccessibleObject( relatedObject as DataAreaUIElement ).AccessibleObject;
			}
			else if ( relatedObject is AddNewRowButtonUIElement )
			{
				accessibilityObject = new AddNewRowButtonUIElement.AddNewButtonAccessibleObject( relatedObject as AddNewRowButtonUIElement ).AccessibleObject;
			}
			else if ( relatedObject is AddNewBoxUIElement )
			{
				accessibilityObject = new UIElementAccessibleObject( 
					relatedObject as UIElement,
					AccessibleRole.Grouping, 
					SR.GetString("AccessibleName_AddNewBoxArea") ).AccessibleObject;
			}
			else if ( relatedObject is GroupByButtonUIElement )
			{
				accessibilityObject = new GroupByButtonUIElement.GroupByButtonAccessibleObject( relatedObject as GroupByButtonUIElement ).AccessibleObject;
			}
			else if ( relatedObject is GroupByBoxUIElement )
			{
				accessibilityObject = new UIElementAccessibleObject( 
					relatedObject as GroupByBoxUIElement,
					AccessibleRole.Grouping, 
					SR.GetString("AccessibleName_GroupByBox") ).AccessibleObject;
			}
			else if ( relatedObject is GroupByBandLabelUIElement )
			{
				accessibilityObject = new GroupByBandLabelUIElement.GroupByBandlLabelAccessibleObject( relatedObject as GroupByBandLabelUIElement ).AccessibleObject;
			}
			else if ( relatedObject is GroupByBoxPromptUIElement )
			{
				accessibilityObject = new GroupByBoxPromptUIElement.GroupByPromptAccessibleObject( relatedObject as GroupByBoxPromptUIElement ).AccessibleObject;
			}
			else
			{
				if ( relatedObject == null )
					Debug.Fail( "null object type passed into UltraGridBase.CreateAccessibilityInstance." );
				else
					Debug.Fail( "Unknown object type passed into UltraGridBase.CreateAccessibilityInstance. type is " + relatedObject.GetType().ToString() );

			}

			return accessibilityObject;
		}

		#endregion CreateAccessibilityInstance

		// JJD 12/15/03 - Added accessibility support
		#region AccessibilityNotifyClientsInternal

		internal void AccessibilityNotifyClientsInternal(AccessibleEvents accEvent, int childID)
		{
			// Get out if everything isn't in order
			if (this.IsDisposed ||
				this.Disposing	||
				!this.IsHandleCreated )
				return;

			this.AccessibilityNotifyClients( accEvent, childID );
		}
		#endregion AccessibilityNotifyClientsInternal

		// JJD 12/15/03 - Added Accessibility support
		#region Public Class UltraGridBaseAccessibleObject

		/// <summary>
		/// The Accessible object for a grid.
		/// </summary>
		// AS 1/16/04 DNF143
		//protected class UltraGridBaseAccessibleObject : Control.ControlAccessibleObject
		protected class UltraGridBaseAccessibleObject : UltraControlAccessibleObject
		{
			#region Private Members

			private UltraGridBase grid;

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor
			/// </summary>
            /// <param name="grid">The UltraWinGrid this accessible object represents.</param>
			// AS 1/7/04
			// The combo will be using this for the
			// ComboDropDownControl - which displays the grid
			// in the dropdown window.
			//
			//public UltraGridBaseAccessibleObject ( UltraGridBase grid ) : base( grid )
			public UltraGridBaseAccessibleObject ( UltraGridBase grid ) : base( grid.ControlForGridDisplay )
			{
				this.grid = grid;
			}

			#endregion Constructor

			#region Base Class Overrides

				#region Bounds

			/// <summary>
			/// Gets the location and size of the accessible object.
			/// </summary>
			public override System.Drawing.Rectangle Bounds
			{
				get
				{
					return base.Bounds;
				}
			}

				#endregion Bounds

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				// AS 1/7/04
				// Use the element from the layout since
				// this won't work for the combo's dropdown portion.
				//
				//UIElement element = this.grid.ControlUIElement;
				UIElement element = this.grid.DisplayLayout.UIElement;

				if ( element == null )
					return null;

				// AS 1/16/04 DNF143
				// Only do this if the grid itself is focused.
				//AccessibleObject focusedObject = this.GetFocused();
				//
				//if ( focusedObject != null )
				if (this.Owner.Focused)
				{
					AccessibleObject focusedObject = this.GetFocused();

					if ( index == 0 )
					{
						element.VerifyChildElements();
						Debug.WriteLine("Get focused object state");
						return focusedObject;
					}

					index--;
				}

				UIElement child;

				for ( int i = 0; i < element.ChildElements.Count; i ++ )
				{
					child = element.ChildElements[i];

					if ( child.IsAccessibleElement )
					{
						if ( index == 0 )
							return child.AccessibilityInstance;

						index--;
					}
				}

				// Loop over the bands to account for fixed band headers
				foreach ( UltraGridBand band in this.grid.DisplayLayout.SortedBands )
				{
					if ( band.ColHeadersAccessibilityObject != null )
					{
						if ( index == 0 )
							return band.ColHeadersAccessibilityObject;

						index--;
					}
				}
 
				// Check if the tenplate add row is on the top
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.grid.DisplayLayout.Rows.TemplateRowLocationDefault == TemplateAddRowLocation.Top )
				TemplateAddRowLocation templateRowLocationDefault = this.grid.DisplayLayout.Rows.TemplateRowLocationDefault;

				if ( templateRowLocationDefault == TemplateAddRowLocation.Top )
				{
					if ( index == 0 )
						return this.grid.DisplayLayout.Rows.TemplateAddRow.AccessibilityObject;

					index--;
				}

				if ( index >=0 && index < this.grid.DisplayLayout.Rows.Count )
					return this.grid.DisplayLayout.Rows[index].AccessibilityObject;
 
				index -= this.grid.DisplayLayout.Rows.Count;

				// Check if the template add row is on the bottom
				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this.grid.DisplayLayout.Rows.TemplateRowLocationDefault == TemplateAddRowLocation.Bottom )
				if ( templateRowLocationDefault == TemplateAddRowLocation.Bottom )
				{
					if ( index == 0 )
						return this.grid.DisplayLayout.Rows.TemplateAddRow.AccessibilityObject;

					index--;
				}

				if ( this.grid.DisplayLayout.SortedBands.Count > 0 )
				{
					AccessibleObject acc = this.grid.DisplayLayout.SortedBands[0].CardScrollbarAccessibilityObject;

					if ( acc != null )
					{
						if ( index == 0 )
							return acc;

						index--;
					}
				}
 
				// AS 1/16/04 DNF143
				//return null;
				return base.GetChild(index);
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{
				int count = 0;

				// AS 1/7/04
				// Use the element from the layout since
				// this won't work for the combo's dropdown portion.
				//
				//UIElement element = this.grid.ControlUIElement;
				UIElement element = this.grid.DisplayLayout.UIElement;

				if ( element == null )
					return 0;


				// AS 1/16/04 DNF143
				// The GetFocused could return a child control.
				//
				//AccessibleObject focusedObject = this.GetFocused();

				// AS 1/16/04 DNF143
				//if ( focusedObject != null )
				if (this.Owner.Focused)
					count++;

				UIElement child;

				for ( int i = 0; i < element.ChildElements.Count; i ++ )
				{
					child = element.ChildElements[i];

					if ( child.IsAccessibleElement )
						count++;
				}

				// Loop over the bands and add one for each band
				// that displays fixed headers 
				foreach ( UltraGridBand band in this.grid.DisplayLayout.SortedBands )
				{
					if ( band.ColHeadersAccessibilityObject != null )
						count++;
					
					if ( band.CardScrollbarAccessibilityObject != null )
						count++;
				}

				// add the band zero row count
				count += this.grid.DisplayLayout.Rows.Count;
 
				// add one for the template add row
				if ( this.grid.DisplayLayout.Rows.TemplateRowLocationDefault != TemplateAddRowLocation.None )
					count++;

				// AS 1/16/04 DNF143
				count += base.GetChildCount();

				return count;
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				if ( this.grid.Focused )
				{
					if ( this.grid.ActiveRow != null )
					{
						if ( this.grid is UltraGrid )
						{
							UltraGridCell cell = ((UltraGrid)(this.grid)).ActiveCell;

							if ( cell != null )
								return cell.AccessibilityObject.GetFocused();
						}

						return this.grid.ActiveRow.AccessibilityObject;
					}

					// AS 1/16/04 DNF143
					// If we have focus but we have no active rows,
					// return the control's accessible object.
					return this;
				}

				// AS 1/16/04 DNF143
				// Get the accessible object of the child control that contains
				// focus.
				//return null;
				return this.GetFocusedChildControl();
			}

				#endregion GetFocused

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				if ( this.grid.Focused )
				{
					if ( this.grid.ActiveRow != null )
					{
						if ( this.grid is UltraGrid )
						{
							UltraGridCell cell = ((UltraGrid)(this.grid)).ActiveCell;

							if ( cell != null )
								return cell.AccessibilityObject.GetSelected();
						}
					}
				}

				return null;
			}

				#endregion GetSelected

				#region HitTest

			/// <summary>
			/// Retrieves the child object at the specified screen coordinates.
			/// </summary>
			/// <param name="x">The horizontal screen coordinate.</param>
			/// <param name="y">The vertical screen coordinate.</param>
			/// <returns>An AccessibleObject that represents the child object at the given screen coordinates. This method returns the calling object if the object itself is at the location specified. Returns a null reference (Nothing in Visual Basic) if no object is at the tested location.</returns>
			public override AccessibleObject HitTest(int x, int y)
			{
				// AS 1/16/04 DNF143
				// See if we're over a child control first.
				AccessibleObject accObject = base.HitTest(x,y);

				if (accObject != this)
					return accObject;

				UIElement mainElement = this.grid.DisplayLayout.UIElement;

				if ( mainElement == null )
					return null;

				return mainElement.HitTest(x,y);
			}

				#endregion HitTest

				#region Navigate

            /// <summary>
            /// Navigates to another accessible object.
            /// </summary>
            /// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
            /// <returns>An AccessibleObject relative to this object.</returns>
            public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{
				UIElement mainElement = this.grid.DisplayLayout.UIElement;

				if ( mainElement == null )
					return null;

				return mainElement.Navigate(navdir);
			}

				#endregion Navigate

				#region Role

			/// <summary>
			/// Gets the role of this accessible object.
			/// </summary>
			public override AccessibleRole Role
			{
				get
				{
					if ( this.grid.AccessibleRole != AccessibleRole.Default )
						return base.Role;

					return AccessibleRole.Table;
				}
			}

				#endregion Role

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Grid

			/// <summary>
			/// Returns the associated grid control.
			/// </summary>
			public UltraGridBase Grid 
			{ 
				get 
				{
					return this.grid; 
				} 
			}

					#endregion Grid

				#endregion Public Properties

			#endregion Properties

		}

		#endregion Public Class UltraGridBaseAccessibleObject

		// MRS 2/19/04 - Added Preset support
		#region Implementation of ISupportPresets
		/// <summary>
		/// Returns a list of properties which can be used in a Preset
		/// </summary>
		/// <param name="presetType">Determines which type(s) of properties are returned</param>
		/// <returns>An array of strings indicating property names</returns>
		string[] ISupportPresets.GetPresetProperties(Infragistics.Win.PresetType presetType)
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList properties = new ArrayList();
			List<string> properties = new List<string>();

			properties.Add("DisplayLayout");

			if ((presetType & Infragistics.Win.PresetType.Appearance) == Infragistics.Win.PresetType.Appearance)
			{
				properties.Add("BackColor");
				properties.Add("BackgroundImage");
				properties.Add("ForeColor");
				properties.Add("FlatMode");
				properties.Add("Font");
				properties.Add("SupportThemes");				
				properties.Add("Text");				
			}
			if ((presetType & Infragistics.Win.PresetType.Behavior) == Infragistics.Win.PresetType.Behavior)
			{
				properties.Add("UpdateMode");
			}
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (string[])properties.ToArray(typeof(string));
			return properties.ToArray();
		}

		/// <summary>
		/// Returns the TypeName of the Preset target
		/// </summary>
		/// <returns>Returns "UltraGridBase"</returns>
		string ISupportPresets.GetPresetTargetTypeName()
		{
			return "UltraGridBase";
		}
		#endregion Implementation of ISupportPresets

		// SSP 6/29/04 - UltraCalc
		//
		#region Implementation of IUltraCalcParticipant

		internal void OnCalcReferenceDirtied( 
			object sender, Infragistics.Win.CalcEngine.UltraCalcReferenceEventArgs e )
		{
			Infragistics.Win.CalcEngine.IUltraCalcReference reference = RefUtils.GetUnderlyingReference( e.Reference );

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( reference is UltraGridRefBase && this == ((UltraGridRefBase)reference).Grid )
			UltraGridRefBase gridRef = reference as UltraGridRefBase;

			if ( gridRef != null && this == gridRef.Grid )
			{
				// MD 8/7/07 - 7.3 Performance
				// Refactored - FxCop - Do not cast unnecessarily
				#region Refactored

				//if ( reference is FormulaRefBase )
				//{
				//    FormulaRefBase formulaRef = (FormulaRefBase)reference;
				//    formulaRef.valueDirtyCounter++;
				//    formulaRef.visibleCounter++;
				//    formulaRef.InvalidateUIElement( );
				//}
				//else if ( reference is FormulaTargetRefBase )
				//{
				//    FormulaTargetRefBase formulaTargetRef = (FormulaTargetRefBase)reference;
				//    FormulaRefBase formulaRef = formulaTargetRef.ContainingFormulaRef;
				//    if ( null != formulaRef )
				//    {
				//        formulaTargetRef.verifiedValueDirtyCounter = formulaRef.valueDirtyCounter - 1;
				//        formulaTargetRef.verifiedRecalcVisibleVersion = formulaRef.visibleCounter - 1;
				//    }
				//    formulaTargetRef.InvalidateUIElement( );
				//}
				//else if ( reference is NestedColumnReference )
				//{
				//    NestedColumnReference nestedColRef = (NestedColumnReference)reference;
				//
				//    // SSP 9/6/04
				//    //
				//    nestedColRef.Column.CalcReference.visibleCounter++;
				//    if ( null == nestedColRef.Rows.ParentRow )
				//        nestedColRef.Column.CalcReference.valueDirtyCounter++;					
				//
				//    nestedColRef.InvalidateUIElement( );
				//}

				#endregion Refactored
				FormulaRefBase formulaRef = reference as FormulaRefBase;

				if ( formulaRef != null )
				{
					formulaRef.valueDirtyCounter++;
					formulaRef.visibleCounter++;
					formulaRef.InvalidateUIElement();
				}
				else
				{
					FormulaTargetRefBase formulaTargetRef = reference as FormulaTargetRefBase;

					if ( formulaTargetRef != null )
					{
						formulaRef = formulaTargetRef.ContainingFormulaRef;

						if ( null != formulaRef )
						{
							formulaTargetRef.verifiedValueDirtyCounter = formulaRef.valueDirtyCounter - 1;
							formulaTargetRef.verifiedRecalcVisibleVersion = formulaRef.visibleCounter - 1;
						}
						formulaTargetRef.InvalidateUIElement();
					}
					else
					{
						NestedColumnReference nestedColRef = reference as NestedColumnReference;

						if ( nestedColRef != null )
						{
							// SSP 9/6/04
							//
							nestedColRef.Column.CalcReference.visibleCounter++;
							if ( null == nestedColRef.Rows.ParentRow )
								nestedColRef.Column.CalcReference.valueDirtyCounter++;

							nestedColRef.InvalidateUIElement();
						}
					}
				}
			}
		}

		internal void RegisterWithCalcManager( )
		{
			// SSP 1/10/05
			// If the calc manager related notifications are suspended then don't register 
			// the grid with the calc manager. Delay until the notifications are resumed. This
			// is to fix a problem where if the CalcManager was set in InitializeLayout event
			// of the grid, the grid would get registered however none of the formulas would
			// get registered. This is to fix that.
			//
			if ( null == this.DisplayLayout || this.DisplayLayout.CalcManagerNotificationsSuspended )
				return;

			if ( this.lastCalcManagerGridRegisteredTo == this.calcManager )
				return;

			this.UnRegisterFromCalcManager( );

			// SSP 6/23/05 BR04651
			// If the name of the grid is null or empty string then throw an exception with an
			// intuitive message. The call to AddParticipant below also ends up throwing an exception 
			// however with a message that's not intuitive.
			//
			if ( null != this.calcManager )
			{
				string gridName = this.Name;
				if ( null == gridName || 0 == gridName.Length )
					throw new InvalidOperationException( "CalcManager requires that a unique name be given to every control participating with the calc manager." );
			}

			try
			{
				// Add it to the new calc manager.
				//
				if ( null != this.calcManager )
				{
					this.calcManager.CalcEngineDirtyValue += 
						new Infragistics.Win.CalcEngine.UltraCalcReferenceEventHandler(
						this.OnCalcReferenceDirtied );

					this.calcManager.AddParticipant( this );

					Infragistics.Win.CalcEngine.IUltraCalcReference rootReference = 
						((Infragistics.Win.CalcEngine.IUltraCalcParticipant)this).RootReference;

					if ( null != rootReference )
						this.calcManager.AddReference( rootReference );
				}

				if ( null != this.DisplayLayout && ! this.DisplayLayout.Disposed )
					this.DisplayLayout.RehookWithCalcManager( this.lastCalcManagerGridRegisteredTo );
			}
			finally
			{
				this.lastCalcManagerGridRegisteredTo = this.calcManager;
			}
		}

		internal void UnRegisterFromCalcManager( )
		{
			// Remove the grid from the old calc manager.
			//
			if ( null != this.lastCalcManagerGridRegisteredTo )
			{
				Infragistics.Win.CalcEngine.IUltraCalcReference rootReference = 
					((Infragistics.Win.CalcEngine.IUltraCalcParticipant)this).RootReference;

				try
				{
					this.calcManager.CalcEngineDirtyValue -= 
						new Infragistics.Win.CalcEngine.UltraCalcReferenceEventHandler(
						this.OnCalcReferenceDirtied );

					if ( null != rootReference )
						this.lastCalcManagerGridRegisteredTo.RemoveReference( rootReference );

					this.lastCalcManagerGridRegisteredTo.RemoveParticipant( this );
				}
				finally
				{
					this.lastCalcManagerGridRegisteredTo = null;
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the calc manager used for evaluating formulas in the grid.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// UltraGrid requires an instance of <b>UltraCalcManager</b> to perform formula 
		/// calculations. You can assign this property at run time or at design time.
		/// At design-time all you have to do is add an instance of 
		/// <b>UltraCalcManager</b> component on the form containing the UltraGrid.
		/// The UltraCalcManager component will automatically search the form for
		/// instances of UltraGrid's and assign their CalcManager properties to itself.
		/// UltraCalcManager is defined in Infragistics.Win.UltraWinCalcManager assembly
		/// and in Infragistics.Win.CalcEngine namespace.
		/// </p>
		/// </remarks>
		[ DefaultValue(null), Browsable( false ) ]
		public Infragistics.Win.CalcEngine.IUltraCalcManager CalcManager
		{
			get
			{
				return this.calcManager;
			}
			set
			{
				if ( this.calcManager != value )
				{
					this.UnRegisterFromCalcManager( );

					this.calcManager = value;

					if ( ! this.Initializing )
						this.RegisterWithCalcManager( );
				}
			}
		}

		Infragistics.Win.CalcEngine.IUltraCalcReference Infragistics.Win.CalcEngine.IUltraCalcParticipant.RootReference 
		{
			get
			{
				return null != this.DisplayLayout ? this.DisplayLayout.CalcReference : null;
			}
		}

		Infragistics.Win.CalcEngine.ReferenceNode Infragistics.Win.CalcEngine.IUltraCalcParticipant.GetDesignerReferences( Infragistics.Win.CalcEngine.IFormulaProvider formulaProvider )
		{
			Infragistics.Win.CalcEngine.ReferenceNode referenceTree = 
				null != this.DisplayLayout ? this.DisplayLayout.GetDesignerReferences( ) : null;

			// Also add the grid reference in the hierarchy.
			//
			if ( null != referenceTree && null != this.DisplayLayout.CalcReference )
			{
				referenceTree = new Infragistics.Win.CalcEngine.ReferenceNode(
					this.DisplayLayout.CalcReference,
					new Infragistics.Win.CalcEngine.ReferenceNode[] { referenceTree },
					false );
			}

			return referenceTree;
		}

		bool Infragistics.Win.CalcEngine.IUltraCalcParticipant.SuppressCalcSettings
		{
			get
			{
				return !(this is Infragistics.Win.UltraWinGrid.UltraCombo);
			}
		}

		// SSP 11/15/04 - UWC159
		// Added RecreateReference method on the IUltraCalcParticipant so we can recreate the
		// calc reference when the name of the control changes at design time.
		//
		void Infragistics.Win.CalcEngine.IUltraCalcParticipant.RecreateReference( )
		{
			if ( null != this.layout && ! this.layout.Disposed && ! this.Disposing && ! this.IsDisposed )
				this.layout.RecreateCalcReference( );
		}

		#endregion // Implementation of IUltraCalcParticipant

		// SSP 8/28/04 UWG3585
		// Overrode EndUpdate so we can bump the cell element version numbers when
		// drawing is resumed. We don't bother dirtying the cell elements when cells'
		// appearance are modified while drawing is suspended. So we need to bump
		// the cell elements version number so they get repositioned. The base class'
		// implementation dirties the control element so we don't need to do that
		// here.
		//
		#region EndUpdate

		/// <summary>
		/// Resets the <see cref="UltraControlBase.IsUpdating"/> flag to false and optionally invalidates the control.
		/// </summary>
		/// <param name="invalidate">True to invalidate the control and dirty the child elements; otherwise false.</param>
		/// <remarks>
		/// <p class="body">This method must be called after <see cref="UltraControlBase.BeginUpdate"/>. If <b>BeginUpdate</b> was called without a subsequent call to <b>EndUpdate</b> the control will not draw itself.</p>
		/// <p class="note"><b>Note:</b> Calling this method passing in false should only be done when it is known that the changes made between 
		/// the <see cref="UltraControlBase.BeginUpdate"/> and <b>EndUpdate</b> calls did not require invalidation or dirtying of the elements or when the invalidation 
		/// is being handled by the programmer.</p>
		/// </remarks>
		/// <seealso cref="UltraControlBase.IsUpdating"/>
		/// <seealso cref="UltraControlBase.BeginUpdate"/>
		// AS 4/6/05
		// Added an overload that takes a boolean indicating whether to invalidate.
		//public override void EndUpdate()
		public override void EndUpdate(bool invalidate)
		{
			// AS 4/6/05
			//if ( null != this.layout )
			if ( invalidate && null != this.layout )
				this.layout.BumpCellChildElementsCacheVersion( );

			// AS 4/6/05
			//base.EndUpdate( );
			base.EndUpdate(invalidate);
		}

		#endregion EndUpdate

		#region Refresh

		// SSP 10/25/04
		// Overrode Refresh so we can bump the cell element version numbers. There is an 
		// optimization in place that causes the grid to not reposition the cell elements
		// if their sizes are the same. It simply offsets their rects. However sometimes
		// it's necessary to do so (for example when a cell's value changes to a different
		// image we want to reposition the cell).
		//
		/// <summary>
		/// Overridden. Forces the control to invalidate its client area and immediately 
		/// redraw itself and any child controls.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>Refresh</b> method re-draws the control. Typically calling this method
		/// is not necessary since UltraGrid will redraw itself automatically whenever 
		/// data or settings change that affect the UltraGrid display. This is
		/// useful when for example the data source doesn't support change notifications 
		/// and as a result when the data changes the UltraGrid doesn't redraw the 
		/// display. In such a scenario this method can be called to redraw the UltraGrid.
		/// </p>
		/// <seealso cref="UltraControlBase.BeginUpdate"/>
        /// <seealso cref="UltraControlBase.EndUpdate(bool)"/>
		/// </remarks>
		public override void Refresh( )
		{
			if ( null != this.layout && ! this.layout.Disposed )
			{
				if ( this.layout.HasUIElement )
				{
					this.layout.BumpCellChildElementsCacheVersion( );
					this.layout.DirtyGridElement( false, false );
				}
			}

			base.Refresh( );
		}

		#endregion // Refresh

		#region Sorting, Filtering, and Column Sizing on UltraDropDown/UltraCombo
		#region Moved Events from UltraGrid to UltraGridBase

		#region FireCommonEvent
		
		internal virtual bool FireCommonEvent( CommonEventIds id, EventArgs e )
		{
			return this.FireCommonEvent(id, e, false);
		}

		internal abstract bool FireCommonEvent( CommonEventIds id, EventArgs e, bool checkInProgress );
		
		#endregion  FireCommonEvent

		#region BeforeSortChange
		/// <summary>
		/// Occurs before the sort indicator is changed.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>band</i> argument returns a reference to an UltraGridBand object that can be used to set properties of, and invoke methods on, the band that will be sorted. You can use this reference to access any of the returned band's properties or methods.</p>
		///	<p class="body">The UltraGrid can automatically sort the contents of columns without the addition of any code, provided the control is able to preload the rows in the band. Preloading is enabled by default if the recordset bound to the band contains less than 1000 rows. If you do not want to preload rows, but you still want to provide column sorting in the control, you must implement column sorting yourself using the <b>BeforeSortChange</b> and <b>AfterSortChange</b> events.</p>
		///	<p class="body">The <i>newsortedcols</i> argument returns a reference to a SortedCols collection that can be used to retrieve references to the UltraGridColumn object or objects being sorted. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the columns from being sorted. This argument can be used to prevent the user from sorting columns unless a certain condition is met.</p>
		///	<p class="body">The <b>AfterSortChange</b> event, which occurs after a sort action is completed, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeSortChange")]	
		public event BeforeSortChangeEventHandler		BeforeSortChange
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORESORTCHANGE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORESORTCHANGE, value ); }
		}

		#endregion BeforeSortChange

		#region AfterSortChange
		/// <summary>
		/// Occurs after a sort action is completed.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>band</i>argument returns a reference to an UltraGridBand object that can be used to set properties of, and invoke methods on, the band that was sorted. You can use this reference to access any of the returned band's properties or methods.</p>
		///	<p class="body">The UltraWinGrid can automatically sort the contents of columns without the addition of any code, provided the control is able to preload the rows in the band. Preloading is enabled by default if the recordset bound to the band contains less than 1000 rows. If you do not want to preload rows, but you still want to provide column sorting in the control, you must implement column sorting yourself using the <b>BeforeSortChange</b> and <b>AfterSortChange</b> events.</p>
		///	<p class="body">The <b>BeforeSortChange</b> event, which occurs before a sort action is completed, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterSortChange")]	
		public event BandEventHandler				AfterSortChange
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERSORTCHANGE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERSORTCHANGE, value );} 
		}
		#endregion AfterSortChange

		#region BeforeColPosChanged
		/// <summary>
		/// Occurs before one or more columns have been moved, swapped, or sized.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>action</i> argument indicates which action  will occur to the column or columns: moving, swapping, or sizing.</p>
		///	<p class="body">The <i>columns</i> argument returns a reference to a SelectedCols collection that can be used to retrieve references to the UltraGridColumn object or objects that will be moved, swapped, or sized. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection. However, all properties of the affected columns are read-only in this event procedure.</p>
		///	<p class="body">The <i>cancel</i> argument enables you to programmatically prevent the column or columns from being moved, swapped, or sized. This argument can be used to prevent the user from moving, swapping, or sizing columns unless a certain condition is met. To prevent the user from attempting to move, swap, or size a column, set the <b>AllowColMoving</b>, <b>AllowColSwapping</b>, <b>AllowColSizing</b> properties, respectively.</p>
		///	<p class="body">
		/// This event is generated before one or more columns are moved, swapped, or sized, either 
		/// programmatically, or by user interaction.
		/// Use UltraGridColumn's <see cref="UltraGridColumn.Width"/> property to resize it, 
		/// ColumnHeader's <see cref="ColumnHeader.SetVisiblePosition"/> method to move it
		/// and <see cref="UltraGridColumn.Swap"/> method to swap the column. Using these
		/// properties and methods will cause this event to be raised.
		/// </p>
		///	<p class="body">The <b>VisiblePosition</b> property can be used to determine both the current and new positions of the column or columns that will be moved or swapped. New positions can be determined by reading the property off of the header of the column or columns in <i>columns</i>, while current positions can be determined by reading the property off of the header of the column or columns in the appropriate band.</p>
		///	<p class="body">The <b>BeforeGroupPosChanged</b> event is generated before one or more groups are moved, swapped, or sized.</p>
		///	<p class="body">The <b>AfterColPosChanged</b> event, which occurs after one or more columns are moved, swapped, or sized, is generated after this event, provided <i>cancel</i> is not set to True.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeColPosChanged")]	
		public event BeforeColPosChangedEventHandler	BeforeColPosChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECOLPOSCHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECOLPOSCHANGED, value ); }
		}
		#endregion BeforeColPosChanged

		#region AfterColPosChanged
		/// <summary>
		/// Occurs after a column has been moved, sized or swapped.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>action</i> argument indicates which action occurred to the column or columns: moving, swapping, or sizing.</p>
		///	<p class="body">The <i>columns</i> argument returns a reference to a SelectedCols collection that can be used to retrieve references to the UltraGridColumn object or objects that were moved, swapped, or sized. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		///	<p class="body">This event is generated after one or more columns are moved, swapped, or sized, either programmatically, or by user interaction. A column can be sized programmatically by setting its <b>Width</b> property and can be moved programmatically by setting its header's <b>VisiblePosition</b> property.</p>
		///	<p class="body">The <b>VisiblePosition</b> property of a column's header can be used to determine the new position of a column that was moved or swapped.</p>
		///	<p class="body">To prevent the user from attempting to move, swap, or size a column, set the <b>AllowColMoving</b>, <b>AllowColSwapping</b>, or <b>AllowColSizing</b> properties, respectively.</p>
		///	<p class="body">The <b>AfterGroupPosChanged</b> event is generated after one or more groups are moved, swapped, or sized.</p>
		///	<p class="body">The <b>BeforeColPosChanged</b> event, which occurs before one or more columns are moved, swapped, or sized, is generated before this event.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_AfterColPosChanged")]	
		public event AfterColPosChangedEventHandler AfterColPosChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERCOLPOSCHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERCOLPOSCHANGED, value );} 
		}
		#endregion AfterColPosChanged

		#region BeforeRowFilterChangedEventHandler
		// SSP 8/1/03
		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
		//
		/// <summary>
		/// BeforeRowFilterChanged event gets fired when the user modifies row filters for a column. This event is cancelable.
		/// </summary>
		/// <remarks>
		/// <seealso cref="AfterRowFilterChanged"/> <seealso cref="BeforeRowFilterChangedEventArgs"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_BeforeRowFilterChanged")]
		public event BeforeRowFilterChangedEventHandler BeforeRowFilterChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWFILTERCHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWFILTERCHANGED, value );} 
		}
		#endregion BeforeRowFilterChangedEventHandler

		#region AfterRowFilterChangedEventHandler
		/// <summary>
		/// AfterRowFilterChanged event gets fired after the user has modified row filters for a column.
		/// </summary>
		/// <remarks>
		/// <seealso cref="BeforeRowFilterChanged"/> <seealso cref="AfterRowFilterChangedEventArgs"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_AfterRowFilterChanged")]
		public event AfterRowFilterChangedEventHandler AfterRowFilterChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERROWFILTERCHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERROWFILTERCHANGED, value );} 
		}
		#endregion AfterRowFilterChangedEventHandler

		#region BeforeRowFilterDropDownPopulateEventHandler
		// SSP 4/15/04
		// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
		// from populating the filter drop down list. This can be useful if there are lot of rows
		// and it takes a long time for the ultragrid to populate the filter drop down list.
		// 
		/// <summary>
		/// Occurs before the row filter drop down is populated.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>BeforeRowFilterDropDownPopulate</b> event to prevent the UltraGrid from populating the filter value list and instead populate it yourself. In <see cref="BeforeRowFilterDropDown"/> event, which is fired after this event, the filter value list is already populated. You can modify the filter value list there as well.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.BeforeRowFilterDropDownPopulateEventArgs"/> <seealso cref="BeforeRowFilterDropDown"/>
        ///	<seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.FilterOperandDropDownItems"/>
		///	</remarks>
		[LocalizedDescription("LDR_UltraGrid_E_BeforeRowFilterDropDownPopulate")]
		public event BeforeRowFilterDropDownPopulateEventHandler BeforeRowFilterDropDownPopulate
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWFILTERDROPDOWNPOPULATE, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWFILTERDROPDOWNPOPULATE, value );} 
		}
		#endregion BeforeRowFilterDropDownPopulateEventHandler

		#region BeforeRowFilterDropDownEventHandler	
		// SSP 3/21/02
		// Added BeforeRowFilterDropDownEventHandler for row filtering feature in version 2
		//
		/// <summary>
		/// Occurs before the row filter drop down is dropped down when the user clicks on filter dropdown symbol on a column header.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>BeforeRowFilterDropDown</b> event to cancel the drop down.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.BeforeRowFilterDropDownEventArgs"/> <seealso cref="BeforeRowFilterDropDownPopulate"/> <seealso cref="AfterRowFilterChanged"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeRowFilterDropDown")]
		public event BeforeRowFilterDropDownEventHandler BeforeRowFilterDropDown
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREROWFILTERDROPDOWN, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREROWFILTERDROPDOWN, value );} 
		}
		#endregion BeforeRowFilterDropDownEventHandler

		#region BeforeCustomRowFilterDialogEventHandler
		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved from UltraGrid
		// SSP 3/21/02
		// Added BeforeCustomRowFilterDialogEventHandler for row filtering feature in version 2
		//
		/// <summary>
		/// Occurs when the user selects (Custom) from the row filter dropdown and before the grid displays the custom row filter dialog.
		/// </summary>
		///	<remarks>
		///	<p class="body">You can use the <b>BeforeRowFilterDropDown</b> event to cancel the drop down.</p>
		///	<seealso cref="Infragistics.Win.UltraWinGrid.BeforeRowFilterDropDownEventArgs"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGrid_E_BeforeCustomRowFilterDialog")]
		public event BeforeCustomRowFilterDialogEventHandler BeforeCustomRowFilterDialog
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECUSTOMROWFILTERDIALOG, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECUSTOMROWFILTERDIALOG, value );} 
		}
		#endregion BeforeCustomRowFilterDialogEventHandler

		#region FilterCellValueChangedEventHandler

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		/// <summary>
		/// FilterCellValueChanged event gets fired when the user modifies a cell in a filter row.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>FilterCellValueChanged</b> event gets fired when the user modifies a cell 
		/// in a filter row.
		/// </p>
		/// <seealso cref="UltraGridOverride.FilterUIType"/>
		/// <seealso cref="UltraGridFilterRow"/>
		/// <seealso cref="UltraGridFilterCell"/>
		/// <seealso cref="UltraGridFilterCell"/>
		/// <seealso cref="ColumnFiltersCollection"/>
		/// <seealso cref="ColumnFilter"/>
		/// <seealso cref="FilterConditionsCollection"/>
		/// <seealso cref="FilterCondition"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_FilterCellValueChanged")]
		public event FilterCellValueChangedEventHandler FilterCellValueChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_FILTERCELLVALUECHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_FILTERCELLVALUECHANGED, value );} 
		}

		#endregion FilterCellValueChangedEventHandler

		#region InitializeRowsCollectionEventHandler

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		/// <summary>
		/// InitializeRowsCollection event is fired whenever UltraGrid creates a new row collection.
		/// </summary>
		/// <remarks>
		/// <seealso cref="RowsCollection"/>
		/// <seealso cref="UltraGridBase.Rows"/>
		/// <seealso cref="UltraGrid.InitializeRow"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_InitializeRowsCollection")]
		public event InitializeRowsCollectionEventHandler InitializeRowsCollection
		{
			add { this.EventsOptimized.AddHandler( EVENT_INITIALIZEROWSCOLLECTION, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_INITIALIZEROWSCOLLECTION, value );} 
		}

		#endregion InitializeRowsCollectionEventHandler

		#region BeforeColumnChooserDisplayed

		// SSP 6/17/05 - NAS 5.3 ColumnChooser
		//
		/// <summary>
		/// BeforeColumnChooserDisplayed event is fired before the column chooser dialog is displayed by the user.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>BeforeColumnChooserDisplayed</b> event is fired before the column chooser dialog 
		/// is displayed by the user.
		/// </p>
		/// <seealso cref="ColumnChooserDialog"/>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// <seealso cref="UltraGridBase.ShowColumnChooser()"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_BeforeColumnChooserDisplayed")]
		public event BeforeColumnChooserDisplayedEventHandler BeforeColumnChooserDisplayed
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFORECOLUMNCHOOSERDISPLAYED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFORECOLUMNCHOOSERDISPLAYED, value );} 
		}

		#endregion BeforeColumnChooserDisplayed

		#region BeforeBandHiddenChanged

		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// 
		/// <summary>
		/// BeforeBandHiddenChanged event is fired when the user hides a band, typically via the column chooser dialog.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>BeforeBandHiddenChanged</b> event is fired when the user hides a band, typically via the column chooser dialog.
		/// </p>
		/// <seealso cref="ColumnChooserDialog"/>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// <seealso cref="UltraGridBase.ShowColumnChooser()"/>
		/// <seealso cref="UltraGridBase.BeforeColumnChooserDisplayed"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_BeforeBandHiddenChanged")]
		public event BeforeBandHiddenChangedEventHandler BeforeBandHiddenChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_BEFOREBANDHIDDENCHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_BEFOREBANDHIDDENCHANGED, value );} 
		}

		#endregion BeforeBandHiddenChanged

		#region AfterBandHiddenChanged

		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// 
		/// <summary>
		/// AfterBandHiddenChanged event is fired after the user hides a band, typically via the column chooser dialog.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>AfterBandHiddenChanged</b> event is fired after the user hides a band, typically via the column chooser dialog.
		/// </p>
		/// <seealso cref="ColumnChooserDialog"/>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// <seealso cref="UltraGridBase.ShowColumnChooser()"/>
		/// <seealso cref="UltraGridBase.BeforeColumnChooserDisplayed"/>
		/// </remarks>
		[LocalizedDescription("LDR_UltraGrid_E_AfterBandHiddenChanged")]
		public event AfterBandHiddenChangedEventHandler AfterBandHiddenChanged
		{
			add { this.EventsOptimized.AddHandler( EVENT_AFTERBANDHIDDENCHANGED, value ); }
			remove { this.EventsOptimized.RemoveHandler( EVENT_AFTERBANDHIDDENCHANGED, value );} 
		}

		#endregion AfterBandHiddenChanged

		#region OnBeforeSortChange
		/// <summary>
		/// Called before sort change
		/// </summary>
		virtual protected void OnBeforeSortChange ( BeforeSortChangeEventArgs e )
		{
			BeforeSortChangeEventHandler eDelegate = (BeforeSortChangeEventHandler)EventsOptimized[EVENT_BEFORESORTCHANGE];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion OnBeforeSortChange

		#region OnAfterSortChange
		/// <summary>
		/// Called after a band has been sorted
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
        virtual protected void OnAfterSortChange(BandEventArgs e)
		{
			BandEventHandler eDelegate = (BandEventHandler)EventsOptimized[EVENT_AFTERSORTCHANGE];

			if ( eDelegate != null )
				eDelegate( this, e );			
		}
		#endregion OnAfterSortChange

		#region OnAfterColPosChanged
		/// <summary>
		/// Called after a column postion has changed
		/// </summary>
		/// <param name="e">EventArgs</param>
		virtual protected void OnAfterColPosChanged ( AfterColPosChangedEventArgs e )
		{
			AfterColPosChangedEventHandler eDelegate = (AfterColPosChangedEventHandler)EventsOptimized[EVENT_AFTERCOLPOSCHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );		
		}
		#endregion OnAfterColPosChanged

		#region OnBeforeColPosChanged
		/// <summary>
		/// Called before a column postion changes
		/// </summary>
		virtual protected void OnBeforeColPosChanged ( BeforeColPosChangedEventArgs e )
		{
			BeforeColPosChangedEventHandler eDelegate = (BeforeColPosChangedEventHandler)EventsOptimized[EVENT_BEFORECOLPOSCHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion OnBeforeColPosChanged

		#region OnBeforeRowFilterDropDown
		/// <summary>
		/// Called before row filters drop down is dopped down.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeRowFilterDropDown( BeforeRowFilterDropDownEventArgs e )
		{
			BeforeRowFilterDropDownEventHandler eDelegate = (BeforeRowFilterDropDownEventHandler)EventsOptimized[EVENT_BEFOREROWFILTERDROPDOWN];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion OnBeforeRowFilterDropDown

		#region OnBeforeRowFilterDropDownPopulate
		// SSP 4/15/04 - Virtual Binding
		// Added BeforeRowFilterDropDownPopulate to give the users a chance to prevent the ultragrid
		// from populating the filter drop down list. This can be useful if there are lot of rows
		// and it takes a long time for the ultragrid to populate the filter drop down list.
		// 
		/// <summary>
		/// Called before UltraGrid populates the row filter drop down.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeRowFilterDropDownPopulate( BeforeRowFilterDropDownPopulateEventArgs e )
		{
			BeforeRowFilterDropDownPopulateEventHandler eDelegate = (BeforeRowFilterDropDownPopulateEventHandler)EventsOptimized[EVENT_BEFOREROWFILTERDROPDOWNPOPULATE];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion OnBeforeRowFilterDropDownPopulate

		#region OnBeforeCustomRowFilterDialog
		/// <summary>
		/// Called before grid displays custom row filters dialog.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeCustomRowFilterDialog( BeforeCustomRowFilterDialogEventArgs e )
		{
			BeforeCustomRowFilterDialogEventHandler eDelegate = (BeforeCustomRowFilterDialogEventHandler)EventsOptimized[EVENT_BEFORECUSTOMROWFILTERDIALOG];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion OnBeforeCustomRowFilterDialog

		#region OnAfterRowFilterChanged
		// SSP 8/1/03
		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
		//
		/// <summary>
		/// OnAfterRowFilterChanged gets after the user has modified row filters for a column. This method fires the associated event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterRowFilterChanged( AfterRowFilterChangedEventArgs e )
		{
			AfterRowFilterChangedEventHandler eDelegate = (AfterRowFilterChangedEventHandler)EventsOptimized[EVENT_AFTERROWFILTERCHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion OnAfterRowFilterChanged
		
		#region OnBeforeRowFilterChanged
		// SSP 8/1/03
		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
		//
		/// <summary>
		/// OnBeforeRowFilterChanged gets called when the user modifies row filters for a column. This method fires the associated event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeRowFilterChanged( BeforeRowFilterChangedEventArgs e )
		{
			BeforeRowFilterChangedEventHandler eDelegate = (BeforeRowFilterChangedEventHandler)EventsOptimized[EVENT_BEFOREROWFILTERCHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}
		#endregion OnBeforeRowFilterChanged

		#region OnFilterCellValueChanged

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		/// <summary>
		/// OnFilterCellValueChanged is called when the user modifies a cell in a filter row. This method fires the <see cref="UltraGridBase.FilterCellValueChanged"/> event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnFilterCellValueChanged( FilterCellValueChangedEventArgs e )
		{
			FilterCellValueChangedEventHandler eDelegate = (FilterCellValueChangedEventHandler)EventsOptimized[EVENT_FILTERCELLVALUECHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion OnFilterCellValueChanged

		#region OnInitializeRowsCollection

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		/// <summary>
		/// OnInitializeRowsCollection is called whenever UltraGrid creates a new row collection. This method fires the <see cref="InitializeRowsCollection"/> event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnInitializeRowsCollection( InitializeRowsCollectionEventArgs e )
		{
			InitializeRowsCollectionEventHandler eDelegate = (InitializeRowsCollectionEventHandler)EventsOptimized[EVENT_INITIALIZEROWSCOLLECTION];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion OnInitializeRowsCollection

		#region OnBeforeColumnChooserDisplayed

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		//
		/// <summary>
		/// OnBeforeColumnChooserDisplayed is called before the column chooser dialog is displayed by the user.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeColumnChooserDisplayed( BeforeColumnChooserDisplayedEventArgs e )
		{
			BeforeColumnChooserDisplayedEventHandler eDelegate = (BeforeColumnChooserDisplayedEventHandler)EventsOptimized[EVENT_BEFORECOLUMNCHOOSERDISPLAYED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion OnBeforeColumnChooserDisplayed

		#region OnBeforeBandHiddenChanged

		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
		// TestAdvantage.
		// 
		/// <summary>
		/// Called when the user hides a band, typically via the column chooser dialog. This method
		/// raises BeforeBandHiddenChanged event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnBeforeBandHiddenChanged( BeforeBandHiddenChangedEventArgs e )
		{
			BeforeBandHiddenChangedEventHandler eDelegate = (BeforeBandHiddenChangedEventHandler)EventsOptimized[EVENT_BEFOREBANDHIDDENCHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion OnBeforeBandHiddenChanged

		#region OnAfterBandHiddenChanged

		// SSP 10/18/05 - NAS 5.3 Column Chooser
		// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
		// TestAdvantage.
		// 
		/// <summary>
		/// Called after the user hides a band, typically via the column chooser dialog. This method
		/// raises AfterBandHiddenChanged event.
		/// </summary>
		/// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		protected virtual void OnAfterBandHiddenChanged( AfterBandHiddenChangedEventArgs e )
		{
			AfterBandHiddenChangedEventHandler eDelegate = (AfterBandHiddenChangedEventHandler)EventsOptimized[EVENT_AFTERBANDHIDDENCHANGED];

			if ( eDelegate != null )
				eDelegate( this, e );
		}

		#endregion OnAfterBandHiddenChanged

		#endregion Moved Events from UltraGrid to UltraGridBase

        // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox - Moved here from the UltraGrid
        #region BeforeHeaderCheckStateChanged

        /// <summary>
        /// Occurs before the CheckState of the Header CheckBox is changed
        /// </summary>
        [LocalizedDescription("LD_UltraGrid_E_BeforeHeaderCheckStateChanged")]
        public event BeforeHeaderCheckStateChangedEventHandler BeforeHeaderCheckStateChanged
        {
            add { this.EventsOptimized.AddHandler(EVENT_BEFOREHEADERCHECKSTATECHANGED, value); }
            remove { this.EventsOptimized.RemoveHandler(EVENT_BEFOREHEADERCHECKSTATECHANGED, value); }
        }
        #endregion BeforeHeaderCheckStateChanged

        // CDS 02/02/09 TFS12512 - NAS v9.1 Header CheckBox - Moved here from the UltraGrid
        #region AfterHeaderCheckStateChanged

        /// <summary>
        /// Occurs after the CheckState of the Header CheckBox is changed
        /// </summary>
        [LocalizedDescription("LD_UltraGrid_E_AfterHeaderCheckStateChanged")]
        public event AfterHeaderCheckStateChangedEventHandler AfterHeaderCheckStateChanged
        {
            add { this.EventsOptimized.AddHandler(EVENT_AFTERHEADERCHECKSTATECHANGED, value); }
            remove { this.EventsOptimized.RemoveHandler(EVENT_AFTERHEADERCHECKSTATECHANGED, value); }
        }
        #endregion AfterHeaderCheckStateChanged

        // CDS NAS v9.1 Header CheckBox
        #region OnBeforeHeaderCheckBoxStateChanged

        /// <summary>
        /// Fires the BeforeHeaderCheckBoxStateChanged event.
        /// </summary>
        /// <param name="e">The event args containing the event information.</param>
        protected virtual void OnBeforeHeaderCheckBoxStateChanged(BeforeHeaderCheckStateChangedEventArgs e)
        {
            BeforeHeaderCheckStateChangedEventHandler eDelegate = (BeforeHeaderCheckStateChangedEventHandler)EventsOptimized[EVENT_BEFOREHEADERCHECKSTATECHANGED];
            if (eDelegate != null)
                eDelegate(this, e);
        }

        #endregion OnBeforeHeaderCheckBoxStateChanged

        // CDS NAS v9.1 Header CheckBox
        #region OnAfterHeaderCheckBoxStateChanged

        /// <summary>
        /// Fires the AfterHeaderCheckBoxStateChanged event.
        /// </summary>
        /// <param name="e">The event args containing the event information.</param>
        protected virtual void OnAfterHeaderCheckBoxStateChanged(AfterHeaderCheckStateChangedEventArgs e)
        {
            AfterHeaderCheckStateChangedEventHandler eDelegate = (AfterHeaderCheckStateChangedEventHandler)EventsOptimized[EVENT_AFTERHEADERCHECKSTATECHANGED];
            if (eDelegate != null)
                eDelegate(this, e);
        }

        #endregion OnAfterHeaderCheckBoxStateChanged


		// MRS 12/1/04 - UWG567, UWG3023, UWG3414, and UWG3470
		// Moved from UltraGrid
		#region Moved Filtering Code from UltraGrid
		#region "Filter code" 

		#region FilterDropDown

		internal ValueList FilterDropDown
		{
			get
			{
				if ( null == this.filterDropDown )
					this.filterDropDown = new Infragistics.Win.ValueList( );

				return this.filterDropDown;
			}
		}

		#endregion // FilterDropDown

		#region CustomRowFiltersDialog

		// SSP 8/28/03 - Optimizations
		// Instead of creating the form and keeping it around, recreate it every time
		// it's displayed.
		//
		
		
		#endregion // CustomRowFiltersDialog

		#region HasFilterDropDown

		internal bool HasFilterDropDown
		{
			get
			{
				return null != this.filterDropDown;
			}
		}

		#endregion // HasFilterDropDown

		#endregion // End of "Filter Code" region
		#endregion Moved Filtering Code from UltraGrid

		#region HeaderClickActionDefault
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal abstract HeaderClickAction HeaderClickActionDefault { get; }
		#endregion HeaderClickActionDefault

		#endregion Sorting, Filtering, and Column Sizing on UltraDropDown/UltraCombo

		// SSP 1/14/05 BR01756
		// Took out the restriction we had that didn't allow setting of cell values in
		// UltraDropDownBase. Also moved the UpdateData from UltraGridBase to UltraGrid.
		//
		#region UpdateData

		// SSP 11/20/03 UWG2741
		// Added following CommitRowCallback row traversal class for committing modified rows.
		//
		#region CommitRowCallback Class

		// MD 1/24/08
		// Made changes to allow for VS2008 style unit test accessors
		//private class CommitRowCallback : RowsCollection.IRowCallback
		private class CommitRowCallback : IRowCallback
		{
			internal CommitRowCallback( )
			{
			}

			public bool ProcessRow( UltraGridRow row )
			{
				// SSP 6/17/04 UWG3407
				// Don't update an unmodified template add-row. That is if the user has
				// clicked into a template add-row and not typed in anything then we should
				// not commit the row.
				//
				//if ( null != row && row.DataChanged )
				if ( null != row 
					// SSP 11/17/05 BR07772
					// If the column is bound then we need to honor the UpdateData method
					// call even if modifications were made via the calc manager.
					// 
					//&& row.DataChanged 
					&& ( row.DataChanged || row.dataChangedViaCalcManager )
					&& ! row.IsUnModifiedAddRowFromTemplate )
					row.Update( );

				return true;
			}
		}							
		
		#endregion // CommitRowCallback Class

		/// <summary>
		/// Updates (commits) any modified information.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>UpdateData</b> method updates any modified information in the grid, sending it to the data provider. When the update is complete, any rows that were marked as having modified data will have that mark cleared.  The <b>DataChanged</b> property will be set to False.</p>
		///	<p class="body">Normally, the grid handles the updating of data automatically based on the UpdateMode property, so there will be few situations in which you will need to invoke this method. The major exception is when you want to update the data in response to an event that does not cause the grid to lose focus. For example, clicking on a toolbar button. Since toolbars do not typically take focus, the grid will not send any data to the data provider until you invoke the <b>UpdateData</b> method.</p>
        /// <p class="body">Note that if a cell in the grid is currently in edit mode, this method will not commit the changes to that cell. To handle this, you may want to call the PerformAction method and specify the ExitEditMode action. This allows you to attempt to take the cell out of edit mode and check to make sure this process is successful before calling UpdateData.</p>
		///	</remarks>
		public void UpdateData()
		{
			if ( null == this.DisplayLayout )
				return;

			Infragistics.Win.UltraWinGrid.BandsCollection bands = this.DisplayLayout.SortedBands;

			if ( null == bands )
				return;

			// SSP 11/20/03 UWG2741
			// Band.CommitRowUpdateList simply works on the child rows collection associated
			// with the active row in the parent band. It doesn't take care of other child
			// rows collections that belong to non-active parent rows. Look in the 
			// CommitRowUpdateList implementation which has been commented out as a result.
			// Commented out the original code and added new code.
			//
			this.Rows.InternalTraverseRowsHelper( new UltraGrid.CommitRowCallback( ), true, true );
			
		}	

		#endregion // UpdateData

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
		//
		#region Preset Application Logic

			#region ApplyPresetFromXml( string, bool )

		/// <summary>
		/// Applies the Preset data to the UltraGrid.
		/// The Preset data contained in the target file must be stored in an XML format.
		/// </summary>
		/// <param name="fileName">The file containing Preset data stored in an XML format.</param>
		/// <param name="clearAllSettings">Determines whether to clear all property settings which can be specified in a Preset file before applying the new settings.</param>
		public void ApplyPresetFromXml( string fileName, bool clearAllSettings )
		{
			PresetSerializer.FromXml( fileName ).Apply( this, clearAllSettings );
		}

			#endregion // ApplyPresetFromXml( string, bool )

			#region ApplyPresetFromXml( string )

		/// <summary>
		/// Applies the Preset data to the UltraGrid.
		/// The Preset data contained in the target file must be stored in an XML format.
		/// This overload defaults to clearing all property settings which can be specified in a Preset file prior to applying the new settings.
		/// </summary>
		/// <param name="fileName">The file containing Preset data stored in an XML format.</param>
		public void ApplyPresetFromXml( string fileName )
		{
			this.ApplyPresetFromXml( fileName, true );
		}

			#endregion // ApplyPresetFromXml( string )

			#region ApplyPresetFromBinary( string, bool )

		/// <summary>
		/// Applies the Preset data to the UltraGrid.
		/// The Preset data contained in the target file must be stored in a binary format.
		/// </summary>
		/// <param name="fileName">The file containing Preset data stored in a binary format.</param>
		/// <param name="clearAllSettings">Determines whether to clear all property settings which can be specified in a Preset file before applying the new settings.</param>
		public void ApplyPresetFromBinary( string fileName, bool clearAllSettings )
		{
			PresetSerializer.FromBinary( fileName ).Apply( this, clearAllSettings );
		}

			#endregion // ApplyPresetFromBinary( string, bool )

			#region ApplyPresetFromBinary( string )

		/// <summary>
		/// Applies the Preset data to the UltraGrid.
		/// The Preset data contained in the target file must be stored in a binary format.
		/// This overload defaults to clearing all property settings which can be specified in a Preset file prior to applying the new settings.
		/// </summary>
		/// <param name="fileName">The file containing Preset data stored in a binary format.</param>
		public void ApplyPresetFromBinary( string fileName )
		{
			this.ApplyPresetFromBinary( fileName, true );
		}

			#endregion // ApplyPresetFromBinary( string )

		#endregion // Preset Application Logic

		// SSP 6/16/05 - NAS 5.3 Column Chooser Feature
		// 
		#region Column Chooser Feature

		#region ShowColumnChooser

		/// <summary>
		/// Displays the column chooser dialog that lets the user hide or unhide columns.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public void ShowColumnChooser( )
		{
			this.ShowColumnChooser( null );
		}

		/// <summary>
		/// Displays the column chooser dialog that lets the user hide or unhide columns.
		/// </summary>
		/// <param name="dialogCaption">Specifies the caption of the column chooser dialog. If null the default caption is used.</param>
		/// <remarks>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public void ShowColumnChooser( string dialogCaption )
		{
			this.ShowColumnChooser( dialogCaption, false );
		}

		/// <summary>
		/// Displays the column chooser dialog that lets the user hide or unhide columns.
		/// </summary>
		/// <param name="band">Specifies the band whose columns to display in the column chooser dialog. If null then columns from all bands are displayed.</param>
		/// <param name="allowBandSelection">Specifies whether the user can select a different band in the dialog.</param>
		/// <remarks>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public void ShowColumnChooser( UltraGridBand band, bool allowBandSelection )
		{
			this.ShowColumnChooser( band, allowBandSelection, null );
		}

		/// <summary>
		/// Displays the column chooser dialog that lets the user hide or unhide columns.
		/// </summary>
		/// <param name="band">Specifies the band whose columns to display in the column chooser dialog. If null then columns from all bands are displayed.</param>
		/// <param name="allowBandSelection">Specifies whether the user can select a different band in the dialog.</param>
		/// <param name="dialogCaption">Specifies the caption of the column chooser dialog. If null the default caption is used.</param>
		/// <remarks>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public void ShowColumnChooser( UltraGridBand band, bool allowBandSelection, string dialogCaption )
		{
			this.ShowColumnChooser( band, allowBandSelection, dialogCaption, false );
		}

		/// <summary>
		/// Displays the column chooser dialog that lets the user hide or unhide columns.
		/// </summary>
		/// <param name="band">Specifies the band whose columns to display in the column chooser dialog. If null then columns from all bands are displayed.</param>
		/// <param name="allowBandSelection">Specifies whether the user can select a different band in the dialog.</param>
		/// <param name="dialogCaption">Specifies the caption of the column chooser dialog. If null the default caption is used.</param>
		/// <param name="disposeOnClose">
		/// Specifies whether to dispose the dialog when the dialog is 
		/// closed. If <b>false</b> the dialog will be cached and used the next time this method
		/// is called. This also maintains the state of the dialog.
		/// </param>
		/// <remarks>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public void ShowColumnChooser( UltraGridBand band, bool allowBandSelection, 
			string dialogCaption, bool disposeOnClose )
		{
			this.ShowColumnChooser( band, allowBandSelection, dialogCaption, disposeOnClose, 
				new Rectangle( -1, -1, 0, 0 ) );
		}

		/// <summary>
		/// Displays the column chooser dialog that lets the user hide or unhide columns.
		/// </summary>
		/// <param name="dialogCaption">Specifies the caption of the column chooser dialog. If null the default caption is used.</param>
		/// <param name="disposeOnClose">
		/// Specifies whether to dispose the dialog when the dialog is 
		/// closed. If <b>false</b> the dialog will be cached and used the next time this method
		/// is called. This also maintains the state of the dialog.
		/// </param>
		/// <remarks>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public void ShowColumnChooser( string dialogCaption, bool disposeOnClose )
		{
			this.ShowColumnChooser( null, true, dialogCaption, disposeOnClose );
		}

		/// <summary>
		/// Displays the column chooser dialog that lets the user hide or unhide columns.
		/// </summary>
		/// <param name="disposeOnClose">
		/// Specifies whether to dispose the dialog when the dialog is 
		/// closed. If <b>false</b> the dialog will be cached and used the next time this method
		/// is called. This also maintains the state of the dialog.
		/// </param>
		/// <remarks>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public void ShowColumnChooser( bool disposeOnClose )
		{
			this.ShowColumnChooser( (UltraGridBand)null, disposeOnClose );
		}

		/// <summary>
		/// Displays the column chooser dialog that lets the user hide or unhide columns.
		/// </summary>
		/// <param name="band">Specifies the band whose columns to display in the column chooser dialog. If null then columns from all bands are displayed.</param>
		/// <param name="allowBandSelection">Specifies whether the user can select a different band in the dialog.</param>
		/// <param name="dialogCaption">Specifies the caption of the column chooser dialog. If null the default caption is used.</param>
		/// <param name="disposeOnClose">
		/// Specifies whether to dispose the dialog when the dialog is 
		/// closed. If <b>false</b> the dialog will be cached and used the next time this method
		/// is called. This also maintains the state of the dialog.
		/// </param>
		/// <param name="dialogRect">
		/// Specifies the location and/or size of the dialog. If rectangle's width is 0 then 
		/// the default width is used. Likewise with the height. if the rectangle's Left 
		/// is -1 then the default left coordinate is used. Likewise with the Top.
		/// </param>
		/// <remarks>
		/// <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public void ShowColumnChooser( UltraGridBand band, bool allowBandSelection, 
					string dialogCaption, bool disposeOnClose, Rectangle dialogRect )
		{
			// If null is specified for the dialogCaption then use the default.
			// 
			if ( null == dialogCaption )
				dialogCaption = SR.GetString( "ColumnChooserDialogCaption" );

			// If default is null then use empty string.
			// 
			if ( null == dialogCaption )
				dialogCaption = string.Empty;

			if ( null == this.cachedColumnChooserDialog || this.cachedColumnChooserDialog.IsDisposed )
				this.cachedColumnChooserDialog = new ColumnChooserDialog( );

			ColumnChooserDialog dialog = this.cachedColumnChooserDialog;

			dialog.DisposeOnClose = disposeOnClose ? DefaultableBoolean.True : DefaultableBoolean.False;

			dialog.Text = dialogCaption;

			// Set the Owner of the dialog to the form this grid is contained in.
			// 
			Form owner = this.DisplayLayout.Form;
            if (null != owner)
            {
                // MBS 6/8/09 - TFS18270
                // It seems that the .NET method CheckParentingCycle doesn't like it when you change
                // the owner to a parent of the current owner (i.e. if the dialog's owner is a floating
                // window, whose owner is the form, and you try to set the owner directly to the form,
                // the CheckParentingCycle method will throw an exception because the new owner is in 
                // the parent chain of the current owner).  We will work around this by simply setting
                // the Owner to null first
                dialog.Owner = null;

                dialog.Owner = owner;
            }

			dialog.ColumnChooserControl.SourceGrid = this;
			dialog.ColumnChooserControl.CurrentBand = band;

			Rectangle rect = dialog.DesktopBounds;

			if ( -1 != dialogRect.Left )
				rect.X = dialogRect.Left;

			if ( -1 != dialogRect.Top )
				rect.Y = dialogRect.Top;

			if ( 0 != dialogRect.Width )
				rect.Width = dialogRect.Width;

			if ( 0 != dialogRect.Height )
				rect.Height = dialogRect.Height;

			// SSP 11/18/05 BR07493
			// 
			if ( -1 != dialogRect.Left || -1 != dialogRect.Top )
				dialog.StartPosition = FormStartPosition.Manual;

			dialog.DesktopBounds = rect;

			// SSP 8/12/05 BR05526
			// 
			dialog.ColumnChooserControl.MultipleBandSupport = allowBandSelection
				? MultipleBandSupport.ShowBandSelectionUI 
				: ( null != band ? MultipleBandSupport.SingleBandOnly : MultipleBandSupport.DisplayColumnsFromAllBands );

			// SSP 4/25/06 - App Styling
			// Set the StyleSetName of the display grid of the column chooser to the style set name of this grid.
			// 
			string styleSetName = this.StyleSetName;
			if ( null != styleSetName && styleSetName.Length > 0 )
				dialog.ColumnChooserControl.DisplayGrid.StyleSetName = styleSetName;

			// AS 1/29/07 NA 2007 Vol 1
			dialog.ColumnChooserControl.DisplayGrid.StyleLibraryName = this.StyleLibraryName;
			dialog.ColumnChooserControl.DisplayGrid.UseAppStyling = this.UseAppStyling;

			BeforeColumnChooserDisplayedEventArgs eventArgs = new BeforeColumnChooserDisplayedEventArgs( dialog );
			this.FireCommonEvent( CommonEventIds.BeforeColumnChooserDisplayed, eventArgs );

			if ( ! eventArgs.Cancel )
			{
				if ( DefaultableBoolean.False != dialog.DisposeOnClose )
					this.cachedColumnChooserDialog = null;

				dialog.Show( );
			}
			else if ( disposeOnClose && null != this.cachedColumnChooserDialog )
			{
				if ( ! this.cachedColumnChooserDialog.IsDisposed )
					this.cachedColumnChooserDialog.Dispose( );
				this.cachedColumnChooserDialog = null;
			}
		}

		#endregion // ShowColumnChooser

		#region RegisterColumnChooser

		internal void RegisterColumnChooser( UltraGridColumnChooser cc )
		{
			if ( null == this.registeredColumnChoosers )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//this.registeredColumnChoosers = new ArrayList( );
				this.registeredColumnChoosers = new List<UltraGridColumnChooser>();
			}

			this.columnChooserHasBeenDispalyedAtLeastOnce = true;

			if ( ! this.registeredColumnChoosers.Contains( cc ) )
				this.registeredColumnChoosers.Add( cc );
		}

		#endregion // RegisterColumnChooser

		#region UnRegisterColumnChooser

		internal void UnRegisterColumnChooser( UltraGridColumnChooser cc )
		{
			if ( null != this.registeredColumnChoosers )
				this.registeredColumnChoosers.Remove( cc );
		}

		#endregion // UnRegisterColumnChooser

		#region GetRegisteredColumnChoosers

		internal UltraGridColumnChooser[] GetRegisteredColumnChoosers( bool visibleOnly )
		{
			if ( null != this.registeredColumnChoosers )
			{
				// Remove any disposed column choosers. This typically shouldn't happen since
				// the UltraGridColumnChooser unregisters itself when it's disposed.
				// 
				for ( int i = this.registeredColumnChoosers.Count - 1; i >= 0; i-- )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//UltraGridColumnChooser cc = (UltraGridColumnChooser)this.registeredColumnChoosers[i];
					UltraGridColumnChooser cc = this.registeredColumnChoosers[ i ];

					if ( null == cc || cc.IsDisposed )
					{
						Debug.Assert( false, "It shouldn't get here since the UltraGridColumnChooser unregisters when it's disposed." );
						this.registeredColumnChoosers.RemoveAt( i );
					}
				}

                if ( 0 == this.registeredColumnChoosers.Count )
					this.registeredColumnChoosers = null;
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics	
			//ArrayList retList = this.registeredColumnChoosers;
			List<UltraGridColumnChooser> retList = this.registeredColumnChoosers;

			// Return visible column choosers only the visible column choosers are requested.
			// 
			if ( visibleOnly )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//retList = new ArrayList( );
				retList = new List<UltraGridColumnChooser>();

				if ( null != this.registeredColumnChoosers )
				{
					foreach ( UltraGridColumnChooser cc in this.registeredColumnChoosers )
					{
						if ( cc.Visible )
							retList.Add( cc );
					}
				}
			}

			return null != retList
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//? (UltraGridColumnChooser[])retList.ToArray( typeof( UltraGridColumnChooser ) )
				? retList.ToArray()
				// Note that the callers are relying on return value being a non-null value
				// for convenience.
				// 
				: new UltraGridColumnChooser[ 0 ];
		}

		#endregion // GetRegisteredColumnChoosers

		#region IsOverColumnChooser

		internal bool IsOverColumnChooser( )
		{
			return this.IsOverColumnChooser( Control.MousePosition, true );
		}
				
		internal bool IsOverColumnChooser( Point mousePos, bool mousePosInScreenCoordinate )
		{
			return null != this.GetColumnChooserUnder( mousePos, mousePosInScreenCoordinate );
		}

		#endregion // IsOverColumnChooser

		#region ShouldScrollGridOnDragMoveHelper

		internal bool ShouldScrollGridOnDragMoveHelper( Point mouseLocInClient )
		{
			// If the mouse is over a column chooser then don't scroll the grid while dragging.
			// 
			if ( this.IsOverColumnChooser( mouseLocInClient, false ) )
				return false;

			// If a column from a column chooser is being dragged then don't scroll the grid
			// until the mouse enters the grid. This is to prevent the UltraGrid from scrolling
			// while the user starts dragging a column from the column chooser towards the 
			// grid.
			// 
			UltraGridColumnChooser cc = this.GetDraggingColumnChooser( );
			if ( null != cc )
				return null != this.cachedColumnChooserDragInfo 
					&& this.cachedColumnChooserDragInfo.mouseHasBeenInGridWhileDragging;

			return true;
		}

		#endregion // ShouldScrollGridOnDragMoveHelper

		#region GetColumnChooserUnder
		
		internal UltraGridColumnChooser GetColumnChooserUnder( Point mousePos, bool mousePosInScreenCoordinate )
		{
			Point mousePosInScreen = mousePosInScreenCoordinate ? mousePos : this.PointToScreen( mousePos );

			foreach ( UltraGridColumnChooser cc in this.GetRegisteredColumnChoosers( true ) )
			{
				Point p = cc.PointToClient( mousePosInScreen );
				if ( cc.ClientRectangle.Contains( p ) )
					return cc;
			}

			return null;
		}

		#endregion // GetColumnChooserUnder

		#region GetItemsBeingDragged

		internal virtual UltraGridColumn[] GetItemsBeingDragged( out bool isDraggingGroupByButton )
		{
			isDraggingGroupByButton = false;

			// Return null. UltraGrid class overrides this and returns meaningful info.
			// UltraCombo and UltraDropDowns currently do not support column dragging.
			// 
			return null;
		}

		#endregion // GetItemsBeingDragged

		#region GetDraggingColumnChooser
		
		internal UltraGridColumnChooser GetDraggingColumnChooser( )
		{
			// If a drag operation is in progress and we have cached the draggingColumnChooser
			// then return it.
			// 
			if ( null != this.cachedColumnChooserDragInfo 
				&& null != this.cachedColumnChooserDragInfo.draggingColumnChooser )
				return this.cachedColumnChooserDragInfo.draggingColumnChooser;

			// Otherwise search all the registered and visible column choosers to see which
			// column chooser has initialiated the column drag.
			// 
			foreach ( UltraGridColumnChooser cc in this.GetRegisteredColumnChoosers( true ) )
			{
				if ( cc.IsDraggingFromThisColumnChooser )
					return cc;
			}

			return null;
		}

		#endregion // GetDraggingColumnChooser

		#region ColumnChooser_OnDragStart

		internal void ColumnChooser_OnDragStart( )
		{
			// Null out any previous cache. ColumnChooser_OnDragEnd also does this.
			// 
			this.cachedColumnChooserDragInfo = null;

			// Get the columns that are being dragged.
			// 
			bool isDraggingGroupByButton;
			UltraGridColumn[] dragColumns = this.GetItemsBeingDragged( out isDraggingGroupByButton );
			if ( null != dragColumns && dragColumns.Length > 0 )
			{
				// If at least one of the drag columns is excluded from the column chooser
				// then return without doing anything.
				// 
				foreach ( UltraGridColumn column in dragColumns )
				{
					if ( ExcludeFromColumnChooser.True == column.ExcludeFromColumnChooserResolved )
						return;
				}

				// Cache the drag columns. Also if the column is being dragged from a column chooser
				// then store that column chooser as well. The drag-and-drop operation needs to make
				// a distinction between whether a column is being dragged from a grid or a column
				// chooser.
				// 
				this.cachedColumnChooserDragInfo = new ColumnChooserCache( );
				this.cachedColumnChooserDragInfo.dragColumns = dragColumns;
				this.cachedColumnChooserDragInfo.draggingColumnChooser = this.GetDraggingColumnChooser( );
				this.cachedColumnChooserDragInfo.isDraggingGroupByButton = isDraggingGroupByButton;

				// Delegate the calls to the column choosers.
				// 
				// MD 7/26/07 - 7.3 Performance
				// OnDragStart does nothing right now, uncomment if logic is added to OnDragStart
				//foreach ( UltraGridColumnChooser cc in this.GetRegisteredColumnChoosers( true ) )
				//    cc.OnDragStart( );
			}
		}

		#endregion // ColumnChooser_OnDragStart

		#region ColumnChooser_OnDragMove

		internal bool ColumnChooser_OnDragMove( Point mouseLocInClient, out CurrentCursor cursorToDisplay )
		{
			bool ret = false;
			cursorToDisplay = CurrentCursor.Default;

			if ( null != this.cachedColumnChooserDragInfo )
			{
				// When dragging a column from a column chooser set the mouseHasBeenInGridWhileDragging 
				// flag if the mouse enters the grid at least once. This is used to prevent the grid
				// from scrolling the columns.
				// 
				if ( ! this.cachedColumnChooserDragInfo.mouseHasBeenInGridWhileDragging )
				{
					UIElement elem = this.DisplayLayout.UIElement.ElementFromPoint( mouseLocInClient );
					if ( null != elem )
						elem = elem.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );

					if ( null != elem && elem.Rect.Contains( mouseLocInClient ) )
						this.cachedColumnChooserDragInfo.mouseHasBeenInGridWhileDragging = true;
				}

				Point mouseLocInScreen = this.PointToScreen( mouseLocInClient );

				// Delegate the calls to the column choosers.
				// 
				foreach ( UltraGridColumnChooser cc in this.GetRegisteredColumnChoosers( true ) )
					cc.OnDragMove( mouseLocInScreen );

				ret = this.ColumnChooser_DragDropHelper( mouseLocInClient, false, out cursorToDisplay );
			}

			return ret;
		}

		#endregion // ColumnChooser_OnDragMove

		#region ColumnChooser_SetHidden

		internal void ColumnChooser_SetHidden( UltraGridColumn column, bool hidden )
		{
			// MD 1/27/09 - Groups in RowLayouts
			// Call off to the new overload
			//this.ColumnChooser_SetHidden(column, hidden, true);
			this.ColumnChooser_SetHidden( column, hidden, null );
		}

		// MD 1/27/09 - Groups in RowLayouts
		internal void ColumnChooser_SetHidden( UltraGridColumn column, bool hidden, ILayoutGroup newParentGroup )
		{
			this.ColumnChooser_SetHidden( column, hidden, newParentGroup, true );
		}

		// SSP 8/11/05 - NAS 5.3 Column Chooser - BR05521
		// Added an overload of ColumnChooser_SetHidden that takes in firePosChangedNotifications parameter.
		// 
		// MD 1/27/09 - Groups in RowLayouts
		// Added a parameter for the new group of the column if it is bing changed.
		//internal void ColumnChooser_SetHidden( UltraGridColumn column, bool hidden, bool firePosChangedNotifications )
		internal void ColumnChooser_SetHidden( UltraGridColumn column, bool hidden, ILayoutGroup newParentGroup, bool firePosChangedNotifications )
		{
			// SSP 8/11/05 - NAS 5.3 Column Chooser - BR05521
			// Added an overload of ColumnChooser_SetHidden that takes in firePosChangedNotifications parameter.
			// 
			firePosChangedNotifications = firePosChangedNotifications && column.Hidden != hidden;
			if ( firePosChangedNotifications )
			{
				BeforeColPosChangedEventArgs e = new BeforeColPosChangedEventArgs( 
					PosChanged.HiddenStateChanged, new ColumnHeader[] { column.Header } );
                this.FireCommonEvent( CommonEventIds.BeforeColPosChanged, e );
				if ( e.Cancel )
					return;
			}

			column.Hidden = hidden;

			// MD 1/27/09 - Groups in RowLayouts
			// Even if Hidden is set t False, the column may still be hidden.
			hidden = column.HiddenResolved;

			// MRS 2/21/06 - BR09836
			// When a column is made visible, we need to make sure that it is not 
			// overlapping any other column in a RowLayout. 
			if (hidden == false &&
				column.Band.UseRowLayoutResolved == true)
			{
				// MD 1/21/09 - Groups in RowLayouts
				// Changed the signature of EnsureItemDoesntOverlap.
				//this.EnsureItemDoesntOverlap(column, true, true, false);
				this.EnsureItemDoesntOverlap( column, true, newParentGroup );
			}

			// If the column is a group-by column then make sure that it's Hidden doesn't
			// always resolve to True.
			// 
			if ( ! hidden && column.IsGroupByColumn )
				column.HiddenWhenGroupByOverride = DefaultableBoolean.False;

			// Also unseleft the column if it was selected.
			// 
			if ( hidden )
				column.Header.Selected = false;

			// SSP 8/11/05 - NAS 5.3 Column Chooser - BR05521
			// Added an overload of ColumnChooser_SetHidden that takes in firePosChangedNotifications parameter.
			// 
			if ( firePosChangedNotifications )
			{
				AfterColPosChangedEventArgs e = new AfterColPosChangedEventArgs(
					PosChanged.HiddenStateChanged, new ColumnHeader[] { column.Header } );
				this.FireCommonEvent( CommonEventIds.AfterColPosChanged, e );
			}
		}

		internal void ColumnChooser_SetHidden( UltraGridBand band, bool hidden )
		{
			if ( hidden == band.Hidden )
				return;

			// SSP 10/18/05 - NAS 5.3 Column Chooser
			// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
			// TestAdvantage.
			// 
			BeforeBandHiddenChangedEventArgs beforeBandHiddenEventArgs = new BeforeBandHiddenChangedEventArgs( band );
			this.FireCommonEvent( CommonEventIds.BeforeBandHiddenChanged, beforeBandHiddenEventArgs );
			if ( beforeBandHiddenEventArgs.Cancel )
				return;

			band.Hidden = hidden;

			// SSP 10/18/05 - NAS 5.3 Column Chooser
			// Added BeforeBandHiddenChanged and AfterBandHiddenChanged events. This was needed by 
			// TestAdvantage.
			// 
			this.FireCommonEvent( CommonEventIds.AfterBandHiddenChanged, new AfterBandHiddenChangedEventArgs( band ) );
		}

		#endregion // ColumnChooser_SetHidden

		#region ColumnChooser_DragDropHelper

		private bool ColumnChooser_DragDropHelper( Point mouseLocInClient, bool performDrop, out CurrentCursor cursorToDisplay )
		{
			cursorToDisplay = CurrentCursor.Default;

			UltraGridColumn[] dragColumns = null != this.cachedColumnChooserDragInfo 
				? this.cachedColumnChooserDragInfo.dragColumns : null;
			if ( null == dragColumns )
				return false;

			bool ret = false;
			UltraGridColumnChooser draggingCC = this.GetDraggingColumnChooser( );
			UltraGridColumnChooser ccUnderMouse = this.GetColumnChooserUnder( mouseLocInClient, false );
			bool mouseOverGrid = this.ControlForGridDisplay.ClientRectangle.Contains( mouseLocInClient );
			bool mouseOverChooser = null != ccUnderMouse;

			// MD 1/27/09 - Groups in RowLayouts
			// Determine the drop group if it has changed.
			ILayoutGroup dropGroup = null;
			if ( dragColumns[ 0 ].Band.RowLayoutStyle == RowLayoutStyle.GroupLayout )
			{
				UIElement element = this.DisplayLayout.UIElement;

				if ( element != null )
					element = element.ElementFromPoint( mouseLocInClient );

				if ( element != null )
				{
					HeaderUIElement headerElement = (HeaderUIElement)element.GetAncestor( typeof( HeaderUIElement ) );
					CellUIElementBase cellElement = (CellUIElementBase)element.GetAncestor( typeof( CellUIElementBase ) );
					
					ILayoutChildItem childItem = null;
					Rectangle elementRect =	Rectangle.Empty;
					if ( cellElement != null )
					{
						childItem = cellElement.Column;
						elementRect = cellElement.Rect;
					}
					else if ( headerElement != null )
					{
						childItem = headerElement.Header;
						elementRect = headerElement.Rect;
					}

					if ( childItem != null )
					{
						GridBagLayoutDragStrategy.DropLocation dropLocation = GridBagLayoutDragStrategy.GetDropLocation( mouseLocInClient, elementRect );
						dropGroup = childItem.GetDropGroup( dropLocation );
					}
				}
			}

			foreach ( UltraGridColumn column in dragColumns )
			{
				if ( null == column || ! column.ColumnChooserEnabledResolved )
					continue;

				// DraggingCC would be null if columns are being dragged from the grid. Otherwise 
				// it would be the instance of column chooser from which the columns are being 
				// dragged from.
				// 
				if ( null == draggingCC )
				{
					if ( mouseOverChooser || ! mouseOverGrid )
					{
						if ( this.cachedColumnChooserDragInfo.isDraggingGroupByButton )
						{
							// If a group-by column button from the group-by box is dragged and dropped
							// onto the column chooser or outside of the grid then ungroup-by the column. 
							// Don't hide or unhide the column though. The hidden state of the column 
							// should be maintained. This is how outlook behaves.
							// 
							if ( DefaultableBoolean.True == column.AllowGroupByResolved )
							{
								if ( performDrop )
								{
									bool origHiddenState = column.Hidden;

									column.SetGroupByColumnStatus( false );

									if ( origHiddenState )
										this.ColumnChooser_SetHidden( column, true );
								}

								ret = true;
							}
						}
						else
						{
							// Hide the column that's dropped over a column chooser or outside
							// of the grid.
							// 
							if ( performDrop )
								this.ColumnChooser_SetHidden( column, true );

							ret = true;
						}

						if ( ret && ! performDrop && ! mouseOverChooser && ! mouseOverGrid )
							cursorToDisplay = CurrentCursor.HideColumnCursor;
					}
					else if ( DefaultableBoolean.True == this.cachedColumnChooserDragInfo.groupByButtonDroppedOverColumnHeaders )
					{
						// If a group-by column that's hidden is dragged and dropped over column
						// headers then unhide it. This emulates Outlook.
						// 
						if ( performDrop )
							this.ColumnChooser_SetHidden( column, false );

						ret = true;
					}
				}
				else // null != draggingCC 
				{
					// If columns are dragged from a column chooser and and dropped over the grid then
					// unhide them.
					// 
					if ( ! mouseOverChooser && mouseOverGrid )
					{
						if ( performDrop )
						{
							if ( DefaultableBoolean.True == this.cachedColumnChooserDragInfo.droppedOverGroupByBox )
							{
								// If the column is dropped over the group-by box then keep it hidden
								// like the outlook.
								// 
								column.HiddenWhenGroupByOverride = ! column.HiddenInternal ? DefaultableBoolean.False : DefaultableBoolean.Default;
							}
							else 
							{
								// MD 1/27/09 - Groups in RowLayouts
								//this.ColumnChooser_SetHidden( column, false );
								this.ColumnChooser_SetHidden( column, false, dropGroup );
							}
						}

						ret = true;
					}
					else if ( mouseOverChooser )
					{
						// If a column is being dragged from a column chooser and the mouse is over
						// the column chooser then display the normal arrow cursor.
						// 
						cursorToDisplay = CurrentCursor.None;
					}
				}
			}

			if ( ret && ! performDrop && CurrentCursor.Default == cursorToDisplay )
				cursorToDisplay = CurrentCursor.Valid;

			return ret;
		}

		#endregion // ColumnChooser_DragDropHelper

		#region ColumnChooser_ColumnsDropped

		private bool ColumnChooser_ColumnsDropped( Point mouseLocInClient )
		{
			CurrentCursor cursorToDisplay;
			return this.ColumnChooser_DragDropHelper( mouseLocInClient, true, out cursorToDisplay );
		}

		#endregion // ColumnChooser_ColumnsDropped

		#region ColumnChooser_OnDragEnd

		internal void ColumnChooser_OnDragEnd( bool canceled, Point mouseLocInClient )
		{
			if ( null != this.cachedColumnChooserDragInfo )
			{
				try
				{
					if ( ! canceled )
						this.ColumnChooser_ColumnsDropped( mouseLocInClient );

					// Delegate the calls to the column choosers.
					// 
					foreach ( UltraGridColumnChooser cc in this.GetRegisteredColumnChoosers( true ) )
						cc.OnDragEnd( );
				}
				finally
				{
					// Null out the cache.
					// 
					this.cachedColumnChooserDragInfo = null;
				}
			}
		}

		#endregion // ColumnChooser_OnDragEnd

		#region RefreshColumnChoosers

		// SSP 3/10/06 BR10744
		// 
		internal void RefreshColumnChoosers( )
		{
			foreach ( UltraGridColumnChooser cc in this.GetRegisteredColumnChoosers( false ) )
				cc.SetNeedsReinitialization( );
		}

		#endregion // RefreshColumnChoosers

		#region HideColumnCursor

		internal Cursor HideColumnCursor
		{
			get
			{
				if ( null == this.hideColumnCursor )
				{
					System.IO.Stream stream	= typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream( typeof( UltraGrid ), "HideColumn16.cur" );
					this.hideColumnCursor = new Cursor( stream );
					stream.Close( );
				}

				return this.hideColumnCursor;
			}
		}

		#endregion // HideColumnCursor

		#endregion // Column Chooser Feature
        
		// MRS 2/21/06 - BR09836
		#region EnsureItemDoesntOverlap
		// MD 1/21/09 - Groups in RowLayouts
		// Changed the signature of this method.
		//internal void EnsureItemDoesntOverlap(UltraGridColumn column, bool right, bool below, bool belowFirst )
		internal void EnsureItemDoesntOverlap( UltraGridColumn column, bool spanXChanged, ILayoutGroup newGroup )
		{
			Debug.Assert( column.Band != null, "Cannot ensure item overlap of a column that does not belong to a band." );
			Debug.Assert( column.Band.Layout == this.DisplayLayout, "Could not ensure that items do not overlap if the column is not associated with the layout of this grid." );
			Debug.Assert( column.Band.Layout.Grid == this, "Could not ensure that items do not overlap if the column is not associated with this grid." );
			Debug.Assert( column.Hidden == false, "The column specified in 'EnsureItemDoesntOverlap' should be visible.");

			if ( column.Band == null || 
				column.Band.Layout != this.DisplayLayout || 
				column.Band.Layout.Grid != this ||
				column.Hidden == true )
			{
				return;
			}

			Debug.Assert( column.Band.UseRowLayoutResolved , "Cannot ensure item overlap of a column that is not using RowLayouts." );			
			
			if (column.Band.UseRowLayoutResolved == false)
				return;

			// create a drag manager since that will provide the info
			// to the drag strategy so it can verify the item's positions
			GridBagLayoutDragManager columnDragManager = new GridBagLayoutDragManager( column, GridBagLayoutDragManager.GridBagLayoutDragType.HeaderReposition );

			// create a temporary drag strategy
			using (GridBagLayoutDragStrategy gridBagLayoutDragStrategy = new GridBagLayoutDragStrategy( columnDragManager ))
			{
				// MD 1/21/09 - Groups in RowLayouts
				// The grid needs to have a reference to the drag strategy which ensuring the item doesn't overlap.
				( (IGridDesignInfo)this ).GridBagLayoutDragStrategy = gridBagLayoutDragStrategy;

				// use the interface to get the info for the method
				IGridBagLayoutDragManager dragManager = columnDragManager as IGridBagLayoutDragManager;
			
				// store the hash table since it will be 
				// manipulated by the ensureitemdoesntoverlap method
				Hashtable hash = dragManager.GetResolvedGCs();
			
				// use the logic in the drag strategy to ensure 
				// that items do not overlap
				// MD 1/15/09 - Groups in RowLayouts
				// This method is now static
				//gridBagLayoutDragStrategy.EnsureItemDoesntOverlap(
				GridBagLayoutDragStrategy.EnsureItemDoesntOverlap(
					dragManager.ItemBeingDragged,
					dragManager.GetVisibleLayoutItems(),
					hash,
					// MD 1/21/09 - Groups in RowLayouts
					// Use the new overload of EnsureItemDoesntOverlap.
					//right, below, belowFirst);
					spanXChanged, null );
			
				// update the layout infos
                // MRS - NAS 9.1 - Groups in RowLayout
				//dragManager.SetResolvedGCs(hash);                
				dragManager.SetResolvedGCs( hash, newGroup );

				// MD 1/21/09 - Groups in RowLayouts
				( (IGridDesignInfo)this ).GridBagLayoutDragStrategy = null;
			}
		}

		#endregion //EnsureItemDoesntOverlap

		// SSP 3/6/06 - App Styling
		// 
		#region App Styling

		#region CreateComponentRole

		/// <summary>
		/// Factory method used to create the component role that provides the style information for the control.
		/// </summary>
		protected override AppStyling.ComponentRole CreateComponentRole()
		{
			return new UltraGridRole( this );
		}

		#endregion // CreateComponentRole

		// AS 1/29/07 NA 2007 Vol 1
		// Technically this should have been done before when we added the component.
		//
		#region InitializeInboxControlStyler
		internal void InitializeInboxControlStyler(Infragistics.Win.AppStyling.Runtime.InboxControlStyler styler)
		{
			styler.DefaultStyleLibraryName = this.StyleLibraryName;
			styler.DefaultStyleSetName = this.StyleSetName;

			if (this.UseAppStyling == false)
				styler.ApplyAppStyling = Infragistics.Win.AppStyling.Runtime.ApplyAppStyling.UseControlSettingAndDefaultToDisabled;
			else
				styler.ApplyAppStyling = Infragistics.Win.AppStyling.Runtime.ApplyAppStyling.UseControlSettingAndDefaultToEnabled;
		} 
		#endregion //InitializeInboxControlStyler

		#endregion // App Styling

		#region InternalHandleDataError

		// SSP 9/15/06 - NAS 6.3
		// 
		internal void InternalHandleDataError( string errorText, Exception exception, bool defaultCancelValue )
		{
			UltraGrid grid = this as UltraGrid;
			if ( null != grid )
			{
				DataErrorInfo dataErrorInfo = new DataErrorInfo( exception, null, null, null, DataErrorSource.Unspecified, errorText );
				grid.InternalHandleDataError( dataErrorInfo, defaultCancelValue );
			}
		}

		#endregion // InternalHandleDataError

        // MRS 5/17/07 - BR22994 (fixed copied from BR18054)
        #region GridControlCodeDomSerializer class

        internal class GridControlCodeDomSerializer : UltraCodeDomSerializer
        {
            /// <summary>
            /// Invoked when the object should serialize itself.
            /// </summary>
            /// <param name="manager">Serialization manager</param>
            /// <param name="value">Object to serialize</param>
            /// <returns>The serialized object information</returns>
            public override object Serialize(IDesignerSerializationManager manager, object value)
            {
                Debug.Assert(value is UltraGridBase, "This code dom serializer wasn't created for a grid control!");

                // Reset the local name cache so sub-objects can use the names they were orignally assigned
                ResetLocalNameCache(manager, value);
                return base.Serialize(manager, value);
            }

            // MRS 4/9/2008 - BR22994
            // This code was promoted to the base class
            #region Commented out
            //// There is a bug in the CLR2 serialization logic where the names of locals will not be cleared after saving
            //// the file.  When local name are then regenerated for sub-objects, they need new names because it teh cache 
            //// is not cleared and it appears the names are already taken.  This clears the cache of local names through 
            //// reflection.
            //private static void ResetLocalNameCache(IDesignerSerializationManager manager, object value)
            //{
            //    try
            //    {
            //        Type cacheType = typeof(CodeDomSerializer).Assembly.GetType("System.ComponentModel.Design.Serialization.ComponentCache");

            //        if (cacheType == null)
            //        {
            //            Debug.Fail("Could not get the component cache type");
            //            return;
            //        }

            //        object componentCache = manager.GetService(cacheType);

            //        if (componentCache == null)
            //            return;

            //        Type cacheEntryType = cacheType.GetNestedType("Entry", BindingFlags.NonPublic);

            //        if (cacheEntryType == null)
            //        {
            //            Debug.Fail("Could not get the component cache entry type");
            //            return;
            //        }

            //        FieldInfo cacheField = cacheType.GetField("cache", BindingFlags.Instance | BindingFlags.NonPublic);

            //        if (cacheField == null)
            //        {
            //            Debug.Fail("Could not get the cache field");
            //            return;
            //        }

            //        object cacheDictionary = cacheField.GetValue(componentCache);

            //        if (cacheDictionary == null)
            //            return;

            //        PropertyInfo indexer = cacheField.FieldType.GetProperty("System.Collections.IDictionary.Item", BindingFlags.Instance | BindingFlags.NonPublic);

            //        if (indexer == null)
            //        {
            //            Debug.Fail("Could not get the indexer");
            //            return;
            //        }

            //        object componentCacheEntry = indexer.GetValue(cacheDictionary, new object[] { value });

            //        if (componentCacheEntry == null)
            //            return;

            //        FieldInfo localNamesField = cacheEntryType.GetField("localNames", BindingFlags.Instance | BindingFlags.NonPublic);

            //        if (localNamesField == null)
            //        {
            //            Debug.Fail("Could not get the local names field");
            //            return;
            //        }

            //        localNamesField.SetValue(componentCacheEntry, null);
            //    }
            //    catch { }
            //}
            #endregion //Commented out
        }

        #endregion //GridControlCodeDomSerializer class

        //  BF 2/28/08  FR09238 - AutoCompleteMode
        #region AutoCompleteFilterCondition
        internal virtual FilterCondition AutoCompleteFilterCondition
        {
            get { return null; }
        }
        #endregion AutoCompleteFilterCondition


        // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel        
        #region MRS NAS v8.3 - Exporting CalcManager Formulas to Excel

        /// <summary>
        /// Returns a <see cref="IUltraCalcReference"/> based on the context, or null if none are found.
        /// </summary>
        /// <param name="context">The context from which to pull the reference.  In the case of the grid, generally this is an
        /// <see cref="UltraGridCell"/>, <see cref="UltraGridRow"/>, <see cref="UltraGridColumn"/>, or <see cref="SummarySettings"/>.</param>
        public static IUltraCalcReference GetReferenceFromContext(object context)
        {           
            UltraGridCell cell = context as UltraGridCell;

            if (cell != null)
                return cell.CalcReference;

            UltraGridRow row = context as UltraGridRow;

            if (row != null)
                return row.CalcReference;

            UltraGridColumn column = context as UltraGridColumn;
            if (column != null)
                return column.CalcReference;

            SummarySettings summarySettings = context as SummarySettings;
            if (summarySettings != null)
                return summarySettings.CalcReference;

            SummaryValue summaryValue = context as SummaryValue;
            if (summaryValue != null)
                return summaryValue.CalcReference;

            return null;
        }

        #endregion // MRS NAS v8.3 - Exporting CalcManager Formulas to Excel

        //  BF 12/2/08
        #region NA 9.1 - UltraCombo MultiSelect
        internal virtual bool CanEditCell( UltraGridRow row, UltraGridColumn column, bool byMouse )
        {
            return this is UltraGrid;
        }
        #endregion NA 9.1 - UltraCombo MultiSelect


        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout        
        
        #region GetLayoutGroupFromPoint
        /// <summary>
        /// Returns the UltraGridGroup which contains the specified point;
        /// </summary>
        /// <param name="point">A point in grid coordinates.</param>
        /// <returns>An UltraGridGroup that contains the specified point or null if the point is not inside a group.</returns>
        /// <remarks>
        /// <para class="body">This method is only valid when <see cref="RowLayoutLabelStyle"/> is set to GroupLayout.</para>
        /// </remarks>
        public UltraGridGroup GetLayoutGroupFromPoint(Point point)
        {
			return this.GetLayoutGroupFromPointInternal( point, this.DisplayLayout.UIElement );
		}

		internal UltraGridGroup GetLayoutGroupFromPointInternal( Point point, UIElement mainElement )
		{
			UIElement containerElement = this.GetLayoutGroupContainerElement( point, mainElement );
            return UltraGridBase.GetLayoutGroupFromPoint(point, containerElement);
        }

        internal UIElement GetLayoutGroupContainerElement(Point point, UIElement mainElement)
        {
            UIElement element = mainElement.ElementFromPoint(point);
            if (element == null)
                return null;

            UIElement containerElement = element;
            while (!(containerElement is BandHeadersUIElement) &&
                !(containerElement is RowCellAreaUIElement) &&
                !(containerElement is CardLabelAreaUIElement) &&
                containerElement != null)
            {
                containerElement = containerElement.Parent;
            }

            return containerElement;
        }        

        /// <summary>
        /// Returns the UltraGridGroup which containts the specified point. 
        /// </summary>
        /// <param name="point">A Point in control coordinates.</param>
        /// <param name="containerElement">The layout containing element. This must be one of the following types: BandHeadersUIElement, RowCellAreaUIElement, or CardLabelAreaUIElement.</param>
        /// <returns></returns>
        public static UltraGridGroup GetLayoutGroupFromPoint(Point point, UIElement containerElement)
        {
			return UltraGridBase.GetLayoutGroupFromPointHelper( point, containerElement, null );
		}

        /// <summary>
        /// Returns the UltraGridGroup which containts the specified point. 
        /// </summary>
        /// <param name="point">A Point in control coordinates.</param>
        /// <param name="containerElement">The layout containing element. This must be one of the following types: BandHeadersUIElement, RowCellAreaUIElement, or CardLabelAreaUIElement.</param>
        /// <param name="group">The highest parent group within which to search.</param>
        /// <returns></returns>
		internal static UltraGridGroup GetLayoutGroupFromPointHelper( Point point, UIElement containerElement, UltraGridGroup group )
		{
            if (containerElement == null)
                return null;

            UltraGridGroup[] groups;

            if (group == null)
            {
                UltraGridBand band = containerElement.GetContext(typeof(UltraGridBand)) as UltraGridBand;
                groups = band.GetLayoutVisibleGroups(true);
            }
            else
                groups = group.GetLayoutVisibleGroups();

            UltraGridGroup groupFromPoint = null;
            foreach (UltraGridGroup childGroup in groups)
            {
                groupFromPoint = UltraGridBase.GetLayoutGroupFromPointHelper(point, containerElement, childGroup);
                if (groupFromPoint != null)
                    return groupFromPoint;
            }

            if (group != null)
            {
                Rectangle groupRect = UltraGridBase.GetLayoutGroupRect(group, containerElement);
                if (groupRect.Contains(point))
                    return group;
            }

            return null;
        }
        #endregion //GetLayoutGroupFromPoint

        #region GetLayoutGroupRect
        /// <summary>
        /// Returns a rectangle bounding a group and all of it's contents when RowLayoutStyle is GroupLayout.
        /// </summary>
        /// <param name="group">The group whose bounding rectangle is to be returned.</param>
        /// <param name="containingElement">A UIElement containing the elements of the group. This must be one of the following types element: <see cref="BandHeadersUIElement"/>, <see cref="CardLabelAreaUIElement"/>, or <see cref="RowUIElement"/></param>
        /// <returns>A rectangle bounding a group and all of it's contents when RowLayoutStyle is GroupLayout.</returns>
        /// <remarks>
        /// <para class="body">The returned rect includes all elements of the group such as the group header, column headers, cells, and any child groups and their contents.</para>
        /// <para class="body">This method is only valid when the <see cref="RowLayoutStyle"/> property is set to GroupLayout.</para>
        /// </remarks>
        public static Rectangle GetLayoutGroupRect(UltraGridGroup group, UIElement containingElement)
        {
            if (group == null)
                throw new ArgumentNullException("group");

            if (containingElement == null)
                throw new ArgumentNullException("containingElement");

            UltraGridBand band = group.Band;

            if (band == null)
                return Rectangle.Empty;

            if (band.RowLayoutStyle != RowLayoutStyle.GroupLayout)
                throw new ArgumentException("This method is only valid for groups whose Band's RowLayoutStyle is to set to GroupLayout.");

            if (containingElement is RowUIElement)
                containingElement = containingElement.GetDescendant(typeof(RowCellAreaUIElement));

            if (containingElement is BandHeadersUIElement == false &&
                containingElement is CardLabelAreaUIElement == false &&
                containingElement is RowCellAreaUIElement == false)
            {
                throw new ArgumentException("containingElement must be a BandHeadersUIElement, RowUIElement, RowCellAreaUIElement, or CardLabelAreaUIElement.");
            }

            List<IProvideRowLayoutColumnInfo> groupsAndColumns = UltraGridBand.GetGroupsAndColumns(band);
            List<UIElement> groupElements = new List<UIElement>();

            foreach (IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo in groupsAndColumns)
            {   
                if (UltraGridGroup.IsDescendantOfGroup(group, iProvideRowLayoutColumnInfo))
                {
                    foreach (UIElement element in containingElement.ChildElements)
                    {
                        if (element.HasContext(iProvideRowLayoutColumnInfo.Header) ||
                            element.HasContext(iProvideRowLayoutColumnInfo))
                        {
                            groupElements.Add(element);
                        }
                    }                    
                }
            }

            if (groupElements.Count == 0)
                return Rectangle.Empty;

            Rectangle rect = groupElements[0].Rect;
            for (int i = 1; i < groupElements.Count; i++)
            {
                rect = Rectangle.Union(rect, groupElements[i].Rect);
            }

            return rect;
        }        
        #endregion //GetLayoutGroupRect

        #region IsChildItemDescendantOfGroup
        private static bool IsChildItemDescendantOfGroup(ILayoutChildItem layoutItem, ILayoutGroup group)
        {
            // Everything is a descendant of the band. 
            if (group is UltraGridBand)
                return true;

            UltraGridGroup testGroup;

            // The group header is technically not contained in the group. It gets added to the 
            // group's parent along with the group content area. But for the purposes of the rect
            // we want to include the header in the group. 
            GroupHeader groupHeader = layoutItem as GroupHeader;
            if (groupHeader != null)
                testGroup = groupHeader.Group;
            else
                testGroup = layoutItem.ParentGroup as UltraGridGroup;

            bool isDescendant = testGroup == group;

            while (isDescendant == false &&
                testGroup != null)
            {
                isDescendant = testGroup == group;
                testGroup = testGroup.RowLayoutGroupInfo.ParentGroup;
            }

            return isDescendant;
        }
        #endregion //IsChildItemDescendantOfGroup

        // MD 1/16/09 - Groups in RowLayout
		#region GridBagLayoutDragStrategy

		GridBagLayoutDragStrategy IGridDesignInfo.GridBagLayoutDragStrategy
		{
			get { return null; }
			set { }
		} 

		#endregion GridBagLayoutDragStrategy

        #endregion // NAS 9.1 - Groups in RowLayout

    }// end of class
}


