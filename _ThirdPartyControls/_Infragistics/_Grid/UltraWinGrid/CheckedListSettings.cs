#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Design;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Windows.Forms.Design;

using Infragistics.Shared;
using Infragistics.Win;
using System.Reflection;
using System.Drawing.Design;
using System.Globalization;

using System.Runtime.Serialization;
using Infragistics.Shared.Serialization;
using System.Collections.Generic;
using System.Text;


namespace Infragistics.Win.UltraWinGrid
{
	#region CheckedListSettings class
	/// <summary>
	/// Exposes properties which control the behavior of the checkboxes displayed in the
    /// dropdown list used by the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo">UltraCombo</see> control.
	/// </summary>
    /// <remarks>
    /// <p class="body">
    /// The CheckedListSettings class provides a way to designate a <see cref="Infragistics.Win.UltraWinGrid.UltraGridColumn">column</see>
    /// as the provider of a logical "checked state" for each associated <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">row</see>.
    /// A collection of the rows whose cell values for this column evaluate to true are returned by the
    /// <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.CheckedRows">CheckedRows</see> collection.
    /// This mechanism provides a way to emulate the behavior exhibited by the .NET <a href="http://msdn.microsoft.com/en-us/library/system.windows.forms.checkedlistbox.aspx">CheckedListBox</a> control.
    /// </p>
    /// <p class="body">
    /// The <see cref="Infragistics.Win.EditorCheckedListSettings.EditorValueSource">EditorValueSource</see> property provides a way
    /// to stipulate that the control's <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.Value">Value</see> property reflect
    /// the checked rows rather than the <see cref="Infragistics.Win.UltraWinGrid.UltraDropDownBase.SelectedRow">SelectedRow</see>,
    /// effectively enabling "multiple row selection".
    /// </p>
    /// <p class="body">
    /// When EditorValueSource is set to 'CheckedItems', the value for each row in the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBase.Rows">Rows</see>
    /// collection whose cell for the <see cref="Infragistics.Win.UltraWinGrid.CheckedListSettings.CheckStateMember">CheckStateMember</see>
    /// column equates to a value of true comprises the control's <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.Value">value</see>. The string
    /// representation of the cell in the <see cref="Infragistics.Win.UltraWinGrid.UltraDropDownBase.DisplayMember">DisplayMember</see> column is displayed
    /// in the edit portion, separated by the <see cref="Infragistics.Win.EditorCheckedListSettings.ListSeparator">ListSeparator</see> character.
    /// </p>
    /// </remarks>
    /// <seealso cref="Infragistics.Win.UltraWinGrid.UltraCombo.CheckedListSettings">CheckedListSettings property (UltraCombo class)</seealso>
    /// <seealso cref="Infragistics.Win.UltraWinGrid.UltraCombo.CheckedRows">CheckedRows property (UltraCombo class)</seealso>
    /// <seealso cref="Infragistics.Win.EditorCheckedListSettings">EditorCheckedListSettings class</seealso>
	public class CheckedListSettings : EditorCheckedListSettings, ISerializable
    {
        #region Member variables

        private string                              checkStateMember = null;
        private UltraCombo                          ultraCombo = null;
        private string                              controlText = string.Empty;
        private List<object>                        lastValues = null;

		#endregion Member variables

		#region Constructor
		/// <summary>
		/// Creates a new instance of the <see cref="CheckedListSettings"/> class.
		/// </summary>
		internal CheckedListSettings( UltraCombo ultraCombo )
        {
            if ( ultraCombo == null )
                throw new ArgumentNullException("ultraCombo");

            this.ultraCombo = ultraCombo;
            RowsCollection rowsCollection = this.ultraCombo.Rows;
            rowsCollection.DataChanged += new DataChangedHandler(this.OnRowsCollectionDataChanged);
            rowsCollection.SubObjectDisposed += new SubObjectDisposedEventHandler(this.OnRowsCollectionDisposed);
            this.ultraCombo.InitializeRowsCollection += new InitializeRowsCollectionEventHandler(this.OnInitializeRowsCollection);
        }

		#endregion Constructor

        #region Properties

            #region IsValueProvidedByCheckedItemList
        internal bool IsValueProvidedByCheckedItemList
        {
            get
            {
                return  this.ultraCombo.CheckStateColumn != null &&
                        this.EditorValueSource == EditorWithComboValueSource.CheckedItems;
            }
        }
            #endregion IsValueProvidedByCheckedItemList

            #region CheckStateMember
        /// <summary>
        /// Returns or sets the key of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridColumn">column</see>
        /// which defines the control's <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.Value">Value</see> when
        /// the <see cref="Infragistics.Win.EditorCheckedListSettings.EditorValueSource">EditorValueSource</see> property
        /// is set to 'CheckedItems'.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// When the CheckStateMember property is not set, or is set to a non-existent column, no other properties of
        /// the <see cref="Infragistics.Win.EditorCheckedListSettings">base</see> class are applicable, and the control
        /// behaves as if all other properties of this instance are left at their default values.
        /// </p>
        /// <p class="body">
        /// The CheckStateMember property need not necessarily reference a <a href="http://msdn.microsoft.com/en-us/library/system.boolean.aspx">boolean</a> column,
        /// but the column's data type must be one that can convert to and from the boolean data type. For example, a column of the string data type could be used,
        /// in accompaniment with a column <see cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.Style">style</see> of 'CheckBox', provided that the cell values
        /// for that column are such that they can be converted to boolean values (i.e., "True" or "False").
        /// </p>
        /// <p class="body">
        /// A collection of the <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">rows</see> whose value for the CheckStateMember
        /// column equates to true is returned by the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.CheckedRows">CheckedRows</see> collection.
        /// </p>
        /// </remarks>
        /// <exception cref="System.Exception">Thrown if the CheckStateMember property is set to the same value as the <see cref="Infragistics.Win.UltraWinGrid.UltraDropDownBase.ValueMember">ValueMember</see> property.</exception>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.UltraCombo.CheckedRows">CheckedRows property (UltraCombo class)</seealso>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.UltraDropDownBase.ValueMember">ValueMember property (UltraCombo class)</seealso>
		[DefaultValue(null)]
		[Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing")]
		[LocalizedDescription("LD_CheckedListSettings_P_CheckStateMember")]
        [LocalizedCategory("LC_Behavior")]
        public string CheckStateMember
        {
            get { return string.IsNullOrEmpty(this.checkStateMember) ? string.Empty : this.checkStateMember; }

            set
            {
                if ( value != this.checkStateMember )
                {
                    //  BF 2/5/09   TFS13564
                    //  Don't allow the CheckStateMember and the ValueMember to be one and the same.
                    this.VerifyCheckStateColumn( PropertyIds.CheckStateMember, this.ultraCombo.ValueMemberResolved, value );

                    this.checkStateMember = value;

                    //  BF 12/22/08
                    //  Dirty up the checked rows when this changes.
                    this.ultraCombo.CheckedRows.DirtyAll();

                    this.NotifyPropChange( PropertyIds.CheckStateMember );
                }
            }
        }

        /// <summary>
        /// Returns whether the <see cref="CheckStateMember"/> property requires serialization.
        /// </summary>
        protected bool ShouldSerializeCheckStateMember()
        {
            return string.IsNullOrEmpty(this.checkStateMember) == false;
        }

        /// <summary>
        /// Restores the value of the <see cref="CheckStateMember"/> property to its default.
        /// </summary>
        public void ResetCheckStateMember()
        {
            this.CheckStateMember = null;
        }
            #endregion CheckStateMember

        #endregion Properties

        #region Methods

            #region ShouldSerialize
        /// <summary>
        /// Returns whether this <see cref="Infragistics.Win.UltraWinGrid.CheckedListSettings">CheckedListSettings</see> instance requires serialization.
        /// </summary>
        public override bool ShouldSerialize()
        {
            return  this.ShouldSerializeCheckStateMember() ||
                    base.ShouldSerialize();
        }
            #endregion ShouldSerialize

            #region Reset
        /// <summary>
        /// Restores all property values for this <see cref="Infragistics.Win.UltraWinGrid.CheckedListSettings">CheckedListSettings</see> instance to their respective defaults.
        /// </summary>
        public override void Reset()
        {
            this.ResetCheckStateMember();
            base.Reset();
        }
            #endregion Reset

            #region OnRowsCollectionDataChanged
        private void OnRowsCollectionDataChanged(object sender, DataChangedEventArgs e)
        {
            UltraGridColumn checkStateColumn = this.ultraCombo.CheckStateColumn;
            if ( checkStateColumn == null )
                return;

            ListChangedEventArgs listChangedArgs = e.ListChangedArgs;

            //  BF 2/10/09  TFS13739
            //  If we got in here because the data source fired ListChanged, but
            //  that was triggered from SetCellValue, it implies that UltraCombo
            //  is bound to UltraDataSource. Since we are going to get this same
            //  notification again when SetCellValue fires the DataChanged event,
            //  we should ignore this notification.
            UltraGridLayout layout = this.ultraCombo.DisplayLayout;
            if ( listChangedArgs != null && layout != null && layout.IsInSetCellValue )
                return;

            string columnKey = e.ColumnKey;
            bool checkStateColumnChanged = string.Equals(columnKey, checkStateColumn.Key, StringComparison.InvariantCultureIgnoreCase);

            bool dirtyAll = listChangedArgs != null && listChangedArgs.ListChangedType != ListChangedType.ItemChanged;
            bool columnRemoved = listChangedArgs != null && listChangedArgs.ListChangedType == ListChangedType.PropertyDescriptorDeleted;
            bool checkStateColumnRemoved = checkStateColumnChanged && columnRemoved;

            if ( checkStateColumnChanged || listChangedArgs != null )
            {
                //  Get the old values before marking the CheckedRows dirty
                CheckedRowsCollection checkedRowsCollection = this.ultraCombo.CheckedRows;

                //  Dirty the collection...if the ListChanged event is what got
                //  us in here, and it was for a reason that could mean the row
                //  or column was removed, dirty everything, otherwise only dirty
                //  the checked rows.
                this.ultraCombo.DirtyCheckedRowsCollection( dirtyAll );

                //  Determine whether the value has changed
                bool valueChanged = checkedRowsCollection.Equals(this.lastValues) == false;
                
                //  Determine whether the "check state" has changed
                bool checkStateChanged =
                    checkStateColumnChanged &&
                    checkStateColumnRemoved == false &&
                    ((listChangedArgs != null && listChangedArgs.ListChangedType == ListChangedType.ItemChanged) || e.Row != null);               

                //  Copy the current values over and hang onto them so we can
                //  compare them the next time we get this notification.
                this.lastValues = new List<object>( checkedRowsCollection.Values );

                //  BF 2/10/09  TFS13739
                //  Only fire CheckStateChanged if something has actually changed.
                if ( checkStateChanged )
                {
                    UltraGridRow checkStateRow = e.Row;

                    //  If the Row property of the event arguments is null,
                    //  try to get a reference to the UltraGridRow from the
                    //  ListChangedEventArgs.
                    if ( checkStateRow == null && listChangedArgs != null &&
                         listChangedArgs.ListChangedType == ListChangedType.ItemChanged )
                    {
                        RowsCollection rowsCollection = sender as RowsCollection;
                        if ( rowsCollection != null )
                        {
                            int oldIndex = listChangedArgs.OldIndex;
                            int newIndex = listChangedArgs.NewIndex;
                            if ( oldIndex == newIndex && newIndex >= 0 && newIndex < rowsCollection.Count )
                                checkStateRow = rowsCollection[newIndex];
                        }
                    }

                    if ( checkStateRow != null )
                        this.ultraCombo.FireCheckStateChanged( checkStateRow );

                }

                //  Have the editor that is in edit mode fire its ValueChanged event;
                //  when the editor is servicing the UltraCombo control, that control's
                //  ValueChanged event will fire, otherwise the embedded editor's event
                //  will be handled (or not) by the consuming control and that control
                //  will broadcast the event.
                UltraGridComboEditor editor = this.ultraCombo.EditorInEditMode;
                bool editorValueProvidedByCheckedItemList = editor != null && editor.InternalIsValueProvidedByCheckedItems;
                bool controlValueProvidedByCheckedItemList = this.IsValueProvidedByCheckedItemList;

                if ( editorValueProvidedByCheckedItemList || controlValueProvidedByCheckedItemList )
                {
                    //  The value has changed if the contents of the CheckedRows
                    //  collection has changed; if it has, fire the appropriate
                    //  events.
                    if ( valueChanged )
                    {
                        //  If the editor is null it implies that the change was triggered
                        //  programmatically by setting one of the control properties, so
                        //  fire the control's ValueChanged event in that case.
                        if ( editor != null && editorValueProvidedByCheckedItemList )
                            editor.InternalRaiseValueChangedEvent();
                        else
                        if ( controlValueProvidedByCheckedItemList )
                            this.ultraCombo.FireEvent(ComboEventIds.ValueChanged, EventArgs.Empty);

                        //  Call RaiseTextChangedEvent, which will fire the event if the new text
                        //  is different that the old text.
                        this.RaiseTextChangedEvent();
                    }
                }

                //  Dirty the control element if a column was removed
                if ( columnRemoved )
                {
                    UIElement controlElement = this.ultraCombo.UIElement;
                    if ( controlElement != null )
                        controlElement.DirtyChildElements( true );
                }

                //  BF 2/6/09   (Related to TFS13643)
                //  Originally I thought (the lack of) this was causing TFS13643,
                //  but it turned out to be something else. Even still, we should
                //  do this, and since there is no UpdateMode property, we have to
                //  do it here.
                if ( checkStateColumnChanged )
                {
                    UltraGridRow row = e.Row;
                    if ( row != null && row.DataChanged )
                        row.EndEdit();
                }
            }
        }
            #endregion OnRowsCollectionDataChanged

            #region OnDispose
        /// <summary>
        /// Called when this object is disposed of.
        /// </summary>
        protected override void OnDispose()
        {
            if ( this.ultraCombo == null )
                return;

            //  Detach from the DataChanged and SubObjectDisposed event sinks
            RowsCollection rowsCollection = this.ultraCombo.Rows;
            if ( rowsCollection != null )
            {
                rowsCollection.DataChanged -= new DataChangedHandler(this.OnRowsCollectionDataChanged);
                rowsCollection.SubObjectDisposed -= new SubObjectDisposedEventHandler(this.OnRowsCollectionDisposed);
            }

            //  Detach from the InitializeRowsCollection event sink
            this.ultraCombo.InitializeRowsCollection -= new InitializeRowsCollectionEventHandler(this.OnInitializeRowsCollection);

            base.OnDispose();
        }
            #endregion OnDispose

            #region SetControlTextHelper
        internal void SetControlTextHelper( string value )
        {
            //  BF 2/5/09   TFS13588
            value = value == null ? string.Empty : value;

            if ( value != this.controlText )
            {
                UltraGridBand bandZero = this.ultraCombo.DisplayLayout.BandZero;

                if ( bandZero != null )
                {
                    ColumnsCollection columns = bandZero.Columns;
                    string displayMember = this.ultraCombo.DisplayMemberResolved;
                    string valueMember = this.ultraCombo.ValueMemberResolved;
                    UltraGridColumn displayMemberColumn = columns.Exists(displayMember) ? columns[displayMember] : null;
                    UltraGridColumn valueMemberColumn = columns.Exists(valueMember) ? columns[valueMember] : null;

                    if ( displayMemberColumn != null && valueMember != null )
                    {
                        //  Split the specified value on the ListSeparator character
                        //  to get a list of the string values, then add each one to
                        //  a dictionary for a unique list that we can use for fast
                        //  lookup.
                        string separator = this.ListSeparatorResolved.Trim();
                        string[] strings = value.Split( separator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
                        Dictionary<string, object> dictionary = new Dictionary<string,object>(StringComparer.InvariantCultureIgnoreCase);
                        foreach( string val in strings )
                        {
                            //  Eliminate leading and trailing whitespace
                            string displayText = val.Trim();

                            if ( dictionary.ContainsKey(displayText) == false )
                                dictionary.Add( displayText, null );
                        }

                        //  Iterate the Rows collection and match each row's text
                        RowsCollection rows = this.ultraCombo.Rows;
                        int itemCount = strings.Length;
                        int foundCount = 0;
                        List<object> dataValues = new List<object>(itemCount);
                        foreach( UltraGridRow row in rows )
                        {
                            string cellText = row.GetCellText( displayMemberColumn );

                            //  If the cell text matches one of the specified string values,
                            //  remove it from the dictionary, increment the count of found
                            //  items, and add it to the list of data values.
                            if ( dictionary.ContainsKey(cellText) )
                            {
                                foundCount++;
                                dictionary.Remove( cellText );
                                object dataValue = row.GetCellValue( valueMemberColumn );
                                dataValues.Add( dataValue );
                            }

                            //  Since we don't include duplicates, we can break this loop
                            //  if the number of items are equal.
                            if ( foundCount == itemCount )
                                break;
                        }

                        //  Set the ICheckedItemList.Value property, which will check the
                        //  appropriate rows and fire ValueChanged and CheckStateChanged.
                        ICheckedItemList checkedItemList = this.ultraCombo as ICheckedItemList;
                        checkedItemList.Value = dataValues;
                    }
                }
            }
        }
            #endregion SetControlTextHelper

            #region SetCheckState
        internal bool SetCheckState( UltraGridRow row, CheckState checkState, bool checkExistingValue, bool fireDataChanged )
        {
            UltraGridColumn checkStateColumn = string.IsNullOrEmpty(this.checkStateMember) == false ?
                this.ultraCombo.CheckStateColumn :
                null;

            return this.SetCheckState( row, checkStateColumn, checkState, checkExistingValue, fireDataChanged );
        }

        internal bool SetCheckState( UltraGridRow row, UltraGridColumn checkStateColumn, CheckState checkState, bool checkExistingValue, bool fireDataChanged )
        {
            if ( row == null || checkStateColumn == null )
                return false;

            bool retVal = false;

            if ( checkStateColumn.IsBound == false )
                retVal = this.SetCheckState_Unbound( row, checkStateColumn, checkState, checkExistingValue, fireDataChanged );
            else
            {
                //  If the caller specified that we should check the existing value,
                //  do that before proceeding, so we avoid triggering a ValueChanged
                //  when nothing has changed.
                if ( checkExistingValue )
                {
                    object currentCellValue = row.GetCellValue( checkStateColumn );
                    CheckState currentCheckState = CheckedListSettings.ToCheckState( checkStateColumn, currentCellValue );
                    if ( checkState == currentCheckState )
                        return true;
                }

                //  Call the row's SetCellValue method to set the value.
                object cellValue = CheckedListSettings.FromCheckState( checkStateColumn, checkState );
                retVal = row.SetCellValue( checkStateColumn, cellValue, true, false, fireDataChanged );
            }

            //  If the dropdown is open, invalidate the row
            if ( retVal && this.ultraCombo.IsDroppedDown )
                row.InvalidateCellAllRegions( checkStateColumn, true, true );

            //  Dirty the control element
            UIElement controlElement = this.ultraCombo.UIElement;
            if ( controlElement != null )
                controlElement.DirtyChildElements( true );

            // CDS 02/02/09 TFS12512 NAS v9.1 Header CheckBox - Synchronization added to support the combination
            // of the CheckBox in Header feature and the CheckList feature of the UltraCombo.
            checkStateColumn.UpdateHeaderCheckBox(row.ParentCollection);

            return retVal;
        }

        private bool SetCheckState_Unbound( UltraGridRow row, UltraGridColumn checkStateColumn, CheckState checkState, bool checkExistingValue, bool fireDataChanged )
        {
            if ( row == null || checkStateColumn == null )
                return false;

            UltraGridCell cell = row.Cells[checkStateColumn];

            //  If the caller specified that we should check the existing value,
            //  do that before proceeding, so we avoid triggering a ValueChanged
            //  when nothing has changed.
            if ( checkExistingValue )
            {
                object currentCellValue = cell.Value;
                CheckState currentCheckState = CheckedListSettings.ToCheckState( checkStateColumn, currentCellValue );
                if ( checkState == currentCheckState )
                    return true;
            }

            //  Assign the new CheckState to the cell's Value property.
            return cell.SetValueInternal( checkState, true, false, false, fireDataChanged );
        }

            #endregion SetCheckState

            #region RaiseTextChangedEvent
        internal void RaiseTextChangedEvent()
        {
            string listSeparator = this.ListSeparatorResolved;
            ICheckedItemList checkedItemList = this.ultraCombo as ICheckedItemList;
            string newText = checkedItemList.GetText( this.ultraCombo.Value as System.Collections.IList, listSeparator );

            if ( string.Equals(newText, this.controlText) == false )
            {
                this.controlText = newText;
                this.ultraCombo.RaiseTextChangedEvent();
            }
        }
            #endregion RaiseTextChanged

            #region ToCheckState
        internal static CheckState ToCheckState( UltraGridColumn checkStateColumn, object cellValue )
        {
            if ( checkStateColumn == null )
            {
                Debug.Fail( "ToCheckState called on a null column - unexpected." );
                return CheckState.Indeterminate;
            }

            //  BF 2/10/09  TFS13645
            //  We can handle typeof(string) correctly, but we have to manually convert.
            Type dataType = checkStateColumn.DataType;

            if ( dataType == typeof(string) && cellValue is string )
            {
                string cellStringValue = cellValue as string;

                if ( string.IsNullOrEmpty(cellStringValue) == false )
                {
                    //  Boolean
                    string trueString = true.ToString(checkStateColumn.FormatInfo);
                    string falseString = false.ToString(checkStateColumn.FormatInfo);

                    if ( string.Equals(cellStringValue, trueString, StringComparison.InvariantCultureIgnoreCase) ||
                         string.Equals(cellStringValue, falseString, StringComparison.InvariantCultureIgnoreCase) )
                        return string.Equals(cellStringValue, trueString) ? CheckState.Checked : CheckState.Unchecked;

                    //  CheckState
                    string checkedString = CheckState.Checked.ToString();
                    string uncheckedString = CheckState.Unchecked.ToString();
                    string indeterminateString = CheckState.Indeterminate.ToString();

                    bool matchesChecked = string.Equals(cellStringValue, checkedString, StringComparison.InvariantCultureIgnoreCase);
                    bool matchesUnchecked = string.Equals(cellStringValue, uncheckedString, StringComparison.InvariantCultureIgnoreCase);
                    bool matchesIndeterminate = string.Equals(cellStringValue, indeterminateString, StringComparison.InvariantCultureIgnoreCase);

                    if ( matchesChecked || matchesUnchecked || matchesIndeterminate )
                        return matchesChecked ? CheckState.Checked : matchesUnchecked ? CheckState.Unchecked : CheckState.Indeterminate;
                }
                else
                    //  A null or empty string translates to 'Indeterminate'
                    return CheckState.Indeterminate;
            }

            object convertedCellValue = checkStateColumn.ConvertValueToDataType( cellValue );

            CheckState checkState = CheckedListSettings.IsNull(convertedCellValue) ?
                CheckState.Indeterminate :
                convertedCellValue is CheckState ?
                (CheckState)convertedCellValue :
                convertedCellValue is bool && (bool)convertedCellValue ?
                CheckState.Checked :
                CheckState.Unchecked;

            return checkState;
        }
            #endregion ToCheckState

            #region FromCheckState
        internal static object FromCheckState( UltraGridColumn checkStateColumn, CheckState checkState )
        {
            if ( checkStateColumn == null )
            {
                Debug.Fail( "ToCheckState called on a null column - unexpected." );
                return CheckState.Indeterminate;
            }

            Type dataType = checkStateColumn.DataType;

            //  If the column's type is CheckState, we don't have to translate anything
            if ( dataType == typeof(CheckState) )
                return checkState;
            else
            //  If the column's type is string, we have to translate to a value
            //  that can be parsed into a boolean value, because "Checked" and
            //  "Unchecked" don't get parsed into true/false.
            if ( dataType == typeof(string) )
            {
                string trueString = true.ToString(checkStateColumn.FormatInfo);
                string falseString = false.ToString(checkStateColumn.FormatInfo);

                return checkState == CheckState.Checked ? trueString : checkState == CheckState.Unchecked ? falseString : null;
            }

            //  Let the column convert the value
            object convertedCellValue = checkStateColumn.ConvertValueToDataType( checkState );
            return convertedCellValue;
        }
            #endregion FromCheckState

            #region IsNull
        internal static bool IsNull( object value ) { return value == null || value == DBNull.Value; }
            #endregion IsNull

            //  BF 2/5/09   TFS13564
            #region VerifyCheckStateColumn
        internal void VerifyCheckStateColumn( Nullable<PropertyIds> propId, string valueMemberResolved )
        {   
            this.VerifyCheckStateColumn( propId, valueMemberResolved, this.checkStateMember );
        }

        internal void VerifyCheckStateColumn( Nullable<PropertyIds> propId, string valueMemberResolved, string checkStateMember )
        {   
            //  Don't verify direct property settings while initializing, so we don't
            //  throw an exception for the wrong property.
            if ( propId.HasValue && this.ultraCombo.Initializing )
                return;
            
            if ( string.IsNullOrEmpty(valueMemberResolved) || string.IsNullOrEmpty(checkStateMember) )
                return;

            if ( string.Equals(valueMemberResolved, checkStateMember, StringComparison.InvariantCultureIgnoreCase) )
            {
                string exceptionMessage = propId.HasValue && propId.Value == PropertyIds.ValueMember ?
                    SR.GetString("UltraCombo_E_ValueMemberCannotEqualCheckStateMember", valueMemberResolved) :
                    propId.HasValue && propId.Value == PropertyIds.CheckStateMember ?
                    SR.GetString("CheckedListSettings_E_CheckStateMemberCannotEqualValueMember", valueMemberResolved) :
                    SR.GetString("UltraCombo_E_ValueMemberAndCheckStateMemberAreSame", valueMemberResolved);
                    
                throw new Exception( exceptionMessage );
            }
        }
            #endregion VerifyCheckStateColumn

            //  BF 2/5/09   TFS13588
            #region OnInitializeRowsCollection
        private void OnInitializeRowsCollection(object sender, InitializeRowsCollectionEventArgs e)
        {
            //  Hook DataChanged and SubObjectDisposed when the collection is created
            UltraCombo ultraCombo = sender as UltraCombo;
            if ( ultraCombo == this.ultraCombo )
            {
                RowsCollection rowsCollection = e.Rows;
                if ( rowsCollection != null )
                {
                    DataChangedHandler dataChangedHandler = new DataChangedHandler(this.OnRowsCollectionDataChanged);
                    rowsCollection.DataChanged -= dataChangedHandler;
                    rowsCollection.DataChanged += dataChangedHandler;

                    SubObjectDisposedEventHandler subObjectDisposedHandler = new SubObjectDisposedEventHandler(this.OnRowsCollectionDisposed);
                    rowsCollection.SubObjectDisposed -= subObjectDisposedHandler;
                    rowsCollection.SubObjectDisposed += subObjectDisposedHandler;
                }
            }
            else
                Debug.Fail( "OnInitializeRowsCollection should not be getting called." );

        }
            #endregion OnInitializeRowsCollection

            //  BF 2/5/09   TFS13588
            #region OnRowsCollectionDisposed
        private void OnRowsCollectionDisposed(SubObjectBase notifier)
        {
            //  Unhook DataChanged and SubObjectDisposed when the collection is disposed of
            RowsCollection rowsCollection = notifier as RowsCollection;
            if ( rowsCollection != null )
            {
                rowsCollection.DataChanged -= new DataChangedHandler(this.OnRowsCollectionDataChanged);
                rowsCollection.SubObjectDisposed -= new SubObjectDisposedEventHandler(this.OnRowsCollectionDisposed);
                this.ultraCombo.DirtyCheckedRowsCollection( true );
            }
            else
                Debug.Fail( "OnRowsCollectionDisposed should not be getting called." );
        }
            #endregion OnRowsCollectionDisposed

        #endregion Methods

        #region Serialization

        /// <summary>
		/// Creates a new instance of the <see cref="CheckedListSettings"/> class from deserialized data.
		/// </summary>
		/// <param name="info">SerializationInfo</param>
		/// <param name="context">StreamingContext</param>
		protected CheckedListSettings( SerializationInfo info, StreamingContext context ) : base( info, context )
		{
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{						
					case "CheckStateMember":
						this.checkStateMember = (string)Utils.DeserializeProperty( entry, typeof(string), this.checkStateMember );
						break;
				}
			}
		}

		[System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, Flags=System.Security.Permissions.SecurityPermissionFlag.SerializationFormatter)]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			if ( this.ShouldSerializeCheckStateMember() )
				Utils.SerializeProperty( info, "CheckStateMember", this.checkStateMember );

            base.GetObjectData( info, context );
		}

        #endregion Serialization

        #region DataChangedEventArgs class
        internal class DataChangedEventArgs : EventArgs
        {
            #region Member variables
            private UltraGridRow row = null;
            private string columnKey = string.Empty;
            private ListChangedEventArgs listChangedArgs = null;
            #endregion Member variables

            #region Constructor
            internal DataChangedEventArgs( UltraGridRow row, string columnKey, ListChangedEventArgs listChangedArgs )
            {
                this.row = row;
                this.columnKey = columnKey;
                this.listChangedArgs = listChangedArgs;
            }
            #endregion Constructor

            #region Row
            internal UltraGridRow Row { get { return this.row; } }
            #endregion Row

            #region ColumnKey
            internal string ColumnKey { get { return this.columnKey; } }
            #endregion ColumnKey

            #region ListChangedArgs
            internal ListChangedEventArgs ListChangedArgs { get { return this.listChangedArgs; } }
            #endregion ListChangedArgs
        }
        #endregion DataChangedEventArgs class

        #region DataChangedHandler delegate

        internal delegate void DataChangedHandler( object sender, DataChangedEventArgs e );

        #endregion DataChangedHandler delegate
    }
    #endregion CheckedListSettings class

    #region CheckedRowsCollection class
    /// <summary>
    /// Encapsulates the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.CheckedRows">CheckedRows</see> collection.
    /// </summary>
    public class CheckedRowsCollection :  SubObjectsCollectionBase,
                                          IEnumerable<UltraGridRow>
    {

        #region Member variables
        
        private Dictionary<object, UltraGridRow> allRows = null;
        private Dictionary<object, UltraGridRow> checkedRows = null;
        private UltraCombo ultraCombo = null;
        private bool allDirty = true;
        private bool checkedRowsDirty = true;
        private List<object> values = null;
        private int rowFiltersVersion = int.MinValue;

        #endregion Member variables

        #region Constructors
        internal CheckedRowsCollection( UltraCombo ultraCombo )
        {
            if ( ultraCombo == null )
                throw new ArgumentNullException("ultraCombo");

            this.ultraCombo = ultraCombo;

            UltraGridBand bandzero = this.ultraCombo.DisplayLayout.BandZero;
            this.rowFiltersVersion = bandzero != null ? bandzero.RowFiltersVersion : int.MinValue;
        }

        #endregion Constructors

        #region AllRows
        private Dictionary<object, UltraGridRow> AllRows
        {
            get
            {
                this.VerifyCollection();

                return this.allRows;
            }
        }
        #endregion AllRows

        #region CheckedRows
        private Dictionary<object, UltraGridRow> CheckedRows
        {
            get
            {
                this.VerifyCollection();

                return this.checkedRows;
            }
        }
        #endregion CheckedRows

        #region Indexer
        /// <summary>
        /// Returns the <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">UltraGridRow</see>
        /// at the specified ordinal position within this collection.
        /// </summary>
        /// <param name="index">The ordinal position of the row within this collection.</param>
        /// <remarks>
        /// <p class="note">
        /// The elements of this collection can be indexed by their associated
        /// "data values", i.e., the value of the cell that intersects with the column referenced by the
        /// <see cref="Infragistics.Win.UltraWinGrid.UltraDropDownBase.ValueMember">ValueMember</see>
        /// property. If the value is of type integer, however, the integer must be boxed
        /// into a variable of type object in order to access that indexer overload; passing
        /// an integer directly will cause execution to route through this overload, and the
        /// parameter value will be interpreted as an index, not a data value.
        /// </p>
        /// </remarks>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.CheckedRowsCollection.this[object]">this[object] indexer</seealso>
        public UltraGridRow this[int index]
        {
            get { return this.List[index] as UltraGridRow; }
        }

        /// <summary>
        /// Returns the <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">UltraGridRow</see>
        /// whose cell value for the column referenced by the
        /// <see cref="Infragistics.Win.UltraWinGrid.UltraDropDownBase.ValueMember">ValueMember</see>
        /// property is equal to the specified <paramref name="value"/>
        /// </summary>
        /// <param name="value">The value to test.</param>
        /// <remarks>
        /// <p class="note">
        /// This indexer provides a way to access a <see cref="Infragistics.Win.UltraWinGrid">row</see>
        /// by its associated "data value", i.e., the value of the cell that intersects with the
        /// column referenced by the <see cref="Infragistics.Win.UltraWinGrid.UltraDropDownBase.ValueMember">ValueMember</see>
        /// property. A standard indexer is also exposed, the 'index' parameter of which is implied to be
        /// the ordinal position of the element to access. Use that overload when the index of the row within
        /// this collection is already available.
        /// </p>
        /// </remarks>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.CheckedRowsCollection.this[int]">this[int] indexer</seealso>
        public UltraGridRow this[object value]
        {
            get
            { 
                this.VerifyCollection();
                UltraGridRow row = null;
                this.CheckedRows.TryGetValue( value, out row );
                return row;
            }
        }
        #endregion Indexer

        #region Count
        /// <summary>
        /// Returns the number of <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">Rows</see>
        /// contained within this collection.
        /// </summary>
        public override int Count
        {
            get
            { 
                this.VerifyCollection();
                return this.CheckedRows.Count;
            }
        }
        #endregion Count

        #region VerifyCollection
        private void VerifyCollection()
        {
            //  BF 2/6/09   TFS13516
            this.allDirty = this.allDirty || this.VerifyRowFiltersVersion();

            if ( this.allDirty || this.checkedRowsDirty )
            {
                this.values = null;

                bool dirtyAll = this.allDirty;
                bool dirtyCheckedRows = dirtyAll || this.checkedRowsDirty;
                
                if ( this.allRows == null )
                    this.allRows = new Dictionary<object, UltraGridRow>( this.ultraCombo.Rows.Count );

                if ( this.checkedRows == null )
                    this.checkedRows = new Dictionary<object, UltraGridRow>();

                if ( dirtyAll )
                {
                    this.allRows.Clear();
                    this.allDirty = false;
                }

                if ( dirtyCheckedRows )
                {
                    this.ClearCheckedRows();
                    this.checkedRowsDirty = false;
                }

                UltraGridBand bandZero = this.ultraCombo.DisplayLayout.BandZero;
                if ( bandZero != null )
                {
                    string valueMember = this.ultraCombo.ValueMemberResolved;
                    string checkStateMember = this.ultraCombo.CheckedListSettings.CheckStateMember;

                    UltraGridColumn valueColumn = bandZero != null && bandZero.Columns.Exists(valueMember) ? bandZero.Columns[valueMember] : null;
                    UltraGridColumn checkStateColumn = bandZero != null && bandZero.Columns.Exists(checkStateMember) ? bandZero.Columns[checkStateMember] : null;

                    if ( valueColumn != null && checkStateColumn != null )
                    {
                        RowsCollection rowsCollection = this.ultraCombo.Rows;
                        foreach ( UltraGridRow row in rowsCollection )
                        {
                            //  BF 2/6/09   TFS13516
                            if ( row.IsFilteredOut )
                                continue;

                            object checkStateCellValueRaw = checkStateColumn.IsBound ?
                                row.GetCellValue(checkStateColumn) :
                                row.Cells[checkStateColumn].Value;

                            object valueColumnCellValue = valueColumn.IsBound ?
                                row.GetCellValue(valueColumn) :
                                row.Cells[valueColumn].Value;

                            if ( dirtyAll &&
                                 this.allRows.ContainsKey(valueColumnCellValue) == false )
                                this.allRows.Add( valueColumnCellValue, row );

                            if ( dirtyCheckedRows )
                            {
                                CheckState checkState = CheckedListSettings.ToCheckState(checkStateColumn, checkStateCellValueRaw);
                                bool isChecked = (checkState == CheckState.Checked);
                                if ( isChecked && this.checkedRows.ContainsKey(valueColumnCellValue) == false )
                                    this.AddCheckedRow( valueColumnCellValue, row );


                            }
                        }
                    }
                }
            }
        }
        #endregion VerifyCollection

        #region VerifyRowFiltersVersion
        private bool VerifyRowFiltersVersion()
        {
            UltraGridBand bandZero = this.ultraCombo.DisplayLayout.BandZero;
            if ( bandZero == null )
                return false;

            int bandVersion = bandZero.RowFiltersVersion;
            bool different = bandVersion != this.rowFiltersVersion;
            this.rowFiltersVersion = bandVersion;

            return different;
        }
        #endregion VerifyRowFiltersVersion

        #region AddCheckedRow
        private void AddCheckedRow( object dataValue, UltraGridRow row )
        {
            //  Keep the inner list synchronized so that things like GetItem work correctly.
            this.List.Add( row );
            this.checkedRows.Add( dataValue, row );
        }
        #endregion AddCheckedRow

        #region ClearCheckedRows
        private void ClearCheckedRows()
        {
            this.List.Clear();

            if ( this.checkedRows != null )
                this.checkedRows.Clear();
        }
        #endregion ClearCheckedRows

        #region DirtyAll
        internal void DirtyAll()
        { 
            this.allDirty = true;
            this.checkedRowsDirty = true;
        }
        #endregion DirtyAll

        #region DirtyCheckedRows
        internal void DirtyCheckedRows(){ this.checkedRowsDirty = true; }
        #endregion DirtyCheckedRows

        #region Values
        internal List<object> Values
        {
            get
            {
                this.VerifyCollection();

                if ( this.values == null )
                {
                    Dictionary<object, UltraGridRow> dictionary = this.CheckedRows;
                    this.values = new List<object>( dictionary.Count );

                    foreach( KeyValuePair<object, UltraGridRow> pair in dictionary )
                    {
                        this.values.Add( pair.Key );
                    }
                }

                return this.values;
            }
        }
        #endregion Values

        #region Equals
        internal bool Equals( List<object> values )
        {
            this.VerifyCollection();
            
            //  If the specified list of values is null or empty,
            //  retutn true if the CheckedRows collection is empty,
            //  false otherwise.
            bool isNullOrEmpty = (values == null || values.Count == 0);
            if ( isNullOrEmpty )
                return this.checkedRows.Count == 0;

            //  If the counts are different, they are not equal
            if ( this.checkedRows.Count != values.Count )
                return false;

            //  If any one value in the list is not found,
            //  they are not equal
            foreach ( object value in values )
            {
                if ( this[value] == null )
                    return false;
            }

            return true;
        }
        #endregion Equals

        #region ToString
        /// <summary>
        /// Returns the string representation of each <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">row</see>
        /// in this collection.
        /// </summary>
        public override string ToString()
        {
            string listSeparator = this.ultraCombo.HasCheckedListSettings ?
                this.ultraCombo.CheckedListSettings.ListSeparatorResolved :
                System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

            return this.ToString( listSeparator );
        }

        /// <summary>
        /// Returns the string representation of each <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow">row</see>
        /// in this collection, with each entry except for the last followed by the specified <paramref name="listSeparator"/>.
        /// </summary>
        /// <param name="listSeparator">The string to be appended after each entry, except for the last one.</param>
        /// <returns>The string representation of this instance, based on the contents of the associated collection.</returns>
        public string ToString( string listSeparator )
        {
            this.VerifyCollection();
            Dictionary<object, UltraGridRow> checkedRows = this.CheckedRows;
            if ( checkedRows.Count == 0 )
                return string.Empty;

            List<object> values = new List<object>( checkedRows.Count );
            foreach( KeyValuePair<object, UltraGridRow> pair in checkedRows )
            {
                values.Add( pair.Key );
            }

            return this.ToString( values, listSeparator );
        }

        internal string ToString( System.Collections.IList values, string listSeparator )
        {
            if ( values == null || values.Count == 0 )
                return string.Empty;

            UltraGridBand bandZero = this.ultraCombo.DisplayLayout.BandZero;
            string displayMember = this.ultraCombo.DisplayMemberResolved;
            UltraGridColumn displayMemberColumn = bandZero != null && bandZero.Columns.Exists(displayMember) ? bandZero.Columns[displayMember] : null;

            if ( displayMemberColumn == null )
                return string.Empty;

            this.VerifyCollection();

            Dictionary<object, UltraGridRow> allRows = this.AllRows;
            if ( allRows.Count == 0 )
                return string.Empty;

            List<string> strings = new List<string>(values.Count);

            for ( int i = 0, count  = values.Count; i < count; i ++ ) 
            {
                UltraGridRow row = null;
                object value = values[i];
                if ( allRows.TryGetValue(value, out row) )
                {
                    string displayString = row.GetCellText( displayMemberColumn );
                    if ( string.IsNullOrEmpty(displayString) )
                        continue;

                    strings.Add( displayString );
                }
            }

            if ( strings.Count == 0 )
                return string.Empty;

            StringBuilder sb = new StringBuilder();

            for ( int i = 0, count = strings.Count; i < count; i ++ )
            {
                sb.Append( strings[i] );

                if ( i < (count - 1) )
                    sb.Append( listSeparator );
            }

            return sb.ToString();
        }

        #endregion ToString

        #region Dispose
        /// <summary>
        /// Releases any resources used by this instance.
        /// </summary>
        public override void Dispose()
        {
            //  Nullify the dictionary we use to hash the values/rows
            if ( this.allRows != null )
                this.allRows = null;

            //  Nullify the dictionary we use to hash the values/rows
            if ( this.checkedRows != null )
                this.checkedRows = null;

            //  Call the base class implementation
            base.Dispose();
        }
        #endregion Dispose

        #region SubObjectsCollectionBase overrides

            #region All
        /// <summary>
        /// Returns the contents of this collection as an array.
        /// </summary>
        public override object[] All
        {
            get
            {
                Dictionary<object, UltraGridRow>.ValueCollection rows = this.CheckedRows.Values;
                UltraGridRow[] rowArray = new UltraGridRow[rows.Count];
                rows.CopyTo( rowArray, 0 );
                return rowArray;
            }

            set { throw new NotSupportedException(); }
        }
            #endregion All

            #region InitialCapacity
        /// <summary>
        /// Returns the inital size of this collection.
        /// </summary>
        protected override int InitialCapacity
        {
            get { return 5; }
        }
            #endregion InitialCapacity

            #region IsReadOnly
        /// <summary>
        /// Returns true since this collection cannot be modified.
        /// </summary>
        public override bool IsReadOnly
        {
            get { return true; }
        }
            #endregion IsReadOnly

        #endregion SubObjectsCollectionBase overrides

        #region IEnumerable<UltraGridRowRow> Members

        IEnumerator<UltraGridRow> IEnumerable<UltraGridRow>.GetEnumerator()
        {
            this.VerifyCollection();

            Dictionary<object, UltraGridRow>.ValueCollection rows = this.CheckedRows.Values;
            foreach ( UltraGridRow row in rows )
            {
                yield return row;
            }
        }

        #endregion IEnumerable<UltraGridRow> Members

    }
    #endregion CheckedRowsCollection class

}
