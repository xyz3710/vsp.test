#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.Globalization;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using Infragistics.Win.UltraWinMaskedEdit;
	using System.Drawing.Design;
	using System.Windows.Forms.Design;
	using System.Text;
	using System.Security.Permissions;
	using Infragistics.Shared.Serialization;

	// SSP 3/21/02
	// Version 2 code.
	// Row Filter Implementation
	// Added FilterCondition class
	// 

	// SSP 7/15/02 UWG1367
	// Implemented ISerialzable interface.
	//

	/// <summary>
	/// Class used for defining a row filter condition.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// A <b>FilterCondition</b> object defines a single condition.
	/// Multiple FilterCondition instances can be added to the 
	/// <see cref="FilterConditionsCollection"/>. 
	/// A <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/> instance
	/// contains a <b>FilterConditionsCollection</b> instance. The <b>ColumnFilter</b>
	/// has <see cref="ColumnFilter.LogicalOperator"/> property which specifies
	/// how multiple conditions contained in the ColumnFilter's FilterConditionCollection
	/// are to be combined.
	/// A <see cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/> 
	/// can contain multiple <b>ColumnFilter</b> instances. Both the
	/// <see cref="UltraGridBand"/> and <see cref="RowsCollection"/> objects expose
	/// <b>ColumnFilters</b> property. This property returns a collection of
	/// <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/> objects.
	/// UltraGrid will filter rows using either the 
	/// RowsCollection's <see cref="RowsCollection.ColumnFilters"/> or 
	/// UltraGridBand's <see cref="UltraGridBand.ColumnFilters"/> depending on the
	/// what the Override's <see cref="UltraGridOverride.RowFilterMode"/> property
	/// is set to. See <see cref="UltraGridOverride.RowFilterMode"/> for more information.
	/// </p>
	/// <seealso cref="FilterConditionsCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter.FilterConditions"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/>
	/// <seealso cref="UltraGridBand.ColumnFilters"/>
	/// <seealso cref="RowsCollection.ColumnFilters"/>
	/// <seealso cref="UltraGridOverride.RowFilterMode"/>
	/// <seealso cref="UltraGridOverride.FilterUIType"/>
	/// </remarks>
	[ Serializable() ]
	public class FilterCondition : SubObjectBase, ISerializable
		// SSP 8/21/03 UWG2603
		// Added a way for the users to implement their own custom filtering logic.
		// Implemented ICloneable interface.
		//
		, ICloneable
	{
		#region Private Vars

		private FilterConditionsCollection	parentCollection = null;		
		private object						compareValue	 = null;
		private FilterComparisionOperator	comparisionOperator;

		private System.Text.RegularExpressions.Regex	cachedRegex			= null;
		private string									cachedRegexPattern	= null;

		// SSP 5/16/05 - NAS 5.2 FilterComparisonType property
		// Perform case insensitive regex matching based on the new FilterComparisonType 
		// property setting.
		//
		private System.Text.RegularExpressions.RegexOptions cachedRegexOptions = System.Text.RegularExpressions.RegexOptions.None;

		// SSP 8/13/02 UWG1556
		// When converting the compareValue to the column's data type, if conversion is
		// not successful it will throw exceptions and exceptions slow things down. So
		// cache whether we were able to convert the value or not.
		//
		private bool   lastConversionSuccessfull = false;
		private object lastAttemptedValueForConversion = null;
		private object lastConvertedValue = null;
		private Type   lastConversionDataType = null;

		// SSP 8/1/03 UWG1654 - Filter Action
		// Added a way for the user to be able to construct filter conditions and evaluate
		// it themselves by calling the new UltraGridRow.MeetsCriteria method.
		// Added a column variable.
		//
		private UltraGridColumn column = null;

		#endregion // Private Vars

		#region Constructor

		internal FilterCondition( 
			FilterConditionsCollection parentCollection, 
			FilterComparisionOperator comparisionOperator,
			object compareValue )
		{
			if ( null == parentCollection )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_43"), Shared.SR.GetString("LE_ArgumentNullException_328") );

			if ( !Enum.IsDefined( typeof( FilterComparisionOperator ), comparisionOperator ) )
				throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_129"), (int)comparisionOperator, typeof( FilterComparisionOperator ) );

            // JDN 12/20/04 BR00867
            // Ensure the search pattern is valid
            FilterCondition.ValidateSearchPattern( comparisionOperator, compareValue );

			this.comparisionOperator = comparisionOperator;
			this.compareValue = compareValue;

			// SSP 5/5/05
			//
			//this.parentCollection = parentCollection;
			this.Initialize( parentCollection );
		}

		// SSP 8/1/03 UWG1654 - Filter Action
		// Added a way for the user to be able to construct filter conditions and evaluate
		// it themselves by calling the new UltraGridRow.MeetsCriteria method.
		// Added the following constructor.
		//
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="column">Value of this column will be compared with the <b>compareVlaue</b> by the specified comparision operator.</param>
		/// <param name="comparisionOperator">Comparision operator.</param>
		/// <param name="compareValue">This value will be compared with the <b>column</b>'s value by the specified comparision operator.</param>
		public FilterCondition( 
			UltraGridColumn column,
			FilterComparisionOperator comparisionOperator, 
			object compareValue )
		{
			if ( !Enum.IsDefined( typeof( FilterComparisionOperator ), comparisionOperator ) )
				throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_129"), (int)comparisionOperator, typeof( FilterComparisionOperator ) );

			if ( null == column )
				throw new ArgumentNullException( "column" );

            // JDN 12/20/04 BR00867
            // Ensure the search pattern is valid
            FilterCondition.ValidateSearchPattern( comparisionOperator, compareValue );

			this.comparisionOperator = comparisionOperator;
			this.compareValue = compareValue;
			this.column = column;
		}

		// SSP 6/28/05
		// Added a convenient constructor.
		// 
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="comparisionOperator">Comparision operator.</param>
		/// <param name="compareValue">This value will be compared with the <b>column</b>'s value by the specified comparision operator.</param>
		public FilterCondition( FilterComparisionOperator comparisionOperator, object compareValue )
		{
			
			
			
			this.comparisionOperator = comparisionOperator;
			this.compareValue = compareValue;
		}

        // MBS 12/3/08 - NA9.1 Excel Style Filtering
        // Added an additional overload for discoverability that shows that the user can use a 
        // SpecialFilterOperand as the compare value
        //
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="comparisonOperator">Comparision operator.</param>
        /// <param name="specialOperand">An instance of a special operand that provides advanced filtering capabilities.</param>
        /// <seealso cref="SpecialFilterOperand"/>
        /// <seealso cref="SpecialFilterOperands"/>
        public FilterCondition(FilterComparisionOperator comparisonOperator, SpecialFilterOperand specialOperand)
            : this(comparisonOperator, (object)specialOperand)
        { }

		// SSP 6/28/05
		// Added a convenient constructor.
		// 
		/// <summary>
		/// Constructor.
		/// </summary>
		public FilterCondition( )
		{
		}
	
		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal FilterCondition( SerializationInfo info, StreamingContext context )
		protected FilterCondition( SerializationInfo info, StreamingContext context )
		{
			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			//
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "CompareValue":
						// JJD 8/19/02
						// Use the DeserializeObjectProperty static method to de-serialize properties instead.
						this.compareValue = Utils.DeserializeObjectProperty( entry );
						//this.compareValue = (object)entry.Value;

                        // MBS 12/3/08 - NA9.1 Excel Style Filtering
                        // If the user has serialized one of the special operators, then we would have
                        // written out the name of the operand, so see if we have that operand at this point
                        if (this.compareValue != null && this.compareValue is string)
                        {
                            SpecialFilterOperand operand = SpecialFilterOperands.GetRegisteredOperand((string)this.compareValue);
                            if (operand != null)
                                this.compareValue = operand;
                        }

						break;
					case "ComparisionOperator":
						//this.comparisionOperator = (FilterComparisionOperator)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.comparisionOperator = (FilterComparisionOperator)Utils.DeserializeProperty( entry, typeof(FilterComparisionOperator), this.comparisionOperator );
						//this.comparisionOperator = (FilterComparisionOperator)Utils.ConvertEnum(entry.Value, this.comparisionOperator);
						break;
					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;
					default:
						Debug.Assert( false, "Invalid entry in FilterCondition de-serialization ctor" );
						break;
				}
			}
		}

		#endregion // Constructor

		#region Public Virtual Methods

		#region MeetsCriteria

		// SSP 8/21/03 UWG2603
		// Added a way for the users to implement their own custom filtering logic.
		// Renamed DoesRowPassCondition to MeetsCriteria and made it public virtual.
		//
		/// <summary>
		/// Returns true if the row passes the filter condition.
		/// </summary>
		/// <param name="row">The row to test with the filter condition.</param>
		/// <returns>true if the row meets the criteria of the FilterCondition, false if it does not.</returns>
		//internal bool DoesRowPassCondition( UltraGridRow row )
		public virtual bool MeetsCriteria( UltraGridRow row )
		{
			// SSP 8/21/03 UWG2603
			// Added a way for the users to implement their own custom filtering logic.
			// 
			if ( FilterComparisionOperator.Custom == this.ComparisionOperator )
				return true;

			UltraGridColumn column = this.Column;

			Debug.Assert( null != column, "Null column !" );

			if ( null == column )
				return true;

			Debug.Assert( column.Band == row.Band, "Row has to belong to the same band as the one ColumnFilter is associated with." );

			if ( column.Band != row.Band )
				return true;

			// Get two values to compare
			//
			object val = UltraGridBand.GetValueForFilterComparision( row, column );
			object compareVal = this.CompareValue;

            // MBS 12/3/08 - NA9.1 Excel Style Filtering
            // Store this variable so that we can avoid multiple casts later
            SpecialFilterOperand specialOperand = compareVal as SpecialFilterOperand;

			// If the compare value is a column, then get the value
			// associated with that column
			//
			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( null != compareVal && compareVal is UltraGridColumn )
			//{
			//    compareVal = UltraGridBand.GetValueForFilterComparision( row, (UltraGridColumn)compareVal );
			//}
			UltraGridColumn gridColumn = compareVal as UltraGridColumn;

			if ( gridColumn != null )
			{
				compareVal = UltraGridBand.GetValueForFilterComparision( row, gridColumn );
			}

			// SSP 8/13/02 UWG1556
			// When converting the compareValue to the column's data type, if conversion is
			// not successful it will throw exceptions and exceptions slow things down. So
			// cache whether we were able to convert the value or not.
			//
			// Except for match and like, if val1 and val2 are not of the
			// same type, then convert them to the same type (column's data type)
			// comparision because it doesn't make sense for example to
			// compare 10 int value to "2" string and get the wrong result.
			//
			// SSP 4/20/05 - NAS 5.2 Filter Row
			// Added NotLike, DoesNotMatch, StartsWith, DoesNotStartWith, EndsWith, DoesNotEndWith,
			// Contains and DoesNotContain members to FilterComparisionOperator enum. Also don't 
			// convert special BlankCellValue and NonBlankCellValue objects.
			//
			// --------------------------------------------------------------------------------------
			//if ( FilterComparisionOperator.Match != this.ComparisionOperator &&
			//	FilterComparisionOperator.Like != this.ComparisionOperator )
			bool quantitativeComparison = false;
			switch( this.ComparisionOperator )
			{
				case FilterComparisionOperator.Custom:
				case FilterComparisionOperator.Equals:
				case FilterComparisionOperator.NotEquals:
				case FilterComparisionOperator.GreaterThan:
				case FilterComparisionOperator.GreaterThanOrEqualTo:
				case FilterComparisionOperator.LessThan:
				case FilterComparisionOperator.LessThanOrEqualTo:
					quantitativeComparison = true;
					break;
			}

			if ( quantitativeComparison 
				// SSP 6/12/06 BR13422
				// Use type checking because runtime deserialization 
				// produces different instances.
				// 
				// ----------------------------------------------------
				//&& FilterCondition.BlankCellValue != compareVal 
				//&& FilterCondition.NonBlankCellValue != compareVal 
				&& ! ( compareVal is BlanksClass )
				&& ! ( compareVal is NonBlanksClass )
				// ----------------------------------------------------
				// SSP 12/9/05 BR07680 BR07852
				// If a RowFilterComparer has been specified on the column then always pass in 
				// raw values without converting them.
				// 
				&& null == column.RowFilterComparer )
			// --------------------------------------------------------------------------------------
			{
				// See if we actually need to convert the values.
				//
				bool convertValues = true;

				// If we are comparing by text, then convert both of them to strings.
				//
				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//if ( null != this.Column.Editor && !this.Column.Editor.ComparesByValue( this.Column.EditorOwnerInfo, null ) )
				EmbeddableEditorBase editor = column.GetEditor( row );
				if ( null != editor && !editor.ComparesByValue( column.EditorOwnerInfo, null ) )
				{
					if ( null != compareVal && !( compareVal is DBNull ) )
						compareVal = compareVal.ToString( );

					if ( null != val && !( val is DBNull ) )
						val = val.ToString( );

					// Set this flag to false so we don't try to convert below.
					//
					convertValues = false;
				}

				// If any one of the values is null, then don't attempt to convert.
				// Nulls and DBNulls are always taken to be less than anything else.
				//
				if ( convertValues && 
					( null == compareVal || null == val 
					|| compareVal is DBNull || val is DBNull 
					|| compareVal.GetType( ) == val.GetType( ) ) )
					convertValues = false;
			
				if ( convertValues )
                {
                    // MBS 12/3/08 - NA9.1 Excel Style Filtering
                    // We could be using a SpecialFilterOperand for the compareVal here,
                    // so we should not try to perform any conversions on it.
                    //
                    //if ( compareVal.GetType( ) != column.DataType )
                    if ( specialOperand == null && compareVal.GetType() != column.DataType)
					{
						// Since the compareValue will stay the same, cache the converted value.
						// Also don't attempt to convert values again if the conversion had
						// failed last time otherwise it will raise exceptions and cause 
						// considerable slow downs.
						//
						if ( this.lastConversionDataType == column.DataType &&
							this.lastAttemptedValueForConversion == compareVal )
						{
							if ( this.lastConversionSuccessfull && null != this.lastConvertedValue )
								compareVal = this.lastConvertedValue;
						}
						else
						{
							object tmpVal = column.ConvertValueToDataType( compareVal );

							this.lastAttemptedValueForConversion = compareVal;
							this.lastConversionDataType = column.DataType;
							this.lastConversionSuccessfull = null != tmpVal;

							if ( null != tmpVal )
								compareVal = tmpVal;

							this.lastConvertedValue = compareVal;
						}
					}

					if ( val.GetType( ) != column.DataType )
					{
						object tmpVal = column.ConvertValueToDataType( val );

						if ( null != tmpVal )
							val = tmpVal;
					}

					// If somehow ConvertValueToDataType fails and the two objects are
					// not the same type, then convert both to strings.
					//
                    // MBS 12/3/08 - NA9.1 Excel Style Filtering
                    // We could be using a SpecialFilterOperand for the compareVal here,
                    // so we should not try to perform any conversions on it.
                    //
					//if ( compareVal.GetType( ) != val.GetType( ) )
                    if ( specialOperand == null && compareVal.GetType( ) != val.GetType( ) )
					{
						// SSP 7/23/07 BR24237
						// If the column's data type is Object and the data value is numeric
						// then try to compare both values as decimal.
						// 
						// ----------------------------------------------------------------------
						//compareVal = compareVal.ToString( );
						//val = val.ToString( );
						if ( typeof( object ) == column.DataType 
							&& Utilities.IsNumericType( val.GetType( ) ) )
						{
							try
							{
								object tmpCompareVal = ValueConstraint.ValueToDataValue( compareVal, typeof( decimal ), column.FormatInfo, column.Format );
								object tmpVal = null;
								if ( null != tmpCompareVal )
									tmpVal = ValueConstraint.ValueToDataValue( val, typeof( decimal ), column.FormatInfo, column.Format );

								if ( null != tmpCompareVal && null != tmpVal )
								{
									compareVal = tmpCompareVal;
									val = tmpVal;
									convertValues = false;
								}
							}
							catch
							{
							}
						}

						if ( convertValues )
						{
							compareVal = compareVal.ToString( );
							val = val.ToString( );
						}
						// ----------------------------------------------------------------------
					}
				}
			}

            // MBS 12/3/08 - NA9.1 Excel Style Filtering
            if (specialOperand != null)
            {
                if (null != val && specialOperand.SupportsDataType(val.GetType()) == false)
                    return false;

                return specialOperand.Match(this.ComparisionOperator, val);
            }
		
			return this.Evaluate( val, compareVal );
		}

		#endregion // MeetsCriteria

		#region Clone

		// SSP 8/21/03 UWG2603
		// Added a way for the users to implement their own custom filtering logic.
		// Added Clone method and implemented IClonable.
		//
		/// <summary>
		/// Clones the filter condition object.
		/// </summary>
		/// <returns></returns>
		public virtual object Clone( )
		{
			FilterCondition fc = (FilterCondition)this.MemberwiseClone( );

			// Null out these cached values.
			//
			fc.cachedRegex = null;
			fc.cachedRegexPattern = null;

			return fc;
		}

		#endregion // Clone

		#endregion // Public Virtual Methods

		#region Public Properties

		#region ComparisionOperator

		/// <summary>
		/// Comparision operator that will be used for comparing compareValue to
		/// the column's value (for the row that's being filtered).
		/// </summary>
		public FilterComparisionOperator ComparisionOperator
		{
			get
			{
				return this.comparisionOperator;
			}
			set
			{
				if ( value != this.comparisionOperator )
				{
					if ( !Enum.IsDefined( typeof( FilterComparisionOperator ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_104"), (int)value, typeof( FilterComparisionOperator ) );

					this.comparisionOperator = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ComparisionOperator );
				}
			}
		}

		#endregion // ComparisionOperator

		#region CompareValue

		/// <summary>
		/// Gets or sets the compare value. Compare value is the value on the right side of the comparision operator. 
		/// Cell's value will be on the left side.
		/// </summary>
		public object CompareValue
		{
			get
			{
				return this.compareValue;
			}
			set
			{
				if ( value != this.compareValue )
				{
					this.compareValue = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.CompareValue );
				}
			}
		}

		#endregion // CompareValue
        
        // MBS 12/3/08 - NA9.1 Excel Style Filtering
        // Added a SpecialOperand property to assist discoverability
        //
        #region SpecialOperand

        /// <summary>
        /// Gets or sets the operand that is used as the compare value.  Note that this
        /// property delegates to the CompareValue property.
        /// </summary>
        /// <seealso cref="SpecialFilterOperands"/>
        public SpecialFilterOperand SpecialOperand
        {
            get { return this.CompareValue as SpecialFilterOperand; }
            set { this.CompareValue = value; }
        }
        #endregion //SpecialOperand

        #region Column

        // SSP 8/21/03 UWG2603
		// Added a way for the users to implement their own custom filtering logic.
		// Made Column property public.
		//
		/// <summary>
		/// Associated column.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				// SSP 8/1/03 UWG1654 - Filter Action
				// Added a way for the user to be able to construct filter conditions and evaluate
				// it themselves by calling the new UltraGridRow.MeetsCriteria method.
				// Added a column variable.
				//
				//return this.ParentCollection.Column;
				return null != this.column ? this.column : 
					( null != this.ParentCollection ? this.ParentCollection.Column : null );
			}
		}

		#endregion // Column

		#region BlankCellValue

		// SSP 4/18/05 - NAS 5.2 Filter Row
		// Added BlankCellValue and NonBlankCellValue static instances for easier filtering 
		// of blanks and non blanks. Typically you have to create multiple filter conditions 
		// for blanks since a cell is considered to be blank if it's null, "" or DBNull. 
		// BlankCellValue and NonBlankCellValue only makes sense as an operand for Equals 
		// and NotEquals operators.
		//
		/// <summary>
		/// An object that represents blank cell values. Blank cell values include
		/// <b>null</b> (<b>Nothing</b> in VB), DBNull and empty string.
		/// </summary>
		public static object BlankCellValue
		{
			get
			{
				return BlanksClass.Value;
			}
		}

		#endregion // BlankCellValue

		#region NonBlankCellValue 

		// SSP 4/18/05 - NAS 5.2 Filter Row
		// Added BlankCellValue and NonBlankCellValue static instances for easier filtering 
		// of blanks and non blanks. Typically you have to create multiple filter conditions 
		// for blanks since a cell is considered to be blank if it's null, "" or DBNull. 
		// BlankCellValue and NonBlankCellValue only makes sense as an operand for Equals 
		// and NotEquals operators.
		//
		/// <summary>
		/// An object that represents non-blank cell values. Non-blank cell values include
		/// everything except <b>null</b> (<b>Nothing</b> in VB), DBNull and empty string.
		/// </summary>
		public static object NonBlankCellValue 
		{
			get
			{
				return NonBlanksClass.Value;
			}
		}

		#endregion // NonBlankCellValue 

        // MBS 6/22/09 - TFS18639
        #region ErrorCellValue

        /// <summary>
        /// An object that represents cells with IDataErrorInfo errors.
        /// </summary>
        public static object ErrorCellValue
        {
            get
            {
                return ErrorsClass.Value;
            }
        }
        #endregion //ErrorCellValue
        //
        #region NonErrorCellValue

        /// <summary>
        /// An object that represents cells without IDataErrorInfo errors.
        /// </summary>
        public static object NonErrorCellValue
        {
            get
            {
                return NonErrorsClass.Value;
            }
        }
        #endregion //NonErrorCellValue

        #endregion // Public Properties

        #region GetFilterComparisonOperator

		internal static FilterComparisionOperator GetFilterComparisonOperator( object operatorValue )
		{
			FilterComparisionOperator comparisionOperator = operatorValue is FilterComparisionOperator 
				? (FilterComparisionOperator)operatorValue : FilterComparisionOperator.Custom;

			return comparisionOperator;
		}

		#endregion // GetFilterComparisonOperator

		#region Base Overrides

		#region ToString

		// SSP 5/16/05 - NAS 5.2 Filter Row
		// Rewrote the ToString implementation. The original code is commented out further below.
		//

		internal void ToString( StringBuilder sb, bool includeColumnName )
		{
			FilterCondition.ToString( this.Column, sb, includeColumnName, 
				this.ComparisionOperator, this.CompareValue );
		}
		
		internal static void ToString( UltraGridColumn column, StringBuilder sb, bool includeColumnName,
			object comparisonOperator, object compareValue )
		{
			const char SPACE = ' ';
			const char QUOTE = '\'';

			includeColumnName = includeColumnName && null != column;

			string compareValueAsString;

			if ( null == compareValue || DBNull.Value == compareValue || compareValue is DBNullClass )
				compareValueAsString = DBNullClass.Value.ToString( );
			else if ( compareValue is EmptyStringClass )
				compareValueAsString = string.Empty;
			else 
				compareValueAsString = compareValue.ToString( );

			string compareOperatorAsString = FilterCondition.GetOperatorName( column, FilterCondition.GetFilterComparisonOperator( comparisonOperator ) );

			if ( includeColumnName )
				sb.Append( ColumnValueListDataValue.ToString( column ) ).Append( SPACE );

			sb.Append( compareOperatorAsString ).Append( SPACE );
			sb.Append( QUOTE ).Append( compareValueAsString ).Append( QUOTE );
		}

		/// <summary>
		/// Overridden. Returns the string representation of this filter condition.
		/// </summary>
		/// <returns></returns>
		public override string ToString( )
		{
			StringBuilder sb = new StringBuilder( );
			this.ToString( sb, true );
			return sb.ToString( );
		}

		// SSP 5/16/05 - NAS 5.2 Filter Row
		//
		#region Original Implementation
		
		#endregion // Original Implementation

		#endregion // ToString

		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "ComparisionOperator", this.ComparisionOperator );
			//info.AddValue( "ComparisionOperator", (int)this.ComparisionOperator );
	
			// SSP 10/15/06 BR16724
			// 
			// ------------------------------------------------------------------------------
			object compareVal = this.CompareValue;

			// If the compare value is a column then serialize it using SerializedColumnID 
			// since the column object cannot be serialized.
			// 
			if ( compareVal is UltraGridColumn )
			{
				UltraGridColumn col = (UltraGridColumn)compareVal;
				if ( null != col.Band )
					compareVal = col.Band.GetSerializedColumnId( col );
			}
            // MBS 12/3/08 - NA9.1 Excel Style Filtering
            // We don't want to serialize any of the static operands, so we'll just write
            // out the name and then look through the registered operands later
            else if( compareVal is DateOperand )
            {
                compareVal = ((DateOperand)compareVal).Name;
            }

			Utils.SerializeObjectProperty( info, "CompareValue", compareVal );

			
			// ------------------------------------------------------------------------------
		}

		#endregion // ISerializable.GetObjectData

		#endregion // Base Overrides

		#region Private/Internal Properties

		#region ParentCollection

		private FilterConditionsCollection ParentCollection
		{
			get
			{
				return this.parentCollection;
			}
		}

		#endregion // ParentCollection

		#endregion // Private/Internal Properties

		#region Private/Internal Methods

		#region InitializeFrom

		internal void InitializeFrom( FilterCondition source )
		{
			if ( source.ShouldSerializeTag( ) )
				this.tagValue = source.tagValue;

			this.comparisionOperator = source.comparisionOperator;
			this.compareValue = source.compareValue;

			// Null these cached values.
			//
			this.cachedRegex = null;
			this.cachedRegexPattern = null;
		}

		#endregion // InitializeFrom

		#region CompareValues

		private int CompareValues( object val1, object val2 )
		{
			// SSP 11/10/05 BR07680
			// Added RowFilterComparer property. If it's specified use that.
			// 
			if ( null != this.column && null != this.column.RowFilterComparer )
				return this.column.RowFilterComparer.Compare( val1, val2 );

			
			// (Should the SortComparer settings off the column
			// be used for comparision ?)
			//
			// SSP 1/19/05 BR01830
			// If not sorting (like when merging, or filtering), don't consider two objects 
			// as equal if their ToString results are equal because the ToString by default
			// returns the type name which would be the same even if the objects were 
			// different.
			//
			//return RowsCollection.DefaultCompare( val1, val2 );
			//
			// JAS v5.2 GroupBy Break Behavior 5/4/05 - Added 'column' argument.
			//
			//return RowsCollection.RowsSortComparer.DefaultCompare( val1, val2, false );
			// SSP 5/16/05 - FilterComparisonType property
			// Also changed the default behavior so we now perform case insensitive filter
			// comparisons. Note that this changes the default behavior in v5.2. v5.1 and
			// older versions performed case sensitive comparisons. It's imperative that
			// now we perform case insensitive comparisons by default because of the
			// filter row.
			//
			//return RowsCollection.RowsSortComparer.DefaultCompare( val1, val2, false, column );
			return RowsCollection.RowsSortComparer.DefaultCompare( val1, val2, false, this.IsCaseInsensitive );
		}

		#endregion // CompareValues

		#region IsCaseInsensitive

		// SSP 5/16/05 - NAS 5.2 FilterComparisonType property
		// Added convertToLower parameter.
		//
		private bool IsCaseInsensitive
		{
			get
			{
				UltraGridColumn column = this.Column;
				return  null == column || FilterComparisonType.CaseInsensitive == column.FilterComparisonTypeResolved;
			}
		}

		#endregion // IsCaseInsensitive

		#region ConvertToString

		// SSP 3/9/05 - NAS 5.2 Filter Row
		// Added ConvertToString helper method for converting a value into a string.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string ConvertToString( object val )
		private static string ConvertToString( object val )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return this.ConvertToString( val, false );
			return FilterCondition.ConvertToString( val, false );
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string ConvertToString( object val, bool convertToLower )
		private static string ConvertToString( object val, bool convertToLower )
		{
			string str = null != val ? val.ToString( ) : null;

			if ( convertToLower && null != str )
				str = str.ToLower( System.Globalization.CultureInfo.CurrentCulture );

			return str;
		}

		#endregion // ConvertToString

		#region Evaluate

		private bool Evaluate( object val1, object val2 )
		{
			return this.Evaluate( this.ComparisionOperator, val1, val2 );
		}
	
		// SSP 3/9/05 - NAS 5.2 Filter Row
		// Added NotLike, DoesNotMatch, StartsWith, DoesNotStartWith, EndsWith, DoesNotEndWith,
		// Contains and DoesNotContain members to FilterComparisionOperator enum.
		// Added static Evaluate overload that takes in comparisonOperator parameter. Code in
		// there is moved from the existing non-static Evaluate method.
		//
		private bool Evaluate( FilterComparisionOperator comparisonOperator, object val1, object val2 )
		{
			// SSP 8/13/02 UWG1556
			// Moved all the code into DoesRowPassCondition which calls this method.
			//
			

			bool ret = true;

			switch ( comparisonOperator )
			{
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.Equals:
				{
					// SSP 4/18/05 - NAS 5.2 Filter Row
					// Added BlankCellValue and NonBlankCellValue static instances for easier filtering 
					// of blanks and non blanks. Typically you have to create multiple filter conditions 
					// for blanks since a cell is considered to be blank if it's null, "" or DBNull. 
					// BlankCellValue and NonBlankCellValue only makes sense as an operand for Equals 
					// and NotEquals operators.
					//
					// SSP 6/12/06 BR13422
					// Use Equals because runtime deserialization produces different instances.
					// 
					//if ( FilterCondition.BlankCellValue == val2 || FilterCondition.NonBlankCellValue == val2 )
					// MD 8/7/07 - 7.3 Performance
					// FxCop - Do not cast unnecessarily
					//if ( val2 is BlanksClass || val2 is NonBlanksClass )
					bool isBlanksClass = val2 is BlanksClass;

					if ( isBlanksClass || val2 is NonBlanksClass )
					{
						bool cellValueBlank = null == val1 || DBNull.Value == val1 || val1 is string && 0 == ((string)val1).Length;
						// SSP 6/12/06 BR13422
						// Use Equals because runtime deserialization produces different instances.
						// 
						//ret = FilterCondition.BlankCellValue == val2 ? cellValueBlank : ! cellValueBlank;
						// MD 8/7/07 - 7.3 Performance
						// FxCop - Do not cast unnecessarily
						//ret = val2 is BlanksClass ? cellValueBlank : ! cellValueBlank;
						ret = isBlanksClass ? cellValueBlank : !cellValueBlank;
					}
                    else
                    {
                        ret = (null == val1 && null == val2) || 0 == this.CompareValues(val1, val2);
                    }

					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.GreaterThan:
				{
					ret = this.CompareValues( val1, val2 ) > 0;
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.GreaterThanOrEqualTo:
				{
					ret = this.CompareValues( val1, val2 ) >= 0;
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.LessThan:
				{
					ret = this.CompareValues( val1, val2 ) < 0;
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.LessThanOrEqualTo:
				{
					ret = this.CompareValues( val1, val2 ) <= 0;
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.Like:
				{
					// SSP 3/10/05 - NAS 5.2 Filter Row
					// Use ConvertToString helper method instead of performing a ToString on the value.
					// 
					// --------------------------------------------------------------------------------
					
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string str1 = this.ConvertToString( val1 );
					//string str2 = this.ConvertToString( val2 );
					string str1 = FilterCondition.ConvertToString( val1 );
					string str2 = FilterCondition.ConvertToString( val2 );

					// JAS 3/18/05 BR02713 - If val1 == "" and val2 == "?*" then StrLike blows up.
					// Quite bizarre.  So if it blows up, then we can just assume that the match failed and return false.
					try
					{
						ret = null != str1 && null != str2 && Microsoft.VisualBasic.CompilerServices.StringType.StrLike( str1, str2, 
							// SSP 5/16/05 - NAS 5.2 FilterComparisonType property
							// Pass in Binary to perform case sensitive comparison if FilterComparisonType is set to CaseSensitive.
							//
							//Microsoft.VisualBasic.CompareMethod.Text 
							this.IsCaseInsensitive ? Microsoft.VisualBasic.CompareMethod.Text : Microsoft.VisualBasic.CompareMethod.Binary );
					}
					catch
					{
						ret = false;
					}
					// --------------------------------------------------------------------------------
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.Match:
				{
					ret = false;

					if ( null != val1 && null != val2 )
					{
						System.Text.RegularExpressions.Regex rx = val2 as System.Text.RegularExpressions.Regex;

						if ( null == rx )
						{
							// SSP 3/10/05
							// Use ConvertToString helper method instead of performing a ToString on the value.
							// 
							//string pattern = val2.ToString( );
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//string pattern = this.ConvertToString( val2 );
							string pattern = FilterCondition.ConvertToString( val2 );

							Debug.Assert( null != pattern, "Compare value is not a valid pattern." );

							if ( null != pattern )
							{
								// SSP 5/16/05 - NAS 5.2 FilterComparisonType property
								// Perform case insensitive regex matching based on the new FilterComparisonType 
								// property setting.
								//
								System.Text.RegularExpressions.RegexOptions regexOptions = System.Text.RegularExpressions.RegexOptions.ExplicitCapture;
								if ( this.IsCaseInsensitive )
									regexOptions |= System.Text.RegularExpressions.RegexOptions.IgnoreCase;

								// SSP 5/16/05 - NAS 5.2 FilterComparisonType property / Optimizations
								// 
								//if ( null != this.cachedRegex && null != this.cachedRegexPattern && this.cachedRegexPattern.Equals( pattern ) )
								if ( null != this.cachedRegex && this.cachedRegexPattern == pattern && this.cachedRegexOptions == regexOptions )
								{
									rx = this.cachedRegex;
								}
								else
								{
									// SSP 5/16/05 - NAS 5.2 FilterComparisonType property
									// Perform case insensitive regex matching based on the new FilterComparisonType 
									// property setting.
									//
									//rx = new System.Text.RegularExpressions.Regex( pattern );
									rx = new System.Text.RegularExpressions.Regex( pattern, regexOptions );

									this.cachedRegex = rx;
									this.cachedRegexPattern = pattern;

                                    // MBS 11/14/06
                                    // We weren't actually setting the cached regex options, so we'd create a new regex option
                                    // every time
                                    this.cachedRegexOptions = regexOptions;
								}

								// SSP 6/12/03
								// Moved this down from here.
								//
								//ret = rx.IsMatch( val1.ToString( ) );
							}
						}

						// SSP 6/12/03
						// Moved this down from above.
						//
						if ( null != rx )
						{
							// SSP 3/10/05
							// Use ConvertToString helper method instead of performing a ToString on the value.
							// 
							//ret = rx.IsMatch( val1.ToString( ) );
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//string str1 = this.ConvertToString( val1 );
							string str1 = FilterCondition.ConvertToString( val1 );

							ret = null != str1 && rx.IsMatch( str1 );
						}
					}					
									
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.NotEquals:
				{
					// SSP 4/18/05 - NAS 5.2 Filter Row
					// Added BlankCellValue static instance for easier filtering of blanks or
					// non blanks. Typically you have to create multiple filter conditions for blanks
					// since a cell is considered to be blank if it's null, "" or DBNull.
					// Just do ! Equals.
					//
					//ret = ( ( null == val1 ) != ( null == val2 ) ) || 0 != this.CompareValues( val1, val2 );
					ret = ! this.Evaluate( FilterComparisionOperator.Equals, val1, val2 );
					break;
				}
				// SSP 3/9/05 - NAS 5.2 Filter Row
				// Added NotLike, DoesNotMatch, StartsWith, DoesNotStartWith, EndsWith, DoesNotEndWith,
				// Contains and DoesNotContain members to FilterComparisionOperator enum.
				//
				// --------------------------------------------------------------------------------------
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.NotLike:
				{
					ret = ! this.Evaluate( FilterComparisionOperator.Like, val1, val2 );
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.DoesNotMatch:
				{
					ret = ! this.Evaluate( FilterComparisionOperator.Match, val1, val2 );
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.StartsWith:
				{
					// Perform case insensitive comparison base on the new FilterComparisonType 
					// property setting.
					//
					ret = false;

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string str1 = this.ConvertToString( val1 );
					//string str2 = this.ConvertToString( val2 );
					string str1 = FilterCondition.ConvertToString( val1 );
					string str2 = FilterCondition.ConvertToString( val2 );

					if ( null != str1 && null != str2 && str1.Length >= str2.Length )
					{
						ret = 0 == string.Compare( 
							str1, 0, str2, 0, str2.Length, this.IsCaseInsensitive, 
							System.Globalization.CultureInfo.CurrentCulture );
					}
					
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.DoesNotStartWith:
				{
					ret = ! this.Evaluate( FilterComparisionOperator.StartsWith, val1, val2 );
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.EndsWith:
				{
					bool caseInsensitive = this.IsCaseInsensitive;

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string str1 = this.ConvertToString( val1, caseInsensitive );
					//string str2 = this.ConvertToString( val2, caseInsensitive );
					string str1 = FilterCondition.ConvertToString( val1, caseInsensitive );
					string str2 = FilterCondition.ConvertToString( val2, caseInsensitive );

					ret = null != str1 && null != str2 && str1.EndsWith( str2 );
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.DoesNotEndWith:
				{
					ret = ! this.Evaluate( FilterComparisionOperator.EndsWith, val1, val2 );
					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.Contains:
				{
					bool caseInsensitive = this.IsCaseInsensitive;

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string str1 = this.ConvertToString( val1, caseInsensitive );
					//string str2 = this.ConvertToString( val2, caseInsensitive );
					string str1 = FilterCondition.ConvertToString( val1, caseInsensitive );
					string str2 = FilterCondition.ConvertToString( val2, caseInsensitive );

                    // MBS 2/6/09 - TFS13607
                    // Perform the comparisons using the Ordinal type, since the IndexOf relies on the current
                    // culture, and we don't want a search to fail on one culture while working on another.
					//ret = null != str1 && null != str2 && str1.IndexOf( str2 ) >= 0;
                    ret = null != str1 && null != str2 && str1.IndexOf(str2, StringComparison.Ordinal) >= 0;

					break;
				}
				case Infragistics.Win.UltraWinGrid.FilterComparisionOperator.DoesNotContain:
				{
					ret = ! this.Evaluate( FilterComparisionOperator.Contains, val1, val2 );
					break;
				}
				// --------------------------------------------------------------------------------------
				default:
				{
					Debug.Assert( false, "Unknown ComparisionOperator" );
					break;
				}
			}

			return ret;
		}

		#endregion // Evaluate

		#region Initialize

		// SSP 8/21/03 UWG2603
		// Added a way for the users to implement their own custom filtering logic.
		// Added Initialize method.
		//
		// SSP 5/5/05
		//
		//internal void Initialize( FilterConditionsCollection parentCollection, UltraGridColumn column )
		internal void Initialize( FilterConditionsCollection parentCollection )
		{
			this.parentCollection = parentCollection;

			// SSP 5/5/05
			//
			//this.column = column;
			Debug.Assert( null != this.parentCollection );
			this.column = null != this.parentCollection ? this.parentCollection.Column : null;

			// SSP 10/15/06 BR16724
			// Deserialize SerializedColumnID into a column object.
			// 
			if ( this.compareValue is SerializedColumnID )
			{
				if ( null != this.column && null != this.column.Band )
					this.compareValue = this.column.Band.GetMatchingColumn( (SerializedColumnID)this.compareValue );
			}
		}

		#endregion // Initialize

        #region ValidateSearchPattern

        // JDN 12/20/04 BR00867
        // Ensure the search pattern or regular expression is valid
		// SSP 4/23/05 - NAS 5.2 Filter Row
		//
        //private void ValidateSearchPattern( FilterComparisionOperator comparisonOperator, object compareValue )
		internal static void ValidateSearchPattern( FilterComparisionOperator comparisonOperator, object compareValue )
        {
            switch ( comparisonOperator )
            {
                case FilterComparisionOperator.Like:
				// SSP 8/31/05 BR06017
				//
				case FilterComparisionOperator.NotLike:
                {
                    try
                    {
                        Microsoft.VisualBasic.CompilerServices.StringType.StrLike( "testString", compareValue.ToString( ), Microsoft.VisualBasic.CompareMethod.Text );
                    }
                    catch
                    {
                        throw new ArgumentException( Shared.SR.GetString("RowFilterPatternException", compareValue.ToString( )) );
                    }
                }
                    break;
                case FilterComparisionOperator.Match:
				// SSP 8/31/05 BR06017
				//
				case FilterComparisionOperator.DoesNotMatch:
                {
                    try
                    {
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Remove unused locals
                        //System.Text.RegularExpressions.Regex rx = 
                            new System.Text.RegularExpressions.Regex( compareValue.ToString( ) );
                    }
                    catch
                    {
                        throw new ArgumentException( Shared.SR.GetString("RowFilterRegexException", compareValue.ToString( )) );
                    }
                }
                    break;
            }
        }

        #endregion // ValidateSearchPattern

		#region InternalEquals

		// SSP 4/18/05 - NAS 5.2 Filter Row
		//
		internal bool InternalEquals( FilterCondition fc )
		{
            // MRS 6/5/2008 - BR33648
            if (fc.GetType() != this.GetType())
                return false;

            // MBS 7/22/08 - BR35038
            // We shouldn't consider the values to be equal if they are different types
            if (fc.CompareValue != null && this.CompareValue != null && 
                fc.CompareValue.GetType() != this.CompareValue.GetType())
                return false;

			if ( this.ComparisionOperator != fc.ComparisionOperator )
				return false;

            if ( 0 != this.CompareValues( this.CompareValue, fc.CompareValue ) )
				return false;

			return true;
		}

		#endregion // InternalEquals

		#region GetOperatorName

		// SSP 5/16/05 - NAS 5.2 Filter Row
		// Added GetOperatorName helper method.
		//

		private static string GetOperatorName( UltraGridColumn column, FilterComparisionOperator filterOperator )
		{
			string operatorText;

			switch ( filterOperator )
			{
				case FilterComparisionOperator.Equals:
					operatorText = SR.GetString( "RowFilterDropDown_Operator_Equals" );
					break;
				case FilterComparisionOperator.GreaterThan:
					operatorText = SR.GetString( "RowFilterDropDown_Operator_GreaterThan" );
					break;
				case FilterComparisionOperator.GreaterThanOrEqualTo:
					operatorText = SR.GetString( "RowFilterDropDown_Operator_GreaterThanOrEqualTo" );
					break;
				case FilterComparisionOperator.LessThan:
					operatorText = SR.GetString( "RowFilterDropDown_Operator_LessThan" );
					break;								
				case FilterComparisionOperator.LessThanOrEqualTo:
					operatorText = SR.GetString( "RowFilterDropDown_Operator_LessThanOrEqualTo" );
					break;
				case FilterComparisionOperator.Like:
					operatorText = SR.GetString( "RowFilterDropDown_Operator_Like" );
					break;
				case FilterComparisionOperator.Match:
					operatorText = SR.GetString( "RowFilterDropDown_Operator_Match" );
					break;
				case FilterComparisionOperator.NotEquals:
					operatorText = SR.GetString( "RowFilterDropDown_Operator_NotEquals" );
					break;
					// SSP 5/9/05 - NAS 5.2 Filter Row
					// Added default for the rest of the operators.
					//
				default:
					operatorText = null != column && null != column.Layout
						? column.Layout.GetFilterComparisonOperatorName( filterOperator )
						: filterOperator.ToString( );
					break;
			}

			return operatorText;
		}

		#endregion // GetOperatorName

		#endregion // Private/Internal Methods
	}

    // MBS 6/22/09 - TFS18639
    #region DataErrorFilterCondition

    /// <summary>
    /// A class used for filtering rows based on whether the cells, in the column that this
    /// filter condition is associated with, have an IDataErrorInfo error on them.
    /// </summary>
    [Serializable()]
    public class DataErrorFilterCondition : FilterCondition
    {
        /// <summary>
        /// Initializes a new instance of this class.
        /// </summary>
        /// <param name="shouldMatchError">True if the class should consider an error a valid match.</param>
        public DataErrorFilterCondition(bool shouldMatchError)
        {
            this.ComparisionOperator = FilterComparisionOperator.Equals;

            if (shouldMatchError)
                this.CompareValue = ErrorsClass.Value;
            else
                this.CompareValue = NonErrorsClass.Value;
        }

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected DataErrorFilterCondition(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Comparision operator that will be used for comparing compareValue to
        /// the column's value (for the row that's being filtered).  Note that this class
        /// only supports 'Equals' and 'NotEquals'
        /// </summary>
        public new FilterComparisionOperator ComparisionOperator
        {
            get { return base.ComparisionOperator; }
            set
            {
                if (value != FilterComparisionOperator.Equals && value != FilterComparisionOperator.NotEquals)
                    throw new ArgumentException(SR.GetString("LE_DataErrorFilterCondition_InvalidComparisionOperator"));

                base.ComparisionOperator = value;
            }
        }

        /// <summary>
        /// Returns true if the row passes the filter condition.
        /// </summary>
        /// <param name="row">The row to test with the filter condition.</param>
        /// <returns>true if the row meets the criteria of the FilterCondition, false if it does not.</returns>
        public override bool MeetsCriteria(UltraGridRow row)
        {
            IDataErrorInfo errorInfo = row.GetDataErrorInfo();

            // If the row does not have any error info associated with it, or does not implement
            // the interface, then we only consider the row a match if the CompareValue
            // is NotEquals, since this basically means "not equal to an error".  Also do the same
            // thing if we don't have a reference to the column, since we can't, by default, handle
            // filtering when the error is on the row itself
            if (errorInfo == null || this.Column == null)
                return this.CompareValue is NonErrorsClass;

             string error = row.GetDataError(this.Column);
             switch (this.ComparisionOperator)
             {
                 case FilterComparisionOperator.Equals:
                     return (this.CompareValue is ErrorsClass && !String.IsNullOrEmpty(error)) ||
                            (this.CompareValue is NonErrorsClass && String.IsNullOrEmpty(error));

                 case FilterComparisionOperator.NotEquals:
                     return !(this.CompareValue is ErrorsClass && !String.IsNullOrEmpty(error)) &&
                            !(this.CompareValue is NonErrorsClass && String.IsNullOrEmpty(error));
             }

             Debug.Fail("Unsupported FilterComparisonOperator: " + this.ComparisionOperator);
             return false;
        }
    }
    #endregion //DataErrorFilterCondition
}


