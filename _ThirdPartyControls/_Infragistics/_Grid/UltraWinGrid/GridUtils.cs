#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Permissions;
using Infragistics.Win.Layout;
using System.Collections.Generic;

namespace Infragistics.Win.UltraWinGrid
{

	#region GridUtils Class

	internal class GridUtils
	{
		// AS 4/7/06 ImageBackgroundDisabled
		// There are a few places where we are stripping out ImageBackground.
		// To make it easier to manage adding more in the future, I've added some
		// constants for these here.
		//
		#region Constants
		internal const AppearancePropFlags ImageProperties = AppearancePropFlags.ImageBackground |
			AppearancePropFlags.Image | AppearancePropFlags.ImageBackgroundDisabled;

		internal const AppearancePropFlags ImageBackgroundProps = AppearancePropFlags.ImageBackground |
			AppearancePropFlags.ImageBackgroundDisabled;

		internal const AppearancePropFlags CommonBypassProps = AppearancePropFlags.BackColor |
			AppearancePropFlags.ForeColor | AppearancePropFlags.ImageBackground | AppearancePropFlags.ImageBackgroundDisabled;

		#endregion //Constants

		#region Constructor

		private GridUtils( )
		{
		}

		#endregion // Constructor

		#region ValidateEnum

		private static bool IsEnumFlags( Type enumType )
		{
			object[] flagsAttributes = enumType.GetCustomAttributes( typeof( FlagsAttribute ), true );
			return null != flagsAttributes && flagsAttributes.Length > 0;
		}

		internal static void ValidateEnum( Type enumType, object enumVal )
		{
			if ( ! Enum.IsDefined( enumType, enumVal ) && ! GridUtils.IsEnumFlags( enumType ) )
				throw new InvalidEnumArgumentException( enumType.Name, (int)enumVal, enumType );
		}
		
		internal static void ValidateEnum( string argumentName, Type enumType, object enumVal )
		{
			if ( ! Enum.IsDefined( enumType, enumVal ) && ! GridUtils.IsEnumFlags( enumType ) )
				throw new InvalidEnumArgumentException( argumentName, (int)enumVal, enumType );
		}

		#endregion // ValidateEnum

		#region ValidateNull

		internal static void ValidateNull( string paramName, object val )
		{
			if ( null == val )
				throw new ArgumentNullException( paramName );
		}

		internal static void ValidateNull( object val )
		{
			if ( null == val )
				throw new ArgumentNullException( );
		}

		#endregion // ValidateNull

		// MRS 3/23/05 - Support for dragging columns in RowLayout Mode
		#region IsPointInGroupByBox
		internal static bool IsPointInGroupByBox( UltraGridBase grid, Point pointInControlCoords )
		{
			if (grid.DisplayLayout.GroupByBox.HiddenResolved)
				return false;
			
			UIElement element = grid.DisplayLayout.UIElement.ElementFromPoint(pointInControlCoords);
			if (null == element)
				return false;

			element = element.GetAncestor(typeof (GroupByBoxUIElement));
			return (element != null);
		}
		#endregion IsPointInGroupByBox

		// MRS 4/1/05 - Support for dragging columns in RowLayout Mode
		#region IsControlKeyDown
		internal static bool IsControlKeyDown
		{
			get 
			{
				return ((Control.ModifierKeys & System.Windows.Forms.Keys.Control) != 0);
			}
		}
		#endregion IsControlKeyDown

		// MRS 4/7/05 - Replaced Case with CharacterCasing
		#region CaseToCharacterCasing
		internal static CharacterCasing CaseToCharacterCasing(Case textCase)
		{
			switch (textCase)
			{
				case Case.Upper:
					return CharacterCasing.Upper;
				case Case.Lower:
					return CharacterCasing.Lower;
				default:
					return CharacterCasing.Normal;
			}
		}
		#endregion CaseToCharacterCasing

		#region CharacterCasingToCase
		internal static  Case CharacterCasingToCase(CharacterCasing characterCasing)
		{
			switch (characterCasing)
			{
				case CharacterCasing.Upper:
					return Case.Upper;
				case CharacterCasing.Lower:
					return Case.Lower;
				default:
					return Case.Unchanged;
			}
		}
		#endregion CharacterCasingToCase

		#region GetNearestDataRow

		// SSP 2/25/07 BR18124
		// 
		internal static UltraGridRow GetNearestDataRow( UltraGridRow row, bool prev )
		{
			if ( null == row )
				return null;

			UltraGridLayout layout = row.Layout;
			int scrollPos = row.ScrollPosition;

			if ( scrollPos < 0 || null == layout )
				return null;

			while ( true )
			{
				scrollPos += prev ? -1 : 1;
				UltraGridRow rr = layout.GetRowAtScrollPos( scrollPos );
				if ( null == rr || rr.IsDataRow )
					return rr;
			}
		}

		#endregion // GetNearestDataRow

		#region Swap

		internal static void Swap( object[] arr, int i, int j )
		{
			object tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}

		internal static void Swap( long[] arr, int i, int j )
		{
			long tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}

		#endregion // Swap
		
		#region IsObjectOfType

		// SSP 4/16/05 - NAS 5.2 Filter Row
		//
		internal static bool IsObjectOfType( object obj, Type type )
		{
			return null != obj && GridUtils.IsObjectOfType( obj.GetType( ), type );
		}

		// SSP 4/16/05 - NAS 5.2 Filter Row
		//
		internal static bool IsObjectOfType( Type objectType, Type typeToCheck )
		{
			return objectType == typeToCheck || typeToCheck.IsAssignableFrom( objectType );
		}

		#endregion // IsObjectOfType

		#region ExtractExistingElement

		internal static object ExtractExistingElement( ArrayList list, Type type, bool removeExtractedElement )
		{
			return DummyUIElement.ExtractExistingElementHelper( list, type, removeExtractedElement );
		}

		#region DummyUIElement 

		private class DummyUIElement : UIElement
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid uninstantiated internal classes
			private DummyUIElement() { }

			internal static object ExtractExistingElementHelper( ArrayList list, Type type, bool removeExtractedElement )
			{
				return UIElement.ExtractExistingElement( list, type, removeExtractedElement );
			}
		}

		#endregion // DummyUIElement 

		#endregion // ExtractExistingElement

		#region ResolveBackColorsToTransparent

		// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ResolveBackColorsToTransparent helper method.
		//
		internal static void ResolveBackColorsToTransparent( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			Color ee = Color.Empty;
			Color tt = Color.Transparent;

			GridUtils.ResolveColors( ref appData, ref flags,
				AppearancePropFlags.BackColor | AppearancePropFlags.BackColor2 
				| AppearancePropFlags.BackColorDisabled | AppearancePropFlags.BackColorDisabled2,
				tt, tt, tt, tt, ee, ee, ee, ee );
		}

		#endregion // ResolveBackColorsToTransparent

		#region ResolveColors

		// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ResolveColors helper method.
		//
		internal static void ResolveColors( 
			ref AppearanceData appData, 
			ref AppearancePropFlags requestedPropFlags, 
			AppearancePropFlags onlyResolveTheseProps,
			Color backColor, Color backColor2, Color backColorDisabled, Color backColorDisabled2,
			Color foreColor, Color foreColorDisabled,
			Color borderColor, Color borderColor3DBase )
		{
			AppearancePropFlags flags = requestedPropFlags & onlyResolveTheseProps;

			if ( 0 != ( AppearancePropFlags.BackColor & flags ) )
			{
				flags ^= AppearancePropFlags.BackColor;
				appData.BackColor = backColor;
			}

			if ( 0 != ( AppearancePropFlags.BackColor2 & flags ) )
			{
				flags ^= AppearancePropFlags.BackColor2;
				appData.BackColor2 = backColor2;
			}

			if ( 0 != ( AppearancePropFlags.ForeColor & flags ) )
			{
				flags ^= AppearancePropFlags.ForeColor;
				appData.ForeColor = foreColor;
			}

			if ( 0 != ( AppearancePropFlags.BorderColor & flags ) )
			{
				flags ^= AppearancePropFlags.BorderColor;
				appData.BorderColor = borderColor;
			}

			if ( 0 != ( AppearancePropFlags.BorderColor3DBase & flags ) )
			{
				flags ^= AppearancePropFlags.BorderColor3DBase;
				appData.BorderColor3DBase = borderColor3DBase;
			}

			if ( 0 != ( AppearancePropFlags.BackColorDisabled & flags ) )
			{
				flags ^= AppearancePropFlags.BackColorDisabled;
				appData.BackColorDisabled = backColorDisabled;
			}

			if ( 0 != ( AppearancePropFlags.BackColorDisabled2 & flags ) )
			{
				flags ^= AppearancePropFlags.BackColorDisabled2;
				appData.BackColorDisabled2 = backColorDisabled2;
			}

			if ( 0 != ( AppearancePropFlags.ForeColorDisabled & flags ) )
			{
				flags ^= AppearancePropFlags.ForeColorDisabled;
				appData.ForeColorDisabled = foreColorDisabled;
			}

			requestedPropFlags &= ~flags;
		}

		#endregion // ResolveColors

		#region UIElement ToolTipItem Related

		#region UIElementToolTipItemBase Class

		internal abstract class UIElementToolTipItemBase : Infragistics.Win.IToolTipItem
		{
			internal bool showToolTipOverElement = false;

			internal UIElementToolTipItemBase( )
			{
			}

			protected abstract string GetToolTipText( 
				Point mousePosition, 
				UIElement element, 
				UIElement previousToolTipElement, 
				ToolTipInfo toolTipInfoDefault );

			
			ToolTipInfo Infragistics.Win.IToolTipItem.GetToolTipInfo( Point mousePosition, UIElement element, UIElement previousToolTipElement, ToolTipInfo toolTipInfoDefault )
			{
				string tipText = this.GetToolTipText( mousePosition, element, previousToolTipElement, toolTipInfoDefault );
				
				if ( null != tipText && tipText.Length > 0 
					&& this.showToolTipOverElement && null != element && null != element.Control )
				{
					Rectangle rect = element.Control.RectangleToScreen( element.Rect );
					toolTipInfoDefault.Location = rect.Location;
					toolTipInfoDefault.Size = rect.Size;

					toolTipInfoDefault.Margins = new Infragistics.Win.Margins( 0, 0, 0, 0, false );
				}

				toolTipInfoDefault.ToolTipText = tipText;

				return toolTipInfoDefault;
			}
		}

		#endregion // UIElementToolTipItemBase Class

		#region UIElementToolTipItem Class

		// SSP 5/6/05 - NAS 5.2 Filter Row/Fixed Add Row
		//
		internal class UIElementToolTipItem : UIElementToolTipItemBase
		{
			private string toolTipText = null;

			internal UIElementToolTipItem( string toolTipText ) : base( )
			{
				this.toolTipText = toolTipText;
			}

			internal void Initialize( string toolTipText )
			{
				this.toolTipText = toolTipText;
			}
	
			protected override string GetToolTipText( Point mousePosition, UIElement element, UIElement previousToolTipElement, ToolTipInfo toolTipInfoDefault )
			{
				return this.toolTipText;
			}
		}

		#endregion // UIElementToolTipItem Class

		#region TextUIElementToolTipItem Class

		private class TextUIElementToolTipItem : UIElementToolTipItemBase
		{
			private static TextUIElementToolTipItem VALUE = null;

			private TextUIElementToolTipItem( ) : base( )
			{
				this.showToolTipOverElement = true;
			}

			internal static TextUIElementToolTipItem Value
			{
				get
				{
					if ( null == VALUE )
						VALUE = new TextUIElementToolTipItem( );

					return VALUE;
				}
			}

			protected override string GetToolTipText( Point mousePosition, UIElement element, UIElement previousToolTipElement, ToolTipInfo toolTipInfoDefault )
			{
				// If the tool tip is meant to be displayed if the text is clipped then 
				// don't display the tool tip if the text is fully visible.
				// 
				TextUIElementBase textElem = null != element ? (TextUIElementBase)GridUtils.GetChild( element, typeof( TextUIElementBase ) ) : null;
				if ( null != textElem && ! textElem.IsTextFullyVisible )
					return textElem.Text;

				return null;
			}
		}

		#endregion // TextUIElementToolTipItem Class

		#region CreateToolTipItem

		internal static UIElementToolTipItem CreateToolTipItem( string toolTipText, IToolTipItem existingToolTipItem )
		{
			UIElementToolTipItem tt = null;

			if ( null != toolTipText && toolTipText.Length > 0 )
			{
				tt = existingToolTipItem as UIElementToolTipItem;
				if ( null == tt )
					tt = new UIElementToolTipItem( toolTipText );
				else
					tt.Initialize( toolTipText );
			}

			return tt;
		}

		#endregion // CreateToolTipItem

		#region SetupElementToolTip

		// SSP 7/28/05 - Header, Row, Summary Tooltips
		// 
		internal static bool SetupElementToolTip( UIElement element, string toolTipText, bool useTextElemText )
		{
			UIElementToolTipItemBase tti = null;

			if ( null == toolTipText || 0 == toolTipText.Length )
			{
				if ( useTextElemText )
				{
					TextUIElementBase textElem = (TextUIElementBase)GridUtils.GetChild( element, typeof( TextUIElementBase ) );
					if ( null != textElem )
					{
						textElem.TrackCharactersRendered = true;
						textElem.TrackTextArea = true;
						tti = TextUIElementToolTipItem.Value;
					}
				}
			}
			else
			{
				tti = GridUtils.CreateToolTipItem( toolTipText, element.ToolTipItem );
			}

			element.ToolTipItem = tti;
			return null != tti;
		}

		#endregion // SetupElementToolTip

		#endregion // UIElement ToolTipItem Related

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#region ReplaceColor

		//// SSP 5/8/05 - NAS 5.2 Filter Row
		////
		//internal static void ReplaceColor( Bitmap bmp, Color source, Color target )
		//{
		//    for ( int y = 0; y < bmp.Height; y++ )
		//    {
		//        for ( int x = 0; x < bmp.Width; x++ )
		//        {
		//            if ( bmp.GetPixel( x, y ) == source )
		//                bmp.SetPixel( x, y, target );
		//        }
		//    }
		//}

		//#endregion // ReplaceColor

		#endregion Not Used

		#region ResolveAlignments

		// SSP 5/11/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ResolveAlignments helper method.
		//
		internal static void ResolveAlignments( 
			ref AppearanceData appData,
			ref AppearancePropFlags flags,
			HAlign textHAlign,
			VAlign textVAlign,
			HAlign imageHAlign,
			VAlign imageVAlign )
		{
			if ( 0 != ( AppearancePropFlags.TextHAlign & flags ) )
			{
				flags ^= AppearancePropFlags.TextHAlign;
				appData.TextHAlign = textHAlign;
			}

			if ( 0 != ( AppearancePropFlags.TextVAlign & flags ) )
			{
				flags ^= AppearancePropFlags.TextVAlign;
				appData.TextVAlign = textVAlign;
			}

			if ( 0 != ( AppearancePropFlags.ImageHAlign & flags ) )
			{
				flags ^= AppearancePropFlags.ImageHAlign;
				appData.ImageHAlign = imageHAlign;
			}

			if ( 0 != ( AppearancePropFlags.ImageVAlign & flags ) )
			{
				flags ^= AppearancePropFlags.ImageVAlign;
				appData.ImageVAlign = imageVAlign;
			}
		}

		#endregion // ResolveAlignments

		#region ToSingleLine

		// SSP 5/16/05
		// Added ToSingleLine helper method.
		//
		internal static string ToSingleLine( string text )
		{
			string newLine = Environment.NewLine;
			return text.Replace( newLine, " " );
		}

		#endregion // ToSingleLine

		#region IsEmpty

		internal static bool IsEmpty( string str )
		{
			return null == str || 0 == str.Length;
		}

		#endregion // IsEmpty

		#region ApplyDelimiters

		// SSP 12/01/05 - NAS 6.1 Multi-cell Operations Support
		// 
		internal static string ApplyDelimiters( string text, string delim )
		{
			if ( null == text )
				text = string.Empty;

			if ( null != delim && delim.Length > 0 )
				text = delim + text + delim;

			return text;
		}

		#endregion // ApplyDelimiters

		#region AddPadding

		internal static Bitmap AddPadding( Bitmap image, Infragistics.Win.Layout.Insets padding, Color paddingColor )
		{
			Bitmap bmp = new Bitmap( image, image.Width + padding.Left + padding.Right, image.Height + padding.Top + padding.Bottom );
			
			int w = bmp.Width;
			int h = bmp.Height;

			for ( int y = 0; y < h; y++ )
			{
				for ( int x = 0; x < w; x++ )
				{
					if ( x < padding.Left || y < padding.Right 
						|| x >= padding.Left + image.Width || y >= padding.Top + image.Height )
					{
						bmp.SetPixel( x, y, paddingColor );
					}
					else
					{
						bmp.SetPixel( x, y, image.GetPixel( x - padding.Left, y - padding.Top ) );
					}
				}
			}

			return bmp;
		}

		#endregion // AddPadding

		#region GetChild

		internal static UIElement GetChild( UIElement parent, Type type )
		{
			if ( parent.HasChildElements )
			{
				UIElementsCollection list = parent.ChildElements;
				for ( int i = 0, count = list.Count; i < count; i++ )
				{
					UIElement elem = list[i];
					if ( GridUtils.IsObjectOfType( elem, type ) )
						return elem;
				}
			}

			return null;
		}

		#endregion // GetChild

		#region MaxActivation

		// SSP 5/2/05 BR04321
		// After discussing with Joe, the behavior was changed so Disabled takes precedence 
		// over the NoEdit.
		// 
		private static readonly int[] activationLogicalOrder = { 0, 1, 3, 2 };
		internal static Activation MaxActivation( Activation a1, Activation a2 )
		{
			return activationLogicalOrder[ (int)a1 ] > activationLogicalOrder[ (int)a2 ] ? a1 : a2;
		}

		#endregion // MaxActivation

		#region GetBandAssociatedWithChapteredColumn

		// SSP 6/2/05 BR03609
		// Added GetBandAssociatedWithChapteredColumn method.
		// 
		internal static UltraGridBand GetBandAssociatedWithChapteredColumn( UltraGridColumn chapteredColumn )
		{
			UltraGridLayout layout = null != chapteredColumn.Band ? chapteredColumn.Band.Layout : null;
			BandsCollection bands = null != layout ? layout.SortedBands : null;

			if ( null != bands )
			{
				for ( int i = 0, count = bands.Count; i < count; i++ )
				{
					UltraGridBand band = bands[i];
					if ( band.ParentColumn == chapteredColumn )
						return band;
				}
			}

			return null;			
		}

		#endregion // GetBandAssociatedWithChapteredColumn

		#region RemoveAll
		
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static void RemoveAll( ArrayList list, object itemToRemove )
		internal static void RemoveAll<T>( List<T> list, T itemToRemove ) where T : class
		{
			int delta = 0;
			int count = list.Count;

			for ( int i = 0; i < count; i++ )
			{
				if ( itemToRemove == list[i] )
					delta++;
				else if ( 0 != delta )
					list[ i - delta ] = list[i];
			}

			list.RemoveRange( count - delta, delta );
		}

		#endregion // RemoveAll

		#region GetDragIndicatorOffset

		// SSP 6/29/05 - NAS 5.3 Column Chooser
		// 
		internal static Point GetDragIndicatorOffset( UIElement[] dragElements )
		{
			Rectangle rect = dragElements[0].Rect;
			for (int i=1; i< dragElements.Length; i++)
				rect = Rectangle.Union(rect, dragElements[i].Rect);

			// Default to bottom-center if element or element's control is not available.
			// 
			Point dragWindowOffset = new Point( - rect.Width / 2, - rect.Height );

			UIElement element = dragElements[0];
			if ( null != element.Control )
			{
				Point mouseLoc =  element.Control.PointToClient( Control.MousePosition );
				if ( rect.Contains( mouseLoc ) )
					dragWindowOffset = new Point( rect.X - mouseLoc.X, rect.Y - mouseLoc.Y );
			}

			return dragWindowOffset;
		}

		#endregion // GetDragIndicatorOffset

		#region GetColumnFromLayoutItem

		internal static UltraGridColumn GetColumnFromLayoutItem( ILayoutItem item )
		{
			UltraGridColumn column = item as UltraGridColumn;
			if ( null == column )
			{
				if ( item is ColumnHeader )
					column = ((ColumnHeader)item).Column;
				else if ( item is RowAutoSizeLayoutManagerHolder.CellLayoutItem )
					column = ((RowAutoSizeLayoutManagerHolder.CellLayoutItem)item).Column;
					// SSP 8/3/05
					// 
				else if ( item is SummaryLayoutItem )
					column = ((SummaryLayoutItem)item).Column;

			}

			return column;
		}

		#endregion // GetColumnFromLayoutItem

		#region SetCursorToCurrent
		
		// SSP 7/13/05 - NAS 5.3 Column Chooser
		// Moved this from the DragEffect.
		//
		internal static void SetCursorToCurrent( UltraGridBase grid, CurrentCursor currentCursor )
		{
			Cursor cursor = null;

			switch ( currentCursor )
			{
				case CurrentCursor.Valid:
					cursor = Cursors.Arrow;
					break;
				case CurrentCursor.Invalid:
					cursor = Cursors.No;
					break;
					// SSP 7/13/05 - NAS 5.3 Column Chooser
					// Added HideColumnCursor. 
					// 
				case CurrentCursor.HideColumnCursor:
					cursor = null != grid ? grid.HideColumnCursor : Cursors.No;
					break;
				default:
					cursor = Cursors.Default;
					break;
			}
	
			if ( null != cursor )
				Cursor.Current = cursor;
		}

		#endregion // SetCursorToCurrent

		#region InitializeArray

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal static void InitializeArray( Array arr, object val )
		//{
		//    for ( int i = 0; i < arr.Length; i++ )
		//        arr.SetValue( val, i );
		//}

		#endregion Not Used

		internal static void InitializeArray( int[] arr, int val )
		{
			InitializeArray( arr, val, 0, arr.Length );
		}

		internal static void InitializeArray( int[] arr, int val, int startIndex, int length )
		{
			for ( int i = 0; i < length; i++ )
				arr[ startIndex + i ] = val;
		}

		#endregion // InitializeArray

		#region Deflate

		internal static void Deflate( ref Size size, int delta )
		{
			Deflate( ref size, delta, delta );
		}

		internal static void Deflate( ref Size size, int deltaWidth, int deltaHeight )
		{
			size.Width -= deltaWidth;
			size.Height -= deltaHeight;
		}

		#endregion // Deflate

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region Inflate

		//#if DEBUG
		//        /// <summary>
		//        /// Increases the width and the height of the size by the delta amount.
		//        /// </summary>
		//        /// <param name="size"></param>
		//        /// <param name="delta"></param>
		//#endif
		//        internal static void Inflate( ref Size size, int delta )
		//        {
		//            Inflate( ref size, delta, delta );
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Increases the width and the height of the size by the deltaWidth and deltaHeight amounts.
		//        /// </summary>
		//        /// <param name="size"></param>
		//        /// <param name="deltaWidth"></param>
		//        /// <param name="deltaHeight"></param>
		//#endif
		//        internal static void Inflate( ref Size size, int deltaWidth, int deltaHeight )
		//        {
		//            size.Width += deltaWidth;
		//            size.Height += deltaHeight;
		//        }

		//        #endregion // Inflate

		#endregion Not Used

		#region IsHotTracking

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal static bool IsHotTracking( UIElement elem, bool isHotTracking )
		{
			if ( ! isHotTracking || null == elem 
				// SSP 9/9/05 BR06305
				// 
				|| ! elem.Enabled )
				return false;

			// Don't hottrack anything if a cell is in edit mode.
			// 
			UltraGridLayout layout = (UltraGridLayout)elem.GetContext( typeof( UltraGridLayout ) );
			if ( null == layout || null != layout.CellInEditMode 
				// SSP 9/9/05 BR06307
				// 
				|| null != layout.CellGoingIntoEditMode )
				return false;

			// If an element has mouse capture return false.
			// 
			ControlUIElementBase controlElem = elem.ControlElement;
			if ( null == controlElem || null != controlElem.ElementWithMouseCapture )
				return false;

			return true;
		}

		#endregion // IsHotTracking

		#region IsRowHotTracking

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal static bool IsRowHotTracking( UIElement rowDescendantElem )
		{
			RowUIElementBase rowElem = null != rowDescendantElem 
				? (RowUIElementBase)rowDescendantElem.GetAncestor( typeof( RowUIElementBase ) )
				: null;

			return null != rowElem && IsHotTracking( rowElem, rowElem.isMouseOverSelectorOrCellArea );
		}

		#endregion // IsRowHotTracking

		#region GetElemCount

		internal static int GetElemCount( object[] arr, object elem )
		{
			int c = 0;
			for ( int i = 0; i < arr.Length; i++ )
			{
				if ( elem == arr[i] )
					c++;
			}

			return c;
		}

		#endregion // GetElemCount

		#region GetFirstItem

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Returns the first item in the specified enumerable.
		//        /// </summary>
		//        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		//        /// <returns></returns>
		//#endif
		//        internal static object GetFirstItem( IEnumerable e )
		//        {
		//            foreach ( object o in e )
		//                return o;

		//            return null;
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Returns the first item in the specified array. If the array is null, or
		//        /// it has no items then returns null.
		//        /// </summary>
		//        /// <param name="arr"></param>
		//        /// <returns></returns>
		//#endif
		//        internal static object GetFirstItem( object[] arr )
		//        {
		//            return null != arr && arr.Length > 0 ? arr[0] : null;
		//        }

		#endregion Not Used

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static object GetFirstItem( ArrayList list )
		internal static T GetFirstItem<T>( List<T> list )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return null != list && list.Count > 0 ? list[0] : null;
			return null != list && list.Count > 0 ? list[ 0 ] : default( T );
		}

		#endregion // GetFirstItem

		#region GetLastItem

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Returns the last item in the specified enumerable.
		//        /// </summary>
		//        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		//        /// <returns></returns>
		//#endif
		//        internal static object GetLastItem( IEnumerable e )
		//        {
		//            object lastItem = null;
		//            foreach ( object o in e )
		//                lastItem = o;

		//            return lastItem;
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Returns the last item in the specified array. If the array is null, or
		//        /// it has no items then returns null.
		//        /// </summary>
		//        /// <param name="arr"></param>
		//        /// <returns></returns>
		//#endif
		//        internal static object GetLastItem( object[] arr )
		//        {
		//            return null != arr && arr.Length > 0 ? arr[ arr.Length - 1 ] : null;
		//        }

		#endregion Not Used

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static object GetLastItem( ArrayList list )
		internal static T GetLastItem<T>( List<T> list )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return null != list && list.Count > 0 ? list[ list.Count - 1 ] : null;
			return null != list && list.Count > 0 ? list[ list.Count - 1 ] : default( T );
		}

		#endregion // GetLastItem

		#region IsBackgroundOpaque

		// SSP 12/2/05 BR07533
		// 
		internal static bool IsBackgroundOpaque( ref AppearanceData appData )
		{
			if ( Alpha.Transparent == appData.BackColorAlpha )
				return false;

			if ( Alpha.Default == appData.BackColorAlpha 
				|| Alpha.UseAlphaLevel == appData.BackColorAlpha )
			{
				int alpha = appData.AlphaLevel;
				if ( 0 != alpha && alpha < 255 )
					return false;
			}

			return true;
		}

		#endregion // IsBackgroundOpaque

		#region IsBackgroundFullyTransparent

		// SSP 4/29/06 - App Styling - BR11852
		// 
		internal static bool IsBackgroundFullyTransparent( ref AppearanceData appData )
		{
			if ( null == appData.ImageBackground || Alpha.Transparent == appData.ImageBackgroundAlpha )
			{
				if ( Alpha.Transparent == appData.BackColorAlpha )
					return true;

				if ( ( Color.Empty == appData.BackColor || Color.Transparent == appData.BackColor )
					&& ( GradientStyle.None == appData.BackGradientStyle || GradientStyle.Default == appData.BackGradientStyle
					|| Color.Empty == appData.BackColor2 || Color.Transparent == appData.BackColor2 ) )
					return true;
			}

			return false;				
		}

		#endregion // IsBackgroundFullyTransparent

		#region ShouldDrawBackColor

		// SSP 4/29/06 - App Styling - BR11852
		// 
		internal static bool ShouldDrawBackColor( ref AppearanceData appData )
		{
			return ! IsBackgroundFullyTransparent( ref appData );
		}

		// SSP 4/29/06 - App Styling - BR11852
		// 
		internal static bool ShouldDrawBackColor( UltraGridLayout layout, StyleUtils.Role eRole )
		{
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( layout, eRole, out order );

			if ( ( order.UseStyleBefore || order.UseStyleAfter ) && null != role )
			{
				AppearanceData appData = new AppearanceData( );
				AppearancePropFlags flags = AppearancePropFlags.AllRender;
				role.ResolveAppearance( ref appData, ref flags, AppStyling.RoleState.Normal );

				if ( GridUtils.ShouldDrawBackColor( ref appData ) )
					return true;
			}

			return false;
		}

		#endregion // ShouldDrawBackColor

		#region GetRowScrollRegion

		internal static RowScrollRegion GetRowScrollRegion( UIElement elem )
		{
			return null != elem ? (RowScrollRegion)elem.GetContext( typeof( RowScrollRegion ) ) : null;
		}

		#endregion // GetRowScrollRegion

		#region GetColScrollRegion
		
		internal static ColScrollRegion GetColScrollRegion( UIElement elem )
		{
			return null != elem ? (ColScrollRegion)elem.GetContext( typeof( ColScrollRegion ) ) : null;
		}

		#endregion // GetColScrollRegion

		#region IsElemFromActiveRsrCsrIntersection

		internal static bool IsElemFromActiveRsrCsr( UIElement elem )
		{
			RowScrollRegion rsr = GetRowScrollRegion( elem );
			ColScrollRegion csr = GetColScrollRegion( elem );

			return null != rsr && null != csr && rsr.IsActiveScrollRegion && csr.IsActiveScrollRegion;
		}

		#endregion // IsElemFromActiveRsrCsrIntersection

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#region GetNextItem

		//internal static object GetNextItem( ArrayList list, object item )
		//{
		//    return GetNextPrevItem( list, item, true );
		//}

		//#endregion // GetNextItem

		#endregion Not Used

		#region GetPrevItem

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static object GetPrevItem( ArrayList list, object item )
		internal static T GetPrevItem<T>( List<T> list, T item )
		{
			return GetNextPrevItem( list, item, false );
		}

		#endregion // GetPrevItem

		#region GetNextPrevItem

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static object GetNextPrevItem( ArrayList list, object item, bool next )
		internal static T GetNextPrevItem<T>( List<T> list, T item, bool next )
		{
			if ( null != list )
			{
				int i = list.IndexOf( item );

				if ( next )
					i++;
				else
					i--;

				if ( i >= 0 && i < list.Count )
					return list[i];
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return null;
			return default( T );
		}

		#endregion // GetNextPrevItem

		#region IsFromSameRowsCollection

		// SSP 12/5/05 - NAS 6.1 Copy & Paste
		// 
		internal static bool IsFromSameRowsCollection( IEnumerable cells )
		{
			UltraGridBand band = null;

			foreach ( UltraGridCell cell in cells )
			{
				if ( null == band )
					band = cell.Band;
				else if ( band != cell.Band )
					return false;
			}

			return true;
		}

		#endregion // IsFromSameRowsCollection

		#region NonNullCriteria 

		// SSP 12/13/05 - Recursive Row Enumerator
		// 
		internal class NonNullCriteria : IMeetsCriteria
		{
			public bool MeetsCriteria( object item )
			{
				return null != item;
			}
		}

		#endregion // NonNullCriteria 

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uninstantiated internal classes
		#region Not Used

		//#region MeetsCriteriaComplement Class

		//// SSP 12/13/05 - Recursive Row Enumerator
		//// 
		//internal class MeetsCriteriaComplement : IMeetsCriteria
		//{
		//    private IMeetsCriteria i;

		//    internal MeetsCriteriaComplement( IMeetsCriteria i )
		//    {
		//        GridUtils.ValidateNull( i );

		//        this.i = i;
		//    }

		//    public bool MeetsCriteria( object item )
		//    {
		//        return ! this.i.MeetsCriteria( item );
		//    }
		//}

		//#endregion // MeetsCriteriaComplement Class

		//#region MeetsCriteriaChain Class

		//// SSP 12/13/05 - Recursive Row Enumerator
		//// 
		//internal class MeetsCriteriaChain : IMeetsCriteria
		//{
		//    private IMeetsCriteria x, y;
		//    private bool combineUsingOr;

		//    internal MeetsCriteriaChain( IMeetsCriteria x, IMeetsCriteria y, bool combineUsingOr )
		//    {
		//        GridUtils.ValidateNull( x );
		//        GridUtils.ValidateNull( y );

		//        this.x = x;
		//        this.y = y;
		//        this.combineUsingOr = combineUsingOr;
		//    }

		//    public bool MeetsCriteria( object item )
		//    {
		//        bool x = this.x.MeetsCriteria( item );
		//        bool y = this.y.MeetsCriteria( item );

		//        return this.combineUsingOr ? x || y : x && y;
		//    }
		//}

		//#endregion // MeetsCriteriaChain Class

		#endregion Not Used

		#region MeetsCriteriaEnumerator Class

		// SSP 12/13/05 - Recursive Row Enumerator
		// 
		internal class MeetsCriteriaEnumerator : IEnumerator
		{
			private IEnumerator enumerator;
			private IMeetsCriteria meetsCriteriaCallback;
			private object currentItem;

			internal MeetsCriteriaEnumerator( IEnumerator enumerator, IMeetsCriteria meetsCriteriaCallback )
			{
				GridUtils.ValidateNull( enumerator );
				GridUtils.ValidateNull( meetsCriteriaCallback );

				this.enumerator = enumerator;
				this.meetsCriteriaCallback = meetsCriteriaCallback;
			}

			public void Reset( )
			{
				this.enumerator.Reset( );
				this.currentItem = null;
			}

			public bool MoveNext( )
			{
				do
				{
					if ( ! this.enumerator.MoveNext( ) )
						return false;

					this.currentItem = this.enumerator.Current;
				}
				while ( ! this.meetsCriteriaCallback.MeetsCriteria( this.currentItem ) );

				return true;
			}

			public object Current
			{
				get
				{
					return this.currentItem;
				}
			}
		}

		#endregion // MeetsCriteriaEnumerator Class

		#region MeetsCriteriaEnumerable Class

		// SSP 12/13/05 - Recursive Row Enumerator
		// 
		internal class MeetsCriteriaEnumerable : IEnumerable
		{
			private IEnumerable e;
			private IMeetsCriteria i;

			internal MeetsCriteriaEnumerable( IEnumerable e, IMeetsCriteria i )
			{
				GridUtils.ValidateNull( e );
				GridUtils.ValidateNull( i );

				this.e = e;
				this.i = i;
			}

			public IEnumerator GetEnumerator( )
			{
				return new MeetsCriteriaEnumerator( this.e.GetEnumerator( ), this.i );
			}
		}

		#endregion // MeetsCriteriaEnumerable Class

		#region GetRoot

		// SSP 12/28/05 BR08374
		// 
		internal static Control GetRoot( Control control )
		{
			if ( null != control )
			{
				while ( null != control.Parent )
					control = control.Parent;
			}

			return control;
		}

		#endregion // GetRoot

		#region GetRootControlForSearching

		// SSP 12/28/05 BR08374
		// 
		internal static Control GetRootControlForSearching( UltraGridBase grid )
		{
			if ( null == grid )
				return null;

			Control ret = grid.FindForm( );
			if ( null == ret )
				ret = GridUtils.GetRoot( grid );

			return ret;
		}

		#endregion // GetRootControlForSearching

		#region HasBackGradient

		// SSP 2/21/06 BR09319
		// 
		internal static bool IsBackgroundScrollable( AppearanceBase app )
		{
			// MRS 10/22/04 - UWG3066
			// MRS 10/25/04 - Check the Alpha, not just transparent
			//if (this.Appearance.BackColor == Color.Transparent)
			// SSP 11/29/04
			// Only check the alpha if the color is not empty. Empty colors return 0 for A.
			//
			//if (this.Appearance.BackColor.A < 255)
			if ( ! app.BackColor.IsEmpty && app.BackColor.A < 255 )
				return false;

			// JJD 11/05/01
			// if we have a background image we can't scroll
			//
			if ( GridUtils.HasBackgroundImage( app ) )
				return false;

			// JJD 11/05/01
			// if we are doign a gradient back fill we can't scroll
			//
			switch ( app.BackGradientStyle )
			{
				case GradientStyle.Default:
				case GradientStyle.None:
					// SSP 2/21/06 BR09319
					// Changed the structure so we return true at the end.
					// 
					//return true;
					break;

				default:
					if ( app.BackColor2 != Color.Empty )
						return false;

					break;
			}

			return true;
		}

		#endregion // HasBackGradient

		#region HasBackgroundImage
		
		// SSP 2/21/06 BR09319
		// 		
		internal static bool HasBackgroundImage( AppearanceBase app )
		{
			// Check if we have a background image
			//
			System.Drawing.Image imageBackground = app.ImageBackground;

			if ( null != imageBackground &&
				imageBackground.Height > 0 &&
				imageBackground.Width > 0 )
			{
				return true;
			}

			return false;
		}

		#endregion // HasBackgroundImage

		#region AlphabeticalComparer

		// SSP 8/28/06 BR14825
		// Added a class for comparing string attributes of specific classes. Add support for more
		// classes as needde.
		// 
		internal class AlphabeticalComparer : IComparer
		{
            // MRS 7/24/2009 - TFS19613
            // ---------------------------------------------------------------------            
            bool useColumnChooserCaption = false;

            internal AlphabeticalComparer() : this(false) { }

            internal AlphabeticalComparer(bool useColumnChooserCaption)
            {
                this.useColumnChooserCaption = useColumnChooserCaption;
            }
            // ---------------------------------------------------------------------

			public int Compare( object x, object y )
			{
				string xxStr = null;
				string yyStr = null;
				bool ignoreCase = true;

				if ( x is UltraGridColumn && y is UltraGridColumn )
				{
                    // MRS 7/24/2009 - TFS19613
                    //xxStr = ((UltraGridColumn)x).Header.Caption;
                    //yyStr = ((UltraGridColumn)y).Header.Caption;
                    if (this.useColumnChooserCaption)
                    {
                        xxStr = ((UltraGridColumn)x).ColumnChooserCaptionResolved;
                        yyStr = ((UltraGridColumn)y).ColumnChooserCaptionResolved;
                    }
                    else
                    {
                        xxStr = ((UltraGridColumn)x).Header.Caption;
                        yyStr = ((UltraGridColumn)y).Header.Caption;
                    }
				}
				else
					Debug.Assert( false, "Unknown type of objects." );

				return string.Compare( xxStr, yyStr, ignoreCase );
			}
		}

		#endregion // AlphabeticalComparer

		#region DisplaysTextValues

		// SSP 9/11/06 BR14332
		// 
		internal static bool DisplaysTextValues( UltraGridColumn column )
		{
			IValueList iValueList = column.ValueList;
			ValueList vl = iValueList as ValueList;
			if ( null != vl )
			{
				switch ( vl.DisplayStyle )
				{
					case ValueListDisplayStyle.DisplayText:
					case ValueListDisplayStyle.DisplayTextAndPicture:
						return true;
				}
			}

			// If there is a value list then we'll be treating the mapped values as text.
			// 
			if ( null != iValueList )
				return true;

			EmbeddableEditorBase editor = column.Editor;
			if ( null != editor && editor.SupportsValueList 
				&& ! editor.ComparesByValue( column.EditorOwnerInfo, null ) )
				return true;

			return false;
		}

		#endregion // DisplaysTextValues

        // MRS 12/19/06 - fxCop
        #region SetCursorPosition
        internal static void SetCursorPosition(Point screenPosition)
        {
            try
            {
                Cursor.Position = screenPosition;
            }
            catch (System.Security.SecurityException)
            {
                try
                {
                    NativeWindowMethods.SetCursorPosApi(screenPosition);
                }
                catch (System.Security.SecurityException) { }
            }
        }
        #endregion SetCursorPosition

        // MRS 12/19/06 - fxCop
        #region FocusControl
        internal static bool FocusControl(Control control)
        {
            try
            {
                return control.Focus();
            }
            catch (System.Security.SecurityException)
            {
                try
                {
                    if (control.CanFocus)
                        NativeWindowMethods.SetFocusApi(control.Handle);
                }
                catch (System.Security.SecurityException) { }
            }

            return control.Focused;
        }
        #endregion FocusControl

        // MRS 12/19/06 - fxCop - copied from Win 'MD 11/7/06 - BR17484'
        // I copied this method here and made it internal. We can't use the method in
        // Win, because changing the method in Win would open a potential security hole. 
        #region FocusControlWithoutScrollingIntoView

        internal static void FocusControlWithoutScrollingIntoView(Control control)
        {
            Point oldAutoScrollOffset = control.AutoScrollOffset;

            try
            {
                Point newOffset = control.Location;
                Control parent = control.Parent;

                while (parent != null)
                {
                    ScrollableControl scrollableParent = parent as ScrollableControl;

                    if (scrollableParent != null)
                        break;

                    newOffset.Offset(parent.Location);
                    parent = parent.Parent;
                }

                control.AutoScrollOffset = newOffset;

                if (DisposableObject.HasSamePublicKey(control.GetType()))
                    GridUtils.FocusControl(control);
                else
                    control.Focus();
            }
            finally
            {
                control.AutoScrollOffset = oldAutoScrollOffset;
            }
        }

        #endregion FocusControlWithoutScrollingIntoView

		#region GetDefaultDataType

		// SSP 2/1/08 BR25932
		// 
		/// <summary>
		/// For column styles that have specific data types associated with them, it
		/// returns the associated data type.
		/// </summary>
		/// <param name="style">Column style</param>
		/// <returns>Returns the associated data type or null if the specified style does not have
		/// any specific data type associated with it</returns>
		public static Type GetDefaultDataType( ColumnStyle style )
		{
			switch ( style )
			{
				case ColumnStyle.CheckBox:
				case ColumnStyle.TriStateCheckBox:
					return typeof( bool );
				case ColumnStyle.Color:
					return typeof( Color );
				case ColumnStyle.Currency:
				case ColumnStyle.CurrencyNonNegative:
				case ColumnStyle.CurrencyPositive:
					return typeof( decimal );
				case ColumnStyle.Time:
				case ColumnStyle.TimeWithSpin:
					return typeof( DateTime );
				case ColumnStyle.Date:
				case ColumnStyle.DateTime:
				case ColumnStyle.DateTimeWithoutDropDown:
				case ColumnStyle.DateTimeWithSpin:
				case ColumnStyle.DateWithoutDropDown:
				case ColumnStyle.DateWithSpin:
				case ColumnStyle.DropDownCalendar:
					return typeof( DateTime );
				case ColumnStyle.TimeZone:
					return typeof( TimeZone );
				case ColumnStyle.Double:
				case ColumnStyle.DoubleNonNegative:
				case ColumnStyle.DoubleNonNegativeWithSpin:
				case ColumnStyle.DoublePositive:
				case ColumnStyle.DoublePositiveWithSpin:
				case ColumnStyle.DoubleWithSpin:
					return typeof( double );
				case ColumnStyle.Font:
                    // MRS 2/20/2008 - BR30701
                    // FontNameEditor doesn't edit font's, it edits font names: strings. 
                    //return typeof( Font );
                    return typeof(string);
                case ColumnStyle.Integer:
				case ColumnStyle.IntegerNonNegative:
				case ColumnStyle.IntegerNonNegativeWithSpin:
				case ColumnStyle.IntegerPositive:
				case ColumnStyle.IntegerPositiveWithSpin:
				case ColumnStyle.IntegerWithSpin:
					return typeof( int );
			}

			return null;
		}

		#endregion // GetDefaultDataType

        // MBS 8/18/08 - BR35632
        // Copied from Win.Utilities
        #region GetWeakReferenceTarget

        // AS 10/23/06
        // Changed to internal since we can use this elsewhere in win.
        //
        //private static object GetWeakReferenceTarget( WeakReference weakRef )
        internal static object GetWeakReferenceTarget(WeakReference weakRef)
        {
            try
            {
                // AS 10/25/06
                //if ( weakRef.IsAlive == false )
                if (weakRef == null || weakRef.IsAlive == false)
                    return null;

                return weakRef.Target;
            }
            catch (InvalidOperationException)
            {
                return null;
            }

        }
        #endregion GetWeakReferenceTarget


        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

        internal static LayoutManagerBase GetFlattenedLayoutManager(LayoutManagerBase groupedLayoutManager)
        {
            LayoutManagerBase flattenedLayoutManager = UltraGridBand.CreateGridBagLayoutManager();            
            foreach (ILayoutItem item in groupedLayoutManager.LayoutItems)
            {
                GroupLayoutItemBase groupLayoutItemBase = item as GroupLayoutItemBase;
                if (groupLayoutItemBase != null)
                {
                    IGridBagConstraint groupConstraint = groupedLayoutManager.LayoutItems.GetConstraint(groupLayoutItemBase) as IGridBagConstraint;

                    // MRS 2/19/2009 - TFS14175
                    // If this is a GroupLayoutItemCellContent and there are no contents, 
                    // we need to add in an item as a placeholder so that the headers and cells 
                    // match up.
                    //
                    if ( !(groupLayoutItemBase is GroupLayoutItemHeaderContent) &&
                        groupLayoutItemBase.LayoutManager.LayoutItems.Count == 0)
                    {
                        object constraint = groupedLayoutManager.LayoutItems.GetConstraint(item);
                        flattenedLayoutManager.LayoutItems.Add(item, constraint);
                        continue;
                    }

                    int offSetX = groupConstraint.OriginX;
                    int offSetY = groupConstraint.OriginY;
                    GetFlattenedLayoutManagerHelper(flattenedLayoutManager, groupLayoutItemBase, offSetX, offSetY);
                }
                else
                {
                    object constraint = groupedLayoutManager.LayoutItems.GetConstraint(item);
                    flattenedLayoutManager.LayoutItems.Add(item, constraint);
                }
            }
            return flattenedLayoutManager;
        }

        internal static void GetFlattenedLayoutManagerHelper(LayoutManagerBase flattenedLayoutManager, GroupLayoutItemBase groupLayoutItemBase, int offSetX, int offSetY)
        {            
            LayoutManagerBase groupedLayoutManager = groupLayoutItemBase.LayoutManager;
            foreach (ILayoutItem item in groupedLayoutManager.LayoutItems)
            {
                GroupLayoutItemBase childGroupLayoutItemBase = item as GroupLayoutItemBase;
                if (childGroupLayoutItemBase != null)
                {
                    IGridBagConstraint groupConstraint = groupedLayoutManager.LayoutItems.GetConstraint(item) as IGridBagConstraint;
                    int childOffSetX = offSetX + groupConstraint.OriginX;
                    int childOffSetY = offSetY + groupConstraint.OriginY;
                    GetFlattenedLayoutManagerHelper(flattenedLayoutManager, childGroupLayoutItemBase, childOffSetX, childOffSetY);
                }
                else
                {
                    IGridBagConstraint groupedConstraint = groupedLayoutManager.LayoutItems.GetConstraint(item) as IGridBagConstraint;
                    FlattenedGridBagConstraint flattenedConstraint = new FlattenedGridBagConstraint(groupedConstraint, offSetX, offSetY);
                    flattenedLayoutManager.LayoutItems.Add(item, flattenedConstraint);
                }
            }            
        }

        public class FlattenedGridBagConstraint : IGridBagConstraint
        {
            #region Private Members
            IGridBagConstraint groupedGridBagConstraint;
            private int offSetX;
            private int offSetY;
            #endregion //Private Members

            #region Constructor
            internal FlattenedGridBagConstraint(IGridBagConstraint groupedGridBagConstraint, int offSetX, int offSetY)
            {
                this.groupedGridBagConstraint = groupedGridBagConstraint;
                this.offSetX = offSetX;
                this.offSetY = offSetY;
            }
            #endregion //Constructor

            #region IGridBagConstraint Members

            public AnchorType Anchor
            {
                get { return this.groupedGridBagConstraint.Anchor; }
            }

            public FillType Fill
            {
                get { return this.groupedGridBagConstraint.Fill; }
            }

            public Insets Insets
            {
                get { return this.groupedGridBagConstraint.Insets; }
            }

            public int OriginX
            {
                get { return this.groupedGridBagConstraint.OriginX + this.offSetX; }
            }

            public int OriginY
            {
                get { return this.groupedGridBagConstraint.OriginY + this.offSetY; }
            }

            public int SpanX
            {
                get { return this.groupedGridBagConstraint.SpanX; }
            }

            public int SpanY
            {
                get { return this.groupedGridBagConstraint.SpanY; }
            }

            public float WeightX
            {
                get { return this.groupedGridBagConstraint.WeightX; }
            }

            public float WeightY
            {
                get { return this.groupedGridBagConstraint.WeightY; }
            }

            #endregion
        }

        #endregion // NAS 9.1 - Groups in RowLayout

		// MD 1/12/09 - Groups in RowLayout
		#region GetHeaderFromLayoutItem

		internal static HeaderBase GetHeaderFromLayoutItem( ILayoutItem item )
		{
			HeaderBase header = item as HeaderBase;
			if ( null != header )
				return header;

			UltraGridColumn column = item as UltraGridColumn;
			if ( column != null )
				return column.Header;

			GroupLayoutItemBase groupLayoutItem = item as GroupLayoutItemBase;
			if ( groupLayoutItem != null )
				return groupLayoutItem.Group.Header;

			RowAutoSizeLayoutManagerHolder.CellLayoutItem cellLayoutItem = item as RowAutoSizeLayoutManagerHolder.CellLayoutItem;
			if ( cellLayoutItem != null )
				return cellLayoutItem.Column.Header;

			SummaryLayoutItem summaryLayoutItem = item as SummaryLayoutItem;
			if ( summaryLayoutItem != null )
				return summaryLayoutItem.Column.Header;

			return null;
		}

		#endregion GetHeaderFromLayoutItem
	}

	#endregion // GridUtils Class

	// MD 1/24/08
	// Made changes to allow for VS2008 style unit test accessors
	// Moved outside from GridUtils
	#region IMeetsCriteria Interface

	// SSP 12/13/05 - Recursive Row Enumerator
	// 
	internal interface IMeetsCriteria
	{
		bool MeetsCriteria( object item );
	}

	#endregion // IMeetsCriteria Interface

	#region DefaultableFlagsEnumUITypeEditor

	/// <summary>
	/// UITypeEditor class for editing flags enum that have non-zero Default enum member.
	/// </summary>
    // MRS 12/19/06 - fxCop
    //
    
    
    // SSP 8/16/06 - NAS 6.3
	// Needed to use DefaultableFlagsEnumUITypeEditor for other UltraFormattedTextEditor so moved
	// the implementation into win and named it slightly different so there are no conflicts.
	// 
	//public class DefaultableFlagsEnumUITypeEditor : Infragistics.Shared.FlagsEnumUITypeEditor 
	public class DefaultableFlagsEnumUITypeEditor : Infragistics.Win.Design.DefaultableFlagsEnumerationUITypeEditor
	{
		// SSP 8/16/06 - NAS 6.3
		// Needed to use DefaultableFlagsEnumUITypeEditor for other UltraFormattedTextEditor so moved
		// the implementation into win and named it slightly different so there are no conflicts.
		// 
		
	}

	#endregion // DefaultableFlagsEnumUITypeEditor

	#region LocalizedResourceGetter

	// SSP 5/16/05 - NAS 5.2 Filter Row
	// Added LocalizedResourceGetter.
	//
	internal class LocalizedResourceGetter
	{
		internal string resourceName = null;

		internal LocalizedResourceGetter( string resourceName )
		{
			this.resourceName = resourceName;
		}

		public override string ToString( )
		{
			return SR.GetString( this.resourceName );
		}
	}

	#endregion // LocalizedResourceGetter

	#region AppearanceManager

	// SSP 7/6/05 - NAS 5.3 Empty Rows
	// 
	/// <summary>
	/// Class for managing appearance properties.
	/// </summary>
	[ Serializable( ) ]
	public class AppearanceManager : SubObjectBase, ISerializable
	{
		#region Private Vars

		private UltraGridLayout layout = null;
		private Infragistics.Win.AppearanceHolder[] appearanceHolders = null;
		private PropertyIds[] appearancePropIds = null;
		private int maxAppearanceCount = -1;

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList deserializedAppearances = null;
		private List<AppearanceHolder> deserializedAppearances = null;

		#endregion // Private Vars

		#region Constructor
		
		internal AppearanceManager( UltraGridLayout layout, int maxAppearanceCount, PropertyIds[] appearancePropIds )
		{
			this.Initialize( layout, maxAppearanceCount, appearancePropIds );
		}
		
		internal AppearanceManager( SerializationInfo info, StreamingContext context )
		{
			// Since we're not saving properties with default values, we must set the default here.
			//
			this.Reset();

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<AppearanceHolder> list = new List<AppearanceHolder>();

			this.deserializedAppearances = list;

			foreach( SerializationEntry entry in info )
			{
				if ( typeof( AppearanceHolder ) == entry.ObjectType )
				{
					int index = int.Parse( entry.Name );
					AppearanceHolder appHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );

					while ( index > list.Count - 1 )
						list.Add( null );

					Debug.Assert( null == list[ index ] );
					list[ index ] = appHolder;
				}
				else
				{
					switch ( entry.Name )
					{
						case "Tag":
							this.DeserializeTag( entry );
							break;
						case "maxAppearanceCount":
							this.maxAppearanceCount = (int)Utils.DeserializeProperty( entry, typeof(int), this.maxAppearanceCount );
							break;
						default:
							Debug.Assert( false, "Unknown property serialized" );
							break;
					}
				}
			}
		}

		#endregion // Constructor

		#region OnDeserializationComplete

		internal void OnDeserializationComplete( UltraGridLayout layout, int maxAppearanceCount, PropertyIds[] appearancePropIds )
		{
			this.Initialize( layout, maxAppearanceCount, appearancePropIds );

			if ( null != this.deserializedAppearances && this.deserializedAppearances.Count > 0 )
			{
				Debug.Assert( null == this.appearanceHolders );
				this.appearanceHolders = new AppearanceHolder[ this.maxAppearanceCount ];
				int count = Math.Min( this.appearanceHolders.Length, this.deserializedAppearances.Count );

				for ( int i = 0; i < count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//this.appearanceHolders[ i ] = (AppearanceHolder)this.deserializedAppearances[ i ];
					this.appearanceHolders[ i ] = this.deserializedAppearances[ i ];
				}
			}
		}

		#endregion // OnDeserializationComplete

		#region Initialize

		private void Initialize( UltraGridLayout layout, int maxAppearanceCount, PropertyIds[] appearancePropIds )
		{
			GridUtils.ValidateNull( "appearancePropIds", appearancePropIds );

			this.layout = layout;
			this.maxAppearanceCount = maxAppearanceCount;
			this.appearancePropIds = appearancePropIds;

			if ( null != this.appearanceHolders && this.maxAppearanceCount != this.appearanceHolders.Length )
			{
				Debug.Assert( false, "The maxAppearanceCount should never change once appearanceHolders has been allocated !" );

				AppearanceHolder[] oldArr = this.appearanceHolders;
				this.appearanceHolders = new AppearanceHolder[ this.maxAppearanceCount ];

				Debug.Assert( this.appearanceHolders.Length >= this.deserializedAppearances.Count,
					"There should not have been more appearances serialized than the maxAppearanceCount." );

				int count = Math.Min( oldArr.Length, this.appearanceHolders.Length );

				for ( int i = 0; i < count; i++ )
					this.appearanceHolders[i] = oldArr[i];
			}
		}

		#endregion // Initialize

		#region Appearances

		private AppearancesCollection Appearances
		{
			get
			{
				return null != this.layout ? this.layout.Appearances : null;
			}
		}

		#endregion // Appearances
		
		#region AppearanceHolders
		
		private Infragistics.Win.AppearanceHolder[] AppearanceHolders
		{
			get
			{
				// create the appearanceHolders on the 
				if ( null == this.appearanceHolders )
					this.appearanceHolders = new Infragistics.Win.AppearanceHolder[ this.maxAppearanceCount ];

				return this.appearanceHolders;
			}
		}

		#endregion // AppearanceHolders

		#region GetAppearance

		internal Infragistics.Win.AppearanceBase GetAppearance( int index )
		{
			if ( null == this.AppearanceHolders[ index ] )
			{
                
				this.AppearanceHolders[ index ] = new Infragistics.Win.AppearanceHolder();
            
				// hook up the prop change notifications
				//
				this.appearanceHolders[ index ].SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}

			this.AppearanceHolders[ index ].Collection = this.Appearances;

			return this.AppearanceHolders[ index ].Appearance;
		}

		#endregion // GetAppearance

		#region SetAppearance

		internal void SetAppearance( int index, Infragistics.Win.AppearanceBase appearance )
		{
			// SSP 11/28/01 UWG778
			// Moved this if statement from within the second if statement to here.
			//
			// SSP 11/9/01 UWG722
			// if this.AppearanceHolders[ (int)index ] is null, then fill in the slot
			// with a new appearance holder before going ahead with setting appearance 
			// on the appearance holder.
			//
			if ( null == this.AppearanceHolders[ index ] )
			{
				this.AppearanceHolders[ index ] = new Infragistics.Win.AppearanceHolder();
            
				// hook up the prop change notifications
				//
				this.appearanceHolders[ index ].SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}

			if ( appearance != this.AppearanceHolders[ index ].Appearance )
			{
				this.AppearanceHolders[ index ].Collection = this.Appearances;
				this.AppearanceHolders[ index ].Appearance = appearance;

				this.NotifyPropChange( this.appearancePropIds[ index ], null );
			}
		}

		#endregion // SetAppearance

		#region ShouldSerializeAppearance
		
		internal bool ShouldSerializeAppearance( int index ) 
		{
			return this.HasAppearance( index ) && this.GetAppearance( index ).ShouldSerialize( );
		}

		#endregion // ShouldSerializeAppearance

		#region ResetAppearance
		
		internal void ResetAppearance( int index ) 
		{
			// SSP 8/20/02
			// When we are trying to reset the appearance associated with passed in
			// layout appearance index, why are we checking if we have the appearance.
			// (LayoutAppearanceIndex passed in could be Caption, and we are checking
			// here if we have the Appearance).
			//
			//if ( this.HasAppearance )
			if ( this.HasAppearance( index ) )
			{
				if ( null != this.appearanceHolders[ index ] )
				{
					//					// remove the prop change notifications
					//					//
					//					this.appearanceHolders[ (int)index ].Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
                    
					// JJD 11/19/01
					// Call Reset on the holder instead of its Appearance
					//
					this.appearanceHolders[ index ].Reset();	
					// AS - 10/2/01   We only need to reset the appearance data, not unhook.
					//this.appearanceHolders[ (int)index ].Appearance.Reset();
	
					// Notify listeners that the appearance has changed. 
					this.NotifyPropChange( this.appearancePropIds[ index ] );
				}
			}
		}

		#endregion // ResetAppearance
 
		#region HasAppearance

		internal bool HasAppearance( int index )
		{
			if ( null != this.appearanceHolders && 
				null != this.appearanceHolders[ index ] &&
				this.appearanceHolders[ index ].HasAppearance )
				return true;

			return false;
		}

		#endregion // HasAppearance

		#region GetAppearanceIfAllocated

		internal Infragistics.Win.AppearanceBase GetAppearanceIfAllocated( int index )
		{
			if ( null != this.appearanceHolders )
			{
				AppearanceHolder holder = this.appearanceHolders[ index ];
				if ( null != holder && holder.HasAppearance )
					return holder.Appearance;
			}

			return null;
		}

		#endregion // GetAppearanceIfAllocated

		#region ShouldSerializeAppearances

		internal bool ShouldSerializeAppearances( ) 
		{
			for ( int i = 0; i < this.maxAppearanceCount; i++ )
			{
				if ( this.ShouldSerializeAppearance( i ) )
					return true;
			}

			return false;
		}

		#endregion // ShouldSerializeAppearances

		#region ShouldSerializeAppearances

		internal void ResetAppearances( ) 
		{
			for ( int i = 0; i < this.maxAppearanceCount; i++ )
				this.ResetAppearance( i );
		}

		#endregion // ShouldSerializeAppearances

		#region InitializeFrom
		
		internal void InitializeFrom( AppearanceManager source )
		{
			int count = Math.Min( source.maxAppearanceCount, this.maxAppearanceCount );
			for ( int i = 0; i < count; i++ )
			{
				if ( null != source.appearanceHolders && null != source.appearanceHolders[ i ] )
				{
					this.GetAppearance( i );
					this.AppearanceHolders[ i ].InitializeFrom( source.appearanceHolders[ i ] );
				}
				else if ( null != this.appearanceHolders && null != this.appearanceHolders[ i ] )
					this.appearanceHolders[ i ].Reset( );
			}
		}

		#endregion // InitializeFrom

		#region Implementation of ISerializable

        // MRS 12/19/06 - fxCop        
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
		{
			this.SerializeTag( info, "Tag" );

			Utils.SerializeProperty( info, "maxAppearanceCount", this.maxAppearanceCount );

			if ( null != this.appearanceHolders )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( this.appearanceHolders[i] != null &&
						this.appearanceHolders[i].ShouldSerialize() )
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, i.ToString(), this.appearanceHolders[i] );
						//info.AddValue( i.ToString(), this.appearanceHolders[i] );
					}
				}
			}
		}

		#endregion // Implementation of ISerializable

		#region Reset

		/// <summary>
		/// Resets the object to its default state.
		/// </summary>
		public void Reset( )
		{
			this.ResetAppearances( );
		}

		#endregion // Reset

		#region ShouldSerialize

		/// <summary>
		/// Returns true if the object needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerialize( )
		{
			return this.ShouldSerializeAppearances( );
		}

		#endregion // ShouldSerialize

		#region ResolveAppearance

		internal void ResolveAppearance( int index, ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			AppearanceBase app = this.GetAppearanceIfAllocated( index );
			if ( null != app )
				app.MergeData( ref appData, ref flags );
		}

		#endregion // ResolveAppearance

		#region OnDispose

		/// <summary>
		/// Called when this object is Disposed of
		/// </summary>
		protected override void OnDispose( )
		{
			// call reset appearance which will remove us a as sub object
			// prop change notify listener for all appearances 
			//
			// JJD 11/19/01
			// Unhook and call Reset to dispose the holders properly
			//
			//this.ResetAppearances();
			if ( null != this.appearanceHolders )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( null != this.appearanceHolders[ i ] )
					{
						// unhook the prop change notifications
						//
						this.appearanceHolders[ i ].SubObjectPropChanged -= this.SubObjectPropChangeHandler;
						this.appearanceHolders[ i ].Reset();
						this.appearanceHolders[ i ] = null;
					}
				}
			}
         
			base.OnDispose();
		}

		#endregion // OnDispose

		#region OnSubObjectPropChanged

		/// <summary>
		/// Called when another sub object that we are listening to notifies
		/// us that one of its properties has changed.
		/// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange )
		{
            base.OnSubObjectPropChanged(propChange);

			int appearanceIndex = -1;

			for ( int i = 0; i < this.AppearanceHolders.Length; i++ )
			{
				if ( this.HasAppearance( i ) && this.GetAppearance( i ) == propChange.Source )
				{
					appearanceIndex = i;
					break;
				}
			}

			if ( appearanceIndex >= 0 && null != this.appearancePropIds )
			{
				this.NotifyPropChange( this.appearancePropIds[ appearanceIndex ], propChange );
			}
			else
			{
				Debug.Assert( false );
				this.NotifyPropChange( propChange );
			}
		}

		#endregion // OnSubObjectPropChanged
	}

	#endregion // AppearanceManager

	#region ValueEmbeddableEditorOwner Class

	// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
	// 
	internal class ValueEmbeddableEditorOwner : Infragistics.Win.UltraWinEditors.DefaultEditorOwner
	{
		private object dataValue = null;

		public ValueEmbeddableEditorOwner( object dataValue ) : base( )
		{
			this.dataValue = dataValue;
		}

		public override object GetValue( object ownerContext )
		{
			return this.dataValue;
		}
	}

	#endregion // ValueEmbeddableEditorOwner Class

    // MBS 12/21/06 - Conditional Formatting
    #region CellContextProvider

    internal class CellContextProvider : IConditionContextProvider
    {
        // MBS 8/18/08 - BR35632
        //private UltraGridRow baseRow = null;
        //private UltraGridColumn baseColumn = null;
        private WeakReference baseRowRef = null;
        private WeakReference baseColumnRef = null;

        public CellContextProvider(UltraGridRow row, UltraGridColumn column)
        {
            if (row == null || column == null)
                throw new ArgumentNullException();

            // MBS 8/18/08 - BR35632
            //this.baseRow = row;
            //this.baseColumn = column;
            this.baseRowRef = new WeakReference(row);
            this.baseColumnRef = new WeakReference(column);
        }

        public object Context
        {
            get
            {
                // MBS 8/18/08 - BR35632
                //return this.baseRow.Cells[this.baseColumn];
                UltraGridRow row = GridUtils.GetWeakReferenceTarget(this.baseRowRef) as UltraGridRow;
                UltraGridColumn col = GridUtils.GetWeakReferenceTarget(this.baseColumnRef) as UltraGridColumn;
                if (row != null && col != null)
                    return row.Cells[col];

                return null;
            }
        }
    }

    #endregion //CellContextProvider

}
