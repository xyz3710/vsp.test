#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Collections;
	using System.Collections.Generic;



	class OrderedGroupColLevels
	{
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList		levels = null;    
		private List<List<ColumnHeader>> levels;

		private int				levelCount = -1;

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal OrderedGroupColLevels( ArrayList colHeaderList, int levelCount )
		internal OrderedGroupColLevels( List<ColumnHeader> colHeaderList, int levelCount )
		{
			// MD 1/21/09 - Groups in RowLayouts
			// Only a level count of 1 should be used in group layout style.
			Debug.Assert( 
				levelCount == 1 || 
				colHeaderList.Count == 0 ||
				colHeaderList[ 0 ].Band == null ||
				colHeaderList[ 0 ].Band.RowLayoutStyle != RowLayoutStyle.GroupLayout, "Multiple levels cannot be use din GroupLayout style." );

			this.levelCount = levelCount;

			// Call method to recurse COL_LIST and create
			// an entry in the VECTOR for each level.
			this.CreateColLevels( colHeaderList );
		}

		internal void Dispose( )
		{
			if ( null != this.levels )
				this.Levels.Clear( );
		}
		
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList GetLevelColHeaderList( int level )
		internal List<ColumnHeader> GetLevelColHeaderList( int level )
		{
			
			if ( level < 0 || level >= this.Levels.Count )
				return null;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (ArrayList)this.Levels[ level ];
			return this.Levels[ level ];

		}
		
		
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList Levels
		internal List<List<ColumnHeader>> Levels
		{
			get
			{
				if ( null == this.levels )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//this.levels = new ArrayList( );
					this.levels = new List<List<ColumnHeader>>();
				}

				return this.levels;
			}
		}

		internal int LevelCount
		{
			get
			{
				return this.levelCount;
			}
		}

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList GetHeaderList( )
		internal List<HeaderBase> GetHeaderList()
		{
			// CAVEAT: the list that is returned is in the correct
			// order, but the Level properties of the column objects
			// are not set. This method would need to clone the column
			// objects and set their level properties. This is not
			// needed now, but might be in the future.

			// Create the HEADER_LIST of the ColumnHeadrs
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList orderedColsClone = new ArrayList( );
			List<HeaderBase> orderedColsClone = new List<HeaderBase>();

			// Loop through all the levels and return
			// the columns in a flattened list.
			for ( int currentLevel = 0; currentLevel < this.LevelCount; currentLevel++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList levelColHeadrList = this.GetLevelColHeaderList( currentLevel );
				List<ColumnHeader> levelColHeadrList = this.GetLevelColHeaderList( currentLevel );
            
				if ( null != levelColHeadrList )
				{
                
					for ( int i = 0; i < levelColHeadrList.Count; i++ )
					{
						try
						{
							orderedColsClone.Add( levelColHeadrList[i] );
						}
						catch( Exception )
						{
							orderedColsClone.Clear( );							
							orderedColsClone = null;

							return null;
						}
					}
				}
			}
			
			return orderedColsClone;
		}
	

		
		internal bool ClearLevels()
		{
			for ( int i = 0; i < this.Levels.Count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList colHeaderList = this.Levels[i] as ArrayList;
				List<ColumnHeader> colHeaderList = this.Levels[ i ];

				// If there is a COL_LIST at this level
				// then clear the headers on the level
				// and delete it.
				if ( null != colHeaderList )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.ClearLevel( colHeaderList );
					OrderedGroupColLevels.ClearLevel( colHeaderList );

					colHeaderList.Clear( );
					colHeaderList = null;
				}

			}

			this.Levels.Clear( );

			return true;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool ClearLevel( ArrayList columnHeaderList )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static bool ClearLevel( ArrayList columnHeaderList )
		internal static bool ClearLevel( List<ColumnHeader> columnHeaderList )
		{
			if ( null == columnHeaderList )
				return false;
        
	
			// Once we're done then clear the list
			columnHeaderList.Clear( );

			return true;
		}

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal bool CreateColLevels( ArrayList colHeaderList)
		internal bool CreateColLevels( List<ColumnHeader> colHeaderList )
		{
			// Clear any existing
			this.ClearLevels( );

			// Create the new list, starting with level 0.
			this.CreateLevel( colHeaderList, 0, 0);

			return true;
		}

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal bool CreateLevel( ArrayList colHeaderList, int startCol, int lastLevel )
		internal bool CreateLevel( List<ColumnHeader> colHeaderList, int startCol, int lastLevel )
		{
			int currentLevel = lastLevel;

			bool ret = true;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList colHeaderLevels = new ArrayList( );
			List<ColumnHeader> colHeaderLevels = new List<ColumnHeader>();
				

			// If the COlList is empty create one level.
			if ( null != colHeaderList && 0 == colHeaderList.Count )
			{
				
				try
				{
					// Add a new Col List representing the level
					this.Levels.Add( colHeaderLevels );
				}
				catch ( Exception e1 )
				{
					Debug.Assert( false, "OrderedGroupColLevels.CreateLevel Failed:" + e1 );
					ret = false;
				}

				// If we have no more Columns left, make sure
				// that all the Levels have been created
				if ( currentLevel < ( this.LevelCount - 1 ) )
				{
					this.CreateLevel( colHeaderList, 0, ++currentLevel );
					return true;
				}        
			}
			else    
			{
				// Level is not passed in, to account levels that are empty.
				// Get the current level currentLevel = (*StartCol)->GetLevel();


				try
				{
					// Add a new Col List representing the level
					this.Levels.Add( colHeaderLevels );

					int i;
				
					// For each column that belongs on this level
					// add to the list
					for ( i = startCol; i < colHeaderList.Count; i++ )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//ColumnHeader columnHeader = colHeaderList[i] as ColumnHeader;
						ColumnHeader columnHeader = colHeaderList[ i ];

						Debug.Assert( null != columnHeader, "All elements of colHeaderList must be ColumnHeader instances" );

						if ( null == columnHeader )
							continue;													 

						//If column is on a different level
						//call this method recursively to create new level.
						// MD 1/21/09 - Groups in RowLayouts
						// Use LevelResolved because it checks the band's row layout style
						//if ( currentLevel != columnHeader.Column.Level )
						if ( currentLevel != columnHeader.Column.LevelResolved )
						{
							this.CreateLevel( colHeaderList, i, ++currentLevel );
							return true;
						}

						colHeaderLevels.Add( columnHeader );
					}
            
					if ( i >= colHeaderList.Count )
					{
						// If we have no more Columns left, make sure
						// that all the Levels have been created
						if ( currentLevel < ( this.LevelCount - 1 ) )
						{
							this.CreateLevel( colHeaderList, i, ++currentLevel );
							return true;
						}
					}
				}
				catch ( Exception e2 )
				{
					Debug.Assert( false, "OrderedGroupColLevels.CreateLevel Failed: " + e2 );
					ret = false;
				}
			}

			return ret;
		}
	}


	internal class OrderedDropColumns
	{
		bool                    valid;
		GroupHeader             dropGroupHeader	= null;
		DropLocation            dropLocation;

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//ArrayList				dragItems		= null;
		List<HeaderBase> dragItems = null;

		OrderedGroupColLevels   groupColLevels	= null;
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal OrderedDropColumns( DropLocationInfo dropLocationInfo, ArrayList dragItems )
		internal OrderedDropColumns( DropLocationInfo dropLocationInfo, List<HeaderBase> dragItems )
		{
			this.dropLocation     = dropLocationInfo.DropLocation;
			this.dragItems        = dragItems;
			this.dropGroupHeader  = dropLocationInfo.GroupHeader;

			this.groupColLevels = null;

			this.Valid = true;

			this.CreateDropColumnsList( dropLocationInfo );
		}


		internal void Dispose( )
		{
			if ( null != this.groupColLevels )
			{
				this.groupColLevels.Dispose( );
				
				this.groupColLevels = null;
			}
		}

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList GetHeaderList()
		internal List<HeaderBase> GetHeaderList()
		{
			if ( null != this.groupColLevels )
				return groupColLevels.GetHeaderList( );
			else
				return null;
		}

		internal PositionInfo GetPositionInfo( ColumnHeader columnHeader )
		{
			PositionInfo posInfo = new PositionInfo( );

			if ( null == this.groupColLevels )
				return null;

			posInfo.Level = -1;
			posInfo.VisiblePos = -1;
			posInfo.Group = null;

			for ( int currentLevel = 0; currentLevel < this.groupColLevels.LevelCount; currentLevel++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList levelColHeaderList = this.groupColLevels.GetLevelColHeaderList( currentLevel );
				List<ColumnHeader> levelColHeaderList = this.groupColLevels.GetLevelColHeaderList( currentLevel );

				if ( null != levelColHeaderList )
				{
					int visPos = 0;                					

					for ( int i = 0; i < levelColHeaderList.Count; i++ )
					{

						if( columnHeader == levelColHeaderList[i] )
						{
							posInfo.Level = (short)currentLevel;
							posInfo.VisiblePos = (short)visPos;
							posInfo.Group = this.dropGroupHeader.Group;

							return posInfo;                            
						}

						visPos++;

					}
				}
			}

			return null;
		}

		internal bool Valid
		{
			get
			{
				return this.valid;
			}
			set
			{
				this.valid = value;
			}
		}

			
		internal bool CreateDropColumnsList( DropLocationInfo dropLocationInfo )
		{
			GroupHeader dropGroupHeader = dropLocationInfo.GroupHeader;
			
			ColumnHeader dropColumnHeader = dropLocationInfo.ColumnHeader;

			bool ret = false;

			if ( null != this.groupColLevels )
			{
				this.groupColLevels.ClearLevels( );
				this.groupColLevels = null;
			}

			if ( null == this.dragItems || null == dropGroupHeader )
				return false;

			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved column because in GroupLayout style, it will return a different enumarator which returns the columns 
			// actually visible in the group
			
			IEnumerable<UltraGridColumn> groupCols = dropGroupHeader.Group.ColumnsResolved;
			List<ColumnHeader> tmp = new List<ColumnHeader>();

			foreach ( UltraGridColumn column in groupCols )
			{
				tmp.Add( column.Header );
			}
    
			if ( null == groupCols )
				return false;

			// MD 1/21/09 - Groups in RowLayouts
			// Use LevelCountResolved because it checks the band's row layout style
			//this.groupColLevels = new OrderedGroupColLevels( tmp, dropGroupHeader.Band.LevelCount );
			this.groupColLevels = new OrderedGroupColLevels( tmp, dropGroupHeader.Band.LevelCountResolved );

    
			// Remove any of the columns that are being dragged.
			for ( int i = 0; i < this.dragItems.Count; i++ )
			{
				ColumnHeader columnHeader = this.dragItems[i] as ColumnHeader;

				Debug.Assert( null != columnHeader, "All elements of dragItems must be ColumnHeader instances" );
        
				if ( null != columnHeader )
				{
					this.Remove( columnHeader );
				}        
			}


			// Find the DropColumn's new Visible Position
			// After any columns that are being moved
			// have been removed
			PositionInfo dropPosInfo = new PositionInfo();
    
			if ( null != dropColumnHeader )
			{
				dropPosInfo = this.GetPositionInfo( dropColumnHeader );
				
				ret = null != dropPosInfo;
			}
			else if ( null != dropGroupHeader )
			{
				dropPosInfo.Group = dropGroupHeader.Group;
				dropPosInfo.VisiblePos = 0;

				if ( dropLocationInfo.IsDropTargetGroup )
				{        
					dropPosInfo.Level = 0;
				}
				else if ( dropLocationInfo.IsDropTargetEmptyLevel )
				{
					dropPosInfo.Level = (short)dropLocationInfo.Level;
				}

				ret = true;
			}
			else
			{
				ret = false;
			}

			// If we are unable to find the DropColumn's
			// new VisiblePos, it was one of the columns
			// being dragged, so bail
			if ( !ret )
			{
				this.Valid = false;
				return false;
			}

			// If the Drop is after this column, then
			// increment the visible position.
			if ( DropLocation.After == this.dropLocation )
				dropPosInfo.VisiblePos++;

			int itemInsertIterator;

			// Now add the columns back to the collection
			// with the new VisiblePosition
			for ( itemInsertIterator = this.dragItems.Count - 1;
				  itemInsertIterator >= 0; itemInsertIterator-- )
			{
				ColumnHeader columnHeader = this.dragItems[ itemInsertIterator ] as ColumnHeader;

				Debug.Assert( null != columnHeader, "All elements of dragItems must be ColumnHeader instances" );
        
				if ( null != columnHeader )
				{
					this.Insert( columnHeader,  dropPosInfo );
				}
			}

			return ret;

		}


		internal bool Insert( ColumnHeader columnHeader, PositionInfo newPosInfo )
		{
			if ( null == this.groupColLevels )
				return false;

			bool ret = false;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList colLevelList = this.groupColLevels.GetLevelColHeaderList( newPosInfo.Level );
			List<ColumnHeader> colLevelList = this.groupColLevels.GetLevelColHeaderList( newPosInfo.Level );

			//
			// pColLevelList might be 0xcdcdcdcd, so I wrapped this in a try.
			//
			try
			{
				if ( null != colLevelList )
				{
					// insert the item into the collection at the correct spot
					//
					// SSP 10/11/01 UWG470
					// If the newPosInfo.VisiblePos is out of range, then
					// it will throw an exception. So get arround it.
					//
					//colLevelList.Insert( newPosInfo.VisiblePos, columnHeader );
					colLevelList.Insert( 
						Math.Min( newPosInfo.VisiblePos, colLevelList.Count ), 
						columnHeader );

					ret = true;
				}
			}
			catch( Exception e1 )
			{
				Debug.Assert( false, e1.ToString( ) );
				return false;
			}

			return ret;
		}

		internal bool Remove( Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader )
		{
			if ( null == this.groupColLevels )
				return false;

			// If the Column to remove is from a different
			// Group, then we don't have to worry about
			// removing him from the drop group.
			// SSP 8/12/05 BR05569
			// 
			//if ( columnHeader.Group.Header != this.dropGroupHeader )
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( null == columnHeader.Group || columnHeader.Group.Header != this.dropGroupHeader )
			UltraGridGroup columnHeaderGroup = columnHeader.Group;

			if ( null == columnHeaderGroup || columnHeaderGroup.Header != this.dropGroupHeader )
				return true;

			bool ret = false;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList levelColHeaderList = this.groupColLevels.GetLevelColHeaderList( columnHeader.Column.Level );
			// MD 1/21/09 - Groups in RowLayouts
			// Use LevelResolved because it checks the band's row layout style
			//List<ColumnHeader> levelColHeaderList = this.groupColLevels.GetLevelColHeaderList( columnHeader.Column.Level );
			List<ColumnHeader> levelColHeaderList = this.groupColLevels.GetLevelColHeaderList( columnHeader.Column.LevelResolved );

			if ( null != levelColHeaderList )
			{
				try
				{
					// Determine if the column is part of the group.
					// If so, remove it.					

					// If the ColumnHeader was found in the list, then remove it
					if ( levelColHeaderList.IndexOf( columnHeader ) >= 0 )					
						levelColHeaderList.Remove( columnHeader );
					
					ret = true;
				}
				catch( Exception e )
				{
					Debug.Assert( false, "OrderedDropColumns.Remove Failed: " + e );
					ret = false;
				}
			}

			return ret;
		}
	}

}
