#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Win;
using System.Diagnostics;
using System.Drawing.Imaging;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Summary description for CardCaptionUIElement.
	/// </summary>
	public class CardCaptionUIElement : UIElement
	{
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public CardCaptionUIElement(UIElement parent) : base(parent)
		{
		}

		#endregion Constructor

		#region Base Class Overrides

			#region InitAppearance 

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
											   ref AppearancePropFlags	requestedProps)
		{
			UltraGridRow row = this.Row;
			UltraGridBand band = row.BandInternal;

			// SSP 7/15/02
			// Compressed card view.
			// If the themes are turned on and we are in compressed card view mode,
			// then use the theme colors for the foreground.
			//
			// --------------------------------------------------------------------
			if ( 0 != ( AppearancePropFlags.ForeColor & requestedProps ) &&
				null != row && row.IsCardStyleCompressed && row.Layout.ThemesBeingUsed )
			{
				bool isActive = false;
				Point mouseLocation = Control.MousePosition;
				UltraGridBase grid = row.Layout.Grid;

				if ( null != grid )
				{
					mouseLocation = grid.PointToClient( mouseLocation );

					if ( this.Rect.Contains( mouseLocation ) )
						isActive = true;
				}

				if ( ! row.Selected )
				{
					if ( !isActive )
						appearance.ForeColor = XPThemes.Explorer.GroupHeaderColor;
					else
						appearance.ForeColor = XPThemes.Explorer.ActiveGroupHeaderColor;
				}
				else
				{
					if ( !isActive )
						appearance.ForeColor = XPThemes.Explorer.SpecialGroupHeaderColor;
					else
						appearance.ForeColor = XPThemes.Explorer.ActiveSpecialGroupHeaderColor;
				}

				requestedProps &= ~AppearancePropFlags.ForeColor;
			}
			// --------------------------------------------------------------------

			// Resolve the appearance.
			ResolveAppearanceContext context = new ResolveAppearanceContext(this.GetType(), requestedProps);

			// SSP 3/13/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( band, StyleUtils.Role.CardCaption, out context.ResolutionOrder );

			// JJD 1/10/02
			// Set the IsCard flag on the context
			//
			context.IsCard = true;

			// JJD 1/10/02
			// If this is the active row apply the active card caption settings
			//
			if ( row.IsActiveRow )
			{
				context.IsActiveRow = true;
				band.ResolveAppearance(ref appearance, ref context);
				context.IsActiveRow = false;
			}

			// JJD 1/10/02
			// If this is a selected row apply the selected card caption settings
			//
			if ( row.Selected )
			{
				context.IsSelected = true;
				band.ResolveAppearance(ref appearance, ref context);
				context.IsSelected = false;
			}

			band.ResolveAppearance(ref appearance, ref context);
		}

			#endregion InitAppearance 

			#region OnDoubleClick

		// SSP 7/1/02
		// Overrode OnDoubleClick to expand and collapse the card when the card caption
		// is double clicked.
		//
        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{
			if ( !adjustableArea && null != this.Row && this.Row.IsCardStyleCompressed )
			{
				// SSP 7/30/03 UWG2430
				// This method gets called when the card caption is double clicked and also when
				// the card expansion indicator is clicked. We should only select when the card
				// caption is double clicked and not select when the expansion indicator is
				// clicked.
				// Pass in true for selectCard parameter.
				//
                //this.Row.ToggleCardCompressedState( );
				this.Row.ToggleCardCompressedState( true );
				return;
			}

            base.OnDoubleClick( adjustableArea );
		}

			#endregion // OnDoubleClick

			#region OnMouseEnter

		// SSP 7/16/02 
		// Compressed card view.
		// Overrode OnMouseEnter and OnMouseLeave methods.
		//
		/// <summary>
		/// Called when the mouse enters this element
		/// </summary>
		protected override void OnMouseEnter( )
		{
			// We want to invalidate the caption ui element so that it get's redrawn
			// when the mouse is entered in the case of the compressed card view and 
			// when the themes are enabled.
			//
			if ( this.Row.IsCardStyleCompressed && this.Row.Layout.ThemesBeingUsed )
				this.DirtyChildElements( );
		}

			#endregion // OnMouseEnter

			#region OnMouseLeave
       
		/// <summary>
		/// Called when the mouse leaves this element
		/// </summary>
		protected override void OnMouseLeave( )
		{
			// We want to invalidate the caption ui element so that it get's redrawn
			// when the mouse is entered in the case of the compressed card view and 
			// when the themes are enabled.
			//
			if ( this.Row.IsCardStyleCompressed && this.Row.Layout.ThemesBeingUsed )
				this.DirtyChildElements( );
		}

			#endregion // OnMouseLeave
	
			#region DrawTheme

		// SSP 7/15/02
		// Compressed card view.
		// Overrode DrawTheme to draw themed group header if the card style is compressed
		// so that it meshes with the card expansion button which is also a themed element.
		//
		/// <summary>
		/// Used by an element to render using the system theme. This method will not
		/// be invoked if the themes are not supported.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		/// <returns>Returning true will prevent the invocation of the DrawBackColor,
		/// DrawImageBackground, DrawBorders, DrawImage and DrawForeground. Return
		/// false when themes are not supported by the element or if unable to 
		/// render using the system theme.</returns>
		protected override bool DrawTheme( ref UIElementDrawParams drawParams )
		{
			// Do the themed drawing only if the card view style is compressed.
			//
			if ( this.Row.IsCardStyleCompressed )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//bool isCardCompressed = this.Row.IsCardCompressed;

				bool retVal = XPThemes.Explorer.DrawBackground(
					null != this.Row && this.Row.Selected ?
					ThemedExplorer.BackgroundType.SpecialGroupHeader :
					ThemedExplorer.BackgroundType.NormalGroupHeader,
					ref drawParams,
					this.Rect,
					this.ClipRect );

				return retVal;
			}

			return false;
		}

			#endregion // DrawTheme

			#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Row.Band, StyleUtils.Role.CardCaption );
			}
		}

			#endregion // UIRole

		#endregion Base Class Overrides

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			Rectangle textRect = this.RectInsideBorders;

			// Save the current child elements collection so we can
			// potentially reuse some of the elements.
			UIElementsCollection oldElements = this.ChildElements;

			// Clear the current child elements collection.
			this.childElementsCollection = null;            

			// SSP 6/27/02
			// If the card style is compressed, then add the expand/collapse button
			//
            // MRS NAS v8.2 - CardView Printing
            //if ( this.Row.IsCardStyleCompressed )
            if ( this.ShouldShowExpansionIndicator )
			{
				CardExpansionUIElement expansionElement = (CardExpansionUIElement)CardCaptionUIElement.ExtractExistingElement( oldElements, typeof( CardExpansionUIElement ), true );
 
				if ( null == expansionElement )
					expansionElement = new CardExpansionUIElement( this );

				Rectangle rect = textRect;

				Size idealSize = this.Row.Band.GetIdealCompressedCardViewButtonSize( );

				rect.Width = idealSize.Width;
				rect.X = textRect.Right - rect.Width;

				// Adjust the text rect to account for the expansion indicator.
				//
				textRect.Width -= rect.Width;

				// Also center it on the card caption if card caption height is bigger than
				// the button height.
				//
				if ( rect.Height > idealSize.Height )
				{
					rect.Y  += ( rect.Height - idealSize.Height ) / 2;
					rect.Height = idealSize.Height;
				}

				expansionElement.Rect = rect;

				this.ChildElements.Add( expansionElement );
			}

			// Display an image if necessary.
			//
			// Get our resolved appearance.
			AppearanceData		appearanceData = new AppearanceData();
			AppearancePropFlags requestedProps = AppearancePropFlags.Image | AppearancePropFlags.ImageHAlign | AppearancePropFlags.ImageVAlign;
			this.InitAppearance(ref appearanceData, ref requestedProps);

			// If we have an image, display it.
			if (appearanceData.Image != null)
			{
				// SSP 8/6/03
				// In row layout designer, this.Control is not an UltraGridBase. It's the row 
				// layout designer control.
				//
				//Image image = appearanceData.GetImage(((UltraGridBase)this.Control).ImageList);
				Image image = appearanceData.GetImage( this.Row.Layout.Grid.ImageList );

				if (image != null)
				{
					ImageUIElement imageElement = (ImageUIElement)CardLabelUIElement.ExtractExistingElement(oldElements, typeof(ImageUIElement), true);
					if (null == imageElement)
						imageElement = new ImageUIElement( this, image );
					else
						imageElement.Image = image;

					imageElement.Scaled = true;

					// Shrink the image size if necessary
					Size size = image.Size;
					if (this.RectInsideBorders.Height > 2 && size.Height > (this.RectInsideBorders.Height - 2))
					{
						// Proportinally shrink the width
						size.Width  = (int)((double)size.Width * ((double)(this.RectInsideBorders.Height - 2) / (double)size.Height));

						// Shrink the height
						size.Height = this.RectInsideBorders.Height - 2;
					}

					// Make sure the width isn't too big
					if (this.RectInsideBorders.Width > 2 && size.Width > (this.RectInsideBorders.Width - 2))
						size.Width = this.RectInsideBorders.Width - 2;

					Rectangle imgRect	= this.RectInsideBorders;
					imgRect.Width		= size.Width;
					imgRect.Height		= size.Height;

					// Move the image based on alignment
					DrawUtility.AdjustHAlign(appearanceData.ImageHAlign, ref imgRect, this.RectInsideBorders); 
					DrawUtility.AdjustVAlign(appearanceData.ImageVAlign, ref imgRect, this.RectInsideBorders);

					// Make sure the imgae has at least a 1 pixel padding on top and bottom.
					if (imgRect.Top == this.RectInsideBorders.Top)
						imgRect.Y++;
					else if (imgRect.Bottom == this.RectInsideBorders.Bottom)
						imgRect.Y--;

					imageElement.Rect = imgRect;

					this.ChildElements.Add( imageElement );

					// Adjust the text rect appropriately
					switch (appearanceData.ImageHAlign)
					{
						case HAlign.Center:
							break;

						case HAlign.Right:
							imgRect.X--;
							textRect.Width -= imgRect.Width + 1;
							break;

						default:
						case HAlign.Left:
							imgRect.X++;
							textRect.Width	-= imgRect.Width + 1;
							textRect.X		+= imgRect.Width + 1;
							break;
					}
				}
			}

			// Create a text UIElement
			//
			// Try to reuse one from the old list.
			TextUIElement captionTextElement = (TextUIElement)CardCaptionUIElement.ExtractExistingElement(oldElements, typeof(TextUIElement), true);

			// If we didn't find one in the old list then create a new one.
			if (captionTextElement == null)
				captionTextElement				= new TextUIElement(this, this.Row.CardCaption);
			else
			{
				captionTextElement.TextHAlign	= HAlign.Default;
				captionTextElement.TextVAlign	= VAlign.Default;
				captionTextElement.Text			= this.Row.CardCaption;
			}

			// SSP 10/7/03 UWG2696
			// Set the multiline to true to allow the text to have multiline string.
			//
			// ------------------------------------------------------------------------------------
			captionTextElement.MultiLine = this.Row.Band.HasCardSettings 
				&& this.Row.Band.CardSettings.CaptionLines > 1;
			// ------------------------------------------------------------------------------------

			captionTextElement.Rect	= textRect;

			// Add the new element.
			this.ChildElements.Add(captionTextElement);
		}

		#endregion PositionChildElements

		#region Internal Properties

			#region Row

		internal UltraGridRow Row
		{
			get
			{
				RowUIElementBase rowElement =  this.GetAncestor(typeof(RowUIElementBase)) as RowUIElementBase;

				if (rowElement != null)
					return rowElement.Row;
				else
					return null;
			}
		}

			#endregion Row

        // MRS NAS v8.2 - CardView Printing
        #region ShouldShowExpansionIndicator

        private bool ShouldShowExpansionIndicator
        {
            get
            {
                UltraGridRow row = this.Row;
                if (row != null && this.Row.IsCardStyleCompressed == false)
                    return false;
                
                UltraGridLayout layout = this.Row.Layout;
                if (layout.IsPrintLayout || layout.IsExportLayout)
                    return false;

                return true;
            }
        }
        #endregion //ShouldShowExpansionIndicator

		#endregion Internal Properties

	}

	// SSP 6/27/02
	// Compressed card view.
	// Added CardExpansionUIElement element.
	//
	/// <summary>
	/// UI element used as a button in compressed card view to allow the user to expand and collapse individual cards.
	/// </summary>
	public class CardExpansionUIElement : ImageUIElementBase
	{	
		#region Variables

		private MouseButtons lastMouseDownButton = MouseButtons.None;

		#endregion // Variables 

		#region Constructors
	
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>CardExpansionUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		//internal CardExpansionUIElement( UIElement parent ) : base( parent )
		public CardExpansionUIElement( UIElement parent ) : base( parent )
		{
			// Set the transparent color for the image.
			//
			// JJD 8/12/02 - WTB279
			// Removed colorkey support since GDI+ throws an error trying to render
			// icons if the color key is set.
//			this.ColorKey = Color.FromArgb( 192, 192, 192 );
			this.CenterImage = true;
		}

		#endregion // Constructors

		#region Private/Internal Properties

		#region Row

		internal UltraGridRow Row
		{
			get
			{
				return (UltraGridRow)this.GetContext( typeof( UltraGridRow ), true );
			}
		}

		#endregion // Row

		#region IsMouseOver

		private bool IsMouseOver
		{
			get
			{
				Point mousePos = Control.MousePosition;

				System.Windows.Forms.Control control = this.Control;
 
				return null != control && this.Rect.Contains( control.PointToClient( mousePos ) );
			}
		}

		#endregion // IsMouseOver

		#endregion // Private/Internal Properties

		#region Private/Internal Methods

		#endregion // Private/Internal Methods

		#region Protected Overridden Methods

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData,
			ref AppearancePropFlags	requestedProps )
		{
			// SSP 7/15/02
			// If the cursor is over the button, and xp-themes is on, then
			// set the cursor to hand to conform to XP behaviour with
			// groups.
			//
			if ( 0 != ( AppearancePropFlags.Cursor & requestedProps ) )
			{
				bool themesSupported = null != this.Row && this.Row.Layout.ThemesBeingUsed;

				if ( themesSupported && this.IsMouseOver )
				{
					appData.Cursor = Cursors.Hand;					
					requestedProps &= ~AppearancePropFlags.Cursor;
				}				
			}

			base.InitAppearance( ref appData, ref requestedProps );
		}

		#endregion // InitAppearance

		#region GetImageAttributes

		/// <summary>
		/// Gets the image attributes used for drawing the image
		/// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		/// <remarks>This method can be overridden in dervied classes to supply the image attributes.</remarks>
		protected override ImageAttributes GetImageAttributes( ref UIElementDrawParams drawParams ) 
		{
			ImageAttributes imageAttributes = new ImageAttributes( );

			imageAttributes.ClearColorKey( );

			// JJD 8/12/02 - WTB279
			// Removed colorkey support since GDI+ throws an error trying to render
			// icons if the color key is set.
			//imageAttributes.SetColorKey( this.ColorKey, this.ColorKey );
			ColorMap transparentColorMap = new ColorMap( );
			transparentColorMap.OldColor = Color.FromArgb( 0, 0, 0 );
			transparentColorMap.NewColor = Color.Transparent;

			ColorMap colorMap = new ColorMap( );
			colorMap.OldColor = Color.FromArgb( 192, 192, 192 );
			colorMap.NewColor = drawParams.AppearanceData.ForeColor;

			ColorMap[] colorMaps = new ColorMap[] { transparentColorMap, colorMap };

			imageAttributes.SetRemapTable( colorMaps );

			return imageAttributes; 
		}

		#endregion // GetImageAttributes

		#region OnMouseDown

		/// <summary>
		/// Called when the mouse down message is received over the element. 
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		/// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
		/// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
		/// <returns>If true then bypass default processing</returns>
		protected override bool OnMouseDown( MouseEventArgs e, bool adjustableArea,
			ref UIElement captureMouseForElement )
		{
			// If the left mouse button is clicked, then return true so the
			// selection strategy bypasses the selection logic.
			//
			if ( !adjustableArea && MouseButtons.Left == e.Button )
			{
				// Set the mouse button that the element was clicked with so 
				// by OnClick knows which button was clicked
				//
				this.lastMouseDownButton = e.Button;

				return true;
			}

			return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );
		}

		#endregion // OnMouseDown

		#region OnMouseUp

		/// <summary>
		/// Called when the mouse up message is received over the element. 
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		protected override bool OnMouseUp( MouseEventArgs e )
		{
			if ( MouseButtons.Left == this.lastMouseDownButton )
			{
				this.lastMouseDownButton = MouseButtons.None;

				UltraGridRow row = this.Row;

				// Toggle the expanded state of the card.
				//
				if ( null != row )
				{
					// SSP 7/30/03 UWG2430
					// This method gets called when the card caption is double clicked and also when
					// the card expansion indicator is clicked. We should only select when the card
					// caption is double clicked and not select when the expansion indicator is
					// clicked.
					// Pass in false for selectCard parameter.
					//
					//row.ToggleCardCompressedState( );
					row.ToggleCardCompressedState( false );

					return true;
				}
			}

			return base.OnMouseUp( e );
		}

		#endregion // OnMouseUp

		#region DrawTheme

		/// <summary>
		/// Used by an element to render using the system theme. This method will not
		/// be invoked if the themes are not supported.
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		/// <returns>Returning true will prevent the invocation of the DrawBackColor,
		/// DrawImageBackground, DrawBorders, DrawImage and DrawForeground. Return
		/// false when themes are not supported by the element or if unable to 
		/// render using the system theme.</returns>
		protected override bool DrawTheme( ref UIElementDrawParams drawParams )
		{
			bool isCardCompressed = this.Row.IsCardCompressed;
			bool isRowSelected = this.Row.Selected;

			bool retVal = XPThemes.Explorer.DrawButton(
				// SSP 7/15/02
				// If the row is selected, then use special group header colors,
				// otherwise use the normal group header colors.
				//
				!isRowSelected ?
				( isCardCompressed 
				  ? Infragistics.Win.ThemedExplorer.ExplorerButtonType.NormalGroupExpand
				  : Infragistics.Win.ThemedExplorer.ExplorerButtonType.NormalGroupCollapse 
				)
				:
				( isCardCompressed 
				  ? Infragistics.Win.ThemedExplorer.ExplorerButtonType.SpecialGroupExpand
				  : Infragistics.Win.ThemedExplorer.ExplorerButtonType.SpecialGroupCollapse
				),
				// winXP collapse and expand buttons do not get depressed when the
				// mouse is down. So we are going to do the same here.
				//
				this.IsMouseOver 
				? Infragistics.Win.ThemedExplorer.ExplorerButtonState.Hot 
				: Infragistics.Win.ThemedExplorer.ExplorerButtonState.Normal, 
				ref drawParams,
				this.Rect,
				this.ClipRect );

			// Return true if success otherwise false to allow the default drawing
			// to take place.
			//
			return retVal;
		}

		#endregion // DrawTheme

		#region OnMouseEnter

		/// <summary>
		/// Called when the mouse enters this element
		/// </summary>
		protected override void OnMouseEnter( )
		{
			this.DirtyChildElements( );
		}

		#endregion // OnMouseEnter

		#region OnMouseLeave
       
		/// <summary>
		/// Called when the mouse leaves this element
		/// </summary>
		protected override void OnMouseLeave( )
		{
			this.DirtyChildElements( );
		}

		#endregion // OnMouseLeave

		#region OnDoubleClick

		// SSP 7/30/03 UWG2430 UWG2431
		// Overrode OnDoubleClick method.
		//
        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{
			// Do not call the base class because we don't want it to propagate to the parent
			// card caption element.
			//
		}

		#endregion // OnDoubleClick

		#endregion // Protected Overridden Methods

		#region Public Overridden Properties

		#region Image

		/// <summary>
		/// Overridden. Returns an image.
		/// </summary>
		public override Image Image
		{
			get
			{
				if ( this.Row.IsCardCompressed )
					return this.Row.Layout.CardExpansionImage;
				else 
					return this.Row.Layout.CardExpansionImageInverse;
			}
			set
			{
				throw new NotSupportedException( Shared.SR.GetString("LE_V2_NotSupportedException_305") );
			}
		}

		#endregion // Image

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				if ( this.IsMouseOver )				
					return UIElementBorderStyle.RaisedSoft;
				else
					return UIElementBorderStyle.None;
			}
		}

		#endregion // BorderStyle

		#endregion // Protected Overridden Properties
	}
}
