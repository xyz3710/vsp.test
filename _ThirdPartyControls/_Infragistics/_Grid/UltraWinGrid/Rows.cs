#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Collections.Generic;

	#region ValuePropertyDescriptor Class

	internal class ValuePropertyDescriptor : ValuePropertyDescriptorBase
	{
		private UltraGridBand band = null;

		/// <summary>
		/// Contructor
		/// </summary>
		/// <param name="type">Element type in the list objects.</param>
		/// <param name="band"></param>
		/// <param name="name"></param>
		internal ValuePropertyDescriptor( 
			Type type, 
			UltraGridBand band,
			string name
			) : base( type, name )
		{
			this.band = band;
		}

		/// <summary>
		/// return this.band.IsReadOnly
		/// </summary>
		public override bool IsReadOnly 
		{
			get 
			{
				// SSP 4/23/04 UWG3010
				// Changed the name of IsReadOnly off UltraGridBand to IsBoundListReadOnly. When the 
				
				// underlying bound list is read-only we should still allow editing unbound columns.
				//
				//return this.band.IsReadOnly;
				return this.band.IsBoundListReadOnly;
			}
		}

		/// <summary>
		/// Component is an instance of UltraGridRow for which to set
		/// the value to the bound list.
		/// </summary>
		/// <param name="rowObject"></param>
		/// <param name="value"></param>
		public override void SetValue( object rowObject, object value ) 
		{
			// We are passing in a row object because we need to know which place in the bound
			// list to put the new value in. Typically it should be the actual object whose property
			// is being set however since the whole object is being set in this case we need to
			// pass in something that identifies the slot in the bound list where to put the
			// new object. For example if we are bound to an array of DateTime objects then the
			// value would be an instance of DateTime and the first parameter really needs to be
			// something that identifies the slot in the bound list. In the case of the UltraGrid
			// we will use the rowObject.
			//

			UltraGridRow row = (UltraGridRow)rowObject;
	
			// SSP 9/12/03
			// Use the list associated with the rows collection rather than the band's
			// List property.
			//
			//this.band.List[ row.ListIndex ] = value;
			row.ParentCollection.Data_List[ row.ListIndex ] = value;

			// Clear the cached listObject so it regets it next time
			//
			// SSP 5/8/03 UWG2249
			// Instead of clearing the list object, decrement the verified version number. Either of
			// these will cause the row to reget the list object when the ListObject property is
			// accessed.
			// 
			//row.InternalClearCachedListObject( );
			row.InternalDecrementVerifiedVersion( );
		}
	}

	// ZS 3/3/04 - I moved part of this code to Infragistics.Win.ValuePropertyDescriptorBase as it is common code 
	// for Grid, TabStripControl and CalendarInfo data binding.
//	internal class ValuePropertyDescriptor : PropertyDescriptor 
//	{
//		private Type type = null;
//		private UltraGridBand band = null;
//
//
//		/// <summary>
//		/// Contructor
//		/// </summary>
//		/// <param name="type">Element type in the list objects.</param>
//		/// <param name="band"></param>
//		/// <param name="name"></param>
//		internal ValuePropertyDescriptor( 
//			Type type, 
//			UltraGridBand band,
//			string name
//			) : base( name, null )
//		{
//			this.type = type;
//			this.band = band;
//		}
//
//
//		/*
//		/// <summary>
//		/// Gets category
//		/// </summary>
//		public override string Category 
//		{ 
//			get 
//			{
//				return typeof(UltraGridBand).Name;
//			}
//		}*/
//  
//		
//		/// <summary>
//		/// Return false
//		/// </summary>
//		public override bool IsReadOnly 
//		{
//			get 
//			{
//				return this.band.IsReadOnly;
//			}
//		}
//
//
//		/// <summary>
//		/// Gets property type
//		/// </summary>
//		public override Type PropertyType 
//		{
//			get 
//			{
//				return this.type;
//			}
//		}
//
//		/// <summary>
//		/// Gets component type
//		/// </summary>
//		public override Type ComponentType 
//		{
//			get 
//			{
//				return this.type;
//			}
//		}
//
//
//
//		/// <summary>
//		/// Returns false
//		/// </summary>
//		/// <param name="component"></param>
//		/// <returns></returns>
//		public override bool CanResetValue( object component ) 
//		{
//			return false;
//		}
//
//		/// <summary>
//		/// Returns the passed in object.
//		/// </summary>
//		/// <param name="component"></param>
//		/// <returns></returns>
//		public override object GetValue( object component ) 
//		{
//			// Passed in component would be the value in the bound list.
//			// For example if we were bound to an array of DateTime objects then
//			// the component passed in would be an instance of DateTime.
//			//
//			return component;
//		}
//
//		/// <summary>
//		/// Does nothing
//		/// </summary>
//		/// <param name="component"></param>
//		public override void ResetValue( object component ) 
//		{
//			// DO nothing since CanReset returns false
//			//
//		}
//
//		/// <summary>
//		/// Component is an instance of UltraGridRow for which to set
//		/// the value to the bound list.
//		/// </summary>
//		/// <param name="rowObject"></param>
//		/// <param name="value"></param>
//		public override void SetValue( object rowObject, object value ) 
//		{
//			// We are passing in a row object because we need to know which place in the bound
//			// list to put the new value in. Typically it should be the actual object whose property
//			// is being set however since the whole object is being set in this case we need to
//			// pass in something that identifies the slot in the bound list where to put the
//			// new object. For example if we are bound to an array of DateTime objects then the
//			// value would be an instance of DateTime and the first parameter really needs to be
//			// something that identifies the slot in the bound list. In the case of the UltraGrid
//			// we will use the rowObject.
//			//
//
//			UltraGridRow row = (UltraGridRow)rowObject;
//	
//			// SSP 9/12/03
//			// Use the list associated with the rows collection rather than the band's
//			// List property.
//			//
//			//this.band.List[ row.ListIndex ] = value;
//			row.ParentCollection.Data_List[ row.ListIndex ] = value;
//
//			// Clear the cached listObject so it regets it next time
//			//
//			// SSP 5/8/03 UWG2249
//			// Instead of clearing the list object, decrement the verified version number. Either of
//			// these will cause the row to reget the list object when the ListObject property is
//			// accessed.
//			// 
//			//row.InternalClearCachedListObject( );
//			row.InternalDecrementVerifiedVersion( );
//		}
//
//		/// <summary>
//		/// Returns true
//		/// </summary>
//		/// <param name="component"></param>
//		/// <returns></returns>
//		public override bool ShouldSerializeValue( object component ) 
//		{
//			return true;
//		}
//	}

	#endregion // ValuePropertyDescriptor Class

	#region ScrollCountSparseArray 

	internal class ScrollCountManagerSparseArray : SparseArray, ICreateItemCallback
	{
		#region Private Variables

		private int verifiedScrollCountVersion = -1;
		private RowsCollection ownerRowsCollection = null;

		#endregion // Private Variables

		#region Constructor
		
		internal ScrollCountManagerSparseArray( RowsCollection ownerRowsCollection )
			: base( true, 40, 0.2f )			
		{
			if ( null == ownerRowsCollection )
				throw new ArgumentNullException( "ownerRowsCollection" );

			this.ownerRowsCollection = ownerRowsCollection;
			if ( null != this.ownerRowsCollection.Layout )
				this.verifiedScrollCountVersion = this.ownerRowsCollection.Layout.ScrollableRowCountVersion;
		}

		#endregion // Constructor

		#region DirtyScrollCount

		internal void DirtyScrollCount( )
		{
			this.verifiedScrollCountVersion--;
			this.ownerRowsCollection.DirtyParentRowScrollCount( );
		}

		#endregion // DirtyScrollCount

		#region VerifyAgainstScrollVersion

		private void VerifyAgainstScrollVersion( )
		{
			// SSP 9/21/06 BR16000 - Optmization
			
			
			// This is to make sure that we don't notify calc-manager only once of Resync of rows
			// in NotifyCalcManager_RowsCollectionResynced.
			// 
			// ------------------------------------------------------------------------------------------------
			
			RowsCollectionReference calcReference = null;
			int resyncEventCounter = 0;
			// Only bother with this logic the first time.
			// 
			if ( this.verifiedScrollCountVersion < 0 )
			{
				calcReference = this.ownerRowsCollection.CalcReference;
				if ( null != calcReference )
				{
					calcReference.resyncEventSuspended++;
					resyncEventCounter = calcReference.resyncEventCounter;
				}
			}

			try
			{
				this.ownerRowsCollection.EnsureFiltersEvaluated( );

				this.ownerRowsCollection.EnsureNotDirty( );
				UltraGridLayout layout = this.ownerRowsCollection.Layout;
				if ( null != layout && layout.ScrollableRowCountVersion != this.verifiedScrollCountVersion )
				{
					this.DirtyScrollCountInfo( );
					this.verifiedScrollCountVersion = layout.ScrollableRowCountVersion;
				}
			}
			finally
			{
				if ( null != calcReference )
				{
					calcReference.resyncEventSuspended--;
					if ( resyncEventCounter > 0 && resyncEventCounter < calcReference.resyncEventCounter )
						this.ownerRowsCollection.NotifyCalcManager_RowsCollectionResynced( );
				}
			}
			// ------------------------------------------------------------------------------------------------
		}

		#endregion // VerifyAgainstScrollVersion

		#region GetOwnerData

		protected override object GetOwnerData( object item )
		{
			return ((UltraGridRow)item).sortedRows_SparseArrayOwnerData;
		}

		#endregion // GetOwnerData

		#region SetOwnerData

		protected override void SetOwnerData( object item, object ownerData )
		{
			((UltraGridRow)item).sortedRows_SparseArrayOwnerData = ownerData;
		}

		#endregion // SetOwnerData

		#region VisibleCount

		internal int VisibleCount
		{
			get
			{
				this.VerifyAgainstScrollVersion( );
				return this.GetVisibleCount( );
			}
		}

		#endregion // VisibleCount

		#region ScrollCount

		internal int ScrollCount
		{
			get
			{
				this.VerifyAgainstScrollVersion( );
				return this.GetScrollCount( );
			}
		}

		#endregion // ScrollCount

		#region GetItem

		internal UltraGridRow GetItem( int index, bool create )
		{
			return (UltraGridRow)base.GetItem( index, create ? this : null );
		}

		#endregion // GetItem

		#region VisibleIndexOf

		internal int VisibleIndexOf( ISparseArrayMultiItem immediateChild )
		{
			this.VerifyAgainstScrollVersion( );
			return base.GetVisibleIndexOf( immediateChild );
		}

		#endregion // VisibleIndexOf

		#region ScrollIndexOf

		internal int ScrollIndexOf( ISparseArrayMultiItem immediateChild )
		{
			this.VerifyAgainstScrollVersion( );
			return base.GetScrollIndexOf( immediateChild );
		}

		#endregion // ScrollIndexOf

		#region GetItemAtVisibleIndex

		internal UltraGridRow GetItemAtVisibleIndex( int visibleIndex )
		{
			this.VerifyAgainstScrollVersion( );
			return (UltraGridRow)this.GetItemAtVisibleIndex( visibleIndex, this );
		}

		#endregion // GetItemAtVisibleIndex

		#region GetItemAtVisibleIndexOffset

		internal UltraGridRow GetItemAtVisibleIndexOffset( UltraGridRow startRow, int offset )
		{
			this.VerifyAgainstScrollVersion( );
			return (UltraGridRow)base.GetItemAtVisibleIndexOffset( startRow, offset, this );
		}

		#endregion // GetItemAtVisibleIndexOffset

		#region GetItemAtScrollIndex

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal UltraGridRow GetItemAtScrollIndex( int scrollIndex )
		//{
		//    // SSP 8/2/06 BR14598
		//    // Added an overload that takes in allocate parameter.
		//    // 
		//    return this.GetItemAtScrollIndex( scrollIndex, true );
		//    /*
		//    this.VerifyAgainstScrollVersion( );
		//    return (UltraGridRow)this.GetItemAtScrollIndex( scrollIndex, this );
		//    */
		//}

		#endregion Not Used

		// SSP 8/2/06 BR14598
		// Added an overload that takes in allocate parameter.
		// 
		internal UltraGridRow GetItemAtScrollIndex( int scrollIndex, bool allocate )
		{
			this.VerifyAgainstScrollVersion( );
			return (UltraGridRow)this.GetItemAtScrollIndex( scrollIndex, allocate ? this : null );
		}

		#endregion // GetItemAtScrollIndex

		#region NotifyItemScrollCountChanged

		internal new void NotifyItemScrollCountChanged( ISparseArrayMultiItem item )
		{
			base.NotifyItemScrollCountChanged( item );
		}

		#endregion // NotifyItemScrollCountChanged

		#region OnScrollCountChanged

		protected override void OnScrollCountChanged( )
		{
			this.ownerRowsCollection.DirtyParentRowScrollCount( );
		}			

		#endregion // OnScrollCountChanged

		#region ICreateItemCallback.CreateItem

		object ICreateItemCallback.CreateItem( SparseArray array, int relativeIndex )
		{
			bool newRowCreated;
			UltraGridRow row = this.ownerRowsCollection.UnSortedActualRows.GetItem( relativeIndex, true, true, out newRowCreated );

			// If the row is created then fire initialize row on the row.
			//
			if ( newRowCreated && null != row )
				row.FireInitializeRow( );

			return row;
		}

		#endregion // ICreateItemCallback.CreateItem

		#region ToArray

		internal UltraGridRow[] ToArray( bool create )
		{
			if ( ! create )
				return (UltraGridRow[])base.ToArray( typeof( UltraGridRow ) );

			UltraGridRow[] array = new UltraGridRow[ this.Count ];
			for ( int i = 0; i < this.Count; i++ )
				array[i] = this.GetItem( i, true );

			return array;
		}

		#endregion // ToArray
	}

	#endregion // ScrollCountSparseArray 

	#region RowsCollection Class

	/// <summary>
	/// A collection of Infragistics.Win.UltraWinGrid.UltraGridRow objects
	/// </summary>
	public class RowsCollection : SubObjectsCollectionBase, IComparer,
                                  IEnumerable<UltraGridRow> //  BF NA 9.1 - UltraCombo MultiSelect

	{
		#region Class/Interface Definitions

		#region ListObjectHolder Class

		private class ListObjectHolder
		{
			internal int listIndex = -1;
			internal object listObject = null;
			internal object comparisionListObject = null;
			// SSP 4/13/04 - Virtual Binding
			//
			internal UltraGridRow row = null;

			internal ListObjectHolder( UltraGridRow row, object listObject, object comparisionListObject, int listIndex )
				: this( listObject, comparisionListObject, listIndex )
			{
				this.row = row;
			}

			internal ListObjectHolder( object listObject, object comparisionListObject, int listIndex )
			{
				this.listIndex = listIndex;
				this.listObject = listObject;
				this.comparisionListObject = comparisionListObject;
			}

			public override int GetHashCode( )
			{
				return this.comparisionListObject.GetHashCode( );
			}

			public override bool Equals( object o )
			{
				ListObjectHolder listObjectHolder = o as ListObjectHolder;
				return null != listObjectHolder && listObjectHolder.comparisionListObject == this.comparisionListObject;
			}
		}

		#endregion // ListObjectHolder Class

		#region RowsSortComparer Class

		// SSP 2/17/04 - Virtual Mode - Optimization
		//
		// MD 9/20/07 - 7.3 Performance
		// Implement generic comparer so we can use the generic sort merge
		//internal class RowsSortComparer : IComparer
		internal class RowsSortComparer : IComparer, IComparer<UltraGridRow>
		{
			private RowsCollection rows;
			private UltraGridBand band;
			private UltraGridColumn[] sortColumns;
			private UltraGridRow currentAddRow;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//private bool designMode;

			// SSP 11/11/05 BR07685
			// We need to implement the new IGroupByEvaluatorEx interface because we need
			// to make sure that the sorting logic is consistent with the grouping logic.
			// 
			private bool groupingRows;
			private IGroupByEvaluator[] groupByEvaluators;
			internal bool groupByEvaluatorExUsed = false;

			// SSP 12/4/06 BR16115 - Optimization
			// Cache the values, namely the translated text for better performance in case of
			// value lists.
			// 
			// --------------------------------------------------
			private Hashtable[] cellComparisonValues = null;
			private class CellComparisonValue
			{
				UltraGridRow row;
				internal object value;
				internal object text;
				internal bool comparesByValue;
				private int cachedListIndex = -1;

				internal CellComparisonValue( UltraGridRow row, UltraGridColumn column )
				{
					this.row = row;
					this.value = row.GetCellValue( column );

					EmbeddableEditorBase editor = column.GetEditor( row );
					this.comparesByValue = null == editor || editor.ComparesByValue( column.EditorOwnerInfo, row );
				}

				internal int ListIndex
				{
					get
					{
						if ( this.cachedListIndex < 0 )
							this.cachedListIndex = this.row.ListIndex;

						return this.cachedListIndex;
					}
				}
			}
			// --------------------------------------------------

			internal RowsSortComparer( RowsCollection rows )
				: this( rows, false )
			{
			}

			// SSP 11/11/05 BR07685
			// We need to implement the new IGroupByEvaluatorEx interface because we need
			// to make sure that the sorting logic is consistent with the grouping logic.
			// Added an overload that takes in groupingRows parameter.
			// 
			internal RowsSortComparer( RowsCollection rows, bool groupingRows )
			{
				// SSP 11/11/05 BR07685
				// We need to implement the new IGroupByEvaluatorEx interface because we need
				// to make sure that the sorting logic is consistent with the grouping logic.
				// Added an overload that takes in groupingRows parameter.
				// 
				this.groupingRows = groupingRows;

				this.rows = rows;
				this.band = rows.band;

				HeaderClickAction headerClickAction = this.band.GetHeaderClickActionResolved( false );

				// SSP 4/19/04 - Virtual Binding Related
				// Added ExternalSortSingle and ExternalSortMulti members.
				// If the header click action is ExternalSortSingle or ExternalSortMulti then
				// don't sort by columns, but by the list indeces.
				//
				//if ( this.band.HasSortedColumns )
				if ( this.band.HasSortedColumns
					&& HeaderClickAction.ExternalSortSingle != headerClickAction
					&& HeaderClickAction.ExternalSortMulti != headerClickAction )
				{
					SortedColumnsCollection sortedColsColl = this.band.SortedColumns;

					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//ArrayList list = new ArrayList( );
					List<UltraGridColumn> list = new List<UltraGridColumn>();

					for ( int i = 0; i < sortedColsColl.Count; i++ )
					{
						UltraGridColumn column = sortedColsColl[i];

						// SSP 8/2/02 UWG1481
						// If the column's index is less than 0, then that means we are in the middle
						// of resetting the layout or something like that. Column is invalid (it is not
						// in the band's columns collection anymore, however the sorted columns 
						// collection is still holding the reference to it. So ignore those columns.
						//
						if ( column.Index < 0 )
							continue;

						list.Add( column );
					}

					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//this.sortColumns = (UltraGridColumn[])list.ToArray( typeof( UltraGridColumn ) );
					this.sortColumns = list.ToArray();
				}
				else
				{
					this.sortColumns = new UltraGridColumn[0];
				}

				// SSP 11/11/05 BR07685
				// We need to implement the new IGroupByEvaluatorEx interface because we need
				// to make sure that the sorting logic is consistent with the grouping logic.
				// 
				this.groupByEvaluators = new IGroupByEvaluator[this.sortColumns.Length];
				for ( int i = 0; i < this.groupByEvaluators.Length; i++ )
					this.groupByEvaluators[i] = this.sortColumns[i].GroupByEvaluatorResolved;

				this.currentAddRow = this.rows.CurrentAddRow;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.designMode = band.Layout.Grid.DesignMode;

				// SSP 12/4/06 BR16115 - Optimization
				// Cache the values, namely the translated text for better performance in case of
				// value lists.
				// 
				this.cellComparisonValues = new Hashtable[this.sortColumns.Length];
				for ( int i = 0; i < this.sortColumns.Length; i++ )
					this.cellComparisonValues[i] = new Hashtable( this.rows.Count );
			}

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			// Added the 'column' parameter so that the SortComparisonTypeResolved property can be honored.
			//
			//SSP 9/5/01
			//Made this function a static function
			//private int DefaultCompare( object x, object y )		
			//internal static int DefaultCompare( object x, object y )
			internal static int DefaultCompare( object x, object y, UltraGridColumn column )
			{
				return RowsCollection.RowsSortComparer.DefaultCompare( x, y, true, column );
			}

			// JAS v5.2 GroupBy Break Behavior 5/4/05
			// Added the 'column' parameter so that the SortComparisonTypeResolved property can be honored.
			//
			//SSP 9/5/01
			//Made this function a static function
			//private int DefaultCompare( object x, object y )		
			//internal static int DefaultCompare( object x, object y, bool duringSort )
			internal static int DefaultCompare( object x, object y, bool duringSort, UltraGridColumn column )
			{
				Debug.Assert( null != column );
				return DefaultCompare( x, y, duringSort, null != column && SortComparisonType.CaseInsensitive == column.SortComparisonTypeResolved );
			}

			// SSP 5/16/05 - FilterComparisonType property
			// Added an overload of DefaultCompare that takes in caseInSensitive.
			//
			internal static int DefaultCompare( object x, object y, bool duringSort, bool caseInSensitive )
			{
				// MBS 11/13/06
				// Refactored code into Win.Utilities
				#region Refactored Code
				//    //Check for a null database field
				//    if ( x == null || System.DBNull.Value == x )
				//    {
				//        if ( y == null || System.DBNull.Value == y )
				//            return 0;

				//        return -1;
				//    }

				//    if ( y == null || System.DBNull.Value == y )
				//        return 1;

				//    // JAS v5.2 GroupBy Break Behavior 5/4/05
				//    //
				//    // SSP 5/16/05 - FilterComparisonType property
				//    // Added an overload of DefaultCompare that takes in caseInSensitive.
				//    //
				//    // ------------------------------------------------------------------------------------------
				//    /*
				//    Debug.Assert( column != null, "'column' should not be null." );
				//    if( column != null && x is string && y is string )
				//    {
				//        string strX = x as string;
				//        string strY = y as string;
				//        bool isCaseInsensitive = column.SortComparisonTypeResolved == SortComparisonType.CaseInsensitive;
				//        return string.Compare( strX, strY, isCaseInsensitive, System.Globalization.CultureInfo.CurrentCulture );
				//    }
				//    */
				//    if ( caseInSensitive )
				//    {
				//        string xxStr = x as string;
				//        string yyStr = y as string;
				//        if ( null != xxStr && null != yyStr )
				//            return string.Compare( xxStr, yyStr, caseInSensitive, System.Globalization.CultureInfo.CurrentCulture );
				//    }
				//    // ------------------------------------------------------------------------------------------

				//    // SSP 1/12/05 BR01572
				//    // Moved this code from below. Before this change if two cells from the same
				//    // column contained values that had different types then we ended up throwing
				//    // an exception (see the if block below). We should first try the IComparable
				//    // and then throw an exception.
				//    //
				//    IComparable comparable = x as IComparable;

				//    // JJD 1/16/02
				//    // If the objects implement IComparable then call that
				//    //
				//    if ( comparable != null )
				//        return comparable.CompareTo( y );

				//    // Check types.
				//    //
				//    if ( x.GetType( ) != y.GetType( ) )
				//        throw new Exception(Shared.SR.GetString("LE_Exception_240") );

				//    // SSP 1/12/05 BR01572
				//    // Moved this code before the above if statement.
				//    //
				//    /*
				//    IComparable comparable = x as IComparable;

				//    // JJD 1/16/02
				//    // If the objects implement IComparable then call that
				//    //
				//    if ( comparable != null )
				//        return comparable.CompareTo( y );
				//    */

				//    int compare = 0;

				//    System.Type xType = x.GetType( );

				//    if ( xType == typeof(string) )
				//    {
				//        // AS 1/8/03 fxcop
				//        // Explicitly specify a cultureinfo
				//        //
				//        //compare = string.Compare( (string)x,(string)y );
				//        compare = string.Compare( (string)x,(string)y, false, System.Globalization.CultureInfo.CurrentCulture );
				//    }

				//    else if ( xType == typeof( char ) )
				//    {
				//        if ( (char)x < (char)y )
				//            compare = -1;
				//        else
				//            if ( (char)x > (char)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(int) )
				//    {
				//        if ( (int)x < (int)y )
				//            compare = -1;
				//        else
				//            if ( (int)x > (int)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(uint) )
				//    {
				//        if ( (uint)x < (uint)y )
				//            compare = -1;
				//        else
				//            if ( (uint)x > (uint)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(long) )
				//    {
				//        if ( (long)x < (long)y )
				//            compare = -1;
				//        else
				//            if ( (long)x > (long)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(ulong) )
				//    {
				//        if ( (ulong)x < (ulong)y )
				//            compare = -1;
				//        else
				//            if ( (ulong)x > (ulong)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(short) )
				//    {
				//        if ( (short)x < (short)y )
				//            compare = -1;
				//        else
				//            if ( (short)x > (short)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(ushort) )
				//    {
				//        if ( (ushort)x < (ushort)y )
				//            compare = -1;
				//        else
				//            if ( (ushort)x > (ushort)y )
				//            compare = 1;
				//    }


				//    else if ( xType == typeof(double) )
				//    {
				//        if ( (double)x < (double)y )
				//            compare = -1;
				//        else
				//            if ( (double)x > (double)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(float) )
				//    {
				//        if ( (float)x < (float)y )
				//            compare = -1;
				//        else
				//            if ( (float)x > (float)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(decimal) )
				//    {
				//        if ( (decimal)x < (decimal)y )
				//            compare = -1;
				//        else
				//            if ( (decimal)x > (decimal)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(System.DateTime) )
				//    {
				//        if ( (System.DateTime)x < (System.DateTime)y )
				//            compare = -1;
				//        else
				//            if ( (System.DateTime)x > (System.DateTime)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(System.DayOfWeek) )
				//    {
				//        if ( (System.DayOfWeek)x < (System.DayOfWeek)y )
				//            compare = -1;
				//        else
				//            if ( (System.DayOfWeek)x > (System.DayOfWeek)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(byte) )
				//    {
				//        if ( (byte)x < (byte)y )
				//            compare = -1;
				//        else
				//            if ( (byte)x > (byte)y )
				//            compare = 1;
				//    }

				//    else if ( xType == typeof(sbyte) )
				//    {
				//        if ( (sbyte)x < (sbyte)y )
				//            compare = -1;
				//        else
				//            if ( (sbyte)x > (sbyte)y )
				//            compare = 1;
				//    }
				//        // SSP 5/7/02
				//        // Added support for System.Drawing.Color
				//        //
				//    else if ( xType == typeof( System.Drawing.Color ) )
				//    {
				//        int xArgbVal = ((System.Drawing.Color)x).ToArgb( );
				//        int yArgbVal = ((System.Drawing.Color)y).ToArgb( );

				//        if ( xArgbVal < yArgbVal )
				//            compare = -1;
				//        else
				//            if ( xArgbVal > yArgbVal )
				//            compare = 1;
				//    }
				//        //Default: convert to string and compare
				//        // SSP 1/19/05 BR01830
				//        // If not sorting (like when merging, or filtering), don't consider two objects 
				//        // as equal if their ToString results are equal because the ToString by default
				//        // returns the type name which would be the same even if the objects were 
				//        // different. Added the following else-if block.
				//        //
				//        // ----------------------------------------------------------------------------
				//    else if ( ! duringSort )
				//    {
				//        compare = x.Equals( y ) ? 0 : -2;
				//    }
				//        // ----------------------------------------------------------------------------
				//    else
				//    {
				//        string newX = x.ToString();
				//        string newY = y.ToString();

				//        // AS 1/8/03 fxcop
				//        // Explicitly specify a cultureinfo
				//        //
				//        //compare = string.Compare( (string)newX,(string)newY );
				//        compare = string.Compare( (string)newX,(string)newY, false, System.Globalization.CultureInfo.CurrentCulture );
				//    }


				//    return compare;

				#endregion //Refactored Code

				return Utilities.DefaultCompare( x, y, duringSort, caseInSensitive );
			}

			int IComparer.Compare( object x, object y )
			{
				Infragistics.Win.UltraWinGrid.UltraGridRow rowX = x as Infragistics.Win.UltraWinGrid.UltraGridRow;
				Infragistics.Win.UltraWinGrid.UltraGridRow rowY = y as Infragistics.Win.UltraWinGrid.UltraGridRow;

				// MD 9/20/07 - 7.3 Performance
				// Moved all code to new CompareHelper method
				return this.CompareHelper( rowX, rowY );
			}

			// MD 9/20/07 - 7.3 Performance
			// Moved all code from non-generic compare method so it could also be used in the generic compare method
			private int CompareHelper( Infragistics.Win.UltraWinGrid.UltraGridRow rowX, Infragistics.Win.UltraWinGrid.UltraGridRow rowY )
			{
				if ( rowX == rowY )
					return 0;

				if ( null == rowX )
				{
					if ( this.sortColumns.Length > 0 && SortIndicator.Descending == this.sortColumns[0].SortIndicator )
						return 1;

					return -1;
				}

				if ( null == rowY )
				{
					if ( this.sortColumns.Length > 0 && SortIndicator.Descending == this.sortColumns[0].SortIndicator )
						return -1;

					return 1;
				}

				// SSP 11/14/03 Add Row Feature
				// Ensure that the add row is at the top or the bottom depending on whether add-rows are 
				// displayed at the top or at the bottom.
				// Added following block of code.
				// 
				// ----------------------------------------------------------------------------------------------
				if ( null != this.currentAddRow && ( rowX == this.currentAddRow || rowY == this.currentAddRow ) )
				{
					TemplateAddRowLocation addRowLocDef = this.rows.TemplateRowLocationDefault;

					if ( TemplateAddRowLocation.Top == addRowLocDef )
						return rowX == this.currentAddRow ? -1 : 1;
					else if ( TemplateAddRowLocation.Bottom == addRowLocDef )
						return rowX == this.currentAddRow ? 1 : -1;
				}
				// ----------------------------------------------------------------------------------------------

				int compare = 0;

				// SSP 12/4/06 BR16115 - Optimization
				// Cache the values, namely the translated text for better performance in case of
				// value lists.
				// 
				CellComparisonValue cvX = null, cvY = null;

				for ( int i = 0; i < this.sortColumns.Length; i++ )
				{
					Infragistics.Win.UltraWinGrid.UltraGridColumn column = this.sortColumns[i];

					// SSP 11/11/05 BR07685
					// We need to implement the new IGroupByEvaluatorEx interface because we need
					// to make sure that the sorting logic is consistent with the grouping logic.
					// Added the if block.
					// 
					if ( this.groupingRows && this.groupByEvaluators[i] is IGroupByEvaluatorEx )
					{
						IGroupByEvaluatorEx evaluator = (IGroupByEvaluatorEx)this.groupByEvaluators[i];
						compare = evaluator.Compare( rowX.Cells[column], rowY.Cells[column] );
						this.groupByEvaluatorExUsed = true;
					}
					// SSP 1/10/02 UWG864
					// If we have a user specified comparer, then pass in cell objects
					// for comparing the cells.
					//
					else if ( null != column.SortComparer )
					{
						compare = column.SortComparer.Compare( rowX.Cells[column], rowY.Cells[column] );
					}
					else
					{
						// SSP 12/4/06 BR16115 - Optimization
						// Cache the values, namely the translated text for better performance in case of
						// value lists.
						// 
						// ------------------------------------------------------------------------------------------------
						Hashtable cellComparisonValues = this.cellComparisonValues[i];
						cvX = cellComparisonValues[ rowX ] as CellComparisonValue;
						cvY = cellComparisonValues[ rowY ] as CellComparisonValue;

						if ( null == cvX )
							cellComparisonValues[rowX] = cvX = new CellComparisonValue( rowX, column );

						if ( null == cvY )
							cellComparisonValues[rowY] = cvY = new CellComparisonValue( rowY, column );

						object valueX, valueY;
						if ( cvX.comparesByValue && cvY.comparesByValue )
						{
							valueX = cvX.value;
							valueY = cvY.value;
						}
						else
						{
							// Note: GetCellText takes into consideration column being unbound.
							//							
							if ( null == cvX.text )
								cvX.text = rowX.GetCellText( column );

							if ( null == cvY.text )
								cvY.text = rowY.GetCellText( column );

							valueX = cvX.text;
							valueY = cvY.text;
						}

						
						// ------------------------------------------------------------------------------------------------

						// JAS v5.2 GroupBy Break Behavior 5/4/05 - Added 'column' argument.
						//
						//compare = RowsSortComparer.DefaultCompare( valueX, valueY );
						compare = RowsSortComparer.DefaultCompare( valueX, valueY, column );
						// ----------------------------------------------------------------
					}

					if ( compare != 0 )
					{
						// if we are sorting descended then flip the sign
						//
						if ( column.SortIndicator == SortIndicator.Descending )
							compare *= -1;

						return compare;
					}
				}

				// SSP 8/19/02 UWG1273
				// Instead of returning 0 here which will cause the Array.Sort method
				// to put equal rows any way it wishes, return -1 or 1 based on the
				// index so that it will put the row with higher list index after the
				// row with lower list index. The result of this will be that when
				// a column is sorted by and all the cells of that column are blank,
				// the sorting will retain rows' positions. Before Array.Sort was 
				// reordering the rows even when all the rows were equal randomly
				// so to the user it would look as if grid sorted even if it did not 
				// need to sort.
				//
				if ( 0 == compare )
				{
					// SSP 12/4/06 BR16115 - Optimization
					// Cache the values, namely the translated text for better performance in case of
					// value lists.
					// 
					// --------------------------------------------------------------------------------
					//compare = rowX.ListIndex < rowY.ListIndex ? -1 : 1;
					int xxListIndex = null != cvX ? cvX.ListIndex : rowX.ListIndex;
					int yyListIndex = null != cvY ? cvY.ListIndex : rowY.ListIndex;

					compare = xxListIndex < yyListIndex ? -1 : 1;
					// --------------------------------------------------------------------------------
				}

				return compare;
			}

			// MD 9/20/07 - 7.3 Performance
			// Implemented generic compare so we can use generic sort merge
			#region IComparer<UltraGridRow> Members

			int IComparer<UltraGridRow>.Compare( UltraGridRow rowX, UltraGridRow rowY )
			{
				return this.CompareHelper( rowX, rowY );
			}

			#endregion
		}

		#endregion // RowsSortComparer Class

		#region ListIndexComparer Class
		
		// SSP 10/20/04 UWG3732
		// Added ListIndexComparer because when the header click action is external sort,
		// we need to sort the rows according to their list indexes. Typically we don't 
		// need to sort rows with header click action however in group-by situation we 
		// need to perform the list index sort because in SyncRowsHelper we try to maintain
		// the group-by rows and their child rows in the order they were before and what 
		// that means is that we need to sort their child rows. Look in the SyncRowsHelper 
		// for more info, especially the part concerning how we use SyncRows_Add and 
		// SyncRows_Remove calls to add new rows instead of simply recreating the whole
		// hierarchy.
		//
		internal class ListIndexComparer : IComparer
		{
			public int Compare( object x, object y )
			{
				UltraGridRow xxRow = (UltraGridRow)x;
				UltraGridRow yyRow = (UltraGridRow)y;

				int xxListIndex = null != xxRow ? xxRow.ListIndex : -1;
				int yyListIndex = null != yyRow ? yyRow.ListIndex : -1;

				return xxListIndex < yyListIndex ? -1 : ( xxListIndex > yyListIndex ? 1 : 0 );
			}
		}

		#endregion // ListIndexComparer Class
	
		#region RowsSparseArray Class

		internal class RowsSparseArray : Infragistics.Shared.SparseArray
		{
			private RowsCollection ownerRowsCollection = null;

			internal RowsSparseArray( RowsCollection rows ) : base( true )
			{
				this.ownerRowsCollection = rows;
			}

			protected override object GetOwnerData( object item )
			{
				return ((UltraGridRow)item).unsortedRows_SparseArrayOwnerData;
			}

			protected override void SetOwnerData( object item, object ownerData )
			{
				((UltraGridRow)item).unsortedRows_SparseArrayOwnerData = ownerData;
			}

			internal UltraGridRow GetItem( int index, bool create )
			{
				bool newRowCreated;
				return this.GetItem( index, create, false, out newRowCreated );
			}

			internal UltraGridRow GetItem( int index, bool create, bool calledFromScrollCountManagerSparseArray, out bool newRowCreated )
			{
				newRowCreated = false;
				UltraGridRow row = (UltraGridRow)this[ index ];

				if ( null == row && create )
				{
					// MD 12/4/06 - BR17930
					// Use the new overload which sets the InternalCachedListObject
					//row = this.ownerRowsCollection.AllocateNewRow( );
					row = this.ownerRowsCollection.AllocateNewRow( index );

					newRowCreated = true;
					this[ index ] = row;

					if ( ! calledFromScrollCountManagerSparseArray )
					{
						UltraGridRow tmpRow = (UltraGridRow)this.ownerRowsCollection.SparseArray[ index ];
						Debug.Assert( null == tmpRow );
						this.ownerRowsCollection.SparseArray[ index ] = row;

						// Fire InitializeRow for the new row only if not called from the scroll 
						// count manager sparse array. If we did get called from there then we 
						// don't need to fire the initializerow here as it does that itself.
						//
						row.FireInitializeRow( );
					}
				}

				return row;
			}

			
			
			
			
			internal UltraGridRow[] ToArray( bool create, bool syncWithSortedSparseArray )
			{
				return this.ToArray( create, syncWithSortedSparseArray, false );
			}

			// SSP 9/7/07 BR24503
			// Added an overload with dontFireInitializeRowOnNewRows parameter.
			// 
			internal UltraGridRow[] ToArray( bool create, bool syncWithSortedSparseArray, bool dontFireInitializeRowOnNewRows )
			{
				UltraGridRow[] array = (UltraGridRow[])base.ToArray( typeof( UltraGridRow ) );

				if ( create )
				{
					for ( int i = 0; i < array.Length; i++ )
					{
						if ( null == array[i] )
						{
							
							
							
							// ----------------------------------------------------
							if ( syncWithSortedSparseArray )
							{
								bool newRowCreated;
								array[i] = this.GetItem( i, true, false, out newRowCreated );
							}
							else
							{
								UltraGridRow row = this.ownerRowsCollection.AllocateNewRow( i );
								array[i] = row;
								this[i] = row;

								// SSP 9/7/07 BR24503
								// We need to fire InitializeRow event on the newly created row.
								// 
								if ( ! dontFireInitializeRowOnNewRows )
									row.FireInitializeRow( );
							}
							
							// ----------------------------------------------------
						}
					}
				}

				return array;
			}
		}

		#endregion // RowsSparseArray Class

		#endregion // Class/Interface Definitions

		#region Private vars

		// SSP 4/5/04 - Virtual Binding Related Optimizations
		//
		//private ArrayList unsortedActualRows = null;
		private RowsCollection.RowsSparseArray unsortedActualRows = null;
		private ScrollCountManagerSparseArray sparseArray = null;		

		private Infragistics.Win.UltraWinGrid.UltraGridBand band;
		private Infragistics.Win.UltraWinGrid.UltraGridRow  parentRow;
		private UltraGridRow firstVisibleCard = null;

		private int  verifyVersion	= 0;

		private int  cardAreaHeight = 0;
		private int  cardAreaHeightInCards = 0;

		// SSP 10/4/01
		// This little flag was causing problems all over the grid.
		// Default should be false. Look in Layout.Rows and ChildBand.Rows
		// properties for further info.
		//
		//private bool rowsDirty = true;
		private bool rowsDirty = false;

		// MRS 7/25/05 - BR04641
		//
		//private int  sortVersion = 0;
		internal int  sortVersion = 0;

		// SSP 2/20/04
		// groupByColumn member variable is never used anywhere. Commenting it out.
		//
		//private UltraGridColumn groupByColumn = null;
		
		// SSP 1/16/02
		//
		//private int verifiedGroupBySortVersion = -1;
		// This will be initialized in the constructor.
		//
		private bool verifiedGroupBySortDirectionAscending;

		private int verifiedGroupByHierarchyVersion = -1;
		internal int verifiedGroupByHierarchyBandsVersion = -1;

		// SSP 1/22/04 - Optimizations
		//
		//private int verifiedGroupByStyleVersion = -1;

		// anti-recursion flags
		private bool inInitGroupByRows = false;
		private bool inInitNonGroupByRows = false;
		private bool inSyncRows = false;

		private IBindingList oldBoundList = null;
		private System.ComponentModel.ListChangedEventHandler listChangedHandler = null;

		// SSP 3/21/02
		// vars for row filtering
		//	
		private int rowFiltersVersion = 0;
		private ColumnFiltersCollection columnFilters = null;
		// SSP 8/1/03 UWG1654 - Filter Action
		// Added a mechanism to cache AreAnyFiltersActive.
		//
		private bool cachedAreAnyFiltersActive = false;
		private int verifiedRowFiltersVersion = -1;
		
		// SSP 5/17/02
		// Summary rows feature
		//
		private SummaryValuesCollection summaryValues = null;
		private int dataChangedVersionNumber = 0;

		// SSP 12/10/02 UWG1883
		// Implemented code to cache the binding list.
		//
		private object lastParentListObject = null;
		private IList  cachedList = null;

		// SSP 4/9/03 UWG2186
		// Added a mechanism to cache the visible row count for the rows collection.
		// The card-view code makes use of this however it causes a serious performance
		// degradation. So we need to cache it. Currently VisibleRowCount just loops
		// through all the rows.
		//
		// SSP 2/20/04 - Virtual Mode 
		//
		//private int cachedVisibleRowCount = -1;

		// SSP 6/19/03 UWG2232
		// Related to SuspendRowSynchronization and ResumeRowSynchronization methods.
		// When the row syncronization is suspended, we also suspend firing InitialiazeRow
		// (we don't respond to binding events). So when the row syncrhonization is resumed
		// fire initialize row on all the rows.
		//
		// SSP 3/31/05 BR03055
		// Made this internal from private.
		//
		internal bool fireInitializeRowsOnAllRow = false;

		// SSP 8/7/03 UWG1963
		// Added dontSyncRows flag to prevent the syncing of the rows in some situations.
		//
		private bool dontSyncRows = false;
		
		// SSP 8/12/03 - Optimizations - Grand Verify Version Number
		//
		private int verifiedGrandVersion_VerifyGroupByVersion = -1;
		private int verifiedGrandVersion_EnsureSortOrder = -1;

		// SSP 11/7/03 - Add Row Feature
		// 
		private UltraGridRow templateAddRow = null;
		private Hashtable addRowDefaultCellValues = null;
		internal bool inAddingTemplateAddRow = false;
		private bool currentAddRowModifiedByUserFlag = false;
		private UltraGridRow currentAddRow = null;

		// SSP 7/27/04 - UltraCalc
		//
		private RowsCollectionReference calcReference = null;

		// SSP 10/27/04 - UltraCalc
		// Moved the filter verification logic into the row collection from row. This is so
		// that we can evaluate filters at once on all the rows of a row collection and fire 
		// rows synced event to the calc manager instead of having to fire row deleted/added 
		// event for every row whose hidden state changes.
		//
		private bool inEnsureFiltersEvaluated = false;
		private int ensureFiltersEvaluated_verifiedFiltersVersion = 0;
		private int ensureFiltersEvaluated_verifiedFiltersGrandVersion = 0;

		// SSP 11/3/04 - Merged Cell Feature
		//
		private MergedCellCache mergedCellCache = null;

		// SSP 3/14/05 - NAS 5.2 Fixed Rows/Filter Row/Fixed Summary Footers
		//
		private FixedRowsCollection fixedRowCollection = null;
		private UltraGridFilterRow filterRow = null;
		internal int verifiedSpecialRowsCacheVersion	= -1;
		private UltraGridRow[] cachedSpecialRowsTop		= null;
		private UltraGridRow[] cachedSpecialRowsBottom  = null;
		private UltraGridRow[] cachedFixedRowsTop		= null;
		private UltraGridRow[] cachedFixedRowsBottom  = null;

		// MRS 12/5/05 - BR08024
		private bool inEnsureSortOrder = false;

		// SSP 7/20/06 BR11460
		// VisibleRow.GetTotalHeight sets the CardAreaHeight to available height. This overrides the
		// height that may have been explicitly set. We need to maintain the user set CardAreaHeight.
		// 
		private int explicitlySetCardAreaHeight = 0;

		#endregion // Private vars 

		#region Constructors

		internal RowsCollection( Infragistics.Win.UltraWinGrid.UltraGridBand band, 
			Infragistics.Win.UltraWinGrid.UltraGridRow parentRow )
		{
			this.EnsureSparseArrayCreated( );
			this.Initialize( band, parentRow, null );            
		}

		// SSP 9/5/03 UWG2634
		// this.Bands property verifies if the data source has been loaded. That could end up
		// accessing the rows collection recursively while we are trying to allocate the rows
		// collection. We don't want that to happen.
		// Added a constructor that does not require passing in a bands collection. 
		//
		private RowsCollection( )
		{
			this.EnsureSparseArrayCreated( );
		}

		#endregion // Constructors

		#region Base class overrides

		#region Count

		/// <summary>
		/// Overridden method overridden to implement lazy initialization of rows.
		/// </summary>
		public override int Count
		{
			get
			{
				// SSP 4/8/04
				// Commented out the original code and added call to EnsureVerified.
				//
				this.EnsureNotDirty( );
				

				return this.SparseArray.Count;
			}
		}

		#endregion // Count

		#region InitialCapacity

		/// <summary>
		/// Abstract property that specifies the initial capacity
		/// of the collection
		/// </summary>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		protected override int InitialCapacity
		{
			get
			{
				return 10;
			}        
		}

		#endregion // InitialCapacity

		#region List

		/// <summary>
		/// The list that contains the item references 
		/// </summary>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		// MD 5/15/08
		// VS2008 has a new warning where an obsolete member should not override a non-obsolete member. Also, this doesn't need 
		// to be obsoleted anyway: the constructors are private so this class cannot be derived from and the List member is protected.
		//[ Obsolete( "Obsoleted", true ) ]
		protected override ArrayList List
		{
			get 
			{
				Debug.Assert( false, "List should not be getting called. Rows collection is not using it all." );

				return null;
			}
		}

		#endregion // List

		#region IsReadOnly

		/// <summary>
		/// True if the collection is read only
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		#endregion // IsReadOnly

		#region GetItem

		/// <summary>
		/// Returns the item at the specified index
		/// </summary>
        /// <param name="index">Index of the object to retrieve.</param>
        /// <returns>The object at the index</returns>
        public override object GetItem(int index)
		{
			this.EnsureNotDirty( );

			return this.SparseArray.GetItem( index, true );
		}

		#endregion // GetItem

		#region OnSubObjectPropChanged

		/// <summary>
		/// Called when a property has changed
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			if ( null != this.columnFilters && propChange.Source == this.columnFilters )
			{
				// Bump the row filters version number
				// SSP 5/13/04 UWG3260 - Virtual Binding
				// Combined the verified band and rows version numbers into one.
				// We only want to bump the filters version number on the rows collection
				// if band's column filters are going to be used for filtering rows.
				// Moved this into the if block below. 
				//
				//this.BumpRowFiltersVersion( );
				
				if ( Infragistics.Win.UltraWinGrid.RowFilterMode.SiblingRowsOnly == this.Band.RowFilterModeResolved )
				{
					// SSP 5/13/04 UWG3260 - Virtual Binding
					// Moved this here from before the if condition.
					//
					this.BumpRowFiltersVersion( );

					// Call RowsChangedHelper which dirties the scrollable row count,
					// the visible rows and the grid element.
					//
					// SSP 4/8/04 - Virtual Binding
					// When an item from the scroll count sparse array is added, removed or moved
					// it will call ScrollCountManagerSparseArray.OnScrollCountChanged which takes
					// necessary steps to dirty the visible rows etc.
					//
					//this.RowsChangedHelper( );
					this.SparseArray.DirtyScrollCount( );
				}
			}

			// SSP 3/14/05 - NAS 5.2 Fixed Rows
			//
			if ( null != this.fixedRowCollection && propChange.Source == this.fixedRowCollection )
			{
				this.NotifyPropChange( PropertyIds.FixedRows );
			}
		}

		#endregion // OnSubObjectPropChanged

		#region OnDispose

		// SSP 9/9/03 UWG2308
		// Overrode OnDispose.
		//
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnDispose( )
		{
			// SSP 11/30/04 - Merged Cell Feature
			//
			this.ClearMergedCellCache( );

			// Unhook from the binding list.
			//
			this.UnBind( );

			RowsCollection.DisposeRows( this.SparseArray );

			// SSP 11/2/04 UWG3736
			// Null out the list object vars so the row collection doesn't keep holding a reference back
			// to the bound list even when the grid's DataSource is reset to null.
			//
			// ----------------------------------------------------------------------------------------
			this.cachedList = null;
			this.lastParentListObject = null;
			this.oldBoundList = null;
			this.unsortedActualRows = null;
			if ( null != this.sparseArray )
				this.sparseArray.Clear( );
			// ----------------------------------------------------------------------------------------

			base.OnDispose( );
		}

		#endregion // OnDispose

		#region CopyTo

		/// <summary>
		/// Copies the items from the collection into the array.
		/// </summary>
		/// <param name="array">The array to receive the items.</param>
		/// <param name="index">The index to start with.</param>
		public override void CopyTo(Array array, int index) 
		{
			// SSP 12/5/05
			// 
			//this.SparseArray.CopyTo( array, index );
			this.SparseArray.CopyTo( array, index, this.SparseArray );
		}

		#endregion // CopyTo

		#region IndexOf

        /// <summary>
        /// Returns the index of the item in the collection that has the passed in key or -1 if key not found.
        /// </summary>
        /// <param name="obj">The object whose index in the collection will be determined.</param>
        /// <returns>The index of the item in the collection that has the passed in key, or -1
        /// if no item is found.</returns>
        // AS 12/3/02 UWS624
		// Changed to virtual
		//public int IndexOf(object obj)
		public override int IndexOf( object obj ) 
		{
			// SSP 6/10/04 UWG3406
			// If the row collection is marked dirty then sync the row collection.
			//
			this.EnsureNotDirty( );

			return this.SparseArray.IndexOf( obj );
		}

		#endregion // IndexOf

		#region CreateArray

		// SSP 4/6/04
		// Overrode CreateArray method.
		//
		/// <summary>
		/// Overridden. Virtual method used by the All 'get' method to create the array it returns.
		/// </summary>
		/// <returns>The newly created object array</returns>
		/// <remarks>This is normally overridden in a derived class to allocate a type safe array.</remarks>
		protected override object[] CreateArray()
		{
			return new UltraGridRow[ this.Count ];
		}

		#endregion // CreateArray

		#region All

		/// <summary>
		/// Returns an array containing all the items contained in this rows collection.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public override object[] All 
		{
			get 
			{
				// SSP 10/29/04 - UltraCalc
				//
				//object[] array = new UltraGridRow[ this.Count ];
				UltraGridRow[] array = new UltraGridRow[ this.Count ];

				for ( int i = 0; i < this.Count; i++ )
					array[i] = this[i];
                
				return array;
			}
			set 
			{
				throw new NotSupportedException();
			}
		}

		#endregion // All

		#endregion // Base class overrides

		#region Public Properties

		#region Indexer

		/// <summary>
		/// Indexer.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow this[ int index ]
		{
			get
			{
				this.EnsureNotDirty( );

				return this.SparseArray.GetItem( index, true );
			}
		}

		#endregion // Indexer

		#region ParentRow
		
		/// <summary>
		/// Returns reference to parent row.
		/// </summary>
		/// <remarks>
		/// Parent row could either be a group-by row or an instance of UltraGridRow. If it's a group-by row then it will be from the same band as this RowsCollection, otherwise the it will be from parent band of this RowsCollection's band.
		/// </remarks>
		// SSP 4/22/02 
		// Made ParentRow property public bacause we do want to allow the users to 
		// be able to traverse up the rows hierarchy by accessing parent row 
		// (for example from an event that fires and they are given a RowsCollection 
		// object and they want to check which band it belongs to and whether it's
		// top-most rows collection).
		// Also a new summary is called for.
		
		//internal Infragistics.Win.UltraWinGrid.UltraGridRow ParentRow
		public Infragistics.Win.UltraWinGrid.UltraGridRow ParentRow
		{
			get
			{
				return this.parentRow;
			}
		}

		#endregion // ParentRow

		#region SummaryValues

		// SSP 5/17/02
		// Added SummaryValues property for summary rows feature.
		//
		/// <summary>
		/// Returns a collection of SummaryValue objects. Each of these SummaryValue objects in the collection is associated with a SummarySettings object in Summaries collection of the Band. The user can use this collection to get value of a summary for this rows collection.
		/// </summary>
		public SummaryValuesCollection SummaryValues
		{
			get
			{
				if ( null == this.summaryValues )
				{
					this.summaryValues = new SummaryValuesCollection( this );
				}

				return this.summaryValues;
			}
		}

		#endregion // SummaryValues

		#region ColumnFilters

		/// <summary>
		/// Column filters for filtering the rows in this rows collection. 
		/// They will apply only if the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/> resolves to SiblingRowsOnly.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.RowFilterMode"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterCondition"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterConditionsCollection"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter.FilterConditions"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/>
		/// <seealso cref="UltraGridBand.ColumnFilters"/>
		/// <seealso cref="UltraGridOverride.FilterUIType"/>
		/// </remarks>
		public ColumnFiltersCollection ColumnFilters
		{
			get
			{
				// SSP 9/2/03 UWG2330
				// If we have group-by rows, then resolve the column filters to the column
				// filters of the top-level rows collection.
				//
				// --------------------------------------------------------------------------
				RowsCollection topLevelRowsCollection = this.TopLevelRowsCollection;
				if ( this != topLevelRowsCollection )
					return topLevelRowsCollection.ColumnFilters;
				// --------------------------------------------------------------------------
				
				// SSP 8/6/04 UWG3547
				// For the root rows collection make use of the band's column filters. We also
				// are now always resolving the RowFilterMode to AllRowsInBand for the root 
				// band. This was to done in response to recurring newsgroup posts and bug 
				// reports regarding the reason why Band's ColumnFilters did not apply when
				// RowFilterMode was left to be Default because we resolved the RowFilterMode 
				// to SiblingRowsOnly for non-horizontal multiband view styles.
				//
				// ----------------------------------------------------------------------------
				if ( null != this.Band && null == this.Band.ParentBand )
					return this.Band.ColumnFilters;
				// ----------------------------------------------------------------------------

				if ( null == this.columnFilters )
				{
					this.columnFilters = new Infragistics.Win.UltraWinGrid.ColumnFiltersCollection( this.Band );

					this.columnFilters.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.columnFilters;
			}
		}

		#endregion // ColumnFilters

		#region CardAreaHeight

		// SSP 3/19/03 - Row Layout Functionality
		// Made CardAreaHeight proeprty public because we need to access it in the row layout
		// designer.
		//
		/// <summary>
		/// Caches the height of the card area
		/// </summary>
		//internal int CardAreaHeight
		public int CardAreaHeight
		{
			get
			{
				// calculate a minimum height for 1 card
				//
				int minHeight = this.Band.CalculateRequiredCardAreaHeight(1);
				
				if ( this.cardAreaHeight < 1 )
				{
					// SSP 6/2/8/02
					// Adde code for compressed card view.
					// If the card style is compressed, then when the number of cards
					// in the rows collection are as such that height of the card is
					// bigger than the height required show all the cards, then shrink
					// down the card area to save space.
					//
					int tmpHeight = 0;
					if ( this.ForceSingleColumnCompressedAndVariableCardView( out tmpHeight ) && tmpHeight > 0 )
						minHeight = tmpHeight;

					return minHeight;
				}

				//	BF 11.9.04	NAS2005 Vol1 - CardView vertical scrolling
				//
				//	We now allow the card area to be resized (vertically)
				//	smaller than the card height, so initialize the 'height'
				//	stack variable to the value of this.cardAreaHeight.
				//
				//int height;
				int height = this.cardAreaHeight;

				// variable height style uses the cached pixel value
				//
				// SSP 2/28/03 - Row Layout Functionality
				// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
				// to StandardLabels card style so use the StyleResolved instead of the Style property.
				//
				if ( this.band.CardSettings.StyleResolved == CardStyle.VariableHeight
					// SSP 6/28/02
					// Same applies to compressed card view also.
					//
					// SSP 2/28/03 - Row Layout Functionality
					// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
					// to StandardLabels card style so use the StyleResolved instead of the Style property.
					//
					|| CardStyle.Compressed == this.band.CardSettings.StyleResolved	)
				{
					// make sure the height is big enough for at least one card
					//
					//	BF 11.9.04	NAS2005 Vol1 - CardView vertical scrolling
					//
					//	We don't want to do this anymore, since we now display
					//	a vertical scrollbar when the card area's height is less
					//	than that of the cards being displayed. I made the minimum
					//	size 1 just in case it is possible to resize the element to
					//	a zero height, in which case it would not be possible to size
					//	it back.
					//
					//height = Math.Max( this.cardAreaHeight, minHeight );
					height = Math.Max( this.cardAreaHeight, 1 );
				}
				else
				{
					// the integral height card styles use the cached height in cards
					//
					// JM UWG1019 - Validate the card area height in cards
					//				against the number of visible rows.
					if ( this.cardAreaHeightInCards > this.VisibleRowCount )
						this.cardAreaHeightInCards = this.VisibleRowCount;

					if ( this.cardAreaHeightInCards >  1 )
						height = this.Band.CalculateRequiredCardAreaHeight( this.cardAreaHeightInCards );
					//	BF 11.9.04	NAS2005 Vol1 - CardView vertical scrolling
					//
					//	We now allow the card area to be resized (vertically)
					//	smaller than the card height, so we don't want to do
					//	this anymore.
					//
					//	else
					//		height = minHeight;
				}

				if ( height > minHeight )
				{
					// Make sure the height is not greater than the max height based
					// on the MaxCardAreaRows setting
					//
					int maxCardRows = this.band.CardSettings.MaxCardAreaRows;
					int maxHeight	= height;

					// For integral height card styles we should set the height
					// based on the lesser of either the number cards last set or
					// the MaxCardAreaRows value.
					//
					// SSP 2/28/03 - Row Layout Functionality
					// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
					// to StandardLabels card style so use the StyleResolved instead of the Style property.
					//
					if ( this.band.CardSettings.StyleResolved != CardStyle.VariableHeight
						// SSP 6/28/02
						// Compressed card view
						// Added below clause in the condition
						//
						// SSP 2/28/03 - Row Layout Functionality
						// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
						// to StandardLabels card style so use the StyleResolved instead of the Style property.
						//
						&& CardStyle.Compressed != this.band.CardSettings.StyleResolved	)
					{
						if ( maxCardRows > 0 )
							maxCardRows = Math.Min( maxCardRows, Math.Max( 1, this.cardAreaHeightInCards ) );
						else
							maxCardRows = Math.Max( 1, this.cardAreaHeightInCards );
					}
					
					if ( maxCardRows > 0 )
						maxHeight = this.Band.CalculateRequiredCardAreaHeight( maxCardRows );

					height = Math.Min( height, maxHeight );
				}

				// JM 02-04-02 Save the new height.
				this.cardAreaHeight = height;

				return height;
			}

			set
			{
				// SSP 7/20/06 BR11460
				// VisibleRow.GetTotalHeight sets the CardAreaHeight to available height. This overrides the
				// height that may have been explicitly set. We need to maintain the user set CardAreaHeight.
				// 
				this.InternalSetCardAreaHeight( value );
				this.explicitlySetCardAreaHeight = value;
			}
		}

		// SSP 7/20/06 BR11460
		// VisibleRow.GetTotalHeight sets the CardAreaHeight to available height. This overrides the
		// height that may have been explicitly set. We need to maintain the user set CardAreaHeight.
		// Added InternalSetCardAreaHeight. Code in there was moved from the set of the CardAreaHeight 
		// property.
		// 
		internal void InternalSetCardAreaHeight( int value )
		{
			this.cardAreaHeight = value;

			// cache the number of cards high this represents
			//
			// JM UWG1019 Compare against the actual (not potential) number
			//			  or rows in the card area.
			//this.cardAreaHeightInCards  = Math.Max( 1, ( this.band.GetTotalRowsInCardArea( value ) ) );
			this.cardAreaHeightInCards = Math.Max( 1, ( this.GetTotalActualRowsInCardArea( value ) ) );
		}

		// SSP 7/20/06 BR11460
		// VisibleRow.GetTotalHeight sets the CardAreaHeight to available height. This overrides the
		// height that may have been explicitly set. We need to maintain the user set CardAreaHeight.
		// 
		internal void ResetCardAreaHeightToExplicitlySetValue( )
		{
			if ( this.explicitlySetCardAreaHeight > 0 && this.explicitlySetCardAreaHeight != this.cardAreaHeight )
				this.InternalSetCardAreaHeight( this.explicitlySetCardAreaHeight );
		}

		#endregion // CardAreaHeight

		// MD 10/11/07 - BR27317
		// This was accidentally removed - It has been added back in
		// MD 8/3/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		//#region Not Used

		#region FirstVisibleCardRow

		// SSP 12/5/03 UWG2295
		// Added FirstVisibleCardRow property.
		//
		/// <summary>
		/// Gets or sets the first visible card in the card area associated with this collection.
		/// </summary>
		public UltraGridRow FirstVisibleCardRow
		{
		    get
		    {
		        return this.FirstVisibleCard;
		    }
		    set
		    {
		        this.FirstVisibleCard = value;

		        if ( null == this.ParentRow || this.ParentRow.AllAncestorsExpanded )
		        {
		            if ( null != this.Layout )
		                this.Layout.DirtyGridElement( );
		        }
		    }
		}

		#endregion // FirstVisibleCardRow

		//#endregion Not Used

		#region VisibleRowCount

		// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		// Added overloads that take in includeRowTypes and includeSpecialRows.
		//
		internal int GetVisibleRowCount( bool includeSpecialRows )
		{
			int visibleCount = this.SparseArray.VisibleCount;
			if ( includeSpecialRows )
				visibleCount += this.GetSpecialRowsVisibleCount( true ) + this.GetSpecialRowsVisibleCount( false );

			return visibleCount;
		}

		internal int GetVisibleRowCount( IncludeRowTypes includeRowTypes )
		{
			return this.GetVisibleRowCount( IncludeRowTypes.SpecialRows == includeRowTypes );
		}

		/// <summary>
		/// Returns the total number of visible rows in the collection. If the template add-row is visible, this property's return value will include that as well even though the template add-rows are not part of the collection, they are visible.
		/// </summary>
		public int VisibleRowCount
		{
			get
			{
				// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// Added overloads that take in includeRowTypes and includeSpecialRows.
				//
				return this.GetVisibleRowCount( true );
				

				

				
			}
		}

		#endregion // VisibleRowCount

		#region FilteredInRowCount

		// SSP 9/10/03 UWG248
		// Added FilteredInRowCount method.
		//
		/// <summary>
		/// Gets the number of rows that are filtered in (not filtered out).
		/// </summary>
		/// <remarks>
		/// <para class="body">
		/// Returns the number of rows that pass filter conditions. If this is a group-by rows collection,
		/// this property will return the number of group-by rows that are filtered in. To get the count
		/// of data rows that are filtered in, use the <see cref="FilteredInNonGroupByRowCount"/>.
		/// </para>
		/// <seealso cref="FilteredInNonGroupByRowCount"/>
		/// <seealso cref="GetFilteredInNonGroupByRows"/> 
		/// <seealso cref="GetFilteredOutNonGroupByRows"/> 
		/// </remarks>
		public int FilteredInRowCount
		{
			get
			{
				int count = 0;

				for ( int i = 0; i < this.Count; i++ )
				{
					if ( ! this[i].IsFilteredOut )
						count++;
				}

				return count;
			}
		}

		#endregion // FilteredInRowCount

		#region FilteredInNonGroupByRowCount

		// SSP 11/24/04 BR00926
		// Added FilteredInNonGroupByRowCount.
		//
		/// <summary>
		/// Gets the number of non-group-by rows that are filtered in (not filtered out).
		/// </summary>
		public int FilteredInNonGroupByRowCount
		{
			get
			{
				int count = 0;
				
				if ( ! this.IsGroupByRows )
				{
					count = this.FilteredInRowCount;
				}
				else
				{
					foreach ( UltraGridGroupByRow groupByRow in this )
						count += groupByRow.Rows.FilteredInNonGroupByRowCount;
				}

				return count;
			}
		}

		#endregion // FilteredInNonGroupByRowCount

		#region TemplateAddRow

		// SSP 11/7/03 - Add Row Feature
		// Added TemplateAddRow property.
		//
		/// <summary>
		/// Returns the template add-row associated with this rows-collection.
		/// </summary>
		/// <remarks>
		/// <p class="body">Returns the template add-row associated with this rows-collection.</p>
		/// <p class="body">Add-row functionality can be enabled using the Override's <see cref="UltraGridOverride.AllowAddNew"/> property.
		/// See <see cref="Infragistics.Win.UltraWinGrid.AllowAddNew"/> enum for more information.
		/// </p>
		/// <seealso cref="UltraGridOverride.AllowAddNew"/> <seealso cref="Infragistics.Win.UltraWinGrid.AllowAddNew"/>
		/// </remarks>
		public UltraGridRow TemplateAddRow
		{
			get
			{
				if ( null == this.templateAddRow )
				{
					// SSP 4/5/04 - Virtual Binding Related Optimizations
					//
					//this.templateAddRow = this.AllocateNewRow( -1 );
					this.templateAddRow = this.AllocateNewRow( );
					
					// Fire InitializeTemplateAddRow event.
					//
					this.FireInitializeTemplateAddRow( );
				}

				return this.templateAddRow;
			}
		}

		#endregion // TemplateAddRow

		#region TemplateAddRowInternal

		internal UltraGridRow TemplateAddRowInternal
		{
			get
			{
				return this.templateAddRow;
			}
		}

		#endregion // TemplateAddRowInternal

		#region Band
		
		/// <summary>
		/// UltraGridBand object associated with this rows collection.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Rows in this RowsCollection are from the same band as this band.
		/// </p>
		/// </remarks>
		// SSP 4/22/02 
		// Made Band property public bacuse we do want to allow the users to 
		// be able to have access to Band (for example from an event that fires
		// and they are given a RowsCollection object).
		//
		//internal UltraGridBand Band
		public UltraGridBand Band
		{
			get
			{
				return this.band;
			}
		}

		#endregion // Band

		#endregion // Public Properties

        #region Public Methods

		#region Refresh

		// SSP 10/27/03 UWG2730
		// Added Refresh overload that doesn't take in recursive parameter as requested by above bug.
		//
		/// <summary>
		/// Refresh the display and/or refetch the data non-recursively.
		/// </summary>
        /// <param name="action">The refresh action.</param>
		/// <remarks>
		/// <p class="body">Generally, painting a control is handled automatically while no events are occurring. However, there may be situations where you want the form or control updated immediately, for example, after some external event has caused a change to the form. In such a case, you would use the <b>Refresh</b> method.</p>
		/// <p class="body">The <b>Refresh</b> method can also be used to ensure that the user is viewing the latest copy of the data from the record source.</p>
		/// <p class="body">Use the overload of <b>Refresh</b> method that takes in <b>recursive</b> parameter to recursively perform this operation on descendant rows as well.</p>
		/// </remarks>
		public void Refresh( RefreshRow action )
		{
			this.Refresh( action, false );
		}

		/// <summary>
		/// Refresh the display and/or refetch the data with or without events.
		/// </summary>
        /// <param name="action">The refresh action.</param>        
        /// <param name="recursive">true to recursively refresh all descendant rows.</param>
		/// <remarks>
		/// <p class="body">Generally, painting a control is handled automatically while no events are occurring. However, there may be situations where you want the form or control updated immediately, for example, after some external event has caused a change to the form. In such a case, you would use the <b>Refresh</b> method.</p>
		/// <p class="body">The <b>Refresh</b> method can also be used to ensure that the user is viewing the latest copy of the data from the record source.</p>
		/// </remarks>
		public void Refresh( RefreshRow action, bool recursive )
		{
			this.Refresh( action, recursive, false );
		}

		// JJD 1/21/02 - UWG851 - added refresh method
		/// <summary>
		/// Refresh the display and/or refetch the data with or without events.
		/// </summary>
        /// <param name="action">The refresh action.</param>
        /// <param name="initializeTemplateAddRow">true to force the Initialization of TemplateAddRows (in addition to normal rows).</param>
        /// <param name="recursive">true to recursively refresh all descendant rows.</param>
		/// <remarks>
		/// <p class="body">Generally, painting a control is handled automatically while no events are occurring. However, there may be situations where you want the form or control updated immediately, for example, after some external event has caused a change to the form. In such a case, you would use the <b>Refresh</b> method.</p>
		/// <p class="body">The <b>Refresh</b> method can also be used to ensure that the user is viewing the latest copy of the data from the record source.</p>
		/// </remarks>
		// SSP 1/6/04 UWG2811
		// Added an overload that takes in initializeTemplateAddRow parameter.
		//
		//public void Refresh( RefreshRow action, bool recursive )
		public void Refresh( RefreshRow action, bool recursive, bool initializeTemplateAddRow )
		{
			// SSP 2/1/02 UWG1017
			// If we are bound to a non-IBindingList, then check the count
			// of the rows to the count of the bound list, and if they are
			// different, then sync the rows. Because when elements are
			// added or deleted from the IList, we don't get any messages.
			// 
			// SSP 6/4/02
			//
			//this.verifyVersion++;
			this.BumpVerifyVersion( );
			if ( !this.IsBoundToBindingList )
			{
				IList list = this.Data_List;

				if ( null != list && list.Count != this.Count )
					this.SyncRows( );
			}


			// SSP 8/18/04
			// This method should work for UltraCombo and UltraDropDown as well.
			//
			//UltraGridBase grid = this.band == null ? null : this.band.Layout.Grid as UltraGrid;
			UltraGridBase grid = null != this.Layout ? this.Layout.Grid : null;

			// This is only valid for the grid
			if ( grid == null )
				return;

			Control control = grid.ControlForGridDisplay;

			if ( control == null && !control.Created )
				return;

			switch ( action )
			{
				case Infragistics.Win.UltraWinGrid.RefreshRow.RefreshDisplay:
					// If all they want to do is refresh the display then
					// just do an invalidate below
					//
					break;
        
				case Infragistics.Win.UltraWinGrid.RefreshRow.FireInitializeRow:

					for (int i = 0; i < this.Count; i++ )
					{
						this[i].Refresh( action, recursive );
					}				

					break;

					// SSP 11/25/02 UWG1684
					// Added new ReloadData member to RefreshRow enum and implemented
					// necessary code
				case Infragistics.Win.UltraWinGrid.RefreshRow.ReloadData:
				
					if ( this.IsTopLevel )
					{
						if ( this.Band.BindingManager is CurrencyManager )
						{
							CurrencyManager cm = (CurrencyManager)this.Band.BindingManager;

							cm.Refresh( );
						}

                        // MBS 11/4/08 - TFS9064
                        // Reset the cached list on the band so that we can re-create it
                        this.Band.ResetCachedList();

                        // MBS 2/17/09 - TFS13089
                        // We should call the method that also bumps the verification versions
                        //
						//this.rowsDirty = true;
                        this.DirtyRows();

						if ( recursive )
						{
							for (int i = 0; i < this.Count; i++ )
							{
								this[i].Refresh( action, recursive );
							}
						}
					}
					else
						this.NotifyCalcManager_RowsCollectionResynced( );

					break;
			}

			// SSP 1/6/04 UWG2811
			// Added an overload that takes in initializeTemplateAddRow parameter.
			//
			if ( initializeTemplateAddRow )
			{
				// If the template add-row has already been created then fire the 
				// InitializeTemplateAddRow here.
				// 
				if ( null != this.templateAddRow )
				{
					this.FireInitializeTemplateAddRow( );
				}
					// Otherwise access the TemplateAddRow property which will fire the
					// InitializeTemplateAddRow.
					//
				else if ( null != this.TemplateAddRow ) 
				{
					// Do nothing.
				}
			}

			// SSP 1/20/04
			// Bump the cell child elements version number.
			//
			// ----------------------------------------------------------------
			if ( null != this.Layout )
			{
				this.Layout.DirtyGridElement( );
				this.Layout.BumpCellChildElementsCacheVersion( );
			}
			// ----------------------------------------------------------------

			// invalidate the control and call update to force a paint
			//
			// SSP 3/26/04 UWG3123
			// Check the IsUpdating flag before invalidating and updating.
			//
			if ( null == grid || ! grid.IsUpdating )
			{
				control.Invalidate();

				// SSP 2/19/04 - Optimization
				// Why are we updating the control right away. Especially if the recursive
				// parameter is true and we have many child rows collections. In that case
				// we would end up painting the grid a lot of times.
				// Commented out the call to Update below. Invalidating should be enough.
				//
				//control.Update();
			}

			// SSP 12/6/04 - Merged Cell Feature
			// Clear the merged cell cache. This is not necessary however Refresh should 
			// probably do it.
			//
			this.ClearMergedCellCache( );
		}

		#endregion // Refresh

		#region ExpandAll

		/// <summary>
		/// Expand all rows in the collection. <see cref="UltraGrid.InitializeLayout"/>
		/// </summary>
		/// <param name="recursive">Specifies whether to recursively expand descendant rows as well.</param>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.CollapseAll(bool)"/>
		public void ExpandAll( bool recursive )
		{
			// SSP 10/23/03 UWG2334
			// Expanding or collapsing a row could lead to the cell in edit mode being scrolled 
			// out of view which we don't want happen. So exit the edit mode and if that fails 
			// then return without expanding any rows.
			//
			if ( null != this.Layout && ! this.Layout.ExitEditModeHelper( ) )
				return;

			//RobA UWG466 10/1/01 implemented method
			//

			// SSP 4/27/04 - Virtual Binding - Optmizations
			// If this collection is lowest leve rows collection, that is the rows contained
			// in this collection do not contain descendant rows then simply return.
			// 
			// ------------------------------------------------------------------------------
			bool rowsHaveChildRows = null == this.Band || this.Band.HasChildBands( false ) || this.IsGroupByRows;
			if ( ! rowsHaveChildRows )
				return;
			// ------------------------------------------------------------------------------

			//Loop over collection, expanding each row
			//
			for ( int i = 0; i < this.Count; ++i )
			{
				// SSP 12/21/04 BR00074 / Optimization
				//
				// ------------------------------------------------------------------------------
				
				UltraGridRow row = this[i];
				row.Expanded = true;

				if ( recursive )
				{
					ChildBandsCollection childBands = row.ChildBands;
					if ( null != childBands )
					{
						for ( int j = 0; j < childBands.Count; j++ )
							childBands[j].Rows.ExpandAll( recursive );
					}
				}
				// ------------------------------------------------------------------------------
			}
		}

		#endregion // ExpandAll

		#region CollapseAll

		/// <summary>
		/// Collapse all rows in the collection.
		/// </summary>
		/// <param name="recursive"></param>
		/// <remarks>
		/// <p>
		/// The following sample code collapses all the rows in ultraGrid1.
		/// </p>
		/// <p></p>
		/// <p>C#:</p>
		/// <p></p>
		/// <pre>
		/// private void button1_Click(object sender, System.EventArgs e)
		/// {
		/// 	// Collapse all the rows recursively.
		/// 	//
		/// 	this.ultraGrid1.Rows.CollapseAll( true );
		/// }
		/// </pre>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.ExpandAll"/>
		public void CollapseAll( bool recursive )
		{
			// SSP 10/23/03 UWG2334
			// Expanding or collapsing a row could lead to the cell in edit mode being scrolled 
			// out of view which we don't want happen. So exit the edit mode and if that fails 
			// then return without expanding any rows.
			//
			if ( null != this.Layout && ! this.Layout.ExitEditModeHelper( ) )
				return;

			//RobA UWG466 10/1/01 implemented method
			//

			//Loop over collection, collapsing each row
			//
			for ( int i = 0; i < this.Count; ++i )
			{
				// first collapse the child rows
				//
				if ( recursive && this[i].HasChildRows )
				{
					foreach ( UltraGridChildBand childBand in this[i].ChildBands )
					{
						childBand.Rows.CollapseAll( recursive );
					}
				}
		
				// then collapse this row
				//
				this[i].Expanded = false;
			}
		}	

		#endregion // CollapseAll

		#region GetRelatedVisibleCard

		internal UltraGridRow GetRelatedVisibleCard( UltraGridRow row, NavigateType navType, bool includeNonActivateable )
		{
			if ( row == null )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_241") );

			if ( !row.IsCard )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_242"), Shared.SR.GetString("LE_ArgumentNullException_241") );

			if ( row.ParentCollection != this )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentNullException_245"), Shared.SR.GetString("LE_ArgumentNullException_241") );

			// get the card area element associated with this collection
			//
			// SSP 7/2/02
			// Compressed card view. We are calling GetCardAreaElement passing in true
			// for the scrollIntoView (3rd param) which scroll the whole card into
			// view if it's partially visible. However we don't want to do that if the
			// item we are looking for in accordance with the navType specified is already
			// in view. So only scroll if item is not found.
			// First call GetCardAreaElement with scrollIntoView as false.
			//
			//CardAreaUIElement cardAreaElement = this.GetCardAreaElement( null, null, true, false );
			CardAreaUIElement cardAreaElement = this.GetCardAreaElement( null, null, false, false );

			UltraGridRow item = null;
			if ( cardAreaElement != null )
				item = cardAreaElement.GetRelatedVisibleCard( row, navType, includeNonActivateable );

			// SSP 7/2/02
			// Related to the change above.
			// Now if we did not get the item, then scroll the card into view and then try again.
			//
			if ( null == item )
			{
				cardAreaElement = this.GetCardAreaElement( null, null, false, false );

				if ( cardAreaElement != null )
					item = cardAreaElement.GetRelatedVisibleCard( row, navType, includeNonActivateable );
			}

			return item;
		}

		#endregion

		#region ScrollCardIntoView

		/// <summary>
		/// Scrolls passed in row into view
		/// </summary>
		/// <param name="row">The card row.</param>
		public void ScrollCardIntoView( UltraGridRow row )
		{
			this.ScrollCardIntoView( row, null, null, true );
		}

		/// <summary>
		/// Scrolls passed in row into view
		/// </summary>
		/// <param name="row">The card row.</param>
		/// <param name="csr">The column scrolling region (if null will use the active region).</param>
		public void ScrollCardIntoView( UltraGridRow row, ColScrollRegion csr )
		{
			this.ScrollCardIntoView( row, null, csr, true );
		}

		/// <summary>
		/// Scrolls passed in row into view
		/// </summary>
		/// <param name="row">The card row.</param>
		/// <param name="rsr">The row scrolling region (if null will use the active region).</param>
		public void ScrollCardIntoView( UltraGridRow row, RowScrollRegion rsr )
		{
			this.ScrollCardIntoView( row, rsr, null, true );
		}

		/// <summary>
		/// Scrolls passed in row into view
		/// </summary>
		/// <param name="row">The card row.</param>
		/// <param name="rsr">The row scrolling region (if null will use the active region).</param>
		/// <param name="csr">The column scrolling region (if null will use the active region).</param>
		/// <param name="exitEditMode">If true will exit edit mode first.</param>
		public void ScrollCardIntoView( UltraGridRow row, 
			RowScrollRegion rsr, 
			ColScrollRegion csr,
			bool exitEditMode )
		{
			if ( row == null )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_241") );

			if ( !row.IsCard )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_242"), Shared.SR.GetString("LE_ArgumentNullException_241") );

			if ( row.ParentCollection != this )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentNullException_245"), Shared.SR.GetString("LE_ArgumentNullException_241") );

			// get the card area element associated with this collection
			//
			// SSP 7/2/02
			// Compressed card view. As the name of this method suggests, we are trying to
			// scroll a particular card into view and not the whole card area. So pass
			// in the row we are scrolling into the new overload of GetCardAreaElement so
			// that i only scrolls only the row and not the whole card area.
			//
			//CardAreaUIElement cardAreaElement = this.GetCardAreaElement( rsr, csr, true, exitEditMode );
			CardAreaUIElement cardAreaElement = this.GetCardAreaElement( rsr, csr, true, row, exitEditMode );

			if ( cardAreaElement != null )
			{
				cardAreaElement.ScrollCardIntoView( row, exitEditMode );

				// force a verification to reposition the elements
				//
				cardAreaElement.Parent.VerifyChildElements( true );
			}
		}
		
		#endregion

		#region IsCardVisible

		/// <summary>
		/// Determines if passed in row is visible
		/// </summary>
		/// <param name="row">The card row.</param>
		/// <returns>True if the this row is visible.</returns>
		public bool IsCardVisible( UltraGridRow row )
		{
			return this.IsCardVisible( row, null, null );
		}

		/// <summary>
		/// Determines if passed in row is visible
		/// </summary>
		/// <param name="row">The card row.</param>
		/// <param name="csr">The column scrolling region (if null will use the active region).</param>
		/// <returns>True if the this row is visible.</returns>
		public bool IsCardVisible( UltraGridRow row, ColScrollRegion csr )
		{
			return this.IsCardVisible( row, null, csr );
		}

		/// <summary>
		/// Determines if passed in row is visible
		/// </summary>
		/// <param name="row">The card row.</param>
		/// <param name="rsr">The row scrolling region (if null will use the active region).</param>
		/// <returns>True if the this row is visible.</returns>
		public bool IsCardVisible( UltraGridRow row, RowScrollRegion rsr )
		{
			return this.IsCardVisible( row, rsr, null );
		}

		/// <summary>
		/// Determines if passed in row is visible
		/// </summary>
		/// <param name="row">The card row.</param>
		/// <param name="rsr">The row scrolling region (if null will use the active region).</param>
		/// <param name="csr">The column scrolling region (if null will use the active region).</param>
		/// <returns>True if the this row is visible.</returns>
		public bool IsCardVisible( UltraGridRow row, RowScrollRegion rsr, ColScrollRegion csr )
		{
			if ( !row.IsCard )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_242"), Shared.SR.GetString("LE_ArgumentNullException_241") );

			if ( row.ParentCollection != this )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentNullException_245"), Shared.SR.GetString("LE_ArgumentNullException_241") );

			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( grid == null )
				return false;

			if ( rsr == null )
				rsr = grid.ActiveRowScrollRegion;

			if ( csr == null )
				csr = grid.ActiveColScrollRegion;

			// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			// Split the RowUIElement into RowUIElementBase class and RowUIElement class so the filter
			// row element can derive from RowUIElementBase.
			//
			//UIElement element = this.Layout.UIElement.GetDescendant( typeof( RowUIElement ), new object[] { rsr, csr, row } );
			UIElement element = this.Layout.UIElement.GetDescendant( typeof( RowUIElementBase ), new object[] { rsr, csr, row } );

			return element != null;
		}

		#endregion // IsCardVisible

		#region GetRowWithListIndex

		/// <summary>
		/// Returns the actual UltraGridRow associated with list index in the binding list.
		/// </summary>
        /// <param name="listIndex">The index of the row in the binding list.</param>
		// SSP 8/9/02
		// Made this public due to a customer request.
		//
		//internal UltraGridRow GetRowWithListIndex( int listIndex )
		public UltraGridRow GetRowWithListIndex( int listIndex )
		{
            // MRS v7.2 - PDF Report Writer
            // Added an overload with a 'create' param
			//return this.GetRowWithListIndex( listIndex, false );
            return this.GetRowWithListIndex(listIndex, true);
		}

        /// <summary>
        /// Returns the actual UltraGridRow associated with list index in the binding list.
        /// </summary>
        /// <param name="listIndex">The index of the row in the binding list.</param>
        /// <param name="create">Specifies true to indicate that if the row is not already created, it should be created now. Specify false to indicate that if the row is not already created, the creation of the row should not be forced.</param>
        public UltraGridRow GetRowWithListIndex(int listIndex, bool create)
        {
            // MRS v7.2 - PDF Report Writer
            // Added an overload with a 'create' param
            //return this.GetRowWithListIndex( listIndex, false );
            return this.GetRowWithListIndexHelper(listIndex, false, create);
        }

		#endregion // GetRowWithListIndex

		#region GetRowAtVisibleIndex

		/// <summary>
		/// Gets a row based on its visible index.
		/// </summary>
		/// <param name="visibleIndex">The index relative to all other visible rows.</param>
		/// <returns>The row at the requested visible index. Row's that have their Hidden property set will not be included.</returns>
		public UltraGridRow GetRowAtVisibleIndex( int visibleIndex )
		{
			return this.GetRowAtVisibleIndex( visibleIndex, true );
		}
		
		internal UltraGridRow GetRowAtVisibleIndex( int visibleIndex, IncludeRowTypes includeRowTypes )
		{
			return this.GetRowAtVisibleIndex( visibleIndex, IncludeRowTypes.SpecialRows == includeRowTypes );
		}
		
		internal UltraGridRow GetRowAtVisibleIndex( int visibleIndex, bool includeSpecialRows )
		{
			// SSP 4/14/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// We have to take into account filter row, summary row etc...
			// Commented out the original code and added the new code below.
			//
			// --------------------------------------------------------------------------------------
			if ( visibleIndex < 0 )
				return null;

			UltraGridRow row = null;
			if ( includeSpecialRows )
			{
				row = this.GetSpecialRowAtVisibleIndex( visibleIndex, true );
				if ( null == row )
				{
					visibleIndex -= this.GetSpecialRowsVisibleCount( true );
					row = this.SparseArray.GetItemAtVisibleIndex( visibleIndex );

					if ( null == row )
					{
						visibleIndex -= this.SparseArray.VisibleCount;
						row = this.GetSpecialRowAtVisibleIndex( visibleIndex, false );
					}
				}
			}
			else
			{
				row = this.SparseArray.GetItemAtVisibleIndex( visibleIndex );
			}

			return row;
			
			// --------------------------------------------------------------------------------------
		}

		#endregion // GetRowAtVisibleIndex

		#region GetRowAtVisibleIndexOffset

		/// <summary>
		/// Gets a row based on a start row and a relative visible index.
		/// </summary>
		/// <param name="startAtRow">The row to start with. If this is null the first visible row in the collection will be used.</param>
		/// <param name="visibleIndexOffset">The index offset from the startAtRow relative to all other visible rows.</param>
		/// <returns>The row at the requested visible index offset. Row's that have their Hidden property set will not be included.</returns>
		public UltraGridRow GetRowAtVisibleIndexOffset( UltraGridRow startAtRow, int visibleIndexOffset )
		{
			return this.GetRowAtVisibleIndexOffset( startAtRow, visibleIndexOffset, IncludeRowTypes.SpecialRows );
		}

		// SSP 5/10/05 BR03761
		// Added an overload of GetRowAtVisibleIndexOffset that takes in includeRowTypes parameter.
		//	
		internal UltraGridRow GetRowAtVisibleIndexOffset( UltraGridRow startAtRow, int visibleIndexOffset, IncludeRowTypes includeRowTypes )
		{
			// SSP 11/14/03 Add Row Feature
			// Template add-row Index will be negative.
			//
			//if ( null != startAtRow && startAtRow.Index < 0 )
			// SSP 4/14/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// We have to take into account filter row, summary row etc...
			//
			//if ( null != startAtRow && ( startAtRow.Index < 0 && ! startAtRow.IsTemplateAddRow ) )
			if ( null != startAtRow && ( startAtRow.Index < 0 && startAtRow.IsDataRow ) )
				// SSP 4/30/04 - Virtual Binding
				// Instead of throwing an exception when the specified startAtRow is invalid,
				// return null.
				//
				//throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_246"));
				return null;

			// SSP 5/10/05 BR03761
			// Added an overload of GetRowAtVisibleIndexOffset that takes in includeRowTypes parameter.
			//	
			// ----------------------------------------------------------------------------------------
            if ( IncludeRowTypes.DataRowsOnly == includeRowTypes )
				return this.SparseArray.GetItemAtVisibleIndexOffset( startAtRow, visibleIndexOffset );
			// ----------------------------------------------------------------------------------------

			// SSP 4/9/03 UWG2186
			// We need to optimize this method because the card-view makes use of this method and it
			// crawls to a halt if there are lots of rows. (The test project for this bug had over
			// 34000 rows and tracking the thumb a few times led to a halt and overall the grid
			// was very unresponsive in its maximized state).
			// Commented out existing code and added this block of code.
			//
			// --------------------------------------------------------------------------------------
			if ( null == startAtRow )
			{
				return this.GetRowAtVisibleIndex( visibleIndexOffset );
			}
			else
			{
				UltraGridRow row = this.SparseArray.GetItemAtVisibleIndexOffset( startAtRow, visibleIndexOffset );
				if ( null != row )
					return row;

				int visibleIndex = this.GetVisibleIndex( startAtRow, true );
				if ( visibleIndex >= 0 )
				{
					return this.GetRowAtVisibleIndex( visibleIndex + visibleIndexOffset );
				}
				else
				{
					// SSP 4/14/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
					// We have to take into account filter row, summary row etc...
					// Commented out the original code and added the new code below.
					//
					// --------------------------------------------------------------------------------------
					if ( visibleIndexOffset >= 0 )
					{
						int count = this.CountIncludingSpecialRows;
						bool startAtRowEncountered = false;
						int tmpVisibleIndex = -1;
						for ( int i = 0; i < count; i++ )
						{
							row = this.GetRowAtIndex( i, true );
							if ( row == startAtRow )
								startAtRowEncountered = true;

							if ( startAtRowEncountered && ! row.HiddenInternal )
							{
								tmpVisibleIndex++;
								if ( tmpVisibleIndex == visibleIndexOffset )
									return row;
							}
						}
					}
					else
					{
						int count = this.CountIncludingSpecialRows;
						bool startAtRowEncountered = false;
						int tmpVisibleIndex = 1;
						for ( int i = count - 1; i >= 0; i-- )
						{
							row = this.GetRowAtIndex( i, true );
							if ( row == startAtRow )
								startAtRowEncountered = true;

							if ( startAtRowEncountered && ! row.HiddenInternal )
							{
								tmpVisibleIndex--;
								if ( tmpVisibleIndex == visibleIndexOffset )
									return row;
							}
						}
					}
					
					// --------------------------------------------------------------------------------------
				}
				
				return null;
			}
			// --------------------------------------------------------------------------------------

			
		}

		#endregion // GetRowAtVisibleIndexOffset

		#region GetEnumerator

		/// <summary>
		/// IEnumerable Interface Implementation
		/// </summary>
        /// <returns>A type safe enumerator</returns>
		public RowEnumerator GetEnumerator() // non-IEnumerable version
		{	
			this.EnsureNotDirty( );
           
			return new RowEnumerator(this);
		}

		#endregion // GetEnumerator

		// SSP 11/19/04 BR00655
		// Added GetAllNonGroupByRows, GetFilteredInNonGroupByRows and GetFilteredOutNonGroupByRows methods.
		//
		#region GetAllNonGroupByRows

		/// <summary>
		/// Gets all the non-group-by rows that are associated with this collection.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This method is useful in group-by mode where rows are grouped by columns and you want to get 
		/// all the non-group-by rows. In group-by mode the root row collection contains the group-by rows. 
		/// To access the actual non-group-by rows one has to recursively traverse the child rows (by 
		/// using the <see cref="UltraGridRow.ChildBands"/> property or by typecasting the row into an 
		/// <b>UltraGridGroupByRow</b> object and accessing it's <see cref="UltraGridGroupByRow.Rows"/> 
		/// property). This method makes accessing all the non-group-by rows easier by returning an 
		/// array that contains all non-group-by rows.
		/// </p>
		/// </remarks>
		/// <returns>An array that contains the non-group-by rows (actual rows). The returned array is not held by the row collection.</returns>
		public UltraGridRow[] GetAllNonGroupByRows( )
		{
			this.EnsureNotDirty( );

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<UltraGridRow> list = new List<UltraGridRow>();

			this.GetAllNonGroupByRows( list );

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (UltraGridRow[])list.ToArray( typeof( UltraGridRow ) );
			return list.ToArray();
		}

		#endregion // GetAllNonGroupByRows

		#region GetFilteredInNonGroupByRows

		/// <summary>
		/// Gets all the filtered in rows (rows that are not filtered out).
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Gets all the filtered in rows (rows that are not filtered out). As the name suggests this method 
		/// does not return group-by rows even when this collection is a collection of group-by rows. It 
		/// always returns an array of actual (non-group-by) rows that are filtered in. In the case where
		/// this collection contains group-by rows it returns the descendant filtered in non-group-by rows.
		/// </p>
		/// <seealso cref="GetFilteredOutNonGroupByRows"/> <seealso cref="GetAllNonGroupByRows()"/>
		/// </remarks>
		/// <returns></returns>
		public UltraGridRow[] GetFilteredInNonGroupByRows( )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<UltraGridRow> list = new List<UltraGridRow>();

			this.GetFilteredRows( list, false );

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (UltraGridRow[])list.ToArray( typeof( UltraGridRow ) );;
			return list.ToArray();
		}

		#endregion // GetFilteredInNonGroupByRows

		#region GetFilteredOutNonGroupByRows

		/// <summary>
		/// Gets all the filtered out rows (rows that do not match the filtering criteria).
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Gets all the filtered out rows (rows that do not match the filtering criteria). As the name 
		/// suggests this method does not return group-by rows even when this collection is a collection 
		/// of group-by rows. It always returns an array of actual (non-group-by) rows that are filtered 
		/// out. In the case where this collection contains group-by rows it returns the descendant
		/// filtered out non-group-by rows.
		/// </p>
		/// <seealso cref="GetFilteredInNonGroupByRows"/> <seealso cref="GetAllNonGroupByRows()"/>
		/// </remarks>
		/// <returns></returns>
		public UltraGridRow[] GetFilteredOutNonGroupByRows( )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<UltraGridRow> list = new List<UltraGridRow>();

			this.GetFilteredRows( list, true );

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (UltraGridRow[])list.ToArray( typeof( UltraGridRow ) );;
			return list.ToArray();
		}

		#endregion // GetFilteredOutNonGroupByRows

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo

		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo(Infragistics.Win.UltraWinGrid.UltraGridRow [] array, int index)
		{
			this.EnsureNotDirty( );

			// SSP 4/27/04 - Virtual Binding Related
			//
			//base.CopyTo( (System.Array)array, index );
			this.CopyTo( (System.Array)array, index );
		}

		#endregion //CopyTo

		// JDN 11/16/04 Added Synchronous Sorting and Filtering
        #region EnsureSortedAndFiltered

		// SSP 2/4/05
		// Added EnsureSortedAndFilteredHelper helper method that takes in prcoess mode and a band.
		//
		internal void EnsureSortedAndFilteredHelper( ProcessMode processMode, UltraGridBand lowestLevelBand )
		{
			if ( ProcessMode.Synchronous == processMode || ProcessMode.SynchronousExpanded == processMode )
			{
				this.EnsureSortedAndFiltered( 
					ProcessMode.Synchronous == processMode ? RecursionType.All : RecursionType.Expanded,
					lowestLevelBand );
			}
		}

		/// <summary>
		/// Ensure Rows in the current RowCollection are sorted and filtered
		/// </summary>
		public void EnsureSortedAndFiltered()
		{
			// Just ensure that this RowCollection is sorted and filtered
			this.EnsureSortedAndFiltered( RecursionType.None );
		}

		/// <summary>
		/// Ensure Rows in the RowCollection are sorted and filtered.
		/// </summary>
		/// <param name="recursion">RecursionType enum specifying whether the method should be applied recursively
		/// to child bands of the current RowCollection's Band</param>
		/// <returns>void.</returns>
		/// <remarks>Specifying a RecursionType of "None" will ensure the current RowCollection is synchronously sorted
		/// and filtered.  Specifying "All" or "Expanded" will ensure that either all rows, or all expanded rows in child
		/// bands are synchronously sorted and filtered.</remarks>
		public void EnsureSortedAndFiltered( RecursionType recursion )
		{
			// If recursion is "None" just ensure that this RowCollection is sorted and filtered
			// otherwise apply the RecursionType to all bands below this band
			this.EnsureSortedAndFiltered( recursion, null );
		}

		/// <summary>
		/// Ensure Rows in the RowCollection are sorted and filtered.
		/// </summary>
		/// <param name="recursion">RecursionType enum specifying whether the method should be recursively applied
		/// to child bands of the current RowCollection's Band.</param>
		/// <param name="lowestLevelBand">The lowest level Band to which the method should be recursively applied.</param>
		/// <returns>void.</returns>
		/// <remarks>Specifying a RecursionType of "None" will ensure the current RowCollection is synchronously sorted
		/// and filtered.  Specifying "All" or "Expanded" will ensure that either all rows, or all expanded rows, in the 
		/// specified Bands are synchronously sorted and filtered.  The method will be called recursively on the
		/// this Band and all child bands until the lowest level Band is reached or all bands if the lowestLevelBand is null. 
		/// </remarks>
		public void EnsureSortedAndFiltered( RecursionType recursion, UltraGridBand lowestLevelBand )
		{
			if ( this.Band == null || this.Band.Layout == null )
				return;

			// Ensure that this RowCollection is sorted and filtered
			this.EnsureNotDirty();
			this.EnsureFiltersEvaluated();

            // MRS 2/4/2008 - BR30242
            // If we are in GroupBy mode, then it's always recursive, so do that check separately. 
            //
            //// Just return if no recursion, or we have reached a specified lowest level band, or there are no child bands
            //if ( recursion == RecursionType.None ||
            //    lowestLevelBand != null && this.Band == lowestLevelBand && !this.IsGroupByRows ||
            //    !this.Band.HasChildBands(false) )
            //    return;   
            // Just return if no recursion, or we have reached a specified lowest level band, or there are no child bands
            if (recursion == RecursionType.None ||
                !this.IsGroupByRows &&
                (this.Band == lowestLevelBand ||
                !this.Band.HasChildBands(false)))
                return;                  

			// Recursively call method if we have child bands
			// call method on all rows or just expanded rows based on RecursionType
			foreach ( UltraGridRow row in this )
			{ 
				if ( row.ChildBands != null && (recursion != RecursionType.Expanded || row.Expanded))
				{
					foreach ( UltraGridChildBand childBand in row.ChildBands )
					{
						if ( lowestLevelBand == null || lowestLevelBand.IsTrivialDescendantOf(childBand.Band) )
							childBand.Rows.EnsureSortedAndFiltered( recursion, lowestLevelBand );                              
					}
				}
			}
		} 

        #endregion // EnsureSortedAndFiltered

		#region DeallocateCells

		// SSP 8/26/06 - NAS 6.3
		// Added ClearCells method.
		// 
		/// <summary>
		/// Releases allocated cell objects. Cells are allocated lazily as they are accessed.
		/// This method is for releasing cells that have been allocated so for.
		/// </summary>
		/// <param name="recursive">If true then clears the cells of the descendant rows as well.</param>
		/// <remarks>
		/// <p class="body">
		/// Cells are lazily allocated. They are allocated as they are accessed. However once a cell
		/// is allocated, it remains referenced until the row is removed. You can use this method to 
		/// release references to the cells that have been allocated. Note that they will be re-allocated
		/// when they are accessed next time.
		/// </p>
		/// <p class="body">
		/// One use for this method is to reset all the cells to their default settings. For example,
		/// if you have set some settings on the cells of a row and you want to clear all those settings
		/// then you can use this method to simply clear the cell collection. This will have the same
		/// effect as having reset all the cells of the row with one added benefit that the references
		/// to the cells will have been released and thus may result in improved memory usage. Note
		/// that the cells will be re-allocated when they are accessed the next time. This method is 
		/// exposed on the <see cref="UltraGridRow"/> as well so this operation can be performed on 
		/// an individual row as well.
		/// </p>
		/// <p class="body">
		/// <b>Note:</b> If one of the cells of this row is the current active cell or is selected then
		/// it will not be cleared from the collection as the ActiveCell property of the grid (or
		/// the SelectedCellsCollection in the case of selected cells) is holding reference to the
		/// cell. Such cells will not be cleared. They will remain part of the cell collection. However
		/// their settings will be reset. In other words, the end result will be the same.
		/// </p>
		/// <p class="body">
		/// Note that any references to the cells from this row after ClearCells call should be considered
		/// invalid (except for the active cell and selected cells as noted above). Also the cell collection 
		/// itself is released, which is also lazily allocated.
		/// </p>
		/// </remarks>
		public void DeallocateCells( bool recursive )
		{
			this.DeallocateCellsHelper( this.Layout.ActiveCell, false, recursive );
		}

		internal void DeallocateCellsHelper( UltraGridCell activeCell, bool retainUnboundData, bool recursive )
		{
			foreach ( UltraGridRow row in this.NonNullItems )
			{
				row.DeallocateCellsHelper( activeCell, retainUnboundData, recursive );
			}
		}

		#endregion // DeallocateCells

		#endregion // Public Methods

		#region Private/Internal Methods

		#region CreateEmptyRowsCollection

		// SSP 9/5/03 UWG2634
		// this.Bands property verifies if the data source has been loaded. That could end up
		// accessing the rows collection recursively while we are trying to allocate the rows
		// collection. We don't want that to happen.
		// Added CreateEmptyRowsCollection static method.
		//
		internal static RowsCollection CreateEmptyRowsCollection( )
		{
			return new RowsCollection( );
		}

		#endregion // CreateEmptyRowsCollection

		#region Initialize

		internal void Initialize(  Infragistics.Win.UltraWinGrid.UltraGridBand band, 
			Infragistics.Win.UltraWinGrid.UltraGridRow parentRow, UltraGridColumn groupByColumn )
		{
			if ( null == band )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_238"));
						
			this.band       = band;
			this.parentRow  = parentRow;

			// SSP 2/20/04
			// groupByColumn member variable is never used anywhere. Commenting it out.
			//
			//this.groupByColumn = groupByColumn;

			// SSP 5/4/04 - Virtual Binding
			// Commented out following code. We are lazily binding to the list so there is
			// no need to do it here.
			//
			
		}

		#endregion // Initialize

		#region EnsureSparseArrayCreated

		private void EnsureSparseArrayCreated( )
		{
			if ( null == this.sparseArray )
				this.sparseArray = new ScrollCountManagerSparseArray( this );
		}

		#endregion // EnsureSparseArrayCreated

		#region GetNextVisibleRow
		
		internal UltraGridRow GetNextVisibleRow( UltraGridRow row )
		{
			return this.GetNextVisibleRow( row, IncludeRowTypes.SpecialRows );
		}
		
		// SSP 5/10/05 BR03761
		// Added overloads of GetNextVisibleRow and GetPrevVisibleRow that take in 
		// includeRowTypes parameter.
		//
		internal UltraGridRow GetNextVisibleRow( UltraGridRow row, IncludeRowTypes includeRowTypes )
		{
			return this.GetRowAtVisibleIndexOffset( row, row.HiddenInternal ? 0 : 1, includeRowTypes );
		}

		#endregion // GetNextVisibleRow

		#region GetPrevVisibleRow
		
		internal UltraGridRow GetPrevVisibleRow( UltraGridRow row )
		{
			return this.GetPrevVisibleRow( row, IncludeRowTypes.SpecialRows );
		}

		// SSP 5/10/05 BR03761
		// Added overloads of GetNextVisibleRow and GetPrevVisibleRow that take in 
		// includeRowTypes parameter.
		//		
		internal UltraGridRow GetPrevVisibleRow( UltraGridRow row, IncludeRowTypes includeRowTypes )
		{
			return this.GetRowAtVisibleIndexOffset( row, -1, includeRowTypes );
		}

		#endregion // GetPrevVisibleRow

		#region EnsureNotDirty

		internal void EnsureNotDirty( )
		{
			// SSP 11/11/05 BR07435
			// Make sure the top level row collection is in sync.
			// 
			if ( ! this.IsTopLevel )
				this.TopLevelRowsCollection.EnsureNotDirty( );

			if ( this.rowsDirty )
				this.SyncRows( );

			this.VerifyGroupByVersion( );

			this.EnsureSortOrder( );

			// SSP 3/30/05 - NAS 5.2 Fixed Rows
			//
			if ( null != this.fixedRowCollection )
				this.fixedRowCollection.VerifyFixedRows( );
		}

		#endregion // EnsureNotDirty

		#region InternalTraverseRowsHelper
		
		// SSP 5/7/03 - Excel Exporting Functionality
		// Added InternalTraverseRowsHelper method.
		//

		internal bool InternalTraverseRowsHelper( IUltraGridExporter exporter )
		{
			// Skip hidden bands.
			//
			if ( this.Band.HiddenResolved )
				return true;

			// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
			// Export summary rows.
			//
			//for ( int i = 0; i < this.Count; i++ )
			for ( int i = 0, count = this.CountIncludingSpecialRows; i < count; i++ )
			{
				// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
				// Export summary rows.
				//
				//UltraGridRow row = this[i];
				UltraGridRow row = this.GetRowAtIndex( i, true );
				if ( null == row )
					continue;

				ProcessRowParams processRowParams = new ProcessRowParams( );

				exporter.ProcessRow( row, processRowParams );
				
				if ( processRowParams.TerminateExport )
					// Return false to stop further traversal
					//
					return false;

				if ( ! processRowParams.SkipDescendants && null != row.ChildBands )
				{
					for ( int j = 0; j < row.ChildBands.Count; j++ )
					{
						UltraGridChildBand childBand = row.ChildBands[j];

						// Skip hidden bands.
						//
						if ( childBand.Band.HiddenResolved )
							continue;

						if ( ! childBand.Rows.InternalTraverseRowsHelper( exporter ) )
							// Return false to stop further traversal
							//
							return false;
					}
				}

				if ( processRowParams.SkipSiblings )
					break;
			}

			return true;
		}

		// SSP 11/20/03 UWG2741
		// Added following InternalTraverseRowsHelper overload.
		//
		internal bool InternalTraverseRowsHelper( IRowCallback rowCallback, bool recursive, bool onlyAllocatedRows )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridRow row = this[i];

				if ( ! rowCallback.ProcessRow( row ) )
					// Return false to stop further traversal
					//
					return false;
				
				if ( recursive && ( ! onlyAllocatedRows || null != row.childBands ) && null != row.ChildBands )
				{
					for ( int j = 0; j < row.ChildBands.Count; j++ )
					{
						UltraGridChildBand childBand = row.ChildBands[j];

						if ( ( ! onlyAllocatedRows || childBand.HaveRowsBeenInitialized ) && null != childBand.Rows )
						{
							if ( ! childBand.Rows.InternalTraverseRowsHelper( rowCallback, recursive, onlyAllocatedRows ) )
								return false;
						}
					}
				}
			}

			return true;
		}

		#endregion // InternalTraverseRowsHelper

		#region InternalGetListObjectForRow

		internal object InternalGetListObjectForRow( UltraGridRow row )
		{
			//Debug.Assert( this.TopLevelRowsCollection == row.ParentCollection.TopLevelRowsCollection, "Row does not belong to this rows collection. !" );

			if ( this != row.ParentCollection )
				return null;

			int listIndex = row.ListIndex;

			// SSP 2/1/02 UWG1022
			// Use the list instead of IBindingList so that it works if
			// we are bound to a non-IBindingList list.
			//
			//IBindingList bindingList = this.BindingList;
			IList list = this.Data_List;


			// SSP 1/9/02 UWG916
			// Check for binding list being null (which shouldn't really
			// happen unless we have an invalid row).
			//
			// SSP 2/1/02 UWG1022
			//Debug.Assert( null != bindingList, "Null binding list !" );
			//Debug.Assert( null != list, "Null list !" );

			if ( null == list )
				return null;
			
			if ( listIndex < 0 || listIndex >= list.Count )
				return null;

			return list[ listIndex ];			
		}

		#endregion // InternalGetListObjectForRow

		#region DirtyDataList

		// SSP 2/5/04 UWG2947
		// Added DirtyDataList method.
		//
		internal void DirtyDataList( )
		{
			this.verifyVersion++;
			this.lastParentListObject = null;
		}

		#endregion // DirtyDataList
		
		#region UnBind
		
		internal void UnBind( )
		{
			// If we are bound, then unbind
			//
			if ( null != this.oldBoundList )
			{
				this.oldBoundList.ListChanged -= this.ListChangedHandler;

				this.oldBoundList = null;

				if ( UltraGridBand.binding_debug )
					Debug.WriteLine( "U N b i n d i n g   f r o m   t h e   l i s t !!!!!!!!!!!!!!!!!!!!!!!!!!!!" );
			}
		}

		#endregion // UnBind

		#region BindToList

		internal void BindToList( )
		{
			this.BindToList( this.BindingList );
		}

		// SSP 12/10/02 UWG1883
		// Added list parameter so we can call it from Data_List property.
		//
		private void BindToList( IBindingList list )
		{
			// SSP 1/9/02 UWG916
			// Only hook into the binding list if the HookIntoList off the band
			// returns true because that takes into account various things
			// like design mode etc.
			//
			// SSP 9/5/03 UWG2634
			// Added a check for band being null.
			//
			//if ( !this.Band.HookIntoList )
			if ( null == this.band || !this.Band.HookIntoList )
				return;

			// Don't bind if not top level. (In group by situations,
			// child rows collection of group by rows is not top level).
			// This is because only the top level rows collection for a band
			// has unsorted rows collection.
			//
			if ( !this.IsTopLevel )
				return;

			// For child rows we get the list by calling GetValue
			// on the propertydescriptor of the band's parent column
			//
			//IBindingList list = this.BindingList;

			// If there is no binding list, then reset the flag on 
			// the band so that other rows collection associated with
			// the same band do not try to bind again.
			//
			if ( null == list )
			{
				this.band.HookIntoList = false;				
			}
		
			if ( list == this.oldBoundList )
				return;

			// SSP 3/15/04 - Designer Usability Improvements Related
			// Don't hook into the binding manager/binding list events if we are extracting 
			// the data structure.
			//
			if ( null != this.Layout && this.Layout.ExtractDataStructureOnly )
				return;

			if ( null != this.oldBoundList )
			{
				this.oldBoundList.ListChanged -= this.ListChangedHandler;

				if ( UltraGridBand.binding_debug )
					Debug.WriteLine( "U N H O O K I N G FROM THE OLD LIST !!!!!!!!!!!!!!!!!" );
			}
			
			this.oldBoundList = null;
            			
			if ( null != list )
			{
				list.ListChanged += this.ListChangedHandler;
				this.oldBoundList = list;

				if ( UltraGridBand.binding_debug )
					Debug.WriteLine( "B I N D I N G  T O  T H E  L I S T !!!!!!!!!!!!!!!!!!!!!!!!!!!!" );
			}
		}

		#endregion // BindToList

		#region HasAnyVisibleRows

		// SSP 3/28/02
		// Added HasAnyVisibleRows method for row filters feature.
		//
		internal bool HasAnyVisibleRows( )
		{	
			return this.VisibleRowCount > 0;
		}

		#endregion // HasAnyVisibleRows

		#region AllocateNewRow
		
		// SSP 4/5/04 - Virtual Binding Related Optimizations
		// Took out listIndex parameter.
		//
		//internal UltraGridRow AllocateNewRow( int listIndex )
		internal UltraGridRow AllocateNewRow( )
		{
			// SSP 4/5/04 - Virtual Binding Related Optimizations
			//
			//return new UltraGridRow( this.Band, listIndex, this );
			return new UltraGridRow( this.Band, this );
		}

		// MD 12/4/06 - BR17930
		// Added new overload
		internal UltraGridRow AllocateNewRow( int listIndex )
		{
			UltraGridRow row = this.AllocateNewRow();

			IList dataList = this.Data_List;

			if ( dataList != null && listIndex >= 0 && listIndex < dataList.Count )
				row.InternalCachedListObject = dataList[ listIndex ];

			return row;
		}

		#endregion // AllocateNewRow

		#region RowDeleted

		// SSP 9/9/03 UWG2308
		// Added RowDeleted. We need to dispose of rows that we relinquish so their
		// descendant rows collections unhook from the binding list.
		//
		internal static void RowDeleted( UltraGridRow row )
		{
			// SSP 11/12/03 Add Row Feature
			// If the row was an add-row and it gets deleted then reset the TemplateAddRowHiddenFlag 
			// flag to false to unhide the template add-row.
			//
			// --------------------------------------------------------------------------------------
			if ( null != row.ParentCollection && row.ParentCollection.CurrentAddRow == row )
			{
				row.ParentCollection.CurrentAddRow = null;
			}
			// --------------------------------------------------------------------------------------

            // MBS 4/16/08 - RowEditTemplate NA2008 V2
            UltraGridRowEditTemplate template = row.RowEditTemplateResolved;
            if (template != null && template.IsShown)
                template.Close(true, true);

			// SSP 4/5/04 - Virtual Binding Related Optimizations
			// Set the deleted to true here.
			//
			row.deleted = true;
			row.Dispose( );
		}

		#endregion // RowDeleted

		#region NotifyCalcManager_RowAdded

		internal void NotifyCalcManager_RowAdded( UltraGridRow row )
		{
			// SSP 11/10/04 - Merged Cell Feature
			//
			RowsCollection.MergedCell_RowStateChanged( row, MergedCell.State.Inserted );

			if ( null != this.CalcManger 
				&& ! this.Layout.CalcManagerNotificationsSuspended 
				&& ! this.TopLevelRowsCollection.inSyncRows )
			{
				Infragistics.Shared.SparseArray rowsList = 
					null != row.ParentCollection ? row.ParentCollection.SparseArray : null;

				// Bump the parser instance version number so parsed references of the rows get
				// updated with the new indexes. If the row was added at the end of the 
				// collection then there is no need to bump the parser instance version number.
				//
				if ( ( null == rowsList || 1 + rowsList.IndexOf( row ) != rowsList.Count )
					&& null != this.CalcReference )
					this.CalcReference.BumpParserInstanceVersion( );

				this.CalcManger.AddRowReference( row.CalcReference );
			}
		}

		#endregion // NotifyCalcManager_RowAdded

		#region NotifyCalcManager_RowDeleted

		internal void NotifyCalcManager_RowDeleted( UltraGridRow row )
		{
			// SSP 11/10/04 - Merged Cell Feature
			//
			RowsCollection.MergedCell_RowStateChanged( row, MergedCell.State.Deleted );

			if ( null != this.CalcManger 
				&& ! this.Layout.CalcManagerNotificationsSuspended 
				&& ! this.TopLevelRowsCollection.inSyncRows )
			{
				// Bump the parser instance version number so parsed references of the rows get
				// updated with the new indexes.
				//
				if ( null != this.CalcReference )
					this.CalcReference.BumpParserInstanceVersion( );

				this.NotifyCalcManager_ValueChanged( row );

				this.CalcManger.RemoveRowReference( row.CalcReference );
			}
		}

		#endregion // NotifyCalcManager_RowDeleted

		#region NotifyCalcManager_HiddenStateChanged

		// SSP 11/10/04
		//
		internal void NotifyCalcManager_HiddenStateChanged( UltraGridRow row )
		{
			// SSP 11/11/9/05 BR07639
			// If a template add-row's Hidden is set, we need to re-generate the cached 
			// special rows arrays. The special rows arrays aren't supposed to contain
			// hidden items.
			// 
			this.verifiedSpecialRowsCacheVersion--;

			// SSP 11/10/04 - Merged Cell Feature
			//
			RowsCollection.MergedCell_RowStateChanged( row, MergedCell.State.Hidden );

			Infragistics.Win.CalcEngine.IUltraCalcManager calcManager = this.CalcManger;
			if ( null != calcManager )
			{
				FormulaRowIndexSource formulaRowIndexType = this.band.FormulaRowIndexSourceResolved;
				if ( FormulaRowIndexSource.VisibleIndex == formulaRowIndexType )
				{
					if ( row.HiddenInternal )
						this.NotifyCalcManager_RowDeleted( row );
					else
						this.NotifyCalcManager_RowAdded( row );
				}
			}
		}

		#endregion // NotifyCalcManager_HiddenStateChanged

		#region NotifyCalcManager_ValueChanged

		// SSP 6/23/05 BR04577
		// 
		internal static void NotifyCalcManager_ValueChanged( UltraGridRefBase reference )
		{
			UltraGridLayout layout = reference.Layout;
			if ( null != layout && ! layout.CalcManagerNotificationsSuspended )
			{
				if ( null == reference.Formula )
					reference.NotifyCalcEngineValueChanged( );
			}
		}

		#endregion // NotifyCalcManager_ValueChanged

		#region NotifyCalcManager_ValueChanged

		internal void NotifyCalcManager_ValueChanged( UltraGridRow row, UltraGridColumn column )
		{
			// SSP 11/10/04 - Merged Cell Feature
			//
			RowsCollection.MergedCell_CellStateChanged( row, column, MergedCell.State.Value );

			if ( null != this.CalcManger )
			{
				UltraGridCell cell = row.GetCellIfAllocated( column );
				if ( null != cell )
					this.NotifyCalcManager_ValueChanged( cell );
			}				
		}

		#endregion // NotifyCalcManager_ValueChanged

		#region NotifyCalcManager_ValueChanged

		internal void NotifyCalcManager_ValueChanged( UltraGridCell cell )
		{
			// SSP 11/10/04 - Merged Cell Feature
			//
			RowsCollection.MergedCell_CellStateChanged( cell, MergedCell.State.Value );

			if ( null != cell 
				&& cell.HasCalcReference 
				&& ! cell.CalcReference.HasFormula
				&& null != this.CalcManger 
				&& ! this.Layout.CalcManagerNotificationsSuspended 
				&& ! this.TopLevelRowsCollection.inSyncRows )
			{
				cell.CalcReference.NotifyCalcEngineValueChanged( );
			}
		}

		#endregion // NotifyCalcManager_ValueChanged

		#region NotifyCalcManager_ValueChanged

		internal void NotifyCalcManager_ValueChanged( UltraGridRow row )
		{
			// SSP 11/10/04 - Merged Cell Feature
			//
			RowsCollection.MergedCell_RowStateChanged( row, MergedCell.State.Value );

			// If we are currently syncing then don't bother notifying the calc engine of
			// value change because we will be firing topological change on the whole row
			// collection.
			//
			if ( null != this.CalcManger 
				&& ! this.Layout.CalcManagerNotificationsSuspended 
				&& ! this.TopLevelRowsCollection.inSyncRows 
				// Skip the row for which we are currently in CommitEditMode off the cell because
				// we will be notifying calc engine for that row later on. Look in UltraGridCell's
				// CommitEditValue for more info.
				//
				&& row != this.Layout.StoredCellValuesRow )
			{
				try
				{
					Infragistics.Win.CalcEngine.IUltraCalcManager calcManager = this.CalcManger;
					
					if ( ! ((RowReference)row.CalcReference).inCellReferenceValueSet || row.InInitializeRowEvent )
					{				
						CellsCollection cells = row.HasCells ? row.Cells : null;
						if ( null != cells )
						{
							for ( int i = 0; i < cells.Count; i++ )
							{
								if ( cells.HasCell( i ) )
								{
									UltraGridCell cell = cells[i];

									if ( null != cell && cell.HasCalcReference && ! cell.CalcReference.HasFormula )
										calcManager.NotifyValueChanged( cell.CalcReference );
								}
							}
						}
					}
				}
				catch
				{
					Debug.Assert( false, "Calc manager threw an exception." );
				}
			}
		}

		#endregion // NotifyCalcManager_ValueChanged

		#region NotifyCalcManager_RowsCollectionResynced
		
		internal void NotifyCalcManager_RowsCollectionResynced( )
		{
			Infragistics.Win.CalcEngine.IUltraCalcManager calcManager = this.CalcManger;
			
			if ( null != calcManager && ! this.Layout.CalcManagerNotificationsSuspended )
			{
				try
				{
					this.CalcReference.BumpParserInstanceVersion( );

					// Don't fire the Resync when the row collection is resynced the first time.
					//
					// SSP 9/21/06 BR16000
					// Don't fire the event if it's suspended.
					// 
					//if ( this.CalcReference.resyncEventCounter > 0 )
					if ( this.CalcReference.resyncEventCounter > 0 && 0 == this.CalcReference.resyncEventSuspended )
						calcManager.RowsCollectionReferenceResynched( this.CalcReference );
				}
				catch
				{
					Debug.Assert( false, "Calc manager threw an exception." );
				}
			}

			if ( null != this.CalcReference )
				this.CalcReference.resyncEventCounter++;
		}

		#endregion // NotifyCalcManager_RowsCollectionResynced

		#region NotifyCalcManager_RowsCollectionSorted

		internal void NotifyCalcManager_RowsCollectionSorted( )
		{
			// SSP 11/11/04 - Merged Cell Feature
			// Clear merged cell cache since the row orders could have changed.
			//
			this.ClearMergedCellCache( );

			Infragistics.Win.CalcEngine.IUltraCalcManager calcManager = this.CalcManger;
			
			if ( null != calcManager && ! this.Layout.CalcManagerNotificationsSuspended )
			{
				try
				{
					this.CalcReference.BumpParserInstanceVersion( );

					// Don't fire the Resync when the row collection is resynced the first time.
					//
					if ( this.CalcReference.resyncEventCounter > 0 )
						calcManager.RowsCollectionReferenceSorted( this.CalcReference );
				}
				catch
				{
					Debug.Assert( false, "Calc manager threw an exception." );
				}
			}
		}

		#endregion // NotifyCalcManager_RowsCollectionSorted

		#region DisposeRows

		// SSP 9/9/03 UWG2308
		// Added DisposeRows method.
		//
		internal static void DisposeRows( IList rows )
		{
			if ( null != rows )
			{
				for ( int i = 0; i < rows.Count; i++ )
				{
					UltraGridRow row = rows[i] as UltraGridRow;

					if ( null != row )
						RowsCollection.RowDeleted( row );
				}
			}
		}

		#endregion // DisposeRows

		#region ActivateAddRow
		
		// SSP 7/1/04 UWG3217
		// Added ActivateAddRow method.
		//
		private void ActivateAddRow( UltraGridRow addedRow )
		{
			// If the row collection is dirty, do nothing.
			//
			if ( this.IsRowsDirty )
				return;

			if ( null == this.Layout || null == this.Layout.Grid )
				return;

            // MBS 8/1/08 - BR33916
            // If the band is hidden, then we shouldn't try activating a row
            // that won't even be shown
            if (this.Band != null && this.Band.HiddenResolved)
                return;

			UltraGridRow parentRow = this.TopLevelRowsCollection.ParentRow;
			
			if ( null == parentRow || parentRow.AreAllAncestorsCurrentRows )
			{
				// SSP 6/8/05 BR03609
				// Check for null BindingManager.
				// 
				//int pos = this.Band.BindingManager.Position;
				BindingManagerBase bm = this.Band.BindingManager;
				int pos = null != bm ? bm.Position : -1;

				if ( pos >= 0 && pos < this.UnSortedActualRows.Count 
					&& addedRow == this.UnSortedActualRows[pos] )
					this.Layout.Grid.TempActiveRow = addedRow;
			}
		}

		#endregion // ActivateAddRow

		#region OnListChangedHelper

		#region BR03576

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		private class LastListChangedEventInfo
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//internal int oldIndex, newIndex;
			internal int newIndex;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//internal ListChangedType listChangedType;

			internal bool fireInitializeRowInItemMoved = false;

			internal LastListChangedEventInfo( ListChangedEventArgs e )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.oldIndex = e.OldIndex;

				this.newIndex = e.NewIndex;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
                //this.listChangedType = e.ListChangedType;
			}
		}

		private LastListChangedEventInfo listChanged_LastItemAddedNotification = null;

		#endregion // BR03576

		// SSP 11/19/03 Add Row Feature
		// Added calledFromBandListChanged parameter so that we know if we get called from
		// the UltraGridBand.OnListChanged method.
		//
		//internal void OnListChangedHelper( System.ComponentModel.ListChangedEventArgs e )
		internal void OnListChangedHelper( System.ComponentModel.ListChangedEventArgs e, bool calledFromBandListChanged )
		{
			// SSP 6/6/03 UWG2232
			// If the user has suspended row synchronization, then don't process the event.
			// With UWG2232, simply accessing the bound list for the rows collection 
			// ( call to the PropertyDescriptor.GetValue on the parent chaptered column )
			// causes an unhandled index out of range exception in microsoft code at a later
			// point.
			//
			// ----------------------------------------------------------------------------
			UltraGridBase grid = this.Layout.Grid;

			if ( null != grid && grid.RowSynchronizationSuspended )
			{
				this.DirtyRows( );
				return;
			}
			// ----------------------------------------------------------------------------

			// If we are not connected to the grid's rows hierarchy any more, then
			// unbind from the bidning list and return.
			//
			// SSP 1/18/02
			// Use newly added IsStillValid which makes sure all the ancestors
			// are valid as well.
			//
			
			if ( ( null != this.ParentRow && !this.ParentRow.IsStillValid ) ||
				( null == this.ParentRow && this != this.Layout.Rows ) )
			{
				// Unhook from the binding list's ListChnaged event.
				//
				this.UnBind( );
				return;
			}

			// SSP 2/19/04
			// We are rehooking into the new list in Data_List's get every time the list
			// changes. So we don't need to do it here. Commenting out the below code.
			// 
			

			if ( UltraGridBand.binding_debug )
			{
				//Debug.WriteLine( ((Band)this.layout.Bands[this.Index]).Columns[0].Header.Caption );

				//Debug.WriteLine( "Has parent = " + (this.parentBand != null ).ToString() );

				//Debug.WriteLine( "Index = " + this.Index.ToString() );
		
				if ( null != this.ParentRow )
					Debug.WriteLine( ++this.Layout.Grid.tmp + ": Band[" + this.band.Index + "]: ParentRow.Hashcode = " + this.ParentRow.GetHashCode( ) + "   OnListChanged: " );
				else
					Debug.WriteLine( ++this.Layout.Grid.tmp + ": Band[" + this.band.Index + "]: ParentRow = null OnListChanged: " );
				//Debug.WriteLine( "   Bands[0].BindingList == sender = " + ( sender ==  this.Layout.Bands[0].BindingList  ) );
				//Debug.WriteLine( "   Bands[1].BindingList == sender = " + ( sender ==  this.Layout.Bands[1].BindingList  ) );
				//Debug.WriteLine( "   Bands[2].BindingList == sender = " + ( sender ==  this.Layout.Bands[2].BindingList  ) );
				Debug.WriteLine( "   e.Type                   = " + Enum.GetName( typeof( ListChangedType ), e.ListChangedType ) );
				Debug.WriteLine( "   e.OldIndex               = " + e.OldIndex );
				Debug.WriteLine( "   e.NewIndex               = " + e.NewIndex );
				Debug.WriteLine( "   BindingList.Count        = " + this.BindingList.Count );
				Debug.WriteLine("");
			
				
			}


			// SSP 6/4/02
			// 
			//this.verifyVersion++;
			this.BumpVerifyVersion( );

			
			
			
			
			

			// Get the unsorted rows list. We keep a list that is unsorted.
			// The position of a row in this list corresponds to the position
			// in the underlying binding list for this rows collection.
			// 
			RowsCollection.RowsSparseArray rowsList = this.UnSortedActualRows;

			// SSP 11/18/03 Add Row Feature
			//
			// ------------------------------------------------------------------------------
			//IBindingList bindingList = this.BindingList;
			IBindingList bindingList = this.ResolvedDataList as IBindingList;
			// ------------------------------------------------------------------------------

			if ( null == bindingList )
				return;

			
			
			
			
			// SSP 2/19/04 - Virtual Binding Related Optimizations
			// If this rows collection is marked dirty, then don't bother handling add,
			// move, remove or reset since we are going to be syncing with the whole
			// list anyways.
			//
			if ( this.IsRowsDirty )
				return;

			switch ( e.ListChangedType )
			{
				case ListChangedType.ItemChanged:
				{
					int itemIndex = e.NewIndex;

					// for ItemChanged event, invalidate the row associated with
					// e.NewIndex.
					if ( itemIndex >= 0 && itemIndex < rowsList.Count )
					{
						UltraGridRow row = (UltraGridRow)rowsList[ itemIndex ];

						// SSP 4/28/04 - Virtual Binding
						// In virtual mode, rows list is sparse and thus row doesn't have to have 
						// been created so far.
						//
						//Debug.Assert( null != row, "Null row in the list !" );

						// SSP 10/14/03 UWG2703
						// Bump the verify version on the child rows collections as well.
						//
						if ( null != row )
						{
							row.BumpChildRowsVerifyVersion( );

							// Only invalidate it if in view.
							//
							// SSP 11/12/04 - Optimization
							//
							//if ( null == row.ParentRow || row.ParentRow.Expanded )
							if ( row.IsPotentiallyVisibleOnScreen )
							{
								// SSP 11/1/05 - CLR2 now exposes PropertyDescriptor off the ListChangedEventArgs.
								// 
								PropertyDescriptor propertyDescriptor = null;
								propertyDescriptor = e.PropertyDescriptor;

								// SSP 3/24/05 BR02996
								// If the bound data source is an UltraDataSource then invalidate the cell 
								// that was modified instead of the whole row if the event args includes 
								// info on which cell was modified.
								//
								// ------------------------------------------------------------------------------
								//row.InvalidateItemAllRegions( );
								Infragistics.Win.UltraWinDataSource.UltraDataSourceListChangedEventArgs udsEventArgs
									= e as Infragistics.Win.UltraWinDataSource.UltraDataSourceListChangedEventArgs;
								if ( null != udsEventArgs )
									propertyDescriptor = udsEventArgs.PropertyDescriptor;

								if ( null != propertyDescriptor )
								{
									UltraGridColumn modifiedColumn = this.band.Columns.FindColumn( propertyDescriptor );
									if ( null != modifiedColumn )
										// SSP 5/6/05 BR03624
										// Also dirty the element as well because if the column is Object
										// type then a image could get assigned to the cell value which 
										// could require the cell element use a different renderer if the
										// previous value happened to be something other than an image.
										//
										//row.InvalidateCellAllRegions( modifiedColumn, true, false );
										row.InvalidateCellAllRegions( modifiedColumn, true, true );
								}
								else
								{
									// SSP 7/12/05 BR04897
									// Don't just invalidate the row, also dirty the child elements. ItemChanged
									// gets fired also when for example data error is set or cleared on a row or
									// a cell in which case we would need to reposition the cell elements so 
									// the error icon elements can be removed/added etc...
									// 
									//row.InvalidateItemAllRegions( );
									row.InvalidateItemAllRegions( true );
								}
								// ------------------------------------------------------------------------------
							}

							// SSP 10/29/03 UWG2725
							// Fire the InitilaizeRow on the row however only do so if 
							// this row's ancestors are not current rows. For those rows
							// Band.DataSource_ItemChanged will fire the event.
							//
							// ------------------------------------------------------------
                            //
                            // MBS 8/22/08 - BR35776
                            // In some cases we don't actually create the binding manager for performance 
                            // reasons, which means that we won't actually hook the DataSource_ItemChanged
                            // event, so we should fire the InitializeRow method here.  Note that we now
                            // check the row's property instead of the parent's, since the AreAllAncestorsCurrentRows
                            // check will also check for the precense of a BindingManager
                            //
                            //if ( null != this.parentRow && ! this.parentRow.AreAllAncestorsCurrentRows )
                            if (null != this.parentRow && !row.AreAllAncestorsCurrentRows)
                                row.FireInitializeRow();
							// ------------------------------------------------------------

							// SSP 8/3/04 - UltraCalc
							// Notify the calc manager of the change in the row's value.
							//
							// ------------------------------------------------------------------
							row.ParentCollection.NotifyCalcManager_ValueChanged( row );
							// ------------------------------------------------------------------

                            // MBS 4/22/08 - RowEditTemplate NA2008 V2
                            UltraGridRowEditTemplate template = row.RowEditTemplateResolved;
                            if (template != null && (template.IsShown || template.Row != null))
                            {
                                template.DirtyAllProxies();

                                // If there is anything bound to the cell that's being edited, we need to make sure 
                                // that the new information is pulled in
                                template.RefreshBindings(this.Band);
                            }
						}
					}
						// If the e.NewIndex is -1, that means list has chnaged
						// considerably in which syncronize the rows with the list.
						//
					else if ( itemIndex < 0 )
					{
						// Reinitialize the rows if the index is -1.
						// SSP 2/19/04
						//
						//this.InitRows( );
						this.DirtyRows( );
					}
					else
					{
						Debug.Assert( false, "Invalid item index !" );
					}

					break;
				}
				case ListChangedType.PropertyDescriptorAdded:
				case ListChangedType.PropertyDescriptorChanged:
				case ListChangedType.PropertyDescriptorDeleted:
					// Don't do anything here as PreopryDescriptor related events
					// are handled in Band.OnListChanged event handler. 
					// The reason for that is that each band has a
					// binding list associated with it. This binding list is the one
					// that should handle adding of a column because there could be
					// multiple rows collection associated with the same band and we
					// don't want to be handling the adding of a column in there 
					// multiple times. Not only that, band is the one that manages 
					// columns.
					//
					break;
				case ListChangedType.Reset:
				{
					// On a list reset, init the rows.
					//
					// SSP 2/19/04
					//
					//this.InitRows( );
					this.DirtyRows( );

					break;
				}
				case ListChangedType.ItemDeleted:
				{
					// Row is deleted.
					//


					// SSP 1/21/02
					// Return if rowsList count is less than the bindingList's count
					// because the row could have been already deleted from our rowsList.
					//
					if ( rowsList.Count <= bindingList.Count )
						return;

					int deletedItemIndex = e.NewIndex;
					if ( deletedItemIndex >= 0 && deletedItemIndex < rowsList.Count )
					{
						UltraGridRow row = (UltraGridRow)rowsList[deletedItemIndex];
						rowsList.RemoveAt( deletedItemIndex );
						
						// SSP 4/28/04 - Virtual Binding
						// If a row is null then we are in virtual mode (and not sorted because
						// if sorting was on then all the rows would have been created so it's
						// safe to assume that if a row is null in the rows list then we are in
						// this mode. Added the if block and enclosed the existing code int the
						// else block.
						//
						if ( null == row )
						{
							this.SparseArray.RemoveAt( deletedItemIndex );
						}
						else
						{
							this.SyncRows_Remove( row );

							row.OnDeleted( );

							// Call RowDeleted static method. This method is to be called any time a 
							// row is deleted.
							//
							RowsCollection.RowDeleted( row );
						}

						// SSP 4/8/04 - Virtual Binding
						// When an item from the scroll count sparse array is added, removed or moved
						// it will call ScrollCountManagerSparseArray.OnScrollCountChanged which takes
						// necessary steps to dirty the visible rows etc.
						//
						
					}
					else
					{
						Debug.WriteLine( "Deleted Item Index out of range: " + deletedItemIndex );
					}
					break;
				}
				case ListChangedType.ItemAdded:
				{
					// Row is added.
					//					

					// When a row is added, ItemAdded is fired twice, one time when the
					// row is added, and second time when the row is committed to the 
					// binding list. This has probably something to do with the fact
					// that added row can be cancelled before the event is fired second
					// time.
					// To circumvent this, we checking the count and if they are equal
					// don't process the ItemAdded event.
					//
					if ( rowsList.Count >= bindingList.Count )
					{
						// SSP 7/9/04 UWG3460
						// When an add-row is committed (by calling endedit on it), in the 
						// ItemChanged event off the currency manager, index is -1. We don't 
						// get appropriate ItemChanged event on the IBindingList either. 
						// What we get is ItemAdded a second time on the IBindingList. So we 
						// should fire InitializeRow on the row. The reason for doing this is 
						// that if the row is committed in a different grid then this, we 
						// will never end up firing InitializeRow even though the values have 
						// been changed in the row in the other grid.
						// Added following code.
						// --------------------------------------------------------------------
						if ( ! calledFromBandListChanged )
						{
							if ( e.NewIndex >= 0 && e.NewIndex < rowsList.Count )
							{
								// SSP 5/24/05 BR03576
								// Added the following if block and enclosed the existing code into the else
								// block. For an explanation see the comment above LastListChangedEventInfo 
								// class definition.
								// 
								System.Data.DataView dataView = bindingList as System.Data.DataView;								
								if ( null != dataView 
									&& null != this.listChanged_LastItemAddedNotification 
									&& this.listChanged_LastItemAddedNotification.newIndex != e.NewIndex )
								{
									this.listChanged_LastItemAddedNotification.fireInitializeRowInItemMoved = true;
									this.listChanged_LastItemAddedNotification.newIndex = e.NewIndex;
								}
								else
								{
									// SSP 5/24/05 BR03576
									// Related to above change. Null out the listChanged_LastItemAddedNotification 
									// since its not needed anymore.
									//
									this.listChanged_LastItemAddedNotification = null;

									UltraGridRow row = rowsList[ e.NewIndex ] as UltraGridRow;
									if ( null != row )
										row.FireInitializeRow( );
								}
							}
								// SSP 3/2/05 BR02644
								// If the parent row is not active (currency manager's Position) then 
								// when a row is added to the child row collection via DataTable object
								// then we get ItemAdded however the count off the binding list is 
								// still the old count. We need to reget the parent DataRowView as well
								// as the this row collection's DataView. Simply regetting the DataView
								// is not enough. Added the following else if block.
								//
								// --------------------------------------------------------------------
							else if ( null != this.band.ParentBand && e.NewIndex == rowsList.Count )
							{
								if ( 0 == rowsList.Count )
								{
									// Dirty the rows if the rows count is 0.
									//
									this.DirtyRows( );
								}
								else
								{
									this.DirtyDataList( );
									if ( null != this.parentRow )
										this.parentRow.InternalDecrementVerifiedVersion( );

									this.InvalidateRowCollection( true );
								}
							}
							// --------------------------------------------------------------------
						}
						// --------------------------------------------------------------------

						return;
					}

					int addedItemIndex = e.NewIndex;
					if ( addedItemIndex >= 0 && addedItemIndex <= rowsList.Count )
					{
						// SSP 7/8/02 UWG1321
						// Reset the rowsDirty to false since list changed got fired
						// so we don't have to sync the rows. The reason why rows get
						// dirtied is because we don't know if the list changed event
						// is ever going to get fired when the row is added. We needed
						// to make sure that rows got synced properly because some 
						// implementations did not fire the ListChanged event when rows
						// were added causing the grid's rows collection to be out of
						// sync with the binding lists rows. However if ListChanged
						// does get fired (otherwise we wouldn't be in this ListChanged
						// handler), then that workaround is not necessary. So set the 
						// flag to false.
						//
						if ( this.Band.HookIntoList )
							this.rowsDirty = false;
						
						// SSP 11/12/03 Add Row Feature
						//
						// ----------------------------------------------------------------------
						//UltraGridRow row = this.AllocateNewRow( addedItemIndex );
						// SSP 3/25/05 BR02978
						// Initialize the added row with default values also when the row is 
						// added through the add-new buttons and not just when its added through
						// the template add-row. Commented out the original code and added the 
						// new code below it.
						// ----------------------------------------------------------------------
						
						UltraGridRow row = this.inAddingTemplateAddRow 
							? this.SyncRows_GetTemplateAddRow( ) : this.AllocateNewRow( );
						// ----------------------------------------------------------------------
						// ----------------------------------------------------------------------

						rowsList.Insert( addedItemIndex, row );
						row.InternalCachedListObject = bindingList[ addedItemIndex ];

						// SSP 11/13/03 Add Row Feature
						//
						// SSP 3/25/05 BR02978
						// Initialize the added row with default values also when the row is 
						// added through the add-new buttons and not just when its added through
						// the template add-row.
						//
						// ----------------------------------------------------------------------
						//if ( null != templateRowUtilized )
						//	this.CopyDefaultValuesToAddRow( templateRowUtilized );
						if ( this.inAddingTemplateAddRow || this.band.inAddNew )
							this.CopyDefaultValuesToAddRow( row );
						// ----------------------------------------------------------------------

                        // MBS 4/22/09 - TFS16811
                        // It's possible that we could get the notification to add a new row, but we 
                        // haven't verified the groupby hierarchy.  This can happen in cases where
                        // the user is continuously adding data to the grid (i.e. on a separate
                        // thread that is marshalled back to the UI thread) and we change the order
                        // of the GroupBy columns.
                        this.VerifyGroupByVersion();

						// SSP 4/12/02 UWG1085
						// If this rows collection is not sorted, then add the row
						// at the same position that it was added in the binding list.
						//
                        // MBS 10/1/08 - TFS7220
                        // If we're allowing the users to sort the data externally, then
                        // we shouldn't try to add the row to the end of the sparse array
                        // since we know that the item was added to a particular index and
                        // should respect that so that the order shown in the grid matches
                        // the order in the data source.
                        //
						//if ( null != this.Band && !this.Band.HasSortedColumns )
                        HeaderClickAction headerClickAction = this.Band != null ? 
                            this.Band.GetHeaderClickActionResolved(false) : 
                            HeaderClickAction.Default;
                        //
                        if (null != this.Band && (!this.Band.HasSortedColumns || 
                            headerClickAction == HeaderClickAction.ExternalSortMulti || 
                            headerClickAction == HeaderClickAction.ExternalSortSingle))
							this.SyncRows_Add( row, addedItemIndex );
						else
							this.SyncRows_Add( row );

						// SSP 8/23/02 UWG1434
						// Made IsAddRow public. However since it has to be read-only, added InternalSetIsAddRow
						// internal method for setting it. So use the new InternalSetIsAddRow method instead.
						//
						//row.IsAddRow = true;
						// SSP 7/1/04 UWG3217
						// Only set the isAddNew flag if the row is being added through the grid.
						//
						// --------------------------------------------------------------------------
						//row.InternalSetIsAddRow( true );
						if ( this.Band.inAddNew || this.inAddingTemplateAddRow )
							row.InternalSetIsAddRow( true );
						// --------------------------------------------------------------------------

						// If the band is expecting for us to set the newly added
						// row, then set the band.addedRow to the new row.
						//
						if ( this.Band.inAddNew )
						{
							this.Band.addedRow = row;
						}

						// SSP 5/24/05 BR03576
						// For an explanation see the comment above LastListChangedEventInfo class definition.
						// 
						this.listChanged_LastItemAddedNotification = new RowsCollection.LastListChangedEventInfo( e );

						// SSP 7/1/04 UWG3217
						// Activate the add row if it matches the binding manager's position. When a new
						// row is added, we get binding manager PositionChanged before getting 
						// ListChanged.ItemAdded. What that does is that when we get the PositionChanged
						// we don't have the added row yet so we never end up activating anything.
						// 
						this.ActivateAddRow( row );

						// SSP 4/8/04 - Virtual Binding
						// When an item from the scroll count sparse array is added, removed or moved
						// it will call ScrollCountManagerSparseArray.OnScrollCountChanged which takes
						// necessary steps to dirty the visible rows etc.
						//
						//this.RowsChangedHelper( );

						// SSP 4/12/02 UWG1085
						// Fire initialize row event on the newly added row.
						//
						row.FireInitializeRow( );
					}
					else
					{
						Debug.WriteLine( "Added Item Index out of range: " + addedItemIndex );
					}
					break;
				}		
				case ListChangedType.ItemMoved:
				{
					// A row is moved within the list. It's index has changed.
					//

					int oldItemIndex = e.OldIndex;
					int newItemIndex = e.NewIndex;
					if ( oldItemIndex >= 0 && oldItemIndex < rowsList.Count &&
						newItemIndex >= 0 && newItemIndex < rowsList.Count )
					{
						UltraGridRow row = (UltraGridRow)rowsList[ oldItemIndex ];

						// SSP 4/28/04 - Virtual Binding
						// In virtual mode, rows list is sparse and thus row doesn't have to have 
						// been created so far.
						//
						//if ( row.ListIndex == oldItemIndex )
						if ( null == row || row.ListIndex == oldItemIndex )
						{
							rowsList.RemoveAt( oldItemIndex );
						
							// SSP 4/5/04 - Virtual Binding Related Optimizations
							//
							

							rowsList.Insert( newItemIndex, row );

							// SSP 4/12/02 UWG1715
							// When a row is moved within the collection, we don't want to readd it to
							// the collection causing the row to be in the collection multiple times. We
							// don't want to reposition it either because the row will jump from location
							// to location once the user updates the newly added row and potentially
							// may get scrolled out of view.
							// 
							//this.SyncRows_Add( row );

							// SSP 2/26/04 UWG2982
							// Also move the item in the rows collection if it's not sorted.
							//
							// ----------------------------------------------------------------
							if ( null != this.Layout )
							{
								// If we don't have a scrollbar before we performed this operation then
								// 
								RowScrollRegion rsr = this.Layout.ActiveRowScrollRegion;
								bool scrollbarVisible = true;
								if ( null != rsr && ! this.IsRowsDirty )
									// SSP 11/4/04 UWG3593
									// Don't assume the column scrollbars are visible.
									//
									//scrollbarVisible = rsr.WillScrollbarBeShown( true );
									scrollbarVisible = rsr.WillScrollbarBeShown( );

								if ( ! this.Band.HasSortedColumns )
								{
									if ( oldItemIndex < this.SparseArray.Count && newItemIndex < this.SparseArray.Count )
									{
										// SSP 11/15/04 BR00015
										// In template add-row mode of Top, the added row could be at the wrong place
										// even if there are no sort columns.
										//
										// ------------------------------------------------------------------------------
										//this.SparseArray.RemoveAt( oldItemIndex );
										//this.SparseArray.Insert( newItemIndex, row );
										int index = this.SparseArray.IndexOf( row );
										if ( index >= 0 )
										{
											this.SparseArray.RemoveAt( index );
											this.SparseArray.Insert( newItemIndex, row );
										}
										// ------------------------------------------------------------------------------
									}
								}

								// SSP 5/24/05 BR03576
								// For an explanation see the comment above LastListChangedEventInfo class definition.
								// 
								// ------------------------------------------------------------------------------------
								if ( null != this.listChanged_LastItemAddedNotification )
								{
									bool fireInitializeRow = this.listChanged_LastItemAddedNotification.fireInitializeRowInItemMoved 
										&& this.listChanged_LastItemAddedNotification.newIndex == newItemIndex;

									this.listChanged_LastItemAddedNotification = null;

									if ( fireInitializeRow && null != row )
										row.FireInitializeRow( );
								}
								// ------------------------------------------------------------------------------------

								// If the BindingManager handled the ItemMoved before we did then it
								// would've already fired the PositionChanged and our active row could be
								// the wrong row. So make sure it's the right active row.
								//
								// SSP 5/18/05 BR03434
								// Added SyncWithCurrencyManager to prevent the UltraGrid from synchronizing with
								// the currency manager. When there are a lot of deep nested bands activating a 
								// different ancestor row caues a lot of slowdown.
								// Enclosed the existing code into the if block.
								//
								if ( grid is UltraGrid && ((UltraGrid)grid).SyncWithCurrencyManager )
								{
									UltraGridRow activeRow = this.Layout.ActiveRow;
									if ( null != activeRow && activeRow.Band == this.Band && ! this.IsRowsDirty )
									{
										UltraGridRow currRow = this.Band.GetCurrentRow( false, true );
										if ( null != currRow )
											this.Layout.Grid.TempActiveRow = currRow;
									}
								}

								// If the scroll bar wasn't visible initially then make sure that it doesn't
								// show up again by setting the FirstRow to null.
								//
								if ( ! scrollbarVisible && null != rsr )
									rsr.FirstRow = null;
							}
							// ----------------------------------------------------------------

							// SSP 4/8/04 - Virtual Binding
							// When an item from the scroll count sparse array is added, removed or moved
							// it will call ScrollCountManagerSparseArray.OnScrollCountChanged which takes
							// necessary steps to dirty the visible rows etc.
							//
							//this.RowsChangedHelper( );
						}
						else
						{
							Debug.WriteLine( "row.ListIndex != oldItemIndex : " + row.ListIndex + " != " + oldItemIndex );
						}
					}
					else
					{
						Debug.WriteLine( "Move Item Indexs out of range: new index = "  + newItemIndex + " old index = " + oldItemIndex );

						// SSP 8/28/04 UWG3142
						// We get ItemMoved with negative indexes when there is a filter on the 
						// data view and a row's value changes so that its visibility changes.
						// In such a case just sync the rows.
						//
						if ( null != rowsList && null != bindingList && rowsList.Count != bindingList.Count )
							this.DirtyRows( );
					}

					break;
				}
			}


			// SSP 5/16/02
			// Fire the ListChanged event on the ultra drop down if the grid is ultra drop down.
			// ListChanged was added to IValueList interface and subsequently to the UltraDropDownBase
			// and that is supposed to be fired whenever the rows have changed notifying the owners
			// of the drop down (owners of the IValueList actually) that number of items have
			// changed so they can take appropriate actions.
			//
			if ( null != this.Layout && this.Layout.Grid is UltraDropDownBase )
			{
				UltraDropDownBase ultraDropDown = (UltraDropDownBase)this.Layout.Grid;

				ultraDropDown.FireListChanged( );
			}

            // CDS 01/23/09 TFS12496 NAS v9.1 Header CheckBox
            this.DirtyCheckStateCacheValidEntry(null);
            if (this.Band != null)
                this.Band.DirtyCheckStateCacheValidEntry(null);
		}

		#endregion // OnListChangedHelper

		#region OnListChanged
		
		private void OnListChanged( object sender, System.ComponentModel.ListChangedEventArgs e )
		{
			// We should catch any exceptions thrown by OnListChangedHelper
			// and do a Debug.Assert with the message because any exception
			// that thrown inside the ListChanged event handler is caught by
			// the framework so we won't see any indication that an exception
			// was thrown in our OnListChangedHelper function and that there
			// is something wrong with it.
			//
			try
			{
				// SSP 11/19/03 Add Row Feature
				// Pass in false for calledFromBandListChanged parameter.
				//
				//this.OnListChangedHelper( e ); 
				this.OnListChangedHelper( e, false ); 
			}
			catch ( Exception exception )
			{
				Debug.Assert( false, "Exception throw in OnListChnagedHelper !\n" + exception.Message );

				throw;
			}

            //  BF NA 9.1 - UltraCombo MultiSelect
            //  Send a notification out to any interested listeners that
            //  the data has changed.
            PropertyDescriptor propDescriptor = e.PropertyDescriptor;
            string columnKey = propDescriptor != null ? propDescriptor.Name : string.Empty;
            CheckedListSettings.DataChangedEventArgs eventArgs = new CheckedListSettings.DataChangedEventArgs( null, columnKey, e );
            this.FireDataChanged( eventArgs );
		}

		#endregion // OnListChanged

		#region BumpRowFiltersVersion

		internal void BumpRowFiltersVersion( )
		{
			this.rowFiltersVersion++;

			// SSP 8/1/03 UWG1654 - Filter Action
			// Bump cell child elements version.
			//
			if ( null != this.Layout )
			{
				// SSP 8/14/03 - Optimizations
				//
				this.Layout.BumpGrandVerifyVersion( );

				this.Layout.BumpCellChildElementsCacheVersion( );
			}
		}

		#endregion // BumpRowFiltersVersion

		#region InternalTraverseRowsHelper

		// MRS 1/28/05 - BR00166
		// Added an overload that allows inclusion of TemplateAddRows
		//internal bool InternalTraverseRowsHelper( UltraGridBand band, IRowCallback rowCallback, bool recursive )
		internal bool InternalTraverseRowsHelper( UltraGridBand band, IRowCallback rowCallback, bool recursive, bool includeTemplateAddRows )
		{
			// SSP 12/8/05 BR07665
			// Added an overload that takes in rowsCollectionCallback parameter.
			// 
			return this.InternalTraverseRowsHelper( band, rowCallback, null, recursive, includeTemplateAddRows );
		}

		// SSP 12/8/05 BR07665
		// Added an overload that takes in rowsCollectionCallback parameter.
		// 
		internal bool InternalTraverseRowsHelper( UltraGridBand band, IRowCallback rowCallback, 
			IRowsCollectionCallback rowsCollectionCallback, bool recursive, bool includeTemplateAddRows )
		{
			// SSP 12/8/05 BR07665
			// Added an overload that takes in rowsCollectionCallback parameter.
			// 
			if ( null != rowsCollectionCallback )
			{
				if ( ! rowsCollectionCallback.ProcessRowsCollection( this ) )
					return false;
			}

			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridRow row = this[i];

				if ( row.Band == band )
				{
					if ( !rowCallback.ProcessRow( row ) )
						// Return false to stop further traversal
						//
						return false;
				}
				
				if ( recursive )
				{
					// SSP 9/2/03
					// Don't access ChildBands property unless we have to.
					//
					//if ( null != row.ChildBands && 
					//	( band.IsDescendantOfBand( row.Band ) || band == row.Band ) )
					if ( ( band.IsDescendantOfBand( row.Band ) || ( row is UltraGridGroupByRow && band == row.Band ) )
						&& null != row.ChildBands )
					{
						for ( int j = 0; j < row.ChildBands.Count; j++ )
						{
							UltraGridChildBand childBand = row.ChildBands[j];

							if ( band == childBand.Band ||
								band.IsDescendantOfBand( childBand.Band ) )
							{
								// MRS 1/28/05 - BR00166
								//if ( !childBand.Rows.InternalTraverseRowsHelper( band, rowCallback, recursive ) )
								// SSP 12/8/05 BR07665
								// Pass along the new rowsCollectionCallback parameter.
								// 
								//if ( !childBand.Rows.InternalTraverseRowsHelper( band, rowCallback, recursive, includeTemplateAddRows ) )
								if ( !childBand.Rows.InternalTraverseRowsHelper( band, rowCallback, rowsCollectionCallback, recursive, includeTemplateAddRows ) )
									// Return false to stop further traversal
									//
									return false;

								break;
							}
						}
					}
				}
			}

			// MRS 1/28/05 - BR00166
			// Account for TemplateAddRows
			if (includeTemplateAddRows 
				&& this.IsTemplateAddRowVisible
				&& this.TemplateAddRow != null)				
			{
				UltraGridRow row = this.TemplateAddRow;

				if ( row.Band == band )
				{
					if ( !rowCallback.ProcessRow( row ) )
						// Return false to stop further traversal
						//
						return false;
				}
			}

			return true;
		}

		internal bool InternalTraverseRowsHelper( UltraGridBand band, IRowCallback rowCallback, bool recursive )
		{
			// MRS 1/28/05 - BR00166
			// Added an overload that allows inclusion of TemplateAddRows
			return this.InternalTraverseRowsHelper(band, rowCallback, recursive, false);
		}

		#endregion // InternalTraverseRowsHelper

		#region InternalClearHelper
		
		internal void InternalClearHelper( )
		{
			// SSP 4/8/04 - Virutal Binding Related
			//
			//this.InternalClear( );
			if ( null != this.sparseArray )
				this.sparseArray.Clear( );

			if ( null != this.unsortedActualRows )
			{
				// SSP 9/9/03 UWG2308
				// Clean up the row. Since rows collection hooks into the binding list we
				// need to make sure that the row's descendant rows collections unhook
				// from the binding list.
				//
				RowsCollection.DisposeRows( this.unsortedActualRows );

				this.unsortedActualRows.Clear( );
				this.unsortedActualRows = null;
			}
		}

		#endregion // InternalClearHelper

		#region DirtyRows
		
		internal void DirtyRows( )
		{			
			this.rowsDirty = true;

			UltraGridLayout layout = this.Layout;
			UltraGridBase grid = null != layout ? layout.Grid : null;

			// SSP 6/19/03 UWG2232
			// Related to SuspendRowSynchronization and ResumeRowSynchronization methods.
			// When the row syncronization is suspended, we also suspend firing InitialiazeRow
			// (we don't respond to binding events). So when the row syncrhonization is resumed
			// fire initialize row on all the rows.
			//
			if ( ! this.fireInitializeRowsOnAllRow && null != grid && grid.RowSynchronizationSuspended )
				this.fireInitializeRowsOnAllRow = true;

			// SSP 2/20/07 BR19262
			// 
			// --------------------------------------------------------
			if ( grid is UltraGrid )
			{
				UltraGrid gridCtrl = grid as UltraGrid;
				Selected selection = gridCtrl.SelectedIfAllocated;
				if ( null != selection )
					selection.needsToVerifyRowCollections = true;
			}
			// --------------------------------------------------------

			// SSP 6/4/02
			//
			//this.verifyVersion++;
			this.BumpVerifyVersion( );

			// SSP 5/13/04 UWG3260
			// Bump the filters version number so they get re-evaluated. When we get a ListReset, we
			// should not assume that the underlying values will stay the same. So it's appropriate
			// for us to re-evaluate the row filters.
			//
			// --------------------------------------------------------------------------------------
			if ( this.AreAnyFiltersActive )
				this.BumpRowFiltersVersion( );
			// --------------------------------------------------------------------------------------

			// SSP 4/8/05 - Virtual Binding Related Optimizations
			// Dirty the scroll count of the parent row.
			//
			this.DirtyParentRowScrollCount( );
        }

		#endregion // DirtyRows

		#region BumpVerifyVersion

		internal void BumpVerifyVersion( )
		{
			this.verifyVersion++;

			this.DataChanged_DirtySummaries( );

			// SSP 8/12/03 - Optimizations - Grand Verify Version Number
			//
			if ( null != this.Layout )
				// SSP 12/2/05 BR07897
				// Don't cause the scroll to fill logic to be executed, otherwise scrolling
				// will be slower when ExpansionIndicator is set to CheckOnDisplay when
				// a row gets scrolled into view the first time.
				// 
				//this.Layout.BumpGrandVerifyVersion( );
				this.Layout.BumpGrandVerifyVersion( false );
		}

		#endregion // BumpVerifyVersion

		#region SortGroupByRowsHelper
		
		private void SortGroupByRowsHelper( )
		{
			if ( this.SparseArray.Count > 0 )
			{
				UltraGridColumn groupByColumn = this.GroupByColumn;
				Debug.Assert( null != groupByColumn );
				if ( null != groupByColumn )
				{
					if ( SortIndicator.Ascending == groupByColumn.SortIndicator )
					{
						if ( !this.verifiedGroupBySortDirectionAscending )
						{
							// SSP 10/20/04 UWG3747
							// Added GroupByComparer property to allow the user to sort group-by 
							// rows using a custom comparer. Added the if block and enclosed the
							// existing code into the else block.
							//
							if ( null != groupByColumn.GroupByComparer )
								this.SparseArray.Sort( groupByColumn.GroupByComparer );
							else
								this.SparseArray.Reverse( );

							this.verifiedGroupBySortDirectionAscending = true;
						}					
					}
						// descending SortIndicator because group by columns have to have 
						// SortIndicator set on them.
						//
					else 
					{
						if ( this.verifiedGroupBySortDirectionAscending )
						{
							// SSP 10/20/04 UWG3747
							// Added GroupByComparer property to allow the user to sort group-by 
							// rows using a custom comparer. Added the following if statement.
							// NOTE: We do want to reverse the sorted rows after sorting by the
							// group comparer.
							//
							if ( null != groupByColumn.GroupByComparer )
								this.SparseArray.Sort( groupByColumn.GroupByComparer );

							this.SparseArray.Reverse( );
							this.verifiedGroupBySortDirectionAscending = false;
						}
					}
				}

				// SSP 1/16/02
				// Sorting not necessary because all that can change is the
				// sort direction on the group by column. So all we
				// need to do is to reverse the order

				//this.List.Sort( new GroupByRowComparer( this ) );
			}
		}

		#endregion // SortGroupByRowsHelper

		#region AfterUngroupRows_EnsureActiveRow

		// SSP 12/21/04 BR00986
		// Added AfterUngroupRows_EnsureActiveRow method.
		//
		private void AfterUngroupRows_EnsureActiveRow( )
		{
			// Do this only for the root row collection and non-group-by rows.
			//
			UltraGridLayout layout = this.Layout;
			if ( null == this.ParentRow && null != layout && layout.IsDisplayLayout
				&& ! this.IsGroupByRows 
				// SSP 10/19/05 BR06690
				// Only do so for UltraGrid. Don't do this for UltraCombo.
				// 
				&& layout.Grid is UltraGrid )
			{
				UltraGridRow currentActiveRow = layout.Grid.TempActiveRow;
				if ( null == currentActiveRow )
					currentActiveRow = layout.ActiveRow;

				if ( null == currentActiveRow )
				{
					UltraGridRow newActiveRow = this.band.GetCurrentRow( false, true );
					if ( null != newActiveRow )
						layout.Grid.SetTempActiveRow( newActiveRow, false );
				}
			}
		}

		#endregion // AfterUngroupRows_EnsureActiveRow

		#region VerifyGroupByVersion
		
        // MBS 2/26/09 - TFS12415
        // Made this method internal
		//private void VerifyGroupByVersion( )
        internal void VerifyGroupByVersion()
		{
			// SSP 8/12/03 - Optimizations - Grand Verify Version Number
			//
			// ----------------------------------------------------------------------------------------
			if ( this.band.Layout.GrandVerifyVersion == this.verifiedGrandVersion_VerifyGroupByVersion )
				return;
			// ----------------------------------------------------------------------------------------

			// if we are in SyncRows, then don't do anything
			// SSP 11/11/05 BR07435
			// Use the property which checks for the top-level row collection.
			// 
			//if ( this.inSyncRows )
			if ( this.InSyncRows )
				return;

			// SSP 12/12/06 BR17955
			// If we are in the already in the process of initializing group-by rows then return.
			// 
			if ( this.inInitGroupByRows || this.inInitNonGroupByRows )
				return;

			// SSP 11/30/07 BR28677
			// Also if the top-level row collection is in the process of creating group-by hierarchy 
			// then also return.
			// 
			RowsCollection topLevelRows = this.TopLevelRowsCollection;
			if ( topLevelRows.inInitGroupByRows || topLevelRows.inInitNonGroupByRows )
				return;

			SortedColumnsCollection sortedColumns = this.Band.InternalSortedColumns;
			bool bandHasSortedColumns = null != sortedColumns;

			// SSP 11/8/01 UWG688
			// Don't regroup while the sorted columns collection is still trying to remove
			// a group by column.
			//
			//if ( this.Band.HasSortedColumns && this.Band.SortedColumns.inProcessingNewSortedColumns )
			// SSP 4/15/02
			// Don't use HasSortedColumns property. Instead use the newly added IsSortedColumnsCollectionCreated
			// property because here we want to check if the the sorted columns collection
			// has been allocated or not. HasSortedColumns checks for the count as well which
			// we don't want to check here because even if the count is 0, we still want to return
			// if the sorted columns collection is in the midst of removing a column.
			//
			//if ( bandHasSortedColumns && this.Band.SortedColumns.inProcessingNewSortedColumns )
			if ( null != sortedColumns && sortedColumns.inProcessingNewSortedColumns )
				return;

			// SSP 8/12/03 - Optimizations - Grand Verify Version Number
			// 
			// ----------------------------------------------------------------------------------------
			this.verifiedGrandVersion_VerifyGroupByVersion = this.Band.Layout.GrandVerifyVersion;
			// ----------------------------------------------------------------------------------------

			// SSP 3/30/06 BR11065
			// Don't send any notification to the calc manager if the calc mode is syncrhonous
			// until we have completed the processing.
			// 
			// ------------------------------------------------------------------------------------
			
			BandReference bandCalcRef = this.Band.HasCalcReference ? this.Band.CalcReference : null;
			if ( null != bandCalcRef )
				bandCalcRef.SuspendVerifyGroupLevelSummaryFormulas( );
			// ------------------------------------------------------------------------------------

			try
			{
				// for top-levels 
				if ( 
					// SSP 1/22/04 - Optimization
					// Removed groupByStyleVersion.
					//
					//( this.verifiedGroupByStyleVersion != this.band.Layout.GroupByStyleVersion || 
					//this.verifiedGroupByHierarchyBandsVersion != this.band.GroupByHierarchyVersion ) &&
					this.verifiedGroupByHierarchyBandsVersion != this.band.GroupByHierarchyVersion &&
					// SSP 4/17/03 - Optimizations
					//( null == this.ParentRow || !( this.ParentRow is UltraGridGroupByRow ) ) )
					!( this.ParentRow is UltraGridGroupByRow ) )
				{
					if ( ViewStyleBand.OutlookGroupBy == this.band.Layout.ViewStyleBand  && this.band.HasGroupBySortColumns )
					{
						this.InitGroupByRows( );
					}
					else 
					{
						this.InitNonGroupByRows( );
					}

					// SSP 1/22/04 - Optimization
					// Removed groupByStyleVersion.
					//
					//this.verifiedGroupByStyleVersion = this.band.Layout.GroupByStyleVersion;

					this.verifiedGroupByHierarchyBandsVersion = this.band.GroupByHierarchyVersion;
				
					if ( null != this.GroupByColumn )
					{
						this.verifiedGroupByHierarchyVersion = this.GroupByColumn.GroupByHierarchyVersion;
					
						// SSP 1/16/02 
						// Since we are only looking at the SortDirection, we don't
						// need to have a version. We just verify against the sort direction.
						//
						//this.verifiedGroupBySortVersion = this.GroupByColumn.GroupBySortVersion;
						this.verifiedGroupBySortDirectionAscending = SortIndicator.Ascending == this.GroupByColumn.SortIndicator;
					}

					// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
					// Commented out the following line of code. ScrollCountManagerSparseArray itself 
					// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
					// method.
					//
					//this.DirtyScrollableRowCountHelper( );

					// SSP 12/21/04 BR00986
					// After re-grouping make sure any deleted rows are unselected.
					// Also activate a regular row if possible.
					//
					// ------------------------------------------------------------
					this.UnSelectRowsNotInCollection( );
					this.AfterUngroupRows_EnsureActiveRow( );
					// ------------------------------------------------------------

					// SSP 5/5/05 - NAS 5.2 InitializeRowsCollection event
					// Whenever the rows collection is grouped or ungrouped or it's group-by rows
					// hierarchy changes fire the InitializeRowsCollection event.
					//
					this.FireInitializeRowsCollection( );
				}

				// If we are not going to be having group by rows at all, then
				// don't need to verify below
				// SSP 12/13/01
				//if ( ViewStyleBand.OutlookGroupBy != this.band.Layout.ViewStyleBand || !this.band.HasSortedColumns )
				// SSP 4/17/03 Optimizations.
				//
				//if ( ViewStyleBand.OutlookGroupBy != this.band.Layout.ViewStyleBand || !bandHasSortedColumns )
				if ( ! bandHasSortedColumns || ViewStyleBand.OutlookGroupBy != this.band.Layout.ViewStyleBand )
				{
					return;
				}

				UltraGridColumn groupByColumn = this.GroupByColumn;
				if ( null == groupByColumn )
					return;

				if ( this.verifiedGroupByHierarchyVersion != groupByColumn.GroupByHierarchyVersion )
				{
					// SSP 8/1/08 BR29916
					// 
					if ( !this.IsStillValid )
						return;

					this.InitGroupByRows( );

					// SSP 1/16/02 
					// Since we are only looking at the SortDirection, we don't
					// need to have a version. We just verify against the sort direction.
					//
					//this.verifiedGroupBySortVersion = this.GroupByColumn.GroupBySortVersion;
					this.verifiedGroupBySortDirectionAscending = SortIndicator.Ascending == this.GroupByColumn.SortIndicator;
				
					this.verifiedGroupByHierarchyVersion = this.GroupByColumn.GroupByHierarchyVersion;
					this.verifiedGroupByHierarchyBandsVersion = this.band.GroupByHierarchyVersion;
				
					this.Band.Layout.ColScrollRegions.DirtyMetrics( false );
					// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
					// Commented out the following line of code. ScrollCountManagerSparseArray itself 
					// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
					// method.
					//
					//this.DirtyScrollableRowCountHelper( );

					// SSP 12/21/04 BR00986
					// After re-grouping make sure any deleted rows are unselected.
					// Also activate a regular row if possible.
					//
					// ------------------------------------------------------------
					this.UnSelectRowsNotInCollection( );
					this.AfterUngroupRows_EnsureActiveRow( );
					// ------------------------------------------------------------

					// SSP 5/5/05 - NAS 5.2 InitializeRowsCollection event
					// Whenever the rows collection is grouped or ungrouped or it's group-by rows
					// hierarchy changes fire the InitializeRowsCollection event.
					//
					this.FireInitializeRowsCollection( );
				}

				// SSP 1/16/02 
				// Since we are only looking at the SortDirection, we don't
				// need to have a version. We just verify against the sort direction.
				//
				//if ( this.verifiedGroupBySortVersion != this.GroupByColumn.GroupBySortVersion )
				if ( this.verifiedGroupBySortDirectionAscending !=
					( SortIndicator.Ascending == groupByColumn.SortIndicator ) )
				{
					this.SortGroupByRowsHelper( );

					// SSP 1/22/02 UWG964
					// When the rows are resorted, dirty the scroll position cache 
					// version on the layout.
					// 
					// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
					// Commented out the following line of code. ScrollCountManagerSparseArray itself 
					// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
					// method.
					//
					//this.DirtyScrollableRowCountHelper( );

					// SSP 1/16/02 
					// Since we are only looking at the SortDirection, we don't
					// need to have a version. We just verify against the sort direction.
					//
					//this.verifiedGroupBySortVersion = this.GroupByColumn.GroupBySortVersion;
					this.verifiedGroupBySortDirectionAscending = SortIndicator.Ascending == groupByColumn.SortIndicator;

					// SSP 5/5/08 BR30242 - Optimizations
					// For optimizing performance, if a calc manager is listening into 
					// notifications, send a single resync instead of multiple sort 
					// notifications for each child row collection that gets sorted.
					// --------------------------------------------------------------------
					if ( null != this.calcReference && null != this.CalcManger )
						topLevelRows.VerifyGroupByVersion_EnsureNotDirtyRecursiveHelper( );
					// --------------------------------------------------------------------
				}
                // MBS 5/15/09 - TFS17529
                // If we've bumped the sort version number, but the sort indicator is the same,
                // we should still be re-sorting if the user has specified a GroupByComparer.                
                else if (this.sortVersion != this.Band.SortedColumns.SortVersion &&
                    groupByColumn.GroupByComparer != null)
                {
                    this.SparseArray.Sort(groupByColumn.GroupByComparer);
                    this.sortVersion = this.Band.SortedColumns.SortVersion;
                }
			}
			finally
			{
				// SSP 3/30/06 BR11065
				// Don't send any notification to the calc manager if the calc mode is syncrhonous
				// until we have completed the processing.
				// 
				if ( null != bandCalcRef )
					bandCalcRef.ResumeVerifyGroupLevelSummaryFormulas( true );
			}
		}

		// SSP 5/5/08 BR30242
		// 
		private void VerifyGroupByVersion_EnsureNotDirtyRecursiveHelper( )
		{
			if ( this.IsGroupByRows )
			{
				foreach ( UltraGridGroupByRow groupByRow in this )
				{
					RowsCollection childRows = groupByRow.Rows;
					if ( null != childRows )
					{
						childRows.EnsureNotDirty( );
						childRows.VerifyGroupByVersion_EnsureNotDirtyRecursiveHelper( );						
					}
				}
			}

			if ( this.IsTopLevel )
				this.NotifyCalcManager_RowsCollectionResynced( );
		}

		#endregion // VerifyGroupByVersion

		#region EnsureSortOrder
		
		// SSP 11/17/04 UWG2234 
		// Changed the behavior so that when a column is sorted we maintain the scroll position
		// instead of keeping the first row the first row.
		//
		internal void MaintainScrollPositionHelper( )
		{
			this.MaintainScrollPositionHelper( 0 );
		}

		// SSP 5/12/05 - BR03865 - NAS 5.2 Fixed Rows 
		// Added an overload of MaintainScrollPositionHelper that takes in deltaScrollPos.
		// 
		internal void MaintainScrollPositionHelper( int deltaScrollPos )
		{
			RowScrollRegionsCollection rsrColl = null != this.Layout ? this.Layout.RowScrollRegions : null;
			if ( null != rsrColl )
			{
				for ( int i = 0; i < rsrColl.Count; i++ )
				{
					RowScrollRegion rsr = rsrColl[ i ];
					UltraGridRow firstRow = rsr.InternalFirstRow;

					if ( null != firstRow && firstRow.IsDescendantOf( this ) )
					{
						int currScrollPosition = rsr.HasScrollBarInfo ? rsr.ScrollBarInfo.Value : 0;

						// SSP 5/12/05 - BR03865 - NAS 5.2 Fixed Rows 
						// Added an overload of MaintainScrollPositionHelper that takes in deltaScrollPos.
						// Added following line.
						//
						currScrollPosition = Math.Max( 0, currScrollPosition + deltaScrollPos );

						if ( currScrollPosition >= 0 )
							rsr.AfterSortScrollPositionToMaintain = currScrollPosition;
					}
				}

				// SSP 4/21/05 BR03382
				// Handle card-view as well.
				//
				// ------------------------------------------------------------------------
				if ( this.IsCardRows && this.IsPotentiallyVisibleOnScreen )
				{
					if ( CardStyle.VariableHeight != this.Band.CardSettings.StyleResolved 
						&& CardStyle.Compressed != this.Band.CardSettings.StyleResolved )
					{
						ColScrollRegionsCollection csrColl = this.Layout.ColScrollRegions;
						for ( int i = 0; i < rsrColl.Count; i++ )
						{
							for ( int j = 0; j < csrColl.Count; j++ )
							{
								CardAreaUIElement cardAreaElem = this.GetCardAreaElement( rsrColl[ i ], csrColl[ j ], false );
								if ( null != cardAreaElem && ! cardAreaElem.Disposed )
									cardAreaElem.MaintainScrollPositionHelper( );
							}
						}
					}

					this.firstVisibleCard = null;
				}
				// ------------------------------------------------------------------------
			}
		}

		private void EnsureSortOrder( )
		{	
			// MRS 12/5/05 - BR08024
			bool originalInEnsureSortOrder = this.inEnsureSortOrder;
			this.inEnsureSortOrder = true;

			// MRS 12/5/05 - BR08024
			// Wrapped this in a try..finally, to make sure the inEnsureSortOrder
			// flag gets reset. 
			try
			{
				// SSP 8/12/03 - Optimizations - Grand Verify Version Number
				// Note: This will prevent recursive calls as well which I think is a good side effect.
				// This shouldn't be called recursively.
				//
				// ----------------------------------------------------------------------------------------
				if ( this.Band.Layout.GrandVerifyVersion == this.verifiedGrandVersion_EnsureSortOrder )
					return;
				// ----------------------------------------------------------------------------------------

				// SSP 11/8/01 UWG688
				// Don't resort while the sorted columns collection still trying to remove
				// a column
				//
				// SSP 4/17/03
				// Don't check if we have any sorted columns. Check if the sorted columns collection
				// has been allocated because we could be in the middle of processing the Clear operaion
				// on the sorted columns collection and HasSortedColumns would return 0 (since the sorted 
				// columns were cleared).
				// 
				
				SortedColumnsCollection sortedColumns = this.Band.InternalSortedColumns;

				if ( null != sortedColumns && sortedColumns.inProcessingNewSortedColumns 
					// SSP 10/27/05 BR05556
					// if we are in SyncRows, then don't do anything.
					// 
					// SSP 11/11/05 BR07435
					// Use the property which checks for the top-level row collection.
					// 
					//|| this.inSyncRows 
					|| this.InSyncRows )
					return;

				// SSP 8/12/03 - Optimizations - Grand Verify Version Number
				// Note: This will prevent recursive calls as well which I think is a good side effect.
				// This shouldn't be called recursively.
				//
				this.verifiedGrandVersion_EnsureSortOrder = this.Band.Layout.GrandVerifyVersion;

				if ( null == sortedColumns )
					return;

				// SSP 4/17/03 Optimizations
				// 
				//if ( this.Count < 1 )
				//	return;
				//
				//if ( null != this.GroupByColumn )
				//	return;

				// SSP 4/17/03 Optimizations
				// HasSortedColumns checks for the the sorted columns being less than 1.
				//
				//if ( !this.band.HasSortedColumns ||
				//	this.band.SortedColumns.Count < 1 )
				if ( sortedColumns.SortVersion == this.sortVersion ||
					sortedColumns.Count < 1 || null != this.GroupByColumn )
				{				
					return;
				}
			
				// SSP 10/25/04 UWG3756 UWG3662 UWG3624
				// If the row collection has been removed from the hierarchy then don't sort the rows.
				//
				// ------------------------------------------------------------------------------------
				if ( this.Disposed || this.SparseArray.Count > 0 && null != this.SparseArray[0]
					&& this != ((UltraGridRow)this.SparseArray[0]).ParentCollection )
				{
					//Debug.Assert( false, "EnsureSortOrder called on an invalid row collection !" );
					return;
				}
				// ------------------------------------------------------------------------------------

				// SSP 3/31/05 BR03040
				// Moved the code that sets the sortVersion from below the following if block. Also 
				// added call to ClearMergedCellCache.
				//
				// ------------------------------------------------------------------------------------
				// JJD 05/15/02
				// Moved cache of the sort version to before the call to Sort
				// in case code in a custom comparer checks the row Index
				// or accesses another property on the rows collection that
				// would cause us to recurse.
				//
				this.sortVersion = this.band.SortedColumns.SortVersion;

				this.ClearMergedCellCache( );
				// ------------------------------------------------------------------------------------

				// SSP 4/19/04 - Virtual Binding Related
				// Added ExternalSortSingle and ExternalSortMulti members. If the header click action 
				// is ExternalSortSingle or ExternalSortMulti then don't sort.
				//
				HeaderClickAction headerClickAction = this.band.GetHeaderClickActionResolved( false );
				if ( HeaderClickAction.ExternalSortSingle == headerClickAction 
					|| HeaderClickAction.ExternalSortMulti == headerClickAction )
				{
					// SSP 10/21/04 UWG3732
					// In the case where the header click action is external sort, then we have to
					// sort the rows by their list indexes. We only need to do this for regular rows
					// that are grouped.
					//
					if ( null != this.parentRow && this.parentRow.IsGroupByRow )
					{
						this.SparseArray.Sort( new ListIndexComparer( ) );

						// SSP 3/23/05 - NAS 5.2 Fixed Rows
						// Move the fixed rows to top or bottom depending on the fixed rows style. Also ensure
						// that they are in the same order in the main row collection as they are in the fixed
						// row collection.
						//
						this.MoveFixedRowsToProperLocation( );

						// SSP 11/17/04 UWG2234 
						// Changed the behavior so that when a column is sorted we maintain the scroll position
						// instead of keeping the first row the first row.
						//
						this.MaintainScrollPositionHelper( );
					}

					return;
				}

				// SSP 4/17/03 Optimizations
				// Moved this into one check above.
				//
				//if ( sortedColumns.SortVersion == this.sortVersion )
				//	return;

				// SSP 1/23/02
				// Only sort if the list has more than one row
				//
				if ( this.SparseArray.Count > 1 )
				{
					// Copy all the rows into a temporary array.
					//
					// SSP 4/13/04 - Virtual Binding
					//
					//UltraGridRow[] rows = (UltraGridRow[])this.SparseArray.ToArray( typeof( UltraGridRow ) );
					UltraGridRow[] rows = this.SparseArray.ToArray( true );

					// SSP 7/9/04 UWG3503
					// If the row collection contains group-by rows then return without sorting. This state
					// can occur when the TopRowsCollection has not verified its own group-by version (when
					// the group-by hierarchy changes). The state being referred to here is the state where
					// a row collection contains group-by rows even though its GroupByColumn property 
					// retruns null (we are checking for GroupByColumn above in this method). Also note that
					// we have already set the verify version numbers above. It's not a problem since this
					// row collection will be discarded when the top-level row collection verifies the 
					// group-by hierarchy.
					//
					// --------------------------------------------------------------------------------------
					if ( null != rows[0] && rows[0].IsGroupByRow )
						return;
					// --------------------------------------------------------------------------------------

					// Sort the array.
					//
					// SSP 2/17/04 - Virtual Mode - Optimization
					//
					//Array.Sort( temp, this );
					// MD 9/20/07 - 7.3 Performance
					// Use the generic sort merge, which is faster
					//Infragistics.Win.Utilities.SortMerge( rows, new RowsSortComparer( this ) );
					Infragistics.Win.Utilities.SortMergeGeneric<UltraGridRow>( rows, new RowsSortComparer( this ) );

					this.SparseArray.Clear( );
					this.SparseArray.AddRange( rows );

					// SSP 3/23/05 - NAS 5.2 Fixed Rows
					// Move the fixed rows to top or bottom depending on the fixed rows style. Also ensure
					// that they are in the same order in the main row collection as they are in the fixed
					// row collection.
					//
					this.MoveFixedRowsToProperLocation( );

					// SSP 11/17/04 UWG2234 
					// Changed the behavior so that when a column is sorted we maintain the scroll position
					// instead of keeping the first row the first row.
					//
					this.MaintainScrollPositionHelper( );

					// SSP 8/3/04 - UltraCalc
					// Notify the calc manager.
					//
					this.NotifyCalcManager_RowsCollectionSorted( );
				}

				// SSP 10/31/01 UWG617
				// Bump the rows' scroll position version so that they get recalculated
				// when asked for
				//
				// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
				// Commented out the following line of code. ScrollCountManagerSparseArray itself 
				// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
				// method.
				//
				//this.Band.Layout.BumpScrollRowPositionVersion( );
			}
			finally
			{
				this.inEnsureSortOrder = originalInEnsureSortOrder;	
			}
		}

		#endregion // EnsureSortOrder

		#region IComparer.Compare
		
		int IComparer.Compare( object x, object y )
		{
			int compare = 0;
			object valueX;
			object valueY;

			if ( x == null )
			{
				if ( y == null )
					return 0;

				// if we are descending then flip the sign
				// x > y
				// SSP 11/14/03 Add Row Feature
				// With add-row feature, we are going to sort the rows even when there aren't any sorted
				// columns to ensure that the add row is at the top or the bottom depending on whether
				// add-rows are displayed at the top or at the bottom.
				// 
				//if ( this.band.SortedColumns[0].SortIndicator == SortIndicator.Descending )
				if ( this.band.HasSortedColumns && this.band.SortedColumns[0].SortIndicator == SortIndicator.Descending )
					return 1;

				//x < y
				return -1;
			}

			if ( y == null )
			{
				// if we are descending then flip the sign
				//
				// SSP 11/14/03 Add Row Feature
				// With add-row feature, we are going to sort the rows even when there aren't any sorted
				// columns to ensure that the add row is at the top or the bottom depending on whether
				// add-rows are displayed at the top or at the bottom.
				// 
				//if ( this.band.SortedColumns[0].SortIndicator == SortIndicator.Descending )
				if ( this.band.HasSortedColumns && this.band.SortedColumns[0].SortIndicator == SortIndicator.Descending )
					return -1;

				return 1;
			}

			Infragistics.Win.UltraWinGrid.UltraGridRow rowX = x as Infragistics.Win.UltraWinGrid.UltraGridRow;
			Infragistics.Win.UltraWinGrid.UltraGridRow rowY = y as Infragistics.Win.UltraWinGrid.UltraGridRow;
			Infragistics.Win.UltraWinGrid.UltraGridColumn column;
			IComparer comparer = null;

			// SSP 11/14/03 Add Row Feature
			// Ensure that the add row is at the top or the bottom depending on whether add-rows are 
			// displayed at the top or at the bottom.
			// Added following block of code.
			// 
			// ----------------------------------------------------------------------------------------------
			if ( rowX == rowY )
				return 0;

			if ( null != this.CurrentAddRow && ( rowX == this.CurrentAddRow || rowY == this.CurrentAddRow ) )
			{
				TemplateAddRowLocation addRowLocDef = this.TemplateRowLocationDefault;

				if ( TemplateAddRowLocation.Top == addRowLocDef )
					return rowX == this.CurrentAddRow ? -1 : 1;
				else if ( TemplateAddRowLocation.Bottom == addRowLocDef )
					return rowX == this.CurrentAddRow ? 1 : -1;
			}
			// ----------------------------------------------------------------------------------------------

			for ( int i = 0; i < this.band.SortedColumns.Count; i++ )
			{
				column = this.band.SortedColumns[i];

				// SSP 8/2/02 UWG1481
				// If the column's index is less than 0, then that means we are in the middle
				// of resetting the layout or something like that. Column is invalid (it is not
				// in the band's columns collection anymore, however the sorted columns 
				// collection is still holding the reference to it. So ignore those columns.
				//
				if ( column.Index < 0 )
					continue;

				comparer = column.SortComparer;
				UltraGridCell cellX = null, cellY = null;
				valueX = null;
				valueY = null;

				// SSP 1/10/02 UWG864
				// If we have a user specified comparer, then pass in cell objects
				// for comparing the cells.
				//
				if ( null != comparer )
				{
					cellX = rowX.Cells[column];
					cellY = rowY.Cells[column];

					// SSP 1/10/02 UWG864
					// When comparing through a comparer given by the user, pass in
					// cells as objects for comparing.
					//
					//compare = comparer.Compare( valueX, valueY );
					compare = comparer.Compare( cellX, cellY );
				}
				else
				{
					if ( column.IsBound )
					{
						valueX = rowX.GetCellValue( column );
						valueY = rowY.GetCellValue( column );					
					}
					else
					{
						valueX = rowX.Cells[column].Value;
						valueY = rowY.Cells[column].Value;
					}


					// SSP 8/2/02 UWG1126 UWG1483
					// EM Embeddable editors.
					// Due to embeddable editors, we can't just look at the value list. Use
					// the ComparesByValue method of the editor to determine if the owner
					// should be comparing (for sort as well as other purposes) by text or the
					// data value.
					//
					// ----------------------------------------------------------------
					
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					// Since two different cells in the same column could have two differnt editors,
					// check to see if both editors compare by value before comparing by value.
					//
					
					EmbeddableEditorBase rowXEditor = column.GetEditor( rowX );
					EmbeddableEditorBase rowYEditor = column.GetEditor( rowY );

					bool compareByValue = true;
					if ( rowXEditor == rowYEditor )
					{
						if ( null != rowXEditor && ! rowXEditor.ComparesByValue( column.EditorOwnerInfo, rowX ) )
							compareByValue = false;
					}
					else 
					{
						if ( null != rowXEditor && ! rowXEditor.ComparesByValue( column.EditorOwnerInfo, rowX ) )
							compareByValue = false;
						else if ( null != rowYEditor && ! rowYEditor.ComparesByValue( column.EditorOwnerInfo, rowY ) )
							compareByValue = false;
					}

					if ( ! compareByValue )
					{
						// Note: GetCellText takes into consideration column being unbound.
						//
						valueX = rowX.GetCellText( column );
						valueY = rowY.GetCellText( column );
					}

					// JAS v5.2 GroupBy Break Behavior 5/4/05
					//
					//compare = DefaultCompare( valueX, valueY );
					// SSP 5/16/05 - NAS 5.2 FilterComparisonType property
					//
					//compare = DefaultCompare( valueX, valueY, column );
					//compare = DefaultCompare( valueX, valueY, column );
					compare = RowsCollection.RowsSortComparer.DefaultCompare( valueX, valueY, column );
					// ----------------------------------------------------------------
				}

				if ( compare != 0 )
				{
					// if we are sorting descended then flip the sign
					//
					if ( this.band.SortedColumns[i].SortIndicator == SortIndicator.Descending )
						compare *= -1;					
																										
					return compare;
				}
			}

			// SSP 8/19/02 UWG1273
			// Instead of returning 0 here which will cause the Array.Sort method
			// to put equal rows any way it wishes, return -1 or 1 based on the
			// index so that it will put the row with higher list index after the
			// row with lower list index. The result of this will be that when
			// a column is sorted by and all the cells of that column are blank,
			// the sorting will retain rows' positions. Before Array.Sort was 
			// reordering the rows even when all the rows were equal randomly
			// so to the user it would look as if grid sorted even if it did not 
			// need to sort.
			//
			if ( 0 == compare && null != rowX && null != rowY && rowX != rowY )
			{
				compare = rowX.ListIndex < rowY.ListIndex ? -1 : 1;
			}

			return compare;
		}

		#endregion // IComparer.Compare

		#region DefaultCompare
		
		// SSP 5/16/05
		// Commented out the following method.
		//
		

		#endregion // DefaultCompare

		#region CreateGroupByRowRows
		
		// SSP 11/11/05 BR07685
		// We need to implement the new IGroupByEvaluatorEx interface because we need
		// to make sure that the sorting logic is consistent with the grouping logic.
		// Added rowsAlreadySorted parameter.
		// 
		//private void CreateGroupByRowRows( UltraGridRow[] rows, ref int startIndex )
		private void CreateGroupByRowRows( UltraGridRow[] rows, ref int startIndex, bool rowsAlreadySorted )
		{			
			// clear our list
			this.SparseArray.Clear( );		

			UltraGridGroupByRow parentGroupByRow = this.ParentRow as UltraGridGroupByRow;

			//Debug.WriteLine( "CreateGroupByRowRows start:  startIndex = " + startIndex );
		
			while ( startIndex < rows.Length )
			{
				Infragistics.Win.UltraWinGrid.UltraGridRow row = rows[ startIndex ];				

				if ( null != parentGroupByRow )
				{
					// SSP 1/23/02
					// We always want to add at least one child row to a group by row.
					//
					//if ( !parentGroupByRow.DoesRowMatchGroup( row, true ) )
					if ( this.SparseArray.Count > 0 && 
						!parentGroupByRow.DoesRowMatchGroup( row, true ) )
					{
						break;
					}
				}

				this.SparseArray.Add( row );
				
				row.InitRowsCollection( this );

				startIndex++;
			}

			this.rowsDirty = false;

			//Debug.WriteLine( "CreateGroupByRowRows end:  startIndex = " + startIndex );

			// set the sort version, since these are sorted in the right order
			// already so that we don't resort them unnecessarily
			//
			// SSP 11/11/05 BR07685
			// We need to implement the new IGroupByEvaluatorEx interface because we need
			// to make sure that the sorting logic is consistent with the grouping logic.
			// Added rowsAlreadySorted parameter. Only set the sortVersion if 
			// rowsAlreadySorted parameter is true.
			// 
			//this.sortVersion = this.band.SortedColumns.SortVersion;
			if ( rowsAlreadySorted )
				this.sortVersion = this.band.SortedColumns.SortVersion;
		}

		#endregion // CreateGroupByRowRows

		#region CreateGroupByRowsHelper
		
		// SSP 11/11/05 BR07685
		// We need to implement the new IGroupByEvaluatorEx interface because we need
		// to make sure that the sorting logic is consistent with the grouping logic.
		// Added rowsAlreadySorted parameter.
		// 
		//private void CreateGroupByRowsHelper( UltraGridRow[] rows, ref int startIndex )
		private void CreateGroupByRowsHelper( UltraGridRow[] rows, ref int startIndex, bool rowsAlreadySorted )
		{	
			UltraGridColumn column = this.GroupByColumn;

			// clear our list
			ScrollCountManagerSparseArray sparseArr = this.SparseArray;
			sparseArr.Clear( );		

			UltraGridGroupByRow parentGroupByRow = this.ParentRow as UltraGridGroupByRow;
			UltraGridGroupByRow groupByRow = null;

			//Debug.WriteLine( "CreateGroupByRowsHelper Start:  startIndex = " + startIndex );
			
			while ( startIndex < rows.Length )
			{
				Infragistics.Win.UltraWinGrid.UltraGridRow row = rows[ startIndex ];				

				if ( null != parentGroupByRow )
				{
					// SSP 1/23/02
					// We always want to add at least one child row to a group by row.
					//
					//if ( !parentGroupByRow.DoesRowMatchGroup( row, true ) )
					if ( sparseArr.Count > 0 
						&& !parentGroupByRow.DoesRowMatchGroup( row, true ) )
					{
						break;
					}
				}				
				
				// Is it time to create a new GroupByRow ?
				if ( null == groupByRow || !groupByRow.DoesRowMatchGroup( row, false ) )
				{
					// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
					// Use the new overload.
					// 
					//groupByRow = new UltraGridGroupByRow( this.band, this, column, row.GetCellValue( column ) );
					groupByRow = new UltraGridGroupByRow( this.band, this, column, row );
					
					sparseArr.Add( groupByRow );

					// JAS v5.2 GroupBy Break Behavior 5/3/05
					//
					//IGroupByEvaluator evaluator = column.GroupByEvaluator;
					IGroupByEvaluator evaluator = column.GroupByEvaluatorResolved;

					// JJD 1/17/02
					// If they have specified an evaulator then get the group by value 
					//
					if ( evaluator != null )
						groupByRow.InitializeCommonValue( evaluator.GetGroupByValue( groupByRow, row ) );

					int oldStartIndex = startIndex;
					
					UltraGridChildBand childBand = groupByRow.ChildBands[0];
					if ( childBand.IsGroupByChildBand )
					{
						// SSP 11/11/05 BR07685
						// We need to implement the new IGroupByEvaluatorEx interface because we need
						// to make sure that the sorting logic is consistent with the grouping logic.
						// Added rowsAlreadySorted parameter.
						// 
						//childBand.Rows.CreateGroupByRowsHelper( rows, ref startIndex );
						childBand.Rows.CreateGroupByRowsHelper( rows, ref startIndex, rowsAlreadySorted );
					}
					else
					{
						// SSP 11/11/05 BR07685
						// We need to implement the new IGroupByEvaluatorEx interface because we need
						// to make sure that the sorting logic is consistent with the grouping logic.
						// Added rowsAlreadySorted parameter.
						// 
						//childBand.Rows.CreateGroupByRowRows( rows, ref startIndex );
						childBand.Rows.CreateGroupByRowRows( rows, ref startIndex, rowsAlreadySorted );
					}

					Debug.Assert( startIndex != oldStartIndex, "Some rows should have been added to the child group by rows" );

					if ( startIndex <= oldStartIndex )
						startIndex = 1 + oldStartIndex;

					// SSP 9/27/01
					// Added code to fire InitializeRow event for a newly
					// created group by row
					//
					// SSP 1/11/05
					// We should not be catching the exceptions thrown by developers. Commented out
					// the try-catch portion.
					//
					//try
					//{
					groupByRow.FireInitializeRow( );
					//}
					//catch ( Exception )
					//{
					//}
				}
			}

			//Debug.WriteLine( "CreateGroupByRowsHelper End:  startIndex = " + startIndex );

			// SSP 1/11/05 UWG3747
			// Added GroupByComparer property to allow the user to sort group-by 
			// rows using a custom comparer.
			//
			// ----------------------------------------------------------------------------
			if ( null != column.GroupByComparer )
			{
				sparseArr.Sort( column.GroupByComparer );
				if ( SortIndicator.Ascending != column.SortIndicator )
					sparseArr.Reverse( );
			}
			// ----------------------------------------------------------------------------

			this.rowsDirty = false;

			// set these version numbers so InitGroupByRows does not get called
			// again in VerifyGroupByVersion( ) function
			//
			// SSP 1/16/02 
			// Since we are only looking at the SortDirection, we don't
			// need to have a version. We just verify against the sort direction.
			//
			//this.verifiedGroupBySortVersion = this.GroupByColumn.GroupBySortVersion;
			this.verifiedGroupBySortDirectionAscending = SortIndicator.Ascending == column.SortIndicator;

			this.verifiedGroupByHierarchyVersion = column.GroupByHierarchyVersion;
			this.verifiedGroupByHierarchyBandsVersion = this.band.GroupByHierarchyVersion;
		}

		// SSP 11/11/05 BR07685
		//
		

		#endregion // CreateGroupByRowsHelper

		#region EnsureAddRowPositionedCorrectly
		
		internal void EnsureAddRowPositionedCorrectly( )
		{
			// MD 7/27/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( null != this.currentAddRow && TemplateAddRowLocation.None != this.TemplateRowLocationDefault )
			//{
			//    int addRowIndex = TemplateAddRowLocation.Top == this.TemplateRowLocationDefault 
			//        ? 0 : this.SparseArray.Count - 1;
			TemplateAddRowLocation templateRowLocationDefault = this.TemplateRowLocationDefault;

			if ( null != this.currentAddRow && TemplateAddRowLocation.None != templateRowLocationDefault )
			{
				int addRowIndex = TemplateAddRowLocation.Top == templateRowLocationDefault
					? 0 : this.SparseArray.Count - 1;

				int actualAddRowIndex = this.SparseArray.IndexOf( this.currentAddRow );
				if ( actualAddRowIndex >= 0 && addRowIndex >= 0 && actualAddRowIndex != addRowIndex )
				{
					this.SparseArray.RemoveAt( actualAddRowIndex );
					this.SparseArray.Insert( addRowIndex, this.currentAddRow );
				}
			}
		}

		#endregion // EnsureAddRowPositionedCorrectly

		#region InitNonGroupByRows

		private void InitNonGroupByRows( )
		{
			this.InitNonGroupByRows( null );
		}

		// SSP 7/30/04 UWG3581
		// Added an overload of InitNonGroupByRows that takes in 
		// fireInitializeRowOnTheseRows parameter.
		//
		//private void InitNonGroupByRows( )
		private void InitNonGroupByRows( IList fireInitializeRowOnTheseRows )
		{
			if ( ! this.inInitNonGroupByRows )
			{
				this.inInitNonGroupByRows = true;

				try
				{
					Debug.Assert( ! this.band.HasGroupBySortColumns && ! ( this.ParentRow is UltraGridGroupByRow ), "InitNonGroupByRows should only get called when there are no group-by columns." );
					if ( this.ParentRow is UltraGridGroupByRow )
						return;

					// SSP 3/31/05 BR03040 - Optimization
					// Clear the merged cells so we don't try to process merged cells collection when
					// sparse array sends notifications as a result of below code.
					//
					this.ClearMergedCellCache( );

					RowsCollection.RowsSparseArray rowsList = this.UnSortedActualRows;

					// SSP 4/13/04 - Virtual Binding
					//
					//foreach ( UltraGridRow row in rowsList )
					foreach ( UltraGridRow row in rowsList.NonNullItems )
					{
						// Initialize the parent collection of the row to this.
						// 
						row.InitRowsCollection( this );
					}

					this.SparseArray.Clear( );

					// SSP 11/14/03 Add Row Feature
					// If we are displaying template add-rows, then sort rows even if there are no
					// sorted columns to ensure that the added row is at the location where it's supposed to
					// be (ie. either at the top or at the bottom depending on whether add-rows are displayed
					// at the top or at the bottom.
					//
					// SSP 4/13/04 - Virtual Binding 
					// If there are no sort columns and the add-row is at the correct location in the unsorted
					// rows collection then don't force sort.
					//
					// ----------------------------------------------------------------------------------------
					//bool performSort =  this.band.HasSortedColumns || TemplateAddRowLocation.None != this.TemplateRowLocationDefault;
					bool performSort =  this.band.HasSortedColumns;

					// Added ExternalSortSingle and ExternalSortMulti members. If the header click action 
					// is ExternalSortSingle or ExternalSortMulti then don't sort.
					//
					if ( performSort )
					{
						HeaderClickAction headerClickAction = this.band.GetHeaderClickActionResolved( false );
						if ( HeaderClickAction.ExternalSortSingle == headerClickAction 
							|| HeaderClickAction.ExternalSortMulti == headerClickAction )
							performSort = false;
					}
					// ----------------------------------------------------------------------------------------
					
					if ( ! performSort )
					{
						this.SparseArray.AddRange( rowsList );

                        // MBS 11/18/08 - TFS10304
                        // We base whether we have a special row, such as a TemplateAddRow, on whether
                        // or not we have already hit the maximum number of rows for the given collection.
                        // However, we may have calculated and cached the special rows before we've 
                        // initialized the rows in the collection, so we will now be out of sync with
                        // the valid number allowed; this can lead to an infinite loop when calculating
                        // visible and scrollable metrics since we are constantly adding and removing
                        // the TemplateAddRow.
                        this.verifiedSpecialRowsCacheVersion--;

						// SSP 4/13/04 - Virtual Binding Related Optimizations
						//
						this.EnsureAddRowPositionedCorrectly( );

						// SSP 7/30/04 UWG3581
						// Fire InitializeRow before sorting so if there are any unbound sort columns
						// then the developer is given a chance to pupulate the cell values of those
						// unbound columns before sorting the rows based on that column.
						//
						// --------------------------------------------------------------------------
						if ( null != fireInitializeRowOnTheseRows )
						{
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//this.FireInitializeRow( fireInitializeRowOnTheseRows );
							RowsCollection.FireInitializeRow( fireInitializeRowOnTheseRows );
						}
						// --------------------------------------------------------------------------
					}
					else
					{
						
						
						
						//Infragistics.Win.UltraWinGrid.UltraGridRow[] rowsArray = rowsList.ToArray( true );
						// SSP 9/6/07 BR24503
						// ToArray method call will potentially create new rows and also raise
						// InitializeRow event on them. Any new rows will also be added to rowsList.
						// If fireInitializeRowOnTheseRows is the same as rowsList then that means we
						// will end up firing InitializeRow twice on those created rows. Once in the
						// ToArray and then below it. Therefore if fireInitializeRowOnTheseRows is the
						// same as rowsList then prevent ToArray from raising the InitializeRow as we
						// are going to be raising it on all rows below if
						// fireInitializeRowOnTheseRows is the same as rowsList.
						// 
						//Infragistics.Win.UltraWinGrid.UltraGridRow[] rowsArray = rowsList.ToArray( true, false );
						Infragistics.Win.UltraWinGrid.UltraGridRow[] rowsArray = rowsList.ToArray( true, false, rowsList == fireInitializeRowOnTheseRows );

						// SSP 7/30/04 UWG3581
						// Fire InitializeRow before sorting so if there are any unbound sort columns
						// then the developer is given a chance to pupulate the cell values of those
						// unbound columns before sorting the rows based on that column.
						//
						// --------------------------------------------------------------------------
						if ( null != fireInitializeRowOnTheseRows )
						{
							this.SparseArray.AddRange( rowsArray );

							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//this.FireInitializeRow( fireInitializeRowOnTheseRows );
							RowsCollection.FireInitializeRow( fireInitializeRowOnTheseRows );
						}
						// --------------------------------------------------------------------------

						// SSP 2/17/04 - Virtual Binding - Optimization
						//
						//Array.Sort( rowsArray, (IComparer)this );
						Infragistics.Win.Utilities.SortMerge( rowsArray, new RowsSortComparer( this ) );

						// SSP 7/30/04 UWG3581
						// Related to change above. Clear the list of the previously added rows before
						// readding them again below.
						//
						this.SparseArray.Clear( );

						this.SparseArray.AddRange( rowsArray );
					}

					// SSP 11/17/04 UWG2234 
					// Changed the behavior so that when a column is sorted we maintain the scroll position
					// instead of keeping the first row the first row.
					//
					this.MaintainScrollPositionHelper( );

					this.sortVersion = this.band.SortedColumns.SortVersion;

					// SSP 1/22/04 - Optimization
					// Also set verifiedGroupByHierarchyBandsVersion otherwise we will get called
					// a second time unnecessarily by VerifyGroupByVersion method.
					//
					this.verifiedGroupByHierarchyBandsVersion = this.band.GroupByHierarchyVersion;
				}
				finally
				{
					this.inInitNonGroupByRows = false;

					// SSP 9/2/04
					// Notify the calc manager of the change in the group-by hierarchy.
					//
					this.NotifyCalcManager_RowsCollectionResynced( );
				}
			}


			// SSP 1/22/04
			// Original code.
			//
			
		}

		#endregion // InitNonGroupByRows

		#region InitGroupByRows
		
		private void InitGroupByRows( )
		{
			if ( this.inInitGroupByRows )
				return;

			
			// structure for this rows collection and not the top level group by
			// rows collection ( which is what we are doing )
			if ( !this.IsTopLevel )
			{
				this.TopLevelRowsCollection.InitGroupByRows( );                
				return;
			}

			this.inInitGroupByRows = true;

			try
			{
				// SSP 6/4/02
				//
				//this.verifyVersion++;
				this.BumpVerifyVersion( );

				// set the rowsDirty flag to false
				this.rowsDirty = false;

				// SSP 3/31/05 BR03040 - Optimization
				// Clear the merged cells so we don't try to process merged cells collection when
				// sparse array sends notifications as a result of below code.
				//
				this.ClearMergedCellCache( );

				// SSP 2/20/04 - Optimization
				//
				//ArrayList rows = this.UnSortedActualRows;
				//rows = (ArrayList)rows.Clone( );
				//rows.Sort( (IComparer)this );
				// SSP 4/13/04 - Virtual Binding
				//
				//UltraGridRow[] rows = (UltraGridRow[])this.UnSortedActualRows.ToArray( typeof( UltraGridRow ) );
				
				
				
				//UltraGridRow[] rows = this.UnSortedActualRows.ToArray( true );
				UltraGridRow[] rows = this.UnSortedActualRows.ToArray( true, false );

				// SSP 11/11/05 BR07685
				// We need to implement the new IGroupByEvaluatorEx interface because we need
				// to make sure that the sorting logic is consistent with the grouping logic.
				// Pass in true for the new groupingRows parameter.
				// 
				//Infragistics.Win.Utilities.SortMerge( rows, new RowsCollection.RowsSortComparer( this ) );
				RowsCollection.RowsSortComparer rowsSortComparer = new RowsCollection.RowsSortComparer( this, true );
				Infragistics.Win.Utilities.SortMerge( rows, rowsSortComparer );

				Infragistics.Win.UltraWinGrid.UltraGridRow currentActiveRow = this.Layout.ActiveRow;

				// SSP 8/26/05 BR05797
				// If the user had activated the template add-row and then attempts to group rows then 
				// make sure we don't commit the template add-row if it's not been modified.
				// 
				// ------------------------------------------------------------------------------------
				if ( null != currentActiveRow && currentActiveRow.IsUnModifiedAddRowFromTemplate )
				{
					// Cancel the add-row since the template add-row hasn't been modified by the user.
					// 
					currentActiveRow.CancelEdit( );

					// Reget the active row in case it changes.
					// 
					currentActiveRow = this.Layout.ActiveRow;
				}
				// ------------------------------------------------------------------------------------
				
				// SSP 11/11/05 BR07685
				// We need to implement the new IGroupByEvaluatorEx interface because we need
				// to make sure that the sorting logic is consistent with the grouping logic.
				// Added rowsAlreadySorted parameter.
				// 
				//this.CreateGroupByRowsHelper( rows );
				int startIndex = 0;
				this.CreateGroupByRowsHelper( rows, ref startIndex, ! rowsSortComparer.groupByEvaluatorExUsed );

				// SSP 11/19/03
				// Commented following block of code out.
				//
				
				
				// SSP 10/22/01
				// Don't set the FirstRow of all the row scroll regions to null
				// even if we don't have to.
				//
				//this.Band.Layout.RowScrollRegions.DirtyAllVisibleRows( true );
				if ( null == this.ParentRow || this.ParentRow.Expanded )
				{
					this.Layout.RowScrollRegions.ResetFirstRowIfDescendantOf( this );
					this.Layout.RowScrollRegions.DirtyAllVisibleRows( false );
				}

				// SSP 11/19/03
				// Added following block of code.
				//
				UltraGridRow activeRow = null;
				// SSP 1/2/04 UWG2812
				// Check for the list count begin greater than 0.
				//
				if ( this.Count > 0 )
				{					
					if ( null == this.parentRow )
					{
						activeRow = this[0];
					}
					else if ( null != currentActiveRow )
					{
						if ( currentActiveRow.IsDescendantOf( this.parentRow ) )
						{
							if ( null == this.parentRow || this.parentRow.Expanded && this.parentRow.AllAncestorsExpanded )
								activeRow = this[0];
						}
					}
				}

				// SSP 11/19/03
				// Commented following block of code out.
				//
				

                // MBS 5/7/08 - BR32580
                // Moved from within the 'if' block below
                UltraGrid grid = this.Layout.Grid as UltraGrid;

				// SSP 1/17/02 UWG937
				// Only set the active row if this is a display layout.
				//
				// SSP 11/19/03
				// 
				//if ( this.Band.Layout.IsDisplayLayout )
				if ( null != activeRow && this.Layout.IsDisplayLayout )
				{
                    // MBS 5/7/08 - BR32580
                    // Moved above
                    //UltraGrid grid = this.Layout.Grid as UltraGrid;

					this.Layout.Grid.ActiveRow = activeRow;				

					if ( null != grid && null != grid.ActiveRow )
					{
						// SSP 6/31/02
						// If the active rows is already in the selected rows collection,
						// then don't add it to it.
						//
						// SSP 7/8/02 UWG1154
						// If the select type is none, then don't select the row.
						//
						if ( SelectType.None != this.Band.SelectTypeGroupByRowResolved &&
							!grid.Selected.Rows.Contains( grid.ActiveRow ) )
						{
							// SSP 8/2/02 UWG1482
							// Select the row by calling InternalSelectItem and also set the 
							// pivot item to that row.
							// 
							//grid.Selected.Rows.InternalAdd( this.Band.Layout.Grid.ActiveRow );
							// SSP 5/13/03
							// Set the dontUpdateWhenSelecting flag to true to prevent InternalSelectItem
							// from calling Update on the grid because we don't want to prematurely update
							// the control while we are in the process of constructing the group-by hierarchy.
							// 
							bool origDontUpdateWhenSelecting = this.Layout.DontUpdateWhenSelecting;
							this.Layout.DontUpdateWhenSelecting = true;
							try
							{
								grid.InternalSelectItem( grid.ActiveRow, true, true );
								grid.SetPivotItem( grid.ActiveRow, false );							
							}
							finally
							{
								this.Layout.DontUpdateWhenSelecting = origDontUpdateWhenSelecting;
							}
						}
					}
				}
                // MBS 5/7/08 - BR32580
                // Perform the verification for the active row at this point to make sure that
                // we do not continue to hold onto a row that is invalid and needs to be disposed
                else if (grid != null)
                {
                    grid.VerifyActiveRowValid();
                }
			}
			finally
			{
				this.inInitGroupByRows = false;

				// SSP 9/2/04
				// Notify the calc manager of the change in the group-by hierarchy.
				//
				this.NotifyCalcManager_RowsCollectionResynced( );
			}

			// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
			// Commented out the following line of code. ScrollCountManagerSparseArray itself 
			// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
			// method.
			//
			//this.DirtyScrollableRowCountHelper( );
			
			// SSP 10/5/01
			//
			//this.Band.Layout.RowScrollRegions.DirtyAllVisibleRows( );
			// SSP 10/22/01
			// Commented this out because it's getting called above inside 
			// try catch statement
			//
			//this.band.Layout.RowScrollRegions.DirtyAllVisibleRows(true);
		}
	
		#endregion // InitGroupByRows

		#region InitRows
		
		internal void InitRows( )
		{
			// SSP 9/21/01
			// Since SyncRows and InitRows were doing almost the same
			// things, merged them into one function
			//
			this.SyncRows( );
			
		}

		#endregion // InitRows

		#region SyncRows_Add
		
		internal void SyncRows_Add( UltraGridRow row )
		{
			this.SyncRows_Add( row, -1 );
		}

		// SSP 4/12/02 UWG1085
		// Added an overload that takes in desiredIndex.
		//
		internal void SyncRows_Add( UltraGridRow row, int desiredIndex )
		{
			if ( !this.IsGroupByRows )
			{
				// SSP 4/12/02 UWG1085
				// If newly added parameter desiredIndex is specified, then insert
				// the row at that position.
				//
				if ( desiredIndex >= 0 && desiredIndex < this.SparseArray.Count )
				{
					this.SparseArray.Insert( desiredIndex, row );
				}
				else
				{
					// SSP 11/14/03 Add Row Feature
					// Enclosed the existing code in the else block while added the if block. We need to
					// make sure that when a new row is added via template add-row, if the template add
					// row is at the top, then we need to add it at the top.
					//
					if ( row == this.CurrentAddRow && TemplateAddRowLocation.Top == this.TemplateRowLocationDefault )
					{
						this.SparseArray.Insert( 0, row );
					}
					else
					{
						this.SparseArray.Add( row );
					}
				}
				
				// SSP 3/4/02
				// Make sure the ParentCollection of the row refers to the 
				// correct parent collection.
				//
				if ( row.ParentCollection != this )
				{
					row.InitRowsCollection( this );
				}

				// SSP 7/26/04 - UltraCalc
				// Notify the calc manager if any of the row deletion.
				//
				// ----------------------------------------------------------------------
				this.NotifyCalcManager_RowAdded( row );
				// ----------------------------------------------------------------------            

				return;
			}
			
			UltraGridGroupByRow groupByRow = null;

			foreach ( UltraGridGroupByRow gr in this.SparseArray )
			{
				if ( gr.DoesRowMatchGroup( row ) )
				{
					groupByRow = gr;
					break;
				}
			}

			if ( null != groupByRow )
			{
				// if a matching group by row is found, then drill down
				// the child rows and add it to appropriate group by row's
				// rows collection
				//
				groupByRow.Rows.SyncRows_Add( row );
			}
			else
			{	
				UltraGridColumn groupByColumn = this.GroupByColumn;

				// if no matching groupByRow was found, then we have to create
				// a new group by row and add row to it's rows collection
				//
				// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
				// Use the new overload.
				// 
				//UltraGridGroupByRow newGroupByRow = new UltraGridGroupByRow( this.band, this, groupByColumn, row.GetCellValue( groupByColumn ) );
				UltraGridGroupByRow newGroupByRow = new UltraGridGroupByRow( this.band, this, groupByColumn, row );

				// JAS v5.2 GroupBy Break Behavior 5/3/05
				//
				//IGroupByEvaluator evaluator = groupByColumn.GroupByEvaluator;
				IGroupByEvaluator evaluator = groupByColumn.GroupByEvaluatorResolved;

				// JJD 1/17/02
				// If they have specified an evaulator then get the group by value 
				//
				if ( evaluator != null )
					newGroupByRow.InitializeCommonValue( evaluator.GetGroupByValue( newGroupByRow, row ) );

				// SSP 12/20/04 BR00935
				// Added GetGroupByRowInsertLocation and EnsureCorrectSortPosition methods.
				//
				// ----------------------------------------------------------------------------
				//this.SparseArray.Add( newGroupByRow );
				int insertAt = this.GetGroupByRowInsertLocation( newGroupByRow );
				if ( insertAt >= 0 )
					this.SparseArray.Insert( insertAt, newGroupByRow );
				else 
					this.SparseArray.Add( newGroupByRow );
				// ----------------------------------------------------------------------------

				newGroupByRow.InitRowsCollection( this );

				newGroupByRow.Rows.SyncRows_Add( row );

                // MBS 1/27/09 - TFS11602
                // After talking with Sandip, it seems that since this method is meant to specifically handle
                // adding a row and not cause the root Rows collection to be regenerated.  Therefore, update
                // the groupby version here so that we don't blow away the root rows colletion 
                this.verifiedGroupByHierarchyVersion = groupByColumn.GroupByHierarchyVersion;

				// SSP 7/26/04 - UltraCalc
				// Notify the calc manager if any of the row deletion.
				//
				// ----------------------------------------------------------------------
				this.NotifyCalcManager_RowAdded( newGroupByRow );
				// ----------------------------------------------------------------------            

				// SSP 9/27/01  UWG357
				// Added code to fire InitializeRow event for a newly
				// created group by row
				//
				try
				{
					newGroupByRow.FireInitializeRow( );
				}
				catch ( Exception )
				{
				}
			}
		}

		#endregion // SyncRows_Add

		#region SyncRows_RemoveHelper
		
		private void SyncRows_RemoveHelper( )
		{
			UltraGridGroupByRow parentGroupByRow = this.ParentRow as UltraGridGroupByRow;

			if ( null != parentGroupByRow &&
				0 == parentGroupByRow.Rows.Count )
			{
				// SSP 7/26/04 - UltraCalc
				// Notify the calc manager if any of the row deletion.
				//
				// ----------------------------------------------------------------------
				if ( null != parentGroupByRow.ParentCollection )
					parentGroupByRow.ParentCollection.NotifyCalcManager_RowDeleted( parentGroupByRow );
				// ----------------------------------------------------------------------            

				parentGroupByRow.ParentCollection.SparseArray.Remove( parentGroupByRow );

				// SSP 10/3/01 UWG372
				// Set the deleted flag to true
				//
				parentGroupByRow.deleted = true;


				parentGroupByRow.ParentCollection.SyncRows_RemoveHelper( );
			}
		}

		#endregion // SyncRows_RemoveHelper

		#region SyncRows_Remove

		// SSP 3/4/02
		// Added this overload
		//
		internal void SyncRows_Remove( UltraGridRow row )
		{
			this.SyncRows_Remove( row, true );
		}

		#endregion // SyncRows_Remove

		#region EnsureProperFirstRowInRowScrollRegions

		// SSP 9/19/02 UWG1678
		//
		internal void EnsureProperFirstRowInRowScrollRegions( UltraGridRow rowBeingDeleted )
		{
			RowScrollRegionsCollection rsrCollection = 
				null != this.Layout 
				? this.Layout.RowScrollRegions
				: null;

			if ( null != rsrCollection )
			{
				for ( int i = 0; i < rsrCollection.Count; i++ )
				{
					RowScrollRegion rsr = rsrCollection[i];
 
					// SSP 8/7/03 UWG1963
					// Added dontSyncRows flag to prevent the syncing of the rows in some situations.
					// AdjustFirstRow can end up accessing the rows collection and may end up causing
					// SyncRows. We want to prevent that from happening.
					//
					bool origDontSyncRows = this.dontSyncRows;
					this.dontSyncRows = true;
					try
					{
						rsr.AdjustFirstRow( rowBeingDeleted );
					}
					finally
					{
						this.dontSyncRows = origDontSyncRows;
					}
				}
			}
		}

		#endregion // EnsureProperFirstRowInRowScrollRegions

		#region SyncRows_Remove
		
		internal void SyncRows_Remove( UltraGridRow row, bool setDeletedFlag )
		{
			// SSP 6/12/02
			// Calling UltraGridRow.ParentRow causes the rows to be synced if they are dirty.
			// We don't want to sync the rows in the middle of another sync operation (from
			// which we are being called from). This does not cause an infinite operation
			// it's just that it slows things down. This is only noticable when all the
			// rows are recursively loaded. If this causes problems, then remove it.
			//
			//UltraGridGroupByRow parentGroupByRow = row.ParentRow as UltraGridGroupByRow;			
			UltraGridGroupByRow parentGroupByRow =
				null != row.ParentCollection 
				? ( row.ParentCollection.ParentRow as UltraGridGroupByRow )
				: null;

			// SSP 9/19/02 UWG1678
			// If the first row of a scroll region happens to be the row that's being 
			// deleted, then next time the grid gets painted, the grid will be scrolled
			// all the way to the top.
			//
			this.EnsureProperFirstRowInRowScrollRegions( row );

			// SSP 7/26/04 - UltraCalc
			// Notify the calc manager if any of the row deletion.
			//
			// ----------------------------------------------------------------------
			if ( null != row.ParentCollection )
				row.ParentCollection.NotifyCalcManager_RowDeleted( row );
			// ----------------------------------------------------------------------            

			row.ParentCollection.SparseArray.Remove( row );
			
			// SSP 10/3/01 UWG372
			// Set the deleted flag to true
			//
			// SSP 3/4/02 
			// Look at the newly added setDeletedFlag parameter.
			//
			if ( setDeletedFlag )
				row.deleted = true;

			row.ParentCollection.SyncRows_RemoveHelper( );

			// SSP 3/12/04 UWG3072
			// We need to make sure when a row is removed, it doesn't stay in 
			// the selected rows collection.
			//
			// SSP 7/14/06 BR14019
			// Deselect the row only if the row is being removed. If it's just being moved
			// around from one group to another, don't deselect it. Added the if condition
			// to the existing code.
			// 
			if ( setDeletedFlag )
				this.EnsureNotInSelectedRowsCollection( row );
		}

		#endregion // SyncRows_Remove

		#region EnsureCorrectSortPosition

		// SSP 3/4/02
		// Added this helper function for UltraGridRow.RefreshSortPosition
		//
		internal void EnsureCorrectSortPosition( UltraGridRow row )
		{
			Debug.Assert( null != row, "Null row passed in." );

			if ( null == row )
				return;

			Debug.Assert( !( row is UltraGridGroupByRow ),
				"EnsureCorrectSortPosition can only be called with row that is not a group by row." );

			if ( row is UltraGridGroupByRow )
				return;

			if ( !this.Contains( row ) )
			{
				Debug.Assert( false, "Passed in row object does not belong to this rows collection." );
				return;
			}

			// If the rows collection is dirty and needs synced or needs the 
			// whole group by rows hierarchy recreated or the row criteria
			// has changed and needs the whole collection resorted, then do
			// so.
			//
			this.EnsureNotDirty( );
			
			// If there is no sort criteria on the band, then just return.
			//
			if ( !this.Band.HasSortedColumns )
				return;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//int rowCount = this.Count;

			// SSP 5/0/04 - Virtual Binding
			//
			//ArrayList rowList = this.List;
			IList rowList = this.SparseArray;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//rowCount = rowList.Count;

			int oldRowPosition = rowList.IndexOf( row );
			
			Debug.Assert( oldRowPosition >= 0, "row in not rowsList !" );
			if ( oldRowPosition < 0 )
				return;

			// SSP 3/23/05 - NAS 5.2 Fixed Rows
			// Commented out the original code. Use the new MoveRowToCorrectSortOrder method
			// instead which takes into account the fixed rows. Also it maintains the scrollbar
			// position so the fact that the following commented out code contained fixes
			// for UWG1993 should not be a problem because maintaing scroll position should
			// kepp UWG1993 fixed.
			// 
			// ----------------------------------------------------------------------------------
			this.MoveRowToCorrectSortOrder( row );
			
			// ----------------------------------------------------------------------------------
		}

		// SSP 12/20/04 BR00935
		// Added GetGroupByRowInsertLocation and EnsureCorrectSortPosition methods.
		//
		private int GetGroupByRowInsertLocation( UltraGridGroupByRow groupByRow )
		{
			UltraGridColumn column = this.GroupByColumn;

			if ( groupByRow.ParentCollection != this || null == this.GroupByColumn || ! this.IsGroupByRows )
			{
				Debug.Assert( false );
				return -1;
			}

			SparseArray sparseArr = this.SparseArray;
			UltraGridColumn groupByColumn = this.GroupByColumn;
			bool ascending = SortIndicator.Ascending == groupByColumn.SortIndicator;
			IComparer groupBySortComparer = groupByColumn.GroupByComparer;
			IComparer columnSortComparer = groupByColumn.SortComparer;

			int i = 0, j = sparseArr.Count - 1;
			int currIndex = sparseArr.IndexOf( groupByRow );
			while ( i <= j )
			{
				int m = ( i + j ) / 2;

				UltraGridGroupByRow iiGroupByRow = (UltraGridGroupByRow)sparseArr[m];
				int r;
				// SSP 3/23/05
				//
				// ------------------------------------------------------------------
				//if ( iiGroupByRow == groupByRow )
				//	r = 0;
				//else 
				if ( iiGroupByRow == groupByRow )
				{
					if ( i != j )
						iiGroupByRow = (UltraGridGroupByRow)sparseArr[ ++m ];
					else
						break;
				}
				// ------------------------------------------------------------------

				if ( null != groupBySortComparer )
					r = groupBySortComparer.Compare( groupByRow, iiGroupByRow );
				else if ( null != columnSortComparer 
					&& groupByRow.Rows.Count > 0 && iiGroupByRow.Rows.Count > 0 )
					r = columnSortComparer.Compare( 
						groupByRow.Rows[0].Cells[ groupByColumn ],
						iiGroupByRow.Rows[0].Cells[ groupByColumn ] );
				else 
					// JAS v5.2 GroupBy Break Behavior 5/4/05 - Added the 'column' argument.
					//
					//r = RowsCollection.RowsSortComparer.DefaultCompare( 
					//	groupByRow.Value, iiGroupByRow.Value );
					r = RowsCollection.RowsSortComparer.DefaultCompare( 
						groupByRow.Value, iiGroupByRow.Value, column );

				if ( ! ascending )
					r = -r;

				// SSP 3/29/05
				// Commented out the original code and added the new code below it.
				//
				// ------------------------------------------------------------------
				//if ( r < 0 )
				//	j = m - 1;
				//else if ( r >= 0 )
				//	i = m + 1;

				if ( r < 0 )
					j = m - 1;
				else if ( r > 0 )
					i = m + 1;
					// If the row is where it should be then don't move it. That's what the
					// the below else if does.
					//
				else if ( currIndex >= 0 && currIndex < m )
					j = m - 1;
				else
					i = m + 1;
				// ------------------------------------------------------------------
			}

			return i;
		}
		
		// SSP 12/20/04 BR00935
		// Added GetGroupByRowInsertLocation and EnsureCorrectSortPosition methods.
		//
		internal void EnsureCorrectSortPosition( UltraGridGroupByRow groupByRow )
		{
			int currentIndex = this.IndexOf( groupByRow );
			if ( currentIndex >= 0 )
			{
				int i = this.GetGroupByRowInsertLocation( groupByRow );
				if ( i != currentIndex && i >= 0 )
				{
					this.SparseArray.RemoveAt( currentIndex );
					if ( currentIndex < i )
						i--;
					this.SparseArray.Insert( i, groupByRow );
				}
			}
		}

		#endregion // EnsureCorrectSortPosition

		#region EnsureNotInSelectedRowsCollection

		// SSP 3/12/04 UWG3072
		// Added EnsureNotInSelectedRowsCollection method. We need to make sure when a row is
		// removed, it doesn't stay in the selected rows collection.
		//
		private void EnsureNotInSelectedRowsCollection( UltraGridRow row )
		{
			// SSP 6/21/04 UWG3398
			// We also need to remove the row's cells from the selected cells collection.
			// Moved the check to see if the row is selected right before we go to remove
			// it below.
			//
			//if ( null != row && row.Selected )
			if ( null != row )
			{
				UltraGrid grid = this.Band.Layout.Grid as UltraGrid;
				if ( null == grid || !grid.HasSelectedBeenAllocated 
					// SSP 1/22/04
					// Also if this is a print or an export layout, return.
					//
					|| grid.DisplayLayout != this.Band.Layout )
					return;

				if ( row.Selected )
				{
					int index = grid.Selected.Rows.IndexOf( row );
					if ( index >= 0 )
						grid.Selected.Rows.RemoveAt( index );
				}

				// SSP 6/21/04 UWG3398
				// Also make sure that we remove the row's cells from the selected
				// cells collection.
				//
				if ( grid.Selected.HasCells && row.HasCells )
					row.Cells.RemoveCellsFromSelected( grid.Selected.Cells );
			}
		}

		#endregion // EnsureNotInSelectedRowsCollection

		#region UnSelectRowsNotInCollection

		// SSP 9/19/02 UWG1670
		// Added UnSelectRowsNotInCollection method.
		//
		private void UnSelectRowsNotInCollection( )
		{
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if ( null == grid || !grid.HasSelectedBeenAllocated 
				// SSP 1/22/04
				// Also if this is a print or an export layout, return.
				//
				|| grid.DisplayLayout != this.Band.Layout )
				return;

			Infragistics.Win.UltraWinGrid.Selected selection = grid.Selected;

			if ( selection.HasRows )
			{
				SelectedRowsCollection selectedRows = selection.Rows;
				for ( int i = selectedRows.Count - 1; i >= 0; i-- )
				{
					UltraGridRow row = selectedRows[i];
					if ( null != row && !row.IsStillValid )
						selectedRows.Remove( row );
				}
			}

			if ( selection.HasCells )
			{
				SelectedCellsCollection selectedCells = selection.Cells;
				for ( int i = selectedCells.Count - 1; i >= 0; i-- )
				{
					UltraGridCell cell = selectedCells[i];
					UltraGridRow row = null != cell ? cell.Row : null;
					if ( null != row && !row.IsStillValid )
						selectedCells.Remove( cell );
				}
			}
		}

		#endregion // UnSelectRowsNotInCollection

		#region GetObjectForRowComparision

		// SSP 1/4/05 BR08526
		// See comments above listObject_ForComparison member declaration for more info.
		// 
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal object GetObjectForRowComparisionFromRow( UltraGridRow row )
		internal static object GetObjectForRowComparisionFromRow( UltraGridRow row )
		{
			// SSP 11/16/06 BR16418
			// Do this for CLR1.x versions as well since one could use CLR1.x assemblies with CLR2 
			// version (VS2005).
			// 
//#if CLR2
			if ( null != row.listObject_ForComparison )
				return row.listObject_ForComparison;
//#endif

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//return this.GetObjectForRowComparision( row.InternalCachedListObject );
			return RowsCollection.GetObjectForRowComparision( row.InternalCachedListObject );
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal object GetObjectForRowComparision( object listObject )
		internal static object GetObjectForRowComparision( object listObject )
		{
			System.Data.DataRowView dvr = listObject as System.Data.DataRowView;
			if ( null != dvr )
				return dvr.Row;
			
			return listObject;
		}

		#endregion // GetObjectForRowComparision

		#region AdjustScrollRegionFirstRow

		// SSP 5/8/03 UWG2249
		//
		internal void AdjustScrollRegionFirstRow( Hashtable hash )
		{
			RowScrollRegionsCollection rsrColl = this.Band.Layout.RowScrollRegions;

			// Make sure the first rows of all the row scroll regions are valid because
			// they could have been deleted.
			//
			for ( int i = 0; i < rsrColl.Count; i++ )
			{
				RowScrollRegion rsr = rsrColl[i];

				UltraGridRow firstRow = rsr.InternalFirstRow;
				UltraGridRow newFirstRow = null;

				if ( null != firstRow && this.Band == firstRow.Band && firstRow.Band.IsDescendantOfBand( this.Band ) )
				{
					while ( null != firstRow &&								
						this.Band != firstRow.Band &&
						null != firstRow.ParentCollection && 
						null != firstRow.ParentCollection.parentRow )
					{
						firstRow = firstRow.ParentCollection.parentRow;
					}

					if ( null != firstRow )
					{
						object listObject = firstRow.InternalCachedListObject;

						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//object comparisionListObject = this.GetObjectForRowComparision( listObject );
						object comparisionListObject = RowsCollection.GetObjectForRowComparision( listObject );

						if ( null != comparisionListObject )
						{
							if ( null != hash )
							{
								ListObjectHolder listObjectHolder = null;
							
								if ( null != listObject && null != comparisionListObject )
									listObjectHolder = (ListObjectHolder)hash[ new ListObjectHolder( listObject, comparisionListObject, -1 ) ];
							
								if ( null != listObjectHolder && 
									listObjectHolder.listIndex >= 0 && 
									listObjectHolder.listIndex < unsortedActualRows.Count )
								{
									newFirstRow = (UltraGridRow)unsortedActualRows[ listObjectHolder.listIndex ];
								}
							}
							else
							{
								foreach ( UltraGridRow row in this.UnSortedActualRows )
								{
									// MD 7/26/07 - 7.3 Performance
									// FxCop - Mark members as static
									//object tmp = this.GetObjectForRowComparision( row.InternalCachedListObject );
									object tmp = RowsCollection.GetObjectForRowComparision( row.InternalCachedListObject );

									if ( tmp == comparisionListObject )
									{
										newFirstRow = row;
										break;
									}
								}
							}
						}

						if ( null == newFirstRow && null != unsortedActualRows && unsortedActualRows.Count > 0 )
						{
							// SSP 4/5/04 - Virtual Binding Related Optimizations
							//
							//int firstRowListIndex = firstRow.InternalListIndex;
							int firstRowListIndex = 0;

							if ( firstRowListIndex >= 0 )
							{
								if ( firstRowListIndex >= unsortedActualRows.Count )
									firstRowListIndex = unsortedActualRows.Count - 1;

								newFirstRow = (UltraGridRow)unsortedActualRows[ firstRowListIndex ];
							}
						}
					}

					if ( null != newFirstRow )
						rsr.InternalFirstRow = newFirstRow;
				}
			}
		}

		#endregion // AdjustScrollRegionFirstRow

		#region FireInitializeRow

		// SSP 9/10/03
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void FireInitializeRow( IList rows )
		internal static void FireInitializeRow( IList rows )
		{
			IEnumerable e = rows is SparseArray ? ((Infragistics.Shared.SparseArray)rows).NonNullItems : rows;

			foreach ( UltraGridRow row in e )
			{
				if ( null != row )
					row.FireInitializeRow( );
			}
		}

		#endregion // FireInitializeRow

		#region SyncRowsHelper_AddRowHelper

		// SSP 10/30/03 UWG2726
		//
		private void SyncRowsHelper_AddRowHelper( UltraGridRow addedRow )
		{
			this.Band.addedRow = addedRow;
			if ( null != addedRow )
			{
				this.Band.addedRow.InternalSetIsAddRow( true );

				// SSP 10/30/03 UWG2726
				// Set the active row to the new row.
				//
				// ------------------------------------------------------------------------
				if ( null == this.parentRow || this.parentRow.AreAllAncestorsCurrentRows )
				{
					UltraGrid grid = this.Layout.Grid as UltraGrid;
					if ( null != grid )
						grid.TempActiveRow = this.Band.addedRow;
				}
				// ------------------------------------------------------------------------
			}
		}

		#endregion // SyncRowsHelper_AddRowHelper

		#region SyncRowsHelper

		// SSP 5/8/03 UWG2249
		// Added a new method of syncing the rows that will sync if the sort changed on the
		// underlying bound list.
		//
		internal bool SyncRowsHelper( IList boundList )
		{
			int i;
			RowsCollection.RowsSparseArray unsortedActualRows = this.UnSortedActualRows;

			// If the rows in binding list and the the rows in our list are in the same order,
			// then we don't need to syncronize.
			//
			if ( unsortedActualRows.Count == boundList.Count )
			{
				bool inSameOrder = true;
			
				for ( i = 0; i < unsortedActualRows.Count; i++ )
				{
					UltraGridRow row = (UltraGridRow)unsortedActualRows[i];
					if ( null == row )
						continue;

					// SSP 1/4/05 BR08526
					// Use the GetObjectForRowComparisionFromRow helper method instead.
					// 
					//object o1 = this.GetObjectForRowComparision( row.InternalCachedListObject );
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//object o1 = this.GetObjectForRowComparisionFromRow( row );
					//
					//object o2 = this.GetObjectForRowComparision( boundList[i] );
					object o1 = RowsCollection.GetObjectForRowComparisionFromRow( row );

					object o2 = RowsCollection.GetObjectForRowComparision( boundList[ i ] );

					if ( null == o1 || null == o2 || o1 != o2 )
					{
						inSameOrder = false;
						break;
					}
				}

				if ( inSameOrder )
				{
					// SSP 5/24/04 UWG2248
					// Also make sure that the rows in the sorted rows collection are in the same
					// order as the un sorted rows collection if we don't have any sorted columns.
					// Added following block of code.
					//
					// ----------------------------------------------------------------------------
					bool initNonGroupByRowsCalled = false;
					if ( ! this.Band.HasSortedColumns )
					{
						SparseArray rowsList = this.SparseArray;
						inSameOrder = null != rowsList && rowsList.Count == unsortedActualRows.Count;
						if ( inSameOrder )
						{
							int count = rowsList.Count;
							for ( i = 0; inSameOrder && i < count; i++ )
								inSameOrder = rowsList[i] == unsortedActualRows[i];
						}
						
						// If not in same order, call InitNonGroupByRows to re-initialize the 
						// sorted rows list.
						//
						if ( ! inSameOrder )
						{
							this.InitNonGroupByRows( );
							initNonGroupByRowsCalled = true;
						}
					}
					// ----------------------------------------------------------------------------

					// SSP 6/19/03 UWG2232
					// Related to SuspendRowSynchronization and ResumeRowSynchronization methods.
					// When the row syncronization is suspended, we also suspend firing InitialiazeRow
					// (we don't respond to binding events). So when the row syncrhonization is resumed
					// fire initialize row on all the rows.
					// Added the if block and enclosed the already existing code in the else block.
					// NOTE: fireInitializeRowsOnAllRow flag is reset in the SyncRows in the finally bock
					// that encloses the code that calls this method. So it will be reset in case you are
					// wondering as to why we are not resetting it here.
					//
					if ( this.fireInitializeRowsOnAllRow )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.FireInitializeRow( unsortedActualRows );
						RowsCollection.FireInitializeRow( unsortedActualRows );
					}

					// SSP 9/8/04
					// Notify the calc manager so it recalculates everything. Even though the rows
					// haven't changed, data may have changed.
					//
					if ( ! initNonGroupByRowsCalled )
						this.NotifyCalcManager_RowsCollectionResynced( );

					return false;
				}
			}

			// SSP 11/3/04 - Merged Cell Feature
			// When the rows collection is synced clear the merged cell cache.
			//
			this.ClearMergedCellCache( );

			// SSP 11/13/03 Add Row Feature
			//
			UltraGridRow templateRowUtilized = null;

			// SSP 4/13/04 - Virtual Binding
			//
			bool loadOnDemand = this.VirtualModeResolved;

			// If we can't sync because the list objects of the binding list are as such that
			// we can't rely on them for syncing purposes, then reuse old rows and fire
			// initialize rows. Also do that if the binding list has 0 items, then clear 
			// our list. Also if we haven't been initialized before (rows list count is 0), 
			// then load the rows at once.
			//
			if ( 0 == boundList.Count || 0 == unsortedActualRows.Count )
			{
				// If there are no rows in the bound list then clear the unsorted rows list.
				//
				if ( 0 == boundList.Count )
				{
					// SSP 9/9/03 UWG2308
					// Clean up the rows. Since rows collection hooks into the binding list we
					// need to make sure that the row's descendant rows collections unhook
					// from the binding list.
					//
					RowsCollection.DisposeRows( unsortedActualRows );
					
					unsortedActualRows.Clear( );
				}
					// If the unsorted rows list is empty, then load the rows from the bound list.
					//
				else
				{
					int previousRowCount = unsortedActualRows.Count;
					unsortedActualRows.Clear( );
					unsortedActualRows.Expand( boundList.Count );

					int tmpListCount = boundList.Count;
					// See if we are adding a row.
					//
					if ( previousRowCount + 1 == tmpListCount 
						&& ( this.inAddingTemplateAddRow || this.Band.inAddNew ) )
					{
						templateRowUtilized = this.inAddingTemplateAddRow 
							? this.SyncRows_GetTemplateAddRow( ) : this.AllocateNewRow( );
						unsortedActualRows[ tmpListCount - 1 ] = templateRowUtilized;
						templateRowUtilized.InternalCachedListObject = boundList[ tmpListCount - 1 ];
						tmpListCount--;
					}

					// Don't create rows if load style is not LoadOnDemand.
					//
					if ( ! loadOnDemand )
					{
						for ( i = 0; i < tmpListCount; i++ )
						{
							UltraGridRow row = this.AllocateNewRow( );
							row.InternalCachedListObject = boundList[i];
							unsortedActualRows[ i ] = row;
						}
					}
				}

				// SSP 11/13/03 Add Row Feature
				//
				if ( null != templateRowUtilized )
					this.CopyDefaultValuesToAddRow( templateRowUtilized );

				// If we are in the middle of Band.AddNew, then find the row that was added
				// and set the Band.addedRow variable to that added row. Notice how we are
				// checking if the addedRows.Count is 1. This is because there should be
				// only one added row. If there are more than 1, then something is wrong.
				// So don't give back the wrong add row.
				//
				// SSP 11/13/03 Add Row Feature
				// Commented out the original code and added the new one below it.
				//
				// ------------------------------------------------------------------------------
				//if ( this.Band.inAddNew && 1 == addedRows.Count )
				//	this.SyncRowsHelper_AddRowHelper( (UltraGridRow)addedRows[ 0 ] );
				if ( null != templateRowUtilized )
					this.SyncRowsHelper_AddRowHelper( templateRowUtilized );
				// ------------------------------------------------------------------------------

				if ( this.Band.HasGroupBySortColumns )
				{
					// SSP 6/10/03
					// We want to fire the initialize row before constructing the group-by hierarchy. 
					// This is because if the rows are grouped by an unbound column, then the unbound 
					// column values need to be initialized before the group-by hierarchy is constructed.
					//
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.FireInitializeRow( unsortedActualRows );
					RowsCollection.FireInitializeRow( unsortedActualRows );

					// If there are group-by columns, then reconstruct the group-by hierarchy based on
					// the unsortedActualRows.
					//
					this.InitGroupByRows( );
				}
				else 
				{
					// SSP 7/30/04 UWG3581
					// Before this fix we were adding the rows to the rows collection, sorting 
					// (InitNonGroupByRows sorts the rows as well) and then firing InitializeRows event. 
					// This caused a problem when sorting was based on an unbound column's cell values 
					// because we would sort and then fire InitializeRow which is where the user would 
					// typically populate unbound columns. Changed it so that now we would add the rows
					// to the rows collection, fire InitializeRow and then sort the rows.
					// Commented out below code added added the new code below it.
					//
					// --------------------------------------------------------------------------------
					
					this.InitNonGroupByRows( unsortedActualRows );
					// --------------------------------------------------------------------------------
				}
			}
			else
			{
				Hashtable hash = null;

				// If we encounter an existing row that doesn't have an associated list object in the
				// binding list, then we will remove it. As we remove it, we will add it to the 
				// removeRowsList so later we can dispose them.
				//
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList removeRowsList = new ArrayList( );
				List<UltraGridRow> removeRowsList = new List<UltraGridRow>();

				// SSP 12/3/04 BR00049
				// If load on demand is turned on then assume the data source has performant IndexOf implemented.
				// In load on demand according to BR00049 bug should not access all the list objects in the list
				// when syncing. There are two ways to solve this. One is that we discard the existing rows. 
				// This means that the rows will lose their expanded states. The second is to use IndexOf to check 
				// if the existing list objects are valid for maintaing the rows. This has performance implications
				// if the IList implementation does not implement the IndexOf operation efficiently. However 
				// losing the state is worse of the two so decided to use the second solution.
				// 
				//if ( loadOnDemand && boundList is Infragistics.Win.UltraWinDataSource.IUltraDataRowsCollection )
                //
                // MBS 1/21/08 - BR29252
                // As mentioned in the bug fix above, there was a tradeoff as to which approach to take when using 
                // LoadOnDemand, so we make the assumption that the underlying DataSource has an efficient IndexOf 
                // operation; unfortunately, there are more cases now where a user would have a non-performant 
                // implementation, such as in the case of generic lists (including BindingList<T>), which use a linear
                // search for the IndexOf implementation.  Therefore, we have decided to expose a property to the user
                // that allows them the decision of whether to use the IndexOf implementation or caching.
                //
                //if ( loadOnDemand )
                if (loadOnDemand && false == this.Band.Layout.UseOptimizedDataResetMode)
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//ArrayList validRows = new ArrayList( );
					List<ListObjectHolder> validRows = new List<ListObjectHolder>();

					foreach ( UltraGridRow row in unsortedActualRows.NonNullItems )
					{
						object listObject = row.InternalCachedListObject;

						// SSP 1/4/05 BR08526
						// Use the GetObjectForRowComparisionFromRow helper method instead.
						// 
						//object comparisionListObject = this.GetObjectForRowComparision( listObject );
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//object comparisionListObject = this.GetObjectForRowComparisionFromRow( row );
						object comparisionListObject = RowsCollection.GetObjectForRowComparisionFromRow( row );

						int newIndex = null != comparisionListObject ? boundList.IndexOf( comparisionListObject ) : -1;

						if ( newIndex >= 0 )
							validRows.Add( new ListObjectHolder( row, listObject, comparisionListObject, newIndex ) );
						else
							removeRowsList.Add( row );
					}

					unsortedActualRows.Clear( );
					unsortedActualRows.Expand( boundList.Count );
					foreach ( ListObjectHolder item in validRows )
					{
						Debug.Assert( item.listIndex < unsortedActualRows.Count );
						if ( item.listIndex < unsortedActualRows.Count )
						{
							// Slot at item.listIndex should be empty.
							//
							UltraGridRow oldRow = (UltraGridRow)unsortedActualRows[ item.listIndex ];
							Debug.Assert( null == oldRow );
							if ( null != oldRow )
								removeRowsList.Add( oldRow );

							unsortedActualRows[ item.listIndex ] = item.row;
						}
					}
				}
				else
				{
					// Construct a hash table so we can quickly lookup if list objects associated with the
					// old rows also exist in the new binding list that we are trying to syncronize this
					// rows collection with. We also need to store the index the list object is at in the
					// binding list. So for that we will use ListObjectHolder helper class.
					// 
					hash = new Hashtable( boundList.Count, 0.90f );

					for ( i = 0; i < boundList.Count; i++ )
					{
						object listObject = boundList[i];

						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//object comparisionListObject = this.GetObjectForRowComparision( listObject );
						object comparisionListObject = RowsCollection.GetObjectForRowComparision( listObject );

						if ( null != comparisionListObject )
						{					
							ListObjectHolder listObjectHolder = new ListObjectHolder( listObject, comparisionListObject, i );
							hash[ listObjectHolder ] = listObjectHolder;
						}
					}

					UltraGridRow[] newRows = new UltraGridRow[ boundList.Count ];			
					
					foreach ( UltraGridRow row in unsortedActualRows.NonNullItems )
					{
						ListObjectHolder listObjectHolder = null;
						if ( null != row && null != row.InternalCachedListObject )
						{
							object listObject = row.InternalCachedListObject;

							// SSP 1/4/05 BR08526
							// Use the GetObjectForRowComparisionFromRow helper method instead.
							// 
							//object comparisionListObject = this.GetObjectForRowComparision( listObject );
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//object comparisionListObject = this.GetObjectForRowComparisionFromRow( row );
							object comparisionListObject = RowsCollection.GetObjectForRowComparisionFromRow( row );

							if ( null != comparisionListObject )
								listObjectHolder = (ListObjectHolder)hash[ new ListObjectHolder( listObject, comparisionListObject, -1 ) ];
						}

						// If this row's listObject doesn't have any associated listObject in the new binding list
						// that we are trying to syncronize this rows collection with, then remove that row. This
						// is an indication that the row is removed from the binding list.
						//
						if ( null == listObjectHolder )
						{
							if ( null != row )
								removeRowsList.Add( row );
							continue;
						}

						// If the row's listObject is still in the new binding list, then reuse the row and
						// put the row in it's new index. newListIndex is the index of the list object in
						// the bound list.
						//
						int newListIndex = listObjectHolder.listIndex;

						// There shouldn't be any row at newRows[ newListIndex ] because in the binding list,
						// there should be one and oly one listObject at specified index. This shouldn't happen,
						// but if it does then remove the row that was there.
						//
						if ( null != newRows[ newListIndex ] && newRows[ newListIndex ] != row )
						{
							Debug.Assert( false, "Duplicate unsortedActualRows objects encountered !" );
							removeRowsList.Add( newRows[ newListIndex ] );
						}

						newRows[ newListIndex ] = row;
						row.InternalCachedListObject = listObjectHolder.listObject;
					}

					// Clear the unsortedActualRows list. Later we are adding the rows in the newRows array
					// to this list.
					//
					unsortedActualRows.Clear( );
					unsortedActualRows.AddRange( newRows );
				}

				// Keep track of any new rows we add so we can fire the InitializeRow for them after
				// we are done syncrhonizing.
				//
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList addedRows = new ArrayList( );
				List<UltraGridRow> addedRows = new List<UltraGridRow>();
				
				if ( ! loadOnDemand || ( this.inAddingTemplateAddRow || this.Band.inAddNew ) )
				{
					// Now the newRows array should have all the reused rows and null slots where we
					// we need to add new rows.
					//
					for ( i = 0; i < unsortedActualRows.Count; i++ )
					{
						// If we encounter a null slot in the array, then we need to add a new row.
						// Null slot means we couln't find an old row associated with the list object
						// at the specified location (i'th index) in the bound list. Thus we need 
						// to add a row for that new list object.
						//
						UltraGridRow row = (UltraGridRow)unsortedActualRows[i];
						if ( null == row )
						{							
							if ( ( this.inAddingTemplateAddRow || this.Band.inAddNew ) && null == templateRowUtilized )
							{
								if ( this.inAddingTemplateAddRow )
									templateRowUtilized = this.SyncRows_GetTemplateAddRow( );
								else
									templateRowUtilized = this.AllocateNewRow( );

								row = templateRowUtilized;
							}
							else
							{
								if ( loadOnDemand )
									break;

								row = this.AllocateNewRow( );
							}

							unsortedActualRows[i] = row;
							row.InternalCachedListObject = boundList[i];
							addedRows.Add( row );
						}
					}
				}

				// SSP 11/13/03 Add Row Feature
				//
				if ( null != templateRowUtilized )
					this.CopyDefaultValuesToAddRow( templateRowUtilized );

				// If we are in the middle of Band.AddNew, then find the row that was added
				// and set the Band.addedRow variable to that added row. Notice how we are
				// checking if the addedRows.Count is 1. This is because there should be
				// only one added row. If there are more than 1, then something is wrong.
				// So don't give back the wrong add row.
				//
				// SSP 11/13/03 Add Row Feature
				// Commented out the original code and added the new one below it.
				//
				// ------------------------------------------------------------------------------
				//if ( this.Band.inAddNew && 1 == addedRows.Count )
				//	this.SyncRowsHelper_AddRowHelper( (UltraGridRow)addedRows[ 0 ] );
				if ( null != templateRowUtilized )
					this.SyncRowsHelper_AddRowHelper( templateRowUtilized );
				// ------------------------------------------------------------------------------

				// If we have group-by rows, then call SyncRows_Remove and SyncRows_Add to add and remove
				// rows from the appropriate groups.
				//
				if ( this.Band.HasGroupBySortColumns )
				{
					// SSP 6/19/03 UWG2232
					// Related to SuspendRowSynchronization and ResumeRowSynchronization methods.
					// When the row syncronization is suspended, we also suspend firing InitialiazeRow
					// (we don't respond to binding events). So when the row syncrhonization is resumed
					// fire initialize row on all the rows.
					// Added the if block and enclosed the already existing code in the else block.
					// NOTE: fireInitializeRowsOnAllRow flag is reset in the SyncRows in the finally bock
					// that encloses the code that calls this method. So it will be reset in case you are
					// wondering as to why we are not resetting it here.
					//
					if ( this.fireInitializeRowsOnAllRow )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.FireInitializeRow( unsortedActualRows );
						RowsCollection.FireInitializeRow( unsortedActualRows );
					}
					else
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.FireInitializeRow( addedRows );
						RowsCollection.FireInitializeRow( addedRows );
					}

					// SSP 6/12/06 BR13568
					// Added the if block and enclosed the existing code into the else block. If we are in
					// load-on-demand mode then recreate the group-by structure since addedRows array
					// would have been empty in this mode. Above we leave the empty slots for new rows.
					// However for grouping rows, we have to create all the rows in order to group.
					// 
					if ( loadOnDemand )
					{
						this.InitGroupByRows( );
					}
					else
					{
						for ( i = 0; i < removeRowsList.Count; i++ )
						{
							// MD 8/10/07 - 7.3 Performance
							// Use generics
							//this.SyncRows_Remove( (UltraGridRow)removeRowsList[i], true );
							this.SyncRows_Remove( removeRowsList[ i ], true );
						}

						for ( i = 0; i < addedRows.Count; i++ )
						{
							// MD 8/10/07 - 7.3 Performance
							// Use generics
							//this.SyncRows_Add( (UltraGridRow)addedRows[i] );
							this.SyncRows_Add( addedRows[ i ] );
						}
					}
				}
					// If there are no group-by columns, then call InitNonGroupByRows to syncronize
					// the rows collection with the unsortedActualRows.
					//
				else 
				{
					// SSP 7/30/04 UWG3581
					// Before this fix we were adding the rows to the rows collection, sorting 
					// (InitNonGroupByRows sorts the rows as well) and then firing InitializeRows event. 
					// This caused a problem when sorting was based on an unbound column's cell values 
					// because we would sort and then fire InitializeRow which is where the user would 
					// typically populate unbound columns. Changed it so that now we would add the rows
					// to the rows collection, fire InitializeRow and then sort the rows.
					// Commented out below code added added the new code below it.
					//
					// --------------------------------------------------------------------------------
					
					this.InitNonGroupByRows( this.fireInitializeRowsOnAllRow ? (IList)unsortedActualRows : (IList)addedRows );
					// --------------------------------------------------------------------------------
				}

				// Make sure the first rows of all the row scroll regions are valid because
				// they could have been deleted.
				//
				this.AdjustScrollRegionFirstRow( hash );

				// SSP 9/9/03 UWG2308
				// Clean up the row. Since rows collection hooks into the binding list we
				// need to make sure that the row's descendant rows collections unhook
				// from the binding list.
				//
				// ------------------------------------------------------------------------
				if ( null != removeRowsList )
					RowsCollection.DisposeRows( removeRowsList );
				// ------------------------------------------------------------------------
			}

			return true;
		}

		#endregion // SyncRowsHelper
		
		#region SyncRows

		internal void SyncRows( )
		{
			// SSP 6/19/03 UWG2232
			// If we are already in syncRows, then return. This happens in rare situations when we 
			// set the UltraGridBase.TempActiveRow property to a row, we are setting the Enabled property
			// of a timer. Enabled property of the timer goes and fires Tick on any pending timers
			// apparently which is not a nice behaviour. If the user in the timer's tick does something
			// that will lead to rows collection getting dirty, then we will get here even though we
			// are already in here.
			// 
			// ----------------------------------------------------------------------------------------
			if ( this.inSyncRows )
				return;
			// ----------------------------------------------------------------------------------------

			// SSP 8/7/03 UWG1963
			// Added dontSyncRows flag to prevent the syncing of the rows in some situations.
			//
			if ( this.dontSyncRows )
				return;

			// SSP 5/9/06 BR12207
			// Don't reset the dirtyRows flag while initializing because while initializing
			// the data source would not have been loaded anyways. Also when not initializing,
			// load the data source if not already loaded.
			// 
			// --------------------------------------------------------------------------------
			UltraGridLayout layout = this.Layout;
			UltraGridBase grid = null != layout ? layout.Grid : null;
			if ( null != grid && grid.Initializing )
				return;

			if ( null != layout )
				layout.EnsureDataSourceAttached( );
			// --------------------------------------------------------------------------------

			// SSP 11/11/03 Add Row Feature
			// If the parent row is a template add-row, then it can't have any children.
			//
			// --------------------------------------------------------------------
			// SSP 4/14/05 - NAS 5.2 Fixed Rows/Filter Row
			// Use the new IsDataRow which checks for IsTemplateAddRow as well as other special
			// rows like the filter row.
			//
			//if ( null != this.parentRow && this.parentRow.IsTemplateAddRow )
			if ( null != this.parentRow && ! this.parentRow.IsDataRow )
				return;
			// --------------------------------------------------------------------

			if ( UltraGridBand.binding_debug )
				Debug.WriteLine( "Band[" + this.Band.Index + "]: S Y N C I N G  T H E  R O W S   !!" );
			
			// SSP 12/28/01
			// Rebind
			// SSP 10/9/07 BR26018
			// Only call BindToList if this row collection is the root row collection. Otherwise it's
			// not necessary as accessing Data_List property will bind to the list. The problem with
			// calling BindToList is that it ends up getting the data list twice - once here and then
			// a second time further below where we force DirtyDataList and reget the list again.
			// 
			//this.BindToList( );
			// SSP 6/11/08 BR33700
			// Moved this below after we dirty the data list. Calling ResolvedDataList below will not 
			// hook into the list if the ancestors are active rows because it takes a different path
			// to getting the list and doesn't call Data_List. Therefore we need to ensure we bind to
			// the list. To solve the original performance issue of BR26018, we are moving call to
			// BindToList to after we call DirtyDataList.
			// 
			

			lock ( this.UnSortedActualRows.SyncRoot ) 
			{
				// SSP 6/4/02
				//
				//this.verifyVersion++;
				this.BumpVerifyVersion( );

				// set the rowsDirty flag to false
				this.rowsDirty = false;

				this.inSyncRows = true;

				try
				{
					// SSP 12/17/03 UWG2799
					// Make sure the grid element's rect is initialized to the grid's size. If its empty
					// then the ScrollRowIntoView method off the row scroll region will make the passed 
					// in row the first visible row (since the extent of the scroll region will be 0),
					// and this may end up showing a scrollbar even when all rows can fit the region and
					// there is no need for one. So we need to initialize the elemnets rect here.
					// 
					// --------------------------------------------------------------------------------
					if ( null == this.parentRow )
					{
						UltraGridUIElement gridElem = null != layout 
							? layout.UIElement as UltraGridUIElement : null;

						// If the grid element's rect is not initialized, then initialize it.
						//
						if ( null != gridElem && gridElem.Rect.IsEmpty )
							gridElem.InternalInitializeRect( true );
					}
					// --------------------------------------------------------------------------------

					// SSP 11/16/04
					// Also return if layout is null.
					//
					//if ( null == this.band 
					if ( null == this.band || null == layout ||
						( null == this.parentRow && layout.Rows != this ) )
						return;

					if ( !this.IsTopLevel )
					{
						//this.ParentRow.ParentCollection.SyncRows( );
						return;
					}

					// SSP 6/8/05 BR03609
					// Use the new ShouldDisplayDataFromDataSource property instead.
					// 
					// ----------------------------------------------------------------------
					
					if ( ! this.band.Layout.ShouldDisplayDataFromDataSource )
						// ----------------------------------------------------------------------
					{
						// SSP 3/10/04 - Designer Usability Improvements Related
						// Added ExtractDataStructure method to be able to extract the data structure from
						// a data source. If we are in a state where there is no data source bound to
						// us but we have the structure then simply add two rows and return.
						//
						// ------------------------------------------------------------------------------
						if ( null != grid && ( this.Layout.SortedBands.Count > 1 || this.Band.Columns.Count > 0 ) )
						{
							// SSP 7/30/04 UWG3488
							// Only add dummy rows in design mode or when showing previews in the designer
							// dialogs. Note: Designer dialogs set the IDesignInfo.DesignMode property on
							// preview grids to true and thus the grid.DesignMode will evaluate to true
							// here causing use to add the two dummy rows. This was done to fix the 
							// problem where we were showing two dummy rows at runtime if the grid had 
							// unbound columns and there was no datasource set on it. 
							// Added following if block and enclosed the existing code into the associated
							// else block.
							//
							// SSP 6/8/05 BR03609
							// Related to above.
							// 
							//if ( ! designMode )
							if ( ! grid.DesignMode )
							{
								this.SparseArray.Clear( );
								this.UnSortedActualRows.Clear( );
							}
							else
							{
								// If the collection already has 2 rows then reuse them. We want to keep
								// the same rows to prevent an assert so this is not for optimization.
								//
								// SSP 1/28/05
								//
								// ----------------------------------------------------------------------
								
								if ( 2 != this.GetAllNonGroupByRows( ).Length )
								{
									this.SparseArray.Clear( );
									this.UnSortedActualRows.Clear( );
									this.UnSortedActualRows.Add( this.AllocateNewRow( ) );
									this.UnSortedActualRows.Add( this.AllocateNewRow( ) );
									this.Band.BumpGroupByHierarchyVersion( );
								}
								// ----------------------------------------------------------------------
							}
						}
						// ------------------------------------------------------------------------------

						return;
					}
			
					IList list = null;

					try
					{
						// SSP 6/8/05 - Optimization
						// Took this out because design mode is taken care of above.
						// 
						
							// SSP 11/19/03 Add Row Feature
							// This else-if block is not necessary. Necessary code already exists
							// in the ResolvedDataList property that we are calling in the else
							// block.
							//
							
						// SSP 6/8/05 - Optimization
						// Related to change above.
						// 
						//else
						//{
							// SSP 5/8/03 UWG2249
							// Instead of clearing the list object, decrement the verified version number. Either of
							// these will cause the row to reget the list object when the ListObject property is
							// accessed.
							// 
							//this.parentRow.InternalClearCachedListObject( );
							if ( null != this.parentRow )
								this.parentRow.InternalDecrementVerifiedVersion( );

							// SSP 2/5/04 UWG2947
							// Also reget the data list instead of using the cached list.
							//
							this.DirtyDataList( );

							// SSP 6/11/08 BR33700
							// Moved this here from above.
							// 
                            // MBS 9/5/08 - TFS6970
                            // We should check to see if the BindingList is null at this point before calling BindToList.
                            // This is necessary because the BindToList method will set the HookIntoList flag for the 
                            // entire band to false if it comes across a child that does not have a binding list, which
                            // in turn will prevent us from getting any notifications from various rows collections
                            // on the same band that *do* have a valid BindingList.
                            //
                            // NOTE: This does not break the fix that was made for BR26018, part of which was that
                            // the PropertyDescriptor.GetValue() was called multiple times in this method.  This doesn't
                            // happen now because of the fix to BR33700, which places this code *after* we call
                            // DirtyDataList(), so checking this.BindingList and then subsequently calling BindToList
                            // will use a cached version of Data_List.
                            //
                            // this.BindToList();
                            if (this.BindingList != null)
                                this.BindToList();							

							// SSP 11/19/03 Add Row Feature
							// Moved the following code into ResolvedDataList property so use that.
							//
							// --------------------------------------------------------------------------
							
							list = this.ResolvedDataList;
							// --------------------------------------------------------------------------
						//}
					}
					catch ( Exception exception1 )
					{
						Debug.Assert( false, "Exception caught while trying to get the list !\n" + exception1.Message );
					}
           
					// SSP 12/15/04
					// List could be null if the parent row is disposed in the process of getting the list.
					//
					//Debug.Assert( null != list, "Unable to get a list for rows from the CurrencyManager" );
					//Debug.Assert( null != list || null != this.parentRow && this.parentRow.Disposed, "Unable to get a list for rows from the CurrencyManager" );

					if ( null == list )
					{
						this.SparseArray.Clear( );

						// SSP 8/24/01 
						// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
						// Commented out the following line of code. ScrollCountManagerSparseArray itself 
						// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
						// method.
						//
						//this.DirtyScrollableRowCountHelper( );
						return;
					}

					SparseArray rowsList = this.UnSortedActualRows;

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Remove unused locals
					//int numberOfRows = list.Count;

					// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
					// Commented out the following line of code. ScrollCountManagerSparseArray itself 
					// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
					// method.
					//
					//bool scrollCountChanged = rowsList.Count != numberOfRows;

					// SSP 10/1/01
					// If Syncing rows first time, then decrease the sort
					// version so rows get resorted
					//
					if ( 0 == rowsList.Count )
						this.sortVersion--;

					UltraGridRow activeRow =  layout.ActiveRow;

					// SSP 11/16/04 UWG2234 
					// We make use of the following info (further down in this method) only if this rows 
					// collection is from the root band. Therefore only bother getting it for the root 
					// band.
					// 
					// ------------------------------------------------------------------------------------
					
					UltraGridRow activeRowParent = null;
					int activeRowParentIndex = -1;
					RowsCollection activeRowParentRowsCollection = null;					
					if ( null == this.Band.ParentBand )
					{
						activeRowParent = null != activeRow ? activeRow.ParentRow : null;
						activeRowParentIndex = null != activeRowParent ? activeRowParent.Index : -1;
						activeRowParentRowsCollection = 
							null != activeRowParent ? activeRowParent.ParentCollection : null;
					}
					// ------------------------------------------------------------------------------------
		
					// SSP 4/7/04
					//
					

					// SSP 4/28/03 UWG1981
					// Commented out the original code and added new code below it.
					//
					// ----------------------------------------------------------------------------------
					

					// SSP 5/8/03 UWG2249
					// Added a new method of syncing the rows that will sync if the sort changed on the
					// underlying bound list.
					//
					bool rowsChanged = this.SyncRowsHelper( list );

					
			
					// SSP 10/01/01
					// Only set the active row for UltraGrid. UltraCombo and UltraDropDown
					// should not be synced with the binding manager's position 
					//
					if ( grid is UltraGrid )
					{
						//RobA UWG400 10/3/01 not clear as to why we need to clear the active
						//row.  This was also causing UWG434 by setting the active row to null.
									

						UltraGrid gridCtrl = grid as UltraGrid;

						// SSP 9/28/01 UWG373
						// Set the active row 
						//
						//RobA UWG427 10/2/01 we don't want to do this for a print layout
						//
						// SSP 5/7/03 - Export Functionality
						//
						//if ( null == this.Band.ParentBand && !this.Band.Layout.IsPrintLayout )
						// SSP 5/18/05 BR03434
						// Added SyncWithCurrencyManager to prevent the UltraGrid from synchronizing with
						// the currency manager. When there are a lot of deep nested bands activating a 
						// different ancestor row caues a lot of slowdown.
						// 
						//if ( null == this.Band.ParentBand && ! layout.IsPrintLayout && ! layout.IsExportLayout )
						if ( gridCtrl.SyncWithCurrencyManager && null == this.Band.ParentBand && ! layout.IsPrintLayout && ! layout.IsExportLayout 
							// SSP 10/9/06 BR15222
							// If we already have a valid active row then don't change it.
							// 
							&& ( null == gridCtrl.ActiveRowInternal || ! gridCtrl.ActiveRowInternal.IsStillValid ) )
						{
							// set the active row to the current position on band
							// zero rows
							//

							// SSP 6/8/05 BR03609
							// Check for BindingManager being null.
							// 
							BindingManagerBase bm = this.Band.BindingManager;
							int pos = null != bm ? bm.Position : -1;

							if ( pos < 0 )
								pos = 0;

							// SSP 1/28/02 UWG1001
							// Set the TempActiveRow to null. Below is where we are going
							// to set the TempActiveRow to appropriate value. If those conditions
							// are not true, then we do want to make sure the TempActiveRow does
							// not have an invalid row in it. So set it to null.
							//
							grid.TempActiveRow = null;

							// SSP 11/27/01 UWG717
							// When a child row of a group by row
							// is deleted, then make the parent group by row of
							// that deleted child row the active row.
							//
							//------------------------------------------------------------------------------
							bool activeRowAssigned = false;
							if ( null != activeRowParent && null != activeRowParentRowsCollection &&
								activeRowParent is UltraGridGroupByRow )                                 
							{
								if ( activeRowParentIndex >= 0 && 
									activeRowParentIndex < activeRowParentRowsCollection.Count )
								{
									// SSP 8/27/04 UWG3142
									// Also if the new active row happens to belong to the same group that
									// we are activating and that group is already expanded then bail out.
									// Commented out the original code and added the new code below it.
									//
									// ----------------------------------------------------------------------
									//grid.TempActiveRow = activeRowParentRowsCollection[ activeRowParentIndex ];									
									//activeRowAssigned = true;
									UltraGridGroupByRow groupByRow = activeRowParentRowsCollection[ activeRowParentIndex ] as UltraGridGroupByRow;
									UltraGridRow newActiveRow = pos >= 0 && pos < rowsList.Count 
										? (UltraGridRow)rowsList[ pos ] : null;
									if ( null == newActiveRow || null == newActiveRow.ParentCollection 
										|| groupByRow != newActiveRow.ParentCollection.ParentRow 
										|| ! groupByRow.Expanded )
									{                                       									
										// SSP 11/17/04 UWG2234 
										// Changed the behavior so that when a column is sorted we maintain the scroll position
										// instead of keeping the first row the first row.
										//
										//grid.TempActiveRow = groupByRow;
										grid.SetTempActiveRow( groupByRow, false );

										activeRowAssigned = true;
									}
									// ----------------------------------------------------------------------
								}
							}
							//------------------------------------------------------------------------------



							// SSP 11/27/01 UWG717
							// If the active row is assigned above, then do not
							// reassign it.
							//if ( pos < rowsList.Count )
							if ( !activeRowAssigned && pos < rowsList.Count )
							{
								// SSP 10/8/01
								// Now we are doing delayed active row change. When deleting
								// multiple rows for example, we don't want to be
								// setting the active row multiple times causing the grid
								// to be repainted everytime
								//
								//this.Band.Layout.Grid.ActiveRow = (Row)rowsList[ pos ];
								// SSP 11/17/04 UWG2234 
								// Changed the behavior so that when a column is sorted we maintain the scroll position
								// instead of keeping the first row the first row.
								//
								//grid.TempActiveRow = (UltraGridRow)rowsList[ pos ];
								grid.SetTempActiveRow( (UltraGridRow)rowsList[ pos ], false );
							}							
						}

						// SSP 3/11/03 UWG2052
						// Exit the edit mode if the row associated with the cell in edit mode is
						// deleted. Accessing the ActiveCell proeprty off the grid should accomplish that.
						//
						if ( null != gridCtrl )
						{
							UltraGridCell cell = gridCtrl.ActiveCell;
						}
					}


					// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
					// Commented out the following code. ScrollCountManagerSparseArray itself 
					// should be doing what needs to be done in ScrollCountManagerSparseArray.OnScrollCountChanged 
					// method.
					//
					

					// SSP 5/5/04
					// Only perform the following actions if rows changed in any way.
					// Enclosed the existing code in the if block.
					//
					if ( rowsChanged )
					{
						// SSP 8/18/04 - Optimizations
						// Also only dirty the visible rows if this rows collection is visible on
						// the screen (or likely visible). Also make use of DirtyGridElement as a
						// matter of convention.
						// 
						//if ( null != this.band && null != this.band.Layout )
						//	this.Layout.RowScrollRegions.DirtyAllVisibleRows( );
						if ( null != layout && this.IsPotentiallyVisibleOnScreen )
							layout.DirtyGridElement( true );

						// SSP 9/19/02 UWG1670
						// Unselect rows that are deleted.
						//
						this.UnSelectRowsNotInCollection( );
					}
				}
					// SSP 11/16/04
					// Commented out the catch block because finally block below is all we need
					// considering the fact that the catch block rethrows the exception.
					//
					
					// SSP 1/23/02
					// Added this finally block to ensure we reset the anit-recursion flag
					// in case we return from within the try block.
					//
				finally
				{
					this.inSyncRows = false;

					// SSP 6/19/03 UWG2232
					// Related to SuspendRowSynchronization and ResumeRowSynchronization methods.
					// When the row syncronization is suspended, we also suspend firing InitialiazeRow
					// (we don't respond to binding events). So when the row syncrhonization is resumed
					// fire initialize row on all the rows.
					// Reset the flag.
					//
					this.fireInitializeRowsOnAllRow = false;
				}
			}

			// SSP 5/16/02
			// Fire the ListChanged event on the ultra drop down if the grid is ultra drop down.
			// ListChanged was added to IValueList interface and subsequently to the UltraDropDownBase
			// and that is supposed to be fired whenever the rows have changed notifying the owners
			// of the drop down (owners of the IValueList actually) that number of items have
			// changed so they can take appropriate actions.
			//
			if ( null != this.Layout && this.Layout.Grid is UltraDropDownBase )
			{
				UltraDropDownBase ultraDropDown = (UltraDropDownBase)this.Layout.Grid;

				ultraDropDown.FireListChanged( );

				// SSP 1/21/05 BR01891 
				// Added SyncWithCurrencyManager property on UltraCombo. This will cause the 
				// UltraCombo to synchronize the selected row with the currency manager's Position.
				//
				if ( ultraDropDown is UltraCombo )
					((UltraCombo)ultraDropDown).SyncSelectedRowWithCurrencyManager( true );
			}
		}

		#endregion // SyncRows

		#region GetCardAreaElement

		private CardAreaUIElement GetCardAreaElement( RowScrollRegion rsr, 
			ColScrollRegion csr,
			bool scrollIntoView,
			bool exitEditMode )
		{
			return this.GetCardAreaElement( rsr, csr, scrollIntoView, null, exitEditMode );
		}

		// SSP 7/2/02
		// Comrpessed card view.
		// Added an overload of GetCardAreaElement that takes in a row that we
		// are trying to scroll into view. This way we don't scroll the whole
		// card row into view when the row we are trying to scroll is already
		// in view. With compressed card view this is often the case if 
		// the cards are compressed.
		// Added an overload of GetCardAreaElement
		//
		private CardAreaUIElement GetCardAreaElement( RowScrollRegion rsr, 
			ColScrollRegion csr,
			bool scrollIntoView,
			UltraGridRow row,
			bool exitEditMode )
		{
			return this.GetCardAreaElement( rsr, csr, scrollIntoView, row, exitEditMode, true );
		}

		// SSP 4/21/05 BR03382
		// Added an overload that only takes in rsr, csr and verifyElements.
		//
		private CardAreaUIElement GetCardAreaElement( RowScrollRegion rsr, ColScrollRegion csr, bool verifyElements )
		{
			return this.GetCardAreaElement( rsr, csr, false, null, false, verifyElements );
		}

		// SSP 4/21/05 BR03382
		// Added an overload that takes in verifyElements.
		//
		private CardAreaUIElement GetCardAreaElement( RowScrollRegion rsr, 
			ColScrollRegion csr,
			bool scrollIntoView,
			UltraGridRow row,
			bool exitEditMode,
			bool verifyElements )
		{

			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( grid == null )
			{
				Debug.Fail( "Grid element not found in ScrollCardIntoView method" );
				return null;
			}

			if ( rsr == null )
				rsr = grid.ActiveRowScrollRegion;

			if ( csr == null )
				csr = grid.ActiveColScrollRegion;

			UIElement mainElement = this.Layout.UIElement;

			if ( mainElement == null )
			{
				Debug.Fail( "Main element not found in ScrollCardIntoView method" );
				return null;
			}

			// make sure that all elements are positioned properly
			//
			// SSP 4/21/05 BR03382
			// Added an overload that takes in verifyElements. Only verify if that parameter is true.
			//
			if ( verifyElements )
				mainElement.VerifyChildElements( true );

			RowColRegionIntersectionUIElement rowColRegionIntersection = mainElement.GetDescendant( typeof( RowColRegionIntersectionUIElement ), new object[] { rsr, csr } ) as RowColRegionIntersectionUIElement;

			if ( rowColRegionIntersection == null )
			{
				Debug.Fail( "RowCol intersection element not found in ScrollCardIntoView method" );
				return null;
			}

			if ( scrollIntoView )
			{
				//	SSP 7/2/02
				// Compressed card view.
				// Added an overload that takes in a row.
				// If a row is passed in, then we only want to scroll that row and not
				// the whole card area.
				//
				if ( null != row )
					rsr.EnsureRowIsFullyInView( row, exitEditMode, false );
				else
					rsr.EnsureRowIsFullyInView( this.FirstVisibleCard, exitEditMode );

				// force a verification to reposition the elements
				//
				rowColRegionIntersection.VerifyChildElements( true );
			}

			// get the card area element associated with this collection
			//
			CardAreaUIElement cardAreaElement = rowColRegionIntersection.GetDescendant( typeof( CardAreaUIElement ), new object[] { this } ) as CardAreaUIElement;

			if ( cardAreaElement == null && scrollIntoView )
			{
				// If the card area is not in the region we need to scroll
				// the card island into view by making it the first row
				// in the region and by dirtying the entire data area
				//
				//rsr.SetDestroyVisibleRows();
				
				//rsr.FirstRow = this.FirstVisibleCard;
				
				rowColRegionIntersection.GetAncestor( typeof( DataAreaUIElement) ).DirtyChildElements();

				// force a verification to reposition the elements
				//
				rowColRegionIntersection.VerifyChildElements( true );

				cardAreaElement = rowColRegionIntersection.GetDescendant( typeof( CardAreaUIElement ), new object[] { this } ) as CardAreaUIElement;
			}

			return cardAreaElement;
		}

		#endregion

		#region FindFirstNonCompressedCard
		
		internal UltraGridRow FindFirstNonCompressedCard( )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				if ( !this[i].IsCardCompressed )
					return this[i];
			}

			return null;
		}

		#endregion // FindFirstNonCompressedCard

		#region CalculateCardAreaScrollRange
		
		internal int CalculateCardAreaScrollRange( int heightAvailableForCards, int firstVisibleCardIndex, out int scrollBarValue )
		{
			scrollBarValue = 0;

			int cardSpacingVertical = this.Band.CardSpacingVertical;
			int tmpHeightForCards = heightAvailableForCards;
			int columns = 0;
			
			// SSP 11/25/03 - Add Row Feature
			// Skip hidden rows and also take into account the add-new row.
			// VisibleRowCount and GetRowAtVisibleIndex are cached.
			//
			//for ( int i = 0; i < this.Count; i++ )
			//{
			//	 UltraGridRow row = this[i];
			for ( int i = 0; i < this.VisibleRowCount; i++ )
			{
				UltraGridRow row = this.GetRowAtVisibleIndex( i );
				Debug.Assert( null != row );
				if ( null == row )
					continue;

				// Skip visible rows.
				//
				// SSP 8/1/03 UWG1654 - Filter Action
				// Use HiddenInternal instead of the public Hidden behavior of which we changed in regards to filtering.
				//
				//if ( row.Hidden )
				if ( row.HiddenInternal )
					continue;

				int cardHeight = row.CardHeight;

				Debug.Assert( cardHeight > 0, "Card height of a visible card is 0 !" );

				if ( tmpHeightForCards < cardHeight )
				{
					tmpHeightForCards = heightAvailableForCards - cardHeight - cardSpacingVertical;
					columns++;
				}
				else
				{
					tmpHeightForCards -= cardHeight + cardSpacingVertical;

					// Since we have at least one visible row at this point, make sure
					// that we start out with at least one column.
					//
					if ( 0 == columns )
						columns = 1;
				}

				if ( 0 == scrollBarValue && firstVisibleCardIndex <= i )
					scrollBarValue = columns - 1;
			}

			if ( 0 == scrollBarValue )
				scrollBarValue = columns;

			return columns - 1;
		}

		#endregion // CalculateCardAreaScrollRange

		#region ForceSingleColumnCompressedAndVariableCardView
		
		// SSP 10/20/05 BR06341
		// We should do this for variable height cards as well. Changed the name of the method.
		// 
		//internal bool ForceSingleColumnCompressedCardView( out int height )
		private bool ForceSingleColumnCompressedAndVariableCardView( out int height )
		{	
			height = 0;

			int minHeight = this.Band.CalculateRequiredCardAreaHeight( 1 );

			// SSP 2/28/03 - Row Layout Functionality
			// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
			// to StandardLabels card style so use the StyleResolved instead of the Style property.
			//
			// SSP 10/20/05 BR06341
			// We should do this for variable height cards as well.
			// 
			//if ( CardStyle.Compressed == this.Band.CardSettings.StyleResolved )
			CardStyle cardStyle = this.Band.CardSettings.StyleResolved;
			if ( CardStyle.Compressed == cardStyle || CardStyle.VariableHeight == cardStyle )
			{
				// Only do so if all the cards are compressed.
				//
				// SSP 10/20/05 BR06341
				// We should do this for variable height cards as well.
				// 
				//if ( null == this.FindFirstNonCompressedCard( ) )
				if ( CardStyle.VariableHeight == cardStyle || null == this.FindFirstNonCompressedCard( ) )
				{
					// Calculate this for convenience.
					int cardSpacing = this.Band.CardSpacingVertical;

					int totalHeight = 0;

					// Start out with margins and borders.
					//
					int cardAreaBorderThickness	= this.Layout.GetBorderThickness( this.Band.BorderStyleCardAreaResolved );
					// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
					// Only add scrollbar height if scrollbar's going to be visible.
					//
					//totalHeight += SystemInformation.HorizontalScrollBarHeight +
					totalHeight += this.CardAreaHorizontalScrollbarHeightResolved +
						2 * ( this.Band.CardAreaMarginsResolved + cardAreaBorderThickness );

					// SSP 10/20/05 BR06341
					// 
					int miscHeight = totalHeight;
					int maxCardHeight = 0;

					// SSP 11/25/03 - Add Row Feature
					// Skip hidden rows and also take into account the add-new row.
					// VisibleRowCount and GetRowAtVisibleIndex are cached.
					//
					//for ( int i = 0; i < this.Count; i++ )
					//{
					//	UltraGridRow row = this[i];
					for ( int i = 0; i < this.VisibleRowCount; i++ )
					{
						UltraGridRow row = this.GetRowAtVisibleIndex( i );

						Debug.Assert( null != row );
						if ( null != row )
						{
							int rowCardHeight = row.CardHeight;
							totalHeight += rowCardHeight;
								
							if ( i > 0 )
								totalHeight += cardSpacing;

							// SSP 10/20/05 BR06341
							// Added the if block and enclosed the existing code into the else block.
							// 
							if ( CardStyle.VariableHeight == cardStyle )
							{
								if ( rowCardHeight > maxCardHeight )
									maxCardHeight = rowCardHeight;
							}
							// If the height already exceeds height for 1 expanded
							// card, then break.
							//
							else if ( totalHeight >= minHeight )
							{
								break;
							}
						}
					}

					// SSP 10/20/05 BR06341
					// 
					if ( CardStyle.VariableHeight == cardStyle && maxCardHeight + miscHeight < minHeight )
					{
						height = maxCardHeight + miscHeight;
						return true;
					}

					if ( totalHeight < minHeight )
					{
						height = totalHeight;
						return true;
					}
				}
			}

			return false;
		}

		#endregion // ForceSingleColumnCompressedAndVariableCardView

		#region GetTotalActualRowsInCardArea

		// JM UWG1019
		//
		internal int GetTotalActualRowsInCardArea(int totalCardAreaHeight)
		{
			// SSP 6/7/02
			// CVMS Row filtering. In row filtering, when all the rows get filtered out, there
			// won't be any visible rows and VisibleRowCount will rreturn 0. We still
			// want to display the card so the user can unfilter the rows.
			// So if the VisibleRowCount is 0, return 1 because we are going to have
			// at least one card row.
			//
			//return Math.Min(this.Band.GetTotalRowsInCardArea(totalCardAreaHeight), this.VisibleRowCount);
			return Math.Min(this.Band.GetTotalRowsInCardArea(totalCardAreaHeight), 
				Math.Max( 1, this.VisibleRowCount ) );
		}

		#endregion // GetTotalActualRowsInCardArea

		#region ValidateCard

		// SSP 11/14/03 Add Row Feature
		// Added ValidateCard method so that we have one location where we are validating
		// the card rather than having to duplicate the code all over the place.
		//
		internal bool ValidateCard( UltraGridRow row )
		{
			// SSP 4/14/05 - NAS 5.2 Fixed Rows/Filter Row
			// Use the new IsDataRow which checks for IsTemplateAddRow as well as other special
			// rows like the filter row.
			//
			//return this == row.ParentCollection && ( row.Index >= 0 || row.IsTemplateAddRow );
			return this == row.ParentCollection && this.GetRowIndex( row, true ) >= 0;
		}

		#endregion // ValidateCard

		#region VerifyGroupByVersionHelper

		internal void VerifyGroupByVersionHelper( )
		{
			if ( this.rowsDirty )
				this.SyncRows();

			this.VerifyGroupByVersion( );
		}

		#endregion // VerifyGroupByVersionHelper

		#region GetFirstVisibleRow
		
		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetFirstVisibleRow( )
		{
			return this.GetFirstVisibleRow( true );
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetFirstVisibleRow( IncludeRowTypes includeRowTypes )
		{
			return this.GetFirstVisibleRow( IncludeRowTypes.SpecialRows == includeRowTypes );
		}
		
		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetFirstVisibleRow( bool includeSpecialRows )
		{
			// SSP 8/4/03 - Optmizations
			// Use the new optimized GetRowAtVisibleIndex method. This will be faster in
			// the case where lots of rows in the beginning of the collection are filtered 
			// out (or hidden).
			//
			
			return this.GetRowAtVisibleIndex( 0, includeSpecialRows );
		}

		#endregion // GetFirstVisibleRow

		#region GetLastVisibleRow
		
		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetLastVisibleRow( )
		{
			return this.GetLastVisibleRow( true );
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetLastVisibleRow( IncludeRowTypes includeRowTypes )
		{
			return this.GetLastVisibleRow( IncludeRowTypes.SpecialRows == includeRowTypes );
		}

		// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		// Added overloads that take in includeRowTypes and includeSpecialRows.
		//
		internal Infragistics.Win.UltraWinGrid.UltraGridRow GetLastVisibleRow( bool includeSpecialRows )
		{
			// SSP 8/4/03 - Optmizations
			// Use the new optimized GetRowAtVisibleIndex method. This will be faster in
			// the case where lots of rows in the end of the collection are filtered 
			// out (or hidden).
			//
			
			// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// Added overloads that take in includeRowTypes and includeSpecialRows.
			//
			//int vrCount = this.VisibleRowCount;
			//return vrCount > 0 ? this.GetRowAtVisibleIndex( vrCount - 1 ) : null;
			int vrCount = this.GetVisibleRowCount( includeSpecialRows );
			return vrCount > 0 ? this.GetRowAtVisibleIndex( vrCount - 1, includeSpecialRows ) : null;
		}

		#endregion // GetLastVisibleRow

		#region GetRowWithListIndex
        
        // MRS v7.2 - PDF Report Writer
        // Added Overload for backward compatibility and renamed this method 
        // from GetRowWithListIndex        
        internal UltraGridRow GetRowWithListIndexHelper(int listIndex, bool dontSyncRows)
        {
            return this.GetRowWithListIndexHelper(listIndex, dontSyncRows, true);
        }

		// SSP 10/28/02 UWG1787
		// Added an overload of GetRowListIndex that takes in dontSyncRows parameter.
		//
        // MRS v7.2 - PDF Report Writer
		//internal UltraGridRow GetRowWithListIndex( int listIndex, bool dontSyncRows )
        internal UltraGridRow GetRowWithListIndexHelper(int listIndex, bool dontSyncRows, bool create)
		{
			if ( !dontSyncRows )
			{
				if ( this.rowsDirty )
					this.SyncRows( );

				this.VerifyGroupByVersion( );
			}
			
			// SSP 1/22/02
			// If the listIndex is out of range, then return null
			//
			if ( listIndex < 0 || listIndex >= this.UnSortedActualRows.Count )
				return null;
			
			// SSP 4/13/04 - Virtual Binding
			//
			//return (UltraGridRow)this.UnSortedActualRows[ listIndex ];
            // MRS v7.2 - PDF Report Writer
			//return (UltraGridRow)this.UnSortedActualRows.GetItem( listIndex, true );
            return (UltraGridRow)this.UnSortedActualRows.GetItem(listIndex, create);
		}

		#endregion // GetRowWithListIndex

		#region GetRowListIndex

		internal int GetRowListIndex( UltraGridRow row )
		{
			return this.UnSortedActualRows.IndexOf( row );
		}

		#endregion // GetRowListIndex

		#region GetScrollIndex
		
		// SSP 4/27/04 - Virtual Binding
		//
		internal int GetScrollIndex( UltraGridRow row )
		{
			Debug.Assert( this == row.ParentCollection, "Row doesn't belong to this parent collection !" );

			int scrollIndex = this.SparseArray.ScrollIndexOf( row );

			// SSP 4/14/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// We have to take into account filter row, summary row etc...
			// Commented out the original code and added the new code below.
			//
			// ------------------------------------------------------------------------------
			if ( scrollIndex >= 0 )
			{
				scrollIndex += this.GetSpecialRowsScrollCount( true );
			}
			else 
			{
				scrollIndex = this.GetSpecialRowScrollIndex( row, true );

				if ( scrollIndex < 0 )
				{
					scrollIndex = this.GetSpecialRowScrollIndex( row, false );
					if ( scrollIndex >= 0 )
						scrollIndex += this.GetSpecialRowsScrollCount( true ) + this.SparseArray.ScrollCount;
				}
			}
			
			// ------------------------------------------------------------------------------

			return scrollIndex;
		}

		#endregion // GetScrollIndex
		
		#region GetVisibleIndex
		
		// SSP 4/14/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		// Added an overload of GetVisibleIndex that takes in includeSpecialRows parameter.
		//
		internal int GetVisibleIndex( UltraGridRow row )
		{
			return this.GetVisibleIndex( row, true );
		}

		// SSP 4/27/04 - Virtual Binding
		//
		internal int GetVisibleIndex( UltraGridRow row, bool includeSpecialRows )
		{
			Debug.Assert( this == row.ParentCollection, "Row doesn't belong to this parent collection !" );

			int visibleIndex = this.SparseArray.VisibleIndexOf( row );

			// SSP 4/14/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// We have to take into account filter row, summary row etc... For that added
			// includeSpecialRows parameter. Commented out the original code and added the 
			// new code below.
			// 
			// ------------------------------------------------------------------------------
			if ( includeSpecialRows )
			{
				if ( visibleIndex >= 0 )
				{
					visibleIndex += this.GetSpecialRowsVisibleCount( true );
				}
				else 
				{
					visibleIndex = this.GetSpecialRowVisibleIndex( row, true );

					if ( visibleIndex < 0 )
					{
						visibleIndex = this.GetSpecialRowVisibleIndex( row, false );
						if ( visibleIndex >= 0 )
							visibleIndex += this.GetSpecialRowsVisibleCount( true ) + this.SparseArray.VisibleCount;
					}
				}
			}
			
			// ------------------------------------------------------------------------------

			return visibleIndex;
		}

		#endregion // GetVisibleIndex

		#region NotifyRowScrollCountChanged

		// SSP 4/27/04 - Virtual Binding
		//
		internal void NotifyRowScrollCountChanged( UltraGridRow row )
		{
			// SSP 11/11/04 - Merged Cell Feature
			//
			RowsCollection.MergedCell_RowStateChanged( row, MergedCell.State.Expanded );

			this.SparseArray.NotifyItemScrollCountChanged( row );
		}

		#endregion // NotifyRowScrollCountChanged

		#region DirtyParentRowScrollCount
		
		internal void DirtyParentRowScrollCount( )
		{
			this.DirtyParentRowScrollCount( true );
		}

		// SSP 4/27/04 - Virtual Binding
		//
		internal void DirtyParentRowScrollCount( bool dirtyFixedRowsCollection )
		{
			UltraGridBand band = this.Band;
			UltraGridLayout layout = band.Layout;

			// SSP 3/30/05 - NAS 5.2 Fixed Rows
			// When a row is expanded/collapsed the sibling rows after get unfixed (actually they
			// remain in the fixed rows collection however their FixedResolved returns false).
			//
			if ( dirtyFixedRowsCollection && this.HasFixedRowsBeenAllocated )
				this.FixedRows.BumpFixedRowsCollectionVersion( );

			// SSP 3/30/05 - NAS 5.2 Row Numbers
			//
			band.DirtyCachedRowSelectorNumberWidth( );

			UltraGridRow parentRow = this.ParentRow;
			if ( null != parentRow )
			{
				if ( parentRow.Expanded )
				{
					parentRow.NotifyScrollCountChanged( );
				}
					// SSP 8/28/04 UWG3629
					// Added the following else-if block. When a row is added/removed it may
					// affect the visibility of the parent row's expansion indicator. We need
					// to dirty the pre row area ui element. However that's not enough because 
					// the row col region intersection ui element positions the row connector
					// elements differently based on whether the expansion indicator element
					// is positioned or not.
					// 
				else if ( parentRow.IsPotentiallyVisibleOnScreen )
				{
					if ( null != layout )
					{
						// SSP 4/3/06 BR11266
						// If this is called from within paint operation then don't invalidate
						// the grid. This can cause performance issues if ExpansionIndicator is
						// set to CheckOnDisplay and scroll counts have been dirtied on the 
						// child rows whose parents are collapsed.
						// 
						// --------------------------------------------------------------------
						//this.Layout.DirtyGridElement( );
						UltraGridBase grid = layout.Grid;
						if ( null != grid && ! grid.Drawing )
							layout.DirtyGridElement( );
						// --------------------------------------------------------------------
					}
				}
			}
			else if ( null != layout )
			{
				layout.BumpGrandVerifyVersion( );
				layout.DirtyGridElement( true );

				// SSP 10/21/05 BR07016
				// Changing the row filter criteria in an UltraCombo can end up causing 
				// more rows to become visible, necessiating displaying of the scrollbar.
				// UltraCombo keeps includeVerticalScrollbar flag which it sets in the
				// CalcDropDownSize. We need to reset that flag. To do that, force 
				// re-calculation of the drop down size.
				// 
				// ----------------------------------------------------------------------
				UltraDropDownBase ultraDropDown = layout.Grid as UltraDropDownBase;
				// MRS 12/5/05 - BR08024
				// There's no reason to resize the dropdown when sorting and 
				// doing so causes a flicker because the rows collection gets cleared 
				// and then re-populated. 
				//
				//if ( null != ultraDropDown && ! this.inEnsureFiltersEvaluated )
				if ( null != ultraDropDown && ! this.inEnsureFiltersEvaluated && ! this.inEnsureSortOrder )				
					ultraDropDown.ReCalculateDropDownSize( true );
				// ----------------------------------------------------------------------
			}
		}

		#endregion // DirtyParentRowScrollCount

		#region InvalidateRowCollection

		// SSP 3/2/05 BR02644
		// Added InvalidateRowCollection method.
		//
		internal void InvalidateRowCollection( bool dirtyGridElem )
		{
			if ( this.IsPotentiallyVisibleOnScreen )
			{
				UltraGridLayout layout = this.Layout;
				if ( null != layout )
					layout.DirtyGridElement( );
			}
		}

		#endregion // InvalidateRowCollection

		#region GetRowAtScrollIndex
		
		// SSP 4/27/04 - Virtual Binding
		//
		internal UltraGridRow GetRowAtScrollIndex( int scrollIndex )
		{
			return this.GetRowAtScrollIndex( scrollIndex, true );
		}

		// SSP 8/2/06 BR14598
		// Added an overload that takes in allocate parameter.
		// 
		private UltraGridRow GetRowAtScrollIndex( int scrollIndex, bool allocate )
		{
			// In card-view, the whole card island is treated as being associated with a single 
			// scroll position.
			//
			if ( this.IsCardRows )
			{
				// SSP 4/21/04
				// Return the first visible row instead of the first row if we can.
				//
				//return 0 == scrollIndex && this.Count > 0 ? this[0] : null;
				UltraGridRow tmpRow = null;
				if ( 0 == scrollIndex )
				{
					tmpRow = this.GetFirstVisibleRow( );
					if ( null == tmpRow && this.Count > 0 )
						tmpRow = this[0];
				}

				// SSP 11/22/04
				// Return the tmpRow and not null.
				//
				//return null;
				return tmpRow;
			}

			// SSP 4/14/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
			// We have to take into account filter row, summary row etc...
			// Commented out the original code and added the new code below.
			//
			// --------------------------------------------------------------------------------------
			if ( scrollIndex < 0 )
				return null;

			UltraGridRow row = this.GetSpecialRowAtScrollIndex( scrollIndex, true );
			if ( null == row )
			{
				scrollIndex -= this.GetSpecialRowsScrollCount( true );

				// SSP 8/2/06 BR14598
				// Added an overload that takes in allocate parameter.
				// 
				//row = this.SparseArray.GetItemAtScrollIndex( scrollIndex );
				row = this.SparseArray.GetItemAtScrollIndex( scrollIndex, allocate );

				if ( null == row )
				{
					scrollIndex -= this.SparseArray.ScrollCount;
					row = this.GetSpecialRowAtScrollIndex( scrollIndex, false );
				}
			}

			return row;
			
			// --------------------------------------------------------------------------------------
		}

		#endregion // GetRowAtScrollIndex

		// SSP 11/7/03 - Add Row Feature
		//
		#region Add Row Feature - Methods

		#region FireInitializeTemplateAddRow
		
		internal void FireInitializeTemplateAddRow( )
		{
			// Fire InitializeTemplateAddRow event.
			//
			UltraGrid grid = null != this.Layout ? this.Layout.Grid as UltraGrid : null;
			if ( null != grid && null != this.templateAddRow )
				grid.FireEvent( GridEventIds.InitializeTemplateAddRow, new InitializeTemplateAddRowEventArgs( this.templateAddRow ) );
		}

		#endregion // FireInitializeTemplateAddRow

		#region GetTemplateAddRowValue

		internal object GetTemplateAddRowValue( UltraGridColumn column )
		{
			object val = null;
			if ( this.AddRowDefaultCellValues.ContainsKey( column ) )
				val = this.AddRowDefaultCellValues[ column ];
			
			return null != val ? val : column.DefaultCellValue;
		}

		#endregion // GetTemplateAddRowValue

		#region SetTemplateAddRowValue

		internal void SetTemplateAddRowValue( UltraGridColumn column, object val )
		{
			this.AddRowDefaultCellValues[ column ] = val;
		}

		#endregion // SetTemplateAddRowValue

		#region OnTemplateAddRowVisibleStateChanged

		private void OnTemplateAddRowVisibleStateChanged( )
		{
			this.OnTemplateAddRowVisibleStateChanged( this.currentAddRow );
		}

		// SSP 5/2/05 - NAS 5.2 Fixed Rows/Fixed Add Row
		// Added an overload that takes in currentAddRow parameter.
		//
		private void OnTemplateAddRowVisibleStateChanged( UltraGridRow currentAddRowParam )
		{
			// SSP 5/1/05 - NAS 5.2 Fixed Rows_
			// 
			this.verifiedSpecialRowsCacheVersion--;

			// Since setting this flag could potentially end up unhinding or hiding
			// the template add-row, dirty the scrollable row count and invalidate
			// the grid.
			//
			if ( null != this.Layout )
			{
				this.DirtyParentRowScrollCount( );

				// If the add-row is at the top then as soon as it gets modified a new
				// template add-row will appear on top of it. Scroll that row into view.
				//
				RowScrollRegion rsr = this.Layout.ActiveRowScrollRegion;
				if ( null != rsr && null != currentAddRowParam )
				{
					if ( TemplateAddRowLocation.Top == this.TemplateRowLocationResolved && rsr.FirstRow == currentAddRowParam )
					{
						UltraGridRow prevVisibleRow = currentAddRowParam.GetPrevVisibleRow( );

						if ( prevVisibleRow == this.templateAddRow )
						{
							// Set the first row to the template add-row.
							//
							rsr.SetFirstRow( prevVisibleRow );
							rsr.SetDestroyVisibleRows( true );

							// SSP 5/14/07 BR22720
							// Enclosed the existing code into the if block. If we are in the process
							// of syncrhonizing row collection with the underlying list then don't 
							// syncrhonously perform the following two operations as it can lead to
							// operations that assume rows being in a valid state. If this causes
							// a problem, then take out the change and reconsider how to fix the
							// bug.
							// 
							if ( !this.InSyncRows )
							{
								// If the data area elem's height is too small to accomodate the new template
								// add-row and current add-row, then make sure the current add-row is in
								// view because that's what the user is editing.
								//
								rsr.RegenerateVisibleRows( );

								// SSP 3/31/05 BR03101
								// Pass in false for the exit edit mode.
								//
								//rsr.ScrollRowIntoView( this.currentAddRowParam );
								// SSP 5/12/05 - NAS 5.2 Fixed Add Row
								// Pass in true for dontUpdateGridControl parameter to reduce prevent 
								// any flicker.
								//
								//rsr.ScrollRowIntoView( currentAddRowParam, false );
								rsr.ScrollRowIntoView( currentAddRowParam, false, true );
							}
						}
					}
					// SSP 5/2/05 - NAS 5.2 Fixed Rows/Fixed Add Row
					// 
					else if ( AllowAddNew.FixedAddRowOnTop == this.Band.AllowAddNewResolved 
						|| AllowAddNew.FixedAddRowOnBottom == this.Band.AllowAddNewResolved )
					{
						// Only scroll the commited add-row into view when committing the add-row.
						// That state would be indicated by the this.currentAddRow being null since
						// when the add-row is committed the currentAddRow member var on the rows 
						// collection is set to null.
						//
						if ( null == this.currentAddRow )
							// SSP 5/12/05 - NAS 5.2 Fixed Add Row
							// Pass in true for dontUpdateGridControl parameter to reduce prevent 
							// any flicker.
							//
							//rsr.ScrollRowIntoView( currentAddRowParam, false );
							rsr.ScrollRowIntoView( currentAddRowParam, false, true );
					}
				}

				this.Layout.DirtyGridElement( );
			}
		}

		#endregion // OnTemplateAddRowVisibleStateChanged

		#region OnUserModifiedCellValue

		internal void OnUserModifiedCellValue( UltraGridCell cell )
		{
			if ( null != cell && cell.Row == this.currentAddRow )
				this.CurrentAddRowModifiedByUser = true;

			// SSP 12/7/04 - Merged Cell Feature
			// As soon as the user modifies the cell unmerged the associated merged cell 
			// at the cell.
			//
			RowsCollection.MergedCell_CellStateChanged( cell, MergedCell.State.Value );
		}

		#endregion // OnUserModifiedCellValue

		#region SyncRows_GetTemplateAddRow
		
		// SSP 4/5/04 - Virtual Binding Related Optimizations
		// Took out listIndex parameter.
		//
		//internal UltraGridRow SyncRows_GetTemplateAddRow( int listIndex )
		internal UltraGridRow SyncRows_GetTemplateAddRow( )
		{
			UltraGridRow row = this.TemplateAddRow;
			// SSP 4/5/04 - Virtual Binding Related Optimizations
			//
			//row.InternalListIndex = listIndex;
			this.templateAddRow = null;
			this.CurrentAddRow = row;
			return row;
		}

		#endregion // SyncRows_GetTemplateAddRow

		#region CopyDefaultValuesToAddRow
		
		internal void CopyDefaultValuesToAddRow( UltraGridRow addRow )
		{
			foreach ( UltraGridColumn column in addRow.Band.Columns )
			{
				if ( ! column.IsChaptered )
				{
					object obj = this.GetTemplateAddRowValue( column );
					
					UltraGridGroupByRow ancestorGroupByRow = this.parentRow as UltraGridGroupByRow;
					while ( null != ancestorGroupByRow )
					{
						if ( ancestorGroupByRow.Column == column )
						{
							obj = ancestorGroupByRow.Value;
							break;
						}
					}

					if ( null != obj )
						addRow.SetCellValue( column, obj );
				}
			}
		}

		#endregion // CopyDefaultValuesToAddRow

		#region AddTemplateAddRowToDataList
		
		internal bool AddTemplateAddRowToDataListHelper( bool activateFirstCellInNewRow, out UltraGridRow addedRow )
		{
			addedRow = null;

			Debug.Assert( null != this.Layout );
			if ( null == this.Layout || ! ( this.Layout.Grid is UltraGrid ) )
				return false;

			if ( null != this.ParentRow )
			{
				UltraGridLayout layout = this.Layout;
				UltraGrid grid = (UltraGrid)layout.Grid;

				// Set the IgnoreDataSourePositionChange to true so that we don't try to
				// activate the parent row when we get PositionChanged event.
				//
				bool origIgnoreDataSourePositionChange = layout.IgnoreDataSourePositionChange;
				layout.IgnoreDataSourePositionChange = true;
				try
				{
					this.ParentRow.InternalPositionToRow( );
				}
				finally
				{
					layout.IgnoreDataSourePositionChange = origIgnoreDataSourePositionChange;
				}

				if ( ! this.ParentRow.AreAllAncestorsCurrentRows
					// SSP 7/5/07 BR22475
					// If SyncWithCurrencyManager is set to false then InternalPositionToRow will
					// do nothing above. In which case we still should support adding rows.
					// 
					&& grid.SyncWithCurrencyManager
					)
					return false;
			}

			this.inAddingTemplateAddRow = true;
			try
			{
				UltraGridRow origTemplateAddRow = this.TemplateAddRow;
				this.Band.AddNewHelper( this );
				//Debug.Assert( origTemplateAddRow != this.templateAddRow );
				if ( origTemplateAddRow != this.templateAddRow )
				{
					// If activateFirstCellInNewRow parameter indicates that we should enter
					// the first cell in the row in edit mode, then do so.
					//
					addedRow = this.CurrentAddRow;
					Debug.Assert( null != addedRow, "CurrentAddRow not set !" );
					if ( null != addedRow && activateFirstCellInNewRow )
					{
						// SSP 11/25/02 UWG1858
						// Only activate the first cell if no other cell from the row has been activated.
						// Enclosed the below code into the if block.
						//
						// SSP 17/04 BR00446
						// AddNewHelper method activates the added row. If the activation fails because
						// for example BeforeRowDeactivate is canceled and as a result the previous row
						// remains active then we should not attempt to activate the added row again 
						// which is what the EnterFirstCellInEditMode would do. We should simply return
						// in that case.
						// 
						//if ( null == this.Layout.ActiveCell || this.Layout.ActiveCell.Row != addedRow )
						if ( null == this.Layout.ActiveCell && this.Layout.ActiveRow == addedRow )
							addedRow.EnterFirstCellInEditMode( );
					}

					return true;
				}
			}
			finally
			{
				this.inAddingTemplateAddRow = false;
			}

			return false;
		}

		internal bool AddTemplateAddRowToDataList( bool activateFirstCellInNewRow )
		{
			try
			{
				UltraGridRow addedRow;
				return this.AddTemplateAddRowToDataListHelper( activateFirstCellInNewRow, out addedRow );
			}
			catch ( Exception e )
			{
				UltraGrid grid = this.Layout.Grid as UltraGrid;
				if ( null != grid )
				{
					DataErrorInfo dataError = new DataErrorInfo( e, null, null, null,
						DataErrorSource.RowAdd, 
						SR.GetString( "DataErrorRowAddMessage", e.Message ) );

					grid.InternalHandleDataError( dataError );
				}

				return false;
			}
		}

		#endregion // AddTemplateAddRowToDataList

		#endregion // Add Row Feature - Methods

		#region VerifyFilterInfo

		// SSP 8/1/03 UWG1654 - Filter Action
		// Added a mechanism to cache AreAnyFiltersActive.
		//
		internal void VerifyFilterInfo( )
		{
			// SSP 5/13/04 UWG3260 - Virtual Binding
			// Combined the verified band and rows version numbers into one.
			//
			//if ( this.verifiedRowFiltersVersion != this.RowFiltersVersion2 + this.Band.RowFiltersVersion )
			//{
			//	this.verifiedRowFiltersVersion = this.RowFiltersVersion + this.Band.RowFiltersVersion;
			// SSP 5/18/04 UWG3292
			// Check for band and layout being null.
			//
			//if ( this.verifiedRowFiltersVersion != this.RowFiltersVersion )
			// MD 7/27/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( this.verifiedRowFiltersVersion != this.RowFiltersVersion && null != this.band && null != this.band.Layout )
			//{
			//    this.verifiedRowFiltersVersion = this.RowFiltersVersion;
			int rowFiltersVersion = this.RowFiltersVersion;

			if ( this.verifiedRowFiltersVersion != rowFiltersVersion && null != this.band && null != this.band.Layout )
			{
				this.verifiedRowFiltersVersion = rowFiltersVersion;

				// SSP 10/27/04
				// This is being done in EnsureFiltersEvaluated now.
				//
				

				this.cachedAreAnyFiltersActive = 
					RowFilterMode.AllRowsInBand == this.Band.RowFilterModeResolved 
					? this.Band.AreAnyBandFiltersActive
					: this.HasColumnFilters && this.ColumnFilters.IsAnyColumnFilterActive;
			}
		}

		#endregion // VerifyFilterInfo

		#region EnsureFiltersEvaluated
		
		internal ColumnFiltersCollection ActiveColumnFilters
		{
			get
			{
				// SSP 5/12/05 
				// ----------------------------------------------------------------------
				
				return RowFilterMode.AllRowsInBand == this.band.RowFilterModeResolved 
					? this.band.ColumnFiltersInternal 
					: this.TopLevelRowsCollection.columnFilters;
				// ----------------------------------------------------------------------
			}
		}

		#region ActiveColumnFiltersForceAllocation

		// SSP 5/12/05 - NAS 5.2 Filter Row
		// Added ActiveColumnFiltersForceAllocation property.
		//
		internal ColumnFiltersCollection ActiveColumnFiltersForceAllocation
		{
			get
			{
				return RowFilterMode.AllRowsInBand == this.band.RowFilterModeResolved 
					? this.band.ColumnFilters
					: this.ColumnFilters;
			}
		}

		#endregion // ActiveColumnFiltersForceAllocation

		#region FiltersNeedToBeEvaluated

		// SSP 4/11/07 BR21578
		// 
		/// <summary>
		/// Indicates whether the filters need to be evaluated on rows.
		/// </summary>
		internal bool FiltersNeedToBeEvaluated
		{
			get
			{
				return this.RowFiltersVersion != this.ensureFiltersEvaluated_verifiedFiltersVersion;
			}
		}

		#endregion // FiltersNeedToBeEvaluated

		// SSP 10/27/04 - UltraCalc
		// Moved the filter verification logic into the row collection. This is so
		// that we can evaluate filters at once on all the rows of a row collection
		// and fire rows synced event to the calc manager instead of having to fire
		// row deleted/added event for every row whose hidden state changes.
		//
		internal void EnsureFiltersEvaluated( )
		{
			if ( this.ensureFiltersEvaluated_verifiedFiltersGrandVersion == this.band.Layout.GrandVerifyVersion )
				return;

			// SSP 1/31/05
			// If we are in SyncRows or in the process of changing the group-by columns (as indicated 
			// by the inProcessingNewSortedColumns flag) then don't evaluate the filters yet. Wait till
			// these operations to finish before applying filters.
			//
			// ----------------------------------------------------------------------------------------
			SortedColumnsCollection sortedColumns = this.band.InternalSortedColumns;
			if (
				// SSP 11/11/05 BR07435
				// Use the property which checks for the top-level row collection.
				// 
				//this.inSyncRows 
				this.InSyncRows 
				|| null != sortedColumns && sortedColumns.inProcessingNewSortedColumns )
				return;
			// ----------------------------------------------------------------------------------------

            // MBS 2/26/09 - TFS14232 
            // Removed this code in favor of a fix in the SortedColumnsCollection's OnSortChanged
            //
            #region Removed
            //// MBS 2/11/09 - TFS12415
            //// We don't want to try to verify the filters if we're in the middle of initializing the
            //// group-by rows, since the sparse array will not have fully updated its contents yet
            //// and the IsGroupByRows property could return true without the sparse array
            //// actually containing GroupByRows
            //if (this.inInitGroupByRows || this.TopLevelRowsCollection.inInitGroupByRows)
            //    return;
            ////
            //// If we're not initializing the group-by rows, then we should make sure that we've evaluated any 
            //// pending changes to the structure before we update our filter version numbers.  This fixes an issue
            //// where we may have set both a GroupByColumn and filter in the same operation and the grid needs
            //// to process all of these on the next paint.
            //this.VerifyGroupByVersion();
            #endregion //Removed

            // MRS 3/9/2009 - TFS12415
            // We should not ensure the filters if we are in the middle of initialization
            // the GroupByRows. 
            //
            if (this.inInitGroupByRows || this.TopLevelRowsCollection.inInitGroupByRows)
                return;

            this.ensureFiltersEvaluated_verifiedFiltersGrandVersion = this.band.Layout.GrandVerifyVersion;

            if (this.RowFiltersVersion == this.ensureFiltersEvaluated_verifiedFiltersVersion
                || this.inEnsureFiltersEvaluated)
                return;            

			this.inEnsureFiltersEvaluated = true;

			try
			{
				this.EnsureNotDirty( );
				this.VerifyFilterInfo( );

				this.ensureFiltersEvaluated_verifiedFiltersVersion = this.RowFiltersVersion;

                
				if ( this.Disposed)
                    // MRS 3/9/2009 - TFS12415
                    // Bail out if this rows collection is not valid. 
                    //
                    // MBS 5/19/09 - TFS17243
                    // Pulling out this fix in favor of one below, since checking IsStillValid will cause the 
                    // SparseArray to reverify the scroll position, which needs to check the visible rows.
                    // Since we haven't re-applied the filters at this point, rows that were previously filtered 
                    // out could still remain filtered out even though they match the filtering criteria. 
                    //
                    //|| this.IsStillValid == false)
                    return;

				// SSP 5/13/04 UWG3260
				// Since we are going to be re-evaluating the filters on rows, dirty the scroll count.
				//
				if ( null != this.sparseArray )
					this.sparseArray.DirtyScrollCount( );

				// SSP 12/3/04 - Merged Cell Feature
				// Clear merged cell cache since the row orders could have changed.
				//
				this.ClearMergedCellCache( );

				bool allChildRowsFilteredOut = true;

				// Filters don't apply to group-by rows. However we want to return true from
				// IsFilteredOut property of the group-by row if all its children are filtered out.
				//
				if ( this.IsGroupByRows )
				{
                    // MBS 5/19/09 - TFS17243
                    // Instead of checking IsStillValid, which can have additional side effects as noted
                    // above, simply check to see if the current GroupByRows version isn't the same as
                    // the band's.  If this is the case, we should delay checking the filtering at this
                    // point since the rows might well not be valid.
                    if (this.verifiedGroupByHierarchyBandsVersion != this.band.GroupByHierarchyVersion)
                        return;

					// SSP 11/19/04 BR00655
					// Implemented UltraGridRow.IsFilteredOut and RowsCollection.FilteredInRowCount
					// for group-by rows. Before group-by rows were never considered to be filtered 
					// out. Now if all the children of a group-by row are filtered out then the 
					// group-by row is considered to be filtered out as well.
					//                    
                    foreach (UltraGridGroupByRow groupByRow in this.SparseArray)
                    {
                        groupByRow.Rows.EnsureFiltersEvaluated();
                        allChildRowsFilteredOut = allChildRowsFilteredOut && groupByRow.InternalIsFilteredOut;
                    }
				}
				else
				{
					ColumnFiltersCollection columnFilters = this.ActiveColumnFilters;
					this.cachedAreAnyFiltersActive = null != columnFilters && columnFilters.IsAnyColumnFilterActive;

					// SSP 11/19/04 BR00655
					// Implemented UltraGridRow.IsFilteredOut and RowsCollection.FilteredInRowCount
					// for group-by rows. Before group-by rows were never considered to be filtered 
					// out. Now if all the children of a group-by row are filtered out then the 
					// group-by row is considered to be filtered out as well.
					//
					// ----------------------------------------------------------------------------
					//foreach ( UltraGridRow row in this.SparseArray.NonNullItems )
					//	row.ApplyFilters( columnFilters );
					foreach ( UltraGridRow row in this.SparseArray.NonNullItems )
					{
						row.ApplyFilters( columnFilters );
						allChildRowsFilteredOut = allChildRowsFilteredOut && row.InternalIsFilteredOut;

						// SSP 4/22/05 BR03517
						// This is to fix the situation where if the VisibleRowCount (or any method
						// that would cause the sparse array to calculate its visible/scroll info) 
						// is called from within the FilterRow event handler then it would return 
						// the wrong result. The reason is that we are dirtying the scroll count in 
						// the beginning of this method which means that the sparse array would 
						// verify the visible/scroll counts when VisibleRowCount is called and after 
						// that we would be hiding the rows here without notifying the sparse array.
						//
						this.SparseArray.NotifyItemScrollCountChanged( row );
					}
					// ----------------------------------------------------------------------------

					// SSP 9/4/02 UWG1630
					// If a cell from this row collection (or a descendant row) is in edit mode and it's
					// not visible any more (possibly due to apply filters above) then exit the edit mode
					// on the cell. Otherwise we will end up in a weird state where the cell is not 
					// visible but it's in edit mode.
					//
					UltraGridCell activeCell = this.Layout.ActiveCell;
					if ( null != activeCell && activeCell.IsInEditMode 
						&& activeCell.Row.IsDescendantOf( this ) && activeCell.Row.HiddenResolved )
						activeCell.ExitEditMode( false, true );
				}

				// SSP 11/19/04 BR00655
				// Implemented UltraGridRow.IsFilteredOut and RowsCollection.FilteredInRowCount
				// for group-by rows. Before group-by rows were never considered to be filtered 
				// out. Now if all the children of a group-by row are filtered out then the 
				// group-by row is considered to be filtered out as well.
				//
				UltraGridGroupByRow parentGroupByRow = this.parentRow as UltraGridGroupByRow;
				if ( null != parentGroupByRow )
					parentGroupByRow.InternalIsFilteredOut = allChildRowsFilteredOut;

				// If the filtered state of a row changes, then dirty the summaries so they
				// get recalculated.
				//
				this.DataChanged_DirtySummaries( );

				if ( FormulaRowIndexSource.VisibleIndex == this.Band.FormulaRowIndexSourceResolved )
					this.NotifyCalcManager_RowsCollectionResynced( );
			}
			finally
			{
				this.inEnsureFiltersEvaluated = false;
			}
		}

		#endregion // EnsureFiltersEvaluated

		#region GetFilteredRows

		// SSP 11/19/04 BR00655
		//
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void GetFilteredRows( ArrayList list, bool filteredOut )
		private void GetFilteredRows( List<UltraGridRow> list, bool filteredOut )
		{
			this.EnsureFiltersEvaluated( );

			if ( ! this.IsGroupByRows )
			{
				foreach ( UltraGridRow row in this )
				{
					if ( filteredOut == row.IsFilteredOut )
						list.Add( row );
				}
			}
			else
			{
				foreach ( UltraGridGroupByRow groupByRow in this )
					groupByRow.Rows.GetFilteredRows( list, filteredOut );
			}
		}

		#endregion // GetFilteredRows

		#region GetAllNonGroupByRows

		// SSP 11/19/04 BR00655
		//
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void GetAllNonGroupByRows( ArrayList list )
		private void GetAllNonGroupByRows( List<UltraGridRow> list )
		{
			if ( ! this.IsGroupByRows )
			{
				foreach ( UltraGridRow row in this )
					list.Add( row );
			}
			else
			{
				foreach ( UltraGridGroupByRow groupByRow in this )
					groupByRow.Rows.GetAllNonGroupByRows( list );
			}
		}

		#endregion // GetAllNonGroupByRows

		// SSP 11/3/04 - Merged Cell Feature
		//
		#region Merged Cell Feature

		#region BumpRowHeightVersion

		internal void BumpRowHeightVersion( )
		{
			if ( this.HasMergedCellCache )
				this.MergedCellCache.BumpRowHeightVersion( );
		}

		#endregion // BumpRowHeightVersion

		#region ClearMergedCellCache
		
		internal void ClearMergedCellCache( )
		{
			this.mergedCellCache = null;
		}

		#endregion // ClearMergedCellCache

		#region ClearUndoHistoryHelper

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// 
		private void ClearUndoHistoryHelper( MergedCell.State state )
		{
			// DO NOTHING
		}

		#endregion // ClearUndoHistoryHelper

		#region MergedCell_RowStateChanged
		
		internal static void MergedCell_RowStateChanged( UltraGridRow row, MergedCell.State state )
		{
			RowsCollection rows = null != row ? row.ParentCollection : null;
			if ( null != rows )
			{
				if ( null != rows.mergedCellCache )
					rows.mergedCellCache.MergedCell_RowStateChanged( row, state );

				// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				// 
				rows.ClearUndoHistoryHelper( state );
			}
		}

		#endregion // MergedCell_RowStateChanged

		#region MergedCell_CellStateChanged
		
		internal static void MergedCell_CellStateChanged( UltraGridCell cell, MergedCell.State state )
		{
			RowsCollection rows = null != cell && null != cell.Row ? cell.Row.ParentCollection : null;
			if ( null != rows )
			{
				if ( null != rows.mergedCellCache )
					rows.mergedCellCache.MergedCell_CellStateChanged( cell, state );

				// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				// 
				rows.ClearUndoHistoryHelper( state );
			}
		}

		internal static void MergedCell_CellStateChanged( UltraGridRow row, UltraGridColumn column, MergedCell.State state )
		{
			RowsCollection rows = null != row ? row.ParentCollection : null;
			if ( null != rows )
			{
				if ( null != rows.mergedCellCache )
					rows.mergedCellCache.MergedCell_CellStateChanged( row, column, state );

				// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
				// 
				rows.ClearUndoHistoryHelper( state );
			}
		}

		#endregion // MergedCell_CellStateChanged

		#region VerifyMergedCellVersion

		internal void VerifyMergedCellVersion( )
		{
			if ( null != this.mergedCellCache && null != this.Band 
				&& this.Band.MergedCellVersion != this.mergedCellCache.verifiedMergedCellVersion )
			{
				// Version number is being reset in the MergedCellCache' constructor.
				//
				this.ClearMergedCellCache( );
			}
		}

		#endregion // VerifyMergedCellVersion

		#endregion // Merged Cell Feature

		#region GetRowFromPrintRow

        // MRS v7.2 - PDF Report Writer
        // Overload for backward compatibility
        internal UltraGridRow GetRowFromPrintRow(UltraGridRow printRow)
        {
            return this.GetRowFromPrintRow(printRow, true);
        }

		// SSP 5/18/05 BR04116
		// Added GetRowFromPrintRow.
		//
        // MRS v7.2 - PDF Report Writer
		//internal UltraGridRow GetRowFromPrintRow( UltraGridRow printRow )
        internal UltraGridRow GetRowFromPrintRow(UltraGridRow printRow, bool create)
		{
            // MRS v7.2 - PDF Report Writer
			//return this.GetRowWithListIndex( printRow.ListIndex);
            return this.GetRowWithListIndex(printRow.ListIndex, create);
		}

		#endregion // GetRowFromPrintRow

		#region IsTrivialDescendantOf

		// SSP 12/8/05 BR07665
		// 
		internal bool IsTrivialDescendantOf( RowsCollection rows )
		{
			if ( this == rows )
				return true;

			RowsCollection parentRows = null != this.parentRow ? this.parentRow.ParentCollection : null;
			return null != parentRows && parentRows.IsTrivialDescendantOf( rows );
		}

		#endregion // IsTrivialDescendantOf

		// SSP 12/13/05 - Recursive Row Enumerator
		// 
		#region GetRowEnumerator

		// MD 1/24/08
		// Made changes to allow for VS2008 style unit test accessors
		//internal class MeetsCriteriaGridRowType : GridUtils.IMeetsCriteria
		internal class MeetsCriteriaGridRowType : IMeetsCriteria
		{
			private GridRowType rowTypes;
			private UltraGridBand bandToMatch;

			internal MeetsCriteriaGridRowType( GridRowType rowTypes, UltraGridBand bandToMatch )
			{
				this.rowTypes = rowTypes;
				this.bandToMatch = bandToMatch;
			}

			private bool DoesRowMatchRowType( UltraGridRow row )
			{
				if ( 0 != ( GridRowType.DataRow & this.rowTypes ) && row.IsDataRow )
					return true;

				if ( 0 != ( GridRowType.GroupByRow & this.rowTypes ) && row.IsGroupByRow )
					return true;

				return false;
			}

			public bool MeetsCriteria( object item )
			{
				UltraGridRow row = (UltraGridRow)item;

				if ( null != this.bandToMatch && this.bandToMatch != row.BandInternal )
					return false;

				if ( ! this.DoesRowMatchRowType( row ) )
					return false;

				return true;
			}
		}

		/// <summary>
		/// Gets the rows associated with this rows collection as well as descendant row collections
		/// upto the lowestLevelBand.
		/// </summary>
		/// <param name="rowTypes">Whether to get data rows or group-by rows or both.</param>
		/// <param name="bandToMatch">If this parameter is non-null then rows from
		/// only this band will be returned. If it is null, then rows from all bands will be returned.</param>
		/// <param name="lowestLevelBand">If this parameter is non-null then rows from
		/// bands upto the lowestLevelBand (inclusive) will be returned. If <b>bandToMatch</b> is
		/// specified as non-null then this will be ignored.</param>
		/// <returns>Returns an IEnumerable instance that can be used to enumerate the rows.
		/// Note that you can use a for each loop to traverse the rows.</returns>
		/// <remarks>
		/// <seealso cref="UltraGridBand.GetRowEnumerator"/>
		/// </remarks>
		public IEnumerable GetRowEnumerator( GridRowType rowTypes, UltraGridBand bandToMatch, UltraGridBand lowestLevelBand )
		{
			IEnumerable e = new RowsEnumerator.Enumerable(
				this, null != bandToMatch ? bandToMatch : lowestLevelBand );

			return new GridUtils.MeetsCriteriaEnumerable( e, 
				new MeetsCriteriaGridRowType( rowTypes, bandToMatch ) );
		}

		#endregion // GetRowEnumerator

		#endregion // Private/Internal Methods

		#region Private/Internal Properties

		#region SparseArray

		internal ScrollCountManagerSparseArray SparseArray
		{
			get
			{
				return this.sparseArray;
			}
		}

		#endregion // SparseArray

		#region NonNullItems

		// SSP 4/30/04 - Virtual Binding
		//
		internal IEnumerable NonNullItems
		{
			get
			{
				this.EnsureNotDirty( );
				return this.SparseArray.NonNullItems;
			}
		}

		#endregion // NonNullItems

		#region Layout
		
		internal UltraGridLayout Layout
		{
			get
			{
				return null != this.band ? this.band.Layout : null;
			}
		}

		#endregion // Layout

		#region SummaryValuesVisible

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Added an overload of GetSummaryValueFromPosition that takes in context.
		//
		//internal bool HasVisibleSummaryValues
		internal bool HasVisibleSummaryValues( SummaryDisplayAreaContext context )
		{
			return 
				// SSP 7/18/02 UWG1389
				// If the band doesn't have any visible summaries, then return false
				// so we don't create any summary footer.
				//
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Pass along the new context paramter.
				//
				//this.Band.HasSummaries && this.Band.Summaries.HasSummaryVisibleInFooter
				//&& this.HasSummaryValues && this.SummaryValues.GetSummaryFooterHeight( ) > 0;
				this.Band.HasSummaries && this.Band.Summaries.HasSummaryVisibleInFooter( context )
				&& this.HasSummaryValues && this.SummaryValues.GetSummaryFooterHeight( context ) > 0;
		}

		#endregion

		#region HasSummaryValues

		internal bool HasSummaryValues
		{
			get
			{
				return this.SummaryValues.Count > 0;
			}
		}

		#endregion

		#region HasSummaryValuesAllocated

		internal bool HasSummaryValuesAllocated
		{
			get
			{
				return null != this.summaryValues;
			}
		}

		#endregion	

		#region DataChanged_DirtySummaries

		internal void DataChanged_DirtySummaries( )
		{
			if ( this.IsTopLevel )
			{
				this.dataChangedVersionNumber++;

				// SSP 6/17/04 UWG3207
				// Added a way to suspend dirtying of summaries when we receive ItemChanged
				// notification.
				//
				// ------------------------------------------------------------------------
				if ( null != this.Layout && null != this.Layout.Grid 
					&& this.Layout.Grid.SummaryUpdatesSuspended )
					return;
				// ------------------------------------------------------------------------

				// SSP 3/26/04 UWG3101
				// If we have group-by rows then the top-most collection won't have any summary
				// values. Commented out the original code and added the new code below it.
				//
				// ------------------------------------------------------------------------------
				//bool flagNeedsInvalidate = this.HasSummaryValuesAllocated && this.HasSummaryValues;
				bool flagNeedsInvalidate = 
					this.IsGroupByRows 
					? null != this.Band && this.Band.HasSummaries
					: this.HasSummaryValuesAllocated && this.HasSummaryValues;
				// ------------------------------------------------------------------------------

				RowsCollection rows = null != this.ParentRow ? this.ParentRow.ParentCollection : null;

				while ( null != rows )
				{
					if ( rows.HasSummaryValuesAllocated && rows.HasSummaryValues )
					{
						flagNeedsInvalidate = rows.SummaryValues.DirtySummaryValues( this.Band ) || flagNeedsInvalidate;
					}

					rows = null != rows.ParentRow ? rows.ParentRow.ParentCollection : null;
				}                

				// If there are summaries, then dirty the data area ui element.
				//
                if (flagNeedsInvalidate && null != this.Layout)
                {
                    // SSP 7/21/05 - NAS 5.3 Header Placement
                    // Also bump the row elem version numbers so the summary footer element repositions
                    // itself.
                    // 
                    //this.Layout.DirtyDataAreaElement( );
                    //
                    // MBS 10/29/08 - TFS9266
                    // Instead of dirtying the entire element, only invalidate the summaries themselves,
                    // for performance reasons.  It doesn't seem liked we need to dirty the entire
                    // element to get the footer to reposition, since I was able to add/update rows
                    // in a variety of settings (i.e. grouped, child rows, etc) and the footer 
                    // was repositioned correctly
                    //
                    //this.Layout.DirtyDataAreaElement( false, true, true );
                    //
					// MD 2/10/09 - TFS13331
					// This causes a huge performance hit when there are multiple summaries in each rows colleciton. For each cell being updated, we loop through each
					// summary value. A new static method has been added ot the SummaryValue which optimaizes the dirtying of elements when multiple summary values are
					// present.
					//foreach (SummaryValue val in this.SummaryValues)
					//    val.InvalidateItemAllRegions();
					SummaryValue[] summaryValues = new SummaryValue[ this.SummaryValues.Count ];

					for ( int i = 0; i < this.SummaryValues.Count; i++ )
						summaryValues[ i ] = this.SummaryValues[ i ];

					SummaryValue.InvalidateItemAllRegions( summaryValues, this.Band, this );
                }
			}
			else
			{
				this.TopLevelRowsCollection.DataChanged_DirtySummaries( );
			}
		}

		#endregion // DataChanged_DirtySummaries

		#region DataChangedVersion

		internal int DataChangedVersion
		{
			get
			{
				if ( this.IsTopLevel )
					return this.dataChangedVersionNumber;
				else
					return this.TopLevelRowsCollection.dataChangedVersionNumber;
			}
		}

		#endregion // DataChangedVersion

		#region IsBoundToBindingList
		
		internal bool IsBoundToBindingList
		{
			get
			{
				return null != this.Band.BindingList;
			}
		}

		#endregion // IsBoundToBindingList

		#region Data_List
		
		internal IList Data_List
		{
			get
			{
				// SSP 2/1/02 UWG1017
				// Added this property.
				//

				// SSP 9/15/03 - Optimizations
				// Cache the data list in the top level rows collection rather
				// than every children in case we have group-by rows.
				//
				// ------------------------------------------------------------
				if ( ! this.IsTopLevel )
					return this.TopLevelRowsCollection.Data_List;
				// ------------------------------------------------------------

				// SSP 10/15/03
				// If band is null, return null.
				//
				if ( null == this.Band )
					return null;

				if ( null == this.Band.ParentBand )
					return this.Band.List;

				UltraGridRow parentRow = this.ParentRow;
				if ( parentRow is UltraGridGroupByRow )
					parentRow = parentRow.FindActualAncestorRow;

				Debug.Assert( null != parentRow, "If the this.band.ParentBand is non-null, then parent row must also be non-null !" );

				if ( null == parentRow )
					return null;
                
				object parentRowListObject = parentRow.ParentCollection.InternalGetListObjectForRow( parentRow );
				
				if ( null == parentRowListObject )
					return null;
				
				// SSP 12/10/02 UWG1883
				// Implemented code to cache the binding list.
				//
				// return this.band.ParentColumn.PropertyDescriptor.GetValue( parentRowListObject ) as IList;
				// --------------------------------------------------------------------------------
				if ( this.lastParentListObject == parentRowListObject && null != this.cachedList )
					return this.cachedList;

				this.lastParentListObject = parentRowListObject;

                // MBS 11/4/08 - TFS9064
                // If the object returned by the property descriptor is an IList<> but does not implement
                // IList, we will try to create the cached list based on the type information, assuming
                // that the list object implements IEnumerable<>
                //
				//this.cachedList = this.band.ParentColumn.PropertyDescriptor.GetValue( parentRowListObject ) as IList;
                object listObject = this.band.ParentColumn.PropertyDescriptor.GetValue( parentRowListObject );
                this.cachedList = listObject as IList;
                //
                if(this.cachedList == null && listObject != null)
                {
                    Type listType;
                    if (DataBindingUtils.ImplementsGenericIEnumerable(listObject.GetType(), out listType))
                    {
                        Type newList = typeof(List<>).MakeGenericType(listType);
                        this.cachedList = Activator.CreateInstance(newList, listObject) as IList;
                    }
                }

				// SSP 10/14/03 UWG2703
				// If the cached list changes, then rehook.
				//
				// ------------------------------------------------------------------------------
				if ( null != this.cachedList && this.cachedList != this.oldBoundList )
				{
					this.BindToList( this.cachedList as IBindingList );

					// SSP 2/4/04 UWG2945
					// Since the list has changed, bump the verify version number so we
					// discard the cached listObjects off the rows.
					//
					this.verifyVersion++;

					if ( ! this.IsRowsDirty && ! this.inSyncRows 
						// SSP 3/2/05
						// We need to check if the binding list count is different from the unsorted actual rows count
						// and not the sparse array count because in the case of the group-by rows sparse array count
						// will be the count of the group-by rows.
						//
						//&& this.cachedList.Count != this.SparseArray.Count 
						&& ( null == this.unsortedActualRows || this.cachedList.Count != this.unsortedActualRows.Count ) )
						this.DirtyRows( );
				}
				// ------------------------------------------------------------------------------

				return this.cachedList;
				// --------------------------------------------------------------------------------
			}
		}

		#endregion // Data_List

		#region BindingList

		internal IBindingList BindingList
		{
			get
			{
				// SSP 2/1/02 UWG1017
				// Moved below code down into Data_List property that
				// returns an IList which we return as IBindingList.
				//
				
				
				return this.Data_List as IBindingList;
			}
		}

		#endregion // BindingList

		#region ResolvedDataList

		// SSP 11/19/03 Add Row Feature
		// Added ResolvedDataList property.
		//
		internal IList ResolvedDataList
		{
			get
			{
				if ( null != this.band )
				{
					// SSP 9/17/01
					// If the parentRow is the active row, (and all of it's ancestors 
					// are ative in their respective bands ), use the List off the 
					// band. (NOTE: Here active row means the row is the current row
					// in the associated binding manager).
					//
					if ( null == this.band.ParentBand 
						|| null != this.parentRow && this.parentRow.AreAllAncestorsCurrentRows 
						// SSP 11/18/05 BR07819
						// If bound to a non-IBindingList, the currency manager does not refresh
						// its List propertly. Sometimes it holds on to the wrong list, list that
						// may have been deleted already (maybe because the parent row got deleted).
						// This is because if bound to a non-IBindingList, there are no notifications 
						// notifying the currency manager of changes.
						// 
						&& null != this.band.BindingList )
					{
						return this.band.List;
					}					
				}
							
				// SSP 9/10/03 UWG2308
				// Use the Data_List property which caches the list. Calling GetValue
				// twice somehow causes the DataSet datasource to allocate a lot of memory
				// and also cause massive slowdowns. Run UWG2308 sample and see the 
				// difference this change makes.
				//
				// ------------------------------------------------------------------------
				//// For child rows we get the list by calling GetValue
				//// on the propertydescriptor of the band's parent column
				////
				//list = this.band.ParentColumn.PropertyDescriptor.GetValue( this.parentRow.ListObject ) as IList;
				return this.Data_List;
				// ------------------------------------------------------------------------
			}
		}

		#endregion // ResolvedDataList

		#region VerifyVersion

		// SSP 10/4/01 UWG483
		// Added this property. Instead of having verifyVersion an internal
		// member variable that Row.ListObject access, created this property
		// to do that. Also it was necessary when we have group by row
		// hierarchy structure.
		// 
		internal int VerifyVersion
		{
			get
			{
				return this.TopLevelRowsCollection.verifyVersion;
			}
		}

		#endregion // VerifyVersion

		#region HasColumnFilters
		
		internal bool HasColumnFilters
		{
			get
			{
				// SSP 9/2/03 UWG2330
				// If we have group-by rows, then resolve the column filters to the column
				// filters of the top-level rows collection.
				//
				// --------------------------------------------------------------------------
				//return null != this.columnFilters && this.columnFilters.Count > 0;
				RowsCollection topLevelRows = this.TopLevelRowsCollection;
				return null != topLevelRows.columnFilters && topLevelRows.columnFilters.Count > 0;
				// --------------------------------------------------------------------------
			}
		}

		#endregion // HasColumnFilters

		#region AreAnyFiltersActive
		
		internal bool AreAnyFiltersActive
		{
			get
			{
				// SSP 8/1/03 UWG1654 - Filter Action
				// Added a mechanism to cache AreAnyFiltersActive.
				//
				// ------------------------------------------------------------------------------
				
				this.VerifyFilterInfo( );
				return this.cachedAreAnyFiltersActive;
				// ------------------------------------------------------------------------------
			}
		}

		#endregion // AreAnyFiltersActive

		#region RowFiltersVersion
		
		internal int RowFiltersVersion
		{
			get
			{
				// SSP 9/2/03 UWG2330
				// Since we are resolving the column filters to the top most rows collection's
				// column filters, return the RowFiltersVersion off the top most rows 
				// collection.
				//
				// --------------------------------------------------------------------------------
				if ( ! this.IsTopLevel )
					return this.TopLevelRowsCollection.RowFiltersVersion;
				// --------------------------------------------------------------------------------

				// SSP 7/31/02
				// Add the grand summaries version number off the band
				//
				//return this.rowFiltersVersion;

				int version = this.rowFiltersVersion;

				if ( null != this.band )
				{
					// SSP 5/13/04 UWG3260 - Virtual Binding
					// Combined the verified band and rows version numbers into one.
					// Added following line of code that adds the band's RowFiltersVersion.
					// 
					//version += this.band.Layout.RowFiltersVersion;
					version += band.RowFiltersVersion;
				}

				return version;
			}
		}

		#endregion // RowFiltersVersion

		#region IsRowsDirty
		
		internal bool IsRowsDirty
		{
			get
			{
				return this.rowsDirty;
			}
		}

		#endregion // IsRowsDirty

		#region ListChangedHandler
		
		private System.ComponentModel.ListChangedEventHandler ListChangedHandler
		{
			get
			{
				if ( null == this.listChangedHandler )
					this.listChangedHandler = new System.ComponentModel.ListChangedEventHandler( this.OnListChanged );

				return this.listChangedHandler;
			}
		}

		#endregion // ListChangedHandler			

		#region UnSortedActualRows
		
		internal RowsCollection.RowsSparseArray UnSortedActualRows
		{
			get
			{
				// We only have unsorted rows array list for top level rows collection.
				// (Meaning, in group by rows situation, there are multiple levels of
				// rows collection for the same band and same parent row in the parent band).
				// The top level is the one that is immediatedly below a row in parent band.
				//
				if ( this.ParentRow is UltraGridGroupByRow )
				{
					UltraGridGroupByRow parentGroupByRow = (UltraGridGroupByRow)this.ParentRow;

					return parentGroupByRow.ParentCollection.UnSortedActualRows;
				}

				if ( null == this.unsortedActualRows )
					this.unsortedActualRows = new RowsCollection.RowsSparseArray( this );

				return this.unsortedActualRows;
			}
		}

		#endregion // UnSortedActualRows

		#region GroupByColumn
		
		internal UltraGridColumn GroupByColumn
		{
			get
			{	
				//return this.groupByColumn;

				if ( null == this.ParentRow )
				{
					if ( this.band.HasGroupBySortColumns )
						return this.band.SortedColumns[0];
				}
				else if ( this.ParentRow is UltraGridGroupByRow )
				{					
					UltraGridGroupByRow parentGroupByRow = (UltraGridGroupByRow)this.ParentRow;

					int index = parentGroupByRow.Band.SortedColumns.IndexOf( parentGroupByRow.Column );

					if ( index >= 0 )
					{	
						UltraGridColumn column = null;
						if ( 1 + index < parentGroupByRow.Band.SortedColumns.Count )
							column = parentGroupByRow.Band.SortedColumns[ 1 + index ];

						if ( null != column && column.IsGroupByColumn )
							return column;
					}
				}
				else if ( this.IsTopLevel )
				{
					if ( this.band.HasGroupBySortColumns )
						return this.band.SortedColumns[0];
				}
				
				return null;
			}
		}

		#endregion // GroupByColumn

		#region ParentChildBand
		
		private UltraGridChildBand ParentChildBand
		{
			get
			{
				if ( null == this.ParentRow )
					return null;

				return this.ParentRow.ChildBands[ this.band ];
			}
		}

		#endregion // ParentChildBand

		#region TopLevelRowsCollection
		
		internal RowsCollection TopLevelRowsCollection
		{
			get
			{
				if ( this.IsTopLevel )
					return this;

				// SSP 9/26/01 UWG337
				// recursive function, should call itself on parent collection
				// and not just return ParentCollection
				//
				//return this.ParentRow.ParentCollection;
				return this.ParentRow.ParentCollection.TopLevelRowsCollection;
			}
		}

		#endregion // TopLevelRowsCollection

		#region IsTopLevel

		internal bool IsTopLevel
		{
			get
			{
				// SSP 4/17/03 Optimizations
				//
				//return null == this.ParentRow || !( this.ParentRow is UltraGridGroupByRow );
				return !( this.parentRow is UltraGridGroupByRow );
			}
		}

		#endregion // IsTopLevel

		#region InSyncRows

		// SSP 11/11/05 BR07435
		// Added InSyncRows helper property.
		// 
		internal bool InSyncRows
		{
			get
			{
				return this.TopLevelRowsCollection.inSyncRows;
			}
		}

		#endregion // InSyncRows

		#region IsRootRowsCollection
		
		// SSP 9/8/04
		// Added IsRootRowsCollection property.
		//
		internal bool IsRootRowsCollection
		{
			get
			{
				return null == this.parentRow;
			}
		}

		#endregion // IsRootRowsCollection

		#region IsGroupByRows

		/// <summary>
		/// Indicates if this rows collection contains or is to contain group-by rows.
		/// </summary>
        // MRS v7.2 - Document Exporter
		//internal bool IsGroupByRows
        public bool IsGroupByRows
		{
			get
			{
				return null != this.GroupByColumn;
			}
		}

		#endregion // IsGroupByRows

		#region IsCardAreaHorizontalScrollbarVisible

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		//
		internal bool IsCardAreaHorizontalScrollbarVisible
		{
			get
			{
				return CardScrollbars.Horizontal == this.Band.CardScrollbarsResolved;
			}
		}

		#endregion // IsCardAreaHorizontalScrollbarVisible

		#region CardAreaHorizontalScrollbarHeightResolved

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		//
		internal int CardAreaHorizontalScrollbarHeightResolved
		{
			get
			{
				return this.Band.CardAreaHorizontalScrollbarHeightResolved;
			}
		}

		#endregion // CardAreaHorizontalScrollbarHeightResolved

		#region CardAreaHeightInCards
		
		internal int CardAreaHeightInCards
		{
			get
			{
				return this.cardAreaHeightInCards;
			}
		}

		#endregion // CardAreaHeightInCards

		#region InternalFirstVisibleCard

		// SSP 7/15/03 UWG2240
		// Added InternalFirstVisibleCard property.
		//
		internal UltraGridRow InternalFirstVisibleCard
		{
			get
			{
				return this.firstVisibleCard;
			}
		}

		#endregion // InternalFirstVisibleCard

		#region FirstVisibleCard
		
		internal UltraGridRow FirstVisibleCard
		{
			get
			{
				// JJD 12/11/01
				// If we have cached the first card make sure that
				// it is still in the collection
				//
				if ( this.firstVisibleCard != null )
				{
					// SSP 11/14/03 Add Row Feature
					// Template add-rows have negative indeces.
					//
					// ------------------------------------------------------------------
					
					if ( ! this.ValidateCard( this.firstVisibleCard ) )
						this.firstVisibleCard = null;
					// ------------------------------------------------------------------
				}

				// JJD 12/11/01
				// If the first card is hidden then we need to get
				// the next or previous visible row.
				//
				if ( this.firstVisibleCard != null )
				{
					if ( this.firstVisibleCard.HiddenResolved )
					{
						UltraGridRow row = this.firstVisibleCard.GetNextVisibleRelative( false, true, false );

						if ( row == null )
							row = this.firstVisibleCard.GetPrevVisibleRelative( false, true, false );					

						this.firstVisibleCard = row;
					}					
				}
				else
				{
					// JJD 12/11/01
					// Cache the first card
					//
					this.firstVisibleCard = this.GetFirstVisibleRow();
				}

				return this.firstVisibleCard;
			}

			set
			{
				UltraGridRow row = value as UltraGridRow;

				if (row	!= null)
				{
					// SSP 11/14/03 Add Row Feature
					// Template add-rows have negative indeces.
					//
					// ------------------------------------------------------------------
					
					if ( ! this.ValidateCard( row ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_245"));
					// ------------------------------------------------------------------
				}

				this.firstVisibleCard = row;
			}
		}

		#endregion // FirstVisibleCard

		#region ActualRowsCount_NoSync
		
		internal int ActualRowsCount_NoSync
		{
			get
			{
				return this.UnSortedActualRows.Count;
			}
		}

		#endregion // ActualRowsCount_NoSync

		#region ActualRowsCount
		
		internal int ActualRowsCount
		{
			get
			{
				if ( this.rowsDirty )
					this.SyncRows( );

				return this.UnSortedActualRows.Count;
			}
		}

		#endregion // ActualRowsCount
		
		#region VirtualModeResolved
		
		// SSP 4/27/04 - Virtual Binding
		//
		internal bool VirtualModeResolved
		{
			get
			{
				return null != this.Layout && LoadStyle.LoadOnDemand == this.Layout.LoadStyle;
			}
		}

		#endregion // VirtualModeResolved

		#region IsCardRows
		
		// SSP 4/27/04 - Virtual Binding
		//
		internal bool IsCardRows
		{
			get
			{
				// SSP 5/10/04 - Optimizations
				//
				//return ! this.IsGroupByRows && this.Band.CardView;
				return this.Band.CardView && ! this.IsGroupByRows;
			}
		}

		#endregion // IsCardRows

		#region ScrollCount
		
		// SSP 4/27/04 - Virtual Binding
		//
		internal int ScrollCount
		{
			get
			{
				// SSP 10/20/05 BR06933
				// Moved this below. Only return 1 if the row collection has some rows. 
				// If the rows collection is empty then return 0.
				// 
				

				int scrollCount = this.SparseArray.ScrollCount;

				// SSP 4/14/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
				// We have to take into account filter row, summary row etc...
				// Commented out the original code and added the new code below.
				//
				//if ( this.IsTemplateAddRowVisible )
				//	scrollCount++;
				scrollCount += this.GetSpecialRowsScrollCount( true ) 
							+ this.GetSpecialRowsScrollCount( false );

				// SSP 10/20/05 BR06933
				// Moved this here from above. Only return 1 if the row collection has some 
				// rows. If the rows collection is empty then return 0.
				// 
				// In card-view, the whole card island is treated as being associated with a single 
				// scroll position.
				//
				if ( scrollCount > 0 && this.IsCardRows )
					return 1;

				return scrollCount;
			}
		}

		#endregion // ScrollCount

		// SSP 11/7/03 - Add Row Feature
		//
		#region Add Row Feature - Properties

		// JAS 2005 v2 XSD Support
		//
		#region ContainsLessThanMaxRows

		internal bool ContainsLessThanMaxRows
		{
			get
			{
				
				
				
				
				UltraGridBand band = this.Band;
				if ( null != band )
				{
					int maxRows = band.MaxRows;
					if ( int.MaxValue != maxRows && this.Count >= maxRows )
						return false;
				}

				return true;
				
			}
		}

		#endregion // ContainsLessThanMaxRows

		#region HasTemplateAddRow
		
		internal bool HasTemplateAddRow
		{
			get
			{
				return null != this.templateAddRow;
			}
		}

		#endregion // HasTemplateAddRow

		#region AddRowDefaultCellValues
		
		private Hashtable AddRowDefaultCellValues
		{
			get
			{
				if ( null == this.addRowDefaultCellValues )
					this.addRowDefaultCellValues = new Hashtable( null != this.Band && this.Band.Columns.Count > 0 ? this.Band.Columns.Count : 10 );

				return this.addRowDefaultCellValues;
			}
		}

		#endregion // AddRowDefaultCellValues

		#region TemplateRowLocationDefault
		
		internal TemplateAddRowLocation TemplateRowLocationDefault
		{
			get
			{
				// We don't support adding rows when rows are grouped-by.
				//
				if ( ! this.Band.HasGroupBySortColumns )
				{
					AllowAddNew allowAddNew = this.Band.AllowAddNewResolved;

					// SSP 4/13/05 - NAS 5.2 Fixed Add Row
					//
					//if ( AllowAddNew.TemplateOnBottom == allowAddNew )
					if ( AllowAddNew.TemplateOnBottom == allowAddNew || AllowAddNew.FixedAddRowOnBottom == allowAddNew )
					{
						if ( this.Band.Layout.IsDisplayLayout )
							return TemplateAddRowLocation.Bottom;
					}
						// SSP 4/13/05 - NAS 5.2 Fixed Add Row
						//
						//else if ( AllowAddNew.TemplateOnTop == allowAddNew || AllowAddNew.TemplateOnTopWithTabRepeat == allowAddNew )
					else if ( AllowAddNew.TemplateOnTop == allowAddNew || AllowAddNew.TemplateOnTopWithTabRepeat == allowAddNew 
						|| AllowAddNew.FixedAddRowOnTop == allowAddNew )
					{
						if ( this.Band.Layout.IsDisplayLayout )
							return TemplateAddRowLocation.Top;
					}
				}

				return TemplateAddRowLocation.None;
			}
		}

		// SSP 3/31/06 BR11064
		// Added IsAllowAddNewFixed method.
		// 
		internal static bool IsAllowAddNewFixed( AllowAddNew allowAddNew )
		{
			return AllowAddNew.FixedAddRowOnTop == allowAddNew 
				|| AllowAddNew.FixedAddRowOnBottom == allowAddNew;
		}

		#endregion // TemplateRowLocationDefault

		#region TemplateRowLocationResolved

		internal TemplateAddRowLocation TemplateRowLocationResolved
		{
			get
			{
				TemplateAddRowLocation loc = this.TemplateRowLocationDefault;

				// SSP 4/13/05
				// Simple optimization. Moved this below after the following if block. Only bother 
				// checking for max rows if loc is not None to begin with.
				//
				

				if ( TemplateAddRowLocation.None != loc )
				{
					if ( null != this.currentAddRow 
						// SSP 5/1/05 - NAS 5.2 Fixed Add Row
						// 
						//&& ! this.currentAddRowModifiedByUserFlag 
						&& ( ! this.currentAddRowModifiedByUserFlag || FixedRowStyle.Default != this.currentAddRow.fixedResolvedLocationOverride 
							// SSP 3/31/06 BR11064
							// For child bands as well show the template add-row when Enter key is pressed when AllowAddNew is set
							// to FixedAddRowOnTop or FixedAddRowOnBottom.
							// 
							|| RowsCollection.IsAllowAddNewFixed( this.Band.AllowAddNewResolved )
						   )
						// SSP 12/12/03 UWG2794
						// Don't cause premature creation of a template add-row and as a result firing
						// of InitializeTemplateAddRow prematurely.
						//
						//|| this.TemplateAddRow.Hidden 
						|| null != this.templateAddRow && this.templateAddRow.Hidden
						)
					{
						loc = TemplateAddRowLocation.None;
					}
					else
					{
						UltraGridRow parentRow = this.TopLevelRowsCollection.ParentRow;

						if ( null != parentRow && ( parentRow.IsTemplateAddRow || parentRow.IsUnModifiedAddRowFromTemplate ) )
							loc = TemplateAddRowLocation.None;
					}
				}

				// SSP 4/13/05
				// Simple optimization. Moved this here from above. Only bother checking
				// for max rows if loc is not None to begin with.
				//
				// JAS 2005 v2 XSD Support
				// If the band already contains the maximum number of allowed rows (or more)
				// then we must disallow the template row from being visible.
				//
				if( TemplateAddRowLocation.None != loc && ! this.ContainsLessThanMaxRows )
					return TemplateAddRowLocation.None;

				return loc;
			}
		}

		#endregion // TemplateRowLocationResolved

		#region IsTemplateAddRowVisible
		
		internal bool IsTemplateAddRowVisible
		{
			get
			{
				return TemplateAddRowLocation.None != this.TemplateRowLocationResolved;
			}
		}

		#endregion // IsTemplateAddRowVisible

		#region CurrentAddRow

		internal UltraGridRow CurrentAddRow
		{
			get
			{
				return this.currentAddRow;
			}
			set
			{
				if ( this.currentAddRow != value )
				{
					bool origIsTemplateAddRowVisible = this.IsTemplateAddRowVisible;

					// SSP 5/2/05 - NAS 5.2 Fixed Rows/Fixed Add Row
					// When currentAddRow is committed it's visibility as far as UltraGridRow.ScrollCountInternal
					// is concerned may change. Look in there for more info.
					// 
					UltraGridRow origCurrentAddRow = this.currentAddRow;
					bool wasCurrentRowFixed = null != origCurrentAddRow && origCurrentAddRow.FixedResolved;
					if ( wasCurrentRowFixed )
					{
						// SSP 8/30/06 BR15101 BR15110
						// We need to set the currentAddRow before calling NotifyScrollCountChanged because 
						// merged cell logic will cause syncrhonous verification of special fixed rows and
						// that logic will still think that the current add row is what the currentAddRow
						// member var holds.
						// 
						this.currentAddRow = value;

						// Reset the fixed resolved override setting on the old current add-row.
						//
						origCurrentAddRow.InternalSetFixedResolvedOverride( FixedRowStyle.Default );
						origCurrentAddRow.NotifyScrollCountChanged( );

						// SSP 10/1/08 BR32168
						// We need to notify the calc manager since previously it would have skipped 
						// calculations on this row (since it would have been considered hidden due 
						// to row.ScrollCountInternal implementation). And therefore we need to 
						// nofity the calc manager now so the row gets calculated.
						// 
						this.NotifyCalcManager_HiddenStateChanged( origCurrentAddRow );						
					}

					this.currentAddRow = value;

					// Reset the flag.
					//
					this.currentAddRowModifiedByUserFlag = false;

					// SSP 5/2/05 - NAS 5.2 Fixed Rows/Fixed Add Row
					// Set the isAddRow flag on the row. This does eventually get set however we
					// want to do it as soon as possible otherwise OnTemplateAddRowVisibleStateChanged 
					// call below may lead to UltraGridRow.ApplyFilters to get called which will
					// filter it out unless isAddRow flag is set.
					//
					if ( null != this.currentAddRow )
						this.currentAddRow.InternalSetIsAddRow( true );

					// SSP 5/2/05 - NAS 5.2 Fixed Rows/Fixed Add Row
					// When currentAddRow is committed it's visibility as far as UltraGridRow.ScrollCountInternal
					// is concerned may change. Look in there for more info.
					//
					if ( wasCurrentRowFixed )
						this.OnTemplateAddRowVisibleStateChanged( origCurrentAddRow );

					// Since setting this flag could potentially end up unhinding or hiding
					// the template add-row, dirty the scrollable row count and invalidate
					// the grid.
					//
					if ( this.IsTemplateAddRowVisible != origIsTemplateAddRowVisible )
						this.OnTemplateAddRowVisibleStateChanged( );
				}
			}
		}

		#endregion // CurrentAddRow

		#region CurrentAddRowModifiedByUser

		internal bool CurrentAddRowModifiedByUser
		{
			get
			{
				return this.currentAddRowModifiedByUserFlag;
			}
			set
			{
				if ( this.currentAddRowModifiedByUserFlag != value )
				{
					bool origIsTemplateAddRowVisible = this.IsTemplateAddRowVisible;

					this.currentAddRowModifiedByUserFlag = value;

					// Since setting this flag could potentially end up unhinding or hiding
					// the template add-row, dirty the scrollable row count and invalidate
					// the grid.
					//
					if ( this.IsTemplateAddRowVisible != origIsTemplateAddRowVisible )
						this.OnTemplateAddRowVisibleStateChanged( );				
				}
			}
		}

		#endregion // CurrentAddRowModifiedByUser

		#endregion // Add Row Feature - Properties

		#region CalcReference

		// SSP 7/27/04 - UltraCalc
		//
		internal RowsCollectionReference CalcReference
		{
			get
			{
				if ( null == this.calcReference && null != this.Layout )
					this.calcReference = new RowsCollectionReference( this );

				return this.calcReference;
			}
		}

		#endregion // CalcReference

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return null != this.calcReference;
			}
		}

		#endregion // HasCalcReference

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region NestedColumnReferences

		//#if DEBUG
		//        /// <summary>
		//        /// Returns a map of nested column references with columns as the key and values as the NestedColumnReference instances.
		//        /// </summary>
		//#endif
		//        internal Hashtable NestedColumnReferences
		//        {
		//            get
		//            {
		//                throw new Exception( );
		//            }
		//        }

		//        #endregion // NestedColumnReferences

		#endregion Not Used

		#region FirstRow
		
		internal UltraGridRow FirstRow
		{
			get
			{
				return this.Count > 0 ? this[0] : null;
			}
		}

		#endregion // FirstRow

		// SSP 8/18/04
		// Added AreAllAncestorsExpanded method.
		//
		#region AreAllAncestorRowsExpanded
		
		internal bool AreAllAncestorRowsExpanded
		{
			get
			{
				UltraGridRow parentRow = this.ParentRow;
				while ( null != parentRow )
				{
					if ( ! parentRow.Expanded )
						return false;

					parentRow = parentRow.ParentRow;
				}

				return true;
			}
		}

		#endregion // AreAllAncestorRowsExpanded

		#region IsPotentiallyVisibleOnScreen
		
		internal bool IsPotentiallyVisibleOnScreen
		{
			get
			{
				return
					( null == this.ParentRow || this.ParentRow.IsPotentiallyVisibleOnScreen )
					&& this.AreAllAncestorRowsExpanded;
			}
		}

		#endregion // IsPotentiallyVisibleOnScreen

		#region IsStillValid
        
		// SSP 9/1/04
		// Added IsStillValid property.
		//
		internal bool IsStillValid
		{
			get
			{
				return ! this.Disposed && ( null == this.ParentRow || this.ParentRow.IsStillValid )
					&& null != this.Band && this.Band.IsStillValid;
			}
		}

		#endregion // IsStillValid

		#region CalcManger

		private Infragistics.Win.CalcEngine.IUltraCalcManager CalcManger
		{
			get
			{
				UltraGridLayout layout = this.Layout;
				return null != layout ? layout.CalcManager : null;
			}
		}

		#endregion // CalcManger

		// SSP 11/3/04 - Merged Cell Feature
		//
		#region MergedCellCache

		internal MergedCellCache MergedCellCache
		{
			get
			{
				this.VerifyMergedCellVersion( );

				if ( null == this.mergedCellCache )
					this.mergedCellCache = new MergedCellCache( this );

				return this.mergedCellCache;
			}
		}

		#endregion // MergedCellCache

		#region HasMergedCellCache 
		
		internal bool HasMergedCellCache 
		{
			get
			{
				return null != this.mergedCellCache;
			}
		}

		#endregion // HasMergedCellCache 

		#endregion // Private/Internal Properties

		#region Fixed Rows/Filter Row/Fixed Add-Row/Extension of Summaries Functionality

		#region FireInitializeRowsCollection

		internal void FireInitializeRowsCollection( )
		{
			UltraGridBase grid = null != this.band && null != this.band.Layout ? this.band.Layout.Grid : null;
			if ( null != grid )
			{
				InitializeRowsCollectionEventArgs e = new InitializeRowsCollectionEventArgs( this );
				grid.FireCommonEvent( CommonEventIds.InitializeRowsCollection, e );
			}
		}

		#endregion // FireInitializeRowsCollection

		// SSP 3/22/05 - NAS 5.2 Fixed Rows
		//
		#region FixedRows

		/// <summary>
		/// A collection of fixed rows. Rows can be added or removed from this collection to fix/unfix rows.
		/// </summary>
		/// <remarks>
		/// <p class="body">Adding a row to this collection will make the row fixed. You can also fix
		/// a row by setting its <see cref="UltraGridRow.Fixed"/> property. Note that presence
		/// of a row in the FixedRows collection does not necessarily make the row fixed. There
		/// are situations where fixed rows are not supported (in card-view, in child row collections,
		/// in horizontal view style). In those situations you can still add the row to the 
		/// FixedRows collection however the row will not be actually fixed. It will however move
		/// the row to either top or bottom of the parent collection (depending on the 
		/// <see cref="UltraGridOverride.FixedRowStyle"/> property settings).</p>
		/// <p class="body"><b>Note</b> that fixed rows are not supported in child row collections. Adding
		/// rows to the FixedRows collection of a child row collection will however move the added
		/// rows to the top or the bottom of the row collection. It will also apply the fixed row
		/// related appearances to the rows in this collection. Note that fixed rows are not supported 
		/// in card-view.</p>
		/// <p class="body">Also note that when a fixed row is expanded, any sibling fixed rows occuring
		/// after it will not be fixed. They will remain non-fixed until the expanded fixed row is collapsed. 
		/// They will however remain in the FixedRows collection and keep their fixed row appearances.</p>
		/// <seealso cref="UltraGridRow.Fixed"/> 
		/// </remarks>
		public FixedRowsCollection FixedRows
		{
			get
			{
				if ( null == this.fixedRowCollection )
				{
					this.fixedRowCollection = new FixedRowsCollection( this );
					this.fixedRowCollection.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.fixedRowCollection;
			}
		}

		#endregion // FixedRows

		#region HasFixedRowsBeenAllocated

		internal bool HasFixedRowsBeenAllocated
		{
			get
			{
				return null != this.fixedRowCollection;
			}
		}

		#endregion // HasFixedRowsBeenAllocated

		#region HasFixedDataRowsVisibleOnTop

		internal bool HasFixedDataRowsVisibleOnTop
		{
			get
			{
				return null != this.fixedRowCollection 
					&& this.fixedRowCollection.TopFixed
					&& this.fixedRowCollection.HasFixedResolvedRows;
			}
		}

		#endregion // HasFixedDataRowsVisibleOnTop

		#region HasFixedDataRowsVisibleOnBottom

		internal bool HasFixedDataRowsVisibleOnBottom
		{
			get
			{
				return null != this.fixedRowCollection 
					&& this.fixedRowCollection.BottomFixed
					&& this.fixedRowCollection.HasFixedResolvedRows;
			}
		}

		#endregion // HasFixedDataRowsVisibleOnBottom

		#region Move
		
		/// <summary>
		/// Moves the specified row in the row collection to the new index. The end result
		/// is that the row will be at the specified newIndex.
		/// </summary>
		/// <param name="row">The row must exist in the collection.</param>
		/// <param name="newIndex">The new position of the row.</param>
		public void Move( UltraGridRow row, int newIndex )
		{
			this.Move( row, newIndex, false );
		}

		/// <summary>
		/// Moves the specified row in the row collection to the new index. The end result
		/// is that the row will be at the specified newIndex.
		/// </summary>
		/// <param name="row">The row must exist in the collection.</param>
		/// <param name="newIndex">The new position of the row.</param>
		/// <param name="forceDirtyPrimaryMetrics">true to force the dirtying of the layout's metrics.</param>
		public void Move( UltraGridRow row, int newIndex, bool forceDirtyPrimaryMetrics )
		{
			Infragistics.Shared.SparseArray sarr = this.SparseArray;
			int index = sarr.IndexOf( row );
			Debug.Assert( index >= 0 );
			if ( index >= 0 && index != newIndex )
			{
				sarr.RemoveAt( index );
				sarr.Insert( newIndex, row );
			}
			else if ( forceDirtyPrimaryMetrics )
			{
				// Cause the RowScrollRegion.GetMaxScrollPosition to get recalculated.
				//
				this.Layout.BumpGrandVerifyVersion( );
				this.Layout.DirtyDataAreaElement( true, true );
			}
		}

		#endregion // Move

		#region MoveFixedRowsToProperLocation
		
		internal void MoveFixedRowsToProperLocation( )
		{
			FixedRowsCollection fixedRows = this.FixedRows;
			if ( fixedRows.Count > 0 )
			{
				Infragistics.Shared.SparseArray sarr = this.SparseArray;
				bool topFixed = fixedRows.TopFixed;

				for ( int i = 0; i < fixedRows.Count; i++ )
					this.Move( fixedRows[i], topFixed ? i : sarr.Count - 1, true );
			}
		}

		#endregion // MoveFixedRowsToProperLocation

		#region MoveRowToCorrectSortOrder

		internal void MoveRowToCorrectSortOrder( UltraGridRow row )
		{
			ScrollCountManagerSparseArray sarr = this.SparseArray;            
			int currIndex = sarr.IndexOf( row );
			//Debug.Assert( currIndex >= 0 );
			if ( currIndex < 0 )
				return;

			IComparer comparer = new RowsSortComparer( this );
			FixedRowsCollection fixedRows = this.FixedRows;
			bool rowFixed = row.Fixed;
			int i, j;

			if ( rowFixed )
			{
				// If row is fixed then position it withing fixed rows.
				//
				if ( fixedRows.TopFixed )
				{
					i = 0;
					j = fixedRows.Count - 1;
				}
				else
				{
					i = sarr.Count - fixedRows.Count;
					j = sarr.Count - 1;
				}
			}
			else
			{
				// If row is not fixed then position it withing un-fixed rows.
				//
				if ( fixedRows.TopFixed )
				{
					i = fixedRows.Count;
					j = sarr.Count - 1;
				}
				else
				{
					i = 0;
					j = sarr.Count - 1 - fixedRows.Count;
				}
			}
            
			while ( i <= j )
			{
				int m = ( i + j ) / 2;
				UltraGridRow tmpRow = sarr.GetItem( m, true );
				if ( row == tmpRow )
				{
					if ( i != j )
						tmpRow = sarr.GetItem( ++m, true );
					else
						break;
				}

				int r = comparer.Compare( row, tmpRow );

				if ( r < 0 )
					j = m - 1;
				else if ( r > 0 )
					i = m + 1;
					// If the row is where it should be then don't move it. That's what the
					// the below else if does.
					//
				else if ( currIndex < m )
					j = m - 1;
				else 
					i = m + 1;
			}
			
			if ( currIndex != i )
			{
				int newIndex = currIndex < i ? i - 1 : i;
				this.Move( row, newIndex, true );

				// Also make sure that the rows in the fixed row collection are in the same
				// order as in the main row collection.
				//
				if ( rowFixed )
					fixedRows.Move( row, newIndex );

				// If the moved row were the first row of the row scroll region mainain the
				// scroll position.
				//
				this.MaintainScrollPositionHelper( );
			}
		}

		#endregion // MoveRowToCorrectSortOrder

		#region FilterRow 

		/// <summary>
		/// Returns the filter row associated with this row collection.
		/// </summary>
		public UltraGridFilterRow FilterRow 
		{
			get
			{
				Debug.Assert( this.IsTopLevel );
				if ( null == this.filterRow )
					this.filterRow = new UltraGridFilterRow( this );

				return this.filterRow;
			}
		}

		#endregion // FilterRow 

		#region HasFilterRow

		internal bool HasFilterRow
		{
			get
			{
				return null != this.filterRow;
			}
		}

		#endregion // HasFilterRow

		#region IsFilterRowVisibleHelper
		
		private bool IsFilterRowVisibleHelper
		{
			get
			{
				return FilterUIType.FilterRow == this.Band.FilterUITypeResolved
					// SSP 3/31/06 BR11014
					// Hidden property of the filter row returns true if all of the columns disallow filtering.
					// Use the GetHiddenHelper method.
					// 
					//&& ( null == this.filterRow || ! this.filterRow.Hidden )
					&& ! UltraGridFilterRow.GetHiddenHelper( this.filterRow, this.Band ) 
					// In case of group-by rows, display the filter row with the
					// top level group-by rows only. Don't display the filter row
					// at every island.
					//
					&& this.IsTopLevel
					// Card-view doesn't support filter row yet.
					//
					&& ! this.IsCardRows
					// Filter row should not be displayed in print and export layouts.
					//
					&& this.Layout.IsDisplayLayout
					// SSP 8/4/05 BR05321
					// This is to prevent the filter row's row selector from being displayed
					// even when the UltraGrid is not bound to any data.
					// 
					&& ( null != this.Band.BindingManager || this.Band.Columns.Count > 0 );
			}
		}

		#endregion // IsFilterRowVisibleHelper

		#region GetSpecialRows

		private UltraGridRow[] GetSpecialRows( bool top )
		{
			this.EnsureSpecialAndFixedRowsCacheCalculated( );

			return top ? this.cachedSpecialRowsTop : this.cachedSpecialRowsBottom;
		}

		#endregion // GetSpecialRows

		#region EnsureSpecialAndFixedRowsCacheCalculated

		#region GetSpecialRowsHelper

		// SSP 6/23/05 BR04635 BR04640
		// 
		//private void GetSpecialRowsHelper( Type rowType, ArrayList list, bool top )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void GetSpecialRowsHelper( Type rowType, ArrayList list, bool top, UltraGridRow[] rowsToRecycle )
		private void GetSpecialRowsHelper( Type rowType, List<UltraGridRow> list, bool top, UltraGridRow[] rowsToRecycle )
		{
			// Since there is no special row type for template add-rows use the UltraGridRow.
			//
			if ( typeof( UltraGridRow ) == rowType )
			{
				TemplateAddRowLocation loc = top ? TemplateAddRowLocation.Top : TemplateAddRowLocation.Bottom;
				UltraGridRow addRow = null;
				if ( loc == this.TemplateRowLocationResolved )
					addRow = this.TemplateAddRow;
				else if ( null != this.currentAddRow && loc == this.TemplateRowLocationDefault )
					addRow = this.currentAddRow;

				if ( null != addRow )
					list.Add( addRow );
			}
			
			if ( top && typeof( UltraGridFilterRow ) == rowType )
			{
				// Filter row is always at the top.
				//
				if ( this.IsFilterRowVisibleHelper )
					list.Add( this.FilterRow );
			}

			if ( typeof( UltraGridSummaryRow ) == rowType )
			{
				if ( this.Band.HasSummaries )
					// SSP 6/23/05 BR04635 BR04640
					// 
					//this.Band.Summaries.GetSummaryRows( list, top, this );
					this.Band.Summaries.GetSummaryRows( list, top, this, rowsToRecycle );
			}
		}

		// SSP 6/23/05 BR04635 BR04640
		// 
		//private void GetSpecialRowsHelper( ArrayList list, bool top )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void GetSpecialRowsHelper( ArrayList list, bool top, UltraGridRow[] rowsToRecycle )
		private void GetSpecialRowsHelper( List<UltraGridRow> list, bool top, UltraGridRow[] rowsToRecycle )
		{
			UltraGridBand band = this.Band;
			UltraGridLayout layout = band.Layout;

			// SSP 8/3/06 BR14784
			// 
			bool allRowsFilteredOut_NeedsHeaderFlag = false;

			// If the row collection doesn't have any rows or all rows are hidden then don't
			// add the special rows. This is because if the 
			bool addSpecialRows = this.IsRootRowsCollection || this.SparseArray.VisibleCount > 0
				|| this.IsTemplateAddRowVisible;
			if ( !addSpecialRows && this.SparseArray.Count > 0 )
			{
				if ( layout.ViewStyleImpl.ShouldApplySpecialRowFiltersCode( )
					&& band.IsFilterUIEnabled
					&& RowFilterAction.HideFilteredOutRows == band.RowFilterActionResolved )
				{
					addSpecialRows = true;

                    // SSP 8/3/06 BR14784
                    // 
                    // MRS 7/17/2008 - BR34178
                    //allRowsFilteredOut_NeedsHeaderFlag = top;
                    // We need to do this when using filter icons, but not when there is a 
                    // filter row. 
                    if (band.FilterUITypeResolved != FilterUIType.FilterRow)
                        allRowsFilteredOut_NeedsHeaderFlag = top;
				}
			}

			if ( !addSpecialRows )
				return;

			long[] sequence = new long[] 
				{
					band.GetSequenceSummaryRowResolved( top ),
					band.GetSequenceFilterRowResolved( top ),
					band.GetSequenceFixedAddRowResolved( top ) 
				};

			Type[] types = new Type[]
				{
					typeof( UltraGridSummaryRow ),
					typeof( UltraGridFilterRow ),
					typeof( UltraGridRow )
				};

			// Sort by the sequence numbers so the rows are displayed in that order.
			//
			for ( int i = 0; i < sequence.Length; i++ )
			{
				for ( int j = 1 + i; j < sequence.Length; j++ )
				{
					if ( sequence[i] > sequence[j] )
					{
						GridUtils.Swap( sequence, i, j );
						GridUtils.Swap( types, i, j );
					}
				}
			}

			for ( int i = 0; i < types.Length; i++ )
				// SSP 6/23/05 BR04635 BR04640
				// 
				//this.GetSpecialRowsHelper( types[ i ], list, top );
				this.GetSpecialRowsHelper( types[i], list, top, rowsToRecycle );


			// SSP 9/22/06 BR14784
			// 
			if ( allRowsFilteredOut_NeedsHeaderFlag && layout.ViewStyleImpl.Using_CreateRowsList_FixedRowsFeature )
			{
				bool addHeader = false;
				HeaderPlacement headerPlacement = band.HeaderPlacementResolved;
				switch ( headerPlacement )
				{
					case HeaderPlacement.OncePerRowIsland:
					case HeaderPlacement.RepeatOnBreak:
						addHeader = true;
						break;
					case HeaderPlacement.OncePerGroupedRowIsland:
						if ( this.IsTopLevel )
							addHeader = true;
						break;
				}

				if ( addHeader )
					list.Add( this.CreateHeadersRow( rowsToRecycle ) );
			}
		}

		// SSP 9/22/06 BR14784
		// 
		private HeadersRow CreateHeadersRow( UltraGridRow[] rowsToRecycle )
		{
			if ( null != rowsToRecycle )
			{
				for ( int i = 0; i < rowsToRecycle.Length; i++ )
				{
					HeadersRow row = rowsToRecycle[i] as HeadersRow;
					if ( null != row )
					{
						rowsToRecycle[i] = null;
						return row;
					}
				}
			}

			return new HeadersRow( this, false );
		}

		#endregion // GetSpecialRowsHelper

		#region ToRowArrayHelper

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UltraGridRow[] ToRowArrayHelper( ArrayList list )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private static UltraGridRow[] ToRowArrayHelper( ArrayList list )
		private static UltraGridRow[] ToRowArrayHelper( List<UltraGridRow> list )
		{
			return null != list && list.Count > 0 
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//? (UltraGridRow[])list.ToArray( typeof( UltraGridRow ) ) : null;
				? list.ToArray() 
				: null;
		}

		#endregion // ToRowArrayHelper

		#region CalcSpecialAndFixedRowsHelper

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void AddFixedRows( ArrayList list, bool top )
		private void AddFixedRows( List<UltraGridRow> list, bool top )
		{
			if ( this.HasFixedRowsBeenAllocated && top == this.FixedRows.TopFixed )
			{
				foreach ( UltraGridRow row in this.FixedRows )
				{
					if ( row.FixedResolved )
						list.Add( row );
				}
			}
		}

		// SSP 6/23/05 BR04635 BR04640
		// 
		//private void CalcSpecialAndFixedRowsHelper( out ArrayList outSpecialRows, out ArrayList outFixedRows, bool top )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void CalcSpecialAndFixedRowsHelper( out ArrayList outSpecialRows, 
		//    out ArrayList outFixedRows, bool top, UltraGridRow[] rowsToRecycle )
		private void CalcSpecialAndFixedRowsHelper( 
			out List<UltraGridRow> outSpecialRows,
			out List<UltraGridRow> outFixedRows, 
			bool top, 
			UltraGridRow[] rowsToRecycle )
		{
			outSpecialRows = null;
			outFixedRows = null;

			bool supportsFixedRows = this.SupportsFixedRows;
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList fixedRows = new ArrayList( 10 );
			//ArrayList specialRows = new ArrayList( 5 );
			List<UltraGridRow> fixedRows = new List<UltraGridRow>( 10 );
			List<UltraGridRow> specialRows = new List<UltraGridRow>( 5 );

			// SSP 6/23/05 BR04635 BR04640
			// 
			//this.GetSpecialRowsHelper( specialRows, top );
			this.GetSpecialRowsHelper( specialRows, top, rowsToRecycle );

			// For BOTTOM fixed rows add the fixed data rows to the list before adding the
			// fixed special rows so the fixed data rows appear before the fixed special rows.
			//
			if ( ! top )
				this.AddFixedRows( fixedRows, top );

			if ( null != specialRows && specialRows.Count > 0 )
			{
				// If fixed add-row functionality is on then set the fixedResolvedLocationOverride member 
				// var on the template add-row (or add-row from template) to FixedRowStyle.Top or
				// FixedRowStyle.Bottom depending on whether the AllowAddNewResolved is FixedAddRowOnTop
				// or FixedAddRowOnBottom.
				//
				// ------------------------------------------------------------------------------------
				UltraGridRow addRow;
				if ( specialRows.Contains( this.templateAddRow ) ) 
					addRow = this.templateAddRow;
				else if ( specialRows.Contains( this.currentAddRow ) )
					addRow = this.currentAddRow;
				else
					addRow = null;

				if ( supportsFixedRows && null != addRow )
				{
					if ( AllowAddNew.FixedAddRowOnTop == this.Band.AllowAddNewResolved )
						addRow.InternalSetFixedResolvedOverride( FixedRowStyle.Top );
					else if ( AllowAddNew.FixedAddRowOnBottom == this.Band.AllowAddNewResolved )
						addRow.InternalSetFixedResolvedOverride( FixedRowStyle.Bottom );
					else
						addRow.InternalSetFixedResolvedOverride( FixedRowStyle.Default );
				}
				// ------------------------------------------------------------------------------------

				int lastFixedRowIndex = -1;
				int firstFixedRowIndex = -1;
				if ( top ? this.HasFixedDataRowsVisibleOnTop : this.HasFixedDataRowsVisibleOnBottom )
				{
					// If there are some data rows that are fixed then cause all the special rows to be
					// fixed too. Otherwise having a template add-row, summary row or filter row between
					// fixed data rows and scrolling data rows doesn't make sense. We are forcing them
					// to be fixed by setting their fixedResolvedLocationOverride member var by
					// calling InternalSetFixedResolvedOverride further below in the for loop.
					//
					firstFixedRowIndex = 0;
					lastFixedRowIndex = specialRows.Count - 1;
				}
				else
				{
					// Also force scrolling special rows to be fixed if they occur before fixed special
					// rows (or after in the case of bottom fixed rows). We are forcing them to be fixed 
					// by setting their fixedResolvedLocationOverride member var by calling 
					// InternalSetFixedResolvedOverride further below in the for loop.
					//
					for ( int i = 0; i < specialRows.Count; i++ )
					{
						UltraGridRow row = (UltraGridRow)specialRows[i];
						if ( row.FixedResolved )
						{
							if ( firstFixedRowIndex < 0 )
								firstFixedRowIndex = i;

							lastFixedRowIndex = i;
						}
					}
				}

				for ( int i = 0; i < specialRows.Count; i++ )
				{
					UltraGridRow row = (UltraGridRow)specialRows[i];
					if ( top ? lastFixedRowIndex >= 0 && i <= lastFixedRowIndex 
						: firstFixedRowIndex >= 0 && i >= firstFixedRowIndex )
					{
						row.InternalSetFixedResolvedOverride( top ? FixedRowStyle.Top : FixedRowStyle.Bottom );
						fixedRows.Add( row );
					}
					// If the add-row from template isn't going to be fixed then don't include it in 
					// the special rows list since it's already part of the rows collection (the 
					// sparse array) since it's a data row now.
					//
					else if ( row == this.currentAddRow )
					{
						specialRows.RemoveAt( i );

						// SSP 10/20/05 BR06786
						// We also need to modify the firstFixedRowIndex and lastFixedRowIndex
						// indexes accordingly.
						// 
						// --------------------------------------------------------------------
						if ( firstFixedRowIndex > i )
							firstFixedRowIndex--;

						if ( lastFixedRowIndex > i )
							lastFixedRowIndex--;
						// --------------------------------------------------------------------

						i--;
					}
				}
			}

			// For TOP fixed rows add the fixed data rows to the list after adding the
			// fixed special rows so the fixed data rows appear after the fixed special rows.
			//
			if ( top )
				this.AddFixedRows( fixedRows, top );

			outSpecialRows = specialRows;
			outFixedRows = fixedRows;
		}

		#endregion // CalcSpecialAndFixedRowsHelper

		internal void EnsureSpecialAndFixedRowsCacheCalculated( )
		{
			if ( this.verifiedSpecialRowsCacheVersion != this.Layout.GrandVerifyVersion )
			{				
				this.verifiedSpecialRowsCacheVersion = this.Layout.GrandVerifyVersion;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList specialRows, fixedRows;
				List<UltraGridRow> specialRows, fixedRows;

				// SSP 6/23/05 BR04635 BR04640
				// 
				//this.CalcSpecialAndFixedRowsHelper( out specialRows, out fixedRows, true );
				this.CalcSpecialAndFixedRowsHelper( out specialRows, out fixedRows, true, this.cachedSpecialRowsTop );

				bool rowCountChanged = 
					fixedRows.Count != ( null != this.cachedFixedRowsTop ? this.cachedFixedRowsTop.Length : 0 )
					|| specialRows.Count != ( null != this.cachedSpecialRowsTop ? this.cachedSpecialRowsTop.Length : 0 );

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.cachedSpecialRowsTop = this.ToRowArrayHelper( specialRows );
				//this.cachedFixedRowsTop   = this.ToRowArrayHelper( fixedRows );
				this.cachedSpecialRowsTop = RowsCollection.ToRowArrayHelper( specialRows );
				this.cachedFixedRowsTop = RowsCollection.ToRowArrayHelper( fixedRows );

				// SSP 6/23/05 BR04635 BR04640
				// 
				//this.CalcSpecialAndFixedRowsHelper( out specialRows, out fixedRows, false );
				this.CalcSpecialAndFixedRowsHelper( out specialRows, out fixedRows, false, this.cachedSpecialRowsBottom );

				rowCountChanged = rowCountChanged 
					|| fixedRows.Count != ( null != this.cachedFixedRowsBottom ? this.cachedFixedRowsBottom.Length : 0 )
					|| specialRows.Count != ( null != this.cachedSpecialRowsBottom ? this.cachedSpecialRowsBottom.Length : 0 );

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.cachedSpecialRowsBottom = this.ToRowArrayHelper( specialRows );
				//this.cachedFixedRowsBottom   = this.ToRowArrayHelper( fixedRows );
				this.cachedSpecialRowsBottom = RowsCollection.ToRowArrayHelper( specialRows );
				this.cachedFixedRowsBottom = RowsCollection.ToRowArrayHelper( fixedRows );

				// If the scroll count if this collection changes, notify the parent row's
				// sparse array that it's scroll count has changed.
				//
				if ( rowCountChanged )
					this.DirtyParentRowScrollCount( false );
			}	
		}

		#endregion // EnsureSpecialAndFixedRowsCacheCalculated

		#region GetTopSpecialRows

		internal UltraGridRow[] GetTopSpecialRows( )
		{
			return this.GetSpecialRows( true );
		}

		#endregion // GetTopSpecialRows

		#region GetBottomSpecialRows

		internal UltraGridRow[] GetBottomSpecialRows( )
		{
			return this.GetSpecialRows( false );
		}

		#endregion // GetBottomSpecialRows

		#region GetSpecialRowAtIndex

		internal UltraGridRow GetSpecialRowAtIndex( int index, bool top )
		{
			if ( index >= 0 )
			{
				UltraGridRow[] specialRows = this.GetSpecialRows( top );
				if ( null != specialRows && index < specialRows.Length )
					return specialRows[ index ];
			}

			return null;
		}

		#endregion // GetSpecialRowAtIndex

		#region GetSpecialRowAtVisibleIndex

		internal UltraGridRow GetSpecialRowAtVisibleIndex( int index, bool top )
		{
			// Since special rows returned by GetSpecialRows are always visible
			// just use the GetSpecialRowAtIndex.
			//
			return this.GetSpecialRowAtIndex( index, top );
		}

		#endregion // GetSpecialRowAtVisibleIndex

		#region GetSpecialRowAtScrollIndex

		internal UltraGridRow GetSpecialRowAtScrollIndex( int index, bool top )
		{
			// Since none of the special rows have any descendants, just use the
			// GetSpecialRowAtVisibleIndex.
			//
			return this.GetSpecialRowAtVisibleIndex( index, top );
		}

		#endregion // GetSpecialRowAtScrollIndex

		#region GetSpecialRowsCount

		internal int GetSpecialRowsCount( bool top )
		{
			UltraGridRow[] specialRows = this.GetSpecialRows( top );
			return null != specialRows ? specialRows.Length : 0;
		}

		#endregion // GetSpecialRowsCount

		#region GetSpecialRowsVisibleCount

		internal int GetSpecialRowsVisibleCount( bool top )
		{
			// Since special rows returned by GetSpecialRows are always visible
			// just use the GetSpecialRowsCount.
			//
			return this.GetSpecialRowsCount( top );
		}

		#endregion // GetSpecialRowsVisibleCount

		#region GetSpecialRowsScrollCount

		internal int GetSpecialRowsScrollCount( bool top )
		{
			// Since none of the special rows have any descendants, just use the
			// GetSpecialRowsVisibleCount.
			//
			return this.GetSpecialRowsVisibleCount( top );
		}

		#endregion // GetSpecialRowsScrollCount

		#region GetSpecialRowIndex

		internal int GetSpecialRowIndex( UltraGridRow specialRow, bool top )
		{
			UltraGridRow[] specialRows = this.GetSpecialRows( top );
			return null != specialRows ? Array.IndexOf( specialRows, specialRow ) : -1;
		}

		#endregion // GetSpecialRowIndex

		#region GetSpecialRowVisibleIndex

		internal int GetSpecialRowVisibleIndex( UltraGridRow specialRow, bool top )
		{
			// Since special rows returned by GetSpecialRows are always visible
			// just use the GetSpecialRowIndex.
			//
			return this.GetSpecialRowIndex( specialRow, top );
		}

		#endregion // GetSpecialRowVisibleIndex

		#region GetSpecialRowScrollIndex

		internal int GetSpecialRowScrollIndex( UltraGridRow specialRow, bool top )
		{
			// Since none of the special rows have any descendants, just use the
			// GetSpecialRowVisibleIndex.
			//
			return this.GetSpecialRowVisibleIndex( specialRow, top );
		}

		#endregion // GetSpecialRowScrollIndex

		#region FixedAddRow_TranslateIndex

		private int FixedAddRow_TranslateIndex( int index, bool toExternalIndexSpace )
		{
			// This is to make the fixed add-row from template add-row work. While the
			// add-row is fixed it is both in the SpecialRows list and in the main collection.
			// For the purposes of GetRowIndex and GetRowAtIndex use the one in the special
			// rows list.
			// 

			if ( null == this.currentAddRow )
				return index;

			UltraGridRow row = this.currentAddRow;
			int topSpecialIndex = this.GetSpecialRowIndex( row, true );
			int bottomSpecialIndex = this.GetSpecialRowIndex( row, false );

			// One or the the other, but not both.
			//
			Debug.Assert( topSpecialIndex < 0 || bottomSpecialIndex < 0 );

			if ( topSpecialIndex >= 0 || bottomSpecialIndex >= 0 )
			{
				int mainIndex = this.SparseArray.IndexOf( row );
				Debug.Assert( mainIndex >= 0 );
				if ( mainIndex >= 0 )
				{
					mainIndex += this.GetSpecialRowsCount( true );

					if ( index >= mainIndex )
						index += toExternalIndexSpace ? -1 : 1;
				}
			}

			return index;
		}

		#endregion // FixedAddRow_TranslateIndex

		#region CountIncludingSpecialRows
		
		internal int CountIncludingSpecialRows
		{
			get
			{
				return this.GetRowCount( true );
			}
		}

		#endregion // CountIncludingSpecialRows

		#region GetRowIndex

		internal int GetRowIndex( UltraGridRow row, IncludeRowTypes includeRowTypes )
		{
			return this.GetRowIndex( row, IncludeRowTypes.SpecialRows == includeRowTypes );
		}
		
		internal int GetRowIndex( UltraGridRow row, bool includeSpecialRows )
		{
			// SSP 8/12/05 BR05556
			// If the rows are marked dirty then verify them.
			// 
			this.EnsureNotDirty( );

			int index = this.SparseArray.IndexOf( row );

			if ( includeSpecialRows )
			{
				// This is to make the fixed add-row from template add-row work. While the
				// add-row is fixed it is both in the SpecialRows list and in the main collection.
				// For the purposes of GetRowIndex and GetRowAtIndex use the one in the special
				// rows list.
				// 
				if ( row == this.currentAddRow 
					&& ( this.GetSpecialRowIndex( row, true ) >= 0 || this.GetSpecialRowIndex( row, false ) >= 0 ) )
					index = -1;

				if ( index >= 0 )
				{
					index += this.GetSpecialRowsCount( true );
				}
				else 
				{
					index = this.GetSpecialRowIndex( row, true );

					if ( index < 0 )
					{
						index = this.GetSpecialRowIndex( row, false );
						if ( index >= 0 )
							index += this.GetSpecialRowsCount( true ) + this.SparseArray.Count;
					}
				}

				// This is to make the fixed add-row from template add-row work. While the
				// add-row is fixed it is both in the SpecialRows list and in the main collection.
				// For the purposes of GetRowIndex and GetRowAtIndex use the one in the special
				// rows list.
				// 
				index = this.FixedAddRow_TranslateIndex( index, true );
			}

			return index;
		}

		#endregion // GetRowIndex

		#region GetRowCount

		internal int GetRowCount( IncludeRowTypes includeRowTypes )
		{
			return this.GetRowCount( IncludeRowTypes.SpecialRows == includeRowTypes );
		}

		internal int GetRowCount( bool includeSpecialRows )
		{
			int count = this.Count;
			if ( includeSpecialRows )
			{
				count += this.GetSpecialRowsCount( true ) + this.GetSpecialRowsCount( false );

				// This is to make the fixed add-row from template add-row work. While the
				// add-row is fixed it is both in the SpecialRows list and in the main collection.
				// For the purposes of GetRowIndex and GetRowAtIndex use the one in the special
				// rows list.
				// 
				count = this.FixedAddRow_TranslateIndex( count, true );
			}

			return count;
		}

		#endregion // GetRowCount

		#region GetRowAtIndex
		
		internal UltraGridRow GetRowAtIndex( int index, IncludeRowTypes includeRowTypes )
		{
			return this.GetRowAtIndex( index, IncludeRowTypes.SpecialRows == includeRowTypes );
		}
		
		internal UltraGridRow GetRowAtIndex( int index, bool includeSpecialRows )
		{
			if ( index >= 0 )
			{
				if ( includeSpecialRows )
				{
					// This is to make the fixed add-row from template add-row work. While the
					// add-row is fixed it is both in the SpecialRows list and in the main collection.
					// For the purposes of GetRowIndex and GetRowAtIndex use the one in the special
					// rows list.
					// 
					index = this.FixedAddRow_TranslateIndex( index, false );

					UltraGridRow row = this.GetSpecialRowAtIndex( index, true );
					if ( null != row )
						return row;

					index -= this.GetSpecialRowsCount( true );
				}

				int count = this.Count;
				if ( index >= 0 && index < count )
					return this[ index ];

				index -= count;

				if ( includeSpecialRows )
					return this.GetSpecialRowAtIndex( index, false );
			}
			
			return null;
		}

		#endregion // GetRowAtIndex

		#region GetRowAtIndexOffset

		internal UltraGridRow GetRowAtIndexOffset( UltraGridRow row, int offset, IncludeRowTypes includeRowTypes )
		{
			return this.GetRowAtIndexOffset( row, offset, IncludeRowTypes.SpecialRows == includeRowTypes );
		}
		
		internal UltraGridRow GetRowAtIndexOffset( UltraGridRow row, int offset, bool includeSpecialRows )
		{
			int index = this.GetRowIndex( row, includeSpecialRows );
			return index >= 0 ? this.GetRowAtIndex( index + offset, includeSpecialRows ) : null;
		}

		#endregion // GetRowAtIndexOffset

		#region GetFirstRow

		internal UltraGridRow GetFirstRow( IncludeRowTypes includeRowTypes )
		{
			return this.GetFirstRow( IncludeRowTypes.SpecialRows == includeRowTypes );
		}

		internal UltraGridRow GetFirstRow( bool includeSpecialRows )
		{
			return this.GetRowAtIndex( 0, includeSpecialRows );
		}

		#endregion // GetFirstRow

		#region GetLastRow

		internal UltraGridRow GetLastRow( IncludeRowTypes includeRowTypes )
		{
			return this.GetLastRow( IncludeRowTypes.SpecialRows == includeRowTypes );
		}

		internal UltraGridRow GetLastRow( bool includeSpecialRows )
		{
			int count = this.GetRowCount( includeSpecialRows );
			return this.GetRowAtIndex( count - 1, includeSpecialRows );
		}

		#endregion // GetLastRow

		#region GroupByRowLevel

		// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
		// Moved GroupByRowLevel property from the UltraGridGroupByRow to here.
		//
		internal int GroupByRowLevel
		{
			get
			{
				if ( !( this.ParentRow is UltraGridGroupByRow ) )
					return 0;

				return 1 + ((UltraGridGroupByRow)this.ParentRow).GroupByRowLevel;
			}
		}

		#endregion // GroupByRowLevel

		#region SupportsFixedRows
		
		internal bool SupportsFixedRows
		{
			get
			{
				UltraGridBand band = this.Band;
				UltraGridLayout layout = null != band ? band.Layout : null;
				ViewStyleBase viewStyle = null != layout ? layout.ViewStyleImpl : null;
				if ( null != this.ParentRow 
					|| band.CardView 
					|| null == viewStyle 
					|| ! viewStyle.SupportsFixedRows 
					|| ! layout.IsDisplayLayout )
					return false;

				return true;
			}
		}

		#endregion // SupportsFixedRows

		#region IsTemplateOrAddRowFromTemplate

		internal bool IsTemplateOrAddRowFromTemplate( UltraGridRow row )
		{
			return row == this.templateAddRow || row == this.currentAddRow;
		}

		#endregion // IsTemplateOrAddRowFromTemplate

		#region IsFixedAddRow

		internal bool IsFixedAddRow( UltraGridRow row )
		{
			return this.IsTemplateOrAddRowFromTemplate( row ) && row.FixedResolved;
		}

		#endregion // IsFixedAddRow

		#region IsLastFixedRowOnTop
		
		internal bool IsLastFixedRowOnTop( UltraGridRow row )
		{
			UltraGridRow[] fixedRows = this.GetFixedRows( true );
			return null != fixedRows && fixedRows.Length > 0 && fixedRows[ fixedRows.Length - 1 ] == row;
		}

		#endregion // IsLastFixedRowOnTop

		#region IsFirstFixedRowOnBottom
		
		internal bool IsFirstFixedRowOnBottom( UltraGridRow row )
		{
			UltraGridRow[] fixedRows = this.GetFixedRows( false );
			return null != fixedRows && fixedRows.Length > 0 && fixedRows[ 0 ] == row;
		}

		#endregion // IsFirstFixedRowOnBottom

		#region HasFixedRows

		internal bool HasFixedRows( bool top )
		{
			UltraGridRow[] fixedRows = this.GetFixedRows( top );
			return null != fixedRows && fixedRows.Length > 0;
		}

		#endregion // HasFixedRows

		#region GetFixedRows

		internal UltraGridRow[] GetFixedRows( bool top )
		{
			this.EnsureSpecialAndFixedRowsCacheCalculated( );

			return top ? this.cachedFixedRowsTop : this.cachedFixedRowsBottom;
		}

		#endregion // GetFixedRows

		#region GetFirstScrollableRow
		
		internal UltraGridRow GetFirstScrollableRow( )
		{
			UltraGridRow row = null;
			for ( int i = 0, count = this.ScrollCount; i < count; i++ )
			{
				row = this.GetRowAtScrollIndex( i );
				if ( null == row || ! row.FixedResolved )
					break;
			}

			return row;
		}

		#endregion // GetFirstScrollableRow

		#region GetLastScrollableRow

		internal UltraGridRow GetLastScrollableRow( )
		{
			
			
			
			for ( int i = this.ScrollCount - 1; i >= 0; i-- )
			{
				UltraGridRow row = this.GetRowAtScrollIndex( i );
				if ( null == row || ! row.FixedResolved )
					return row;
			}

			return null;
			
			
		}

		#endregion // GetLastScrollableRow

		#region IsLastScrollableRowAllocated

		// SSP 8/2/06 BR14598
		// 
		internal bool IsLastScrollableRowNotAllocatedYet
		{
			get
			{
				for ( int i = this.ScrollCount - 1; i >= 0; i-- )
				{
					UltraGridRow row = this.GetRowAtScrollIndex( i, false );
					if ( null == row )
						return true;
					else if ( ! row.FixedResolved )
						break;
				}

				return false;
			}
		}

		#endregion // IsLastScrollableRowAllocated

		#region GetFirstLastActivatableRow

		internal UltraGridRow GetFirstLastActivatableRow( bool first )
		{
			return this.GetFirstLastActivatableRow( first, false, false, false );
		}

		// SSP 12/28/05 BR08498
		// Added CanNavigateToViaSelectionStrategy helper method.
		// 
		private bool CanNavigateToViaSelectionStrategy( UltraGridRow row, bool shiftKeyDown, bool ctrlKeyDown )
		{
			UltraGridLayout layout = this.Layout;
			UltraGridBase grid = null != layout ? layout.Grid : null;
			ISelectionStrategy selectionStrategy = null != grid 
				? ((ISelectionManager)grid).GetSelectionStrategy( row ) : null;

			return null != selectionStrategy && selectionStrategy.CanItemBeNavigatedTo( row, shiftKeyDown, ctrlKeyDown  );
		}

		// SSP 12/28/05 BR08498
		// Added an overload that takes in checkForSelectionStrategyNavigation, shiftKeyDown 
		// and ctrlKeyDown parameters.
		// 
		internal UltraGridRow GetFirstLastActivatableRow( bool first, 
			bool checkForSelectionStrategyNavigation, bool shiftKeyDown, bool ctrlKeyDown )
		{
			UltraGridRow row = first ? this.GetFirstVisibleRow( ) : this.GetLastVisibleRow( );
			// SSP 12/28/05 BR08498
			// Added an overload that takes in checkForSelectionStrategyNavigation, shiftKeyDown 
			// and ctrlKeyDown parameters.
			// 
			//while ( null != row && Activation.Disabled == row.ActivationResolved )
			while ( null != row && ( Activation.Disabled == row.ActivationResolved 
				|| checkForSelectionStrategyNavigation && ! this.CanNavigateToViaSelectionStrategy( row, shiftKeyDown, ctrlKeyDown ) )
				)
			{
				row = this.GetRowAtVisibleIndexOffset( row, first ? 1 : -1 );
			}

			return row;				
		}

		#endregion // GetFirstLastActivatableRow

		#region AddRowModifiedByUser

		// SSP 6/12/06 BR13432
		// 
		/// <summary>
		/// Specifies whether the add-row belonging to this row collection has been modified by the 
		/// user. This flag is used to determine whether to commit a template add-row when 
		/// the user moves off the row. If the user doesn't modify a template add-row then it will
		/// not be committed when the user leaves the template add-row.
		/// </summary>
		public bool AddRowModifiedByUser
		{
			get
			{
				return this.CurrentAddRowModifiedByUser;
			}
			set
			{
				this.CurrentAddRowModifiedByUser = value;
			}
		}

		#endregion // AddRowModifiedByUser

		#endregion // Fixed Rows/Filter Row/Fixed Add-Row/Extension of Summaries Functionality

        // MRS NAS v8.2 - CardView Printing
        #region NAS v8.2 - CardView Printing

        #region ExpandAllCards

        /// <summary>
        /// Expand all cards in the collection when using <see cref="UltraGridBand.CardView"/> mode. Only applies when <see cref="CardStyle"/> is compressed.
        /// </summary>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.CollapseAllCards"/>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.ExpandAll"/>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.CollapseAll"/>
        public void ExpandAllCards()
        {
            UltraGridLayout layout = this.Layout;
            UltraGridBand band = this.Band;

            if (band != null &&
                (band.CardView == false || band.CardSettings.Style != CardStyle.Compressed))
            {
                return;
            }

            // MRS NAS v8.2 - CardView Printing
            // Copied from ExpandAll method. Presumably the same issue applies.
            //
            // SSP 10/23/03 UWG2334
            // Expanding or collapsing a row could lead to the cell in edit mode being scrolled 
            // out of view which we don't want happen. So exit the edit mode and if that fails 
            // then return without expanding any rows.
            //
            if (null != layout && !layout.ExitEditModeHelper())
                return;

            //Loop over collection, expanding each row
            //
            for (int i = 0; i < this.Count; ++i)
            {                
                this[i].IsCardCompressed = false;             
            }
        }

        #endregion // ExpandAllCards

        #region CollapseAllCards

        /// <summary>
        /// Collapse all cards in the collection when using <see cref="UltraGridBand.CardView"/> mode. Only applies when <see cref="CardStyle"/> is compressed.
        /// </summary>        
        /// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.ExpandAllCards"/>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.ExpandAll"/>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.CollapseAll"/>
        public void CollapseAllCards()
        {
            UltraGridLayout layout = this.Layout;
            UltraGridBand band = this.Band;

            if (band != null &&
                (band.CardView == false || band.CardSettings.Style != CardStyle.Compressed))
            {
                return;
            }

            // MRS NAS v8.2 - CardView Printing
            // Copied from ExpandAll method. Presumably the same issue applies.
            //
            // SSP 10/23/03 UWG2334
            // Expanding or collapsing a row could lead to the cell in edit mode being scrolled 
            // out of view which we don't want happen. So exit the edit mode and if that fails 
            // then return without expanding any rows.
            //
            if (null != layout && !layout.ExitEditModeHelper())
                return;

            //RobA UWG466 10/1/01 implemented method
            //

            //Loop over collection, collapsing each row
            //
            for (int i = 0; i < this.Count; ++i)
            {                
                // then collapse this row
                //
                this[i].IsCardCompressed = true;
            }
        }

        #endregion // CollapseAllCards

        #endregion //NAS v8.2 - CardView Printing

        // CDS NAS v9.1 Header CheckBox
        #region NA v9.1 Header CheckBox

        private Dictionary<UltraGridColumn, CheckState> checkStateCache = new Dictionary<UltraGridColumn, CheckState>();
        internal Dictionary<UltraGridColumn, bool> checkStateCacheValidHashTable = new Dictionary<UltraGridColumn, bool>();
        // CDS 02/04/08 - TFS12517 - NAS v9.1 Header CheckBox - When the synchronization changes, we need to invalidate all the cached CheckState values, so we'll using versioning.
        private int currentHeaderCheckBoxSynchronizationVersion = 0;

        #region GetHeaderCheckState

        internal CheckState GetHeaderCheckState(UltraGridColumn column)
        {
            // 01/29/08 CDS TFS12797 Make sure the column is part of the band.
            if (this.band == null ||
                !this.band.Columns.Contains(column))
                throw new InvalidOperationException(Shared.SR.GetString("LE_RowsCollection_ColumnNotInBand"));

            if (!this.IsCachedCheckStateValid(column))
            {
                //get the value and put it in the cache.
                // CDS 2/26/09 TFS14684 Don't want to synchronize since we're obtaining the value from the cells
                //this.SetCheckStateCache(column, column.GetHeaderCheckedStateBasedOnCellValues(this), false);
                this.SetCheckStateCache(column, column.GetHeaderCheckedStateBasedOnCellValues(this), false, false);
            }
            return (CheckState)this.checkStateCache[column];
        }

        #endregion GetHeaderCheckState
        
        #region IsCachedCheckStateValid

        private bool IsCachedCheckStateValid(UltraGridColumn column)
        {
            // CDS 1/30/09 TFS12517
            // Check to see if the synchronization has changed, which would invalidate the cache
            if (column.Band != null &&
                column.Band.HeaderCheckBoxSynchronizationVersion != this.currentHeaderCheckBoxSynchronizationVersion)
            {
                this.currentHeaderCheckBoxSynchronizationVersion = column.Band.HeaderCheckBoxSynchronizationVersion;
                this.DirtyCheckStateCacheValidEntry(null);
                return false;
            }

            if (!this.checkStateCacheValidHashTable.ContainsKey(column))
                return false;
            return (bool)this.checkStateCacheValidHashTable[column];
        }

        #endregion IsCachedCheckStateValid

        #region DirtyCheckStateCacheValidEntry

        internal void DirtyCheckStateCacheValidEntry(UltraGridColumn column)
        {
            // CDS 01/23/ 09 If column is null, clear the entire dictionary.
            if (column == null)
                this.checkStateCacheValidHashTable.Clear();
            else
                this.checkStateCacheValidHashTable[column] = false;
        }

        #endregion DirtyCheckStateCacheValidEntry

        #region SetCheckStateCache

        // CDS 2/26/09 TFS14684 
        //internal void SetCheckStateCache(UltraGridColumn column, CheckState state, bool isCancelable)
        internal void SetCheckStateCache(UltraGridColumn column, CheckState state, bool isCancelable, bool synchronize)
        {

            
            // CDS 03/26/09 TFS14747 Only need to take the active cell out of EditMode if we are going to synchronize
            if (synchronize)
            {
                UltraGrid grid = this.Layout.Grid as UltraGrid;

                // CDS 3/26/09 TFS12512 The Grid property is not an UltraGrid for the UltraCombo, so we need an null check.
                //if ( null != grid.ActiveCell &&
                if (null != grid &&
                    null != grid.ActiveCell &&
                    grid.ActiveCell.IsInEditMode &&
                    grid.ActiveCell.ExitEditMode(false, false))
                    return;
            }

            System.Windows.Forms.CheckState newCheckState = state;

            // check to see if the column exists
            bool columnCheckStateExists = this.checkStateCache.ContainsKey(column);

            if (!columnCheckStateExists ||
                (CheckState)this.checkStateCache[column] != newCheckState)
            {
                // If it doesn't exist or the state is different, fire the Before event

                // CDS 3/26/09 TFS12512 Events have been moved to the base class so they will fire for the UltraCombo.
                UltraGridBase gridBase = this.Layout.Grid;

                // Fire the BeforeHeaderCheckStateChanged event
                BeforeHeaderCheckStateChangedEventArgs args = new BeforeHeaderCheckStateChangedEventArgs(column, this, state, columnCheckStateExists ? (CheckState)this.checkStateCache[column]: CheckState.Indeterminate, isCancelable);
                // CDS 3/26/09 TFS12512 Changed to use the base class
                //grid.FireEvent(GridEventIds.BeforeHeaderCheckStateChanged, args);
                gridBase.FireCommonEvent(CommonEventIds.BeforeHeaderCheckStateChanged, args);

                // Check for cancelling
                if (args.Cancel && 
                    isCancelable)
                    return;

                newCheckState = args.NewCheckState;

                this.checkStateCache[column] = newCheckState;
                // CDS 2/24/09 TFS12452 - Unrelated to actual bug, but needed fixing. Need to mark the cached CheckState 
                // as valid outside this if statement. Otherwise, we will re-synch whenever the header is painted and the synched
                // CheckState matches the one saved one (even though it was marked as not valid).
                //this.checkStateCacheValidHashTable[column] = true;

                column.DirtyHeaderUIElements(this);

                // CDS 6/18/09 TFS17819 Moved the synchronization to prior to the After event firing
                // Synchronize if necessary
                if (synchronize &&
                    column.HeaderCheckBoxSynchronizationResolved == HeaderCheckBoxSynchronization.RowsCollection &&
                    newCheckState != CheckState.Indeterminate)
                    column.SetCellValuesBasedOnCheckState(this, newCheckState);

                // Fire the AfterHeaderCheckStateChanged event
                
                // CDS 6/18/09 TFS17819 Passed this (RowsCollection) instead of null
                //// CDS 3/26/09 TFS12512 Changed to use the base class
                ////grid.FireEvent(GridEventIds.AfterHeaderCheckStateChanged, new AfterHeaderCheckStateChangedEventArgs(column, null));
                //gridBase.FireCommonEvent(CommonEventIds.AfterHeaderCheckStateChanged, new AfterHeaderCheckStateChangedEventArgs(column, null));
                gridBase.FireCommonEvent(CommonEventIds.AfterHeaderCheckStateChanged, new AfterHeaderCheckStateChangedEventArgs(column, this));
            }

            // CDS 2/24/09 TFS12452 Moved from inside the if statement, to make sure the cached value is marked valid
            this.checkStateCacheValidHashTable[column] = true;

            // CDS 6/18/09 TFS17819 Moved the synchronization to prior to the After event firing
            //// CDS 2/26/09 TFS14684 Jump out if we don't intend to synchronize
            //if (!synchronize)
            //    return;

            //// Synchronize if necessary
            //if (column.HeaderCheckBoxSynchronizationResolved == HeaderCheckBoxSynchronization.RowsCollection &&
            //    newCheckState != CheckState.Indeterminate)
            //    column.SetCellValuesBasedOnCheckState(this, newCheckState);
        }

        #endregion

        #endregion NA v9.1 Header CheckBox

        //  BF NA 9.1 - UltraCombo MultiSelect
        #region NA 9.1 - UltraCombo MultiSelect

            #region DataChanged event
        internal event CheckedListSettings.DataChangedHandler DataChanged;
        
        internal void FireDataChanged( CheckedListSettings.DataChangedEventArgs args )
        {
            if ( this.DataChanged != null )
                this.DataChanged( this, args );
        }

        internal void RaiseDataChangedEvent( string columnKey )
        {
            CheckedListSettings.DataChangedEventArgs args =
                new CheckedListSettings.DataChangedEventArgs( null, columnKey, null );

            this.FireDataChanged( args );
        }
            #endregion DataChanged event

            #region IEnumerable<UltraGridRow> Members
        IEnumerator<UltraGridRow> IEnumerable<UltraGridRow>.GetEnumerator()
        {
            foreach( UltraGridRow row in this )
            {
                yield return row;
            }

        }
            #endregion IEnumerable<UltraGridRow> Members

        #endregion NA 9.1 - UltraCombo MultiSelect
    }

	#endregion // RowsCollection Class

	// MD 5/5/08
	// Made changes to allow for VS2008 style unit test accessors
	// Moved outside from RowsCollection
	#region IRowsCollectionCallback Interface Definition

	// SSP 12/8/05 BR07665
	// Added IRowsCollectionCallback interface.
	// 
	internal interface IRowsCollectionCallback
	{
		/// <summary>
		/// Return true to continue traversing, false to stop.
		/// </summary>
		/// <param name="rows"></param>
		/// <returns></returns>
		bool ProcessRowsCollection( RowsCollection rows );
	}

	#endregion // IRowsCollectionCallback Interface Definition

	// MD 1/24/08
	// Made changes to allow for VS2008 style unit test accessors
	// Moved outside from RowsCollection
	#region IRowCallback Interface Definition

	internal interface IRowCallback
	{
		/// <summary>
		/// Return true to continue traversing, false to stop.
		/// </summary>
		/// <param name="row"></param>
		/// <returns></returns>
		bool ProcessRow( UltraGridRow row );
	}

	#endregion // IRowCallback Interface Definition

	#region RowEnumerator

	/// <summary>
	/// Enumerator for a collection of rows
	/// </summary>
	public class RowEnumerator : IEnumerator
	{   
		private IEnumerator enumerator = null;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="rows">The rows collection to enumerate.</param>
		public RowEnumerator( RowsCollection rows )
		{
			// SSP 8/18/04
			// Pass in ICreateItemCallback which the rows.SparseArray implements so that
			// in LoadOnDemand mode rows that haven't been created yet get created.
			//
			//this.enumerator = rows.SparseArray.GetEnumerator( );
			this.enumerator = rows.SparseArray.GetEnumerator( rows.SparseArray );
		}

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="rows">The selected rows collection to enumerate.</param>
		public RowEnumerator( SelectedRowsCollection rows )
		{
			this.enumerator = ((IEnumerable)rows).GetEnumerator( );
		}

		/// <summary>
		/// Resets the enumerator.
		/// </summary>
		public void Reset( )
		{
			this.enumerator.Reset( );
		}

		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		/// <summary>
		/// Type-safe version of Current
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Current 
		{
			get
			{
				return (UltraGridRow)this.enumerator.Current;
			}
		}

        /// <summary>
        /// Advances the enumerator to the next row in the collection.
        /// </summary>
        /// <returns><b>True</b> if the enumerator was successfully advanced to the next element; <b>false</b> if the enumerator has passed the end of the collection.</returns>
        public bool MoveNext()
        {
			return this.enumerator.MoveNext( );
        }
    }

	// SSP 4/9/04 - Virtual Binding Related
	// Commented out the original and added new one above.
	//
	

	#endregion // RowEnumerator
}

