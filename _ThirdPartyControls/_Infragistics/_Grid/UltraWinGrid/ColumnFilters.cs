#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.Globalization;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Shared.Serialization;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using Infragistics.Win.UltraWinMaskedEdit;
	using System.Drawing.Design;
	using System.Windows.Forms.Design;
	using System.Text;
	using System.Text.RegularExpressions;
	using System.Security.Permissions;


	// SSP 3/21/02
	// Version 2 Row Filter Implementation
	// Added ColumnFiltersCollection class
	// 


	/// <summary>
	/// ColumnFiltersCollection class. UltraGridBand.ColumnFilters and RowsCollection.ColumnFilters return objects of this type.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// A <b>FilterCondition</b> object defines a single condition.
	/// Multiple FilterCondition instances can be added to the 
	/// <see cref="FilterConditionsCollection"/>. 
	/// A <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/> instance
	/// contains a <b>FilterConditionsCollection</b> instance. The <b>ColumnFilter</b>
	/// has <see cref="ColumnFilter.LogicalOperator"/> property which specifies
	/// how multiple conditions contained in the ColumnFilter's FilterConditionCollection
	/// are to be combined.
	/// A <see cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/> 
	/// can contain multiple <b>ColumnFilter</b> instances. Both the
	/// <see cref="UltraGridBand"/> and <see cref="RowsCollection"/> objects expose
	/// <b>ColumnFilters</b> property. This property returns a collection of
	/// <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/> objects.
	/// UltraGrid will filter rows using either the 
	/// RowsCollection's <see cref="RowsCollection.ColumnFilters"/> or 
	/// UltraGridBand's <see cref="UltraGridBand.ColumnFilters"/> depending on the
	/// what the Override's <see cref="UltraGridOverride.RowFilterMode"/> property
	/// is set to. See <see cref="UltraGridOverride.RowFilterMode"/> for more information.
	/// </p>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterCondition"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter.FilterConditions"/>
	/// <seealso cref="UltraGridBand.ColumnFilters"/>
	/// <seealso cref="RowsCollection.ColumnFilters"/>
	/// <seealso cref="UltraGridOverride.RowFilterMode"/>
	/// <seealso cref="UltraGridOverride.FilterUIType"/>
	/// </remarks>
	[ Serializable() ]
	public class ColumnFiltersCollection : KeyedSubObjectsCollectionBase, ISerializable
	{	
		#region Static constants

		// SSP 11/25/03
		// We should be regetting the strings from the sr every time because it could change.
		// Commented out below code and added the necessary code elsewhere.
		//
		

		#endregion // Static constants

		#region Private Variables

		// Used for syncronizing with the band's columns collecion.
		//
		private int columnsCollectionVersion = -1;

		private UltraGridBand band = null;

		// SSP 6/30/03 UWG2086
		// Added an anti-recursion flag.
		//
		private bool inSynchronizeWithColumnsColection = false;

		// SSP 3/9/05 - Added LogicalOperator to ColumnFiltersCollection
		//
		private FilterLogicalOperator logicalOperator = FilterLogicalOperator.And;

		#endregion // Private Variables

		#region Contructors

		internal ColumnFiltersCollection( UltraGridBand band ) : base( )
		{
			if ( null == band )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_104"), Shared.SR.GetString("LE_ArgumentNullException_319") );

			this.band = band;
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal ColumnFiltersCollection( SerializationInfo info, StreamingContext context )
		protected ColumnFiltersCollection( SerializationInfo info, StreamingContext context )
		{
			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			//
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( Infragistics.Win.UltraWinGrid.ColumnFilter ) )
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					ColumnFilter item = (ColumnFilter)Utils.DeserializeProperty( entry, typeof(ColumnFilter), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd( item );
				}
				else
				{
					switch ( entry.Name )
					{
						case "Tag":
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						default:
							Debug.Assert( false, "Invalid entry in ColumnFiltersCollection de-serialization ctor" );
							break;
					}
				}
			}
		}
		
		#endregion //Contructors

		#region InitialCapacity
		
		/// <summary>
		/// Property that specifies the initial capacity
		/// of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				if ( null != this.Band && null != this.Band.Columns )
					return this.Band.Columns.Count;

				return 10;
			}
		}

		#endregion // InitialCapacity

		#region All

		/// <summary>
		/// The collection as an array of objects
		/// </summary>
		public override object[] All 
		{
			get 
			{
				this.SynchronizeWithColumnsColection( );

				object[] array = new object[ this.Count ];

				// we need to call getobject so that 
				// any cells that haven't been created 
				// will be created now before we
				// return the array
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					array[ i ] = this.GetItem( i );
				}

				return array;
			}
            
			set 
			{
				throw new NotSupportedException( );
			}
		}

		#endregion // All

		#region IsReadOnly

		/// <summary>
		/// Returns true if the collection is read-only. This is used
		/// to make the All property read-only.
		/// </summary>
		public override bool IsReadOnly 
		{
			get
			{
				return true;
			}
		}

		#endregion // IsReadOnly

		#region Band

		private UltraGridBand Band 
		{
			get
			{
				return this.band;
			}
		}

		#endregion // Band

		#region InternalClearHelper

		private void InternalClearHelper( )
		{
			for ( int i = 0; i < this.List.Count; i++ )
			{
				ColumnFilter columnFilter = this.List[i] as Infragistics.Win.UltraWinGrid.ColumnFilter;

				if ( null != columnFilter )
                    columnFilter.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			}

			// SSP 5/2/05
			// Clear teh list directly because calling InternalClear uses the GetItem method which
			// causes us to allocate column filters.
			//
			//base.InternalClear( );
			this.List.Clear( );
		}

		#endregion //InternalClearHelper
	
		#region InternalRemoveHelper

		private void InternalRemoveHelper( int index )
		{
			// Unhook from the ColumnFilter at index before removing it from
			// the list.
			//
			ColumnFilter columnFilter = this.List[ index ] as Infragistics.Win.UltraWinGrid.ColumnFilter;

			if ( null != columnFilter )
				columnFilter.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			// Now remove it.
			//
			base.InternalRemove( index );
		}

		#endregion // InternalRemoveHelper

		#region SynchronizeWithColumnsColection

		private void SynchronizeWithColumnsColection( )
		{
			// SSP 6/30/03 UWG2086
			// Added an anti-recursion flag.
			//
			if ( this.inSynchronizeWithColumnsColection )
				return;

			this.inSynchronizeWithColumnsColection = true;

			try
			{
				ColumnsCollection columns = this.Band.Columns;

				// get the number of columns
				//
				int columnCount = columns.Count;

				// if our count is the same then exit
				//
				if ( columnCount == this.List.Count )
				{
					// JJD 10/17/01
					// Only exit if the version numbers match
					//
					if ( this.columnsCollectionVersion == this.Band.ColumnsVersion )
						return;
				}

				// if there are no columns then just clear the cells collection
				// and return
				//
				if ( 0 == columnCount )
				{				
					this.InternalClearHelper( );
					return;
				}

				this.List.Capacity = this.List.Count;

				int columnIndex;

				// JJD 10/17/01
				// loop over the list and verify that the indexes match 
				// the indexes in the coumns collection
				//
				for ( int i = 0; i < this.List.Count; ++i )
				{
					// if the cell slot is null continue
					//
					if ( this.List[i] == null )
						continue;

					// get the index of the column filter's column
					//
					columnIndex = ((Infragistics.Win.UltraWinGrid.ColumnFilter)this.List[i]).Column.Index;

					// If the column filter's column's index is less than
					// zero it means that this column have been
					// removed from the collection, so remove
					// the column filter as well and continue
					//
					if ( columnIndex < 0 )
					{
						this.InternalRemoveHelper( i );
						--i;
						continue;
					}

					// If the column filter's column's index is less than
					// the column filter's index then remove the prior
					// column filter slot, decrement i and continue.
					// Note: this should always be a null slot
					// since non-null slots would have already
					// been verified
					//
					if ( columnIndex < i )
					{
						System.Diagnostics.Debug.Assert( this.List[i - 1] == null, "Cell slot filled, should be null." );
					
						this.InternalRemoveHelper( i - 1 );
					
						// Decrement i by 2 to account for this cell moving
						// up a slot and the ++ of the for loop so that
						// we can check this cell again in case multiple
						// empty slots need to be removed
						//
						i -= 2;
					
						continue;
					}

					// If the cell's column's index is greater than
					// the cell's index then add a null slot and 
					// continue.
					//
					if ( columnIndex > i )
					{
						this.List.Insert( i, null );
						continue;
					}
				}

				// if our count is greater then get rid of
				// excess cell slots
				//
				if ( columnCount < this.List.Count ) 
				{
					// Unhook from the column filters before removing them.
					//
					// SSP 8/7/03 UWG2223
					//
					//for ( int j = this.List.Count; j >= columnCount; j-- )
					for ( int j = this.List.Count - 1; j >= columnCount; j-- )
					{
						Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter =
							this.List[j] as Infragistics.Win.UltraWinGrid.ColumnFilter;

						if ( null != columnFilter )
							columnFilter.SubObjectPropChanged -= this.SubObjectPropChangeHandler;					
					}

					this.List.RemoveRange( columnCount,
						this.List.Count - columnCount ); 
				}
				else
				{
					// append empty slots into the collection to handle
					// every column.
					//
					// Note: we lazily create the column filter in case no one
					//       asks for them.
					//
					while ( columnCount > this.List.Count ) 
					{
						this.List.Add( null );
					}
				}

				// JJD 10/17/01
				// Cache the version number of the columns collection
				//
				this.columnsCollectionVersion = this.Band.ColumnsVersion;
			}
			finally
			{
				this.inSynchronizeWithColumnsColection = false;
			}
		}

		#endregion //SynchronizeWithColumnsColection
	
		#region IndexOf

		/// <summary>
		/// Returns the index of the item in the collection that has the
		/// passed in key or -1 if key not found.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <returns>The index of the item in the collection that has the passed in key, or -1
        /// if no item is found.</returns>
		public override int IndexOf( String key ) 
		{
			// get the index from the columns collection
			//
			// SSP 7/17/02 UWG1367
			// Check for band being null because during deserialization it may be null.
			//
			if ( null != this.Band )
				return this.Band.Columns.IndexOf( key );

			return base.IndexOf( key );
		}

		#endregion //IndexOf

		#region GetItem

		/// <summary>
		/// Returns the cell of the columns that has the key
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <returns>The item in the collection that has the passed in key.</returns>
		protected override IKeyedSubObject GetItem( String key ) 
		{            
			// Syncronize with band's column's collection.
			//
			this.SynchronizeWithColumnsColection( );

			int index = this.IndexOf( key );

			if ( index < 0 )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_106"), Shared.SR.GetString("LE_ArgumentException_320") );
 
			// Call GetObject below so that if the cell has not
			// been created it will be created now
			//
			return (IKeyedSubObject)this.GetItem( index );
		}

		/// <summary>
		/// Returns the cell at the specified index
		/// </summary>
        /// <param name="index">Index of the object to retrieve.</param>
        /// <returns>The object at the index</returns>
		public override object GetItem( int index )
		{
			// Syncronize with band's column's collection.
			//
			this.SynchronizeWithColumnsColection( );

			Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter = 
				(Infragistics.Win.UltraWinGrid.ColumnFilter)this.List[ index ];

			// if the slot for this cell is null then create it now
			//
			if ( null == columnFilter )
			{
				columnFilter = new Infragistics.Win.UltraWinGrid.ColumnFilter( this.Band.Columns[ index ] );

				// Hook into the column filter's SubObjectPropChanged event
				//
				columnFilter.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				
				this.List[ index ] = columnFilter;
			}

			return columnFilter;
		}
		
		#endregion // GetItem

		#region Count

		// SSP 7/16/02
		// Overrode the Count property.
		//
		/// <summary>
		/// Overridden. Returns the count.
		/// </summary>
		public override int Count
		{
			get
			{
				// Syncronize with band's column's collection.
				//
				this.SynchronizeWithColumnsColection( );

				return base.Count;
			}
		}

		#endregion // Count

		#region Indexers

		/// <summary>
		/// indexer
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColumnFilter this[ int index ] 
		{
			get
			{
				// Syncronize with band's column's collection.
				//
				this.SynchronizeWithColumnsColection();

				return (Infragistics.Win.UltraWinGrid.ColumnFilter)this.GetItem( index );
			}
		}

		/// <summary>
		/// indexer (by string key)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColumnFilter this[ String columnKey ] 
		{
			get
			{
				// Syncronize with band's column's collection.
				//
				this.SynchronizeWithColumnsColection( );

				return (Infragistics.Win.UltraWinGrid.ColumnFilter)this.GetItem( columnKey );
			}
		}

       
		/// <summary>
		/// indexer (by column)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.ColumnFilter this[ UltraGridColumn column ] 
		{
			get
			{
				// Syncronize with band's column's collection.
				//
				this.SynchronizeWithColumnsColection();

				// JJD 12/04/01 - UWG837
				// Use nthe column's index instead of its Key to
				// get the cell since it may not have a key.
				//
				//                return (UltraGridCell)this.GetItem( column.Key );
				return (Infragistics.Win.UltraWinGrid.ColumnFilter)this.GetItem( column.Index );
			}
		}
		
		#endregion // Indexers

		#region HasColumnFilter

		internal bool HasColumnFilter( int index )
		{
			// Syncronize with band's column's collection.
			//
			this.SynchronizeWithColumnsColection();

			return null != this.List[ index ];
		}

		internal bool HasColumnFilter( string columnKey )
		{
			// Syncronize with band's column's collection.
			//
			this.SynchronizeWithColumnsColection();

			int index = this.IndexOf( columnKey );

			if ( index < 0 )
				return false;

			return null != this.List[ index ];
		}


		internal bool HasColumnFilter( UltraGridColumn column )
		{
			int columnIndex = this.Band.Columns.IndexOf( column );

			Debug.Assert( columnIndex >= 0, "Column does not belong to the band associated with this ColumnFiltersCollection." );

			if ( columnIndex >= 0 )
				return this.HasColumnFilter( columnIndex );

			return false;
		}

		/// <summary>
		/// Clears filters associated with all the columns.
		/// </summary>
		public void ClearAllFilters( )
		{
			for ( int i = 0; i < this.List.Count; i++ )
			{
				Infragistics.Win.UltraWinGrid.ColumnFilter columnFilter =
					this.List[i] as Infragistics.Win.UltraWinGrid.ColumnFilter;

				if ( null != columnFilter )
				{
					columnFilter.ClearFilterConditions( );
				}
			}
		}

		#endregion // HasColumnFilter

		#region OnSubObjectPropChanged

		/// <summary>
		/// Called when a property has changed
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// Notify the listeners that ColumnFilters have changed.
			//
			this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ColumnFilters );
		}

		#endregion // OnSubObjectPropChanged

		#region DoesRowPassFilters

		internal bool DoesRowPassFilters( UltraGridRow row )
		{
			return this.DoesRowPassFilters( row, null );
		}

		// SSP 9/4/02 UWG1568
		// Added an overload that takes in ignoreFiltersForColumn parameter.
		//
		internal bool DoesRowPassFilters( UltraGridRow row, UltraGridColumn ignoreFiltersForColumn )
		{
			UltraGridBand band = this.Band;

			Debug.Assert( null != band, "Null band !" );

			if ( null == band )
				return true;

			Debug.Assert( row.Band == band, "Row must belong to the same band as the column filters collection." );

			if ( row.Band != band )
				return true;

			// Syncronize with band's columns collection first.
			//
			this.SynchronizeWithColumnsColection( );

			// SSP 3/9/05 - Added LogicalOperator to ColumnFiltersCollection.
			//
			bool pass = true;

			for ( int i = 0; i < this.Count; i++ )
			{
				if ( this.HasColumnFilter( i ) )
				{
					ColumnFilter columnFilter = this[i];

					// SSP 9/4/02 UWG1568
					// Added ignoreFiltersForColumn parameter to the method. Ignore column filters for 
					// passed in ignoreFiltersForColumn column.
					// 
					if ( columnFilter.Column == ignoreFiltersForColumn )
						continue;

					// If the row does not pass the filter, return false.
					//
					// SSP 3/9/05 - Added LogicalOperator to ColumnFiltersCollection.
					//
					// --------------------------------------------------------------------
					//if ( !columnFilter.DoesRowPassFilters( row ) )
					//	return false;
					if ( columnFilter.HasFilterConditions )
					{
						pass = columnFilter.DoesRowPassFilters( row );
						if ( ! pass && FilterLogicalOperator.And == this.LogicalOperator
							|| pass && FilterLogicalOperator.Or == this.LogicalOperator )
							break;
					}
					// --------------------------------------------------------------------
				}
			}

            //  BF 2/28/08  FR09238 - AutoCompleteMode
            //
            //  Apply the AutoCompleteMode filter condition, as a logical AND operation,
            //  since we want to filter out rows that do not begin with the text in the
            //  edit portion.
            //
            UltraGridLayout layout = this.band != null ? this.band.Layout : null;
            FilterCondition filterCondition = layout != null ? layout.AutoCompleteFilterCondition : null;
            if ( filterCondition != null )
                pass = pass && filterCondition.MeetsCriteria( row );

			// If the row passed all the filters above, then return true.
			//
			// SSP 3/9/05 - Added LogicalOperator to ColumnFiltersCollection.
			//
			//return true;
			return pass;
		}

		#endregion // DoesRowPassFilters

		#region IsAnyColumnFilterActive

		internal bool IsAnyColumnFilterActive
		{
			get
			{
				for ( int i = 0; i < this.List.Count; i++ )
				{
					ColumnFilter columnFilter = (ColumnFilter)this.List[i];

					if ( null != columnFilter )
					{
						if ( columnFilter.HasFilterConditions )
							return true;
					}
				}

				return false;
			}
		}

		#endregion // IsAnyColumnFilterActive

		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			// Only serialize if there are filters. Accessing this.Count will cause the
			// collection to be created because this collection is lazy-colleciton.
			//
			if ( this.List.Count > 0 )
			{
				for ( int i = 0; i < this.List.Count; i++ )
				{
					// Don't serialize the column filters object that haven't been created
					// yet so use this.List instead of this for accessing these objects
					// because the latter will cause the object to be lazy-created.
					//
					ColumnFilter columnFilter = this.List[i] as ColumnFilter;

					if ( null != columnFilter )
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, i.ToString( ), columnFilter );
						//info.AddValue( i.ToString( ), columnFilter );
					}
				}
			}
		}

		#endregion // ISerializable.GetObjectData

		#region InitializeFrom

		internal void InitializeFrom( ColumnFiltersCollection source, PropertyCategories categories  )
		{
			// If the property categories say no, then don't initialize from
			// the source.
			//
			if ( 0 == ( PropertyCategories.ColumnFilters & categories ) )
				return;

			if ( source.ShouldSerializeTag( ) )
				this.tagValue = source.tagValue;

			this.ClearAllFilters( );

			
			for ( int i = 0; i < this.Count; i++ )
			{
				ColumnFilter columnFilter = this[i];

				for ( int j = 0; j < source.List.Count; j++ )
				{
					ColumnFilter sourceColumnFilter = source.List[j] as ColumnFilter;


					if ( null != sourceColumnFilter && columnFilter.CanInitializeFrom( sourceColumnFilter ) )
					{
						columnFilter.InitializeFrom( sourceColumnFilter );
						break;
					}
				}
			}


			// AS 1/24/06 BR09304
			this.logicalOperator = source.logicalOperator;
		}

		#endregion // InitializeFrom

		#region CopyFrom

		// SSP 12/27/05 BR07564
		// 
		/// <summary>
		/// Initializes columns filters in this collection with the ones from the source column filters collection.
		/// </summary>
		/// <param name="source"></param>
		/// <remarks>
		/// <p class="body">
		/// Columns are matched by their <see cref="UltraGridColumn.Key"/>'s. This method will copy the filter conditions
		/// of columns that exist in both collections. For columns that exist in the destination collection
		/// but not in the source collection, those column�s filter conditions will be cleared. For columns that exist in 
		/// the source collection but not in the destination layout, no action will be taken.
		/// </p>
		/// </remarks>
		public void CopyFrom( ColumnFiltersCollection source )
		{
			this.InitializeFrom( source, PropertyCategories.All );
		}

		#endregion // CopyFrom
		
		#region ShouldSerialize

		// SSP 7/16/02 UWG1367
		//
		internal bool InternalShouldSerialize( )
		{
			// SSP 3/9/05 - Added LogicalOperator to ColumnFiltersCollection.
			//
			//if ( this.List.Count <= 0 )
			//	return false;

			for ( int i = 0; i < this.Count; i++ )
			{
				if ( this[i].ShouldSerialize( ) )
					return true;
			}

			// SSP 3/9/05 - Added LogicalOperator to ColumnFiltersCollection.
			//
			//return false;
			return this.ShouldSerializeLogicalOperator( );
		}
        
		#endregion // ShouldSerialize

		#region LogicalOperator

		// SSP 3/9/05 - Added LogicalOperator to ColumnFiltersCollection.
		//
		/// <summary>
		/// Specifies whether the column filters should be combined using Or or And logical operator.
		/// </summary>
		public FilterLogicalOperator LogicalOperator
		{
			get
			{
				return this.logicalOperator;
			}
			set
			{
				if ( this.logicalOperator != value )
				{
					GridUtils.ValidateEnum( "LogicalOperator", typeof( FilterLogicalOperator ), value );

					this.logicalOperator = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.LogicalOperator );
				}
			}
		}

		#endregion // LogicalOperator

		#region ShouldSerializeLogicalOperator

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeLogicalOperator( )
		{
			return FilterLogicalOperator.And != this.logicalOperator;
		}

		#endregion // ShouldSerializeLogicalOperator

		#region ResetLogicalOperator

		/// <summary>
		/// Resets the property to its default value of And.
		/// </summary>
		public void ResetLogicalOperator( )
		{
			this.LogicalOperator = FilterLogicalOperator.And;
		}

		#endregion // ResetLogicalOperator

	}

}
