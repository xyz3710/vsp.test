#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	///	Button UIElement that represents a <see cref="UltraGridBand"/> and is used to create a new row in the band.
	/// </summary>
	public class AddNewRowButtonUIElement : Infragistics.Win.ButtonUIElement 
	{
		#region Member Variables

		// AS 1/6/04 accessibility
		private AccessibleObject			accessibleObject;

		#endregion //Member Variables

		// SSP 2/7/02 UWG847
		// Commented this out because we have PrimaryContext for holding
		// the band.
		//
		//private UltraGridBand	band;

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>AddNewRowButtonUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="band">Associated <b>Band</b></param>
		//internal AddNewRowButtonUIElement( UIElement parent, UltraGridBand band ) : base( parent )
		public AddNewRowButtonUIElement( UIElement parent, UltraGridBand band ) : base( parent )
		{
			// SSP 2/7/02 UWG847
			// Took out the memer variable for the band. 
			// We should be using the PrimaryContext to hold the band.
			//
			//this.band = band;
			// SSP 5/29/02 UWG1143
			// Use the newly added initialize method instead
			//
			//this.PrimaryContext = band;
			this.Initialize( band );


			//RobA 10/3/01 UWG458
			//Hook into Element Click event to receive left mouse click
			//
			this.ElementClick += new Infragistics.Win.UIElementEventHandler ( this.LeftClick );
		}

		// SSP 5/29/02 UWG1143
		// Added Intialize method
		//
		internal void Initialize( UltraGridBand band )
		{
			this.PrimaryContext = band;
		}
		
		/// <summary>
		/// Overriden to unhook ourselves
		/// </summary>
		override protected void OnDispose()
		{
			//RobA 10/3/01 UWG458
			//unhook from event
			//
			this.ElementClick -= new Infragistics.Win.UIElementEventHandler ( this.LeftClick );
			base.OnDispose();
		}

		// SSP 1/2/02
		// Added this method.
		/// <summary>
		/// Overridden method that check's the band's AllowAddNew
		/// and also if the band has enough context right now to be able to
		/// add a new row in that band.
		/// </summary>
		public override bool Enabled
		{
			get
			{
				// SSP 2/7/02
				//
				//return band.AddNewBoxButtonEnabledResolved;
				// SSP 1/24/03 UWG1945
				// Call the base class implementation which also checks for things like the ancestor
				// being disabled and the control itself being disabled. So for example if the grid 
				// was disabled, it will check that and return appropriately.
				//
				//return this.Band.AddNewBoxButtonEnabledResolved;
				return base.Enabled && this.Band.AddNewBoxButtonEnabledResolved;
			}
		}

		/// <summary>
		/// The UltraGridBand object represents all the rows that occur at a single level of a hierarchical data set. Bands can be expanded or collapsed to display the data in the rows they contain.
		/// </summary>
		/// <remarks>
		/// <p class="body">The UltraGridBand object represents all the records at one level of a hierarchical recordset. Bands are the foundation of hierarchical data in the UltraWinGrid. When bound to a recordset, each band corresponds to a single Command. (A band can also be considered as roughly equivalent to the table or query level of organization within a database.) Although the rows in a band may be visually separated (appearing grouped under the rows of the next higher band in the hierarchy) they are in fact one set of records. In the data hierarchy of the grid, bands come after the grid itself, but before rows and cells.</p>
		/// <p class="body">There is always at least one UltraGridBand present in the UltraWinGrid, even when it is displaying a single-level (flat) recordset. Most of the properties that apply to the control at the topmost (grid) level also apply to the UltraGridBand object, since the band rather than the control is the primary container object for data. There is also broad support for applying different formatting and behavior attributes to individual bands. Since a band is effectively "a grid within a grid" you may want to have bands be markedly different from one another. For example, one band might display column headers and row selectors for each group of records, while another might display only data cells.</p>
		/// <p class="body">Bands can be displayed either horizontally or vertically within the grid, depending on the setting of the <b>ViewStyleBand</b> property. You can also hide entire bands from view by setting the <b>Hidden</b> property of the UltraGridBand object.</p>
		/// </remarks>
		protected Infragistics.Win.UltraWinGrid.UltraGridBand Band 
		{
			get
			{
				// SSP 2/7/02 UWG847
				// Took out the memer variable for the band. 
				// We should be using the PrimaryContext to hold the band.
				//
				//return this.band;
				return (Infragistics.Win.UltraWinGrid.UltraGridBand)this.PrimaryContext;
			}
		}

		/// <summary>
		/// Returns or sets a value that determines the border style of an object.
		/// </summary>
		/// <remarks>
		/// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
		/// </remarks>
		public override Infragistics.Win.UIElementButtonStyle ButtonStyle
		{
			get
			{
				// SSP 3/27/06 - App Styling
				// We have to perform all of the resolution in the ButtonStyleResolve property, 
				// especially because of the app styling now.
				// 
				
				
				UltraGridBand band = this.Band;
				return band.Layout.AddNewBox.GetButtonStyleResolved( band );
				
			}
		}

		#region InitAppearance
		
		
		
		
		
		

		#endregion // InitAppearance

		#region InitControlAppearance

		
		
		
		
        /// <summary>
        /// Resolves the control specific appearances for this button. Default implementation
        /// merges in its Infragistics.Win.ButtonUIElementBase.Appearance. This method
        /// should only merge in the appearance properties exposed by the controls. It
        /// should not merge in any defaults.  The InitAppearance method calls this method
        /// in between the calls that resolve app-style appearance settings. In other
        /// words this method should itself not resolve any app-style appearance settings.
        /// Also note that the InitAppearance method will check UseControlInfo setting
        /// of the app-style and if it's false it will not call this method. Therefore
        /// the overridden implementations do no need to check for UseControlInfo app-style
        /// setting.
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitControlAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			base.InitControlAppearance( ref appData, ref flags );

			this.Band.Layout.AddNewBox.ResolveButtonAppearance( ref appData, ref flags, false );
		}

		#endregion // InitControlAppearance

		#region InitializeDefaultAppearance

		
		
		
		
		/// <summary>
		/// Overridden. Resolves the default appearance.
		/// </summary>
		/// <param name="appData">AppearanceData structure to update</param>
		/// <param name="flags">Appearance properties to resolve</param>
		protected override void InitializeDefaultAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			base.InitializeDefaultAppearance( ref appData, ref flags );

			this.Band.Layout.AddNewBox.ResolveButtonAppearance( ref appData, ref flags, true );
		}

		#endregion // InitializeDefaultAppearance

		// SSP 2/7/02 UWG847
		// Overrode the PositionChildElements so that we can assign HotKeyPrefix
		// to the text element of the button.
		//
		/// <summary>
		/// Overridden. Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// Call the base class' PositionChildEelements first
			//
			base.PositionChildElements( );

			// Get the text element so we can set the HotKeyPrefix on it.
			//
			TextUIElementBase textElem = this.textElement;

			// If this.textElement is not set for some reason, then 
			// try to get the text element by calling GetDescendant.
			//
			if ( null == textElem )
			{
				textElem = (TextUIElementBase)this.GetDescendant( typeof( TextUIElementBase ) );
			}

			Debug.Assert( null != textElem, "Unable to get the text element for the add new button." );

			if ( null != textElem )
			{
				// Set the HotKeyPrefix to show.
				//
				textElem.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.Show;
			}
		}


		// SSP 10/2/01
		// Commented
		

			// SSP 10/2/01
			// ButtonUIElement has built in support for caption, so there
			// is no need to create a separate text ui element. Just 
			// asssign the Text property to caption
			
		
		//}

		//RobA 10/3/01 UWG458 moved this to LeftClick
	

		private void LeftClick( object sender, Infragistics.Win.UIElementEventArgs e )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//Infragistics.Win.UltraWinGrid.UltraGridRow row = null;

			//RobA UWG239 8/23/01
			// SSP 10/2/01
			// Use enabled property instead since we disable the button
			// if AllowAdd is false in the first place. (Enabled is set
			// to false by taking into consideration other factors as well).
			//
			//if ( this.Band.AllowAdd )
			if ( this.Enabled )
				// SSP 1/2/02
				// Call the newly added internalAddNew method instead of AddNew
				//
				//row = this.band.AddNew();				
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//row = this.Band.InternalAddNew( );
				this.Band.InternalAddNew();
		}

		
		
		/// <summary>
		/// Return true if this element wants to be notified when the mouse hovers over it. This property is read-only.
		/// </summary>
		/// <remarks>Returns true since add new buttons need MouseHover notifications for showing the tooltips.</remarks>
		protected override bool WantsMouseHoverNotification
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// overridden method called whenever the mouse hovers on the element
		/// it displays a tooltip for the AddNewRowButton
		/// </summary>
		protected override void OnMouseHover( )
		{	
			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if ( this.Band.AddButtonToolTipText != null )
			{
				Infragistics.Win.ToolTip toolTip = grid.TooltipTool;

				if ( null != toolTip )
				{
					toolTip.SetMargin( 1, 1, 1, 1 );
					toolTip.ToolTipText = this.Band.AddButtonToolTipText;
					toolTip.Show();
				}
			}
		}

		/// <summary>
		/// Hide tooltip
		/// </summary>
		protected override void OnMouseLeave()
		{
			// JJD 1/7/02
			// Call the base implementation in case themes are supported 
			//
			base.OnMouseLeave();

			UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

			if ( null == grid )
				return;

			// Don't create TooltipTool if we don't have one 
			// already
			if ( grid.HasTooltipTool )
			{
				Infragistics.Win.ToolTip toolTip = grid.TooltipTool;

				if ( null != toolTip )
				{
					toolTip.Hide();
				}
			}
		}

		// AS 1/6/04 Accessibility
		#region IsAccessibleElement
		/// <summary>
		/// Indicates if the element supports accessibility.
		/// </summary>
		public override bool IsAccessibleElement
		{
			get { return true; }
		}
		#endregion //IsAccessibleElement

		#region AccessibilityInstance
		/// <summary>
		/// Returns the accessible object associated with the element.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note</b> Derived elements that plan to return an accessible object must override 
		/// the <see cref="IsAccessibleElement"/> member.</p>
		/// </remarks>
		public override AccessibleObject AccessibilityInstance
		{
			get 
			{ 
				if (this.accessibleObject == null)
					this.accessibleObject = this.Band.Layout.Grid.CreateAccessibilityInstance(this);

				return this.accessibleObject; 
			}
		}
		#endregion //AccessibilityInstance

		#region AddNewButtonAccessibleObject
		/// <summary>
		/// Accessible object representing a <see cref="AddNewRowButtonUIElement"/> in the <see cref="AddNewBox"/>
		/// </summary>
		public class AddNewButtonAccessibleObject : ButtonUIElementBase.ButtonElementAccessibleObject
		{
			#region Constructor
			/// <summary>
			/// Initializes a new <see cref="AddNewButtonAccessibleObject"/>
			/// </summary>
			/// <param name="element">Associated element</param>
			public AddNewButtonAccessibleObject(AddNewRowButtonUIElement element) : base(element)
			{
			}
			#endregion //Constructor

			#region Properties

			private AddNewRowButtonUIElement Button
			{
				get { return this.UIElement as AddNewRowButtonUIElement; }
			}

			private UltraGridBand Band
			{
				get { return this.Button.Band; }
			}

			#endregion //Properties

			#region Base class overrides

			#region KeyboardShortcut
			/// <summary>
			/// Gets the shortcut key or access key for the accessible object
			/// </summary>
			public override string KeyboardShortcut
			{
				get
				{
					char ch = Infragistics.Win.Utilities.GetFirstMnemonicChar(this.Band.AddButtonCaptionResolved);

					if (ch != '\0')
						return new String(ch,1);

					return null;
				}
			}
			#endregion //KeyboardShortcut

			#region Name
			/// <summary>
			/// Returns the name of the accessible object.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if (name == null)
						name = Infragistics.Win.Utilities.StripMnemonics(this.Band.AddButtonCaptionResolved);

					return name;
				}
				set
				{
					base.Name = value;
				}
			}
			#endregion //Name

			#endregion //Base class overrides
		}
		#endregion //AddNewButtonAccessibleObject

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
		//
		#region DrawTheme

		/// <summary>
        /// Renders the element using the System theme.
		/// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
        /// <returns>True if the element was able to be rendered using the system themes.</returns>
		protected override bool DrawTheme(ref Infragistics.Win.UIElementDrawParams drawParams)
		{            
			if( this.ButtonStyle == UIElementButtonStyle.Button3D )
				return base.DrawTheme( ref drawParams );

			return false;
		}

		#endregion // DrawTheme

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Band, StyleUtils.Role.AddNewBoxButton );
			}
		}

		#endregion // UIRole
	}
	
}
