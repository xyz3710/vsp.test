#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Diagnostics;
using System.Collections;
using Infragistics.Win;
using Infragistics.Shared;
using System.Collections.Generic;

namespace Infragistics.Win.UltraWinGrid
{
	// JAS v5.2 GroupBy Break Behavior 5/2/05
	//
	internal class GroupByEvaluatorFactory
	{
		#region CreateEvaluator

		internal static IGroupByEvaluator CreateEvaluator( UltraGridColumn columnBeingGrouped )
		{			
			if( columnBeingGrouped == null )
				return null;

			Debug.Assert( columnBeingGrouped.GroupByEvaluator == null, "CreateEvaluator should not be called if the column has been assigned a custom evaluator already!" );
			
			GroupByMode mode = columnBeingGrouped.GroupByModeResolved;

			// If the GroupByMode resolves to 'Value' then just return null so that the grid's
			// default evaluation logic will be used.
			//
			if( mode == GroupByMode.Value )
				return null;

			IGroupByEvaluator evaluator;
		
			switch( mode )
			{
				case GroupByMode.Date:
					evaluator = new DateEvaluator();
					break;

				case GroupByMode.FirstCharacter:
					evaluator = new FirstCharactersEvaluator( 1, columnBeingGrouped.SortComparisonTypeResolved );
					break;

				case GroupByMode.First2Characters:
					evaluator = new FirstCharactersEvaluator( 2, columnBeingGrouped.SortComparisonTypeResolved );
					break;

				case GroupByMode.First3Characters:
					evaluator = new FirstCharactersEvaluator( 3, columnBeingGrouped.SortComparisonTypeResolved );
					break;

				case GroupByMode.First4Characters:
					evaluator = new FirstCharactersEvaluator( 4, columnBeingGrouped.SortComparisonTypeResolved );
					break;

				case GroupByMode.Hour:
					evaluator = new HourEvaluator();
					break;

				case GroupByMode.Minute:
					evaluator = new MinuteEvaluator();
					break;

				case GroupByMode.Month:
					evaluator = new MonthEvaluator();
					break;

				case GroupByMode.OutlookDate:
					evaluator = new OutlookDateEvaluator();
					break;

				case GroupByMode.Quarter:
					evaluator = new QuarterEvaluator();
					break;

				case GroupByMode.Second:
					evaluator = new SecondEvaluator();
					break;

				case GroupByMode.Text:
					evaluator = new CellTextEvaluator( columnBeingGrouped.SortComparisonTypeResolved );
					break;

				case GroupByMode.Year:
					evaluator = new YearEvaluator();
					break;

				default:
					Debug.Fail( "Unrecognized GroupByMode value: " + mode.ToString() );
					evaluator = null;
					break;
			}

			return evaluator;
		}

		#endregion // CreateEvaluator
			
		#region Evaluator Classes

			#region CellTextEvaluator

		// Evaluates the text of a cell, not the value of a cell.
		//
		private class CellTextEvaluator : IGroupByEvaluator
		{
			#region Data

			private readonly bool isCaseInsensitive;

			#endregion // Data

			#region CellTextEvaluator

			internal CellTextEvaluator( SortComparisonType sortComparisonType )
			{
				this.isCaseInsensitive = sortComparisonType == SortComparisonType.CaseInsensitive;
			}

			#endregion // CellTextEvaluator

			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				string text = row.GetCellText( groupByRow.Column );
				return text == null ? "" : text;
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{				
				string cellText = row.GetCellText( groupByRow.Column );

				cellText = cellText == null ? "" : cellText; 
             
				return String.Compare( groupByRow.Value.ToString(), cellText, this.isCaseInsensitive ) == 0; 
			}

			#endregion // DoesGroupContainRow			
		}

			#endregion // CellValueEvaluator

			#region DateEvaluator

		// Evaluates dates (not times) for columns of type DateTime.
		//
		private class DateEvaluator : IGroupByEvaluator
		{
			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				return DateEvaluator.GetGroupByValueHelper( groupByRow, row );
			}

			#endregion // GetGroupByValue

			#region GetGroupByValueHelper

			internal static DateTime GetGroupByValueHelper( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				return groupByRow.Value is DateTime ? (DateTime)groupByRow.Value : DateTime.MinValue;
			}

			#endregion // GetGroupByValueHelper

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{             
				DateTime dateInCell, dateInGrp;
				if( DateEvaluator.DoesGroupContainRowHelper( groupByRow, row, out dateInGrp, out dateInCell ) )
					return true;

				return dateInCell.Date == dateInGrp.Date;		
			}

			#endregion // DoesGroupContainRow	
		
			#region DoesGroupContainRowHelper

			internal static bool DoesGroupContainRowHelper( UltraGridGroupByRow groupByRow, UltraGridRow row, out DateTime dateInGroup, out DateTime dateInCell )
			{
				object cellValue = row.GetCellValue( groupByRow.Column );

				dateInGroup = (DateTime)groupByRow.Value;

				// If the cell's value is not a DateTime then the GroupBy row's value will be
				// DateTime.MinValue because the GetGroupByValue method says so.
				//
				if( ! (cellValue is DateTime) )
				{
					Debug.Assert( (DateTime)groupByRow.Value == DateTime.MinValue, "groupByRow.Value should be DateTime.MinValue when the cell's value is null, but it is not." );
					dateInCell = dateInGroup;
					return true;
				}

				dateInCell = (DateTime)cellValue;			
				
				return false;
			}

			#endregion // DoesGroupContainRowHelper
		}

			#endregion // DateEvaluator

			#region FirstCharactersEvaluator

		// Evaluates the first N characters of a cell's value.
		//
		// SSP 11/11/05 BR07685
		// We need to implement the new IGroupByEvaluatorEx interface because we need
		// to make sure that the sorting logic is consistent with the grouping logic.
		// This is because of how the .NET sorts case-sensitive text.
		// 
		//private class FirstCharactersEvaluator : IGroupByEvaluator
		private class FirstCharactersEvaluator : IGroupByEvaluatorEx
		{
			#region Data

			private readonly int  numChars;
			private readonly bool isCaseInsensitive;

			#endregion // Data

			#region Constructor

			internal FirstCharactersEvaluator( int numChars, SortComparisonType sortComparisonType )
			{
				Debug.Assert( numChars > 0, "'numChars' should not be less than or equal to zero." );

				this.numChars = numChars;
				this.isCaseInsensitive = sortComparisonType == SortComparisonType.CaseInsensitive;
			}

			#endregion // Constructor

			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				return this.GetFirstCharactersOfCellText( groupByRow, row ); 
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{             
				string groupByVal   = groupByRow.Value == null ? "" : groupByRow.Value.ToString();
				string firstLetters = this.GetFirstCharactersOfCellText( groupByRow, row );

				return String.Compare( groupByVal, firstLetters, this.isCaseInsensitive ) == 0; 
			}

			#endregion // DoesGroupContainRow			

			#region GetFirstCharactersOfCellText

			// Helper method.
			//
			private string GetFirstCharactersOfCellText( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				// SSP 11/11/05 BR07685
				// 
				return this.GetFirstCharactersOfText( row.GetCellText( groupByRow.Column ) );

                
			}

			#endregion // GetFirstCharactersOfCellText

			#region GetFirstCharactersOfText

			// SSP 11/11/05 BR07685
			// 
			private string GetFirstCharactersOfText( string text )
			{
				if ( null == text )
					text = string.Empty;
				else if ( this.numChars < text.Length )
					text = text.Substring( 0, this.numChars );

				return text;
			}

			#endregion // GetFirstCharactersOfText

			#region IGroupByEvaluatorEx.Compare

			// SSP 11/11/05 BR07685
			// We need to implement the new IGroupByEvaluatorEx interface because we need
			// to make sure that the sorting logic is consistent with the grouping logic.
			// This is because of how the .NET sorts case-sensitive text.
			// 
			int IGroupByEvaluatorEx.Compare( UltraGridCell xxCell, UltraGridCell yyCell )
			{
				string xx = this.GetFirstCharactersOfText( xxCell.Text );
				string yy = this.GetFirstCharactersOfText( yyCell.Text );

				return String.Compare( xx, yy, this.isCaseInsensitive );
			}

			#endregion // IGroupByEvaluatorEx.Compare
		}

			#endregion // FirstCharactersEvaluator

			#region HourEvaluator

		// Evaluates the Date and Hour portions of a DateTime.
		//
		private class HourEvaluator : IGroupByEvaluator
		{
			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				DateTime dt = DateEvaluator.GetGroupByValueHelper( groupByRow, row );

				// Set the Minute and Second portions to 0 for every group so that the arbitrary Minute 
				// and Second values found in cells do not get displayed in the group descriptions.
				//
				return new DateTime( dt.Year, dt.Month, dt.Day, dt.Hour, 0, 0 );
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{             
				DateTime dateInCell, dateInGrp;
				if( DateEvaluator.DoesGroupContainRowHelper( groupByRow, row, out dateInGrp, out dateInCell ) )
					return true;

				return	dateInCell.Date == dateInGrp.Date &&
					dateInCell.Hour == dateInGrp.Hour;
			}

			#endregion // DoesGroupContainRow			
		}

			#endregion // HourEvaluator

			#region MinuteEvaluator

		// Evaluates the Date, Hour, and Minute portions of a DateTime.
		//
		private class MinuteEvaluator : IGroupByEvaluator
		{
			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				DateTime dt = DateEvaluator.GetGroupByValueHelper( groupByRow, row );

				// Set the Second portion to 0 for every group so that the arbitrary Second
				// values found in cells do not get displayed in the group descriptions.
				//
				return new DateTime( dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0 );	
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{             
				DateTime dateInCell, dateInGrp;
				if( DateEvaluator.DoesGroupContainRowHelper( groupByRow, row, out dateInGrp, out dateInCell ) )
					return true;

				return	dateInCell.Date   == dateInGrp.Date &&
					dateInCell.Hour	  == dateInGrp.Hour &&
					dateInCell.Minute == dateInGrp.Minute;
			}

			#endregion // DoesGroupContainRow			
		}

			#endregion // MinuteEvaluator

			#region MonthEvaluator

		// Evaluates the Year and Month portion of a DateTime.
		//
		private class MonthEvaluator : IGroupByEvaluator
		{
			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				DateTime dt = DateEvaluator.GetGroupByValueHelper( groupByRow, row );

				// Set the Day portion to 1 for every group so that the arbitrary Day values
				// found in cells do not get displayed in the group descriptions.
				//
				return new DateTime( dt.Year, dt.Month, 1 );
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{             
				DateTime dateInCell, dateInGrp;
				if( DateEvaluator.DoesGroupContainRowHelper( groupByRow, row, out dateInGrp, out dateInCell ) )
					return true;

				return	dateInCell.Year  == dateInGrp.Year &&
					dateInCell.Month == dateInGrp.Month;
			}

			#endregion // DoesGroupContainRow			
		}

			#endregion // MonthEvaluator

			#region OutlookDateEvaluator

		// Performs Outlook 2003-style grouping of dates.
		//
		private class OutlookDateEvaluator : IGroupByEvaluator
		{
			#region Data

			private readonly string BEYOND_NEXT_MONTH;
			private readonly string NEXT_MONTH;
			private readonly string LATER_THIS_MONTH;
			private readonly string THREE_WEEKS_AWAY;
			private readonly string TWO_WEEKS_AWAY;
			private readonly string NEXT_WEEK;
			private readonly string	TODAY;
			private readonly string	YESTERDAY;
			private readonly string SUNDAY;
			private readonly string MONDAY;
			private readonly string TUESDAY;
			private readonly string WEDNESDAY;
			private readonly string THURSDAY;
			private readonly string FRIDAY;
			private readonly string SATURDAY;
			private readonly string LAST_WEEK;
			private readonly string TWO_WEEKS_AGO;
			private readonly string THREE_WEEKS_AGO;
			private readonly string EARLIER_THIS_MONTH;
			private readonly string LAST_MONTH;
			private readonly string OLDER;
			private readonly string NONE;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//private ArrayList dateInfos;
			private List<DateInfo> dateInfos;

			private DateTime  lastUpdated;

			private Hashtable dayMap;

			#endregion // Data

			#region Constructor

			internal OutlookDateEvaluator()
			{
				// Get a reference to a Sunday.
				//
				DateTime sunday = DateTime.Today;
				while( sunday.DayOfWeek != DayOfWeek.Sunday )
					sunday = sunday.AddDays( 1 );

				// Load day names using a format string stored in the resource file.
				//
				SUNDAY				= SR.GetString( "Outlook_GroupByMode_Description_DayOfWeekFormatString", sunday );
				MONDAY				= SR.GetString( "Outlook_GroupByMode_Description_DayOfWeekFormatString", sunday.AddDays(1) );
				TUESDAY				= SR.GetString( "Outlook_GroupByMode_Description_DayOfWeekFormatString", sunday.AddDays(2) );
				WEDNESDAY			= SR.GetString( "Outlook_GroupByMode_Description_DayOfWeekFormatString", sunday.AddDays(3) );
				THURSDAY			= SR.GetString( "Outlook_GroupByMode_Description_DayOfWeekFormatString", sunday.AddDays(4) );
				FRIDAY				= SR.GetString( "Outlook_GroupByMode_Description_DayOfWeekFormatString", sunday.AddDays(5) );
				SATURDAY			= SR.GetString( "Outlook_GroupByMode_Description_DayOfWeekFormatString", sunday.AddDays(6) );

				BEYOND_NEXT_MONTH	= SR.GetString("Outlook_GroupByMode_Description_BeyondNextMonth");
				NEXT_MONTH			= SR.GetString("Outlook_GroupByMode_Description_NextMonth");
				LATER_THIS_MONTH	= SR.GetString("Outlook_GroupByMode_Description_LaterThisMonth");
				THREE_WEEKS_AWAY	= SR.GetString("Outlook_GroupByMode_Description_ThreeWeeksAway");
				TWO_WEEKS_AWAY		= SR.GetString("Outlook_GroupByMode_Description_TwoWeeksAway");
				NEXT_WEEK			= SR.GetString("Outlook_GroupByMode_Description_NextWeek");
				TODAY				= SR.GetString("Outlook_GroupByMode_Description_Today");
				YESTERDAY			= SR.GetString("Outlook_GroupByMode_Description_Yesterday");
				LAST_WEEK			= SR.GetString("Outlook_GroupByMode_Description_LastWeek");
				TWO_WEEKS_AGO		= SR.GetString("Outlook_GroupByMode_Description_TwoWeeksAgo");
				THREE_WEEKS_AGO		= SR.GetString("Outlook_GroupByMode_Description_ThreeWeeksAgo");
				EARLIER_THIS_MONTH	= SR.GetString("Outlook_GroupByMode_Description_EarlierThisMonth");
				LAST_MONTH			= SR.GetString("Outlook_GroupByMode_Description_LastMonth");
				OLDER				= SR.GetString("Outlook_GroupByMode_Description_Older");
				NONE				= SR.GetString("Outlook_GroupByMode_Description_None");
			}

			#endregion // Constructor

			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				object   cellValue  = row.GetCellValue( groupByRow.Column );
				string   dateLabel  = String.Empty;
				DateTime targetDate = DateTime.MinValue;

				if( cellValue == null || cellValue is DBNull )
				{
					dateLabel = this.NONE;
				}
				else if( cellValue is DateTime )
				{
					targetDate = (DateTime)cellValue;
					dateLabel  = this.GetDateLabel( targetDate );					
				}

				// Set the groupby row's description to the appropriate date label.
				// We use that description in the DoesGroupContainRow method to determine
				// which rows belong in that group.
				//
				groupByRow.Description = dateLabel;

				return targetDate;
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{             
				object cellValue = row.GetCellValue( groupByRow.Column );

				string currentDateLabel = groupByRow.Description;

				// Empty cells fall into the 'None' category.
				//
				if( cellValue == null || cellValue is DBNull )
					return currentDateLabel == this.NONE;

				if( ! (cellValue is DateTime) )
					return true;

				string dateLabel = this.GetDateLabel( (DateTime)cellValue );				

				// Since the groupby row's Value property must store a DateTime object if the column's DataType
				// is DateTime, we determine if the current value is in the group via comparing the Description
				// of the group against the Description (a.k.a "date label") associated with the current value.
				//
				return dateLabel == currentDateLabel;
			}

			#endregion // DoesGroupContainRow			

			#region GetDateLabel

			private string GetDateLabel( DateTime targetDate )
			{
				// If the list of date information is stale or empty, rebuild it.
				//
				if( this.lastUpdated.Date < DateTime.Today )
					this.InitDateInfos();

				foreach( DateInfo info in this.dateInfos )
					if( info.Contains( targetDate ) )
						return info.DateLabel;

				Debug.Fail( "Could not find a date range for the target date: " + targetDate.ToString() );

				return String.Empty;
			}

			#endregion // GetDateLabel

			#region InitDateInfos

			private void InitDateInfos()
			{			
				const int daysPerWeek	 = 7;
				DayOfWeek firstDayOfWeek = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
					
				DateTime today		  = DateTime.Today;
				DateTime startOfMonth = new DateTime( today.Year, today.Month, 1 );
				DateTime startOfWeek  = today;

				// Travel back in time to the first day of the week.
				//
				while( startOfWeek.DayOfWeek != firstDayOfWeek )
					startOfWeek = startOfWeek.AddDays( -1 );

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//this.dateInfos = new ArrayList();
				this.dateInfos = new List<DateInfo>();

				DateTime begin, end;

				// SSP 11/1/05 BR07446
				// Moved the present week logic from between the past and the future to here.
				// The reason for this is that "The Present Week" gets the highest priority
				// when matching dates.
				// 
				#region The Present Week

				DateTime currentDay = startOfWeek;

				for( int i = 0; i < daysPerWeek; ++i )
				{
					bool isToday = currentDay.DayOfWeek == today.DayOfWeek;
					bool isYesterday = 
						(int)currentDay.DayOfWeek + 1  == (int)today.DayOfWeek 
						|| // If the week does not start on Sunday, then Sunday follows Saturday in the same week.
						currentDay.DayOfWeek == DayOfWeek.Sunday && today.DayOfWeek == DayOfWeek.Saturday;

					string label;

					if( isToday  )
						label = this.TODAY;
					else if( isYesterday )
						label = this.YESTERDAY;
					else
						label = this.DayMap[currentDay.DayOfWeek] as string;

					Debug.Assert( label != null && label.Length > 0, "The label value was not determined." );

					this.dateInfos.Add( new DateInfo( currentDay, currentDay, label ) );

					// Bump the current day up so that the next iteration through the loop 
					// will calculate the next day.
					//
					currentDay = currentDay.AddDays( 1 );

					// If the day that we just added was the last day of the week,
					// we must break out of the loop.
					//
					if( currentDay.DayOfWeek == firstDayOfWeek )
						break;
				}

				#endregion // The Present Week

				#region The Past

				// OLDER THAN LAST MONTH
				//
				begin = DateTime.MinValue;
				end   = startOfMonth.AddMonths( -1 ).AddDays( -1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.OLDER ) );

				// THREE WEEKS AGO
				//
				begin = startOfWeek.AddDays( -(daysPerWeek * 3) );
				end	  = begin.AddDays( daysPerWeek - 1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.THREE_WEEKS_AGO ) );

				// TWO WEEKS AGO
				//
				begin = startOfWeek.AddDays( -(daysPerWeek * 2) );
				end	  = begin.AddDays( daysPerWeek - 1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.TWO_WEEKS_AGO ) );

				// LAST WEEK
				//
				begin = startOfWeek.AddDays( -daysPerWeek );
				end	  = begin.AddDays( daysPerWeek - 1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.LAST_WEEK ) );

				// EARLIER THIS MONTH 
				// This catches all days which slip between the cracks of "3 weeks ago" and "last month", if any.
				// This entry must come after "N Weeks Ago" entries for this to work properly.
				//
				begin = startOfMonth;
				end   = startOfWeek.AddDays( -1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.EARLIER_THIS_MONTH ) );

				// LAST MONTH - Note: Add this after the "N Weeks Ago" and "Last Week" entries
				// because the collection of DateInfo objects is read from beginning to end, and
				// the "Last Month" entry does not account for a date existing in the "N Weeks Ago"
				// categories.
				//
				begin = startOfMonth.AddMonths( -1 );
				end   = startOfMonth.AddDays( -1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.LAST_MONTH ) );

				#endregion // The Past

				// SSP 11/1/05 BR07446
				// Moved the present week logic from here to above, before the "The Past" logic.
				// The reason for this is that "The Present Week" gets the highest priority
				// when matching dates.
				//
				#region The Present Week - Commented Out

				

				#endregion // The Present Week - Commented Out

				#region The Future

				// NEXT WEEK
				//
				begin = startOfWeek.AddDays( daysPerWeek );
				end	  = begin.AddDays( daysPerWeek - 1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.NEXT_WEEK ) );

				// TWO WEEKS AWAY
				//
				begin = startOfWeek.AddDays( daysPerWeek * 2 );
				end	  = begin.AddDays( daysPerWeek - 1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.TWO_WEEKS_AWAY ) );

				// THREE WEEKS AWAY
				//
				begin = startOfWeek.AddDays( daysPerWeek * 3 );
				end	  = begin.AddDays( daysPerWeek - 1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.THREE_WEEKS_AWAY ) );

				// LATER THIS MONTH 
				// This catches all days which slip between the cracks of "3 weeks away" and "next month", if any.
				// This entry must come after the "N Weeks Away" entries for this to work properly.
				//
				begin = startOfMonth;
				end   = startOfMonth.AddMonths( 1 ).AddDays( -1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.LATER_THIS_MONTH ) );

				// NEXT MONTH
				// This entry must come after the "N Weeks Away" entries for this to work properly.
				//
				begin = startOfMonth.AddMonths( 1 );
				end   = startOfMonth.AddMonths( 2 ).AddDays( -1 );
				this.dateInfos.Add( new DateInfo( begin, end, this.NEXT_MONTH ) );

				// BEYOND NEXT MONTH
				//
				begin = startOfMonth.AddMonths( 2 );
				end   = DateTime.MaxValue;
				this.dateInfos.Add( new DateInfo( begin, end, this.BEYOND_NEXT_MONTH ) );

				#endregion // The Future

				// Cache the date that this list of DateInfo was last updated.
				//
				this.lastUpdated = today;
			}

			#endregion // InitDateInfos

			#region DayMap

			private Hashtable DayMap
			{
				get
				{
					if( this.dayMap == null )
					{
						Debug.Assert( this.SUNDAY != null, "DayMap should not be accessed before the day name strings have been given values." );

						this.dayMap = new Hashtable( 7 );
						this.dayMap.Add( DayOfWeek.Sunday,    this.SUNDAY );
						this.dayMap.Add( DayOfWeek.Monday,    this.MONDAY );
						this.dayMap.Add( DayOfWeek.Tuesday,   this.TUESDAY );
						this.dayMap.Add( DayOfWeek.Wednesday, this.WEDNESDAY );
						this.dayMap.Add( DayOfWeek.Thursday,  this.THURSDAY );
						this.dayMap.Add( DayOfWeek.Friday,    this.FRIDAY );
						this.dayMap.Add( DayOfWeek.Saturday,  this.SATURDAY );
					}

					return this.dayMap;
				}
			}

			#endregion // DayMap

			#region DateInfo class

			private class DateInfo
			{
				#region Data

				private readonly DateTime beginDate;
				private readonly DateTime endDate;
				private readonly string   dateLabel;

				#endregion // Data

				#region Ctor

				internal DateInfo( DateTime begin, DateTime end, string label )
				{
					this.beginDate = begin;
					this.endDate   = end;
					this.dateLabel = label;
				}

				#endregion // Ctor

				#region Contains

				internal bool Contains( DateTime date )
				{
					return  this.beginDate.Date <= date.Date 
						&&
						date.Date <= this.endDate.Date;
				}

				#endregion // Contains

				#region DateLabel

				internal string DateLabel
				{
					get { return this.dateLabel; }
				}

				#endregion // DateLabel
			}

			#endregion // DateInfo class
		}

			#endregion // OutlookDateEvaluator

			#region QuarterEvaluator

		// Groups dates based on which quarter of which year they fall into.
		//
		private class QuarterEvaluator : IGroupByEvaluator
		{
			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				DateTime dt = DateEvaluator.GetGroupByValueHelper( groupByRow, row );

				// In case this evaluator is being used on a column which is not of type DateTime
				// then just return the dummy value MinValue.
				//
				if( dt == DateTime.MinValue )
					return dt;

				// Determine the month which represents the quarter that the date falls into.
				//
				int month = dt.Month - (dt.Month - 1) % 3;

				// Set the Day portion to 1 for every group so that the arbitrary Day values
				// found in cells do not get displayed in the group descriptions.
				//
				return new DateTime( dt.Year, month, 1 );
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{     
				DateTime dateInCell, dateInGrp;
				if( DateEvaluator.DoesGroupContainRowHelper( groupByRow, row, out dateInGrp, out dateInCell ) )
					return true;

				return  dateInCell.Year  == dateInGrp.Year  &&
					dateInCell.Month >= dateInGrp.Month &&
					dateInCell.Month <  dateInGrp.Month + 3;
			}

			#endregion // DoesGroupContainRow			
		}

			#endregion // QuarterEvaluator

			#region SecondEvaluator

		// Evaluates the Date, Hour, Minute, and Second portions of a DateTime.
		//
		private class SecondEvaluator : IGroupByEvaluator
		{
			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				DateTime dt = DateEvaluator.GetGroupByValueHelper( groupByRow, row );

				// Create a new DateTime object so that the Millisecond component gets eliminated.
				//
				return new DateTime( dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second );	
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{             
				DateTime dateInCell, dateInGrp;
				if( DateEvaluator.DoesGroupContainRowHelper( groupByRow, row, out dateInGrp, out dateInCell ) )
					return true;

				return	dateInCell.Date   == dateInGrp.Date   &&
					dateInCell.Hour	  == dateInGrp.Hour   &&
					dateInCell.Minute == dateInGrp.Minute &&
					dateInCell.Second == dateInGrp.Second;
			}

			#endregion // DoesGroupContainRow			
		}

			#endregion // SecondEvaluator

			#region YearEvaluator

		// Evaluates the Year portion of a DateTime.
		//
		private class YearEvaluator : IGroupByEvaluator
		{
			#region GetGroupByValue

			object IGroupByEvaluator.GetGroupByValue( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{
				DateTime dt = DateEvaluator.GetGroupByValueHelper( groupByRow, row );

				// Set the Month and Day portions to 1 for every group so that the arbitrary Month 
				// and Day values found in cells do not get displayed in the group descriptions.
				//
				return new DateTime( dt.Year, 1, 1 );
			}

			#endregion // GetGroupByValue

			#region DoesGroupContainRow

			bool IGroupByEvaluator.DoesGroupContainRow( UltraGridGroupByRow groupByRow, UltraGridRow row )
			{             
				DateTime dateInCell, dateInGrp;
				if( DateEvaluator.DoesGroupContainRowHelper( groupByRow, row, out dateInGrp, out dateInCell ) )
					return true;

				return dateInCell.Year == dateInGrp.Year;
			}

			#endregion // DoesGroupContainRow			
		}

			#endregion // YearEvaluator

		#endregion // Evaluator Classes
	}
}
