#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Diagnostics;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;
	using Infragistics.Win.UltraWinScrollBar;

    /// <summary>
    /// The DataAreaUIElement contains the row and column scrolling
    /// regions.
    /// </summary>
	public class DataAreaUIElement : UIElement
	{

		// JJD 12/12/03 - Added Accessibility support
		private AccessibleObject accessibilityObject;

		// SSP 11/23/04 - Merged Cell Feature
		// Keep a flag on the data area ui element that specifies whether to use scroll window
		// when scrolling the background in merged cells mode. If the MergedCellContentArea is
		// VisibleRect where we are repositioning the contents of merged cells whenever it 
		// scrolls then we can't use scroll window to scroll. This flag is reset to false in
		// The PositionChildElements of the data area element and set to true by the merged cell
		// elements in their PositionChildElements logic.
		//
		internal bool dontUseUseScrollWindow = false;

		//#if DEBUG
		//		/// <summary>
		//		/// Constructor.
		//		/// </summary>
		//		/// <param name="parent">The parent element</param>
		//#endif
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b></b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		//internal DataAreaUIElement(UIElement parent ) : base( parent )
		public DataAreaUIElement(UIElement parent ) : base( parent )
		{
		}

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type,object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
		/// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
        protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( typeof( GroupByBoxUIElement ) == type ||
				typeof( AddNewBoxUIElement ) == type ||
				typeof( CaptionAreaUIElement ) == type )
				return false;

			if ( null != contexts )
			{
				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						if ( context is GroupByBox || context is AddNewBox )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch


		/// <summary>
		/// Returns the Layout object that determines the layout of the object. 
		/// This property is read-only at run-time.
		/// </summary><remarks><para>The Layout property of an object is used to access 
		/// the Layout object that determines the settings of various properties 
		/// related to the appearance and behavior of the object. The Layout object 
		/// provides a simple way to maintain multiple layouts for the grid and 
		/// apply them as needed. You can also save grid layouts to disk, the 
		/// registry or a storage stream and restore them later.</para><para>The 
		/// Layout object has properties such as Appearance and Override, so the 
		/// Layout object has sub-objects of these types, and their settings are 
		/// included as part of the layout. However, the information that is 
		/// actually persisted depends on how the settings of these properties were 
		/// assigned. If the properties were set using the Layout object's intrinsic 
		/// objects, the property settings will be included as part of the layout. 
		/// However, if a named object was assigned to the property from a 
		/// collection, the layout will only include the reference into the collection, 
		/// not the actual settings of the named object. 
		/// (For an overview of the difference between named and intrinsic objects, 
		/// please see the <see cref="UltraGridLayout.Appearance"/>property.</para>
		/// <para>
		/// For example, if the Layout object's Appearance property is used to 
		/// set values for the intrinsic Appearance object like this:</para><pre>
		/// UltraGrid1.DisplayLayout.Appearance.ForeColor = vbBlue</pre><para>
		/// Then the setting (in this case, ForeColor) will be included as part 
		/// of the layout, and will be saved, loaded and applied along with the 
		/// other layout data. However, suppose you apply the settings of a named 
		/// object to the UltraGridLayout's Appearance property in this manner:</para>
		/// <p class = "code">UltraGrid1.Appearances.Add "New1"</p>
		/// <p class = "code">UltraGrid1.Appearances("New1").ForeColor = System.Drawing.Color.Blue</p>
		/// <p class = "code">UltraGrid1.Layout.Appearance = UltraGrid1.Appearances("New1")</p>
		/// <para>In this case, the ForeColor setting will not be persisted as 
		/// part of the layout. Instead, the layout will include a reference to 
		/// the "New1" Appearance object and use whatever setting is present in 
		/// that object when the layout is applied.</para><para>By default, the 
		/// layout includes a copy of the entire Appearances collection, so if the 
		/// layout is saved and restored using the default settings, the object 
		/// should always be present in the collection when it is referred to. 
		/// However, it is possible to use the Load and Save methods of the Layout object 
		/// in such a way that the collection will not be re-created when the layout 
		/// is applied. If this is the case, and the layout contains a reference to 
		/// a nonexistent object, the default settings for that object's properties 
		/// will be used.</para>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridLayout Layout
		{
			get
			{
				return ((UltraGridUIElement)this.GetAncestor( typeof( UltraGridUIElement) ) ).Layout;
			}
		}

        /// <summary>
        /// Called during a drawing operation to ensure that all child elements are created and positioned properly. If the ChildElementsDirty flag is true then the default implementation will call PositionChildElements and reset the flag.
        /// </summary>
        /// <param name="controlElement">The control's main UIElement</param>
        /// <param name="recursive">If true will call this method on all descendant elements</param>
		protected override void VerifyChildElements( ControlUIElementBase controlElement,
			bool recursive )
		{

			// if the recursive flag is true then call RegenerateVisibleRows and
			// RegenerateVisibleHeaders
			// 
			if ( recursive )
			{
				// However, first call the base classes implementation with false
				// so that the positionChildElements method for this element will
				// be called (which makes sure the scroll regions are sized properly).
				//
				// This needs to be done before we call RegenerateVisibleRows and 
				// RegenerateVisibleHeaders so those routines can get the correct
				// origins and extents for each scroll region.
				//
				base.VerifyChildElements( controlElement, false );

				Infragistics.Win.UltraWinGrid.UltraGridLayout layout = this.Layout;

				RowScrollRegion rsr; 
				ColScrollRegion csr;

				// iterate over each row scroll region
				//
				for ( int i = 0; i < layout.RowScrollRegions.Count; i++ )
				{
					rsr = layout.RowScrollRegions[i];

					// call RegenerateVisibleRows on all non-hidden regions
					//
					if ( !rsr.Hidden )
						rsr.RegenerateVisibleRows();

				}

				// iterate over each column scroll region
				//
				for ( int i = 0; i < layout.ColScrollRegions.Count; i++ )
				{
					csr = layout.ColScrollRegions[i];
    
					// call RegenerateVisibleHeaders on all non-hidden regions
					//
					if ( !csr.Hidden )
						csr.RegenerateVisibleHeaders();

				}

			}

			// call the base classes implementation with the passed
			// in recursive flag
			//
			base.VerifyChildElements( controlElement, recursive );

		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// SSP 11/23/04 - Merged Cell Feature
			// Keep a flag on the data area ui element that specifies whether to use scroll window
			// when scrolling the background in merged cells mode. If the MergedCellContentArea is
			// VisibleRect where we are repositioning the contents of merged cells whenever it 
			// scrolls then we can't use scroll window to scroll. This flag is reset to false in
			// The PositionChildElements of the data area element and set to true by the merged cell
			// elements in their PositionChildElements logic.
			//
			this.dontUseUseScrollWindow = false;

			// SSP 10/27/03 UWG1808
			// If the scroll bar is not visible, then autofit all the way to the right edge of
			// the grid. Reset the haSSSSVerticalScrollbar to false.
			//
			this.haSSSSVerticalScrollbar = false;

			// make sure the regions are origins and extents are correct
			//
			this.ResizeRowScrollRegions( );
			this.ResizeColScrollRegions( );

			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = this.Layout;

			// SSP 4/21/05 BR03382
			// Moved this below the call to InitializeMetrics. The reason for the change is
			// that if someone searched the ui element tree from within the InitializeMetrics
			// then they would not find the ui element sice we are clearing the collection
			// here.
			//
			//UIElementsCollection oldElements = this.ChildElements;			
			//this.childElementsCollection = null;

			// make sure the column metrics have been properly calculated
			//
			layout.ColScrollRegions.InitializeMetrics();

			// SSP 4/21/05 BR03382
			// Moved this here from above the call to InitializeMetrics. The reason for the 
			// change is that if someone searched the ui element tree from within the 
			// InitializeMetrics then they would not find the ui element sice we are clearing 
			// the collection here.
			//
			UIElementsCollection oldElements = this.ChildElements;			
			this.childElementsCollection = null;

			Rectangle bounds = this.RectInsideBorders;

			RowScrollRegion rsr; 
			ColScrollRegion csr;
			RowScrollRegion rsrPrevious = null; 
			ColScrollRegion csrPrevious = null;
			RowScrollRegion rsrLastVisible = (RowScrollRegion)layout.RowScrollRegions.LastVisibleRegion; 
			ColScrollRegion csrLastVisible = (ColScrollRegion)layout.ColScrollRegions.LastVisibleRegion;

			// SSP 1/27/05 BR02053
			// Instead of checking IsPrinting flag, make use of IsDisplayLayout. What we are 
			// interested in finding out is this element is in the print layout or not. Simply
			// checking the IsPrinting flag won't tell us that the display layout could verify 
			// it's ui elements while printing (like when the grid gets a paint while printing,
			// especially with the new UltraGridPrintDocument).
			//
			//if ( this.Layout.Grid.IsPrinting )
			if ( this.Layout.IsPrintLayout )
			{
				rsr = this.Layout.RowScrollRegions[0];
				csr = this.Layout.ColScrollRegions[0];

				// see if we have one in the old child element collection
				// that we can re-use
				//
				RowColRegionIntersectionUIElement rowcol = (RowColRegionIntersectionUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( RowColRegionIntersectionUIElement ), true );

				// if we don't have one then create a new one
				//
				if ( null == rowcol )
					rowcol = new RowColRegionIntersectionUIElement( this );
            
				// initialize it with references to the passed in row and column
				// scrolling regions
				//
				rowcol.Initialize( rsr, csr );

				// add it to the child elements collection
				//
				this.ChildElements.Add( rowcol );

				// calculate and set its rect
				//
				Rectangle boundsRect= this.RectInsideBorders;
				Rectangle rsrCsrIntersectRect = boundsRect;

				rsrCsrIntersectRect.Offset( csr.Origin - boundsRect.Left, rsr.Origin - boundsRect.Top );

				rsrCsrIntersectRect.Width  = csr.Extent;
				rsrCsrIntersectRect.Height = rsr.Extent;
 
				// make sure we don't go outside the bounding area
				//
				rsrCsrIntersectRect.Intersect( boundsRect );

				rowcol.Rect = rsrCsrIntersectRect;

				return;														
			}

			// iterate over each row scroll region
			//
			for ( int i = 0; i < layout.RowScrollRegions.Count; i++ )
			{
				rsr = layout.RowScrollRegions[i];

				// bypass hidden regions
				//
				if ( rsr.Hidden )
					continue;

				// if we can't fir anymore then break out of for loop
				//
				// JJD 10/05/01 - UWG502
				// Check origin against bottom instead of height
				// 
				//if ( rsr.Origin >= bounds.Height )				
				if ( rsr.Origin >= bounds.Bottom )				
					break;


				// SSP 9/28/01 UWG259 
				// Uncommented the code
				//
				// SSP 10/24/01 Regarding the TODO that was here: Already implemented
				// Resize the current rsr so that it occupies the 
				// rest of the space if the nextRsr is not going
				// to be shown in the case of below condition
				//--------------------------------------------------------
				bool lastRsr = false;
				RowScrollRegion nextRsr = null;
				if ( 1 + i < layout.RowScrollRegions.Count )
					nextRsr = layout.RowScrollRegions[ 1 + i ];

				// JJD 10/05/01 - UWG502
				// Check origin against bottom instead of height
				// 
				//if ( null != nextRsr && nextRsr.Origin >= bounds.Height )
				if ( null != nextRsr && nextRsr.Origin >= bounds.Bottom )
					lastRsr = true;
				//--------------------------------------------------------


				csrPrevious = null;

				// iterate over each column scroll region adding an
				// itersection element for each
				//
				for ( int j = 0; j < layout.ColScrollRegions.Count; j++ )
				{
					

					csr = layout.ColScrollRegions[j];
    
					// JJD 10/05/01 - UWG502
					// Check origin against right instead of width
					// 
					if ( csr.Hidden ||
						csr.Origin >= bounds.Right )
						//                         csr.Origin >= bounds.Width )
						continue;

					// SSP 9/28/01 UWG259 
					// Uncommented the code
					//
					// SSP 10/24/01 Regarding the TODO that was here: Already implemented
					// Resize the current csr so that it occupies the 
					// rest of the space if the nextRsr is not going
					// to be show in the case of below condition
					//--------------------------------------------------------
					bool lastCsr = false;

					ColScrollRegion nextCsr = null;
					if ( 1 + i < layout.ColScrollRegions.Count )
						nextCsr = layout.ColScrollRegions[ 1 + i ];

					//RobA 10/4/01 
					//if ( null != nextCsr && nextCsr.Origin >= bounds.Height )

					// JJD 10/05/01 - UWG502
					// Check origin against right instead of width
					// 
					//if ( null != nextCsr && nextCsr.Origin >= bounds.Width )
					if ( null != nextCsr && nextCsr.Origin >= bounds.Right )
						lastCsr = true;
					//--------------------------------------------------------

					this.AddIntersectionHelper( oldElements, 
						rsr, 
						csr,
						rsrPrevious,
						csrPrevious,
						// SSP 8/29/01 UWG259
						//( rsrLastVisible == rsr ),
						//( csrLastVisible == csr ));
						lastRsr || ( rsrLastVisible == rsr ),
						lastCsr || ( csrLastVisible == csr ));

					csrPrevious = csr;
				}
                
				rsrPrevious = rsr; 
			}

			// if no elements have been added then add the first regions
			//
			if ( null == this.childElementsCollection )
			{
				this.AddIntersectionHelper( oldElements,
					(RowScrollRegion)layout.RowScrollRegions.FirstVisibleRegion,
					(ColScrollRegion)layout.ColScrollRegions.FirstVisibleRegion,
					null, null, true, true );
			}

			// iterate over each child element looking for row splitters
			//
			for ( int k = 0; k < this.childElementsCollection.Count; k++ )
			{
				if ( this.childElementsCollection[k].GetType() == typeof( RowSplitterBarUIElement ) )
				{
					// iterate over each child element looking for col splitters
					//
					for ( int l = 0; l < this.childElementsCollection.Count; l++)
					{
						if ( this.childElementsCollection[l].GetType() == typeof( ColSplitterBarUIElement ) )
						{
							// see if we have one in the old child element collection
							// that we can re-use
							//
							SplitterIntersectionUIElement splitterIntersection = (SplitterIntersectionUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( SplitterIntersectionUIElement ), true );

							// if we don't have one then create a new one
							//
							if ( null == splitterIntersection )
								// SSP 1/5/04 UWG1606
								// Added RowColSplitterIntersectionUIElement class. Use that instead.
								//
								//splitterIntersection = new SplitterIntersectionUIElement( this,
								splitterIntersection = new RowColSplitterIntersectionUIElement( this,
									(SplitterUIElement)this.childElementsCollection[l],
									(SplitterUIElement)this.childElementsCollection[k]	);
							else
								splitterIntersection.Initialize( (SplitterUIElement)this.childElementsCollection[l],
									(SplitterUIElement)this.childElementsCollection[k] );


							// add it to the child elements collection
							//
							this.ChildElements.Add( splitterIntersection );
						}
					}
				}
			}

			// JJD 1/11/02
			// Call dispose on any of the old elements that have not been used.
			// SSP 5/17/05 - Optimization
			// Call the method off the UIElements collection instead to reduce duplication of code.
			//
			oldElements.DisposeElements( );
			

			// SSP 10/27/03 UWG1808
			// If the scroll bar is not visible, then autofit all the way to the right edge of
			// the grid. However when the scrollbar is needed, then we need to re-autofit the columns.
			// So we need to find out if the scrollbar's visible state changed.
			//
			// ----------------------------------------------------------------------------------
			// SSP 12/4/03 UWG2734 UWG2765
			// Commented out the if condition. Dirty colscrollregion metrics regardless of whether
			// AutoFitColumns is true or not. This should not cause any problems. We need to do
			// this due to the fix for UWG2734 which involved not adding the header fully covered 
			// under the vertical scrollbar to the visible headers collection. So when the vertical
			// scrollbar disappears, we need to regenerate visible headers to add the header
			// that would show up where the vertical scrollbar was before. NOTE: DirtyMetrics
			// goes and dirties the grid element. We don't need to dirty the grid element since
			// we are already in verify stage. So if it causes any problem simply set the dirty
			// visible headers flag on all the col scroll regions to true.
			// 
			//if ( this.Layout.AutoFitColumns )
			//{
				if ( haDDDDVerticalScrollbar != haSSSSVerticalScrollbar )
					this.Layout.ColScrollRegions.DirtyMetrics( false );

				haDDDDVerticalScrollbar = haSSSSVerticalScrollbar;
			//}
			// ----------------------------------------------------------------------------------
		}

		// SSP 10/27/03 UWG1808
		// If the scroll bar is not visible, then autofit all the way to the right edge of
		// the grid. However when the scrollbar is needed, then we need to re-autofit the columns.
		// So we need to find out if the scrollbar's visible state changed.
		//
		internal bool haDDDDVerticalScrollbar = true;
		private bool haSSSSVerticalScrollbar = true;

		private RowColRegionIntersectionUIElement AddIntersectionHelper( UIElementsCollection oldElements,
			RowScrollRegion rsr, 
			ColScrollRegion csr,
			RowScrollRegion rsrPrevious, 
			ColScrollRegion csrPrevious,
			bool lastVisibleRsr,
			bool lastVisibleCsr )
		{
			if ( null == rsr )
			{
				Debug.Assert( false, "Null RowScrollRegion passed into AddIntersectionHelper" );
				return null;
			}

			if ( null == csr )
			{
				Debug.Assert( false, "Null ColScrollRegion passed into AddIntersectionHelper" );
				return null;
			}

			// SSP 5/12/06 - Optmizations
			// 
			UltraGridLayout layout = this.Layout;
			if ( null == layout )
			{
				Debug.Assert( false, "No layout!" );
				return null;
			}

			// see if we have one in the old child element collection
			// that we can re-use
			//
			RowColRegionIntersectionUIElement rowcol = (RowColRegionIntersectionUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( RowColRegionIntersectionUIElement ), true );

			// if we don't have one then create a new one
			//
			if ( null == rowcol )
				rowcol = new RowColRegionIntersectionUIElement( this );
            
			// initialize it with references to the passed in row and column
			// scrolling regions
			//
			rowcol.Initialize( rsr, csr );

			// add it to the child elements collection
			//
			this.ChildElements.Add( rowcol );

			// calculate and set its rect
			//
			Rectangle bounds = this.RectInsideBorders;
			Rectangle rsrCsrIntersectRect = bounds;

			rsrCsrIntersectRect.Offset( csr.Origin - bounds.Left, rsr.Origin - bounds.Top );

			rsrCsrIntersectRect.Width  = csr.Extent;
			rsrCsrIntersectRect.Height = rsr.Extent;

			// SSP 9/28/01 UWG259
			// if the col scroll region or the row scroll region
			// is the last scroll region  make sure that it 
			// occupies the rest of the data area
			//
			//-----------------------------------------------
			if ( lastVisibleCsr )
			{
				rsrCsrIntersectRect.Width = bounds.Right - rsrCsrIntersectRect.Left;
			}
			if ( lastVisibleRsr )
			{
				rsrCsrIntersectRect.Height = bounds.Bottom - rsrCsrIntersectRect.Top;
			}
			//-----------------------------------------------

 
			// make sure we don't go outside the bounding area
			//
			rsrCsrIntersectRect.Intersect( bounds );

			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
			//int splitterSize  = 0;
			int colRegionSplitBoxWidth = 0;
			int rowRegionSplitBoxHeight = 0;
			int colRegionSplitterBarWidth = 0;
			int rowRegionSplitterBarHeight = 0;

			if ( this.Layout.Grid is UltraGrid )
			{
				// SSP 5/12/06 - App Styling
				// Added custom properties for controlling widths of splitter bars and split boxes.
				// 
				//splitterSize  = this.Layout.SplitterBarWidth;
				colRegionSplitBoxWidth = layout.ColScrollRegionSplitBoxWidthResolved;
				rowRegionSplitBoxHeight = layout.RowScrollRegionSplitBoxHeightResolved;
				colRegionSplitterBarWidth = layout.ColScrollRegionSplitterBarWidthResolved;
				rowRegionSplitterBarHeight = layout.RowScrollRegionSplitterBarHeightResolved;
			
				// if this isn't the first colscrollregion but it is the first
				// row scroll region then add a col splitter for the 
				// previous csr
				//
				if ( null == rsrPrevious &&
					null != csrPrevious )
				{
					// see if we have one in the old child element collection
					// that we can re-use
					//
					ColSplitterBarUIElement splitter = (ColSplitterBarUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( ColSplitterBarUIElement ), true );

					// if we don't have one then create a new one
					//
					if ( null == splitter )
						splitter = new ColSplitterBarUIElement( this );

					// initialize context
					//
					splitter.initialize( csrPrevious );

					// add it to the child elements collection
					//
					this.ChildElements.Add( splitter );

					splitter.Rect = new Rectangle( 
						// SSP 5/12/06 - App Styling
						// Added custom properties for controlling widths of splitter bars and split boxes.
						// 
						//rsrCsrIntersectRect.Left - splitterSize,
						rsrCsrIntersectRect.Left - colRegionSplitterBarWidth,
						rsrCsrIntersectRect.Top,
						// SSP 5/12/06 - App Styling
						// Added custom properties for controlling widths of splitter bars and split boxes.
						// 
						//splitterSize,
						colRegionSplitterBarWidth,
						this.Rect.Height ); 
	                                               
				}

				// if this isn't the first rowscrollregion but it is the first
				// col scroll region then add a row splitter for the 
				// previous csr
				//
				if ( null == csrPrevious &&
					null != rsrPrevious )
				{
					// see if we have one in the old child element collection
					// that we can re-use
					//
					RowSplitterBarUIElement splitter = (RowSplitterBarUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( RowSplitterBarUIElement ), true );

					// if we don't have one then create a new one
					//
					if ( null == splitter )
						splitter = new RowSplitterBarUIElement( this );

					// initialize context
					//
					splitter.initialize( rsrPrevious );

					// add it to the child elements collection
					//
					this.ChildElements.Add( splitter );

					splitter.Rect = new Rectangle( rsrCsrIntersectRect.Left,
						// SSP 5/12/06 - App Styling
						// Added custom properties for controlling widths of splitter bars and split boxes.
						// 
						//rsrCsrIntersectRect.Top - splitterSize,
						rsrCsrIntersectRect.Top - rowRegionSplitterBarHeight,
						this.Rect.Width,
						// SSP 5/12/06 - App Styling
						// Added custom properties for controlling widths of splitter bars and split boxes.
						// 
						//splitterSize 
						rowRegionSplitterBarHeight
						); 
	                                               
				}
			}

			Rectangle horzScrollbarRect = new Rectangle(0,0,0,0);
			Rectangle vertScrollbarRect = new Rectangle(0,0,0,0);
			bool needsHorzScrollbar = false;
			bool needsVertScrollbar = false;

			// JJD 12/19/01
			// Only set the needsHorzScrollbar flag true if we are
			// on the last visible row scroll region
			//
			needsHorzScrollbar = lastVisibleRsr && csr.WillScrollbarBeShown();
			
			if ( needsHorzScrollbar )
			{
				// calculate the scrollbar rect and adjust the 
				// rsrCsrIntersectRect so they don't overlap
				//
				horzScrollbarRect = rsrCsrIntersectRect;

				rsrCsrIntersectRect.Height -= SystemInformation.HorizontalScrollBarHeight;

				horzScrollbarRect.Height = SystemInformation.HorizontalScrollBarHeight;
                
				horzScrollbarRect.Offset( 0, rsrCsrIntersectRect.Height );

				// check if we need the col split box
				//
				if ( null == csrPrevious && 
					csr.Layout.ColScrollRegions.IsSplitBoxVisible )
				{
					// calculate the rect for the split box
					//
					Rectangle splitBoxRect = horzScrollbarRect;

					// SSP 5/12/06 - App Styling
					// Added custom properties for controlling widths of splitter bars and split boxes.
					// 
					// ------------------------------------------------------------------------------------
					
					splitBoxRect.Width = colRegionSplitBoxWidth;

					// adjust the scrolbar rect so it doesn't overlap
					//
					horzScrollbarRect.Width -= colRegionSplitBoxWidth;
					horzScrollbarRect.Offset( colRegionSplitBoxWidth, 0 );
					// ------------------------------------------------------------------------------------

					// see if we have one in the old child element collection
					// that we can re-use
					//
					ColSplitBoxUIElement splitbox = (ColSplitBoxUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( ColSplitBoxUIElement ), true );

					// if we don't have one then create a new one
					//
					if ( null == splitbox )
						splitbox = new ColSplitBoxUIElement( this, csr.Layout.ColScrollRegions );

					// add it to the child elements collection
					//
					this.ChildElements.Add( splitbox );
					splitbox.Rect = splitBoxRect;

				}
			}


			// JJD 12/19/01
			// Only set the needsVertScrollbar flag true if we are
			// on the last visible col scroll region
			//
			needsVertScrollbar = lastVisibleCsr && rsr.WillScrollbarBeShown();

			// SSP 10/27/03 UWG1808
			// If the scroll bar is not visible, then autofit all the way to the right edge of
			// the grid. However when the scrollbar is needed, then we need to re-autofit the columns.
			// So we need to find out if the scrollbar's visible state changed.
			//
			haSSSSVerticalScrollbar = haSSSSVerticalScrollbar || needsVertScrollbar;

			if ( needsVertScrollbar )
			{
				// calculate the scrollbar rect and adjust the 
				// rsrCsrIntersectRect so they don't overlap
				//
				vertScrollbarRect = rsrCsrIntersectRect;

				rsrCsrIntersectRect.Width -= SystemInformation.VerticalScrollBarWidth;

				vertScrollbarRect.Width = SystemInformation.VerticalScrollBarWidth;
                
				vertScrollbarRect.Offset( rsrCsrIntersectRect.Width, 0 );

				// if both scrollbars are shown then we need to adjust both scrollbar
				// dimensions and add a scrollbar interection element
				//
				if ( needsHorzScrollbar )
				{
					// adjust the horizontal scrollbar rect so it
					// doesn't overlap the vertical scrollbar
					//
					horzScrollbarRect.Width -= vertScrollbarRect.Width;

					// calculate the rect for the scrollbar inersection element
					//
					//RobA UWG13 9/28/01 we don't need to add 1 to X and Y
					//
					

					Rectangle scrollbarIntersectRect
						= new Rectangle( horzScrollbarRect.Right,
						vertScrollbarRect.Bottom,
						vertScrollbarRect.Width,
						horzScrollbarRect.Height );


					// see if we have one in the old child element collection
					// that we can re-use
					//
					ScrollbarIntersectionUIElement splitterIntersection = (ScrollbarIntersectionUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( ScrollbarIntersectionUIElement ), true );

					// if we don't have one then create a new one
					//
					if ( null == splitterIntersection )
						splitterIntersection = new ScrollbarIntersectionUIElement( this );

					// add it to the child elements collection
					//
					this.ChildElements.Add( splitterIntersection );

					splitterIntersection.Rect = scrollbarIntersectRect;
				}

				// check if we need the row split box
				//
				if ( null == rsrPrevious && 
					csr.Layout.RowScrollRegions.IsSplitBoxVisible )					
				{
					// calculate the rect for the split box
					//
					Rectangle splitBoxRect = vertScrollbarRect;

					// SSP 5/12/06 - App Styling
					// Added custom properties for controlling widths of splitter bars and split boxes.
					// 
					// ------------------------------------------------------------------------------------
					
					splitBoxRect.Height = rowRegionSplitBoxHeight;

					// adjust the scrolbar rect so it doesn't overlap
					//
					vertScrollbarRect.Height -= rowRegionSplitBoxHeight;
					vertScrollbarRect.Offset( 0, rowRegionSplitBoxHeight );
					// ------------------------------------------------------------------------------------

					// see if we have one in the old child element collection
					// that we can re-use
					//
					RowSplitBoxUIElement splitbox = (RowSplitBoxUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( RowSplitBoxUIElement ), true );

					// if we don't have one then create a new one
					//
					if ( null == splitbox )
						splitbox = new RowSplitBoxUIElement( this, csr.Layout.RowScrollRegions );

					// add it to the child elements collection
					//
					this.ChildElements.Add( splitbox );

					splitbox.Rect = splitBoxRect;

				}
			}

			// set rsr and csr scrollbar rects
			//            

			if ( needsHorzScrollbar )
			{
				// see if we have one in the old child element collection
				// that we can re-use
				//
				ColScrollbarUIElement scrollbar = (ColScrollbarUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( ColScrollbarUIElement ), true );

				// if we don't have one then create a new one
				//
				if ( null == scrollbar )
				{
					// AS - 12/14/01
					// Need to pass the scrollbar info into the element.
					//
					//scrollbar = new ColScrollbarUIElement( this );
					scrollbar = new ColScrollbarUIElement( this, csr.ScrollBarInfo );
				}

				// initialize context
				//
				// AS - 12/14/01
				//scrollbar.initialize( csr );
				scrollbar.initialize( csr, csr.ScrollBarInfo );

				// add it to the child elements collection
				//
				this.ChildElements.Add( scrollbar );

				scrollbar.Rect = horzScrollbarRect; 

				// set csr scrollbar rects
				csr.ScrollbarRect = horzScrollbarRect;                                               
			}
				// SSP 8/7/01 UWG17 
				// If we are not going to show the scrollbars, set the scrollbar
				// rect in the region to empty rect.
			else 
			{
				// JJD 12/19/01
				// Only hide the scrollbar if we are on the last visible 
				// row scroll region
				//
				if ( null != csr && lastVisibleRsr )
					csr.ScrollbarRect = Rectangle.Empty;
			}

			if ( needsVertScrollbar )
			{
				// see if we have one in the old child element collection
				// that we can re-use
				//
				RowScrollbarUIElement scrollbar = (RowScrollbarUIElement)DataAreaUIElement.ExtractExistingElement( oldElements, typeof( RowScrollbarUIElement ), true );

				// if we don't have one then create a new one
				//
				if ( null == scrollbar )
				{
					// AS - 12/14/01
					// Need to pass the scrollbar info into the element.
					//
					//scrollbar = new RowScrollbarUIElement( this );
					scrollbar = new RowScrollbarUIElement( this, rsr.ScrollBarInfo );
				}

				// initialize context
				//
				// AS - 12/14/01
				//scrollbar.initialize( rsr );
				scrollbar.initialize( rsr, rsr.ScrollBarInfo );

				// add it to the child elements collection
				//
				this.ChildElements.Add( scrollbar );

				scrollbar.Rect = vertScrollbarRect; 

				// set csr scrollbar rects
				rsr.ScrollbarRect = vertScrollbarRect;            
			}
				// SSP 8/7/01 UWG17 
				// If we are not going to show the scrollbars, set the scrollbar
				// rect in the region to empty rect.
			else 
			{
				// JJD 12/19/01
				// Only hide the scrollbar if we are on the last visible 
				// col scroll region
				//
				if ( null != csr && lastVisibleCsr )
					rsr.ScrollbarRect = Rectangle.Empty;
			}

			rowcol.Rect = rsrCsrIntersectRect;

			return rowcol;
		}

		/// <summary>
		/// Returning true causes all drawing of this element to be expicitly clipped
		/// to its region
		/// </summary>
		protected override bool ClipSelf { get { return true; } }

        
		/// <summary>
		/// Override the Rect property so we can call verifyChildElements on the set
		/// </summary>
		public override System.Drawing.Rectangle Rect
		{
			get
			{
				return base.rectValue;
			}
			set
			{
				base.Rect = value;
				this.VerifyChildElements( this.ControlElement, false );
			}
		}
 
       
		/// <summary>
		/// Resizes all RowScrollRegions
		/// </summary>
		protected void ResizeRowScrollRegions ( )
		{
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = this.Layout;

			int origin = this.Rect.Top;
			int totalExtent = 0;

			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
			//int splitterWidth = layout.SplitterBarWidth;
			int splitterWidth = layout.RowScrollRegionSplitterBarHeightResolved;

			foreach ( RowScrollRegion rsr in layout.RowScrollRegions )
			{
				// Do not size or consider null or hidden colscrollregions
				//
				if ( rsr.Hidden )
				{
					rsr.SetOriginAndExtent( 0, 0 );					
				}
				else
				{
					totalExtent += rsr.Extent;

					// If the last Region, we want to adjust its extent as well as origin
					//   otherwise, just origin
					//
					if ( rsr.IsLastVisibleRegion )
					{
						rsr.SetOriginAndExtent ( origin, rsr.Extent + this.Rect.Height - totalExtent );
					}
					else
						rsr.SetOriginAndExtent ( origin, rsr.Extent );

					origin         += rsr.Extent + splitterWidth;

					// Adjust the nTotalExtent as well so we don't screw up
					// the extent calculation for the last row scroll region
					//
					totalExtent    += splitterWidth;
				}
			}
		}

      
		/// <summary>
		/// Resizes all ColScrollRegions
		/// </summary>
		protected void ResizeColScrollRegions ()
		{
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = this.Layout;

			int origin = this.Rect.Left;
			int totalExtent = 0;

			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
			//int splitterWidth = layout.SplitterBarWidth;
			int splitterWidth = layout.ColScrollRegionSplitterBarWidthResolved;

			foreach ( ColScrollRegion csr in layout.ColScrollRegions )
			{
				// Do not size or consider null or hidden colscrollregions
				//
				if ( csr.Hidden )
				{
					csr.SetOriginAndExtent( 0, 0 );
				}
				else
				{
					totalExtent += csr.Extent;

					// If the last Region, we want to adjust its extent as well as origin
					//   otherwise, just origin
					//
					if ( csr.IsLastVisibleRegion )
					{
						csr.SetOriginAndExtent ( origin, csr.Extent + this.Rect.Width - totalExtent );
					}
					else
						csr.SetOriginAndExtent ( origin, csr.Extent );

					origin         += csr.Extent + splitterWidth;

					// Adjust the nTotalExtent as well so we don't screw up
					// the extent calculation for the last row scroll region
					//
					totalExtent    += splitterWidth;
				}
			}
		}

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{
			// this element doesn't draw a background
		}
 
		/// <summary>
		/// this element doesn't draw an image background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return UIElementBorderStyle.None;
			}
		}

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return 0;
			}
		}

		// SSP 3/19/03 - Row Layout Functionality
		// Added RowLayoutDesignerElement property.
		//
		#region RowLayoutDesignerElement

		/// <summary>
		/// Indicates whether this element is being used for row layout designer element.
		/// </summary>
		internal protected virtual bool RowLayoutDesignerElement
		{
			get
			{
				return false;
			}
		}

		#endregion // RowLayoutDesignerElement

		// JJD 12/23/03 - Accessibility
		#region IsAccessibleElement property

		/// <summary>
		/// Indicates if the element supports accessibility
		/// </summary>
		public override bool IsAccessibleElement { get { return true; } }

		#endregion IsAccessibleElement property

		// JJD 12/12/03 - Added Accessibility support
		#region AccessibilityInstance property

		/// <summary>
		/// Returns the accesible object representing the data area of the grid.
		/// </summary>
		public override AccessibleObject AccessibilityInstance
		{
			get
			{
				if ( this.accessibilityObject == null )
				{
					UltraGridUIElement gridElement = this.GetAncestor( typeof(UltraGridUIElement) ) as UltraGridUIElement;

					if ( gridElement != null )
					{
						UltraGridBase grid = gridElement.Grid;

						if ( grid != null )
							this.accessibilityObject = grid.CreateAccessibilityInstance	( this );
					}
				}

				return this.accessibilityObject;
			}
		}

		#endregion AccessibilityInstance property

		// JJD 12/12/03 - Added Accessibility support
		#region Public Class DataAreaAccessibleObject

		/// <summary>
		/// The Accessible object for the date area of a grid.
		/// </summary>
		public class DataAreaAccessibleObject : UIElementAccessibleObject
		{
			#region Constructor
			
			/// <summary>
			/// Constructor.
			/// </summary>
            /// <param name="dataAreaElement">The DataAreaUIElement that this accessible object represents.</param>
			public DataAreaAccessibleObject ( DataAreaUIElement dataAreaElement ) : base(  dataAreaElement, AccessibleRole.Grouping, Shared.SR.GetString("AccessibleName_DataArea") )
			{
//				this.dataAreaElement = dataAreaElement;
			}

			#endregion Constructor

			#region Base Class Overrides

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				return null;
			}

				#endregion GetSelected

				#region Select

			/// <summary>
			/// Modifies the selection or moves the keyboard focus of the accessible object.
			/// </summary>
			/// <param name="flags">One of the <see cref="System.Windows.Forms.AccessibleSelection"/> values.</param>
			public override void Select(System.Windows.Forms.AccessibleSelection flags)
			{
			
			}

				#endregion Select

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Grid

			/// <summary>
			/// Returns the associated grid control.
			/// </summary>
			public UltraGridBase Grid 
			{ 
				get 
				{
					UltraGridUIElement gridElement = this.UIElement.GetAncestor( typeof(UltraGridUIElement) ) as UltraGridUIElement;

					if ( gridElement != null )
						return gridElement.Grid;

					return null; 
				} 
			}

					#endregion Grid

				#endregion Public Properties

			#endregion Properties
		}

		#endregion Public Class DataAreaAccessibleObject
    }
}
