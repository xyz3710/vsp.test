#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win;
using System.Runtime.Serialization;
using System.Collections;
using System.Drawing.Design;
using System.Security.Permissions;
using Infragistics.Win.Layout;


namespace Infragistics.Win.UltraWinGrid
{

	#region LayoutUtility Class

	internal class LayoutUtility
	{
		#region DistanceSquared

		private static int DistanceSquared( Rectangle rect, Point p )
		{
			rect.Offset( - p.X, - p.Y );

			if ( rect.X < 0 && rect.Right > 0 )
			{
				if ( rect.Y < 0 && rect.Bottom > 0 )
					return 0;
				else
					return Math.Min( rect.Y * rect.Y, rect.Bottom * rect.Bottom );
			}
			else if ( rect.Y < 0 && rect.Bottom > 0 )
			{
				return Math.Min( rect.X * rect.X, rect.Right * rect.Right );
			}
			else
			{
				return
					Math.Min( rect.X * rect.X + rect.Y * rect.Y,
					Math.Min( rect.Right * rect.Right + rect.Y * rect.Y,
					Math.Min( rect.X * rect.X + rect.Bottom * rect.Bottom, 
							  rect.Right * rect.Right + rect.Bottom * rect.Bottom ) ) );
			}
		}

		#endregion // DistanceSquared

		#region IUIElementCallback Interface

		private interface IUIElementCallback
		{
			bool ProcessElement( UIElement element );
		}

		#endregion // IUIElementCallback Interface

		#region NearesetColumnHeaderCallback Class

		private class NearesetColumnHeaderCallback : IUIElementCallback
		{
			internal ColumnHeader nearestColumnHeader = null;

			private UltraGridBand band = null;
			private int minDistance = 0;
			private Point point = Point.Empty;

			internal NearesetColumnHeaderCallback( UltraGridBand band, Point point )
			{
				this.point = point;
				this.band = band;
			}

			bool IUIElementCallback.ProcessElement( UIElement element )
			{
				HeaderUIElement headerElem = element as HeaderUIElement;

				if ( null != headerElem && headerElem.Header is ColumnHeader &&
					( null == band || band == headerElem.Header.Band ) )
				{
					int distance = DistanceSquared( headerElem.Rect, this.point );

					if ( distance <= this.minDistance || null == this.nearestColumnHeader )
					{
						this.nearestColumnHeader = (ColumnHeader)headerElem.Header;
						this.minDistance = distance;
					}
				}

				return true;
			}
		}

		#endregion // NearesetColumnHeaderCallback Class

		#region TraverseDescendants

		private static bool TraverseDescendants( UIElement parentElement, IUIElementCallback callback )
		{			
			for ( int i = 0; i < parentElement.ChildElements.Count; i++ )
			{
				UIElement elem = parentElement.ChildElements[i];

				if ( null != elem )
				{
					if ( ! callback.ProcessElement( elem ) )
						return false;

					if ( ! TraverseDescendants( elem, callback ) )
						return false;
				}
			}

			return true;
		}

		#endregion // TraverseDescendants

		#region FindNearesetColumnHeader

		internal static ColumnHeader FindNearesetColumnHeader( UIElement parentElem, UltraGridBand band, Point p )
		{
			NearesetColumnHeaderCallback c = new NearesetColumnHeaderCallback( band, p ); 

			TraverseDescendants( parentElem, c );

			return c.nearestColumnHeader;
		}

		#endregion // FindNearesetColumnHeader
        
        // MRS 2/10/2009 - TFS13728
        #region FindNearesetGroupHeader

        internal static GroupHeader FindNearesetGroupHeader(UIElement parentElem, UltraGridBand band, Point p)
        {
            NearesetGroupHeaderCallback c = new NearesetGroupHeaderCallback(band, p);

            TraverseDescendants(parentElem, c);

            return c.nearestGroupHeader;
        }

        #endregion // FindNearesetGroupHeader

        // MRS 2/10/2009 - TFS13728
        #region NearesetColumnHeaderCallback Class

        private class NearesetGroupHeaderCallback : IUIElementCallback
        {
            internal GroupHeader nearestGroupHeader = null;

            private UltraGridBand band = null;
            private int minDistance = 0;
            private Point point = Point.Empty;

            internal NearesetGroupHeaderCallback(UltraGridBand band, Point point)
            {
                this.point = point;
                this.band = band;
            }

            bool IUIElementCallback.ProcessElement(UIElement element)
            {
                HeaderUIElement headerElem = element as HeaderUIElement;

                if (null != headerElem && headerElem.Header is GroupHeader &&
                    (null == band || band == headerElem.Header.Band))
                {
                    int distance = DistanceSquared(headerElem.Rect, this.point);

                    if (distance <= this.minDistance || null == this.nearestGroupHeader)
                    {
                        this.nearestGroupHeader = (GroupHeader)headerElem.Header;
                        this.minDistance = distance;
                    }
                }

                return true;
            }
        }             

        #endregion // NearesetColumnHeaderCallback Class

        #region NearesetCellCallback Class

        private class NearesetCellCallback : IUIElementCallback
		{
			internal UltraGridCell  nearestCell = null;

			private UltraGridBand band = null;			
			private int minDistance = 0;
			private Point point = Point.Empty;

			internal NearesetCellCallback( UltraGridBand band, Point point )
			{
				this.point = point;
				this.band = band;
			}

			bool IUIElementCallback.ProcessElement( UIElement element )
			{
				// MRS 8/2/05 - BR05186
				// This whole method needs to account for HeaderUIElement as well as CellUIElement. 
				//
				#region Old Code
//                CellUIElement cellElem = element as CellUIElement;
//
//				if ( null != cellElem && null != cellElem.Row && 
//					( null == this.band || this.band == cellElem.Row.Band )
//					// SSP 11/17/03 Add Row Feature
//					// If the row is a template add-row, then don't select it.
//					//
//					// SSP 4/20/05 - NAS 5.2 Filter Row
//					// Use the Selectable property instead.
//					//
//					//&& cellElem.Row.CanSelectRow 
//					&& cellElem.Row.Selectable )
//				{
//					if ( ! cellElem.Contains( this.point ) )
//					{
//						// Only find cells from the row that the point is over.
//						//
//						if ( null != cellElem.Parent && ! cellElem.Parent.Contains( this.point ) )
//							return true;
//					}
//
//					int distance = DistanceSquared( cellElem.Rect, this.point );
//
//					if ( distance <= this.minDistance || null == this.nearestCell )
//					{
//						this.nearestCell = cellElem.Cell;
//						this.minDistance = distance;
//					}
//				}
//
//				return true;
				#endregion Old Code

				if (! (element is CellUIElement) &&
					! (element is HeaderUIElement ) )
				{
					return true;
				}

				UltraGridRow row = element.GetContext(typeof(UltraGridRow)) as UltraGridRow;

				if ( null != row && 
					( null == this.band || this.band == row.Band )
					// SSP 11/17/03 Add Row Feature
					// If the row is a template add-row, then don't select it.
					//
					// SSP 4/20/05 - NAS 5.2 Filter Row
					// Use the Selectable property instead.
					//
					//&& cellElem.Row.CanSelectRow 
					&& row.Selectable )
				{
					if ( ! element.Contains( this.point ) )
					{
						// Only find cells from the row that the point is over.
						//
						if ( null != element.Parent && ! element.Parent.Contains( this.point ) )
							return true;
					}

					int distance = DistanceSquared( element.Rect, this.point );

					if ( distance <= this.minDistance || null == this.nearestCell )
					{						
						UltraGridCell cell = element.GetContext(typeof(UltraGridCell)) as UltraGridCell;
						
						// MRS 11/1/05 - BR07432						
						// GetContext on the HeaderUIElement was returning a cell
						// This didn't make sense, so I removed the cell context
						// from the HeaderUIElement GetContext.
						// Now we have to account for headers here.
						if (cell == null)
						{
							UltraGridColumn column = element.GetContext(typeof(UltraGridColumn)) as UltraGridColumn;
							if (column != null)
								cell = row.Cells[column.Key];
						}
						
						if (cell != null)
						{
							this.nearestCell = cell;
							this.minDistance = distance;
						}
					}
				}

				return true;
			}
		}

		#endregion // NearesetCellCallback Class

		#region FindNearesetCell

		internal static UltraGridCell FindNearesetCell( UIElement parentElem, UltraGridBand band, Point p )
		{
			NearesetCellCallback c = new NearesetCellCallback( band, p );

			TraverseDescendants( parentElem, c );

			return c.nearestCell;
		}

		#endregion // FindNearesetCell

		#region ItemMergeBorderHelper

		// SSP 12/1/03
		// Added following ItemMergeBorderHelper helper method.
		//
		internal static void ItemMergeBorderHelper( ref Rectangle rect, ILayoutItem item, Rectangle containerRect )
		{
			// SSP 12/1/03 UWG2641
			// Added support for cell and label insets.
			//
			Insets insets = RowLayoutColumnInfo.GetItemInsetsHelper( item );

			// Merge the borders with the previous cell.
			//
			// SSP 12/1/03 UWG2641
			// If there is a left inset, then don't merge.
			//
			//if ( rect.X > this.containerRect.X )
			if ( 0 == insets.Left && rect.X > containerRect.X )
			{
				rect.X--;
				rect.Width++;
			}

			// SSP 12/1/03 UWG2641
			// If there is a top inset, then don't merge.
			//
			//if ( rect.Y > this.containerRect.Y )
			if ( 0 == insets.Top && rect.Y > containerRect.Y )
			{
				rect.Y--;
				rect.Height++;
			}
		}

		#endregion // ItemMergeBorderHelper

		#region IsElementInActiveScrollRegion

		// SSP 12/1/04 - Merged Cell Feature
		// Added IsElementInActiveScrollRegion helper method.
		//
		internal static bool IsElementInActiveScrollRegion( UIElement elem )
		{
			RowColRegionIntersectionUIElement scrollRegionElem = 
				(RowColRegionIntersectionUIElement)elem.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );

			if ( null != scrollRegionElem )
			{
				RowScrollRegion rsr = scrollRegionElem.RowScrollRegion;
				ColScrollRegion csr = scrollRegionElem.ColScrollRegion;

				return null != rsr && null != csr && rsr.IsActiveScrollRegion && csr.IsActiveScrollRegion;
			}

			return false;
		}

		#endregion // IsElementInActiveScrollRegion

		#region GetAssociatedLayoutItem

		// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// 
		internal static ILayoutItem GetAssociatedLayoutItem( LayoutManagerBase lm, UIElement elem, ILayoutItem layoutItem )
		{
			foreach ( ILayoutItem item in lm.LayoutItems )
			{
				if ( elem is CellUIElementBase )
				{
					UltraGridColumn column = ((CellUIElementBase)elem).Column;
					if ( item == column )
						return item;

					if ( item is RowAutoSizeLayoutManagerHolder.CellLayoutItem 
						&& column == ((RowAutoSizeLayoutManagerHolder.CellLayoutItem)item).Column )
						return item;
				}
				else if ( elem is HeaderUIElement )
				{
					HeaderBase header = ((HeaderUIElement)elem).Header;
					if ( item == header )
						return item;
				}
				else if ( elem is SummaryValueUIElement )
				{
					SummaryValue sv = ((SummaryValueUIElement)elem).SummaryValue;
					UltraGridColumn column = null != sv ? sv.SummarySettings.SummaryPositionColumnResolved : null;
					if ( item is SummaryLayoutItem && column == ((SummaryLayoutItem)item).Column )
						return item;
				}
				// SSP 8/29/05 BR05818
				//
				else if ( layoutItem is UltraGridColumn && item is RowAutoSizeLayoutManagerHolder.CellLayoutItem )
				{
					RowAutoSizeLayoutManagerHolder.CellLayoutItem cellLayoutItem = (RowAutoSizeLayoutManagerHolder.CellLayoutItem)item;
                    if ( layoutItem == cellLayoutItem.Column )
						return cellLayoutItem;
				}
			}

			return null;
		}

		#endregion // GetAssociatedLayoutItem

		#region GetAssociatedGridBagConstraint

		// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// 
		internal static IGridBagConstraint GetAssociatedGridBagConstraint( ILayoutItem item )
		{
			if ( item is IGridBagConstraint )
				return (IGridBagConstraint)item;

			if ( item is SummaryLayoutItem )
				return ((SummaryLayoutItem)item).Column;

			if ( item is RowAutoSizeLayoutManagerHolder.CellLayoutItem )
				return ((RowAutoSizeLayoutManagerHolder.CellLayoutItem)item).Column;

			Debug.Assert( false );
			return null;
		}

		#endregion // GetAssociatedGridBagConstraint

		#region AccountForLabelPosition

		// SSP 8/16/05 BR05387
		// 
		internal static Size AccountForLabelPosition( UltraGridBand band, Size defaultSize )
		{
			Size size = new Size( 0, 0 );

			if ( band.AreColumnHeadersInSeparateLayoutArea )
			{
				if ( band.CardView )
					size.Height = defaultSize.Height;
				else
					size.Width = defaultSize.Width;
			}

			return size;
		}

		#endregion // AccountForLabelPosition

		#region Intersects

		// SSP 8/18/05 BR05463
		// 
		internal static bool Intersects( int xx1, int ww1, int xx2, int ww2 )
		{
			return Math.Min( xx1 + ww1, xx2 + ww2 ) - Math.Max( xx1, xx2 ) > 0;
		}

		#endregion // Intersects

		#region GetLogicalGridBagLayoutRect

		// SSP 9/27/07 BR26350
		// Added GetLogicalGridBagLayoutRect. This is used by the UltraGrid to find
		// out if a column spans entire row-layout. This is used for determining if
		// column qualifies for cell merging since cells can be merged only if there
		// are no other intervening cells from other columns between the cells of the
		// column in question from two consecutive rows. The easiest way would be to
		// simply check if the column's SpanY is the same as the vertical span height
		// of the entire layout. However this does not take into account a situation
		// where the column's SpanY is smaller however still there are no intervening
		// cells because the columns associated with what would be intervening cells
		// have been marked as LabelPosition of LabelOnly (their cells are hidden).
		// 
		/// <summary>
		/// Returns the union of logical dimensions of all the visible items. In other words,
		/// the returned rect is the logical bounds that contains all the items.
		/// </summary>
		/// <param name="dims"></param>
		/// <returns></returns>
		internal static Rectangle GetLogicalGridBagLayoutRect( GridBagLayoutItemDimensionsCollection dims )
		{
			int minX, minY, maxX, maxY;
			minX = minY = int.MaxValue;
			maxX = maxY = 0;

			foreach ( ILayoutItem item in dims )
			{
                // MRS - NAS 9.1 - Groups in RowLayout
                //
                #region Old Code
                //UltraGridColumn col = GridUtils.GetColumnFromLayoutItem( item );

                //if ( null == col || col != item && col.Header != item )
                //{
                //    Debug.Assert( false );
                //    continue;
                //}

                //bool isVisible = item.IsVisible;
                //if ( isVisible )
                //{
                //    LabelPosition labelPos = col.RowLayoutColumnInfo.LabelPositionResolved;
                //    if ( col == item )
                //    {
                //        if ( LabelPosition.LabelOnly == labelPos )
                //            isVisible = false;
                //    }
                //    else
                //    {
                //        if ( LabelPosition.None == labelPos )
                //            isVisible = false;
                //    }
                //}
                #endregion //Old Code
                //
                bool isVisible = item.IsVisible;
                RowLayoutColumnInfo rowLayoutColumnInfo = UltraGridBand.GetRowLayoutColumnInfo(item);
                if (rowLayoutColumnInfo == null)
                    continue;

                if ( isVisible )
                {
                    LabelPosition labelPos = rowLayoutColumnInfo.LabelPositionResolved;
                    switch (labelPos)
                    {
                        case LabelPosition.LabelOnly:
                            if (item is UltraGridColumn || item is GroupLayoutItemBase)
                                isVisible = false;

                            break;

                        case LabelPosition.None:
                            if (item is HeaderBase)
                                isVisible = false;

                            break;
                    }
                }

                if ( isVisible )
				{
					GridBagLayoutItemDimensions dim = dims[item];

					minX = Math.Min( minX, dim.OriginX );
					minY = Math.Min( minY, dim.OriginY );

					maxX = Math.Max( maxX, dim.OriginX + dim.SpanX );
					maxY = Math.Max( maxY, dim.OriginY + dim.SpanY );
				}
			}


			Rectangle rect = minX < maxX && minY < maxY
				? new Rectangle( minX, minY, maxX - minX, maxY - minY )
				: Rectangle.Empty;

			return rect;
		}

		#endregion // GetLogicalGridBagLayoutRect
	}

	#endregion // LayoutUtility Class

	#region LayoutContainerCalcSize Class
		
	internal class LayoutContainerCalcSize : Infragistics.Win.Layout.ILayoutContainer
	{
		private Rectangle containerRect = Rectangle.Empty;

		#region Constructor

		internal LayoutContainerCalcSize( )
		{
			this.Initialize( );
		}

		internal LayoutContainerCalcSize( Size size )
		{
			this.Initialize( size );
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//#endif
		//        internal LayoutContainerCalcSize( Rectangle containerRect )
		//        {
		//            this.Initialize( containerRect );
		//        }

		#endregion Not Used

		#endregion // Constructor

		#region Initialize

		internal void Initialize( )
		{
			this.Initialize( Rectangle.Empty );
		}

		internal void Initialize( Size size )
		{
			this.Initialize( new Rectangle( new Point( 0, 0 ), size ) );
		}

		internal void Initialize( Rectangle rect )
		{
			this.containerRect = rect;
		}

		#endregion // Initialize

		#region Implementation of ILayoutContainer

		#region ILayoutContainer.PositionItem

		void ILayoutContainer.PositionItem(Infragistics.Win.Layout.ILayoutItem item, System.Drawing.Rectangle rect, object containerContext)
		{
		}

		#endregion // ILayoutContainer.PositionItem

		#region ILayoutContainer.GetBounds

		System.Drawing.Rectangle ILayoutContainer.GetBounds(object containerContext)
		{
			return this.containerRect;
		}

		#endregion // ILayoutContainer.GetBounds

		#endregion // Implementation of ILayoutContainer
	}

	#endregion // LayoutContainerCalcSize Class

	#region LayoutContainerHelper Class

	internal sealed class LayoutContainerHelper
	{
		private LayoutContainerHelper( )
		{
		}

		#region ExtractUIElement

		internal static UIElement ExtractUIElement( UIElementsCollection oldElements, Type elementType )
		{
			if ( null != oldElements )
			{
				for ( int i = 0, count = oldElements.Count; i < count; i++ )
				{
					UIElement elem = oldElements[i];

					if ( null != elem && elem.GetType( ) == elementType )
					{
						oldElements[i] = null;
						return elem;
					}
				}
			}

			return null;
		}

		#endregion // ExtractUIElement

		#region GetContainerRectHelper

        // MRS - NAS 9.1 - Groups in RowLayout
        // Overload for backward compatilibity
        internal static Rectangle GetContainerRectHelper(
            UltraGridBand band,
            Rectangle rect,
            UIElementBorderStyle containerBorderStyle,
            UIElementBorderStyle itemBorderStyle)
        {
            return GetContainerRectHelper(band, rect, containerBorderStyle, itemBorderStyle, true);
        }

        internal static Rectangle GetContainerRectHelper( 
			UltraGridBand band, 
			Rectangle rect, 
			UIElementBorderStyle containerBorderStyle,
			UIElementBorderStyle itemBorderStyle, 
            // MRS - NAS 9.1 - Groups in RowLayout
            bool adjustForRowSelectors)
		{
			Rectangle containerRect = rect;
				
			bool rowBordersMergeable = band.Layout.CanMergeAdjacentBorders( band.BorderStyleRowResolved );
			bool cellBordersMergeable = band.Layout.CanMergeAdjacentBorders( band.BorderStyleCellResolved );
			int cellSpacing = band.CellSpacingResolved;

            // MRS - NAS 9.1 - Groups in RowLayout
            // Added this 'if' block. We don't want to account for row selectors when laying out
            // the interior content area of a group. 
            if (adjustForRowSelectors)
            {
                // When laying out header area or the summary area, we have to specify the exactly the
                // same container rect as the row cell area if we want to align the headers or summaries
                // exactly with the cells in the row cell area. The row cell area doesn't contain the 
                // row selectors however the header area and the summary area do. That's why we are 
                // subtracting the row selector width for when laying out items in all three areas. Not
                // only that, further below, we are also inflating the rect by row border width just like
                // we would do for the row cell area. Remember this method gets called for all three:
                // row cell area, header area and the summary area.
                //
                int rowSelectorWidth = band.RowSelectorExtent;
                if (rowSelectorWidth > 0)
                {
                    if (rowBordersMergeable)
                        rowSelectorWidth--;

                    containerRect.X += rowSelectorWidth;
                    containerRect.Width -= rowSelectorWidth;
                }
            }


			// Adjust the horizontal coordinates based on the row border style.
			//
			if ( ! cellBordersMergeable || ! rowBordersMergeable || cellSpacing > 0 )
			{
				int rowBorderWidth = band.Layout.GetBorderThickness( band.BorderStyleRowResolved );

				containerRect.Inflate( - rowBorderWidth, 0 );
			}

			// Unlike horizontal borders of headers, cells and summaries which have to be aligned 
			// with each other, vertical borders don't have to be aligned in the same manner
			// with items from different layout containers. So we don't need make sure the heights
			// are the same like we have to with the width. As a matter of fact, heights are typically
			// different since the header heights, cell heights and summary footer heights almost
			// always differ. So adjust the VERTICAL coordinates based on the container and 
			// item border styles.
			// 
			if ( cellSpacing > 0 ||  ! band.Layout.CanMergeAdjacentBorders( containerBorderStyle, itemBorderStyle ) )
			{
				containerRect.Inflate( 0, - band.Layout.GetBorderThickness( containerBorderStyle ) );
			}

			return containerRect;
		}

		#endregion // GetContainerRectHelper

		// SSP 5/14/03 - Hiding Individual Cells Feature
		// Added Hidden property off the cell to allow the user to be able to hide a cell.
		//
		#region IsCellHidden

		internal static bool IsCellHidden( UltraGridRow row, UltraGridColumn column )
		{
			// SSP 6/10/05 BR04499
			// In a filter row a cell that's not allocated could be hidden if the FilterOperandStyle
			// of the associated column resolves to None.
			// 
			//UltraGridCell cell = null != row && null != column ? row.GetCellIfAllocated( column ) : null;
			//return null != cell && cell.Hidden;
			return null != row && null != column && row.IsCellHidden( column );
		}

		#endregion // IsCellHidden

		#region IsRowLayoutDesignerElement
		
		internal static bool IsRowLayoutDesignerElement( UIElement elem )
		{
			DataAreaUIElement dataAreaElem = (DataAreaUIElement)elem.GetAncestor( typeof( DataAreaUIElement ) );

			return null != dataAreaElem && dataAreaElem.RowLayoutDesignerElement;
		}

		#endregion // IsRowLayoutDesignerElement
	}

	#endregion // LayoutContainerHelper Class

	#region LayoutContainerBase 
	
	// SSP 11/17/04
	// Created a base class for layout container implementations that lay out ui elements.
	//
	internal abstract class LayoutContainerBase : Infragistics.Win.Layout.ILayoutContainer
	{
		#region Private Variables

		protected Rectangle containerRect;
		protected Rectangle clipRect;
		protected UltraGridBand band = null;
		protected UIElementsCollection oldElements = null;

		// SSP 6/27/03 UWG2421
		// In layout designer, add all the elements, even the ones that would not be visible 
		// because they are outside of the clip rect. We need to do this because the in the
		// designer, the drag window uses the cell and header elements to draw itself when
		// that cell/header combination is being dragged.
		//
		protected bool isLayoutDesigner = false;

		#endregion // Private Variables

		#region Constructor

		protected LayoutContainerBase( )
		{
		}

		#endregion // Constructor

		#region ShouldPositionElement

		internal bool ShouldPositionElement( System.Drawing.Rectangle rect )
		{
			// SSP 6/27/03 UWG2421
			// In layout designer, add all the elements, even the ones that would not be visible 
			// because they are outside of the clip rect. We need to do this because the in the
			// designer, the drag window uses the cell and header elements to draw itself when
			// that cell/header combination is being dragged.
			//
			return this.clipRect.IntersectsWith( rect ) || this.isLayoutDesigner;
		}

		#endregion // ShouldPositionElement

		#region Implementation of ILayoutContainer

		public abstract void PositionItem(Infragistics.Win.Layout.ILayoutItem item, System.Drawing.Rectangle rect, object containerContext);

		System.Drawing.Rectangle ILayoutContainer.GetBounds(object containerContext)
		{
			return this.containerRect;
		}

		#endregion // Implementation of ILayoutContainer
	}

	#endregion // LayoutContainerBase 

	#region LayoutContainerHeaderArea Class

	internal class LayoutContainerHeaderArea : LayoutContainerBase
	{
		#region Private Variables

		private Rectangle colHeaderAreaRect;
		private BandHeadersUIElement containerElem = null;
		private bool headerBordersMergeable = false;

        //JDN 11/19/04 Added RowSelectorHeader
        private RowSelectorHeaderStyle rowSelectorHeaderStyle;
		// SSP 4/20/05 BR03339
		// 
        //private int rowSelectorHeaderWidth;

		#endregion // Private Variables

		#region Constructor
		
		private LayoutContainerHeaderArea( )
		{
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="colHeaderAreaRect"></param>
		//#endif
		//        internal LayoutContainerHeaderArea( UltraGridBand band, Rectangle colHeaderAreaRect )
		//        {
		//            this.Initialize( band, colHeaderAreaRect );
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="colHeaderAreaRect"></param>
		//        /// <param name="containerElem"></param>
		//        /// <param name="oldElements"></param>
		//#endif
		//        internal LayoutContainerHeaderArea( UltraGridBand band, Rectangle colHeaderAreaRect, 
		//            BandHeadersUIElement containerElem, UIElementsCollection oldElements )
		//        {
		//            this.Initialize( band, colHeaderAreaRect, containerElem, oldElements );
		//        }

		#endregion Not Used

		#endregion // Constructor

		#region Initialize

		internal void Initialize( UltraGridBand band, Rectangle colHeaderAreaRect )
		{
			this.Initialize( band, colHeaderAreaRect, null, null );            
		}

        // MRS - NAS 9.1 - Groups in RowLayout
        // Overload for backward compatibilty
        //
        internal void Initialize(
            UltraGridBand band,
            Rectangle colHeaderAreaRect,
            BandHeadersUIElement containerElem,
            UIElementsCollection oldElements
            )
        {
            this.Initialize(band, colHeaderAreaRect, containerElem, oldElements, null);
        }

		internal void Initialize( 
			UltraGridBand band, 
			Rectangle colHeaderAreaRect, 
			BandHeadersUIElement containerElem, 
			UIElementsCollection oldElements,
            // MRS - NAS 9.1 - Groups in RowLayout
            UltraGridGroup group)
		{
			this.band = band;
			UltraGridLayout layout = band.Layout;
			this.colHeaderAreaRect = colHeaderAreaRect;

			// SSP 11/8/04 UWG3699
			// If there are group-by columns then we need to take into account the group-by 
			// indentation when calculating the extent of the header area.
			//
			// ------------------------------------------------------------------------------------
			Rectangle workRect = colHeaderAreaRect;
			ViewStyleBase viewStyle = band.Layout.ViewStyleImpl;

			// SSP 10/27/05 BR07267
			// 
			// --------------------------------------------------------------------------------
			if ( viewStyle.IsOutlookGroupBy && viewStyle.BandHasFixedHeaders( band ) 
				|| HeaderPlacement.OncePerGroupedRowIsland == band.HeaderPlacementResolved )
			{
				int delta = layout.ViewStyleImpl.CalcGroupByConnectorsExtent( band );
				workRect.X += delta;
				workRect.Width -= delta;
			}
			
			// --------------------------------------------------------------------------------
			// ------------------------------------------------------------------------------------

            // MRS - NAS 9.1 - Groups in RowLayout     
            // Only adjust for Row Selectors if there is no group, or if 
            // the group is the first group. 
            //
            //this.containerRect = LayoutContainerHelper.GetContainerRectHelper( this.band, 
            //    workRect, UIElementBorderStyle.None, this.band.BorderStyleHeaderResolved);
            bool adjustForRowSelectors = (group == null)
                ? true
                : group.GroupLayoutItemHeaderContent.FirstItem;
            
            this.containerRect = LayoutContainerHelper.GetContainerRectHelper(this.band,
                workRect, UIElementBorderStyle.None, this.band.BorderStyleHeaderResolved,
                adjustForRowSelectors);


			this.containerElem = containerElem;
			this.oldElements = oldElements;
				
			this.clipRect = null != this.containerElem ? this.containerElem.ClipRect : Rectangle.Empty;

			this.headerBordersMergeable = layout.CanMergeAdjacentBorders( this.band.BorderStyleHeaderResolved );

            //JDN 11/19/04 Added RowSelectorHeader
            this.rowSelectorHeaderStyle = this.band.RowSelectorHeaderStyleResolved;
			// SSP 4/20/05 BR03339
			// 
            //this.rowSelectorHeaderWidth = this.band.RowSelectorExtent;

			// SSP 6/27/03 UWG2421
			// In layout designer, add all the elements, even the ones that would not be visible 
			// because they are outside of the clip rect. We need to do this because the in the
			// designer, the drag window uses the cell and header elements to draw itself when
			// that cell/header combination is being dragged.
			//
			this.isLayoutDesigner = null != this.containerElem 
				? LayoutContainerHelper.IsRowLayoutDesignerElement( this.containerElem ) : false;
		}

		#endregion Initialize

		#region CreateEmpty

		internal static LayoutContainerHeaderArea CreateEmpty( )
		{
			return new LayoutContainerHeaderArea( );
		}

		#endregion // CreateEmpty

		#region AdjustForMergedBorders

		private void AdjustForMergedBorders( ref Rectangle rect, ILayoutItem item )
		{
			if ( headerBordersMergeable )
			{
				// SSP 12/1/03 UWG2641
				// Added support for cell and label insets.
				//
				// --------------------------------------------------------------------
				LayoutUtility.ItemMergeBorderHelper( ref rect, item, this.containerRect );
				
				// --------------------------------------------------------------------
			}

			// Take into account the row selector. Extend the left most headers so
			// they occupy the area above the row selectors.
			//
			if ( rect.Left == this.containerRect.Left )
			{
				if ( this.colHeaderAreaRect.Left < this.containerRect.Left )
				{
					// SSP 4/20/05 BR03339
					// If the row selector header style is not ExtendFirstColumn thne we are extending 
					// the headers left to occupy the space over the row selectors here and then doing
					// the reverse in PositionItem. Instead simply do not extend the headers left here.
					// Added following condition.
					//
					if ( RowSelectorHeaderStyle.ExtendFirstColumn == this.rowSelectorHeaderStyle )
					{
						rect.X = this.colHeaderAreaRect.Left;
						rect.Width += this.containerRect.Left - this.colHeaderAreaRect.Left;
					}
					// SSP 7/25/05
					// Make sure that the first header and the row selector header elements don't overlap.
					// This is needed because the container rect is extended left by 1 pixel if the row
					// borders are mergeable and the row selectors are visible (see row element's 
					// position child elements for more info).
					// 
					else if ( RowSelectorHeaderStyle.SeparateElement == this.rowSelectorHeaderStyle 
						&& this.band.Layout.CanMergeAdjacentBorders( this.band.BorderStyleRowResolved ) )
					{
						rect.X++;
						rect.Width--;
					}
				}
			}

			// Like wise make sure that the last header extends all the way to the header area
			// extent.
			//
			if ( rect.Right == this.containerRect.Right )
			{
				rect.Width = this.colHeaderAreaRect.Right - rect.X;
			}
		}

		#endregion // AdjustForMergedBorders

		#region PositionItem

		public override void PositionItem(Infragistics.Win.Layout.ILayoutItem item, System.Drawing.Rectangle rect, object containerContext)
		{
			this.AdjustForMergedBorders( ref rect, item );

            // MRS - NAS 9.1 - Groups in RowLayout
			//if ( item is ColumnHeader )
            if (item is HeaderBase)
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//ColumnHeader header = (ColumnHeader)item;
                HeaderBase header = (HeaderBase)item;

				if ( null == this.containerElem )
				{
					// If the container elem is null, then we are setting the rects on the row layout info.
					//
                    // MRS - NAS 9.1 - Groups in RowLayout
					//header.Column.RowLayoutColumnInfo.CachedItemRectHeader = rect;
                    header.RowLayoutColumnInfo.CachedItemRectHeader = rect;

					// SSP 6/20/03
					// Set the first item to true if it's the left most item occupying the space above
					// the row selectors.
					//
					if ( rect.X == this.colHeaderAreaRect.X )
						header.FirstItem = true;
					else
						header.FirstItem = false;

					// Set the last item to false
					//
					header.LastItem = false;
				}
				else
				{
					// SSP 6/27/03 UWG2421
					// In layout designer, add all the elements, even the ones that would not be visible 
					// because they are outside of the clip rect. We need to do this because the in the
					// designer, the drag window uses the cell and header elements to draw itself when
					// that cell/header combination is being dragged.
					//
					// SSP 11/17/04
					// Centralized the logic in the base class.
					//
					//if ( ! this.isLayoutDesigner && ! this.clipRect.IntersectsWith( rect ) )
					if ( ! this.ShouldPositionElement( rect ) 
						// SSP 11/17/04
						// Added a way to hide the header or the cell of a column in the row layout mode.
						// This way you can do things like hide a row of headers or add unbound columns
						// and hide their cells to show grouping headers etc...
						//
                        // MRS - NAS 9.1 - Groups in RowLayout
						//|| ! header.Column.RowLayoutColumnInfo.ShouldPositionLabelElement )
                        || !header.RowLayoutColumnInfo.ShouldPositionLabelElement)
						return;

					// SSP 4/20/05 BR03339
					// We are extending the headers left to occupy the space over the row selectors in
					// AdjustForMergedBorders and then doing the reverse here. Instead simply do not
					// extend the headers left in the AdjustForMergedBorders. Commented out the 
					// following code.
					//
					

					// MD 2/12/09 - TFS13812
					// Added an extra parameter to AddElementHelper to specify whether the element is a right fixed header separator element.
					//this.containerElem.AddElementHelper( 
					//    this.oldElements, typeof( HeaderUIElement ), rect, header, false );
					this.containerElem.AddElementHelper( this.oldElements, typeof( HeaderUIElement ), rect, header, false, false );
				}
			}
            // MRS - NAS 9.1 - Groups in RowLayout
            else if (item is GroupLayoutItemHeaderContent)
            {
                GroupLayoutItemHeaderContent groupLayoutItemHeaderContent = (GroupLayoutItemHeaderContent)item;

                if (null == this.containerElem)
                {
                    // If the container elem is null, then we are setting the rects on the row layout info.
                    //                    
                    groupLayoutItemHeaderContent.RowLayoutGroupInfo.CachedItemRectHeader = rect;
                }
                else
                {
                    // SSP 6/27/03 UWG2421
                    // In layout designer, add all the elements, even the ones that would not be visible 
                    // because they are outside of the clip rect. We need to do this because the in the
                    // designer, the drag window uses the cell and header elements to draw itself when
                    // that cell/header combination is being dragged.
                    if (!this.ShouldPositionElement(rect)
                        || !groupLayoutItemHeaderContent.RowLayoutGroupInfo.ShouldPositionCellElement)
                        return;
                    
                    bool isFirstItem = rect.X == this.colHeaderAreaRect.X;
                    UltraGridGroup parentGroup = groupLayoutItemHeaderContent.Group.RowLayoutGroupInfo.ParentGroup;
                    while (isFirstItem && parentGroup != null)
                    {
                        if (parentGroup.GroupLayoutItemHeaderContent.FirstItem == false)
                            isFirstItem = false;

                        parentGroup = parentGroup.RowLayoutGroupInfo.ParentGroup;
                    }
                    
                    groupLayoutItemHeaderContent.FirstItem = isFirstItem;

                    LayoutContainerHeaderArea layoutContainerHeaderArea = new LayoutContainerHeaderArea();
                    layoutContainerHeaderArea.Initialize(
                        band,
                        rect,
                        containerElem,
                        oldElements,
                        groupLayoutItemHeaderContent.Group
                        );
                    groupLayoutItemHeaderContent.LayoutManager.LayoutContainer(layoutContainerHeaderArea, null);
                }
            }
            else
            {
                Debug.Assert(false, "Unknown type of item !");
            }
		}

		#endregion // PositionItem
	}

	#endregion // LayoutContainerHeaderArea Class

	#region LayoutContainerCellArea Class

	internal class LayoutContainerCellArea : LayoutContainerBase
	{
		#region Private Variables

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private Rectangle rowCellAreaRect;

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Split the RowCellAreaUIElement into RowCellAreaUIElementBase class and 
		// RowCellAreaUIElement class so the filter cell area ui element can derive from 
		// RowCellAreaUIElementBase .
		//
		//private RowCellAreaUIElement containerElem = null;
		private RowCellAreaUIElementBase containerElem = null;
		private bool sameRow = false;
		private bool dirtyCellElems = true;
		private int cellSpacing = 0;
		private bool cellBordersMergable = false;

		// SSP 7/8/05 - NAS 5.3 Empty Rows
		// 
		private UltraGridRow row = null;

        #endregion // Private Variables

		#region Constructor

		private LayoutContainerCellArea( )
		{
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="rowCellAreaRect"></param>
		//#endif
		//        internal LayoutContainerCellArea( UltraGridBand band, Rectangle rowCellAreaRect )
		//        {
		//            this.Initialize( band, rowCellAreaRect );
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="rowCellAreaRect"></param>
		//        /// <param name="containerElem"></param>
		//        /// <param name="oldElements"></param>
		//        /// <param name="sameRow"></param>
		//        /// <param name="dirtyCellElems"></param>
		//#endif
		//        internal LayoutContainerCellArea( UltraGridBand band, Rectangle rowCellAreaRect,
		//            RowCellAreaUIElement containerElem, UIElementsCollection oldElements, bool sameRow, bool dirtyCellElems )
		//        {
		//            this.Initialize( band, rowCellAreaRect,	containerElem, oldElements, sameRow, dirtyCellElems );
		//        }

		#endregion Not Used

		#endregion // Constructor

		#region Initialize

		internal void Initialize( UltraGridBand band, Rectangle rowCellAreaRect )
		{            
			this.Initialize( band, rowCellAreaRect, null, null, false, false );            
		}

        // MRS - NAS 9.1 - Groups in RowLayout
        // Overload for backward compatibility
        //
        internal void Initialize(
            UltraGridBand band,
            Rectangle rowCellAreaRect,
            RowCellAreaUIElementBase containerElem,
            UIElementsCollection oldElements,
            bool sameRow,
            bool dirtyCellElems)
        {
            this.Initialize(band, rowCellAreaRect, containerElem, oldElements, sameRow, dirtyCellElems, null);
        }

		internal void Initialize( 
			UltraGridBand band, 
			Rectangle rowCellAreaRect,
			// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			// Split the RowCellAreaUIElement into RowCellAreaUIElementBase class and 
			// RowCellAreaUIElement class so the filter cell area ui element can derive from 
			// RowCellAreaUIElementBase .
			//
			//RowCellAreaUIElement containerElem, 
			RowCellAreaUIElementBase containerElem, 
			UIElementsCollection oldElements, 
			bool sameRow, 
			bool dirtyCellElems,
            // MRS - NAS 9.1 - Groups in RowLayout
            UltraGridGroup group)
		{
			this.band = band;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.rowCellAreaRect = rowCellAreaRect;

			UIElementBorderStyle borderStyleRow = this.band.BorderStyleRowResolved;
			UIElementBorderStyle borderStyleCell = this.band.BorderStyleCellResolved;
			this.cellSpacing = this.band.CellSpacingResolved;
			this.cellBordersMergable = this.band.Layout.CanMergeAdjacentBorders( borderStyleCell );

            // MRS - NAS 9.1 - Groups in RowLayout
            bool adjustForRowSelectors = (group == null)
                ? true
                : group.GroupLayoutItemCellContent.FirstItem;                    

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.containerRect = LayoutContainerHelper.GetContainerRectHelper( this.band, 
			//    this.rowCellAreaRect, borderStyleRow, borderStyleCell );
			this.containerRect = LayoutContainerHelper.GetContainerRectHelper( this.band,
				rowCellAreaRect, borderStyleRow, borderStyleCell,
                // MRS - NAS 9.1 - Groups in RowLayout
                adjustForRowSelectors);

			this.containerElem = containerElem;
			this.oldElements = oldElements;
			this.sameRow = sameRow;
			this.dirtyCellElems = dirtyCellElems;
			this.clipRect = null != this.containerElem ? this.containerElem.ClipRect : Rectangle.Empty;

			// SSP 7/8/05 - NAS 5.3 Empty Rows
			// 
			this.row = null != this.containerElem ? this.containerElem.Row : null;

			// SSP 6/27/03 UWG2421
			// In layout designer, add all the elements, even the ones that would not be visible 
			// because they are outside of the clip rect. We need to do this because the in the
			// designer, the drag window uses the cell and header elements to draw itself when
			// that cell/header combination is being dragged.
			//
			this.isLayoutDesigner = null != this.containerElem 
				? LayoutContainerHelper.IsRowLayoutDesignerElement( this.containerElem ) : false;
		}

		#endregion // Initialize

		#region CreateEmpty

		internal static LayoutContainerCellArea CreateEmpty( )
		{
			return new LayoutContainerCellArea( );
		}

		#endregion // CreateEmpty

		#region AdjustForMergedBorders

		private void AdjustForMergedBorders( ref Rectangle rect, ILayoutItem item )
		{
			// Adjust the cell rect to allow for cell spacing.
			//
			if ( this.cellSpacing > 0 )
			{
				// Only adjust the rect if we are laying out the cell elements. When we are just figuring out
				// the cell rects, which are cached off RowLayoutColumnInfo and returned by
				// RowLayoutColumnInfo.CachedItemRectCell proeprty, we shouldn't adjust for cell spacing. This is
				// so that the CachedItemRectCell retruns a rect that includes the cell spacing. This
				// maintains the backward compatiblity when it comes to cell selection becase cell section
				// the way it currently works is that the mouse doesn't have to moved inside of the cell for
				// it to be selected. It only has to be moved inside the cell rect that includes the spacing.
				// NOTE: Also doing this makes it easier for the column resizing to work in regular view
				// since the column resizing doesn't have to take into account cell spacing because the
				// CachedItemRectCell includes the cellspacing.
				//
				if ( null != this.containerElem )
					rect.Inflate( - this.cellSpacing, - this.cellSpacing );
				return;
			}

			if ( this.cellBordersMergable )
			{
				// SSP 12/1/03 UWG2641
				// Added support for cell and label insets.
				//
				// --------------------------------------------------------------------
				LayoutUtility.ItemMergeBorderHelper( ref rect, item, this.containerRect );
				
				// --------------------------------------------------------------------
			}

			// SSP 7/8/05 - NAS 5.3 Empty Rows
			// Depending on the empty row style, the empty row may need to extend the first cell left.
			// Call the new AdjustCellUIElementRect method for that.
			// 
			if ( null != this.containerElem && null != this.row )
			{
				UltraGridColumn column = GridUtils.GetColumnFromLayoutItem( item );
				if ( null != column )
					this.row.AdjustCellUIElementRect( this.containerElem, column, ref rect );
			}
		}
		
		#endregion // AdjustForMergedBorders

		#region PositionItem

		public override void PositionItem(Infragistics.Win.Layout.ILayoutItem item, System.Drawing.Rectangle rect, object containerContext)
		{
			// SSP 5/12/05 - NAS 5.2 Filter Row
			// Filter cells can have different sizes (for example if the FilterOperatorLocation
			// is AboveOperand). Therefore filter row uses CellLayoutItem as the items to represent
			// cells. If that's the case then convert it into a column items since the positioning
			// logic assumes items are either columns or headers.
			//
			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( item is RowAutoSizeLayoutManagerHolder.CellLayoutItem )
			//    item = ((RowAutoSizeLayoutManagerHolder.CellLayoutItem)item).Column;
			RowAutoSizeLayoutManagerHolder.CellLayoutItem cellLayoutItem = item as RowAutoSizeLayoutManagerHolder.CellLayoutItem;
			if ( cellLayoutItem != null )
				item = cellLayoutItem.Column;

			this.AdjustForMergedBorders( ref rect, item );

			// SSP 7/8/05 - Optimizations
			// Row can be null (if container elem is null).
			// 
			UltraGridRow row = this.row;

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( item is UltraGridColumn )
			//{
			//    UltraGridColumn column = (UltraGridColumn)item;
			UltraGridColumn column = item as UltraGridColumn;

			if ( column != null )
			{
				if ( null == this.containerElem )
				{
					// If the container elem is null, then we are setting the rects on the row layout info.
					//
					column.RowLayoutColumnInfo.CachedItemRectCell = rect;
				}
				else
				{
					// SSP 6/27/03 UWG2421
					// In layout designer, add all the elements, even the ones that would not be visible 
					// because they are outside of the clip rect. We need to do this because the in the
					// designer, the drag window uses the cell and header elements to draw itself when
					// that cell/header combination is being dragged.
					//
					// SSP 11/17/04
					// Centralized the logic in the base class.
					//
					//if ( ! isLayoutDesigner && ! this.clipRect.IntersectsWith( rect ) )
					if ( ! this.ShouldPositionElement( rect ) 
						// SSP 11/17/04
						// Added a way to hide the header or the cell of a column in the row layout mode.
						// This way you can do things like hide a row of headers or add unbound columns
						// and hide their cells to show grouping headers etc...
						//
						|| ! column.RowLayoutColumnInfo.ShouldPositionCellElement )						
						return;

					// SSP 7/8/05 - Optimizations
					// 
					//UltraGridRow row = this.containerElem.Row;

					// Check to see if the cell's Hidden property is set to true.
					//
					if ( ! LayoutContainerHelper.IsCellHidden( row, column ) )
					{
						this.containerElem.AddElementHelper( this.oldElements, typeof( CellUIElementBase ),	rect,
							this.clipRect, row, column, ref this.sameRow, this.dirtyCellElems, true );
					}
				}
			}
            // MRS - NAS 9.1 - Groups in RowLayout
            // Commented out the section for ColumnHeader and replaced it with a section for HeaderBase.                
            #region Old Code
            //else if ( item is ColumnHeader )
            //{
            //    ColumnHeader header = (ColumnHeader)item;

            //    // MD 8/3/07 - 7.3 Performance
            //    // Prevent calling expensive getters multiple times
            //    UltraGridColumn headerColumn = header.Column;

            //    if ( null == this.containerElem )
            //    {
            //        // If the container elem is null, then we are setting the rects on the row layout info.
            //        //
            //        // MD 8/3/07 - 7.3 Performance
            //        // Prevent calling expensive getters multiple times
            //        //header.Column.RowLayoutColumnInfo.CachedItemRectHeader = rect;
            //        headerColumn.RowLayoutColumnInfo.CachedItemRectHeader = rect;
            //    }
            //    else
            //    {
            //        // SSP 6/27/03 UWG2421
            //        // In layout designer, add all the elements, even the ones that would not be visible 
            //        // because they are outside of the clip rect. We need to do this because the in the
            //        // designer, the drag window uses the cell and header elements to draw itself when
            //        // that cell/header combination is being dragged.
            //        //
            //        // SSP 11/17/04
            //        // Centralized the logic in the base class.
            //        //
            //        //if ( ! this.isLayoutDesigner && ! this.clipRect.IntersectsWith( rect ) )
            //        if ( ! this.ShouldPositionElement( rect ) 
            //            // SSP 11/17/04
            //            // Added a way to hide the header or the cell of a column in the row layout mode.
            //            // This way you can do things like hide a row of headers or add unbound columns
            //            // and hide their cells to show grouping headers etc...
            //            //
            //            // MD 8/3/07 - 7.3 Performance
            //            // Prevent calling expensive getters multiple times
            //            //|| ! header.Column.RowLayoutColumnInfo.ShouldPositionLabelElement )
            //            || !headerColumn.RowLayoutColumnInfo.ShouldPositionLabelElement )
            //            return;

            //        // SSP 7/8/05 - Optimizations
            //        // 
            //        //UltraGridRow row = this.containerElem.Row;

            //        // Check to see if the cell's Hidden property is set to true.
            //        //
            //        // MD 8/3/07 - 7.3 Performance
            //        // Prevent calling expensive getters multiple times
            //        //if ( ! LayoutContainerHelper.IsCellHidden( row, header.Column ) )
            //        if ( !LayoutContainerHelper.IsCellHidden( row, headerColumn ) )
            //        {
            //            HeaderUIElement headerElem = (HeaderUIElement)LayoutContainerHelper.ExtractUIElement( this.oldElements, typeof( HeaderUIElement ) );

            //            if ( null == headerElem )
            //                headerElem = new HeaderUIElement( this.containerElem );

            //            headerElem.InitializeHeader( header );
            //            headerElem.Rect = rect;
            //            this.containerElem.ChildElements.Add( headerElem );
            //        }
            //    }
            //}
            #endregion //Old Code
            else if (item is HeaderBase)
            {
                HeaderBase header = (HeaderBase)item;
                IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = header.RowLayoutColumnInfoProvider;

                if (null == this.containerElem)
                {
                    // If the container elem is null, then we are setting the rects on the row layout info.
                    //

                    iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.CachedItemRectHeader = rect;
                }
                else
                {
                    // SSP 6/27/03 UWG2421
                    // In layout designer, add all the elements, even the ones that would not be visible 
                    // because they are outside of the clip rect. We need to do this because the in the
                    // designer, the drag window uses the cell and header elements to draw itself when
                    // that cell/header combination is being dragged.
                    //
                    // SSP 11/17/04
                    // Centralized the logic in the base class.
                    //
                    //if ( ! this.isLayoutDesigner && ! this.clipRect.IntersectsWith( rect ) )
                    if (!this.ShouldPositionElement(rect)
                        // SSP 11/17/04
                        // Added a way to hide the header or the cell of a column in the row layout mode.
                        // This way you can do things like hide a row of headers or add unbound columns
                        // and hide their cells to show grouping headers etc...
                        //
                        // MD 8/3/07 - 7.3 Performance
                        // Prevent calling expensive getters multiple times
                        //|| ! header.Column.RowLayoutColumnInfo.ShouldPositionLabelElement )
                        || !iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ShouldPositionLabelElement)
                        return;

                    // Check to see if the cell's Hidden property is set to true.
                    //                    
                    if (header is GroupHeader ||
                        !LayoutContainerHelper.IsCellHidden(row, iProvideRowLayoutColumnInfo as UltraGridColumn))
                    {
                        HeaderUIElement headerElem = (HeaderUIElement)LayoutContainerHelper.ExtractUIElement(this.oldElements, typeof(HeaderUIElement));

                        if (null == headerElem)
                            headerElem = new HeaderUIElement(this.containerElem);

                        headerElem.InitializeHeader(header);
                        headerElem.Rect = rect;
                        this.containerElem.ChildElements.Add(headerElem);
                    }
                }
            }
            // MRS - NAS 9.1 - Groups in RowLayout
            else if (item is GroupLayoutItemCellContent)
            {
                GroupLayoutItemCellContent groupLayoutItemCellContent = (GroupLayoutItemCellContent)item;

                if (null == this.containerElem)
                {
                    // If the container elem is null, then we are setting the rects on the row layout info.
                    //                    
                    groupLayoutItemCellContent.RowLayoutGroupInfo.CachedItemRectCell = rect;
                }
                else
                {
                    // SSP 6/27/03 UWG2421
                    // In layout designer, add all the elements, even the ones that would not be visible 
                    // because they are outside of the clip rect. We need to do this because the in the
                    // designer, the drag window uses the cell and header elements to draw itself when
                    // that cell/header combination is being dragged.
                    if (!this.ShouldPositionElement(rect)
                        || !groupLayoutItemCellContent.RowLayoutGroupInfo.ShouldPositionCellElement)
                        return;
                                        
                    LayoutContainerCellArea layoutContainerCellArea = new LayoutContainerCellArea();
                    layoutContainerCellArea.Initialize(
                        band, 
                        rect, 
                        containerElem, 
                        oldElements, 
                        sameRow, 
                        dirtyCellElems,
                        groupLayoutItemCellContent.Group);
                    
                    groupLayoutItemCellContent.LayoutManager.LayoutContainer(layoutContainerCellArea, null);
                }
            }
            else
            {
                Debug.Assert(false, "Unknown type of item !");
            }
		}

		#endregion // PositionItem
	}

	#endregion // LayoutContainerCellArea Class

	#region SummaryLayoutItem Class
	
	internal class SummaryLayoutItem : ILayoutItem
	{
		private UltraGridColumn column = null;
		private SummarySettings[] summaries = null;
		private int[] summaryHeights = null;

		// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		private SummaryDisplayAreaContext summaryDisplayAreaContext = null;

		#region Constructor

		// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added an overload of constructor that takes in summaryDisplayAreaContext.
		//
		internal SummaryLayoutItem( UltraGridColumn column ) 
			: this( column, SummaryDisplayAreaContext.AllExceptGroupbySummaries )
		{
		}

		// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal SummaryLayoutItem( UltraGridColumn column )
		internal SummaryLayoutItem( UltraGridColumn column, SummaryDisplayAreaContext summaryDisplayAreaContext )
		{
			if ( null == column )
				throw new ArgumentNullException( "column" );

			this.column = column;

			// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//this.summaries = this.column.Band.Summaries.GetFixedSummariesForDisplayColumn( column );
			GridUtils.ValidateNull( summaryDisplayAreaContext );
			this.summaryDisplayAreaContext = summaryDisplayAreaContext;
			this.summaries = this.column.Band.Summaries.GetFixedSummariesForDisplayColumn( summaryDisplayAreaContext, column );

			if ( null == this.summaries )
				this.summaries = new SummarySettings[0];
			
			this.CalcSummaryHeights( );
		}

		#endregion // Constructor

		#region Column

		// SSP 4/14/05 - NAS 5.2 Extension of Summaries Functionality
		//
		internal UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		#endregion // Column

		#region CalcSummaryHeights

		// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
		// Implemented support for multiple summaries in row layout mode.
		//
		private void CalcSummaryHeights( )
		{
			AppearanceData appData = new AppearanceData( );
			AppearancePropFlags flags = AppearancePropFlags.FontData;

			// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
			// Pass along the new summaryDisplayAreaContext parameter.
			// 
			//SummaryValuesCollection.ResolveSummaryValueAppearanceHelper( this.column.Band, ref appData, ref flags );
			SummaryValuesCollection.ResolveSummaryValueAppearanceHelper( this.column.Band, ref appData, ref flags, this.summaryDisplayAreaContext );

			int lineHeight = this.column.Band.Layout.CalculateFontHeight( ref appData );

			summaryHeights = new int[ this.summaries.Length ];
			for ( int i = 0; i < this.summaries.Length; i++ )
			{
				int height = lineHeight * Math.Max( 1, this.summaries[i].Lines );

				height += 2 * this.column.Band.Layout.GetBorderThickness( this.column.Band.BorderStyleSummaryValueResolved );

				height += 2 * this.column.Band.CellPaddingResolved;

				summaryHeights[i] = height;
			}
		}

		#endregion // CalcSummaryHeights

		#region Summaries
		
		internal SummarySettings[] Summaries
		{
			get
			{
				return this.summaries;
			}
		}

		#endregion // Summary

		#region SummaryHeights
		
		internal int[] SummaryHeights
		{
			get
			{
				return this.summaryHeights;
			}
		}

		#endregion // SummaryHeights

		#region Implementation of ILayoutItem
	
		System.Drawing.Size ILayoutItem.PreferredSize
		{
			get
			{
				// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
				// Implemented support for multiple summaries in row layout mode.
				//
				int height = UltraGridBand.ArrSum( this.summaryHeights );
			
				// SSP 9/9/05 BR06184
				// Use the ILayoutItem.PreferredSize implementation instead because that
				// takes into account the LabelPosition setting of LabelOnly.
				//
				//Size size = this.column.PreferredSize;
				Size size = ((ILayoutItem)this.column).PreferredSize;

				// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
				// If the layout is for summary footer in group-by rows then compress the 
				// vertical space between summary values. This is the behavior that JoeD 
				// and I decided to be appropriate. Otherwise the group-by row could be
				// relaly tall.
				//
				//if ( size.Height < height )
				if ( size.Height < height || null != this.summaryDisplayAreaContext && this.summaryDisplayAreaContext.IsGroupByRowSummaries )
					size.Height = height;

				return size;
			}
		}

		System.Drawing.Size ILayoutItem.MinimumSize
		{
			get
			{
				// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
				// If the layout is for summary footer in group-by rows then compress the 
				// vertical space between summary values. This is the behavior that JoeD 
				// and I decided to be appropriate. Otherwise the group-by row could be
				// relaly tall.
				//
				//return this.column.MinimumSize;
				// SSP 9/9/05 BR06184
				// Use the ILayoutItem.MinimumSize implementation instead because that
				// takes into account the LabelPosition setting of LabelOnly.
				//
				//Size size = this.column.MinimumSize;
				Size size = ((ILayoutItem)this.column).MinimumSize;

				if ( null != this.summaryDisplayAreaContext && this.summaryDisplayAreaContext.IsGroupByRowSummaries )
					size.Height = 0;

				return size;
			}
		}

		bool ILayoutItem.IsVisible
		{
			get
			{
				return true;
			}
		}

		#endregion // Implementation of ILayoutItem

	}

	#endregion // SummaryLayoutItem Class

	#region LayoutContainerSummaryArea Class

	internal class LayoutContainerSummaryArea : Infragistics.Win.Layout.ILayoutContainer
	{
		#region Private Variables

		private Rectangle summaryAreaRect;
		private Rectangle containerRect;
		private Rectangle clipRect;
		private UltraGridBand band = null;
		private RowsCollection rows = null;
		private FixedSummaryLineUIElement containerElem = null;
		private UIElementsCollection oldElements = null;
		private bool summaryBordersMergeable = false;

		// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
		//
		private int clipLeft = int.MinValue;

		#endregion // Private Variables

		#region Constructor

		private LayoutContainerSummaryArea( )
		{
		}

		internal LayoutContainerSummaryArea( 
			FixedSummaryLineUIElement containerElem, 
			UIElementsCollection oldElements,
			// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
			// Added clipLeft parameter.
			//
			int clipLeft )
		{
            // MRS - NAS 9.1 - Groups in RowLayout
			//this.Initialize( containerElem, oldElements, clipLeft );
            this.Initialize(containerElem, oldElements, clipLeft, containerElem.Rect, null);
		}

		#endregion // Constructor

		#region Initialize

		internal void Initialize( 
			FixedSummaryLineUIElement containerElem, 
			UIElementsCollection oldElements,
			// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
			// Added clipLeft parameter.
			//
			int clipLeft,
            // MRS - NAS 9.1 - Groups in RowLayout
            Rectangle rect,
            GroupLayoutItemSummaryContent groupLayoutItemSummaryContent)
		{
			this.containerElem = containerElem;
			this.rows = (RowsCollection)containerElem.GetContext( typeof( RowsCollection ) );
			this.band = rows.Band;

            // MRS - NAS 9.1 - Groups in RowLayout
			//this.summaryAreaRect = this.containerElem.Rect;
            this.summaryAreaRect = rect;

			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			//
			SummaryDisplayAreaContext summariesDisplayAreaContext = containerElem.SummaryDisplayAreaContext;
			this.clipLeft = clipLeft;

			// SSP 5/23/03 - Fixed headers
			// Take into account the fact the row cell area element could have been shrinked in
			// the fixed headers mode. However we still need to pass in the whole row cell area
			// rect to the layout manager and not the shrunk rect.
			//
			if ( this.band.UseFixedHeaders )
			{
				ColScrollRegion csr = (ColScrollRegion)this.containerElem.GetContext( typeof( ColScrollRegion ), true );

				if ( null != csr )
				{
					int delta = band.GetFixedHeaders_OriginDelta( csr );
					summaryAreaRect.X -= delta;

					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// Don't add delta if the summary footer is in a group-by row because the 
					// GroupByRowUIElement.PositionChildElementsHelper method uses the complete
					// band extent as the extent of the summary footer.
					//
					// summaryAreaRect.Width += delta;
					if ( null == summariesDisplayAreaContext || ! summariesDisplayAreaContext.IsGroupByRowSummaries )
						summaryAreaRect.Width += delta;
				}
			}

			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			// Since summary footers of group-by rows extend left beyond the row selectors
			// of the non-group-by rows, we need to remove that extension when calling 
			// LayoutContainerHelper.GetContainerRectHelper.
			//
			// --------------------------------------------------------------------------------
			//this.containerRect = LayoutContainerHelper.GetContainerRectHelper( this.band, 
			//	this.summaryAreaRect, this.containerElem.BorderStyle, this.band.BorderStyleSummaryValueResolved );
			Rectangle tmpRect = this.summaryAreaRect;
			if ( null != summariesDisplayAreaContext && UltraGridSummaryRow.ShouldLeftAlignGroupByRowsFooter( summariesDisplayAreaContext, rows ) )
			{
				int groupBySummariesIndent = this.band.CalcGroupBySummariesIndent( this.rows );
				tmpRect.X += groupBySummariesIndent;
				tmpRect.Width -= groupBySummariesIndent;
				this.summaryAreaRect = tmpRect;
			}

            // MRS - NAS 9.1 - Groups in RowLayout
            bool adjustForRowSelectors = (groupLayoutItemSummaryContent == null)
                ? true
                : groupLayoutItemSummaryContent.FirstItem;

			this.containerRect = LayoutContainerHelper.GetContainerRectHelper( this.band, 
				tmpRect, this.containerElem.BorderStyle, this.band.BorderStyleSummaryValueResolved,
                // MRS - NAS 9.1 - Groups in RowLayout
                adjustForRowSelectors);
			// --------------------------------------------------------------------------------

			// SSP 6/26/03 
			// We need to pass in the container rect that's exactly the same as the row cell
			// area rect so that the summaries get aligned up with the cells in the row
			// cell area. We are getting the summary line element as the container rect which 
			// could be smaller than the summary footer because of the summary footer borders. 
			// 
			// ----------------------------------------------------------------------------
			SummaryFooterUIElement summaryFooterElem = 
				(SummaryFooterUIElement)this.containerElem.GetAncestor( typeof( SummaryFooterUIElement ) );
			if ( null != summaryFooterElem && ! this.band.Layout.CanMergeAdjacentBorders( summaryFooterElem.BorderStyle ) )
			{
				int footerBorderWidth = this.band.Layout.GetBorderThickness( summaryFooterElem.BorderStyle );
				this.containerRect.Inflate( footerBorderWidth, 0 );
			}
			// ----------------------------------------------------------------------------

			this.oldElements = oldElements;

			this.clipRect = null != this.containerElem ? this.containerElem.ClipRect : Rectangle.Empty;

			// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
			// Summaries in group-by row need to be clipped so they don't overlap the group-by row
			// description element.
			//
			if ( clipRect.X < this.clipLeft )
			{
				clipRect.Width -= this.clipLeft - clipRect.X;
				clipRect.X = this.clipLeft;
			}

			this.summaryBordersMergeable = this.band.Layout.CanMergeAdjacentBorders( this.band.BorderStyleSummaryValueResolved );
		}

		#endregion // Initialize

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region CreateEmpty

		//#if DEBUG
		//        /// <summary>
		//        /// Creates and returns an empty instance.
		//        /// </summary>
		//        /// <returns></returns>
		//#endif
		//        internal static LayoutContainerSummaryArea CreateEmpty( )
		//        {
		//            return new LayoutContainerSummaryArea( );
		//        }

		//        #endregion // CreateEmpty

		#endregion Not Used

		#region AdjustForMergedBorders

		private void AdjustForMergedBorders( ref Rectangle rect, ILayoutItem item )
		{
			if ( this.summaryBordersMergeable )
			{
				// Merge the borders with the previous cell.
				//
				if ( rect.X > this.containerRect.X )
				{
					rect.X--;
					rect.Width++;
				}

				if ( rect.Y > this.containerRect.Y )
				{
					rect.Y--;
					rect.Height++;
				}
			}

			// Take into account the row selector. Extend the left most headers so
			// they occupy the area above the row selectors.
			//
			if ( rect.Left == this.containerRect.Left )
			{
				// SSP 6/26/03
				//
				//if ( this.summaryAreaRect.Left < this.containerRect.Left )
				//{
					rect.X = this.summaryAreaRect.Left;
					rect.Width += this.containerRect.Left - this.summaryAreaRect.Left;
				//}
			}

			// Like wise make sure that the last header extends all the way to the header area
			// extent.
			//
			if ( rect.Right == this.containerRect.Right )
			{
				rect.Width = this.summaryAreaRect.Right - rect.X;
			}
		}

		#endregion // AdjustForMergedBorders

		#region Implementation of ILayoutContainer

		#region ILayoutContainer.PositionItem

		void ILayoutContainer.PositionItem(Infragistics.Win.Layout.ILayoutItem item, System.Drawing.Rectangle rect, object containerContext)
		{
			this.AdjustForMergedBorders( ref rect, item );

			if ( ! this.clipRect.IntersectsWith( rect ) )
				return;

			UIElement addedElem = null;

			if ( item is SummaryLayoutItem )
			{
				SummaryLayoutItem summaryLayoutItem = (SummaryLayoutItem)item;

				// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
				// Implemented support for multiple summaries in row layout mode.
				//
				SummarySettings[] summaries = summaryLayoutItem.Summaries;
				if ( null != summaries && summaries.Length > 0 && UltraGridBand.ArrSum( summaryLayoutItem.SummaryHeights ) > 0 )
				{					
					int top = rect.Y;
					float factor = rect.Height / UltraGridBand.ArrSum( summaryLayoutItem.SummaryHeights );
					float slack = 0.0f;
					for ( int i = 0; i < summaries.Length; i++ )
					{
						SummaryValue summaryValue = this.rows.SummaryValues.GetSummaryValueFor( summaries[i] );
						if ( null != summaryValue )
						{
							SummaryValueUIElement summaryElem = this.containerElem.InternalGetSummaryValueUIElement( oldElements, summaryValue );
							int tmpHeight;

							bool isLastItem = i == summaries.Length - 1;

							if ( isLastItem )
							{
								tmpHeight = rect.Bottom - top;
							}
							else
							{
								tmpHeight = (int)factor * summaryLayoutItem.SummaryHeights[ i ];
								slack += factor * summaryLayoutItem.SummaryHeights[ i ] - tmpHeight;

								if ( slack > 1.0f )
								{
									slack--;
									tmpHeight++;
								}
							}

							summaryElem.Rect = new Rectangle( rect.X, top, rect.Width, 
											tmpHeight + ( this.summaryBordersMergeable && ! isLastItem ? 1 : 0 ) );
							this.containerElem.ChildElements.Add( summaryElem );
							addedElem = summaryElem;

							top += tmpHeight;
						}
					}
				}
			}
            // MRS - NAS 9.1 - Groups in RowLayout
			//else if ( item is ColumnHeader )
            else if (item is HeaderBase)
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//ColumnHeader header = (ColumnHeader)item;
                HeaderBase header = (HeaderBase)item;

				HeaderUIElement headerElem = (HeaderUIElement)LayoutContainerHelper.ExtractUIElement( this.oldElements, typeof( HeaderUIElement ) );

				if ( null == headerElem )
					headerElem = new HeaderUIElement( this.containerElem );

				headerElem.InitializeHeader( header );
				headerElem.Rect = rect;
				this.containerElem.ChildElements.Add( headerElem );
				addedElem = headerElem;
			}
            // MRS - NAS 9.1 - Groups in RowLayout
            else if (item is GroupLayoutItemSummaryContent)
            {
                GroupLayoutItemSummaryContent groupLayoutItemSummaryContent = (GroupLayoutItemSummaryContent)item;

                bool isFirstItem = rect.X == this.summaryAreaRect.X;
                UltraGridGroup parentGroup = groupLayoutItemSummaryContent.Group.RowLayoutGroupInfo.ParentGroup;
                while (isFirstItem && parentGroup != null)
                {
                    if (parentGroup.GroupLayoutItemSummaryContent.FirstItem == false)
                        isFirstItem = false;

                    parentGroup = parentGroup.RowLayoutGroupInfo.ParentGroup;
                }

                groupLayoutItemSummaryContent.FirstItem = isFirstItem;                

                LayoutContainerSummaryArea layoutContainerSummaryArea = new LayoutContainerSummaryArea();
                layoutContainerSummaryArea.Initialize(
                    containerElem,
                    oldElements,
                    clipLeft,
                    rect,
                    groupLayoutItemSummaryContent);

                groupLayoutItemSummaryContent.LayoutManager.LayoutContainer(layoutContainerSummaryArea, null);
            }
            else
            {
                Debug.Assert(false, "Unknown type of item !");
            }

			// SSP 5/6/05 - NAS 5.2 Extension of Summaries Functionality
			// If the summaries are in group-by row then make sure they do not overlap the
			// group-by description element.
			// 
			if ( rect.X < this.clipLeft && rect.Right > this.clipLeft && null != addedElem )
			{
				RowCellAreaUIElementBase.AddFixedCellSeparatorUIElement( 
					this.containerElem, this.band, this.oldElements, this.clipLeft );

				rect = addedElem.Rect;
				rect.Width -= this.clipLeft - rect.X;
				rect.X = this.clipLeft;

				if ( addedElem is SummaryValueUIElement )
					((SummaryValueUIElement)addedElem).InternalSetClipSelfRegion( rect );
				else if ( addedElem is HeaderUIElement )
					((HeaderUIElement)addedElem).InternalSetClipSelfRegion( rect );
				else
					Debug.Assert( false );
			}
		}

		#endregion // ILayoutContainer.PositionItem

		#region ILayoutContainer.GetBounds

		System.Drawing.Rectangle ILayoutContainer.GetBounds(object containerContext)
		{
			return this.containerRect;
		}

		#endregion // ILayoutContainer.GetBounds

		#endregion // Implementation of ILayoutContainer
	}

	#endregion // LayoutContainerCellArea Class

	#region LayoutContainerCardLabelArea Class

	internal class LayoutContainerCardLabelArea : LayoutContainerBase
	{
		#region Private Variables

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private Rectangle cardLabelAreaRect;

		private CardLabelAreaUIElement containerElem = null;
		private bool headerBordersMergeable = false;

		#endregion // Private Variables

		#region Constructor

		private LayoutContainerCardLabelArea( )
		{
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="cardLabelAreaRect"></param>
		//#endif
		//        internal LayoutContainerCardLabelArea( UltraGridBand band, Rectangle cardLabelAreaRect )
		//        {
		//            this.Initialize( band, cardLabelAreaRect );
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="cardLabelAreaRect"></param>
		//        /// <param name="containerElem"></param>
		//        /// <param name="oldElements"></param>
		//#endif
		//        internal LayoutContainerCardLabelArea( UltraGridBand band, Rectangle cardLabelAreaRect, 
		//            CardLabelAreaUIElement containerElem, UIElementsCollection oldElements )
		//        {
		//            this.Initialize( band, cardLabelAreaRect, containerElem, oldElements );
		//        }

		#endregion Not Used

		#endregion // Constructor

		#region Initialize

		internal void Initialize( UltraGridBand band, Rectangle cardLabelAreaRect )
		{
			this.Initialize( band, cardLabelAreaRect, null, null );
		}

		internal void Initialize( UltraGridBand band, Rectangle cardLabelAreaRect, 
			CardLabelAreaUIElement containerElem, UIElementsCollection oldElements )
		{
			this.band = band;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.cardLabelAreaRect = cardLabelAreaRect;

			this.containerRect = cardLabelAreaRect;

			this.containerElem = containerElem;
			this.oldElements = oldElements;
				
			this.clipRect = null != this.containerElem ? this.containerElem.ClipRect : Rectangle.Empty;

			this.headerBordersMergeable = band.Layout.CanMergeAdjacentBorders( band.BorderStyleHeaderResolved );

			// SSP 6/27/03 UWG2421
			// In layout designer, add all the elements, even the ones that would not be visible 
			// because they are outside of the clip rect. We need to do this because the in the
			// designer, the drag window uses the cell and header elements to draw itself when
			// that cell/header combination is being dragged.
			//
			this.isLayoutDesigner = null != this.containerElem 
				? LayoutContainerHelper.IsRowLayoutDesignerElement( this.containerElem ) : false;
		}

		#endregion // Initialize

		#region CreateEmpty

		internal static LayoutContainerCardLabelArea CreateEmpty( )
		{
			return new LayoutContainerCardLabelArea( );
		}

		#endregion // CreateEmpty

		#region AdjustForMergedBorders

		private void AdjustForMergedBorders( ref Rectangle rect, ILayoutItem item )
		{
			if ( this.headerBordersMergeable )
			{
				// SSP 12/1/03 UWG2641
				// Added support for cell and label insets.
				//
				// --------------------------------------------------------------------
				LayoutUtility.ItemMergeBorderHelper( ref rect, item, this.containerRect );
				
				// --------------------------------------------------------------------
			}
		}

		#endregion // AdjustForMergedBorders

		#region PositionItem

		public override void PositionItem(Infragistics.Win.Layout.ILayoutItem item, System.Drawing.Rectangle rect, object containerContext)
		{
			this.AdjustForMergedBorders( ref rect, item );

			if ( item is HeaderBase )
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//ColumnHeader header = (ColumnHeader)item;
                HeaderBase header = (HeaderBase)item;

				if ( null == this.containerElem )
				{
					// If the container elem is null, then we are setting the rects on the row layout info.
					//
                    // MRS - NAS 9.1 - Groups in RowLayout
					//header.Column.RowLayoutColumnInfo.CachedItemRectHeader = rect;
                    header.RowLayoutColumnInfo.CachedItemRectHeader = rect;
				}
				else
				{
					// SSP 6/27/03 UWG2421
					// In layout designer, add all the elements, even the ones that would not be visible 
					// because they are outside of the clip rect. We need to do this because the in the
					// designer, the drag window uses the cell and header elements to draw itself when
					// that cell/header combination is being dragged.
					//
					// SSP 11/17/04
					// Centralized the logic in the base class.
					//
					//if ( ! this.isLayoutDesigner && ! this.clipRect.IntersectsWith( rect ) )
					if ( ! this.ShouldPositionElement( rect ) 
						// SSP 11/17/04
						// Added a way to hide the header or the cell of a column in the row layout mode.
						// This way you can do things like hide a row of headers or add unbound columns
						// and hide their cells to show grouping headers etc...
						//
                        // MRS - NAS 9.1 - Groups in RowLayout
						//|| ! header.Column.RowLayoutColumnInfo.ShouldPositionLabelElement )
                        || !header.RowLayoutColumnInfo.ShouldPositionLabelElement)
						return;

					this.containerElem.AddLabelElementHelper( this.oldElements, rect, header, false, false,	false, true );
				}
			}
            // MRS - NAS 9.1 - Groups in RowLayout
            else if (item is GroupLayoutItemHeaderContent)
            {
                GroupLayoutItemHeaderContent groupLayoutItemHeaderContent = (GroupLayoutItemHeaderContent)item;

                if (null == this.containerElem)
                {
                    // If the container elem is null, then we are setting the rects on the row layout info.
                    //                    
                    groupLayoutItemHeaderContent.RowLayoutGroupInfo.CachedItemRectHeader = rect;
                }
                else
                {
                    // SSP 6/27/03 UWG2421
                    // In layout designer, add all the elements, even the ones that would not be visible 
                    // because they are outside of the clip rect. We need to do this because the in the
                    // designer, the drag window uses the cell and header elements to draw itself when
                    // that cell/header combination is being dragged.
                    if (!this.ShouldPositionElement(rect)
                        || !groupLayoutItemHeaderContent.RowLayoutGroupInfo.ShouldPositionCellElement)
                        return;

                    LayoutContainerCardLabelArea layoutContainerCardLabelArea = new LayoutContainerCardLabelArea();
                    layoutContainerCardLabelArea.Initialize(
                        band,
                        rect,
                        containerElem,
                        oldElements
                        );
                    groupLayoutItemHeaderContent.LayoutManager.LayoutContainer(layoutContainerCardLabelArea, null);
                }
            }
			else
			{
				Debug.Assert( false, "Unknown type of item !" );
			}
		}

		#endregion // PositionItem
	}

	#endregion // LayoutContainerCardLabelArea Class

	#region LayoutContainerCardCellArea Class

	internal class LayoutContainerCardCellArea : LayoutContainerBase
	{
		#region Private Variables

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private Rectangle rowCellAreaRect;

		// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Split the RowCellAreaUIElement into RowCellAreaUIElementBase class and 
		// RowCellAreaUIElement class so the filter cell area ui element can derive from 
		// RowCellAreaUIElementBase .
		//
		//private RowCellAreaUIElement containerElem = null;
		private RowCellAreaUIElementBase containerElem = null;
        // SSP 6/13/05
        // sameRow is not being used anywhere.
        // 
		//private bool sameRow = false;
		private int cellSpacing = 0;
		private bool cellBordersMergable = false;

		#endregion // Private Variables

		#region Constructor

		private LayoutContainerCardCellArea( )
		{
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="rowCellAreaRect"></param>
		//#endif
		//        internal LayoutContainerCardCellArea( UltraGridBand band, Rectangle rowCellAreaRect )
		//        {
		//            this.Initialize( band, rowCellAreaRect );
		//        }

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor
		//        /// </summary>
		//        /// <param name="band"></param>
		//        /// <param name="rowCellAreaRect"></param>
		//        /// <param name="containerElem"></param>
		//        /// <param name="oldElements"></param>
		//#endif
		//        internal LayoutContainerCardCellArea( UltraGridBand band, Rectangle rowCellAreaRect,
		//            RowCellAreaUIElement containerElem, UIElementsCollection oldElements )
		//        {
		//            this.Initialize( band, rowCellAreaRect, containerElem, oldElements );
		//        }

		#endregion Not Used

		#endregion // Constructor

		#region Initialize

		internal void Initialize( UltraGridBand band, Rectangle rowCellAreaRect )
		{
			this.Initialize( band, rowCellAreaRect, null, null );
		}

		internal void Initialize( 
			UltraGridBand band, 
			Rectangle rowCellAreaRect,
			// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
			// Split the RowCellAreaUIElement into RowCellAreaUIElementBase class and 
			// RowCellAreaUIElement class so the filter cell area ui element can derive from 
			// RowCellAreaUIElementBase .
			//
			//RowCellAreaUIElement containerElem, 
			RowCellAreaUIElementBase containerElem, 
			UIElementsCollection oldElements )
		{
			this.band = band;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.rowCellAreaRect = rowCellAreaRect;

			this.containerRect = rowCellAreaRect;

			this.containerElem = containerElem;
			this.oldElements = oldElements;
            // SSP 6/13/05
            // sameRow is not being used anywhere.
            // 
			//this.sameRow = sameRow;

			this.clipRect = null != this.containerElem ? this.containerElem.ClipRect : Rectangle.Empty;

			this.cellSpacing = this.band.CellSpacingResolved;
			this.cellBordersMergable = this.band.Layout.CanMergeAdjacentBorders( this.band.BorderStyleCellResolved );

			// SSP 6/27/03 UWG2421
			// In layout designer, add all the elements, even the ones that would not be visible 
			// because they are outside of the clip rect. We need to do this because the in the
			// designer, the drag window uses the cell and header elements to draw itself when
			// that cell/header combination is being dragged.
			//
			this.isLayoutDesigner = null != this.containerElem 
				? LayoutContainerHelper.IsRowLayoutDesignerElement( this.containerElem ) : false;
		}

		#endregion // Initialize

		#region CreateEmpty

		internal static LayoutContainerCardCellArea CreateEmpty( )
		{
			return new LayoutContainerCardCellArea( );
		}

		#endregion // CreateEmpty

		#region AdjustForMergedBorders

		private void AdjustForMergedBorders( ref Rectangle rect, ILayoutItem item )
		{
			// Adjust the cell rect to allow for cell spacing.
			//
			if ( this.cellSpacing > 0 )
			{
				// Only adjust the rect if we are laying out the cell elements. When we are just figuring out
				// the cell rects, which are cached off RowLayoutColumnInfo and returned by
				// RowLayoutColumnInfo.CachedItemRectCell proeprty, we shouldn't adjust for cell spacing. This is
				// so that the CachedItemRectCell retruns a rect that includes the cell spacing. This
				// maintains the backward compatiblity when it comes to cell selection becase cell section
				// the way it currently works is that the mouse doesn't have to moved inside of the cell for
				// it to be selected. It only has to be moved inside the cell rect that includes the spacing.
				// NOTE: Also doing this makes it easier for the column resizing to work in regular view
				// since the column resizing doesn't have to take into account cell spacing because the
				// CachedItemRectCell includes the cellspacing.
				//
				if ( null != this.containerElem )
					rect.Inflate( - this.cellSpacing, - this.cellSpacing );
				return;
			}

			if ( this.cellBordersMergable )
			{
				// SSP 12/1/03 UWG2641
				// Added support for cell and label insets.
				//
				// --------------------------------------------------------------------
				LayoutUtility.ItemMergeBorderHelper( ref rect, item, this.containerRect );
				
				// --------------------------------------------------------------------
			}
		}

		#endregion // AdjustForMergedBorders

		#region PositionItem

		public override void PositionItem(Infragistics.Win.Layout.ILayoutItem item, System.Drawing.Rectangle rect, object containerContext)
		{
			this.AdjustForMergedBorders( ref rect, item );

			if ( item is UltraGridColumn )
			{
				UltraGridColumn column = (UltraGridColumn)item;

				if ( null == this.containerElem )
				{
					// If the container elem is null, then we are setting the rects on the row layout info.
					//
					column.RowLayoutColumnInfo.CachedItemRectCell = rect;
				}
				else
				{
					// SSP 6/27/03 UWG2421
					// In layout designer, add all the elements, even the ones that would not be visible 
					// because they are outside of the clip rect. We need to do this because the in the
					// designer, the drag window uses the cell and header elements to draw itself when
					// that cell/header combination is being dragged.
					//
					// SSP 11/17/04
					// Centralized the logic in the base class.
					//
					//if ( ! this.isLayoutDesigner && ! this.clipRect.IntersectsWith( rect ) )
					if ( ! this.ShouldPositionElement( rect ) 
						// SSP 11/17/04
						// Added a way to hide the header or the cell of a column in the row layout mode.
						// This way you can do things like hide a row of headers or add unbound columns
						// and hide their cells to show grouping headers etc...
						//
						|| ! column.RowLayoutColumnInfo.ShouldPositionCellElement )
						return;

					UltraGridRow row = this.containerElem.Row;

					// Check to see if the cell's Hidden property is set to true.
					//
					if ( ! LayoutContainerHelper.IsCellHidden( row, column ) )
					{
						this.containerElem.AddCardCellHelper( this.oldElements,	rect, this.containerElem.Row,
							this.band, column, false, false, false, false, false, true );
					}
				}
			}
            // MRS - NAS 9.1 - Groups in RowLayout
			//else if ( item is ColumnHeader )
            else if (item is HeaderBase)
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//ColumnHeader header = (ColumnHeader)item;
                HeaderBase header = (HeaderBase)item;

				// MD 8/3/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridColumn headerColumn = header.Column;
                IProvideRowLayoutColumnInfo iProvideRowLayoutColumnInfo = header.RowLayoutColumnInfoProvider;

				if ( null == this.containerElem )
				{
					// If the container elem is null, then we are setting the rects on the row layout info.
					//
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//header.Column.RowLayoutColumnInfo.CachedItemRectHeader = rect;
                    iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.CachedItemRectHeader = rect;
				}
				else
				{
					// SSP 6/27/03 UWG2421
					// In layout designer, add all the elements, even the ones that would not be visible 
					// because they are outside of the clip rect. We need to do this because the in the
					// designer, the drag window uses the cell and header elements to draw itself when
					// that cell/header combination is being dragged.
					//
					// SSP 11/17/04
					// Centralized the logic in the base class.
					//
					//if ( ! this.isLayoutDesigner && ! this.clipRect.IntersectsWith( rect ) )
					if ( ! this.ShouldPositionElement( rect ) 
						// SSP 11/17/04
						// Added a way to hide the header or the cell of a column in the row layout mode.
						// This way you can do things like hide a row of headers or add unbound columns
						// and hide their cells to show grouping headers etc...
						//
						// MD 8/3/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//|| ! header.Column.RowLayoutColumnInfo.ShouldPositionLabelElement )
                        || !iProvideRowLayoutColumnInfo.RowLayoutColumnInfo.ShouldPositionLabelElement)
						return;

					UltraGridRow row = this.containerElem.Row;

					// Check to see if the cell's Hidden property is set to true.
					//
					// MD 8/3/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( ! LayoutContainerHelper.IsCellHidden( row, header.Column ) )
                    // MRS - NAS 9.1 - Groups in RowLayout
					//if ( !LayoutContainerHelper.IsCellHidden( row, headerColumn ) )
                    if (header is GroupHeader ||
                        !LayoutContainerHelper.IsCellHidden(row, iProvideRowLayoutColumnInfo as UltraGridColumn))
					{
						CardLabelUIElement cardLabelElem = (CardLabelUIElement)LayoutContainerHelper.ExtractUIElement( this.oldElements, typeof( CardLabelUIElement ) );

						if ( null == cardLabelElem )
							cardLabelElem = new CardLabelUIElement( this.containerElem );

						cardLabelElem.InitializeHeader( header );
						cardLabelElem.Rect = rect;
						this.containerElem.ChildElements.Add( cardLabelElem );
					}
				}
			}
            // MRS - NAS 9.1 - Groups in RowLayout
            else if (item is GroupLayoutItemCellContent)
            {
                GroupLayoutItemCellContent groupLayoutItemCellContent = (GroupLayoutItemCellContent)item;

                if (null == this.containerElem)
                {
                    // If the container elem is null, then we are setting the rects on the row layout info.
                    //                    
                    groupLayoutItemCellContent.RowLayoutGroupInfo.CachedItemRectCell = rect;
                }
                else
                {
                    // SSP 6/27/03 UWG2421
                    // In layout designer, add all the elements, even the ones that would not be visible 
                    // because they are outside of the clip rect. We need to do this because the in the
                    // designer, the drag window uses the cell and header elements to draw itself when
                    // that cell/header combination is being dragged.
                    if (!this.ShouldPositionElement(rect)
                        || !groupLayoutItemCellContent.RowLayoutGroupInfo.ShouldPositionCellElement)
                        return;

                    LayoutContainerCardCellArea lLayoutContainerCardCellArea = new LayoutContainerCardCellArea();
                    lLayoutContainerCardCellArea.Initialize(
                        band,
                        rect,
                        containerElem,
                        oldElements
                        );
                    groupLayoutItemCellContent.LayoutManager.LayoutContainer(lLayoutContainerCardCellArea, null);
                }
            }
			else
			{
				Debug.Assert( false, "Unknown type of item !" );
			}
		}

		#endregion // PositionItem
	}

	#endregion // LayoutContainerCellArea Class

	#region RowAutoSizeLayoutManagerHolder Class

	// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
	// 
	internal class RowAutoSizeLayoutManagerHolder
	{
		private UltraGridRow row = null;
		private LayoutManagerBase rowLayoutManager = null;
		private int verifiedRowLayoutVersion = -1;
		private Size cachedRowLayoutPreferredSize = Size.Empty;

		private RowAutoSizeCalculator rowAutoSizeCalculator = null;

		public RowAutoSizeLayoutManagerHolder( UltraGridRow row )
		{
			this.row = row;
		}

		private void VerifyCachedLayoutManager( )
		{
			UltraGridBand band = this.row.BandInternal;
			band.VerifyRowLayoutCache( );

			if ( this.verifiedRowLayoutVersion != band.RowLayoutCacheVersion
				// SSP 5/7/07 BR22392 
				// Added check for dontVerifyRowLayoutCache.
				// 
				// SSP 3/14/08 BR31270
				// If rowLayoutManager is null then we have to create it.
				// 
				//&& ! band.dontVerifyRowLayoutCache
				&& ( ! band.dontVerifyRowLayoutCache || null == this.rowLayoutManager )
				)
			{
				// We need to maintain the preferred sizes explicitly set on the cell layout items.
				// 
				Hashtable oldItemsTable = null;
				if ( null != this.rowLayoutManager )
				{
					oldItemsTable = new Hashtable( this.rowLayoutManager.LayoutItems.Count );
					foreach ( ILayoutItem item in this.rowLayoutManager.LayoutItems )
					{
						// MD 8/7/07 - 7.3 Performance
						// FxCop - Do not cast unnecessarily
						//if ( item is CellLayoutItem )
						//    oldItemsTable[ ((CellLayoutItem)item).Column ] = item;
						CellLayoutItem cellLayoutItem = item as CellLayoutItem;

						if ( cellLayoutItem != null )
							oldItemsTable[ cellLayoutItem.Column ] = item;
					}
				}

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//GridBagLayoutManager destLM = band.CreateGridBagLayoutManager( );
				GridBagLayoutManager destLM = UltraGridBand.CreateGridBagLayoutManager();

				this.rowLayoutManager = destLM;

				GridBagLayoutManager srcLM = (GridBagLayoutManager)band.RowLayoutManager;
				if ( null != srcLM )
				{
					foreach ( ILayoutItem item in srcLM.LayoutItems )
					{
						object constraint = srcLM.LayoutItems.GetConstraint( item );
						if ( item is UltraGridColumn )
						{
							// We need to maintain the preferred sizes explicitly set on the cell layout items.
							// 
							CellLayoutItem cellLayoutItem = null != oldItemsTable ? (CellLayoutItem)oldItemsTable[ item ] : null;

							if ( null == cellLayoutItem )
								cellLayoutItem = new CellLayoutItem( this, (UltraGridColumn)item );							

							destLM.LayoutItems.Add( cellLayoutItem, constraint );
						}
						else
							destLM.LayoutItems.Add( item, constraint );
					}

					// SSP 4/11/06 BR11399
					// Moved this below.
					// 
					

					// Release the reference to rowAutoSizeCalculator as it's not needed any more.
					// 
					this.rowAutoSizeCalculator = null;
				}

				this.cachedRowLayoutPreferredSize = Size.Empty;
				this.verifiedRowLayoutVersion = band.RowLayoutCacheVersion;
			}

			if ( this.cachedRowLayoutPreferredSize.IsEmpty )
			{
				// SSP 4/11/06 BR11399
				// Moved this code that copies over the column widths between layout managers from
				// the above if block.
				// 
				// ----------------------------------------------------------------------------------
				// This is to make sure the widths of the cells are the same as the widths columns.
				//
				GridBagLayoutManager srcLM = (GridBagLayoutManager)band.RowLayoutManager;
				GridBagLayoutManager destLM = (GridBagLayoutManager)this.rowLayoutManager;
				if ( null != srcLM && null != destLM )
					Array.Copy( srcLM.ColumnWidths, destLM.ColumnWidths, Math.Min( srcLM.ColumnWidths.Length, destLM.ColumnWidths.Length ) );
				// ----------------------------------------------------------------------------------

				Size preferredSize = this.rowLayoutManager.CalculatePreferredSize( band.GetCachedLayoutContainerCalcSize( ), null );
				this.cachedRowLayoutPreferredSize = preferredSize;
			}
		}

		private RowAutoSizeCalculator CachedRowAutoSizeCalculator
		{
			get
			{
				if ( null == this.rowAutoSizeCalculator )
					this.rowAutoSizeCalculator = new RowAutoSizeCalculator( this.row );

				return this.rowAutoSizeCalculator;
			}
		}

		internal LayoutManagerBase RowLayoutManager
		{
			get
			{
				this.VerifyCachedLayoutManager( );
				return this.rowLayoutManager;
			}
		}

		internal Size RowLayoutPreferredSize
		{
			get
			{
				this.VerifyCachedLayoutManager( );
				return this.cachedRowLayoutPreferredSize;
			}
		}

		// SSP 2/6/08 BR27135
		// Renamed the method to PrepareForExplicitPerformAutoSize.
		// 
		//internal void ClearManuallySetItemPreferredSizes( )
		internal void PrepareForExplicitPerformAutoSize( )
		{
			if ( null != this.rowLayoutManager )
			{
				foreach ( ILayoutItem item in this.rowLayoutManager.LayoutItems )
				{
					CellLayoutItem cellLayoutItem = item as CellLayoutItem;
					if ( null != cellLayoutItem )
						// SSP 2/6/08 BR27135
						// Use the new PrepareForExplicitPerformAutoSize method instead which resets 
						// a version number as well as sets a flag.
						// 
						//cellLayoutItem.PreferredSizeOverride = Size.Empty;
						cellLayoutItem.PrepareForExplicitPerformAutoSize( );
				}
			}

			this.DirtyCachedSize( );
		}

		internal void DirtyCachedSize( )
		{
			this.cachedRowLayoutPreferredSize = Size.Empty;

			if ( null != this.rowLayoutManager )
				this.rowLayoutManager.InvalidateLayout( );

			this.row.InvalidateItemAllRegions( true );
		}

		#region RowAutoSizeCalculator Class

		// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
		// Added RowAutoHeightCalculator helper class for calculating ideal size of cells.
		// 
		internal class RowAutoSizeCalculator
		{
			private UltraGridRow row = null;
			int cellBorderThickness = 0;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//int cellSpacing = 0;

			int autoMaxLines = 0;
			private AppearanceData lastCellAppearance;
			private int lastlastCellAppearanceLineHeight;
			private UltraGridBand band;
			private UltraGridLayout layout;

			public RowAutoSizeCalculator( UltraGridRow row )
			{
				this.row = row;
				this.band = row.BandInternal;
				this.layout = this.band.Layout;
				this.cellBorderThickness = this.layout.GetBorderThickness( row.BorderStyleCellResolved );

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.cellSpacing = this.band.CellSpacingResolved;

				// This is what the CalculateAutoHeight code did before this class was added.
				// 0 means no limit.
				// 
				RowSizing rowSizingMode = this.band.RowSizingResolved;
				this.autoMaxLines = RowSizing.AutoFixed == rowSizingMode || RowSizing.AutoFree == rowSizingMode
					? this.band.RowSizingAutoMaxLinesResolved : 0;

				this.lastCellAppearance = new AppearanceData( );
				this.lastlastCellAppearanceLineHeight = -1;
			}

			// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// 
			internal Size GetIdealCellSize( UltraGridColumn column )
			{
				Size idealSize = Size.Empty;

				if ( column.IsChaptered )
					return idealSize;

				// Column width should not get smaller than the min column width
				//
				// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
				// Note that CellSizeResolved already takes out cell spacing.
				// 
				//int colWidth = Math.Max( column.MinWidth, column.Width - cellSpacing - cellBorderThickness );
				int colWidth = Math.Max( column.MinWidth, column.CellSizeResolved.Width - 2 * cellBorderThickness );
                
                // MRS 12/11/2007 - BR28989            
                if (column.IsColumnBorderMerged)
                    colWidth += cellBorderThickness;

				// SSP 5/1/03 - Cell Level Editor
				// Added Editor and ValueList properties off the cell so the user can set the editor
				// and value list on a per cell basis.
				//
				//EmbeddableEditorBase editor = column.Editor;
				EmbeddableEditorBase editor = column.GetEditor( this.row );

				Debug.Assert( null != editor, "Column.Editor null !" );

				if ( null != editor )
				{
					// SSP 11/20/03 UWG2745
					// Pass in the column width so the editor can calculate the required size based
					// on the available width.
					//
					//Size size = editor.GetSize( column.EditorOwnerInfo, this, false, true, false );
					// SSP 4/18/05 - NAS 5.2 Filter Row
					// Use the new EditorOwnerInfo property of the cell because filter row cells 
					// use different owners.
					//
					//Size size = editor.GetSize( column.EditorOwnerInfo, this, false, true, false, colWidth );
					idealSize = editor.GetSize( this.row.GetEditorOwnerInfo( column ), this.row, false, true, false, colWidth );
				}

				// Constraint the height according to autoMaxLines if it's set.
				// 
				if ( this.autoMaxLines > 0 )
				{
					AppearanceData appData = new AppearanceData( );
					AppearancePropFlags flags = AppearancePropFlags.FontData;
					this.row.ResolveCellAppearance( column, ref appData, flags );
					
					if ( this.lastlastCellAppearanceLineHeight < 0 
						|| ! this.lastCellAppearance.IsSameFont( ref appData ) )
					{
						this.lastCellAppearance = appData;
						this.lastlastCellAppearanceLineHeight = this.layout.CalculateFontHeight( ref appData );
					}

					// SSP 10/31/05 BR07130
					// A typo. It should have been Math.Min instead of Math.Max.
					// 
					//idealSize.Height = Math.Max( idealSize.Height, autoMaxLines * this.lastlastCellAppearanceLineHeight );
					idealSize.Height = Math.Min( idealSize.Height, autoMaxLines * this.lastlastCellAppearanceLineHeight );
				}

                // MRS 1/3/2008 - BR29247
                // Account for the CellSpacing. We were previously accounting for this in 
                // UltraGridRow.CalculateAutoHeight but it was only doing it for non-RowLayout mode, 
                // which is, of course, wrong.
                // So we now account for it here and I commented out the code in UltraGridRow.CalculateAutoHeight.
                //idealSize.Height += 2 * this.cellBorderThickness;
                idealSize.Height += 2 * (this.cellBorderThickness + band.CellSpacingResolved);

				return idealSize;
			}
		}

		#endregion // RowAutoSizeCalculator Class

		#region CellLayoutItem Class

		// SSP 4/14/05 - NAS 5.2 Filter Row
		// 
		internal class CellLayoutItem : ILayoutItem
		{
			private UltraGridRow row = null;
			private UltraGridColumn column = null;

			private RowAutoSizeLayoutManagerHolder lmHolder = null;

			// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// 
			private Size preferredSizeOverride = Size.Empty;

			// SSP 8/29/05 BR05837
			// A version number that's bumped every time the PreferredCellSize is changed.
			// This is used when the RowSizing is not Syncrhonized and a column is vertically
			// span-resized. When this happens all the cells in all the rows should assume
			// the size as indicated by the red rectangle that shows up while span-resizing.
			// 
			private int verifiedRowLayoutColumnInfo_PreferredCellSizeVersion = -1;

			internal CellLayoutItem( RowAutoSizeLayoutManagerHolder lmHolder, UltraGridColumn column )
			{
				GridUtils.ValidateNull( lmHolder );
				GridUtils.ValidateNull( column );

				this.lmHolder = lmHolder;
				this.row = this.lmHolder.row;
				this.column = column;

				// SSP 8/29/05 BR05837
				// 
				this.verifiedRowLayoutColumnInfo_PreferredCellSizeVersion = this.column.RowLayoutColumnInfo.preferredCellSizeHeightVersion;
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid uncalled private code
			#region Not Used

			//internal UltraGridRow Row
			//{
			//    get
			//    {
			//        return this.row;
			//    }
			//}

			#endregion Not Used

			internal UltraGridColumn Column
			{
				get
				{
					return this.column;
				}
			}

			internal ILayoutItem ColumnAsLayoutItem
			{
				get
				{
					return this.column;
				}
			}

			// SSP 2/6/08 BR27135
			// Added PrepareForExplicitPerformAutoSize method.
			// 
			private bool autoSizingExplicitly = false;
			internal void PrepareForExplicitPerformAutoSize( )
			{
				this.autoSizingExplicitly = true;
				this.PreferredSizeOverride = Size.Empty;
			}

			// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// 
			/// <summary>
			/// When a cell is resized, its new size is stored using this property. This gets discarded
			/// when auto-size is performed.
			/// </summary>
			internal Size PreferredSizeOverride
			{
				get
				{
					// SSP 8/29/05 BR05837
					// A version number that's bumped every time the column's PreferredCellSize is changed.
					// This is used when the RowSizing is not Syncrhonized and a column is vertically
					// span-resized. When this happens all the cells in all the rows should assume
					// the size as indicated by the red rectangle that shows up while span-resizing.
					// 
					RowLayoutColumnInfo ci = this.column.RowLayoutColumnInfo;
					if ( this.verifiedRowLayoutColumnInfo_PreferredCellSizeVersion != ci.preferredCellSizeHeightVersion )
					{
						// SSP 2/6/08 BR27135
						// Just set the preferredSizeOverride to empty size. Also don't reset the
						// version number. Now different values of version numbers means that 
						// we should use the last span-resized size. We reset the number when this
						// row is explicitly sized or auto-sized.
						// 
						// ----------------------------------------------------------------------------
						this.preferredSizeOverride = Size.Empty;
						
						// ----------------------------------------------------------------------------
					}

					return this.preferredSizeOverride;
				}
				set
				{
					// SSP 2/6/08 BR27135
					// Reset the version number so we don't ovewrite the set value in the get above.
					// 
					RowLayoutColumnInfo ci = this.column.RowLayoutColumnInfo;
					this.verifiedRowLayoutColumnInfo_PreferredCellSizeVersion = ci.preferredCellSizeHeightVersion;

					if ( this.preferredSizeOverride != value )
					{
						this.preferredSizeOverride = value;
						this.lmHolder.DirtyCachedSize( );
					}
				}
			}

			public Size PreferredSize
			{
				get
				{
					Size size = this.ColumnAsLayoutItem.PreferredSize;

					// SSP 8/2/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
					// 
					// --------------------------------------------------------------------------
					Size preferredSizeOverride = this.PreferredSizeOverride;
					if ( preferredSizeOverride.Width > 0 )
						size.Width = preferredSizeOverride.Width;

					// SSP 2/23/06 BR09967
					// If the label position is set to LabelOnly then return 0 as for the height of
					// the cell since the cell should be hidden in this case.
					// 
					RowLayoutColumnInfo rowLayoutColumnInfo = this.Column.RowLayoutColumnInfo;
					if ( LabelPosition.LabelOnly == rowLayoutColumnInfo.LabelPositionResolved )
						return size;

					if ( preferredSizeOverride.Height > 0 )
					{
						size.Height = preferredSizeOverride.Height;
					}
					else if ( this.row.IsDataRow )
					{
						// SSP 2/6/08 BR27135
						// Only use ideal size if row sizing mode is Auto.
						// 
						// --------------------------------------------------------------------------------------------
						//Size tmpSize = this.lmHolder.CachedRowAutoSizeCalculator.GetIdealCellSize( this.Column );
						//size.Height = tmpSize.Height;
						RowSizing rowSizing = this.row.BandInternal.RowSizingResolved;
						if ( ( RowSizing.AutoFree == rowSizing || RowSizing.AutoFixed == rowSizing || this.autoSizingExplicitly )
							// PreferredCellSizeHeightVersion is bumped when the preferred cell size height of the
							// row layout column info is changed. In which case we want to use that value unless
							// after that explicit PerformAutoSize operation is performed.
							// 
							&& this.verifiedRowLayoutColumnInfo_PreferredCellSizeVersion == rowLayoutColumnInfo.preferredCellSizeHeightVersion )
						{
							Size tmpSize = this.lmHolder.CachedRowAutoSizeCalculator.GetIdealCellSize( this.Column );

							// If the row is auto-sized explicitly either via the row's PerformAutoSize method or
							// via the user interface, use the ideal size only. Otherwise use the maximum of the
							// ideal size and the column's PreferredCellSize setting.
							// 
							if ( this.autoSizingExplicitly )
								size.Height = tmpSize.Height;
							else
								size.Height = Math.Max( size.Height, tmpSize.Height );
						}
						// --------------------------------------------------------------------------------------------
					}
					// --------------------------------------------------------------------------
					else if ( this.row.IsFilterRow )
					{
						UltraGridFilterRow filterRow = (UltraGridFilterRow)this.row;
						size.Height = Math.Max( size.Height, filterRow.GetIdealFilterCellHeight( this.column ) );
					}

					return size;
				}
			}

			public Size MinimumSize
			{
				get
				{
					return this.ColumnAsLayoutItem.MinimumSize;
				}
			}

			public bool IsVisible
			{
				get
				{
					return this.ColumnAsLayoutItem.IsVisible;
				}
			}

		}

		#endregion // CellLayoutItem Class
	}

	#endregion // RowAutoSizeLayoutManagerHolder Class
}
