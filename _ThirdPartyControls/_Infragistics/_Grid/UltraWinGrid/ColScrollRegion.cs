#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Collections;
	using System.Globalization;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using Infragistics.Win.UltraWinScrollBar;
	using System.Security.Permissions;

    /// <summary>
    /// Defines a region that scrolls columns. Adjacent column scroll regions are separated by splitter bars.
    /// </summary>
    /// <remarks>
	/// <seealso cref="RowScrollRegion"/>
	/// <seealso cref="RowScrollRegionsCollection"/>
	/// <seealso cref="ColScrollRegionsCollection"/>
	/// <seealso cref="UltraGridLayout.RowScrollRegions"/>
	/// <seealso cref="UltraGridLayout.ColScrollRegions"/>
	/// <seealso cref="UltraGridBase.ActiveRowScrollRegion"/>
	/// <seealso cref="UltraGridBase.ActiveColScrollRegion"/>
	/// <seealso cref="UltraGridLayout.MaxRowScrollRegions"/>
	/// <seealso cref="UltraGridLayout.MaxColScrollRegions"/>
	/// </remarks>
	[ Serializable() ]
	[ TypeConverter( typeof( ColScrollRegion.ColScrollRegionTypeConverter ) ) ]
	sealed public class ColScrollRegion : ScrollRegionBase, ISerializable
	{
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//private static readonly int COL_LINE_SCROLL_DELTA  = 10;
		private const int COL_LINE_SCROLL_DELTA = 10;

		// SSP 5/21/03 - Optimizations
		// Before we were using COL_LINE_SCROLL_DELTA above for the small change. However
		// that's too slow so we will use new constant below for the small change.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//internal static readonly int HORIZONTAL_SCROLLBAR_SMALL_CHANGE = 30;
		internal const int HORIZONTAL_SCROLLBAR_SMALL_CHANGE = 30;

		

		private VisibleHeadersCollection 
			visibleHeaders = null;
        
		private HeaderBase  priorHeader = null;
		private GridItemBase  pivotItem = null;
		private UltraGridCell    pivotCell = null;
		private UltraGridBand    lastBand = null;

		private bool    destroyVisibleHeaders = true;
		private int     overallWidth = 0;
		private int     lastBandOrigin;
		private int     lastBandExtent;
		private int     position = 0; //scroll position
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private UltraGridColumn		columnToScrollIntoView = null;

		// SSP 7/10/03 - Fixed headers
		// In fixed headers area, we need to knwo which item is being scrolled so
		// that we know how to scroll the scroll region when the user is drag selecting
		// items. Alsow we need to use fixedToNonFixedAreaBoundaryCrossed flag to
		// scroll the first non-fixed header into view when the user crosses the boundary
		// from fixed to non-fixed area.
		// 
		private GridItemBase lastPivotItem = null;
		internal bool fixedToNonFixedAreaBoundaryCrossed = false;
		
		// SSP 2/14/04 UWG2964
		//
		private int bandsTotalFixedHeadersExtentVerifiedVersion = -1;
		private int cachedMaxFixedHeadersRight = 0;


		/// <summary>
		/// contructor
		/// </summary>
		[ Browsable( false ) ] 
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public ColScrollRegion( int extent )
		{
			this.Extent = extent;

			this.destroyVisibleHeaders = true;
		}

		internal ColScrollRegion( ColScrollRegionsCollection colScrollRegions ) 
			: base ( colScrollRegions )
		{
			this.destroyVisibleHeaders = true;
		}

		internal void SetDestroyVisibleHeaders ( ) 
		{
			this.SetDestroyVisibleHeaders( true );
		}

		internal void SetDestroyVisibleHeaders ( bool destroy ) 
		{
			this.SetDestroyVisibleHeaders( destroy, false );
		}

		internal void SetDestroyVisibleHeaders ( bool destroy, bool setScrollToBeginning ) 
		{
			this.SetDestroyVisibleHeaders( destroy, setScrollToBeginning, true );
		}

		internal void SetDestroyVisibleHeaders ( bool destroy, 
			bool setScrollToBeginning,
			bool dirtyElement ) 
		{ 
			// SSP 7/9/02 UWG1153
			// Check for the layout being null because during deserialization,
			// it may be null.
			//
			if ( dirtyElement && null != this.Layout )
				this.Layout.DirtyGridElement( false );

			this.destroyVisibleHeaders = destroy;

			if ( setScrollToBeginning )
				this.position = 0;
		}
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			//set the assembly name because of version number conflicts
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			PropertyCategories propCat = context.Context is PropertyCategories
				? (PropertyCategories)context.Context	: PropertyCategories.All;

			if( (propCat & PropertyCategories.ColScrollRegions ) == 0 )
				return;

			base.GetObjectData( info, context );
		}
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal ColScrollRegion( SerializationInfo info, StreamingContext context )
		private ColScrollRegion( SerializationInfo info, StreamingContext context )
			: base( info, context )
		{	
			

			//this.destroyVisibleHeaders = true;

			//Set in call to SetCollection()
			//this.SetCollection( null );
			//this.collection = null;

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void CopyState ( ColScrollRegion target, ColScrollRegion source )
		internal static void CopyState( ColScrollRegion target, ColScrollRegion source )
		{
			target.Width = source.Width;
			target.Origin = source.Origin;
			target.Scrollbar = source.Scrollbar;
			target.SizingMode = source.SizingMode;
			target.Hidden = source.Hidden;
		}

		internal ColScrollRegion Clone()
		{
			//ColScrollRegion clone = new ColScrollRegion((ColScrollRegionsCollection)this.collection);
			ColScrollRegion clone = (ColScrollRegion)this.MemberwiseClone();

			if ( clone == null )
				return null;
           
			clone.clonedFromRegion = this;

			if ( clone.VisibleHeaders == null )
				return null;

            // MRS 3/23/2009 - TFS14941
            clone.visibleHeaders = this.visibleHeaders.Clone(clone);

			clone.extentLastResize = this.extentLastResize;
			clone.overallWidth = this.overallWidth;
			clone.tagValue = this.Tag;
			
			// Reset last band cached values
			//
			this.lastBand = null;
			this.lastBandExtent = 0;
			this.lastBandOrigin = 0;
			this.originCached = false;

			//RobA 10/23/01 implemented
			this.scrolling = false;

			// Added flag to detect if the headers collection was regenerated
			// during a scrolling operation
			//
			this.regeneratedDuringScrolling = false;

			//copy old state structure over
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.CopyState( clone, this );
			ColScrollRegion.CopyState( clone, this );

			// JJD 11/30/01 - UWG798
			// We don't need to copy the exclusize items over since the
			// MemberwiseClone above means the list is shared.
			//
			// iterate over the exlusive items inserting each one into the
			// clone's list
			//



			return clone;
		}

		internal bool IsExclusive
		{
			get
			{
				// SSP 7/24/03 - Optimizations
				// Use HasExclusiveItems which does not cause allocation of the exclusive items
				// collection. 
				//
				//return null != this.ExclusiveItems && this.ExclusiveItems.Count > 0;
				return this.HasExclusiveItems;
			}
		}

		
		/// <summary>
		/// Returns or sets the position of a scroll bar in a colscrollregion. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The valid range for this property is from 0 to the value of the colscrollregion's <b>Range</b> property, inclusive. This property equals the <strong>Range</strong> property when the scroll bar is in its rightmost position.</p>
		/// <p class="body">In addition to using this property, a colscrollregion can be scrolled by invoking its Scroll method. When a colscrollregion is scrolled, the <b>BeforeColRegionScroll</b> event is generated.</p>
		/// <p class="body">A colscrollregion's scroll bar can be hidden by setting the colscrollregion's <b>ScrollBar</b> property to 3 (ScrollBarHide). When a colscrollregion's scroll bar is not displayed, the value of this property is  0.</p>
		/// </remarks>
        [ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public int	Position
		{
			get
			{
				return this.position;
			} 

			set
			{
				if ( value != this.position )
				{
					this.position = value;

					// JJD 11/03/01
					// Only issue the notification if we are not a clone
					// which we are during a scrolling operation
					//
					if ( !this.IsClone )
					{
						//RobA UWG362 9/27/01 we were not doing this
						this.NotifyPropChange( PropertyIds.Position, null );
					}
				}
			}
		}

		
		internal new ColScrollRegion ClonedFrom 
		{
			get
			{
				return (ColScrollRegion)base.ClonedFrom;
			}
		}

		internal int OverallWidth
		{
			get
			{
				return this.overallWidth;
			}
		}


		internal ColScrollRegion ExclusiveColScrollRegion
		{
			get
			{
				// if we are cloned then return called the clonedfrom's implementation
				//
				if ( this.IsClone )
					return ((ColScrollRegion)this.ClonedFrom).ExclusiveColScrollRegion;

				return this.ExclusiveItems.Count > 0 ? this : null;				
			}
		}

		
		internal GridItemBase GetPivotItem()
		{
			return this.pivotItem;
		}
		internal void SetPivotItem( GridItemBase gridItem )
		{
			this.pivotItem = gridItem;

			// SSP 7/10/03 - Fixed headers
			// In fixed headers area, we need to knwo which item is being scrolled so
			// that we know how to scroll the scroll region when the user is drag selecting
			// items. Also reset fixedToNonFixedAreaBoundaryCrossed flag.
			//
			this.lastPivotItem = gridItem;
			this.fixedToNonFixedAreaBoundaryCrossed = false;
		}


		// SSP 11/2/01
		// Overrode this method because we need to set the destroyVisibleHeaders flag
		// to true when the extent has changed so that the headers get recreated
		//
		internal override void SetOriginAndExtent( int origin, int extent ) 
		{
			bool extentChanged = this.Extent != extent;
			
			base.SetOriginAndExtent( origin, extent );

			if ( extentChanged )
			{
				// JJD 11/02/01
				// Just set the flag to true instead of calling SetDestroyVisibleHeaders
				// which dirties the elements and invalidates the grid
				//
				//this.SetDestroyVisibleHeaders( );
				this.destroyVisibleHeaders = true;

				// SSP 4/25/08 BR27115
				// When auto-fitting, change in extent of the grid should re-autofit columns. For that
				// dirty the metrics.
				// 
				UltraGridLayout layout = this.Layout;
				if ( null != layout && layout.AutoFitAllColumns && layout.ColScrollRegionsAllocated )
					layout.ColScrollRegions.DirtyMetrics( );
			}
		}


		internal void SetPivotCell( UltraGridCell newPivotCell )
		{
			this.pivotCell = newPivotCell;

			// SSP 7/10/03 - Fixed headers
			// In fixed headers area, we need to knwo which item is being scrolled so
			// that we know how to scroll the scroll region when the user is drag selecting
			// items. Also reset fixedToNonFixedAreaBoundaryCrossed flag.
			//
			this.lastPivotItem = newPivotCell;
			this.fixedToNonFixedAreaBoundaryCrossed = false;
		}


		internal UltraGridCell GetPivotCell( )
		{
			return this.pivotCell;
		}

		/// <summary>
		/// Resets member variables at the start of a metrics recalc.
		/// </summary>
		public void ResetExclusiveWidth() 
		{ 
			this.priorHeader             = null; 
			this.overallWidth            = 0;
			this.lastBand                = null;
			this.lastBandExtent          = 0;
			this.lastBandOrigin          = 0;
			this.originCached            = false;

			this.ExclusiveItems.InternalClear();
		
			if ( null != this.visibleHeaders )
				this.visibleHeaders.InternalClear();

			// Set the m_fDestroyVisibleHeaders flag to true so that we
			// will rebuild the elem header list on the next paint
			//
			destroyVisibleHeaders    = true;

		}
 
		/// <summary>
		/// Returns false since this region has a horizontal scrollbar
		/// </summary>
		protected override bool IsVerticalScrollbar
		{
			get
			{
				return false;
			}
		}

		// SSP 12/1/04 - Merged Cell Feature
		// Added IsActiveScrollRegion  method.
		//
		/// <summary>
		/// Returns true if the column scroll region is the same instance as the <see cref="UltraGridBase.ActiveColScrollRegion"/>. 
		/// </summary>
		public override bool IsActiveScrollRegion 
		{
			get
			{
				return null != this.Layout && this == this.Layout.ActiveColScrollRegion;
			}
		}
           
		/// <summary>
		/// <see cref="Infragistics.Win.UltraWinGrid.VisibleHeadersCollection"/>
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public VisibleHeadersCollection VisibleHeaders
		{
			get 
			{
				if ( null == this.visibleHeaders )
					this.visibleHeaders = new VisibleHeadersCollection( this );

				return this.visibleHeaders;
			}
		}

		internal void OnHeaderPropChange( VisibleHeader visibleHeader, PropChangeInfo propChange ) 
		{			
			bool recreateList          = false;
			bool invalidateRegion      = false;
			bool recalcDisplayBounds   = false;
        
			if ( propChange.PropId is PropertyIds )
			{
				switch ( (PropertyIds)propChange.PropId )
				{
					case PropertyIds.Hidden:
					case PropertyIds.Width:
					case PropertyIds.Columns:
					case PropertyIds.Level:
						recalcDisplayBounds = true;
						break;
					case PropertyIds.ValueList :
						if ( null != this.Layout.Bands )
							this.Layout.Bands.OnBaseFontChanged();
						recreateList   = true;

						break;

					case PropertyIds.Header:
						recalcDisplayBounds = true;
						recreateList        = true;
						break;
				}
			}

			if( recreateList )
			{
				// set the dirty flag so we recalc the metrics 
				//  on the next paint
				//
				// Mark the col sync lists dirty so they get recreated
				// on the next band metrics calculation
				//
				this.Layout.SynchronizedColListsDirty = true;
				// Pass false into GetColScrollRegionsColl so we don't
				// force an immediate metrics calculation
				//
				this.Layout.GetColScrollRegions( false ).DirtyMetrics();
			}

			// Recalculate Display bounds (distribute bounds to all colscrollregions)
			//
			if ( recalcDisplayBounds )
				this.Layout.DirtyGridElement();

			if ( invalidateRegion )
				this.Invalidate();			
		}
		
	

		/// <summary>
		/// The overall rect of this column scrolling region
		/// (spanning all row scrolling regions)
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public override Rectangle Rect 
		{ 
			get
			{
				Rectangle rect = this.Layout.DataArea;

				rect.Width = this.Extent;

				rect.Offset( this.Origin - rect.Left, 0 );
 
				return rect;
			}
		}

		internal void AdjustExclusiveWidth( HeaderBase header, ViewStyleBase viewStyle )
		{            			
			// First get the overall width. Hidden and chaptered columns will
			// have an overall width of 0
			//
			int overallWidth = header.Extent;

			// set first item flag based on a band break from the last call
			//
			// Only set first item flag if overall width > 0 ( not hidden )
			//
			header.FirstItem = overallWidth != 0 && (( null == this.priorHeader ) || (header.Band != this.priorHeader.Band ) );

			viewStyle.AdjustExclusiveMetrics(this, header, this.priorHeader);

			
			// Check the overall width before updating
			//
			if ( overallWidth > 0 )
			{
				// Reset last item flag on the previous item to false since
				// it is no longer the last item
				//
				// Only reset the priorposition item's LastItem to false if
				// the band hasn't changed
				//
				if ( null != this.priorHeader &&
					header.Band == this.priorHeader.Band )
				{					
					this.priorHeader.LastItem = false;
				}
			        
				
				// Mark this item as the last. If we get another item subsequently this
				// items last item flag will be reset above
				//
				header.LastItem = true;

				this.priorHeader = header;

				// Use a stack variable to hold the temp value instead of
				// of placing the method call inside the 'max' macro
				// below since that causes the method to be called twice
				// 
				this.UpdateExclusiveWidth(header); 
			}

			// Set lastBand to NULL to force a reclac of cached band extent and origin
			// values
			//
			this.lastBand = null;

			try
			{
				this.ExclusiveItems.InternalAdd( header );
			}
			catch(Exception)
			{
			}

		}

		internal void InitializeFrom( ColScrollRegion csr )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.CopyState( this, csr );
			ColScrollRegion.CopyState( this, csr );
			
			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( csr.ShouldSerializeTag() )
				this.tagValue = csr.Tag;
		}
		internal void UpdateExclusiveWidth( HeaderBase header )
		{
			Debug.Assert(this == header.ExclusiveColScrollRegion,
				"Invalid ColScrollRegion in ColScrollRegion.UpdateExclusiveWidth" );
			
			// Use a stack variable to hold the temp value instead of
			// of placing the method call inside the 'max' macro
			// below since that causes the method to be called twice
			// 
			int temp = header.Origin + header.Extent;

			this.overallWidth = System.Math.Max(this.overallWidth, temp);
		}

		internal void UpdateExclusiveWidthWithCardBand( UltraGridBand band )
		{
			if ( band == null || !band.CardView )
			{
				Debug.Fail("Invalid band in ColScrollRegion.UpdateExclusiveWidthWithCardBand" );
				return;
			}
			
			if ( band.ParentBand == null )
				return;

			// Use a stack variable to hold the temp value instead of
			// of placing the method call inside the 'max' macro
			// below since that causes the method to be called twice
			// 
			int temp;

			if (this.Layout.ViewStyleImpl.HasMultiRowTiers )
			{
				int parentBandOrigin = this.GetBandOrigin( band.ParentBand );
				int parentBandExtent = this.GetBandExtent( band.ParentBand );

				temp = parentBandOrigin 
					+ parentBandExtent 
					+ band.CalculateRequiredCardAreaWidth(1)
					+ band.PreRowAreaExtent;
			}
			else
			{
				temp = band.GetOrigin() + band.CalculateRequiredCardAreaWidth(1);
			}

			this.overallWidth = System.Math.Max(this.overallWidth, temp);
		}

		internal void RegenerateVisibleHeaders()
		{
			// check anti-recursion flag
			//
			if ( this.regenerating )
				return;

			// if the destroy flag is set then regen the headers
			//
			if ( this.destroyVisibleHeaders )
			{
				// reset the destroy flag
				//
				this.destroyVisibleHeaders = false;
				
				try 
				{
					// set the anti-recursion flag
					//
					this.regenerating = true;

					// get the overallwidth (which may trigger a recalculation)
					//
					if ( !this.HasExclusiveItems )
					{
						// JJD 12/27/01
						// Use the new GetOverallExtent method which can return
						// different extents for each region if card view bands
						// exist.
						//
						this.overallWidth = this.Layout.GetOverallExtent( this );
					}

					// SSP 12/3/01 UWG824
					// Reset the scroll info before recreating the header list so that
					// this.position gets set to proper value before going into 
					// RecreateHeaderList.
					//
					this.ResetScrollInfo( );
					
					// call the viewstyle to recreate the header list
					//
					bool rtn = this.Layout.ViewStyleImpl.RecreateHeaderList( this );

					// if there was a change then position the scrollbar
					//
					if ( rtn )
					{
						this.PositionScrollbar( true );
					}

					// reset the anti-recursion flag
					//
					this.regenerating = false;
				}
				catch ( Exception )
				{
					// just in case we blow up make sure
					// the anti-recursion flag gets reset
					//
					this.regenerating = false;
				
					throw;
				}
			}

		}

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
		public override bool ShouldSerialize() 
		{
			return base.ShouldSerialize();
		}
						   


		/// <summary>
		/// Resets all properties back to their default values
		/// </summary>
		public override void Reset() 
		{
			base.Reset();
					
		}

		
		

		
		
		/// <summary>
		/// Returns the height of the client area of this region in container
		/// coords.
		/// </summary>
		/// <param name="rsr">The RowScrollRegion</param>
		/// <returns>The client height of the ColScrollRegion</returns>
		// Note: The RowScrollRegion param is simply edited and then ignored
		//       since it will only have meaning when we implement row/col
		//       pivoted view styles in the future.
		public int GetClientHeight( RowScrollRegion rsr )
		{
			RowScrollRegion rowScrollRegion = null;

			// Use new ValidateRowRegionParam which takes a variant
			// ptr as an argument
			//
			rowScrollRegion = this.Layout.ValidateRowRegionParam( rsr );

			Debug.Assert( null != rowScrollRegion, "Row scroll region missing.");

			int height = this.Height;			

			if ( this.WillScrollbarBeShown() )
			{
				height -= SystemInformation.HorizontalScrollBarHeight;
			}

			if ( height < 0 )
				height = 0;

			return height;
		}

		/// <summary>
		/// Returns the width of the client area of this region in container
		/// coords.
		/// </summary>
		/// <param name="rsr">The RowScrollRegion</param>
		/// <returns>The client width of the ColScrollRegion</returns>
		public int GetClientWidth( RowScrollRegion rsr )
		{			
			if ( this.IsClone )
				return this.ClonedFrom.GetClientWidth( rsr );
				
			if ( rsr == null )
				rsr = this.Layout.Grid.ActiveRowScrollRegion;
				
			if ( rsr == null )			
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_46"));
			
			
			// Use newer GetClippedExtent method
			//
			int extent = this.ClippedExtent;

			if ( this.IsLastVisibleRegion && rsr.WillScrollbarBeShown() )
			{
				extent -= SystemInformation.VerticalScrollBarWidth;
			}

			if ( extent < 0 )
				extent = 0;

			return extent;			
		}

		/// <summary>
		/// Returns or sets the height of an object in container units.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Height</b> property is used to determine the vertical dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">For the ColScrollRegion object, this property returns the height available to row data. This value excludes the height of the grid's outer border. The height occupied by the scrollbars does not affect the value of this property.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public int Height
		{
			get
			{	
				Rectangle dataAreaRect;				
				dataAreaRect = this.Layout.Grid.ClientRectangle;				

				// Instead of using the grid client area, get the
				// data area UI element and intersect its rect
				//
				// Pass true into GetUIElement to trigger a verification of the 
				// sub element rects
				//
				// Only verify the UIElements if we are not scrolling
				//
				UIElement element = this.Layout.GetUIElement( !this.scrolling && !this.IsClone );

				if ( null != element )
				{
					DataAreaUIElement dataArea = (DataAreaUIElement)element.GetDescendant( typeof( DataAreaUIElement ) );

					if ( null != dataArea )
						dataAreaRect = Rectangle.Intersect(dataAreaRect, dataArea.RectInsideBorders );
				}

				int height = dataAreaRect.Height;

				return System.Math.Max( 0, height );
			}
		}

		/// <summary>
		/// Returns or sets the width of an object in container units.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Width</b> property is used to determine the horizontal dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">For the ColScrollRegion object, this property always includes the vertical scrollbar's <b>Width</b> for the RowScrollRegion.</p>
		/// </remarks>
        [ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public int Width
		{
			get
			{
				return this.Extent;
			}

			set
			{
				if ( value != this.Extent )
				{
					this.Extent = value;

					// SSP 4/25/03 UWG2190
					//
					if ( null != this.Layout )
						this.Layout.DirtyGridElement( true );

					this.NotifyPropChange( PropertyIds.Width, null );
				}
			}
		}

		/// <summary>
		/// Returns the distance between the left edge of an object and the left edge of the control. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">For the ColScrollRegion object, the value returned is expressed in terms of the coordinate system specified by the control's container.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public int Left
		{
			get
			{
				return this.Origin;
			}
		}


		/// <summary>
		/// Returns a scroll bar's maximum, or rightmost, position in a colscrollregion. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property indicates the value of a scroll bar's <b>Position</b> property when the scroll box is in its rightmost position.</p>
		/// <p class="body">The value of this property, itself, is the width of the area not in view in the colscrollregion, expressed in terms of the coordinate system set by the scale mode of the control's container.</p>
		/// <p class="body">When the total area is viewable in the colscrollregion, this value of this property is 0. </p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public int Range
		{
			get
			{
				
				int overallWidth;

				// get the overall width if we aren't exclusive
				//
				if ( this.ExclusiveItems.Count < 1 ) 
				{
					// JJD 12/27/01
					// Use the new GetOverallExtent method which can return
					// different extents for each region if card view bands
					// exist.
					//
					overallWidth = this.Layout.GetOverallExtent( this );
				}
				else
				{
					overallWidth = this.OverallWidth;
				}

				// Only take row scrollbars into account if they are visible
				//
				int rowScrollBarWidth = this.IsLastVisibleRegion
					// SSP 11/4/04 UWG3593
					// Don't assume the column scrollbars are visible.
					//
					//&& this.Layout.RowScrollRegions.AreScrollbarsVisible(true)
					&& this.Layout.RowScrollRegions.AreScrollbarsVisible( ScrollbarVisibility.Check )
					? SystemInformation.VerticalScrollBarWidth
					: 0;

				// Return a value for range that corresponds to the highest position
				// value possible
				//
				int range = overallWidth - ( this.Extent - rowScrollBarWidth );

				return System.Math.Max( 0, range );
			}

		}

		/// <summary>
		/// Returns the RowColRegionIntersectionUIElement object
		/// </summary>
		/// <param name="rsr">An optional RowScrollRegion. If 'null' will use the active RowScrollRegion. </param>
		public RowColRegionIntersectionUIElement GetUIElement( RowScrollRegion rsr )
		{			
			RowColRegionIntersectionUIElement rci;
    
			// Pass true into GetUIElement to trigger a verification of the 
			// sub element rects
			// Only verify the UIElements if we are not scrolling
			//
			UIElement element = this.Layout.GetUIElement( !this.scrolling && !this.IsClone);

			RowScrollRegion rowScrollRegion = null;

			// Use new ValidateRowRegionParam which takes a variant
			// ptr as an argument
			//
			rowScrollRegion = this.Layout.ValidateRowRegionParam( rsr );			

			Debug.Assert( null != rowScrollRegion, "Row scroll region missing in ColScrollRegion.GetUIElement");

			object[] contexts = new Object[2];			
			contexts[0] = rowScrollRegion;
			contexts[1] = this;
			rci = (RowColRegionIntersectionUIElement)element.GetDescendant( typeof(RowColRegionIntersectionUIElement), contexts);

			return rci;			
		}
		
		/// <summary>
		/// Scrolls a scrolling region by the specified increment.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to scroll a scrolling region.</p>
		/// <p class="body">When a colscrollregion is scrolled, the value of the colscrollregion's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated</p>
		/// <p class="body">The <b>ScrollCellIntoView</b>, <b>ScrollColIntoView</b>, <b>ScrollGroupIntoView</b>, <b>ScrollHeaderIntoView</b> and <b>ScrollRowIntoView</b> methods can be invoked to scroll an object into a scrolling region's viewable area. </p>
		/// </remarks>
		/// <param name="action">The ColScrollAction to perform</param>
		public void Scroll( ColScrollAction action )
		{			
			if ( this.IsClone )
			{
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_47"));
			}
				
			ScrollEventType e = new ScrollEventType();

			// validate action
			//
			switch ( action )
			{
				case ColScrollAction.LineLeft:
					e = ScrollEventType.SmallDecrement;
					break;
				case ColScrollAction.LineRight:
					e = ScrollEventType.SmallIncrement;
					break;
				case ColScrollAction.PageLeft:
					e = ScrollEventType.LargeDecrement;
					break;
				case ColScrollAction.PageRight:
					e = ScrollEventType.LargeIncrement;
					break;
				case ColScrollAction.Left:
					e = ScrollEventType.First;
					break;
				case ColScrollAction.Right:
					e = ScrollEventType.Last;
					break;
				default:					 
					throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_ArgumentException_48"));				
			}
						
			// call OnScroll to do the scroll action
			//
			this.OnScroll(this.ScrollBarInfo, e); 

			// reset the lastScrollType so we don't start doing
			// pixel level scrolling
			//
			this.lastScrollType = ScrollEventType.EndScroll;

			return;			
		}


		/// <summary>
		/// Scrolls the specified cell into view for a scrolling region.
		/// </summary>
        /// <param name="cell">The cell to scroll into view.</param>
        /// <param name="rowScrollRegion">The RowScrollRegion</param>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a cell is viewable in a column or row scrolling region.</p>
		/// <p class="body">If this method is invoked for a colscrollregion and the column is already in the viewable area of the region, this method does not perform any scrolling.</p>
		/// <p class="body">If the colscrollregion is scrolled as a result of invoking this method, the value of the column scrolling region's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated. If the rowscrollregion is scrolled as a result of invoking this method, the <b>BeforeRowRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b>, <b>ScrollColIntoView</b>, <b>ScrollGroupIntoView</b>, <b>ScrollHeaderIntoView</b> and <b>ScrollRowIntoView</b> methods can also be invoked to scroll an object into a scrolling region's viewable area.</p>
		/// </remarks>
		public void ScrollCellIntoView(UltraGridCell cell, RowScrollRegion rowScrollRegion )
		{
			this.ScrollCellIntoView( cell, rowScrollRegion, false );
		}

		/// <summary>
		/// Scrolls the specified cell into view for a scrolling region.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a cell is viewable in a column or row scrolling region.</p>
		/// <p class="body">If this method is invoked for a colscrollregion and the column is already in the viewable area of the region, this method does not perform any scrolling.</p>
		/// <p class="body">If the colscrollregion is scrolled as a result of invoking this method, the value of the column scrolling region's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated. If the rowscrollregion is scrolled as a result of invoking this method, the <b>BeforeRowRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b>, <b>ScrollColIntoView</b>, <b>ScrollGroupIntoView</b>, <b>ScrollHeaderIntoView</b> and <b>ScrollRowIntoView</b> methods can also be invoked to scroll an object into a scrolling region's viewable area.</p>
		/// </remarks>
        /// <param name="cell">The cell to scroll into view.</param>
        /// <param name="rowScrollRegion">The RowScrollRegion</param>
		/// <param name="leftAlign">True to specify that the cell should be aligned to the left of the ColScrollRegion</param>
		public void ScrollCellIntoView(UltraGridCell cell, RowScrollRegion rowScrollRegion, bool leftAlign )
		{
			// make sure the cell is active and is the same as our layout
			//
			if (cell.Layout != this.Layout)			
				throw new  InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_49"));				
		
			// If the cell is hidden don't do anything
			//						
			// SSP 5/14/03 - Hiding Individual Cells Feature
			// Changed the original internal Cell.Hidden to Cell.InternalHidden because we
			// needed to add Hidden public method off the cell to implement this feature.
			//
			//if ( !cell.Hidden )
			if ( !cell.InternalHidden )
			{			    
				RowScrollRegion rsr = null;
			    
				rsr = this.Layout.ValidateRowRegionParam( rowScrollRegion );
				Debug.Assert( null != rsr, "Row scroll region missing in ColScrollRegion.ScrollCellIntoView method.");

				// scroll the row into view
				//
				rsr.ScrollRowIntoView( cell.Row );

				// scroll the column into view
				//
				this.ScrollColIntoView( cell.Column, leftAlign );
			}
		}

		/// <summary>
		/// Scrolls the specified column into view for a colscrollregion.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a column is viewable in a column scrolling region.</p>
		/// <p class="body">If the column is already in the viewable area of the column scrolling region, this method does not perform any scrolling.</p>
		/// <p class="body">If the colscrollregion is scrolled as a result of invoking this method, the value of the column scrolling region's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b>, <b>ScrollCellIntoView</b>, <b>ScrollGroupIntoView</b> and <b>ScrollHeaderIntoView</b> methods can also be invoked to scroll an object into a colscrollregion's viewable area.</p>
		/// </remarks>
		/// <param name="column"></param>
		public void ScrollColIntoView( Infragistics.Win.UltraWinGrid.UltraGridColumn column )
		{
			this.ScrollColIntoView( column, false );
		}

		/// <summary>
		/// Scrolls the specified column into view for a colscrollregion.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a column is viewable in a column scrolling region.</p>
		/// <p class="body">If the column is already in the viewable area of the column scrolling region, this method does not perform any scrolling.</p>
		/// <p class="body">If the colscrollregion is scrolled as a result of invoking this method, the value of the column scrolling region's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b>, <b>ScrollCellIntoView</b>, <b>ScrollGroupIntoView</b> and <b>ScrollHeaderIntoView</b> methods can also be invoked to scroll an object into a colscrollregion's viewable area.</p>
		/// </remarks>
		/// <param name="column"> </param>
		/// <param name="leftAlign"> </param>
		public void ScrollColIntoView( Infragistics.Win.UltraWinGrid.UltraGridColumn column, bool leftAlign )
		{
			if (this.Layout.Grid.InInitializeLayout &&							
				Infragistics.Win.UltraWinGrid.AllowColSizing.Synchronized == column.Band.AllowColSizingResolved &&
				leftAlign)
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Avoid unused private fields
				//this.columnToScrollIntoView = column;

				this.Layout.ScrollColIntoViewPending = true;
				return;
			}

			// Added support for optional LeftAlign parameter
			//
			this.ScrollHeaderIntoView( column.Header, leftAlign );
		}

		/// <summary>
		/// Scrolls the specified group into view for a colscrollregion.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a group is viewable in a column scrolling region. If the group is already in the viewable area of the column scrolling region, this method does not perform any scrolling.</p>
		/// <p class="body">If invoking this method does cause the column scrolling region to be scrolled, the value of the column scrolling region's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b>, <b>ScrollCellIntoView</b>, <b>ScrollColIntoView</b> and <b>ScrollHeaderIntoView</b> methods can also be invoked to scroll an object into a column scrolling region's viewable area.</p>
		/// </remarks>
		/// <param name="group">The group to scroll into view</param>
		public void	ScrollGroupIntoView( UltraGridGroup group )
		{
			this.ScrollGroupIntoView( group, false );
		}

		/// <summary>
		/// Scrolls the specified group into view for a colscrollregion.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a group is viewable in a column scrolling region. If the group is already in the viewable area of the column scrolling region, this method does not perform any scrolling.</p>
		/// <p class="body">If invoking this method does cause the column scrolling region to be scrolled, the value of the column scrolling region's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b>, <b>ScrollCellIntoView</b>, <b>ScrollColIntoView</b> and <b>ScrollHeaderIntoView</b> methods can also be invoked to scroll an object into a column scrolling region's viewable area.</p>
		/// </remarks>
        /// <param name="group">The group to scroll into view</param>
		/// <param name="leftAlign">True to specify that the group should be aligned to the left edge of the ColScrollRegion</param>
		public void	ScrollGroupIntoView( UltraGridGroup group, bool leftAlign )
		{
			// Added support for optional LeftAlign parameter
			//
			this.ScrollHeaderIntoView( group.Header, leftAlign );
		}

		
		internal int SafeExtent
		{
			get
			{
				// call m_Layout.GetUIElement() if the extent is zero (this will
				// force the extent to be set if it wasn't already)
				//
				// Pass true into GetUIElement to trigger a verification of the 
				// sub element rects
				//
				// Pass true for fForceInitializeRect to cover the
				// situation where, for example, a split is performed in
				// InitializeLayout.
				//
				if ( 0 == this.Extent )
					this.Layout.GetUIElement(false, true);

				return this.Extent;
			}
		}
		internal int ClippedExtent
		{
			get
			{
				int extent = this.SafeExtent;

				if ( extent > 0 && this.Layout.IsDisplayLayout )
				{
					// Get the data area UI element clip the extent to it
					//
					UIElement  element = this.Layout.GetUIElement(false);

					if (null != element)
					{
						element = element.GetDescendant( typeof( Infragistics.Win.UltraWinGrid.DataAreaUIElement ) );

						if ( null != element )
						{
							Rectangle  dataAreaRect = element.RectInsideBorders;

							// clip the extent to the bottom of the data area
							//
							if ( extent > dataAreaRect.Right - this. SafeOrigin )
								extent = dataAreaRect.Right - this.Origin;
						}
						else
						{
							// If we don't have a data area that means the grid
							//may be too smaall so set the extent to 0
							//
							extent = 0;
						}
					}
				}

				if ( extent < 0 )
					extent = 0;

				return extent;
			}
		}
		internal int SafeOrigin
		{
			get
			{
				// call Layout.GetUIElement() if the extent is zero (this will
				// force the extent to be set if it wasn't already)
				//
				// Pass true into GetUIElement to trigger a verification of the 
				// sub element rects
				//
				// Pass true for fForceInitializeRect to cover the
				// situation where, for example, a split is performed in
				// InitializeLayout.
				//
				if ( 0 == this.Extent )
					this.Layout.GetUIElement( false, true );

				return this.Origin;
			}
		}


		/// <summary>
		/// The index of this region in the col scroll regions collection
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[ MergableProperty( false ) ]
		public int Index
		{
			get
			{
				// SSP 12/4/02 UWG1840
				// Return -1 if the collection is null rather than assuming it's non-null and then
				// accessing IndexOf method which will throw an null reference exception.
				//
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//if ( null == this.collection )
				if ( null == this.collectionValue )
					return -1;

				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return ((ColScrollRegionsCollection)this.collection).IndexOf( this );
				return ((ColScrollRegionsCollection)this.collectionValue).IndexOf( this );
			}
		}

		// SSP 2/14/04 UWG2964
		// Added GetBandsTotalFixedHeadersExtent method. We need to take into account the fixed
		// headers when calculating the LargeIncrement for the column scroll region scrollbar.
		//
		internal int GetBandsTotalFixedHeadersExtent( )
		{
			if ( null == this.Layout || null == this.Layout.SortedBands )
				return 0;

			if ( this.bandsTotalFixedHeadersExtentVerifiedVersion != this.Layout.GrandVerifyVersion )
			{
				int maxFixedHeadersRight = 0;

				// SSP 5/20/04 UWG3255
				// For horizontal view style (HasMultiRowTiers is true), calculations are different.
				// Add the if block and enclosed the existing code in the else block.
				// 
				if ( this.Layout.ViewStyleImpl.HasMultiRowTiers )
				{
					for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
					{
						UltraGridBand band = this.Layout.SortedBands[i];

						if ( band.UseFixedHeadersResolved( this ) )
						{
							int bandFixedHeadersExtent = band.PreRowAreaExtent 
								+ band.RowSelectorExtent + band.GetFixedHeadersExtent( this );

							maxFixedHeadersRight += bandFixedHeadersExtent;
						}
					}
				}
				else
				{
					for ( int i = 0; i < this.Layout.SortedBands.Count; i++ )
					{
						UltraGridBand band = this.Layout.SortedBands[i];

						if ( band.UseFixedHeadersResolved( this ) )
						{
							int fixedHeadersRight = band.GetOrigin( BandOrigin.RowSelector ) + band.GetFixedHeadersExtent( this );
							maxFixedHeadersRight = Math.Max( maxFixedHeadersRight, fixedHeadersRight );
						}
					}
				}

				this.cachedMaxFixedHeadersRight = maxFixedHeadersRight;
				this.bandsTotalFixedHeadersExtentVerifiedVersion = this.Layout.GrandVerifyVersion;
			}

			return this.cachedMaxFixedHeadersRight;
		}

		/// <summary>
		/// sets the scroll information and scroll bar properties in accords with
		/// the state of the scroll region
		/// </summary>
		protected override void ResetScrollInfo()
		{ 
			if ( 0 == this.overallWidth)
				return;

			if ( !this.scrollbarRectSet )
				return;

			// SSP 3/10/05 BR02638 BR02742
			// Only setup the scrollbar info if the layout is a display layout. For print and
			// excel export layouts this should not be necessary. Not only that the code below
			// resets the Position of the col scroll region which causes the columns that were
			// fully visible on the previous page to be repeated on the next page when printing.
			//
			if ( null == this.Layout || ! this.Layout.IsDisplayLayout )
				return;

			int siPos, siMax, siMin, siPage;
		    
			siMin = 0;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//int overallWidth;
			//
			//if ( this.ExclusiveItems.Count < 1)
			//{
			//    // JJD 12/27/01
			//    // Use the new GetOverallExtent method which can return
			//    // different extents for each region if card view bands
			//    // exist.
			//    //
			//    overallWidth = this.Layout.GetOverallExtent( this );
			//}
			//else
			//{
			//    overallWidth = this.overallWidth;
			//}
		    
			// If this is the last col scroll region then allow
			// for a row scroll bar
			// 
			// Only take row scrollbars into account if they are visible
			//
			int rowScrollBarWidth = this.IsLastVisibleRegion 
				// SSP 11/4/04 UWG3593
				// Don't assume the column scrollbars are visible.
				//
				//&& this.Layout.RowScrollRegions.AreScrollbarsVisible ( true )
				&& this.Layout.RowScrollRegions.AreScrollbarsVisible( ScrollbarVisibility.Check )
				? SystemInformation.VerticalScrollBarWidth 
				: 0;

			int page = this.Extent - rowScrollBarWidth;

			// SSP 2/14/04 UWG2964
			// Also take into account the fixed headers.
			//
			// ------------------------------------------------------------------------------------
			page -= this.GetBandsTotalFixedHeadersExtent( );
			// ------------------------------------------------------------------------------------

			// set the page (largechange) to 90 percent of the visible
			page = (int)((float)page * 0.9f);
		    
			// SSP 5/18/05 BR04060
			//
			//siPage = System.Math.Max(1, page);
			siPage = page = System.Math.Max(1, page);

			siPos = System.Math.Max ( 0, System.Math.Min( this.Position , this.overallWidth - siPage ) );

			// JJD 8/7/01 - UWG30
			// Make sure the max isn't less than the current position, otherwise an exception
			// is thrown below
			//
			//siMax = page + ( this.overallWidth - (this.Extent - rowScrollBarWidth ) ) - 1;
			// SSP 5/18/05 BR04060
			// Reverting back to the old code. The if block below ensures that the scrollbar position is
			// less than the max rather than the other way around. The problem with enlarging the max
			// instead of shrinking the pos is that the scrollbar's max would end up being out of sync
			// with the extent of the bands.
			//
			//siMax = System.Math.Max ( siPos, page + ( this.overallWidth - (this.Extent - rowScrollBarWidth ) ) - 1 );
			siMax = System.Math.Max( siMin, page + ( this.overallWidth - ( this.Extent - rowScrollBarWidth ) ) - 1 );


			// SSP 11/26/01 UWG570
			// Ensure that the pos is less than or equal to siMax - siPage
			// because we do not want to exceed that otherwise we will be left
			// with empty space after the last column. Actually it could be
			// worse if columns are deleted and pos is not ensured to be within
			// this constraint in this manner. 
			//
			if ( siPos > 0 && siPos > 1 + siMax - siPage )
			{
				siPos = Math.Max( siMin, 1 + siMax - siPage );
				this.destroyVisibleHeaders = true;
			}


			// If allow scroll is false set the max to 0 to disable the scrollbars
			//
			// Remove allow scroll property
			//
			if (this.IsClone)
			{
				this.position = siPos;
			}
			else
			{
                // MRS 12/6/2007 - BR28937
                // This 'if' block basically prevents us from updating the Min, Max, and LargeChange
                // on the scrollbar when the grid is disabled. There does not appear to be any reason
                // for this, and doing do causes problems, because we don't re-set the properties when
                // the grid is re-enabled. So I am ripping it out so that the Min, Max, and LargeChange
                // are always set, regardless of the Enabled state of the grid. 
                //// AS - 12/14/01
                ////if ( !this.Layout.Grid.Enabled && this.ScrollBarInfo.Created )
                //if ( !this.Layout.Grid.Enabled )
                //{
                //    // AS - 12/14/01
                //    // No longer needed since we are using uielements for the scrollbar.
                //    // When the scrollbar uielement is disable, the scrollbar will be
                //    // as well.
                //    //
                //    //this.ScrollBarInfo.Enabled = false;
                //}
                //else
				{
					this.ScrollBarInfo.Minimum = siMin;
					this.ScrollBarInfo.Maximum = siMax;
					this.ScrollBarInfo.LargeChange = siPage;
					
					// SSP 5/21/03 - Optimizations
					//
					//this.ScrollBarInfo.SmallChange = COL_LINE_SCROLL_DELTA;
					if ( null != this.Layout )
					this.scrollBarInfoValue.SmallChange = Math.Min( siPage, 
						null != this.Layout ? this.Layout.ColumnScrollbarSmallChange : HORIZONTAL_SCROLLBAR_SMALL_CHANGE );

					this.ScrollBarInfo.Value =  this.position = siPos;
					 
				}  
			}
		}

		/// <summary>
		/// Scrolls the specified header into view for a colscrollregion.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a column or group header is viewable in a column scrolling region.</p>
		/// <p class="body">If the header is already in the viewable area of the column scrolling region, this method does not perform any scrolling.</p>
		/// <p class="body">If the colscrollregion is scrolled as a result of invoking this method, the value of the column scrolling region's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b>, <b>ScrollCellIntoView</b>, <b>ScrollColIntoView</b> and <b>ScrollGroupIntoView</b> methods can also be invoked to scroll an object into a colscrollregion's viewable area.</p>
		/// </remarks>
		/// <param name="header">The HeaderBase to scroll into view.</param>
		/// <param name="leftAlign">True to specify that the header should be left aligned in the ColScrollRegion</param>
		public void ScrollHeaderIntoView  ( HeaderBase header, bool leftAlign )
		{	
			if ( header.Hidden )
				return;
			
			// JJD 12/21/01
			// If this band is in card view just exit since it is never necessary to scroll
			// a column into view with a card. They are all in view.
			//
			if ( header.Band.CardView )
			{
				//	BF 1.24.05	NAS2005 Vol1 - CardView vertical scrolling
				CardAreaUIElement cardAreaElement = header.Band.CardAreaUIElement;
				if ( cardAreaElement != null && cardAreaElement.HasVerticalScrollbar )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.ScrollCardColIntoView( header );
					ColScrollRegion.ScrollCardColIntoView( header );
				}

				return;
			}

			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( null != header.ExclusiveColScrollRegion )			
			//{
			//    if ( this != header.ExclusiveColScrollRegion )
			ColScrollRegion exclusiveColScrollRegion = header.ExclusiveColScrollRegion;

			if ( null != exclusiveColScrollRegion )
			{
				if ( this != exclusiveColScrollRegion )
				{
					throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_50"));
				}
			}

			// SSP 5/23/03 - Fixed headers
			// If the header is fixed, then return without scrolling because fixed headers can't be
			// scrolled.
			//
			if ( header.Band.IsHeaderFixed( this, header ) 
				&& ( ! header.Band.Layout.ViewStyleImpl.HasMultiRowTiers || null == header.Band.ParentBand ) )
				return;

			Rectangle region = new Rectangle();

			// get the regions rect
			//
			this.GetRegionRect(ref region, Layout.Grid.ActiveRowScrollRegion );

			int newRight = region.Left + this.GetClientWidth( this.Layout.Grid.ActiveRowScrollRegion );
		
			if (newRight < region.Right)
			{
				region.Width = newRight - region.Left;
			}

			Rectangle headerRectangle = region;

			int origin = 0;
			int extent = 0;


			//call to get the origin and extent
			// Get the origin and extent of the position item using the new
			// GetDimensions method
			//
			header.GetDimensions(
				Infragistics.Win.UltraWinGrid.PositionDimensions.InsidePreRowSelectors,
				Infragistics.Win.UltraWinGrid.DimOriginBase.Relative,
				ref origin,
				ref extent,
				this);


			origin += this.Origin;

			headerRectangle.X = origin;
			headerRectangle.Width = extent;
			
			if ( !leftAlign )
			{
				Rectangle intersectRect = Rectangle.Intersect(headerRectangle, region);				
				if (!intersectRect.IsEmpty && intersectRect.Equals(headerRectangle))
				{
					// SSP 6/09/03 - Fixed headers
					// Added the if block and enclosed the already existing code in the else statement.
					// Ensure that the header is not scrolled underneath the fixed headers before
					// returning.
					//
					if ( header.Band.UseFixedHeadersResolved( this ) )
					{
						//int fixedHeadersAreaRight = header.Band.GetLeftFixedHeadersAreaRight( this );
						int leftFixedHeadersAreaRight = header.Band.GetLeftFixedHeadersAreaRight( this );
						int rightFixedHeadersAreaLeft = header.Band.GetRightFixedHeadersAreaLeft( this );

						//if ( headerRectangle.X > leftFixedHeadersAreaRight )
						if ( headerRectangle.X > leftFixedHeadersAreaRight && headerRectangle.Right <= rightFixedHeadersAreaLeft )
							return;
					}
					else
					{
						return;
					}
				}
			}

			// SSP 6/09/03 - Fixed headers
			//
			// ------------------------------------------------------------------------------
			if ( header.Band.UseFixedHeadersResolved( this ) )
			{
				// scroll the visibleHeader into view
				//
				this.InternalScroll(ScrollEventType.EndScroll, 0, 
					headerRectangle, leftAlign, true, header );

				return;
			}
			// ------------------------------------------------------------------------------

			// scroll the visibleHeader into view
			//
			this.InternalScroll(ScrollEventType.EndScroll, 0, 
				headerRectangle, leftAlign);
		}

		//	BF 1.24.05	NAS2005 Vol1 - CardView vertical scrolling
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void ScrollCardColIntoView( HeaderBase header )
		private static void ScrollCardColIntoView( HeaderBase header )
		{
			UltraGridBand band = header.Band;
			UltraGridColumn column = header.Column;

			if ( column == null || ! band.CardView )
			{
				Debug.Assert( false, "ScrollCardColIntoView called on a header that  does not represent a CardView column - method will do nothing." );
				return;
			}

			//	Return if the header is hidden
			if ( header.Hidden )
				return;

			//	Start with the CardCaptionHeight; note that this accounts
			//	for whether the caption is actually being displayed. Also,
			//	add in 1/2 of the CardSpacingVertical, since that is the amount
			//	by which the elements are offset from the top.
			int cardTop = band.CardCaptionHeight + (band.CardSpacingVertical / 2);

			// SSP 10/28/05 BR06780
			// Added the if block and enclosed the existing code into the else block. We
			// need to take into account the row-layout mode.
			// 
			int cardBottom;
			if ( band.UseRowLayoutResolved )
			{
				// MD 8/2/07 - 7.3 Performance
				// Use already cached value - Prevent calling expensive getters multiple times
				//RowLayoutColumnInfo ci = header.Column.RowLayoutColumnInfo;
				RowLayoutColumnInfo ci = column.RowLayoutColumnInfo;

				cardTop += ci.CachedItemRectCell.Top;
				cardBottom = cardTop + ci.CachedItemRectCell.Height;
			}
			else
			{
				//	Iterate the ordered headers for this card band, until
				//	we hit this card column's header, and sum the heights
				//	of the columns that precede this one; this will give
				//	us the logical top of the card we are scrolling into
				//	view.
				// SSP 6/16/05
				// If groups are displayed then OrderedHeaders will contain group headers. We
				// have to take that into account here.
				// 
				// ----------------------------------------------------------------------------
				
				ICollection orderedHeaders = band.OrderedHeaders;			

				// If groups are displayed then OrderedHeaders will contain group headers. We are interested
				// in column headers here. Also we need to loop through them in the same order as the card
				// row element positions the headers.
				//
				if ( band.GroupsDisplayed )
				{
					ArrayList list = new ArrayList( );
					for ( int i = 0; i < band.OrderedHeaders.Count; i++ )
					{
						UltraGridGroup group = band.OrderedHeaders[i].Group;

						// MD 1/19/09 - Groups in RowLayout
						// A non-hidden group could be within a hidden group. USe the resolved value.
						//if ( null != group && ! group.Hidden )
						if ( null != group && !group.HiddenResolved )
						{
							// MD 1/21/09 - Groups in RowLayouts
							// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
							// actually visible in the group
							//for ( int j = 0; j < group.Columns.Count; j++ )
							//    list.Add( group.Columns[j].Header );
							foreach ( UltraGridColumn groupColumn in group.ColumnsResolved )
								list.Add( groupColumn.Header );
						}
					}

					orderedHeaders = list;
				}

				foreach ( HeaderBase headerIterator in orderedHeaders )
				{
					//	Skip hidden ones, assert if it isn't a Column
					UltraGridColumn currentCol = headerIterator.Column;
                    // MRS 2/23/2009 - Found while fixing TFS14354
					//if ( currentCol == null || currentCol.Hidden )
                    if (currentCol == null || currentCol.HiddenResolved)
					{
						Debug.Assert( currentCol != null, "Column property of an OrderedHeader returned null for a card column - unexpected." );
						continue;
					}

					if ( currentCol == column )
						break;

					cardTop += currentCol.CardCellHeight;
				}
				// ----------------------------------------------------------------------------

				cardBottom = cardTop + column.CardCellHeight;
			}

			CardAreaUIElement cardAreaElement = band.CardAreaUIElement;

			if ( cardAreaElement == null )
			{
				Debug.Assert( false, "Unable to obtain a CardAreaUIElement for this band in ColScrollRegion.ScrollCardIntoView." );
				return;
			}

			CardAreaScrollRegionUIElement cardAreaScrollRegionElement = cardAreaElement.GetDescendant( typeof(CardAreaScrollRegionUIElement) ) as CardAreaScrollRegionUIElement;
			if ( cardAreaScrollRegionElement != null )
			{
				Rectangle rectInsideBorders = cardAreaScrollRegionElement.RectInsideBorders;
				int cardAreaScrollPos = cardAreaElement.CardAreaScrollRegionVerticalOffset;

				int cardAreaBottom = cardAreaScrollPos + rectInsideBorders.Height;
				int cardAreaTop = cardAreaScrollPos;

				if ( cardBottom > cardAreaBottom )
				{
					int difference = cardBottom - rectInsideBorders.Height;
					cardAreaElement.CardAreaScrollRegionVerticalOffset = difference;
				}
				else
				if ( cardTop < cardAreaTop )
					cardAreaElement.CardAreaScrollRegionVerticalOffset = cardTop;
			}

		}

		// JJD 1/19/02 - UWG456
		// Added overload to ScrollRowIntoView that takes a row scroll region
		//
		/// <summary>
		/// Scrolls the specified row into view for a rowscrollregion.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a row is viewable in a rowscrollregion.</p>
		/// <p class="body">If the row is already in the viewable area of the row scrolling region, this method does not perform any scrolling.</p>
		/// <p class="body">If the rowscrollregion is scrolled as a result of invoking this method, the <b>BeforeRowRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b> and <b>ScrollCellIntoView</b> methods can also be invoked to scroll an object into a rowscrollregion's viewable area.</p>
		/// </remarks>
		/// <param name="row">The row to scroll into view</param>
		public void ScrollRowIntoView ( UltraGridRow row )
		{
			this.ScrollRowIntoView( row, this.Layout.ActiveRowScrollRegion );
		}

		/// <summary>
		/// Scrolls the specified row into view for a rowscrollregion.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a row is viewable in a rowscrollregion.</p>
		/// <p class="body">If the row is already in the viewable area of the row scrolling region, this method does not perform any scrolling.</p>
		/// <p class="body">If the rowscrollregion is scrolled as a result of invoking this method, the <b>BeforeRowRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b> and <b>ScrollCellIntoView</b> methods can also be invoked to scroll an object into a rowscrollregion's viewable area.</p>
		/// </remarks>
        /// <param name="row">The row to scroll into view</param>
		/// <param name="rsr">The RowScrollRegion</param>
		public void ScrollRowIntoView ( UltraGridRow row, RowScrollRegion rsr )
		{
			Rectangle regionRect;

			// get the region's rect
			//
			// JJD 1/19/02 - UWG456
			// Use passed in region
			//
			regionRect = rsr.Rect;

			Rectangle rowRect = regionRect;

			HeaderBase firstHeader = null;

			if ( this.ExclusiveItems.Count > 0)
			{
				// loop over the exclusive items in our list looking
				// for the first visible col from the row's band
				//
				foreach ( HeaderBase header in this.ExclusiveItems )
				{
                    
					// Skip over hidden position items
					//
					if ( header.Hidden )
						continue;

					// skip over position items from other bands
					//
					if ( header.Band != row.Band )
						continue;

					if ( null != header.GetFirstVisibleCol( true ) )
					{
						firstHeader = header;
						break;
					}
				}
			}
			else
			{	
				HeadersCollection headers = row.Band.OrderedHeaders;
				if ( null != headers )
				{
					foreach ( HeaderBase header in headers ) 
					{

						// Skip over hidden position items
						//
						if ( header.Hidden )
							continue;

						// Skip over position items that are in any
						// exclusive col scrolling region
						//
						if ( null != header.ExclusiveColScrollRegion )
							continue;
						
						if ( null != header.GetFirstVisibleCol( true ) )
						{
							firstHeader = header;
							break;
						}
					}
				}

			}

			if ( null == firstHeader )
				return;
			
			UltraGridColumn firstCol = firstHeader.GetFirstVisibleCol( true );

			if ( null == firstCol )
				return;

			UltraGridColumn lastCol = firstCol.Band.Header.GetRelatedVisibleCol(firstCol, 
				Infragistics.Win.UltraWinGrid.NavigateType.Last, true);

			if ( null == lastCol )
				lastCol = firstCol;

			// calculate what the row's rect would be based on the current
			// scroll position
			//
			rowRect.X = firstCol.Header.OverallOrigin - this.Position;			
			rowRect.Width  = lastCol.Header.OverallOrigin + 
				lastCol.Extent - this.Position - rowRect.X;


			Rectangle intersectRect = Rectangle.Intersect(rowRect, regionRect );
			// if the row is completely visible then exit
			//
			if ( intersectRect.IsEmpty &&
				Rectangle.Equals(intersectRect, rowRect ) )
				return;

			// scroll the row into view
			//
			InternalScroll( ScrollEventType.EndScroll, 0, rowRect );
		}
		
        		
		
		internal override bool OnScroll( ScrollBarInfo scrollBar,
			ScrollEventType scrollType, bool exitEditMode, bool invalidateRegion )
		{		
			// call this function so that the grid exits the edit mode
			// on active cell and does data updating if UpdateMode
			// is set to OnCellChangeOrLostFocus or OnRowChangeOrLostFocus
			//this.Layout.Grid.InternalLostFocus( true );

			// exit edit mode on scroll
			if ( null != this.Layout.ActiveCell && this.Layout.ActiveCell.IsInEditMode
				&& exitEditMode )
			{				
				this.Layout.ActiveCell.ExitEditMode();

				// SSP 5/3/02
				// If we did not exit the edit mode either because the ExitEditMode
				// event was cancelled or because there was an invalid value in the
				// cell, then we do not want to scroll.
				//
				if ( null != this.Layout.ActiveCell && this.Layout.ActiveCell.IsInEditMode )
				{
					// Cancel the scroll. Pass in true to prevent any scroll notifications from
					// coming in.
					//
					scrollBar.CancelScroll( true );

					return false;
				}

				// hide HTML doc window, if we have one				
				//this.Layout.HideHTMLDocWindow();
			}

			Debug.Assert( this.ScrollBarInfo == scrollBar, "Wrong scrollbar passed into ScrollRegionBase.OnScroll");

			return this.InternalScroll( scrollType, COL_LINE_SCROLL_DELTA );
		}


		internal bool InternalScroll( ScrollEventType  scrollType, int lineDelta )
		{
			return this.InternalScroll(scrollType, lineDelta, new Rectangle(0,0,0,0));
		}

		internal bool InternalScroll( ScrollEventType  scrollType, int lineDelta,
			Rectangle scrollIntoViewRect)
		{
			return this.InternalScroll( scrollType, lineDelta, scrollIntoViewRect, false );
		}

		internal bool InternalScroll( ScrollEventType  scrollType, int lineDelta,
			Rectangle scrollIntoViewRect, bool leftAlign  )
		{
			return this.InternalScroll( scrollType, lineDelta, scrollIntoViewRect, leftAlign, false, null );
		}

		internal bool InternalScroll( ScrollEventType  scrollType, int lineDelta,
									Rectangle scrollIntoViewRect, bool leftAlign,
			// SSP 6/10/03 - Fixed headers
			// Added below two parameters to support fixed column situations.
			//
			bool takeIntoAccountFixedHeaders,
			HeaderBase headerBeingScrolled
			)
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			//
			// If we are in the BeforeColRegionScroll event,
			// we wind up not scrolling correctly, so lets cache
			// a member to tell us to dirty the grid after the event
			// has returned from the host environment (ex VB).
			//
			if (null != grid &&
				null != grid.EventManager &&
				grid.EventManager.InProgress( Infragistics.Win.UltraWinGrid.GridEventIds.BeforeColRegionScroll ))
			{
				this.dirtyGridElement = true;				
			}

			int oldScrollPosition     = this.Position;

			// clone ourself
			//		    
			ColScrollRegion clone = (ColScrollRegion)this.Clone();
			
			if ( null == clone ) 
			{
				return false;				
			}
		        
			// Added flag to detect if the headers collection was regenerated
			// during a scrolling operation
			//		    
			this.regeneratedDuringScrolling = false;

			// Set a flag so we know that we are scrolling
			//		    
			this.scrolling = true;

			// SSP 5/23/02
			// Implemented code to not to invalidate and/or update the control if IsUpdating
			// is ture because we are going to be invalidating the whole grid once EndUpdate
			// is called anyways.
			//
			bool isUpdating = null != this.Layout && null != this.Layout.Grid && this.Layout.Grid.IsUpdating;

			// JJD 12/26/01
			// Call Update again to force a draw of any previously
			// invalidated areas
			//
			// SSP 11/24/03 UWG2057
			// Don't check if the grid has been created or not. Instead check if the
			// display control (ControlFroDisplay) has been created. In the case of
			// an UltraCombo which is hidden, Created will return false since it's
			// never been made visible. However it's drop down portion may be
			// visible if it were dropped down in a grid.
			//
			//if ( null != this.Layout.Grid && this.Layout.Grid.Created )
			//	this.Layout.Grid.ControlForGridDisplay.Update();
			Control displayControl = null != this.Layout && null != this.Layout.Grid 
				? this.Layout.Grid.ControlForGridDisplay : null;

			// SSP 3/24/05 BR02994
			//
			bool shouldUpdate = ! isUpdating && null != displayControl && displayControl.Created;

			// SSP 3/26/04 UWG3123
			// Check the isUpdating flag. We don't want to update the control if BeginUpdate has
			// been called.
			//
			//if ( null != displayControl && displayControl.Created )
			if ( shouldUpdate )
				displayControl.Update();

			// call ScrollPositionItems on the clone
			//			
			// Added support for optional LeftAlign parameter
			//		    
			// SSP 6/10/03 - Fixed headers
			// Added below parameter to support fixed column situations.
			//
			//clone.ScrollHeaders( scrollType, lineDelta, scrollIntoViewRect, leftAlign);
			clone.ScrollHeaders( scrollType, lineDelta, scrollIntoViewRect, leftAlign, takeIntoAccountFixedHeaders, headerBeingScrolled );

			// Reset the scrolling flag
			//		    
			this.scrolling = false;

			// Added flag to detect if the headers collection was regenerated
			// during a scrolling operation
			//		    
			this.regeneratedDuringScrolling = false;

			if ( oldScrollPosition == this.Position )
			{	
				this.lastScrollType = scrollType;				
				return false;
			}
			
			this.ResetScrollInfo();

			// SSP 3/24/05 BR02994
			// Moved this here from below.
			//
			Rectangle regionRect = new Rectangle(0,0,0,0);

			// Pass true for last param (bClipToDataArea)
			//		        
			this.GetRegionRect( ref regionRect, null, false, true);

			// JJD 12/26/01
			// Call Update again to force a draw of the possibly invalidated
			// scrollbars from the call to ResetScrollInfo above
			//
			// SSP 5/23/02
			// If we are currently updating (IsUpdating off the grid is true) then don't 
			// invalidate and/or update the control because we will invalidate the control
			// when EndUpdate is called.
			//
			//if ( null != this.Layout.Grid && this.Layout.Grid.Created )
			// SSP 11/24/03 UWG2057
			// Look above.
			//
			//if ( !isUpdating && null != this.Layout.Grid && this.Layout.Grid.Created )
			//	this.Layout.Grid.ControlForGridDisplay.Update();
			if ( shouldUpdate )
			{
				// SSP 3/24/05 BR02994
				// 
				// ------------------------------------------------------------
				//displayControl.Update( );
				int origPaintCount = this.Layout.DrawSequence;
				displayControl.Update( );
				if ( origPaintCount != this.Layout.DrawSequence )
				{
					Rectangle lastPaintRect = this.Layout.lastPaintRect;
					if ( lastPaintRect.Width >= regionRect.Width 
						&& lastPaintRect.Height >= regionRect.Height )
						shouldUpdate = false;
				}
				// ------------------------------------------------------------
			}
		    
			// SSP 11/24/03 UWG2057
			// Look above.
			//
			//if ( null != this.Layout.Grid && this.Layout.Grid.Created)			
			if ( null != displayControl && displayControl.Created )
			{
				System.Windows.Forms.Control control = this.Layout.Grid.ControlForGridDisplay;
				
                // SSP 3/24/05 BR02994
				// Moved this above.
				//
				

				// check to see if this is the last col scrolling region.
				// In that case we need to adjust the size of the scrollbar 
				// so it doesn't overlap the vertical scrollbar and invalidate
				// any row scrolling regions that don't have the scrollbar
				// 
				ColScrollRegionsCollection csrColl = this.Layout.ColScrollRegions;
				RowScrollRegionsCollection rsrColl = this.Layout.RowScrollRegions;
		    
				Debug.Assert( null != csrColl, "No col scrolling region collection in ColScrollRegion::OnScroll" );
				Debug.Assert( null != rsrColl, "No row scrolling region collection in ColScrollRegion::OnScroll" );

				if ( null != csrColl && null != rsrColl )
				{
					if ( this.IsLastVisibleRegion )
					{
						Rectangle rcVertScrollbar    = regionRect;
						rcVertScrollbar.X = regionRect.Right - SystemInformation.VerticalScrollBarWidth;

		                
						// iterate over the collection looking for the first 
						// non-locked region to return
						//
						foreach ( RowScrollRegion rsr in rsrColl )
						{
							if (rsr.WillScrollbarBeShown())
							{
								regionRect.Width = rcVertScrollbar.Left - regionRect.Left;
							}
							else
							{
								rcVertScrollbar.Y     = rsr.Origin;
								rcVertScrollbar.Height  = rsr.Extent;

								// JJD 10/02/01
								// Invalidate the ControlForGridDisplay instead to support
								// the UltraCombo
								//
								// this.Layout.Grid.Invalidate( rcVertScrollbar, false );
								// SSP 5/23/02
								// If we are currently updating (IsUpdating off the grid is true) then don't 
								// invalidate and/or update the control because we will invalidate the control
								// when EndUpdate is called.
								//
								if ( !isUpdating )
									control.Invalidate( rcVertScrollbar, false );
							}
						}		                
					}
				}

				short smoothScrollTime = 0;

				// JJD 11/02/01
				// Don't try to use smoothscrolling. There are problems with it
				//
				// if this is a first page operation (not a repeat page) 
				// then check the async key state to see if we 
				// should attempt a smooth scroll
				//
				
				Rectangle scrollRect	= regionRect;
				int scrollDelta			= oldScrollPosition - this.Position;

				if ( scrollDelta < 0 )
				{
					scrollRect.X     -= scrollDelta;
					scrollRect.Width += scrollDelta;
				}		            
				else
				{
					scrollRect.Width   -= scrollDelta;
				}
		            
				scrollRect = Rectangle.Intersect(scrollRect, regionRect);
				// make sure the scroll rect is not outside our region boundaries
				//	
				// JJD 11/05/01
				// Check new IsBackgroundScrollable property which returns false
				// if we are draing a backgroun image of doing a gradient fill
				// of the background
				//
				if ( 
					// SSP 3/11/05 BR02994
					// Added below condition.
					//
					shouldUpdate &&
					// SSP 5/19/03 - Fixed headers
					// When fixed headers are enabled, don't use the scroll window to scroll the grid.
					// 
					! this.Layout.UseFixedHeaders && 
					// SSP 7/21/03 UWG1997
					// Added UseScrollWindow property so that the user can prevent the grid from using 
					// ScrollWindow method for scrolling.
					// Added vertical parameter to specify the scroll direction.
					//
					//this.Layout.IsBackgroundScrollable &&
					this.Layout.IsBackgroundScrollable( false ) &&
					! scrollRect.IsEmpty &&
					! this.Layout.RowScrollRegions.AreCardsInView )
				{					
					// initialize both the clip and the exposed rect to the scroll
					// rect
					//
					Rectangle clipRect = scrollRect;
					Rectangle exposedRect = scrollRect;

					// offset the clip rect so that it reflects the destination 
					// of the scroll rect
					//		            
					clipRect.Offset(scrollDelta, 0);

					// depending on whether we are scrolling right or left
					// set the exposed rects top or bottom coord
					//
					if ( scrollDelta < 0 )
					{
						int temp1 = exposedRect.X;						
						exposedRect.X    = clipRect.Right;
						exposedRect.Width -= exposedRect.X - temp1;
					}		                
					else
					{
						exposedRect.Width   = clipRect.Left - exposedRect.Left;
					}
		                

					clipRect = Rectangle.Intersect(clipRect, regionRect);
					// make sure the clip and exposed rects are not outside our
					// region boundaries
					//					
					if ( ! clipRect.IsEmpty  )
					{
						exposedRect = Rectangle.Intersect(exposedRect, regionRect);						

						// AS 1/8/03 fxcop
						// Cannot have 2 public/protected members that differ only by case
						//this.cachedScrollOrigin -= scrollDelta;
						this.cachedScrollOriginValue -= scrollDelta;

						// now scroll the window
						//
						Rectangle updateRect = Rectangle.Empty;

						// SSP 5/23/02
						// If we are currently updating (IsUpdating off the grid is true) then don't 
						// invalidate and/or update the control because we will invalidate the control
						// when EndUpdate is called.
						//
						if ( !isUpdating )
						{
							// JJD 11/02/01
							// Added scrollwindow support
							//
							control.Update();

                            // JAS 10/11/04 - This informs the ComboDropDownControl, if there is one, that
                            // we are scrolling and that it should ignore any calls to Refresh().
                            if( this.Layout != null )
                                this.Layout.IsScrolling = true;

							if ( control is UltraGridBase )
							{
								// InternalScrollControl will work in situations where
								// DrawUtility.ScrollControl might not. For example, when
								// the Infragistics.Win assembly has unmanaged code rights
								// but the calling application doesn't
								//
								((UltraGridBase)control).InternalScrollControl( scrollDelta, 
									0,
									regionRect,
									clipRect,
									ref updateRect,
									false,
									true,
									true,
									smoothScrollTime );
							}
							else
							{
								DrawUtility.ScrollControl( control,
									scrollDelta, 
									0,
									regionRect,
									clipRect,
									ref updateRect,
									false,
									true,
									true,
									smoothScrollTime );
							}

                            // JAS 10/11/04
                            if( this.Layout != null )
                                this.Layout.IsScrolling = false;


                            // invalidate the exposed area
							//															
							// JJD 10/02/01
							// Invalidate the ControlForGridDisplay instead to support
							// the UltraCombo
							//
							//this.Layout.Grid.Invalidate( exposedRect, false );
							control.Invalidate( exposedRect, false );
						}
					}
					else
					{		                
						// AS 1/8/03 fxcop
						// Cannot have 2 public/protected members that differ only by case
						//this.cachedScrollOrigin = SCROLL_ORIGIN_BASE;
						this.cachedScrollOriginValue = SCROLL_ORIGIN_BASE;

						// the clip rect didn't intersect so invalidate the entire region
						//
						// JJD 10/02/01
						// Invalidate the ControlForGridDisplay instead to support
						// the UltraCombo
						//
						//this.Layout.Grid.Invalidate( regionRect, false );	
						// SSP 5/23/02
						// If we are currently updating (IsUpdating off the grid is true) then don't 
						// invalidate and/or update the control because we will invalidate the control
						// when EndUpdate is called.
						//
						if ( !isUpdating )
							control.Invalidate( regionRect, false );
					}
				}
				else
				{		            
					// AS 1/8/03 fxcop
					// Cannot have 2 public/protected members that differ only by case
					//this.cachedScrollOrigin = SCROLL_ORIGIN_BASE;
					this.cachedScrollOriginValue = SCROLL_ORIGIN_BASE;

					// the scroll rect didn't intersect so invalidate the entire region
					//
					// JJD 10/02/01
					// Invalidate the ControlForGridDisplay instead to support
					// the UltraCombo
					//
					//this.Layout.Grid.Invalidate( , false );
					// SSP 5/23/02
					// If we are currently updating (IsUpdating off the grid is true) then don't 
					// invalidate and/or update the control because we will invalidate the control
					// when EndUpdate is called.
					//
					if ( !isUpdating )
						control.Invalidate( regionRect, false );
				}								

				// JJD 11/06/01
				// Moved dirty element logic from inside scrollwindow if block
				// above since we always need to dirty the data area on a scroll
				//
				// Dirty the data area
				//
				UIElement element = this.Layout.UIElement.GetDescendant( typeof( DataAreaUIElement ) );

				if ( null != element )
					element.DirtyChildElements( false );

				// SSP 5/23/02
				// If we are currently updating (IsUpdating off the grid is true) then don't 
				// invalidate and/or update the control because we will invalidate the control
				// when EndUpdate is called.
				//
				if ( !isUpdating )
				{
					// Set flag so we dont try to check for last row in
					// RowScrollRegion.RegenerateVisibleRows
					//
					this.Layout.ScrollingColumns = true;
				
					control.Update();		        

					this.Layout.ScrollingColumns = false;
				}

				// If we are in edit mode then reposition the edit ctl
				//
				// SSP 4/29/02
				// EM Embeddable editors.
				//if ( null != this.Layout.EditControl && this.Layout.EditControl.Visible )
				UltraGridCell cellInEdit = this.Layout.CellInEditMode;
				if ( null != cellInEdit)
				{
					try
					{
						cellInEdit.RepositionTextBoxCtl();
					}
					catch( Exception )
					{
						Debug.Assert(false, "Failure in this.Layout.ActiveCell.RepositionEditCtl() call!" );
					}
				}
			}

			// keep track of the last scroll type
			//		    
			this.lastScrollType = scrollType;

			if ( null != grid )
			{
				// fire the after event
				//
				grid.FireEvent( GridEventIds.AfterColRegionScroll,  new ColScrollRegionEventArgs(this) );
			}

			return true;
		}

		internal void ScrollHeaders( ScrollEventType scrollType, int lineDelta,
									Rectangle scrollIntoViewRect, bool leftAlign,
			// SSP 6/10/03 - Fixed Column Feature
			// Added below two parameters to support fixed column situations.
			//
			bool takeIntoAccountFixedHeaders,
			HeaderBase headerBeingScrolled
			)
		{			
			if ( null == this.ClonedFrom )
			{				
				Debug.Assert(false, "ScrollPositionItems can only be called on a cloned ColScrollRegion");
				return;
			}

			
			if ( ! scrollIntoViewRect.IsEmpty )
			{			    
				Rectangle regionRect = new Rectangle(0,0,0,0);
				int deltaX = 0;

				// get the regions rect
				//
				this.GetRegionRect(ref regionRect, this.Layout.Grid.ActiveRowScrollRegion, !scrollIntoViewRect.IsEmpty, false);

				// SSP 6/10/03 - Fixed headers
				// Take into account the fixed headers.
				//
				// --------------------------------------------------------------------------------------------
				if ( takeIntoAccountFixedHeaders && null != headerBeingScrolled )
				{
					if ( headerBeingScrolled.Band.UseFixedHeadersResolved( this ) )
					{
						int fixedHeadersOffset = 0;

						if ( headerBeingScrolled.Band.Layout.ViewStyleImpl.HasMultiRowTiers )
						{
							// Horizontal view style

							if ( headerBeingScrolled.Band.IsHeaderFixed( this, headerBeingScrolled ) )
							{
								// In horizontal view style, fixed headers from descendant bands can be scrolled
								// because of the way fixed headers work in horizontal view style. In the first band
								// fixed headers are really fixed; meaning they can't be scrolled. However fixed
								// headers from descendant bands can be scrolled and need to be scrolled in order
								// to make them visible because of the multi-tier nature of the horizontal view
								// style.
								//
								Debug.Assert( null != headerBeingScrolled.Band,
									"In horizontal view style, fixed headers can only be scrolled if they are from a band other than the first band !" );

								// With non-fixed headers, we need to make sure that they don't get scrolled underneath
								// the fixed headers (which is what we are doing in the else block of this if statement.
								// We don't need to do that with the fixed headers because they don't get scrolled
								// underneath other fixed headers.
								//
								// DON'T NEED TO DO ANYTHING HERE
							}
							else
							{							
								UltraGridBand band = headerBeingScrolled.Band;
								while ( null != band )
								{
									fixedHeadersOffset += band.PreRowAreaExtent + band.RowSelectorExtent +
										band.GetFixedHeadersExtent( this );
									band = band.ParentBand;
								}
							}
						}
						else
						{
							// Vertical view style

							// In vertical view styles, fixed headers can't be scrolled because they are
							// fixed.
							//
							Debug.Assert( ! headerBeingScrolled.Band.IsHeaderFixed( this, headerBeingScrolled ),
								"In vertical view style, fixed headers can't be scrolled !" );

							fixedHeadersOffset = headerBeingScrolled.Band.GetOrigin( BandOrigin.RowCellArea ) 
								// MD 12/9/08 - 9.1 - Column Pinning Right
								// This should only use the left-fixed headers extent
								//+ headerBeingScrolled.Band.GetFixedHeadersExtent( this );
								+ headerBeingScrolled.Band.GetFixedHeadersExtent( this, false );
						}

						regionRect.X += fixedHeadersOffset;
						regionRect.Width -= fixedHeadersOffset;

						// MD 12/9/08 - 9.1 - Column Pinning Right
						// Remove the right-fixed headers extent from the scroll region
						regionRect.Width -= headerBeingScrolled.Band.GetFixedHeadersExtent( this, true );
					}
				}
				// --------------------------------------------------------------------------------------------

				int newRight = regionRect.Left + this.GetClientWidth( this.Layout.Grid.ActiveRowScrollRegion );

				if ( newRight < regionRect.Right )
					regionRect.Width = newRight - regionRect.Left;

				// calculate the amount to change the scroll position
				//
				if ( leftAlign )
				{
					deltaX = scrollIntoViewRect.Left - regionRect.Left;
				}
				else
				{
					// We are now adjusting for origin above
					if ( scrollIntoViewRect.Right > regionRect.Right)
						deltaX = scrollIntoViewRect.Right - regionRect.Right;

					if ( scrollIntoViewRect.Left < regionRect.Left + deltaX )
						deltaX = scrollIntoViewRect.Left - regionRect.Left;
				}

				this.Position = System.Math.Max( 0, this.Position + deltaX );

				// Make sure position doesn't exceed range
				//
				int range = this.Range;

				this.Position = System.Math.Min( this.Position, range );
			}
			else
			{
				// since the scrollbar's Value property already reflects
				// the scrolling change we can save it in our Position 
				// property
				//
				//this.Position = this.ScrollBarInfo.Value;
				
				int maxPosition = 1 + this.ScrollBarInfo.Maximum - this.ScrollBarInfo.LargeChange;

				//RobA 9/27/01 the ScrollBarInfo's value has not been set yet 
				//if the scrolltype is one of these types.  So we need to calculate 
				//the new position
				//
				switch ( scrollType )
				{
					case ScrollEventType.SmallDecrement:
						this.Position = Math.Max( 0, this.Position - this.ScrollBarInfo.SmallChange );
						break;
					case ScrollEventType.SmallIncrement:
						this.Position = Math.Min( maxPosition, this.Position + this.ScrollBarInfo.SmallChange );
						break;
					case ScrollEventType.LargeDecrement:
						this.Position = Math.Max( 0, this.Position - this.ScrollBarInfo.LargeChange );
						break;
					case ScrollEventType.LargeIncrement:
						this.Position = Math.Min( maxPosition, this.Position + this.ScrollBarInfo.LargeChange );
						break;
					case ScrollEventType.First:
						this.Position = 0;
						break;
					case ScrollEventType.Last:
						this.Position = maxPosition;
						break;
					case ScrollEventType.ThumbPosition:
					case ScrollEventType.ThumbTrack:
						this.Position = this.ScrollBarInfo.Value;
						break;
					case ScrollEventType.EndScroll:
						return;
				}
			}

			this.ResetScrollInfo();

			if ( this.ClonedFrom.Position == this.Position )
			{
				((ColScrollRegion)this.ClonedFrom).lastScrollType = scrollType;
			}

			// regenerate the elem header list based on the new scroll position
			//
			// JJD 11/02/01
			// set the destroyVisibleHeaders flag directly instead of calling
			// SetDestroyVisibleHeaders which will dirty the grid element and
			// invalidate the control
			//
			//this.SetDestroyVisibleHeaders( true );
			this.destroyVisibleHeaders = true;

			this.RegenerateVisibleHeaders();

			// get a ptr to the grid
			//			
			UltraGridBase grid = this.Layout.Grid;

			if ( null != grid )
			{
				int holdPosition = this.Position;

				// fire the before event
				//
				bool cancel;
				BeforeColRegionScrollEventArgs eventArgs = new BeforeColRegionScrollEventArgs(this, (ColScrollRegion)this.ClonedFrom);
				
				grid.FireBeforeColRegionScroll( eventArgs );
				
				cancel = eventArgs.Cancel;


				if ( ((ColScrollRegion)this.ClonedFrom).dirtyGridElement )
				{
					this.Layout.DirtyGridElement();
					this.dirtyGridElement = false;
				}


				if ( cancel )
					return;

				// Added flag to detect if the headers collection was regenerated
				// during a scrolling operation
				//
				if ( ((ColScrollRegion)this.ClonedFrom).regeneratedDuringScrolling)
				{
					// Dirty and regenerate the headers if the cloned from was
					// regenerated during the firing of the event above
					//					
					this.SetDestroyVisibleHeaders( true );
					this.RegenerateVisibleHeaders();					
				}
				else
					// If the postion was changed in the event above then regenerate the visible
					// headers
					//
					if ( holdPosition !=  this.Position )
				{

					// regenerate the elem header list based on the new scroll position
					//
					this.RegenerateVisibleHeaders ( );					
				}
			}

			// update the cloned from region's state 
			//
			// JJD 11/02/01
			// Set the lowercase position since setting the property dirties 
			// the grid again
			//
			this.ClonedFrom.position = this.Position;
			((ColScrollRegion)this.ClonedFrom).overallWidth = this.overallWidth;
			((ColScrollRegion)this.ClonedFrom).visibleHeaders = this.visibleHeaders;
		}


		private void GetRegionRect ( ref Rectangle regionRect ) 
		{
			this.GetRegionRect( ref regionRect, null, true, false );
		}
		private void GetRegionRect ( ref Rectangle regionRect, RowScrollRegion rsr ) 
		{
			this.GetRegionRect( ref regionRect, rsr, true, false );
		}
		private void GetRegionRect ( ref Rectangle regionRect, RowScrollRegion rsr, bool clipToParent ) 
		{
			this.GetRegionRect( ref regionRect, rsr, clipToParent, false );
		}
		private void GetRegionRect ( ref Rectangle region, RowScrollRegion rowRegion,
									bool clipToParent, bool clipToDataArea ) 
		{
			Rectangle dataAreaRect = new Rectangle(0,0,0,0);
			UIElement dataAreaElement = null;
			
			// Get the DataArea element rect (if exists)
			//
			UIElement element = this.Layout.UIElement;

			
			if ( null != element )
			{
				dataAreaElement = element.GetDescendant( typeof( Infragistics.Win.UltraWinGrid.DataAreaUIElement ) );
				
				if (null != dataAreaElement)
				{
					dataAreaRect = dataAreaElement.RectInsideBorders;
				}
			}


			// Instead of using the grid client rect for top and bottom use the new
			// uielement instead (this allows for other areas like the caption to be excluded.
			//
			if ( null != rowRegion )
			{
				region.Y = rowRegion.Origin;
				region.Height = rowRegion.Extent;
			}
			else
			{
				// Use UIElement to get the rect
				//
				if ( null != dataAreaElement )
				{
					region = dataAreaElement.Rect;
				}
				else
				{
					region.Y = this.Layout.RowScrollRegions.GetNextVisibleRegion(null, false).Origin;
					RowScrollRegion lastVisibleRowRegion = (RowScrollRegion)this.Layout.RowScrollRegions.LastVisibleRegion;
					region.Height = lastVisibleRowRegion.Origin + lastVisibleRowRegion.Extent - region.Y;
				}
			}

			region.X = this.Origin;
			region.Width = this.Extent;

			// If we have a DataArea element then intersect with it
			//
			if ( clipToDataArea && null != dataAreaElement ) 
			{
				region = Rectangle.Intersect( region, dataAreaRect );
			}

			if ( null != rowRegion && rowRegion.WillScrollbarBeShown() )
				region.Width -= SystemInformation.VerticalScrollBarWidth;

			if ( this.WillScrollbarBeShown() )			
				region.Height -= SystemInformation.HorizontalScrollBarHeight;
			
		        
			if ( clipToParent ) 
				this.Layout.ClipToParent(ref region, true);								
			
		}

		//RobA UWG173 8/15/01 method not implemented
		internal override void CheckIfSizeChanged()
		{						
			//Things that were commented out, had to to with freeze events in the
			//ActiveX version
			
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( grid == null )
				return;

			//	If we get in here when the AfterColRegionSize event is in progress,
			//	we are in a wacky state, probably because DoEvents was called during the event
			//	If this happens, set this flag, which will be evaluated at the end of this function to determine
			//	whether to dirty metrics so we can paint
			if ( grid.IsEventInProgress( GridEventIds.AfterColRegionSize ) )
			{
				this.eventInterrupted = true;
				return;
			}

			//if hidden don't fire events
			//
			if ( !grid.Visible || this.Hidden || grid.FirstDraw )
				return;
			
			// get the clipped extent (accounts for the situation
			// where the grid is smaller than the region extent )
			//
			int clippedExtent = this.ClippedExtent;

			if ( clippedExtent == this.extentLastResize )
			{		
				
				//If m_bFireAfterColRegionSize is true,
				//and it is OK to fire events, fire the
				//AfterColRegionSize event.
				//
				//if ( m_bFireAfterColRegionSize && pGrid->OkToFireEvents() )
				//{

					//Fire after event
					//
					//ColScrollRegionEventArgs e = new ColScrollRegionEventArgs( this );
					//grid.OnAfterColRegionSize( e );

					//m_bFireAfterColRegionSize = false;
				//}

				return;
			}
		
			// cache the clipped extent so we know when it changes
			//
			this.extentLastResize = clippedExtent;

			

			//Fire after event
			//
			ColScrollRegionEventArgs e = new ColScrollRegionEventArgs( this );
			grid.FireEvent( GridEventIds.AfterColRegionSize, e );
		
			
			if( this.eventInterrupted )
			{
				this.Layout.DirtyGridElement( false, true );				
			}

			
		}


		internal int GetBandOrigin ( UltraGridBand band )
		{
			// if this is not an exclusive region then just return
			// the bands origin
			//
			if ( !this.HasExclusiveItems )
				return band.GetOrigin();

			ViewStyleBase viewStyle = this.Layout.ViewStyleImpl;

			// for vertical displays the origin is always the same
			//
			if ( !viewStyle.HasMultiRowTiers )
				return band.GetOrigin();
			
			// We cache the origin we calculated the last time and if
			// the band is the same we use the cached origin
			// 
			// Otherwise, call GetBandExtent to recache the origin and
			// extent.
			//
			if ( this.lastBand != band )
				this.GetBandExtent ( band );

			// if there was a header from the band found then return its
			// cached origin
			//
			if ( this.originCached )
				return this.lastBandOrigin;

			// Otherwise, If there is a parent band return its 
			// origin plus extent
			//
			if ( band.ParentBand != null )
				return this.GetBandOrigin( band.ParentBand )
					+ this.GetBandExtent( band.ParentBand );

			return 0;

		}

		internal int GetBandExtent ( UltraGridBand band )
		{
			// if this is not an exclusive region then just return
			// the bands overall width
			//
			// SSP 6/13/05 BR04558
			// Since row-layout mode doesn't support exclusive items always return the band extent
			// for bands using the row-layout mode.
			// 
			//if ( !this.HasExclusiveItems )
			if ( !this.HasExclusiveItems || band.UseRowLayoutResolved )
				return band.GetExtent();

			// we cache the width we calculated the last time and if
			// the band is the same we use the cached width
			//
			if ( this.lastBand == band )
				return this.lastBandExtent;

			this.lastBand       = band;
			this.lastBandExtent = band.PreRowAreaExtent + band.RowSelectorExtent;
			this.lastBandOrigin = 0;

			this.originCached   = false;
            
			Infragistics.Win.UltraWinGrid.HeaderBase header;

			// loop over the exclusive items in our list accumulating
			// the width of all headers in the passed in band
			//
			for ( int i = 0; i < this.ExclusiveItems.Count; i ++ )
			{
				header = ExclusiveItems[i];

				// Skip over hidden position items
				//
				if ( header.Hidden )
					continue;

				// skip over position items from other bands
				//
				if ( header.Band != band )
					continue;

				// set the origin with the first header
				//
				if ( !this.originCached )
				{
					this.originCached   = true;

					// JJD 1/23/02 - UWG971
					// Subtract out the PreRowSelectorExtent since it was 
					// included above
					//
					this.lastBandOrigin = header.OverallOrigin - header.Band.PreRowAreaExtent;
				}

				// JJD 1/23/02 - UWG971
				// Subtract out the RowSelectorExtent (which is only
				// non-zero on the first item) since it was included above
				//
				this.lastBandExtent += header.Extent - header.RowSelectorExtent;
			}

			return this.lastBandExtent;
		}

		/// <summary>
		/// Splits a scrolling region into two scrolling regions. 
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to split one scrolling region into two scrolling regions. This method returns a ColScrollRegion object or a RowScrollRegion object that corresponds to the new scrolling region that is created by the split.</p>
		/// <p class="body">ColScrollRegions are split from right to left, with the new region created by the split appearing to the left of the existing region. RowScrollRegions are split from bottom to top, with the new region created by the split appearing above the existing region.</p>
		/// <p class="body">Specifying <i>width</i> when splitting a ColScrollRegion will set the width of the new region (leftmost of the two resulting ColScrollRegions.) Specifying <i>height</i> when splitting a RowScrollRegion will set the height of the new region (topmost of the two resulting RowScrollRegions.)</p>
		/// <p class="body">When a ColScrollRegion is split, the <b>BeforeColRegionSplit</b> and the <b>AfterColRegionSplit</b> events are generated. When a RowsScrollRegion is split, the <b>BeforeRowRegionSplit</b> and the <b>AfterRowRegionSplit</b> events are generated.</p>
		/// </remarks>
		public  ColScrollRegion Split()
		{
			return this.Split( 0 );
		}

		/// <summary>
		/// Splits a scrolling region into two scrolling regions. 
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to split one scrolling region into two scrolling regions. This method returns a ColScrollRegion object or a RowScrollRegion object that corresponds to the new scrolling region that is created by the split.</p>
		/// <p class="body">ColScrollRegions are split from right to left, with the new region created by the split appearing to the left of the existing region. RowScrollRegions are split from bottom to top, with the new region created by the split appearing above the existing region.</p>
		/// <p class="body">Specifying <i>width</i> when splitting a ColScrollRegion will set the width of the new region (leftmost of the two resulting ColScrollRegions.) Specifying <i>height</i> when splitting a RowScrollRegion will set the height of the new region (topmost of the two resulting RowScrollRegions.)</p>
		/// <p class="body">When a ColScrollRegion is split, the <b>BeforeColRegionSplit</b> and the <b>AfterColRegionSplit</b> events are generated. When a RowsScrollRegion is split, the <b>BeforeRowRegionSplit</b> and the <b>AfterRowRegionSplit</b> events are generated.</p>
		/// </remarks>
		/// <param name="extent">Specifying 0 for extent will split the specified region in half. </param>
		public  ColScrollRegion Split(int extent)
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_51"));

			//RobA UWG524 10/16/01 
			if ( this.SizingMode == SizingMode.Fixed )
			{
				// AS - 2/20/02
				// This is a public method that can be called from code. This should
				// not be displaying a messagebox. Instead, we should throw a
				// notsupportedexception. Of course, then we need to catch the exception 
				// in the oncolsplitboxdrop.
				//
				//System.Windows.Forms.MessageBox.Show( this.Layout.Grid, "Cannot split this region.", "Error");				
				//return null;
				throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_52") );
			}

			//RobA 10/22/01 implemented
			if ( this.ClonedFrom != null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_53"));
			
			// Throw an error if the extent is negative
			//
			if ( extent < 0 )
				throw new ArgumentOutOfRangeException(Shared.SR.GetString("LE_ArgumentOutOfRangeException_54") ) ;

			// Make sure we don't split the region if we have already reached the max
			//
			if ( this.Layout.MaxColScrollRegions > 0 &&
				this.Layout.ColScrollRegions.Count >= this.Layout.MaxColScrollRegions )
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_55") );

			// SSP 6/14/02 UWG1189
			// Before calling ClippedExtent, verify the grid's elements so
			// the Extent properties on the scroll regions get set to proper values.
			// This is necessary for example when the code to split the region
			// is put in the form's load event handler. At that point the grid
			// has not been drawn and thus the elements may not be verified.
			// So we want to force re-verification here. I didn't make this change
			// in the ClippedExtent or SafeExtent propeties which ClippedExtent calls
			// because reverifying everytime those properties are called (and they
			// are being called from quite a few places) may slow things down
			// considerably.
			//
			this.Layout.GetUIElement( true, true );

			// Use newer GetClippedExtent method
			//
			int currentExtent   = this.ClippedExtent;
			int splitExtent     = extent;

			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
			//int splitterBarWidth = this.Layout.SplitterBarWidth;
			int splitterBarWidth = this.Layout.ColScrollRegionSplitterBarWidthResolved;

			// Throw an error if the requested split is too great 
			//
			if ( extent > 0 && currentExtent < splitExtent + splitterBarWidth )
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_56") );

			if ( splitExtent <= 0 || currentExtent - splitExtent <= splitterBarWidth  )
			{
				int holdExtent = currentExtent;

				currentExtent  = ( currentExtent - splitterBarWidth ) / 2;
				splitExtent = holdExtent - ( currentExtent + splitterBarWidth );
			}
			else if ( splitExtent < currentExtent )
			{
				currentExtent -= splitExtent + splitterBarWidth;
			}
			else
			{
				// they specified a split greater than the size of the region
				// to split
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_56") );
			}

			// We do not allow zero-sized regions
			if ( currentExtent <= 0 || splitExtent <= 0 )
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_57") );

			int index = this.Index;

			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
			//ColScrollRegion csr = new ColScrollRegion( (ColScrollRegionsCollection)(this.collection) );
			ColScrollRegion csr = new ColScrollRegion( (ColScrollRegionsCollection)(this.collectionValue) );

			// set the origin and extent of the new region
			//
			csr.SetOriginAndExtent ( this.Origin, splitExtent );

			// fire the before event
			//
			if ( null != this.Layout.Grid )
			{
				BeforeColRegionSplitEventArgs args = new BeforeColRegionSplitEventArgs( this, csr );

				grid.FireEvent(GridEventIds.BeforeColRegionSplit, args );

				if ( args.Cancel )
					return null;
			}

			// the event wasn't cancelled so adjust the origin and extent
			// of original region and add the region to the collection
			//
			this.SetOriginAndExtent ( this.Origin + splitExtent + splitterBarWidth, currentExtent );

			this.Layout.ColScrollRegions.InternalInsert( csr, index );

			this.Layout.DirtyGridElement();

			this.NotifyPropChange( PropertyIds.Width );

			return csr;
		}

		internal int HorizontalAdjustment
		{
			get
			{
				return this.Origin - this.Position;
			}
		}

		// SSP 11/4/04 UWG3593
		// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
		// that specifies whether to assume and if so to whether to assume the scrollbar
		// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
		// even when they are not needed because we assume the horizontal scrollbar is 
		// visible to find out if vertical scrollbar should be displayed or not.
		//
		//internal override bool WillScrollbarBeShown ( bool assumeRowScrollbarsVisible )
		internal override bool WillScrollbarBeShown( ScrollbarVisibility assumeRowScrollbarsVisible )
		{
			// If we don't have any bands then we won't be showing the scrollbars
			//
			// JJD 12/20/01
			// If autoFitColumns is true or band 0 is in cardview then 
			// don't show a horizontal scrollbar. 
			//
			if ( 
				// SSP 3/3/05 BR02359
				// We need to show the scrollbars if min width contraints of columns cause them
				// to be displayed beyond the right edge of the grid so the user can scroll those
				// columns into view.
				//
				//this.Layout.AutoFitColumns ||
				 this.Layout.SortedBands.Count < 1 ||
				 this.Layout.SortedBands[0].CardView )
				return false;

			Infragistics.Win.UltraWinGrid.Scrollbar
				enumScrollbar =  this.ScrollbarResolved;

			bool show;

			switch( enumScrollbar )
			{
				case  Infragistics.Win.UltraWinGrid.Scrollbar.Show:
					show = true;
					break;

				case Infragistics.Win.UltraWinGrid.Scrollbar.ShowIfNeeded:
			{
					int rowScrollBarWidth  = SystemInformation.VerticalScrollBarWidth;
					int adjustment         = 0;
					int overallWidth;

					// get the overall width 
					//
					if ( this.ExclusiveItems.Count < 1 ) 
					{
						// JJD 12/27/01
						// Use the new GetOverallExtent method which can return
						// different extents for each region if card view bands
						// exist.
						//
						overallWidth = this.Layout.GetOverallExtent( this );
					}
					else
					{
						overallWidth = this.OverallWidth;
					}

					// SSP 11/4/04 UWG3593
					// If the overall width is greater than the extent then don't
					// even bother checking if the vertical scrollbars are visible.
					//
					//if ( overallWidth > this.Extent - rowScrollBarWidth && this.IsLastVisibleRegion )
					if ( overallWidth <= this.Extent && overallWidth > this.Extent - rowScrollBarWidth 
						&& this.IsLastVisibleRegion )
					{
						// Only take into account the width of the vertical scrollbar 
						// if there are any visible
						//
						// SSP 11/4/04 UWG3593
						// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
						// that specifies whether to assume and if so to whether to assume the scrollbar
						// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
						// even when they are not needed because we assume the horizontal scrollbar is 
						// visible to find out if vertical scrollbar should be displayed or not.
						//
						//if ( assumeRowScrollbarsVisible  || 
						//	this.Layout.RowScrollRegions.AreScrollbarsVisible(true))
						if ( ScrollbarVisibility.Visible == assumeRowScrollbarsVisible  || 
							ScrollbarVisibility.Check == assumeRowScrollbarsVisible 
							&& this.Layout.RowScrollRegions.AreScrollbarsVisible( ScrollbarVisibility.Hidden ) )
						{
							adjustment = rowScrollBarWidth;
						}
					}

					show = ( overallWidth > this.Extent
						- adjustment );
				}
					break;
				
				case Infragistics.Win.UltraWinGrid.Scrollbar.Hide:
					show = false;
					break;
				
				default:
					Debug.Assert(false, "Invalid enumScrollbar in ScrollRegionBase::WillScrollbarBeShown");
					show = true;
					break;
			}

			return show;            
		}
		internal override void PositionScrollbar( bool resetScrollInfo )
		{
			// AS - 12/14/01 UWG862
			// We do not want to update the scroll info, if we are in a drag situation.
			// Additional changes were made to the scrollbar info class and scrollbar
			// uielement to force a repaint before the scroll event is set.
			//
			// AS 1/8/03 - fxcop
			// Cannot have 2 public/protected members that differ only by case
			//protected int     cachedScrollOrigin;
			//if (this.scrollBarInfo != null && this.scrollBarInfo.IsThumbInDrag)
			if (this.scrollBarInfoValue != null && this.scrollBarInfoValue.IsThumbInDrag)
				return;

			// Exit if scrollbar rect hasn't been set yet
			//
			if ( !this.scrollbarRectSet )
				return;

			// If this is a clone then call this method on the
			// cloned from region
			//
			if ( this.IsClone )
			{
				this.ClonedFrom.PositionScrollbar( resetScrollInfo );
				return;
			}
            
			// reset the position dirty flag
			//
			this.scrollbarPositionDirty = false;

			// See if the scrollbar should be hidden or shown
			//
			// SSP 11/4/04 UWG3593
			// Don't assume the vertical scrollbars are visible.
			//
			//bool show = ( !this.Hidden && this.WillScrollbarBeShown ( true ) );
			bool show = ( !this.Hidden && this.WillScrollbarBeShown( ScrollbarVisibility.Check ) );

			

			this.ShowScrollbar( show, resetScrollInfo );
		}
		internal void AdjustWidthDuringColSynch( HeaderBase headerParam, int adjustment )
		{
			HeaderBase header;
			HeaderBase priorHeader = null;
			ViewStyleBase viewStyle = this.Layout.ViewStyleImpl;

			Debug.Assert ( null != viewStyle, "View style not found in ColScrollRegionAdjustWidthDuringColSynch" );

			// loop over the exclusive items in our list to recalc all
			// of the origins
			//
			for ( int i = 0; i < this.ExclusiveItems.Count; i++ )
			{
				header = this.ExclusiveItems[i];

				// Skip over hidden position items
				//
				if ( header.Hidden  )
					continue;

				viewStyle.AdjustExclusiveMetrics( this,	header,	priorHeader );
				priorHeader = header;
			}
		}

		
		internal VisibleHeader GetNearestSameBandHeader( Point point, GridItemBase lastGridItem )
		{
			Rectangle regionRect = new Rectangle(0,0,0,0);

			this.GetRegionRect( ref regionRect );

			UltraGridBand band = lastGridItem.Band;

			Type itemType = lastGridItem.GetType();

			HeaderBase			pivotHeader   = null;
			UltraGridColumn		pivotColumn   = null;
			UltraGridGroup		pivotGroup    = null;
			HeaderBase			lastHeader    = null;
			UltraGridColumn		lastColumn    = null;
			UltraGridGroup		lastGroup     = null;

			// try to get the privot header
			//
			if ( this.pivotItem != null )
			{
				if ( itemType == typeof(GroupHeader) && lastGridItem.GetType() == typeof(GroupHeader) )
				{
					lastGroup = ((GroupHeader)lastGridItem).Group;

					if ( ((GroupHeader)this.pivotItem).IsGroup )
						pivotGroup = ((GroupHeader)this.pivotItem).Group;
				}

				else if( itemType == typeof(ColumnHeader) && lastGridItem.GetType() == typeof(ColumnHeader) )
				{
					lastColumn = ((ColumnHeader)lastGridItem).Column;

					if ( !((ColumnHeader)this.pivotItem).IsGroup )
						pivotColumn = ((ColumnHeader)this.pivotItem).Column;
				}
				// SSP 4/16/05 - NAS 5.2 Filter Row
				// Added UltraGridFilterCell class.
				//
				//else if ( itemType == typeof(UltraGridCell))
				else if ( GridUtils.IsObjectOfType( itemType, typeof(UltraGridCell) ) )
				{
					lastColumn  = lastGridItem.GetColumn();
					pivotColumn = this.pivotItem.GetColumn();
				}
			}

			// if we hav columns checks if they are part of a group
			//
			// MD 1/21/09 - Groups in RowLayouts
			// Use the resolved group because in GroupLayout style, it will return the parent group of the layout item.
			//if ( pivotColumn != null )
			//    pivotGroup  = pivotColumn.Group;
			//
			//if ( lastColumn != null )
			//    lastGroup  = lastColumn.Group;
			if ( pivotColumn != null )
				pivotGroup = pivotColumn.GroupResolved;

			if ( lastColumn != null )
				lastGroup = lastColumn.GroupResolved;

			// use the group (if we have it) to get the pivot header or use
			// the column if we don't.
			//
			if ( pivotGroup != null )
				pivotHeader = pivotGroup.Header;
			else if ( pivotColumn != null )
				pivotHeader = pivotColumn.Header;

			// use the group (if we have it) to get the last header or use
			// the column if we don't.
			//
			if ( lastGroup != null )
				lastHeader = lastGroup.Header;
			else if ( lastColumn != null )
				lastHeader = lastColumn.Header;

			// make sure the pivot header is from the same band
			//
			if ( pivotHeader != null && pivotHeader.Band != lastGridItem.Band )
				pivotHeader = null;

			int pivotLeft     = 0;
			int pivotRight    = 0;
			
    
			if ( pivotHeader != null )
			{
				// calc the pivot header's left and right coordinates
				//
				pivotLeft = pivotHeader.OverallOrigin + this.Origin - this.Position;

				// SSP 6/20/03 - Fixed headers
				// If the header is a fixed header, then above calculation will have to be
				// offset to take into account the fact that the fixed headers do not scroll
				// the same way as non-fixed headers do.
				//
				// --------------------------------------------------------------------------
				if ( pivotHeader.Band.UseFixedHeadersResolved( this ) )
				{
					if ( pivotHeader.Band.IsHeaderFixed( this, pivotHeader ) )
					{
						pivotLeft += pivotHeader.Band.GetFixedHeaders_OriginDelta( this );
					}
					else
					{
						int fixedHeadersRight = this.Origin + band.GetLeftFixedHeadersAreaRight( this );
						if ( pivotLeft < fixedHeadersRight)
							pivotLeft = fixedHeadersRight;
					}
				}
				// --------------------------------------------------------------------------

				pivotRight = pivotLeft + pivotHeader.Extent;
			}

			
			VisibleHeader    lastGridItemElemHeader = null;
			VisibleHeader    nearestHeader          = null;
			int				 left = 0, right = 0;

			// JJD 1/12/02
			// Determine whether to loop thru the collection forward or backward
			// based on whether the point is to the left or right of the
			// pivot header
			//
			bool loopForwards = ( point.X <= pivotLeft) || pivotHeader == null;

			// SSP 7/10/03 - Fixed headers
			//
			bool isPivotHeaderFixedHeader = false;
			bool areNonFixedHeadersScrolledUnderneathFixedHeaders = false;
			int fixedHeadersAreaRight = 0;
			bool usingFixedHeaders = band.UseFixedHeadersResolved( this );
			VisibleHeader lastNonFixedSelectableHeader = null;
			if ( usingFixedHeaders )
			{
				HeaderBase dragSelectionPivotItemHeader = this.lastPivotItem as HeaderBase;
				if ( null == dragSelectionPivotItemHeader && null != this.pivotCell )
					dragSelectionPivotItemHeader = this.pivotCell.Column.Header;

				isPivotHeaderFixedHeader = null != dragSelectionPivotItemHeader ? band.IsHeaderFixed( this, dragSelectionPivotItemHeader ) : false;
				fixedHeadersAreaRight = this.Origin + band.GetLeftFixedHeadersAreaRight( this );
				areNonFixedHeadersScrolledUnderneathFixedHeaders = band.AreNonFixedHeadersScrolledUnderneathFixedHeaders( this );
			}

			// iterate over the header collection looking for the nearest item in the same band
			//
			for ( int index = loopForwards ? 0 : this.VisibleHeaders.Count - 1; 
					index >= 0 && index < this.VisibleHeaders.Count; 
					index += loopForwards ? 1 : -1 )
			{
				VisibleHeader visibleHeader = this.VisibleHeaders[index];

				// bypass hidden items
				//
				if ( visibleHeader.Header.Hidden )
					continue;

				// bypass headers from other bands
				//
				if ( visibleHeader.Header.Band != band )
					continue;

				left   = visibleHeader.Header.OverallOrigin + this.Origin - this.Position;

				right  = left + visibleHeader.Header.Extent;

				// SSP 7/10/03 - Fixed headers
				// If the header is a non-fixed header and is completely scrolled out of view 
				// underneath the fixed headers then don't return it.
				//
				// ------------------------------------------------------------------------------
				if ( usingFixedHeaders )
				{
					// If the header is a fixed header, then above calculation will have to be
					// offset to take into account the fact that the fixed headers do not scroll
					// the same way as non-fixed headers do.
					//
					bool isHeaderFixed = band.IsHeaderFixed( this, visibleHeader.Header );
					if ( isHeaderFixed )
					{
						int fixedHeaderDelta = band.GetFixedHeaders_OriginDelta( this );
						left += fixedHeaderDelta;
						right += fixedHeaderDelta;
					}

					// If the non-fixed header is scrolled underneath fixed headers, don't select it.
					//
					if ( ! isHeaderFixed && right < fixedHeadersAreaRight )
					{
						continue;
					}

					// If the pivot item is a non-fixed item and the mouse is over the fixed items (or to the left),
					// then make sure we select the first fully visible non-fixed item.
					//
					if ( ! isPivotHeaderFixedHeader && left >= fixedHeadersAreaRight &&
						point.X < fixedHeadersAreaRight )
					{
						if ( ! loopForwards || null == lastNonFixedSelectableHeader )
							lastNonFixedSelectableHeader = visibleHeader;
					}

					// If the pivot item is fixed and the mouse is past all the headers, then
					// make sure we return the last visible header.
					//
					if ( isPivotHeaderFixedHeader && left >= fixedHeadersAreaRight &&
						point.X > left && this.fixedToNonFixedAreaBoundaryCrossed )
					{
						if ( loopForwards || null == lastNonFixedSelectableHeader )
							lastNonFixedSelectableHeader = visibleHeader;
					}

					// If the user started drag selecting from a fixed header and is going into 
					// non-fixed area, then don't select any non-fixed headers until we've scrolled
					// the first non-fixed header into view (areNonFixedHeadersScrolledUnderneathFixedHeaders 
					// is for that).
					//
					if ( isPivotHeaderFixedHeader && ! isHeaderFixed 
						&& areNonFixedHeadersScrolledUnderneathFixedHeaders 
						&& ! this.fixedToNonFixedAreaBoundaryCrossed )
					{
						continue;
					}

					// If the drag-selection began in non-fixed area and the mouse is over a
					// fixed area, then don't select the fixed item until the first non-fixed
					// header is completely scrolled into view.
					//
					if ( isHeaderFixed && ! isPivotHeaderFixedHeader 
						 && areNonFixedHeadersScrolledUnderneathFixedHeaders )
					{
						continue;
					}

					if ( ! isHeaderFixed && left < fixedHeadersAreaRight )
					{
						left = fixedHeadersAreaRight;
					}
				}
				// ------------------------------------------------------------------------------

				if ( point.X >= left )
				{
					if ( point.X <= right )
						return visibleHeader;

					// JJd 1/12/02
					// If we are looping backwards and the left of this row
					// is to the right of the pivot header set the nearest 
					// header to this header
					//
					if ( pivotHeader!= null && !loopForwards )
					{
						if ( left    >= pivotRight )
						{
							nearestHeader = visibleHeader;
							break;
						}
					}
				}
				else
				{
					// JJd 1/12/02
					// Only set the nearest header if we are looping forwards and
					// the left is less than the pivot left
					//
					if ( pivotHeader != null && loopForwards )
					{
						if ( left <= pivotLeft  )
						{
							nearestHeader = visibleHeader;
							break;
						}
					}
				}

				// if we come across the last guy save his elem header ptr
				// so we can return it if all else fails
				//
				if ( lastHeader == visibleHeader.Header )
					lastGridItemElemHeader = visibleHeader;
			}

			// SSP 7/15/03 - Fixed headers
			//
			// ------------------------------------------------------------------
			if ( usingFixedHeaders && null == nearestHeader )
			{
				nearestHeader = lastNonFixedSelectableHeader;
			}
			// ------------------------------------------------------------------

			return nearestHeader != null ? nearestHeader : lastGridItemElemHeader;
		}

		#region InternalSetLastPivotItem
		
		// SSP 7/16/03 - Fixed headers
		// Added InternalSetLastPivotItem method.
		//
		internal void InternalSetLastPivotItem( GridItemBase pivotItem )
		{
			this.lastPivotItem = pivotItem;
			this.fixedToNonFixedAreaBoundaryCrossed = false;
		}

		#endregion // InternalSetLastPivotItem

		#region IsPivotItemFixedHeaderItem

		// SSP 7/15/03 - Fixed headers
		// added IsPivotItemFixedHeaderItem method.
		//
		internal bool IsPivotItemFixedHeaderItem( )
		{
			if ( this.lastPivotItem is HeaderBase || this.lastPivotItem is UltraGridCell )
			{
				HeaderBase header = this.lastPivotItem as HeaderBase;

				if ( null == header )
					header = ((UltraGridCell)this.lastPivotItem).Column.Header;

				return header.Band.UseFixedHeadersResolved( this ) && header.Band.IsHeaderFixed( this, header );
			}

			return false;
		}

		#endregion // IsPivotItemFixedHeaderItem

		#region FixedHeaders_DragSelectScrollHelper
		
		// SSP 7/15/03 - Fixed headers
		// Added FixedHeaders_DragSelectScrollHelper method.
		//
		internal bool FixedHeaders_DragSelectScrollHelper( 
			Point mouseLoc, out bool doesNeedScroll, ref int timerInterval, bool performScroll )
		{
			Rectangle regionRect = new Rectangle(0,0,0,0);

			// get the region's rect (intersected with the active row scroll region )
			//
			this.GetRegionRect ( ref regionRect, this.Layout.Grid.ActiveRowScrollRegion );

			doesNeedScroll = false;

			if ( this.lastPivotItem is HeaderBase || this.lastPivotItem is UltraGridCell )
			{
				HeaderBase header = this.lastPivotItem as HeaderBase;

				if ( null == header )
					header = ((UltraGridCell)this.lastPivotItem).Column.Header;

				// First see if the fixed headers are on.
				//
				if ( header.Band.UseFixedHeadersResolved( this ) )
				{
					int delta = 0;
					bool scrollAllTheWay = false;
					int fixedHeadersAreaRight = header.Band.GetLeftFixedHeadersAreaRight( this );
					int bandRight = regionRect.Left
						+ header.Band.GetOrigin( BandOrigin.PreRowArea ) 									
						- this.Position
						+ header.Band.GetFixedHeaders_OriginDelta( this )
						+ header.Band.GetExtent( BandOrigin.PreRowArea )
						- header.Band.GetFixedHeaders_ExtentDelta( this );

					bool nonFixedHeadersScrolled = header.Band.AreNonFixedHeadersScrolledUnderneathFixedHeaders( this );

					// We need to scroll differently when the user has drag selected from
					// a fixed headers area or a non-fixed heaers area.
					//
					if ( header.Band.IsHeaderFixed( this, header ) )
					{
						// NOTE: We shouldn't scroll if the user is drag selecting items from 
						// fixed area and is still in fixed area.
						
						if ( mouseLoc.X > regionRect.Left + fixedHeadersAreaRight )
						{
							// If the user is drag selecting from a fixed headers area to non-fixed
							// area, then scroll all the items to the right so there are no non-fixed
							// items scrolled underneath the fixed items. This is what excel does when
							// you drag select cells from fixed area to non-fixed area.
							//
							if ( mouseLoc.X <= regionRect.Right && nonFixedHeadersScrolled )
							{
								// We want to scroll all the way only once during a single drag select
								// session. So check for the fixedToNonFixedAreaBoundaryCrossed flag.
								//
								if ( ! this.fixedToNonFixedAreaBoundaryCrossed )
								{
									delta = -25;
									scrollAllTheWay = true;									
								}
							}
							else if ( mouseLoc.X > regionRect.Right && bandRight > regionRect.Right )
							{
								delta = mouseLoc.X - regionRect.Right;
							}

							// Since we've crossed the fixed to non-fixed area boundary set the
							// below flag.
							//
							if ( performScroll )
								this.fixedToNonFixedAreaBoundaryCrossed = true;
						}
						else if ( this.fixedToNonFixedAreaBoundaryCrossed
							&& nonFixedHeadersScrolled 
							&& mouseLoc.X < regionRect.X + fixedHeadersAreaRight )
						{
							delta = mouseLoc.X - ( regionRect.X + fixedHeadersAreaRight );
						}
					}
					else
					{
						// If the user is drag selecting from non-fixed area and is right of the
						// scroll region, then scroll the headers left.
						//
						if ( mouseLoc.X > regionRect.Right && bandRight > regionRect.Right )
						{
							delta = mouseLoc.X - regionRect.Right;
						}
							// If the user is drag selecting from non-fixed area and the mouse is
							// left of the fixed area, then scroll.
							//
						else if ( nonFixedHeadersScrolled 
							&& mouseLoc.X < regionRect.X + fixedHeadersAreaRight )
						{
							delta = mouseLoc.X - ( regionRect.X + fixedHeadersAreaRight );
						}
					}

					if ( 0 != delta )
					{
						doesNeedScroll = true;

						// return faster timer intervals for longer distances
						//
						int distanceOutsideRgn = Math.Abs( delta );
						if ( scrollAllTheWay )
							timerInterval = 1;
						else if ( distanceOutsideRgn > 100 )
							timerInterval =    40;
						else if ( distanceOutsideRgn > 80 )
							timerInterval =    65;
						else if ( distanceOutsideRgn > 60 )
							timerInterval =    80;
						else if ( distanceOutsideRgn > 50 )
							timerInterval =    100;
						else if ( distanceOutsideRgn > 40 )
							timerInterval =    150;
						else if ( distanceOutsideRgn > 20 )
							timerInterval =    200;
						else if ( distanceOutsideRgn > 10 )
							timerInterval =    300;
						else if ( distanceOutsideRgn > 6 )
							timerInterval =    400;
						else 
							timerInterval =    500;

						if ( performScroll )
						{
							int scrollValue = this.ScrollBarInfo.Value;
							int origScrollValue = scrollValue;
							if ( scrollAllTheWay )
							{
								if ( ! this.Layout.ViewStyleImpl.HasMultiRowTiers )
								{
									// Vertical view style and derivatives

									scrollValue = 0;
								}
								else
								{
									// Horizontal view style

									if ( null == header.Band.ParentBand )
									{
										// For the topmost band, in order to ensure that the first non-fixed header
										// gets scrolled into view, we have to set the scroll bar position to 0.
										//
										scrollValue = 0;
									}
									else
									{
										// For non-zero bands, we shouldn't scroll the headers in the parent band
										// when drag selecting cells in the band.
										//
										int scrollDelta = this.Position, tmpOrigin, tmpExtent;
										UltraGridBand.GetBandOriginExtent_HorizontalViewStyle(
											header.Band.ParentBand, this, ref scrollDelta, out tmpOrigin, out tmpExtent );

										scrollValue = this.Position - scrollDelta;
									}
								}
							}
							else
							{
								int sign = delta > 0 ? 1 : -1;

								// Vary delta according to the timer interval.
								//
								int distanceToScroll;
								if ( timerInterval > 400 )
									distanceToScroll = COL_LINE_SCROLL_DELTA;
								else if ( timerInterval > 300 )
									distanceToScroll = COL_LINE_SCROLL_DELTA * 2;
								else if ( timerInterval > 200 )
									distanceToScroll = COL_LINE_SCROLL_DELTA * 3;
								else 
									distanceToScroll = COL_LINE_SCROLL_DELTA * 4;
								
								scrollValue += sign * distanceToScroll;
								scrollValue = Math.Max( this.ScrollBarInfo.Minimum, Math.Min( this.ScrollBarInfo.Maximum - 1, scrollValue ) );
							}
							
							this.ScrollBarInfo.Value = scrollValue;
							this.InternalScroll( ScrollEventType.ThumbTrack, scrollValue - origScrollValue );
						}
					}

					return true;
				}
			}

			return false;
		}

		#endregion // FixedHeaders_DragSelectScrollHelper

		internal bool DoesDragNeedScroll( Point mouseLoc, ref int timerInterval )
		{
			// SSP 6/22/05 - NAS 5.3 Column Chooser
			// Don't scroll the grid if the mouse is over the column chooser.
			// 
			UltraGridBase gridBase = this.Layout.Grid;
			if ( null != gridBase && ! gridBase.ShouldScrollGridOnDragMoveHelper( mouseLoc ) )
				return false;

			// SSP 7/10/03 - Fixed headers
			//
			// --------------------------------------------------------------------------
			bool doesNeedScroll;
			// Since rows cannot be selected with headers and cells, to find out if rows
			// are being selected, just check if there are any selected rows.
			//
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			bool isSelectingRows = null != grid && grid.HasSelectedBeenAllocated && grid.Selected.HasRows;
			if ( ! isSelectingRows && this.FixedHeaders_DragSelectScrollHelper( mouseLoc, out doesNeedScroll, ref timerInterval, false ) )
				return doesNeedScroll;
			// --------------------------------------------------------------------------

			Rectangle regionRect = new Rectangle(0,0,0,0);

			// get the region's rect (intersected with the active row scroll region )
			//
			this.GetRegionRect ( ref regionRect, this.Layout.Grid.ActiveRowScrollRegion );

			int distanceOutsideRgn = 0;

			// if the x coordinate of the cursor falls in the region then
			// return false
			//
			if ( mouseLoc.X >= regionRect.Left  && mouseLoc.X <= regionRect.Right )
				return false;

			// calculate the distance from the region's borders to where
			// the cursor is
			//
			if ( mouseLoc.X < regionRect.Left )
				distanceOutsideRgn = regionRect.Left - mouseLoc.X;
			else
				distanceOutsideRgn = mouseLoc.X - regionRect.Right;

			// return faster timer intervals for longer distances
			//
			if ( distanceOutsideRgn > 100 )
				timerInterval =    40;
			else
				if ( distanceOutsideRgn > 80 )
				timerInterval =    65;
			else
				if ( distanceOutsideRgn > 60 )
				timerInterval =    80;
			else
				if ( distanceOutsideRgn > 50 )
				timerInterval =    100;
			else
				if ( distanceOutsideRgn > 40 )
				timerInterval =    150;
			else
				if ( distanceOutsideRgn > 20 )
				timerInterval =    200;
			else
				if ( distanceOutsideRgn > 10 )
				timerInterval =    300;
			else
				if ( distanceOutsideRgn > 6 )
				timerInterval =    400;
			else
				timerInterval =    500;

			return true;
		}


		internal void DoDragScroll(	Point mouseLoc, int timerInterval )
		{
			// SSP 7/10/03 - Fixed headers
			//
			// --------------------------------------------------------------------------
			bool doesNeedScroll;
			// Since rows cannot be selected with headers and cells, to find out if rows
			// are being selected, just check if there are any selected rows.
			//
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			bool isSelectingRows = null != grid && grid.HasSelectedBeenAllocated && grid.Selected.HasRows;
			if ( ! isSelectingRows && this.FixedHeaders_DragSelectScrollHelper( mouseLoc, out doesNeedScroll, ref timerInterval, true ) )
				return;
			// --------------------------------------------------------------------------

			Rectangle regionRect = new Rectangle(0,0,0,0);

			// get the region's rect (intersected with the active row scroll region )
			//
			this.GetRegionRect ( ref regionRect, this.Layout.Grid.ActiveRowScrollRegion );

			ScrollEventType e = new ScrollEventType();

			// determine if we need to scroll up or down
			//
			if ( mouseLoc.X < regionRect.Left )
				e = ScrollEventType.SmallDecrement;
			else if ( mouseLoc.X > regionRect.Right )
				e = ScrollEventType.SmallIncrement;
			else
				return;

			int nDelta;

			// use the time interval to determoine the scroll delta, slower
			// timer intervals mean we want to scroll less
			//
			if ( timerInterval > 400 )
				nDelta = COL_LINE_SCROLL_DELTA;
			else
				if ( timerInterval > 300 )
				nDelta = COL_LINE_SCROLL_DELTA * 3;
			else
				if ( timerInterval > 200 )
				nDelta = COL_LINE_SCROLL_DELTA * 5;
			else
				nDelta = COL_LINE_SCROLL_DELTA * 8;

			// call InternalScroll to scroll the cols
			//
			int ScrollValue = this.ScrollBarInfo.Value;
			ScrollValue += (e == ScrollEventType.SmallIncrement) ? nDelta : -nDelta;
			
			// 8/28/01 UWG 258
			// This is going to throw an exception if the ScrollValue does not fall in the
			// range of ScrollBarInfo.Minumum and ScrollBarControl.Maximum
			//this.ScrollBarInfo.Value = ScrollValue >= 0 ? ScrollValue : 0;
			this.ScrollBarInfo.Value = Math.Max( this.ScrollBarInfo.Minimum, Math.Min( this.ScrollBarInfo.Maximum - 1, ScrollValue >= 0 ? ScrollValue : 0 ) );
			
			this.InternalScroll(e, nDelta);
		}

		#region type converter

		/// <summary>
		/// ColScrollRegion type converter.
		/// </summary>
		public sealed class ColScrollRegionTypeConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if (destinationType == typeof(InstanceDescriptor)) 
				{
					return true;
				}
				
				return base.CanConvertTo(context, destinationType);
			}
                       
 			/// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
			/// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
 			public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) 
			{
				if (destinationType == null) 
				{
					throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_20"));
				}

				if ( destinationType == typeof(InstanceDescriptor) && 
					 value is ColScrollRegion ) 
				{
					ColScrollRegion item = (ColScrollRegion)value;
					
					ConstructorInfo ctor;

					ctor = typeof(ColScrollRegion).GetConstructor(new Type[] { typeof( int ) } );

					if (ctor != null) 
					{
						//false as the last parameter here causes generation of a local variable for the type 
						return new InstanceDescriptor(ctor, new object[] { item.Extent }, false);
					}
				}

				return base.ConvertTo(context, culture, value, destinationType);
			}
		}
		#endregion
	}
}
