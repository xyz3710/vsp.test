#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Diagnostics;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	internal class GroupByBoxPromptProvider : TextProviderBase
	{
		GroupByBoxUIElement groupByBoxElement = null;

		internal GroupByBoxPromptProvider( GroupByBoxUIElement groupByBoxElement ): base( groupByBoxElement )
		{
			this.groupByBoxElement = groupByBoxElement;
		}

		private GroupByBox GroupByBox
		{
			get
			{
				return null != this.groupByBoxElement ? this.groupByBoxElement.GroupByBox : null;
			}
		}

		public override String GetText( DependentTextUIElement element )
		{
			return null != this.GroupByBox ? this.GroupByBox.Prompt : string.Empty;
		}
	}


	/// <summary>
	///		Summary description for GroupByBoxUIElement.
	/// </summary>
	public class GroupByBoxUIElement : UIElement
	{
		#region Member Variables

		// AS 1/6/04 accessibility
		private AccessibleObject		accessibleObject;

		private GroupByBoxPromptProvider promptProvider = null;
		
		#endregion //Member Variables
				
		internal GroupByBoxUIElement( UIElement parent ) : base( parent )
		{
		}

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b></b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="groupByBox">Associated <b>GroupByBox</b></param>
		public GroupByBoxUIElement( UIElement parent, GroupByBox groupByBox ) : this( parent )
		{
			this.InitializeGroupByBox(groupByBox);
		}


		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type,object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
		/// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
        protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( typeof( DataAreaUIElement ) == type ||
				 typeof( AddNewBoxUIElement ) == type )
				return false;

			if ( null != contexts )
			{
				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						if ( context is UltraGridRow || 
							context is UltraGridCell ||
							context is RowScrollRegion ||
							context is ColScrollRegion ||
							context is AddNewBox )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch

		private TextProviderBase PromptProvider
		{
			get
			{
				if ( null == this.promptProvider )
                    this.promptProvider = new GroupByBoxPromptProvider( this );

				return this.promptProvider;
			}
		}		

		/// <summary>
		/// <see cref="Infragistics.Win.UltraWinGrid.GroupByBox"/>
		/// </summary>
		public Infragistics.Win.UltraWinGrid.GroupByBox GroupByBox
		{
			get
			{
				return (GroupByBox)this.PrimaryContext;
			}
		}

		internal void InitializeGroupByBox( Infragistics.Win.UltraWinGrid.GroupByBox groupByBox )
		{
			this.PrimaryContext = groupByBox;
		}


		// AS - 10/31/01
		// There were 2 virtual implementations of GetContext in our framework
		// and it only made sense to have one virtual since the non-virtual one
		// simply called off to the virtual one.
		//

//		// SSP 10/31/01 
//		// Overrode this method
//		//
//		/// <summary>
//		/// Returns an object of requested type that relates to the
//		/// element or null.
//		/// </summary>
//		/// <param name="type"></param>
//		/// <param name="checkParentElements"></param>
//		/// <returns></returns>
//		public override object GetContext( Type type, bool checkParentElements )
//		{			
//			object o = this.GetContext( type );
//
//			if ( null != o )
//				return o;
//
//			return base.GetContext( type, checkParentElements );
//		}


        /// <summary>
        /// Returns an object of requested type that relates to the element or null.
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="checkParentElements">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
        public override object GetContext(Type type, bool checkParentElements)
		{			
			if ( typeof( Infragistics.Win.UltraWinGrid.GroupByBox ) == type )
				return this.PrimaryContext;

			// SSP 9/7/01
			if ( typeof(Infragistics.Win.UltraWinGrid.SpecialBoxBase) == type )
				return this.PrimaryContext;

			// Call the base class implementation
			//			
			return base.GetContext( type, checkParentElements );		
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{				
				return null != this.GroupByBox && 
					UIElementBorderStyle.Default != this.GroupByBox.BorderStyle ?
				      this.GroupByBox.BorderStyle :
					    UIElementBorderStyle.None; // Default for GroupByBox is None
			}
		}


        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			this.GroupByBox.ResolveAppearance( ref appearance, requestedProps );			
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			UIElementsCollection oldElements = this.childElementsCollection;

			this.childElementsCollection = null;

			Infragistics.Win.UltraWinGrid.GroupByBox groupByBox = this.GroupByBox;

			Size promptSize = groupByBox.PromptSize;
			//promptSize.Width = Math.Min( this.Rect.Width, 8 + promptSize.Width );

			GroupByBox.ButtonDefinition[] buttons = groupByBox.GetButtons( );


			Rectangle captionRect = Rectangle.Empty;

			if ( buttons.Length <= 0 )
			{
				GroupByBoxPromptUIElement captionElement = (GroupByBoxPromptUIElement)GroupByBoxUIElement.ExtractExistingElement( 
					oldElements, typeof( GroupByBoxPromptUIElement ), true );

				if ( null == captionElement )			
					captionElement = new GroupByBoxPromptUIElement( this, this.PromptProvider );

				captionElement.InitProvider( this.PromptProvider );

				Rectangle rectInsideBorders = this.RectInsideBorders;

				captionRect = new Rectangle( rectInsideBorders.Left + GroupByBox.BTN_BAND_HEIGHT_OFFSET,
					rectInsideBorders.Top, promptSize.Width, rectInsideBorders.Height );

				captionRect.Inflate( 0, -GroupByBox.BTN_BAND_HEIGHT_OFFSET );

				// SSP 12/2/04 BR00104
				// Honor TextHAlign and TextVAlign if they are non default. Position the prompt 
				// element within the group box according to these settings.
				//
				// --------------------------------------------------------------------------------------
				if ( groupByBox.HasPromptAppearance )
				{
					HAlign hAlign = groupByBox.PromptAppearance.TextHAlign;
					if ( HAlign.Default != hAlign )
						DrawUtility.AdjustHAlign( hAlign, ref captionRect, 
							Rectangle.Inflate( rectInsideBorders, -GroupByBox.BTN_BAND_HEIGHT_OFFSET, 0 ) );

					VAlign vAlign = groupByBox.PromptAppearance.TextVAlign;
					if ( VAlign.Default != vAlign )
						DrawUtility.AdjustVAlign( vAlign, ref captionRect, rectInsideBorders );
				}
				// --------------------------------------------------------------------------------------

				captionElement.Rect = captionRect;

				this.ChildElements.Add( captionElement );
			}
			else
			{
				captionRect = this.Rect;					
				captionRect.Width = 0;
				captionRect.Height = GroupByBox.BTN_BAND_HEIGHT_OFFSET - 2;
			}

			Rectangle lastButtonRect = new Rectangle(
				this.Rect.Left + GroupByBox.BTN_BAND_HEIGHT_OFFSET, 
				captionRect.Bottom + 2, 
				2 + promptSize.Width, 
				2 + promptSize.Height );
			lastButtonRect.Width = 0;

			// SSP 7/9/02
			// Only do so for full group by box style.
			//
			if ( GroupByBoxStyle.Full == groupByBox.Style )
				lastButtonRect.Y -= lastButtonRect.Height / 2;

			UIElementsCollection buttonElems = new UIElementsCollection( buttons.Length );

			if ( null != buttons && buttons.Length > 0 )
			{
				for ( int i = 0; i < buttons.Length; i++ )
				{
					Debug.Assert( null != buttons[i].Caption, "An invalid item enocuntered in buttons" );

					if ( null == buttons[i].Caption )
						continue;

					
					UIElement buttonElement = null;

					if ( null != buttons[i].column )
					{
						buttonElement = (UIElement)GroupByBoxUIElement.ExtractExistingElement( oldElements, typeof( GroupByButtonUIElement ), true );

						if ( null == buttonElement )						
							buttonElement = new GroupByButtonUIElement( this, buttons[i] );

						((GroupByButtonUIElement)buttonElement).Initialize( buttons[i] );
					}
					else
					{
						buttonElement = (UIElement)GroupByBoxUIElement.ExtractExistingElement( oldElements, typeof( GroupByBandLabelUIElement ), true );
						
						if ( null == buttonElement )						
							buttonElement = new GroupByBandLabelUIElement( this, buttons[i] );					

						((GroupByBandLabelUIElement)buttonElement).init( buttons[i] );
					}

					Size buttonSize = groupByBox.GetButtonSize( buttons[i] );

					
					
					Rectangle buttonRect = new Rectangle( 
						lastButtonRect.Right + GroupByBox.BTN_MIN_SPACING_HORZ,
						// SSP 7/9/02
						// Group by box compact style.
						//
						//lastButtonRect.Top + lastButtonRect.Height / 2,
						lastButtonRect.Top + ( GroupByBoxStyle.Full == groupByBox.Style ? lastButtonRect.Height / 2 : 0 ),
						//buttonWidth, buttonHeight );
						buttonSize.Width, buttonSize.Height );

					
					
					// skipp button connector for the first button
					if ( buttonElems.Count > 0 )
					{
						Rectangle connectorRect = Rectangle.Empty;

						if ( GroupByBoxStyle.Full == groupByBox.Style )
						{
							connectorRect.Y		    = lastButtonRect.Bottom;
							connectorRect.X		    = lastButtonRect.Right - GroupByBox.BTN_CONNECTOR_LEFT_INDENT;
							connectorRect.Width	    = buttonRect.Left - connectorRect.Left;
							connectorRect.Height    = (buttonRect.Bottom - GroupByBox.BTN_CONNECTOR_BOTTOM_INDENT) - connectorRect.Top;
						}
						else
						{
							// SSP 7/9/02
							// Group by box compact style.
							//
							connectorRect.Y		    = ( lastButtonRect.Top + lastButtonRect.Bottom ) / 2;
							connectorRect.X		    = lastButtonRect.Right;
							connectorRect.Width	    = buttonRect.Left - connectorRect.Left;

							UIElementBorderStyle connectorBorderStyle = groupByBox.ButtonConnectorStyle;

							if ( UIElementBorderStyle.Default == connectorBorderStyle )
								connectorBorderStyle = UIElementBorderStyle.Solid;

							Debug.Assert( null != groupByBox.Layout, "No layout off the group by box !" );
							if ( null != groupByBox.Layout )
								connectorRect.Height    = groupByBox.Layout.GetBorderThickness( connectorBorderStyle );
							else
								connectorRect.Height = 2;
						}

						ButtonConnectorUIElement btnConnElem = new ButtonConnectorUIElement( this );

						btnConnElem.Rect = connectorRect;

						this.ChildElements.Add( btnConnElem );
					}

					buttonElement.Rect = buttonRect;
					buttonElems.Add( buttonElement );

					lastButtonRect = buttonRect;
				}
			}		
		
			
			// doesn't look like they need to be in reverse order after all
			for ( int i = 0; i <  buttonElems.Count; i++ )
			{
				this.ChildElements.Add( buttonElems[i] );
			}

			if ( null != oldElements )
				oldElements.Clear( );
            oldElements = null;			
		}	


		/// <summary>
		/// Overridden to return true indicating that the child elements
		/// are to be clipped. This is baecause group by buttons and band
		/// labels could be hidden if more buttons are added than can be
		/// displayed in the group by box
		/// </summary>
		protected override bool ClipChildren
		{
			get
			{
				return true;
			}
		}

		// AS 1/6/04 Accessibility
		#region IsAccessibleElement
		/// <summary>
		/// Indicates if the element supports accessibility.
		/// </summary>
		public override bool IsAccessibleElement
		{
			get { return true; }
		}
		#endregion //IsAccessibleElement

		#region AccessibilityInstance
		/// <summary>
		/// Returns the accessible object associated with the element.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note</b> Derived elements that plan to return an accessible object must override 
		/// the <see cref="IsAccessibleElement"/> member.</p>
		/// </remarks>
		public override AccessibleObject AccessibilityInstance
		{
			get 
			{ 
				if (this.accessibleObject == null)
					this.accessibleObject = this.GroupByBox.Layout.Grid.CreateAccessibilityInstance(this);

				return this.accessibleObject; 
			}
		}
		#endregion //AccessibilityInstance

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.GroupByBox.Layout, StyleUtils.Role.GroupByBox );
			}
		}

		#endregion // UIRole
	}	





	/// <summary>
	/// Used for showing prompt in the group by box ui element
	/// </summary>
	public class GroupByBoxPromptUIElement : UIElement 
	{
		#region Member Variables

		// AS 1/7/04 accessibility
		private AccessibleObject		accessibleObject;

		private IUIElementTextProvider promptProvider = null;

		#endregion //Member Variables

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		/// <param name="promptProvider"></param>
		internal GroupByBoxPromptUIElement( GroupByBoxUIElement parent, IUIElementTextProvider promptProvider )
			: base( parent )
		{
			this.promptProvider = promptProvider;
		}

		internal void InitProvider( IUIElementTextProvider promptProvider )
		{
			this.promptProvider = promptProvider;

			if ( null != this.childElementsCollection && this.childElementsCollection.Count > 0  &&
				this.childElementsCollection[0] is DependentTextUIElement )
			{
				((DependentTextUIElement)this.childElementsCollection[0]).InitProvider( promptProvider );
			}
		}


		/// <summary>
		/// Returns the owning GroupByBox
		/// </summary>
		public Infragistics.Win.UltraWinGrid.GroupByBox GroupByBox
		{
			get
			{
				return (GroupByBox)this.Parent.GetContext( typeof( GroupByBox ), true );
			}
		}


        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{				
				// SSP 3/28/06 - App Styling
				// 
				
				//return UIElementBorderStyle.None;

				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this.GroupByBox.Layout, StyleUtils.CachedProperty.BorderStyleGroupByBoxPrompt, out val ) )
				{
					val = StyleUtils.CacheBorderStylePropertyValue( this.GroupByBox.Layout,
						StyleUtils.CachedProperty.BorderStyleGroupByBoxPrompt,
						StyleUtils.Role.GroupByBoxPrompt, 
						UIElementBorderStyle.Default,
						UIElementBorderStyle.None );
				}

				return (UIElementBorderStyle)val;
				
			}
		}


        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// SSP 1/3/02 UWG706
			// Use the newly added this method to resolve the prompt appearance.
			//
			//this.GroupByBox.ResolveAppearance( ref appearance, AppearancePropFlags.FontData );
			this.GroupByBox.ResolvePromptAppearance( ref appearance, requestedProps );
		}



		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			UIElementsCollection oldElements = this.childElementsCollection;

			this.childElementsCollection = null;

			DependentTextUIElement captionElement = (DependentTextUIElement)GroupByBoxUIElement.ExtractExistingElement( 
				oldElements, typeof( DependentTextUIElement ), true );

			if ( null == captionElement )			
				captionElement = new DependentTextUIElement( this, this.promptProvider );

			captionElement.InitProvider( this.promptProvider );

			Rectangle rectInsideBorders = this.RectInsideBorders;

			captionElement.Rect = rectInsideBorders;

			this.ChildElements.Add( captionElement );
		}

		// AS 1/7/04 Accessibility
		#region IsAccessibleElement
		/// <summary>
		/// Indicates if the element supports accessibility.
		/// </summary>
		public override bool IsAccessibleElement
		{
			get { return true; }
		}
		#endregion //IsAccessibleElement

		#region AccessibilityInstance
		/// <summary>
		/// Returns the accessible object associated with the element.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note</b> Derived elements that plan to return an accessible object must override 
		/// the <see cref="IsAccessibleElement"/> member.</p>
		/// </remarks>
		public override AccessibleObject AccessibilityInstance
		{
			get 
			{ 
				if (this.accessibleObject == null)
					this.accessibleObject = this.GroupByBox.Layout.Grid.CreateAccessibilityInstance(this);

				return this.accessibleObject; 
			}
		}
		#endregion //AccessibilityInstance

		#region GroupByBandlLabelAccessibleObject
		/// <summary>
		/// Accessible object representing a <see cref="GroupByBoxPromptUIElement"/> in the <see cref="GroupByBox"/>
		/// </summary>
		public class GroupByPromptAccessibleObject : UIElementAccessibleObject
		{
			#region Constructor
			/// <summary>
			/// Initializes a new <see cref="GroupByPromptAccessibleObject"/>
			/// </summary>
			/// <param name="element">Associated element</param>
			public GroupByPromptAccessibleObject(GroupByBoxPromptUIElement element) : base(element, AccessibleRole.StaticText)
			{
			}
			#endregion //Constructor

			#region Base class overrides

			#region Name
			/// <summary>
			/// Returns the name of the accessible object.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if (name == null)
					{
						name = Infragistics.Win.Utilities.StripMnemonics(
							((GroupByBoxPromptUIElement)this.UIElement).GroupByBox.Prompt);
					}

					return name;
				}
				set
				{
					base.Name = value;
				}
			}
			#endregion //Name

			#endregion //Base class overrides
		}
		#endregion //AddNewButtonAccessibleObject

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.GroupByBox.Layout, StyleUtils.Role.GroupByBoxPrompt );
			}
		}

		#endregion // UIRole
	}

	
}
