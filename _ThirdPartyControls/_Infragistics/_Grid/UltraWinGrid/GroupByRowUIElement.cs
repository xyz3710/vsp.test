#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	/// The DataAreaUIElement contains the row and column scrolling
	/// regions.
	/// </summary>
	public class GroupByRowUIElement : UIElement
	{
		// space between the GroupByRowExpansionIndicatorUIElement and the
		// GroupByRowDescriptionUIElement		
		internal const int GROUP_BY_EXPANSION_INDICATOR_SPACING		= 2;

		// AS - 12/19/01 
		// To place the lines in the center of the indicator, it must be
		// an odd number. Outlook seems to use 11 so we should too.
		//
		//internal const int EXPANSION_INDICATOR_WIDTH				= 10;
		internal const int EXPANSION_INDICATOR_WIDTH				= 11;
		internal const int GROUP_BY_INDENT = 
			2 * GROUP_BY_EXPANSION_INDICATOR_SPACING + EXPANSION_INDICATOR_WIDTH;
		

		private MouseButtons lastMouseDownButton = MouseButtons.None;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private Point		 lastMouseDownPoint  = Point.Empty;

		// SSP 5/16/05 - Optimization
		// Row col region intersection element had an optimization for not causing the existing
		// row elements to reposition their descendants. It never worked so made it work.
		//
		//private Size origParentSize;		
		internal Rectangle lastClipRect;

		// SSP 4/26/05 - NAS 5.2 Extension of Summaries Functionality
		// Summary footers in group-by rows and when using fixed headers need to clip
		// so the summaries do not overwrite the description when scrolled left.
		//
		internal int groupByRowSummaryFooter_ClipLeft = int.MinValue;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		internal GroupByRowUIElement( UIElement parent ) : base( parent )
		{
		}

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>GroupByRowUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="row">Associated <b>UltraGridGroupByRow</b></param>
		public GroupByRowUIElement( UIElement parent, Infragistics.Win.UltraWinGrid.UltraGridGroupByRow row) : this( parent )
		{
			this.InitializeRow(row);
		}

		internal void InitializeRow( Infragistics.Win.UltraWinGrid.UltraGridGroupByRow row )
		{
			this.PrimaryContext     = row;

			// SSP 5/16/05 - Optimization
			// Row col region intersection element had an optimization for not causing the existing
			// row elements to reposition their descendants. It never worked so made it work.
			//
			
		}

		// SSP 5/16/05 - Optimization
		// Row col region intersection element had an optimization for not causing the existing
		// row elements to reposition their descendants. It never worked so made it work.
		//
		

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			this.GroupByRow.ResolveAppearance( ref appData, flags );
		}


		/// <summary>
		/// The associated GroupByRow object (read-only).
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridGroupByRow GroupByRow
		{
			get
			{
				return (Infragistics.Win.UltraWinGrid.UltraGridGroupByRow)this.PrimaryContext;
			}
		}

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type, object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
		/// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
        protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( null != contexts )
			{
				UltraGridGroupByRow row = this.GroupByRow;

				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						// Group-by rows don't contain cells.
						//
						if ( context is UltraGridCell )
							return false;

						if ( context is UltraGridGroupByRow && row != context )
							return false;						

						if ( context is HeaderBase )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch

		#region PositionChildElements

		// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Added PositionChildElementsHelper. Code in there is moved from the 
		// the PositionChildElements method.
		//
		internal static void PositionChildElementsHelper(
			UltraGridGroupByRow row,
			Rectangle rectInsideBorders,
            // MRS 4/9/2008 - BR31806
            // Added elementContext param to allow us to get the ColScrollRegion
            UIElement elementContext,
            out Rectangle expansionIndicatorRect,
			out Rectangle groupByDescriptionRect,
			out Rectangle groupBySummariesRect,
			out bool summariesWrapped )
		{
			summariesWrapped = false;
			expansionIndicatorRect = groupByDescriptionRect = groupBySummariesRect = Rectangle.Empty;

			UltraGridBand band = row.BandInternal;
			UltraGridLayout layout = band.Layout;
            
			Rectangle workRect;
				
			// JAS 2005 v2 GroupBy Row Extensions
			// In some cases it is not necessary to add an expansion indicator.
			//
			GroupByRowExpansionStyle expansionStyle = band.GroupByRowExpansionStyleResolved;
			bool needsExpansionIndicator = 
				expansionStyle == GroupByRowExpansionStyle.ExpansionIndicator ||
				expansionStyle == GroupByRowExpansionStyle.ExpansionIndicatorAndDoubleClick;

			if( needsExpansionIndicator )
			{
				workRect = rectInsideBorders;

				// JAS 2005 v2 GroupBy Row Extensions
				//			
				//				workRect.X += GROUP_BY_EXPANSION_INDICATOR_SPACING;
				if( band.IndentationGroupByRow != 0 )
				{
					workRect.X += band.IndentationGroupByRowExpansionIndicatorResolved;
				}
				else
				{
					// There is a special case when the IndentationGroupByRow is set to 0.
					// In this situation we base the indicator offset on the level of the groupby row.
					// If we did not do this, then the indicators would all be in a straight vertical line
					// and it would be very difficult to discern the relationships between the groups.
					//
					workRect.X += (band.IndentationGroupByRowExpansionIndicatorResolved * row.GroupByRowLevel);
				}

				if ( workRect.Height > EXPANSION_INDICATOR_WIDTH )
				{
					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// This was causing the Height of the workRect to be EXPANSION_INDICATOR_WIDTH 
					// or 1 + EXPANSION_INDICATOR_WIDTH depending on whether the eleme height was odd 
					// or even. This caused a problem with wrapping of the summaries where when the
					// group-by row called this method to find out if summaries are wrapped or not
					// it resulted in different calculations than what actually happened when elements
					// were positioned. Changed the code so that it's always EXPANSION_INDICATOR_WIDTH.
					//
					//workRect.Inflate( 0, -( ( workRect.Height - EXPANSION_INDICATOR_WIDTH ) / 2 ) );
					workRect.Y += ( workRect.Height - EXPANSION_INDICATOR_WIDTH ) / 2;
					workRect.Height = EXPANSION_INDICATOR_WIDTH;
				}

				workRect.Width = workRect.Height;

				// SSP 12/11/01
				// If the grid is in flat mode, then deflate the expansion indicator
				// rect by 1 pixel.
				//
				if ( workRect.Width >= EXPANSION_INDICATOR_WIDTH )
				{
					// AS 3/20/06 AppStyling
					//if ( layout.Grid.FlatMode )
					if ( layout.Grid.UseFlatModeResolved )
						workRect.Inflate( -1, -1 );
				}

				expansionIndicatorRect = workRect;
			}
			else
			{
				// JAS 2005 v2 GroupBy Row Extensions
				// If there is no expansion indicator then we need to configure the work rect
				// so that it provides the proper location to start positioning the description element.
				//
				workRect = new Rectangle( rectInsideBorders.Location, Size.Empty );
			}

			Rectangle tmp = workRect;
			workRect = rectInsideBorders;

			// JAS 2005 v2 GroupBy Row Extensions - Note: We do not want to use the Band's 
			// IndentationGroupByRowExpansionIndicatorResolved property here because this is determining
			// the distance between the expansion indicator and the description element, which should always
			// be the same value.
			//
			workRect.X = GROUP_BY_EXPANSION_INDICATOR_SPACING + tmp.Right;
			workRect.Width -= GROUP_BY_EXPANSION_INDICATOR_SPACING + tmp.Width;
			int groupByRowPadding = band.GroupByRowPaddingResolved;
			workRect.Inflate( -groupByRowPadding, -groupByRowPadding );

			groupByDescriptionRect = workRect;

			GroupBySummaryDisplayStyle style = band.GroupBySummaryDisplayStyleResolved;
			if ( band.HasSummaries && ( GroupBySummaryDisplayStyle.SummaryCells == style 
				|| GroupBySummaryDisplayStyle.SummaryCellsAlwaysBelowDescription == style ) )
			{
				SummarySettingsCollection summaries = band.Summaries;
				SummaryValuesCollection summaryValues = row.Rows.SummaryValues;				
				SummaryDisplayAreaContext context = SummaryDisplayAreaContext.GetInGroupByRowsSummariesContext( row );
				int summariesHeight = summaryValues.GetSummaryFooterHeight( context );

				if ( summariesHeight > 0 )
				{
					groupBySummariesRect = rectInsideBorders;
					groupBySummariesRect.Y += groupByRowPadding;
					groupBySummariesRect.Height = summariesHeight;

					// groupByRowIndent is negative.
					//
					int groupBySummariesIndent = band.CalcGroupBySummariesIndent( row );
					groupBySummariesRect.X += groupBySummariesIndent;
                    
                    // MRS 4/9/2008 - BR31806
                    //groupBySummariesRect.Width = band.GetExtent( BandOrigin.RowSelector );
                    ColScrollRegion colScollRegion = (elementContext != null)
                        ? GridUtils.GetColScrollRegion(elementContext) :
                        null;

                    if (colScollRegion != null &&
                        colScollRegion.IsExclusive)
                    {
                        groupBySummariesRect.Width = colScollRegion.GetBandExtent(band);
                    }
                    else
                        groupBySummariesRect.Width = band.GetExtent(BandOrigin.RowSelector);

					// Use the Rect instead of RectInsideBorders. The root group-by rows do not
					// draw left borders however non-root do. So if it's non-root group-by row
					// then substract 1. Look in the GroupByRowUIElement.Border3DSides for more
					// info.
					//
					if ( null != row.ParentRow )
						groupBySummariesRect.X--;

					summariesWrapped = true;
					Size descriptionSize = row.GetDescriptionSize( GroupBySummaryDisplayStyle.SummaryCells == style );
					groupByDescriptionRect.Height = descriptionSize.Height;

					// If the GroupBySummaryDisplayStyle is SummaryCells then figure out if we need to wrap
					// the summaries. This style specifies that if there is no conflict between the group-by
					// summaries and the group-by row description then show them on the same line horizontally.
					//
					if ( GroupBySummaryDisplayStyle.SummaryCells == style )
					{
						// Figure out the left of the left most summary.
						//
						int leftOfLeftMostSummary = summaries.GetLeftOfLeftMostSummary( context );
						leftOfLeftMostSummary += groupBySummariesIndent;

						summariesWrapped = rectInsideBorders.X + leftOfLeftMostSummary 
							< groupByDescriptionRect.X + descriptionSize.Width;
					}

                    // MRS 5/26/2009 - TFS17968
                    if (summariesWrapped == false)
                    {
                        if (summaries.Band.RowLayoutStyle != RowLayoutStyle.None &&
                            summaries.Band.RowLayoutLabelStyle == RowLayoutLabelStyle.WithCellData)
                        {
                            summariesWrapped = true;
                        }
                    }

					if ( summariesWrapped )
						groupBySummariesRect.Y = groupByDescriptionRect.Bottom;
					else 
						// If showing summaries on the same line as the description then shrink
						// the description to fit its contents.
						//
						groupByDescriptionRect.Width = descriptionSize.Width;

					groupBySummariesRect.Height = Math.Min( groupBySummariesRect.Height,
						rectInsideBorders.Bottom - groupBySummariesRect.Y );
				}
			}			
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			// Commented out the original code and added new code.
			//
			// --------------------------------------------------------------------------
			Infragistics.Win.UltraWinGrid.UltraGridGroupByRow groupByRow = this.GroupByRow;
            
			bool summariesWrapped;
			Rectangle expansionIndicatorRect, descriptionRect, summariesRect;
            // MRS 4/9/2008 - BR31806
            //GroupByRowUIElement.PositionChildElementsHelper( groupByRow, this.RectInsideBorders,
            GroupByRowUIElement.PositionChildElementsHelper(groupByRow, this.RectInsideBorders, this,
				out expansionIndicatorRect, out descriptionRect, out summariesRect, out summariesWrapped );

			GroupByRowExpansionIndicatorUIElement expansionElement = null;
			GroupByRowDescriptionUIElement descriptionElement = null;
			SummaryFooterUIElement summariesElement = null;

			if ( ! expansionIndicatorRect.IsEmpty )
			{
				expansionElement = (GroupByRowExpansionIndicatorUIElement)GroupByRowUIElement.ExtractExistingElement( 
					this.childElementsCollection, typeof( GroupByRowExpansionIndicatorUIElement ), true );

				if ( null == expansionElement )
					expansionElement = new GroupByRowExpansionIndicatorUIElement( this );

				expansionElement.Rect = expansionIndicatorRect;
			}

			if ( ! descriptionRect.IsEmpty )
			{
				descriptionElement = (GroupByRowDescriptionUIElement)GroupByRowUIElement.ExtractExistingElement( 
					this.childElementsCollection, typeof( GroupByRowDescriptionUIElement ), true );

				if ( null == descriptionElement )
					descriptionElement = new GroupByRowDescriptionUIElement( this );

				descriptionElement.Rect = descriptionRect;
			}

			this.groupByRowSummaryFooter_ClipLeft = int.MinValue;
			if ( ! summariesRect.IsEmpty )
			{
				summariesElement = (SummaryFooterUIElement)GroupByRowUIElement.ExtractExistingElement( 
					this.childElementsCollection, typeof( SummaryFooterUIElement ), true );

				SummaryDisplayAreaContext context = SummaryDisplayAreaContext.GetInGroupByRowsSummariesContext( groupByRow );
				if ( null == summariesElement )
					summariesElement = new SummaryFooterUIElement( this, groupByRow.Rows, context );
				else 
					summariesElement.Initialize( groupByRow.Rows, context );

				summariesElement.Rect = summariesRect;

				// Summary footers in group-by rows and when using fixed headers need to clip
				// so the summaries do not overwrite the description when scrolled left.
				//
				if ( ! descriptionRect.IsEmpty && ! summariesWrapped && summariesRect.X < descriptionRect.Right )
					this.groupByRowSummaryFooter_ClipLeft = descriptionRect.Right;
			}

			this.ChildElements.Clear( );

			if ( null != expansionElement )
				this.ChildElements.Add( expansionElement );

			if ( null != descriptionElement )
				this.ChildElements.Add( descriptionElement );

			if ( null != summariesElement )
				this.ChildElements.Add( summariesElement );

			// SSP 7/28/05 - Header, Row, Summary Tooltips
			// 
			GridUtils.SetupElementToolTip( this, groupByRow.ToolTipText, false );

			
			// --------------------------------------------------------------------------
		}

		#endregion // PositionChildElements

		/// <summary>
		/// Called when the mouse down message is received over the element. 
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		/// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
		/// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
		/// <returns>If true then bypass default processing</returns>
		protected override bool OnMouseDown( MouseEventArgs e, 
			bool adjustableArea,
			ref UIElement captureMouseForElement )
		{		
			this.lastMouseDownButton = e.Button;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.lastMouseDownPoint = new Point( e.X, e.Y );

			// SSP 10/11/01 UWG366
			// When right clicked, select the row
			//
			if ( MouseButtons.Right == e.Button )
			{
				this.GroupByRow.ClickRight( );
			}

			return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );
		}



        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{	
			Infragistics.Win.UltraWinGrid.UltraGridGroupByRow row = this.GroupByRow;

			if ( this.lastMouseDownButton == MouseButtons.Left )
			{
				// JAS v5.2 DoubleClick Events 4/27/05 - Lifted this variable definition out of the if block.
				//
				//Infragistics.Win.UltraWinGrid.UltraGridGroupByRow row = this.GroupByRow;

				// JAS 2005 v2 GroupBy Row Extensions
				// Double clicking on the groupby row should only result in a change in expansion state
				// if the row's IsExpandable indicates that it should.
				//
				bool toggleExpansion = true;
                if (row != null)
                {
                    // MBS 6/9/09 - TFS18320
                    // Seems like the fix for TFS17752 causes us to now expand a GroupByRow on double-click
                    // if the row allows any type of expansion, so we need to ensure that we do allow
                    // the double-click action to do this
                    //
                    //toggleExpansion = row.IsExpandable;
                    GroupByRowExpansionStyle expansionStyle = row.BandInternal.GroupByRowExpansionStyleResolved;
                    toggleExpansion = row.IsExpandable &&
                        (expansionStyle == GroupByRowExpansionStyle.DoubleClick ||
                         expansionStyle == GroupByRowExpansionStyle.ExpansionIndicatorAndDoubleClick);
                }

				if( toggleExpansion )
				{
					//*vl 100200 - Exit edit mode when clicking expansion indicator (bug 1855)
					if ( row.Layout.ActiveCell != null )
					{
						// SSP 8/6/04 UWG3400
						// If the exitting of edit mode was canceled (may be because the cell contained 
						// invalid input) then don't proceed with expansion of the row.
						//
						//row.Layout.ActiveCell.ExitEditMode();
						if ( row.Layout.ActiveCell.ExitEditMode( ) )
							// JAS v5.2 DoubleClick Events 4/27/05 - Don't return here because
							// we need to fire the DoubleClickRow event on the grid.
							//return;
							toggleExpansion = false;
					}

					// JJD 9/26/00 
					// Added ShowAsExpanded method to allow us to show row expansion indicators
					// even if the band does have child bands
					//
					// JAS v5.2 DoubleClick Events 4/27/05 - Only toggle if the user didn't cancel it.
					if( toggleExpansion )
						row.Expanded = !row.ShowAsExpanded;
				}
			}

			// JAS v5.2 DoubleClick Events 4/27/05
			//
			if( row.BandInternal != null && row.BandInternal.Layout != null )
			{
				UltraGrid grid = row.BandInternal.Layout.Grid as UltraGrid;
				if( grid != null )
					grid.FireEvent( 
						GridEventIds.DoubleClickRow, 
						new DoubleClickRowEventArgs( row, RowArea.GroupByRowArea ) );
			}

			base.OnDoubleClick( adjustableArea );
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.GroupByRow.BorderStyleResolved;
			}
		}

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				// if the group by row is the left most (top most ) groupby row
				// then only draw the border at the top to be consistent
				// with what outlook does
				//
				if ( null == this.GroupByRow.ParentRow )
					return Border3DSide.Top;

				return Border3DSide.Top | Border3DSide.Left;
			}
		}

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.GroupByRow.BandInternal, StyleUtils.Role.GroupByRow );
			}
		}

		#endregion // UIRole
	}

	#region GroupByRowDescriptionProvider Class

	internal class GroupByRowDescriptionProvider : TextProviderBase 
	{
		internal GroupByRowDescriptionProvider( UIElement element ) : base( element )
		{			
		}

		public override string GetText( DependentTextUIElement textElement )
		{
			UltraGridGroupByRow groupByRow = 
				(UltraGridGroupByRow)textElement.GetContext( typeof( UltraGridGroupByRow ), true );

			return groupByRow.DescriptionWithSummaries;
		}
	}

	#endregion // GroupByRowDescriptionProvider Class

	#region GroupByRowDescriptionUIElement Class

	/// <summary>
	/// UIElement for displaying the text in group-by rows.
	/// </summary>
	public class GroupByRowDescriptionUIElement : UIElement
	{

		internal GroupByRowDescriptionUIElement( UIElement parent ) : base( parent )
		{
		}

		/// <summary>
		/// The associated GroupByRow object (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridGroupByRow GroupByRow
		{
			get
			{
				return (Infragistics.Win.UltraWinGrid.UltraGridGroupByRow)this.Parent.GetContext( typeof( UltraGridGroupByRow ), true );
			}
		}


        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return UIElementBorderStyle.None;
			}
		}


        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			// SSP 2/25/02
			// Added code for applying any image settings on the group by row appearance.
			//
			//base.InitAppearance( ref appData, ref flags );
			this.GroupByRow.ResolveAppearance( ref appData, flags );

			// set the vertical text alignment to middle
			appData.TextVAlign = VAlign.Middle;
		}

		// SSP 2/2/07 BR19790
		// Added CalcImageSize and PositionImageHelper methods. Logic in PositionImageHelper is moved
		// from the PositionChildElements.
		// 
		internal static Size CalcImageSize( UltraGridGroupByRow row, int availableHeight )
		{
			Rectangle workRect = Rectangle.Empty;
			workRect.Height = availableHeight;
			return PositionImageHelper( row, ref workRect, null, null );
		}

		// SSP 2/2/07 BR19790
		// Added CalcImageSize and PositionImageHelper methods. Logic in PositionImageHelper is moved
		// from the PositionChildElements.
		// 
		internal static Size PositionImageHelper( 
			UltraGridGroupByRow row, 
			ref Rectangle workRect,
			GroupByRowDescriptionUIElement containerElem,
			UIElementsCollection oldElements )
		{
			UltraGridLayout layout = row.Layout;

			AppearanceData appData = new AppearanceData( );
			AppearancePropFlags flags = 
				AppearancePropFlags.ImageHAlign |
				AppearancePropFlags.ImageVAlign |
				AppearancePropFlags.Image;

			row.ResolveAppearance( ref appData, ref flags );

			if ( appData.HasPropertyBeenSet( AppearancePropFlags.Image ) )
			{
				// Take into account the scroll bar if visible because the length
				// of the group by row extends until the end of the row scroll region
				// including the scroll bar.
				//
				RowColRegionIntersectionUIElement rsrElem = null == containerElem ? null 
					: (RowColRegionIntersectionUIElement)containerElem.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );
				if ( null != rsrElem && workRect.Right > rsrElem.Rect.Right )
				{
					RowScrollRegion rsr = rsrElem.RowScrollRegion;
					if ( null != rsr && ! rsr.ScrollbarRect.IsEmpty )
						workRect.Width -= SystemInformation.VerticalScrollBarWidth;
				}

				Size imageSize = Size.Empty;
				UIElement imageElem = null;

				// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
				// 
				if ( appData.Image is UltraGridGroupByRow.UseCellDataAsImage )
				{
					if ( row.Column.Editor is EmbeddableImageRenderer )
					{
						EmbeddableImageRenderer editor = layout.GroupByRowImageRenderer;
						UltraGridRow descendantDataRow = row.FindFirstRegularRow( );
						if ( null != descendantDataRow )
						{
							EmbeddableEditorOwnerBase ownerInfo = new ValueEmbeddableEditorOwner( descendantDataRow.GetCellValue( row.Column ) );
							object ownerContext = null;
							
							imageSize = editor.GetSize( ownerInfo, ownerContext, true, true, false );

							if ( ! imageSize.IsEmpty && null != containerElem )
								imageElem = editor.GetEmbeddableElement(
									containerElem, ownerInfo, ownerContext, false, false, false, false, 
									CellUIElementBase.FindEmbeddableUIElement( oldElements, true ) );
						}
					}
				}
				else
				{
					Image image = appData.GetImage( layout.Grid.ImageList );
					if ( null != image )
					{
						imageSize = image.Size;

						if ( null != containerElem )
						{
							Infragistics.Win.ImageUIElement imageElement = 
								(ImageUIElement)GroupByRowDescriptionUIElement.ExtractExistingElement( oldElements, typeof( ImageUIElement ), true );

							if ( null == imageElement )
							{
								imageElement = new ImageUIElement( containerElem, image );						
							}
							else
							{
								imageElement.Image = image;
							}

							imageElement.Scaled = true;
							imageElem = imageElement;
						}
					}
				}

				// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
				// 
				//Image image = appData.GetImage( row.Layout.Grid.ImageList );
				//if ( null != image )
				//{
				//	Size imageSize = image.Size;

				System.Diagnostics.Debug.Assert( imageSize.Height > 0 && imageSize.Width > 0,
					"Image with height and/or width 0 or less !." );

				// Only proceed if the image width and heigh are greater than 0.
				//
				if ( imageSize.Width > 0 && imageSize.Height > 0 )
				{
					Rectangle imageRect = new Rectangle( workRect.Location, imageSize );
 
					if ( imageRect.Height > workRect.Height )
					{
						double scaleFactor = (double)workRect.Height / imageRect.Height;

						imageRect.Height = workRect.Height;
						imageRect.Width = (int)( imageRect.Width * scaleFactor );
					}
						
					switch ( appData.ImageHAlign )
					{						
						case Infragistics.Win.HAlign.Default:
						case Infragistics.Win.HAlign.Left:
							// SSP 2/2/07 BR19790
							// We need to adjust the width accordingly as well.
							// 
							//workRect.X = 2 + imageRect.Right;
							int delta = 2 + imageRect.Right - workRect.X;
							workRect.X += delta;
							workRect.Width -= delta;

							break;
						case Infragistics.Win.HAlign.Right:
							imageRect.X = workRect.Right - imageRect.Width;
							workRect.Width = imageRect.Right - workRect.Left;
							break;
						case Infragistics.Win.HAlign.Center:							
						default:
							imageRect.X += ( workRect.Width - imageRect.Width ) / 2;
							break;
					}
					
					switch ( appData.ImageVAlign )
					{						
						case Infragistics.Win.VAlign.Top:								
							break;
						case Infragistics.Win.VAlign.Bottom:
							imageRect.Y = workRect.Bottom - imageRect.Height;
							break;
						case Infragistics.Win.VAlign.Middle:
						case Infragistics.Win.VAlign.Default:
						default:
							imageRect.Y += ( workRect.Height - imageRect.Height ) / 2;
							break;
					}

					if ( null != containerElem && null != imageElem )
					{
						imageElem.Rect = imageRect;
						containerElem.ChildElements.Add( imageElem );
					}

					return imageRect.Size;
				}
			}

			return Size.Empty;
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			Infragistics.Win.UltraWinGrid.UltraGridGroupByRow row = this.GroupByRow;

			Rectangle workRect = this.RectInsideBorders;
			
			UIElementsCollection oldElements = this.ChildElements;

			this.childElementsCollection = null;
			

			// SSP 2/2/07 BR19790
			// Moved the logic into PositionImageHelper so it can be used to calculate the image
			// size as well.
			// 
			GroupByRowDescriptionUIElement.PositionImageHelper( row, ref workRect, this, oldElements );
			


			// SSP 2/25/02
			// rowPadding is not used anywhere. So commented below line out.
			//
			//int rowPadding = row.Band.GroupByRowPaddingResolved;
            
			DependentTextUIElement textElement = 
				(DependentTextUIElement)GroupByRowDescriptionUIElement.ExtractExistingElement( oldElements, typeof( DependentTextUIElement ), true );

			if ( null == textElement )
				textElement = new DependentTextUIElement( this, new GroupByRowDescriptionProvider( this ) );

			this.ChildElements.Add( textElement );

			// SSP 2/25/02
			//
			//Rectangle textRect = this.Rect;
			Rectangle textRect = workRect;

			textElement.Rect = textRect;

			// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
			// 
			oldElements.DisposeElements( );

			oldElements.Clear( );
		}

		


        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{
			// this element doesn't draw a background
		}

		/// <summary>
		/// this element doesn't draw an image background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
		}
	}

	#endregion // GroupByRowDescriptionUIElement Class
}
