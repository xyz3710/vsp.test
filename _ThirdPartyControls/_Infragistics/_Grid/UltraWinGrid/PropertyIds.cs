#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;

	/// <summary>
	///    uniquely identify each property.  
	/// </summary>
	public enum PropertyIds 
	{
		/// <summary>
		/// property id identifying Activation property
		/// </summary>
		Activation                = 1,

		/// <summary>
		/// property id identifying ActiveCellAppearance property
		/// </summary>
		ActiveCellAppearance      = 2,

		/// <summary>
		/// property id identifying ActiveRowAppearance property
		/// </summary>
		ActiveRowAppearance       = 3,

		/// <summary>
		/// property id identifying Add property
		/// </summary>
		Add                       = 4,

		/// <summary>
		/// property id identifying AddButtonCaption property
		/// </summary>
		AddButtonCaption          = 5,

		/// <summary>
		/// property id identifying AddButtonToolTipText property
		/// </summary>
		AddButtonToolTipText      = 6,

		/// <summary>
		/// property id identifying AddNewBox property
		/// </summary>
		AddNewBox                 = 7,

		/// <summary>
		/// property id identifying AddNewBoxStyle property
		/// </summary>
		AddNewBoxStyle            = 8,

		/// <summary>
		/// property id identifying AddNewRowButtonUIElement property
		/// </summary>
		AddNewRowButtonUIElement  = 9,

		/// <summary>
		/// property id identifying AllowAddNew property
		/// </summary>
		AllowAddNew               = 10,

		/// <summary>
		/// property id identifying AllowColMoving property
		/// </summary>
		AllowColMoving            = 11,

		/// <summary>
		/// property id identifying AllowColSizing property
		/// </summary>
		AllowColSizing            = 12,

		/// <summary>
		/// property id identifying AllowColSwapping property
		/// </summary>
		AllowColSwapping          = 13,

		/// <summary>
		/// property id identifying AllowDelete property
		/// </summary>
		AllowDelete               = 14,

		/// <summary>
		/// property id identifying AllowGroupMoving property
		/// </summary>
		AllowGroupMoving          = 15,

		/// <summary>
		/// property id identifying AllowGroupSwapping property
		/// </summary>
		AllowGroupSwapping        = 16,

		/// <summary>
		/// property id identifying AllowUpdate property
		/// </summary>
		AllowUpdate               = 17,

		/// <summary>
		/// property id identifying AlphaBlendEnabled property
		/// </summary>
		AlphaBlendEnabled         = 18,

		/// <summary>
		/// property id identifying Appearance property
		/// </summary>
		Appearance                = 20,

		/// <summary>
		/// property id identifying AutoEdit property
		/// </summary>
		AutoEdit                  = 22,

		/// <summary>
		/// property id identifying AutoPreviewEnabled property
		/// </summary>
		AutoPreviewEnabled        = 23,

		/// <summary>
		/// property id identifying AutoPreviewField property
		/// </summary>
		AutoPreviewField          = 24,

		/// <summary>
		/// property id identifying AutoPreviewHidden property
		/// </summary>
		AutoPreviewHidden         = 25,

		/// <summary>
		/// property id identifying AutoPreviewMaxLines property
		/// </summary>
		AutoPreviewMaxLines       = 26,

		/// <summary>
		/// property id identifying AutoSizeEdit property
		/// </summary>
		AutoSizeEdit              = 27,

		/// <summary>
		/// property id identifying Band property
		/// </summary>
		Band                      = 28,

		/// <summary>
		/// property id identifying Bands property
		/// </summary>
		Bands                     = 29,

		/// <summary>
		/// property id identifying BorderStyle property
		/// </summary>
		BorderStyle               = 30,

		/// <summary>
		/// property id identifying BorderStyleCaption property
		/// </summary>
		BorderStyleCaption        = 31,

		/// <summary>
		/// property id identifying BorderStyleCell property
		/// </summary>
		BorderStyleCell           = 32,

		/// <summary>
		/// property id identifying BorderStyleHeader property
		/// </summary>
		BorderStyleHeader         = 33,

		/// <summary>
		/// property id identifying BorderStyleRow property
		/// </summary>
		BorderStyleRow            = 34,

		/// <summary>
		/// property id identifying ButtonAppearance property
		/// </summary>
		ButtonAppearance          = 35,

		/// <summary>
		/// property id identifying ButtonConnectorStyle property
		/// </summary>
		ButtonConnectorStyle = 36,

		/// <summary>
		/// property id identifying ButtonConnectorColor property
		/// </summary>
		ButtonConnectorColor      = 37,

		/// <summary>
		/// property id identifying ButtonDisplayStyle property
		/// </summary>
		ButtonDisplayStyle        = 38,

		/// <summary>
		/// property id identifying Caption property
		/// </summary>
		Caption                   = 39,

		/// <summary>
		/// property id identifying CaptionAppearance property
		/// </summary>
		CaptionAppearance         = 40,

		/// <summary>
		/// property id identifying Cell property
		/// </summary>
		Cell                      = 41,

		/// <summary>
		/// property id identifying CellAppearance property
		/// </summary>
		CellAppearance            = 42,

		/// <summary>
		/// property id identifying CellClickAction property
		/// </summary>
		CellClickAction           = 43,

		/// <summary>
		/// property id identifying CellMultiLine property
		/// </summary>
		CellMultiLine             = 44,

		/// <summary>
		/// property id identifying CellPadding property
		/// </summary>
		CellPadding               = 45,

		/// <summary>
		/// property id identifying CellSpacing property
		/// </summary>
		CellSpacing               = 46,

		/// <summary>
		/// property id identifying Cells property
		/// </summary>
		Cells                     = 47,

		/// <summary>
		/// property id identifying Clear property
		/// </summary>
		Clear                     = 48,

		/// <summary>
		/// property id identifying ClearUnbound property
		/// </summary>
		ClearUnbound              = 49,

		/// <summary>
		/// property id identifying ColHeaderLines property
		/// </summary>
		ColHeaderLines            = 50,

		/// <summary>
		/// property id identifying ColHeadersVisible property
		/// </summary>
		ColHeadersVisible         = 51,

		/// <summary>
		/// property id identifying ColScrollRegion property
		/// </summary>
		ColScrollRegion           = 52,

		/// <summary>
		/// property id identifying ColScrollRegions property
		/// </summary>
		ColScrollRegions          = 53,

		/// <summary>
		/// property id identifying ColSpan property
		/// </summary>
		ColSpan                   = 54,

		/// <summary>
		/// property id identifying Column property
		/// </summary>
		Column                    = 55,

		/// <summary>
		/// property id identifying Columns property
		/// </summary>
		Columns                   = 56,

		/// <summary>
		/// property id identifying DataChanged, property
		/// </summary>
		DataChanged               = 57,

		/// <summary>
		/// property id identifying DataType property
		/// </summary>
		DataType                  = 58,

		/// <summary>
		/// property id identifying DefaultColWidth property
		/// </summary>
		DefaultColWidth           = 60,

		/// <summary>
		/// property id identifying DefaultRowHeight property
		/// </summary>
		DefaultRowHeight          = 61,

		/// <summary>
		/// property id identifying Delete property
		/// </summary>
		Delete                    = 62,

		/// <summary>
		/// property id identifying Description property
		/// </summary>
		Description               = 63,

		/// <summary>
		/// property id identifying DroppedDown property
		/// </summary>
		DroppedDown               = 66,

		/// <summary>
		/// property id identifying EditCellAppearance property
		/// </summary>
		EditCellAppearance        = 67,

		/// <summary>
		/// property id identifying Enabled property
		/// </summary>
		Enabled                   = 68,

		/// <summary>
		/// property id identifying ExclusiveColScrollRegion property
		/// </summary>
		ExclusiveColScrollRegion  = 69,

		/// <summary>
		/// property id identifying ExpandChildRowsOnLoad property
		/// </summary>
		ExpandChildRowsOnLoad     = 70,

		/// <summary>
		/// property id identifying ExpandRowsOnLoad property
		/// </summary>
		ExpandRowsOnLoad          = 71,

		/// <summary>
		/// property id identifying Expandable property
		/// </summary>
		Expandable                = 72,

		/// <summary>
		/// property id identifying Expanded property
		/// </summary>
		Expanded                  = 73,

		/// <summary>
		/// property id identifying ExpansionIndicator property
		/// </summary>
		ExpansionIndicator        = 74,

		/// <summary>
		/// property id identifying Indentation property
		/// </summary>
		Indentation               = 75,

		/// <summary>
		/// property id identifying FixedHeight property
		/// </summary>
		FixedHeight               = 76,

		/// <summary>
		/// property id identifying DisplayLayout property
		/// </summary>
		DisplayLayout                = 77,

		/// <summary>
		/// property id identifying Group property
		/// </summary>
		Group                     = 78,

		/// <summary>
		/// property id identifying GroupHeaderLines property
		/// </summary>
		GroupHeaderLines          = 79,

		/// <summary>
		/// property id identifying GroupHeadersVisible property
		/// </summary>
		GroupHeadersVisible       = 80,

		/// <summary>
		/// property id identifying Groups property
		/// </summary>
		Groups                    = 81,

		/// <summary>
		/// property id identifying Header property
		/// </summary>
		Header                    = 82,

		/// <summary>
		/// property id identifying HeaderAppearance property
		/// </summary>
		HeaderAppearance          = 83,

		/// <summary>
		/// property id identifying HeaderClickAction property
		/// </summary>
		HeaderClickAction         = 84,

		/// <summary>
		/// property id identifying HeaderVisible property
		/// </summary>
		HeaderVisible             = 85,

		/// <summary>
		/// property id identifying Height property
		/// </summary>
		Height                    = 86,

		/// <summary>
		/// property id identifying Hidden property
		/// </summary>
		Hidden                    = 87,

		/// <summary>
		/// property id identifying Index property
		/// </summary>
		Index                     = 88,

		/// <summary>
		/// property id identifying InterBandSpacing property
		/// </summary>
		InterBandSpacing          = 89,

		/// <summary>
		/// property id identifying IsAlternate property
		/// </summary>
		IsAlternate               = 90,

		/// <summary>
		/// property id identifying Key property
		/// </summary>
		Key                       = 91,

		/// <summary>
		/// property id identifying Layout property
		/// </summary>
		Layout                    = 92,

		/// <summary>
		/// property id identifying Level property
		/// </summary>
		Level                     = 93,

		/// <summary>
		/// property id identifying LevelCount property
		/// </summary>
		LevelCount                = 94,

		/// <summary>
		/// property id identifying LockedWidth property
		/// </summary>
		LockedWidth               = 95,

		/// <summary>
		/// property id identifying MaskClipMode property
		/// </summary>
		MaskClipMode              = 96,

		/// <summary>
		/// property id identifying MaskDataMode property
		/// </summary>
		MaskDataMode              = 97,

		/// <summary>
		/// property id identifying MaskDisplayMode property
		/// </summary>
		MaskDisplayMode           = 98,

		/// <summary>
		/// property id identifying MaskInput property
		/// </summary>
		MaskInput                 = 99,

		/// <summary>
		/// property id identifying MaxColScrollRegions property
		/// </summary>
		MaxColScrollRegions       = 100,

		/// <summary>
		/// This property is obsolete. Use <see cref="UltraGridColumn.MaxValue"/> instead.
		/// </summary>
		/// <seealso cref="UltraGridColumn.MaxValue"/>	
		MaxDate                   = 101,

		/// <summary>
		/// property id identifying MaxRowScrollRegions property
		/// </summary>
		MaxRowScrollRegions       = 102,

		/// <summary>
		/// property id identifying MaxSelectedCells property
		/// </summary>
		MaxSelectedCells          = 103,

		/// <summary>
		/// property id identifying MaxSelectedRows property
		/// </summary>
		MaxSelectedRows           = 104,

		/// <summary>
		/// property id identifying MaxWidth property
		/// </summary>
		MaxWidth                  = 105,

		/// <summary>
		/// This property is obsolete. Use <see cref="UltraGridColumn.MinValue"/> instead.
		/// </summary>
		/// <seealso cref="UltraGridColumn.MinValue"/>	
		MinDate                   = 106,

		/// <summary>
		/// property id identifying MinWidth property
		/// </summary>
		MinWidth                  = 107,

		/// <summary>
		/// property id identifying Nullable property
		/// </summary>
		Nullable                  = 108,

		/// <summary>
		/// NullText property id
		/// </summary>
		NullText				  = 109,	

		/// <summary>
		/// property id identifying Override property
		/// </summary>
		Override                  = 110,

		/// <summary>
		/// property id identifying Overrides property
		/// </summary>
		Overrides                 = 111,

		/// <summary>
		/// property id identifying ParentColumn property
		/// </summary>
		ParentColumn              = 112,

		/// <summary>
		/// property id identifying Prompt property
		/// </summary>
		Prompt                    = 113,

		/// <summary>
		/// property id identifying PromptChar property
		/// </summary>
		PromptChar                = 114,

		/// <summary>
		/// PadChar property id
		/// </summary>
		PadChar					  = 115,

		/// <summary>
		/// property id identifying ProportionalResize property
		/// </summary>
		ProportionalResize        = 116,

		/// <summary>
		/// property id identifying Remove property
		/// </summary>
		Remove                    = 117,

		/// <summary>
		/// property id identifying RowAlternateAppearance property
		/// </summary>
		RowAlternateAppearance    = 118,

		/// <summary>
		/// property id identifying RowAppearance property
		/// </summary>
		RowAppearance             = 119,

		/// <summary>
		/// property id identifying RowConnectorColor property
		/// </summary>
		RowConnectorColor         = 120,

		/// <summary>
		/// property id identifying RowConnectorStyle property
		/// </summary>
		RowConnectorStyle         = 121,

		/// <summary>
		/// property id identifying RowPreviewAppearance property
		/// </summary>
		RowPreviewAppearance      = 122,

		/// <summary>
		/// property id identifying RowScrollRegion property
		/// </summary>
		RowScrollRegion           = 123,

		/// <summary>
		/// property id identifying RowScrollRegions property
		/// </summary>
		RowScrollRegions          = 124,

		/// <summary>
		/// property id identifying RowSelectorAppearance property
		/// </summary>
		RowSelectorAppearance     = 125,

		/// <summary>
		/// property id identifying RowSelectors property
		/// </summary>
		RowSelectors              = 126,

		/// <summary>
		/// property id identifying RowSizing property
		/// </summary>
		RowSizing                 = 127,

		/// <summary>
		/// property id identifying RowSizingArea property
		/// </summary>
		RowSizingArea             = 128,

		/// <summary>
		/// property id identifying RowSizingAutoMaxLines property
		/// </summary>
		RowSizingAutoMaxLines     = 129,

		/// <summary>
		/// property id identifying RowSpacingAfter property
		/// </summary>
		RowSpacingAfter           = 130,

		/// <summary>
		/// property id identifying RowSpacingBefore property
		/// </summary>
		RowSpacingBefore          = 131,

		/// <summary>
		/// property id identifying ScrollTipField property
		/// </summary>
		ScrollTipField            = 132,

		/// <summary>
		/// property id identifying Scrollbar property
		/// </summary>
		Scrollbar                 = 133,

		/// <summary>
		/// property id identifying Scrollbars property
		/// </summary>
		Scrollbars                = 134,

		/// <summary>
		/// property id identifying SelLength property
		/// </summary>
		SelLength                 = 135,

		/// <summary>
		/// property id identifying SelStart property
		/// </summary>
		SelStart                  = 136,

		/// <summary>
		/// property id identifying SelText property
		/// </summary>
		SelText                   = 137,

		/// <summary>
		/// property id identifying SelectTypeCell property
		/// </summary>
		SelectTypeCell            = 138,

		/// <summary>
		/// property id identifying SelectTypeCol property
		/// </summary>
		SelectTypeCol             = 139,

		/// <summary>
		/// property id identifying SelectTypeRow property
		/// </summary>
		SelectTypeRow             = 140,

		/// <summary>
		/// property id identifying SelectedCellAppearance property
		/// </summary>
		SelectecCellAppearance    = 141,

		/// <summary>
		/// property id identifying Selected property
		/// </summary>
		Selected                  = 142,

		/// <summary>
		/// property id identifying SelectedRowAppearance property
		/// </summary>
		SelectedRowAppearance     = 143,

		/// <summary>
		/// property id identifying SizingMode property
		/// </summary>
		SizingMode                = 144,

		/// <summary>
		/// property id identifying SortIndicator property
		/// </summary>
		SortIndicator             = 145,

		/// <summary>
		/// property id identifying SortedColumns property
		/// </summary>
		SortedColumns                = 147,

		/// <summary>
		/// property id identifying Style property
		/// </summary>
		Style                     = 148,

		/// <summary>
		/// property id identifying TabNavigation property
		/// </summary>
		TabNavigation             = 149,

		/// <summary>
		/// property id identifying TabStop property
		/// </summary>
		TabStop                   = 150,

		/// <summary>
		/// property id identifying Text property
		/// </summary>
		Text                      = 151,

		/// <summary>
		/// property id identifying TipStyleCell property
		/// </summary>
		TipStyleCell              = 152,

		/// <summary>
		/// property id identifying TipStyleRowConnector property
		/// </summary>
		TipStyleRowConnector      = 153,

		/// <summary>
		/// property id identifying TipStyleScroll property
		/// </summary>
		TipStyleScroll            = 154,

		/// <summary>
		/// property id identifying Value property
		/// </summary>
		Value                     = 155,

		/// <summary>
		/// property id identifying ValueList property
		/// </summary>
		ValueList                 = 156,

		/// <summary>
		/// property id identifying ValueLists property
		/// </summary>
		ValueLists                 = 157,

		/// <summary>
		/// property id identifying VertScrollBar property
		/// </summary>
		VertScrollBar             = 160,

		/// <summary>
		/// property id identifying ViewStyle property
		/// </summary>
		ViewStyle                 = 161,

		/// <summary>
		/// property id identifying ViewStyleBand property
		/// </summary>
		ViewStyleBand             = 162,

		/// <summary>
		/// property id identifying VisiblePosition property
		/// </summary>
		VisiblePosition           = 163,

		/// <summary>
		/// property id identifying Width property
		/// </summary>
		Width                     = 164,

		/// <summary>
		/// property id identifying the column's Format property
		/// </summary>
		ColumnFormat              = 165,

		
		/// <summary>
		/// Property id identifying Overrride's GroupByColumnAppearance property
		/// </summary>
		GroupByColumnAppearance	  = 166,

		/// <summary>
		/// Property id identifying Overrride's GroupByColumnHeaderAppearance property
		/// </summary>
		GroupByColumnHeaderAppearance = 167,

		/// <summary>
		/// Property id identifying Overrride's GroupByRowAppearance property
		/// </summary>
		GroupByRowAppearance	  = 168,
       
		/// <summary>
		/// Property id identifying Override's GroupByRowPadding property
		/// </summary>
		GroupByRowPadding		  = 170,
		
		/// <summary>
		/// Property id identifying Override's SelectTypeGroupByRow property
		/// </summary>
		SelectTypeGroupByRow	  = 171,

		/// <summary>
		///  Property id identifying Override's AllowGroupBy property		
		/// </summary>
		AllowGroupBy			  = 172,

		/// <summary>
		///  Property id identifying Override's GroupByColumnsHidden property		
		/// </summary>
		GroupByColumnsHidden	  = 173,

		/// <summary>
		/// property id identifying GroupByBox property
		/// </summary>
		GroupByBox                = 174,

		/// <summary>
		/// property id identifying GroupByBox.ShowBandLabels property
		/// </summary>
		ShowBandLabels			  = 175,

		/// <summary>
		/// property id identifying GroupByBox.BandLabelAppearance property
		/// </summary>
		BandLabelAppearance		  = 176,

		/// <summary>
		/// property id identifying GroupByBox.BandLabelBorderStyle property
		/// </summary>
		BandLabelBorderStyle	  = 177,

		/// <summary>
		/// property id identifying Column.IsGroupByColumn property
		/// </summary>
		IsGroupByColumn			  = 188,

		/// <summary>
		/// property id identifying Override.GroupByRowDescriptionMask property
		/// </summary>
		GroupByRowDescriptionMask = 189,

		/// <summary>
		/// property id identifying ColScrollRegoin.Position
		/// </summary>
		Position				  = 191,

		/// <summary>
		/// property id identifying Band.AutoPreviewIndentation
		/// </summary>
		AutoPreviewIndentation	  = 192,

		/// <summary>
		/// property id identifying Column.Case property
		/// </summary>
		Case					  = 193,

		/// <summary>
		/// property id identifying Layouts collection
		/// </summary>
		Layouts                   = 194,

		/// <summary>
		/// property id identifying DataSource property
		/// </summary>
		DataSource                = 195,

		/// <summary>
		/// property id identifying DataMember property
		/// </summary>
		DataMember                = 196,

		/// <summary>
		/// property id identifying ImageList property
		/// </summary>
		ImageList                 = 197,

		/// <summary>
		/// property id identifying the SelectionStrategyFilter property
		/// </summary>
		SelectionStrategyFilter   = 198,

		/// <summary>
		/// property id identifying the UpdateMode property
		/// </summary>
		UpdateMode				  = 199,

		/// <summary>
		/// property id identifying the DisplayMember property
		/// </summary>
		DisplayMember			  = 200,

		/// <summary>
		/// property id identifying the ValueMember property
		/// </summary>
		ValueMember				  = 201,

		/// <summary>
		/// property id identifying the DropDownWidth property
		/// </summary>
		DropDownWidth			  = 202,

		/// <summary>
		/// property id identifying the MaxDropDownItems property
		/// </summary>
		MaxDropDownItems		  = 203,

		/// <summary>
		/// property id identifying the MinDropDownItems property
		/// </summary>
		MinDropDownItems		  = 204,

		/// <summary>
		/// property id identifying the AutoSize property
		/// </summary>
		AutoSize				  = 205,

		/// <summary>
		/// property id identifying the DropDownStyle property
		/// </summary>
		DropDownStyle			  = 206,

		/// <summary>
		/// property id identifying the Padding property
		/// </summary>
		Padding					  = 207,

		/// <summary>
		/// Property id identifying the MaskLiteralsAppearance property.
		/// </summary>
		MaskLiteralsAppearance	  = 208,

		/// <summary>
		/// Property id identifying the ButtonStyle property.
		/// </summary>
		ButtonStyle				  = 209,

		/// <summary>
		/// Property id identifying the Appearances collection property.
		/// </summary>
		Appearances				  = 210,

		/// <summary>
		/// Property id identifying the CellButtonAppearance property.
		/// </summary>
		CellButtonAppearance	  = 211,

		/// <summary>
		/// property id identifying BorderStyleCard property
		/// </summary>
		BorderStyleCard		      = 212,

		/// <summary>
		/// property id identifying BorderStyleCardArea property
		/// </summary>
		BorderStyleCardArea       = 213,

		/// <summary>
		/// property id identifying CardSpacing property
		/// </summary>
		CardSpacing               = 214,

		/// <summary>
		/// Property id identifying the CardAreaAppearance property.
		/// </summary>
		CardAreaAppearance        = 216,

		/// <summary>
		/// Property id identifying the CardCaptionAppearance property.
		/// </summary>
		CardCaptionAppearance     = 217,

		/// <summary>
		/// Property id identifying the ActiveCardCaptionAppearance property.
		/// </summary>
		ActiveCardCaptionAppearance   = 218,

		/// <summary>
		/// Property id identifying the SelectedCardCaptionAppearance property.
		/// </summary>
		SelectedCardCaptionAppearance = 219,

		/// <summary>
		/// Property id identifying the CardView property.
		/// </summary>
		CardView					 = 220,

		/// <summary>
		/// Property id identifying the AutoFitColumns property.
		/// </summary>
		AutoFitColumns				 = 221,	

		/// <summary>
		/// Property id identifying the CardCaption property.
		/// </summary>
		CardCaption				     = 222,
	
		/// <summary>
		/// Property id identifying the CardSettings property.
		/// </summary>
		CardSettings				 = 223,

		/// <summary>
		/// Property id identifying the CaptionField property.
		/// </summary>
		CaptionField				 = 224,

		/// <summary>
		/// Property id identifying the AllowLabelSizing property.
		/// </summary>
		AllowLabelSizing             = 225,

		/// <summary>
		/// Property id identifying the AllowSizing property.
		/// </summary>
		AllowSizing                  = 226,

		/// <summary>
		/// Property id identifying the AutoFit property.
		/// </summary>
		AutoFit			             = 227,

		/// <summary>
		/// Property id identifying the CaptionLines property.
		/// </summary>
		CaptionLines                 = 228,

		/// <summary>
		/// Property id identifying the CardStyle property.
		/// </summary>
		CardStyle                    = 229,

		/// <summary>
		/// Property id identifying the LabelWidth property.
		/// </summary>
		LabelWidth                   = 230,

		/// <summary>
		/// Property id identifying the MaxCardAreaCols property.
		/// </summary>
		MaxCardAreaCols              = 231,

		/// <summary>
		/// Property id identifying the MaxCardAreaRows property.
		/// </summary>
		MaxCardAreaRows              = 232,

		/// <summary>
		/// Property id identifying the ShowCaption property.
		/// </summary>
		ShowCaption                  = 233,

		/// <summary>
		/// Property id identifying the ScrollBarLook property.
		/// </summary>
		ScrollBarLook				 = 234,

		/// <summary>
		/// Property id identifying PromptAppearance property.
		/// </summary>
		PromptAppearance			 = 235,
		
		/// <summary>
		/// Property id identifying HiddenWhenGroupBy property
		/// </summary>
		HiddenWhenGroupBy			 = 236,

		/// <summary>
		/// Property id identifying ScrollStyle property
		/// </summary>
		ScrollStyle					 = 237,

		// SSP 3/21/02
		// Added property ids for properties for row filtering feature
		//
		/// <summary>
		/// Property id identifying AllowRowFiltering property
		/// </summary>
		AllowRowFiltering			 = 300,

		/// <summary>
		/// Property id identifying RowFilterMode property
		/// </summary>
		RowFilterMode				 = 301,

		/// <summary>
		/// Property id identifying ComparisionOperator property
		/// </summary>
		ComparisionOperator			 = 302,

		/// <summary>
		/// Property id identifying LogicalOperator property
		/// </summary>
		LogicalOperator				 = 303,

		/// <summary>
		/// Property id identifying CompareValue property
		/// </summary>
		CompareValue				 = 304,

		/// <summary>
		/// Property id identifying FilterConditions property
		/// </summary>
		FilterConditions			 = 305,

		/// <summary>
		/// Property id identifying ColumnFilter property
		/// </summary>
		ColumnFilter				 = 306,
	
		/// <summary>
		/// Property id identifying ColumnFilters property
		/// </summary>
		ColumnFilters				 = 307,

		/// <summary>
		/// Property id identifying AllowRowSummaries property
		/// </summary>
		AllowRowSummaries			 = 308,

		/// <summary>
		/// Property id identifying BorderStyleSummaryFooter property
		/// </summary>
		BorderStyleSummaryFooter	 = 309,
		
		/// <summary>
		/// Property id identifying BorderStyleSummaryValue property
		/// </summary>
		BorderStyleSummaryValue		 = 310,

		/// <summary>
		/// Property id identifying SummaryFooterAppearance property
		/// </summary>
		SummaryFooterAppearance		 = 311,

		/// <summary>
		/// Property id identifying SummaryValueAppearance property
		/// </summary>
		SummaryValueAppearance		 = 312,

		/// <summary>
		/// Property id identifying DisplayInGroupBy property
		/// </summary>
		DisplayInGroupBy			 = 313,

		/// <summary>
		/// Property id identifying SummaryType property
		/// </summary>
		SummaryType					 = 314,

		/// <summary>
		/// Property id identifying CustomSummaryCalculator property
		/// </summary>
		CustomSummaryCalculator		 = 315,

		/// <summary>
		/// Property id identifying DisplayFormat property
		/// </summary>
		DisplayFormat				 = 316,

		/// <summary>
		/// Property id identifying SummaryPosition property
		/// </summary>
		SummaryPosition				 = 317,

		/// <summary>
		/// Property id identifying SummaryPositionColumn property
		/// </summary>
		SummaryPositionColumn		 = 318,

		/// <summary>
		/// Property id identifying SummaryPositionColumn property
		/// </summary>
		SourceColumn				 = 319,

		/// <summary>
		/// Property id identifying Lines property
		/// </summary>
		Lines						 = 320,

		/// <summary>
		/// Property id identifying Summaries property.
		/// </summary>
		Summaries					 = 321,

		/// <summary>
		/// Property id identifying SummaryFooterCaptionVisible property.
		/// </summary>
		SummaryFooterCaptionVisible	 = 322,

		/// <summary>
		/// Property id identifying SummaryFooterCaption property.
		/// </summary>
		SummaryFooterCaption		 = 323,

		/// <summary>
		/// Property id identifying SummaryFooterCaption property.
		/// </summary>
		PriorityScrolling			 = 324,

		/// <summary>
		/// Property id identifying SummaryFooterCaption property.
		/// </summary>
		BorderStyleSummaryFooterCaption = 325,

		/// <summary>
		/// Property id identifying SummaryButtonImage property.
		/// </summary>
		SummaryButtonImage				= 326,

		/// <summary>
		/// Property id identifying FilterDropDownButtonImage property.
		/// </summary>
		FilterDropDownButtonImage		= 327,

		// SSP 7/19/02 UWG1364
		// Added a new ActiveFilterDropDownButtonImage property off the layout.
		//
		/// <summary>
		/// Property id identifying FilterDropDownButtonImage property.
		/// </summary>
		FilterDropDownButtonImageActive	= 328,

		/// <summary>
		/// Property id identifying IsCardCompressed property.
		/// </summary>
		IsCardCompressed				= 329,

		/// <summary>
		/// Property id identifying GroupByBox.Style property.
		/// </summary>
		GroupByBoxStyle					= 330,

		/// <summary>
		/// Property id identifying SummaryFooterCaptionAppearance property.
		/// </summary>
		SummaryFooterCaptionAppearance	= 331,

		// SSP 8/15/02 
		// Added SummarySettings and SummaryValue
		//
		/// <summary>
		/// Property id identifying SummarySettings property.
		/// </summary>
		SummarySettings					= 332,

		/// <summary>
		/// Property id identifying SummaryValue property.
		/// </summary>
		SummaryValue					= 333,

		// SSP 2/14/03 - RowSelectorWidth property
		// Added RowSelectorWidth property to allow the user to control the widths
		// of the row selectors.
		//
		/// <summary>
		/// Property id identifying RowSelector property.
		/// </summary>
		RowSelectorWidth				= 334,

		// SSP 4/14/03 
		// Added ColumnAutoSizeMode off the override and the column.
		//
		/// <summary>
		/// Property id identifying RowSelector property.
		/// </summary>
		ColumnAutoSizeMode				= 335,

		// SSP 1/30/03 - Row Layout Functionality
		//
		#region Row Layout Code

		/// <summary>
		/// Property id identifying <see cref="UltraGridBand.UseRowLayout"/> property.
		/// </summary>
		UseRowLayout					= 336,

		/// <summary>
		/// Property id identifying LabelPosition property.
		/// </summary>
		LabelPosition					= 337,

		/// <summary>
		/// Property id identifying LabelSpan property.
		/// </summary>
		LabelSpan						= 338,	

		/// <summary>
		/// Property id identifying MinimumCellSize property.
		/// </summary>
		MinimumCellSize					= 339,

		/// <summary>
		/// Property id identifying MinimumLabelSize property.
		/// </summary>
		MinimumLabelSize				= 340,

		/// <summary>
		/// Property id identifying OriginX property.
		/// </summary>
		OriginX							= 341,

		/// <summary>
		/// Property id identifying OriginY property.
		/// </summary>
		OriginY							= 342,

		/// <summary>
		/// Property id identifying PreferredCellSize property.
		/// </summary>
		PreferredCellSize				= 343,
		
		/// <summary>
		/// Property id identifying PreferredLabelSize property.
		/// </summary>
		PreferredLabelSize				= 344,

		/// <summary>
		/// Property id identifying SpanX property.
		/// </summary>
		SpanX							= 345,

		/// <summary>
		/// Property id identifying SpanY property.
		/// </summary>
		SpanY							= 346,

		/// <summary>
		/// Property id identifying WeightX property.
		/// </summary>
		WeightX							= 347,

		/// <summary>
		/// Property id identifying WeightY property.
		/// </summary>
		WeightY							= 348,

		/// <summary>
		/// Property id identifying BorderStyleRowSelector property.
		/// </summary>
		BorderStyleRowSelector			= 349,

		/// <summary>
		/// Property id identifying RowLayout property.
		/// </summary>
		RowLayout						= 350,

		/// <summary>
		/// Property id identifying RowLayouts property.
		/// </summary>
		RowLayouts						= 351,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.AllowRowLayoutCellSizing"/> and <see cref="Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo.AllowCellSizing"/> properties.
		/// </summary>
		AllowRowLayoutCellSizing		= 352,
		
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.AllowRowLayoutLabelSizing"/> and <see cref="Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo.AllowLabelSizing"/> properties.
		/// </summary>
		AllowRowLayoutLabelSizing		= 353,

		/// <summary>
		/// Property id identifying <see cref="UltraGridBand.RowLayoutLabelStyle"/> property.
		/// </summary>
		RowLayoutLabelStyle				= 354,

		/// <summary>
		/// Property id identifying RowLayoutColumnInfo property.
		/// </summary>
		RowLayoutColumnInfo				= 355,

		#endregion // Row Layout Code

		/// <summary>
		/// Property id identifying CellDisplayStyle property.
		/// </summary>
		CellDisplayStyle				= 356,

		/// <summary>
		/// Property id identifying ColumnScrollbarSmallChange property.
		/// </summary>
		ColumnScrollbarSmallChange		= 357,

		// SSP 5/20/03 - Fixed headers
		//
		#region Fixed headers property ids

		/// <summary>
		/// Property id identifying Fixed property.
		/// </summary>
		Fixed							= 358,

		/// <summary>
		/// Property id identifying UseFixedHeaders property.
		/// </summary>
		UseFixedHeaders					= 359,

		/// <summary>
		/// Property id identifying FixedHeaderIndicator property.
		/// </summary>
		FixedHeaderIndicator			= 360,

		/// <summary>
		/// Property id identifying FixedCellSeparatorColor property.
		/// </summary>
		FixedCellSeparatorColor			= 361,

		/// <summary>
		/// Property id identifying FixedHeaderAppearance property.
		/// </summary>
		FixedHeaderAppearance			= 362,

		/// <summary>
		/// Property id identifying FixedCellAppearance property.
		/// </summary>
		FixedCellAppearance				= 363,

		/// <summary>
		/// Property id identifying FixedHeaderOnImage property.
		/// </summary>
		FixedHeaderOnImage				= 364,

		/// <summary>
		/// Property id identifying FixedHeaderOffImage property.
		/// </summary>
		FixedHeaderOffImage				= 365,

		#endregion // Fixed headers property ids

		// SSP 8/1/03 UWG1662
		// We need to expose a public ReadOnly property that the user can set to make 
		// the UltraCombo read-only.
		//
		/// <summary>
		/// Property id identifying ReadOnly property.
		/// </summary>
		ReadOnly						= 366,

		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
		// appearances.
		//
		/// <summary>
		/// Property id identifying RowFilterAction	property.
		/// </summary>
		RowFilterAction					= 367,

		/// <summary>
		/// Property id identifying FilteredOutRowAppearance property.
		/// </summary>
		FilteredOutRowAppearance		= 368,

		/// <summary>
		/// Property id identifying FilteredOutCellAppearance property.
		/// </summary>
		FilteredOutCellAppearance		= 369,

		/// <summary>
		/// Property id identifying FilteredInRowAppearance property.
		/// </summary>
		FilteredInRowAppearance			= 370,

		/// <summary>
		/// Property id identifying FilteredIntCellAppearance property.
		/// </summary>
		FilteredInCellAppearance		= 371,

		// SSP 11/7/03 - Add Row Feature
		//
		/// <summary>
		/// Property id identifying AddRowAppearance property.
		/// </summary>
		AddRowAppearance				= 372,

		/// <summary>
		/// Property id identifying TemplateAddRowAppearance property.
		/// </summary>
		TemplateAddRowAppearance		= 373,

		/// <summary>
		/// Property id identifying TemplateAddRowSpacingAfter property.
		/// </summary>
		TemplateAddRowSpacingAfter		= 374,

		/// <summary>
		/// Property id identifying TemplateAddRowSpacingBefore property.
		/// </summary>
		TemplateAddRowSpacingBefore		= 375,

		/// <summary>
		/// Property id identifying BorderStyleTemplateAddRow property.
		/// </summary>
		BorderStyleTemplateAddRow		= 376,

		/// <summary>
		/// Property id identifying DefaultCellValue property.
		/// </summary>
		DefaultCellValue				= 377,

		// SSP 11/24/03 - Scrolling Till Last Row Visible
		// Added the functionality to not allow the user to scroll any further once the
		// last row is visible.
		//
		/// <summary>
		/// Property id identifying ScrollBounds property.
		/// </summary>
		ScrollBounds					= 378,

		// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
		// Added CardScrollbars property to card settings object.
		//
		/// <summary>
		/// Property id identifying CardScrollbars property.
		/// </summary>
		CardScrollbars					= 379,

		// SSP 11/25/03 UWG2013 UWG2553
		// Added MinRowHeight property to Override to allow for unrestricted control over the 
		// row heights.
		//
		/// <summary>
		/// Property id identifying MinRowHeight property.
		/// </summary>
		MinRowHeight					= 380,

		// SSP 12/1/03 UWG2641
		// Added CellInsets and LabelInsets properties.
		//
		/// <summary>
		/// Property id identifying CellInsets property.
		/// </summary>
		CellInsets						= 381,

		/// <summary>
		/// Property id identifying LabelInsets property.
		/// </summary>
		LabelInsets						= 382,

		// SSP 12/11/03 UWG2781
		// Added UseEditorMaskSettings property.
		//
		/// <summary>
		/// Property id identifying UseEditorMaskSettings property.
		/// </summary>
		UseEditorMaskSettings			= 383,

		// SSP 12/11/03 UWG2766
		// Added AddRowCellAppearance and TemplateAddRowCellAppearance properties.
		//
		/// <summary>
		/// Property id identifying AddRowCellAppearance property.
		/// </summary>
		AddRowCellAppearance			= 384,

		/// <summary>
		/// Property id identifying TemplateAddRowCellAppearance property.
		/// </summary>
		TemplateAddRowCellAppearance	= 385,

		// SSP 12/12/03 DNF135
		// Added a way to control whether ink buttons get shown.
		// Added ShowInkButton property.
		//
		/// <summary>
		/// Property id identifying ShowInkButton property.
		/// </summary>
		ShowInkButton					= 386,

		// SSP 1/5/04 UWG1606
		// Added appearances for the splitter bars.
		//
		/// <summary>
		/// Property id identifying SplitterBarHorizontalAppearance property.
		/// </summary>
		SplitterBarHorizontalAppearance = 387,

		/// <summary>
		/// Property id identifying SplitterBarVerticalAppearance property.
		/// </summary>
		SplitterBarVerticalAppearance   = 388,

		// SSP 4/2/04 UWG3046
		// Added CharacterCasing property.
		//
		/// <summary>
		/// Property id identifying <see cref="UltraCombo.CharacterCasing"/> property or <see cref="UltraGridColumn.CharacterCasing"/>.
		/// </summary>
		CharacterCasing					= 389,

		// SSP 4/13/04 - Virtual Binding
		// Added LoadStyle property to UltraGridLayout.
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.LoadStyle"/> property.
		/// </summary>
		LoadStyle						= 390,

		// SSP 6/29/04 - UltraCalc
		// Added Formula and FormulaErrorValue properties.
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.Formula"/> property.
		/// </summary>
		Formula							= 391,

		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.FormulaErrorValue"/> property.
		/// </summary>
		FormulaErrorValue				= 392,

		// SSP 7/6/04 - UltraCalc
		// Added FormulaErrorAppearance.
		//
		/// <summary>
		/// Property id identifying FormulaErrorAppearance property.
		/// </summary>
		FormulaErrorAppearance		= 393,

		// MRS 9/14/04
		// Added a property to control whether or not the grid displays 
		// a string in a cell while calculating
		//
		/// <summary>
		/// Property id identifying ShowCalculatingText property.
		/// </summary>
		ShowCalculatingText					= 394,

		// SSP 10/17/04
		// Added FormulaRowIndexSource property on the override.
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.FormulaRowIndexSource"/> property.
		/// </summary>
		FormulaRowIndexSource			= 395,
		
		// MRS 10/22/04 - UWG3633
		// Added MaxLength property to UltraCombo.
		//
		/// <summary>
		/// MaxLength
		/// </summary>
		MaxLength = 396,

		// SSP 11/3/04 - Merged Cell Feature
		//
		/// <summary>
		/// Property id identifying MergedCellAppearance property.
		/// </summary>
		MergedCellAppearance  = 397,

		/// <summary>
		/// Property id identifying MergedCellStyle property.
		/// </summary>
		MergedCellStyle		  = 398,

		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.MergedCellEvaluationType"/> property.
		/// </summary>
		MergedCellEvaluationType = 399,

		/// <summary>
		/// Property id identifying MergedCellContentArea property.
		/// </summary>
		MergedCellContentArea = 400,

		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.MergedCellEvaluator"/> property.
		/// </summary>
		MergedCellEvaluator	  = 401,

		/// <summary>
		/// Property id identifying LimitToList property.
		/// </summary>
		LimitToList	  = 402,

		// SSP 11/16/04
		// Implemented column sizing using cells in non-rowlayout mode. 
		// Added ColumnSizingArea property to the override.
		//
		/// <summary>
		/// Property id identifying ColumnSizingArea property.
		/// </summary>
		ColumnSizingArea	  = 403,

		//JDN 11/19/04 Added RowSelectorHeader
		/// <summary>
		/// property id identifying RowSelectorHeaderAppearance property
		/// </summary>
		RowSelectorHeaderAppearance     = 404,

		//JDN 11/19/04 Added RowSelectorHeader
		/// <summary>
		/// property id identifying RowSelectorHeaderStyle property
		/// </summary>
		RowSelectorHeaderStyle          = 405,

		// SSP 12/14/04 - IDataErrorInfo Support
		//
		/// <summary>
		/// Property id identifying SupportDataErrorInfo property.
		/// </summary>
		SupportDataErrorInfo			= 406,

		/// <summary>
		/// Property id identifying DataErrorRowAppearance property.
		/// </summary>
		DataErrorRowAppearance			= 407,

		/// <summary>
		/// Property id identifying DataErrorCellAppearance property.
		/// </summary>
		DataErrorCellAppearance			= 408,

		/// <summary>
		/// Property id identifying DataErrorRowSelectorAppearance property.
		/// </summary>
		DataErrorRowSelectorAppearance	= 409,

		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		/// <summary>
		/// Property id identifying GroupByRowSpacingBefore property.
		/// </summary>
		GroupByRowSpacingBefore			= 410,

		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		/// <summary>
		/// Property id identifying GroupByRowSpacingAfter property.
		/// </summary>
		GroupByRowSpacingAfter			= 411,

		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		/// <summary>
		/// The <see cref="UltraGridPrintDocument.ColumnClipMode"/> property
		/// </summary>
		ColumnClipMode					= 412,

		/// <summary>
		/// The <see cref="UltraGridPrintDocument.FitWidthToPages"/> property
		/// </summary>
		FitWidthToPages					= 413,

		/// <summary>
		/// The <see cref="UltraGridPrintDocument.RowProperties"/> property
		/// </summary>
		RowProperties					= 414,

		/// <summary>
		/// The <see cref="UltraGridPrintDocument.Grid"/> property
		/// </summary>
		Grid							= 415,

		// SSP 3/14/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		// --------------------------------------------------------------------------
		/// <summary>
		/// Property id identifying <b>SummaryDisplayArea</b> property.
		/// </summary>
		SummaryDisplayArea				= 416,

		/// <summary>
		/// Property id identifying <b>FixedRows</b> property.
		/// </summary>
		FixedRows						= 417,

		/// <summary>
		/// Property id identifying <b>FilterRow</b> property.
		/// </summary>
		FilterRow						= 418,

		/// <summary>
		/// Property id identifying <b>SpecialRowPromptField</b> property.
		/// </summary>
		SpecialRowPromptField			= 419,

		/// <summary>
		/// Property id identifying <b>FilterEvaluationTrigger</b> property.
		/// </summary>
		FilterEvaluationTrigger			= 420,

		/// <summary>
		/// Property id identifying <b>FilterCellAppearance</b> property.
		/// </summary>
		FilterCellAppearance			= 421,

		/// <summary>
		/// Property id identifying <b>FilterOperandStyle</b> property.
		/// </summary>
		FilterOperandStyle				= 422,

		/// <summary>
		/// Property id identifying <b>FilterOperatorLocation</b> property.
		/// </summary>
		FilterOperatorLocation				= 423,

		/// <summary>
		/// Property id identifying <b>FilterOperatorAppearance</b> property.
		/// </summary>
		FilterOperatorAppearance		= 424,

		/// <summary>
		/// Property id identifying <b>FilterOperatorDefaultValue</b> property.
		/// </summary>
		FilterOperatorDefaultValue		= 425,

		/// <summary>
		/// Property id identifying <b>BorderStyleFilterCell</b> property.
		/// </summary>
		BorderStyleFilterCell			= 426,

		/// <summary>
		/// Property id identifying <b>BorderStyleFilterOperator</b> property.
		/// </summary>
		BorderStyleFilterOperator		= 427,

		/// <summary>
		/// Property id identifying <b>BorderStyleFilterRow</b> property.
		/// </summary>
		BorderStyleFilterRow			= 428,

		/// <summary>
		/// Property id identifying <b>BorderStyleSpecialRowSeparator</b> property.
		/// </summary>
		BorderStyleSpecialRowSeparator	= 429,

		/// <summary>
		/// Property id identifying <b>FilterClearButtonAppearance</b> property.
		/// </summary>
		FilterClearButtonAppearance		= 430,

		/// <summary>
		/// Property id identifying <b>FilterClearButtonLocation</b> property.
		/// </summary>
		FilterClearButtonLocation = 431,

		/// <summary>
		/// Property id identifying <b>FilterCellAppearanceActive</b> property.
		/// </summary>
		FilterCellAppearanceActive		= 432,
		
		/// <summary>
		/// Property id identifying <b>FilterRowAppearance</b> property.
		/// </summary>
		FilterRowAppearance				= 433,

		/// <summary>
		/// Property id identifying <b>FilterRowAppearanceActive</b> property.
		/// </summary>
		FilterRowAppearanceActive		= 434,

		/// <summary>
		/// Property id identifying <b>FilterRowPromptAppearance</b> property.
		/// </summary>
		FilterRowPromptAppearance		= 435,

		/// <summary>
		/// Property id identifying <b>FilterRowPrompt</b> property.
		/// </summary>
		FilterRowPrompt					= 436,

		/// <summary>
		/// Property id identifying <b>FilterRowSelectorAppearance</b> property.
		/// </summary>
		FilterRowSelectorAppearance		= 437,

		/// <summary>
		/// Property id identifying <b>FilterRowSpacingAfter</b> property.
		/// </summary>
		// MD 5/5/08
		// Found while making unit tests for 8.2 - Rotated Column Headers
		// This int value is used by another enum value
		//FilterRowSpacingAfter			= 338,
		FilterRowSpacingAfter			= 438,
	
		/// <summary>
		/// Property id identifying <b>FilterRowSpacingBefore</b> property.
		/// </summary>
		// MD 5/5/08
		// Found while making unit tests for 8.2 - Rotated Column Headers
		// This int value is used by another enum value
		//FilterRowSpacingBefore			= 339,
		FilterRowSpacingBefore			= 439,

		/// <summary>
		/// Property id identifying <b>FilterUIType</b> property.
		/// </summary>
		FilterUIType					= 440,

		/// <summary>
		/// Property id identifying <b>FixedRowAppearance</b> property.
		/// </summary>
		FixedRowAppearance				= 441,
		
		/// <summary>
		/// Property id identifying <b>FixedRowCellAppearance</b> property.
		/// </summary>
		FixedRowCellAppearance			= 442,

		/// <summary>
		/// Property id identifying <b>FixedRowIndicator</b> property.
		/// </summary>
		FixedRowIndicator				= 443,

		/// <summary>
		/// Property id identifying <b>FixedRowSelectorAppearance</b> property.
		/// </summary>
		FixedRowSelectorAppearance		= 444,

		/// <summary>
		/// Property id identifying <b>FixedRowsLimit</b> property.
		/// </summary>
		FixedRowsLimit					= 445,

		/// <summary>
		/// Property id identifying <b>FixedRowSortOrder</b> property.
		/// </summary>
		FixedRowSortOrder				= 446,

		/// <summary>
		/// Property id identifying <b>FixedRowStyle</b> property.
		/// </summary>
		FixedRowStyle					= 447,

		/// <summary>
		/// Property id identifying <b>GroupBySummaryDisplayStyle</b> property.
		/// </summary>
		GroupBySummaryDisplayStyle		= 448,

		/// <summary>
		/// Property id identifying <b>SequenceFilterRow</b> property.
		/// </summary>
		SequenceFilterRow			= 449,

		/// <summary>
		/// Property id identifying <b>SequenceFixedAddRow</b> property.
		/// </summary>
		SequenceFixedAddRow				= 450,

		/// <summary>
		/// Property id identifying <b>SequenceSummaryRow</b> property.
		/// </summary>
		SequenceSummaryRow				= 451,

		/// <summary>
		/// Property id identifying <b>SpecialRowSeparator</b> property.
		/// </summary>
		SpecialRowSeparator				= 452,

		/// <summary>
		/// Property id identifying <b>SpecialRowSeparatorAppearance</b> property.
		/// </summary>
		SpecialRowSeparatorAppearance	= 453,

		/// <summary>
		/// Property id identifying <b>SpecialRowSeparatorHeight</b> property.
		/// </summary>
		SpecialRowSeparatorHeight		= 454,

		/// <summary>
		/// Property id identifying <b>SummaryFooterSpacingAfter</b> property.
		/// </summary>
		SummaryFooterSpacingAfter		= 455,

		/// <summary>
		/// Property id identifying <b>SummaryFooterSpacingBefore</b> property.
		/// </summary>
		SummaryFooterSpacingBefore		= 456,

		/// <summary>
		/// Property id identifying <b>TemplateAddRowPromptAppearance</b> property.
		/// </summary>
		TemplateAddRowPromptAppearance	= 457,

		/// <summary>
		/// Property id identifying <b>TemplateAddRowPrompt</b> property.
		/// </summary>
		TemplateAddRowPrompt		= 458,

		/// <summary>
		/// Property id identifying <b>AllowFixing</b> property.
		/// </summary>
		AllowFixing						= 459,

		/// <summary>
		/// Property id identifying <b>FilterOperatorDropDownItems</b> property.
		/// </summary>
		FilterOperatorDropDownItems		= 460,

		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.FixedRowOnImage"/> property.
		/// </summary>
		FixedRowOnImage					= 461,

		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.FixedRowOffImage"/> property.
		/// </summary>
		FixedRowOffImage				= 462,
		// --------------------------------------------------------------------------

		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		// Added AutoFitStyle.
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.AutoFitStyle"/> property.
		/// </summary>
		AutoFitStyle					= 470,

		// JAS 2005 v2 XSD Support
		// --------------------------------------------------------------------------
		/// <summary>
		/// Property id identifying <see cref="UltraGridBand.MaxRows"/> property.
		/// </summary>
		MaxRows							= 471,

		/// <summary>
		/// Property id identifying <see cref="UltraGridBand.MinRows"/> property.
		/// </summary>
		MinRows							= 472,

		// I needed to append the word 'Column' to the end of this ID because this enum
		// already contains a value named 'MaxLength'.
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.MaxLength"/> property.
		/// </summary>
		MaxLengthColumn					= 473,

		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.MaxValueExclusive"/> property.
		/// </summary>
		MaxValueExclusive				= 474,

		// I appended the word 'Column' to the end of this ID because this enum
		// so that it has a similar name to the 'MaxLengthColumn' value.
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.MinLength"/> property.
		/// </summary>
		MinLengthColumn					= 475,

		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.MinValueExclusive"/> property.
		/// </summary>
		MinValueExclusive				= 476,

		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.RegexPattern"/> property.
		/// </summary>
		RegexPattern					= 477,
		// --------------------------------------------------------------------------

		// JAS 2005 v2 GroupBy Row Extensions
		//
		// --------------------------------------------------------------------------
		/// <summary>
		/// Property id identifying <see cref="UltraGridBand.IndentationGroupByRow"/> property.
		/// </summary>
		IndentationGroupByRow			= 478,

		/// <summary>
		/// Property id identifying <see cref="UltraGridBand.IndentationGroupByRowExpansionIndicator"/> property.
		/// </summary>
		IndentationGroupByRowExpansionIndicator = 479,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.GroupByRowExpansionStyle"/> property.
		/// </summary>
		GroupByRowExpansionStyle		= 480,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.GroupByRowInitialExpansionState"/> property.
		/// </summary>
		GroupByRowInitialExpansionState	= 481,
		// --------------------------------------------------------------------------

		// SSP 3/30/05 NAS 5.2 Row Numbers
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.RowSelectorNumberStyle"/> property.
		/// </summary>
		RowSelectorNumberStyle			= 482,

		// MRS 4/5/05 - Added properties for RowLayout column moving and span sizing
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.AllowRowLayoutColMoving"/> property.
		/// </summary>
		AllowRowLayoutColMoving = 483,
		
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.AllowRowLayoutCellSpanSizing"/> property.
		/// </summary>
		AllowRowLayoutCellSpanSizing = 484,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.AllowRowLayoutLabelSpanSizing"/> property.
		/// </summary>
		AllowRowLayoutLabelSpanSizing = 485,

		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.CaptionVisible"/> property.
		/// </summary>
		CaptionVisible					= 486,


		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.HeaderStyle"/> property.
		/// </summary>
		HeaderStyle						= 487,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.HeaderStyle"/> property.
		/// </summary>
		RowSelectorStyle				= 488,

		// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.NewColumnLoadStyle"/> property.
		/// </summary>
		NewColumnLoadStyle				= 489,

		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.NewBandLoadStyle"/> property.
		/// </summary>
		NewBandLoadStyle				= 490,

		// JAS v5.2 Wrapped Header Text 4/18/05
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.WrapHeaderText"/> property.
		/// </summary>
		WrapHeaderText					= 491,

		// JAS v5.2 GroupBy Break Behavior 5/3/05
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.GroupByMode"/> property.
		/// </summary>
		GroupByMode						= 492,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.GroupBySummaryValueAppearance"/> property.
		/// </summary>
		GroupBySummaryValueAppearance	= 493,

		// JAS v5.2 GroupBy Break Behavior 5/4/05
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.SortComparisonType"/> and <see cref="UltraGridColumn.SortComparisonType"/> properties.
		/// </summary>
		SortComparisonType				= 494,

		// SSP 5/9/05 - NAS 5.2 Filter Row
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.FilterOperatorsValueList"/> property.
		/// </summary>
		FilterOperatorsValueList		= 495,

		// SSP 5/16/05 - NAS 5.2 Filter Row
		//
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.FilterComparisonType"/> and <see cref="UltraGridColumn.FilterComparisonType"/> property.
		/// </summary>
		FilterComparisonType			= 496,

		// SSP 6/8/05 - NAS 5.2 Filter Row
		// Added FilterClearButtonVisible on the column for greater flexibility. This is equivalent
		// of the Override's FilterClearButtonLocation.
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.FilterClearButtonVisible"/> property.
		/// </summary>
		FilterClearButtonVisible		= 497,

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// 
		// ----------------------------------------------------------------------------------
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.ExcludeFromColumnChooser"/> and <see cref="UltraGridBand.ExcludeFromColumnChooser"/> property.
		/// </summary>
		ExcludeFromColumnChooser		= 498,

		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.ColumnChooserCaption"/> property.
		/// </summary>
		ColumnChooserCaption			= 499,

		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.ColumnChooserEnabled"/> property.
		/// </summary>
		ColumnChooserEnabled			= 500,
		// ----------------------------------------------------------------------------------

		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// 
		// ----------------------------------------------------------------------------------
		/// <summary>
		/// Property id identifying <see cref="Infragistics.Win.UltraWinGrid.EmptyRowSettings.EmptyAreaAppearance"/> property.
		/// </summary>
		EmptyAreaAppearance				= 501,

		/// <summary>
        /// Property id identifying <see cref="Infragistics.Win.UltraWinGrid.EmptyRowSettings.ShowEmptyRows"/> property.
		/// </summary>
		ShowEmptyRows					= 502,

		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.EmptyRowSettings"/> property.
		/// </summary>
		EmptyRowSettings				= 503,
		// ----------------------------------------------------------------------------------

		// SSP 7/14/05 - NAS 5.3 Header Placement
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.HeaderPlacement"/> property.
		/// </summary>
		HeaderPlacement					= 504,

		// SSP 7/28/05 - Header, Row, Summary Tooltips
		// 
		/// <summary>
		/// Property id identifying ToolTipText property.
		/// </summary>
		ToolTipText						= 505,

		// SSP 7/29/05 - NAS 5.3 Tab Index
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.TabIndex"/> property.
		/// </summary>
		TabIndex						= 506,

		// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.InvalidValueBehavior"/> property.
		/// </summary>
		InvalidValueBehavior			= 507,

		// SSP 8/7/05 - NAS 5.3 Hottracking
		// 
		// ----------------------------------------------------------------------
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.HotTrackCellAppearance"/> property.
		/// </summary>
		HotTrackCellAppearance			= 508,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.HotTrackHeaderAppearance"/> property.
		/// </summary>
		HotTrackHeaderAppearance		= 509,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.HotTrackRowAppearance"/> property.
		/// </summary>
		HotTrackRowAppearance			= 510,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.HotTrackRowCellAppearance"/> property.
		/// </summary>
		HotTrackRowCellAppearance		= 511,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.HotTrackRowSelectorAppearance"/> property.
		/// </summary>
		HotTrackRowSelectorAppearance	= 512,
		// ----------------------------------------------------------------------

		// SSP 8/17/05 BR05463
		// Added RowLayoutCellNavigationVertical property.
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.RowLayoutCellNavigationVertical"/> property.
		/// </summary>
		RowLayoutCellNavigationVertical		= 513,

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		// ----------------------------------------------------------------------
		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.ClipboardCellDelimiter"/> property.
		/// </summary>
		ClipboardCellDelimiter  = 514,

		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.ClipboardCellSeparator"/> property.
		/// </summary>
		ClipboardCellSeparator	= 515,

		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.ClipboardRowSeparator"/> property.
		/// </summary>
		ClipboardRowSeparator	= 516,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.AllowMultiCellOperations"/> property.
		/// </summary>
		AllowMultiCellOperations = 517,
		// ----------------------------------------------------------------------

		// SSP 1/3/06 BR08523
		// We have DisplayFormat property, but we don't have the associated format provider property.
		// So added DisplayFormatProvider property.
		// 
		/// <summary>
        /// Property id identifying <see cref="Infragistics.Win.UltraWinGrid.SummarySettings.DisplayFormatProvider"/> property.
		/// </summary>
		DisplayFormatProvider	= 518,

		//	BF 2/15/06	BR07876
		/// <summary>
		/// Property id identifying <see cref="UltraCombo.AlwaysInEditMode"/> property.
		/// </summary>
		AlwaysInEditMode					= 519,

		// SSP 2/17/06 BR08864
		// Added IgnoreMultiCellOperation property on the column.
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.IgnoreMultiCellOperation"/> property.
		/// </summary>
		IgnoreMultiCellOperation	= 520,

		// SSP 3/3/06 BR10430
		// Added TipStyleHeader property.
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.TipStyleHeader"/> property.
		/// </summary>
		TipStyleHeader			= 521,

		//MRS 4/18/06 - BR11468
		/// <summary>
		/// Property id identifying the <see cref="UltraDropDownBase.DropDownSearchMethod"/> property.
		/// </summary>
		DropDownSearchMethod	= 522,

		//MRS 4/18/06 - BR11468
		/// <summary>
		/// Property id identifying the <see cref="UltraDropDownBase.EditAreaDisplayStyle"/> property.
		/// </summary>
		EditAreaDisplayStyle	= 523,

		//MRS 4/18/06 - BR11468
		/// <summary>
		/// Property id identifying the <see cref="UltraCombo.AllowNull"/> property.
		/// </summary>
		AllowNull = 524,

		// SSP 4/5/06 - NAS 6.2 - Support for New SpellCheck functionality 
		// Added SpellChecker property.
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridColumn.SpellChecker"/> property.
		/// </summary>
		SpellChecker			= 525,

		// SSP 8/26/06 - NAS 6.3
		// Added ReadOnlyCellAppearance to the override.
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.ReadOnlyCellAppearance"/> property.
		/// </summary>
		ReadOnlyCellAppearance	= 526,

		// SSP 8/28/06 - NAS 6.3
		// Added CellHottrackInvalidationStyle property on the layout.
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridLayout.CellHottrackInvalidationStyle"/> property.
		/// </summary>
		CellHottrackInvalidationStyle = 527,

        // MBS 11/15/06 - NAS7.1
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.ValueBasedAppearance"/> property.
        /// </summary>
        ValueBasedAppearance = 528,

		// SSP 10/19/07 BR25706 BR25707
		// Added ActiveRowCellAppearance. This is to let one be able to specify the 
		// appearance for cell of the active row.
		// 
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.ActiveRowCellAppearance"/> property.
		/// </summary>
		ActiveRowCellAppearance = 529,

        // MBS 1/21/08 - BR29252
        /// <summary>
        /// Property id identifying <see cref="UltraGridLayout.UseOptimizedDataResetMode"/> property.
        /// </summary>
        UseOptimizedDataResetMode = 530,

        // MBS 2/20/08 - RowEditTemplate NA2008 V2
        /// <summary>
        /// Property id identifying the <see cref="UltraGridBand.RowEditTemplate"/> property.
        /// </summary>
        RowEditTemplate = 531,
        //
        /// <summary>
        /// Property id identifying the <see cref="UltraGridOverride.RowEditTemplateUIType"/> property.
        /// </summary>
        RowEditTemplateUIType = 532,

        // MRS NAS v8.2 - CardView Printing
        /// <summary>
        /// Property id identifying <see cref="UltraGridLayout.AllowCardPrinting"/> property.
        /// </summary>
        AllowCardPrinting = 533,

        //  BF 2/28/08  FR09238 - AutoCompleteMode
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo">UltraCombo</see> control's <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.AutoCompleteMode">AutoCompleteMode</see> property,
        /// and the <see cref="Infragistics.Win.UltraWinGrid.UltraGridColumn">UltraGridColumn</see> class' <see cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.AutoCompleteMode">AutoCompleteMode</see> property.
        /// </summary>
        AutoCompleteMode = 534,

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		/// <summary>
		/// Property id identifying <see cref="HeaderBase.TextOrientation"/> property.
		/// </summary>
		TextOrientation = 535,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.ColumnHeaderTextOrientation"/> property.
		/// </summary>
		ColumnHeaderTextOrientation = 536,

		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.ColumnHeaderTextOrientation"/> property.
		/// </summary>
		GroupHeaderTextOrientation = 537,

        // MRS NAS v8.2 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGrid.ExitEditModeOnLeave"/> property.
        /// </summary>
        ExitEditModeOnLeave = 538,

        /// <summary>
        /// Property id identifying <see cref="UltraGrid.RowUpdateCancelAction"/> property.
        /// </summary>
        RowUpdateCancelAction = 539,

        /// <summary>
        /// Property id identifying <see cref="UltraGrid.SyncWithCurrencyManager"/> property.
        /// </summary>
        SyncWithCurrencyManager = 540,

        /// <summary>
        /// Property id identifying <see cref="UltraGridLayout.UseScrollWindow"/> property.
        /// </summary>
        UseScrollWindow = 541,

        /// <summary>
        /// Property id identifying <see cref="UltraGridLayout.MaxBandDepth"/> property.
        /// </summary>
        MaxBandDepth = 542, 

        // CDS 04/14/08 NA 8.2 ImeMode
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.ImeMode"/> property.
        /// </summary>
        ImeMode = 543,

		// MD 5/5/08
		// Found while making unit tests for 8.2 - Rotated Column Headers
		// There was no enum value for the MultiCellSelectionMode property
		/// <summary>
		/// Property id identifying <see cref="UltraGridOverride.MultiCellSelectionMode"/> property.
		/// </summary>
		MultiCellSelectionMode = 544,

        // MBS 5/6/08 - RowEditTemplate NA2008 V2
        /// <summary>
        /// Property id identifying <see cref="UltraGridOverride.RowEditTemplateRowSelectorImage"/> property.
        /// </summary>
        RowEditTemplateRowSelectorImage = 545,

        // MRS NAS v8.3 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.MinValue"/> property.
        /// </summary>
        MinValue = 546,

        // MRS NAS v8.3 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.MaxValue"/> property.
        /// </summary>
        MaxValue = 547,

        // MRS NAS v8.3 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.GroupByEvaluator"/> property.
        /// </summary>
        GroupByEvaluator = 548,

        // MRS NAS v8.3 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.SortComparer"/> property.
        /// </summary>
        SortComparer = 549,

        // MRS NAS v8.3 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.GroupByComparer"/> property.
        /// </summary>
        GroupByComparer = 550,

        // MRS NAS v8.3 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.RowFilterComparer"/> property.
        /// </summary>
        RowFilterComparer = 551,

        // MRS NAS v8.3 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.Editor"/> property.
        /// </summary>
        Editor = 552,

        // MRS NAS v8.3 - Unit Testing
        /// <summary>
        /// Property id identifying <see cref="UltraGridColumn.FormulaValueConverter"/> property.
        /// </summary>
        FormulaValueConverter = 553,

		// MD 9/23/08 - TFS6601
		/// <summary>
		/// Property id identifying ReserveSortIndicatorSpaceWhenAutoSizing property.
		/// </summary>
		ReserveSortIndicatorSpaceWhenAutoSizing = 554,

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout
        /// <summary>
        /// Property id identifying <see cref="Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo.ParentGroup"/> property.
        /// </summary>
        ParentGroup = 555,

        // Added this as the result of a unit test which uncovered a bug. 
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo.AllowCellSizing"/> property.
        /// </summary>
        AllowCellSizing = 556,

        /// <summary>
        /// Property id identifying <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand.RowLayoutStyle"/> property.
        /// </summary>
        RowLayoutStyle = 557,

        #endregion //NAS 9.1 - Groups in RowLayout

		// MD 12/8/08 - 9.1 - Column Pinning Right
		/// <summary>
		/// Property id identifying <see cref="Infragistics.Win.UltraWinGrid.HeaderBase.FixOnRight"/> property.
		/// </summary>
		FixOnRight = 558,

		/// <summary>
		/// Property id identifying <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.FixHeadersOnRight"/> property.
		/// </summary>
		FixHeadersOnRight = 559,

        // MBS 12/4/08 - NA9.1 Excel Style Filtering
        /// <summary>
        /// Property id intentifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.FilterUIProvider"/> property.
        /// </summary>
        FilterUIProvider = 560,

        //  BF 11/25/08 NA 9.1 - UltraCombo MultiSelect
		/// <summary>
		/// Property identifier for the <see cref="Infragistics.Win.UltraWinGrid.CheckedListSettings.CheckStateMember">CheckStateMember</see> property.
		/// </summary>
		CheckStateMember = 561,

        // CDS NAS v9.1 Header CheckBox
        /// <summary>
        /// Property id identifying HeaderCheckBoxVisibility property.
        /// </summary>
        HeaderCheckBoxVisibility = 562,

        // CDS NAS v9.1 Header CheckBox
        /// <summary>
        /// Property id identifying HeaderCheckBoxAlignment property.
        /// </summary>
        HeaderCheckBoxAlignment = 563,

        // CDS NAS v9.1 Header CheckBox
        /// <summary>
        /// Property id identifying HeaderCheckBoxSynchronization property.
        /// </summary>
        HeaderCheckBoxSynchronization = 564,

        //  BF 2/10/09  TFS13731
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.CheckedListSettings">CheckedListSettings</see> property.
        /// </summary>
        CheckedListSettings = 565,

        // MBS 3/31/09 - NA9.2 CellBorderColor
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.ActiveCellBorderThickness">ActiveCellBorderThickness</see> property.
        /// </summary>
        ActiveCellBorderThickness = 566,

        // MBS 4/6/09 - NA9.2 SelectionOverlay
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.SelectionOverlayColor">SelectionOverlayColor</see> property.
        /// </summary>
        SelectionOverlayColor = 567,
        //
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.SelectionOverlayBorderColor">SelectionOverlayBorderColor</see> property.
        /// </summary>
        SelectionOverlayBorderColor = 568,
        //
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.SelectionOverlayBorderThickness">SelectionOverlayBorderThickness</see> property.
        /// </summary>
        SelectionOverlayBorderThickness = 569,

        // CDS 9.2 Column Moving Indicators
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.DragDropIndicatorSettings">DragDropIndicatorSettings</see> property.
        /// </summary>
        DragDropIndicatorSettings = 570,

        // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.GroupByRowConnectorAppearance">GroupByRowConnectorAppearance</see> property.
        /// </summary>
        GroupByRowConnectorAppearance = 571,

        // CDS 9.2 RowSelector State-Specific Images
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.RowSelectorImageInfo.ActiveRowImage">ActiveRowImage</see> property.
        /// </summary>
        RowSelectorActiveRowImage = 572,

        // CDS 9.2 RowSelector State-Specific Images
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.RowSelectorImageInfo.DataChangedImage">DataChangedImage</see> property.
        /// </summary>
        RowSelectorDataChangedImage = 573,

        // CDS 9.2 RowSelector State-Specific Images
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.RowSelectorImageInfo.AddNewRowImage">AddNewRowImage</see> property.
        /// </summary>
        RowSelectorAddNewRowImage = 574,

        // CDS 9.2 RowSelector State-Specific Images
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.RowSelectorImageInfo.ActiveAndDataChangedImage">ActiveAndDataChangedImage</see> property.
        /// </summary>
        RowSelectorActiveAndDataChangedImage = 575,

        // CDS 9.2 RowSelector State-Specific Images
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.RowSelectorImageInfo.ActiveAndAddNewRowImage">ActiveAndAddNewRowImage</see> property.
        /// </summary>
        RowSelectorActiveAndAddNewRowImage = 576,

        // CDS 9.2 RowSelector State-Specific Images
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.RowSelectorImages">RowSelectorImages</see> property.
        /// </summary>
        RowSelectorImages = 577,

        // MBS 6/19/09 - TFS18639
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.FilterOperandDropDownItems">FilterOperandDropDownItems</see> property.
        /// </summary>
        FilterOperandDropDownItems = 578,

        // CDS 6/25/09 9.2 UltraCombo DropDownButtonDisplayStyle
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.DropDownButtonDisplayStyle">DropDownButtonDisplayStyle</see> property.
        /// </summary>
        DropDownButtonDisplayStyle = 579,

        // MBS 7/1/09 - NA9.2 ActiveCellHeaderAppearances
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.ActiveCellRowSelectorAppearance">ActiveCellRowSelectorAppearance</see> property.
        /// </summary>
        ActiveCellRowSelectorAppearance = 580,
        //
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.ActiveCellColumnHeaderAppearance">ActiveCellColumnHeaderAppearance</see> property.
        /// </summary>
        ActiveCellColumnHeaderAppearance = 581,

        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled, SelectedAppearancesEnabled, DefaultSelectedBackColor & DefaultSelectedForeColor)
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.ActiveAppearancesEnabled">ActiveAppearancesEnabled</see> property.
        /// </summary>
        ActiveAppearancesEnabled = 582,
        //
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.SelectedAppearancesEnabled">SelectedAppearancesEnabled</see> property.
        /// </summary>
        SelectedAppearancesEnabled = 583,
        //
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.DefaultSelectedForeColor">DefaultSelectedForeColor</see> property.
        /// </summary>
        DefaultSelectedBackColor = 584,
        //
        /// <summary>
        /// Property id identifying the <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.DefaultSelectedForeColor">DefaultSelectedForeColor</see> property.
        /// </summary>
        DefaultSelectedForeColor = 585,   

        // MBS 8/7/09 - TFS18607
        /// <summary>
        /// Property id identifying the <see cref="UltraGridOverride.AddRowEditNotificationInterface"/> property
        /// </summary>
        AddRowEditNotificationInterface = 586,
	}
}
