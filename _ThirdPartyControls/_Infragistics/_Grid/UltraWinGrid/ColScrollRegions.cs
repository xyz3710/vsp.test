#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Shared.Serialization;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Security.Permissions;


    /// <summary>
    /// Collection of all column scrolling regions in a layout
    /// </summary>
    [ Serializable()]
	[ TypeConverter( typeof( ExpandableObjectConverter ) ) ]
    sealed public class ColScrollRegionsCollection : ScrollRegionsCollectionBase,
											  ISerializable
    {
        private bool metricsDirty;
        private bool initializingMetrics;
		private int initialCapacity = 5;

		internal ColScrollRegionsCollection( UltraGridLayout layout ) : base ( layout ) 
        {
        }

        /// <summary>
        /// Add method required during de-serialization. 
        /// </summary>
        /// <param name="obj">The object to add to the collection.</param>
        /// <returns>The position into which the new element was inserted.</returns>
        /// <remarks>Should not be called outside InitializeComponent.</remarks>
		[ Browsable( false ) ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ] 
		public int Add( object obj )
		{
			if ( obj == null )
				throw new ArgumentNullException();

			if ( ! ( obj is ColScrollRegion ) )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_58"));

			if ( ((ColScrollRegion)obj).Collection != null )
				throw new NotSupportedException();

			((ColScrollRegion)obj).InitCollection( this );

			return this.AddScrollRegionHelper( (ColScrollRegion)obj );
		}

		/// <summary>
		/// overridden property that indicates whether this collection is
		/// read-only
		/// </summary>
        public override bool IsReadOnly { get { return true; } }

		/// <summary>
		/// ColScrollRegion Indexer
		/// </summary>
        public Infragistics.Win.UltraWinGrid.ColScrollRegion this[ int index ]
        {
            get
            {
                return (ColScrollRegion)this.List[ index ];
            }
        }

//#if DEBUG
		/// <summary>
		///  Clears the collection
		/// </summary>
//#endif
		// SSP 5/19/04
		// Made Clear method public. We need this for design time serialization to work 
		// properly. More specifically, when a transaction gets canceled, the designer
		// looks for a Clear method to clear the collection and then add the items
		// it contained using Add method. However if Clear is not public, it simply
		// adds the items so items end up being duplicated every time a transaction
		// is canceled. Also exposing Clear method to the end users is not bad either.
		//
		//internal void Clear()
		public void Clear( )
		{
        
			foreach ( ColScrollRegion csr in this )
			{
				// remove the prop change notifications
				//
				csr.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// call dispose on the band object
				//
				csr.Dispose();
			}

			this.InternalClear();

			this.metricsDirty = true;
		}

		internal ColScrollRegionsCollection Clone()
		{
			Infragistics.Win.UltraWinGrid.ColScrollRegionsCollection clone = new ColScrollRegionsCollection(this.Layout);

			for ( int i = 0; i < this.Count; i++ )
			{
				Infragistics.Win.UltraWinGrid.ColScrollRegion csr = new ColScrollRegion( this );

				csr.InitializeFrom( this[i] );

				clone.AddScrollRegionHelper( csr );

			}

			return clone;
		}
		private int AddScrollRegionHelper( ColScrollRegion csr )
		{
			// Add a listener for property changes.
			csr.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			
			// Add the region and return the index
			int index = this.InternalAdd(csr);			

			return index;
		}

		
		internal void InitLayout( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			this.Layout = layout;
			
			for(int i = 0; i < this.Count; i++)
			{
				this[i].InitCollection(this);

				this[i].SubObjectPropChanged += this.SubObjectPropChangeHandler;				
			}
		}
 
        /// <summary>
        /// Called when this object is disposed of
        /// </summary>
        protected override void OnDispose() 
        {
			this.Clear();
        }


        /// <summary>
        /// True if the splitter box to the left of the horizontal
        /// scrollbar is visible. This property is read-only and is 
        /// determined based on the number of ColScrollRegions
        /// and the value of the layout's <b>MaxColScrollRegions</b>
        /// property.
        /// </summary>
        public bool IsSplitBoxVisible
        {
            get
            {
				//RobA UWG494 10/4/01 Check so see if we have already reached our max
				//
				return this.Count < this.Layout.MaxColScrollRegions;
                
                //return true;
            }
        }

		internal ColScrollRegion GetRegionFromOrigin( int origin )
		{
			ColScrollRegion ret = null;
			
			for ( int i = 0; i < this.Count && null == ret; i++ )
				//i != pCSR_Vec->end() && ! pCSR ;			
			{
				ColScrollRegion csr = this[i];

				if ( null == csr )
					continue;

				if ( csr.SafeOrigin <= origin &&
					( csr.SafeOrigin + csr.ClippedExtent ) >= origin )
					ret = csr;
			}

			return ret;
		}


		// SSP 11/4/04 UWG3593
		// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
		// that specifies whether to assume and if so to whether to assume the scrollbar
		// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
		// even when they are not needed because we assume the horizontal scrollbar is 
		// visible to find out if vertical scrollbar should be displayed or not.
		//
		//internal override bool AreScrollbarsVisible ( bool fAssumeColScrollbarsVisible ) 
		internal override bool AreScrollbarsVisible( ScrollbarVisibility assumeColScrollbarsVisible ) 
		{

			// iterate over the list and return true if any region
			// has a scrollbar
			//
			for ( int i=0; i < this.Count; ++i )
			{
				ColScrollRegion csr = this[i];

				if ( csr != null &&
					 csr.WillScrollbarBeShown( assumeColScrollbarsVisible ) )
					return true;
			}
    
			return false;
		}
    
		internal void OnSplitBoxDrop( Point point )
        {
            
			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
            //int splitterBarWidth  = this.Layout.SplitterBarWidth;
			int splitterBarWidth  = this.Layout.ColScrollRegionSplitterBarWidthResolved;

            foreach ( ColScrollRegion csr in this )
            {
                // if it is within an existing col scroll region
                // then split the region at that stop
                //
                if ( point.X > csr.Origin + splitterBarWidth && 
                     point.X < csr.Origin + csr.Extent - splitterBarWidth )
                {
					// AS - 2/20/02
					// We were showing a messagebox when the split box was dropped
					// on a fixed col scroll region. Instead, we are now throwing an
					// exception since the user may also call that routine from code.
					// We'll catch the exception and eat it, if needed.
					//
					try
					{
						csr.Split( point.X - csr.Origin );
					}
					catch {}
                    break;
                }
            }
        }
 
		internal void OnResize( ColScrollRegion csr, int deltaX )
        {
            
			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
            //int splitterBarWidth            = this.Layout.SplitterBarWidth;
			int splitterBarWidth            = this.Layout.ColScrollRegionSplitterBarWidthResolved;

            int newExtent                   = csr.Extent + deltaX;
            int newRight                    = csr.Origin + newExtent;
            ColScrollRegion nextRegion      = this.GetNextVisibleRegion ( csr, false );
            ColScrollRegion regionToRemove  = null;
            ColScrollRegion regionToResize  = null;


            if ( newExtent < 2 )
            {
                // since it was dragged to the left of the range delete it
                // and adjust the next region to pick up its space
                //
                regionToRemove  = csr;
                regionToResize  = nextRegion;
            }
            else
            if ( null != nextRegion &&
                 newRight >= nextRegion.Origin 
                             + nextRegion.Extent
                             
                                // + nextRegion.ClientWidth( NULL )
                             - splitterBarWidth  )
            {
                // since it was dragged to the right of the range  
                // if the next region isn't fixed delete the next region
                // adjust this region to pick up its space
                //
                if ( SizingMode.Fixed == nextRegion.SizingMode )
                {
                    // the next region is fixed so just delete
                    // the passed in region  (don't attempt to
                    // resize the next region)
                    //
                    regionToRemove  = csr;
                }
                else
                {
                    regionToRemove  = nextRegion;
                    regionToResize  = csr;
                }
            }
            else
            {
                // we are not deleting so just resize this region
                //
                regionToResize  = csr;
            }

            if ( null != regionToRemove )
            {
                // Save the lower of the 2 oriigns as the
                // new origin
                //
                int newOrigin = regionToRemove.Origin;

                if ( null != regionToResize && 
                     regionToResize.Origin < newOrigin )
                    newOrigin = regionToResize.Origin;

                // recalc the extent so that the appropriate remaining region
                // will pick up the extent of the removed region
                //
                if ( null != regionToResize )
                {
                    newExtent = regionToResize.Extent +
                                regionToRemove.Extent + 
                                splitterBarWidth;
                }

                this.Remove( regionToRemove );

                // check to make sure the above remove wasn't cancelled
                // by trying to find the region we attempted to remove
                // in the collection
                //
                int index = this.IndexOf( regionToRemove );
                
                // if the region is still there (remove was cancelled)
                // then just exit
                //
                if ( index >= 0 )
                    return;

                // Call SetOriginAndExtent to ensure that the adjacent
                // regionToResize absorbs all of the freed up real estate
                //
                if ( null != regionToResize )
                    regionToResize.SetOriginAndExtent( newOrigin, newExtent ); 

                // force an update of the UIElement which will cause
                // the metaregion rects to be re-calculated
                //
                this.Layout.UpdateUIElementRect();
            }
            else
            if ( null != regionToResize )
                this.SetExtent( regionToResize, newExtent );

        }

		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// pass the notification along to our listeners
			//
			this.metricsDirty = true;
			this.NotifyPropChange( PropertyIds.ColScrollRegions, propChange );

			if ( propChange.PropId is PropertyIds )
			{
				switch ( (PropertyIds)propChange.PropId )
				{
					case PropertyIds.Scrollbar:
					case PropertyIds.Hidden:
					case PropertyIds.Remove:
						//RobA UWG362 9/27/01
					case PropertyIds.Position:
					{
						this.Layout.DirtyGridElement();
						return;
					}
				}
			}
		}

		internal override void CheckIfSizeChanged()
		{		
			for ( int i=0; i < this.Count; ++i )
				this[i].CheckIfSizeChanged();
		}


		internal bool MetricsDirty
        {
            get
            {
                return this.metricsDirty;
            }
        }

		internal void DirtyMetrics( )
		{
			this.DirtyMetrics( false );
		}
 
		internal void DirtyMetrics( bool setScrollToBeginning )
		{
            this.metricsDirty = true;
    
            // iterate over the collection dirtying each one
            //
            for ( int i = 0; i < this.Count; i++ ) 
            {
                this[i].SetDestroyVisibleHeaders( true, setScrollToBeginning );
            }

			// SSP 7/9/02 UWG1153
			// Check for the layout being null because during deserialization,
			// it may be null.
			//
			if ( null != this.Layout )
			{
				// JAS v5.2 Wrapped Header Text 4/25/05
				//
				this.Layout.BumpColumnHeaderHeightVersionNumber();

				// JJD 3/9/00
				// Since the col regions are being recalculated we also need to 
				// reload the visible rows
				//
				this.Layout.RowScrollRegions.DirtyAllVisibleRows( setScrollToBeginning );

				// JJD 4/25/00
				// Invalidate entire grid element
				//
				this.Layout.DirtyGridElement( false );
			}
		}
		internal bool InitializingMetrics
        {
            get
            {
                return this.initializingMetrics;
            }
        }

		//static int tmp = 0;

		internal void InitializeMetrics( )
		{
			// JJD 01/07/02
			// Don't do this recursively.
			//
			if ( this.initializingMetrics || !this.metricsDirty )
				return;

			// SSP 5/26/06 - Optimizations
			// Wrap the code metrics initialization code in BeginGraphicsCaching and EndGraphicsCaching
			// so all the font size calculations that can occur during metrics calculations make use
			// of a single cached graphics object instead of potentially creating multiple of them.
			// Added new InitializeMetricsHelper method and moved the code from here into that method.
			// 
			try
			{
				DrawUtility.BeginGraphicsCaching( );

				this.InitializeMetricsHelper( );
			}
			finally
			{
				DrawUtility.EndGraphicsCaching( );
			}
		}

		private void InitializeMetricsHelper( )
		{
			//Debug.WriteLine( ++tmp + " InitializeMetrics *************************" );

            this.metricsDirty = false;

            this.initializingMetrics = true;

			// JJD 12/28/01
			// Wrap logic in try/finally so that initializingMetrics flag
			// will be guaranteed to be reset
			//
			try
			{
				// tell each col scroll region to reset its
				// exclusive metrics
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					this[i].ResetExclusiveWidth();
				}

                // MRS 3/4/2009 - TFS14874
				//this.Layout.BandsOverallExtent = this.Layout.SortedBands.CalculateBandMetrics();
                this.Layout.BandsOverallExtent = this.Layout.Bands.CalculateBandMetrics();

				// tell each col scroll region to regenerate
				// its column hedaers based on the new metrics
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					this[i].RegenerateVisibleHeaders();
				}
			}
			finally
			{
	            this.initializingMetrics = false;

				// JJD 01/05/01
				// Reset metricsDirty flag since that may have been dirtied
				// while autofitting columns to the width
				//
				if ( this.Layout.AutoFitAllColumns )
					this.metricsDirty = false;
			}

            
            // If we are in edit mode then reposition the edit ctl
            //
			// SSP 4/29/02
			// EM Embeddable editors.
			//if ( null != this.Layout.EditControl && this.Layout.EditControl.Visible )
            //
            // MBS 3/4/08 - RowEditTemplate NA2008 V2
            // When we're using a proxy to edit a cell, we don't need to bother with this
            //
			//if ( null != this.Layout.CellInEditMode )
            if (null != this.Layout.CellInEditMode && this.Layout.ActiveProxy == null)
        		this.Layout.TextControlPositionDirty = true;									
        }

		internal override void InitFirstRegion() 
        {
            if ( Count < 1 )
            {
                ColScrollRegion csr = new ColScrollRegion( this );

                this.List.Add( csr );

                // hook up the prop change notifications
                //
                csr.SubObjectPropChanged += this.SubObjectPropChangeHandler;
            }
        }
        internal void InternalInsert( ColScrollRegion csr, int insertAt ) 
        {
            this.InternalInsert( insertAt, csr );

            // hook up the prop change notifications
            //
            csr.SubObjectPropChanged += this.SubObjectPropChangeHandler;

            this.metricsDirty = true;
        }
		internal ColScrollRegion GetNextVisibleRegion ( ColScrollRegion startRegion, 
                                                      bool wrap )
        {
            if ( this.Count < 1 )
                return null;

            ColScrollRegion lastVisibleRegion = null;

            // iterate over the collection (with a reverse iterator)
            // looking for the next visible region to return
            //
            for ( int i = this.Count - 1; i >= 0 ; i-- )
            {
                // when we encounter the passed in start region we
                // want to return if we found a visible region
                // or if the fWrap flag is false 
                //
                if ( startRegion == this[i] )
                {
                    if ( null != lastVisibleRegion || !wrap )
                        return lastVisibleRegion;
                }

                // bypass hidden regions
                //      
                if ( this[i].Hidden )
                    continue;

                // keep track of the last visible region
                //
                lastVisibleRegion = this[i];
            }

            // return the last visible region we found
            //
            return lastVisibleRegion;
        }

		internal ColScrollRegion GetPreviousVisibleRegion ( ColScrollRegion startRegion, 
			bool wrap )
		{
			// SSP 8/7/02 UWG1194
			// Logic for traversing the row scroll regions (through Shift-F6 keystrokes)
			// passes in null as the startRegion to get the first visible region.
			// So we don't need to throw an exception if startRegion is null. Below logic
			// does not access the startRegion in any way.
			//
			
			
			// SSP 8/7/02 UWG1194
			// This should be checking for no scroll regions, not less than 2.
			//
			//if ( this.Count < 2 )
			if ( this.Count <= 0 )
				return null;

			ColScrollRegion lastVisibleRegion = null;

			// iterate over the collection  looking for the previous 
			// visible region to return
			//
			// SSP 8/7/02 UWG1194
			// 
			//for ( int i = 0; i < this.Count - 1; i++ )
			for ( int i = 0; i < this.Count; i++ )
			{
				// when we encounter the passed in start region we
				// want to return if we found a visible region
				// or if the fWrap flag is false 
				//
				if ( startRegion == this[i] )
				{
					if ( null != lastVisibleRegion || !wrap )
						return lastVisibleRegion;
				}

				// bypass hidden regions
				//      
				if ( this[i].Hidden )
					continue;

				// keep track of the last visible region
				//
				lastVisibleRegion = this[i];
			}

			// return the last visible region we found
			//
			return lastVisibleRegion;
		}

		internal void SetExtent( ColScrollRegion csr, int newExtent )
        {
			UltraGrid grid = this.Layout.Grid as UltraGrid;
            if ( null != grid )
            {
                // if we are already in the BeforeColResionSize or
                // BeforeColRegionSplit events return an error
                //
                if ( grid.EventManager.InProgress( GridEventIds.BeforeColRegionSize ) ||
                     grid.EventManager.InProgress( GridEventIds.BeforeColRegionSplit ) )
                {
                    throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_256"));
                }
            }

            // get a pointer to next the visible region (if there is one)
            //
            ColScrollRegion nextVisibleRegion = GetNextVisibleRegion ( csr, false );

            if ( null == nextVisibleRegion )
            {
                throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_61") );
            }

            int thisOrigin    = csr.Origin;
            int thisExtent    = csr.Extent;
            int nextOrigin    = nextVisibleRegion.Origin;
            int nextExtent    = nextVisibleRegion.Extent;

            // Check for valid value
            //
            // don't throw an error if new extent is too large. Instead, reduce it
            // to the largest allowable value
            //
            if ( newExtent < 0  ) 
        		throw new ArgumentOutOfRangeException(Shared.SR.GetString("LE_ArgumentOutOfRangeException_62"));

            // Allow enough space for the next regions scrollbar and a splitter bar
            // between regions
            //
            int maxExtent = thisExtent + nextExtent - SystemInformation.VerticalScrollBarWidth;

            if ( newExtent > maxExtent )
            {
                if ( maxExtent > 0 )
                    newExtent = maxExtent;
            }

            if ( newExtent < 0 )
                 newExtent = 0;

            // calculate the delta extent
            //
            int delta = newExtent - thisExtent;

            // set the adjusted origin and extent values
            //
            // Note: pass false in for the 3rd parameter so the scrollbar
            //       doesn't get repositioned immediately. This prevents
            //       a scrollbar flicker if the before event gets cancelled. 
            //
            csr.SetOriginAndExtent( thisOrigin, newExtent );

            nextVisibleRegion.SetOriginAndExtent( nextOrigin + delta, 
                                                  nextExtent - delta );

            // fire the before event - if cancelled return S_OK;
            //
            if ( null != this.Layout.Grid )
            {
                BeforeColRegionSizeEventArgs args = new BeforeColRegionSizeEventArgs( csr, nextVisibleRegion );
                grid.FireEvent( GridEventIds.BeforeColRegionSize, args );

                if ( args.Cancel )
                {
                    // reset the origin's and extents back to their original value
                    //
                    csr.SetOriginAndExtent( thisOrigin, 
                                            thisExtent );

                    nextVisibleRegion.SetOriginAndExtent( nextOrigin, 
                                                          nextExtent );
                }
            }

            this.Layout.DirtyGridElement();

            // reposition the scrollbars now
            //
            csr.PositionScrollbar( true );
            nextVisibleRegion.PositionScrollbar( true );

            // set the metrics dirty flag so we rebuild the visible headers collection
            // on the next paint operation
            //
            this.metricsDirty = true;

        }

        /// <summary>
        /// Remove a ColScrollRegion from a ColScrollRegions collection.
        /// </summary>
		/// <remarks>
		/// <p class="body">Use this method to remove a ColScrollRegion object from an ColScrollRegions collection.</p>
		/// <p class="body">Removing a colscrollregion from the control will cause the <b>BeforeColRegionRemoved</b> event to be generated.</p>
		/// <p class="body">To add a ColScrollRegion object to a ColScrollRegions collection, invoke the colscrollregion's <b>Split</b> method.</p>
		/// </remarks>
        public void Remove( ColScrollRegion csr )
        {
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_63"));

			// make sure there is at least one other region
            //
            if ( this.Count < 2 )
                throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_258") );

            // make sure that the csr is in the collection
            //
            int index = this.IndexOf( csr );

            if ( index >= 0 )
            {
                BeforeColRegionRemovedEventArgs args = new BeforeColRegionRemovedEventArgs( csr );

                // fire the before event to see if they want to cancel it
                //
                if ( null != this.Layout.Grid )
                    grid.FireEvent( GridEventIds.BeforeColRegionRemoved, args );

                // if cancel was set true in the event then
                // just return
                //
                if ( args.Cancel )
                    return;

                // unhook the prop change notifications
                //
                csr.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				csr.Dispose();
            }

            // remove the csr from our internal list
            //
            this.InternalRemove( csr );

            this.metricsDirty = true;

			// SSP 4/25/03 UWG2190
			// Also dirty the grid element so the elements get repositioned.
			//
			if ( null != this.Layout )
			{
				this.Layout.DirtyGridElement( true );
				this.Layout.UIElement.VerifyChildElements( true );
			}

            this.NotifyPropChange( PropertyIds.Remove );

        }

		// AS - 11/21/01
		// When resetting the collection, we were only resetting the
		// items in the collection and not clearing the collection.
		//

		/// <summary>
		/// Resets all properties back to their default values.
		/// </summary>
		public override void Reset()
		{
			// call the base implementation first
			base.Reset();

			// remove all items except the first
			for (int i = this.List.Count - 1; i > 0; i--)
			{
				ColScrollRegion csr = (ColScrollRegion)this.List[i];

				csr.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				csr.Dispose();

				this.InternalRemove( csr );
			}

			this.metricsDirty = true;

			this.NotifyPropChange( PropertyIds.Remove );
		}
     
		/// <summary>
		/// IEnumerable Interface Implementation        
		/// </summary>
        /// <returns>A type safe enumerator</returns>
        public new ColScrollRegionEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new ColScrollRegionEnumerator(this);
        }

		/// <summary>
		/// inner class implements IEnumerator interface
		/// </summary>
        public class ColScrollRegionEnumerator : DisposableObjectEnumeratorBase
        {   
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="colScrollRegions"></param>
            public ColScrollRegionEnumerator( ColScrollRegionsCollection colScrollRegions ) : base( colScrollRegions )
            {
            }

			/// <summary>
			/// non-IEnumerator version: type-safe
			/// </summary>
            public ColScrollRegion Current
            {
                get
                {
                    return (ColScrollRegion)((IEnumerator)this).Current;
                }
            }
        }
		internal ColScrollRegion DefaultActiveRegion
		{
			get
			{
				
				if ( this.Count < 1 )
				{
					Debug.Fail("No col scrolling regions in ColScrollRegionsColl::GetDefaultActiveRegion()");
					return null;
				}

				ColScrollRegion firstVisibleRegion = null;
				
				
    
				// iterate over the collection looking for the first 
				// non-locked region to return
				//
				for ( int i=0; i< this.Count; i++ )
				{
					if ( this[i].Hidden )
						continue;

					if ( null == firstVisibleRegion )
						firstVisibleRegion = this[i];

					
					// Remove allow scroll property
					//
					//        if ( (*ItemIterator)->GetAllowScroll() )
					return this[i];
				}

				// all of the regions are locked so just return the first one
				// in the collection
				//
				return firstVisibleRegion;
			}			
		}

		/// <summary>
		/// 
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return this.initialCapacity;
			}
		}


		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			//set the assembly name because of version number conflicts
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			PropertyCategories propCat = context.Context is PropertyCategories
				? (PropertyCategories)context.Context	: PropertyCategories.All;

			if( (propCat & PropertyCategories.ColScrollRegions ) == 0 )
				return;

			//all values that were set are now save into SerializationInfo

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Count", this.Count );
			//info.AddValue( "Count", this.Count );

			// add each layout in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, i.ToString(), this[i] );
				//info.AddValue( i.ToString(), this[i] );
			}

			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}

		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal ColScrollRegionsCollection( SerializationInfo info, StreamingContext context ) : base ( null )
		private ColScrollRegionsCollection( SerializationInfo info, StreamingContext context ) : base ( null )
		{
			PropertyCategories propCat = context.Context is PropertyCategories
				? (PropertyCategories)context.Context	: PropertyCategories.All;

			if( (propCat & PropertyCategories.ColScrollRegions ) == 0 )
				return;

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( ColScrollRegion ) )
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					ColScrollRegion item = (ColScrollRegion)Utils.DeserializeProperty( entry, typeof(ColScrollRegion), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd(item);
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							//this.initialCapacity = Math.Max( this.initialCapacity, (int)entry.Value );
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/17/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.initialCapacity = Math.Max(this.initialCapacity, (int)Utils.DeserializeProperty( entry, typeof(int), this.initialCapacity ));
							//this.initialCapacity = Math.Max(this.initialCapacity, (int)DisposableObject.ConvertValue( entry.Value, typeof(int) ));
							break;
						}

						// JJD 1/31/02
						// De-Serialize the tag property
						//
						case "Tag":
						{
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						}

						default:
						{
							Debug.Assert( false, "Invalid entry in BandsCollection de-serialization ctor" );
							break;
						}				
					}
				}
			}
		}


		
        internal void InitializeFrom( ColScrollRegionsCollection source, PropertyCategories propertyCategories )
		{
			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			int countdiff = this.Count - source.Count;
			//if current collection is larger than source
			if ( countdiff > 0 )
			{
				for ( int i = this.Count; i > source.Count; i-- ) 
				{
					this[i-1].Dispose();
					this.InternalRemove(i-1);
				}
			}
			//if current collection is less than source
			if ( countdiff < 0 )
			{
				for ( int i = this.Count; i < source.Count; i++ )
				{
					ColScrollRegion csr = new ColScrollRegion(this);
					this.AddScrollRegionHelper(csr);
				}
			}

			for ( int index = 0; index < this.Count; index++ )
				this[index].InitializeFrom(source[index]);
		}

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.ColScrollRegion[] array, int index)
		{
			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo
    }
}
