#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Collections.Generic;

    /// <summary>
    /// Collection of headers
    /// </summary>
    public class HeadersCollection : SubObjectsCollectionBase
    {
        private int initialCapacity;
        private bool readOnly = false;

        internal HeadersCollection( int initialCapacity )
        {
            this.initialCapacity = initialCapacity;
        }

        
		/// <summary>
		/// indexer
		/// </summary>
        public Infragistics.Win.UltraWinGrid.HeaderBase this[ int index ]
        {
            get
            {
                return (HeaderBase)this.GetItem( index );
            }
            set
            {
                if ( !this.readOnly )
                    throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_161"));
                this.List[ index ] = value;
            }
        }

		/// <summary>
		/// Property: get isreadonly
		/// </summary>
        public override bool IsReadOnly
        {
            get
            {
                return this.readOnly;
            }
        }

        internal void SetReadOnly( bool readOnly )
        {
            this.readOnly = readOnly;
        }
		internal int Capacity
        {
            get
            {
                return this.List.Capacity;
            }
            set
            {
                this.List.Capacity = value;
            }
        }

		internal void Sort( IComparer comparer )
		{
			this.List.Sort( comparer );
		}

        /// <summary>
        /// Returns the first visible header or null if none are visible
        /// </summary>
        public HeaderBase FirstVisibleHeader
        {
            get
            {
                for( int i = 0; i < this.Count; i++ )
                {
                    if ( !this[i].Hidden )
                        return this[i];
                }

                return null;
            }
        }

		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		// Added LastVisibleHeader.
		//
		/// <summary>
		/// Returns the last visible header or null if none are visible
		/// </summary>
		public HeaderBase LastVisibleHeader
		{
			get
			{
				for( int i = this.Count - 1; i >= 0; i-- )
				{
					if ( ! this[i].Hidden )
						return this[i];
				}

				return null;
			}
		}

        internal int InternalAdd( HeaderBase header )
        {
            return base.InternalAdd( header );
        }

        internal void InternalRemove( HeaderBase header )
        {
            base.InternalRemove( header );
        }

        internal void InternalInsert( int index, HeaderBase header )
        {
            base.InternalInsert( index, header );
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal void InternalInsertRange( int index, HeadersCollection headers )
		//{
		//    this.List.InsertRange( index, headers );
		//}

		#endregion Not Used


        internal new void InternalClear()
        {
            base.InternalClear();
        }
        
        /// <summary>
        /// Specifies the initial capacity of the collection
        /// </summary>
        protected override int InitialCapacity
        {
            get
            {
                return this.initialCapacity;
            }
        }

		internal void Swap( HeaderBase header1, HeaderBase header2 )
		{
			int index1 = this.List.IndexOf( header1 );
			int index2 = this.List.IndexOf( header2 );
			
			if ( index1 < 0 || index2 < 0 )
			{
				Debug.Fail("Invalid header parameters in HeadersCollection.Swap.");
				return;
			}

			// swap the 2 headers
			//
			this.List[index1] = header2;
			this.List[index2] = header1;

            // MRS 2/18/2009 - TFS14007
            header1.NotifyPropChange(PropertyIds.VisiblePosition);
            header2.NotifyPropChange(PropertyIds.VisiblePosition);

		}
     
         
		/// <summary>
		/// IEnumerable Interface Implementation        
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public HeaderEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new HeaderEnumerator(this);
        }

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.HeaderBase[] array, int index)
		{
			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo
        
		/// <summary>
		/// inner class implements IEnumerator interface
        /// </summary>
        public class HeaderEnumerator : DisposableObjectEnumeratorBase
        {   
			/// <summary>
			/// Constructor
			/// </summary>
			/// <param name="rowScrollRegions"></param>
            public HeaderEnumerator(HeadersCollection rowScrollRegions) : base( rowScrollRegions )
            {
            }

			/// <summary>
			/// non-IEnumerator version: type-safe
			/// </summary>
            public HeaderBase Current  
            {
                get
                {
                    return (HeaderBase)((IEnumerator)this).Current;
                }
            }
        }


		// SSP 1/14/03 - Accessibility
		// Renamed ColumnHeadersAccessibleObject to HeadersAccessibleObject since this
		// will contain band header and group headers as well.
		//
		#region HeadersAccessibleObject Class

		/// <summary>
		/// The Accessible object for a row or band's column headers.
		/// </summary>
		public class HeadersAccessibleObject : AccessibleObjectWrapper
		{
			#region Private Members

			private UltraGridRow row;
			private UltraGridBand band;
			private UltraGridChildBand childBand;
			private System.Windows.Forms.AccessibleObject[] headerAccessibleObjects;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList headersArray = null;
			List<HeaderBase> headersArray = null;

			int columnsVersion = -1;

			#endregion Private Members

			#region Constructor
			
			/// <summary>
			/// Constructor
			/// </summary>
            /// <param name="band">The UltraGridBand to which the headers belong.</param>
			public HeadersAccessibleObject( UltraGridBand band )
			{
				this.band = band;
			}
			
			/// <summary>
			/// Constructor
			/// </summary>
            /// <param name="row">The UltraGridRow to which the headers belong.</param>
			public HeadersAccessibleObject( UltraGridRow row )
			{
				this.row = row;
				this.band = this.row.Band;
			}
			
			/// <summary>
			/// Constructor
			/// </summary>
            /// <param name="childBand">The UltraGridChildBand to which the headers belong.</param>
			public HeadersAccessibleObject( UltraGridChildBand childBand )
			{
				this.childBand = childBand;
				this.row = this.childBand.ParentRow;
				this.band = this.childBand.Band;
			}

			#endregion Constructor

			#region Base Class Overrides

				#region Bounds

			/// <summary>
			/// Gets the location and size of the accessible object.
			/// </summary>
			public override System.Drawing.Rectangle Bounds
			{
				get
				{
					Infragistics.Win.UIElement element = this.UIElement;

					if ( element == null )
						return Rectangle.Empty;
	
					Control control = element.Control;

					if ( control == null )
						return Rectangle.Empty;

					return control.RectangleToScreen( element.ClipRect );
				}
			}

				#endregion Bounds

				#region DefaultAction

			/// <summary>
			/// Gets a string that describes the default action of the object.
			/// </summary>
			public override string DefaultAction
			{
				get
				{
					return null;
				}
			}

				#endregion DefaultAction

				#region Description

			/// <summary>
			/// Gets a string that describes the visual appearance of the specified object. Not all objects have a description.
			/// </summary>
			public override string Description
			{
				get
				{
					return null;
				}
			}

				#endregion Description

				#region DoDefaultAction

			/// <summary>
			/// Performs the default action associated with this accessible object.
			/// </summary>
			public override void DoDefaultAction()
			{
			}

				#endregion DoDefaultAction

				#region GetChild

			/// <summary>
			/// Retrieves the accessible child corresponding to the specified index.
			/// </summary>
			/// <param name="index">The zero-based index of the accessible child.</param>
			/// <returns>An AccessibleObject that represents the accessible child corresponding to the specified index.</returns>
			public override AccessibleObject GetChild(int index)
			{
				// Access the Headers to ensure that we have initialized the headersArray and
				// headerAccessibleObjects member vars.
				//
				if ( index >= 0 && index < this.Headers.Count )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//return this.GetAccessibleObjectFor( (HeaderBase)this.Headers[ index ] );
					return this.GetAccessibleObjectFor( this.Headers[ index ] );
				}

				return null;
			}

				#endregion GetChild

				#region GetChildCount

			/// <summary>
			/// Retrieves the number of children belonging to an accessible object.
			/// </summary>
			/// <returns>The number of children belonging to an accessible object.</returns>
			public override int GetChildCount()
			{
				// SSP 1/14/04
				//
				//return this.band.Columns.Count;
				return this.Headers.Count;
			}

				#endregion GetChildCount

				#region GetFocused

			/// <summary>
			/// Retrieves the object that has the keyboard focus.
			/// </summary>
			/// <returns>An AccessibleObject that specifies the currently focused child. This method returns the calling object if the object itself is focused. Returns a null reference (Nothing in Visual Basic) if no object has focus.</returns>
			public override AccessibleObject GetFocused()
			{
				return null;
			}

				#endregion GetFocused

			//JJD 12/17/03
				#region GetMarshallingControl

			/// <summary>
			/// Returns the control used to synchronize accessibility calls.
			/// </summary>
			/// <returns>A control to be used to synchronize accessibility calls.</returns>
			protected override Control GetMarshallingControl()
			{
				return this.band.Layout.Grid;
			}

				#endregion GetMarshallingControl

				#region GetSelected

			/// <summary>
			/// Retrieves the currently selected child.
			/// </summary>
			/// <returns>An AccessibleObject that represents the currently selected child. This method returns the calling object if the object itself is selected. Returns a null reference (Nothing in Visual Basic) if is no child is currently selected and the object itself does not have focus.</returns>
			public override AccessibleObject GetSelected()
			{
				
				return null;
			}

				#endregion GetSelected

				#region Help

			/// <summary>
			/// Gets a description of what the object does or how the object is used.
			/// </summary>
			public override string Help
			{
				get
				{
					return null;
				}
			}

				#endregion Help

				#region HitTest

			/// <summary>
			/// Retrieves the child object at the specified screen coordinates.
			/// </summary>
			/// <param name="x">The horizontal screen coordinate.</param>
			/// <param name="y">The vertical screen coordinate.</param>
			/// <returns>An AccessibleObject that represents the child object at the given screen coordinates. This method returns the calling object if the object itself is at the location specified. Returns a null reference (Nothing in Visual Basic) if no object is at the tested location.</returns>
			public override AccessibleObject HitTest(int x, int y)
			{
				
				return base.HitTest( x, y );
			}

				#endregion HitTest

				#region Name

			/// <summary>
			/// The accessible name for the data area.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if ( name != null && name.Length > 0 )
						return name;

					return Shared.SR.GetString("AccessibleName_ColumnHeaders");
				}
				set
				{
					base.Name = value;
				}
			}

				#endregion Name

				#region Navigate

            /// <summary>
            /// Navigates to another accessible object.
            /// </summary>
            /// <param name="navdir">One of the <see cref="System.Windows.Forms.AccessibleNavigation"/> values.</param>
            /// <returns>An AccessibleObject relative to this object.</returns>
            public override AccessibleObject Navigate(AccessibleNavigation navdir)
			{

				UltraGridRow sibling = null;

				switch ( navdir )
				{
						#region case FirstChild/LastChild

					case AccessibleNavigation.FirstChild:
					case AccessibleNavigation.LastChild:
						return base.Navigate( navdir );

						#endregion case FirstChild/LastChild

						#region case Down

					case AccessibleNavigation.Down:
						if ( this.row != null )
							sibling = this.row.GetNextVisibleRelative( false, true, false );
						else
						{
							for ( int i = this.band.Index + 1; i < this.band.Layout.SortedBands.Count; i++ )
							{
								if ( this.band.Layout.SortedBands[i].ParentBand == this.band.ParentBand )
								{
									if ( this.band.Layout.SortedBands[i].ColHeadersAccessibilityObject != null )
										return this.band.Layout.SortedBands[i].ColHeadersAccessibilityObject;
								}
							}

							sibling = this.band.Layout.GetFirstOrLastRowInGrid( true );
						}
						break;

						#endregion case Down

						#region case Up

					case AccessibleNavigation.Up:
						if ( this.row != null )
							sibling = this.row.GetPrevVisibleRelative( false, true, false );
						else
						{
							for ( int i = this.band.Layout.SortedBands.Count - 1; i > this.band.Index; i-- )
							{
								if ( this.band.Layout.SortedBands[i].ParentBand == this.band.ParentBand )
								{
									if ( this.band.Layout.SortedBands[i].ColHeadersAccessibilityObject != null )
										return this.band.Layout.SortedBands[i].ColHeadersAccessibilityObject;
								}
							}
						}
						break;

						#endregion case Up

						#region case Next

					case AccessibleNavigation.Next:
						if ( this.row != null )
							sibling = this.row.GetNextVisibleRelative( false, true, false );
						else
						{
							for ( int i = this.band.Index + 1; i < this.band.Layout.SortedBands.Count; i++ )
							{
								if ( this.band.Layout.SortedBands[i].ColHeadersAccessibilityObject != null )
									return this.band.Layout.SortedBands[i].ColHeadersAccessibilityObject;
							}

							sibling = this.band.Layout.GetFirstOrLastRowInGrid( true );
						}
						break;

						#endregion case Next

						#region case Previous

					case AccessibleNavigation.Previous:
						if ( this.row != null )
							sibling = this.row.GetPrevVisibleRelative( false, true, false );
						else
						{
							for ( int i = this.band.Layout.SortedBands.Count - 1; i > this.band.Index; i-- )
							{
								if ( this.band.Layout.SortedBands[i].ColHeadersAccessibilityObject != null )
									return this.band.Layout.SortedBands[i].ColHeadersAccessibilityObject;
							}

							sibling = this.band.Layout.GetFirstOrLastRowInGrid( true );

							if ( sibling != null )
								sibling = sibling.GetSibling( SiblingRow.Last, false );
						}
						break;

						#endregion case Previous

						
						#region case Right

					case AccessibleNavigation.Right:
						break;

						#endregion case Right

						#region case Left

						
					case AccessibleNavigation.Left:
						break;

						#endregion case Left
				}

				if ( sibling != null )
					return sibling.AccessibilityObject;

				return null;
			}

				#endregion Navigate

				#region Parent

			/// <summary>
			/// Gets the parent of an accessible object.
			/// </summary>
			public override AccessibleObject Parent
			{
				get
				{
					if ( this.row == null )
						return this.band.Layout.Grid.AccessibilityObject;

					return this.row.AccessibilityObject;
				}
			}

				#endregion Parent

				#region Role

			/// <summary>
			/// Gets the role of this accessible object.
			/// </summary>
			public override AccessibleRole Role
			{
				get
				{
					return AccessibleRole.Grouping;
				}
			}

				#endregion Role

				#region Select

			/// <summary>
			/// Modifies the selection or moves the keyboard focus of the accessible object.
			/// </summary>
			/// <param name="flags">One of the <see cref="System.Windows.Forms.AccessibleSelection"/> values.</param>
			public override void Select(System.Windows.Forms.AccessibleSelection flags)
			{
			}

				#endregion Select

				#region State

			/// <summary>
			/// Gets the state of this accessible object.
			/// </summary>
			public override AccessibleStates State
			{
				get
				{
					return AccessibleStates.ReadOnly;
				}
			}

				#endregion State

				#region Value

			/// <summary>
			/// Returns the row's description
			/// </summary>
			public override string Value
			{
				get
				{
					return null;
				}
				set
				{
				}
			}

				#endregion Value

			#endregion Base Class Overrides

			#region Properties

				#region Public Properties

					#region Band

			/// <summary>
			/// Returns the associated band.
			/// </summary>
			public UltraGridBand Band 
			{ 
				get 
				{
					return this.band; 
				} 
			}

					#endregion Band

					#region ChildBand

			/// <summary>
			/// Returns the associated child band or null.
			/// </summary>
			public UltraGridChildBand ChildBand 
			{ 
				get 
				{
					return this.childBand; 
				} 
			}

					#endregion ChildBand

					#region Row

			/// <summary>
			/// Returns the associated row.
			/// </summary>
			public UltraGridRow Row 
			{ 
				get 
				{
					return this.row; 
				} 
			}

					#endregion Row

					#region UIElement

			/// <summary>
			/// Returns the associated UIElement or null.
			/// </summary>
			public Infragistics.Win.UIElement UIElement
			{
				get
				{
					Infragistics.Win.UIElement element = this.band.Layout.UIElement.GetDescendant( typeof(RowColRegionIntersectionUIElement), new object[] { this.band.Layout.ActiveRowScrollRegion, this.band.Layout.ActiveColScrollRegion } );

					if ( element == null )
						return null;

					if ( this.band.CardView == true )
					{
						CardLabelAreaUIElement cla = null;

						if ( this.childBand != null )
							cla = element.GetDescendant( typeof( CardLabelAreaUIElement ), this.childBand.Rows ) as CardLabelAreaUIElement;
						else if ( this.row != null )
							cla = element.GetDescendant( typeof( CardLabelAreaUIElement ), this.row.ParentCollection ) as CardLabelAreaUIElement;
						else
							cla = element.GetDescendant( typeof( CardLabelAreaUIElement ), this.band.Layout.Rows ) as CardLabelAreaUIElement;
				
						return cla;
					}
					else
					{
						BandHeadersUIElement bhe = null;

						if ( this.row != null )
							bhe = element.GetDescendant( typeof( BandHeadersUIElement ), this.row ) as BandHeadersUIElement;
						else
							bhe = element.GetDescendant( typeof( BandHeadersUIElement ), this.band.Header ) as BandHeadersUIElement;

						return bhe;
					}
				}
			}

					#endregion UIElement

				#endregion Public Properties

			#endregion Properties

			#region Private/Internal Properties

			#region Headers
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//internal ArrayList Headers
			internal List<HeaderBase> Headers
			{
				get
				{
					if ( null != this.headersArray && this.columnsVersion == this.Band.ColumnsVersion )
						return this.headersArray;

					this.columnsVersion = this.Band.ColumnsVersion;

					int i;
					Hashtable hashtable = null;
					if ( null != this.headersArray && null != this.headerAccessibleObjects )
					{
						hashtable = new Hashtable( headersArray.Count );
						for ( i = 0; i < this.headerAccessibleObjects.Length && i < headersArray.Count; i++ )
						{
							if ( null != this.headerAccessibleObjects[i] )
								hashtable[ headersArray[i] ] = this.headerAccessibleObjects[i];
						}
					}						

					if ( null != this.headersArray )
						this.headersArray.Clear( );
					else
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//this.headersArray = new ArrayList( );
						this.headersArray = new List<HeaderBase>();
					}

					// First add the band header, even if it's not visible.
					//
					this.headersArray.Add( this.Band.Header );

					// Then add group headers if groups are being used.
					//
					if ( this.Band.GroupsDisplayed )
					{
						GroupsCollection groups = this.Band.Groups;
						for ( i = 0; i < groups.Count; i++ )
							this.headersArray.Add( groups[i].Header );
					}

					// Then add all the column headers.
					//
					ColumnsCollection cols = this.Band.Columns;
					for ( i = 0; i < cols.Count; i++ )
						this.headersArray.Add( cols[i].Header );

					this.headerAccessibleObjects = new System.Windows.Forms.AccessibleObject[ this.headersArray.Count ];
					for ( i = 0; i < this.headersArray.Count; i++ )
					{
						this.headerAccessibleObjects[i] = 
							null != hashtable && hashtable.ContainsKey( headersArray[ i ] ) 
							? hashtable[ headersArray[i] ] as System.Windows.Forms.AccessibleObject
							: null;
					}

					return this.headersArray;
				}
			}

			#endregion // Headers

			#endregion // Private/Internal Properties

			#region Private/Internal Methods

			internal AccessibleObject GetAccessibleObjectFor( HeaderBase header )
			{
				int index = this.Headers.IndexOf( header );
				if ( index >= 0 && index < this.headerAccessibleObjects.Length )
				{
					AccessibleObject ao = this.headerAccessibleObjects[ index ];

					if ( null != ao )
						return ao;
					
					if ( header is ColumnHeader )
						ao = this.band.Layout.Grid.CreateAccessibilityInstance( new ColumnHeader.ColumnHeaderAccessibleObject( (ColumnHeader)header, this ) );
					else if ( header is GroupHeader )
						ao = this.band.Layout.Grid.CreateAccessibilityInstance( new GroupHeader.GroupHeaderAccessibleObject( (GroupHeader)header, this ) );
					else if ( header is BandHeader )
						ao = this.band.Layout.Grid.CreateAccessibilityInstance( new BandHeader.BandHeaderAccessibleObject( (BandHeader)header, this ) );
					else
						Debug.Assert( false, "Unknown type of header !" );

					return this.headerAccessibleObjects[ index ] = ao;						
				}

				return null;
			}

			#endregion // Private/Internal Methods
		}

		#endregion // HeadersAccessibleObject Class
    }
}
