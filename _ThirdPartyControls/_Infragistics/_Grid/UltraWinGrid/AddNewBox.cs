#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Security.Permissions;
	using Infragistics.Shared.Serialization;
	using System.Collections;
	using System.Collections.Generic;

	/// <summary>
	/// Returns a reference to the AddNewBox object. This property is read-only 
	/// at design-time and run-time.
	/// </summary>
	/// <remarks>
	/// <p class="body">This property returns a reference to an AddNewBox object that can be used to set properties of, and invoke methods on, the AddNew box. You can use this reference to access any of the AddNew box's properties or methods.</p>
	/// <p class="body">Use the returned reference to show or hide the AddNew box or adjust its or its buttons' appearance.</p>
	/// </remarks>
	[ TypeConverter( typeof( System.ComponentModel.ExpandableObjectConverter ) ), Serializable()  ]
	sealed public class AddNewBox : SpecialBoxBase, ISerializable, 
		//MRS 2/23/04
		//Added ISupportPresets
		ISupportPresets
	{
		private AddNewBoxStyle							style;
		private UIElementButtonStyle					buttonStyle = UIElementButtonStyle.Default;

		
		private Infragistics.Win.AppearanceHolder		buttonAppearanceHolder = null;
	
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//static internal readonly int ADDNEWBTN_CONNECTOR_LEFT_INDENT = 8;
		//static internal readonly int ADDNEWBTN_CONNECTOR_BOTTOM_INDENT = 4;
		//static internal readonly int ADDNEWBTN_BAND_HEIGHT_OFFSET = 8;
		//static internal readonly int ADDNEWBTN_BAND_HEIGHT_OFFSET_COMPACT = 2;
		//static internal readonly int ADDNEWBTN_MIN_SPACING_HORZ = 10;
		//static internal readonly int ADDNEWBTN_MIN_SPACING_HORZ_COMPACT = 10;
		//static internal readonly int ADDNEWBTN_MIN_SPACING_VERT = 6;
		internal const int ADDNEWBTN_CONNECTOR_LEFT_INDENT = 8;
		internal const int ADDNEWBTN_CONNECTOR_BOTTOM_INDENT = 4;
		internal const int ADDNEWBTN_BAND_HEIGHT_OFFSET = 8;
		internal const int ADDNEWBTN_BAND_HEIGHT_OFFSET_COMPACT = 2;
		internal const int ADDNEWBTN_MIN_SPACING_HORZ = 10;
		internal const int ADDNEWBTN_MIN_SPACING_HORZ_COMPACT = 10;
		internal const int ADDNEWBTN_MIN_SPACING_VERT = 6;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//static internal readonly int ADDNEWBTN_MIN_SPACING_VERT_COMPACT = 3;	
		

        /// <summary>
        /// Constructor. Initializes a new instance of <see cref="AddNewBox"/>.
        /// </summary>
        /// <param name="layout">The UltraGridLayout instance associated with this AddNewBox</param>
		public AddNewBox( Infragistics.Win.UltraWinGrid.UltraGridLayout layout ) : base( layout )
		{
			this.Reset();
		}


		internal override void InitializeFrom( SpecialBoxBase source, PropertyCategories propertyCategories )
		{
            base.InitializeFrom( source, propertyCategories );

			if ( (propertyCategories & PropertyCategories.General) == 0 )
				return;
		
			// JAS 7/21/05 BR05060 - I rewrote the following code because it was getting messy.
			// I also moved the set of ButtonStyle out of the if() block.  It didn't belong there.
			// Setting the ButtonStyle should not only occur if there is an AppearanceHolder object.
			//
			#region Rewritten & Fixed
			
			#endregion // Rewritten & Fixed

			AddNewBox addNewBox = source as AddNewBox;
			if( addNewBox != null )
			{
				this.Style = addNewBox.style;
				this.ButtonStyle = addNewBox.buttonStyle;

				// SSP 11/4/05 BR07503
				// Clone copies over the AppearanceCollection as well. We don't want that.
				// 
				// ------------------------------------------------------------------------------
				// Use the source's ButtonAppearanceHolder instead of its AppearanceHolder
				//
				//if( addNewBox.buttonAppearanceHolder != null )
				//	this.buttonAppearanceHolder = addNewBox.buttonAppearanceHolder.Clone();

				if ( null != addNewBox.buttonAppearanceHolder &&
					addNewBox.buttonAppearanceHolder.HasAppearance )
				{
					// Get appearance to trigger allocation of holder
					//
					Infragistics.Win.AppearanceBase app = this.ButtonAppearance;
			
					// init the holder from the source
					//
					this.buttonAppearanceHolder.InitializeFrom( addNewBox.buttonAppearanceHolder );
				}
				else
				{
					// reset the appearance holder
					//
					if ( null != this.buttonAppearanceHolder &&
						this.buttonAppearanceHolder.HasAppearance )
					{
						this.buttonAppearanceHolder.Reset();
					}
				}
				// ------------------------------------------------------------------------------
			}
		}

		/// <summary>
		/// Returns true is any of the properties have been set to non-default values
		/// </summary>
        /// <returns>Returns true is any of the properties have been set to non-default values</returns>
		public override bool ShouldSerialize() 
		{
			return base.ShouldSerialize( ) ||
				   this.ShouldSerializeStyle() ||
				   this.ShouldSerializeButtonAppearance( ) ||
					this.ShouldSerializeButtonStyle();
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		public bool ShouldSerializeButtonAppearance() 
		{
			return ( this.HasButtonAppearance &&
				this.buttonAppearanceHolder.ShouldSerialize() );
		}


		/// <summary>
		/// Resets all properties back to their default values
		/// </summary>
		public override void Reset() 
		{
			base.Reset( );
			this.ResetStyle();
			this.ResetButtonAppearance( );
			this.ResetButtonStyle();
		}

		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// If the base class did not find any matching propChange.Source
			// then fire NotifyPropChange with PropertyIds.AddNewBox
			if ( !base.OnSubObjectPropChangedHelper( propChange ) )			
				this.NotifyPropChange( PropertyIds.AddNewBox, propChange );
				//	changes to ButtonAppearance were not reflected until grid was invalidated
			else if ( this.HasButtonAppearance &&
				this.buttonAppearanceHolder.RootAppearance == propChange.Source )
				this.NotifyPropChange( PropertyIds.ButtonAppearance, propChange );
		}

		/// <summary>
		/// Called when the object is disposed of
		/// </summary>
		protected override void OnDispose()
		{
			
			// It is importantant to unhook the holder and call reset on it
			// when we are disposed
			//
			if ( this.buttonAppearanceHolder != null )
			{
				this.buttonAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.buttonAppearanceHolder.Reset();
				this.buttonAppearanceHolder = null;
			}


			base.OnDispose();
		}




		// SSP 10/31/01 
		// 
		internal override void OnInitLayout()
		{
			base.OnInitLayout( );

			if ( this.Layout != null )
			{
				if ( this.buttonAppearanceHolder != null )
					this.buttonAppearanceHolder.Collection = this.Layout.Appearances;
			}
		}
	
		/// <summary>
		/// Returns or sets a value that determines the AddNew box's display style.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property specifies the display style of the AddNew box. When set to 0 (AddNewBoxStyleFull) the full AddNew Box will be displayed, with the arrangement of the buttons corresponding to the the hierarchical relationships of the bands in the grid. When the 1 (AddNewBoxStyleCompact) setting is used, the AddNew Box will be displayed using as little real estate as possible while still maintaining a visually acceptable appearance.</p>
		/// <p class="body">Note that in the compact view the AddNew buttons appear in the same horizontal row, regardless of the hierarchical structure. Buttons for sibling bands do not necessarily appear adjacent to one another; if a band has child bands, their AddNew buttons will appear immediately following that of their parent band.</p>
		/// </remarks>
		[ LocalizedDescription("LD_AddNewBox_P_Style") ]
		[ LocalizedCategory("LC_Display") ]
		public AddNewBoxStyle Style
		{
			get
			{		
				return this.style;						
			}
			set
			{
				if( this.style != value )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(AddNewBoxStyle), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_0") );

					this.style = value;
					this.NotifyPropChange( PropertyIds.AddNewBoxStyle, null );
				}
				this.style = value;
			}
		}

		/// <summary>
		/// Returns true if the Style property needs to be serialized (not null)
		/// </summary>
        /// <returns>Returns true if the Style property needs to be serialized (not null)</returns>
		public bool ShouldSerializeStyle() 
		{
			return ( this.style != AddNewBoxStyle.Full );
		}
 
		/// <summary>
		/// Resets Style to its default value (Full).
		/// </summary>
		public void ResetStyle() 
		{
			this.Style = AddNewBoxStyle.Full;
		}

		internal override string DefaultPrompt
		{
			get
			{
				return SR.GetString("AddNewBoxDefaultPrompt"); //"Add ...";
			}
		}


		/// <summary>
		/// Specifies the appearance of add-new buttons.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// You can use the <b>ButtonAppearance</b> property to control the appearance of
		/// buttons that the add-new box displays. <b>Note</b> that you can specify the
		/// overall look of all the buttons that the UltraGrid displays using the
		/// Override's <see cref="UltraGridOverride.ButtonStyle"/> property.
		/// </p>
		/// <seealso cref="Infragistics.Win.Appearance"/>
		/// <seealso cref="AddNewBox.ButtonStyle"/>
		/// <seealso cref="UltraGridOverride.ButtonStyle"/>
		/// <seealso cref="SpecialBoxBase.ButtonConnectorColor"/>
		/// <seealso cref="SpecialBoxBase.ButtonConnectorStyle"/>
		/// <seealso cref="SpecialBoxBase.BorderStyle"/>
		/// </remarks>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		[ LocalizedDescription("LD_AddNewBox_P_ButtonAppearance") ]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.AppearanceBase ButtonAppearance
		{
			get
			{
				if ( null == this.buttonAppearanceHolder )
				{
				    
					this.buttonAppearanceHolder = new Infragistics.Win.AppearanceHolder();
				
					// hook up the prop change notifications
					//
					this.buttonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.buttonAppearanceHolder.Collection = this.Layout.Appearances;

				return this.buttonAppearanceHolder.Appearance;
			}
			set
			{
				
				if( this.buttonAppearanceHolder == null  ||
					!this.buttonAppearanceHolder.HasAppearance ||					
					this.buttonAppearanceHolder.Appearance != value )
				{
					// remove the old prop change notifications
					//
					if ( null == this.buttonAppearanceHolder ) 
					{
						this.buttonAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.buttonAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;					
					}	
				       
  					// Initialize the collection
					//
					if (this.Layout != null)
						this.buttonAppearanceHolder.Collection = this.Layout.Appearances;

                    
					this.buttonAppearanceHolder.Appearance = value;
            
					
					this.NotifyPropChange( PropertyIds.ButtonAppearance, null );
				}
			}
		}



		
		/// <summary>
		/// Specifies the style of buttons in the add-new box.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>ButtonStyle</b> property specifies the style of buttons in the add-new box.
		/// <b>Note</b> that you can specify the overall look of all the buttons that 
		/// the UltraGrid displays using the Override's 
		/// <see cref="UltraGridOverride.ButtonStyle"/> property.
		/// </p>
		/// <seealso cref="Infragistics.Win.UIElementBorderStyle"/>
		/// <seealso cref="AddNewBox.ButtonAppearance"/>
		/// </remarks>
		[ LocalizedDescription("LD_AddNewBox_P_ButtonStyle") ]
		[ LocalizedCategory("LC_Display") ]
		public Infragistics.Win.UIElementButtonStyle ButtonStyle
		{
			get
			{
				return this.buttonStyle;
			}

			set
			{
				if ( value != this.buttonStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UIElementButtonStyle), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_1") );

					this.buttonStyle = value;
					this.NotifyPropChange( PropertyIds.ButtonStyle, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the Button Border Style property needs to be serialized (not null )
		/// </summary>
        /// <returns>Returns true if the Button Border Style property needs to be serialized (not null )</returns>
		public bool ShouldSerializeButtonStyle() 
		{
			return ( this.buttonStyle != Infragistics.Win.UIElementButtonStyle.Default );
		}
 
		/// <summary>
		/// Resets the Button Border Style to default
		/// </summary>
		public void ResetButtonStyle() 
		{
			this.buttonStyle = Infragistics.Win.UIElementButtonStyle.Default;
		}


		// SSP 3/27/06 - App Styling
		// Made the property into a method because we need to pass in the context of a band.
		// 
		//internal Infragistics.Win.UIElementButtonStyle ButtonStyleResolved
		internal Infragistics.Win.UIElementButtonStyle GetButtonStyleResolved( UltraGridBand band )
		{
			// SSP 3/27/06 - App Styling
			// 
			// ------------------------------------------------------------------------------------------
			//return Infragistics.Win.UIElementButtonStyle.Default == this.ButtonStyle ?
			//	Infragistics.Win.UIElementButtonStyle.Button : this.ButtonStyle;
			object objStyle;

			// If band is specified then get the band's ButtonStyle setting otherwise get the 
			// layout's button style setting.
			// 
			if ( null != band )
			{
				if ( ! StyleUtils.GetCachedPropertyVal( band, StyleUtils.CachedProperty.ButtonStyleAddNewBoxButton, out objStyle ) )
				{
					UIElementButtonStyle resolveVal = band.GetButtonStyleResolved( 
						StyleUtils.GetControlAreaRole( band.Layout ), StyleUtils.CachedProperty.ButtonStyleLayout );

					objStyle = StyleUtils.CacheButtonStylePropertyValue( band, 
						StyleUtils.CachedProperty.ButtonStyleAddNewBoxButton, 
						StyleUtils.Role.AddNewBoxButton, this.ButtonStyle, resolveVal );
				}
			}
			else
			{
				if ( ! StyleUtils.GetCachedPropertyVal( this.Layout, StyleUtils.CachedProperty.ButtonStyleAddNewBoxButton, out objStyle ) )
				{
					UIElementButtonStyle resolveVal = UIElementButtonStyle.Button;

					objStyle = StyleUtils.CacheButtonStylePropertyValue( this.Layout, 
						StyleUtils.CachedProperty.ButtonStyleAddNewBoxButton, 
						StyleUtils.Role.AddNewBoxButton, this.ButtonStyle, resolveVal );
				}
			}

			return (UIElementButtonStyle)objStyle;
			// ------------------------------------------------------------------------------------------
		}


		
		internal int ButtonHeight
		{
			
			//
			get
			{
				
				// SSP 9/20/01
				// moved all the code to GetButtonSize so ButtonHeight
				// property and GetButtonWidth can use the same code to
				// calculate width and the height of a button

				return this.GetButtonSize( "hgWy" ).Height;
			}
		}

		internal Size GetButtonSize( string caption )
		{
			Size size = Size.Empty;

			AppearanceData appData = new AppearanceData();
			this.ResolveButtonAppearance( ref appData, AppearancePropFlags.FontData );
			
			// JJD 12/14/01
			// Moved font height logic into CalculateFontHeight
			//
			// get the extext of the caption
			//
			if ( caption.Length > 0 )
				size = this.Layout.CalculateFontSize( ref appData, caption );
			else
				size = this.Layout.CalculateFontSize( ref appData, "WWWWy" );

			return size;
		}

		internal int GetButtonWidth( string caption )
		{
			// SSP 9/20/01
			// moved all the code to GetButtonSize so ButtonHeight
			// property and GetButtonWidth can use the same code to
			// calculate width and the height of a button

			return this.GetButtonSize( caption ).Width;
		}

		/// <summary>
		/// Returns true if a button appearance object has been created.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
		Browsable( false ) ]		
		public bool HasButtonAppearance
		{
			get
			{
				return ( null != this.buttonAppearanceHolder &&
					this.buttonAppearanceHolder.HasAppearance );
			}
		}


		/// <summary>
		/// Resolves all of the button's appearance properties
		/// </summary>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		public void ResolveButtonAppearance( ref AppearanceData appData )
		{
			this.ResolveAppearance( ref appData, AppearancePropFlags.AllRenderAndCursor );
		}

		/// <summary>
		/// Resolves selected properties of the button's appearance
		/// </summary>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		/// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
		public void ResolveButtonAppearance( ref AppearanceData appData,
			AppearancePropFlags requestedProps )
		{
			// SSP 4/27/06 - App Styling - BR11781
			// RequestedProps parameter should be passed in by ref.
			// 
			//this.ResolveButtonAppearance( ref appData, requestedProps, true );
			this.ResolveButtonAppearance( ref appData, ref requestedProps, true );
		}

		// JAS BR04889 7/22/05 - ButtonStyle of WindowsXPCommand
		// Added an overload of ResolveAppearance that takes in resolveDefaultColors.
		//
		internal void ResolveButtonAppearance( 
			ref AppearanceData appData,
			// SSP 4/27/06 - App Styling - BR11781
			// RequestedProps parameter should be passed in by ref.
			// 
			//AppearancePropFlags requestedProps,
			ref AppearancePropFlags requestedProps,
			bool resolveDefaultColors )
		{
			// Use OBJECT_TYPE_SpecialBoxBase instead of OBJECT_TYPE_HEADER so we don't get any
			// properties from the header appearance 
			//
			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.SpecialBoxBase), requestedProps );

			// SSP 3/12/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( this.Layout, StyleUtils.Role.AddNewBoxButton, out context.ResolutionOrder );

			context.ResolveDefaultColors = resolveDefaultColors;
			
			// SSP 3/8/06 - App Styling
			// 
			if ( context.ResolutionOrder.UseStyleBefore )
				StyleUtils.ResolveAppearance( AppStyling.RoleState.Normal, ref appData, ref context );

			// first try to apply the addnew box's appearance
			//
			// SSP 3/8/06 - App Styling
			// Check for UseControlInfo.
			// 
			//if ( this.buttonAppearanceHolder != null )
			if ( this.buttonAppearanceHolder != null && context.ResolutionOrder.UseControlInfo )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, 
				//	ButtonAppearance.Data,
				//	ref context.UnresolvedProps );
				ButtonAppearance.MergeData( ref appData, ref context.UnresolvedProps );
			}

			// SSP 3/8/06 - App Styling
			// 
			if ( context.ResolutionOrder.UseStyleAfter )
				StyleUtils.ResolveAppearance( AppStyling.RoleState.Normal, ref appData, ref context );

			// SSP 4/27/06 - App Styling - BR11781
			// Enclosed the existing code into the if block. Check for the resolveDefaultColors parameter.
			// 
			if ( resolveDefaultColors )
			{
				// default the back and fore color if not already set
				//
				if ( ( (requestedProps & AppearancePropFlags.BackColor) != 0) &&
					!appData.HasPropertyBeenSet(AppearancePropFlags.BackColor ) )
					appData.BackColor = SystemColors.Control;

				//Default the PictureAlign and
				//PictureVAlign proeprties.
				if ( ( (requestedProps & AppearancePropFlags.ImageHAlign) != 0) && 
					!appData.HasPropertyBeenSet(AppearancePropFlags.ImageHAlign ) )
					appData.ImageHAlign = HAlign.Left;

				if ( ( (requestedProps & AppearancePropFlags.ImageVAlign) != 0) && 
					!appData.HasPropertyBeenSet(AppearancePropFlags.ImageVAlign ) )
					appData.ImageVAlign = VAlign.Middle;

				//Default PictureBackgroundStyle to Centered.
				if ( ( (requestedProps & AppearancePropFlags.ImageBackgroundStyle) != 0) && 
					!appData.HasPropertyBeenSet(AppearancePropFlags.ImageBackgroundStyle) )
					appData.ImageBackgroundStyle = ImageBackgroundStyle.Centered;
			}

			// SSP 5/11/06 - App Styling - BR11781
			// RequestedProps parameter should be passed in by ref.
			// 
			requestedProps = context.UnresolvedProps;

			// Strip out all bits from the the request and resolved mask except
			// for font and forecolor before calling m_Layout.ResolveAppearance
			// because that is all we ever want to inherit
			//
			// SSP 5/11/06 - App Styling - BR11781
			// RequestedProps parameter should be passed in by ref.
			// 
			//context.UnresolvedProps &= AppearancePropFlags.FontData | AppearancePropFlags.ForeColor;
			const AppearancePropFlags PROPS_TO_RESOLVE = AppearancePropFlags.FontData | AppearancePropFlags.ForeColor;
			context.UnresolvedProps &= PROPS_TO_RESOLVE;

			// SSP 8/3/06 BR14796
			// Resolve the add-new boxes font data appearance settings otherwise the add-new button will
			// end up drawing with a different font than one we measured the add-new button's size with.
			// This is because the add-new button will inherit it's font from the parent add-new box 
			// element. 
			// 
			this.ResolveAppearance( ref appData, ref context.UnresolvedProps );

			// call the layout's resolve appearance to fill in any properties that
			// weren't set already
			//
			this.Layout.ResolveAppearance( ref appData, ref context );

			// SSP 5/11/06 - App Styling - BR11781
			// RequestedProps parameter should be passed in by ref.
			// 
			requestedProps ^= ( requestedProps & PROPS_TO_RESOLVE ) ^ context.UnresolvedProps;
		}

		/// <summary>
		/// Resets the button appearance
		/// </summary>
		public void ResetButtonAppearance() 
		{
			if ( null != this.buttonAppearanceHolder )
			{
//				// remove the prop change notifications
//				//
//				this.buttonAppearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
//                    
//				this.buttonAppearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.buttonAppearanceHolder.Appearance.Reset();
				// Call reset on the holder instead of the appearance
				//
				this.buttonAppearanceHolder.Reset();
			}
		}



		internal override bool DefaultHidden
		{
			get
			{
				return true;
			}
		}	



		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
						
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			base.GetObjectData( info, context );

			if ( this.ShouldSerializeButtonAppearance() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ButtonAppearanceHolder", this.buttonAppearanceHolder );
				//info.AddValue("ButtonAppearanceHolder", this.buttonAppearanceHolder );
			}

			if ( this.ShouldSerializeStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Style", this.Style );
				//info.AddValue("Style", (int)this.Style );
			}

			if ( this.ShouldSerializeButtonStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ButtonStyle", this.ButtonStyle );
				//info.AddValue("ButtonStyle", (int)this.ButtonStyle );
			}
		}


        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        // AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//public AddNewBox( SerializationInfo info, StreamingContext context ) : base( info, context )
		private AddNewBox( SerializationInfo info, StreamingContext context ) : base( info, context )
		{
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "Style":
					{
						//this.Style = (Infragistics.Win.UltraWinGrid.AddNewBoxStyle)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Style = (AddNewBoxStyle)Utils.DeserializeProperty( entry, typeof(AddNewBoxStyle), this.Style );
						//this.Style = (AddNewBoxStyle)Utils.ConvertEnum(entry.Value, this.Style);
						break;
					}

					case "ButtonAppearanceHolder":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.buttonAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.buttonAppearanceHolder = (AppearanceHolder)entry.Value;
						break;
					}

					// JAS 7/21/05 BR05060 - This was never being deserialized.
					case "ButtonStyle":
					{
						this.ButtonStyle = (UIElementButtonStyle)Utils.DeserializeProperty( entry, typeof(UIElementButtonStyle), this.ButtonStyle );
						break;
					}
				}
			}
		}



		// MRS 2/23/04 - Added Preset support
		#region Implementation of ISupportPresets
		/// <summary>
		/// Returns a list of properties which can be used in a Preset
		/// </summary>
		/// <param name="presetType">Determines which type(s) of properties are returned</param>
		/// <returns>An array of strings indicating property names</returns>
		string[] ISupportPresets.GetPresetProperties(Infragistics.Win.PresetType presetType)
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList properties = new ArrayList();
			List<string> properties = new List<string>();

			//Appearance
			if ((presetType & Infragistics.Win.PresetType.Appearance) == Infragistics.Win.PresetType.Appearance)
			{
				properties.Add("Appearance");				
				properties.Add("BorderStyle");	
				properties.Add("ButtonAppearance");	
				properties.Add("ButtonConnectorColor");	
				properties.Add("ButtonConnectorStyle");	
				properties.Add("ButtonStyle");
				// MRS 5/26/04 - Moved to Behavior
				//properties.Add("Hidden");
				properties.Add("Style");	
			}

			//Behavior
			if ((presetType & Infragistics.Win.PresetType.Behavior) == Infragistics.Win.PresetType.Behavior)
			{				
				// MRS 5/26/04 - Moved from Appearance
				properties.Add("Hidden");	
			}
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (string[])properties.ToArray(typeof(string));
			return properties.ToArray();
		}

		/// <summary>
		/// Returns the TypeName of the Preset target
		/// </summary>
		/// <returns>Returns "AddNewBox"</returns>
		string ISupportPresets.GetPresetTargetTypeName()
		{
			return "AddNewBox";
		}
		#endregion Implementation of ISupportPresets
		
	}

	
}
