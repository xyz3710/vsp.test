#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Collections.Generic;

// SSP 4/23/02
// Added SummaryValuesCollection class for Summary Rows Feature
//

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// Collection of <see cref="SummaryValue"/> objects that the RowsCollection exposes via its <see cref="RowsCollection.SummaryValues"/> property.
	/// </summary>
	/// <remarks>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand.Summaries"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummarySettings"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummaryValue"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.SummaryValuesCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.RowsCollection.SummaryValues"/>
	/// </remarks>
	public sealed class SummaryValuesCollection : SubObjectsCollectionBase
	{
		#region Private Variables

		private RowsCollection rows = null;

		private int verifiedSummariesVersion = 0;

		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Added SummaryFooterCaption property on the SummaryValuesCollection.
		//
		private string summaryFooterCaption = null;

		#endregion  // Private Variables

		#region Constructor

		internal SummaryValuesCollection( RowsCollection rows ) : base( )
		{
			if ( null == rows )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_342"), Shared.SR.GetString("LE_ArgumentNullException_361") );

			Debug.Assert( null != rows.Band, "RowsCollection has null band !" );

			this.rows = rows;
		}

		#endregion // Constructor

		#region Protected properties
		
		#region InitialCapacity

		/// <summary>
		/// Property that specifies the initial capacity
		/// of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return 5;
			}
		}

		#endregion // InitialCapacity

		#endregion // Protected properties

		#region Public methods
		
		#region GetItem

		/// <summary>
		/// Returns the item at the specified index
		/// </summary>
        /// <param name="index">Index of the object to retrieve.</param>
        /// <returns>The object at the index</returns>
        public override object GetItem(int index)
		{
			this.VerifySummariesVersion( );

			// call the base class implementaion
			//
			return base.GetItem( index );
		}

		#endregion // GetItem

		#region indexer

		/// <summary>
		/// indexer 
		/// </summary>
		public SummaryValue this[ int index ]
		{
			get
			{
				this.VerifySummariesVersion( );

				return (SummaryValue)base.GetItem( index );
			}
		}

		// SSP 8/15/02 UWG1435
		// Made the SummarySettingsCollection keyed collection so also adding 
		// a keyed indexer to summary values collection since there is one to
		// one correspondence between elements of SummarySettingsCollection
		// and the associated SummaryValuesCollection.
		//
		/// <summary>
		/// indexer 
		/// </summary>
		public SummaryValue this[ string key ]
		{
			get
			{
				Debug.Assert( null != this.Band, "No band !" );

				if ( null == this.Band )
					return null;

				this.VerifySummariesVersion( );

				SummarySettings summary = this.Band.Summaries[ key ];

				SummaryValue summaryValue = this.GetSummaryValueFor( summary );

                if ( null == summaryValue )
					throw new ArgumentException(
						Infragistics.Shared.SR.GetString( "LER_ArguementException_1" ), 
						Infragistics.Shared.SR.GetString( "LER_ArguementException_2" ) );

				return summaryValue;
			}
		}

		// SSP 7/27/04 - UltraCalc
		// Added an indexer that gets the SummaryValue associated with a summary settings object.
		//
		/// <summary>
		/// Indexer.
		/// </summary>
		public SummaryValue this[ SummarySettings summarySettings ]
		{
			get
			{
				this.VerifySummariesVersion( );

				SummaryValue summaryValue = this.GetSummaryValueFor( summarySettings );

				if ( null == summaryValue )
					throw new ArgumentException( "SummarySettings is not a valid summary from this band.", "summarySettings" );

				return summaryValue;
			}
		}

		#endregion // indexer

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.SummaryValue[] array, int index)
		{
			this.VerifySummariesVersion();

			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo

		// SSP 6/23/03 - Excel Exporting
		//
		#region GetSummaryForPositionColumn

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		// Added an overload of GetSummaryValueFromPosition that takes in context.
		//
		/// <summary>
		/// Finds a summary value that will be displayed at position designated by summaryPosition and positionColumn parameters. If summaryPosition is UseSummaryPositionColumn, it will find a fixed summary that will be displayed under positionColumn at the level specified by the level parameter. If the summaryPosition is Left, Center or Right then the positionColumn parameter will be ignored and it will find a free-form summary that will be displayed under that area at the level specified by the level parameter.
		/// </summary>
		/// <param name="summaryPosition">Summary position.</param>
		/// <param name="positionColumn">This must be non-null if the summaryPosition parameter is UseSummaryPositionColumn.</param>
		/// <param name="level">Specifies the level. Level starts from 0.</param>
        /// <returns>The summary value of the summary at the specified position.</returns>
		public SummaryValue GetSummaryValueFromPosition( SummaryPosition summaryPosition, UltraGridColumn positionColumn, int level )
		{
			return this.GetSummaryValueFromPosition( summaryPosition, positionColumn, level, null );
		}

		// SSP 5/18/05 - NAS 5.2 Extension of Summaries Functionality
		// Added an overload of GetSummaryValueFromPosition that takes in summaryRowContext.
		// This is used by the excel exporter component.
		//
		/// <summary>
		/// Finds a summary value that will be displayed at position designated by summaryPosition and positionColumn parameters. If summaryPosition is UseSummaryPositionColumn, it will find a fixed summary that will be displayed under positionColumn at the level specified by the level parameter. If the summaryPosition is Left, Center or Right then the positionColumn parameter will be ignored and it will find a free-form summary that will be displayed under that area at the level specified by the level parameter.
		/// </summary>
		/// <param name="summaryPosition">Summary position.</param>
		/// <param name="positionColumn">This must be non-null if the summaryPosition parameter is UseSummaryPositionColumn.</param>
		/// <param name="level">Specifies the level. Level starts from 0.</param>
		/// <param name="summaryRowContext">Specifies the level. Level starts from 0.</param>
        /// <returns>The summary value of the summary at the specified position.</returns>
		public SummaryValue GetSummaryValueFromPosition( SummaryPosition summaryPosition, UltraGridColumn positionColumn, int level, UltraGridSummaryRow summaryRowContext )
		{
			SummaryDisplayAreaContext context = null != summaryRowContext
				? summaryRowContext.SummaryDisplayAreaContext
				: new SummaryDisplayAreaContext( SummaryDisplayAreas.Bottom );

			return this.GetSummaryValueFromPosition( context, summaryPosition, positionColumn, level );
		}

        internal SummaryValue GetSummaryValueFromPosition(SummaryPosition summaryPosition, UltraGridColumn positionColumn, int level, SummaryDisplayAreas matchAny)
        {
            SummaryDisplayAreaContext context = new SummaryDisplayAreaContext(matchAny);
            return this.GetSummaryValueFromPosition(context, summaryPosition, positionColumn, level);
        }

		private SummaryValue GetSummaryValueFromPosition( SummaryDisplayAreaContext context, SummaryPosition summaryPosition, UltraGridColumn positionColumn, int level )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<SummarySettings> list = new List<SummarySettings>();

			if ( SummaryPosition.UseSummaryPositionColumn == summaryPosition )
			{
				if ( null == positionColumn )
					throw new ArgumentNullException( "positionColumn", "You must specify a valid positionColumn if summaryPosition is UseSummaryPositionColumn." );

				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				// Added an overload of GetSummaryValueFromPosition that takes in context.
				//
				//this.Band.Summaries.GetFixedSummariesForLevel( level, list );
				this.Band.Summaries.GetFixedSummariesForLevel( context, level, list );
			}
			else
			{
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				// Added an overload of GetSummaryValueFromPosition that takes in context.
				//
				//this.Band.Summaries.GetFreeFormSummariesForLevel( level, list );
				this.Band.Summaries.GetFreeFormSummariesForLevel( context, level, list );
			}
			
			for ( int i = 0; i < list.Count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//SummarySettings summary = (SummarySettings)list[ i ];
				SummarySettings summary = list[ i ];

				if ( SummaryPosition.UseSummaryPositionColumn == summaryPosition )
				{
					if ( null != summary && summary.SummaryPositionColumnResolved == positionColumn )
						return this.GetSummaryValueFor( summary );
				}
				else
				{
					if ( null != summary && summaryPosition == summary.SummaryPosition )
						return this.GetSummaryValueFor( summary );
				}
			}

			return null;
		}

//		/// <summary>
//		/// Finds a free-form summary value that will be displayed at position designated by summaryPosition. SummaryPosition must be one of Left, Center or Right.
//		/// </summary>
//		/// <param name="summaryPosition">Summary position.</param>
//		/// <param name="level">Specifies the level. Level starts from 0.</param>
//		/// <returns></returns>
//		public SummaryValue GetSummaryValueFromPosition( SummaryPosition summaryPosition, int level )
//		{
//			return this.GetSummaryValueFromPosition( summaryPosition, level );
//		}

		#endregion // GetSummaryForPositionColumn


        // MRS v7.2 - Document Exporter
        //
        #region GetFixedSummaryLineHeight

        /// <summary>
        /// Returns the height for the specified level of the fixed summary area of the summary
        /// footer. If there are no summary settings for that level, then it will return 0.
        /// NOTE: This method includes the borders.
        /// </summary>
        /// <param name="summaryRowContext">An UltraGridSummaryRow to use as context</param>
        /// <param name="level">The level of the summary</param>        
        /// <returns>The height for the specified level of the fixed summary area of the summary footer.</returns>
        [EditorBrowsable( EditorBrowsableState.Advanced)]
        public int GetFixedSummaryLineHeight(int level, UltraGridSummaryRow summaryRowContext)
        {
            SummaryDisplayAreaContext context = null != summaryRowContext
                ? summaryRowContext.SummaryDisplayAreaContext
                : new SummaryDisplayAreaContext(SummaryDisplayAreas.Bottom);

            return this.GetFixedSummaryLineHeight(context, level);
        }

        #endregion // GetFixedSummaryLineHeight

        // MRS v7.2 - Document Exporter
        //
        #region GetFreeFormSummaryLineHeight

        /// <summary>
        /// Returns the height for the specified level of the freeform summary area of the summary
        /// footer. If there are no summary settings for that level, then it will return 0.
        /// NOTE: This method includes the borders.
        /// </summary>
        /// <param name="summaryRowContext">An UltraGridSummaryRow to use as context</param>
        /// <param name="level">The level of the summary</param>
        /// <returns></returns>
        [EditorBrowsable( EditorBrowsableState.Advanced)]
        public int GetFreeFormSummaryLineHeight(int level, UltraGridSummaryRow summaryRowContext)
        {
            SummaryDisplayAreaContext context = null != summaryRowContext
                 ? summaryRowContext.SummaryDisplayAreaContext
                 : new SummaryDisplayAreaContext(SummaryDisplayAreas.Bottom);

            return this.GetFreeFormSummaryLineHeight(context, level);
        }

        #endregion // GetFixedSummaryLineHeight

        // MRS v7.2 - Document Exporter
        //
        #region GetFreeFormFooterAreaHeight

        /// <summary>
        /// Returns the height required for the free-form footer area. It returns 0 if
        /// there are no summaries to be displayed in free-form area of the summary
        /// footer. NOTE: This method includes the borders.
        /// </summary>
        /// <param name="summaryRowContext">An UltraGridSummaryRow to use as context</param>
        /// <returns>The height required for the free-form footer area.</returns>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public int GetFreeFormFooterAreaHeight(UltraGridSummaryRow summaryRowContext)
        {
            SummaryDisplayAreaContext context = null != summaryRowContext
                 ? summaryRowContext.SummaryDisplayAreaContext
                 : new SummaryDisplayAreaContext(SummaryDisplayAreas.Bottom);

            return this.GetFreeFormFooterAreaHeight(context);
        }

        #endregion // GetFixedFooterAreaHeight

        // MRS v7.2 - Document Exporter
        //
        #region GetFixedFooterAreaHeight

        /// <summary>
        /// Returns the height required for the fixed footer area. It returns 0 if
        /// there are no summaries to be displayed in fixed area of the summary
        /// footer. NOTE: This method includes the borders.
        /// </summary>
        /// <param name="summaryRowContext">An UltraGridSummaryRow to use as context</param>
        /// <returns>The height required for the fixed footer area.</returns>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public int GetFixedFooterAreaHeight(UltraGridSummaryRow summaryRowContext)
        {
            SummaryDisplayAreaContext context = null != summaryRowContext
                 ? summaryRowContext.SummaryDisplayAreaContext
                 : new SummaryDisplayAreaContext(SummaryDisplayAreas.Bottom);

            return this.GetFixedFooterAreaHeight(context);
        }

        #endregion // GetFixedFooterAreaHeight

        // MRS v7.2 - Document Exporter
        //
        #region GetCaptionAreaHeight

        /// <summary>
        /// Returns the height required for the caption of the summary footer.
        /// It returns 0 if there is no caption to be displayed.
        /// NOTE: This method includes the borders.
        /// </summary>
        /// <param name="summaryRowContext">An UltraGridSummaryRow to use as context</param>
        /// <returns>the height required for the caption of the summary footer.</returns>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public int GetCaptionAreaHeight(UltraGridSummaryRow summaryRowContext)
        {
            SummaryDisplayAreaContext context = null != summaryRowContext
                 ? summaryRowContext.SummaryDisplayAreaContext
                 : new SummaryDisplayAreaContext(SummaryDisplayAreas.Bottom);

            return this.GetCaptionAreaHeight(context);
        }

        #endregion // GetCaptionAreaHeight

        #endregion // Public methods

        #region Public properties

        #region IsReadOnly

        /// <summary>
		/// Returns true if the collection is read-only
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		#endregion // IsReadOnly

		#region Count

		/// <summary>
		/// Overridden. Returns the number of elements in this collection.
		/// </summary>
		public override int Count
		{
			get
			{
				this.VerifySummariesVersion( );

				return base.Count;
			}
		}

		#endregion // Count

		#region SummaryFooterCaption

		// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
		// Added SummaryFooterCaption property on the SummaryValuesCollection.
		//
		/// <summary>
		/// Gets or sets summary footer caption substitution string. This will be displayed
		/// in the caption of the associated summary footer.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can use this property to specify the summary footer caption for 
		/// individual summary footers. A good place to initialize this property is in 
		/// <see cref="UltraGridBase.InitializeRowsCollection"/> event or the <see cref="UltraGrid.SummaryValueChanged"/> event.
		/// </p>
		/// <p class="body">This property specifies a substitution string where certain tokens get replaced by the associated values. See <see cref="UltraGridBand.SummaryFooterCaption"/> for more information.</p>
		/// <p class="body">Summary captions can be hidden using the <see cref="UltraGridOverride.SummaryFooterCaptionVisible"/> property.</p>
		/// <seealso cref="UltraGridOverride.SummaryFooterCaptionVisible"/>
		/// </remarks>
		public string SummaryFooterCaption
		{
			get
			{
				return this.summaryFooterCaption;
			}
			set
			{
				if ( this.summaryFooterCaption != value )
				{
					this.summaryFooterCaption = value;

					if ( null != this.Rows )
						this.Rows.InvalidateRowCollection( true );

					this.NotifyPropChange( PropertyIds.SummaryFooterCaption );
				}
			}
		}

		#endregion // SummaryFooterCaption

		// SSP 6/24/03 - Excel Exporting
		//
		#region SummaryFooterCaptionResolved

		/// <summary>
		/// Resolved summary footer caption for the summary footer associated with this summary values collection.
		/// </summary>
		public string SummaryFooterCaptionResolved
		{
			get
			{
				string ret = this.GetSummaryFooterCaption( );

				return null != ret ? ret : "";
			}
		}

		#endregion // SummaryFooterCaptionResolved

		#region SummaryFooterCaptionVisibleResolved

		// SSP 8/7/03 - Excel Exporting
		// Added SummaryFooterCaptionVisibleResolved.
		//
		/// <summary>
		/// Indicates whether the summary footer caption is visible or not.
		/// </summary>
		public bool SummaryFooterCaptionVisibleResolved
		{
			get
			{
				return this.Band.SummaryFooterCaptionVisibleResolved;
			}
		}

		#endregion // SummaryFooterCaptionVisibleResolved

		#endregion // Public properties

		#region Private/Internal Properties

		#region Rows

		internal RowsCollection Rows
		{
			get
			{
				return this.rows;
			}
		}

		#endregion // Rows

		#region Band

		internal UltraGridBand Band
		{
			get
			{
				return this.Rows.Band;
			}
		}

		#endregion // Band

		#region BorderStyleSummaryCaption

		internal UIElementBorderStyle BorderStyleSummaryCaption
		{
			get
			{
				return this.Band.BorderStyleSummaryValueResolved;
			}
		}

		#endregion // BorderStyleSummaryCaption

		#region BorderStyleSummaryArea

		internal UIElementBorderStyle BorderStyleSummaryArea
		{
			get
			{
				return this.Band.BorderStyleSummaryValueResolved;
			}
		}

		#endregion // BorderStyleSummaryArea

		#endregion // Private/Internal Properties

		#region Private/Internal Methods

		#region InternalClearHelper

		internal void InternalClearHelper( )
		{
			// Unhook from items. Make sure base class' methods are used
			// because we don't want to cause an infinite loop because this
			// method could have been called from VerifySummariesVersion and
			// we don't want to cause reverification
			//
			ArrayList list = this.List;
			int count = list.Count;

			for ( int i = 0; i < count; i++ )
			{
				SubObjectBase item = (SubObjectBase)list[i];

				if ( null != item )
					item.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			}

			this.InternalClear( );
		}

		#endregion // InternalClearHelper

		#region InternalRemoveHelper

		internal void InternalRemoveHelper( SummaryValue summaryValue )
		{
			this.InternalRemove( this.IndexOf( summaryValue ) );
		}


		internal void InternalRemoveHelper( int index )
		{
			SummaryValue summaryValue = base.GetItem( index ) as SummaryValue;

			if ( null != summaryValue )
				summaryValue.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			base.InternalRemove( summaryValue );
		}

		#endregion // InternalRemoveHelper

		#region InternalAddSummaryValueFor

		private void InternalAddSummaryValueFor( int index, SummarySettings summarySettings )
		{
			if ( null == summarySettings )
				return;

			SummaryValue summaryValue = new SummaryValue( this, summarySettings );

			summaryValue.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			this.InternalInsert( index, summaryValue );
		}

		#endregion // InternalAddSummaryValueFor

		#region GetSummaryValueFor

		internal SummaryValue GetSummaryValueFor( SummarySettings summarySettings )
		{
			// SSP 12/18/02
			// Call VerifySummariesVersion to verify the summary values.
			//
			this.VerifySummariesVersion( );

			ArrayList list = this.List;
			int count = list.Count;

			for ( int i = 0; i < count; i++ )
			{
				Debug.Assert( list[i] is SummaryValue, "An element in SummaryValuesCollection is not an isntance of SummaryValue !" );

				if ( list[i] is SummaryValue )
				{
					SummaryValue summaryValue = (SummaryValue)list[i];

					if ( null != summaryValue && summaryValue.SummarySettings == summarySettings )
						return summaryValue;

				}
			}

			return null;
		}

		#endregion // GetSummaryValueFor

		#region DoesSummarySettingsExistFor

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool DoesSummarySettingsExistFor( SummaryValue summaryValue, SummarySettingsCollection summaries )
		private static bool DoesSummarySettingsExistFor( SummaryValue summaryValue, SummarySettingsCollection summaries )
		{
			for ( int i = 0; i < summaries.Count; i++ )
			{
				if ( summaryValue.SummarySettings == summaries[i] )
					return true;
			}

			return false;
		}
		
		#endregion // DoesSummarySettingsExistFor

		#region VerifySummariesVersion

		internal void VerifySummariesVersion( )
		{
			if ( this.Band.Summaries.SummariesVersion == this.verifiedSummariesVersion )
				return;				 

			// Assign the summary version to the verified summary version before
			// we actually go ahead and verify so this way we can prevent any 
			// infinite loops.
			//
			this.verifiedSummariesVersion = this.Band.Summaries.SummariesVersion;

			if ( !this.Band.HasSummaries )
			{
				this.InternalClearHelper( );
			}
			else
			{
				SummarySettingsCollection summaries = this.Band.Summaries;

				ArrayList list = this.List;

				// Remove any SummaryValue objects that have no SummarySettings
				// objects in the summaries collection. This would be the
				// case if summary settings objects are removed from the summaries
				// collection.
				//
                // MRS 8/1/2008 - BR35252
                // Removing items from a list while looping through that list forward is a bad
                // idea. This will cause the wrong items to be removed (or not removed).
                // Loop backward instead. 
                //
                //for ( int i = 0; i < list.Count; i++ )
                for (int i = list.Count - 1; i >= 0; i--)
				{
					Debug.Assert( list[i] is SummaryValue, "An element in SummaryValuesCollection is not an isntance of SummaryValue !" );

					if ( list[i] is SummaryValue )
					{
						SummaryValue summaryValue = (SummaryValue)list[i];
					
						// If summaryValue has no associated summary settings object in
						// summaries collection, then remove it.
						//
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.DoesSummarySettingsExistFor( summaryValue, summaries ) )
						if ( !SummaryValuesCollection.DoesSummarySettingsExistFor( summaryValue, summaries ) )
							this.InternalRemoveHelper( summaryValue );
					}
					else
					{
						this.InternalRemoveHelper( i );
					}
				}


				// Now add new summary value objects for summary settings objects for which
				// there are no summary value objects currently.
				//
				for ( int i = 0; i < summaries.Count; i++ )
				{
					SummarySettings summary = summaries[i];

					SummaryValue summaryValue = this.GetSummaryValueFor( summary );

					if ( null == summaryValue )
						this.InternalAddSummaryValueFor( i, summary );
				}
			}
		}

		#endregion // VerifySummariesVersion

		#region GetSummaryFooterHeight

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal int GetSummaryFooterHeight( )
		internal int GetSummaryFooterHeight( SummaryDisplayAreaContext context )
		{
			UltraGridBand band = this.Band;
			UltraGridLayout layout = band.Layout;

			// SSP 7/18/02 UWG1389
			// Return 0 if there are no visible summaries.
			//
			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//if ( !this.Band.Summaries.HasSummaryVisibleInFooter )
			if ( ! band.Summaries.HasSummaryVisibleInFooter( context ) )
				return 0;

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//int fixedHeight = this.GetFixedFooterAreaHeight( );
			//int freeHeight  = this.GetFreeFormFooterAreaHeight( );
			//int captionHeight = this.GetCaptionAreaHeight( );
			int captionHeight = this.GetCaptionAreaHeight( context );
			int fixedHeight = this.GetFixedFooterAreaHeight( context );
			int freeHeight  = this.GetFreeFormFooterAreaHeight( context );

			// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
			// If the summary footer is displayed in the group-by row then the border style
			// is None. Changed from proeprty to method so we can pass the summary display
			// area context to it.
			//
			//int borderWidth = null != this.Band ? this.Band.Layout.GetBorderThickness( this.Band.BorderStyleSummaryFooterResolved ) : 2;
			UIElementBorderStyle footerBorderStyle = band.GetBorderStyleSummaryFooterResolved( context );
			int borderWidth = layout.GetBorderThickness( footerBorderStyle );

			// SSP 8/29/03 UWG2261
			// Take into account any merged borders.
			//
			// --------------------------------------------------------------------
			if ( layout.CanMergeAdjacentBorders( footerBorderStyle, band.BorderStyleSummaryValueResolved ) )
                borderWidth = 0;
			// --------------------------------------------------------------------

			return 2 * borderWidth + captionHeight + fixedHeight + freeHeight;
		}

		#endregion // GetSummaryFooterHeight
		
		#region GetAppearanceFontHeight

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added context parameter and as an optimization made appData parameter by reference.
		//
		//private int GetAppearanceFontHeight( AppearanceData appData )
		private int GetAppearanceFontHeight( SummaryDisplayAreaContext context, ref AppearanceData appData )
		{
			UltraGridBase grid = null;
			if ( null != this.Band && null != this.Band.Layout )
				grid = this.Band.Layout.Grid;
			Debug.Assert( null != grid, "No grid !" );

			if ( null == grid )
				return 0;

			Font font = grid.Font;

			Font createdFont = appData.CreateFont( font );

			if ( null != createdFont )
				font = createdFont;

			// AS 8/12/03 optimization - Use the new caching mechanism.
			// Plus, that method can take a null control.
			//
			//Graphics graphics = null != grid ? DrawUtility.CreateReferenceGraphics( grid ) : null;
			// MRS 6/1/04 - UWG2915
			//Graphics graphics = DrawUtility.GetCachedGraphics( grid );
			Graphics graphics = this.Band.Layout.GetCachedGraphics();

			int lineHeight = null != graphics 
                //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
				//? Size.Ceiling( graphics.MeasureString( "ghW", font ) ).Height
				// SSP 11/4/05 BR07555
				// Need to pass printing param when measuring in Whidbey. When printing,
				// in Whidbey we need use the GDI+ measuring.
				// 
				//? Size.Ceiling( DrawUtility.MeasureString( graphics, "ghW", font ) ).Height
				? this.Band.Layout.MeasureString( graphics, "ghW", font ).Height
				: font.Height;

			if ( null != createdFont )
				createdFont.Dispose( );
			createdFont = null;

			if ( null != graphics )
			{
				// AS 8/12/03 optimization - Use the new caching mechanism.
				//
				//graphics.Dispose( );
				// MRS 6/1/04 - UWG2915
				//DrawUtility.ReleaseCachedGraphics(graphics);
				this.Band.Layout.ReleaseCachedGraphics(graphics);
			}
			graphics = null;

			return lineHeight;
		}

		#endregion // GetAppearanceFontHeight

		#region GetSummaryValueAppearanceFontHeight

		// SSP 2/27/03 - Row Layout Functionality
		// Changed the access modifier from private to internal
		//
		//private int GetSummaryValueAppearanceFontHeight( )
		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added context parameter.
		//
		//internal int GetSummaryValueAppearanceFontHeight( )
		internal int GetSummaryValueAppearanceFontHeight( SummaryDisplayAreaContext context )
		{
			AppearanceData appData = new AppearanceData( );
			AppearancePropFlags flags = AppearancePropFlags.FontData;

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added context parameter.
			//
			//this.ResolveSummaryValueAppearance( ref appData, ref flags );
			//return this.GetAppearanceFontHeight( ref appData );
			this.ResolveSummaryValueAppearance( context, ref appData, ref flags );
			return this.GetAppearanceFontHeight( context, ref appData );
		}

		#endregion // GetSummaryValueAppearanceFontHeight

		#region GetSummaryLineHeightHelper

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added context parameter.
		//
		//private int GetSummaryLineHeightHelper( ArrayList list )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private int GetSummaryLineHeightHelper( SummaryDisplayAreaContext context, ArrayList list )
		private int GetSummaryLineHeightHelper( SummaryDisplayAreaContext context, List<SummarySettings> list )
		{
			// Measure the height of the font off the SummaryValueAppearance off the override
			//
			int lineHeight = this.GetSummaryValueAppearanceFontHeight( context );
			int maxLines = 0; // max height for the level.				
	
			for ( int i = 0; i < list.Count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//SummarySettings summarySettings = list[i] as SummarySettings;
				SummarySettings summarySettings = list[ i ];

				Debug.Assert( null != summarySettings, "list passed into GetSummaryLineHeightHelper contains an element other than SummarySettings object !" );

				if ( null != summarySettings )
					maxLines = Math.Max( maxLines, summarySettings.Lines );
			}

			if ( maxLines <= 0 )
				return 0;

			int totalHeight = maxLines * lineHeight;

            // MRS 11/18/2008 - TFS10343
            // Take the image into account
            //
            int imageHeight = this.GetSummaryValueAppearanceImageHeight(context);
            totalHeight = Math.Max(totalHeight, imageHeight);

			// Now take into account any border settings.
			//
			Debug.Assert( null != this.Band.Layout, "No layout !" );
			if ( null != this.Band.Layout )
			{
				UIElementBorderStyle borderStyle = this.Band.BorderStyleSummaryValueResolved;
				int borderWidth = this.Band.Layout.GetBorderThickness( borderStyle );
				totalHeight += 2 * borderWidth;

				// SSP 8/29/03 UWG2261
				// Add some padding. Use cell padding.
				//
				totalHeight += 2 * this.Band.CellPaddingResolved;
			}

			return totalHeight;
		}

		#endregion // GetSummaryLineHeightHelper

		#region GetFixedSummaryLineHeight

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal int GetFixedSummaryLineHeight( int level )
		internal int GetFixedSummaryLineHeight( SummaryDisplayAreaContext context, int level )
		{
			if ( !this.Band.HasSummaries )
				return 0;

			// SSP 2/27/03 - Row Layout Functionality
			// We are going to layout fixed summaries the way cells are laid out so just return
			// the height of the cells layout area.
			//
			if ( this.Band.UseRowLayoutResolved )
			{
				// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
				// Use the new GetSummaryLayoutPreferredSize instead because the height can depend
				// on the summary display area context.
				//
				//return 0 == level ? this.Band.PreferredSummaryLayoutSize.Height : 0;
				return 0 == level ? this.Band.GetSummaryLayoutPreferredSize( context ).Height : 0;
			}

			SummarySettingsCollection summaries = this.Band.Summaries;

			// If we don't have any fixed summaries, then no fixed summary footer area.
			// So return 0.
			//
			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//if ( !summaries.HasFixedSummaries )
			if ( !summaries.HasFixedSummaries( context ) )
				return 0;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<SummarySettings> list = new List<SummarySettings>();

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//summaries.GetFixedSummariesForLevel( level, list );
			summaries.GetFixedSummariesForLevel( context, level, list );

			if ( list.Count <= 0 )
				return 0;

			// SSP 8/29/03 UWG2261
			// Account for merged borders.
			// 
			//return this.GetSummaryLineHeightHelper( list );
			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added context parameter.
			//
			//int height = this.GetSummaryLineHeightHelper( list );
			int height = this.GetSummaryLineHeightHelper( context, list );
			return height - 
				( height > 0 && level > 0 && this.Band.Layout.CanMergeAdjacentBorders( this.Band.BorderStyleSummaryValueResolved ) 
				? this.Band.Layout.GetBorderThickness( this.Band.BorderStyleSummaryValueResolved  )
				: 0 );
		}

		#endregion // GetFixedSummaryLineHeight
		
		#region GetFreeFormSummaryLineHeight

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal int GetFreeFormSummaryLineHeight( int level )
		internal int GetFreeFormSummaryLineHeight( SummaryDisplayAreaContext context, int level )
		{
			if ( !this.Band.HasSummaries )
				return 0;

			SummarySettingsCollection summaries = this.Band.Summaries;

			// If we don't have any fixed summaries, then no fixed summary footer area.
			// So return 0.
			//
			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//if ( !summaries.HasFreeFormSummaries )
			if ( ! summaries.HasFreeFormSummaries( context ) )
				return 0;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<SummarySettings> list = new List<SummarySettings>();

			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//summaries.GetFreeFormSummariesForLevel( level, list );
			summaries.GetFreeFormSummariesForLevel( context, level, list );

			if ( list.Count <= 0 )
				return 0;

			// SSP 8/29/03 UWG2261
			// Account for merged borders.
			// 
			//return this.GetSummaryLineHeightHelper( list );
			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//int height = this.GetSummaryLineHeightHelper( list );
			int height = this.GetSummaryLineHeightHelper( context, list );
			return height - 
				( height > 0 && level > 0 && this.Band.Layout.CanMergeAdjacentBorders( this.Band.BorderStyleSummaryValueResolved ) 
				? this.Band.Layout.GetBorderThickness( this.Band.BorderStyleSummaryValueResolved  )
				: 0 );
		}

		#endregion // GetFixedSummaryLineHeight

		#region GetFreeFormFooterAreaHeight

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal int GetFreeFormFooterAreaHeight( )
		internal int GetFreeFormFooterAreaHeight( SummaryDisplayAreaContext context )
		{
			if ( !this.Band.HasSummaries )
				return 0;

			SummarySettingsCollection summaries = this.Band.Summaries;

			// If we don't have any fixed summaries, then no fixed summary footer area.
			// So return 0.
			//
			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//if ( !summaries.HasFreeFormSummaries )
			if ( ! summaries.HasFreeFormSummaries( context ) )
				return 0;

			int totalHeight = 0;

			for ( int i = 0; ; i++ )
			{
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				//int summaryLevelHeight = this.GetFreeFormSummaryLineHeight( i );
				int summaryLevelHeight = this.GetFreeFormSummaryLineHeight( context, i );

				if ( summaryLevelHeight <= 0 )
					break;

				totalHeight += summaryLevelHeight;
			}

			

			return totalHeight;
		}

		#endregion // GetFixedFooterAreaHeight

		#region GetFixedFooterAreaHeight

		// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
		// Added ability to display summaries in group-by rows aligned with columns, 
		// summary footers for group-by row collections, fixed summary footers and
		// being able to display the summary on top of the row collection.
		//
		//internal int GetFixedFooterAreaHeight( )
		internal int GetFixedFooterAreaHeight( SummaryDisplayAreaContext context )
		{
			if ( !this.Band.HasSummaries )
				return 0;

			SummarySettingsCollection summaries = this.Band.Summaries;

			// If we don't have any fixed summaries, then no fixed summary footer area.
			// So return 0.
			//
			// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
			// Added ability to display summaries in group-by rows aligned with columns, 
			// summary footers for group-by row collections, fixed summary footers and
			// being able to display the summary on top of the row collection.
			//
			//if ( !summaries.HasFixedSummaries )
			if ( ! summaries.HasFixedSummaries( context ) )
				return 0;

			int totalHeight = 0;

			for ( int i = 0; ; i++ )
			{
				// SSP 4/13/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				//int summaryLevelHeight = this.GetFixedSummaryLineHeight( i );
				int summaryLevelHeight = this.GetFixedSummaryLineHeight( context, i );

				if ( summaryLevelHeight <= 0 )
					break;

				totalHeight += summaryLevelHeight;
			}

			

			return totalHeight;
		}

		#endregion // GetFixedFooterAreaHeight

		#region GetCaptionAreaHeight

		// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
		//
		//internal int GetCaptionAreaHeight( )
		internal int GetCaptionAreaHeight( SummaryDisplayAreaContext context )
		{
			if ( null == this.Band || !this.Band.SummaryFooterCaptionVisibleResolved 
				// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
				// Group-by rows do not show summary captions.
				//
				|| context.IsGroupByRowSummaries )
				return 0;

			int borderWidth = this.Band.Layout.GetBorderThickness( this.Band.BorderStyleSummaryFooterCaptionResolved );

			AppearanceData appData = new AppearanceData( );
			AppearancePropFlags flags = AppearancePropFlags.FontData;

			// SSP 10/23/02
			// Resolve the caption appearance rather than the footer appearance.
			//this.ResolveSummaryFooterAppearance( ref appData, ref flags );
			this.ResolveSummaryFooterCaptionAppearance( ref appData, ref flags );

			// Since we don't have any appearance for summary captions, just use
			// the summary value appearance.
			//
			// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
			// Added context parameter.
			//
			//return 2 * borderWidth + this.GetAppearanceFontHeight( appData );
			return 2 * borderWidth + this.GetAppearanceFontHeight( context, ref appData );
		}

		#endregion // GetCaptionAreaHeight

		#region DirtySummaryValues

		internal bool DirtySummaryValues( UltraGridBand band )
		{
			bool ret = false;

			for ( int i = 0; i < this.List.Count; i++ )
			{
				SummaryValue summaryValue = this.List[i] as SummaryValue;

				UltraGridBand sourceColumnBand = 
					null != summaryValue && null != summaryValue.SummarySettings
					&& null != summaryValue.SummarySettings.SourceColumn
					? summaryValue.SummarySettings.SourceColumn.Band
					: null;

				if ( null != sourceColumnBand &&
					( sourceColumnBand == band || sourceColumnBand.IsDescendantOfBand( band ) ) )
				{
					summaryValue.DirtySummaryValue( );
					ret = true;
				}
			}

			return ret;
		}

		#endregion // DirtySummaryValues

		#region ResolveSummaryValueAppearanceHelper

		// SSP 2/27/03 - Row Layout Functionality
		// Created ResolveSummaryValueAppearanceHelper method and moved code from ResolveSummaryValueAppearance
		// method into this method so we can call it from summary layout logic without a reference to 
		// summary values collection which we don't need for the purposes of this mehtod.
		//
		internal static void ResolveSummaryValueAppearanceHelper( UltraGridBand band, 
			ref AppearanceData appData, ref AppearancePropFlags propFlags,
			// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
			// Added summaryDisplayAreaContext parameter.
			//
			SummaryDisplayAreaContext summaryDisplayAreaContext )
		{
			if ( null == band )
				return;

			// First merge the SummaryValueAppearance off the band's override.
			//
			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof( SummaryValueUIElement ), propFlags );

			// SSP 3/13/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( band, StyleUtils.Role.SummaryValue, out context.ResolutionOrder );

			// 5/3/05 - Optimizations
			// Use the MergeBLOverrideAppearances method instead.
			//
			band.MergeBLOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.SummaryValueAppearance 
				// SSP 3/13/06 - App Styling
				// 
				, AppStyling.RoleState.Normal );

			propFlags = context.UnresolvedProps;
			

			// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
			// If summaries are in group-by rows then only use the cell border colors.
			//
			//AppearancePropFlags flags = propFlags & ( AppearancePropFlags.BackColor | AppearancePropFlags.ForeColor | AppearancePropFlags.BorderColor );
			AppearancePropFlags flags = propFlags & (
				null != summaryDisplayAreaContext && summaryDisplayAreaContext.IsGroupByRowSummaries
				? AppearancePropFlags.BorderColor | AppearancePropFlags.BorderColor3DBase 
				: AppearancePropFlags.BackColor | AppearancePropFlags.ForeColor | AppearancePropFlags.BorderColor );

			if ( 0 != flags )
			{
				AppearancePropFlags origFlags = flags;

				// SSP 7/16/02
				// Commented below code out because we are already resolving the summary value appearance
				// above.
				//
				

				bool resolvingCellBackColor =  AppearancePropFlags.BackColor == ( flags & AppearancePropFlags.BackColor );

				// SSP 7/16/02
				//
				// -----------------------------------------------------------------------------
				ResolveAppearanceContext cellContext = 
					new ResolveAppearanceContext( typeof( UltraGridCell ), flags );
				band.ResolveAppearance( ref appData, ref context );
				flags = cellContext.UnresolvedProps;
				// -----------------------------------------------------------------------------

				// SSP 7/16/02
				// Added code above took below code out.
				//
				
					
				if ( resolvingCellBackColor )
					appData.BackColor = UltraGridLayout.DarkenColor( appData.BackColor, UltraGridLayout.SUMMARY_FOOTER_SHADE_AMOUNT );

				propFlags ^= ( origFlags ^ flags );
			}			

			if ( 0 == propFlags )
				return;

			// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
			// Added summaryDisplayAreaContext parameter. Enclosed the existing code into the if block.
			// 
			if ( null == summaryDisplayAreaContext || ! summaryDisplayAreaContext.IsGroupByRowSummaries )
			{
				// Using SummaryValueUIElement instead of SummaryValue object in order to be
				// consistent with resolving summary footer appearance since there is no summary footer
				// object, had to use SummaryFooterUIElement.
				//
				context.UnresolvedProps = propFlags;
				band.ResolveAppearance( ref appData, ref context );
				propFlags = context.UnresolvedProps;

				if ( 0 == propFlags )
					return;

				// Resolve the remaining props by calling ResolveAppearance off the band which
				// will eventually call the layout's ResolveAppearance and assign any
				// defaults as well.
				//
				context = new ResolveAppearanceContext( typeof( SummaryFooterUIElement ), propFlags );
				band.ResolveAppearance( ref appData, ref context );
				propFlags = context.UnresolvedProps;
			}
		}

		#endregion // ResolveSummaryValueAppearanceHelper

		#region ResolveSummaryValueAppearance

		// SSP 11/22/02 
		// Made ResolveSummaryValueAppearance public. 
		//
		/// <summary>
		/// Resolves the summary value apperanace from the band.
		/// </summary>
        /// <param name="appData">The structure to contain the resolved apperance.</param>
        /// <param name="propFlags">Bit flags indictaing which properties to resolve.</param>
        //internal void ResolveSummaryValueAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		public void ResolveSummaryValueAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		{
			this.ResolveSummaryValueAppearance( SummaryDisplayAreaContext.BottomScrollingSummariesContext, ref appData, ref propFlags );
		}

		// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
		// Added an overload that takes in the new summaryDisplayAreaContext parameter.
		// 
		internal void ResolveSummaryValueAppearance( SummaryDisplayAreaContext summaryDisplayAreaContext,
			ref AppearanceData appData, ref AppearancePropFlags propFlags  )
		{
			Debug.Assert( null != this.Rows && null != this.Rows.Band );

			// Use the cell's back color and border color for summary values by default.
			//
			if ( null != this.Rows && null != this.Rows.Band )
			{
				// SSP 2/27/03 - Row Layout Functionality
				// Moved below code into ResolveSummaryValueAppearanceHelper helper.
				// Commented out the moved code.
				//
				// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
				// Pass along the new summaryDisplayAreaContext parameter.
				// 
				//SummaryValuesCollection.ResolveSummaryValueAppearanceHelper( this.Rows.Band, ref appData, ref propFlags );
				SummaryValuesCollection.ResolveSummaryValueAppearanceHelper( this.Rows.Band, ref appData, ref propFlags, summaryDisplayAreaContext );
				
			}
		}

		#endregion // ResolveSummaryValueAppearance

		#region ResolveSummaryFooterAppearance

		// SSP 11/22/02 
		// Made ResolveSummaryFooterAppearance public. 
		//
		/// <summary>
		/// Resolves the appearance used by the summary footer.
		/// </summary>
        /// <param name="appData">The structure to contain the resolved apperance.</param>
        /// <param name="propFlags">Bit flags indictaing which properties to resolve.</param>
        //internal void ResolveSummaryFooterAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		public void ResolveSummaryFooterAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		{
			this.ResolveSummaryFooterAppearance(  
				SummaryDisplayAreaContext.BottomScrollingSummariesContext, ref appData, ref propFlags );
		}

        // MRS 5/9/2008 - BR32722
        // Added an overload that takes an UltraGridSummaryRow
        //
        /// <summary>
        /// Resolves the appearance used by the summary footer.
        /// </summary>
        /// <param name="appData"></param>
        /// <param name="propFlags"></param>        
        /// <param name="summaryRow"></param>
        public void ResolveSummaryFooterAppearance(UltraGridSummaryRow summaryRow, ref AppearanceData appData, ref AppearancePropFlags propFlags)
        {
            this.ResolveSummaryFooterAppearance(
                summaryRow.SummaryDisplayAreaContext, ref appData, ref propFlags);
        }

		// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
		// Added an overload of ResolveSummaryFooterAppearance that takes in 
		// summaryDisplayAreaContext parameter.
		// 
		internal void ResolveSummaryFooterAppearance( 
			SummaryDisplayAreaContext summaryDisplayAreaContext,
			ref AppearanceData appData, ref AppearancePropFlags propFlags )
		{
			UltraGridBand band = this.Band;
			Debug.Assert( null != band, "No band !" );

			if ( null != band )
			{
				// SSP 5/3/05 - NAS 5.2 Extension of Summaries Functionality
				// Summary footer elements in group-by rows do not draw themselves. Don't
				// resolve the footer appearance for such elements. Not only that the 
				// summary values in the group-by rows should make use of the group-by row 
				// appearance. Enclosed the existing code into the if block.
				// 
				if ( null == summaryDisplayAreaContext || ! summaryDisplayAreaContext.IsGroupByRowSummaries )
				{
					ResolveAppearanceContext context = new ResolveAppearanceContext( typeof( SummaryFooterUIElement ), propFlags );

					// SSP 3/13/06 - App Styling
					// 
					context.Role = StyleUtils.GetRole( band, StyleUtils.Role.SummaryFooter, out context.ResolutionOrder );

					band.ResolveAppearance( ref appData, ref context );
				}
			}
		}

		#endregion // ResolveSummaryFooterAppearance

		#region ResolveSummaryFooterCaptionAppearance

		// SSP 11/22/02 
		// Made ResolveSummaryFooterCaptionAppearance public. 
		//
		/// <summary>
		/// Resolves the appearance used by the summary footer caption.
		/// </summary>
        /// <param name="appData">The structure to contain the resolved apperance.</param>
        /// <param name="propFlags">Bit flags indictaing which properties to resolve.</param>
        //internal void ResolveSummaryFooterCaptionAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		public void ResolveSummaryFooterCaptionAppearance( ref AppearanceData appData, ref AppearancePropFlags propFlags )
		{
			UltraGridBand band = this.Band;
			Debug.Assert( null != band, "No band !" );

			if ( null != band )
			{
				ResolveAppearanceContext context = new ResolveAppearanceContext( typeof( SummaryFooterCaptionUIElement ), propFlags );

				// SSP 3/13/06 - App Styling
				// 
				context.Role = StyleUtils.GetRole( band, StyleUtils.Role.SummaryFooterCaption, out context.ResolutionOrder );

				band.ResolveAppearance( ref appData, ref context );
			}
		}

		#endregion // ResolveSummaryFooterCaptionAppearance

		#region ReplaceSummaryFooterCaptionToken

		internal string ReplaceSummaryFooterCaptionToken( string token )
		{
			UltraGridBand band = this.Band;

			// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
			// Added GROUPBYROWVALUE token. Added the following if block.
			//
			if ( "GROUPBYROWVALUE".Equals( token ) )
			{
				UltraGridGroupByRow parentGroupByRow = this.Rows.ParentRow as UltraGridGroupByRow;
				return null != parentGroupByRow ? parentGroupByRow.GetDisplayText( ) : string.Empty;
			}
			else if ( token.Equals( "BANDHEADER" ) )
			{
				string str = null != band.Header.Caption ? band.Header.Caption : band.Key;

				return null != str ? str : "";
			}
			else 
			{
				UltraGridBand parentBand = band.ParentBand;

				UltraGridColumn column = null;

				if ( token.Equals( "SCROLLTIPFIELD" ) )
				{
					column = null != parentBand ? parentBand.ScrollTipCol : null;
				}
				else
				{
					if ( null != parentBand && null != parentBand.Columns && parentBand.Columns.Exists( token ) )
						column = parentBand.Columns[ token ];
				}

				if ( null != column )
				{
					Debug.Assert( null != this.Rows, "Null rows !" );

					if ( null != this.Rows )
					{
						string str = this.Rows.TopLevelRowsCollection.ParentRow.GetCellText( column );
						return null != str ? str : column.NullTextResolved;
					}
				}
			}

			return token;
		}
		

		#endregion // ReplaceSummaryFooterCaptionToken

		#region GetSummaryFooterCaption

		/// <summary>
		/// Gets the string to be displayed in 
		/// </summary>
		/// <returns></returns>
		internal string GetSummaryFooterCaption( )
		{
			// SSP 5/5/05 - NAS 5.2 Extension of Summaries Functionality
			// Added SummaryFooterCaption property on the SummaryValuesCollection. Also
			// added SummaryFooterCaptionInternal to the band. This was done because we
			// need to resolve the summary footer caption differently based on whether the
			// summary footer is for the root row collection or a child row collection in
			// group-by scenario. We can't use the phrase "Grand Summaries" for group-by
			// row child rows.
			//
			//string captionStr = this.Band.SummaryFooterCaption;
			string captionStr = null != this.SummaryFooterCaption 
				? this.SummaryFooterCaption : this.Band.SummaryFooterCaptionInternal;

			if ( null == captionStr )
			{
				if ( this.Rows.IsRootRowsCollection )
				{
					captionStr = SR.GetString( "SummaryFooterCaption_RootRows" );
				}
				else if ( this.Rows.ParentRow.IsGroupByRow )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Remove unused locals
					//UltraGridGroupByRow parentGroupByRow = (UltraGridGroupByRow)this.Rows.ParentRow;

					captionStr = SR.GetString( "SummaryFooterCaption_GroupByChildRows" );
				}
				else
				{
					captionStr = SR.GetString( "SummaryFooterCaption_ChildBandNonGroupByRows" );
				}
			}

			int index = 0;

			System.Text.StringBuilder sb = new System.Text.StringBuilder( 10 + captionStr.Length );
			
			while ( index < captionStr.Length )
			{
				string substitutedStr = null;

				int bracketStartIndex = captionStr.IndexOf( '[', index );
				int bracketEndIndex   = -1;
				if ( bracketStartIndex >= 0 )
				{
					bracketEndIndex = captionStr.IndexOf( ']', 1 + bracketStartIndex );

					if ( bracketEndIndex > 0 )
					{
						substitutedStr = this.ReplaceSummaryFooterCaptionToken( captionStr.Substring( 1 + bracketStartIndex, bracketEndIndex - bracketStartIndex - 1 ) );
					}
				}

				if ( null != substitutedStr )
				{
					if ( null == sb )
						sb = new System.Text.StringBuilder( captionStr.Length + substitutedStr.Length );

					sb.Append( captionStr, index, bracketStartIndex - index );
					sb.Append( substitutedStr );
					index = 1 + bracketEndIndex;
				}
				else
					break;
			}

			if ( null != sb )
			{
				if ( index < captionStr.Length )
					sb.Append( captionStr, index, captionStr.Length - index );

				return sb.ToString( );
			}
			
			return captionStr;
		}

		#endregion // GetSummaryFooterCaption

		#region GetFormulaSummaryFromPrintFormulaSummary

		// SSP 1/18/05 BR01753
		// Only enable calculations in display layout. Print and export layouts will
		// copy over the calculated values from the display layout.
		//
		internal SummaryValue GetFormulaSummaryFromPrintFormulaSummary( SummaryValue printFormulaSummary )
		{
			for ( int i = 0, count = this.Count; i < count; i++ )
			{
				SummaryValue summary = this[i];
				string formula = summary.SummarySettings.Formula;
				if ( formula == printFormulaSummary.SummarySettings.Formula )
					return summary;
			}

			return null;
		}

		#endregion // GetFormulaSummaryFromPrintFormulaSummary

        // MRS 11/18/2008 - TFS10343
        #region GetSummaryValueAppearanceImageHeight

        internal int GetSummaryValueAppearanceImageHeight(SummaryDisplayAreaContext context)
        {
            AppearanceData appData = new AppearanceData();
            AppearancePropFlags flags = AppearancePropFlags.Image;

            this.ResolveSummaryValueAppearance(context, ref appData, ref flags);

            if (appData.Image != null &&
                this.Band != null &&
                this.Band.Layout != null &&
                this.Band.Layout.Grid != null)
            {
                Image image = appData.GetImage(this.Band.Layout.Grid.ImageList);
                return image.Height;
            }

            return 0;
        }

        #endregion // GetSummaryValueAppearanceImageHeight

		#endregion // Private/Internal Methods/Properties

		#region Protected Overridden Methods

		#region OnSubObjectPropChanged

		// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
		// Overrode OnSubObjectPropChanged.
		//
		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			if ( propChange.Source is SummaryValue )
			{
				SummaryValue summaryVal = (SummaryValue)propChange.Source;
                summaryVal.InvalidateItemAllRegions( );
			}
		}

		#endregion // OnSubObjectPropChanged

		#endregion // Protected Overridden Methods
	}
}
