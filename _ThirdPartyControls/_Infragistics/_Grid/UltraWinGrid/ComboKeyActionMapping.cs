#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Collections;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// Enumerates the possible actions that can be performed on the combo
    /// </summary>
    public enum UltraComboAction 
    {
		/// <summary>
		/// Toggle Drop Down 
		/// </summary>
        ToggleDropdown   = 0,
		
		/// <summary>
		/// Drop Down 
		/// </summary>
        Dropdown		 = 1,
		
		/// <summary>
		/// Close Drop Down 
		/// </summary>
        CloseDropdown    = 2,

		/// <summary>
		/// Undo Change 
		/// </summary>
        UndoChange       = 3,

		/// <summary>
		/// Next Row 
		/// </summary>
		NextRow          = 4,
		
		/// <summary>
		/// Previous Row 
		/// </summary>
        PrevRow          = 5,

		/// <summary>
		/// First Row 
		/// </summary>
        FirstRow		 = 6,
		
		/// <summary>
		/// Last Row 
		/// </summary>
        LastRow			 = 7,

		/// <summary>
		/// Scroll the list up one page
		/// </summary>
        PageUp			 = 8,
		
		/// <summary>
		/// Scroll the list down one page
		/// </summary>
        PageDown		 = 9,
    }

	

    /// <summary>
    /// Bit flags that describe the state of the control. For example, if
    /// the first cell in the second row is active but not in edit mode bits
    /// Row, Cell, and FirstCell will be set. 
    /// </summary>
    [Flags] public enum UltraComboState
    {
		/// <summary>
		/// The list is dropped down
		/// </summary>
        IsDroppedDown     = 0x00000001,
		
		/// <summary>
		/// A row is selected
		/// </summary>
        Row		          = 0x00000002,
		
		/// <summary>
		/// DropDownStyle is not a drop down list
		/// </summary>
        HasEdit           = 0x00000004,

        //  BF NA 9.1 - UltraCombo MultiSelect
        /// <summary>
		/// The <see cref="Infragistics.Win.UltraWinGrid.UltraCombo.Value">Value</see>
        /// property is determined by the <see cref="Infragistics.Win.ICheckedItemList">ICheckedItemList</see>
        /// implementation, which is in effect when the <see cref="Infragistics.Win.EditorCheckedListSettings.EditorValueSource">EditorValueSource</see>
        /// property is set to 'CheckedItems'.
		/// </summary>
        ValueProvidedByCheckedItemList = 0x00000008,
    }



    /// <summary>
    /// Key/Action mapping object for UltraCombo.
    /// </summary>
    public class ComboKeyActionMapping : KeyActionMappingBase
    {
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="keyCode">Indicates the key being mapped.</param>
		/// <param name="actionCode">The action to perform.</param>
        /// <param name="stateDisallowed">The disallowed states. These are bit flags that specify the state that the control MUST NOT be in for this mapping to be active. If the current state of the control has any of these bits turned on this mapping will be ignored. </param>
        /// <param name="stateRequired">The required states. These are bit flags that specify the state that the control MUST be in for this mapping to be active.</param>
        /// <param name="specialKeysDisallowed">The special keys that are NOT allowed. If shift, ctrl or alt are pressed and their corresponding bit is turned on in this property the mapping will be ignored.</param>
        /// <param name="specialKeysRequired">The special keys that are required. These keys (any combination of shift/control/alt) MUST be pressed. Otherwise, this mapping will be ignored.</param>
        public ComboKeyActionMapping( Keys keyCode,
                                          UltraComboAction actionCode,
                                          UltraComboState stateDisallowed,
                                          UltraComboState stateRequired,
                                          SpecialKeys specialKeysDisallowed,
                                          SpecialKeys specialKeysRequired )
            : base( keyCode,
                    actionCode,
                    (Int64)stateDisallowed,
                    (Int64)stateRequired,
                    specialKeysDisallowed,
                    specialKeysRequired )
        {
        }

        /// <summary>
        /// Gets/sets the action code. 
        /// </summary>
        public new UltraComboAction ActionCode
        {
            get 
            {
                return (UltraComboAction)base.ActionCode;
            }
            set
            {
				// test that the value is in range
				//
				if ( !Enum.IsDefined( typeof(UltraComboAction), value ) )
					throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_120") );

				base.ActionCode = value;
            }
        }

        /// <summary>
        /// Gets/sets the disallowed state. These are bit flags that specify
        /// the state that the control MUST NOT be in for this mapping to be
        /// active. If the current state of the control has any of these 
        /// bits turned on this mapping will be ignored.
        /// </summary>
        public new UltraComboState StateDisallowed
        {
            get 
            {
                return (UltraComboState)base.StateDisallowed;
            }
            set
            {
                base.StateDisallowed = (int)value;
            }
        }

        /// <summary>
        /// Gets/sets the required state. These are bit flags that specify
        /// the state that the control MUST be in for this mapping to be
        /// active.  
        /// </summary>
        public new UltraComboState StateRequired
        {
            get 
            {
                return (UltraComboState)base.StateRequired;
            }
            set
            {
                base.StateRequired = (int)value;
            }
        }
    }
}
