#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Drawing.Printing;
using Infragistics.Win.Printing;
using Infragistics.Win.Layout;
using System.Collections.Generic;

namespace Infragistics.Win.UltraWinGrid
{
	internal class PrintManager
	{

		private static int PAGE_WIDTHS_ARRAY_MAX = 10000;
		
		private System.Drawing.Size		logicalPageDimensions = System.Drawing.Size.Empty;
		private UltraGrid				grid = null;
		private long[]					pageHOrigin = new long[PAGE_WIDTHS_ARRAY_MAX];
		private LogicalPageInfo[]		logicalPageList = new LogicalPageInfo[PAGE_WIDTHS_ARRAY_MAX];
		private PrintDocument			printDocument = null;
		private Page					currentPrintingPage = null;
		//private PageSettings			pageSettings = null;
		private Size					physicalPageMarginBoundsSize = Size.Empty;
		private bool					hasCalculatedPageDimensions = false;
		private LogicalPageLayoutInfo	logicalPageLayoutInfoHolder = null;
		private bool					calledFromPrint = false;
		private LogicalPageLayoutInfo   defaultLayoutInfo = null;
		private double					scaleFactor = 0;
		private PrintDocument			cachedPrintDocument = null;
		private LogicalPageLayoutInfo	cachedLogicalPageLayoutInfo = null;
		private string[]				printerClipError =	{	"HP LaserJet 4Si",
    															"HP LaserJet 6L",
																"Hewlett Packard LaserJet 6L",
																"HP LaserJet 6L (PCL)",
																"HP DeskJet 870cxi"
															};
		private bool					textNeedsClipping = false;
		private PrintPreviewSettings	printPreviewSettings = null;
		//private int						copiesPrinted = 0;


		// SSP 4/11/02 UWG1049
		// Created a flag to cache the paper dimentions that we get from the printer manager.
		// Apparently, they are not cached by the printer manager, and if called repeatedly,
		// it will slow down considerably (instead of taking a a few seconds, it takes a
		// few minutes without this caching).
		//
		private bool physicalPaperSizeDirtyFlag = true;

		// MRS 5/21/04 - UWG2915
		private Graphics printerMetricsGraphics = null;

		internal PrintManager( UltraGrid grid )
		{
			this.grid = grid;					
		}
		internal PrintDocument PrintDocument
		{
			get
			{
				if ( this.printDocument == null )
				{
					this.printDocument = new PrintDocument();
					this.printDocument.PrinterSettings.FromPage = 1;
					this.printDocument.PrinterSettings.ToPage = 1;
					this.printDocument.PrinterSettings.MinimumPage = 1;							
				}

				return this.printDocument;
			}

			set
			{
				if ( this.printDocument != value )
					this.printDocument = value;

				if ( this.printDocument.PrinterSettings.FromPage == 0 )
				{
					this.printDocument.PrinterSettings.FromPage = 1;
					this.printDocument.PrinterSettings.MinimumPage = 1;
				}

				if ( this.printDocument.PrinterSettings.ToPage == 0)
					this.printDocument.PrinterSettings.ToPage = 1;

				// SSP 4/11/02 UWG1049
				// Implementation mechanism to cache the paper physical dimentsions.
				// So dirty the flag here if a new print document is set.
				//
				this.physicalPaperSizeDirtyFlag = true;
			}
		}

		internal bool CalledFromPrint
		{
			set
			{
				if ( this.calledFromPrint != value )
					this.calledFromPrint = value;
			}
		}


		internal Page CurrentPrintingPage
		{
			get
			{
				if ( this.currentPrintingPage == null )
					this.currentPrintingPage = new Page(0,0);
				return this.currentPrintingPage;
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid uncalled private code
			#region Not Used

			//set
			//{
			//    if ( this.currentPrintingPage != value )
			//        this.currentPrintingPage = value;		
			//}

			#endregion Not Used
		}

		internal LogicalPageInfo GetLogicalPageInfo( int logicalPage )
		{
			return this.logicalPageList[logicalPage];
		}
		private void SetPageHOrigin( int page, long origin )
		{
			this.pageHOrigin[page] = origin;
		}
		private long GetPageHOrigin( int page )
		{
			return this.pageHOrigin[page];
		}

		// SSP 6/8/06 BR13501
		// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
		// 
		private int AdjustScrollOffsetForWholeCells( VisibleRow vr, int scrollOffset )
		{
			UltraGridRow row = vr.Row;
			UltraGridLayout layout = this.grid.PrintLayout;
			ColScrollRegion csr = null != layout ? layout.ActiveColScrollRegion : null;
			GridBagLayoutManager lm = null != row ? row.LayoutManager as GridBagLayoutManager : null;

			if ( null == csr || null == row || null == layout || null == lm || ! row.UseRowLayoutResolved )
				return 0;

			Rectangle rect = vr.GetDimensions( csr, VisibleRowDimensions.OutsideRow, DimOriginBase.Normalized );

			LayoutContainerCalcSize lc = row.Band.GetCachedLayoutContainerCalcSize( );
			lc.Initialize( rect.Size );

			GridBagLayoutItemDimensionsCollection dims = lm.GetLayoutItemDimensions( lc, null );

			int rowCutOff = -scrollOffset;
			GridBagLayoutItemDimensions topMostCutOffRange = null;

			foreach ( ILayoutItem item in lm.LayoutItems )
			{
				GridBagLayoutItemDimensions dim = dims.Exists( item ) ? dims[ item ] : null;
				if ( null != dim )
				{
					Rectangle bounds = dim.Bounds;
					if ( bounds.Top < rowCutOff && bounds.Bottom > rowCutOff )
					{
						if ( null == topMostCutOffRange || bounds.Top < topMostCutOffRange.Bounds.Top )
							topMostCutOffRange = dim;
					}
				}
			}

			if ( null != topMostCutOffRange )
                 return Math.Max( 0, rowCutOff - topMostCutOffRange.Bounds.Top );

			return 0;
		}

		// SSP 6/8/06 BR13501
		// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
		// 
		internal ClipRowBehavior ClipRowBehaviorResolved
		{
			get
			{
				ClipRowBehavior clipRowBehavior = null != this.LogicalPageLayoutInfoHolder 
					? this.LogicalPageLayoutInfoHolder.ClipRowBehavior
					: ClipRowBehavior.Default;

				if ( ClipRowBehavior.Default == clipRowBehavior )
					clipRowBehavior = ClipRowBehavior.PixelBased;
				
				return clipRowBehavior;
			}
		}

		private void CalculateFirstRows()
		{
            // MRS NAS v8.2 - CardView Printing
            if (this.IsPrintingCards)
            {
                this.CalculateFirstCards();
                return;
            }

			// Make sure we have at least one row
			//
			// SSP 5/19/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
			// We need to also take into account the filter row, summary row etc...
			// Use VisibleRowCount instead of the Count.
			//
			//if ( this.grid.PrintLayout.Rows.Count <= 0 )
			if ( this.grid.PrintLayout.Rows.VisibleRowCount <= 0 )
			{
				// if there are no rows, add a single empty page
				//
				// RobA UWG743 11/19/01
				this.AddLogicalPrintPage( null );
				return;
			}

			// SSP 5/19/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
			// We need to also take into account the filter row, summary row etc...
			//
			//UltraGridRow firstRow = this.grid.PrintLayout.Rows[0];
			UltraGridRow firstRow = this.grid.PrintLayout.Rows.GetFirstVisibleRow( );

			RowScrollRegion rsr = this.grid.PrintLayout.RowScrollRegions[0];
			bool moreRows = true;
			
			//Add the first Prin t Page
			//
			moreRows = this.AddLogicalPrintPage( firstRow );
			if ( !moreRows )
				return;

			// SSP 3/17/05 BR02832
			// If a row being printed is taller than the page height (not the same
			// thing as if it cross the bottom of the page) then start the row on
			// the next page at the point where we left off.
			//
			int scrollOffset = 0;

			// SSP 6/8/06 BR13501
			// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
			// 
			ClipRowBehavior clipRowBehavior = this.ClipRowBehaviorResolved;

			//Loop over rows and determine number of 
			//logicalPageDimensions.Height
			//
			do
			{
				//Calculate the size of the rowscrollregion
				//
				double footerHeight;
				double headerHeight;

				if ( this.LogicalPageLayoutInfoHolder.PageFooterHeight == -1 )
                    footerHeight = 0;
				else
					footerHeight = this.LogicalPageLayoutInfoHolder.PageFooterHeight;

				if ( this.LogicalPageLayoutInfoHolder.PageHeaderHeight == -1 )
					headerHeight = 0;
				else
                    headerHeight = this.LogicalPageLayoutInfoHolder.PageHeaderHeight;

				
				headerHeight += ( 2 * this.grid.PrintLayout.GetBorderThickness( this.LogicalPageLayoutInfoHolder.PageHeaderBorderStyle ) );
				footerHeight += ( 2 * this.grid.PrintLayout.GetBorderThickness( this.LogicalPageLayoutInfoHolder.PageFooterBorderStyle ) );

				// SSP 7/6/04 UWG3489
				// Don't scale the header and footer heights becase the user specifies them already 
				// scaled. For example, if the scale factor is 0.5 and if the user specifies 100
				// as the header height, on the paper it will be 50 because the graphics will scale
				// the whole page by 0.5.
				// 
				//headerHeight /= this.scaleFactor;
				//footerHeight /= this.scaleFactor;

				double height = this.PhysicalPageMarginBoundsSize.Height;
				height /= this.scaleFactor;
				height -= ( footerHeight + headerHeight );

				
				//Setup RowScrollRegion to be in correct position
				//
				rsr.Origin = (int)headerHeight;
				rsr.Height = (int)height;
				rsr.SetFirstRow( firstRow );

				// SSP 3/30/05 BR02832 BR03457
				// Also reset the scrollOffset on the rsr.
				//
				rsr.scrollOffset = scrollOffset;

				rsr.SetDestroyVisibleRows();
				rsr.RegenerateVisibleRows();
				VisibleRowsCollection visibleRows = rsr.VisibleRows;

                if ( visibleRows.Count == 0 )
				{
					// if there are no rows, add a single empty page
					//
					Debug.Assert(false,"VisibleRows count = 0 in CalculateFirstRows");
					this.AddLogicalPrintPage( null );
					break;
				}
      
				//Reset number of rows to 0
				//
				int numRowsOnPage = 0;
			
				//loop over visiblerows until the a row doesn't fit on the page
				//or there are no more rows
				//
				for ( int i=0; i < visibleRows.Count; ++i )
				{
					
                    numRowsOnPage++;

					VisibleRow vr = visibleRows[i];
					firstRow = vr.Row;

					Debug.Assert( firstRow != null, "No firstRow!" );
            
					//if the bottom of the visible row is off the page
					//start a new page witht that visible row as first row
					//
					// SSP 7/23/02 UWG1269
					// Use the extent because the Height intersects the rect with the
					// data area element rect and returns. However since we are printing
					// the data area rect will not be the right one. The way printing is
					// currently being done is that position ui elements of various
					// printing elements like page header and page footer elements assume 
					// that all the necessary calcualtions regarding printing have already 
					// been done. Since we are in the midst of calculating that those 
					// methods shouldn't get called or else NullReference exception may 
					// take place as some of the variables haven't been initialized yet. 
					// Accessing Height may try to verify the rects and call position child
					// elements of the elements and we don't want that. So just use Extent
					// here.
					//
					//if ( rsr.VisibleRows[i].Bottom > rsr.Height )
					//MRS 6/18/04 - UWG3252 The last row has to account for Summaries
					//if ( rsr.VisibleRows[i].Bottom > rsr.Extent )
					// SSP 3/17/05 BR02832 BR03457
					// If a row being printed is taller than the page height (not the same
					// thing as if it cross the bottom of the page) then start the row on
					// the next page at the point where we left off.
					//
					// --------------------------------------------------------------------------
					
					int rowTop = vr.Top;
					int rowHeight = vr.GetTotalHeight( );
					int rsrBottom = rsr.Extent;

                    // MRS 4/27/2009 - TFS16619
                    if (rowTop > rsrBottom)
                    {
                        // If the top of the row is off the bottom of the page, it means that the 
                        // headers are taller than the page, and so no rows will print. This means we 
                        // will get stuck in a loop where we just print the headers over and over
                        // at the top of every page. So fire the Error event and then cancel the print. 
                        //
                        ErrorEventArgs ee = new ErrorEventArgs(ErrorType.Printing, SR.GetString("HeadersAreTooTallToPrint_Text"));
                        this.grid.FireEvent(GridEventIds.Error, ee);

                        if (ee.Cancel == false)
                        {
                            Form form = this.grid.PrintLayout.Form;

                            MessageBoxButtons buttons = MessageBoxButtons.OK;

                            DialogResult result = MessageBox.Show(
                                form,
                                ee.ErrorText,
                                SR.GetString("PrintingError_Dialog_Title"),
                                buttons,
                                MessageBoxIcon.Error);
                        }

                        return;
                    }

					if ( rowTop + rowHeight > rsrBottom )
					{
						if ( 1 == numRowsOnPage )
						{
							// If a single row is visible and it spans beyond the bottom of the page
							// then that row is taller than the page height. In that case start the 
							// row on the next page where it's left off on this page. This code keeps
							// doing that until the whole of the row is printed.
							//
							scrollOffset = rowTop - rsrBottom;

							// SSP 6/8/06 BR13501
							// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
							// 
							// ----------------------------------------------------------------------
							if ( ClipRowBehavior.CellBased == clipRowBehavior )
							{
								int delta = this.AdjustScrollOffsetForWholeCells( vr, scrollOffset );

								// Only adjust the scrollOffset if delta is less than the page 
								// height. This should always be the case.
								// 
								if ( delta < rsrBottom )
									scrollOffset += delta;
							}
							// ----------------------------------------------------------------------
						}
						else
						{
							// Reset the scroll offset to 0 so the current row starts out on the next 
							// page.
							//
							scrollOffset = 0;
						}

						// SSP 7/13/05 BR04400
						// When firing InitializeLogicalPrintPage which is what the AddLogicalPrintPage
						// call below does, make sure that the RowScrollRegion points to the correct 
						// first row. Although not necessary for the BR04400, we should also make sure
						// that the visible rows are regenerated since one might want to make use of
						// those as well in the InitializeLogicalPrintPage event.
						// 
						// ----------------------------------------------------------------------------
						rsr.SetFirstRow( firstRow );
						rsr.scrollOffset = scrollOffset;
						rsr.RegenerateVisibleRows( );
						// ----------------------------------------------------------------------------

						//Add a new logical print page
						//
						moreRows = this.AddLogicalPrintPage( firstRow, scrollOffset );

						break;
					}
					// --------------------------------------------------------------------------

					moreRows = false;
				}            
			} while ( moreRows );	
		}
		
		private void CalculateHorizontalOrigins()
		{
            // MRS NAS v8.2 - CardView Printing
            // Added 'if' block to handle CardView Printing
            if (this.IsPrintingCards == false)
            {
                // MD 7/26/07 - 7.3 Performance
                // FxCop - Remove unused locals
                //double longestExtent = this.GetBandsOverallWidth( true );
                this.GetBandsOverallWidth(true);
            }
            else
            {
                this.CalculateCardViewHorizontalOrigins();
            }
        }

		private void SetScaleFactor()
		{
			// SSP 4/11/02 UWG1049
			// Implemented mechanism to cache the paper physical dimensions.
			// So dirty the flag here if a new print document is set.
			//
			this.physicalPaperSizeDirtyFlag = true;

			int pages = this.logicalPageLayoutInfoHolder.FitWidthToPages;

			this.scaleFactor = 1;

			// If FitWidthToPages is set to anything > 0, we must scale.
			if ( pages > 0 )
			{
				//We want the bands actual overall width, not the width including
				//clipped columns that are started again on the next page.
				double bandsOverallWidth = this.grid.PrintLayout.BandsOverallExtent;

                // size of the physical page.
				//
				Size physicalPage = this.PhysicalPageMarginBoundsSize;

                // get the total width of all the pages (physical size of page * num pages)
                //
				double totalWidthOfPages;
				// SSP 4/11/02 UWG1049
				// This is done in PhysicalPageMarginBoundsSize preoprty now.
				//				
				
				totalWidthOfPages =  physicalPage.Width * pages;

                // if the bands overall width is greater than the width for the number of
                // pages specified in FitWidthToPages, we must scale the band smaller.                
				// SSP 11/26/02 UWG1824
				// Use the GetBandsOverallWidth method which takes into account the columns that get
				// carried over to the next page if they don't fully fit the current page rather than
				// the BandsOverAllWidth which doesn't take into account that.
				//
				//if ( bandsOverallWidth > totalWidthOfPages )
				if ( this.GetBandsOverallWidth( false ) > totalWidthOfPages )
				{
					// determine the scale factor
					this.scaleFactor = totalWidthOfPages / bandsOverallWidth;

                    // now see what the bands width is with the scale factor
					bandsOverallWidth = this.GetBandsOverallWidth( false );

                    // if it still doesn't, slowly decrement scale factor
					while ( bandsOverallWidth > totalWidthOfPages )
                    {
						this.scaleFactor -= 0.001;

						bandsOverallWidth = this.GetBandsOverallWidth( false );
					}
				}
			}
		}

		internal LogicalPageLayoutInfo DefaultLayoutInfo
		{
			get
			{
				if ( this.defaultLayoutInfo == null )
					this.defaultLayoutInfo = new LogicalPageLayoutInfo( this.grid );
				return this.defaultLayoutInfo;
			}
		}

		internal PrintPreviewSettings PrintPreviewSettings
		{
			get
			{
				if ( this.printPreviewSettings == null )
					this.printPreviewSettings = new PrintPreviewSettings();
				return this.printPreviewSettings;
			}
		}

		private void CalculatePageDimensions()
		{

			//RobA UWG559 10/17/01
			System.Windows.Forms.Cursor oldCursor = Cursor.Current;

			Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

			//Set the layoutinfoholder
			//
			this.ResetLayoutInfoHolder();

			//calculate scalefactor 
			//
			this.SetScaleFactor();

            this.CalculateHorizontalOrigins();
			
			//Determine firstRow for each logical page
			//for efficiency only calculate the first rows up
			//to the page we are asked to print up to
			//
			this.CalculateFirstRows();				

			//Set flag to indicate that we have calculated pages
			//
			this.hasCalculatedPageDimensions = true;

			//RobA UWG559 10/17/01
			Cursor.Current = oldCursor;
			
		}

		private bool HasCalculatedPageDimensions
		{
			get
			{
				return this.hasCalculatedPageDimensions;
			}
		}

		// MRS 6/10/04 - UWG2968
		//private LogicalPageLayoutInfo LogicalPageLayoutInfoHolder
		internal LogicalPageLayoutInfo LogicalPageLayoutInfoHolder
		{
			get
			{
				if ( this.logicalPageLayoutInfoHolder == null )
					this.logicalPageLayoutInfoHolder = new LogicalPageLayoutInfo( this.grid );

				
				return this.logicalPageLayoutInfoHolder;
			}
		}


		internal LogicalPageInfo CurrentPrintingLogicalPageInfo
		{
			get
			{
				return this.GetLogicalPageInfo( this.currentPrintingPage.y );				
			}
		}
		private void ResetLayoutInfoHolder()
		{
			//if a default value was not set
			//use logicalPageLayoutInfoHolder's reset 
			//
			if ( this.defaultLayoutInfo == null )
				this.LogicalPageLayoutInfoHolder.Reset();


			//default value was set, We need to default
			//every page to their modification
			//
			else if ( this.LogicalPageLayoutInfoHolder != null )
				this.logicalPageLayoutInfoHolder = this.defaultLayoutInfo.Copy();
		}
		
		private bool AddLogicalPrintPage( UltraGridRow row )
		{
			return this.AddLogicalPrintPage( row, 0 );
		}
		
		// SSP 3/17/05 BR02832
		// If a row being printed is taller than the page height (not the same
		// thing as if it cross the bottom of the page) then start the row on
		// the next page at the point where we left off. Added an overload of 
		// AddLogicalPrintPage that takes in scrollOffset parameter.
		//
		private bool AddLogicalPrintPage( UltraGridRow row, int scrollOffset )
		{

			//Reset back to default values
			//
			this.ResetLayoutInfoHolder();

			//Fire InitializeLogicalPrintPage event
			//
			CancelableLogicalPrintPageEventArgs e = new CancelableLogicalPrintPageEventArgs( this.printDocument, this.logicalPageLayoutInfoHolder, 
																							this.logicalPageDimensions.Height + 1 );
			this.grid.FireEvent( GridEventIds.InitializeLogicalPrintPage, e );

			if ( e.Cancel )
				return false;

			//Calculate header and footer heights
			//
			this.CalculateHeaderFooterHeights();

			//Create a new LogicalPageInfo and add it to the 
			//logicalPageList.  currentLogicalPageLayoutInfo is already initialized
			//from either InitializePrint or InitializePrintPreview
			//				
			this.logicalPageList[this.logicalPageDimensions.Height] = 
				// SSP 3/17/05 BR02832
				// If a row being printed is taller than the page height (not the same
				// thing as if it crosses the bottom of the page) then start the row on
				// the next page at the point where we left off. Added an overload of 
				// AddLogicalPrintPage that takes in scrollOffset parameter.
				//
				//new LogicalPageInfo( row, this.logicalPageLayoutInfoHolder );
				new LogicalPageInfo( row, this.logicalPageLayoutInfoHolder, scrollOffset );

			++this.logicalPageDimensions.Height;

			//this is used to make the code more efficient 
			//if we were only asked to print up to a certain
			//page there is no need to determine the first rows of 
			//the pages that follow
			//
			if ( this.PrintDocument.PrinterSettings.PrintRange == PrintRange.AllPages )
				return true;
		
			return ( (this.logicalPageDimensions.Height * this.logicalPageDimensions.Width) 
						<= this.printDocument.PrinterSettings.ToPage );				
			
		}
		private void CalculateHeaderFooterHeights()
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//Size size = Size.Empty;

			if ( this.logicalPageLayoutInfoHolder.PageFooterHeight == -1 )
			{
                if (!( this.logicalPageLayoutInfoHolder.PageFooter == null ||
					// AS 1/8/03 - fxcop
					// Do not compare against string.empty - test the length instead
					//this.logicalPageLayoutInfoHolder.PageFooter == string.Empty) )					
					this.logicalPageLayoutInfoHolder.PageFooter.Length == 0) )					
				{
                    AppearanceData appData = new AppearanceData();
					AppearancePropFlags requestedProps = AppearancePropFlags.AllRender;

					this.ResolveSpecifiedAppearance( ref appData , ref requestedProps,
						// AS 1/28/05 Optimization - Don't force the appearance to be created
						//this.logicalPageLayoutInfoHolder.PageFooterAppearance );
						this.logicalPageLayoutInfoHolder.HasPageFooterAppearanceBeenModified ? 
							this.logicalPageLayoutInfoHolder.PageFooterAppearance : null
						// SSP 3/16/06 - App Styling
						// Pass in the role param so the ResolveSpecifiedAppearance knows which role 
						// to resolve style appearance settings for.
						// 
						, StyleUtils.Role.PageFooter );

					// JJD 12/14/01
					// Moved font height logic into CalculateFontHeight
					//
					// get the extext of the caption
					//
					// SSP 8/12/03 UWG2196
					// Take into account the fact that we assign special meaning to tab in the text.
					// First tab means center the following text and the second tab means right-align
					// the following text.
					//
					// ----------------------------------------------------------------------------------
					//size = this.grid.PrintLayout.CalculateFontSize( ref appData, this.logicalPageLayoutInfoHolder.PageFooter );
					//this.logicalPageLayoutInfoHolder.PageFooterHeight = size.Height;
					string[] arr = ( null != this.logicalPageLayoutInfoHolder.PageFooter ? this.logicalPageLayoutInfoHolder.PageFooter : "" ).Split( '\t' );
					Size size1 = this.grid.PrintLayout.CalculateFontSize( ref appData, arr.Length >= 1 && arr[0].Length > 0 ? arr[0] : "lWg" );
					Size size2 = this.grid.PrintLayout.CalculateFontSize( ref appData, arr.Length >= 2 && arr[1].Length > 0 ? arr[1] : "lWg" );
					Size size3 = this.grid.PrintLayout.CalculateFontSize( ref appData, arr.Length >= 3 && arr[2].Length > 0 ? arr[2] : "lWg" );
					this.logicalPageLayoutInfoHolder.PageFooterHeight = Math.Max( size1.Height, Math.Max( size2.Height, size3.Height ) );
					// ----------------------------------------------------------------------------------

					//*vl 060701 - add 4 pixels to height because bottom edge of some chars
					//             was being clipped 
					this.logicalPageLayoutInfoHolder.PageFooterHeight += 4;				
				}
			}

			if ( this.logicalPageLayoutInfoHolder.PageHeaderHeight == -1 )
			{
				if ( !(this.logicalPageLayoutInfoHolder.PageHeader == null ||
					// AS 1/8/03 - fxcop
					// Do not compare against string.empty - test the length instead
					//this.logicalPageLayoutInfoHolder.PageHeader == string.Empty ))					
					this.logicalPageLayoutInfoHolder.PageHeader.Length == 0 ))					
				{
					AppearanceData appData = new AppearanceData();
					AppearancePropFlags requestedProps = AppearancePropFlags.AllRender;

					this.ResolveSpecifiedAppearance( ref appData , ref requestedProps,
						// AS 1/28/05 Optimization - Don't force the appearance to be created
						//this.logicalPageLayoutInfoHolder.PageHeaderAppearance );
						this.logicalPageLayoutInfoHolder.HasPageHeaderAppearanceBeenModified ? 
						this.logicalPageLayoutInfoHolder.PageHeaderAppearance : null
						// SSP 3/16/06 - App Styling
						// Pass in the role param so the ResolveSpecifiedAppearance knows which role 
						// to resolve style appearance settings for.
						// 
						, StyleUtils.Role.PageHeader );

					// JJD 12/14/01
					// Moved font height logic into CalculateFontHeight
					//
					// get the extext of the caption
					//
					// SSP 8/12/03 UWG2196
					// Take into account the fact that we assign special meaning to tab in the text.
					// First tab means center the following text and the second tab means right-align
					// the following text.
					//
					// ----------------------------------------------------------------------------------
					//size = this.grid.PrintLayout.CalculateFontSize( ref appData, this.logicalPageLayoutInfoHolder.PageHeader );
					//this.logicalPageLayoutInfoHolder.PageHeaderHeight = size.Height;
					string[] arr = ( null != this.logicalPageLayoutInfoHolder.PageHeader ? this.logicalPageLayoutInfoHolder.PageHeader : "" ).Split( '\t' );
					Size size1 = this.grid.PrintLayout.CalculateFontSize( ref appData, arr.Length >= 1 && arr[0].Length > 0 ? arr[0] : "lWg" );
					Size size2 = this.grid.PrintLayout.CalculateFontSize( ref appData, arr.Length >= 2 && arr[1].Length > 0 ? arr[1] : "lWg" );
					Size size3 = this.grid.PrintLayout.CalculateFontSize( ref appData, arr.Length >= 3 && arr[2].Length > 0 ? arr[2] : "lWg" );
					this.logicalPageLayoutInfoHolder.PageHeaderHeight = Math.Max( size1.Height, Math.Max( size2.Height, size3.Height ) );
					// ----------------------------------------------------------------------------------

					//*vl 060701 - add 4 pixels to height because bottom edge of some chars
					//             was being clipped 
					this.logicalPageLayoutInfoHolder.PageHeaderHeight += 4;	
				}
			}

        }
		internal void  InternalResolveHeaderAppearance( ref AppearanceData appData,
			ref AppearancePropFlags requestedProps )
		{
			this.ResolveSpecifiedAppearance( ref appData, ref requestedProps,
				// AS 1/28/05 Optimization - Don't force the appearance to be created
				//this.CurrentPrintingLogicalPageInfo.LogicalPageLayoutInfo.PageHeaderAppearance );
				this.CurrentPrintingLogicalPageInfo.LogicalPageLayoutInfo.HasPageHeaderAppearanceBeenModified ? 
				this.CurrentPrintingLogicalPageInfo.LogicalPageLayoutInfo.PageHeaderAppearance : null
				// SSP 3/16/06 - App Styling
				// Pass in the role param so the ResolveSpecifiedAppearance knows which role 
				// to resolve style appearance settings for.
				// 
				, StyleUtils.Role.PageHeader );
		}
		internal void  InternalResolveFooterAppearance( ref AppearanceData appData,
			ref AppearancePropFlags requestedProps )
		{
			this.ResolveSpecifiedAppearance( ref appData, ref requestedProps, 
				// AS 1/28/05 Optimization - Don't force the appearance to be created
				//this.CurrentPrintingLogicalPageInfo.LogicalPageLayoutInfo.PageFooterAppearance );
				this.CurrentPrintingLogicalPageInfo.LogicalPageLayoutInfo.HasPageFooterAppearanceBeenModified ? 
				this.CurrentPrintingLogicalPageInfo.LogicalPageLayoutInfo.PageFooterAppearance : null
				// SSP 3/16/06 - App Styling
				// Pass in the role param so the ResolveSpecifiedAppearance knows which role 
				// to resolve style appearance settings for.
				// 
				, StyleUtils.Role.PageFooter );
		}

		internal void  ResolveSpecifiedAppearance( ref AppearanceData appData,
			ref AppearancePropFlags requestedProps, Appearance app
			// SSP 3/14/06 - App Styling
			// Added role parameter.
			// 
			, StyleUtils.Role role )
		{

			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.UltraGridLayout), requestedProps );

			// SSP 3/14/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( this.grid.PrintLayout, role, out context.ResolutionOrder );
			if ( context.ResolutionOrder.UseStyleBefore )
				StyleUtils.ResolveAppearance( AppStyling.RoleState.Normal, ref appData, ref context );
			
			// SSP 3/14/06 - App Styling
			// 
			//if ( app != null )
			if ( app != null && context.ResolutionOrder.UseControlInfo )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, app.Data, ref context.UnresolvedProps );
				app.MergeData( ref appData, ref context.UnresolvedProps );
			}

			// SSP 3/14/06 - App Styling
			// 
			if ( context.ResolutionOrder.UseStyleAfter )
				StyleUtils.ResolveAppearance( AppStyling.RoleState.Normal, ref appData, ref context );

			if ( context.UnresolvedProps == 0 )
				return;

				// call the layout's resolve appearance to fill in any properties that
				// weren't set already (except for font)
				//
			else
			{
				// SSP 10/7/03 UWG2312
				// Clear the ImageBackground because we don't want to use the same image background
				// in the header/footer elements as used in the grid ui element. Since grid elem
				// encompasses header/footer elems, image background specified on the layout 
				// appearance should only be drawn in the grid element.
				//
				// --------------------------------------------------------------------------------
				//this.grid.PrintLayout.ResolveAppearance( ref appData, ref context );
				// 4/7/06 ImageBackgroundDisabled
				//AppearancePropFlags clearFlags = context.UnresolvedProps & AppearancePropFlags.ImageBackground;
				AppearancePropFlags clearFlags = context.UnresolvedProps & GridUtils.ImageBackgroundProps;
				context.UnresolvedProps &= ~clearFlags;
				this.grid.PrintLayout.ResolveAppearance( ref appData, ref context );
				context.UnresolvedProps |= clearFlags;
				// --------------------------------------------------------------------------------
			}			
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Shows the page dialog
		//        /// </summary>
		//        /// <returns></returns>
		//#endif
		//        internal DialogResult ShowPageDialog()
		//        {
		//            //create a Page Setup Dialog
		//            //
		//            PageSetupDialog psDlg = new PageSetupDialog() ;

		//            //not saving pagesettings, should we in this version 
		//            //should this get passed into the print
		//            /*
		//            if (this.pageSettings == null) 
		//            {
		//                this.pageSettings =  new PageSettings();
		//            }

		//            //Set the dialogs settings to our stored settings
		//            //
		//            psDlg.PageSettings = this.pageSettings ;
		//            */

		//            psDlg.Document = this.printDocument;

		//            return psDlg.ShowDialog();
		//        }
		//#if DEBUG
		//        /// <summary>
		//        /// Shows print Dialog
		//        /// </summary>
		//        /// <returns></returns>
		//#endif
		//        internal DialogResult ShowPrintDialog()
		//        {

		//            //create an new print dialog
		//            //
		//            PrintDialog printdlg = new PrintDialog();

		//            //set the print dialog's document to the new PrintDocument
		//            //
		//            printdlg.Document = this.PrintDocument;

		//            //Allow a range of pages to be printed
		//            //
		//            printdlg.AllowSomePages = true;	
		//            printdlg.AllowPrintToFile = false;

		//            return printdlg.ShowDialog();			
		//        }

		#endregion Not Used

		internal void ShowPrintPreviewDialog()
		{
			// SSP 4/11/02 UWG1049
			// Implementation mechanism to cache the paper physical dimentsions.
			// So dirty the flag here if a new print document is set.
			//
			this.physicalPaperSizeDirtyFlag = true;


			//create a new Print preview dialog 
			//
			// AS 4/25/05 BR03549
			//PrintPreviewDialog dlg = new PrintPreviewDialog();
			using (PrintPreviewDialog dlg = new PrintPreviewDialog())
			{
				//set dialog's document to the new PrintDocument
				//
				dlg.Document = this.PrintDocument;

				//RobA 10/31/01 UWG
				//if the zoom property was set, set the PrintPreviewControl's
				//zoom property
				//
				if ( this.printPreviewSettings != null && 
					this.printPreviewSettings.Zoom != -1 )
				{
					// SSP 9/26/02 UWG1199
					// A better way to do this is to just access the PrintPreviewControl
					// property off the dlg rather than traverse through its children.
					//
					// --------------------------------------------------------------------------
					

					if ( null != dlg.PrintPreviewControl )
						dlg.PrintPreviewControl.Zoom = this.printPreviewSettings.Zoom;
					// --------------------------------------------------------------------------
				}
		
				//RobA UWG109 9/21/01 set the default size bigger		
				dlg.Height = 500;
				dlg.Width  = 400;
			
			
				if ( this.PrintPreviewSettings.DialogLeft != -1 ||
					this.PrintPreviewSettings.DialogTop != -1  )
					dlg.StartPosition = FormStartPosition.Manual;

				//RobA 10/31/01 exposed a Size property 
				//and location property
				//
				if ( this.PrintPreviewSettings.DialogLeft != -1 )
					dlg.Left = this.PrintPreviewSettings.DialogLeft;				

				if ( this.PrintPreviewSettings.DialogTop != -1 )
					dlg.Top = this.PrintPreviewSettings.DialogTop;						
			
				if ( this.PrintPreviewSettings.DialogHeight != -1 )
					dlg.Height = this.PrintPreviewSettings.DialogHeight;

				if ( this.PrintPreviewSettings.DialogWidth != -1 )
					dlg.Width = this.PrintPreviewSettings.DialogWidth;

				// JDN 1/6/05 BR00581 - Added DialogCaption and DialogIcon properties
				if ( this.PrintPreviewSettings.DialogCaption != null )
					dlg.Text = this.PrintPreviewSettings.DialogCaption;

				// JDN 1/6/05 BR00581 - Added DialogCaption and DialogIcon properties
				if ( this.PrintPreviewSettings.DialogIcon != null )
					dlg.Icon = this.PrintPreviewSettings.DialogIcon;
			
				//fires begin print page event
				//
				dlg.ShowDialog();

				// AS 4/25/05 BR03549
				// To make sure the fact that the printpreviewdialog is not
				// properly disposed by the framework does not prevent the
				// printdocument from being disposed, we'll clear its reference
				// to that component.
				//
				dlg.Document = null;
			}
		}
		private void SetStartPage()
		{
			//RobA 10/25/01
			//we should not need to worry about copies
			//set number of copies that have been printed to 0
			//
			//this.copiesPrinted = 0;


			//set currentPage if user wants to start from a page other than 1
			//
			if ( this.PrintDocument.PrinterSettings.PrintRange == PrintRange.AllPages )
				this.CurrentPrintingPage.Reset();
			else
				this.ConvertPhysicalToLogicalPage(this.CurrentPrintingPage, this.PrintDocument.PrinterSettings.FromPage );			
		}

		private void ConvertPhysicalToLogicalPage( Page LogicalPage, int PhysicalPage )
		{		
			//logicalPageDimensions must be set before this method is called
			//


			if ( PhysicalPage == 0 )
				Debug.Assert(false, "Trying to set the logicalPage to PhysicalPage 0" );
			
			if ( PhysicalPage == 1 )
			{
				LogicalPage.x = 0;
				LogicalPage.y = 0;
				return;
			}

			//need to work in n = 0 terms
			// 
			--PhysicalPage;

			LogicalPage.x = (PhysicalPage % ( this.logicalPageDimensions.Width ) );
			LogicalPage.y = (PhysicalPage / ( this.logicalPageDimensions.Width ) );
		}

	

		private double GetBandsOverallWidth( bool addPageHOrgin)
		{
			if ( this.grid.PrintLayout != null )
			{
                // MRS 3/12/2009 - TFS15083
                this.grid.PrintLayout.ColScrollRegions.DirtyMetrics();

				UltraGridBand band = null;
				UltraGridBand bandWidest = null;

				double longestExtent = 0;

                // x-position on the current page.
				//
                double pageExtent = 0;

                // Keeps a running total of our x-position.
				//
				double totalExtent = 0;

                int count = this.grid.PrintLayout.SortedBands.Count;

				Debug.Assert( count > 0, "No bands in PrintLayout!" );

                for ( int i = 0; i < count; i++ )
                {
                    band = (UltraGridBand)this.grid.PrintLayout.SortedBands.GetItem(i);

				    pageExtent = 0;
                    totalExtent = 0;

                    
					// GetBandOverallWidth gets the width of the band and all
					// of its children bands.  No need to add the horizontal position
					// on this pass
					//
					this.GetBandOverallWidth( ref pageExtent, ref totalExtent, band,
						 false );
	

                    if ( totalExtent > longestExtent )
					{
						longestExtent = totalExtent;
						bandWidest = band;
					}
				}
        
                // now use the widest band to determine dimensions
				//
				this.logicalPageDimensions.Width = 1;
				this.logicalPageDimensions.Height = 0;

				pageExtent = 0;
				totalExtent = 0;

				
				// GetBandOverallWidth gets the width of the band and all
				// of its children bands.  Add the horizontal position
				// on this pass
				//
				this.GetBandOverallWidth( ref pageExtent, ref totalExtent, bandWidest, 
					addPageHOrgin );
			
				return longestExtent;
			}

			return 0;
		}


		// SSP 10/20/03 UWG2707 UWG2617
		// Added Range helper class.
		//
		private class Range : IComparable
		{
			internal int start;
			internal int end;

			internal Range( int start, int end )
			{
				this.start = start;
				this.end = end;
			}

			public int CompareTo( object x )
			{
				if ( null == x )
					return 1;

				// Compare by start
				//
				int r = this.start.CompareTo( ((Range)x).start );
				if ( 0 == r )
					r = this.end.CompareTo( ((Range)x).end );

				return r;
			}
		}

		private void GetBandOverallWidth( ref double pageExtent , 
			ref double totalExtent,
			UltraGridBand band, bool addPageHOrigin )
		{

			//
			// Make sure we have everything we need.
			//
			if ( band == null || band.Columns == null || this.grid.PrintLayout == null ||
				this.grid.PrintLayout.SortedBands == null ) 
				return;

			// Size of the physical page's margins.
			//
			Size physicalPage = this.PhysicalPageMarginBoundsSize;

			if ( band.HiddenResolved ) 
				return;
			
			// Step 1:
			// Get band's extent.
			//
			HeadersCollection headers = band.OrderedHeaders;

			ColScrollRegionsCollection csrs = this.grid.PrintLayout.GetColScrollRegions( false );
		    
			csrs.InitializeMetrics();

			if ( headers != null )
			{
				// SSP 10/20/03 UWG2707 UWG2617
				// Added proper support for groups and row layouts. Below algorithm should work for
				// the non-group and non-row-layout mode as well so commented out the original code
				// and added the following code.
				//
				// ----------------------------------------------------------------------------------
				int i;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = new ArrayList( );
				List<Range> list = new List<Range>();

				// Construct an list of Range instances each with information on a column's
				// left and right coordinate.
				//
				int farthestRight = 0;
				for ( i = 0; i < band.Columns.Count; i++ )
				{
					UltraGridColumn column = band.Columns[i];
                    // MRS 2/23/2009 - Found while fixing TFS14354
					//if ( null == column || column.Hidden )
                    if (null == column || column.HiddenResolved)
						continue;

					int origin = column.Header.OverallOrigin;
					int extent = column.Header.Extent;

					// Take into account the fact that we extend the last column in a group 
					// to the right of the group so that it occupies the rest of the group.
					//
					// MD 1/21/09 - Groups in RowLayouts
					// In group layout style, we don't have to worry about extending the last column in the group.
					//if ( band.GroupsDisplayed && null != column.Group && column.LastItemInLevel )
					if ( band.GroupsDisplayed && 
						band.RowLayoutStyle != RowLayoutStyle.GroupLayout &&
						null != column.Group && 
						column.LastItemInLevel )
					{
						Rectangle groupHeaderRect = Rectangle.Empty, discard = Rectangle.Empty;
						column.Group.CalculateHeaderRect( ref groupHeaderRect, ref discard );
						extent = groupHeaderRect.Width - column.RelativeOrigin;
					}

					farthestRight = Math.Max( farthestRight, origin + extent );
					list.Add( new Range( origin, origin + extent ) );
				}

				// Sort the list of Ranges by their left coordinates. Range implements IComparable which
				// does the comparing based on the start member variable.
				//
				list.Sort( );

				// Add a dummy range with the start and the end as the farthest right. This is so that
				// the below logic works out nicely.
				// 
				list.Add( new Range( farthestRight, farthestRight ) );

				// Initialize currentPageOrigin to 0. The reason for initializing to pageExtent is that
				// although this method should always get called with pageExtent of 0 in case it doesn't
				// (due to some future change some other place) then we still want this method to work 
				// properly. That's why we are using pageExtent.
				//
				double currentPageOrigin = pageExtent;

				Debug.Assert( null != this.LogicalPageLayoutInfoHolder, "No logical page layout info !" );
				ColumnClipMode columnClipMode = null != this.LogicalPageLayoutInfoHolder 
					? this.LogicalPageLayoutInfoHolder.ColumnClipMode 
					: ColumnClipMode.Default;

				if ( ColumnClipMode.Default == columnClipMode )
					columnClipMode = band.GroupsDisplayed || band.UseRowLayoutResolved 
						? ColumnClipMode.SplitClippedColumns 
						: ColumnClipMode.RepeatClippedColumns;
				
				for ( i = 0; i < list.Count; i++ )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//Range range = (Range)list[i];
					Range range = list[ i ];

					// If the current column's left is beyond the current page, then add pages until
					// the column's left is visible on the current page.
					//
					while ( scaleFactor * ( range.start - currentPageOrigin ) > physicalPage.Width )
					{
						// Take into account the 1 pixel wide left and the right borders that the page
						// will have.
						//
						const int PAGE_BORDER_WIDTH_DELTA = 2;
						currentPageOrigin += ( physicalPage.Width - PAGE_BORDER_WIDTH_DELTA ) / scaleFactor;

						this.StartNewPage( ref pageExtent, ref totalExtent, 
							(long)currentPageOrigin, 0, addPageHOrigin );
					}
				
					// If the ColumnClipMode is RepeatClippedColumns, then we need to repeat columns that are
					// clipped as a result of lack of enough remaining space on the current page.
					//
					if ( ColumnClipMode.RepeatClippedColumns == columnClipMode 
						&& scaleFactor * ( range.end - currentPageOrigin ) > physicalPage.Width )
					{
						// If the column is wider than the page, then don't even bother repeating
						// it on the next page.
						//
						bool skipColumnSplittingLogic = scaleFactor * ( range.end - range.start ) > physicalPage.Width;
						
						// If this is the first column (starts at the band's origin), then don't
						// repeat it on the second page.
						//
						if ( ! skipColumnSplittingLogic && Math.Abs( band.GetOrigin( BandOrigin.RowSelector ) - range.start ) <= 2 )
						{
							// In horizontal view style do so only for the first band.
							//
							if ( 0 == band.Index || ! band.Layout.ViewStyleImpl.HasMultiRowTiers )
								skipColumnSplittingLogic = true;
						}

						if ( ! skipColumnSplittingLogic )
						{
							// The column doesn't fit the remaining space so start a new page
							// with the origin of the column as the origin of the new page.
							//
							this.StartNewPage( ref pageExtent, ref totalExtent, 
								range.start, range.end - range.start, addPageHOrigin );

							// Set the currentPageOrigin to the origin of the new page.
							//
							currentPageOrigin = range.start;
						}
					}

					// SSP 3/19/04 UWG2917
					// If this is the last item, then make sure it's end is taken into account when
					// calculating the total extent.
					//
					if ( list.Count - 1 == i )
					{
						double slack = scaleFactor * ( range.end - currentPageOrigin ) - pageExtent;
						if ( slack > 0.0 )
						{
							pageExtent += slack;
							totalExtent += slack;
						}
					}
				}

				// Commented out the following code and added the above code to replace it.
				//
				
				// ----------------------------------------------------------------------------------
			}
		}

		// MRS 6/10/04 UWG2968
		//private Size PhysicalPageMarginBoundsSize
		internal Size PhysicalPageMarginBoundsSize
		{
			get
			{
				// SSP 4/11/02 UWG1049
				// Created a flag to cache the paper dimensions that we get from the printer manager.
				// Apparently, they are not cached by the printer manager, and if called repeatedly,
				// it will slow down considerably (instead of taking a a few seconds, it takes a
				// few minutes without this caching).
				//
				if ( this.physicalPaperSizeDirtyFlag )
				{
					// SSP 10/9/02 UWG1726
					// Commented out below code and added new one below it.
					//
					//-------------------------------------------------------------------------------
					

					this.physicalPageMarginBoundsSize = 
						new Size( this.printDocument.DefaultPageSettings.PaperSize.Width,
						this.printDocument.DefaultPageSettings.PaperSize.Height );

					if ( null != this.printDocument && null != this.printDocument.DefaultPageSettings &&
						this.printDocument.DefaultPageSettings.Landscape )
					{
						this.physicalPageMarginBoundsSize = new Size(
							this.physicalPageMarginBoundsSize.Height,
							this.physicalPageMarginBoundsSize.Width );
					}

					// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
					// Have the ultragridprintdocument remove any area used by
					// its header/footer/page border/padding/etc.
					//
					// MRS 10/21/05 - BR06946
					// CalculateAvailablePageSize is now a public method on 
					// UltraPrintDocument
					//
//					if (this.printDocument is UltraGridPrintDocument)
//						this.physicalPageMarginBoundsSize = ((UltraGridPrintDocument)this.printDocument).CalculateAvailablePageSize(this.physicalPageMarginBoundsSize);					
					if (this.printDocument is UltraPrintDocument)
						this.physicalPageMarginBoundsSize = ((UltraPrintDocument)this.printDocument).CalculateAvailablePageSize(this.physicalPageMarginBoundsSize);					

					this.physicalPageMarginBoundsSize.Width -=
						( this.printDocument.DefaultPageSettings.Margins.Right +
						this.printDocument.DefaultPageSettings.Margins.Left );

					this.physicalPageMarginBoundsSize.Height -=
						( this.printDocument.DefaultPageSettings.Margins.Bottom +
						this.printDocument.DefaultPageSettings.Margins.Top );
					
					// MRS 10/22/04 - UWG3718
					// Account for the borders
					UltraGridLayout layout = this.grid.PrintLayout;
					this.physicalPageMarginBoundsSize.Height -=
						( layout.GetBorderThickness(layout.BorderStyle) * 2 );

					this.physicalPageMarginBoundsSize.Width -=
						( layout.GetBorderThickness(layout.BorderStyle) * 2 );

					//-------------------------------------------------------------------------------

					// SSP 4/11/02 UWG1049
					// Created a flag to cache the paper dimentions that we get from the printer manager.
					// Apparently, they are not cached by the printer manager, and if called repeatedly,
					// it will slow down considerably (instead of taking a a few seconds, it takes a
					// few minutes without this caching).
					// Set the flag to false.
					//
					this.physicalPaperSizeDirtyFlag = false;
				}

				return this.physicalPageMarginBoundsSize;
			}
		}
		private void StartNewPage( ref double pageExtent , 
									ref double totalExtent,
									long origin,
									double colWidth, bool addPageHOrigin )
		{

			// pad the extent, so that the column gets
			// bumped to a new page.
			//
            totalExtent += this.PhysicalPageMarginBoundsSize.Width - pageExtent;
			pageExtent += this.PhysicalPageMarginBoundsSize.Width - pageExtent;

			colWidth *= this.scaleFactor;

			//add this column's width to extents
			//
            pageExtent += colWidth;
			totalExtent += colWidth;

			
			//if we are only testing to see which band is the widest 
			//we don't need to add the horizontal origin yet
			//this will be added when we call GetBandOverallWidth with 
			//the widest band
			//
			if ( addPageHOrigin )
			{
                this.SetPageHOrigin( logicalPageDimensions.Width, origin );	
				++logicalPageDimensions.Width;
			}

			//*vl 021501 - we probably need to reset lPageExtent now
			pageExtent = colWidth;
		}
		internal void PrintPage(object sender, PrintPageEventArgs ev)
		{
		
			Graphics graphics = ev.Graphics;

			//Set pagescale if scaleFactor was set
			//
			if ( this.scaleFactor != 1 )
				graphics.PageScale = (float)this.scaleFactor;

			//Set printing flag
			//
			this.grid.IsPrinting = true;

			Rectangle scaledRect = ev.MarginBounds;

			// scale the rect up
			//
			if ( this.scaleFactor != 1 )
			{
				// SSP 12/2/02 UWG1772
				// Don't OFFSET the rect, but rather SCALE the rect.
				//
				//scaledRect.Offset( (int)(this.scaleFactor * (double)scaledRect.X ), (int)(this.scaleFactor * (double)scaledRect.Y) );
				scaledRect.X = (int)( (double)scaledRect.X / this.scaleFactor );
				scaledRect.Y = (int)( (double)scaledRect.Y / this.scaleFactor );
				
				scaledRect.Width	= (int)((double)scaledRect.Width / this.scaleFactor);
				scaledRect.Height	= (int)((double)scaledRect.Height / this.scaleFactor);
			}

			//center grid
			//
			this.grid.PrintLayout.UIElement.Rect = scaledRect;

			
			//Get the horizontal position needed to scroll to
			//
			long pos = this.GetPageHOrigin( this.CurrentPrintingPage.x );
			
			// MRS 4/29/05 - BR03596 
			// If the user cancelled the first logical page, then there's
			// nothing to print. 
			if (this.logicalPageList.Length == 0)
				return;

			//Get the row we need to scroll to
			//
			LogicalPageInfo logPageInfo = this.GetLogicalPageInfo( this.CurrentPrintingPage.y );

            // MRS NAS v8.2 - CardView Printing
            // Check for CardView. I add this 'if' block and put the existing code into the 'else'
            if (this.IsPrintingCards)
            {
                UltraGridLayout printLayout = this.grid.PrintLayout;
                printLayout.Rows.FirstVisibleCardRow = logPageInfo.FirstRow;
                printLayout.UIElement.VerifyChildElements(true);
                
                CardAreaUIElement cardAreaUIElement = printLayout.UIElement.GetDescendant(typeof(CardAreaUIElement)) as CardAreaUIElement;
                if (cardAreaUIElement != null)
                {
                    cardAreaUIElement.CardAreaScrollRegionVerticalOffset = logPageInfo.ScrollOffset;
                    printLayout.UIElement.DirtyChildElements(true);
                    printLayout.UIElement.VerifyChildElements(true);

                    CardAreaScrollRegionUIElement cardAreaScrollRegionUIElement = cardAreaUIElement.GetDescendant(typeof(CardAreaScrollRegionUIElement)) as CardAreaScrollRegionUIElement; 
                    Debug.Assert(cardAreaScrollRegionUIElement != null, "Failed to get CardAreaScrollRegionUIElement; unexpected.");
                    if (cardAreaScrollRegionUIElement != null)
                    {
                        AdjustCardElementHorizontalPositions(cardAreaUIElement, (int)-pos);                        

                        // If any card is taller or wider than the page,
                        // and the ClipRowBehavior is cellbased, then we need to replace any
                        // clipped cells with blank white UIElements so they will not print
                        // on this page. We can't just remove them, because we don't want the gray 
                        // (Control - color) background of the RowUIElement printing. 
                        ClipRowBehavior clipRowBehavior = this.ClipRowBehaviorResolved;
                        if (clipRowBehavior == ClipRowBehavior.CellBased)
                        {
                            int borderThickness = printLayout.GetBorderThickness(printLayout.BorderStyleResolved);

                            if (IsAnyCardClipped(cardAreaScrollRegionUIElement, cardAreaUIElement.Rect.Bottom, true))
                            {
                                int pageHeight = (int)this.GetAvailablePageHeight(borderThickness);
                                GetPartiallyClippedElements(cardAreaUIElement, cardAreaUIElement.Rect.Bottom, pageHeight, true, ClippedElementAction.Replace, typesToExcludeFromClippingInCardViewPrinting);
                            }

                            if (IsAnyCardClipped(cardAreaScrollRegionUIElement, cardAreaUIElement.Rect.Right, false))
                            {
                                int pageWidth = (int)this.GetAvailablePageWidth(borderThickness);
                                GetPartiallyClippedElements(cardAreaUIElement, cardAreaUIElement.Rect.Right, pageWidth, false, ClippedElementAction.Replace, typesToExcludeFromClippingInCardViewPrinting);
                            }
                        }
                    }
                }
                else
                    Debug.Fail("Could not find the cardAreaUIElement when printing in CardView");
                
            }
            else
            {
                // SSP 3/17/05 BR02832
                // If a row being printed is taller than the page height (not the same
                // thing as if it cross the bottom of the page) then start the row on
                // the next page at the point where we left off. Added the following
                // line of code.
                //
                this.grid.PrintLayout.RowScrollRegions[0].scrollOffset = logPageInfo.ScrollOffset;

                //set first row 
                //
                this.grid.PrintLayout.RowScrollRegions[0].SetFirstRow(logPageInfo.FirstRow);
                this.grid.PrintLayout.RowScrollRegions[0].SetDestroyVisibleRows();

                //set col position
                //
                this.grid.PrintLayout.ColScrollRegions[0].Position = (int)pos;
                this.grid.PrintLayout.ColScrollRegions[0].SetDestroyVisibleHeaders();
            }
			
			
			//Draw the grid
			//
			this.grid.PrintLayout.UIElement.Print( graphics, this.textNeedsClipping );

			//Reset printing flag
			//
			this.grid.IsPrinting = false;


			ev.HasMorePages = this.NextPageHelper();	
			
			if ( !ev.HasMorePages )
			{
				//RobA 10/25/01
				//we should not need to worry about copies
				
				// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
				// Do not reset the starting page now. It will be reset
				// when the BeginPrint is called. Otherwise, we have no
				// way of determing that the print is complete.
				//
				////Reset to start page
				////also resets copiesprinted
				////
				//this.SetStartPage();

				//set back to false just in case this gets call from 
				//print preview 
				//
				this.textNeedsClipping = false;
			}			
			
		}
		private bool NextPageHelper()
		{
			bool result = false;

			//RobA 10/25/01
			//we should not need to worry about copies
			

			//increment to the next physical page within this logical page
			//currentPageX are physical pages
			//currentPageY are logical pages
			//
			++this.CurrentPrintingPage.x;

			//if out of physical pages within the logical page
			//go to the next logical page otherwise set hasmorepages
			//
			if ( this.CurrentPrintingPage.x >= this.logicalPageDimensions.Width )
			{
				//set to the first physical page
				//
				this.CurrentPrintingPage.x = 0;

				//increment to next logical page
				//
				++this.CurrentPrintingPage.y;

				//if there is another logical page set hasmorepages to true
				//<
				if ( this.CurrentPrintingPage.y < this.logicalPageDimensions.Height )	
					result = this.MorePages();				
			}

			else
				//set MorePages to true if there are any
				//
				result = this.MorePages();

			return result;
		}

		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		// Need a way for the UltraGridPrintDocument to know if there are more pages.
		#region HasMorePages
		internal bool HasMorePages
		{
			get
			{
				if (this.printDocument == null)
					return false;

				if (this.CurrentPrintingPage.y >= this.logicalPageDimensions.Height)
					return false;

				return this.MorePages();
			}
		}
		#endregion //HasMorePages

		internal void BeginPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
		{	
					
			//Fire Before print event only if called from print
			//
			if ( this.calledFromPrint )
			{
				//Cache these objects so we can compare after we fire beforeprint event
				//
				this.SaveToCache();

				//Fire Before Print event
				//
				// SSP 12/15/03 UWG2560
				// Added PrintLayout property to InitializePrint and InitializePrintPreview event args.
				// Pass in the print layout.
				//
				//CancelablePrintEventArgs printEvent = new CancelablePrintEventArgs( this.PrintDocument, this.DefaultLayoutInfo );
				CancelablePrintEventArgs printEvent = new CancelablePrintEventArgs( this.PrintDocument, this.DefaultLayoutInfo, this.grid.PrintLayout );
				this.grid.FireEvent( GridEventIds.BeforePrint, printEvent);

				//return if it was cancelled
				//
				if ( printEvent.Cancel )
				{
					e.Cancel = true;
					return;
				}

				//if this was modified we need to recalculate
				//
				if ( this.hasCalculatedPageDimensions && 
					this.HaveCacheValuesChanged() )
				{
					//If the LogicalPageLayoutInfoHolder was modified
					//set flag to false
					//
					this.hasCalculatedPageDimensions = false;
				}

				//Determine if text is goign to be clipped
				//
				this.textNeedsClipping = this.ShouldClipText();
				
			}
			
			//Only call this once
			//
			if ( !this.HasCalculatedPageDimensions )
			{
				//calculate page positions based on PrintDialog
				//set HasCalculatedPageDimensions to true
				//
				this.CalculatePageDimensions();

				//need to set this to true so that if we are already in print preview
				//and the user clicks print we need to fire the before print event
				//
				this.calledFromPrint = true;
			}

            // MRS 12/20/05 - BR08299
            // Apparently, the FromPage does not validate it's value against the MinimumPage, 
            // but rather only raises an exception if it's set to less than 0. 
            // So even though we are setting MinimumPage to 1, we still need to check for 0 here. 
            if ((this.PrintDocument.PrinterSettings.PrintRange != PrintRange.AllPages &&
                this.PrintDocument.PrinterSettings.FromPage <= 0))
            {
                this.PrintDocument.PrinterSettings.FromPage = 1;
            }

			// MRS 4/8/05 - BR03233
			if ( (this.PrintDocument.PrinterSettings.PrintRange != PrintRange.AllPages &&
				this.PrintDocument.PrinterSettings.FromPage > this.TotalNumberOfPages ) 
				// MRS 4/29/05 - BR03596 
				// If the user cancelled the first logical page, then there's
				// nothing to print. 
				|| (null == this.logicalPageList
				|| this.logicalPageList[0] == null) )				
			{
				e.Cancel = true;
				return;
			}

			//Set CurrentLogicalPage to start on FromPage
			//determined by the print Document
			//
			this.SetStartPage();

		}

		// MRS 5/21/04 - UWG2915
		#region EndPrint
		internal void EndPrint( object sender, System.Drawing.Printing.PrintEventArgs e )
		{			
			DisposePrinterMetricsGraphicsHelper();
		}

		internal void DisposePrinterMetricsGraphicsHelper()
		{
			if ( this.printerMetricsGraphics != null )
			{
				this.printerMetricsGraphics.Dispose();
				this.printerMetricsGraphics = null;
			}
		}
		#endregion EndPrint

		
		private bool ShouldClipText()
		{
			bool result = false;
			
			//if auto, determine if this is a printer that has a problem
			//
			if ( this.defaultLayoutInfo.ClippingOverride == ClippingOverride.Auto )
			{
				for ( int i=0; i < this.printerClipError.Length; ++i)
				{
					// AS 1/8/03 fxcop
					// Explicitly specify a cultureinfo
					//
					//string printerName = this.printDocument.PrinterSettings.PrinterName.ToUpper();
					//string clipError = this.printerClipError[i].ToUpper();
					string printerName = this.printDocument.PrinterSettings.PrinterName.ToUpper( System.Globalization.CultureInfo.CurrentCulture );
					string clipError = this.printerClipError[i].ToUpper( System.Globalization.CultureInfo.CurrentCulture );
					
					if ( printerName.IndexOf(clipError) != -1 )
						result = true;					
				}
			}

			//if override is yes, return true to clip
			//
			else if ( this.defaultLayoutInfo.ClippingOverride == ClippingOverride.Yes )
				result = true;

			return result;
		}

		private void SaveToCache()
		{
			this.cachedLogicalPageLayoutInfo = new LogicalPageLayoutInfo( this.grid );
			this.cachedPrintDocument = new PrintDocument();


			//save the passed in LogicalPageLayoutInfo settings
			//
			this.cachedLogicalPageLayoutInfo.FitWidthToPages = this.defaultLayoutInfo.FitWidthToPages;
			this.cachedLogicalPageLayoutInfo.PageFooter = this.defaultLayoutInfo.PageFooter;
			this.cachedLogicalPageLayoutInfo.PageFooterBorderStyle = this.defaultLayoutInfo.PageFooterBorderStyle;
			this.cachedLogicalPageLayoutInfo.PageFooterHeight = this.defaultLayoutInfo.PageFooterHeight;
			this.cachedLogicalPageLayoutInfo.PageHeader = this.defaultLayoutInfo.PageHeader;
			this.cachedLogicalPageLayoutInfo.PageFooterBorderStyle = this.defaultLayoutInfo.PageHeaderBorderStyle;
			this.cachedLogicalPageLayoutInfo.PageHeaderHeight = this.defaultLayoutInfo.PageHeaderHeight;

			//save the passed in PrintDocument settings
			//that would cause us to have to recalculate
			//
			this.cachedPrintDocument.DefaultPageSettings = (PageSettings)this.PrintDocument.DefaultPageSettings.Clone();	
		
		}

		
		private bool HaveCacheValuesChanged()
		{			
			return ( 
				
				//compare cachedLogicalPageLayoutInfo with the passed 
				//in LogicalPageLayoutInfo settings
				//
				this.cachedLogicalPageLayoutInfo.FitWidthToPages != this.defaultLayoutInfo.FitWidthToPages ||
				this.cachedLogicalPageLayoutInfo.PageFooter != this.defaultLayoutInfo.PageFooter			 ||
				this.cachedLogicalPageLayoutInfo.PageFooterBorderStyle != this.defaultLayoutInfo.PageFooterBorderStyle ||
				this.cachedLogicalPageLayoutInfo.PageFooterHeight != this.defaultLayoutInfo.PageFooterHeight ||
				this.cachedLogicalPageLayoutInfo.PageHeader != this.defaultLayoutInfo.PageHeader ||
				this.cachedLogicalPageLayoutInfo.PageFooterBorderStyle != this.defaultLayoutInfo.PageHeaderBorderStyle ||
				this.cachedLogicalPageLayoutInfo.PageHeaderHeight != this.defaultLayoutInfo.PageHeaderHeight ||
				this.defaultLayoutInfo.HasAppearanceChanged ||

				//compare the cachedPrintDocument settings with the passed 
				//in PrintDocument settings
				//
				this.cachedPrintDocument.DefaultPageSettings.Bounds != this.PrintDocument.DefaultPageSettings.Bounds ||
				this.cachedPrintDocument.DefaultPageSettings.Landscape != this.PrintDocument.DefaultPageSettings.Landscape ||
				!this.cachedPrintDocument.DefaultPageSettings.Margins.Equals( this.PrintDocument.DefaultPageSettings.Margins )||
				this.cachedPrintDocument.DefaultPageSettings.PaperSize != this.PrintDocument.DefaultPageSettings.PaperSize );	
	
			
		}

		private bool MorePages()
		{
			//If print range is not all, print only up to the ToPage
			//
			return ( this.PrintDocument.PrinterSettings.PrintRange == PrintRange.AllPages  ||
				  (this.CurrentPrintingPageNumber <= this.PrintDocument.PrinterSettings.ToPage ) );		
				
		}
		internal int CurrentPrintingPageNumber
		{
			get
			{
				return ( (this.currentPrintingPage.x + 1)  + (this.currentPrintingPage.y * this.logicalPageDimensions.Width) );
			}
		}


		internal int TotalNumberOfPages
		{
			get
			{
				return ( this.logicalPageDimensions.Height * this.logicalPageDimensions.Width );
			}
		}

		#region PrinterMetricsGraphics
		/// <summary>
		/// Gets the Printer Metrics Graphics object which can be used for Metrics calculations involving printing. 
		/// </summary>
		internal Graphics PrinterMetricsGraphics
		{
			get 
			{ 
				if ( this.printerMetricsGraphics == null )
					  this.printerMetricsGraphics = PrintDocument.PrinterSettings.CreateMeasurementGraphics();
			
				return this.printerMetricsGraphics; 
			}
		}
		#endregion PrinterMetricsGraphics

        // MRS NAS v8.2 - CardView Printing
        #region NAS v8.2 - CardView Printing

        #region Private Members
        private static Type[] typesToExcludeFromClippingInCardViewPrinting = new Type[] { typeof(RowUIElement), typeof(CardLabelAreaUIElement), typeof(RowCellAreaUIElement), typeof(CardAreaScrollRegionUIElement) };
        private enum ClippedElementAction
        {
            None, 
            Replace,
            Remove
        }
        #endregion //Private Members

        #region Private Properties

        #region IsPrintingCards
        private bool IsPrintingCards
        {
            get
            {
                return this.grid.PrintLayout.SortedBands[0].CardView;
            }
        }
        #endregion //IsPrintingCards

        #endregion //Private Properties

        #region Private Methods

        #region AdjustCardElementHorizontalPositions
        private static void AdjustCardElementHorizontalPositions(CardAreaUIElement cardAreaUIElement, int hScrollOffset)
        {
            CardAreaScrollRegionUIElement cardAreaScrollRegionUIElement = cardAreaUIElement.GetDescendant(typeof(CardAreaScrollRegionUIElement)) as CardAreaScrollRegionUIElement;
            Debug.Assert(cardAreaScrollRegionUIElement != null, "Failed to get CardAreaScrollRegionUIElement; unexpected.");
            if (cardAreaScrollRegionUIElement != null)
            {
                if (hScrollOffset != 0)
                {
                    foreach (UIElement element in cardAreaScrollRegionUIElement.ChildElements)
                    {
                        element.Offset(hScrollOffset, 0);
                        element.DirtyChildElements(true);
                        element.VerifyChildElements(true);
                    }
                }
            }
        }
        #endregion //AdjustCardElementHorizontalPositions

        #region AdjustOffsetForCellBasedCutOffPoint
        private static int AdjustOffsetForCellBasedCutOffPoint(CardAreaUIElement cardAreaUIElement, int limit, int maxExtent, bool vertical, int scrollOffset)
        {
            UIElement[] partiallyClippedElements = GetPartiallyClippedElements(cardAreaUIElement, limit, maxExtent, vertical, ClippedElementAction.None, typesToExcludeFromClippingInCardViewPrinting);

            int lowestCutOffPoint = int.MaxValue;
            foreach (UIElement element in partiallyClippedElements)
            {
                int elementLimit = vertical ? element.Rect.Top : element.Rect.Left;
                lowestCutOffPoint = Math.Min(elementLimit, lowestCutOffPoint);
            }

            if (lowestCutOffPoint != int.MaxValue)
            {
                int delta = maxExtent - lowestCutOffPoint;
                if (delta > 0 && delta < maxExtent)
                    scrollOffset -= delta;
            }
            return scrollOffset;
        }
        #endregion //AdjustOffsetForCellBasedCutOffPoint

        #region CalculateCardViewHorizontalOrigins
        private void CalculateCardViewHorizontalOrigins()
        {
            // MRS NAS v8.2 - CardView Printing
            UltraGridLayout printLayout = this.grid.PrintLayout;

            // Make sure we have at least one row
            //
            if (printLayout.Rows.VisibleRowCount <= 0)
                return;

            Size availablePageSize = GetAvailablePageSize(printLayout);

            UltraGridRow firstRow = printLayout.Rows.GetFirstVisibleRow();
            CardAreaUIElement cardAreaUIElement = GetTemporaryCardAreaUIElement(printLayout, availablePageSize, firstRow);

            UIElement rowElement = cardAreaUIElement.GetDescendant(typeof(RowUIElement));
            if (rowElement == null)
            {
                // No rows, just return.
                // I guess this could be a problem if the labels fill the entire page, but
                // that's such an edge case, let's not worry about it for now. 
                return;
            }

            int totalWidthRequired = rowElement.Rect.Right;

            // MRS 4/22/2009 - TFS16768
            // These needs to get reset on each new print operation, so that we 
            // don't keep adding extra pages. 
            //
            this.logicalPageDimensions.Width = 1;
            this.logicalPageDimensions.Height = 0;

            // Is a card wider than the page?
            if (totalWidthRequired > availablePageSize.Width)
            {
                ClipRowBehavior clipRowBehavior = this.ClipRowBehaviorResolved;

                int hScrollOffset = 0;

                this.SetPageHOrigin(this.logicalPageDimensions.Width, hScrollOffset);
                ++this.logicalPageDimensions.Width;

                hScrollOffset = availablePageSize.Width;

                while (hScrollOffset < totalWidthRequired)
                {
                    if (clipRowBehavior == ClipRowBehavior.CellBased)
                    {
                        hScrollOffset = AdjustOffsetForCellBasedCutOffPoint(
                            cardAreaUIElement,
                            availablePageSize.Width,
                            availablePageSize.Width,
                            false,
                            hScrollOffset);
                    }

                    this.SetPageHOrigin(this.logicalPageDimensions.Width, hScrollOffset);
                    ++this.logicalPageDimensions.Width;

                    AdjustCardElementHorizontalPositions(cardAreaUIElement, -hScrollOffset);

                    hScrollOffset += availablePageSize.Width;
                }
            }
        }
        #endregion //CalculateCardViewHorizontalOrigins

        #region CalculateFirstCards

        private void CalculateFirstCards()
        {
            UltraGridLayout printLayout = this.grid.PrintLayout;

            // Make sure we have at least one row
            //
            if (printLayout.Rows.VisibleRowCount <= 0)
            {
                // if there are no rows, add a single empty page
                //
                // RobA UWG743 11/19/01
                this.AddLogicalPrintPage(null);
                return;
            }

            UltraGridRow firstRow = printLayout.Rows.GetFirstVisibleRow();
            bool moreRows = true;
            int visibleRowIndex = firstRow.VisibleIndex;

            //Add the first Print Page
            //
            moreRows = this.AddLogicalPrintPage(firstRow);
            if (!moreRows)
                return;

            int vScrollOffset = 0;
            ClipRowBehavior clipRowBehavior = this.ClipRowBehaviorResolved;            

            CardAreaUIElement cardAreaUIElement = null;

            UIElement parentElement = printLayout.UIElement.GetDescendant(typeof(RowColRegionIntersectionUIElement));
                        
            //Loop over rows and determine number of 
            //logicalPageDimensions.Height
            //
            do
            {
                Size availablePageSize = GetAvailablePageSize(printLayout);

                // MRS 4/22/2009 - TFS16768
                // The row passed into GetTemporaryCardAreaUIElement (below), is not
                // applied as the first visible card. The first visible card comes only
                // from the FirstVisibleCard property. 
                //
                firstRow.ParentCollection.FirstVisibleCard = firstRow;

                if (cardAreaUIElement == null)
                    cardAreaUIElement = GetTemporaryCardAreaUIElement(printLayout, availablePageSize, firstRow);
                else
                {
                    // MRS 4/22/2009 - TFS16768
                    // Calling InitializeRow doesn't actually set the firstRow on the element. To 
                    // do that, we have to set the FirstVisibleCard. So this is not longer
                    // neccessary. So since we are now setting FirstVisibleCard above, we no
                    // longer need this code. 
                    //
                    //cardAreaUIElement.InitializeRow(firstRow);
                    //cardAreaUIElement.VerifyChildElements(true);
                }

                int totalVisibleCards = cardAreaUIElement.TotalVisibleCards;
                int heightAvailableForCards = cardAreaUIElement.HeightAvailableForCards;

                //If there is only one card on the page, check to see if it's taller than the page height. 
                
                int cardAreaHeight = cardAreaUIElement.Rect.Height;
                vScrollOffset = cardAreaHeight;

                CardAreaScrollRegionUIElement cardAreaScrollRegionUIElement = cardAreaUIElement.GetDescendant(typeof(CardAreaScrollRegionUIElement)) as CardAreaScrollRegionUIElement;
                if (cardAreaScrollRegionUIElement != null)
                {
                    bool isAnyCardClipped = IsAnyCardClipped(cardAreaScrollRegionUIElement, vScrollOffset, true);

                    while (isAnyCardClipped)
                    {
                        if (clipRowBehavior == ClipRowBehavior.CellBased)
                        {
                            vScrollOffset = AdjustOffsetForCellBasedCutOffPoint(cardAreaUIElement, vScrollOffset, vScrollOffset, true, vScrollOffset);                            
                        }

                        moreRows = this.AddLogicalPrintPage(firstRow, vScrollOffset);
                        vScrollOffset += cardAreaHeight;

                        isAnyCardClipped = IsAnyCardClipped(cardAreaScrollRegionUIElement, vScrollOffset, true);
                    }
                }
                else
                    Debug.Fail("Failed to get CardAreaScrollRegionUIElement; unexpected.");
                
                firstRow = printLayout.Rows.GetRowAtVisibleIndexOffset(firstRow, totalVisibleCards);
                if (firstRow == null)
                    moreRows = false;
                else
                    moreRows = this.AddLogicalPrintPage(firstRow);                    

            } while (moreRows);
        }        

        #endregion //CalculateFirstCards

        #region GetPageSize
        private Size GetAvailablePageSize()
        {
            UltraGridLayout printLayout = this.grid.PrintLayout;
            return this.GetAvailablePageSize(printLayout);
        }

        private Size GetAvailablePageSize(UltraGridLayout printLayout)
        {
            int borderThickness = printLayout.GetBorderThickness(printLayout.BorderStyleResolved);
            int pageWidth = (int)(this.GetAvailablePageWidth(borderThickness));
            int pageHeight = (int)(this.GetAvailablePageHeight(borderThickness));

            return new Size(pageWidth, pageHeight);
        }
        #endregion //GetPageSize

        #region GetAvailablePageHeight
        /// <summary>
        /// Returns the available width of the page in screen coords. This is used to determine 
        /// the size of the on-screen grid object that will be printed so that it fits on a 
        /// printed page. It is not the size in printer coords. 
        /// </summary>
        /// <param name="borderThickness"></param>
        /// <returns></returns>
        private double GetAvailablePageHeight(int borderThickness)
        {
            double footerHeight;
            double headerHeight;

            if (this.LogicalPageLayoutInfoHolder.PageFooterHeight == -1)
                footerHeight = 0;
            else
                footerHeight = this.LogicalPageLayoutInfoHolder.PageFooterHeight;

            if (this.LogicalPageLayoutInfoHolder.PageHeaderHeight == -1)
                headerHeight = 0;
            else
                headerHeight = this.LogicalPageLayoutInfoHolder.PageHeaderHeight;


            headerHeight += (2 * this.grid.PrintLayout.GetBorderThickness(this.LogicalPageLayoutInfoHolder.PageHeaderBorderStyle));
            footerHeight += (2 * this.grid.PrintLayout.GetBorderThickness(this.LogicalPageLayoutInfoHolder.PageFooterBorderStyle));

            // SSP 7/6/04 UWG3489
            // Don't scale the header and footer heights becase the user specifies them already 
            // scaled. For example, if the scale factor is 0.5 and if the user specifies 100
            // as the header height, on the paper it will be 50 because the graphics will scale
            // the whole page by 0.5.
            // 
            //headerHeight /= this.scaleFactor;
            //footerHeight /= this.scaleFactor;

            double height = this.PhysicalPageMarginBoundsSize.Height;
            height /= this.scaleFactor;
            height -= (footerHeight + headerHeight);
            height -= borderThickness * 2;
            return height;
        }
        #endregion //GetAvailablePageHeight

        #region GetAvailablePageWidth
        /// <summary>
        /// Returns the available height of the page in screen coords. This is used to determine 
        /// the size of the on-screen grid object that will be printed so that it fits on a 
        /// printed page. It is not the size in printer coords. 
        /// </summary>
        /// <param name="borderThickness"></param>
        /// <returns></returns>
        private double GetAvailablePageWidth(int borderThickness)
        {
            double width = this.PhysicalPageMarginBoundsSize.Width;
            width /= this.scaleFactor;
            width -= borderThickness * 2;
            return width;
        }
        #endregion //GetAvailablePageWidth

        #region GetClippedElements
        // Finds all the elements inside the parent element that will be clipped given the 
        // speficied limit. The limit is either an X (when vertical is false) or Y (when vertical
        // is true) value. 
        private static UIElement[] GetPartiallyClippedElements(UIElement containingElement, int limit, int maxExtent, bool vertical, ClippedElementAction action, Type[] excludedTypes)
        {
            List<UIElement> clippedElementsList = new List<UIElement>();
            GetPartiallyClippedElementsHelper(containingElement, limit, maxExtent, vertical, action, clippedElementsList, excludedTypes);
            return clippedElementsList.ToArray();
        }
        private static void GetPartiallyClippedElementsHelper(UIElement containingElement, int limit, int maxExtent, bool vertical, ClippedElementAction action, List<UIElement> clippedElementsList, Type[] excludedTypes)
        {
            UIElement[] childElements = containingElement.ChildElements.ToArray(typeof(UIElement)) as UIElement[];
            foreach (UIElement element in childElements)
            {
                if (element is CellUIElement)
                {
                    UltraGridCell cell = element.GetContext(typeof(UltraGridCell)) as UltraGridCell;
                    Debug.WriteLine(element.Rect, cell.Column.Key);
                }
                if (IsObjectOfType(element, excludedTypes) == false)
                {
                    if (IsElementClipped(element, limit, maxExtent, vertical))
                    {
                        clippedElementsList.Add(element);

                        switch (action)
                        {
                            case ClippedElementAction.None:
                                break;
                            case ClippedElementAction.Remove:
                                containingElement.ChildElements.Remove(element);
                                break;
                            case ClippedElementAction.Replace:
                                containingElement.ChildElements.Remove(element);
                                BlankUIElement blankElement = new BlankUIElement(containingElement);
                                blankElement.Rect = element.Rect;
                                containingElement.ChildElements.Add(blankElement);
                                break;
                        }

                        continue;
                    }
                }

                GetPartiallyClippedElementsHelper(element, limit, maxExtent, vertical, action, clippedElementsList, excludedTypes);
            }
        }

        private static bool IsCardElementClipped(UIElement element, int limit, bool vertical)
        {
            int elementEnd = (vertical)
                ? element.Rect.Bottom
                : element.Rect.Right;

            return elementEnd > limit;
        }

        private static bool IsElementClipped(UIElement element, int limit, int maxExtent, bool vertical)
        {
            int elementExtent = vertical
                ? element.Rect.Height
                : element.Rect.Width;

            if (elementExtent > maxExtent)
                return false;

            int elementBegin = (vertical)
                ? element.Rect.Top
                : element.Rect.Left;

            if (elementBegin < 0)
                return false;

            int elementEnd = (vertical)
                ? element.Rect.Bottom
                : element.Rect.Right;

            return elementBegin < limit && elementEnd > limit;
        }

        private static bool IsAnyCardClipped(CardAreaScrollRegionUIElement cardAreaScrollRegionUIElement, int limit, bool vertical)
        {
            foreach (UIElement element in cardAreaScrollRegionUIElement.ChildElements)
            {
                if (IsCardElementClipped(element, limit, vertical))
                    return true;
            }

            return false;            
        }

        #endregion // GetClippedElements
        
        #region GetTemporaryCardAreaUIElement
        private static CardAreaUIElement GetTemporaryCardAreaUIElement(UltraGridLayout printLayout, Size size, UltraGridRow firstRow)
        {
            UIElement parentElement = printLayout.UIElement.GetDescendant(typeof(RowColRegionIntersectionUIElement));
            Debug.Assert(parentElement != null, "Failed to get RowColRegionIntersectionUIElement; unexpected");

            CardAreaUIElement cardAreaUIElement = new CardAreaUIElement(parentElement, firstRow);
            cardAreaUIElement.Rect = new Rectangle(0, 0, size.Width, size.Height);
            cardAreaUIElement.VerifyChildElements(true);
            return cardAreaUIElement;
        }
        #endregion //GetTemporaryCardAreaUIElement

        #region IsObjectOfType
        private static bool IsObjectOfType(object o, Type[] types)
        {
            foreach (Type type in types)
            {
                if (o.GetType().IsAssignableFrom(type))
                    return true;
            }

            return false;
        }
        #endregion //IsObjectOfType        

        #endregion //Private Methods

        #region BlankUIElement class
        internal class BlankUIElement : UIElement
        {
            internal BlankUIElement(UIElement parent) : base(parent) { }

            protected override void DrawBackColor(ref UIElementDrawParams drawParams)
            {
                drawParams.Graphics.FillRectangle(Brushes.White, drawParams.Element.Rect);
            }
        }
        #endregion //BlankUIElement class

        #endregion //NAS v8.2 - CardView Printing
    }	
 
	internal class Page
	{
		public int x = 0;
		public int y = 0;

		internal Page( int x, int y )
		{
			this.x = x;
			this.y = y;
		}		

		internal void Reset()
		{
			this.x = 0;
			this.y = 0;
		}
	}

	//RobA 10/31/01 made this class in case in the future we want to expose 
	//other properties off the print preview Control 
	//
	/// <summary>
	/// Class used to hold the Print Preview control Settings
	/// </summary>
	public class PrintPreviewSettings
	{
		private double zoom			= -1;
		private int dialogHeight	= -1;
		private int dialogWidth		= -1;
		private int dialogLeft		= -1;
		private int dialogTop		= -1;

        // JDN 1/6/05 BR00581 - Added DialogCaption and DialogIcon properties
        private string dialogCaption    = null;
        private Icon dialogIcon         = null;


		internal PrintPreviewSettings()
		{
		}


		/// <summary>
		/// Returns or sets a value that determines the zoom level of the print preview window.
		/// </summary>
		/// <remarks>
		/// <p class="body">The Zoom property specifies the level of zoom that will be in effect when the Print Preview dialog is first displayed. Once the print preview is displayed, the user can change the zoom level by using the controls available in the dialog.</p>
		/// <p class="body">A value of 1.0 dispalys a full-size apge (100%). The default value is -1, which automatically sizes the page so that the full page is visible in the dialog.</p>
		/// </remarks>
		public double Zoom
		{
			get
			{
				return this.zoom;
			}

			set
			{
				if ( value != this.zoom )
					this.zoom = value;
			}
		}

		/// <summary>
		/// Height of the print preview dialog. The default is -1 (automatically determined).
		/// </summary>
		public int DialogHeight
		{
			get
			{
				return this.dialogHeight;
			}

			set
			{
				if ( this.dialogHeight != value )
					this.dialogHeight = value;
			}
		}

		/// <summary>
		/// Width of the print preview dialog. The default is -1 (automatically determined).
		/// </summary>
		public int DialogWidth
		{
			get
			{
				return this.dialogWidth;
			}

			set
			{
				if ( this.dialogWidth != value )
					this.dialogWidth = value;
			}
		}

		/// <summary>
		/// Returns or sets the print preview dialog's y-coordinate. Default is -1 (automatically determined).
		/// </summary>
		public int DialogTop
		{
			get
			{
				return this.dialogTop;
			}

			set 
			{
				if ( value != this.dialogTop )
					this.dialogTop = value;
			}
		}

		/// <summary>
		/// Returns or sets the print preview dialog's x-coordinate. Default is -1 (automatically determined).
		/// </summary>
		public int DialogLeft
		{
			get
			{
				return this.dialogLeft;
			}

			set 
			{
				if ( value != this.dialogLeft )
					this.dialogLeft = value;
			}
		}
		
        // JDN 1/6/05 BR00581 - Added DialogCaption and DialogIcon properties
        /// <summary>
        /// Caption of the print preview dialog.
        /// </summary>
        public string DialogCaption
        {
            get { return this.dialogCaption; }

            set
            {
                if ( this.dialogCaption != value )
                    this.dialogCaption = value;
            }
        }

        // JDN 1/6/05 BR00581 - Added DialogCaption and DialogIcon properties
        /// <summary>
        /// Icon for the print preview dialog.
        /// </summary>
        public Icon DialogIcon
        {
            get { return this.dialogIcon; }

            set
            {
                if ( this.dialogIcon != value )
                    this.dialogIcon = value;
            }
        }
	};

}
