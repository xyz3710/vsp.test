#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;


namespace Infragistics.Win.UltraWinGrid.Design
{

	/// <summary>
	/// ReferenceConverter used for Column's EditorControl property.
	/// </summary>
	public class ColumnEditorControlReferenceConverter : ComponentConverter
	{

		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="type"></param>
		public ColumnEditorControlReferenceConverter( System.Type type ) : base( type )
		{
		}

		#endregion // Constructor

		#region IsValueAllowed

		/// <summary>
		/// Returns a value indicating whether a particular value can be added to the standard values collection.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="value"></param>
		/// <returns>true if the value is allowed and can be added to the standard values collection; false if the value cannot be added to the standard values collection.</returns>
		/// <remarks>
		/// This reference converter is used for column's EditorControl property. This method returns true for values implementing <see cref="Infragistics.Win.IProvidesEmbeddableEditor"/> ineterface and returning an instance of <see cref="Infragistics.Win.EmbeddableEditorBase"/> through the <see cref="Infragistics.Win.IProvidesEmbeddableEditor.Editor"/> property that supports rendering the column's data type.
		/// </remarks>
		protected override bool IsValueAllowed( ITypeDescriptorContext context, object value )
		{
			UltraGridColumn column = null != context ? context.Instance as UltraGridColumn : null;

			if ( null != column )
			{
				// Allow nulls.
				//
				if ( null == value )
					return true;

				IProvidesEmbeddableEditor embeddableEditorProvider = value as IProvidesEmbeddableEditor;

				if ( null != embeddableEditorProvider )
				{
					EmbeddableEditorBase editor = embeddableEditorProvider.Editor;

					// Check if the editor is appropriate for the column.
					//
					if ( null != editor && column.IsEditorAppropriate( editor ) )
						return true;
				}
			}

			return false;
		}

		#endregion // IsValueAllowed

		#region GetPropertiesSupported

        /// <summary>
        /// Returns whether this object supports properties, using the specified context.
        /// </summary>
        /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
        /// <returns>true if <see cref="System.ComponentModel.TypeConverter.GetProperties(System.Object)"/> should be called to find the properties of this object; otherwise, false.</returns>
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return false;
		}

		#endregion // GetPropertiesSupported
	}

	// SSP 7/22/02
    // Commented this out.    
    //
	
}
