#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using Infragistics.Shared;
using System.Collections;
using System.Drawing;
using System.Reflection;
using System.ComponentModel.Design.Serialization;
using System.ComponentModel.Design;
using Infragistics.Win.AppStyling;
using Infragistics.Win.AppStyling.Definitions;

namespace Infragistics.Win.UltraWinGrid
{
    #region EventArgs and Delegates

    /// <summary>
    /// Used as event parameter for the RowEditTemplate's RowChanged event.
    /// </summary>
    public class RowEditTemplateRowChangedEventArgs : EventArgs
    {
        #region Constructors

        static RowEditTemplateRowChangedEventArgs()
        {
            Empty = new RowEditTemplateRowChangedEventArgs();
        }
        #endregion //Constructors

        #region Members

        /// <summary>
        /// Represents a default instance of the class with no event data.
        /// </summary>
        public static readonly new RowEditTemplateRowChangedEventArgs Empty;

        #endregion //Members
    }

    /// <summary>
    /// Delegate that is used for the RowChanged event of the RowEditTemplate.
    /// </summary>
    public delegate void RowEditTemplateRowChangedEventHandler(object sender, RowEditTemplateRowChangedEventArgs e);

    #endregion //EventArgs and Delegates

    #region RowEditTemplateDialogSettings

    /// <summary>
    /// A class containing various options for controlling the aspects of the form used to
    /// display the RowEditTemplate when using a DisplayMode of Modal or Modeless.
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class RowEditTemplateDialogSettings : Infragistics.Shared.SubObjectBase
    {
        #region Members

        private const FormBorderStyle DEFAULT_FORMBORDERSTYLE = FormBorderStyle.FixedDialog;

        private string caption;
        private FormBorderStyle formBorderStyle = DEFAULT_FORMBORDERSTYLE;        
        private IButtonControl acceptButton;
        private AutoScaleMode autoScaleMode = AutoScaleMode.Font;
        private IButtonControl cancelButton;
        private bool controlBox;
        private Icon icon;
        private bool maximizeBox = true;
        private Size maximumSize = Size.Empty;
        private bool minimizeBox = true;
        private Size minimumSize = Size.Empty;
        private double opacity = 1.0;
        private bool showIcon = true;        

        #endregion //Members

        #region Constructor

        internal RowEditTemplateDialogSettings()
            : base()
        {            
        }
        #endregion //Constructor

        #region Properties

        #region AcceptButton

        /// <summary>
        /// The accept button of the dialog.  If this is set, the button is 'clicked' whenever the user
        /// presses the 'ENTER' key.
        /// </summary>
        [DefaultValue(null)]
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_AcceptButton")]
        public IButtonControl AcceptButton
        {
            get { return this.acceptButton; }
            set
            {
                if (this.acceptButton == value)
                    return;

                this.acceptButton = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.AcceptButton);
            }
        }
        #endregion //AcceptButton

        #region AutoScaleMode

        /// <summary>
        /// Determines how the dialog will scale when the screen resolution of fonts change.
        /// </summary>
        [DefaultValue(AutoScaleMode.Font)]
        [LocalizedCategory("LC_Layout")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_AutoScaleMode")]
        public AutoScaleMode AutoScaleMode
        {
            get { return this.autoScaleMode; }
            set
            {
                if (this.autoScaleMode == value)
                    return;

                this.autoScaleMode = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.AutoScaleMode);
            }
        }
        #endregion //AutoScaleMode

        #region CancelButton

        /// <summary>
        /// The cancel button of the dialog.  If this property is set, the button is 'clicked' whenever
        /// the user presses the 'ESC' key.
        /// </summary>
        [DefaultValue(null)]
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_CancelButton")]
        public IButtonControl CancelButton
        {
            get { return this.cancelButton; }
            set
            {
                if (this.cancelButton == value)
                    return;

                this.cancelButton = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.CancelButton);
            }
        }
        #endregion //CancelButton

        #region Caption

        /// <summary>
        /// Gets or sets the caption of the dialog.
        /// </summary>
        [DefaultValue(null)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_Caption")]
        public string Caption
        {
            get { return this.caption; }
            set
            {
                if (this.caption == value)
                    return;

                this.caption = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.Caption);
            }
        }
        #endregion //Caption

        #region ControlBox

        /// <summary>
        /// Determines whether the dialog has a Control/System menu box.
        /// </summary>
        [DefaultValue(false)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_ControlBox")]
        public bool ControlBox
        {
            get { return this.controlBox; }
            set
            {
                if (this.controlBox == value)
                    return;

                this.controlBox = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.ControlBox);
            }
        }
        #endregion //ControlBox

        #region FormBorderStyle

        /// <summary>
        /// Gets or sets the border style of the dialog.
        /// </summary>
        [DefaultValue(DEFAULT_FORMBORDERSTYLE)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_FormBorderStyle")]
        public FormBorderStyle FormBorderStyle
        {
            get { return this.formBorderStyle; }
            set
            {
                if (this.formBorderStyle == value)
                    return;

                this.formBorderStyle = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.FormBorderStyle);
            }
        }
        #endregion //FormBorderStyle

        #region Icon

        /// <summary>
        /// Indicates the icon for the dialog.  This icon is displayed in the dialog's system menu box and when the dialog is minimized.
        /// </summary>
        [DefaultValue(null)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_Icon")]
        public Icon Icon
        {
            get { return this.icon; }
            set
            {
                if (this.icon == value)
                    return;

                this.icon = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.Icon);
            }
        }
        #endregion //Icon

        #region MaximizeBox

        /// <summary>
        /// Determines whether the dialog has a maximize box in the upper-right corner of its caption bar.
        /// </summary>
        [DefaultValue(true)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_MaximizeBox")]
        public bool MaximizeBox
        {
            get { return this.maximizeBox; }
            set
            {
                if (this.maximizeBox == value)
                    return;

                this.maximizeBox = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.MaximizeBox);
            }
        }
        #endregion //MaximizeBox

        #region MaximumSize

        /// <summary>
        /// The maximum size the dialog can be resized to.
        /// </summary>        
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_MaximumSize")]
        public Size MaximumSize
        {
            get { return this.maximumSize; }
            set
            {
                if (this.maximumSize == value)
                    return;

                this.maximumSize = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.MaximumSize);
            }
        }

        /// <summary>
        /// Returns whether the property is set to a non-default value.
        /// </summary>
        /// <returns>True if the property is set to a non-default value.</returns>
        protected bool ShouldSerializeMaximumSize()
        {
            return this.maximumSize != Size.Empty;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetMaximumSize()
        {
            this.MaximumSize = Size.Empty;
        }
        #endregion //MaximumSize

        #region MinimizeBox

        /// <summary>
        /// Determines whether the dialog has a minimize box in the upper-right corner of its caption bar.
        /// </summary>
        [DefaultValue(true)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_MinimizeBox")]
        public bool MinimizeBox
        {
            get { return this.minimizeBox; }
            set
            {
                if (this.minimizeBox == value)
                    return;

                this.minimizeBox = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.MinimizeBox);
            }
        }
        #endregion //MinimizeBox

        #region MinimumSize

        /// <summary>
        /// The minimum size the dialog can be resized to.
        /// </summary>
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_MinimumSize")]
        public Size MinimumSize
        {
            get { return this.minimumSize; }
            set
            {
                if (this.minimumSize == value)
                    return;

                this.minimumSize = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.MinimumSize);
            }
        }

        /// <summary>
        /// Returns whether the property is set to a non-default value.
        /// </summary>
        /// <returns>True if the property is set to a non-default value.</returns>
        protected bool ShouldSerializeMinimumSize()
        {
            return this.minimumSize != Size.Empty;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetMinimumSize()
        {
            this.MinimumSize = Size.Empty;
        }
        #endregion //MinimumSize

        #region Opacity

        /// <summary>
        /// The opacity percentage of the control.
        /// </summary>
        [DefaultValue(1.0)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_Opacity")]
        [TypeConverter(typeof(OpacityConverter))]
        public double Opacity
        {
            get { return this.opacity; }
            set
            {
                if (this.opacity == value)
                    return;

                this.opacity = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.Opacity);
            }
        }
        #endregion //Opacity

        #region ShowIcon

        /// <summary>
        /// Indicates whether an icon is displayed in the title bar of the dialog.
        /// </summary>
        [DefaultValue(true)]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplateDialogSettings_P_ShowIcon")]
        public bool ShowIcon
        {
            get { return this.showIcon; }
            set
            {
                if (this.showIcon == value)
                    return;

                this.showIcon = value;
                this.NotifyPropChange(RowEditTemplateDialogSettingsPropertyIds.ShowIcon);
            }
        }
        #endregion //ShowIcon

        #endregion //Properties

        #region Base Class Overrides

        #region Tag

        /// <summary>
        /// Returns or sets an object value that is stored on the settings.
        /// </summary>
        /// <remarks>
        /// <p class="note"><b>Note:</b> The <b>Tag</b> property is not saved with the settings information.</p>
        /// </remarks>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override object Tag
        {
            get
            {
                return base.Tag;
            }
            set
            {
                base.Tag = value;
            }
        }
        #endregion //Tag

        #region ToString

        /// <summary>
        /// Returns an empty string.
        /// </summary>
        /// <returns>An empty string.</returns>
        public override string ToString()
        {
            return String.Empty;
        }
        #endregion //ToString

        #endregion //Base Class Overrides
    }
    #endregion //RowEditTemplateDialogSettings

    #region RowEditTemplatePropertyIds

    /// <summary>
    /// Uniquely identifies properties of the RowEditTemplateDialogSettings class.
    /// </summary>
    public enum RowEditTemplateDialogSettingsPropertyIds
    {
        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.AcceptButton"/> property.
        /// </summary>
        AcceptButton,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.AutoScaleMode"/> property.
        /// </summary>
        AutoScaleMode,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.CancelButton"/> property.
        /// </summary>
        CancelButton,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.ControlBox"/> property.
        /// </summary>
        ControlBox,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.Caption"/> property.
        /// </summary>
        Caption,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.FormBorderStyle"/> property.
        /// </summary>
        FormBorderStyle,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.Icon"/> property.
        /// </summary>
        Icon,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.MaximizeBox"/> property.
        /// </summary>
        MaximizeBox,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.MaximumSize"/> property.
        /// </summary>
        MaximumSize,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.MinimizeBox"/> property.
        /// </summary>
        MinimizeBox,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.MinimumSize"/> property.
        /// </summary>
        MinimumSize,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.Opacity"/> property.
        /// </summary>
        Opacity,

        /// <summary>
        /// Identifies the <see cref="RowEditTemplateDialogSettings.ShowIcon"/> property.
        /// </summary>
        ShowIcon,
    }
    #endregion //RowEditTemplatePropertyIds    

    #region RowEditTemplateUITypeEditor

    /// <summary>
    /// A UITypeEditor providing the selection of a template without the
    /// UI that is provided by the IListSource implementation.
    /// </summary>
    [System.Runtime.InteropServices.ComVisible(false)]
    
    
    public class RowEditTemplateUITypeEditor : Infragistics.Shared.UITypeEditorListBase
    {
        #region Members

        private Dictionary<string, object> nameInstanceMap = new Dictionary<string, object>();

        #endregion //Members

        #region Base Class Overrides
        
        #region GetListItems

        /// <summary>
        /// Returns the set of items to show on the list.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that can be used to gain additional context information.</param>
        /// <param name="provider">An IServiceProvider that this editor can use to obtain services.</param>
        /// <param name="value">The object to edit.</param>
        /// <param name="currentEntry"></param>
        /// <returns>An array of objects containing the list items.</returns>
        protected override object[] GetListItems(ITypeDescriptorContext context, IServiceProvider provider, object value, ref object currentEntry)
        {
            IDesignerHost host = provider.GetService(typeof(IDesignerHost)) as IDesignerHost;
            if (host != null && host.Container != null)
            {
                this.nameInstanceMap.Clear();

                ComponentCollection components = host.Container.Components;
                for (int i = 0; i < components.Count; i++)
                {
                    if (components[i] is UltraGridRowEditTemplate)
                    {
                        string name = ((UltraGridRowEditTemplate)components[i]).Name;
                        this.nameInstanceMap.Add(name, components[i]);
                    }
                }
            }

            List<string> availableTemplates = new List<string>(this.nameInstanceMap.Keys.Count);
            foreach (string key in this.nameInstanceMap.Keys)
                availableTemplates.Add(key);

            return availableTemplates.ToArray();
        }
        #endregion //GetListItems

        #region ValidateEditItem

        /// <summary>
        /// Validates an item selected by the user on the list and returns it.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that can be used to gain additional context information.</param>
        /// <param name="provider">An IServiceProvider that this editor can use to obtain services.</param>
        /// <param name="value">The object to edit.</param>
        /// <param name="selectedEntry"></param>
        /// <returns>An array of objects containing the list items.</returns>
        protected override object ValidateEditItem(ITypeDescriptorContext context, IServiceProvider provider, object value, object selectedEntry)
        {
            string selectedString = selectedEntry.ToString();
            if (this.nameInstanceMap.ContainsKey(selectedString))
                return this.nameInstanceMap[selectedString];

            return selectedEntry;
        }
        #endregion //ValidateEditItem

        #endregion //Base Class Overrides
    }
    #endregion //RowEditTemplateUITypeEditor

    #region RowEditTemplateControlUIElement

    /// <summary>
    /// The main UIElement for the <see cref="UltraGridRowEditTemplate"/> control.
    /// </summary>
    public class RowEditTemplateControlUIElement : ControlUIElementBase
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the element.
        /// </summary>
        /// <param name="template">The template associated with the element.</param>
        public RowEditTemplateControlUIElement(UltraGridRowEditTemplate template)
            : base(template, (IUltraControl)template)
        {
        }
        #endregion //Constructor

        #region Base Class Overrides

        #region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps)
        {
            ((UltraGridRowEditTemplate)this.Control).ResolveAppearance(ref appearance, ref requestedProps);

            base.InitAppearance(ref appearance, ref requestedProps);
        }
        #endregion //InitAppearance

        #region UIRole

        /// <summary>
        /// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
        /// </summary>
        public override UIRole UIRole
        {
            get
            {
                return StyleUtils.GetRole((UltraGridRowEditTemplate)this.Control, StyleUtils.TemplateRole.RowEditTemplatePanel);
            }
        }
        #endregion //UIRole

        #endregion //Base Class Overrides
    }
    #endregion //RowEditTemplateControlUIElement

    #region UltraGridRowEditTemplate

    /// <summary>
    /// The RowEditTemplate is provided as a means of editing a row in the grid using a customizable interface.
    /// </summary>
    /// <remarks>
    /// <p class="note">
    /// <b>Note: </b>A single instance of a RowEditTemplate cannot be associated with multiple <see cref="UltraGridBand"/>s.
    /// </p>
    /// <p class="note">
    /// <b>Note: </b>The associated <see cref="UltraGrid"/> cannot be edited while a RowEditTemplate is shown.
    /// </p>
    /// </remarks>
    /// <seealso cref="UltraGridBand.RowEditTemplate"/>
    /// <seealso cref="UltraGridRow.RowEditTemplate"/>  
    [ToolboxBitmap(typeof(UltraGridRowEditTemplate), AssemblyVersion.ToolBoxBitmapFolder + "UltraGridRowEditTemplate.bmp")]
        
    [ToolboxItem(true)]    
    
    [Designer("Infragistics.Win.UltraWinGrid.Design.UltraGridRowEditTemplateDesigner, " + AssemblyRef.Design)]
    // MBS 6/24/08 - BR34194
    [LocalizedDescription("LD_UltraGridRowEditTemplate")]
    public class UltraGridRowEditTemplate : IGPanel, 
        Infragistics.Shared.IUltraLicensedComponent,
        IListSource,
        ISupportInitialize,
        IUltraControl,
        IUltraControlElement,
        ISupportAppStyling
    {        
        #region ColumnPropertyDescriptor - Private Class

        private sealed class ColumnPropertyDescriptor : PropertyDescriptor
        {
            #region Members

            private string columnKey;
            private Type columnType;
            private UltraGridRowEditTemplate template;
            private UltraGridBand band;

            #endregion //Members

            #region Constructor

            internal ColumnPropertyDescriptor(UltraGridRowEditTemplate template, UltraGridBand band, string columnKey, Type columnType, Attribute[] attrs)
                : base(columnKey, attrs)
            {
                this.band = band;
                this.columnKey = columnKey;
                this.columnType = columnType;
                this.template = template;
            }

            internal ColumnPropertyDescriptor(UltraGridRowEditTemplate template, string columnKey, Type columnType, Attribute[] attrs)
                : this(template, null, columnKey, columnType, attrs)
            {
            }
            #endregion //Constructor

            #region Properties

            #region Column

            private UltraGridColumn Column
            {
                get { return this.band != null ? this.band.Columns[this.columnKey] : null; }
            }
            #endregion //Column

            #region Row

            private UltraGridRow Row
            {
                get
                {
                    if (this.band == null)
                        return null;

                    UltraGridRow row = this.template.Row;
                    while (row != null && row.Band != this.band)
                        row = row.ParentRow;

                    return row;
                }
            }
            #endregion //Row

            #endregion //Properties

            #region Base Class Overrides

            #region CanResetValue

            public override bool CanResetValue(object component)
            {
                return false;
            }
            #endregion //CanResetValue

            #region ComponentType

            public override Type ComponentType
            {
                get { return this.columnType; }
            }
            #endregion //ComponentType

            #region GetValue

            public override object GetValue(object component)
            {
                return this.Row != null ? this.Row.GetCellValue(this.band.Columns[this.columnKey]) : null;
            }
            #endregion //GetValue

            #region IsReadOnly

            public override bool IsReadOnly
            {
                get 
                { 
                    return this.Column == null || this.Column.IsReadOnly || 
                           this.band == null || this.band != this.template.Band; 
                }
            }
            #endregion //IsReadOnly

            #region PropertyType

            public override Type PropertyType
            {
                get { return this.columnType; }
            }
            #endregion //PropertyType

            #region ResetValue

            public override void ResetValue(object component)
            {
            }
            #endregion //ResetValue

            #region SetValue

            public override void SetValue(object component, object value)
            {
                if(this.Row != null)
                    this.Row.Cells[this.columnKey].Value = value;
            }
            #endregion //SetValue

            #region ShouldSerializeValue

            public override bool ShouldSerializeValue(object component)
            {
                return false;
            }
            #endregion //ShouldSerializeValue            

            #endregion //Base Class Overrides
        }
        #endregion //ColumnPropertyDescriptor - Private Class

        #region ColumnDescriptorData - Nested Class

        /// <summary>
        /// For Infragistics infrastructure purposes only.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [TypeConverter(typeof(ColumnDescriptorData.ColumnDescriptorDataTypeConverter))]
        public class ColumnDescriptorData
        {
            #region Members

            private Type columnType;
            private string columnKey;
            private string bandKey;
            private int bandHierarchicalLevel = -1;

            #endregion //Members

            #region Constructor

            /// <summary>
            /// Initializes a new instance of the class.
            /// </summary>
            /// <param name="columnKey">The key of the associated column.</param>
            /// <param name="columnType">The type of the associated column.</param>
            /// <param name="bandHierarchicalLevel">The HierarchicalLevel of the band</param>
            /// <param name="bandKey">The key of the band.</param>
            public ColumnDescriptorData(string columnKey, Type columnType, string bandKey, int bandHierarchicalLevel)
            {
                this.columnKey = columnKey;
                this.columnType = columnType;
                this.bandKey = bandKey;
                this.bandHierarchicalLevel = bandHierarchicalLevel;
            }
            #endregion //Constructor

            #region Properties

            #region BandHierarchicalLevel

            /// <summary>
            /// Gets the HierarchicalLevel of the associated band.
            /// </summary>
            public int BandHierarchicalLevel
            {
                get { return this.bandHierarchicalLevel; }
            }
            #endregion //BandHierarchicalLevel

            #region BandKey

            /// <summary>
            /// Gets the key of the associated band.
            /// </summary>
            public string BandKey
            {
                get { return this.bandKey; }
            }
            #endregion //BandKey

            #region Key

            /// <summary>
            /// Gets the key of the associated column.
            /// </summary>
            public string Key
            {
                get { return this.columnKey; }                
            }
            #endregion //Key

            #region Type

            /// <summary>
            /// Gets the type of the associated column.
            /// </summary>
            public Type Type
            {
                get { return this.columnType; }
            }
            #endregion //Type

            #endregion //Properties

            #region ColumnDescriptorDataTypeConverter

            /// <summary>
            /// The type converter for the ColumnDescriptorData class.
            /// </summary>
            [EditorBrowsable(EditorBrowsableState.Never)]
            public class ColumnDescriptorDataTypeConverter : TypeConverter
            {
                /// <summary>
                /// Returns whether this converter can convert the object to the specified type, using the specified context.
                /// </summary>
                /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
                /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
                /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
                public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
                {
                    if (destinationType == typeof(InstanceDescriptor))
                        return true;

                    return base.CanConvertTo(context, destinationType);
                }

                /// <summary>
                /// Converts the given value object to the specified type, using the specified
                /// context and culture information.
                /// </summary>
                /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
                /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
                /// <param name="value">The System.Object to convert.</param>
                /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
                /// <returns>An System.Object that represents the converted value.</returns>
                public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
                {
                    if (destinationType == typeof(InstanceDescriptor))
                    {
                        ColumnDescriptorData data = value as ColumnDescriptorData;
                        if (data != null)
                        {
                            ConstructorInfo ctor = typeof(ColumnDescriptorData).GetConstructor(new Type[] { typeof(string), typeof(Type), typeof(string), typeof(int) });
                            if (ctor != null)
                            {
                                return new InstanceDescriptor(ctor, new object[] { data.Key, data.Type, data.BandKey, data.BandHierarchicalLevel }, true);
                            }
                        }
                    }
                    return base.ConvertTo(context, culture, value, destinationType);
                }
            }
            #endregion //ColumnDescriptorDataTypeConverter
        }     
        #endregion //ColumnDescriptorData - Nested Class

        #region ColumnDescriptorDataCollection - Nested Class

        /// <summary>
        /// For Infragistics infrastructure purposes only.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [TypeConverter(typeof(ColumnDescriptorDataCollectionTypeConverter))]
        public class ColumnDescriptorDataCollection : List<ColumnDescriptorData>
        {
            #region Constructor

            /// <summary>
            /// Initializes a new instance of the class.
            /// </summary>
            public ColumnDescriptorDataCollection() : base() { }

            /// <summary>
            /// Initializes a new instance of the class.
            /// </summary>
            /// <param name="columns">An array of the columns to initialize with.</param>
            public ColumnDescriptorDataCollection(ColumnDescriptorData[] columns)
                : base(columns)
            {
            }
            #endregion //Constructor

            #region ColumnDescriptorDataCollectionTypeConverter

            /// <summary>
            /// The TypeConverter for ColumnDescriptorDataCollection
            /// </summary>
            [EditorBrowsable(EditorBrowsableState.Never)]
            public class ColumnDescriptorDataCollectionTypeConverter : TypeConverter
            {
                /// <summary>
                /// Returns whether this converter can convert the object to the specified type, using the specified context.
                /// </summary>
                /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
                /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
                /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
                public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
                {
                    if (destinationType == typeof(InstanceDescriptor))
                        return true;

                    return base.CanConvertTo(context, destinationType);
                }

                /// <summary>
                /// Converts the given value object to the specified type, using the specified
                /// context and culture information.
                /// </summary>
                /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
                /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
                /// <param name="value">The System.Object to convert.</param>
                /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
                /// <returns>An System.Object that represents the converted value.</returns>
                public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
                {
                    if (destinationType == typeof(InstanceDescriptor))
                    {
                        ColumnDescriptorDataCollection collection = value as ColumnDescriptorDataCollection;
                        if (collection != null)
                        {
                            ConstructorInfo ctor = typeof(ColumnDescriptorDataCollection).GetConstructor(new Type[] { typeof(IEnumerable[]) });
                            if (ctor != null)
                                return new InstanceDescriptor(ctor, new object[] { collection.ToArray() }, true);
                        }
                    }

                    return base.ConvertTo(context, culture, value, destinationType);
                }
            }
            #endregion //ColumnDescriptorDataCollectionTypeConverter
        }
        #endregion //ColumnDescriptorDataCollection

        #region TemplateBandPropertyCollection

        private class TemplateBandPropertyCollection : BindingList<PropertyDescriptor>, ITypedList
        {
            #region Members

            private UltraGridRowEditTemplate template;
            private UltraGridBand band;            
            private string bandKey;
            private int bandHierarchicalLevel;
            private int colDataIndex;

            #endregion //Members

            #region Constructor

            public TemplateBandPropertyCollection(UltraGridRowEditTemplate template, UltraGridBand band)
            {
                this.band = band;
                this.template = template;

                
                //
                // Create a dummy item since otherwise the BindingManager is going to try to determine the properties
                // of the list contents by adding an item to the list, but PropertyDescriptor doesn't have a blank constructor.
                // Adding our own item will let the BindingManager see the properties, which won't matter for our purposes
                // anyway because ITypedList.GetItemProperties is what's used for the DataBindings list.
                ColumnPropertyDescriptor colProp = new ColumnPropertyDescriptor(template, "DummyRow", typeof(object), null);
                this.Add(colProp);
            }

            public TemplateBandPropertyCollection(UltraGridRowEditTemplate template, string bandKey, int bandHierarchicalLevel, int colDataIndex)
            {
                this.bandKey = bandKey;
                this.bandHierarchicalLevel = bandHierarchicalLevel;
                this.colDataIndex = colDataIndex;
                this.template = template;                

                
                //
                // Create a dummy item since otherwise the BindingManager is going to try to determine the properties
                // of the list contents by adding an item to the list, but PropertyDescriptor doesn't have a blank constructor.
                // Adding our own item will let the BindingManager see the properties, which won't matter for our purposes
                // anyway because ITypedList.GetItemProperties is what's used for the DataBindings list.
                ColumnPropertyDescriptor colProp = new ColumnPropertyDescriptor(template, "DummyRow", typeof(object), null);
                this.Add(colProp);
            }
            #endregion //Constructor

            #region Properties

            #region Band

            internal UltraGridBand Band
            {
                get
                {
                    if (this.band != null)
                        return this.band;

                    if (this.bandKey != null && this.bandHierarchicalLevel > -1)
                    {
                        if (this.template.Grid != null && this.template.Grid.Initializing == false)
                        {
                            foreach (UltraGridBand band in this.template.Grid.DisplayLayout.SortedBands)
                            {
                                if (band.Key == this.bandKey && band.HierarchicalLevel == this.bandHierarchicalLevel)
                                {
                                    this.band = band;
                                    return this.band;
                                }
                            }
                        }
                    }
                    return null;
                }
            }
            #endregion //Band

            #endregion //Properties

            #region ITypedList Members

            #region GetItemProperties

            PropertyDescriptorCollection ITypedList.GetItemProperties(PropertyDescriptor[] listAccessors)
            {
                bool hasListAccessor = false;
                UltraGridBand band = null;

                // Request the Band property to try to get the actual band from the grid instead of having
                // to just use the band key.  This is because we might start off with the list created
                // by the deserialization process, which itself is only aware of the band keys since the
                // template can't access the actual bands yet.  However, once the designer is done loading,
                // we will be able to access these bands, so rather than iterating through every currency manager
                // in the template's BindingContext, we'll just have the collection be able to resolve the
                // band when it is able.
                //
                // NOTE: There seems to be a .NET bug where, when a control is bound to a property on a sub-list,
                // it will not call GetItemProperties on a sibling node (i.e. if a control is bound to a column 
                // in a band other than the template's band, a child band of that column will not have an expansion
                // indicator unless you collapse and re-expand the parent node; this issue is reproducible with
                // a DataSet too).
                if(this.Band != null)
                    // Don't try to access the ParentBands collection if the band reference is null,
                    // since this means that we'll be using the deserialized ColumnDescriptorData instead.
                    // Acessing the collection will cause the deserialized data to be cleared
                    band = this.template.ParentBands.Count == 0 ? this.band : this.template.ParentBands[0];

                string bandKey = this.bandKey;
                int bandHierarchicalLevel = this.bandHierarchicalLevel;
                int colDataIndex = this.colDataIndex;

                List<PropertyDescriptor> list = new List<PropertyDescriptor>();
                if (null != listAccessors && listAccessors.Length > 0)
                {
                    TemplateBandPropertyDescriptor bandProp = listAccessors[listAccessors.Length - 1] as TemplateBandPropertyDescriptor;
                    if (bandProp != null)
                    {
                        hasListAccessor = true;
                        band = bandProp.Band;
                        if (band == null)
                        {
                            bandKey = bandProp.BandKey;
                            bandHierarchicalLevel = bandProp.BandHierarchicalLevel;
                            colDataIndex = bandProp.ColDataIndex;
                        }
                    }
                }

                if (!hasListAccessor)
                {
                    // Check the band member to know if we're dealing with deserialization (ColumnDescriptorData)
                    if (this.band != null && this.template.Band != null)
                    {
                        for (int i = 0; i < this.template.Band.Columns.Count; i++)
                        {
                            UltraGridColumn column = this.template.Band.Columns[i];
                            if (column.IsChaptered)
                                continue;

                            ColumnPropertyDescriptor colProp = new ColumnPropertyDescriptor(this.template, this.template.Band, column.Key, column.DataType, null);
                            list.Add(colProp);
                        }

                        
                        if (this.template.ParentBands.Count > 0)
                        {
                            TemplateBandPropertyDescriptor bandProp = new TemplateBandPropertyDescriptor(this.template.ParentBands[0], this.template);
                            list.Add(bandProp);
                        }
                    }
                    // If the band is null then we're dealing with the codedom deserializer when it's hooking up the 
                    // DataBindings of a control, so we need to create these temporary descriptors
                    else if(this.bandKey != null)
                    {
                        // We rely on the column data being ordered as it appears in the DataBindings designer
                        // (i.e. template columns, then a hierarchical list starting with the root band)
                        for (int i = 0; i < template.ColumnData.Count; i++)
                        {
                            ColumnDescriptorData colData = template.ColumnData[i];
                            if (colData.BandHierarchicalLevel == template.BandHierarchyLevel && colData.BandKey == template.BandKey)
                            {
                                ColumnPropertyDescriptor colProp = new ColumnPropertyDescriptor(this.template, colData.Key, colData.Type, null);
                                list.Add(colProp);
                            }
                            else
                            {
                                TemplateBandPropertyDescriptor bandProp = new TemplateBandPropertyDescriptor(colData.BandKey, colData.BandHierarchicalLevel, i, template);
                                list.Add(bandProp);
                                break;
                            }
                        }
                    }
                    // MBS 5/23/08 
                    // If the template has been unhooked from a band for whatever reason but we still have column data serialized,
                    // try to read that in so that the designer doesn't blow up.  Databinding still will not do anything, though
                    else if (template.ColumnData.Count > 0)
                    {
                        ColumnDescriptorData colData = template.ColumnData[0];
                        string baseBandName = colData.BandKey;
                        int baseBandLevel = colData.BandHierarchicalLevel;

                        // We rely on the column data being ordered as it appears in the DataBindings designer
                        // (i.e. template columns, then a hierarchical list starting with the root band)
                        for (int i = 0; i < template.ColumnData.Count; i++)
                        {
                            colData = template.ColumnData[i];
                            if (colData.BandKey == baseBandName && colData.BandHierarchicalLevel == baseBandLevel)
                            {
                                // Columns of parent bands are read-only through binding
                                ReadOnlyAttribute attr = new ReadOnlyAttribute(true);
                                ColumnPropertyDescriptor colProp = new ColumnPropertyDescriptor(this.template, colData.Key, colData.Type, new Attribute[] { attr });
                                list.Add(colProp);
                            }
                            else
                            {
                                TemplateBandPropertyDescriptor bandProp = new TemplateBandPropertyDescriptor(colData.BandKey, colData.BandHierarchicalLevel, i, this.template);
                                list.Add(bandProp);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    // BR32576: Try to get the band from the list of parent bands if we don't have it from the list accessors                    
                    if (band == null)
                    {
                        // Don't force the parent bands to get created at this point
                        if (this.template.parentBands != null)
                        {
                            foreach (UltraGridBand parentBand in this.template.parentBands)
                            {
                                if (parentBand.Key == bandKey && parentBand.HierarchicalLevel == bandHierarchicalLevel)
                                {
                                    band = parentBand;
                                    break;
                                }
                            }
                        }
                    }
                    
                    if (band != null)
                    {
                        for (int i = 0; i < band.Columns.Count; i++)
                        {
                            UltraGridColumn column = band.Columns[i];
                            if (column.IsChaptered)
                                continue;

                            // Columns of parent bands are read-only through binding
                            ReadOnlyAttribute attr = new ReadOnlyAttribute(true);
                            ColumnPropertyDescriptor colProp = new ColumnPropertyDescriptor(this.template, band, column.Key, column.DataType, new Attribute[] { attr });
                            list.Add(colProp);
                        }

                        int bandIndex = this.template.ParentBands.IndexOf(band);
                        if (bandIndex > -1 && bandIndex < this.template.ParentBands.Count - 1)
                        {
                            TemplateBandPropertyDescriptor bandProp = new TemplateBandPropertyDescriptor(this.template.ParentBands[bandIndex + 1], this.template);
                            list.Add(bandProp);
                        }
                    }
                    // If we still couldn't get to the band at this point, then we have to fallback to our 
                    // serialized information
                    else if (bandKey != null)
                    {
                        // We rely on the column data being ordered as it appears in the DataBindings designer
                        // (i.e. template columns, then a hierarchical list starting with the root band)
                        for (int i = colDataIndex; i < template.ColumnData.Count; i++)
                        {
                            ColumnDescriptorData colData = template.ColumnData[i];
                            if (colData.BandKey == bandKey && colData.BandHierarchicalLevel == bandHierarchicalLevel)
                            {
                                // Columns of parent bands are read-only through binding
                                ReadOnlyAttribute attr = new ReadOnlyAttribute(true);
                                ColumnPropertyDescriptor colProp = new ColumnPropertyDescriptor(this.template, colData.Key, colData.Type, new Attribute[] { attr });
                                list.Add(colProp);
                            }
                            else
                            {
                                TemplateBandPropertyDescriptor bandProp = new TemplateBandPropertyDescriptor(colData.BandKey, colData.BandHierarchicalLevel, i, this.template);
                                list.Add(bandProp);
                                break;
                            }
                        }
                    }
                }

                return new PropertyDescriptorCollection(list.ToArray());
            }
            #endregion //GetItemProperties

            #region GetListName

            string ITypedList.GetListName(PropertyDescriptor[] listAccessors)
            {
                if (this.bandKey != null)
                    return this.bandKey;

                return this.band != null && this.band.Key != null ? this.band.Key : String.Empty;
            }
            #endregion //GetListName

            #endregion            
        }

        #endregion //BandPropertyCollection

        #region TemplateBandPropertyDescriptor

        private class TemplateBandPropertyDescriptor : PropertyDescriptor
        {
            #region Members

            private UltraGridBand band;
            private UltraGridRowEditTemplate template;
            private TemplateBandPropertyCollection properties;
            private string bandKey;
            private int bandHierarchicalLevel;
            private int colDataIndex;

            #endregion //Members

            #region Constructor

            public TemplateBandPropertyDescriptor(UltraGridBand band, UltraGridRowEditTemplate template)
                : base(band.Key, null)
            {
                this.band = band;
                this.template = template;
            }

            public TemplateBandPropertyDescriptor(string bandKey, int bandHierarchicalLevel, int colDataIndex, UltraGridRowEditTemplate template)
                : base(bandKey, null)
            {
                this.bandKey = bandKey;
                this.bandHierarchicalLevel = bandHierarchicalLevel;
                this.colDataIndex = colDataIndex;
                this.template = template;
            }
            #endregion //Constructor

            #region Properties

            #region Band

            internal UltraGridBand Band
            {
                get { return this.band; }
            }
            #endregion //Band

            #region BandKey

            internal string BandKey
            {
                get
                {
                    return this.bandKey;
                }
            }
            #endregion //BandKey

            #region BandHierarchicalLevel

            internal int BandHierarchicalLevel
            {
                get { return this.bandHierarchicalLevel; }
            }
            #endregion //BandHierarchicalLevel

            #region ColDataIndex

            internal int ColDataIndex
            {
                get { return this.colDataIndex; }
            }
            #endregion //ColDataIndex

            #endregion //Properties

            #region Base Class Overrides

            #region CanResetValue

            public override bool CanResetValue(object component)
            {
                return false;
            }
            #endregion //CanResetValue

            #region ComponentType

            public override Type ComponentType
            {
                get { return typeof(UltraGridRowEditTemplate); }
            }
            #endregion //ComponentType

            #region GetValue

            public override object GetValue(object component)
            {
                if (this.properties == null)
                {
                    if (this.Band != null)
                        this.properties = new TemplateBandPropertyCollection(this.template, this.band);
                    else
                        // If the band is null then we're dealing with the codedom deserializer when it's hooking up the 
                        // DataBindings of a control, so we need to create these temporary descriptors
                        this.properties = new TemplateBandPropertyCollection(this.template, this.bandKey, this.bandHierarchicalLevel, this.colDataIndex);
                }
                return this.properties;
            }
            #endregion //GetValue

            #region IsReadOnly

            public override bool IsReadOnly
            {
                get { throw new NotImplementedException(); }
            }
            #endregion //IsReadOnly

            #region PropertyType

            public override Type PropertyType
            {
                get { return (typeof(TemplateBandPropertyCollection)); }
            }
            #endregion //PropertyType

            #region ResetValue

            public override void ResetValue(object component)
            {
            }
            #endregion //ResetValue

            #region SetValue

            public override void SetValue(object component, object value)
            {
                throw new NotSupportedException();
            }
            #endregion //SetValue

            #region ShouldSerializeValue

            public override bool ShouldSerializeValue(object component)
            {
                return true;
            }
            #endregion //ShouldSerializeValue

            #endregion //Base Class Overrides
        }
        #endregion //TemplateBandPropertyDescriptor

        #region TemplateForm - Private Class

        private class TemplateForm : Form
        {
            #region Members

            private UltraGridRowEditTemplate template;

            #endregion //Members

            #region Constructor

            public TemplateForm(UltraGridRowEditTemplate template)
                : base()
            {
                this.template = template;
            }
            #endregion //Constructor

            #region Base Class Overrides

            #region OnFormClosed

            protected override void OnFormClosed(FormClosedEventArgs e)
            {
                // MBS 5/29/08 - BR33328
                // We need to do this in the OnFormClosed with a Modeless template, since
                // a user could cancel the closing of the owning form, which would cause
                // this form to get an OnFormClosing, but there's no way for us to tell
                // if the owning form's FormClosing event has been cancelled.  Note that we have
                // to do this *before* calling the base, otherwise the template will be disposed.
                if (this.template.DisplayMode == RowEditTemplateDisplayMode.Modeless)
                    this.template.ReparentTemplate();

                base.OnFormClosed(e);

                this.template.OnFormClosed();
            }
            #endregion //OnFormClosed

            #region OnFormClosing

            protected override void OnFormClosing(FormClosingEventArgs e)
            {
                base.OnFormClosing(e);

                // If the event was cancelled, we shouldn't try to process our own logic
                if (e.Cancel)
                    return;

                BeforeRowEditTemplateClosedEventArgs args = new BeforeRowEditTemplateClosedEventArgs(this.template, false);
                this.template.OnFormClosing(args);
                if (args.Cancel)
                    e.Cancel = true;
                else if (this.template.DisplayMode == RowEditTemplateDisplayMode.Modal)
                    // When we're showing a modal form, we won't actually get to the FormClosed
                    // event until after we've left the Close() method, so we need to update
                    // this flag here so that the Close method can return successfully.
                    this.template.isFormShown = false;

                // MBS 5/29/08 - BR33328
                // Instead of doing this here, do it in the OnFormClosed override
                //else
                //    // We want to reparent the template at this point, since otherwise 
                //    this.template.ReparentTemplate();
            }
            #endregion //OnFormClosing

            #region OnShown

            protected override void OnShown(EventArgs e)
            {
                if (this.template != null)
                    this.template.OnFormShown();
                else
                    Debug.Fail("We somehow decided to show a form with no template");

                base.OnShown(e);
            }
            #endregion //OnShown

            #endregion //Base Class Overrides
        }
        #endregion //TemplateForm - Private Class

        #region Private Members

        private UltraLicense license;
        private UltraGridRow row;
        private string bandKey;
        private UltraGrid grid;
        private EmbeddableEditorBase lastHookedEditor;
        private RowEditTemplateRowChangedEventHandler rowChangedDelegate;
        private bool isInClose;
        private RowEditTemplateDisplayMode displayMode = RowEditTemplateDisplayMode.Modal;
        private TemplateForm form;
        private bool isFormShown;
        private RowEditTemplateDialogSettings dialogSettings;
        private Control originalParent;
        private DockStyle originalDockStyle;
        private Size originalSize;
        private UltraGridRow designRow;        
        private BindingContext bindingContext;        
        private ColumnDescriptorDataCollection columnData;
        private bool isInitializing;
        private Dictionary<string, UltraGridCellProxy> columnProxyMap;
        internal UltraGridCell activeCellOnDisplay;
        private int bandHierarchicalLevel = -1;
        private UltraGridBand cachedBand;
        private List<UltraGridBand> parentBands;
        private TemplateBandPropertyCollection templateDescriptorList;        
        private bool isDisplayingModalForm;
        private bool hookedIntoComponentChangeService;

        // MBS 5/19/08 - BR33101
        // We don't need these now since we're setting defaults through the designer
        //private bool shouldSkipSetDefaultVisible;
        //private bool shouldSkipDefaultAutoScroll;

        private bool isInCommitChanges;
        private bool hasBindingError;
        private AppearanceHolder appearanceHolder;
        private IUIElementDrawFilter drawFilter;
        private IUIElementCreationFilter creationFilter;
        private IUIElementCursorFilter cursorFilter;
        private RowEditTemplateControlUIElement controlElement;
        private ComponentRole componentRole;
        private Color backColorInternal;
        private bool isClosing;
        private TemplateDisplaySource templateDisplaySource;

        private const int DEFAULT_CONTROL_HEIGHT = 155;
        private const int DEFAULT_CONTROL_WIDTH = 300;

        #endregion //Private Members

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public UltraGridRowEditTemplate()
        {
            try
            {
                this.license = LicenseManager.Validate(typeof(UltraGridRowEditTemplate), this) as UltraLicense;
            }
            catch (System.IO.FileNotFoundException) { }           
        }
        #endregion //Constructor

        #region About Dialog and Licensing Interface

        /// <summary>
        /// Display the about dialog
        /// </summary>
        [DesignOnly(true)]
        [LocalizedDescription("LD_UltraGrid_P_About")]
        [LocalizedCategory("LC_Design")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [ParenthesizePropertyName(true)]
        [Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public object About { get { return null; } }

        UltraLicense IUltraLicensedComponent.License { get { return this.license; } }

        #endregion

        #region Private/Internal Properties

        #region ColumnProxyMap

        internal Dictionary<string, UltraGridCellProxy> ColumnProxyMap
        {
            get
            {
                if (this.columnProxyMap == null)
                    this.columnProxyMap = new Dictionary<string, UltraGridCellProxy>();

                return this.columnProxyMap;
            }
        }
        #endregion //ColumnProxyMap

        #region DesignRow

        private UltraGridRow DesignRow
        {
            get
            {
                if (this.DesignMode)
                {
                    if (this.designRow == null && this.Band != null)
                    {
                        IEnumerator enumerator = this.Grid.Rows.GetRowEnumerator(GridRowType.DataRow, this.Band, null).GetEnumerator();
                        if (enumerator != null)
                        {
                            enumerator.MoveNext();
                            UltraGridRow row = enumerator.Current as UltraGridRow;
                            if (row == null || row.Cells.Count == 0)
                                // There are some cases where we could have originally had a row at design-time
                                // but now that band's columns have been removed, so we don't want to present
                                // the designer to the user.
                                return null;

                            this.designRow = row;
                        }
                    }
                    return this.designRow;
                }
                return null;
            }
            set
            {
                if (this.DesignMode == false || this.designRow == value)
                    return;

                this.designRow = value;
            }
        }
        #endregion //DesignRow

        #region Form

        private Form Form
        {
            get
            {
                if (this.form == null || form.IsDisposed)
                {
                    this.form = new TemplateForm(this);
                    this.form.AcceptButton = this.DialogSettings.AcceptButton;
                    this.form.AutoScaleMode = this.DialogSettings.AutoScaleMode;
                    this.form.CancelButton = this.DialogSettings.CancelButton;
                    this.form.ControlBox = this.DialogSettings.ControlBox;
                    this.form.FormBorderStyle = this.DialogSettings.FormBorderStyle;
                    this.form.Icon = this.DialogSettings.Icon;
                    this.form.MaximizeBox = this.DialogSettings.MaximizeBox;
                    this.form.MaximumSize = this.DialogSettings.MaximumSize;
                    this.form.MinimizeBox = this.DialogSettings.MinimizeBox;
                    this.form.MinimumSize = this.DialogSettings.MinimumSize;
                    this.form.Opacity = this.DialogSettings.Opacity;
                    this.form.ShowIcon = this.DialogSettings.ShowIcon;
                    this.form.ShowInTaskbar = false;
                    this.form.StartPosition = FormStartPosition.Manual;
                    this.form.Text = this.DialogSettings.Caption;
                }
                return this.form;
            }
        }
        #endregion //Form        

        #region IsClosing

        internal bool IsClosing
        {
            get { return this.isClosing; }
        }
        #endregion //IsClosing

        #region ParentBands

        private List<UltraGridBand> ParentBands
        {
            get
            {
                if (this.parentBands == null && this.Grid != null && this.Grid.Initializing == false)
                {
                    this.parentBands = new List<UltraGridBand>();

                    UltraGridBand parentBand = this.Band;
                    while (parentBand != null && parentBand.ParentBand != null)
                    {                        
                        parentBand = parentBand.ParentBand;
                        this.parentBands.Insert(0, parentBand);
                    }

                    
                    // even if the columns haven't changed?
                    //
                    // Now that we've created the parent bands, initialize the column structure for DataBinding
                    this.InitializeColumnData();
                }
                return this.parentBands;
            }
        }
        #endregion //ParentBands

        #region TemplateDescriptorList

        private TemplateBandPropertyCollection TemplateDescriptorList
        {
            get
            {
                if (this.templateDescriptorList == null)
                {
                    if (this.Band == null)
                        // If the band is null then we're dealing with the codedom deserializer when it's hooking up the 
                        // DataBindings of a control.  The collection has the means of locating the band of the grid
                        // when it is available, so it will stop using the ColumnDescriptorData as appropriate
                        this.templateDescriptorList = new TemplateBandPropertyCollection(this, this.bandKey, this.bandHierarchicalLevel, 0);
                    else
                        this.templateDescriptorList = new TemplateBandPropertyCollection(this, this.Band);
                }

                return this.templateDescriptorList;
            }
        }
        #endregion //TemplateDescriptorList

        #endregion //Private/Internal Properties

        #region Protected/Public Properties

        #region ActiveProxy

        /// <summary>
        /// Gets the proxy on the template that is currently being used for editing, or null if the
        /// template isn't shown or no proxy is associated with the cell being edited.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public UltraGridCellProxy ActiveProxy
        {
            get
            {
                if (this.Row == null || this.Row.Layout == null || this.IsShown == false)
                    return null;

                return this.Row.Layout.ActiveProxy;
            }
        }
        #endregion //ActiveProxy

        #region Appearance

        /// <summary>
        /// Gets or sets the appearance of the template.
        /// </summary>
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridRowEditTemplate_P_Appearance")]
        public AppearanceBase Appearance
        {
            get
            {
                if (this.appearanceHolder == null)
                {
                    this.appearanceHolder = new AppearanceHolder();
                    this.appearanceHolder.SubObjectPropChanged += this.OnSubObjectPropChanged;
                }
                return this.appearanceHolder.Appearance;
            }
            set
            {
                if (this.appearanceHolder == null ||
                    !this.appearanceHolder.HasAppearance ||
                    value != this.appearanceHolder.Appearance)
                {
                    if (null == this.appearanceHolder)
                    {
                        this.appearanceHolder = new Infragistics.Win.AppearanceHolder();
                        this.appearanceHolder.SubObjectPropChanged += this.OnSubObjectPropChanged;
                    }

                    this.appearanceHolder.Appearance = value;
                    this.SyncBackColor();

                    if(this.IsHandleCreated)
                        this.Invalidate(true);

                    this.NotifyDesignEnvironmentOfChange();
                }
            }
        }

        /// <summary>
        /// Returns whether the Appearance property has been created.
        /// </summary>
        [Browsable(false)]
        public bool HasAppearance
        {
            get { return this.appearanceHolder != null && this.appearanceHolder.HasAppearance; }
        }

        /// <summary>
        /// Returns true if the Appearance is not the default
        /// </summary>
        /// <returns>True if the appearance is set to a non-default value.</returns>
        protected bool ShouldSerializeAppearance()
        {
            return this.HasAppearance && this.appearanceHolder.ShouldSerialize();
        }

        /// <summary>
        /// Resets the Appearance property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetAppearance()
        {
            if (this.HasAppearance)
            {
                this.appearanceHolder.Reset();
                this.SyncBackColor();
                this.Invalidate(true);                
            }
        }
        #endregion //Appearance

        #region BackColorInternal

        /// <summary>
        /// For internal use only. Used for serializing out the BackColor property value. This
        /// is strictly for backward compatibility.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [DefaultValue(typeof(Color), "")]
        public Color BackColorInternal
        {
            get
            {
                return this.backColorInternal;
            }
            set
            {
                if (value != this.backColorInternal)
                {
                    this.backColorInternal = value;

                    this.SyncBackColor();
                }
            }
        }
        #endregion //BackColorInternal        

        #region Band

        /// <summary>
        /// For Infragistics internal infrastructure purposes only.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public UltraGridBand Band
        {
            get
            {
                if (this.cachedBand != null)
                    return this.cachedBand;

                if (this.bandHierarchicalLevel == -1 || this.Grid == null || this.bandKey == null)
                    return null;

                // If the user has defined a schema at design-time without specifying
                // a DataSource, it is up to the user to ensure that the keys of the band
                // and the underlying list/table match.  If they do not, we will try to 
                // use the first band, assuming that the user had assigned the template
                // to the root band.
                if (this.bandHierarchicalLevel == 0 && this.Grid.DisplayLayout.SortedBands.Exists(this.bandKey) == false)
                {
                    if (this.Grid.DisplayLayout.SortedBands.Count > 0)
                        return this.Grid.DisplayLayout.SortedBands[0];

                    return null;
                }

                foreach (UltraGridBand band in this.Grid.DisplayLayout.SortedBands)
                {
                    if (band.Key == this.bandKey && band.HierarchicalLevel == this.bandHierarchicalLevel)
                    {
                        if (this.Grid.Initializing == false)
                        {
                            this.cachedBand = band;
                        }

                        return band;
                    }
                }

                return null;
            }
        }
        #endregion //Band

        #region BandHierarchyLevel

        /// <summary>
        /// For Infragistics internal infrastructure purposes only.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(-1)]
        public int BandHierarchyLevel
        {
            get { return this.bandHierarchicalLevel; }
            set
            {
                if (this.bandHierarchicalLevel == value)
                    return;

                if (this.IsShown)
                    throw new InvalidOperationException();

                this.bandHierarchicalLevel = value;
                this.cachedBand = null;
                this.DesignRow = null;
                this.parentBands = null;
                this.templateDescriptorList = null;
            }
        }
        #endregion //BandHierarchyLevel

        #region BandKey

        /// <summary>
        /// For Infragistics internal infrastructure purposes only.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(null)]
        public string BandKey
        {
            get { return this.bandKey; }
            set
            {
                if (this.bandKey == value)
                    return;
                
                if (this.IsShown)
                    throw new InvalidOperationException();

                this.bandKey = value;
                this.cachedBand = null;
                this.designRow = null;
                this.parentBands = null;
                this.templateDescriptorList = null;
            }
        }
        #endregion //BandKey

        #region ColumnData

        /// <summary>
        /// For Infragistics internal infrastructure purposes only.  Maintains a list of 
        /// columns available to the template at design-time for deserialization purposes.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public ColumnDescriptorDataCollection ColumnData
        {
            get 
            {
                if (this.columnData == null)
                    this.columnData = new ColumnDescriptorDataCollection();

                return this.columnData; 
            }
        }
        #endregion //ColumnData

        #region ControlUIElement

        /// <summary>
        /// Returns the template's main UIElement.
        /// </summary>
        protected internal virtual ControlUIElementBase ControlUIElement
        {
            get
            {
                if (this.controlElement == null)
                {
                    this.controlElement = new RowEditTemplateControlUIElement(this);
                    this.controlElement.Rect = this.Bounds;
                }
                return this.controlElement;
            }
        }
        #endregion //ControlUIElement

        #region CreationFilter

        /// <summary>
        /// Gets/sets the creation filter property
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public IUIElementCreationFilter CreationFilter
        {
            get
            {
                return this.creationFilter;
            }
            set
            {
                if (value != this.creationFilter)
                {
                    this.creationFilter = value;

                    if (this.IsHandleCreated)
                        this.Invalidate(true);

                    this.NotifyDesignEnvironmentOfChange();
                }
            }
        }
        #endregion CreationFilter

        #region CursorFilter

        /// <summary>
        /// Gets/sets the cursor filter property
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public IUIElementCursorFilter CursorFilter
        {
            get
            {
                return this.cursorFilter;
            }
            set
            {
                if (value != this.cursorFilter)
                {
                    this.cursorFilter = value;

                    this.NotifyDesignEnvironmentOfChange();
                }
            }
        }
        #endregion CursorFilter

        #region DialogSettings

        /// <summary>
        /// Returns a series of properties that affect the dialog used to show the template
        /// when the DisplayMode is set to <b>Modal</b> or <b>Modeless</b>.
        /// </summary>
        /// <seealso cref="DisplayMode"/>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RowEditTemplateDialogSettings DialogSettings
        {
            get
            {
                if (this.dialogSettings == null)
                {
                    this.dialogSettings = new RowEditTemplateDialogSettings();
                    this.dialogSettings.SubObjectPropChanged += new SubObjectPropChangeEventHandler(this.OnSubObjectPropChanged);
                }
                return this.dialogSettings;
            }
        }
        #endregion //DialogSettings

        #region DisplayMode

        /// <summary>
        /// Gets or sets how the template will behave when the user clicks outside
        /// the bounds of the control.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>Though the grid can be navigated while the template is shown
        /// modelessly, it is not possible to simultaneously edit a cell directly on the grid.
        /// </p>
        /// </remarks>
        [DefaultValue(RowEditTemplateDisplayMode.Modal)]
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_UltraGridRowEditTemplate_P_DisplayMode")]
        public RowEditTemplateDisplayMode DisplayMode
        {
            get { return this.displayMode; }
            set
            {
                if (this.displayMode == value)
                    return;

                if (this.IsShown)
                    throw new InvalidOperationException(SR.GetString("LE_UltraGridRowEditTemplate_SetDisplayModeWhileShown"));

                this.displayMode = value;

                // At this point if the form has been used, we should dipose of it, since there are some
                // odd quirks when shifting between Modal and Modeless templates and the reparenting 
                // of the control and it is simpler to just recreate the form.                
                if (this.form != null)
                {
                    this.form.Dispose();
                    this.form = null;
                }
            }
        }
        #endregion //DisplayMode

        #region DrawFilter

        /// <summary>
        /// Gets/sets the draw filter property
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)]
        public IUIElementDrawFilter DrawFilter
        {
            get
            {
                return this.drawFilter;
            }
            set
            {
                if (value != this.drawFilter)
                {
                    this.drawFilter = value;

                    if (this.IsHandleCreated)
                        this.Invalidate(true);

                    this.NotifyDesignEnvironmentOfChange();
                }
            }
        }
        #endregion DrawFilter

        #region IsShown

        /// <summary>
        /// Returns whether the template is currently being used to edit a row.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>If the template has been reparented and manually shown through the
        /// <see cref="UltraGrid.RowEditTemplateRequested"/> event, this property will not take that into
        /// account since at that point the control of the template has left the control of the grid.
        /// </p>
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsShown
        {
            get
            {
                switch (this.DisplayMode)
                {
                    case RowEditTemplateDisplayMode.Popup:
                        return DropDownManager.IsDroppedDown(this.Grid);

                    case RowEditTemplateDisplayMode.Modal:
                        return this.form != null && (this.isFormShown || this.isDisplayingModalForm);

                    case RowEditTemplateDisplayMode.Modeless:
                        return this.form != null && this.isFormShown;
                }

                return false;
            }
        }
        #endregion //IsShown        

        #region Grid

        /// <summary>
        /// For Infragistics internal infrastructure only.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public UltraGrid Grid
        {
            get { return this.grid; }
        }
        #endregion //Grid        

        #region Row

        /// <summary>
        /// Returns the row associated with the template, or null if the template is not currently editing a row.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public UltraGridRow Row
        {
            get 
            {                
                if (this.DesignMode)
                    return this.DesignRow;

                return this.row; 
            }
            internal set
            {
                if (this.row != value)
                {
                    // If we currently have shown the template and we are now setting the row to null,
                    // we need to make sure that we unhook any editors from the grid's events
                    if (this.row != null)
                    {                        
                        this.row.Layout.ActiveProxy = null;
                        this.UnHookFromEditorProxy();
                    }

                    // Make sure that any bound controls commit their changes
                    this.BindingContext[this].EndCurrentEdit();

                    this.row = value;

                    // MBS 5/28/08 - BR33438
                    // We should only have the DataBindings pull in the information if the
                    // row isn't null, since if the row is null we will have hidden the template
                    // (or if the customer has manually shown the template somehow, it is up
                    // to them to handle the values when there is no row)
                    if(this.row != null)
                        // We need to let any data-bound controls know that they need to update
                        // their values with the new row's values
                        this.TemplateDescriptorList.ResetBindings();

                    this.FireRowChanged();
                }
            }
        }
        #endregion //Row 

        #region StyleLibraryName

        /// <summary>
        /// Returns or sets the name of the styleset library in the <see cref="Infragistics.Win.AppStyling.StyleManager"/> that should be used by the control.
        /// </summary>
        /// <remarks>
        /// <p class="body">By default, this property defaults to an empty string and as such will obtain 
        /// its style information from the default style library that is loaded into memory using the 
        /// <see cref="Infragistics.Win.AppStyling.StyleManager.Load(string, bool, string)"/> method. However, there is also an overload of that method 
        /// which will accept a string name which will be used to identify the library (<see cref="Infragistics.Win.AppStyling.StyleManager.Load(string)"/>). Any controls/components 
        /// that have their <b>StyleLibraryName</b> set to that name will obtain their style information 
        /// from that library.</p>
        /// <p class="note"><b>Note:</b> If this property is set and the name does not match up with the 
        /// name of a loaded style library, the control/component will not have any style information.</p>
        /// </remarks>
        [LocalizedDescription("LD_UltraGridRowEditTemplate_P_StyleLibraryName")]
        [LocalizedCategory("LC_Appearance")]
        [DefaultValue("")]
        public string StyleLibraryName
        {
            get
            {
                return this.ComponentRole == null ? string.Empty : this.ComponentRole.StyleLibraryName;
            }
            set
            {
                if (value != this.StyleLibraryName &&
                    this.ComponentRole != null)
                {
                    this.ComponentRole.StyleLibraryName = value;
                }
            }
        }
        #endregion //StyleLibraryName

        #region StyleSetName

        /// <summary>
        /// Returns or sets the name of the styleset in the <see cref="Infragistics.Win.AppStyling.StyleManager"/> that should be used by the control.
        /// </summary>
        [LocalizedDescription("LD_UltraGridRowEditTemplate_P_StyleSetName")]
        [LocalizedCategory("LC_Appearance")]
        [DefaultValue("")]
        public string StyleSetName
        {
            get { return this.ComponentRole == null ? string.Empty : this.ComponentRole.StyleSetName; }
            set
            {
                if (value != this.StyleSetName &&
                    this.ComponentRole != null)
                {
                    this.ComponentRole.StyleSetName = value;
                }
            }
        }
        #endregion //StyleSetName

        #region UseAppStyling

        /// <summary>
        /// Returns or sets whether the control should use application styling.
        /// </summary>
        [LocalizedDescription("LD_UltraGridRowEditTemplate_P_UseAppStyling")]
        [LocalizedCategory("LC_Appearance")]
        [DefaultValue(true)]
        public bool UseAppStyling
        {
            get { return this.ComponentRole == null ? true : this.ComponentRole.Enabled; }
            set
            {
                if (value != this.UseAppStyling &&
                    this.ComponentRole != null)
                {
                    this.ComponentRole.Enabled = value;
                }
            }
        }
        #endregion //UseAppStyling

        #endregion //Public Properties

        #region Private/Internal Methods

        #region CalculateTemplateDisplayLocation

        private Point CalculateTemplateDisplayLocation()
        {
            UIElement rowElement = this.Row.GetUIElement();
            Rectangle exclusionRect = Rectangle.Empty;
            if (rowElement != null)
            {
                Rectangle elementRect;
                RowCellAreaUIElement rcArea = rowElement.GetDescendant(typeof(RowCellAreaUIElement)) as RowCellAreaUIElement;
                if (rcArea != null)
                    elementRect = rcArea.Rect;
                else
                    elementRect = rowElement.Rect;

                // We don't want to show the template over the left or top part of a row if that is out-of-view
                elementRect.X = Math.Max(0, elementRect.X);
                elementRect.Y = Math.Max(0, elementRect.Y);
                exclusionRect = this.Grid.RectangleToScreen(elementRect);
            }

            Rectangle formRect;
            DropDownRepositionInfo info = new DropDownRepositionInfo(SplitMonitorAction.ShiftToMonitorWithCursor);
            switch (this.DisplayMode)
            {
                case RowEditTemplateDisplayMode.Modal:
                case RowEditTemplateDisplayMode.Modeless:     

                    // We need to update the client size to get an accurate size calculation for 
                    // where the template will go.
                    this.Form.ClientSize = this.Size;
                    formRect = new Rectangle(exclusionRect.Location, this.Form.Size);                    

                    break;

                default:
                case RowEditTemplateDisplayMode.Popup:
                    formRect = new Rectangle(exclusionRect.Location, this.Size);
                    break;
            }

            // Have the DropDownManager's logic calculate where we're going to show the template,
            // since it takes into account whether the form will be shown off-screen and will
            // adjust the position accordingly.
            Rectangle rect = DropDownManager.CalculateDropRect(
                exclusionRect,
                formRect,
                this.Grid,
                false,
                DropDownPosition.BelowExclusionRect,
                info);

            return rect.Location;
        }
        #endregion //CalculateTemplateDisplayLocation

        #region DirtyAllProxies

        internal void DirtyAllProxies()
        {
            // Don't force the map to be created if we haven't used it
            if (this.columnProxyMap == null)
                return;

            Dictionary<string, UltraGridCellProxy>.Enumerator enumerator = this.columnProxyMap.GetEnumerator();
            while (enumerator.MoveNext())
                enumerator.Current.Value.Dirty();                
        }
        #endregion //DirtyAllProxies

        #region Display

        internal bool Display(UltraGridCell currentActiveCell, TemplateDisplaySource source)
        {
            // We can't do anything unless the template has a row associated with it
            if (this.Row == null)
                return false;

            this.templateDisplaySource = source;

            // Calculate where the template is going to be shown so that we can provide the user with
            // the correct information so that they can change it as necessary.
            Point startingLocation = this.CalculateTemplateDisplayLocation();

            BeforeRowEditTemplateDisplayedEventArgs beforeDisplayedArgs = new BeforeRowEditTemplateDisplayedEventArgs(this, startingLocation, source);
            this.Grid.FireEvent(GridEventIds.BeforeRowEditTemplateDisplayed, beforeDisplayedArgs);
            if (beforeDisplayedArgs.Cancel)
                return false;

            // We need to call this on the row so that should the user decide to cancel the 
            // changes from the template, we can undo any modifications on the row.
            this.Row.BeginEdit();

            // If there is no proxy that corresponds to the active cell in the grid, we shouldn't
            // prevent any proxies from taking focus since then no proxy will be able to do so
            if (currentActiveCell != null && this.columnProxyMap != null && this.columnProxyMap.ContainsKey(currentActiveCell.Column.Key))
                this.activeCellOnDisplay = currentActiveCell;

            switch (this.DisplayMode)
            {
                #region Modal

                case RowEditTemplateDisplayMode.Modal:
                    this.InitializeForm(beforeDisplayedArgs.Location);

                    // When we're showing the template, someone could use the BeforeEnterMode event of
                    // the grid to try and show the template again, which will cause an exception because
                    // we will not have gotten the OnShown event of the dialog before the first proxy
                    // in the form has gotten focus, so the call to ShowEditTemplate won't bail out yet.
                    // We'll use this flag for the IsShown property so that we can bail out early, and reset
                    // this flag once the form is actually shown so we can use the isFormShown flag instead.
                    this.isDisplayingModalForm = true;

                    // Show the template's form now.  Any of the subsequent events will be handled
                    // in the form's event handlers, since we will not return from the ShowDialog
                    // method until the form is closed.
                    this.Form.ShowDialog(this.GetOwningControl());

                    // This seems to be the only safe place to reparent the template after the dialog has
                    // completely closed, since the OnClosed event fires when the dialog still hasn't completely
                    // closed down, so that might lead to reparenting issues.
                    this.ReparentTemplate();

                    break;

                #endregion //Modal

                #region Modeless

                case RowEditTemplateDisplayMode.Modeless:                                        
                    this.InitializeForm(beforeDisplayedArgs.Location);
                    this.Form.Show(this.GetOwningControl());
                    this.Form.Activate();

                    break;

                #endregion //Modeless

                #region Popup

                case RowEditTemplateDisplayMode.Popup:
                    Rectangle screenRect = Rectangle.Empty;
                    UIElement rowElement = this.Row.GetUIElement();
                    if (rowElement != null)
                    {
                        Rectangle elementRect;
                        RowCellAreaUIElement rcArea = rowElement.GetDescendant(typeof(RowCellAreaUIElement)) as RowCellAreaUIElement;
                        if (rcArea != null)
                            elementRect = rcArea.Rect;
                        else
                            elementRect = rowElement.Rect;

                        // We don't want to show the template over the left or top part of a row if that is out-of-view
                        elementRect.X = Math.Max(0, elementRect.X);
                        elementRect.Y = Math.Max(0, elementRect.Y);
                        screenRect = this.Grid.RectangleToScreen(elementRect);
                    }

                    // Create the parameters for the dropdown
                    DropDownManagerParameters ddmParams = new DropDownManagerParameters();
                    ddmParams.AutoCloseUp = true;
                    ddmParams.BeforeCloseUpHandler = this.OnBeforeAutoCloseUp;
                    ddmParams.CloseUpHandler = this.OnAutoCloseUp;
                    ddmParams.DropDownControl = this;
                    ddmParams.DropDownSize = this.Size;
                    ddmParams.EatMouseMessageOnAutoCloseup = true;
                    ddmParams.ExclusionArea = screenRect;
                    ddmParams.IgnoreClicksInExclusionArea = true;
                    ddmParams.Location = beforeDisplayedArgs.Location;
                    ddmParams.Owner = grid;
                    ddmParams.RepositionInfo = new DropDownRepositionInfo(SplitMonitorAction.UseMonitorWithCursor);
                    ddmParams.UsePriorityMessageFilter = true;

                    DropDownManager.DropDown(ddmParams);

                    // If were were unable to show the template for whatever reason, we should return and undo
                    // anything that has been changed.
                    if (this.IsShown == false)
                    {
                        this.Row.CancelEdit();
                        return false;
                    }

                    // Make sure that the template is visible and focused
                    this.Visible = true;
                    this.Focus();

                    // If we need to activate a proxy, we need to scroll the control into view, but handle setting focus
                    // to it in the proxy's OnPaint, since otherwise the child elements may not have been created yet 
                    // (such as if the proxy is out of view in the template)
                    if (this.activeCellOnDisplay != null && this.ColumnProxyMap.ContainsKey(this.activeCellOnDisplay.Column.Key) &&
                        // If the control isn't visible, we shouldn't try to set focus to it and prevent others from gaining focus
                        this.columnProxyMap[currentActiveCell.Column.Key].Visible)
                    {
                        // If the control isn't visible, we shouldn't try to set focus to it and prevent others from gaining focus
                        if (this.columnProxyMap[this.activeCellOnDisplay.Column.Key].Visible)
                            this.ScrollControlIntoView(this.ColumnProxyMap[this.activeCellOnDisplay.Column.Key]);
                        else
                            this.activeCellOnDisplay = null;
                    }

                    // The template has been shown successfully, so we can notify any listeners
                    AfterRowEditTemplateDisplayedEventArgs afterArgs = new AfterRowEditTemplateDisplayedEventArgs(this, source);
                    this.Grid.FireEvent(GridEventIds.AfterRowEditTemplateDisplayed, afterArgs);

                    break;

                #endregion //Popup
            }

            return true;
        }
        #endregion //Display

        #region ExcludeProperty

        private static bool ExcludeProperty(PropertyDescriptor property, Attribute attribute)
        {
            if (null != attribute)
            {
                Attribute propAttrib = property.Attributes[attribute.GetType()];

                if (null != propAttrib)
                    return false == attribute.Match(propAttrib);
                else
                    return false == attribute.IsDefaultAttribute();
            }

            return false;
        }
        #endregion //ExcludeProperty

        #region FireRowChanged

        private void FireRowChanged()
        {
            if (this.rowChangedDelegate != null)
                this.rowChangedDelegate(this, RowEditTemplateRowChangedEventArgs.Empty);
        }
        #endregion //FireRowChanged

        #region FocusActiveProxy

        internal void FocusActiveProxy(UltraGridCell activeCell)
        {
            if (activeCell != null)
            {
                string columnKey = activeCell.Column.Key;
                if (this.ColumnProxyMap.ContainsKey(columnKey))
                {
                    UltraGridCellProxy proxy = this.ColumnProxyMap[columnKey];  
                    if (!proxy.Visible || proxy.ContainsFocus)
                        return;                    

                    // Make sure that the layout is hooked into the proxy
                    this.HookIntoEditorProxy(proxy);

                    GridUtils.FocusControl(proxy);
                    return;
                }
            }
            return;
        }
        #endregion //FocusActiveProxy

        #region GetOwningControl

        private Control GetOwningControl()
        {
            try
            {
                if (this.Grid != null)
                {
                    Form form = this.Grid.FindForm();
                    return form;
                }
            }
            catch (System.Security.SecurityException) { }

            return this.Grid;
        }
        #endregion //GetOwningControl

        #region HookComponentChangeService

        private void HookComponentChangeService()
        {
            if (this.hookedIntoComponentChangeService)
                return;

            IComponentChangeService changeService = this.GetService(typeof(IComponentChangeService)) as IComponentChangeService;
            if (changeService != null)
            {
                this.hookedIntoComponentChangeService = true;
                changeService.ComponentRemoved += new ComponentEventHandler(this.OnComponentRemoved);
                changeService.ComponentRename += new ComponentRenameEventHandler(this.OnComponentRename);
            }
        }
        #endregion //HookComponentChangeService

        #region HookControl

        private void HookControl(Control control)
        {
            if (this.isInitializing)
                return;

            UltraGridCellProxy proxy = control as UltraGridCellProxy;
            if (proxy != null)
            {                
                proxy.RowEditTemplate = this;

                // If the key is null, don't add it to the map now.  Instead, this will
                // happen when a value is finally set
                if (proxy.ColumnKey == null || proxy.ColumnKey.Length == 0)
                    return;

                // Keep a mapping of the column key to the proxy so that
                // when the template is shown, we can activate the proxy
                // corresponding to the active cell (if any)
                if (this.ColumnProxyMap.ContainsKey(proxy.ColumnKey) == false)
                    this.ColumnProxyMap.Add(proxy.ColumnKey, proxy);
                else
                    throw new InvalidOperationException(SR.GetString("LE_UltraGridRowEditTemplate_MultipleProxiesOnColumn"));
            }
            else
            {
                control.Enter += this.OnControlEnter;

                // We need to know when the controls has any child controls added
                // or removed, in case those controls are proxies
                control.ControlAdded += new ControlEventHandler(OnChildControlAdded);
                control.ControlRemoved += new ControlEventHandler(OnChildControlRemoved);
            }
        }
        #endregion//HookControl

        #region HookControls

        private void HookControls(Control parentControl)
        {
            foreach (Control control in parentControl.Controls)
            {
                this.HookControl(control);

                // Hook the child controls of each control
                this.HookControls(control);
            }
        }
        #endregion //HookControls

        #region HookIntoEditorProxy

        private void HookIntoEditorProxy(UltraGridCellProxy proxy)
        {
            if (this.Row == null)
                return;

            if (this.lastHookedEditor == proxy.Editor)
                return;

            // Unhook from any editor we may have hooked into previously
            //
            this.UnHookFromEditorProxy();

            this.lastHookedEditor = proxy.Editor;
            UltraGridLayout layout = this.Row.Layout;

            this.lastHookedEditor.BeforeEnterEditMode += layout.EmbeddableEditor_BeforeEnterEditModeEventHandler;
            this.lastHookedEditor.AfterEnterEditMode += layout.EmbeddableEditor_AfterEnterEditModeEventHandler;
            this.lastHookedEditor.BeforeExitEditMode += layout.EmbeddableEditor_BeforeExitEditModeEventHandler;
            this.lastHookedEditor.AfterExitEditMode += layout.EmbeddableEditor_AfterExitEditModeEventHandler;

            // If the editor supports drop down, then hook into BeforeDropDown and
            // AfterCloseUp so that we can fire respective grid events.
            //
            if (this.lastHookedEditor.SupportsDropDown)
            {
                this.lastHookedEditor.BeforeDropDown += layout.EmbeddableEditor_BeforeDropDownEventHandler;
                this.lastHookedEditor.AfterCloseUp += layout.EmbeddableEditor_AfterCloseupEventHandler;

                // We need to fire CellListSelect event and in order to do that we need to hook
                // into the SelectionChanged event of the emebddable editor.
                //
                this.lastHookedEditor.SelectionChanged += layout.EmbeddableEditor_SelectionChangedEventHandler;
            }
        }
        #endregion //HookIntoEditorProxy

        #region Initialize

        internal void Initialize(UltraGridBand band)
        {
            Debug.Assert(this.Grid == null || this.Grid.Initializing || this.Row == null || this.Row == this.DesignRow, "Resetting the band while the template still has a row associated with it");

            // If we're getting initialized with the same data, bail out
            if (this.Band != null && band != null && band.HierarchicalLevel == this.bandHierarchicalLevel && band.Key == this.bandKey)
                return;

            this.cachedBand = null;
            this.parentBands = null;
            this.templateDescriptorList = null;

            if (band == null)
            {
                this.bandKey = null;
                this.bandHierarchicalLevel = -1;

                // Make sure that the template is no longer holding onto
                // a reference to the previous grid
                this.SetGrid(null, null);
            }
            else
            {
                this.bandKey = band.Key;                
                if (band.Layout != null)                
                    this.SetGrid(band, band.Layout.Grid as UltraGrid);                
            }

            this.DesignRow = null;

            // Make sure that we dirty all the proxies, since they will now not
            // have any way of getting a value and may need to display differently
            this.DirtyAllProxies();

            this.VerifyColumnData();
        }
        #endregion //Initialize

        #region InitializeColumnData

        private void InitializeColumnData()
        {
            if (this.DesignMode && this.Grid != null && this.Grid.Initializing == false)
            {
                // Clear out existing column information as well as ancestor info
                this.ColumnData.Clear();

                // Initialize the column data in the same order that it appears in the DataBinding
                // list, mainly that the columns on the template will show up, following by
                // a list of ancestor bands, starting with the root.  We depend on this order
                // in the template's PropertyDescriptor for when the designer is deserializing
                // any DataBindings
                this.InitializeColumnData(this.Band);

                foreach (UltraGridBand parentBand in this.ParentBands)
                    this.InitializeColumnData(parentBand);
            }
        }

        private void InitializeColumnData(UltraGridBand band)
        {
            if (band == null)
                return;

            foreach(UltraGridColumn col in band.Columns)
            {
                if (col.IsChaptered)
                    continue;

                this.ColumnData.Add(new ColumnDescriptorData(col.Key, col.DataType, band.Key, band.HierarchicalLevel));
            }
        }
        #endregion //InitializeColumnData

        #region InitializeForm

        private void InitializeForm(Point startLocation)
        {
            if (this.Row == null)
            {
                Debug.Fail("Should not be initializing the form when we have no row");
                return;
            }

            this.Form.ClientSize = this.Size;
            this.Form.Location = startLocation;
            this.originalParent = Utilities.GetParent(this);
            this.originalDockStyle = this.Dock;
            this.originalSize = this.Size;
            this.Dock = DockStyle.Fill;
            this.Form.Controls.Add(this);

            this.Visible = true;
            this.Location = new Point(0, 0);
        }
        #endregion //InitializeForm        

        #region OnAutoCloseUp

        /// <summary>
        /// Provides a callback for when the DropDownManager has closed the template.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        internal void OnAutoCloseUp(object sender, EventArgs e)
        {
            // If an explicit call has been made to the Close method, we don't need to use the
            // callbacks in order to fire the events
            if (this.isInClose)
                return;

            this.isClosing = true;

            // Force the current editor to exit edit mode
            if (this.Row != null && this.Row.Layout.ActiveProxy != null && this.Row.Layout.ActiveProxy.Editor != null)
                this.Row.Layout.ActiveProxy.Editor.ExitEditMode(true, true);

            // We can't explicitly call EndEdit, since this would prevent the user from being
            // able to cancel the BeforeRowUpdate event
            this.CommitChanges();

            this.Row = null;
            this.Visible = false;
            this.isClosing = false;

            this.Grid.FireEvent(GridEventIds.AfterRowEditTemplateClosed, new AfterRowEditTemplateClosedEventArgs(this));
        }
        #endregion //OnAutoCloseUp

        #region OnBandColumnsChanged

        internal void OnBandColumnsChanged()
        {
            this.templateDescriptorList = null;

            // Null out the cached design row so that we 
            // have to re-get the correct information.
            this.DesignRow = null;

            // We want to invalidate the proxies because some might no longer be associated with a 
            // column and so need to redraw themselves            
            this.DirtyAllProxies();
        }
        #endregion //OnBandColumnsChanged

        #region OnBeforeAutoCloseUp

        internal void OnBeforeAutoCloseUp(object sender, EventArgs e)
        {
            // If an explicit call has been made to the Close method, we don't need to use the
            // callbacks in order to fire the events
            if (this.isInClose)
                return;

            Debug.Assert(DropDownManager.GetCurrentDropDownControl(grid) == this, "AutoCloseUp called with a different control; template expected");

            // We need to fire the BeforeRowEditTemplateClosed event, though since this was performed
            // using the DropDownManager, it cannot be cancelled, so this is just a notification to 
            // anyone who cares with the ForceClosing property set to True.
            BeforeRowEditTemplateClosedEventArgs args = new BeforeRowEditTemplateClosedEventArgs(this, true);
            grid.FireEvent(GridEventIds.BeforeRowEditTemplateClosed, args);

            // We need to commit any bound controls' values at this point before the template
            // is hidden, otherwise the value will not be stored
            this.BindingContext[this].EndCurrentEdit();
        }
        #endregion //OnBeforeAutoCloseUp

        #region OnBindingMangerBindingComplete

        private void OnBindingMangerBindingComplete(object sender, BindingCompleteEventArgs e)
        {
            if (this.isInCommitChanges == false)
                return;

            this.hasBindingError = e.BindingCompleteState != BindingCompleteState.Success;
        }
        #endregion //OnBindingMangerBindingComplete

        #region OnChildControlAdded

        private void OnChildControlAdded(object sender, ControlEventArgs e)
        {
            this.HookControl(e.Control);
        }
        #endregion //OnChildControlAdded

        #region OnChildControlRemoved

        private void OnChildControlRemoved(object sender, ControlEventArgs e)
        {
            this.UnhookControl(e.Control);
        }
        #endregion //OnChildControlRemoved

        #region OnComponentRemoving

        private void OnComponentRemoved(object sender, ComponentEventArgs e)
        {
            if (e.Component == this)
                this.UnhookComponentChangeService();
            else if (e.Component is IButtonControl)
            {
                // When an AcceptButton or CancelButton is removed from the template, we need 
                // to reset the appropriate property on the DialogSettings object.
                IButtonControl button = (IButtonControl)e.Component;
                if (this.DialogSettings.AcceptButton == button || this.DialogSettings.CancelButton == button)
                {
                    // Null out the AcceptButton property of the settings using a PropertyDescriptor
                    // so that we will get Undo/Redo functionality.
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(RowEditTemplateDialogSettings));
                    PropertyDescriptor buttonProperty;
                    if (this.DialogSettings.AcceptButton == button)
                        buttonProperty = properties.Find("AcceptButton", false);
                    else
                        buttonProperty = properties.Find("CancelButton", false);

                    buttonProperty.SetValue(this.DialogSettings, null);
                }
            }
        }
        #endregion //OnComponentRemoving

        #region OnComponentRename

        private void OnComponentRename(object sender, ComponentRenameEventArgs e)
        {
            if (e.Component is IButtonControl)
            {
                // When an AcceptButton or CancelButton is renamed on the template, we need 
                // to update the appropriate property on the DialogSettings object.
                IButtonControl button = (IButtonControl)e.Component;
                if (this.DialogSettings.AcceptButton == button || this.DialogSettings.CancelButton == button)
                {
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(RowEditTemplateDialogSettings));
                    PropertyDescriptor buttonProperty;
                    if (this.DialogSettings.AcceptButton == button)
                        buttonProperty = properties.Find("AcceptButton", false);
                    else
                        buttonProperty = properties.Find("CancelButton", false);

                    buttonProperty.SetValue(this.DialogSettings, e.Component);
                }
            }
        }
        #endregion //OnComponentRename

        #region OnControlEnter

        private void OnControlEnter(object sender, EventArgs e)
        {
            // When a control that isn't a proxy becomes the active control,
            // we should not have an active proxy in the grid's layout
            if (this.Row != null)
                this.Row.Layout.ActiveProxy = null;
        }
        #endregion //OnControlEnter

        #region OnFormClosed

        private void OnFormClosed()
        {
            this.isFormShown = false;

            // If the form wasn't closed due to a call to the Close method, then we need 
            // to make sure that we commit the changes here.
            if (this.isInClose == false &&
                // MBS 6/26/08 - BR34102
                // If we've already nulled out the row, that means 
                // that the template has been closed
                this.Row != null)
            {
                this.isClosing = true;

                if (this.Row != null && this.Row.Layout.ActiveProxy != null && this.Row.Layout.ActiveProxy.Editor != null)
                    this.Row.Layout.ActiveProxy.Editor.ExitEditMode(true, true);

                this.CommitChanges();

                // Null out the row to perform any relevant cleanup
                this.Row = null;

                this.isClosing = false;

                // Fire the AfterClosed event now that we've performed all our cleanup
                AfterRowEditTemplateClosedEventArgs args = new AfterRowEditTemplateClosedEventArgs(this);
                this.Grid.FireEvent(GridEventIds.AfterRowEditTemplateClosed, args);                
            }
        }
        #endregion //OnFormClosed

        #region OnFormClosing

        private void OnFormClosing(BeforeRowEditTemplateClosedEventArgs e)
        {
            // If the Close method was called explicitly, we will fire the
            // BeforeRowEditTemplateClosed event there.
            if (this.isInClose)
                return;

            this.Grid.FireEvent(GridEventIds.BeforeRowEditTemplateClosed, e);
        }
        #endregion //OnFormClosing

        #region OnFormShown

        private void OnFormShown()
        {
            this.isFormShown = true;

            // Reset this flag since we're not in the process of showing the form anymore.
            this.isDisplayingModalForm = false;

            // Ensure that the template is visible at this point.  There are some timing issues with a modal template
            // and starting with it maximized after reparenting the template, so it couldn't hurt to double-check this here.
            this.Visible = true;

            // If we need to activate a proxy, we need to scroll the control into view, but handle setting focus
            // to it in the proxy's OnPaint, since otherwise the child elements may not have been created yet 
            // (such as if the proxy is out of view in the template)
            if (this.activeCellOnDisplay != null && this.ColumnProxyMap.ContainsKey(this.activeCellOnDisplay.Column.Key))
            {
                // If the control isn't visible, we shouldn't try to set focus to it and prevent others from gaining focus
                if (this.columnProxyMap[this.activeCellOnDisplay.Column.Key].Visible)
                    this.ScrollControlIntoView(this.ColumnProxyMap[this.activeCellOnDisplay.Column.Key]);
                else
                    this.activeCellOnDisplay = null;
            }

            // The template has been shown successfully, so we can notify any listeners
            AfterRowEditTemplateDisplayedEventArgs afterArgs = new AfterRowEditTemplateDisplayedEventArgs(this, this.templateDisplaySource);
            this.Grid.FireEvent(GridEventIds.AfterRowEditTemplateDisplayed, afterArgs);
        }
        #endregion //OnFormShown

        #region OnProxyEnter

        internal bool OnProxyEnter(UltraGridCellProxy proxy)
        {
            if (this.Row != null && proxy.ColumnKey != null)
            {
                // The first cell in the template will get focus by default when we show the template. 
                // However, if we've clicked on a different cell in a row to show the template, we
                // don't want to set the ActiveCell to this first proxy as we want to automatically
                // set focus to the proxy associated with the clicked cell.
                if (this.activeCellOnDisplay != null && this.activeCellOnDisplay != this.Row.Cells[proxy.ColumnKey])
                    return false;

                if (this.Row.Cells.Exists(proxy.ColumnKey) == false)
                    return false;

                UltraGridCell proxyCell = this.Row.Cells[proxy.ColumnKey];

                // We need to set the active cell of the grid before hooking the editor's events.
                this.Grid.ActiveCell = proxyCell;

                // If we were unable to set the active cell, we can't proceed
                if (this.Grid.ActiveCell != proxyCell)
                {
                    // Put focus back to the proxy associated with the active cell.
                    if (this.Grid.ActiveCell != null && this.ColumnProxyMap.ContainsKey(this.Grid.ActiveCell.Column.Key))
                        GridUtils.FocusControl(this.ColumnProxyMap[this.Grid.ActiveCell.Column.Key]);

                    return false;
                }

                this.Row.Layout.ActiveProxy = proxy;
                this.HookIntoEditorProxy(proxy);

                return true;
            }
            return false;
        }
        #endregion //OnProxyEnter

        #region OnRowCancelEdit

        internal void OnRowCancelEdit()
        {
            this.BindingContext[this].CancelCurrentEdit();
        }
        #endregion //OnRowCancelEdit

        #region OnRowEndEdit

        internal void OnRowEndEdit()
        {
            this.BindingContext[this].EndCurrentEdit();
        }
        #endregion //OnRowEndEdit

        #region OnSubObjectPropChanged

        /// <summary>
        /// Called when a sub object property change notification is recieved.
        /// </summary>
        /// <param name="propChange">PropChangeInfo</param>
        protected virtual void OnSubObjectPropChanged(PropChangeInfo propChange)
        {
            if (propChange.Source == this.dialogSettings && propChange.PropId is RowEditTemplateDialogSettingsPropertyIds)
            {
                // If we haven't created the form now, don't bother doing so since we'll initialize
                // the properties when/if we do create it.
                if (this.form == null)
                    return;

                RowEditTemplateDialogSettingsPropertyIds propId = (RowEditTemplateDialogSettingsPropertyIds)propChange.PropId;
                switch (propId)
                {
                    case RowEditTemplateDialogSettingsPropertyIds.AcceptButton:
                        this.form.AcceptButton = this.dialogSettings.AcceptButton;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.AutoScaleMode:
                        this.form.AutoScaleMode = this.dialogSettings.AutoScaleMode;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.CancelButton:
                        this.form.CancelButton = this.dialogSettings.CancelButton;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.Caption:
                        this.form.Text = this.dialogSettings.Caption;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.ControlBox:
                        this.form.ControlBox = this.dialogSettings.ControlBox;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.FormBorderStyle:
                        this.form.FormBorderStyle = this.dialogSettings.FormBorderStyle;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.Icon:
                        this.form.Icon = this.dialogSettings.Icon;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.MaximizeBox:
                        this.form.MaximizeBox = this.dialogSettings.MaximizeBox;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.MaximumSize:
                        this.form.MaximumSize = this.dialogSettings.MaximumSize;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.MinimizeBox:
                        this.form.MinimizeBox = this.dialogSettings.MinimizeBox;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.MinimumSize:
                        this.form.MinimumSize = this.dialogSettings.MinimumSize;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.Opacity:
                        this.form.Opacity = this.dialogSettings.Opacity;
                        break;

                    case RowEditTemplateDialogSettingsPropertyIds.ShowIcon:
                        this.form.ShowIcon = this.dialogSettings.ShowIcon;
                        break;
                }
            }
            else if (this.HasAppearance && this.Appearance == propChange.Source)
            {
                // We want to set the base values on various properties so that the control
                // will properly pick them up.
                AppearancePropIds appearanceProp = (AppearancePropIds)propChange.PropId;
                switch (appearanceProp)
                {
                    case AppearancePropIds.BackColor:                        
                        this.SyncBackColor();
                        break;

                    case AppearancePropIds.ForeColor:
                        base.ForeColor = this.Appearance.ForeColor;
                        break;

                    case AppearancePropIds.Cursor:
                        base.Cursor = this.Appearance.Cursor;
                        break;
                }

                if(this.IsHandleCreated)
                    this.Invalidate(true);

                this.NotifyDesignEnvironmentOfChange();
            }
        }
        #endregion //OnSubObjectPropChanged

        #region RefreshBindings

        internal void RefreshBindings(UltraGridBand band)
        {
            CurrencyManager cm = null;
            if (this.Band.Key == band.Key && this.BandHierarchyLevel == band.HierarchicalLevel)
                cm = this.BindingContext[this] as CurrencyManager;
            else if (this.BindingContext.Contains(this, band.Key))
                cm = this.BindingContext[this, band.Key] as CurrencyManager;

            if (cm != null)
                cm.Refresh();
        }
        #endregion //RefreshBindings

        #region ReparentTemplate

        internal void ReparentTemplate()
        {
            // MBS 8/6/09 - TFS19300
            // Setting the Parent will implicitly cause the template to be removed
            // from the form's Controls collection, so no reason to do it here since apparently
            // it's causing some binding-related issues.
            //
            //this.Form.Controls.Remove(this);

            this.Visible = false;            
            this.Dock = this.originalDockStyle;
            this.Size = this.originalSize;

            if (this.originalParent != null)
            {
                this.Parent = this.originalParent;
                this.originalParent = null;
            }
        }
        #endregion //ReparentTemplate

        #region SetGrid

        internal void SetGrid(UltraGridBand band, UltraGrid grid)
        {
            this.grid = grid;

            // We don't want to set the HierarchicalLevel while the grid is
            // initializing since this value will likely not be correct at the time
            // the band is deserialized.  Additionally, this is the reason that we serialize
            // this property with the template anyway :-)
            if (this.grid != null && this.grid.Initializing == false)
                this.bandHierarchicalLevel = band.HierarchicalLevel;

            // Verify that we've been able to create the column data for DataBinding
            this.VerifyColumnData();
        }
        #endregion //SetGrid

        #region SyncBackColor

        private void SyncBackColor()
        {
            base.BackColor = Infragistics.Win.Utilities.ResolveControlBackColor(
                StyleUtils.GetRole(this, StyleUtils.TemplateRole.RowEditTemplatePanel),
                ComponentRole.GetResolutionOrderInfo(this.ComponentRole),
                this.HasAppearance ? this.Appearance : null,
                this.backColorInternal);
        }
        #endregion //SyncBackColor

        #region UnhookComponentChangeService

        private void UnhookComponentChangeService()
        {
            if (!this.hookedIntoComponentChangeService)
                return;

            IComponentChangeService changeService = this.GetService(typeof(IComponentChangeService)) as IComponentChangeService;
            if (changeService != null)
            {
                this.hookedIntoComponentChangeService = false;
                changeService.ComponentRemoving -= new ComponentEventHandler(this.OnComponentRemoved);
                changeService.ComponentRename -= new ComponentRenameEventHandler(this.OnComponentRename);
            }
        }
        #endregion //UnhookComponentChangeService

        #region UnhookControl

        private void UnhookControl(Control control)
        {
            if (this.isInitializing)
                return;

            UltraGridCellProxy proxy = control as UltraGridCellProxy;
            if (proxy != null)
            {                
                proxy.RowEditTemplate = null;

                // Remove the mapping from the list
                if(proxy.ColumnKey != null)
                    this.ColumnProxyMap.Remove(proxy.ColumnKey);
            }
            else
            {
                control.Enter -= this.OnControlEnter;
                control.ControlAdded -= new ControlEventHandler(OnChildControlAdded);
                control.ControlRemoved -= new ControlEventHandler(OnChildControlRemoved);
            }
        }
        #endregion //UnhookControl

        #region UnHookFromEditorProxy

        private void UnHookFromEditorProxy()
        {
            if (null != this.lastHookedEditor)
            {
                if (this.Row == null)
                    return;

                UltraGridLayout layout = this.Row.Layout;

                this.lastHookedEditor.BeforeEnterEditMode -= layout.EmbeddableEditor_BeforeEnterEditModeEventHandler;
                this.lastHookedEditor.AfterEnterEditMode -= layout.EmbeddableEditor_AfterEnterEditModeEventHandler;
                this.lastHookedEditor.BeforeExitEditMode -= layout.EmbeddableEditor_BeforeExitEditModeEventHandler;
                this.lastHookedEditor.AfterExitEditMode -= layout.EmbeddableEditor_AfterExitEditModeEventHandler;

                if (this.lastHookedEditor.SupportsDropDown)
                {
                    this.lastHookedEditor.BeforeDropDown -= layout.EmbeddableEditor_BeforeDropDownEventHandler;
                    this.lastHookedEditor.AfterCloseUp -= layout.EmbeddableEditor_AfterCloseupEventHandler;

                    // We need to fire CellListSelect event and in order to do that we need to hook
                    // into the SelectionChanged event of the emebddable editor.
                    //
                    this.lastHookedEditor.SelectionChanged -= layout.EmbeddableEditor_SelectionChangedEventHandler;
                }

                this.lastHookedEditor = null;
            }
        }
        #endregion //UnHookFromEditorProxy

        #region VerifyColumnData

        private bool VerifyColumnData()
        {
            // Simply check the ParentBands property for null, which will
            // generate the parent bands, if applicable, and then initialize the ColumnData 
            // based on the parent bands.
            return null != this.ParentBands;
        }
        #endregion //VerifyColumnData

        #endregion //Private/Internal Methods

        #region Protected/Public Methods

        #region Close

        /// <summary>
        /// Closes the template, if open.  An attempt will be made to commit the changes.
        /// </summary>
        /// <returns>True if the template was closed successfully, or was already closed.</returns>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>If the changes cannot be committed, the template will still try to close itself. 
        /// If you do not want to close the template if the changes cannot be committed, manually call
        /// <see cref="CommitChanges"/> and check the return value before calling this method.
        /// </p>
        /// </remarks>
        public bool Close()
        {
            return this.Close(true);
        }

        /// <summary>
        /// Closes the template, if open.
        /// </summary>
        /// <param name="commitChanges">True if any changes made should be persisted to the underlying row.</param>
        /// <remarks>
        /// <returns>True if the template was closed successfully, or was already closed.</returns>
        /// <p class="note">
        /// <b>Note: </b>If the changes cannot be committed, the template will still try to close itself. 
        /// If you do not want to close the template if the changes cannot be committed, manually call
        /// <see cref="CommitChanges"/> and check the return value before calling this method.
        /// </p>
        /// <p class="note">
        /// <b>Note: </b>Cancelling the changes will not show on the grid unless the underlying object that the row
        /// is bound to implements the <b>IEditableObject</b> interface.  If this is not the case, then cancelling change
        /// will need to be performed manually.
        /// </p>
        /// </remarks>
        public bool Close(bool commitChanges)
        {
            return this.Close(commitChanges, false);
        }

        internal bool Close(bool commitChanges, bool forceClose)
        {
            if (this.IsShown == false || this.Grid == null ||
                // MBS 6/25/08 - BR34102
                this.isInClose || this.isClosing)
                return true;

            try
            {
                // Keep track of the fact that the Close method has been called
                this.isInClose = true;

                // Give the user a chance to cancel the closing of the template.  Note that this is not the method
                // that will be called when the DropDownManager closes the template, since that cannot be cancelled,
                // so we fire the event from a different method.
                BeforeRowEditTemplateClosedEventArgs args = new BeforeRowEditTemplateClosedEventArgs(this, forceClose);
                grid.FireEvent(GridEventIds.BeforeRowEditTemplateClosed, args);
                if (args.Cancel && !forceClose)
                    return false;

                this.isClosing = true;

                // Force the current editor to exit edit mode, which will undo an invalid value
                if (this.Row.Layout.ActiveProxy != null && this.Row.Layout.ActiveProxy.Editor != null)
                    this.Row.Layout.ActiveProxy.Editor.ExitEditMode(true, true);

                if (commitChanges)
                    this.CommitChanges();                
                else
                {
                    this.BindingContext[this].CancelCurrentEdit();
                    this.Row.CancelEdit();
                }

                switch (this.DisplayMode)
                {
                    // Don't reparent the template with a modal dialog because there are some odd issues 
                    // with reparenting the template from a dialog if it's maximized (or possibly other 
                    // scenarios) in that the next time we show the form, the template isn't visible 
                    // though its Visible property is true
                    case RowEditTemplateDisplayMode.Modal:
                        this.Form.Close();
                        break;

                    case RowEditTemplateDisplayMode.Modeless:
                        this.Form.Close();                     
                        break;

                    case RowEditTemplateDisplayMode.Popup:
                        DropDownManager.CloseDropDown(this.Grid);
                        break;
                }                

                // If for some reason the template is still shown after closing it, we should return false
                if (this.IsShown && !forceClose)
                    return false;

                // If we've closed the template and committed the changes as necessary, we'll perform
                // the relevant cleanup by setting the row to null.
                this.Row = null;

                // Fire the AfterClosed event now that we've performed all our cleanup
                AfterRowEditTemplateClosedEventArgs e = new AfterRowEditTemplateClosedEventArgs(this);
                this.Grid.FireEvent(GridEventIds.AfterRowEditTemplateClosed, e);

                return true;
            }
            finally
            {
                this.isInClose = false;
                this.isClosing = false;
            }
        }
        #endregion //Close

        #region CommitChanges

        /// <summary>
        /// Attempts to commit the changes made to the underlying row.
        /// </summary>
        /// <returns>True if the changes were committed.</returns>
        public virtual bool CommitChanges()
        {
            bool wasInCommitChanges = this.isInCommitChanges;
            try
            {
                this.isInCommitChanges = true;
                if (this.Row == null)
                    return false;

                this.BindingContext[this].EndCurrentEdit();
                if (this.hasBindingError)
                {
                    // Reset the flag
                    this.hasBindingError = false;

                    // If we had a binding error when calling EndCurrentEdit, we should
                    // return false so that the user knows that there was a problem.  Additionally,
                    // there really isn't any reason for us to call Row.Update() at this point since
                    // we know that the value is invalid.
                    return false;
                }
                return this.Row.Update();
            }
            finally
            {
                this.isInCommitChanges = wasInCommitChanges;
            }
        }
        #endregion //CommitChanges

        #region NotifyDesignEnvironmentOfChange

        // SSP 6/18/02 UWG1211
        //
        // JJD 9/05/02
        // Renamed the method and made it protected so that it can be called
        // by derived classes
        /// <summary>
        /// <p class="body">In design mode notifies the <see cref="IComponentChangeService"/> that the component has changed so that it can mark the component dirty.</p>
        /// <p class="body">At runtime this method does nothing.</p>
        /// </summary>
        protected void NotifyDesignEnvironmentOfChange()
        {
            // AS - 10/31/01
            // We should be notifying the environment that something changed.
            if (this.DesignMode)
            {
                // get the IComponentChangeService so that we can notify the designer
                // that our control has changed
                //
                // SSP 6/19/02 UWG1211
                // Enclose this in try-catch because if the application is running,
                // then VB IDE doesn't allow modifying the control in the designer.
                // It throws an exception when the user attempts to modify the control
                // (for example resizing a column in the grid) and if we don't catch
                // it the designer blanks out the whole control and the only way to
                // get back the control is to restart the solutition.
                // So enclose the code that notifies the IComponentChange service
                // in try-catch.
                //
                try
                {
                    IComponentChangeService service = this.GetService(typeof(IComponentChangeService)) as IComponentChangeService;

                    if (service != null)
                    {
                        // AS 6/17/02
                        // Call the Changing first.
                        //
                        service.OnComponentChanging(this, null);
                        service.OnComponentChanged(this, null, null, null);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion // NotifyDesignEnvironmentOfChange

        #region OnAppStyleChanged

        /// <summary>
        /// Fired when a property changes on the associated role.
        /// </summary>
        internal protected virtual void OnAppStyleChanged()
        {
            this.SyncBackColor();

            UIElement mainElement = this.ControlUIElement;
            if (mainElement != null)
            {
                mainElement.DirtyChildElements(true);
                this.ControlUIElement.VerifyChildElements(true);
            }

            this.Invalidate(true);
        }
        #endregion //OnAppStyleChanged

        #region OnMouseEnterElement

        /// <summary>
        /// Called when an element is entered (the mouse is moved
        /// over the element)
        /// </summary>
        /// <param name="e"><see cref="UIElementEventArgs"/></param>
        protected virtual void OnMouseEnterElement(UIElementEventArgs e)
        {
        }
        #endregion //OnMouseEnterElement

        #region OnMouseLeaveElement

        /// <summary>
        /// Called when an element is left (the mouse is moved
        /// off the element)
        /// </summary>
        /// <param name="e"><see cref="UIElementEventArgs"/></param>
        protected virtual void OnMouseLeaveElement(UIElementEventArgs e)
        {
        }

        #endregion OnMouseLeaveElement

        #region ResolveAppearance

        /// <summary>
        /// Resolves the template's appearance.
        /// </summary>
        /// <param name="appearance">Structure to receive the updated appearance info.</param>
        /// <param name="requestedProps">Flag enumeration indicating which properties need to be resolved.</param>
        public void ResolveAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps)
        {
            AppStyling.UIRole role = this.ControlUIElement.UIRoleResolved;
            AppStyling.ResolutionOrderInfo order = ComponentRole.GetResolutionOrderInfo(this.ComponentRole);

            if (order.UseStyleBefore && role != null)
                role.ResolveAppearance(ref appearance, ref requestedProps, RoleState.Normal);

            if(order.UseControlInfo && this.HasAppearance)
                this.Appearance.MergeData(ref appearance, ref requestedProps);

            if (order.UseStyleAfter && role != null)
                role.ResolveAppearance(ref appearance, ref requestedProps, RoleState.Normal);

            // Defaults
            if (!appearance.HasPropertyBeenSet(AppearancePropFlags.BackColor) && (requestedProps & AppearancePropFlags.BackColor) != 0)
            {
                appearance.BackColor = this.BackColor;
                requestedProps ^= AppearancePropFlags.BackColor;
            }
        }
        #endregion //ResolveAppearance

        #endregion //Public Methods

        #region Events

        #region RowChanged

        /// <summary>
        /// Fired when the Row has been changed on the template.
        /// </summary>
        [LocalizedDescription("LD_UltraGridRowEditTemplate_E_RowChanged")]
        public event RowEditTemplateRowChangedEventHandler RowChanged
        {
            add
            {
                if (this.rowChangedDelegate == null)
                    this.rowChangedDelegate = value;
                else
                    this.rowChangedDelegate = (RowEditTemplateRowChangedEventHandler)System.Delegate.Combine(this.rowChangedDelegate, value);
            }
            remove
            {
                if (this.rowChangedDelegate != null)
                    this.rowChangedDelegate = (RowEditTemplateRowChangedEventHandler)System.Delegate.Remove(this.rowChangedDelegate, value);
            }
        }
        #endregion //RowChanged

        #endregion //Events

        #region Base Class Overrides

        #region BackColor

        /// <summary>
        /// BackColor property - use the BackColor property of the control's <see cref="Appearance"/> instead.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set
            {
                base.BackColor = value;
                this.Appearance.BackColor = value;
            }
        }
        #endregion //BackColor

        #region BackgroundImage

        /// <summary>
        /// BackgroundImage
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [Browsable(false)]
        public override System.Drawing.Image BackgroundImage
        {
            get { return base.BackgroundImage; }
            set { base.BackgroundImage = value; }
        }
        #endregion //BackgroundImage

        #region BackgroundImageLayout

        /// <summary>
        /// BackgroundImageLayout
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [Browsable(false)]
        public override ImageLayout BackgroundImageLayout
        {
            get { return base.BackgroundImageLayout; }
            set { base.BackgroundImageLayout = value; }
        }
        #endregion //BackgroundImageLayout

        #region BindingContext

        /// <summary>
        /// Overridden. Gets or sets the binding context that should be used for data binding
        /// purposes. This property must be set before data binding.
        /// </summary>        
        public override BindingContext BindingContext
        {
            get
            {
                if (this.bindingContext == null)
                {
                    this.bindingContext = new BindingContext();

                    // Hook the binding manager so that we can be notified when the binding fails
                    this.bindingContext[this].BindingComplete += new BindingCompleteEventHandler(this.OnBindingMangerBindingComplete);
                }

                return this.bindingContext;
            }
            set
            {
                if (this.bindingContext == value)
                    return;

                if(this.bindingContext != null)
                    this.bindingContext[this].BindingComplete -= new BindingCompleteEventHandler(this.OnBindingMangerBindingComplete);

                this.bindingContext = value;

                // Hook the binding manager so that we can be notified when the binding fails
                this.bindingContext[this].BindingComplete += new BindingCompleteEventHandler(this.OnBindingMangerBindingComplete);

                this.OnBindingContextChanged(EventArgs.Empty);
            }
        }
        #endregion //BindingContext

        #region Cursor

        /// <summary>
        /// Cursor property - use the Cursor property of the control's <see cref="Appearance"/> instead.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Cursor Cursor
        {
            get { return base.Cursor; }
            set
            {
                base.Cursor = value;
                this.Appearance.Cursor = value;
            }
        }
        #endregion //Cursor

        #region DefaultSize

        /// <summary>
        /// Returns the default size of the control
        /// </summary>
        protected override Size DefaultSize
        {
            get
            {
                return new Size(DEFAULT_CONTROL_WIDTH, DEFAULT_CONTROL_HEIGHT);
            }
        }
        #endregion //DefaultSize

        #region Dispose

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">True if managed and unmanaged resources should be cleaned up.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {    
                // MBS 6/26/08 
                // Noticed this while writing unit tests for BR34102
                if (this.IsShown)
                    this.Close(true, true);
                //
                this.Row = null;

                if (this.form != null)
                {
                    this.form.Dispose();
                    this.form = null;
                }

                if (this.dialogSettings != null)
                {
                    this.dialogSettings.Dispose();
                    this.dialogSettings = null;
                }

                this.UnhookComponentChangeService();
            }
            base.Dispose(disposing);
        }
        #endregion //Dispose

        #region ForeColor

        /// <summary>
        /// ForeColor property - use the ForeColor property of the control's <see cref="Appearance"/> instead.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override Color ForeColor
        {
            get
            {
                // If the control isn't enabled, we want to use the FontColorDisabled, if available
                if (this.Enabled == false)
                {
                    AppearanceData appearance = new AppearanceData();
                    AppearancePropFlags requestedProps;

                    requestedProps = AppearancePropFlags.ForeColorDisabled;
                    this.ResolveAppearance(ref appearance, ref requestedProps);
                    
                    if (appearance.ForeColorDisabled != Color.Empty)
                        return appearance.ForeColorDisabled;
                }

                // Use the base ForeColor if the user hasn't specified anything, since 
                // that will pick it up from the parent container.
                return base.ForeColor;
            }
            set
            {
                base.ForeColor = value;
                this.Appearance.ForeColor = value;
            }
        }
        #endregion //ForeColor

        #region OnControlAdded

        /// <summary>
        /// Raises the <see cref="Control.ControlAdded"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ControlEventArgs"/> that contains the event data.</param>
        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);

            this.HookControl(e.Control);
        }
        #endregion //OnControlAdded

        #region OnControlRemoved

        /// <summary>
        /// Raises the <see cref="Control.ControlRemoved"/> event.
        /// </summary>
        /// <param name="e">A <see cref="ControlEventArgs"/> that contains the event data.</param>
        protected override void OnControlRemoved(ControlEventArgs e)
        {
            base.OnControlRemoved(e);

            this.UnhookControl(e.Control);
        }
        #endregion //OnControlRemoved       

        #region OnCreateControl

        /// <summary>
        /// Raises the <see cref="Control.CreateControl"/> method.
        /// </summary>
        protected override void OnCreateControl()
        {
            // MBS 5/19/08 - BR33101
            // We now set these properties' defaults through the designer.
            //
            //// We need to have the default Visible property be false, but we don't want to
            //// set it as such if the user has explicitly serialized the value.
            //if (this.shouldSkipSetDefaultVisible == false)
            //{
            //    // Set the Visible property to false using a property descriptor to avoid odd Designer quirks
            //    if (this.DesignMode)
            //    {
            //        PropertyDescriptor visibleProperty = TypeDescriptor.GetProperties(this)["Visible"];
            //        visibleProperty.SetValue(this, false);
            //    }
            //    else
            //        this.Visible = false;
            //
            //    this.shouldSkipSetDefaultVisible = false;
            //}
            //
            //if (this.shouldSkipDefaultAutoScroll == false)
            //    this.AutoScroll = true;

            base.OnCreateControl();
        }
        #endregion //OnCreateControl

        #region OnPaint

        /// <summary>
        /// Calls the ControlUIElement's draw method
        /// </summary>
        /// <param name="e">The <see cref="PaintEventArgs"/></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            if (this.ControlUIElement != null)
            {
                this.ControlUIElement.Draw(e.Graphics,
                    e.ClipRectangle,
                    !this.GetStyle(ControlStyles.OptimizedDoubleBuffer),
                    AlphaBlendMode.Optimized);
            }
            base.OnPaint(e);
        }
        #endregion //OnPaint

        #region ProcessDialogKey

        /// <summary>
        /// Processes a dialog key.
        /// </summary>
        /// <param name="keyData">One of the System.Windows.Forms.Keys values that represents the key to process.</param>
        /// <returns>True if the key was processed by the control; otherwise, false.</returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            // We don't want to let the template handle the key if the editor needs to work with it
            if (this.ActiveProxy != null && this.ActiveProxy.Editor != null && this.ActiveProxy.Editor.IsInputKey(keyData))
                return false;

            return base.ProcessDialogKey(keyData);
        }
        #endregion //ProcessDialogKey

        #region Site

        /// <summary>
        /// The site is set at design time only.
        /// </summary>
        public override ISite Site
        {
            get
            {
                return base.Site;
            }
            set
            {
                // if the site is being reset first try to unhook from the 
                // component change service events
                if (value == null)
                    this.UnhookComponentChangeService();

                base.Site = value;

                // Re-hook the ComponentChange with the new Site
                if (value != null && value.DesignMode)
                    this.HookComponentChangeService();
            }
        }
        #endregion //Site

        #endregion //Base Class Overrides

        #region IListSource Members

        bool IListSource.ContainsListCollection
        {
            get { return true; }
        }

        IList IListSource.GetList()
        { 
            return this.TemplateDescriptorList;
        }

        #endregion

        #region ISupportInitialize Members

        void ISupportInitialize.BeginInit()
        {
            this.isInitializing = true;
        }

        void ISupportInitialize.EndInit()
        {
            this.isInitializing = false;
            
            // We need to hook into the events of all child controls
            // or initialize the proxies after initializing so that we know
            // when we need to null out or intialize proxies in the hierarchy
            this.HookControls(this);

            this.SyncBackColor();

            if (this.DesignMode)
                this.HookComponentChangeService();
        }

        #endregion

        #region IUltraControl Members

        Infragistics.Win.AppStyling.ComponentRole IUltraControl.ComponentRole
        {
            get { return this.ComponentRole; }
        }

        IUIElementCreationFilter IUltraControl.CreationFilter
        {
            get { return this.CreationFilter; }
        }

        IUIElementCursorFilter IUltraControl.CursorFilter
        {
            get { return this.CursorFilter; }
        }

        Cursor IUltraControl.DefaultCursor
        {
            get { return base.Cursor; }
        }

        IUIElementDrawFilter IUltraControl.DrawFilter
        {
            get { return this.DrawFilter; }
        }

        void IUltraControl.FireMouseEnterElement(UIElementEventArgs e)
        {
            this.OnMouseEnterElement(e);
        }

        void IUltraControl.FireMouseLeaveElement(UIElementEventArgs e)
        {
            this.OnMouseLeaveElement(e);
        }

        bool IUltraControl.IsFlatMode
        {
            get { return false; }
        }

        ISelectionManager IUltraControl.SelectionManager
        {
            get { return null; }
        }

        bool IUltraControl.SupportThemes
        {
            get { return false; }
        }

        #endregion

        #region IUltraControlElement Members

        ControlUIElementBase IUltraControlElement.MainUIElement
        {
            get { return this.ControlUIElement; }
        }

        #endregion

        #region ISupportAppStyling Members

        /// <summary>
        /// Returns the <see cref="Infragistics.Win.AppStyling.ComponentRole"/> that the control uses to provide its style information.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public ComponentRole ComponentRole
        {
            get 
            {
                if(this.componentRole == null)
                    this.componentRole = new UltraGridRowEditTemplateRole(this, UltraGridRowEditTemplateRole.ROWEDITTEMPLATE_ROLE_NAME);

                return this.componentRole;
            }
        }

        #endregion
    }
    #endregion //UltraGridRowEditTemplate
}
