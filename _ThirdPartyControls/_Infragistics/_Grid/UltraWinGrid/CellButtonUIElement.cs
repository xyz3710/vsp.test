#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Diagnostics;


	// SSP 7/26/07 BR22926
	// Added the else block. We are carving out space for the button element however not
	// positioning the button element. This is correct however since the cell element
	// doesn't draw the backcolor (DrawBackColor method is overridden), the space reserved
	// for the edit button will end up showing the row element's back color, which could be
	// different than the cell's back color. For example, the editor element could draw
	// white back color while the row element could draw gray, resulting in the space
	// reserved for the edit button to be gray as well. Therefore add a ui element in place
	// of the reserved space to draw the backcolor.
	// 
	/// <summary>
	/// UI element used to render background of the space reserved for an edit button element
	/// when using the <see cref="ColumnStyle"/> of <b>Edit</b>.
	/// </summary>
	/// <remarks>
	/// <para class="body">
	/// When using the column style of <b>Edit</b>, the edit button is displayed according
	/// to the <see cref="UltraGridColumn.ButtonDisplayStyle"/> property setting, which by
	/// default is when the mouse is over the cell. When the 
	/// edit button is not displayed and the horizontal alignment of the cell content is
	/// center or right, the space where the edit button would be dispalyed when the mouse is
	/// moved over the cell is reserved so when the edit button is displayed, the cell contents
	/// do not have to be shifted left (otherwise the edit button would override the contents).
	/// 
	/// However this poses a problem in that the cell itself doesn't draw its background color.
	/// The editor element draws the background color. The editor element doesn't occupy the
	/// space reserved for the edit button. Therefore there is a need for the cell background
	/// color to be drawn in the area when the edit button's space is reserved. This is precisely
	/// the purpose that this ui element serves.
	/// 
	/// <b>Note</b> that if you are using gradients, you may want to set the BackGradientAlignment to
	/// Container or another suitable setting so the backgrounds of the editor element and this element
	/// aren't distinguished in regards to gradient rendering.
	/// </para>
	/// </remarks>
	public class EditButtonSpaceHolderUIElement : UIElement
	{
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">The parent element</param>
		public EditButtonSpaceHolderUIElement( UIElement parent )
			: base( parent )
		{
		}

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps)
		{
			// We need to resolve the BackColor2 as well. Therefore delgate the call to
			// cellElem.
			// 
			CellUIElementBase cellElem = this.parentElement as CellUIElementBase;
			if ( null != cellElem )
				cellElem.InternalInitAppearance( ref appearance, ref requestedProps );
		}
	}

	// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
	// Created a base class for CellButtonUIElement and EditButtonUIElement since they have
	// so much identical functionality.  This new base class automatically figures out the 
	// ButtonStyle for the element.
	//
	/// <summary>
	/// Base class of CellButtonUIElement and EditButtonUIElement.
	/// </summary>
	public class ButtonWithStyleUIElement : Infragistics.Win.ButtonUIElementBase
	{
		#region Constructor

		internal ButtonWithStyleUIElement( UIElement parent ) : base( parent ) { }

		#endregion // Constructor

		#region Properties

		#region ButtonStyle

		/// <summary>
		/// Returns the style of the button.
		/// </summary>
		public override UIElementButtonStyle ButtonStyle
		{
			get
			{
				// SSP 3/27/06 - App Styling
				// Get the ButtonStyle off the CellButton role or the EditButton role depending on
				// which button this is.
				// 
				
				

				UltraGridBand band = this.Column.Band;
				if ( null == band )
					band = this.Row.BandInternal;

				if ( null != band )
				{
					StyleUtils.Role eRole;
					StyleUtils.CachedProperty cacheProp;
					if ( this is CellButtonUIElement )
					{
						eRole = StyleUtils.Role.CellButton;
						cacheProp = StyleUtils.CachedProperty.ButtonStyleCellButton;
					}
					else if ( this is EditButtonUIElement )
					{
						eRole = StyleUtils.Role.CellEditButton;
						cacheProp = StyleUtils.CachedProperty.ButtonStyleCellEditButton;
					}
					else
					{
						eRole = StyleUtils.GetControlAreaRole( band.Layout );
						cacheProp = StyleUtils.CachedProperty.ButtonStyleLayout;
					}

					return band.GetButtonStyleResolved( eRole, cacheProp );
				}
				


				// If the Band is just not around then return a reasonable default value.
				//
				return UIElementButtonStyle.Button3D;
			}
		}

		#endregion // ButtonStyle

		#region Column

		/// <summary>
		/// Returns the UltraGridColumn object.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
		/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn Column
		{
			get
			{
				return
					(Infragistics.Win.UltraWinGrid.UltraGridColumn)
					this.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridColumn ), true );
			}
		}

		#endregion // Column

		#region IsPressed

		// SSP 4/28/06 - App Styling
		// 
		

		#endregion // IsPressed

		#region Row

		// SSP 12/1/03 UWG2085
		// Added Row property below. Added it as public because we already have Column property as public.
		//
		/// <summary>
		/// Returns the UltraGridColumn object.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
		/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return
					(Infragistics.Win.UltraWinGrid.UltraGridRow)
					this.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ), true );
			}
		}

		#endregion // Row

		#endregion // Properties

		#region Methods

		#region DrawTheme

        /// <summary>
        /// Renders the element using the System theme.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
        /// <returns>True if the element was able to be rendered using the system themes.</returns>
		protected override bool DrawTheme(ref Infragistics.Win.UIElementDrawParams drawParams)
		{
			if ( this.ButtonStyle == UIElementButtonStyle.Button3D )
				return base.DrawTheme( ref drawParams );

			return false;
		}

		#endregion // DrawTheme

		#region OnClick

		/// <summary>
		/// overridden method to handle OnClick event
		/// </summary>
		protected override void OnClick()
		{
			// SSP 5/29/03 UWG2276
			// Also make sure the the cell element is enabled before calling OnButtonClick
			// on the cell element.
			//
			//if ( this.Parent is CellUIElement )
			if ( this.Parent is CellUIElement && this.Parent.Enabled )
			{
				// SSP 9/4/03 UWG2622
				// Only call OnButtonClick if this element is enabled.
				//
				if ( this.Enabled )
					( (CellUIElement)this.Parent ).Cell.OnButtonClick();
			}
		}

		#endregion // OnClick

		#endregion // Methods
	}

	// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
	// Made this class derive from ButtonWithStyleUIElement so that it can share the common code with
	// EditButtonUIElement.
	//
	/// <summary>
	/// UI Element for displaying cell buttons
	/// </summary>
	//public class CellButtonUIElement : Infragistics.Win.ButtonUIElementBase
	public class CellButtonUIElement : ButtonWithStyleUIElement
	{
		//#if DEBUG
		//		/// <summary>
		//		/// Constructor.
		//		/// </summary>
		//		/// <param name="parent">The parent element</param>
		//#endif
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>CellButtonUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		internal CellButtonUIElement( CellUIElement parent )
			: base( parent )
		{
		}

		// SSP 4/28/06 - App Styling
		// Override InitControlAppearance instead of InitAppearance. This way we don't have to re-implement
		// the app-style appearance resolution logic.
		// 
		

		// SSP 4/28/06 - App Styling
		// Override InitControlAppearance instead of InitAppearance. This way we don't have to re-implement
		// the app-style appearance resolution logic.
		// 
        /// <summary>
        /// Resolves the control specific appearances for this button. Default implementation
        /// merges in its Infragistics.Win.ButtonUIElementBase.Appearance. This method
        /// should only merge in the appearance properties exposed by the controls. It
        /// should not merge in any defaults.  The InitAppearance method calls this method
        /// in between the calls that resolve app-style appearance settings. In other
        /// words this method should itself not resolve any app-style appearance settings.
        /// Also note that the InitAppearance method will check UseControlInfo setting
        /// of the app-style and if it's false it will not call this method. Therefore
        /// the overridden implementations do no need to check for UseControlInfo app-style
        /// setting.
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
		protected override void InitControlAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			base.InitControlAppearance( ref appData, ref flags );

			this.Row.ResolveCellButtonAppearance( this.Column, ref appData, ref flags );
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			this.childElementsCollectionDirty = false;

			// get the collection (true forces an allocation if not already allocated)
			//
			UIElementsCollection oldElements = this.ChildElements;

			this.childElementsCollection = null;

			CellUIElement cellElement = (CellUIElement)this.Parent;

			// SSP 4/30/03 UWG1988
			// If the cell value is an image, then display that image rather than
			// displaying the text. What was happening before this fix was that when the column's
			// style was set to button, the cells were displaying the images however as soon as 
			// you moved the mouse over the cell, the cell displayed the button without the image
			// on the button.
			//
			// ----------------------------------------------------------------------------------
			UltraGridRow row = cellElement.Row;
			UltraGridColumn column = cellElement.Column;
			object cellValue = row.GetCellValue( column );

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( cellValue is System.Drawing.Image )
			Image imageCellValue = cellValue as Image;

			if ( imageCellValue != null )
			{
				ImageUIElement dataImageElement = (ImageUIElement)CellButtonUIElement.ExtractExistingElement( oldElements, typeof( ImageUIElement ), true );

				if ( null == dataImageElement )
				{
					// MD 8/7/07 - 7.3 Performance
					// FxCop - Do not cast unnecessarily
					//dataImageElement = new ImageUIElement( this, (Image)cellValue );
					dataImageElement = new ImageUIElement( this, imageCellValue );
				}

				// Use the text algins on the cell button appearance. If the cell button appearance doesn't
				// have text algins set, then use the text aligns of the cell appearance. The reason
				// for doing this is to show the image in the same location as shown in the cell so when
				// the user moves the mouse over the cell, the button will show up and the image will
				// be displayed in the same location.
				//
				AppearanceData tmpAppData = new AppearanceData();
				AppearancePropFlags propFlags = AppearancePropFlags.AllRender;
				// SSP 12/1/03 UWG2085
				// Added button appearance property to row object.
				//
				//column.ResolveCellButtonAppearance( ref tmpAppData, ref propFlags );
				// SSP 4/28/06 - App Styling
				// Use the InitAppearance instead which also takes into account the app-style settings.
				// 
				//row.ResolveCellButtonAppearance( column, ref tmpAppData, ref propFlags );
				this.InitAppearance( ref tmpAppData, ref propFlags );

                column.Layout.ResolvedCachedCellAppearance(
                    row,
                    column,
                    ref tmpAppData,
                    ref propFlags
                    // SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
                    // Added hotTrackingCell and hotTrackingRow parameters.
                    // 
                    , false, false);

				// MD 8/7/07 - 7.3 Performance
				// FxCop - Do not cast unnecessarily
				//CellUIElement.PositionImageElement( (Image)cellValue, dataImageElement,
				CellUIElement.PositionImageElement( imageCellValue, dataImageElement,
					this.RectInsideBorders, tmpAppData.TextHAlign, tmpAppData.TextVAlign );

				if ( dataImageElement.Rect.Width > 0 && dataImageElement.Rect.Height > 0 )
				{
					this.ChildElements.Add( dataImageElement );
					oldElements.Clear();
					return;
				}
			}
			// ----------------------------------------------------------------------------------

			AppearanceData appData = new AppearanceData();

			// Resolve this cell's appearance
			//
			// SSP 1/21/02 UWG950
			// Use the InitAppearance method to resolve the appearance which 
			// takes into account the newly added CellButtonAppearance off the
			// column and the overrides.
			//
			//cellElement.Row.ResolveCellAppearance( cellElement.Column, ref appData, AppearancePropFlags.AllRender );			
			AppearancePropFlags flags = AppearancePropFlags.AllRender;
			this.InitAppearance( ref appData, ref flags );


			Rectangle workRect = this.Rect;
			Rectangle textRect, buttonRect;

			textRect = workRect;

			// This was commented out, but correct				
			cellElement.Column.Header.AdjustForCellBorders( ref textRect );

			// SSP 8/6/03
			// In row layout designer, this.Control is not an UltraGridBase. It's the row 
			// layout designer control.
			//
			//System.Drawing.Image image = appData.GetImage( ((UltraGridBase)this.Control).ImageList );
			System.Drawing.Image image = appData.GetImage( this.Column.Layout.Grid.ImageList );

			if ( null != image )
			{
				buttonRect = textRect;

				// Adjust rects taking cell picture into account
				//
				bool scaled = cellElement.Column.Header.AdjustForCellPicture( ref textRect, ref buttonRect, ref appData );

				// add the picture element
				//
				ImageUIElement imageElement = (ImageUIElement)CellButtonUIElement.ExtractExistingElement( oldElements, typeof( ImageUIElement ), true );
				if ( null == imageElement )
				{
					imageElement = new ImageUIElement( this, image );
				}
				// SSP 12/4/03 UWG2776
				// Update the Image on the image element.
				//
				else
				{
					imageElement.Image = image;
				}

				imageElement.Rect = buttonRect;
				imageElement.Scaled = scaled;

				// JDN 2/11/05 BR02288 - offest the image to match the text when pressed
				if ( this.IsPressed )
				{
					Rectangle imageRect = imageElement.Rect;
					imageRect.Offset( 2, 2 );
					imageElement.Rect = imageRect;
				}

				this.ChildElements.Add( imageElement );
			}


			cellElement.Column.Header.AdjustForCellPadding( ref textRect );

			// add the text element
			//
			DependentTextUIElement textElement = (DependentTextUIElement)CellButtonUIElement.ExtractExistingElement( oldElements, typeof( DependentTextUIElement ), true );
			if ( null == textElement )
			{
				textElement = new DependentTextUIElement( this, cellElement.Row );
			}
			else
			{
				textElement.InitProvider( cellElement.Row );
			}
			textElement.Rect = textRect;
			this.ChildElements.Add( textElement );

			// clear the remaining elements (that weren't transferred in the new list)
			//
			oldElements.Clear();
		}


		

		/// <summary>
		/// Returns true if this element needs to draw a focus rect. This should 
		/// be overridden since the default implementation always returns false. 
		/// </summary>
		/// <remarks>Even if this property returns true the focus will not be drawn unless the control has focus.</remarks>
		protected override bool DrawsFocusRect
		{
			get
			{
				// JJD 1/21/02 - UWG951
				// Buttons draw their own focus rects
				//
				CellUIElement cellElement = this.GetAncestor( typeof( CellUIElement ) ) as CellUIElement;

				if ( cellElement == null ||
					!cellElement.HasCell ||
					cellElement.Cell == null ||
					!cellElement.Cell.IsActiveCell )
					return false;

				UltraGridCell cell = cellElement.Cell;

				RowScrollRegion rsr = this.GetContext( typeof( RowScrollRegion ) ) as RowScrollRegion;
				ColScrollRegion csr = this.GetContext( typeof( ColScrollRegion ) ) as ColScrollRegion;

				return ( rsr == cell.Layout.Grid.ActiveRowScrollRegion &&
						 csr == cell.Layout.Grid.ActiveColScrollRegion );
			}
		}

		// JDN 2/11/05 BR02288 - dirty child elements to reset offest image
		/// <summary>
        /// Clear internal flags and invalidate the button
		/// </summary>
        /// <returns>Returning true will ignore the next click event</returns>
		protected override bool OnMouseUp( MouseEventArgs e )
		{
			base.OnMouseUp( e );

			this.DirtyChildElements();

			return false;
		}

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get
			{
				return StyleUtils.GetRole( this.Row.BandInternal, StyleUtils.Role.CellButton );
			}
		}

		#endregion // UIRole

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
		//
		#region Moved Into Base Class

		//		/// <summary>
		//		/// Returns true if the button is currently pressed.
		//		/// </summary>
		//		public bool IsPressed
		//		{
		//			get
		//			{
		//				// AS - 11/12/01 DNF5
		//				// These protected members no longer exist.
		//				//
		//				//return this.mouseDown && this.mouseDownOverButton;
		//				return this.ButtonState == UIElementButtonState.MouseDownAndOver;
		//			}
		//		}

		//		/// <summary>
		//		/// overridden method to handle OnClick event
		//		/// </summary>
		//		protected override void OnClick( )
		//		{
		//			// SSP 5/29/03 UWG2276
		//			// Also make sure the the cell element is enabled before calling OnButtonClick
		//			// on the cell element.
		//			//
		//			//if ( this.Parent is CellUIElement )
		//			if ( this.Parent is CellUIElement && this.Parent.Enabled )
		//			{
		//				// SSP 9/4/03 UWG2622
		//				// Only call OnButtonClick if this element is enabled.
		//				//
		//				if ( this.Enabled )
		//					((CellUIElement)this.Parent).Cell.OnButtonClick();
		//			}
		//		}

		//		/// <summary>
		//		/// Returns the UltraGridColumn object.
		//		/// </summary>
		//		/// <remarks>
		//		/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
		//		/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
		//		/// </remarks>
		//		public Infragistics.Win.UltraWinGrid.UltraGridColumn Column
		//		{
		//			get
		//			{
		//				return 
		//					(Infragistics.Win.UltraWinGrid.UltraGridColumn)
		//					this.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridColumn ), true );
		//			}
		//		}

		//		// SSP 12/1/03 UWG2085
		//		// Added Row property below. Added it as public because we already have Column property as public.
		//		//
		//		/// <summary>
		//		/// Returns the UltraGridColumn object.
		//		/// </summary>
		//		/// <remarks>
		//		/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
		//		/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
		//		/// </remarks>
		//		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		//		{
		//			get
		//			{
		//				return 
		//					(Infragistics.Win.UltraWinGrid.UltraGridRow)
		//					this.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ), true );
		//			}
		//		}

		#endregion // Moved Into Base Class
	}


	// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
	// Made this class derive from ButtonWithStyleUIElement so that it can share the common code with
	// CellButtonUIElement.
	//
	// SSP 3/6/02 UWG1038
	// Changed the parent class from ButtonUIElement to Infragistics.Win.ButtonUIElementBase
	// 
	/// <summary>
	/// UI element that is used for displaying EditButton column style
	/// </summary>
	//public class EditButtonUIElement : Infragistics.Win.ButtonUIElement
	//public class EditButtonUIElement : Infragistics.Win.ButtonUIElementBase
	public class EditButtonUIElement : ButtonWithStyleUIElement
	{
		//#if DEBUG
		//		/// <summary>
		//		/// Constructor.
		//		/// </summary>
		//		/// <param name="parent">The parent element</param>
		//#endif
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b></b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		//internal EditButtonUIElement( CellUIElement parent ) : base( parent )
		public EditButtonUIElement( CellUIElement parent )
			: base( parent )
		{
		}

		// SSP 4/28/06 - App Styling
		// Override InitControlAppearance instead of InitAppearance. This way we don't have to re-implement
		// the app-style appearance resolution logic.
		// 
		

		// SSP 4/28/06 - App Styling
		// Override InitControlAppearance instead of InitAppearance. This way we don't have to re-implement
		// the app-style appearance resolution logic.
		// 
        /// <summary>
        /// Resolves the control specific appearances for this button. Default implementation
        /// merges in its Infragistics.Win.ButtonUIElementBase.Appearance. This method
        /// should only merge in the appearance properties exposed by the controls. It
        /// should not merge in any defaults.  The InitAppearance method calls this method
        /// in between the calls that resolve app-style appearance settings. In other
        /// words this method should itself not resolve any app-style appearance settings.
        /// Also note that the InitAppearance method will check UseControlInfo setting
        /// of the app-style and if it's false it will not call this method. Therefore
        /// the overridden implementations do no need to check for UseControlInfo app-style
        /// setting.
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
		protected override void InitControlAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			base.InitControlAppearance( ref appData, ref flags );

			this.Row.ResolveCellButtonAppearance( this.Column, ref appData, ref flags );
		}

		// SSP 3/6/02 UWG1038
		// Apply image settings on the cell button appearance to the edit buttons as well
		// Also added code for drawing ellipsis as it's supposed to (which it wasn't)
		// Overrode PositionChildElements.
		//
		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{
			this.childElementsCollectionDirty = false;

			// get the collection (true forces an allocation if not already allocated)
			//
			UIElementsCollection oldElements = this.ChildElements;

			ImageUIElement imageElement = (ImageUIElement)EditButtonUIElement.ExtractExistingElement( oldElements, typeof( ImageUIElement ), true );
			//TextUIElement  textElem = (TextUIElement)EditButtonUIElement.ExtractExistingElement( oldElements, typeof( TextUIElement ), true );

			this.childElementsCollection = null;


			AppearanceData appData = new AppearanceData();
			AppearancePropFlags flags = AppearancePropFlags.AllRender;
			this.InitAppearance( ref appData, ref flags );

			// SSP 8/6/03
			// In row layout designer, this.Control is not an UltraGridBase. It's the row 
			// layout designer control.
			//
			//System.Drawing.Image image = appData.GetImage( ((UltraGridBase)this.Control).ImageList );
			System.Drawing.Image image = appData.GetImage( this.Column.Layout.Grid.ImageList );

			// If there is an image, then add an image ui element, otherwise add a
			// text ui element with ellipsis.
			//
			if ( null != image )
			{
				Rectangle workRect = this.RectInsideBorders;
				Rectangle imageRect = workRect;

				Size imageSize = image.Size;

				bool scaled = false;

				// See if the image is bigger than the space we have
				//
				if ( imageSize.Width > imageRect.Width || imageSize.Height > imageRect.Height )
				{
					// Calculate the scaling factor. We want to maintain the
					// same aspect ration for the height and the width.
					//
					double xScaleFactor = (double)workRect.Width / imageSize.Width;
					double yScaleFactor = (double)workRect.Height / imageSize.Height;

					// Use the smaller of x and y scale factors.
					//
					double scaleFactor = Math.Min( xScaleFactor, yScaleFactor );

					// Only scale if the image size is bigger than the sapce we
					// have. If it's smaller, then don't need to stretch it.
					//
					if ( scaleFactor < 1.0 )
					{
						imageRect.Width = Math.Max( 1, (int)( scaleFactor * imageSize.Width ) );
						imageRect.Height = Math.Max( 1, (int)( scaleFactor * imageSize.Height ) );
						scaled = true;
					}
				}

				// If the image is smaller, then use the image alignment settings on the
				// appearance to adjust the rect.
				//
				HAlign hAlign = HAlign.Default != appData.ImageHAlign ? appData.ImageHAlign : HAlign.Center;
				VAlign vAlign = VAlign.Default != appData.ImageVAlign ? appData.ImageVAlign : VAlign.Middle;

				DrawUtility.AdjustHAlign( hAlign, ref imageRect, workRect );
				DrawUtility.AdjustVAlign( vAlign, ref imageRect, workRect );

				// add the picture element
				//
				if ( null == imageElement )
				{
					imageElement = new ImageUIElement( this, image );
				}
				// SSP 12/4/03 UWG2776
				// Update the Image on the image element.
				//
				else
				{
					imageElement.Image = image;
				}

				imageElement.Rect = imageRect;
				imageElement.Scaled = scaled;
				this.ChildElements.Add( imageElement );
			}
			

			// clear the remaining elements (that weren't transferred in the new list)
			//
			oldElements.Clear();
		}

		/// <summary>
        /// Default drawfocus method draws a focus rect inside the element's borders
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawFocus( ref UIElementDrawParams drawParams ) {}


		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get
			{
				return StyleUtils.GetRole( this.Row.BandInternal, StyleUtils.Role.CellEditButton );
			}
		}

		#endregion // UIRole

		// SSP 11/9/01 UWG547
		// Border drawing is taken care of by the framework, so don't override 
		// DrawBorders.
		//
		

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
		//
		#region Moved Into Base Class

		//		/// <summary>
		//		/// overridden method for handling OnClick event. It fires
		//		/// the OnButtonClick event on the grid
		//		/// </summary>
		//		protected override void OnClick( )
		//		{
		//			if ( this.Parent is CellUIElement )
		//			{
		//				// SSP 9/4/03 UWG2622
		//				// Only call OnButtonClick if this element is enabled.
		//				//
		//				if ( this.Enabled )
		//					((CellUIElement)this.Parent).Cell.OnButtonClick();
		//			}
		//		}

		//		/// <summary>
		//		/// returns true if the button is pressed at the moment
		//		/// </summary>
		//		public bool IsPressed
		//		{
		//			get
		//			{
		//				// AS - 11/12/01
		//				// These protected members no longer exist - DNF5.
		//				//
		//				//return this.mouseDown && this.mouseDownOverButton;
		//				return (this.ButtonState & UIElementButtonState.MouseDownAndOver) == UIElementButtonState.MouseDownAndOver; 
		//				
		//			}
		//		}

		//		/// <summary>
		//		/// Returns the UltraGridColumn object.
		//		/// </summary>
		//		/// <remarks>
		//		/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
		//		/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
		//		/// </remarks>
		//		public Infragistics.Win.UltraWinGrid.UltraGridColumn Column
		//		{
		//			get
		//			{
		//				return 
		//					(Infragistics.Win.UltraWinGrid.UltraGridColumn)
		//					this.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridColumn ), true );
		//			}
		//		}

		//		// SSP 12/1/03 UWG2085
		//		// Added Row property below. Added it as public because we already have Column property as public.
		//		//
		//		/// <summary>
		//		/// Returns the UltraGridColumn object.
		//		/// </summary>
		//		/// <remarks>
		//		/// <p class="body">The <b>Column</b> property of an object refers to a specific column in the grid as defined by an UltraGridColumn object. You use the <b>Column</b> property to access the properties of a specified UltraGridColumn object, or to return a reference to an UltraGridColumn object.</p>
		//		/// <p class="body">An UltraGridColumn object represents a single column in the grid. The UltraGridColumn object is closely linked with a single underlying data field that is used to supply the data for all the cells in the column (except in the case of unbound columns, which have no underlying data field). The UltraGridColumn object determines what type of interface (edit, dropdown list, calendar, etc.) will be used for individual cells, as well as controlling certain formatting and behavior-related settings, such as data masking, for the cells that make up the column.</p>
		//		/// </remarks>
		//		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		//		{
		//			get
		//			{
		//				return 
		//					(Infragistics.Win.UltraWinGrid.UltraGridRow)
		//					this.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ), true );
		//			}
		//		}

		#endregion // Moved Into Base Class

		// SSP 11/9/01 UWG547
		// Button drawing is taken care of by the framework, so don't override 
		// DrawBackColor.
		//
		
	}

	// SSP 4/26/02
	// Em Embeddable editors.
	// Commented cell drop down and check box element classes.
	//
	

}
