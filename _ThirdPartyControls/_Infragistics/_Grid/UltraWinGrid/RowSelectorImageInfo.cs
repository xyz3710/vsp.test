#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.Serialization;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using System.Security.Permissions;
using System.Diagnostics;
using Infragistics.Win.Design;

namespace Infragistics.Win.UltraWinGrid
{
    /// <summary>
    /// Class encapsulating all the state-specific row selector images
    /// </summary>
    [
    TypeConverter(typeof(System.ComponentModel.ExpandableObjectConverter)),
    Serializable()
    ]
    [
    InfragisticsFeature(
    Version = FeatureInfo.Version_9_2,
    FeatureName = FeatureInfo.FeatureName_RowSelectorStateSpecificImages)
    ]
     public class RowSelectorImageInfo : SubObjectBase, System.Runtime.Serialization.ISerializable
    {
        
        #region Constructors

        /// <summary>
        /// Constructor for the RowSelectorImageInfo class that accepts a UltraGridLayout object.
        /// </summary>
        /// <param name="layout">The owning UltraGridLayout object</param>
        public RowSelectorImageInfo(UltraGridLayout layout)
        {
            this.layout = layout;
        }

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected RowSelectorImageInfo(SerializationInfo info, StreamingContext context)
        {
            //since we're not saving properties with default values, we must set the default here
            //
            this.Reset();

            //everything that was serialized in ISerializable.GetObjectData() 
            // is now loaded with this foreach loop
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case "AddNewRowImage":
                        this.addNewRowImage = (Image)Utils.DeserializeProperty(entry, typeof(Image), this.addNewRowImage);
                        break;

                    case "ActiveAndDataChangedImage":
                        this.activeDataChangedImage = (Image)Utils.DeserializeProperty(entry, typeof(Image), this.activeDataChangedImage);
                        break;

                    case "ActiveAndAddNewRowImage":
                        this.activeAddNewRowImage = (Image)Utils.DeserializeProperty(entry, typeof(Image), this.activeAddNewRowImage);
                        break;

                    case "ActiveRowImage":
                        this.activeRowImage = (Image)Utils.DeserializeProperty(entry, typeof(Image), this.activeRowImage);
                        break;

                    case "DataChangedImage":
                        this.dataChangedImage = (Image)Utils.DeserializeProperty(entry, typeof(Image), this.dataChangedImage);
                        break;

                    // CDS 6/08/09 TFS18124 Need to deserialize the "useDefault" flags.
                    case "UseDefaultActiveAndAddNewRowImage":
                        this.useDefaultActiveAndAddNewRowImage = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.useDefaultActiveAndAddNewRowImage);
                        break;

                    case "UseDefaultActiveAndDataChangedImage":
                        this.useDefaultActiveAndDataChangedImage = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.useDefaultActiveAndDataChangedImage);
                        break;

                    case "UseDefaultActiveRowImage":
                        this.useDefaultActiveRowImage = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.useDefaultActiveRowImage);
                        break;

                    case "UseDefaultAddNewRowImage":
                        this.useDefaultAddNewRowImage = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.useDefaultAddNewRowImage);
                        break;

                    case "UseDefaultDataChangedImage":
                        this.useDefaultDataChangedImage = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.useDefaultDataChangedImage);
                        break;

                    default:
                        {
                            Debug.Assert(false, "Invalid entry in RowSelectorImageInfo de-serialization ctor");
                            break;
                        }
                }
            }
        }

        #endregion Constructors

        #region Enums

        #region RowState

        [Flags] internal enum RowState
        {
            Default = 0,
            Active = 1,
            DataChanged = 2,
            AddNewRow = 4,
            ActiveAndDataChanged = Active | DataChanged,
            ActiveAndAddNew = Active | AddNewRow,
        }

        #endregion RowState

        #endregion Enums

        #region Private Members

        private Image activeRowImage = null;
        private Image dataChangedImage = null;
        private Image addNewRowImage = null;
        private Image activeDataChangedImage = null;
        private Image activeAddNewRowImage = null;
        private UltraGridLayout layout = null;
        private UltraGrid grid = null;

        private Image activeRowImageFromResource = null;
        private Image addNewRowImageFromResource = null;
        private Image dataChangedImageFromResource = null;
        private Image activeAndAddNewRowImageFromResource = null;
        private Image activeAndDataChangedImageFromResource = null;

        private bool useDefaultActiveRowImage = true;
        private bool useDefaultAddNewRowImage = true;
        private bool useDefaultDataChangedImage = true;
        private bool useDefaultActiveAndAddNewRowImage = true;
        private bool useDefaultActiveAndDataChangedImage = true;

        #endregion Private Members

        #region Public/Internal Properties

        #region ActiveRowImage
        
        /// <summary>
        /// Gets or sets the user-specified Row Selector image used when the row is active.
        /// </summary>
        [TypeConverter(typeof(Appearance.AppearanceImageTypeConverter)),
            Editor(typeof(AppearanceImageEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [LocalizedDescription("LD_RowSelectorImageInfo_P_ActiveRowImage")]
        public Image ActiveRowImage
        {
            get
            {
                if (this.activeRowImage == null &&
                    this.useDefaultActiveRowImage)
                    return this.ActiveRowImageFromResource;

                return this.activeRowImage;
            }
            set
            {
                if (value != this.activeRowImage ||
                   (value == null &&
                    this.useDefaultActiveRowImage))
                {
                    this.useDefaultActiveRowImage = Image.Equals(value, this.ActiveRowImageFromResource);
                    this.activeRowImage = (this.useDefaultActiveRowImage) ? null : value;
                    StyleUtils.DirtyCachedPropertyVals(this.layout);

                    //Notify all listeners
                    this.NotifyPropChange(PropertyIds.RowSelectorActiveRowImage);
                }
            }
        }

        #endregion ActiveRowImage

        #region DataChangedImage

        /// <summary>
        /// Gets or sets the user-specified Row Selector image used when the row's data has been modified.
        /// </summary>
        [TypeConverter(typeof(Appearance.AppearanceImageTypeConverter)),
            Editor(typeof(AppearanceImageEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [LocalizedDescription("LD_RowSelectorImageInfo_P_DataChangedImage")]
        public Image DataChangedImage
        {
            get
            {
                if (this.dataChangedImage == null &&
                    this.useDefaultDataChangedImage)
                    return this.DataChangedImageFromResource;

                return this.dataChangedImage;                
            }
            set
            {
                if (value != this.dataChangedImage ||
                   (value == null &&
                    this.useDefaultDataChangedImage))
                {
                    this.useDefaultDataChangedImage = Image.Equals(value, this.DataChangedImageFromResource);
                    this.dataChangedImage = (this.useDefaultDataChangedImage) ? null : value;
                    StyleUtils.DirtyCachedPropertyVals(this.layout);

                    //Notify all listeners
                    this.NotifyPropChange(PropertyIds.RowSelectorDataChangedImage);
                }
            }
        }

        #endregion DataChangedImage

        #region AddNewRowImage

        /// <summary>
        /// Gets or sets the user-specified Row Selector image used on the AddNew row.
        /// </summary>
        [TypeConverter(typeof(Appearance.AppearanceImageTypeConverter)),
            Editor(typeof(AppearanceImageEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [LocalizedDescription("LD_RowSelectorImageInfo_P_AddNewRowImage")]
        public Image AddNewRowImage
        {
            get
            {
                if (this.addNewRowImage == null &&
                    this.useDefaultAddNewRowImage)
                    return this.AddNewRowImageFromResource;

                return this.addNewRowImage;
            }
            set
            {
                if (value != this.addNewRowImage ||
                   (value == null &&
                    this.useDefaultAddNewRowImage))
                {
                    this.useDefaultAddNewRowImage = Image.Equals(value, this.AddNewRowImageFromResource);
                    this.addNewRowImage = (this.useDefaultAddNewRowImage)? null : value;
                    StyleUtils.DirtyCachedPropertyVals(this.layout);

                    //Notify all listeners
                    
                    this.NotifyPropChange(PropertyIds.RowSelectorAddNewRowImage);
                }
            }
        }

        #endregion AddNewRowImage

        #region ActiveAndDataChangedImage

        /// <summary>
        /// Gets or sets the user-specified Row Selector image used when the row is active and the row's data has changed.
        /// </summary>
        [TypeConverter(typeof(Appearance.AppearanceImageTypeConverter)),
            Editor(typeof(AppearanceImageEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [LocalizedDescription("LD_RowSelectorImageInfo_P_ActiveAndDataChangedImage")]
        public Image ActiveAndDataChangedImage
        {
            get
            {
                if (this.activeDataChangedImage == null &&
                    this.useDefaultActiveAndDataChangedImage)
                    return this.ActiveAndDataChangedImageFromResource;

                return this.activeDataChangedImage;
            }
            set
            {
                if (value != this.activeDataChangedImage ||
                   (value == null &&
                    this.useDefaultActiveAndDataChangedImage))
                {
                    this.useDefaultActiveAndDataChangedImage = Image.Equals(value, this.ActiveAndDataChangedImageFromResource);
                    this.activeDataChangedImage = (this.useDefaultActiveAndDataChangedImage) ? null : value;
                    StyleUtils.DirtyCachedPropertyVals(this.layout);

                    //Notify all listeners
                    this.NotifyPropChange(PropertyIds.RowSelectorActiveAndDataChangedImage);
                }
            }
        }

        #endregion ActiveAndDataChangedImage

        #region ActiveAndAddNewRowImage

        /// <summary>
        /// Gets or sets the user-specified Row Selector image used when the AddNew row is active.
        /// </summary>
        [TypeConverter(typeof(Appearance.AppearanceImageTypeConverter)),
            Editor(typeof(AppearanceImageEditor), typeof(System.Drawing.Design.UITypeEditor))]
        [LocalizedDescription("LD_RowSelectorImageInfo_P_ActiveAndAddNewRowImage")]
        public Image ActiveAndAddNewRowImage
        {
            get
            {
                if (this.activeAddNewRowImage == null &&
                    this.useDefaultActiveAndAddNewRowImage)
                    return this.ActiveAndAddNewRowImageFromResource;

                return this.activeAddNewRowImage;
            }
            set
            {
                if (value != this.activeAddNewRowImage ||
                   (value == null &&
                    this.useDefaultActiveAndAddNewRowImage))
                {
                    this.useDefaultActiveAndAddNewRowImage = Image.Equals(value, this.ActiveAndAddNewRowImageFromResource);
                    this.activeAddNewRowImage = (this.useDefaultActiveAndAddNewRowImage) ? null : value;
                    StyleUtils.DirtyCachedPropertyVals(this.layout);

                    //Notify all listeners
                    this.NotifyPropChange(PropertyIds.RowSelectorActiveAndAddNewRowImage);
                }
            }
        }

        #endregion ActiveAndAddNewRowImage

        #region ActiveRowImageResolved

        internal Image ActiveRowImageResolved
        {
            get
            {
                return (Image)StyleUtils.GetCachedPropertyVal(this.layout, StyleUtils.CustomProperty.RowSelectorActiveRowImage, this.activeRowImage, null, (this.useDefaultActiveRowImage) ? this.ActiveRowImageFromResource : null);
            }
        }

        #endregion ActiveRowImageResolved

        #region DataChangedImageResolved

        internal Image DataChangedImageResolved
        {
            get
            {
                return (Image)StyleUtils.GetCachedPropertyVal(this.layout, StyleUtils.CustomProperty.RowSelectorDataChangedImage, this.dataChangedImage, null, (this.useDefaultDataChangedImage) ? this.DataChangedImageFromResource : null);
            }
        }

        #endregion DataChangedImageResolved

        #region AddNewRowImageResolved

        internal Image AddNewRowImageResolved
        {
            get
            {
                return (Image)StyleUtils.GetCachedPropertyVal(this.layout, StyleUtils.CustomProperty.RowSelectorAddNewRowImage, this.addNewRowImage, null, (this.useDefaultAddNewRowImage) ? this.AddNewRowImageFromResource : null);
            }
        }

        #endregion AddNewRowImageResolved

        #region ActiveAndDataChangedImageResolved

        internal Image ActiveAndDataChangedImageResolved
        {
            get
            {
                return (Image)StyleUtils.GetCachedPropertyVal(this.layout, StyleUtils.CustomProperty.RowSelectorActiveAndDataChangedImage, this.activeDataChangedImage, null, (this.useDefaultActiveAndDataChangedImage) ? this.ActiveAndDataChangedImageFromResource : null);
            }
        }

        #endregion ActiveAndDataChangedImageResolved

        #region ActiveAndAddNewRowImageResolved

        internal Image ActiveAndAddNewRowImageResolved
        {
            get
            {
                return (Image)StyleUtils.GetCachedPropertyVal(this.layout, StyleUtils.CustomProperty.RowSelectorActiveAndAddNewImage, this.activeAddNewRowImage, null, (this.useDefaultActiveAndAddNewRowImage) ? this.ActiveAndAddNewRowImageFromResource : null);
            }
        }

        #endregion ActiveAndAddNewRowImageResolved

        #region AddNewRowImageFromResource

        internal Image AddNewRowImageFromResource
        {
            get
            {
                if (null == this.addNewRowImageFromResource &&
                    this.Grid != null)
                    this.addNewRowImageFromResource = this.Grid.RowAddedIcon.ToBitmap();

                return this.addNewRowImageFromResource;
            }
        }

        #endregion AddNewRowImageFromResource

        #region ActiveRowImageFromResource

        internal Image ActiveRowImageFromResource
        {
            get
            {
                if (null == this.activeRowImageFromResource &&
                    this.Grid != null)
                    this.activeRowImageFromResource = this.Grid.ActiveRowIcon.ToBitmap();

                return this.activeRowImageFromResource;
            }
        }

        #endregion ActiveRowImageFromResource

        #region DataChangedImageFromResource

        internal Image DataChangedImageFromResource
        {
            get
            {
                if (null == this.dataChangedImageFromResource &&
                    this.Grid != null)
                    this.dataChangedImageFromResource = this.Grid.RowModifiedIcon.ToBitmap();

                return this.dataChangedImageFromResource;
            }
        }

        #endregion DataChangedImageFromResource

        #region ActiveAndDataChangedImageFromResource

        internal Image ActiveAndDataChangedImageFromResource
        {
            get
            {
                if (null == this.activeAndDataChangedImageFromResource &&
                    this.Grid != null)
                    this.activeAndDataChangedImageFromResource = this.Grid.ActiveRowModifiedIcon.ToBitmap();

                return this.activeAndDataChangedImageFromResource;
            }
        }

        #endregion ActiveAndDataChangedImageFromResource

        #region ActiveAndAddNewRowImageFromResource

        internal Image ActiveAndAddNewRowImageFromResource
        {
            get
            {
                if (null == this.activeAndAddNewRowImageFromResource &&
                    this.Grid != null)
                    this.activeAndAddNewRowImageFromResource = this.Grid.ActiveRowAddedIcon.ToBitmap();

                return this.activeAndAddNewRowImageFromResource;
            }
        }

        #endregion ActiveAndAddNewRowImageFromResource

        #region Grid

        private UltraGrid Grid
        {
            get
            {
                if (this.grid == null &&
                    this.layout != null)
                {
                    this.grid = this.layout.Grid as UltraGrid;
                }
                return this.grid;
            }
        }

        #endregion Grid

        #region ShouldColorizeActiveRowImage

        internal bool ShouldColorizeActiveRowImage
        {
            get { return this.useDefaultActiveRowImage; }
        }

        #endregion ShouldColorizeActiveRowImage

        #region ShouldColorizeAddNewRowImage


        internal bool ShouldColorizeAddNewRowImage
        {
            get { return this.useDefaultAddNewRowImage; }
        }

        #endregion ShouldColorizeAddNewRowImage

        #region ShouldColorizeDataChangedImage

        internal bool ShouldColorizeDataChangedImage
        {
            get { return this.useDefaultDataChangedImage; }
        }

        #endregion ShouldColorizeDataChangedImage

        #region ShouldColorizeActiveAndAddNewRowImage

        internal bool ShouldColorizeActiveAndAddNewRowImage
        {
            get { return this.useDefaultActiveAndAddNewRowImage; }
        }

        #endregion ShouldColorizeActiveAndAddNewRowImage

        #region ShouldColorizeActiveAndDataChangedImage

        internal bool ShouldColorizeActiveAndDataChangedImage
        {
            get { return this.useDefaultActiveAndDataChangedImage; }
        }

        #endregion ShouldColorizeActiveAndDataChangedImage

        #endregion Public/Internal Properties

        #region Methods

        #region ActiveRowImage serialization

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        public bool ShouldSerializeActiveRowImage()
        {
            return !this.useDefaultActiveRowImage;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetActiveRowImage()
        {
            // CDS 6/04/09 TFS18230 Logic in the setter prevented the proper functionality to set the member to null and reset the 'useDefault' flag
            //this.ActiveRowImage = null;
            //this.useDefaultActiveRowImage = true;
            if (ActiveRowImage != this.ActiveRowImageFromResource)
            {
                this.activeRowImage = null;
                this.useDefaultActiveRowImage = true;

                StyleUtils.DirtyCachedPropertyVals(this.layout);

                //Notify all listeners
                this.NotifyPropChange(PropertyIds.RowSelectorActiveRowImage);
            }
        }

        #endregion ActiveRowImage serialization

        #region DataChangedImage serialization

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        public bool ShouldSerializeDataChangedImage()
        {
            return !this.useDefaultDataChangedImage;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetDataChangedImage()
        {
            // CDS 6/04/09 TFS18230 Logic in the setter prevented the proper functionality to set the member to null and reset the 'useDefault' flag
            //this.DataChangedImage = null;
            //this.useDefaultDataChangedImage = true;
            if (this.DataChangedImage != this.DataChangedImageFromResource)
            {
                this.dataChangedImage = null;
                this.useDefaultDataChangedImage = true;

                StyleUtils.DirtyCachedPropertyVals(this.layout);

                //Notify all listeners
                this.NotifyPropChange(PropertyIds.RowSelectorDataChangedImage);
            }
        }

        #endregion DataChangedImage serialization

        #region AddNewRowImage serialization

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        public bool ShouldSerializeAddNewRowImage()
        {
            return !this.useDefaultAddNewRowImage;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetAddNewRowImage()
        {
            // CDS 6/04/09 TFS18230 Logic in the setter prevented the proper functionality to set the member to null and reset the 'useDefault' flag
            //this.AddNewRowImage = null;
            //this.useDefaultAddNewRowImage = true;
            if (AddNewRowImage != this.AddNewRowImageFromResource)
            {
                this.addNewRowImage = null;
                this.useDefaultAddNewRowImage = true;

                StyleUtils.DirtyCachedPropertyVals(this.layout);

                //Notify all listeners
                this.NotifyPropChange(PropertyIds.RowSelectorAddNewRowImage);
            }
        }


        #endregion AddNewRowImage serialization

        #region ActiveAndDataChangedImage serialization

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        public bool ShouldSerializeActiveAndDataChangedImage()
        {
            return !this.useDefaultActiveAndDataChangedImage;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetActiveAndDataChangedImage()
        {
            // CDS 6/04/09 TFS18230 Logic in the setter prevented the proper functionality to set the member to null and reset the 'useDefault' flag
            //this.ActiveAndDataChangedImage = null;
            //this.useDefaultActiveAndDataChangedImage = true;
            if (this.ActiveAndDataChangedImage != this.ActiveAndDataChangedImageFromResource)
            {
                this.activeDataChangedImage = null;
                this.useDefaultActiveAndDataChangedImage = true;

                StyleUtils.DirtyCachedPropertyVals(this.layout);

                //Notify all listeners
                this.NotifyPropChange(PropertyIds.RowSelectorActiveAndDataChangedImage);
            }
        }

        #endregion ActiveAndDataChangedImage serialization

        #region ActiveAndAddNewRowImage serialization

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        public bool ShouldSerializeActiveAndAddNewRowImage()
        {
            return !this.useDefaultActiveAndAddNewRowImage;
        }

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public void ResetActiveAndAddNewRowImage()
        {
            // CDS 6/04/09 TFS18230 Logic in the setter prevented the proper functionality to set the member to null and reset the 'useDefault' flag
            //this.ActiveAndAddNewRowImage = null;
            //this.useDefaultActiveAndAddNewRowImage = true;
            if (this.ActiveAndAddNewRowImage != this.ActiveAndAddNewRowImageFromResource)
            {
                this.activeAddNewRowImage = null;
                this.useDefaultActiveAndAddNewRowImage = true;

                StyleUtils.DirtyCachedPropertyVals(this.layout);

                //Notify all listeners
                this.NotifyPropChange(PropertyIds.RowSelectorActiveAndAddNewRowImage);
            }
        }

        #endregion ActiveAndAddNewRowImage serialization

        #region OnDispose

        /// <summary>
        /// Invoked when the RowSelectorImageInfo is being disposed.
        /// </summary>
        protected override void OnDispose()
        {            
            this.activeRowImage = null;
            this.dataChangedImage = null;
            this.addNewRowImage = null;
            this.activeDataChangedImage = null;
            this.activeAddNewRowImage = null;
            this.grid = null;
            this.layout = null;

            if (this.activeAndAddNewRowImageFromResource != null)
            {
                this.activeAndAddNewRowImageFromResource.Dispose();
                this.activeAndAddNewRowImageFromResource = null;
            }

            if (this.activeAndDataChangedImageFromResource != null)
            {
                this.activeAndDataChangedImageFromResource.Dispose();
                this.activeAndDataChangedImageFromResource = null;
            }

            if (this.activeRowImageFromResource != null)
            {
                this.activeRowImageFromResource.Dispose();
                this.activeRowImageFromResource = null;
            }

            if (this.addNewRowImageFromResource != null)
            {
                this.addNewRowImageFromResource.Dispose();
                this.addNewRowImageFromResource = null;
            }

            if (this.dataChangedImageFromResource != null)
            {
                this.dataChangedImageFromResource.Dispose();
                this.dataChangedImageFromResource = null;
            }

            base.OnDispose();
        }

        #endregion OnDispose

        #region Reset

        /// <summary>
        /// Resets the object back to its default state.
        /// </summary>
        public void Reset()
        {
            this.ResetActiveAndAddNewRowImage();
            this.ResetActiveAndDataChangedImage();
            this.ResetActiveRowImage();
            this.ResetAddNewRowImage();
            this.ResetDataChangedImage();
        }

        #endregion Reset

        // CDS 6/01/09 TFS18124
        #region ResetInternal

        internal void ResetInternal()
        {
            this.activeAddNewRowImage =
                this.activeDataChangedImage =
                this.activeRowImage =
                this.addNewRowImage =
                this.dataChangedImage = null;

            this.useDefaultActiveAndAddNewRowImage =
                this.useDefaultActiveAndDataChangedImage =
                this.useDefaultActiveRowImage =
                this.useDefaultAddNewRowImage =
                this.useDefaultDataChangedImage = true;

            StyleUtils.DirtyCachedPropertyVals(this.layout);
        }

        #endregion ResetInternal


        #region ShouldSerialize

        internal bool ShouldSerialize()
        {
            return (this.ShouldSerializeActiveAndAddNewRowImage() ||
                    this.ShouldSerializeActiveAndDataChangedImage() ||
                    this.ShouldSerializeActiveRowImage() ||
                    this.ShouldSerializeAddNewRowImage() ||
                    this.ShouldSerializeDataChangedImage());
        }

        #endregion ShouldSerialize

        #region InitializeFrom

        // CDS 6/08/09 TFS18124
        //internal RowSelectorImageInfo InitializeFrom(RowSelectorImageInfo source)
        internal void InitializeFrom(RowSelectorImageInfo source)
        {
            // CDS 6/08/09 TFS18124  Dont create a new instance. Need to serialize the "useDefault" flags too.
            //RowSelectorImageInfo copy = new RowSelectorImageInfo(source.layout);

            //copy.activeAddNewRowImage = source.activeAddNewRowImage;
            //copy.activeDataChangedImage = source.activeDataChangedImage;
            //copy.activeRowImage = source.activeRowImage;
            //copy.addNewRowImage = source.addNewRowImage;
            //copy.dataChangedImage = source.dataChangedImage;

            //return source;

            if (null == source)
            {
                this.ResetInternal();
                return;
            }

            this.layout = source.layout;
            this.activeAddNewRowImage = source.activeAddNewRowImage;
            this.activeDataChangedImage = source.activeDataChangedImage;
            this.activeRowImage = source.activeRowImage;
            this.addNewRowImage = source.addNewRowImage;
            this.dataChangedImage = source.dataChangedImage;

            this.useDefaultActiveAndAddNewRowImage = source.useDefaultActiveAndAddNewRowImage;
            this.useDefaultActiveAndDataChangedImage = source.useDefaultActiveAndDataChangedImage;
            this.useDefaultActiveRowImage = source.useDefaultActiveRowImage;
            this.useDefaultAddNewRowImage = source.useDefaultAddNewRowImage;
            this.useDefaultDataChangedImage = source.useDefaultDataChangedImage;

            StyleUtils.DirtyCachedPropertyVals(this.layout);

        }
        #endregion InitializeFrom

        #region ToString

        /// <summary>
        /// Overriden version of ToString that indicates how many image properties are set.
        /// </summary>
        /// <returns>Returns the number of image properties set in a string representation. Returns string.empty if no images are set.</returns>
        public override string ToString()
        {
            int nonDefaultPropertyCount = 0;

            bool[] properties = new bool[5] { this.useDefaultActiveAndAddNewRowImage, this.useDefaultActiveAndDataChangedImage, this.useDefaultActiveRowImage, this.useDefaultAddNewRowImage, this.useDefaultDataChangedImage};

            foreach (bool i in properties)
                if (!i)
                    nonDefaultPropertyCount++;

            return (nonDefaultPropertyCount > 0) ? string.Format("{0} {1} Set", nonDefaultPropertyCount, (nonDefaultPropertyCount == 1)? "Property" : "Properties") : string.Empty;
        }

        #endregion ToString

        #endregion Methods

        #region ISerializable Members
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (this.ShouldSerializeActiveAndAddNewRowImage())
            {
                Utils.SerializeProperty(info, "ActiveAndAddNewRowImage", this.activeAddNewRowImage);
                // CDS 6/08/09 TFS18124 Need to serialize the "useDefault" flags.
                Utils.SerializeProperty(info, "UseDefaultActiveAndAddNewRowImage", this.useDefaultActiveAndAddNewRowImage);
            }

            if (this.ShouldSerializeActiveAndDataChangedImage())
            {
                Utils.SerializeProperty(info, "ActiveAndDataChangedImage", this.activeDataChangedImage);
                // CDS 6/08/09 TFS18124 Need to serialize the "useDefault" flags.
                Utils.SerializeProperty(info, "UseDefaultActiveAndDataChangedImage", this.useDefaultActiveAndDataChangedImage);
            }

            if (this.ShouldSerializeActiveRowImage())
            {
                Utils.SerializeProperty(info, "ActiveRowImage", this.activeRowImage);
                // CDS 6/08/09 TFS18124 Need to serialize the "useDefault" flags.
                Utils.SerializeProperty(info, "UseDefaultActiveRowImage", this.useDefaultActiveRowImage);
            }

            if (this.ShouldSerializeAddNewRowImage())
            {
                Utils.SerializeProperty(info, "AddNewRowImage", this.addNewRowImage);
                // CDS 6/08/09 TFS18124 Need to serialize the "useDefault" flags.
                Utils.SerializeProperty(info, "UseDefaultAddNewRowImage", this.useDefaultAddNewRowImage);
            }

            if (this.ShouldSerializeDataChangedImage())
            {
                Utils.SerializeProperty(info, "DataChangedImage", this.dataChangedImage);
                // CDS 6/08/09 TFS18124 Need to serialize the "useDefault" flags.
                Utils.SerializeProperty(info, "UseDefaultDataChangedImage", this.useDefaultDataChangedImage);
            }
        }

        #endregion

    }
}
