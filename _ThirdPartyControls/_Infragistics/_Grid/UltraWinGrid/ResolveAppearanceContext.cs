#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// This structure is used to pass around context information
    /// between methods while resolving UltragRid appearances 
    /// </summary>
    internal struct ResolveAppearanceContext
    {
		public AppearancePropFlags UnresolvedProps;

        // Object type 
        private System.Type objectType;

        // Misc info about the object (may not apply to our actual object type)
        public bool         IsAlternate;  // for odd/even coloring
        public bool         IsSelected;
        public bool         IsActiveRow;
        public bool         IsActiveCell;
        public bool         IsCaption;
        public bool         IsCard;
        public bool         IsInEdit;
        public bool         IsRowSelector;
        public bool         IsCellOnly;
		public bool			IsRowPreview;
		public bool			IsGroupByColumn;
		public bool			IsGroupByColumnHeader;

		// SSP 7/22/03 - Fixed headers
		// Added IsFixed that indicates whether the cell/header/column/group is 
		// associated with a fixed header.
		// 
		public bool			IsFixedHeader;
		public bool			IsFixedColumn;

		// SSP 7/24/04 - UltraCalc
		// Added IsFormulaError.
		//
		public bool			IsFormulaError;

		// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
		// Added ResolveDefaultColors.
		// 
		public bool			ResolveDefaultColors;

		// SSP 3/8/06 - App Styling
		// 
		public AppStyling.UIRole Role;
		public AppStyling.ResolutionOrderInfo ResolutionOrder;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="objectType">The type of object being resolved</param>
        /// <param name="requestedProps">Bit flags identifying the appearance properties to be resolved</param>
        public ResolveAppearanceContext( System.Type objectType,
                                         AppearancePropFlags requestedProps )
        {
            this.objectType         = objectType;
            this.UnresolvedProps    = requestedProps;
            this.IsAlternate        = false; 
            this.IsSelected         = false;
            this.IsActiveRow        = false;
            this.IsActiveCell       = false;
            this.IsCaption          = false;
            this.IsCard				= false;
            this.IsInEdit           = false;
            this.IsRowSelector      = false;
            this.IsCellOnly         = false;
			this.IsRowPreview		= false;
			this.IsGroupByColumn	= false;
			this.IsGroupByColumnHeader = false;

			// SSP 7/22/03 - Fixed headers
			// Added IsFixed that indicates whether the cell/header/column/group is 
			// associated with a fixed header.
			// 
			this.IsFixedColumn		= false;
			this.IsFixedHeader		= false;

			// SSP 7/24/04 - UltraCalc
			// Added IsFormulaError.
			//
			this.IsFormulaError		= false;

			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Added ResolveDefaultColors.
			// 
			this.ResolveDefaultColors = true;

			// SSP 3/8/06 - App Styling
			// 
			this.Role = null;
			this.ResolutionOrder = new AppStyling.ResolutionOrderInfo( AppStyling.ResolutionOrder.Default );
        }

        public System.Type ObjectType
        {
            get
            {
                return this.objectType;
            }
        }
    }
}
