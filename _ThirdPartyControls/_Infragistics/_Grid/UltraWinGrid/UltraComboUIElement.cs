#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;


	/// <summary>
	/// The main element for an UltraCombo control (occupies the
	/// entire client area.
	/// </summary>
	public class UltraComboUIElement : ControlUIElementBase 
	{
		// AS 11/11/03 [UltraCombo as Editor]
		// These members are no longer needed.
		//
		//private DropDownButtonUIElement dropDownButtonElement = null;
		//private ComboTextUIElement		textElement = null;
		
				
		internal UltraComboUIElement( UltraCombo combo ) : base( combo, combo )
		{
			this.PrimaryContext    = combo;
		}

		/// <summary>
		/// Return the UltraCombo control
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraCombo Combo
		{
			get
			{
				return (UltraCombo)this.Control;
			}
		}

		 // AS 11/11/03 [UltraCombo as Editor]

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			this.Combo.ResolveAppearance( ref appearance, requestedProps );

			// MRS - NA v6.3 - Office2007
			base.InitAppearance( ref appearance, ref requestedProps );

			 // AS 11/11/03 [UltraCombo as Editor]
		}

		 // AS 11/11/03 [UltraCombo as Editor]

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 6/02/03 UWG2036
				// Commented out the original code below and added new code to support the new
				// DisplayStyle property off the UltraCombo.
				//
				

				 // AS 11/11/03 [UltraCombo as Editor]
				return UIElementBorderStyle.None;
			}
		}

		// SSP 6/02/03 UWG2036
		// Overrode OnMouseEnter and OnMouseLeave.
		//
		#region OnMouseEnter

		/// <summary>
		/// Overridden. Called whenever the mouse enters the element.
		/// </summary>
		protected override void OnMouseEnter( )
		{
			this.DirtyChildElements( );

			base.OnMouseEnter( );
		}

		#endregion // OnMouseEnter

		#region OnMouseLeave

		/// <summary>
		/// Overridden. Called whenever the mouse enters the element.
		/// </summary>
		protected override void OnMouseLeave( )
		{
			this.DirtyChildElements( );

			base.OnMouseLeave( );
		}

		#endregion // OnMouseLeave

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return Border3DSide.All; 
			}
		}

		/// <summary>
		/// Returns true to clip child elements inside borders
		/// </summary>
		protected override bool ClipChildren
		{
			get 
			{
				return true;
			}
		}
		
		 // AS 11/11/03 [UltraCombo as Editor]

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{

			 // AS 11/11/03 [UltraCombo as Editor]

			// get the previous element
			// AS 12/15/03 UWG2797
			// Extract does not check for subclasses - just for the specific
			// type.
			//
			//EmbeddableUIElementBase lastElement = (EmbeddableUIElementBase)UltraComboUIElement.ExtractExistingElement( this.ChildElements, typeof(EmbeddableUIElementBase), true );
			EmbeddableUIElementBase lastElement = (EmbeddableUIElementBase)UltraComboUIElement.ExtractExistingElement( this.ChildElements, this.Combo.InternalEditor.GetEmbeddableElementType(), true );

			// clear the collection
			this.ChildElements.Clear();

			// get the element/have it initialized by the editor
			EmbeddableUIElementBase editorElement = this.Combo.InternalEditor.GetEmbeddableElement( 
				this,					// parent element
				this.Combo.EditorOwner,	// Editor owner
				this.Combo,				// context
				true,					// IncludeEditElements
				false,					// ReserveSpaceForEditElements
				true,					// DrawOuterBorders
				lastElement );			// previous element if there is one

			if (editorElement != null)
			{
				//	Set the element's rect, add it to the child elements collection
				editorElement.Rect = this.RectInsideBorders;
				this.ChildElements.Add( editorElement );
			}
		}

		// AS 11/11/03 TODO
		// Remove?
		/// <summary>
        /// Called when a mouse down message is received. Returning true will cause normal mouse down processing to be skipped
		/// </summary>
        /// <param name="msgInfo">The <see cref="Infragistics.Win.MouseMessageInfo"/> providing mouse information.</param>
        /// <returns>true if the mouse processing has been handled.</returns>
		protected override bool OnPreMouseDown( ref MouseMessageInfo msgInfo )
		{
			

			return false;
		}

		/// <summary>
		/// Overrides the Cursor to return the cursor from the UIElement
		/// </summary>
		public override System.Windows.Forms.Cursor Cursor
		{
			get
			{
				// SSP 1/10/02 UWG702
				// Use the cursor off the Appearance of the UltraCombo not the
				// layout because the layout appearance's cursor is for the drop
				// down part of the combo not for the edit part.
				//
				AppearanceData appData = new AppearanceData();
				this.Combo.ResolveAppearance( ref appData, AppearancePropFlags.Cursor );

				if ( null != appData.Cursor )
					return appData.Cursor;

				return base.Cursor;

				
			}
		}
		
		/// <summary>
		/// 
		/// </summary>
		protected override KeyActionMappingsBase KeyActionMappings
		{ 
			get { return this.Combo.KeyActionMappings; } 
		}

		/// <summary>
        /// The current state.
		/// </summary>
		protected override Int64 CurrentState
		{ 
			get { return (Int64)this.Combo.CurrentState; } 
		}

        /// <summary>
        /// Performs a specific key action
        /// </summary>
        ///	<param name="actionCode">An enumeration value that determines the user action to be performed.</param>
        ///	<param name="ctlKeyDown">A boolean specifies whether the action should be performed as if the control key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is maintained, as it is when the user holds down the control key and selects a row in a grid.</param>
        ///	<param name="shiftKeyDown">A boolean specifies whether the action should be performed as if the shift key is depressed. This mainly affects actions where selection is involved and determines if the existing selection is extended, as it is when the user holds down the shift key and selects a range of rows in a grid.</param>                
        ///	<returns>true if the action completed successfully, false if the action failed.</returns>
        protected override bool PerformKeyAction(Enum actionCode, bool shiftKeyDown, bool ctlKeyDown)
		{ 
			return this.Combo.PerformKeyAction ( actionCode, shiftKeyDown, ctlKeyDown );
		}

		#region UIRole

		// AS 4/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Combo.DisplayLayout, StyleUtils.Role.UltraComboEditPortion );
			}
		}

		#endregion // UIRole

        // MBS 11/9/07 - BR27787
        // Since the editor element is going to take up the entire RectInsideBorders,
        // we should not be drawing the back color here since we'll end up drawing it 
        // twice, resulting in incorrect alpha-blending
        //
        /// <summary>
        /// Handles rendering the background.
        /// </summary>
        /// <param name="drawParams">UIElementDrawParams</param>
        protected override void DrawBackColor(ref UIElementDrawParams drawParams)
        {
        }
	}

	#region Commented Out
	 // AS 11/11/03 [UltraCombo as Editor]
	#endregion //Commented Out
}
