#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
	using System.Collections;
    using System.ComponentModel;
	using System.Globalization;
	using System.Data;
	using System.Windows.Forms;
    using Infragistics.Shared;
    using Infragistics.Shared.Serialization;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Diagnostics;
	using System.Security.Permissions;



    /// <summary>
    /// Contains a flat collection of Band objects, one per hierarchical recordset. 
    /// This object is used internally by UltraGrid for serialization/deserisalization.
    /// </summary> 
	[ TypeConverter( typeof( BandsCollectionConverter ) ) ]
	[ System.ComponentModel.Editor( typeof( BandsCollectionUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
	public class BandsSerializer : BandsCollection
    {
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="layout"></param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public BandsSerializer( UltraGridLayout layout ) : base( layout )
        {
        }

		/// <summary>
		/// For internal use only.
		/// </summary>
        /// <param name="obj">obj</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public int Add ( object obj )
		{
			if ( obj == null )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_22") );

			if ( ! ( obj is UltraGridBand ) )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_23") );

			return this.InternalAdd( obj as UltraGridBand );
		}
	}

    /// <summary>
    /// Returns a flat collection of Band objects, one per hierarchical recordset. 
    /// This property is read-only at run-time.
    /// </summary><remarks><para>The Bands property is used to access the collection 
    /// of Band objects associated with an Layout object or the UltraGrid. Band 
    /// objects are used to display all the data rows from a single level of a data 
    /// hierarchy.</para><para>Each Band object in the collection can be accessed 
    /// by using its Index or Key values. Using the Key value is preferable, because 
    /// the order of an object within the collection (and therefore its Index value) 
    /// may change as objects are added to and removed from the collection.</para></remarks>
	[ TypeConverter( typeof( BandsCollectionConverter ) ) ]
	[ System.ComponentModel.Editor( typeof( BandsCollectionUITypeEditor ), typeof( System.Drawing.Design.UITypeEditor ) ) ]
	[ Serializable() ]
	public class BandsCollection : KeyedSubObjectsCollectionBase,
								   ISerializable
    {
        private     bool    originsDirty = true;
        private     int     extent = 0;
        private		UltraGridLayout  layout;
		private		int		initialCapacity = 5;

        // MRS NAS 9.1 - Sibling Band Order
        //
        #region NAS 9.1 - Sibling Band Order
        // Keeps track of the last time this collection was verified against the 
        // layout.SortedBandsVersion. 
        //
        private int lastGoodSortedBandsVersion = int.MaxValue;
        
        // Indicates if this bands collection is the regular layout.Bands collection (sort == false)
        // or if it is the layout.SortedBands collection (sort == true).
        //
        private bool sort = false;

        // Anti-recursion flag for the VeridySortedBands method. 
        //
        private bool isInVerifySortedBands = false;

        #endregion //NAS 9.1 - Sibling Band Order

        /// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="layout">UltraGridLayout</param>
        // MRS NAS 9.1 - Sibling Band Order
        // Added a new constructor. This one now delgates to the new one. 
        //
        //public BandsCollection( UltraGridLayout layout )        
        //{
        //    this.layout = layout;
        //}
        //
        public BandsCollection(UltraGridLayout layout) : this(layout, false) {}

        // MRS NAS 9.1 - Sibling Band Order
        // Added a new "sort" parameter to the constructor to determine if this is the 
        // regular Bands collection or the SortedBands collection. 
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="layout">UltraGridLayout</param>
        /// <param name="sort">sort</param>
        public BandsCollection(UltraGridLayout layout, bool sort)
        {
            this.sort = sort;
            this.layout = layout;            
        }
 
        /// <summary>
        /// Called when this object is disposed of
        /// </summary>
        protected override void OnDispose() 
        {
            // MRS NAS 9.1 - Sibling Band Order
            // Added 'if' block. 
            // Since the sorted bands collection mirrors the real bands colletion, we 
            // only want to dispose the bands when the "real" collection is disposed.             
            //
            if (this.sort == false)
            {
                // SSP 1/12/05 BR0155 UWG3736
                // Dispose of the bands instead of simply clearing the collection. This way
                // the bands, columns and other subobjects get their Dispose called as well.
                //
                // --------------------------------------------------------------------------
                for (int i = this.Count - 1; i >= 0; i--)
                {
                    UltraGridBand band = this[i];

                    if (null != band)
                        band.Dispose();
                }
                // --------------------------------------------------------------------------
            }

			this.Clear();
        }

		/// <summary>
		/// Returns an empty string
		/// </summary>
        /// <returns>Returns an empty string</returns>
		public override string ToString() { return string.Empty; }

		internal bool HasGroupBySortColumns
		{
			get
			{
				if ( ViewStyleBand.OutlookGroupBy != this.Layout.ViewStyleBand )
					return false;

				// JJD 10/04/01 - UWG353
				// Iterate over the bands collection and return true if any band
				// has a group by column visible
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					// bypass hidden bands
					//
					if ( this[i].HiddenResolved )
						continue;

					if ( this[i].HasGroupBySortColumns )
						return true;
				}				

				return false;
			}
		}

		// SSP 8/13/05 BR04633 BR04666
		// Added an overload of CalculateBandMetrics that takes in pass parameter. We need to
		// perform upto 2 passes in case of autofit and column syncrhonization across bands.
		// 
		internal int CalculateBandMetrics( )
		{
			return this.CalculateBandMetrics( this.Layout.AutoFitAllColumns ? 0 : -1 );
		}
		
		// SSP 8/13/05 BR04633 BR04666
		// Added an overload of CalculateBandMetrics that takes in pass parameter. We need to
		// perform upto 2 passes in case of autofit and column syncrhonization across bands.
		// 
		private int CalculateBandMetrics( int pass )
		{
			int bandsOverallExtent = 0;

			UltraGridBand priorBand = null;

			// SSP 3/31/03 - Row Layout Functionality
			// We want the row-layout designer to be able to design hidden bands.
			// For that to work, then band.GetExtent has to return a valid value.
			// So calculate the band extent even if the band is hidden in design
			// mode and when the row-layout functionality is enabled.
			//
			bool designMode = null != this.layout && 
				null != this.layout.Grid && this.layout.Grid.DesignMode;

			// SSP 10/15/06 BR15686
			// 
			int oldBand0Extent = this.Count > 0 ? this[0].GetExtent( ) : 0;
			
			// Call each band to recalculate its metrics
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridBand band = this[i];

				// SSP 3/31/03 - Row Layout Functionality
				// We want the row-layout designer to be able to design hidden bands.
				// For that to work, then band.GetExtent has to return a valid value.
				// So calculate the band extent even if the band is hidden in design
				// mode and when the row-layout functionality is enabled.
				//
				//if ( band.HiddenResolved )
				if ( band.HiddenResolved && !( designMode && band.UseRowLayoutResolved ) )
					continue;

				// SSP 8/13/05 BR04633 BR04666
				// For pass 1 only calc band metrics if AllowColSizingResolved is Synchronized.
				// This was done so because CalculateBandMetrics will overwrite the auto-fit 
				// extent in row-layout mode.
				// 
				//band.CalculateBandMetrics( priorBand );
				if ( 1 != pass || AllowColSizing.Synchronized == band.AllowColSizingResolved )
					band.CalculateBandMetrics( priorBand );

				priorBand = band;
			}

			// SSP 8/13/05 BR04633 BR04666
			// Added the if block and enclosed the existing code into the else block.
			// 
			if ( 0 == pass )
			{
				if ( null != this.Layout )
					this.Layout.ClearSynchronizedColLists( );
			}
				
				
				
				
			else if ( this.Count > 1 )
			{
				// Call each band to synchronize column widths
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					UltraGridBand band = this[i];

					// JJD 12/27/01
					// Bypass card view bands
					//
					if ( band.HiddenResolved || band.CardView )
						continue;

					band.SynchronizeColWidths();
				}
			}

			// Call each band to update the overall width of any exclusive
			// col scroll regions          
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridBand band = this[i];

				// JJD 12/27/01
				// Bypass card view bands
				//
				// JJD 1/23/02 - UWG971
				// Let card bands thru since we added code in 
				// UpdateExclusizeColRegionWidths to handle them
				//
				if ( band.HiddenResolved )
					continue;

				band.UpdateExclusizeColRegionWidths();
			}

			// JJD 1/02/02
			// Added logic to autofit columns
			//
			// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
			// Commented out the condition because FitColumnsToWidth already checks for 
			// the same condition.
			//
			//if ( this.layout.AutoFitColumns )
			// SSP 8/13/05 BR04633 BR04666
			// 
			// ----------------------------------------------------------------------------------
			//this.layout.ViewStyleImpl.FitColumnsToWidth( );
			if ( -1 == pass )
			{
				this.Layout.ViewStyleImpl.FitColumnsToWidth( );
			}
			else if ( 0 == pass )
			{
				this.Layout.ViewStyleImpl.FitColumnsToWidth( );
				this.Layout.synchronizedColListsDirty = true;
				this.CalculateBandMetrics( 1 );
			}
			// ----------------------------------------------------------------------------------

			// Determine the farthest right (overall extent) any band extends 
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridBand band = this[i];

				if ( band.HiddenResolved )
					continue;

				int right = band.GetOrigin() + band.GetExtent(   );

				if ( right > bandsOverallExtent )
					bandsOverallExtent = right;

				// if this is a single band display exit the for loop
				//
				// SSP 11/24/03
				// Use the ViewStyleIml instead of ViewStyle to check if the view style being used
				// is a single band view style. The reason for this is that for UltraDropDown and
				// UltraCombo, we always use a VewStyleSingle even when ViewStyleBand is set to
				// Multi which it is by default. If this change causes any problem, take it out.
				// I changed it because I noticed it and not because it was causing a problem.
				//
				//if ( ViewStyle.SingleBand == this.Layout.ViewStyle )
				if ( ! this.Layout.ViewStyleImpl.IsMultiBandDisplay )
					break;
			}

			// SSP 10/15/06 BR15686
			// When auto-fit-style is extend-last-column then we have to calculate the band
			// 0 metrics then auto-fit and then resize the drop down according to the extent 
			// of the band based on the auto-fitted column. Therefore the existing call to 
			// ReCalculateDropDownSize from CalculateBandMetrics will result in the wrong
			// drop down size since it would not have yet taken into consideration the 
			// auto-fit-extend-last-column calculated band extent.
			// 
			// ------------------------------------------------------------------------------
			int newBand0Extent = this.Count > 0 ? this[0].GetExtent( ) : 0;
			if ( oldBand0Extent != newBand0Extent )
			{
				UltraDropDownBase udd = this.Layout.Grid as UltraDropDownBase;
				if ( null != udd && 0 != pass )
				{
					udd.ReCalculateDropDownSize( true, false );
				}
			}
			// ------------------------------------------------------------------------------

			// return the overall extent
			//
			return bandsOverallExtent;
		}

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
        protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
        {
			// JAS 5/10/05 BR03829 - Per SSP's recommendation, the grand verify version number is
			// bumped every time this method is invoked.
			//
			if( this.Layout != null )
				this.Layout.BumpGrandVerifyVersion();

            // pass the notification along to our listeners
            //
			if ( propChange.PropId is PropertyIds )
			{
				switch ( (PropertyIds)propChange.PropId )
				{
					case PropertyIds.Columns:
						break;

					// SSP 10/31/01 UWG613
					// Added Indentation case
					//
					case PropertyIds.Indentation:
					// JAS 2005 v2 GroupBy Row Extensions
					case PropertyIds.IndentationGroupByRow:
					case PropertyIds.IndentationGroupByRowExpansionIndicator:
					{
						this.layout.DirtyGridElement( true );

						// SSP 7/24/03
						// Don't pass in true as the first parameter because that will cause initialization
						// of the metrics and possibly lead to premature verification of some dirty state.
						//
						//if ( null != this.layout.GetColScrollRegions( true ) )
						if ( null != this.layout.GetColScrollRegions( false ) )
							this.layout.GetColScrollRegions( false ).DirtyMetrics();
						break;
					}
					// Dirty metrics when changes are made to groups
					//
					case PropertyIds.Groups:
						// RobA UWG475 11/16/01 moved LevelCount to its own 
						// case statement
						//
					//case PropertyIds.LevelCount:
					case PropertyIds.CardView: // JJD 12/26/01 - when card view changes dirty the metrics
					case PropertyIds.Hidden:
					{
						// Set the synchronized col list dirty flag on and
						// dirty the grid
						//
						this.layout.SynchronizedColListsDirty = true;
						this.layout.DirtyGridElement( true );

						// SSP 7/24/03
						// Don't pass in true as the first parameter because that will cause initialization
						// of the metrics and possibly lead to premature verification of some dirty state.
						//
						//if ( null != this.layout.GetColScrollRegions( true ) )
						if ( null != this.layout.GetColScrollRegions( false ) )
							this.layout.GetColScrollRegions( false ).DirtyMetrics();

						// SSP 11/22/04
						// 
						this.layout.BumpScrollableRowCountVersion( );
						
						// SSP 3/30/05 - NAS 5.2 Fixed Rows
						//
						this.layout.BumpFixedRowsCollectionsVersion( );

						// SSP 5/5/06 - App Styling
						// 
						StyleUtils.DirtyCachedPropertyVals( this.layout );

						break;
					}

					// SSP 1/30/03 - Row Layout Functionality
					// Added UseRowLayout entry.
					//
                    // MRS - NAS 9.1 - Groups in RowLayout 
                    // Don't listen for UseRowLayout any more. This is obsolete and 
                    // has been replaced by RowLayoutStyle.
                    //
					//case PropertyIds.UseRowLayout:                                        
                    case PropertyIds.RowLayoutStyle:
					{
						if ( null != this.layout )
						{
							this.layout.BumpCellChildElementsCacheVersion( );
							this.layout.ClearBandRowHeightMetrics( null );

							// Set the synchronized col list dirty flag on and
							// dirty the grid
							//
							this.layout.SynchronizedColListsDirty = true;
							this.layout.DirtyGridElement( true );

							if ( null != this.layout.ColScrollRegions )
								this.layout.ColScrollRegions.DirtyMetrics( );
						}

						break;
					}
					case PropertyIds.LevelCount:
					{
						// RobA UWG475 11/16/01 moved LevelCount to its own 
						// case statement
						//

						this.layout.ClearBandRowHeightMetrics( null );

						// Set the synchronized col list dirty flag on and
						// dirty the grid
						//
						this.layout.SynchronizedColListsDirty = true;
						this.layout.DirtyGridElement( true );

						// SSP 7/24/03
						// Don't pass in true as the first parameter because that will cause initialization
						// of the metrics and possibly lead to premature verification of some dirty state.
						//
						//if ( null != this.layout.GetColScrollRegions( true ) )
						if ( null != this.layout.GetColScrollRegions( false ) )
							this.layout.GetColScrollRegions( false ).DirtyMetrics();
						break;

					}

					
						
					// Dirty visible rows if the col/group headers visible state
					// changes
					case PropertyIds.ColHeadersVisible:
					case PropertyIds.GroupHeadersVisible:
					case PropertyIds.HeaderVisible:
					case PropertyIds.Expandable:	
					case PropertyIds.AutoPreviewEnabled:
					case PropertyIds.AutoPreviewMaxLines:
					case PropertyIds.AutoPreviewField:
					// RobA UWG644 11/15/01
					// added ColHeaderLines and GroupHeaderLines
					case PropertyIds.ColHeaderLines:
					case PropertyIds.GroupHeaderLines:
					// SSP 6/21/02
					// Summary Rows feature.
					// 
					// SSP 8/2/02
					// Moved the SummaryFooterCaptionVisible to the override. However we
					// still need to do this for the summary footer caption.
					//
					//case Infragistics.Win.UltraWinGrid.PropertyIds.SummaryFooterCaptionVisible:
					// SSP 3/30/05 - NAS 5.2 Filter Row
					//
					case Infragistics.Win.UltraWinGrid.PropertyIds.SpecialRowPromptField:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SummaryFooterCaption:
					{
						// SSP 3/24/05 BR02957
						// Cause the row scroll regions to recalc the max scroll pos if the ScrollBounds
						// is set to ScrollToFill. For that simply bump the grand version number.
						//
						this.layout.BumpGrandVerifyVersion( );

						// SSP 3/30/05 - NAS 5.2 Filter Row
						//
						this.layout.BumpCellChildElementsCacheVersion( );

						// If expandable has changed we need to dirty the visible
						// rows
						//
						this.layout.RowScrollRegions.DirtyAllVisibleRows();
						this.layout.DirtyGridElement( true );
						break;
					}
					
					case PropertyIds.AddButtonCaption:
					{
						
						this.layout.DirtyGridElement( false );
						break;
					}

                    // MBS 3/12/08 - RowEditTemplate NA2008 V2
                    case PropertyIds.RowEditTemplate:
                        // Since changing the RowEditTemplate could alter the size of the
                        // RowSelectors, the columns could also get out of sync
                        this.layout.GetColScrollRegions(false).DirtyMetrics();
                        break;

                    // MRS NAS 9.1 - Sibling Band Order                        
                    case PropertyIds.VisiblePosition:  
                        // Bump the sorted bands version.
                        this.BumpSortedBandsVersion();

                        // Dirty the grid element so the bands are re-ordered visually. 
                        this.layout.DirtyGridElement(true);

                        // MRS 1/30/2009 - TFS12874
                        this.layout.Grid.RefreshColumnChoosers();

                        break;

					default:
					{
						this.layout.DirtyGridElement( false );
						break;
					}
				}
			}
			else
			{
				this.layout.DirtyGridElement( false );
			}

			this.NotifyPropChange( PropertyIds.Band, propChange );
        }

		

		/// <summary>
		/// Returns true
		/// </summary>
        public override bool IsReadOnly { get { return true; } }

        
		/// <summary>
		/// indexer 
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridBand this[ int index ] 
        {
            get
            {
                // MRS NAS 9.1 - Sibling Band Order
                // Veryify the collection before accessing the indexer. 
                //
                this.VerifySortedBands();

                return (UltraGridBand)base.GetItem( index );
            }
        }        

        /// <summary>
		/// indexer (by string key)
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridBand this[ String key ] 
        {
            get
            {
                // MRS NAS 9.1 - Sibling Band Order
                // Veryify the collection before accessing the indexer. 
                //
                this.VerifySortedBands();

                return (UltraGridBand)base.GetItem( key );
            }
        }
        
        /// <summary>
        /// Returns true if band origins need to be recalculated
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)   ]
        public bool OriginsDirty
        {
            get 
            {
                return this.originsDirty;
            }
        }

        /// <summary>
        /// Flags the collection so that the band origins
        /// will be recalculated the next time VerifyBandOrigins
        /// is called
        /// </summary>
        public void DirtyOrigins()
        {
            this.originsDirty = true;
        }

		/// <summary>
		/// Returns the Band's Width
		/// </summary>
        public int Extent 
        {
            get
            {
//                this.VerifyBandOrigins();

                return this.extent;
            }
        }

		/// <summary>
		/// <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout"/>
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]			
		public Infragistics.Win.UltraWinGrid.UltraGridLayout Layout
		{
			get 
			{
				return this.layout;
			}
		}
 
        /// <summary>
        /// Abstract property that specifies the initial capacity
        /// of the collection
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)   ]
        protected override int InitialCapacity
        {
            get
            {
				return this.initialCapacity;
            }
		}
        
        /// <summary>
        /// The collection as an array of objects
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        Browsable(false)   ]
        public override object[] All 
        {
			// AS 1/8/03 - fxcop
			// Properties should not be write only - even
			// though we know it wasn't anyway
			get { return base.All; }
            set 
            {
				throw new NotSupportedException();
            }
        }


		// SSP 2/7/02
		// Overrode this newly added virtual property off the KeyedSubObjectsCollectionBase
		// to allow for duplicate keys. Look at UWG1018 test project which has a bands
		// structure with 2 different bands with the same key.
		//
		/// <summary>
		/// Returns true if the collection allows 2 or more items to have the same key value.
		/// </summary>
		/// <remarks>
		/// In BandsCollection, this property is overridden to return true because we do
		/// want to allow different bands with the same keys in the collection.
		/// </remarks>
		public override bool AllowDuplicateKeys
		{
			get
			{
				return true;
			}
		}


		internal Infragistics.Win.UltraWinGrid.UltraGridBand InternalAdd( UltraGridBand parentBand, 
                                  Infragistics.Win.UltraWinGrid.UltraGridColumn parentColumn,
								  Infragistics.Win.UltraWinGrid.UltraGridBand [] oldBands ) 
        {
			bool newBandCreated;
			return this.InternalAdd( parentBand, parentColumn, oldBands, out newBandCreated );
		}

		// SSP 7/23/07 BR24065
		// Added an overload of InternalAdd that takes newBandCreated out parameter.
		// 
		internal Infragistics.Win.UltraWinGrid.UltraGridBand InternalAdd( UltraGridBand parentBand,
								  Infragistics.Win.UltraWinGrid.UltraGridColumn parentColumn,
								  Infragistics.Win.UltraWinGrid.UltraGridBand[] oldBands,
								  out bool newBandCreated )
		{
            // MRS NAS 9.1 - Sibling Band Order
            Debug.Assert(this.sort == false, "Attempting to add a band the sorted bands collection; unexpected. This method should only be used on the Bands collection.");

			// SSP 7/23/07 BR24065
			// Added an overload of InternalAdd that takes newBandCreated out parameter.
			// 
			newBandCreated = false;

            UltraGridBand band = null;

			// JJD 8/27/01
			// If an old band array was passed in check if it contains
			// a band for the parent column. If so use it. This is so we
			// don't create a new band unnecessarily.
			//
			if ( oldBands != null )
			{
				for ( int i = 0; i < oldBands.Length; i++ )
				{
					if ( oldBands[i] != null &&
						 oldBands[i].ParentColumn == parentColumn )
					{
						band = oldBands[i];
						oldBands[i] = null;
						break;
					}
				}
			}
			
			// JJD 8/27/01
			// if we didn't have an old band that matched then create
			// a new one now
			//
			if ( band == null )
			{
				band = new UltraGridBand( this.layout, parentBand, parentColumn );

				// SSP 7/23/07 BR24065
				// Added an overload of InternalAdd that takes newBandCreated out parameter.
				// 
				newBandCreated = true;
			}

            // hook up the prop change notifications
            //
            band.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// SSP 3/10/04 - Virtual Binding Related
			// When a band is added, we need to make sure that we add the band after the 
			// parent band and before parent band's siblings and if parent band doesn't
			// have any siblings then before the parent band's ancestors' siblings. 
			// Basically look at how we form the bands collection in InitColumns method
			// which gets called recursively to create the bands hierarchy. The resulting
			// bands collection is in a format where band and it's descendant are added
			// first and then band's sibling is added and then band's siblings descendants
			// are added and so on.
			//
			// ----------------------------------------------------------------------------
			//this.InternalAdd( band );
			int index = null != parentBand ? this.IndexOf( parentBand ) : -1;
			if ( index >= 0 )
			{
				// Skip the sibling bands. We want to append the band after its sibling.
				//
				index++;
				while ( index < this.Count && this[ index ].IsDescendantOfBand( parentBand ) )
					index++;

				this.InternalInsert( index , band );
			}
			else
			{
				this.InternalAdd( band );
			}

			// Bump the columns version of the parent band so the child bands collection
			// of rows in the parent band know to add an entry in the child bands
			// collection for the added band.
			//
			if ( null != parentBand )
				parentBand.BumpColumnsVersion( );
			// ----------------------------------------------------------------------------

            // MRS NAS 9.1 - Sibling Band Order
            this.BumpSortedBandsVersion();

            return band;

        }

		// SSP 3/8/04 - Virtual Binding
		// If the column is a chaptered column, then delete the associated band from
		// the bands collection as well. Added FindBand method.
		//
		internal UltraGridBand FindBand( UltraGridColumn chapteredColumn )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				if ( this[i].ParentColumn == chapteredColumn )
					return this[i];
			}

			return null;
		}

		// SSP 3/8/04 - Virtual Binding
		// If the column is a chaptered column, then delete the associated band from
		// the bands collection as well. Added InternalRemove method.
		//
		internal void InternalRemove( UltraGridBand band )
		{
			// Unhook from the various binding related events that the band hooks into.
			//
			band.UnWireDataSource();

			// Unhook from the band's SubObjectPropChanged event.
			//
			band.SubObjectPropChanged -= this.SubObjectPropChangeHandler;		

			int index = this.IndexOf( band );
			if ( index >= 0 )
				this.InternalRemoveAt( index );

			// Bump the columns version of the parent band so the child bands collection
			// of rows in the parent band know to add an entry in the child bands
			// collection for the added band.
			//
			if ( null != band && null != band.ParentBand )
				band.ParentBand.BumpColumnsVersion( );

			if ( null != this.Layout )
			{
				this.Layout.BumpGrandVerifyVersion( );
				this.Layout.BumpCellChildElementsCacheVersion( );
				this.Layout.DirtyGridElement( true );
				this.Layout.ColScrollRegions.DirtyMetrics( );
			}

            // MRS NAS 9.1 - Sibling Band Order
            this.BumpSortedBandsVersion();
		}

		internal Infragistics.Win.UltraWinGrid.UltraGridBand [] ClearAllButBandZero()
		{
			Infragistics.Win.UltraWinGrid.UltraGridBand [] oldBands = null;

			if ( this.Count > 1 )
			{
				oldBands = new Infragistics.Win.UltraWinGrid.UltraGridBand [this.Count - 1];

				for ( int i = 0; i < oldBands.Length; i++ )
				{
					oldBands[i] = this[i + 1];

					// JJD 10/10/01 - UWG510
					// Unwire the band from the data source
					//
					oldBands[i].UnWireDataSource();

					// remove the prop change notifications
					//
					oldBands[i].SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				}

				this.List.RemoveRange( 1, this.Count - 1 );
			}

            // MRS NAS 9.1 - Sibling Band Order
            this.BumpSortedBandsVersion();

			return oldBands;
		}

        /// <summary>
        /// Clears the collection
        /// </summary>
        internal void Clear( ) 
        {
			// SSP 1/24/05 BR00269
			// If the bands serializer (layout's loadedBands member var) is being cleared,
			// then don't unwire the bands because the main Layout.Bands are still valid.
			// Enclosed the existing code into the if block below.
			//
			if ( null == this.Layout || this != this.Layout.LoadedBandsInternal )
			{        
				foreach ( UltraGridBand band in this )
				{
					// JJD 10/10/01 - UWG510
					// Unwire the band from the data source
					//
					band.UnWireDataSource();

					// remove the prop change notifications
					//
					band.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				}
			}           

            this.InternalClear();

            // MRS NAS 9.1 - Sibling Band Order
            this.BumpSortedBandsVersion();
        }

		internal void OnBaseFontChanged()
		{
			// Notify each band that the base font has changed
			//
			for ( int i=0; i< this.Count; ++i )
			{
				this[i].ClearCachedHeightValues();
			}
											  
		    // Dirty the metrics when the font has changed
			//
			// Check the flag that lets us know if we are re-initializing
			// all sub items of the layout.
			//
			if ( !this.layout.InitializingAllSubItems )
				// SSP 7/24/03
				// Don't pass in true as the first parameter because that will cause initialization
				// of the metrics and possibly lead to premature verification of some dirty state.
				//
				//this.layout.GetColScrollRegions(true).DirtyMetrics();
				this.layout.GetColScrollRegions( false ).DirtyMetrics();
		}

		internal void ClearDrawCache()
		{
			// JJD 11/06/01
			// Clear each band's cache
			//
			for ( int i=0; i< this.Count; ++i )
			{
				this[i].ClearDrawCache();
			}
		}
     
        
		/// <summary>
		/// IEnumerable Interface Implementation        
		/// </summary>
        /// <returns>A type safe enumerator</returns>
        public BandEnumerator GetEnumerator() // non-IEnumerable version
        {
			return new BandEnumerator(this);
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{

			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			PropertyCategories propCat = context.Context is PropertyCategories
				? (PropertyCategories)context.Context	: PropertyCategories.All;

			if( (propCat & PropertyCategories.Bands) == 0 )
				return;

			//all values that were set are now save into SerializationInfo
			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Count", this.Count );
			//info.AddValue( "Count", this.Count );

			// add each layout in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, i.ToString(), this[i]  );
				//info.AddValue( i.ToString(), this[i] );
			}

			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal BandsCollection( SerializationInfo info, StreamingContext context )
		protected BandsCollection( SerializationInfo info, StreamingContext context )
		{
			PropertyCategories propCat = context.Context is PropertyCategories
				? (PropertyCategories)context.Context	: PropertyCategories.All;

			if ( ( propCat & PropertyCategories.Bands ) == 0 )
				return;
															 
			//this will have to be set in SetLayout()
			this.layout = null;

			//everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( UltraGridBand ) )
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					UltraGridBand item = (UltraGridBand)Utils.DeserializeProperty( entry, typeof(UltraGridBand), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd(item);
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							//this.initialCapacity = Math.Max( this.initialCapacity, (int)entry.Value );
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/17/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.initialCapacity = Math.Max(this.initialCapacity, (int)Utils.DeserializeProperty( entry, typeof(int), this.initialCapacity ));
							//this.initialCapacity = Math.Max(this.initialCapacity, (int)DisposableObject.ConvertValue( entry.Value, typeof(int) ));
							break;
						}

						// JJD 1/31/02
						// De-Serialize the tag property
						//
						case "Tag":
						{
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						}

						default:
						{
							Debug.Assert( false, "Invalid entry in BandsCollection de-serialization ctor" );
							break;
						}				
					}
				}
			}
		}

		internal void InitLayout( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			this.InitLayout( layout, false );
		}

		internal void InitLayout( Infragistics.Win.UltraWinGrid.UltraGridLayout layout,
								  bool calledFromEndInit )
		{
			this.layout =  layout;
			
			for( int i = 0; i < this.Count; i++ )
			{
				// JJD 11/21/01 
				// Added flag so that we know if we are being called from inside
				// ISupportInitialize.EndInit
				//
				this[i].InitLayout( this.layout, calledFromEndInit );

				if ( !calledFromEndInit )
					this[i].SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}
		}

		internal void InitAppearanceHolders()
		{

			for ( int i = 0; i < this.Count; i++ )
			{				
				if ( this[i].HasOverride )
					this[i].Override.InitAppearanceHolders();
			}
		}

		// SSP 6/2/04 UWG3382
		// Added InitializeParentBandPointers method and moved the code in it from
		// the InitializeFrom method.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void InitializeParentBandPointers( BandsCollection source )
		internal static void InitializeParentBandPointers( BandsCollection source )
		{
			for ( int i = 0; i < source.Count; ++i )
			{
				int index = source[i].ParentIndex;

				if ( source[i].ParentBand == null && index != -1 )
					source[i].SetParentBand( source[index] );
			}
		}

		internal void InitializeFrom( BandsCollection source, PropertyCategories categories  )
		{

            // MRS 2/6/2009 - TFS13287
            // We should never be calling InitializeFrom the Sorted Bands collection because 
            // the initialization of the VisiblePosition property might cause might cause the order of the collection to change while we are looping through it.
            Debug.Assert(this.sort == false, "We should never be calling InitializeFrom the Sorted Bands collection because the initialization of the VisiblePosition property might cause might cause the order of the collection to change while we are looping through it.");

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			//ROBA UWG124 8/7/01
			//We need to reset all the parentband pointers
			//
			// SSP 6/2/04 UWG3382
			// Moved the commented out code into the new InitializeParentBandPointers
			// method so we can use it from a different place.
			// 
			// ----------------------------------------------------------------------------
			

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.InitializeParentBandPointers( source );
			BandsCollection.InitializeParentBandPointers( source );

			// ----------------------------------------------------------------------------

			// if we are attached to the main layout we can't add or remove bands 
			// since the rowset dictates the band structure
			//
			// SSP 5/7/03 - Export Functionality
			// Also check for the export layout.
			//
			//if ( this.Layout.IsDisplayLayout || this.Layout.IsPrintLayout )
			if ( this.Layout.IsDisplayLayout || this.Layout.IsPrintLayout || this.Layout.IsExportLayout )
			{
				// get band 0 from us and the source
				//
				UltraGridBand band0 = this.Count > 0 ? this[0] : null;
				UltraGridBand sourceBand0 = source.Count > 0 ? source[0] : null;

				// if the band is a logical match then call the recursive
				// InitBandWithChildren method.
				//
				// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
				// 
				// ----------------------------------------------------------------------------
				
				if ( band0 != null && sourceBand0 != null )
				{
					if ( band0.CanInitializeFrom( sourceBand0 ) )
					{
						InitBandWithChildren( source, sourceBand0, band0, categories );
					}
					// If bands didn't match and we weren't able to call InitBandWithChildren above
					// then we have to hide all the columns in the band if the NewColumnLoadStyle 
					// dictates that we hide columns by default.
					//
					else if ( this.Layout.IsApplyingLayout 
						&& ( NewColumnLoadStyle.Show != this.Layout.NewColumnLoadStyleInEffect 
							 || NewBandLoadStyle.Show != this.Layout.NewBandLoadStyleInEffect ) )
					{
						if ( band0.HasColumnsBeenAllocated )
						{
							for ( int i = 0; i < band0.Columns.Count; i++ )
							{
								UltraGridColumn column = band0.Columns[ i ];
								if ( null != column )
								{
									if ( NewColumnLoadStyle.Hide == this.Layout.NewColumnLoadStyleInEffect )
										column.Hidden = true;
								}
							}

							for ( int i = 0; i < this.Count; i++ )
							{
								UltraGridBand band = this[i];
								if ( null != band && band != band0 && NewBandLoadStyle.Hide == this.Layout.NewBandLoadStyleInEffect )
									band.Hidden = true;
							}
						}
					}
				}
				// ----------------------------------------------------------------------------

				return;
			}

			// we are not attached to the main layout so first
			// clear our current collection and then copy over
			// the source's structure 
			//
			this.Clear();

			// if there are no bands on the source just return
			//
			if ( source.Count < 1 )
				return;

			// Call the recursive CopyBandWithChildren method passing
			// in the source collections band 0.
			//
			CopyBandWithChildren( source, source[0], null, categories );  // null because band 0 does not have a parent
		}

		internal object Clone()
		{
            // MRS NAS 9.1 - Sibling Band Order
            // I can't see why we would ever try to clone the sorted columns. 
            //
            Debug.Assert(this.sort == false, "Cloning the SortedColumns collection; unexpected.");
            
			BandsCollection bandCol = new BandsCollection( this.layout );            
			
			bandCol.InitializeFrom( this, PropertyCategories.All );
			return bandCol;
		}

		internal void CopyBandWithChildren(	BandsCollection sourceBands, 
											UltraGridBand source,
											UltraGridBand parentBand,
											PropertyCategories categories )
		{
			UltraGridColumn parentColumn = null;

			// SSP 9/4/02 UWG1289
			// Also check for source' parent column being null. We don't want throw
			// a NullReferenceException below. This could happen if the columns
			// collection is not serialized along with the parent band so there 
			// would be no parent column since none of the columns were serialized.
			//
			//if ( parentBand != null )
			if ( parentBand != null && null != source.ParentColumn )
			{
				// get the index of the parent column from the source's
				// parent band's columns collection
				//
				int nParentBandColIndex = source.ParentColumn.Index;
        
				// Since the GetParentBandColIndex above returned a column 
				// ordinal we need decrement it (becuase ordinal is one based)
				//
				nParentBandColIndex--;

				if ( nParentBandColIndex >= 0 )
				{
					// get the coresponding column in out parent bands 
					// columns collection
					//
					parentColumn = parentBand.Columns[nParentBandColIndex];
					Debug.Assert( parentColumn != null, "ParentBandCol not found in CopyBandWithChildren");
				}
				else
				{
					Debug.WriteLine("nParentBandColIndex < 0 in CopyBandWithChildren");
				}
			}

			UltraGridBand newBand = new UltraGridBand(this.layout, parentBand, parentColumn);

			// SSP 7/29/03 UWG2508
			// Add the newBand first and then call initialize from instead of calling the
			// initialize from before adding the band. The reason for doing this is that
			// InitializeFrom may end up firing events or executing code that leads to
			// UltraGridLayout.Rows property to get accessed. That property relies on
			// the Bands collection to have at least one column (that's how it creates
			// the rows collection). Instead of modifying the Rows property to return 
			// null if the collection has 0 bands, modified this code which adds the band
			// first ensuring that there is a band when InitializeFrom is called.
			//
			// --------------------------------------------------------------------------
			
			// insert the band into the collection
			//
			//hr = _Insert( -1, pNewBand, false );
			this.InternalAdd( newBand );

			// init the target from the source
			//
			newBand.InitializeFrom( source, categories );
			// --------------------------------------------------------------------------


			// Needed to reset.
			
			
			// loop thru the source bands collection looking for direct
			// children of the passed in source
			//
			for ( int index = 0; index < sourceBands.Count; index++ )
			{
				if ( sourceBands[index].ParentBand == source )
				{
					// Call this function recursively to add the child bands
					//
					// JJD 7/20/2001
					// Pass in sourceBands[index] as the source for this child band
					//
//					CopyBandWithChildren( sourceBands, source, newBand, categories );
					CopyBandWithChildren( sourceBands,  sourceBands[index], newBand, categories );
				}
			}
		}

		internal void InitBandWithChildren( BandsCollection sourceBands, 
											UltraGridBand source, UltraGridBand target,
											PropertyCategories categories )
		{
			// init the target from the source
			//
			target.InitializeFrom( source, categories );
		    
			// use nested iterators to look for direct child bands that
			// match in the source and target collections
			//
			for ( int index = 0; index < this.Count; index++ )
			{
				// First make sure the target band is a direct child
				// if the passed in target
				//
				if ( this[index].ParentBand == target )
				{
					for ( int i = 0; i < sourceBands.Count; i++ )
					{
						// Make sure the source band is a direct child of
						// the passed in source and then call the target
						// band's CanInitializeFrom method to complete the
						// match check
						//
						if ( sourceBands[i].ParentBand == source && this[index].CanInitializeFrom( sourceBands[i] ) )
						{
							// the band's match logically so call this function recursively
							//
							// JJD 7/20/2001
							// Pass in sourceBands[index] as the source for this child band
							//
							//this.InitBandWithChildren ( sourceBands, source, target, categories );
							this.InitBandWithChildren ( sourceBands, sourceBands[i], this[index], categories );
							break;
						}
					}
				}
			}
		}

		// SSP 6/24/04 UWG2937
		// Added an overload of InternalCopyFromBandsSerializer that takes in 
		// calledFromEndInit parameter.
		//
		internal void InternalCopyFromBandsSerializer( BandsSerializer source, bool clearSource )
		{
			this.InternalCopyFromBandsSerializer( source, clearSource, false );
		}

		// SSP 7/1/03 UWG2443
		// Added InternalCopyFromBandsSerializer method.
		//
		// SSP 5/25/04 - Extract Data Structure Related
		// Added clearSource parameter.
		//
		//internal void InternalCopyFromBandsSerializer( BandsSerializer source )
		// SSP 6/24/04 UWG2937
		// Added calledFromEndInit parameter.
		//
		//internal void InternalCopyFromBandsSerializer( BandsSerializer source, bool clearSource )
		internal void InternalCopyFromBandsSerializer( BandsSerializer source, bool clearSource, bool calledFromEndInit )
		{
			if ( null != source )
			{
				// SSP 8/3/04 UWG3545
				// If all bands were added in the get of BandsSerializer then return without 
				// doing anything.
				//
				// ----------------------------------------------------------------------------
				int i;
				bool allSourceBandsAddedInBandsSerializer = true;
				for ( i = 0; allSourceBandsAddedInBandsSerializer && i < source.Count; i++ )
					allSourceBandsAddedInBandsSerializer = source[i].addedInBandsSerializer;

				if ( allSourceBandsAddedInBandsSerializer )
					return;
				// ----------------------------------------------------------------------------

				this.Clear( );

				while ( source.Count > 0 && source[0].addedInBandsSerializer )
				{
					source.InternalRemoveAt( 0 );
				}

				for ( i = 0; i < source.Count; i++ )
				{
					if ( null != source[i] )
					{
						this.InternalAdd( source[i] );

						// SSP 6/24/04 UWG2937
						// Added calledFromEndInit parameter.
						//
						//source[i].InitLayout( this.layout, false );
						source[i].InitLayout( this.layout, calledFromEndInit );

						// SSP 6/3/04
						// Also hook into the band.
						//
						source[i].SubObjectPropChanged -= this.SubObjectPropChangeHandler;
						source[i].SubObjectPropChanged += this.SubObjectPropChangeHandler;
					}
				}

				// SSP 5/25/04 - Extract Data Structure Related
				// Added clearSource parameter. Only clear if clearSource parameter is true.
				// Added if condition to the already existing Clear call.
				//
				if ( clearSource )
					source.Clear( );
			}
		}


		// SSP 3/18/04 - Virtual Binding Related
		// If a chaptered column gets removed, also remove the associated band.
		//
		internal UltraGridBand GetChildBand( UltraGridColumn chapteredColumn )
		{
			UltraGridBand childBand = null;
			for ( int i = 0; null == childBand && i < this.Count; i++ )
			{
				if ( chapteredColumn == this[i].ParentColumn )
					childBand = this[i];
			}

			return childBand;
		}

        // MRS NAS 9.1 - Sibling Band Order
        #region NAS 9.1 - Sibling Band Order

        #region BumpSortedBandsVersion

        private void BumpSortedBandsVersion()
        {
            if (this.layout != null)
                this.layout.BumpSortedBandsVersion();
        }
        #endregion //BumpSortedBandsVersion

        #region BandSortComparer class

        // MRS 3/24/2009 - TFS15238
        // Re-wrote this whole SortComparer class to keep the children directly under their parent
        // bands. 
        //
        #region Old Code
//#if DEBUG
//        /// <summary>
//        /// Sorted the bands in the list based on their position in the grid and the 
//        /// VisiblePosition property of the band. 
//        /// </summary>
//#endif
//        private class BandSortComparer : IComparer
//        {
//            #region IComparer Members

//            int IComparer.Compare(object x, object y)
//            {
//                // I don't know why we are getting nulls in here when there are no 
//                // nulls on the list.
//                // Always assume null is less than non-null so all nulls are last.  
//                //
//                if (x == null)
//                    return y == null ? 0 : 1;
//                else
//                    if (y == null) return -1;

//                UltraGridBand bandX = (UltraGridBand)x;
//                UltraGridBand bandY = (UltraGridBand)y;                

//                // If bandX has a parent and it's the same parent as bandY, 
//                // compare the VisiblePositions. 
//                if (bandX.ParentBand != null &&
//                    bandX.ParentBand == bandY.ParentBand)
//                {
//                    int result = bandX.VisiblePosition.CompareTo(bandY.VisiblePosition);

//                    // If the VisiblePosition is not 0, use it. If it is 0, we want to 
//                    // fall back to the old behavior and use the band Index. 
//                    if (result != 0)
//                        return result;
//                }

//                // MRS 2/4/2009 - TFS13015
//                int depthX = BandSortComparer.GetBandDepth(bandX);
//                int depthY = BandSortComparer.GetBandDepth(bandY);

//                if (depthX != depthY)
//                    return depthX.CompareTo(depthY);

//                // Do what we always did before and use the band Index. This is the index
//                // of the band in the Bands collection, so it's the original index as 
//                // provided by the BindingManager, not the Index into the SortedBands. 
//                //
//                return bandX.Index.CompareTo(bandY.Index);
//            }

//            #endregion

//            private static int GetBandDepth(UltraGridBand band)
//            {
//                int depth = 0;
//                UltraGridBand parentBand = band.ParentBand;
//                while (parentBand != null)
//                {
//                    depth++;
//                    parentBand = parentBand.ParentBand;
//                }

//                return depth;

//            }
        //        }
        #endregion // Old Code
        //
        private class BandSortComparer : IComparer
        {
            #region IComparer Members

            int IComparer.Compare(object x, object y)
            {
                return this.Compare((UltraGridBand)x, (UltraGridBand)y);
            }

            int Compare(UltraGridBand bandX, UltraGridBand bandY)
            {
                if (bandX == bandY)
                    return 0;

                if (null == bandX)
                    return -1;

                if (null == bandY)
                    return 1;

                if (bandX.IsRootBand)
                    return -1;

                if (bandY.IsRootBand)
                    return 1;

                int result;

                int xDepth = bandX.HierarchicalLevel;
                int yDepth = bandY.HierarchicalLevel;

                // There's no way to compare items that are at different depths. So call the
                // Compare method recursively using the parent of the lower item until we
                // get to a point where the items are at the same depth.
                if (xDepth < yDepth)
                {
                    result = this.Compare(bandX, bandY.ParentBand);
                    if (0 != result)
                        return result;

                    return -1;
                }                
                else if (xDepth > yDepth)
                {
                    result = this.Compare(bandX.ParentBand, bandY);
                    if (0 != result)
                        return result;

                    return 1;
                }

                // If we reach this point, we know that the bands are at the same depth. 
                // So compare their parent bands. 
                // A result of 0 here indicates that the items are both under the same parent
                // and we should move on to comparing the VisiblePositions. 
                result = this.Compare(bandX.ParentBand, bandY.ParentBand);
                if (0 != result)
                    return result;

                Debug.Assert(bandX.ParentBand == bandY.ParentBand);
                result = bandX.VisiblePosition.CompareTo(bandY.VisiblePosition);
                if (0 != result)
                    return result;

                // Do what we always did before and use the band Index. This is the index
                // of the band in the Bands collection, so it's the original index as 
                // provided by the BindingManager, not the Index into the SortedBands. 
                //
                return bandX.Index.CompareTo(bandY.Index);
            }

            #endregion            
        }
        #endregion //BandSortComparer class

        #region Count
        /// <summary>
        /// Returns the number of items in the collection
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]        
        public override int Count
        {
            get
            {
                // Override the count so that we can verify the sorted bands collection
                // whenever someone asks for the count. Note that the Enumerator uses the 
                // count, so verifying here means we don't need to verify inside GetEnumerator. 
                //
                this.VerifySortedBands();

                return base.Count;
            }
        }
        #endregion //Count

        #region VerifySortedBands
        
        private void VerifySortedBands()
        {
            // Anti-recursion
            if (this.isInVerifySortedBands)
                return;

            // If this is not the SortedBands collection, do nothing. 
            if (this.sort == false)
                return;

            // If the layout is null do nothing. 
            if (this.layout == null)
                return;            

            // If the version numbers match, do nothing. 
            if (this.lastGoodSortedBandsVersion == this.layout.sortedBandsVersion)
                return;            

            // Get the "real" bands collection and store them in a member variable. 
            // We must reference the Bands property before we set the anti-recursion 
            // flag below.
            // If we do not, then the Bands collection will get created by the reference
            // to it's enumerator later on and will end up out of synch. 
            //
            BandsCollection bands = this.layout.Bands;

            // Anti-recursion flag. 
            this.isInVerifySortedBands = true;

            try
            {
                // Clear the internal list. We are using the list here so as not
                // to cause any version numbers to get bumped. Also, the InternalAdd
                // method hooks events on the Band, and we don't need to do that, since
                // they are already hooked by the "real" Bands collection. 
                this.List.Clear();

                // Loop through all the "real" bands and add them to the SortedBands
                // List. Again, we use the internal List here so as not to bump any versions
                // numbers or hook any events. 
                foreach (UltraGridBand band in bands)
                {
                    this.List.Add(band);
                }
                                
                // Sort the bands. this will sort the bands by index, this maintaining
                // the original order, unless the VisiblePosition on a band is set.                  
                // MRS 1/30/2009 - TFS13015
                //this.List.Sort(new BandSortComparer());
                Infragistics.Win.Utilities.SortMerge(this.List, new BandSortComparer());

                // Update the version number.
                this.lastGoodSortedBandsVersion = this.layout.sortedBandsVersion;
            }
            finally
            {
                // Re-set the anti-recursion flag back to false. 
                this.isInVerifySortedBands = false;
            }
        }
        #endregion //VerifySortedBands        

        #endregion NAS 9.1 - Sibling Band Order        

        // CDS 7/07/09 - TFS17996
        #region DirtyCachedContainsCheckboxOnTopOrBottom

        internal void DirtyCachedContainsCheckboxOnTopOrBottom()
        {
            for (int i = 0; i < this.Count; i++)
                this[i].DirtyCachedContainsCheckboxOnTopOrBottom();
        }

        #endregion DirtyCachedContainsCheckboxOnTopOrBottom

    }// end of class


    /// <summary>
    /// Enumerator for the BandsCollection
    /// </summary>
    public class BandEnumerator: DisposableObjectEnumeratorBase
    {   
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="bands">BandsCollection</param>
        public BandEnumerator( BandsCollection bands ) : base( bands )
        {
        }

       	/// <summary>
		/// Type-safe version of Current
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridBand Current 
        {
            get
            {
                return (UltraGridBand)((IEnumerator)this).Current;
            }
        }
    }
}
