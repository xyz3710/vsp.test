#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Diagnostics;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// The element that appears above the vertical 
    /// scrollbar that is used for spliiting RowScrollRegions
    /// regions.
    /// </summary>
    public class RowSplitBoxUIElement : SplitterUIElement
    {

        private RowScrollRegionsCollection rowScrollRegions;
		internal RowSplitBoxUIElement( UIElement parent,
                                     RowScrollRegionsCollection rowScrollRegions )
            : base( parent, false )
        {			
            this.rowScrollRegions = rowScrollRegions;
        }

		// SSP 1/5/04 UWG1606
		// Added appearances for the splitter bars.
		//
        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			UltraGridLayout layout = null != this.rowScrollRegions ? this.rowScrollRegions.Layout : null;
			if ( null != layout )
			{
				// SSP 3/13/06 - App Styling
				// 
				// ----------------------------------------------------------------------------------------------------
				//if ( null != layout && layout.GetHasAppearance( UltraGridLayout.LayoutAppearanceIndex.SplitterBarHorizontalAppearance ) )
				//	layout.SplitterBarHorizontalAppearance.MergeData( ref appearance, ref requestedProps );	
				layout.MergeLayoutAppearances( ref appearance, ref requestedProps, 
					UltraGridLayout.LayoutAppearanceIndex.SplitterBarHorizontalAppearance, 
					StyleUtils.Role.RowScrollRegionSplitBox, AppStyling.RoleState.Normal );
				// ----------------------------------------------------------------------------------------------------
			}
		}

        /// <summary>
        /// Flags indicating which sides of the element to
        /// draw borders.
        /// </summary>
		public override System.Windows.Forms.Border3DSide BorderSides
		{
			get
			{
                return Border3DSide.All;
            }
        }
 
		// SSP 3/28/06 - App Styling
		// Overrode BorderStyle.
		// 
        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{				
				UltraGridLayout layout = null != this.rowScrollRegions ? this.rowScrollRegions.Layout : null;

				if ( null == layout )
					return base.BorderStyle;
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( layout, StyleUtils.CachedProperty.BorderStyleSplitBox, out val ) )
				{
					val = StyleUtils.CacheBorderStylePropertyValue( layout,
						StyleUtils.CachedProperty.BorderStyleSplitBox,
						StyleUtils.Role.ScrollRegionSplitBox, 
						UIElementBorderStyle.Default,
						base.BorderStyle );
				}

				return (UIElementBorderStyle)val;
			}
		}

        /// <summary>
        /// Called after a move/resize operation. 
        /// </summary>
        /// <param name="delta">The delta</param>
        public override void ApplyAdjustment(Point delta)
        {
            // call OnSplitBoxDrop if the splitbox was moved at least
            // its own height down
            //
            if ( delta.Y > this.Rect.Height )
			{
				// adjust the point from relative to the splitbox
				// to relative to the client area of the control
				//
				delta.Offset( this.Rect.Left, this.Rect.Top );
                
				this.rowScrollRegions.OnSplitBoxDrop( delta );
				
			}
        }


		// SSP 8/16/01 UWG133
		// We need to exit the edit mode when user pressed the mouse on the
		// split box for splitting the region
        /// <summary>
        /// Called when the mouse down message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown( MouseEventArgs e, 
										 bool adjustableArea,
										 ref UIElement captureMouseForElement )
		{
			bool ret = base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );

			if ( null != this.rowScrollRegions )
			{
				// exit edit mode when user clicks on the split box
				if ( null != this.rowScrollRegions.Layout.ActiveCell )
				{				
					this.rowScrollRegions.Layout.ActiveCell.ExitEditMode();
				}
			}

			return ret;
		}

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.rowScrollRegions.Layout, StyleUtils.Role.RowScrollRegionSplitBox );
			}
		}

		#endregion // UIRole
    }
}
