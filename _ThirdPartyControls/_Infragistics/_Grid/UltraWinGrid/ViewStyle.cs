#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
	using System.Windows.Forms;
	using System.Diagnostics;
	using System.Collections;
	using System.Collections.Generic;

	#region ViewStyleBase Class

    /// <summary>
    /// Abstarct bas class for all ViewStyle classes.
    /// </summary>
	internal abstract class ViewStyleBase
	{
		#region Variables

		// SSP 9/19/02 UWG1637
		// VerifyRowList calls AdjustListToFillRegion which pops the last row from the visible rows 
		// collection and calls AddSiblingRows again to restart filling of the region from that
		// row. This however causes problems with VisibleRowContext's headerBands member gets
		// out of sync with the visible rows collection as a result of popping the last visible row.
		// The side effect is that the header for the last visible row that was popped doesn't show
		// up. So added lastVisibleRowHadHeaders so that the OrientVisibleRowsForward knows that
		// we are in this situation so it can compensate for it.
		// The reason the variable is added to the ViewStyle base class rather than the ViewStyleHorizontal
		// is that the AdjustListToFillRegion is in ViewStyle class (this class) and didn't want to
		// duplicate the code code in AdjustListToFillRegion.
		//
		internal VisibleRow lastVisibleRowHadHeaders = null;

		private Infragistics.Win.UltraWinGrid.UltraGridLayout layout;

        // MBS 4/23/09 - TFS12665
        // Keep track of when we're recreating the entire row list so we can make some optimizations
        private bool isInRecreateRowList;

		#endregion // Variables

		#region Constructor

		internal ViewStyleBase( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			this.layout = layout;
		}

		#endregion // Constructor

		#region Layout

		internal Infragistics.Win.UltraWinGrid.UltraGridLayout Layout
		{
			get
			{
				return this.layout;
			}
		}

		#endregion // Layout

		#region FitColumnsToWidth

		internal virtual void FitColumnsToWidth()
		{
			// JJD 1/02/02
			// Added logic to autofit columns
			//
			if ( 
				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// Also check for extend last column.
				//
				//this.layout.AutoFitAllColumns &&
				( this.layout.AutoFitAllColumns || this.layout.AutoFitExtendLastColumn ) &&
				// MRS 8/15/05 - BR05633
				// The fix for BR04975 (below) caused BR05633. 
				// Check to make sure either Grid is created OR the ControlForGridDisplay is created.
				//
//				 this.layout.Grid != null	&&
//				// MRS 7/18/05 - BR04975
//				//Should be looking at the Display grid here, not the control. 
//				//this.layout.Grid.Created )
//				this.layout.Grid.ControlForGridDisplay != null &&
//				this.layout.Grid.ControlForGridDisplay.Created )
				this.layout.Grid != null &&
				(this.layout.Grid.Created || 
				(this.layout.Grid.ControlForGridDisplay != null &&
				this.layout.Grid.ControlForGridDisplay.Created)))
			{
				System.Drawing.Rectangle clientRect = this.layout.Grid.ClientRectangle;
								
				// MRS 6/10/04 - UWG2968 - BEGIN
				// If this is a print layout, then the width of the client rect
				// should be the width of the page.
				if ( this.layout.IsPrintLayout &&
					this.layout.Grid is UltraGrid)
				{
					PrintManager printManager = ((UltraGrid)this.layout.Grid).PrintManager;
					if ( printManager != null )
					{ 
						int pages = printManager.LogicalPageLayoutInfoHolder.FitWidthToPages;
						if ( pages > 0 )
						{
							double bandsOverallWidth = this.layout.BandsOverallExtent;

							// size of the physical page.
							//
							Size physicalPage = printManager.PhysicalPageMarginBoundsSize;

							// get the total width of all the pages (physical size of page * num pages)
							//
							double totalWidthOfPages;

							totalWidthOfPages =  physicalPage.Width * pages;

                            // CDS 3/16/09 TFS15339
                            // If the bandsOverallWidth is less or equal to the totalWidthOfPages, 
                            // the grid can fit on the page so set the clientRect's to the totalWidthOfPages.
                            //if (bandsOverallWidth < (int)totalWidthOfPages)
                            if (bandsOverallWidth <= (int)totalWidthOfPages)
								clientRect.Width = (int)totalWidthOfPages;

                            // MRS 6/16/2009 - TFS18464                            
                            // If we are using ExtendLastColumn, and FitToPages, then we need to use
                            // the Total Page Width as the client rect so that the last column
                            // is extented to fill the page. The size of the on-screen grid is irrelevant
                            // in this case. 
                            //
                            if (this.layout.AutoFitExtendLastColumn)
                                clientRect.Width = (int)totalWidthOfPages;
						}
					}					
				}
				// MRS 6/10/04 - UWG2968 - END

				// SSP 10/27/03 UWG1808
				// If the scroll bar is not visible, then autofit all the way to the right edge of
				// the grid. The logic for re-auto-fitting when the scrollbar's visible state changes
				// is in DataAreaUIElement.
				//
				// ------------------------------------------------------------------------------------
				bool hasScrollBar = false;
				// SSP 10/20/05 BR07161
				// When printing we never display the scrollbars in print layouts. Enclosed the
				// existing code in the if block.
				// 
				if ( ! this.layout.IsPrintLayout )
				{
					for ( int i = 0; i < this.layout.RowScrollRegions.Count; i++ )
					{
						RowScrollRegion rsr = this.layout.RowScrollRegions[i];
						// SSP 11/4/04 UWG3593
						// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
						// that specifies whether to assume and if so to whether to assume the scrollbar
						// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
						// even when they are not needed because we assume the horizontal scrollbar is 
						// visible to find out if vertical scrollbar should be displayed or not.
						//
						//if ( null != rsr && rsr.WillScrollbarBeShown( false ) )
						if ( null != rsr && rsr.WillScrollbarBeShown( ScrollbarVisibility.Check ) )
							hasScrollBar = true;
					}

					// SSP 10/28/05 BR07267
					// Also update the haDDDDVerticalScrollbar flag which we use to determine when
					// the vertical scrollbar visibility state changes.
					// 
					if ( this.layout.IsDisplayLayout )
					{
						DataAreaUIElement dataAreaElem = this.layout.DataAreaElement;
						if ( null != dataAreaElem )
							dataAreaElem.haDDDDVerticalScrollbar = hasScrollBar;
					}
				}
				// ------------------------------------------------------------------------------------

				// SSP 7/31/03 UWG2550
				// If the grid is a drop down then use the DropDownWidth property settings of the
				// drop down.
				//
				// ------------------------------------------------------------------------------
				if ( this.layout.Grid is UltraDropDownBase )
				{
					UltraDropDownBase dropDown = (UltraDropDownBase)this.layout.Grid;

					int dropDownWidth = dropDown.DropDownWidth;

					if ( dropDownWidth > 0 )
					{
						// Add border and scrollbar width.
						//
						// SSP 10/27/03 UWG1808
						// If the scroll bar is not visible, then autofit all the way to the right edge of
						// the grid. The logic for re-auto-fitting when the scrollbar's visible state changes
						// is in DataAreaUIElement.
						//
						//dropDownWidth += SystemInformation.VerticalScrollBarWidth
						// MRS 5/19/05 - BR04103
						// We should not be adding anything here
//						dropDownWidth += ( hasScrollBar ? SystemInformation.VerticalScrollBarWidth : 0 )
//							+ this.layout.GetBorderThickness( this.layout.BorderStyleResolved );

						clientRect.Width = dropDownWidth;
					}					
				}
				// ------------------------------------------------------------------------------

				if ( !clientRect.IsEmpty )
				{
					int right = clientRect.Right 
						// SSP 10/27/03 UWG1808
						// If the scroll bar is not visible, then autofit all the way to the right edge of
						// the grid. The logic for re-auto-fitting when the scrollbar's visible state changes
						// is in DataAreaUIElement.
						//
						//- SystemInformation.VerticalScrollBarWidth
						- ( hasScrollBar ? SystemInformation.VerticalScrollBarWidth : 0 )
						// SSP 6/1/05 BR03774
						// Right should be relative to the row scroll region which does not
						// include the left border of the grid.
						//
						//- this.layout.GetBorderThickness( this.layout.BorderStyleResolved );
						- 2 * this.layout.GetBorderThickness( this.layout.BorderStyleResolved );

					if ( right > 0 )
					{
						// Call each band to resize its columns to fit
						// in the allotted space       
						//
						for ( int i = 0; i < this.layout.SortedBands.Count; i++ )
						{
							UltraGridBand band = this.layout.SortedBands[i];

							// JJD 12/27/01
							// Bypass card view or hidden bands
							// 
							if ( band.HiddenResolved || band.CardView )
								continue;

							band.FitColumnsToWidth( right );
						}
					}
				}
			}
		}

		#endregion FitColumnsToWidth

		#region HasFixedHeaders 
		
		// SSP 7/18/05 - NAS 5.3 Header Placement
		// Made HasFixedHeaders non-virtual. Instead added BandHasFixedHeaders method that takes in
		// the band context.
		// 
		//internal virtual bool HasFixedHeaders 
		internal bool HasFixedHeaders
		{ 
			get
			{
				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
				
				return this.BandHasFixedHeaders( null );
			}
		}

		// SSP 7/18/05 - NAS 5.3 Header Placement
		// 
		internal virtual bool BandHasFixedHeaders( UltraGridBand band )
		{ 
			// JJD 12/13/01
			// If band zero displays in cardview then we don't need to show column headers
			//
			if ( this.layout.SortedBands.Count < 1 || 
				this.layout.SortedBands[0].CardView )
				return false;

			return true; 
		}

		#endregion // HasFixedHeaders 
		
		#region IsMultiBandDisplay 
		
		internal virtual bool IsMultiBandDisplay 
		{ 
			get
			{
				return false; 
			}
		}

		#endregion // IsMultiBandDisplay 

		#region IsOutlookGroupBy 

		internal virtual bool IsOutlookGroupBy 
		{ 
			get
			{
				return false; 
			}
		}

		#endregion // IsOutlookGroupBy 

		#region HasMultiRowTiers 
		
		internal virtual bool HasMultiRowTiers 
		{ 
			get
			{
				return false; 
			}
		}

		#endregion // HasMultiRowTiers 

		#region GetFixedHeaderBands
		
		internal virtual void GetFixedHeaderBands( VisibleRowsCollection visibleRows,
			UltraGridBand [] headerBands,
			ref int headerBandCount )
		{
			// init the return count
			//
			headerBandCount = 0;

			if ( this.HasFixedHeaders )
			{
				if ( this.layout.SortedBands.Count > 0 )
				{
					headerBandCount = 1;
					headerBands[0]  = this.layout.SortedBands[0];
				}
			}

		}
		
		#endregion // GetFixedHeaderBands

		#region GetFixedHeaderHeight

		internal virtual int GetFixedHeaderHeight( ) 
		{
			return this.GetFixedHeaderHeight( true );
		}

		internal virtual int GetFixedHeaderHeight( bool includeOverlappedBorders ) 
		{
			int headerHeight = 0;
			
			if ( this.HasFixedHeaders )
			{
				BandsCollection bands = this.layout.SortedBands;
				if ( null != bands )
				{
					for ( int i = 0; i < bands.Count; i++ )
					{
						UltraGridBand band = bands[i];

						// if the band isn't hidden then aggregate its header height
						//
						// SSP 7/18/05 - NAS 5.3 Header Placement
						// 
						//if ( !band.Hidden )
						if ( !band.Hidden && this.BandHasFixedHeaders( band ) )
						{
							// SSP 7/18/05 - NAS 5.3 Header Placement
							// Now the vertical band can also have fixed headers however they differ from
							// the horizontal band in that in vertical the headers are stacked vertically
							// where as in the horizontal they are stacked horizontally.
							// 
							// ----------------------------------------------------------------------------
							//headerHeight = System.Math.Max( headerHeight, band.GetTotalHeaderHeight( includeOverlappedBorders ) );
							int totalHeaderHeight = band.GetTotalHeaderHeight( includeOverlappedBorders );
							if ( this.HasMultiRowTiers )
								headerHeight = System.Math.Max( headerHeight, totalHeaderHeight );
							else
								headerHeight += totalHeaderHeight;
							// ----------------------------------------------------------------------------
						}

						// if this is a single band display we need to stop after
						// processing the first band
						//
						if ( !this.IsMultiBandDisplay )
							break;
					}
				}
			}

			return headerHeight; 
		}

		#endregion // GetFixedHeaderHeight

		#region GetLastDescendantRow
		
		internal UltraGridRow GetLastDescendantRow( UltraGridRow parentRow )
		{
			//RobA 10/26/01 implemented


			UltraGridRow descendantRow = null;

			if ( parentRow != null && parentRow.IsExpanded )
			{
				// iterate over the bands collection 
				// looking for the last child row (of the last child band )
				//
				for ( int i =0; i < this.Layout.SortedBands.Count; ++i )
				{
					UltraGridBand band = this.Layout.SortedBands[i];

					if ( band.Hidden )
						continue;

					// check to see if this band is a direct child
					// of the parent row's band
					//
					// SSP 4/11/02 UWG1082
					// If the parentRow is a group by row, then it's child rows also
					// have the same band. So if the parent row is a group by row, then
					// use it's band, otherwise use the child band.
					//
					//if ( parentRow.Band == band.ParentBand )
					// MD 8/7/07 - 7.3 Performance
					// FxCop - Do not cast unnecessarily
					//if ( (  ( parentRow is UltraGridGroupByRow ) && parentRow.Band == band ) ||
					//     ( !( parentRow is UltraGridGroupByRow ) && parentRow.Band == band.ParentBand ) )
					bool parentRowIsGroupByRow = parentRow is UltraGridGroupByRow;

					if ( ( parentRowIsGroupByRow && parentRow.Band == band ) ||
						 ( parentRowIsGroupByRow == false && parentRow.Band == band.ParentBand ) )
					{
						UltraGridRow lastChildRow = null;
						
						// get its last child row in this band (if any)
						//
						// SSP 11/11/03 Add Row Feature
						//
						//lastChildRow = parentRow.GetChild( ChildRow.Last, band );
						lastChildRow = parentRow.GetChild( ChildRow.Last, band, IncludeRowTypes.SpecialRows );

						if ( lastChildRow != null )
						{
							UltraGridRow lastVisibleChildRow = null;

							// Since the last child row may be hidden call GetPrevVisibleRelative
							// from that row which will return itself if not hidden. 
							// Otherwise, it will read backwards until it finds a visible
							// row in the same band.
							//
							lastVisibleChildRow = lastChildRow.GetPrevVisibleRelative( false, false, true );

							if ( lastVisibleChildRow != null )
							{
								// we found the last child row in this band
								// so call this function recursively to get its
								// last descendant row
								//
								descendantRow = this.GetLastDescendantRow( lastVisibleChildRow );

								// if this row doesn't have a descendant 
								// return lastChildRow as the last descendant
								//
								if ( descendantRow == null )
									descendantRow = lastVisibleChildRow;

							}
						}
					}
				}
			}
			
			return descendantRow;
		}

		#endregion // GetLastDescendantRow

		// SSP 8/17/04 - UltraCalc
		// Added OnAfterRowListCreated and OnAfterHeaderListCreated methods.
		//
		#region OnAfterRowListCreated
		
		// SSP 12/21/04 BR01302
		// Added OnAfterRowListCreated_ProcessBand method and moved the code from OnAfterRowListCreated
		// into here so the card area can call it when it gets scrolled.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void OnAfterRowListCreated_ProcessBand( UltraGridBand band )
		internal static void OnAfterRowListCreated_ProcessBand( UltraGridBand band )
		{
			Infragistics.Win.CalcEngine.IUltraCalcManager calcManager = null != band.Layout ? band.Layout.CalcManager : null;

			// SSP 11/18/05 BR07794
			// Check for calc notifications being suspended.
			// 
			//if ( null == calcManager )
			if ( null == calcManager || band.Layout.CalcManagerNotificationsSuspended )
				return;

			foreach ( UltraGridColumn col in band.Columns )
			{
				if ( null != col 
					&& null != col.CalcReference 
					&& col.CalcReference.RecalcDeferred
					&& col.CalcReference.HasFormula
					&& ! col.CalcReference.HasFormulaSyntaxError )
				{
					calcManager.RowsCollectionVisibilityChanged( col.CalcReference );
				}
			}

			foreach ( SummarySettings summary in band.Summaries )
			{
				if ( null != summary
					&& null != summary.CalcReference 
					&& summary.CalcReference.RecalcDeferred
					&& summary.CalcReference.HasFormula
					&& ! summary.CalcReference.HasFormulaSyntaxError )
				{
					calcManager.RowsCollectionVisibilityChanged( summary.CalcReference );
				}
			}
		}
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void OnAfterRowListCreated( RowScrollRegion rsr )
		internal static void OnAfterRowListCreated( RowScrollRegion rsr )
		{
			VisibleRowsCollection vrColl = null != rsr ? rsr.VisibleRows : null;
			if ( null != vrColl && null != rsr.Layout && ! rsr.inGetMaxScrollPosition
				&& ! rsr.Layout.CalcManagerNotificationsSuspended )
			{
				Infragistics.Win.CalcEngine.IUltraCalcManager calcManager = rsr.Layout.CalcManager;
				if ( null != calcManager )
				{
					foreach ( UltraGridBand band in rsr.Layout.SortedBands )
						if ( null != band.InternalSortedColumns 
								&& band.InternalSortedColumns.inProcessingNewSortedColumns )
							return;

					bool[] processedBands = new bool[ rsr.Layout.SortedBands.Count ];

					foreach ( VisibleRow vr in vrColl )
					{
						UltraGridRow row = vr.Row;
						UltraGridBand band = null != row ? row.Band : null;
						int bandIndex = null != band ? band.Index : -1;

						if ( bandIndex >= 0 && bandIndex < processedBands.Length
							&& ! processedBands[ bandIndex ]
							&& null != band.Columns )
						{
							// SSP 12/21/04 BR01302
							// Moved the code into the new OnAfterRowListCreated_ProcessBand method so the
							// card area can call it when it gets scrolled.
							//
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Mark members as static
							//this.OnAfterRowListCreated_ProcessBand( band );
							ViewStyleBase.OnAfterRowListCreated_ProcessBand( band );

							processedBands[ bandIndex ] = true;
						}
					}
				}
			}
		}

		#endregion // OnAfterRowListCreated

		// MD 7/26/07 - 7.3 Performance
		// This method does nothing
		#region Removed

		//        #region OnAfterHeaderListCreated

		//#if DEBUG
		//        /// <summary>
		//        /// Called after visible headers are regenerated for a col scroll region. This 
		//        /// method is called from the end of the RecreateHeaderList method.
		//        /// </summary>
		//        /// <param name="csr"></param>
		//#endif
		//        internal void OnAfterHeaderListCreated( ColScrollRegion csr )
		//        {
		//        }

		//        #endregion // OnAfterHeaderListCreated

		#endregion Removed

		#region RecreateHeaderList

		internal abstract bool RecreateHeaderList( ColScrollRegion csr );

		#endregion // RecreateHeaderList
        
		#region RecreateRowList

		internal virtual void RecreateRowList( RowScrollRegion rsr ) 
		{
            bool wasInRecreateRowList = this.isInRecreateRowList;
            try
            {
                // MBS 4/23/09 - TFS12665
                // For efficiency, we should cache some of the more expensive property getters while
                // we reposition all of the rows.  Also keep track of whether we're this process
                this.isInRecreateRowList = true;
                if(this.layout != null)
                    this.layout.BeginCaching();

                // first clear the visible rows collection. This will
                // actually cache the cleared visible rows so that
                // they can be re-used.
                //
                rsr.VisibleRows.InternalClear();

                UltraGridRow firstRow = rsr.FirstRow;

                // walk up the parent chain looking for the first 
                // visible band. This is to cover a situation where
                // the view style is changed from multi to single
                // band and our top row is in a child band.
                //
                while (firstRow != null)
                {
                    // MD 8/2/07 - 7.3 Performance
                    // Prevent calling expensive getters multiple times
                    //if ( firstRow.ParentRow == null )
                    UltraGridRow parentRow = firstRow.ParentRow;

                    if (parentRow == null)
                        break;

                    // SSP 3/28/02
                    // Added following block of code for row filtering feature. 
                    // We are now purposedly adding hidden visible rows associated
                    // with hidden rows because once all the rows in a rows collection 
                    // get filtered out, we still want to show the header for that rows
                    // collection so that the user can unfilter them.
                    // Added following block of code.
                    //
                    //-------------------------------------------------------------------
                    bool rowHiddenResolvedFlag = firstRow.HiddenResolved;

                    if (rowHiddenResolvedFlag && this.ShouldApplySpecialRowFiltersCode())
                    {
                        bool discard;
                        if (!this.VerifyShouldRemoveHiddenRow(rsr, firstRow, out discard))
                            rowHiddenResolvedFlag = false;
                    }
                    //-------------------------------------------------------------------

                    // JJD 8/28/01
                    // Only check for ancetors for multiband displays
                    //
                    // SSP 3/28/02
                    // For row filter feature. Look at the note above.
                    //
                    //if ( this.IsMultiBandDisplay && !firstRow.HiddenResolved )
                    if (this.IsMultiBandDisplay && !rowHiddenResolvedFlag)
                    {
                        // Use AllAncestorsExpanded to walk up the parent chain to
                        // make sure that all ancestor rows are expanded
                        //
                        if (firstRow.AllAncestorsExpanded)
                        {
                            // if this view style has multi row tiers 
                            // (horizontal view) and this row has a 
                            // parent row then check if the row has
                            // any previous cross band siblings
                            //
                            // MD 8/2/07 - 7.3 Performance
                            // Prevent calling expensive getters multiple times
                            //if ( this.HasMultiRowTiers && firstRow.ParentRow != null )
                            if (this.HasMultiRowTiers && parentRow != null)
                            {
                                // if it has cross band siblings then it can be the 
                                // first row. Otherwise, continue up the parent 
                                // chain
                                //
                                // SSP 11/11/03 Add Row Feature
                                //
                                //if ( firstRow.HasPrevSibling( false, true ) ||
                                if (firstRow.HasPrevSibling(false, true, IncludeRowTypes.SpecialRows) ||
                                    firstRow.HasSiblingInPrevSiblingBand(true))
                                    break;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }

                    // if the parent is a group by row, then break out of the loop
                    //
                    // MD 8/2/07 - 7.3 Performance
                    // Prevent calling expensive getters multiple times
                    //if ( firstRow.ParentRow is UltraGridGroupByRow ) 
                    if (parentRow is UltraGridGroupByRow)
                        break;

                    // MD 8/2/07 - 7.3 Performance
                    // Prevent calling expensive getters multiple times
                    //firstRow = firstRow.ParentRow;
                    firstRow = parentRow;
                }

                if (null != firstRow)
                {
                    if (!firstRow.HiddenResolved)
                        rsr.SetFirstRow(firstRow);

                    VisibleRowFetchRowContext context = new VisibleRowFetchRowContext(rsr);

                    context.beginAtRow = firstRow;

                    // SSP 8/3/06 BR14784
                    // Added Using_CreateRowsList_FixedRowsFeature. Code in there was moved from here.
                    // Commented out the existing code and added new code. Note this shouldn't change the behavior.
                    // The logic needed to be used some other place so it was moved into a helper property.
                    // 
                    // ------------------------------------------------------------------------------------------------
                    if (this.Using_CreateRowsList_FixedRowsFeature)
                        this.CreateRowsList_FixedRowsFeature(ref context, null, null);
                    else
                        this.AddSiblingRows(ref context, null, null);
                    
                    // ------------------------------------------------------------------------------------------------
                }

                // SSP 8/17/04 - UltraCalc
                // Added OnAfterRowListCreated and OnAfterHeaderListCreated methods. Call
                // OnAfterRowListCreated  after the visible rows are regenerated.
                //
                // ----------------------------------------------------------------------------
                // MD 7/26/07 - 7.3 Performance
                // FxCop - Mark members as static
                //this.OnAfterRowListCreated( rsr );
                ViewStyleBase.OnAfterRowListCreated(rsr);
                // ----------------------------------------------------------------------------
            }
            finally
            {
                // MBS 4/23/09 - TFS12665
                // Make sure that we end the caching process so that later if some of the properties that we
                // have cached change, we don't have to worry about keeping this in sync for less-timely operations
                this.isInRecreateRowList = wasInRecreateRowList;
                if (this.layout != null)
                    this.layout.EndCaching();
            }
		}

		#endregion // RecreateRowList

		#region VerifyRowList

		internal virtual void VerifyRowList( RowScrollRegion rsr )
		{
			// SSP 4/28/05 - NAS 5.2 Fixed Rows_
			// 
			if ( this.SupportsFixedRows )
			{
				this.RecreateRowList( rsr );
				return;
			}

			// set up the context structure
			//
			VisibleRowFetchRowContext context = 
				new VisibleRowFetchRowContext( rsr );   // forward fetch

			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Got rid of row scroll region parameter since context already contains 
			// the row scroll region.
			//
			//if ( !this.VerifyExistingRows( rsr, ref context ) )		
			if ( !this.VerifyExistingRows( ref context ) )		
				return;

			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Got rid of row scroll region parameter since context already contains 
			// the row scroll region.
			//
			//this.AdjustListToFillRegion( rsr, ref context );
			this.AdjustListToFillRegion( ref context );
		}

		#endregion // VerifyRowList

		#region CalculateGroupByIndent

		// SSP 7/19/05 - NAS 5.3 Header Placement
		// Added CalcGroupByConnectorsExtent method.
		// 
		internal int CalcGroupByConnectorsExtent( UltraGridBand band )
		{
			// Get the group-by rows indent.
			//
			int indent = this.CalculateGroupByIndent( band );

			if ( indent > 0 )
			{
				// Add the pre row area extent of the band.
				// 
				indent += band.PreRowAreaExtent;

				// CalculateGroupByIndent method call above returns a value that has 
				// PreRowAreaExtent of the parent band.
				// 
				if ( null != band.ParentBand )
					indent -= band.ParentBand.PreRowAreaExtent;
			}

			return indent;
		}

		// JJD 1/23/02 - UWG971
		// Moved groupby indent logic into helper method
		//
		internal int CalculateGroupByIndent( UltraGridBand band )
		{
			int groupByIndent = 0;

			if ( band.HasGroupBySortColumns )
			{
				// JAS 2005 v2 GroupBy Row Extensions
				//
//				groupByIndent = ( 1 + band.SortedColumns.LastGroupByColIndex ) * 
//								( GroupByRowUIElement.GROUP_BY_INDENT );
				groupByIndent = ( 1 + band.SortedColumns.LastGroupByColIndex ) * 
								( band.IndentationGroupByRowResolved );

				if ( band.PreRowAreaExtent > 0 )
				{
					// JJD 1/24/02 - UWG971
					// Check whether expansion indicators will be diapleyed
					// before adjusting for them.
					//
					bool showExpansionIndicator = true;

					switch ( band.ExpansionIndicatorResolved )
					{
						case ShowExpansionIndicator.Never:
							showExpansionIndicator = false;
							break;
						case ShowExpansionIndicator.Default:
							showExpansionIndicator = this.IsMultiBandDisplay;
							break;
					}

					if ( showExpansionIndicator )
					{
						// SSP 11/27/01 UWG771
						// Use the same calculations used in positioning the expansion indicator.
						// ( This fixes a problem when Band.Indentation property is set.
						//
						//groupByIndent -= Math.Max( 0, ( band.PreRowAreaExtent - UltraGridBand.EXPANSION_INDICATOR_WIDTH ) / 2 );
						groupByIndent -= ( UltraGridBand.PRE_ROW_SELECTOR_WIDTH / 2 ) - ( UltraGridBand.EXPANSION_INDICATOR_WIDTH / 2 );
					}
				}

				if ( null != band.ParentBand )
					groupByIndent += band.ParentBand.PreRowAreaExtent;
			}

			return groupByIndent;
		}

		#endregion // CalculateGroupByIndent

		#region AddBandHeaders

		/// <summary>
		/// Appends headers from the band into the VisibleHeaders
		/// collection of the ColScrollRegion
		/// </summary>
		protected bool AddBandHeaders( ColScrollRegion csr, UltraGridBand band )
		{
			if ( band.HiddenResolved )
				return false;

			bool headerAdded    = false;
			int position        = csr.Position;
			int origin;

			// SSP 10/30/03 UWG2734
			// Take into account the vertical scrollbar.
			//
			// --------------------------------------------------------------------------
			int csrExtent = csr.Extent;
			// SSP 12/4/03 UWG2765 UWG2734
			// Only substract the vertical scrollbar width if this is the last visibel
			// csr and all the row scroll regions have scrollbar visible.
			//
			//if ( csr.WillScrollbarBeShown( ) )
			//	csrExtent = csrExtent - SystemInformation.VerticalScrollBarWidth;
			if ( csr.IsLastVisibleRegion )
			{
				RowScrollRegionsCollection rsrColl = null != this.Layout ? this.Layout.RowScrollRegions : null;
				bool allRSRsHaveVerticalScrollbar = null != rsrColl && rsrColl.Count > 0;
				for ( int i = 0; allRSRsHaveVerticalScrollbar && i < rsrColl.Count; i++ )
					allRSRsHaveVerticalScrollbar = rsrColl[i].WillScrollbarBeShown( );

				if ( allRSRsHaveVerticalScrollbar )
					csrExtent = csrExtent - SystemInformation.VerticalScrollBarWidth;
			}
			// --------------------------------------------------------------------------

			if ( band.HiddenResolved )
				return false;
			
			// SSP 5/23/03 - Fixed headers
			//
			bool usingFixedHeaders = band.UseFixedHeadersResolved( csr );

			// SSP 11/23/04 - Row Layout - Merged Cell Feature
			// Added logic for row-layout mode. Note: Cell, header etc. positioning logic 
			// doesn't make use of VisibleHeaders. Merged cells functionality needs to make
			// use of it.
			//
			//if ( ! usingFixedHeaders )
			if ( ! usingFixedHeaders && ! band.UseRowLayoutResolved )
			{
				// JJD 1/23/02 - UWG971
				// For exclusive regions gets the band's origin from the region
				//
				if ( csr.IsExclusive )
					origin = csr.GetBandOrigin( band );
				else
					origin = band.GetOrigin( BandOrigin.RowSelector );

				foreach ( HeaderBase header in band.OrderedHeaders )
				{
					if ( header.Hidden )
						continue;

					// JJD 11/30/01 - UWG798 && UWG799
					// Filter out exclusive headers
					//
					if ( csr.IsExclusive )
					{
						if ( header.ExclusiveColScrollRegion != csr.ExclusiveColScrollRegion )
							continue;
					}
					else
					{
						if ( header.ExclusiveColScrollRegion != null )
							continue;
					}

					// Check to see if we are past the extent of the
					// scrolling region. If so break out of for loop
					//
					// SSP 10/30/03 UWG2734
					// Take into account the vertical scrollbar.
					//
					//if ( origin > position + csr.Extent )
					if ( origin > position + csrExtent )
						break;

					// SSP 7/2/07 BR24380 
					// Take into account the fact that the last header could have been extended by the
					// auto-fit-extend-last-column functionality.
					// 
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//int headerExtent = this.GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( header );
					int headerExtent = ViewStyleBase.GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( header );

					// if this header is in the visible area then
					// add it to the visible headers collection
					//
					// SSP 7/2/07 BR24380 
					// 
					//if ( origin + header.Extent >= position )
					if ( origin + headerExtent >= position )
					{
						csr.VisibleHeaders.AddVisibleHeaderFromCache( header, origin );					
						headerAdded = true;
					}

					// add this headers extent to the origin
					//
					// SSP 7/2/07 BR24380 
					// 
					//origin += header.Extent;
					origin += headerExtent;
				}

				return headerAdded;
			}
			else
			{			
				bool horizontalViewStyle = this.HasMultiRowTiers;
			
				// SSP 6/11/03 - Fixed headers
				// Added code to get the band origin using the helper method in horizontal view style since
				// in horizontal view style, a band's origin depends on the parent band's origin and extent.
				// Added the if block and enclosed the already existing code in the else block.
				//			
				// SSP 11/23/04 - Row Layout - Merged Cell Feature
				// Added logic for row-layout mode. Note: Cell, header etc. positioning logic 
				// doesn't make use of VisibleHeaders. Merged cells functionality needs to make
				// use of it.
				//
				//if ( horizontalViewStyle )
				if ( horizontalViewStyle && usingFixedHeaders )
				{
					int tmpScrollPosition = position;
					int tmpExtent;
					UltraGridBand.GetBandOriginExtent_HorizontalViewStyle( band, csr, ref tmpScrollPosition, out origin, out tmpExtent );
				}
				else
				{
					// JJD 1/23/02 - UWG971
					// For exclusive regions gets the band's origin from the region
					//
					origin = band.GetOrigin( BandOrigin.RowSelector );
				}

				// MD 12/9/08 - 9.1 - Column Pinning Right
				// Renamed for clarity
				//int fixedHeadersRight = origin;
				int leftFixedHeadersRightEdge = origin;

				bool nonFixedHeaderEncountered = false;

				// SSP 11/23/04 - Row Layout - Merged Cell Feature
				// Added logic for row-layout mode. Note: Cell, header etc. positioning logic 
				// doesn't make use of VisibleHeaders. Merged cells functionality needs to make
				// use of it.
				//
				// ------------------------------------------------------------------------------
				if ( band.UseRowLayoutResolved )
				{
					HeadersCollection headers = band.LayoutOrderedVisibleColumnHeadersVertical;
					Debug.Assert( null != headers );
					if ( null != headers )
					{
						int fixedHeadersOriginDelta = band.GetFixedHeaders_OriginDelta( csr );
						int bandOrigin = origin + fixedHeadersOriginDelta - position;

						foreach ( HeaderBase header in headers )
						{
							int headerOrigin = bandOrigin + header.Origin;

							// In fixed headers mode if the header is not fixed then substract the 
							// scrollbar position from the header origin.
							//
							if ( usingFixedHeaders && ! band.IsHeaderFixed( csr, header ) )
								headerOrigin -= position;

							int headerRight = headerOrigin + header.Extent;

							// Add the header if the column's going to be visible. In autofit mode all the
							// columns are visible.
							//
							if ( headerRight > 0 && headerRight > bandOrigin && headerOrigin < csrExtent 
								|| band.Layout.AutoFitAllColumns )
							{
								csr.VisibleHeaders.AddVisibleHeaderFromCache( header, headerOrigin );
								headerAdded = true;
							}
						}
					}
				
					return headerAdded;
				}
				// ------------------------------------------------------------------------------

				foreach ( HeaderBase header in band.OrderedHeaders )
				{
					if ( header.Hidden )
						continue;

					// SSP 5/23/03 - Fixed headers
					//
					// MD 12/9/08 - 9.1 - Column Pinning Right
					// Cache this method call's value so we don't have to call it multiple times.
					bool isHeaderFixed = band.IsHeaderFixed( csr, header );

					// MD 12/9/08 - 9.1 - Column Pinning Right
					// Use the cached value for whether the header is fixed. Also, removed the check for nonFixedHeaderEncountered. 
					// It was not needed here before because the assert checks for it above anyway and now we need to get in here for
					// right-fixed headers as well.
					//if ( ! nonFixedHeaderEncountered && band.IsHeaderFixed( csr, header ) )
					if ( isHeaderFixed )
					{
						// MD 12/9/08 - 9.1 - Column Pinning Right
						// For right-fixed headers, we need to do something a little different.
						if ( header.FixOnRightResolved )
						{
							int headerExtent = ViewStyleBase.GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( header );
							csr.VisibleHeaders.AddVisibleHeaderFromCache( header, origin );
							headerAdded = true;

							origin += headerExtent;
						}
						// MD 12/9/08 - 9.1 - Column Pinning Right
						// Wrapped the left-fixed header code in an else block.
						else
						{
						// Check to see if we are past the extent of the
						// scrolling region. If so break out of for loop
						//
						// SSP 7/14/03
						//
						//if ( origin > csr.Extent + band.GetFixedHeaders_OriginDelta( csr ) )
						// SSP 10/30/03 UWG2734
						// Take into account the vertical scrollbar.
						//
						//if ( origin - position + band.GetFixedHeaders_OriginDelta( csr ) > csr.Extent )
						if ( origin - position + band.GetFixedHeaders_OriginDelta( csr ) > csrExtent )
							break;

						csr.VisibleHeaders.AddVisibleHeaderFromCache( header, origin );
						headerAdded = true;

						// add this headers extent to the origin
						//
						// SSP 7/2/07 BR24380 
						// Take into account the fact that the last header could have been extended by the
						// auto-fit-extend-last-column functionality.
						// 
						//origin += header.Extent;
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//int headerExtent = this.GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( header );
						int headerExtent = ViewStyleBase.GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( header );

						origin += headerExtent;

						leftFixedHeadersRightEdge = origin;
						}
					}
					else
					{
						nonFixedHeaderEncountered = true;

						// Check to see if we are past the extent of the
						// scrolling region. If so break out of for loop
						//
						// SSP 7/14/03
						//
						//if ( origin > csr.Extent + band.GetFixedHeaders_OriginDelta( csr ) )
						// SSP 10/30/03 UWG2734
						// Take into account the vertical scrollbar.
						//
						//if ( origin - position > csr.Extent )
						// MD 12/9/08 - 9.1 - Column Pinning Right
						// We can't break out here because the origin must be kept updated for the right-fixed headers which will be added later.
						//if ( origin - position > csrExtent )
						//    break;

						// SSP 7/2/07 BR24380 
						// Take into account the fact that the last header could have been extended by the
						// auto-fit-extend-last-column functionality.
						// 
						//int headerExtent = header.Extent;
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//int headerExtent = this.GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( header );
						int headerExtent = ViewStyleBase.GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( header );

						// if this header is in the visible area then
						// add it to the visible headers collection
						//
						//if ( origin + headerExtent >= fixedHeadersOrigin + band.GetFixedHeaders_OriginDelta( csr ) )
						if ( origin - position + headerExtent > leftFixedHeadersRightEdge - position + band.GetFixedHeaders_OriginDelta( csr )  )
						{
							csr.VisibleHeaders.AddVisibleHeaderFromCache( header, origin );
							headerAdded = true;
						}

						// add this headers extent to the origin
						//
						origin += headerExtent;
					}
				}

				return headerAdded;
			}
		}

		#region GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout

		// SSP 7/2/07 BR24380 
		// 
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private int GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( HeaderBase header )
		private static int GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout( HeaderBase header )
		{
			int headerExtent = header.Extent;

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( header is ColumnHeader )
			//    headerExtent += ( (ColumnHeader)header ).Column.AutoFitExtendLastColumn_WidthDelta;
			ColumnHeader columnHeader = header as ColumnHeader;

			if ( columnHeader != null )
				headerExtent += columnHeader.Column.AutoFitExtendLastColumn_WidthDelta;

			return headerExtent;
		}

		#endregion // GetHeaderExtent_AutoFitExtendLastColumn_NonRowLayout

		#endregion // AddBandHeaders

		#region AdjustBandOrigin

		internal virtual void AdjustBandOrigin( UltraGridBand band, 
			UltraGridBand priorBand,
			ref int origin )
		{
			origin = 0;
		}

		#endregion // AdjustBandOrigin

		#region AdjustDescendantBandOrigins
		
		internal virtual void AdjustDescendantBandOrigins( UltraGridBand band, int adjustment ) { }

		#endregion // AdjustDescendantBandOrigins

		#region AdjustExclusiveMetrics
		
		internal virtual void AdjustExclusiveMetrics( Infragistics.Win.UltraWinGrid.ColScrollRegion csr,
			                                            HeaderBase header,
			                                            HeaderBase priorHeader)
		{
			int nOrigin = 0;
		    
			if ( null != priorHeader )
				nOrigin = priorHeader.Extent +  priorHeader.Origin;

			this.SetExclusiveMetrics(header, nOrigin);		    
		}

		#endregion // AdjustExclusiveMetrics
		
		#region SetExclusiveMetrics
		
		internal virtual void SetExclusiveMetrics( HeaderBase header, int origin )
		{
			// JJD 1/23/02 - UWG971
			// For the fist header in an exclusive region offset the header by 
			// the prerowarea extent
			//
			if ( header.ExclusiveColScrollRegion != null &&
				 header.FirstItem )
				origin += header.Band.PreRowAreaExtent;

			header.SetOrigin(origin);
		}

		#endregion // SetExclusiveMetrics

		#region IsFirstRow
		
		internal virtual bool IsFirstRow( VisibleRow visibleRow )
		{
			// for the row to be the first row it must be in band 0 (no parent row)
			// and it can't have any previous sibling rows
			//
			// SSP 11/11/03 Add Row Feature
			//
			//return (null == visibleRow.Row.ParentRow && !visibleRow.Row.HasPrevSibling() );
			return (null == visibleRow.Row.ParentRow && !visibleRow.Row.HasPrevSibling( false, IncludeRowTypes.SpecialRows ) );
		}

		#endregion // IsFirstRow

		#region OnScrollTop
		
		private void OnScrollTop( ref ScrollRowsContext scrollContext )
		{	
			VisibleRowsCollection visibleRows = scrollContext.VisibleRows;

			// SSP 4/28/05 - NAS 5.2 Fixed Rows_
			// VisibleRow.IsFirstRow and VisibleRow.IsAbsoluteFirstRow( ) and ViewStyleBase.IsFirstRow( )
			// all do the same. Just use the IsAbsoluteFirstRow instead.
			//
			//if ( visibleRows.Count > 0 && this.IsFirstRow( visibleRows[0] ) )
			if ( visibleRows.Count > 0 && visibleRows[0].IsAbsoluteFirstRow )
			{
				return;
			}

			UltraGridRow topRow = null;

			// Call the grid's GetFirstLastRowHelper method to get the first
			// row since passing NULL into SetFirstRow caused it to use the
			// current row position instead of the top row
			//
			if ( null != this.Layout )
			{
				// RobA UWG843 12/6/01
				// commented out line was correct, this is all we need to do
				//
				// SSP 5/19/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
				// We need to also take into account the filter row, summary row etc...
				//
				//topRow = this.Layout.Grid.GetFirstLastRowHelper( true );
				topRow = this.Layout.Rows.GetFirstVisibleRow( );

				//if ( this.layout.ActiveRow != null )
				//	topRow = this.Layout.Grid.DisplayLayout.GetFirstRowInBand ( true, this.Layout.Grid.ActiveRow, false, false );
				// if an ActiveRow is not set just use the first row
				//
				//else
				//	topRow = this.Layout.Rows[0];
			}
				

			// Null out first row cache. This wll cause use to start at the first
			// row when we call RecreateRowList below
			//			
			scrollContext.rowScrollRegion.SetFirstRow( topRow );

			// completely recreate the row list
			//
			this.RecreateRowList( scrollContext.rowScrollRegion );

			// invalidate the display
			//
			scrollContext.invalidate     = true;
			scrollContext.scrollDelta    = 0;
		}

		#endregion // OnScrollTop

		#region CanScrollDownMore

		/// <summary>
		/// Returns true if the region can be scrolled down more.
		/// </summary>
		/// <param name="scrollContext"></param>
		/// <param name="oldTopHeaderBand"></param>
		/// <returns></returns>
		protected bool CanScrollDownMore( ref ScrollRowsContext scrollContext, UltraGridBand oldTopHeaderBand )
		{
			int temp = 0;
			return this.CanScrollDownMore( ref scrollContext, oldTopHeaderBand, ref temp );
		}

		#endregion // CanScrollDownMore

		#region CanScrollDownMore  

		/// <summary>
		/// Returns true if the region can be scrolled down more.
		/// </summary>
		/// <param name="scrollContext"></param>
		/// <param name="oldTopHeaderBand"></param>
		/// <param name="newTopRowCutoff"></param>
		/// <returns></returns>
		protected bool CanScrollDownMore( ref ScrollRowsContext scrollContext,
			UltraGridBand oldTopHeaderBand,
			ref int newTopRowCutoff)
		{			
			VisibleRowsCollection visibleRows = scrollContext.VisibleRows;
			
			if ( visibleRows.Count < 1 )
				return false;

			// SSP 11/24/03 - Scrolling Till Last Row Visible
			// Added the functionality to not allow the user to scroll any further once the
			// last row is visible. 
			// 
			// ----------------------------------------------------------------------------------
			// SSP 5/12/05 - NAS 5.2 Fixed Rows
			// Added ScrollBoundsResolved. If there are fixed rows at the bottom then
			// always resolve the scroll bounds to ScrollToFill. Added ScrollBoundsResolved
			// property.
			//
			//if ( null != this.Layout && ScrollBounds.ScrollToFill == this.Layout.ScrollBounds )
			if ( null != this.Layout && ScrollBounds.ScrollToFill == this.Layout.ScrollBoundsResolved )
			{
				UltraGridRow firstRow = visibleRows[0].Row;
				if ( null != firstRow && null != scrollContext.rowScrollRegion 
					&& firstRow.ScrollPosition >= scrollContext.rowScrollRegion.GetMaxScrollPosition( ) )
					return false;
			}
			// ----------------------------------------------------------------------------------
			
			//RobA UWG252 9/26/01 We want to be able to scroll until only 1 row is visible
			

			return true; 
		}

		#endregion // CanScrollDownMore  

		#region LastRow

		internal virtual UltraGridRow LastRow
		{
			get
			{	
				UltraGridRow returnValue = null;

				UltraGridBase grid = this.Layout.Grid;

				if ( null == grid )
				{
					throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_302"));				
				}

				// get the last row in band 0
				//
				UltraGridRow row = grid.GetRow( Infragistics.Win.UltraWinGrid.ChildRow.Last );

				// Since the last row may be hidden call GetPrevVisibleRelative
				// from that row which will return itself if not hidden. 
				// Otherwise, it will read backwards until it finds a visible
				// row.
				//
				if ( null != row )
				{				
					returnValue = row.GetPrevVisibleRelative();
				}

				return returnValue;
			}			
		}

		#endregion // LastRow
		
		#region SupportsGroupByRows
		
		internal virtual bool SupportsGroupByRows
		{
			get
			{
				return false;
			}
		}

		#endregion // SupportsGroupByRows

		#region SupportsFixedRows

		// SSP 4/13/05 - NAS 5.2 Fixed Rows
		//
		internal virtual bool SupportsFixedRows
		{
			get
			{
				return true;
			}
		}

		#endregion // SupportsFixedRows

		#region ShouldApplySpecialRowFiltersCode

		// SSP 3/21/02
		//
		internal bool ShouldApplySpecialRowFiltersCode( )
		{
			if ( !this.IsMultiBandDisplay )
			{
				
				
				UltraGridLayout layout = this.layout;
				UltraGridBand band = null != layout ? layout.BandZero : null;
				if ( null != band && RowFilterMode.AllRowsInBand == band.RowFilterModeResolved )
					return false;
				
				
			}

			return true;
		}
		
		#endregion // ShouldApplySpecialRowFiltersCode

		#region EndOfRegion 

		/// <summary>
		/// Returns true if the region has been filled
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		protected bool EndOfRegion ( ref VisibleRowFetchRowContext context )
		{
			// JJD 9/1/00 - ult714
			// Always return false if there are less than 2 rows in the list. Prevents
			// logic problems when scrolling
			// 
			if ( context.lastRowInserted == null || 
				context.VisibleRows.Count < 2 )
				return false;

			// SSP 3/20/02
			// If the the only row in the collection is hidden, then also return false
			//
			if ( this.ShouldApplySpecialRowFiltersCode( ) )
			{
				if ( 1 == context.VisibleRows.Count  &&
					// SSP 8/1/03 UWG1654 - Filter Action
					// Use HiddenInternal instead of the public Hidden behavior of which we changed in regards to filtering.
					//
					//context.rowScrollRegion.VisibleRows[0].Row.Hidden )
					context.VisibleRows[0].Row.HiddenInternal )
					return false;
			}


			if ( context.maxTiersToAdd > 0 )
			{
				if ( context.lastTierAdded != context.lastRowInserted.Tier )
				{
					context.lastTierAdded = context.lastRowInserted.Tier;
					context.tiersAdded++;
				}

				if ( context.tiersAdded >= context.maxTiersToAdd )
				{
					if ( this.HasMultiRowTiers )
					{
						return ( context.lastRowInserted.ParentVisibleRow == null ||
							// SSP 11/11/03 Add Row Feature
							//
							//context.lastRowInserted.Row.HasPrevSibling() ||
							context.lastRowInserted.Row.HasPrevSibling( false, IncludeRowTypes.SpecialRows ) ||
							context.lastRowInserted.Row.HasSiblingInPrevSiblingBand( true ) );
					}

					return true;
				}
			}

			if ( context.fetchBackward )
			{
				bool fEndOfRegion =  this.HasFixedHeaders
					? context.lastRowInserted.OrigTop <= this.GetFixedHeaderHeight() + context.rowScrollRegion.ScrollOffset
					: context.lastRowInserted.OrigTop <= context.rowScrollRegion.ScrollOffset;
				//: context.lastRowInserted->GetTop() <= context.lastRowInserted->GetBand()->GetTotalHeaderHeight() ;

				if ( !fEndOfRegion )
					return false;

				// If the view style supports multi-row tiers (horizontal view)
				// when we are fetching backwards we aren't done until all parent
				// rows (on the same tier) are loaded
				//
				if ( this.HasMultiRowTiers )
				{
					return ( context.lastRowInserted.ParentVisibleRow == null ||
						// SSP 11/11/03 Add Row Feature
						//
						//context.lastRowInserted.Row.HasPrevSibling() ||
						context.lastRowInserted.Row.HasPrevSibling( false, IncludeRowTypes.SpecialRows ) ||
						context.lastRowInserted.Row.HasSiblingInPrevSiblingBand( true ) );
				}
                    
				return true;

			}
                
			return context.lastRowInserted.OrigTop > context.regionHeight;
		}

		#endregion // EndOfRegion 

		#region InsertRow

		/// <summary>
		/// Inserts a row element into the list and calls the virtual 
		/// OrientElemRowForward or OrientElemRowBackward methods to set 
		/// the top, tier and hasheader props of the elemrow. It also
		/// sets the 'has' flags on the elem rows and calls the virtual
		/// AddAllDescendants method to add all descendant rows
		/// </summary>
		/// <param name="context"></param>
		/// <param name="visibleRow"></param>
		/// <param name="insertDescendantRows"></param>
		/// <returns></returns>
		protected bool InsertRow( ref VisibleRowFetchRowContext context,
			VisibleRow visibleRow,
			bool insertDescendantRows )
		{
			bool rowAdded      = false;
			bool insertRow     = ( context.beginAtRow == null );

			// SSP 3/20/02
			// 
			bool add = !visibleRow.Row.HiddenResolved;
			
			// SSP 3/20/02
			// When all the rows in a rows colleciton get filtered out, we
			// still want to show the band headers associated with that rows
			// collection (so that the user can unfilter the rows in that 
			// colleciton). So we are going to cheat and add the first row
			// of that rows collection anyways and modify the logic in
			// RowColIntersectionUIElement to not to add the hidden row.
			//
			if ( !add && this.ShouldApplySpecialRowFiltersCode( ) )
			{
				// SSP 9/2/03 UWG2330
				// Use the new helper method instead. Commented out the original code and added new one.
				//
				// ----------------------------------------------------------------------------------------
				
				if ( visibleRow.Band.ShouldAddFilteredOutRow( this, visibleRow.Row ) )
					add = true;
				// ----------------------------------------------------------------------------------------
			}


			// if the row is hidden then return false
			//
			// SSP 3/20/02
			//
			//if ( EndOfRegion ( ref context ) || visibleRow.Row.HiddenResolved )
			if ( EndOfRegion ( ref context ) || !add )
				return false;

			// if we are reading forward we need to insert the parent row before its
			// children
			//
			if ( !context.fetchBackward )
			{
				if ( insertRow )
				{
					context.VisibleRows.InternalInsert ( visibleRow, context.insertBefore );
					rowAdded = true;
				}
			}

			// if we are reading forward call OrientElemRowForward before
			// processing the descendant rows
			//
			if ( insertRow && !context.fetchBackward )
			{
				OrientVisibleRowForward ( ref context, visibleRow );

				context.lastRowInserted = visibleRow;
			}

			// Add children of this row
			//
			if ( insertDescendantRows              &&
				visibleRow.Row.Expanded   &&
				
				// SSP 3/21/02
				// Only add the descendant rows if the row is not hidden.
				// This is necessary for the row filtering because we may have
				// a row being added that's hidden.
				//
				// SSP 9/2/03 UWG2330
				// Commented out the original code and added the new code below.
				//
				// ------------------------------------------------------------------------------------------
				//!visibleRow.Row.HiddenResolved &&
				( !visibleRow.Row.HiddenResolved 
				|| visibleRow.Row.Band.ShouldAddFilteredOutRow( this, visibleRow.Row ) ) &&
				// ------------------------------------------------------------------------------------------

				!EndOfRegion ( ref context ) )
			{
				rowAdded = AddAllDescendants ( ref context, visibleRow ) || rowAdded;
			}

			// if we are reading backward we need to insert the parent row after its
			// children
			//
			if ( context.fetchBackward )
			{

				if ( !insertRow || EndOfRegion ( ref context ) )
					return rowAdded;

				OrientVisibleRowBackward ( ref context, visibleRow );

				context.lastRowInserted = visibleRow;

				context.VisibleRows.InternalInsert ( visibleRow, context.insertBefore );
                 
				rowAdded = true;

				// since we are reading backwards we need to change the insert before value
				// to the element just added so that the next element will be inserted 
				// before this one
				//
				context.insertBefore     = visibleRow;

			}

			return rowAdded;
		}

		#endregion // InsertRow

		#region AddAllDescendants 

		/// <summary>
		/// Adds all of the child rows from each of the child band's for 
		/// a given parent row.
		/// </summary>
		/// <returns></returns>
		protected bool AddAllDescendants ( ref VisibleRowFetchRowContext context,
			VisibleRow parentVisibleRow )
		{

			// if the parent row isn't expanded then don't bother adding the children
			//

			// JJD 12/11/01
			// We can never add descendant rows if the parent band is displaying
			// its rows in a cardview
			//
			if ( !parentVisibleRow.Row.Expanded ||
				 parentVisibleRow.Row.IsCard )
				return false;

			if ( parentVisibleRow.Row is UltraGridGroupByRow )
				return AddChildRows( ref context, parentVisibleRow, null );

			// if this is a single band display style we can just return here
			//
			if ( !this.IsMultiBandDisplay )
			{
				Debug.Assert( context.beginAtRow == null, "BeginAtRow specified for single band display in AddAllDescendants");  
				return false;
			}

			bool processChildBand;
			bool rowAdded = false;

			BandsCollection bands = this.layout.SortedBands;

			int startIndex;
			int endIndex;
			int step;

			if ( context.fetchBackward )
			{
				startIndex = bands.Count - 1;
				endIndex   = 0;
				step       = -1;
			}
			else
			{
				startIndex = 0;
				endIndex   = bands.Count - 1;
				step       = 1;
			}

			// iterate over the bands collection looking for bands that are children
			// of the parent row's band
			//
			for ( int i = startIndex; 
				// SSP 2/25/02 UWG1028
				// Instead of checking for startIndex and endIndex
				// check for step. Because if startIndex and endIndex
				// started out as both the same value ( 0 ) and step
				// was -1, then the condition would not work properly.
				//
				//( startIndex > endIndex ) 
				( step < 0 )
				? i >= endIndex
				: i <= endIndex;
				i += step )				
			{
				UltraGridBand band = bands[i];

				if ( band.HiddenResolved )
					continue;

				// check to see if this band is a direct child
				// of the parent row's band
				//
				if ( band.ParentBand == parentVisibleRow.Band )
				{
					if ( context.beginAtRow != null )
					{
						// since m_pBeginAtRow was specified and hasn't been
						// satisfied (nulled out). We want to skip child
						// bands that aren't in the band parentage chain
						// of the 'beginAt' row. We need to do this so we
						// can reconstruct
						// 
						//
						UltraGridBand tempBand = context.beginAtRow.Band;
						processChildBand = false;

						// walk up the band parent chain looking for a match
						//
						while ( tempBand != null )
						{
							if ( tempBand == band )
							{
								processChildBand = true;
								break;
							}
                            
							tempBand   = tempBand.ParentBand;

						}
					}
					else
						processChildBand = true;

					if ( processChildBand )
					{
						rowAdded = AddChildRows ( ref context, 
							parentVisibleRow,
							band ) || rowAdded;
					}
				}
			}

			return rowAdded;
		}

		#endregion // AddAllDescendants 

		#region AddChildRows 

		/// <summary>
		/// Add child rows of a specific parent row in a specific band
		/// </summary>
		/// <returns></returns>
		protected virtual bool AddChildRows ( ref VisibleRowFetchRowContext context,
			VisibleRow parentVisibleRow,
			UltraGridBand  childBand )
		{
			Infragistics.Win.UltraWinGrid.UltraGridRow visibleChild;
            
			visibleChild = parentVisibleRow.Row.GetChild( context.fetchBackward 
				? ChildRow.Last 
				: ChildRow.First, 
				childBand,
				// SSP 11/11/03 Add Row Feature
				// Added below line.
				IncludeRowTypes.SpecialRows 
				);

			// JJD 11/27/00 - ult2159
			// If the row get next/previous sibling until we find one that
			// is not hidden
			//
			// SSP 4/22/04 - Virtual Binding Optimizations
			// Instead of doing a linear search to find the first visible child, use the 
			// visible indeces related methods.
			//
			// --------------------------------------------------------------------------------
			
			if ( null != visibleChild  && visibleChild.HiddenResolved )
			{
				visibleChild = 
					context.fetchBackward 
					? visibleChild.ParentCollection.GetPrevVisibleRow( visibleChild )
					: visibleChild.ParentCollection.GetNextVisibleRow( visibleChild );
			}
			// --------------------------------------------------------------------------------

			// SSP 3/21/02
			// For row filtering
			//
			// If all the rows in the whole rows colleciton are filtered
			// out, then add the first one for the purposes of adding the
			// band header associated with it. RowColIntersectionUIElement
			// is going to look at the hidden property to determine whether
			// to actually draw it or not.
			//
			if ( null == visibleChild && this.ShouldApplySpecialRowFiltersCode( ) )
			{
				// SSP 11/11/03 Add Row Feature
				//
				//visibleChild = parentVisibleRow.Row.GetChild( ChildRow.First, childBand );
				visibleChild = parentVisibleRow.Row.GetChild( ChildRow.First, childBand, IncludeRowTypes.SpecialRows );

				// SSP 9/2/03 UWG2330
				// 
				// ----------------------------------------------------------------------------------------
				if ( null != visibleChild && ! visibleChild.Band.ShouldAddFilteredOutRow(
					this, visibleChild ) )
					visibleChild = null;
				// ----------------------------------------------------------------------------------------
			}


			bool rowsAdded = false;

			if ( visibleChild != null )
			{
				rowsAdded = this.AddSiblingRows ( ref context,
					parentVisibleRow, 
					visibleChild );
                
			}

			return rowsAdded;
		}

		#endregion // AddChildRows 

		#region AddSiblingRows 

		/// <summary>
		/// Adds the specified row and siblings of the specified row to the 
		/// specified ElemRowSinkrow. We are always adding to ElemRowSink, 
		/// so user may be responsible for clearing it before calling
		/// AddSiblingRows
		///
		/// Child rows are added as well by the call to InsertRow.
		///
		/// Returns: true if at least one row was added false otherwise
		/// </summary>
		/// <param name="context"></param>
		/// <param name="parentVisibleRow"></param>
		/// <param name="firstSibling"></param>
		/// <returns></returns>
		protected bool AddSiblingRows( ref VisibleRowFetchRowContext context, 
			VisibleRow parentVisibleRow, 
			Infragistics.Win.UltraWinGrid.UltraGridRow firstSibling ) 
		{			
			VisibleRow visibleRow = null;

			RowsCollection parentRows = null != firstSibling
											? firstSibling.ParentCollection 
											: this.layout.Rows;




			bool      rowAdded      = false;
			bool      reInsertFirst = false;
			bool      insertDescendantRows = true;

			if ( EndOfRegion ( ref context ) )
				return false;

			if ( context.beginAtRow != null)
			{
				// SSP 9/17/01
				//
				if ( context.beginAtRow.ParentCollection == parentRows )
				//if ( context.beginAtRow.Band == band )
				{
					// since we are at the correct band set pFirstSibling to
					// the beginAt row 
					//
					firstSibling = context.beginAtRow;

					// null out m_pBeginAtRow since we found the 'begin at' row
					// and have successfully recreated the call nesting on the stack
					// to continue loading rows from here on
					//

					context.beginAtRow   = null;

					// reuse the BeginAtRowElem (if provided)
					//
					visibleRow = context.beginAtVisibleRow;
					
					//RobA UWG303 9/20/01 needed to initialize the row so that
					//its SubObjectPropChanged was hooked up
					if ( visibleRow != null )
					{
						visibleRow.Initialize( visibleRow.ParentVisibleRow, visibleRow.Row);						
					}

					// if we are fetching backwards from here we want to skip
					// processing of this row's descendata rows
					//
					if ( context.fetchBackward )
						insertDescendantRows = false;
				}
				else
				{
					// since this is not the beginAt row's band we
					// need to walk up its parent chain until we
					// get to the proper band
					//
					Infragistics.Win.UltraWinGrid.UltraGridRow parentRow = context.beginAtRow.ParentRow;

					if ( context.beginAtVisibleRow != null )
					{
						visibleRow = context.beginAtVisibleRow.ParentVisibleRow;						
					}


					// walk up the row parent chain looking for a band match
					//
					while ( parentRow != null )
					{
						// SSP 9/17/01
						//
						if ( parentRow.ParentCollection == parentRows )
						//if ( parentRow.Band == band )
						{
							firstSibling   = parentRow;

							// when we are scrolling backwards we need to set the 
							// reInsertFirst so that after we have drilled down
							// to the 'begin at' row and are unwinding the stack 
							// back to this level we will add this row to the
							// list since it was bypassed on the way down
							//
							if ( context.fetchBackward )
								reInsertFirst  = true;
							break;
						}
                
						parentRow   = parentRow.ParentRow;

						if ( visibleRow != null )
						{
							visibleRow = visibleRow.ParentVisibleRow;	
							
						}

					}

				}
			}

			if ( firstSibling == null )
			{
				// FAIL("pFirstSibling pointer NULL in CViewStyle::AddSiblingRows");
				return false;
			}

			//
			// Add the first row to ElemRowSink's VisibleRows collection
			//
			// create an ElemRow (visible row) object
			//
			if ( visibleRow == null )
			{
				visibleRow = context.VisibleRows.VisibleRowFromCache;				
				visibleRow.Initialize( parentVisibleRow, firstSibling );
			}
			

			// Add this row to the row element sink as int as options don't tell us to skip
			//
			if ( !this.InsertRow( ref context, visibleRow, insertDescendantRows ) ) 
				return false;

			if ( this.EndOfRegion ( ref context ) )
				return true; 

			rowAdded = true;

			// when we are scrolling backwards we use the reInsertFirst  
			// flag so that after we have drilled down to the 'begin at'
			// row and have unwound the stack back to this level we need
			// to call InsertRow again to add just this row (not its
			// descendants to the list since it was bypassed on the way down
			//
			if ( reInsertFirst )
			{
				if ( !this.InsertRow( ref context, visibleRow, false ) ) 
					return rowAdded;
			}

			// JJD 12/11/01
			// In cardview we only add the first row since the single VisibleRow
			// actually represents the entire cardview island of cards
			//
			if ( firstSibling.IsCard )
				return true;

			Infragistics.Win.UltraWinGrid.UltraGridRow row = firstSibling;

			// SSP 2/20/04 - Optimization
			// Instead of calling repeatedly within the while loop below, call it once
			// here and use the local variable in the loop. ShouldApplySpecialRowFiltersCode
			// doesn't rely on any view style state so its value won't change inside the
			// loop.
			//
			bool shouldApplySpecialFilterCode = this.ShouldApplySpecialRowFiltersCode( );

			//
			// Get sibling rows of FirstSibling, and add them to VisibleRows collection
			//
			while ( !this.EndOfRegion ( ref context ) )
			{
                // SSP 4/22/04 - Virtual Binding - Optimizations
				// Get the next visible row using visible indeces rather than doing a 
				// linear search.
				//
				// ----------------------------------------------------------------------
				
				UltraGridRow tmpRow = context.fetchBackward 
					? row.ParentCollection.GetPrevVisibleRow( row )
					: row.ParentCollection.GetNextVisibleRow( row );

				// If all the rows in the rows collection are filtered out then see if we
				// should display the column headers for the rows collection by adding a
				// the first hidden row into the visible rows collection. NOTE: Below we 
				// are making sure if this should actually be done by checking for other 
				// conditions.
				// 
				if ( shouldApplySpecialFilterCode && null == tmpRow 
					&& 0 == row.ParentCollection.VisibleRowCount )
				{
					tmpRow = row.ParentCollection.Count > 0 ? row.ParentCollection[0] : null;

					// If we have already processed the row before then re-process it again.
					// 
					if ( row == tmpRow )
						tmpRow = null;
				}

				row = tmpRow;
				// ----------------------------------------------------------------------

				if ( null == row )
					break;

				// SSP 3/20/02
				// 
				bool insertRow = !row.HiddenResolved;

				// SSP 3/20/02
				// When all the rows in a rows colleciton get filtered out, we
				// still want to show the band headers associated with that rows
				// collection (so that the user can unfilter the rows in that 
				// colleciton). So we are going to cheat and add the first row
				// of that rows collection anyways and modify the logic in
				// RowColIntersectionUIElement to not to add the hidden row.
				//
				// SSP 2/20/04 - Optimization
				// Instead of calling repeatedly within the while loop below, call it once
				// here and use the local variable in the loop. ShouldApplySpecialRowFiltersCode
				// doesn't rely on any view style state so its value won't change inside the
				// loop.
				//
				//if ( !insertRow && this.ShouldApplySpecialRowFiltersCode( ) )
				if ( !insertRow && shouldApplySpecialFilterCode )
				{
					// SSP 9/2/03 UWG2330
					// Use the new helper method instead. Commented out the original code and added new one.
					//
					// ----------------------------------------------------------------------------------------
					
					if ( row.Band.ShouldAddFilteredOutRow( this, row ) )
						insertRow = true;
					// ----------------------------------------------------------------------------------------
				}

				// SSP 3/20/02
				// 
				//if ( !row.HiddenResolved )
				if ( insertRow )
				{
					visibleRow = context.VisibleRows.VisibleRowFromCache;
					visibleRow.Initialize( parentVisibleRow, row );
                    
					if ( !this.InsertRow( ref context, visibleRow, true ) ) 
						return false;
				}
			}

			return rowAdded;
		}

		#endregion // AddSiblingRows 

		#region OnScrollBottom
		
		private void OnScrollBottom( ref ScrollRowsContext scrollContext )
		{			
			VisibleRowsCollection visibleRows = scrollContext.VisibleRows;

			VisibleRow oldTopRow = visibleRows[0];

			UltraGridBand oldTopHeaderBand = null != oldTopRow && oldTopRow.HasHeader ? oldTopRow.Band : null;

			// check if we have reached the end of the rowset
			//
			if ( !scrollContext.forceScroll && !this.CanScrollDownMore( ref scrollContext, oldTopHeaderBand ) )
				return;

			UltraGridRow lastRow = this.LastRow;

			if (  null == lastRow ) 
			{
				Debug.Fail("LastRow failed in ViewStyle.OnScrollBottom");
				return;
			}

			// Clear all visible rows, incl first, we'll re-add it from the start
			scrollContext.rowScrollRegion.ClearVisibleRows();			

			// set up the context structure to fetch backwards
			//
			VisibleRowFetchRowContext context = new VisibleRowFetchRowContext( scrollContext.rowScrollRegion, true );
			
			context.beginAtRow       = lastRow;

			// call AddSiblingRows to start loading rows
			//			
			this.AddSiblingRows ( ref context, null, null );

			if ( visibleRows.Count < 1 )
			{
				Debug.Fail("Something wierd here, no rows in visible list in ViewStyle.OnScrollBottom");
			}
			else
			{
				// call VerifyRowList to make sure everything in aligned properly
				//				
				this.VerifyRowList ( scrollContext.rowScrollRegion );

				// save the rcInvalid rect (which should be the entire region
				//
				Rectangle invalidRect = scrollContext.invalidRect;

				scrollContext.rowScrollRegion.lastScrollType = ScrollEventType.EndScroll;

				// call OnScrollUp and OnScrollDown to compensate for possible
				// differences in calulating the top row when fetching backwards
				//
				this.OnScrollUp ( ref scrollContext );
				this.OnScrollDown ( ref scrollContext );

				VisibleRow lastVisibleRow = visibleRows[visibleRows.Count-1];

				// keep scrolling down by one row until the last row is completely visible
				//
				//RobA 9/27/01 we want to scroll until only 1 row is visible
				//
				while ( visibleRows.Count > 1 )
					//&&					
					// Take the origin of the row scroll region into account
					// when checking if last row os visible
					//
				//	( scrollContext.rowScrollRegion.Origin +
				//	lastVisibleRow.GetTop() + lastVisibleRow.GetTotalHeight() >= 
				//	invalidRect.Bottom - ( lastVisibleRow.Row.BaseHeight * 2 ) ) )
				{
					// save the last row's top
					//
					int top = lastVisibleRow.GetTop();

					scrollContext.rowScrollRegion.lastScrollType = ScrollEventType.EndScroll;

					// scroll down one row
					//
					this.OnScrollDown ( ref scrollContext );

					// if the last row hasn't changed and its top hasn't changed
					// then exit out to prevent an infinite loop
					//
					// SSP 12/4/03 UWG2773
					// Instead of comparing the visible rows, compare the actual UltraGridRow because the logic
					// in OnScrollDown does not reuse the same visible rows.
					//
					//if ( lastVisibleRow == visibleRows[visibleRows.Count-1] && lastVisibleRow.GetTop() == top )
					if ( ( lastVisibleRow == visibleRows[visibleRows.Count-1] ||
							null != lastVisibleRow && null != visibleRows[visibleRows.Count-1]
							&& lastVisibleRow.Row == visibleRows[visibleRows.Count-1].Row 
						 ) && lastVisibleRow.GetTop() == top )
						break;

					// re-get the last row
					//
					lastVisibleRow =  visibleRows[visibleRows.Count-1];
				}

				scrollContext.rowScrollRegion.lastScrollType = ScrollEventType.EndScroll;

				// restore the saved invalid rect
				//
				scrollContext.invalidRect = invalidRect;

				// set the new top row
				//
				scrollContext.rowScrollRegion.SetFirstRow( visibleRows[0].Row );
			}

			scrollContext.invalidate     = true;
			scrollContext.scrollDelta    = 0;

		}

		#endregion // OnScrollBottom

		#region CalcScrollUpAmount

		private void CalcScrollUpAmount( ref ScrollRowsContext scrollContext,
			VisibleRow oldTopRow,
			int   origTopOfRow,
			int   origBottom )
		{
			this.CalcScrollUpAmount( ref scrollContext, oldTopRow, origTopOfRow,
				origBottom, false );
		}

		private void CalcScrollUpAmount( ref ScrollRowsContext scrollContext,
			VisibleRow oldTopVisibleRow,
			int   origTopOfRow,
			int   origBottom,
			bool  pixelScroll )
		{
			// Always use the bottom of row difference to calculate the
			// scroll delta since that will always work
			//
			scrollContext.scrollDelta    = oldTopVisibleRow.Bottom - origBottom;

			if ( this.HasFixedHeaders )
			{
				// Add a pixel to the bottom of the invalid area to allow
				// the row selectors to correctly draw there borders
				//

				// always invaldate the headers if we have multi-row
				// tiers (horizontal mode) in case we have sibling bands
				//
				if ( this.HasMultiRowTiers )
				{
					scrollContext.scrollRect.Y += System.Math.Max( origTopOfRow, this.GetFixedHeaderHeight() ) + 1;
            
					scrollContext.invalidRect.Height = oldTopVisibleRow.GetTopOfRow(false);

					scrollContext.invalidate = true;
				}
				else
				{
					// SSP 12/3/01 UWG803
					// We need to add the fixed header height to the scroll rect
					// not just assign to it.
					//
					//scrollContext.scrollRect.Y = this.GetFixedHeaderHeight();
					scrollContext.scrollRect.Y += this.GetFixedHeaderHeight();
					scrollContext.scrollRect.Height -= this.GetFixedHeaderHeight();
				}
			}
			else
			{
				// if the old top row doesn't have a header now
				// we need adjust the top of the scrolling area to 
				// where the the old top row was (after its header
				// and completely invalidate the area above
				//
				if ( !oldTopVisibleRow.HasHeader )
				{
					scrollContext.scrollRect.Y    += origTopOfRow + 1;
					
					scrollContext.invalidRect.Height = scrollContext.scrollRect.Top - scrollContext.invalidRect.Top;					
					
					scrollContext.invalidate = true;
				}
			}

			scrollContext.scrollRect.Height -= scrollContext.scrollDelta;
		}

		#endregion // CalcScrollUpAmount

		#region OnScrollUp 

		private void OnScrollUp ( ref ScrollRowsContext scrollContext )
		{
			VisibleRowsCollection visibleRows = scrollContext.VisibleRows;

			VisibleRow oldTopRow  = visibleRows[0];
			VisibleRow visibleRow = null;

			// SSP 4/28/05 - NAS 5.2 Fixed Rows_
			// VisibleRow.IsFirstRow and VisibleRow.IsAbsoluteFirstRow( ) and ViewStyleBase.IsFirstRow( )
			// all do the same. Just use the IsAbsoluteFirstRow instead.
			//
			//if ( this.IsFirstRow( oldTopRow ) )
			if ( oldTopRow.IsAbsoluteFirstRow )
				return;

			int        currTopOfRow       = oldTopRow.GetTopOfRow();
			int        currTier           = oldTopRow.Tier;
			int        origBottom         = oldTopRow.Bottom;

			int i;

			// iterate thru the current list bumping the top value and the
			// tier for every row
			//
			for ( i = 0; i < visibleRows.Count; i++ )
			{
				visibleRow = visibleRows[i];				
				visibleRow.SetTop( visibleRow.GetTop() + scrollContext.rowScrollRegion.Extent );
				visibleRow.Tier = visibleRow.Tier + 1 ;
			}

			// remove the front row since we will be reinserting it on the call
			// to AddSiblingRows below
			//
			scrollContext.rowScrollRegion.PopFrontVisibleRow();

			// set up the context structure to the call to AddSiblingRows below
			//
			VisibleRowFetchRowContext context = 
				new VisibleRowFetchRowContext( scrollContext.rowScrollRegion, true );

			context.beginAtRow       = oldTopRow.Row;
			context.beginAtVisibleRow   = oldTopRow;

			context.insertBefore     = visibleRows.Count > 0 
				? visibleRows[0]
				: null;
		
			context.lastRowInserted  = context.insertBefore;
			context.lastTierAdded    = null != context.lastRowInserted 
				? context.lastRowInserted.Tier
				: currTier;

			// set max tiers to add to either 1 or 2 depending on whether
			// there was a tier break between the old top row and the next row
			//
			// Only set context.maxTiersToAdd to 1 if the view style supports 
			// multi row tiers (horizontal view)
			//
			context.maxTiersToAdd    = ( this.HasMultiRowTiers
				&& ( currTier == context.lastTierAdded )				
				// If we are on the last row then always
				// set context.maxTiersToAdd to 2
				//
				&& null != context.lastRowInserted )
				? 1 : 2;

			// JJD 9/27/01 
			// We are now allowing the last row to be scrolled to the
			// top. Therefore if we just popped off the last row then
			// set maxTiersToAdd to 1.
			//
			if ( null == context.lastRowInserted	&&
				 null != context.beginAtVisibleRow	&&
				 this.IsLastRow(context.beginAtVisibleRow.Row) )
			{
				context.maxTiersToAdd    = 1;
			}

			// call AddSiblingRows which will reconstruct the stack down to
			// firstRowToInsert and will start inserting from that point 
			// backward
			//			
			this.AddSiblingRows ( ref context, null, null );
			
			this.VerifyRowList( scrollContext.rowScrollRegion );

			this.VerifyRowList( scrollContext.rowScrollRegion );

			Debug.Assert( null != oldTopRow, "No old top row in ViewStyleBase.OnScrollUp");

			// calculate the amount to scroll
			//
			if ( null != oldTopRow )
			{
				this.CalcScrollUpAmount( ref scrollContext,
					oldTopRow,
					currTopOfRow,
					origBottom,
					false );
			}

			// set the new top row
			//
			scrollContext.rowScrollRegion.SetFirstRow( visibleRows[0].Row );
		}

		#endregion // OnScrollUp 

		#region IsLastRow 

		internal virtual bool IsLastRow ( UltraGridRow row )
		{
			// for single band display we can just check for a next sibling
			//
			// SSP 11/11/03 Add Row Feature
			//
			//return !row.HasNextSibling();
			return !row.HasNextSibling( false, IncludeRowTypes.SpecialRows );
		}

		#endregion // IsLastRow 

		#region CalculateRowSpecialFlags 

		/// <summary>
		/// Calculates the special flags needed to draw the row and cells
		/// properly. These flags include whether the row is responsible for
		/// its top bnorder, et al.
		/// </summary>
		protected virtual Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags CalculateRowSpecialFlags ( VisibleRow visibleRow, VisibleRow lastVisibleRow )
		{
			Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags  specialFlags = 0;

			Debug.Assert( null != visibleRow.Band, "No Band for visibleRow" );
			
			Infragistics.Win.UIElementBorderStyle bsr = visibleRow.Band.BorderStyleRowResolved;

			if ( bsr != Infragistics.Win.UIElementBorderStyle.Inset &&
				bsr != Infragistics.Win.UIElementBorderStyle.Raised &&
				bsr != Infragistics.Win.UIElementBorderStyle.InsetSoft &&
				bsr != Infragistics.Win.UIElementBorderStyle.RaisedSoft&&
				bsr != Infragistics.Win.UIElementBorderStyle.None )
			{
				if ( visibleRow.HasHeader || 
					( visibleRow.GetTop() <= this.GetFixedHeaderHeight() &&
					visibleRow.Band.GetTotalHeaderHeight() > 0 ) )
				{
					specialFlags |= Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags.SkipTopBorder;
				}
				else
					if ( null != lastVisibleRow )
				{
					// SSP 9/20/01
					// modified below if clause to check for parent collection and band
					// since lastVisibleRow could have been a group by row and in that
					// case we would not want to skip the top border
					//
					if ( lastVisibleRow.Row.ParentCollection == visibleRow.Row.ParentCollection &&
						//lastVisibleRow.Band == visibleRow.Band &&
						lastVisibleRow.PostRowSpacing == 0      &&
						// SSP 5/3/05 - NAS 5.2 Fixed Rows/Special Row Separators
						// Use the RowSpacingAfterResolvedBase and RowSpacingBeforeResolvedBase instead which
						// unlike the RowSpacingAfterResolved and RowSpacingBeforeResolved do not take into
						// account the special row separators.
						// 
                        //lastVisibleRow.Row.RowSpacingAfterResolved == 0 &&
						//visibleRow.Row.RowSpacingBeforeResolved == 0 
						lastVisibleRow.Row.RowSpacingAfterResolvedBase == 0 &&
						visibleRow.Row.RowSpacingBeforeResolvedBase == 0 )
					{
						specialFlags |= Infragistics.Win.UltraWinGrid.ViewStyleSpecialFlags.SkipTopBorder;
					}
				}
			}

			visibleRow.SpecialFlags = specialFlags;

			return specialFlags;
		}

		#endregion // CalculateRowSpecialFlags 

		#region SetNextCrossBandSibling 

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//protected void SetNextCrossBandSibling ( ref VisibleRowFetchRowContext context, VisibleRow visibleRow )
		protected static void SetNextCrossBandSibling( ref VisibleRowFetchRowContext context, VisibleRow visibleRow )
		{
			if ( null == context.lastRowInserted )
				return;

			int lastLevel;
			int thisLevel = (int)visibleRow.Band.HierarchicalLevel;

			VisibleRow lastVisibleRow = context.lastRowInserted;

			// walk up the last element's parent cahin looking for the last
			// elem row at the same hierarchical level
			//
			while ( null != lastVisibleRow )
			{
				lastLevel = (int)lastVisibleRow.Band.HierarchicalLevel;

				// MD 7/26/07 - 7.3 Performance
				// The two method called in these if blocks have been removed
				//if ( lastLevel == thisLevel )
				//{
				//    // only set NextCrossBandSibling on the last elem row if
				//    // we share the same parent row
				//    //
				//    if ( visibleRow.Row.ParentRow == lastVisibleRow.Row.ParentRow )
				//    {
				//        lastVisibleRow.SetNextCrossBandSibling( visibleRow );
				//        visibleRow.SetPrevCrossBandSibling( lastVisibleRow );
				//    }
				//}

				if ( lastLevel <= thisLevel )
					break;

				lastVisibleRow = lastVisibleRow.ParentVisibleRow;
			}
		}

		#endregion // SetNextCrossBandSibling 

		#region OrientVisibleRowForward 

		/// <summary>
		/// Sets the row's top, tier and has header flag when we are fetching
		/// forward. The default is to increment the tier for every row.
		/// Some view styles override this method
		/// </summary>
		/// <param name="context"></param>
		/// <param name="visibleRow"></param>
		protected virtual void OrientVisibleRowForward ( ref VisibleRowFetchRowContext context, 
			VisibleRow visibleRow )
		{
			visibleRow.PostRowSpacing = 0;

			// check if this is the firat row inserted
			//
			if ( null == context.lastRowInserted )
			{
				visibleRow.Tier = 0;
				// SSP 9/14/07 BR25887
				// Bottom fixed rows do not have headers so don't offset their start location by the fixed
				// header height.
				// 
				//visibleRow.SetTop( this.GetFixedHeaderHeight() + context.rowScrollRegion.scrollOffset );
				int fixedHeaderHeight = ! context.isBottomFixedRowsContext ? this.GetFixedHeaderHeight( ) : 0;
				visibleRow.SetTop( fixedHeaderHeight + context.rowScrollRegion.scrollOffset );

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
				//visibleRow.HasHeader = !this.HasFixedHeaders;
				this.SetHasHeaderHelper( ref context, visibleRow, !this.BandHasFixedHeaders( visibleRow.Band ) );

				// Set the special flags here
				//
				visibleRow.SpecialFlags = this.CalculateRowSpecialFlags ( visibleRow, context.lastRowInserted );
				return;
			}

			visibleRow.Tier = context.lastRowInserted.Tier + 1;
			visibleRow.SetTop( context.lastRowInserted.GetTop() + context.lastRowInserted.GetTotalHeight() );
			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
			// ------------------------------------------------------------------------------------
			
			bool hasHeader = !this.BandHasFixedHeaders( visibleRow.Band ) 
				// SSP 11/11/03 Add Row Feature
				//
				//&& !visibleRow.Row.HasPrevSibling();
				&& !visibleRow.Row.HasPrevSibling( false, IncludeRowTypes.SpecialRows );
			this.SetHasHeaderHelper( ref context, visibleRow, hasHeader );
			// ------------------------------------------------------------------------------------

			// Set the special flags here
			//
			visibleRow.SpecialFlags = this.CalculateRowSpecialFlags ( visibleRow, context.lastRowInserted );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.SetNextCrossBandSibling ( ref context, visibleRow );
			ViewStyleBase.SetNextCrossBandSibling( ref context, visibleRow );
		}

		#endregion // OrientVisibleRowForward 

		#region OrientVisibleRowBackward 

		/// <summary>
		/// Sets the row's top, tier and has header flag when we are fetching
		/// backward. The default is to increment the tier for every row.
		/// Some view styles override this method
		/// </summary>
		/// <param name="context"></param>
		/// <param name="visibleRow"></param>
		protected virtual void OrientVisibleRowBackward ( 
			ref VisibleRowFetchRowContext context,
			VisibleRow visibleRow )
		{
			visibleRow.PostRowSpacing = 0;

			// check if this is the first row inserted
			//
			if ( null == context.lastRowInserted )
			{
				visibleRow.Tier = 5000;
				visibleRow.SetTop( context.regionHeight - visibleRow.GetTotalHeight() );
    
				if ( visibleRow.Band.GetTotalHeaderHeight() >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset ||
					this.GetFixedHeaderHeight()            >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset )
				{
					visibleRow.SetTop( this.GetFixedHeaderHeight() + context.rowScrollRegion.ScrollOffset );
				}

				// SSP 7/18/05 - NAS 5.3 Header Placement
				// 
				//visibleRow.HasHeader = !this.HasFixedHeaders;
				this.SetHasHeaderHelper( ref context, visibleRow, !this.BandHasFixedHeaders( visibleRow.Band ) );
				return;
			}

			visibleRow.Tier = context.lastRowInserted.Tier - 1;
			visibleRow.SetTop( context.lastRowInserted.GetTop() - context.lastRowInserted.GetTotalHeight() );
    
			if ( visibleRow.Band.GetTotalHeaderHeight() >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset ||
				this.GetFixedHeaderHeight()            >= visibleRow.GetTop() + context.rowScrollRegion.ScrollOffset )
			{
				visibleRow.SetTop( this.GetFixedHeaderHeight() + context.rowScrollRegion.ScrollOffset );
			}

			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
			// ------------------------------------------------------------------------------------
			
			bool hasHeader = !this.BandHasFixedHeaders( visibleRow.Band )
				// SSP 11/11/03 Add Row Feature
				//
				//&& !visibleRow.Row.HasPrevSibling();
				&& !visibleRow.Row.HasPrevSibling( false, IncludeRowTypes.SpecialRows );
			this.SetHasHeaderHelper( ref context, visibleRow, hasHeader );
			// ------------------------------------------------------------------------------------
		}

		#endregion // OrientVisibleRowBackward 

		#region VerifyShouldRemoveHiddenRow

		// SSP 3/27/02
		// Added this helper method. Used for row filter feature.
		//
		internal bool VerifyShouldRemoveHiddenRow( RowScrollRegion rowScrollRegion, UltraGridRow row, out bool recreateRowList )
		{
			recreateRowList = false;

			if ( null == row || !row.IsStillValid )
				return true;

			bool rowHidden = row.HiddenResolved;

			if ( rowHidden && this.ShouldApplySpecialRowFiltersCode( ) )
			{
				// SSP 9/2/03 UWG2330
				// Commented out below code and added new one.
				//
				// ------------------------------------------------------------------------------------
				
				if ( row.Band.ShouldAddFilteredOutRow( this, row ) )
				{
					return false;
				}
				// ------------------------------------------------------------------------------------
				else
				{
					Debug.WriteLine( "VerifyExistingRows: had to cause recreation of rows ********" );
					recreateRowList = true;
					return true;
				}
			}

			return rowHidden;
		}

		#endregion // VerifyShouldRemoveHiddenRow

		#region VerifyExistingRows
		
		// SSP 4/28/05 - NAS 5.2 Fixed Rows
		// Got rid of row scroll region parameter since context already contains 
		// the row scroll region.
		//
		//private bool VerifyExistingRows( RowScrollRegion rowScrollRegion, ref VisibleRowFetchRowContext  forwardContext )
		private bool VerifyExistingRows( ref VisibleRowFetchRowContext  forwardContext )
		{	
			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Got rid of row scroll region parameter since context already contains 
			// the row scroll region.
			//
			//VisibleRowsCollection visibleRows = rowScrollRegion.VisibleRows;
			RowScrollRegion rowScrollRegion = forwardContext.rowScrollRegion;
			VisibleRowsCollection visibleRows = forwardContext.VisibleRows;
    
			bool resetStartRow = false;

			// SSP 3/27/02
			// Added this flag in case we are in a state that requires us to 
			// recreate visible rows. This was added for row filtering feature.
			//
			bool recreateRowList = false;

			begin_iteration:

				// remove any leading rows that are not active. This
				// is more efficient than doing it in the for loop 
				// below when all the rows are inactive since we
				// nned to restart the iteration after each removal.
				//
				// Remove hidden rows from the front of the list as well
				//				
				while ( visibleRows.Count > 0 &&
					visibleRows[0].Row.HiddenResolved )
				{
					// SSP 3/27/02
					// Added following block of code for row filtering feature. 
					// We don't want to remove any hidden rows that were purposedly added
					// (which is what may happen sometimes when row filtering is enabled)
					// to the visible rows collection during creation of visible rows
					// colleciton in the view style logic. We also at the same time want 
					// to ensure that we don't end up in a 'weird' state so recreate the 
					// visible rows collection if necessary.
					//
					//--------------------------------------------------------------------------------
					bool reallyRemoveRow = this.VerifyShouldRemoveHiddenRow( rowScrollRegion, visibleRows[0].Row, out recreateRowList );

					if ( recreateRowList )
					{
						// Clearing the visible rows collection will cause the 
						// visible rows to be recreated further down.
						//
						visibleRows.InternalClear( );
						break;
					}

					// If we are not to remove the row, then continue
					//
					if ( !reallyRemoveRow )
						break;
					//--------------------------------------------------------------------------------
					

					rowScrollRegion.PopFrontVisibleRow();

					// Set a flag so we know to reset the first row at the end
					//
					resetStartRow = true;
				}

			// if the current list is empty then call RecreateRowList and
			// exit
			//
			if ( visibleRows.Count <= 0 )
			{
				this.RecreateRowList( rowScrollRegion );				
				return false;
			}
			
			VisibleRow visibleRow = visibleRows[0];
			UltraGridRow       row;
			UltraGridRow       parentRow;
			bool      removeFromList;

			// MD 7/26/07 - 7.3 Performance
			// The two methods called in this for loop have been removed
			//// loop thru elem rows nulling out the NextCrossBandSibling ptr
			////
			//for ( int i = 0; i < visibleRows.Count; i++ )
			//{
			//    VisibleRow temp = visibleRows[i];
			//    temp.SetNextCrossBandSibling( null );
			//    temp.SetPrevCrossBandSibling( null );
			//}

			for ( int i = 0; i < visibleRows.Count; i++ )
			{
				visibleRow     = visibleRows[i];

				removeFromList = false;

				row            = visibleRow.Row;
				parentRow      = row.ParentRow;

				// if the row is no longer valid (or is hidden or
				// is a child of a row that is not expanded)
				// then remove it from the list and restart the iteration 
				// since removing the item invalidates the iterator in a list
				//
				if ( row.HiddenResolved )
				{
					// SSP 3/27/02
					// Added following block of code for row filtering feature. 
					// We don't want to remove any hidden rows that were purposedly added
					// (which is what may happen sometimes when row filtering is enabled)
					// to the visible rows collection during creation of visible rows
					// colleciton in the view style logic. We also at the same time want 
					// to ensure that we don't end up in a 'weird' state so recreate the 
					// visible rows collection if necessary.
					//
					//--------------------------------------------------------------------------------
					bool reallyRemoveRow = this.VerifyShouldRemoveHiddenRow( rowScrollRegion, row, out recreateRowList );

					if ( recreateRowList )
					{
						// Clearing the visible rows collection will cause the 
						// visible rows to be recreated further down.
						//
						visibleRows.InternalClear( );
						break;
					}
					//--------------------------------------------------------------------------------

					// SSP 3/27/02
					// Only remove if VerifyShouldRemoveHiddenRow returns true
					// Added if condition.
					if ( reallyRemoveRow )
						removeFromList = true;
				}

					// SSP 3/27/02
					// Took out the else in else-if and added as a condition !removeFromList
					//
				//else
					if (
						// SSP 3/27/02
						// Added !removeFromList
						//
						!removeFromList && 
						null != parentRow ) 
				{
					// SSP 9/2/03 UWG2330
					//
					// --------------------------------------------------------------------------------
					
					if ( ! parentRow.Expanded )
						removeFromList = true;
					else if ( parentRow.HiddenResolved && 
						! parentRow.Band.ShouldAddFilteredOutRow( this, parentRow ) )
						removeFromList = true;
					// --------------------------------------------------------------------------------
				}

				if ( removeFromList )
				{
					rowScrollRegion.RemoveVisibleRow( visibleRow );
					goto begin_iteration;
				}

				// call the virtual OrientElemRowForward method which
				// will reset top, tier and has headers flag
				//
				this.OrientVisibleRowForward ( ref forwardContext, visibleRow );

				forwardContext.lastRowInserted = visibleRow;
				forwardContext.insertAfter     = visibleRow;

			}

			// SSP 3/27/02
			// If recreateRowList is set to true, then recreate the row list
			// and return. This was added for row filtering.
			//
			if ( recreateRowList )
			{
				this.RecreateRowList( rowScrollRegion );				
				return false;
			}

			// Call ResetStartRow if the flag is set
			//
			if ( resetStartRow )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.ResetStartRow( rowScrollRegion );
				ViewStyleBase.ResetStartRow( rowScrollRegion );
			}

			return true;
		}

		#endregion // VerifyExistingRows

		#region ResetStartRow
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void ResetStartRow(  RowScrollRegion rowScrollRegion )
		private static void ResetStartRow( RowScrollRegion rowScrollRegion )
		{	
			VisibleRowsCollection visibleRows = rowScrollRegion.VisibleRows;

			if ( visibleRows.Count <= 0 )
				return;

			rowScrollRegion.SetFirstRow( visibleRows[0].Row );
		}

		#endregion // ResetStartRow

		#region AdjustListToFillRegion
		
		// SSP 4/28/05 - NAS 5.2 Fixed Rows
		// Got rid of row scroll region parameter since context already contains 
		// the row scroll region.
		//
		//void AdjustListToFillRegion( RowScrollRegion rowScrollRegion, ref VisibleRowFetchRowContext forwardContext )
		private void AdjustListToFillRegion( ref VisibleRowFetchRowContext forwardContext )
		{		
			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Got rid of row scroll region parameter since context already contains 
			// the row scroll region.
			//
			//VisibleRowsCollection visibleRows = rowScrollRegion.VisibleRows;
			RowScrollRegion rowScrollRegion = forwardContext.rowScrollRegion;
			VisibleRowsCollection visibleRows = forwardContext.VisibleRows;

			// check to see if the region is already filled
			//
			if ( forwardContext.lastRowInserted.OrigTop >= forwardContext.regionHeight )
			{
				// remove any rows that extend beyond the extent of the region
				//
				while ( visibleRows.Count > 1 &&
					visibleRows.LastRow.OrigTop >= forwardContext.regionHeight 
					
					// Added the row's height into the calculation
					// since otherwise PopBackVisibleRow ended up
					// causing rows to the added and then removed on
					// each verify (paint) operation
					//
					+ visibleRows.LastRow.GetTotalHeight() )
				{
					rowScrollRegion.PopBackVisibleRow();					
				}
				return;
			}
			
			VisibleRow oldLastRow = forwardContext.lastRowInserted;

			// remove the last row since we will be reinserting it on the call
			// to AddSiblingRows below
			//
			rowScrollRegion.PopBackVisibleRow();

			forwardContext.beginAtRow       = oldLastRow.Row;
			forwardContext.beginAtVisibleRow = oldLastRow;
			forwardContext.insertAfter      = 
				visibleRows.Count > 0 ? visibleRows.LastRow
				: null;
			forwardContext.lastRowInserted = forwardContext.insertAfter;

			// SSP 9/19/02 UWG1637
			// Added below code and enclosed the AddSiblingRows call in try-finally
			// block so that we can ensure that lastVisibleRowHadHeaders is set
			// to null.
			//
			if ( null != oldLastRow && oldLastRow.HasHeader )
				this.lastVisibleRowHadHeaders = oldLastRow;
			try
			{
				// call AddSiblingRows which will reconstruct the stack down to
				// firstRowToInsert and will start inserting from that point 
				// forward
				//
				this.AddSiblingRows ( ref forwardContext, null, null );
			}
			finally
			{
				this.lastVisibleRowHadHeaders = null;
			}
		}

		#endregion // AdjustListToFillRegion

		#region CalcScrollDownAmount

		private void CalcScrollDownAmount( ref ScrollRowsContext scrollContext,
			VisibleRow newTopRow, int origBottom )
		{
			this.CalcScrollDownAmount( ref scrollContext, newTopRow, origBottom, false );
		}

		private void CalcScrollDownAmount( ref ScrollRowsContext scrollContext,
			VisibleRow newTopRow, int origBottom, bool pixelScroll )
		{
			// Always use the bottom of row difference to calculate the
			// scroll delta since that will always work
			//
			scrollContext.scrollDelta = newTopRow.Bottom - origBottom;

			// calculate the amount to scroll
			//
			if ( this.HasFixedHeaders )
			{
				// always invaldate the headers if we have multi-row
				// tiers (horizontal mode) in case we have sibling bands
				//

				scrollContext.scrollRect.Y  += this.GetFixedHeaderHeight() - scrollContext.scrollDelta;
				scrollContext.invalidate     = true;

				if ( this.HasMultiRowTiers )
				{
					scrollContext.invalidRect.Height = 2 + System.Math.Max( this.GetFixedHeaderHeight(), newTopRow.GetTopOfRow() );
				}
				else
				{
					if ( pixelScroll )
					{
						scrollContext.invalidRect.Height = this.GetFixedHeaderHeight() + 2;
					}
					else
					{
						scrollContext.invalidRect.Y   += this.GetFixedHeaderHeight();
						scrollContext.invalidRect.Height = 2;
					}
				}

			}
			else
			{
				// Add 2 pixels to the bottom of the invalid area to allow
				// the row selectors to correctly draw there borders
				//
				if ( newTopRow.GetTopOfRow() > 0 )
				{
					scrollContext.invalidRect.Height = newTopRow.GetTopOfRow( false ) + 2;
					scrollContext.invalidate     = true;           
				}

				// scroll from the top of the row (including the header area)
				//
				scrollContext.scrollRect.Y += newTopRow.GetTopOfRow() - scrollContext.scrollDelta;
			}
		}

		#endregion // CalcScrollDownAmount
		
		#region OnScrollDown 

		/// <summary>
		/// Scrolls down one row
		/// </summary>
		/// <param name="scrollContext"></param>
		protected void OnScrollDown ( ref ScrollRowsContext scrollContext )
		{
			VisibleRowsCollection visibleRows = scrollContext.VisibleRows;
			VisibleRow  visibleRow       = visibleRows[0];
			VisibleRow  oldTopRow        = visibleRow;
			VisibleRow  newTopRow        = null;

			int        currTop          = visibleRow.GetTop();

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//int        currTopOfRow     = visibleRow.GetTopOfRow();

			int        newTopRowOrigBottom    = visibleRow.Bottom;
			int        currTier         = visibleRow.Tier;
			int        newTier          = 0;
			int        firstTierTop     = currTop;
			int        firstTierBottom  = 0;

			UltraGridBand oldTopHeaderBand = visibleRow.HasHeader
				? visibleRow.Band
				: null;


			//bool        endReached     = true;
			bool        retried        = false;
			bool        pixelScroll = false;
			bool        removeTopRows;

			// check if this is a repeat scroll down (the user kept the down
			// arrow button depressed and therefore we haven't received
			// an 'end scroll' message
			//

			// FUTURE: Pixel scrolling needs to be implemented in OnScrollUp
			//
			if ( this.HasFixedHeaders && scrollContext.rowScrollRegion.lastScrollType == scrollContext.scrollType )
			{
				// if m_nLastScrollDelta has not been set then we need to use
				// the min row height to calculate our pixel based scrolling delta
				//
				if ( 0 == scrollContext.rowScrollRegion.lastScrollDelta )
				{
					scrollContext.rowScrollRegion.lastScrollDelta = - this.Layout.MinRowHeight * 19 / 20;
				}

				// if the scrolling delta is less than half the scroll region extent then we want
				// to scroll based on the pixel value to produce a more uniform repeat
				// scroll
				//
				if ( scrollContext.rowScrollRegion.Extent / 2 > - scrollContext.rowScrollRegion.lastScrollDelta )
				{
					pixelScroll = true;
				}
			}
			else
			{
				// reset the offset and delta for a potential repeat scroll
				//
				scrollContext.rowScrollRegion.scrollOffset     = 0;
				scrollContext.rowScrollRegion.lastScrollDelta  = 0;
				pixelScroll = false;
			}
			
			VisibleRow backRow = visibleRows[visibleRows.Count-1];
			

			// check if we have reached the end of the rowset
			//
			if ( null != backRow && this.IsLastRow( backRow.Row ) )
			{ 
				if ( !this.CanScrollDownMore( ref scrollContext, oldTopHeaderBand ) )
					return;

				scrollContext.rowScrollRegion.scrollOffset     = 0;
				scrollContext.rowScrollRegion.lastScrollDelta  = 0;
				pixelScroll = false;
			}

			check_first_tier:

				// iterate thru the current list looking for a break
				// in tier. These are the rows we want to
				// remove from the list.
				//
				for ( int i = 0; i < visibleRows.Count;	i++ )
				{					
					visibleRow = visibleRows[i];
        
					newTier     = visibleRow.Tier;

					if ( newTier > currTier )
					{
						// same the first row of the next tier
						// since this will become the new top row
						//
						newTopRow              = visibleRow;
						newTopRowOrigBottom    = visibleRow.Bottom;
						break;
					}

					firstTierTop       = System.Math.Min( firstTierTop, visibleRow.GetTop() );
					firstTierBottom    = System.Math.Max( firstTierBottom, visibleRow.GetTop() + 
						visibleRow.GetTotalHeight() + 
						visibleRow.PostRowSpacing );
        
					scrollContext.scrollDelta    = - visibleRow.GetTotalHeight();
				}

			if ( pixelScroll )
			{
				scrollContext.scrollDelta    = scrollContext.rowScrollRegion.lastScrollDelta;

				// accumulate the offset 
				//
				scrollContext.rowScrollRegion.scrollOffset  += scrollContext.rowScrollRegion.lastScrollDelta;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//int firstTierHeight = firstTierBottom - firstTierTop;

				removeTopRows = ( firstTierBottom + scrollContext.rowScrollRegion.lastScrollDelta < this.GetFixedHeaderHeight() );

				if ( removeTopRows )
				{
					// adjust the offset based on the removal of the top tier of rows
					//
					scrollContext.rowScrollRegion.scrollOffset = firstTierBottom - this.GetFixedHeaderHeight() + scrollContext.rowScrollRegion.lastScrollDelta;

					if ( this.HasFixedHeaders )
					{
						if ( null != newTopRow )
						{
							if ( newTopRow.HasHeader )
								scrollContext.rowScrollRegion.scrollOffset += newTopRow.Band.GetTotalHeaderHeight();
							//already commented                    else
							//already commented                    if ( pNewTopRow->GetTop() != pNewTopRow->GetOrigTop())
							//already commented                        ScrollContext.m_ElemRowSink.m_nScrollOffset += pNewTopRow->GetTop() - pNewTopRow->GetOrigTop();
						}
					}
					else
					{
						scrollContext.rowScrollRegion.scrollOffset -= oldTopRow.Band.GetTotalHeaderHeight();
                
						if ( null != newTopRow && newTopRow.HasHeader )
							scrollContext.rowScrollRegion.scrollOffset += newTopRow.Band.GetTotalHeaderHeight();

					}
					
					int newOffset = scrollContext.rowScrollRegion.scrollOffset;
					Debug.Assert( newOffset <= this.GetFixedHeaderHeight(), "Invalid new row scroll pixel offset" );
					
				}
    
				int targetDelta   = scrollContext.rowScrollRegion.lastScrollDelta;
				int actualDelta   = scrollContext.scrollDelta;
				Debug.Assert( targetDelta == actualDelta, "Pixel scroll delta has changed");
				
			}
			else
				removeTopRows = true;

			if ( removeTopRows )
			{
				// if we didn't find the new top row in the list
				// then just call RecreateRowList to create it from
				// scratch
				//
				if ( null == newTopRow )
				{
					// We might be in a situation where there isn't any 2nd tier row in
					// the list to use as the new top row. In that case we call VerifyExistingRows
					// and AdjustListToFillRegion with a region height artificailly large
					// enough to contain the 2nd tier rows. Then we just loop around and try
					// again
					// 
					if ( !retried && null != oldTopRow )
					{
						// reset the fRetried flag so we don't end up in a possible loop
						// (we should only come thru here once)
						//
						retried = true;

						// set up the context structure
						//
						VisibleRowFetchRowContext context = 
							new VisibleRowFetchRowContext( scrollContext.rowScrollRegion );

						// double the region height so that VerifyExistingRows and AdjustListToFillRegion
						// will load up another region's worth of rows
						//
						context.regionHeight += context.regionHeight * oldTopRow.Row.TotalHeight;
						
						// SSP 4/28/05 - NAS 5.2 Fixed Rows
						// Got rid of row scroll region parameter since context already contains 
						// the row scroll region.
						//
						//if ( !this.VerifyExistingRows( scrollContext.rowScrollRegion, ref context ) )
						if ( !this.VerifyExistingRows( ref context ) )
						{
							scrollContext.invalidate     = true;
							scrollContext.scrollDelta    = 0;
							return;
						}

						// SSP 4/28/05 - NAS 5.2 Fixed Rows
						// Got rid of row scroll region parameter since context already contains 
						// the row scroll region.
						//
						//this.AdjustListToFillRegion( scrollContext.rowScrollRegion, ref context );
						this.AdjustListToFillRegion( ref context );						

						goto check_first_tier;

					}
					
					this.RecreateRowList( scrollContext.rowScrollRegion );
					scrollContext.invalidate     = true;
					scrollContext.scrollDelta    = 0;

					return;
				}

				// pop off all the rows up to 9but not including) the
				// new top row
				//
				while ( newTopRow != visibleRows[0] )
				{
					scrollContext.rowScrollRegion.PopFrontVisibleRow();
				}
			}
			else
			{
				// we are not removing the top rows (pixel based scroll) 
				// so just use the orig top row 
				//
				newTopRow              = oldTopRow;
				newTopRowOrigBottom    = oldTopRow.Bottom;
			}

			// call VerifyRowList to re-adjust all the top coordinates and add
			// new rows to the end of the visible rows list
			//			
			this.VerifyRowList( scrollContext.rowScrollRegion );

			Debug.Assert( null != newTopRow, "No new top row in ViewStyle.OnScrollDown");

			// Make sure newtoprow is still valid after VerifyRowList.
			// If not use old top row
			//

			// calculate the amount to scroll
			//
			if ( null != newTopRow )
				this.CalcScrollDownAmount( ref scrollContext, newTopRow,					
					newTopRowOrigBottom, pixelScroll );


			if ( removeTopRows )
			{
				// set the new top row
				//
				scrollContext.rowScrollRegion.SetFirstRow( newTopRow.Row );
			}
		}

		#endregion // OnScrollDown 

		#region OnScrollPageUp 
		
		private void OnScrollPageUp ( ref ScrollRowsContext scrollContext )
		{
			VisibleRowsCollection visibleRows = scrollContext.VisibleRows;

			VisibleRow  oldTopRow  = visibleRows[0];
			VisibleRow  visibleRow = null;

			// SSP 4/28/05 - NAS 5.2 Fixed Rows_
			// VisibleRow.IsFirstRow and VisibleRow.IsAbsoluteFirstRow( ) and ViewStyleBase.IsFirstRow( )
			// all do the same. Just use the IsAbsoluteFirstRow instead.
			//
			//if ( this.IsFirstRow( oldTopRow ) )
			if ( oldTopRow.IsAbsoluteFirstRow )
				return;

			int     currTop              = oldTopRow.GetTop();
			int     currTopOfRow        = oldTopRow.GetTopOfRow();
			int     currTier            = oldTopRow.Tier;
			int     oldTopRowOrigBottom = oldTopRow.Bottom;
			int     topOffset           = System.Math.Max( oldTopRow.Row.TotalHeight, 
				scrollContext.rowScrollRegion.Extent - ( currTop + oldTopRow.GetTotalHeight() ) );
			int     tiers               = 0;
			int     lastTier            = -1;

			// iterate thru the current list bumping the top value and the
			// tier for every row
			//
			for ( int i = 0; i < visibleRows.Count; i++ )
			{
				visibleRow = visibleRows[i];

				// count the tiers as we go down
				//
				if ( visibleRow.Tier != lastTier )
				{
					tiers++;
					lastTier   = visibleRow.Tier;
				}

				visibleRow.SetTop( visibleRow.GetTop() + topOffset );
				visibleRow.Tier = visibleRow.Tier + 500;
			}
			
			// remove the front row since we will be reinserting it on the call
			// to AddSiblingRows below
			//
			scrollContext.rowScrollRegion.PopFrontVisibleRow();

			// set up the context structure to the call to AddSiblingRows below
			//
			VisibleRowFetchRowContext context = 
				new VisibleRowFetchRowContext( scrollContext.rowScrollRegion, true ); // backwards fetch

			context.beginAtRow       = oldTopRow.Row;
			context.beginAtVisibleRow   = oldTopRow;

			context.insertBefore     = visibleRows.Count > 0 
				? visibleRows[0]
				: null;
			context.lastRowInserted  = context.insertBefore;
			context.lastTierAdded    = null != context.lastRowInserted 
				? context.lastRowInserted.Tier
				: currTier;

			// call AddSiblingRows which will reconstruct the stack down to
			// firstRowToInsert and will start inserting from that point 
			// backward
			//
			this.AddSiblingRows ( ref context, null, null );

			this.VerifyRowList( scrollContext.rowScrollRegion );

			Debug.Assert( null != oldTopRow, "No new top row in ViewStyle.OnScrollPageUp");

			// calculate the amount to scroll
			//
			if ( null != oldTopRow )
			{
				this.CalcScrollUpAmount( ref scrollContext,
					oldTopRow, currTopOfRow, oldTopRowOrigBottom ); // fPixelScroll FUTURE: add support for pixel scrolling			
			}

			// set the new top row
			//
			scrollContext.rowScrollRegion.SetFirstRow( visibleRows[0].Row );
		}
		
		#endregion // OnScrollPageUp 

		#region OnScrollPageDown 
		
		private void OnScrollPageDown ( ref ScrollRowsContext scrollContext )
		{
			VisibleRowsCollection visibleRows = scrollContext.VisibleRows;

			// set up the context structure
			//
			VisibleRowFetchRowContext context = new 
				VisibleRowFetchRowContext( scrollContext.rowScrollRegion ); // forward fetch

			// double the region height so that VerifyExistingRows and AdjustListToFillRegion
			// will load up another region's worth of rows
			//
			context.regionHeight = context.regionHeight * 2;

			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Got rid of row scroll region parameter since context already contains 
			// the row scroll region.
			//
			//if ( !this.VerifyExistingRows( scrollContext.rowScrollRegion, ref context ) )
			if ( !this.VerifyExistingRows( ref context ) )
			{
				scrollContext.invalidate     = true;
				scrollContext.scrollDelta    = 0;
				return;
			}

			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Got rid of row scroll region parameter since context already contains 
			// the row scroll region.
			//
			//this.AdjustListToFillRegion( scrollContext.rowScrollRegion, ref context );
			this.AdjustListToFillRegion( ref context );

			// get the old top row and save its band ptr it it has a header
			//			
			VisibleRow oldTopRow = visibleRows[0];

			UltraGridBand  oldTopHeaderBand = null != oldTopRow && oldTopRow.HasHeader
				? oldTopRow.Band
				: null;

			// calculate approximately where the cutoff is for where the new
			// top row is displayed now
			//
			// SSP 1/7/03 UWG1923
			// Substract the horizontal scroll bar height only if it's visible.
			//
			// ---------------------------------------------------------------------------------------------------
			//int newTopCutoff  = System.Math.Max( 1, scrollContext.rowScrollRegion.Extent - SystemInformation.HorizontalScrollBarHeight );
			bool isHorizontalScrollBarVisible = true;

			if ( !scrollContext.rowScrollRegion.IsLastVisibleRegion ||
				 ( null != scrollContext.rowScrollRegion.Layout.ColScrollRegions &&
				   null != scrollContext.rowScrollRegion.Layout.ColScrollRegions.LastVisibleRegion &&
				// SSP 11/4/04 UWG3593
				// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
				// that specifies whether to assume and if so to whether to assume the scrollbar
				// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
				// even when they are not needed because we assume the horizontal scrollbar is 
				// visible to find out if vertical scrollbar should be displayed or not.
				//
				//! scrollContext.rowScrollRegion.Layout.ColScrollRegions.LastVisibleRegion.WillScrollbarBeShown( true ) ) )
				! scrollContext.rowScrollRegion.Layout.ColScrollRegions.LastVisibleRegion.WillScrollbarBeShown( ScrollbarVisibility.Visible ) ) )
				isHorizontalScrollBarVisible = false;

			int newTopCutoff  = System.Math.Max( 1, 
				scrollContext.rowScrollRegion.Extent - ( isHorizontalScrollBarVisible ? SystemInformation.HorizontalScrollBarHeight : 0 ) );
			// ---------------------------------------------------------------------------------------------------

			// SSP 1/7/03 UWG1923
			// We shouldn't be taking into account the header heights because they are
			// included in the row's tops.
			//
			

			// check if we have reached the end of the rowset
			//
			if ( !this.CanScrollDownMore( ref scrollContext, oldTopHeaderBand, ref newTopCutoff ) )
				return;

			// pop off all the rows up to (but not including) the
			// new top row
			//
			// Never pop off the last row in the list since that will cause a GPF
			// so make sure the size is > 1 before popping a row
			//
			// SSP 1/7/03 UWG1923
			// We need to pop off all the rows that are fully visible. To check for that, we need
			// to check if the bottom is less than bottom of the row scroll region.
			// 
			// -------------------------------------------------------------------------------------
			
			// SSP 3/10/05 BR02635
			// Pop off at least one visible row because if the first row happened to be taller than 
			// the height of the entire scroll region then none of the rows would end up being popped
			// off and the visible rows would remain the sample. In other words the page down action
			// would not do anything.
			//
			//while ( visibleRows.Count > 1 && visibleRows[0].OrigTop + visibleRows[0].GetTotalHeight( ) <= newTopCutoff )
			int popFrontVisibleRowCount = 0;
			while ( visibleRows.Count > 1 && 
					( 0 == popFrontVisibleRowCount 
					  || visibleRows[0].OrigTop + visibleRows[0].GetTotalHeight( ) <= newTopCutoff ) )
			{
				// SSP 12/2/03 - Scrolling Till Last Row Visible
				// Added the functionality to not allow the user to scroll any further once the
				// last row is visible.
				// 
				// --------------------------------------------------------------------------
				if ( ! this.CanScrollDownMore( ref scrollContext, oldTopHeaderBand ) )
					break;
				// --------------------------------------------------------------------------
				
				scrollContext.rowScrollRegion.PopFrontVisibleRow();

				// SSP 3/10/05 BR02635
				// Related to the change above.
				//
				popFrontVisibleRowCount++;
			}
			// -------------------------------------------------------------------------------------
			
			VisibleRow newTopRow = visibleRows[0];

			// set the new top row
			//
			scrollContext.rowScrollRegion.SetFirstRow( newTopRow.Row );

			int newTopRowOrigBottom = newTopRow.Bottom;

			// call VerifyRowList to re-adjust all the top coordinates and add
			// new rows to the end of the visible rows list
			//
			this.VerifyRowList( scrollContext.rowScrollRegion );

			Debug.Assert( null != newTopRow, "No new top row in ViewStyle.OnScrollPageDown");

			// calculate the amount to scroll
			//
			if ( null != newTopRow )
			{
				this.CalcScrollDownAmount( ref scrollContext,
					newTopRow,
					newTopRowOrigBottom );
			}
		}
		
		#endregion // OnScrollPageDown 

		#region OnEndScroll

		void OnEndScroll( ref ScrollRowsContext scrollContext )
		{
			// Check if the ScrollOffset is non-zero. This means that during
			// a repeated (pixel) scrolling operation the top row was not
			// aligned to 0. Therefore we need to call OnScrollUp or OnScrollDown
			// to scroll one more row. Which will then end up being aligned properlt.
			// 
			if ( 0 != scrollContext.rowScrollRegion.scrollOffset )
			{
				// save the region rect
				//
				Rectangle regionRect = scrollContext.invalidRect;

				switch ( scrollContext.rowScrollRegion.lastScrollType )
				{
					case ScrollEventType.SmallDecrement :
						this.OnScrollUp( ref scrollContext );
						break;

					case ScrollEventType.SmallIncrement :						
					{
						VisibleRowsCollection visibleRows = scrollContext.VisibleRows;
						
						// if we have received an end scroll message we need
						// to pop off any rows that are completely scrolled up
						// out of view
						//
						VisibleRow frontRow = visibleRows[0];

						while ( null != frontRow && 
							frontRow.GetTop() + 
							frontRow.GetTotalHeight() +  
							frontRow.PostRowSpacing 
							< this.GetFixedHeaderHeight() )
						{
							// pop off this front row
							//
							scrollContext.rowScrollRegion.PopFrontVisibleRow();

							// call VerifyRowList to re-adjust all the top coordinates and add
							// new rows to the end of the visible rows list
							//
							this.VerifyRowList( scrollContext.rowScrollRegion );
							

							// get the new front row
							//
							frontRow = visibleRows.Count > 0 
								? visibleRows[0]
								: null;

							// SSP 11/30/01 UWG806
							// Somehow VerifyRowList was readding the same row that
							// we are popping off in the beginning of the loop causing
							// the loop to go into infite loop. Setting these to 0
							// seems to have fixed the problem.
							//
							// Added these.
							scrollContext.rowScrollRegion.scrollOffset     = 0;
							scrollContext.rowScrollRegion.lastScrollDelta  = 0;
							scrollContext.scrollDelta = 0;
						}
             
						this.OnScrollDown( ref scrollContext );

						// invalidate the entire region
						//
						scrollContext.invalidate = true;
						scrollContext.invalidRect   = regionRect;			
					
						break;
					}
				}
			}

			scrollContext.rowScrollRegion.scrollOffset     = 0;
			scrollContext.rowScrollRegion.lastScrollDelta  = 0;
		}

		#endregion // OnEndScroll
		
		#region ScrollRows

		#region ScrollRows_PageUpHelper

		internal UltraGridRow ScrollRows_PageUpHelper( 
			ref ScrollRowsContext scrollContext,
			ScrollbarVisibility colScrollbarVisibility,
			int maxIteration, UltraGridRow targetLastRow,
			bool returnWhenTargetRowFullyVisible )
		{
			for ( UltraGridRow lastRSRFirstRow = null 
				  // Non-positive maxIteration means no limit.
				  ; maxIteration <= 0 || 0 != --maxIteration 
				  ; lastRSRFirstRow = scrollContext.rowScrollRegion.InternalFirstRow )
			{

				this.ScrollRowsHelper( ref scrollContext );
				this.RecreateRowList( scrollContext.rowScrollRegion );

				if ( returnWhenTargetRowFullyVisible 
					&& scrollContext.VisibleRows.IsFullyVisible( targetLastRow, colScrollbarVisibility ) )
					return scrollContext.rowScrollRegion.InternalFirstRow;

				VisibleRow tmpVr = scrollContext.VisibleRows.GetFirstLastScrollableRow( false, true, colScrollbarVisibility );

				// Once the original first row becomes the last row we have completed the Page Up
				// operation.
				//
				if ( null == tmpVr 
					// Also break out if the row gets completely scrolled out of view. This can 
					// happen for example if there is not enough scrollable space to show a full 
					// visible row or when the heights of the rows differ in such a way that it
					// would be impossible to get the targetLastRow to be the last fully visible
					// row. This only applies to when we are trying to scroll down one row at a 
					// time the targetLastRow. If we are scrolling up one by one (scroll type is 
					// SmallIncrement) then don't do this check. For that the maxIteration will 
					// be passed in.
					//
					|| ! scrollContext.VisibleRows.HasAssociatedVisibleRow( targetLastRow ) 
					&& ScrollEventType.SmallIncrement != scrollContext.scrollType )
					break;

				// Once the original first row becomes the last row we have completed the Page Up
				// operation.
				//
				if ( targetLastRow == tmpVr.Row 
					// If for some reason the ScrollRows call above resulted in NO OP then break
					// out too other wise we will be in an infinite loop.
					//
					|| lastRSRFirstRow == scrollContext.rowScrollRegion.InternalFirstRow )
					return scrollContext.rowScrollRegion.InternalFirstRow;
			}

			return null;
		}

		#endregion // ScrollRows_PageUpHelper
				
		#region ScrollRowsHelper

		// SSP 7/14/05
		// Adde ScrollRowsHelper method. Code in there is moved from the ScrollRows method.
		// 
		internal void ScrollRowsHelper( ref ScrollRowsContext scrollContext )
		{
			UltraGridLayout layout = this.Layout;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//RowsCollection rootRows = layout.Rows;

			RowScrollRegion rsr = scrollContext.rowScrollRegion;
			int currentScrollBarPos = scrollContext.scrollTrackPos;
			int maxScrollPos = rsr.GetMaxScrollPosition( );
			ScrollbarVisibility colScrollbarVisibility = ScrollbarVisibility.Check;
			UltraGridRow oldFirstRow = rsr.FirstRow;

			int newScrollPos = -1;
			UltraGridRow newFirstRow = null;

			// SSP 7/14/05 - Optimizations
			// 
			scrollContext.findScrollableArea = scrollContext.findScrollableArea
				&&  ( ScrollEventType.SmallDecrement == scrollContext.scrollType 
				|| ScrollEventType.SmallIncrement == scrollContext.scrollType 
				|| ScrollEventType.ThumbTrack == scrollContext.scrollType );

			ScrollableAreaFinder.VisibleRowInfo[] origVisibleRowInfo = scrollContext.findScrollableArea 
				? ScrollableAreaFinder.GetVisibleRowInfos( ref scrollContext ) : null;

			switch ( scrollContext.scrollType )
			{
				case ScrollEventType.First:
				{
					newScrollPos = 0;
					break;
				}
				case ScrollEventType.Last:
				{
					newScrollPos = maxScrollPos;
					break;
				}
				case ScrollEventType.SmallDecrement:
				case ScrollEventType.SmallIncrement:
				{
					UltraGridRow firstRow = oldFirstRow;
					if ( null != firstRow )
					{
						int scrollPos = firstRow.ScrollPosition;
						int delta = ScrollEventType.SmallDecrement == scrollContext.scrollType ? -1 : 1;

						newScrollPos = delta + ( scrollPos >= 0 ? scrollPos : currentScrollBarPos );
						newScrollPos = Math.Max( 0, newScrollPos );
					}

					break;
				}					
				case ScrollEventType.LargeDecrement:
				case ScrollEventType.LargeIncrement:
				{
					// SSP 7/14/05 - Optimizations
					// 
					if ( ScrollbarVisibility.Check == colScrollbarVisibility )
						colScrollbarVisibility = layout.ColScrollRegions.AreScrollbarsVisible( ScrollbarVisibility.Visible )
							? ScrollbarVisibility.Visible : ScrollbarVisibility.Hidden;

					// Get the first or the last fully visible row depending on whether we are performing
					// a page down or page up operation.
					// 
					bool first = ScrollEventType.LargeDecrement == scrollContext.scrollType;
					VisibleRow vr = scrollContext.VisibleRows.GetFirstLastScrollableRow( first, true, colScrollbarVisibility );
					if ( null != vr )
					{
						if ( ScrollEventType.LargeIncrement == scrollContext.scrollType )
						{
							// When performing a Page Down operation make the last fully visible row the
							// first row.
							//
							newFirstRow = vr.Row;
						}
						else
						{
							// When performing a Page Up operation, the first row has to become the last
							// fully visible row. The following code scrolls up one row at a time until the
							// first row becomes the last fully visible row. Note that each scroll up operation
							// does not involve painting or ui element positioning and therefore it's efficient.
							// 

							try
							{
								// SSP 7/14/05 - Optimizations
								// Guess the new first row that will cause the current first row to become
								// the last fully visible row. Commented out the original code and added new
								// code.
								// 
								// ----------------------------------------------------------------------------
								int scrollPos = vr.Row.ScrollPosition;
								int delta = Math.Max( 1, scrollContext.VisibleRows.Count - 1 );
								UltraGridRow guessedFirstRow = layout.GetRowAtScrollPos( scrollPos - delta );
								if ( null != guessedFirstRow )
								{
									rsr.SetFirstRow( guessedFirstRow, false, false );
									this.RecreateRowList( rsr );

									// Get the last fully visible scrollable row.
									// 
									VisibleRow tmpVr = scrollContext.VisibleRows.GetFirstLastScrollableRow( false, true, colScrollbarVisibility );
									if ( null != tmpVr )
									{
										if ( tmpVr.Row == vr.Row )
										{
											// If the last scrollbale visible row is the same as the old first scrollable row then
											// we have performed the Page Up operation. There is nothing more to be done.
											// 
											newFirstRow = rsr.InternalFirstRow;
										}
										else if ( tmpVr.Row.ScrollPosition < scrollPos )
										{
											// If we scrolled too much (meaning the old first scrollbale row has been scrolled out
											// of view at the bottom) then scroll up one by one until the first scrollable row is
											// the last fully visible scrollable row. We do this in the for loop further below.
											// 
											scrollContext.scrollType = ScrollEventType.SmallIncrement;
											newFirstRow = this.ScrollRows_PageUpHelper( ref scrollContext, colScrollbarVisibility, delta, vr.Row, true );
										}
									}
								}

								if ( null == newFirstRow )
								{
									// If there is no fully visible scrollable row then fall back to performing
									// a SmallDecrement.
									// 
									rsr.SetFirstRow( vr.Row, false, false );
									this.RecreateRowList( scrollContext.rowScrollRegion );
									scrollContext.scrollType = ScrollEventType.SmallDecrement;

									newFirstRow = this.ScrollRows_PageUpHelper( ref scrollContext, colScrollbarVisibility, 0, vr.Row, false );
								}

								
								// ----------------------------------------------------------------------------
							}
							finally
							{
								scrollContext.scrollType = ScrollEventType.LargeDecrement;
							}
						}
					}
					// SSP 7/14/05
					// If the new first row is the same as the old first row then emulate 
					// SmallIncrement/SmallDecrement. This can happen for example there is the row's
					// height is bigger than the rsr.
					// 
					//else
					if ( null == vr || null == newFirstRow || newFirstRow == oldFirstRow )
					{
						newFirstRow = null;

						// If not even a single row is fully visible then simply scroll up or down by 
						// a single row. In this scenario the page down and up behave like the row down
						// and up.
						// 

						ScrollEventType origScrollType = scrollContext.scrollType;
						scrollContext.scrollType = ScrollEventType.LargeDecrement == origScrollType
							? ScrollEventType.SmallDecrement : ScrollEventType.SmallIncrement;

						try
						{
							this.ScrollRowsHelper( ref scrollContext );
						}
						finally
						{
							scrollContext.scrollType = origScrollType;
						}
					}

					break;
				}
				case ScrollEventType.ThumbTrack:
				{
					newScrollPos = scrollContext.scrollTrackPos * scrollContext.rowScrollRegion.scrollbarFactor;
					break;
				}
				case ScrollEventType.EndScroll:
				{
					this.OnEndScroll( ref scrollContext );
					break;
				}
			}

			if ( null != newFirstRow )
			{
				newScrollPos = newFirstRow.ScrollPosition;
				Debug.Assert( newScrollPos >= 0 );
			}

			newScrollPos = Math.Min( newScrollPos, maxScrollPos );
			if ( newScrollPos >= 0 )
				newFirstRow = layout.GetRowAtScrollPos( newScrollPos );

			if ( null != newFirstRow )
				rsr.SetFirstRow( newFirstRow, false, false );

			this.RecreateRowList( rsr );

			// SSP 7/14/05 - Optimizations
			// 
			//if ( ScrollEventType.SmallDecrement == scrollContext.scrollType 
			//	|| ScrollEventType.SmallIncrement == scrollContext.scrollType 
			//	|| ScrollEventType.ThumbTrack == scrollContext.scrollType )
			if ( scrollContext.findScrollableArea )
			{
				try
				{
					ScrollableAreaFinder.FindAreasToScrollHelper( ref scrollContext, origVisibleRowInfo );
				}
				catch
				{
					Debug.Assert( false );
					scrollContext.invalidate = true;
					scrollContext.invalidRect = new Rectangle( 0, rsr.Origin, 0, rsr.Extent );
				}
			}
			else 
			{
				scrollContext.invalidate = true;
				scrollContext.scrollDelta = 0;
			}
		}

		#endregion // ScrollRowsHelper

		internal virtual void ScrollRows( ref ScrollRowsContext scrollContext )
		{
			// first call VerifyRowList to make sure the rows are active
			//
			this.VerifyRowList( scrollContext.rowScrollRegion );

			VisibleRowsCollection visibleRows = scrollContext.VisibleRows;

			// if the current list is empty then call RecreateRowList and
			// exit
			//
			if ( 0 == visibleRows.Count )
			{
				this.RecreateRowList( scrollContext.rowScrollRegion );
				scrollContext.invalidate     = true;
				scrollContext.scrollDelta    = 0;
				return;
			}
			
			// SSP 3/28/05 - NAS 5.2 Fixed Rows
			// Added the following if block and enclosed the existing code into the else block.
			// 
			if ( this.SupportsFixedRows )
			{
				this.ScrollRowsHelper( ref scrollContext );
			}
			else
			{
				switch ( scrollContext.scrollType )
				{
					case ScrollEventType.First:
						this.OnScrollTop( ref scrollContext );
						break;
					case ScrollEventType.Last:
						this.OnScrollBottom( ref scrollContext );
						break;
					case ScrollEventType.SmallDecrement:
						this.OnScrollUp( ref scrollContext );
						break;
					case ScrollEventType.SmallIncrement:
						this.OnScrollDown( ref scrollContext );
						break;
					case ScrollEventType.LargeDecrement:
						this.OnScrollPageUp( ref scrollContext );
						break;
					case ScrollEventType.LargeIncrement:
						this.OnScrollPageDown( ref scrollContext );
						break;
					case ScrollEventType.EndScroll:
						this.OnEndScroll( ref scrollContext );
						break;
				}
			}

			scrollContext.rowScrollRegion.LastScrollType = scrollContext.scrollType;
		}

		#endregion // ScrollRows

		#region ScrollRowsCancelled

		internal virtual void ScrollRowsCancelled( ref ScrollRowsContext scrollContext )
		{			
			if ( 0 != scrollContext.rowScrollRegion.ScrollOffset )
			{
				// set the offset back to zero
				//
				scrollContext.rowScrollRegion.ScrollOffset = 0;

				// call VerifyRowList to realign all elemrows back
				// to a top of zero
				//
				this.VerifyRowList( scrollContext.rowScrollRegion );
			}
		}

		#endregion // ScrollRowsCancelled

		// SSP 4/27/05 - NAS 5.2 Fixed Rows
		// 
		#region NAS 5.2 Fixed Rows

		#region Using_CreateRowsList_FixedRowsFeature

		// SSP 8/3/06 BR14784
		// Added Using_CreateRowsList_FixedRowsFeature. Code in there was moved from RecreateRowList method.
		// 
		internal bool Using_CreateRowsList_FixedRowsFeature
		{
			get
			{
				// SSP 7/18/05 - NAS 5.3 Header Placement
				//
				// ------------------------------------------------------------------
				bool headerPlacementInEffect = false;
				foreach ( UltraGridBand band in this.Layout.SortedBands )
				{
					if ( !band.HiddenResolved && HeaderPlacement.RepeatOnBreak != band.HeaderPlacementResolved )
					{
						headerPlacementInEffect = true;
						break;
					}
				}
				// ------------------------------------------------------------------

				// SSP 4/29/05 - NAS 5.2 Fixed Rows_
				// Added the if block and enclosed the existing code into the else block.
				//
				if (
					// SSP 7/18/05 - NAS 5.3 Header Placement
					// Added headerPlacementInEffect condition.
					// 
					headerPlacementInEffect ||
					this.SupportsFixedRows && ( this.Layout.Rows.HasFixedRows( true ) || this.Layout.Rows.HasFixedRows( false ) ) )
					return true;

				return false;

			}
		}

		#endregion // Using_CreateRowsList_FixedRowsFeature

		#region CreateRowsList_FixedRowsFeature

		/// <summary>
		/// Generates visible rows.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="parentVisibleRow"></param>
		/// <param name="firstSibling"></param>
		/// <returns></returns>
		protected bool CreateRowsList_FixedRowsFeature( ref VisibleRowFetchRowContext context, 
			VisibleRow parentVisibleRow, 
			Infragistics.Win.UltraWinGrid.UltraGridRow firstSibling ) 
		{
			// First add the bottom fixed rows. This way we can find out how far down the scrolling
			// rows should go down to.
			// 
			VisibleRowFetchRowContext bottomFixedRowsContext = context;

			// SSP 9/14/07 BR25887
			// Added isBottomFixedRowsContext.
			// 
			bottomFixedRowsContext.isBottomFixedRowsContext = true;

			this.AddBottomFixedRows( ref bottomFixedRowsContext );
			if ( null != bottomFixedRowsContext.lastRowInserted )
				context.regionHeight -= bottomFixedRowsContext.lastRowInserted.BottomOverall;

			VisibleRowsCollection visibleRows = context.VisibleRows;
			int bottomFixedRowsCount = visibleRows.Count;

			// Add the top fixed rows.
			// 
			this.AddTopFixedRows( ref context );

			UltraGridRow row = context.beginAtRow;
			int scrollPos = row.ScrollPosition;
			Hashtable parentVisibleRowMap = new Hashtable( );
			if ( null != parentVisibleRow )
				parentVisibleRowMap[ parentVisibleRow.Row ] = parentVisibleRow;
			
			do
			{
				// Skip the fixed rows since they are processed above.
				// 
				if ( ! row.FixedResolved )
					this.AddVisibleRow( ref context, row );

				scrollPos++;
				row = context.rowScrollRegion.Layout.GetRowAtScrollPos( scrollPos );
			} 
			while ( null != row && ! this.EndOfRegion( ref context ) );

			Debug.Assert( visibleRows.Count >= bottomFixedRowsCount );
			// SSP 8/4/05 BR05320
			// 
			//if ( bottomFixedRowsCount > 0 && visibleRows.Count > bottomFixedRowsCount )
			if ( bottomFixedRowsCount > 0 && visibleRows.Count >= bottomFixedRowsCount )
			{
				// Move the bottom fixed rows to the end of the visible rows collection.
				//
				visibleRows.InternalMoveRowsToEnd( 0, bottomFixedRowsCount );

				// Fixup the tops of the bottom fixed rows. We had added them relative to 0.
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.OffsetRows( ref context, visibleRows.Count - bottomFixedRowsCount, bottomFixedRowsCount, context.regionHeight );
				ViewStyleBase.OffsetRows( ref context, visibleRows.Count - bottomFixedRowsCount, bottomFixedRowsCount, context.regionHeight );

				VisibleRow firstBottomFixedRow = visibleRows[ visibleRows.Count - bottomFixedRowsCount ];

				// Remove scrollable rows that are scrolled completely underneath the bottom
				// fixed rows.
				//
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//bool needsVertScrollbar = this.RemoveScrollableRowsBelowLocation( ref context, firstBottomFixedRow.Top );
				bool needsVertScrollbar = ViewStyleBase.RemoveScrollableRowsBelowLocation( ref context, firstBottomFixedRow.Top );

				needsVertScrollbar = needsVertScrollbar || ! context.rowScrollRegion.IsFirstScrollableRowVisible( ScrollbarVisibility.Hidden );
				if ( this.Layout.ColScrollRegions.AreScrollbarsVisible( 
					needsVertScrollbar ? ScrollbarVisibility.Visible : ScrollbarVisibility.Hidden ) )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.OffsetRows( ref context, visibleRows.Count - bottomFixedRowsCount, bottomFixedRowsCount, 
					//    - SystemInformation.HorizontalScrollBarHeight );
					ViewStyleBase.OffsetRows( ref context, visibleRows.Count - bottomFixedRowsCount, bottomFixedRowsCount,
						-SystemInformation.HorizontalScrollBarHeight );

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.RemoveScrollableRowsBelowLocation( ref context, firstBottomFixedRow.Top );
					ViewStyleBase.RemoveScrollableRowsBelowLocation( ref context, firstBottomFixedRow.Top );
				}

				// If the row before the first bottom fixed row is from a different row collection
				// then display the column headers on the bottom fixed row.
				//
                if (! firstBottomFixedRow.HasHeader && visibleRows.Count > bottomFixedRowsCount 
					&& visibleRows[ visibleRows.Count - bottomFixedRowsCount - 1 ].Row.ParentCollection != firstBottomFixedRow.Row.ParentCollection 
                    // MRS 8/19/2009 - TFS20637
                    // Don't do this if the fixed row is a GroupByRow. 
                    //
                    && ! firstBottomFixedRow.Row.IsGroupByRow)
				{
					int origBottom = firstBottomFixedRow.BottomOverall;

					// SSP 7/18/05 - NAS 5.3 Header Placement
					// 
					//firstBottomFixedRow.HasHeader = true;
					this.SetHasHeaderHelper( ref context, firstBottomFixedRow, true );

					int newBottom = firstBottomFixedRow.BottomOverall;
					Debug.Assert( newBottom >= origBottom );

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.OffsetRow( firstBottomFixedRow, origBottom - newBottom );
					ViewStyleBase.OffsetRow( firstBottomFixedRow, origBottom - newBottom );

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.RemoveScrollableRowsBelowLocation( ref context, firstBottomFixedRow.Top );
					ViewStyleBase.RemoveScrollableRowsBelowLocation( ref context, firstBottomFixedRow.Top );
				}
			}

			// FirstRow proeprty of the RowScrollRegion should return the first scrollable
			// row. If FirstRow off the row scroll region was a fixed row then set the FirstRow
			// of the RowScrollRegion to the first scrolling row in the generated visible rows.
			//
			VisibleRow firstScrollableRow = context.VisibleRows.GetFirstScrollableRow( );
			if ( null != firstScrollableRow )
				context.rowScrollRegion.SetFirstRow( firstScrollableRow.Row, false, false );

			return true;
		}

		#endregion // CreateRowsList_FixedRowsFeature

		#region RemoveScrollableRowsBelowLocation
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool RemoveScrollableRowsBelowLocation( ref VisibleRowFetchRowContext context, int location )
		internal static bool RemoveScrollableRowsBelowLocation( ref VisibleRowFetchRowContext context, int location )
		{
			bool needsVertScrollbar = false;
			VisibleRowsCollection vrColl = context.VisibleRows;
			for ( int i = vrColl.Count - 1; i >= 0; i-- )
			{
				VisibleRow vr = vrColl[ i ];
				if ( ! vr.Row.FixedResolved )
				{
					if ( vr.Top > location )
					{
						needsVertScrollbar = true;
						context.VisibleRows.InternalRemoveAt( i );
					}
					else if ( vr.BottomOverall > location )
					{
						vr.IsFullyVisibleOverride = DefaultableBoolean.False;
						vr.ClipBottomTo = location - vr.Top;
						needsVertScrollbar = true;
						break;
					}
				}
			}

			return needsVertScrollbar;
		}

		#endregion // RemoveScrollableRowsBelowLocation

		#region OffsetRows

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void OffsetRow( VisibleRow vr, int amountToOffset )
		private static void OffsetRow( VisibleRow vr, int amountToOffset )
		{
			vr.SetTop( vr.Top + amountToOffset );
		}
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void OffsetRows( ref VisibleRowFetchRowContext context, int index, int count, int amountToOffset )
		private static void OffsetRows( ref VisibleRowFetchRowContext context, int index, int count, int amountToOffset )
		{
			VisibleRowsCollection vrColl = context.VisibleRows;
			for ( int i = 0; i < count; i++ )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.OffsetRow( vrColl[ i + index ], amountToOffset );
				ViewStyleBase.OffsetRow( vrColl[ i + index ], amountToOffset );
			}
		}

		#endregion // OffsetRows

		#region GetParentVisibleRow

		private VisibleRow GetParentVisibleRowHelper( ref VisibleRowFetchRowContext context, UltraGridRow childRow )
		{
			UltraGridRow parentRow = childRow.ParentRow;
			VisibleRow parentVr = null != parentRow ? (VisibleRow)context.ParentVisibleRowMap[ parentRow ] : null;
			if ( null != parentRow && null == parentVr )
			{
				parentVr = context.VisibleRows.VisibleRowFromCache;
				parentVr.Initialize( this.GetParentVisibleRowHelper( ref context, parentRow ), parentRow );
				context.ParentVisibleRowMap[ parentRow ] = parentVr;
			}

			return parentVr;
		}

		#endregion // GetParentVisibleRow

		#region AddVisibleRow

		// SSP 7/21/05 - NAS 5.3 Header Placement
		// 
		private VisibleRow AddHeadersHelper( ref VisibleRowFetchRowContext context, UltraGridRow row )
		{
			if ( row is HeadersRow )
				return null;

			if ( null == context.lastRowInserted || row.Band.IsTrivialDescendantOf( context.lastRowInserted.Band ) )
			{
				int stopHierarchyLevel = null != context.lastRowInserted 
					? 1 + context.lastRowInserted.Row.OverallHierarchyLevel : 0;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = new ArrayList( );
				List<HeadersRow> list = new List<HeadersRow>();

				UltraGridRow iiRow = row.ParentRow;
				while ( null != iiRow && iiRow.OverallHierarchyLevel >= stopHierarchyLevel )
				{
					UltraGridBand iiBand = iiRow.Band;
					HeaderPlacement headerPlacement = iiBand.HeaderPlacementResolved;

					bool addHeadersRow = false;

					if ( HeaderPlacement.OncePerRowIsland == headerPlacement )
					{
						if ( ! iiRow.IsGroupByRow )
							addHeadersRow = true;
					}
					else if ( HeaderPlacement.OncePerGroupedRowIsland == headerPlacement )
					{
						if ( iiRow.ParentCollection.IsTopLevel )
							addHeadersRow = true;
					}

					if ( addHeadersRow )
					{
						// SSP 9/22/06 BR14784
						// 
						//HeadersRow headersRow = new HeadersRow( iiRow.ParentCollection );
						HeadersRow headersRow = new HeadersRow( iiRow.ParentCollection, true );

						list.Add( headersRow );
					}

					iiRow = iiRow.ParentRow;
				}

				list.Reverse( );
				VisibleRow prevRow = null;
				foreach ( HeadersRow headersRow in list )
				{
					this.AddVisibleRow( ref context, headersRow );

					if ( null != context.lastRowInserted )
						context.lastRowInserted.AttachedHeadersRow = prevRow;

					prevRow = context.lastRowInserted;
				}

				return prevRow;
			}

			return null;
		}

		internal void AddVisibleRow( ref VisibleRowFetchRowContext context, UltraGridRow row )
		{
			// SSP 7/21/05 - NAS 5.3 Header Placement
			// 
			VisibleRow attachHeadersRow = this.AddHeadersHelper( ref context, row );

			VisibleRow parentVr = this.GetParentVisibleRowHelper( ref context, row );
			VisibleRow vr = context.VisibleRows.VisibleRowFromCache;
			vr.Initialize( parentVr, row );

			context.ParentVisibleRowMap[ row ] = vr;

			// SSP 7/21/05 - NAS 5.3 Header Placement
			// Only add if not HeadersRow.
			// 
			// SSP 9/22/06 BR14784
			// 
			// --------------------------------------------------------------------------------------
			HeadersRow headersRow = vr.Row as HeadersRow;
			if ( null == headersRow || ! headersRow.IsAttachedHeader )
				context.VisibleRows.InternalAdd( vr );
			//if ( ! ( vr.Row is HeadersRow ) )
			//	context.VisibleRows.InternalAdd( vr );
			// --------------------------------------------------------------------------------------

			if ( context.fetchBackward )
				this.OrientVisibleRowBackward( ref context, vr );
			else
				this.OrientVisibleRowForward( ref context, vr );

			context.lastRowInserted = vr;

			// SSP 7/21/05 - NAS 5.3 Header Placement
			// 
            vr.AttachedHeadersRow = attachHeadersRow;
		}

		#endregion // AddVisibleRow

		#region AddTopFixedRows

		internal void AddTopFixedRows( ref VisibleRowFetchRowContext context )
		{
			UltraGridLayout layout = this.Layout;
			RowsCollection rows = layout.Rows;
			UltraGridRow[] topFixedRows = rows.GetFixedRows( true );

			if ( null != topFixedRows && topFixedRows.Length > 0 )
			{
				for ( int i = 0; i < topFixedRows.Length; i++ )
				{
					UltraGridRow row = topFixedRows[ i ];
					this.AddVisibleRow( ref context, row );
				}
			}
		}

		#endregion // AddTopFixedRows

		#region AddBottomFixedRows

		internal void AddBottomFixedRows( ref VisibleRowFetchRowContext context )
		{
			UltraGridLayout layout = this.Layout;
			RowsCollection rows = layout.Rows;
			UltraGridRow[] bottomFixedRows = rows.GetFixedRows( false );

			if ( null != bottomFixedRows )
			{
				for ( int i = 0; i < bottomFixedRows.Length; i++ )
				{
					UltraGridRow row = bottomFixedRows[ i ];
					this.AddVisibleRow( ref context, row );

					// Force the bottom fixed rows to not have any headers.
					//
					if ( 0 == i && null != context.lastRowInserted )
						// SSP 7/18/05 - NAS 5.3 Header Placement
						// 
						//context.lastRowInserted.HasHeader = false;
						this.SetHasHeaderHelper( ref context, context.lastRowInserted, false );
				}
			}
		}

		#endregion // AddBottomFixedRows

		#region ScrollableAreaFinder Class

		private class ScrollableAreaFinder
		{
			#region VisibleRowInfo Class

			internal class VisibleRowInfo
			{
				internal UltraGridRow row = null;
				internal int top;
				internal int bottom;
			
				internal VisibleRowInfo( VisibleRow vr )
				{
					this.row = vr.Row;
					this.top = vr.Top;
					this.bottom = vr.Bottom;
				}
			
				public override int GetHashCode( )
				{
					return this.row.GetHashCode( ) ^ ( this.bottom - this.top );
				}

				public override bool Equals( object o )
				{
					VisibleRowInfo vri = o as VisibleRowInfo;

					return null != vri && this.row == vri.row
						// Check for the height.
						//
						&& this.bottom - this.top == vri.bottom - vri.top;
				}
			}

			#endregion // VisibleRowInfo  Class

			#region Private Vars

			/// <summary>
			/// Old visible rows.
			/// </summary>
			private VisibleRowInfo[] oo = null;

			/// <summary>
			/// New visible rows.
			/// </summary>
			private VisibleRowInfo[] nn = null;

			/// <summary>
			/// Extent of the row scroll region.
			/// </summary>
			private int rsrExtent;

			#endregion // Private Vars

			#region Constructor

			private ScrollableAreaFinder( ref ScrollRowsContext scrollContext, VisibleRowInfo[] oldVisibleRows, VisibleRowInfo[] newVisibleRows )
			{
				this.oo = oldVisibleRows;
				this.nn = newVisibleRows;
				this.rsrExtent = ScrollableAreaFinder.GetRsrExtent( scrollContext.rowScrollRegion );
			}

			#endregion // Constructor

			#region GetRsrExtent
			
			private static int GetRsrExtent( RowScrollRegion rsr )
			{
				int rsrExtent = rsr.Extent;

				// RowScrollRegion's Extent property includes the horizontal scrollbar height.
				// We have to take it out here. If the following code doesn't work then always 
				// assuming that there is a horizontal scrollbar visible will also work.
				//
				bool hasHorizScrollbar = rsr.IsLastVisibleRegion;
				UltraGridLayout layout = rsr.Layout;

                // MBS 4/2/09 
                // Discovered this while implementing the NA9.2 CellBorderColor feature.  Basically, this code ignores
                // the IsLastVisibleRegion property when checking the DataAreaUIElement for a scrollbar.  The problem with
                // this is that it could very well find a scrollbar as a child, but since the DataAreaUIElement is one
                // of the upper-most parent elements, it doesn't mean that this scrollbar is a child of the RowScrollRegion.
                // If for whatever reason this check breaks something, the other alternative fix is to get the RowScrollRegion's
                // UIElement and see if the ColScrollbarUIElement is a child of that.
                //
                // The test case where this was discovered was if a row in the top RowScrollRegion (i.e. not last visible region)
                // is partially visible and the horizontal scrollbars are shown at the bottom, scrolling down with the keyboard
                // would not properly invalidate the area below the active row because we think there are scrollbars there.
                //
				//if ( null != layout && null != layout.DataAreaElement )
                if (hasHorizScrollbar && null != layout && null != layout.DataAreaElement )
				{
					UIElement horizScrollbarElem = GridUtils.GetChild( layout.DataAreaElement, typeof( ColScrollbarUIElement ) );
					hasHorizScrollbar = null != horizScrollbarElem;
				}

                if (hasHorizScrollbar)
                    rsrExtent -= SystemInformation.HorizontalScrollBarHeight;

				return rsrExtent;
			}

			#endregion // GetRsrExtent

			#region GetVisibleRowInfos

			internal static VisibleRowInfo[] GetVisibleRowInfos( ref ScrollRowsContext scrollContext )
			{
				VisibleRowsCollection vrColl = scrollContext.VisibleRows;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = new ArrayList( vrColl.Count );
				List<VisibleRowInfo> list = new List<VisibleRowInfo>( vrColl.Count );

				int rsrExtent = ScrollableAreaFinder.GetRsrExtent( scrollContext.rowScrollRegion );

				for ( int i = 0, count = vrColl.Count; i < count; i++ )
				{
					VisibleRow vr = vrColl[i];
                    VisibleRowInfo vri = new VisibleRowInfo( vr );
					if ( vri.top >= 0 && vri.bottom <= rsrExtent && ! vr.IsClipBottomToSet )
						list.Add( vri );
				}

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//return (VisibleRowInfo[])list.ToArray( typeof( VisibleRowInfo ) );
				return list.ToArray();
			}

			#endregion // GetVisibleRowInfos

			#region FindAreasToScrollHelper
			
			internal static void FindAreasToScrollHelper( ref ScrollRowsContext scrollContext, VisibleRowInfo[] oldVisibleRows )
			{
				VisibleRowInfo[] newVisibleRows = ScrollableAreaFinder.GetVisibleRowInfos( ref scrollContext );
				ScrollableAreaFinder i = new ScrollableAreaFinder( ref scrollContext, oldVisibleRows, newVisibleRows );
				i.FindAreasToScroll( ref scrollContext );
			}

			#endregion // FindAreasToScrollHelper

			#region Matches
			
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//internal bool Matches( VisibleRowInfo oldVr, VisibleRowInfo newVr, int ooDelta )
			internal static bool Matches( VisibleRowInfo oldVr, VisibleRowInfo newVr, int ooDelta )
			{
				return oldVr.row == newVr.row 
					&& ooDelta + oldVr.top == newVr.top 
					&& ooDelta + oldVr.bottom == newVr.bottom;
			}

			#endregion // Matches
			
			#region GetMatchingRowsCount
			
			internal int GetMatchingRowsCount( int ooStart, int nnStart, bool forward, int ooDelta )
			{
				int i;
				int matchingRowsCount = 0;

				if ( forward )
				{
					for ( i = 0; i < Math.Min( oo.Length - ooStart, nn.Length - nnStart ); i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( this.Matches( oo[ ooStart + i ], nn[ nnStart + i ], ooDelta ) )
						if ( ScrollableAreaFinder.Matches( oo[ ooStart + i ], nn[ nnStart + i ], ooDelta ) )
							matchingRowsCount++;
						else
							break;
					}
				}
				else
				{
					for ( i = 0; i <= Math.Min( ooStart , nnStart ); i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( this.Matches( oo[ ooStart - i ], nn[ nnStart - i ], ooDelta ) )
						if ( ScrollableAreaFinder.Matches( oo[ ooStart - i ], nn[ nnStart - i ], ooDelta ) )
							matchingRowsCount++;
						else
							break;
					}
				}

				return matchingRowsCount;
			}

			#endregion // GetMatchingRowsCountf
			
			#region FindAreasToScroll

			internal void FindAreasToScroll( ref ScrollRowsContext scrollContext )
			{
				int i;

				int topSameRowsCount = this.GetMatchingRowsCount( 0, 0, true, 0 );
				int bottomSameRowsCount = this.GetMatchingRowsCount( oo.Length - 1, nn.Length - 1, false, 0 );

				// SSP 10/31/05 BR07264
				// 
				bool invalidateEntireRegion = true;

				if ( topSameRowsCount + bottomSameRowsCount < nn.Length )
				{
					Rectangle scrollRect = Rectangle.Empty;
					Rectangle invalidRect1 = Rectangle.Empty;
					Rectangle invalidRect2 = Rectangle.Empty;
					int scrollDelta = 0;

					int iiFirstOldScrollableRow = topSameRowsCount;
					int iiLastOldScrollableRow = oo.Length - bottomSameRowsCount - 1;

					int invalidAreaTop = topSameRowsCount > 0 ? nn[ topSameRowsCount - 1 ].bottom : 0;
					int invalidAreaBottom = bottomSameRowsCount > 0 ? nn[ nn.Length - bottomSameRowsCount ].top : rsrExtent;

					// SSP 9/9/05 BR06224
					// Enclosed the following code into the if block. Make sure the indexes are
					// within range.
					// 
					if ( iiFirstOldScrollableRow >= 0 && iiLastOldScrollableRow >= 0 )
					{
						i = Array.IndexOf( nn, oo[ iiFirstOldScrollableRow ] );

						// Since the new first row may go from having header to not having header
						// it may not match. In that case try matching the second row. This is the
						// case where we are performing scroll down by a single row.
						//
						if ( i < 0 && nn.Length > 1 + iiFirstOldScrollableRow && oo.Length > 1 + iiFirstOldScrollableRow
							// SSP 9/6/06 - NAS 6.3 - Optimizations - BR13748 BR14522
							// When thumb-tracking, more than 1 row may get scrolled up at a time.
							
							// 
							//&& nn[ 1 + iiFirstOldScrollableRow ].row == oo[ iiFirstOldScrollableRow ].row 
							)
						{
							iiFirstOldScrollableRow++;
							i = Array.IndexOf( nn, oo[ iiFirstOldScrollableRow ] );

							// SSP 9/6/06 - NAS 6.3 - Optimizations - BR13748 BR14522
							// Change related to above change. This makes sure that we only do this when
							// scrolling down. That is when the row was above in the old rows collection and
							// now it's below.
							
							if ( i < iiFirstOldScrollableRow )
								i = -1;
						}

						if ( i >= 0 )
						{
							// Scrolling rows down.
							//

							scrollDelta = nn[i].top - oo[ iiFirstOldScrollableRow ].top;
							int matchingRowsCount = this.GetMatchingRowsCount( iiFirstOldScrollableRow, i, true, scrollDelta );
							if ( matchingRowsCount > 0 )
							{
								scrollRect.Y = nn[ i ].top - scrollDelta;
								scrollRect.Height = nn[ i + matchingRowsCount - 1 ].bottom - nn[ i ].top;
							}
						}
						else
						{
							i = Array.IndexOf( nn, oo[ iiLastOldScrollableRow ] );
							if ( i >= 0 )
							{
								// Scrolling rows up.
								//

								scrollDelta = nn[i].bottom - oo[ iiLastOldScrollableRow ].bottom ;
								int matchingRowsCount = this.GetMatchingRowsCount( iiLastOldScrollableRow, i, false, scrollDelta );
								if ( matchingRowsCount > 0 )
								{
									scrollRect.Y = nn[ i - matchingRowsCount + 1 ].top - scrollDelta;
									scrollRect.Height = nn[ i ].bottom - nn[ i - matchingRowsCount + 1 ].top;
								}
							}
						}
					}

					if ( ! scrollRect.IsEmpty )
					{
						scrollContext.scrollDelta = scrollDelta;

						invalidRect1.Y = invalidAreaTop;
						invalidRect1.Height = scrollRect.Y + scrollDelta - invalidRect1.Y;

						invalidRect2.Y = scrollRect.Bottom + scrollDelta;
						invalidRect2.Height = invalidAreaBottom - invalidRect2.Y;

						// OnScroll method updates invalidRect2 first and then invalidRect1. If 
						// scrolling down then cause the first update to coincide with the rect
						// that gets exposed as a result of scroll window operation.
						//
						if ( scrollDelta > 0 )
						{
							Rectangle tmp = invalidRect1;
							invalidRect1 = invalidRect2;
							invalidRect2 = tmp;
						}

						// Since all the calculations above were relative to the top left of the
						// row scroll region, offset them by the origin.
						//
						int rsrOrigin = scrollContext.rowScrollRegion.Origin;
						scrollRect.Offset( 0, rsrOrigin );
						invalidRect1.Offset( 0, rsrOrigin );
						invalidRect2.Offset( 0, rsrOrigin );

						scrollContext.scrollRect = scrollRect;
						scrollContext.invalidRect = invalidRect1;
						scrollContext.invalidRect2 = invalidRect2;
						scrollContext.invalidate = true;

						// SSP 10/31/05 BR07264
						// 
						invalidateEntireRegion = false;
					}
					// SSP 10/31/05 BR07264
					// 
					
				}
				// SSP 9/6/06 BR13748
				// 
				else if ( topSameRowsCount == bottomSameRowsCount && topSameRowsCount == nn.Length && nn.Length > 0 )
				{
					if ( nn[0].top == oo[0].top )
						invalidateEntireRegion = false;
				}

				// SSP 10/31/05 BR07264
				// 
				if ( invalidateEntireRegion )
				{
					// If we weren't able to find the scrollable area then invalidate the entire
					// scroll region. To do so assign a non-zero dummy value to scrollDelta.
					//
					scrollContext.scrollDelta = -1;
					scrollContext.invalidate = true;
				}
			}

			#endregion // FindAreasToScroll

		}

		#endregion // ScrollableAreaFinder Class


		#endregion // NAS 5.2 Fixed Rows

		#region InternalOrientVisibleRowForward

		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// Added InternalOrientVisibleRowForward method.
		// 
		internal void InternalOrientVisibleRowForward( ref VisibleRowFetchRowContext context, VisibleRow visibleRow )
		{
			this.OrientVisibleRowForward( ref context, visibleRow );
		}

		#endregion // InternalOrientVisibleRowForward

		// SSP 7/18/05 - NAS 5.3 Header Placement
		// 
		#region NAS 5.3 Header Placement
		
		internal void SetHasHeaderHelper( ref VisibleRowFetchRowContext context, VisibleRow vr, bool hasHeader )
		{
			vr.HasHeader = hasHeader;

			if ( ! this.HasMultiRowTiers )
			{
				HeaderPlacement headerPlacement = vr.Band.HeaderPlacementResolved;
				if ( HeaderPlacement.OncePerRowIsland == headerPlacement 
					|| HeaderPlacement.OncePerGroupedRowIsland == headerPlacement )
				{
					vr.HasHeader = false;

					object key;
					if ( HeaderPlacement.OncePerRowIsland == headerPlacement )
						key = vr.Row.ParentCollection;
					else
						key = vr.Row.ParentCollection.TopLevelRowsCollection;

					if ( ! context.CurrentHeaders.ContainsKey( key ) )
					{
						context.CurrentHeaders[ key ] = DBNull.Value;
						vr.HasHeader = hasHeader;
					}
				}
				else if ( HeaderPlacement.FixedOnTop == headerPlacement )
				{
					vr.HasHeader = false;
				}
			}
		}

		#endregion // NAS 5.3 Header Placement

        // MBS 4/23/09 - TFS12665
        #region IsInRecreateRowList

        internal bool IsInRecreateRowList
        {
            get { return this.isInRecreateRowList; }
        }
        #endregion //IsInRecreateRowList
    }

	#endregion // ViewStyleBase Class

	#region ScrollRowsContext Struct
	
	internal struct ScrollRowsContext
	{
		internal ScrollRowsContext(
			ScrollEventType scrollType,
			RowScrollRegion rowScrollRegion,
			int scrollTrackPos,
			int scrollMin,
			int scrollMax,
			Rectangle regionRect)
		{
		
			this.scrollType			= scrollType;
			this.rowScrollRegion	= rowScrollRegion;
			this.scrollTrackPos		= scrollTrackPos;
			this.scrollMin			= scrollMin;
			this.scrollMax			= scrollMax;
			this.scrollRect			= regionRect;
			this.invalidRect		= regionRect;
			this.scrollDelta		= 0;
			this.invalidate			= false;			
			this.forceScroll		= false;

			// SSP 4/27/05 - NAS 5.2 Fixed Rows
			//
			this.visibleRows = this.rowScrollRegion.VisibleRows;
			this.invalidRect2 = Rectangle.Empty;

			// SSP 7/14/05 - Fixed Rows / Optimizations
			// 
			UseScrollWindow useScrollWindow = this.rowScrollRegion.Layout.UseScrollWindow;
            this.findScrollableArea = UseScrollWindow.VerticalOnly == useScrollWindow || UseScrollWindow.Both == useScrollWindow;
		}

		internal ScrollEventType	scrollType;
		internal RowScrollRegion	rowScrollRegion;		
		internal int				scrollTrackPos;
		internal int				scrollMin;
		internal int				scrollMax;		
		internal Rectangle        scrollRect;
		internal Rectangle        invalidRect;
		internal int				scrollDelta;
		internal bool				invalidate;
		internal bool				forceScroll; // used to force a scroll to bottom

		// SSP 4/27/05 - NAS 5.2 Fixed Rows
		//
		private VisibleRowsCollection visibleRows;
		internal Rectangle invalidRect2;
		
		// SSP 7/14/05 - Fixed Rows / Optimizations
		// 
		internal bool findScrollableArea;

		internal VisibleRowsCollection VisibleRows
		{
			get
			{
				return this.visibleRows;
			}
		}
	};

	#endregion // ScrollRowsContext Struct

	#region VisibleRowFetchRowContext Struct
	
	internal struct VisibleRowFetchRowContext
	{	
		// SSP 4/27/05
		// Use the other overload of the constructor instead of duplicating the code.
		//
		internal VisibleRowFetchRowContext( RowScrollRegion rowScrollRegion ) : this( rowScrollRegion, false )
		{
		}
		

		internal VisibleRowFetchRowContext( RowScrollRegion rowScrollRegion, bool fetchBackward)
		{				
			this.rowScrollRegion	= rowScrollRegion;
			this.regionHeight		= rowScrollRegion.Extent;
			this.fetchBackward		= fetchBackward;
			this.maxTiersToAdd		= 0;
			this.lastTierAdded		= -1;
			this.tiersAdded			= 0;
			this.lastRowInserted	= null; 
			this.insertBefore		= null;
			this.insertAfter		= null;
			this.beginAtRow			= null;
			this.beginAtVisibleRow	= null;
        
			// SSP 5/31/02 UWG1152
			// Use the newly added Layout.MaxBandDepth property instead of using
			// a constant.
			//
			//this.headerBands = new UltraGridBand[VisibleRowFetchRowContext.MaxBandDepth];
			Debug.Assert( null != rowScrollRegion.Layout, "No layout off the rowScrollRegion !" );
			int maxBandDepthInUse = null != rowScrollRegion.Layout ? rowScrollRegion.Layout.MaxBandDepthEnforced : UltraGridBand.MAX_BAND_DEPTH;
			this.headerBands = new UltraGridBand[maxBandDepthInUse];

			// SSP 4/27/05 - NAS 5.2 Fixed Rows
			//
			this.visibleRows = this.rowScrollRegion.VisibleRows;
			this.parentVisibleRowMap = null;

			// SSP 7/18/05 - NAS 5.3 Header Placement
			// 
			this.currentHeaders = null;

			// SSP 9/14/07 BR25887
			// Added isBottomFixedRowsContext.
			// 
			this.isBottomFixedRowsContext = false;
		}

		// SSP 5/31/02 UWG1152
		// Added a new MaxBandDepth property so use that instead of hardcoding MaxBandDepth
		// constant.
		//
		//const int MaxBandDepth = 100;

		internal RowScrollRegion rowScrollRegion;
		internal bool          fetchBackward;
		internal int           regionHeight;
		internal int           lastTierAdded;
		internal int         tiersAdded;
		internal int         maxTiersToAdd;
		internal VisibleRow    lastRowInserted; 
		internal VisibleRow    insertBefore;
		internal VisibleRow    insertAfter;
		internal UltraGridRow  beginAtRow;        // row to begin fetching with
		internal VisibleRow    beginAtVisibleRow;    // used to reinsert a row that was just removed
		internal UltraGridBand[]        headerBands; //JJD 5/19/00 used horizontal view see if sibling band header is required 

		// SSP 4/27/05 - NAS 5.2 Fixed Rows
		//
		private Hashtable parentVisibleRowMap;
		private VisibleRowsCollection visibleRows;

		// SSP 9/14/07 BR25887
		// 
		internal bool isBottomFixedRowsContext;

		internal Hashtable ParentVisibleRowMap
		{
			get
			{
				if ( null == this.parentVisibleRowMap )
					this.parentVisibleRowMap = new Hashtable( );

				return this.parentVisibleRowMap;
			}
		}

		internal VisibleRowsCollection VisibleRows
		{
			get
			{
				return this.visibleRows;
			}
		}

		// SSP 7/18/05 - NAS 5.3 Header Placement
		// 
		#region NAS 5.3 Header Placement

		private Hashtable currentHeaders;
		internal Hashtable CurrentHeaders 
		{
			get
			{
				if ( null == this.currentHeaders )
					this.currentHeaders = new Hashtable( );

				return this.currentHeaders;
			}
		}
		
		#endregion // NAS 5.3 Header Placement

	};

	#endregion // VisibleRowFetchRowContext Struct

}
