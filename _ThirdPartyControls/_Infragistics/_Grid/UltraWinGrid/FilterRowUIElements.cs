#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;

// SSP 3/15/05 - NAS 5.2 Filter Row Functionality
// Added this file.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region FilterRowUIElement

	/// <summary>
	/// A class to represent a filter row.
	/// </summary>
	public class FilterRowUIElement : RowUIElementBase
	{
		#region Constructor
		
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public FilterRowUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region FilterRow

		/// <summary>
		/// Gets the associated filter row.
		/// </summary>
		public UltraGridFilterRow FilterRow
		{
			get
			{
				return (UltraGridFilterRow)this.Row;
			}
		}

		#endregion // FilterRow
		
		#region Base Overrides

		#region CreateRowCellAreaUIElement

		/// <summary>
		/// Method for creating RowCellAreaUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing RowCellAreaUIElementBase if one exists. Otherwise, a new instance.</returns>
        protected override RowCellAreaUIElementBase CreateRowCellAreaUIElement(UIElementsCollection oldElems)
		{
			FilterRowCellAreaUIElement elem = (FilterRowCellAreaUIElement)FilterRowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( FilterRowCellAreaUIElement ), true );
			return null != elem ? elem : new FilterRowCellAreaUIElement( this );
		}

		#endregion // CreateRowCellAreaUIElement

		#region CreateRowSelectorUIElement

		/// <summary>
		/// Method for creating RowSelectorUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing RowSelectorUIElementBase if one exists. Otherwise, a new instance.</returns>
        protected override RowSelectorUIElementBase CreateRowSelectorUIElement(UIElementsCollection oldElems)
		{
			FilterRowSelectorUIElement elem = (FilterRowSelectorUIElement)FilterRowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( FilterRowSelectorUIElement ), true );
			return null != elem ? elem : new FilterRowSelectorUIElement( this );
		}

		#endregion // CreateRowSelectorUIElement

		#region CreatePreRowAreaUIElement

		/// <summary>
		/// Method for creating PreRowAreaUIElement derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>An existing PreRowAreaUIElement if one exists. Otherwise, a new instance.</returns>
        protected override PreRowAreaUIElement CreatePreRowAreaUIElement(UIElementsCollection oldElems)
		{
			PreRowAreaUIElement elem = (PreRowAreaUIElement)FilterRowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( PreRowAreaUIElement ), true );
			return null != elem ? elem : new PreRowAreaUIElement( this );
		}

		#endregion // CreatePreRowAreaUIElement

		#endregion // Base Overrides
	}

	#endregion // FilterRowUIElement

	#region FilterRowSelectorUIElement

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Added FilterRowSelectorUIElement class.
	//
	/// <summary>
	/// The ui element for the row selectors of filter rows.
	/// </summary>
	public class FilterRowSelectorUIElement : RowSelectorUIElementBase
	{
		#region Constructor
		
		internal FilterRowSelectorUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region FilterRow

		/// <summary>
		/// The associated filter row object (read-only).
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridFilterRow FilterRow
		{
			get
			{
				return (UltraGridFilterRow)this.Row;
			}
		}

		#endregion // FilterRow


		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// If the row selector of the filter row is displaying a filter clear button then
				// don't draw any borders around the row selector otherwise the button will get
				// shrunk to a very small size.
				//
				UltraGridFilterRow filterRow = this.FilterRow;
				if ( filterRow.RowSelectorHasClearButton )
					return UIElementBorderStyle.None;

				return filterRow.BandInternal.BorderStyleFilterRowSelectorResolved;
			}
		}

		#endregion // BorderStyle

		#region HeaderStyle

		/// <summary>
		/// Returns the HeaderStyle that this row selector should use.
		/// </summary>
		protected override HeaderStyle HeaderStyle
		{
			get
			{
				// If the row selector of the filter row is displaying a filter clear button then
				// don't draw any borders around the row selector otherwise the button will get
				// shrunk to a very small size.
				//
				if ( this.FilterRow.RowSelectorHasClearButton )
					return HeaderStyle.Standard;

				return base.HeaderStyle;
			}
		}

		#endregion // HeaderStyle

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// Since a Row Selector element doesn't have any child elements
		/// PositionChildElements doesn't do anything
		/// </summary>
		protected override void PositionChildElements( )
		{
			UltraGridFilterRow filterRow = this.FilterRow;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridBand band = filterRow.BandInternal;

			FilterClearButtonUIElement filterClearButtonElem = 
				(FilterClearButtonUIElement)FilterRowSelectorUIElement.ExtractExistingElement( 
					this.childElementsCollection, typeof( FilterClearButtonUIElement ), true );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//ImageUIElement imageElem = 
			//    (ImageUIElement)FilterRowSelectorUIElement.ExtractExistingElement( 
			//        this.childElementsCollection, typeof( ImageUIElement ), true );

			if ( null != this.childElementsCollection )
				this.childElementsCollection.Clear( );

			Rectangle workRect = this.RectInsideBorders;

			if ( filterRow.RowSelectorHasClearButton )
			{
				// If the clear button is displayed.
				//

				if ( null == filterClearButtonElem )
					filterClearButtonElem = new FilterClearButtonUIElement( this );

				filterClearButtonElem.Rect = workRect;
				this.ChildElements.Add( filterClearButtonElem );
			}
			else
			{
				// Otherwise display a filter image in the row selector of the filter row.
				//
				
			}

			this.rowIndicatorIconRect = workRect;
		}

		#endregion // PositionChildElements
	}

    #endregion // FilterRowSelectorUIElement

	#region FilterRowCellAreaUIElement Class

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the RowCellAreaUIElement into RowCellAreaUIElementBase class and 
	// RowCellAreaUIElement class so the filter cell are ui element can derive from 
	// CellUIElementBase.
	//
	/// <summary>
	/// The RowCellAreaUIElement contains the CellUIElements for a specific row.
	/// </summary>
	public class FilterRowCellAreaUIElement : RowCellAreaUIElementBase
	{
		#region Constructor
		
		internal FilterRowCellAreaUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor
		
		#region CreateCellUIElement

		/// <summary>
		/// Method for creating CellUIElementBase derived class instances.
		/// </summary>
        /// <param name="oldElems">The old UIElements collection.</param>
        /// <returns>The existing CellUIElementBase or a new one if there was no existing instance.</returns>
		protected override CellUIElementBase CreateCellUIElement( UIElementsCollection oldElems )
		{
			FilterCellUIElement elem = (FilterCellUIElement)FilterRowCellAreaUIElement.ExtractExistingElement( oldElems, typeof( FilterCellUIElement ), true );
			return null != elem ? elem : new FilterCellUIElement( this );
		}

		#endregion // CreateCellUIElement
	}

	#endregion // FilterRowCellAreaUIElement Class

	#region FilterCellUIElement

	/// <summary>
	/// The ui element class for representing cells.
	/// </summary>
	public class FilterCellUIElement : CellUIElementBase
	{
		#region Private Vars

		#endregion // Private Vars

		#region Constructor
		
		internal FilterCellUIElement( UIElement parent ) : base( parent )
		{	
		}

		/// <summary>
		/// Initializes a new <b>FilterCellUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="column">Associated <b>Column</b></param>
		public FilterCellUIElement( UIElement parent, UltraGridColumn column ) : base( parent, column )
		{	
		}

		#endregion // Constructor

		#region PositionChildElementsHelper

		internal override void PositionChildElementsHelper( 
			UltraGridColumn column, UltraGridRow rowParam, RowScrollRegion rsr, ColScrollRegion csr )
		{
			UltraGridFilterRow row = rowParam as UltraGridFilterRow;
			if ( null == row )
			{
				Debug.Assert( false );
				this.ChildElements.Clear( );
				return;
			}

			// Extract old elements.
			//
			EmbeddableUIElementBase operandEmbeddableElem = this.FindEmbeddableUIElement( true );
			FilterOperatorUIElement operatorElem = (FilterOperatorUIElement)FilterCellUIElement.ExtractExistingElement( 
				this.childElementsCollection, typeof( FilterOperatorUIElement ), true );
			FilterClearButtonUIElement clearButtonElem = (FilterClearButtonUIElement)FilterCellUIElement.ExtractExistingElement( 
				this.childElementsCollection, typeof( FilterClearButtonUIElement ), true );
			RowPromptUIElement rowPromptElement = (RowPromptUIElement)FilterCellUIElement.ExtractExistingElement( 
				this.childElementsCollection, typeof( RowPromptUIElement ), true );

			this.ChildElements.Clear( );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridBand band = row.BandInternal;

			FilterOperatorLocation operatorLocation = column.FilterOperatorLocationResolved;
			Rectangle workRect = this.RectInsideBorders;

			UltraGridCell cell = row.GetCellIfAllocated( column );
			operandEmbeddableElem = this.GetEmbeddableEditorElementHelper( 
				column, row, rsr, csr, cell, operandEmbeddableElem );

			bool includeEditElements = null == operandEmbeddableElem || operandEmbeddableElem.IncludeEditElements;

			// Add the operator element.
			//
			Rectangle operatorRect = Rectangle.Empty;
			if ( FilterOperatorLocation.AboveOperand == operatorLocation )
			{
				int operatorHeight = row.GetIdealOpearatorCellHeight( column );
				operatorRect = workRect;
				operatorRect.Height = operatorHeight;
				workRect.Y += operatorHeight;
				workRect.Height -= operatorHeight;
			}
			else if ( FilterOperatorLocation.WithOperand == operatorLocation )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//int filterOperatorWidth = row.GetIdealOperatorButtonSize( ).Width;
				int filterOperatorWidth = UltraGridFilterRow.GetIdealOperatorButtonSize().Width;

				operatorRect = workRect;
				operatorRect.Width = Math.Min( workRect.Width, filterOperatorWidth );
				workRect.Width -= operatorRect.Width;
				workRect.X = operatorRect.Right;
			}

			if ( operatorRect.Width > 0 && operatorRect.Height > 0 )
			{
				if ( null == operatorElem )
					operatorElem = new FilterOperatorUIElement( this );

				operatorElem.Rect = operatorRect;
				this.ChildElements.Add( operatorElem );
			}

			// Add the Clear button element.
			//
			if ( includeEditElements )
			{
				if ( column.FilterClearButtonVisibleResolved )
				{
					Rectangle clearButtonRect = workRect;

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//clearButtonRect.Width = Math.Min( workRect.Width, row.GetIdealClearButtonSize( ).Width );
					clearButtonRect.Width = Math.Min( workRect.Width, UltraGridFilterRow.GetIdealClearButtonSize().Width );

					workRect.Width -= clearButtonRect.Width;
					if ( FilterOperatorLocation.AboveOperand == operatorLocation )
						workRect.X = clearButtonRect.Right;
					else
						clearButtonRect.X = workRect.Right;

					if ( clearButtonRect.Width > 0 && clearButtonRect.Height > 0 )
					{
						if ( null == clearButtonElem )
							clearButtonElem = new FilterClearButtonUIElement( this );

						clearButtonElem.Rect = clearButtonRect;
						this.ChildElements.Add( clearButtonElem );
					}
				}
			}

			// Add the operand editor element.
			//
			if ( null != operandEmbeddableElem )
			{
				operandEmbeddableElem.Rect = workRect;
				this.ChildElements.Add( operandEmbeddableElem );
			}

			// Add filter row prompt if any. Row prompts can be displayed in the row cell area
			// or a cell if the band's SpecialRowPromptField property is set.
			//
			RowPromptUIElement.AddRowPromptElementHelper( row, this, null, rowPromptElement );
		}

		#endregion // PositionChildElementsHelper

	}

	#endregion // FilterCellUIElement

	#region FilterOperatorUIElement

	/// <summary>
	/// UI element class for an operator element in a filter row element.
	/// </summary>
	public class FilterOperatorUIElement : UIElement
	{
		#region Private Vars

		internal const int DEFAULT_FILTER_OPERATOR_BUTTON_SIZE = 18;

		private bool displayingButton = false;

		#endregion // Private Vars

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public FilterOperatorUIElement( UIElement parent ) : base( parent )
		{
		}

		#endregion // Constructor

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			this.Column.ResolveFilterOperatorAppearance( ref appData, ref flags );
		}

		#endregion // InitAppearance

		#region FilterRow

		/// <summary>
		/// Returns the associated filter row.
		/// </summary>
		public UltraGridFilterRow FilterRow
		{
			get
			{
				return (UltraGridFilterRow)this.GetContext( typeof( UltraGridFilterRow ), true );
			}
		}

		#endregion // FilterRow

		#region Column

		/// <summary>
		/// Returns the associated column.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return (UltraGridColumn)this.GetContext( typeof( UltraGridColumn ), true );
			}
		}

		#endregion // Column

		#region BorderSides

		/// <summary>
		/// Overridden property that returns an appropriate Border3DSide
		/// structure for drawing the borders for the cell UI element.
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return FilterOperatorLocation.WithOperand == this.Column.FilterOperatorLocationResolved
					? ( ! this.displayingButton ? Border3DSide.Right : (Border3DSide)0 ) : Border3DSide.Bottom;
			}
		}

		#endregion // BorderSides

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.Column.Band.BorderStyleFilterOperatorResolved;
			}
		}

		#endregion // BorderStyle

		#region PositionChildElements

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void PositionChildElements( )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//UltraGridFilterRow filterRow = this.FilterRow;
			//UltraGridBand band = filterRow.BandInternal;

			UltraGridColumn column = this.Column;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//FilterOperatorLocation operatorLocation = column.FilterOperatorLocationResolved;

			EmbeddableEditorBase editor = column.FilterOperatorEditor;

			EmbeddableUIElementBase editorElem = CellUIElement.FindEmbeddableUIElement( this.childElementsCollection, true );
			this.ChildElements.Clear( );

			FilterCellUIElement filterCellElem = (FilterCellUIElement)this.Parent.GetAncestor( typeof( FilterCellUIElement ) );
			EmbeddableUIElementBase operandEditorElem = null != filterCellElem ? filterCellElem.FindEmbeddableUIElement( false ) : null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//this.displayingButton = false && FilterOperatorLocation.WithOperand == operatorLocation
			//    || null == operandEditorElem || operandEditorElem.IncludeEditElements;
			this.displayingButton = ( null == operandEditorElem || operandEditorElem.IncludeEditElements );

			editorElem = editor.GetEmbeddableElement(
				this, 
				column.FilterOperatorEditorOwnerInfo,
				this,
				this.displayingButton,
				this.displayingButton,
				false,
				false,
				editorElem );

			editorElem.Rect = this.RectInsideBorders;
			this.ChildElements.Add( editorElem );
		}

		#endregion // PositionChildElements

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.FilterRow.BandInternal, StyleUtils.Role.FilterOperator );
			}
		}

		#endregion // UIRole
	}

	#endregion // FilterOperatorUIElement

	#region FilterClearButtonUIElement

	/// <summary>
	/// UI element class for an operator element in a filter row element.
	/// </summary>
	public class FilterClearButtonUIElement : ImageAndTextButtonUIElement
	{
		#region Private/Internal Variables

		internal const int DEFAULT_FILTER_CLEAR_BUTTON_SIZE = 18;

		#endregion // Private/Internal Variables

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public FilterClearButtonUIElement( UIElement parent ) : base( parent )
		{
			this.ElementClick += new UIElementEventHandler( this.ButtonClicked );
		}

		#endregion // Constructor

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			this.FilterRow.BandInternal.ResolveFilterClearButtonAppearance( ref appData, ref flags );

			// SSP 4/24/06 - App Styling
			// Call the base class implementation which will resolve defaults based on the button style.
			// 
			base.InitAppearance( ref appData, ref flags );
		}

		#endregion // InitAppearance

		#region FilterRow

		/// <summary>
		/// Returns the associated filter row.
		/// </summary>
		public UltraGridFilterRow FilterRow
		{
			get
			{
				return (UltraGridFilterRow)this.GetContext( typeof( UltraGridFilterRow ), true );
			}
		}

		#endregion // FilterRow

		#region Column

		/// <summary>
		/// Returns the associated column if any. If this button is displayed in the 
		/// filter row selector then the Column property will return null.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return (UltraGridColumn)this.GetContext( typeof( UltraGridColumn ), true );
			}
		}

		#endregion // Column

		#region PositionChildElements

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UltraGridBand band = this.FilterRow.Band;

			// Since this button element only displays image use the ImageHAlign and ImageVAlign
			// instead of TextHAlign and TextVAlign settings. For that set UseTextPlacement to false.
			//
			this.UseTextPlacement = false;

			// Set the button style to the resolved button style.
			//
			// SSP 3/27/06 - App Styling
			// Pass in the role related information in order to get the button style setting off the
			// currect role.
			// 
			//this.Style = band.ButtonStyleResolved;
			this.Style = band.GetButtonStyleResolved( StyleUtils.Role.FilterClearButton, StyleUtils.CachedProperty.ButtonStyleFilterClearButton );

			// Set the filter clear button image.
			//
			// SSP 6/8/05 BR04478
			// Honor the Image settings on the FilterClearButtonAppearance. 
			// The ResolveFilterClearButtonAppearance assigns the default image if none is set
			// by the user.
			// 
			// --------------------------------------------------------------------------------
			//this.Image = band.Layout.FilterClearButtonImageFromResource;

			AppearanceData appData = new AppearanceData( );
			AppearancePropFlags flags = AppearancePropFlags.Image;

			this.InitAppearance( ref appData, ref flags );

			this.Image = appData.GetImage( band.Layout.Grid.ImageList );
			// --------------------------------------------------------------------------------

			// Scale down the image if necessary.
			//
			this.ScaleImage = ScaleImage.OnlyWhenNeeded;

			// Call the base and let it position it's child elements.
			//
			base.PositionChildElements( );

			// Show tool tip when the mouse is hovered over the element.
			// 
			string toolTipText = null != this.Column
				? SR.GetString( "FilterClearButtonToolTip_FilterCell", this.Column.Header.Caption )
				: SR.GetString( "FilterClearButtonToolTip_RowSelector" );

			this.ToolTipItem = GridUtils.CreateToolTipItem( toolTipText, this.ToolTipItem );
		}

		#endregion // PositionChildElements

		#region ButtonClicked

		private void ButtonClicked( object sender, UIElementEventArgs e )
		{
			Debug.Assert( null != this.FilterRow );
			if ( null != this.FilterRow )
				this.FilterRow.OnFilterClearButtonClicked( this.Column );
		}

		#endregion // ButtonClicked

		#region OnDispose

		/// <summary>
		/// Overridden. Called when the object is being disposed.
		/// </summary>
		protected override void OnDispose( )
		{
			this.ElementClick -= new UIElementEventHandler( this.ButtonClicked );
		}

		#endregion // OnDispose

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.FilterRow.BandInternal, StyleUtils.Role.FilterClearButton );
			}
		}

		#endregion // UIRole
	}

	#endregion // FilterClearButtonUIElement

}
