#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Diagnostics;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	///		Summary description for SortIndicatorUIElement.
	/// </summary>
	public class SortIndicatorUIElement : UIElement
	{
		private bool ascending;
		
		internal static int SORTIND_HEIGHT =  8;
		internal static int SORTIND_WIDTH  =  8;

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>SortIndicatorUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="ascending">True if ascending, otherwise false</param>
		//internal SortIndicatorUIElement( UIElement parent, bool ascending ): base( parent )
		public SortIndicatorUIElement( UIElement parent, bool ascending ): base( parent )
		{
			this.ascending = ascending;	
		}

		internal void InitAscending( bool ascending )
		{
			this.ascending = ascending;	
		}
		

		/// <summary>
		/// Default borders rendering - does nothing
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBorders(ref Infragistics.Win.UIElementDrawParams drawParams )
		{			
		}

		/// <summary>
		/// Default Focus drawing - does nothing
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawFocus(ref Infragistics.Win.UIElementDrawParams drawParams )
		{
		}

		// SSP 12/3/01 UWG817
		// Overrode DrawBackColor and DrawImageBackground because
		// sort indicator elements should not be drawing them.
		// 
        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{
			// this element doesn't draw a background
		}

		/// <summary>
		/// this element doesn't draw an image background
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
		}
		

		/// <summary>
		/// Render the Sort Indicator in the Foreground
		/// </summary>
		/// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawForeground(ref Infragistics.Win.UIElementDrawParams drawParams )
		{
			// Changed to use PolyLine to get nicer SortIndicators that match outlook

			Rectangle thisRect = this.rectValue;

			// center the indicator in this element's rect
			//
			int startY = thisRect.Top + ( (thisRect.Height - SORTIND_HEIGHT ) / 2 );
			int startX = thisRect.Left + ( (thisRect.Width - SORTIND_WIDTH ) / 2 );

			System.Drawing.Point[] shadowPts = null;
			System.Drawing.Point[] highlightPts = null;
			
																		
			System.Drawing.Point[] shadowPtsAscending = new Point[] { new Point(0,5), new Point(1,5),new Point(1,3),new Point(2,3),new Point(2,1), new Point(3,1), new Point(3,0), new Point(3,1)};
			System.Drawing.Point[] shadowPtsDescending = new Point[] { new Point(7,0), new Point(0,0), new Point(0,1), new Point(1,1), new Point(1,3), new Point(2,3), new Point(2,5),new Point(3,5), new Point(3,6), new Point(3,5)};
																								
			System.Drawing.Point[] highlightPtsAscending = new Point[] {new Point(0,6), new Point(7,6), new Point(7,5), new Point(6,5), new Point(6,3), new Point(5,3), new Point(5,1), new Point(4,1), new Point(4,0), new Point(4,1)};
			System.Drawing.Point[] highlightPtsDescending = new Point[] {new Point(4,6), new Point(4,5), new Point(5,5), new Point(5,3), new Point(6,3), new Point(6,1), new Point(7,1), new Point(6,1)};

			if ( this.ascending )
			{
				shadowPts = shadowPtsAscending;
				highlightPts = highlightPtsAscending;
			}

			else
			{
				shadowPts = shadowPtsDescending;
				highlightPts = highlightPtsDescending;
			}

			// Shift the points
			//
			for ( int i = 0; i < shadowPts.Length; i++ )
			{
				shadowPts[i].X += startX;
				shadowPts[i].Y += startY;
			}
			for ( int i = 0; i < highlightPts.Length; i++ )
			{
				highlightPts[i].X += startX;
				highlightPts[i].Y += startY;
			}

			// AS 12/19/02 UWG1831
			// Get the pen from the draw params so the alpha bit can be set if needed.
			//
			//System.Drawing.Pen pen = new Pen(SystemColors.ControlDark);
			System.Drawing.Pen pen = drawParams.GetPen(DrawCache.PenType.Solid, SystemColors.ControlDark);
			drawParams.Graphics.DrawPolygon( pen, shadowPts );

			// AS 12/19/02 UWG1831
			// Get it again since the color may need to have it alpha channel set.
			//
			//pen.Color = SystemColors.ControlLightLight;
			pen = drawParams.GetPen(DrawCache.PenType.Solid, SystemColors.ControlLightLight);

			drawParams.Graphics.DrawPolygon( pen, highlightPts );

			

			
		}


	}
}
