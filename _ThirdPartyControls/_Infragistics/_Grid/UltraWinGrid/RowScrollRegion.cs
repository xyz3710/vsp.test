#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.Collections;
	using System.Globalization;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using Infragistics.Win.UltraWinScrollBar;
	using System.Security.Permissions;



    /// <summary>
    /// Defines a region that scrolls rows. Adjacent row scroll regions are separated by splitter bars.
    /// </summary>
    /// <remarks>
    /// <seealso cref="ColScrollRegion"/>
    /// <seealso cref="RowScrollRegionsCollection"/>
    /// <seealso cref="ColScrollRegionsCollection"/>
    /// <seealso cref="UltraGridLayout.RowScrollRegions"/>
    /// <seealso cref="UltraGridLayout.ColScrollRegions"/>
    /// <seealso cref="UltraGridBase.ActiveRowScrollRegion"/>
    /// <seealso cref="UltraGridBase.ActiveColScrollRegion"/>
	/// <seealso cref="UltraGridLayout.MaxRowScrollRegions"/>
	/// <seealso cref="UltraGridLayout.MaxColScrollRegions"/>
    /// </remarks>
	[ Serializable() ]
	[ TypeConverter( typeof( RowScrollRegion.RowScrollRegionTypeConverter ) ) ]
	public sealed class RowScrollRegion : ScrollRegionBase,
		ISerializable
	{
			
		
		private Infragistics.Win.UltraWinGrid.VisibleRowsCollection 
			visibleRows = null;

		private Infragistics.Win.UltraWinGrid.UltraGridRow
			firstRow = null;
		private bool destroyVisibleRows = true;
		
		internal int scrollbarFactor = 1;
		private bool forceScroll    = false;
		private bool isScrollTipActive = false;
		internal int lastScrollDelta = 0;
		internal int  scrollOffset;
		private Rectangle scrollTipRect;

		private GridItemBase pivotItem;
		private int rowsScrolledDuringDrag = 0;

		// SSP 8/24/01
		private bool regeneratingRows = false;

		// SSP 7/31/03 UWG2258
		// Added dontUpdateInOnScroll flag to prevent the OnScroll from calling 
		// Update on the control.
		//
		internal bool dontUpdateInOnScroll = false;

		// SSP 11/17/04 UWG2234 
		// Changed the behavior so that when a column is sorted we maintain the scroll position
		// instead of keeping the first row the first row.
		//
		private int afterSortScrollPositionToMaintain = -1;

		// SSP 11/17/05 BR07751
		// 
		private bool inScrollRowIntoView = false;

		// SSP 7/17/06 BR12782
		// 
		private bool verifyGridRectAndVisibleRows = false;

		#region constructors

		internal RowScrollRegion( RowScrollRegionsCollection rowScrollRegions ) 
			: base ( rowScrollRegions )
		{
			this.scrollTipRect = Rectangle.Empty;			
		}
		
		/// <summary>
		/// contructor
		/// </summary>
		[ Browsable( false ) ] 
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public RowScrollRegion( int extent )
		{
			this.Extent			= extent;
			this.scrollTipRect	= Rectangle.Empty;			
		}

		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal RowScrollRegion( SerializationInfo info, StreamingContext context )
		private RowScrollRegion( SerializationInfo info, StreamingContext context )
			: base( info, context )
		{		
			
			
		}

		#endregion
		
		internal Rectangle ScrollTipRect { get { return this.scrollTipRect; } }
		

		
		internal void InitializeFrom( RowScrollRegion rsr )
		{
			//this.CopyState( rsr, this );
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.CopyState( this, rsr );
			RowScrollRegion.CopyState( this, rsr );

			this.destroyVisibleRows = true;
			this.visibleRows = null;
			
			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( rsr.ShouldSerializeTag() )
				this.tagValue = rsr.Tag;
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			//set the assembly name because of version number conflicts
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			base.GetObjectData( info, context );

		}

		internal new RowScrollRegion ClonedFrom 
		{
			get
			{
				return (RowScrollRegion)base.ClonedFrom;
			}
		}

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
		public override bool ShouldSerialize() 
		{
			return base.ShouldSerialize();
			
		}
 
		/// <summary>
		/// Resets all properties back to their default values
		/// </summary>
		public override void Reset() 
		{

			base.Reset();
		}

 
		/// <summary>
		/// Returns true since this region has a vertical scrollbar
		/// </summary>
		protected override bool IsVerticalScrollbar
		{
			get
			{
				return true;
			}
		}

		// SSP 12/1/04 - Merged Cell Feature
		// Added IsActiveScrollRegion  method.
		//
		/// <summary>
		/// Returns true if the row scroll region is the same instance as the <see cref="UltraGridBase.ActiveRowScrollRegion"/>. 
		/// </summary>
		public override bool IsActiveScrollRegion 
		{
			get
			{
				return null != this.Layout && this == this.Layout.ActiveRowScrollRegion;
			}
		}

		// SSP 7/17/06 BR12782
		// 
		internal bool VerifyGridRectAndVisibleRows
		{
			get
			{
				return this.verifyGridRectAndVisibleRows;
			}
			set
			{
				this.verifyGridRectAndVisibleRows = value;
			}
		}

		/// <summary>
		/// Returns a reference to a VisibleRows collection of the Row objects that are currently displayed in a rowscrollregion. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property returns a reference to a VisibleRows collection that can be used to retrieve references to the Row objects that are currently displayed in a rowscrollregion. You can use this reference to access any of the returned collection's properties or methods, as well as the properties or methods of the objects within the collection.</p>
		/// <p class="body">As rows in the rowscrollregion are scrolled into and out of view, their corresponding Row objects are added to and removed from the VisibleRows collection returned by this property.</p>
		/// <p class="body">Rows that have their <b>Hidden</b> property set to True, and therefore are not displayed, are not included in the collection.</p>
		/// <p class="body">The <b>Count </b>property of the returned VisibleRows collection can be used to determine the number of rows currently displayed in the rowscrollregion.</p>
		/// <p class="body">NOTE: The visible rows collection will always contain an extra row scrolled out of view at the bottom. This is helpful in determining the next row that will come into view when the row scroll region is scrolled down by one row. See example section to find out the dimensions of visible rows and if they are actually visible on the screen.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public Infragistics.Win.UltraWinGrid.VisibleRowsCollection VisibleRows
		{
			get 
			{
				if ( null == this.visibleRows )
					this.visibleRows = new VisibleRowsCollection( this );

				// SSP 7/17/06 BR12782 BR05929
				// 
				// ----------------------------------------------------------------------
				if ( this.verifyGridRectAndVisibleRows )
				{
					this.verifyGridRectAndVisibleRows = false;

					UltraGridLayout layout = this.Layout;
					if ( null != layout && layout.HasUIElement && ! layout.Disposed 
						&& layout.IsDisplayLayout && null != layout.Grid
						&& ! layout.Grid.Disposing && ! layout.Grid.IsDisposed )
					{
						layout.UIElement.InternalInitializeRect( true );
						this.RegenerateVisibleRows( );
					}
				}
				// ----------------------------------------------------------------------

				return this.visibleRows;
			}
		}

		internal void OnRowPropChange( VisibleRow vr, PropChangeInfo propChange ) 
		{

			UltraGridRow row = vr.Row;
			
			Debug.Assert( (row == (UltraGridRow)propChange.Source ), 
				"The vr.Row points to a different Row in RowScrollRegion::OnRowPropChange" );
			
			bool recreateList			= false;
			bool invalidateRegion		= false;
			bool invalidateRow			= true;// assume that we only want to invalidate the row
			bool invalidateRowDown		= false;

			// SSP 3/11/05 BR02735
			//
			bool bumpCellElementsVersion = false;

			// SSP 5/19/05 BR04171 - NAS 5.2 Fixed Rows
			// Added dirtyRowElem.
			//
			bool dirtyRowElem = false;

			UltraGridCell invalidateCell = null;

            // MRS 4/16/2009 - TFS16617
            bool dirtyCellElem = false;

			if ( propChange.PropId is PropertyIds )
			{
				switch ( (PropertyIds)propChange.PropId )
				{
					case PropertyIds.Expanded:
					{
						recreateList = true;
						// SSP 4/24/03
						// Rather than invalidating the region from row down, invalidate the whole thing. 
						// What happens is that trying to get the region rect ends up verifying the ui elements
						// so if multiple rows were expanded, multiple verifications will take place slowing
						// things down. So invalidate the whole grid.
						//
						
						invalidateRegion = true;
						break;
					}
					case PropertyIds.Delete:
					{
						// Added support for deleting rows
						//
						recreateList = true;
						invalidateRowDown = true;
						break;
					}

						// JJD 8/7/01 - UWG101
						// Added height case so that we dirty visible rows
						//
					case PropertyIds.Height:						
					case PropertyIds.Hidden:
					case PropertyIds.Width:
					case PropertyIds.RowSpacingBefore:
					case PropertyIds.RowSpacingAfter:
					case PropertyIds.AutoPreviewHidden:
					// SSP 3/11/05 BR02735
					// Added Description case.
					//
					case PropertyIds.Description:
						// SSP 4/24/03
						// Rather than invalidating the region from row down, invalidate the whole thing. 
						// What happens is that trying to get the region rect ends up verifying the ui elements
						// so if multiple rows were expanded, multiple verifications will take place slowing
						// things down. So invalidate the whole grid.
						//
						//invalidateRowDown = true;
						invalidateRegion = true;

						// SSP 3/11/05 BR02735
						//
						bumpCellElementsVersion = true;

						// SSP 3/24/05 BR02957
						// Cause the row scroll regions to recalc the max scroll pos if the ScrollBounds
						// is set to ScrollToFill. For that simply bump the grand version number.
						//
						this.verifiedMaxScrollPositionVersion--;

						break;

					//ROBA 9/13/01 add Selected so that the row gets redrawn as selected
					//
					case PropertyIds.Selected:
						// SSP 5/29/02
						// Added case for Activation. When the activation of a row changes
						// we want to invalidate it so it reflects for example rows going from
						// allowedit to disabled.
						//
					case PropertyIds.Activation:
						row.InvalidateItemAllRegions(true);

						// SSP 1/24/03 UWG1862 - Optimizations
						// If we have alreay invalidated the row, then don't do it again below. 
						//
						invalidateRow = false;
					break;

					// SSP 5/19/05 BR04171 - NAS 5.2 Fixed Rows
					//
					case PropertyIds.AllowFixing:
					// SSP 7/29/05 - Header, Row, Summary Tooltips
					// 
					case PropertyIds.ToolTipText:
					// SSP 8/15/05 BR04897
					// Added PropertyIds.DataChanged entry.
					//
					case PropertyIds.DataChanged:
						dirtyRowElem = true;
					break;

					case PropertyIds.Cells:
					{
						PropChangeInfo baseCellChange = propChange.FindPropId( PropertyIds.Cell );
						if ( baseCellChange != null )
						{
							// set the pInvalidateCell variable so we invalidate
							// just this cell instead of the entire row
							//
							// SSP 4/12/04 - Virtual Binding Related
							// Use FindSource instead of assuming the baseCellChange.Source is a cell.
							//
							//invalidateCell = baseCellChange.Source as UltraGridCell;
							invalidateCell = (UltraGridCell)baseCellChange.FindSource( typeof( UltraGridCell ) );

							// SSP 4/16/05 - NAS 5.2 Filter Row
							// Added UltraGridFilterCell class. Apparently FindSource does not look for
							// derived class instances.
							//
							if ( null == invalidateCell )
								invalidateCell = (UltraGridCell)baseCellChange.FindSource( typeof( UltraGridFilterCell ) );

							if ( invalidateCell == null )
							{
								Debug.Assert( false, "Cell notification invalid in RowScrollREgion::OnRowPropChanged" );
								return;
							}

							if ( baseCellChange.PropId is PropertyIds )
							{
								switch ( (PropertyIds)baseCellChange.PropId )
								{
									case PropertyIds.TabStop:
									{
										// we don't need to invalidate anything when this property changes
										invalidateCell = null;
										invalidateRow = false;
										break;
									}
								}
							}

                            // MRS 4/16/2009 - TFS16617
                            PropChangeInfo propChangeRoot = baseCellChange.FindTrigger(null);
                            if (propChangeRoot.PropId is PropertyIds)
                                dirtyCellElem = dirtyCellElem || (PropertyIds)propChangeRoot.PropId == PropertyIds.Appearance;
						}
						break;
					}
				}
			}

			// SSP 3/11/05 BR02735
			//
			if ( bumpCellElementsVersion && null != this.Layout )
				this.Layout.BumpCellChildElementsCacheVersion( );

			if ( recreateList )
			{
				// set the destroy flag so we recreate the 
				// list on the next paint
				//
				this.SetDestroyVisibleRows( true );
			}

			if ( invalidateRegion )
			{
				this.Layout.DirtyGridElement();
				// set the destroy flag so we recreate the 
				// list on the next paint
				//
				this.SetDestroyVisibleRows( true );
			}
			else if ( invalidateRowDown )
			{
				Rectangle rcInvalid = new Rectangle(0,0,0,0);

				// get the regions overall rect
				//
				this.GetRegionRect ( ref rcInvalid );

				// offset the top based on the row's top (including headers)
				//
				rcInvalid.X         += vr.OrigTop;
				rcInvalid.Height    -= vr.OrigTop;

				// invalidate from this row down to
				// the bottom of the region
				//
				this.Invalidate( rcInvalid );

				this.Layout.DirtyGridElement();
				// set the destroy flag so we recreate the 
				// list on the next paint
				//
				this.SetDestroyVisibleRows( true );
			}
			else if ( invalidateRow )
			{
				// invalidate just the one cell or the entire row
				//
                if ((invalidateCell != null) && (invalidateCell != null))
                {
                    // MRS 4/16/2009 - TFS16617
                    //invalidateCell.InvalidateItem(this);
                    invalidateCell.InvalidateItem(this, dirtyCellElem);
                }
                else
                    // SSP 5/19/05 BR04171 - NAS 5.2 Fixed Rows
                    // Added dirtyRowElem.
                    //
                    //row.InvalidateItem( this );
                    row.InvalidateItem(this, dirtyRowElem);
			}
		}

		internal bool DestroyVisibleRows
		{
			get
			{
				return this.destroyVisibleRows;
			}
		}

		// SSP 11/4/04 UWG3593
		// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
		// that specifies whether to assume and if so to whether to assume the scrollbar
		// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
		// even when they are not needed because we assume the horizontal scrollbar is 
		// visible to find out if vertical scrollbar should be displayed or not.
		//
		//internal override bool WillScrollbarBeShown ( bool fAssumeColScrollbarsVisible )
		internal override bool WillScrollbarBeShown( ScrollbarVisibility assumeColScrollbarsVisible )
		{			
			// If we don't have any bands then we won't be showing the scrollbars
			//
			if ( this.Layout.SortedBands.Count < 1 )
				return false;

			// JJD 12/21/01
			// If this is a band 0 row and there are no groupby rows 
			// in the band then the card area occupies the entire region
			// and we don't need a vertical scrollbar
			//
			// MD 8/14/07 - 7.3 Performance
			// Cache the band don't we don't have to index into the collectio multiple times
			//if ( this.Layout.Bands[0].CardView &&
			//    !( this.Layout.Bands[0].HasGroupBySortColumns &&
			//       this.Layout.Bands[0].Layout.ViewStyleImpl.SupportsGroupByRows ) )
			//    return false;
			UltraGridBand band = this.Layout.SortedBands[ 0 ];

			if ( band.CardView &&
				!( band.HasGroupBySortColumns &&
				   band.Layout.ViewStyleImpl.SupportsGroupByRows ) )
			{
				return false;
			}

			Infragistics.Win.UltraWinGrid.Scrollbar
				enumScrollbar =  this.ScrollbarResolved;

			bool fShow;

			switch( enumScrollbar )
			{		
					// Let ssScrollbarShow case use same logic as ssScrollbarShowIfNeeded since
					// RegenerateVisibleRows may need to be called so that the ResetScrollInfo
					// method can accurately determine if the scrollbar should be disabled        
					//
				case Infragistics.Win.UltraWinGrid.Scrollbar.Show:
				case Infragistics.Win.UltraWinGrid.Scrollbar.ShowIfNeeded:			
					// make sure the visible rows are not dirty
					// Don't call RegenerateVisibleRows if we are in onDraw
					//
					if ( this.DestroyVisibleRows )
					{
						// If we don't have visible rows we need to call
						// RegenerateVisibleRows even if we are in the
						// middle of a drawing operation.
						// This solves a problem were the scrollbars weren't
						// showing up after a split. Introduced with fix for ult962.
						//
													 this.RegenerateVisibleRows( );
					}

					if ( Infragistics.Win.UltraWinGrid.Scrollbar.Show == enumScrollbar )
						fShow = true;
					else
						if ( this.VisibleRows.Count < 1 )
						fShow = false;
					else
						// SSP 4/28/05 - NAS 5.2 Fixed Rows
						// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
						// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
						//
						// --------------------------------------------------------------------
						
						fShow = ! this.IsFirstScrollableRowVisible( assumeColScrollbarsVisible ) 
								|| ! this.IsLastScrollableRowVisible( assumeColScrollbarsVisible );
						// --------------------------------------------------------------------				
					break;
    
				case Infragistics.Win.UltraWinGrid.Scrollbar.Hide:
					fShow = false;
					break;
    
				default:
					Debug.Fail("Invalid enumScrollbar in RowScrollRegion.WillScrollbarBeShown()");
					fShow = true;
					break;
			}

			return fShow;
		}

		/// <summary>
		/// Keeps track of pivot item for extended selection procesing
		/// </summary>
        /// <param name="gridItem">The grid item to assign as the new pivot item.</param>
		public void SetPivotItem( GridItemBase gridItem )
		{
			this.pivotItem = gridItem;
			this.rowsScrolledDuringDrag = 0;
		}

		/// <summary>
		/// The overall rect of this row scrolling region
		/// (spanning all column scrolling regions)
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public override Rectangle Rect 
		{ 
			get
			{
				Rectangle rect = this.Layout.DataArea;

				rect.Height = this.Extent;

				rect.Offset( 0, this.Origin - rect.Top );
 
				return rect;
			}
		}


		// SSP 8/24/01 UWG221 - Added this method
		internal void ScrollToEnd( )
		{
			bool origForceScroll = this.forceScroll;

			this.forceScroll = true;

			try
			{
				this.OnScroll( this.ScrollBarInfo, ScrollEventType.Last );
			}
			catch ( Exception e )
			{
				Debug.Assert( false, "Exception: " + e.Message );
			}

			this.forceScroll = origForceScroll;
			
		}
		
		internal void RegenerateVisibleRows()
		{
			this.RegenerateVisibleRows( true );
		}

		internal void RegenerateVisibleRows( bool resetScrollInfo )
		{
			if ( this.regeneratingRows )
				return;

			if ( this.destroyVisibleRows )
			{
                // MRS 5/14/2009 - TFS8698
                // Initialize the column scroll region metrics because the col header position
                // might affect the height of GroupByRows with summaries - the summaries might have 
                // to shift to a new line when there is no room for them on the first line with 
                // the summary text. 
                //
                this.Layout.ColScrollRegions.InitializeMetrics();

				this.destroyVisibleRows = false;

				// SSP 8/24/01 Anti-Recursion flag
				this.regeneratingRows = true;

				try
				{
                    // MBS 4/24/09 - TFS12665
                    // Perform the same optimization that is done in the RecreateRowList method on the ViewStyle, since 
                    // EnsureScrollRegionFilled will benefit greatly from a lot of the caching logic.  Note that is is
                    // not redundant to do this here in addition to in RecreateRowList, since RecreateRowList can be 
                    // called from other places as well that will use this optimization.
                    this.Layout.BeginCaching();

					// SSP 11/24/03 - Scrolling Till Last Row Visible
					// Added the functionality to not allow the user to scroll any further once the
					// last row is visible. Make sure the row scroll region is filled.
					//
					// --------------------------------------------------------------------------
					this.EnsureScrollRegionFilled( true );
					// --------------------------------------------------------------------------

					this.Layout.ViewStyleImpl.RecreateRowList(this);

					// SSP 8/7/01 UWG17 Uncommented if clause
					//if ( this.scrollbarPositionDirty )
					if ( this.scrollbarPositionDirty )
						this.PositionScrollbar( true );
	 
					if ( resetScrollInfo )
						this.ResetScrollInfo();
				}
				finally
				{
                    // MBS 4/24/09 - TFS12665
                    this.Layout.EndCaching();

					// SSP 8/24/01 Anti-Recursion flag
					this.regeneratingRows = false;			
				}
			
			}
		}
	
		// SSP 9/19/02 UWG1678
		//
		internal void AdjustFirstRow( UltraGridRow rowBeingDeleted )
		{
			// SSP 5/8/03 UWG2249
			// If the row being deleted is the only child row of a group-by parent row, then when
			// it gets deleted, the parent row will also get deleted as no group rows with 0 child
			// rows are allowed.
			//
			// SSP 5/5/04 - Virtual Binding Related - Optimization
			//
			//if ( null != rowBeingDeleted && rowBeingDeleted.ParentRow is UltraGridGroupByRow )
			//{
			//	UltraGridGroupByRow groupByRow = (UltraGridGroupByRow)rowBeingDeleted.ParentRow;
			if ( null != rowBeingDeleted && rowBeingDeleted.ParentCollection.ParentRow is UltraGridGroupByRow )
			{
				UltraGridGroupByRow groupByRow = (UltraGridGroupByRow)rowBeingDeleted.ParentCollection.ParentRow;

				if ( 1 == groupByRow.Rows.Count && groupByRow.Rows[0] == rowBeingDeleted )
				{
					// MD 8/2/07 - 7.3 Performance
					// Refactored - Prevent calling expensive getters multiple times
					//while ( groupByRow.ParentRow is UltraGridGroupByRow )
					//    groupByRow = (UltraGridGroupByRow)groupByRow.ParentRow;
					UltraGridGroupByRow nextGroupByRow = groupByRow.ParentRow as UltraGridGroupByRow;

                    // MBS 1/27/09 - TFS11603
                    // We shouldn't keep walking up the parent chain looking for the row that should
                    // be scrolled into view when there are more than 1 child rows, since we only want 
                    // to do this when the row will not have any more child rows after the row is deleted
                    //
					//while ( nextGroupByRow != null )
                    while ( nextGroupByRow != null && nextGroupByRow.Rows.Count == 1)
					{
						groupByRow = nextGroupByRow;
						nextGroupByRow = groupByRow.ParentRow as UltraGridGroupByRow;
					}
					
					rowBeingDeleted = groupByRow;
				}
			}

			// SSP 5/8/03 UWG2249
			// Related to above change.
			//
			//if ( null != this.firstRow && rowBeingDeleted == this.firstRow )
			if ( null != this.firstRow && ( rowBeingDeleted == this.firstRow || this.firstRow.IsDescendantOf( rowBeingDeleted ) ) )
			{
				// SSP 11/17/03 Add Row Feature
				// If the row that's being deleted is an add-row that was added via template add-row,
				// then make the template add-row the first row.
				//
				// --------------------------------------------------------------------------------------
				UltraGridRow row = null;
				if ( null != this.firstRow.ParentCollection 
					&& TemplateAddRowLocation.None != this.firstRow.ParentCollection.TemplateRowLocationDefault
					&& this.firstRow == this.firstRow.ParentCollection.CurrentAddRow )
				{
					row = this.firstRow.ParentCollection.TemplateAddRow;
				}
				// --------------------------------------------------------------------------------------
				
				if ( null == row )
				{
					row = this.firstRow.GetNextVisibleRow( true );

					if ( null == row )
						row = this.firstRow.GetPrevVisibleRow( );
				}
			
				if ( null != row )
					this.firstRow = row;
			}
		}

		// SSP 11/17/04 UWG2234 
		// Changed the behavior so that when a column is sorted we maintain the scroll position
		// instead of keeping the first row the first row.
		//
		internal int AfterSortScrollPositionToMaintain
		{
			get
			{
				return this.afterSortScrollPositionToMaintain;
			}
			set
			{
				this.afterSortScrollPositionToMaintain = value;
			}
		}

		#region ScrollPosition

		// SSP 5/17/05 BR03793
		// Added ScrollPosition property so the customer can scroll back to the previous scroll
		// position after performing an operation.
		//
		/// <summary>
		/// Gets or sets the position of the associated scrollbar.
		/// </summary>
		public int ScrollPosition
		{
			get
			{
				return this.HasScrollBarInfo ? this.ScrollBarInfo.Value : 0;
			}
			set
			{
				Infragistics.Win.UltraWinScrollBar.ScrollBarInfo scrollBarInfo = this.ScrollBarInfo;
				if ( null != scrollBarInfo )
				{
					int pos = Math.Max( scrollBarInfo.Minimum, Math.Min( scrollBarInfo.Maximum, value ) );
					scrollBarInfo.Value = pos;

					// Call OnScroll to actually cause the row scroll region to apply the new scroll bar value.
					//
					this.OnScroll( scrollBarInfo, ScrollEventType.ThumbPosition );
				}
			}
		}

		#endregion // ScrollPosition

		// SSP 9/3/02 UWG1535
		// Made FirstRow property public.
		// 
		//internal Infragistics.Win.UltraWinGrid.UltraGridRow FirstRow
		/// <summary>
		/// Gets or sets the first visible row in this row scroll region.
		/// </summary>
		/// <remarks>
		/// <p class="body">FirstRow property can be used to access the first visible row. It can also be used to set the first visible row.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridRow FirstRow
		{
			get
			{
				// SSP 11/17/04 UWG2234 
				// Changed the behavior so that when a column is sorted we maintain the scroll position
				// instead of keeping the first row the first row.
				//
				if ( this.AfterSortScrollPositionToMaintain >= 0 && null != this.Layout
					&& ! this.inGetMaxScrollPosition )
				{
					UltraGridRow row = this.Layout.GetRowAtScrollPos( this.AfterSortScrollPositionToMaintain );
					if ( null != row )
						this.firstRow = row;

					// Reset the var so we don't do this the next time this property is accessed.
					//
					this.afterSortScrollPositionToMaintain = -1;
				}

				// JJD 1/09/02
				// Rewrote get to valid the first row can still be
				// shown
				//
				// Walk up the first row's parent chain until we 
				// find a row that can be used as a first row
				//
				while ( this.firstRow != null )
				{
					// SSP 3/27/02
					// Row filtering feature code.
					//
					bool hiddenResolved = this.firstRow.HiddenResolved;

					// SSP 8/2/02 UWG1394
					// Accessing HiddenResolved may cause the row to get hidden and Notify
					// prop change may cause the visible rows to be regenerated and this property
					// set to null (in case no rows are visible). So if the firstRow has been
					// set to null, then break out of the loop.
					//
					if ( null == this.firstRow )
						break;

					if ( hiddenResolved && null != this.Layout.ViewStyleImpl )
					{
						bool recreateRowList = false;

						hiddenResolved = this.Layout.ViewStyleImpl.VerifyShouldRemoveHiddenRow( this, this.firstRow, out recreateRowList );
					}

					// SSP 10/18/05 BR07008
					// 
					if ( null == this.firstRow )
						break;

					// the row can only be the first row if
					// it is still valid, not hidden and all of
					// its ancestors are expanded
					//
					if ( this.firstRow.AllAncestorsExpanded 
						// SSP 11/30/05
						// Call to AllAncestorsExpanded above can lead to nulling out of firstRow.
						// 
						//&& this.firstRow.IsStillValid 
						&& null != this.firstRow && this.firstRow.IsStillValid 
						// SSP 3/27/02
						// Use the hiddenResolved variable instead which takes into account
						// other factors.
						// 
						 //!this.firstRow.HiddenResolved
						&& !hiddenResolved )
						break;

					// SSP 11/30/05
					// 
					if ( null == this.firstRow )
						break;

					// set the first row to its parent row
					//
					this.firstRow = this.firstRow.ParentRow;
				}

				// if we don't have a first row then get it
				//
				if ( null == this.firstRow )
					this.firstRow = this.Layout.Rows.GetFirstVisibleRow();

				// SSP 6/12/03 UWG2199
				// If the first row is null and the first band is a card view band, then the grid
				// won't show anything (no column headers). This situation arises when you have
				// all the rows filtered out so there are no visible rows and somehow you split
				// the row scroll region or do something that causes the firstRow to be null on
				// a row scroll region, then the view style logic won't add any card rows at all
				// and there will be no column headers at all showing in the row scroll region.
				//
				// --------------------------------------------------------------------------
				if ( null == this.firstRow )
				{
					UltraGridLayout layout = this.Layout;
					if ( null != layout && null != layout.SortedBands && layout.SortedBands.Count > 0 
						// SSP 9/2/03 UWG2330
						// We are not filtering out group-by rows as well. 
						// Commented out below condition. This applies card view as well as in
						// non-card view.
						//
						// ----------------------------------------------------------------------
						//&& layout.Bands[0].CardView 						
						// ----------------------------------------------------------------------
						&& null != layout.Rows && layout.Rows.Count > 0 )
					{
						UltraGridRow row = layout.Rows[0];

						// Call VerifyShouldRemoveHiddenRow which returns true if the row filtering is
						// turned on and it does other checks to ensure that we don't just return 
						// a first row that's hidden when it's inappropriate.
						//
						bool recreateRowList = false;
						if ( ! this.Layout.ViewStyleImpl.VerifyShouldRemoveHiddenRow( this, row, out recreateRowList ) )
							this.firstRow = row;
					}
				}
				// --------------------------------------------------------------------------

				return this.firstRow;
			}

			set
			{
				if ( value != this.firstRow )
				{
					// SSP 2/26/04 UWG2982
					// Check for value being null. Null is a valid value. FirstRow get basically
					// gets the first visible row in case the firstRow is null.
					//
					//if ( value.Band.Layout != this.Layout )
					if ( null != value && value.Band.Layout != this.Layout )
						throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_247") );

					// SSP 9/6/02 UWG1643
					// We need to ensure that temp active row becomes the active row now
					// because if it becomes the active row later on, then it will cause
					// the active row to scroll into view and the first row that this property
					// is assigned to will get lost.
					//
					// SSP 1/27/05 BR02053
					// Do this only for the display layout.
					//
					//if ( null != this.Layout && null != this.Layout.Grid )
					if ( null != this.Layout && null != this.Layout.Grid && this.Layout.IsDisplayLayout )
					{
						if ( null != this.Layout.Grid.TempActiveRow && 
							this.Layout.Grid.ActiveRow != this.Layout.Grid.TempActiveRow )
							this.Layout.Grid.PaintTimer_Tick( null, null );
					}

					this.firstRow = value;
					
					// SSP 9/3/02 UWG1535
					// Made this property public. However we need to do expand the ancestors and
					// set the destroyVisibleRows to true in order for it to work as expected.
					//
					// ---------------------------------------------------------------------------
					if ( null != this.firstRow )
						this.firstRow.ExpandAncestors( );

                    // MBS 1/22/08 - BR28948
                    // If the new first row is set to a value that is greater than the current max scroll position,
                    // we need to make sure that the rows will fill the bounds of the control when using ScrollToFill.
                    // This is done by decrementing the version number so that when we later go into GetMaxScrollPosition,
                    // we will perform the necessary logic.
                    if (this.firstRow != null && this.firstRow.ScrollPosition > this.cachedMaxScrollPosition)
                        this.verifiedMaxScrollPositionVersion--;

					this.SetDestroyVisibleRows( );
					// ---------------------------------------------------------------------------

					this.VisibleRows.Dirty = true;
					this.Invalidate();
				}
			}
		}

		// SSP 5/8/03 UWG2249
		// Added InternalFirstRow property to get the first row without doing any kind of
		// verification like the FirstRow does. This property is used by the SyncRowsHelper
		// logic in the rows collection.
		//
		internal UltraGridRow InternalFirstRow
		{
			get
			{
				return this.firstRow;
			}
			set
			{
				this.firstRow = value;
			}
		}

		

		
		
		internal GridItemBase GetPivotItem()
		{
			// JAS 9/23/04 UWG3630 - This code prevents the pivot row from 
			// being invalid, particularly to avoid it having an index of -1.
			if( this.pivotItem == null )
				return null;

            // MBS 5/7/08 - BR32580
            // Refactored into VerifyPivotItem
            //UltraGridRow row = this.pivotItem.GetRow();
            //if( row != null && ! row.IsStillValid )
            //{
            //    // SP 11/02/04 UWG3736
            //    // Set the pivot item to null.
            //    //
            //    //return null;
            //    this.pivotItem = null;
            //}
            this.VerifyPivotItem();

			return this.pivotItem;
		}

        // MBS 5/7/08 - BR32580
        // Refactored from above
        internal void VerifyPivotItem()
        {
            if (this.pivotItem == null)
                return;

            UltraGridRow row = this.pivotItem.GetRow();
            if (row != null && !row.IsStillValid)
            {
                // SP 11/02/04 UWG3736
                // Set the pivot item to null.
                //
                //return null;
                this.pivotItem = null;
            }
        }

		// SSP 10/23/06 BR16793
		// 
		internal GridItemBase PivotItemInternal
		{
			get
			{
				return this.pivotItem;
			}
		}

		/// <summary>
		/// Returns or sets the height of an object in container units.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Height</b> property is used to determine the vertical dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">For the RowScrollRegion object, this property always includes the horizontal scrollbar's <B>Height</B> for the ColScrollRegion.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public int Height
		{
			get
			{
				
				return this.GetClippedExtent();
			}

			set
			{
				if ( value != this.Extent )
				{
					this.Extent = value;

					// SSP 4/25/03 UWG2190
					//
					if ( null != this.Layout )
						this.Layout.DirtyGridElement( true );
					this.NotifyPropChange( PropertyIds.Height, null );
				}
			}				
		}

		

		internal int GetClippedExtent()
		{
			//RobA 11/5/01 implemented

			int extent = this.Extent;

			// SSP 5/7/03 - Export Functionality
			//
			//if ( extent > 0 && ( this.Layout.IsDisplayLayout || this.Layout.IsPrintLayout ) )
			if ( extent > 0 && ( this.Layout.IsDisplayLayout || this.Layout.IsPrintLayout || this.Layout.IsExportLayout ) )
			{

	            // Get the data area UI element clip the extent to it
			    //
				Infragistics.Win.UIElement element = this.Layout.GetUIElement(false);

				if ( element != null )
				{
					element = element.GetDescendant( typeof( Infragistics.Win.UltraWinGrid.DataAreaUIElement ) );
        
					if ( element != null )
					{
						Rectangle dataAreaRect = element.RectInsideBorders;

                        // clip the extent to the bottom of the data area
						//
						if ( extent > dataAreaRect.Bottom - this.Origin )
							extent = dataAreaRect.Bottom - this.Origin;
					}
				}
				else
				{
					// If we don't have a data area that means the grid
					//may be too smaall so set the extent to 0
					//
					extent = 0;
				}
			}
  
			if ( extent < 0 )
				extent = 0;

			return extent;
		}


		/// <summary>
		/// Returns or sets the width of an object in container units.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Width</b> property is used to determine the horizontal dimension of an object. It is generally expressed in the scale mode of the object's container, but can also be specified in pixels.</p>
		/// <p class="body">For the ColScrollRegion object, this property always includes the vertical scrollbar's <b>Width</b> for the RowScrollRegion.</p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public int Width
		{
			get
			{

				Rectangle dataAreaRect = this.Layout.ClientRect;				

				// Instead of using the grid client area, get the
				// data area UI element and intersect its rect
				//
				// Pass true into GetUIElement to trigger a verification of the 
				// sub element rects
				//
				UIElement element = this.Layout.GetUIElement( true );

				if ( null != element )
				{
					element = element.GetDescendant( typeof ( DataAreaUIElement ) );

					if ( null != element)
						dataAreaRect = Rectangle.Intersect( dataAreaRect, element.RectInsideBorders );
				}

				int width = dataAreaRect.Width;
				if ( width < 0 )
					width = 0;
				
				return width; 
			}
		}

      
		/// <summary>
		/// Scrolls a scrolling region by the specified increment.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to scroll a scrolling region.</p>
		/// <p class="body">When a rowscrollregion is scrolled, the <b>BeforeRowRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>ScrollCellIntoView</b>, <b>ScrollColumnIntoView</b>, <b>ScrollGroupIntoView</b>, and <b>ScrollRowIntoView</b> methods can be invoked to scroll an object into a scrolling region's viewable area. </p>
		/// </remarks>
		/// <param name="action">The row scroll action to perform.</param>
		public void Scroll( RowScrollAction action )
		{
			if ( this.IsClone ) 
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_248"));

			ScrollEventType e = new ScrollEventType();

			// validate action
			//
			switch ( action )
			{
				case RowScrollAction.LineUp:
					e = ScrollEventType.SmallDecrement;
					break;
				case RowScrollAction.LineDown:
					e = ScrollEventType.SmallIncrement;
					break;
				case RowScrollAction.PageUp:
					e = ScrollEventType.LargeDecrement;
					break;
				case RowScrollAction.PageDown:
					e = ScrollEventType.LargeIncrement;
					break;
				case RowScrollAction.Top:
					e = ScrollEventType.First;
					break;
				case RowScrollAction.Bottom:
					e = ScrollEventType.Last;					
					break;
				default:
					throw new InvalidEnumArgumentException();
			}
    
			// call OnScroll to do the scroll action
			//
			this.OnScroll(this.ScrollBarInfo, e );

			// reset the lastScrollType so we don't start doing
			// pixel level scrolling
			//
			this.lastScrollType = ScrollEventType.EndScroll;
		}

		/// <summary>
		/// Scrolls the specified cell into view for a scrolling region.
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to ensure that a cell is viewable in a column or row scrolling region.</p>
		/// <p class="body">If this method is invoked for a colscrollregion and the column is already in the viewable area of the region, this method does not perform any scrolling.</p>
		/// <p class="body">If the colscrollregion is scrolled as a result of invoking this method, the value of the column scrolling region's <b>Position</b> property changes and the <b>BeforeColRegionScroll</b> event is generated. If the rowscrollregion is scrolled as a result of invoking this method, the <b>BeforeRowRegionScroll</b> event is generated.</p>
		/// <p class="body">The <b>Scroll</b>, <b>ScrollColumnIntoView</b>, <b>ScrollGroupIntoView</b>, and <b>ScrollRowIntoView</b> methods can also be invoked to scroll an object into a scrolling region's viewable area.</p>
		/// </remarks>
		/// <param name="cell">The cell to scroll into view</param>
		/// <param name="colScrollRegion">The column scroll region.</param>
		public void ScrollCellIntoView(UltraGridCell cell, ColScrollRegion colScrollRegion)
		{				
			// If the cell is hidden don't do anything
			//
			// SSP 5/14/03 - Hiding Individual Cells Feature
			// Changed the original internal Cell.Hidden to Cell.InternalHidden because we
			// needed to add Hidden public method off the cell to implement this feature.
			//
			//if ( !cell.Hidden )
			if ( !cell.InternalHidden )
			{
				// Use new ValidateColRegionParam which takes a variant
				// ptr as an argument
				//
				ColScrollRegion csr = this.Layout.ValidateColRegionParam( colScrollRegion );

				Debug.Assert( null != csr, "Col scroll region missing.");

				// JJD 1/15/02
				// Make sure the row is scrolled into view first even if
				// the row is a card sice the card area has to be in view.
				//
				this.ScrollRowIntoView( cell.Row );

				// JJD 12/21/01
				// If this cell's row is a card don't call scrollthe call ScrollCardIntoView of its parent rows collection
				//
				if ( cell.Row.IsCard )
				{
					// scroll the column into view
					//
					cell.Row.ParentCollection.ScrollCardIntoView( cell.Row, csr );
				}
				else
				{
					// scroll the column into view
					//
					csr.ScrollColIntoView( cell.Column );
				}
			}
		}

		internal int SafeExtent
		{
			get
			{				
				// call Layout.GetUIElement() if the extent is zero (this will
				// force the extent to be set if it wasn't already)
				//
				if ( 0 == this.Extent )
				{
					// SSP 4/3/06 BR11216
					// If the rect of the grid element has not been initialized yet then initialize it
					// and also position the grid element's ui elements. This will initialize the 
					// extents of the scroll regions.
					// 
					// --------------------------------------------------------------------------------
					//this.Layout.GetUIElement( false );
					this.Layout.GetUIElement( false, true );
					// --------------------------------------------------------------------------------
				}

				return this.Extent;
			}
		}

		// SSP 11/2/01
		// Overrode this method because we need to set the destroyVisibleRows flag
		// to true when the extent has changed so that the rows get recreated
		//
		internal override void SetOriginAndExtent( int origin, int extent ) 
		{
			bool extentChanged = this.Extent != extent;
			
			base.SetOriginAndExtent( origin, extent );

			if ( extentChanged )
			{
				this.SetDestroyVisibleRows( );

				// SSP 12/9/03
				// Dirty the max scroll position since the extent has changed.
				//
				this.verifiedMaxScrollPositionVersion--;
			}
		}

		/// <summary>
		/// Always returns a vaild origin even if it hasn't been set yet.
		/// </summary>
		int SafeOrigin
		{
			get
			{
				// call Layout.GetUIElement() if the extent is zero (this will
				// force the extent to be set if it wasn't already)
				//
				if ( 0 == this.Extent )
					this.Layout.GetUIElement( false );

				return this.Origin;
			}
		}

		internal int ClippedExtent
		{
			get
			{                       
				int extent = this.SafeExtent;

				if ( extent > 0 && this.Layout.IsDisplayLayout )
				{
					// Get the data area UI element clip the extent to it
					//
					UIElement element = this.Layout.GetUIElement( false );

					if ( null != element )
					{
						element = element.GetDescendant( typeof ( DataAreaUIElement ) );

						if ( null != element )
						{
							Rectangle dataAreaRect = element.RectInsideBorders;

							// clip the extent to the bottom of the data area
							//
							if ( extent > dataAreaRect.Bottom - this.SafeOrigin )
								extent = dataAreaRect.Bottom - this.Origin;
						}
						else
						{
							// If we don't have a data area that means the grid
							//may be too smaall so set the extent to 0
							//
							extent = 0;
						}
					}
				}

				if ( extent < 0 )
					extent = 0;

				return extent;
			}
		}

		/// <summary>
		/// The index of this RowScrollRegion in the RowScrollRegions collection.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		[ MergableProperty( false ) ]
		public int Index
		{
			get
			{
				// SSP 12/4/02 UWG1840
				// Return -1 if the collection is null rather than assuming it's non-null and then
				// accessing IndexOf method which will throw an null reference exception.
				//
				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//if ( null == this.collection )
				if ( null == this.collectionValue )
					return -1;

				// AS 1/8/03 fxcop
				// Cannot have 2 public/protected members that differ only by case
				//return ((RowScrollRegionsCollection)this.collection).IndexOf( this );
				return ((RowScrollRegionsCollection)this.collectionValue).IndexOf( this );
			}
		}


		/// <summary>
		/// Splits the region in half.
		/// </summary>
        /// <returns>The new row scroll region created by the split.</returns>
		public RowScrollRegion Split()
		{
			return this.Split( 0 );
		}
        
		/// <summary>
		/// Splits a scrolling region into two scrolling regions. 
		/// </summary>
		/// <remarks>
		/// <p class="body">Invoke this method to split one scrolling region into two scrolling regions. This method returns a ColScrollRegion object or a RowScrollRegion object that corresponds to the new scrolling region that is created by the split.</p>
		/// <p class="body">ColScrollRegions are split from right to left, with the new region created by the split appearing to the left of the existing region. RowScrollRegions are split from bottom to top, with the new region created by the split appearing above the existing region.</p>
		/// <p class="body">Specifying <i>width</i> when splitting a ColScrollRegion will set the width of the new region (leftmost of the two resulting ColScrollRegions.) Specifying <i>height</i> when splitting a RowScrollRegion will set the height of the new region (topmost of the two resulting RowScrollRegions.)</p>
		/// <p class="body">When a ColScrollRegion is split, the <b>BeforeColRegionSplit</b> and the <b>AfterColRegionSplit</b> events are generated. When a RowsScrollRegion is split, the <b>BeforeRowRegionSplit</b> and the <b>AfterRowRegionSplit</b> events are generated.</p>
		/// </remarks>
		/// <param name="extent">Specifying 0 for extent will split the specified region in half. </param>
        /// <returns>The new row scroll region created by the split.</returns>
		public  RowScrollRegion Split( int extent )
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( null == grid )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_51"));

			//RobA UWG524 10/16/01 
			if ( this.SizingMode == SizingMode.Fixed )
			{
				// AS - 2/20/02
				// This is a public method that can be called from code. This should
				// not be displaying a messagebox. Instead, we should throw a
				// notsupportedexception. Of course, then we need to catch the exception 
				// in the oncolsplitboxdrop.
				//
				//System.Windows.Forms.MessageBox.Show( this.Layout.Grid, "Cannot split this region.", "Error");				
				//return null;
				throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_52") );
			}

			//RobA 10/22/01 implemented
			if ( this.ClonedFrom != null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_249"));
		
			// Throw an error if the extent is negative
			//
			if ( extent < 0 )
				throw new ArgumentOutOfRangeException(Shared.SR.GetString("LE_ArgumentOutOfRangeException_250") ) ;

			// Make sure we don't split the region if we have already reached the max
			//
			if ( this.Layout.MaxRowScrollRegions > 0 &&
				this.Layout.RowScrollRegions.Count >= this.Layout.MaxRowScrollRegions )
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_251") );

			// SSP 6/14/02 UWG1189
			// Before calling ClippedExtent, verify the grid's elements so
			// the Extent properties on the scroll regions get set to proper values.
			// This is necessary for example when the code to split the region
			// is put in the form's load event handler. At that point the grid
			// has not been drawn and thus the elements may not be verified.
			// So we want to force re-verification here. I didn't make this change
			// in the ClippedExtent or SafeExtent propeties which ClippedExtent calls
			// because reverifying everytime those properties are called (and they
			// are being called from quite a few places) may slow things down
			// considerably.
			//
			this.Layout.GetUIElement( true, true );

			// Use newer GetClippedExtent method
			//
			int currentExtent   = this.ClippedExtent;
			int splitExtent     = extent;

			// SSP 5/12/06 - App Styling
			// Added custom properties for controlling widths of splitter bars and split boxes.
			// 
			//int splitterBarWidth = this.Layout.SplitterBarWidth;
			int splitterBarWidth = this.Layout.RowScrollRegionSplitterBarHeightResolved;

			// Throw an error if the requested split is too great 
			//
			if ( extent > 0 && currentExtent < splitExtent + splitterBarWidth )
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_56") );

			if ( splitExtent <= 0 || currentExtent - splitExtent <= splitterBarWidth  )
			{
				int holdExtent = currentExtent;

				currentExtent  = ( currentExtent - splitterBarWidth ) / 2;
				splitExtent = holdExtent - ( currentExtent + splitterBarWidth );
			}
			else if ( splitExtent < currentExtent )
			{
				currentExtent -= splitExtent + splitterBarWidth;
			}
			else
			{
				// they specified a split greater than the size of the region
				// to split
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_56") );
			}

			// We do not allow zero-sized regions
			if ( currentExtent <= 0 || splitExtent <= 0 )
				throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_57") );

			int index = this.Index;

			// AS 1/8/03 fxcop
			// Cannot have 2 public/protected members that differ only by case
			//RowScrollRegion rsr = new RowScrollRegion( (RowScrollRegionsCollection)(this.collection) );
			RowScrollRegion rsr = new RowScrollRegion( (RowScrollRegionsCollection)(this.collectionValue) );

			// set the origin and extent of the new region
			//
			rsr.SetOriginAndExtent ( this.Origin, splitExtent );
			
			// fire the before event
			//
			BeforeRowRegionSplitEventArgs args = new BeforeRowRegionSplitEventArgs( this, rsr );

			grid.FireEvent( GridEventIds.BeforeRowRegionSplit, args );

			if ( args.Cancel )
				return null;

			// the event wasn't cancelled so adjust the origin and extent
			// of original region and add the region to the collection
			//
			this.SetOriginAndExtent ( this.Origin + splitExtent + splitterBarWidth, currentExtent);

			this.Layout.RowScrollRegions.InternalInsert( rsr, index );

			this.Layout.DirtyGridElement();

			this.NotifyPropChange( PropertyIds.Width );

			return rsr;
		}

		/// <summary>
		/// Returns the RowColRegionIntersectionUIElement for this RowScrollRegion and the specified ColScrollRegion.
		/// </summary>
		/// <param name="csr">The column scroll region.</param>
        /// <returns>The RowColRegionIntersectionUIElement for this RowScrollRegion and the specified ColScrollRegion.</returns>
		public RowColRegionIntersectionUIElement GetUIElement( ColScrollRegion csr )
		{
			// Pass true into GetUIElement to trigger a verification of the 
			// sub element rects
			//
			UIElement element = this.Layout.GetUIElement( true ); 

			
			// Use new ValidateColRegionParam which takes a variant
			// ptr as an argument
			//
			ColScrollRegion colScrollRegion = 
				this.Layout.ValidateColRegionParam( csr );
			
			Debug.Assert( null != colScrollRegion, "Col scroll region missing." );

			object[] contexts = new Object[2];			
			contexts[0] = colScrollRegion;
			contexts[1] = this;
			element = element.GetDescendant( typeof(RowColRegionIntersectionUIElement),				
				contexts );			

			return (RowColRegionIntersectionUIElement)element;
		}


		/// <summary>
		/// gets the top of the scroll region in client coordinates
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public int Top
		{
			get
			{          
				return this.SafeOrigin;
			}
		}
		internal int GetClientHeight ( ColScrollRegion csr )
		{
			if ( this.IsClone )
				return ((RowScrollRegion)this.ClonedFrom).GetClientHeight ( csr );
    
			if ( null == csr )
				csr = this.Layout.GetActiveColScrollRegion( true );

			if ( null == csr )
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_252"));


			// Use newer GetClippedExtent method
			//
			int extent = this.ClippedExtent;

			if ( this.IsLastVisibleRegion && csr.WillScrollbarBeShown() )
			{
				extent -= SystemInformation.HorizontalScrollBarHeight;

				if ( extent < 0 )
					extent = 0;
			}

			return extent;
		}

		/// <summary>
		/// Scrolls passed in row into view.
		/// </summary>
		/// <param name="row">The row to scroll into view.</param>
		public void ScrollRowIntoView( UltraGridRow row )
		{
			this.ScrollRowIntoView( row, true );
		}
        
		internal void ScrollRowIntoView( UltraGridRow row, bool exitEditMode )
		{
			// SSP 5/12/05 - NAS 5.2 Fixed Add Row
			// Maintain the old behavior by passing in false for the dontUpdateGridControl.
			//
			this.ScrollRowIntoView( row, exitEditMode, false );
		}
		
		// SSP 5/12/05 - NAS 5.2 Fixed Add Row
		// Added an overload of ScrollRowIntoView that takes in dontUpdateGridControl parameter.
		// This was done to force the ScrollRowIntoView to NOT call Update.
		//
		internal void ScrollRowIntoView( UltraGridRow row, bool exitEditMode, bool dontUpdateGridControl )
		{
			this.ScrollRowIntoView( row, exitEditMode, dontUpdateGridControl, true );
		}

		// SSP 7/21/05 BR05030
		// Added overload that takes in maintainClosesScrollPosition parameter.
		// 
		internal void ScrollRowIntoView( UltraGridRow row, bool exitEditMode, bool dontUpdateGridControl, 
			bool maintainClosestScrollPosition )
		{
			//bool selfUsedFlag = false;
			//this.ScrollRowIntoView( row, exitEditMode, dontUpdateGridControl, maintainClosestScrollPosition, ref selfUsedFlag );

			// SSP 11/17/05 BR07751
			// Added anti-recursion flag.
			// 
			if ( this.inScrollRowIntoView )
				return;

			this.inScrollRowIntoView = true;
			UltraGridBase grid = this.Layout.Grid;
			grid.tempActiveRow_ScrollIntoView = false;

			try
			{
				bool selfUsedFlag = false;
				this.ScrollRowIntoViewHelper( row, exitEditMode, dontUpdateGridControl, 
					maintainClosestScrollPosition, ref selfUsedFlag );
			}
			finally
			{
				this.inScrollRowIntoView = false;
			}
		}

		// SSP 10/19/05 BR06988
		// Added an overload that takes in selfUsedFlag parameter.
		// 
		// SSP 11/17/05 BR07751
		// Renamed ScrollRowIntoView into ScrollRowIntoViewHelper.
		// 
		private void ScrollRowIntoViewHelper( UltraGridRow row, bool exitEditMode, bool dontUpdateGridControl, 
			bool maintainClosestScrollPosition, ref bool selfUsedFlag )
		{
			//Debug.WriteLine( "ScrollRowIntoView" );

			//RobA UWG374 10/1/01 we want to recreate the visible rows collection
			//before we check to see if the row is visible because in some situations the
			//row is not in the visible rows collection yet
			//
			this.RegenerateVisibleRows();

			UltraGridRow oldFirstRow = this.FirstRow;
							
			// make sure the row is active and is the same as our layout
			//
			if (row.Layout != this.Layout ) 
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_253"));

			// If the row is hidden don't do anything
			//
			if ( row.HiddenResolved )
				return;

			// JJD 12/21/01
			// If this row is a card then call ScrollCardIntoView of its parent rows collection
			//
			if ( row.IsCard )
			{
				row.ParentCollection.ScrollCardIntoView( row, this );
				return;
			}

			VisibleRow  visibleRow;

			// Use GetClientHeight to calc bottom since that takes into account
			// the col scrollbar.
			//
			// SSP 4/21/05 BR03498
			//
			//int bottom = this.GetClientHeight( this.Layout.GetActiveColScrollRegion( false ) );
			ColScrollRegion activeCSR = this.Layout.GetActiveColScrollRegion( false );
			int bottom = this.GetClientHeight( activeCSR ); 

			// Check if the Bottom comes back as zero. If so, the metrics
			// have not been initialized, and we'll get stuck in the loop
			// below, so bail out.
			if (bottom == 0)
			{
				// SSP 4/9/02 UWG1065
				// If the height of the scroll region is 0, then the
				// metrics has not been initialized yet, so just set the
				// row to the first row instead of just bailing out.
				//
				this.SetFirstRow( row, false );
				this.SetDestroyVisibleRows( true );

				return;
			}

			check_for_row_in_list:

				// SSP 4/21/05 BR03498
				// Reget the bottom of the scroll region in case the scrollbar's visibility changes,
				// for example as a result of auto-fitting columns, or column metrics getting 
				// recalculated. Added following line.
				//
				bottom = this.GetClientHeight( activeCSR ); 


				this.Layout.ViewStyleImpl.VerifyRowList( this );

			// iterate over the elem row collection looking for the pivot row 
			//
			VisibleRowsCollection visibleRows = this.VisibleRows;
			int count = visibleRows.Count;
			for ( int i = 0; i < count; i++ )
			{
				visibleRow = visibleRows[i];

				if ( visibleRow.Row == row )
				{
					// If the row is the first row in the list we have to assume
					// it is fuly visible
					//
					if ( visibleRows[0] == visibleRow )
						return;

					// SSP 5/16/05 - NAS 5.2 Fixed Rows
					// If the row we are looking for is the first scrollable row then return. 
					// This essentially does the same thing as the above check, however taking
					// into account the fixed rows. Also return if the row that's being scrolled 
					// into view is a fixed row because no amount of scrolling up or down will
					// cause it to change it's position and thus become fully visible.
					//
					if ( visibleRows.GetFirstScrollableRow( ) == visibleRow || row.FixedResolved )
						return;

					// return if the row is completely in view
					//
					// SSP 4/28/05 - NAS 5.2 Fixed Rows
					// Use the new IsRowFullyVisible method instead.
					//
					//if ( visibleRow.GetTopOfRow() + row.TotalHeight <= bottom )
					if ( this.IsRowFullyVisible( visibleRow ) )
						return;

					
					// call OnScroll to scroll one row					
					//
					// Pass in true for "InvalidateRegion" so that
					// we paint ourselves correctly.
					//
					// SSP 8/6/03
					// Set the dontUpdateInOnScroll to prevent the OnScroll from calling Update
					// on the grid.
					//
					bool origDontUpdateInOnScroll = this.dontUpdateInOnScroll;
					try
					{
						// SSP 10/22/04 UWG2980
						// Only set dontUpdateInOnScroll to true here if we are being called from
						// the GetMaxScrollPosition. Otherwise don't set the flag to true because
						// it has adverse impact on the performance when using up or down keys 
						// (keyboard keys) to scroll the grid.
						// 
						// SSP 5/12/05 - NAS 5.2 Fixed Add Row
						// Added an overload of ScrollRowIntoView that takes in dontUpdateGridControl parameter.
						// This was done to force the ScrollRowIntoView to NOT call Update.
						//
						//if ( this.inGetMaxScrollPosition )
						if ( this.inGetMaxScrollPosition || dontUpdateGridControl )
							this.dontUpdateInOnScroll = true;						

						// SSP 5/12/05 BR03883
						// Check if the OnScroll operation succeeds. If it does not then break out 
						// of the loop otherwise we will be stuck in an infinite loop.
						//
                        UltraGridRow origFirstRow = this.firstRow;

						if ( this.OnScroll( this.ScrollBarInfo, ScrollEventType.SmallIncrement, exitEditMode, false )  )
						{
							// SSP 5/12/05 BR03883
							// Check if the OnScroll operation succeeds. If it does not then break out 
							// of the loop otherwise we will be stuck in an infinite loop.
							//
							if ( origFirstRow == this.firstRow )
								break;

							// reset the this.lastScrollType so we don't start doing
							// pixel level scrolling
							//
							this.lastScrollType = ScrollEventType.EndScroll;
							goto check_for_row_in_list;
						}						
						else
							// If BeforeRowRegionScroll event was cancelled, simply return
							return;
					}
					finally
					{
						this.dontUpdateInOnScroll = origDontUpdateInOnScroll;
					}

					// reset the this.lastScrollType so we don't start doing
					// pixel level scrolling
					//
                    // jJD 4/17/01
                    // unreachable code removed
                    //
					//this.lastScrollType = ScrollEventType.EndScroll;					
				}
			}

			// the row wasn't in the vr list so set it as the first row
			// and invalidate the display
			//
			// Use put_FirstRow instead, so that events get fired and scrolling does
			// not occur if BeforeRowRegionScroll event is cancelled 
			// put_FirstRow( &Row );	
		
			// RobA UWG869 12/26/01 
			// set the member variable so that we don't invalidate the 
			// rowscrollregion
			//
			//this.FirstRow = row;	
			// SSP 1/7/05 BR01428
			// We need to fire scroll events. Use the new overload of SetFirstRow that takes in
			// fireScrollEvents parameter.
			//
			// ----------------------------------------------------------------------------------
			//this.SetFirstRow( row, false );
			this.SetFirstRow( row, false, true );

			// If the scroll event was cancelled then return.
			//
			if ( this.firstRow != row )
				return;
			// ----------------------------------------------------------------------------------

			this.SetDestroyVisibleRows();

			this.RegenerateVisibleRows( false );

			// SSP 7/21/05 BR05030
			// When navigating child rows via down arrow (which skips the parent rows), scroll
			// in such a way that as many of the the current visible rows are kept in view.
			// 
			// ----------------------------------------------------------------------------------
			if ( maintainClosestScrollPosition && null != oldFirstRow && null != row )
			{
				int oldFirstRowScrollPos = oldFirstRow.ScrollPosition;
				int scrollPos = row.ScrollPosition;
				if ( oldFirstRowScrollPos >= 0 && oldFirstRowScrollPos < scrollPos )
				{
					Rectangle regionRect = Rectangle.Empty;
					this.GetRegionRect( ref regionRect, null, false, true );
					ScrollRowsContext scrollContext = new ScrollRowsContext( 
						ScrollEventType.LargeDecrement, this, this.ScrollBarInfo.Value, 
						this.ScrollBarInfo.Minimum, this.ScrollBarInfo.Maximum, regionRect );

					scrollContext.findScrollableArea = false;

					this.Layout.ViewStyleImpl.ScrollRows( ref scrollContext );

					// SSP 10/19/05 BR06988
					// 
					// ------------------------------------------------------------
					
					selfUsedFlag = false;
					this.ScrollRowIntoViewHelper( row, exitEditMode, true, false, ref selfUsedFlag );
					if ( selfUsedFlag )
						return;
					// ------------------------------------------------------------
				}
			}
			// ----------------------------------------------------------------------------------

			// SSP 7/31/03 UWG2258
			// When the UltraDropDown is dropped down and the selected item happens to be the
			// last row, we scroll that row into view and make it the first row. However that 
			// leaves empty space below that first row and it doesn't look good. We should 
			// scroll the row into view in such a way that we don't leave any space below it.
			// NOTE: This doesn't prevent the user from scrolling all the way up though.
			//
			// ----------------------------------------------------------------------------------------
			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
			// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
			//
			//if ( this.IsLastRowVisible( true ) )
			// SSP 9/19/05 BR05264
			// The following code makes sure that the UltraGrid doesn't leave big empty space
			// after the last visible row by scrolling down as much as possible while still
			// maintaining the last visible row fully in view. This code was added before 
			// ScrollBounds functionality was implemented. With ScrollBounds, this code not 
			// necessary anymore. However to maintain previous behavior we are leaving the 
			// code here, except modifying it to fix BR05264 for the UltraGrid.
			//
			//if ( this.IsLastScrollableRowVisible( true ) )
			if ( ( ScrollBounds.ScrollToLastItem != this.Layout.ScrollBoundsResolved || ! ( this.Layout.Grid is UltraGrid ) )
				&& this.IsLastScrollableRowVisible( true ) )
			{
				bool origDontUpdateInOnScroll = this.dontUpdateInOnScroll;
				this.dontUpdateInOnScroll = true;

				try
				{
					UltraGridRow lastValidFirstRow = null;

					do
					{
						lastValidFirstRow = this.FirstRow;

						// Scroll up by one row.
						//
						this.OnScroll( this.ScrollBarInfo, ScrollEventType.SmallDecrement, exitEditMode, false );

						// AS 8/18/03
						// Make sure that a scroll operation actually occured or
						// we can get caught in an infinite loop.
						//
						if (lastValidFirstRow == this.FirstRow)
							break;
					}
					// SSP 8/6/03
					// Pass in false for the assumeScrollbarsVisible.
					//
					//while ( this.IsLastRowVisible( true ) );
					// SSP 4/28/05 - NAS 5.2 Fixed Rows
					// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
					// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
					//
					//while ( this.IsLastRowVisible( false ) );
					while ( this.IsLastScrollableRowVisible( false ) );

					if ( null != lastValidFirstRow )
					{
						row = lastValidFirstRow;

						// SSP 1/7/05 BR01428
						// We need to fire scroll events. Use the new overload of SetFirstRow that takes in
						// fireScrollEvents parameter.
						//
						//this.SetFirstRow( row, false );			
						this.SetFirstRow( row, false, true );

						this.SetDestroyVisibleRows();
						this.RegenerateVisibleRows( false );

						// SSP 4/28/05 - NAS 5.2 Fixed Rows
						// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
						// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
						//
						//Debug.Assert( this.IsLastRowVisible( false ), "Above logic should have left the last absolute row still visible." );
						Debug.Assert( this.IsLastScrollableRowVisible( false ), "Above logic should have left the last absolute row still visible." );
					}
				}
				finally
				{
					// Ensure that we reset the flag.
					//
					this.dontUpdateInOnScroll = origDontUpdateInOnScroll;
				}
			}
			// ----------------------------------------------------------------------------------------

            // CDS 6/29/09 TFS14916 Find the index of the second non-fixed row.
            int secondNonFixedRowIndex = 1;
            for (int i = 0; i < visibleRows.Count; i++)
            {
                if (visibleRows[i].Row.FixedResolved)
                    secondNonFixedRowIndex++;
                else
                    break;
            }

            // CDS 6/29/09 TFS14916 Cannot use the hard-coded index of 1 as we need to take into
            // account fixed rows (including the Filter and AddNew rows).
            //// RobA UWG869 12/26/01
            //// If the oldFirstRow is really the second row in the regenerated visibleRows
            //// collection then reset the first row back to the old row and call 
            //// OnScroll with SmallDecrement
            ////	
            //if ( visibleRows.Count > 1  &&
            //     oldFirstRow == visibleRows[1].Row )
            if (visibleRows.Count > secondNonFixedRowIndex &&
                 oldFirstRow == visibleRows[secondNonFixedRowIndex].Row)
			{
				//this.FirstRow = oldFirstRow;
				this.SetFirstRow( oldFirstRow, false );
				
				this.SetDestroyVisibleRows();

				this.RegenerateVisibleRows( false );

				this.OnScroll( this.ScrollBarInfo, ScrollEventType.SmallDecrement, exitEditMode, false );

				return;
			}

			this.Invalidate();

			this.ResetScrollInfo();

			// SSP 10/19/05 BR06988
			// 
			selfUsedFlag = true;

			// SSP 5/12/05 - NAS 5.2 Fixed Rows
			// Don't call Update on the grid. This causes flicker under certain situations.
			//
			//this.Layout.Grid.Update();
		}

		internal void EnsureRowIsFullyInView( UltraGridRow row, bool exitEditMode )
		{
			this.EnsureRowIsFullyInView( row, exitEditMode, true );
		}

		// SSP 7/2/02
		// Added an overload of EnsureRowIsFullyInView that takes in scrollWholeCardArea parameter
		// that indicates whether to scroll the whole card row into view instead of just the
		// row.
		//
		internal void EnsureRowIsFullyInView( UltraGridRow row, bool exitEditMode, bool scrollWholeCardArea )
		{
			// iterate over the elem row collection looking for the pivot row 
			//
			VisibleRowsCollection visibleRows = this.VisibleRows;
			int			count = visibleRows.Count;
			int			minHeightRequired = 0;
			int			pixelsToScroll = 0;
			bool		rowFound = false;
			VisibleRow  visibleRow;

			// Use GetClientHeight to calc bottom since that takes into account
			// the col scrollbar.
			//
			int bottom = this.GetClientHeight( this.Layout.GetActiveColScrollRegion( false ) ); 

			for ( int i = 0; i < count; i++ )
			{
				visibleRow = visibleRows[i];

				if ( row.IsCard )
				{
					if ( row.ParentCollection == visibleRow.Row.ParentCollection )
					{
						rowFound			= true;

						// SSP 7/2/02
						// If scrollWholeCardArea is false, then use the passed in row's card height
						// because we only want to scroll the row into view and not the whle
						// card area.
						//
						if ( !scrollWholeCardArea )
						{
							// In the case of compressed card view use the height of the card.
							//
							minHeightRequired = row.CardHeight;	
						}
						else
						{
							minHeightRequired	= visibleRow.Band.CalculateRequiredCardAreaHeight( 1 );
						}
					}
				}
				else
				{
					if ( visibleRow.Row == row )
					{
						rowFound			= true;
						minHeightRequired	= row.TotalHeight;
					}
				}

				if ( rowFound )
				{
					// If this is the first row already then scrolling can't
					// help us
					//
					if ( i == 0 || visibleRow.Tier == 0 )
						return;

					int currentAvailableHeight	= bottom - visibleRow.GetTopOfRow( true );
					
					if ( currentAvailableHeight < minHeightRequired )
					{
						pixelsToScroll = minHeightRequired - currentAvailableHeight;
					}

					break;
				}
			}

			// if the row wasn't found in the list then set the first row
			// to it and exit
			//
			if ( !rowFound )
			{
				// exit edit mode before doing a scroll
				//
				if ( exitEditMode && this.Layout.ActiveCell != null )
					this.Layout.ActiveCell.ExitEditMode();

				this.SetFirstRow( row, true );
			
				this.SetDestroyVisibleRows();
				return;
			}

			if ( pixelsToScroll < 1 )
				return;

			// exit edit mode before doing a scroll
			//
			if ( exitEditMode && this.Layout.ActiveCell != null )
				this.Layout.ActiveCell.ExitEditMode();

			int fixedHeaderHeight = this.Layout.ViewStyleImpl.GetFixedHeaderHeight();

			rowFound = false;

			// Loop over the visible rows looking for a row whose
			// top is greater than the pixels to scroll plus the fixed header height
			//
			for ( int i = 1; i < count; i++ )
			{
				visibleRow = visibleRows[i];

				if ( row.IsCard )
				{
					if ( row.ParentCollection == visibleRow.Row.ParentCollection )
					{
						rowFound			= true;
					}
				}
				else
				{
					if ( visibleRow.Row == row )
					{
						rowFound			= true;
					}
				}

				if ( rowFound || visibleRow.GetTop() >= pixelsToScroll + fixedHeaderHeight )
				{
					// If the tier is less than 2 then just scrolldown 1 row
					// which is more efficient
					//
					if ( visibleRow.Tier < 2 )
					{
						if ( this.OnScroll( this.ScrollBarInfo, ScrollEventType.SmallIncrement, exitEditMode, false )  )
						{
							// reset the this.lastScrollType so we don't start doing
							// pixel level scrolling
							//
							this.lastScrollType = ScrollEventType.EndScroll;
						}						
					}
					else
					{
						// Otherwise, set the first row to this visible row
						// and invalidate the entire region
						//
						this.SetFirstRow( visibleRow.Row, true );
			
						this.SetDestroyVisibleRows();
					}

					return;
				}
			}
		}

		internal void SetDestroyVisibleRows()
		{
			SetDestroyVisibleRows( true, false );
		}

		internal void SetDestroyVisibleRows( bool Set )
		{
			SetDestroyVisibleRows( Set, false );
		}


		internal void SetDestroyVisibleRows( bool Set, bool setScrollToBeginning )
		{
			// Let the last param (fInvalidate) default to true so we don't
			// get into a state where the elemnt is marked dirty without an
			// invalidation
			//
			// SSP 5/29/02 UWG1153
			// Check for the layout being null because during deserialization,
			// it may be null.
			//
			if ( null != this.Layout )
				this.Layout.DirtyGridElement( false, false );
			this.destroyVisibleRows = Set;
			
			// If the range is too small (would lock the scrollbars then
			// set it higher. If we encountered eof then it will be reset back
			//
			if ( setScrollToBeginning )
			{
				this.SetPivotItem( null );
				this.SetFirstRow( null );
			}
		}


		private void GetRegionRect ( ref Rectangle regionRect ) 
		{
			this.GetRegionRect( ref regionRect, null, true, false );
		}
		private void GetRegionRect ( ref Rectangle regionRect, ColScrollRegion csr ) 
		{
			this.GetRegionRect( ref regionRect, csr, true, false );
		}
		private void GetRegionRect ( ref Rectangle regionRect, ColScrollRegion csr, bool clipToParent ) 
		{
			this.GetRegionRect( ref regionRect, csr, clipToParent, false );
		}
	
		private void GetRegionRect ( ref Rectangle regionRect, 
			ColScrollRegion colRegion, 
			bool clipToParent,
			bool clipToDataArea ) 
		{			
			if ( null != this.ClonedFrom  )
			{
				((RowScrollRegion)this.ClonedFrom).GetRegionRect(ref regionRect);
				return;
			}

			Rectangle dataAreaRect = new Rectangle(0,0,0,0);
			UIElement dataArea = null;

			// Get the DataArea element rect (if exists)
			//
			UIElement element = this.Layout.GetUIElement( false );

			if ( null != element )
			{
				dataArea = element.GetDescendant( typeof( DataAreaUIElement ) );

				if ( null != dataArea )
					dataAreaRect = dataArea.RectInsideBorders;
			}

			// Instead of using the grid client rect for left and right use the new
			// uielement instead (this allows for other areas like the caption to be excluded).
			//
			if ( null != colRegion )
			{
				regionRect.X       = colRegion.Origin;
				regionRect.Width   = colRegion.Extent;
			}
			else
			{
				// Use UIElement to get the rect
				//
				if ( null != dataArea )
				{
					regionRect = dataArea.Rect;
				}
				else
				{					
					regionRect.X = this.Layout.GetColScrollRegions( false ).GetNextVisibleRegion ( null, false ).Origin;
					ColScrollRegion  lastVisibleColRegion = (ColScrollRegion)this.Layout.GetColScrollRegions( false ).LastVisibleRegion;
					regionRect.Width = lastVisibleColRegion.Origin + lastVisibleColRegion.Extent - regionRect.X;
				}
			}

			regionRect.Y    = this.Origin;
			regionRect.Height = this.Extent;

			// If we have a DataArea element then intersect with it
			//
			if ( clipToDataArea && null != dataArea )
			{
				regionRect = Rectangle.Intersect( regionRect, dataAreaRect );
			}

			if ( null != colRegion && colRegion.WillScrollbarBeShown() )
			{
				regionRect.Height -= SystemInformation.HorizontalScrollBarHeight;
			}

			if ( this.WillScrollbarBeShown() )
			{
				regionRect.Width -= SystemInformation.VerticalScrollBarWidth;
			}

			if ( clipToParent )
				this.Layout.ClipToParent( ref regionRect, true );
		}

		internal bool ScrollRows( ScrollEventType scrollType, ref ScrollRowsContext src )
		{
			if ( !this.IsClone )
			{
				Debug.Fail("ScrollRows can only be caled on a cloned RowScrollRegion");
				return false;
			}

			UltraGridLayout layout = this.Layout;
			ViewStyleBase viewStyle = layout.ViewStyleImpl;
			if ( null == viewStyle )
				return false;

			int rowPosition;
			UltraGridRow row;

			switch ( scrollType )
			{
				
				case ScrollEventType.ThumbTrack:
				{
					int maxPosition = layout.RowScrollRange - 1;

					rowPosition = src.scrollTrackPos * this.scrollbarFactor;

					// if the scrollbar is at the bottom then ensure that we get the last row
					//

					if ( rowPosition > maxPosition )
						rowPosition = maxPosition;

					// get the row at this track position
					//					
					row = layout.GetRowAtScrollPos( rowPosition ); 

					//UltraGrid grid = this.Layout.Grid as UltraGrid;
										
					if ( null != row       && 
						//null !=grid   &&
						row.Band.ScrollTipsEnabled )
					{
						
						//Infragistics.Win.ToolTip toolTip = grid.GetTooltipTool();
						Infragistics.Win.ToolTip toolTip = layout.Grid.TooltipTool;
						
						if ( null != toolTip )
						{
							Rectangle exclusionAreaRect;

							// AS - 12/14/01
							//exclusionAreaRect = this.ClonedFrom.ScrollBarInfo.Bounds;
							exclusionAreaRect = this.ClonedFrom.ScrollbarRect;

							// set up an exclusion rect that is somewhat larger than the
							// scrollbar rect. The tooltip is prevented from overlapping
							// this rect.
							//
							exclusionAreaRect.X   -= SystemInformation.VerticalScrollBarWidth;
							exclusionAreaRect.Width  +=  2*SystemInformation.VerticalScrollBarWidth;

							// SSP 4/2/04 UWG2956
							// Use ControlForGridDisplay instead of Grid because in the case of UltraCombo,
							// control that's displaying the grid is not the UltraCombo, but it's the control
							// that gets embedded in the drop down form.
							//
							//exclusionAreaRect = this.Layout.Grid.RectangleToScreen( exclusionAreaRect );
							exclusionAreaRect = layout.Grid.ControlForGridDisplay.RectangleToScreen( exclusionAreaRect );

							Point cursorPos = System.Windows.Forms.Cursor.Position;

							
							Rectangle currentRect  = new Rectangle( cursorPos.X, cursorPos.Y, 1, 1 );

							// SSP 12/20/01
							// Use the WorkingArea off the SystemInformation instead.
							//
							//Rectangle desktopRect = Infragistics.Win.UIElement.GetDeskTopWorkArea( currentRect );
							// MD 7/26/07 - 7.3 Performance
							// FxCop - Remove unused locals
							//Rectangle desktopRect = SystemInformation.WorkingArea;

							if ( ! this.ClonedFrom.isScrollTipActive )
							{
								Rectangle workRect = this.ClonedFrom.scrollTipRect;

								// on the first thumb track initialize the cached tooltip rect
								//
								workRect.X		= exclusionAreaRect.X;
								workRect.Y		= System.Math.Max( cursorPos.Y, exclusionAreaRect.Y );
                        		workRect.Size	= Size.Empty;

								this.ClonedFrom.scrollTipRect = workRect;

								this.ClonedFrom.isScrollTipActive = true;
							}

							
							// get the tip string and its size
							//
							int  lines = 0;
							// SSP 7/19/05
							// TipStyleScroll is used by the scrolling as well as the row connector tooltips. We 
							// need to resolve the TipStyleScroll differently based on whether the scrolling logic 
							// or the row connector logic is calling it.
							// 
							//string strTip = row.GetScrollTip( ref lines );
							string strTip = row.GetScrollTip( ref lines, false );
							if ( null != strTip && strTip.Length > 0 )
							{								
								toolTip.SetMargin( 3, 3, 0, 0 );
								toolTip.ToolTipText = strTip;
								Size size = toolTip.CalcRequiredSize( );
								

								//if the scrollTipRect crossed over the exclusionArea then offset it accordingly
								if ( this.ClonedFrom.ScrollTipRect.X + size.Width  > exclusionAreaRect.Left )
								{
									Rectangle workRect = this.ClonedFrom.scrollTipRect;
									workRect.Offset( - ( workRect.X + size.Width - exclusionAreaRect.Left ), 0 );
									this.ClonedFrom.scrollTipRect = workRect;
								}
                            
								//if the size of the tool tip window has increased then update the cached scrollTipRect too
								if ( this.ClonedFrom.ScrollTipRect.Width < size.Width )
								{
									Rectangle workRect = this.ClonedFrom.scrollTipRect;
									workRect.Width = size.Width;
									this.ClonedFrom.scrollTipRect = workRect;
								}
								if ( this.ClonedFrom.ScrollTipRect.Height < size.Height )
								{
									Rectangle workRect = this.ClonedFrom.scrollTipRect;
									workRect.Height = size.Height;
									this.ClonedFrom.scrollTipRect = workRect;
								}

								//RobA UWG317 9/25/01
								toolTip.AutoPopDelay = 0;
								
								// SSP 10/25/01  
								// Use the grid's font to display the tool tip since
								// the font that tool tip has could be different ( may be
								// a cell's font ).
								//
								toolTip.Font = layout.Grid.Font;

								if ( !toolTip.Control.Visible || 
									!toolTip.Control.Bounds.Contains( this.ClonedFrom.scrollTipRect ) )
								{
									toolTip.Show( this.ClonedFrom.scrollTipRect, 0, true, true, true,
										exclusionAreaRect, true, false );									
								}
							}							
						}
						
					}					
				
					// SSP 2/28/02
					// Now we are doing immediate scrolling and deferred scrolling depending
					// on the value of newly added property ScrollStyle off the layout.
					// So if the ScrollStyle is set to Immediate, then do not return
					// and execute the code below that actually does the scrolling.
					//					
					// Added if condition to already existing return false statement.
					//
					if ( Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate != layout.ScrollStyle )
					{
						return false;
					}
					else
					{
						// if the scrollbar is at the bottom then ensure that we get the last row
						//
						if ( rowPosition > maxPosition )					
						{
							rowPosition = maxPosition;

							// If the last row is already visible then we can just ignore this
							// and return
							//
							//						if ( this.IsLastRowVisible() )
							//							return false;
						}

						// SSP 9/6/06 - NAS 6.3 - Optimizations - BR13748 BR14522
						// Added the if block and enclosed the existing code into the else block.
						
						// 
						// ----------------------------------------------------------------------------
						if ( viewStyle.SupportsFixedRows )
						{
							viewStyle.ScrollRows( ref src );
						}
						else
						{
							// get the row at this track position
							//
							row =  layout.GetRowAtScrollPos( rowPosition );

							// SSP 10/20/03 UWG2580
							// If the new first row differs from the previous first row, then recreate the
							// visible rows.
							//
							// --------------------------------------------------------------------------
							bool recreateVisibleRows = this.firstRow != row;
							// --------------------------------------------------------------------------

							// set the row
							//
							this.SetFirstRow( row );

							// SSP 10/20/03 UWG2580
							// 
							// --------------------------------------------------------------------------
							if ( recreateVisibleRows )
								viewStyle.RecreateRowList( this );
							// --------------------------------------------------------------------------
				
							// mark the cloned from region's list dirty
							//					
							this.ClonedFrom.SetDestroyVisibleRows( true );

							// set the flags to cause an invalidation instead of a scroll
							//
							src.invalidate     = true;
							src.scrollDelta    = 0;
							//						ScrollRowsContext tmp = src;
							//						tmp.scrollType = ScrollEventType.EndScroll;
							//						viewStyle.ScrollRows( ref tmp );
							//						src = tmp;
							//						src.scrollType = ScrollEventType.ThumbTrack;
						}
						// ----------------------------------------------------------------------------
					}
					break;
				}

				case ScrollEventType.ThumbPosition:
				{
					int maxPosition = layout.RowScrollRange - 1;

					rowPosition = src.scrollTrackPos * this.scrollbarFactor;

					// if the scrollbar is at the bottom then ensure that we get the last row
					//
					if ( rowPosition > maxPosition )					
					{
						rowPosition = maxPosition;

						// If the last row is already visible then we can just ignore this
						// and return
						//
//						if ( this.IsLastRowVisible() )
//							return false;
					}

					// get the row at this track position
					//
					row =  layout.GetRowAtScrollPos( rowPosition );

					// SSP 10/20/03 UWG2580 UWG2733
					// If the new first row differs from the previous first row, then recreate the
					// visible rows.
					//
					// --------------------------------------------------------------------------
					bool recreateVisibleRows = this.firstRow != row;
					// --------------------------------------------------------------------------

					// set the row
					//
					this.SetFirstRow( row );

					// SSP 10/20/03 UWG2580 UWG2733
					// 
					// --------------------------------------------------------------------------
					if ( recreateVisibleRows )
						viewStyle.RecreateRowList( this );
					// --------------------------------------------------------------------------
										
					// mark the cloned from region's list dirty
					//					
					this.ClonedFrom.SetDestroyVisibleRows( true );

					// set the flags to cause an invalidation instead of a scroll
					//
					src.invalidate     = true;
					src.scrollDelta    = 0;				
					break;
				}
				
				case System.Windows.Forms.ScrollEventType.EndScroll:
					
					if ( this.ClonedFrom.isScrollTipActive )
					{						
						this.ClonedFrom.isScrollTipActive = false;

						//if ( this.Layout.Grid is UltraGrid )
						//{
						//	Infragistics.Win.ToolTip toolTip = ((UltraGrid)(this.Layout.Grid)).GetTooltipTool();

							Infragistics.Win.ToolTip toolTip = layout.Grid.TooltipTool;

							if ( null != toolTip )
								toolTip.Hide();
						//}
					}					
					viewStyle.ScrollRows( ref src );
					break;
				default:					
					viewStyle.ScrollRows( ref src );
					break;
			}

			// Moved logic for firing the before scroll event into InternalScrollToNewFirstRow
			// so it could be called from put_FirstRow as well
			//			
			if ( !this.InternalScrollToNewFirstRow( false ) )
			{
				// if we had been doing pixel level scrolling (during a
				// repeated lineup/down operation) we need to realign
				// the row elements so the first row is aligned at its top
				//				
				if ( 0 != this.scrollOffset )
				{
					Rectangle regionRect = new Rectangle(0,0,0,0);

					this.ClonedFrom.GetRegionRect( ref regionRect, null, false );

					// notify the viewstyle that the cancel occurred
					//
					viewStyle.ScrollRowsCancelled( ref src );

					// invalidate the entire region
					//
					if ( null != layout.Grid )
					{
						// JJD 10/02/01
						// Invalidate the ControlForGridDisplay instead to support
						// the UltraCombo
						//
						//this.Layout.Grid.Invalidate( , false );						
						layout.Grid.ControlForGridDisplay.Invalidate( regionRect, false );
					}
				}

				return false;
			}

			return true;
		}
		
		
		internal override bool OnScroll( ScrollBarInfo scrollBar,
			ScrollEventType scrollType, bool exitEditMode, bool invalidateRegion )
		{
			UltraGridLayout layout = this.Layout;

			// AS 1/16/06
			// While debugging another issue, I came across a null ref while
			// canceling the designer dialog because the Layout was null. Since
			// we seem to be making lots of assumptions here that the Layout is
			// non-null, we'll just get out when it is - which should only be
			// during deserialization.
			// 
			if (layout == null)
				return true;

			// call this function so that the grid exits the edit mode
			// on active cell and does data updating if UpdateMode
			// is set to OnCellChangeOrLostFocus or OnRowChangeOrLostFocus
			//this.Layout.Grid.InternalLostFocus( true );

			// exit edit mode on scroll
			if ( null != layout.ActiveCell && exitEditMode )
			{				
				layout.ActiveCell.ExitEditMode();

				// SSP 5/3/02
				// If we did not exit the edit mode either because the ExitEditMode
				// event was cancelled or because there was an invalid value in the
				// cell, then we do not want to scroll.
				//
				if ( null != layout.ActiveCell && layout.ActiveCell.IsInEditMode )
				{
					// Cancel the scroll. Pass in true to prevent any scroll notifications from
					// coming in.
					//
					scrollBar.CancelScroll( true );

					return false;
				}

				// hide HTML doc window, if we have one				
				//this.Layout.HideHTMLDocWindow();
			}			
			
			
			Debug.Assert( scrollBar == this.ScrollBarInfo, "Wrong scrollbar passed into CRowScrollRegion::OnScroll");

			// clone ourself
			//
			RowScrollRegion clone = (RowScrollRegion)this.Clone( );

			// SSP 4/12/04
			// We need to clear the visible rows of the clone rsr so that the visible rows of 
			// the rsr unhook from the rows' SubObjectPropChange and Disposed events. NOTE: If
			// you change this to make use of the Dispose/OnDispose mechanism to ensure visible
			// rows of clone unhook from rows, then keep in mind that Clone method does a 
			// MemberWiseClone which will also copy over the scrollbar info etc.
			//
			try
			{
				if ( null == clone )
					return false;
				//Debug.WriteLine( "OnScroll before update" );

				// SSP 5/23/02
				// Implemented code to not to invalidate and/or update the control if IsUpdating
				// is ture because we are going to be invalidating the whole grid once EndUpdate
				// is called anyways.
				//
				bool isUpdating = null != layout && null != layout.Grid && layout.Grid.IsUpdating;

				// SSP 4/23/03 - Optimizations
				// if we are doing a page up down or thumb scrolling where we are going to
				// invalidate the whole grid, then don't call update on the grid.
				// 
			
				// SSP 7/31/03 UWG2258
				// Added dontUpdateInOnScroll flag to prevent the OnScroll from calling 
				// Update on the control.
				//
				//bool shouldUpdate = 
				// SSP 10/14/03
				// Also if we are in the middle of BeginUpdate and EndUpdate, then we shouldn't
				// call Update on the control.
				//
				//bool shouldUpdate = ! this.dontUpdateInOnScroll &&
				bool shouldUpdate = ! isUpdating && ! this.dontUpdateInOnScroll &&
					( ScrollEventType.LargeDecrement != scrollType &&
					ScrollEventType.LargeIncrement != scrollType &&
					ScrollEventType.ThumbPosition != scrollType 
					// SSP 5/23/05- Optimizations
					// Use scroll window when doing ThumbTrack as well.
					//
					//&& ScrollEventType.ThumbTrack != scrollType 
					);

				// SSP 10/13/03 UWG2057
				// Instead of calling ControlForDisplay all over the place, use a local variable.
				//
				Control control = null != layout && null != layout.Grid ? layout.Grid.ControlForGridDisplay : null;

				// JJD 12/19/01
				// Moved call to Update to before we change any of our
				// state to reflect the new scroll position
				//
				// SSP 5/23/02
				// If we are currently updating (IsUpdating off the grid is true) then don't 
				// invalidate and/or update the control because we will invalidate the control
				// when EndUpdate is called.
				//
				//if ( null != this.Layout.Grid && this.Layout.Grid.Created )
				// SSP 10/13/03 UWG2057
				// Don't check if the grid has been created or not. Instead check if the
				// display control (ControlFroDisplay) has been created. In the case of
				// an UltraCombo which is hidden, Created will return false since it's
				// never been made visible. However it's drop down portion may be
				// visible if it were dropped down in a grid.
				//
				//if ( shouldUpdate && !isUpdating && null != this.Layout.Grid && this.Layout.Grid.Created )
				//	this.Layout.Grid.ControlForGridDisplay.Update();
				if ( shouldUpdate && !isUpdating && null != layout.Grid && null != control && control.Created )
				{
					control.Update();

					// SSP 11/18/05 BR07751
					// If the control gets invalidated from within the last paint, then update again.
					// Note that if nothing got invalidated in the last paint (the temp active row gets
					// invalidated), then this will result in a no-op and therefore it should not 
					// introduce any efficiency issues.
					// 
					control.Update( );
				}

				UltraGridRow holdFirstRow       = this.firstRow;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//ScrollEventType holdLastScrollType = this.lastScrollType;

				Rectangle regionRect = new Rectangle(0,0,0,0);

                // MRS 4/17/2009 - TFS16751
                // Store the ScrollBarInfo Value. The call to GetRegionRect (below)
                // will call WillScrollBarBeShown, which ultimately checks to see if 
                // this.DetroyVisibleRows is true and if so, regenerates the VisibleRows. 
                // When this happens, the ScrollBarInfo.Value gets reset based on the current
                // FirstRow, thus cancelling the scroll operation unintentionally. 
                // So by storing the value here, before the GetRegionRect is called, we 
                // ensure that we use the right value to scroll to. 
                //                
                int scrollBarInfoValue = this.ScrollBarInfo.Value;

				// Pass true for last param (clipToDataArea)
				//			
				this.GetRegionRect( ref regionRect, null, false, true );

				// JJD 12/19/01
				// cache whether any partially visible cards are in view
				//
				bool werePartialCardsInView = this.AreCardsInView( true );
			
				ScrollRowsContext src;

                // MRS 4/17/2009 - TFS16751 
                // See previous note for the explanation of why we are using a local 
                // variable here instead of hte ScrollBarInfo.Value.
                //
				//src = new ScrollRowsContext( scrollType, clone, this.ScrollBarInfo.Value, 
                src = new ScrollRowsContext(scrollType, clone, scrollBarInfoValue, 
					this.ScrollBarInfo.Minimum, this.ScrollBarInfo.Maximum, regionRect );    

				src.forceScroll =  this.forceScroll;
				this.forceScroll = false;

				// call ScrollPositionItems on the clone
				//
				bool scrolled = clone.ScrollRows( scrollType, ref src );
			
				if ( !scrolled )
					return false;

				this.ResetScrollInfo();

				// SSP 10/9/03 UWG1868
				// We need to suspend the SyncMouseEnter logic while updating the control
				// because calling OnMouseEnter and OnMouseLeave cause some elements to
				// invalidate themselves which will cause flickers in grid.
				// Enclosed the following code in try-finally so we can suspend and resume
				// mouse sync enter logic.
				//
				try
				{
					//Debug.WriteLine( "OnScroll before update2" );
					// JJD 12/26/01
					// Call Update again to force a draw of the possibly invalidated
					// scrollbars from the call to ResetScrollInfo above
					//
					// SSP 10/13/03 UWG2057
					// Don't check if the grid has been created or not. Instead check if the
					// display control (ControlFroDisplay) has been created. In the case of
					// an UltraCombo which is hidden, Created will return false since it's
					// never been made visible. However it's drop down portion may be
					// visible if it were dropped down in a grid.
					//
					//if ( shouldUpdate && null != this.Layout.Grid && this.Layout.Grid.Created )
					if ( shouldUpdate && null != control && control.Created )
					{
						// SSP 10/9/03 UWG1868
						// Suspend the sync-enter logic.
						//
						layout.UIElement.InternalSuspendSyncMouseEnter( );

						// SSP 3/11/05 BR02633
						// 
						// ------------------------------------------------------------
						//control.Update( );
						int origPaintCount = layout.DrawSequence;
						control.Update( );
						if ( origPaintCount != layout.DrawSequence )
						{
							Rectangle lastPaintRect = layout.lastPaintRect;
							if ( lastPaintRect.Width >= regionRect.Width 
								&& lastPaintRect.Height >= regionRect.Height )
								shouldUpdate = false;
						}
						// ------------------------------------------------------------
					}

					// SSP 10/13/03 UWG2057
					// Don't check if the grid has been created or not. Instead check if the
					// display control (ControlFroDisplay) has been created. In the case of
					// an UltraCombo which is hidden, Created will return false since it's
					// never been made visible. However it's drop down portion may be
					// visible if it were dropped down in a grid.
					//
					//if ( null != this.Layout.Grid && this.Layout.Grid.Created )
					if ( null != control && control.Created )
					{
						// SSP 10/13/03 UWG2057
						// Moved this above.
						//
						//System.Windows.Forms.Control control = this.Layout.Grid.ControlForGridDisplay;

						// check to see if this is the last row scrolling region.
						// In that case we need to adjust the size of the scrollbar 
						// so it doesn't overlap the horizontal scrollbar and invalidate
						// any col scrolling regions that don't have the scrollbar
						// 
						RowScrollRegionsCollection rsrColl =  layout.RowScrollRegions;
    
						Debug.Assert( null != rsrColl, "No row scrolling region collection in RowScrollRegion.OnScroll" );

						if ( null != rsrColl )
						{
							if ( this.IsLastVisibleRegion )
							{
								Rectangle horzScrollbarRect = regionRect;						
								horzScrollbarRect.Y = regionRect.Bottom - SystemInformation.HorizontalScrollBarHeight;
								// SSP 10/22/04 UWG2980
								// 
								horzScrollbarRect.Height = regionRect.Bottom - horzScrollbarRect.Y;

								ColScrollRegionsCollection  csrColl = layout.GetColScrollRegions( false );

								Debug.Assert( null != csrColl, "No col scrolling region collection in RowScrollRegion.OnScroll" );
				
								// SSP 10/22/04 UWG2980
								//
								// ------------------------------------------------------------------------------
								
								int horizScrollbarVisibleCount = 0;
								for ( int i = 0; i < csrColl.Count; i++ )
								{
									if ( csrColl[i].WillScrollbarBeShown( ) )
										horizScrollbarVisibleCount++;
								}

								if ( horizScrollbarVisibleCount > 0 )
								{
									// If at least one horizontal scrollbar is visible then exclude the 
									// horizontal scrollbar area from the region rect.
									//
									regionRect.Height = horzScrollbarRect.Top - regionRect.Top;

									// SSP 5/24/05 BR04058
									// We still need to invalidate the scrollbar area even if shouldUpdate 
									// is false. Instead we need to check for isUpdating.
									// 
									//if ( horizScrollbarVisibleCount != csrColl.Count && shouldUpdate )
									if ( horizScrollbarVisibleCount != csrColl.Count && ! isUpdating )
									{
										control.Invalidate( horzScrollbarRect, false );

										// SSP 5/24/05 BR04058
										// Related to above change.
										//
										if ( shouldUpdate )
											control.Update( );
									}
								}
								// ------------------------------------------------------------------------------
							}
						}

						// SSP 7/3/02
						// We also don't want to scroll window if any cards came to be partially
						// visible.
						//
						werePartialCardsInView = werePartialCardsInView || this.AreCardsInView( true );

						if ( 0 != src.scrollDelta  
							// SSP 7/3/02
							// scrollDelta may be 0 if there were partial cards in view and scrolling
							// cause the card to not change it's bott (because it was the last card
							// and when scrolled up or down it still is the last card. Remember we
							// resize the card if it can't fit in a scroll region.
							//
							|| werePartialCardsInView )
						{
							// we have a non-zero scoll delta so setup up our
							// call to scrollwindowex
							//
							short smoothScrollTime = 0;

							// JJD 11/02/01
							// Don't try to use smoothscrolling. There are problems with it
							//
							
							src.scrollRect.X		= regionRect.Left;
							src.scrollRect.Width	= regionRect.Width;

							// make sure the scroll rect is not outside our region boundaries
							//
							// If the caller passes in true for invalidateRegion,
							// do not use ScrollWindowEx() - move right down to
							// the bottom, where we invalidate.
							//
							src.scrollRect = Rectangle.Intersect(src.scrollRect, regionRect );

							// JJD 11/06/01
							// If we are doing a page down operation invalidate the region
							//
							if ( scrollType == ScrollEventType.LargeDecrement  ||
								scrollType == ScrollEventType.LargeIncrement )
								invalidateRegion = true;

							if ( ! shouldUpdate )
								invalidateRegion = true;
					
							// JJD 11/05/01
							// Check new IsBackgroundScrollable property which returns false
							// if we are draing a backgroun image of doing a gradient fill
							// of the background
							//
							// JJD 12/19/01
							// Don't call ScrollWindo if partial cards were in view
							//
							if ( !werePartialCardsInView &&
								// SSP 7/21/03 UWG1997
								// Added UseScrollWindow property so that the user can prevent the grid from using 
								// ScrollWindow method for scrolling.
								// Added vertical parameter to specify the scroll direction.
								//
								//this.Layout.IsBackgroundScrollable &&
								layout.IsBackgroundScrollable( true ) &&
								!src.scrollRect.IsEmpty && 
								! invalidateRegion )
							{
								// initialize both the clip and the exposed rect to the scroll
								// rect
								//
								Rectangle clipRect         = src.scrollRect;
								Rectangle exposedRect      = src.scrollRect;

								// offset the clip rect so that it reflects the destination 
								// of the scroll rect
								//
								clipRect.Offset( 0, src.scrollDelta );

								// make sure the clip rect is not outside our region boundaries
								//
								clipRect = Rectangle.Intersect( clipRect, regionRect );
								if ( !clipRect.IsEmpty )
								{
									if ( src.invalidate )
									{
										src.invalidRect.X   = regionRect.Left;
										src.invalidRect.Width  = regionRect.Right-src.invalidRect.X;

										// SSP 5/23/05 - NAS 5.2 Fixed Rows
										// Added invalidRect2 on the ScrollRowsContext. Also assign the correct
										// x and width to the invalidRect2 just like how we are doing above to
										// invalidRect.
										// 
										// SSP 5/31/05 BR04349
										// Only do so if invalidRect2 is not empty. Enclosed the existing code 
										// into the if block.
										//
										if ( ! src.invalidRect2.IsEmpty )
										{
											src.invalidRect2.X   = src.invalidRect.X;
											src.invalidRect2.Width  = src.invalidRect.Width;
										}

										Rectangle workRect;

										// make sure the invalid rect is not outside our region boundaries
										//								
										src.invalidRect = Rectangle.Intersect( src.invalidRect, regionRect );

										// check if the invalid rect overlaps the clip rect.
										//
										workRect = Rectangle.Intersect( src.invalidRect, clipRect );
										if ( !workRect.IsEmpty )
										{
											// the rects overlap so if the interection is at the
											// top, set the clip rect's top to eliminate the overlap.
											// Otherwise, adjust its bottom if that is where the overlap
											// is.
											//
											// Note: We should never get a situation where the invalid
											//       rect is entirely contained in the middle of the clip rect
											//
											if ( workRect.Top == clipRect.Top )
											{
												clipRect.Y = workRect.Bottom;

												// SSP 11/8/04 UWG2924 BR01557 BR01542
												// We need to adjust the height here after adjusting the top by 
												// the same amount.
												//
												clipRect.Height -= workRect.Height;
											}
											else if ( workRect.Bottom == clipRect.Bottom )
												clipRect.Height   = workRect.Top - clipRect.Top;
											else
												Debug.Assert( false, "Invalid scroll and invalid rects in RowScrollRegion.OnScroll" );
										}
									}

									// depending on whether we are scrolling up or down
									// set the exposed rects top or bottom coord
									//
									if ( src.scrollDelta < 0 )
									{
										exposedRect.Y		= clipRect.Bottom;

										// JJD 11/26/01 - UWG683 and UWG710 
										// Adjust the height of the exposed rect to go all the 
										// way to the bottom of the scrolling region
										//
										exposedRect.Height	= regionRect.Bottom - exposedRect.Top;
									}
									else
										exposedRect.Height = clipRect.Top - exposedRect.Top;

									// make sure the exposed rect is not outside our
									// region boundaries
									//							
									exposedRect = Rectangle.Intersect( exposedRect, regionRect );
							
									// AS 1/8/03 fxcop
									// Cannot have 2 public/protected members that differ only by case
									//this.cachedScrollOrigin -= src.scrollDelta;
									this.cachedScrollOriginValue -= src.scrollDelta;

									// now scroll the window
									// No ScrollWindowEx equivalent was found in the .NET framework
									// So we are causing it to redraw the whole grid
							
									// JJD 10/02/01
									// Invalidate the ControlForGridDisplay instead to support
									// the UltraCombo
									//
									//this.Layout.Grid.Invalidate( src.scrollRect ); 

									Rectangle updateRect = Rectangle.Empty;

									//this.Layout.Grid.Invalidate( horzScrollbarRect, false );
									// SSP 5/23/02
									// If we are currently updating (IsUpdating off the grid is true) then don't 
									// invalidate and/or update the control because we will invalidate the control
									// when EndUpdate is called.
									// If updating, then skip all the logic for doing the invalidation.
									//
									if ( !isUpdating )
									{
										// JJD 12/19/01
										// Moved call to Update to before we change any of our
										// state to reflect the new scroll position
										//
										//							control.Update();

                                        // JAS 10/11/04 - This informs the ComboDropDownControl, if there is one, that
                                        // we are scrolling and that it should ignore any calls to Refresh().
                                        if( layout != null )
                                            layout.IsScrolling = true;

										// MD 8/7/07 - 7.3 Performance
										// FxCop - Do not cast unnecessarily
										//if ( control is UltraGridBase )
										//{
										//    // InternalScrollControl will work in situations where
										//    // DrawUtility.ScrollControl might not. For example, when
										//    // the Infragistics.Win assembly has unmanaged code rights
										//    // but the calling application doesn't
										//    //
										//    ((UltraGridBase)control).InternalScrollControl( 0, 
										UltraGridBase grid = control as UltraGridBase;

										if ( grid != null )
										{
											// InternalScrollControl will work in situations where
											// DrawUtility.ScrollControl might not. For example, when
											// the Infragistics.Win assembly has unmanaged code rights
											// but the calling application doesn't
											//
											grid.InternalScrollControl( 0, 
												src.scrollDelta,
												regionRect, //src.scrollRect,
												clipRect,
												ref updateRect,
												false,
												true,
												true,
												smoothScrollTime );
										}
										else
										{
											DrawUtility.ScrollControl( control,
												0, 
												src.scrollDelta,
												regionRect, //src.scrollRect,
												clipRect,
												ref updateRect,
												false,
												true,
												true,
												smoothScrollTime );
										}

                                        // JAS 10/11/04
                                        if( layout != null )
                                            layout.IsScrolling = false;

										// SSP 5/23/05 - NAS 5.2 Fixed Rows - Optimizations
										// Enclosed the existing code into the if block. If invalidRect2 is set
										// then we don't need to invalidate the exposed rect. InvalidRect2 would
										// already have been set to include the exposed rect. Note that this is
										// an optimization. If it causes a problem take it out.
										//
										if ( src.invalidRect2.IsEmpty )
										{
											// invalidate the exposed area
											//
											// JJD 10/02/01
											// Invalidate the ControlForGridDisplay instead to support
											// the UltraCombo
											//
											//this.Layout.Grid.Invalidate( exposedRect, false );
											control.Invalidate( exposedRect, false );

											//Debug.WriteLine( "OnScroll before scrollwindow update" );

											// JJD 11/02/01
											// Force the paint by calling update
											//
											control.Update();
										}
									}
								}
								else
								{
									// the clipping rect wasn't in the region
									// so just invalidate everything
									//
									src.invalidRect         = regionRect;
									src.invalidate          = true;
									// AS 1/8/03 fxcop
									// Cannot have 2 public/protected members that differ only by case
									//this.cachedScrollOrigin = RowScrollRegion.SCROLL_ORIGIN_BASE;
									this.cachedScrollOriginValue = RowScrollRegion.SCROLL_ORIGIN_BASE;
								}
							}
							else
							{
								// the scrolling rect wasn't in the region
								// so just invalidate everything
								//
								src.invalidRect         = regionRect;
								src.invalidate          = true;
								// AS 1/8/03 fxcop
								// Cannot have 2 public/protected members that differ only by case
								//this.cachedScrollOrigin = RowScrollRegion.SCROLL_ORIGIN_BASE;
								this.cachedScrollOriginValue = RowScrollRegion.SCROLL_ORIGIN_BASE;
							}
						}

						// if the invalidate flag was set then do the invalidation
						//				
						if ( src.invalidate )
						{
							src.invalidRect = Rectangle.Intersect( src.invalidRect, regionRect );

							// SSP 5/23/05 - NAS 5.2 Fixed Rows
							// Added invalidRect2 on the ScrollRowsContext.
							//
							// --------------------------------------------------------------------
							src.invalidRect2 = Rectangle.Intersect( src.invalidRect2, regionRect );

							// If the two invalid rects intersect then perform a single paint as an 
							// optimization.
							//
							if ( src.invalidRect.IntersectsWith( src.invalidRect2 ) )
							{
								src.invalidRect = Rectangle.Union( src.invalidRect, src.invalidRect2 );
								src.invalidRect2 = Rectangle.Empty;
							}

							if ( ! src.invalidRect2.IsEmpty && ! isUpdating )
							{
								control.Invalidate( src.invalidRect2, false );

								// Call update to make sure that invalidation above takes effect.
								//
								if ( shouldUpdate )
									control.Update( );
							}
							// --------------------------------------------------------------------

							if ( !src.invalidRect.IsEmpty ) 
							{

								// JJD 10/02/01
								// Invalidate the ControlForGridDisplay instead to support
								// the UltraCombo
								//
								//this.Layout.Grid.Invalidate( src.invalidRect, false );
								//this.Layout.Grid.Invalidate( horzScrollbarRect, false );
								// SSP 5/23/02
								// If we are currently updating (IsUpdating off the grid is true) then don't 
								// invalidate and/or update the control because we will invalidate the control
								// when EndUpdate is called.
								// If updating, then skip all the logic for doing the invalidation.
								//
								if ( !isUpdating )
									control.Invalidate( src.invalidRect, false );
							}
						}

						//Debug.WriteLine( "OnScroll before last update" );

						// JJD 11/02/01
						// Force the paint by calling update
						//
						//this.Layout.Grid.Invalidate( horzScrollbarRect, false );
						// SSP 5/23/02
						// If we are currently updating (IsUpdating off the grid is true) then don't 
						// invalidate and/or update the control because we will invalidate the control
						// when EndUpdate is called.
						// If updating, then skip all the logic for doing the invalidation.
						//
						if ( shouldUpdate && !isUpdating )
							control.Update();
					}
				}
					// SSP 10/9/03 UWG1868
					// Enclosed the code in try-finally so we can suspend and resume sync-mouse-enter logic.
					//
				finally
				{
					if ( null != control && control.Created 
						&& layout.UIElement.InternalIsSyncMouseEnterSuspended )
					{
						layout.UIElement.InternalResumeSyncMouseEnter( );
						if ( shouldUpdate )
							control.Update( );
					}
				}

				if ( holdFirstRow != this.firstRow )
				{
					if ( null != layout.Grid )
					{
						// fire the after event
						//
						layout.Grid.FireAfterRowRegionScroll( new RowScrollRegionEventArgs( this ) );
					}
				}

				return true;
			}
			finally
			{
				// SSP 4/12/04
				// We need to clear the visible rows of the clone rsr so that the visible rows of 
				// the rsr unhook from the rows' SubObjectPropChange and Disposed events.
				//
				if ( null != clone && null != clone.visibleRows && this.visibleRows != clone.visibleRows )
					clone.visibleRows.InternalClear( );
			}
		}
		internal RowScrollRegion Clone( )
		{
			//RowScrollRegion clone = new RowScrollRegion((RowScrollRegionsCollection)this.collection);
			RowScrollRegion clone = (RowScrollRegion)this.MemberwiseClone();

			if ( clone == null )
				return null;

			clone.clonedFromRegion = this;

			// SSP 8/23/02 UWG1603
			// Set the visibleRows to null. The whole idea behind cloning is that we
			// can fire BeforeRowRegionScroll with two different RowScrollRegions with
			// different VisibleRowsCollections. So after calling MemberWiseClone, 
			// visibleRows member will refere to the same visible rows collection and
			// when the event is fired, both row scroll regions will have the same 
			// visible rows collection defeating the whole purpose of firing the
			// the event. So set the visibleRows to null on the clone so that it gets
			// recreated when the VisibleRows property is accessed.
			//
			clone.visibleRows = null;


			if ( clone.VisibleRows == null )
				return null;

			clone.extentLastResize = this.extentLastResize;
			
			// SSP 10/25/01
			// Implemented
			//m_nScrollOffset          = m_nScrollOffset;
			clone.scrollOffset		   = this.ScrollOffset;

			clone.lastScrollDelta = this.lastScrollDelta;
			clone.lastScrollType = this.lastScrollType;
            clone.tagValue = this.Tag;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.CopyState( clone, this );
			RowScrollRegion.CopyState( clone, this );

			clone.SetFirstRow(this.firstRow);
			return clone;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void CopyState ( RowScrollRegion target, RowScrollRegion source )
		internal static void CopyState( RowScrollRegion target, RowScrollRegion source )
		{
			
			target.Extent = source.Extent;
			target.Origin = source.Origin;
			target.Scrollbar = source.Scrollbar;
			target.SizingMode = source.SizingMode;
			target.Hidden = source.Hidden;
		}



		/// <summary>
		/// Returns the scroll position of the first row
		/// </summary>
		/// <returns></returns>
		int GetScrollPosition( )
		{
			if ( null == this.firstRow )
				return 0;

			// SSP 2/13/04
			// Commented below code out. This optimization is useless.
			//
			
			
			return this.firstRow.ScrollPosition;
		}
		
		#region GetMinScrollPosition

		// SSP 5/1/05 - NAS 5.2 Fixed Rows_
		// 
		internal int GetMinScrollPosition( )
		{
			UltraGridRow[] topFixedRows = this.Layout.Rows.GetFixedRows( true );
			return null == topFixedRows ? 0 : topFixedRows.Length;
		}

		#endregion // GetMinScrollPosition

		// SSP 11/24/03 - Scrolling Till Last Row Visible
		// Added GetMaxScrollPosition method.
		//
		#region GetMaxScrollPosition

		private int cachedMaxScrollPosition = -1;
		private int verifiedMaxScrollPositionVersion = -1;
		internal bool inGetMaxScrollPosition = false;
		
		internal int GetMaxScrollPosition( )
		{
			return this.GetMaxScrollPosition( false );
		}
		
		private static int tmp = 0;
		internal int GetMaxScrollPosition( bool scrollToFill )
		{
			UltraGridLayout layout = this.Layout;
			Debug.Assert( null != layout, "No layout !" );
			// SSP 5/12/05 - NAS 5.2 Fixed Rows
			// Added ScrollBoundsResolved. If there are fixed rows at the bottom then
			// always resolve the scroll bounds to ScrollToFill. Added ScrollBoundsResolved
			// property.
			//
			//if ( null == layout || ScrollBounds.ScrollToLastItem == layout.ScrollBounds )
			if ( null == layout || ScrollBounds.ScrollToLastItem == layout.ScrollBoundsResolved )
			{
				// SSP 12/11/03 UWG2780
				// In horizontal view style, if the last visible row is the only child row
				// of its parent row, then the parent row will always have to be visible. 
				// Remember in horizontal view style the parent row and its first child row
				// occupy the same logical scroll position. (However internally we don't
				// calculate our scroll positions that way). So we need to substract one
				// in such a situation so we use the proper scrollbar max value otherwise
				// the scrollbar will not go all the way down when the last visible row is
				// the only child row of its parent row. See UWG2780 test project after
				// commenting this code out to see what I mean.
				// Commented out the original line of code and added new code below.
				//
				// ----------------------------------------------------------------------------------------
				//return null != layout ? layout.RowScrollRange - 1 : 0;
				int maxScrollPosition = 0;
				if ( null != layout )
				{
					maxScrollPosition = layout.RowScrollRange - 1;

					// If the view style is horizontal, then we need to adjust the max scroll position
					// depending on what can be the last visible row.
					//
					if ( layout.ViewStyleImpl.HasMultiRowTiers )
					{
						UltraGridRow lastVisRow = maxScrollPosition >= 0 
							? layout.GetRowAtScrollPos( maxScrollPosition ) : null;
						while ( maxScrollPosition > 0 && null != lastVisRow 
							&& null != lastVisRow.ParentCollection 
							&& 1 == lastVisRow.ParentCollection.VisibleRowCount  )
						{
							maxScrollPosition--;
							lastVisRow = lastVisRow.ParentRow;
						}
					}
				}

				return maxScrollPosition;
				// ----------------------------------------------------------------------------------------
			}

			// SSP 12/2/05 BR07897
			// 
			//if ( this.cachedMaxScrollPosition < 0 || verifiedMaxScrollPositionVersion != layout.GrandVerifyVersion )
			if ( this.cachedMaxScrollPosition < 0 || verifiedMaxScrollPositionVersion != layout.ScrollToFillVersion )
			{
				Debug.WriteLine( ++tmp + ": Processing GetMaxScrollPosition ********" );

				// We should not get called recursively. However if we do, bail out.
				//
				if ( this.inGetMaxScrollPosition )
				{
					Debug.WriteLine( "This method should not have been called recursively !" );
					return layout.RowScrollRange - 1;
				}

				// We need to set the destroyVisibleRows to false so that below logic doesn't trigger
				// RowScrollRegion.RegenerateVisibleRows method.
				//
				bool origDontUpdateInOnScroll = this.dontUpdateInOnScroll;
				bool origDestroyVisibleRows = this.destroyVisibleRows;

				// SSP 4/24/05 - NAS 5.2 Filter Row
				// Moved this to after we set the flags below.
				//
				//UltraGridRow origFirstRow = this.FirstRow;
				
				this.dontUpdateInOnScroll = true;
				this.destroyVisibleRows = false;				
				this.inGetMaxScrollPosition = true;

				// SSP 4/24/05 - NAS 5.2 Filter Row
				// Moved this from before we set above flags.
				//
				UltraGridRow origFirstRow = this.FirstRow;

				try
				{
					int scrollableRowCount = layout.RowScrollRange;
					if ( scrollableRowCount <= 1 )
					{
						// If none or single rows are visible than the max scroll position should be 0.
						// Row scroll region can't scroll if there are no or single rows visible.
						//
						this.cachedMaxScrollPosition = Math.Max( 0, scrollableRowCount - 1 );

						// SSP 12/2/05 BR07897
						// 
						//this.verifiedMaxScrollPositionVersion = layout.GrandVerifyVersion;
						this.verifiedMaxScrollPositionVersion = layout.ScrollToFillVersion;
					}
					else
					{
						// Below logic calls IsLastRowVisible to find out if the last row is fully visible.
						// However we need to store whether the horizontal scroll bar so that we can tell
						// IsLastRowVisible whether to substract horizontal scrollbar height from the rsr
						// extent.
						//
						ScrollbarVisibility colScrollbarVisibility = 
							// SSP 11/4/04 UWG3593
							// Changed the parameter for assumeColScrollbarsVisible from boolean to an enum
							// that specifies whether to assume and if so to whether to assume the scrollbar
							// is hidden or visible. This is to fix UWG3593 where the scrollbars are displayed 
							// even when they are not needed because we assume the horizontal scrollbar is 
							// visible to find out if vertical scrollbar should be displayed or not.
							//
							//layout.ColScrollRegions.AreScrollbarsVisible( true )
							layout.ColScrollRegions.AreScrollbarsVisible( ScrollbarVisibility.Visible )
							? ScrollbarVisibility.Visible : ScrollbarVisibility.Hidden;

						// Find out the last row from bottom that will optimally fill the row scroll region
						// (when it's made the first visible row in the rsr).
						//
						UltraGridRow lastValidRow = null;

						// SSP 5/12/05 - NAS 5.2 Fixed Rows
						//
						bool forceLastValidRowToBeNewFirstRow = false;

						// SSP 4/16/04 - Virtual Binding
						// In virtual mode where rows are being created as they are requested for,
						// the scroll count could change (as a result of GetRowAtScrollPos call below),
						// in which case we need to compensate for that.
						// Commented out the original code and added new code below.
						//
						//for ( int scrollPosition = scrollableRowCount - 1; scrollPosition >= 0; scrollPosition-- )
						//{
						//	UltraGridRow row = layout.GetRowAtScrollPos( scrollPosition );
						for ( int delta = 1; delta <= layout.RowScrollRange; delta++ )
						{
							UltraGridRow row = layout.GetRowAtScrollPos( layout.RowScrollRange - delta );

							// In virtual mode, row can be null if one or more rows get hidden when they
							// are created as a result of GetRowAtScrollPos which would also cause the
							// scroll count to change.
							//
							//Debug.Assert( null != row );
							if ( null != row )
							{
								// SSP 4/30/05 - NAS 5.2 Fixed Rows_
								// Skip all the bottom fixed rows. Enclosed the existing code into
								// the if block.
								//
								if ( FixedRowStyle.Bottom != row.FixedResolvedLocation )
								{
									this.firstRow = row;
									layout.ViewStyleImpl.RecreateRowList( this );

									// SSP 4/28/05 - NAS 5.2 Fixed Rows
									// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
									// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
									//
									//if ( ! this.IsLastRowVisible( colScrollbarVisibility ) )
									if ( ! this.IsLastScrollableRowVisible( colScrollbarVisibility ) )
										break;

									lastValidRow = this.firstRow;
								}

								// SSP 5/12/05 - NAS 5.2 Fixed Rows
								// If the current FirstRow is as such that it leaves empty space in 
								// the scroll region then make the last valid row the first row. This
								// will keep the first row in view, it will just fill the region. 
								// ForceLastValidRowToBeNewFirstRow is being used below outside of 
								// this for loop.
								//
								if ( origFirstRow == row )
									forceLastValidRowToBeNewFirstRow = true;
							}
							else
							{
								break;
							}
						}

						// SSP 4/16/04 - Virtual Binding
						// In virtual mode where rows are being created as they are requested for,
						// the scroll count could change (as a result of filter evaluation or row being
						// hidden), so reget the scroll count.
						//
						scrollableRowCount = layout.RowScrollRange;						

						// The max value of the scrollbar should be the scroll position associated with the
						// last row from bottom that will optimally fill the row scroll region (when it's made
						// the first visible row in the rsr).
						//
						this.cachedMaxScrollPosition = null != lastValidRow ? lastValidRow.ScrollPosition : scrollableRowCount - 1;

						// Reset the firstRow member var to the original value and recreate the visible rows
						// collection starting from that row. We need to leave the first row to what it was
						// before and make sure that the visible rows collection is in sync with the first
						// visible row.
						// 
						this.firstRow = origFirstRow;
						layout.ViewStyleImpl.RecreateRowList( this );

						// If scrollToFill parameter is true that means we should make sure that the row scroll region
						// is not partially filled.
						//
						// SSP 5/12/05 - NAS 5.2 Fixed Rows
						// When there are fixed rows IsLastRowVisible can't be used. Instead
						// make use of the fact that if the first row causes there to be empty
						// space after the last row then reset the FirstRow to the lastValidRow.
						// 
						//if ( scrollToFill && null != lastValidRow && this.IsLastRowVisible( colScrollbarVisibility ) )
						if ( scrollToFill && null != lastValidRow && forceLastValidRowToBeNewFirstRow )
						{
							// SSP 7/21/05 BR05006
							// Fire BeforeRowRegionScroll since the first row is changing.
							// 
							// --------------------------------------------------------------------
							//this.firstRow = origFirstRow = lastValidRow;
							if ( origFirstRow != lastValidRow )
							{
								origFirstRow = lastValidRow;
								this.SetFirstRow( lastValidRow, true, true );
							}

							this.firstRow = lastValidRow;
							// --------------------------------------------------------------------

							// SSP 5/26/05 BR04275 - NAS 5.2 Fixed Rows
							// Make sure the afterSortScrollPositionToMaintain is not greater than max scroll position
							// otherwise in the get of the FirstRow we will end up assigning the FirstRow that's beyond
							// the max scroll position.
							// 
							if ( this.afterSortScrollPositionToMaintain > this.cachedMaxScrollPosition )
								this.afterSortScrollPositionToMaintain = this.cachedMaxScrollPosition;

							layout.ViewStyleImpl.RecreateRowList( this );
						}
					}

					// SSP 12/2/05 BR07897
					// 
					//this.verifiedMaxScrollPositionVersion = layout.GrandVerifyVersion;
					this.verifiedMaxScrollPositionVersion = layout.ScrollToFillVersion;
				}
				finally
				{
					this.inGetMaxScrollPosition = false;
					this.dontUpdateInOnScroll = origDontUpdateInOnScroll;
					this.destroyVisibleRows = origDestroyVisibleRows;
					this.firstRow = origFirstRow;
				}
			}

			return this.cachedMaxScrollPosition;
		}

		internal void EnsureScrollRegionFilled( )
		{
			this.EnsureScrollRegionFilled( false );
		}
		
		internal void EnsureScrollRegionFilled( bool calledFromRegenerateVisibleRows )
		{
			// SSP 5/12/05 - NAS 5.2 Fixed Rows
			// Added ScrollBoundsResolved. If there are fixed rows at the bottom then
			// always resolve the scroll bounds to ScrollToFill. Added ScrollBoundsResolved
			// property.
			//
			//if ( null != this.Layout && ScrollBounds.ScrollToFill == this.Layout.ScrollBounds )
			if ( null != this.Layout && ScrollBounds.ScrollToFill == this.Layout.ScrollBoundsResolved )
			{
				if ( ! calledFromRegenerateVisibleRows )
					this.verifiedMaxScrollPositionVersion--;

				this.GetMaxScrollPosition( true );
			}
		}

		#endregion // GetMaxScrollPosition

		/// <summary>
		/// Sets the scollinfo of the scrollbar to match its new size and
		/// the region's extent
		/// </summary>
		protected override void ResetScrollInfo()
		{
		
			if ( this.IsClone )
			{				
				((RowScrollRegion)this.ClonedFrom).ResetScrollInfo();
				return;
			}

			// JJD 10/29/01
			// Only reset the scroll info if we aren't inside a scroll
			// notification already
			//
			// JJD 11/07/01
			// The following code was put in for optimization reasons
			// but it caused the thumb not to be position properly
			// after a page down or page up operation. Comented it out.
			//
//			if ( this.InOnScroll )
//				return;

			
			// AS 1/8/03 - fxcop
			// Cannot have 2 public/protected members that differ only by case
			//if ( null == this.scrollBarInfo )
			if ( null == this.scrollBarInfoValue )
				return;

			// SSP 10/26/04 UWG3436
			// If the thumb is being dragged then don't set the Value of the scrollbar based on
			// the first visible row because in the Deferred scroll mode the first row would be
			// the original first row and not the one that's associated with the current thumb
			// position of the scrollbar.
			//
			if ( this.scrollBarInfoValue.IsThumbInDrag )
				return;

			// If we don't have any bands then we won't be showing the scrollbars
			//
			UltraGridLayout layout = this.Layout;
			if ( layout.SortedBands.Count < 1 )
				return;

			// Changed nEstimatedRowsVisible, nScrollPosition and nScrollRange
			// to signed longs so we don't get into huge positive numbers that
			// are really negative
			//
			int scrollPosition;
			
			// SSP 11/24/03 - Scrolling Till Last Row Visible
			// Use the new GetMaxScrollPosition method.
			//
			//int scrollRange     = this.Layout.RowScrollRange - 1;
			int scrollRange     = this.GetMaxScrollPosition( );

			// SSP 4/23/04 UWG3211
			// Both the RowHeightResolved and GetMinRowHeight can be 0 in row-layout mode if
			// there are no columns in which case use the default font height as the estimated 
			// row height.
			//
			//int estimatedRowsVisible = (int)(((double)this.Extent / Math.Max( this.Layout.BandZero.RowHeightResolved, this.Layout.BandZero.GetMinRowHeight( ) ) ) * ( 2.0 / 3) );
			int rowHeight = Math.Max( layout.BandZero.RowHeightResolved, layout.BandZero.GetMinRowHeight( ) );
			if ( rowHeight <= 0 )
				rowHeight = layout.DefaultFontHeight;

			int estimatedRowsVisible = (int)(((double)this.Extent / rowHeight ) * ( 2.0 / 3) );

			// SSP 7/14/05
			// Make sure that the LargeChange on the scrollbar is at least 1. EstimatedRowsVisible can be
			// 0 if the row height is bigger than the extent of the rsr. Setting the LargeChange to 0
			// causes the page up and down via the scrollbar not work.
			// 
			estimatedRowsVisible = Math.Max( 1, estimatedRowsVisible );

			scrollPosition  = this.GetScrollPosition();

			// SSP 5/14/07 BR22379 BR22282
			// Commented out below code. We don't need this code at all. In order to make sure the
			// thumb is sized correctly so that it's size is proportional to the number of rows that
			// can be scrolled into view, we need the scroll range and large change values to be 
			// proportional as well. See the bug for more info.
			// 
			// --------------------------------------------------------------------------------------
			
			// --------------------------------------------------------------------------------------

			//RobA UWG252 9/26/01
			//Add the estimated rows so that the region thinks there are more
			//rows so that we can scroll until there is only 1 row
			//
			scrollRange += estimatedRowsVisible - 1;

			this.scrollbarFactor = 1;

			// SSP 5/1/05 - NAS 5.2 Fixed Rows_
			// Use the GetMinScrollPosition method which takes into account the fixed rows.
			//
			//int scrollInfoMin  = 0;
			int scrollInfoMin  = this.GetMinScrollPosition( );

			int scrollInfoMax  = scrollRange;
			int scrollInfoPage = estimatedRowsVisible;
			int scrollInfoPos  = scrollPosition;

			// Disable the scrollbar if the grid is disabled
			//
			// AS - 12/14/01
			//if ( !this.Layout.Grid.Enabled && this.ScrollBarInfo.Created )
			if ( !layout.Grid.Enabled  )
			{
				// AS - 12/14/01
				// No longer needed since we are using uielements for the scrollbar.
				// When the scrollbar uielement is disable, the scrollbar will be
				// as well. Besides, shouldn't this have been false?
				//
				//this.ScrollBarInfo.Enabled = true;
			}
			else
			{
				bool enable;
				
				// Even if we are showing the scrollbar will have to
				// disable it if all rows are already visible
				//
				if ( this.VisibleRows.Count < 1 )
					enable = false;
				else
					// SSP 4/28/05 - NAS 5.2 Fixed Rows
					// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
					// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
					// Also use IsFirstScrollableRowVisible instead of IsAbsoluteFirstRow.
					//
					// ------------------------------------------------------------------------
					
					enable = ! this.IsFirstScrollableRowVisible( ) || ! this.IsLastScrollableRowVisible( );
				// ------------------------------------------------------------------------

				if ( enable )
				{					
					try
					{
						// SSP 1/7/04
						// Make sure the range is correct.
						//
						// ------------------------------------------------------------------
						scrollInfoMin = Math.Max( 0, scrollInfoMin );
						scrollInfoMax = Math.Max( scrollInfoMin, scrollInfoMax );
						// ------------------------------------------------------------------

						this.ScrollBarInfo.Minimum = scrollInfoMin;
						this.ScrollBarInfo.Maximum = scrollInfoMax;
						this.ScrollBarInfo.LargeChange = scrollInfoPage;					
						this.ScrollBarInfo.Value = Math.Max( scrollInfoMin, Math.Min( scrollInfoMax, scrollInfoPos ) );										
					}
					catch ( Exception )
					{				
						
					}
				}
				// AS - 12/14/01
				// SSP 10/26/04 UWG3726 UWG3057
				// Uncommented the following line out. We do want to set the disabled to true
				// if all the rows are visible.
				//
				this.ScrollBarInfo.Enabled = enable;
			}
		}

		// SSP 4/28/05 - NAS 5.2 Fixed Rows
		// Added an overload of IsFirstScrollableRowVisible that takes in colScrollbarVisibility.
		//
		internal bool IsFirstScrollableRowVisible( )
		{
			return this.IsFirstScrollableRowVisible( ScrollbarVisibility.Check );
		}

		// SSP 4/28/05 - NAS 5.2 Fixed Rows
		// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
		// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
		//
		//internal bool IsFirstRowVisible( )
		internal bool IsFirstScrollableRowVisible( ScrollbarVisibility colScrollbarVisibility )
		{
			// SSP 4/28/05 - NAS 5.2 Fixed Rows_
			// 
			// ----------------------------------------------------------------------------
			UltraGridRow firstScrollableRow = this.Layout.Rows.GetFirstScrollableRow( );
			
			// If there are no scrollable rows then return true since there is nothing to
			// scroll into view.
			//
			if ( null == firstScrollableRow )
				return true;

			VisibleRow vr = this.VisibleRows.GetAssociatedVisibleRow( firstScrollableRow );
			return null != vr && this.IsRowFullyVisible( colScrollbarVisibility, vr );

            
			// ----------------------------------------------------------------------------
		}

		// SSP 4/28/05 - NAS 5.2 Fixed Rows
		// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
		// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
		//
		//internal bool IsLastRowVisible( )
		internal bool IsLastScrollableRowVisible( )
		{
			return this.IsLastScrollableRowVisible( false );
		}

		// SSP 4/28/05 - NAS 5.2 Fixed Rows
		// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
		// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
		//
		//internal bool IsLastRowVisible( bool assumeColScrollbarsVisible )
		internal bool IsLastScrollableRowVisible( bool assumeColScrollbarsVisible )
		{
			return this.IsLastScrollableRowVisible( 
				assumeColScrollbarsVisible ? ScrollbarVisibility.Visible : ScrollbarVisibility.Check );
		}

		// SSP 12/4/03 - Scrolling Till Last Row Visible
		// Added a way to specify whether the scrollbar is hidden by changing from bool to
		// an enum with three members.
		//
		//internal bool IsLastRowVisible( bool assumeColScrollbarsVisible )
		// SSP 4/28/05 - NAS 5.2 Fixed Rows
		// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
		// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
		//
		//internal bool IsLastRowVisible( ScrollbarVisibility colScrollbarVisibility )
		internal bool IsLastScrollableRowVisible( ScrollbarVisibility colScrollbarVisibility )
		{			
			// SSP 4/28/05 - NAS 5.2 Fixed Rows_
			// 
			// --------------------------------------------------------------------------------------------
			// SSP 8/2/06 BR14598
			// If the last scrollable row hasn't been allocated yet, then that means it hasn't been
			// made visible yet (otherwise it would have been allocated). The idea is to not force
			// allocation of the last visible row.
			// 
			if ( this.Layout.Rows.IsLastScrollableRowNotAllocatedYet )
				return false;

			UltraGridRow lastScrollableRow = this.Layout.Rows.GetLastScrollableRow( );

			// If there are no scrollable rows then return true since there is nothing to
			// scroll into view.
			//
			if ( null == lastScrollableRow )
				return true;

			VisibleRow vr = this.VisibleRows.GetAssociatedVisibleRow( lastScrollableRow );
			return null != vr && this.IsRowFullyVisible( colScrollbarVisibility, vr );

			
			// --------------------------------------------------------------------------------------------
		}

		#region IsRowFullyVisible

		// SSP 4/28/05 - NAS 5.2 Fixed Rows_
		// Added IsRowFullyVisible method.
		//

		internal bool IsRowFullyVisible( VisibleRow visibleRow )
		{
			return this.IsRowFullyVisible( ScrollbarVisibility.Check, visibleRow );
		}

		internal bool IsRowFullyVisible( ScrollbarVisibility colScrollbarVisibility, VisibleRow visibleRow )
		{			
			// get the last row in the list
			//
			int regionBottom       = this.Extent;

			// SSP 7/22/05 BR05030
			// Noticed while debugging BR05030. GetTopOfRow does not include the headers
			// however GetTotalHeight does. Use GetTop instead which includes everything,
			// including headers.
			// 
			//int rowBottom = visibleRow.GetTopOfRow( ) + visibleRow.GetTotalHeight( );
			int rowBottom = visibleRow.GetTop( ) + visibleRow.GetTotalHeight( );

			// If the only visible row is the specified visible row then always return true
			// since there is nothing we can do to scroll it into view. This is what the
			// IsLastRowVisible method does.
			//
			if ( 1 == this.VisibleRows.Count && visibleRow == this.VisibleRows[ 0 ] )
				return true;

			// SSP 4/29/05 - NAS 5.2 Fixed Rows_
			// Added IsFullyVisibleOverride on the VisibleRow.
			//
			if ( DefaultableBoolean.Default != visibleRow.IsFullyVisibleOverride )
				return DefaultableBoolean.True == visibleRow.IsFullyVisibleOverride;

			if ( regionBottom < rowBottom )
				return false;
				
			if ( this.IsLastVisibleRegion )
			{
				// Only take into account the width of the vertical scrollbar 
				// if there are any visible
				//
				// SSP 12/4/03 - Scrolling Till Last Row Visible
				// Added a way to specify whether the scrollbar is hidden.
				//
				//if ( assumeColScrollbarsVisible || 
				//	this.Layout.GetColScrollRegions( false ).AreScrollbarsVisible( true ) )
				bool isScrollbarVisible;
				if ( ScrollbarVisibility.Visible == colScrollbarVisibility )
					isScrollbarVisible = true;
				else if ( ScrollbarVisibility.Hidden == colScrollbarVisibility )
					isScrollbarVisible = false;					
				else // if ScrollbarVisibility.Check == colScrollbarVisibility
				{
					// SSP 11/4/04 UWG3593
					// To find out if the horizontal scrollbars are visible don't assume the vertical
					// scrollbars are visible.
					//
					// --------------------------------------------------------------------------------------
					//isScrollbarVisible = this.Layout.GetColScrollRegions( false ).AreScrollbarsVisible( true );
					ColScrollRegionsCollection csrColl = this.Layout.GetColScrollRegions( false );
					isScrollbarVisible = csrColl.AreScrollbarsVisible( ScrollbarVisibility.Check );
					// --------------------------------------------------------------------------------------
				}

				if ( isScrollbarVisible )
				{
					regionBottom -= SystemInformation.HorizontalScrollBarHeight;

					if ( regionBottom < rowBottom )
						return false;
				}
			}

			return true;
		}

		#endregion // IsRowFullyVisible

		bool InternalScrollToNewFirstRow( bool invalidate )
		{
			if ( !this.IsClone)
			{
				Debug.Fail( "ScrollToNewFirstRow called on base RowScrollRegion");
				return false;
			}

			// SSP 4/23/03 - Optimizations.
			//
			bool dirtyVisibleRows = false;
    
			// if the first row changed then fire the before change event
			//
			if ( this.ClonedFrom.firstRow != this.firstRow )
			{
				// SSP 4/23/03 - Optimizations.
				// Set the flag to true so we mark visible rows as dirty.
				//
				dirtyVisibleRows = true;

				// get a reference to the grid
				//
				UltraGridBase grid = this.Layout.Grid;

				if ( null != grid )
				{
					// fire the before event
					//
					BeforeRowRegionScrollEventArgs e = new BeforeRowRegionScrollEventArgs( this, this.ClonedFrom );
					
					grid.FireBeforeRowRegionScroll( e );
					
					bool cancel = e.Cancel;
					if ( cancel )
						return false;
				}
			}

			// update the cloned from region's state 
			//
			this.ClonedFrom.scrollOffset      = this.scrollOffset;
			this.ClonedFrom.lastScrollDelta   = this.lastScrollDelta;
			this.ClonedFrom.lastScrollType    = this.lastScrollType;
			
			// SSP 1/7/03 UWG1923
			// We should be copying from the clone to the cloned-from (this is the clone).
			// and not the other way around.
			//
			// ---------------------------------------------------------------------------
			
			if ( null != this.visibleRows )
				this.ClonedFrom.VisibleRows.CopyRowsFromHelper( this.VisibleRows );
			else
				this.ClonedFrom.VisibleRows.InternalClear( );
			// ---------------------------------------------------------------------------

			this.ClonedFrom.SetFirstRow( this.firstRow, invalidate );

			// In case something was changed in the BeforeRowScrollRegion event
			// above marh the visible rows collection as dirty
			// 
			// SSP 4/23/03 - Optimizations.
			// Only mark the visible rows as dirty which also marks the grid elem dirty
			// if something changed.
			//
			if ( dirtyVisibleRows )
				this.ClonedFrom.SetDestroyVisibleRows( true );
			// SSP 11/8/04 UWG2924 BR01557 BR01542
			// If the old and new first row are the same however there is a scroll delta 
			// (in other words we scrolled by a fixed pixel amount which we only do in
			// in single band that caused the old and the new first row to remain the same)
			// then we still need to dirty the data area element so all the row elements
			// get repositioned.
			//
			else if ( 0 != this.lastScrollDelta && null != this.Layout )
				this.Layout.DirtyDataAreaElement( false, false );

			return true;
		}

		internal void SetFirstRow( UltraGridRow newFirstRow )
		{
			this.SetFirstRow( newFirstRow, true );
		}

		internal void SetFirstRow( UltraGridRow newFirstRow, bool invalidate )
		{
			this.SetFirstRow( newFirstRow, invalidate, false );
		}

		internal void SetFirstRow( UltraGridRow newFirstRow, bool invalidate, bool fireScrollEvents )
		{
			if ( this.firstRow == newFirstRow )
				return;

			// SSP 1/7/05 BR01428
			// Added and overload that takes in the new fireScrollEvents paramter.
			//
			// ------------------------------------------------------------------------------
			if ( fireScrollEvents )
			{
				RowScrollRegion cloneRSR = this.IsClone ? this : this.Clone( );
				if ( null != cloneRSR )
				{
					cloneRSR.firstRow = newFirstRow;
					bool scrolled = cloneRSR.InternalScrollToNewFirstRow( invalidate );

					// SSP 3/21/07 BR20049
					// Raise AfterRowRegionScroll as well since InternalScrollToNewFirstRow
					// only raises the BeforeRowRegionScroll.
					// 
					if (scrolled)
					{
						UltraGridBase grid = this.Layout.Grid;
						if ( null != grid )
							grid.FireAfterRowRegionScroll(new RowScrollRegionEventArgs(this));
					}

					return;
				}
			}
			// ------------------------------------------------------------------------------

			// Do not dirty grid element if we are the cloned rowscrollregion
			//
			if ( invalidate && !this.IsClone )
				this.Layout.DirtyGridElement( false );

			// update our member variable
			//
			this.firstRow = newFirstRow;
		}								  


		internal ScrollEventType LastScrollType 
		{
			get
			{
				return this.lastScrollType;
			}
			set
			{
				this.lastScrollType = value;
			}
		}

		

		internal int ScrollOffset
		{
			get
			{
				return this.scrollOffset;
			}
			set
			{
				this.scrollOffset = value;
			}
		}

		

		internal void ClearVisibleRows ( ) 
		{
			this.visibleRows.InternalClear();
		}
		internal void PopFrontVisibleRow( )
		{
			VisibleRow  visibleRow = this.VisibleRows[0];

			if ( null != visibleRow )
			{
				this.VisibleRows.InternalRemove( visibleRow );
			}
		}

		
		//RobA UWG175 8/15/01 method not implemented
		internal override void CheckIfSizeChanged()
		{		
	
			UltraGrid grid = this.Layout.Grid as UltraGrid;

			if ( grid == null )
				return;

			//	If we get in here when the AfterColRegionSize event is in progress,
			//	we are in a wacky state, probably because DoEvents was called during the event
			//	If this happens, set this flag, which will be evaluated at the end of this function to determine
			//	whether to dirty metrics so we can paint
			if ( grid.IsEventInProgress( GridEventIds.AfterColRegionSize ) )
			{
				this.eventInterrupted = true;
				return;
			}

			//if hidden don't fire events
			//
			if ( !grid.Visible || this.Hidden || grid.FirstDraw )
				return;

    
			// get the clipped extent (accounts for the situation
			// where the grid is smaller than the region extent )
			//
			int clippedExtent = this.ClippedExtent;

			if ( clippedExtent == this.extentLastResize )
				return;

			// cache the clipped extent so we know when it changes
			//
			this.extentLastResize = clippedExtent;
			

		    // fire the after resize event
			//
			RowScrollRegionEventArgs e = new RowScrollRegionEventArgs( this );
			grid.FireEvent( GridEventIds.AfterRowRegionSize, e );

			if( this.eventInterrupted )
			{
				this.Layout.DirtyGridElement( false, true );				
			}
		}


		internal void RemoveVisibleRow ( VisibleRow visibleRow )
		{
			this.visibleRows.InternalRemove( visibleRow );
		}
		internal void PopBackVisibleRow( )
		{
			VisibleRow  visibleRow = this.visibleRows.LastRow;

			if ( null != visibleRow )
			{
				visibleRows.InternalRemove( visibleRow );
			}
		}
		internal override void PositionScrollbar( bool resetScrollInfo )
		{
			// AS - 12/14/01 UWG862
			// We do not want to update the scroll info, if we are in a drag situation.
			// Additional changes were made to the scrollbar info class and scrollbar
			// uielement to force a repaint before the scroll event is set.
			//
			// AS 1/8/03 - fxcop
			// Cannot have 2 public/protected members that differ only by case
			//if (this.scrollBarInfo != null && this.scrollBarInfo.IsThumbInDrag)
			if (this.scrollBarInfoValue != null && this.scrollBarInfoValue.IsThumbInDrag)
				return;

			// Exit if scrollbar rect hasn't been set
			//
			if ( !this.scrollbarRectSet )
			{
				this.scrollbarPositionDirty = true;
				return;
			}

			if ( this.IsClone )
			{
				this.clonedFromRegion.PositionScrollbar( resetScrollInfo );
				return;
			}
    
			// reset the position dirty flag
			//
			this.scrollbarPositionDirty = false;

			bool show = this.WillScrollbarBeShown() && !this.Hidden;

			

			// If the scrollbar rect is empty it means that the visiblerows
			// list has changed (e.g. as a result of expanding a row) such
			// that a scrollbar is necessary. So dirty our cached rect and
			// dirty the grid element so that everything will be synchronized
			// on the next draw.
			//
			if ( show != ( !this.ScrollbarRect.IsEmpty ) )
			{
				this.scrollbarRectSet         = false;
				this.scrollbarPositionDirty   = true;
				this.Layout.DirtyGridElement();
				show = !show;
			}
			else
			{
				if ( resetScrollInfo )
					this.ResetScrollInfo();
			}
			
			this.ShowScrollbar( show, resetScrollInfo );
		}

		// SSP 4/30/05 - NAS 5.2 Fixed Rows_
		// Added scrollableRowsOnly parameter.
		//
		//internal UltraGridRow GetLastVisibleRow( UltraGridBand band, bool nonGroupByRowsOnly )
		internal UltraGridRow GetLastVisibleRow( UltraGridBand band, bool nonGroupByRowsOnly, bool scrollableRowsOnly )
		{
			// SSP 8/15/02 UWG1575
			// Check for the visibleRows being null. Otherwise it will throw a
			// NullReferenceException below. When there are no visible rows
			// there is no last visible row.
			//
			if ( null == this.visibleRows || this.visibleRows.Count <= 0 )
				return null;

			int bottom = this.GetClientHeight( this.Layout.GetActiveColScrollRegion( false ) ); 

			// traverse list from bottom 
			for ( int i = this.visibleRows.Count - 1; i >= 0; i-- )
			{
				// SSP 4/30/05 - NAS 5.2 Fixed Rows_
				// Use the IsRowFullyVisible method to find out if the row is fully visible 
				// or not.
				//
				VisibleRow vr = this.visibleRows[i];
				UltraGridRow row = vr.Row;
				if ( ! this.IsRowFullyVisible( vr ) )
					continue;

				// see if its really visible and in the specified band

				// RobA UWG826 12/10/01 Changed this from < to <= because 
				// when the drop down called this to find the last visible row,
				// the bottom of the last visible row was == to the bottom of the
				// scroll region
				//
				// MD 8/15/07 - 7.3 Performance
				// Use the cached row so we don't have to call the indexer again
				//if ( this.visibleRows[i].Bottom <= bottom && row.Band == band 
				if ( vr.Bottom <= bottom && row.Band == band 
					// SSP 2/6/04 UWG2950
					// Added overloads of GetFirstVisibleRow and GetLastVisibleRow that take in 
					// onlyNonGroupByRows parameter. The reason for doing this is that when we
					// are pasing down or up on a cell, we want to look for non-group-by rows
					// only since they are the onces who have cells.
					//
					&& ( ! nonGroupByRowsOnly || ! ( row is UltraGridGroupByRow ) )
					// SSP 4/30/05 - NAS 5.2 Fixed Rows_
					// Added scrollableRowsOnly parameter.
					//
					&& ( ! scrollableRowsOnly || null == row || ! row.FixedResolved )
					)
					return row;
			}

			return null;
		}

		internal VisibleRow GetVisibleRow( UltraGridRow row )
		{
			if ( null == this.VisibleRows )
				return null;
			
			// iterate over the elem row collection looking for pivot row 
			//
			for ( int i = 0; i < this.VisibleRows.Count; i++ )
			{
				// on a match return the elem row
				//
				VisibleRow visibleRow = this.VisibleRows[i];
				if ( row == visibleRow.Row )
					return visibleRow;
			}
    
			return null;
		}

		// SSP 4/30/05 - NAS 5.2 Fixed Rows_
		// Commented out the following method. Not being used anymore.
		//
		

		// SSP 4/30/05 - NAS 5.2 Fixed Rows_
		// Added scrollableRowsOnly parameter.
		//
		//internal UltraGridRow GetFirstVisibleRow( UltraGridBand band, bool nonGroupByRowsOnly )
		internal UltraGridRow GetFirstVisibleRow( UltraGridBand band, bool nonGroupByRowsOnly, bool scrollableRowsOnly )
		{
			// SSP 8/15/02 UWG1575
			// Check for the visibleRows being null. Otherwise it will throw a
			// NullReferenceException below. When there are no visible rows
			// there is no first visible row.
			//
			if ( null == this.visibleRows || this.visibleRows.Count <= 0 )
				return null;

			Rectangle rect = new Rectangle();
			this.GetRegionRect( ref rect, this.Layout.GetActiveColScrollRegion( false ) );

			// traverse list from bottom 
			for ( int i = 0; i < this.visibleRows.Count; ++i )
			{
				VisibleRow vr = this.visibleRows[i];

				//*vl 082800 - Must take origin into account (bug ult 1162)
				int topOfRow = vr.GetTopOfRow() + this.Origin;

				// if we're past bottom, bail
				if ( topOfRow >= rect.Bottom )
					return null;

				// SSP 4/30/05 - NAS 5.2 Fixed Rows_
				//
				if ( ! this.IsRowFullyVisible( vr ) )
					continue;

				UltraGridRow row = this.visibleRows[i].Row;

				// see if its really visible and in the specified band
				if ( topOfRow >= rect.Top && row.Band == band
					// SSP 2/6/04 UWG2950
					// Added overloads of GetFirstVisibleRow and GetLastVisibleRow that take in 
					// onlyNonGroupByRows parameter. The reason for doing this is that when we
					// are pasing down or up on a cell, we want to look for non-group-by rows
					// only since they are the onces who have cells.
					//
					&& ( ! nonGroupByRowsOnly || ! ( row is UltraGridGroupByRow ) )
					// SSP 4/30/05 - NAS 5.2 Fixed Rows_
					// Added scrollableRowsOnly parameter.
					//
					&& ( ! scrollableRowsOnly || null == row || ! row.FixedResolved )
					)
					return row;
			}
			return null;
		}
		internal void KybdScroll( ScrollEventType scrollType )
		{
			// SSP 2/6/04
			// If the scroll region is displaying all the rows and thus there is no need for a
			// scrollbar, then don't scroll since all the rows will be in view.
			//
			// ----------------------------------------------------------------------------------
			// SSP 5/9/06 BR11466
			// Check the Scrollbars setting.
			// 
			//if ( ! this.WillScrollbarBeShown( ) )
			Scrollbars scrollbars = this.Layout.Scrollbars;
			if ( Scrollbars.Horizontal != scrollbars && Scrollbars.None != scrollbars && ! this.WillScrollbarBeShown( ) )
				return;
			// ----------------------------------------------------------------------------------

			// call OnScroll to scroll the rows
			//
			this.OnScroll( this.ScrollBarInfo, scrollType);

			// reset the m_eLastScrollType so we don't start doing
			// pixel level scrolling
			//
			this.lastScrollType = ScrollEventType.EndScroll; 
		}


		/// <summary>
		/// Returns true if two rows are selection compatible. 
		/// Whether they belong to the same band and if they are at the
		/// same level.
		/// </summary>
		/// <param name="row1"></param>
		/// <param name="row2"></param>
		/// <returns></returns>
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool AreRowsSelectionCompatible( 
		//    Infragistics.Win.UltraWinGrid.UltraGridRow row1,
		//    Infragistics.Win.UltraWinGrid.UltraGridRow row2 )
		internal static bool AreRowsSelectionCompatible(
			Infragistics.Win.UltraWinGrid.UltraGridRow row1,
			Infragistics.Win.UltraWinGrid.UltraGridRow row2 )
		{
			if ( row1.Band != row2.Band )
				return false;

			if ( row1 is UltraGridGroupByRow && !( row2 is UltraGridGroupByRow ) )
				return false;

			if ( row2 is UltraGridGroupByRow && !( row1 is UltraGridGroupByRow ) )
				return false;

			if ( ( row1 is UltraGridGroupByRow ) && ( row2 is UltraGridGroupByRow ) )
			{
				if ( ((UltraGridGroupByRow)row1).Column != ((UltraGridGroupByRow)row2).Column )
					return false;
			}

			// SSP 11/17/03 Add Row Feature
			// We don't allow selection of template rows since they are not real rows in the
			// sense that they don't have any data row associated with them. So return false
			// if any of the two rows is a template add-row.
			// 
			if ( 
				// SSP 4/20/05 - NAS 5.2 Filter Row
				// Use the Selectable property instead.
				//
				//! row1.CanSelectRow || ! row2.CanSelectRow 
				! row1.Selectable || ! row2.Selectable 
				|| row1.IsUnModifiedAddRowFromTemplate || row2.IsUnModifiedAddRowFromTemplate )
				return false;

			return true;
		}
 
		internal bool AreCardsInView( bool nonScrollableCardsOnly )
		{
			if ( this.Hidden )
				return false;

			// JJD 12/19/01
			// Iterate over the visible rows and return true if any 
			// one is a card area
			//
			if ( this.visibleRows != null )
			{
				for ( int i = 0; i < this.visibleRows.Count; i++ )
				{
					if ( this.visibleRows[i].IsCardArea )
					{
						if ( nonScrollableCardsOnly )
						{
							// SSP 8/19/02 UWG1579
							// If the card style is compressed, then return true.
							//
							if ( null != this.visibleRows[i].Row && 
								 this.visibleRows[i].Row.Band.CardView &&
								 this.visibleRows[i].Row.Band.HasCardSettings &&
								// SSP 2/28/03 - Row Layout Functionality
								// Added StyleResolved proeprty because in row layout mode we need to resolve VariableHeight
								// to StandardLabels card style so use the StyleResolved instead of the Style property.
								//
                                 CardStyle.Compressed == this.visibleRows[i].Row.Band.CardSettings.StyleResolved )
								return true;

							int extent = this.Extent;

							if ( ( !this.Layout.AutoFitAllColumns ) &&
								 this.IsLastVisibleRegion )
								extent -= SystemInformation.HorizontalScrollBarHeight;

							// JJD 1/18/02
							// If the existing visible row's height is greater than
							// the height required for 1 card then return true
							//
							if (this.VisibleRows[i].Band.CalculateRequiredCardAreaHeight(1) < this.VisibleRows[i].GetTotalHeight() )
								return true;

							if ( this.visibleRows[i].Bottom < extent )
							{
								continue;
							}
						}

						return true;
					}
				}
			}

			return false;
		}

		internal VisibleRow GetNearestSameBandRow( Point point, GridItemBase LastGridItem )
		{
			//make sure that there are 1 or more visible rows and LastGridItem is valid
			if ( this.VisibleRows.Count < 1 || LastGridItem == null )
				return null;

			//Debug.WriteLine( " Row index = " + this.visibleRows[0].Row.Index.ToString() );

			Rectangle rcRegion = new Rectangle(0,0,0,0);

			this.GetRegionRect( ref rcRegion, this.Layout.Grid.ActiveColScrollRegion );
 
			UltraGridRow pivotRow = null;

			// MD 8/2/07 - 7.3 Performance
			// Cached row - Prevent calling expensive getters multiple times
			UltraGridRow lastGridItemRow = LastGridItem.GetRow();

			// if the lastitem and the pivot item are the same, try to get the privot row
			//
			if ( this.pivotItem != null && this.pivotItem.GetType() == LastGridItem.GetType() )
			{
				pivotRow = this.pivotItem.GetRow();

				// make sure the pivot item is from the same band
				//
				// SSP 10/11/01
				// Use AreRowsSelectionCompatible method to determine if tow rows can
				// be selected at the same time
				//
				//if ( pivotRow != null &&  pivotRow.Band != LastGridItem.Band )
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( pivotRow != null &&  !this.AreRowsSelectionCompatible( pivotRow, LastGridItem.GetRow( ) ) )
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( pivotRow != null && !RowScrollRegion.AreRowsSelectionCompatible( pivotRow, LastGridItem.GetRow() ) )
				if ( pivotRow != null && !RowScrollRegion.AreRowsSelectionCompatible( pivotRow, lastGridItemRow ) )
					pivotRow = null;
			}

			VisibleRow  nearestVisibleRow = null;
			VisibleRow	lastGridItemVisibleRow = null;
			UltraGridRow			row;
			VisibleRow	visibleRow;

			int        pivotRowTop    = 0;
			int        pivotRowBottom = 0;
			int        topOfRow, bottomOfRow;
			bool       pivotRowFound = false;

			// iterate over the elem row collection looking for the pivot row 
			//
			for ( int index = 0; index < this.visibleRows.Count; index++ )
			{
				visibleRow = this.visibleRows[index];

				// SSP 3/27/02
				// Skip the hidden rows.
				//
				if ( null == visibleRow.Row || visibleRow.Row.HiddenResolved )
					continue;

				if ( pivotRow != null )
				{
					if ( visibleRow.Row == pivotRow )
					{
						pivotRowTop        = this.Origin + visibleRow.GetTopOfRow();
						pivotRowBottom     = pivotRowTop + visibleRow.Row.TotalHeight;
						pivotRowFound      = true;
						break;
					}
				}
				else
				{
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( LastGridItem.GetRow() == visibleRow.Row )
					if ( lastGridItemRow == visibleRow.Row )
					{
						lastGridItemVisibleRow = visibleRow;
						break;
					}
				}
			}

			if ( !pivotRowFound )
			{
				if ( this.rowsScrolledDuringDrag > 0 )
				{
					pivotRowTop        = rcRegion.Top;
					pivotRowBottom     = rcRegion.Top;
				}
				if ( this.rowsScrolledDuringDrag < 0 )
				{
					pivotRowTop        = rcRegion.Bottom;
					pivotRowBottom     = rcRegion.Bottom;
				}
			}

			//if the mouse is above the region and the visible row is in the same band
			// then just return the top visible row
			// SSP 10/11/01
			// Use AreRowsSelectionCompatible method to determine if tow rows can
			// be selected at the same time
			//
			//if ( point.Y < rcRegion.Top && LastGridItem.Band == this.visibleRows[0].Band )
			if ( point.Y < rcRegion.Top && 
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.AreRowsSelectionCompatible( LastGridItem.GetRow( ), this.visibleRows[0].Row ) )
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//RowScrollRegion.AreRowsSelectionCompatible( LastGridItem.GetRow(), this.visibleRows[ 0 ].Row ) )
				RowScrollRegion.AreRowsSelectionCompatible( lastGridItemRow, this.visibleRows[ 0 ].Row ) )
				return this.visibleRows[0];

			//if the mouse is below the region and the visible row is in the same band
			// then just return the last visible row
			if ( point.Y > rcRegion.Bottom )
			{	
				int count = this.visibleRows.Count - 1;
				while( count >= 0 )
				{
					VisibleRow vr = this.visibleRows[count];
					if ( vr.Bottom <= rcRegion.Bottom )
					{
						// SSP 10/11/01
						// Use AreRowsSelectionCompatible method to determine if tow rows can
						// be selected at the same time
						//
						//if( LastGridItem.Band != vr.Band || vr.Row.HiddenResolved )
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if( !this.AreRowsSelectionCompatible( LastGridItem.GetRow( ), vr.Row ) || vr.Row.HiddenResolved )
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( !RowScrollRegion.AreRowsSelectionCompatible( LastGridItem.GetRow(), vr.Row ) || vr.Row.HiddenResolved )
						if ( !RowScrollRegion.AreRowsSelectionCompatible( lastGridItemRow, vr.Row ) || vr.Row.HiddenResolved )
							break;
						
						return vr;
					}
					count--;
				}
			}

			// JJD 1/12/02
			// Loop forwards or backwards depending on the relative
			// position of the point and the pivot row
			//
		    bool loopForwards = point.Y <= pivotRowTop || pivotRow == null;

			// iterate over the elem rows collection looking for the nearest 
			// row in the same band
			//
			for ( int i = loopForwards ? 0 : this.visibleRows.Count - 1; 
					i >= 0 && i < this.visibleRows.Count; 
					i += loopForwards ? 1 : -1 )
			{
				visibleRow = this.visibleRows[i];
				row     = visibleRow.Row;

				// bypass hidden items
				//
				if ( row.HiddenResolved )
					continue;

				// bypass rows from other bands
				//
				// SSP 10/11/01
				// Use AreRowsSelectionCompatible method to determine if tow rows can
				// be selected at the same time
				//
				//if ( visibleRow.Band != LastGridItem.Band )
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( !this.AreRowsSelectionCompatible( visibleRow.Row, LastGridItem.GetRow( ) ) )
				// MD 8/2/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( !RowScrollRegion.AreRowsSelectionCompatible( visibleRow.Row, LastGridItem.GetRow() ) )
				if ( !RowScrollRegion.AreRowsSelectionCompatible( visibleRow.Row, lastGridItemRow ) )
					continue;

				topOfRow     = this.Origin + visibleRow.GetTopOfRow();
				bottomOfRow  = topOfRow + row.TotalHeight;

				if ( topOfRow    >=  rcRegion.Bottom ||
					bottomOfRow  <   rcRegion.Top )
					continue;

				if ( point.Y >= topOfRow )
				{
					//Debug.WriteLine( "**Point = " + point.Y.ToString() + "Bottom of row = " + bottomOfRow.ToString() + " Index = " + i.ToString() );
					if ( point.Y <= bottomOfRow )
					{
						//Debug.WriteLine( "nearestVisibleRow assigned: Point = " + point.Y.ToString() + "Top = " + topOfRow.ToString() + "Bottom = " + bottomOfRow.ToString() );
						nearestVisibleRow = visibleRow;
						break;
					}

					// JJd 1/12/02
					// If we are looping backwards and the top of this row
					// is below the bottom of the pivot row set the nearest 
					// row to this row
					//
					if ( pivotRow != null && !loopForwards )
					{
						if ( topOfRow >= pivotRowBottom )
						{
							nearestVisibleRow = visibleRow;
							break;
						}
					}
				}
				else
				{
					// JJd 1/12/02
					// Only set the visible row if we are looping forwards and
					// the top is above the pivot top
					//
					if ( pivotRow != null && loopForwards &&  topOfRow <= pivotRowTop  )
					{
						nearestVisibleRow = visibleRow;
						break;
					}
				}
			}

			return nearestVisibleRow != null ? nearestVisibleRow : lastGridItemVisibleRow;
		}

		internal bool DoesDragNeedScroll( Point mouseLoc, ref int timerInterval )
		{

			Rectangle rcRegion = new Rectangle(0,0,0,0);

			// get the region's rect (intersected with the active col scroll region )
			//
			this.GetRegionRect( ref rcRegion, this.Layout.Grid.ActiveColScrollRegion );

			// if the y coordinate of the cursor falls in the region then return false
			//
			if ( mouseLoc.Y >= rcRegion.Top && mouseLoc.Y <= rcRegion.Bottom )
			{
				//Debug.WriteLine ( " Mouse *Y* = " + mouseLoc.Y.ToString() + " bottom = " + rcRegion.Bottom.ToString() );
				return false;
			}

			// SSP 6/22/05 - NAS 5.3 Column Chooser
			// Don't scroll the grid if the mouse is over the column chooser.
			// 
			UltraGridBase grid = this.Layout.Grid;
			if ( null != grid && ! grid.ShouldScrollGridOnDragMoveHelper( mouseLoc ) )
				return false;

			int distanceOutsideRgn = 0;

			// caclulate the distance from the region's borders to where
			// the cursor is
			//
			if ( mouseLoc.Y < rcRegion.Top )
				distanceOutsideRgn = rcRegion.Top - mouseLoc.Y;
			else if ( rcRegion.Bottom < mouseLoc.Y )
				distanceOutsideRgn = mouseLoc.Y - rcRegion.Bottom;

			// return faster timer intervals for longer distances
			//
			// SSP 8/9/05 BR05436
			// Added entry for 120.
			// 
			if ( distanceOutsideRgn > 120 )
				timerInterval =    1;
			else 
				if ( distanceOutsideRgn > 100 )
				timerInterval =    40;
			else
				if ( distanceOutsideRgn > 80 )
				timerInterval =    65;
			else
				if ( distanceOutsideRgn > 60 )
				timerInterval =    80;
			else
				if ( distanceOutsideRgn > 50 )
				timerInterval =    100;
			else
				if ( distanceOutsideRgn > 40 )
				timerInterval =    150;
			else
				if ( distanceOutsideRgn > 20 )
				timerInterval =    200;
			else
				if ( distanceOutsideRgn > 10 )
				timerInterval =    300;
			else
				if ( distanceOutsideRgn > 6 )
				timerInterval =    400;
			else
				timerInterval =    500;

			// SSP 8/9/05 BR05436
			// 
			// ------------------------------------------------------------------------
			Control displayControl = this.Layout.Grid.ControlForGridDisplay;
			if ( null != displayControl )
			{
				Rectangle regionRectInScreen = displayControl.RectangleToScreen( rcRegion );
				Rectangle deskTopRect = Utilities.ScreenFromRectangle( regionRectInScreen ).Bounds;
				Point mouseLocInScreen = displayControl.PointToScreen( mouseLoc );

				if ( mouseLocInScreen.Y + 1 >= deskTopRect.Bottom 
					|| mouseLocInScreen.Y <= deskTopRect.Y )
				{
					timerInterval = 1;
				}
				else
				{
					int d = mouseLocInScreen.Y <= regionRectInScreen.Y 
						? regionRectInScreen.Y - deskTopRect.Y
						: deskTopRect.Bottom - regionRectInScreen.Bottom;

					if ( d > 0 && d < 120 )
						timerInterval = Math.Max( 1, (int)( timerInterval * d / 120.0 ) );
				}
			}
			// ------------------------------------------------------------------------

			//ROBA UWG152 8/16/01
			// SSP 9/10/01
			// If we are scrolling up, then scroll even if the last row is visible
			// since other rows at the top could be hidden
			// 
			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
			// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
			//
			//if ( mouseLoc.Y > rcRegion.Bottom && this.IsLastRowVisible() )
			if ( mouseLoc.Y > rcRegion.Bottom && this.IsLastScrollableRowVisible() )
				return false;

			// SSP 9/10/01
			// If we are scrolling up and if the first row is already visible, then don't
			// scroll
			// 
			// SSP 4/28/05 - NAS 5.2 Fixed Rows
			// Changed the names of IsFirstRowVisible and IsLastRowVisible to 
			// IsFirstScrollableRowVisible and IsLastScrollableRowVisible.
			//
			//if ( mouseLoc.Y < rcRegion.Top && this.IsFirstRowVisible( ) )
			if ( mouseLoc.Y < rcRegion.Top && this.IsFirstScrollableRowVisible( ) )
				return false;


			return true;
		}

		

		internal void DoDragScroll( Point mouseLoc, int timerInterval )
		{
			Rectangle rcRegion = new Rectangle(0,0,0,0);

			// get the region's rect (intersected with the active col scroll region )
			//
			this.GetRegionRect ( ref rcRegion, this.Layout.Grid.ActiveColScrollRegion );

			ScrollEventType e = new ScrollEventType();

			// determine if we need to scroll up or down
			//
			if ( mouseLoc.Y < rcRegion.Top )
				e = ScrollEventType.SmallDecrement;
			else if ( mouseLoc.Y > rcRegion.Bottom )
				e = ScrollEventType.SmallIncrement;
			else
			{
				//Debug.WriteLine ( "Mouse Y = " + mouseLoc.Y.ToString() + " bottom = " + rcRegion.Bottom.ToString() );
				return;
			}
			// call OnScroll to scroll the rows
			//
			
			bool fScrolled = this.OnScroll( this.ScrollBarInfo, e );

			// reset the lastScrollType so we don't start doing
			// pixel level scrolling
			//
			this.LastScrollType = ScrollEventType.EndScroll;

			if ( fScrolled )
			{
				// keep track of the net rows scrolled up or down during
				// the drag operation. We usee this in GetNearestSameBandRow
				// to determine if the pivot item is logically above or below
				// the region
				//
				if ( e == ScrollEventType.SmallDecrement )
					this.rowsScrolledDuringDrag--;
				else if ( e == ScrollEventType.SmallIncrement )
					this.rowsScrolledDuringDrag++;
			}
		}

		// SSP 4/12/04
		// Dispose of the cloned rsr so that the visible rows of the rsr unhook from the rows'
		// SubObjectPropChange and Disposed events.
		// Overrode OnDispose so we can clear the visible rows collection.
		//
		/// <summary>
		/// Called when the object is disposed of.
		/// </summary>
		protected override void OnDispose( )
		{
			base.OnDispose( );

			// Call InternalClear on the visible rows so they unhook from the rows.
			//
			if ( null != this.visibleRows )
				this.visibleRows.InternalClear( );
		}

		#region type converter

		/// <summary>
		/// RowScrollRegion type converter.
		/// </summary>
		public sealed class RowScrollRegionTypeConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if (destinationType == typeof(InstanceDescriptor)) 
				{
					return true;
				}
				
				return base.CanConvertTo(context, destinationType);
			}

            /// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) 
			{
				if (destinationType == null) 
				{
					throw new ArgumentNullException("destinationType");
				}

				if ( destinationType == typeof(InstanceDescriptor) && 
					 value is RowScrollRegion ) 
				{
					RowScrollRegion item = (RowScrollRegion)value;
					
					ConstructorInfo ctor;

					ctor = typeof(RowScrollRegion).GetConstructor(new Type[] { typeof( int ) } );

					if (ctor != null) 
					{
						//false as the last parameter here causes generation of a local variable for the type 
						return new InstanceDescriptor(ctor, new object[] { item.Extent }, false);
					}
				}

				return base.ConvertTo(context, culture, value, destinationType);
			}
		}
		
		#endregion

	}
}
