#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

	/// <summary>
	///	Represents a column header or a band label inside a group-by box ui element.
	/// </summary>
										  // JAS v5.2 New Default Look & Feel For UltraGrid 4/12/05
	public class GroupByButtonUIElement :  HeaderUIElementBase
	{   
		#region Member Variables

		// AS 1/6/04 accessibility
		private AccessibleObject			accessibleObject;

		private GroupByBox.ButtonDefinition buttonDef;

		private bool dragging = false;
		private bool startDraggig = false;
		private Point lastClickedPoint = Point.Empty;

		#endregion //Member Variables

		internal GroupByButtonUIElement( UIElement parent, GroupByBox.ButtonDefinition buttonDef ) 
			// JAS v5.2 New Default Look & Feel For UltraGrid 4/12/05
			// : base( parent )
			: base( parent, false, false )
		{
			this.Initialize( buttonDef );
		}

		internal void Initialize( GroupByBox.ButtonDefinition buttonDef )
		{
			this.buttonDef = buttonDef;

			this.PrimaryContext = buttonDef.column.Header;
		}

		/// <summary>
		/// Returns or sets a value that determines the style of a button.
		/// </summary>
		public override Infragistics.Win.UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.GroupByBox.ButtonBorderStyleResolved;
			}
		}



		internal GroupByBox GroupByBox
		{
			get
			{
				return (GroupByBox)this.Parent.GetContext( typeof( GroupByBox ), true );
			}
		}

        /// <summary>
        /// Returns an object of requested type that relates to the element or null.
        /// </summary>
        /// <param name="type">The requested type or null to pick up default context object.</param>
        /// <param name="recursive">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns null or an object of requested type that relates to the element.</returns>
        /// <remarks> Classes that override this method normally need to override the <see cref="Infragistics.Win.UIElement.ContinueDescendantSearch(System.Type,System.Object[])"/> method as well.</remarks>
        public override object GetContext(Type type, bool recursive)
		{
			// Context of band is used for the drag strategy to 
			// get the buton or band label for a band 
			//
			if ( typeof( Infragistics.Win.UltraWinGrid.UltraGridBand ) == type )
			{
				if ( null != this.buttonDef.column )
					return this.buttonDef.column.Band; 
				else if ( null != this.buttonDef.band )
					return this.buttonDef.band;		
			}

			// Context of band is used for the drag strategy to 
			// get the buton or band label for a band 
			//
			if ( typeof( Infragistics.Win.UltraWinGrid.HeaderBase ) == type )
			{
				if ( null != this.buttonDef.column )
					return this.buttonDef.column.Header;
			}


			// Context of band is used for the drag strategy to 
			// get the buton or band label for a band 
			//
			if ( typeof( Infragistics.Win.UltraWinGrid.ColumnHeader ) == type )
			{
				if ( null != this.buttonDef.column )
					return this.buttonDef.column.Header;
			}


			return base.GetContext( type, recursive );
		}

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type, object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
		/// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
        protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( null != contexts )
			{
				object primaryContext = this.PrimaryContext;
				Type   primaryContextType = null != primaryContext ? primaryContext.GetType( ) : null;

				UltraGridBand band = null != this.buttonDef.column ? this.buttonDef.column.Band : this.buttonDef.band;

				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						Type contextType = context.GetType( );

						if ( primaryContextType == contextType && context != primaryContext )
							return false;

						if ( null != band && context is UltraGridBand && context != band )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch

		// AS - 10/31/01
		// There were 2 virtual implementations of GetContext in our framework
		// and it only made sense to have one virtual since the non-virtual one
		// simply called off to the virtual one.
		//
//
//		/// <summary>
//		/// Returns an object of requested type that relates to the
//		/// element or null.
//		/// </summary>
//		/// <param name="type"></param>
//		/// <returns></returns>
//		public override object GetContext( Type type )
//		{		
//				
//			// Context of band is used for the drag strategy to 
//			// get the buton or band label for a band 
//			//
//			if ( typeof( Infragistics.Win.UltraWinGrid.Band ) == type )
//			{
//				if ( null != this.buttonDef.column )
//					return this.buttonDef.column.Band; 
//				else if ( null != this.buttonDef.band )
//					return this.buttonDef.band;		
//			}
//
//			// Context of band is used for the drag strategy to 
//			// get the buton or band label for a band 
//			//
//			if ( typeof( Infragistics.Win.UltraWinGrid.HeaderBase ) == type )
//			{
//				if ( null != this.buttonDef.column )
//					return this.buttonDef.column.Header;
//			}
//
//			// Context of band is used for the drag strategy to 
//			// get the buton or band label for a band 
//			//
//			if ( typeof( Infragistics.Win.UltraWinGrid.ColumnHeader ) == type )
//			{
//				if ( null != this.buttonDef.column )
//					return this.buttonDef.column.Header;
//			}
//					
//
//			// Call the base class implementation
//			//			
//			return base.GetContext( type );		
//		}




        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// SSP 2/21/06 BR10188 - HeaderStyle of WindowsXPCommand
			// Honor the colors set on the header appearances.
			// 
			Infragistics.Win.UltraWinGrid.GroupByBox groupByBox = this.GroupByBox;
			groupByBox.ResolveGroupByButtonAppearance( ref appearance, ref requestedProps, buttonDef, false );

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/12/05
			// Give the base class a chance to set HeaderStyle-specific settings.
			//
			base.InitAppearance( ref appearance, ref requestedProps );

			// SSP 10/31/01 UWG605
			// Use the new ResolveGroupByButtonAppearance method to resolve
			// a group by button appearance which is going to take into account
			// group by header appearance as well.
			//
			//this.GroupByBox.ResolveButtonAppearance( ref appearance, requestedProps );
			// SSP 2/21/06 BR10188 - HeaderStyle of WindowsXPCommand
			// Honor the colors set on the header appearances.
			// 
			//this.GroupByBox.ResolveGroupByButtonAppearance( ref appearance, requestedProps, buttonDef );
			groupByBox.ResolveGroupByButtonAppearance( ref appearance, ref requestedProps, buttonDef, true );
			
			//RobA UWG213 8/27/01
			requestedProps &= ~AppearancePropFlags.FontData;			
		}

		internal HeaderBase Header
		{
			get
			{
				Debug.Assert( null != this.buttonDef.column );

				if ( null != this.buttonDef.column )
					return this.buttonDef.column.Header;

				return null;
			}
		}

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements( )
		{
			TextUIElement			textElement				= null;
			SortIndicatorUIElement	sortIndicatorElement	= null;
			ImageUIElement			imageElement			= null;

			if ( this.childElementsCollection != null )
			{
				textElement				= (TextUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof(TextUIElement), true );
				sortIndicatorElement	= (SortIndicatorUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof(SortIndicatorUIElement), true );
				imageElement			= (ImageUIElement)HeaderUIElement.ExtractExistingElement( this.childElementsCollection, typeof(ImageUIElement), true );

				this.childElementsCollection.Clear();
			}

			AppearanceData appData = new AppearanceData();

			// SSP 10/31/01 UWG605
			// Use the new ResolveGroupByButtonAppearance method to resolve
			// a group by button appearance which is going to take into account
			// group by header appearance as well.
			//
			//if ( this.Header != null )
			//	this.Header.ResolveAppearance( ref appData, AppearancePropFlags.AllRender );
			this.GroupByBox.ResolveGroupByButtonAppearance( ref appData,  AppearancePropFlags.AllRender, this.buttonDef );
			
			// Create the text element
			//
			if ( null == textElement)
			{
				textElement = new TextUIElement( this, string.Empty );
			}

			Rectangle textRect = this.RectInsideBorders; //textElement.Rect;
			Rectangle sortIndicatorRect = textRect;
			bool addSortIndicator = false;

            // MRS 8/21/2008 - BR35683
            // Commented out the 'if' statement. 
            // This check for 40 does not make sense here. My guess is that this was carried 
            // over from the HeaderUIElement, where it does make sense. A column header is 
            // sizable, so if it's too small, we don't want to show the sort indicator so 
            // we can show more of the header text. But in a GroupByButton, the button is 
            // not sizable, it is sized to fit it's contents, so there's no reason to 
            // enforce an arbitrary limit here.
            //
            //// make sure we have a reasonable amount of space for the sort indicator
            ////
            //if ( textRect.Width > 40 )
			{
				switch( this.Header.SortIndicator )
				{
					case SortIndicator.Ascending:
					case SortIndicator.Descending:
					{

						addSortIndicator = true;
					
						sortIndicatorRect = textRect;
						sortIndicatorRect.X = textRect.Right - 10;
						sortIndicatorRect.Width = 9;
				
						textRect.Width = sortIndicatorRect.Left - textRect.Left;
						break;
					}
				}
			}

			textRect.Inflate(-1,0);

			Image image = null;
			Size imageSize = Size.Empty;
			this.GroupByBox.CalcDisplayedImageSize( appData, textRect.Height, out image, out imageSize );


			// JJD 08/17/01 - UWG59
			// Added support for displaying images in headers
			//
			if ( image != null )
			{
				if ( null == imageElement )
				{
					imageElement = new ImageUIElement( this, image );
				}
				else
				{
					imageElement.Image = image;
				}

				imageElement.Scaled = true;

				// make sure the width isn't too big
				//
				if ( textRect.Width > 2 && imageSize.Width > textRect.Width - 2 )
					imageSize.Width = textRect.Width - 2;

				Rectangle imgRect	= textRect;
				imgRect.Width		= imageSize.Width;
				imgRect.Height		= imageSize.Height;

				//move image based on alignment
				DrawUtility.AdjustHAlign( appData.ImageHAlign, ref imgRect, textRect ); 
				DrawUtility.AdjustVAlign( appData.ImageVAlign, ref imgRect, textRect );

				// make sure the imgae has at least a 1 pixel 
				// padding on top and bottom
				//
				if ( imgRect.Top == textRect.Top )
					imgRect.Y++;
				else
					if ( imgRect.Bottom == textRect.Bottom )
					imgRect.Y--;


				// adjust the text rect appropriately
				//
				switch ( appData.ImageHAlign )
				{
					case HAlign.Center:
						break;

					case HAlign.Right:
						imgRect.X--;
						textRect.Width -= imgRect.Width + 1;
						break;

					default:
					case HAlign.Left:
						imgRect.X++;
						textRect.Width	-= imgRect.Width + 1;
						textRect.X		+= imgRect.Width + 1;
						break;
				}

				imageElement.Rect = imgRect;

				this.ChildElements.Add( imageElement );
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			// Removed maxLines and headerLines locals and realized the rest of this code doesn't really do anything
			#region Not Used

			//// Width of rcText is now final, so calculate top/bottom
			////
			//int headerLines = this.Header.IsGroup ? this.Header.Band.GroupHeaderLines:
			//    this.Header.Band.ColHeaderLines;

			//Rectangle adjTextRect = textRect;

			//if ( this.Header != null )
			//{

			//    //Pass in the number of ColHeaderLines.
			//    //

			//    int maxLines = 0;

			//    if ( this.Header.Band != null && this.Header.Band.ColHeaderLines > 0 )
			//        maxLines = this.Header.Band.ColHeaderLines;

			//    //CalculateTextSpace ( appData, m_pPositionItem->GetHeader()->GetCaption(), rcAdjText, lMaxLines );

			//    adjTextRect.Intersect( textRect );
			//}

			//int height = textRect.Height;
			//int textHeight = adjTextRect.Height;

			//switch ( appData.TextVAlign )
			//{

			//    case Infragistics.Win.VAlign.Bottom:
			//    {
			//        textRect.Height = textHeight;
			//        textRect.Y = textRect.Bottom - textHeight;
			//        break;
			//    }
			//    case Infragistics.Win.VAlign.Top:
			//    {
			//        textRect.Height = textHeight;
			//        break;
			//    }

			//    default:
			//    case Infragistics.Win.VAlign.Middle:
			//    {
			//        textRect.Y += ( height - textHeight ) / 2;
			//        textRect.Height = (textRect.Top + textHeight) - textRect.Y;
			//        break;
			//    }
			//}

			#endregion Not Used

			// Now that top/bottom of text rect is finalized, 
			// adjust top/bottom of rcSortIndicator and rcSwapButton
			//
			sortIndicatorRect.Y    =  textRect.Top;
			sortIndicatorRect.Height = textRect.Height;

			
			if ( addSortIndicator )
			{
				//-1 since sortindicator was clipping the bottom of the header's border
				//when it was printing
				//
				--sortIndicatorRect.Height;

				if ( sortIndicatorElement == null )
					sortIndicatorElement = new SortIndicatorUIElement(this, this.Header.SortIndicator == SortIndicator.Ascending);
				else
					sortIndicatorElement.InitAscending( this.Header.SortIndicator == SortIndicator.Ascending );
 
				sortIndicatorElement.Rect = sortIndicatorRect;

				this.ChildElements.Add( sortIndicatorElement );
			}
			
		

			textElement.Rect = textRect; // this.RectInsideBorders;

			// SSP 11/25/02 UWG1829
			// Use the buttonDef.Caption which contains the header caption with new line characters
			// replaced with spaces.
			//
			//textElement.Text = this.Header.Caption;
			textElement.Text = this.buttonDef.Caption;

			//ROBA 8/3/01 UWG85 needed to use appData's alignment
			//textElement.TextAlignment = ContentAlignment.MiddleCenter;

			// JJD 11/07/01
			// TextAlignment property has been removed
			//
//			textElement.TextAlignment = Win.AppearanceData.ContentAlignmentFromHVAlign( appData.TextHAlign, appData.TextVAlign );
			this.ChildElements.Add( textElement );
		}

		
		
		// SSP 9/26/01 UWG331
		// took this out move the code in OnMouseUp to check for left
		// mouse button click in OnMouseUp
		//
		

		/// <summary>
		/// Overridden method.
		/// </summary>
		protected override void OnCaptureAborted( )
		{
            // MRS 4/25/2008 - BR32345
            #region Re-factored into a helper method
            //UltraGrid grid = null;

            //// Reset flags 
            //// SSP 10/3/01
            //// we want to set the dragging to false below in the if
            //// statemenet because the condition of the if statement
            //// is checkig for this flag and it will always be false
            //// if we do it here
            ////
            ////this.dragging = false;
            //this.startDraggig = false;

            //if ( null != this.Header && null != this.Header.Band.Layout )
            //    grid = this.Header.Band.Layout.Grid as UltraGrid;

            //if ( this.dragging && null != grid )
            //{
            //    this.dragging = false;

            //    ((ISelectionManager)grid).OnDragEnd( true );
            //    return;
            //}
            #endregion //Re-factored into a helper method
            //
            // I am not sure why we are returning here before calling the base, but I
            // I am keeping it that way so as not to break anything. 
            //
            if (this.EndDragHelper(true))
                return;

            base.OnCaptureAborted();
		}
		

		/// <summary>
		/// Return true if this element wants to be notified when the mouse hovers over it. This property is read-only.
		/// </summary>
		/// <remarks>Returns true since group by buttons need MouseHover notifications for showing the tooltips.</remarks>
		protected override bool WantsMouseHoverNotification
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// overridden method called whenever the mouse hovers on the element
		/// it displays a tooltip for the AddNewRowButton
		/// </summary>
		protected override void OnMouseHover( )
		{	
			UltraGrid grid = this.GroupByBox.Layout.Grid as UltraGrid;

			if ( null == grid )
				return;

            // MBS 9/14/07 - BR26538
            // Respect the TipStyleHeader when determining whether we should show a ToolTip
            if (this.buttonDef.column != null &&
                this.buttonDef.column.Band != null &&
                this.buttonDef.column.Band.TipStyleHeaderResolved == TipStyle.Hide)
                return;
            
			string caption = this.buttonDef.Caption;

			if ( null != caption )
			{
				Infragistics.Win.ToolTip toolTip = grid.TooltipTool;

				if ( null != toolTip )
				{
					toolTip.SetMargin( 1, 1, 1, 1 );
					toolTip.ToolTipText = SR.GetString("GroupByButtonToolTip"); //"Click to toggle sort direction";
					toolTip.Show();
				}
			}
		}


        /// <summary>
        /// Called when the mouse down message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown( MouseEventArgs e, 
										 bool adjustableArea,
										 ref UIElement captureMouseForElement )
		{

			// SSP 9/27/01 UWG365
			// Exit the edit mode before we potentially sort
			// or start a drag operation
			//
			// MD 8/3/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//if ( null != this.Header.Column.Layout.ActiveCell &&
			//    this.Header.Column.Layout.ActiveCell.IsInEditMode )
			//    this.Header.Column.Layout.ActiveCell.ExitEditMode( );
			UltraGridCell activeCell = this.Header.Column.Layout.ActiveCell;

			if ( null != activeCell && activeCell.IsInEditMode )
				activeCell.ExitEditMode();

			this.lastClickedPoint = new Point( e.X, e.Y );

			// Reset flags
			this.startDraggig = false;
			this.dragging = false;

			if ( e.Button == System.Windows.Forms.MouseButtons.Left )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//ColumnHeader columnHeader = this.Header as ColumnHeader;

				captureMouseForElement = this;

				// this flag will start the dragging if the mouse has
				// moved more than 4 pixels is
				this.startDraggig = true;
				return true;
			}

			return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );			
		}


		/// <summary>
		/// Overridden method. Has logic for column dragging.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		/// <returns></returns>
		protected override void OnMouseMove( MouseEventArgs e )
		{
			Point p = new Point( e.X, e.Y );


			UltraGrid grid = null;

			if ( null != this.Header && null != this.Header.Band.Layout )
				grid = this.Header.Band.Layout.Grid as UltraGrid;

			// If dragging is already in pursuit
			if ( null != grid && this.dragging )
			{				
				ISelectionManager sm = grid as ISelectionManager;
				
				if ( null != sm )
				{
					int timerInterval = 200;
					if ( sm.DoesDragNeedScrollHorizontal( p, ref timerInterval ) )
					{
						sm.DoDragScrollHorizontal( timerInterval );
					}
				}

				// MRS 3/15/04 - Column Moving for RowLayouts
				//grid.DragEffect.OnDragMove( p );								
				grid.DragMoveHelper(p);

				return;
			}

			if ( startDraggig )
			{
				// JAS v5.2 New Default Look & Feel For UltraGrid 4/12/05
				// Now that the GroupByButton supports hottracking, the element
				// must be invalidated once the dragging begins to erase the 
				// hottracking border which is drawn prior to the drag operation.
				//
				if( this.ActiveThemeMouseTracking )
					this.Invalidate();

				int dx = p.X - this.lastClickedPoint.X;
				int dy = p.Y - this.lastClickedPoint.Y;

				// If the distance moved is greater thatn 4 points, start the
				// draggin operation
				if ( Math.Sqrt( dx*dx + dy*dy ) > 4 )
				{		
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Remove unused locals
					//HeaderClickAction action = this.Header.Band.HeaderClickActionResolved;
								
					if ( null != grid )
					{
						bool ret = false;

						if ( this.Header is ColumnHeader )
						{
							Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader = (ColumnHeader)this.Header;
							
							//MRS 3/23/05 - Column Drag/Drop support for RowLayouts
							//ret = columnHeader.StartDragNoSelection( true );
							// MD 8/3/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//if (this.Header.Column.Band.UseRowLayoutResolved)
							UltraGridColumn headerColumn = this.Header.Column;

							if ( headerColumn.Band.UseRowLayoutResolved )
							{
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Remove unused locals
								//bool rowLayoutDragStarted = grid.StartGridBagLayoutDrag(this.Header.Column, true);
								// MD 8/3/07 - 7.3 Performance
								// Prevent calling expensive getters multiple times
								//grid.StartGridBagLayoutDrag( this.Header.Column, true );
								grid.StartGridBagLayoutDrag( headerColumn, true );

								// If there's no GroupByBox showing, we can simply exit. 
								// If there is one, we want to let this go ahead and create 
								// the usual dragColumnStrategy in addition to the GridBagLayoutDragStrategy.
								// MD 8/3/07 - 7.3 Performance
								// Prevent calling expensive getters multiple times
								//if (!this.Header.Column.Band.Layout.GroupByBox.HiddenResolved)
								if ( !headerColumn.Band.Layout.GroupByBox.HiddenResolved )
									ret = columnHeader.StartDragNoSelection( true );
							}
							else
								ret = columnHeader.StartDragNoSelection( true );
						}	

						// if start drag operation was successful
						if ( ret )
						{
							// MRS 3/15/04 - Column Moving for RowLayouts
							//grid.DragEffect.OnDragMove( p );								
							grid.DragMoveHelper(p);
							
							// not to start drag next time since the dragging has
							// already been started
							this.startDraggig = false;
							
							// set it to true to indicate that currenty we are in drag mode
							this.dragging = true;

							return;
						}
					}
				}
			}

			base.OnMouseMove( e );
		}

		/// <summary>
		/// Called when the mouse up message is received over the element. 
		/// </summary>
		/// <param name="e">Mouse event arguments</param>
		protected override bool OnMouseUp( MouseEventArgs e )
		{
			UltraGrid grid = null;
			
			this.startDraggig = false;

			if ( null != this.Header && null != this.Header.Band.Layout )
				grid = this.Header.Band.Layout.Grid as UltraGrid;

			if ( this.dragging && null != grid )
			{	
				// Reset the flags
				this.dragging = false;	

				((ISelectionManager)grid).OnDragEnd( false );
				
				// JAS v5.2 New Default Look & Feel For UltraGrid 4/12/05
				// Now that the GroupByButton supports hottracking, we need to invalidate
				// it once the drag operation ends so that the hottracking border which is
				// drawn when the cursor re-enters the GroupByButton after a cancelled drag
				// operation can be erased.
				//
				if( this.ActiveThemeMouseTracking )
					this.Invalidate();

				return true;
			}
			else
			{
				if ( null != this.buttonDef.column && MouseButtons.Left == e.Button )
				{
					UltraGridColumn column = this.buttonDef.column;

					column.ClickGroupByButton( );
				}
			}

			return base.OnMouseUp( e );
		}

		/// <summary>
		/// Hide tooltip
		/// </summary>
		protected override void OnMouseLeave()
		{
			// JJD 1/7/02
			// Call the base implementation in case themes are supported 
			//
			base.OnMouseLeave();

			UltraGrid grid = this.GroupByBox.Layout.Grid as UltraGrid;

			if ( null == grid )
				return;

			// Don't create TooltipTool if we don't have one 
			// already
			if ( grid.HasTooltipTool )
			{
				Infragistics.Win.ToolTip toolTip = grid.TooltipTool;

				if ( null != toolTip )
				{
					toolTip.Hide();
				}
			}
		}

		// AS 1/6/04 Accessibility
		#region IsAccessibleElement
		/// <summary>
		/// Indicates if the element supports accessibility.
		/// </summary>
		public override bool IsAccessibleElement
		{
			get { return true; }
		}
		#endregion //IsAccessibleElement

		#region AccessibilityInstance
		/// <summary>
		/// Returns the accessible object associated with the element.
		/// </summary>
		/// <remarks>
		/// <p class="note"><b>Note</b> Derived elements that plan to return an accessible object must override 
		/// the <see cref="IsAccessibleElement"/> member.</p>
		/// </remarks>
		public override AccessibleObject AccessibilityInstance
		{
			get 
			{ 
				if (this.accessibleObject == null)
					this.accessibleObject = this.Header.Band.Layout.Grid.CreateAccessibilityInstance(this);

				return this.accessibleObject; 
			}
		}
		#endregion //AccessibilityInstance

		#region GroupByButtonAccessibleObject
		/// <summary>
		/// Accessible object representing a <see cref="GroupByButtonUIElement"/> in the <see cref="Infragistics.Win.UltraWinGrid.GroupByBox"/>
		/// </summary>
		public class GroupByButtonAccessibleObject : UIElementAccessibleObject
		{
			#region Constructor
			/// <summary>
			/// Initializes a new <see cref="GroupByButtonAccessibleObject"/>
			/// </summary>
			/// <param name="element">Associated element</param>
			public GroupByButtonAccessibleObject(GroupByButtonUIElement element) : base(element, AccessibleRole.PushButton)
			{
			}
			#endregion //Constructor

			#region Properties

			private GroupByButtonUIElement Button
			{
				get { return this.UIElement as GroupByButtonUIElement; }
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid uncalled private code
			#region Not Used

			//private UltraGridBand Band
			//{
			//    get { return this.Button.Header.Band; }
			//}

			#endregion Not Used

			private HeaderBase Header
			{
				get { return this.Button.Header; }
			}

			#endregion //Properties

			#region Base class overrides

			#region Name
			/// <summary>
			/// Returns the name of the accessible object.
			/// </summary>
			public override string Name
			{
				get
				{
					string name = base.Name;

					if (name == null)
						name = Infragistics.Win.Utilities.StripMnemonics(this.Header.Caption);

					return name;
				}
				set
				{
					base.Name = value;
				}
			}
			#endregion //Name

			#region DefaultAction
			/// <summary>
			/// Gets a string that describes the default action of the object. Not all objects have a default action.
			/// </summary>
			public override string DefaultAction
			{
				get
				{
					if ( !this.UIElement.Enabled )
						return null;

					return SR.GetString("AccessibleActionPush");
				}
			}
			#endregion //DefaultAction

			#region DoDefaultAction
			/// <summary>
			/// Performs the default action associated with this accessible object.
			/// </summary>
			public override void DoDefaultAction()
			{
				if (this.UIElement.Enabled)
				{
                    // SSP 6/13/05
                    // Whidbey gives warning for accessing private buttonDef member var.
                    //
					//UltraGridColumn column = this.Button.buttonDef.column;
                    HeaderBase header = this.Button.Header;
                    UltraGridColumn column = null != header ? header.Column : null;

					if (column != null)
						column.ClickGroupByButton();
				}
			}
			#endregion //DoDefaultAction

			#endregion //Base class overrides
		}
		#endregion //GroupByButtonAccessibleObject

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/12/05
		//
		#region Header/RowSelector Styles

			#region HeaderStyle
		/// <summary>
		/// Returns the HeaderStyle to be used by the GroupByButton.
		/// </summary>
		protected override HeaderStyle HeaderStyle
		{
			get
			{
				// If the Band associated with this GroupByButton's column is available,
				// return it's unresolved HeaderStyle value.  We need the unresolved value
				// because this property should resolve to 'Default' if no HeaderStyle was
				// specified.  This is necessary because the Band's HeaderStyle will resolve
				// to 'XPThemed', which the GroupByButton's default value should not be.  This
				// logic preserves the original behavior, prior to this functionality being added.
				//
				// MD 8/3/07 - 7.3 Performance
				// Refactored - Prevent calling expensive getters multiple times
				//if( this.Header				!= null &&
				//    this.Header.Column		!= null &&
				//    this.Header.Column.Band != null )
				//    return this.Header.Column.Band.HeaderStyleUnresolved;
				HeaderBase header = this.Header;

				if ( header != null )
				{
					UltraGridColumn column = header.Column;

					if ( column != null && column.Band != null )
						return column.Band.HeaderStyleUnresolved;
				}

				return HeaderStyle.Default;
			}
		}

			#endregion // HeaderStyle

			#region ActiveThemeMouseTracking

		/// <summary>
		/// Indicates if the element supports hot tracking over the header.
		/// </summary>
		protected override bool ActiveThemeMouseTracking
		{
			get
			{
                // MRS NAS v8.1 - Windows Vista Style for UltraWinTree
                //// Return true only if the HeaderStyle supports hottracking.
                //// Before this override was added, the base's default value of 'false' was always used.
                ////
                //return	this.HeaderStyle == HeaderStyle.WindowsXPCommand ||
                //    this.HeaderStyle == HeaderStyle.XPThemed;
                //
                switch (this.HeaderStyle)
                {
                    case HeaderStyle.WindowsXPCommand:
                    case HeaderStyle.XPThemed:
                    case HeaderStyle.WindowsVista:
                        return true;
                    case HeaderStyle.Standard:
                        return false;
                    case HeaderStyle.Default:
                        // MRS 7/25/2008
                        return false;
                    default:
                        Debug.Fail("Unknown HeaderStyle");
                        return false;
                }                
            }
		}

			#endregion // ActiveThemeMouseTracking

			#region DrawTheme

        /// <summary>
        /// Renders the element using the System theme.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
        /// <returns>True if the element was able to be rendered using the system themes.</returns>
		protected override bool DrawTheme(ref Infragistics.Win.UIElementDrawParams drawParams)
		{
			// If the HeaderStyle dictates that themed rendering should be used then just call off the 
			// base implementation, otherwise return false so that the non-themed rendering can occur.
			//
			if( this.HeaderStyle == HeaderStyle.XPThemed )
				return base.DrawTheme( ref drawParams );

			return false;
		}

			#endregion // DrawTheme

			#region RequiresSeparator

		/// <summary>
		/// Returns a boolean indicating if the item requires the rendering a separator.
		/// </summary>
		protected override bool RequiresSeparator
		{
			// For hottracking to be rendered when using the XPThemed style, the element must have a separator.
			// We only want the separator to exist when hottracking.  At all other times, no separator is needed.
			//
			get { return (this.HeaderState & UIElementButtonState.MouseOver) != 0;  }
		}

			#endregion // RequiresSeparator

		#endregion // Header/RowSelector Styles

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				if ( null != this.buttonDef.column )
					return StyleUtils.GetRole( this.GroupByBox.Layout, StyleUtils.Role.GroupByBoxColumnHeader );
				else if ( null != this.buttonDef.band )
					return StyleUtils.GetRole( this.buttonDef.band, StyleUtils.Role.GroupByBoxBandLabel );

				return null;
			}
		}

		#endregion // UIRole

        // MRS 4/25/2008 - BR32345
        #region OnDispose
        /// <summary>Called when element is disposed of.</summary>
        /// <remarks>
        /// <para class="body">The default implementation calls Dispose on all child elements and then clears the child elements collection.</para>
        /// </remarks>
        protected override void OnDispose()
        {
            this.EndDragHelper(true);

            base.OnDispose();
        }

        #endregion // OnDispose

        // MRS 4/25/2008 - BR32345
        // Re-factored code from the OnCaptureAborted into this helper method. 
        #region EndDragHelper
        private bool EndDragHelper(bool cancelled)
        {
            UltraGrid grid = null;

            // Reset flags 
            // SSP 10/3/01
            // we want to set the dragging to false below in the if
            // statemenet because the condition of the if statement
            // is checkig for this flag and it will always be false
            // if we do it here
            //
            //this.dragging = false;
            this.startDraggig = false;

            if (null != this.Header && null != this.Header.Band.Layout)
                grid = this.Header.Band.Layout.Grid as UltraGrid;

            if (this.dragging && null != grid)
            {
                this.dragging = false;

                ((ISelectionManager)grid).OnDragEnd(cancelled);
                return true;
            }

            return false;
        }
        #endregion //EndDragHelper
	}
	
}
