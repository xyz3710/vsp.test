#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

#region Using directives

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.ComponentModel;
using System.Diagnostics;

using Infragistics.Win.UltraWinGrid;
using Infragistics.Shared;

using System.Drawing;

#endregion

namespace Infragistics.Win.UltraDataGridView
{
	/// <summary>
	/// A DataGridViewColumn which displays a DropDown UltraGrid. 
	/// </summary>	
    [ToolboxBitmap(typeof(UltraGridBase), AssemblyVersion.ToolBoxBitmapFolder + "UltraCombo.bmp")] // MRS 11/13/05
	public class UltraGridComboColumn : UltraTextEditorColumnBase
	{
		#region Private  /Internal Members

		private DropDownStyle dropDownStyle = DropDownStyle.DropDown;

		#endregion Private  /Internal Members

		#region Constructors
		/// <summary>
		/// Initializes a new <see cref="UltraColumnBase"/>
		/// </summary>
		public UltraGridComboColumn(): base() {}

		/// <summary>
		/// Initializes a new <see cref="UltraColumnBase"/>
		/// </summary>
		public UltraGridComboColumn(IContainer container) : base(container) { }
		
		#endregion Constructors

		#region Overrides

		#region CreateEditor
		/// <summary>
		/// Factory method for creating an editor
		/// </summary>
		/// <returns></returns>
		protected override Infragistics.Win.EmbeddableEditorBase CreateEditor()
		{
			UltraGridComboEditor editor = new UltraGridComboEditor(this.Owner);
			editor.Control.BindingContext = new BindingContext();
			return editor;
		}
		#endregion CreateEditor

		#region InitializeEditor
		/// <summary>
		/// Called after the creation of the editor to allow initializion such as hooking events or setting default properties
		/// </summary>
		protected override void InitializeEditor()
		{
			base.InitializeEditor();

			UltraCombo ultraCombo = this.UltraGridComboEditor.Control;
			ultraCombo.AfterCloseUp += new EventHandler(ultraCombo_AfterCloseUp);
			ultraCombo.AfterDropDown += new EventHandler(ultraCombo_AfterDropDown);
			ultraCombo.BeforeDropDown += new CancelEventHandler(ultraCombo_BeforeDropDown);
			ultraCombo.InitializeLayout += new InitializeLayoutEventHandler(ultraCombo_InitializeLayout);
			ultraCombo.InitializeRow += new InitializeRowEventHandler(ultraCombo_InitializeRow);
			ultraCombo.RowSelected += new RowSelectedEventHandler(ultraCombo_RowSelected);
		}
		#endregion InitializeEditor
				
		#region CreateOwner
		/// <summary>
		/// Factory method for creating the editor's Owner
		/// </summary>
		/// <returns></returns>
		protected override UltraColumnBase.ColumnOwner CreateOwner()
		{
			return new ColumnOwner(this);
		}
		#endregion CreateOwner

		#region Dispose
		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose(bool disposing)
		{
			UltraCombo ultraCombo = this.UltraGridComboEditor.Control;
			ultraCombo.AfterCloseUp -= new EventHandler(ultraCombo_AfterCloseUp);
			ultraCombo.AfterDropDown -= new EventHandler(ultraCombo_AfterDropDown);
			ultraCombo.BeforeDropDown -= new CancelEventHandler(ultraCombo_BeforeDropDown);
			ultraCombo.InitializeLayout -= new InitializeLayoutEventHandler(ultraCombo_InitializeLayout);
			ultraCombo.InitializeRow -= new InitializeRowEventHandler(ultraCombo_InitializeRow);
			ultraCombo.RowSelected -= new RowSelectedEventHandler(ultraCombo_RowSelected);

			base.Dispose(disposing);
		}
		#endregion Dispose

        #region InitializeFrom
        /// <summary>
        /// Used to copy the settings of the specified <see cref="UltraColumnBase"/>
        /// </summary>
        /// <param name="source">The column whose properties should be copied</param>
        protected override void InitializeFrom(UltraColumnBase source)
        {
            base.InitializeFrom(source);

            UltraGridComboColumn column = source as UltraGridComboColumn;

            if (column == null)
                return;

            this.DataSource = column.DataSource;
            this.DataMember = column.DataMember;
            this.DisplayMember = column.DisplayMember;
            this.DisplayLayout.CopyFrom(column.DisplayLayout);
            this.DropDownStyle = column.DropDownStyle;
            this.ValueMember = column.ValueMember;
        }
        #endregion //InitializeFrom

		#endregion Overrides

		#region Private Methods

		#endregion Private Methods

		#region Private Properties

		#region UltraGridComboEditor

		internal UltraGridComboEditor UltraGridComboEditor
		{
			get { return this.Editor as UltraGridComboEditor; }
		}

		#endregion UltraGridComboEditor

		#endregion Private Properties

		#region Public Properties

		#region DataSource
		/// <summary>
		/// Indicates the source of data for the column UltraCombo.
		/// </summary>
		[LocalizedDescription("LD_UltraGridComboColumn_P_DataSource")]
		[LocalizedCategory("LC_Data")]
		[DefaultValue(null)]
		[RefreshProperties( RefreshProperties.All)]
		[TypeConverter( "System.Windows.Forms.Design.DataSourceConverter, System.Design" )  ]
		public object DataSource 
		{
			get 
			{
				return this.UltraGridComboEditor.Control.DataSource;
			}

			set 
			{
				this.UltraGridComboEditor.Control.DataSource = value;
			}
		}

		#endregion DataSource

		#region DataMember

		/// <summary>
		/// Indicates a sub-list of the DataSource to show in the column UltraCombo.
		/// </summary>
		[LocalizedDescription("LD_UltraGridComboColumn_P_DataMember")]
		[LocalizedCategory("LC_Data")]
		[DefaultValue("")]
		[Editor("System.Windows.Forms.Design.DataMemberListEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing")]
		[RefreshProperties(RefreshProperties.All)]		
		public string DataMember
		{
			get
			{
				return this.UltraGridComboEditor.Control.DataMember;
			}
			set
			{
				this.UltraGridComboEditor.Control.DataMember = value;
			}
		}

		#endregion DataMember

		#region DisplayMember
		/// <summary>
		/// This identifies the column(via its key) in the dropdown that will be used 
		/// to set the text to display in the cell(or combo's edit area) 
		/// </summary>
		[DefaultValue(null)]
		[Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing")]
		[LocalizedDescription("LD_UltraGridComboColumn_P_DisplayMember")]
		[LocalizedCategory("LC_Data")]
		public string DisplayMember
		{
			get
			{
				return this.UltraGridComboEditor.Control.DisplayMember;
			}
			set
			{
				this.UltraGridComboEditor.Control.DisplayMember = value;
			}
		}
		#endregion DisplayMember

		#region DisplayLayout

		/// <summary>
		/// Returns the DisplayLayout object that determines the layout of the control. This property is read-only at run-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>DisplayLayout</b> property of an object is used to access the DisplayLayout object that determines the settings of various properties related to the appearance and behavior of the object. The DisplayLayout object provides a simple way to maintain multiple layouts for the grid and apply them as needed. You can also save grid layouts to disk, the registry or a storage stream and restore them later.</p>
		///	<p class="body">The DisplayLayout object has properties such as <b>Appearance</b> and <b>Override</b>, so the DisplayLayout object has sub-objects of these types, and their settings are included as part of the layout. However, the information that is actually persisted depends on how the settings of these properties were assigned. If the properties were set using the DisplayLayout object's intrinsic objects, the property settings will be included as part of the layout. However, if a named object was assigned to the property from a collection, the layout will only include the reference into the collection, not the actual settings of the named object. (For an overview of the difference between named and intrinsic objects, please see the <a href="appearance_property.htm"><b>Appearance</b> property</a>.</p>
		///	<p class="body">For example, if the DisplayLayout object's <b>Appearance</b> property is used to set values for the intrinsic Appearance object like this:</p>
		///	<p class="code">UltraWinGrid1.Layout.Appearance.ForeColor = vbBlue</p>
		///	<p class="body">Then the setting (in this case, <b>ForeColor</b>) will be included as part of the layout, and will be saved, loaded and applied along with the other layout data. However, suppose you apply the settings of a named object to the DisplayLayout's <b>Appearance</b> property in this manner:</p>
		///	<p class="code">UltraWinGrid1.Appearances.Add "New1"</p>
		///	<p class="code">UltraWinGrid1.Appearances("New1").ForeColor = vbBlue</p>
		///	<p class="code">UltraWinGrid1.DisplayLayout.Appearance = UltraWinGrid1.Appearances("New1")</p>
		///	<p class="body">In this case, the <b>ForeColor</b> setting will not be persisted as part of the layout. Instead, the layout will include a reference to the "New1" Appearance object and use whatever setting is present in that object when the layout is applied.</p>
		///	<p class="body">By default, the layout includes a copy of the entire Appearances collection, so if the layout is saved and restored using the default settings, the object should always be present in the collection when it is referred to. However, it is possible to use the <b>Load</b> and <b>Save</b> methods of the DisplayLayout object in such a way that the collection will not be re-created when the layout is applied. If this is the case, and the layout contains a reference to a nonexistent object, the default settings for that object's properties will be used.</p>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridComboColumn_P_DisplayLayout")]
		[LocalizedCategory("LC_Layout")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public Infragistics.Win.UltraWinGrid.UltraGridLayout DisplayLayout
		{
			get
			{
				return this.UltraGridComboEditor.Control.DisplayLayout;
			}
		}

		#region ShouldSerializeDisplayLayout
		
//		/// <summary>
//		/// Returns true if this property is not set to its default value
//		/// </summary>
//		/// <returns></returns>
//		protected bool ShouldSerializeDisplayLayout()
//		{
//			return this.UltraGridComboEditor.Control.DisplayLayout.ShouldSerialize() || this.UltraGridComboEditor.Control.DisplayLayout.ForceSerialization;
//		}
		#endregion ShouldSerializeDisplayLayout

		#region ResetDisplayLayout
		/// <summary>
		/// Resets the properties of the object to their default values.
		/// </summary>
		///	<remarks>
		///	<p class="body">
		///	Use this method to reset the properties of an DisplayLayout object to their default values. The appearance of any object associated with the DisplayLayout object will change accordingly. You can specify which groups of properties should be reset using the <i>categories</i> parameter. If you do not specify a value for this parameter, all property values are reset. You can also specify whether the <b>InitializeLayout</b> event will fire as a result of this method being invoked. If you do not specify this parameter, the event does not occur.</p>
		///	<p class="body">
		///	When specifying 256 (PropCatGeneral), the following property settings for the DisplayLayout object are reset:<br></br><br></br>
		///	<table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul class="body">
		///		<li>AddNewBox</li>
		///		<li>AlphaBlendEnabled</li>
		///		<li>BorderStyle</li>
		///		<li>BorderStyleCaption</li>
		///		<li>Caption</li>
		///		<li>Enabled</li></ul>
		///		</td>
		///		<td valign="top"><ul class="body">
		///		<li>EstimatedRows</li>
		///		<li>Font</li>
		///		<li>InterBandSpacing</li>
		///		<li>MaxColScrollRegions</li>
		///		<li>MaxRowScrollRegions</li>
		///		<li>Override</li>
		///		<li>RowConnectorColor</li>
		///		<li>RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul class="body">
		///		<li>ScrollBars</li>
		///		<li>TabNavigation</li>
		///		<li>TagVariant</li>
		///		<li>TipDelay</li>
		///		<li>ViewStyle</li>
		///		<li>ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">
		///	Multiple DisplayLayout categories can be copied by combining them using logical <b>Or</b>.</p>
		///	</remarks>
		public void ResetDisplayLayout()
		{
			this.UltraGridComboEditor.Control.DisplayLayout.Reset();
		}
		#endregion ResetDisplayLayout

		#endregion DisplayLayout

		#region DropDownStyle
		/// <summary>
		/// Determines whether the edit portion of the control is editable.
		/// </summary>
		[LocalizedDescription("LD_UltraGridComboColumn_P_DropDownStyle")]
		[LocalizedCategory("LC_Behavior")]
		public DropDownStyle DropDownStyle
		{
			get
			{
				return this.dropDownStyle;
			}
			set
			{
				this.dropDownStyle = value;
			}
		}

		#endregion DropDownStyle

		#region ValueMember
		/// <summary>
		/// This identifies the column (via its key) in the dropdown
		/// that is associated with the value that the grid's cell is bound to.
		/// </summary>
		[DefaultValue(null)]
		[Editor("System.Windows.Forms.Design.DataMemberFieldEditor, System.Design", "System.Drawing.Design.UITypeEditor, System.Drawing")]
		[LocalizedDescription("LD_UltraGridComboColumn_P_ValueMember")]
		[LocalizedCategory("LC_Data")]
		public string ValueMember
		{
			get
			{
				return this.UltraGridComboEditor.Control.ValueMember;
			}

			set
			{
				this.UltraGridComboEditor.Control.ValueMember = value;
			}
		}
		#endregion ValueMember

		#endregion Public Properties

		#region Public Methods

		#region SetDataBinding
		/// <summary>
		/// Sets the datasource and datamember for the control in one atomic operation.
		/// </summary>
		public void SetDataBinding(object dataSource, string dataMember)
		{
			this.UltraGridComboEditor.Control.SetDataBinding(dataSource, dataMember);
		}

		/// <summary>
		/// Sets the datasource and datamember for the control in one atomic operation.
		/// </summary>
		public void SetDataBinding(object dataSource, string dataMember, bool hideNewColumns)
		{
			this.UltraGridComboEditor.Control.SetDataBinding(dataSource, dataMember, hideNewColumns);
		}
		#endregion SetDataBinding

		#region DataBind

		/// <summary>
		/// Start Databinding
		/// </summary>
		public void DataBind()
		{
			this.UltraGridComboEditor.Control.DataBind();
		}

		#endregion DataBind

		#endregion Public Methods

		#region Private Methods

		#region OnXXX methods

		#region OnAfterCloseUp
		/// <summary>
		/// Fires the AfterCloseUp event.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnAfterCloseUp(System.EventArgs e)
		{
			EventHandler eDelegate = (EventHandler)Events[EVENT_AFTERCLOSEUP];

			if (eDelegate != null)
				eDelegate(this, e);
		}
		#endregion OnAfterCloseUp

		#region OnAfterDropDown
		/// <summary>
		/// Fires the AfterDropDown event.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnAfterDropDown(System.EventArgs e)
		{
			EventHandler eDelegate = (EventHandler)Events[EVENT_AFTERDROPDOWN];

			if (eDelegate != null)
				eDelegate(this, e);
		}
		#endregion OnAfterDropDown

		#region OnBeforeDropDown
		/// <summary>
		/// Fires the BeforeDropDown event.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnBeforeDropDown(System.EventArgs e)
		{
			EventHandler eDelegate = (EventHandler)Events[EVENT_BEFOREDROPDOWN];

			if (eDelegate != null)
				eDelegate(this, e);
		}
		#endregion OnBeforeDropDown

		#region OnInitializeLayout
		/// <summary>
		/// Fires the InitializeLayout event.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializeLayout(InitializeLayoutEventArgs e)
		{
			InitializeLayoutEventHandler eDelegate = (InitializeLayoutEventHandler)Events[EVENT_INITIALIZELAYOUT];

			if (eDelegate != null)
				eDelegate(this, e);
		}
		#endregion OnInitializeLayout

		#region OnInitializeRow
		/// <summary>
		/// Fires the InitializeRow event.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnInitializeRow(InitializeRowEventArgs e)
		{
			InitializeRowEventHandler eDelegate = (InitializeRowEventHandler)Events[EVENT_INITIALIZEROW];

			if (eDelegate != null)
				eDelegate(this, e);
		}
		#endregion OnInitializeRow

		#region OnRowSelected
		/// <summary>
		/// Fires the RowSelected event.
		/// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> that contains the event data.</param>
		virtual protected void OnRowSelected(RowSelectedEventArgs e)
		{
			RowSelectedEventHandler eDelegate = (RowSelectedEventHandler)Events[EVENT_ROWSELECTED];

			if (eDelegate != null)
				eDelegate(this, e);
		}
		#endregion OnRowSelected

		#endregion OnXXX methods

		#endregion Private Methods

		#region Events

		private static readonly object EVENT_AFTERCLOSEUP = new object();
		private static readonly object EVENT_AFTERDROPDOWN = new object();
		private static readonly object EVENT_BEFOREDROPDOWN = new object();
		private static readonly object EVENT_INITIALIZELAYOUT = new object();
		private static readonly object EVENT_INITIALIZEROW = new object();
		private static readonly object EVENT_ROWSELECTED = new object();

		#region AfterDropDown
		/// <summary>
		/// Fired after the dropdown list is displayed 
		/// </summary>
		[LocalizedDescription("LD_UltraGridComboColumn_E_AfterDropDown")]
		public event EventHandler AfterDropDown
		{
			add { this.Events.AddHandler(EVENT_AFTERDROPDOWN, value); }
			remove { this.Events.RemoveHandler(EVENT_AFTERDROPDOWN, value); }
		}
		#endregion AfterDropDown

		#region AfterCloseUp
		/// <summary>
		/// Fired after the dropdown list closes 
		/// </summary>
		[LocalizedDescription("LD_UltraGridComboColumn_E_AfterCloseUp")]
		public event EventHandler AfterCloseUp
		{
			add { this.Events.AddHandler(EVENT_AFTERCLOSEUP, value); }
			remove { this.Events.RemoveHandler(EVENT_AFTERCLOSEUP, value); }
		}
		#endregion AfterCloseUp

		#region BeforeDropDown
		/// <summary>
		/// Fired before the dropdown list is displayed 
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// The <b>BeforeDropDown</b> event can be canceled; when this happens, the dropdown list is not displayed, and the <see cref="AfterDropDown"/> event does not fire.
		/// </p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridComboColumn_E_BeforeDropDown")]
		public event CancelEventHandler BeforeDropDown
		{
			add { this.Events.AddHandler(EVENT_BEFOREDROPDOWN, value); }
			remove { this.Events.RemoveHandler(EVENT_BEFOREDROPDOWN, value); }
		}
		#endregion BeforeDropDown

		#region InitializeLayout
		/// <summary>
		/// Occurs when the display layout is initialized, such as when the control is loading data from the data source.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <i>layout</i> argument returns a reference to a DisplayLayout object that can be used to set properties of, and invoke methods on, the layout of the control. You can use this reference to access any of the returned layout's properties or methods.</p>
		///	<p class="body">Like a form's <b>oad</b> event, this event provides an opportunity to configure the control before it is displayed. It is in this event procedure that actions such as creating appearances, valuelists, and unbound columns should take place.</p>
		///	<p class="body">This event is generated when the control is first preparing to display data from the data source. This may occur when the data source changes, or when the <b>Refresh</b> method is invoked.</p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridComboColumn_E_InitializeLayout")]
		public event InitializeLayoutEventHandler InitializeLayout
		{
			add { this.Events.AddHandler(EVENT_INITIALIZELAYOUT, value); }
			remove { this.Events.RemoveHandler(EVENT_INITIALIZELAYOUT, value); }
		}
		#endregion InitializeLayout

		#region InitializeRow
		/// <summary>
		/// Occurs when a row is initialized
		/// </summary>
		///	<remarks>
		///	<p class="body">The <i>row</i> argument returns a reference to an UltraGridRow object that can be used to set properties of, and invoke methods on, the row being displayed. You can use this reference to access any of the returned row's properties or methods.</p>
		///	<p class="body">The <i>reinitialize</i> argument can be used to determine whether the row's data has been changed since it was last displayed. The value of <i>reinitialize</i> can also be controlled when invoking the control or row's <b>Refresh</b> method, which causes this event to be generated. </p>
		///	<p class="body">This event is generated once for each row being displayed or printed and provides an opportunity to perform actions on the row before it is rendered, such as populating an unbound cell or changing a cell's color based on its value.</p>
		///	<p class="body">The <b>ViewStyle</b> and <b>ViewStyleBand</b> properties of the control and  DisplayLayout object are read-only in this event procedure.</p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeGroupByRow"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeLayout"/>
		///	</remarks>
		[LocalizedDescription("LD_UltraGridComboColumn_E_InitializeRow")]
		public event InitializeRowEventHandler InitializeRow
		{
			add { this.Events.AddHandler(EVENT_INITIALIZEROW, value); }
			remove { this.Events.RemoveHandler(EVENT_INITIALIZEROW, value); }
		}
		#endregion InitializeRow

		#region RowSelected
		/// <summary>
		/// Occurs when the row selection changes.
		/// </summary>
		[LocalizedDescription("LD_UltraGridComboColumn_E_RowSelected")]
		public event RowSelectedEventHandler RowSelected
		{
			add { this.Events.AddHandler(EVENT_ROWSELECTED, value); }
			remove { this.Events.RemoveHandler(EVENT_ROWSELECTED, value); }
		}
		#endregion RowSelected

		#endregion //Events

		#region Event Handlers

		#region Editor Events

		void ultraCombo_AfterCloseUp(object sender, EventArgs e)
		{
			this.OnAfterCloseUp(e);
		}

		void ultraCombo_AfterDropDown(object sender, EventArgs e)
		{
			this.OnAfterDropDown(e);
		}

		void ultraCombo_BeforeDropDown(object sender, CancelEventArgs e)
		{
			this.OnBeforeDropDown(e);
		}

		void ultraCombo_InitializeLayout(object sender, InitializeLayoutEventArgs e)
		{
			this.OnInitializeLayout(e);
		}

		void ultraCombo_InitializeRow(object sender, InitializeRowEventArgs e)
		{
			this.OnInitializeRow(e);
		}

		void ultraCombo_RowSelected(object sender, RowSelectedEventArgs e)
		{
			this.OnRowSelected(e);
		}
		#endregion Editor Events

		#endregion Event Handlers		

		#region Column Owner
		/// <summary>
		/// Column Owner
		/// </summary>
		protected new class ColumnOwner: UltraTextEditorColumnBase.ColumnOwner
		{
			#region Constructor

			/// <summary>
			/// ColumnOwner Constructor
			/// </summary>
			/// <param name="column">An UltraDataGridViewColumn</param>
			public ColumnOwner(UltraGridComboColumn column) : base(column) { }

			#endregion Constructor

			#region Private Properties

			#region UltraGridComboColumn
			private UltraGridComboColumn UltraGridComboColumn
			{
				get { return this.Column as UltraGridComboColumn; }
			}
			#endregion UltraGridComboColumn

			#endregion Private Properties

			#region Overrides

			#region GetValueList

			/// <summary>
			/// Returns a list of predefined values.
			/// </summary>
			/// <param name="ownerContext">The owner context.</param>
			/// <returns>Returns a list of predefined values or null.</returns>
			public override IValueList GetValueList(object ownerContext)
			{
				return this.UltraGridComboColumn.UltraGridComboEditor.ValueList;
			}

			#endregion GetValueList

			#region MustSelectFromList

			/// <summary>
			/// Returns whether a selection can only be made from the value list.
			/// </summary>
			/// <param name="ownerContext">The owner context</param>
			/// <remarks>
			/// <p class="body">If true will act as a combo with a style of DropDownList.</p>
			/// </remarks>
			/// <returns>If true will act as a combo with a style of DropDownList.</returns>
			public override bool MustSelectFromList(object ownerContext)
			{
				return this.UltraGridComboColumn.DropDownStyle == DropDownStyle.DropDownList;
			}

			#endregion MustSelectFromList

			#endregion Overrides
		}
		#endregion Column Owner

	}
}
