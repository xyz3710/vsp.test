#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Permissions;
using Infragistics.Win.Layout;
using System.Collections.Generic;

// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
// Added this file.
// 

namespace Infragistics.Win.UltraWinGrid
{

	#region MultiCellOperationInfo Class

	// SSP 11/15/05 - NAS 6.1 Multi-cell Operations Support
	// 
	/// <summary>
	/// Contains nested classes and other infrastructure for performing multi-cell operations.
	/// </summary>
	public class MultiCellOperationInfo
	{
		#region Nested Classes

		#region CellValueHolder Class

		/// <summary>
		/// A class that contains information on the cell value.
		/// </summary>
		public class CellValueHolder
		{
			private object value;
			private bool isValueDisplayText;

			private bool ignore = false;
			
			// The following flags are used by the CellValuesCollection's ConvertToDataValue method 
			// to pass back info to MultiCellValueAction's Perform method.
			// 
			private string conversionError = null;
			private bool hasSameValue = false;

			/// <summary>
			/// Constructor. Initializes a new instance of <see cref="CellValueHolder"/>.
			/// </summary>
			/// <param name="value">The cell value.</param>
			/// <param name="isValueDisplayText">Specifies whether the cell value is display text or the raw cell value.
			/// See <see cref="Value"/> property for more info.</param>
			public CellValueHolder( object value, bool isValueDisplayText )
			{
				this.value = value;
				this.isValueDisplayText = true;
			}

			/// <summary>
			/// Gets or sets the value.
			/// </summary>
			/// <remarks>
			/// <p class="body">
			/// If you set this property, make sure you also set the IsValueDisplayText property
			/// based on whether the value you are setting is a display text or not. If
			/// IsValueDisplayText is set to true, then the editor data filter conversions (namely
			/// DisplayToEditor and then EditorToOwner) will be performed on the set value.
			/// </p>
			/// </remarks>
			public object Value
			{
				get
				{
					return this.value;
				}
				set
				{
					this.value = value;
				}
			}

			/// <summary>
			/// Specifies whether the value being set is display text.
			/// </summary>
			/// <remarks>
			/// <p class="body">
			/// Please see the remarks of the <see cref="Value"/> property for information on what this property does.
			/// </p>
			/// </remarks>
			public bool IsValueDisplayText
			{
				get
				{
					return this.isValueDisplayText;
				}
				set
				{
					this.isValueDisplayText = value;
				}
			}

			internal string ConversionError
			{
				get
				{
					return this.conversionError;
				}
				set
				{
					this.conversionError = value;
				}
			}

			internal bool HasConversionError
			{
				get
				{
					return null != this.ConversionError;
				}
			}

			internal bool Ignore
			{
				get
				{
					return this.ignore;
				}
				set
				{
					this.ignore = value;
				}
			}

			internal bool HasSameValue
			{
				get
				{
					return this.hasSameValue;
				}
				set
				{
					this.hasSameValue = value;
				}
			}

			internal void Initialize( object val, bool isDisplayText )
			{
				this.value = val;
				this.isValueDisplayText = isDisplayText;
			}
		}

		#endregion // CellValueHolder Class

		#region CellsCollection Class

		/// <summary>
		/// Class associted with the <see cref="BeforeMultiCellOperationEventArgs.Cells"/> property.
		/// </summary>
		public class CellsCollection : ICollection
		{
			#region Private Vars

			private UltraGridLayout layout;
			private UltraGridCell[] table;
			private int rowCount;
			private int columnCount;
			private int nonNullCount;

			#endregion // Private Vars

			#region Constructor

			internal CellsCollection( UltraGridLayout layout, UltraGridCell[] table, int rowCount, int columnCount )
			{
				this.layout = layout;
				this.table = table;
				this.rowCount = rowCount;
				this.columnCount = columnCount;
				this.nonNullCount = this.table.Length - GridUtils.GetElemCount( this.table, null );
			}

			#endregion // Constructor

			#region Layout

			internal UltraGridLayout Layout
			{
				get
				{
					return this.layout;
				}
			}

			#endregion // Layout

			#region Indexer

			/// <summary>
			/// Indexer
			/// </summary>
			public UltraGridCell this[ int row, int column ]
			{
				get
				{
					return this.table[ row * this.ColumnCount + column ];
				}
			}

			#endregion // Indexer

			#region RowCount

			/// <summary>
			/// Number of logical rows that comprises the rectangular selection this cells collection represents.
			/// Note that when performing Delete operation (deleting contents of cells), the selection is not 
			/// required to be rectangular, in which case the indexer may return null values for cellls that
			/// are not selected.
			/// </summary>
			public int RowCount
			{
				get
				{
					return this.rowCount;
				}
			}

			#endregion // RowCount

			#region ColumnCount

			/// <summary>
			/// Number of logical columns that comprises the rectangular selection this cells collection represents.
			/// Note that when performing Delete operation (deleting contents of cells), the selection is not 
			/// required to be rectangular, in which case the indexer may return null values for cellls that
			/// are not selected.
			/// </summary>
			public int ColumnCount
			{
				get
				{
					return this.columnCount;
				}
			}

			#endregion // ColumnCount

			#region Implementation of ICollection

			void ICollection.CopyTo(System.Array array, int index)
			{
				this.table.CopyTo( array, index );
			}

			int ICollection.Count
			{
				get
				{
					return this.nonNullCount;
				}
			}

			bool ICollection.IsSynchronized
			{
				get
				{
					return this.table.IsSynchronized;
				}
			}
			
			object ICollection.SyncRoot
			{
				get
				{
					return this.table;
				}
			}

			System.Collections.IEnumerator IEnumerable.GetEnumerator()
			{
				return new GridUtils.MeetsCriteriaEnumerator( 
					this.table.GetEnumerator( ),
					new GridUtils.NonNullCriteria( ) );
			}

			#endregion // Implementation of ICollection

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid uncalled private code
			#region Not Used

			//#region Clone

			//internal CellsCollection Clone( )
			//{
			//    return new CellsCollection( this.Layout, (UltraGridCell[])this.table.Clone( ), this.rowCount, this.columnCount );
			//}

			//#endregion // Clone

			//            #region FirstCell

			//#if DEBUG
			//            /// <summary>
			//            /// Gets the first cell in the collection
			//            /// </summary>
			//#endif
			//            internal UltraGridCell FirstCell
			//            {
			//                get
			//                {
			//                    UltraGridCell cell = null;
			//                    for ( int i = 0; null == cell && i < this.table.Length; i++ )
			//                        cell = this.table[i];

			//                    return cell;
			//                }
			//            }

			//            #endregion // FirstCell

			#endregion Not Used

			#region LastCell
			
			internal UltraGridCell LastCell
			{
				get
				{
					UltraGridCell cell = null;
					for ( int i = this.table.Length - 1; null == cell && i >= 0; i-- )
						cell = this.table[i];

					return cell;
				}
			}

			#endregion // LastCell
		}

		#endregion // CellsCollection Class

		#region CellValuesCollection Class

		/// <summary>
		/// Class associted with the <see cref="BeforeMultiCellOperationEventArgs.NewValues"/> property.
		/// </summary>
		public class CellValuesCollection
		{
			#region Private Vars

			private Hashtable table = null;
			private MultiCellOperationInfo.CellsCollection cells = null;

			#endregion // Private Vars

			#region Constructor
			
			internal CellValuesCollection( MultiCellOperationInfo.CellsCollection cells )
			{
				this.cells = cells;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.table = this.CreateTable( cells );
				this.table = CellValuesCollection.CreateTable( cells );
			}

			#endregion // Constructor

			#region SetValues

			internal void SetValues( ClipboardData values )
			{
				int rowCount = Math.Min( this.cells.RowCount, values.RowCount );
				int columnCount = Math.Min( this.cells.ColumnCount, values.ColumnCount );

				for ( int y = 0; y < rowCount; y++ )
				{
					for ( int x = 0; x < columnCount; x++ )
					{
						UltraGridCell cell = this.cells[ y, x ];
						Debug.Assert( null != cell, "Cells collection should be parse only for Delete operation. For other operations a rectangular selection is required." );
						if ( null != cell )
						{
							object val = null;
							bool isDisplayText = false;
							
							if ( values.HasCellTexts )
							{
								val = values.GetCellText( y, x );
								isDisplayText = true;
							}
							else if ( values.HasCellValues )
							{
								val = values.GetCellValue( y, x );
								isDisplayText = false;
							}

							this[ cell ].Initialize( val, isDisplayText );
						}
					}
				}
			}

			#endregion // SetValues

			#region CreateTable

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//private Hashtable CreateTable( MultiCellOperationInfo.CellsCollection cells )
			private static Hashtable CreateTable( MultiCellOperationInfo.CellsCollection cells )
			{
				Hashtable table = new Hashtable( );

				foreach ( UltraGridCell cell in cells )
					table.Add( cell, new CellValueHolder( null, false ) );

				return table;
			}

			#endregion // CreateTable

			#region Exists
			
			/// <summary>
			/// Returns true if the cell exists in the collection.
			/// </summary>
			/// <param name="cell">An UltraGridCell</param>
            /// <returns>true if the cell exists in the collection.</returns>
			public bool Exists( UltraGridCell cell )
			{
				return this.table.ContainsKey( cell );
			}

			#endregion // Exists

			#region ValidateCell

			private void ValidateCell( UltraGridCell cell )
			{
				if ( ! this.Exists( cell ) )
					throw new ArgumentException( "Invalid cell. Cell does not exist in the collection.", "cell" );
			}

			#endregion // ValidateCell

			#region Indexer

			/// <summary>
			/// Gets or sets the new value for the specified cell. See remarks for the 
			/// <see cref="BeforeMultiCellOperationEventArgs.NewValues"/>
			/// </summary>
			public CellValueHolder this[ UltraGridCell cell ]
			{
				get
				{
					this.ValidateCell( cell );

					return (CellValueHolder)this.table[ cell ];
				}
			}

			#endregion // Indexer

			#region Cells

			internal MultiCellOperationInfo.CellsCollection Cells
			{
				get
				{
					return this.cells;
				}
			}

			#endregion // Cells

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid uncalled private code
			#region Not Used

			//#region Clone

			//internal CellValuesCollection Clone( )
			//{
			//    CellValuesCollection clone = (CellValuesCollection)this.MemberwiseClone( );
			//    clone.cells = this.cells.Clone( );

			//    clone.table = new Hashtable( );				
			//    foreach ( DictionaryEntry item in this.table )
			//        clone.table.Add( item.Key, item.Value );

			//    return clone;
			//}

			//#endregion // Clone

			#endregion Not Used

			#region GetColumnDefaultValue

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//internal object GetColumnDefaultValue( UltraGridCell cell )
			internal static object GetColumnDefaultValue( UltraGridCell cell )
			{
                // MBS 10/29/08 - TFS9383
                // I'm not sure why we're always returning DBNull here since the
                // data source might not be able to handle this.  We should be
                // using the DefaultCellValue of the column, and if this is null,
                // it will be properly converted by the Nullable property on the column.
                //
				//return DBNull.Value;
                //
                // MBS 1/30/09 - TFS13247
                // The default value of 'null' might not work either, so if it is null, we should
                // try to get the appropriate value based on the column's properties
                //return cell.Column.DefaultCellValue;                                
                return cell.Column.DefaultCellValue ?? cell.GetNullValueForMultiCellOperation();
			}

			#endregion // GetColumnDefaultValue

			#region ConvertToDataValue
			
			internal bool ConvertToDataValue( MultiCellOperationContext context, 
				out UltraGridCell stopCellExclusive, bool checkForReadOnlyCells )
			{
				stopCellExclusive = null;
				UltraGrid grid = this.Cells.Layout.Grid as UltraGrid;

				Hashtable readOnlyColumns = new Hashtable( );

				UltraGridCell lastCell = this.Cells.LastCell;

				foreach ( UltraGridCell cell in this.Cells )
				{
					CellValueHolder cvh = this[ cell ];
					if ( cvh.Ignore )
						continue;

					MultiCellOperationErrorInfo errorInfo = null;
					bool showErrorDlgDefault = true;
					bool displayContinueQuestion = cell != lastCell;

					if ( checkForReadOnlyCells && ! cell.CanBeModified )
					{
						string errorText = SR.GetString( "Error_Cell_ReadOnly", GridUtils.ToSingleLine( cell.Column.Header.Caption ) );
						cvh.ConversionError = errorText;

						// If the whole column is read-only then show the error message once for the entire 
						// column instead of one for each cell.
						// 
						if ( cell.Column.IsReadOnly )
						{
							if ( readOnlyColumns.ContainsKey( cell.Column ) )
								showErrorDlgDefault = false;
							else
								readOnlyColumns[ cell.Column ] = null;

							// If all the cells are from this read-only column then don't ask the user
							// whether to continue, since all the cells are going to be read-only.
							// 
							displayContinueQuestion = displayContinueQuestion && this.Cells.ColumnCount >= 2;
						}

						errorInfo = new MultiCellOperationErrorInfo(
							context.operationBeingPerformed, cell, new InvalidOperationException( errorText ), null, displayContinueQuestion );

						// Since read-only cells can't be cleared, default the Action to Continue instead
						// of ClearCellAndContinue.
						// 
						errorInfo.Action = MultiCellOperationErrorInfo.ErrorAction.Continue;
					}

					// Only convert if the value is string and IsValueDisplayText is set to true.
					// 
					if ( null == errorInfo && cvh.IsValueDisplayText && cvh.Value is string )
					{
						string displayText = (string)cvh.Value;
						object val;
						string error = null;

						if ( 0 == displayText.Length && cell.Column.IsNullable )
                            // MBS 11/17/08 - TFS10423
                            // We shouldn't just assume that the cell can accept DBNull.Value
							//val = DBNull.Value;
                            val = cell.GetNullValueForMultiCellOperation();
						else
						{
							val = cell.Column.ConvertDisplayTextToDataValue( cell.Row, displayText, out error );

							
							
							
							
							if ( null == val && null == error && displayText.Length > 0 )
								error = SR.GetString( "DataErrorCellUpdateInvalidDataValue" );
						}

						if ( null != error )
						{
							// If the value being pasted is the same as the cell's current value, don't show
							// any message.
							// 
							object currVal = cell.Value;
							if ( object.Equals( displayText, currVal )
								|| 0 == displayText.Length && ( DBNull.Value == currVal || null == currVal ) )
							{
								error = null;
								cvh.HasSameValue = true;
								val = currVal;
							}
							else
							{
								error = SR.GetString( "MultiCellOperation_Error_ConversionError", displayText, error );

								// If we weren't able to convert the value, then fire Error event.
								// 
								cvh.ConversionError = error;
								errorInfo = new MultiCellOperationErrorInfo(
									context.operationBeingPerformed, cell, new InvalidOperationException( error ), null, displayContinueQuestion );
							}
						}

						if ( null == error )
							cvh.Value = val;
					}

					if ( null != errorInfo )
					{
						// If the column is not nullable then we can't clear the cell. In which case
						// default the Action to Continue instead of ClearCellAndContinue.
						// 
						if ( ! cell.Column.IsNullable && MultiCellOperationErrorInfo.ErrorAction.ClearCellAndContinue == errorInfo.Action )
                            errorInfo.Action = MultiCellOperationErrorInfo.ErrorAction.Continue;

						if ( null != grid )
							grid.InternalHandleMultiCellOperationError( errorInfo, ! showErrorDlgDefault );

						// Take appropriate action based on the Action property settings.
						// 
						switch ( errorInfo.Action )
						{
							case MultiCellOperationErrorInfo.ErrorAction.Continue:
								// Continue with other cells. This will leave the cell value to 
								// what it is. It will know to not modify the cell value by looking
								// at the ConversionError which is set above.
								// 
								break;
							case MultiCellOperationErrorInfo.ErrorAction.ClearCellAndContinue:
								// This will clear the cell value.
								// 
								cvh.ConversionError = null;

								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//cvh.Value = this.GetColumnDefaultValue( cell );
								cvh.Value = CellValuesCollection.GetColumnDefaultValue( cell );

								break;
							case MultiCellOperationErrorInfo.ErrorAction.Revert:
								// Cancel the entire operaion. Simply return false. Note that there
								// is nothing to revert since we haven't applied any cell values yet.
								// 
								return false;
							case MultiCellOperationErrorInfo.ErrorAction.Stop:
								// Stop the operation at this point. Apply the cell values we have
								// processed so far. Set the stopCellExclusive out parameter so the 
								// caller knows when to stop.
								// 
								stopCellExclusive = cell;
								return true;
						}
					}
				}

				return true;
			}

			#endregion // ConvertToDataValue

			#region ConvertToDisplayText
			
			internal void ConvertToDisplayText( MultiCellOperationContext context )
			{
				foreach ( UltraGridCell cell in this.Cells )
				{
					CellValueHolder cvh = this[ cell ];
					if ( null != cvh && ! cvh.IsValueDisplayText )
					{
						string displayText = cell.Column.GetCellText( cell.Row, cvh.Value );
						cvh.Value = displayText;
						cvh.IsValueDisplayText = true;
					}
				}
			}

			#endregion // ConvertToDisplayText

			#region ResetCellValues
			
			internal bool ResetCellValues( MultiCellOperationContext context )
			{
				foreach ( UltraGridCell cell in this.Cells )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this[ cell ].Initialize( this.GetColumnDefaultValue( cell ), false );
					this[ cell ].Initialize( CellValuesCollection.GetColumnDefaultValue( cell ), false );
				}

				return true;
			}

			#endregion // ResetCellValues
		}

		#endregion // CellValuesCollection Class

		#region ClipboardData Class

		[ Serializable() ]
		internal class ClipboardData
		{
			#region Private Vars

			private string[] columnKeys = null;
			private object[] cellValues = null;
			private object[] cellTexts  = null;
			private int rowCount = 0;
			private int columnCount = 0;

			#endregion // Private Vars

			#region Constructor

			internal ClipboardData( 
				string[] columnKeys,
				object[] cellValues, 
				object[] cellTexts, 
				int rowCount, 
				int columnCount )
			{
				this.columnKeys  = columnKeys;
				this.cellValues  = cellValues;
				this.cellTexts   = cellTexts;
				this.rowCount    = rowCount;
				this.columnCount = columnCount;
			}

			#endregion // Constructor

			#region GetCellValue

			public object GetCellValue( int row, int column )
			{
				return this.CellValues[ row * this.columnCount + column ];
			}

			#endregion // GetCellValue

			#region GetCellText

			public object GetCellText( int row, int column )
			{
				return this.CellTexts[ row * this.columnCount + column ];
			}

			#endregion // GetCellText

			#region ColumnKeys
		
			public string[] ColumnKeys
			{
				get
				{
					return this.columnKeys;
				}
				set
				{
					this.columnKeys = value;
				}
			}

			#endregion // ColumnKeys

			#region CellValues
		
			public object[] CellValues
			{
				get
				{
					return this.cellValues;
				}
				set
				{
					this.cellValues = value;
				}
			}

			#endregion // CellValues

			#region CellTexts
		
			public object[] CellTexts
			{
				get
				{
					return this.cellTexts;
				}
				set
				{
					this.cellTexts = value;
				}
			}

			#endregion // CellTexts

			#region HasCellValues
			
			internal bool HasCellValues
			{
				get
				{

					return null != this.cellValues;
				}
			}

			#endregion // HasCellValues

			#region HasCellTexts
			
			internal bool HasCellTexts
			{
				get
				{

					return null != this.cellTexts;
				}
			}

			#endregion // HasCellTexts

			#region RowCount

			public int RowCount
			{
				get
				{
					return this.rowCount;
				}
				set
				{
					this.rowCount = value;
				}
			}

			#endregion // RowCount

			#region ColumnCount

			public int ColumnCount
			{
				get
				{
					return this.columnCount;
				}
				set
				{
					this.columnCount = value;
				}
			}

			#endregion // ColumnCount
		}

		#endregion // ClipboardData Class

		#region CellComparer Class

		internal class CellComparer : IComparer
		{
			#region Private Vars

			private Hashtable columnAbsoluteLocations = null;
			private UltraGridBand lastBand = null;
			private UltraGridColumn[] lastOrderedColumns = null;

			#endregion // Private Vars

			#region GetOrderedVisibleColumns
			
			private UltraGridColumn[] GetOrderedVisibleColumns( UltraGridBand band )
			{
				if ( this.lastBand == band && null != this.lastOrderedColumns )
					return this.lastOrderedColumns;

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = new ArrayList( );
				List<UltraGridColumn> list = new List<UltraGridColumn>();

				UltraGridColumn column = band.GetFirstVisibleCol( band.Layout.ActiveColScrollRegion, true, true );
				while ( null != column )
				{
					list.Add( column );
					column = band.GetRelatedVisibleCol( column, NavigateType.Next, true, true );
				}

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//this.lastOrderedColumns = (UltraGridColumn[])list.ToArray( typeof( UltraGridColumn ) );
				this.lastOrderedColumns = list.ToArray();

				this.lastBand = band;
				return this.lastOrderedColumns;
			}

			#endregion // GetOrderedVisibleColumns

			#region GetColumnAt
			
			internal UltraGridColumn GetColumnAt( UltraGridBand band, int location )
			{
				UltraGridColumn[] orderedCols = this.GetOrderedVisibleColumns( band );

				return location < orderedCols.Length ? orderedCols[ location ] : null;
			}

			#endregion // GetColumnAt

			#region GetAbsoluteLocation
			
			internal int GetAbsoluteLocation( UltraGridColumn column )
			{
				if ( null != this.columnAbsoluteLocations )
				{
					object val = columnAbsoluteLocations[ column ];
					if ( null != val )
						return (int)val;
				}

				UltraGridColumn[] orderedCols = this.GetOrderedVisibleColumns( column.Band );
				int loc = Array.IndexOf( orderedCols, column );

				if ( null == this.columnAbsoluteLocations )
					this.columnAbsoluteLocations = new Hashtable( );
				this.columnAbsoluteLocations[ column ] = loc;

				return loc;
			}

			#endregion // GetAbsoluteLocation

			#region CompareHorizLocation
			
			internal int CompareHorizLocation( UltraGridCell x, UltraGridCell y )
			{
				int xxColLoc = this.GetAbsoluteLocation( x.Column );
				int yyColLoc = this.GetAbsoluteLocation( y.Column );
				return xxColLoc.CompareTo( yyColLoc );
			}

			#endregion // CompareHorizLocation

			#region CompareVertLocation
			
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//internal int CompareVertLocation( UltraGridCell x, UltraGridCell y )
			internal static int CompareVertLocation( UltraGridCell x, UltraGridCell y )
			{
				return Selected.SelectionPositionSortComparer.CompareRow( x.Row, y.Row );
			}

			#endregion // CompareVertLocation

			#region Compare
			
			internal int Compare( UltraGridCell xx, UltraGridCell yy )
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//int r = this.CompareVertLocation( xx, yy );
				int r = CellComparer.CompareVertLocation( xx, yy );

				if ( 0 != r )
					return r;

				return this.CompareHorizLocation( xx, yy );
			}

			#endregion // Compare

			#region IComparer.Compare

			int IComparer.Compare( object xObj, object yObj )
			{
				UltraGridCell xx = (UltraGridCell)xObj;
				UltraGridCell yy = (UltraGridCell)yObj;

				return this.Compare( xx, yy );
			}

			#endregion // IComparer.Compare
		}

		#endregion // CellComparer Class

		#region MultiCellOperationContext Class

		internal class MultiCellOperationContext
		{
			internal MultiCellOperation operationBeingPerformed;

			internal MultiCellOperationContext( MultiCellOperation operationBeingPerformed )
			{
				this.operationBeingPerformed = operationBeingPerformed;
			}
		}

		#endregion // MultiCellOperationContext Class

		#region ParserTSV Class

		internal class ParserTSV
		{
			private MultiCellOperationInfo.ClipboardData data = null;

			private string text = null;
			private string cellSeparator = null;
			private string rowSeparator = null;
			private string cellDelimiter = null;

			private ParserTSV( string text, string cellSeparator, 
				string rowSeparator, string cellDelimiter )
			{
				this.text = text;
				this.cellSeparator = cellSeparator;
				this.rowSeparator = rowSeparator;
				this.cellDelimiter = cellDelimiter;
			}

			internal string RemoveDelimiters( string text )
			{
				if ( null != text )
				{
					text = text.Trim( );

					string delim = this.cellDelimiter;

					if ( text.Length >= 2 * delim.Length && text.StartsWith( delim ) && text.EndsWith( delim ) )
						text = text.Substring( delim.Length, text.Length - 2 * delim.Length );
				}

				return text;
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//private string[] Split( string text, string separator )
			private static string[] Split( string text, string separator )
			{
				if ( null == text || null == separator || 0 == separator.Length || 0 == text.Length )
				{
					Debug.Assert( false );
					return null != text && text.Length > 0 ? new string[] { text } : null;
				}

				// If the separator is only one character in length then use the more efficient
				// Split method of the string.
				// 
				if ( 1 == separator.Length )
					return text.Split( separator[0] );

				separator = System.Text.RegularExpressions.Regex.Escape( separator );
				string[] lines = System.Text.RegularExpressions.Regex.Split( text, separator, 
					RegexOptions.ExplicitCapture | RegexOptions.Singleline );

				// Regex.Split method retrns an array that contains an empty string at the end.
				// Take out that empty string at the end.
				// 
				int ei = lines.Length - 1;
				while ( ei >= 0 && ( null == lines[ ei ] || 0 == lines[ ei ].Length ) )
					ei--;

				if ( ei < 0 )
				{
					lines = null;
				}
				else if ( ei != lines.Length - 1 )
				{
					string[] arr = new string[ 1 + ei ];
					Array.Copy( lines, 0, arr, 0, arr.Length );
					lines = arr;
				}

				return lines;
			}

			private void Parse( )
			{
				this.data = null;
				Hashtable table = new Hashtable( );

				// Get the lines by splitting the text by the row separator.
				// 
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//string[] lines = this.Split( text, this.rowSeparator );
				string[] lines = ParserTSV.Split( text, this.rowSeparator );

				int columnCount = 0;
				int rowCount = 0;

				if ( null != lines )
				{
					// Number of lines is the row count.
					// 
					rowCount = lines.Length;

					for ( int y = 0; y < lines.Length; y++ )
					{
						// Split the line into cell values.
						// 
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//string[] cells = null != lines[y] ? this.Split( lines[y], this.cellSeparator ) : null;
						string[] cells = null != lines[ y ] ? ParserTSV.Split( lines[ y ], this.cellSeparator ) : null;

						if ( null == cells )
							continue;

						// Column count is the maximum number of columns encountered.
						// 
						columnCount = Math.Max( columnCount, cells.Length );
                
						for ( int x = 0; x < cells.Length; x++ )
							table[ new Point( x, y ) ] = this.RemoveDelimiters( cells[ x ] );
					}
				}

				if ( rowCount * columnCount > 0 )
				{
					// Create an array of ClipboardDataCellValue that represents the table of cell values.
					// 

					object[] cellTexts = new object[ rowCount * columnCount ];

					for ( int y = 0; y < rowCount; y++ )
					{
						for ( int x = 0; x < columnCount; x++ )
							cellTexts[ y * columnCount + x ] = table[ new Point( x, y ) ];
					}

					// Set the data member variable to the array.
					// 
					this.data = new MultiCellOperationInfo.ClipboardData( null, null, cellTexts, rowCount, columnCount );
				}
			}

			internal static MultiCellOperationInfo.ClipboardData Parse( string text, UltraGridLayout layout )
			{
				ParserTSV p = new ParserTSV( text, layout.ClipboardCellSeparatorResolved, 
					layout.ClipboardRowSeparatorResolved, layout.ClipboardCellDelimiterResolved );

				p.Parse( );
				return p.data;
			}
		}

		#endregion // ParserTSV Class

		#endregion // Nested Classes

		#region Private Vars

		private UltraGridLayout layout = null;

		#endregion // Private Vars

		#region Constructor

		internal MultiCellOperationInfo( UltraGridLayout layout )
		{
			this.layout = layout;
		}

		#endregion // Constructor

		#region Layout
		
		internal UltraGridLayout Layout
		{
			get
			{
				return this.layout;
			}
		}

		#endregion // Layout

		#region CellQualifies

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool CellQualifies( UltraGridCell cell )
		private static bool CellQualifies( UltraGridCell cell )
		{
            // MRS 1/16/2008 - BR29722
            //return ! cell.Row.HiddenResolved && ! cell.Column.Hidden;
            return !cell.Row.HiddenResolved &&
                !cell.Column.HiddenResolved;
		}

		#endregion // CellQualifies

		#region ConvertToSparseTable

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UltraGridCell[] ConvertToSparseTable( UltraGridCell[] sortedCells, out int rowCount, out int columnCount, out Exception error )
		private static UltraGridCell[] ConvertToSparseTable( UltraGridCell[] sortedCells, out int rowCount, out int columnCount, out Exception error )
		{
			rowCount = 0;
			columnCount = 0;
			error = null;

			// Construct a table where points designating the row 'locations' and column 'locations'
			// are the keys and the values are the cells. Essentially the goal is to construct a 'sparse'
			// rectangular table that represents non-contiguous and non-rectangular selection where 
			// non-selected cells are skipped from the 'sparse' table.
			// 
			Hashtable table = new Hashtable( sortedCells.Length );
			CellComparer comparer = new CellComparer( );
			UltraGridRow lastRow = null;

			for ( int i = 0; i < sortedCells.Length; i++ )
			{
				UltraGridCell cell = sortedCells[i];
				if ( cell.Row != lastRow )
				{
					rowCount++;
					lastRow = cell.Row;
				}

				int y = rowCount - 1;
				int x = comparer.GetAbsoluteLocation( cell.Column );
				table[ new Point( x, y ) ] = cell;
			}

			// Figure out the number of columns the 'rectangular' table will have. Keep in mind that
			// selection could skip columns altogather. That is you could have a cells from Col0 and
			// Col2 selected, but none from Col1.
			// 
			int minX = 0, maxX = 0;
			foreach ( Point p in table.Keys )
			{
				minX = Math.Min( minX, p.X );
				maxX = Math.Max( maxX, p.X );
			}

			// Create a columnMap array that we can use to 'compact' that table and eliminate 
			// empty columns (columns that do not have any selected cells).
			// 
			int[] columnMap = new int[ 1 + maxX - minX ];
			GridUtils.InitializeArray( columnMap, -1 );
			foreach ( Point p in table.Keys )
				columnMap[ p.X - minX ] = 1;

			// Initialize the map so the elements are the 'compacted' index.
			// 
			columnCount = 0;
			for ( int x = 0; x < columnMap.Length; x++ )
			{
				if ( columnMap[x] > 0 )
				{
					columnMap[ x ] = columnCount;
					columnCount++;
				}
			}

			// If the sortedCells array is already in the desired 'rectangular' form then return it.
			//
			if ( columnCount * rowCount == sortedCells.Length )
				return sortedCells;

			// This shouldn't happen.
			// 
			if ( columnCount <= 0 || rowCount <= 0 )
			{
				Debug.Assert( false );
				error = new Exception( "Invalid selection." );
				return null;
			}

			// Create the rectangular 'sparse' table and return it.
			// 
			UltraGridCell[] retCells = new UltraGridCell[ columnCount * rowCount ];
			foreach ( DictionaryEntry e in table )
			{
				Point p = (Point)e.Key;
				retCells[ p.Y * columnCount + columnMap[ p.X - minX ] ] = (UltraGridCell)e.Value;
			}

			return retCells;
		}

		#endregion // ConvertToSparseTable

		#region GetCellsForMultiCellOperation

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void AddCellsHelper( ArrayList list, UltraGridCell cell )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private static void AddCellsHelper( ArrayList list, UltraGridCell cell )
		private static void AddCellsHelper( List<UltraGridCell> list, UltraGridCell cell )
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//if ( this.CellQualifies( cell ) )
			if ( MultiCellOperationInfo.CellQualifies( cell ) )
				list.Add( cell );
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void AddCellsHelper( ArrayList list, UltraGridRow row )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private static void AddCellsHelper( ArrayList list, UltraGridRow row )
		private static void AddCellsHelper( List<UltraGridCell> list, UltraGridRow row )
		{
			Debug.Assert(row.IsGroupByRow == false, "A groupby row does not contain cells and therefore there is nothing to add.");

			// AS 1/17/06 BR09041
			// AS 1/26/06 BR09335
			// Changed to use the SupportCells property.
			//
			//if (row.IsGroupByRow == false)
			if (row.SupportsCells)
			{
				foreach ( UltraGridCell cell in row.Cells )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.AddCellsHelper( list, cell );
					MultiCellOperationInfo.AddCellsHelper( list, cell );
				}
			}
		}

		private UltraGridCell[] GetCellsForMultiCellOperation( MultiCellOperationContext context, Selected selection )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<UltraGridCell> list = new List<UltraGridCell>();

			// If cells are selected then use them for the multi-cell operations.
			// 
			if ( selection.Cells.Count > 0 )
			{
				foreach ( UltraGridCell cell in selection.Cells )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.AddCellsHelper( list, cell );
					MultiCellOperationInfo.AddCellsHelper( list, cell );
				}
			}
			// If rows are selected then use the associated cells for the 
			// multi-cell operations.
			// 
			else if ( selection.Rows.Count > 0 )
			{
				foreach ( UltraGridRow row in selection.Rows )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.AddCellsHelper( list, row );
					MultiCellOperationInfo.AddCellsHelper( list, row );
				}
			}
			else
			{
				// Otherwise use the active cell or if there isn't any active cell,
				// the cells of the active row.
				// 
				UltraGridRow activeRow = this.Layout.ActiveRow;
				UltraGridCell activeCell = this.Layout.ActiveCell;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//if ( null != activeCell && this.CellQualifies( activeCell ) )
				if ( null != activeCell && MultiCellOperationInfo.CellQualifies( activeCell ) )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.AddCellsHelper( list, activeCell );
					MultiCellOperationInfo.AddCellsHelper( list, activeCell );
				}
				else if ( null != activeRow )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.AddCellsHelper( list, activeRow );
					MultiCellOperationInfo.AddCellsHelper( list, activeRow );
				}
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (UltraGridCell[])list.ToArray( typeof( UltraGridCell ) );
			return list.ToArray();
		}

		#endregion // GetCellsForMultiCellOperation

		#region HasSelectionHelper

		internal UltraGridBand HasSelectionHelper( )
		{
			Selected selection = this.GetSelectedHelper( );

			if ( null != selection )
			{
				if ( selection.HasCells )
					return selection.Cells[0].Band;
				else if ( selection.HasRows )
					return selection.Rows[0].Band;
				else if ( null != this.Layout.ActiveCell )
					return this.Layout.ActiveCell.Band;
				else if ( null != this.Layout.ActiveRow )
					return this.Layout.ActiveRow.Band;
			}

			return null;
		}

		#endregion // HasSelectionHelper

		// AS 1/17/06 BR09041
		// GroupBy rows do not have a cells collection and therefore there is nothing to
		// cut/copy/paste. We decided that it didn't make sense for the end user to see
		// an error message for this situation so if we have groupby rows selected, we'll
		// consider cut/copy/paste as invalid operations.
		//
		#region HasGroupByRowSelection

		internal bool HasGroupByRowSelection
		{
			get
			{
				Selected selection = this.GetSelectedHelper( );

				if ( null != selection )
				{
					if ( selection.HasGroupByRows )
						return true;

					if ( selection.HasCells || selection.HasRows)
						return false;

					else if ( null != this.Layout.ActiveCell )
						return false;

					else if ( null != this.Layout.ActiveRow )
						return this.Layout.ActiveRow.IsGroupByRow;
				}

				return false;
			}
		}

		#endregion // HasGroupByRowSelection

		#region GetSelectedHelper

		internal Selected GetSelectedHelper( )
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			return null != grid && grid.HasSelectedBeenAllocated ? grid.Selected : null;
		}

		#endregion // GetSelectedHelper

		#region ScrollAndSelect
		
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal void ScrollAndSelect( IList cells, bool scrollIntoView, bool activateFirstCell )
		internal void ScrollAndSelect( IList<UltraGridCell> cells, bool scrollIntoView, bool activateFirstCell )
		{
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			if ( null != grid )
			{
				// SSP 3/31/06 BR11083
				// When primary key of a child row is modified via paste operation
				// then that row may get removed from the existing row collection
				// and moved to a different row collection. This means that the
				// associated cells in the cells list will be disposed. Filter out
				// the disposed cells.
				// 
				// ------------------------------------------------------------------
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList nonDisposedCells = new ArrayList( );
				List<UltraGridCell> nonDisposedCells = new List<UltraGridCell>();

				foreach ( UltraGridCell cell in cells )
				{
					if ( ! cell.Disposed )
						nonDisposedCells.Add( cell );
				}

				cells = nonDisposedCells;
				// ------------------------------------------------------------------

				// NOTE: SelectNewSelection returns false on success, true on failure.
				// 
				bool success = ! grid.SelectNewSelection( cells );

				if ( success && cells.Count > 0 )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//UltraGridCell firstCell = (UltraGridCell)cells[0];
					UltraGridCell firstCell = cells[ 0 ];

					if ( activateFirstCell && null != firstCell )
							firstCell.Activate( );

					if ( scrollIntoView )
					{
						// MRS 1/10/06 - BR08665
						// Sandip's notes say that we are following Excel behavior, 
						// but in my testing, Excel 2003 does not scroll the selection
						// into view, it only scrolls the Active Cell into view, 
						// if it was not completely in view. 
						//
						//grid.ScrollSelectionIntoView( );
						grid.ScrollActiveItemIntoView();
						
					}
				}
			}
		}

		#endregion // ScrollAndSelect

		#region GetCurrentSelection

		internal MultiCellOperationInfo.CellsCollection GetCurrentSelection( MultiCellOperationContext context, out Exception error )
		{
			error = null;
			Selected selection = this.GetSelectedHelper( );

			return null != selection 
				? this.GetCellsCollection( context, selection, out error )
				: null;
		}

		internal MultiCellOperationInfo.CellsCollection GetCellsCollection( MultiCellOperationContext context, Selected selection, out Exception error )
		{
			UltraGridCell[] cells = this.GetCellsForMultiCellOperation( context, selection );

			return this.GetCellsCollection( context, cells, out error );
		}

		internal MultiCellOperationInfo.CellsCollection GetCellsCollection( MultiCellOperationContext context, UltraGridCell[] cells, out Exception error )
		{
			if ( null == cells || 0 == cells.Length )
			{
				error = new InvalidOperationException( SR.GetString( "MultiCellOperation_Error_NoCellsSelected" ) );
				return null;
			}

			if ( ! GridUtils.IsFromSameRowsCollection( cells ) )
			{
				error = new InvalidOperationException( SR.GetString( "MultiCellOperation_Error_CrossParentSelection" ) );
				return null;
			}

			Utilities.SortMerge( cells, new CellComparer( ) );

			int rowCount, columnCount;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//UltraGridCell[] table = this.ConvertToSparseTable( cells, out rowCount, out columnCount, out error );
			UltraGridCell[] table = MultiCellOperationInfo.ConvertToSparseTable( cells, out rowCount, out columnCount, out error );

			if ( null != error )
				return null;

			if ( MultiCellOperation.Delete != context.operationBeingPerformed )
			{
				bool isSelectionRectangular = Array.IndexOf( table, null ) < 0;
				if ( ! isSelectionRectangular )
				{
					
					error = new InvalidOperationException( SR.GetString( "MultiCellOperation_Error_NonRectangularSelection" ) );
					return null;
				}
			}
			
			return new MultiCellOperationInfo.CellsCollection( this.Layout, table, rowCount, columnCount );
		}

		#endregion // GetCurrentSelection

		#region GetRectangularSelection

		internal MultiCellOperationInfo.CellsCollection GetRectangularSelection( 
			UltraGridCell anchorCell, int rowCount, int columnCount, out Exception error )
		{
			error = null;
			UltraGridBand band = anchorCell.Band;
			CellComparer comparer = new CellComparer( );

			int anchorColumnLoc = comparer.GetAbsoluteLocation( anchorCell.Column );
			if ( anchorColumnLoc < 0 )
			{
				Debug.Assert( false );
				error = new Exception( "Invalid current selection." );
				return null;
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList columnsList = new ArrayList( );
			List<UltraGridColumn> columnsList = new List<UltraGridColumn>();

			for ( int i = 0; i < columnCount; i++ )
			{
				UltraGridColumn col = comparer.GetColumnAt( band, anchorColumnLoc + i );
				if ( null == col )
					break;

				columnsList.Add( col );
			}

			UltraGridRow row = anchorCell.Row;
			RowsCollection parentRows = row.ParentCollection;

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList rowsList = new ArrayList( rowCount );
			List<UltraGridRow> rowsList = new List<UltraGridRow>( rowCount );

			rowsList.Add( row );

			while ( rowsList.Count < rowCount && null != row )
			{
				row = parentRows.GetNextVisibleRow( row, IncludeRowTypes.DataRowsOnly );
				if ( null != row )
					rowsList.Add( row );
			}

			if ( rowsList.Count < rowCount )
			{
				error = new InvalidOperationException( SR.GetString( "MultiCell_Paste_Error_TooManyRows", rowCount, rowsList.Count ) );
				return null;
			}

			if ( columnsList.Count < columnCount )
			{
				error = new InvalidOperationException( SR.GetString( "MultiCell_Paste_Error_TooManyColumns", columnCount, columnsList.Count ) );
				return null;
			}

			UltraGridCell[] cells = new UltraGridCell[ rowsList.Count * columnsList.Count ];

			int ii = 0;
			foreach ( UltraGridRow iRow in rowsList )
			{
				foreach ( UltraGridColumn iCol in columnsList )
					cells[ ii++ ] = iRow.Cells[ iCol ];	
			}

			return new MultiCellOperationInfo.CellsCollection( this.Layout, cells, rowsList.Count, columnsList.Count );
		}

		#endregion // GetRectangularSelection

		#region ClipboardHasSuitableData
		
		internal bool ClipboardHasSuitableData
		{
			get
			{
				ClipboardData data;
				Exception error;
				return this.GetDataFromClipboardHelper( true, out data, out error );
			}
		}

		#endregion // ClipboardHasSuitableData

		#region GetDataFromClipboard

		private bool GetDataFromClipboardHelper( bool justCheckForExistence, out ClipboardData data, out Exception error )
		{
			data = null;
			error = null;

            // MBS 12/8/08 - TFS10936
            // It's possible for an application to lock the clipboard such that an exception will be thrown if 
            // we try to access the data on it; if for whatever reason an application cannot access the 
            // clipboard after several attempts (all performed by the GetDataObject method), an
            // ExternalException will be thrown, which we should handle.
            try
            {
                IDataObject o = Clipboard.GetDataObject();
                if (o.GetDataPresent(typeof(ClipboardData)))
                {
                    if (justCheckForExistence)
                        return true;

                    data = o.GetData(typeof(ClipboardData)) as ClipboardData;
                    if (null != data)
                        return true;
                }

                if (o.GetDataPresent(typeof(string)))
                {
                    if (justCheckForExistence)
                        return true;

                    string text = o.GetData(typeof(string)) as string;
                    if (null != text && text.Length > 0)
                    {
                        data = ParserTSV.Parse(text, this.layout);
                        return true;
                    }
                }
            }
            catch (System.Runtime.InteropServices.ExternalException) { }

			return false;
		}

		internal ClipboardData GetDataFromClipboard( out Exception error )
		{
			ClipboardData data;
			this.GetDataFromClipboardHelper( false, out data, out error );
			return data;
		}

		#endregion // GetDataFromClipboard

		#region GetClipboardDataObject

		private IDataObject GetClipboardDataObject( CellValuesCollection cellValues, bool fireBeforeMultiCellOperationEvent )
		{
			DataObject o = new DataObject( );

			// Fire the BeforeMultiCellOperation.
			// 
			MultiCellOperationContext context = new MultiCellOperationContext( MultiCellOperation.Copy );
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			if ( fireBeforeMultiCellOperationEvent && null != grid )
			{
				BeforeMultiCellOperationEventArgs e = new BeforeMultiCellOperationEventArgs( context.operationBeingPerformed, cellValues );
				grid.FireEvent( GridEventIds.BeforeMultiCellOperation, e );
				if ( e.Cancel )
					return null;
			}

			// Convert the cell values to display texts.
			// 
			cellValues.ConvertToDisplayText( context );

			// Get the clipboard data in our internal format.
			// 
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//o.SetData( typeof( ClipboardData ), this.CreateUltraGridClipboardData( cellValues ) );
			o.SetData( typeof( ClipboardData ), MultiCellOperationInfo.CreateUltraGridClipboardData( cellValues ) );

			// Get the tab separated as well as HTML cell values.
			// 
			o.SetData( this.GetTabSeparatedText( cellValues ) );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//o.SetData( DataFormats.Html, this.GetHtmlText( cellValues ) );
			o.SetData( DataFormats.Html, MultiCellOperationInfo.GetHtmlText( cellValues ) );

			return o;
		}

		#endregion // GetClipboardDataObject

		#region IncludeColumnHeaders

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool IncludeColumnHeaders( UltraGridBand band )
		internal static bool IncludeColumnHeaders( UltraGridBand band )
		{
			return 0 != ( AllowMultiCellOperation.CopyWithHeaders & band.AllowMultiCellOperationsResolved );
		}

		#endregion // IncludeColumnHeaders

		#region CreateUltraGridClipboardData

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal ClipboardData CreateUltraGridClipboardData( CellValuesCollection cellValues )
		internal static ClipboardData CreateUltraGridClipboardData( CellValuesCollection cellValues )
		{
			CellsCollection cells = cellValues.Cells;
			string[] clipboardColumnKeys = new string[ cells.ColumnCount ];
			object[] clipboardCellValues = new object[ cells.RowCount * cells.ColumnCount ];
			object[] clipboardCellTexts  = new object[ cells.RowCount * cells.ColumnCount ];

			for ( int x = 0; x < cells.ColumnCount; x++ )
				clipboardColumnKeys[ x ] = cells[ 0, x ].Column.Key;

			int ii = 0;
			foreach ( UltraGridCell cell in cells )
			{
				clipboardCellValues[ ii ] = cell.Value;
				clipboardCellTexts[ ii ]  = cellValues[ cell ].Value;
				ii++;
			}

			return new ClipboardData( clipboardColumnKeys, 
				clipboardCellValues, clipboardCellTexts, cells.RowCount, cells.ColumnCount );
		}

		#endregion // CreateUltraGridClipboardData

		#region GetCellValues

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal CellValuesCollection GetCellValues( CellsCollection cells )
		internal static CellValuesCollection GetCellValues( CellsCollection cells )
		{
			CellValuesCollection cellValues = new CellValuesCollection( cells );

			foreach ( UltraGridCell cell in cellValues.Cells )
				cellValues[ cell ].Initialize( cell.Value, false );

			return cellValues;
		}

		#endregion // GetCellValues

		#region GetCellTextHelper

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal string GetCellTextHelper( CellValueHolder cvh, string delim )
		internal static string GetCellTextHelper( CellValueHolder cvh, string delim )
		{
			object val = cvh.Value;
			string text = null != val ? val.ToString( ) : null;
			if ( null == text )
				text = string.Empty;

			text = GridUtils.ApplyDelimiters( text, delim );

			return text;
		}

		#endregion // GetCellTextHelper

		#region GetColumnHeaderText

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal string GetColumnHeaderText( UltraGridCell cell, string delim )
		internal static string GetColumnHeaderText( UltraGridCell cell, string delim )
		{
			string text = null != cell 
				? GridUtils.ToSingleLine( cell.Column.Header.Caption ) 
				: string.Empty;

			text = GridUtils.ApplyDelimiters( text, delim );

			return text;
		}

		#endregion // GetColumnHeaderText

		#region GetTabSeparatedText

		internal object GetTabSeparatedText( CellValuesCollection cellValues )
		{
			StringBuilder sb = new StringBuilder( );
			CellsCollection cells = cellValues.Cells;

			string rowSeparator = this.Layout.ClipboardRowSeparatorResolved;
			string cellSeparator = this.Layout.ClipboardCellSeparatorResolved;
			string delim = this.Layout.ClipboardCellDelimiterResolved;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//if ( this.IncludeColumnHeaders( cells[ 0, 0 ].Band ) )
			if ( MultiCellOperationInfo.IncludeColumnHeaders( cells[ 0, 0 ].Band ) )
			{
				for ( int x = 0; x < cells.ColumnCount; x++ )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string text = this.GetColumnHeaderText( cells[ 0, x ], delim );
					string text = MultiCellOperationInfo.GetColumnHeaderText( cells[ 0, x ], delim );

					sb.Append( text );
					if ( 1 + x < cells.ColumnCount )
						sb.Append( cellSeparator );
				}

				sb.Append( rowSeparator );
			}

			for ( int y = 0; y < cells.RowCount; y++ )
			{
				for ( int x = 0; x < cells.ColumnCount; x++ )
				{
					// NOTE: The values should already have been converted to display text.
					// 
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string text = this.GetCellTextHelper( cellValues[ cells[ y, x ] ], delim );
					string text = MultiCellOperationInfo.GetCellTextHelper( cellValues[ cells[ y, x ] ], delim );

					sb.Append( text );
					if ( 1 + x < cells.ColumnCount )
						sb.Append( this.layout.ClipboardCellSeparatorResolved );
				}

				if ( 1 + y < cells.RowCount )
					sb.Append( this.layout.ClipboardRowSeparatorResolved );
			}

			return sb.ToString( );
		}

		#endregion // GetTabSeparatedText

		#region GetHtmlText

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal object GetHtmlText( CellValuesCollection cellValues )
		internal static object GetHtmlText( CellValuesCollection cellValues )
		{
			// AS 1/6/06 BR08650
			// The CF_HTML format is supposed to be encoded using UTF-8.
			// http://msdn.microsoft.com/library/default.asp?url=/workshop/networking/clipboard/htmlclipboard.asp
			// 
			Encoding encoding = System.Text.Encoding.UTF8;

			CellsCollection cells = cellValues.Cells;
			StringBuilder sb = new StringBuilder( );

			// Create html clipboard header mask we can use with the string.Format to
			// create the actual header.
			// 
			string headerMask = "Version:1.0\r\nStartHTML:{0:0000000000}"
				+ "\r\nEndHTML:{1:0000000000}"
				+ "\r\nStartFragment:{2:0000000000}"
				// AS 1/6/06 BR08650
				// Added extra line feed for formatting.
				//+ "\r\nEndFragment:{3:0000000000}";
				+ "\r\nEndFragment:{3:0000000000}\r\n";

			// For efficiency purposes, create a stub that's going to be the same length
			// as the final actual header. This way the StringBuilder.Replace doesn't have
			// to move memory.
			// 
			string headerStub = string.Format( headerMask, 0, 0, 0, 0 );

			sb.Append( headerStub );

			// AS 1/6/06 BR08650
			// We need the byte count offset.
			//
			//int iiStartHTML = sb.Length;
			int iiStartHTML = encoding.GetByteCount(sb.ToString());

			// AS 1/6/06 BR08650
			// We should include the charset.
			//
			//sb.Append( "<html><body>\r\n" );
			sb.AppendFormat( "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset={0}\" ></head>\r\n", encoding.WebName);
			sb.Append( "<body>\r\n" );

			// AS 1/6/06 BR08650
			// We need the byte count offset.
			//
			//int iiStartFragment = sb.Length;
			int iiStartFragment = encoding.GetByteCount(sb.ToString());

			sb.Append( "<!--StartFragment-->\r\n" );

			sb.Append( "<table>" );

			// AS 2/3/06 BR09817
			// We should not insert a table row for the headers unless
			// we are actually going to output the column header text 
			// since that could cause an application (like Excel) to 
			// interpret that as a blank row of cells. I moved these 
			// tr appends into the if block below.
			//
			//sb.Append( "<tr>" );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//if ( this.IncludeColumnHeaders( cells[ 0, 0 ].Band ) )
			if ( MultiCellOperationInfo.IncludeColumnHeaders( cells[ 0, 0 ].Band ) )
			{
				sb.Append( "<tr>" );

				for ( int x = 0; x < cells.ColumnCount; x++ )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string text = this.GetColumnHeaderText( cells[ 0, x ], null );
					string text = MultiCellOperationInfo.GetColumnHeaderText( cells[ 0, x ], null );

					// AS 1/6/06
					// While fixing BR08650, I noticed that we are not encoding
					// the text for html so we could end up putting out invalid
					// text. E.g. if the cell value has < or > characters.
					//
					text = System.Web.HttpUtility.HtmlEncode(text);

					sb.Append( "<th>" ).Append( text ).Append( "</th>" );
				}

				sb.Append( "</tr>" );
			}

			// AS 2/3/06 BR09817
			//sb.Append( "</tr>" );

			for ( int y = 0; y < cells.RowCount; y++ )
			{
				sb.Append( "<tr>" );

				for ( int x = 0; x < cells.ColumnCount; x++ )
				{
					// NOTE: The values should already have been converted to display text.
					// 
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string text = this.GetCellTextHelper( cellValues[ cells[ y, x ] ], null );
					string text = MultiCellOperationInfo.GetCellTextHelper( cellValues[ cells[ y, x ] ], null );

					// AS 1/6/06
					// While fixing BR08650, I noticed that we are not encoding
					// the text for html so we could end up putting out invalid
					// text. E.g. if the cell value has < or > characters.
					//
					text = System.Web.HttpUtility.HtmlEncode(text);

					sb.Append( "<td>" ).Append( text ).Append( "</td>" );
				}

				sb.Append( "</tr>" );
			}

			sb.Append( "</table>" );

			sb.Append( "\r\n<!--EndFragment-->" );

			// AS 1/6/06 BR08650
			// We need the byte count offset.
			//
			//int iiEndFragment = sb.Length;
			int iiEndFragment = encoding.GetByteCount(sb.ToString());

			sb.Append( "\r\n</body></html>" );

			// AS 1/6/06 BR08650
			// We need the byte count offset.
			//
			//int iiEndHTML = sb.Length;
			int iiEndHTML = encoding.GetByteCount(sb.ToString());

			// Create the actual clipboard header based on the actual offsets.
			// 
			string actualHeader = string.Format( headerMask, iiStartHTML, iiEndHTML, iiStartFragment, iiEndFragment );

			Debug.Assert( actualHeader.Length == headerStub.Length, "The lengths should be the same." );

			// Replace the stub header with the actual header.
			// 
			sb.Replace( headerStub, actualHeader, 0, headerStub.Length );

			// AS 1/6/06 BR08650
			// We have to return a memory stream that contains the utf encoded bytes.
			//
			//return sb.ToString( );
			return new System.IO.MemoryStream( encoding.GetBytes(sb.ToString()) );
		}

		#endregion // GetHtmlText

		#region GetMultiCellOperationRelatedState
		
		internal UltraGridState GetMultiCellOperationRelatedState( )
		{
			UltraGridState state = 0;

			if ( this.CanPerformMultiCellOperation( MultiCellOperation.Copy ) )
				state |= UltraGridState.CanCopy;

			if ( this.CanPerformMultiCellOperation( MultiCellOperation.Cut ) )
				state |= UltraGridState.CanCut;

			if ( this.CanPerformMultiCellOperation( MultiCellOperation.Delete ) )
				state |= UltraGridState.CanDeleteCells;

			if ( this.CanPerformMultiCellOperation( MultiCellOperation.Paste ) )
				state |= UltraGridState.CanPaste;

			if ( this.CanPerformMultiCellOperation( MultiCellOperation.Redo ) )
				state |= UltraGridState.CanRedo;

			if ( this.CanPerformMultiCellOperation( MultiCellOperation.Undo ) )
				state |= UltraGridState.CanUndo;

			return state;
		}

		#endregion // GetMultiCellOperationRelatedState

		#region CanPerformMultiCellOperation
		
		internal bool CanPerformMultiCellOperation( MultiCellOperation operation )
		{
			// First check if there is an selected or active cell/row that we can
			// perform the multi-cell operation on. And then check to see if the
			// associated band allows for the specified operation.
			// 
			switch ( operation )
			{
				case MultiCellOperation.Copy:
				case MultiCellOperation.Cut:
				case MultiCellOperation.Delete:
				case MultiCellOperation.Paste:
					UltraGridBand band = this.HasSelectionHelper( );
					if ( null == band || ! band.IsMultiCellOperationAllowed( operation ) )
						return false;
					break;
			}

			// Then check if the conditions are right for each operation.
			// 
			switch ( operation )
			{
				case MultiCellOperation.Copy:
				case MultiCellOperation.Cut:
					// We are already checking for if we have selection above. Don't check to see
					// if the selection is suitable for copy or cut operation because we want to
					// fire an error event if the user attempts to copy a non-rectangular selection
					// so a message box can be displayed to the user.
					// 
					// AS 1/17/06 BR09041
					if (this.HasGroupByRowSelection)
						return false;
					break;
				case MultiCellOperation.Delete:
					// When rows are selected, the Delete key should delete the rows, not clear its
					// cell contents. Therefore return false for the multi-cell Delete operation if
					// rows, and not the cells, are selected.
					// 
					Selected selected = this.GetSelectedHelper( );
					if ( null == selected || selected.HasRows || ! selected.HasCells )
						return false;
					break;
				case MultiCellOperation.Paste:
					// NOTE: ClipboardHasSuitableData only checks to see if the clipboard has 
					// UltraGridClipboardData or string data. It doesn't actually parse the data.
					// 
					// AS 1/17/06 BR09041
					if (this.HasGroupByRowSelection)
						return false;

					return this.ClipboardHasSuitableData;
				case MultiCellOperation.Redo:
					if ( ! this.Layout.ActionHistory.CanRedo( ) )
						return false;
					break;
				case MultiCellOperation.Undo:
					if ( ! this.Layout.ActionHistory.CanUndo( ) )
						return false;
					break;
				default:
					Debug.Assert( false, "Unknown type of clipboard operation." );
					return false;
			}

			return true;
		}

		#endregion // CanPerformMultiCellOperation

		#region PerformMultiCellOperation
		
		internal bool PerformMultiCellOperation( MultiCellOperation operation )
		{
			// Multi cell operations are valid for only the UltraGrid.
			// 
			UltraGrid grid = this.Layout.Grid as UltraGrid;
			Selected selection = null != grid ? grid.Selected : null;
			if ( null == selection )
				return false;

			MultiCellOperationContext context = new MultiCellOperationContext( operation );
			Exception error = null;
			bool ret = false;

			switch ( operation )
			{
				case MultiCellOperation.Copy:
				case MultiCellOperation.Cut:
				case MultiCellOperation.Delete:
				{
					// Get the selected cells and their values.
					// 
					CellsCollection cells = this.GetCurrentSelection( context, out error );

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//CellValuesCollection cellValues = null != cells ? this.GetCellValues( cells ) : null;
					CellValuesCollection cellValues = null != cells ? MultiCellOperationInfo.GetCellValues( cells ) : null;

					if ( null == error )
					{
						// Ge the clipboard object that we need to set. Do so only for Copy and Cut
						// operations.
						// 
						IDataObject o = MultiCellOperation.Delete != operation 
							? this.GetClipboardDataObject( cellValues, true ) : null;

						if ( null != o )
						{
							try
							{
								
								Utilities.CopyToClipboard(o, true);

								// SSP 3/21/06 BR10990
								// We need to return true when the operation is successful.
								// 
								ret = true;
							}
							catch ( Exception exception )
							{
								error = exception;
								break;
							}
						}

						// If we are performing Delete or Cut operations, then delete the cell values.
						// Note that in case of Cut, procceed only if Copy operation was successful.
						// 
						if (
							// SSP 3/21/06 BR10990
							// When cutting, only perform the Delete operation if the Copy operation was successful.
							// 
							//null != o && MultiCellOperation.Cut == operation 
							ret && null != o && MultiCellOperation.Cut == operation 
							|| MultiCellOperation.Delete == operation )
						{
							// Call ResetCellValues to reset the cell values to their column's default values.
							// 
							if ( cellValues.ResetCellValues( new MultiCellOperationContext( MultiCellOperation.Delete ) ) )
								// Apply the new cell values to the cells. This method will also take
								// care of maintining the undo history for the action.
								// 
								ret = this.PerformMultiCellValueActionHelper( context, cellValues );
						}
					}

					break;
				}
				case MultiCellOperation.Paste:
				{
					// Get the data from the clipboard.
					// 
					ClipboardData data = this.GetDataFromClipboard( out error );
					if ( null != data && null == error )
					{
						// Get the current selection.
						// 
						CellsCollection cells = this.GetCurrentSelection( context, out error );
						if ( null == error && cells.RowCount > 0 && cells.ColumnCount > 0 )
						{
							// Get the anchor cell from which the paste operation will begin. The anchor
							// cell is the top and left-most cell.
							// 
							UltraGridCell anchorCell = cells[ 0, 0 ];

							// Starting from the anchor cell, get block of cells with the same number
							// of rows and columns as in the clipboard data.
							// 
							cells = this.GetRectangularSelection( anchorCell, data.RowCount, data.ColumnCount, out error );
							if ( null != cells && null == error )
							{
								// Get current cell values for cells onto which we are pasting the 
								// clipboard data.
								// 
								// MD 7/26/07 - 7.3 Performance
								// FxCop - Mark members as static
								//CellValuesCollection cellValues = this.GetCellValues( cells );
								CellValuesCollection cellValues = MultiCellOperationInfo.GetCellValues( cells );

								if ( null != cellValues )
								{
									// Set the cell values from the clipboard data to the cellValues
									// data structure.
									// 
									cellValues.SetValues( data );

									// Apply the cell values in the cellValues data structure to the
									// cells. This method will also take care of maintining the undo 
									// history for the action.
									// 
									ret = this.PerformMultiCellValueActionHelper( context, cellValues );
								}
							}
						}
					}

					break;
				}
				case MultiCellOperation.Redo:
					ret = this.Layout.ActionHistory.Redo( );
					break;
				case MultiCellOperation.Undo:
					ret = this.Layout.ActionHistory.Undo( );
					break;
				default:
					Debug.Assert( false, "Unknown type of clipboard operation." );
					return false;
			}

			if ( null != error )
			{
				// If there was an error (typically relating to how the current selection is
				// inappropriate), then fire the Error event.
				// 
				MultiCellOperationErrorInfo errorInfo = new MultiCellOperationErrorInfo( operation, null, error, null, false );
				grid.InternalHandleMultiCellOperationError( errorInfo, false );
			}

			return ret;
		}

		#endregion // PerformMultiCellOperation

		#region PerformMultiCellValueActionHelper

		private bool PerformMultiCellValueActionHelper( MultiCellOperationContext context, CellValuesCollection newCellValues )
		{
			// Create a multi cell value action from the specified cell values.
			// 
			ActionHistory actionHistory = this.Layout.ActionHistory;
			ActionFactory.MultiCellValuesAction action = new ActionFactory.MultiCellValuesAction( actionHistory, newCellValues );

			// Tell the action history to perform the action so it can can keep
			// track of the undo action.
			// 
			return actionHistory.PerformAction( context, action );
		}

		#endregion // PerformMultiCellValueActionHelper
	}

	#endregion // MultiCellOperationInfo Class

}
