#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections.Generic;
using System.Text;
using Infragistics.Shared;

namespace Infragistics.Win.UltraWinGrid
{
    // NOTE: When adding new pre-defined operands to this class, there are a few steps that need to be followed:
    // 1) Define the public property
    // 2) Update the default capacity of the RegisteredOperands collection 
    // 3) Register the operand through the VerifyDefaultOperandsRegistered method
    // 4) Null out the referend to the operand in the Reset method so that it will be regenerated

    /// <summary>
    /// A static class providing pre-defined operands.
    /// </summary>
    public static class SpecialFilterOperands
    {
        #region Members

        private static RelativeDateOperand tomorrow;
        private static RelativeDateOperand today;
        private static RelativeDateOperand yesterday;
        private static RelativeDateOperand nextWeek;
        private static RelativeDateOperand thisWeek;
        private static RelativeDateOperand lastWeek;
        private static RelativeDateOperand nextMonth;
        private static RelativeDateOperand thisMonth;
        private static RelativeDateOperand lastMonth;
        private static RelativeDateOperand nextQuarter;
        private static RelativeDateOperand thisQuarter;
        private static RelativeDateOperand lastQuarter;
        private static RelativeDateOperand nextYear;
        private static RelativeDateOperand thisYear;
        private static RelativeDateOperand lastYear;
        private static RelativeDateOperand yearToDate;
        private static QuarterOperand quarter1;
        private static QuarterOperand quarter2;
        private static QuarterOperand quarter3;
        private static QuarterOperand quarter4;
        private static MonthOperand january;
        private static MonthOperand february;
        private static MonthOperand march;
        private static MonthOperand april;
        private static MonthOperand may;
        private static MonthOperand june;
        private static MonthOperand july;
        private static MonthOperand august;
        private static MonthOperand september;
        private static MonthOperand october;
        private static MonthOperand november;
        private static MonthOperand december;

        private static bool hasRegisteredDefaultOperands;
        private static Dictionary<string, SpecialFilterOperand> registeredOperands;

        #endregion //Members

        #region Private Properties

        #region RegisteredOperands

        private static Dictionary<string, SpecialFilterOperand> RegisteredOperands
        {
            get
            {
                if (registeredOperands == null)                    
                    registeredOperands = new Dictionary<string, SpecialFilterOperand>(32);

                return registeredOperands;
            }
        }
        #endregion //RegisteredOperands

        #endregion //Private Properties

        #region Public Properties

        #region Tomorrow

        /// <summary>
        /// An operand used to determine if a specified value is equal to tomorrow's date.
        /// </summary>
        public static SpecialFilterOperand Tomorrow
        {
            get
            {
                if (tomorrow == null)
                    tomorrow = new RelativeDateOperand("Tomorrow", SR.GetString("SpecialFilterOperand_Tomorrow"), RelativeDateOperand.RelativeDate.Tomorrow);

                return tomorrow;
            }
        }
        #endregion //Tomorrow

        #region Today

        /// <summary>
        /// An operand used to determine if a specified value is equal to today's date.
        /// </summary>
        public static SpecialFilterOperand Today
        {
            get
            {
                if (today == null)
                    today = new RelativeDateOperand("Today", SR.GetString("SpecialFilterOperand_Today"), RelativeDateOperand.RelativeDate.Today);

                return today;
            }
        }
        #endregion //Today

        #region Yesterday

        /// <summary>
        /// An operand used to determine if a specified value is equal to yesterday's date.
        /// </summary>
        public static SpecialFilterOperand Yesterday
        {
            get
            {
                if (yesterday == null)
                    yesterday = new RelativeDateOperand("Yesterday", SR.GetString("SpecialFilterOperand_Yesterday"), RelativeDateOperand.RelativeDate.Yesterday);

                return yesterday;
            }
        }
        #endregion //Yesterday

        #region NextWeek

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of next week.
        /// </summary>
        public static SpecialFilterOperand NextWeek
        {
            get
            {
                if (nextWeek == null)
                    nextWeek = new RelativeDateOperand("NextWeek", SR.GetString("SpecialFilterOperand_NextWeek"), RelativeDateOperand.RelativeDate.NextWeek);

                return nextWeek;
            }
        }
        #endregion //NextWeek

        #region ThisWeek

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of this week.
        /// </summary>
        public static SpecialFilterOperand ThisWeek
        {
            get
            {
                if (thisWeek == null)
                    thisWeek = new RelativeDateOperand("ThisWeek", SR.GetString("SpecialFilterOperand_ThisWeek"), RelativeDateOperand.RelativeDate.ThisWeek);

                return thisWeek;
            }
        }
        #endregion //ThisWeek

        #region LastWeek

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of last week.
        /// </summary>
        public static SpecialFilterOperand LastWeek
        {
            get
            {
                if (lastWeek == null)
                    lastWeek = new RelativeDateOperand("LastWeek", SR.GetString("SpecialFilterOperand_LastWeek"), RelativeDateOperand.RelativeDate.LastWeek);

                return lastWeek;
            }
        }
        #endregion //LastWeek

        #region NextMonth

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of next month.
        /// </summary>
        public static SpecialFilterOperand NextMonth
        {
            get
            {
                if (nextMonth == null)
                    nextMonth = new RelativeDateOperand("NextMonth", SR.GetString("SpecialFilterOperand_NextMonth"), RelativeDateOperand.RelativeDate.NextMonth);

                return nextMonth;
            }
        }
        #endregion //NextMonth

        #region ThisMonth

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of this month.
        /// </summary>
        public static SpecialFilterOperand ThisMonth
        {
            get
            {
                if (thisMonth == null)
                    thisMonth = new RelativeDateOperand("ThisMonth", SR.GetString("SpecialFilterOperand_ThisMonth"), RelativeDateOperand.RelativeDate.ThisMonth);

                return thisMonth;
            }
        }
        #endregion //ThisMonth

        #region LastMonth

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of last month.
        /// </summary>
        public static SpecialFilterOperand LastMonth
        {
            get
            {
                if (lastMonth == null)
                    lastMonth = new RelativeDateOperand("LastMonth", SR.GetString("SpecialFilterOperand_LastMonth"), RelativeDateOperand.RelativeDate.LastMonth);

                return lastMonth;
            }
        }
        #endregion //LastMonth

        #region NextQuarter

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of next quarter.
        /// </summary>
        public static SpecialFilterOperand NextQuarter
        {
            get
            {
                if (nextQuarter == null)
                    nextQuarter = new RelativeDateOperand("NextQuarter", SR.GetString("SpecialFilterOperand_NextQuarter"), RelativeDateOperand.RelativeDate.NextQuarter);

                return nextQuarter;
            }
        }
        #endregion //NextQuarter

        #region ThisQuarter

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of the current quarter.
        /// </summary>
        public static SpecialFilterOperand ThisQuarter
        {
            get
            {
                if (thisQuarter == null)
                    thisQuarter = new RelativeDateOperand("ThisQuarter", SR.GetString("SpecialFilterOperand_ThisQuarter"), RelativeDateOperand.RelativeDate.ThisQuarter);

                return thisQuarter;
            }
        }
        #endregion //ThisQuarter

        #region LastQuarter

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of the previous quarter.
        /// </summary>
        public static SpecialFilterOperand LastQuarter
        {
            get
            {
                if (lastQuarter == null)
                    lastQuarter = new RelativeDateOperand("LastQuarter", SR.GetString("SpecialFilterOperand_LastQuarter"), RelativeDateOperand.RelativeDate.LastQuarter);

                return lastQuarter;
            }
        }
        #endregion //LastQuarter

        #region NextYear

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of next year.
        /// </summary>
        public static SpecialFilterOperand NextYear
        {
            get
            {
                if (nextYear == null)
                    nextYear = new RelativeDateOperand("NextYear", SR.GetString("SpecialFilterOperand_NextYear"), RelativeDateOperand.RelativeDate.NextYear);

                return nextYear;
            }
        }
        #endregion //NextYear

        #region ThisYear

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of the current year.
        /// </summary>
        public static SpecialFilterOperand ThisYear
        {
            get
            {
                if (thisYear == null)
                    thisYear = new RelativeDateOperand("ThisYear", SR.GetString("SpecialFilterOperand_ThisYear"), RelativeDateOperand.RelativeDate.ThisYear);

                return thisYear;
            }
        }
        #endregion //ThisYear

        #region LastYear

        /// <summary>
        /// An operand used to determine if a specified value is between the first and last days of the previous year.
        /// </summary>
        public static SpecialFilterOperand LastYear
        {
            get
            {
                if (lastYear == null)
                    lastYear = new RelativeDateOperand("LastYear", SR.GetString("SpecialFilterOperand_LastYear"), RelativeDateOperand.RelativeDate.LastYear);

                return lastYear;
            }
        }
        #endregion //LastYear

        #region YearToDate

        /// <summary>
        /// An operand used to determine if a specified value is between the first day of the year and the current date.
        /// </summary>
        public static SpecialFilterOperand YearToDate
        {
            get
            {
                if (yearToDate == null)
                    yearToDate = new RelativeDateOperand("YearToDate", SR.GetString("SpecialFilterOperand_YearToDate"), RelativeDateOperand.RelativeDate.YearToDate);

                return yearToDate;
            }
        }
        #endregion //YearToDate

        #region Quarter1

        /// <summary>
        /// An operand used to determine if a specified value falls within the first 3 months of the calendar year.
        /// </summary>
        public static SpecialFilterOperand Quarter1
        {
            get
            {
                if (quarter1 == null)
                    quarter1 = new QuarterOperand("Quarter1", SR.GetString("SpecialFilterOperand_Quarter1"), 1);

                return quarter1;
            }
        }
        #endregion //Quarter1

        #region Quarter2

        /// <summary>
        /// An operand used to determine if a specified value falls within the second 3 months of the calendar year.
        /// </summary>
        public static SpecialFilterOperand Quarter2
        {
            get
            {
                if (quarter2 == null)
                    quarter2 = new QuarterOperand("Quarter2", SR.GetString("SpecialFilterOperand_Quarter2"), 2);

                return quarter2;
            }
        }
        #endregion //Quarter1

        #region Quarter3

        /// <summary>
        /// An operand used to determine if a specified value falls within the third 3 months of the calendar year.
        /// </summary>
        public static SpecialFilterOperand Quarter3
        {
            get
            {
                if (quarter3 == null)
                    quarter3 = new QuarterOperand("Quarter3", SR.GetString("SpecialFilterOperand_Quarter3"), 3);

                return quarter3;
            }
        }
        #endregion //Quarter3

        #region Quarter4

        /// <summary>
        /// An operand used to determine if a specified value falls within the last 3 months of the calendar year.
        /// </summary>
        public static SpecialFilterOperand Quarter4
        {
            get
            {
                if (quarter4 == null)
                    quarter4 = new QuarterOperand("Quarter4", SR.GetString("SpecialFilterOperand_Quarter4"), 4);

                return quarter4;
            }
        }
        #endregion //Quarter4

        #region January

        /// <summary>
        /// An operand representing the first month of the year.
        /// </summary>
        public static SpecialFilterOperand January
        {
            get
            {
                if (january == null)
                    january = new MonthOperand("January", SR.GetString("SpecialFilterOperand_January"), 1);

                return january;
            }
        }
        #endregion //January

        #region February

        /// <summary>
        /// An operand representing the second month of the year.
        /// </summary>
        public static SpecialFilterOperand February
        {
            get
            {
                if (february == null)
                    february = new MonthOperand("February", SR.GetString("SpecialFilterOperand_February"), 2);

                return february;
            }
        }
        #endregion //February

        #region March

        /// <summary>
        /// An operand representing the third month of the year.
        /// </summary>
        public static SpecialFilterOperand March
        {
            get
            {
                if (march == null)
                    march = new MonthOperand("March", SR.GetString("SpecialFilterOperand_March"), 3);

                return march;
            }
        }
        #endregion //March

        #region April

        /// <summary>
        /// An operand representing the fourth month of the year.
        /// </summary>
        public static SpecialFilterOperand April
        {
            get
            {
                if (april == null)
                    april = new MonthOperand("April", SR.GetString("SpecialFilterOperand_April"), 4);

                return april;
            }
        }
        #endregion //April

        #region May

        /// <summary>
        /// An operand representing the fifth month of the year.
        /// </summary>
        public static SpecialFilterOperand May
        {
            get
            {
                if (may == null)
                    may = new MonthOperand("May", SR.GetString("SpecialFilterOperand_May"), 5);

                return may;
            }
        }
        #endregion //May

        #region June

        /// <summary>
        /// An operand representing the sixth month of the year.
        /// </summary>
        public static SpecialFilterOperand June
        {
            get
            {
                if (june == null)
                    june = new MonthOperand("June", SR.GetString("SpecialFilterOperand_June"), 6);

                return june;
            }
        }
        #endregion //June

        #region July

        /// <summary>
        /// An operand representing the seventh month of the year.
        /// </summary>
        public static SpecialFilterOperand July
        {
            get
            {
                if (july == null)
                    july = new MonthOperand("July", SR.GetString("SpecialFilterOperand_July"), 7);

                return july;
            }
        }
        #endregion //July

        #region August

        /// <summary>
        /// An operand representing the eighth month of the year.
        /// </summary>
        public static SpecialFilterOperand August
        {
            get
            {
                if (august == null)
                    august = new MonthOperand("August", SR.GetString("SpecialFilterOperand_August"), 8);

                return august;
            }
        }
        #endregion //August

        #region September

        /// <summary>
        /// An operand representing the ninth month of the year.
        /// </summary>
        public static SpecialFilterOperand September
        {
            get
            {
                if (september == null)
                    september = new MonthOperand("September", SR.GetString("SpecialFilterOperand_September"), 9);

                return september;
            }
        }
        #endregion //September

        #region October

        /// <summary>
        /// An operand representing the tenth month of the year.
        /// </summary>
        public static SpecialFilterOperand October
        {
            get
            {
                if (october == null)
                    october = new MonthOperand("October", SR.GetString("SpecialFilterOperand_October"), 10);

                return october;
            }
        }
        #endregion //October

        #region November

        /// <summary>
        /// An operand representing the eleventh month of the year.
        /// </summary>
        public static SpecialFilterOperand November
        {
            get
            {
                if (november == null)
                    november = new MonthOperand("November", SR.GetString("SpecialFilterOperand_November"), 11);

                return november;
            }
        }
        #endregion //November
        
        #region December

        /// <summary>
        /// An operand representing the last month of the year.
        /// </summary>
        public static SpecialFilterOperand December
        {
            get
            {
                if (december == null)
                    december = new MonthOperand("December", SR.GetString("SpecialFilterOperand_December"), 12);

                return december;
            }
        }
        #endregion //December

        #endregion //Properties

        #region Private Methods

        #region VerifyDefaultOperandsRegistered

        private static void VerifyDefaultOperandsRegistered()
        {
            if (hasRegisteredDefaultOperands)
                return;

            // Make sure that we start with a clean slate
            RegisteredOperands.Clear();

            RegisteredOperands.Add(SpecialFilterOperands.Yesterday.Name, SpecialFilterOperands.Yesterday);
            RegisteredOperands.Add(SpecialFilterOperands.Today.Name, SpecialFilterOperands.Today);
            RegisteredOperands.Add(SpecialFilterOperands.Tomorrow.Name, SpecialFilterOperands.Tomorrow);
            RegisteredOperands.Add(SpecialFilterOperands.LastWeek.Name, SpecialFilterOperands.LastWeek);
            RegisteredOperands.Add(SpecialFilterOperands.ThisWeek.Name, SpecialFilterOperands.ThisWeek);
            RegisteredOperands.Add(SpecialFilterOperands.NextWeek.Name, SpecialFilterOperands.NextWeek);
            RegisteredOperands.Add(SpecialFilterOperands.LastMonth.Name, SpecialFilterOperands.LastMonth);
            RegisteredOperands.Add(SpecialFilterOperands.ThisMonth.Name, SpecialFilterOperands.ThisMonth);
            RegisteredOperands.Add(SpecialFilterOperands.NextMonth.Name, SpecialFilterOperands.NextMonth);
            RegisteredOperands.Add(SpecialFilterOperands.LastQuarter.Name, SpecialFilterOperands.LastQuarter);
            RegisteredOperands.Add(SpecialFilterOperands.ThisQuarter.Name, SpecialFilterOperands.ThisQuarter);
            RegisteredOperands.Add(SpecialFilterOperands.NextQuarter.Name, SpecialFilterOperands.NextQuarter);
            RegisteredOperands.Add(SpecialFilterOperands.LastYear.Name, SpecialFilterOperands.LastYear);
            RegisteredOperands.Add(SpecialFilterOperands.ThisYear.Name, SpecialFilterOperands.ThisYear);
            RegisteredOperands.Add(SpecialFilterOperands.NextYear.Name, SpecialFilterOperands.NextYear);
            RegisteredOperands.Add(SpecialFilterOperands.YearToDate.Name, SpecialFilterOperands.YearToDate);
            RegisteredOperands.Add(SpecialFilterOperands.Quarter1.Name, SpecialFilterOperands.Quarter1);
            RegisteredOperands.Add(SpecialFilterOperands.Quarter2.Name, SpecialFilterOperands.Quarter2);
            RegisteredOperands.Add(SpecialFilterOperands.Quarter3.Name, SpecialFilterOperands.Quarter3);
            RegisteredOperands.Add(SpecialFilterOperands.Quarter4.Name, SpecialFilterOperands.Quarter4);
            RegisteredOperands.Add(SpecialFilterOperands.January.Name, SpecialFilterOperands.January);
            RegisteredOperands.Add(SpecialFilterOperands.February.Name, SpecialFilterOperands.February);
            RegisteredOperands.Add(SpecialFilterOperands.March.Name, SpecialFilterOperands.March);
            RegisteredOperands.Add(SpecialFilterOperands.April.Name, SpecialFilterOperands.April);
            RegisteredOperands.Add(SpecialFilterOperands.May.Name, SpecialFilterOperands.May);
            RegisteredOperands.Add(SpecialFilterOperands.June.Name, SpecialFilterOperands.June);
            RegisteredOperands.Add(SpecialFilterOperands.July.Name, SpecialFilterOperands.July);
            RegisteredOperands.Add(SpecialFilterOperands.August.Name, SpecialFilterOperands.August);
            RegisteredOperands.Add(SpecialFilterOperands.September.Name, SpecialFilterOperands.September);
            RegisteredOperands.Add(SpecialFilterOperands.October.Name, SpecialFilterOperands.October);
            RegisteredOperands.Add(SpecialFilterOperands.November.Name, SpecialFilterOperands.November);
            RegisteredOperands.Add(SpecialFilterOperands.December.Name, SpecialFilterOperands.December);

            hasRegisteredDefaultOperands = true;
        }
        #endregion //VerifyDefaultOperandsRegistered

        #endregion //Private Methods

        #region Public Methods

        #region GetRegisteredOperand

        /// <summary>
        /// Gets the registered operand with the specified name.
        /// </summary>
        /// <param name="operandName">The name of the operand.</param>
        /// <returns>The registered operand with the specified name, if it exists.</returns>
        public static SpecialFilterOperand GetRegisteredOperand(string operandName)
        {
            if (String.IsNullOrEmpty(operandName))
                return null;

            // We don't want to actually force any of the operands to be created until we need them,
            // so make sure that we've created them now
            VerifyDefaultOperandsRegistered();

            if (RegisteredOperands.ContainsKey(operandName))
                return RegisteredOperands[operandName];

            return null;
        }
        #endregion //GetRegisteredOperand

        #region GetRegisteredOperands

        /// <summary>
        /// Gets a list of all registered operands.
        /// </summary>
        /// <returns>A list of registered operands.</returns>
        public static SpecialFilterOperand[] GetRegisteredOperands()
        {
            return GetRegisteredOperands(null);
        }

        /// <summary>
        /// Gets a list of operands that support the specified type.
        /// </summary>
        /// <param name="dataType">The type that all matching operands should support.</param>
        /// <returns>A list of matching registered operands.</returns>
        public static SpecialFilterOperand[] GetRegisteredOperands(Type dataType)
        {
            // We don't want to actually force any of the operands to be created until we need them,
            // so make sure that we've created them now
            VerifyDefaultOperandsRegistered();

            List<SpecialFilterOperand> matches = new List<SpecialFilterOperand>();
            foreach (SpecialFilterOperand operand in RegisteredOperands.Values)
            {
                if (dataType == null || operand.SupportsDataType(dataType))
                    matches.Add(operand);
            }

            return matches.ToArray();
        }
        #endregion //GetRegisteredOperands

        #region Register

        /// <summary>
        /// Registers a user-defined operand.  This method can also be used to re-register a pre-defined
        /// operand that has been previously unregistered.
        /// </summary>
        /// <param name="operand"></param>
        public static void Register(SpecialFilterOperand operand)
        {
            if (operand == null)
                return;

            // We want to make sure that we've generated the list of default operands at this point
            // in case the user tries to add an operand with the same Name as one of the defaults.
            VerifyDefaultOperandsRegistered();

            // Bail out if the operand has already been registered.
            if (RegisteredOperands.ContainsKey(operand.Name))
                return;

            RegisteredOperands.Add(operand.Name, operand);
        }
        #endregion //Register

        #region Reset

        /// <summary>
        /// Resets the list of registered operands to the default state.
        /// </summary>
        public static void Reset()
        {
            Reset(true);
        }

        /// <summary>
        /// Resets the list of registered operands to the default state.
        /// </summary>
        /// <param name="regenerateDefaults">If true, will cause all predefined operands to be recreated.</param>
        public static void Reset(bool regenerateDefaults)
        {
            // Set the flag that tells us that next time we need to verify the operands,
            // we should start over and rebuild the list.
            hasRegisteredDefaultOperands = false;

            // If we need to recreate the default operands, null them out here so
            // that they are recreated when they are needed.
            if (regenerateDefaults)
            {
                tomorrow = null;
                today = null;
                yesterday = null;
                nextWeek = null;
                thisWeek = null;
                lastWeek = null;
                nextMonth = null;
                thisMonth = null;
                lastMonth = null;
                nextQuarter = null;
                thisQuarter = null;
                lastQuarter = null;
                nextYear = null;
                thisYear = null;
                lastYear = null;
                yearToDate = null;
                quarter1 = null;
                quarter2 = null;
                quarter3 = null;
                quarter4 = null;
                january = null;
                february = null;
                march = null;
                april = null;
                may = null;
                june = null;
                july = null;
                august = null;
                september = null;
                october = null;
                november = null;
                december = null;
            }
        }
        #endregion //Reset

        #region Unregister

        /// <summary>
        /// Unregisters a user-defined operand.  This method can also be used to unregister
        /// built-in operands as well.
        /// </summary>
        /// <param name="operand"></param>
        public static void Unregister(SpecialFilterOperand operand)
        {
            if (operand == null)
                return;

            // Make sure that we've created the default list so that the user can remove one 
            // of them from the list
            VerifyDefaultOperandsRegistered();

            if (RegisteredOperands.ContainsKey(operand.Name) == false)
                return;

            RegisteredOperands.Remove(operand.Name);
        }
        #endregion //Unregister

        #endregion //Public Methods
    }
}
