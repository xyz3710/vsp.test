#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using Infragistics.Shared;
using Infragistics.Shared.Serialization;
using Infragistics.Win.Printing;
using System.Runtime.Serialization;

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// A <see cref="PrintDocument"/> used to print the contents of an associated <see cref="UltraGrid"/>
	/// </summary>
	/// <remarks>
	/// <p class="body">The <see cref="UltraGrid"/> previously exposed only methods (e.g. <see cref="UltraGrid.PrintPreview()"/> and <see cref="UltraGrid.Print()"/>) for previewing and printing 
	/// the contents of the grid respectively. These methods utilize the intrinsic .net controls for printing and previewing. The primary purpose for the <b>UltraGridPrintDocument</b> is to 
	/// allow the grid to be printed and/or previewed using custom classes and controls that can deal with a <see cref="System.Drawing.Printing.PrintDocument"/> including the 
	/// <b>UltraPrintPreviewControl</b>.</p>
	/// <p class="note"><b>Note:</b> When the <see cref="PrintDocument.Print"/> method is invoked, events on the associated <see cref="Grid"/> will be invoked just as when using the 
	/// <see cref="UltraGrid.Print()"/> method including the <see cref="UltraGrid.InitializePrint"/> and <see cref="UltraGrid.BeforePrint"/> events.</p>
	/// </remarks>
	[DefaultProperty("Grid")]
	[ToolboxBitmap(typeof(UltraGridBase), AssemblyVersion.ToolBoxBitmapFolder + "UltraGridPrintDocument.bmp")] // MRS 11/13/05
    //
    // JAS v5.3 SmartTags 7/11/05 - Added the UltraGridPrintDocumentDesigner for CLR2 only.
    //
    [Designer("Infragistics.Win.UltraWinGrid.Design.UltraGridPrintDocumentDesigner, " + AssemblyRef.Design)]
    // AS 4/8/05
	//
	
	
	[LocalizedDescription("LD_UltraGridPrintDocument")] // JAS BR06191 10/10/05
	public class UltraGridPrintDocument : 
		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		//PrintDocument,
		UltraPrintDocument,
		Infragistics.Shared.IUltraLicensedComponent
	{
		#region Member Variables

		private UltraLicense				license = null;

		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		// This is now maintained by the base class.
		//private IContainer					container;

		private UltraGrid					grid;

		// AS 1/4/05
		// Since this is a new component and most people will expect wysiwyg printing,
		// I changed the default to copy all row properties.
		//
		//private RowPropertyCategories		retainRowPropertyCategories;
		private const RowPropertyCategories	RowPropertyCategoriesDefault = RowPropertyCategories.All;
		private RowPropertyCategories		retainRowPropertyCategories = RowPropertyCategoriesDefault;

		// AS 1/28/05
		// Add a way to initialize some default values for the LogicalPageInfo.
		//
		private const int					FitWidthToPagesDefault = 0;
		private int							fitWidthToPages = FitWidthToPagesDefault;

		private ColumnClipMode				columnClipMode = ColumnClipMode.Default;

		#endregion //Member Variables

		#region Constructor
		/// <summary>
		/// Initializes a new <see cref="UltraGridPrintDocument"/>
		/// </summary>
		public UltraGridPrintDocument() : this(null)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="UltraGridPrintDocument"/>
		/// </summary>
		/// <param name="container">An IContainer representing the container of the UltraTabbedMdiManager</param>
		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		//public UltraGridPrintDocument(IContainer container)
		public UltraGridPrintDocument(IContainer container) : base(container)
		{
			try
			{
				this.license = LicenseManager.Validate( typeof(UltraGridPrintDocument), this ) as UltraLicense;
			}
			catch (System.IO.FileNotFoundException)	{}

			
		}
		#endregion //Constructor

		#region Properties

		#region Grid
		/// <summary>
		/// Returns or sets the associated <see cref="UltraGrid"/> whose rows will be printed.
		/// </summary>
		[DefaultValue(null)]
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraGridPrintDocument_P_Grid")]
		public UltraGrid Grid
		{
			get { return this.grid; }
			set
			{
				if (this.grid == value)
					return;

				// unhook any events of the grid
				if (this.grid != null)
					this.grid.Disposed -= new EventHandler(this.OnGridDisposed);

				this.grid = value;

				// hook any events of the grid
				if (this.grid != null)
					this.grid.Disposed += new EventHandler(this.OnGridDisposed);

				// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
				this.NotifyPropChange(PropertyIds.Grid);
			}
		}
		#endregion //Grid

		#region RowProperties
		/// <summary>
		/// Returns or sets flags indicating which row properties should be synchronized while printing.
		/// </summary>
		[Editor(typeof(Infragistics.Shared.FlagsEnumUITypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
		// AS 1/4/05
		//[DefaultValue(RowPropertyCategories.None)]
		[DefaultValue(RowPropertyCategories.All)]
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraGridPrintDocument_P_RowProperties")]
		public RowPropertyCategories RowProperties
		{
			get { return this.retainRowPropertyCategories; }
			set 
			{
				if (value == this.retainRowPropertyCategories)
					return;

				this.retainRowPropertyCategories = value;

				// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.RowProperties );
			}
		}
		#endregion //RowProperties

		#region ColumnClipMode
		/// <summary>
		/// Returns or sets an enumeration indicating how columns that intersect with a page are printed.
		/// </summary>
		[DefaultValue(ColumnClipMode.Default)]
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraGridPrintDocument_P_ColumnClipMode")]
		public ColumnClipMode ColumnClipMode
		{
			get { return this.columnClipMode; }
			set 
			{
				if (value == this.columnClipMode)
					return;
				
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.VerifyEnumPropertyIsValid(typeof(ColumnClipMode), (int)value);
				UltraGridPrintDocument.VerifyEnumPropertyIsValid( typeof( ColumnClipMode ), (int)value );

				this.columnClipMode = value;

				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ColumnClipMode );
			}
		}
		#endregion //ColumnClipMode

		#region FitWidthToPages
		/// <summary>
		/// Returns or sets a value that specifies the maximum number of sheets of paper that will be used to print a single logical page of the report.
		/// </summary>
		[DefaultValue(FitWidthToPagesDefault)]
		[LocalizedCategory("LC_Behavior")]
		[LocalizedDescription("LD_UltraGridPrintDocument_P_FitWidthToPages")]
		public int FitWidthToPages
		{
			get { return this.fitWidthToPages; }
			set 
			{
				if (value == this.fitWidthToPages)
					return;
				
				if (value < 0)
					throw new ArgumentOutOfRangeException("value", value, SR.GetString("LER_ValueTooLow", "FitWidthToPages", 0));

				this.fitWidthToPages = value;

				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FitWidthToPages );
			}
		}
		#endregion //FitWidthToPages

		#endregion //Properties

		#region Methods

		// MRS 10/21/05 - BR06946
		// Moved this method up to UltraPrintDocument and made it 
		// public.
		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		#region CalculateAvailablePageSize
//		internal Size CalculateAvailablePageSize(Size pageSize)
//		{
//			return base.CalculatePageBodyArea(pageSize);
//		}
		#endregion //CalculateAvailablePageSize

		#region OnGridDisposed
		private void OnGridDisposed(object sender, EventArgs e)
		{
			this.Grid = null;
		}
		#endregion //OnGridDisposed

		#region VerifyEnumPropertyIsValid
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void VerifyEnumPropertyIsValid( Type enumType, int value )
		private static void VerifyEnumPropertyIsValid( Type enumType, int value )
		{
			if (!Enum.IsDefined(enumType, value))
				throw new InvalidEnumArgumentException("value", value, enumType);
		}
		#endregion //VerifyEnumPropertyIsValid

		#endregion //Methods

		#region Base class overrides

		#region DeserializeProperty
		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		/// <summary>
		/// Invoked during runtime deserialization when deserializing from a stream using the <see cref="UltraPrintDocument.LoadFromXml(System.IO.Stream)"/> and <see cref="UltraPrintDocument.LoadFromBinary(System.IO.Stream)"/> methods.
		/// </summary>
		/// <param name="entry">Serialization entry containing the serialized property value</param>
		protected override void DeserializeProperty( SerializationEntry entry )
		{
			switch(entry.Name)
			{
				case "FitWidthToPages":
					this.fitWidthToPages = (int)Utils.DeserializeProperty(entry, typeof(int), this.fitWidthToPages);
					break;
				case "ColumnClipMode":
					this.columnClipMode = (ColumnClipMode)Utils.DeserializeProperty(entry, typeof(ColumnClipMode), this.columnClipMode);
					break;
				case "RowProperties":
					this.retainRowPropertyCategories = (RowPropertyCategories)Utils.DeserializeProperty(entry, typeof(RowPropertyCategories), this.retainRowPropertyCategories);
					break;
				default:
					base.DeserializeProperty(entry);
					break;
			}
		}
		#endregion //DeserializeProperty

		#region Dispose
		/// <summary>
        /// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.license != null)
				{
					this.license.Dispose();
					this.license = null;
				}
			}

			base.Dispose(disposing);
		}
		#endregion // Dispose

		#region GetObjectData
		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		/// <summary>
		/// Serializes the properties and subobjects of the print document into the specified <see cref="ObjectStreamer"/>
		/// </summary>
		/// <param name="streamer">Object into which the properties should be serialized</param>
		protected override void GetObjectData( ObjectStreamer streamer )
		{
			base.GetObjectData(streamer);

			System.Collections.IDictionary dictionary = streamer.Dictionary;

			if (this.fitWidthToPages != FitWidthToPagesDefault)
				dictionary.Add( "FitWidthToPages", this.fitWidthToPages);

			if (this.columnClipMode != ColumnClipMode.Default)
				dictionary.Add( "ColumnClipMode", this.columnClipMode);

			if (this.retainRowPropertyCategories != RowPropertyCategoriesDefault)
				dictionary.Add( "RowProperties", this.retainRowPropertyCategories);
		}
		#endregion //GetObjectData

		#region OnBeginPrint
		/// <summary>
		/// Used to invoke the <b>BeginPrint</b> event when a print operation has started.
		/// </summary>
		/// <param name="e">Event arguments</param>
		protected override void OnBeginPrint(System.Drawing.Printing.PrintEventArgs e)
		{
			// raise the event to any listener first...
			base.OnBeginPrint(e);

			// if it wasn't cancelled...
			if (!e.Cancel)
			{
				// if the grid 
				if (this.Grid == null || this.Grid.Rows.Count == 0)
					e.Cancel = true;
				else
				{
                    // MRS 6/8/07 - BR23420
                    if (this.Grid.PrintManager != null &&
                        this.Grid.PrintManager.PrintDocument == this)
                        return;

					// invoke a helper method to do what we used to do 
					// to prepare for a print/printpreview method call in the grid
					if (!this.Grid.PrintDocumentHelper(this))
						e.Cancel = true;
					else
					{
						PrintManager printManager = this.Grid.PrintManager;

						Debug.Assert(printManager != null, "Unable to access the grid's print manager");

						if (printManager == null)
							e.Cancel = true;
						else
						{
							// AS 11/9/04
							// we were originally going to set this 
							// flag to prevent the BeforePrint event from
							// being invoked but we talked about it
							// and decided to let the event get raised.
							//printManager.CalledFromPrint = false;

							// call BeginPrint on PrintManager to process
							// the start of the print operation
							printManager.BeginPrint(this, e);
						}
					}

					// if its cancelled, dispose the grid's print info
					
					
					if (e.Cancel)
					{
						// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
						//this.Grid.DisposePrintHelper();
						this.OnPrintEnded();
					}
				}
			}
		}
		#endregion //OnBeginPrint

		#region OnEndPrint
		
		#endregion //OnEndPrint

		#region OnPagePrinted
		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		/// <summary>
		/// Raises the <see cref="UltraPrintDocument.PagePrinted"/> event.
		/// </summary>
		/// <param name="e">A <see cref="PagePrintedEventArgs"/> that provides data for the event.</param>
		/// <remarks>
		/// <p class="body">Raising an event invokes the event handler through a delegate.</p>
		/// <p class="body">The <b>OnPagePrinted</b> method also allows derived classes to handle the event without attaching a delegate. This is the preferred technique for handling the event in a derived class.</p>
		/// <p class="note">Notes to Inheritors:  When overriding <b>OnPagePrinted</b> in a derived class, be sure to call the base class's <b>OnPagePrinted</b> method so that registered delegates receive the event.</p>
		/// </remarks>
		/// <seealso cref="PagePrintedEventArgs"/>
		/// <seealso cref="UltraPrintDocument.PagePrinted"/>
		protected override void OnPagePrinted(PagePrintedEventArgs e)
		{
			// call the base implementation first
			base.OnPagePrinted(e);

			// If the page was not printed (i.e. a page was being inserted), we may need to 
			// get this value from the print manager.
			//
			if (!e.HasMorePages && this.Grid != null)
				e.HasMorePages = this.Grid.PrintManager.HasMorePages;
		}
		#endregion //OnPagePrinted

		#region OnPrintEnded
		/// <summary>
		/// Invoked when the print operation has ended.
		/// </summary>
		protected override void OnPrintEnded()
		{
			base.OnPrintEnded();

			// post print cleanup
			if (this.grid != null)
				this.grid.DisposePrintHelper();
		}
		#endregion //OnPrintEnded

		#region Reset
		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		/// <summary>
		/// Invoked when the properties of the component should be reset to their default values.
		/// </summary>
		/// <param name="deserializing">True if the component is being deserialized from a stream</param>
		public override void Reset(bool deserializing)
		{
			base.Reset(deserializing);

			// during deserialization, do not reset the component properties
			if (!deserializing)
			{
				if (this.grid != null)
					this.grid = null;
			}

			this.columnClipMode = ColumnClipMode.Default;
			this.fitWidthToPages = FitWidthToPagesDefault;
			this.retainRowPropertyCategories = RowPropertyCategoriesDefault;
		}
		#endregion //Reset

		#region SupportsVariableSectionSize
		// AS 1/28/05 UltraGridPrintDocument/UltraPrintDocument support.
		/// <summary>
		/// The <see cref="UltraGridPrintDocument"/> does not support the modification of the header/footer height, page border style, etc. once a print operation has begun.
		/// </summary>
		protected override bool SupportsVariableSectionSize
		{
			get { return false; }
		}
		#endregion //SupportsVariableSectionSize

		// MD 1/12/09 - Printing total number of pages
		#region TotalNumberOfPages

		/// <summary>
		/// Gets the total number of pages to be printed or null if that value cannot be determined.
		/// </summary>
		protected override int? TotalNumberOfPages
		{
			get
			{
				if ( this.Grid != null && this.Grid.PrintManager != null )
					return this.Grid.PrintManager.TotalNumberOfPages;

				return base.TotalNumberOfPages;
			}
		} 

		#endregion TotalNumberOfPages

		#endregion //Base class overrides

		#region Interfaces

		#region Licensing interface

		

		/// <summary>
		/// Returns the cached license for the control
		/// </summary>
		UltraLicense IUltraLicensedComponent.License
		{ 
			get { return this.license; } 
		}

		#endregion

		#endregion ////Interfaces
	}
}
