#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinDataSource;
using System.Collections.Generic;

// SSP 6/14/05 - NAS 5.3 Column Chooser Feature
// Added UltraGridColumnChooser class.
// 

namespace Infragistics.Win.UltraWinGrid
{
	#region UltraGridColumnChooser

	/// <summary>
	/// This control allows the user to select which columns to display in an UltraGrid.
	/// It displays the list of columns that the user can hide or unhide from the UltraGrid.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// <b>UltraGridColumnChooser</b> allows the user to select which columns to display in 
	/// an UltraGrid. It displays the list of columns that the user can hide or unhide from 
	/// the UltraGrid.
	/// </p>
	/// <p class="body">
	/// <see cref="ColumnChooserDialog"/> embeds this control inside of it. Typically you 
	/// would display the <b>ColumnChooserDialog</b> to make use of this control. 
	/// Use the <see cref="UltraGridBase.ShowColumnChooser()"/> method of the UltraGrid to 
	/// display the column chooser dialog. You can also embed this control inside your 
	/// own custom dialog.
	/// </p>
	/// <p class="body">
	/// To associate an UltraGrid with a UltraGridColumnChooser set its 
	/// <see cref="UltraGridColumnChooser.SourceGrid"/> property. UltraGridColumnChooser will
	/// display the columns of that grid. If your grid has multiple bands then you
	/// can specify a particular band whose columns to display by using
	/// <see cref="UltraGridColumnChooser.CurrentBand"/> property. The UltraGridColumnChooser
	/// can also display columns of multiple bands at the same time. It also exposes an user 
	/// interface that lets the user select the band whose columns to display. These two
	/// aspects can be controlled using the <see cref="UltraGridColumnChooser.MultipleBandSupport"/>
	/// property.
	/// </p>
	/// <p class="body">
	/// <b>UltraGridColumnChooser</b> has two main modes of operation. 
	/// <see cref="UltraGridColumnChooser.Style"/> property specifies which mode is used.
	/// <p>
	/// <b>HiddenColumnsOnly</b> mode displays the columns that are
	/// currently hidden in the UltraGrid. In this mode the user can drag the columns between 
	/// the UltraGrid and the Column Chooser to hide or unhide them.
	/// </p>  
	/// <p>
	/// <b>AllColumnsWithCheckBoxes</b> mode displays all the columns,
	/// hidden or otherwise, in the column chooser. There is a checkbox next to each column
	/// which the user can use to hide or unhide columns. This mode also offers the same drag
	/// and drop capability as the <b>HiddenColumnsOnly</b> option. 
	/// </p>  
	/// <p>
	/// <b>AllColumnsAndChildBandsWithCheckBoxes</b> mode behaves
	/// the same as <b>AllColumnsWithCheckBoxes</b> except that it also lets the user hide or unhide
	/// child bands. Each child band displays a check box next to it, just like the columns that
	/// the user can use to hide or unhide the child band. <b>AllColumnsAndChildBandsWithCheckBoxes</b> 
	/// is the default column chooser style.
	/// </p>  
	/// </p>
	/// <p class="body">
	/// Various appearance related aspects can be controlled by the 
	/// <see cref="UltraGridColumnChooser.DisplayLayout"/> property. For example, the BackColor
	/// of the column chooser can be set using the UltraGridColumnChooser.DisplayLayout.Appearance.BackColor.
	/// By default the column chooser syncrhonizes its appearance with the source grid. You can prevent
	/// the column chooser from doing this by setting the 
	/// <see cref="UltraGridColumnChooser.SyncLookWithSourceGrid"/> property to <b>false</b>.
	/// This way you can specify appearance that's different from the source grid.
	/// </p>
	/// <seealso cref="ColumnChooserDialog"/> <seealso cref="UltraGridBase.ShowColumnChooser()"/>
	/// <seealso cref="UltraGridColumnChooser.SourceGrid"/> <seealso cref="UltraGridColumnChooser.CurrentBand"/>
	/// <seealso cref="UltraGridColumnChooser.MultipleBandSupport"/> <seealso cref="UltraGridColumnChooser.Style"/>
	/// </remarks>
	
		
	[ToolboxBitmap(typeof(UltraGridBase), AssemblyVersion.ToolBoxBitmapFolder + "UltraGridColumnChooser.bmp")] // MRS 11/13/05
	[ DefaultProperty( "SourceGrid" ) ]
	[ Designer( typeof( Infragistics.Win.UltraControlDesigner ) ) ]
	[ LocalizedDescription("LD_UltraGridColumnChooser") ] // JAS BR06191 10/10/05
	public class UltraGridColumnChooser : Control, Infragistics.Shared.IUltraLicensedComponent
		// SSP 2/26/07 BR18838
		// 
		, ISupportInitialize
	{
		#region Private Vars

		// License is required for the About dialog.
		// 
		private UltraLicense license = null;

		internal const string VALUE_COLUMN = "Value";
		internal const string VISIBLE_COLUMN = "Visible";

		private UltraGridBase sourceGrid = null;
		private UltraGridBase lastHookedIntoGrid = null;
		private UltraGridBand currentBand = null;
		private string serializedCurrentBandKey = null;
		private bool needsReinitialization = true;
		// SSP 7/22/05 BR05137
		// 
		private bool syncLookWithSourceGrid = true;
		// SSP 8/12/05 BR05531
		// 
		private bool needsToApplySourceGridAppearances = true;

		private ColumnChooserStyle style = ColumnChooserStyle.AllColumnsAndChildBandsWithCheckBoxes;
		private MultipleBandSupport multipleBandSupport = MultipleBandSupport.ShowBandSelectionUI;

		private Infragistics.Win.UltraWinGrid.UltraGrid displayGrid;
		private System.Windows.Forms.ComboBox comboBoxBandSelector;

		// SSP 8/28/06 BR14825
		// Added ColumnDisplayOrder on the UltraGridColumnChooser.
		// 
		private const ColumnDisplayOrder DEFAULT_COLUMN_DISPLAY_ORDER = ColumnDisplayOrder.Alphabetical;
		private ColumnDisplayOrder columnDisplayOrder = DEFAULT_COLUMN_DISPLAY_ORDER;

		#endregion // Private Vars

		#region InitializeComponent

		private void InitializeComponent()
		{
			this.displayGrid = new Infragistics.Win.UltraWinGrid.ColumnChooserGrid();
			this.comboBoxBandSelector = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.displayGrid)).BeginInit();
			// 
			// displayGrid
			// 
			this.displayGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.displayGrid.Location = new System.Drawing.Point(17, 17);
			this.displayGrid.Name = "displayGrid";
			this.displayGrid.SyncWithCurrencyManager = false;
			this.displayGrid.TabIndex = 0;
			this.displayGrid.Text = "ultraGrid1";
			this.displayGrid.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.displayGrid_CellChange);
			this.displayGrid.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.displayGrid_InitializeLayout);
			this.displayGrid.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.displayGrid_InitializeRow);
			this.displayGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.displayGrid_KeyDown);
			this.displayGrid.Click += new System.EventHandler(this.displayGrid_Click);
			// 
			// comboBoxBandSelector
			// 
			this.comboBoxBandSelector.Dock = System.Windows.Forms.DockStyle.Top;
			this.comboBoxBandSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxBandSelector.Location = new System.Drawing.Point(123, 17);
			this.comboBoxBandSelector.Name = "comboBoxBandSelector";
			this.comboBoxBandSelector.TabIndex = 0;
			this.comboBoxBandSelector.Visible = false;
			this.comboBoxBandSelector.SelectedIndexChanged += new System.EventHandler(this.comboBoxBandSelector_SelectedIndexChanged);
			((System.ComponentModel.ISupportInitialize)(this.displayGrid)).EndInit();

		}

		#endregion // InitializeComponent

		#region Constructor		

		/// <summary>
		/// Constructor.
		/// </summary>
		public UltraGridColumnChooser()
		{
			// This is required for the About dialog.
			// 
			try
			{
				this.license = LicenseManager.Validate( typeof(UltraGridColumnChooser), this ) as UltraLicense;
			}
			catch (System.IO.FileNotFoundException)	{}

			this.InitializeComponent( );

			if ( ! this.Controls.Contains( this.DisplayGrid ) )
				this.Controls.Add( this.DisplayGrid );

			if ( ! this.Controls.Contains( this.comboBoxBandSelector ) )
				this.Controls.Add( this.comboBoxBandSelector );
			
			this.InitializeDisplayGrid( );

			this.BindingContext = new BindingContext( );

			this.displayGrid.CreationFilter = new ColumnChooserGridCreationFilter( this );
			this.displayGrid.DrawFilter = new ColumnChooserGridDrawFilter( this );
		}


		#endregion // Constructor

		#region Event Handlers

		#region sourceGrid_PropertyChanged

		private void sourceGrid_PropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			if ( null == this.SourceGrid || this.SourceGrid.IsDisposed
				|| this.IsDisposed || null == this.DisplayGrid || this.DisplayGrid.IsDisposed )
			{
				this.SourceGrid = null;
				Debug.Assert( false );
				return;
			}

			if ( this.SyncLookWithSourceGrid )
			{
				if ( null != e.ChangeInfo.FindSource( typeof( AppearanceBase ) ) 
					|| null != e.ChangeInfo.FindSource( typeof( Appearance ) ) 
					|| null != e.ChangeInfo.FindSource( typeof( LinkedAppearance ) ) 
					|| null != e.ChangeInfo.FindSource( typeof( UltraGridOverride ) ) )
				{
					// SSP 8/12/05 BR05531
					// 
					//this.SetNeedsReinitialization( );
					this.SetNeedsToApplySourceGridAppearances( );

					return;
				}
			}

			Type[] sourceTypes = new Type[]
				{
					typeof( UltraGridColumn ),
					typeof( ColumnsCollection ),
					typeof( UltraGridBand )
				};

			PropertyIds[] ids = new PropertyIds[]
				{

					// For refreshing display when a column's hidden state changes.
					// 
					PropertyIds.Hidden,

					// A column is grouped by.
					// 
					PropertyIds.IsGroupByColumn,
					PropertyIds.SortIndicator,

					// A column's hidden state may change as a result of change in these
					// properties.
					// 
					PropertyIds.HiddenWhenGroupBy,
					PropertyIds.GroupByColumnsHidden,

					// For refreshing display when columns are added or removed.
					// 
					PropertyIds.Add,
					PropertyIds.Remove,					
					PropertyIds.Clear,
					PropertyIds.ExcludeFromColumnChooser
				};

            // MRS 4/21/2009 - TFS16756
            // Made this into a List<> so I can conditionally add an item.
            // -----------------------------------------------------------------------
            //
            #region Old Code
            //PropertyIds[] ids_RequiresReInitialization = new PropertyIds[]
            //    {
            //        // For refreshing display when columns are added or removed.
            //        // 
            //        PropertyIds.Add,
            //        PropertyIds.Remove,					
            //        PropertyIds.Clear,
            //        PropertyIds.ExcludeFromColumnChooser
            //    };
            #endregion //Old Code
            //
            List<PropertyIds> ids_RequiresReInitialization_List = new List<PropertyIds>(4);
            ids_RequiresReInitialization_List.Add(PropertyIds.Add);
            ids_RequiresReInitialization_List.Add(PropertyIds.Remove);
            ids_RequiresReInitialization_List.Add(PropertyIds.Clear);
            ids_RequiresReInitialization_List.Add(PropertyIds.ExcludeFromColumnChooser);

            if (this.ColumnDisplayOrder == ColumnDisplayOrder.SameAsGrid)
                ids_RequiresReInitialization_List.Add(PropertyIds.VisiblePosition);

            PropertyIds[] ids_RequiresReInitialization = ids_RequiresReInitialization_List.ToArray();
            // -----------------------------------------------------------------------


			bool requiresReInitialization = false;
			bool idFound = false;
			bool sourceFound = false;

			foreach ( Type type in sourceTypes )
			{
				if ( null != e.ChangeInfo.FindSource( type ) )
				{
					sourceFound = true; 
					break;
				}
			}

			if ( sourceFound )
			{
				foreach ( PropertyIds id in ids_RequiresReInitialization )
				{
					if ( null != e.ChangeInfo.FindPropId( id ) )
					{
						idFound = true; 
						requiresReInitialization = true;
						break;
					}
				}

				if ( ! idFound )
				{
					foreach ( PropertyIds id in ids )
					{
						if ( null != e.ChangeInfo.FindPropId( id ) )
						{
							idFound = true; 
							break;
						}
					}
				}
			}

			// SSP 11/18/05 BR07802
			// Also re-initialize if the datasource on the source grid changes.
			// 
			// --------------------------------------------------------------------------
			PropertyIds[] ids_StandAlone_RequiresReinitialization = new PropertyIds[] 
				{
					PropertyIds.DataSource,
					PropertyIds.DataMember
				};

			foreach ( PropertyIds id in ids_StandAlone_RequiresReinitialization )
			{
				if ( null != e.ChangeInfo.FindPropId( id ) )
				{
					needsReinitialization = true;
					break;
				}
			}
			// --------------------------------------------------------------------------

			// SSP 11/18/05 BR07802
			// 
			//if ( idFound && sourceFound )
			if ( idFound && sourceFound || needsReinitialization )
			{
				if ( ColumnChooserStyle.HiddenColumnsOnly == this.Style || requiresReInitialization )
				{
					this.SetNeedsReinitialization( );
				}
				else
				{
					// Fire InitializeRow on every row to update the VISIBLE_COLUMN cell values to the new 
					// hidden states of the column.
					// 
					this.DisplayGrid.Rows.Refresh( RefreshRow.FireInitializeRow, true );

					this.DisplayGrid.DisplayLayout.BumpCellChildElementsCacheVersion( );
					this.DisplayGrid.DisplayLayout.UIElement.DirtyChildElements( );
				}
			}
			else
			{
				this.DisplayGrid.DisplayLayout.UIElement.DirtyChildElements( );
			}
		}

		#endregion // sourceGrid_PropertyChanged

		#region sourceGrid_Disposed

		private void sourceGrid_Disposed( object sender, EventArgs e )
		{
			this.SourceGrid = null;
		}

		#endregion // sourceGrid_Disposed

		#region displayGrid_InitializeRow
		
		private void displayGrid_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
		{
			// Initialize the cell values of the check box cells.
			// 

			UltraGridCell visibleColumnCell = e.Row.Cells[ UltraGridColumnChooser.VISIBLE_COLUMN ];

			object val = ColumnBandHolder.GetWrappedValue( e.Row.Cells[ UltraGridColumnChooser.VALUE_COLUMN ] );
			if ( val is UltraGridColumn )
			{
				UltraGridColumn column = (UltraGridColumn)val;
				visibleColumnCell.Value = ! column.Hidden;

				// If the band is hidden then disable all it's columns.
				// 
				visibleColumnCell.Row.Activation = column.Band.HiddenResolved ? Activation.Disabled : Activation.AllowEdit;
			}
			else if ( val is UltraGridBand )
			{
				UltraGridBand band = (UltraGridBand)val;
				visibleColumnCell.Value = ! band.HiddenResolved;

				// Hide the check box cell for bands if their visible states can not be changed.
				// 
				visibleColumnCell.Hidden = band.IsRootBand 
					|| ColumnChooserStyle.AllColumnsAndChildBandsWithCheckBoxes != this.Style;

				// SSP 12/9/05 BR07952
				// If the an ancestor band is hidden then disable the checkboxes of the child 
				// bands and their columns.
				// 
				visibleColumnCell.Row.Activation = null != band.ParentBand && band.ParentBand.HiddenResolved
					? Activation.Disabled : Activation.AllowEdit;

				// If a single band is currently being displayed and if the band name is already 
				// being displayed in the band selector combo and the band doesn't have a checkbox 
				// next to it then hide the band header. Displaying it in the band selector and 
				// then displaying it as a header is redundant.
				// 
				e.Row.Hidden = 
					// If there is no checkbox.
					visibleColumnCell.Hidden 
					// If a single band is being displayed
					&& null != this.CurrentBand 
					// If the band selector combo is visible.
					&& ( null != this.comboBoxBandSelector && this.comboBoxBandSelector.Visible );
			}
		}

		#endregion // displayGrid_InitializeRow

		#region displayGrid_InitializeLayout

		private void displayGrid_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
		{
			this.InitializeDisplayGridLayout( );
		}

		#endregion // displayGrid_InitializeLayout

		#region ToggleVisibleCellCheckState

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void ToggleVisibleCellCheckState( UltraGridCell cell )
		private static void ToggleVisibleCellCheckState( UltraGridCell cell )
		{
			if ( UltraGridColumnChooser.VISIBLE_COLUMN == cell.Column.Key )
			{
				EmbeddableEditorBase editor = cell.EditorResolved;
				object visibleVal = null != editor && editor.IsInEditMode ? editor.Value : cell.Value;
				bool isVisible = visibleVal is bool && (bool)visibleVal;
				bool newVisibleState = ! isVisible;

				object val = ColumnBandHolder.GetWrappedValue( cell.Row.Cells[ UltraGridColumnChooser.VALUE_COLUMN ] );
				if ( val is UltraGridColumn )
				{
					UltraGridColumn column = (UltraGridColumn)val;
					column.Layout.Grid.ColumnChooser_SetHidden( column, ! newVisibleState );
				}
				else if ( val is UltraGridBand )
				{
					UltraGridBand band = (UltraGridBand)val;
					band.Layout.Grid.ColumnChooser_SetHidden( band, ! newVisibleState );
				}

				if ( cell.IsInEditMode )
					cell.ExitEditMode( );

				// Fire the initialize row on all the rows so we can disable the cells of the
				// band that may have just got hidden.
				// 
				cell.Layout.Rows.Refresh( RefreshRow.FireInitializeRow, true );
			}
		}

		#endregion // ToggleVisibleCellCheckState

		#region displayGrid_CellChange

		private void displayGrid_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
		{
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//this.ToggleVisibleCellCheckState( e.Cell );
			UltraGridColumnChooser.ToggleVisibleCellCheckState( e.Cell );
		}

		#endregion // displayGrid_CellChange

		#region displayGrid_KeyDown

		private void displayGrid_KeyDown( object sender, KeyEventArgs e )
		{
			// To fascilitate proper keyboard navigation and better look & feel, we are not 
			// letting the cells go into edit mode or even be activated. The whole row is active
			// at a time. When Space key is pressed, we have to toggle the checkbox if the check
			// box cells are visible.
			//

			if ( Keys.Space == e.KeyCode && ! e.Control && ! e.Alt && null != this.displayGrid )
			{
				UltraGridRow activeRow = this.displayGrid.ActiveRow;
				UltraGridCell activeCell = this.displayGrid.ActiveCell;
				if ( null != activeRow && ( null == activeCell || ! activeCell.IsInEditMode ) )
				{
					UltraGridCell visibleCell = activeRow.Cells.Exists( VISIBLE_COLUMN ) ? activeRow.Cells[ VISIBLE_COLUMN ] : null;

					if ( null != visibleCell && visibleCell.CanBeModified && ! visibleCell.InternalHidden )
					{
						// Toggle the cell value.
						// 
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.ToggleVisibleCellCheckState( visibleCell );
						UltraGridColumnChooser.ToggleVisibleCellCheckState( visibleCell );

						e.Handled = true;
					}
				}
			}
		}

		#endregion // displayGrid_KeyDown

		#region displayGrid_Click

		private void displayGrid_Click( object sender, EventArgs e )
		{
			if ( null == this.DisplayGrid || null == this.DisplayGrid.DisplayLayout )
				return;

			// To fascilitate proper keyboard navigation and better look & feel, we are not 
			// letting the cells go into edit mode or even be activated. The whole row is active
			// at a time. When mouse is clicked on the checkbox cell we need to toggle the 
			// hidden state of the cell.
			//

			// Find the checkbox cell under the mouse.
			// 
			Point mousePos = this.DisplayGrid.PointToClient( Control.MousePosition );
			UIElement elem = this.DisplayGrid.DisplayLayout.UIElement;
			elem = null != elem ? elem.ElementFromPoint( mousePos ) : null;
			elem = null != elem ? elem.GetAncestor( typeof( CellUIElementBase ) ) : null;
			UltraGridCell cell = null != elem ? (UltraGridCell)elem.GetContext( typeof( UltraGridCell ) ) : null;

			// Toggle it if the cell is the check box cell and it's not in edit mode.
			//
			if ( null != cell && VISIBLE_COLUMN == cell.Column.Key && ! cell.IsInEditMode 
				// SSP 12/9/05 BR07953
				// Don't allow checking/unchecking the checkbox if the cell is disabled.
				// 
				&& cell.CanBeModified && ( null == elem || elem.Enabled ) )
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.ToggleVisibleCellCheckState( cell );
				UltraGridColumnChooser.ToggleVisibleCellCheckState( cell );
		}

		#endregion // displayGrid_Click

		#region comboBoxBandSelector_SelectedIndexChanged

		private void comboBoxBandSelector_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ColumnBandHolder item = this.comboBoxBandSelector.SelectedItem as ColumnBandHolder;
			this.CurrentBand = null != item ? item.Value as UltraGridBand : null;
		}

		#endregion // comboBoxBandSelector_SelectedIndexChanged

		#endregion // Event Handlers

		#region Private/Internal Methods/Properties

		#region InitializeAppearancesFromSourceGrid

		internal void InitializeAppearancesFromSourceGrid( )
		{
			UltraGridLayout layout = this.DisplayGrid.DisplayLayout;
			UltraGridLayout sourceGridLayout = null != this.SourceGrid ? this.SourceGrid.DisplayLayout : null;
			if ( null == layout || null == sourceGridLayout || layout.Disposed ||  sourceGridLayout.Disposed )
				return;

			// Make the headers look the same as the source grid.
			// 
			if ( this.SyncLookWithSourceGrid )
			{
				// AS 3/20/06 AppStyling
				//layout.Grid.SupportThemes = this.SourceGrid.SupportThemes;
				layout.Grid.UseOsThemes = this.SourceGrid.UseOsThemes;
				layout.Appearance = (AppearanceBase)sourceGridLayout.Appearance.Clone( );
				layout.Override.HeaderStyle = sourceGridLayout.Override.HeaderStyle;

				// SSP 11/4/05 BR07555
				// Added TextRenderingMode property to UltraGrid and also the EmbeddableEditorOwnerBase.
				// Also added IsPirnting on the EmbeddableEditorOwnerBase.
				// 
				layout.Grid.TextRenderingMode = this.SourceGrid.TextRenderingMode;

				// SSP 8/12/05 BR05531
				// 
				this.needsToApplySourceGridAppearances = false;
			}

			// SSP 10/19/05 BR06671
			// Copy over the caption of the source grid to the display grid in case the
			// CaptionVisible of the display grid's display layout is set to true.
			// 
			this.DisplayGrid.Text = this.SourceGrid.Text;
		}

		#endregion // InitializeAppearancesFromSourceGrid

		#region InitializeDisplayGrid

		private void InitializeDisplayGrid( )
		{
			UltraGridLayout layout = this.DisplayGrid.DisplayLayout;
			if ( null == layout || layout.Disposed )
				return;

			// Set some properties on the display grid to make it suitable for the column chooser.
			// 
			layout.CaptionVisible = DefaultableBoolean.False;
			layout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;			
			layout.Override.ExpansionIndicator = Infragistics.Win.UltraWinGrid.ShowExpansionIndicator.Never;
			layout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.Select;
			layout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
			layout.RowConnectorStyle = Infragistics.Win.UltraWinGrid.RowConnectorStyle.None;
			layout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
			layout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
			layout.Override.AllowColMoving = AllowColMoving.NotAllowed;
			layout.Override.AllowColSizing = AllowColSizing.None;
			layout.Override.AllowRowLayoutCellSizing = RowLayoutSizing.None;
			layout.Override.AllowRowLayoutLabelSizing = RowLayoutSizing.None;
			layout.Override.RowSizing = RowSizing.AutoFixed;
			layout.Override.CellPadding = 2;
			layout.MaxColScrollRegions = 1;
			layout.MaxRowScrollRegions = 1;	
			layout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.None;
			layout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.None;
			layout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.None;
			layout.Override.CellClickAction = CellClickAction.RowSelect;
			this.SetupKeyActionMappings( );
		}

		#endregion // InitializeDisplayGrid

		#region FindKeyActionMapping

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private GridKeyActionMapping FindKeyActionMapping( GridKeyActionMappings mappings, UltraGridAction action, Keys key )
		private static GridKeyActionMapping FindKeyActionMapping( GridKeyActionMappings mappings, UltraGridAction action, Keys key )
		{
			foreach ( GridKeyActionMapping mapping in mappings )
			{
				if ( action == mapping.ActionCode && key == mapping.KeyCode )
					return mapping;
			}

			return null;
		}

		#endregion // FindKeyActionMapping

		#region SetupKeyActionMappings

		private void SetupKeyActionMappings( )
		{
			if ( null == this.DisplayGrid || this.DisplayGrid.IsDisposed )
				return;

			GridKeyActionMappings mappings = this.DisplayGrid.KeyActionMappings;

			// Remove all key actions other than the following.
			// 
			UltraGridAction[] actions = new UltraGridAction[]
			{
				UltraGridAction.NextRowByTab,
				UltraGridAction.PrevRowByTab,
				UltraGridAction.FirstRowInGrid,
				UltraGridAction.LastRowInGrid,
			};

			// Remove all key actions other than ones with the following keys.
			// 
			Keys[] keys = new Keys[] { Keys.Tab };

			GridKeyActionMapping mapping;

			for ( int i = mappings.Count - 1; i >= 0; i-- )
			{
				mapping = mappings[i];

                if ( Array.IndexOf( actions, mapping.ActionCode ) >= 0 )
					continue;

				if ( Array.IndexOf( keys, mapping.KeyCode ) >= 0 )
					continue;

				mappings.Remove( i );
			}
			
			// Add two mappings that do the same as PrefRowByTab and NextRowByTab however with Up and Down
			// arrow keys.
			// 
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//mapping = this.FindKeyActionMapping( mappings, UltraGridAction.PrevRowByTab, Keys.Tab );
			mapping = UltraGridColumnChooser.FindKeyActionMapping( mappings, UltraGridAction.PrevRowByTab, Keys.Tab );

			if ( null != mapping )
				mappings.Add( new GridKeyActionMapping( Keys.Up, mapping.ActionCode, mapping.StateDisallowed, mapping.StateRequired, SpecialKeys.All, 0 ) );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//mapping = this.FindKeyActionMapping( mappings, UltraGridAction.NextRowByTab, Keys.Tab );
			mapping = UltraGridColumnChooser.FindKeyActionMapping( mappings, UltraGridAction.NextRowByTab, Keys.Tab );

			if ( null != mapping )
				mappings.Add( new GridKeyActionMapping( Keys.Down, mapping.ActionCode, mapping.StateDisallowed, mapping.StateRequired, SpecialKeys.All, 0 ) );
		}

		#endregion // SetupKeyActionMappings

		#region InitializeDisplayGridLayout

		private void InitializeDisplayGridLayout( )
		{
			UltraGridLayout layout = this.DisplayGrid.DisplayLayout;
			UltraGridLayout sourceGridLayout = null != this.SourceGrid ? this.SourceGrid.DisplayLayout : null;
			if ( null == layout || null == sourceGridLayout || layout.Disposed ||  sourceGridLayout.Disposed )
				return;

			this.InitializeAppearancesFromSourceGrid( );

			foreach ( UltraGridBand band in layout.SortedBands )
			{
				band.HeaderVisible = false;

                // MRS - NAS 9.1 - Groups in RowLayout
				//band.UseRowLayout = true;
                band.RowLayoutStyle = RowLayoutStyle.ColumnLayout;

				band.RowLayoutLabelStyle = RowLayoutLabelStyle.WithCellData;

				if ( band.IsRootBand )
					band.Indentation = 0;

				// Make the headers look the same as the source grid.
				// 
				UltraGridBand sourceBand = sourceGridLayout.SortedBands.Exists( band.Key ) 
					? sourceGridLayout.SortedBands[ band.Key ] : null;
				if ( null != sourceBand )
					band.Override.HeaderStyle = sourceBand.Override.HeaderStyle;

				foreach ( UltraGridColumn column in band.Columns )
				{
					if ( UltraGridColumnChooser.VALUE_COLUMN != column.Key
						&& ( ColumnChooserStyle.AllColumnsWithCheckBoxes != this.Style 
							 && ColumnChooserStyle.AllColumnsAndChildBandsWithCheckBoxes != this.Style 
							 || UltraGridColumnChooser.VISIBLE_COLUMN != column.Key ) )
						column.Hidden = true;

					if ( UltraGridColumnChooser.VISIBLE_COLUMN == column.Key )
					{
						column.Header.VisiblePosition = 0;
						column.RowLayoutColumnInfo.LabelPosition = LabelPosition.None;
						column.RowLayoutColumnInfo.OriginX = 0;
						column.RowLayoutColumnInfo.SpanX = 2;
					}
					else if ( UltraGridColumnChooser.VALUE_COLUMN == column.Key )
					{
						band.RowLayoutLabelPosition = LabelPosition.None;
						column.RowLayoutColumnInfo.OriginX = 2;
						column.RowLayoutColumnInfo.SpanX = RowLayoutColumnInfo.Remainder;

						// When the dialog is resized, resize the columns and not the checkboxes
						// if they are visible.
						//
						column.RowLayoutColumnInfo.WeightX = 1.0f;

						column.TabStop = false;
					}
				}
			}
		}

		#endregion // InitializeDisplayGridLayout
		
		#region HasNonExcludedDescendantBand

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private bool HasNonExcludedDescendantBand( UltraGridBand[] bands, UltraGridBand parentBand )
		private static bool HasNonExcludedDescendantBand( UltraGridBand[] bands, UltraGridBand parentBand )
		{
			foreach ( UltraGridBand band in bands )
			{
				if ( band.IsDescendantOfBand( parentBand ) )
					return true;
			}

			return false;
		}

		#endregion // HasNonExcludedDescendantBand

		#region CreateColumnChooserGridDataStructure

		// SSP 7/20/06 BR14412
		// Band keys can be the same in the case of the recursive datasource. Therefore generate an unique
		// name for table and relation.
		// 
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string GetUniqueTableName( DataSet ds, string baseName )
		private static string GetUniqueTableName( DataSet ds, string baseName )
		{
			// MD 2/6/09 - TFS13534
			// We cannot use '.' characters in the table names because they are used as property separators in the binding context when we are 
			// trying to get the binding manager.
			baseName = baseName.Replace( '.', '_' );

			if ( ! ds.Tables.Contains( baseName ) )
				return baseName;

			int i = 1;
			while ( ds.Tables.Contains( baseName + i ) )
				i++;

			return baseName + i;
		}

		// SSP 7/20/06 BR14412
		// Band keys can be the same in the case of the recursive datasource. Therefore generate an unique
		// name for table and relation.
		// 
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string GetUniqueRelationName( DataSet ds, string baseName )
		private static string GetUniqueRelationName( DataSet ds, string baseName )
		{
			if ( ! ds.Relations.Contains( baseName ) )
				return baseName;

			int i = 1;
			while ( ds.Relations.Contains( baseName + i ) )
				i++;

			return baseName + i;
		}

		private void CreateColumnChooserGridDataStructureHelper( 
			DataSet dataSet, 
			DataTable parentTable, 
			UltraGridBand band, 
			bool recursive, 
			int uniqueKeyValueCounter, 
			UltraGridBand[] nonExcludedBands )
		{
			// If the band is excluded and it doesn't have any non-excluded descendants then return.
			// If an excluded band has non-excluded descendants then add the band. Only the band header
			// will be displayed.
			// 
			if ( Array.IndexOf( nonExcludedBands, band ) < 0 
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//&& ! this.HasNonExcludedDescendantBand( nonExcludedBands, band ) )
				&& !UltraGridColumnChooser.HasNonExcludedDescendantBand( nonExcludedBands, band ) )
				return;

			// Show the band headers if the grid has multiple bands or if the user is allowed to hide
			// child bands.
			// 
			bool addBandRow = band.Layout.SortedBands.Count > 1 && ( recursive 
				|| ColumnChooserStyle.AllColumnsAndChildBandsWithCheckBoxes == this.Style );

			// SSP 7/20/06 BR14412
			// Band keys can be the same in the case of the recursive datasource. Therefore generate an unique
			// name for table and relation.
			// 
			//DataTable childTable = this.CreateTableFromBand( band, ref uniqueKeyValueCounter, addBandRow, nonExcludedBands );
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//string tableName = this.GetUniqueTableName( dataSet, band.Key );
			string tableName = UltraGridColumnChooser.GetUniqueTableName( dataSet, band.Key );

			DataTable childTable = this.CreateTableFromBand( tableName, band, ref uniqueKeyValueCounter, addBandRow, nonExcludedBands );

			dataSet.Tables.Add( childTable );

			if ( null != parentTable )
			{
				// SSP 7/20/06 BR14412
				// Band keys can be the same in the case of the recursive datasource. Therefore generate an unique
				// name for table and relation.
				// 
				//dataSet.Relations.Add( band.Key, parentTable.Columns["key2"], childTable.Columns["key1"], false );
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//string relationName = this.GetUniqueRelationName( dataSet, band.Key );			
				string relationName = UltraGridColumnChooser.GetUniqueRelationName( dataSet, band.Key );

				dataSet.Relations.Add( relationName, parentTable.Columns["key2"], childTable.Columns["key1"], false );
			}

			if ( recursive )
			{
				foreach ( UltraGridBand bandIterator in this.SourceGrid.DisplayLayout.SortedBands )
				{
					if ( bandIterator.ParentBand == band )
						this.CreateColumnChooserGridDataStructureHelper( dataSet, childTable, bandIterator, recursive, uniqueKeyValueCounter, nonExcludedBands );
				}
			}
		}

		// SSP 7/20/06 BR14412
		// Band keys can be the same in the case of the recursive datasource. Therefore generate an unique
		// name for table and relation.
		// 
		//private DataTable CreateTableFromBand( UltraGridBand band, ref int uniqueKeyValueCounter, bool addBandRow, UltraGridBand[] nonExcludedBands )
		private DataTable CreateTableFromBand( string tableName, UltraGridBand band, ref int uniqueKeyValueCounter, bool addBandRow, UltraGridBand[] nonExcludedBands )
		{
			// SSP 7/20/06 BR14412
			// Band keys can be the same in the case of the recursive datasource. Therefore generate an unique
			// name for table and relation.
			// 
			//DataTable table = new DataTable( band.Key );
			DataTable table = new DataTable( tableName );

			table.Columns.Add( "key1", typeof( string ) );
			table.Columns.Add( "key2", typeof( string ) );

			table.Columns.Add( UltraGridColumnChooser.VALUE_COLUMN, typeof( object ) );
			table.Columns.Add( UltraGridColumnChooser.VISIBLE_COLUMN, typeof( bool ) );

			int parentRowKey = uniqueKeyValueCounter;
			if ( addBandRow )
				table.Rows.Add( new object[] { parentRowKey, ++uniqueKeyValueCounter, new ColumnBandHolder( band ) } );

			// If the band is not excluded then add its columns.
			// 
			if ( Array.IndexOf( nonExcludedBands, band ) >= 0 )
			{
				// SSP 8/28/06 BR14825
				// Added ColumnDisplayOrder on the UltraGridColumnChooser. Sort the columns
				// according to it.
				// 
				//foreach ( UltraGridColumn column in this.GetNonExcludedColumns( band ) )
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//UltraGridColumn[] nonExcludedColumns = this.GetNonExcludedColumns( band );
				UltraGridColumn[] nonExcludedColumns = UltraGridColumnChooser.GetNonExcludedColumns( band );

				this.SortColumnsByDisplayOrder( nonExcludedColumns );

				foreach ( UltraGridColumn column in nonExcludedColumns )
				{
					if ( ! column.IsChaptered )
					{
						if ( column.Hidden 
							|| ColumnChooserStyle.AllColumnsWithCheckBoxes == this.Style 
							|| ColumnChooserStyle.AllColumnsAndChildBandsWithCheckBoxes == this.Style )
							table.Rows.Add( new object[] { parentRowKey, ++uniqueKeyValueCounter, new ColumnBandHolder( column ) } );
					}
				}
			}

			return table;
		}

		internal void CreateColumnChooserGridDataStructure( )
		{
			DataSet dataSet = new DataSet( );

			UltraGridBand[] nonExcludedBands = this.GetNonExcludedBands( );
			UltraGridBand currentBand = this.CurrentBand;

			this.CreateColumnChooserGridDataStructureHelper( dataSet, null, 
				null != currentBand ? currentBand : this.SourceGrid.DisplayLayout.SortedBands[0],
				null == currentBand, 
				1,
				nonExcludedBands );

			this.DisplayGrid.SetDataBinding( dataSet, null );
		}

		#endregion // CreateColumnChooserGridDataStructure

		#region SortColumnsByDisplayOrder

		// SSP 8/28/06 BR14825
		// 
		private void SortColumnsByDisplayOrder( UltraGridColumn[] columns )
		{
			IComparer comparer = null;
			
			ColumnDisplayOrder order = this.ColumnDisplayOrder;
			if ( ColumnDisplayOrder.Alphabetical == order )
                // MRS 7/24/2009 - TFS19613
				//comparer = new GridUtils.AlphabeticalComparer( );
                comparer = new GridUtils.AlphabeticalComparer(true);
			else if ( ColumnDisplayOrder.SameAsGrid == order )
				comparer = new UltraGridBand.ColumnVisiblePositionComparer( );
			else
				Debug.Assert( false );

			if ( null != comparer )
				Array.Sort( columns, comparer );
		}

		#endregion // SortColumnsByDisplayOrder

		#region ReHookIntoSourceGrid

		private void ReHookIntoSourceGrid( )
		{
			if ( this.lastHookedIntoGrid != this.SourceGrid )
			{
				this.UnhookFromSourceGrid( );

				this.lastHookedIntoGrid = this.SourceGrid;
				if ( null != this.lastHookedIntoGrid )
				{
					this.lastHookedIntoGrid.PropertyChanged += new PropertyChangedEventHandler( this.sourceGrid_PropertyChanged );
					this.lastHookedIntoGrid.Disposed += new EventHandler( this.sourceGrid_Disposed );

					// Register the column chooser with the grid.
					// 
					this.lastHookedIntoGrid.RegisterColumnChooser( this );
				}
			}
		}

		#endregion // ReHookIntoSourceGrid

		#region UnhookFromSourceGrid

		private void UnhookFromSourceGrid( )
		{
			if ( null != this.lastHookedIntoGrid )
			{
				this.lastHookedIntoGrid.PropertyChanged -= new PropertyChangedEventHandler( this.sourceGrid_PropertyChanged );
				this.lastHookedIntoGrid.Disposed -= new EventHandler( this.sourceGrid_Disposed );

				// Unregister the column chooser from the grid.
				// 
				this.lastHookedIntoGrid.UnRegisterColumnChooser( this );

				this.lastHookedIntoGrid = null;
			}
		}

		#endregion // UnhookFromSourceGrid

		#region VerifyInitialized

		internal void VerifyInitialized( )
		{
			if ( this.needsReinitialization )
			{
				this.needsReinitialization = false;

				// If the source grid has been disposed off for some reason than null it out.
				// 
				if ( null != this.SourceGrid && this.SourceGrid.IsDisposed )
					this.SourceGrid = null;

				this.Initialize( );
			}

			// SSP 8/12/05 BR05531
			// 
			if ( this.needsToApplySourceGridAppearances )
			{
				this.InitializeAppearancesFromSourceGrid( );
			}
		}

		#endregion // VerifyInitialized

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#region DoesBandHaveChoosableColumns

		//private bool DoesBandHaveChoosableColumns( UltraGridBand band )
		//{
		//    foreach ( UltraGridColumn column in band.Columns )
		//    {
		//        return null != column;
		//    }

		//    return false;
		//}

		//#endregion // DoesBandHaveChoosableColumns

		#endregion Not Used

		#region ClearBandsCombobox

		private void ClearBandsCombobox( )
		{
			if ( null != this.comboBoxBandSelector )
				this.comboBoxBandSelector.Items.Clear( );
		}

		#endregion // ClearBandsCombobox

		#region LoadBandsCombobox

		#region ColumnBandHolder Class

		internal class ColumnBandHolder : IComparable
		{
			private object val = null;
			private string text = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//private ArrayList list = null;

			internal ColumnBandHolder( object val ) : this( val, null )
			{
			}

			internal ColumnBandHolder( object val, string text )
			{
				this.val = val;
				this.text = text;
			}

			#region GetWrappedValue

			internal static object GetWrappedValue( UltraGridCell cell )
			{
				object val = cell.Value;
				if ( val is ColumnBandHolder )
					val = ((ColumnBandHolder)val).Value;

				return val;
			}

			#endregion // GetWrappedValue

			public object Value
			{
				get
				{
					return this.val;
				}
			}

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid uncalled private code
			#region Not Used

			//public ArrayList List
			//{
			//    get
			//    {
			//        if ( null == this.list )
			//            this.list = new ArrayList( );

			//        return this.list;
			//    }
			//}

			#endregion Not Used

			public int CompareTo( object o )
			{
				return string.Compare( 
					this.ToString( ), 
					null != o ? o.ToString( ) : string.Empty, 
					true, 
					System.Globalization.CultureInfo.CurrentCulture );
			}

			internal static string GetCaptionAsSingleLine( HeaderBase header )
			{
				string caption = header.Caption;
				if ( null == caption )
					caption = string.Empty;

				return GridUtils.ToSingleLine( caption );
			}

			public override string ToString( )
			{
				if ( null != this.text )
					return this.text;

				if ( this.val is UltraGridColumn )
					return GetCaptionAsSingleLine( ((UltraGridColumn)this.val).Header );

				if ( this.val is UltraGridBand )
					return GetCaptionAsSingleLine( ((UltraGridBand)this.val).Header );

				if ( null != this.val )
					return this.val.ToString( );

				return string.Empty;
			}

			public override bool Equals( object o )
			{
				return this.val == o 
					|| o is ColumnBandHolder 
					&& ((ColumnBandHolder)o).val == this.val;
			}

			public override int GetHashCode( )
			{
				return null != this.val ? this.val.GetHashCode( ) : 0;
			}
		}

		#endregion // ColumnBandHolder Class

		private void LoadBandsCombobox( bool addAllEntry )
		{
			this.ClearBandsCombobox( );

			if ( null != this.SourceGrid && null != this.comboBoxBandSelector )
			{
				object allBandsItem = null;
				if ( addAllEntry )
				{
                    // MRS 4/26/07 - BR22208
                    //allBandsItem = new ColumnBandHolder( this.SourceGrid.DisplayLayout.Bands, "All" );
                    allBandsItem = new ColumnBandHolder(this.SourceGrid.DisplayLayout.SortedBands, SR.GetString("ColumnChooserAllBandsItem"));

					this.comboBoxBandSelector.Items.Add( allBandsItem );
				}

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//ArrayList list = new ArrayList( );
				List<ColumnBandHolder> list = new List<ColumnBandHolder>();

				foreach ( UltraGridBand band in this.GetNonExcludedBands( ) )
					list.Add( new ColumnBandHolder( band ) );

				// Sort by band captions and and add them to the items.
				//
				list.Sort( );

				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//this.comboBoxBandSelector.Items.AddRange( (object[])list.ToArray( typeof( object ) ) );
				this.comboBoxBandSelector.Items.AddRange( list.ToArray() );

				// Select the current band or All depending on the CurrentBand settings.
				// 
				object itemToSelect = allBandsItem;
				if ( null != this.CurrentBand )
				{
					ColumnBandHolder currentBandItem = new ColumnBandHolder( this.CurrentBand );
					if ( this.comboBoxBandSelector.Items.Contains( currentBandItem ) )
						itemToSelect = currentBandItem;
				}

				this.comboBoxBandSelector.SelectedItem = itemToSelect;
			}
		}

		#endregion // LoadBandsCombobox

		#region GetNonExcludedColumns

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private UltraGridColumn[] GetNonExcludedColumns( UltraGridBand band )
		private static UltraGridColumn[] GetNonExcludedColumns( UltraGridBand band )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<UltraGridColumn> list = new List<UltraGridColumn>();

			foreach (  UltraGridColumn column in band.Columns )
			{
				if ( ! column.IsChaptered 
					&& ExcludeFromColumnChooser.True != column.ExcludeFromColumnChooserResolved )
					list.Add( column );
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics	
			//return (UltraGridColumn[])list.ToArray( typeof( UltraGridColumn ) );
			return list.ToArray();
		}

		#endregion // GetNonExcludedColumns

		#region GetNonExcludedBands

		private UltraGridBand[] GetNonExcludedBands( )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics	
			//ArrayList list = new ArrayList( );
			List<UltraGridBand> list = new List<UltraGridBand>();

			if ( null != this.SourceGrid && null != this.SourceGrid.DisplayLayout )
			{
				foreach ( UltraGridBand band in this.SourceGrid.DisplayLayout.SortedBands )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//UltraGridColumn[] nonExcludedColumns = this.GetNonExcludedColumns( band );
					UltraGridColumn[] nonExcludedColumns = UltraGridColumnChooser.GetNonExcludedColumns( band );

					if ( null != nonExcludedColumns && nonExcludedColumns.Length > 0 )
						list.Add( band );				
				}
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (UltraGridBand[])list.ToArray( typeof( UltraGridBand ) );
			return list.ToArray();
		}

		#endregion // GetNonExcludedBands

		#region Initialize

		private void Initialize( )
		{
			this.ReHookIntoSourceGrid( );

			this.DisplayGrid.SetDataBinding( null, null );

			if ( null != this.SourceGrid )
			{
				this.LoadBandsCombobox( true );

				// If the source grid's view style is single band then display only the root band.
				// 
				UltraGridBand[] nonExcludedBands = this.GetNonExcludedBands( );
				MultipleBandSupport multipleBandSupport = this.MultipleBandSupport;

				if ( ! this.SourceGrid.DisplayLayout.ViewStyleImpl.IsMultiBandDisplay
					|| nonExcludedBands.Length <= 1 )
				{
					// If there is only one band then use SingleBandOnly as the multipleBandSupport.
					// This means that the band selector will be hidden.
					// 
					multipleBandSupport = MultipleBandSupport.SingleBandOnly;
					this.CurrentBand = nonExcludedBands.Length > 0 ? nonExcludedBands[0] : null;
				}

				if ( null != this.comboBoxBandSelector )
					this.comboBoxBandSelector.Visible = MultipleBandSupport.ShowBandSelectionUI == multipleBandSupport;

				// If displaying columns from multiple bands at once then set the CurrentBand to null.
				// 
				if ( MultipleBandSupport.DisplayColumnsFromAllBands == multipleBandSupport
					&& nonExcludedBands.Length > 1 )
					this.CurrentBand = null;

				// SSP 8/12/05 BR05497
				// If MultipleBandSupport is set to SingleBandOnly however CurrentBand is not set,
				// then show the columns from the root band.
				// 
				if ( MultipleBandSupport.SingleBandOnly == multipleBandSupport 
					&& null == this.CurrentBand && nonExcludedBands.Length >= 1 )
					this.CurrentBand = nonExcludedBands[0];

				this.CreateColumnChooserGridDataStructure( );

				this.DisplayGrid.Rows.ExpandAll( true );
			}
		}

		#endregion // Initialize

		#region SetNeedsReinitialization

		internal void SetNeedsReinitialization( )
		{
			this.needsReinitialization = true;

			// SSP 8/12/05 BR05531
			// 
			this.SetNeedsToApplySourceGridAppearances( );
		}

		#endregion // SetNeedsReinitialization

		#region SetNeedsToApplySourceGridAppearances

		// SSP 8/12/05 BR05531
		// 
		private void SetNeedsToApplySourceGridAppearances( )
		{
			this.needsToApplySourceGridAppearances = true;

			if ( null != this.DisplayGrid && null != this.DisplayGrid.DisplayLayout 
				&& this.DisplayGrid.DisplayLayout.HasUIElement )
				this.DisplayGrid.DisplayLayout.UIElement.DirtyChildElements( );
		}

		#endregion // SetNeedsToApplySourceGridAppearances

		// MD 7/26/07 - 7.3 Performance
		// This method does nothing
		#region Removed

		//        #region OnDragStart


		//#if DEBUG
		//        /// <summary>
		//        /// Called by the UltraGrid whenever the mouse is moved while dragging items. This
		//        /// gets called only when this control is visible.
		//        /// </summary>
		//#endif
		//        internal void OnDragStart( )
		//        {
		//        }

		//        #endregion // OnDragStart

		#endregion Removed

		#region OnDragMove
		
		internal void OnDragMove( Point mouseLocInScreen )
		{
			if ( this.IsDraggingFromSourceGrid )
			{
				Point mouseLocInClient = this.PointToClient( mouseLocInScreen );

				bool drawAsActive = this.ClientRectangle.Contains( mouseLocInClient );
				this.DrawAsActiveHelper( drawAsActive );
			}
		}

		#endregion // OnDragMove

		#region IsDraggingFromThisColumnChooser

		/// <summary>
		/// Returns true if a column from this column chooser is being dragged.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Advanced ), Browsable( false ) ]
		public bool IsDraggingFromThisColumnChooser
		{
			get
			{
				return this.DisplayGrid.Capture;
			}
		}

		#endregion // IsDraggingFromThisColumnChooser

		#region IsDraggingFromSourceGrid

		internal bool IsDraggingFromSourceGrid
		{
			get
			{
				return null != this.SourceGrid && this.SourceGrid.Capture;
			}
		}

		#endregion // IsDraggingFromSourceGrid

		#region DrawAsActiveHelper

		internal bool drawAsActive = false;

		internal void DrawAsActiveHelper( bool active )
		{
			if ( this.drawAsActive != active )
			{
				this.drawAsActive = active;

				if ( null != this.DisplayGrid )
					this.DisplayGrid.DisplayLayout.UIElement.DirtyChildElements( );
			}
		}

		#endregion // DrawAsActiveHelper

		#region OnDragEnd
		
		internal void OnDragEnd( )
		{
			this.DrawAsActiveHelper( false );
		}

		#endregion // OnDragEnd

		#endregion // Private/Internal Methods/Properties

		#region Public Properties

		#region SourceGrid

		/// <summary>
		/// Gets or sets the UltraGridBase instance this column chooser control is currently 
		/// displaying the columns of.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <see cref="CurrentBand"/> property can be used to restrict the control to displaying 
		/// columns of a particular band.
		/// </p>
		/// </remarks>
		[ TypeConverter( typeof( ColumnChooserSourceGridFilter ) ) ]
		[DefaultValue(null)] // AS 4/5/06 BR11341
		public UltraGridBase SourceGrid
		{
			get
			{
				return this.sourceGrid;
			}
			set
			{
				if ( this.sourceGrid != value )
				{
					// Unhook from the old grid.
					// 
					this.UnhookFromSourceGrid( );

					this.sourceGrid = value;

					// Set a flag indicating that we need to reget the columns from the new grid.
					//
					this.SetNeedsReinitialization( );
				}
			}
		}

		#endregion // SourceGrid

		#region DisplayLayout
        
		/// <summary>
		/// Returns the display layout of the UltraGrid used by the column chooser control to 
		/// display the list of column headers.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Internally the column chooser control uses an UltraGrid to display the list of columns. 
		/// This UltraGrid is different from the grid for which the column chooser control is being 
		/// displayed. Use the <see cref="SourceGrid"/> property to get or set the UltraGridBase 
		/// instance whose columns you want to display in the column chooser.
		/// </p>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		public UltraGridLayout DisplayLayout
		{
			get
			{
				return this.DisplayGrid.DisplayLayout;
			}
		}

		#endregion // DisplayLayout

		#region DisplayGrid
        
		internal UltraGrid DisplayGrid
		{
			get
			{
				return this.displayGrid;
			}
		}

		#endregion // DisplayGrid

		#region CurrentBand
		
		/// <summary>
		/// Gets or sets the band whose columns are currently being displayed in the column chooser control.
		/// If null then displays columns from all bands.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[DefaultValue(null)] // AS 4/5/06 BR11341
		public UltraGridBand CurrentBand
		{
			get
			{
				return this.currentBand;
			}
			set
			{
				if ( value != this.currentBand )
				{
					if ( null != value && value.Layout.Grid != this.SourceGrid )
						throw new ArgumentException( "CurrentBand must be a valid band from the SourceGrid.", "CurrentBand" );

					this.currentBand = value;

					this.serializedCurrentBandKey = null != this.currentBand ? this.currentBand.Key : null;

					this.SetNeedsReinitialization( );
				}
			}
		}

		#endregion // CurrentBand

		#region SerializedCurrentBandKey
		
		/// <summary>
		/// Used for design-time serialization. Use <see cref="CurrentBand"/> property instead.
		/// </summary>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Never ), 
		  DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		public string SerializedCurrentBandKey
		{
			get
			{
				return this.serializedCurrentBandKey;
			}
			set
			{
				this.serializedCurrentBandKey = value;
			}
		}

		#endregion // SerializedCurrentBandKey

		#region MultipleBandSupport

		/// <summary>
		/// Specifies whether to display a user interface for letting the user select 
		/// a different band and also whether to display columns of multiple bands or
		/// a single band. Default is <b>ShowBandSelectorUI</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// See <see cref="UltraGridColumnChooser"/> for more information.
		/// </p>
		/// </remarks>
		public MultipleBandSupport MultipleBandSupport
		{
			get
			{
				return this.multipleBandSupport;
			}
			set
			{
				if ( value != this.multipleBandSupport )
				{
					GridUtils.ValidateEnum( typeof( MultipleBandSupport ), value );

					this.multipleBandSupport = value;

					this.SetNeedsReinitialization( );
				}
			}
		}

		#endregion // MultipleBandSupport

		#region Style

		/// <summary>
		/// Specifies the style of the column chooser. Default is <b>AllColumnsAndChildBandsWithCheckBoxes</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Specifies the style of the column chooser. Default is <b>AllColumnsAndChildBandsWithCheckBoxes</b>. 
		/// See <see cref="ColumnChooserStyle"/> and <see cref="UltraGridColumnChooser"/> for more information. 
		/// </p>
		/// <seealso cref="ColumnChooserStyle"/> <seealso cref="UltraGridColumnChooser"/>
		/// </remarks>
		public ColumnChooserStyle Style
		{
			get
			{
				return this.style;
			}
			set
			{
				if ( value != this.style )
				{
					GridUtils.ValidateEnum( typeof( ColumnChooserStyle ), value );

					this.style = value;

					this.SetNeedsReinitialization( );
				}
			}
		}

		#endregion // Style

		#region SyncLookWithSourceGrid

		/// <summary>
		/// Specifies whether to apply appearance of the source grid to the column chooser. Default is <b>True</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// When <b>SyncLookWithSourceGrid</b> is set to <b>True</b> the source grid's appearance 
		/// will be applied to the column chooser so they look similar. If this property is left 
		/// to <b>True</b> the appearance set on the column chooser's <see cref="UltraGridColumnChooser.DisplayLayout"/> 
		/// may get overridden. Therefore set this property to <b>False</b> if you want to set appearance
		/// on the DisplayLayout of the column chooser.
		/// </p>
		/// <p class="body">
		/// <b>Note</b> that not all appearances are copied from the source grid to the column chooser's 
		/// display layout. If you find that some appearance is left uncopied then set this property to 
		/// <b>False</b> and manually set the appearance on the DisplayLayout of the column chooser.
		/// </p>
		/// </remarks>
		public bool SyncLookWithSourceGrid
		{
			get
			{
				return this.syncLookWithSourceGrid;
			}
			set
			{
				if ( value != this.syncLookWithSourceGrid )
				{
					this.syncLookWithSourceGrid = value;

					this.SetNeedsReinitialization( );
				}
			}
		}

		#endregion // SyncLookWithSourceGrid

		#region CurrentSelectedItem

		/// <summary>
		/// Returns either an UltraGridColumn or UltraGridBand instance that's currently selected.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>CurrentSelectedItem</b> property can be used to find out which column or band has
		/// been selected by the user. The return value can be an instace of UltraGridColumn or
		/// UltraGridBand or null in case no item is currently selected.
		/// </p>
		/// </remarks>
		public object CurrentSelectedItem
		{
			get
			{
				object item = null;

				if ( null != this.DisplayGrid && null != this.DisplayGrid )
				{
					UltraGridRow activeRow = this.DisplayGrid.ActiveRow;
					if ( null != activeRow && activeRow.Cells.Exists( VALUE_COLUMN ) )
						item = ColumnBandHolder.GetWrappedValue( activeRow.Cells[ VALUE_COLUMN ] );
				}

				return item;
			}
		}

		#endregion // CurrentSelectedItem

		// AS 11/3/06 NA 2007 Vol 1 - StyleLibraryName
		#region StyleLibraryName

		/// <summary>
		/// Specifies the name of the library from which the application style information will be obtained.
		/// </summary>
		public string StyleLibraryName
		{
			get
			{
				return null != this.DisplayGrid ? this.DisplayGrid.StyleLibraryName : null;
			}
			set
			{
				if (null != this.DisplayGrid)
					this.DisplayGrid.StyleLibraryName = value;
			}
		}

		#endregion // StyleLibraryName

		#region StyleSetName

		// SSP 5/22/06 BR12829
		// Added StyleSetName property.
		// 
		/// <summary>
		/// Specifies the style set name.
		/// </summary>
		public string StyleSetName
		{
			get
			{
				return null != this.DisplayGrid ? this.DisplayGrid.StyleSetName : null;
			}
			set
			{
				if ( null != this.DisplayGrid )
					this.DisplayGrid.StyleSetName = value;
			}
		}

		#endregion // StyleSetName

		#region ColumnDisplayOrder

		// SSP 8/28/06 BR14825
		// Added ColumnDisplayOrder on the UltraGridColumnChooser.
		// 
		/// <summary>
		/// Specifies the order of columns. Default is <b>Alphabetical</b>.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnDisplayOrder"/>
		/// </remarks>
		public ColumnDisplayOrder ColumnDisplayOrder
		{
			get
			{
				return this.columnDisplayOrder;
			}
			set
			{
				if ( this.columnDisplayOrder != value )
				{
					this.columnDisplayOrder = value;

					this.SetNeedsReinitialization( );
				}
			}
		}

		#endregion // ColumnDisplayOrder

		#endregion // Public Properties

		#region Public Methods

		#region GetIdealSize

		/// <summary>
		/// Returns the size that this column chooser control should be in order to appropriately
		/// display column headers.
		/// </summary>
		/// <returns></returns>
		public Size GetIdealSize( )
		{
			return this.GetIdealSize( false );
		}

		/// <summary>
		/// Returns the size that this column chooser control should be in order to appropriately
		/// display column headers.
		/// </summary>
		/// <param name="actualRequiredSize">If <b>actualRequiredSize</b> is <b>True</b> then 
		/// returns the size required to fully display all the columns. If it's <b>False</b> then
		/// it restricts to desktop bounds, to a minimum size and also makes sure that the ratio 
		/// between height and width is no greater than 3.</param>
		/// <returns></returns>
		public Size GetIdealSize( bool actualRequiredSize )
		{
			this.VerifyInitialized( );

			System.Drawing.Size minSize = new Size( 160, 240 );
			System.Drawing.Size size = new Size( 160, 240 );

			if ( null != this.SourceGrid )
			{
				UltraGridLayout layout = this.DisplayGrid.DisplayLayout;

				// SSP 10/20/05 BR06792
				// We also need to restore the column widths when the method finishes because 
				// the following code auto-resizes the columns, potentially changing their widths.
				// 
				//AutoFitStyle origAutoFitStyle = layout.AutoFitStyle;
				UltraGridLayout savedLayout = layout.Clone( );

				layout.AutoFitStyle = AutoFitStyle.None;

				int maxRight = 0;

				foreach ( UltraGridBand band in layout.SortedBands )
				{
					foreach ( UltraGridColumn column in band.Columns )
					{
						if ( ! column.IsChaptered )
							column.PerformAutoResizeHelper( null, 0, true, false, true );
					}
				}

				layout.ColScrollRegions.InitializeMetrics( );

				foreach ( UltraGridBand band in layout.SortedBands )
				{
					int bandRight = band.GetOrigin( BandOrigin.PreRowArea ) + band.GetExtent( BandOrigin.PreRowArea );
					maxRight = Math.Max( maxRight, bandRight );
				}

				int height = 0;
				UltraGridRow lastRow = null;
				// SSP 12/13/05 - Recursive Row Enumerator
				// Removed the skipGroupByRow parameter.
				// 
				//RowsEnumerator rowsEnumerator = new RowsEnumerator( layout.Rows, null, false );
				RowsEnumerator rowsEnumerator = new RowsEnumerator( layout.Rows, null );

				while ( rowsEnumerator.MoveNext( ) )
				{
					UltraGridRow row = rowsEnumerator.Current;
					height += row.TotalHeight;

					if ( null != lastRow && lastRow.Band != row.Band )
						height += row.Band.Layout.InterBandSpacing;

					lastRow = row;
				}

				if ( MultipleBandSupport.ShowBandSelectionUI == this.MultipleBandSupport )
					height += this.comboBoxBandSelector.Height;
				
				int SLACK = 8;
				size.Height = SLACK + height;
				size.Width = SLACK + maxRight + SystemInformation.VerticalScrollBarWidth;

				if ( ! actualRequiredSize )
				{
					Rectangle rect = new Rectangle( 0, 0, 1, 1 );
					rect = Infragistics.Win.UIElement.GetDeskTopWorkArea( this.SourceGrid.RectangleToScreen( rect  ) );

					size.Width = Math.Min( Math.Max( minSize.Width, size.Width ), rect.Width );
					size.Height = Math.Min( Math.Max( minSize.Height, size.Height ), rect.Height );

					size.Height = Math.Min( size.Height, 3 * size.Width );
				}

				// SSP 10/20/05 BR06792
				// We also need to restore the column widths when the method finishes because 
				// the following code auto-resizes the columns, potentially changing their widths.
				// 
				//layout.AutoFitStyle = origAutoFitStyle;
				layout.CopyFrom( savedLayout );
			}

			return size;
		}

		#endregion // GetIdealSize

		#region ResetMultipleBandSupport

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		/// <returns></returns>
		public void ResetMultipleBandSupport( )
		{
			this.MultipleBandSupport = MultipleBandSupport.ShowBandSelectionUI;
		}

		#endregion // ResetMultipleBandSupport

		#region ResetColumnDisplayOrder

		// SSP 8/28/06 BR14825
		// Added ColumnDisplayOrder on the UltraGridColumnChooser.
		// 
		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		/// <returns></returns>
		public void ResetColumnDisplayOrder( )
		{
			this.ColumnDisplayOrder = DEFAULT_COLUMN_DISPLAY_ORDER;
		}

		#endregion // ResetColumnDisplayOrder

		#region ResetStyle

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		/// <returns></returns>
		public void ResetStyle( )
		{
			this.Style = ColumnChooserStyle.AllColumnsAndChildBandsWithCheckBoxes;
		}

		#endregion // ResetStyle

		#region ResetSyncLookWithSourceGrid

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		/// <returns></returns>
		public void ResetSyncLookWithSourceGrid( )
		{
			this.SyncLookWithSourceGrid = true;
		}

		#endregion // ResetSyncLookWithSourceGrid

		#region GetHeaderUIElement

		/// <summary>
		/// Returns the ui element associated with the specified column header if it's currently displayed. Returns null if it's not currently in view.
		/// </summary>
		/// <param name="column">The column whose header UIElement is to be retrieved.</param>
        /// <returns>The ui element associated with the specified column header if it's currently displayed. Returns null if it's not currently in view.</returns>
		public UIElement GetHeaderUIElement( UltraGridColumn column )
		{
			UIElement elem = null;

			UltraGridCell cell = this.FindCellForColumn( this.DisplayGrid.Rows, column );
			if ( null != cell )
			{
				// Get the main elem.
				// 
				elem = this.DisplayGrid.DisplayLayout.UIElement;

				// Find the row element associated with the cell that represents the 
				// passed in column.
				// 
				if ( null != elem )
					elem = elem.GetDescendant( typeof( RowUIElementBase ), cell.Row );

				// Find the descendant header element.
				// 
				if ( null != elem )
					elem = elem.GetDescendant( typeof( HeaderUIElement ) );
			}

			return elem;
		}

		private UltraGridCell FindCellForColumn( RowsCollection rows, UltraGridColumn column )
		{
			foreach ( UltraGridRow row in rows )
			{
				UltraGridCell cell = row.Cells[ VALUE_COLUMN ];
				if ( column == ColumnBandHolder.GetWrappedValue( cell ) )
					return cell;

				if ( null != row.ChildBands )
				{
					foreach ( UltraGridChildBand childBand in row.ChildBands )
					{
						cell = this.FindCellForColumn( childBand.Rows, column );
						if ( null != cell )
							return cell;
					}
				}
			}

			return null;
		}

		#endregion // GetHeaderUIElement

		#endregion // Public Methods

		#region Protected Methods

		#region ShouldSerializeSerializedCurrentBandKey

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSerializedCurrentBandKey( )
		{
			return null != this.serializedCurrentBandKey && this.serializedCurrentBandKey.Length > 0;
		}

		#endregion // ShouldSerializeSerializedCurrentBandKey

		#region ShouldSerializeMultipleBandSupport

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMultipleBandSupport( )
		{
			return MultipleBandSupport.ShowBandSelectionUI != this.multipleBandSupport;
		}

		#endregion // ShouldSerializeMultipleBandSupport

		#region ShouldSerializeColumnDisplayOrder

		// SSP 8/28/06 BR14825
		// Added ColumnDisplayOrder on the UltraGridColumnChooser.
		// 
		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeColumnDisplayOrder( )
		{
			return DEFAULT_COLUMN_DISPLAY_ORDER != this.columnDisplayOrder;
		}

		#endregion // ShouldSerializeColumnDisplayOrder

		#region ShouldSerializeStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeStyle( )
		{
			return ColumnChooserStyle.AllColumnsAndChildBandsWithCheckBoxes != this.style;
		}

		#endregion // ShouldSerializeStyle

		#region ShouldSerializeSyncLookWithSourceGrid

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSyncLookWithSourceGrid( )
		{
			return ! this.syncLookWithSourceGrid;
		}

		#endregion // ShouldSerializeSyncLookWithSourceGrid

		#region Dispose

		/// <summary>
        /// Clean up any resources being used.
		/// </summary>
        /// <param name="disposing">True if managed resources should be released.</param>
		protected override void Dispose( bool disposing )
		{
			// Unhook and release reference to the source grid.
			// 
			this.SourceGrid = null;
			this.UnhookFromSourceGrid( );

			base.Dispose( disposing );
		}

		#endregion // Dispose

		#region DefaultSize

		/// <summary>
		/// Overridden method returns the default size of the control
		/// </summary>
		protected override Size DefaultSize 
		{
			get
			{
				return new Size( 140, 160 );
			}
		}

		#endregion // DefaultSize

		#endregion // Protected Methods

        #region Overrides

        #region Padding

        // MRS 11/2/05 - BR04592
        // Microsoft added a Padding property to Control in CLR2. 
        // Since our controls don't support it, we need to shadow and 
        // hide it. 
        /// <summary>
        /// The Padding property is shadowed and hidden. It is not supported by this class. 
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public new Padding Padding
        {
            get { return base.Padding; }
            set { base.Padding = value; }
        }
        #endregion // Padding

        #region BackgroundImageLayout

        // MRS 11/2/05 - BR04592
        // Microsoft added a BackgroundImageLayout property to Control in CLR2. 
        // Since our controls don't support it, we need to shadow and 
        // hide it. 
        /// <summary>
        /// The BackgroundImageLayout property is shadowed and hidden. It is not supported by this class. 
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public new ImageLayout BackgroundImageLayout
        {
            get { return base.BackgroundImageLayout; }
            set { base.BackgroundImageLayout = value; }
        }
        #endregion // BackgroundImageLayout

		#region OnCreateControl

		// SSP 12/9/05 BR07954
		// Overrode OnCreateControl to re-layout the controls. When the Visible of the
		// combox box is set the .NET framework fails to re-layout the parent control.
		// 
		/// <summary>
		/// Called when the control is created 
		/// </summary>
		protected override void OnCreateControl( )
		{
			base.OnCreateControl( );

			this.PerformLayout( );
		}

		#endregion // OnCreateControl

        #endregion // Overrides

        #region About Dialog and Licensing Interface

        /// <summary>
		/// Display the about dialog
		/// </summary>
		[ DesignOnly( true ) ]
		[ LocalizedDescription("LD_UltraGrid_P_About") ]
		[ LocalizedCategory("LC_Design") ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ ParenthesizePropertyName( true ) ]
		[ Editor(typeof(AboutDialogEditor), typeof(System.Drawing.Design.UITypeEditor) ) ]
		public object About { get { return null; } }


		UltraLicense IUltraLicensedComponent.License { get { return this.license; } }
	
		#endregion

		// SSP 2/26/07 BR18838
		// 
		#region ISupportInitialize Implementation

		void ISupportInitialize.BeginInit( )
		{
			if ( null != this.displayGrid )
				this.displayGrid.InternalSetInitializing( true );
		}

		void ISupportInitialize.EndInit( )
		{
			if ( null != this.displayGrid )
				this.displayGrid.InternalSetInitializing( false );
		}

		#endregion // ISupportInitialize Implementation
	}

	#endregion // UltraGridColumnChooser

	#region ColumnChooserGrid

	// SSP 4/25/06 - App Styling
	// Added ColumnChooserGrid so the app-style logic can create the right component role for it.
	// 
	/// <summary>
	/// Grid that's embedded inside the column chooser.
	/// </summary>
	[ToolboxItem(false)] // AS 5/5/06 BR12208
	public class ColumnChooserGrid : UltraGrid
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public ColumnChooserGrid( ) : base( )
		{
		}
	}

	#endregion // ColumnChooserGrid

	#region ColumnChooserGridCreationFilter Class

	internal class ColumnChooserGridCreationFilter : IUIElementCreationFilter
	{
		#region Private Vars

		private UltraGridColumnChooser ultraGridColumnChooser = null;

		private GridHeaderElement extractedGridHeaderElem = null;

		#endregion // Private Vars

		#region Constructor

		internal ColumnChooserGridCreationFilter( UltraGridColumnChooser ultraGridColumnChooser )
		{
			this.ultraGridColumnChooser = ultraGridColumnChooser;
		}

		#endregion // Constructor

		#region GridHeaderElement

		private class GridHeaderElement : Infragistics.Win.UltraWinGrid.HeaderUIElement 
		{
			#region Private Vars

			private HeaderBase sourceGridHeader = null;

			#endregion // Private Vars

			#region Constructor

			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="parent">Parent element.</param>
			/// <param name="sourceGridHeader">Source grid header. The header from the grid for which
			/// the column chooser control is being displayed for.</param>
			internal GridHeaderElement( UIElement parent, HeaderBase sourceGridHeader ) 
				: base( parent, sourceGridHeader )
			{
				this.sourceGridHeader = sourceGridHeader;

				
			}

			#endregion // Constructor

			#region InitAppearance
					
			protected override void InitAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
			{

				HeaderBase header = this.Header;
				if ( null != header )
				{
					UltraGridRow row = (UltraGridRow)this.GetContext( typeof( UltraGridRow ), true );
					UltraGridCell cell = null != row ? row.Cells[ UltraGridColumnChooser.VALUE_COLUMN ] : null;

					// If the element represents a column header.
					// 
					if ( null != header.Column 
						&& null != row && row.IsActiveRow && ! this.IsMouseDownOverElem
						// SSP 8/10/05 BR05528
						// Since the BackColor won't be honored if the header is themed, don't honor
						// the fore color either otherwise the fore color may end up being "close" to
						// the back color causing the text to be hard to read.
						// 
						&& HeaderStyle.XPThemed != this.HeaderStyle )
					{
						AppearanceData selectedAppearance = new AppearanceData( );
						AppearancePropFlags selectedFlags = AppearancePropFlags.BackColor 
							| AppearancePropFlags.ForeColor;
						
                        // MBS 5/5/08 - RowEditTemplate NA2008 V2
                        // Added isProxyResolution parameter
                        //
                        //row.ResolveCellAppearance( cell.Column, ref selectedAppearance, ref selectedFlags,
                        //    false, true, ForceActive.None, false, false, false );
                        row.ResolveCellAppearance(cell.Column, ref selectedAppearance, ref selectedFlags,
                            false, true, ForceActive.None, false, false, false, false);

						AppearanceData.Merge( ref appData, ref selectedAppearance, ref flags );
					}

					// Force left alignment for column headers.
					// 
					appData.TextHAlign = HAlign.Left;
					flags &= ~AppearancePropFlags.TextHAlign;

					// If the element represents a band header.
					// 
					if ( header is BandHeader )
					{
						appData.FontData.Bold = DefaultableBoolean.True;
						flags &= ~AppearancePropFlags.FontBold;
					}
				}

				base.InitAppearance( ref appData, ref flags );
			}

			#endregion // InitAppearance

			#region PositionChildElements

			protected override void PositionChildElements( )
			{
				Infragistics.Win.UltraWinGrid.HeaderBase header = this.Header;
				Debug.Assert( null != header );
				if ( null == header )
					return;

				base.PositionChildElements( );

				// Headers in the dialog should not display sort indicators, filter, swap,
				// summary etc... icons.
				// 
				TextUIElementBase textElem = null;
				ImageUIElementBase imageElem = null;

				for ( int i = this.ChildElements.Count - 1; i >= 0; i-- )
				{
					UIElement elem = this.ChildElements[i];

					// MD 8/7/07 - 7.3 Performance
					// Refactored - FxCop - Do not cast unnecessarily
					//if ( elem is TextUIElementBase )
					//    textElem = elem as TextUIElementBase;
					//else if ( elem is ImageUIElementBase )
					//    imageElem = elem as ImageUIElementBase;
					//else 
					//    this.ChildElements.RemoveAt( i );
					TextUIElementBase currentTextElement = elem as TextUIElementBase;

					if ( currentTextElement != null )
						textElem = currentTextElement;
					else
					{
						ImageUIElementBase currentImageElement = elem as ImageUIElementBase;

						if ( currentImageElement != null )
							imageElem = currentImageElement;
						else
							this.ChildElements.RemoveAt( i );
					}
				}

				Rectangle thisRect = this.RectInsideBorders;

				if ( null != textElem )
				{
					// SSP 8/12/05 BR05524
					// Use the ColumnChooserCaption.
					// 
					Infragistics.Win.UltraWinGrid.ColumnHeader columnHeader = this.Header as Infragistics.Win.UltraWinGrid.ColumnHeader;

					// MD 8/3/07 - 7.3 Performance
					// Refactored - Prevent calling expensive getters multiple times
					//if ( null != columnHeader && null != columnHeader.Column )
					//    textElem.Text = columnHeader.Column.ColumnChooserCaptionResolved;
					if ( null != columnHeader )
					{
						UltraGridColumn headerColumn = columnHeader.Column;

						if ( null != headerColumn )
							textElem.Text = headerColumn.ColumnChooserCaptionResolved;
					}

					if ( null != imageElem && ! imageElem.Rect.IntersectsWith( textElem.Rect ) )
					{
						Rectangle imgRect = imageElem.Rect;
						Rectangle textRect = textElem.Rect;

						if ( textRect.X > imgRect.X )
						{
							textRect.X = imgRect.Right;
							textRect.Width = thisRect.Right - textRect.X;
						}
						else
						{
							textRect.Width = imgRect.X - textRect.X;
						}

						textElem.Rect = textRect;
					}
					else
					{
						textElem.Rect = thisRect;
					}
				}
			}

			#endregion // PositionChildElements

			#region IsMouseDownOverElem

			private bool IsMouseDownOverElem
			{
				get
				{
					return this.HasCapture;
				}
			}

			#endregion // IsMouseDownOverElem

			#region OnMouseDown

			protected override bool OnMouseDown( MouseEventArgs e, 
				bool adjustableArea, ref UIElement captureMouseForElement )
			{
				// Activate the row on mouse down.
				// 
				UltraGridRow row = (UltraGridRow)this.GetContext( typeof( UltraGridRow ), true );
				if ( null != row )
					row.Activate( );

				// Clear the selected columns in the source grid. Otherwise the drag and drop logic
				// will drag the selected items.
				// 
				UltraGrid sourceGrid = null != this.Header ? this.Header.Band.Layout.Grid as UltraGrid : null;
				if ( null != sourceGrid )
					sourceGrid.ClearSelectedColumns( );

				if ( this.Enabled )
					return base.OnMouseDown( e, adjustableArea, ref captureMouseForElement );

				return true;
			}

			#endregion // OnMouseDown

			#region OnMouseUp

			/// <summary>
			/// Called when the mouse up message is received over the element. 
			/// </summary>
			/// <param name="e">Mouse event arguments</param>
			protected override bool OnMouseUp( MouseEventArgs e )
			{
				// Clicking on a header in the column chooser should not sort the column.
				// 
				this.sortClickPending = false;

				bool ret = base.OnMouseUp( e );

				UltraGridRow row = (UltraGridRow)this.GetContext( typeof( UltraGridRow ), true );
				if ( null != row && row.IsActiveRow && ! row.Layout.Disposed )
					row.Layout.DirtyGridElement( );

				return ret;
			}

			#endregion // OnMouseUp

			#region OnDoubleClick

            /// <summary>
            /// Called when the mouse is double clicked on this element.
            /// </summary>
            /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
            protected override void OnDoubleClick(bool adjustableArea)
			{
				// When double clicked toggle the hidden state of the associated column or band.
				// 
				if ( this.sourceGridHeader is ColumnHeader )
				{
					UltraGridColumn column = this.sourceGridHeader.Column;
					if ( null != column && this.Enabled )
						column.Layout.Grid.ColumnChooser_SetHidden( column, ! column.Hidden );
				}
				else if ( this.sourceGridHeader is BandHeader )
				{
					// Find out if the user is allowed to change the hidden state of the band.
					// 
					bool bandHidingAllowed = false;
					RowCellAreaUIElement rowCellArea = (RowCellAreaUIElement)this.GetAncestor( typeof( RowCellAreaUIElement ) );
					if ( null != rowCellArea && null != rowCellArea.Row 
						&& rowCellArea.Row.Cells.Exists( UltraGridColumnChooser.VISIBLE_COLUMN ) )
					{
						CellUIElementBase cellElem = (CellUIElementBase)rowCellArea.GetDescendant( typeof( CellUIElementBase ), rowCellArea.Row.Cells[ UltraGridColumnChooser.VISIBLE_COLUMN ] );
						bandHidingAllowed = null != cellElem && cellElem.Enabled && null != cellElem.FindEmbeddableUIElement( );
					}

					UltraGridBand band = this.sourceGridHeader.Band;
					if ( bandHidingAllowed && null != band )
						band.Layout.Grid.ColumnChooser_SetHidden( band, ! band.Hidden );
				}
			}

			#endregion // OnDoubleClick

			#region Adjustable

			public override bool Adjustable
			{
				get
				{
					return false;
				}
			}

			#endregion // Adjustable

			#region IsColumnChooserHeader

			internal override bool IsColumnChooserHeader
			{
				get
				{
					return true;
				}
			}

			#endregion // IsColumnChooserHeader

			#region HeaderState

			/// <summary>
			/// Indicates the state of the header.
			/// </summary>
			protected override UIElementButtonState HeaderState
			{
				get
				{
					return this.Enabled ? base.HeaderState : UIElementButtonState.Inactive;
				}
			}

			#endregion // HeaderState
		}

		#endregion // GridHeaderElement

		#region BeforeCreateChildElements

		public virtual bool BeforeCreateChildElements(Infragistics.Win.UIElement parent)
		{
			// Make sure the column chooser grid is initialized to the proper list of columns.
			// We are lazily initializing columns list.
			// 
			if ( parent is UltraGridUIElement && null != this.ultraGridColumnChooser )
				this.ultraGridColumnChooser.VerifyInitialized( );

			// We need to reuse the old grid header elem so clicks, mouse downs, up all get
			// delivered to the same header elem.
			// 
			RowCellAreaUIElement rowCellArea = parent as RowCellAreaUIElement;
			if ( null != rowCellArea )
				this.extractedGridHeaderElem = (GridHeaderElement)GridUtils.ExtractExistingElement( 
										rowCellArea.ChildElements, typeof( GridHeaderElement ), true );

			return false;
		}

		#endregion // BeforeCreateChildElements

		#region AfterCreateChildElements

		private GridHeaderElement CreateNewGridHeaderElement( UIElement parent, HeaderBase header )
		{
			GridHeaderElement elem = null;

			if ( null != this.extractedGridHeaderElem
				&& ! this.extractedGridHeaderElem.Disposed
				&& parent == this.extractedGridHeaderElem.Parent 
				&& this.extractedGridHeaderElem.Header == header )
			{
				elem = this.extractedGridHeaderElem;
				this.extractedGridHeaderElem = null;
			}

			if ( null == elem )
				elem = new GridHeaderElement( parent, header );

			return elem;
		}

		public virtual void AfterCreateChildElements(Infragistics.Win.UIElement parent)
		{
			// In each row position the column or band header that row is supposed to
			// represent (by looking at the associated VALUE_COLUMN cell's value). Each
			// row represents a single band or column header.
			// 

            RowCellAreaUIElement rowCellArea = parent as RowCellAreaUIElement;
			if ( null != rowCellArea )
			{
				for ( int i = rowCellArea.ChildElements.Count - 1; i >= 0; i-- )
				{
					CellUIElement cellElem = rowCellArea.ChildElements[i] as CellUIElement;
					if ( null != cellElem && null != cellElem.Column 
						&& UltraGridColumnChooser.VALUE_COLUMN == cellElem.Column.Key )
					{
						Rectangle elemRect = cellElem.Rect;
						rowCellArea.ChildElements.RemoveAt( i );
						UIElement elem = null;
						UltraGridRow row = rowCellArea.Row;

						UltraGridCell columnCell = row.Cells[ UltraGridColumnChooser.VALUE_COLUMN ];
						UltraGridCell visibleCell = row.Cells.Exists( UltraGridColumnChooser.VISIBLE_COLUMN ) 
							? row.Cells[ UltraGridColumnChooser.VISIBLE_COLUMN ] : null;
						object obj = UltraGridColumnChooser.ColumnBandHolder.GetWrappedValue( columnCell );

						// MD 8/7/07 - 7.3 Performance
						// FxCop - Do not cast unnecessarily
						//if ( obj is UltraGridColumn )
						//{
						//    UltraGridColumn sourceColumn = (UltraGridColumn)obj;
						UltraGridColumn sourceColumn = obj as UltraGridColumn;

						if ( sourceColumn != null )
						{
							elem = this.CreateNewGridHeaderElement( rowCellArea, sourceColumn.Header );

							// If the band is hidden the the activation of the column cells will be
							// disabeld. In which case show the columns disabled since if the band is
							// hidden there is no point in hiding/unhding its columns.
							// 
							elem.Enabled = Activation.Disabled != columnCell.ActivationResolved;
						}
						// MD 8/7/07 - 7.3 Performance
						// FxCop - Do not cast unnecessarily
						//else if ( obj is UltraGridBand )
						//{
						//    UltraGridBand sourceBand = (UltraGridBand)obj;
						else 
						{
							UltraGridBand sourceBand = obj as UltraGridBand;

							if ( sourceBand != null )
							{
								elem = this.CreateNewGridHeaderElement( rowCellArea, sourceBand.Header );

								// Give indication that the header is a band header and that it can't be dragged.
								// 
								elem.Enabled = false;

								// This is for not displaying the the hide band check box for the root band
								// since it doesn't make any sense to hide the root band. Also do the same
								// if the style is AllColumnsAndChildBandsWithCheckBoxes. Visible cells are
								// hidden in the displayGrid_InitializeRow if these conditions are true so
								// all we have to do here is to look at the resolved hidden state of the cell.
								//
								if ( visibleCell.InternalHidden )
								{
									rowCellArea.ChildElements.Clear( );
									elemRect = rowCellArea.Rect;
								}
							}
						}

						Debug.Assert( null != elem );
						if ( null != elem )
						{
							elem.Rect = elemRect;
							rowCellArea.ChildElements.Add( elem );
						}

						break;
					}
				}
			}
		}

		#endregion // AfterCreateChildElements
	}

	#endregion // ColumnChooserGridCreationFilter Class

	#region ColumnChooserGridDrawFilter Class

	internal class ColumnChooserGridDrawFilter : IUIElementDrawFilter
	{
		private UltraGridColumnChooser ultraGridColumnChooser = null;

		internal ColumnChooserGridDrawFilter( UltraGridColumnChooser ultraGridColumnChooser )
		{
			GridUtils.ValidateNull( ultraGridColumnChooser );

			this.ultraGridColumnChooser = ultraGridColumnChooser;
		}

		public Infragistics.Win.DrawPhase GetPhasesToFilter(ref Infragistics.Win.UIElementDrawParams drawParams)
		{
			if ( drawParams.Element is UltraGridUIElement )
				return DrawPhase.AfterDrawChildElements;

			return DrawPhase.None;
		}

		public bool DrawElement(Infragistics.Win.DrawPhase drawPhase, ref Infragistics.Win.UIElementDrawParams drawParams)
		{
			UIElement elem = drawParams.Element;
			if ( DrawPhase.AfterDrawChildElements == drawPhase && elem is UltraGridUIElement )
			{
				if ( this.ultraGridColumnChooser.drawAsActive )
				{
					drawParams.AppearanceData.BorderColor = Color.Red;

					Rectangle rect = elem.Rect;
					drawParams.DrawBorders( 
						UIElementBorderStyle.Solid, 
						Border3DSide.All,
						rect );

					rect.Inflate( -1, -1 );

					drawParams.DrawBorders( 
						UIElementBorderStyle.Solid, 
						Border3DSide.All,
						rect );
				}
			}

			return false;
		}	
	}

	#endregion // ColumnChooserGridDrawFilter Class

	#region ColumnChooserButtonUIElement

	/// <summary>
	/// UI element class for the column chooser button element.
	/// </summary>
	/// <remarks>
	/// <p class="body">UI element class for the column chooser button element. This
	/// element is displayed in the column headers area above the row selectors when
	/// the <see cref="UltraGridOverride.RowSelectorHeaderStyle"/> property is set to
	/// <b>ColumnChooserButton</b> value.</p>
	/// <seealso cref="ColumnChooserDialog"/> <seealso cref="UltraGridColumnChooser"/> <seealso cref="UltraGridOverride.RowSelectorHeaderStyle"/>
	/// </remarks>
	public class ColumnChooserButtonUIElement : ImageAndTextButtonUIElement
	{
		#region Private/Internal Variables

		internal const int DEFAULT_COLUMN_CHOOSER_BUTTON_SIZE = 18;

		#endregion // Private/Internal Variables

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="parent">The parent element</param>
		public ColumnChooserButtonUIElement( UIElement parent ) : base( parent )
		{
			this.ElementClick += new UIElementEventHandler( this.ButtonClicked );
		}

		#endregion // Constructor

		#region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appData">The appearance structure to initialize</param>
        /// <param name="flags">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appData, ref AppearancePropFlags flags)
		{
			UltraGridBand band = this.Band;
			if ( null != band )
			{
				band.ResolveRowSelectorHeaderAppearance( ref appData, ref flags, false, false );

				// Since this is a button, give the base implementation a chance at resolving
				// appearance.
				// 
				base.InitAppearance( ref appData, ref flags );

				if ( 0 != ( AppearancePropFlags.Image & flags ) )
				{
					appData.Image = band.Layout.ColumnChooserButtonImageFromResource;
					flags ^= AppearancePropFlags.Image;
				}

				GridUtils.ResolveAlignments( ref appData, ref flags, HAlign.Center, VAlign.Middle, HAlign.Center, VAlign.Middle );
			}
		}

		#endregion // InitAppearance

		#region Band

		/// <summary>
		/// Returns the associated band.
		/// </summary>
		public UltraGridBand Band
		{
			get
			{
				return (UltraGridBand)this.GetContext( typeof( UltraGridBand ), true );
			}
		}

		#endregion // Band

		#region PositionChildElements

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UltraGridBand band = this.Band;

			// Since this button element only displays image use the ImageHAlign and ImageVAlign
			// instead of TextHAlign and TextVAlign settings. For that set UseTextPlacement to false.
			//
			this.UseTextPlacement = false;

			// Set the button style to the resolved button style.
			//
			// SSP 3/27/06 - App Styling
			// Pass in the role related information in order to get the button style setting off the
			// currect role.
			// 
			//this.Style = band.ButtonStyleResolved;
			this.Style = band.GetButtonStyleResolved( StyleUtils.Role.ColumnChooserButton, StyleUtils.CachedProperty.ButtonStyleColumnChooserButton );

			// Use the RowSelectorHeaderAppearance' image. Since the row selector header can contain
			// only a single element there is no need for having a separate appearance for the 
			// column chooser button.
			//
			AppearanceData appData = new AppearanceData( );
			AppearancePropFlags flags = AppearancePropFlags.Image;
			this.InitAppearance( ref appData, ref flags );

			this.Image = appData.GetImage( band.Layout.Grid.ImageList );

			// Scale down the image if necessary.
			//
			this.ScaleImage = ScaleImage.OnlyWhenNeeded;

			// Call the base and let it position it's child elements.
			//
			base.PositionChildElements( );

			// Show tool tip when the mouse is hovered over the element.
			// 
			string toolTipText = SR.GetString( "ColumnChooserButtonToolTip" );
			this.ToolTipItem = GridUtils.CreateToolTipItem( toolTipText, this.ToolTipItem );
		}

		#endregion // PositionChildElements

		#region ButtonClicked

		private void ButtonClicked( object sender, UIElementEventArgs e )
		{
			UltraGridBand band = this.Band;
			UltraGridBase grid = null != band ? band.Layout.Grid : null;
			Debug.Assert( null != grid );
			if ( null != grid )
				grid.ShowColumnChooser( band, true );
		}

		#endregion // ButtonClicked

		#region OnDispose

		/// <summary>
		/// Overridden. Called when the object is being disposed.
		/// </summary>
		protected override void OnDispose( )
		{
			this.ElementClick -= new UIElementEventHandler( this.ButtonClicked );
		}

		#endregion // OnDispose

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Band, StyleUtils.Role.ColumnChooserButton );
			}
		}

		#endregion // UIRole
	}

	#endregion // ColumnChooserButtonUIElement

	#region ColumnChooserSourceGridFilter

	/// <summary>
	/// ReferenceConverter used for UltraGridColumnChooser's SourceGrid property.
	/// </summary>
	public class ColumnChooserSourceGridFilter : ComponentConverter
	{
		#region Constructor

		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="type">A System.Type that represents the type to associate with this component converter.</param>
		public ColumnChooserSourceGridFilter( System.Type type ) : base( type )
		{
		}

		#endregion // Constructor

		#region IsValueAllowed

		/// <summary>
		/// Returns a value indicating whether a particular value can be added to the standard values collection.
		/// </summary>
        /// <param name="context">The ITypeDescriptorContext</param>
		/// <param name="value">The value</param>
		/// <returns>True if the value is allowed and can be added to the standard values collection; false if the value cannot be added to the standard values collection.</returns>
		protected override bool IsValueAllowed( ITypeDescriptorContext context, object value )
		{
			UltraGrid grid = value as UltraGrid;
			if ( null != grid && grid.Parent is UltraGridColumnChooser )
				return false;

			return base.IsValueAllowed( context, value );
		}

		#endregion // IsValueAllowed

//		#region GetPropertiesSupported
//
//		/// <summary>
//		/// Returns false so the expansion indicator doesn't show up in the property grid. 
//		/// </summary>
//		public override bool GetPropertiesSupported( ITypeDescriptorContext context )
//		{
//			return false;
//		}
//
//		#endregion // GetPropertiesSupported
	}

	#endregion // ColumnChooserSourceGridFilter

}
