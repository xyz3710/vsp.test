#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.Globalization;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.IO;
	using System.Runtime.Serialization;
	using System.Runtime.Serialization.Formatters.Binary;
	using Infragistics.Win.UltraWinMaskedEdit;
	using System.Security.Permissions;

	// AS - 12/19/01
	using Infragistics.Win.UltraWinScrollBar;
	
	using Infragistics.Shared.Serialization;
	using System.Runtime.Serialization.Formatters.Soap;

	// SSP 8/31/04 - UltraCalc
	//
	using Infragistics.Win.CalcEngine;
	using System.Collections.Generic;

	/// <summary>
	/// The main display layout of an <see cref="Infragistics.Win.UltraWinGrid.UltraGrid"/>, <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> or <see cref="Infragistics.Win.UltraWinGrid.UltraDropDown"/> control. 
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>DisplayLayout</b> property of an object is used to access the UltraGridLayout object that determines the settings of various properties related to the appearance and behavior of the object. The UltraGridLayout object provides a simple way to maintain multiple layouts for the grid and apply them as needed. You can also save grid layouts to disk, the registry or a storage stream and restore them later.</p>
	/// <p class="body">The UltraGridLayout object has properties such as <b>Appearance</b> and <b>Override</b>, so the UltraGridLayout object has sub-objects of these types, and their settings are included as part of the layout. However, the information that is actually persisted depends on how the settings of these properties were assigned. If the properties were set using the UltraGridLayout object's intrinsic objects, the property settings will be included as part of the layout. However, if a named object was assigned to the property from a collection, 
	/// the layout will only include the reference into the collection, not the actual settings of the named object. (For an overview of the difference between named and intrinsic objects, please see the <see cref="UltraGridLayout.Appearance"/>property.</p>
	/// <p class="body">For example, if the Layout object's <b>Appearance</b> property is used to set values for the intrinsic Appearance object like this:</p>
	/// <p class="code">UltraGrid1.DisplayLayout.Appearance.ForeColor = vbBlue</p>
	/// <p class="body">Then the setting (in this case, <b>ForeColor</b>) will be included as part of the layout, and will be saved, loaded and applied along with the other layout data. However, suppose you apply the settings of a named object to the UltraGridLayout's <b>Appearance</b> property in this manner:</p>
	/// <p class="code">UltraWinGrid1.Appearances.Add "New1"</p>
	/// <p class="code">UltraWinGrid1.Appearances("New1").ForeColor = vbBlue</p>
	/// <p class="code">UltraWinGrid1.Layout.Appearance = UltraWinGrid1.Appearances("New1")</p>
	/// <p class="body">In this case, the ForeColor setting will not be persisted as part of the layout. Instead, the layout will include a reference to the "New1" Appearance object and use whatever setting is present in that object when the layout is applied.</p>
	/// <p class="body">By default, the layout includes a copy of the entire Appearances collection, so if the layout is saved and restored using the default settings, the object should always be present in the collection when it is referred to. However, it is possible to use the <b>Load</b> and <b>Save</b> methods of the UltraGridLayout object in such a way that the collection will not be re-created when the layout is applied. If this is the case, and the layout contains a reference to a nonexistent object, the default settings for that object's properties will be used.</p>
	///	<p></p>
	/// <p class="body">
	/// The following sample code shows how to set some UltraGridLayout properties in the InitializeLayout event.
	/// </p>
	/// <p></p>
	/// <pre>
	/// Private Sub UltraGrid1_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UltraGrid1.InitializeLayout
	/// 
	///     e.Layout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.MultiBand
	///     e.Layout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
	/// 
	///     e.Layout.AutoFitStyle = AutoFitStyle.ResizeAllColumns
	///     e.Layout.MaxRowScrollRegions = 3
	///     e.Layout.CaptionAppearance.ForeColor = Color.Red
	/// 
	///     e.Layout.GroupByBox.BandLabelBorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted
	///     e.Layout.GroupByBox.ShowBandLabels = Infragistics.Win.UltraWinGrid.ShowBandLabels.IntermediateBandsOnly
	/// 
	///     e.Layout.Override.CellAppearance.BackColor = Color.White
	///     e.Layout.Override.CellAppearance.BackColor2 = Color.Blue
	///     e.Layout.Override.CellAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.VerticalBump
	/// 
	///     e.Layout.AddNewBox.Hidden = False
	///     e.Layout.AddNewBox.Style = Infragistics.Win.UltraWinGrid.AddNewBoxStyle.Compact
	///     e.Layout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.PopupBorderless
	///     e.Layout.AddNewBox.ButtonConnectorStyle = Infragistics.Win.UIElementBorderStyle.Dotted
	/// 
	/// End Sub
	/// </pre>
	/// </remarks>
	[ TypeConverter( typeof( UltraGridLayout.DisplayLayoutTypeConverter ) ) ]
	[Serializable()]
	public class UltraGridDisplayLayout : UltraGridLayout
	{
		/// <summary>
		/// Contructor
		/// </summary>
		public UltraGridDisplayLayout() : base()
		{
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal UltraGridDisplayLayout(SerializationInfo info, StreamingContext context) : base(info, context)
		protected UltraGridDisplayLayout(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		/// <summary>
		/// Returns an empty string.
		/// </summary>
        /// <returns>An empty string.</returns>
		public override String ToString() 
		{
			return string.Empty;
		}
	}
	
	/// <summary>
	/// An object that maintains the structure and settings for an <see cref="Infragistics.Win.UltraWinGrid.UltraGrid"/>, <see cref="Infragistics.Win.UltraWinGrid.UltraCombo"/> or <see cref="Infragistics.Win.UltraWinGrid.UltraDropDown"/> control. 
	/// </summary>
	/// <remarks>
	/// <p class="body">The <b>DisplayLayout</b> property of an object is used to access the UltraGridLayout object that determines the settings of various properties related to the appearance and behavior of the object. The UltraGridLayout object provides a simple way to maintain multiple layouts for the grid and apply them as needed. You can also save grid layouts to disk, the registry or a storage stream and restore them later.</p>
	/// <p class="body">The UltraGridLayout object has properties such as <b>Appearance</b> and <b>Override</b>, so the UltraGridLayout object has sub-objects of these types, and their settings are included as part of the layout. However, the information that is actually persisted depends on how the settings of these properties were assigned. If the properties were set using the UltraGridLayout object's intrinsic objects, the property settings will be included as part of the layout. However, if a named object was assigned to the property from a collection, 
	/// the layout will only include the reference into the collection, not the actual settings of the named object. (For an overview of the difference between named and intrinsic objects, please see the <see cref="UltraGridLayout.Appearance"/>property.</p>
	/// <p class="body">For example, if the Layout object's <b>Appearance</b> property is used to set values for the intrinsic Appearance object like this:</p>
	/// <p class="code">UltraGrid1.DisplayLayout.Appearance.ForeColor = vbBlue</p>
	/// <p class="body">Then the setting (in this case, <b>ForeColor</b>) will be included as part of the layout, and will be saved, loaded and applied along with the other layout data. However, suppose you apply the settings of a named object to the UltraGridLayout's <b>Appearance</b> property in this manner:</p>
	/// <p class="code">UltraWinGrid1.Appearances.Add "New1"</p>
	/// <p class="code">UltraWinGrid1.Appearances("New1").ForeColor = vbBlue</p>
	/// <p class="code">UltraWinGrid1.Layout.Appearance = UltraWinGrid1.Appearances("New1")</p>
	/// <p class="body">In this case, the ForeColor setting will not be persisted as part of the layout. Instead, the layout will include a reference to the "New1" Appearance object and use whatever setting is present in that object when the layout is applied.</p>
	/// <p class="body">By default, the layout includes a copy of the entire Appearances collection, so if the layout is saved and restored using the default settings, the object should always be present in the collection when it is referred to. However, it is possible to use the <b>Load</b> and <b>Save</b> methods of the UltraGridLayout object in such a way that the collection will not be re-created when the layout is applied. If this is the case, and the layout contains a reference to a nonexistent object, the default settings for that object's properties will be used.</p>
	///	<p></p>
	/// <p class="body">
	/// The following sample code shows how to set some UltraGridLayout properties in the InitializeLayout event.
	/// </p>
	/// <p></p>
	/// <pre>
	/// Private Sub UltraGrid1_InitializeLayout(ByVal sender As System.Object, ByVal e As Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs) Handles UltraGrid1.InitializeLayout
	/// 
	///     e.Layout.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.MultiBand
	///     e.Layout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy
	/// 
	///     e.Layout.AutoFitStyle = AutoFitStyle.ResizeAllColumns
	///     e.Layout.MaxRowScrollRegions = 3
	///     e.Layout.CaptionAppearance.ForeColor = Color.Red
	/// 
	///     e.Layout.GroupByBox.BandLabelBorderStyle = Infragistics.Win.UIElementBorderStyle.Dotted
	///     e.Layout.GroupByBox.ShowBandLabels = Infragistics.Win.UltraWinGrid.ShowBandLabels.IntermediateBandsOnly
	/// 
	///     e.Layout.Override.CellAppearance.BackColor = Color.White
	///     e.Layout.Override.CellAppearance.BackColor2 = Color.Blue
	///     e.Layout.Override.CellAppearance.BackGradientStyle = Infragistics.Win.GradientStyle.VerticalBump
	/// 
	///     e.Layout.AddNewBox.Hidden = False
	///     e.Layout.AddNewBox.Style = Infragistics.Win.UltraWinGrid.AddNewBoxStyle.Compact
	///     e.Layout.AddNewBox.ButtonStyle = Infragistics.Win.UIElementButtonStyle.PopupBorderless
	///     e.Layout.AddNewBox.ButtonConnectorStyle = Infragistics.Win.UIElementBorderStyle.Dotted
	/// 
	/// End Sub
	/// </pre>
	/// </remarks>
	[ TypeConverter( typeof( UltraGridLayout.LayoutTypeConverter ) ) ]
	[ Serializable() ]

	public class UltraGridLayout : KeyedSubObjectBase,
		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//	
		//IScrollableRowCountManagerOwner,
		ICloneable,
		ISelectionStrategyProvider, 
		ISerializable,
		IImageListProvider,

		//MRS 2/23/04
		//Add ISupportPresets
		ISupportPresets
    {
        #region Private Members

        // This is by how much the back color used for summaries will be
		// shaded.
		//
		internal const int SUMMARY_FOOTER_SHADE_AMOUNT = 20;

		private Infragistics.Win.AppearanceHolder[]            appearanceHolders = null;

		private UltraGridBase								   grid = null;
		private Infragistics.Win.UltraWinGrid.UltraGridOverride         overrideObj = null;
		private Infragistics.Win.UltraWinGrid.BandsCollection  bands = null;
		private Infragistics.Win.UltraWinGrid.BandsSerializer  loadedBands = null;
		private Infragistics.Win.UltraWinGrid.RowsCollection   rows = null;
		// SSP 1/22/04 - Optimization
		// Removed groupByStyleVersion.
		//
		//private int										groupByStyleVersion = 0;
		private int									    bandCardViewVersion = 0;
		private ColScrollRegionsCollection              colScrollRegions = null;
		private RowScrollRegionsCollection              rowScrollRegions = null;
		private ValueListsCollection					valuelists = null;
		private AppearancesCollection					appearances = null;
		private SelectionStrategyBase					selectionStrategyCell = null;
		private SelectionStrategyBase					selectionStrategyRow = null;
		private SelectionStrategyBase					selectionStrategyGroupByRow = null;
		private SelectionStrategyBase					selectionStrategyColumn = null;
		
		// SSP 4/5/02
		// Associated property was being used anywhere
		//private bool									editModeExited;
		
		private UltraGridUIElement						element = null;

		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		// Obsoleted AutoFitColumns and added AutoFitStyle property.
		//
		//private bool									autoFitColumns = false;
		private AutoFitStyle							autoFitStyle = AutoFitStyle.None;
		
		private ViewStyleBase                           viewStyleImpl = null;

		private Infragistics.Win.UltraWinGrid.Scrollbars       scrollbars;
		private Infragistics.Win.UltraWinGrid.ViewStyle        viewStyle;
		private Infragistics.Win.UltraWinGrid.ViewStyleBand    viewStyleBand;
		private bool									snaking = false;

		private Infragistics.Win.UIElementBorderStyle	borderStyle;
		private int										interBandSpacing;
		private int										maxColScrollRegions;
		private int										maxRowScrollRegions;
		private System.Drawing.Color					rowConnectorColor;
		private RowConnectorStyle						rowConnectorStyle;
		private TabNavigation							tabNavigation;
		private Infragistics.Win.UIElementBorderStyle	borderStyleCaption;	
        
		//private bool                                    elementPrimaryMetricsDirty;
 
		private int                                     bandsOverallExtent;
		// SSP 8/15/05 BR04633 BR04666
		// Made internal.
		// 
		//private bool									synchronizedColListsDirty;
		internal bool									synchronizedColListsDirty;

		private int										cachedCaptionHeight;
		private int										cachedFontHeight = 0;
		private int										cachedBaseFontHeight = 0;
		private bool									initializingAllSubItems = false;
		private bool                                    scrollingColumns = false;		
		private bool                                    scrollColIntoViewPending = false;
		private bool									loadedBandsApplied = false;
		private AddNewBox								addNewBox = null;
		private GroupByBox								groupByBox = null;

		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//	
		//private ScrollableRowCountManager               scrollableRowCountManager = null;
		//		private bool									isHTMLInitialized = false;
		
		
		private bool									textControlPositionDirty = false;
		
		//private bool									selectCellsInRectangle = false;
		private bool									gettingFirstCell = false;
		private bool									togglingByKybd = false;

		// SSP 4/29/02
		// EM Embeddable editors.
		// Below three flags are not used anymore so commented them out. They were being used
		// for date time drop down calendar. Since all the necessary code is moved down into
		// the DateTime embeddable editor, they are not needed.
		//
		
		
		private bool ignoreDataSourePositionChange = false;

		
		// SSP 4/26/02
		// EM Embeddable editors.
		// Commented out below variable declarations.
		//
				
		
		// SSP 2/19/04
		//
		//private int scrollRowPositionVersion			= 0;
		
		// SSP 3/19/02
		// Added a version number for causing all the scrollable row count managers
		// in the hierarchy to recalcuate their row counts. This is used for row filtering
		// as rows in rows collections get filtered out, we need to recalcuate the 
		// row counts for all the associated scrollable row count managers.
		//
		private int scrollableRowCountVersion			= 0;


		// AS - 12/19/01
		private ScrollBarLook							scrollbarLook = null;

		// JJD 1/16/02 - UWG880
		// Added special cursor for autosizing columns
		//
		private System.Windows.Forms.Cursor				autoSizeCursor = null;

		// SSP 11/26/03 UWG2344
		// Added PerformAutoSize to the UltraGridRow and perform auto size functionality for rows.
		//
		System.Windows.Forms.Cursor						rowAutoSizeCursor = null;

		// JJD 1/17/02
		// Added line up and line down cursors
		//
		private System.Windows.Forms.Cursor				lineUpCursor = null;
		private System.Windows.Forms.Cursor				lineDownCursor = null;
		

		// SSP 11/28/01	UWG769
		// Keep a version number to force repositioning of child elements
		// of all the cell ui elements. This is needed when let's say
		// RowAppearance.Image has changed. Normally we cache the cell 
		// elements so when the grid is dirtied, we don't call
		// PositionChildElements on all the cells which would be very
		// inefficient, especially when scrolling. However, we need to
		// reposition at times like when RowAppearance.Image has changed.
		//
		private int cellChildElementsCacheVersion = 0;

		// SSP 6/13/05 BR04539
		// 
		private int rowChildElementsCacheVersion = 0;
		
		// SSP 2/28/02
		// Added ScrollStyle for vertical scrolling to allow for immediate
		// scrolling of rows when thumb is tracked.
		//
		Infragistics.Win.UltraWinGrid.ScrollStyle scrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Deferred;

		// SSP 3/29/02
		// Added a flag that will prevent CellChange from firing when an item
		// from value list is selected. We only want to fire the cell change
		// event when the user actually types into a text box, and not when
		// a new item is selected from a vlaue list or month view calendar.
		//
		private bool dontFireCellChangeEvent = false;

		// SSP 5/31/02 UWG1152
		// Added MaxBandDepth property and associated state variable.
		//
		private int maxBandDepth = UltraGridBand.MAX_BAND_DEPTH; // Use the default depth of 100
		private int	maxBandDepthCurrentlyEnforced = UltraGridBand.MAX_BAND_DEPTH;


		// SSP 4/5/02
		//
		private EditorWithMask		editorWithMask = null;
		private EditorWithText		editorWithText = null;
		private ColorPickerEditor	colorPickerEditor = null;
		private Infragistics.Win.CheckEditor checkEditor = null;
		// SSP 7/17/02
		// Added a new CheckEditorTriState property for tri state check box style.
		//
		private Infragistics.Win.CheckEditor checkEditorTriState = null;
		private Infragistics.Win.DateTimeEditor dateTimeEditor = null;
		// SSP 8/11/03 - Embeddable image renderer
		//
		private Infragistics.Win.EmbeddableImageRenderer imageRenderer = null;
		private EmbeddableUIElementBase embeddableElementBeingEdited = null;
		private RowScrollRegion         embeddableElementBeingEdited_RowScrollRegion = null;
		private ColScrollRegion         embeddableElementBeingEdited_ColScrollRegion = null;
		private bool cancellingEditOperationFlag	= false;
		private bool forceExitEditOperationFlag		= false;
		private EmbeddableEditorBase lastHookedEditor = null;
		private CancelEventHandler		embeddableEditor_BeforeEnterEditModeEventHandler = null;
		private EventHandler			embeddableEditor_AfterEnterEditModeEventHandler  = null;
		private Infragistics.Win.BeforeExitEditModeEventHandler
			embeddableEditor_BeforeExitEditModeEventHandler = null;
		private EventHandler			embeddableEditor_AfterExitEditModeEventHandler = null;
		private CancelEventHandler		embeddableEditor_BeforeDropDownEventHandler	= null;
		private EventHandler			embeddableEditor_AfterCloseupEventHandler = null;
		private KeyEventHandler		embeddableEditor_KeyDownEventHandler = null;
		private KeyEventHandler		embeddableEditor_KeyUpEventHandler = null;
		private KeyPressEventHandler		embeddableEditor_KeyPressEventHandler = null;		
		// SSP 10/11/02 UWG1749
		// Added event handler for SelectionChanged of the embeddable editor so that we
		// can fire CellListSelect event on the grid.
		//
		private EventHandler embeddableEditor_SelectionChangedEventHandler = null;

		// SSP 6/11/02
		// Added PriorityScrolling property.
		//
		private bool priorityScrolling = false;

		// SSP 6/21/02
		//
		private Image						filterDropDownButtonImage = null;
		
		// SSP 7/19/02 UWG1364
		// Added activeFilterDropDownButtonImage and defaultActiveFilterDropDownButtonImage
		// variables.
		//
		private Image						filterDropDownButtonImageActive = null;
		private Image						defaultActiveFilterDropDownButtonImage = null;

		private Image						sigmaImage = null;
		private Image						filterDropDownButtonImageFromResource = null;
		private Image						sigmaImageFromResource = null;
		private Image						cardExpansionImage = null;
		private Image						cardExpansionImageInverse = null;

		// SSP 7/31/02
		// This version number gets incremeneted whenever the row filtering mode changes 
		// because the filter conditions that get applied also changes.
		//
		private int rowFiltersVersionNumber = 0;


		// SSP 7/25/02 UWG1407
		// Added dontUpdateWhenSelecting to prevent the grid's InternalSelectItem from
		// calling Update. What happens when tabbing from cell to cell, the cell that
		// gets tabbed into gets activated first (and as a result selected) before 
		// going into edit mode. So if the InternalSelectItem updates the control for
		// a split second the cell look selected right before it goes into edit mode.
		// 
		private bool dontUpdateWhenSelecting = false;

		// SSP 8/8/02
		// AutoSizeEdit feature for embeddable editors,
		//
		private CancelableAutoSizeEditEventArgs lastAutoSizeEditEventArgs = null;

		// SSP 12/23/02
		// Added code to cache the resolved appearance of a cell since for a single cell
		// resolve cell appearance gets called at least twice.
		//
		private UltraGridRow      lastResolvedCellRow = null;
		private UltraGridColumn   lastResolvedCellColumn = null;
		private AppearanceData    lastResolvedCellAppData = new AppearanceData( );
		private AppearancePropFlags lastResolvedCellFlags = 0;

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		private bool lastResolvedHotTrackingRow  = false;
		private bool lastResolvedHotTrackingCell = false;

		// SSP 1/6/02 UWG1903
		// Anti-recursion flags.
		//
		private bool in_EnsureDataSourceAttached = false;
		internal bool dataSourceAttachedAtLeastOnce = false;

		// SSP 1/14/03 UWG1892
		// Added retainRowPropertyCategories parameter to Print and PrintPreview methods for
		// making WYSIWIG style printing. 
		//
		private RowPropertyCategories retainRowPropertyCategories = RowPropertyCategories.None;

		// SSP 2/28/03 - Row Layout Functionality
		// 
		private int rowLayoutVersion = 0;

		// SSP 5/7/03 - Excel Exporting Functionality
		// Added IsExportLayout method.
		//
		private bool isExportLayout = false;

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		private UltraGridCell cellGoingIntoEditMode = null;

		// SSP 9/19/03 UWG2579
		// This will be set by the UltraGridCell.EnterEditorIntoEditMode method to indicate that
		// the cell is entering the edit mode via mouse click (rather than tab or something else).
		// This is used by the UltraGridCell.OnAfterCellEnterEditMode method to select all the
		// text in the editor. This will be reset by the UltraGridCell.EnterEditorIntoEditMode 
		// imediately after the call to EmbeddableEditorBase.EnterEditMode returns.
		//
		internal bool goingIntoEditModeByMouse = false;

		// SSP 5/19/03 - Fixed headers
		// Added UseFixedHeaders property.
		//
		private bool useFixedHeaders = false;
		private Image fixedHeaderOnImage = null;
		private Image fixedHeaderOffImage = null;
		private Image fixedHeaderOnImageFromResource = null;
		private Image fixedHeaderOffImageFromResource = null;

		// SSP 3/29/05 - NAS 5.2 Fixed Rows
		//
		private Image fixedRowOnImage = null;
		private Image fixedRowOffImage = null;

		// SSP 5/22/03
		// Added a property to allow the user to control the small change of the column scroll bar.
		//
		private int columnScrollbarSmallChange = ColScrollRegion.HORIZONTAL_SCROLLBAR_SMALL_CHANGE;

		// SSP 7/21/03 UWG1997
		// Added UseScrollWindow property so that the user can prevent the grid from using 
		// ScrollWindow method for scrolling.
		//
		// SSP 8/4/03
		// Default should be both because by default we want to use the scroll window.
		//
		//private Infragistics.Win.UltraWinGrid.UseScrollWindow useScrollWindow = Infragistics.Win.UltraWinGrid.UseScrollWindow.None;
		private Infragistics.Win.UltraWinGrid.UseScrollWindow useScrollWindow = Infragistics.Win.UltraWinGrid.UseScrollWindow.Both;

		// SSP 11/24/03 - Scrolling Till Last Row Visible
		// Added the functionality to not allow the user to scroll any further once the
		// last row is visible.
		//
		private Infragistics.Win.UltraWinGrid.ScrollBounds scrollBounds = ScrollBounds.ScrollToLastItem;

		// SSP 8/12/03 - Optimizations - Grand Verify Version Number
		//
		private int grandVerifyVersion = 0;

		// SSP 12/2/05 BR07897
		// 
		private int scrollToFillVersion = 0;

		// SSP 3/15/04 - Designer Usability Improvements Related
		// 
		private bool extractDataStructureOnly = false;

		// SSP 4/13/04 - Virtual Binding
		// Added LoadStyle property to UltraGridLayout.
		//
		private LoadStyle loadStyle = LoadStyle.PreloadRows;

		// SSP 6/29/04 - UltraCalc
		// Added calcReference member var.
		//
		private GridReference calcReference = null;
		private int calcManagerNotificationsSuspended_Counter = 0;
         
		// SSP 10/14/04
		// Store values of all the cells in a row when the user modifies a cell through UI so that
		// when we get ItemChanged notification on that row we do not notify the calc engine that
		// values of all the cells in the row changed even though only one cell's value might
		// have changed. The only way we can tell which cells' values changed is by storing the cell
		// values and comparing them.
		//
		private UltraGridRow storedCellValuesRow = null;
		private Hashtable storedCellValues = null;

		// SSP 12/22/04 - IDataErrorInfo Support
		//
		private Image dataErrorImageFromResource = null;

		// SSP 3/30/05 - NAS 5.2 Fixed Rows/Filter Row
		//
		private int fixedRowsCollectionsVersion = 0;
		private EmbeddableEditorBase filterOperatorEditor_AboveOperand = null;
		private EmbeddableEditorBase filterOperatorEditor_WithOperand = null;
		private ValueList filterOperatorValueListAll = null;
		private EditorWithCombo filterRowComboEditor = null;
		private EditorWithText filterRowTextEditor = null;
		private Image filterClearButtonImageFromResource = null;
		private Image filterRowSelectorImageFromResource = null;
		private object[] filterRowCache = null;
		private const int FILTER_ROW_CACHE_STEP = 6;

		// SSP 5/3/05 - NewColumnLoadStyle and NewBandLoadStyle
		// Added NewColumnLoadStyle and NewBandLoadStyle properties. 
		// Also added inApplyLoadedBands flag.
		//
		private NewColumnLoadStyle newColumnLoadStyle = NewColumnLoadStyle.Show;
		private NewBandLoadStyle newBandLoadStyle = NewBandLoadStyle.Show;

        // MRS 9/19/07 - BR26542
        // Renamed 'inApplyLoadedBands' to 'isApplyingLayout' everywhere it occures
		//private bool inApplyLoadedBands = false;
        private bool isApplyingLayout = false;

		// SSP 6/30/05 - NAS 5.3 Column Chooser
		// 
		private DefaultableBoolean columnChooserEnabled = DefaultableBoolean.Default;

		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// 
		private Infragistics.Win.UltraWinGrid.EmptyRowSettings emptyRowSettings = null;
		private Image columnChooserButtonImageFromResource = null;

		// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
		//
		private EmbeddableImageRenderer groupByRowImageRenderer = null;

		// SSP 8/28/06 - NAS 6.3
		// Added CellHottrackInvalidationStyle property on the layout.
		// 
		private CellHottrackInvalidationStyle cellHottrackInvalidationStyle = CellHottrackInvalidationStyle.Default;

        // MBS 4/24/09 - TFS12665
        private int cacheListenerCount;
        private Dictionary<UltraGridBand, CachedBandInfo> cachedBandData;
        private Dictionary<ChildBandsCollection, CachedChildBandsInfo> cachedChildBandsData;
        private Dictionary<UltraGridRow, CachedRowInfo> cachedRowData;

		// The LayoutAppearanceIndex enum specifies the index value for
		// each of the appearance objects in the appearanceHolders array
		//
		internal enum LayoutAppearanceIndex
		{
			Default = 0,
			Caption = 1,
			// SSP 1/5/04 UWG1606
			// Added appearances for the splitter bars.
			//
			SplitterBarHorizontalAppearance = 2,
			SplitterBarVerticalAppearance = 3
		};

		private const int LastLayoutAppearanceIndex = 3;

        // JAS 10/11/04 
        private bool isScrolling = false;

        // JAS 10/11/04 - This informs the ComboDropDownControl, if there is one, that
        // we are scrolling, which it uses to ignore any calls to Refresh().
        internal bool IsScrolling
        {
            get
            {
                return this.isScrolling;
            }
            set
            {
                this.isScrolling = value;
            }
        }

		// JAS v5.2 Wrapped Header Text 4/18/05
		//
		private int columnHeaderHeightVersionNumber = 0;

		// JAS 4/5/05 v5.2 CaptionVisible property
		//
		private DefaultableBoolean captionVisible = DefaultableBoolean.Default;

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		private string clipboardCellDelimiter = null;
		private string clipboardRowSeparator = null;
		private string clipboardCellSeparator = null;

		// SSP 5/26/06 BR13063
		// Due to printing where we have multiple layouts using the same component role, we need to
		// cache the properties at each layout.
		// 
		internal AppStyling.StylePropertyCache propertyCache = new AppStyling.StylePropertyCache( 1 + (int)StyleUtils.CachedProperty.LastValue );

        // MRS 3/25/2008 - BR31396
        // Keep track of whether any columns in the grid are using ColSpan.
        internal bool hasColSpan = false;
        internal bool needToVerifyColSpan = true;

        // the follwowing array lists the propertyids of the apperance
		// objects in the appearanceHolder array. These ids MUST be ordered
		// the same as the LayoutAppearanceIndexIndexs listed above
		//
		private static readonly PropertyIds [] AppearancePropIds = new PropertyIds[]
		{
			PropertyIds.Appearance,
			PropertyIds.CaptionAppearance,
			// SSP 1/5/04 UWG1606
			// Added appearances for the splitter bars.
			//
			PropertyIds.SplitterBarHorizontalAppearance,
			PropertyIds.SplitterBarVerticalAppearance
		};

        // MRS NAS v8.2 - CardView Printing
        //
        private Infragistics.Win.UltraWinGrid.AllowCardPrinting allowCardPrinting = AllowCardPrinting.Never;

        // MRS NAS 9.1 - Sibling Band Order
        #region NAS 9.1 - Sibling Band Order

        // I added a new SortedBands collection property on the Layout. This collection
        // mirrors the "real" Bands collection, but sorts the bands in order to support
        // the new VisiblePosition property on the Band. 
        //
        private Infragistics.Win.UltraWinGrid.BandsCollection sortedBands = null;

        // Keep a version number which will be bumped any time bands are added
        // or removed. This will be used to let us know that we have to rebuild the 
        // SortedBands collection. 
        //
        internal int sortedBandsVersion = int.MinValue;

        #endregion //NAS 9.1 - Sibling Band Order

        //  BF 2/10/09  TFS13739
        private bool isInSetCellValue = false;

        // MBS 4/6/09 - NA9.2 SelectionOverlay
        private Color selectionOverlayColor = Color.Empty;
        private Color selectionOverlayBorderColor = Color.Empty;
        private int selectionOverlayBorderThickness = -1;
        private const int SELECTION_BORDER_THICKNESS = 3;
        private const byte DEFAULT_OVERLAY_ALPHA = 21;

        // CDS 9.2 RowSelector State-Specific Images
        private RowSelectorImageInfo rowSelectorImages = null;

        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties (DefaultSelectedBackColor & DefaultSelectedForeColor)
        private Color defaultSelectedBackColor = SystemColors.Highlight;
        private Color defaultSelectedForeColor = SystemColors.HighlightText;

        #endregion // Private Members


        // MBS 3/4/08 - RowEditTemplate NA2008 V2
        private UltraGridCellProxy activeProxy;
        //
        internal UltraGridCellProxy ActiveProxy
        {
            get { return this.activeProxy; }
            set
            {
                if (this.activeProxy == value)
                    return;

                this.activeProxy = value;
            }
        }

		#region Cell appearance caching

		// JJD 10/31/01
		// Added caching logic to optimize cell painting
		//
		private int  drawSequence	= 1;
		private bool drawing		= false;

		// SSP 5/11/05 BR02633
		//
		internal Rectangle lastPaintRect = Rectangle.Empty;

		/// <summary>
		/// Returns true if the layout is currently being rendered.
		/// </summary>
		[Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public bool IsDrawing { get { return this.drawing; } }

		internal int DrawSequence { get { return this.drawSequence; } }

		internal void InitializeDrawingFlag ( bool newState )
		{
			this.drawSequence++;
			this.drawing = newState;

			// AS 3/17/08 BR29569
			// We are caching the resolved appearance on an EditorWithText within the Win assembly
			// but we are doing so in the context of the DrawElement method of the element (control
			// element in this case). The cell is caching its appearance and not dirtying that cache
			// until its OnBeforeDraw but the DrawElement also encompasses a call to the 
			// verifychildelements which means that if the layout has cached the appearance for the
			// cell after the last draw, it will end up using that appearance if someone asks to 
			// resolve the appearance while positioning the child elements. That is what happened here.
			// The EditorWithText asked the owner to resolve the appearance in the position child elements,
			// it returned the cached appearance which had Window as the backcolor instead of Red which 
			// was set on the cell before the paint call. The EditorWithText held onto this appearance
			// which had the wrong color (and could have had the wrong font info, etc.) and used that.
			// We can get around this by clearing the cache before letting the controlelement process
			// the paint operation. Before/after the paint, we set the DrawingFlag so we'll centralize
			// the clearing of the cache here.
			// 
			this.DirtyCachedCellAppearanceData();

			// JJD 11/06/01
			// Clear the draw cache when we stop drawing
			//
			if ( newState == false )
				this.SortedBands.ClearDrawCache();
		}

		#endregion

		#region AutoSizeCursor

		internal System.Windows.Forms.Cursor AutoSizeCursor
		{
			get
			{
				if (this.autoSizeCursor == null)
				{
					// Load the cursor resource.
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream	stream	= this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "AutoSize.cur");
					System.IO.Stream	stream	= 
						typeof( UltraGridLayout ).Module.Assembly.GetManifestResourceStream(typeof( UltraGridLayout ), "AutoSize.cur");
					
					this.autoSizeCursor = new Cursor(stream);

					stream.Close();
				}

				return this.autoSizeCursor;
			}
		}

		#endregion AutoSizeCursor
		
		#region AutoSizeCursor

		// SSP 11/26/03 UWG2344
		// Added PerformAutoSize to the UltraGridRow and perform auto size functionality for rows.
		// Added RowAutoSizeCursor property.
		//
		internal System.Windows.Forms.Cursor RowAutoSizeCursor
		{
			get
			{
				if ( null == this.rowAutoSizeCursor )
				{
					// Load the cursor resource.
					System.IO.Stream stream = typeof( UltraGridLayout ).Module.Assembly.GetManifestResourceStream( typeof( UltraGridLayout ), "RowAutoSize.cur" );
					
					this.rowAutoSizeCursor = new Cursor(stream);

					stream.Close();
				}

				return this.rowAutoSizeCursor;
			}
		}

		#endregion AutoSizeCursor

		#region LineDownCursor

		internal System.Windows.Forms.Cursor LineDownCursor
		{
			get
			{
				if (this.lineDownCursor == null)
				{
					// Load the cursor resource.
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream	stream	= this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "LineDown.cur");
					System.IO.Stream	stream	= 
						typeof( UltraGridLayout ).Module.Assembly.GetManifestResourceStream(typeof( UltraGridLayout ), "LineDown.cur");
					
					this.lineDownCursor = new Cursor(stream);

					stream.Close();
				}

				return this.lineDownCursor;
			}
		}

		#endregion LineDownCursor

		#region LineUpCursor

		internal System.Windows.Forms.Cursor LineUpCursor
		{
			get
			{
				if (this.lineUpCursor == null)
				{
					// Load the cursor resource.
					// SSP 6/14/02 UWG1238
					// this.GetType returns the derived class' type and Module.Assembly will be
					// the assembly that the derived class if from and the resources are in
					// our assembly. So below statemenet won't work if we use GetType( ).
					// Instead use the typeof operator with our class name as the operand.
					//
					//System.IO.Stream	stream	= this.GetType().Module.Assembly.GetManifestResourceStream(this.GetType(), "LineUp.cur");
					System.IO.Stream	stream	= 
						typeof( UltraGridLayout ).Module.Assembly.GetManifestResourceStream(typeof( UltraGridLayout ), "LineUp.cur");
					
					this.lineUpCursor = new Cursor(stream);

					stream.Close();
				}

				return this.lineUpCursor;
			}
		}

		#endregion LineUpCursor

		#region IImageListProvider interface

		/// <summary>
		/// Returns the <see cref="UltraGridBase.ImageList"/> property.
		/// </summary>
		System.Windows.Forms.ImageList IImageListProvider.ImageList
		{
			get
			{
				if ( this.grid == null )
					return null;

				return this.grid.ImageList;
			}
		}

		#endregion // IImageListProvider

		/// <summary>
		/// Contructor
		/// </summary>
		public UltraGridLayout()
		{
			//RobA UWG577 10/18/01 had to make this contructor public 
			//with the lastest build of .Net

			//ROBA UWG223 8/22/01
			this.synchronizedColListsDirty = true;
			this.Reset();			
		}

		/// <summary>
		/// Contructor
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		public UltraGridLayout( String key ) : base( key )
		{
			//ROBA UWG223 8/22/01
			this.synchronizedColListsDirty = true;
			this.Reset();
		}


		// SSP 9/28/01
		// Added this helper function that array of IDisposables and
		// disposes of them if not null
		/// <summary>
		/// Helper function that array of IDisposables and
		/// disposes of them if not null
		/// </summary>
		/// <param name="objectsToDispose"></param>
		internal static void DisposeObjects( IDisposable[] objectsToDispose )
		{
			if ( null == objectsToDispose )
				return;

			if ( 0 == objectsToDispose.Length )
				return;

			for ( int i = 0; i < objectsToDispose.Length; i++ )
			{
				if ( null != objectsToDispose[i] )
					objectsToDispose[i].Dispose( );
			}
		}

		
		internal bool ScrollingColumns
		{
			get
			{
				return this.scrollingColumns;
			}
			set
			{
				if ( this.scrollingColumns != value )
					this.scrollingColumns = value;
			}
		}

		/// <summary>
		/// Determines whether sub-items are currently being initialized. This property is read-only and is only available at run-time.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
		public bool InitializingAllSubItems
		{
			get
			{
				return this.initializingAllSubItems;
			}
		}


		internal bool IgnoreDataSourePositionChange
		{
			get
			{
				return this.ignoreDataSourePositionChange;
			}

			set
			{
				if ( this.ignoreDataSourePositionChange != value )
					this.ignoreDataSourePositionChange = value;
			}

		}

		internal bool HasBackgroundImage
		{
			get
			{
				// Check if we have a background image
				//
				if ( this.HasAppearance )
				{
					System.Drawing.Image imageBackground = this.Appearance.Data.ImageBackground;

					if ( null != imageBackground &&
						imageBackground.Height > 0 &&
						imageBackground.Width > 0 )
					{
						return true;
					}
				}

				return false;
			}
		}

		// SSP 7/21/03 UWG1997
		// Added UseScrollWindow property so that the user can prevent the grid from using 
		// ScrollWindow method for scrolling.
		// Added vertical parameter to specify the scroll direction.
		//
		//internal bool IsBackgroundScrollable
		internal bool IsBackgroundScrollable( bool vertical )
		{
			// SSP 7/21/03 UWG1997
			// Added UseScrollWindow property so that the user can prevent the grid from using 
			// ScrollWindow method for scrolling.
			// Added vertical parameter to specify the scroll direction.
			//
			// --------------------------------------------------------------------------------
			if ( vertical )
			{
				if ( Infragistics.Win.UltraWinGrid.UseScrollWindow.Both != this.UseScrollWindow 
					&& Infragistics.Win.UltraWinGrid.UseScrollWindow.VerticalOnly != this.UseScrollWindow )
					return false;
			}
			else
			{
				if ( Infragistics.Win.UltraWinGrid.UseScrollWindow.Both != this.UseScrollWindow 
					&& Infragistics.Win.UltraWinGrid.UseScrollWindow.HorizontalOnly != this.UseScrollWindow )
					return false;
			}
			// --------------------------------------------------------------------------------

			// SSP 11/23/04 - Merged Cell Feature
			// Keep a flag on the data area ui element that specifies whether to use scroll window
			// when scrolling the background in merged cells mode. If the MergedCellContentArea is
			// VisibleRect where we are repositioning the contents of merged cells whenever it 
			// scrolls then we can't use scroll window to scroll. This flag is reset to false in
			// The PositionChildElements of the data area element and set to true by the merged cell
			// elements in their PositionChildElements logic.
			//
            if ( null != this.DataAreaElement && this.DataAreaElement.dontUseUseScrollWindow )
				return false;

			// SSP 2/21/06 BR09319
			// Commented out the original code and added new code. The original code is moved into the
			// new GridUtils.IsBackgroundScrollable method. The reason for this change is that now
			// we need to check the EmptyAreaAppearance as well if empty rows are visible.
			// 
			// ------------------------------------------------------------------------------------------
			if ( this.HasAppearance && ! GridUtils.IsBackgroundScrollable( this.Appearance ) )
				return false;

			if ( this.HasEmptyRowSettings && this.EmptyRowSettings.ShowEmptyRows
				&& this.EmptyRowSettings.HasEmptyAreaAppearance 
				&& ! GridUtils.IsBackgroundScrollable( this.EmptyRowSettings.EmptyAreaAppearance ) )
				return false;

			return true;

			
			// ------------------------------------------------------------------------------------------
		}

		// SSP 7/21/03 UWG1997
		// Added UseScrollWindow property so that the user can prevent the grid from using 
		// ScrollWindow method for scrolling.
		//
		/// <summary>
		/// Specifies whether to use scroll window when scrolling the grid.
		/// </summary>
		/// <remarks>
		/// <seealso cref="ScrollStyle"/> <seealso cref="UseScrollWindow"/> <seealso cref="Scrollbars"/> <seealso cref="ScrollBarLook"/>
		/// </remarks>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Advanced ),
		  DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Infragistics.Win.UltraWinGrid.UseScrollWindow UseScrollWindow
		{
			get
			{
				return this.useScrollWindow;
			}
			set
			{
				if ( value != this.useScrollWindow )
				{
					if ( ! Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.UseScrollWindow ), value ) )
						throw new InvalidEnumArgumentException( "UseScrollWindow", (int)value, typeof( Infragistics.Win.UltraWinGrid.UseScrollWindow ) );

					this.useScrollWindow = value;

                    // MRS NAS v8.2 - Unit Testing
                    this.NotifyPropChange(PropertyIds.UseScrollWindow);
				}
			}
		}

		internal bool ShouldSerializeUseScrollWindow( )
		{
			return UseScrollWindow.Both != this.useScrollWindow;
		}

		internal void ResetUseScrollWindow( )
		{
			this.UseScrollWindow = UseScrollWindow.Both;
		}

		internal int CachedCaptionHeight
		{
			get
			{
				// If the height is cached, don't recalc it.
				//
				if ( this.cachedCaptionHeight == 0 )
				{
					// First get the appearance data object.
					//
					ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.UltraGridLayout ), 
						AppearancePropFlags.Image | 
						AppearancePropFlags.ImageAlpha | 
						AppearancePropFlags.ImageHAlign | 
						AppearancePropFlags.ImageVAlign | 
						AppearancePropFlags.FontData );
					context.IsCaption = true;
					Size imageSize = new Size(0,0);
					string caption = this.grid.Text;
										     
					// Call ResolveAppearance to get the caption's appearance data
					//
					AppearanceData appData = new AppearanceData();
					this.ResolveAppearance( ref appData, ref context );

					// If the Appearance object has a picture, let's get the
					// height of it.
					//
					if ( appData.Image != null )
					{
						// SSP 10/25/01
						// Implemented
						//
						// Use GetImage function of AppearanceData to get the appropriate image
						// from the appData.
						//
						Image image = appData.GetImage( this.Grid.ImageList );
						if ( null != image )
						{
							imageSize  = image.Size;
							imageSize.Height++;
						}
					}

					// for VAlign of top or bottom, add the heights of the
					// text and picture.
					//
					if ( (appData.ImageVAlign == VAlign.Top ||
						appData.ImageVAlign == VAlign.Bottom ) &&
						appData.ImageHAlign == HAlign.Center  )
					{
					
						// Add the height of the picture to captionarea.
						//
						this.cachedCaptionHeight = imageSize.Height;

						// If the caption has text, add the height of the font to the captionarea, 
						//						
						if ( caption != null )
						{							
							// JJD 12/14/01
							// Moved font height logic into CalculateFontHeight
							//
							// get the extext of the caption
							//
							this.cachedCaptionHeight = this.CalculateFontSize( ref appData, caption ).Height;

							if ( this.cachedCaptionHeight != 0 )
								//*vl 060701 - add 4 pixels to height because bottom edge of some chars
								//   was being clipped 
								this.cachedCaptionHeight += 4;	
			
						}

					}
       
						// for VAlign center, take the max value of the picture height
						// and the caption text height.
						//
					else
					{
						// If the caption has text, add the height of the font to the captionarea, 
						//
						if ( caption != null )
						{							
							// JJD 12/14/01
							// Moved font height logic into CalculateFontHeight
							//
							// get the extext of the caption
							//
							this.cachedCaptionHeight = this.CalculateFontSize( ref appData, caption ).Height;
			
							if ( this.cachedCaptionHeight != 0 )
								//*vl 060701 - add 4 pixels to height because bottom edge of some chars
								//   was being clipped 
								this.cachedCaptionHeight += 4;	
						}		            
						
						// If the picture is taller, use its height instead.
						//
						if ( imageSize.Height > this.cachedCaptionHeight )
							this.cachedCaptionHeight = imageSize.Height;

					}
				}

				// If we have a height then add enough height to handle the borders
				// as well
				if ( this.cachedCaptionHeight != 0 )
					return this.cachedCaptionHeight + (2 * this.GetBorderThickness( this.BorderStyleCaption ));					
				else
					return 0;
			}
		}


		/// <summary>
		/// Returns the active row. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridBase.ActiveRow"/>
		/// </remarks>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false) ]
		public Infragistics.Win.UltraWinGrid.UltraGridRow ActiveRow
		{
			get
			{
				return null != this.grid ? this.grid.ActiveRow : null; 
			}
		}

		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false) ]
		internal Infragistics.Win.UltraWinGrid.UltraGridCell ActiveCell
		{
			get
			{
				UltraGrid grid = this.grid as UltraGrid;

				return null != grid ? grid.ActiveCell : null; 
			}			
		}

		#region ActiveRowInternal

		// SSP 4/1/05 - Optimizations
		// Added ActiveRowInternal.
		//		
		internal UltraGridRow ActiveRowInternal
		{
			get
			{
				return null != this.grid ? this.grid.ActiveRowInternal : null;
			}
		}

		#endregion // ActiveRowInternal

		#region ActiveCellInternal
		
		// SSP 4/22/05 - Optimizations
		// Added ActiveCellInternal.
		//
		internal UltraGridCell ActiveCellInternal
		{
			get
			{
				UltraGrid grid = this.grid as UltraGrid;

				return null != grid ? grid.ActiveCellInternal : null; 
			}
		}

		#endregion // ActiveCellInternal

		internal int AddNewBoxMaxHeight
		{
			get
			{
				if ( this.AddNewBox.Hidden )
					return 0;

				int promptBtn = this.AddNewBox.PromptSize.Height;
				int lastBtnBottom = 0;				
								
				//gets the height of the
				//button, taking Picture and PictureAlignment into
				//account.
				//
				// SSP 3/27/06 - App Styling
				// Changed the ButtonStyleResolved property into a method because we need to pass in the context
				// of a band.
				// 
				//int btnHeight = this.addNewBox.ButtonHeight + 3 + ( 2 * this.GetButtonThickness( this.addNewBox.ButtonStyleResolved ) );
				int btnHeight = this.addNewBox.ButtonHeight + 3 + ( 2 * this.GetButtonThickness( this.addNewBox.GetButtonStyleResolved( null ) ) );

				if ( btnHeight > promptBtn )
					promptBtn = btnHeight;

				if ( this.SortedBands != null )
				{
					UltraGridBand lastBand = null;
					int bandLevel;

					if ( this.addNewBox.Style == AddNewBoxStyle.Full )
						lastBtnBottom = Math.Max( AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET, promptBtn - btnHeight ) + AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET;
					
					else
					{
						lastBtnBottom = Math.Max( AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET_COMPACT, promptBtn - btnHeight) + AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET_COMPACT;

						return Math.Max( promptBtn, lastBtnBottom ) + 
							( 2 * this.GetBorderThickness( this.addNewBox.BorderStyle ) ) +
							//	NOTE: the following addition is arbitrary tweakage,
							//	just to give a little breathing room at the bottom
							( AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET_COMPACT * 2 );
					}

					for ( int i = 0; i < this.SortedBands.Count; ++i )
					{
						if ( this.sortedBands[i].Hidden )
							continue;

						bandLevel = this.sortedBands[i].HierarchicalLevel;

						if ( lastBand == null )
							lastBtnBottom += btnHeight;

						else
						{
							// if the level is decreasing allow enough vertical room
							// so that we don't overlap the buttons
							//
							if ( bandLevel <= lastBand.HierarchicalLevel )
							{
								lastBtnBottom += btnHeight + AddNewBox.ADDNEWBTN_MIN_SPACING_VERT;
							}
							else
							{
								lastBtnBottom += AddNewBox.ADDNEWBTN_BAND_HEIGHT_OFFSET;
							}
						}

						lastBand = this.sortedBands[i];
					}

				}

				return Math.Max( promptBtn, lastBtnBottom ) + ( 2 * this.GetBorderThickness( this.addNewBox.BorderStyle ) );                
			}
		}


		internal static Color DarkenColor( Color color, int byAmount )
		{
			return Color.FromArgb( color.A,
				Math.Min( 255, Math.Max( 0, color.R - byAmount ) ),
				Math.Min( 255, Math.Max( 0, color.G - byAmount ) ), 
				Math.Min( 255, Math.Max( 0, color.B - byAmount ) ) );
		}

		// SSP 11/18/03 Add Row Feature
		// Added GetColorShade method.
		//
		internal static Color GetColorShade( Color color, int byAmount )
		{
			if ( byAmount + color.R > 255 
				|| byAmount + color.B > 255 
				|| byAmount + color.G > 255 )
				return DarkenColor( color, byAmount );
			else
				return LightenColor( color, byAmount );
		}

		internal static Color LightenColor( Color color, int byAmount )
		{
			return UltraGridLayout.DarkenColor( color, -byAmount );
		}

		internal void ResolveAppearance( ref AppearanceData appData,
			ref ResolveAppearanceContext context )
		{
			if ( context.IsRowPreview )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.RowPreview 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );

				return;
			}
			
			// First merge in the edit cell appearance if the cell is in edit
			//
			if (context.IsInEdit)
			{
				this.MergeOverrideAppearances ( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.EditCell 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.EditMode );

				return;			
			}

			// if this is the active cell pass just merge the active cell and row appearances and exit
			//
			if (context.IsActiveCell)
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveCell 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Active );
				
				// Only merge the active roww appearance if that flag is set.
				// This can happen in a cell appearance only pass
				//
				if (context.IsActiveRow)
				{
					// SSP 10/19/07 BR25706 BR25707
					// Added ActiveRowCellAppearance property.
					// 
					this.MergeOverrideAppearances( ref appData, ref context,
						UltraGridOverride.OverrideAppearanceIndex.ActiveRowCellAppearance );

					// SSP 3/13/06 - App Styling
					// We need to resolve on the row role.
					// 
					//this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveRow );
					AppStyling.UIRole rowRole = StyleUtils.GetRole( this, StyleUtils.Role.Row );
					this.MergeOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.ActiveRow 
						, rowRole, AppStyling.RoleState.Active );
				}

				return;
			}

			// if this is the active row pass just merge the active row appearance and exit
			//
			if (context.IsActiveRow)
			{
				// JJD 1/10/02
				// Use the active card caption appearance if type is CardCaptionUIElement
				//
				if (typeof(Infragistics.Win.UltraWinGrid.CardCaptionUIElement) == context.ObjectType)
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveCardCaption 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Active );
				}
				else
				{
					// SSP 10/19/07 BR25706 BR25707
					// Added ActiveRowCellAppearance property.
					// 
					if ( typeof( UltraGridCell ) == context.ObjectType )
						this.MergeOverrideAppearances( ref appData, ref context,
							UltraGridOverride.OverrideAppearanceIndex.ActiveRowCellAppearance );

					// SSP 3/13/06 - App Styling
					// We need to resolve on the row role.
					// 
					//this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.ActiveRow );
					AppStyling.UIRole rowRole = StyleUtils.GetRole( this, StyleUtils.Role.Row );
					this.MergeOverrideAppearances( ref appData, ref context, UltraGridOverride.OverrideAppearanceIndex.ActiveRow, 
						rowRole, AppStyling.RoleState.Active );
				}
				return;
			}

			// if this is the selected pass just merge the selected appearance and exit
			//
			if (context.IsSelected)
			{
				// JJD 1/10/02
				// Use the selected card caption appearance if type is CardCaptionUIElement
				//
				if (typeof(Infragistics.Win.UltraWinGrid.CardCaptionUIElement) == context.ObjectType)
				{
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedCardCaption 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Selected );
				}
				else
				{
					if (typeof(Infragistics.Win.UltraWinGrid.UltraGridCell)==context.ObjectType)
					{
						this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedCell 
							// SSP 3/13/06 - App Styling
							// 
							, AppStyling.RoleState.Selected );
					}

					// SSP 3/13/06 - App Styling
					// 
					//this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedRow );
					
					
					
					AppStyling.UIRole rowRole = typeof( UltraGridGroupByRow ) == context.ObjectType 
						? context.Role : StyleUtils.GetRole( this, StyleUtils.Role.Row );
					this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SelectedRow, 
						rowRole, AppStyling.RoleState.Selected );

				}

				// if we still don't have a selected backcolor or forecolor then 
				// resolve them using the system defaults as preferences
				//
				this.ResolveColors( ref appData,
                    ref context,
                    // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (DefaultSelectedBackColor & DefaultSelectedForeColor)
                    //SystemColors.Highlight,           // preferenceBack
                    this.DefaultSelectedBackColorResolved,// preferenceBack
                    SystemColors.WindowFrame,		  // preferenceBorder
                    //
                    //SystemColors.HighlightText,		  // preferenceFore
                    this.DefaultSelectedForeColorResolved,// preferenceFore
					true);							  // selectedPass

				return;				
			}

			// Added support for RowSelectorAppearance property off the override object
			//
			if (context.IsRowSelector ) 
			{
				this.MergeOverrideAppearances(ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.RowSelector 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );

				// JJD 4/17/01
				// On the first pass resolving a row selector's appearance 
				// we need to exit so we don't pick up any layout defaults before
				// we can do the 2nd pass to use header settings
				//
				return;
			}

			// SSP 7/25/03 - Fixed headers.
			// Added the if blocks for IsFixedHeader and IsFixedColumn.
			//
			// ----------------------------------------------------------------------------------------------
			if ( context.IsFixedHeader )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.FixedHeaderAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.FixedColumn );

				return;
			}

			if ( context.IsFixedColumn )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.FixedCellAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.FixedColumn );

				return;
			}
			// ----------------------------------------------------------------------------------------------

			// SSP 7/24/04 - UltraCalc
			// Resolve CellErrorAppearance.
			//
			// ----------------------------------------------------------------------
			if ( context.IsFormulaError )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.FormulaErrorAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.FormulaError );

				// SSP 3/16/06
				// We should return here since the default appearances will be resolved in a later phase.
				// 
				return;
			}
			// ----------------------------------------------------------------------

			// SSP 7/25/03
			// Added blocks for IsGroupByColumnHeader and IsGroupByColumn and took out the code that
			// resolved group-by column/header from code further down.
			//
			// ----------------------------------------------------------------------------------------------
			if ( context.IsGroupByColumnHeader )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.GroupByColumnHeader 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.GroupByColumn );

				return;
			}

			if ( context.IsGroupByColumn )
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.GroupByColumn 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.GroupByColumn );

				return;
			}
			// ----------------------------------------------------------------------------------------------


			ResolveAppearanceContext layoutContext = context;

			AppearancePropFlags		 bypassedProps = 0;

			bool stripPicture = true;


			if (typeof(Infragistics.Win.UltraWinGrid.CardCaptionUIElement) == context.ObjectType)
			{
				this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.CardCaption 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );

				// remove the backcolor / forecolor bits from the request mask
				//
				layoutContext.UnresolvedProps &= ~(AppearancePropFlags.BackColor|AppearancePropFlags.ForeColor);
				layoutContext.UnresolvedProps &= ~AppearancePropFlags.ImageBackground;
				bypassedProps |= AppearancePropFlags.ImageBackground | AppearancePropFlags.BackColor | AppearancePropFlags.ForeColor;

			}

			else if (typeof(Infragistics.Win.UltraWinGrid.CardAreaUIElement) == context.ObjectType)
			{
				this.MergeOverrideAppearances( ref appData, ref context, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.CardArea 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );

				// remove the backcolor / forecolor bits from the request mask
				//
				
				layoutContext.UnresolvedProps &= ~GridUtils.CommonBypassProps;
				bypassedProps |= GridUtils.CommonBypassProps;
			}

			else if (context.ObjectType==typeof(Infragistics.Win.UltraWinGrid.HeaderBase) )
			{
				// SSP 7/25/03
				// We are resolving group-by related appearances above.
				//
				
				
				this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Header 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );

				// remove the backcolor / forecolor bits from the request mask
				//
				
				layoutContext.UnresolvedProps &= ~GridUtils.CommonBypassProps;
				bypassedProps |= GridUtils.CommonBypassProps;
			}

			else if (context.ObjectType==typeof(Infragistics.Win.UltraWinGrid.UltraGridCell) ) 
			{
				// merge in cell appearance properties
				//
				this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Cell 
					// SSP 3/13/06 - App Styling
					//
					, AppStyling.RoleState.Normal );

				// AS 4/7/06 ImageBackground
				//layoutContext.UnresolvedProps &= ~AppearancePropFlags.ImageBackground;
				//bypassedProps |= AppearancePropFlags.ImageBackground;
				layoutContext.UnresolvedProps &= ~GridUtils.ImageBackgroundProps;
				bypassedProps |= GridUtils.ImageBackgroundProps;

				//merge in row appearance properties as well
				if ( !context.IsCellOnly )
				{
					// SSP 7/25/03
					// We are resolving group-by related appearances above.
					//
					

					// SSP 3/8/06 - App Styling
					// We need to resolve on the row role.
					// 
					AppStyling.UIRole rowRole = StyleUtils.GetRole( this, StyleUtils.Role.Row );

					if(context.IsAlternate)
					{
						this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.RowAlternate 
							// SSP 3/13/06 - App Styling
							// 
							, rowRole, AppStyling.RoleState.AlternateItem );
					}
					else
					{
						this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Row 
							// SSP 3/13/06 - App Styling
							// 
							, rowRole, AppStyling.RoleState.AlternateItem );
					}

					
					layoutContext.UnresolvedProps &= ~GridUtils.CommonBypassProps;
					bypassedProps |= GridUtils.CommonBypassProps;
				}
				else
					return;
				
			}
			else if (context.ObjectType == typeof (Infragistics.Win.UltraWinGrid.UltraGridRow))
			{
				// If the alternate flag is set only merge in the alternate 
				// row appearance
				//
				if(context.IsAlternate)
				{
					this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.RowAlternate 
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.AlternateItem );
				}
				else
				{
					this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.Row
						// SSP 3/13/06 - App Styling
						// 
						, AppStyling.RoleState.Normal );
				}

				// remove the backcolor / forecolor bits from the request mask
				//
				
				layoutContext.UnresolvedProps &= ~GridUtils.CommonBypassProps;
				bypassedProps |= GridUtils.CommonBypassProps;
			}

			else if (context.ObjectType == typeof (Infragistics.Win.UltraWinGrid.UltraGridLayout ) )
			{
				// if we are resolving for the caption then merge in the caption
				// appearance
				//
				if( context.IsCaption )
				{
					this.MergeLayoutAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridLayout.LayoutAppearanceIndex.Caption
						// SSP 3/13/06 - App Styling
						// 
						, context.Role, AppStyling.RoleState.Normal );
					
					// Remove the backcolor / forecolor bits from the request mask 
					//
					layoutContext.UnresolvedProps &= ~(AppearancePropFlags.BackColor | AppearancePropFlags.ForeColor );
					bypassedProps |= AppearancePropFlags.BackColor | AppearancePropFlags.ForeColor;
				}

				else
					stripPicture=false;					

			}
				// SSP 5/20/02
				// Summary rows feature. Added code for resolving summary value and summary
				// footer appearances.
				// Using SummaryFooterUIElement since there is no summary footer object and
				// for the sake of consistency, I am also using the SummaryValueUIElement instead
				// of SummaryValue object.
				//
				// ---------------------------------------------------------------------------
			else if ( context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.SummaryFooterUIElement ) )
			{
				this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SummaryFooterAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );
					
				// remove the background image from the requested mask.
				//
				
				layoutContext.UnresolvedProps &= ~GridUtils.ImageBackgroundProps;
				bypassedProps |= GridUtils.ImageBackgroundProps;
			}
			else if ( context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.SummaryFooterCaptionUIElement ) )
			{
				this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SummaryFooterCaptionAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );
					
				// remove the background image from the requested mask.
				//
				
				layoutContext.UnresolvedProps &= ~GridUtils.ImageBackgroundProps;
				bypassedProps |= GridUtils.ImageBackgroundProps;
			}
			else if ( context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.SummaryValueUIElement ) )
			{
				this.MergeOverrideAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridOverride.OverrideAppearanceIndex.SummaryValueAppearance 
					// SSP 3/13/06 - App Styling
					// 
					, AppStyling.RoleState.Normal );

				// By default resolve the text trimming for the summary values to 
				// EllipsisCharacter.
				//
				if ( 0 != ( layoutContext.UnresolvedProps & AppearancePropFlags.TextTrimming ) )
				{
					appData.TextTrimming = TextTrimming.EllipsisCharacter;
					layoutContext.UnresolvedProps &= ~AppearancePropFlags.TextTrimming;
				}
					
				// remove the backcolor / forecolor bits from the request mask
				//
				
				layoutContext.UnresolvedProps &= ~GridUtils.CommonBypassProps;
				bypassedProps |= GridUtils.CommonBypassProps;
			}
			// ---------------------------------------------------------------------------------------


			if( stripPicture )
			{
				// JJD 11/05/01
				// Remove the gradient property bits as well
				//
				bypassedProps |= AppearancePropFlags.ImageBackground | 
					AppearancePropFlags.ImageBackgroundDisabled | // AS 4/7/06 ImageBackgroundDisabled
					AppearancePropFlags.BackColor2 | 
					AppearancePropFlags.BackGradientStyle;

				layoutContext.UnresolvedProps &= ~bypassedProps;
			}

			// If the alternate flag don't merge the layout props since this is
			// not the last pass
			//
			if ( !context.IsAlternate )
			{
				// SSP 3/13/06 - App Styling
				// We are resolving the layout's default appearance. For that we need to get the Layout ui role.
				// 
				//this.MergeLayoutAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridLayout.LayoutAppearanceIndex.Default );
				// SSP 9/29/06 BR15371
				// 
				//AppStyling.UIRole layoutRole = StyleUtils.GetRole( this, StyleUtils.GetControlAreaRole( this ) );
				AppStyling.UIRole layoutRole = StyleUtils.GetRole( this, StyleUtils.GetControlAreaRole( this ), out layoutContext.ResolutionOrder );

				this.MergeLayoutAppearances( ref appData, ref layoutContext, Infragistics.Win.UltraWinGrid.UltraGridLayout.LayoutAppearanceIndex.Default, 
					layoutRole, AppStyling.RoleState.Normal );
			}

			// strip out any bit in bypassedProps that weren't in the passed in context
			//
			bypassedProps &= context.UnresolvedProps;

			// set UnresolvedProps in the passed in context to the bypassed props and
			// the those still unresolved in the stack layoutContext
			//
			context.UnresolvedProps = layoutContext.UnresolvedProps | bypassedProps;
			
			// if we have resolved all requested props (except for props we want
			// to bypass on this pass) then we can just return
			//
			if ( ( context.UnresolvedProps & ~bypassedProps ) == 0)
			{
				return;
			}

			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Added ResolveDefaultColors on the context.
			// 
			if ( ! context.ResolveDefaultColors )
				return;

			// Since we're at the top level, we ignore the context and simply
			// override all defaults

			// if we still don't have a selected backcolor or forecolor then 
			// resolve them using the system defaults as preferences
			//

			if ((context.ObjectType == typeof(Infragistics.Win.UltraWinGrid.UltraGridRow))||
				(context.ObjectType == typeof(Infragistics.Win.UltraWinGrid.UltraGridCell)))
			{
				// for rows and cells we don't want to force the resolution
				// of the fore and back colors because of the band level
				// caching that we are doing. Instead, ResolveColors will
				// be called at the end of the row/cell's InternalResolveAppearance
				// methods
				//
				// Since we had to make an additional pass for alternate rows
				// we must return here since this is not the final pass.
				//
				if ( context.IsAlternate )
				{
					return;
				}
			}
			else
			{
				Color preferenceBack;
				Color preferenceFore;
				Color preferenceBorder = SystemColors.WindowFrame;

				// When resolving the layout object use the btnface for the
				// background but the windowtext for the foreground. This allows
				// us to draw the grid's borders properly if they select the
				// sBorderStyleForeColor setting for the borders.
				//
				if ( typeof(Infragistics.Win.UltraWinGrid.CardAreaUIElement) == context.ObjectType ||
					context.ObjectType == typeof(Infragistics.Win.UltraWinGrid.UltraGridLayout) ||
					// For summary footers, resolve the layout's colors as well.
					//
					typeof( Infragistics.Win.UltraWinGrid.SummaryFooterUIElement ) == context.ObjectType )
				{
					// SSP 1/27/05 BR02053
					// Instead of checking IsPrinting flag, make use of IsDisplayLayout. What we are 
					// interested in finding out is this element is in the print layout or not. Simply
					// checking the IsPrinting flag won't tell us that the display layout could verify 
					// it's ui elements while printing (like when the grid gets a paint while printing,
					// especially with the new UltraGridPrintDocument).
					//
					//if ( this.grid.IsPrinting )
					if ( this.IsPrintLayout )
					{
						preferenceBack = Color.White;
						preferenceFore = Color.Black;
					}
					else
					{
						//ROBA 8/3/01 UWG34
						//preferenceBack = this.grid.BackColor;
						//preferenceFore = this.grid.ForeColor;
						
						//RobA 9/25/01 we are now hidding the 
						//the control's backcolor and forecolor
						preferenceBack = SystemColors.Control;				
						preferenceFore = SystemColors.ControlText;
					}
					
				}

				else if (context.ObjectType == typeof(Infragistics.Win.UltraWinGrid.UltraGridCell))
				{
					preferenceBack = SystemColors.Window;
					preferenceFore = SystemColors.WindowText;
				}
				
				else if ( typeof( Infragistics.Win.UltraWinGrid.SummaryValueUIElement ) == context.ObjectType )
				{
					preferenceBack = UltraGridLayout.DarkenColor( SystemColors.Window, 20 );
					preferenceFore = SystemColors.WindowText;
				}

				else if (typeof(Infragistics.Win.UltraWinGrid.CardCaptionUIElement) == context.ObjectType)
				{
					preferenceBack = SystemColors.InactiveCaption;
					preferenceFore = SystemColors.InactiveCaptionText;
				}

				else 
				{
					preferenceBack = SystemColors.Control;
					preferenceFore = SystemColors.ControlText;
				}

				this.ResolveColors( ref appData, ref context, preferenceBack, preferenceBorder, preferenceFore, false );
			}

			if ( ( (context.UnresolvedProps & AppearancePropFlags.AlphaLevel) != 0) && 
				!appData.HasPropertyBeenSet(AppearancePropFlags.AlphaLevel) && 
				appData.AlphaLevel == 0)
			{
				appData.AlphaLevel = 255;
				context.UnresolvedProps ^= AppearancePropFlags.AlphaLevel;
			}

			if ( ( (context.UnresolvedProps & AppearancePropFlags.ImageHAlign) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.ImageHAlign) )
			{
				appData.ImageHAlign = HAlign.Left;
				context.UnresolvedProps ^= AppearancePropFlags.ImageHAlign;
			}

			if ( ( (context.UnresolvedProps & AppearancePropFlags.ImageVAlign) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.ImageVAlign) )
			{
				appData.ImageVAlign = VAlign.Middle;
				context.UnresolvedProps ^= AppearancePropFlags.ImageVAlign;
			}

			if ( ( (context.UnresolvedProps & AppearancePropFlags.TextHAlign) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextHAlign) )
			{

				if (typeof(Infragistics.Win.UltraWinGrid.CardCaptionUIElement) == context.ObjectType)
				{
					appData.TextHAlign = HAlign.Center;
				}
				else if ( context.IsCard || context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.CardCaptionUIElement ) )
				{
					appData.TextHAlign = HAlign.Left;
				}
				else if ( context.IsCaption || context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.HeaderBase ) )
				{
					appData.TextHAlign = HAlign.Center;
				}
				else
				{
					appData.TextHAlign = HAlign.Left;
				}

				context.UnresolvedProps ^= AppearancePropFlags.TextHAlign;
			}

			if ( ( (context.UnresolvedProps & AppearancePropFlags.TextVAlign) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextVAlign) )
			{
				if ( context.IsCaption || context.ObjectType == typeof ( Infragistics.Win.UltraWinGrid.HeaderBase ) )
				{
					appData.TextVAlign = VAlign.Middle;
				}
				else
				{
					appData.TextVAlign = VAlign.Top;
				}
				
				context.UnresolvedProps ^= AppearancePropFlags.TextVAlign;
			}

			if ( ( (context.UnresolvedProps & AppearancePropFlags.ImageBackgroundStyle) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.ImageBackgroundStyle) )
			{
				appData.ImageBackgroundStyle = ImageBackgroundStyle.Centered;
				context.UnresolvedProps ^= AppearancePropFlags.ImageBackgroundStyle;
			}

			// JJD 11/05/01
			// Added support for texttrimming default property resolution
			//
			if ( ( (context.UnresolvedProps & AppearancePropFlags.TextTrimming) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextTrimming) )
			{
				// JJD 5/14/03 the default trimming should be character
				//appData.TextTrimming = TextTrimming.None;
				appData.TextTrimming = TextTrimming.Character;
				context.UnresolvedProps ^= AppearancePropFlags.TextTrimming;
			}

			// JJD 11/05/01
			// Added support for BackGradientStyle default property resolution
			//
			if ( ( (context.UnresolvedProps & AppearancePropFlags.BackGradientStyle) !=0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.BackGradientStyle) )
			{
				appData.BackGradientStyle = GradientStyle.None;
				context.UnresolvedProps ^= AppearancePropFlags.BackGradientStyle;
			}

			// SSP 1/10/02 UWG702
			// If the grid is a ultracombo, then resolve the cursor from the ultracombo's
			// appearance and use that if cursor is not set on layout's appearance.
			//
			if ( ( context.UnresolvedProps & AppearancePropFlags.Cursor ) !=0 ) 
			{
				UltraCombo ultraCombo = this.grid as UltraCombo;

				if ( null != ultraCombo && ultraCombo.HasAppearance && 
					ultraCombo.Appearance.Data.HasPropertyBeenSet( AppearancePropFlags.Cursor ) )
				{
					appData.Cursor  = ultraCombo.Appearance.Data.Cursor;
					context.UnresolvedProps ^= AppearancePropFlags.Cursor;
				}
			}
		}

		internal void ResolveColors( ref AppearanceData appData, 
			ref ResolveAppearanceContext context, 
			System.Drawing.Color preferenceBack, 
			System.Drawing.Color preferenceBorder, 
			System.Drawing.Color preferenceFore,
			bool selectedPass)
		{
			bool backColorNeedsSetting;
			bool borderColorNeedsSetting;
			bool foreColorNeedsSetting;

			// determine if either forecolor or backcolor need setting
			//

			backColorNeedsSetting = ( ( (context.UnresolvedProps & AppearancePropFlags.BackColor) != 0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.BackColor) 	);

			borderColorNeedsSetting = ( ( (context.UnresolvedProps & AppearancePropFlags.BorderColor) != 0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.BorderColor) 	);

			foreColorNeedsSetting = ( ( (context.UnresolvedProps & AppearancePropFlags.ForeColor) != 0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.ForeColor) 	);

			// Merge in the base appearance forecolor since it may have been 
			// excluded in the ResolveAppearance logic
			//
			if (foreColorNeedsSetting && !selectedPass)
			{
				ResolveAppearanceContext tempContext = context;
				tempContext.UnresolvedProps = AppearancePropFlags.ForeColor;

				// SSP 3/13/06 - App Styling
				// We are resolving the layout's default appearance. For that we need to get the Layout ui role.
				// 
				//this.MergeLayoutAppearances( ref appData, ref tempContext, Infragistics.Win.UltraWinGrid.UltraGridLayout.LayoutAppearanceIndex.Default );
				AppStyling.UIRole layoutRole = StyleUtils.GetRole( this, StyleUtils.GetControlAreaRole( this ) );
				this.MergeLayoutAppearances( ref appData, ref tempContext, UltraGridLayout.LayoutAppearanceIndex.Default,
					layoutRole, AppStyling.RoleState.Normal );

				foreColorNeedsSetting = ( !appData.HasPropertyBeenSet(AppearancePropFlags.ForeColor) );
                
			}

			if (borderColorNeedsSetting && !selectedPass)
			{
				ResolveAppearanceContext tempContext = context;
				tempContext.UnresolvedProps = AppearancePropFlags.BorderColor;

				// SSP 3/13/06 - App Styling
				// 
				//this.MergeLayoutAppearances( ref appData, ref tempContext, Infragistics.Win.UltraWinGrid.UltraGridLayout.LayoutAppearanceIndex.Default );
				AppStyling.UIRole layoutRole = StyleUtils.GetRole( this, StyleUtils.GetControlAreaRole( this ) );
				this.MergeLayoutAppearances( ref appData, ref tempContext, Infragistics.Win.UltraWinGrid.UltraGridLayout.LayoutAppearanceIndex.Default, 
					layoutRole, AppStyling.RoleState.Normal );

				borderColorNeedsSetting = ( !appData.HasPropertyBeenSet(AppearancePropFlags.BorderColor) );                
			}

			// if both fore and back color need setting then use the passed
			// in preferences to set both and return
			//
			if (backColorNeedsSetting)
            {
                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties (DefaultSelectedBackColor)
                //appData.BackColor = preferenceBack;
                //context.UnresolvedProps ^= AppearancePropFlags.BackColor;
                if (!preferenceBack.IsEmpty)
                {
                    appData.BackColor = preferenceBack;
                    context.UnresolvedProps ^= AppearancePropFlags.BackColor;
                }
			}

			if (foreColorNeedsSetting)
			{
                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties (DefaultSelectedForeColor)
                //appData.ForeColor = preferenceFore;
                //context.UnresolvedProps ^= AppearancePropFlags.ForeColor;
                if (!preferenceFore.IsEmpty)
                {
                    appData.ForeColor = preferenceFore;
                    context.UnresolvedProps ^= AppearancePropFlags.ForeColor;
                }
			}

			// on the selected pass we want to exit here since either forecolor
			// or back color has already been set
			//
			if (selectedPass || !borderColorNeedsSetting)
				return;

			// If the bordercolor needs setting use the passed in preference always
			//
			if (borderColorNeedsSetting)
			{
				appData.BorderColor = preferenceBorder;
				context.UnresolvedProps ^= AppearancePropFlags.BorderColor;
			}

			// If the backcolor needs setting use the passed in preference always
			//
			if (backColorNeedsSetting)
			{
				appData.BackColor = preferenceBack;
				context.UnresolvedProps ^= AppearancePropFlags.BackColor;
				return;
			}

			// If the forecolor needs setting use the passed in preference always
			//
			if (foreColorNeedsSetting)
			{
				appData.ForeColor = preferenceFore;
				context.UnresolvedProps ^= AppearancePropFlags.ForeColor;
			}


			return;			
		}

		#region MergeOverrideAppearances

		internal void MergeOverrideAppearances( ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index )
		{
			// SSP 3/29/05 - NAS 5.2 Optimizations
			// Commented out the original code and added new code.
			//
			if ( null != this.overrideObj )
			{
				AppearanceBase app = this.overrideObj.GetAppearanceIfAllocated( index );
				if ( null != app )
				{
					// JJD 12/12/02 - Optimization
					// Call the Appearance object's MergeData method instead so
					// we don't make unnecessary copies of the data structure
					//Infragistics.Win.AppearanceData.MergeAppearance( ref appData, overrideAppearance.Data, ref context.UnresolvedProps );
					app.MergeData( ref appData, ref context.UnresolvedProps );
				}
			}
			
		}

		// SSP 3/29/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Added an overload of MergeOverrideAppearances.
		//
		internal void MergeOverrideAppearances( 
			ref AppearanceData appData, 
			ref AppearancePropFlags flags,
			UltraGridOverride.OverrideAppearanceIndex index )
		{
			if ( null != this.overrideObj )
			{
				AppearanceBase app = this.overrideObj.GetAppearanceIfAllocated( index );
				if ( null != app )
					app.MergeData( ref appData, ref flags );
			}
		}

		// SSP 3/13/06 - App Styling
		// Added an overload that takes in roleState parameter.
		// 
		internal void MergeOverrideAppearances( ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index,
			AppStyling.RoleState roleState )
		{
			this.MergeOverrideAppearances( ref appData, ref context, index, context.Role, roleState );
		}

		// SSP 3/13/06 - App Styling
		// Added an overload that takes in role and roleState parameter.
		// 
		internal void MergeOverrideAppearances( ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridOverride.OverrideAppearanceIndex index,
			AppStyling.UIRole role, AppStyling.RoleState roleState )
		{
			if ( context.ResolutionOrder.UseStyleBefore )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref context );

			if ( context.ResolutionOrder.UseControlInfo )
				this.MergeOverrideAppearances( ref appData, ref context, index );

			if ( context.ResolutionOrder.UseStyleAfter )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref context );
		}

		#endregion // MergeOverrideAppearances

		#region HasOverrideAppearance

		// SSP 3/29/05 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		// Added HasOverrideAppearances method.
		//
		internal bool HasOverrideAppearance( UltraGridOverride.OverrideAppearanceIndex index )
		{
			return null != this.overrideObj && this.overrideObj.HasAppearance( index );
		}

		#endregion // HasOverrideAppearance

		#region MergeLayoutAppearances

		
		
		
		
		
		// SSP 3/13/06 - App Styling
		// Added an overload that takes in flags instead of context.
		// 
		internal void MergeLayoutAppearances( ref AppearanceData appData, 
			ref AppearancePropFlags flags,
			UltraGridLayout.LayoutAppearanceIndex index )
		{
			// SSP 4/1/05 - Optimizations
			// Added GetAppearanceIfAllocated.
			//
			Infragistics.Win.AppearanceBase layoutAppearance = this.GetAppearanceIfAllocated( index );
			if ( null != layoutAppearance )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//Infragistics.Win.AppearanceData.MergeAppearance( ref appData, layoutAppearance.Data, ref context.UnresolvedProps );
				layoutAppearance.MergeData( ref appData, ref flags );
			}
		}

		// SSP 3/13/06 - App Styling
		// Added an overload that resolves the app styling appearance setting as well.
		// 
		internal void MergeLayoutAppearances( ref AppearanceData appData, 
			ref AppearancePropFlags flags,
			UltraGridLayout.LayoutAppearanceIndex index,
			StyleUtils.Role eRole, AppStyling.RoleState roleState )
		{
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( this, eRole, out order );
			this.MergeLayoutAppearances( ref appData, ref flags, index, role, order, roleState );
		}

		// SSP 3/13/06 - App Styling
		// Added an overload that resolves the app styling appearance setting as well.
		// 
		internal void MergeLayoutAppearances( ref AppearanceData appData, 
			ref AppearancePropFlags flags,
			UltraGridLayout.LayoutAppearanceIndex index,
			AppStyling.UIRole role, AppStyling.ResolutionOrderInfo resOrderInfo, AppStyling.RoleState roleState )
		{
			if ( resOrderInfo.UseStyleBefore )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref flags );

			if ( resOrderInfo.UseControlInfo )
				this.MergeLayoutAppearances( ref appData, ref flags, index );

			if ( resOrderInfo.UseStyleAfter )
				StyleUtils.ResolveAppearance( role, roleState, ref appData, ref flags );
		}

		// SSP 3/13/06 - App Styling
		// Added an overload that resolves the app styling appearance setting as well.
		// 
		internal void MergeLayoutAppearances( ref AppearanceData appData, 
			ref ResolveAppearanceContext context,
			UltraGridLayout.LayoutAppearanceIndex index,
			AppStyling.UIRole role, AppStyling.RoleState roleState )
		{
			this.MergeLayoutAppearances( ref appData, ref context.UnresolvedProps, index, role, context.ResolutionOrder, roleState );
		}

		#endregion // MergeLayoutAppearances

		// SSP 12/4/02 UWG1840
		// Added an overload.
		//
		internal void OnDeserializationComplete( )
		{
			this.OnDeserializationComplete( true );
		}

		// SSP 12/4/02 UWG1840
		// Added calledFromEndInit parameter which will be passed to the BandsCollection.InitLayout .
		//
		//internal void OnDeserializationComplete()
		internal void OnDeserializationComplete( bool calledFromEndInit )
		{
			this.SynchronizedColListsDirty = true;

			this.InitAppearanceHolders();

			if ( this.valuelists != null )
				this.valuelists.InitAppearances( this.appearances );

			if ( this.colScrollRegions != null )
				this.colScrollRegions.InitLayout( this );

			if ( this.rowScrollRegions != null )
				this.rowScrollRegions.InitLayout( this );

			if ( this.loadedBands != null )
				// SSP 12/4/02 UWG1840
				// Added calledFromEndInit parameter which will be passed to the BandsCollection.InitLayout .
				//
				//this.loadedBands.InitLayout( this, true );
				this.loadedBands.InitLayout( this, calledFromEndInit );

			// SSP 1/9/03 UWG1914
			// Also initialize the deserialized bands collection with the layout.
			//
            // MRS 5/29/2009 - TFS17970
            //if ( null != this.sortedBands )
            //    this.sortedBands.InitLayout( this, calledFromEndInit );
            if (null != this.bands)
                this.bands.InitLayout(this, calledFromEndInit);

			// AS - 12/19/01
			if (this.scrollbarLook != null)
				this.scrollbarLook.InitializeAppearances( this.Appearances );

			// SSP 7/6/05 - NAS 5.3 Empty Rows
			// 
			if ( null != this.emptyRowSettings )
				this.emptyRowSettings.OnDeserializationComplete( this );
		}

        // SSP 1/12/05 BR0155 UWG3736
		// Added DiposeCachedEditors. Code in there is moved from OnDispose.
		//
		private void DisposeCachedEditors( )
		{
			if ( null != this.editorWithMask )
			{
				// SSP 1/12/05
				// Unhook from the editor.
				//
				this.editorWithMask.InvalidChar -= new EditorWithMask.InvalidCharEventHandler( this.OnMaskEditControlInvalidChar );
				this.editorWithMask.InvalidOperation -= new EditorWithMask.InvalidOperationEventHandler( this.OnMaskEditControlInvalidOperation );
				
				this.editorWithMask.Dispose( );
				this.editorWithMask = null;
			}

			if ( null != this.editorWithText )
			{
				this.editorWithText.Dispose( );
				this.editorWithText = null;
			}

			if ( null != this.colorPickerEditor )
			{
				this.colorPickerEditor.Dispose( );
				this.colorPickerEditor = null;									 
			}

			if ( null != this.checkEditor )
			{
				this.checkEditor.Dispose( );
				this.checkEditor = null;
			}

			if ( null != this.checkEditorTriState )
			{				
				this.checkEditorTriState.Dispose( );
				this.checkEditorTriState = null;
			}

			if ( null != this.dateTimeEditor )
			{
				this.dateTimeEditor.Dispose( );
				this.dateTimeEditor = null;
			}

			// SSP 8/11/03 - Embeddable image renderer
			// 
			if ( null != this.imageRenderer )
			{
				this.imageRenderer.Dispose( );
				this.imageRenderer = null;
			}

			// SSP 1/31/05
			// Force columns to release editor references.
			//
			if ( null != this.sortedBands )
			{
				foreach ( UltraGridBand band in this.sortedBands )
				{
					foreach ( UltraGridColumn column in band.Columns )
						column.DirtyDefaultEditor( );
				}
			}
		}

		/// <summary>
		/// Called when the object is disposed of
		/// </summary>
		protected override void OnDispose()
		{	
			// SSP 4/26/02
			// EM Embeddable editors.
			// Don't need month view control any more because the DateEditor takes
			// care of month drop down.
			//
			

			// SSP 4/26/02
			// EM Embeddable editors.
			//
			

			// unhook notifications and call dispose on the Override object
			//
			if ( null != this.overrideObj )
			{
				this.overrideObj.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.overrideObj.Dispose();
				this.overrideObj = null;
			}

			// SSP 5/2/05
			// Dispose off the bands after disposing off the rows. Moved the following block
			// of code at the end of this method after we dispose the rows.
			//
			

			// unhook notifications and call dispose on the ColScrollregions collection
			//
			if ( null != this.colScrollRegions )
			{
				this.colScrollRegions.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.colScrollRegions.Dispose();
				this.colScrollRegions = null;
			}

			// unhook notifications and call dispose on the RowScrollregions collection
			//
			if ( null != this.rowScrollRegions )
			{
				this.rowScrollRegions.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.rowScrollRegions.Dispose();
				this.rowScrollRegions = null;
			}

			// JJD 11/19/01
			// Clear the appearance holders
			//
			if ( this.appearanceHolders != null )
			{
				for ( LayoutAppearanceIndex i = 0; i < (LayoutAppearanceIndex)this.appearanceHolders.Length; i++ )
				{
					if ( null != this.appearanceHolders[ (int)i ] )
					{
						this.appearanceHolders[ (int)i ].SubObjectPropChanged -= this.SubObjectPropChangeHandler;
						this.appearanceHolders[ (int)i ].Reset();
						this.appearanceHolders[ (int)i ] = null;
					}
				}
			}

			// AS - 12/19/01
			if (this.scrollbarLook != null)
				this.scrollbarLook.Dispose();

			// JJD 1/11/02
			// Dispose of the UIElement
			//
			if ( this.element != null )
			{
				this.element.Dispose();
				this.element = null;
			}

			// JJD 1/16/02 - UWG880
			// Added special cursor for autosizing columns
			//
			if ( this.autoSizeCursor != null )
			{
				this.autoSizeCursor.Dispose();
				this.autoSizeCursor = null;
			}

			// JJD 1/17/02
			// Added line up and line down cursors
			//
			if ( this.lineUpCursor != null )
			{
				this.lineUpCursor.Dispose();
				this.lineUpCursor = null;
			}

			if ( this.lineDownCursor != null )
			{
				this.lineDownCursor.Dispose();
				this.lineDownCursor = null;
			}


			// SSP 7/30/02 
			// Dispose of filter and summary row images and any default editors that
			// were allocated.
			//
			// ------------------------------------------------------------------
			if ( null != this.sigmaImageFromResource )
			{
				this.sigmaImageFromResource.Dispose( );
				this.sigmaImageFromResource = null;
			}
			
			if ( null != this.filterDropDownButtonImageFromResource )
			{
				this.filterDropDownButtonImageFromResource.Dispose( );
				this.filterDropDownButtonImageFromResource = null;
			}

			if ( null != this.defaultActiveFilterDropDownButtonImage )
			{
				this.defaultActiveFilterDropDownButtonImage.Dispose( );
				this.defaultActiveFilterDropDownButtonImage = null;
			}

			// SSP 5/8/05 - NAS 5.2 Filter Row
			// 
			this.ClearFilterRowCache( );

			// SSP 7/22/03 - Fixed headers
			// 
			// ------------------------------------------------------------
			if ( null != this.fixedHeaderOnImageFromResource )
				this.fixedHeaderOnImageFromResource.Dispose( );
			this.fixedHeaderOnImageFromResource = null;

			if ( null != this.fixedHeaderOffImageFromResource )
				this.fixedHeaderOffImageFromResource.Dispose( );
			this.fixedHeaderOffImageFromResource = null;
			// ------------------------------------------------------------

			// SSP 12/22/04 - IDataErrorInfo Support
			//
			if ( null != this.dataErrorImageFromResource )
			{
				this.dataErrorImageFromResource.Dispose( );
				this.dataErrorImageFromResource = null;
			}

			// SSP 1/12/05 BR0155 UWG3736
			// Moved the code that disposes editors into the new DisposeCachedEditors method.
			//
			this.DisposeCachedEditors( );
			// ------------------------------------------------------------------

			// SSP 9/19/02 UWG1685
			// Dispose of the appearances collection when the layout gets disposed.
			// We need to do this for above bug. For more info, look in 
			// UltraGridColumn.ValueList's set. It makes sure that a value list
			// is not assigned to different grids by comparing the ValueList.Appearances.			
			//
			if ( null != this.appearances )
			{
				this.appearances.Dispose( );
				this.appearances = null;
			}

			// SSP 1/19/04 UWG2898
			// Dispose of the rows as well.
			//
			// --------------------------------------------------------------
			if ( null != this.rows )
				this.rows.Dispose( );
			this.rows = null;
			// --------------------------------------------------------------

			// SSP 5/2/05
			// Dispose off the bands after disposing off the rows. Moved the following block
			// of code here from above in the middle of this method.
			//
			// unhook notifications and call dispose on the bands collection
			//
			if ( null != this.sortedBands )
			{
				this.sortedBands.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.sortedBands.Dispose();
				this.sortedBands = null;
			}

			// SSP 7/7/05 - NAS 5.3 Empty Rows
			// 
			if ( null != this.emptyRowSettings )
			{
				this.emptyRowSettings.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.emptyRowSettings.Dispose( );
			}

            // MBS 7/9/08 - BR33993
            if (this.valuelists != null)
            {
                foreach (ValueList vl in this.valuelists)
                    vl.ReleaseResources();
            }
            //
            if (this.filterOperatorValueListAll != null)
                this.filterOperatorValueListAll.ReleaseResources();

            // CDS 9.2 RowSelector State-Specific Images
            if (this.rowSelectorImages != null)
            {
                this.rowSelectorImages.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
                this.rowSelectorImages.Dispose();
                this.rowSelectorImages = null;
            }

			base.OnDispose();
		}

		internal void InitGrid( UltraGridBase grid ) 
		{
			this.grid       = grid;

			// SSP 8/12/04 UWG3606
			// If the grid is null then don't assume it's going to be something other than
			// UltraGrid. Also added code in InitGrid to clear out the viewStyleImpl so we
			// recreate it.
			//
			// SSP 9/14/04 - Optimizations
			// Pass in the new parameter.
			//
			this.ClearViewStyle( true );
		}


		// SSP 4/26/02
		// EM Embeddable editors.
		//
		

		internal void UpdateUIElementRect()
		{
			if ( null == this.grid )
				return;

			this.UIElement.Rect = this.grid.ClientRectangle;

			// Call the new CheckIfSizeChanged methods off the region collections
			// so that the AfterRegionSizeChanged events will be fired (if needed )
			//
			this.RowScrollRegions.CheckIfSizeChanged();

			this.ColScrollRegions.CheckIfSizeChanged();
		}

		internal void DirtyGridElement( )
		{
			this.DirtyGridElement( true, true );
		}

		internal void DirtyGridElement( bool primaryMetricsDirty )
		{
			this.DirtyGridElement( primaryMetricsDirty, true );
		}

		internal void DirtyGridElement( bool primaryMetricsDirty, bool fInvalidate )
		{
            this.DirtyGridElement( primaryMetricsDirty, fInvalidate, false );
		}

		// SSP 3/30/05 - NAS 5.2
		// Added an overload of DirtyGridElement that takes in dirtyColMetrics parameter.
		//
		internal void DirtyGridElement( bool primaryMetricsDirty, bool fInvalidate, bool dirtyColMetrics )
		{
			// if the bPrimaryMetricsDirty param is true set the 
			// m_bElementPrimaryMetricsDirty flag to true which will cause
			// a SetRect all on the grid element during the next draw
			// operation
			//
			if ( primaryMetricsDirty )
			{
				
				//elementPrimaryMetricsDirty  = true;
							
				this.RowScrollRegions.DirtyAllVisibleRows();
			}

			// SSP 3/30/05 - NAS 5.2
			// Added an overload of DirtyGridElement that takes in dirtyColMetrics parameter.
			//
			if ( dirtyColMetrics )
			{
				if ( null != this.colScrollRegions )
				{
					this.colScrollRegions.DirtyMetrics( );
					this.synchronizedColListsDirty = true;
				}
			}

			// the the grid elements dirty flag so that all sub element
			// rects will be validated on the next draw
			//
            
			if ( null != this.grid )
			{
				// JJD 11/02/01
				// Don't have to invalidate until after the first draw
				//
				if ( !this.grid.FirstDraw )
					this.UIElement.DirtyChildElements( fInvalidate );
			}

		}

		#region DirtyColMetrics

		// SSP 5/6/05
		// Added DirtyColMetrics on the layout for convenience.
		//
		internal void DirtyColMetrics( bool setScrollToBeginning )
		{
			if ( null != this.colScrollRegions )
				this.colScrollRegions.DirtyMetrics( setScrollToBeginning );
		}

		#endregion // DirtyColMetrics

		#region DirtyComboArea

		// SSP 1/21/05 BR01891
		// Added DirtyComboArea.
		//
		internal void DirtyComboArea( )
		{
			UltraCombo combo = this.grid as UltraCombo;
			if ( null != combo )
			{
				UIElement elem = combo.UIElement;
				if ( null != elem )
					elem.DirtyChildElements( );
			}
		}

		#endregion // DirtyComboArea

		// SSP 8/29/04 UWG3610
		// Added DirtyDataAreaElement method.
		//
		internal void DirtyDataAreaElement( )
		{
			this.DirtyDataAreaElement( false, true );
		}
		
		internal void DirtyDataAreaElement( bool primaryMetricsDirty, bool invalidate )
		{
			this.DirtyDataAreaElement( primaryMetricsDirty, invalidate, false );
		}
		
		internal void DirtyDataAreaElement( bool primaryMetricsDirty, bool invalidate, bool bumpCellElemsVersion )
		{
			if ( primaryMetricsDirty )
				this.RowScrollRegions.DirtyAllVisibleRows( );

			if ( null != this.grid && ! this.grid.FirstDraw )
			{
				Infragistics.Win.UltraWinGrid.UltraGridUIElement mainElem = this.UIElement;
				UIElement elem = null != mainElem && null != mainElem.DataAreaElement
					? (UIElement)mainElem.DataAreaElement : (UIElement)mainElem;

				if ( null != elem )
					elem.DirtyChildElements( invalidate );

				if ( bumpCellElemsVersion )
					this.BumpCellChildElementsCacheVersion( );
			}
		}

		// SSP 11/22/04 - Merged Cell Feature
		// Added DataAreaElement property.
		//
		internal DataAreaUIElement DataAreaElement
		{
			get
			{
				UltraGridUIElement elem = this.UIElement;
				return null != elem ? elem.DataAreaElement : null;
			}
		}

		// SSP 12/13/04 - Merged Cell Feature
		//
		internal void BumpMergedCellVersion( )
		{
			if ( null != this.sortedBands )
			{
				foreach ( UltraGridBand band in this.sortedBands )
					band.BumpMergedCellVersion( );
			}
		}

		internal int BandsOverallExtent
		{
			get
			{
				// make sure we calculate it if necessary
				//
				if ( !this.ColScrollRegions.InitializingMetrics )
					this.ColScrollRegions.InitializeMetrics();

				return this.bandsOverallExtent;
			}

			set
			{
				this.bandsOverallExtent = value;
			}
		}

			#region GetOverallExtent

		internal int GetOverallExtent( ColScrollRegion csr )
		{
			// get the overall extent based on for non cardview bands
			//
			int overallExtent = this.BandsOverallExtent;

			if ( csr.Extent > 0 )
			{
				// Loop over cardview bands to adjust the overall extent
				//
				for ( int i = 0; i < this.SortedBands.Count; i++ )
				{
					UltraGridBand band = this.SortedBands[i];

					// bypass hidden bands or bands that are not card view
					//
					if ( !band.CardView || band.HiddenResolved )
						continue;

					// get the max card columns that  can be displayed
					//
					int cardColumns;

					if ( band.CardSettings.MaxCardAreaCols > 0 )
						cardColumns = band.CardSettings.MaxCardAreaCols;
					else
						cardColumns = 3;

					// Calculate the extent required to display those cards
					//
					int cardAreaExtent	= band.CalculateRequiredCardAreaWidth( cardColumns );

					if ( band.CardSettings.AutoFit )
					{
						// restrict the extent to the available extent of
						// the column scrolling region
						//
						cardAreaExtent = Math.Min( cardAreaExtent, csr.Extent );
					}
					else
					{
						// if the width needed is > the available width in the region 
						// decrement the # of columns and recalculate the width needed 
						// until it is not > the available width or the columns = 1.
						//
						while (cardAreaExtent > csr.Extent && cardColumns > 1)
						{
							cardColumns--;
							cardAreaExtent = band.CalculateRequiredCardAreaWidth( cardColumns );
						}
					}

					// adjust the overall extent to accomodate this band
					//
					overallExtent  = Math.Max( overallExtent, cardAreaExtent + band.GetOrigin( BandOrigin.PreRowArea ) );
				}
			}

			return overallExtent;
		}

			#endregion GetOverallExtent


		/// <summary>
		/// Returns true if the object is attached to a grid that is firing the event.
		/// </summary>
		/// <returns></returns>
		/// <remarks>
		/// <p class="body">You can use the <b>InProgress</b> property in your event code to determine whether the code is being executed by the object that fired the event. This property is also useful when you have multiple instances of a control, as it will always apply to a specific instance.</p>
		/// </remarks>
		bool InProgress( Enum eventId )
		{			
			return null != this.grid && this.grid.IsEventInProgress( eventId );				
		}

													   
		internal void ValidateSynchronizedColLists()
		{
			if ( this.grid is UltraGrid )
			{
				if ( !this.synchronizedColListsDirty || this.InProgress(GridEventIds.InitializeLayout) )
					return;
			}

			else
			{
				if ( !this.synchronizedColListsDirty || this.InProgress(DropDownEventIds.InitializeLayout) )
					return;
			}


			//Infragistics.Win.UltraWinGrid.ViewStyle vwStyle = this.ViewStyle;
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//ViewStyleBase vwStyle = this.ViewStyleImpl;

			synchronizedColListsDirty = false;

			if ( this.sortedBands != null )
			{
				// SSP 8/13/05 BR04633 BR04666
				// This is to make column synchronization work when auto-fit functionality is turned on.
				// Moved the code into the new ClearSynchronizedColLists method.
				// 
				// --------------------------------------------------------------------------------------
				int nSynchronizedBands = this.ClearSynchronizedColLists( );
				
				// --------------------------------------------------------------------------------------

				// if at least one band needs col synchronization
				// call BuildSynchronizedColLists on band 0
				//
				if ( nSynchronizedBands > 0 )
				{	
					if ( this.sortedBands.Count > 0 && this.sortedBands[0] != null )
						this.sortedBands[0].BuildSynchronizedColLists( null );
				}	
			}
		}

		#region ClearSynchronizedColLists

		// SSP 8/13/05 BR04633 BR04666
		// This is to make column synchronization work when auto-fit functionality is turned on.
		// Added ClearSynchronizedColLists method. Code in there was moved from the 
		// ValidateSynchronizedColLists method.
		// 
		internal int ClearSynchronizedColLists( )
		{
			int nSynchronizedBands = 0;

			ViewStyleBase vwStyle = this.ViewStyleImpl;
			if ( null != this.sortedBands && null != vwStyle )
			{
				for ( int index = 0; index < this.sortedBands.Count; index++ )
				{
					UltraGridBand band = this.sortedBands[index];

					// if the band is hidden then continue
					//
					if ( band.HiddenResolved )
						continue;

					// count the number of bands that need synchronization
					//
					if ( band.AllowColSizingResolved != AllowColSizing.Free )
						nSynchronizedBands++;

					if ( band.Columns != null )
					{
						ColumnsCollection columns = band.Columns;
						for( int i = 0, count = columns.Count; i < count; i++ )
						{
							columns[i].RemoveFromSynchronizedColList();
						}
					}

					// if we have a single band display break since we
					// are only interested in band 0
					//
					if ( !vwStyle.IsMultiBandDisplay )
						break;
				}
			}

			return nSynchronizedBands;
		}

		#endregion // ClearSynchronizedColLists

		/// <summary>
		/// Resets all appearanceHolders in the appearanceHolder array
		/// </summary>
		public void ResetAppearanceHolders() 
		{
			if ( this.appearanceHolders != null )
			{
				for ( LayoutAppearanceIndex i = 0; i < (LayoutAppearanceIndex)this.appearanceHolders.Length; i++ )
				{
					if ( null != this.appearanceHolders[ (int)i ] )
						this.ResetAppearance(i);
				}
			}

		}

		[ Browsable(false), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		private Infragistics.Win.AppearanceHolder[] AppearanceHolders
		{
			get
			{
				// create the appearanceHolders on the 
				if ( null == this.appearanceHolders )
					this.appearanceHolders = new Infragistics.Win.AppearanceHolder[LastLayoutAppearanceIndex + 1];

				return this.appearanceHolders;
			}
		}
		internal bool GetHasAppearance( LayoutAppearanceIndex index )
		{
			return ( null != this.appearanceHolders && 
				null != this.appearanceHolders[ (int)index ] &&
				this.appearanceHolders[ (int) index ].HasAppearance );
		}
		private bool ShouldSerializeAppearance( LayoutAppearanceIndex index ) 
		{
			return ( this.GetHasAppearance( index ) && 
				this.GetAppearance( index ).ShouldSerialize() );
		}
		private void ResetAppearance( LayoutAppearanceIndex index ) 
		{
			// SSP 8/20/02
			// When we are trying to reset the appearance associated with passed in
			// layout appearance index, why are we checking if we have the appearance.
			// (LayoutAppearanceIndex passed in could be Caption, and we are checking
			// here if we have the Appearance).
			//
			//if ( this.HasAppearance )
			if ( this.HasLayoutAppearance( index ) )
			{
				if ( null != this.appearanceHolders[ (int)index ] )
				{
					//					// remove the prop change notifications
					//					//
					//					this.appearanceHolders[ (int)index ].Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
                    
					// JJD 11/19/01
					// Call Reset on the holder instead of its Appearance
					//
					this.appearanceHolders[ (int)index ].Reset();	
					// AS - 10/2/01   We only need to reset the appearance data, not unhook.
					//this.appearanceHolders[ (int)index ].Appearance.Reset();

					// SSP 8/14/02 UWG1290
					// Calling Reset on the appearance holder does not fire NotifyPropChange
					// so OnSubObjectPropChanged of the layout which dirties the cached height
					// values never gets called. So we need to do it here. Why doesn't reseting
					// the appearance holder does not fire the prop change ?????????
					//
					if ( LayoutAppearanceIndex.Default == index )
						this.OnBaseFontChanged( );
	
					// Notify listeners that the appearance has changed. 
                    // MRS NAS v8.2 - Unit Testing
					//this.NotifyPropChange(PropertyIds.Appearance);
                    this.NotifyPropChange(UltraGridLayout.AppearancePropIds[(int)index], null);
				}
			}
		}
 
		// SSP 8/20/02
		// Added HasLayoutAppearance method
		//
		private bool HasLayoutAppearance( LayoutAppearanceIndex index )
		{
			if ( null != this.appearanceHolders && 
				null != this.appearanceHolders[ (int)index ] &&
				this.appearanceHolders[ (int)index ].HasAppearance )
				return true;

			return false;
		}

		// SSP 4/1/05 - Optimizations
		// Added GetAppearanceIfAllocated.
		//
		internal Infragistics.Win.AppearanceBase GetAppearanceIfAllocated( LayoutAppearanceIndex index )
		{
			if ( null != this.appearanceHolders )
			{
				AppearanceHolder holder = this.appearanceHolders[ (int)index ];
				if ( null != holder && holder.HasAppearance )
					return holder.Appearance;
			}

			return null;
		}

		internal Infragistics.Win.AppearanceBase GetAppearance( LayoutAppearanceIndex index )
		{
			if ( null == this.AppearanceHolders[ (int)index ] )
			{
                
				this.AppearanceHolders[ (int)index ] = new Infragistics.Win.AppearanceHolder();
            
				// hook up the prop change notifications
				//
				this.appearanceHolders[ (int)index ].SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}

			this.AppearanceHolders[ (int)index ].Collection = this.Appearances;

			return this.AppearanceHolders[ (int)index ].Appearance;
		}

		private void SetAppearance( LayoutAppearanceIndex index, Infragistics.Win.AppearanceBase appearance )
		{
			// SSP 11/28/01 UWG778
			// Moved this if statement from within the second if statement to here.
			//
			// SSP 11/9/01 UWG722
			// if this.AppearanceHolders[ (int)index ] is null, then fill in the slot
			// with a new appearance holder before going ahead with setting appearance 
			// on the appearance holder.
			//
			if ( null == this.AppearanceHolders[ (int)index ] )
			{
				this.AppearanceHolders[ (int)index ] = new Infragistics.Win.AppearanceHolder();
            
				// hook up the prop change notifications
				//
				this.appearanceHolders[ (int)index ].SubObjectPropChanged += this.SubObjectPropChangeHandler;
			}

			// AS 2/23/06 Optimization
			// This could cause an appearance to be created just to do 
			// a reference comparison and throw it away so first check to
			// see that it has an appearance.
			//
			//if ( appearance != this.AppearanceHolders[ (int)index ].Appearance )
			if ( (null != appearance && this.AppearanceHolders[ (int)index ].HasAppearance == false) ||
				appearance != this.AppearanceHolders[ (int)index ].Appearance )
			{
				this.AppearanceHolders[ (int)index ].Collection = this.Appearances;
				this.AppearanceHolders[ (int)index ].Appearance = appearance;

				this.NotifyPropChange( UltraGridLayout.AppearancePropIds[ (int)index ], null );
			}
		}

		/// <summary>
		/// Returns true if this is the grid's main display layout.
		/// </summary>
		/// <remarks>
		/// <p class="body">Since you may want to apply different layouts for display and printing, this property indicates that this layout is being used for display purposes. Use <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.IsPrintLayout"/> to determine if the layout is being used for printing.</p>
		/// </remarks>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool IsDisplayLayout
		{
			get
			{
				return ( null != this.grid &&
					this == this.grid.DisplayLayout );
			}
		}

		/// <summary>
		/// Returns true if this is the layout being used for printing.
		/// </summary>
		/// <remarks>
		/// <p class="body">Since you may want to apply different layouts for display and printing, this property indicates that this layout is being used for printing purposes. Use <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout.IsDisplayLayout"/> to determine if the layout is being used for display.</p>
		/// </remarks>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool IsPrintLayout
		{
			get
			{
				return ( null != this.grid &&
					this == this.grid.PrintLayout );
			}
		}

		internal Rectangle DataArea
		{
			get
			{
				// SSP 11/5/01
				// Imlemented
				//
				// SSP 11/22/04 - Optimization
				// Use the new DataAreaElement property instead.
				//
				//Infragistics.Win.UIElement elem = this.GetUIElement( false );
				//elem = elem.GetDescendant( typeof( DataAreaUIElement ) );
				UIElement elem = this.DataAreaElement;
			
				if ( null != elem )
					return elem.RectInsideBorders;

				return Rectangle.Empty;
			}
		}

		internal UIElementBorderStyle BorderStyleSplitter
		{
			get
			{
				// SSP 3/25/06 - App Styling
				// 
				
				UIElementBorderStyle borderStyle = StyleUtils.GetRoleBorderStyle( this, StyleUtils.Role.ScrollRegionSplitterBar );

				if ( UIElementBorderStyle.Default == borderStyle )
					borderStyle = UIElementBorderStyle.Raised;

				return borderStyle;

				
				
			}
		}

		/// <summary>
		/// The width of the splitterbars based on the splitter border style (read-only)
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
		public int SplitterBarWidth
		{
			get
			{
				int borderThickness = this.GetBorderThickness( this.BorderStyleSplitter );				
				int faceWidth = 3; 

				return faceWidth + ( 2 * borderThickness ); 
			}
		}

		#region ColScrollRegionSplitBoxWidthResolved

		// SSP 5/12/06 - App Styling
		// 
		internal int ColScrollRegionSplitBoxWidthResolved
		{
			get
			{
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.ColScrollRegionSplitBoxWidth, out val ) )
				{
					val = StyleUtils.CachePropertyValue( this, StyleUtils.CustomProperty.ColScrollRegionSplitBoxWidth,
						0, 0, this.SplitterBarWidth );
				}

				return (int)val;
			}
		}

		#endregion // ColScrollRegionSplitBoxWidthResolved

		#region RowScrollRegionSplitBoxHeightResolved

		// SSP 5/12/06 - App Styling
		// 
		internal int RowScrollRegionSplitBoxHeightResolved
		{
			get
			{
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.RowScrollRegionSplitBoxHeight, out val ) )
				{
					val = StyleUtils.CachePropertyValue( this, StyleUtils.CustomProperty.RowScrollRegionSplitBoxHeight,
						0, 0, this.SplitterBarWidth );
				}

				return (int)val;
			}
		}

		#endregion // RowScrollRegionSplitBoxHeightResolved

		#region ColScrollRegionSplitterBarWidthResolved

		// SSP 5/12/06 - App Styling
		// 
		internal int ColScrollRegionSplitterBarWidthResolved
		{
			get
			{
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.ColScrollRegionSplitterBarWidth, out val ) )
				{
					val = StyleUtils.CachePropertyValue( this, StyleUtils.CustomProperty.ColScrollRegionSplitterBarWidth,
						0, 0, this.SplitterBarWidth );
				}

				return (int)val;
			}
		}

		#endregion // ColScrollRegionSplitterBarWidthResolved

		#region RowScrollRegionSplitterBarHeightResolved

		// SSP 5/12/06 - App Styling
		// 
		internal int RowScrollRegionSplitterBarHeightResolved
		{
			get
			{
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.RowScrollRegionSplitterBarHeight, out val ) )
				{
					val = StyleUtils.CachePropertyValue( this, StyleUtils.CustomProperty.RowScrollRegionSplitterBarHeight,
						0, 0, this.SplitterBarWidth );
				}

				return (int)val;
			}
		}

		#endregion // RowScrollRegionSplitterBarHeightResolved

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values
		/// </summary>
        /// <returns>Returns true if the object needs to be serialized.</returns>
		internal protected bool ShouldSerialize() 
		{
            return
                this.ShouldSerializeAddNewBox() ||
                //				this.ShouldSerializeAlphaBlendEnabled()||
                this.ShouldSerializeAppearance() ||
                this.ShouldSerializeAppearances() ||
                this.ShouldSerializeBands() ||
                this.ShouldSerializeBorderStyleCaption() ||
                this.ShouldSerializeCaptionAppearance() ||
                this.ShouldSerializeColScrollRegions() ||
                this.ShouldSerializeBorderStyle() ||
                this.ShouldSerializeInterBandSpacing() ||
                this.ShouldSerializeKey() ||
                this.ShouldSerializeMaxColScrollRegions() ||
                this.ShouldSerializeMaxRowScrollRegions() ||
                this.ShouldSerializeOverride() ||
                this.ShouldSerializeRowConnectorColor() ||
                this.ShouldSerializeRowConnectorStyle() ||
                this.ShouldSerializeRowScrollRegions() ||
                this.ShouldSerializeScrollbars() ||
                this.ShouldSerializeTabNavigation() ||
                this.ShouldSerializeTag() ||
                this.ShouldSerializeValueLists() ||
                this.ShouldSerializeViewStyle() ||
                this.ShouldSerializeViewStyleBand() ||
                // SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
                // Obsoleted AutoFitColumns and added AutoFitStyle property.
                //
                //this.ShouldSerializeAutoFitColumns() ||
                this.ShouldSerializeAutoFitStyle() ||
                //RobA 6/7/01 UWG124
                this.ShouldSerializeAppearanceHolders() ||
                // AS - 12/19/01
                this.ShouldSerializeScrollBarLook() ||
                // SSP 2/28/02
                // Added ScrollStyle property
                ShouldSerializeScrollStyle() ||
                // SSP 5/31/02 UWG1152
                // Added MaxBandDepth property
                //
                this.ShouldSerializeMaxBandDepth() ||
                // SSP 6/11/02
                // Added PriorityScrolling property.
                //
                this.ShouldSerializePriorityScrolling() ||
                // SSP 7/19/02
                // 
                this.ShouldSerializeFilterDropDownButtonImage() ||
                this.ShouldSerializeFilterDropDownButtonImageActive() ||
                this.ShouldSerializeSummaryButtonImage() ||

                // SSP 5/19/03 - Fixed headers
                //
                this.ShouldSerializeUseFixedHeaders() ||
                this.ShouldSerializeFixedHeaderOnImage() ||
                this.ShouldSerializeFixedHeaderOffImage() ||

                // SSP 3/29/05 - NAS 5.2 Fixed Rows
                //
                this.ShouldSerializeFixedRowOnImage() ||
                this.ShouldSerializeFixedRowOffImage() ||

                // SSP 5/22/03
                // Added a property to allow the user to control the small change of the column scroll bar.
                //
                this.ShouldSerializeColumnScrollbarSmallChange() ||
                // SSP 11/24/03 - Scrolling Till Last Row Visible
                // Added ScrollBounds property.
                //
                this.ShouldSerializeScrollBounds() ||
                // SSP 4/13/04 - Virtual Binding
                // Added LoadStyle property to UltraGridLayout.
                //
                this.ShouldSerializeLoadStyle() ||

                // JAS 4/5/05 v5.2 CaptionVisible property
                //
                this.ShouldSerializeCaptionVisible() ||

                // SSP 5/3/05 - NewColumnLoadStyle
                // Added NewColumnLoadStyle and NewBandLoadStyle.
                //
                this.ShouldSerializeNewColumnLoadStyle() ||
                this.ShouldSerializeNewBandLoadStyle() ||

                // SSP 6/30/05 - NAS 5.3 Column Chooser
                //
                this.ShouldSerializeColumnChooserEnabled() ||

                // SSP 7/6/05 - NAS 5.3 Empty Rows
                // 
                this.ShouldSerializeEmptyRowSettings() ||
                // SSP 11/15/05 - NAS 6.1 Multi-cell Operations
                // 
                this.ShouldSerializeClipboardCellDelimiter() ||
                this.ShouldSerializeClipboardCellSeparator() ||
                this.ShouldSerializeClipboardRowSeparator() ||
                // SSP 8/28/06 - NAS 6.3
                // Added CellHottrackInvalidationStyle property on the layout.
                // 
                this.ShouldSerializeCellHottrackInvalidationStyle() ||
                // MBS 1/21/08 - BR29252
                //
                this.ShouldSerializeUseOptimizedDataResetMode() ||
                // MBS 4/24/08 - Unit Test fixes
                this.ShouldSerializeAllowCardPrinting() ||
                this.ShouldSerializeGroupByBox() ||
                this.ShouldSerializeSelectionOverlayBorderThickness() ||
                // MBS 4/17/09 - NA9.2 Selection Overlay
                this.ShouldSerializeSelectionOverlayBorderColor() ||
                this.ShouldSerializeSelectionOverlayColor() ||

                // CDS 9.2 RowSelector State-Specific Images
                this.ShouldSerializeRowSelectorImages() ||

                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (DefaultSelectedBackColor & DefaultSelectedForeColor)
                this.ShouldSerializeDefaultSelectedBackColor() ||
                this.ShouldSerializeDefaultSelectedForeColor();
		}
 
		/// <summary>
		/// Resets all Layout properties back to their default values
		/// </summary>
		public void Reset() 
		{
			// AS - 11/21/01 UWG762
			// This internal flag is normally used to note that we are initializing from
			// another band but I think the same applies when resetting the layout. Without
			// this, we were generating an exception because we got into the OnColumnsChanged
			// logic.
			//
			this.initializingAllSubItems = true;

			// SSP 11/26/01 UWG709
			// Reset the group by box.
			//
			this.ResetGroupByBox( );

			this.ResetAddNewBox();
			//			this.ResetAlphaBlendEnabled();
			this.ResetAppearance();
			this.ResetAppearanceHolders();

			// SSP 9/26/02 UWG1525
			// Moved this from below to here. What happens is that
			// when the bands are reset, somehow the col scroll region matrices get
			// reinitialized prematurely and the width of the columns is calcualted
			// before the the AutoFitColumns property is reset. So the widths don't
			// get reset this method is called. The place where matrices get renitialized
			// is when the BandsCollection.OnSubObjectPropChange calls GetColScrollRegions
			// with true as the initializeMetrics parameter. It's being done in
			// couple of places and I didn't want to change that. So instead I am moving
			// this up before the band's get reset. This will fix the UWG1525 and seems
			// less 'dangerous'.
			//
			// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
			// Obsoleted AutoFitColumns and added AutoFitStyle property.
			//
			//this.ResetAutoFitColumns();
			this.ResetAutoFitStyle( );

			this.ResetBands();
			this.ResetInterBandSpacing();
			this.ResetBorderStyle();
			this.ResetBorderStyleCaption();
			this.ResetCaptionAppearance();
			this.ResetColScrollRegions();
					
			// JJD 8/28/01 - UWG104
			// Don't reset the key property since that will blow away
			// the key into the layouts collection
			//
			// this.ResetKey();
		
			this.ResetMaxColScrollRegions();
			this.ResetMaxRowScrollRegions();
			this.ResetOverride();
			this.ResetRowConnectorColor();
			this.ResetRowConnectorStyle();
			this.ResetRowScrollRegions();
			this.ResetScrollbars();
			this.ResetTabNavigation();
			this.ResetValueLists();
			this.ResetViewStyle();
			this.ResetViewStyleBand();	

			// SSP 9/26/02 UWG1525
			// Moved this from here to before we reset the bands above. What happens is that
			// when the bands are reset, somehow the col scroll region matrices get
			// reinitialized prematurely and the width of the columns is calcualted
			// before the the AutoFitColumns property is reset. So the widths don't
			// get reset this method is called. The place where matrices get renitialized
			// is when the BandsCollection.OnSubObjectPropChange calls GetColScrollRegions
			// with true as the initializeMetrics parameter. It's being done in
			// couple of places and I didn't want to change that. So instead I am moving
			// this up before the band's get reset. This will fix the UWG1525 and seems
			// less 'dangerous'.
			//
			//this.ResetAutoFitColumns();

			// AS - 12/19/01
			this.ResetScrollBarLook();

			// SSP 2/28/02
			// Added ScrollStyle property
			//
			this.ResetScrollStyle( );

			// SSP 5/31/02 UWG1152
			// Added MaxBandDepth property
			//
			this.ResetMaxBandDepth( );
			// SSP 6/11/02
			//
			this.ResetPriorityScrolling( );

			// SSP 7/19/02
			// 
			this.ResetFilterDropDownButtonImage( );
			this.ResetFilterDropDownButtonImageActive( );
			this.ResetSummaryButtonImage( );										

			// SSP 5/19/03 - Fixed headers
			//
			this.ResetUseFixedHeaders( );
			this.ResetFixedHeaderOnImage( );
			this.ResetFixedHeaderOffImage( );

			// SSP 3/29/05 - NAS 5.2 Fixed Rows
			//
			this.ResetFixedRowOnImage( );
			this.ResetFixedRowOffImage( );

			// SSP 5/22/03
			// Added a property to allow the user to control the small change of the column scroll bar.
			//
            this.ResetColumnScrollbarSmallChange( );

			// SSP 11/24/03 - Scrolling Till Last Row Visible
			// Added ScrollBounds property.
			//
			this.ResetScrollBounds( );

			// SSP 4/13/04 - Virtual Binding
			// Added LoadStyle property to UltraGridLayout.
			//
			this.ResetLoadStyle( );

			// JAS 4/5/05 v5.2 CaptionVisible property
			//
			this.ResetCaptionVisible();

			// SSP 5/10/05 - NAS 5.2 Filter Row
			//
			this.ResetFilterOperatorsValueList( );

			// SSP 5/3/05 - NewColumnLoadStyle
			// Added NewColumnLoadStyle and NewBandLoadStyle.
			//
			this.ResetNewColumnLoadStyle( );
			this.ResetNewBandLoadStyle( );

			// SSP 6/30/05 - NAS 5.3 Column Chooser
			//
			this.ResetColumnChooserEnabled( );

			// SSP 7/6/05 - NAS 5.3 Empty Rows
			// 
			this.ResetEmptyRowSettings( );

			// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
			// 
			this.ResetClipboardCellDelimiter( );
			this.ResetClipboardCellSeparator( );
			this.ResetClipboardRowSeparator( );

			// SSP 8/28/06 - NAS 6.3
			// Added CellHottrackInvalidationStyle property on the layout.
			// 
			this.ResetCellHottrackInvalidationStyle( );

            // MBS 1/21/08 - BR29252
            //
            this.ResetUseOptimizedDataResetMode();

            // MRS NAS v8.2 - CardView Printing
            this.ResetAllowCardPrinting();

            // MRS NAS v8.2 - Unit Testing
            this.ResetUseScrollWindow();

            // MBS 4/17/09 - NA9.2 Selection Overlay
            this.ResetSelectionOverlayBorderColor();
            this.ResetSelectionOverlayColor();
            this.ResetSelectionOverlayBorderThickness();

            // CDS 9.2 RowSelector State-Specific Images
            this.ResetRowSelectorImages();

            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (DefaultSelectedBackColor & DefaultSelectedForeColor)
            this.ResetDefaultSelectedBackColor();
            this.ResetDefaultSelectedForeColor();

			// AS - 11/21/01 UWG762
			this.initializingAllSubItems = false;            
		}

		/// <summary>
		/// A collection of column scrolling region objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The ColScrollRegions collection contains the ColScrollRegion objects that represent all of the column scrolling regions that exist in the grid.</p>
		/// </remarks>
		[ Browsable(false) ] 
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public ColScrollRegionsCollection ColScrollRegions
		{
			get
			{
				if ( null == this.colScrollRegions )
				{
					this.colScrollRegions = new ColScrollRegionsCollection( this );

					// hook up so we get notified of property changes
					//
					this.colScrollRegions.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if ( this.colScrollRegions.Count < 1 )
				{
					// JJD 11/19/01
					// Only add the first region if outside the initialization
					// process
					//
					// JJD 1/21/02 - UWG955
					// Create the default region if we are firing the InitializeLayout event.
					//
					if ( this.grid != null &&
						( this.grid.InInitializeLayout || !this.grid.Initializing ) )
					{
						this.colScrollRegions.InitFirstRegion();
					}
				}

				return this.colScrollRegions;
			}
		}

		// SSP 86/03 UWG2567
		//
		internal bool ColScrollRegionsAllocated
		{
			get
			{
				return null != this.colScrollRegions;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		protected bool ShouldSerializeColScrollRegions() 
		{
			return ( null != this.colScrollRegions && 
				this.colScrollRegions.ShouldSerialize() );
		}
 
		/// <summary>
		/// Resets ColSrollRegions. The control will revert to having one main column scrolling region.
		/// </summary>
		public void ResetColScrollRegions() 
		{
			if ( null != this.colScrollRegions )
				this.colScrollRegions.Reset();
		}


		/// <summary>
		/// A collection of row scrolling region objects.
		/// </summary>
		/// <remarks>
		/// <p class="body">The RowScrollRegions collection contains the RowScrollRegion objects that represent all of the row scrolling regions that exist in the grid.</p>
		/// </remarks>
		[ Browsable(false) ] 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public RowScrollRegionsCollection RowScrollRegions
		{
			get
			{
				if ( null == this.rowScrollRegions )
				{
					this.rowScrollRegions = new RowScrollRegionsCollection( this );

					// hook up so we get notiffied of property changes
					//
					this.rowScrollRegions.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				if ( this.rowScrollRegions.Count < 1 )
				{
					// JJD 11/20/01
					// Only add the first region if outside the initialization
					// process
					//
					// JJD 1/21/02 - UWG955
					// Create the default region if we are firing the InitializeLayout event.
					//
					if ( this.grid != null &&
						( this.grid.InInitializeLayout || !this.grid.Initializing ) )
					{
						this.rowScrollRegions.InitFirstRegion();
					}
				}

				return this.rowScrollRegions;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		protected bool ShouldSerializeRowScrollRegions() 
		{
			return ( null != this.rowScrollRegions && 
				this.rowScrollRegions.ShouldSerialize() );
		}
 
		/// <summary>
		/// Resets row scroll regions. The control will revert to having one main row scrolling region.
		/// </summary>
		public void ResetRowScrollRegions() 
		{
			if ( null != this.rowScrollRegions )
				this.rowScrollRegions.Reset();
		}

		/// <summary>
		/// Returns an empty string.		
		/// </summary>
        /// <returns>An empty string.</returns>
		public override String ToString() 
		{
			// AS - 11/21/01
			// A more descriptive name should be returned.
			//
			// AS 1/8/03 - fxcop
			// Do not compare against string.empty - test the length instead
			//if (this.Key == null || this.Key == string.Empty)
			if (this.Key == null || this.Key.Length == 0)
			{
				if (this.PrimaryCollection != null)
					return this.PrimaryCollection.IndexOf(this).ToString();

				else
					return this.GetType().Name;
			}
			else
				return this.Key;
		}
		/// <summary>  
		/// Returns or sets the Appearance object that controls the object's formatting.
		/// </summary>  
		/// <remarks>
		/// <p class="body">The <b>Appearance</b> property of an object is used to associate the object with an Appearance object that will determine its appearance. The Appearance object has properties that control settings such as color, borders, font, transparency, etc. For many objects, you do not set formatting properties directly. Instead, you set the properties of an Appearance object, which controls the formatting of the object it is attached to.</p>
		/// <p class="body">There are two ways of working with the <b>Appearance</b> property and assigning the attributes of an Appearance object to other objects. One way is to create a new Appearance object, adding it directly to the Appearances collection. (Both the Appearance object and the Appearances collection are parts of the control's persistable layout and are therefore located in the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBase.DisplayLayout"/> object.) Then you assign the new Appearance object to the <b>Appearance</b> property of the object you want to format. This method uses a "named" Appearance object that you must explicitly create (and to which you must assign property settings) before it can be used. For instance, you could create an object in the control's Appearances collection and assign it some values as follows:</p>
		/// <p class="code">UltraWinGrid1.DisplayLayout.Appearances.Add "New1"</p>
		/// <p class="code">UltraWinGrid1.DisplayLayout.Appearances("New1").BorderColor = vbBlue</p>
		/// <p class="code">UltraWinGrid1.DisplayLayout.Appearances("New1").ForeColor = vbRed</p>
		/// <p class="body">Creating the object in this way does not apply formatting to any visible part of the control. The object simply exists in the collection with its property values, waiting to be used. To actually use the object, you must assign it to the control's (or another object's) <b>Appearance</b> property:</p>
		/// <p class="code">UltraWinGrid1.DisplayLayout.Appearance = UltraWinGrid1.DisplayLayout.Appearances("New1")</p>
		/// <p class="body">In this case, only one Appearance object exists. The control's appearance is governed by the settings of the "New1" object in the collection. Any changes you make to the object in the collection will immediately be reflected in the control.</p>
		/// <p class="body">The second way of working with the <b>Appearance</b> property is to use it to set property values directly, such as:</p>
		/// <p class="code">UltraWinGrid1.DisplayLayout.Appearance.ForeColor = vbBlue</p>
		/// <p class="body">In this case, an Appearance object is automatically created by the control. This Appearance object is not a member of an Appearances collection and it does not have a name. It is specific to the object for which it was created; it is an "intrinsic" Appearance object. Changes to the properties of an intrinsic Appearance object are reflected only in the object to which it is attached.</p>
		/// <p class="body">Note that you can assign properties from a named Appearance object to an intrinsic Appearance object without creating a dependency relationship. For example, the following code...</p>
		/// <p class="code">UltraWinGrid1.DisplayLayout.Appearance.ForeColor = UltraWinGrid1.DisplayLayout.Appearances("New1").ForeColor</p>
		/// <p class="body">...does <i>not</i> establish a relationship between the foreground color of the intrinsic object and that of the named object. It is simply a one-time assignment of the named object's value to that of the intrinsic object. In this case, two Appearance objects exist - one in the collection and one attached to the control - and they operate independently of one another.</p>
		/// <p class="body">If you wish to assign all the properties of a named object to an intrinsic object at once without creating a dependency relationship, you can use the <b>Clone</b> method of the Appearance object to duplicate its settings and apply them. So if you wanted to apply all the property settings of the named Appearance object "New1" to the control's intrinsic Appearance object, but you did not want changes made to "New1" automatically reflected in the grid, you would use the following code:</p>
		/// <p class="code">UltraWinGrid1.DisplayLayout.Appearance = UltraWinGrid1.DisplayLayout.Appearances("New1").Clone</p>
		/// <p class="body">Note that the properties of an Appearance object can also operate in a hierarchical fashion. Certain properties can be set to a "use default" value, which indicates to the control that the property should take its setting from the object's parent. This functionality is enabled by default, so that unless you specify otherwise, child objects resemble their parents, and formatting set at higher levels of the grid hierarchy is inherited by objects lower in the hierarchy.</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_Appearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase Appearance
		{
			get
			{
				return this.GetAppearance( LayoutAppearanceIndex.Default );
			}

			set
			{
				this.SetAppearance( LayoutAppearanceIndex.Default, value );

				// MRS 4/15/05 - BR03397
				this.SynchGridBackColor();
			}
		}
		/// <summary>
		/// Returns true if an Appearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasAppearance
		{
			get
			{
				return this.GetHasAppearance( LayoutAppearanceIndex.Default );
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAppearance() 
		{
			return this.ShouldSerializeAppearance( LayoutAppearanceIndex.Default ); 
		}
 
		/// <summary>
		/// Resets the Appearance object associated with the Displaylayout.
		/// </summary>
		public void ResetAppearance() 
		{
			this.ResetAppearance( LayoutAppearanceIndex.Default ); 

			// MRS 4/15/05 - BR03397
			this.SynchGridBackColor();
		}


		/// <summary>
		/// A collection of all the Appearance objects created for use with this control.
		/// </summary>
		/// <remarks>
		/// <p class="body">The Appearances collection is used to contain Appearance objects that you have created and added to the control as pre-defined formatting templates. It does not represent a collection of all the Appearance objects that exist in the control. The intrinsic Appearance objects that are stored by objects such as the UltraGridBand, UltraGridRow, UltraGridCell are not included in the grid's Appearances collection.</p>
		/// </remarks>
		[
		LocalizedDescription("LD_UltraGridLayout_P_Appearances"),
		LocalizedCategory("LC_Appearance"),
		DesignerSerializationVisibility( DesignerSerializationVisibility.Content )
		]
		public AppearancesCollection Appearances
		{
			get
			{
				if (null == this.appearances)
				{
					// JJD 8/13/02
					// Pass this in as the image list provider.
					this.appearances = new AppearancesCollection(this);
					this.appearances.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.appearances;
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		protected bool ShouldSerializeAppearances()
		{
			if ( this.appearances != null && 
				this.appearances.Count > 0 )
				return true;	
			else
				return false;
		}

	
		/// <summary>
		/// Returns true if any appearance properties need to be
		/// persisted
		/// </summary>
		private bool ShouldSerializeAppearanceHolders() 
		{
			if ( this.appearanceHolders != null )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( null != this.appearanceHolders[i]  &&
						this.appearanceHolders[i].ShouldSerialize() )
					{
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Reset appearances to null
		/// </summary>
		public void ResetAppearances()
		{
			if ( this.appearances != null )
			{
				this.appearances.Clear();

				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.Appearances );
			}
		}


		/// <summary>
		/// Sets the formatting attributes of an object's caption based upon the Appearance object.
		/// <see cref="Infragistics.Win.Appearance"/>
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>CaptionAppearance</b> property is used to specify the appearance of the grid's caption (the grid's caption is visible whenever the <b>Caption</b> property of the grid is set to a non-empty string). When you assign an Appearance object to the <b>CaptionAppearance</b> property, the properties of that object will be applied to the grid's caption. 
		///	You can use the <b>CaptionAppearance</b> property to examine or change any of the appearance-related properties that are currently assigned to the caption, for example:</p> 
		///	<p class="code">UltraWinGrid1.CaptionAppearance.ForeColor = vbBlue</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_CaptionAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase CaptionAppearance
		{
			get
			{
				
				return this.GetAppearance( LayoutAppearanceIndex.Caption );
				
			}

			set
			{				
				this.SetAppearance( LayoutAppearanceIndex.Caption, value );				
			}
		}



		/// <summary>
		/// Returns true if an CaptionAppearance object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasCaptionAppearance
		{
			get
			{
				return this.GetHasAppearance( LayoutAppearanceIndex.Caption );
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCaptionAppearance() 
		{
			return this.ShouldSerializeAppearance( LayoutAppearanceIndex.Caption ); 
		}
 
		/// <summary>
		/// Reset appearance
		/// </summary>
		public void ResetCaptionAppearance() 
		{
			this.ResetAppearance( LayoutAppearanceIndex.Caption ); 
		}

		/// <summary>
		/// A collection of UltraGridBand objects. When used at the grid level, the collection includes all the UltraGridBand objects in the control.
		/// </summary>
		/// <remarks>
		/// <p class="body">The Bands collection contains all of the UltraGridBand objects in the grid. Each UltraGridBand object represents a single level of a hierarchical data set.</p>
		/// <see cref="Infragistics.Win.UltraWinGrid.BandsCollection"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_Bands")]
		[ LocalizedCategory("LC_Data") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden) ]
		public Infragistics.Win.UltraWinGrid.BandsCollection Bands
		{
			get
			{
				if ( null == this.bands )
				{
                    this.bands = new Infragistics.Win.UltraWinGrid.BandsCollection(this);

					// hook up the new prop change notifications
					//
                    this.bands.SubObjectPropChanged += this.SubObjectPropChangeHandler;

					// there should always be a band zero even if
					// we aren't bound
					//
                    this.bands.InternalAdd(null, null, null);
				}

				// SSP 1/6/03 UWG1902
				// Attaches the data source if not alreay attached. This is to fix a bug in the ultradropdown
				// where since the control is hidden when the application is run, OnCreateControl doesn't get
				// called. So rather than waiting for OnCreateControl to get called, this will attach the 
				// data source whenever the Bands property is accessed. Also, it will not attach any data sources
				// later on because that situation (the situation where the data source was once attached and is
				// not attached any more) should only arise when the layout is disposed of.
				//
				this.EnsureDataSourceAttached( );

                return this.bands;
			}
		}

		// MD 5/22/09 - TFS16348
		// Added an internal property so we don't end up performing any verification code when accessing the bands.
		internal Infragistics.Win.UltraWinGrid.BandsCollection BandsInternal
		{
			get
			{
				
				return this.bands;
			}
		}
		
		// SSP 1/6/03 UWG1902
		// Attaches the data source if not alreay attached. This is to fix a bug in the ultradropdown
		// where since the control is hidden when the application is run, OnCreateControl doesn't get
		// called. So rather than waiting for OnCreateControl to get called, this will attach the 
		// data source whenever the Bands property is accessed. Also, it will not attach any data sources
		// later on because that situation (the situation where the data source was once attached and is
		// not attached any more) should only arise when the layout is disposed of.
		// This is called by the Bands property.
		//
		internal void EnsureDataSourceAttached( )
		{
			if ( this.dataSourceAttachedAtLeastOnce || this.in_EnsureDataSourceAttached )
				return;

			// SSP 10/15/03 UWG2634
			// If the rows collection has not been initialized with a band, then don't
			// verify. We would be in this state while the Rows.get is in the middle of
			// allocating a rows collection. Look in the Rows.get for more info.
			//
			if ( null == this.Rows || null == this.Rows.Band )
				return;

			this.in_EnsureDataSourceAttached = true;

			try
			{
				this.dataSourceAttachedAtLeastOnce =
                    // MRS 2/4/2009 - TFS13235                    
					//null != this.sortedBands && this.sortedBands.Count > 0 && null != this.sortedBands[0].BindingManager
                    null != this.bands && this.bands.Count > 0 && null != this.bands[0].BindingManager
                    && (this.Grid != null && this.Grid.BindingContext != null); // MRS 6/26/07 - BR24212

				if ( !this.dataSourceAttachedAtLeastOnce &&
					null != this.grid && 
					!this.grid.Initializing && 
					!this.Disposed &&
					null != this.grid.DataSource &&
					null != this.grid.BindingContext &&
					1 == this.Bands.Count && null == this.Bands[0].BindingManager )
				{
					// SSP 10/11/06 - NAS 6.3
					// Added SetInitialValue on the UltraCombo.
					// 
					if ( this.grid.dontLoadDataSource )
						return;

					// Now that we have met all the requirements for verifying that the data source is
					// attached, call VerifyDataSourceAttached and set the dataSourceAttachedAtLeastOnce
					// flag to true.
					//
					this.grid.VerifyDataSourceAttached( );

					this.dataSourceAttachedAtLeastOnce = true;
				}
			}
			finally
			{
				this.in_EnsureDataSourceAttached = false;
			}
		}

		bool LoadedBandsApplied
		{
			get
			{
				return this.loadedBandsApplied;
			}
		}

		// SSP 4/29/06 BR11367
		// Added ApplyLoadedBandsHelper. Code in there is moved from the existing ApplyLoadedBands method.
		// 
		internal void ApplyLoadedBandsHelper( )
		{
			if ( null == this.sortedBands || this.sortedBands.Count <= 0 )
				return;

			if ( this.loadedBands != null )
			{
				// SSP 12/4/02 UWG1817
				// Only initialize if the bands in the loaded bands are not the same as the
				// bands in this.Bands.
				// Added below if condition to the already existing line of code that calls
				// InitializeFrom.
				//
                if (this.loadedBands.Count <= 0 || !object.ReferenceEquals(this.loadedBands[0], this.SortedBands[0]))
                {
                    // MRS 2/6/2009 - TFS13287
                    // We should never be calling InitializeFrom the Sorted Bands collection because 
                    // the initialization of the VisiblePosition property might cause might cause the order of the collection to change while we are looping through it.
                    //this.SortedBands.InitializeFrom(this.loadedBands, PropertyCategories.All);
                    this.Bands.InitializeFrom(this.loadedBands, PropertyCategories.All);
                }
			}

			// JJD 8/20/01 
			// Make sure all the appearance holders are referencing
			// the layout's appearance collection 
			//
			this.SortedBands.InitAppearanceHolders( );
		}
		
		internal void ApplyLoadedBands()
		{
			// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
			// Added inApplyLoadedBands flag. Enclosed the following code into try-finally block.
			//
			this.isApplyingLayout = true;
			try
			{
				// JJD 10/29/01
				// If the BindingContext hasn't been initialized then it would be 
				// fruitless to do anything with the data source
				//
				if ( this.Grid == null || 
					this.Grid.BindingContext == null )
					return;

				// JJD 11/26/01 - UWG290 
				// Make sure the DataSource is not null at runtime
				//
				if ( this.Grid.DataSource == null  &&
					!this.Grid.DesignMode )
					return;

				// SSP 6/18/02 UWG1229
				// Added a flag to prevent the rows from being loaded until the 
				// print layout and the main layout have been merged. We want to
				// carry over any unbound columns first before firing the initialize
				// row.
				// 
				UltraGrid ultraGrid = this.Grid as UltraGrid;
				if ( null != ultraGrid && ultraGrid.Printing_DontInitializeRows )
					return;
 
				// SSP 5/16/02 UWG1133 UWG1138
				// Only apply the loaded bands if the datasource has been initialized and
				// one we can know that is by checking the bound column's count. If it's
				// greater than 0, then apply the bands. This solves the problem where
				// the designer puts the code that calls EndInit on all the components in
				// such a way that grid's EndInit gets called before the data 
				// source's EndInit gets called. So what we end up applying the loaded
				// bands before the data source has been initialized and all the columns
				// in the bands are loaded. So do this only if the bound columns count is
				// greater than 0.
				//
				// SSP 10/15/02 UWG1614
				// Reverted this changed due to UWG1614 regression bug. To fix the original
				// bugs UWG1133 UWG1138 that prompted this change, we changed the way we
				// were applying the loaded bands in EndInit of the grid. Now instead of doing
				// the applying of loaded bands in there, we are delaying it to ensure
				// that the data source has been initialized.
				//
				if ( this.SortedBands.Count > 0 )
					//if ( this.Bands.Count > 0 && this.Bands[0].Columns.BoundColumnsCount > 0 )
				{
					// SSP 5/16/02 UWG1133 UWG1138
					// Set the flag from within the if statement below because we don't
					// want to set this flag to true without actually applying the loaded 
					// bands.
					// Moved this from aboved (before the if statement)
					// JJD 10/19/01
					// reset the flag
					//
					this.loadedBandsApplied = true;

					// SSP 4/29/06 BR11367
					// Added ApplyLoadedBandsHelper. Code in there is moved from the existing ApplyLoadedBands method.
					// 
					// ----------------------------------------------------------------------------------------------
					this.ApplyLoadedBandsHelper( );
					
					// ----------------------------------------------------------------------------------------------

					this.loadedBands = null;

					// JJD 10/19/01
					// Fire the initialize layout event now that we are all hooked
					// up and loaded bands have been applied
					//
					// AS -	10/25/01
					// At design time, we want to display two default rows
					//
					if ( //!this.grid.DesignMode &&
						//this.LoadedBandsApplied &&
						this.SortedBands[0].Columns.Count > 0 )
					{
						// JJD 11/21/01 - UWG767
						// Set the return value to true so we know if the
						// event was fired
						//
						//initializeLayoutFired = true;

						// JJD 12/03/01 - UWG813
						// Made initializeLayoutFired a member flag of UltraGridBase
						// to prevent IniitalizeLayout from firing multiple times
						//
						if ( !this.grid.InitializeLayoutFired )
							this.grid.FireInitializeLayout( new InitializeLayoutEventArgs( this ) ) ;

						// create the row objects
						//
						if ( null != this.rows )
							// SSP 4/14/04 - Virtual Binding
							// Simply Dirty the rows instead of initializing rows at this point. Any time
							// anything (like count or the indexer) off the rows is accessed, it will initialize
							// itself.
							//
							//this.rows.InitRows();
							this.rows.DirtyRows( );
					}

					this.SortedBands.DirtyOrigins();
					this.RowScrollRegions.DirtyAllVisibleRows();

					// SSP 10/22/03 UWG2306
					// Dont pass true for scrollToBeginning. Someone could've set the FirstRow
					// of a rowscrollregion before getting here and we don't want to reset
					// that to the first row.
					//
					//this.ColScrollRegions.DirtyMetrics( true );
					this.ColScrollRegions.DirtyMetrics( false );
				
					// SSP 5/26/06 - Optimizations
					// Don't force initialization of the metrics here. It will be done lazily next time
					// in the paint or when the elements are verified.
					// 
					this.ColScrollRegions.InitializeMetrics();

                    // MBS 3/5/08 - BR31041
                    // Calling InitializeMetrics will cause the cached filter versions to be verified, even
                    // though the filters might not be correctly updated at this point, so make sure that
                    // we recalculate this value later, such as during a paint.
                    if (this.rows != null)
                        this.rows.BumpRowFiltersVersion();

					this.DirtyGridElement( true );
				}
			}
			finally
			{
				this.isApplyingLayout = false;
			}
		}

		/// <summary>
		/// Returns true if any band has a non default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		protected bool ShouldSerializeBands() 
		{
            // MRS 3/12/2009 - Found while fixing TFS15230
			//if ( null != this.sortedBands )
            if (null != this.bands)
			{
				// SSP 5/25/04
				// We need to serialize the bands and columns when the data structure has
				// been extracted out of a data source.
				//
				// ------------------------------------------------------------------------
                // MRS 3/12/2009 - Found while fixing TFS15230
				//if ( this.sortedBands.Count > 0 && this.AlwaysSerializeBandsAndColumns )
                if (this.bands.Count > 0 && this.AlwaysSerializeBandsAndColumns)
					return true;
				// ------------------------------------------------------------------------

                // MRS 3/12/2009 - Found while fixing TFS15230
				//foreach( UltraGridBand band in this.sortedBands )
                foreach (UltraGridBand band in this.bands)
				{
					if ( band.ShouldSerialize() )
						return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Returns a clone of the bands collection. This is for internal use by UltraGrid to seriaize/deserialize bands.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		[ Browsable( false ) ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public BandsSerializer BandsSerializer
		{
			get
			{
				if ( this.loadedBands == null )
					this.loadedBands = new BandsSerializer( this );

				// JJD 11/20/01
				// If we are at design time and not initializing then
				// init the contents of the loadedbands object to the
				// bands in the collection.
				//
				if (
					// SSP 7/1/03 UWG2443
					// If this layout is UltraGridLayout, then don't check for the following conditions.
					// It means that this is not the main layout of the grid. It could be either a print layout
					// in which case this property would not be accessed since print layouts arise during
					// run-time only (and if the user accesses this property during runtime, then there is
					// no harm in copying the bands to the loadedBands) or it could be a layout in the 
					// LayoutsCollection that the designer is trying to serialize. If it is a layout in the
					// layout collection, then always copy the bands to the loaded bands otherwise the
					// serialization will fail because the returned loadedBands won't contain any bands
					// and thus bands collection and subobjects won't get serialized. This is what happened
					// when you tried to save a layout during design-time to a layout in the layouts collection
					// using the ultragrid layout wizard.
					// Added below condition and or'd it with the existing condition.
					//
					( typeof( UltraGridLayout ) == this.GetType( )
					  && ( null != this.sortedBands && this.loadedBands.Count <= 0 )
					)
					||
                    (					
						this.grid != null &&
						!this.grid.Initializing &&
						this.grid.DesignMode 
						// SSP 1/15/01 UWG921
						// During design time, there are times when BindingContext off the control
						// is null. In that case we won't have any bands because there won't be
						// a binding manager to create the bands from. So check to see
						// if we have a binding manager initialized on the grid and if
						// so, then and only then proceed.
						//
						// SSP 10/15/02 UWG1614
						// If the binding manager is null
						//&& null != this.Grid.BindingManager 
						&& ( null != this.Grid.BindingManager || this.loadedBands.Count <= 0 ) 
						// SSP 10/16/02 UWG1759
						// If we are in the middle of initializing with data source, then
						// don't reget the loaded bands.
						//
						&& !this.grid.InternalInSet_ListManager 
						// SSP 5/28/03 UWG2270
						// Also only do this while we are trying to serialize.
						// Added below condition.
						//
						&& this.grid.IsSerializing
					)
					)
				{
					
					
					
					
					// --------------------------------------------------------------------------------
                    // MRS 3/12/2009 - TFS15230
					//this.InitializeBandsSerializeFromBandsHelper( this.SortedBands );
                    this.InitializeBandsSerializeFromBandsHelper(this.Bands);
					
					// --------------------------------------------------------------------------------
				}

				return this.loadedBands;
			}
		}

		
		
		
		
		private void InitializeBandsSerializeFromBandsHelper( BandsCollection bands )
		{
			if ( this.loadedBands == null )
				this.loadedBands = new BandsSerializer( this );

			this.loadedBands.Clear();
					
			if ( null != bands )
			{
				foreach ( UltraGridBand band in bands )
				{
					this.loadedBands.Add( band );

					// SSP 7/1/03 UWG2443
					// Added new addedInBandsSerializer flag to the band object which indicates whether
					// the band has been added to to the bands serializer collection in here.
					//
					if ( null != band )
						band.addedInBandsSerializer = true;
				}
			}
		}

		// SSP 3/27/03 - Row Layout Functionality
		// We need to clear the loaded bands collection in the BeginInit. 
		// Added an internal method for clearing the loaded bands.
		// 
		internal void InternalClearLoadedBands( )
		{
			if ( null != this.loadedBands )
				this.loadedBands.Clear( );
		}

		// SSP 7/1/03 UWG2443
		//
		internal BandsSerializer LoadedBandsInternal
		{
			get
			{
				return this.loadedBands;
			}
		}		

		/// <summary>
		/// Returns true if any band has a non default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		protected bool ShouldSerializeBandsSerializer() 
		{
			return this.ShouldSerializeBands();
		}
 
		/// <summary>
		/// Calls the Reset method on each band
		/// </summary>
		public void ResetBands() 
		{
            // MRS 3/12/2009 - Found while fixing TFS15230
			//if ( null != this.sortedBands )
            if (null != this.bands)
			{
				// call Reset on each bound band
				//
                // MRS 3/12/2009 - Found while fixing TFS15230
				//foreach ( UltraGridBand band in this.sortedBands )
                foreach (UltraGridBand band in this.bands)
				{
					band.Reset();
				}
			}
		}

		// SSP 3/15/04 - Designer Usability Improvements Related
		// Added ExtractDataStructure method to be able to extract the data structure from
		// a data source.
		//
		internal void ExtractDataStructure( object dataSource, string dataMember )
		{
			try
			{
				Debug.Assert( ! this.extractDataStructureOnly );
				this.extractDataStructureOnly = true;

				if ( null == this.Grid )
					return;

				BindingManagerBase bindingManager = this.Grid.BindingContext[ dataSource, dataMember ];

				if ( null == bindingManager )
					return;

				this.ListManagerUpdated( bindingManager );

				if ( null != this.rows )
				{
					this.rows.InternalClearHelper( );
					this.rows.UnBind( );
				}				
			}
			finally
			{
				this.extractDataStructureOnly = false;
			}
		}

		internal bool ExtractDataStructureOnly
		{
			get
			{
				return this.extractDataStructureOnly;
			}
		}

		internal void ListManagerUpdated( )
		{
			if ( null != this.Grid )
				this.ListManagerUpdated( this.Grid.BindingManager );
		}

		#region ShouldDisplayDataFromDataSource 

		// SSP 6/8/05 BR03609
		// Added ShouldDisplayDataFromDataSource property.
		// 
		internal bool ShouldDisplayDataFromDataSource
		{
			get
			{
				return null != this.grid && null != this.grid.BindingManager && ! this.grid.DesignMode;
			}
		}

		#endregion // ShouldDisplayDataFromDataSource 

		// SSP 3/15/04 - Designer Usability Improvements Related
		// Added an overload of ListManagerUpdated that takes in a bindingManager parameter.
		//
		private void ListManagerUpdated( BindingManagerBase bindingManager )
		{
			if ( null == this.grid )
				return;

			// SSP 8/2/04 - UltraCalc
			// Remove the grid reference from the calc manager and also suspend the calc 
			// manager notifications. Also enclosed the code in try-finally block so we 
			// can resume the calc manager notifications as well as re-add the grid
			// reference to the calc manager after loading the new data structure from 
			// the data source.
			//
			this.SuspendCalcManagerNotifications( );
			try
			{		
				// SSP 5/31/02 UWG1152
				// Set the max band depth currently enforced to max band depth property.
				//
				this.maxBandDepthCurrentlyEnforced = this.MaxBandDepth;

				// SSP 3/15/04 - Designer Usability Improvements Related
				// Added an overload of ListManagerUpdated that takes in a bindingManager parameter.
				//
				//BindingManagerBase bindingManager = this.grid.BindingManager;

				if ( null == bindingManager )
				{
					// JJD 10/10/01 - UWG510
					// Since there isn't a binding manager we need to 
					// clear the bands collection and all cached rows
					//
					if ( this.grid != null )
						this.grid.ClearActiveRow();

					if ( this.rows != null )
					{
						this.rows.InternalClearHelper();

						// SSP 9/9/03 UWG2308
						// Unbind from the binding list before setting the rows collection to null.
						//
						this.rows.UnBind( );

						// SSP 11/2/04 UWG3736
						// Dispose of the row collection as well so it disposes the descendant rows
						// recursively as well. NOTE: We wre already nulling out the rows below.
						//
						this.rows.Dispose( );

						this.rows = null;

						// JJD 10/22/01
						// Null out scrollableRowCountManager so that it will
						// get recreated with the new rows collection the
						// next time someone asks for it
						//
						// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
						// Took out the scrollable count manager code. Commented out following line.
						//
						//this.scrollableRowCountManager = null;					
					}

					if ( this.bands != null )
					{
						// SSP 1/12/05 BR0155 UWG3736
						// Dispose of the bands instead of simply clearing the collection. This way
						// the bands, columns and other subobjects get their Dispose called as well.
						//
						//this.bands.Clear();
						// SSP 10/25/06 BR15871
						// At design-time, maintain the bands serializer. This way if you set the
						// DataSource property to null and then set it back then the previous 
						// settings will be maintained.
						// 
						// --------------------------------------------------------------------------
						//this.bands.Dispose( );
						// SSP 2/2/07 BR18628
						// 
						//if ( null != this.grid && this.grid.DesignMode )
						bool maintainSettings = null != this.grid && ( this.grid.DesignMode 
							|| null != this.grid.DataSource );
						if ( maintainSettings )
						{
							// Access the bands serializer to make sure that it initializes itself
							// from the bands collection before we clear the bands collection.
							// 
							BandsSerializer bandsSerializer = this.BandsSerializer;
							
							// SSP 2/2/07 BR18628
							// 
							if ( ( null == bandsSerializer || 0 == bandsSerializer.Count ) && this.bands.Count > 0 )
								this.InitializeBandsSerializeFromBandsHelper( this.bands );


							this.bands.Clear( );
						}
						else
						{
							this.bands.Dispose( );
						}
						// --------------------------------------------------------------------------
                        
						this.bands = null;                        
					}

					if ( this.colScrollRegions != null )
					{
						this.synchronizedColListsDirty = true;
						this.ColScrollRegions.DirtyMetrics( true );
					}
				
					if ( this.rowScrollRegions != null )
						this.RowScrollRegions.DirtyAllVisibleRows( true );

                    // MRS NAS 9.1 - Sibling Band Order
                    this.BumpSortedBandsVersion();

					this.DirtyGridElement( true );

					return;
				}

				Infragistics.Win.UltraWinGrid.UltraGridBand [] oldBands = null;

				// if we don't have at least 1 band then create one
				//
				if ( this.Bands.Count < 1 )
				{
					this.Bands.InternalAdd( null, null, null );
				}
				else
					if ( this.Bands.Count > 1 )
				{
					// JJD 8/27/01
					// Clear out all bands but band 0 but keep an
					// array of the other bands so that we will try to 
					// reuse them.
					//
					oldBands = this.Bands.ClearAllButBandZero();
				}

				string dataMember = this.grid.DataMember;

				if ( null != bindingManager )
				{
					// get the item props
					//
					PropertyDescriptorCollection props = bindingManager.GetItemProperties();

					bool hasNonChapteredColumns = false;
					string firstChapteredPropName = null;

					// loop over all the props to make sure there is at least
					// one no chaptered property. This can happen if the DataMember
					// is not specified. In that case the propertys can contain
					// one or more chaptered properties only
					//
					foreach ( PropertyDescriptor prop in props )
					{
						// check if this property is chaptered
						//
						if ( typeof(IList).IsAssignableFrom( prop.PropertyType )   && 
							!typeof(Array).IsAssignableFrom( prop.PropertyType ) )
						{
							// keep track of the name of the first chaptered column
							//
							if ( firstChapteredPropName == null )
								firstChapteredPropName = prop.Name;

						}
						else
						{
							hasNonChapteredColumns = true;
							break;
						}
					}

					if ( !hasNonChapteredColumns )
					{
						// use the first chaptered property name
						// append the name to the original datamember (if specified)
						//
						if ( firstChapteredPropName != null )
						{
							// AS 1/8/03 - fxcop
							// Do not compare against string.empty - test the length instead
							//if ( dataMember != null && !dataMember.Equals(String.Empty)) 
							// SSP 4/6/04
							// We should be checking for length being not 0, not length being 0.
							//
							//if ( dataMember != null && dataMember.Length == 0) 
							if ( dataMember != null && dataMember.Length != 0 ) 
								dataMember = dataMember + "." + firstChapteredPropName;
							else
								dataMember = firstChapteredPropName;

							// get the child bands binding manager
							//
							bindingManager = this.Grid.BindingContext[this.Grid.DataSource, dataMember];
						}
					}
				}


				// init the listmanager
				//
				this.bands[0].InitListManager( bindingManager, dataMember, oldBands );

				// JJD 11/26/01 - UWG290
				// Attempt to apply loaded bands that haven't been applied so far 
				//
				// JJD 11/21/01 - UWG767
				// Save return from ApplyLoadedBands so we know not to fire
				// the event twice
				//
				// JJD 12/03/01 - UWG813
				// Made initializeLayoutFired a member flag of UltraGridBase
				// to prevent IniitalizeLayout from firing multiple times
				//
				// bool initializeLayoutFired = this.ApplyLoadedBands();
				// SSP 3/15/04 - Designer Usability Improvements Related
				// Added ExtractDataStructure method to be able to extract the data structure from
				// a data source. Don't apply the serialized bands if performing data structure
				// extraction. Instead clear the serialized info.
				//
				if ( ! this.ExtractDataStructureOnly )
					this.ApplyLoadedBands();
				else
					this.loadedBands = null;

			}
			finally
			{
				this.ResumeCalcManagerNotifications( );
			}

			// fire the initialize layout event
			//
			// JJD 10/19/01
			// Only fire the InitializeLayout and init the rows if
			// the loaded bands have been applied
			//
			// AS -	10/25/01
			// At design time, we want to display two default rows
			//
			// JJD 11/21/01 - UWG767
			// Check saved return from ApplyLoadedBands so we don't fire
			// the event twice
			//
			// JJD 12/03/01 - UWG813
			// Made initializeLayoutFired a member flag of UltraGridBase
			// to prevent IniitalizeLayout from firing multiple times
			//
			if ( //!this.grid.DesignMode &&
				//!initializeLayoutFired &&
				this.LoadedBandsApplied )
			{
				if ( !this.grid.InitializeLayoutFired )
					this.grid.FireInitializeLayout( new InitializeLayoutEventArgs( this ) ) ;

				// create the row objects
				//
				// SSP 9/9/03 UWG2308
				// Don't call InitRows. We are already doing this in ApplyLoadedBands
				// above and besides rows collection keep a dirty flag so it should get
				// initialized whenever the grid is painted or user acceses count or the
				// indexer of the rows collection.
				//
				// ------------------------------------------------------------------------
				//if ( null != this.rows )
				//	this.rows.InitRows();
				// ------------------------------------------------------------------------

				// SSP 7/23/02 UWG1420
				// Notify the combo that the data source has been initialized. If the value
				// is set before the data source is set, then OnDataSourceInitialized will
				// set the selected row to the appropriate value.
				//
				UltraCombo ultraCombo = this.grid as UltraCombo;
				if ( null != ultraCombo )
					ultraCombo.InternalDataSourceInitialized( );
			}

		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyCell
		{ 
			get
			{
				UltraGrid grid = this.grid as UltraGrid;

				if ( null == grid )
					return null;

				SelectionStrategyBase strategy = null;

				// if there is an override object then get its strategy
				//
				if ( this.HasOverride )
					strategy = ((ISelectionStrategyProvider)(this.overrideObj)).SelectionStrategyCell;

				// if the strategy wasn't explicitly set then default to
				// extended
				//
				if ( strategy == null )
				{
					// if the default extended selection strategy hasn't been
					// created then do it now
					//
					if ( this.selectionStrategyCell == null )
						this.selectionStrategyCell = new SelectionStrategyExtended( grid );

					strategy = this.selectionStrategyCell;
				}

				return strategy;
			}
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyRow
		{ 
			get
			{
				UltraGrid grid = this.grid as UltraGrid;

				if ( null == grid )
					return null;

				SelectionStrategyBase strategy = null;

				// if there is an override object then get its strategy
				//
				if ( this.HasOverride )
					strategy = ((ISelectionStrategyProvider)(this.overrideObj)).SelectionStrategyRow;

				// if the strategy wasn't explicitly set then default to
				// extended
				//
				if ( strategy == null )
				{
					// if the default extended selection strategy hasn't been
					// created then do it now
					//
					if ( this.selectionStrategyRow == null )
						this.selectionStrategyRow = new SelectionStrategyExtended( grid );

					strategy = this.selectionStrategyRow;
				}

				return strategy;
			}
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyGroupByRow
		{ 
			get
			{
				UltraGrid grid = this.grid as UltraGrid;

				if ( null == grid )
					return null;

				SelectionStrategyBase strategy = null;

				// if there is an override object then get its strategy
				//
				if ( this.HasOverride )
					strategy = ((ISelectionStrategyProvider)(this.overrideObj)).SelectionStrategyGroupByRow;

				// if the strategy wasn't explicitly set then default to
				// extended
				//
				if ( strategy == null )
				{
					// if the default extended selection strategy hasn't been
					// created then do it now
					//
					if ( this.selectionStrategyGroupByRow == null )
						this.selectionStrategyGroupByRow = new SelectionStrategyExtended( grid );

					strategy = this.selectionStrategyGroupByRow;
				}

				return strategy;
			}
		}

		SelectionStrategyBase ISelectionStrategyProvider.SelectionStrategyColumn
		{ 
			get
			{
				UltraGrid grid = this.grid as UltraGrid;

				if ( null == grid )
					return null;

				SelectionStrategyBase strategy = null;

				// if there is an override object then get its strategy
				//
				if ( this.HasOverride )
					strategy = ((ISelectionStrategyProvider)(this.overrideObj)).SelectionStrategyColumn;

				// if the strategy wasn't explicitly set then default to
				// extended
				//
				if ( strategy == null )
				{
					// if the default extended selection strategy hasn't been
					// created then do it now
					//
					if ( this.selectionStrategyColumn == null )
						this.selectionStrategyColumn = new SelectionStrategyExtended( grid );

					strategy = this.selectionStrategyColumn;
				}

				return strategy;
			}
		}
		

		// SSP 1/6/05
		// Making Rows property public so one can access the rows collection of a print layout.
		//
		/// <summary>
		/// Returns the row collection associated with this layout. For display layout this is the same
		/// as the <see cref="UltraGridBase.Rows"/>.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false),
		EditorBrowsable( EditorBrowsableState.Advanced ) ]
		//internal Infragistics.Win.UltraWinGrid.RowsCollection Rows
		public Infragistics.Win.UltraWinGrid.RowsCollection Rows
		{
			get
			{
				if ( null == this.rows )
				{
					// SSP 9/5/03 UWG2634
					// this.Bands property verifies if the data source has been loaded. That could end up
					// accessing the rows collection recursively while we are trying to allocate the rows
					// collection. We don't want that to happen. What I mean by this is that we asuume in
					// all the places that Rows property always returns a rows collection. So this.Bands
					// property will access this property while we are still in here trying to allocated
					// a new rows collection however since the rows member var hasn't been set yet, we
					// will be executing the same code here again and allocating a second rows collection.
					// To put it short, set the rows member var to a new rows collection and then access
					// the Bands property.
					// ------------------------------------------------------------------------------------
					//this.rows = new RowsCollection( this.Bands[0], null );
					this.rows = RowsCollection.CreateEmptyRowsCollection( );
                    // MRS 2/4/2009 - TFS13235
					//this.rows.Initialize( this.SortedBands[0], null, null );
                    this.rows.Initialize(this.Bands[0], null, null);
					// Null out the scrollable row count manager so it gets recreated.
					//
					// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
					// Took out the scrollable count manager code. Commented out following line.
					//
					//this.scrollableRowCountManager = null;					
					// ------------------------------------------------------------------------------------
					
					// SSP 10/4/01
					// Changed the Rows.rowsDirty flag's default value from
					// true to false, so that the rows collection used for
					// something else (possibly selection) don't try to
					// sync the rows with the database. So setting the
					// dirty flag here to true.
					//
					rows.DirtyRows( );

					//this.rows.InitRows();

					// SSP 5/3/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					// Fire the new InitializeRowsCollection event.
					//
					this.rows.FireInitializeRowsCollection( );
				}

				return this.rows;
			}
		}

		// SSP 9/5/03 UWG2634
		// Added IsRowsCollectionAllocated.
		//
		internal bool IsRowsCollectionAllocated
		{
			get
			{
				return null != this.rows;
			}
		}

		// SSP 2/20/07 BR19262
		// 
		internal RowsCollection RowsIfAllocated
		{
			get
			{
				return this.rows;
			}
		}

		// SSP 8/2/06 BR14616
		// 
		internal void EnsureRowsSyncedWithDataSource( )
		{
			if ( null != this.rows && this.rows.IsRowsDirty )
			{
				this.rows.EnsureNotDirty( );
				UltraGrid grid = this.grid as UltraGrid;
				if ( null != grid )
					grid.EnsureTempActiveRowAssigned( );
			}
		}

		// SSP 5/25/04 - Extract Data Structure Related
		// Added DiposeRows method.
		//
		internal void DisposeRows( )
		{
			if ( null != this.rows )
			{
				this.rows.Dispose( );
				this.rows = null;
			}
		}
 
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		internal Infragistics.Win.UltraWinGrid.ViewStyleBase ViewStyleImpl
		{
			get
			{
				if ( null == this.viewStyleImpl )
				{
					// SSP 8/12/04 UWG3606
					// If the grid is null then don't assume it's going to be something other than
					// UltraGrid. Also added code in InitGrid to clear out the viewStyleImpl so we
					// recreate it.
					//
					//if ( this.grid is Infragistics.Win.UltraWinGrid.UltraGrid )
					if ( null == this.grid || this.grid is Infragistics.Win.UltraWinGrid.UltraGrid )
					{
						if ( this.viewStyle == Infragistics.Win.UltraWinGrid.ViewStyle.MultiBand )
						{
							if ( this.viewStyleBand == ViewStyleBand.Horizontal )   
								this.viewStyleImpl = new ViewStyleHorizontal( this );
							else if ( this.viewStyleBand == ViewStyleBand.Vertical )
								this.viewStyleImpl = new ViewStyleVertical( this );
							else
								this.viewStyleImpl = new ViewStyleOutlookGroupBy( this );
						}
						else
						{
							if ( ViewStyleBand.OutlookGroupBy == this.ViewStyleBand )
								this.viewStyleImpl = new ViewStyleOutlookGroupBySingle( this );
							else
								this.viewStyleImpl = new ViewStyleSingle( this );
						}
					}
					else
					{
						// JJD 9/24/01
						// Dropdowns and combos only use the single band view style
						//
						this.viewStyleImpl = new ViewStyleSingle( this );
					}
				}

				return this.viewStyleImpl;
			}
		}

		///	<summary>
		///	The UltraGridOverride object is used to determine the behavior of bands in the grid. Applying an UltraGridOverride to an object replaces that object's default behavior with the behavior specified by the settings of the UltraGridOverride. The DisplayLayout object has an <b>Override</b> property setting that determines the default settings for all UltraGridBands. The UltraGridBand object also has an <b>Override</b> property for settings specific to that band.
		///	</summary>
		///	<remarks>
		///	<p class="body">Because the UltraWinGrid was designed primarily to work with hierarchical data, hierarchical concepts are built into the control at many levels. One of the fundamental design attributes of the Grid is that the objects that make up the control exist in hierarchies, and are influenced by the other objects in a hierarchical fashion. Through the concept of inheritance, objects in the Grid can derive the settings of their properties from the settings of objects that exist above them in a given hierarchy.</p>
		///	<p class="body">Two of the main hierarchies you will encounter in the UltraWinGrid are the Appearance hierarchy and the Override hierarchy. The Appearance hierarchy provides a way for Grid objects to inherit the settings of the properties that affect the object's appearance, such as properties related to color, font and transparency. The Override hierarchy provides the inheritance framework for other properties of the grid that are not necessarily related to appearance. These two hierarchies are implemented through two objects: the Appearance object and the UltraGridOverride object. 
		///	Both of these objects serve as "formatters" - they offer collections of properties that are applied to other objects in order to produce a desired appearance or behavior. For example, the UltraGridBand object has an Override sub-object. All of the UltraGridBand's properties that can inherit their values exist as properties of the UltraGridBand's UltraGridOverride object; they do not appear directly as properties of the UltraGridBand object itself.</p>
		///	<p class="body">You will encounter two types of UltraGridOverride objects. Intrinsic UltraGridOverride objects are built in to other objects. They contain the Override properties associated with that object. They do not appear in the control's Overrides collection. The other type of UltraGridOverride is the stand-alone object that you can create by invoking the <b>Add</b> method of the Overrides collection. The settings of a stand-alone UltraGridOverride's properties do not have any effect on the Grid until the stand-alone object is applied to one of the intrinsic UltraGridOverride objects. 
		///	Stand-alone Overrides give you an easy way to create groups of attributes and apply them to objects as needed.</p>
		///	<p class="body">When you change the properties of an UltraGridOverride object, you are not required to specify a value for every property that object supports. Whether the UltraGridOverride object is a stand-alone object you are creating from scratch, or an intrinsic object that is already attached to some other object, you can set certain properties and ignore others. The properties you do not explicitly set are given a "use default" value that indicates there is no specific setting for that property.</p>
		///	<p class="body">Properties that are set to the "use default" value derive their settings from other objects by following an override hierarchy. In the override hierarchy, each object has a parent object from which it can inherit the actual numeric values to use in place of the "use default" values. The "use default" value should not be confused with the initial setting of the property, which is generally referred to as the default value. In many cases, the default setting of an object's property will be "use default"; this means that the property is initially set not to use a specific value. 
		///	The "use default" value will be 0 for an enumerated property (usually indicated by a constant ending in the word "default," such as HeaderClickActionDefault) or -1 (0xFFFFFFFF) for a numeric setting, such as that used by size and position-related properties.</p>
		///	<p class="body">So for example, if the UltraGridOverride object of the top-level band has its <b>HeaderClickAction</b> property set to 0 (HeaderClickActionDefault), the control will use the setting of the grid's <b>HeaderClickAction</b> property for the band, because the grid is above the top-level band in the override hierarchy. The top-most level of the override hierarchy is the UltraWinGrid control itself. If any of the UltraWinGrid's UltraGridOverride object properties are set to their "use default" values, the control uses built-in values (the "factory presets") for those properties. 
		///	For example, the factory preset of the <b>HeaderClickAction</b> property of the grid's UltraGridOverride object is the value that causes the column headers to be used for selecting columns: 1 (HeaderClickActionSelect). This is the value that will be used to determine how column headers in the grid will behave when the <b>HeaderClickAction</b> property of the grid's Override object is set to the "use default" value.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_Override")]
		[ LocalizedCategory("LC_Behavior") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Content) ]
		public Infragistics.Win.UltraWinGrid.UltraGridOverride Override
		{
			get
			{
				if ( null == this.overrideObj )
				{
					this.overrideObj = new Infragistics.Win.UltraWinGrid.UltraGridOverride( this );
            
					// hook up the prop change notifications
					//
					this.overrideObj.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}
				return this.overrideObj;
			}

			set
			{
				if ( value != this.overrideObj )
				{
					this.overrideObj = value;
					this.NotifyPropChange( PropertyIds.Override, null );
				}
			}
		}

		/// <summary>
		/// Returns true if an UltraGridOverride object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasOverride
		{
			get
			{
				return null != this.overrideObj;
			}
		}

		#region OverrideInternal

		// SSP 5/5/05 - Optimizations
		// Added OverrideInternal property.
		//
		internal UltraGridOverride OverrideInternal
		{
			get
			{
				return this.overrideObj;
			}
		}

		#endregion // OverrideInternal

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeOverride() 
		{
			return ( this.HasOverride && 
				this.overrideObj.ShouldSerialize() );
		}
 
		/// <summary>
		/// Reset override
		/// </summary>
		public void ResetOverride() 
		{
			if ( this.HasOverride )
				this.overrideObj.Reset();
		}

		/// <summary>
		/// Returns a collection of ValueList objects. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>ValueLists</b> property is used to access the collection of ValueList objects associated with the UltraWinGrid. ValueList objects are used to provide the contents of the dropdown lists that are displayed in a cell when the column containing the cell has its <b>Style</b> property set to one of the dropdown list styles.</p>
		///	<p class="body">Each ValueList object in the collection can be accessed by using its <b>Index</b> or <b>Key</b> values. Using the <b>Key</b> value is preferable, because the order of an object within the collection (and therefore its <b>Index</b> value) may change as objects are added to and removed from the collection.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_ValueLists")]
		[ LocalizedCategory("LC_Data") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Content) ]
		public Infragistics.Win.ValueListsCollection ValueLists
		{
			get
			{
				if ( null == this.valuelists )
				{
					this.valuelists = new Infragistics.Win.ValueListsCollection( this.Appearances );
            
					// hook up the prop change notifications
					//
					this.valuelists.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}
				return this.valuelists;
			}

		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		protected bool ShouldSerializeValueLists() 
		{
			return ( this.HasValueLists && 
				this.valuelists.Count > 0  );
		}
 
		/// <summary>
		/// Resets valuelists
		/// </summary>
		public void ResetValueLists() 
		{
			if ( this.valuelists != null )
			{
				this.valuelists.Clear();
			}
		}

		/// <summary>
		/// Returns true if an valuelist object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasValueLists
		{
			get
			{
				return null != this.valuelists;
			}
		}

		/// <summary>
		/// Returns a reference to the GroupByBox object associated wiht the layout. The GroupByBox is used to specify GroupBy rows when the control is in GroupBy mode.
		/// <see cref="Infragistics.Win.UltraWinGrid.GroupByBox"/> 
		/// </summary>
		/// <remarks>
		/// <p class="body">The GroupBy Box is an interface element that gives you a way to dynamically group row data for display. When the GroupBy Box is visible, you can drag column headers into it using the mouse. The data will then be grouped according to the field you have specified. The grid creates a series of GroupBy rows whihc serve to aggregate data that has a common field value. You can then expand or collapse the GroupBy row to display or hide the data for a particular value.</p>
		/// <p class="body">For example, suppose you have a band in the grid that displays address information, including a field for City. If you drag the header of the City column into the GroupBy box, the grid will display a GroupBy row for every city that occurs in the rows of the band. You can expand the GroupBy row for a particular city to see all the data that relates to that city (i.e. all the data that has the name of the chosen city specified in its City field.)</p>
		/// <p class="body">The GroupBy Box object supports properties that give you control over the look of the GroupBy Box interface element (including an Appearance property.)</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_GroupByBox")]
		[ LocalizedCategory("LC_Display") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Content) ]
		public Infragistics.Win.UltraWinGrid.GroupByBox GroupByBox
		{
			get
			{
				if ( null == this.groupByBox )
				{
					this.groupByBox = new Infragistics.Win.UltraWinGrid.GroupByBox( this );
            
					// hook up the prop change notifications
					//
					this.groupByBox.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.groupByBox;
			}
		}


		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeGroupByBox() 
		{			
			return ( this.HasGroupByBox && 
				this.GroupByBox.ShouldSerialize() );
		}
 
		/// <summary>
		/// Reset addnewbox
		/// </summary>
		public void ResetGroupByBox() 
		{
			if ( this.HasGroupByBox )
				this.GroupByBox.Reset();
		}

		internal bool HasGroupByBox
		{
			get
			{
				return null != this.groupByBox;
			}
		}

		/// <summary>
		/// The AddNewBox object represents the AddNew Box interface for entering new data rows into the grid.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a grid is being used to display a flat recordset, the conventional approach for adding data has been to place an empty row at the bottom of the grid. New data is entered into this row and appended to the data source, then the row reserved for new data entry is cleared and moved down to appear below the newly added row. However, when working with a hierarchical recordset, this metaphor is no longer effective. Multiple bands of data are represented as distinct groups of rows, and which group of rows receives the new data is significant. Simply adding new data to the last row in a band will not position the new record correctly with respect to the band's parent recordset.</p>
		/// <p class="body">To effectively add new data to a hierarchical recordset, the UltraGrid implements an interface called the "AddNew Box." The AddNew Box displays one or more buttons that are used to trigger the addition of new data. The number of buttons corresponds to the number of hierarchical bands displayed. Each band has its own AddNew button, and connecting lines link the buttons, illustrating a hierarchical relationship that mirrors that of the data.</p>
		/// <p class="body">To use the AddNew Box, you first set focus to a row or cell in the band to which you want to add data. You should determine where in the hierarchy you want the record to appear, then select a record that corresponds to that location. You then click the AddNew button for the band you want to contain the new data, and an empty data entry row appears in the band a the point you selected. For example, if you have a Customers/Orders hierarchy and you wanted to add data for a new order, you would first locate the customer to whom the order belonged, select that customer's record (or one of that customer's existing order records) and click the AddNew button for the Orders band. A blank row would appear below any existing orders that were displayed for the customer.</p>
		/// <p class="body">The AddNewBox object contains properties that control the various attributes of the AddNew Box interface. For example, you can use the <b>Hidden</b> property of the AddNewBox object to selectively display or hide the interface, thus enabling or disabling the user's ability to add new data. You can also use this object to control the appearance of the AddNew buttons, and specify other formatting features. You can also chose between the standard and compact display styles for the AddNew Box. The compact style preserves screen space by compressing the display of the AddNew buttons and eliminating the display of a hierarchical structure.</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_AddNewBox")]
		[ LocalizedCategory("LC_Display") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Content) ]
		public Infragistics.Win.UltraWinGrid.AddNewBox AddNewBox
		{
			get
			{
				if ( null == this.addNewBox )
				{
					this.addNewBox = new Infragistics.Win.UltraWinGrid.AddNewBox( this );
            
					// hook up the prop change notifications
					//
					this.addNewBox.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}
				return this.addNewBox;
			}

		}

		/// <summary>
		/// Returns true if an AddNewBox object has been created.
		/// </summary>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public bool HasAddNewBox
		{
			get
			{
				return null != this.addNewBox;
			}
		}
		
		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAddNewBox() 
		{			
			return ( this.HasAddNewBox && 
				this.addNewBox.ShouldSerialize() );
		}
 
		/// <summary>
		/// Resets the AddNew box
		/// </summary>
		public void ResetAddNewBox() 
		{
			if ( this.HasAddNewBox )
				this.addNewBox.Reset();
		}

		
		/// <summary>
		/// Returns the associated UltraGridBase instance or null if not set (read-only).
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>Grid</b> property returns the UltraGridBase instance this UltraGridLayout object is associated with. UltraGridLayout object keeps a reference back to the UltraGridBase instance it belongs to. For example, given ultraGrid1 of type UltraGrid, ultraGrid1 == ultraGrid1.DisplayLayout.Grid will always evaluate to true. The same applies to UltraCombo and UltraDropDown as well since they all derive from UltraGridBase class.</p>
		/// </remarks>
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		public UltraGridBase Grid
		{
			get
			{
				return this.grid;
			}
		}

		internal UIElementBorderStyle BorderStyleResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleLayout, out val ) )
				{
					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleLayout,
						StyleUtils.GetControlAreaRole( this ), this.BorderStyle, 
						this.IsPrintLayout ? UIElementBorderStyle.Solid : UIElementBorderStyle.InsetSoft );
				}

				return (UIElementBorderStyle)val;
                
				
			}
		}

		/// <summary>
		/// Specifies the style that will be used for the border of the Grid control.
		/// <see cref="Infragistics.Win.UIElementBorderStyle"/>
		/// </summary>
		[ LocalizedDescription("LD_UltraGridLayout_P_BorderStyle")]
		[ LocalizedCategory("LC_Display") ]
		public Infragistics.Win.UIElementBorderStyle BorderStyle
		{
			get
			{
				return this.borderStyle;
			}

			set
			{
				if ( value != this.borderStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UIElementBorderStyle), value ) )
                        // MRS NAS v8.2 - Unit Testing
						//throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_163") );
                        throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_ArgumentException_163"));

					this.borderStyle = value;
					this.NotifyPropChange( PropertyIds.BorderStyle, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeBorderStyle() 
		{
			return ( this.borderStyle != Infragistics.Win.UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets BorderStyle to its default value.
		/// </summary>
		public void ResetBorderStyle() 
		{
			this.BorderStyle = Infragistics.Win.UIElementBorderStyle.Default;
		}

		/// <summary>
		/// Returns or sets the vertical space between bands.
		/// </summary>
		///	<remarks>
		///	<p class="body">This is the vertical space between the last child row of parent record A, and the first child row of Parent Record B, where Parent record B is the next sibling record after parent record A.</p>
		///	<p class="body">The <b>InterbandSpacing</b> property determines the spacing between bands in a hierarchical record set. Specifically, it determines the vertical space between the last row of one band and the first row of that band's child band. The higher the value, the greater the space between bands and their children.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_InterBandSpacing")]
		[ LocalizedCategory("LC_Display") ]
		public int InterBandSpacing
		{
			get
			{
				return this.interBandSpacing;
			}

			set
			{
				if ( value != this.interBandSpacing )
				{
					// RobA UWG565 11/15/01
					//
					if ( value < 0 )
						throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_163"),Shared.SR.GetString("LE_ArgumentOutOfRangeException_332") );

					this.interBandSpacing = value;
					this.NotifyPropChange( PropertyIds.InterBandSpacing, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeInterBandSpacing() 
		{
			return ( this.interBandSpacing != 5 );
		}
 
		
		// SSP 10/9/01 
		// Function name should be ResetInterBandSpacing and not ResetBorderInterBandSpacing
		// to be consistent with the associated property and ShouldSerialize
		// function names
		//
		/// <summary>
		/// Resets border interband spacing to its default value (5).
		/// </summary>
		public void ResetInterBandSpacing() 
		{
			this.InterBandSpacing = 5;
		}

		/// <summary>
		/// Returns or sets the maximum number of column scrolling regions.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>MaxColScrollRegions</b> property can be used to limit the number of column scrolling regions that may be present at one time in the UltraWinGrid. When the maximum number of regions has been created, no more may be added either through code or by using the user interface of the grid.</p>
		///	<p class="body">The default setting of this property is 10. The minimum setting for this property is 1.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_MaxColScrollRegions")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MaxColScrollRegions
		{
			get
			{
				// JJD 12/20/01
				// If autoFitColumns is true then don't allow more than 1 
				// column scrolling region
				//
				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// Obsoleted AutoFitColumns and added AutoFitStyle property.
				//
				//if ( this.autoFitColumns )
				if ( AutoFitStyle.ResizeAllColumns == this.AutoFitStyle )
					return 1;

				return this.maxColScrollRegions;
			}

			set
			{
				//RobA UWG566 10/17/01
				//
				if ( value <= 0 )
					throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_165"), Shared.SR.GetString("LE_ArgumentOutOfRangeException_333"));

				if ( value != this.maxColScrollRegions )
				{
					this.maxColScrollRegions = value;
					this.NotifyPropChange( PropertyIds.MaxColScrollRegions, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMaxColScrollRegions() 
		{
			return ( this.maxColScrollRegions != 10 );
		}
 
		/// <summary>
		/// Resets MaxColScrollRegions to its default value (10).
		/// </summary>
		public void ResetMaxColScrollRegions() 
		{
			this.MaxColScrollRegions = 10;
		}

		/// <summary>
		/// Returns or sets the maximum number of row scrolling regions.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>MaxRowScrollRegions</b> property can be used to limit the number of row scrolling regions that may be present at one time in the UltraGrid. When the maximum number of regions has been created, no more may be added either through code or by using the user interface of the grid.</p>
		///	<p class="body">The default setting of this property is 10. The minimum setting for this property is 1.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_MaxRowScrollRegions")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MaxRowScrollRegions
		{
			get
			{
				return this.maxRowScrollRegions;
			}

			set
			{
				//RobA UWG566 10/17/01
				//
				if ( value <= 0 )
					throw new ArgumentOutOfRangeException( Shared.SR.GetString("LE_ArgumentOutOfRangeException_167"), Shared.SR.GetString("LE_ArgumentOutOfRangeException_333"));

				if ( value != this.maxRowScrollRegions )
				{
					this.maxRowScrollRegions = value;
					this.NotifyPropChange( PropertyIds.MaxRowScrollRegions, null );
				}
			}
		}

		

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMaxRowScrollRegions() 
		{
			return ( this.maxRowScrollRegions != 10 );
		}
 
		/// <summary>
		/// Resets ResetMaxRowScrollRegions to its default value (10).
		/// </summary>
		public void ResetMaxRowScrollRegions() 
		{
			this.MaxRowScrollRegions = 10;
		}

		#region RowConnectorColorResolved

		// SSP 3/16/06 - App Styling
		// Added RowConnectorColorResolved.
		// 
        /// <summary>
        /// Returns the resolved color of the RowConnector lines. 
        /// </summary>        
        // MRS v7.2 - Document Exporter
		//internal System.Drawing.Color RowConnectorColorResolved
        [EditorBrowsable( EditorBrowsableState.Advanced)]
        // MBS 7/27/07 - BR25195
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
        public System.Drawing.Color RowConnectorColorResolved
		{
			get
			{
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.RowConnectorColor, out val ) )
				{
					val = StyleUtils.CachePropertyValue( this, StyleUtils.CustomProperty.RowConnectorColor,
						this.rowConnectorColor, Color.Empty, SystemColors.ControlDark );
				}

				return (Color)val;
			}
		}

		#endregion // RowConnectorColorResolved

		/// <summary>
		/// Returns or sets the color of the lines used to connect rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>RowConnectorColor</b> property determines the color of the lines used to connect rows and bands when hierarchical data is being displayed by the grid. In addition to specifying the color of these lines, you can also set their style using the <b>RowConnectorStyle</b> property.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_RowConnectorColor")]
		[ LocalizedCategory("LC_Appearance") ]
		public System.Drawing.Color RowConnectorColor
		{
			get
			{
				return this.rowConnectorColor;
			}

			set
			{
				if ( value != this.rowConnectorColor )
				{
					this.rowConnectorColor = value;
					this.NotifyPropChange( PropertyIds.RowConnectorColor, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowConnectorColor() 
		{
			// SSP 3/16/06 - App Styling
			// Changed the default from ControlDark to Empty.
			// 
			//return ( this.rowConnectorColor != System.Drawing.SystemColors.ControlDark );
			return this.rowConnectorColor != System.Drawing.Color.Empty;
		}
 
		/// <summary>
		/// Resets RowConnectorColor to its default value.
		/// </summary>
		public void ResetRowConnectorColor() 
		{
			// SSP 3/16/06 - App Styling
			// Changed the default from ControlDark to Empty.
			// 
			//this.RowConnectorColor = System.Drawing.SystemColors.ControlDark;
			this.RowConnectorColor = System.Drawing.Color.Empty;
		}

		#region RowConnectorStyleResolved

		// SSP 3/16/06 - App Styling
		// Added RowConnectorStyleResolved.
		// 
        /// <summary>
        /// Returns the resolved Row Connector Style.
        /// </summary>        
        // MRS v7.2 - Document Exporter
        //internal RowConnectorStyle RowConnectorStyleResolved
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        // MBS 7/27/07 - BR25195
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
        public RowConnectorStyle RowConnectorStyleResolved
		{
			get
			{
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.RowConnectorStyle, out val ) )
				{
					val = StyleUtils.CachePropertyValue( this, StyleUtils.CustomProperty.RowConnectorStyle,
						this.rowConnectorStyle, RowConnectorStyle.Default, RowConnectorStyle.Dotted );
				}

				return (RowConnectorStyle)val;
			}
		}

		#endregion // RowConnectorStyleResolved

		/// <summary>
		/// Returns or sets the style of the lines used to connect rows.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>RowConnectorStyle</b> property is used to determine the type of line that will be used to connect child bands to their parents. You can choose from several line styles. Note that not all styles are available on all operating systems. If the version of the OS that is running your program does not support a particular line style, row connector lines formatted with that style will be drawn using solid lines.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_RowConnectorStyle")]
		[ LocalizedCategory("LC_Appearance") ]
		public RowConnectorStyle RowConnectorStyle
		{
			get
			{
				return this.rowConnectorStyle;
			}

			set
			{
				if ( value != this.rowConnectorStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(RowConnectorStyle), value ) )
                        // MRS NAS v8.2 - Unit Testing
						//throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_168") );
                        throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_ArgumentException_168"));

					this.rowConnectorStyle = value;
					this.NotifyPropChange( PropertyIds.RowConnectorStyle, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeRowConnectorStyle() 
		{
			return ( this.rowConnectorStyle != RowConnectorStyle.Default );
		}
 
		/// <summary>
		/// Resets RowConnectorStyle to its default value.
		/// </summary>
		public void ResetRowConnectorStyle() 
		{
			this.RowConnectorStyle = RowConnectorStyle.Default;
		}

		/// <summary>
		/// Specifies whether the AutoFitColumns feature is enabled. This feature automatically resizes the column widths proportionally to fit the visible area of the grid. <b>Note:</b> This property has been obsoleted. Use <see cref="UltraGridLayout.AutoFitStyle"/> property instead.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>Note:</b> This property has been obsoleted. Use <see cref="UltraGridLayout.AutoFitStyle"/> property instead.</p>
		/// <p class="body">AutoFitColumns provides automatic resizing of the columns in a band to fit within the visible area of a grid. When set to True, the columns of the band will automatically resize themselves proportionallly so that all columns for the band are visible. If you have set up the grid so that it can be resized (for example, by automatically adjusting its size based on that of its container) the columns will resize themselves dynamically as the size of the grid changes.</p>
		/// <p class="body">Note that while you can apply this setting at run-time, resetting AutoFitColumns to False will not restore the columns to their previous widths. The internally stored column width is altered by setting this property; if you wish to restore column widths after changing the setting of AutoFitColumns you must write code to store and re-apply the widths.</p>
		/// <p class="body">For UltraDropDown and UltraCombo this feature auto-fits the columns to <see cref="UltraDropDownBase.DropDownWidth"/> setting. If the <b>DropDownWidth</b> is set to -1 or 0 then the columns are resized to fit the width of the UltraDropDown or the UltraCombo control.</p>
        /// <p class="body">Note that this is different from having the column resize itself to ensure that all the cell contents are fully visible. For that use <see cref="UltraGridColumn.PerformAutoResize( Infragistics.Win.UltraWinGrid.PerformAutoSizeType )"/>.</p>
        /// <seealso cref="UltraGridColumn.PerformAutoResize( Infragistics.Win.UltraWinGrid.PerformAutoSizeType )"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_AutoFitColumns")]
		[ LocalizedCategory("LC_Behavior") ]
		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		// Obsoleted AutoFitColumns and added AutoFitStyle property.
		//
		[ Obsolete( "Use AutoFitStyle property instead", false ),
		  EditorBrowsable( EditorBrowsableState.Never ), Browsable( false ), 
		  DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public bool AutoFitColumns
		{
			get
			{
				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// Obsoleted AutoFitColumns and added AutoFitStyle property. Note: Before you
				// change the following line make sure that all the references to AutoFitColumns
				// proeprty get the correct behavior.
				//
				//return this.autoFitColumns;
				return AutoFitStyle.ResizeAllColumns == this.AutoFitStyle;
			}

			set
			{
				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// Obsoleted AutoFitColumns and added AutoFitStyle property.
				//
				this.AutoFitStyle = value ? AutoFitStyle.ResizeAllColumns  : AutoFitStyle.None;
				
			}
		}

		// SSP 6/18/04 UWG3253
		// Added ResetAutoFit_ColumnLastAdjustmentFullyAppliedToIndex method.
		// To fix an issue where repeatedly toggling AutoFitColumns property was causing
		// the auto-fit columns logic to be executed multiple times and every time the
		// logic was executed, the auto-fit slack as a result of rounding was being applied
		// to a different column based on the columnLastAdjustmentFullyAppliedToIndex 
		// counter variable causing a wave like effect (see UWG3253 sample). To fix this 
		// issue, whenever the value of AutoFitColumns is changed, we will reset this
		// columnLastAdjustmentFullyAppliedToIndex variable on all bands.
		//
		internal void ResetAutoFit_ColumnLastAdjustmentFullyAppliedToIndex( )
		{
			if ( null != this.sortedBands )
			{
				for ( int i = 0; i < this.sortedBands.Count; i++ )
				{
					UltraGridBand band = this.sortedBands[i];

					if ( null != band )
						band.ResetAutoFit_ColumnLastAdjustmentFullyAppliedToIndex( );
				}
			}
		}

		#region ShouldSerializeAutoFitColumns

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeAutoFitColumns() 
		{
			// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
			// Obsoleted AutoFitColumns and added AutoFitStyle property.
			//
			//return ( this.autoFitColumns != false );
			return false;
		}

		#endregion // ShouldSerializeAutoFitColumns

		#region ResetAutoFitColumns

		/// <summary>
		/// Resets AutoFitColumns to its default value (false).
		/// </summary>
		public void ResetAutoFitColumns() 
		{
			// SSP 10/14/02 UWG1755
			// Use the property rather than the variable.
			//
			//this.autoFitColumns = false;
			this.ResetAutoFitStyle( );
		}

		#endregion // ResetAutoFitColumns

		#region AutoFitStyle

		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		// Obsoleted AutoFitColumns and added AutoFitStyle property.
		//
		/// <summary>
		/// Specifies whether the AutoFitColumns feature is enabled. This feature automatically resizes the column widths proportionally to fit the visible area of the grid.
		/// </summary>
		/// <remarks>
		/// <p class="body">When <b>AutoFitStyle</b> is set to <b>ResizeAllColumns</b>, UltraGrid automatically resizes columns in a band to fit within the visible area of the grid.</p>
		/// <p class="body">When <b>AutoFitStyle</b> is set to <b>ExtendLastColumn</b>, UltraGrid automatically resizes the last column to fit within the visible area of the grid.</p>
		/// <p class="body">In Row-Layout mode if you enable this functionality and then disable it the widths of the columns will be restored to their previous settings. In non non-row-layout mode however this is not the case. In non-row-layout mode if you set the property to <b>ResizeAllColumns</b> and reset it back to <b>None</b> the UltraGrid will not restore the columns to their previous widths. The internally stored column width is altered by setting this property; if you wish to restore column widths after changing the setting of <b>AutoFitStyle</b> then you must write code to store and re-apply the widths.</p>
		/// <p class="body">For UltraDropDown and UltraCombo this feature auto-fits the columns to <see cref="UltraDropDownBase.DropDownWidth"/> setting. If the <b>DropDownWidth</b> is set to -1 or 0 then the columns are resized to fit the width of the UltraDropDown or the UltraCombo control.</p>
        /// <p class="body">Note that this is different from having the column resize itself to ensure that all the cell contents are fully visible. For that feature use the <see cref="UltraGridColumn.PerformAutoResize( Infragistics.Win.UltraWinGrid.PerformAutoSizeType )"/> method.</p>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.AutoFitStyle"/> <seealso cref="UltraGridColumn.PerformAutoResize( Infragistics.Win.UltraWinGrid.PerformAutoSizeType )"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_AutoFitStyle") ]
		[ LocalizedCategory("LC_Behavior") ]
		public AutoFitStyle AutoFitStyle
		{
			get
			{
				return this.autoFitStyle;
			}
			set
			{
				if ( value != this.autoFitStyle )
				{
					GridUtils.ValidateEnum( typeof( AutoFitStyle ), value );
					this.autoFitStyle = value;

					this.NotifyPropChange( PropertyIds.AutoFitStyle );
					this.NotifyPropChange( PropertyIds.AutoFitColumns );
				}
			}
		}

		#endregion // AutoFitStyle

		#region AutoFitAllColumns
		
		internal bool AutoFitAllColumns
		{
			get
			{
				return AutoFitStyle.ResizeAllColumns == this.AutoFitStyle;
			}
		}

		#endregion // AutoFitAllColumns

		#region AutoFitExtendLastColumn

		internal bool AutoFitExtendLastColumn
		{
			get
			{
				// Only allow ExtendLastColumn when there is a single column scroll region.
				//
				return AutoFitStyle.ExtendLastColumn == this.AutoFitStyle 
					&& this.ColScrollRegionsAllocated && 1 == this.ColScrollRegions.Count;
			}
		}

		#endregion // AutoFitExtendLastColumn

		#region ShouldSerializeAutoFitStyle

		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		// Obsoleted AutoFitColumns and added AutoFitStyle property.
		//
		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeAutoFitStyle( ) 
		{
			return AutoFitStyle.None != this.AutoFitStyle;
		}

		#endregion // ShouldSerializeAutoFitStyle

		#region ResetAutoFitStyle

		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		// Obsoleted AutoFitColumns and added AutoFitStyle property.
		//
		/// <summary>
		/// Resets ResetAutoFitStyle to its default value of <b>None</b>.
		/// </summary>
		public void ResetAutoFitStyle( ) 
		{
			this.AutoFitStyle = AutoFitStyle.None;
		}

		#endregion // ResetAutoFitStyle

		#region RowConnectorBorderStyle
		
		internal UIElementBorderStyle RowConnectorBorderStyle
		{
			get
			{
				// SSP 3/16/06 - App Styling
				// Use the new RowConnectorStyleResolved.
				// 
				//UIElementBorderStyle eBorderStyle = (UIElementBorderStyle)this.rowConnectorStyle;
				UIElementBorderStyle eBorderStyle = (UIElementBorderStyle)this.RowConnectorStyleResolved;

				// map inset and raised to the soft versions
				//
				switch ( eBorderStyle )
				{
					case UIElementBorderStyle.Inset:
						eBorderStyle = UIElementBorderStyle.InsetSoft;
						break;

					case UIElementBorderStyle.Raised:
						eBorderStyle = UIElementBorderStyle.RaisedSoft;
						break;

					case UIElementBorderStyle.Default:
						eBorderStyle = UIElementBorderStyle.Dotted;
						break;
				}

				return eBorderStyle;
			}
		}

		#endregion // RowConnectorBorderStyle

		#region TabNavigation

		/// <summary>
		/// Returns or sets a value that indicates how the control will respond when the TAB key is pressed.
		/// </summary>
		///	<remarks>
		///	<p class="body">When this property is set to 0 (TabNavigationNextCell) and a cell has focus, pressing TAB will give focus to the cell to the right, or the first cell in the row below the active row if the active cell is the rightmost cell in the row. If a row has focus, pressing TAB will give focus to the row below the active row, unless the active row is the last row in the control, in which case the next control in the form's tab order will receive focus.</p>
		///	<p class="body">When this property is set to 1 (TabNavigationNextControl) the control passes focus from itself to the next control in the tab order when the TAB key is pressed.</p>
		///	<p class="body">The 2 (TabNavigationNextControlOnLastCell) combines these two kinds of functionality. The TAB key will shift focus to the next control on the form only when the last cell in the grid has focus, otherwise it will move between cells. (Similarly, when the first cell in the grid has focus, pressing SHIFT+TAB will shift focus to the previous control on the form.)</p>
		///	<p class="body">Use the <b>TabStop</b> property of a cell or column to determine whether an individual cell or the cells in a column should receive focus when the user presses the TAB key.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_TabNavigation")]
		[ LocalizedCategory("LC_Behavior") ]
		public TabNavigation TabNavigation
		{
			get
			{
				return this.tabNavigation;
			}

			set
			{
				if ( value != this.tabNavigation )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(TabNavigation), value ) )
                        // MRS NAS v8.2 - Unit Testing
						//throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_169"));
                        throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_ArgumentException_169"));

					this.tabNavigation = value;
					this.NotifyPropChange( PropertyIds.TabNavigation, null );
				}
			}
		}

		#endregion // TabNavigation

		#region ShouldSerializeTabNavigation

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeTabNavigation( ) 
		{
			return ( this.tabNavigation != TabNavigation.NextCell );			
		}

		#endregion // ShouldSerializeTabNavigation
 
		#region ResetTabNavigation

		/// <summary>
		/// Resets TabNavigation to its default value (next cell).
		/// </summary>
		public void ResetTabNavigation( ) 
		{
			this.TabNavigation = TabNavigation.NextCell;
		}

		#endregion // ResetTabNavigation

		#region BorderStyleCaption

		/// <summary>
		/// Returns or sets the border style of the control's caption area.
		/// <see cref="Infragistics.Win.UIElementBorderStyle"/>
		/// </summary>
		/// <remarks>
		/// The caption area is displayed when caption text is specified for the grid. If the grid's <b>Text</b> property is set to an empty string, the caption will not be displayed.
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_BorderStyleCaption")]
		[ LocalizedCategory("LC_Display") ]
		public Infragistics.Win.UIElementBorderStyle BorderStyleCaption
		{
			get
			{
				return this.borderStyleCaption;
			}

			set
			{
				if ( value != this.borderStyleCaption )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UIElementBorderStyle), value ) )
                        // MRS NAS v8.2 - Unit Testing
						//throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_170") );
                        throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_ArgumentException_170"));

					this.borderStyleCaption = value;
					this.NotifyPropChange( PropertyIds.BorderStyleCaption, null );
				}
			}
		}

		#endregion // BorderStyleCaption

		#region BorderStyleCaptionResolved

		// SSP 11/6/01 UWG666
		//
		internal Infragistics.Win.UIElementBorderStyle BorderStyleCaptionResolved
		{
			get
			{
				// SSP 3/21/06 - App Styling
				// Get the border style from the role.
				// 
				
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this, StyleUtils.CachedProperty.BorderStyleGridCaption, out val ) )
				{
					// To maintain the previous behavior, default the grid caption's border style to the 
					// BorderStyleHeader property of the override. If that's not set then get the header 
					// role's border style.
					// 
					UIElementBorderStyle resolveValue = null != this.overrideObj ? this.overrideObj.BorderStyleHeader : UIElementBorderStyle.Default;
					if ( UIElementBorderStyle.Default == resolveValue )
					{
						resolveValue = StyleUtils.GetRoleBorderStyle( this, StyleUtils.Role.Header );

						if ( UIElementBorderStyle.Default == resolveValue )
							resolveValue = UIElementBorderStyle.RaisedSoft;
					}

					val = StyleUtils.CacheBorderStylePropertyValue( this, StyleUtils.CachedProperty.BorderStyleGridCaption,
						StyleUtils.Role.GridCaption, this.BorderStyleCaption, resolveValue );
				}

				return (UIElementBorderStyle)val;
				
				
			}
		}

		#endregion // BorderStyleCaptionResolved

		#region ShouldSerializeBorderStyleCaption

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeBorderStyleCaption() 
		{
			return ( this.borderStyleCaption != Infragistics.Win.UIElementBorderStyle.Default );
		}

		#endregion // ShouldSerializeBorderStyleCaption
 
		#region ResetBorderStyleCaption

		/// <summary>
		/// Resets BorderStyleCaption to its default value.
		/// </summary>
		public void ResetBorderStyleCaption() 
		{
			this.BorderStyleCaption = Infragistics.Win.UIElementBorderStyle.Default;
		}

		#endregion // ResetBorderStyleCaption
		
		#region ScrollbarsResolved

		internal Infragistics.Win.UltraWinGrid.Scrollbars ScrollbarsResolved
		{
			get
			{
				// JJD 1/23/02 - UWG497
				// For dropdowns at runtime who have the automatic setting we
				// need to check the IncludeHorizontalScrollbar and 
				// IncludeVerticalScrollbar flags that were set when the dropdown
				// was dropped down.
				//
				if ( this.grid != null && 
					!this.grid.DesignMode &&
					this.scrollbars == Infragistics.Win.UltraWinGrid.Scrollbars.Automatic )
				{
					UltraDropDownBase dropDown = this.grid as UltraDropDownBase;

					if ( dropDown != null )
					{
						if ( dropDown.IncludeHorizontalScrollbar )
						{
							if ( dropDown.IncludeVerticalScrollbar )
								return Infragistics.Win.UltraWinGrid.Scrollbars.Both;

							return Infragistics.Win.UltraWinGrid.Scrollbars.Horizontal;
						}
						else 
						{
							if ( dropDown.IncludeVerticalScrollbar )
								return Infragistics.Win.UltraWinGrid.Scrollbars.Vertical;
						}

						return Infragistics.Win.UltraWinGrid.Scrollbars.None;
					}
				}

				return this.scrollbars;
			}
		}

		#endregion // ScrollbarsResolved

		#region ScrollStyle

		// SSP 2/28/02
		// Added ScrollStyle for vertical scrolling to allow for immediate
		// scrolling of rows when thumb is tracked.
		//
		/// <summary>
		/// Controls how vertical row scrolling is done in the grid.
		/// </summary>
		/// <remarks>
		/// <seealso cref="ScrollStyle"/> <seealso cref="UseScrollWindow"/> <seealso cref="Scrollbars"/> <seealso cref="ScrollBarLook"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_ScrollStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.ScrollStyle ScrollStyle
		{
			get
			{
				return this.scrollStyle;
			}
			set
			{
                // MRS NAS v8.2 - Unit Testing
                if (this.scrollStyle == value)
                    return;

                // MRS NAS v8.2 - Unit Testing
                if (!Enum.IsDefined(typeof(ScrollStyle), value))
                    throw new InvalidEnumArgumentException("ScrollStyle", (int)value, typeof(ScrollStyle));

				this.scrollStyle = value;

				// Notify that the property has changed.
				//
				this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ScrollStyle );
			}
		}

		#endregion // ScrollStyle

		#region ShouldSerializeScrollStyle

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value.</returns>
		protected bool ShouldSerializeScrollStyle( ) 
		{
			return Infragistics.Win.UltraWinGrid.ScrollStyle.Deferred != this.scrollStyle;
		}

		#endregion // ShouldSerializeScrollStyle
 
		#region ResetScrollStyle

		/// <summary>
		/// Resets ScrollStyle to its default value (Deferred).
		/// </summary>
		public void ResetScrollStyle( ) 
		{
			this.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Deferred;
		}

		#endregion // ResetScrollStyle

		// SSP 11/24/03 - Scrolling Till Last Row Visible
		// Added the functionality to not allow the user to scroll any further once the
		// last row is visible.
		//
		#region ScrollBounds

		/// <summary>
		/// Specifies the vertical scrolling behavior in regard to how the UltraGrid scrolls rows. Default is <b>ScrollToLastItem</b>.
		/// </summary>
		/// <remarks>
        /// <p class="body">Setting <b>ScrollBounds</b> to <b>ScrollToFill</b> will disallow the user from scrolling further down once the last row becomes visible. While setting it to <b>ScrollToLastItem</b> will allow the user to continue scrolling until last row is the only visible row in the row scroll region.</p>
        /// <seealso cref="ScrollStyle"/> <seealso cref="UseScrollWindow"/> <seealso cref="Scrollbars"/> <seealso cref="ScrollBarLook"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridLayout_P_ScrollBounds")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.ScrollBounds ScrollBounds
		{
			get
			{
				return this.scrollBounds;
			}
			set
			{
				if ( this.scrollBounds != value )
				{
					if ( ! Enum.IsDefined( typeof( ScrollBounds ), value ) )
						throw new InvalidEnumArgumentException( "ScrollBounds", (int)value, typeof( ScrollBounds ) );

					this.scrollBounds = value;

					this.BumpGrandVerifyVersion( );
					if ( ! this.Disposed )
						this.DirtyGridElement( true );

					this.NotifyPropChange( PropertyIds.ScrollBounds );
				}
			}
		}

		#endregion // ScrollBounds

		#region ScrollBoundsResolved

		// SSP 5/12/05 - NAS 5.2 Fixed Rows
		// Added ScrollBoundsResolved. If there are fixed rows at the bottom then
		// always resolve the scroll bounds to ScrollToFill. Added ScrollBoundsResolved
		// property.
		//
		internal Infragistics.Win.UltraWinGrid.ScrollBounds ScrollBoundsResolved
		{
			get
			{
				ScrollBounds ret = this.ScrollBounds;

                // MRS 7/27/07 - BR25116
                // We never want to scroll to fill in a print layout
                if (ret == ScrollBounds.ScrollToFill &&
                    this.IsPrintLayout)
                {
                    return ScrollBounds.ScrollToLastItem;
                }

				if ( ScrollBounds.ScrollToFill != ret
					&& null != this.rows && this.rows.HasFixedRows( false ) )
					ret = ScrollBounds.ScrollToFill;

				return ret;
			}
		}

		#endregion // ScrollBoundsResolved

		#region ShouldSerializeScrollBounds

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
		/// <returns></returns>
		protected bool ShouldSerializeScrollBounds( )
		{
			return Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToLastItem != this.scrollBounds;
		}

		#endregion // ShouldSerializeScrollBounds

		#region ResetScrollBounds

		/// <summary>
		/// Resets the <see cref="ScrollBounds"/> property to its default value of <b>ScrollToLastItem</b>.
		/// </summary>
		public void ResetScrollBounds( )
		{
			this.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToLastItem;
		}

		#endregion // ResetScrollBounds

		#region EnsureScrollRegionsFilled

		// SSP 11/24/03 - Scrolling Till Last Row Visible
		// Added the functionality to not allow the user to scroll any further once the
		// last row is visible.
		//
		internal void EnsureScrollRegionsFilled( )
		{
			if ( null != this.rowScrollRegions )
			{
				for ( int i = 0; i < this.rowScrollRegions.Count; i++ )
					this.rowScrollRegions[i].EnsureScrollRegionFilled( );
			}
		}

		#endregion // EnsureScrollRegionsFilled

		// SSP 1/5/04 UWG1606
		// Added appearances for the splitter bars.
		//
		#region Splitter Bar Appearance related

		#region SplitterBarHorizontalAppearance

		/// <summary>
		/// Appearance for the horizontal splitter bar and the split box.
		/// </summary>
		/// <remarks>
		/// <p class="body">This is applied to both the splitbox for splitting the row scroll region as well as the splitter bar that gets shown between the split row scroll regions.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridLayout_P_SplitterBarHorizontalAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SplitterBarHorizontalAppearance
		{
			get
			{
				return this.GetAppearance( LayoutAppearanceIndex.SplitterBarHorizontalAppearance );
			}
			set
			{
				this.SetAppearance( LayoutAppearanceIndex.SplitterBarHorizontalAppearance, value );
			}
		}

		#endregion // SplitterBarHorizontalAppearance

		#region ShouldSerializeSplitterBarHorizontalAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
		/// <returns></returns>
		protected bool ShouldSerializeSplitterBarHorizontalAppearance( ) 
		{
			return this.ShouldSerializeAppearance( LayoutAppearanceIndex.SplitterBarHorizontalAppearance ); 
		}

		#endregion // ShouldSerializeSplitterBarHorizontalAppearance
 
		#region ResetSplitterBarHorizontalAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSplitterBarHorizontalAppearance( ) 
		{
			this.ResetAppearance( LayoutAppearanceIndex.SplitterBarHorizontalAppearance );
		}

		#endregion // ResetSplitterBarHorizontalAppearance

		#region SplitterBarVerticalAppearance

		/// <summary>
		/// Appearance for the vertical splitter bar and the split box.
		/// </summary>
		/// <remarks>
		/// <p class="body">This is applied to both the splitbox for splitting the col scroll region as well as the splitter bar that gets shown between the split col scroll regions.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridLayout_P_SplitterBarVerticalAppearance")]
		[ LocalizedCategory("LC_Appearance") ]
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		public Infragistics.Win.AppearanceBase SplitterBarVerticalAppearance
		{
			get
			{
				return this.GetAppearance( LayoutAppearanceIndex.SplitterBarVerticalAppearance );
			}
			set
			{
				this.SetAppearance( LayoutAppearanceIndex.SplitterBarVerticalAppearance, value );
			}
		}

		#endregion // SplitterBarVerticalAppearance

		#region ShouldSerializeSplitterBarVerticalAppearance

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
		/// <returns></returns>
		protected bool ShouldSerializeSplitterBarVerticalAppearance( ) 
		{
			return this.ShouldSerializeAppearance( LayoutAppearanceIndex.SplitterBarVerticalAppearance ); 
		}

		#endregion // ShouldSerializeSplitterBarVerticalAppearance
 
		#region ResetSplitterBarVerticalAppearance

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSplitterBarVerticalAppearance( ) 
		{
			this.ResetAppearance( LayoutAppearanceIndex.SplitterBarVerticalAppearance );
		}

		#endregion // ResetSplitterBarVerticalAppearance

		#endregion // Splitter Bar Appearance related

		/// <summary>
		/// Returns or sets a value that determines whether horizontal and/or vertical scrollbars are shown.
		/// </summary>
		/// 
		[ LocalizedDescription("LD_UltraGridLayout_P_Scrollbars")]
		[ LocalizedCategory("LC_Behavior") ]
		public Infragistics.Win.UltraWinGrid.Scrollbars Scrollbars
		{
			get
			{
				return this.scrollbars;
			}

			set
			{
				if ( value != this.scrollbars )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Scrollbars), value ) )
                        // MRS NAS v8.2 - Unit Testing
						//throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_171") );
                        throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_ArgumentException_171"));

					this.scrollbars = value;
					this.NotifyPropChange( PropertyIds.Scrollbars, null );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeScrollbars() 
		{
			return ( this.scrollbars != Infragistics.Win.UltraWinGrid.Scrollbars.Automatic );
		}
 
		/// <summary>
		/// Resets Scrollbars to its default value (Automatic).
		/// </summary>
		public void ResetScrollbars() 
		{
			this.Scrollbars = Infragistics.Win.UltraWinGrid.Scrollbars.Automatic;
		}

		/// <summary>
		/// Clears the ViewStyle scheme used to render row data.
		/// </summary>
		// SSP 9/14/04 - Optimizations
		// Added groupByAffected parameter to the method.
		//
		//protected void ClearViewStyle( )
		protected void ClearViewStyle( bool groupByAffected )
		{
			this.viewStyleImpl = null;

			// SSP 1/22/04 - Optimizations
			//
			// --------------------------------------------------------------------
			// SSP 9/14/04
			//
			//if ( null != this.bands )
			if ( groupByAffected && null != this.sortedBands )
			{
				for ( int i = 0; i < this.sortedBands.Count; i++ )
				{
					// SSP 9/14/04 - Optimizations
					// Only bump the version number if the band has group-by sort columns otherwise 
					// the rows collection will go through the InitNonGroupByRows logic.
					//
					// ------------------------------------------------------------------------------
					//this.bands[i].BumpGroupByHierarchyVersion( );
					bool hasGroupByColumns = this.sortedBands[i].HasSortedColumns 
						&& this.sortedBands[i].SortedColumns.Count > 0 
						&& this.sortedBands[i].SortedColumns[0].InternalGetIsGroupByColumn( );

                    if ( hasGroupByColumns )
						this.sortedBands[i].BumpGroupByHierarchyVersion( );
					// ------------------------------------------------------------------------------
				}
			}

			this.BumpGrandVerifyVersion( );

			// SSP 4/5/04 - Virtual Binding Related Optimizations
			//
			this.BumpScrollableRowCountVersion( );
			// --------------------------------------------------------------------

			// SSP 5/17/04 UWG3257
			// Bump the row filters version number since a change of view style from
			// vertical to horizontal can potentially lead to a different resolved
			// row filter mode. In horizontal, row filter mode is always resolved to
			// AllRowsInBand whereas in vertical Default is it's resolved to 
			// SiblingRowsOnly.
			//
			this.BumpRowFiltersVersion( );

			// SSP 3/30/05 - NAS 5.2 Fixed Rows
			//
			this.BumpFixedRowsCollectionsVersion( );

			this.NotifyPropChange(PropertyIds.ViewStyle );
		}

		/// <summary>
		/// Returns or sets a value that determines the type of view displayed by the control.
		/// </summary>
		///	<remarks>
		///	<p class="body">If the grid is bound to a valid data source, the grid checks the <b>ViewStyle</b> property and determines if the data source is supplying a hierarchical recordset or a flat recordset.</p>
		///	<p class="body">If the data source is supplying hierarchical data and the <b>ViewStyle</b> property is set to 1 (ViewStyleMultiBand) the grid displays a hierarchical recordset. Otherwise, the grid displays a flat, single-band recordset.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_ViewStyle")]
		[ LocalizedCategory("LC_Display") ]
		public Infragistics.Win.UltraWinGrid.ViewStyle ViewStyle
		{
			get
			{
				return this.viewStyle;
			}

			set
			{
				if ( value != this.viewStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(ViewStyle), value ) )
                        // MRS NAS v8.2 - Unit Testing
						//throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_172") );
                        throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_ArgumentException_172"));

					// SSP 11/24/03 - Scrolling Till Last Row Visible
					//
					this.BumpGrandVerifyVersion( );

					UltraGrid grid = this.grid as UltraGrid;

					if ( null == grid )
					{
						this.viewStyle = value;

                        // MRS NAS v8.2 - Unit Testing
                        this.NotifyPropChange(PropertyIds.ViewStyle);

						return;
					}

					if ( grid.EventManager.InProgress( GridEventIds.InitializeRow ) )
						throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_173") );

					// SSP 1/27/05 BR02053
					// Only do this for the display layout. Print and export layouts should not be
					// modifying the ActiveRow of the grid.
					//
					//if ( value == ViewStyle.SingleBand )
					if ( value == ViewStyle.SingleBand && this.IsDisplayLayout )
					{
						// get the current active row
						//
						UltraGridRow activeRow = this.ActiveRow;

						// check if the active row has a parent row (not a band 0 row)
						//
						if ( activeRow != null &&
							activeRow.ParentRow != null )
						{
							// get the band 0 (top level parent) row of the active row
							//
							UltraGridRow band_0_Row = ActiveRow.GetBand_0_Row();

							// set this row as the active row
							//
							this.grid.ActiveRow = band_0_Row;

							// if the clear operation was cancelled then
							// just exit
							//
							if ( band_0_Row != this.grid.ActiveRow && 
								null       != this.grid.ActiveRow )
								return;
						}
					}

					this.viewStyle = value;

					// SSP 9/14/04 - Optimizations
					// Pass in the new parameter.
					//
					//this.ClearViewStyle();
					this.ClearViewStyle( false );
				}
			}
		}

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeViewStyle() 
		{
			return ( this.viewStyle != Infragistics.Win.UltraWinGrid.ViewStyle.MultiBand );
		}
 
		/// <summary>
		/// Resets ViewStyle to its default value (multiband).
		/// </summary>
		public void ResetViewStyle() 
		{
			// SSP 5/4/04
			// Set the property instead of the member variable.
			//
			//this.viewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.MultiBand;
			this.ViewStyle = Infragistics.Win.UltraWinGrid.ViewStyle.MultiBand;
		}

		/// <summary>
		/// Returns or sets a value that determines the type of view displayed by the control.
		/// </summary>
		///	<remarks>
		///	<p class="body">The <b>ViewStyleBand</b> property of the UltraWinGrid determines how bands will be arranged within the control. The arrangement of bands also depends on the type of recordset to which the control is bound - a flat (non-hierarchical) recordset will only appear in a single band, regardless of the setting of this property.</p>
		///	<p class="body">If the control is bound to a hierarchical recordset, you have a choice of styles for viewing the bands of hierarchical data. You can choose to view the data in a single band (in which case only the top-most level of the hierarchy will be shown). You can select a horizontal view, where bands are separated into columns that break the data up from left to right.</p>
		///	<p class="body">You can also choose a vertical view, where bands are separated into groups of rows, and indented in a manner similar to that of an outline or tree view.</p>
		///	<p class="body">In general, the vertical view style fits more data into a smaller area, while the horizontal view style makes the divisions between the levels of the hierarchy more apparent. Which view style you choose will depend on the requirements of your application.</p>
		///	</remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_ViewStyleBand")]
		[ LocalizedCategory("LC_Display") ]
		public Infragistics.Win.UltraWinGrid.ViewStyleBand ViewStyleBand
		{
			get
			{
				return this.viewStyleBand;
			}

			set
			{
				if ( value != this.viewStyleBand )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(ViewStyleBand), value ) )
                        // MRS NAS v8.2 - Unit Testing
						//throw new ArgumentException(Shared.SR.GetString("LE_NotSupportedException_175") );
                        throw new InvalidEnumArgumentException(Shared.SR.GetString("LE_NotSupportedException_175"));

					UltraGrid grid = this.grid as UltraGrid;

					if ( null != grid )
					{
						if ( grid.EventManager.InProgress( GridEventIds.InitializeRow ) )
							throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_173") );
					}
                    
					// SSP 9/14/04
					// Only bump the 
					bool isGroupByAffected = ViewStyleBand.OutlookGroupBy == value
												|| ViewStyleBand.OutlookGroupBy == this.viewStyleBand;

					this.viewStyleBand = value;

					// SSP 1/22/04
					// Moved the code into ClearViewStyle.
					//					
					

					// SSP 9/14/04 - Optimizations
					// Pass in the new parameter.
					//
					//this.ClearViewStyle();
					this.ClearViewStyle( isGroupByAffected );

					this.NotifyPropChange( PropertyIds.ViewStyleBand );
				}
			}
		}

		// SSP 1/22/04 - Optimization
		// Removed groupByStyleVersion.
		//
		//internal int GroupByStyleVersion { get { return this.groupByStyleVersion; } }

		internal int BandCardViewVersion { get { return this.bandCardViewVersion; } }

		internal void BumpBandCardViewVersion() { this.bandCardViewVersion++; }

		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeViewStyleBand() 
		{
			return ( this.viewStyleBand != Infragistics.Win.UltraWinGrid.ViewStyleBand.Vertical );
		}
 
		/// <summary>
		/// Reset ViewStyleBand to its default setting (vertical).
		/// </summary>
		public void ResetViewStyleBand() 
		{
			this.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.Vertical;
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal AllowAddNew AllowAddNewDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowAddNew() )
					return this.overrideObj.AllowAddNew;

				return AllowAddNew.Yes;
			}
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal AllowColMoving AllowColMovingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowColMoving() )
					return this.overrideObj.AllowColMoving;

				return AllowColMoving.WithinBand;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal AllowColSizing AllowColSizingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowColSizing() )
					return this.overrideObj.AllowColSizing;

				// SSP 8/13/05 BR04633 BR04666
				// This is to make column synchronization work when auto-fit functionality is turned on.
				// If AllowColSizing is explicitly set to Syncrhonized then honor that when auto-fit 
				// columns is enabled.
				// 
				//return AllowColSizing.Synchronized;
				return ! this.AutoFitAllColumns ? AllowColSizing.Synchronized : AllowColSizing.Free;
			}
		}

		// SSP 4/14/03
		//
		internal ColumnAutoSizeMode ColumnAutoSizeModeDefault
		{
			get
			{
				if ( this.HasOverride && this.Override.ShouldSerializeColumnAutoSizeMode( ) )
					return this.Override.ColumnAutoSizeMode;

				return ColumnAutoSizeMode.VisibleRows;
			}
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal AllowColSwapping AllowColSwappingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowColSwapping() )
					return this.overrideObj.AllowColSwapping;

				return AllowColSwapping.NotAllowed;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal DefaultableBoolean AllowDeleteDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowDelete() )
					return this.overrideObj.AllowDelete;

				return DefaultableBoolean.True;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal AllowGroupMoving AllowGroupMovingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowGroupMoving() )
					return this.overrideObj.AllowGroupMoving;

				return AllowGroupMoving.WithinBand;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal AllowGroupSwapping AllowGroupSwappingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowGroupSwapping() )
					return this.overrideObj.AllowGroupSwapping;

				return AllowGroupSwapping.NotAllowed;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal DefaultableBoolean AllowGroupByDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowGroupBy() )
					return this.overrideObj.AllowGroupBy;

				return DefaultableBoolean.True;
			}
		}



		// SSP 3/21/02
		// Added RowFilterModeDefault property
		//
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal Infragistics.Win.UltraWinGrid.RowFilterMode RowFilterModeDefault
		{
			get
			{
				// SSP 7/18/05
				// This is not necessary as the UltraGridBand.RowFilterModeResolved takes care of it.
				// 
				

				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowFilterMode( ) )
					return this.overrideObj.RowFilterMode;

				// Use the default of SiblingRowsOnly
				//
				return Infragistics.Win.UltraWinGrid.RowFilterMode.SiblingRowsOnly;
			}
		}

		#region AllowRowFilteringDefault

		// SSP 5/5/05 - NAS 5.2 Filter Row
		// Resolve AllowRowFiltering to True if the new FilterUIType property is set to
		// to a non-default value. Not needed anymore.
		//
		

		#endregion // AllowRowFilteringDefault

		#region RowFilterActionDefault

		// SSP 8/1/03 UWG1654 - Filter Action
		// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
		// appearances.
		//
		internal RowFilterAction RowFilterActionDefault
		{
			get
			{
				if ( this.HasOverride && 
					this.overrideObj.ShouldSerializeRowFilterAction( ) )
					return this.overrideObj.RowFilterAction;

				return RowFilterAction.HideFilteredOutRows;
			}
		}

		#endregion // RowFilterActionDefault

		// SSP 5/31/02
		// Added code for summary rows feature.
		//
		#region AllowRowSummariesDefault

		internal AllowRowSummaries AllowRowSummariesDefault
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeAllowRowSummaries( ) )
					return this.overrideObj.AllowRowSummaries;

				return AllowRowSummaries.False;
			}
		}

		#endregion // AllowRowSummariesDefault

		#region SummaryFooterCaptionVisibleDefault

		internal bool SummaryFooterCaptionVisibleDefault
		{
			get
			{
				if ( this.HasOverride && this.Override.ShouldSerializeSummaryFooterCaptionVisible( ) )
					return DefaultableBoolean.False != this.Override.SummaryFooterCaptionVisible;

				return true;
			}
		}

		#endregion // SummaryFooterCaptionVisibleDefault

		// SSP 6/30/05 - NAS 5.3 Column Chooser
		// Added HiddenWhenGroupByResolved on the column instead.
		// 
		

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int GroupByRowPaddingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeGroupByRowPadding() )
					return this.overrideObj.GroupByRowPadding;

				// Default is 1
				return 1;
			}
		}


		/// <summary>
		/// Removes all the group by columns from all the bands ungrouping all the rows.
		/// </summary>
		public void ClearGroupByColumns( )
		{
			if ( null == this.SortedBands )
				return;

			for ( int i = 0; i < this.SortedBands.Count; i++ )
			{
				UltraGridBand band = this.SortedBands[i];

				band.ClearGroupByColumns( );
			}
		}


		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal string GroupByRowDescriptionMaskDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeGroupByRowDescriptionMask() )
					return this.overrideObj.GroupByRowDescriptionMask;

				// JJD 11/28/01
				// Changed mask delimiters fron '<>' to '[]' so we could more
				// easily document it in XML
				//
				// Default 
				return SR.GetString("LDR_Layout_mask");
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal DefaultableBoolean AllowUpdateDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeAllowUpdate() )
					return this.overrideObj.AllowUpdate;

				return DefaultableBoolean.True;
			}
		}

		// SSP 3/25/06 - App Styling
		// 
		

		// SSP 3/25/06 - App Styling
		// 
		

		//JM 01-16-02 The following is no longer needed - now using
		//			  BorderStyleRow for cards
		
		
		// SSP 3/25/06 - App Styling
		// 
		

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal CellClickAction CellClickActionDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCellClickAction() )
					return this.overrideObj.CellClickAction;

				return CellClickAction.Edit;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal DefaultableBoolean CellMultiLineDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCellMultiLine() )
					return this.overrideObj.CellMultiLine;

				return DefaultableBoolean.False;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int CellPaddingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCellPadding() )
					return this.overrideObj.CellPadding;

				return 1;
			}
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int GetCellSpacingDefault( UltraGridBand band )
		{
			if ( this.HasOverride &&
				this.overrideObj.ShouldSerializeCellSpacing() )
				return this.overrideObj.CellSpacing;

			// JJD 12/14/01
			// For cardview bands return a default of 1
			//
			return ( band != null && band.CardView ) ? 1 : 0;
		}

		[Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int CardSpacingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeCardSpacing() )
					return this.overrideObj.CardSpacing;

				return 3;
			}
		}
		internal int ColWidthDefault
		{
            get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeDefaultColWidth() )
					return this.overrideObj.DefaultColWidth;

				return -1;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int RowHeightDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeDefaultRowHeight() )
					return this.overrideObj.DefaultRowHeight;

				return -1;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal ShowExpansionIndicator ExpansionIndicatorDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeExpansionIndicator() )
					return this.overrideObj.ExpansionIndicator;

				return ShowExpansionIndicator.Default;
			}
		}
		
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal HeaderClickAction HeaderClickActionDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeHeaderClickAction() )
					return this.overrideObj.HeaderClickAction;

				// MRS 11/18/04 - UWG567, UWG3023, UWG3414, and UWG3470
				// Support HeaderClickAction in Combo and DropDown
//				return 
//					ViewStyleBand.OutlookGroupBy == this.ViewStyleBand 
//					? HeaderClickAction.SortMulti 
//					: HeaderClickAction.Select;
				return this.grid.HeaderClickActionDefault;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int MaxSelectedCellsDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeMaxSelectedCells() )
					return this.overrideObj.MaxSelectedCells;

				return -1;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int MaxSelectedRowsDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeMaxSelectedRows() )
					return this.overrideObj.MaxSelectedRows;

				return -1;
			}
		}

		
		
		

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal DefaultableBoolean RowSelectorsDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSelectors() )
					return this.overrideObj.RowSelectors;

				return DefaultableBoolean.True;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal RowSizing RowSizingDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSizing() )
					return this.overrideObj.RowSizing;

				return RowSizing.Sychronized;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal RowSizingArea RowSizingAreaDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSizingArea() )
					return this.overrideObj.RowSizingArea;

				return RowSizingArea.RowSelectorsOnly;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int RowSizingAutoMaxLinesDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSizingAutoMaxLines() )
					return this.overrideObj.RowSizingAutoMaxLines;

				return 0;
			}
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int RowSpacingBeforeDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSpacingBefore() )
					return this.overrideObj.RowSpacingBefore;

				return 0;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal int RowSpacingAfterDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeRowSpacingAfter() )
					return this.overrideObj.RowSpacingAfter;

				return 0;
			}
		}

		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		internal int GroupByRowSpacingBeforeDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeGroupByRowSpacingBefore() )
					return this.overrideObj.GroupByRowSpacingBefore;

				return 0;
			}
		}
		
		// SSP 12/21/04 BR01386
		// Added GroupByRowSpacingBefore and GroupByRowSpacingAfter properties.
		//
		internal int GroupByRowSpacingAfterDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeGroupByRowSpacingAfter() )
					return this.overrideObj.GroupByRowSpacingAfter;

				return 0;
			}
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal SelectType SelectTypeCellDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSelectTypeCell() )
					return this.overrideObj.SelectTypeCell;

				return SelectType.Extended;
			}
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal SelectType SelectTypeColDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSelectTypeCol() )
					return this.overrideObj.SelectTypeCol;

				return SelectType.Extended;
			}
		}
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal SelectType SelectTypeRowDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSelectTypeRow() )
					return this.overrideObj.SelectTypeRow;

				return SelectType.Extended;
			}
		}

		// SSP 2/14/03 - RowSelectorWidth property
		// Added RowSelectorWidth property to allow the user to control the widths
		// of the row selectors.
		//
		// SSP 2/4/05 - IDataErrorInfo Support
		// Resolve the row selector width differently based on whether row data error 
		// is supported so the data error icon and the row icons both have enough space.
		//
		//internal int RowSelectorWidthDefault
		internal int GetRowSelectorWidthDefault( UltraGridBand band )
		{
			if ( this.HasOverride && this.overrideObj.ShouldSerializeRowSelectorWidth( ) )
				return this.overrideObj.RowSelectorWidth;

			// SSP 2/4/05 - IDataErrorInfo Support
			// Resolve the row selector width differently based on whether row data error 
			// is supported so the data error icon and the row icons both have enough space.
			//
			//return UltraGridBand.ROW_SELECTOR_WIDTH;
			// SSP 3/29/05 - NAS 5.2 Fixed Rows
			// Also take into account the fixed row indicator width.
			//
			// ----------------------------------------------------------------------
			//return ! band.SupportsDataErrorInfoOnRowsResolved 
			//	? UltraGridBand.ROW_SELECTOR_WIDTH
			//	: UltraGridBand.ROW_SELECTOR_WIDTH_WITH_DATA_ERROR_ICONS;
			int rowSelectorWidth = UltraGridBand.ROW_SELECTOR_ROW_ICON_WIDTH;
			if ( band.SupportsDataErrorInfoOnRowsResolved )
				rowSelectorWidth += DataErrorIconUIElement.DEFAULT_WIDTH;

			if ( FixedRowIndicator.Button == band.FixedRowIndicatorResolved )
				rowSelectorWidth += FixedRowIndicatorUIElement.FIXED_ROW_INDICATOR_WIDTH
					+ 2 * FixedRowIndicatorUIElement.FIXED_ROW_INDICATOR_PADDING;

			rowSelectorWidth += band.GetRowSelectorNumberWidth( );

            // MBS 2/28/08 - RowEditTemplate NA2008 V2            
            if ((band.RowEditTemplateUITypeResolved & RowEditTemplateUIType.RowSelectorImage) != 0)
                rowSelectorWidth += RowSelectorEditTemplateUIElement.ICON_EXTENT +
                    2 * RowSelectorEditTemplateUIElement.PADDING;
			
			rowSelectorWidth = Math.Max( rowSelectorWidth, UltraGridBand.ROW_SELECTOR_WIDTH );

			return rowSelectorWidth;
			// ----------------------------------------------------------------------
		}

		// SSP 7/8/02 UWG1154
		//
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal SelectType SelectTypeGroupByRowDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeSelectTypeGroupByRow() )
					return this.overrideObj.SelectTypeGroupByRow;

				return SelectType.Extended;
			}
		}

		// SSP 7/21/06 BR14294
		// Tip style needs to be resolved differently based whether the ToolTipText has been explicitly set
		// or the cell's value (clipped) will be displayed.
		// Commented out the TipStyleCellDefault.
		// 
		

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal TipStyle TipStyleRowConnectorDefault
		{
			get
			{
				if ( this.HasOverride &&
					this.overrideObj.ShouldSerializeTipStyleRowConnector() )
					return this.overrideObj.TipStyleRowConnector;

				return TipStyle.Show;
			}
		}

		// SSP 7/19/05
		// 
		

		internal int CellChildElementsCacheVersion
		{
			get
			{
				return this.cellChildElementsCacheVersion;
			}
		}

		internal void BumpCellChildElementsCacheVersion( )
		{
			this.cellChildElementsCacheVersion++;

			// SSP 6/13/05
			// Added RowChildElementsCacheVersion.
			// 
			this.BumpRowChildElementsCacheVersion( );
		}

		// SSP 7/31/01
		// Added RowFiltersVersionNumber and BumpRowFiltersVersionNumber.
		//
		internal int RowFiltersVersion
		{
			get
			{
				return this.rowFiltersVersionNumber;
			}
		}

		internal void BumpRowFiltersVersion( )
		{
			this.rowFiltersVersionNumber++;

			// SSP 8/1/03 UWG1654 - Filter Action
			// Bump cell child elements version.
			//
			this.BumpCellChildElementsCacheVersion( );

			// SSP 8/12/03 - Optimizations - Grand Verify Version Number
			//
			this.BumpGrandVerifyVersion( );
		}

		// SSP 6/17/04 UWG3207
		// Added a way to suspend dirtying of summaries when we receive ItemChanged
		// notification. Adedd BumpSummariesVersion method.
		//
		internal void BumpSummariesVersion( )
		{
            // MRS 5/7/2009 - TFS17252
            //for ( int i = 0; i < this.sortedBands.Count; i++ )
            //{
            //    UltraGridBand band = this.sortedBands[i];
            //    if ( null != band && band.HasSummaries )
            //        band.Summaries.BumpSummariesVersion( );
            //}
            BandsCollection bands = this.bands;
            for (int i = 0; i < bands.Count; i++)
            {
                UltraGridBand band = bands[i];
                if (null != band && band.HasSummaries)
                    band.Summaries.BumpSummariesVersion();
            }
		}

		internal void OnOverrideChanged ( PropChangeInfo propChange, UltraGridBand band)
		{
			// SSP 8/12/03 - Optimizations - Grand Verify Version Number
			//
			this.BumpGrandVerifyVersion( );

			// SSP 3/20/06 - App Styling
			// 
			StyleUtils.DirtyCachedPropertyVals( this );
			
			if ( propChange.PropId is PropertyIds )
			{
				PropertyIds propertyId = (PropertyIds)propChange.PropId;
				switch ( propertyId )
				{
					// SSP 8/6/03
					// Added a case for Appearance.
					//
					case PropertyIds.Appearance:
						this.DirtyGridElement( true );
						this.BumpCellChildElementsCacheVersion( );
						break;
					case PropertyIds.GroupByColumnsHidden:
						// SSP 7/22/03 - Fixed headers
						// Changing the GroupByColumnsHidden property can potentially cause columns
						// to get hidden or unhidden if those columns are group-by. In which case
						// we need to recalc the band metrics.
						//
						if ( null != this.colScrollRegions )
							this.ColScrollRegions.DirtyMetrics( );
						this.SynchronizedColListsDirty = true;
						this.DirtyGridElement( );
						this.BumpRowLayoutVersion( );
						this.BumpFixedHeadersVerifyVersion( );
						break;
					case PropertyIds.HeaderAppearance:
                    //JDN 11/19/04 Added RowSelectorHeader
                    case PropertyIds.RowSelectorHeaderAppearance:
					case PropertyIds.RowAppearance:
					case PropertyIds.RowAlternateAppearance:
						// SSP 9/27/01
						// added cases for group by related properties in override
						//
					case PropertyIds.GroupByColumnAppearance:
					case PropertyIds.GroupByColumnHeaderAppearance:
					case PropertyIds.RowPreviewAppearance:
					
						// JJD 1/21/02 - UWG815 Moved nullText from layout to override			
					case PropertyIds.NullText:
					{
						if ( null != propChange.FindPropId( Infragistics.Win.AppearancePropIds.FontData ) )
						{
							// if the apperances are changing let the bands collection know
							// to kill cached values
							//
							if ( (this.bands != null) && (band !=null) )
								this.bands.OnBaseFontChanged();
								// SSP 8/14/02 UWG1290
								// If the font settings on a band's override did not change, then layout's
								// override's font settings may have changed in which case we need to call
								// OnBaseFontChanged on the layout (which will also call the bands 
								// collection's OnBaseFontChanged as well).
								//
							else
								this.OnBaseFontChanged( );
						}
							// If any appearance property changes other than font dirty the grid to 
							// force a repaint
						else
						{
							this.DirtyGridElement();

							// SSP 11/28/01 UWG769
							// Also bump the CellChildElementsCacheVersion so that all the cell ui elements
							// reposition their child elements. Normally they don't becaus of the caching 
							// mechanism, however in circumstances where certain properties off
							// the appearances (like Image off RowAppearance or RowAlternateAppearance) 
							// changes, we do want the cell elements to recreate child elemnts so 
							// any image elements can be added or removed or repositioned.
							//
							if ( (PropertyIds)propChange.PropId == PropertyIds.RowPreviewAppearance ||
								(PropertyIds)propChange.PropId == PropertyIds.RowAppearance ||
								(PropertyIds)propChange.PropId == PropertyIds.RowAlternateAppearance )
							{
								this.BumpCellChildElementsCacheVersion( );
							}
						}

						// SSP 2/28/03 - Row Layout Functionality
						//
						this.BumpRowLayoutVersion( );
						break;
					}

					case PropertyIds.BorderStyleHeader:
					{
						this.DirtyGridElement();

						if ( this.colScrollRegions != null )
							// SSP 7/24/03
							// Don't pass in true as the first parameter because that will cause initialization
							// of the metrics and possibly lead to premature verification of some dirty state.
							//
							//this.GetColScrollRegions( true ).DirtyMetrics();
							this.GetColScrollRegions( false ).DirtyMetrics( );

						// SSP 2/28/03 - Row Layout Functionality
						//
						this.BumpRowLayoutVersion( );
						break;
					}

					case PropertyIds.AllowUpdate:
					case PropertyIds.AllowAddNew:
					{
						// When the caption changes let the control site
						// know that it nneds to call GetControlInfo on the
						// next accelerator
						//
						if ( this.grid !=null )
						{
							// SSP 11/11/03 Add Row Feature
							// Also dirty the visible rows since change in AllowAddNew could lead to
							// add-rows showing up or being hidden.
							//
							// ----------------------------------------------------------------------------
							//this.DirtyGridElement();
							this.DirtyGridElement( true, true );
							// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
							//
							//this.BumpScrollRowPositionVersion( );
							//if ( null != this.scrollableRowCountManager )
							//	this.scrollableRowCountManager.DirtyRowCount( );
							this.BumpScrollableRowCountVersion( );
							// ----------------------------------------------------------------------------
						}
						break;
					}

					case PropertyIds.CellPadding:
					case PropertyIds.CellSpacing:
						// SSP 11/26/03 - Optimizations
						//
						// ------------------------------------------------
						this.ClearBandRowHeightMetrics( band );
						this.DirtyGridElement( true );
						if ( null != this.colScrollRegions )
							this.colScrollRegions.DirtyMetrics( );
						goto case PropertyIds.Clear;
						// ------------------------------------------------
					case PropertyIds.Clear:
					case PropertyIds.AllowColSizing:
					case PropertyIds.BorderStyleRow:
					case PropertyIds.BorderStyleCell:
					case PropertyIds.DefaultColWidth:
					case PropertyIds.RowSelectors:
						// SSP 2/14/03 - RowSelectorWidth property
						// Added RowSelectorWidth property to allow the user to control the widths
						// of the row selectors.
					case PropertyIds.RowSelectorWidth:
                        //JDN 11/19/04 Added RowSelectorHeader
                    case Win.UltraWinGrid.PropertyIds.RowSelectorHeaderStyle:
						// SSP 9/27/01
						// added group by property cases
						//
					case PropertyIds.GroupByRowAppearance:
					case PropertyIds.GroupByRowDescriptionMask:
					case PropertyIds.GroupByRowPadding:
					case PropertyIds.ExpansionIndicator:
						// JJD 12/13/01
						// Addedsupport for new card properties
						//
					case PropertyIds.CardSpacing:
					case PropertyIds.BorderStyleCard:
					case PropertyIds.BorderStyleCardArea:
					// SSP 11/11/03 Add Row Feature
					//
					case PropertyIds.BorderStyleTemplateAddRow:
						// SSP 8/28/03
						// Added an entry for BorderStyleRowSelector.
						//
					case PropertyIds.BorderStyleRowSelector:
					// SSP 2/4/05 - IDataErrorInfo Support
					//
					case PropertyIds.SupportDataErrorInfo:
					case PropertyIds.DataErrorRowSelectorAppearance:
					case PropertyIds.DataErrorCellAppearance:
					case PropertyIds.DataErrorRowAppearance:
					// SSP 3/30/05 - NAS 5.2 Row Numbers
					//
					case PropertyIds.RowSelectorNumberStyle:
					// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
					//
					case PropertyIds.RowSelectorStyle:
					case PropertyIds.HeaderStyle:
					// JAS v5.2 New Default Look & Feel For UltraGrid 4/13/05
					//
					case PropertyIds.ButtonStyle:
					// JAS v5.2 Wrapped Header Text 4/18/05
					//
					case PropertyIds.WrapHeaderText:
					// SSP 3/29/05 - NAS 5.2 Filter Row/Fixed Rows
					//
					// ----------------------------------------------------------------------------
					case PropertyIds.FixedRowAppearance:
					case PropertyIds.FixedRowCellAppearance:
					case PropertyIds.FixedRowSelectorAppearance:
					case Infragistics.Win.UltraWinGrid.PropertyIds.BorderStyleFilterCell:
					case Infragistics.Win.UltraWinGrid.PropertyIds.BorderStyleFilterOperator:
					case Infragistics.Win.UltraWinGrid.PropertyIds.BorderStyleFilterRow:
					case Infragistics.Win.UltraWinGrid.PropertyIds.BorderStyleSpecialRowSeparator:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterCellAppearance:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterCellAppearanceActive:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterClearButtonAppearance:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterClearButtonLocation:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterOperandStyle:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterOperatorAppearance:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterOperatorDefaultValue:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterOperatorDropDownItems:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterOperatorLocation:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterRowAppearance:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterRowAppearanceActive:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterRowPrompt:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterRowSelectorAppearance:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterRowSpacingAfter:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterRowSpacingBefore:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterUIType:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FixedRowIndicator:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FixedRowsLimit:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FixedRowSortOrder:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FixedRowStyle:
					case Infragistics.Win.UltraWinGrid.PropertyIds.GroupBySummaryDisplayStyle:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SequenceFilterRow:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SequenceFixedAddRow:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SequenceSummaryRow:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SpecialRowSeparator:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SpecialRowSeparatorHeight:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SpecialRowSeparatorAppearance:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SummaryDisplayArea:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SummaryFooterSpacingAfter:
					case Infragistics.Win.UltraWinGrid.PropertyIds.SummaryFooterSpacingBefore:
					case Infragistics.Win.UltraWinGrid.PropertyIds.TemplateAddRowPrompt:
					case Infragistics.Win.UltraWinGrid.PropertyIds.TemplateAddRowPromptAppearance:
					case Infragistics.Win.UltraWinGrid.PropertyIds.FilterRowPromptAppearance:
					// SSP 7/18/05 - NAS 5.3 Header Placement
					// 
                    case Infragistics.Win.UltraWinGrid.PropertyIds.HeaderPlacement:
					// ----------------------------------------------------------------------------
                    // MBS 3/12/08 - RowEditTemplate NA2008 V2
                    case Infragistics.Win.UltraWinGrid.PropertyIds.RowEditTemplateUIType:     
                    // MBS 5/6/08 - RowEditTemplate NA2008 V2
                    case Infragistics.Win.UltraWinGrid.PropertyIds.RowEditTemplateRowSelectorImage:
                    //
                    // MBS 12/30/08 - NA9.1 Excel Style Filtering
                    case PropertyIds.FilterUIProvider:
					{
						if ( PropertyIds.FixedRowStyle == propertyId
							|| PropertyIds.FixedRowsLimit == propertyId 
							|| PropertyIds.FixedRowSortOrder == propertyId )
							this.BumpFixedRowsCollectionsVersion( );

                        // MBS 3/20/09 - TFS15264
                        // If we've changed the type of filtering we're providing, we should also
                        // clear the ValueList that we used for filtering, since if we were originally
                        // allowing filtering through the header icons, the first time that we drop down
                        // the list in a FilterCell, we'll still be using the same list.  This is because
                        // we were generally expecting one or the other; the header icons recreate the list
                        // each time, while the FilterCell clears the list after exiting edit mode, so we 
                        // never would clear the list
                        if (this.grid != null && this.grid.HasFilterDropDown)
                            this.grid.FilterDropDown.ValueListItems.Clear();

						// SSP 7/15/05
						// 
						if ( PropertyIds.SummaryDisplayArea == propertyId )
							this.BumpScrollableRowCountVersion( );

						// SSP 4/22/05 - NAS 5.2 Filter Row
						// 
						this.BumpVerifyEditorVersionNumber( null );

						// For the above changes set the synchronized col list dirty
						// flag on 
						//
						this.synchronizedColListsDirty = true;
						// Moved dirty col metrics up here so they aren't called
						// for the cases below
						//

						if( this.colScrollRegions != null )
							// SSP 7/24/03
							// Don't pass in true as the first parameter because that will cause initialization
							// of the metrics and possibly lead to premature verification of some dirty state.
							//
							//this.GetColScrollRegions( true ).DirtyMetrics();
							this.GetColScrollRegions( false ).DirtyMetrics( );
						//    
						// fall thru dirty element logic below
						//
						
						this.ClearBandRowHeightMetrics( band );
						if ( this.rowScrollRegions != null )
							this.rowScrollRegions.DirtyAllVisibleRows();
						this.DirtyGridElement( true );					

						// SSP 8/2/02 
						// Also bump the cell cache version number.
						//
						this.BumpCellChildElementsCacheVersion( );

						// SSP 2/28/03 - Row Layout Functionality
						//
						this.BumpRowLayoutVersion( );
						break;
					}

					case PropertyIds.DefaultRowHeight:
					case PropertyIds.RowSpacingBefore:
					case PropertyIds.RowSpacingAfter:
					// SSP 12/21/04 BR01386
					// Added entries for GroupByRowSpacingBefore and GroupByRowSpacingAfter.
					//
					case PropertyIds.GroupByRowSpacingBefore:
					case PropertyIds.GroupByRowSpacingAfter:
					// SSP 11/25/03 UWG2013 UWG2553
					// Added MinRowHeight property to Override to allow for unrestricted control over the row heights.
					//
					case PropertyIds.MinRowHeight:
					// SSP 11/18/03 Add Row Feature
					//
					case PropertyIds.TemplateAddRowSpacingBefore:
					case PropertyIds.TemplateAddRowSpacingAfter:
					// JAS 2005 v2 GroupBy Row Extensions
					//
					case PropertyIds.GroupByRowExpansionStyle:
					case PropertyIds.GroupByRowInitialExpansionState:
					{
						this.ClearBandRowHeightMetrics ( band );
						if ( this.rowScrollRegions != null )
							this.rowScrollRegions.DirtyAllVisibleRows();
						this.DirtyGridElement( true );
						break;
					}

					case PropertyIds.EditCellAppearance:
					{
						// SSP 4/29/02
						// EM Embeddable editors. 
						// To reflect the changes in color settings, dirtying the cell
						// would be sufficient.
						//
						
						if ( null != this.CellInEditMode )
							this.CellInEditMode.InvalidateItem( );
						break;
					}

						// Invalidate the grid when selected appearance changes
						//
					case PropertyIds.ActiveCellAppearance:
					case PropertyIds.ActiveRowAppearance:
					case PropertyIds.CellAppearance:
						// SSP 12/7/01 UWG546
						// Added CellButtonAppearance.
					case PropertyIds.CellButtonAppearance:
					case PropertyIds.SelectedRowAppearance:
					case PropertyIds.SelectecCellAppearance:
					case PropertyIds.RowSelectorAppearance:
						// JJD 12/13/01
						// Addedsupport for new card properties
						//
					case PropertyIds.CardCaptionAppearance:
					case PropertyIds.ActiveCardCaptionAppearance:
					case PropertyIds.SelectedCardCaptionAppearance:
					// SSP 7/22/03 - Fixed headers
					//
					case PropertyIds.FixedHeaderAppearance:
					case PropertyIds.FixedCellAppearance:
					case PropertyIds.FixedCellSeparatorColor:
						// SSP 8/1/03 UWG1654 - Filter Action
						// Added cases for FilterRowAppearance and FilterCellAppearance.
						//
					case PropertyIds.FilteredOutRowAppearance:
					case PropertyIds.FilteredOutCellAppearance:
					case PropertyIds.FilteredInRowAppearance:
					case PropertyIds.FilteredInCellAppearance:
					{
						this.ClearBandRowHeightMetrics ( band );
						this.DirtyGridElement( true );
						if ( this.colScrollRegions != null )
							// SSP 7/24/03
							// Don't pass in true as the first parameter because that will cause initialization
							// of the metrics and possibly lead to premature verification of some dirty state.
							//
							//this.GetColScrollRegions( true ).DirtyMetrics();
							this.GetColScrollRegions( false ).DirtyMetrics();

						
						// SSP 12/3/01 UWG810
						// Bump the cell cache version so that cell elements
						// reprosition their children.
						//
						this.BumpCellChildElementsCacheVersion( );

						// SSP 2/28/03 - Row Layout Functionality
						//
						this.BumpRowLayoutVersion( );
						break;
					}

						// JJD 1/10/02
						// If the CardAreaAppearance changes then dirty the grid
						//
					case PropertyIds.CardAreaAppearance:
					case PropertyIds.AllowColSwapping:
					case PropertyIds.AllowGroupSwapping:					
					case PropertyIds.AllowRowFiltering:					
					case PropertyIds.AllowRowSummaries:
					// SSP 7/22/03 - Fixed headers
					// Added FixedHeaderIndicator case.
					//
					case PropertyIds.FixedHeaderIndicator:
					{
						// SSP 4/8/03
						// Since tunring on/off the row filtering, row summaries or swapping
						// changes the default widths of the columns, dirty the colscrollregion
						// matrix as well.
						//
						if ( null != this.ColScrollRegions )
							this.ColScrollRegions.DirtyMetrics( );

						this.DirtyGridElement( false );

						// SSP 2/28/03 - Row Layout Functionality
						//
						this.BumpRowLayoutVersion( );

						break;
					}
					case PropertyIds.RowSizing:
					{
						// SSP 8/23/02 UWG1185
						// Change in RowSizing requires dirtying the primary metrics as well.
						this.DirtyGridElement( true );

						// SSP 8/26/05 BR05862
						//
						this.ClearBandRowHeightMetrics( null );

						break;
					}
						// SSP 7/31/02
						// When the row filter mode changes, we need to dirty the grid
						// and regenerate visible rows because the filters that should be
						// aplied now have changed.
						// Added below case.
						//
						// SSP 8/1/03 UWG1654 - Filter Action
						// Added an entry for PropertyIds.RowFilterAction.
						//
					case PropertyIds.RowFilterAction:
					case PropertyIds.RowFilterMode:
					{
						this.BumpRowFiltersVersion( );

						this.BumpScrollableRowCountVersion( );
						// SSP 2/19/04 - Virtual Mode - Scrolling Related Optimization
						//
						//this.BumpScrollRowPositionVersion( );
						break;
					}
						// SSP 6/10/02
						// Added code for summary rows.
						//
					case PropertyIds.BorderStyleSummaryFooter:
					case PropertyIds.BorderStyleSummaryFooterCaption:
					case PropertyIds.BorderStyleSummaryValue:					
					case PropertyIds.SummaryFooterAppearance:
					case PropertyIds.SummaryFooterCaptionAppearance:
					case PropertyIds.SummaryValueAppearance:
					// SSP 5/4/05 - NAS 5.2 Extension of Summaries Functionality
					// Added entry for GroupBySummaryValueAppearance.
					//
					case PropertyIds.GroupBySummaryValueAppearance:
					// SSP 11/11/03 Add Row Feature
					// Added a case for TemplateAddRowAppearance.
					//
					case PropertyIds.TemplateAddRowAppearance:
					case PropertyIds.AddRowAppearance:
					// SSP 12/11/03 UWG2766
					// Added AddRowCellAppearance and TemplateAddRowCellAppearance properties.
					//
					case PropertyIds.TemplateAddRowCellAppearance:
					case PropertyIds.AddRowCellAppearance:
						//	BF 8.2.02
						//	We need to do this when the SummaryFooterCaptionVisible property changes
					case PropertyIds.SummaryFooterCaptionVisible:
					// SSP 8/22/05 BR05760
					// 
					case PropertyIds.HotTrackCellAppearance:
					case PropertyIds.HotTrackHeaderAppearance:
					case PropertyIds.HotTrackRowAppearance:
					case PropertyIds.HotTrackRowCellAppearance:
					case PropertyIds.HotTrackRowSelectorAppearance:
                    // MBS 5/12/09 - NA9.2 GroupByRowConnector Appearance
                    case PropertyIds.GroupByRowConnectorAppearance:
					{
						if ( this.rowScrollRegions != null )
							this.rowScrollRegions.DirtyAllVisibleRows();
						this.DirtyGridElement( true );
						break;
					}
					// SSP 5/12/03 - Optimizations
					// Added a way to just draw the text without having to embed a embeddable ui element in
					// cells to speed up rendering.
					//
					// SSP 12/11/03 UWG2783
					// Added a case for CellMultiLine.
					//
					// SSP 12/12/03 DNF135
					// Added a way to control whether ink buttons get shown.
					// Added ShowInkButton property.
					//
					case PropertyIds.ShowInkButton:
					case PropertyIds.CellMultiLine:
					case PropertyIds.CellDisplayStyle:
					// SSP 8/26/06 - NAS 6.3
					// Added ReadOnlyCellAppearance to the override.
					// 
					case PropertyIds.ReadOnlyCellAppearance:
						// SSP 10/19/07 BR25706 BR25707
						// Added ActiveRowCellAppearance. This is to let one be able to specify the 
						// appearance for cell of the active row.
						// 
					case PropertyIds.ActiveRowCellAppearance:
                    // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (ActiveAppearancesEnabled & SelectedAppearancesEnabled)
                    case PropertyIds.ActiveAppearancesEnabled:
                    case PropertyIds.SelectedAppearancesEnabled:
					{
						this.DirtyGridElement( );
						this.BumpCellChildElementsCacheVersion( );
						break;
					}
					// SSP 6/11/03 - Fixed Headers
					// If the fixed header count is changed, then dirty the col scroll metrics.
					//
					case PropertyIds.Fixed:
					// SSP 11/22/04 - Merged Cell Feature
					// 
					case PropertyIds.MergedCellAppearance:
					case PropertyIds.MergedCellContentArea:
					case PropertyIds.MergedCellStyle:
					case PropertyIds.MergedCellEvaluationType:
					{
						this.BumpMergedCellVersion( );						
						break;
					}

					// JAS v5.2 GroupBy Break Behavior 5/4/05
					//
					case PropertyIds.SortComparisonType:
					{
						this.BumpSortVersionNumber();
						break;
					}

					// MD 4/17/08 - 8.2 - Rotated Column Headers
					case PropertyIds.ColumnHeaderTextOrientation:
					case PropertyIds.GroupHeaderTextOrientation:
					{
						this.ClearCachedCaptionHeight();
						this.DirtyGridElement();

						// MD 7/8/08 - BR34519
						// The row layout version should also be bumped when the rotation changes.
						this.BumpRowLayoutVersion();

						break;
					}

					// MD 12/9/08 - 9.1 - Column Pinning Right
					case PropertyIds.FixHeadersOnRight:
					{
						this.BumpFixedHeadersVerifyVersion();
						break;
					}
                    // CDS 02/04/08 - TFS12517 - NAS v9.1 Header CheckBox - When the synchronization changes, we need to invalidate all the cached CheckState values, so bump the version number. 
                    // Need to bump the HeaderCheckBoxSynchronizationVersion on the band (or bands) to dirty the CheckState cache.
                    case PropertyIds.HeaderCheckBoxSynchronization:
                    {
                        if (band == null)
                        {
                            // no band was supplied, so bump the version on all the bands
                            foreach (UltraGridBand gridBand in this.Bands)
                                gridBand.BumpHeaderCheckBoxSynchronizationVersion();
                        }
                        else
                            // bump the version on the provided band
                            band.BumpHeaderCheckBoxSynchronizationVersion();

                        // CDS 2/24/09 TFS12452 Dirty the grid when the header checkbox needs to be resynched.
                        this.DirtyGridElement();
                        break;
                    }

                    // MBS 3/31/09 - NA9.2 CellBorderColor                    
                    case PropertyIds.ActiveCellBorderThickness:
                    {
                        UltraGrid grid = this.Grid as UltraGrid;
                        if (grid != null && grid.ActiveCellInternal != null)
                            grid.ActiveCellInternal.InvalidateItemAllRegions();

                        break;
                    }
                    // CDS 7/02/09 TFS19043 Need to dirty the RowLayoutCachedInfo to recalculate the header height
                    case PropertyIds.HeaderCheckBoxVisibility:
                    case PropertyIds.HeaderCheckBoxAlignment:
                    {
                        if (band == null)
                        {
                            // no band was supplied, so bump the version on all the bands
                            foreach (UltraGridBand gridBand in this.Bands)
                            // CDS 7/07/09 TFS17996
                            //  gridBand.DirtyRowLayoutCachedInfo();
                            {
                                gridBand.DirtyRowLayoutCachedInfo();
                                gridBand.DirtyCachedContainsCheckboxOnTopOrBottom();
                            }
                        }
                        else
                            // bump the version on the provided band
                        // CDS 7/07/09 TFS17996
                        //  band.DirtyRowLayoutCachedInfo();
                        {
                            band.DirtyRowLayoutCachedInfo();
                            band.DirtyCachedContainsCheckboxOnTopOrBottom();
                        }
                        break;
                    }
                    // MBS 8/19/09 - TFS20554
                    case PropertyIds.ActiveCellColumnHeaderAppearance:
                    {
                        UltraGridCell cell = this.ActiveCellInternal;
                        if (cell != null)
                        {
                            cell.Column.Header.InvalidateItemAllRegions();
                        }
                        break;
                    }
                    case PropertyIds.ActiveCellRowSelectorAppearance:
                    {
                        UltraGridCell cell = this.ActiveCellInternal;
                        if (cell != null)
                        {
                            RowUIElement rowElem = cell.Row.GetUIElement() as RowUIElement;
                            if (rowElem != null)
                            {
                                UIElement rowSelectorElem = rowElem.GetDescendant(typeof(RowSelectorUIElementBase));
                                if (rowSelectorElem != null)
                                    rowSelectorElem.Invalidate();
                            }
                        }
                        break;
                    }

				}		
			}
		}

		// SSP 11/4/05 BR07555
		// 
		internal void ClearBandRowHeightMetrics(  )
		{
			this.ClearBandRowHeightMetrics( null );
		}

		internal void ClearBandRowHeightMetrics ( UltraGridBand band )
		{
			
			if ( null != band )
				band.ClearCachedHeightValues();
			else
			{				
				if ( this.sortedBands == null )
					return;
				
				// loop thru the bands collection and call ClearCachedHeightValues
				//
				for ( int i=0; i<this.sortedBands.Count; i++ ) 
				{
					((UltraGridBand)this.sortedBands[i]).ClearCachedHeightValues();					
				}
			}
		

		}


		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{

			if ( propChange.Source == this.overrideObj )
			{
				this.OnOverrideChanged( propChange, null );
				this.NotifyPropChange( PropertyIds.Override, propChange );
				return;
			}

			else if ( propChange.Source == this.addNewBox )
			{
				bool dirtyEntireGrid = false;
				
				if ( propChange.PropId is PropertyIds )
				{
					switch ( (PropertyIds)propChange.PropId )
					{
						case PropertyIds.ButtonAppearance:
						case PropertyIds.Hidden:
						case PropertyIds.Appearance:
						case PropertyIds.AddNewBoxStyle:
							dirtyEntireGrid = true;
							break;
					}

					if ( dirtyEntireGrid )
					{
						this.DirtyGridElement( true );
					}
					else 
					{
						UIElement element = this.GetUIElement(false);

						if ( element != null )
						{
							element = element.GetDescendant( typeof(AddNewBoxUIElement) );
							if ( element != null )
								element.DirtyChildElements();
						}
					}
				}
				
				this.NotifyPropChange( PropertyIds.AddNewBox, propChange );
				
				return;
			}		

			else if ( propChange.Source == this.groupByBox )
			{
				bool dirtyEntireGrid = false;
				
				if ( propChange.PropId is PropertyIds )
				{
					switch ( (PropertyIds)propChange.PropId )
					{
						case PropertyIds.ButtonAppearance:
						case PropertyIds.Hidden:
						case PropertyIds.Appearance:
						case PropertyIds.AddNewBoxStyle:
						case PropertyIds.ShowBandLabels:
						case PropertyIds.BandLabelBorderStyle:
						case PropertyIds.Prompt:
						case PropertyIds.GroupByBoxStyle:
							dirtyEntireGrid = true;
							break;
					}

					if ( dirtyEntireGrid )
					{
						this.DirtyGridElement( true );
					}
					else 
					{
						UIElement element = this.GetUIElement(false);

						if ( element != null )
						{
							element = element.GetDescendant( typeof( GroupByBoxUIElement ) );
							if ( element != null )
								element.DirtyChildElements();
						}
					}
				}
				
				this.NotifyPropChange( PropertyIds.GroupByBox, propChange );
				
				return;
			}		
			
			else if ( propChange.Source == this.bands )
			{
				// Pass this notifcation on to our listeners with the
				// property id of 'Bands'
				
				this.NotifyPropChange( PropertyIds.Bands, propChange );
				return;
			}

			else if ( propChange.Source == this.colScrollRegions )
			{
				// Pass this notifcation on to our listeners with the
				// property id of 'ColScrollRegions'
				this.NotifyPropChange( PropertyIds.ColScrollRegions, propChange );
				return;
			}

            
			else if ( propChange.Source == this.rowScrollRegions )
			{
				// Pass this notifcation on to our listeners with the
				// property id of 'RowScrollRegions'
				this.NotifyPropChange( PropertyIds.RowScrollRegions, propChange );
				return;
			}


			else if ( propChange.Source == this.valuelists )
			{
				this.NotifyPropChange( PropertyIds.ValueLists, propChange );
				return;
			}

			else if ( propChange.Source == this.appearances )
			{
				this.NotifyPropChange( Infragistics.Win.AppearancePropIds.AppearancesCollection, propChange );
				return;
			}
				// AS 6/17/02 UWG1239
				// Moved up from below since the next else just checks for the existence
				// of appearance holders, not whether the source came from an appearance.
				//
				// AS - 12/19/01
			else if (propChange.Source == this.scrollbarLook)
			{
				this.NotifyPropChange( PropertyIds.ScrollBarLook, propChange );
			}
			// SSP 5/9/05 - NAS 5.2 Filter Row
			//
			else if ( this.filterOperatorValueListAll == propChange.Source )
			{
				this.NotifyPropChange( PropertyIds.FilterOperatorsValueList, propChange );
			}
			// SSP 7/7/05 - NAS 5.3 Empty Rows
			//
			else if ( this.emptyRowSettings == propChange.Source )
			{
				this.BumpGrandVerifyVersion( );
				this.BumpCellChildElementsCacheVersion( );
				this.DirtyGridElement( true );
				this.NotifyPropChange( PropertyIds.EmptyRowSettings, propChange );
			}
            // CDS 9.2 RowSelector State-Specific Images
            else if (this.rowSelectorImages == propChange.Source)
            {
                this.DirtyGridElement();
                this.NotifyPropChange(PropertyIds.RowSelectorImages, propChange);
            }
            // JJD 11/20/01 - UWG677
            // Check appearanceHolders not null instead of HasAppearance
            //
            else if (this.appearanceHolders != null)
            {

                // loop thru our appearance object table to see if the Notifier was one
                // of these.
                for (int i = 0; i < this.appearanceHolders.Length; i++)
                {
                    if (this.appearanceHolders[i] != null &&
                        // SSP 11/29/01 UWG779
                        // Use HasAppearance to check if an Appearance object
                        // has been allocated or not instead of ShouldSerialize
                        // because when the appearance is Reset, it ShouldSerialize
                        // will return false and it won't enter if block and not 
                        // do anything which we don't want because we do want the
                        // reset appearance settings to take effect.
                        // 
                        this.appearanceHolders[i].HasAppearance &&
                        //this.appearanceHolders[i].ShouldSerialize() &&
                        propChange.Source == this.appearanceHolders[i].RootAppearance)
                    {

                        bool fontChanging = (null != propChange.FindPropId(Infragistics.Win.AppearancePropIds.FontData));
                        bool imageChanging = (null != propChange.FindPropId(Infragistics.Win.AppearancePropIds.Image));
                        bool imageAlignChanging = (null != propChange.FindPropId(Infragistics.Win.AppearancePropIds.ImageHAlign)) ||
                            (null != propChange.FindPropId(Infragistics.Win.AppearancePropIds.ImageVAlign));

                        // if the apperances are changing let the bands collection know
                        // to kill cached values
                        //
                        switch (i)
                        {
                            case (int)LayoutAppearanceIndex.Default:
                                {
                                    // MRS 10/22/04 - UWG3066
                                    // MRS 10/25/04 - Moved this check to the proper place
                                    if ((Infragistics.Win.AppearancePropIds)propChange.PropId == Infragistics.Win.AppearancePropIds.BackColor)
                                        this.SynchGridBackColor();

                                    this.OnBaseFontChanged();

                                    // MRS 1/27/06 - BR09073
                                    if (imageChanging)
                                        this.BumpCellChildElementsCacheVersion();

                                    break;
                                }
                        }

                        if (fontChanging || imageChanging || imageAlignChanging)
                            this.DirtyGridElement(true);

                        this.cachedCaptionHeight = 0;
                        // Pass this notifcation on to our listeners with the
                        // property id for this appearance property
                        //
                        this.NotifyPropChange(UltraGridLayout.AppearancePropIds[i], propChange);
                        break;
                    }
                }
            }
		}

		#region OnPropertyChanged

		// SSP 6/5/06 BR13306 - App Styling
		// Overrode OnPropertyChanged.
		// 
		/// <summary>
		/// Overridden. Called when a property has changed.
		/// </summary>
		protected override void OnObjectPropChanged( PropChangeInfo propChange )
		{
			if ( null != propChange && propChange.PropId is PropertyIds )
			{
				switch ( (PropertyIds)propChange.PropId )
				{
					case PropertyIds.BorderStyle:
					case PropertyIds.RowConnectorColor:
					case PropertyIds.RowConnectorStyle:
					case PropertyIds.BorderStyleCaption:
					case PropertyIds.FixedHeaderOffImage:
					case PropertyIds.FixedHeaderOnImage:
					case PropertyIds.FixedRowOffImage:
					case PropertyIds.FixedRowOnImage:
					case PropertyIds.FilterDropDownButtonImage:
					case PropertyIds.FilterDropDownButtonImageActive:
					case PropertyIds.SummaryButtonImage:
						StyleUtils.DirtyCachedPropertyVals( this );
						break;
				}
			}

			base.OnObjectPropChanged( propChange );
		}

		#endregion // OnPropertyChanged

		internal void ClearCachedCaptionHeight()
		{
			this.cachedCaptionHeight = 0;
		}


		/// <summary>
		/// Creates and returns a reference to a clone (identical copy) of this object.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoke this method to return a reference to a copy of an object. This is different from setting a variable equal to an object, which does not make a new copy.</p>
		///	<p class="body">This method is also useful when you want to base one object on another. For example, if you wanted one Appearance object to be the same as another, with the exception of several properties, you could clone the existing Appearance object, make changes to the copy, and use it to change an object's appearance.</p>
		///	</remarks>
        /// <returns>A clone (identical copy) of this object.</returns>
		public UltraGridLayout Clone()
		{
			return this.Clone( PropertyCategories.All );
		}

		/// <summary>
		/// Creates and returns a reference to a clone (identical copy) of this object.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoke this method to return a reference to a copy of an object. This is different from setting a variable equal to an object, which does not make a new copy.</p>
		///	<p class="body">This method is also useful when you want to base one object on another. For example, if you wanted one Appearance object to be the same as another, with the exception of several properties, you could clone the existing Appearance object, make changes to the copy, and use it to change an object's appearance.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are copied:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul class="body">
		///		<li>AddNewBox</li>
		///		<li>AlphaBlendEnabled</li>
		///		<li>BorderStyle</li>
		///		<li>BorderStyleCaption</li>
		///		<li>Caption</li>
		///		<li>Enabled</li>
		///		<li>EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul class="body">
		///		<li>Font</li>
		///		<li>InterBandSpacing</li>
		///		<li>MaxColScrollRegions</li>
		///		<li>MaxRowScrollRegions</li>
		///		<li>Override</li>
		///		<li>RowConnectorColor</li>
		///		<li>RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul class="body">
		///		<li>ScrollBars</li>
		///		<li>TabNavigation</li>
		///		<li>TagVariant</li>
		///		<li>ViewStyle</li>
		///		<li>ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be copied by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The UltraGridLayout object supports an additional way to copy from an Layout object, the <b>CopyFrom</b> method.</p>
		///	</remarks>
		/// <param name="propertyCategories">Optional property category</param>
        /// <returns>A clone (identical copy) of this object.</returns>
		public UltraGridLayout Clone( PropertyCategories propertyCategories )
		{
			//create a new layout
			//
			UltraGridLayout newLayout = new UltraGridLayout();

			//initialize from
			//
			newLayout.InitializeFrom( this, propertyCategories );			

			return newLayout;
		}

		/// <summary>
		///  Applies the attributes of an existing UltraGridLayout object to the current UltraGridLayout object, using the property categories specified.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoke this method to copy some or all of an existing UltraGridLayout object's property settings to another UltraGridLayout object. This method does not create a new UltraGridLayout object - it merely copies settings from one object to another.</p>
		///	<p class="body">Invoke the <b>Clone</b> method to make a copy of the current UltraGridLayout object. <b>Clone</b> returns a reference to an UltraGridLayout object, whereas this method does not.</p>
		///	<p class="body">Multiple categories can be copied by combining them using logical <b>Or</b>.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are copied:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///	<td valign="top"><ul class="body">
		///	<li>AddNewBox</li>
		///	<li>AlphaBlendEnabled</li>
		///	<li>BorderStyle</li>
		///	<li>BorderStyleCaption</li>
		///	<li>Caption</li>
		///	<li>Enabled</li>
		///	<li>EstimatedRows</li></ul>
		///	</td>
		///	<td valign="top"><ul class="body">
		///	<li>Font</li>
		///	<li>InterBandSpacing</li>
		///	<li>MaxColScrollRegions</li>
		///	<li>MaxRowScrollRegions</li>
		///	<li>Override</li>
		///	<li>RowConnectorColor</li>
		///	<li>RowConnectorStyle</li></ul>
		///	</td>
		///	<td valign="top"><ul class="body">
		///	<li>ScrollBars</li>
		///	<li>TabNavigation</li>
		///	<li>TagVariant</li>
		///	<li>ViewStyle</li>
		///	<li>ViewStyleBand</li></ul>
		///	</td>
		///	</tr>
		///	</table>
		///	</p>
		///	</remarks>
		/// <param name="source">Source layout object</param>
		public void CopyFrom( UltraGridLayout source )
		{
			this.CopyFrom( source, PropertyCategories.All );
		}
		
		/// <summary>
		///  Applies the attributes of an existing UltraGridLayout object to the current UltraGridLayout object, using the property categories specified.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoke this method to copy some or all of an existing UltraGridLayout object's property settings to another UltraGridLayout object. This method does not create a new UltraGridLayout object - it merely copies settings from one object to another.</p>
		///	<p class="body">Invoke the <b>Clone</b> method to make a copy of the current UltraGridLayout object. <b>Clone</b> returns a reference to an UltraGridLayout object, whereas this method does not.</p>
		///	<p class="body">Multiple categories can be copied by combining them using logical <b>Or</b>.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are copied:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///	<td valign="top"><ul class="body">
		///	<li>AddNewBox</li>
		///	<li>AlphaBlendEnabled</li>
		///	<li>BorderStyle</li>
		///	<li>BorderStyleCaption</li>
		///	<li>Caption</li>
		///	<li>Enabled</li>
		///	<li>EstimatedRows</li></ul>
		///	</td>
		///	<td valign="top"><ul class="body">
		///	<li>Font</li>
		///	<li>InterBandSpacing</li>
		///	<li>MaxColScrollRegions</li>
		///	<li>MaxRowScrollRegions</li>
		///	<li>Override</li>
		///	<li>RowConnectorColor</li>
		///	<li>RowConnectorStyle</li></ul>
		///	</td>
		///	<td valign="top"><ul class="body">
		///	<li>ScrollBars</li>
		///	<li>TabNavigation</li>
		///	<li>TagVariant</li>
		///	<li>ViewStyle</li>
		///	<li>ViewStyleBand</li></ul>
		///	</td>
		///	</tr>
		///	</table>
		///	</p>
		///	</remarks>
		/// <param name="source">Source layout object</param>
		/// <param name="propertyCategories">Optional property category</param>
		public void CopyFrom( UltraGridLayout source, PropertyCategories propertyCategories )
		{
			//	We have to save the current Key, and restore it after
			//	the copy operation is complete, because
			//	the source layout's Key should not be copied to the target's
			//
			string oldKey = this.Key;

			if ( source != null )
			{
				// SSP 8/19/04
				// Suspend calc manager related activities.
				//
				this.SuspendCalcManagerNotifications( );

				//Initialize from and set key back
				//
				this.initializingAllSubItems = true;

				try
				{
					this.InitializeFrom( source, propertyCategories );
				}
				finally
				{
					this.initializingAllSubItems = false;

					// SSP 8/19/04
					// Resume calc manager related activities.
					//
					this.ResumeCalcManagerNotifications( );
				}
				
				this.Key = oldKey;
			}

		}

		internal void ClipToParent( ref Rectangle rectToClip )
		{
			this.ClipToParent( ref rectToClip, false );
		}

		internal void ClipToParent( ref Rectangle rectToClip, bool clientCoords  )
		{		
			Rectangle parentRect;

			// JJD 9/26/01
			// Use ControlForGridDisplay to support the UltraCombo
			// where a differnet control is used for the dropdown
			// display
			//
			//			parentRect = this.Grid.Bounds; 
			parentRect = this.Grid.ControlForGridDisplay.Bounds; 

			// AS - 10/4/01
			// the bounds is in relation to its container so if we are passed in
			// coordinates in client coordinates, we need to adjust the location
			// of the parent rect so the origin base is the same as the rectToClip
			// passed in.
			if (clientCoords)
			{
				Point location = this.Grid.ControlForGridDisplay.Location;
				parentRect.Offset(-location.X, -location.Y);
			}
						
			rectToClip = Rectangle.Intersect(parentRect, rectToClip);						
		}
		
		internal bool SynchronizedColListsDirty
		{
			
			get
			{
				return this.synchronizedColListsDirty;
			}

			set
			{

				if ( null != this.grid )
				{
					if ( this.grid.InInitializeLayout )
					{
						this.synchronizedColListsDirty = true;
						return;
					}
				}

				if ( !value	||
					(this.colScrollRegions != null	&&
					!this.colScrollRegions.InitializingMetrics ) )
				{
					this.synchronizedColListsDirty =  true;
				}
			}
		}
		internal ColScrollRegionsCollection GetColScrollRegions( bool initializeMetrics )
		{
			
			if ( initializeMetrics				&&
				null != this.grid				&&
				this.grid.IsHandleCreated		&&
				!this.grid.FirstDraw			&&
				!this.grid.InInitializeLayout )
				this.ColScrollRegions.InitializeMetrics();

			return this.ColScrollRegions;
		}

		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//	
		#region Commented out IScrollableRowCountManagerOwner implementation
		
		

		#endregion // Commented out IScrollableRowCountManagerOwner implementation

		internal int ScrollableRowCountVersion
		{
			get
			{
				return this.scrollableRowCountVersion;
			}
		}

		// SSP 3/19/02
		// Added a version number for causing all the scrollable row count managers
		// in the hierarchy to recalcuate their row counts. This is used for row filtering
		// as rows in rows collections get filtered out, we need to recalcuate the 
		// row counts for all the associated scrollable row count managers.
		// Added following 2 methods
		//
		internal void BumpScrollableRowCountVersion( )
		{
			this.scrollableRowCountVersion++;
			this.BumpGrandVerifyVersion( );
			this.DirtyGridElement( true );
		}

		internal UIElement GetUIElement( bool verify )
		{
			return this.GetUIElement(verify, false);
		}

		// SSP 10/23/03 UWG2001
		// Added EnsureUIElementRectInitialized method.
		//
		internal void EnsureUIElementRectInitialized( )
		{
			Infragistics.Win.UIElement element = this.UIElement;

			if ( null != element && 
				element.Rect != new Rectangle( new Point( 0, 0 ), this.grid.ControlForGridDisplay.Size ) )
				this.GetUIElement( true, true );
		}
		
		internal UIElement GetUIElement( bool verify, bool forceInitializeRect   )
		{
			Infragistics.Win.UIElement element = this.UIElement;

			// JJD 10/29/01
			// If the forceInitializeRect is true
			// set the main element's rect
			//
			if ( forceInitializeRect )
			{
				// SSP 7/8/02 UWG1249
				// We should be setting the size of the main element to the size
				// of the control for display because in the case of the ultradropdown
				// and ultracombo grid is not the actual control that the data is 
				// displayed in, it's actually a separate control that's embedded
				// in the drop down form.
				// 
				//element.Rect = new Rectangle( new Point( 0, 0 ), this.grid.Size );
				if ( null != this.grid && null != this.grid.ControlForGridDisplay )
					// SSP 12/17/03 UWG2799
					// Moved the code into InternalInitializeRect method.
					//
					//element.Rect = new Rectangle( new Point( 0, 0 ), this.grid.ControlForGridDisplay.Size );
					((UltraGridUIElement)element).InternalInitializeRect( true );
			}

			// JJD 10/29/01
			// If the verify is true then call VerifyChildElements
			//
			if ( verify )
				element.VerifyChildElements( true );

			return element;
		}
		
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal ColScrollRegion ActiveColScrollRegion
		{
			get
			{
				// if we are hooked up to a grid then defer to it
				//
				if ( null != this.grid )
					return this.grid.ActiveColScrollRegion;

				// get the col scroll region collection
				//
				ColScrollRegionsCollection colRegions = this.ColScrollRegions;

				Debug.Assert( null != colRegions, "No col scroll region collection" );

				if ( null == colRegions )
					return null;

				Debug.Assert( colRegions.Count > 0, "No col scroll regions in collection" );

				if ( colRegions.Count < 1 )
					return null;

				// return the first one in the collection
				//
				return colRegions.DefaultActiveRegion;
			}

		}
		
		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal RowScrollRegion ActiveRowScrollRegion
		{
			get
			{
				// if we are hooked up to a grid then defer to it
				//
				if ( null != this.grid )
					return this.grid.ActiveRowScrollRegion;

				// get the row scroll region collection
				//
				RowScrollRegionsCollection rowRegions = this.RowScrollRegions;

				Debug.Assert( null != rowRegions, "No row scroll region collection" );

				if ( null == rowRegions )
					return null;

				Debug.Assert( rowRegions.Count > 0, "No row scroll regions in collection" );

				if ( rowRegions.Count < 1 )
					return null;

				// return the first one in the collection
				//
				return rowRegions.DefaultActiveRegion;
			}

		}

		internal ColScrollRegion ValidateColRegionParam( ColScrollRegion colRegionParam )
		{		
			ColScrollRegion returnColScrollRegion = null;

			if ( null != colRegionParam )
			{				
				returnColScrollRegion = colRegionParam;
				// make sure the column scrolling region is one of ours
				//
				if ( returnColScrollRegion.Layout != this )
				{
					throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_175"));
				}
				this.GetColScrollRegions( true );
			}
			else
			{
				returnColScrollRegion = this.GetActiveColScrollRegion( true );
			}

			if (null == returnColScrollRegion )
			{
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_176"));
			}
		
			return returnColScrollRegion;
		}				 
		internal RowScrollRegion ValidateRowRegionParam( RowScrollRegion rowScrollRegionParam )
		{
			
			RowScrollRegion rowScrollRegionReturn = null;

			if ( null != rowScrollRegionParam )
			{	
				rowScrollRegionReturn = rowScrollRegionParam;

				// make sure the Row scrolling region is one of ours
				//			
				if ( rowScrollRegionReturn.Layout != this )
					throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_177"));
			}
			else
			{				
				rowScrollRegionReturn = this.grid.ActiveRowScrollRegion;				
			}
			
			if ( null == rowScrollRegionReturn )
			{				
				throw new InvalidOperationException(Shared.SR.GetString("LE_InvalidOperationException_178"));
			}

			return rowScrollRegionReturn;
		}

		internal bool ScrollColIntoViewPending
		{
			get
			{
				return this.scrollColIntoViewPending;
			}
			set
			{
				this.scrollColIntoViewPending = value;			
			}			
		}

		// SSP 6/24/02
		//
		internal bool CanMergeAdjacentBorders( Infragistics.Win.UIElementBorderStyle borderStyle1, 
			Infragistics.Win.UIElementBorderStyle borderStyle2 )
		{
			return this.CanMergeAdjacentBorders( borderStyle1 ) && 
				( borderStyle1 == borderStyle2 || this.CanMergeAdjacentBorders( borderStyle2 ) );
		}

		internal bool CanMergeAdjacentBorders( Infragistics.Win.UIElementBorderStyle borderStyle )
		{
						
			switch ( borderStyle )
			{
					// SSP 7/24/03 - Optimizations
					// Moved the case from the very end of the switch statement to here
					// since it's the most common border especially in the grid with the cells.
					//
				case Infragistics.Win.UIElementBorderStyle.Solid :
					return true;
				case UIElementBorderStyle.InsetSoft:
				case UIElementBorderStyle.RaisedSoft:
				case UIElementBorderStyle.Inset:
				case UIElementBorderStyle.Raised:
					// JJD 11/06/01
					// Added support for new FlatMode property
					//
					// AS 3/20/06 AppStyling
					//if ( this.IsPrintLayout || ( this.grid != null && this.grid.FlatMode ) )
					if ( this.IsPrintLayout || ( this.grid != null && this.grid.UseFlatModeResolved ) )
						return true;
					break;

				case Infragistics.Win.UIElementBorderStyle.Default :
				case Infragistics.Win.UIElementBorderStyle.Dotted :
					
				case Infragistics.Win.UIElementBorderStyle.Dashed :
					// SSP 7/24/03 - Optimizations
					// Moved the Solid case from the very end of the switch statement to the 
					// beginning of the switch statement since it's the most common border
					// especially in the grid with the cells.
					//
				//case Infragistics.Win.UIElementBorderStyle.Solid /*ssBorderStyleSolidLine*/:
					return true;
			}
						
			return false;			
		}


		internal int GetButtonThickness( Infragistics.Win.UIElementButtonStyle buttonStyle )
		{
			
			return 3;
		}

		internal int GetBorderThickness( Infragistics.Win.UIElementBorderStyle borderStyle )
		{
            // MBS 9/19/07 - BR26421
            // Reworked logic to call to account for larger border styles
            //
            #region Old Code

            ////int nThickness = 0;
            //int thickness = 0;

            //switch ( borderStyle )
            //{
            //    case Infragistics.Win.UIElementBorderStyle.Default /*ssBorderStyleDefault*/:
            //    case Infragistics.Win.UIElementBorderStyle.Dotted /*ssBorderStyleSmallDots*/:
            //        /*case ssBorderStyleLargeDots:*/
            //    case Infragistics.Win.UIElementBorderStyle.Dashed /*ssBorderStyleDashes*/:
            //    case Infragistics.Win.UIElementBorderStyle.Solid /*ssBorderStyleSolidLine*/:
            //    case Infragistics.Win.UIElementBorderStyle.RaisedSoft /*ssBorderStyleRaisedSoft*/:
            //    case Infragistics.Win.UIElementBorderStyle.InsetSoft /*ssBorderStyleInsetSoft*/:
            //        thickness  = 1;
            //        break;
            //    case Infragistics.Win.UIElementBorderStyle.Raised /*ssBorderStyleRaised*/:
            //    case Infragistics.Win.UIElementBorderStyle.Inset /*ssBorderStyleInset*/:
            //    case Infragistics.Win.UIElementBorderStyle.Etched:
            //    {
            //        // JJD 11/06/01
            //        // Added support for new flat mode property
            //        //
            //        // AS 3/20/06 AppStyling
            //        //if ( this.IsPrintLayout || ( this.grid != null && this.grid.FlatMode ) )
            //        if ( this.IsPrintLayout || ( this.grid != null && this.grid.UseFlatModeResolved ) )
            //            thickness = 1;
            //        else
            //            thickness  = 2;
            //        break;	
            //    }
            //}

            //return thickness;

            #endregion //Old Code
            //
            switch (borderStyle)
            {
                // MRS 3/11/2008 - BR31006
                case Infragistics.Win.UIElementBorderStyle.Default:
                    return 1;

                case Infragistics.Win.UIElementBorderStyle.Raised:
                case Infragistics.Win.UIElementBorderStyle.Inset:
                case Infragistics.Win.UIElementBorderStyle.Etched:
                    {
                        // JJD 11/06/01
                        // Added support for new flat mode property
                        //
                        // AS 3/20/06 AppStyling
                        //if ( this.IsPrintLayout || ( this.grid != null && this.grid.FlatMode ) )
                        if (this.IsPrintLayout || (this.grid != null && this.grid.UseFlatModeResolved))
                            return 1;

                        break;
                    }
            }
            return Infragistics.Win.DrawUtility.CalculateBorderWidths(borderStyle, Border3DSide.Left, this.grid != null ? this.grid.UseFlatModeResolved : false).Left;    
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal System.Drawing.Size SizeOfImages
		{
			get
			{
				
				//if ( m_pGrid )
				// return m_pGrid->GetSizeOfImages();
				if ( null != this.grid )
					return this.grid.GetSizeOfImages();

				//SIZE sizImages = {0,0};
				System.Drawing.Size sizImages = new System.Drawing.Size( 0,0 );

				return sizImages;		
			}
		}	

		internal int RowScrollRange
		{
			get
			{
				// SSP 2/13/04 - Virtual Mode - Optimization
				// Updated the logic that calculates the scroll positions/counts.
				//
				// ------------------------------------------------------------------------------
				//return ((IScrollableRowCountManagerOwner)this).ScrollableRowCountManager.ScrollableRowCount;
				
				return null != this.Rows ? this.Rows.ScrollCount : 0;
				// ------------------------------------------------------------------------------				
			}
		}

		internal UltraGridBand BandZero
		{
			get
			{				
				return ( null != this.sortedBands && this.sortedBands.Count > 0 )
					? this.sortedBands[0] 
					: null;				
			}
		}
		internal ColScrollRegion GetActiveColScrollRegion( bool initializeMetrics )
		{		
			// if we are hooked up to a grid then defer to it
			//
			if ( null != this.grid )
				return this.grid.GetActiveColScrollRegion( initializeMetrics );

			// get the Column scroll region collection
			//
			ColScrollRegionsCollection csrColl = this.GetColScrollRegions( true );

			Debug.Assert ( null != csrColl, "No Col scroll regions collection" );

			if ( null == csrColl )
				return null;

			Debug.Assert( csrColl.Count > 0, "No Col scroll regions in collection" );

			if ( csrColl.Count < 1 )
				return null;

			// return the first one in the collection
			//
			return csrColl.DefaultActiveRegion;
		}
		

		internal UltraGridRow GetRowAtScrollPos( int pos )
		{
			// SSP 2/13/04 - Virtual Mode - Optimization
			// Updated the logic that calculates the scroll positions/counts.
			//
			// ------------------------------------------------------------------------------
			return null != this.Rows ? this.Rows.GetRowAtScrollIndex( pos ) : null;
			
			
			// ------------------------------------------------------------------------------
		}

		internal int MinRowHeight
		{
			get
			{
				int minHeight = 10;
				
				BandsCollection bands = this.sortedBands;

				if ( bands.Count > 0 )
				{
					//BAND_VECTOR::const_iterator BandIterator = pBands->begin();
					int i = 0;


					// set the min height to the first band's min row height
					//
					minHeight = bands[i].GetMinRowHeight();

					// bump the band iterator to point to the next band
					//
					//BandIterator++;
					i++;

					// loop thru all secondary bands comparing their
					// min row heights
					//
					for ( ; i < bands.Count; i++ )
					{
						if ( bands[i].HiddenResolved )
							minHeight = System.Math.Min( minHeight, bands[i].GetMinRowHeight() );
					}
				}

				return minHeight;
			}
		}
		
		Font FontDefault
		{
			get
			{
				Font font = null;
				if ( null != this.grid )
				{
					font = this.grid.Font;
				}
				return font;
			}
		}

		internal void OnBaseFontChanged()
		{
			this.cachedFontHeight = 0;
			this.cachedBaseFontHeight = 0;

			// SSP 3/30/05
			// Don't force creation of bands.
			//
			//this.Bands.OnBaseFontChanged();
			if ( null != this.bands )
				this.bands.OnBaseFontChanged();
		}

		internal int DefaultFontHeight
		{
			get
			{
				// JJD 12/13/01
				// Added logic to cache the font
				//
				if ( this.cachedFontHeight < 1 )
				{
					// SSP 3/16/06 - App Styling
					// Also resolve the app-style appearance settings.
					// 
					// ------------------------------------------------------------------------------
					AppearanceData appData = new AppearanceData( );
					AppearancePropFlags flags = AppearancePropFlags.FontData;
					this.MergeLayoutAppearances( ref appData, ref flags, 
						LayoutAppearanceIndex.Default, StyleUtils.GetControlAreaRole( this ), AppStyling.RoleState.Normal );

					this.cachedFontHeight = this.CalculateFontHeight( ref appData );

                    
					// ------------------------------------------------------------------------------
				}

				return this.cachedFontHeight;
			}
		}

		internal int CalculateFontHeight( ref AppearanceData appData  )
		{
			Font font = this.FontDefault;

			if ( font == null )
			{
                // MBS 12/29/08 - TFS11941
                // There are cases where we won't have an initialized font, like if we're cloning a 
                // layout and there is no associated grid
                //
				//Debug.Fail("Default font hasn't been initialized");
                Debug.Assert(grid == null, "Default font hasn't been initialized");

				return 20;
			}			

			Font createdFont = null;
	
			// if there is a font override then create a temp font
			//
			if ( appData.HasFontData )
			{
				createdFont = appData.CreateFont( font );

				if ( createdFont != null )
					font = createdFont;
			}

			// call CalculateFontHeight with the font
			//
			int fontHeight = this.CalculateFontHeight( font );

			// dispose of the font created above
			//
			if ( createdFont != null )
				createdFont.Dispose();	

			return fontHeight;
		}

		internal System.Drawing.Size CalculateFontSize( ref AppearanceData appData, string text )
		{
			return this.CalculateFontSize( ref appData, text, -1 );
		}

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		internal System.Drawing.Size CalculateFontSize( ref AppearanceData appData, string text, TextOrientationInfo textOrientation )
		{
			return this.CalculateFontSize( ref appData, text, -1, textOrientation );
		}

		// SSP 3/30/05 - NAS 5.2
		// Added an overload of CalculateFontSize that takes in maxWidth paramter.
		//
		internal System.Drawing.Size CalculateFontSize( ref AppearanceData appData, string text, int maxWidth )
		{
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Moved all code to the new overload of CalculateFontSize
			return this.CalculateFontSize( ref appData, text, -1, null );
		}

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		// Added TextOrientationInfo parameter 
		internal System.Drawing.Size CalculateFontSize( ref AppearanceData appData, string text, int maxWidth, TextOrientationInfo textOrientation )
		{
			Font font = this.FontDefault;

			if ( font == null )
			{
				Debug.Fail("Default font hasn't been initialized");
				return new System.Drawing.Size(20,20);
			}			

			Font createdFont = null;
	
			// if there is a font override then create a temp font
			//
			if ( appData.HasFontData )
			{
				createdFont = appData.CreateFont( font );

				if ( createdFont != null )
					font = createdFont;
			}

			// call CalculateFontHeight with the font
			//
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Pass off the text orientation when measuring text
			//Size fontSize = this.CalculateFontSize( font, text, maxWidth );
			Size fontSize = this.CalculateFontSize( font, text, maxWidth, textOrientation );

			// dispose of the font created above
			//
			if ( createdFont != null )
				createdFont.Dispose();	

			return fontSize;
		}

		internal int CalculateFontHeight( Font font )
		{
			// JJD 12/14/01
			// Moved logic into CalculateFontSize. A null string will
			// use the default string of "lWg" and cache the height
			//
			Size stringSize = this.CalculateFontSize( font, null );

			return stringSize.Height;
		}

		internal System.Drawing.Size CalculateFontSize( Font font, string text )
		{
			return this.CalculateFontSize( font, text, -1 );
		}

		// SSP 3/30/05 - NAS 5.2
		// Added an overload of CalculateFontSize that takes in maxWidth paramter.
		//
		internal System.Drawing.Size CalculateFontSize( Font font, string text, int maxWidth )
		{
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Moved code to new overload of CalculateFontSize
			return this.CalculateFontSize( font, text, maxWidth, null );
		}

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		// Added TextOrientationInfo parameter
		internal System.Drawing.Size CalculateFontSize( Font font, string text, int maxWidth, TextOrientationInfo textOrientation )
		{
			Font baseFont = this.FontDefault;

			// JJD 12/14/01
			// Added caching logic for the base font 
			//
			if ( text == null && this.cachedBaseFontHeight > 0 )
			{
				if ( font == baseFont || font == null )
					return new Size( 0, this.cachedBaseFontHeight );
			}

			if ( null == font )
			{
				// if a valid font wasn't passed in use the default
				// font for the layout
				//
				font = baseFont;
			}

			// SSP 10/2/01 UWG342
			// ToHfont forces the creattion of font handle and the framework
			// does not seem to be disposing of the allocated resources
			// necessary to create the font handle, causing eventual display
			// corruption
			// Besides it's not necessary to check for the font handle being
			// null. font reference should be enough
			//
			if ( null == font )
			{
				Debug.Fail("Default font is null");
				return new Size( 20, 20 );
			}
			

			// JJD 8/28/01
			// Use FromHwnd(0) to create a graphics object instead of
			// the control's CreateGraphics() since the latter forces
			// the window to be created (possibly too early in the 
			// process
			//
			// JJD 10/23/01
			// Call CreateReferenceGraphics which will create the least 
			// disruptive graphics object based on the the permissions
			// of the assembly
			//
			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			//Graphics gr = DrawUtility.CreateReferenceGraphics( this.grid );
			// MRS 6/1/04 - UWG2915
			//Graphics gr = DrawUtility.GetCachedGraphics( this.grid );
			Graphics gr = this.GetCachedGraphics();

			// JJD 121/14/01
			// Use Size.Ceiling so we always round up
			//
            //  BF 10.18.04 NAS2005 Vol1 - GDI Text Rendering
			//Size stringSize = Size.Ceiling( gr.MeasureString( text == null ? "lWg" : text, font ) );			
			// SSP 3/30/05 - NAS 5.2
			// Added an overload of CalculateFontSize that takes in maxWidth paramter.
			//
			//Size stringSize = Size.Ceiling( DrawUtility.MeasureString( gr, text == null ? "lWg" : text, font ) );			
			// SSP 11/4/05 BR07555
			// Need to pass printing param when measuring in Whidbey. When printing,
			// in Whidbey we need use the GDI+ measuring.
			// 
			// --------------------------------------------------------------------------------------
			
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Pass off the text orientation
			//Size stringSize = this.MeasureString( gr, text == null ? "lWg" : text, font, maxWidth );
			Size stringSize = this.MeasureString( gr, text == null ? "lWg" : text, font, maxWidth, null, textOrientation );
			// --------------------------------------------------------------------------------------
			
			// AS 8/12/03 optimization - Use the new caching mechanism.
			//
			//gr.Dispose();
			// MRS 6/1/04 - UWG2915
			//DrawUtility.ReleaseCachedGraphics(gr);
			this.ReleaseCachedGraphics(gr);
			
			// JJD 1/11/02
			// Only cache the font height if the font is the basefont
			//
			if ( text == null && font == baseFont )
				this.cachedBaseFontHeight = stringSize.Height;

			return stringSize;
        }

        #region MeasureString

		internal Size MeasureString( Graphics gr, string text, Font font )
		{
			return this.MeasureString( gr, text, font, 0 );
		}

		internal Size MeasureString( Graphics gr, string text, Font font, int maxWidth )
		{
			return Size.Ceiling( this.MeasureStringF( gr, text, font, maxWidth ) );
		}

		internal Size MeasureString( Graphics gr, string text, Font font, StringFormat sf )
		{
			return this.MeasureString( gr, text, font, 0, sf );
		}

        internal Size MeasureString( Graphics gr, string text, Font font, int maxWidth, StringFormat sf )
        {
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Moved code to new overload of MeasureString
			return this.MeasureString( gr, text, font, maxWidth, sf, null );
		}

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		// Added TextOrientationInfo parameter
		internal Size MeasureString( Graphics gr, string text, Font font, int maxWidth, StringFormat sf, TextOrientationInfo textOrientation )
		{
			return Size.Ceiling( this.MeasureStringF( gr, text, font, maxWidth, sf, textOrientation ) );
        }

        #endregion // MeasureString

        #region MeasureStringF

        internal SizeF MeasureStringF( Graphics gr, string text, Font font )
        {
            return this.MeasureStringF( gr, text, font, 0 );
        }

        internal SizeF MeasureStringF( Graphics gr, string text, Font font, int maxWidth )
        {
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Pass off null as the new TextOrientationInfo parameter
            //return this.MeasureStringF( gr, text, font, maxWidth, null );
			return this.MeasureStringF( gr, text, font, maxWidth, null, null );
        }

		internal SizeF MeasureStringF( Graphics gr, string text, Font font, StringFormat sf )
		{
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Pass off null as the new TextOrientationInfo parameter
			//return this.MeasureStringF( gr, text, font, 0, sf );
			return this.MeasureStringF( gr, text, font, 0, sf, null );
		}

		// MD 4/17/08 - 8.2 - Rotated Column Headers
		// Added TextOrientationInfo parameter
		//internal SizeF MeasureStringF( Graphics gr, string text, Font font, int maxWidth, StringFormat sf )
		internal SizeF MeasureStringF( Graphics gr, string text, Font font, int maxWidth, StringFormat sf, TextOrientationInfo textOrientation )
		{
			GdiDrawStringFlags gdiDrawFlags = GdiDrawStringFlags.None;

            if ( this.IsPrintLayout )
				gdiDrawFlags |= GdiDrawStringFlags.IsPrinting;

			TextRenderingMode mode = this.grid.TextRenderingMode;
			if ( TextRenderingMode.GDIPlus == mode )
				gdiDrawFlags |= GdiDrawStringFlags.GDIPlus;
			else if ( TextRenderingMode.GDI == mode )
				gdiDrawFlags |= GdiDrawStringFlags.GDI;
			
			// MD 4/17/08 - 8.2 - Rotated Column Headers
			// Refactored and passed off text orientation to new DrawString method
			//if ( null != sf )
			//{
			//    return maxWidth > 0
			//        ? DrawUtility.MeasureString( gr, text, font, maxWidth, sf, gdiDrawFlags )
			//        : DrawUtility.MeasureString( gr, text, font, PointF.Empty, sf, gdiDrawFlags );
			//}
			//else
			//{
			//    return maxWidth > 0
			//        ? DrawUtility.MeasureString( gr, text, font, maxWidth, gdiDrawFlags )
			//        : DrawUtility.MeasureString( gr, text, font, gdiDrawFlags );
			//}
			RectangleF layoutBounds = new RectangleF( 0, 0, maxWidth, 0 );

			if ( maxWidth < 0 )
				layoutBounds.Width = 0;

			DrawStringParameters drawStringParameters = new DrawStringParameters(
				gr, 
				text, 
				font, 
				null,
				layoutBounds, 
				sf, 
				gdiDrawFlags, 
				textOrientation );

			return DrawUtility.MeasureString( ref drawStringParameters );
        }

        #endregion // MeasureStringF

		internal Rectangle ClientRect
		{
			get
			{
				if ( null == this.grid || 
					!this.grid.Created ||
					!this.grid.Visible )
					throw new InvalidOperationException( Shared.SR.GetString("LE_InvalidOperationException_179"));

				return this.grid.ClientRectangle;
			}
		}

		// SSP 4/5/02
		// This inetrnal propery was not being used anywhere, so commented it out.
		//
		

		

		// SSP 4/26/02
		// EM Embeddable editors.
		//
		


		internal bool Snaking
		{
			get
			{
				return this.snaking;
			}

			set
			{
				if ( this.snaking != value )
					this.snaking = value;
			}

		}


		// SSP 4/26/02
		// EM Embeddable editors.
		// Commented out CreateEdit.
		//
		

		// SSP 4/29/02
		// EM Embeddable editors. 
		// Don't need ClipTextBoxControl anymore since positioning and repositioning
		// of the edit controls will be done by the embeddable editors.
		//
		

		// SSP 4/26/02
		// EM Embeddable editors. 
		// Commented out EditControl since it's obsoleted by embeddable editors.
		//
		
		
		// SSP 4/26/02
		// Em Embeddable editors
		// Commented below code out.		
		

		// SSP 4/29/02
		// EM Embeddable editors. 
		// Don't need PositionTextBoxCtl anymore since positioning and repositioning
		// of the edit controls will be done by the embeddable editors.
		//
		

		internal bool TextControlPositionDirty
		{
			get
			{
				return this.textControlPositionDirty;
			}
			set
			{
				this.textControlPositionDirty = value;
			}
		}

		internal bool DontUpdateWhenSelecting
		{
			get
			{
				return this.dontUpdateWhenSelecting;
			}
			set
			{
				this.dontUpdateWhenSelecting = value;
			}
		}

		internal bool PerformAction( UltraGridAction actionCode, bool shiftKeyDown, bool ctlKeyDown)
		{
			UltraGrid gridctl = this.Grid as UltraGrid;

			switch ( actionCode )
			{
				// SSP 11/22/05 - NAS 6.1 Multi-cell Operations Support
				// 
				// --------------------------------------------------------------------------
				case UltraGridAction.Copy:
					return this.PerformMultiCellOperation( MultiCellOperation.Copy );
				case UltraGridAction.Cut:
					return this.PerformMultiCellOperation( MultiCellOperation.Cut );
				case UltraGridAction.DeleteCells:
					return this.PerformMultiCellOperation( MultiCellOperation.Delete );
				case UltraGridAction.Paste:
					return this.PerformMultiCellOperation( MultiCellOperation.Paste );
				case UltraGridAction.Redo:
					return this.PerformMultiCellOperation( MultiCellOperation.Redo );
				case UltraGridAction.Undo:
					return this.PerformMultiCellOperation( MultiCellOperation.Undo );
				// --------------------------------------------------------------------------
				case UltraGridAction.ExpandRow:
				{
					if ( this.ActiveRow != null )
					{
						this.ActiveRow.Expanded = true;
					}
					return true;
				}

				case UltraGridAction.CollapseRow:
				{
					if ( this.ActiveRow != null )
					{
						this.ActiveRow.Expanded = false;
					}
					return true;
				}


				case UltraGridAction.DeleteRows:
				{
					// Use InternalDeleteSelectedRows so that we
					// can check if deletions are allowed
					//					
					this.DeleteSelectedRows( true );
					return true;
				}

				case UltraGridAction.UndoCell:
				{
					if ( this.ActiveCell != null )
					{
						this.ActiveCell.CancelUpdate();
						return true;
					}
					break;
				}
				
				case UltraGridAction.UndoRow:
				{
					if ( this.ActiveRow != null )
					{
						this.ActiveRow.CancelUpdate();
						return true;
					}
					break;
				}

				case UltraGridAction.ToggleEditMode:
				{
					if ( this.ActiveCell != null )
						this.ActiveCell.ToggleEditMode();
					return true;
				}

				case UltraGridAction.EnterEditMode:
				{
					if ( this.ActiveCell != null )
						this.ActiveCell.EnterEditMode( false );
					return true;
				}

				case UltraGridAction.ExitEditMode:
				{
					if ( this.ActiveCell != null )
						this.ActiveCell.ExitEditMode();
					return true;
				}

				case UltraGridAction.EnterEditModeAndDropdown:
				{
					if ( this.ActiveCell != null )
					{
						// if not cancelled, drop down
						if ( !this.ActiveCell.EnterEditMode( false, true ) )
							this.ActiveCell.ShowDropDown();
					}
					return true;
				}

				case UltraGridAction.ToggleDropdown:
				{
					if ( this.ActiveCell != null )
						this.ActiveCell.ToggleDropDown();
					return true;
				}

				case UltraGridAction.CloseDropdown:
				{
					if ( this.ActiveCell != null )
						this.ActiveCell.HideDropDown();					
					return true;
				}

				case UltraGridAction.NextRegion:
				{
					this.ActivateNextRegion();
					return true;
				}

				case UltraGridAction.PrevRegion:
				{
					this.ActivatePreviousRegion();
					return true;
				}

				case UltraGridAction.DeactivateCell:
				{
					if ( gridctl == null )
						return false;

					gridctl.ActiveCell = null;
					
					return true;
				}

				case UltraGridAction.ActivateCell:
				{
					if ( gridctl == null )
						return false;

					// SSP 10/25/04 UWG3669
					// If the row is a group-by row then don't do any thing. Group-by rows don't have cells.
					//
					//if ( this.ActiveRow != null )
					if ( this.ActiveRow != null && ! this.ActiveRow.IsGroupByRow )
					{
						UltraGridCell newCell = null;

						// get the first/last visible col for this band
						//
						UltraGridColumn newCol = this.ActiveRow.Band.GetFirstVisibleCol( this.grid.ActiveColScrollRegion, false );

						if ( newCol != null )
						{
							// JJD 8/20/01
							// Use newCol instead of this.ActiveCell.Column since
							// active cell can be null
							//
							newCell = this.ActiveRow.Cells[newCol];
							//							newCell = this.ActiveRow.Cells[this.ActiveCell.Column];

							while ( newCell != null && newCell.IsDisabled  && newCell != this.ActiveCell )
							{
								newCell = this.GetNextCell( true, this.ActiveRow, newCell, 
									shiftKeyDown, ctlKeyDown, false, false, false );
							}
						}
						
						if ( newCell != null )
						{
							gridctl.ActiveCell = newCell;

							// only update if action succeeded
							if ( newCell == this.ActiveCell )
								this.grid.Update(); 
						}
					}

					return true;
				}

				case UltraGridAction.ToggleCheckbox:
				{
					// SSP 5/13/02 UWG1106
					// Only allow toggling if the UltraGridCell.CanBeModified returns true
					// which takes into account activation settings and things like that.
					//
					//if ( this.ActiveCell != null )
					if ( this.ActiveCell != null && this.ActiveCell.CanBeModified )
						this.ActiveCell.ToggleCheckBox();

					return true;
				}

				// SSP 4/23/05 - NAS 5.2 Filter Row/Fixed Add Row
				// Added CommitRow action. This is for applying filters when Enter key is 
				// pressed while the filter row is active depending on the 
				// FilterEvaluationTrigger settings. Also it's used for committing the
				// add-row. It can be used to commit regular rows as well however for that
				// the developer has to add a key action mapping.
				//
				case UltraGridAction.CommitRow:
				{
					UltraGridRow activeRow = this.ActiveRow;

					if ( activeRow is UltraGridFilterRow )
					{
						UltraGridFilterRow filterRow = (UltraGridFilterRow)activeRow;
						filterRow.ApplyFilters( );
						return true;
					}
					// SSP 6/7/05 BR04462
					// Only commit the template add-row if it was modified. Note that this can not
					// be taken care of via the key action mappings unless another state is added
					// or the meaning of the existing RowDirty state is changed. Current RowDirty 
					// state is true when the row is modified or it's an add-row.
					// 
					//else if ( null != activeRow )
					else if ( null != activeRow 
						      && ( activeRow != activeRow.ParentCollection.CurrentAddRow 
								   || activeRow.ParentCollection.CurrentAddRowModifiedByUser ) )
					{
						// Exit the edit mode first.
						// 
						UltraGridCell activeCell = this.ActiveCell;
						if ( null != activeCell && activeCell.IsInEditMode )
							activeCell.ExitEditMode( false, false );

						// Only update the row if the exit edit mode operation succeeded.
						//
						activeCell = this.ActiveCell;
						if ( null == activeCell || ! activeCell.IsInEditMode )
						{
							bool activateTemplateAddRow = activeRow.IsAddRowFromTemplate;

							activeRow.Update( );

							// When the user hits enter on the fixed add-row commit the current add-row
							// and after doing so activate the fixed template add-row so the user can 
							// add multiple rows with ease. This is how it works in Outlook.
							//
							activeCell = this.ActiveCell;
							if ( activateTemplateAddRow && activeRow == this.ActiveRow 
								&& TemplateAddRowLocation.None != activeRow.ParentCollection.TemplateRowLocationResolved
								&& activeRow.AllAncestorsExpanded 
								&& ( null == activeCell || ! activeCell.IsInEditMode ) )
							{
								activeRow.ParentCollection.AddTemplateAddRowToDataList( true );
							}

							return true;
						}
					}

					return false;
				}

					
					//Action is intended for selection
				default:
				{
					bool beginUpdateCalled = false;
					bool updateCtrl = false;

					try 
					{
						// RobA 12/26/01 UWG869 
						// if this is a Page action then hold off updating until the end
						//
						if ( !gridctl.IsUpdating)
						{
							if ( actionCode == UltraGridAction.PageUpRow || 
								actionCode == UltraGridAction.PageDownRow ||
								actionCode == UltraGridAction.PageUpCell || 
								actionCode == UltraGridAction.PageDownCell	)
							{
								// SSP 11/16/06 - NAS 6.3 - Optimizations - BR13748 BR14522
								// Don't call BeginUpdate and EndUpdate unless there is going to be 
								// some scrolling. Otherwise calling EndUpdate will cause the grid to
								// be invalidate.
								// ------------------------------------------------------------------------
								//beginUpdateCalled = true;
								//gridctl.BeginUpdate();
								bool willScrollFlag = false;
								if ( UltraGridAction.PageUpRow == actionCode || UltraGridAction.PageUpCell == actionCode )
									willScrollFlag = !this.ActiveRowScrollRegion.IsFirstScrollableRowVisible( );
								else
									willScrollFlag = !this.ActiveRowScrollRegion.IsLastScrollableRowVisible( );

								if ( willScrollFlag )
								{
									beginUpdateCalled = true;
									gridctl.BeginUpdate( );
								}
								// ------------------------------------------------------------------------
							}
						}

						bool wasInEditMode = ( this.ActiveCell != null && this.ActiveCell.IsInEditMode );
						UltraGridBand lastEditBand = this.ActiveCell != null ? this.ActiveCell.Band : null;

						//based on the action code get a selectable item
						GridItemBase item = GetSelectableItem( actionCode, ref shiftKeyDown, ctlKeyDown );
					
						if ( item != null )
						{						
							//get the items selection strategy
							ISelectionStrategy strategy = ((ISelectionManager)this.Grid).GetSelectionStrategy( item );
							
							if ( strategy != null )
							{
								//	BF 9.19.01	UWG282
								//	Use the new 'forceToggle' parameter of ProcessKeyBoardItem
								//	to preserve the existing selection when the space bar is pressed
								bool forceToggle = false;
								if ( actionCode == UltraGridAction.ToggleCellSel ||
									actionCode == UltraGridAction.ToggleRowSel )
									forceToggle = true;

								// SSP 7/25/02 UWG1407
								// Added code to set the dontUpdateWhenSelecting flag and reset it
								// after calling ProcessKeyBordItem. This was done to prevent flickering
								// (when tabbing from cell to cell, cell was getting selected for a split
								// second).
								//
								this.dontUpdateWhenSelecting = true;
								bool ret = true;
								try
								{
									//process that item inside the selection strategy
									//bool ret = strategy.ProcessKeyBoardItem ( item, shiftKeyDown, ctlKeyDown );
									ret = strategy.ProcessKeyBoardItem ( item, shiftKeyDown, ctlKeyDown, forceToggle );
								}
								finally
								{
									this.dontUpdateWhenSelecting = false;
								}

								if ( ret == true )
									updateCtrl = true;

								// SSP 4/9/02 UWG1070
								// If the ProcessKeyBoardItem failed, then don't try to
								// activate the cell.
								//
								//if ( item is UltraGridCell )
								// SSP 6/20/02 UWG1188
								// If we are toggling the cell selection (like with a space key), then
								// don't make it enter edit mode.
								// Added !forceToggle condition.
								//if ( ret && item is UltraGridCell )
								if ( !forceToggle && ret && item is UltraGridCell )
								{
									// SSP 12/16/02 UWG1895
									// Go into edit mode only when navigating cells with tab key. When arrow keys
									// are used to navigate, don't go into edit mode.
									// Added if condition to the code.
									//
									if ( UltraGridAction.NextCellByTab == actionCode ||
										UltraGridAction.PrevCellByTab == actionCode )
									{
										UltraGridCell cell = (UltraGridCell)item;

										if ( (wasInEditMode && lastEditBand == cell.Band)
											// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
											//|| cell.Band.CellClickActionResolved == CellClickAction.Edit
											|| cell.CellClickActionResolved == CellClickAction.Edit
											// JAS v5.2 Select Text On Cell Click 4/7/05
											// MRS 7/22/05 - NAS 2005 Vol. 3 - CellClickAction on the column
											//|| cell.Band.CellClickActionResolved == CellClickAction.EditAndSelectText)
											|| cell.CellClickActionResolved == CellClickAction.EditAndSelectText)
										{
											cell.SetFocusAndActivate( false, true, true );						
										}
									}
								}
								return ret;
							}
						}
					}
					finally
					{
						// RobA 12/27/01 UWG869 
						// Make sure we call EndUpdate to reset the flag and
						// invalidate the control
						// 
						if ( beginUpdateCalled )
							gridctl.EndUpdate();

						if ( updateCtrl )
							gridctl.Update();
					}
					
					return false;
				}
			}

			return false;
		}


		internal GridItemBase GetSelectableItem( UltraGridAction actionCode,
			ref bool shiftKeyDown, bool ctlKeyDown)
		{
			UltraGridCell newCell = null;
			UltraGridRow newRow = null;

			switch ( actionCode )
			{
				case UltraGridAction.NextCellInBand:
				case UltraGridAction.PrevCellInBand:
				{
					bool next;

					if ( actionCode == UltraGridAction.NextCellInBand )
						next = true;
					else
						next = false;

					bool select = true;
					// we don't want to select if control key is down, unless shift key is also down
					if ( ctlKeyDown && ! shiftKeyDown )
						select = false;

					
					// get next or previous cell
					newCell = this.GetNextCell( next,
						this.ActiveRow, // currentRow
						this.ActiveCell, // currentCell
						shiftKeyDown,
						ctlKeyDown,
						false, // bSpanBands
						true, // bSpanRows 
						false, // bSpanParents
						select ); // bSelect*/


					return newCell;					
				}

				case UltraGridAction.NextCell:
				case UltraGridAction.PrevCell:
				{
					bool next;

					if ( actionCode == UltraGridAction.NextCell )
						next = true;
					else
						next = false;
					
					bool spanBands = true;
					bool select = true; 

					//RobA 11/2/01 implemented
					// if the shift or control keys are pressed, we can't span bands because
					// we're adding to the existing selection
					if ( ((ISelectionManager)this.grid).GetSelectionStrategy( this.ActiveCell ).IsMultiSelect &&
						(  shiftKeyDown || ctlKeyDown ) )
						spanBands = false;					

					// we don't want to select if control key is down, unless shift key is also down
					if ( ctlKeyDown && ! shiftKeyDown )
						select = false;

					newCell = this.GetNextCell( next,
						this.ActiveRow, // currentRow
						this.ActiveCell, // currentCell
						shiftKeyDown,
						ctlKeyDown,
						spanBands, // bSpanBands
						true, // bSpanRows 
						true, // bSpanParents
						select ); // bSelect

					return newCell;					
				}

				case UltraGridAction.NextCellByTab:
				case UltraGridAction.PrevCellByTab:
				{
					bool next = (actionCode == UltraGridAction.NextCellByTab) ? true : false;
					
					bool spanBands = true;
					bool select = true;

					//if PrevCellByTab, shift key creates the keyaction, 
					//but should not determine selection
					if (!next) shiftKeyDown = false;

					newCell = this.GetNextCell( next,
						this.ActiveRow, // currentRow
						this.ActiveCell, // currentCell
						shiftKeyDown,
						ctlKeyDown,
						spanBands, // bSpanBands
						true, // bSpanRows 
						true, // bSpanParents
						select, // bSelect
						true ); // bByTabKey

					return newCell;
				}
				case UltraGridAction.AboveCell:
				case UltraGridAction.BelowCell:
				{
					bool down;

					if ( actionCode == UltraGridAction.BelowCell )
						down = true;
					else 
						down = false;

					bool select = true;
					
					// we don't want to select if control key is down, unless shift key is also down
					if ( ctlKeyDown && !shiftKeyDown )
						select = false;

					newCell = this.GetAboveBelowCell( down,
						this.ActiveRow, // currentRow
						this.ActiveCell, // currentCell
						shiftKeyDown,
						ctlKeyDown,
						false, // bSpanBands
						true, // bSpanRows 
						true, // bSpanParents
						select ); // bSelect*/

					return newCell;
				}
				
				case UltraGridAction.PageUpCell:
				case UltraGridAction.PageDownCell:
				{
					// SSP 4/30/05 - NAS 5.2 Fixed Rows_
					// Added PerformPageUpDownHelper method. Use that method instead which takes into
					// account the fixed rows.
					//
					// ------------------------------------------------------------------------------
					if ( null != this.ActiveCell )
					{
						UltraGridRow row = this.PerformPageUpDownHelper( actionCode, this.ActiveCell.Row );
						Debug.Assert( null == row || row.Band == this.ActiveCell.Band );

						newCell = null != row && row.Band == this.ActiveCell.Band
							? row.Cells[ this.ActiveCell.Column ] : null;
					}

					return newCell;
					
					// ------------------------------------------------------------------------------
				}

				case UltraGridAction.FirstCellInRow:
				case UltraGridAction.LastCellInRow:
				{
					if ( this.ActiveRow != null && this.ActiveCell != null )
					{
						if ( actionCode == UltraGridAction.FirstCellInRow )
							newCell = this.GetFirstCellInRow( true, this.ActiveRow, this.ActiveCell,
								shiftKeyDown, ctlKeyDown );
						else
							newCell = this.GetFirstCellInRow( false, this.ActiveRow, this.ActiveCell,
								shiftKeyDown, ctlKeyDown );
					}
					return newCell;
				}

				case UltraGridAction.FirstCellInBand:
				case UltraGridAction.LastCellInBand:
				{
					if ( this.ActiveRow != null && this.ActiveCell != null )
					{
						if ( actionCode == UltraGridAction.FirstCellInBand )
							newCell = this.GetFirstCellInBand( true, this.ActiveRow, this.ActiveCell,
								shiftKeyDown, ctlKeyDown );
						else
							newCell = this.GetFirstCellInBand( false, this.ActiveRow, this.ActiveCell,
								shiftKeyDown, ctlKeyDown );
					}

					return newCell;
				}

				case UltraGridAction.FirstCellInGrid:
				case UltraGridAction.LastCellInGrid:
				{
					if ( this.ActiveRow != null && this.ActiveCell != null )
					{
						if ( actionCode == UltraGridAction.FirstCellInGrid )
							newCell = this.GetFirstCellInGrid( true, this.ActiveCell );
						else
							newCell = this.GetFirstCellInGrid( false, this.ActiveCell );
					}

					return newCell;
				}

				case UltraGridAction.FirstRowInBand:
				case UltraGridAction.LastRowInBand:
				{
					// SSP 12/28/05 BR08498
					// Commented out GetFirstRowInBand. This method gets the first or last row in the row collection 
					// associated with the activeRow, including hidden rows. Here we need to get the first or last
					// visible row. Use the existing GetFirstLastActivatableRow method of the row collection instead.
					// 
					// ----------------------------------------------------------------------------------------------
					
					UltraGridRow activeRow = this.ActiveRow;
					if ( null != activeRow )
						newRow = activeRow.ParentCollection.GetFirstLastActivatableRow(
							actionCode == UltraGridAction.FirstRowInBand, true, shiftKeyDown, ctlKeyDown );
					// ----------------------------------------------------------------------------------------------

					// SSP 4/30/05 - NAS 5.2 Fixed Rows_
					// When there are fixed rows above calls may not result in scrolling to the
					// top or the bottom which is what you would expect when Home or End key
					// is pressed.
					//
					if ( null != newRow && newRow.Band.IsRootBand && null != this.ActiveRowScrollRegion )
						this.ActiveRowScrollRegion.KybdScroll( 
							UltraGridAction.FirstRowInBand == actionCode
							? ScrollEventType.First : ScrollEventType.Last );

					return newRow;
				}

				case UltraGridAction.FirstRowInGrid:
				case UltraGridAction.LastRowInGrid:
				{
					if ( actionCode == UltraGridAction.FirstRowInGrid )
						newRow = this.GetFirstOrLastRowInGrid( true );
					else
						newRow = this.GetFirstOrLastRowInGrid( false );

					// SSP 4/30/05 - NAS 5.2 Fixed Rows_
					// When there are fixed rows above calls may not result in scrolling to the
					// top or the bottom which is what you would expect when Home or End key
					// is pressed.
					//
					if ( null != newRow && null != this.ActiveRowScrollRegion )
						this.ActiveRowScrollRegion.KybdScroll( 
							UltraGridAction.FirstRowInGrid == actionCode
							? ScrollEventType.First : ScrollEventType.Last );

					return newRow;
				}

				case UltraGridAction.NextRow:
				case UltraGridAction.PrevRow:
				case UltraGridAction.NextRowByTab:
				case UltraGridAction.PrevRowByTab:
				{
					NavigateType navType;
                    
					if ( actionCode == UltraGridAction.NextRow ||
						actionCode == UltraGridAction.NextRowByTab )
						navType = NavigateType.Next;
					else
						navType = NavigateType.Prev;

					bool select = true;
					
					// JJD 1/14/02
					// Added NextRowByTab && PrevRowByTab so that we don't
					// don't range a select when doing a back tab. 
					//
					if ( actionCode == UltraGridAction.PrevRowByTab ||
						actionCode == UltraGridAction.NextRowByTab )
					{
						select = false;
						shiftKeyDown = false;
					}
					else
						// we don't want to select if control key is down, unless shift key is also down
						if ( ctlKeyDown && ! shiftKeyDown )
						select = false;

					newRow = this.GetNextRow( navType,
						this.ActiveRow, // currentRow
						shiftKeyDown,
						ctlKeyDown,
						true, // bSpanBands
						true, // bSpanParents
						select );

					return newRow;
				}

				case UltraGridAction.AboveRow:
				case UltraGridAction.BelowRow:
				{
					NavigateType navType;
                    
					if ( actionCode == UltraGridAction.BelowRow )
						navType = NavigateType.Below;
					else
						navType = NavigateType.Above;

					bool select = true;

					// we don't want to select if control key is down, unless shift key is also down
					if ( ctlKeyDown && ! shiftKeyDown )
						select = false;

					newRow = this.GetNextRow( navType,
						this.ActiveRow, // currentRow
						shiftKeyDown,
						ctlKeyDown,
						false, // bSpanBands
						true, // bSpanParents
						select );

					return newRow;
				}

				case UltraGridAction.PageUpRow:
				case UltraGridAction.PageDownRow:
				{
					// SSP 4/30/05 - NAS 5.2 Fixed Rows_
					// Added PerformPageUpDownHelper method. Use that method instead which takes into
					// account the fixed rows.
					//
					// ------------------------------------------------------------------------------
					newRow = this.PerformPageUpDownHelper( actionCode, this.ActiveRow );
					return newRow;
					
					// ------------------------------------------------------------------------------
				}

				case UltraGridAction.ToggleCellSel:
				{
					return this.ActiveCell;
				}

				case UltraGridAction.ToggleRowSel:
				{
					return this.ActiveRow;
				}
			}

			return null;
		}

		// SSP 11/16/04
		// Following method is not being used anywhere. Commenting it out.
		//
		

		internal UltraGridCell GetNextCell( bool next, UltraGridRow currentRow, UltraGridCell currentCell,
			bool shiftKeyDown, bool ctlKeyDown,
			bool spanBands, bool spanRows, bool spanParents )
		{
			return this.GetNextCell(next, currentRow, currentCell, shiftKeyDown, 
				ctlKeyDown, spanBands, spanRows, spanParents, false, false );
		}

		internal UltraGridCell GetNextCell( bool next, UltraGridRow currentRow, UltraGridCell currentCell,
			bool shiftKeyDown, bool ctlKeyDown,
			bool spanBands, bool spanRows, bool spanParents,
			bool select)
		{
			return this.GetNextCell(next, currentRow, currentCell, shiftKeyDown, 
				ctlKeyDown, spanBands, spanRows, spanParents, select, false );
		}

		internal UltraGridCell GetNextCell( bool next, UltraGridRow currentRow, UltraGridCell currentCell,
			bool shiftKeyDown, bool ctlKeyDown,
			bool spanBands, bool spanRows, bool spanParents,
			bool select, bool byTabKey )
		{
			UltraGridRow newRow = null;
			UltraGridCell newCell = null;

			Debug.Assert( currentRow != null, "No current row in GetNextCell!" );

			NavigateType navType;

			if ( next )
				navType = NavigateType.Next;
			else
				navType = NavigateType.Prev;

			//RobA 10/27/01 set snaking to false if the shift key or control key is
			//not down
			//
			// JJD 1/14/02
			// Set snaking to true if we are on a card
			//
			if ( currentRow != null && currentRow.IsCard )
				this.snaking = true;
			else
				if ( !ctlKeyDown || !shiftKeyDown )
				this.snaking = false;

			// get the next/prev visible col for this band
			//
			// SSP 7/29/05 - NAS 5.3 Tab Index
			// Pass along the byTabKey parameter.
			// 
			//UltraGridColumn newCol = currentRow.Band.GetRelatedVisibleCol( currentCell.Column, navType, false );
			UltraGridColumn newCol = currentRow.Band.GetRelatedVisibleCol( currentCell.Column, navType, false, byTabKey );

			if ( newCol == null && spanRows)
			{
				// JJD 1/18/02 - UWG948
				// If they just tabbed off the last activatable cell in an add row
				// and AllowAddNew.TabRepeat was set add a new row
				//
				// SSP 11/17/03 Add Row Feature
				// Added addRowThroughTemplate flag.
				//
				bool addRowThroughTemplate = false;

				if ( next && byTabKey &&
					currentCell != null &&
					currentCell == this.ActiveCell &&
					// SSP 1/24/03 UWG1951
					// Tab-repeat should work even if the last cell is not in edit mode. 
					// Took out the IsInEditMode condition. The bug was that the the last
					// column has activation set to not to enter edit mode, then the tab
					// repeat feature won't work.
					//
					//currentCell.IsInEditMode	&&
					// SSP 11/17/03 Add Row Feature
					// Added logic to go to the template add-row on top from current add-row.
					// Replaced existing code with the new code below.
					// --------------------------------------------------------------------
					//currentCell.Band.AllowAddNewResolved == AllowAddNew.TabRepeat &&					
					//currentCell.Row.IsAddRow 
					( currentCell.Band.AllowAddNewResolved == AllowAddNew.TabRepeat 
					  && currentCell.Row.IsAddRow 
					  || 
					  // NOTE: It's really '=' and not '==' below where addRowThroughTemplate is assigned.
					  ( addRowThroughTemplate = currentCell.Row.IsModifiedAddRowFromTemplate 
					   && TemplateAddRowLocation.Top == currentCell.Row.ParentCollection.TemplateRowLocationResolved
					   && AllowAddNew.TemplateOnTopWithTabRepeat == currentCell.Row.Band.AllowAddNewResolved )
					)
					// --------------------------------------------------------------------
					)
				{
					// exit edit mode
					//
					currentCell.ExitEditMode();

					// if the exit was cancelled return null
					//
					if ( currentCell.IsInEditMode )
						return null;

					// commit changes to the the add new row
					//
					// SSP 7/22/02 UWG1365
					// Instead of calling EndEdit, call Update since EndEdit bypasses
					// event firing. Also don't go ahead and add the new row if the
					// BeforeUpdate which the Update method fires gets cancelled. Just
					// return null in that case.
					//
					//currentCell.Row.EndEdit();
					if ( !currentCell.Row.Update( ) )
						return null;

					// SSP 11/17/03 Add Row Feature
					// Added below if block and enclosed existing code into the else block.
					//
					if ( addRowThroughTemplate )
					{
						currentCell.Row.ParentCollection.AddTemplateAddRowToDataList( true );
					}
					else
					{
						// add a new row
						//
						currentCell.Band.InternalAddNew();
					}					

					// return the active cell
					//
					return this.ActiveCell;
				}

				do
				{
					// get the next/prev visible row since we have run out of columns in
					// this band
					//
					newRow = this.GetRelatedVisibleRow( currentRow, navType, spanBands, spanParents, false );
				
					// SSP 4/11/02 UWG1082
					// If the row is an UltraGridGroupByRow, then continue, until
					// a row that's not an UltraGridGroupByRow is encountered because
					// group by rows don't have any cells.
					//
					if ( newRow is UltraGridGroupByRow )
					{
						currentRow = newRow;
						continue;
					}

					//newRow is null, then there is no prev/next row, so bail
					if ( newRow == null )
						break;
					else
					{
						// if this is the pivot row then set the m_fSelectCellsInRectangle
						// flag back to its default (true)
						// Otherwise, if this was a next or prev cell action  and
						// we are coming off the pivot row then set the
						// flag to false which will allow a snaking selection
						//

						//RobA 10/29/01 implemented snaking
						//
						// JJD 1/14/02
						// Set snaking to true if we are on a card
						//
						// MD 8/2/07 - 7.3 Performance
						// Refactored - Prevent calling expensive getters multiple times
						//if ( newRow.IsCard )
						//    this.snaking = true;
						//else
						//    if ( null == this.grid.ActiveRowScrollRegion.GetPivotItem() ||
						//    newRow == this.grid.ActiveRowScrollRegion.GetPivotItem().GetRow() )
						//    this.snaking = false;
						//
						//else if ( currentRow == this.grid.ActiveRowScrollRegion.GetPivotItem().GetRow() )
						//    this.snaking = true;
						if ( newRow.IsCard )
						{
							this.snaking = true;
						}
						else
						{
							GridItemBase pivotItem = this.grid.ActiveRowScrollRegion.GetPivotItem();

							if ( pivotItem == null )
							{
								this.snaking = false;
							}
							else
							{
								UltraGridRow pivotItemRow = pivotItem.GetRow();

								if ( newRow == pivotItemRow )
									this.snaking = false;
								else if ( currentRow == pivotItemRow )
									this.snaking = true;
							}
						}
						
						currentRow = newRow;

						// get either the first/last/topmost or bottommost visible column in this row's band
						//
						NavigateType navType2 = next ? NavigateType.First : NavigateType.Last;

						// SSP 7/29/05 - NAS 5.3 Tab Index
						// Pass along the byTabKey parameter.
						// 
						//newCol = currentRow.Band.GetRelatedVisibleCol( currentCell.Column, navType2, false );
						newCol = currentRow.Band.GetRelatedVisibleCol( currentCell.Column, navType2, false, byTabKey );
					}
					//ROBA UWG154 8/15/01
				}while ( newCol == null && spanRows && newRow != null );
				//while ( newCol == null && spanRows && currentRow != null );
			}

			if ( newCol != null &&

				// SSP 4/11/02 UWG1082
				// Also check for currentRow being null.
				// Added below two conditions. If no real row was found, then
				// don't do anything.
				//
				null != currentRow && !( currentRow is UltraGridGroupByRow ) )
			{
				newCell = currentRow.Cells[newCol];
				
				// if the cell is disabled or cant be navigated to, skip over it by 
				// calling GetNextCell recursively
				if ( newCell.IsDisabled || 
					//RobA 10/26/01 implemented CanItemBeNavigatedTo
					!((ISelectionManager)this.grid).GetSelectionStrategy( newCell).CanItemBeNavigatedTo( newCell, shiftKeyDown, ctlKeyDown ) )					
				{
					newCell = this.GetNextCell( next, currentRow, newCell, 
						shiftKeyDown, ctlKeyDown,
						spanBands, spanRows, spanParents
						// SSP 11/3/06 BR16572
						// Pass along the byTabKey parameter.
						//
						, false, byTabKey 
						);
						
					// SSP 10/11/02 UWG1471
					// If there is no next navigatable cell, and the current row is an add new
					// row and the AllowAddNew is TabRepeat than add a new row.
					// Copied this code from above.
					//
					// ------------------------------------------------------------------------
					// SSP 11/17/03 Add Row Feature
					// Added addRowThroughTemplate flag.
					//
					bool addRowThroughTemplate = false;

                    if (null == newCell ||
                        // MRS 12/3/2007 - BR28357
                        // If the new cell is on another row, it means we probably skipped some 
                        // cells when tabbing because TabStop is false. In that case, it's the same 
                        // as tabbing off the row and we need to check for TabRepeat.
                        newCell.Row != currentRow)
					{
						if ( next && byTabKey &&
							currentCell != null &&
							currentCell == this.ActiveCell &&
							currentCell.IsInEditMode	&&
							// SSP 11/17/03 Add Row Feature
							// Added logic to go to the template add-row on top from current add-row.
							// Replaced existing code with the new code below.
							// --------------------------------------------------------------------
							//currentCell.Band.AllowAddNewResolved == AllowAddNew.TabRepeat &&					
							//currentCell.Row.IsAddRow 
							( currentCell.Band.AllowAddNewResolved == AllowAddNew.TabRepeat 
							&& currentCell.Row.IsAddRow 
							|| 
							// NOTE: It's really '=' and not '==' below where addRowThroughTemplate is assigned.
							( addRowThroughTemplate = currentCell.Row.IsModifiedAddRowFromTemplate 
							 && TemplateAddRowLocation.Top == currentCell.Row.ParentCollection.TemplateRowLocationResolved 
							 && AllowAddNew.TemplateOnTopWithTabRepeat == currentCell.Row.Band.AllowAddNewResolved )
							)
							// --------------------------------------------------------------------
							)
						{
							// exit edit mode
							//
							currentCell.ExitEditMode();

							// if the exit was cancelled return null
							//
							if ( currentCell.IsInEditMode )
								return null;

							// commit changes to the the add new row
							//
							// SSP 7/22/02 UWG1365
							// Instead of calling EndEdit, call Update since EndEdit bypasses
							// event firing. Also don't go ahead and add the new row if the
							// BeforeUpdate which the Update method fires gets cancelled. Just
							// return null in that case.
							//
							//currentCell.Row.EndEdit();
							if ( !currentCell.Row.Update( ) )
								return null;

							// SSP 11/17/03 Add Row Feature
							// Added below if block and enclosed existing code into the else block.
							//
							if ( addRowThroughTemplate )
							{
								currentCell.Row.ParentCollection.AddTemplateAddRowToDataList( true );
							}
							else
							{
								// add a new row
								//
								currentCell.Band.InternalAddNew();
							}

							// return the active cell
							//
							return this.ActiveCell;
						}
					}
					// ------------------------------------------------------------------------
				}
			}
            
			return newCell;
		}

		internal UltraGridCell GetAboveBelowCell( bool down, UltraGridRow currentRow, UltraGridCell currentCell,
			bool shiftKeyDown, bool ctlKeyDown, bool spanBands, bool spanRows,
			bool spanParents, bool select )
		{
			
			UltraGridCell newCell = null;
			UltraGridRow newRow = null;

			Debug.Assert( currentRow != null, "No current row in GetAboveBelowCell!" );

			NavigateType navType;

			if ( down )
				navType = NavigateType.Below;
			else
				navType = NavigateType.Above;

			//RobA 10/27/01 set snaking to false if the shift key or control key is
			//not down
			//
			// JJD 1/14/02
			// Set snaking to true if we are on a card
			//
			if ( currentRow != null && currentRow.IsCard )
				this.snaking = true;
			else
				if ( !ctlKeyDown || !shiftKeyDown )
				this.snaking = false;

			// SSP 3/5/03 - Row Layout Functionality
			//
			// ----------------------------------------------------------------------------------
			if ( currentCell.Band.UseRowLayoutResolved )
			{
				if ( null == currentCell )
					return null;

				UltraGridRow row = currentRow;

				if ( null == row )
					row = currentCell.Row;

				while ( null != row )
				{
					HeadersCollection headers = currentCell.Band.LayoutOrderedVisibleColumnHeadersVertical;

					Debug.Assert( null != headers, "No headers !" );

					if ( null == headers )
						break;

					bool currentCellColumnFound = false;

					bool backwards = NavigateType.Above == navType;

					for ( int i = backwards ? headers.Count - 1 : 0; 
						backwards ? i >= 0 : i < headers.Count; 
						i += backwards ? -1 : 1 )
					{
						UltraGridColumn column = headers[i].GetFirstVisibleCol( false );

						if ( null == column )
							continue;

						if ( currentCellColumnFound || row != currentCell.Row )
						{
							Debug.Assert( null != column );
							if ( null == column )
								continue;

							// When selecting, make sure we follow a path that will take us to the next row
							// right after a bottom most cell rather than going to the cell on the right column.
							// 
							if ( select && shiftKeyDown && row == currentCell.Row )
							{
								if ( backwards )
								{
									if ( column.RowLayoutColumnInfo.OriginYResolved >= currentCell.Column.RowLayoutColumnInfo.OriginYResolved )
										continue;
								}
								else
								{
									if ( column.RowLayoutColumnInfo.OriginYResolved <= currentCell.Column.RowLayoutColumnInfo.OriginYResolved )
										continue;
								}
							}
								// SSP 11/13/03
								// Added below else-if block.
								
								// down does not feel right when you have a layout with cells that partially
								// span other cells.
								//
							else if ( select && shiftKeyDown )
							{
								if ( backwards )
								{
									if ( column.RowLayoutColumnInfo.OriginXResolved > currentCell.Column.RowLayoutColumnInfo.OriginXResolved  )
										continue;
								}
								else
								{
									if ( column.RowLayoutColumnInfo.OriginXResolved < currentCell.Column.RowLayoutColumnInfo.OriginXResolved )
										continue;
								}
							}
								// SSP 8/17/05 BR05463
								// 
							else if ( RowLayoutCellNavigation.Adjacent == column.Band.RowLayoutCellNavigationVerticalResolved )
							{
								if ( ! LayoutUtility.Intersects( 
										column.RowLayoutColumnInfo.OriginXResolved, 
										column.RowLayoutColumnInfo.SpanXResolved,
										currentCell.Column.RowLayoutColumnInfo.OriginXResolved, 
										currentCell.Column.RowLayoutColumnInfo.SpanXResolved ) )
									continue;

								if ( row == currentCell.Row )
								{								
									if ( backwards 
										? column.RowLayoutColumnInfo.OriginYResolved >= currentCell.Column.RowLayoutColumnInfo.OriginYResolved 
										: column.RowLayoutColumnInfo.OriginYResolved <= currentCell.Column.RowLayoutColumnInfo.OriginYResolved )
										continue;
								}
							}

							newCell = row.Cells[ column ];

							ISelectionStrategy selectionStrategy = null != this.grid 
								? ((ISelectionManager)this.grid).GetSelectionStrategy( currentCell ) : null;
								
							if ( null == selectionStrategy || selectionStrategy.CanItemBeNavigatedTo( newCell, shiftKeyDown, ctlKeyDown ) )
								return newCell;
						}
						else
						{
							if ( column == currentCell.Column )
								currentCellColumnFound = true;
						}
					}

					// SSP 8/17/05
					// 
					if ( ! spanRows )
						break;

					// SSP 11/13/03
					// Use the row, not the currentRow which stays static while in the while loop.
					//
					//row = this.GetRelatedVisibleRow( currentRow, navType, spanBands, spanParents, false );
					row = this.GetRelatedVisibleRow( row, navType, spanBands, spanParents, false );

					// AS 1/26/06 BR09335
					// We need to skip some rows including disabled rows and
					// group by rows (i.e. rows without cells).
					//
					while (row != null)
					{
						if (row.SupportsCells == false)
							row = this.GetRelatedVisibleRow(row, navType, spanBands, spanParents, false);
						else if (row.IsDisabled || !((ISelectionManager)this.grid).GetSelectionStrategy(row).CanItemBeNavigatedTo(row, shiftKeyDown, ctlKeyDown))
							row = this.GetRelatedVisibleRow(row, navType, spanBands, spanParents, false);
						else
							break;
					}
				}

				return null;
			}
			// ----------------------------------------------------------------------------------


			// JJD 1/14/02
			// Added logic to handle cell navigation in a card area
			//
			if ( currentRow != null &&
				currentRow.IsCard )
			{
				do
				{
					// get the next/previous cell
					//
					newCell = this.GetNextCell( down, currentRow, currentCell,
						shiftKeyDown, ctlKeyDown,
						false, false, false, select, false );

					// if we found one then return it
					//
					if ( newCell != null )
						return newCell;

					// get the card above or below this row since we have run out of columns in
					// this band
					//
					newRow = this.GetRelatedVisibleRow( currentRow, navType, spanBands, spanParents, false );

					if ( newRow != null )
					{
						currentRow = newRow;

						UltraGridColumn newColumn;
						if ( down )
							newColumn = currentRow.Band.GetFirstVisibleCol( this.ActiveColScrollRegion, false );
						else
							newColumn = currentRow.Band.GetLastVisibleCol( this.ActiveColScrollRegion, false );

						if ( newColumn != null )
							newCell = currentRow.Cells[ newColumn ];
						else
							newCell = currentRow.Cells[0];

						if ( newCell == null || newCell.IsDisabled || 
							//RobA 10/26/01 implemented CanItemBeNavigatedTo
							!((ISelectionManager)this.grid).GetSelectionStrategy( newCell).CanItemBeNavigatedTo( newCell, shiftKeyDown, ctlKeyDown )) 
							continue;
						else
							break;
					}
						// SSP 1/24/03 UWG1965
						// If the currentRow does not have a related row, then break out other wise
						// we will be stuck in an infinite loop.
						// Added the else block that breaks out of the do-while loop.
						//
					else
					{
						newCell = null;
						break;
					}
				}while ( newCell != null );
			}
			else
			{
				do
				{
					// get the next/prev visible row since we have run out of columns in
					// this band
					//
					newRow = this.GetRelatedVisibleRow( currentRow, navType, spanBands, spanParents, false );

					// AS 1/26/06 BR09335
					// We don't want to access the cells collection of a group by
					// row. This can happen when a filter row is active and the 
					// root rows are groupby rows.
					//
					//if ( newRow != null )
					if ( newRow != null && newRow.SupportsCells )
					{
						// if this is the pivot row then set the m_fSelectCellsInRectangle
						// flag back to its default (true)
						// Otherwise, if this was a next or prev cell action  and
						// we are coming off the pivot row then set the
						// flag to false which will allow a snaking selection
						//RobA 10/29/01 implemented snaking
						// MD 8/2/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( null == this.grid.ActiveRowScrollRegion.GetPivotItem() || 
						//    newRow == this.grid.ActiveRowScrollRegion.GetPivotItem().GetRow() )
						//    this.snaking = false;
						GridItemBase pivotItem = this.grid.ActiveRowScrollRegion.GetPivotItem();

						if ( null == pivotItem || newRow == pivotItem.GetRow() )
							this.snaking = false;

						currentRow = newRow;

						newCell = currentRow.Cells[currentCell.Column];

						if ( newCell == null || newCell.IsDisabled || 
							//RobA 10/26/01 implemented CanItemBeNavigatedTo
							!((ISelectionManager)this.grid).GetSelectionStrategy( newCell).CanItemBeNavigatedTo( newCell, shiftKeyDown, ctlKeyDown )) 
							continue;
						else
							break;
					}
						// SSP 1/24/03 UWG1965
						// If the currentRow does not have a related row, then break out other wise
						// we will be stuck in an infinite loop.
						// Added the else block that breaks out of the do-while loop.
						//
					else
					{
						newCell = null;
						break;
					}
				}while ( newCell != null );
			}

			return newCell;
		}
		internal UltraGridRow GetNextRow( NavigateType navType, UltraGridRow currentRow, bool shiftKeyDown, bool ctlKeyDown,
			bool spanBands, bool spanParents, bool select)
		{
			UltraGridRow newRow = null;
		
			Debug.Assert( currentRow != null, "No current row in GetNextRow!" );

			do
			{
				// get the next/prev visible row since we have run out of columns in
				// this band
				//
				newRow = this.GetRelatedVisibleRow(currentRow, navType, spanBands, spanParents, false );

				if ( newRow != null )
				{
					// if this is the pivot row then set the m_fSelectCellsInRectangle
					// flag back to its default (true)
					// Otherwise, if this was a next or prev cell action  and
					// we are coming off the pivot row then set the
					// flag to false which will allow a snaking selection
					//RobA 10/29/01 implemented snaking
					// JJD 1/14/02
					// Set snaking to true if we are on a card
					//
					// MD 8/2/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( currentRow != null && currentRow.IsCard )
					//    this.snaking = true;
					//else
					//    if ( null == this.grid.ActiveRowScrollRegion.GetPivotItem() || 
					//    newRow == this.grid.ActiveRowScrollRegion.GetPivotItem().GetRow() )
					//    this.snaking = false;
					//else if ( currentRow == this.grid.ActiveRowScrollRegion.GetPivotItem().GetRow() )
					//    this.snaking = true;
					if ( currentRow != null && currentRow.IsCard )
					{
						this.snaking = true;
					}
					else
					{
						GridItemBase pivotItem = this.grid.ActiveRowScrollRegion.GetPivotItem();

						if ( null == pivotItem )
						{
							this.snaking = false;
						}
                        // MBS 6/13/08 - BR33218
                        // After double-checking with Joe, it doesn't seem like there is any reason to set
                        // the snaking flag to true when selecting entire rows, which is what this method
                        // is used for, so we can just ignore this block.  I left the code to remove the snaking
                        // flag about to be safe, though, since if we no longer have a pivot item it can't hurt
                        // to reset the flag.
                        //
                        //else
                        //{
                        //    UltraGridRow pivotItemRow = pivotItem.GetRow();

                        //    if (newRow == pivotItemRow)
                        //        this.snaking = false;
                        //    else if (currentRow == pivotItemRow)
                        //        this.snaking = true;
                        //}
					}

					currentRow = newRow;
				}
			}while ( newRow != null && ( currentRow.IsDisabled || 
				//RobA 10/26/01 implemented CanItemBeNavigatedTo
				!((ISelectionManager)this.grid).GetSelectionStrategy( newRow ).CanItemBeNavigatedTo( currentRow, shiftKeyDown, ctlKeyDown ) ));
			
			return newRow;
		}
		internal UltraGridCell GetFirstCellInRow( bool first, UltraGridRow activeRow, UltraGridCell activeCell, 
			bool shiftKeyDown, bool ctlKeyDown )
		{
			UltraGridCell newCell = null;
			NavigateType navType;
			bool next;

			if ( first )
			{
				navType = NavigateType.First;
				next = true;
			}
			else
			{
				navType = NavigateType.Last;
				next = false;
			}

			// get the first/last visible col for this band
			//
			UltraGridColumn newCol = null;

			newCol= activeRow.Band.GetRelatedVisibleCol( activeCell.Column, navType, false );

			if ( newCol != null )
			{
				newCell = activeRow.Cells[newCol];
				while (  (newCell != null && 
					( newCell.IsDisabled ||
					//RobA 10/26/01 implemented CanItemBeNavigatedTo
					!((ISelectionManager)this.grid).GetSelectionStrategy( newCell ).CanItemBeNavigatedTo( newCell, shiftKeyDown, ctlKeyDown ) )) 
					&& newCell != activeCell ) 
				{
					newCell = this.GetNextCell( next, activeRow, newCell, shiftKeyDown, ctlKeyDown, false, false, false );
				}

				// if we have a new cell, select it
				//				if ( newCell != null )
				//				{
				//					newCell.Band.ClickCell( newCell, false, shiftKeyDown );
				//					
				//					// only update if action succeeded
				//					if ( newCell == this.ActiveCell )
				//						this.grid.Update();
				//				}					
			}

			return newCell;
		}
		internal UltraGridCell GetFirstCellInBand( bool first, UltraGridRow activeRow, UltraGridCell activeCell,
			bool shiftKeyDown, bool ctlKeyDown )
		{
			
			UltraGridCell newCell = null;
			UltraGridRow relatedRow = null;

			bool spanBands = false;
			bool hasSibling = false;

			if ( first )
			{
				// SSP 11/11/03 Add Row Feature
				//
				//hasSibling = activeRow.HasPrevSibling( spanBands );
				hasSibling = activeRow.HasPrevSibling( spanBands, IncludeRowTypes.SpecialRows );

				if ( hasSibling )
					// SSP 11/11/03 Add Row Feature
					//
					//relatedRow = activeRow.GetSibling(SiblingRow.First, spanBands );
					relatedRow = activeRow.GetSibling(SiblingRow.First, spanBands, false, IncludeRowTypes.SpecialRows );
				else
					relatedRow = activeRow;
			}

			else
			{
				// SSP 11/11/03 Add Row Feature
				//
				//hasSibling = activeRow.HasNextSibling( spanBands );
				hasSibling = activeRow.HasNextSibling( spanBands, IncludeRowTypes.SpecialRows );

				if ( hasSibling )
					// SSP 11/11/03 Add Row Feature
					//
					//relatedRow = activeRow.GetSibling( SiblingRow.Last, spanBands );
					relatedRow = activeRow.GetSibling( SiblingRow.Last, spanBands, false, IncludeRowTypes.SpecialRows );
				else
					relatedRow = activeRow;
			}

			Debug.Assert( relatedRow != null, " no ppSSRelatedRow! " );

			newCell = relatedRow.Cells[activeCell.Column];

			while ( ( newCell != null && 
				(newCell.IsDisabled ||
				//RobA 10/26/01 implemented CanItemBeNavigatedTo
				!((ISelectionManager)this.grid).GetSelectionStrategy( newCell ).CanItemBeNavigatedTo( newCell, shiftKeyDown, ctlKeyDown ) ))
				&& newCell != activeCell )
			{
				newCell = this.GetNextCell( first, activeRow, newCell, shiftKeyDown, ctlKeyDown, false, false, false );
			}

			// if we have a new cell, select it
			//			if ( newCell != null )
			//			{
			//				newCell.Band.ClickCell( newCell, false, shiftKeyDown );
			//				// only update if action succeeded
			//				if ( newCell == this.ActiveCell )
			//					this.grid.Update();
			//			}
			return newCell;
		}
		internal UltraGridCell GetFirstCellInGrid( bool first, UltraGridCell activeCell )
		{
			
			UltraGridCell newCell = null;
			UltraGridRow relatedRow = null;
			UltraGridColumn col;

			if ( first )
			{
				relatedRow = this.grid.GetRow( ChildRow.First );
				col = relatedRow.Band.GetFirstVisibleCol( null, false );
			}

			else
			{
				ViewStyleBase viewStyle = this.viewStyleImpl;

				if ( viewStyle == null )
				{
					Debug.Fail("Viewstyle not found in Layout::GetFirstCellInGrid" );
					return null;
				}

				relatedRow = viewStyle.LastRow;
				col = relatedRow.Band.GetLastVisibleCol( null, false );
			}

			// SSP 1/6/04 UWG2833
			// Since we are trying to get the first/last cell here, make sure the
			// row is a regular row and not a group-by row. If it's a group-by row
			// then get the next/previous visible row that's not a group-by row.
			// If none of the regular rows are visible, then return null. Pressing
			// Ctrl+Home will take no action since there are no visible cells to 
			// activate.
			//
			// ----------------------------------------------------------------------
			while ( relatedRow is UltraGridGroupByRow )
			{
				if ( first )
					relatedRow = relatedRow.GetNextVisibleRow( );
				else
					relatedRow = relatedRow.GetPrevVisibleRow( );
			}
			
			if ( null == relatedRow )
				return null;
			// ----------------------------------------------------------------------

			Debug.Assert( relatedRow != null, "no relatedRow in layout.GetFirstCellInGrid!" );
			newCell = relatedRow.Cells[col];

			while ( (newCell != null && 
				( newCell.IsDisabled ||
				//RobA 10/26/01 implemented CanItemBeNavigatedTo
				!((ISelectionManager)this.grid).GetSelectionStrategy( newCell ).CanItemBeNavigatedTo( newCell, false, false ) ))
				&& newCell != activeCell )
			{
				// JJD 9/26/01
				// Use relatedRow instead of active row
				//
				newCell = this.GetNextCell( first, relatedRow, newCell, false, false, false, false, false );
			}

			// if we have a new cell, select it
			//			if ( newCell != null )
			//			{
			//				this.gettingFirstCell = true;
			//				newCell.Band.ClickCell( newCell, false, false );
			//				this.gettingFirstCell = false;
			//
			//				// only update if action succeeded
			//				if ( newCell == this.ActiveCell )
			//					this.grid.Update();
			//			}			

			return newCell;
		}

		// SSP 12/28/05 BR08498
		// Commented out GetFirstRowInBand. This method gets the first or last row in the row collection associated
		// with the activeRow, not the whole band. Therefore the name is confusing.
		// 
		
		
		internal UltraGridRow GetFirstOrLastRowInGrid( bool first )
		{
			UltraGridRow row = null;

			if ( first )
			{
				// SSP 10/3/03 UWG2689
				// We want to get the first visible row rather than simply the first row.
				//
				//row = this.grid.GetRow( ChildRow.First );
				row = this.grid.Rows.GetFirstVisibleRow( );
			}
			else
			{
				ViewStyleBase viewStyle = this.viewStyleImpl;

				if ( viewStyle == null )
				{
					Debug.Fail("Viewstyle not found in Layout::GetFirstRowInGrid" );
					return null;
				}

				row = viewStyle.LastRow;				
			}

			// JJD 9/26/01
			// We shouldn't select the row here since the selection
			// strategy will take care of that based on the row we return
			//
			// if we have a new cell, select it
			
			return row;
		}
		// JJD 1/11/02
		// Removed ref from the first parameter
		//
		internal UltraGridRow GetRelatedVisibleRow( UltraGridRow row, NavigateType navType, bool spanBands,
			bool spanParents, bool includeNonActivateable )
		{
			
			UltraGridRow relatedRow = null;

			// JJD 12/27/01
			// If the row is a card then call the GetRelatedVisibleCard method
			// off its rows collection
			//
			if ( row.IsCard )
			{
				relatedRow = row.ParentCollection.GetRelatedVisibleCard( row, navType, includeNonActivateable );

				if ( relatedRow != null )
					return relatedRow;

				// If a related sibling card wasn't found and we aren't spanning
				// bands or parents then return null
				//
				if ( !spanParents && 
					!spanBands )
					return null;
			}

			ViewStyleBase viewStyle = this.viewStyleImpl;

			if ( viewStyle == null )
			{
				Debug.Fail("Viewstyle not found in layout::GetRelatedVisibleRow");
				return null;
			}

			if ( navType == NavigateType.Above ||
				navType == NavigateType.Below )
			{
				// if the view style doesn't have multi row tiers
				// (not horizontal) the above/below do the same
				// as prev/next so change the navtype appropriately
				//
				if ( !viewStyle.HasMultiRowTiers )
				{
					navType = ( navType == NavigateType.Above ) ? NavigateType.Prev : NavigateType.Next;
				}
			}

			//GOTO Statement
			get_next_row:

				switch( navType )
				{
					case NavigateType.Above:
					{
						bool hasSibling = false;

						// check to see if their are any prev siblings
						//
						// SSP 8/8/03 UWG2288
						// Pass in false as the exclude hidden. The reason for doing this is for efficiency
						// reason. This method takes care of hidden rows.
						//
						//hasSibling = row.HasPrevSibling( spanBands );
						// SSP 11/11/03 Add Row Feature
						//
						//hasSibling = row.HasPrevSibling( spanBands, false );
						hasSibling = row.HasPrevSibling( spanBands, false, IncludeRowTypes.SpecialRows );

						if ( hasSibling )
							// SSP 11/11/03 Add Row Feature
							//
							//relatedRow = row.GetSibling( SiblingRow.Previous, spanBands );
							relatedRow = row.GetSibling( SiblingRow.Previous, spanBands, false, IncludeRowTypes.SpecialRows );
						else if ( spanParents )
						{
							UltraGridRow cousin = null;

							// check for a cross parent sibling
							//
							cousin = row.GetCrossParentSibling( true, spanBands );
							
							if ( cousin != null )
								relatedRow = cousin;
						}
						break;
					}
					case NavigateType.Below:
					{
						bool hasSibling = false;

						// check to see if their are any prev siblings
						//
						// JJD 9/26/01
						// Call has next instead of has previous
						//
						//						hasSibling = row.HasPrevSibling( spanBands );
						// SSP 8/8/03 UWG2288
						// Pass in false as the exclude hidden. The reason for doing this is for efficiency
						// reason. This method takes care of hidden rows.
						//
						//hasSibling = row.HasNextSibling( spanBands );
						// SSP 11/11/03 Add Row Feature
						//
						//hasSibling = row.HasNextSibling( spanBands, false );
						hasSibling = row.HasNextSibling( spanBands, false, IncludeRowTypes.SpecialRows );

						if ( hasSibling )
							// SSP 11/11/03 Add Row Feature
							//
							//relatedRow = row.GetSibling( SiblingRow.Next, spanBands );
							relatedRow = row.GetSibling( SiblingRow.Next, spanBands, false, IncludeRowTypes.SpecialRows );
						else if ( spanParents )
						{
							UltraGridRow cousin = null;

							// check for a cross parent sibling
							//
							cousin = row.GetCrossParentSibling( false, spanBands );
							
							if ( cousin != null )
								relatedRow = cousin;
						}
						break;
					}
					case NavigateType.Next:
					{
						// if the row is expanded get its first child row
						//
						if ( spanBands && spanParents && row.IsExpanded )
							// SSP 11/11/03 Add Row Feature
							//
							//relatedRow = row.GetChild(ChildRow.First, null );
							relatedRow = row.GetChild( ChildRow.First, null, IncludeRowTypes.SpecialRows );

						if ( relatedRow == null )
						{
							bool hasSibling = false;

							// check to see if their are any next siblings
							//
							// SSP 8/8/03 UWG2288
							// Pass in false as the exclude hidden. The reason for doing this is for efficiency
							// reason. This method takes care of hidden rows.
							//
							//hasSibling = row.HasNextSibling( spanBands );
							// SSP 11/11/03 Add Row Feature
							//
							//hasSibling = row.HasNextSibling( spanBands, false );
							hasSibling = row.HasNextSibling( spanBands, false, IncludeRowTypes.SpecialRows );

							if ( hasSibling )
								// SSP 11/11/03 Add Row Feature
								//
								//relatedRow = row.GetSibling( SiblingRow.Next, spanBands );
								relatedRow = row.GetSibling( SiblingRow.Next, spanBands, false, IncludeRowTypes.SpecialRows );
							else if ( spanParents )
							{
								if ( spanBands )
								{
									UltraGridRow parentRow = row.ParentRow;
									// walk up the parent chain looking for siblings of our ancestors
									//
									while ( parentRow != null )
									{
										// SSP 11/11/03 Add Row Feature
										//
										//relatedRow = parentRow.GetSibling( SiblingRow.Next, spanBands );
										relatedRow = parentRow.GetSibling( SiblingRow.Next, spanBands, false, IncludeRowTypes.SpecialRows );
										if ( relatedRow != null)
											break;

										// get next row in the parent chain
										//
										parentRow = parentRow.ParentRow;
									}
								}
								else
								{
									UltraGridRow  cousin = null;

									// check for a cross parent sibling
									//
									cousin = row.GetCrossParentSibling( false, spanBands );
									
									if ( cousin != null )
										relatedRow = cousin;

								}
							}
						}

						break;
					}
					case NavigateType.Prev:
					{
						bool hasSibling = false;

						// check if the row has a previous sibling row
						//
						// SSP 8/8/03 UWG2288
						// Pass in false as the exclude hidden. The reason for doing this is for efficiency
						// reason. This method takes care of hidden rows.
						//
						//hasSibling = row.HasPrevSibling( spanBands );
						// SSP 11/11/03 Add Row Feature
						//
						//hasSibling = row.HasPrevSibling( spanBands, false );
						hasSibling = row.HasPrevSibling( spanBands, false, IncludeRowTypes.SpecialRows );

						if ( hasSibling )
						{
							UltraGridRow previousSibling = null;
							// get the prev sibling row
							//
							// SSP 11/11/03 Add Row Feature
							//
							//previousSibling = row.GetSibling(SiblingRow.Previous, spanBands );
							previousSibling = row.GetSibling( SiblingRow.Previous, spanBands, false, IncludeRowTypes.SpecialRows );

							if ( spanBands && previousSibling != null )
							{
								// if the sibling is expanded get its last visible descendant row
								//

								if ( previousSibling.IsExpanded )
									relatedRow = viewStyle.GetLastDescendantRow( previousSibling );

								// if the child row was found release our reference on the
								// sibling row. Otherwise return the sibling row itself
								// 
								if ( relatedRow == null )
									relatedRow = previousSibling;
							}
							else
								relatedRow = previousSibling;
						}
						else if ( !spanBands && spanParents )
						{
							UltraGridRow cousin = null;

							// check for a cross parent sibling
							//
							cousin = row.GetCrossParentSibling( true, spanBands );
							
							if ( cousin != null )
								relatedRow = cousin;
						}

						if ( spanBands && relatedRow == null )
							// SSP 11/10/03 - Add Row Feature
							// Why not just use the ParentRow property instead of GetParent. Only one place GetParent was
							// used so took it out in an effort to follow .NET conventions.
							//
							//relatedRow = row.GetParent();
							relatedRow = row.ParentRow;
											
						break;
					}
					case NavigateType.First:
					{
						relatedRow = this.grid.GetRow( ChildRow.First );
						break;
					}
					case NavigateType.Last:
					{
						UltraGridRow lastBand0Row = null;

						// get the last band 0 row
						//
						lastBand0Row = this.grid.GetRow( ChildRow.Last );

						if ( lastBand0Row != null )
						{
							// get the last visible descendant of that row
							//
							
							relatedRow = viewStyle.GetLastDescendantRow( lastBand0Row );

							// if a descendant was found use it and release our reference on
							// the band 0 row. Otherwise, use the band 0 row.
							//
							if ( relatedRow == null )
								relatedRow = lastBand0Row;
						}
						break;
					}
							
				}

			if ( relatedRow != null )
			{
				// since we found a related row check to make sure it isn't hidden
				//
				if ( relatedRow.HiddenResolved )
				{
					// since it is hidden we need to get the next/prev row
					// so release our reference on the base row and set the
					// new base row to this hidden row and loop around to find 
					// the next/prev row
					//
					row = relatedRow;

					// Reset *ppSSRelatedRow to NULL so we don't blow up the next 
					// time through after we loop around to get_next_row
					//
					relatedRow = null;

					// adjust the nav type appropriately if first or last was
					// asked for.
					//

					if ( NavigateType.First == navType )
						navType = NavigateType.Next;

					else if ( NavigateType.Last == navType )
						navType = NavigateType.Prev;

					goto get_next_row;
				}
			}

			return relatedRow;
		}

		// SSP 4/25/03 UWG2185
		// Added an overload of DeleteSelectedRows that takes in a parameter that specifies whether to
		// display the delete confirmation dialog.
		//
		internal void DeleteSelectedRows( bool checkAllowDeleteProp )
		{
			this.DeleteSelectedRows( checkAllowDeleteProp, true );
		}

		//internal void DeleteSelectedRows( bool checkAllowDeleteProp )
		internal void DeleteSelectedRows( bool checkAllowDeleteProp, bool displayPrompt )
		{
			UltraGrid gridctl = this.Grid as UltraGrid;

			if ( null == gridctl )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_180"));

			Selected selected = gridctl.Selected;

			if ( selected == null )
				return;

			Infragistics.Win.UltraWinGrid.SelectedRowsCollection selectedRows = selected.Rows;

			// if there are no slected rows ten just return
			//
			if ( selectedRows == null || selectedRows.Count < 1 )
				return;

			// SSP 9/26/01 UWG329
			// Group by rows can not be deleted.
			//
			if ( selectedRows[0] is UltraGridGroupByRow )
				return;

			
			// If the checkAllowDeleteProp is true make sure that the
			// AllowDelete property for the band is true.
			//
			if ( checkAllowDeleteProp )
			{
				// We only need to check 1 row since we don't support
				// cross band selections
				//                
                // MBS 9/16/08 - TFS6776
                // Check the resolved property instead, and cache the Band property
                //
				//if ( selectedRows[0] == null || selectedRows[0].Band.AllowDelete == false )
                UltraGridBand band = selectedRows[0].Band;
                if (selectedRows[0] == null || band.AllowDeleteResolved == DefaultableBoolean.False)
					return;

                // MBS 9/16/08 - TFS6776
                // Since we don't always create a binding list for a band (such as in the case
                // where we have a recursive structure within a BindingSource), we may need
                // to check the Rows collection's list.  We will first check the band to see
                // if we can determine whether we can delete for efficiency, but if we can't, we will check 
                // the parent rows collection of a row.
                bool canDelete;
                if (band.AllowDelete(out canDelete) == false)
                {
                    foreach (UltraGridRow row in selectedRows)
                    {
                        // Bail out if we can't delete any one of the rows
                        if (!band.AllowDelete(row))
                            return;
                    }
                }
                else if (canDelete == false)
                    return;
			}

			InternalDeleteRowsHelper( selectedRows, displayPrompt );
		}


		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal class ListIndexComparer : IComparer
		internal class ListIndexComparer : IComparer<UltraGridRow>
		{

			private int AncestorRowIndexComparer( UltraGridRow row1, UltraGridRow row2 )
			{
				// MD 7/27/07 - 7.3 Performance
				// Refactored - Prevent calling expensive getters multiple times
				//if ( null != row1.ParentRow && null != row2.ParentRow )
				//{
				//    int r = this.AncestorRowIndexComparer( row1.ParentRow, row2.ParentRow );
				//
				//    if ( 0 != r )
				//        return r;
				//}
				UltraGridRow row1ParentRow = row1.ParentRow;

				if ( null != row1ParentRow )
				{
					UltraGridRow row2ParentRow = row2.ParentRow;

					if ( null != row2ParentRow )
					{
						int r = this.AncestorRowIndexComparer( row1ParentRow, row2ParentRow );

						if ( 0 != r )
							return r;
					}
				}

				int listIndex1 = row1.ListIndex;
				int listIndex2 = row2.ListIndex;

				if ( listIndex1 < listIndex2 )
					return -1;
				else if ( listIndex1 > listIndex2 )
					return 1;
				else
					return 0;
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//public int Compare( object a, object b )
			//{
			//    Infragistics.Win.UltraWinGrid.UltraGridRow row1 = a as UltraGridRow;
			//    Infragistics.Win.UltraWinGrid.UltraGridRow row2 = b as UltraGridRow;
			public int Compare( UltraGridRow row1, UltraGridRow row2 )
			{
				if ( null != row1 && null != row2 )
				{
					// SSP 1/17/02
					// Use the newly created AncestorRowIndexComparer method which
					// takes into account the list indexes of all the ancestor rows
					// in determinining which comes first.
					//
					return this.AncestorRowIndexComparer( row1, row2 );

					
				}
				else
				{
					Debug.Assert( false, "a and b should have been Row instances" );
				}

				return -1;
			}
		}

		// SSP 9/21/06 BR15379
		// Moved code for raising min rows violated error event into a separate method from InternalDeleteRowsHelper.
		// 
		internal void RaiseMinRowsViolatedError( UltraGridRow rowOptional, int minRowsForBand )
		{
			UltraGrid ultraGrid = this.Grid as UltraGrid;
			if( ultraGrid != null )
			{
				string errorMsg = SR.GetString(
					"LE_UltraGridLayout_MinRowsViolation", // "There must be at least {0} row(s) in this row collection."
					minRowsForBand
					);
				DataErrorInfo  errorInfo	  = new DataErrorInfo( null, null, rowOptional, null, DataErrorSource.RowDelete, errorMsg );
				ErrorEventArgs errorEventArgs = new ErrorEventArgs( errorInfo );
				ultraGrid.FireEvent( GridEventIds.Error, errorEventArgs );
			}
		}

		// Deletes all rows in the passed in rows collection
		//
		// SSP 4/25/03 UWG2185
		// Added an overload of DeleteSelectedRows that takes in a parameter that specifies whether to
		// display the delete confirmation dialog.
		//
		//internal void InternalDeleteRowsHelper( SelectedRowsCollection Rows )
		internal void InternalDeleteRowsHelper( SelectedRowsCollection Rows, bool displayPrompt ) 
		{
			int i;

			// if there are no rows then just return
			//
			if ( Rows.Count < 1 ) return;

			UltraGrid gridctl = this.Grid as UltraGrid;


			if ( null == gridctl )
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_180"));

			// SSP 11/7/01 UWG655
			// We are supposed to use the BeforeRowDeletedEventArgs's DisplayPromptMsg
			// property to determine wheter to show the message or not because we are
			// giving the user ability to cancel it by setting it to false.
			//
			// SSP 4/25/03 UWG2185
			// Commented out below code.
			//
			//bool displayPromptMsg = true;

			UltraGridRow[] rowArray = new UltraGridRow[Rows.Count];
			for( i = 0; i < Rows.Count; i++ )
				rowArray[i] = Rows[i];

			// --------------------------------------------------
			// JAS 2005 v2 XSD Support - Moved this sorting logic up from below. 
			//
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList rowsToDelete = new ArrayList( rowArray.Length );
			List<UltraGridRow> rowsToDelete = new List<UltraGridRow>( rowArray.Length );

			for ( i = 0; i < rowArray.Length; i++ )
			{
				if ( null != rowArray[i] )
					rowsToDelete.Add( rowArray[i] );
			}

			// Before deleting the rows, sort them according to their positions in the
			// binding list because when we delete them, we want to delete starting out
			// from the row with the highest list index to the lowest list index to 
			// counteract the effect of shifting the rows when a row is deleted.
			// 
			rowsToDelete.Sort( new ListIndexComparer( ) );

			// If the number of rows that will exist in any data island after the deletion
			// is less than the MinRows constraint, then fire the Error event and exit this method.
			//			
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//int	minRowsForBand = this.GetMinRowsForBand( rowArray );
			int minRowsForBand = UltraGridLayout.GetMinRowsForBand( rowArray );

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//if( this.MinRowsConstraintWillBeViolatedIfTheseRowsAreDeleted( rowsToDelete, minRowsForBand ) )
			if ( UltraGridLayout.MinRowsConstraintWillBeViolatedIfTheseRowsAreDeleted( rowsToDelete, minRowsForBand ) )
			{
				Debug.Assert( minRowsForBand > 0, "The MinRows constraint should not be violated if it is set to 0!" );
				// SSP 9/21/06 BR15379
				// Moved code for raising min rows violated error event into a separate method from InternalDeleteRowsHelper.
				// 
				this.RaiseMinRowsViolatedError( null, minRowsForBand );
				return;
			}	
			// --------------------------------------------------

			// fire the BeforeRowsDeleted event
			//
			// SSP 4/25/03 UWG2185
			// Added an overload of DeleteSelectedRows that takes in a parameter that specifies whether to
			// display the delete confirmation dialog.
			//
			//BeforeRowsDeletedEventArgs eventArgs = new BeforeRowsDeletedEventArgs( rowArray, displayPromptMsg );
			BeforeRowsDeletedEventArgs eventArgs = new BeforeRowsDeletedEventArgs( rowArray, displayPrompt );
			
			gridctl.FireEvent( GridEventIds.BeforeRowsDeleted, eventArgs );
			
			if ( eventArgs.Cancel )
				return;
			
			// SSP 11/7/01 UWG655
			// Look at the notes above.
			//
			if ( eventArgs.DisplayPromptMsg )
			{
				DialogResult result;
				// SSP 11/29/01 UWG774
				// Use plural if more than 1 row.
				//

				// AS 2/20/02
				// Add localization support.
				//
				//string msgString = "You have selected " + Rows.Count.ToString() + " row" + ( Rows.Count > 1 ? "s" : string.Empty ) + " for deletion.\nChoose Yes to delete the row" + ( Rows.Count > 1 ? "s" : string.Empty ) + " or No to exit.";
				string msgString;

				// JAS BR05288 7/29/05 - If only one row is being deleted, the msgbox's
				// caption should be "Delete Row" instead of "Delete Rows".
				string captionString;

				if (Rows.Count > 1)
				{
					msgString = SR.GetString("DeleteMultipleRowsPrompt", Rows.Count);
					captionString = SR.GetString("DeleteRowsMessageTitle");
				}
				else
				{
					msgString = SR.GetString("DeleteSingleRowPrompt");
					captionString = SR.GetString("DeleteSingleRowMessageTitle");
				}

				result = MessageBox.Show(
					msgString, 
					captionString, 
					MessageBoxButtons.YesNo, MessageBoxIcon.Question );		
				if ( result != DialogResult.Yes )
					return;
			}

			
			// JAS 2005 v2 XSD Support
			// Moved this logic up.
			
//			ArrayList rowsToDelete = new ArrayList( rowArray.Length );
//			for ( i = 0; i < rowArray.Length; i++ )
//			{
//				if ( null != rowArray[i] )
//					rowsToDelete.Add( rowArray[i] );
//			}
//
//			// Before deleting the rows, sort them according to their positions in the
//			// binding list because when we delete them, we want to delete starting out
//			// from the row with the highest list index to the lowest list index to 
//			// counteract the effect of shifting the rows when a row is deleted.
//			// 
//			rowsToDelete.Sort( new ListIndexComparer( ) );

            // MBS 11/18/08 - TFS10399
            // Fire the BeforeSelectChange before deleting the rows, even though
            // we won't honor it if the user cancels the event.  Since we clear
            // the selection anyway when deleting all selected rows, we can just pass
            // in a blank Selected object
            Selected selected = new Selected();
            gridctl.FireEvent(GridEventIds.BeforeSelectChange, new BeforeSelectChangeEventArgs(typeof(UltraGridRow), selected));

			for ( i = rowsToDelete.Count - 1; i >= 0; i-- )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//UltraGridRow row = (UltraGridRow)rowsToDelete[i];
				UltraGridRow row = rowsToDelete[ i ];

				try
				{
					row.DeleteHelper( );
				}
				catch ( Exception e )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use the cached row
					//DataErrorInfo dataError = new DataErrorInfo( e, null, (UltraGridRow)rowsToDelete[i],
					DataErrorInfo dataError = new DataErrorInfo( e, null, row,
						null, DataErrorSource.RowDelete, 
						SR.GetString("DataErrorDeleteRowUnableToDelete", e.Message) ); //"Unable to delete the row:\n" + e.Message );

					UltraGrid grid = this.Grid as UltraGrid;
					if ( null != grid )
						grid.InternalHandleDataError( dataError );
				}

				// SSP 8/20/01 UWG163
				// Once we delete a row, we need to delete it from the selected
				// rows collection too.
				//Rows.Remove( row );
			}

			// SSP 8/20/01 UWG203
			// Clears the selection after deleting selected rows
			gridctl.ClearAllSelected( );

            // MBS 11/18/08 - TFS10399
            // Fire the AfterSelectChange event
            gridctl.FireEvent(GridEventIds.AfterSelectChange, new AfterSelectChangeEventArgs(typeof(UltraGridRow)));

			//RobA 10/3/01 UWG442
			// if we actually deleted rows fire the AfterRowsDeleted event
			//
			gridctl.FireEvent( GridEventIds.AfterRowsDeleted, null );
			
			this.RowScrollRegions.DirtyAllVisibleRows( false );
			this.DirtyGridElement( );
		}

		// JAS 2005 v2 XSD Support
		//
		#region MinRowsConstraintWillBeViolatedIfTheseRowsAreDeleted

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal bool MinRowsConstraintWillBeViolatedIfTheseRowsAreDeleted( ArrayList rowsToDelete, int minRowsForBand )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal static bool MinRowsConstraintWillBeViolatedIfTheseRowsAreDeleted( ArrayList rowsToDelete, int minRowsForBand )
		internal static bool MinRowsConstraintWillBeViolatedIfTheseRowsAreDeleted( List<UltraGridRow> rowsToDelete, int minRowsForBand )
		{
			RowsCollection	currentTopLevelRowsCollection	= null;
			UltraGridRow	currentRow						= null;
			int				numRowsFoundInSameDataIsland    = 0;
			int				numRowsInDataIsland				= 0;
			for( int i = 0; i < rowsToDelete.Count; ++i )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//currentRow = rowsToDelete[i] as UltraGridRow;
				currentRow = rowsToDelete[ i ];

				if( currentRow == null )
					continue;

				if( currentTopLevelRowsCollection == null )
				{
					currentTopLevelRowsCollection = currentRow.ParentCollection.TopLevelRowsCollection;
					numRowsInDataIsland			  = currentTopLevelRowsCollection.UnSortedActualRows.Count;
				}

				if( currentTopLevelRowsCollection == currentRow.ParentCollection.TopLevelRowsCollection )
				{
					++numRowsFoundInSameDataIsland;
					int numRowsInDataIslandAfterDeletion = numRowsInDataIsland - numRowsFoundInSameDataIsland;
					if( numRowsInDataIslandAfterDeletion < minRowsForBand )
						return true;
				}
				else
				{
					// Once we have finished processing all of the rows in a data island and determined that
					// the MinRows constraint will not be violated by the deletion, we need to reset the 
					// variables used by this loop.  The loop index 'i' is decremented so that the current row
					// will be processed again and treated as the first row in it's data island.
					//
					currentTopLevelRowsCollection = null;
					numRowsFoundInSameDataIsland  = 0;
					--i;
				}				
			}

			return false;
		}

		#endregion // MinRowsConstraintWillBeViolatedIfTheseRowsAreDeleted

		// JAS 2005 v2 XSD Support
		//
		#region GetMinRowsForBand

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private int GetMinRowsForBand( UltraGridRow[] rowArray )
		private static int GetMinRowsForBand( UltraGridRow[] rowArray )
		{
			for( int i = 0; i < rowArray.Length; ++i )
				if( rowArray[i] != null )
					return rowArray[i].Band.MinRows;

			return -1;
		}

		#endregion // GetMinRowsForBand



		internal Rectangle GetFormRect()
		{
			// JJD 10/23/01
			// Since FindForm requires certain minimum access
			// wrap it in a try/catch block
			//
			System.Windows.Forms.Form form = null;
			
			try
			{
				form = this.Grid.FindForm();
			}
			catch ( Exception )
			{
			}

			if ( null == form )				
				return this.ContainerRect;

			return this.GetExternalWindowRect ( form );
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal Form Form
		{
			get
			{
				if ( null == this.grid )
					return null;

				// JJD 10/23/01
				// Since FindForm requires certain minimum access
				// wrap it in a try/catch block
				//
				try
				{
					return this.Grid.FindForm();
				}
				catch ( Exception )
				{
					return null;
				}
			}
		}

		internal Rectangle GetExternalWindowRect ( Control externalControl )
		{
			Debug.Assert( null != this.grid, "no grid in Layout.GetExternalWindowRect" );
			

			Rectangle gridRect = Rectangle.Empty;
			Rectangle externalRect = Rectangle.Empty;

			gridRect = grid.RectangleToScreen( grid.ClientRectangle );
			
			// What else can we do but return the grid's own rect
			//
			if ( null == externalControl )
				return gridRect;


			externalRect = externalControl.RectangleToScreen( externalControl.ClientRectangle );

			externalRect.X -= gridRect.X;
			externalRect.Y -= gridRect.Y;
			
			return externalRect;
		}

		[ Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
		internal Rectangle ContainerRect
		{
			get
			{
				// JJD 12/04/01
				// Wrap parent get in try/catch in case we don't have
				// security rights
				//
				try
				{
					return this.GetExternalWindowRect ( this.Grid.Parent );
				}
				catch
				{
					return this.ClientRect;
				}
			}
		}
		internal void ActivateNextRegion()
		{
			// get the current active col scroll regions
			//
			ColScrollRegion csr = this.grid.ActiveColScrollRegion;
			RowScrollRegion rsr = this.grid.ActiveRowScrollRegion;

			if ( csr == null || rsr == null || this.grid ==  null )
				return;

			// try to get the next col region (fWrap set to false)
			//
			ColScrollRegion nextColRegion = this.colScrollRegions.GetNextVisibleRegion( csr, false );

			if ( nextColRegion == null )
			{
				// since there wasn't any more visible regions to the right of the 
				// current active one try to get the next visible row region
				// this time with fWrap set to true.
				//

				RowScrollRegion nextRowRegion = this.rowScrollRegions.GetNextVisibleRegion( rsr, true);

				// if we have another visible row region set it as the current active row region
				//
				if ( nextRowRegion != null && nextRowRegion != rsr )
					this.grid.ActiveRowScrollRegion = nextRowRegion;
					
				// pass NULL into GetNextVisibleRegion to get the first visible col scroll region
				//
				nextColRegion = this.colScrollRegions.GetNextVisibleRegion( null, false );

			}

			// if the col scroll region has changed then set it as the active one
			//
			if ( nextColRegion != null && nextColRegion != csr )
				this.grid.ActiveColScrollRegion = nextColRegion;
		}
		internal void ActivatePreviousRegion()
		{
			// get the current active col scroll regions
			//
			ColScrollRegion csr = this.grid.ActiveColScrollRegion;
			RowScrollRegion rsr = this.grid.ActiveRowScrollRegion;

			if ( csr == null || rsr ==null || this.grid == null )
				return;

			// try to get the Previous col region (fWrap set to false)
			//
			ColScrollRegion previousColRegion = this.colScrollRegions.GetPreviousVisibleRegion ( csr, false );

			if ( previousColRegion == null )
			{
				// since there wasn't any more visible regions to the right of the 
				// current active one try to get the Previous visible row region
				// this time with fWrap set to true.
				//
				RowScrollRegion previousRowRegion = this.rowScrollRegions.GetPreviousVisibleRegion ( rsr, true );

				// if we have another visible row region set it as the current active row region
				//
				if ( previousRowRegion != null && previousRowRegion != rsr )
					this.grid.ActiveRowScrollRegion = previousRowRegion;

				// pass NULL into GetPreviousVisibleRegion to get the first visible col scroll region
				//
				previousColRegion = this.colScrollRegions.GetPreviousVisibleRegion ( null, false );
			}

			// if the col scroll region has changed then set it as the active one
			//
			if ( previousColRegion != null && previousColRegion != csr )
				this.grid.ActiveColScrollRegion = previousColRegion;
		}


		// SSP 4/26/02
		// EM Embeddable editors.
		// Commented out SetEditCtlText.
		//
		

		internal bool GettingFirstCell
		{
			get
			{ 
				return this.gettingFirstCell;
			}
		}

		internal bool TogglingByKybd
		{
			get
			{
				return this.togglingByKybd;
			}
		}


		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
			PropertyCategories serializeFlags = context.Context is PropertyCategories ? 
				(Infragistics.Win.UltraWinGrid.PropertyCategories)context.Context :
				PropertyCategories.All;
		
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			if ( this.ShouldSerializeBorderStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyle", this.BorderStyle );
				//info.AddValue("BorderStyle", (int)this.BorderStyle );
			}

			if ( this.ShouldSerializeKey() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Key", this.Key );
				//info.AddValue("Key", this.Key );
			}

			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	//info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			if ( this.ShouldSerializeAddNewBox() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "AddNewBox", this.AddNewBox );
				//info.AddValue("AddNewBox", this.AddNewBox );
			}

			// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
			// Obsoleted AutoFitColumns and added AutoFitStyle property.
			//
			// ------------------------------------------------------------------------
			if ( this.ShouldSerializeAutoFitStyle( ) )
			{
				Utils.SerializeProperty( info, "AutoFitStyle", this.autoFitStyle );
			}
			
			// ------------------------------------------------------------------------

			// SSP 9/27/01
			//
			if ( this.ShouldSerializeGroupByBox( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "GroupByBox", this.GroupByBox );
				//info.AddValue( "GroupByBox", this.GroupByBox );
			}

			//			if ( this.ShouldSerializeAlphaBlendEnabled() )
			//				info.AddValue("AlphaBlendEnabled", this.AlphaBlendEnabled );

			//		if ( this.ShouldSerializeAppearance() )
			//		info.AddValue("Appearance", this.Appearance );
			
			if ( this.ShouldSerializeBorderStyleCaption() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BorderStyleCaption", this.BorderStyleCaption );
				//info.AddValue("BorderStyleCaption", (int)this.BorderStyleCaption );
			}

			
			//appearances
			// SSP 10/10/02 UWG1222
			// Only serialize the appearance if the property categories include General flag.
			//
			//if ( this.appearanceHolders != null )
			if ( 0 != ( PropertyCategories.General & serializeFlags ) &&
				this.appearanceHolders != null )
			{
				for ( int i = 0; i < this.appearanceHolders.Length; i++ )
				{
					if ( this.appearanceHolders[i] != null &&
						this.appearanceHolders[i].ShouldSerialize() )
					{
						// JJD 8/20/02
						// Use SerializeProperty static method to serialize properties instead.
						Utils.SerializeProperty( info, i.ToString(), this.appearanceHolders[i] );
						//info.AddValue( i.ToString(), this.appearanceHolders[i] );
					}
				}
			}
			
			if ( this.ShouldSerializeInterBandSpacing() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "InterBandSpacing", this.InterBandSpacing );
				//info.AddValue("InterBandSpacing", this.InterBandSpacing );
			}

			if ( this.ShouldSerializeMaxColScrollRegions() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaxColScrollRegions", this.MaxColScrollRegions );
				//info.AddValue("MaxColScrollRegions", this.MaxColScrollRegions );
			}

			if ( this.ShouldSerializeMaxRowScrollRegions() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaxRowScrollRegions", this.MaxRowScrollRegions );
				//info.AddValue("MaxRowScrollRegions", this.MaxRowScrollRegions );
			}

			if ( this.ShouldSerializeOverride() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Override", this.Override );
				//info.AddValue("Override", this.Override );
			}

			if ( this.ShouldSerializeRowConnectorColor() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowConnectorColor", this.RowConnectorColor );
				//info.AddValue("RowConnectorColor",this.RowConnectorColor );
			}

			if ( this.ShouldSerializeRowConnectorStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowConnectorStyle", this.RowConnectorStyle );
				//info.AddValue("RowConnectorStyle", (int)this.RowConnectorStyle );
			}
			
			if ( this.ShouldSerializeScrollbars() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Scrollbars", this.Scrollbars );
				//info.AddValue("Scrollbars", (int)this.Scrollbars );
			}

			if ( this.ShouldSerializeTabNavigation() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "TabNavigation", this.TabNavigation );
				//info.AddValue("TabNavigation", (int)this.TabNavigation );
			}

			if ( this.ShouldSerializeViewStyle() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ViewStyle", this.ViewStyle );
				//info.AddValue("ViewStyle", (int)this.ViewStyle );
			}

			if ( this.ShouldSerializeViewStyleBand() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ViewStyleBand", this.ViewStyleBand );
				//info.AddValue("ViewStyleBand", (int)this.ViewStyleBand );
			}

			// SSP 6/11/02
			// Added code to serialize MaxBandDepth, PriorityScrolling and ScrollStyle
			// properties.
			//
			// -----------------------------------------------------------------------
			if ( this.ShouldSerializeMaxBandDepth( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "MaxBandDepth", this.MaxBandDepth );
				//info.AddValue("MaxBandDepth", (int)this.MaxBandDepth );
			}

			if ( this.ShouldSerializeScrollStyle( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ScrollStyle", this.ScrollStyle );
				//info.AddValue("ScrollStyle", (int)this.ScrollStyle );
			}

			if ( this.ShouldSerializePriorityScrolling( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "PriorityScrolling", this.PriorityScrolling );
				//info.AddValue("PriorityScrolling", (bool)this.PriorityScrolling );
			}
			// -----------------------------------------------------------------------

			// SSP 8/9/02 UWG1201
			// Serialize the scroll bar look property
			//
			if ( 0 != ( PropertyCategories.General & serializeFlags ) &&
				this.ShouldSerializeScrollBarLook( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ScrollBarLook", this.ScrollBarLook );
				//info.AddValue( "ScrollBarLook", this.ScrollBarLook );
			}

			if (  ( (serializeFlags & PropertyCategories.AppearanceCollection) != 0 ) && 
				this.ShouldSerializeAppearances() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Appearances", this.Appearances );
				//info.AddValue("Appearances", this.Appearances);
			}
			 
			if (  ( (serializeFlags & PropertyCategories.Bands) != 0 ) && 
				this.ShouldSerializeBands() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Bands", this.SortedBands );
				//info.AddValue("Bands", this.Bands );
			}

			if ( ( (serializeFlags & PropertyCategories.ColScrollRegions) != 0 ) &&
				this.ShouldSerializeColScrollRegions() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ColScrollRegions", this.ColScrollRegions );
				//info.AddValue("ColScrollRegions", this.ColScrollRegions );
			}

			if ( ( (serializeFlags & PropertyCategories.RowScrollRegions) != 0 ) &&
				this.ShouldSerializeRowScrollRegions() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "RowScrollRegions",this.RowScrollRegions );
				//info.AddValue("RowScrollRegions",this.RowScrollRegions );
			}

			if ( ( (serializeFlags & PropertyCategories.ValueLists) != 0 ) &&
				this.ShouldSerializeValueLists() )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ValueLists", this.ValueLists );
				//info.AddValue("ValueLists", this.ValueLists );
			}

			// SSP 7/26/02
			// Also serialize the sigma and filter drop down images.
			//
			// --------------------------------------------------------------------------------
			if ( this.ShouldSerializeSummaryButtonImage( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "SummaryButtonImage", this.SummaryButtonImage );
				//info.AddValue( "SummaryButtonImage", this.SummaryButtonImage );
			}

			if ( this.ShouldSerializeFilterDropDownButtonImage( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "FilterDropDownButtonImage", this.FilterDropDownButtonImage );
				//info.AddValue( "FilterDropDownButtonImage", this.FilterDropDownButtonImage );
			}

			if ( this.ShouldSerializeFilterDropDownButtonImageActive( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "FilterDropDownButtonImageActive", this.FilterDropDownButtonImageActive );
				//info.AddValue( "FilterDropDownButtonImageActive", this.FilterDropDownButtonImageActive );
			}
			// --------------------------------------------------------------------------------

			// SSP 5/19/03 - Fixed headers
			//
			// --------------------------------------------------------------------------------
			if ( this.ShouldSerializeUseFixedHeaders( ) )
			{
				Utils.SerializeProperty( info, "UseFixedHeaders", this.useFixedHeaders );
			}

			if ( this.ShouldSerializeFixedHeaderOnImage( ) )
			{
				Utils.SerializeProperty( info, "FixedHeaderOnImage", this.FixedHeaderOnImage );
			}

			if ( this.ShouldSerializeFixedHeaderOffImage( ) )
			{
				Utils.SerializeProperty( info, "FixedHeaderOffImage", this.FixedHeaderOffImage );
			}
			// --------------------------------------------------------------------------------

			// SSP 3/29/05 - NAS 5.2 Fixed Rows
			//
			// --------------------------------------------------------------------------------
			if ( this.ShouldSerializeFixedRowOnImage( ) )
			{
				Utils.SerializeProperty( info, "FixedRowOnImage", this.FixedRowOnImage );
			}

			if ( this.ShouldSerializeFixedRowOffImage( ) )
			{
				Utils.SerializeProperty( info, "FixedRowOffImage", this.FixedRowOffImage );
			}
			// --------------------------------------------------------------------------------

			// SSP 5/22/03
			// Added a property to allow the user to control the small change of the column scroll bar.
			//
			if ( this.ShouldSerializeColumnScrollbarSmallChange( ) )
			{
				Utils.SerializeProperty( info, "ColumnScrollbarSmallChange", this.columnScrollbarSmallChange );
			}


			// SSP 8/4/03
			// Serialize the UseScrollWindow property.
			//
			if ( this.ShouldSerializeUseScrollWindow( ) )
			{
				Utils.SerializeProperty( info, "UseScrollWindow", this.useScrollWindow );
			}

			// SSP 11/24/03 - Scrolling Till Last Row Visible
			// Added ScrollBounds property.
			//
			if ( this.ShouldSerializeScrollBounds( ) )
			{
				Utils.SerializeProperty( info, "ScrollBounds", this.scrollBounds );
			}

			// SSP 4/13/04 - Virtual Binding
			// Added LoadStyle property to UltraGridLayout.
			//
			if ( this.ShouldSerializeLoadStyle( ) )
			{
				Utils.SerializeProperty( info, "LoadStyle", this.loadStyle );
			}

			// JAS 4/5/05 v5.2 CaptionVisible property
			//
			if( this.ShouldSerializeCaptionVisible() )
			{
				Utils.SerializeProperty( info, "CaptionVisible", this.captionVisible );
			}

			// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
			// 
			if ( this.ShouldSerializeNewColumnLoadStyle( ) )
			{
				Utils.SerializeProperty( info, "NewColumnLoadStyle", this.newColumnLoadStyle );
			}

			if ( this.ShouldSerializeNewBandLoadStyle( ) )
			{
				Utils.SerializeProperty( info, "NewBandLoadStyle", this.newBandLoadStyle );
			}

			// SSP 6/30/05 - NAS 5.3 Column Chooser
			//
			if ( this.ShouldSerializeColumnChooserEnabled( ) )
			{
				Utils.SerializeProperty( info, "ColumnChooserEnabled", this.ColumnChooserEnabled );
			}

			// SSP 7/6/05 - NAS 5.3 Empty Rows
			// 
			if ( this.ShouldSerializeEmptyRowSettings( ) )
			{
				Utils.SerializeProperty( info, "EmptyRowSettings", this.EmptyRowSettings );
			}

			// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
			// 
			// ----------------------------------------------------------------------
			if ( this.ShouldSerializeClipboardCellDelimiter( ) )
			{
				Utils.SerializeProperty( info, "ClipboardCellDelimiter", this.clipboardCellDelimiter );
			}

			if ( this.ShouldSerializeClipboardCellSeparator( ) )
			{
				Utils.SerializeProperty( info, "ClipboardCellSeparator", this.clipboardCellSeparator );
			}

			if ( this.ShouldSerializeClipboardRowSeparator( ) )
			{
				Utils.SerializeProperty( info, "ClipboardRowSeparator", this.clipboardRowSeparator );
			}
			// ----------------------------------------------------------------------

			// SSP 8/28/06 - NAS 6.3
			// Added CellHottrackInvalidationStyle property on the layout.
			// 
			if ( this.ShouldSerializeCellHottrackInvalidationStyle( ) )
			{
				Utils.SerializeProperty( info, "CellHottrackInvalidationStyle", this.cellHottrackInvalidationStyle );
			}

            // MRS NAS v8.2 - CardView Printing
            if (this.ShouldSerializeAllowCardPrinting())
            {
                Utils.SerializeProperty(info, "AllowCardPrinting", this.allowCardPrinting);
            }

            // MBS 4/17/09 - NA9.2 Selection Overlay
            if (this.ShouldSerializeSelectionOverlayBorderColor())
                Utils.SerializeProperty(info, "SelectionOverlayBorderColor", this.selectionOverlayBorderColor);
            //
            if (this.ShouldSerializeSelectionOverlayColor())
                Utils.SerializeProperty(info, "SelectionOverlayColor", this.selectionOverlayColor);
            //
            if (this.ShouldSerializeSelectionOverlayBorderThickness())
                Utils.SerializeProperty(info, "SelectionOverlayBorderThickness", this.selectionOverlayBorderThickness);

            // CDS 9.2 RowSelector State-Specific Images
            if (this.ShouldSerializeRowSelectorImages())
            {
                Utils.SerializeProperty(info, "RowSelectorImages", this.rowSelectorImages);
            }

            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (DefaultSelectedBackColor & DefaultSelectedForeColor)
            if (this.ShouldSerializeDefaultSelectedBackColor())
            {
                Utils.SerializeProperty(info, "DefaultSelectedBackColor", this.defaultSelectedBackColor);
            }
            //
            if (this.ShouldSerializeDefaultSelectedForeColor())
            {
                Utils.SerializeProperty(info, "DefaultSelectedForeColor", this.defaultSelectedForeColor);
            }
        }

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal UltraGridLayout (SerializationInfo info, StreamingContext context) 
		protected UltraGridLayout (SerializationInfo info, StreamingContext context) 
		{
			PropertyCategories serializeFlags = context.Context is PropertyCategories ? 
				(Infragistics.Win.UltraWinGrid.PropertyCategories)context.Context :
				PropertyCategories.All;

			//since we're not saving properties with default values, we must set the default here
			this.Reset( );

			foreach( SerializationEntry entry in info )
			{

				if ( entry.ObjectType == typeof( Infragistics.Win.AppearanceHolder ) )
				{
					// convert the name to an int
					//
					int index = int.Parse( entry.Name );
				
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					AppearanceHolder appHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );

					if ( appHolder != null && index >= 0 && index < this.AppearanceHolders.Length )
						this.AppearanceHolders[index] = appHolder;
					else
					{
						Debug.Fail("Invalid override appearance index during de-serialization, index = " + index.ToString() );
					}
				}
				else
				{
                    switch (entry.Name)
                    {


                        case "Key":
                            {
                                //this.Key = (string)entry.Value;
                                // AS 8/14/02
                                // Use the disposable object method to allow deserialization from xml, etc.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.Key = (string)Utils.DeserializeProperty(entry, typeof(string), this.Key);
                                //this.Key = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
                                break;
                            }

                        case "Tag":
                            // AS 8/15/02
                            // Use our tag deserializer.
                            //
                            //this.tagValue = entry.Value;
                            this.DeserializeTag(entry);
                            break;

                        case "AutoFitColumns":
                            //this.autoFitColumns = (bool)entry.Value;
                            // AS 8/14/02
                            // Use the disposable object method to allow deserialization from xml, etc.
                            //
                            // JJD 8/19/02
                            // Use the DeserializeProperty static method to de-serialize properties instead.
                            // SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
                            // Obsoleted AutoFitColumns and added AutoFitStyle property.
                            //
                            //this.autoFitColumns = (bool)Utils.DeserializeProperty( entry, typeof(bool), this.autoFitColumns );
                            bool autoFitColumns = (bool)Utils.DeserializeProperty(entry, typeof(bool), AutoFitStyle.ResizeAllColumns == this.autoFitStyle);
                            this.autoFitStyle = autoFitColumns ? AutoFitStyle.ResizeAllColumns : AutoFitStyle.None;
                            //this.autoFitColumns = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
                            break;

                        // SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
                        // Obsoleted AutoFitColumns and added AutoFitStyle property.
                        //
                        case "AutoFitStyle":
                            this.autoFitStyle = (AutoFitStyle)Utils.DeserializeProperty(entry, typeof(AutoFitStyle), this.autoFitStyle);
                            break;

                        case "BorderStyle":
                            {
                                //this.BorderStyle = (Infragistics.Win.UIElementBorderStyle)(int)entry.Value;
                                // AS 8/15/02
                                // Use our enum serialization method to ensure that the
                                // enum value is valid.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.BorderStyle = (UIElementBorderStyle)Utils.DeserializeProperty(entry, typeof(UIElementBorderStyle), this.BorderStyle);
                                //this.BorderStyle = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.BorderStyle);
                                break;
                            }

                        
                        case "BorderStyleCaption":
                            {
                                //this.BorderStyleCaption = (Infragistics.Win.UIElementBorderStyle)(int)entry.Value;
                                // AS 8/15/02
                                // Use our enum serialization method to ensure that the
                                // enum value is valid.
                                //
                                // JJD 8/20/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.BorderStyleCaption = (UIElementBorderStyle)Utils.DeserializeProperty(entry, typeof(UIElementBorderStyle), this.BorderStyleCaption);
                                //this.BorderStyleCaption = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.BorderStyleCaption);
                                break;
                            }


                        case "InterBandSpacing":
                            {
                                //this.InterBandSpacing = (int)entry.Value;
                                // AS 8/14/02
                                // Use the disposable object method to allow deserialization from xml, etc.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.InterBandSpacing = (int)Utils.DeserializeProperty(entry, typeof(int), this.InterBandSpacing);
                                //this.InterBandSpacing = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
                                break;
                            }

                        case "MaxColScrollRegions":
                            {
                                //this.MaxColScrollRegions = (int)entry.Value;
                                // AS 8/14/02
                                // Use the disposable object method to allow deserialization from xml, etc.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.MaxColScrollRegions = (int)Utils.DeserializeProperty(entry, typeof(int), this.MaxColScrollRegions);
                                //this.MaxColScrollRegions = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
                                break;
                            }

                        case "MaxRowScrollRegions":
                            {
                                //this.MaxRowScrollRegions = (int)entry.Value;
                                // AS 8/14/02
                                // Use the disposable object method to allow deserialization from xml, etc.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.MaxRowScrollRegions = (int)Utils.DeserializeProperty(entry, typeof(int), this.MaxRowScrollRegions);
                                //this.MaxRowScrollRegions = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
                                break;
                            }

                        // RobA UWG819 12/3/01
                        //
                        case "NullText":
                            {
                                //							this.NullText = (string)entry.Value;
                                break;
                            }

                        case "RowConnectorColor":
                            {
                                //this.RowConnectorColor = (System.Drawing.Color)entry.Value;
                                // AS 8/14/02
                                // Use the disposable object method to allow deserialization from xml, etc.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.RowConnectorColor = (Color)Utils.DeserializeProperty(entry, typeof(Color), this.RowConnectorColor);
                                //this.RowConnectorColor = (System.Drawing.Color)DisposableObject.ConvertValue( entry.Value, typeof(System.Drawing.Color) );
                                break;
                            }

                        case "RowConnectorStyle":
                            {
                                //this.RowConnectorStyle = (RowConnectorStyle)(int)entry.Value;
                                // AS 8/15/02
                                // Use our enum serialization method to ensure that the
                                // enum value is valid.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.RowConnectorStyle = (RowConnectorStyle)Utils.DeserializeProperty(entry, typeof(RowConnectorStyle), this.RowConnectorStyle);
                                //this.RowConnectorStyle = (RowConnectorStyle)Utils.ConvertEnum(entry.Value, this.RowConnectorStyle);
                                break;
                            }

                        case "TabNavigation":
                            {
                                //this.TabNavigation = ()(int)entry.Value;
                                // AS 8/15/02
                                // Use our enum serialization method to ensure that the
                                // enum value is valid.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.TabNavigation = (TabNavigation)Utils.DeserializeProperty(entry, typeof(TabNavigation), this.TabNavigation);
                                //this.TabNavigation = (TabNavigation)Utils.ConvertEnum(entry.Value, this.TabNavigation);
                                break;
                            }

                        case "ViewStyle":
                            {
                                //this.ViewStyle = (Infragistics.Win.UltraWinGrid.ViewStyle)(int)entry.Value;
                                // AS 8/15/02
                                // Use our enum serialization method to ensure that the
                                // enum value is valid.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.ViewStyle = (ViewStyle)Utils.DeserializeProperty(entry, typeof(ViewStyle), this.ViewStyle);
                                //this.ViewStyle = (ViewStyle)Utils.ConvertEnum(entry.Value, this.ViewStyle);
                                break;
                            }

                        case "ViewStyleBand":
                            {
                                //this.ViewStyleBand = (Infragistics.Win.UltraWinGrid.ViewStyleBand)(int)entry.Value;
                                // AS 8/15/02
                                // Use our enum serialization method to ensure that the
                                // enum value is valid.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.ViewStyleBand = (ViewStyleBand)Utils.DeserializeProperty(entry, typeof(ViewStyleBand), this.ViewStyleBand);
                                //this.ViewStyleBand = (ViewStyleBand)Utils.ConvertEnum(entry.Value, this.ViewStyleBand);
                                break;
                            }

                        case "Scrollbars":
                            {
                                //this.Scrollbars = (Scrollbars)(int)entry.Value;
                                // AS 8/15/02
                                // Use our enum serialization method to ensure that the
                                // enum value is valid.
                                //
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.Scrollbars = (Scrollbars)Utils.DeserializeProperty(entry, typeof(Scrollbars), this.Scrollbars);
                                //this.Scrollbars = (Scrollbars)Utils.ConvertEnum(entry.Value, this.Scrollbars);
                                break;
                            }

                        case "Appearances":
                            {
                                if ((serializeFlags & PropertyCategories.AppearanceCollection) != 0)
                                {
                                    // JJD 8/19/02
                                    // Use the DeserializeProperty static method to de-serialize properties instead.
                                    this.appearances = (AppearancesCollection)Utils.DeserializeProperty(entry, typeof(AppearancesCollection), null);
                                    this.appearances = (Infragistics.Win.AppearancesCollection)entry.Value;
                                }
                                break;
                            }

                        case "AddNewBox":
                            {
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.addNewBox = (AddNewBox)Utils.DeserializeProperty(entry, typeof(AddNewBox), null);
                                //this.addNewBox = (Infragistics.Win.UltraWinGrid.AddNewBox)entry.Value;
                                break;
                            }

                        case "GroupByBox":
                            {
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.groupByBox = (GroupByBox)Utils.DeserializeProperty(entry, typeof(GroupByBox), null);
                                //this.groupByBox = (Infragistics.Win.UltraWinGrid.GroupByBox)entry.Value;
                                break;
                            }

                        case "Bands":
                            {
                                if ((serializeFlags & PropertyCategories.Bands) != 0)
                                {
                                    // JJD 8/19/02
                                    // Use the DeserializeProperty static method to de-serialize properties instead.
                                    //this.bands = (Infragistics.Win.UltraWinGrid.BandsCollection)entry.Value;
                                    // MRS 5/29/2009 - TFS17970
                                    //this.sortedBands = (BandsCollection)Utils.DeserializeProperty( entry, typeof(BandsCollection), null );
                                    this.bands = (BandsCollection)Utils.DeserializeProperty(entry, typeof(BandsCollection), null);
                                }
                                break;
                            }


                        case "ColScrollRegions":
                            {
                                if ((serializeFlags & PropertyCategories.ColScrollRegions) != 0)
                                {
                                    if (this.colScrollRegions != null)
                                        this.colScrollRegions.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

                                    // JJD 8/19/02
                                    // Use the DeserializeProperty static method to de-serialize properties instead.
                                    this.colScrollRegions = (ColScrollRegionsCollection)Utils.DeserializeProperty(entry, typeof(ColScrollRegionsCollection), null);
                                    //this.colScrollRegions = (Infragistics.Win.UltraWinGrid.ColScrollRegionsCollection)entry.Value;

                                    if (this.colScrollRegions != null)
                                        this.colScrollRegions.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                                }
                                break;
                            }

                        case "RowScrollRegions":
                            {
                                if ((serializeFlags & PropertyCategories.RowScrollRegions) != 0)
                                {
                                    if (this.rowScrollRegions != null)
                                        this.rowScrollRegions.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

                                    // JJD 8/19/02
                                    // Use the DeserializeProperty static method to de-serialize properties instead.
                                    this.rowScrollRegions = (RowScrollRegionsCollection)Utils.DeserializeProperty(entry, typeof(RowScrollRegionsCollection), null);
                                    //this.rowScrollRegions = (Infragistics.Win.UltraWinGrid.RowScrollRegionsCollection)entry.Value;

                                    if (this.rowScrollRegions != null)
                                        this.rowScrollRegions.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                                }
                                break;
                            }

                        case "ValueLists":
                            {
                                if ((serializeFlags & PropertyCategories.ValueLists) != 0)
                                {
                                    // JJD 8/19/02
                                    // Use the DeserializeProperty static method to de-serialize properties instead.
                                    this.valuelists = (ValueListsCollection)Utils.DeserializeProperty(entry, typeof(ValueListsCollection), null);
                                    //this.valuelists = (Infragistics.Win.ValueListsCollection)entry.Value;
                                }
                                break;
                            }

                        case "Override":
                            {
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.overrideObj = (UltraGridOverride)Utils.DeserializeProperty(entry, typeof(UltraGridOverride), null);
                                //this.overrideObj = (Infragistics.Win.UltraWinGrid.UltraGridOverride)entry.Value;
                                break;
                            }


                        // AS - 12/19/01
                        case "ScrollBarLook":
                            {
                                // JJD 8/19/02
                                // Use the DeserializeProperty static method to de-serialize properties instead.
                                this.scrollbarLook = (ScrollBarLook)Utils.DeserializeProperty(entry, typeof(ScrollBarLook), null);
                                //this.scrollbarLook = (ScrollBarLook)entry.Value;
                                break;
                            }
                        // SSP 6/11/02
                        // Added code to serialize MaxBandDepth, PriorityScrolling and ScrollStyle
                        // properties.
                        //
                        case "MaxBandDepth":
                            //this.maxBandDepth = (int)entry.Value;
                            // AS 8/14/02
                            // Use the disposable object method to allow deserialization from xml, etc.
                            //
                            // JJD 8/19/02
                            // Use the DeserializeProperty static method to de-serialize properties instead.
                            this.maxBandDepth = (int)Utils.DeserializeProperty(entry, typeof(int), this.maxBandDepth);
                            //this.maxBandDepth = (int)DisposableObject.ConvertValue( entry.Value, typeof(int) );
                            break;
                        case "PriorityScrolling":
                            //this.priorityScrolling = (bool)entry.Value;
                            // AS 8/14/02
                            // Use the disposable object method to allow deserialization from xml, etc.
                            //
                            // JJD 8/19/02
                            // Use the DeserializeProperty static method to de-serialize properties instead.
                            this.priorityScrolling = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.priorityScrolling);
                            //this.priorityScrolling = (bool)DisposableObject.ConvertValue( entry.Value, typeof(bool) );
                            break;
                        case "ScrollStyle":
                            //this.scrollStyle = (Infragistics.Win.UltraWinGrid.ScrollStyle)(int)entry.Value;
                            // AS 8/15/02
                            // Use our enum serialization method to ensure that the
                            // enum value is valid.
                            //
                            // JJD 8/19/02
                            // Use the DeserializeProperty static method to de-serialize properties instead.
                            this.scrollStyle = (ScrollStyle)Utils.DeserializeProperty(entry, typeof(ScrollStyle), this.scrollStyle);
                            //this.scrollStyle = (ScrollStyle)Utils.ConvertEnum(entry.Value, this.scrollStyle);
                            break;
                        // SSP 7/26/02
                        // Also serialize the sigma and filter drop down images.
                        //
                        // --------------------------------------------------------------------------------
                        case "SummaryButtonImage":
                            //this.SummaryButtonImage = (Image)entry.Value;
                            // AS 8/14/02
                            // Use the disposable object method to allow deserialization from xml, etc.
                            //
                            // JJD 8/19/02
                            // Use the DeserializeProperty static method to de-serialize properties instead.
                            this.SummaryButtonImage = (System.Drawing.Image)Utils.DeserializeProperty(entry, typeof(System.Drawing.Image), null);
                            //this.SummaryButtonImage = (System.Drawing.Image)DisposableObject.ConvertValue( entry.Value, typeof(System.Drawing.Image) );
                            break;
                        case "FilterDropDownButtonImage":
                            //this.FilterDropDownButtonImage = (Image)entry.Value;
                            // AS 8/14/02
                            // Use the disposable object method to allow deserialization from xml, etc.
                            //
                            // JJD 8/19/02
                            // Use the DeserializeProperty static method to de-serialize properties instead.
                            this.FilterDropDownButtonImage = (System.Drawing.Image)Utils.DeserializeProperty(entry, typeof(System.Drawing.Image), null);
                            //this.FilterDropDownButtonImage = (System.Drawing.Image)DisposableObject.ConvertValue( entry.Value, typeof(System.Drawing.Image) );
                            break;
                        case "FilterDropDownButtonImageActive":
                            //this.FilterDropDownButtonImageActive = (Image)entry.Value;
                            // AS 8/14/02
                            // Use the disposable object method to allow deserialization from xml, etc.
                            //
                            // JJD 8/19/02
                            // Use the DeserializeProperty static method to de-serialize properties instead.
                            this.FilterDropDownButtonImageActive = (System.Drawing.Image)Utils.DeserializeProperty(entry, typeof(System.Drawing.Image), null);
                            //this.FilterDropDownButtonImageActive = (System.Drawing.Image)DisposableObject.ConvertValue( entry.Value, typeof(System.Drawing.Image) );
                            break;
                        // --------------------------------------------------------------------------------

                        // SSP 5/19/03 - Fixed headers
                        //
                        // --------------------------------------------------------------------------------
                        case "UseFixedHeaders":
                            this.useFixedHeaders = (bool)Utils.DeserializeProperty(entry, typeof(bool), this.useFixedHeaders);
                            break;
                        case "FixedHeaderOnImage":
                            this.FixedHeaderOnImage = (System.Drawing.Image)Utils.DeserializeProperty(entry, typeof(System.Drawing.Image), null);
                            break;
                        case "FixedHeaderOffImage":
                            this.FixedHeaderOffImage = (System.Drawing.Image)Utils.DeserializeProperty(entry, typeof(System.Drawing.Image), null);
                            break;
                        // --------------------------------------------------------------------------------
                        // SSP 3/29/05 - NAS 5.2 Fixed Rows
                        //
                        // --------------------------------------------------------------------------------
                        case "FixedRowOnImage":
                            this.FixedRowOnImage = (System.Drawing.Image)Utils.DeserializeProperty(entry, typeof(System.Drawing.Image), null);
                            break;
                        case "FixedRowOffImage":
                            this.FixedRowOffImage = (System.Drawing.Image)Utils.DeserializeProperty(entry, typeof(System.Drawing.Image), null);
                            break;
                        // --------------------------------------------------------------------------------
                        // SSP 5/22/03
                        // Added a property to allow the user to control the small change of the column scroll bar.
                        //
                        case "ColumnScrollbarSmallChange":
                            this.columnScrollbarSmallChange = (int)Utils.DeserializeProperty(entry, typeof(int), this.columnScrollbarSmallChange);
                            break;
                        // SSP 8/4/03
                        // Serialize the UseScrollWindow method.
                        //
                        case "UseScrollWindow":
                            this.useScrollWindow = (UseScrollWindow)Utils.DeserializeProperty(entry, typeof(UseScrollWindow), this.useScrollWindow);
                            break;
                        // SSP 11/24/03 - Scrolling Till Last Row Visible
                        // Added ScrollBounds property.
                        //
                        case "ScrollBounds":
                            this.scrollBounds = (ScrollBounds)Utils.DeserializeProperty(entry, typeof(ScrollBounds), this.scrollBounds);
                            break;
                        case "LoadStyle":
                            this.loadStyle = (LoadStyle)Utils.DeserializeProperty(entry, typeof(LoadStyle), this.loadStyle);
                            break;

                        // JAS 4/5/05 v5.2 CaptionVisible property
                        //
                        case "CaptionVisible":
                            this.captionVisible = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.captionVisible);
                            break;
                        // SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
                        // 
                        case "NewColumnLoadStyle":
                            this.newColumnLoadStyle = (NewColumnLoadStyle)Utils.DeserializeProperty(entry, typeof(NewColumnLoadStyle), this.newColumnLoadStyle);
                            break;
                        case "NewBandLoadStyle":
                            this.newBandLoadStyle = (NewBandLoadStyle)Utils.DeserializeProperty(entry, typeof(NewBandLoadStyle), this.newBandLoadStyle);
                            break;
                        // SSP 6/30/05 - NAS 5.3 Column Chooser
                        //
                        case "ColumnChooserEnabled":
                            this.columnChooserEnabled = (DefaultableBoolean)Utils.DeserializeProperty(entry, typeof(DefaultableBoolean), this.columnChooserEnabled);
                            break;
                        // SSP 7/6/05 - NAS 5.3 Empty Rows
                        // 
                        case "EmptyRowSettings":
                            this.emptyRowSettings = (EmptyRowSettings)Utils.DeserializeProperty(entry, typeof(EmptyRowSettings), this.emptyRowSettings);
                            break;
                        // SSP 11/15/05 - NAS 6.1 Multi-cell Operations
                        // 
                        // ----------------------------------------------------------------------
                        case "ClipboardCellDelimiter":
                            this.clipboardCellDelimiter = (string)Utils.DeserializeProperty(entry, typeof(string), this.clipboardCellDelimiter);
                            break;
                        case "ClipboardCellSeparator":
                            this.clipboardCellSeparator = (string)Utils.DeserializeProperty(entry, typeof(string), this.clipboardCellSeparator);
                            break;
                        case "ClipboardRowSeparator":
                            this.clipboardRowSeparator = (string)Utils.DeserializeProperty(entry, typeof(string), this.clipboardRowSeparator);
                            break;
                        // ----------------------------------------------------------------------
                        // SSP 8/28/06 - NAS 6.3
                        // Added CellHottrackInvalidationStyle property on the layout.
                        // 
                        case "CellHottrackInvalidationStyle":
                            this.cellHottrackInvalidationStyle = (CellHottrackInvalidationStyle)Utils.DeserializeProperty(entry, typeof(CellHottrackInvalidationStyle), this.cellHottrackInvalidationStyle);
                            break;
                        case "AllowCardPrinting":
                            // MRS NAS v8.2 - CardView Printing
                            //
                            this.allowCardPrinting = (AllowCardPrinting)Utils.DeserializeProperty(entry, typeof(AllowCardPrinting), this.allowCardPrinting);
                            break;

                        // MBS 4/17/09 - NA9.2 Selection Overlay
                        case "SelectionOverlayColor":
                            this.selectionOverlayColor = (Color)Utils.DeserializeProperty(entry, typeof(Color), this.selectionOverlayColor);
                            break;
                        //
                        case "SelectionOverlayBorderColor":
                            this.selectionOverlayBorderColor = (Color)Utils.DeserializeProperty(entry, typeof(Color), this.selectionOverlayBorderColor);
                            break;
                        //
                        case "SelectionOverlayBorderThickness":
                            this.selectionOverlayBorderThickness = (int)Utils.DeserializeProperty(entry, typeof(int), this.selectionOverlayBorderThickness);
                            break;

                        // CDS 9.2 RowSelector State-Specific Images
                        case "RowSelectorImages":
                            this.rowSelectorImages = (RowSelectorImageInfo)Utils.DeserializeProperty(entry, typeof(RowSelectorImageInfo), null);
                            if (this.rowSelectorImages != null)
                                this.rowSelectorImages.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                            break;

                        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (DefaultSelectedBackColor & DefaultSelectedForeColor)
                        case "DefaultSelectedBackColor":
                            this.defaultSelectedBackColor = (Color)Utils.DeserializeProperty(entry, typeof(Color), this.defaultSelectedBackColor);
                            break;
                        //
                        case "DefaultSelectedForeColor":
                            this.defaultSelectedForeColor = (Color)Utils.DeserializeProperty(entry, typeof(Color), this.defaultSelectedForeColor);
                            break;
                    }
				}
			}
		}

		
		/// <summary>
		/// Loads a layout from a stream, including all properties.
		/// </summary>
		/// <param name="stream">The stream to read.</param>
		///	<remarks>
		///	<p class="body">Invoking this method loads a layout, created by invoking the <see cref="Save(System.IO.Stream)"/> method, from a stream. The stream can be used to retrieve the Layout data from different sources, such as a file on disk, an Internet location or from memory.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate  of a layout.</p>
		///	</remarks>
		public void Load( Stream stream )
		{
			this.Load( stream, PropertyCategories.All );
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Loads a layout from a file, including all properties.
		/// </summary>
		/// <param name="filename">The name of the file to read.</param>
		///	<remarks>
		///	<p class="body">Invoking this method loads a layout, created by invoking the <see cref="Save(string)"/> method, from a stream. The stream can be used to retrieve the Layout data from different sources, such as a file on disk, an Internet location or from memory.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate  of a layout.</p>
		///	</remarks>
		public void Load(string filename)
		{
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Open, FileAccess.Read );

			try
			{
				this.Load(fileStream);
			}
			finally
			{
				fileStream.Close();
			}
		}
		

		/// <summary>
		/// Loads a layout from a stream, using the specified property categories.
		/// </summary>
		/// <param name="stream">The stream to read.</param>
		/// <param name="propertyCategories">Identifies which property categories to load.</param>
		///	<remarks>
		///	<p class="body">Invoking this method loads a layout, created by invoking the <b>Save</b> method, from a stream. The stream can be used to retrieve the Layout data from different sources, such as a file on disk, an Internet location or from memory.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are loaded:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be copied by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>
		public void Load( Stream stream, PropertyCategories propertyCategories )
		{
			if ( !stream.CanRead )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_181"), Shared.SR.GetString("LE_ArgumentException_334"));
			
			BinaryFormatter bformatter = new BinaryFormatter(null ,new StreamingContext(StreamingContextStates.Persistence, propertyCategories ) );

			// JJD 10/17/01
			// Mark the assembly format simple so we don't blow up if the version
			// numbers don't match
			//
			bformatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;

			
			this.LoadHelper(stream, propertyCategories, bformatter);
		}


		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Loads a layout from a file, using the specified property categories.
		/// </summary>
		/// <param name="filename">The name of the file to read.</param>
		/// <param name="propertyCategories">Identifies which property categories to load.</param>
		///	<remarks>
		///	<p class="body">Invoking this method loads a layout, created by invoking the <see cref="Save(string)"/> method, from a file.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are loaded:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be copied by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>
		public void Load(string filename, PropertyCategories propertyCategories)
		{
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Open, FileAccess.Read );

			try
			{
				this.Load(fileStream, propertyCategories);
			}
			finally
			{
				fileStream.Close();
			}
		}


		// AS 8/15/02
		// Added ability to serialize to/from xml.
		//
		/// <summary>
		/// Loads a layout from stream, including all properties.
		/// </summary>
		/// <param name="stream">The stream to read.</param>
		///	<remarks>
		///	<p class="body">Invoking this method loads a layout, created by invoking the <see cref="SaveAsXml(System.IO.Stream)"/> method, from a stream. The stream can be used to retrieve the Layout data from different sources, such as a file on disk, an Internet location or from memory.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate  of a layout.</p>
		///	</remarks>
		public void LoadFromXml( Stream stream )
		{
			this.LoadFromXml( stream, PropertyCategories.All );
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Loads a layout from a file, including all properties.
		/// </summary>
		/// <param name="filename">The name of the file to read.</param>
		public void LoadFromXml(string filename)
		{
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Open, FileAccess.Read );

			try
			{
				this.LoadFromXml(fileStream);
			}
			finally
			{
				fileStream.Close();
			}
		}
		

		/// <summary>
		/// Loads a layout from a stream, using the specified property categories.
		/// </summary>
		/// <param name="stream">The stream to read.</param>
		/// <param name="propertyCategories">Identifies which property categories to load.</param>
		///	<remarks>
		///	<p class="body">Invoking this method loads a layout, created by invoking the <see cref="SaveAsXml(System.IO.Stream)"/> method, from a stream. The stream can be used to retrieve the Layout data from different sources, such as a file on disk, an Internet location or from memory.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are loaded:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be copied by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>
		public void LoadFromXml( Stream stream, PropertyCategories propertyCategories )
		{
			if ( !stream.CanRead )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_181"), Shared.SR.GetString("LE_ArgumentException_334"));
			
			SoapFormatter formatter = new SoapFormatter(null ,new StreamingContext(StreamingContextStates.Persistence, propertyCategories ) );

			// JJD 10/17/01
			// Mark the assembly format simple so we don't blow up if the version
			// numbers don't match
			//
			formatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;

			
			this.LoadHelper(stream, propertyCategories, formatter);
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Loads a layout from a stream, using the specified property categories.
		/// </summary>
		/// <param name="filename">The name of the file to read.</param>
		/// <param name="propertyCategories">Identifies which property categories to load.</param>
		///	<remarks>
		///	<p class="body">Invoking this method loads a layout, created by invoking the <see cref="SaveAsXml(string, PropertyCategories)"/> method, from a file.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are loaded:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be copied by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>
		public void LoadFromXml(string filename, PropertyCategories propertyCategories)
		{
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Open, FileAccess.Read );

			try
			{
				this.LoadFromXml(fileStream, propertyCategories);
			}
			finally
			{
				fileStream.Close();
			}
		}

		// AS 1/21/05 BR01903
		// When serializing out the DataType of a column, the fully qualified
		// name of the type will be serialized. When this is deserialized, the
		// clr may not be able to locate the assembly and will generate an 
		// unhandled exception. To address the immediate issue of the assembly
		// qualified name being serialized, we will hook the AssemblyResolve
		// event of the appdomain and if an wingrid type is requested, we will
		// hand back that of the currently loaded wingrid type. We'll also
		// need to make changes in the shared serialization to better handle
		// serialization/deserialization of the System.Type.
		//
		#region LoadHelper
		private void LoadHelper( Stream stream, PropertyCategories propertyCategories, IFormatter formatter )
		{
			// JJD 8/14/02
			// Set the binder
			formatter.Binder = new Infragistics.Win.UltraWinGrid.Serialization.Binder();
			

			// AS 1/21/05 BR01903
			AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(this.OnAppDomainAssemblyResolve);

			Infragistics.Win.UltraWinGrid.UltraGridLayout newLayout;

			try
			{
				newLayout = formatter.Deserialize( stream ) as Infragistics.Win.UltraWinGrid.UltraGridLayout;
			}
			finally
			{
				// AS 1/21/05 BR01903
				AppDomain.CurrentDomain.AssemblyResolve -= new ResolveEventHandler(this.OnAppDomainAssemblyResolve);
			}

			// SSP 12/4/02 UWG1840
			// Call OnDeserializationComplete instead which hooks up other objects as well besides the
			// bands collection like row scroll regions collection and col scroll regions collection.
			//
			//newLayout.Bands.InitLayout(newLayout, false);
			newLayout.OnDeserializationComplete( false );

			this.Load( newLayout, propertyCategories );
		}
		#endregion //LoadHelper

		// AS 1/21/05 BR01903
		#region OnAppDomainAssemblyResolve
		private System.Reflection.Assembly OnAppDomainAssemblyResolve(object sender, ResolveEventArgs e)
		{
			if (e.Name != null && e.Name.StartsWith("Infragistics.Win.UltraWinGrid"))
				return typeof(UltraGridLayout).Assembly;

			// AS 12/19/07 BR29058
			// A delegate from the TrueCondition in Win was serialized. We need to 
			// be able to provide the assembly for the deserialization.
			//
			if (e.Name != null && e.Name.StartsWith("Infragistics"))
			{
				if (IsInfragisticsAssembly(e.Name, "Win"))
					return typeof(UltraControlBase).Assembly;
			}

			return null;
		}

		// AS 12/19/07 BR29058
		private bool IsInfragisticsAssembly(string assemblyName, string assemblySuffix)
		{
			const string Format = "{0}.{1}.v";

			if (null != assemblyName)
			{
				for (int i = 0; i < AssemblyRef.AllowedAssemblyPrefixes.Length; i++)
				{
					string prefix = AssemblyRef.AllowedAssemblyPrefixes[i];

					if (assemblyName.StartsWith(string.Format(Format, prefix, assemblySuffix)))
						return true;
				}
			}

			return false;
		}
		#endregion //OnAppDomainAssemblyResolve


		internal void InitAppearanceHolders()
		{

			if ( this.valuelists != null )
				this.valuelists.InitAppearanceHolders();

            // MRS 2/4/2009 - TFS13235
			//this.SortedBands.InitAppearanceHolders();
            this.Bands.InitAppearanceHolders();
		}

		/// <summary>
		/// Loads the current layout with attributes of the passed in UltraGridLayout, using the specified property categories.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoking this method loads a Layout using another UltraGridLayout object as the source.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the Layout object are loaded:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be copied by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>
		/// <param name="newLayout">The layout with the property settings to be loaded.</param>
		/// <param name="propertyCategories">Specifies which properties of the layout to load.</param>
		public void Load( UltraGridLayout newLayout, PropertyCategories propertyCategories )
		{
            // MRS 9/19/07 - BR26542
            // Set this flag and added the try...finally block here to revert it.
            this.isApplyingLayout = true;

            try
            {
                // SSP 2/7/02 UWG1018
                // Just call already existing CopyFrom instead of having all this
                // code. It also does not copy the key.
                //
                this.CopyFrom(newLayout, propertyCategories);
            }
            finally
            {
                // MRS 9/19/07 - BR26542
                this.isApplyingLayout = false;
            }
			
		}


		/// <summary>
		/// Saves a layout to a stream, including all properties.
		/// </summary>
		///	<remarks>
		///	<p class="body">Invoking this method saves a layout to a stream. The stream can be used to save the Layout data to different locations, such as a file on disk, an Internet location or to memory.</p>
		///	<p class="body">Invoke the <b>Load</b> method to restore the saved layout.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>		
		public void Save( Stream stream )
		{
			this.Save( stream, PropertyCategories.All );
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Saves a layout to a file, including all properties.
		/// </summary>
		/// <param name="filename">The name of the file to write.</param>
		///	<remarks>
		///	<p class="body">Invoking this method saves a layout to a file.</p>
		///	<p class="body">Invoke the <see cref="Load(string)"/> method to restore the saved layout.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>		
		public void Save(string filename)
		{
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.OpenOrCreate, FileAccess.Write );

			try
			{
				this.Save(fileStream);
			}
			finally
			{
				fileStream.Close();
			}
		}

		/// <summary>
		/// Saves a layout to a stream, using the specified property categories.
		/// </summary>
		///	<remarks>
        /// <param name="stream">The stream to write to.</param>
        /// <param name="propertyCategories">Identifies which property categories to save.</param>
		///	<p class="body">Invoking this method saves a layout to a stream. The stream can be used to save the Layout data to different locations, such as a file on disk, an Internet location or to memory.</p>
		///	<p class="body">Invoke the <b>Load</b> method to restore the saved layout.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are saved:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be saved by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>		
		public void Save( Stream stream, PropertyCategories propertyCategories )
		{
			
			if ( !stream.CanWrite )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_181"), Shared.SR.GetString("LE_ArgumentException_183"));

			BinaryFormatter bformatter = new BinaryFormatter(null ,new StreamingContext(StreamingContextStates.Persistence, propertyCategories ) );

			// JJD 10/17/01
			// Mark the assembly format simple so we don't blow up if the version
			// numbers don't match
			//
			bformatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;

			bformatter.Serialize( stream, this );
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Saves a layout to a file, using the specified property categories.
		/// </summary>
		/// <param name="filename">The name of the file to write.</param>
		/// <param name="propertyCategories">Identifies which property categories to save.</param>
		/// <remarks>
		///	<p class="body">Invoking this method saves a layout to a file.</p>
		///	<p class="body">Invoke the <see cref="Load(string, PropertyCategories)"/> method to restore the saved layout.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are saved:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be saved by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>		
		public void Save(string filename, PropertyCategories propertyCategories)
		{
			//MD 5/23/06 - BR12872
			// If the binary file already existed and wsa longer than the newly saved file,
			// the old file contents would still appear at the bottom of the file.
			// Now the file created each time so no old contents will remain.
			//
			//System.IO.FileStream fileStream = new FileStream( filename, FileMode.OpenOrCreate, FileAccess.Write );
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Create, FileAccess.Write );

			try
			{
				this.Save(fileStream, propertyCategories);
			}
			finally
			{
				fileStream.Close();
			}
		}

		/// <summary>
		/// Saves a layout to a stream, including all properties.
		/// </summary>
		/// <param name="stream">The stream to write to.</param>
		///	<remarks>
		///	<p class="body">Invoking this method saves a layout to a stream. The stream can be used to save the Layout data to different locations, such as a file on disk, an Internet location or to memory.</p>
		///	<p class="body">Invoke the <see cref="LoadFromXml(System.IO.Stream)"/> method to restore the saved layout.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>		
		public void SaveAsXml( Stream stream )
		{
			this.SaveAsXml( stream, PropertyCategories.All );
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Saves a layout to a file including all properties.
		/// </summary>
		/// <param name="filename">The name of the file to write.</param>
		///	<remarks>
		///	<p class="body">Invoking this method saves a layout to a file.</p>
		///	<p class="body">Invoke the <see cref="LoadFromXml(string)"/> method to restore the saved layout.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>		
		public void SaveAsXml(string filename)
		{
			//MD 5/23/06 - BR12872
			// If the xml file already existed and was longer than the newly saved file,
			// the old file contents would still appear at the bottom of the file.
			// Now the file created each time so no old contents will remain.
			//
			//System.IO.FileStream fileStream = new FileStream( filename, FileMode.OpenOrCreate, FileAccess.Write );
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.Create, FileAccess.Write );

			try
			{
				this.SaveAsXml(fileStream, PropertyCategories.All );
			}
			finally
			{
				fileStream.Close();
			}
		}

		/// <summary>
		/// Saves a layout to a stream, using the specified property categories.
		/// </summary>
		/// <param name="stream">The stream to write to.</param>
		/// <param name="propertyCategories">Identifies which property categories to save.</param>
		///	<remarks>
		///	<p class="body">Invoking this method saves a layout to a stream. The stream can be used to save the Layout data to different locations, such as a file on disk, an Internet location or to memory.</p>
		///	<p class="body">Invoke the <see cref="LoadFromXml(System.IO.Stream, PropertyCategories)"/> method to restore the saved layout.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are saved:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be saved by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>		
		public void SaveAsXml( Stream stream, PropertyCategories propertyCategories )
		{
			
			if ( !stream.CanWrite )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_181"), Shared.SR.GetString("LE_ArgumentException_183"));

			SoapFormatter formatter = new SoapFormatter(null ,new StreamingContext(StreamingContextStates.Persistence, propertyCategories ) );

			// JJD 10/17/01
			// Mark the assembly format simple so we don't blow up if the version
			// numbers don't match
			//
			formatter.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;

			formatter.Serialize( stream, this );
		}

		// JJD 8/8/03 - DNF44
		// Added overload that takes a filename
		/// <summary>
		/// Saves a layout to a file, using the specified property categories.
		/// </summary>
		/// <param name="filename">The name of the file to write.</param>
		/// <param name="propertyCategories">Identifies which property categories to save.</param>
		///	<remarks>
		///	<p class="body">Invoking this method saves a layout to a file.</p>
		///	<p class="body">Invoke the <see cref="LoadFromXml(string, PropertyCategories)"/> method to restore the saved layout.</p>
		///	<p class="body">When specifying 256 (PropCatGeneral), the following property settings for the UltraGridLayout object are saved:</p>
		///	<p class="body"><table cellpadding="0" cellspacing="5" border="0">
		///	<tr>
		///		<td valign="top"><ul>
		///		<li class="body">AddNewBox</li>
		///		<li class="body">AlphaBlendEnabled</li>
		///		<li class="body">BorderStyle</li>
		///		<li class="body">BorderStyleCaption</li>
		///		<li class="body">Caption</li>
		///		<li class="body">Enabled</li>
		///		<li class="body">EstimatedRows</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">Font</li>
		///		<li class="body">InterBandSpacing</li>
		///		<li class="body">MaxColScrollRegions</li>
		///		<li class="body">MaxRowScrollRegions</li>
		///		<li class="body">Override</li>
		///		<li class="body">RowConnectorColor</li>
		///		<li class="body">RowConnectorStyle</li></ul>
		///		</td>
		///		<td valign="top"><ul>
		///		<li class="body">ScrollBars</li>
		///		<li class="body">TabNavigation</li>
		///		<li class="body">TagVariant</li>
		///		<li class="body">ViewStyle</li>
		///		<li class="body">ViewStyleBand</li></ul>
		///		</td>
		///	</tr>
		///	</table>
		///	</p>
		///	<p class="body">Multiple Layout categories can be saved by combining them using logical <b>Or</b>.</p>
		///	<p class="body">The <b>Clone</b> and <b>CopyFrom</b> methods can be invoked to make a duplicate of a layout.</p>
		///	</remarks>		
		public void SaveAsXml(string filename, PropertyCategories propertyCategories)
		{
			System.IO.FileStream fileStream = new FileStream( filename, FileMode.OpenOrCreate, FileAccess.Write );

			try
			{
				this.SaveAsXml(fileStream, propertyCategories );
			}
			finally
			{
				fileStream.Close();
			}
		}

		/// <summary>
		/// Returns a clone of the collection
		/// </summary>
		object ICloneable.Clone()
		{
			Infragistics.Win.UltraWinGrid.UltraGridLayout clone = (Infragistics.Win.UltraWinGrid.UltraGridLayout)this.MemberwiseClone();

			clone.InitializeFrom( this, PropertyCategories.All );			
			
			return clone;
		}

		internal void InitializeFrom( UltraGridLayout source, PropertyCategories propertyCategories )
		{
			
			//LAYOUT_SUBITEM_STATE
			//if ( (PropertyCategories & PropertyCategories.General) )
			this.InterBandSpacing = source.interBandSpacing;
			this.MaxColScrollRegions = source.maxColScrollRegions;
			this.MaxRowScrollRegions = source.maxRowScrollRegions;
			this.BorderStyle = source.borderStyle;
			this.RowConnectorStyle = source.rowConnectorStyle;
			this.RowConnectorColor = source.rowConnectorColor;
			this.Scrollbars = source.scrollbars;
			this.TabNavigation = source.tabNavigation;
			this.ViewStyle = source.viewStyle;
			this.ViewStyleBand = source.viewStyleBand;
			// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
			// Obsoleted AutoFitColumns and added AutoFitStyle property.
			//
			//this.autoFitColumns = source.autoFitColumns;
			this.autoFitStyle = source.autoFitStyle;

			// SSP 6/11/02
			// Added code to serialize these properties.
			//
			this.maxBandDepth = source.maxBandDepth;
			this.priorityScrolling = source.priorityScrolling;
			this.scrollStyle = source.scrollStyle;

			// SSP 8/9/02 UWG1201
			//
			if ( 0 != ( PropertyCategories.General & propertyCategories ) )
			{
				this.borderStyleCaption = source.borderStyleCaption;
			}

			// SSP 7/26/02
			// Also serialize the sigma and filter drop down images.
			//
			// --------------------------------------------------------------------------------
			// SSP 7/30/02
			// Use member variables instead of properties because the properties get will
			// return default image from the resource and we don't want to assign that
			// to the properties.
			//
			//this.SummaryButtonImage = source.SummaryButtonImage;
			//this.FilterDropDownButtonImage = source.FilterDropDownButtonImage;
			//this.FilterDropDownButtonImageActive = source.FilterDropDownButtonImageActive;
			this.sigmaImage = source.sigmaImage;
			this.filterDropDownButtonImage = source.filterDropDownButtonImage;
			this.filterDropDownButtonImageActive = source.filterDropDownButtonImageActive;
			// --------------------------------------------------------------------------------

			// SSP 5/19/03 - Fixed headers
			// --------------------------------------------------------------------------------
			this.useFixedHeaders = source.useFixedHeaders;
			this.fixedHeaderOnImage = source.fixedHeaderOnImage;
			this.fixedHeaderOffImage = source.fixedHeaderOffImage;
			// --------------------------------------------------------------------------------

			// SSP 3/29/05 - NAS 5.2 Fixed Rows
			// --------------------------------------------------------------------------------
			this.fixedRowOnImage = source.fixedRowOnImage;
			this.fixedRowOffImage = source.fixedRowOffImage;
			// --------------------------------------------------------------------------------

			// JAS 4/5/05 v5.2 CaptionVisible property
			// --------------------------------------------------------------------------------
			this.captionVisible = source.captionVisible;
			// --------------------------------------------------------------------------------

			// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
			// 
			this.newColumnLoadStyle = source.newColumnLoadStyle;
			this.newBandLoadStyle   = source.newBandLoadStyle;

			// SSP 5/22/03
			// Added a property to allow the user to control the small change of the column scroll bar.
			//
			this.columnScrollbarSmallChange = source.columnScrollbarSmallChange;

			// SSP 11/24/03 - Scrolling Till Last Row Visible
			// Added ScrollBounds property.
			//
			this.scrollBounds = source.scrollBounds;

			// SSP 1/7/05 BR01139
			// Copy over the loadStyle as well.
			//
			this.loadStyle = source.loadStyle;

			// SSP 6/30/05 - NAS 5.3 Column Chooser
			// 
			this.columnChooserEnabled = source.columnChooserEnabled;

			// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
			// 
			this.clipboardCellDelimiter = source.clipboardCellDelimiter;
			this.clipboardCellSeparator = source.clipboardCellSeparator;
			this.clipboardRowSeparator = source.clipboardRowSeparator;

			// SSP 7/6/05 - NAS 5.3 Empty Rows
			// 
			if ( null != source.emptyRowSettings )
				this.EmptyRowSettings.InitializeFrom( source.emptyRowSettings );
			else
				this.ResetEmptyRowSettings( );

			//this.Enabled = source.enabled;
			//			this.AlphaBlendEnabled = source.alphaBlendEnabled;

            // MRS NAS v8.2 - CardView Printing
            this.allowCardPrinting = source.allowCardPrinting;

            // CDS 9.2 RowSelector State-Specific Images
            if (source.HasRowSelectorImages)
                this.RowSelectorImages.InitializeFrom(source.rowSelectorImages);
            // CDS 6/01/09 TFS18124 Need to clear the images if they are set in the destination but not in the source
            else if (this.HasRowSelectorImages &&
                this.rowSelectorImages.ShouldSerialize())
            {
                this.rowSelectorImages.ResetInternal();
            }

			//LAYOUT_SUBITEM_FONT
			
			//LAYOUT_SUBITEM_APPEARANCES
			if ( (propertyCategories & PropertyCategories.AppearanceCollection) != 0 ) 
			{
				// Clear out the existing collection
				//
				if ( this.appearances != null )
					this.appearances.Clear();

				// clone the source's collection
				// JJD 11/19/01
				// Clone each appearance and add it to the collection
				//
				if ( source.appearances != null )
					this.Appearances.CloneAndAppendContents( source.appearances );
				
			}
			
			
			//LAYOUT_SUBITEM_COLSCROLLREGIONS
			if ( (propertyCategories & PropertyCategories.ColScrollRegions) != 0 ) 
			{
				// clone the source's collection 
				if ( source.colScrollRegions != null )
				{
					this.ColScrollRegions.InitializeFrom ( source.colScrollRegions, propertyCategories );
					this.colScrollRegions.DirtyMetrics();
				}
				else
				{
					if ( this.colScrollRegions != null )
					{						
						this.colScrollRegions.Clear();
					}
				}
			}

			//LAYOUT_SUBITEM_ROWSCROLLREGIONS:
			if ( (propertyCategories & PropertyCategories.RowScrollRegions) != 0 ) 
			{
				// clone the source's collection 
				if ( source.rowScrollRegions != null )
				{
					// JJD 10/30/01
					// Use uppcase property to allocate the collection if neceassary
					//
					this.RowScrollRegions.InitializeFrom ( source.rowScrollRegions, propertyCategories );
				}
				else
				{
					if ( this.rowScrollRegions != null )
					{						
						this.rowScrollRegions.Clear();
					}
				}
			}

			//LAYOUT_SUBITEM_VALUELISTS:
			if( (propertyCategories & PropertyCategories.ValueLists) != 0)
			{
				if ( source.valuelists != null )
				{
					// clone the source's collection 
					this.ValueLists.InitializeFrom( source.valuelists );					
				}
				else
					if ( this.valuelists != null )
					this.valuelists.Clear();
			}

			// SSP 8/5/04 UWG3582
			// Initialize the override before the bands. Before bands were being initialized
			// before the override. The reason for this change is that we want to have the 
			// correct Band.RowFilterModeResolved so the initialization of any column filters
			// can correctly bump the filter version numbers. We only bump the filter version
			// numbers if the filters that are being modified are going to be used for 
			// filtering purposes. Look in the RowsCollection's and Band's OnSubObjectChange
			// methods where we are bumping the filter version numbers for more info.
			//
			if ( (propertyCategories & PropertyCategories.General) != 0)
			{
				if ( source.overrideObj != null )
				{
					// init the override from the source
					//
					this.Override.InitializeFrom( source.overrideObj, propertyCategories );						
				}
				else
				{
					this.Override.Reset();
				}
			}

            
			if ( ((propertyCategories & PropertyCategories.Bands) != 0) &&
                // MRS 5/21/2009 - TFS17794
				//source.sortedBands != null )
                source.bands != null)
			{
				// JJD 10/30/01
				// Use uppcase property to allocate the collection if neceassary
				//
                // MRS 2/6/2009 - TFS13287
                // We should never be calling InitializeFrom the Sorted Bands collection because 
                // the initialization of the VisiblePosition property might cause might cause the order of the collection to change while we are looping through it.
                //this.SortedBands.InitializeFrom( source.sortedBands, propertyCategories );
                // MRS 5/21/2009 - TFS17794
                //this.Bands.InitializeFrom(source.sortedBands, propertyCategories);
                this.Bands.InitializeFrom(source.bands, propertyCategories);
				
				// SSP 5/7/03 - Export Functionality
				//
				//if ( this.IsDisplayLayout || this.IsPrintLayout )
				if ( this.IsDisplayLayout || this.IsPrintLayout || this.IsExportLayout )
					this.ColScrollRegions.DirtyMetrics();
			}

			//LAYOUT_SUBITEM_APPEARANCE
			if ( (propertyCategories & PropertyCategories.General) != 0)
			{
				if ( source.appearanceHolders != null )
				{
					for ( int i = 0 ; i < source.appearanceHolders.Length; i++ )
					{
						// MRS 5/18/05 - BR03715
						// remove the old prop change notifications
						//
						if ( this.AppearanceHolders[i] != null )
							this.appearanceHolders[i].SubObjectPropChanged -= this.SubObjectPropChangeHandler;

						if ( source.appearanceHolders[i] != null )
						{							
							this.AppearanceHolders[i] = source.appearanceHolders[i].Clone();

							// JJD 8/20/01
							// Init the holder's Appearances collection referenece
							//
							this.AppearanceHolders[i].Collection = this.Appearances;

							// MRS 5/18/05 - BR03715
							// hook prop change notifications
							//
							this.appearanceHolders[i].SubObjectPropChanged += this.SubObjectPropChangeHandler;
						}
						else
						{
							//RA - Bug 98 7/31/01
							this.AppearanceHolders[i] = null;						
							//this.appearanceHolders[i] = null;						
						}
					}
				}
				else
				{
					// JJD 8/20/01
					// Clear the appearance holders array
					//
					this.appearanceHolders = null;
				}
			}	

			// MRS 10/22/04 - UWG3066
			this.SynchGridBackColor();

			//LAYOUT_SUBITEM_KEY:
			this.Key = source.Key;

			//LAYOUT_SUBITEM_TAGVARIANT
			if ( (propertyCategories & PropertyCategories.General) != 0)
			{
				// JJD 1/31/02
				// Only copy over the tag if the source's tag is a candidate
				// for serialization
				//
				if ( source.ShouldSerializeTag() )
					this.tagValue = source.Tag;
			}		
	
			//LAYOUT_SUBITEM_ADDNEWBOX
			if ( (propertyCategories & PropertyCategories.General) != 0)
			{
				if ( source.addNewBox != null )
				{
					// init the AddNewBox from the source
					//
					this.AddNewBox.InitializeFrom( source.addNewBox, propertyCategories );						
				}

				else 
					this.AddNewBox.Reset();
			}

			//LAYOUT_SUBITEM_ADDNEWBOX
			if ( (propertyCategories & PropertyCategories.General) != 0)
			{
				if ( source.groupByBox != null )
				{
					// init the AddNewBox from the source
					//
					// JJD 10/30/01
					// Use uppcase property to allocate the collection if neceassary
					//
					this.GroupByBox.InitializeFrom( source.groupByBox, propertyCategories );						
				}

				else 
					this.GroupByBox.Reset();
			}

			// AS - 12/19/01
			// SSP 6/18/02 UWG1225
			// Make a clone and assign that instead of assigning the object
			// itself. We don't want two layouts to be referring to the same
			// scroll bar look object.
			//
			//this.ScrollBarLook = source.ScrollBarLook;
			this.ScrollBarLook = source.ScrollBarLook.Clone( );


			// SSP 1/28/02 UWG1007
			// Clear any cached info like font heights
			//
			this.ClearCachedCaptionHeight( );
			this.cachedBaseFontHeight = 0;
			this.cachedFontHeight = 0;
			this.ClearBandRowHeightMetrics( null );
			
			// SSP 8/27/02 UWG1611
			//
			//this.DirtyGridElement( );
			this.DirtyGridElement( true );
			this.BumpCellChildElementsCacheVersion( );

			// SSP 7/25/03 - Fixed headers
			//
			this.BumpFixedHeadersVerifyVersion( );

			// SSP 8/6/03 UWG2567
			// Dirty the metrics and also Bump the row layout version. BumpRowLayoutVersion
			// does both of these.
			//
			this.BumpRowLayoutVersion( );

			// SSP 4/15/04
			// Change in settings like template add-row could effect the scroll count.
			// So bump the scroll count version number.
			//
			this.BumpScrollableRowCountVersion( );

			// AS - 10/29/01
			// Can't invalidate if this is a clone since there won't be a grid reference
			if ( this.grid != null )
			{
				this.grid.Invalidate( );

				// SSP 3/10/06 BR10744
				// 
				this.grid.RefreshColumnChoosers( );
			}

            // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (DefaultSelectedBackColor & DefaultSelectedForeColor)
            this.defaultSelectedBackColor = source.defaultSelectedBackColor;
            this.defaultSelectedForeColor = source.defaultSelectedForeColor;

            // MBS 7/7/09 - TFS19076
            this.selectionOverlayBorderColor = source.selectionOverlayBorderColor;
            this.selectionOverlayBorderThickness = source.selectionOverlayBorderThickness;
            this.selectionOverlayColor = source.selectionOverlayColor;
            if (source.ShouldSerializeSelectionOverlayBorderColor() ||
                source.ShouldSerializeSelectionOverlayBorderThickness() ||
                source.ShouldSerializeSelectionOverlayColor() ||
                // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. (DefaultSelectedBackColor & DefaultSelectedForeColor)
                source.ShouldSerializeDefaultSelectedBackColor() ||
                source.ShouldSerializeDefaultSelectedForeColor())
            {
                StyleUtils.DirtyCachedPropertyVals(this);
            }
		}

		/// <summary>
		/// The main UIElement for the grid (read-only)
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)]
		public UltraGridUIElement UIElement 
		{ 
			get
			{
				if ( this.element == null &&
					this.grid != null )
				{
					// only hook events if this layout is the main grid layout 
					//
					this.element = new UltraGridUIElement( this.grid, this, this.IsDisplayLayout );
				}
				
				return this.element; 
			}
		}

		#region HasUIElement

		// SSP 11/12/04
		// Added HasUIElement property.
		//
		internal bool HasUIElement
		{
			get
			{
				return null != this.element;
			}
		}

		#endregion // HasUIElement

		#region ClearSelectionStrategies

		// SSP 11/2/04 UWG3736
		// When the data source is reset on the grid we need to ensure that the grid doesn't keep 
		// holding any references to the datasource and the list object. In order to do that
		// we need to release all the references to the rows/cells etc... The selection strategies
		// can hold a reference to the selectable item like a cell or a row. So we need to release
		// them as well. Added ClearSelectionStrategies method.
		//
		private void ClearSelectionStrategies( )
		{
			this.selectionStrategyCell = null;
			this.selectionStrategyColumn = null;
			this.selectionStrategyGroupByRow = null;
			this.selectionStrategyRow = null;

			if ( null != this.overrideObj )
				this.overrideObj.ClearSelectionStrategies( );
		}

		#endregion // ClearSelectionStrategies

		#region ClearMainUIElement

		// SSP 10/18/04 UWG3736
		// When the data source is cleared we need to clear the child elements of 
		// the main ui element otherwise some of the descendant ui elements will 
		// keep holding references back to row objects that may end up not releasing 
		// memory until the ui elements are verified next time. For example row 
		// elements could have references to the rows in which case the rows won't
		// be released and so won't the list objects, bands etc...
		// Added ClearMainUIElement method.
		//
		internal void ClearMainUIElement( )
		{
			// SSP 11/02/04 UWG3736
			// Also ensure that the scroll regions aren't holding any references to a row or a cell.
			//
			if ( null != this.colScrollRegions )
			{
				foreach ( ColScrollRegion csr in this.colScrollRegions )
				{
					csr.SetPivotCell( null );
					csr.SetPivotItem( null );
				}
			}
			
			if ( null != this.rowScrollRegions )
			{
				foreach ( RowScrollRegion rsr in this.rowScrollRegions )
				{
					rsr.SetPivotItem( null );
					rsr.FirstRow = null;
				}
			}

            this.ClearSelectionStrategies( );

			// SSP 1/12/05 BR0155 UWG3736
			// Clear the selection as well when the data source is nulled out.
			//
			UltraGrid grid = this.grid as UltraGrid;
			if ( null != grid && grid.HasSelectedBeenAllocated )
				grid.Selected.ClearAllSelected( );

			// SSP 1/12/05
			// Dispose the embeddable editors as well.
			//
			this.DisposeCachedEditors( );

			// SSP 10/18/05 BR07074
			// Don't dispose the elements while we are currently verifying them.
			// 
			//if ( null != this.element )
			if ( null != this.element && ! this.element.inVerifyChildElements )
			{
				this.element.Dispose( );
				this.element = null;

				// SSP 8/23/05
				// If we are in the process of being disposed then don't bother verifying the elems.
				// Enclosed the existing code into the if block. Also don't bother verifying the
				// elems if the datasource is being set to null.
				// 
				if ( ! this.Disposed && null != this.grid && ! this.grid.IsDisposed 
					&& ! this.grid.Disposing && null != this.grid.DataSource )
				{
					// SSP 12/15/04 BR01214
					// Make sure the Rect of the grid element is initialized. Also pass in true for
					// verify so the grid element positions the data area element which is used in
					// ScrollRowIntoView logic to figure out the extent of the row scroll region.
					// 
					UltraGridUIElement gridElem = this.UIElement;
					if ( null != gridElem )
						gridElem.InternalInitializeRect( true );
				}
			}
		}

		#endregion // ClearMainUIElement

		#region ScrollBarLook ( AS - 12/18/01 )
		/// <summary>
		/// Determines the appearance of the scroll bars.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
		[LocalizedCategory("LC_Appearance")]
		[LocalizedDescription("LD_ListIndexComparer_P_ScrollBarLook")]
		public ScrollBarLook ScrollBarLook
		{
			get 
			{ 
				if (this.scrollbarLook == null || this.scrollbarLook.Disposed)
				{
					this.scrollbarLook = new ScrollBarLook(this.Appearances);

					// AS 6/17/02 UWG1239
					this.scrollbarLook.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					this.scrollbarLook.InitializeAppearances(this.Appearances);

					this.ReInitializeScrollBarLook();
				}

				return this.scrollbarLook;
			}

			set
			{
				// if the supplied value is the same, just leave
				if (this.scrollbarLook == value)
					return;

				// unhook from any notifications if we had a scrollbar look.
				if (this.scrollbarLook != null)
					this.scrollbarLook.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// otherwise store the new value
				this.scrollbarLook = value;

				// if we have already created the scroll bar objects, reset the scroll bar look
				this.ReInitializeScrollBarLook();

				// if we have a scroll bar look, hook into the notifications
				if (this.scrollbarLook != null)
				{
					this.scrollbarLook.SubObjectPropChanged += this.SubObjectPropChangeHandler;
					this.scrollbarLook.InitializeAppearances(this.Appearances);
				}

				this.NotifyPropChange(PropertyIds.ScrollBarLook, null);
			}
		}

		private void ReInitializeScrollBarLook()
		{
			if (this.colScrollRegions != null)
			{
				foreach( ColScrollRegion csr in this.ColScrollRegions )
				{
					csr.ScrollBarInfo.ScrollBarLook = this.scrollbarLook;
				}
			}

			if (this.rowScrollRegions != null)
			{
				foreach( RowScrollRegion rsr in this.RowScrollRegions )
				{
					rsr.ScrollBarInfo.ScrollBarLook = this.scrollbarLook;
				}
			}
		}

		/// <summary>
		/// Indicates whether the property needs to be serialized.
		/// </summary>
		/// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeScrollBarLook()
		{
			return this.scrollbarLook != null && this.scrollbarLook.ShouldSerialize();
		}

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void ResetScrollBarLook()
		{
			if (this.scrollbarLook != null)
			{
				// AS 6/17/02 UWG1232
				// Use the upper case so we can get a new scroll bar look
				this.ScrollBarLook.Reset();
			}
		}
		#endregion ScrollBarLook


		#region LayoutTypeConverter

		/// <summary>
		/// Layout object type converter.
		/// </summary>
		public sealed class LayoutTypeConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if (destinationType == typeof(InstanceDescriptor)) 
				{
					return true;
				}
				
				return base.CanConvertTo(context, destinationType);
			}

            /// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) 
			{
				if (destinationType == null) 
				{
					throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_20"));
				}

				if ( destinationType == typeof(InstanceDescriptor) && 
					value is UltraGridLayout ) 
				{
					UltraGridLayout item = (UltraGridLayout)value;
					
					ConstructorInfo ctor;

					bool hasKey = ( item.Key != null &&
						item.Key.Length > 0 );

					if ( hasKey )
						ctor = typeof(UltraGridLayout).GetConstructor(new Type[] { typeof( string ) } );
					else
						ctor = typeof(UltraGridLayout).GetConstructor(new Type[] { } );

					if (ctor != null) 
					{
						//false as the last parameter here causes generation of a local variable for the type 
						if ( hasKey )
							return new InstanceDescriptor(ctor, new object[] { item.Key }, false);
						else
							return new InstanceDescriptor(ctor, new object[] { }, false);
					}
				}

				return base.ConvertTo(context, culture, value, destinationType);
			}
		

		}
		#endregion LayoutTypeConverter
		
		#region DisplayLayoutTypeConverter

		/// <summary>
		/// Layout object type converter.
		/// </summary>
		sealed public class DisplayLayoutTypeConverter : ExpandableObjectConverter 
		{

            /// <summary>
            /// Returns a collection of properties for the type of array specified by the
            /// value parameter, using the specified context and attributes.
            /// </summary>
            /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
            /// <param name="value">An <see cref="System.Object"/> that specifies the type of array for which to get properties.</param>
            /// <param name="attributes">An array of type <see cref="System.Attribute"/> that is used as a filter.</param>
            /// <returns>A <see cref="System.ComponentModel.PropertyDescriptorCollection"/> with the properties that are exposed for this data type, or null if there are no properties.</returns>
            public override PropertyDescriptorCollection GetProperties(
				ITypeDescriptorContext context,
				object value,
				Attribute[] attributes )
			{
				PropertyDescriptorCollection props = base.GetProperties( context, value, attributes );

				UltraGridLayout layout = value as UltraGridLayout;

				// JJD 2/4/02 - UWG578
				// If this is a dropdown or combo filter out properties
				// that don't make sense
				//
				if ( props != null &&
					layout != null &&
					layout.grid is UltraDropDownBase )
				{
					int count = 0;

					// count up all the properties that won't be filtered out
					//
					for ( int i = 0; i < props.Count; i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.FilterOutProperty( props[i] ) )
						if ( !DisplayLayoutTypeConverter.FilterOutProperty( props[ i ] ) )
							count++;
					}

					// allocate an array of the proper size
					//
					PropertyDescriptor [] propArray = new PropertyDescriptor[count];

					int current = 0;

					// copy the unfiltered properties into the array
					//
					for ( int i = 0; i < props.Count; i++ )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//if ( !this.FilterOutProperty( props[i] ) )
						if ( !DisplayLayoutTypeConverter.FilterOutProperty( props[ i ] ) )
						{
							propArray[current] = props[i];
							current++;
						}
					}

					// return the filtered collection
					//
					return new PropertyDescriptorCollection( propArray );
				}

				return props;
			}

			// JJD 2/4/02 - UWG578
			// Filter out properties not meaningful to combos and dropdowns
			//
			// MD 7/26/07 - 7.3 Performance
			// FxCop - Mark members as static
			//private bool FilterOutProperty( PropertyDescriptor pd )
			private static bool FilterOutProperty( PropertyDescriptor pd )
			{
				switch ( pd.DisplayName )
				{
					case "AddNewBox":
					case "GroupByBox":
					case "InterBandSpacing":
					case "Key":
					case "MaxColScrollRegions":
					case "MaxRowScrollRegions":
					case "RowConnectorColor":
					case "RowConnectorStyle":
					case "TabNavigation":
					case "ValueLists":
					case "ViewStyle":
					case "ViewStyleBand":
					{
						return true;
					}
				}

				return false;
			}

		}
		#endregion DisplayLayoutTypeConverter

		#region DontFireCellChangeEvent

		// SSP 3/29/02
		// Added dontFireCellChangeEvent flag to prevent firing of CellChange event
		// when an item from value list or month view control is selected.
		//
		internal bool DontFireCellChangeEvent
		{
			get
			{
				return this.dontFireCellChangeEvent;
			}
			set
			{
				this.dontFireCellChangeEvent = value;
			}
		}

		#endregion // DontFireCellChangeEvent

		// SSP 4/5/02
		// Added methods for embeddable editors
		//
		#region Embeddable editor code

		#region LastAutoSizeEditEventArgs

		// SSP 8/8/02
		// AutoSizeEdit feature for embeddable editors implemented.
		//

		internal CancelableAutoSizeEditEventArgs LastAutoSizeEditEventArgs
		{
			get
			{
				return this.lastAutoSizeEditEventArgs;
			}
			set
			{
				this.lastAutoSizeEditEventArgs = value;
			}
		}

		#endregion // LastAutoSizeEditEventArgs

		#region EmbeddableElementBeingEdited
		
		internal EmbeddableUIElementBase EmbeddableElementBeingEdited
		{
			get
			{
				return this.embeddableElementBeingEdited;
			}
			set
			{
				this.embeddableElementBeingEdited = value;

				// When the embeddable editor being edited is set to null,
				// (which we do when we exit the edit mode), then also set
				// the EmbeddableElementBeingEdited_ColScrollRegion and
				// EmbeddableElementBeingEdited_RowScrollRegion propeties
				// to null.
				//
				if ( null == this.embeddableElementBeingEdited )
				{
					this.EmbeddableElementBeingEdited_ColScrollRegion = null;
					this.EmbeddableElementBeingEdited_RowScrollRegion = null;
				}
			}
		}

		#endregion // EmbeddableElementBeingEdited

		#region EmbeddableElementBeingEdited_RowScrollRegion
		
		internal RowScrollRegion EmbeddableElementBeingEdited_RowScrollRegion
		{
			get
			{
				return this.embeddableElementBeingEdited_RowScrollRegion;
			}
			set
			{
				this.embeddableElementBeingEdited_RowScrollRegion = value;
			}
		}

		#endregion // EmbeddableElementBeingEdited_RowScrollRegion
		
		#region EmbeddableElementBeingEdited_ColScrollRegion

		internal ColScrollRegion EmbeddableElementBeingEdited_ColScrollRegion
		{
			get
			{
				return this.embeddableElementBeingEdited_ColScrollRegion;
			}
			set
			{
				this.embeddableElementBeingEdited_ColScrollRegion = value;
			}
		}

		#endregion // EmbeddableElementBeingEdited_ColScrollRegion

		#region CancellingEditOperationFlag
		
		internal bool CancellingEditOperationFlag
		{
			get
			{
				return this.cancellingEditOperationFlag;
			}
			set
			{
				this.cancellingEditOperationFlag = value;
			}
		}

		#endregion // CancellingEditOperationFlag

		#region ForceExitEditOperationFlag
		
		internal bool ForceExitEditOperationFlag
		{
			get
			{
				return this.forceExitEditOperationFlag;
			}
			set
			{
				this.forceExitEditOperationFlag = value;
			}
		}

		#endregion // ForceExitEditOperationFlag

		#region CellInEditMode
		
		internal UltraGridCell CellInEditMode
		{
			get
			{
				UltraGridCell cell = this.ActiveCell;

				if ( null != cell && cell.IsInEditMode )
					return cell;

				return null;
			}
		}

		#endregion // CellInEditMode

		#region EditorInEditMode
		
		internal EmbeddableEditorBase EditorInEditMode
		{
			get
			{
				UltraGridCell cell = this.CellInEditMode;

				if ( null != cell )
				{
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
					//EmbeddableEditorBase editor = cell.Column.Editor;
					EmbeddableEditorBase editor = cell.EditorResolved;
				
					if ( null != editor && editor.IsInEditMode )
					{
						return editor;
					}
				}

				return null;
			}
		}

		#endregion // EditorInEditMode

		#region HookIntoEditor
		
		internal void HookIntoEditor( EmbeddableEditorBase editor )
		{
			// SSP 7/3/03 - Ink Editor Related
			// If we are trying to hook into the same editor as the last one, then don't unhook
			// and rehook. Simply return for efficiency purpose.
			//
			if ( this.lastHookedEditor == editor )
				return;

			// Unhook from any editor we may have hooked into previously
			//
			this.UnHookFromEditor( );

			this.lastHookedEditor = editor;

			this.lastHookedEditor.BeforeEnterEditMode += this.EmbeddableEditor_BeforeEnterEditModeEventHandler;
			this.lastHookedEditor.AfterEnterEditMode += this.EmbeddableEditor_AfterEnterEditModeEventHandler;
			this.lastHookedEditor.BeforeExitEditMode += this.EmbeddableEditor_BeforeExitEditModeEventHandler;
			this.lastHookedEditor.AfterExitEditMode += this.EmbeddableEditor_AfterExitEditModeEventHandler;

			// If the editor supports drop down, then hook into BeforeDropDown and
			// AfterCloseUp so that we can fire respective grid events.
			//
			if ( this.lastHookedEditor.SupportsDropDown )
			{
				this.lastHookedEditor.BeforeDropDown += this.EmbeddableEditor_BeforeDropDownEventHandler;
				this.lastHookedEditor.AfterCloseUp   += this.EmbeddableEditor_AfterCloseupEventHandler;
				
				// SSP 10/11/02 UWG1749
				// We need to fire CellListSelect event and in order to do that we need to hook
				// into the SelectionChanged event of the emebddable editor.
				//
				this.lastHookedEditor.SelectionChanged += this.EmbeddableEditor_SelectionChangedEventHandler;
			}

			this.lastHookedEditor.KeyDown += this.EmbeddableEditor_KeyDownEventHandler;
			this.lastHookedEditor.KeyUp += this.EmbeddableEditor_KeyUpEventHandler;
			this.lastHookedEditor.KeyPress += this.EmbeddableEditor_KeyPressEventHandler;
			
			//Debug.WriteLine( ++count + ": Hooked into the editor" );
		}

		#endregion // HookIntoEditor

		#region UnHookFromEditor
		
		internal void UnHookFromEditor( )
		{
			if ( null != this.lastHookedEditor )
			{
				this.lastHookedEditor.BeforeEnterEditMode -= this.EmbeddableEditor_BeforeEnterEditModeEventHandler;
				this.lastHookedEditor.AfterEnterEditMode -= this.EmbeddableEditor_AfterEnterEditModeEventHandler;
				this.lastHookedEditor.BeforeExitEditMode -= this.EmbeddableEditor_BeforeExitEditModeEventHandler;
				this.lastHookedEditor.AfterExitEditMode -= this.EmbeddableEditor_AfterExitEditModeEventHandler;

				if ( this.lastHookedEditor.SupportsDropDown )
				{
					this.lastHookedEditor.BeforeDropDown -= this.EmbeddableEditor_BeforeDropDownEventHandler;
					this.lastHookedEditor.AfterCloseUp   -= this.EmbeddableEditor_AfterCloseupEventHandler;

					// SSP 10/11/02 UWG1749
					// We need to fire CellListSelect event and in order to do that we need to hook
					// into the SelectionChanged event of the emebddable editor.
					//
					this.lastHookedEditor.SelectionChanged -= this.EmbeddableEditor_SelectionChangedEventHandler;
				}

				this.lastHookedEditor.KeyDown -= this.EmbeddableEditor_KeyDownEventHandler;
				this.lastHookedEditor.KeyUp -= this.EmbeddableEditor_KeyUpEventHandler;
				this.lastHookedEditor.KeyPress -= this.EmbeddableEditor_KeyPressEventHandler;
				this.lastHookedEditor = null;

				//Debug.WriteLine( --count + ": Unhooked from the editor" );
			}
		}

		#endregion // UnHookFromEditor

		#region EmbeddableEditor_KeyDownEventHandler
		
		internal KeyEventHandler EmbeddableEditor_KeyDownEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_KeyDownEventHandler )
				{
					this.embeddableEditor_KeyDownEventHandler = new KeyEventHandler( this.OnEditorKeyDown );
				}

				return this.embeddableEditor_KeyDownEventHandler;
			}
		}

		#endregion // EmbeddableEditor_KeyDownEventHandler

		#region EmbeddableEditor_KeyUpEventHandler
		
		internal KeyEventHandler EmbeddableEditor_KeyUpEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_KeyUpEventHandler )
				{
					this.embeddableEditor_KeyUpEventHandler = new KeyEventHandler( this.OnEditorKeyUp );
				}

				return this.embeddableEditor_KeyUpEventHandler;
			}
		}

		#endregion // EmbeddableEditor_KeyUpEventHandler

		#region EmbeddableEditor_KeyPressEventHandler

		internal KeyPressEventHandler EmbeddableEditor_KeyPressEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_KeyPressEventHandler )
				{
					this.embeddableEditor_KeyPressEventHandler = new KeyPressEventHandler( this.OnEditorKeyPress );
				}

				return this.embeddableEditor_KeyPressEventHandler;
			}
		}

		#endregion // EmbeddableEditor_KeyPressEventHandler

		#region EmbeddableEditor_AfterExitEditModeEventHandler
		
		internal EventHandler EmbeddableEditor_AfterExitEditModeEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_AfterExitEditModeEventHandler )
				{
					this.embeddableEditor_AfterExitEditModeEventHandler = new EventHandler( this.OnEditorAfterExitEditMode );
				}

				return this.embeddableEditor_AfterExitEditModeEventHandler;
			}
		}	

		#endregion // EmbeddableEditor_AfterExitEditModeEventHandler

		#region EmbeddableEditor_BeforeExitEditModeEventHandler
		
		internal Infragistics.Win.BeforeExitEditModeEventHandler EmbeddableEditor_BeforeExitEditModeEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_BeforeExitEditModeEventHandler )
				{
					this.embeddableEditor_BeforeExitEditModeEventHandler = new Infragistics.Win.BeforeExitEditModeEventHandler( this.OnEditorBeforeExitEditMode );
				}

				return this.embeddableEditor_BeforeExitEditModeEventHandler;
			}
		}

		#endregion // EmbeddableEditor_BeforeExitEditModeEventHandler

		#region EmbeddableEditor_AfterEnterEditModeEventHandler
		
		internal EventHandler EmbeddableEditor_AfterEnterEditModeEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_AfterEnterEditModeEventHandler )
				{
					this.embeddableEditor_AfterEnterEditModeEventHandler = new EventHandler( this.OnEditorAfterEnterEditMode );
				}

				return this.embeddableEditor_AfterEnterEditModeEventHandler;
			}
		}

		#endregion // EmbeddableEditor_AfterEnterEditModeEventHandler

		#region EmbeddableEditor_BeforeEnterEditModeEventHandler
		
		internal CancelEventHandler EmbeddableEditor_BeforeEnterEditModeEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_BeforeEnterEditModeEventHandler )
				{
					this.embeddableEditor_BeforeEnterEditModeEventHandler = new CancelEventHandler( this.OnEditorBeforeEnterEditMode );
				}

				return this.embeddableEditor_BeforeEnterEditModeEventHandler;
			}
		}

		#endregion // EmbeddableEditor_BeforeEnterEditModeEventHandler

		#region EmbeddableEditor_BeforeDropDownEventHandler
		
		internal CancelEventHandler EmbeddableEditor_BeforeDropDownEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_BeforeDropDownEventHandler )
				{
					this.embeddableEditor_BeforeDropDownEventHandler = new CancelEventHandler( this.OnEditorBeforeDropDown );
				}

				return this.embeddableEditor_BeforeDropDownEventHandler;
			}
		}

		#endregion // EmbeddableEditor_BeforeDropDownEventHandler

		#region EmbeddableEditor_AfterCloseupEventHandler
		
		internal EventHandler EmbeddableEditor_AfterCloseupEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_AfterCloseupEventHandler )
				{
					this.embeddableEditor_AfterCloseupEventHandler = new EventHandler( this.OnEditorAfterCloseup );
				}

				return this.embeddableEditor_AfterCloseupEventHandler ;
			}
		}

		#endregion // EmbeddableEditor_AfterCloseupEventHandler

		#region EmbeddableEditor_SelectionChangedEventHandler

		// SSP 10/11/02 UWG1749
		// Added event handler for SelectionChanged of the embeddable editor so that we
		// can fire CellListSelect event on the grid.
		//
		internal EventHandler EmbeddableEditor_SelectionChangedEventHandler
		{
			get
			{
				if ( null == this.embeddableEditor_SelectionChangedEventHandler )
				{
					this.embeddableEditor_SelectionChangedEventHandler = new EventHandler( this.OnEditorSelectionChanged );
				}

				return this.embeddableEditor_SelectionChangedEventHandler;
			}
		}

		#endregion // EmbeddableEditor_SelectionChangedEventHandler

		#region OnEditorBeforeDropDown
		
		internal void OnEditorBeforeDropDown( object sender, CancelEventArgs e )
		{
			UltraGrid grid = this.Grid as UltraGrid;

			Debug.Assert( null != grid, "Embeddable editor fired BeforeDropDown and the grid is not an instance of UltraGrid !" );

			UltraGridCell activeCell = this.ActiveCell;

            // MBS 2/21/08 - RowEditTemplate NA2008 V2
            // The cell could be edited by the template
            //
            //Debug.Assert( null != activeCell && activeCell.IsInEditMode, "Active cell not in edit mode while BeforeDropDown fired by an embeddable editor !" );
            //
            //if ( null == activeCell || !activeCell.IsInEditMode )
            //    return;
            Debug.Assert(null != activeCell && (activeCell.IsInEditMode || activeCell.Row.IsEditTemplateShown), "Active cell not in edit mode while BeforeDropDown fired by an embeddable editor !");
            //
            if (null == activeCell || (!activeCell.IsInEditMode && !activeCell.Row.IsEditTemplateShown))
                return;
			
			if ( null != grid )
			{
				// SSP 4/18/05 - NAS 5.2 Filter Row
				// This could get called for a filter cell.
				//
				// ------------------------------------------------------------------------
				if ( activeCell.IsFilterRowCell )
				{
					UltraGridFilterRow filterRow = (UltraGridFilterRow)activeCell.Row;
					UltraGridColumn column = activeCell.Column;

					FilterOperandStyle filterOperandStyle = column.FilterOperandStyleResolved;
					ValueList valueList = grid.FilterDropDown;

					// Check the count to fill it only once.
					//
					if ( null != valueList && valueList.ValueListItems.Count <= 0
						&& ( FilterOperandStyle.Combo == filterOperandStyle 
							 || FilterOperandStyle.DropDownList == filterOperandStyle 
                             // MBS 12/9/08 - NA9.1 Excel Style Filtering
                             // We want to have the ValueList populated for the editor
                             // that a filter provider will use as well
                             || FilterOperandStyle.FilterUIProvider == filterOperandStyle ) )
					{
                        // MBS 12/9/08 - NA9.1 Excel Style Filtering
                        bool usingFilterProvider = filterOperandStyle == FilterOperandStyle.FilterUIProvider;

						bool allRowsInBand = RowFilterMode.AllRowsInBand == filterRow.Band.RowFilterModeResolved;
						filterRow.Band.LoadFilterValueListHelper( valueList, 
							allRowsInBand ? null : filterRow.ParentCollection, 
							column,
                            // MBS 1/14/09 - TFS12359
                            // We always want to use all of the cell values for the filter provider
                            //false,
                            usingFilterProvider,                            
                            // MBS 1/14/08 - TFS12274
                            usingFilterProvider ? FilterValueListOptions.FilterUIProvider : FilterValueListOptions.None
                            );

						BeforeRowFilterDropDownEventArgs beforeRowFilterDropDownEventArgs = 
							new BeforeRowFilterDropDownEventArgs(
							column, 
							allRowsInBand ? null : filterRow.ParentCollection,
							valueList );

						grid.FireCommonEvent( CommonEventIds.BeforeRowFilterDropDown, beforeRowFilterDropDownEventArgs );
						if ( beforeRowFilterDropDownEventArgs.Cancel )
							e.Cancel = true;

						return;
					}
				}
				// ------------------------------------------------------------------------

				CancelableCellEventArgs ea = new CancelableCellEventArgs( activeCell );

				grid.FireEvent( GridEventIds.BeforeCellListDropDown, ea );

				e.Cancel = ea.Cancel;
			}
		}

		#endregion // OnEditorBeforeDropDown

		#region OnEditorSelectionChanged

		// SSP 10/11/02 UWG1749
		// Added event handler for SelectionChanged of the embeddable editor so that we
		// can fire CellListSelect event on the grid.
		//
		internal void OnEditorSelectionChanged( object sender, EventArgs e )
		{
			UltraGrid grid = this.Grid as UltraGrid;

			Debug.Assert( null != grid, "Embeddable editor fired SelectionChanged and the grid is not an instance of UltraGrid !" );

			UltraGridCell activeCell = this.ActiveCell;

            // MBS 2/21/08 - RowEditTemplate NA2008 V2
            // The cell could be edited by the template
            //
            //Debug.Assert( null != activeCell && activeCell.IsInEditMode, "Active cell not in edit mode while SelectionChanged fired by an embeddable editor !" );
            //
            //if ( null == activeCell || !activeCell.IsInEditMode )
            //    return;
            Debug.Assert(null != activeCell && (activeCell.IsInEditMode || activeCell.Row.IsEditTemplateShown), "Active cell not in edit mode while SelectionChanged fired by an embeddable editor !");
            //
            if (null == activeCell || (!activeCell.IsInEditMode && !activeCell.Row.IsEditTemplateShown))
                return;

			// SSP 5/16/05 - NAS 5.2 Filter Row
			// Forward the event to the filter cell.
			//
			if ( activeCell.IsFilterRowCell )
				((UltraGridFilterCell)activeCell).OnOperandSelectionChanged( sender, e );
			
			if ( null != grid )
			{
				CellEventArgs ea = new CellEventArgs( activeCell );

				grid.FireEvent( GridEventIds.CellListSelect, ea );
			}
		}

		#endregion // OnEditorSelectionChanged

		#region OnEditorAfterCloseup
		
		internal void OnEditorAfterCloseup( object sender, EventArgs e )
		{
			UltraGrid grid = this.Grid as UltraGrid;

			Debug.Assert( null != grid, "Embeddable editor fired BeforeDropDown and the grid is not an instance of UltraGrid !" );

			UltraGridCell activeCell = this.ActiveCell;

            // MBS 2/21/08 - RowEditTemplate NA2008 V2
            // The cell could be edited by the template
            //
            //Debug.Assert( null != activeCell && activeCell.IsInEditMode, "Active cell not in edit mode while BeforeDropDown fired by an embeddable editor !" );
            //
            //if ( null == activeCell || !activeCell.IsInEditMode )
            //    return;
            Debug.Assert(null != activeCell && (activeCell.IsInEditMode || activeCell.Row.IsEditTemplateShown), "Active cell not in edit mode while BeforeDropDown fired by an embeddable editor !");
            //
            if (null == activeCell || (!activeCell.IsInEditMode && !activeCell.Row.IsEditTemplateShown))
                return;

			if ( null != grid )
			{
				CellEventArgs ea = new CellEventArgs( activeCell );

				// SSP 8/15/02 UWG1585
				// A typeo. We are supposed to fire AfterCellListCloseUp instead of BeforeCellListDropDown.
				//
				//grid.FireEvent( GridEventIds.BeforeCellListDropDown, ea );
				grid.FireEvent( GridEventIds.AfterCellListCloseUp, ea );
			}
		}

		#endregion // OnEditorAfterCloseup

		#region OnEditorKeyDown
		
		internal void OnEditorKeyDown( object sender, KeyEventArgs e )
		{
			UltraGrid grid = this.Grid as UltraGrid;

			Debug.Assert( null != grid, "Some editor is in edit mode when the grid is a non-UltraGrid !" );

			grid.OnKeyDownForwarded( e );
		}

		#endregion // OnEditorKeyDown

		#region OnEditorKeyUp
		
		internal void OnEditorKeyUp( object sender, KeyEventArgs e )
		{
			UltraGrid grid = this.Grid as UltraGrid;

			Debug.Assert( null != grid, "Some editor is in edit mode when the grid is a non-UltraGrid !" );

			grid.OnKeyUpForwarded( e );
		}

		#endregion // OnEditorKeyUp

		#region OnEditorKeyPress
		
		internal void OnEditorKeyPress( object sender, KeyPressEventArgs e )
		{
			UltraGrid grid = this.Grid as UltraGrid;

			Debug.Assert( null != grid, "Some editor is in edit mode when the grid is a non-UltraGrid !" );

			grid.OnKeyPressForwarded( e );
		}

		#endregion // OnEditorKeyPress
		
		#region OnEditorBeforeEnterEditMode
		
		internal void OnEditorBeforeEnterEditMode( object sender, CancelEventArgs e )
		{
			// Reset the flags
			//
			this.forceExitEditOperationFlag = false;
			this.cancellingEditOperationFlag = false;

			UltraGrid grid = this.Grid as UltraGrid;
			Debug.Assert( null != grid, "No grid !" );

			if ( null != grid )
			{				
				grid.FireEvent( GridEventIds.BeforeEnterEditMode, e );
			}

			// SSP 4/8/03 UWG2113
			// We have to do this before the editor enters the edit mode or at least
			// before it accesses the EmbeddableOwnerInfo.GetAutoSizeEditInfo. Here
			// we are doing all this after we are entering the edit mode above. It
			// used to work before we changed the behavior of the embeddable text box
			// to syncronously display the text box.
			// Moved this code from the Cell.EnterEditorInEditMode to here. 
			// 
			// ------------------------------------------------------------------------
			UltraGridCell cell = this.ActiveCell;
			Debug.Assert( null != cell, "No active cell !" );
			if ( ! e.Cancel && null != grid && null != cell && DefaultableBoolean.True == cell.Column.AutoSizeEdit )
			{
				this.LastAutoSizeEditEventArgs = null;

				// If cancelled then treat as though AutoSizeEdit is false
				//
				CancelableAutoSizeEditEventArgs autoSizeEventArgs = new CancelableAutoSizeEditEventArgs( cell.Column.AutoSizeEdit );

				grid.FireEvent( GridEventIds.BeforeAutoSizeEdit, autoSizeEventArgs );

				// Store the event args on the layout so when the embeddable editor asks
				// for autosize edit info, we can refere to it.
				//
				if ( !autoSizeEventArgs.Cancel )
					this.LastAutoSizeEditEventArgs = autoSizeEventArgs;
			}
			// ------------------------------------------------------------------------
		}

		#endregion // OnEditorBeforeEnterEditMode

		#region OnEditorAfterEnterEditMode

		internal void OnEditorAfterEnterEditMode( object sender, EventArgs e )
		{
			UltraGrid grid = this.Grid as UltraGrid;
			Debug.Assert( null != grid, "No grid !" );

			// SSP 7/8/03 - Support devepoper calling EnterEditMode on the editor directly.
			// Moved below code from the if block so we can reuse the activecell variable 
			// further below.
			//
			UltraGridCell activeCell = this.ActiveCell;
			Debug.Assert( null != activeCell, "Null activeCell while OnEditorAfterEnterEditMode fired !" );
			if ( null == activeCell )
				return;
			
			// SSP 7/8/03 - Support devepoper calling EnterEditMode on the editor directly.
			// Added the OnAfterCellEnterEditMode helper method of the cell so call that.
			//
			
			activeCell.OnAfterCellEnterEditMode( );
			
			Debug.Assert( null != this.CellInEditMode, "There must have been a cell in edit mode." );

			if ( null != grid )
			{		
				grid.FireEvent( GridEventIds.AfterEnterEditMode, e );
			}
		}

		#endregion // OnEditorAfterEnterEditMode

		#region OnEditorBeforeExitEditMode

		internal void OnEditorBeforeExitEditMode( object sender, Infragistics.Win.BeforeExitEditModeEventArgs e )
		{
			UltraGrid grid = this.Grid as UltraGrid;
			Debug.Assert( null != grid, "No grid !" );

			// JM 07-03-03 - Temporarily comment out until SP deals with this siutation
			//				 (i.e., editor was placed into edit mode directly (from the
			//				 ink provider) without going thru the grid)
			//Debug.Assert( null != this.CellInEditMode, "There must have been a cell in edit mode." );

			if ( null != grid && null != this.CellInEditMode )
			{
				Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs eventArgs =
					new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs( this.ForceExitEditOperationFlag, this.CancellingEditOperationFlag );

                // MRS 4/10/2008 - BR31917
                eventArgs.Cancel = e.Cancel;

				grid.FireEvent( GridEventIds.BeforeExitEditMode, eventArgs );
			
				// Ignore the Cancel on before exit edit mode event args if we are forcing
				// exitting edit mode.
				//
				if ( !this.ForceExitEditOperationFlag )
					e.Cancel = eventArgs.Cancel;

				// If the user cancels the exit, then return without performing
				// any validation on the data.
				//
				if ( e.Cancel && !this.ForceExitEditOperationFlag )
					return;
			}

			// If not cancelling the edit operation, then validate the value in the editor
			// to see if it matches the criteria like MaskInput, MinDate-MaxDate restraints.
			// 
			if ( !this.CancellingEditOperationFlag )
			{
				UltraGridCell cell = this.CellInEditMode;

				// JM 07-03-03 - Temporarily comment out until SP deals with this siutation
				//				 (i.e., editor was placed into edit mode directly (from the
				//				 ink provider) without going thru the grid)
				//Debug.Assert( null != cell, "No cell in edit mode in OnEditorBEFOREExitEditMode !" );

				if ( null != cell )
				{
					bool stayInEdit = false;

					// Try to validate and commit the value. CommitEditValue validates the input
					// and commits that to the bound list. If unsuccessful, then cancel the exit
					// edit mode, unless we are forcing the exit.
					//
					// SSP 10/16/02 UWG1116
					// Pass in true for newly added parameter fireDataError (second argument 
					// in the method call). 
					//
                    // MBS 4/24/08 - RowEditTemplate NA2008 V2
                    // If we're closing the template, we don't want to show any data errors
                    // from committing the value
					//if ( !cell.CommitEditValue( ref stayInEdit, true ) )
                    bool fireDataError = true;
                    bool forceDontThrowException = false;
                    UltraGridRowEditTemplate template = cell.Row.RowEditTemplateResolved;
                    if (template != null && template.IsClosing)
                    {
                        fireDataError = false;
                        forceDontThrowException = true;
                    }
                    //
                    if (!cell.CommitEditValue(ref stayInEdit, fireDataError,forceDontThrowException))
					{
						// If the user set CellDataError event's arg's StayInEditMode to true,
						// then stay in edit mode by cancling this BeforeExitEditMode event.
						// However, only do so if we are not forcing exiting the edit mode.
						//
						if ( stayInEdit && !this.ForceExitEditOperationFlag )
							e.Cancel = true;
					}
				}
			}
		}

		#endregion // OnEditorBeforeExitEditMode

		#region OnEditorAfterExitEditMode

		internal void OnEditorAfterExitEditMode( object sender, EventArgs e )
		{
			// SSP 7/8/03 - Support devepoper calling EnterEditMode on the editor directly.
			// Added the OnAfterCellEnterEditMode helper method of the cell so call that.
			//
			// ----------------------------------------------------------------------------
            UltraGridCell activeCell = this.ActiveCell;

            // MBS 11/14/07 - BR28423
            // We need to make sure that we also exit edit mode on the embeddable element
            // so that the various events can be unhooked, such as listening for a mouse wheel
            if (activeCell == null && this.EmbeddableElementBeingEdited != null)
            {
                UltraGridCell cell = EmbeddableEditorOwnerInfo.GetCellIfAllocatedFromOwnerContext(
                    this.EmbeddableElementBeingEdited.Owner,
                    this.EmbeddableElementBeingEdited.OwnerContext);

                if (cell != null)
                    cell.OnAfterCellExitEditMode();
            }

			if ( null != activeCell )
				activeCell.OnAfterCellExitEditMode( );
			// ----------------------------------------------------------------------------

			// Reset the flags
			//
			this.EmbeddableElementBeingEdited = null;
			this.forceExitEditOperationFlag = false;
			this.cancellingEditOperationFlag = false;

			// SSP 8/8/02
			// Embeddable editors auto size edit feature.
			// Reset the variable to null.
			//
			this.lastAutoSizeEditEventArgs = null;

			UltraGrid grid = this.Grid as UltraGrid;
			Debug.Assert( null != grid, "No grid !" );

			if ( null != grid )
			{			
				// SSP 3/24/05 BR02979
				// Reset the editorWantedLastKeyDown on key up as well as when we exit edit mode on a cell.
				//
				grid.editorWantedLastKeyDown = false;

				grid.FireEvent( GridEventIds.AfterExitEditMode, e );
			}
		}		

		#endregion // OnEditorAfterExitEditMode

		#region RemoveTabKeyActions

		// SSP 4/30/03 UWG2210
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void RemoveTabKeyActions( KeyActionMappings keyActionMappings )
		internal static void RemoveTabKeyActions( KeyActionMappings keyActionMappings )
		{
			Debug.Assert( null != keyActionMappings, "No key action mappings off the editorwithmask !" );

			if ( null != keyActionMappings )
			{
				for ( int i = keyActionMappings.Count - 1; i >= 0; i-- )
				{
					if ( Keys.Tab == keyActionMappings[i].KeyCode )
						keyActionMappings.Remove( i );
				}
			}
		}
		
		#endregion // RemoveTabKeyActions
		
		#region EditorWithMask
		
		internal EditorWithMask EditorWithMask
		{
			get
			{
				if ( null == this.editorWithMask )
				{
					this.editorWithMask = new EditorWithMask( );

					this.editorWithMask.InvalidChar += new EditorWithMask.InvalidCharEventHandler( this.OnMaskEditControlInvalidChar );
					this.editorWithMask.InvalidOperation += new EditorWithMask.InvalidOperationEventHandler( this.OnMaskEditControlInvalidOperation );

					// SSP 4/30/03 UWG2210
					// Remove the action mappings for tabs since we want to go from cell to cell
					// and not from section to section in the grid.
					//
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.RemoveTabKeyActions( this.editorWithMask.KeyActionMappings );
					UltraGridLayout.RemoveTabKeyActions( this.editorWithMask.KeyActionMappings );
				}

				return this.editorWithMask;
			}
		}	

		#endregion // EditorWithMask

		
		//

		

		#region OnMaskEditControlInvalidChar

		private void OnMaskEditControlInvalidChar( object sender, Infragistics.Win.UltraWinMaskedEdit.InvalidCharEventArgs e )
		{
			// SSP 3/14/02
			// Whenever there is a masking error in masked edit control, fire
			// the Error event in the grid so the user get's notified of
			// masking errors and is given a chance to cancel the beep.
			//
			//----------------------------------------------------------------
			UltraGrid grid = this.Grid as UltraGrid;

			Debug.Assert( null != grid );
			if ( null == grid )
				return;

			Infragistics.Win.EditorWithMask maskEditor = this.EditorInEditMode as EditorWithMask;

			Debug.Assert( null != maskEditor && maskEditor.IsInEditMode, "EditorWithMask is not in edit mode, yet OnMaskEditControlInvalidChar called !" );

			if ( null != maskEditor && maskEditor.IsInEditMode )
			{
				// SSP 8/8/02 UWG1287
				// Use the invalid character typed rather than the text currently in the 
				// masked edit control.
				//
				//MaskErrorInfo mi = new MaskErrorInfo(
				//	maskEditor.GetText( null != this.ActiveCell ? this.ActiveCell.Column.MaskDataMode : MaskMode.IncludeLiterals ), 0 );
				MaskErrorInfo mi = new MaskErrorInfo( "" + e.Char, null != this.EditorWithMask.DisplayChars ? this.EditorWithMask.DisplayChars.IndexOf( e.DisplayChar ) : 0 );
			
				ErrorEventArgs ee = new ErrorEventArgs( mi, "Invalid character typed." );

				grid.FireEvent( GridEventIds.Error, ee );

				e.Beep = !ee.MaskErrorInfo.CancelBeep;
			}
			//----------------------------------------------------------------
		}

		#endregion // OnMaskEditControlInvalidChar

		#region OnMaskEditControlInvalidOperation

		private void OnMaskEditControlInvalidOperation( object sender, Infragistics.Win.UltraWinMaskedEdit.InvalidOperationEventArgs e )
		{
			// Don't fire the error event in case of InvalidOperation
			// Just set the beep to false.
			//
			e.Beep = false;
		}

		#endregion // OnMaskEditControlInvalidOperation

		#region EditorWithText
		
		internal EditorWithText EditorWithText
		{
			get
			{
				if ( null == this.editorWithText )
				{
					this.editorWithText = new EditorWithText( );
				}

				return this.editorWithText;
			}
		}

		#endregion // EditorWithText

		#region ColorPickerEditor
		
		internal ColorPickerEditor ColorPickerEditor
		{
			get
			{
				if ( null == this.colorPickerEditor )
				{
					this.colorPickerEditor = new ColorPickerEditor( );
				}

				return this.colorPickerEditor;
			}
		}

		#endregion // ColorPickerEditor

		#region CheckEditorTriState

		internal Infragistics.Win.CheckEditor CheckEditorTriState
		{
			get
			{
				if ( null == this.checkEditorTriState )
				{
					this.checkEditorTriState = new CheckEditor( );
					this.checkEditorTriState.ThreeState = true;
					
					// SSP 11/12/02 UWG1828
					//
					//this.checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
					this.checkEditorTriState.CheckAlign = ContentAlignment.MiddleCenter;
				}

				return this.checkEditorTriState;
			}
		}

		#endregion // CheckEditorTriState

		#region ImageRenderer

		// SSP 8/11/03 - Embeddable image renderer
		//
		internal Infragistics.Win.EmbeddableImageRenderer ImageRenderer
		{
			get
			{
				if ( null == this.imageRenderer )
				{
					this.imageRenderer = new Infragistics.Win.EmbeddableImageRenderer( );
					this.imageRenderer.DrawBorderShadow = false;
				}

				return this.imageRenderer;
			}
		}

		#endregion // ImageRenderer

		#region ImageRenderer

		// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
		//
		internal Infragistics.Win.EmbeddableImageRenderer GroupByRowImageRenderer
		{
			get
			{
				if ( null == this.groupByRowImageRenderer )
				{
					this.groupByRowImageRenderer = new Infragistics.Win.EmbeddableImageRenderer( );
					this.groupByRowImageRenderer.DrawBorderShadow = false;
				}

				return this.groupByRowImageRenderer;
			}
		}

		#endregion // ImageRenderer

		#region CheckEditor
		
		internal Infragistics.Win.CheckEditor CheckEditor
		{
			get
			{
				if ( null == this.checkEditor )
				{
					this.checkEditor = new CheckEditor( );
					this.checkEditor.CheckAlign = ContentAlignment.MiddleCenter;
				}				

				return this.checkEditor;
			}
		}

		#endregion // CheckEditor

		#region DateTimeEditor
		
		internal Infragistics.Win.DateTimeEditor DateTimeEditor
		{
			get
			{
				if ( null == this.dateTimeEditor )
				{
					this.dateTimeEditor = new Infragistics.Win.DateTimeEditor( );

					// SSP 4/30/03 UWG2210
					// Remove the action mappings for tabs since we want to go from cell to cell
					// and not from section to section in the grid.
					//
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.RemoveTabKeyActions( this.dateTimeEditor.KeyActionMappings );
					UltraGridLayout.RemoveTabKeyActions( this.dateTimeEditor.KeyActionMappings );
				}

				return this.dateTimeEditor;
			}
		}

		#endregion // DateTimeEditor

		#region FilterOperatorEditor

		// SSP 3/30/05 - NAS 5.2 Filter Row
		//
		internal EmbeddableEditorBase GetFilterOperatorEditor( UltraGridColumn column )
		{
			if ( FilterOperatorLocation.AboveOperand == column.FilterOperatorLocationResolved )
			{
				if ( null == this.filterOperatorEditor_AboveOperand )
					this.filterOperatorEditor_AboveOperand = new EditorWithCombo( );

				return this.filterOperatorEditor_AboveOperand;
			}
			else // With Cell Filter Operator Location Style
			{
				if ( null == this.filterOperatorEditor_WithOperand )
				{
					EditorWithCombo comboEditor = new EditorWithCombo( );
					comboEditor.ButtonDisplayStyle = Infragistics.Win.ButtonDisplayStyle.Never;
					Infragistics.Win.UltraWinEditors.EditorButton button = new Infragistics.Win.UltraWinEditors.EditorButton( );

					// Make sure the filter icons do not scale and are aligned properly in the filter operator button.
					//
					button.Width = FilterOperatorUIElement.DEFAULT_FILTER_OPERATOR_BUTTON_SIZE;

					comboEditor.ButtonsRight.Add( button );

					this.filterOperatorEditor_WithOperand = comboEditor;
				}

				return this.filterOperatorEditor_WithOperand;
			}
		}

		#endregion // FilterOperatorEditor

		#region GetAssociatedFilterComparisionOperator

		// SSP 3/30/05 - NAS 5.2 Filter Row
		//
		internal FilterComparisionOperator GetAssociatedFilterComparisionOperator( 
			FilterOperatorDefaultValue filterOperatorDefaultValue )
		{
			this.InitFilterRowCache( );

			for ( int i = 0; i < this.filterRowCache.Length; i += FILTER_ROW_CACHE_STEP )
			{
				if ( filterOperatorDefaultValue == (FilterOperatorDefaultValue)this.filterRowCache[ 2 + i ] )
					return (FilterComparisionOperator)this.filterRowCache[ 1 + i ];
			}

			Debug.Assert( false );
			return FilterComparisionOperator.Custom;
		}

		#endregion // GetAssociatedFilterComparisionOperator

		#region GetAssociatedFilterDropDownItem

		// SSP 5/9/05 - NAS 5.2 Filter Row
		//
		internal FilterOperatorDropDownItems GetAssociatedFilterDropDownItem( 
			FilterComparisionOperator filterComparisionOperator )
		{
			this.InitFilterRowCache( );

			for ( int i = 0; i < this.filterRowCache.Length; i += FILTER_ROW_CACHE_STEP )
			{
				if ( filterComparisionOperator == (FilterComparisionOperator)this.filterRowCache[ 1 + i ] )
					return (FilterOperatorDropDownItems)this.filterRowCache[ i ];
			}

			Debug.Assert( false );
			return FilterOperatorDropDownItems.None;
		}

		// SSP 5/12/05 - NAS 5.2 Filter Row
		//
		internal FilterOperatorDropDownItems GetAssociatedFilterDropDownItem( 
			FilterOperatorDefaultValue filterOperatorDefaultValue )
		{
			return this.GetAssociatedFilterDropDownItem( 
				this.GetAssociatedFilterComparisionOperator( filterOperatorDefaultValue ) );
		}

		#endregion // GetAssociatedFilterDropDownItem

		#region GetFilterComparisonOperatorSymbolHelper

		// SSP 3/30/05 - NAS 5.2 Filter Row
		//
		private Image GetFilterComparisonOperatorSymbolHelper( FilterComparisionOperator filterOperator, string symbol )
		{
			Graphics gr = null;
			Graphics grBmp = null;
			Font createdFont = null;
			Image img = null;
			try
			{
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//img = this.GetImageFromResourse( "Images.FilterOp_" + filterOperator.ToString( ) + ".png" );
				img = UltraGridLayout.GetImageFromResourse( "Images.FilterOp_" + filterOperator.ToString() + ".png" );

				if ( null != img )
				{
					// Add 1 pixel pading to the left of the images to make them align properly in the filter
					// operator buttons as well as leave some spacing between the left edge of the value list 
					// and operator icons.
					//
					if ( img is Bitmap )
					{
						Image imgWithPadding = GridUtils.AddPadding( (Bitmap)img, new Infragistics.Win.Layout.Insets( 1, 0, 0, 0 ), Color.Transparent );

						// Dispose the original image since it's not needed anymore. The returned image is a new image.
						//
						img.Dispose( );

						img = imgWithPadding;
					}

					return img;
				}

				Debug.Assert( false, string.Format( "There should be an image in the resource for {0} symbol.", symbol ) );
				
				AppearanceData appData = new AppearanceData( );
				Size sz = this.CalculateFontSize( ref appData, symbol );

				Font font = this.FontDefault;
				createdFont = appData.CreateFont( font );
				if ( null != createdFont )
					font = createdFont;
				
				gr = this.GetCachedGraphics( );
				Bitmap bmp = new Bitmap( sz.Width, sz.Height, gr );

				Rectangle rect = new Rectangle( 0, 0, bmp.Width, bmp.Height );

				grBmp = Graphics.FromImage( bmp );
				grBmp.FillRectangle( System.Drawing.Brushes.Transparent, rect );
				grBmp.DrawString( symbol, font, System.Drawing.Brushes.Black, rect );

				img = bmp;
				return bmp;
			}
			finally
			{
				if ( null != gr )
					this.ReleaseCachedGraphics( gr );

				if ( null != grBmp )
					grBmp.Dispose( );

				if ( null != createdFont )
					createdFont.Dispose( );
			}
		}

		#endregion // GetFilterComparisonOperatorSymbolHelper

		#region GetFilterComparisonOperatorName

		// SSP 5/16/05 - NAS 5.2 Filter Row
		//
		internal string GetFilterComparisonOperatorName( FilterComparisionOperator filterComparisionOperator )
		{
			this.InitFilterRowCache( );

			for ( int i = 0; i < this.filterRowCache.Length; i += FILTER_ROW_CACHE_STEP )
			{
				if ( filterComparisionOperator == (FilterComparisionOperator)this.filterRowCache[ 1 + i ] )
					return this.filterRowCache[ 3 + i ].ToString( );
			}

			//Debug.Assert( false );
			return string.Empty;
		}

		#endregion // GetFilterComparisonOperatorName

		#region InitFilterRowCache
		
		// SSP 5/9/05 - NAS 5.2 Filter Row
		//
		internal void InitFilterRowCache( )
		{
			if ( null != this.filterRowCache )
				return;

			// Operator enum value, operator name, operator symbol, operator symbol as image
			//
			object[] info = this.filterRowCache = new object[]
				{
					// FilterOperatorDropDownItems instance, FilterComparisionOperator instance, FilterOperatorDefaultValue instance, Text Resource Name, Symbol, Image
					FilterOperatorDropDownItems.Equals, null, null, "RowFilterDropDownEquals", "=", null,
					FilterOperatorDropDownItems.NotEquals, null, null, "RowFilterDropDownNotEquals", "!=", null,
					FilterOperatorDropDownItems.LessThan, null, null, "RowFilterDropDownLessThan", "<", null,
					FilterOperatorDropDownItems.LessThanOrEqualTo, null, null, "RowFilterDropDownLessThanOrEqualTo", "<=", null,
					FilterOperatorDropDownItems.GreaterThan, null, null, "RowFilterDropDownGreaterThan", ">", null,
					FilterOperatorDropDownItems.GreaterThanOrEqualTo, null, null, "RowFilterDropDownGreaterThanOrEqualTo", ">=", null,
					FilterOperatorDropDownItems.Like, null, null, "RowFilterDropDownLike", "L", null,
					FilterOperatorDropDownItems.Match, null, null, "RowFilterDropDownMatch", "M", null,
					FilterOperatorDropDownItems.StartsWith, null, null, "RowFilterDropDown_Operator_StartsWith", "T..", null,
					FilterOperatorDropDownItems.Contains, null, null, "RowFilterDropDown_Operator_Contains", "..T..", null,
					FilterOperatorDropDownItems.EndsWith, null, null, "RowFilterDropDown_Operator_EndsWith", "..T", null,
					FilterOperatorDropDownItems.DoesNotStartWith, null, null, "RowFilterDropDown_Operator_DoesNotStartWith", "!T..", null,
					FilterOperatorDropDownItems.DoesNotContain, null, null, "RowFilterDropDown_Operator_DoesNotContain", "!..T..", null,
					FilterOperatorDropDownItems.DoesNotEndWith, null, null, "RowFilterDropDown_Operator_DoesNotEndWith", "!..T", null,
					FilterOperatorDropDownItems.DoesNotMatch, null, null, "RowFilterDropDown_Operator_DoesNotMatch", "!M", null,
					FilterOperatorDropDownItems.NotLike, null, null, "RowFilterDropDown_Operator_NotLike", "!L", null 
				};

			for ( int i = 0; i < info.Length; i += FILTER_ROW_CACHE_STEP )
			{
				FilterOperatorDropDownItems filterOperatorDropDownItem = (FilterOperatorDropDownItems)info[ i ];
				FilterComparisionOperator filterComparisonOperator;

				try
				{
					// Conver the FilterOperatorDropDownItems into FilterComparisionOperator.
					//
					string enumName = filterOperatorDropDownItem.ToString( );
					filterComparisonOperator = (FilterComparisionOperator)Enum.Parse( 
						typeof( FilterComparisionOperator ), enumName );
					info[ 1 + i ] = filterComparisonOperator;

					FilterOperatorDefaultValue filterOperatorDefaultValue = (FilterOperatorDefaultValue)Enum.Parse( 
						typeof( FilterOperatorDefaultValue ), enumName );
					info[ 2 + i ] = filterOperatorDefaultValue;
				}
				catch
				{
					Debug.Assert( false );
					continue;
				}

				// Get the localized name of the operator. Use the LocalizedResourceGetter for
				// that purpose so if the name is changed by the user then we get the updated
				// name.
				//
				//info[ 3 + i ] = SR.GetString( info[ 3 + i ].ToString( ) );
				info[ 3 + i ] = new LocalizedResourceGetter( info[ 3 + i ].ToString( ) );
			}
		}

		#endregion // InitFilterRowCache

		#region ClearFilterRowCache

		// SSP 5/9/05 - NAS 5.2 Filter Row
		//
		internal void ClearFilterRowCache( )
		{
			if ( null != this.filterRowCache )
			{
				object[] info = this.filterRowCache;
				this.filterRowCache = null;

				for ( int i = 0; i < info.Length; i += FILTER_ROW_CACHE_STEP )
				{
					if ( info[ 4 + i ] is Image )
						((Image)info[ 4 + i ]).Dispose( );
				}
			}

			if ( null != this.filterClearButtonImageFromResource )
			{
				this.filterClearButtonImageFromResource.Dispose( );
				this.filterClearButtonImageFromResource = null;
			}

			this.InternalResetFilterOperatorsValueList( );
		}

		internal void InternalResetFilterOperatorsValueList( )
		{
			if ( null != this.filterOperatorValueListAll )
			{
				this.filterOperatorValueListAll.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.filterOperatorValueListAll.Dispose( );
				this.filterOperatorValueListAll = null;
			}
		}

		#endregion // ClearFilterRowCache

		#region LoadFilterOperatorValueList
		
		// SSP 3/30/05 - NAS 5.2 Filter Row
		//
		internal void LoadFilterOperatorValueList( ValueList valueList, FilterOperatorDropDownItems items )
		{
			valueList.ValueListItems.Clear( );

			this.InitFilterRowCache( );

			if ( null != this.filterOperatorValueListAll && valueList != this.filterOperatorValueListAll )
			{
				valueList.DisplayStyle = this.filterOperatorValueListAll.DisplayStyle;
				valueList.ScaleItemImage = this.filterOperatorValueListAll.ScaleItemImage;
				valueList.SortStyle = this.filterOperatorValueListAll.SortStyle;

				foreach ( ValueListItem vlItem in this.filterOperatorValueListAll.ValueListItems )
				{
					object val = vlItem.DataValue;
					Debug.Assert( val is FilterComparisionOperator );
					if ( ! ( val is FilterComparisionOperator ) )
						continue;

					FilterOperatorDropDownItems vlDropDownItem = this.GetAssociatedFilterDropDownItem( (FilterComparisionOperator)val );
					if ( 0 != ( vlDropDownItem & items ) )
					{
						ValueListItem clone = new ValueListItem( );
						clone.Appearance = vlItem.Appearance;
						clone.DataValue = vlItem.DataValue;
						clone.DisplayText = vlItem.DisplayText;
						clone.Tag = vlItem.Tag;
						
						valueList.ValueListItems.Add( clone );
					}
				}
			}
			else
			{
				valueList.DisplayStyle = ValueListDisplayStyle.DisplayTextAndPicture;
				valueList.ScaleItemImage = Infragistics.Win.ScaleImage.Never;

				object[] info = this.filterRowCache;
				for ( int i = 0; i < info.Length; i += FILTER_ROW_CACHE_STEP )
				{
					FilterOperatorDropDownItems filterOperatorDropDownItem = (FilterOperatorDropDownItems)info[ i ];
					FilterComparisionOperator filterComparisonOperator = (FilterComparisionOperator)info[ 1 + i ];

					if ( filterOperatorDropDownItem == ( filterOperatorDropDownItem & items ) )
					{
						string name = info[ 3 + i ].ToString( );
						string symbol = info[ 4 + i ].ToString( );
						Image image = (Image)info[ 5 + i ];
						if ( null == image )
							info[ 5 + i ] = image = this.GetFilterComparisonOperatorSymbolHelper( filterComparisonOperator, symbol );

						ValueListItem item = valueList.ValueListItems.Add( filterComparisonOperator, name );
						if ( null != image )
							item.Appearance.Image = image;
					}
				}
			}
		}

		#endregion // LoadFilterOperatorValueList

		#region FilterOperatorValueListAll
	
		// SSP 3/30/05 - NAS 5.2 Filter Row
		//
		internal ValueList FilterOperatorValueListAll
		{
			get
			{
				if ( null == this.filterOperatorValueListAll )
				{
					this.filterOperatorValueListAll = new ValueList( );
					this.LoadFilterOperatorValueList( this.filterOperatorValueListAll, FilterOperatorDropDownItems.All );
					this.filterOperatorValueListAll.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.filterOperatorValueListAll;
			}
		}

		#endregion // FilterOperatorValueListAll

		#region FilterOperatorsValueList

		/// <summary>
		/// Retruns the value list that will be used for the operator drop down list in the
		/// filter row as well as in the custom filter dialog.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>FilterOperatorsValueList</b> returns the value list that will be used for the
		/// operator drop down list in the filter row as well as in the custom filter dialog. 
		/// Data values of value list items are <see cref="FilterComparisionOperator"/> instances.
		/// You can use this property to change the appearance of the drop down list and its
		/// items. You can also change the operator images (<see cref="ValueListItem.Appearance"/>,
		/// operator names (<see cref="ValueListItem.DisplayText"/>). You can also control the
		/// order of items by rearranging the items in the ValueListItems collection. <b>Note:</b>
		/// You should not change the DataValue of the value list items. Also any new items you
		/// add will not show up.
		/// </p>
		/// <seealso cref="UltraGridOverride.FilterOperatorDropDownItems"/> <seealso cref="UltraGridOverride.FilterOperatorLocation"/> <seealso cref="UltraGridOverride.FilterUIType"/> 
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ Browsable( false ) ]
		[ LocalizedDescription("LD_UltraGridLayout_P_FilterOperatorsValueList")]
		[ LocalizedCategory("LC_Display") ]
		public ValueList FilterOperatorsValueList
		{
			get
			{
				return this.FilterOperatorValueListAll;
			}
		}

		#endregion // FilterOperatorsValueList

		#region ResetFilterOperatorsValueList

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterOperatorsValueList( )
		{
			this.InternalResetFilterOperatorsValueList( );
		}

		#endregion // ResetFilterOperatorsValueList

		#region GetFilterOperandEditor

		#region FilterOperandEditorWithCombo

		// SSP 5/19/05 BR03866
		// 
	    private class FilterOperandEditorWithCombo : EditorWithCombo
		{
			internal FilterOperandEditorWithCombo( ) : base( )
			{
			}

			protected override void OnTextBoxMouseWheel( MouseEventArgs e )
			{
				// If the filter operator value list is dropped down then let it process the 
				// mouse wheel.
				//
				EmbeddableUIElementBase elem = this.IsInEditMode ? this.ElementBeingEdited : null;
				if ( null != elem )
				{
					FilterCellUIElement cellElem = (FilterCellUIElement)elem.GetAncestor( typeof( FilterCellUIElement ) );
					UltraGridFilterCell filterCell = null != cellElem ? cellElem.Cell as UltraGridFilterCell : null;

					if ( null != filterCell )
					{
						ValueList operatorValueList = filterCell.Column.FilterOperatorEditorOwnerInfo.GetValueList( filterCell ) as ValueList;
						if ( null != operatorValueList && ((IValueList)operatorValueList).IsDroppedDown )
						{
							operatorValueList.ProcessOnMouseWheel( e );
							return;
						}
					}
				}

				base.OnTextBoxMouseWheel( e );
			}
		}

		#endregion // FilterOperandEditorWithCombo

		// SSP 4/5/05 - NAS 5.2 Filter Row
		//
		internal EmbeddableEditorBase GetFilterOperandEditor( UltraGridColumn column )
		{
			FilterOperandStyle style = column.FilterOperandStyleResolved;
			EmbeddableEditorBase editor = null;			

			switch ( style )
			{
				case Infragistics.Win.UltraWinGrid.FilterOperandStyle.Combo:
				case Infragistics.Win.UltraWinGrid.FilterOperandStyle.DropDownList:
					if ( null == this.filterRowComboEditor )
						// SSP 5/19/05 BR03866
						// 
						//this.filterRowComboEditor = new EditorWithCombo( );
						this.filterRowComboEditor = new FilterOperandEditorWithCombo( );

					editor = this.filterRowComboEditor;
					break;
				case Infragistics.Win.UltraWinGrid.FilterOperandStyle.UseColumnEditor:
					editor = column.Editor;
					break;
				case Infragistics.Win.UltraWinGrid.FilterOperandStyle.Default:
                // MBS 12/4/08 - NA9.1 Excel Style Filtering
                // Try to get the editor from the FilterUIProvider, otherwise we'll just behave as normally.
                case FilterOperandStyle.FilterUIProvider:
                    if (column.Band != null)
                    {
                        IFilterUIProvider filterProvider = column.Band.FilterUIProviderResolved;
                        if (filterProvider != null)
                        {
                            EmbeddableEditorBase filterEditor = filterProvider.GetFilterCellEditor(column);
                            if (filterEditor != null)
                            {
                                editor = filterEditor;
                                break;
                            }
                        }
                    }
                    goto default;

				case Infragistics.Win.UltraWinGrid.FilterOperandStyle.None:
				case Infragistics.Win.UltraWinGrid.FilterOperandStyle.Disabled:
				case Infragistics.Win.UltraWinGrid.FilterOperandStyle.Edit:
				default:
					if ( null == this.filterRowTextEditor )
						this.filterRowTextEditor = new EditorWithText( );

					editor = this.filterRowTextEditor;
					break;
			}

			return editor;
		}

		#endregion // GetFilterOperandEditor

		#endregion // Embeddable editor code

		// SSP 5/31/02 UWG1152
		// Added MaxBandDepth property to allow the user to be able to explicitly
		// specify upto how many levels the grid should drill down in the bands
		// hierarchy. This can come in use where the data source has a table related
		// to itselef (recursive data relation).
		//
		#region MaxBandDepth

		/// <summary>
		/// Gets or sets the max band depth. Grid will load upto MaxBandDepth number of bands. <b>Note</b> that you have to set the <b>MaxBandDepth</b> before binding the UltraGrid.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this property to force the grid to load upto only a certain number of bands. This can come in use where you have a recursive data relation where a table is related to itself and you only want the grid to drill down the band hierarchy only upto a certain level.</p>
		/// <p class="body">Default value for this property is 100 and range is 1-100 inclusive. Throws an ArgumentOutOfRange exception if set to a value below 1 or greater than 100.</p>
		/// <p class="body"><b>Note:</b> You must set the <b>MaxBandDepth</b> before binding the UltraGrid to a data source in order for this property to have any effect.</p>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridLayout_P_MaxBandDepth")]
		[ LocalizedCategory("LC_Behavior") ]
		public int MaxBandDepth
		{
			get
			{
				return this.maxBandDepth;
			}
			set
			{
                // MRS NAS v8.2 - Unit Testing
                if (maxBandDepth == value)
                    return;

				if ( value < 1 || value > UltraGridBand.MAX_BAND_DEPTH )
					throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_184"), Shared.SR.GetString("LE_ArgumentNullException_335") );
                                
				this.maxBandDepth = value;

                // MRS NAS v8.2 - Unit Testing
                this.NotifyPropChange(PropertyIds.MaxBandDepth);
			}
		}

		#endregion // MaxBandDepth

		#region ShouldSerializeMaxBandDepth

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMaxBandDepth( )
		{
			return UltraGridBand.MAX_BAND_DEPTH != this.MaxBandDepth;
		}

		#endregion // ShouldSerializeMaxBandDepth

		#region ResetMaxBandDepth

		/// <summary>
		/// Resets the MaxBandDepth property to its default value 100.
		/// </summary>
		public void ResetMaxBandDepth( )
		{
			this.MaxBandDepth = UltraGridBand.MAX_BAND_DEPTH;
		}

		#endregion // ResetMaxBandDepth

		#region MaxBandDepthEnforced

		internal int MaxBandDepthEnforced
		{
			get
			{
				return this.maxBandDepthCurrentlyEnforced;
			}
		}

		#endregion // MaxBandDepthEnforced

		#region PriorityScrolling
		
		// SSP 6/11/02
		// Added PriorityScrolling property.
		//
		/// <summary>
		/// Gets/sets whether callbacks made during a scroll operation occur synchronously or asynchronously.
		/// </summary>
		/// <value>
		/// When true, scroll notifications as a result of a scroll arrow or scroll track click will occur synchronously.
		/// </value>
		[LocalizedDescription("LD_DisplayLayoutTypeConverter_P_PriorityScrolling")]
		[LocalizedCategory("LC_Behavior")]
		public bool PriorityScrolling
		{
			get 
			{ 
				return this.priorityScrolling; 
			}
			set 
			{ 
				if ( value != this.priorityScrolling )
				{
					this.priorityScrolling = value;

					this.SyncronizePriorityScrollingToScrollRegions( );

					this.NotifyPropChange( PropertyIds.PriorityScrolling );
				}
			}
		}

		private void SyncronizePriorityScrollingToScrollRegions( )
		{
			if ( null != this.rowScrollRegions )
			{
				for ( int i = 0; i < this.rowScrollRegions.Count; i++ )
				{
					ScrollRegionBase scrollRegion = this.rowScrollRegions[i];

					if ( null != scrollRegion && scrollRegion.HasScrollBarInfo )
						scrollRegion.ScrollBarInfo.PriorityScrolling = this.PriorityScrolling;
				}
			}

			if ( null != this.colScrollRegions )
			{
				for ( int i = 0; i < this.colScrollRegions.Count; i++ )
				{
					ScrollRegionBase scrollRegion = this.colScrollRegions[i];

					if ( null != scrollRegion && scrollRegion.HasScrollBarInfo )
						scrollRegion.ScrollBarInfo.PriorityScrolling = this.PriorityScrolling;
				}
			}
		}

		#endregion // PriorityScrolling

		#region ShouldSerializePriorityScrolling

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializePriorityScrolling( )		
		{
			return this.priorityScrolling;
		}

		#endregion // ShouldSerializePriorityScrolling

		#region ResetPriorityScrolling

		/// <summary>
		/// Resets ScrollStyle to its default value (false).
		/// </summary>
		public void ResetPriorityScrolling( )
		{
			this.PriorityScrolling = false;
		}

		#endregion // ResetPriorityScrolling	

		#region FilterDropDownButtonImage

		/// <summary>
		/// Gets or sets the image that's used to draw the filter drop down buttons on the column headers.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use <see cref="FilterDropDownButtonImageActive"/> to set the image for headers with active filters. FilterDropDownButtonImage will be used for headers that don't have any filters in effect.</p>
		/// <seealso cref="FilterDropDownButtonImageActive"/>
		/// </remarks>
		[ LocalizedCategory("LC_Appearance") ]
		[ LocalizedDescription( "LDR_FilterDropDownButtonImage" ) ]
		public Image FilterDropDownButtonImage
		{
			get
			{
				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				
				return (Image)StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.FilterDropDownButtonImage, 
					this.filterDropDownButtonImage, null, this.FilterDropDownButtonImageFromResource );
				// ------------------------------------------------------------------------------------------
			}
			set
			{
				if ( value != this.filterDropDownButtonImage )
				{
					UltraGrid grid = this.Grid as UltraGrid;

					// If the image being assigned is the same as the one from the resource,
					// then just null out the sigmaImage and the image from the resource will
					// be used.
					//
					if ( null != grid && this.FilterDropDownButtonImageFromResource == value )
						this.filterDropDownButtonImage = null;
					else
						this.filterDropDownButtonImage = value;

					// Invalidate the grid.
					//
					this.DirtyGridElement( false, true );

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FilterDropDownButtonImage );
				}
			}
		}

		#endregion FilterDropDownButtonImage

		#region FilterDropDownButtonImageFromResource

		internal Image FilterDropDownButtonImageFromResource
		{
			get
			{
				if ( null == this.filterDropDownButtonImageFromResource )
				{
					// SSP 7/21/03
					// Added a central method for getting an image from the resource.
					//
					
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.filterDropDownButtonImageFromResource = this.GetImageFromResourse( "filter.bmp" );
					this.filterDropDownButtonImageFromResource = UltraGridLayout.GetImageFromResourse( "filter.bmp" );
				}

				return this.filterDropDownButtonImageFromResource;
			}
		}

		#endregion // FilterDropDownButtonImageFromResource

		#region FilterDropDownButtonImageActive

		// SSP 7/26/02 UWG1364
		// Changed the name from ActiveFilterDropDownButtonImage to FilterDropDownButtonImageActive.
		// 
		/// <summary>
		/// Gets or sets the image that's used to draw the filter drop down buttons on the column headers when there is a row filter active on the column.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use <see cref="FilterDropDownButtonImage"/> to set the image for headers with no active filters. ActiveFilterDropDownButtonImage will be used for headers that have filters in effect.</p>
		/// <seealso cref="FilterDropDownButtonImage"/>
		/// </remarks>
		[ LocalizedCategory("LC_Appearance") ]
		[ LocalizedDescription( "LDR_FilterDropDownButtonImageActive" ) ]
		public Image FilterDropDownButtonImageActive
		{
			get
			{	
				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				
				return (Image)StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.FilterDropDownButtonImageActive, 
					this.filterDropDownButtonImageActive, null, this.FilterDropDownButtonImageFromResourceActive );
				// ------------------------------------------------------------------------------------------
			}
			set
			{
				if ( value != this.filterDropDownButtonImageActive )
				{
					// If the image being assigned is the same as the one from the resource,
					// then just null out the sigmaImage and the image from the resource will
					// be used.
					//
					if ( this.FilterDropDownButtonImageFromResourceActive == value )
						this.filterDropDownButtonImageActive = null;
					else
						this.filterDropDownButtonImageActive = value;

					// Invalidate the grid.
					//
					this.DirtyGridElement( false, true );

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FilterDropDownButtonImageActive );
				}
			}
		}

		#endregion FilterDropDownButtonImageActive

		#region FilterDropDownButtonImageFromResourceActive

		// SSP 7/26/02 UWG1364
		// Changed the name from ActiveFilterDropDownButtonImageFromResource to
		// FilterDropDownButtonImageFromResourceActive
		//
		internal Image FilterDropDownButtonImageFromResourceActive
		{
			get
			{
				if ( null == this.defaultActiveFilterDropDownButtonImage )
				{
					Bitmap src  = this.FilterDropDownButtonImageFromResource as Bitmap;

					Debug.Assert( null != src, "FilterDropDownButtonImageFromResource is not a bitmap !" );

					if ( null != src )
					{
						Bitmap dest = new Bitmap( src );

						for ( int y = 0; y < src.Height; y++ )
						{
							for ( int x = 0; x < src.Width; x++ )
							{
								Color c = src.GetPixel( x, y );

								if ( ( c.R == 120 && c.G == 120 && c.B == 120 ) ||
									( c.R == 220 && c.G == 220 && c.B == 220 ) ||
									( c.R == 240 && c.G == 240 && c.B == 240 ) )
								{
									c = Color.FromArgb( 0, 0, 240 );
									dest.SetPixel( x, y, c );
								}
							}
						}

						this.defaultActiveFilterDropDownButtonImage = dest;
					}
				}

				return this.defaultActiveFilterDropDownButtonImage;
			}
		}

		#endregion // FilterDropDownButtonImageFromResourceActive

		#region FilterDropDownButtonImageFromResource

		// SSP 5/8/05 - NAS 5.2 Filter Row
		//
		internal Image FilterClearButtonImageFromResource
		{
			get
			{
				if ( null == this.filterClearButtonImageFromResource )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.filterClearButtonImageFromResource = this.GetImageFromResourse( "Images.FilterClearButton.png" );
					this.filterClearButtonImageFromResource = UltraGridLayout.GetImageFromResourse( "Images.FilterClearButton.png" );
				}

				return this.filterClearButtonImageFromResource;
			}
		}

		#endregion // FilterDropDownButtonImageFromResource

		#region FilterRowSelectorImageFromResource

		// SSP 5/19/05 - NAS 5.2 Filter Row
		//
		internal Image FilterRowSelectorImageFromResource
		{
			get
			{
				if ( null == this.filterRowSelectorImageFromResource )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.filterRowSelectorImageFromResource = this.GetImageFromResourse( "Images.Filter.png" );
					this.filterRowSelectorImageFromResource = UltraGridLayout.GetImageFromResourse( "Images.Filter.png" );
				}

				return this.filterRowSelectorImageFromResource;
			}
		}

		#endregion // FilterRowSelectorImageFromResource

		#region SummaryButtonImageFromResource

		internal Image SummaryButtonImageFromResource
		{
			get
			{
				if ( null == this.sigmaImageFromResource )
				{
					// SSP 7/21/03
					// Added a central method for getting an image from the resource.
					//
					

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.sigmaImageFromResource = this.GetImageFromResourse( "sigma.bmp" );
					this.sigmaImageFromResource = UltraGridLayout.GetImageFromResourse( "sigma.bmp" );
				}

				return this.sigmaImageFromResource;
			}
		}

		#endregion // SummaryButtonImageFromResource

		#region ShouldSerializeFilterDropDownButtonImage

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterDropDownButtonImage( )
		{
			return null != this.filterDropDownButtonImage;
		}

		#endregion // ShouldSerializeFilterDropDownButtonImage

		#region ResetFilterDropDownButtonImage

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterDropDownButtonImage( )
		{
			this.FilterDropDownButtonImage = null;
		}
		
		#endregion // ResetFilterDropDownButtonImage

		#region ShouldSerializeActiveFilterDropDownButtonImage

		// SSP 7/19/02 UWG1364
		// Added ActiveFilterDropDownButtonImage new property and associated
		// ShouldSerialize and Resets.
		//

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeFilterDropDownButtonImageActive( )
		{
			return null != this.filterDropDownButtonImageActive;
		}

		#endregion // ShouldSerializeActiveFilterDropDownButtonImage

		#region ResetFilterDropDownButtonImageActive

		// SSP 7/19/02 UWG1364
		// Added ActiveFilterDropDownButtonImage new property and associated
		// ShouldSerialize and Resets.
		//

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFilterDropDownButtonImageActive( )
		{
			this.FilterDropDownButtonImageActive = null;
		}
		
		#endregion // ResetFilterDropDownButtonImageActive
		
		#region SummaryButtonImage

		/// <summary>
		/// This image will be drawn on the column headers for summary rows icon. Summary rows icon is the icon that shows up on the column headers when summaries are enabled on that column. User can click on this icon to select summaries he/she wants performed on the associated field.
		/// </summary>
		[ LocalizedCategory("LC_Appearance") ]
		[ LocalizedDescription( "LDR_SummaryButtonImage" ) ]
		public Image SummaryButtonImage
		{
			get
			{
				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				
				return (Image)StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.SummaryButtonImage, 
					this.sigmaImage, null, this.SummaryButtonImageFromResource );
				// ------------------------------------------------------------------------------------------
			}
			set
			{
				if ( value != this.sigmaImage )
				{
					// If the image being assigned is the same as the one from the resource,
					// then just null out the si maImage and the image from the resource will
					// be used.
					//
					if ( this.SummaryButtonImageFromResource == value )
						this.sigmaImage = null;
					else
						this.sigmaImage = value;

					// Invalidate the grid.
					//
					this.DirtyGridElement( false, true );

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.SummaryButtonImage );
				}
			}
		}

		#endregion // SummaryButtonImage

		#region ShouldSerializeSummaryButtonImage

		/// <summary>
		/// Returns true if this property is not set to its default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSummaryButtonImage( )
		{
			return null != this.sigmaImage;
		}

		#endregion // ShouldSerializeSummaryButtonImage

		#region ResetSummaryButtonImage

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSummaryButtonImage( )
		{
			this.SummaryButtonImage = null;
		}
		
		#endregion // ResetSummaryButtonImage

		#region CardExpansionImage

		internal Image CardExpansionImage
		{
			get
			{
				if ( null == this.cardExpansionImage )
				{
					// SSP 7/21/03
					// Added a central method for getting an image from the resource.
					//
					

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.cardExpansionImage = this.GetImageFromResourse( "cardcompress.bmp" );
					this.cardExpansionImage = UltraGridLayout.GetImageFromResourse( "cardcompress.bmp" );
				}

				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				//return this.cardExpansionImage;
				return (Image)StyleUtils.GetCachedPropertyVal( this, 
					StyleUtils.CustomProperty.CardCollapsedIndicatorImage, null, null, this.cardExpansionImage );
				// ------------------------------------------------------------------------------------------
			}
		}

		#endregion // CardExpansionImage

		#region CardExpansionImageInverse

		internal Image CardExpansionImageInverse
		{
			get
			{
				if ( null == this.cardExpansionImageInverse )
				{
					Image image = this.CardExpansionImage;

					if ( null != image )
					{
						Bitmap bmp = new Bitmap( image );

						bmp.RotateFlip( RotateFlipType.Rotate180FlipNone );

						this.cardExpansionImageInverse = bmp;
					}

					Debug.Assert( null != this.cardExpansionImageInverse, "Unable to get the sigma bitmap." );
				}

				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				//return this.cardExpansionImageInverse;
				return (Image)StyleUtils.GetCachedPropertyVal( this, 
					StyleUtils.CustomProperty.CardExpandedIndicatorImage, null, null, this.cardExpansionImageInverse );
				// ------------------------------------------------------------------------------------------
			}
		}

		#endregion // CardExpansionImageInverse

		#region ThemesBeingUsed

		// SSP 7/15/02
		//
		internal bool ThemesBeingUsed
		{
			get
			{
				// AS 3/20/06 AppStyling
				//return null != this.Grid && this.Grid.SupportThemes && XPThemes.ThemesSupported
				return null != this.Grid && this.Grid.UseOsThemesResolved && XPThemes.ThemesSupported
					// SSP 8/16/02
					// Also check for below two properties.
					//
					&& XPThemes.IsThemeActive && XPThemes.AppControlsThemeThemed;
			}
		}

		#endregion // ThemesBeingUsed
		
		#region RefreshFilters

		// SSP 8/13/02 UWG1534
		// RefreshFilters method was added.
		//
		/// <summary>
		/// Reevaluates the filters on the rows.
		/// </summary>
		/// <remarks>
		/// <p class="body">Ultragrid does not evaluate the filter conditions on rows when data changes. This can be useful when the data has changed and you want the row filters conditions evaluated on the rows.</p>
		/// </remarks>
		public void RefreshFilters( )
		{
			this.BumpRowFiltersVersion( );

			this.BumpScrollableRowCountVersion( );

			this.DirtyGridElement( true );
		}

		#endregion //RefreshFilters

		#region RefreshSummaries

		// SSP 7/11/06 BR14108
		// Added RefreshSummaries.
		// 
		/// <summary>
		/// Reevaluates the summaries.
		/// </summary>
		public void RefreshSummaries( )
		{
			this.BumpSummariesVersion( );
			this.DirtyDataAreaElement( false, true, true );
		}

		#endregion // RefreshSummaries

		#region Cell Resolved Appearance Caching

		// SSP 12/23/02
		// Optimizations.
		// Added code to cache the resolved appearance of a cell since for a single cell
		// resolve cell appearance gets called at least twice.
		//
		internal void ResolvedCachedCellAppearance( 
			UltraGridRow row, UltraGridColumn column, 
			ref AppearanceData appData, ref AppearancePropFlags flags,
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingCell and hotTrackingRow parameters.
			// 
			bool hotTrackingCell, bool hotTrackingRow)
		{
			// SSP 9/29/06 BR16377
			// Check for row being null.
			// 
			Debug.Assert( null != row );
			if ( null == row )
				return;

			// If the flags contain non-render flags like Cursor, then don't cache.
			//
			if ( ( AppearancePropFlags.AllRender & flags ) != flags )
			{
				// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
				// Added hotTrackingCell and hotTrackingRow parameters.
				// 
				//row.ResolveCellAppearance( column, ref appData, ref flags, false, false, false );
                //
                // MBS 5/5/08 - RowEditTemplate NA2008 V2
                // Added isProxyResolution parameter
                //
				//row.ResolveCellAppearance( column, ref appData, ref flags, false, false, ForceActive.None, false, hotTrackingCell, hotTrackingRow );
                row.ResolveCellAppearance(column, ref appData, ref flags, false, false, ForceActive.None, false, hotTrackingCell, hotTrackingRow, false);

				return;
			}

			if ( this.lastResolvedCellColumn != column || this.lastResolvedCellRow != row 
				// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
				// 
				|| this.lastResolvedHotTrackingRow != hotTrackingRow || this.lastResolvedHotTrackingCell != hotTrackingCell ) 
			{
				this.lastResolvedCellFlags = AppearancePropFlags.AllRender;
				this.lastResolvedCellAppData = new AppearanceData( );
				// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
				// Added hotTrackingCell and hotTrackingRow parameters.
				// 
				//row.ResolveCellAppearance( column, ref this.lastResolvedCellAppData, ref this.lastResolvedCellFlags, false, false, false );
                //
                // MBS 5/5/08 - RowEditTemplate NA2008 V2
                // Added isProxyResolution parameter
                //
				//row.ResolveCellAppearance( column, ref this.lastResolvedCellAppData, ref this.lastResolvedCellFlags, false, false, ForceActive.None, false, hotTrackingCell, hotTrackingRow );
                row.ResolveCellAppearance(column, ref this.lastResolvedCellAppData, ref this.lastResolvedCellFlags, false, false, ForceActive.None, false, hotTrackingCell, hotTrackingRow, false);

				this.lastResolvedCellColumn = column;
				this.lastResolvedCellRow = row;

				// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
				// 
				this.lastResolvedHotTrackingRow = hotTrackingRow;
				this.lastResolvedHotTrackingCell = hotTrackingCell;
			}

			if ( AppearancePropFlags.AllRender == flags )
			{
				appData = this.lastResolvedCellAppData;
				flags &= this.lastResolvedCellFlags;
			}
			else
			{
				AppearanceData.Merge( ref appData, ref this.lastResolvedCellAppData, ref flags );
			}
		}

		internal void DirtyCachedCellAppearanceData( )
		{
			this.lastResolvedCellRow = null;
			this.lastResolvedCellColumn = null;
		}

		#endregion // Cell Resolved Appearance Caching

		#region RetainRowPropertyCategories

		// SSP 1/14/03 UWG1892
		// Added retainRowPropertyCategories parameter to Print and PrintPreview methods for
		// making WYSIWIG style printing. 
		//
		internal RowPropertyCategories RetainRowPropertyCategories
		{
			get
			{
				return this.retainRowPropertyCategories;
			}
			set
			{
				this.retainRowPropertyCategories = value;
			}
		}

		#endregion // RetainRowPropertyCategories
	
		// JJD 5/19/03 - WTB947
		// Added ForceSerialization as a workaround for a serialization
		// bug MS introdiced in V1.1 of Visual Studio
		#region ForceSerialization

		/// <summary>
		/// Internal property.
		/// </summary>
		/// <remarks>
		/// This property is used internally as a workaround for a serialization bug in Visual Studio that was introduced in version 1.1.
		/// </remarks>
		[ Browsable(false) ]
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Visible ) ]
		[ DefaultValue(false) ]
		public bool ForceSerialization
		{
			get	
			{ 
				// JJD 5/19/03 - WTB947
				// Return true if the toolbars manager is inherited
				// to prevent MS's v1.1 serialization bug from surfacing
				if ( this.Grid != null &&
					 this.Grid.DesignMode &&
					 this.IsDisplayLayout )
				{
					InheritanceAttribute inherit = Utilities.GetInheritanceAttribute( this.Grid );

					if ( inherit != null &&
						 inherit.InheritanceLevel == InheritanceLevel.Inherited )
						return !this.ShouldSerialize();
				}

				return false; 			
			}

			set { } // Don't do anything on a set
		}

		#endregion ForceSerialization

		// SSP 2/28/03 - Row Layout Functionality
		// 
		#region RowLayoutVersion

		internal int RowLayoutVersion
		{
			get
			{
				return this.rowLayoutVersion;
			}
		}

		#endregion // RowLayoutVersion

		#region BumpRowLayoutVersion

		internal void BumpRowLayoutVersion( )
		{
			this.DirtyGridElement( true );
			if ( null != this.colScrollRegions )
				this.colScrollRegions.DirtyMetrics( );

			this.rowLayoutVersion++;

			// SSP 8/12/03 - Optimizations - Grand Verify Version Number
			//
			this.BumpGrandVerifyVersion( );
		}

		#endregion // BumpRowLayoutVersion

		// SSP 5/7/03 - Excel Exporting Functionality
		// Added IsExportLayout method.
		//
		#region IsExportLayout

		/// <summary>
		/// Indicates whether this layout is an export layout.
		/// </summary>
		// SSP 9/11/03 UWG2423
		// Marked Browsable false.
		//
		[ Browsable(false) ]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)] // MRS NAS v8.3 - Unit Testing
		public bool IsExportLayout
		{
			get
			{
				return this.isExportLayout;				
			}
		}

		#endregion // IsExportLayout

		#region InternalSetIsExportLayout
		
		internal void InternalSetIsExportLayout( bool isExportLayout )
		{
			this.isExportLayout = isExportLayout;
		}

		#endregion // InternalSetIsExportLayout

		// SSP 5/12/03 - Optimizations
		// Added a way to just draw the text without having to embedd an embeddable ui element in
		// cells to speed up rendering.
		//
		#region CellGoingIntoEditMode
		
		internal UltraGridCell CellGoingIntoEditMode
		{
			get
			{
				return this.cellGoingIntoEditMode;
			}
			set
			{
				this.cellGoingIntoEditMode = value;
			}
		}

		#endregion // CellGoingIntoEditMode
			
		// SSP 5/19/03 - Fixed headers
		//
		#region Fixed Headers

		#region UseFixedHeaders

		/// <summary>
		/// Specifies whether the fixed-headers feature is enabled. Fixed-headers feature lets you fix (freeze) columns so they do not scroll when the grid is scrolled horizontally.
		/// </summary>
		/// <remarks>
		/// <p class="body">Fixed headers functionality lets you fix columns so they always remain in view even when the grid is scrolled horizontally.</p>
		/// <p class="body">After enabling the fixed headers functionality, you can fix a header by setting the <see cref="HeaderBase.Fixed"/> property of the header to true. If no headers are fixed, setting this property to true will cause the row selectors to be fixed. In other words the row selectors will remian in view when the grid is scrolled horizontally.</p>
		/// <p><seealso cref="HeaderBase.Fixed"/> <seealso cref="UltraGridOverride.FixedHeaderIndicator"/> <seealso cref="HeaderBase.FixedHeaderIndicator"/></p>
		/// </remarks>
		[ LocalizedCategory("LC_Behavior") ]
		[ LocalizedDescription( "LDR_UseFixedHeaders" ) ]
		public bool UseFixedHeaders
		{
			get
			{
				return this.useFixedHeaders;				
			}
			set
			{
				if ( value != this.useFixedHeaders )
				{
					this.useFixedHeaders = value;
					
					// JAS v5.2 Wrapped Header Text 4/25/05
					this.BumpColumnHeaderHeightVersionNumber();

					this.DirtyGridElement( true );
					this.ColScrollRegions.DirtyMetrics( );
					this.SynchronizedColListsDirty = true;
					this.BumpCellChildElementsCacheVersion( );
					this.BumpFixedHeadersVerifyVersion( );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.UseFixedHeaders );
				}
			}
		}

		#endregion // UseFixedHeaders

		#region ShouldSerializeUseFixedHeaders

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
		/// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeUseFixedHeaders( )
		{
			return this.useFixedHeaders;
		}

		#endregion // ShouldSerializeUseFixedHeaders

		#region ResetUseFixedHeaders
		
		/// <summary>
		/// Resets the UseFixedHeaders property to its default value of false.
		/// </summary>
		public void ResetUseFixedHeaders( )
		{
			this.UseFixedHeaders = false;
		}

		#endregion // ResetUseFixedHeaders

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//        #region GetIconFromResourseAsBitmap

		//        // SSP 5/19/05 - NAS 5.2 Filter Row
		//        // Added GetIconFromResourseAsBitmap method.
		//        //
		//#if DEBUG
		//        /// <summary> 
		//        /// Gets the icon embedded in the assembly with the passed in resource name and
		//        /// converts it to a bitmp and returns it.
		//        /// </summary>
		//        /// <param name="resourceName"></param>
		//        /// <returns></returns>
		//#endif
		//        internal Image GetIconFromResourseAsBitmap( string resourceName )
		//        {
		//            // load the icon from the manifest resources. 
		//            //
		//            // SSP 6/14/02 UWG1238
		//            // this.GetType returns the derived class' type and Module.Assembly will be
		//            // the assembly that the derived class if from and the resources are in
		//            // our assembly. So below statemenet won't work if we use GetType( ).
		//            // Instead use the typeof operator with our class name as the operand.
		//            //
		//            //System.IO.Stream stream = this.GetType().Module.Assembly.GetManifestResourceStream( this.GetType(), "filter.bmp" );
		//            System.IO.Stream stream = 
		//                typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream( typeof( UltraGrid ), resourceName );

		//            Bitmap bmp = null;

		//            if ( null != stream )
		//            {
		//                Icon icon = new Icon( stream );

		//                bmp = icon.ToBitmap( );

		//                icon.Dispose( );
		//                stream.Close( );				
		//            }

		//            Debug.Assert( null != bmp, "No " + resourceName + " resource image in the assembly." );

		//            return bmp;
		//        }

		//        #endregion // GetIconFromResourseAsBitmap

		#endregion Not Used

		#region GetImageFromResourse

		// SSP 7/21/03
		// Added a central method for getting an image from the resource.
		//
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal Image GetImageFromResourse( string resourceName )
		internal static Image GetImageFromResourse( string resourceName )
		{
			// load the icon from the manifest resources. 
			//
			// SSP 6/14/02 UWG1238
			// this.GetType returns the derived class' type and Module.Assembly will be
			// the assembly that the derived class if from and the resources are in
			// our assembly. So below statemenet won't work if we use GetType( ).
			// Instead use the typeof operator with our class name as the operand.
			//
			//System.IO.Stream stream = this.GetType().Module.Assembly.GetManifestResourceStream( this.GetType(), "filter.bmp" );
			System.IO.Stream stream = 
				typeof( UltraGrid ).Module.Assembly.GetManifestResourceStream( typeof( UltraGrid ), resourceName );
					
			Image image = null;

			if ( null != stream )
			{
				image = Bitmap.FromStream( stream );
			}

			//Debug.Assert( null != image, "No " + resourceName + " resource image in the assembly." );

			return image;
		}

		#endregion // GetImageFromResourse

		#region FixedHeaderOnImage

		/// <summary>
		/// Gets or sets the image that's used to draw the fixed header indicator when the header is fixed.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use <see cref="FixedHeaderOnImage"/> to change the image in the fixed header indicator of the headers that are fixed.</p>
		/// <seealso cref="FixedHeaderOffImage"/>
		/// </remarks>
		[ LocalizedCategory("LC_Appearance") ]
		[ LocalizedDescription( "LDR_FixedHeaderOnImage" ) ]
		public Image FixedHeaderOnImage
		{
			get
			{
				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				
				return (Image)StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.FixedHeaderOnImage, 
					this.fixedHeaderOnImage, null, this.FixedHeaderOnImageFromResource );
				// ------------------------------------------------------------------------------------------
			}
			set
			{
				if ( value != this.fixedHeaderOnImage )
				{
					UltraGrid grid = this.Grid as UltraGrid;

					// If the image being assigned is the same as the one from the resource,
					// then just null out the sigmaImage and the image from the resource will
					// be used.
					//
					if ( null != grid && this.fixedHeaderOnImageFromResource == value )
						this.fixedHeaderOnImage = null;
					else
						this.fixedHeaderOnImage = value;

					// Invalidate the grid.
					//
					this.DirtyGridElement( false, true );

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FixedHeaderOnImage );
				}
			}
		}

		#endregion // FixedHeaderOnImage

		#region ShouldSerializeFixedHeaderOnImage

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedHeaderOnImage( )
		{
			return null != this.fixedHeaderOnImage && this.fixedHeaderOnImage != this.fixedHeaderOnImageFromResource;
		}

		#endregion // ShouldSerializeFixedHeaderOnImage

		#region ResetFixedHeaderOnImage

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedHeaderOnImage( )
		{
			this.FixedHeaderOnImage = null;
		}

		#endregion // ResetFixedHeaderOnImage

		#region FixedHeaderOffImageFromResource

		private Image FixedHeaderOffImageFromResource
		{
			get
			{
				if ( null == this.fixedHeaderOffImageFromResource )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.fixedHeaderOffImageFromResource = this.GetImageFromResourse( "FixedHeaderOff.bmp" );
					this.fixedHeaderOffImageFromResource = UltraGridLayout.GetImageFromResourse( "FixedHeaderOff.bmp" );
				}

				return this.fixedHeaderOffImageFromResource;
			}
		}

		#endregion // FixedHeaderOffImageFromResource

		#region FixedHeaderOnImageFromResource

		private Image FixedHeaderOnImageFromResource
		{
			get
			{
				if ( null == this.fixedHeaderOnImageFromResource )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.fixedHeaderOnImageFromResource = this.GetImageFromResourse( "FixedHeaderOn.bmp" );
					this.fixedHeaderOnImageFromResource = UltraGridLayout.GetImageFromResourse( "FixedHeaderOn.bmp" );
				}

				return this.fixedHeaderOnImageFromResource;
			}
		}

		#endregion // FixedHeaderOnImageFromResource

		#region FixedHeaderOffImage

		/// <summary>
		/// Gets or sets the image that's used to draw the fixed header indicator when the header is not fixed.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use <see cref="FixedHeaderOffImage"/> to change the image in the fixed header indicator of the headers that are not fixed.</p>
		/// <seealso cref="FixedHeaderOnImage"/>
		/// </remarks>
		[ LocalizedCategory("LC_Appearance") ]
		[ LocalizedDescription( "LDR_FixedHeaderOffImage" ) ]
		public Image FixedHeaderOffImage
		{
			get
			{
				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				
				return (Image)StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.FixedHeaderOffImage, 
					this.fixedHeaderOffImage, null, this.FixedHeaderOffImageFromResource );
				// ------------------------------------------------------------------------------------------
			}
			set
			{
				if ( value != this.fixedHeaderOffImage )
				{
					UltraGrid grid = this.Grid as UltraGrid;

					// If the image being assigned is the same as the one from the resource,
					// then just null out the sigmaImage and the image from the resource will
					// be used.
					//
					if ( null != grid && this.fixedHeaderOffImageFromResource == value )
						this.fixedHeaderOffImage = null;
					else
						this.fixedHeaderOffImage = value;

					// Invalidate the grid.
					//
					this.DirtyGridElement( false, true );

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FixedHeaderOffImage );
				}
			}
		}

		#endregion // FixedHeaderOffImage

		#region ShouldSerializeFixedHeaderOffImage

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedHeaderOffImage( )
		{
			return null != this.fixedHeaderOffImage && this.fixedHeaderOffImage != this.fixedHeaderOffImageFromResource;
		}

		#endregion // ShouldSerializeFixedHeaderOffImage

		#region ResetFixedHeaderOffImage

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedHeaderOffImage( )
		{
			this.FixedHeaderOffImage = null;
		}

		#endregion // ResetFixedHeaderOffImage

		#region FixedCellSeparatorColorDefault
		
		internal Color FixedCellSeparatorColorDefault
		{
			get
			{
				if ( this.HasOverride && this.overrideObj.ShouldSerializeFixedCellSeparatorColor( ) )
					return this.overrideObj.FixedCellSeparatorColor;

				return Color.Gray;
			}
		}

		#endregion // FixedCellSeparatorColorDefault

		#region BumpFixedHeadersVerifyVersion

		internal void BumpFixedHeadersVerifyVersion( )
		{
			if ( null != this.sortedBands )
			{
				for ( int i = 0; i < this.sortedBands.Count; i++ )
				{
					this.sortedBands[i].BumpFixedHeadersVerifyVersion( true );
				}
			}
		}

		#endregion // BumpFixedHeadersVerifyVersion

		#region BumpFixedRowsCollectionsVersion

		// SSP 3/30/05 - NAS 5.2 Fixed Rows
		//
		internal void BumpFixedRowsCollectionsVersion( )
		{
			this.fixedRowsCollectionsVersion++;
			this.BumpGrandVerifyVersion( );
		}

		internal int FixedRowsCollectionsVersion
		{
			get
			{
				return this.fixedRowsCollectionsVersion;
			}
		}

		#endregion // BumpFixedRowsCollectionsVersion

		#endregion // Fixed Headers

		// SSP 3/29/05 - NAS 5.2 Fixed Rows
		//
		#region Fixed Rows

		#region FixedRowOnImage

		/// <summary>
		/// Gets or sets the image that's used to draw the fixed row indicator when the row is fixed.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use <see cref="FixedRowOnImage"/> to change the image in the fixed row indicator of the rows that are fixed.</p>
		/// <seealso cref="FixedRowOffImage"/>
		/// </remarks>
		[ LocalizedCategory("LC_Appearance") ]
		[ LocalizedDescription( "LDR_FixedRowOnImage" ) ]
		public Image FixedRowOnImage
		{
			get
			{
				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				
				return (Image)StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.FixedRowOnImage, 
					this.fixedRowOnImage, null, this.FixedHeaderOnImageFromResource );
				// ------------------------------------------------------------------------------------------
			}
			set
			{
				if ( value != this.fixedRowOnImage )
				{
					UltraGrid grid = this.Grid as UltraGrid;

					// If the image being assigned is the same as the one from the resource,
					// then just null out the sigmaImage and the image from the resource will
					// be used.
					//
					if ( null != grid && this.fixedHeaderOnImageFromResource == value )
						this.fixedRowOnImage = null;
					else
						this.fixedRowOnImage = value;

					// Invalidate the grid.
					//
					this.DirtyGridElement( false, true );

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FixedRowOnImage );
				}
			}
		}

		#endregion // FixedRowOnImage

		#region ShouldSerializeFixedRowOnImage

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedRowOnImage( )
		{
			return null != this.fixedRowOnImage && this.fixedRowOnImage != this.fixedHeaderOnImageFromResource;
		}

		#endregion // ShouldSerializeFixedRowOnImage

		#region ResetFixedRowOnImage

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedRowOnImage( )
		{
			this.FixedRowOnImage = null;
		}

		#endregion // ResetFixedRowOnImage

		#region FixedRowOffImage

		/// <summary>
		/// Gets or sets the image that's used to draw the fixed row indicator when the row is not fixed.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use <see cref="FixedRowOffImage"/> to change the image in the fixed row indicator of the rows that are not fixed.</p>
		/// <seealso cref="FixedRowOnImage"/>
		/// </remarks>
		[ LocalizedCategory("LC_Appearance") ]
		[ LocalizedDescription( "LDR_FixedRowOffImage" ) ]
		public Image FixedRowOffImage
		{
			get
			{
				// SSP 4/16/06 - App Styling
				// 
				// ------------------------------------------------------------------------------------------
				
				return (Image)StyleUtils.GetCachedPropertyVal( this, StyleUtils.CustomProperty.FixedRowOffImage, 
					this.fixedRowOffImage, null, this.FixedHeaderOffImageFromResource );
				// ------------------------------------------------------------------------------------------
			}
			set
			{
				if ( value != this.fixedRowOffImage )
				{
					UltraGrid grid = this.Grid as UltraGrid;

					// If the image being assigned is the same as the one from the resource,
					// then just null out the sigmaImage and the image from the resource will
					// be used.
					//
					if ( null != grid && this.fixedHeaderOffImageFromResource == value )
						this.fixedRowOffImage = null;
					else
						this.fixedRowOffImage = value;

					// Invalidate the grid.
					//
					this.DirtyGridElement( false, true );

					// SSP 3/20/06 - App Styling
					// 
					StyleUtils.DirtyCachedPropertyVals( this );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.FixedRowOffImage );
				}
			}
		}

		#endregion // FixedRowOffImage

		#region ShouldSerializeFixedRowOffImage

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		internal protected bool ShouldSerializeFixedRowOffImage( )
		{
			return null != this.fixedRowOffImage && this.fixedRowOffImage != this.fixedHeaderOffImageFromResource;
		}

		#endregion // ShouldSerializeFixedRowOffImage

		#region ResetFixedRowOffImage

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetFixedRowOffImage( )
		{
			this.FixedRowOffImage = null;
		}

		#endregion // ResetFixedRowOffImage

		#endregion // Fixed Rows

		// SSP 5/22/03
		// Added a property to allow the user to control the small change of the column scroll bar.
		//

		#region ColumnScrollbarSmallChange

		/// <summary>
		/// Specifies the small change for the horizontal scrollbar in a column scroll region. Default is 30.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// You can use ColumnScrollbarSmallChange property to change the small change of the scrollbar in the column scroll region. By default it is 30. Small change of the scrollbar specifies how much the it will scroll when the arrow is clicked.
		/// </p>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridLayout_P_ColumnScrollbarSmallChange")]
        public int ColumnScrollbarSmallChange
		{
			get
			{
				return this.columnScrollbarSmallChange;
			}
			set
			{
				if ( this.columnScrollbarSmallChange != value )
				{
					if ( value <= 0 )
						throw new ArgumentOutOfRangeException( "ColumnScrollbarSmallChange", value, Infragistics.Shared.SR.GetString( "LER_ArguementOutOfRangeException12" ) );

					this.columnScrollbarSmallChange = value;

					// Dirty the metrics so we reset the horizontal scrollbars to the new small change.
					//
					this.ColScrollRegions.DirtyMetrics( );

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ColumnScrollbarSmallChange );
				}
			}
		}

		#endregion // ColumnScrollbarSmallChange

		#region ShouldSerializeColumnScrollbarSmallChange

		/// <summary>
		/// Returns true if the property has been set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeColumnScrollbarSmallChange( )
		{
			return ColScrollRegion.HORIZONTAL_SCROLLBAR_SMALL_CHANGE != this.columnScrollbarSmallChange;
		}

		#endregion // ShouldSerializeColumnScrollbarSmallChange

		#region ResetColumnScrollbarSmallChange

		/// <summary>
		/// Resets the ColumnScrollbarSmallChange property to its default vlaue of 30.
		/// </summary>
		public void ResetColumnScrollbarSmallChange( )
		{
            // MRS NAS v8.2 - Unit Testing
			//this.columnScrollbarSmallChange = ColScrollRegion.HORIZONTAL_SCROLLBAR_SMALL_CHANGE;
            this.ColumnScrollbarSmallChange = ColScrollRegion.HORIZONTAL_SCROLLBAR_SMALL_CHANGE;
		}

		#endregion // ResetColumnScrollbarSmallChange

		#region ExitEditModeHelper

		// SSP 10/23/03 UWG2334
		// Added ExitEditModeHelper method.
		//		
		internal bool ExitEditModeHelper( )
		{
			UltraGridCell activeCell = this.ActiveCell;

			if ( null != activeCell )
				activeCell.ExitEditMode( );

			// Reget the active cell in case the user put some other cell in
			// edit mode through one of the event handlers.
			//
			activeCell = this.ActiveCell;

			return null == activeCell || ! activeCell.IsInEditMode;
		}

		#endregion // ExitEditModeHelper

		#region BumpGrandVerifyVersion

		// SSP 8/12/03 - Optimizations - Grand Verify Version Number
		//
		internal void BumpGrandVerifyVersion( )
		{
			this.BumpGrandVerifyVersion( true );
		}

		// SSP 12/2/05 BR07897
		// Added an overload of BumpGrandVerifyVersion that takes in bumpScrollToFillVersion.
		// 
		internal void BumpGrandVerifyVersion( bool bumpScrollToFillVersion )
		{
			this.grandVerifyVersion++;

			// SSP 12/2/05 BR07897
			// Added an overload of BumpGrandVerifyVersion that takes in bumpScrollToFillVersion.
			// 
			if ( bumpScrollToFillVersion )
				this.scrollToFillVersion++;

			// SSP 6/13/05
			// Added RowChildElementsCacheVersion.
			// 
			this.BumpRowChildElementsCacheVersion( );
		}

		#endregion // BumpGrandVerifyVersion

		#region GrandVerifyVersion

		internal int GrandVerifyVersion
		{
			get
			{
				return this.grandVerifyVersion;
			}
		}

		#endregion // GrandVerifyVersion

		#region ScrollToFillVersion

		// SSP 12/2/05 BR07897
		// 
		internal int ScrollToFillVersion
		{
			get
			{
				return this.scrollToFillVersion;
			}
		}

		#endregion // ScrollToFillVersion
		
		#region BumpRowChildElementsCacheVersion

		// SSP 6/13/05
		// Added RowChildElementsCacheVersion.
		// 
		internal void BumpRowChildElementsCacheVersion( )
		{
			this.rowChildElementsCacheVersion++;
		}

		#endregion // BumpRowChildElementsCacheVersion

		#region RowChildElementsCacheVersion

		// SSP 6/13/05
		// Added RowChildElementsCacheVersion.
		// 
		internal int RowChildElementsCacheVersion
		{
			get
			{
				return this.rowChildElementsCacheVersion;
			}
		}

		#endregion // RowChildElementsCacheVersion

		#region BumpVerifyEditorVersionNumber

		// SSP 4/22/05 - NAS 5.2 Filter Row
		//
		internal void BumpVerifyEditorVersionNumber( UltraGridBand changedBand )
		{
			if ( null != this.sortedBands )
			{
				foreach ( UltraGridBand band in this.sortedBands )
				{
					if ( null != band && band.HasColumnsBeenAllocated )
					{
						foreach ( UltraGridColumn col in band.Columns )
						{
							if ( null == changedBand || changedBand == band )
								col.DirtyDefaultEditor( );
						}
					}
				}
			}
		}

		#endregion // BumpVerifyEditorVersionNumber

		// MRS 2/23/04 - Added Preset support
		#region Implementation of ISupportPresets
		/// <summary>
		/// Returns a list of properties which can be used in a Preset
		/// </summary>
		/// <param name="presetType">Determines which type(s) of properties are returned</param>
		/// <returns>An array of strings indicating property names</returns>
		string[] ISupportPresets.GetPresetProperties(Infragistics.Win.PresetType presetType)
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList properties = new ArrayList();
			List<string> properties = new List<string>();

			properties.Add("AddNewBox");
			properties.Add("GroupByBox");				
			properties.Add("Override");			
			properties.Add("ScrollBarLook");

			if ((presetType & Infragistics.Win.PresetType.Appearance) == Infragistics.Win.PresetType.Appearance)
			{
				properties.Add("Appearance");				
				properties.Add("BorderStyle");
				properties.Add("BorderStyleCaption");
				properties.Add("CaptionAppearance");

				// JAS v5.2 4/8/05
				properties.Add("CaptionVisible");

				properties.Add("FilterDropDownButtonImage");
				properties.Add("FilterDropDownButtonImageActive");
				properties.Add("FixedHeaderOffImage");
				properties.Add("FixedHeaderOnImage");
				// SSP 5/19/03 - Fixed headers
				//
				properties.Add("FixedRowOnImage");
				properties.Add("FixedRowOffImage");

				properties.Add("InterBandSpacing");
				properties.Add("RowConnectorColor");
				properties.Add("RowConnectorStyle");
				// MRS 5/26/04 - Moved to Behavior
				//properties.Add("Scrollbars");
				properties.Add("SplitterBarHorizontalAppearance");
				properties.Add("SplitterBarVerticalAppearance");
				properties.Add("SplitterBarWidth");
				properties.Add("SummaryButtonImage");
				// MRS 5/26/04 - Moved to Behavior
				//properties.Add("ViewStyle");
				//properties.Add("ViewStyleBand");

                // CDS 7/24/09 NAS 9.2 Cell ActiveAppearance Related properties.
                properties.Add("DefaultSelectedBackColor");
                properties.Add("DefaultSelectedForeColor");				
			}
			if ((presetType & Infragistics.Win.PresetType.Behavior) == Infragistics.Win.PresetType.Behavior)
			{
				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// Obsoleted AutoFitColumns and added AutoFitStyle property.
				//
				//properties.Add("AutoFitColumns");
				properties.Add("AutoFitStyle");

				// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
				// 
				properties.Add("ClipboardCellDelimiter");
				properties.Add("ClipboardCellSeparator");
				properties.Add("ClipboardRowSeparator");

				// SSP 8/28/06 - NAS 6.3
				// Added CellHottrackInvalidationStyle property on the layout.
				// 
				properties.Add("CellHottrackInvalidationStyle");

				properties.Add("MaxColScrollRegions");
				properties.Add("MaxRowScrollRegions");
				properties.Add("ScrollBounds");
				properties.Add("ScrollStyle");
				properties.Add("TabNavigation");
				properties.Add("UseFixedHeaders");	
				properties.Add("ColumnScrollbarSmallChange");
				properties.Add("MaxBandDepth");				

				// SSP 5/3/05 - NewColumnLoadStyle
				// Added NewColumnLoadStyle and NewBandLoadStyle.
				//
				properties.Add("NewBandLoadStyle");
				properties.Add("NewColumnLoadStyle");				

				// SSP 4/27/04
				// Added LoadStyle property.
				//
				properties.Add("LoadStyle");

				// MRS 5/26/04 - Moved from Appearance
				properties.Add("Scrollbars");
				properties.Add("ViewStyle");
				properties.Add("ViewStyleBand");
			}
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (string[])properties.ToArray(typeof(string));
			return properties.ToArray();
		}

		/// <summary>
		/// Returns the TypeName of the Preset target
		/// </summary>
		/// <returns>Returns "UltraGridLayout"</returns>
		string ISupportPresets.GetPresetTargetTypeName()
		{
			return "UltraGridLayout";
		}
		#endregion Implementation of ISupportPresets

		// SSP 4/13/04 - Virtual Binding
		// Added LoadStyle property to UltraGridLayout.
		//
		#region LoadStyle

		/// <summary>
		/// Specifies how rows are loaded. Default value is <b>PreloadRows</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">Set <b>LoadStyle</b> to <b>LoadOnDemand</b> 
		/// to load rows as needed instead of loading all rows at once. <b>NOTE:</b> Some operations, like sorting rows, 
		/// will cause the all rows to be loaded regardless of the load style setting because they 
		/// require access to all rows. For example if you have a summary that sums the values of a
		/// column then when the summary is calculated all the rows will be loaded.</p>
		/// <p class="body">
		/// <b>Note:</b> Due to the nature of <b>UltraCombo</b> and <b>UltraDropDown</b>, load-on-demand is not
		/// supported on these controls.
		/// </p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridLayout_P_LoadStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		public LoadStyle LoadStyle
		{
			get
			{
				return this.loadStyle;
			}
			set
			{
				if ( this.loadStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( LoadStyle ), value ) )
						throw new InvalidEnumArgumentException( "LoadStyle", (int)value, typeof( LoadStyle ) );

					this.loadStyle = value;

					this.NotifyPropChange( PropertyIds.LoadStyle );
				}
			}
		}

		#endregion // LoadStyle

		#region ShouldSerializeLoadStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeLoadStyle( )
		{
			return LoadStyle.PreloadRows != this.LoadStyle;
		}

		#endregion // ShouldSerializeLoadStyle

		#region ResetLoadStyle

		/// <summary>
		/// Resets the <see cref="UltraGridLayout.LoadStyle"/> to its default value of <b>PreloadRows</b>.
		/// </summary>
		public void ResetLoadStyle( )
		{
			this.LoadStyle = LoadStyle.PreloadRows;
		}

		#endregion // ResetLoadStyle
		
		// SSP 5/25/04
		// Added AlwaysSerializeBandsAndColumns property.
		// We need to serialize the bands and columns when the data structure has
		// been extracted out of a data source.
		//
		#region AlwaysSerializeBandsAndColumns
		
		internal bool AlwaysSerializeBandsAndColumns
		{
			get
			{
				return null != this.Grid && null == this.Grid.DataSource && null != this.sortedBands 
					&& ( this.sortedBands.Count > 1 
						|| this.sortedBands.Count > 0 && null != this.sortedBands[0].Columns 
							&& this.sortedBands[0].Columns.Count > 0 );
			}
		}

		#endregion // AlwaysSerializeBandsAndColumns

		// MRS 5/21/04 - UWG2915
		#region GetCachedGraphics
		/// <summary>
		/// Called to get a graphics object suitable for doing metrics calculations only.
		/// </summary>
		/// <returns>A graphics object suitable for doing metrics calculations only. This graphics object shouldn't be rendered on.</returns>
		/// <remarks>
		/// <p>Do NOT call the <b>Dispose</b> method on the graphics object returned from this method.</p>
		/// <p>Instead, each call to this method should be paired with a call to <see cref="ReleaseCachedGraphics"/>.</p>
		/// <p>During graphics caching calls to <see cref="GetCachedGraphics"/> will return a single cached graphics object and calls to <see cref="ReleaseCachedGraphics"/> will be ignored.</p>
		/// </remarks>
		public Graphics GetCachedGraphics()
		{			
			if ( !this.IsPrintLayout )
				return DrawUtility.GetCachedGraphics(this.Grid);
			else
				return ((UltraGrid)this.Grid).PrintManager.PrinterMetricsGraphics;
	
		}
		#endregion    //GetCachedGraphics	

		#region ReleaseCachedGraphics
		/// <summary>
		/// Called to release a graphics object that was returned from a prior call to <see cref="GetCachedGraphics"/>.
		/// </summary>
		/// <param name="gr">The graphics object to release.</param>
		/// <remarks>
		/// <p>Do NOT call the <b>Dispose</b> method on the graphics object returned from <see cref="GetCachedGraphics"/>. Use this method instead.</p>
		/// <p>During graphics caching calls to <see cref="GetCachedGraphics"/> will return a single cached graphics object and calls to <see cref="ReleaseCachedGraphics"/> will be ignored.</p>
		/// </remarks>
		public void ReleaseCachedGraphics( Graphics gr )
		{			
			if ( !this.IsPrintLayout )
				DrawUtility.ReleaseCachedGraphics( gr );
		}
		#endregion    //ReleaseCachedGraphics	

		// SSP 6/29/04 - UltraCalc 
		//
		#region UltraCalc Related Changes

		#region CalcReference

		internal GridReference CalcReference
		{
			get
			{
				if ( null == this.calcReference )
					this.calcReference = new GridReference( this );

				return this.calcReference;
			}
		}

		#endregion // CalcReference

		#region HasCalcReference
		
		internal bool HasCalcReference
		{
			get
			{
				return null != this.calcReference;
			}
		}

		#endregion // HasCalcReference

		#region RecreateCalcReference

		// SSP 11/15/04 - UWC159
		// Added RecreateReference method on the IUltraCalcParticipant so we can recreate the
		// calc reference when the name of the control changes at design time.
		//
		internal void RecreateCalcReference( )
		{
			bool isSuspended = this.CalcManagerNotificationsSuspended;
			if ( ! isSuspended )
				this.Grid.UnRegisterFromCalcManager( );

			this.calcReference = null;

			if ( ! isSuspended )
				this.ResumeCalcManagerNotifications( );
		}

		#endregion // RecreateCalcReference

		#region CalcManager
		
		internal Infragistics.Win.CalcEngine.IUltraCalcManager CalcManager
		{
			get
			{
				return null != this.grid ? this.grid.CalcManager : null;
			}
		}

		#endregion // CalcManager

		#region SuspendCalcManagerNotifications

		internal void SuspendCalcManagerNotifications( )
		{
			bool previouslySuspended = this.CalcManagerNotificationsSuspended;

			this.calcManagerNotificationsSuspended_Counter++;

			if ( ! previouslySuspended && null != this.Grid )
					this.Grid.UnRegisterFromCalcManager( );
		}

		#endregion // SuspendCalcManagerNotifications

		#region ResumeCalcManagerNotifications

		internal void ResumeCalcManagerNotifications( )
		{
			if ( this.CalcManagerNotificationsSuspended )
			{
				this.calcManagerNotificationsSuspended_Counter--;
				
				if ( ! this.CalcManagerNotificationsSuspended && null != this.Grid )
					this.Grid.RegisterWithCalcManager( );
			}
		}

		#endregion // ResumeCalcManagerNotifications

		#region CalcManagerNotificationsSuspended

		internal bool CalcManagerNotificationsSuspended
		{
			get
			{
				return this.calcManagerNotificationsSuspended_Counter > 0
					// SSP 1/17/05 BR01753
					// Only enable calculations in display layout. Print and export layouts will
					// copy over the calculated values from the display layout.
					//
					|| ! this.IsDisplayLayout;
			}
		}

		#endregion // CalcManagerNotificationsSuspended

		#region RehookWithCalcManager

		// SSP 6/29/04 - UltraCalc 
		//
		internal void RehookWithCalcManager( Infragistics.Win.CalcEngine.IUltraCalcManager oldCalcManager )
		{            
			BandsCollection bands = this.sortedBands;
			if ( null == bands )
				return;

			foreach ( UltraGridBand band in bands )
			{
				if ( null == band )
					continue;

				foreach ( UltraGridColumn column in band.Columns )
				{
					if ( null != column )
						column.FormulaHolder.ReaddReferenceToCalcNetwork( oldCalcManager );
				}

				foreach ( SummarySettings summary in band.Summaries )
				{
					if ( null != summary )
						summary.FormulaHolder.ReaddReferenceToCalcNetwork( oldCalcManager );
				}
			}
		}

		#endregion // RehookWithCalcManager

		#region AddRecalcDeferredFormulasToRecalcChain

		// SSP 1/17/05 BR01753
		// Print and export layout won't calculate formulas but rather they will copy over 
		// the calculated values from the display layout. We need to ensure that all the 
		// columns are calculated before we go ahead with printing.
		// Added AddRecalcDeferredFormulasToRecalcChain method.
		//

		private void AddRecalcDeferredFormulasToRecalcChain_Helper( FormulaRefBase formulaRef )
		{
			if ( formulaRef.RecalcDeferredBase 
				&& null != this.CalcManager 
				&& formulaRef.FormulaHolder.HasActiveFormula 
				&& formulaRef.HasFormula && ! formulaRef.Formula.HasSyntaxError )
			{
				this.CalcManager.PerformAction( UltraCalcAction.AddReferenceToRecalcChain, formulaRef );
			}
		}

		internal void AddRecalcDeferredFormulasToRecalcChain( )
		{
			BandsCollection bands = this.sortedBands;
			if ( null == bands || null == this.CalcManager )
				return;

			foreach ( UltraGridBand band in bands )
			{
				if ( null == band )
					continue;

				foreach ( UltraGridColumn column in band.Columns )
				{
					if ( null != column.CalcReference )
						this.AddRecalcDeferredFormulasToRecalcChain_Helper( column.CalcReference );
				}

				foreach ( SummarySettings summary in band.Summaries )
				{
					if ( null != summary.CalcReference )
						this.AddRecalcDeferredFormulasToRecalcChain_Helper( summary.CalcReference );
				}
			}
		}

		#endregion // AddRecalcDeferredFormulasToRecalcChain

		#region GetDesignerReferences

		internal static ReferenceNode GetReferenceNode( UltraGridBand band )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList childNodes = new ArrayList( );
			List<ReferenceNode> childNodes = new List<ReferenceNode>();

			foreach ( UltraGridColumn column in band.Columns )
			{
				if ( ! column.IsChaptered && null != column.CalcReference )
					childNodes.Add( new ReferenceNode( column.CalcReference, null, true ) );
			}

			// SSP 8/31/04 UWG3672
			// Add summaries as well.
			//
			foreach ( SummarySettings summary in band.Summaries )
			{
				if ( null != summary.Key && summary.Key.Length > 0 && null != summary.CalcReference )
					childNodes.Add( new ReferenceNode( summary.CalcReference, null, true ) );
			}
            
			foreach ( UltraGridBand bandIterator in band.Layout.SortedBands )
			{
				if ( bandIterator.ParentBand == band && null != bandIterator.Key && bandIterator.Key.Length > 0 )
				{
					ReferenceNode childReference = GetReferenceNode( bandIterator );
					if ( null != childReference )
						childNodes.Add( childReference );
				}
			}

			// If the band has no columns and no child bands, return null.
			//
			return 
				childNodes.Count > 0 
				? new ReferenceNode( 
								band.CalcReference,
								// MD 8/10/07 - 7.3 Performance
								// Use generics
								//(ReferenceNode[])childNodes.ToArray( typeof( ReferenceNode ) ),
								childNodes.ToArray(),
								false )
				: null;
		}

		internal ReferenceNode GetDesignerReferences( )
		{
			if ( null == this.SortedBands || this.SortedBands.Count <= 0 )
				return null;

			return GetReferenceNode( this.SortedBands[0] );
		}

		#endregion // GetDesignerReferences

		#region GetVisibleRowsFromAllScrollRegions
		
		#region RowComparerByRelation
		
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private class RowComparerByRelation : IComparer
		private class RowComparerByRelation : IComparer<UltraGridRow>
		{
			internal RowComparerByRelation( )
			{
			}

			private int CompareHelper( UltraGridRow x, UltraGridRow y, bool sameLevelRowsComparisonOnly )
			{
				int r;

				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null != x.ParentRow )
				//{
				//    r = this.CompareHelper( x.ParentRow, y, true );
				UltraGridRow xParentRow = x.ParentRow;

				if ( null != xParentRow )
				{
					r = this.CompareHelper( xParentRow, y, true );

					if ( 0 != r )
						return r;
				}

				// MD 7/27/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( null != y.ParentRow )
				//{
				//    r = this.CompareHelper( x, y.ParentRow, true );
				UltraGridRow yParentRow = y.ParentRow;

				if ( null != yParentRow )
				{
					r = this.CompareHelper( x, yParentRow, true );

					if ( 0 != r )
						return r;
				}

				// If we get here then that means x and y share the same ancestory.
				// Therefore compare the levels.
				//
				r = x.OverallHierarchyLevel.CompareTo( y.OverallHierarchyLevel );

				if ( 0 != r )
					return sameLevelRowsComparisonOnly ? 0 : r;

				// If we get here then x and y belong to the same parent. Therefore
				// compare their indexes.
				//
				return x.Index.CompareTo( y.Index );
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//public int Compare( object xVal, object yVal )
			//{
			//    UltraGridRow x = xVal as UltraGridRow;
			//    UltraGridRow y = yVal as UltraGridRow;
			public int Compare( UltraGridRow x, UltraGridRow y )
			{
				if ( x == y )
					return 0;
				else if ( null == x )
					return -1;
				else if ( null == y )
					return 1;

               return this.CompareHelper( x, y, false );
			}
		}

		#endregion // RowComparerByRelation

		#region GetCardAreaRowsHelper

		// SSP 12/21/04 BR01302
		// Added GetCardAreaRows and GetCardAreaRowsHelper methods for card-view because the
		// VisibleRows off the RowScrollRegion doesn't contain rows from card areas.
		//
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void GetCardAreaRowsHelper( Infragistics.Win.UIElement elem, ArrayList list, Hashtable map )
		private void GetCardAreaRowsHelper( Infragistics.Win.UIElement elem, List<UltraGridRow> list, Hashtable map )
		{
			UIElementsCollection childElems = null != elem && elem.HasChildElements ? elem.ChildElements : null;
			if ( null != childElems )
			{
				for ( int i = 0; i < childElems.Count; i++ )
				{
					UIElement childElem = childElems[i];
					RowCellAreaUIElement rowElem = childElem as RowCellAreaUIElement;
					if ( null == rowElem )
					{
						this.GetCardAreaRowsHelper( childElem, list, map );
					}
					else
					{
						UltraGridRow row = rowElem.Row;
						if ( null != row && ! map.ContainsKey( row ) )
						{
							// SSP 1/6/05 BR01488
							// Only add the row to the list if the row is still valid. Added the if condition.
							//
							// SSP 6/3/05 - Optimizations
							// This is checked for later on at the end of GetVisibleRowsFromAllScrollRegions method.
							// 
							//if ( row.IsStillValid )
								list.Add( row );

							map[ row ] = DBNull.Value;
						}
					}
				}
			}
		}

		#endregion // GetCardAreaRowsHelper

		#region GetCardAreaRows

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private void GetCardAreaRows( UltraGridRow cardRow, ArrayList list, Hashtable map )
		private void GetCardAreaRows( UltraGridRow cardRow, List<UltraGridRow> list, Hashtable map )
		{
			Infragistics.Win.UIElement elem = this.UIElement;
			if ( null != elem )
				elem = elem.GetDescendant( typeof( CardAreaUIElement ), cardRow.ParentCollection );

			if ( null != elem )
				this.GetCardAreaRowsHelper( elem, list, map );
		}

		#endregion // GetCardAreaRows
		
		// SSP 6/3/05 - NAS 5.2 Filter Row/Extension of Summaries
		// Changed excludeTemplateAddRows to forCalculations because now we have to exclude special
		// rows as well.
		// 
		//internal ArrayList GetVisibleRowsFromAllScrollRegions( RowsCollection rows, 
		//			bool excludeRowAncestors, bool excludeGroupByRows, bool excludeTemplateAddRows )
		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//internal ArrayList GetVisibleRowsFromAllScrollRegions( RowsCollection rows, 
		internal List<UltraGridRow> GetVisibleRowsFromAllScrollRegions( RowsCollection rows, 
			bool excludeRowAncestors, bool excludeGroupByRows, bool forCalculations )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<UltraGridRow> list = new List<UltraGridRow>();

			Hashtable map = new Hashtable( );

			if ( null != this.RowScrollRegions )
			{
				foreach ( RowScrollRegion rsr in this.RowScrollRegions )
				{
					VisibleRowsCollection visibleRows = null != rsr ? rsr.VisibleRows : null;
					if ( null != visibleRows )
					{
						foreach ( VisibleRow vr in visibleRows )
						{
							UltraGridRow row = vr.Row;

							do
							{
								// SSP 7/12/05 BR04892
								// Make sure we don't add the same row more than once to the list.
								// 
								if ( null == row || map.ContainsKey( row ) )
									break;

								// Only add if not already added to the list before.
								//
								// SSP 6/3/05 - NAS 5.2 Filter Row/Extension of Summaries
								// Commented out the if conditions. Add the row regardless. Later we will
								// take out the rows that don't match. The reason for doing this is that the
								// call to GetCardAreaRows does not check any conditions.
								// 
								//if ( null != row && ! map.ContainsKey( row )
								//	&& ( null == rows || rows == row.ParentCollection )
								//	&& ( ! excludeGroupByRows || ! row.IsGroupByRow )
								//	&& ( ! excludeTemplateAddRows || ! row.IsTemplateAddRow ) )
								//{
									// SSP 12/21/04 BR01302
									// Added GetCardAreaRows and GetCardAreaRowsHelper methods for card-view because the
									// VisibleRows off the RowScrollRegion doesn't contain rows from card areas.
									//
									// ----------------------------------------------------------------------------
									//list.Add( row );
									if ( ! row.IsCard )
									{
										// SSP 1/6/05 BR01488
										// Only add the row to the list if the row is still valid. Added the if condition.
										//
										// SSP 6/3/05 - Optimizations
										// This is checked for later on at the end of this method.
										// 
										//if ( row.IsStillValid )
											list.Add( row );
									}
									else
									{
										this.GetCardAreaRows( row, list, map );
									}
									// ----------------------------------------------------------------------------
								//}

								map[ row ] = DBNull.Value;

							// SSP 6/3/05
							// It should have been excludeRowAncestors instead of excludeGroupByRows.
							// 
							//} while ( ! excludeGroupByRows && null != row && null != ( row = row.ParentRow ) );
							} while ( ! excludeRowAncestors && null != row && null != ( row = row.ParentRow ) );
						}
					}
				}
			}

			map.Clear( );

			// SSP 6/3/05 - NAS 5.2 Filter Row/Extension of Summaries
			// Moved the code to not include rows that don't match the criteria specified by the
			// parameters to here.
			// 
			// ----------------------------------------------------------------------------------
			for ( int i = 0, count = list.Count; i < count; i++ )
			{
				// MD 8/10/07 - 7.3 Performance
				// Use generics
				//UltraGridRow row = (UltraGridRow)list[i];
				UltraGridRow row = list[ i ];

				if ( null != rows && rows != row.ParentCollection 
					|| excludeGroupByRows && row.IsGroupByRow 
					// For ultra calc only include data rows and group-by rows.
					// 
					|| forCalculations && ! row.IsDataRow && ! row.IsGroupByRow
					// SSP 1/6/05 BR01488
					// Don't include the row in the list if the row is not valid.
					//
					|| ! row.IsStillValid )
				{
					list[i] = null;
				}
			}

			GridUtils.RemoveAll( list, null );
			// ----------------------------------------------------------------------------------

			// MD 8/13/07 - 7.3 Performance
			// Use generics
			//Infragistics.Win.Utilities.SortMerge( list, new RowComparerByRelation( ) );
			Utilities.SortMergeGeneric<UltraGridRow>( list, new RowComparerByRelation() );

			return list;
		}

		#endregion // GetVisibleRowsFromAllScrollRegions

		#region StoreCellValues

		internal void StoreCellValues( UltraGridRow row )
		{
			this.ClearStoredCellValues( );

			if ( null != row && null != row.Band && row.IsStillValid )
			{
				ColumnsCollection columns = row.Band.Columns;
				this.storedCellValues = new Hashtable( 1 + columns.Count );
				foreach ( UltraGridColumn col in columns )
				{
					if ( ! col.IsChaptered )
					{
						object val = row.GetCellValue( col );

						if ( null != val )
							this.storedCellValues[ col ] = val;
					}
				}

				this.storedCellValuesRow = row;
			}
		}

		#endregion // StoreCellValues

		#region StoredCellValuesRow
		
		internal UltraGridRow StoredCellValuesRow
		{
			get
			{
				return this.storedCellValuesRow;
			}
		}

		#endregion // StoredCellValuesRow

		#region ClearStoredCellValues

		internal void ClearStoredCellValues( )
		{
			this.storedCellValuesRow = null;
			if ( null != this.storedCellValues )
				this.storedCellValues.Clear( );
			this.storedCellValues = null;
		}

		#endregion // ClearStoredCellValues

		#region NotifyCalcEngineOfChangedCellsInStoredRow

		internal void NotifyCalcEngineOfChangedCellsInStoredRow( )
		{
			// If the row is deleted or it was null then return null.
			//
			if ( null != this.storedCellValuesRow && null != this.storedCellValues 
				&& this.storedCellValuesRow.IsStillValid )
			{
				foreach ( UltraGridColumn col in this.storedCellValuesRow.Band.Columns )
				{
					if ( ! col.IsChaptered )
					{
						object oldVal = this.storedCellValues[ col ];
						if ( null == oldVal || ! object.Equals( oldVal, this.storedCellValuesRow.GetCellValue( col ) ) )
							this.storedCellValuesRow.ParentCollection.NotifyCalcManager_ValueChanged( this.storedCellValuesRow, col );
					}
				}
			}

			this.ClearStoredCellValues( );
		}

		#endregion // NotifyCalcEngineOfChangedCellsInStoredRow

		#region FormulaRowIndexSourceDefault

		internal FormulaRowIndexSource FormulaRowIndexSourceDefault
		{
			get
			{
				if ( null != this.overrideObj && this.overrideObj.ShouldSerializeFormulaRowIndexSource( ) )
					return this.overrideObj.FormulaRowIndexSource;

				return FormulaRowIndexSource.VisibleIndex;
			}
		}

		#endregion // FormulaRowIndexSourceDefault

		#region EnsureAllFormulasCalculated

		// SSP 1/17/05 BR01753
		// Print and export layout won't calculate formulas but rather they will copy over 
		// the calculated values from the display layout. We need to ensure that all the 
		// columns are calculated before we go ahead with printing.
		//
		internal void EnsureAllFormulasCalculated( )
		{
			IUltraCalcManager calcManager = this.CalcManager;
			if ( null != calcManager )
			{
				GridReference gridRef = this.CalcReference;
				try
				{
					gridRef.recalcDeferredSuspended = true;
					this.AddRecalcDeferredFormulasToRecalcChain( );
					calcManager.PerformAction( Infragistics.Win.CalcEngine.UltraCalcAction.Recalc, (long)-1L );
				}
				finally
				{
					gridRef.recalcDeferredSuspended = false;
				}
			}
		}

		#endregion // EnsureAllFormulasCalculated

		#endregion // UltraCalc Related Changes

		// SSP 10/21/04 UWG3665
		// This is to fix a performance problem where if the same appearance instance is
		// assigned to a lot of cells or rows then all these cells/rows hook into that
		// appearance instance. When it comes the time for the cells to unhook from this
		// appearance (like when the grid is being disposed or the datasource is being
		// reset) it takes a long time because the .NET multicast events use linked list to
		// store the delegates. This requires performing a linear search and if there are a
		// lot of cells/rows that are hooked into the same appearence then it can end up
		// taking a very long time. The solution as implemented by this fix is for the
		// layout to hook into such appearances that are shared by multiple cells/rows once
		// and then manage the cells/rows that refer to these appearances and redirect the
		// sub object prop change to them.
		//
		#region Fix for UWG3665

		private Hashtable multiCastAppearances = null;
		private SubObjectPropChangeEventHandler multicastAppearancePropChangeHandler = null;

		#region OnMulticastAppearanceChanged
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private void DelegateSubObjPropChangeTo( object target, PropChangeInfo propChange )
		private static void DelegateSubObjPropChangeTo( object target, PropChangeInfo propChange )
		{
			UltraGridCell cell = target as UltraGridCell;
			if ( null != cell )
			{
				cell.InternalOnSubObjectPropChanged( propChange );
				return;
			}

			UltraGridRow row = target as UltraGridRow;
			if ( null != row )
			{
				row.InternalOnSubObjectPropChanged( propChange );
				return;
			}

			Debug.Assert( false );
		}

		private void OnMulticastAppearanceChanged( PropChangeInfo propChange ) 
		{
			AppearanceBase appearance = propChange.Source as AppearanceBase;
			Debug.Assert( null != appearance && null != this.multiCastAppearances );
			if ( null != appearance && null != this.multiCastAppearances )
			{
				object val = this.multiCastAppearances[ appearance ];
				if ( val is Hashtable )
				{
					foreach ( object key in ((Hashtable)val).Keys )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Mark members as static
						//this.DelegateSubObjPropChangeTo( key, propChange );
						UltraGridLayout.DelegateSubObjPropChangeTo( key, propChange );
					}
				}
				else
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.DelegateSubObjPropChangeTo( val, propChange );
					UltraGridLayout.DelegateSubObjPropChangeTo( val, propChange );
				}
			}
		}

		#endregion // OnMulticastAppearanceChanged

		#region SetMulticastAppearance

		// SSP 5/21/07 BR22749
		// We have to take into account the fact that the same appearance instance can be
		// set on two or more properties of the same row/cell instance.
		// Added AddMulticastObjectHelper helper method.
		// 
		private static object ONE = 1;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal void AddMulticastObjectHelper( Hashtable table, object obj )
		internal static void AddMulticastObjectHelper( Hashtable table, object obj )
		{
			if ( table.ContainsKey( obj ) )
			{
				int val = (int)table[obj];
				val++;
				table[ obj ] = val;
			}
			else
			{
				table[ obj ] = ONE;
			}
		}

		internal void SetMulticastAppearance( AppearanceBase appearance, object obj )
		{
			Debug.Assert( obj is UltraGridCell || obj is UltraGridRow );

			if ( null == this.multiCastAppearances )
			{
				this.multiCastAppearances = new Hashtable( 2, 0.95f );
				if ( null == this.multicastAppearancePropChangeHandler )
					this.multicastAppearancePropChangeHandler = new SubObjectPropChangeEventHandler( this.OnMulticastAppearanceChanged );
			}

			object val = multiCastAppearances[ appearance ];
			if ( val is Hashtable )
			{
				// SSP 5/21/07 BR22749
				// We have to take into account the fact that the same appearance instance can be
				// set on two or more properties of the same row/cell instance.
				// Added AddMulticastObjectHelper helper method.
				// 
				//((Hashtable)val).Add( obj, DBNull.Value );
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.AddMulticastObjectHelper( ( (Hashtable)val ), obj );
				UltraGridLayout.AddMulticastObjectHelper( ( (Hashtable)val ), obj );
			}
			else if ( null != val )
			{
				Hashtable table = new Hashtable( 2, 0.95f );
				multiCastAppearances[ appearance ] = table;

				// SSP 5/21/07 BR22749
				// We have to take into account the fact that the same appearance instance can be
				// set on two or more properties of the same row/cell instance.
				// Added AddMulticastObjectHelper helper method.
				// 
				//table.Add( val, DBNull.Value );
				//table.Add( obj, DBNull.Value );
				// MD 7/26/07 - 7.3 Performance
				// FxCop - Mark members as static
				//this.AddMulticastObjectHelper( table, val );
				//this.AddMulticastObjectHelper( table, obj );
				UltraGridLayout.AddMulticastObjectHelper( table, val );
				UltraGridLayout.AddMulticastObjectHelper( table, obj );
			}
			else
			{
				multiCastAppearances[ appearance ] = obj;
				appearance.SubObjectPropChanged += this.multicastAppearancePropChangeHandler;
			}
		}

		#endregion // SetMulticastAppearance
		
		#region RemoveMulticastAppearance
		
		internal void RemoveMulticastAppearance( AppearanceBase appearance, object obj )
		{
			if ( null != this.multiCastAppearances )
			{
				// SSP 5/21/07 BR22749
				// 
				// ----------------------------------------------------------------------
				bool removeEntry = false;

				object value = this.multiCastAppearances[appearance];
				if ( value is Hashtable )
				{
					Hashtable table = (Hashtable)value;
					if ( table.ContainsKey( obj ) )
					{
						int val = (int)table[obj];
						val--;
						if ( val > 0 )
							table[obj] = val;
						else
							table.Remove( obj );

						if ( 0 == table.Count )
							removeEntry = true;
					}
				}
				else if ( value == obj )
					removeEntry = true;


				if ( removeEntry )
				{
					this.multiCastAppearances.Remove( appearance );
					appearance.SubObjectPropChanged -= this.multicastAppearancePropChangeHandler;

					if ( 0 == multiCastAppearances.Count )
					{
						this.multiCastAppearances.Clear( );
						this.multiCastAppearances = null;
					}
				}
				
				// ----------------------------------------------------------------------
			}
		}

		#endregion // RemoveMulticastAppearance

		#endregion // Fix for UWG3665

		// MRS 10/22/04 - UWG3066
		#region SynchGridBackColor
		// AS 1/11/06 BR08714
		//private void SynchGridBackColor()
		internal void SynchGridBackColor()
		{
			if ( this.IsDisplayLayout &&
				this.grid != null )
			{
				// AS 1/11/06 BR08714
				// During deserialization, the linkedappearance may be set before the
				// corresponding appearance has been put in the appearances collection.
				// To prevent this, we won't access the appearance while initializing.
				// In addition, I don't think we need to unnecessarily force the 
				// appearance to be created. However to maintain the previous behavior
				// we'll set the BackColor to Color.Empty which is what would have 
				// happened if the appearance was not created or the backcolor wasn't
				// set yet.
				//
				//this.grid.BackColor = this.GetAppearance(LayoutAppearanceIndex.Default).BackColor;
				if (this.HasAppearance && this.grid.Initializing == false)
					this.grid.BackColor = this.Appearance.BackColor;
				else
					this.grid.BackColor = Color.Empty;
			}
		}
		#endregion SynchGridBackColor

		// JAS 4/5/05 v5.2 CaptionVisible property
		//
		#region CaptionVisible Logic

			#region CaptionVisible

		/// <summary>
		/// Gets/sets whether the grid's caption area will be displayed.
		/// By default the caption area will be displayed if the control's Text property is not an empty string.
		/// </summary>
		[LocalizedDescription("LD_UltraGridLayout_P_CaptionVisible") ]
		[LocalizedCategory("LC_Display")]
		public DefaultableBoolean CaptionVisible
		{
			get
			{
				return this.captionVisible;
			}
			set
			{
				if( value != this.captionVisible )
				{
					GridUtils.ValidateEnum( "CaptionVisible", typeof(DefaultableBoolean), value );

					this.captionVisible = value;

					this.NotifyPropChange( PropertyIds.CaptionVisible );
				}				
			}
		}

			#endregion // CaptionVisible

			#region ShouldSerializeCaptionVisible

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCaptionVisible()
		{
			return this.CaptionVisible != DefaultableBoolean.Default;
		}

			#endregion // ShouldSerializeCaptionVisible

			#region ResetCaptionVisible

		/// <summary>
		/// Resets the <see cref="UltraGridLayout.CaptionVisible"/> to 'Default'.
		/// </summary>
		public void ResetCaptionVisible()
		{
			this.CaptionVisible = DefaultableBoolean.Default;
		}

			#endregion // ResetCaptionVisible

		#endregion // CaptionVisible Logic

		// SSP 12/22/04 - IDataErrorInfo Support
		//
		#region DefaultDataErrorImage
		
		internal Image DataErrorImageFromResource
		{
			get
			{
				if ( null == this.dataErrorImageFromResource )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//Bitmap bmpRes = (Bitmap)this.GetImageFromResourse( "DataErrorImage.bmp" );
					Bitmap bmpRes = (Bitmap)UltraGridLayout.GetImageFromResourse( "DataErrorImage.bmp" );

					if ( null != bmpRes && bmpRes.Width > 0 && bmpRes.Height > 0 )
					{
						int w = bmpRes.Width;
						int h = bmpRes.Height;

						Bitmap bitmap = new Bitmap( w, h, System.Drawing.Imaging.PixelFormat.Format32bppArgb );
						
						Color c00 = bmpRes.GetPixel( 0, 0 );
						for ( int x = 0; x < w; x++ )
						{
							for ( int y = 0; y < h; y++ )
							{
								Color c = bmpRes.GetPixel( x, y );
								if ( c == c00 )
									bitmap.SetPixel( x, y, Color.FromArgb( 0, c ) );
								else
									bitmap.SetPixel( x, y, c );
							}
						}

						this.dataErrorImageFromResource = bitmap;
						bmpRes.Dispose( );
					}
					else
					{
						this.dataErrorImageFromResource = bmpRes;
					}
				}

				return this.dataErrorImageFromResource;
			}
		}

		#endregion // DefaultDataErrorImage

		// JAS v5.2 Wrapped Header Text 4/18/05
		//
		#region ColumnHeaderHeightVersionNumber

		internal int ColumnHeaderHeightVersionNumber
		{
			get { return this.columnHeaderHeightVersionNumber; }
		}

		internal void BumpColumnHeaderHeightVersionNumber() { ++this.columnHeaderHeightVersionNumber; }

		#endregion // ColumnHeaderHeightVersionNumber

		// SSP 5/3/05 - NewColumnLoadStyle
		// Added NewColumnLoadStyle and NewBandLoadStyle.
		//
		#region NewColumnLoadStyle & NewBandLoadStyle Related

		#region NewColumnLoadStyle

		/// <summary>
		/// Specifies how to load new columns from data source. Default value is <b>Show</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>NewColumnLoadStyle</b> property can be used specify how new columns are loaded
		/// when a data source is bound. If set to <b>Hide</b> any new columns and bands 
		/// contained in the data source are hidden. With this feature you can define columns
		/// at design time and set a datasource at runtime that has more columns and bands
		/// and have them automatically hidden.
		/// </p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridLayout_P_NewColumnLoadStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		public NewColumnLoadStyle NewColumnLoadStyle
		{
			get
			{
				return this.newColumnLoadStyle;
			}
			set
			{
				if ( value != this.newColumnLoadStyle )
				{
					GridUtils.ValidateEnum( typeof( NewColumnLoadStyle ), value );
					this.newColumnLoadStyle = value;
					this.NotifyPropChange( PropertyIds.NewColumnLoadStyle );
				}
			}
		}

		#endregion // NewColumnLoadStyle

		#region ShouldSerializeNewColumnLoadStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeNewColumnLoadStyle( )
		{
			return NewColumnLoadStyle.Show != this.newColumnLoadStyle;
		}

		#endregion // ShouldSerializeNewColumnLoadStyle

		#region ResetNewColumnLoadStyle

		/// <summary>
		/// Resets the property to its default value of <b>Show</b>.
		/// </summary>
		public void ResetNewColumnLoadStyle( )
		{
			this.NewColumnLoadStyle = NewColumnLoadStyle.Show;
		}

		#endregion // ResetNewColumnLoadStyle

		#region NewBandLoadStyle

		/// <summary>
		/// Specifies how to load new columns from data source. Default value is <b>Show</b>.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>NewBandLoadStyle</b> property can be used specify how new columns are loaded
		/// when a data source is bound. If set to <b>Hide</b> any new columns and bands 
		/// contained in the data source are hidden. With this feature you can define columns
		/// at design time and set a datasource at runtime that has more columns and bands
		/// and have them automatically hidden.
		/// </p>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridLayout_P_NewBandLoadStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		public NewBandLoadStyle NewBandLoadStyle
		{
			get
			{
				return this.newBandLoadStyle;
			}
			set
			{
				if ( value != this.newBandLoadStyle )
				{
					GridUtils.ValidateEnum( typeof( NewBandLoadStyle ), value );
					this.newBandLoadStyle = value;
					this.NotifyPropChange( PropertyIds.NewBandLoadStyle );
				}
			}
		}

		#endregion // NewBandLoadStyle

		#region ShouldSerializeNewBandLoadStyle

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeNewBandLoadStyle( )
		{
			return NewBandLoadStyle.Show != this.newBandLoadStyle;
		}

		#endregion // ShouldSerializeNewBandLoadStyle

		#region ResetNewBandLoadStyle

		/// <summary>
		/// Resets the property to its default value of <b>Show</b>.
		/// </summary>
		public void ResetNewBandLoadStyle( )
		{
			this.NewBandLoadStyle = NewBandLoadStyle.Show;
		}

		#endregion // ResetNewBandLoadStyle

		#region NewColumnLoadStyleInEffect

		internal object newColumnLoadStyleOverride = null;
		internal NewColumnLoadStyle NewColumnLoadStyleInEffect
		{
			get
			{
				if ( null != this.newColumnLoadStyleOverride )
					return (NewColumnLoadStyle)this.newColumnLoadStyleOverride;

				return this.NewColumnLoadStyle;
			}
		}

		#endregion // NewColumnLoadStyleInEffect

		#region NewBandLoadStyleInEffect

		internal object newBandLoadStyleOverride = null;
		internal NewBandLoadStyle NewBandLoadStyleInEffect
		{
			get
			{
				if ( null != this.newBandLoadStyleOverride )
					return (NewBandLoadStyle)this.newBandLoadStyleOverride;

				return this.NewBandLoadStyle;
			}
		}

		#endregion // NewBandLoadStyleInEffect

		#region InApplyLoadedBands

		// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
		// Added inApplyLoadedBands flag. Enclosed the following code into try-finally block.
		//
        // MRS 9/19/07 - BR26542
        // Renamed this to IsApplyingLayout everywhere it appears. 
        // This flag was originally added so that when applying a layout, we know whether or not
        // to honor the NewColumnLoadStyle or NewBandLoadStyle. We want to honor it when setting the
        // DataSource (when we are in the ApplyLoadedBands method) but not when cloning. 
        // But we also want to honor the property when loading a layout. So I renamed it to be more
        // accurate. 
		//internal bool InApplyLoadedBands
        internal bool IsApplyingLayout
		{
			get
			{
				return this.isApplyingLayout;
			}
		}

		#endregion // InApplyLoadedBands

		#endregion // NewColumnLoadStyle & NewBandLoadStyle Related

		#region PerformPageUpDownHelper

		// SSP 4/30/05 - NAS 5.2 Fixed Rows_
		// Added PerformPageUpDownHelper method. 
		//
		internal UltraGridRow PerformPageUpDownHelper( UltraGridAction action, UltraGridRow startRow )
		{
			// NOTE: The code in this method was moved from GetSelectableItem method and modified
			// to take into account fixed rows. For more info search for PerformPageUpDownHelper.
			//

			RowScrollRegion rsr = this.grid.ActiveRowScrollRegion;

			bool nonGroupByRowsOnly = UltraGridAction.PageUpCell == action || UltraGridAction.PageDownCell == action;
			if ( null == startRow && null != this.SortedBands && this.SortedBands.Count > 0 ) 
				startRow = rsr.GetFirstVisibleRow( this.SortedBands[0], nonGroupByRowsOnly, false );

			Debug.Assert( null != startRow && null != rsr );

			UltraGridRow newRow = null;

			if ( null != startRow && null != rsr )
			{
				if ( UltraGridAction.PageUpCell == action || UltraGridAction.PageUpRow == action )
				{
					bool startRowFixedTop = FixedRowStyle.Top == startRow.FixedResolvedLocation;

					UltraGridRow firstVisRow = rsr.GetFirstVisibleRow( startRow.Band, nonGroupByRowsOnly, ! startRowFixedTop );

					// if we're on the first row, scroll up 1 page
					if ( ! startRowFixedTop && startRow == firstVisRow
						// SSP 11/16/06 - NAS 6.3 - Optimizations - BR13748 BR14522
						// 
						&& ! rsr.IsFirstScrollableRowVisible( )
						|| firstVisRow == null )
					{
						rsr.KybdScroll( ScrollEventType.LargeDecrement );
						newRow = rsr.GetFirstVisibleRow( startRow.Band, nonGroupByRowsOnly, true );
						if ( null == newRow || newRow == startRow )
							newRow = rsr.GetFirstVisibleRow( startRow.Band, nonGroupByRowsOnly, false );
					}
					else
						newRow = firstVisRow;
				}
				else if ( UltraGridAction.PageDownCell == action || UltraGridAction.PageDownRow == action )
				{
					bool startRowFixedBottom = FixedRowStyle.Bottom == startRow.FixedResolvedLocation;

					UltraGridRow lastVisRow = rsr.GetLastVisibleRow( startRow.Band, nonGroupByRowsOnly, ! startRowFixedBottom );

					// JJD 12/06/01 - uWG826
					// If the last visible row is the active row and it is also the last
					// row overall then set the new row to the last visible row which
					// won't do anything
					//
					if ( lastVisRow != null && startRow == lastVisRow &&
						this.ViewStyleImpl.IsLastRow( lastVisRow ) )
					{
						newRow = lastVisRow;
					}
					else
					// if we're on the last row, scroll down 1 page
					if ( ! startRowFixedBottom && ( startRow == lastVisRow || lastVisRow == null ) )
					{
						rsr.KybdScroll( ScrollEventType.LargeIncrement );
						newRow = rsr.GetLastVisibleRow( startRow.Band, nonGroupByRowsOnly, true );
						if ( null == newRow || newRow == startRow )
							newRow = rsr.GetLastVisibleRow( startRow.Band, nonGroupByRowsOnly, false );
					}
					else
						newRow = lastVisRow;
				}
				else
					Debug.Assert( false );
			}

			return newRow;
		}

		#endregion // PerformPageUpDownHelper
		
		// JAS v5.2 GroupBy Break Behavior 5/4/05
		//
		#region BumpSortVersionNumber

		internal void BumpSortVersionNumber()
		{
			if( this.sortedBands == null )
				return;

			foreach( UltraGridBand band in this.sortedBands )
				if( band.HasSortedColumns )
				{
					band.SortedColumns.BumpSortVersion();

					// JAS 5/17/05 BR04005 - When the sort version is changed, 
					// the groupby version should change as well.
					//
					band.BumpGroupByHierarchyVersion();
				}

			this.DirtyGridElement( true );
		}

		#endregion // BumpSortVersionNumber

		// SSP 6/30/05 - NAS 5.3 Column Chooser
		// 
		#region NAS 5.3 Column Chooser Related

		#region ColumnChooserEnabled

		/// <summary>
		/// This specifies that the column chooser user interface is enabled for this UltraGrid.
		/// The UltraGrid uses this as a hint to let the user hide columns by dragging and dropping 
		/// them outside of the UltraGrid and also unhide an explicitly hidden group-by column by 
		/// dragging and dropping it over the column headers.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// This specifies that the column chooser user interface is enabled for this UltraGrid.
		/// The UltraGrid uses this as a hint to let the user hide columns by dragging and dropping 
		/// them outside of the UltraGrid and also unhide an explicitly hidden group-by column by 
		/// dragging and dropping it over the column headers.
		/// </p>
		/// <p class="body">
		/// The UltraGrid supports a built in user interface element that the user can use to 
		/// display the column chooser dialog from whithin the UltraGrid. You can enable this 
		/// ui element by setting the <see cref="UltraGridOverride.RowSelectorHeaderStyle"/> to 
		/// <b>ColumnChooserButton</b>. You can also externally display column chooser dialog by
		/// using the <see cref="UltraGridBase.ShowColumnChooser()"/> method or instantiating
		/// a <see cref="ColumnChooserDialog"/> form and displaying it.
		/// </p>
		/// <p class="body">
		/// You can also create custom column chooser dialog using the 
		/// <see cref="UltraGridColumnChooser"/>. To do so create a form and put a 
		/// <b>UltraGridColumnChooser</b> onto it along with any other custom user interface 
		/// elements.
		/// </p>
		/// <p class="body">
		/// This property is resolved to <b>True</b> if the UltraGrid determines that a column
		/// chooser user interface is enabled. Column chooser is considered to be enabled if
		/// the <see cref="UltraGridOverride.RowSelectorHeaderStyle"/> is set to 
		/// <b>ColumnChooserButton</b> or a column chooser dialog has been displayed at least 
		/// once. If neither of these conditions are met then this property is resolved to 
		/// <b>False</b>. When it's resolved to <b>False</b> the user can not hide a column by
		/// dragging and dropping it outside of the UltraGrid. Also the user can not unhide a 
		/// hidden column (a column whose <see cref="UltraGridColumn.Hidden"/> property has 
		/// been explicitly set to <b>True</b>.
		/// </p>
		/// <seealso cref="UltraGridBase.ShowColumnChooser()"/> 
		/// <seealso cref="UltraGridColumnChooser"/>
		/// <seealso cref="UltraGridBase.BeforeColumnChooserDisplayed"/>
		/// <seealso cref="UltraGridOverride.RowSelectorHeaderStyle"/>
		/// <seealso cref="ColumnChooserDialog"/>
		/// </remarks>
		[ LocalizedDescription("LDR_UltraGridLayout_P_ColumnChooserEnabled")]
		[ LocalizedCategory("LC_Behavior") ]
		public DefaultableBoolean ColumnChooserEnabled
		{
			get
			{
				return this.columnChooserEnabled;
			}
			set
			{
				if ( value != this.columnChooserEnabled )
				{
					GridUtils.ValidateEnum( typeof( DefaultableBoolean ), value );

					this.columnChooserEnabled = value;

					this.NotifyPropChange( PropertyIds.ColumnChooserEnabled );
				}
			}
		}

		#endregion // ColumnChooserEnabled

		#region ShouldSerializeColumnChooserEnabled

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeColumnChooserEnabled( )
		{
			return DefaultableBoolean.Default != this.columnChooserEnabled;
		}

		#endregion // ShouldSerializeColumnChooserEnabled

		#region ResetColumnChooserEnabled

		/// <summary>
		/// Resets the <see cref="UltraGridLayout.ColumnChooserEnabled"/> to <b>Default</b>.
		/// </summary>
		public void ResetColumnChooserEnabled( )
		{
			this.ColumnChooserEnabled = DefaultableBoolean.Default;
		}

		#endregion // ResetColumnChooserEnabled

		#region ColumnChooserButtonImageFromResource

		internal Image ColumnChooserButtonImageFromResource
		{
			get
			{
				if ( null == this.columnChooserButtonImageFromResource )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//this.columnChooserButtonImageFromResource = this.GetImageFromResourse( "Images.ColumnChooserButton.png" );
					this.columnChooserButtonImageFromResource = UltraGridLayout.GetImageFromResourse( "Images.ColumnChooserButton.png" );
				}

				return this.columnChooserButtonImageFromResource;
			}
		}

		#endregion // ColumnChooserButtonImageFromResource

		#endregion // NAS 5.3 Column Chooser Related

		// SSP 7/6/05 - NAS 5.3 Empty Rows
		// 
		#region NAS 5.3 Empty Rows

		#region EmptyRowSettings

		/// <summary>
		/// This object contains the settings for empty rows.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// <b>EmptyRowSettings</b> object contains the settings for empty rows.
		/// </p>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.EmptyRowSettings"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.EmptyRowSettings.ShowEmptyRows"/>
		/// </remarks>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		[ LocalizedDescription("LDR_UltraGridLayout_P_EmptyRowSettings")]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.UltraWinGrid.EmptyRowSettings EmptyRowSettings
		{
			get
			{
				if ( null == this.emptyRowSettings )
				{
					this.emptyRowSettings = new EmptyRowSettings( this );
					this.emptyRowSettings.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.emptyRowSettings;
			}
		}

		#endregion // EmptyRowSettings

		#region HasEmptyRowSettings

		internal bool HasEmptyRowSettings
		{
			get
			{
				return null != this.emptyRowSettings;
			}
		}

		#endregion // HasEmptyRowSettings

		#region ShouldSerializeEmptyRowSettings

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeEmptyRowSettings( )
		{
			return null != this.emptyRowSettings && this.emptyRowSettings.ShouldSerialize( );
		}

		#endregion // ShouldSerializeEmptyRowSettings

		#region ResetEmptyRowSettings

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetEmptyRowSettings( )
		{
			if ( null != this.emptyRowSettings )
				this.emptyRowSettings.Reset( );
		}

		#endregion // ResetEmptyRowSettings

		#endregion // NAS 5.3 Empty Rows

		// SSP 8/11/05 BR05398
		// 
		#region Fix for BR05398

		private UltraGridRow initializeRowSuspendedFor = null;

		// MD 8/10/07 - 7.3 Performance
		// Use generics
		//private ArrayList initializeRowSuspendedFor_List = null;
		private List<UltraGridRow> initializeRowSuspendedFor_List = null;

		internal bool SuspendInitializeRowFor( UltraGridRow row )
		{
			if ( null == this.initializeRowSuspendedFor )
			{
				this.initializeRowSuspendedFor = row;
			}
			else if ( row == this.initializeRowSuspendedFor )
			{
				// Return true if the row is already suspended.
				// 
				return true;
			}
			else
			{
				if ( null == this.initializeRowSuspendedFor_List )
				{
					// MD 8/10/07 - 7.3 Performance
					// Use generics
					//this.initializeRowSuspendedFor_List = new ArrayList( );
					this.initializeRowSuspendedFor_List = new List<UltraGridRow>();
				}
				else if ( this.initializeRowSuspendedFor_List.Contains( row ) )
					// Return true if the row is already suspended.
					// 
					return true;

				this.initializeRowSuspendedFor_List.Add( this.initializeRowSuspendedFor );
				this.initializeRowSuspendedFor = row;
			}

			// Return false if the row wasn't already suspended when this method got called.
			// 
			return false;
		}

		internal bool IsInitializeRowSuspendedFor( UltraGridRow row )
		{
			return this.initializeRowSuspendedFor == row
				|| null != this.initializeRowSuspendedFor_List
				   && this.initializeRowSuspendedFor_List.Contains( row );
		}

		internal void ResumeInitializeRowFor( UltraGridRow row )
		{
			if ( row == this.initializeRowSuspendedFor )
			{
				this.initializeRowSuspendedFor = null;
				if ( null != this.initializeRowSuspendedFor_List )
				{
					int lastIndex = this.initializeRowSuspendedFor_List.Count - 1;
					if ( lastIndex >= 0 )
					{
						// MD 8/10/07 - 7.3 Performance
						// Use generics
						//this.initializeRowSuspendedFor = (UltraGridRow)this.initializeRowSuspendedFor_List[ lastIndex ];
						this.initializeRowSuspendedFor = this.initializeRowSuspendedFor_List[ lastIndex ];

						this.initializeRowSuspendedFor_List.RemoveAt( lastIndex );
					}

					if ( 0 == lastIndex )
						this.initializeRowSuspendedFor_List = null;
				}
			}
			else if ( null != this.initializeRowSuspendedFor_List )
			{
				this.initializeRowSuspendedFor_List.Remove( row );
				if ( 0 == this.initializeRowSuspendedFor_List.Count )
					this.initializeRowSuspendedFor_List = null;
			}				
		}

		#endregion // Fix for BR05398

		#region InvalidateHotTrackedElement

		// SSP 9/9/05 BR06315
		// 
		private bool InvalidateHotTrackedElement( Infragistics.Win.UIElement elem )
		{
			if ( elem is Infragistics.Win.UltraWinGrid.HeaderUIElement )
			{
				HeaderUIElement headerElem = (Infragistics.Win.UltraWinGrid.HeaderUIElement)elem;
				if ( headerElem.InvalidateIfHotTracked( ) )
					return true;
			}
			else if ( elem is RowUIElementBase )
			{
				RowUIElementBase rowElem = (RowUIElementBase)elem;
				if ( rowElem.InvalidateIfHotTracked( ) )
					return true;
			}
			else
			{
				if ( elem.HasChildElements )
				{
					foreach ( UIElement childElem in elem.ChildElements )
					{
						if ( this.InvalidateHotTrackedElement( childElem ) )
							return true;
					}
				}
			}

			return false;
		}

		internal void InvalidateHotTrackedElement( )
		{
			if ( this.HasUIElement )
				this.InvalidateHotTrackedElement( this.UIElement );
		}

		#endregion // InvalidateHotTrackedElement

		// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
		// 
		#region NAS 6.1 Multi-cell Operations

		#region Public Properties/Methods

		#region ClipboardCellSeparator

		/// <summary>
		/// Specifies the cell value separator when copying to the clipboard.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridLayout_P_ClipboardCellSeparator")]
		[ LocalizedCategory("LC_Behavior") ]
		public string ClipboardCellSeparator
		{
			get
			{
				return this.clipboardCellSeparator;
			}
			set
			{
				if ( this.clipboardCellSeparator != value )
				{
					this.clipboardCellSeparator = value;
					this.NotifyPropChange( PropertyIds.ClipboardCellSeparator );
				}
			}
		}

		#endregion // ClipboardCellSeparator

		#region ClipboardRowSeparator

		/// <summary>
		/// Specifies the logical row separator when copying to the clipboard.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridLayout_P_ClipboardRowSeparator")]
		[ LocalizedCategory("LC_Behavior") ]
		public string ClipboardRowSeparator
		{
			get
			{
				return this.clipboardRowSeparator;
			}
			set
			{
				if ( this.clipboardRowSeparator != value )
				{
					this.clipboardRowSeparator = value;
					this.NotifyPropChange( PropertyIds.ClipboardRowSeparator );
				}
			}
		}

		#endregion // ClipboardRowSeparator

		#region ClipboardCellDelimiter

		/// <summary>
		/// Specifies the cell value delimiter when copying to the clipboard.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridLayout_P_ClipboardCellDelimiter")]
		[ LocalizedCategory("LC_Behavior") ]
		public string ClipboardCellDelimiter
		{
			get
			{
				return this.clipboardCellDelimiter;
			}
			set
			{
				if ( this.clipboardCellDelimiter != value )
				{
					this.clipboardCellDelimiter = value;
					this.NotifyPropChange( PropertyIds.ClipboardCellDelimiter );
				}
			}
		}

		#endregion // ClipboardCellDelimiter

		#region ShouldSerializeClipboardCellDelimiter

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeClipboardCellDelimiter( )
		{
			return null != this.ClipboardCellDelimiter;
		}

		#endregion // ShouldSerializeClipboardCellDelimiter

		#region ResetClipboardCellDelimiter

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetClipboardCellDelimiter( )
		{
			this.ClipboardCellDelimiter = null;
		}

		#endregion // ResetClipboardCellDelimiter

		#region ShouldSerializeClipboardCellSeparator

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeClipboardCellSeparator( )
		{
			return null != this.ClipboardCellSeparator;
		}

		#endregion // ShouldSerializeClipboardCellSeparator

		#region ResetClipboardCellSeparator

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetClipboardCellSeparator( )
		{
			this.ClipboardCellSeparator = null;
		}

		#endregion // ResetClipboardCellSeparator

		#region ShouldSerializeClipboardRowSeparator

		/// <summary>
		/// Returns true if the property needs to be serialized.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeClipboardRowSeparator( )
		{
			return null != this.ClipboardRowSeparator;
		}

		#endregion // ShouldSerializeClipboardRowSeparator

		#region ResetClipboardRowSeparator

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetClipboardRowSeparator( )
		{
			this.ClipboardRowSeparator = null;
		}

		#endregion // ResetClipboardRowSeparator

		#endregion // Public Properties/Methods

		#region Internal Properties/Methods

		#region ClipboardCellDelimiterResolved

		internal string ClipboardCellDelimiterResolved
		{
			get
			{
				return null != this.clipboardCellDelimiter ? this.clipboardCellDelimiter : string.Empty;
			}
		}

		#endregion // ClipboardCellDelimiterResolved

		#region ClipboardCellSeparatorResolved

		internal string ClipboardCellSeparatorResolved
		{
			get
			{
				return null != this.clipboardCellSeparator ? this.clipboardCellSeparator : "\t";
			}
		}

		#endregion // ClipboardCellSeparatorResolved

		#region ClipboardRowSeparatorResolved

		internal string ClipboardRowSeparatorResolved
		{
			get
			{
				return null != this.clipboardRowSeparator ? this.clipboardRowSeparator : System.Environment.NewLine;
			}
		}

		#endregion // ClipboardRowSeparatorResolved

		#region ActionHistory

		private ActionHistory actionHistory = null;
		internal ActionHistory ActionHistory
		{
			get
			{
				if ( null == this.actionHistory )
					this.actionHistory = new ActionHistory( new MultiCellOperationInfo( this ) );

				return this.actionHistory;
			}
		}

		#endregion // ActionHistory

		#region HasActionHistoryBeenAllocated

		internal bool HasActionHistoryBeenAllocated
		{
			get
			{
				return null != this.actionHistory;
			}
		}

		#endregion // HasActionHistoryBeenAllocated

		#region PerformMultiCellOperation

		internal bool PerformMultiCellOperation( MultiCellOperation operation )
		{
			return this.ActionHistory.MultiCellOperationInfo.PerformMultiCellOperation( operation );
		}

		#endregion // PerformMultiCellOperation

		#region ClearUndoHistoryHelper

		// SSP 11/28/05 - NAS 6.1 Multi-cell Operations
		// 
		internal void ClearUndoHistoryHelper( bool force )
		{
			if ( null != this.actionHistory 
				&& ( force || ! this.actionHistory.IsPerformingAction ) )
				this.actionHistory.Clear( );
		}

		#endregion // ClearUndoHistoryHelper

		#endregion // Internal Properties/Methods

		#endregion // NAS 6.1 Multi-cell Operations

		#region OnAppStyleChanged

		// SSP 3/16/06 - App Styling
		// 
		internal void OnAppStyleChanged( )
		{
			this.DirtyGridElement( true, true, true );
			this.BumpCellChildElementsCacheVersion( );
			this.ClearBandRowHeightMetrics( );
			this.ClearCachedCaptionHeight( );
			this.OnBaseFontChanged( );
			this.BumpBandCardViewVersion( );
			this.BumpColumnHeaderHeightVersionNumber( );
			this.BumpGrandVerifyVersion( );
			this.BumpRowLayoutVersion( );

			StyleUtils.DirtyCachedPropertyVals( this );
		}

		#endregion // OnAppStyleChanged

		#region CellHottrackInvalidationStyle

		// SSP 8/28/06 - NAS 6.3
		// Added CellHottrackInvalidationStyle property on the layout.
		// 
		/// <summary>
		/// Specifies whether to invalidate the cell when the mouse enters or leaves it.
		/// </summary>
		[ LocalizedDescription("LDR_UltraGridLayout_P_CellHottrackInvalidationStyle")]
		[ LocalizedCategory("LC_Behavior") ]
		public CellHottrackInvalidationStyle CellHottrackInvalidationStyle
		{
			get
			{
				return this.cellHottrackInvalidationStyle;
			}
			set
			{
				if ( this.cellHottrackInvalidationStyle != value )
				{
					GridUtils.ValidateEnum( typeof( CellHottrackInvalidationStyle ), value );

					this.cellHottrackInvalidationStyle = value;

                    this.NotifyPropChange( PropertyIds.CellHottrackInvalidationStyle );
				}
			}
		}

		internal CellHottrackInvalidationStyle CellHottrackInvalidationStyleResolved
		{
			get
			{
				if ( CellHottrackInvalidationStyle.Default == this.cellHottrackInvalidationStyle )
				{
					UltraGrid grid = this.grid as UltraGrid;
					return null == grid || grid.InvalidateCellOnMouseEnterLeave
						? CellHottrackInvalidationStyle.Always : CellHottrackInvalidationStyle.Never;
				}

				return this.cellHottrackInvalidationStyle;
			}
		}

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCellHottrackInvalidationStyle( )
		{
			return CellHottrackInvalidationStyle.Default != this.cellHottrackInvalidationStyle;
		}

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Advanced ) ]
		public void ResetCellHottrackInvalidationStyle( )
		{
			this.CellHottrackInvalidationStyle = CellHottrackInvalidationStyle.Default;
		}

		#endregion // CellHottrackInvalidationStyle

        // MRS v7.2 - PDF Report Writer
        //
        #region ResolveLayoutAppearance
        /// <summary>
        /// Resolves the appearance of the Layout. 
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize.</param>
        /// <param name="requestedProps">The appearance properties to resolve.</param>
        /// <param name="resolveDefaultColors">Indicates whether to resolve default colors if the colors are not explicitly set.</param>
        /// <returns>An AppearancePropsFlags indicating the unresolved properties.</returns>
        public void ResolveLayoutAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps, bool resolveDefaultColors)
        {
            ResolveAppearanceContext context = new ResolveAppearanceContext(typeof(Infragistics.Win.UltraWinGrid.UltraGridLayout), requestedProps);
            context.ResolveDefaultColors = resolveDefaultColors;

            // SSP 3/14/06 - App Styling
            // 
            context.Role = StyleUtils.GetRole(this, StyleUtils.GetControlAreaRole(this), out context.ResolutionOrder);

            this.ResolveAppearance(ref appearance, ref context);

            requestedProps = context.UnresolvedProps;
        }
        #endregion //ResolveLayoutAppearance

        // MBS 1/21/08 - BR29252
        #region UseOptimizedDataResetMode

        private bool useOptimizedDataResetMode;

        /// <summary>
        /// Gets or sets whether the grid will use a caching mechanism when receiving a Reset notification 
        /// from the underlying DataSource when using LoadOnDemand.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b>This property should only be enabled when the underlying DataSource does <b>not</b> have an 
        /// efficient implementation of the IndexOf method, such as by performing a linear search.  By enabling this property, 
        /// all the items in the underlying DataSource will be accessed in order to provide a more efficient caching 
        /// mechanism for when the grid receives a Reset notification.
        /// </p>
        /// </remarks>
        [LocalizedCategory("LC_Behavior")]
        [LocalizedDescription("LD_Layout_UseOptimizedDataResetMode")]
        public bool UseOptimizedDataResetMode
        {
            get { return this.useOptimizedDataResetMode; }
            set
            {
                if (value == this.useOptimizedDataResetMode)
                    return;

                this.useOptimizedDataResetMode = value;
                this.NotifyPropChange(PropertyIds.UseOptimizedDataResetMode);
            }
        }

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeUseOptimizedDataResetMode()
        {
            return this.useOptimizedDataResetMode != false;
        }

        /// <summary>
        /// Resets the UseOptimizedDataResetMode property to its default value.
        /// </summary>
        public void ResetUseOptimizedDataResetMode()
        {
            this.UseOptimizedDataResetMode = false;
        }
        #endregion //UseOptimizedDataResetMode

        // MRS 3/25/2008 - BR31396
        #region HasColSpan
        internal bool HasColSpan
        {
            get
            {
                if (this.needToVerifyColSpan == true)
                    this.VerifyColSpan();

                return this.hasColSpan;
            }
        }
        #endregion //HasColSpan

        // MRS 3/25/2008 - BR31396
        #region VerifyColSpan
        internal void VerifyColSpan()
        {
            if (this.needToVerifyColSpan == false)
                return;

            this.hasColSpan = false;
            this.needToVerifyColSpan = false;

            foreach (UltraGridBand band in this.SortedBands)
            {
                if (band.AllowColSizingResolved == AllowColSizing.Synchronized &&
                    band.HiddenResolved == false &&
                    band.UseRowLayoutResolved == false)
                {
                    foreach (UltraGridColumn column in band.Columns)
                    {
                        if (column.IsChaptered == false &&
                            column.HiddenResolved == false &&
                            column.ColSpan != 1)
                        {
                            this.hasColSpan = true;
                            return;
                        }
                    }
                }
            }
        }
        #endregion //VerifyColSpan

        // MRS NAS v8.2 - CardView Printing
        #region CardView Printing

        #region AllowCardPrinting

        /// <summary>
        /// Specifies whether the grid should honor the CardView property on the band when printing. 
        /// </summary>
        /// <remarks>
        /// <p class="body">By default, the grid will not print cards, it will always print in tabular view. By setting this property, you can specify that the grid should print the root band using in CardView if CardView is set to true on the band.</p>
        /// </remarks>
        [LocalizedDescription("LDR_UltraGridLayout_P_AllowCardPrinting")]
        [LocalizedCategory("LC_Behavior")]
        public Infragistics.Win.UltraWinGrid.AllowCardPrinting AllowCardPrinting
        {
            get
            {
                return this.allowCardPrinting;
            }
            set
            {
                if (this.allowCardPrinting != value)
                {
                    if (!Enum.IsDefined(typeof(AllowCardPrinting), value))
                        throw new InvalidEnumArgumentException("AllowCardPrinting", (int)value, typeof(AllowCardPrinting));

                    this.allowCardPrinting = value;

                    this.NotifyPropChange(PropertyIds.AllowCardPrinting);
                }
            }
        }

        #region ShouldSerializeAllowCardPrinting

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns></returns>
        protected bool ShouldSerializeAllowCardPrinting()
        {
            return Infragistics.Win.UltraWinGrid.AllowCardPrinting.Never != this.allowCardPrinting;
        }

        #endregion // ShouldSerializeAllowCardPrinting

        #region ResetAllowCardPrinting

        /// <summary>
        /// Resets the <see cref="AllowCardPrinting"/> property to its default value of <b>Never</b>.
        /// </summary>
        public void ResetAllowCardPrinting()
        {
            this.AllowCardPrinting = Infragistics.Win.UltraWinGrid.AllowCardPrinting.Never;
        }

        #endregion // ResetAllowCardPrinting

        #endregion // AllowCardPrinting

        #region PerformAutoResizeColumns

        /// <summary>
        /// Resizes all columns in all bands based on cell values in either currently visible rows or all rows depending on the value of autoSizeType argument.
        /// </summary>
        /// <param name="autoSizeType"></param>
        /// <param name="sizeHiddenColumns">True to resize columns that are hidden; otherwise false.</param>
        public void PerformAutoResizeColumns(bool sizeHiddenColumns, PerformAutoSizeType autoSizeType)
        {
            this.PerformAutoResizeColumns(sizeHiddenColumns, autoSizeType, true);
        }

        /// <summary>
        /// Resizes all columns in all bands based on cell values in either currently visible rows or all rows depending on the value of autoSizeType argument.
        /// </summary>
        /// <param name="autoSizeType">Specifies if and which rows to base the auto-sizing of the column.</param>
        /// <param name="includeHeader">Specifies whether to include header caption width into the auto-size.</param>
        /// <param name="sizeHiddenColumns">True to resize columns that are hidden; otherwise false.</param>
        public void PerformAutoResizeColumns(bool sizeHiddenColumns, PerformAutoSizeType autoSizeType, bool includeHeader)
        {
            this.PerformAutoResizeHelper(sizeHiddenColumns, autoSizeType, includeHeader);
        }

        #region PerformAutoResizeHelper

        private void PerformAutoResizeHelper(bool sizeHiddenColumns, PerformAutoSizeType autoSizeType, bool includeHeader)
        {
            foreach (UltraGridBand band in this.SortedBands)
            {
                band.PerformAutoResizeColumns(sizeHiddenColumns, autoSizeType, includeHeader);
            }
        }
        #endregion //PerformAutoResizeHelper

        #endregion //PerformAutoResizeColumns

        #region PerformAutoResizeCards
        /// <summary>
        /// Resizes the width of all cards in every band based on cell values in all rows.
        /// </summary>
        /// <param name="includeHiddenColumns"></param>
        public void PerformAutoResizeCards(bool includeHiddenColumns)
        {
            foreach (UltraGridBand band in this.sortedBands)
            {
                band.PerformAutoResizeCards(includeHiddenColumns);
            }            
        }
        #endregion //PerformAutoResizeCards

        #endregion //CardView Printing


        //  BF 2/28/08  FR09238 - AutoCompleteMode
        #region AutoCompleteFilterCondition
        internal FilterCondition AutoCompleteFilterCondition
        {
            get { return this.grid != null ? this.grid.AutoCompleteFilterCondition : null; }
        }
        #endregion AutoCompleteFilterCondition
        // MBS 4/24/09 - TFS12665
        // Implemented a series of caching mechanisms that are used when regenerating a list of
        // all the visible rows.  This is done because there are many times, in a multi-band 
        // situation, where a row will end up calling the same logic multiple times since a child
        // row may need to ask its parent what certain measurements or properties are, which themselves
        // may require walking up the parent chain.
        //
        #region BeginCaching

        internal void BeginCaching()
        {
            this.cacheListenerCount++;

            if (this.bands != null && this.bands.Count > 0)
            {
                if(this.cachedBandData == null)
                    this.cachedBandData = new Dictionary<UltraGridBand, CachedBandInfo>(this.bands.Count);

                if (this.cachedRowData == null)
                    this.cachedRowData = new Dictionary<UltraGridRow, CachedRowInfo>();

                if (this.bands.Count > 1 && this.cachedChildBandsData == null)
                    this.cachedChildBandsData = new Dictionary<ChildBandsCollection, CachedChildBandsInfo>(this.bands.Count);
            }
        }
        #endregion //BeginCaching
        //
        #region EndCaching

        internal void EndCaching()
        {
            // Only clear out the caching data if all the methods that requested caching have ended their request.  This is
            // because some methods might request caching while called from within another that also has caching
            if (--this.cacheListenerCount == 0)
            {
                if (this.cachedBandData != null)
                {
                    this.cachedBandData.Clear();
                    this.cachedBandData = null;
                }

                if (this.cachedRowData != null)
                {
                    this.cachedRowData.Clear();
                    this.cachedRowData = null;
                }

                if (this.cachedChildBandsData != null)
                {
                    this.cachedChildBandsData.Clear();
                    this.cachedChildBandsData = null;
                }
            }
        }
        #endregion //EndCaching
        //
        #region IsCachingBandInfo

        internal bool IsCachingBandInfo
        {
            get { return this.cachedBandData != null; }
        }
        #endregion //IsCachingBandInfo
        //
        #region IsCachingChildBandsInfo

        internal bool IsCachingChildBandsInfo
        {
            get { return this.cachedChildBandsData != null; }
        }
        #endregion //IsCachingChildBandsInfo
        //
        #region IsCachingRowInfo

        internal bool IsCachingRowInfo
        {
            get { return this.cachedRowData != null; }
        }
        #endregion //IsCachingRowInfo
        //
        #region CachedBandInfo - Internal Class

        internal class CachedBandInfo
        {
            private bool? hidden;
            public bool? Hidden
            {
                get { return this.hidden; }
                set { this.hidden = value; }
            }

            private int hierarchicalLevel = -1;
            public int HierarchicalLevel
            {
                get { return this.hierarchicalLevel; }
                set { this.hierarchicalLevel = value; }
            }
        }
        #endregion //CachedBandInfo - Internal Class
        //
        #region CachedChildBandsInfo - Internal Class

        internal class CachedChildBandsInfo
        {
            private int scrollCount = -1;
            public int ScrollCount
            {
                get { return this.scrollCount; }
                set { this.scrollCount = value; }
            }
        }
        #endregion //CachedChildBandsInfo - Internal Class
        //
        #region CachedRowInfo - Internal Class

        internal class CachedRowInfo
        {
            private int baseHeight = -1;
            public int BaseHeight
            {
                get { return this.baseHeight; }
                set { this.baseHeight = value; }
            }

            private int rowSpacingBefore = -1;
            public int RowSpacingBefore
            {
                get { return this.rowSpacingBefore; }
                set { this.rowSpacingBefore = value; }
            }

            private int rowSpacingAfter = -1;
            public int RowSpacingAfter
            {
                get { return this.rowSpacingAfter; }
                set { this.rowSpacingAfter = value; }
            }

            private int scrollCount = -1;
            public int ScrollCount
            {
                get { return this.scrollCount; }
                set { this.scrollCount = value; }
            }
        }
        #endregion //CachedRowInfo - Internal Class
        //
        #region CachedBandData

        internal Dictionary<UltraGridBand, CachedBandInfo> CachedBandData
        {
            get
            {
                return this.cachedBandData;
            }
        }
        #endregion //CachedBandData
        //
        #region CachedChildBandsData

        internal Dictionary<ChildBandsCollection, CachedChildBandsInfo> CachedChildBandsData
        {
            get
            {
                return this.cachedChildBandsData;
            }
        }
        #endregion //CachedChildBandsData
        //
        #region CachedRowData

        internal Dictionary<UltraGridRow, CachedRowInfo> CachedRowData
        {
            get
            {
                return this.cachedRowData;
            }
        }
        #endregion //CachedRowData

        // MRS NAS 9.1 - Sibling Band Order
        #region NAS 9.1 - Sibling Band Order

        #region SortedBands
        /// <summary>
        /// A sorted collection of UltraGridBand objects. The collection includes all the UltraGridBand objects in the control in the order in which they will be displayed.
        /// </summary>
        /// <remarks>
        /// <p class="body">The SortedBands collection mirrors the <see cref="Bands"/> collection and contains all of the UltraGridBand objects in the grid. SortedBands differs from Band in only one respect - it takes into account the <see cref="UltraGridBand.VisiblePosition"/> property on the band. The bands in the SortedBands collection will be in the order in which they will display in the grid.</p>
        /// </remarks>
        /// <seealso cref="Infragistics.Win.UltraWinGrid.BandsCollection"/>        
        [LocalizedDescription("LD_UltraGridLayout_P_SortedBands")]
        [LocalizedCategory("LC_Data")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public Infragistics.Win.UltraWinGrid.BandsCollection SortedBands
        {
            get
            {
                if (this.sortedBands == null)
                {
                    this.sortedBands = new BandsCollection(this, true);
                    this.BumpSortedBandsVersion();
                }                

                return this.sortedBands;
            }
        }
        #endregion //SortedBands        

        #region BumpSortedBandsVersion
        internal void BumpSortedBandsVersion()
        {
            if (this.sortedBandsVersion != int.MaxValue)
                this.sortedBandsVersion++;
            else
                this.sortedBandsVersion = int.MinValue;
        }
        #endregion //BumpSortedBandsVersion

        #endregion NAS 9.1 - Sibling Band Order

        //  BF 2/10/09  TFS13739
        #region IsInSetCellValue
        internal bool IsInSetCellValue
        {
            get { return this.isInSetCellValue; }
            set { this.isInSetCellValue = value; }
        }
        #endregion IsInSetCellValue

        // MBS 4/6/09 - NA9.2 SelectionOverlay
        #region SelectionOverlayColor

        /// <summary>
        /// Gets or sets the color used to overlay the selection area of the grid.  This property overrides the default selection colors.
        /// </summary>
        /// <remarks>
        /// <p class="note">
        /// <b>Note: </b> If either this property or the <see cref="SelectionOverlayBorderColor"/> are set, 
        /// any selected appearances (i.e. cell, row, column) that would normally apply to a cell are ignored.
        /// </p>
        /// </remarks>   
        [LocalizedDescription("LD_UltraGridLayout_P_SelectionOverlayColor")]
        public Color SelectionOverlayColor
        {
            get { return this.selectionOverlayColor; }
            set
            {
                if (this.selectionOverlayColor == value)
                    return;

                this.selectionOverlayColor = value;
                StyleUtils.DirtyCachedPropertyVals(this);
                this.NotifyPropChange(PropertyIds.SelectionOverlayColor);
            }
        }

        internal Color SelectionOverlayColorResolved
        {
            get
            {
                object objVal;
                if (!StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.SelectionOverlayColor, out objVal))
                {
                    objVal = StyleUtils.CachePropertyValue(this,
                        StyleUtils.CustomProperty.SelectionOverlayColor, this.SelectionOverlayColor, Color.Empty, Color.Empty);
                }

                Color color = (Color)objVal;
                if (color != null && color != Color.Empty && color.A == 255)
                {
                    // If the user has specified a solid color, we will add a default alpha channel.
                    // This is because we don't want to draw a solid color over the entire selection
                    // since it would completely obscure everything
                    color = Color.FromArgb(DEFAULT_OVERLAY_ALPHA, color);
                }
                return color;
            }
        }

        /// <summary>
        /// Returns true if the <see cref="SelectionOverlayColor"/> property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected internal bool ShouldSerializeSelectionOverlayColor()
        {
            return this.selectionOverlayColor != Color.Empty;
        }

        /// <summary>
        /// Resets the <see cref="SelectionOverlayColor"/> property to its default value.
        /// </summary>
        public void ResetSelectionOverlayColor()
        {
            this.SelectionOverlayColor = Color.Empty;
        } 
        #endregion //SelectionOverlayColor
        //
        #region SelectionOverlayBorderColor

        /// <summary>
        /// Gets or sets the color used for the borders drawn around the selection overlay.
        /// </summary>
        /// <remarks>
        /// <p class="body">
        /// The border color will be resolved to <see cref="Color.Black"/> if the <see cref="SelectionOverlayColor"/> is set.
        /// If no borders are desired, this property can be set to <see cref="Color.Transparent"/>.
        /// </p>
        /// <p class="note">
        /// <b>Note: </b>If this property is set without the <b>SelectionOverlayColor</b> being
        /// set, the borders will surround the selected area.  If either of these properties are set,
        /// however, any selected appearances (i.e. cell, row, column) that would normally apply
        /// to the cell are now ignored.
        /// </p>
        /// </remarks>        
        [LocalizedDescription("LD_UltraGridLayout_P_SelectionOverlayBorderColor")]
        public Color SelectionOverlayBorderColor
        {
            get { return this.selectionOverlayBorderColor; }
            set
            {
                if (this.selectionOverlayBorderColor == value)
                    return;

                this.selectionOverlayBorderColor = value;
                StyleUtils.DirtyCachedPropertyVals(this);
                this.NotifyPropChange(PropertyIds.SelectionOverlayBorderColor);
            }
        }

        internal Color SelectionOverlayBorderColorResolved
        {
            get
            {
                object objVal;
                if (!StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.SelectionOverlayBorderColor, out objVal))
                {
                    // If the user has specified a selection overlay but not a border color, we need to use a default value
                    // since we do want borders to be drawn by default
                    Color resolvedColor = this.SelectionOverlayColorResolved != Color.Empty ? Color.Black : Color.Empty;

                    objVal = StyleUtils.CachePropertyValue(this,
                        StyleUtils.CustomProperty.SelectionOverlayBorderColor, this.SelectionOverlayBorderColor, Color.Empty, resolvedColor);
                }

                return (Color)objVal;
            }
        }

        /// <summary>
        /// Returns true if the <see cref="SelectionOverlayBorderColor"/> property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected internal bool ShouldSerializeSelectionOverlayBorderColor()
        {
            return this.selectionOverlayBorderColor != Color.Empty;
        }

        /// <summary>
        /// Resets the <see cref="SelectionOverlayBorderColor"/> property to its default value.
        /// </summary>
        public void ResetSelectionOverlayBorderColor()
        {
            this.SelectionOverlayBorderColor = Color.Empty;
        }
        #endregion //SelectionOverlayBorderColor

        #region 9.2 RowSelector State-Specific Images

        #region RowSelectorImages

        /// <summary>
        /// Gets the RowSelectorImageInfo object containing the state-specific row selector images. 
        /// </summary>
        [
        InfragisticsFeature(
        Version = FeatureInfo.Version_9_2,
        FeatureName = FeatureInfo.FeatureName_RowSelectorStateSpecificImages)
        ]
        [LocalizedDescription("LD_UltraGridLayout_P_RowSelectorImages")]
        [LocalizedCategory("LC_Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public RowSelectorImageInfo RowSelectorImages
        {
            get
            {
                if (this.rowSelectorImages == null)
                {
                    this.rowSelectorImages = new RowSelectorImageInfo(this);
                    this.rowSelectorImages.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }
                return this.rowSelectorImages;
            }
        }

        #endregion RowSelectorImages

        #region HasRowSelectorImages

        [
        InfragisticsFeature(
        Version = FeatureInfo.Version_9_2,
        FeatureName = FeatureInfo.FeatureName_RowSelectorStateSpecificImages)
        ]
        internal bool HasRowSelectorImages
        {
            get
            {
                return this.rowSelectorImages != null;
            }
        }

        #endregion HasRowSelectorImages

        #region ShouldSerializeRowSelectorImages

        /// <summary>
        /// Returns true if this property is not set to its default value
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        [
        InfragisticsFeature(
        Version = FeatureInfo.Version_9_2,
        FeatureName = FeatureInfo.FeatureName_RowSelectorStateSpecificImages)
        ]
        protected bool ShouldSerializeRowSelectorImages()
        {
            return (this.HasRowSelectorImages &&
                this.rowSelectorImages.ShouldSerialize());
        }

        #endregion ShouldSerializeRowSelectorImages

        #region ResetRowSelectorImages

        /// <summary>
        /// Resets the RowSelectorImages object
        /// </summary>
        /// 
        [
        InfragisticsFeature(
        Version = FeatureInfo.Version_9_2,
        FeatureName = FeatureInfo.FeatureName_RowSelectorStateSpecificImages)
        ]
        public void ResetRowSelectorImages()
        {
            if (this.HasRowSelectorImages)
                this.rowSelectorImages.Reset();
        }

        #endregion ResetRowSelectorImages

        #endregion RowSelector State-Specific Images
        //
        #region SelectionOverlayBorderThickness

        /// <summary>
        /// Gets or sets the thickness of the borders drawn around the selection overlay.
        /// </summary>
        [LocalizedDescription("LD_UltraGridLayout_P_SelectionOverlayBorderThickness")]
        public int SelectionOverlayBorderThickness
        {
            get { return this.selectionOverlayBorderThickness; }
            set
            {
                if (this.selectionOverlayBorderThickness == value)
                    return;

                this.selectionOverlayBorderThickness = value;
                StyleUtils.DirtyCachedPropertyVals(this);
                this.NotifyPropChange(PropertyIds.SelectionOverlayBorderThickness);
            }
        }

        internal int SelectionOverlayBorderThicknessResolved
        {
            get
            {
                object objVal;
                if (!StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.SelectionOverlayBorderThickness, out objVal))
                {
                    objVal = StyleUtils.CachePropertyValue(this,
                        StyleUtils.CustomProperty.SelectionOverlayBorderThickness, this.SelectionOverlayBorderThickness, -1, SELECTION_BORDER_THICKNESS);
                }

                return (int)objVal;
            }
        }

        /// <summary>
        /// Returns true if the <see cref="SelectionOverlayBorderThickness"/> property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected internal bool ShouldSerializeSelectionOverlayBorderThickness()
        {
            return this.selectionOverlayBorderThickness != -1;
        }

        /// <summary>
        /// Resets the <see cref="SelectionOverlayBorderThickness"/> property to its default value.
        /// </summary>
        public void ResetSelectionOverlayBorderThickness()
        {
            this.SelectionOverlayBorderThickness = -1;
        }
        #endregion //SelectionOverlayBorderThickness

        // CDS 7/22/09 NAS 9.2 Cell ActiveAppearance Related properties. 
        // (ActiveAppearancesEnabled, SelectedAppearancesEnabled, DefaultSelectedBackColor & DefaultSelectedForeColor)
        #region 9.2 Cell ActiveAppearance related properties

        #region ActiveAppearancesEnabledResolved
        internal bool ActiveAppearancesEnabledResolved
        {
            get
            {
                if (this.HasOverride &&
                    this.overrideObj.ShouldSerializeActiveAppearancesEnabled())
                    return (this.overrideObj.ActiveAppearancesEnabled == DefaultableBoolean.True);

                return true;
            }
        }

        #endregion ActiveAppearancesEnabledResolved

        #region SelectedAppearancesEnabledResolved
        internal bool SelectedAppearancesEnabledResolved
        {
            get
            {
                if (this.HasOverride &&
                    this.overrideObj.ShouldSerializeSelectedAppearancesEnabled())
                    return (this.overrideObj.SelectedAppearancesEnabled == DefaultableBoolean.True);

                return true;
            }
        }

        #endregion SelectedAppearancesEnabledResolved

        #region DefaultSelectedBackColor

        /// <summary>
        /// Gets or sets the default color used for the backcolor of selected cells in the grid.
        /// </summary>
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_CellActiveAppearance)
        ]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridLayout_P_DefaultSelectedBackColor")]
        public Color DefaultSelectedBackColor
        {
            get { return this.defaultSelectedBackColor; }
            set
            {
                if (this.defaultSelectedBackColor == value)
                    return;

                this.defaultSelectedBackColor = value;
                StyleUtils.DirtyCachedPropertyVals(this);
                this.DirtyGridElement();
                this.NotifyPropChange(PropertyIds.DefaultSelectedBackColor);
            }
        }

        internal Color DefaultSelectedBackColorResolved
        {
            get
            {
                object objVal;
                if (!StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.DefaultSelectedBackColor, out objVal))
                {
                    objVal = StyleUtils.CachePropertyValue(this,
                        StyleUtils.CustomProperty.DefaultSelectedBackColor, this.DefaultSelectedBackColor, SystemColors.Highlight, SystemColors.Highlight);
                }

                return (Color)objVal;
            }
        }

        /// <summary>
        /// Returns true if the <see cref="DefaultSelectedBackColor"/> property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected internal bool ShouldSerializeDefaultSelectedBackColor()
        {
            return this.defaultSelectedBackColor != SystemColors.Highlight;
        }

        /// <summary>
        /// Resets the <see cref="DefaultSelectedBackColor"/> property to its default value.
        /// </summary>
        public void ResetDefaultSelectedBackColor()
        {
            this.DefaultSelectedBackColor = SystemColors.Highlight;
        }

        #endregion DefaultSelectedBackColor

        #region DefaultSelectedForeColor

        /// <summary>
        /// Gets or sets the default color used for the forecolor of selected cells in the grid.
        /// </summary>
        [InfragisticsFeature(
            Version = FeatureInfo.Version_9_2,
            FeatureName = FeatureInfo.FeatureName_CellActiveAppearance)
        ]
        [LocalizedCategory("LC_Appearance")]
        [LocalizedDescription("LD_UltraGridLayout_P_DefaultSelectedForeColor")]
        public Color DefaultSelectedForeColor
        {
            get { return this.defaultSelectedForeColor; }
            set
            {
                if (this.defaultSelectedForeColor == value)
                    return;

                this.defaultSelectedForeColor = value;
                StyleUtils.DirtyCachedPropertyVals(this);
                this.DirtyGridElement();
                this.NotifyPropChange(PropertyIds.DefaultSelectedForeColor);
            }
        }

        internal Color DefaultSelectedForeColorResolved
        {
            get
            {
                object objVal;
                if (!StyleUtils.GetCachedPropertyVal(this, StyleUtils.CustomProperty.DefaultSelectedForeColor, out objVal))
                {
                    objVal = StyleUtils.CachePropertyValue(this,
                        StyleUtils.CustomProperty.DefaultSelectedForeColor, this.DefaultSelectedForeColor, SystemColors.HighlightText, SystemColors.HighlightText);
                }

                return (Color)objVal;
            }
        }

        /// <summary>
        /// Returns true if the <see cref="DefaultSelectedForeColor"/> property needs to be serialized.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected internal bool ShouldSerializeDefaultSelectedForeColor()
        {
            return this.defaultSelectedForeColor != SystemColors.HighlightText;
        }

        /// <summary>
        /// Resets the <see cref="DefaultSelectedForeColor"/> property to its default value.
        /// </summary>
        public void ResetDefaultSelectedForeColor()
        {
            this.DefaultSelectedForeColor = SystemColors.HighlightText;
        }

        #endregion DefaultSelectedForeColor

        #endregion 9.2 Cell ActiveAppearance related properties
    }
}
