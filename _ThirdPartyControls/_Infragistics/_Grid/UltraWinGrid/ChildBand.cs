#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    /// The UltraGridChildBand object contains a collection of child row's from
    /// a single band with a common parent row. For example, if band 0 was   
    /// 'Customers' and it had 2 child bands, say 'Orders' and 'Invoices'.
    /// Customer 12345 would have 2 UltraGridChildBand objects, one containing a
    /// collection of all of its orders and the other containing a collection
    /// of all of its invoices.  
    /// </summary>
    public class UltraGridChildBand : KeyedSubObjectBase
		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//	
        // , IScrollableRowCountManagerOwner
    {
        private Infragistics.Win.UltraWinGrid.UltraGridBand band;
        private Infragistics.Win.UltraWinGrid.RowsCollection rows = null;

        private Infragistics.Win.UltraWinGrid.ChildBandsCollection  parentCollection;

		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//	
        //private ScrollableRowCountManager scrollableRowCountManager = null;

		// JJD 1/07/04 - Accessibility
		private System.Windows.Forms.AccessibleObject cardLabelsAccessibilityObject;

        internal UltraGridChildBand( Infragistics.Win.UltraWinGrid.ChildBandsCollection parentCollection,
                            Infragistics.Win.UltraWinGrid.UltraGridBand band )
        {
            if ( null == parentCollection )
                throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_43"));

            if ( null == band )
                throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_44"));

            this.parentCollection   = parentCollection;
            this.band               = band;
        }

		/// <summary>
		/// Returns the UltraGridBand that the object belongs to, if any. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Band</b> property of an object refers to a specific band in the grid as defined by an UltraGridBand object. You use the <b>Band</b> property to access the properties of a specified UltraGridBand object, or to return a reference to the UltraGridBand object that is associated with the current object.</p>
		/// <p class="body">UltraGridBand objects are the foundation of the hierarchical data structure used by UltraWinGrid. Any row or cell in the grid must be accessed through its UltraGridBand object. Bands are also used to apply consistent formatting and behavior to the rows that they comprise. An UltraGridBand object is used to display all the data rows from a single level of a data hierarchy. UltraGridBand objects contain multiple sets of child UltraGridRow objects that actually display the data of the recordset. All of the rows that are drawn from a single Command in the DataEnvironment make up a band.</p>
		/// <p class="body">The rows of a band are generally displayed in groups of one more in order to show rows from subsequent bands that are linked to rows in the current band via the structure of the data hierarchy. For example, if a hierarchical recordset has Commands that display Customer, Order and Order Detail data, each one of these Commands maps to its own UltraGridBand in the UltraWinGrid. The rows in the Customer band will appear separated by any Order data rows that exist for the customers. By the same token, rows in the Order band will be appear separated to make room for Order Detail rows. How this looks depends on the <b>ViewStyle</b> settings selected for the grid, but the concept of visual separation is readily apparent when the UltraWinGrid is used with any hierarchical recordset.</p>
		/// <p class="body">Although the rows in a band may appear to be separated, they are treated contiguously. When selecting a column in a band, you will see that the cells of that column become selected in all rows for the band, regardless of any intervening rows. Also, it is possible to collapse the hierarchical display so that any children of the rows in the current band are hidden.</p>
		/// </remarks>
        public UltraGridBand Band
        {
            get
            {
				return this.band;
            }
        }

        /// <summary>
        /// Returns the parent row. 
        /// <see cref="Infragistics.Win.UltraWinGrid.UltraGridRow"/>
        /// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridRow ParentRow
        {
            get
            {
                return this.parentCollection.ParentRow;
            }
        }

        /// <summary>
        /// The index of this row in its parent collection.
        /// </summary>
        public int Index
        {
            get
            {
                return this.parentCollection.IndexOf(this);
            }
        }

		internal bool HaveRowsBeenInitialized
		{
			get
			{
				return null != this.rows;
			}
		}

		// SSP 2/19/04
		//
		internal bool HasRowsBeenAllocated
		{
			get
			{
				return null != this.rows;
			}
		}

		internal RowsCollection TopLevelRows
		{
			get
			{
				// SSP 6/19/03 UWG2232
				// Related to SuspendRowSynchronization and ResumeRowSynchronization methods.
				// Calling TopLevelRowsCollection off the Rows will do the same thing as below
				// except it won't cause any rows collection to get inadvertantly synced which
				// is what we want. The only two calling methods (Band.GetCurrentRow and 
				// Band.GetCurrentParentRows) have assumed this all along so this change shouldn't
				// cause any problems. If it does then those methods will have to modified and
				// this change will have to be backed out.
				//
				

				return this.Rows.TopLevelRowsCollection;
			}
		}

		// SSP 2/20/04
		// GroupByColumn property is apparently not being used anywhere. Commented it out.
		//
		
		

		internal bool IsGroupByChildBand
		{
			get
			{
				if ( this.Band.HasGroupBySortColumns )
				{
					// If the parent row's band and our band are not the same,
					// then check if our band has any group by columns
					if ( this.ParentRow.Band != this.Band )
					{
						return this.Band.HasGroupBySortColumns;
					}
					else if ( this.ParentRow is UltraGridGroupByRow )
					{
						UltraGridGroupByRow parentGroupByRow = (UltraGridGroupByRow)this.ParentRow;

						int index = parentGroupByRow.Band.SortedColumns.IndexOf( parentGroupByRow.Column );

						if ( index >= 0 )
						{	
							UltraGridColumn column = null;
							if ( 1 + index < parentGroupByRow.Band.SortedColumns.Count )
								column = parentGroupByRow.Band.SortedColumns[ 1 + index ];

							if ( null != column && column.IsGroupByColumn )
								return true;
						}
						
					}
				}
				
				return false;
			}
		}
		
		/// <summary>
		/// The collection of child rows.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowsCollection Rows
        {
            get
            {				
				if ( null == this.rows )
				{
					this.rows = new RowsCollection( this.band, this.ParentRow );

					// SSP 10/4/01
					// Changed the Rows.rowsDirty flag's default value from
					// true to false, so that the rows collection used for
					// something else (possibly selection) don't try to
					// sync the rows with the database. So setting the
					// dirty flag here to true.
					//
					rows.DirtyRows( );

					//this.rows.InitRows();

					// SSP 5/3/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
					// Fire the new InitializeRowsCollection event.
					//
					this.rows.FireInitializeRowsCollection( );
				}

				return this.rows;
            }
        }

		// SSP 2/19/04 - Virtual Mode - Optimization
		// Changed the way we calculate the scroll positions. Removed code for 
		// IScrollableRowCountManagerOwner and various implementations of it.
		//	
		#region Commented out IScrollableRowCountManagerOwner implementation

		

		#endregion // Commented out IScrollableRowCountManagerOwner implementation

		// SSP 9/30/02 UWG1717
		// Overrode Key property to return the key of the associated band. Before Key property
		// always returned "" because we never were setting the Key or overriding the Key proeprty
		// to return the key of the band.
		//
		/// <summary>
		/// The internally assigned key value for the UltraGridChildBand. This key is the same as the key of the UltraGridBand object associated with this UltraGridChildBand object. The <b>Key</b> property is read-only for the <b>UltraGridChildBand</b> object.
		/// </summary>
		[DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ] 
		public override String Key
		{
			get
			{
				return this.band.Key;
			}
			set
			{
				throw new NotSupportedException( Shared.SR.GetString( "LER_NotSupportedException_345" ) );
			}
		}

		#region OnDispose

		// SSP 9/9/03 UWG2308
		// Override OnDispose.
		//
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnDispose( )
		{
			if ( null != this.rows )
				this.rows.Dispose( );
			this.rows = null;

			base.OnDispose( );
		}

		#endregion // OnDispose

		// JJD 01/07/04 - Added Accessibility support
		#region GetAccessibleChild

		internal AccessibleObject GetAccessibleChild(int index)
		{
			if ( !this.ParentRow.IsExpanded )
				return null;

			if ( this.CardLabelsAccessibilityObject != null )
			{
				if ( index == 0 )
					return this.CardLabelsAccessibilityObject;

				index--;
			}

			if ( index >= 0 && index < this.Rows.Count )
				return this.Rows[index].AccessibilityObject;

			index -= this.Rows.Count;

			AccessibleObject acc = this.CardScrollbarAccessibilityObject;

			if ( acc != null )
			{
				if ( index == 0 )
					return acc;

				index--;
			}

			return null;
		}

		#endregion GetAccessibleChild

		// JJD 01/07/04 - Added Accessibility support
		#region GetAccessibleChildCount

		internal int GetAccessibleChildCount()
		{
			if ( !this.ParentRow.IsExpanded )
				return 0;

			int count = this.Rows.Count;

			if ( this.CardLabelsAccessibilityObject != null )
				count++;
			
			if ( this.CardScrollbarAccessibilityObject != null )
				count++;

			return count;
		}

		#endregion GetAccessibleChildCount

		// JJD 1/07/04 - Accessibility
		//
		#region CardLabelsAccessibilityObject

		internal System.Windows.Forms.AccessibleObject CardLabelsAccessibilityObject
		{
			get
			{
				// return null if the band is hidden 
				if ( this.band.HiddenResolved == true )
					return null;

				// for card view only return a band accessible object for
				// merge label style
				if ( !this.band.CardView ||
					this.band.CardSettings.StyleResolved != CardStyle.MergedLabels )
					return null;

				if ( this.cardLabelsAccessibilityObject == null )
					this.cardLabelsAccessibilityObject = this.band.Layout.Grid.CreateAccessibilityInstance( new HeadersCollection.HeadersAccessibleObject( this ) );

				return this.cardLabelsAccessibilityObject;
			}
		}

		#endregion CardLabelsAccessibilityObject

		// JJD 1/07/04 - Accessibility
		//
		#region CardScrollbarAccessibilityObject

		internal System.Windows.Forms.AccessibleObject CardScrollbarAccessibilityObject
		{
			get
			{

				if ( !this.ParentRow.IsExpanded ||
					 this.band.CardView == false )
					return null;

				RowColRegionIntersectionUIElement rcr = this.band.Layout.UIElement.GetDescendant( typeof( RowColRegionIntersectionUIElement ), new object [] { this.band.Layout.ActiveRowScrollRegion, this.band.Layout.ActiveColScrollRegion } ) as RowColRegionIntersectionUIElement;

				if ( rcr != null )
				{
					foreach ( UIElement child in rcr.ChildElements )
					{
						CardAreaUIElement cardArea = child as CardAreaUIElement;

						if ( cardArea != null &&
							cardArea.Band == this.band &&
							cardArea.Rows.ParentRow == this.ParentRow )
						{
							return cardArea.ScrollbarAccessibilityObject;
						}
					}
				}

				return null;
			}
		}

		#endregion CardScrollbarAccessibilityObject
	}
}
