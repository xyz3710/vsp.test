#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Drawing;

namespace Infragistics.Win.UltraWinGrid
{

	internal enum DragType
	{
		Bitmap
	};

	internal enum DragObjectType
	{
		Column,
		Group
	};

	internal enum CurrentCursor
	{
		None,
		Valid,
		Invalid,

		// SSP 7/13/05 - NAS 5.3 Column Chooser
		// Added HideColumnCursor and Default entries. 
		// 
		HideColumnCursor,

		Default
	};

	internal class DragEffect 
	{
		private DragStrategy	dragStrategy	= null;

		// SSP 6/27/05 - NAS 5.3 Column Chooser
		// We need to use top level windows for the column chooser. For that changed to 
		// using the Infragistics.Win's DragDropIndicatorManager.
		// 
		//private DragEffectWnd	dragWnd			= null;		
		//private DropGuideWnd	dropGuideWnd	= null;
		private DragDropIndicatorManager dragDropIndicatorManager = null;

		//private Control			parentControl   = null;
		private UltraGrid		grid			= null;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//private DragType		dragType		= DragType.Bitmap;

		private CurrentCursor	currentCursor;		
		private Rectangle		activeItemRect	= Rectangle.Empty;

		// SSP 7/13/05
		// Commented out the associated properties since they weren't being used.
		// 
		//private Cursor			validCursor		= null;
		//private Cursor          invalidCursor	= null;
		//private Cursor          originalCursor  = null;

		
		internal DragEffect( UltraGrid grid )
		{
			this.grid = grid;
			this.dragStrategy  = null;

			// SSP 6/27/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			//this.dragWnd       = null;
			//this.dropGuideWnd   = null;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.dragType      = DragType.Bitmap;

			this.currentCursor = CurrentCursor.None;

			// SSP 7/13/05
			// Commented out the associated properties since they weren't being used.
			// 
			//this.validCursor    = null;
			//this.invalidCursor  = null;
			//this.originalCursor = null;

			//m_dwMsgMgrMouseToken    = 0;
			//m_dwMsgMgrKeyToken      = 0;

			// MRS 5/18/05 - BR03687
			// These will be created lazily
//			this.dragWnd		= new DragEffectWnd( this.grid );			
//			this.dropGuideWnd	= new DropGuideWnd( this.grid );


			this.activeItemRect = Rectangle.Empty;
		}
		
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//internal bool Setup( DragStrategy strategy, DragType dragType )
		internal bool Setup( DragStrategy strategy )
		{
			this.dragStrategy  = strategy;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Avoid unused private fields
			//this.dragType      = dragType;

			// SSP 6/27/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			// --------------------------------------------------------------------------
			if ( null != this.dragDropIndicatorManager )
				this.dragDropIndicatorManager.InitializeDragIndicator( (Image)null );
			
			// --------------------------------------------------------------------------

			if ( null != this.dragStrategy)
			{
				// Set the Drag Bitmap				
				HeaderBase   header = dragStrategy.GetDragBitmapItem( );

				if ( null != header )
				{
					header.SetDragBitmap( this, this.dragStrategy.IsMultiItemDrag( ) );

                    // CDS 9.2 Column Moving Indicators
                    UltraGridBand band = header.Band;
                    if (band != null &&
                        this.dragDropIndicatorManager != null)
                    {
                        // resolve the DragDropIndicatorSettings.
                        DragDropIndicatorSettings dragDropIndicatorSettings = band.DragDropIndicatorSettingsResolved;

                        // if the resolved DragDropIndicatorSettings does not contain all default values, 
                        // then initialize the DropIndicatorControl with the custom values.
                        if (dragDropIndicatorSettings.ShouldSerialize())
                            this.dragDropIndicatorManager.InitializeDropIndicator(dragDropIndicatorSettings);
                    }
                }
			}

			return true;
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//internal DragType DragType 
		//{
		//    get
		//    {
		//        return this.dragType;
		//    }
		//}

		#endregion Not Used

		internal DragStrategy DragStrategy
		{
			get
			{
				return this.dragStrategy;
			}
		}

		// SSP 6/27/05 - NAS 5.3 Column Chooser
		// We need to use top level windows for the column chooser. For that changed to 
		// using the Infragistics.Win's DragDropIndicatorManager.
		// 
		internal DragDropIndicatorManager DragDropIndicatorManager
		{
			get
			{
				if ( null == this.dragDropIndicatorManager )
				{
                    // MRS 2/4/2009 - TFS13513
                    //this.dragDropIndicatorManager = new DragDropIndicatorManager( 
                    //    System.Windows.Forms.Orientation.Horizontal, this.grid );
                    this.dragDropIndicatorManager = new DragDropIndicatorManager(
                        System.Windows.Forms.Orientation.Horizontal, 
                        this.grid,
                        UltraGrid.DRAG_INDICATOR_OPACITY);

					this.dragDropIndicatorManager.DropIndicatorArrowSize = Infragistics.Win.DragDropIndicatorManager.ArrowSize.Large;
				}

				return this.dragDropIndicatorManager;
			}
		}

		// SSP 6/27/05 - NAS 5.3 Column Chooser
		// Width and height params have no meaning since they are always the same size 
		// as the bitmap.
		// 
		//internal void SetBitmap( Bitmap newBitmap, int width, int height, Region windowRegion )
		internal void SetBitmap( Bitmap newBitmap, Region windowRegion, Point dragWindowOffset )
		{
			// SSP 6/27/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			//if ( null != this.dragWnd )
			//	this.dragWnd.SetBitmap( newBitmap, width, height, windowRegion );
			this.DragDropIndicatorManager.DragIndicatorOffset = dragWindowOffset;
			this.DragDropIndicatorManager.InitializeDragIndicator( newBitmap, windowRegion );
		}

		


		// SSP 7/13/05 - NAS 5.3 Column Chooser
		// Moved this to the GridUtils.
		// 
		


		internal CurrentCursor CurrentCursor
		{
			get
			{
				return this.currentCursor;
			}
			set
			{
				if ( this.currentCursor != value )
				{
					this.currentCursor = value;

					// SSP 7/13/05 - NAS 5.3 Column Chooser
					// This is now being done in the UltraGrid's DragMoveHelper method.
					//
					//this.SetCursorToCurrent( );
				}
			}
		}

		// SSP 7/13/05
		// The following two proeprties aren't being used anywhere. Commented them out.
		// 
		

		internal void Display()
		{
			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			if ( null != this.DragDropIndicatorManager )
				this.DragDropIndicatorManager.ShowDragIndicator( );
			

			this.CurrentCursor = CurrentCursor.Valid;
		}
    
		// SSP 6/29/05 - NAS 5.3 Column Chooser
		// We need to use top level windows for the column chooser. For that changed to 
		// using the Infragistics.Win's DragDropIndicatorManager.
		// Added MoveDragIndicator helper method.
		// 
		internal void MoveDragIndicator( Point mouseLoc )
		{
			if ( null != this.dragDropIndicatorManager
				&& this.DragDropIndicatorManager.IsDragIndicatorVisible )
				this.DragDropIndicatorManager.ShowDragIndicator( mouseLoc );
		}

		// SSP 6/29/05 - NAS 5.3 Column Chooser
		// We need to use top level windows for the column chooser. For that changed to 
		// using the Infragistics.Win's DragDropIndicatorManager.
		// Added HideDragIndicator helper method.
		// 
		internal void HideDragIndicator( )
		{
			if ( null != this.dragDropIndicatorManager )
				this.DragDropIndicatorManager.HideDragIndicator( );
		}

		// SSP 7/16/03 - Fixed headers
		//
		int lastColScrollRegionPosition = -1;

		internal void OnDragMove( Point mouseLoc )
		{
			// SSP 7/16/03 - Fixed headers
			// If the headers are scrolled, then invalidate the activeItemRect because
			// the items could have potentially moved.
			//
			// ------------------------------------------------------------------------
			bool activeItemRectValid = true;
			if ( null != this.grid && null != this.grid.ActiveColScrollRegion )
			{
				int newScrollPosition = this.grid.ActiveColScrollRegion.Position;
				if ( this.lastColScrollRegionPosition != newScrollPosition )
					activeItemRectValid = false;

				this.lastColScrollRegionPosition = newScrollPosition;
			}
			// ------------------------------------------------------------------------

			// If we have an ActiveItem Rect set,
			// first check to see if the mouseLoc point
			// is still within that area. If so
			// the location is still valid
			// SSP 7/16/03 - Fixed headers
			// Change related to above.
			//
			//if ( !this.activeItemRect.IsEmpty )
			if ( activeItemRectValid && !this.activeItemRect.IsEmpty )
			{
				if ( this.activeItemRect.Contains( mouseLoc ) )
				{
					if ( null != this.grid )
						mouseLoc = this.grid.PointToScreen( mouseLoc );
            
					// SSP 6/28/05 - NAS 5.3 Column Chooser
					// We need to use top level windows for the column chooser. For that changed to 
					// using the Infragistics.Win's DragDropIndicatorManager.
					// 
					//if ( null != this.dragWnd )
					//	this.dragWnd.Move( mouseLoc );
					this.MoveDragIndicator( mouseLoc );
            
					if ( null != this.grid )
						this.grid.Update( );
            
					return;
				}
			}

			// Changed to take DropLocationInfo class to handle drops on
			// group headers as well as on empty levels.
			bool valid = false;
			DropLocationInfo dropLocationInfo = new DropLocationInfo( );

			Rectangle itemBounds = Rectangle.Empty;

			if ( null == this.dragStrategy )
				return;

			Point origMouseLoc = mouseLoc;

			// let the grid know that we received a mouse move in case
			// it wants to first scroll the row and/or col regions
			// We only want to scroll cols/groups only.
			// 
			valid = this.IsLocationValid( ref mouseLoc, dropLocationInfo, ref itemBounds );
    
			if ( !valid )
			{
				this.CurrentCursor = CurrentCursor.Invalid;

				this.HideDropGuides( );
			}
			else
			{
				this.CurrentCursor = CurrentCursor.Valid;

				
				if ( this.dragStrategy.IsDropValid( mouseLoc, dropLocationInfo, itemBounds ) )
				{
					dropLocationInfo.SetDropTargetItemBounds( itemBounds );

					// Make sure that the point that the dropguide
					// will be displayed is within the bounds of a
					// colscrollregion.
					//
					bool inColScrollRegion = true;
            
					this.dragStrategy.GetDropLocation( dropLocationInfo, mouseLoc, itemBounds );

					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//Point displayPoint = this.CalcDropGuideDisplayPoint( itemBounds, dropLocationInfo );
					Point displayPoint = DragEffect.CalcDropGuideDisplayPoint( itemBounds, dropLocationInfo );

					if ( null != this.grid.DisplayLayout &&
						 null != this.grid.DisplayLayout.GetColScrollRegions( false ) &&
						 null == this.grid.DisplayLayout.GetColScrollRegions( false ).GetRegionFromOrigin( displayPoint.X )
						
						// SSP 11/28/01 UWG777
						// Added these two conditions as well because if the border's of the item
						// where the columns are being dropped, then GetRegionFromOrigin will return null
						// if the displayPoint ( which could be the middle point on the left or the right
						// edge of the target item) happens to be one that's merged with the grid's border.
						//
						&& null == this.grid.DisplayLayout.GetColScrollRegions( false ).GetRegionFromOrigin( displayPoint.X - 2 )
						&& null == this.grid.DisplayLayout.GetColScrollRegions( false ).GetRegionFromOrigin( displayPoint.X + 2 ) 
						)
					{
						inColScrollRegion = false;
					}

					if ( null != dropLocationInfo.GridItem )
					{
						// Need To get Header Element that corresponds
						// to the location of the SwapButton.
						UIElement gridElem = this.grid.DisplayLayout.GetUIElement( false );						
						UIElement element = null;
                
						if ( null != gridElem )
						{
							element = gridElem.ElementFromPoint( mouseLoc );

							UIElement tmpElement = null;

							if ( element is HeaderUIElement )
								tmpElement = element;
							else
								tmpElement = element.GetAncestor( typeof ( HeaderUIElement ) );
														
							if ( null == tmpElement )
							{
								tmpElement = element.GetAncestor( typeof( GroupByBoxUIElement ) );

								if ( null != tmpElement )
								{
									UIElement finalElem = null;

									if ( null != dropLocationInfo.GridItem )
										finalElem = tmpElement.GetDescendant( typeof( GroupByButtonUIElement ), dropLocationInfo.GridItem );
									else if ( null != dropLocationInfo.GroupByBand )
										finalElem = tmpElement.GetDescendant( typeof( GroupByBandLabelUIElement ), dropLocationInfo.GroupByBand );

									if ( null != finalElem )
										tmpElement = finalElem;
								}
							}

							element = tmpElement;
						}

						if ( null != element )
						{                   

							// Needs to check if
							// uielement was fully visible before
							// displaying dropguides. Otherwise
							// guides were being shown passed bounds
							// of grid in some situations.
							//
							if ( element.IsFullyVisible && inColScrollRegion )
							{
								this.ShowDropGuides( dropLocationInfo, mouseLoc, itemBounds );
							}
							else
							{
								Rectangle clipRect = element.ClipRect;

								Rectangle hitTestRect = new Rectangle( displayPoint.X, displayPoint.Y, 1, 1 );
								hitTestRect.Inflate( 4, 4 );

								// Check to see if the point where the DropGuide Window
								// will be shown is visible.
								if ( ( clipRect.Contains( displayPoint ) || 
									// SSP 10/10/01
									// Added below clause to the condition because even if 
									// clipRect.Right == displayPoint.X && clipRect.Top == displayPoint.Y 
									// is true, above clipRect.Contains returns false
									//
									clipRect.Contains( new Point( displayPoint.X - 1, displayPoint.Y ) ) 
									
									// SSP 11/28/01 UWG777
									// ISFullyVisible is going to return false if the header's borders
									// are mergede with some other ui element with higher z-order because
									// clip rect won't be the same as it's real rect and therefore IsFullyVisible
									// is going to return false and also clipRect will be misleading. Instead
									// Use the hitTestRect which is rect 8x8 pixels wide, which will
									// assure that merged borders do not interfere in the manner just described.
									//
									|| !Rectangle.Intersect( clipRect, hitTestRect ).IsEmpty 
									)
									&& inColScrollRegion )
								{
									this.ShowDropGuides( dropLocationInfo, mouseLoc, itemBounds );
								}
								else
									this.HideDropGuides( );
							}
						}
					}
					else if ( inColScrollRegion )
					{
						// SSP 10/11/01
						// Show the drop guides only if the itemBounds is not empty
						//
						if ( !itemBounds.IsEmpty )
							this.ShowDropGuides( dropLocationInfo, mouseLoc, itemBounds );
						else
							this.HideDropGuides( );
					}
						// SSP 10/11/01
						// Added this else clause
						//
					else
					{
						if ( itemBounds.IsEmpty )
							this.HideDropGuides( );
					}
            
				}
				else
				{
					this.HideDropGuides( );
				}

			}

			if ( null != this.grid )
				origMouseLoc = this.grid.PointToScreen( origMouseLoc );
				

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			//if ( null != this.dragWnd )
			//	this.dragWnd.Move( origMouseLoc );
			this.MoveDragIndicator( origMouseLoc );
    
			if ( null != this.grid )
				this.grid.Update( );

		}

		internal void OnLeftButtonUp( Point mouseLoc )
		{
			if ( null != this.dragStrategy && CurrentCursor.Valid == this.CurrentCursor )
			{
				// Changed to take DropLocationInfo class to handle drops on
				// group headers as well as on empty levels.        
				DropLocationInfo dropLocationInfo = new DropLocationInfo( );
				Rectangle itemBounds = Rectangle.Empty;

				if ( this.IsLocationValid( ref mouseLoc, dropLocationInfo, ref itemBounds ) )
				{
					// SSP 7/16/03 - Fixed headers
					// Added following block of code.
					// --------------------------------------------------------------------------------
					ColScrollRegion activeCSR = null != grid ? this.grid.ActiveColScrollRegion : null;
					HeaderBase dropTargetHeader = null;
					
					if ( null != dropLocationInfo )
						dropTargetHeader = 
							dropLocationInfo.IsDropTargetColumn 
							? (HeaderBase)dropLocationInfo.ColumnHeader 
							: (HeaderBase)dropLocationInfo.GroupHeader;

					bool isDropTargetHeaderFixed = null != dropTargetHeader && null != activeCSR ? 
						dropTargetHeader.Band.IsHeaderFixed( activeCSR, dropTargetHeader ) : false;

					bool allDragHeadersNonFixed = true;
					if ( null != activeCSR && null != this.dragStrategy && null != dropLocationInfo &&
						null != this.dragStrategy.DragItems &&  this.dragStrategy.DragItems.Count > 0 )
					{
						for ( int i = 0; allDragHeadersNonFixed && i < this.dragStrategy.DragItems.Count; i++ )
						{
							HeaderBase tmpHeader = this.dragStrategy.DragItems[0] as HeaderBase;

							if ( null != tmpHeader && tmpHeader.Band.IsHeaderFixed( activeCSR, tmpHeader ) )
								allDragHeadersNonFixed = false;
						}
					}
					// --------------------------------------------------------------------------------

					this.dragStrategy.Drop( mouseLoc, dropLocationInfo, itemBounds );

					// SSP 7/16/03 - Fixed headers
					// Added following block of code.
					// --------------------------------------------------------------------------------
					if ( null != activeCSR && null != this.dragStrategy && null != dropLocationInfo &&
						null != this.dragStrategy.DragItems &&  this.dragStrategy.DragItems.Count > 0 )
					{
						if ( null != dropTargetHeader && isDropTargetHeaderFixed && allDragHeadersNonFixed )
						{
							HeaderBase scrollHeader = dropTargetHeader.Band.GetFirstNonFixedHeader( activeCSR );

							if ( null != scrollHeader )
								activeCSR.ScrollHeaderIntoView( scrollHeader, false );
						}
					}
					// --------------------------------------------------------------------------------
				}
			}

			this.TerminateMove();
		}


		internal void OnCancelMode( )
		{
			this.TerminateMove( );
		}

		//  Changed to take CDropLocationInfo class to handle drops on
		// group headers as well as on empty levels.            
		private bool TerminateMove( )
		{
			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			this.DragDropIndicatorManager.HideDragIndicator( );
			this.DragDropIndicatorManager.HideDropIndicator( );
			

			// SSP 2/22/06 BR10351
			// Dispose of the dragDropIndicatorManager otherwise Whidbey option for terminating the
			// application when the last of the form is closed won't work since the 
			// dragDropIndicatorManager may potentially be holding onto a drag effect form.
			// 
			if ( null != this.dragDropIndicatorManager )
			{
				this.dragDropIndicatorManager.Dispose( );
				this.dragDropIndicatorManager = null;
			}

			Cursor.Current = Cursors.Default;

			// SSP 4/23/04 UWG3191
			// Null out the drag strategy so if for some reason OnLeftButtonUp is called
			// again we don't process it second time.
			//
			this.dragStrategy = null;

			return true;
		}
			

		internal bool ShowDropGuides( DropLocationInfo dropLocationInfo, Point point, Rectangle itemBounds )
		{
			// SSP 10/11/01
			// If the itemBounds is empty, then return
			//
			if ( itemBounds.IsEmpty )
				return false;

			// Changed to take DropLocationInfo class to handle drops on
			// group headers as well as on empty levels.        
			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			//if ( null == this.dropGuideWnd || null == this.dragStrategy )
			//	return false;
			if ( null == this.DragDropIndicatorManager || null == this.grid )
				return false;

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			// --------------------------------------------------------------------------------------
			bool isMouseOverColumnChooser = this.grid.IsOverColumnChooser( point, false );
			if ( isMouseOverColumnChooser )
				return false;

			this.DragDropIndicatorManager.ShowDropIndicator( 
				this.grid.RectangleToScreen( itemBounds ),
				DropLocation.Before == dropLocationInfo.DropLocation ? IndicatorAlignment.Near : IndicatorAlignment.Far,
				dropLocationInfo.IsDropTargetCardLabel ? Orientation.Horizontal : Orientation.Vertical );
			
            return true;
			
			
			// --------------------------------------------------------------------------------------
		}

		internal bool HideDropGuides()
		{
			bool hidden = false;
    
			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			// ----------------------------------------------------------------
			if ( null != this.DragDropIndicatorManager )
			{
				this.DragDropIndicatorManager.HideDropIndicator( );
				hidden = true;
			}
			
			// ----------------------------------------------------------------

			this.activeItemRect = Rectangle.Empty;
    
			return hidden;   
		}

		internal bool IsLocationValid( ref Point point, DropLocationInfo dropLocationInfo, ref Rectangle itemBounds )
		{
			// Changed to take CDropLocationInfo class to handle drops on
			// group headers as well as on empty levels.        
			
			if ( null == this.dragStrategy )
				return false;

			bool valid = false;

			int itemHeight = 0;

			// SSP 6/28/05 - NAS 5.3 Column Chooser
			// We need to use top level windows for the column chooser. For that changed to 
			// using the Infragistics.Win's DragDropIndicatorManager.
			// 
			//if ( null != this.dragWnd )
			//	itemHeight = this.dragWnd.Height;
			if ( null != this.DragDropIndicatorManager )
				itemHeight = this.DragDropIndicatorManager.DragIndicatorRectangle.Height;

			Point testPoint = point;

			
			// First try the MouseLocation which is the bottom y coord of the dragged header.
			testPoint = point;

			valid = this.dragStrategy.IsLocationValid( testPoint, dropLocationInfo, ref itemBounds );

			if ( valid )
			{
				// set the point where we had a valid location match
				point = testPoint;
				return true;
			}

			if ( this.dragStrategy is DragStrategyColumn )
			{
				UIElement elem = this.grid.DisplayLayout.GetUIElement( false );

				elem = elem.GetDescendant( typeof( GroupByBoxUIElement ) );

				if ( null != elem && elem.Rect.Contains( point ) )
					return false;
			}

			// MRS 4/12/05 - Only do this if we're not in a RowLayout drag. 
			if (null == this.grid.gridBagLayoutDragStrategy)
			{
				// Then try the Middle of the Dragged header
				testPoint.Y -= ( itemHeight / 2 );
			}

			valid = this.dragStrategy.IsLocationValid( testPoint, dropLocationInfo, ref itemBounds );

			if ( valid )
			{
				// set the point where we had a valid location match
				point = testPoint;
				return true;
			}


			// SSP 10/5/01
			// This was causing problems with dragging. It was showing arrows
			// when unexpected because here we are checking for the top of the
			// drag bitmap to check for target.
			// We don't need to check for the top of the header item.
			// If the user wants to make a drop, he will have to make sure
			// the drag item is halfway inside the drop target.
			//
			

			return valid;
		}

		
		// SSP 12/21/01
		//internal Point CalcDropGuideDisplayPoint( Rectangle itemBounds, DropLocation dropLoc )
		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//internal Point CalcDropGuideDisplayPoint( Rectangle itemBounds, DropLocationInfo dropLocationInfo )
		internal static Point CalcDropGuideDisplayPoint( Rectangle itemBounds, DropLocationInfo dropLocationInfo )
		{
			DropLocation dropLoc = dropLocationInfo.DropLocation;

			Point displayPoint = Point.Empty;

			// SSP 12/21/01
			// In case of card labels, drop guides are drawn horizontally
			//
			if ( dropLocationInfo.IsDropTargetCardLabel )
			{
				displayPoint.X = itemBounds.X;

				if ( DropLocation.Before == dropLoc )
					displayPoint.Y = itemBounds.Top;
				else
					displayPoint.Y = itemBounds.Bottom;
			}
			else
			{
				// Whether the DropGuides are being show before or after
				// the y pos is always the same.    
				displayPoint.Y = itemBounds.Top;

				if ( DropLocation.Before == dropLoc  )
				{    
					displayPoint.X = itemBounds.Left;
				}
				else if ( DropLocation.After == dropLoc )
				{
					displayPoint.X = itemBounds.Right;
				}
			}

			return displayPoint;
		}

		// SSP 6/27/05 - NAS 5.3 Column Chooser
		// We need to use top level windows for the column chooser. For that changed to 
		// using the Infragistics.Win's DragDropIndicatorManager.
		// 
		

	}

}
