#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using Infragistics.Shared;
	using Infragistics.Win;

    /// <summary>
    ///    Summary description for VisibleHeader.
    /// </summary>
    public sealed class VisibleHeader : SubObjectBase
    {
        private int     origin  = 0;
        private Infragistics.Win.UltraWinGrid.HeaderBase 
                        header = null;

		ColScrollRegion  colScrollRegion = null;

        internal VisibleHeader( ColScrollRegion  colScrollRegion )
        {
			this.colScrollRegion = colScrollRegion;
        }
		internal void Initialize( Infragistics.Win.UltraWinGrid.HeaderBase header,
                                  int origin )
        {
            // unhook from old header's notification list
            //
			if ( null != this.header )
				this.header.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

            this.header             = header;
            this.origin             = origin;

            // hook into new header's notification list
            //
			if ( null != this.header )
				this.header.SubObjectPropChanged += this.SubObjectPropChangeHandler;
        }

		/// <summary>
		/// Returns the Header object associated with the object. This property is read-only at run-time. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">A Header object represents a column or group header that specifies information about the column or group, and can also serve as the interface for functionality such as moving, swapping or sorting the column or group. Group headers have the added functionality of serving to aggregate multiple columns under a single heading.</p> 
		/// <p class="body">The <b>Header</b> property provides access to the header that is associated with an object. The <b>Header</b> property provides access to the header that is associated with an object. In some instances, the type of header may be ambiguous, such as when accessing the <b>Header</b> property of a UIElement object. You can use the <b>Type</b> property of the Header object returned by the <b>Header</b> property to determine whether the header belongs to a column or a group.</p>
		/// </remarks>
        public Infragistics.Win.UltraWinGrid.HeaderBase Header
        {
            get
            {
                return this.header;
            }
        }

        /// <summary>
        /// The origin of this header in client coordinates.
        /// </summary>
        public int Origin
        {
            get
            {
                return this.origin;
            }
        }

        /// <summary>
        /// The extent of this header in client coordinates.
        /// </summary>
        public int Extent
        {
            get
            {
                return this.header.Extent;
            }
        }

		/// <summary>
		/// Called when a property has changed on a header
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// pass this on to the Col scroll region's method
			//
			this.colScrollRegion.OnHeaderPropChange( this, propChange );
		}

    }
}
