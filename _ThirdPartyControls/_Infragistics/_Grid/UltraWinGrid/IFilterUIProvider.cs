#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Infragistics.Win.UltraWinGrid
{
    /// <summary>
    /// An interface for providing an extended filtering UI mechanism.     
    /// </summary>
    public interface IFilterUIProvider
    {
        /// <summary>
        /// Closes the UI associated with the provider, if shown.
        /// </summary>        
        /// <param name="applyChanges">True if any changes made by the user within the UI should be applied.</param>
        void Close(bool applyChanges);

        /// <summary>
        /// Returns the editor that should be used in the filter cell when the FilterOperandStyle is
        /// set to Default or FilterUIProvider.
        /// </summary>
        /// <param name="column">The column for which an editor in the filter row is being requested.</param>
        /// <returns>The editor that should be used, or null if the grid should use the default editor.</returns>
        /// <remarks>
        /// <p class="note"><b>Note: </b>The grid will not cache the editor returned from this method,
        /// so it is up to the implementor to use a caching mechanism, if applicable.</p>
        /// </remarks>
        EmbeddableEditorBase GetFilterCellEditor(UltraGridColumn column);

        /// <summary>
        /// Shows the associated UI for performing a filtering operation.
        /// </summary>
        /// <param name="columnFilter">The column filter instance associated with the column being filtered.</param>
        /// <param name="rows">The collection of rows that the filtering associated with the filtering operation.</param>
        /// <param name="exclusionRect">The rectangle that must remain visible and not be obscured by the UI.</param>
        /// <param name="values">The list of cell values that the user can select for comparison.</param>
        /// <remarks>
        /// <p class="note"><b>Note: </b>It is the responsibility of the implementer to apply the filtering to the column.</p>
        /// </remarks>
        void Show(ColumnFilter columnFilter, RowsCollection rows, Rectangle exclusionRect, ValueList values);
    }
}
