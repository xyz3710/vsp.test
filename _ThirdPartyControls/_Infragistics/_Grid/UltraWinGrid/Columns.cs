#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Collections;
    using System.Data;
	using System.Drawing;
	using System.Windows.Forms;
    using Infragistics.Shared;
    using Infragistics.Shared.Serialization;
    using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Diagnostics;
	using System.ComponentModel;
	using System.Security.Permissions;


	/// <summary>
	/// Returns a reference to a collection of Column objects. This property 
	/// is read-only at run-time. This property is not available at design-time.
	/// </summary><remarks><para>This property returns a reference to a 
	/// collection of UltraGridColumn objects that can be used to retrieve references 
	/// to the Column objects that, for the Band and Group objects, belong to a band 
	/// or a group, respectively, or for the Selected object, that are 
	/// currently selected. You can use this reference to access any of the 
	/// returned collection's properties or methods, as well as the properties or 
	/// methods of the objects within the collection.</para><para>For the Selected 
	/// object, as columns are selected and deselected, their corresponding 
	/// Column objects are added to and removed from the SelectedCols collection 
	/// returned by this property. When a column is selected or deselected, the 
	/// BeforeSelectChange event is generated.</para><para>The Count property of 
	/// the returned collection can be used to determine the number of columns 
	/// that either belong to a band or a group or are currently selected.</para></remarks>

	[ Serializable() ]
	[ ListBindable(false )]
    public class ColumnsCollection : KeyedSubObjectsCollectionBase, 
									 ISerializable
    {
        private UltraGridBand band;
		private int initialCapacity = 5;

		// SSP 4/5/05 - Optimizations
		// Used by the UltraGridColumn.Index.
		//
		internal ArrayList listInternal = null;

        // MRS 5/14/2009 - TFS17620
        // Ripped this out because it's causing performance (TFS17620) and serilalization (TFS17457) issues.
        //
        //// MBS 10/7/08 - TFS8698        
        //private bool hasSuspendedMetricsCalculation;        

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="band">The UltraGridBand to which the columns belong.</param>
        public ColumnsCollection( UltraGridBand band )
        {
            this.band = band;

			// SSP 4/5/05 - Optimizations
			// Added listInternal.
			//
			this.listInternal = this.List;
        }

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected ColumnsCollection(SerializationInfo info, StreamingContext context)
		{
			// SSP 4/5/05 - Optimizations
			// Added listInternal.
			//
			this.listInternal = this.List;

			//Needs to be set
			this.band = null;

			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( UltraGridColumn ) )
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					UltraGridColumn item = (UltraGridColumn)Utils.DeserializeProperty( entry, typeof(UltraGridColumn), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd(item);
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							//this.initialCapacity = Math.Max( this.initialCapacity, (int)entry.Value );
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/17/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.initialCapacity = Math.Max(this.initialCapacity, (int)Utils.DeserializeProperty( entry, typeof(int), this.initialCapacity ));
							//this.initialCapacity = Math.Max(this.initialCapacity, (int)DisposableObject.ConvertValue( entry.Value, typeof(int) ));
							break;
						}

						// JJD 1/31/02
						// De-Serialize the tag property
						//
						case "Tag":
						{
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						}

						default:
						{
							Debug.Assert( false, "Invalid entry in BandsCollection de-serialization ctor" );
							break;
						}				
					}
				}
			}
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			//all values that were set are now save into SerializationInfo
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Count", this.Count );
			//info.AddValue( "Count", this.Count );

			// add each layout in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, i.ToString(), this[i] );
				//info.AddValue( i.ToString(), this[i] );
			}

			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}

 
        /// <summary>
        /// Called when this object is disposed of
        /// </summary>
        protected override void OnDispose() 
        {
        
            foreach ( Infragistics.Win.UltraWinGrid.UltraGridColumn column in this )
            {
                // remove the prop change notifications
                //
                column.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

                // call dispose on the band object
                //
                column.Dispose();
            }

            base.InternalClear();
        }

		/// <summary>
		/// Returns false
		/// </summary>
        public override bool IsReadOnly { get { return false; } }

		internal bool AreThereAnyOtherVisibleColumns( Infragistics.Win.UltraWinGrid.UltraGridColumn column )
		{
			for ( int i = 0; i < this.Count; i++ )
			{
				// JJD 10/04/01 -UWG482
				// If the column is the passed in column or the
				// column is hidden continue
				//
				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( this[i] == column || this[i].Hidden )
				UltraGridColumn currentColumn = this[ i ];

				// MD 1/20/09 - Groups in RowLayout
				// This code no longer applies. HiddenResolve will do the hidden check as well as check whether the column is within a hidden group.
				//if ( currentColumn == column || currentColumn.Hidden )
				//    continue;
				//
				//// JJD 10/04/01 -UWG482
				//// If groups are visible then if this column's group is
				//// hidden  continue
				////
				//// SSP 3/4/04
				//// Changed the behavior to give row-layout higher priority than the groups.
				//// Meaning if there are groups and they turn set UseRowLayout to true, we
				//// will ignore groups and use the row-layout functionality. So instead of
				//// using HasGroups, use GroupsDisplayed because that's the final say in
				//// whether groups are being displayed or not.
				////
				////if ( this.band.HasGroups )
				//if ( this.band.GroupsDisplayed )
				//{
				//    // MD 8/15/07 - 7.3 Performance
				//    // Prevent calling expensive getters multiple times
				//    //if ( this[i].Group == null ||
				//    //     this[i].Group.Hidden )
				//    if ( currentColumn.Group == null ||
				//         currentColumn.Group.Hidden )
				//        continue;
				//}
				if ( currentColumn == column || currentColumn.HiddenResolved )
					continue;

				// JJD 10/04/01 -UWG482
				// Since we found a column (other than the passed in column)
				// that will be visible return true
				//
				return true;
			}

			// JJD 10/04/01 -UWG482
			// Since we didn't find a column (other than the passed in column)
			// that will be visible return false
			//
			return false;
		}

		/// <summary>
		/// Associated band object read-only
		/// </summary>
        public UltraGridBand Band
        {
            get
            {
                return this.band;
            }
        }

		/// <summary>
		/// indexer
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridColumn this[ int index ] 
        {
            get
            {
                return (UltraGridColumn)base.GetItem( index );
            }
        }

        
		/// <summary>
		/// indexer (by string key)
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridColumn this[ String key ] 
        {
            get
            {
                return (UltraGridColumn)base.GetItem( key );
            }
        }

        /// <summary>
        /// Abstract property that specifies the initial capacity
        /// of the collection
        /// </summary>
        protected override int InitialCapacity
        {
            get
            {
                return initialCapacity;
            }
        }
         /// <summary>
        /// The collection as an array of objects
        /// </summary>
        public override object[] All 
        {
			// AS 1/8/03 - fxcop
			// Properties should not be write only - even
			// though we know it wasn't anyway
			get { return base.All; }
			set 
            {
				throw new NotSupportedException();
            }
        }


	    /// <summary>
	    /// Called when a property has changed on a sub object
	    /// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
        protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
        {
            // pass the notification along to our listeners
            //
            this.NotifyPropChange( PropertyIds.Columns, propChange );
        }

		internal int MinHeightRequiredByColumns
        {
            get
            {
                int maxHeight = 0;

                // loop thru each column to fint the one with the biggest font
                //
                for ( int i = 0; i < this.Count; i++ )
                {
                    Infragistics.Win.UltraWinGrid.UltraGridColumn column = this[i];

                    // NickC 9/15/00 : By resolving the style (based on data type) we can
                    //                 be more accurate about what the row height is
					// SSP 5/1/03 - Cell Level Editor
					// Added Editor and ValueList properties off the cell so the user can set the editor
					// and value list on a per cell basis.
					//
                    //Infragistics.Win.UltraWinGrid.ColumnStyle style = column.StyleResolved;
					Infragistics.Win.UltraWinGrid.ColumnStyle style = column.GetStyleResolved( null );

                    switch ( style )
                    {
                        case Infragistics.Win.UltraWinGrid.ColumnStyle.DropDown :
                        case Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownList :
                        case Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownValidate :
                        case Infragistics.Win.UltraWinGrid.ColumnStyle.DropDownCalendar :
                            maxHeight = System.Math.Max( 16, maxHeight ); // min height that a dropdown looks correct
                            break;
                    }
                }

                return maxHeight;
            }
        }

        /// <summary>
        ///    <para>
        ///       Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridColumn'/>
        ///       to the collection.
        ///    </para>
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        ///    The <paramref name="Column"/>argument was <see langword='null'/>.
        /// </exception>
        /// <returns>The newly added column</returns>
        public Infragistics.Win.UltraWinGrid.UltraGridColumn Add() 
        {
            UltraGridColumn column = new UltraGridColumn( this.band );

			// hook up the prop change notifications
            //
            column.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			
			// SSP 6/25/03 UWG2407
			// Notify after adding the column to the collection.
			// Moved this below the call to InternalAdd below.
			//
			//this.NotifyPropChange( PropertyIds.Add );

			this.InternalAdd( column );

			// SSP 6/25/03 UWG2407
			// Notify after adding the column to the collection.
			// Moved this from before the above call to InternalAdd.
			//
			this.NotifyPropChange( PropertyIds.Add );

			return column;
        }

		/// <summary>
		/// Add method required during de-serialization. 
		/// </summary>
        /// <param name="obj">The object to add to the collection.</param>
        /// <returns>The position into which the new element was inserted.</returns>
		/// <remarks>Should not be called outside InitializeComponent.</remarks>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
        public int Add( object obj ) 
        {
			if ( obj == null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_113") );
			
			Infragistics.Win.UltraWinGrid.UltraGridColumn column = obj as Infragistics.Win.UltraWinGrid.UltraGridColumn;

			if ( column == null )
				throw new ArgumentException( SR.GetString("LER_Exception_316", obj.GetType().Name) );

			if ( column.Band != null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_115") );

			return this.InternalAdd( column );
        }

        // AS 8/11/04
        // Whidbey uses the wrong overload of the Add method
        // so instead let it use the AddRange method.
        //
        /// <summary>
        /// Add method required during de-serialization. 
        /// </summary>
        /// <remarks>Should not be called outside InitializeComponent.</remarks>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void AddRange(object[] columns)
        {
            if (columns == null)
                throw new ArgumentNullException();

            foreach (object column in columns)
                this.Add(column);
        }

        /// <summary>
		/// Returns false
		/// </summary>
		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}


		/// <summary>
		/// Adds a new, unbound column to the columns collection.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		/// <returns>The newly added column.</returns>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn Add( String key ) 
        {
			// SSP 8/12/04
			// Instead of duplicating the code call the overload that takes in the caption.
			// Both of these methods are doing the same thing exception for setting the
			// caption on the header.
			// Commented out the original code and added the call to the overload of Add
			// that takes in a key and a caption.
			//
			return this.Add( key, null );
			
        }

		/// <summary>
		/// Inserts a new unbound column into the column collection
		/// </summary>
		/// <param name="index">The index into which the new column will be inserted.</param>
		/// <param name="key">The key of the new column.</param>
		public UltraGridColumn Insert ( int index, string key )
		{
			this.ValidateKeyDoesNotExist( key );

			UltraGridColumn column = null;
			if( key == null )
				column = new UltraGridColumn( this.band );
			else
				column = new UltraGridColumn( this.band, key );

			// Add a listener for property changes.
			column.SubObjectPropChanged += this.SubObjectPropChangeHandler;
			
			this.InternalInsert( index, column );

			return column;
		}

        /// <summary>
        /// Adds a new, unbound column to the columns collection.
        /// </summary>
        /// <param name="key">The key of the new column.</param>
        /// <param name="caption">The <see cref="HeaderBase.Caption"/> of the new column.</param>
        /// <returns>The newly added column.</returns>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn Add( string key, string caption ) 
		{
			this.ValidateKeyDoesNotExist( key );

			UltraGridColumn column = new UltraGridColumn( this.band, key );
            
			column.Header.Caption = caption;

			// hook up the prop change notifications
			//
			column.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// SSP 6/25/03 UWG2407
			// Notify after adding the column to the collection.
			// Moved this below the call to InternalAdd below.
			//
			//this.NotifyPropChange( PropertyIds.Add );
			
			this.InternalAdd( column );

			// SSP 6/25/03 UWG2407
			// Notify after adding the column to the collection.
			// Moved this from before the above call to InternalAdd.
			//
			this.NotifyPropChange( PropertyIds.Add );

			return column;
		}
		internal int InternalAdd( UltraGridColumn column ) 
        {
            // hook up the prop change notifications
            //
            column.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// SSP 8/14/03 - Optimizations
			//
			this.BumpColumnsVersion( );

            int index = this.InternalAdd((IKeyedSubObject) column );

			// SSP 8/12/04 - UltraCalc
			// Notify the calc manager.
			//
			this.AddRemoveColumnFromCalcManager( column, true );

			return index;
        }


		internal void InternalInsert( int index, UltraGridColumn column )
		{
			base.InternalInsert( index, (IKeyedSubObject)column );

			// SSP 8/14/03 - Optimizations
			//
			this.BumpColumnsVersion( );
			
			// hook up the prop change notifications
			//
			column.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// SSP 1/20/05 BR01813
			// Fire Add notification. This will cause the band to fire InitializeRow on the rows.
			// We do this when a column is removed in InternalRemoveColumn method.
			//
			this.NotifyPropChange( PropertyIds.Add );

			// SSP 8/12/04 - UltraCalc
			// Notify the calc manager.
			//
			this.AddRemoveColumnFromCalcManager( column, true );
		}


        /// <summary>
        /// The total number of bound columns in the collection.
        /// Note: they are always first in the collection before
        /// any unbound columns.
        /// </summary>
        public int BoundColumnsCount
        {
            get
            {
                return this.Count - this.UnboundColumnsCount;
            }
        }

		// SSP 12/5/03 UWG2775
		// Added ChapteredColumnsCount method.
		//
		internal int ChapteredColumnsCount
		{
			get
			{
				int count = 0;

				for ( int i = 0, c = this.Count; i < c; i++ )
				{
					if ( this[i].IsChaptered )
						count++;
				}

				return count;
			}
		}
		
        /// <summary>
        /// The total number of unbound columns in the collection.
        /// Note: they always follow the bound columns
        /// </summary>
        public int UnboundColumnsCount
        {
            get
            {
                int unboundColumnsCount = 0;

                for( int i = this.Count - 1 ; i >= 0; i-- )
                {
                    if ( this[i].IsBound )
                        break;
                    
                    unboundColumnsCount++;
                }
                
                return unboundColumnsCount;
            }
        }

        /// <summary>
        /// Remove column from collection
        /// </summary>
        /// <param name="index">The index of the column to remove.</param>
        public void Remove( int index ) 
        {
            this.Remove ( this[ index ] );
        }

        /// <summary>
        /// Remove column from collection
        /// </summary>
        public void Remove( String key ) 
        {
            this.Remove ( this[ key ] );
        }

  

		/// <summary>
		/// Remove a column from the collection
		/// </summary>
		/// <param name="index">Index to be removed</param>
		public void	RemoveAt( int index )
		{
			if ( index > this.Count || index < 0 )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_116") );
			
			Infragistics.Win.UltraWinGrid.UltraGridColumn column = this[ index ];

			if ( column.IsBound )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_117") );

			this.InternalRemoveAt(index);			

			// SSP 8/14/03 - Optimizations
			//
			this.BumpColumnsVersion( );

			if ( column != null )
			{
				// Remove a listener for property changes.
				column.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			
				// JJD 11/29/01 - UWG782
				// Notify listeners
				//
				this.NotifyPropChange( PropertyIds.Remove );

				// SSP 8/12/04 - UltraCalc
				// Notify the calc manager.
				//
				this.AddRemoveColumnFromCalcManager( column, false );
			}
		}

				
        /// <summary>
        /// Clears the collection
        /// </summary>
        internal void Clear() 
        {
        
            foreach ( Infragistics.Win.UltraWinGrid.UltraGridColumn column in this )
            {
                // remove the prop change notifications
                //
                column.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// SSP 8/12/04 - UltraCalc
				// Notify the calc manager.
				//
				this.AddRemoveColumnFromCalcManager( column, false );
            }

            this.InternalClear();

			// SSP 8/14/03 - Optimizations
			//
			this.BumpColumnsVersion( );
        }


		// SSP 8/14/03 - Optimizations
		//
		internal void BumpColumnsVersion( )
		{
			if ( null != this.Band )
				this.Band.BumpColumnsVersion( );
		}


        /// <summary>
        /// Clears the unbound columns in the collection
        /// </summary>
        public void ClearUnbound() 
        {
			if ( this.Count < 1 ) return;

			bool columnsRemoved = false;

            // since unbound columns are always at the end
            // of the collection we can remove the last
            // column until we reach a bound column
            //
			// MD 8/15/07 - 7.3 Performance
			// Prevent calling expensive getters multiple times
			//while ( this.Count > 0 &&
			//        !this[ this.Count - 1 ].IsBound )
			//{
			//    UltraGridColumn column = this[ this.Count - 1 ];
			UltraGridColumn column;

			while ( this.Count > 0 &&
				   !( column = this[ this.Count - 1 ] ).IsBound )
			{
                // remove the prop change notifications
                //
				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
                //this[ this.Count - 1 ].SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				column.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				//RobA UWG464 10/12/01
				//bump the version so that the groups columns get removed
				//
				this.Band.BumpColumnsVersion();

				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//this.InternalRemove( this[ this.Count - 1 ] );
				this.InternalRemove( column );

				columnsRemoved = true;

				// SSP 8/12/04 - UltraCalc
				// Notify the calc manager.
				//
				this.AddRemoveColumnFromCalcManager( column, false );
            }

			if ( columnsRemoved )
				this.NotifyPropChange( PropertyIds.ClearUnbound, null );
        }
 
		internal void ClearDrawCache()
		{
			// JJD 11/06/01
			// Clear each column's cache
			//
			for ( int i=0; i< this.Count; ++i )
			{
				this[i].ClearDrawCache();
			}
		}
    
		/// <summary>
		/// IEnumerable Interface Implementation
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public ColumnEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new ColumnEnumerator(this);
        }

		
		internal int MaxHeaderFontHeight
		{
			get
			{
				//RobA 10/23/01 implemented
				//
				int maxHeight = 0;

				// loop thru each column to find the one with the biggest font
				//
				for ( int i=0; i < this.Count; ++i )
				{
					int fontHeight = 0;

					AppearanceData appData = new AppearanceData();

					// SSP 10/30/01 UWG604
					// Resolve the appearance of the header, not the column
					// 
					
					this[i].Header.ResolveAppearance( ref appData, AppearancePropFlags.FontData );

					// JJD 12/14/01
					// Moved font height logic into CalculateFontHeight
					//
					fontHeight = this.Band.Layout.CalculateFontHeight( ref appData );

					maxHeight = Math.Max( fontHeight, maxHeight );
				}

				return maxHeight;

			}
		}

		// MD 7/26/07 - 7.3 Performance
		// Thsi always returns 0 and ahs been removed
		#region Removed

		//internal int MaxHeaderPictureHeight
		//{
		//    get
		//    {
		//        //TODO: implement MaxHeaderPictureHeight
		//        return 0;
		//    }
		//}

		#endregion Removed

		// SSP 7/16/04 UWG3496
		// InitAll is not needed anymore because of the fix for UWG3496.
		// Commented out InitAll method.
		//
		

		internal void InitializeFrom( ColumnsCollection source, PropertyCategories categories )
		{
			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			// if we are attached to the main layout we can't add or remove bound 
			// columns since the rowset dictates the band structure
			//
			// SSP 5/7/03 - Export Functionality
			//
			//if ( this.band.Layout.IsDisplayLayout || this.band.Layout.IsPrintLayout )
			if ( this.band.Layout.IsDisplayLayout || this.band.Layout.IsPrintLayout || this.band.Layout.IsExportLayout )
			{
				// SSP 7/16/04 UWG3496
				// Commented out the following if block (including the else line).
				// We want to execute the same code below regardless of whether the source 
				// layout has any columns or not.
				//
				
				// SSP 7/16/04 UWG3496
				// We need to reset the non-matching columns as well as remove any non-matching
				// unbound columns when loading from a layout.
				//
				Hashtable matchedColumns = new Hashtable( );


				for ( int index = 0; index < source.Count; index++ )
				{
					bool columnFound = false;

					// MD 8/15/07 - 7.3 Performance
					// Cached column - Prevent calling expensive getters multiple times
					UltraGridColumn sourceColumn = source[ index ];

					for ( int i = 0; i < this.Count; i++ )
					{
						// Cell the column's CanInitializeFrom method to see if the column matches
						//
						// MD 8/15/07 - 7.3 Performance
						// Prevent calling expensive getters multiple times
						//if ( this[i].CanInitializeFrom( source[index] ) )
						UltraGridColumn currentColumn = this[ i ];

						if ( currentColumn.CanInitializeFrom( sourceColumn ) )
						{
							// the column's match logically so call the InitializeFrom
							// method of the target column
							//
							// MD 8/15/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//this[i].InitializeFrom ( source[index], categories );
							currentColumn.InitializeFrom( sourceColumn, categories );
                    
							// SSP 7/16/04 UWG3496
							// We need to reset the non-matching columns as well as remove any non-matching
							// unbound columns when loading from a layout.
							//
							// MD 8/15/07 - 7.3 Performance
							// Prevent calling expensive getters multiple times
							//matchedColumns[ this[i] ] = null;
							matchedColumns[ currentColumn ] = null;

							columnFound = true;
							break;
						}
					}

					// if the source column wasn't found and it is an
					// unbound column then append it to our collection
					//                  
					// MD 8/15/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( !columnFound && !source[index].IsBound )
					//{
					//    this.CreateAndAppendColumn( source[index], categories );
					if ( !columnFound && !sourceColumn.IsBound )
					{
						this.CreateAndAppendColumn( sourceColumn, categories );

						// SSP 7/16/04 UWG3496
						// We need to reset the non-matching columns as well as remove any non-matching
						// unbound columns when loading from a layout.
						//
						if ( this.Count > 0 )
							matchedColumns[ this[ this.Count - 1 ] ] = null;
					}

				}

				// SSP 7/16/04 UWG3496
				// We need to reset the non-matching columns as well as remove any non-matching
				// unbound columns when loading from a layout.
				//
				// ----------------------------------------------------------------------------
				for ( int i = this.Count - 1; i >= 0; i-- )
				{
					UltraGridColumn column = this[i];

					if ( ! matchedColumns.ContainsKey( column ) )
					{
						if ( column.IsBound )
						{
                            // MRS 3/16/07 - BR21042
                            // Moved up from below. This needs to happen before the column.Hidden 
                            // is set. 
                            column.Reset();

							// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
							// 
							UltraGridLayout layout = null != this.Band ? this.Band.Layout : null;
							if ( null != layout && layout.IsApplyingLayout 
								&& NewColumnLoadStyle.Hide == layout.NewColumnLoadStyleInEffect )
								column.Hidden = true;

                            // MRS 3/16/07 - BR21042
                            // Moved up. This needs to happen before the column.Hidden 
                            // is set. 
                            //
                            //column.Reset( );

							if ( column.IsHeaderAllocated )
								column.Header.InternalResetVisiblePos( );
						}
						else
						{
							// Remove the unbound column.
							//
							this.RemoveAt( i );
						}
					}
				}
				// ----------------------------------------------------------------------------

				//}

				return;
			}


			// we are not attached to the main layout so first
			// clear our current collection and then copy over
			// the source's structure 
			//
			this.Clear();

			// if there are no bands on the source just return
			//
			if ( source.Count < 1 )
				return;
    
			// copy each column
			//
			for ( int i = 0; i < source.Count; i++ )
			{
				this.CreateAndAppendColumn( source[i], categories );
			}
		}

		internal void CreateAndAppendColumn( UltraGridColumn source, PropertyCategories categories )
		{
			// create a new column with the same key as the source
			//
			UltraGridColumn newColumn = new UltraGridColumn(this.band, source.Key);

			// init the new column from the source column
			//
			newColumn.InitializeFrom( source, categories );

			// JJD 10/26/01
			// Instead of calling the Add method (which doesn't exist
			// hook up notifications and call InternalAdd
			//
			//Add it to the collection
			//this.Add( NewColumn );

			// hook up the prop change notifications
            //
            newColumn.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			this.InternalAdd( newColumn );
		}

		// SSP 12/11/02 UWG1813
		// Added InternalRemoveColumn method.
		//
		internal void InternalRemoveColumn( UltraGridColumn column )
		{
			// Remove the column
			//
			this.InternalRemove( column );

			// SSP 8/14/03 - Optimizations
			//
			this.BumpColumnsVersion( );

			// Notify the prop change
			//
			this.NotifyPropChange( PropertyIds.Remove );

			// SSP 8/12/04 - UltraCalc
			// Notify the calc manager.
			//
			this.AddRemoveColumnFromCalcManager( column, false );
		}

		/// <summary>
		/// Remove a column from the collection
		/// </summary>
		/// <param name="value">Must be a Column object</param>
		public void	Remove( object value )
		{
			if ( value == null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_118") );
			
			Infragistics.Win.UltraWinGrid.UltraGridColumn column = value as Infragistics.Win.UltraWinGrid.UltraGridColumn;

			if ( column == null )
				throw new ArgumentException( SR.GetString("LER_Exception_339", value.GetType().Name ) );

			if ( column.IsBound )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_117") );


			// Remove a listener for property changes.
			column.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalRemove( column );

			// SSP 8/14/03 - Optimizations
			//
			this.BumpColumnsVersion( );
			
			// JJD 11/29/01 - UWG782
			// Notify listeners
			//
			this.NotifyPropChange( PropertyIds.Remove );

			// SSP 8/12/04 - UltraCalc
			// Notify the calc manager.
			//
			this.AddRemoveColumnFromCalcManager( column, false );
		}

		internal void InitBand( Infragistics.Win.UltraWinGrid.UltraGridBand band, bool calledFromEndInit )
		{
			this.band =  band;
			
			for( int i = 0; i < this.Count; i++ )
			{
				this[i].InitBand( this.band, calledFromEndInit );
			}
		}

		internal int TotalBoundColumns
		{
			get
			{
				// start with the total count of all columns
				//
				int nCount = this.Count;
    
				// iterate over the columns in reverse until we find the first(last) bound 
				// column, since all bound columns are before the unbound columns in the collection
				//
				for( int index = this.Count; index >= 0; index-- )
				{
					// we can stop when we encounter the first bound column
					//
					if ( this[index].IsBound )
						break;
        
					// decrement the total count for this unbound column
					//
					nCount--;
				}

				return nCount;
			}
		}

		internal object Clone()
		{
			ColumnsCollection columnCollect = new ColumnsCollection( this.band );
			columnCollect.InitializeFrom( this, PropertyCategories.All );
			return columnCollect;
		}

		// SSP 8/12/04 - UltraCalc
		// Added AddRemoveColumnFromCalcManager method.
		//
		internal void AddRemoveColumnFromCalcManager( UltraGridColumn column, bool add )
		{
			// Only send notifications if this columns collection is the one that the band
			// is holding a reference to. If this is a clone columns collection then don't
			// notify.
			//
			if ( null != this.Band && this == this.Band.Columns )
			{
				if ( add )
					column.FormulaHolder.AddReferenceToCalcNetwork( );
				else
					column.FormulaHolder.RemoveReferenceFromCalcNetwork( );
			}
		}

		#region FindColumn

		// SSP 3/24/05 BR02996
		// Added FindColumn for getting the column associated with a property descriptor.
		//
		internal UltraGridColumn FindColumn( PropertyDescriptor propertyDescriptor )
		{
			for ( int i = 0, count = this.Count; i < count; i++ )
			{
				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( propertyDescriptor == this[i].PropertyDescriptor )
				//    return this[i];
				UltraGridColumn column = this[ i ];

                // MBS 10/6/08 - TFS8567
                // We should be using Equals instead
                //
                //if ( propertyDescriptor == column.PropertyDescriptor )
                if (propertyDescriptor.Equals(column.PropertyDescriptor))
					return column;
			}

			return null;
		}

		#endregion // FindColumn

		#region GetFirstNonChapteredColumn

		// SSP 7/11/05 - NAS 5.3 Empty Rows
		// 
		internal UltraGridColumn GetFirstNonChapteredColumn( )
		{
			for ( int i = 0, count = this.Count; i < count; i++ )
			{
				UltraGridColumn column = this[i];
				if ( ! column.IsChaptered )
					return column;
			}

			return null;
		}

		#endregion // GetFirstNonChapteredColumn

        // MRS 5/14/2009 - TFS17620
        // Ripped this out because it's causing performance (TFS17620) and serilalization (TFS17457) issues.
        //
        #region Old Code for TFS898
//        // MBS 10/7/08 - TFS8698
//        // Added a way to suspend and resume metrics recalculations when setting the VisiblePosition
//        // property of a column, since there are cases where we set this propery in batch 
//        // (i.e. deserializing/initializing from a different collection), and we don't have
//        // a need for recalculating the metrics for each item       
//        //
//        #region RecalculateMetrics

//#if DEBUG
//        /// <summary>
//        /// Recalculates the metrics on the ColScrollRegions if recalculations haven't been suspended.
//        /// </summary>
//#endif
//        internal void RecalculateMetrics()
//        {
//            if (this.hasSuspendedMetricsCalculation)
//                return;

//            if (this.Band != null && this.Band.Layout != null)
//            {
//                UltraGridLayout layout = this.Band.Layout;

//                // MBS 12/5/08
//                // Don't initialize the metrics if we're within the InitializeLayout method,
//                // since a bunch of initializations could be occuring at this point, including
//                // setting the VisiblePosition of the column.  This could lead to prematurely
//                // initializing the metrics, which will cause InitializeRow to fire too early
//                // (i.e. before an unbound column has been added in InitializeRow)
//                if (layout.Grid != null && layout.Grid.InInitializeLayout)
//                    return;

//                if (layout.ColScrollRegionsAllocated)
//                    layout.ColScrollRegions.InitializeMetrics();
//            }
//        }
//        #endregion //RecalculateMetrics
//        //
//        #region ResumeMetricsRecalculation

//        internal void ResumeMetricsRecalculation()
//        {
//            this.hasSuspendedMetricsCalculation = false;
            
//            // MRS 5/13/2009 - TFS17457
//            // We should not be forcing a recalculation here. 
//            //
//            //this.RecalculateMetrics();
//        }
//        #endregion //ResumeMetricsRecalculation
//        //
//        #region SuspendMetricsRecalculation

//        internal void SuspendMetricsRecalculation()
//        {
//            this.hasSuspendedMetricsCalculation = true;
//        }
        //        #endregion //SuspecdMetricsRecalculation
        #endregion //Old Code for TFS898
    }

    /// <summary>
    /// Enumerator for the ColumnsCollection
    /// </summary>
    public class ColumnEnumerator: DisposableObjectEnumeratorBase
    {   
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="columns"></param>
        public ColumnEnumerator( ColumnsCollection columns ) : base( columns )
        {
        }

       
		/// <summary>
		/// Type-safe version of Current
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridColumn Current 
        {
            get
            {
                return (UltraGridColumn)((IEnumerator)this).Current;
            }
        }

    }
}
