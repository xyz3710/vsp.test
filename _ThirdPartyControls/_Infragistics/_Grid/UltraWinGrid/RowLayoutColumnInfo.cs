#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;
using Infragistics.Win.Layout;
using System.Collections.Generic;

// SSP 1/30/03 - Row Layout Functionality
// Added RowLayoutColumnInfo file.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region RowLayoutColumnInfo

	/// <summary>
	/// Class for holding row layout information for a column.
	/// </summary>
	[ Serializable( ), TypeConverter( typeof( RowLayoutColumnInfo.RowLayoutColumnInfoTypeConverter ) ) ]
	public class RowLayoutColumnInfo : KeyedSubObjectBase, ISerializable, IGridBagConstraint, ILayoutItem
	{
		#region Constants

		/// <summary>
		/// This constant can be assigned to OriginX and OriginY to indicate that the cell be positioned relative to the last cell.
		/// </summary>
		public const int Relative = GridBagConstraintConstants.Relative;

		/// <summary>
		/// This constant can be assigned to SpanX and SpanY to indicate that the cell occupy the rest of the logical row or the column in the row-layout respectively.
		/// </summary>
		public const int Remainder = GridBagConstraintConstants.Remainder;

		// Default as in not set.
		//
		private const int DEFAULT_SPAN = -2;

		#endregion // Constants

		#region Private Variables
		
		private LabelPosition labelPosition = LabelPosition.Default;
        // MRS - NAS 9.1 - Groups in RowLayout
        //private int labelSpan	= 1;
        private int labelSpan = -1;

		private int spanX		= DEFAULT_SPAN;
		private int spanY		= DEFAULT_SPAN;
		private int originX		= RowLayoutColumnInfo.Relative;
		private int originY		= RowLayoutColumnInfo.Relative;
		private float weightX	= 0.0f;
		private float weightY	= 0.0f;
		private Size minimumLabelSize	= Size.Empty;
		private Size preferredLabelSize = Size.Empty;
		private Size minimumCellSize	= Size.Empty;
		private Size preferredCellSize	= Size.Empty;
		private RowLayoutSizing allowCellSizing = RowLayoutSizing.Default;
		private RowLayoutSizing allowLabelSizing = RowLayoutSizing.Default;

		private Insets cellInsets = null;
		private Insets labelInsets = null;

        // MRS - NAS 9.1 - Groups in RowLayout
        // This class will servive both columns and groups, now. So 
        // change the column to a context object.
        //
		//private UltraGridColumn column = null;
        private object context = null;

		private SerializedColumnID serializedColumnId = null;
		private int serializedUnboundColumnIndex = -1;

        // MRS - NAS 9.1 - Groups in RowLayou
        //private int verifiedLayoutDimensionsVersion = -1;
        //private int cachedOriginXResolved = 0;
        //private int cachedOriginYResolved = 0;
        //private int cachedSpanXResolved = 0;
        //private int cachedSpanYResolved = 0;        
        internal int verifiedLayoutDimensionsVersion = -1;
        internal int cachedOriginXResolved = 0;
        internal int cachedOriginYResolved = 0;
        internal int cachedSpanXResolved = 0;
        internal int cachedSpanYResolved = 0;        

		private Rectangle cachedItemRectCell = Rectangle.Empty;
		private Rectangle cachedItemRectHeader = Rectangle.Empty;

		// SSP 11/10/04 - Merged Cell Feature
		// Added SpansEntireLayoutHeight property.
		//
		private bool spansEntireLayoutHeight = false;

		// SSP 11/17/04
		// Added a way to hide the header or the cell of a column in the row layout mode.
		// This way you can do things like hide a row of headers or add unbound columns
		// and hide their cells to show grouping headers etc...
		//
        // MRS - NAS 9.1 - Groups in RowLayout
        //private bool shouldPositionCellElement = false;
        //private bool shouldPositionLabelElement = false;
        internal bool shouldPositionCellElement = false;
        internal bool shouldPositionLabelElement = false;

		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		//
		private float weightXDefault = 0.0f;

		// SSP 4/7/05 BR03193
		// We need to store the Hidden property values of the columns with a RowLayout. Added an
		// overload of constructor that takes in columnHidden.
		//
		internal DefaultableBoolean columnHidden = DefaultableBoolean.Default;
		
		// SSP 8/29/05 BR05837
		// A version number that's bumped every time the PreferredCellSize is changed.
		// This is used when the RowSizing is not Syncrhonized and a column is vertically
		// span-resized. When this happens all the cells in all the rows should assume
		// the size as indicated by the red rectangle that shows up while span-resizing.
		// 
		internal int preferredCellSizeHeightVersion = 0;

		// SSP 8/30/06 BR13795
		// Added autoFitAllColumns_preferredCellSizeOverride and autoFitAllColumns_preferredCellSizeOverride.
		// 
		internal Size autoFitAllColumns_preferredCellSizeOverride = Size.Empty;
		internal Size autoFitAllColumns_preferredLabelSizeOverride = Size.Empty;

        // MRS - NAS 9.1 - Groups in RowLayout
        internal RowLayoutColumnInfoContext contextType = RowLayoutColumnInfoContext.Column;
        private SerializedGroupID serializedGroupId = null;
        private UltraGridGroup parentGroup = null;
        private string parentGroupKey = null;
        private int parentGroupIndex = -1;

		// MD 1/21/09 - Groups in RowLayouts
		private bool isVerifyingLayoutItemDimensions;

		#endregion // Private Variables

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="columnKey">A value that uniquely identifies an object in a collection.</param>
		/// <param name="unboundColumnIndex">The unbound column index.</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public RowLayoutColumnInfo( string columnKey, int unboundColumnIndex ) 
			: this( columnKey, unboundColumnIndex, DefaultableBoolean.Default )
		{
		}
		
		// SSP 4/7/05 BR03193
		// We need to store the Hidden property values of the columns with a RowLayout. Added an
		// overload of constructor that takes in columnHidden.
		//
		/// <summary>
		/// Constructor. For use by design-time serialization.
		/// </summary>
        /// <param name="columnKey">A value that uniquely identifies an object in a collection.</param>
        /// <param name="unboundColumnIndex">The unbound column index.</param>
		/// <param name="columnHidden">The hidden setting of the column.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        // MRS - NAS 9.1 - Groups in RowLayout
        // This constructor now calls the new constructor which takes a RowLayoutColumnInfoContext
        //
        #region Old Code
        //public RowLayoutColumnInfo( string columnKey, int unboundColumnIndex, DefaultableBoolean columnHidden ) : base( columnKey )
        //{
        //    // SSP 4/7/05 BR03193
        //    //
        //    this.columnHidden = columnHidden;

        //    this.serializedUnboundColumnIndex = unboundColumnIndex;

        //    this.Reset( );
        //}
        #endregion //Old Code
        public RowLayoutColumnInfo(string columnKey, int unboundColumnIndex, DefaultableBoolean columnHidden) : this(RowLayoutColumnInfoContext.Column, columnKey, unboundColumnIndex, columnHidden) 
        { }

        // MRS - NAS 9.1 - Groups in RowLayout        
        // Added a new constructor which takes a rowLayoutColumnInfoContext.
        //
        /// <summary>
        /// Constructor. For use by design-time serialization.
        /// </summary>
        /// <param name="rowLayoutColumnInfoContext">A value indicating whether this object represents a column or a group.</param>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="unboundColumnIndex">The unbound column index.</param>
        /// <param name="columnHidden">The hidden setting of the column.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public RowLayoutColumnInfo(RowLayoutColumnInfoContext rowLayoutColumnInfoContext, string key, int unboundColumnIndex, DefaultableBoolean columnHidden)
            : this(rowLayoutColumnInfoContext, key, unboundColumnIndex, columnHidden, null, -1)
        {            
        }

        // MRS - NAS 9.1 - Groups in RowLayout
        //
        /// <summary>
        /// Constructor. For use by design-time serialization.
        /// </summary>
        /// <param name="rowLayoutColumnInfoContext">A value indicating whether this object represents a column or a group.</param>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <param name="unboundColumnIndex">The unbound column index.</param>
        /// <param name="columnHidden">The hidden setting of the column.</param>
        /// <param name="parentGroupKey">The key representing the parent group.</param>
        /// <param name="parentGroupIndex">The index of the parent group.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public RowLayoutColumnInfo(RowLayoutColumnInfoContext rowLayoutColumnInfoContext, string key, int unboundColumnIndex, DefaultableBoolean columnHidden, string parentGroupKey, int parentGroupIndex)
            : base(key)
        {
            // SSP 4/7/05 BR03193
            //
            this.columnHidden = columnHidden;

            this.serializedUnboundColumnIndex = unboundColumnIndex;

            this.contextType = rowLayoutColumnInfoContext;

            this.parentGroupKey = parentGroupKey;
            this.parentGroupIndex = parentGroupIndex;

            this.Reset();
        }
		
		internal RowLayoutColumnInfo( UltraGridColumn column ) : base( null != column ? column.Key : "" )
		{
			this.InitColumn( column );
			this.Reset( );
		}

        // MRS - NAS 9.1 - Groups in RowLayout
        internal RowLayoutColumnInfo(UltraGridGroup group)
            : base(null != group ? group.Key : "")
        {
            this.InitGroup(group);
            this.Reset();
        }

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected RowLayoutColumnInfo(SerializationInfo info, StreamingContext context)
		{
			// Since we're not saving properties with default values, we must set the default here.
			//
			this.Reset();

			this.context = null;

			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "Tag":
						this.DeserializeTag( entry );
						break;
					case "Key":
						this.Key = (string)Utils.DeserializeProperty( entry, typeof( string ), this.Key );
						break;
					case "LabelPosition":
						this.labelPosition = (LabelPosition)(int)Utils.DeserializeProperty( entry, typeof( LabelPosition ), this.labelPosition );
						break;
					case "LabelSpan":
						this.labelSpan = (int)Utils.DeserializeProperty( entry, typeof(int), this.labelSpan );
						break;
					case "MinimumCellSize":
						this.minimumCellSize = (Size)Utils.DeserializeProperty( entry, typeof(Size), this.minimumCellSize );
						break;
					case "MinimumLabelSize":
						this.minimumLabelSize = (Size)Utils.DeserializeProperty( entry, typeof(Size), this.minimumLabelSize );
						break;
					case "OriginX":
						this.originX = (int)Utils.DeserializeProperty( entry, typeof(int), this.originX );
						break;
					case "OriginY":
						this.originY = (int)Utils.DeserializeProperty( entry, typeof(int), this.originY );
						break;
					case "PreferredCellSize":
						this.preferredCellSize = (Size)Utils.DeserializeProperty( entry, typeof(Size), this.preferredCellSize );
						break;
					case "PreferredLabelSize":
						this.preferredLabelSize = (Size)Utils.DeserializeProperty( entry, typeof(Size), this.preferredLabelSize );
						break;
					case "SpanX":
						this.spanX = (int)Utils.DeserializeProperty( entry, typeof(int), this.spanX );
						break;
					case "SpanY":
						this.spanY = (int)Utils.DeserializeProperty( entry, typeof(int), this.spanY );
						break;
					case "WeightX":
						this.weightX = (float)Utils.DeserializeProperty( entry, typeof(float), this.weightX );
						break;
					case "WeightY":
                        // MRS - NAS 9.1 - Groups in RowLayout
						//this.weightX = (float)Utils.DeserializeProperty( entry, typeof(float), this.weightX );
                        this.weightY = (float)Utils.DeserializeProperty(entry, typeof(float), this.weightY);
						break;
					case "AllowCellSizing":
						this.allowCellSizing = (RowLayoutSizing)Utils.DeserializeProperty( entry, typeof( RowLayoutSizing ), this.allowCellSizing );
						break;
					case "AllowLabelSizing":
						this.allowLabelSizing = (RowLayoutSizing)Utils.DeserializeProperty( entry, typeof( RowLayoutSizing ), this.allowLabelSizing );
						break;
					case "Column":
						this.serializedColumnId = (SerializedColumnID)Utils.DeserializeProperty( entry, typeof( SerializedColumnID ), null );
						break;
					// SSP 12/1/03 UWG2641
					// Added CellInsets and LabelInsets properties.
					//
					// ----------------------------------------------------------------------
					case "CellInsets":
						this.cellInsets = (Insets)Utils.DeserializeProperty( entry, typeof( Insets ), this.cellInsets );
						break;
					case "LabelInsets":
						this.labelInsets = (Insets)Utils.DeserializeProperty( entry, typeof( Insets ), this.labelInsets );
						break;
					// ----------------------------------------------------------------------
					// SSP 4/7/05 BR03193
					// We need to store the Hidden property values of the columns with a RowLayout.
					//
					case "ColumnHidden":
						this.columnHidden = (DefaultableBoolean)Utils.DeserializeProperty( entry, typeof( DefaultableBoolean ), this.columnHidden );
						break;

                    // MRS - NAS 9.1 - Groups in RowLayout
                    case "RowLayoutColumnInfoContext":
                        this.contextType = (RowLayoutColumnInfoContext)Utils.DeserializeProperty(entry, typeof(RowLayoutColumnInfoContext), RowLayoutColumnInfoContext.Column);
                        break;
                    case "Group":
                        this.serializedGroupId = (SerializedGroupID)Utils.DeserializeProperty(entry, typeof(SerializedGroupID), null);
                        break;
                    case "ParentGroupKey":                        
                        this.parentGroupKey = (string)Utils.DeserializeProperty(entry, typeof(string), null);                        
                        break;
                    case "ParentGroupIndex":
                        this.parentGroupIndex = (int)Utils.DeserializeProperty(entry, typeof(int), -1);
                        break;

					default:
						Debug.Assert( false, "Unknown property in deserialization constructor !" );
						break;
				}
			}            
		}

		#endregion // Constructor

		#region Base Overrides

		#region Key

		/// <summary>
		/// Returns or sets a value that uniquely identifies an object in a collection.
		/// </summary>
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override String Key
		{
			get
			{
                // MRS - NAS 9.1 - Groups in RowLayout
                //if ( null != this.Column )
                //    return this.Column.Key;
                //else
                //    return base.Key;
                switch (this.ContextType)
                {
                    case RowLayoutColumnInfoContext.Group:
                        if (null != this.Group)
                            return this.Group.Key;

                        break;
                        
                    case RowLayoutColumnInfoContext.Column:
                    default:
                        Debug.Assert(this.ContextType == RowLayoutColumnInfoContext.Column, "Unknown RowLayoutColumnInfoContext");
                        if (null != this.Column)
                            return this.Column.Key;

                        break;
                }

                return base.Key;
			}
			set
			{
				throw new NotSupportedException( Shared.SR.GetString("LER_NotSupportedException_346") );
			}
		}

		#endregion // Key

		#region OnDispose
		
		// SSP 12/1/03 UWG2641
		// Overrode OnDispose.
		//
		/// <summary>
		/// Called when the object is disposed off.
		/// </summary>
		protected override void OnDispose( )
		{
			if ( null != this.cellInsets )
				this.cellInsets.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			this.cellInsets = null;

			if ( null != this.labelInsets )
				this.labelInsets.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			this.labelInsets = null;

			base.OnDispose( );
		}

		#endregion // OnDispose

		#endregion // Base Overrides

		#region Private/Internal Properties/Methods

		#region MergeAutoFitOverrideSize

		// SSP 8/30/06 BR13795
		// Added MergeAutoFitOverrideSize method.
		// 
		private Size MergeAutoFitOverrideSize( Size size, Size overrideSize )
		{
			UltraGridLayout layout = null != this.Column ? this.Column.Layout : null;
			if ( null != layout && layout.AutoFitAllColumns )
			{
				if ( overrideSize.Width > 0 )
					size.Width = overrideSize.Width;

				if ( overrideSize.Height > 0 )
					size.Height = overrideSize.Height;
			}

			return size;
		}

		#endregion // MergeAutoFitOverrideSize

		#region InitColumn

		internal void InitColumn( UltraGridColumn column )
		{
			if ( null == column )
				throw new ArgumentNullException( "column" );

            // MRS - NAS 9.1 - Groups in RowLayout
            this.contextType = RowLayoutColumnInfoContext.Column;

			base.Key = column.Key;
			this.context = column;
		}        

		#endregion //InitColumn

        // MRS - NAS 9.1 - Groups in RowLayout
        //
        #region InitGroup

        internal void InitGroup(UltraGridGroup group)
        {
            if (null == group)
                throw new ArgumentNullException("group");
            
            this.contextType = RowLayoutColumnInfoContext.Group;
            base.Key = group.Key;
            this.context = group;
        }

        #endregion //InitGroup

        #region ApplyInsets

		internal static Rectangle ApplyInsets( Rectangle rect, Insets insets )
		{
			if ( null != insets )
			{
				rect.X += insets.Left;
				rect.Y += insets.Top;
				rect.Width -= insets.Left + insets.Right;
				rect.Height -= insets.Top + insets.Bottom;
			}

			return rect;
		}

		#endregion // ApplyInsets

		#region GetItemInsetsHelper
		
		internal static Insets GetItemInsetsHelper( ILayoutItem item )
		{
			// SSP 8/3/05 - NAS 5.3 Row-Auto-Sizing in Row Layout
			// 
			//Debug.Assert( item is IGridBagConstraint, "In grid, item should've implemented IGridBagConstraint interface !" );
			//IGridBagConstraint gc = item as IGridBagConstraint;
			IGridBagConstraint gc = LayoutUtility.GetAssociatedGridBagConstraint( item );

			Insets insets = null != gc ? gc.Insets : null;
			return null != insets ? insets : RowLayoutColumnInfo.emptyInsets;
		}

		#endregion // GetItemInsetsHelper

		#region VerifyLayoutItemDimensions
		
        private void VerifyLayoutItemDimensions()
        {
			// MD 1/21/09 - Groups in RowLayouts
			// Added in an anti-recursion flag because we might get in here twice for a column which could cause issues.
			if ( this.isVerifyingLayoutItemDimensions )
				return;

			this.isVerifyingLayoutItemDimensions = true;

			// MD 2/17/09
			// Found while fixing TFS13902
			// Wrapped in a try finally to make sure isVerifyingLayoutItemDimensions is set back to False.
			try
			{

            // MRS - NAS 9.1 - Groups in RowLayout
            // Note that when this method is called for a RowLayoutColumnInfo of a group, this.Column 
            // will be null and be will bail out. This is correct. 
            // The item's layout dimensions will get verified for groups inside of the 
            // VerifyRowLayoutCache method of the group or band.

            if (null == this.Column || null == this.Column.Band)
			{
				// MD 2/18/09
				// Found while fixing TFS14144
				// This is a betetr fix for TFS13902 and should have always been done. Instances of this class for Groups 
				// were bailing out of this method without doing any verification at all. This is wrong and instead, we
				// should have been verifying the layout cache of the group or band. We chose to verify the band to be safe.
				UltraGridBand band = this.GetBand();
				if ( band != null )
					band.VerifyRowLayoutCache();

                return;
			}

            
            // MRS - NAS 9.1 - Groups in RowLayout
            //GridBagLayoutItemDimensionsCollection itemDimensionsColl = this.Column.Band.LayoutItemDimensions;
            GridBagLayoutItemDimensionsCollection itemDimensionsColl;
            if (this.Column.Band.RowLayoutStyle == RowLayoutStyle.GroupLayout &&
                this.ParentGroup != null)
            {
                itemDimensionsColl = this.ParentGroup.LayoutItemDimensions;
            }
            else
                itemDimensionsColl = this.Column.Band.LayoutItemDimensions;

			// MD 1/23/09 - Groups in RowLayouts
			// Use the new version number which is specifically for layout items. We needed to separate these version numbers because we were verifying 
			// in here while the band was verifying its layout, but before it got a chance to store its flattened layouts, so the LayoutItemDimensions
			// cached above was out of date. Now, when the flattened layouts are recreated, the layout item cache version is bumped.
			//if (this.verifiedLayoutDimensionsVersion != this.Column.Band.RowLayoutCacheVersion)
			//{
			//    this.verifiedLayoutDimensionsVersion = this.Column.Band.RowLayoutCacheVersion;
			if ( this.verifiedLayoutDimensionsVersion != this.Column.Band.RowLayoutCacheVersionForLayoutItems )
			{
				this.verifiedLayoutDimensionsVersion = this.Column.Band.RowLayoutCacheVersionForLayoutItems;

                if (itemDimensionsColl.Exists(this))
                {
                    GridBagLayoutItemDimensions itemDimensions = itemDimensionsColl[this];

                    this.cachedOriginXResolved = itemDimensions.OriginX;
                    this.cachedOriginYResolved = itemDimensions.OriginY;
                    this.cachedSpanXResolved = itemDimensions.SpanX;
                    this.cachedSpanYResolved = itemDimensions.SpanY;

                    // Items with spanX of Remainder should have spanX of at least 2 since we need one of
                    // them to be for the label.
                    //
                    if (RowLayoutColumnInfo.Remainder == this.SpanX || RowLayoutColumnInfo.Relative == this.SpanX)
                    {
                        this.cachedSpanXResolved++;
                    }

                    // Same applies to spanY.
                    //
                    if (RowLayoutColumnInfo.Remainder == this.SpanY || RowLayoutColumnInfo.Relative == this.SpanY)
                    {
                        this.cachedSpanYResolved++;
                    }
                }
                else
                {
                    //Debug.Assert( false, "LayoutItemDimensions should contain all the row layout info objects with visible associated columns." );

                    this.cachedOriginXResolved = this.originX;
                    this.cachedOriginYResolved = this.originY;
                    this.cachedSpanXResolved = this.spanX;
                    this.cachedSpanYResolved = this.spanY;

                    // SSP 4/12/05 - NAS 5.2 Row Layout Designer Changes
                    // Allow hidden columns to be dragged.
                    //
                    if (this.cachedSpanXResolved < 0)
                        this.cachedSpanXResolved = 2;
                    if (this.cachedSpanYResolved < 0)
                        this.cachedSpanYResolved = 2;
                }

                // SSP 11/17/04
                // Added a way to hide the header or the cell of a column in the row layout mode.
                // This way you can do things like hide a row of headers or add unbound columns
                // and hide their cells to show grouping headers etc...
                // NOTE: The reason we aren't just hiding the HeaderBase or the UltraGridColumn
                // from the LayoutManager is because that would cause the HeaderLayoutManager
                // and CellLayoutManager to be out of sync as far as number of logical columns
                // and their preferred sizes etc... go.
                // 
                this.shouldPositionCellElement = LabelPosition.LabelOnly != this.LabelPositionResolved;
                this.shouldPositionLabelElement = LabelPosition.None != this.LabelPositionResolved;
            }

			// MD 2/17/09
			// Found while fixing TFS13902
			// Wrapped in a try finally to make sure isVerifyingLayoutItemDimensions is set back to False.
			}
			finally
			{
				// MD 1/21/09 - Groups in RowLayouts
				this.isVerifyingLayoutItemDimensions = false;
			}
        }

		#endregion // VerifyLayoutItemDimensions

		#region CachedItemRectCell
		
		internal Rectangle CachedItemRectCell
		{
			get
			{
				return this.cachedItemRectCell;
			}
			set
			{
				this.cachedItemRectCell = value;
			}
		}

		#endregion // CachedItemRectCell

		#region CachedItemRectHeader
		
		internal Rectangle CachedItemRectHeader
		{
			get
			{
				return this.cachedItemRectHeader;
			}
			set
			{
				// SSP 8/1/05 - NAS 5.3 Wrap Header Text for Row-Layout
				// 
				//this.cachedItemRectHeader = value;
				if ( this.cachedItemRectHeader != value )
				{
					if ( this.cachedItemRectHeader.Width != value.Width && null != this.Column )
						// SSP 4/24/08 BR27115
						// Don't dirty the column metrics - pass false for dirtyColMetrics parameter.
						// 
						//this.Column.ClearFontCache( );
						this.Column.ClearFontCache( false );

					this.cachedItemRectHeader = value;
				}
			}
		}

		#endregion // CachedItemRectHeader

		#region CachedHeaderRect

		internal Rectangle CachedHeaderRect
		{
			get
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//if ( this.Column.Band.AreColumnHeadersInSeparateLayoutArea )
                if (this.GetBand().AreColumnHeadersInSeparateLayoutArea)
				{
					return this.CachedItemRectHeader;
				}
				else
				{
                    // MRS 2/10/2009 - TFS13728
                    // There's no CachedItemRectCell for a group. 
					//return Rectangle.Union( this.CachedItemRectCell, this.CachedItemRectHeader );
                    if (this.CachedItemRectCell.IsEmpty == false)
                        return Rectangle.Union(this.CachedItemRectCell, this.CachedItemRectHeader);
                    else
                        return this.CachedItemRectHeader;
				}
			}
		}

		#endregion // CachedHeaderRect

		#region CachedOverallCellRect
		
		internal Rectangle CachedOverallCellRect
		{
			get
			{
				// SSP 11/29/04
				//
				//if ( LabelPosition.None != this.LabelPositionResolved )
				if ( this.IsHeaderVisibleWithCell )
				{
					return Rectangle.FromLTRB(
						Math.Min( this.cachedItemRectCell.Left, this.cachedItemRectHeader.Left ),
						Math.Min( this.cachedItemRectCell.Top, this.cachedItemRectHeader.Top ),
						Math.Max( this.cachedItemRectCell.Right, this.cachedItemRectHeader.Right ),
						Math.Max( this.cachedItemRectCell.Bottom, this.cachedItemRectHeader.Bottom ) );
				}

				return this.cachedItemRectCell;
			}
			set
			{
				this.cachedItemRectCell = value;
			}
		}

		#endregion // CachedItemRectCell

		#region InitializeFrom
		
		internal void InitializeFrom( RowLayoutColumnInfo source )
		{
			if ( null == source )
			{
				this.Reset( );
				return;
			}

			this.labelPosition = source.labelPosition;
			this.labelSpan = source.labelSpan;
			this.minimumCellSize = source.minimumCellSize;
			this.minimumLabelSize = source.minimumLabelSize;
			this.preferredCellSize = source.preferredCellSize;
			this.preferredLabelSize = source.preferredLabelSize;
			this.originX = source.originX;
			this.originY = source.originY;
			this.spanX = source.spanX;
			this.spanY = source.spanY;
			this.weightX = source.weightX;
			this.weightY = source.weightY;
			this.allowCellSizing = source.allowCellSizing;
			this.allowLabelSizing = source.allowLabelSizing;

			// SSP 12/1/03 UWG2641
			// Added CellInsets and LabelInsets properties.
			//
			// --------------------------------------------------------------------
			if ( source.ShouldSerializeCellInsets( ) )
				this.CellInsets.CopyFrom( source.cellInsets );
			// SSP 8/2/05
			// Otherwise we need to reset the label insets.
			// 
			else if ( null != this.cellInsets )
				this.cellInsets.Reset( );

			if ( source.ShouldSerializeLabelInsets( ) )
				this.LabelInsets.CopyFrom( source.labelInsets );
			// SSP 8/2/05
			// Otherwise we need to reset the label insets.
			// 
			else if ( null != this.labelInsets )
				this.labelInsets.Reset( );
			// --------------------------------------------------------------------

			// SSP 4/7/05 BR03193
			// We need to store the Hidden property values of the columns with a RowLayout.
			//
			this.columnHidden = source.columnHidden;

            // MRS - NAS 9.1 - Groups in RowLayout
            this.contextType = source.contextType;            
            this.parentGroupKey = source.parentGroupKey;
            this.parentGroupIndex = source.parentGroupIndex;
            // Hook up to the ParentGroup
            UltraGridBand band = this.GetBand();
            if (band != null)
            {
                UltraGridBand.InitRowLayoutColumnInfo(this, band);
            }
            else
            {
                Debug.Fail("Failed to get the band; unexpected.");
                this.parentGroup = null;
            }

			this.verifiedLayoutDimensionsVersion--;
		}

		#endregion // InitializeFrom

		#region CanInitializeFrom

		internal bool CanInitializeFrom( RowLayoutColumnInfo rowLayoutColumnInfo )
		{
            // MRS - NAS 9.1 - Groups in RowLayout
            // Re-factored this code to account for groups
            #region Old Code
            //UltraGridColumn column = rowLayoutColumnInfo.column;            

            //UltraGridColumn thisColumn = this.Column;

            //Debug.Assert( null != thisColumn && null != thisColumn.Band, "No column or band !" );

            //if ( null == column && null != thisColumn && null != thisColumn.Band )
            //{
            //    if ( null != rowLayoutColumnInfo.serializedColumnId )
            //    {
            //        column = thisColumn.Band.GetMatchingColumn( rowLayoutColumnInfo.serializedColumnId );
            //    }
            //    else if ( null != rowLayoutColumnInfo.Key && rowLayoutColumnInfo.Key.Length > 0 &&
            //         thisColumn.Band.Columns.Exists( rowLayoutColumnInfo.Key ) )
            //    {
            //        column = thisColumn.Band.Columns[ rowLayoutColumnInfo.Key ];
            //    }
            //    else if ( rowLayoutColumnInfo.serializedUnboundColumnIndex >= 0 ) 
            //    {
            //        int columnIndex = rowLayoutColumnInfo.serializedUnboundColumnIndex +
            //            thisColumn.Band.Columns.BoundColumnsCount;

            //        if ( columnIndex >= 0 && columnIndex < thisColumn.Band.Columns.Count )
            //            column = thisColumn.Band.Columns[ columnIndex ];
            //    }
            //}

            //if ( thisColumn == column )
            //    return true;
            //else if ( null != column && null != thisColumn && 
            //    null != column.Key && column.Key.Equals( thisColumn.Key ) )
            //    return true;

            //return false;
            #endregion // Old Code
            //
            object context = rowLayoutColumnInfo.Context;

            object thisContext = this.Context;

            UltraGridBand band = rowLayoutColumnInfo.GetBand();
            UltraGridBand thisBand = this.GetBand();

            Debug.Assert(null != thisContext && null != thisBand, "No column or band !");

            if (null == context && null != thisContext && null != thisBand)
            {
                if (null != rowLayoutColumnInfo.serializedColumnId)
                {
                    switch (rowLayoutColumnInfo.ContextType)
                    {
                        case RowLayoutColumnInfoContext.Group:
                            context = thisBand.GetMatchingGroup(rowLayoutColumnInfo.serializedGroupId);
                            break;

                        case RowLayoutColumnInfoContext.Column:
                        default:
                            Debug.Assert(rowLayoutColumnInfo.ContextType == RowLayoutColumnInfoContext.Column, "Unknown RowLayoutColumnInfoContext");
                            context = thisBand.GetMatchingColumn(rowLayoutColumnInfo.serializedColumnId);
                            break;
                    }
                }
                else if (null != rowLayoutColumnInfo.Key && rowLayoutColumnInfo.Key.Length > 0)
                {
                    switch (rowLayoutColumnInfo.ContextType)
                    {
                        case RowLayoutColumnInfoContext.Group:
                            if (thisBand.Groups.Exists(rowLayoutColumnInfo.Key))
                                context = thisBand.Groups[rowLayoutColumnInfo.Key];
                            break;

                        case RowLayoutColumnInfoContext.Column:
                        default:
                            Debug.Assert(rowLayoutColumnInfo.ContextType == RowLayoutColumnInfoContext.Column, "Unknown RowLayoutColumnInfoContext");
                            if (thisBand.Columns.Exists(rowLayoutColumnInfo.Key))
                                context = thisBand.Columns[rowLayoutColumnInfo.Key];
                            break;
                    }                    
                }
                else if (rowLayoutColumnInfo.serializedUnboundColumnIndex >= 0)
                {
                    int columnIndex = rowLayoutColumnInfo.serializedUnboundColumnIndex +
                        thisBand.Columns.BoundColumnsCount;

                    if (columnIndex >= 0 && columnIndex < thisBand.Columns.Count)
                        context = thisBand.Columns[columnIndex];
                }
            }

            if (thisContext == context)
                return true;
            else if (null != context && null != thisContext &&
                null != rowLayoutColumnInfo.Key && rowLayoutColumnInfo.Key.Equals(this.Key))
                return true;

            return false;
        }

		#endregion // CanInitializeFrom

		#region AllowCellSizingResolved

		internal RowLayoutSizing AllowCellSizingResolved
		{
			get
			{
				// SSP 6/26/03 UWG2387
				// In design mode, allow resizing.
				//
				// ----------------------------------------------------------------------------------
				UltraGridBase grid = this.Column.Band.Layout.Grid;
				if ( null != grid && grid.DesignMode )
					return RowLayoutSizing.Both;

				// SSP 6/27/03 UWG2371
				// If the the gridbase is not an UltraGrid (either it's a combo or a drop down), then
				// don't allow resizing.
				//
				if ( !( grid is UltraGrid ) )
					return RowLayoutSizing.None;
				// ----------------------------------------------------------------------------------

				if ( RowLayoutSizing.Default != this.allowCellSizing )
					return this.allowCellSizing;

				return this.Column.Band.AllowRowLayoutCellSizingResolved;
			}
		}

		#endregion // AllowCellSizingResolved

		#region AllowLabelSizingResolved
		
		internal RowLayoutSizing AllowLabelSizingResolved
		{
			get
			{
				// SSP 6/26/03 UWG2387
				// In design mode, allow resizing.
				//
				// ----------------------------------------------------------------------------------
                // MRS - NAS 9.1 - Groups in RowLayout
				//UltraGridBase grid = this.Column.Band.Layout.Grid;
                UltraGridBand band = this.GetBand();
                UltraGridBase grid = band.Layout.Grid;

				if ( null != grid && grid.DesignMode )
					return RowLayoutSizing.Both;

				// SSP 6/27/03 UWG2371
				// If the the gridbase is not an UltraGrid (either it's a combo or a drop down), then
				// don't allow resizing.
				//
				if ( !( grid is UltraGrid ) )
					return RowLayoutSizing.None;
				// ----------------------------------------------------------------------------------

				if ( RowLayoutSizing.Default != this.allowCellSizing )
					return this.allowCellSizing;

                // MRS - NAS 9.1 - Groups in RowLayout
				//return this.Column.Band.AllowRowLayoutLabelSizingResolved;
                return band.AllowRowLayoutLabelSizingResolved;
			}
		}

		#endregion // AllowLabelSizingResolved

		#region SpansEntireLayoutHeight
		
		// SSP 11/10/04 - Merged Cell Feature
		// Added SpansEntireLayoutHeight property.
		//
		internal bool SpansEntireLayoutHeight
		{
			get
			{
				this.Column.Band.VerifyRowLayoutCache( );
				return this.spansEntireLayoutHeight;
			}
		}

		#endregion // SpansEntireLayoutHeight

		#region InternalSetSpansEntireLayoutHeight
		
		// SSP 11/10/04 - Merged Cell Feature
		// Added SpansEntireLayoutHeight property.
		//
		internal void InternalSetSpansEntireLayoutHeight( bool val )
		{
			this.spansEntireLayoutHeight = val;
		}

		#endregion // InternalSetSpansEntireLayoutHeight

		#region LabelSpanResolved

		// SSP 11/17/04
		// Added a way to hide the header or the cell of a column in the row layout mode.
		// This way you can do things like hide a row of headers or add unbound columns
		// and hide their cells to show grouping headers etc...
		// Added LabelSpanResolved property and replaced LabelSpan get calls with 
		// LabelSpanResolved get calls in most places.
		//
		/// <summary>
		/// Returns the resolved label span.
		/// </summary>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Advanced ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int LabelSpanResolved
		{
			get
			{
				// SSP 6/6/05 BR04312
				// 
				//return this.LabelSpan;
				LabelPosition labelPos = this.LabelPositionResolved;
				int entireSpan = 0;

				if ( LabelPosition.Left == labelPos || LabelPosition.Right == labelPos )
					entireSpan = this.SpanX;
				else if ( LabelPosition.Top == labelPos || LabelPosition.Bottom == labelPos )
					entireSpan = this.SpanY;
                
				return entireSpan > 0 ? Math.Min( entireSpan, this.LabelSpan ) : this.LabelSpan;
			}
		}

		#endregion // LabelSpanResolved

		#region ShouldPositionCellElement

		// SSP 11/17/04
		// Added a way to hide the header or the cell of a column in the row layout mode.
		// This way you can do things like hide a row of headers or add unbound columns
		// and hide their cells to show grouping headers etc...
		//
		internal bool ShouldPositionCellElement
		{
			get
			{
				return this.shouldPositionCellElement;
			}
		}

		#endregion // ShouldPositionCellElement

		#region ShouldPositionHeaderElement

		// SSP 11/17/04
		// Added a way to hide the header or the cell of a column in the row layout mode.
		// This way you can do things like hide a row of headers or add unbound columns
		// and hide their cells to show grouping headers etc...
		//
		internal bool ShouldPositionLabelElement
		{
			get
			{
				return this.shouldPositionLabelElement;
			}
		}

		#endregion // ShouldPositionHeaderElement

		#region IsHeaderVisibleWithCell

		// SSP 11/29/04
		// Added IsHeaderVisibleWithCell property.
		//
		internal bool IsHeaderVisibleWithCell
		{
			get
			{
                // MRS 2/24/2009 - Found while fixing TFS14427
                Debug.Assert(this.Column != null, "This property is not valid for groups, only columns. It should never be called for a group");
                if (this.Column == null)
                    return false;

				return ! this.Column.Band.AreColumnHeadersInSeparateLayoutArea && this.LabelSpan > 0 
					&& LabelPosition.None != this.LabelPositionResolved;
			}
		}

		#endregion // IsHeaderVisibleWithCell

		#region WeightXDefault

		// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
		// Added WeightXInternal and WeightXDefault properties.
		//
		internal float WeightXDefault
		{
			get
			{
				return this.weightXDefault;
			}
			set
			{
				if ( value != this.weightXDefault )
				{
					if ( value < 0.0f )
						throw new ArgumentOutOfRangeException( "WeightXDefault", SR.GetString( "LER_ArguementOutOfRangeException10" ) );

					this.weightXDefault = value;
				}
			}
		}

		#endregion // WeightXDefault

		#region Clone

		// SSP 8/2/05 - NAS 5.3 Column Swapping in Row-Layout
		// 
		internal virtual RowLayoutColumnInfo Clone( )
		{
            // MRS - NAS 9.1 - Groups in RowLayout
			//RowLayoutColumnInfo ci = new RowLayoutColumnInfo( this.Column );
            //
            RowLayoutColumnInfo ci;
            switch (this.ContextType)
            {                
                case RowLayoutColumnInfoContext.Group:
                    ci = new RowLayoutColumnInfo(RowLayoutColumnInfoContext.Group, this.Group.Key, -1, DefaultableBoolean.Default);
                    break;
                case RowLayoutColumnInfoContext.Column:
                default:
                    Debug.Assert(this.ContextType == RowLayoutColumnInfoContext.Column, "Unknown RowLayoutColumnInfoContext");
                    ci = new RowLayoutColumnInfo(this.Column);
                    break;
            }

			ci.InitializeFrom( this );
			return ci;
		}

		#endregion // Clone

        // MRS - NAS 9.1 - Groups in RowLayout
        #region GetBand
        internal UltraGridBand GetBand()
        {
            if (this.Column != null)
                return this.Column.Band;

            if (this.Group != null)
                return this.Group.Band;

            return null;
        }
        #endregion //GetBand

        // MRS - NAS 9.1 - Groups in RowLayout
        #region GetHeader
        private HeaderBase GetHeader()
        {
            if (this.Column != null)
                return this.Column.Header;

            if (this.Group != null)
                return this.Group.Header;

            return null;
        }
        #endregion //GetBand

        #endregion // Private/Internal Properties/Methods

        #region Public Properties

        #region Column

        /// <summary>
		/// Returns the associated column.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Advanced ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_Column") ]
		public UltraGridColumn Column
		{
			get
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//return this.column;
                return this.context as UltraGridColumn;
			}
		}

		#endregion // Column        

        #region LabelPosition

        /// <summary>
		/// Indicates if and how the label (column header) is be shown in relation to the cell.
		/// </summary>
		/// <remarks>
		/// <p class="body">There are two row-layout modes concerning column label positioning. Column labels can be displayed with the cells inside of each row or in a separate area. This any setting of this property is honored when the column labels are displayed with the cells however only the <b>None</b> and <b>LabelOnly</b> are honored when the labels are in separate area. By default, the column labels are not displayed with the cells.</p>
		/// <p class="body">When column labels are displayed with cells, they are repeated in every row while in the other mode, they are displayed in a separate area. In card-view, that separate area is the card-label area left of the rows and in non-card-view, it is the header area above the rows. In card-view, setting <see cref="UltraGridCardSettings.Style"/> to a value other than <b>MergedLabels</b> displays the column labels with the cells. In non-card view setting the <see cref="UltraGridBand.RowLayoutLabelStyle"/> to <b>WithCellData</b> displays the column labels with the cells.</p>
		/// <p><seealso cref="LabelPosition"/> <seealso cref="UltraGridBand.RowLayoutLabelStyle"/> <seealso cref="UltraGridCardSettings.Style"/> <seealso cref="SpanX"/> <seealso cref="SpanY"/> <seealso cref="LabelSpan"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_LabelPosition") ]
		public LabelPosition LabelPosition
		{
			get
			{
				return this.labelPosition;
			}
			set
			{
				if ( this.labelPosition != value )
				{
					if ( ! Enum.IsDefined( typeof( LabelPosition ), value ) )
						throw new InvalidEnumArgumentException( "LabelPosition", (int)value, typeof( LabelPosition ) );

					this.labelPosition = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.LabelPosition );
				}
			}
		}

		#endregion // LabelPosition

		#region LabelPositionResolved

		/// <summary>
		/// Retruns the resolved LabelPosition. In merged-headers mode, since the headers are not with the cells,
		/// this property will resolve to None.
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Advanced ), Browsable( false ), 
		  DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public LabelPosition LabelPositionResolved
		{
			get
			{
				// If the headers are displayed in separate area, then return None for the label position.
				// Also return None if the label span is less than or equal to 0.
				//
				// SSP 11/17/04
				// Added a way to hide the header or the cell of a column in the row layout mode.
				// This way you can do things like hide a row of headers or add unbound columns
				// and hide their cells to show grouping headers etc...
				// Now the new LabelOnly value is valid even when the headers are in separate
				// area. Commented out the following two lines.
				//
				//if ( this.Column.Band.AreColumnHeadersInSeparateLayoutArea || this.LabelSpan <= 0 )
				//	return LabelPosition.None;

				if ( LabelPosition.Default != this.LabelPosition )
					return this.LabelPosition;

                // MRS - NAS 9.1 - Groups in RowLayout
				//return this.Column.Band.RowLayoutLabelPositionResolved;
				// MD 2/25/09 - TFS14602
                //return this.GetBand().RowLayoutLabelPositionResolved;
				return this.GetBand().GetRowLayoutLabelPositionResolved( this.ContextType );
			}
		}

		#endregion // LabelPositionResolved

		#region LabelSpan

		/// <summary>
		/// Specifies the portion of SpanX or SpanY that is assigned to label (column header). When LablePosition is set to Left or Right LabelSpan specifies the portion of SpanX for the label and when it's Top or Bottom, it specifies the portion of SpanY for the label. If the LabelPosition is set to None or is not displayed with cells, then this property is ignored. If LabelPosition is set to <b>LabelOnly</b> which means cells aren't displayed then this property is ignored as well and the entire SpanX and SpanY is used for the label. Default value is 1.
		/// </summary>
		/// <remarks>
		/// <p><seealso cref="LabelPosition"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_LabelSpan") ]		
		public int LabelSpan
		{
			get
			{
                if (this.labelSpan == -1)
                    return this.DefaultLabelSpanResolved;

				return this.labelSpan;
			}
			set
			{
				if ( this.labelSpan != value )
				{
                    // MRS - NAS 9.1 - Groups in RowLayout
					//if ( value < 0 )
                    if (value < 0 && value != this.DefaultLabelSpan)
						throw new ArgumentOutOfRangeException( "LabelSpan", value, SR.GetString( "LER_ArguementOutOfRangeException1" ) );

					this.labelSpan = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.LabelSpan );
				}
			}
		}

		#endregion // LabelSpan

		#region MinimumLabelSize

		/// <summary>
		/// Minimum label size. Default value is Size(0,0) which means it will calculate a reasonable default size.
		/// </summary>
		/// <remarks>
		/// <p class="body">If MinimumLabelSize property is set to a size with width greater then 0, then the bigger of the MinimumLabelSize width and PreferredLabelSize width will be used as the size of the label. The same applies to height as well.</p>
		/// <p class="body"><seealso cref="PreferredLabelSize"/> <seealso cref="PreferredCellSize"/> <seealso cref="MinimumCellSize"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_MinimumLabelSize") ]
		public Size MinimumLabelSize
		{
			get
			{
				return this.minimumLabelSize;
			}
			set
			{
				if ( this.minimumLabelSize != value )
				{
					if ( value.Width < 0 || value.Height < 0 )
						throw new ArgumentOutOfRangeException( "MinimumLabelSize", value, SR.GetString( "LER_ArguementOutOfRangeException2" ) );

					this.minimumLabelSize = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.MinimumLabelSize );
				}
			}
		}

		#endregion // MinimumLabelSize

		#region PreferredLabelSize

		/// <summary>
		/// Preferred label size. Default value is Size(0,0) which means it will calculate a reasonable default size.
		/// </summary>
		/// <remarks>
		/// <p class="body">Headers with the same OriginX and SpanX (vertically stacked) with different PreferredLabelSize settings will result in all such cells having the max of the widths when displayed. Likewise headers with the same OriginY and SpanY (horizontally aligned) with different PreferredCellSize settings will result in all such cells having the max of the heights when displayed.</p>
		/// <p class="body">If MinimumLabelSize property is set to a size with width greater then 0, then the bigger of the MinimumLabelSize width and PreferredLabelSize width will be used as the size of the label. The same applies to height as well.</p>
		/// <p class="body"><seealso cref="PreferredCellSize"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_PreferredLabelSize") ]
		public Size PreferredLabelSize
		{
			get
			{
				// SSP 8/30/06 BR13795
				// Added autoFitAllColumns_preferredCellSizeOverride and autoFitAllColumns_preferredCellSizeOverride.
				// 
				//return this.preferredLabelSize;
				return this.MergeAutoFitOverrideSize( this.preferredLabelSize, this.autoFitAllColumns_preferredLabelSizeOverride );
			}
			set
			{
				// SSP 8/30/06 BR13795
				// Added autoFitAllColumns_preferredCellSizeOverride and autoFitAllColumns_preferredCellSizeOverride.
				// 
				this.autoFitAllColumns_preferredLabelSizeOverride = Size.Empty;

				if ( this.preferredLabelSize != value )
				{
					if ( value.Width < 0 || value.Height < 0 )
						throw new ArgumentOutOfRangeException( "PreferredLabelSize", value, SR.GetString( "LER_ArguementOutOfRangeException3" ) );

					this.preferredLabelSize = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.PreferredLabelSize );
				}
			}
		}

		#endregion // PreferredLabelSize

		#region MinimumCellSize

		/// <summary>
		/// Minimum cell size. Default value is Size(0,0) which means it will calculate a reasonable default size.
		/// </summary>
		/// <remarks>
		/// <p class="body">If MinimumCellSize property is set to a size with width greater then 0, then the bigger of the MinimumCellSize width and PreferredCellSize width will be used as the size of the cell. The same applies to height as well.</p>
		/// <p class="body"><seealso cref="PreferredCellSize"/> <seealso cref="PreferredLabelSize"/> <seealso cref="MinimumLabelSize"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_MinimumCellSize") ]
		public Size MinimumCellSize
		{
			get
			{
				return this.minimumCellSize;
			}
			set
			{
				if ( this.minimumCellSize != value )
				{
					if ( value.Width < 0 || value.Height < 0 )
						throw new ArgumentOutOfRangeException( "MinimumCellSize", value, SR.GetString( "LER_ArguementOutOfRangeException4" ) );

					this.minimumCellSize = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.MinimumCellSize );
				}
			}
		}

		#endregion // MinimumCellSize

		#region PreferredCellSize

		/// <summary>
		/// Preferred cell size. Default value is Size(0,0) which means it will calculate a reasonable default size.
		/// </summary>
		/// <remarks>
		/// <p class="body">If the Width of the PreferredCellSize is set to 0, then the value of <see cref="UltraGridColumn.Width"/> will be used as the width. If the Height is set to 0, then a reasonable default height will be calculated.</p>
		/// <p class="body">Cells with the same OriginX and SpanX (vertically stacked) with different PreferredCellSize settings will result in all such cells having the max of the widths when displayed. Likewise Cells with the same OriginY and SpanY (horizontally aligned) with different PreferredCellSize settings will result in all such cells having the max of the heights when displayed.</p>
		/// <p class="body">If MinimumCellSize property is set to a size with width greater then 0, then the bigger of the MinimumCellSize width and PreferredCellSize width will be used as the size of the cell. The same applies to height as well.</p>
		/// <p class="body"><seealso cref="PreferredLabelSize"/></p>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_PreferredCellSize") ]
		public Size PreferredCellSize
		{
			get
			{
				// SSP 8/30/06 BR13795
				// Added autoFitAllColumns_preferredCellSizeOverride and autoFitAllColumns_preferredCellSizeOverride.
				// 
				//return this.preferredCellSize;
				return this.MergeAutoFitOverrideSize( this.preferredCellSize, this.autoFitAllColumns_preferredCellSizeOverride );
			}
			set
			{
				// SSP 8/30/06 BR13795
				// Added autoFitAllColumns_preferredCellSizeOverride and autoFitAllColumns_preferredCellSizeOverride.
				// 
				this.autoFitAllColumns_preferredCellSizeOverride = Size.Empty;

				if ( this.preferredCellSize != value )
				{
					if ( value.Width < 0 || value.Height < 0 )
						throw new ArgumentOutOfRangeException( "PreferredCellSize", value, SR.GetString( "LER_ArguementOutOfRangeException5" ));

					// SSP 8/29/05 BR05837
					// A version number that's bumped every time the PreferredCellSize is changed.
					// This is used when the RowSizing is not Syncrhonized and a column is vertically
					// span-resized. When this happens all the cells in all the rows should assume
					// the size as indicated by the red rectangle that shows up while span-resizing.
					// 
					if ( value.Height != this.preferredCellSize.Height )
						this.preferredCellSizeHeightVersion++;

					this.preferredCellSize = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.PreferredCellSize );
				}
			}
		}

		#endregion // PreferredCellSize

		#region ActualCellSize

		// SSP 8/31/06 BR13802
		// 
		/// <summary>
		/// Gets or sets actual cell size. Setting this property emulates resizing of the column via UI.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Setting this property will resize the column to the specified size. However note that row-layout 
		/// constraints of other columns may prevent the column from actually realizing the specified size.
		/// In other words setting the property and then reading it may return a different value. Also note that
		/// setting this property on one column can modify the widths of the other columns if auto-fit columns
		/// functionality is enabled or row-layout constraints on other columns require it so.
		/// This method emulates resizing of the column via user interface (UI).
		/// </p>
		/// <p class="body">
		/// You can specify width or height component of the value as 0 in which case that component will not be
		/// resized.
		/// </p>
		/// <seealso cref="WeightX"/> <seealso cref="WeightY"/>
		/// </remarks>
		// SSP 12/13/06
		// Added Browsable and DesignerSerializationVisibility attributes.
		// 
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Size ActualCellSize
		{
			get
			{
				return this.CachedItemRectCell.Size;
			}
			set
			{
				this.SetActualSizeHelper( false, value );
			}
		}

		#endregion // ActualCellSize
                
		#region ActualCellSize

		// SSP 8/31/06 BR13802
		// 
		/// <summary>
		/// Gets or sets actual label size. Setting this property emulates resizing of the column header via UI.
		/// </summary>
		/// <remarks>
		/// <p class="body">
		/// Setting this property will resize the column header to the specified size. However note that row-layout 
		/// constraints of other columns may prevent the column header from actually realizing the specified size.
		/// In other words setting the property and then reading it may return a different value. Also note that
		/// setting this property on one column can modify the widths of the other columns if auto-fit columns
		/// functionality is enabled or row-layout constraints on other columns require it so.
		/// This method emulates resizing of the column header via user interface (UI).
		/// </p>
		/// <p class="body">
		/// You can specify width or height component of the value as 0 in which case that component will not be
		/// resized.
		/// </p>
		/// <seealso cref="WeightX"/> <seealso cref="WeightY"/>
		/// </remarks>
		// SSP 12/13/06
		// Added Browsable and DesignerSerializationVisibility attributes.
		// 
		[ Browsable( false ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public Size ActualLabelSize
		{
			get
			{
				return this.CachedItemRectHeader.Size;
			}
			set
			{
				this.SetActualSizeHelper( true, value );
			}
		}

		private void SetActualSizeHelper( bool header, Size value )
		{
            // MRS - NAS 9.1 - Groups in RowLayout
            // Re-factored this code to handle Groups
            //
            
            //UltraGridColumn column = this.Column;
            //UltraGridBand band = null != column ? column.Band : null;            
            //
            object context = this.Context;
            UltraGridBand band = this.GetBand();

            if (null != band)
            {
                if (null != band.Layout && !band.Layout.Disposed)
                    band.Layout.ColScrollRegions.InitializeMetrics();

                Size containerSize = header ? band.GetActualHeaderLayoutContainerSize() : band.GetActualRowLayoutContainerSize();

                // MRS - NAS 9.1 - Groups in RowLayout                
                //band.ResizeLayoutItemHelper(null, header ? (ILayoutItem)column.Header : (ILayoutItem)column,
                //    containerSize, value, Point.Empty, false, 0 != value.Width, 0 != value.Height);
                band.ResizeLayoutItemHelper(null, header ? (ILayoutItem)this.GetHeader() : (ILayoutItem)context,
                    containerSize, value, Point.Empty, false, 0 != value.Width, 0 != value.Height);
            }
            else
            {
                if (header)
                    this.PreferredLabelSize = value;
                else
                    this.PreferredCellSize = value;
            }
        }

		#endregion // ActualCellSize

		#region OriginX

		/// <summary>
		/// Horizontal coordinate in the row-layout.
		/// </summary>
		/// <remarks>
		/// <p class="body">OriginX specifies the horizontal cell coordinate in the row layout. If OriginX is set to <see cref="RowLayoutColumnInfo.Relative"/> then it will be positioned relative to the last item in the row-layout.</p>
		/// <seealso cref="OriginY"/> <seealso cref="SpanX"/> <seealso cref="SpanY"/>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_OriginX") ]
		// SSP 7/8/03
		[ System.ComponentModel.MergableProperty( false ) ]
		public int OriginX
		{
			get
			{
				return this.originX;
			}
			set
			{
				if ( this.originX != value )
				{
					if ( value < 0 && RowLayoutColumnInfo.Relative != value )
						throw new ArgumentOutOfRangeException( "OriginX", value, SR.GetString( "LER_ArguementOutOfRangeException6" ) );

					this.originX = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.OriginX );
				}
			}
		}

		#endregion // OriginX

		#region OriginY

		/// <summary>
		/// Vertical coordinate in the row-layout.
		/// </summary>
		/// <remarks>
		/// <p class="body">OriginY specifies the vertical cell coordinate in the row layout. If OriginY is set to <see cref="RowLayoutColumnInfo.Relative"/> then it will be positioned relative to the last item in the row-layout.</p>
		/// <seealso cref="OriginX"/> <seealso cref="SpanX"/> <seealso cref="SpanY"/>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_OriginY") ]
		// SSP 7/8/03
		[ System.ComponentModel.MergableProperty( false ) ]
		public int OriginY
		{
			get
			{
				return this.originY;
			}
			set
			{
				if ( this.originY != value )
				{
					if ( value < 0 && RowLayoutColumnInfo.Relative != value )
						throw new ArgumentOutOfRangeException( "OriginY", value, SR.GetString( "LER_ArguementOutOfRangeException7" ) );

					this.originY = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.OriginY );
				}
			}
		}

		#endregion // OriginY

		#region SpanX

		/// <summary>
		/// Number of cells this item will span in the the row-layout horizontally. <see cref="RowLayoutColumnInfo.Remainder"/> constant specifies that this cell occupy remainder of space horizontally.
		/// </summary>
		/// <remarks>
		/// <p class="body">Number of cells this item will span in the the row-layout horizontally. <see cref="RowLayoutColumnInfo.Remainder"/> constant specifies that this cell occupy remainder of space horizontally.</p>
		/// <seealso cref="OriginX"/> <seealso cref="OriginY"/> <seealso cref="SpanY"/>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_SpanX") ]
		public int SpanX
		{
			get
			{
                if (RowLayoutColumnInfo.DEFAULT_SPAN == this.spanX)
                    return 2;                    

				return this.spanX;
			}
			set
			{
				if ( this.spanX != value )
				{
					if ( value <= 0 && RowLayoutColumnInfo.Remainder != value && 
						 RowLayoutColumnInfo.Relative != value && RowLayoutColumnInfo.DEFAULT_SPAN != value )
						throw new ArgumentOutOfRangeException( "SpanX", value, SR.GetString( "LER_ArguementOutOfRangeException8" ) );

					this.spanX = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.SpanX );
				}
			}
		}

		#endregion // SpanX

		#region SpanY

		/// <summary>
		/// Number of cells this item will span in the row layout vertically. <see cref="RowLayoutColumnInfo.Remainder"/> constant specifies that this cell occupy remainder of space vertically.
		/// </summary>
		/// <remarks>
		/// <p calss="body">Number of cells this item will span in the row layout vertically. <see cref="RowLayoutColumnInfo.Remainder"/> constant specifies that this cell occupy remainder of space vertically.</p>
		/// <seealso cref="OriginX"/> <seealso cref="OriginY"/> <seealso cref="SpanX"/>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_SpanY") ]
		public int SpanY
		{
			get
			{
                if (RowLayoutColumnInfo.DEFAULT_SPAN == this.spanY)
                    return 2;

				return this.spanY;
			}
			set
			{
				if ( this.spanY != value )
				{
					if ( value <= 0 && RowLayoutColumnInfo.Remainder != value && 
						RowLayoutColumnInfo.Relative != value && RowLayoutColumnInfo.DEFAULT_SPAN != value )
						throw new ArgumentOutOfRangeException( "SpanY", value, SR.GetString( "LER_ArguementOutOfRangeException9" ) );

					this.spanY = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.SpanY );
				}
			}
		}

		#endregion // SpanY

		#region OriginXResolved
		
		/// <summary>
		/// Returns the resolved OriginX.
		/// </summary>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Advanced ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int OriginXResolved
		{
			get
			{
				this.VerifyLayoutItemDimensions( );

				return this.cachedOriginXResolved;
			}
		}

		#endregion // OriginXResolved

		#region OriginYResolved
		
		/// <summary>
		/// Returns the resolved OriginY.
		/// </summary>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Advanced ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int OriginYResolved
		{
			get
			{
				this.VerifyLayoutItemDimensions( );

				return this.cachedOriginYResolved;
			}
		}

		#endregion // OriginYResolved

		#region SpanXResolved
		
		/// <summary>
		/// Returns the resolved SpanX.
		/// </summary>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Advanced ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int SpanXResolved
		{
			get
			{
				this.VerifyLayoutItemDimensions( );

				return this.cachedSpanXResolved;
			}
		}

		#endregion // SpanXResolved

		#region SpanYResolved
		
		/// <summary>
		/// Returns the resolved SpanY.
		/// </summary>
		[ Browsable( false ), EditorBrowsable( EditorBrowsableState.Advanced ), DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public int SpanYResolved
		{
			get
			{
				this.VerifyLayoutItemDimensions( );

				return this.cachedSpanYResolved;
			}
		}

		#endregion // SpanYResolved

		#region WeightX

		/// <summary>
		/// Indicates how the extra horizontal space will be distributed among items. Default value is 0.0. Higher values give higher priority.
		/// </summary>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_WeightX") ]
		public float WeightX
		{
			get
			{
				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// Use the weightXDefault if weightX is set to default.
				//
				//return this.weightX;
				return this.weightX > 0.0f ? this.weightX : this.weightXDefault;
			}
			set
			{
				if ( this.weightX != value )
				{
					if ( value < 0.0f )
						throw new ArgumentOutOfRangeException( "WeightX", SR.GetString( "LER_ArguementOutOfRangeException10" ) );

					this.weightX = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.WeightX );
				}
			}
		}

		#endregion // WeightX

		#region WeightY

		/// <summary>
		/// Indicates how the extra vertical space will be distributed among items. Default value is 0.0. Higher values give higher priority.
		/// </summary>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_WeightY") ]
		public float WeightY
		{
			get
			{
				return this.weightY;
			}
			set
			{
				if ( this.weightY != value )
				{
					if ( value < 0.0f )
						throw new ArgumentOutOfRangeException( "WeightY", SR.GetString( "LER_ArguementOutOfRangeException11" ) );

					this.weightY = value;

					// Notify the prop change
					//
					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.WeightY );
				}
			}
		}

		#endregion // WeightY

		#region AllowCellSizing

		/// <summary>
		/// Indicates whether the user is allowed to resize the cell.
		/// </summary>
		/// <remarks>
		/// <p class="body">Indicates whether the user is allowed to resize the cell associated with the column associated with this RowLayoutInfoColumn object.</p>
		/// <seealso cref="RowLayoutSizing"/> <seealso cref="AllowLabelSizing"/>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_AllowCellSizing") ]
		public RowLayoutSizing AllowCellSizing
		{
			get
			{
				return this.allowCellSizing;
			}
			set
			{
				if ( this.allowCellSizing != value )
				{
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.RowLayoutSizing ), value ) )
						throw new InvalidEnumArgumentException( "AllowCellSizing", (int)value, typeof( Infragistics.Win.UltraWinGrid.RowLayoutSizing ) );

					this.allowCellSizing = value;

                    // MRS - NAS 9.1 - Groups in RowLayout
                    this.NotifyPropChange(PropertyIds.AllowCellSizing);
				}
			}
		}

		#endregion // AllowCellSizing

		#region AllowLabelSizing

		/// <summary>
		/// Indicates whether the user is allowed to resize the column label.
		/// </summary>
		/// <remarks>
		/// <p class="body">Indicates whether the user is allowed to resize the cell associated with the column associated with this RowLayoutInfoColumn object.</p>
		/// <seealso cref="RowLayoutSizing"/> <seealso cref="AllowCellSizing"/>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_AllowLabelSizing") ]
		public RowLayoutSizing AllowLabelSizing
		{
			get
			{
				return this.allowLabelSizing;
			}
			set
			{
				if ( this.allowLabelSizing != value )
				{
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.RowLayoutSizing ), value ) )
						throw new InvalidEnumArgumentException( "AllowLabelSizing", (int)value, typeof( Infragistics.Win.UltraWinGrid.RowLayoutSizing ) );

					this.allowLabelSizing = value;

                    // MRS - NAS 9.1 - Groups in RowLayout
                    this.NotifyPropChange(PropertyIds.AllowLabelSizing);
				}
			}
		}

		#endregion // AllowLabelSizing

		#region CellInsets

		// SSP 12/1/03 UWG2641
		// Added CellInsets and LabelInsets properties.
		//
		/// <summary>
		/// Returns an instance of Insets object used for specifying the insets around the cells.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>Insets</b> property sepcifies the insets or spacing around the cells.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_CellInsets") ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		public Insets CellInsets
		{
			get
			{
				if ( null == this.cellInsets )
				{
					this.cellInsets = new Insets( );
					this.cellInsets.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.cellInsets;
			}
		}

		#endregion // CellInsets

		#region LabelInsets

		// SSP 12/1/03 UWG2641
		// Added CellInsets and LabelInsets properties.
		//
		/// <summary>
		/// Returns an instance of Insets object used for specifying the insets around the cells.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>Insets</b> property sepcifies the insets or spacing around the cells.</p>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayoutColumnInfo_P_LabelInsets") ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		public Insets LabelInsets
		{
			get
			{
				if ( null == this.labelInsets )
				{
					this.labelInsets = new Insets( );
					this.labelInsets.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.labelInsets;
			}
		}

		#endregion // LabelInsets

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

        #region Context

        /// <summary>
        /// Returns the associated column or group.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [LocalizedDescription("LDR_RowLayoutColumnInfo_P_Context")]
        public object Context
        {
            get
            {
                return this.context;
            }
        }

        #endregion // Context

        #region Group

        /// <summary>
        /// Returns the associated group or null if this RowLayoutColumnInfo is associated with a column.
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Advanced), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [LocalizedDescription("LDR_RowLayoutColumnInfo_P_Group")]
        public UltraGridGroup Group
        {
            get
            {
                return this.context as UltraGridGroup;
            }
        }

        #endregion // Group

        #region ParentGroup

        /// <summary>
        /// Returns the parent group which contains this group. This is only application when <see cref="UltraGridBand.RowLayoutStyle"/> is set to GroupLayout.
        /// </summary>
        [Editor(typeof(GroupUITypeEditor), typeof(UITypeEditor))]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [LocalizedDescription("LDR_RowLayoutColumnInfo_P_ParentGroup")]
        public UltraGridGroup ParentGroup
        {
            get
            {
                return this.parentGroup;
            }
            set
            {
                if (this.parentGroup != value)
                {
					// Make sure the new parent group is not a descendant of this group.
					if ( this.contextType == RowLayoutColumnInfoContext.Group && this.Group == value )
					{
						UltraGridGroup group = this.Group;
						UltraGridGroup parentGroup = value;
						while ( parentGroup != null )
						{
							if ( parentGroup == group )
								throw new ArgumentException( SR.GetString( "LE_RowLayoutColumnInfo_GroupParentedToSelf" ) ); // A group cannot be in its own ancestor chain.

							parentGroup = parentGroup.RowLayoutGroupInfo.ParentGroup;
						}
					}

                    this.parentGroup = value;

                    if (this.parentGroup == null)
                    {
                        this.parentGroupKey = null;
                        this.parentGroupIndex = -1;
                    }
                    else
                    {
                        this.parentGroupKey = this.parentGroup.Key;
                        this.parentGroupIndex = this.parentGroup.Index;
                    }

                    this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.ParentGroup);
                }
            }
        }

        #endregion // ParentGroup

        #region ParentGroupIndex

        /// <summary>
        /// Stores the Index of the parent group for serialization purposes. 
        /// </summary>        
        [Browsable(false)]
        [EditorBrowsable( EditorBrowsableState.Never)]         
        public int ParentGroupIndex
        {
            get
            {
                return this.parentGroupIndex;
            }
            set
            {
                this.parentGroupIndex = value;
            }
        }

        #endregion // ParentGroupIndex

        #region ParentGroupKey

        /// <summary>
        /// Stores the Key of the parent group for serialization purposes. 
        /// </summary>        
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string ParentGroupKey
        {
            get
            {
                return this.parentGroupKey;
            }
            set
            {
                this.parentGroupKey = value;
            }
        }

        #endregion // ParentGroupKey

        #region RowLayoutColumnInfoContext

        /// <summary>
        /// Returns an enum indicating whether this RowLayoutColumnInfo is associated with a Group or a Column.
        /// </summary>        
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        [LocalizedDescription("LDR_RowLayoutColumnInfo_P_RowLayoutColumnInfoContext")]
        public RowLayoutColumnInfoContext ContextType
        {
            get
            {
                return this.contextType;
            }
        }

        #endregion // RowLayoutColumnInfoContext

        #endregion //NAS 9.1 - Groups in RowLayout

        #endregion // Public Properties

        #region Protected Methods

        #region ShouldSerializeLabelPosition

        /// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeLabelPosition( )
		{
			return LabelPosition.Default != this.labelPosition;
		}

		#endregion // ShouldSerializeLabelPosition

		#region ShouldSerializeOriginX

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeOriginX( )
		{
			return RowLayoutColumnInfo.Relative != this.originX;
		}

		#endregion // ShouldSerializeOriginX

		#region ShouldSerializeOriginY

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeOriginY( )
		{
			return RowLayoutColumnInfo.Relative != this.originY;
		}

		#endregion // ShouldSerializeOriginY

		#region ShouldSerializeSpanX

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSpanX( )
		{
			return DEFAULT_SPAN != this.spanX;
		}

		#endregion // ShouldSerializeSpanX

		#region ShouldSerializeSpanY

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeSpanY( )
		{
			return DEFAULT_SPAN != this.spanY;
		}

		#endregion // ShouldSerializeSpanY

		#region ShouldSerializeWeightX

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeWeightX( )
		{
			return 0.0f != this.weightX;
		}

		#endregion // ShouldSerializeWeightX

		#region ShouldSerializeWeightY

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeWeightY( )
		{
			return 0.0f != this.weightY;
		}

		#endregion // ShouldSerializeWeightY

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ShouldSerializeContextType

        /// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeContextType()
		{
            return this.contextType != RowLayoutColumnInfoContext.Column;
        }

        #endregion // ShouldSerializeContextType

        #region ShouldSerializeMinimumLabelSize

        /// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMinimumLabelSize( )
		{
			return Size.Empty != this.minimumLabelSize;
		}

		#endregion // ShouldSerializeMinimumLabelSize

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ShouldSerializeParentGroup

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeParentGroup()
        {
            return this.parentGroup != null;
        }

        #endregion // ShouldSerializeParentGroup

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ShouldSerializeParentGroupIndex

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeParentGroupIndex()
        {
            return this.parentGroupIndex != -1;
        }

        #endregion // ShouldSerializeParentGroupIndex

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ShouldSerializeParentGroupKey

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeParentGroupKey()
        {
            return this.parentGroupKey != null && this.parentGroupKey.Length > 0;
        }

        #endregion // ShouldSerializeParentGroupKey

        #region ShouldSerializePreferredLabelSize

        /// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializePreferredLabelSize( )
		{
			return Size.Empty != this.preferredLabelSize;
		}

		#endregion // ShouldSerializePreferredLabelSize

		#region ShouldSerializeMinimumCellSize

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeMinimumCellSize( )
		{
			return Size.Empty != this.minimumCellSize;
		}

		#endregion // ShouldSerializeMinimumCellSize

		#region ShouldSerializePreferredCellSize

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializePreferredCellSize( )
		{
			return Size.Empty != this.preferredCellSize;
		}

		#endregion // ShouldSerializePreferredCellSize

		#region ShouldSerializeAllowCellSizing

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAllowCellSizing( )
		{
			return RowLayoutSizing.Default != this.allowCellSizing;
		}

		#endregion // ShouldSerializeAllowCellSizing

		#region ShouldSerializeAllowLabelSizing

		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeAllowLabelSizing( )
		{
			return RowLayoutSizing.Default != this.allowLabelSizing;
		}

		#endregion // ShouldSerializeAllowLabelSizing

		#region ShouldSerializeCellInsets

		// SSP 12/1/03 UWG2641
		// Added CellInsets and LabelInsets properties.
		//
		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCellInsets( )
		{
			return null != this.cellInsets && this.cellInsets.ShouldSerialize( );
		}

		#endregion // ShouldSerializeCellInsets

		#region ShouldSerializeLabelInsets

		// SSP 12/1/03 UWG2641
		// Added CellInsets and LabelInsets properties.
		//
		/// <summary>
		/// Returns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeLabelInsets( )
		{
			return null != this.labelInsets && this.labelInsets.ShouldSerialize( );
		}

		#endregion // ShouldSerializeLabelInsets

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ShouldSerializeLabelSpan

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeLabelSpan()
        {
            // MRS - NAS 9.1 - Groups in RowLayout
            //return 1 != this.labelSpan;
            return this.DefaultLabelSpan != this.labelSpan;
        }

        #endregion // ShouldSerializeLabelSpan

		#region OnSubObjectPropChanged

		// SSP 12/1/03 UWG2641
		// Added CellInsets and LabelInsets properties.
		//
		/// <summary>
		/// Listener notification
		/// </summary>
		/// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			if ( propChange.Source == this.cellInsets )
			{
				this.NotifyPropChange( PropertyIds.CellInsets, propChange );
			}
			else if ( propChange.Source == this.labelInsets )
			{
				this.NotifyPropChange( PropertyIds.LabelInsets, propChange );
			}
			else
				Debug.Assert( false, "Unknown sub object !" );
		}

		#endregion // OnSubObjectPropChanged

        #endregion // Protected Methods

        #region Public Methods

        #region Initialize

        /// <summary>
		/// Initializes the row layout info with the passed in arguments.
		/// </summary>
		/// <param name="originX">The origin X</param>
        /// <param name="originY">The origin Y</param>
		public void Initialize( int originX, int originY )
		{
			this.OriginX = originX;
			this.OriginY = originY;

			// MD 1/27/09 - Groups in RowLayouts
			// There were some recursion issues occurring and they are solved by pushing the new values to the cache.
			if ( originX >= 0 )
				this.cachedOriginXResolved = originX;

			if ( originY >= 0 )
				this.cachedOriginYResolved = originY;
		}

		/// <summary>
		/// Initializes the row layout info with the passed in arguments.
		/// </summary>
        /// <param name="originX">The origin X</param>
        /// <param name="originY">The origin Y</param>
		/// <param name="spanX">The span X</param>
		/// <param name="spanY">The span X</param>
		public void Initialize( int originX, int originY, int spanX, int spanY )
		{
			this.Initialize( originX, originY );
			this.SpanX = spanX;
			this.SpanY = spanY;

			// MD 1/27/09 - Groups in RowLayouts
			// There were some recursion issues occurring and they are solved by pushing the new values to the cache.
			Debug.Assert( spanX >= 0, "The span X was not initialized" );
			Debug.Assert( spanY >= 0, "The span Y was not initialized" );

			if ( spanX >= 0 )
				this.cachedSpanXResolved = spanX;

			if ( spanY >= 0 )
				this.cachedSpanYResolved = spanY;
		}

		/// <summary>
		/// Initializes the row layout info with the passed in arguments.
		/// </summary>
        /// <param name="originX">The origin X</param>
        /// <param name="originY">The origin Y</param>
        /// <param name="spanX">The span X</param>
        /// <param name="spanY">The span X</param>
		/// <param name="preferredCellWidth">0 for default.</param>
		/// <param name="preferredCellHeight">0 for default.</param>
		public void Initialize( int originX, int originY, int spanX, int spanY, int preferredCellWidth, int preferredCellHeight )
		{
			this.Initialize( originX, originY, spanX, spanY );
			this.PreferredCellSize = new Size( preferredCellWidth, preferredCellHeight );
		}

		/// <summary>
		/// Initializes the row layout info with the passed in arguments.
		/// </summary>
        /// <param name="originX">The origin X</param>
        /// <param name="originY">The origin Y</param>
        /// <param name="spanX">The span X</param>
        /// <param name="spanY">The span X</param>
		/// <param name="labelPosition">The label position</param>
		/// <param name="labelSpan">1 is the default.</param>
		public void Initialize( int originX, int originY, int spanX, int spanY, LabelPosition labelPosition, int labelSpan )
		{
			this.Initialize( originX, originY, spanX, spanY );
			this.LabelPosition = labelPosition;
			this.LabelSpan = labelSpan;
		}

		/// <summary>
		/// Initializes the row layout info with the passed in arguments.
		/// </summary>
        /// <param name="originX">The origin X</param>
        /// <param name="originY">The origin Y</param>
        /// <param name="spanX">The span X</param>
        /// <param name="spanY">The span X</param>
		/// <param name="weightX">0.0 is the default.</param>
		/// <param name="weightY">0.0 is the default.</param>
		public void Initialize( int originX, int originY, int spanX, int spanY, float weightX, float weightY )
		{
			this.Initialize( originX, originY, spanX, spanY );
			this.WeightX = weightX;
			this.WeightY = weightY;
		}

		/// <summary>
		/// Initializes the row layout info with the passed in arguments.
		/// </summary>
        /// <param name="originX">The origin X</param>
        /// <param name="originY">The origin Y</param>
        /// <param name="spanX">The span X</param>
        /// <param name="spanY">The span X</param>
		/// <param name="weightX">0.0 is the default.</param>
		/// <param name="weightY">0.0 is the default.</param>
		/// <param name="labelPosition">The label position.</param>
		/// <param name="labelSpan">1 is the default.</param>
		public void Initialize( int originX, int originY, int spanX, int spanY, float weightX, float weightY, LabelPosition labelPosition, int labelSpan )
		{
			this.Initialize( originX, originY, spanX, spanY, weightX, weightY );
			this.LabelPosition = labelPosition;
			this.LabelSpan = labelSpan;
		}

		/// <summary>
		/// Initializes the row layout info with the passed in arguments.
		/// </summary>
        /// <param name="originX">The origin X</param>
        /// <param name="originY">The origin Y</param>
        /// <param name="spanX">The span X</param>
        /// <param name="spanY">The span X</param>
		/// <param name="weightX">0.0 is the default.</param>
		/// <param name="weightY">0.0 is the default.</param>
		/// <param name="labelPosition">The label position.</param>
		/// <param name="labelSpan">1 is the default.</param>
		/// <param name="preferredCellWidth">0 for default.</param>
		/// <param name="preferredCellHeight">0 for default.</param>
		/// <param name="preferredLabelWidth">0 for default.</param>
		/// <param name="preferredLabelHeight">0 for default.</param>
		public void Initialize( int originX, int originY, int spanX, int spanY, float weightX, float weightY,
				LabelPosition labelPosition, int labelSpan, int preferredCellWidth, int preferredCellHeight, 
				int preferredLabelWidth, int preferredLabelHeight )
		{
			this.Initialize( originX, originY, spanX, spanY, weightX, weightY, labelPosition, labelSpan );
			this.PreferredCellSize = new Size( preferredCellWidth, preferredCellHeight );
			this.PreferredLabelSize = new Size( preferredLabelWidth, preferredLabelHeight );
		}

		#endregion // Initialize

		#region ResetOriginX

		/// <summary>
		/// Resets the property to its default value of <see cref="RowLayoutColumnInfo.Relative"/>.
		/// </summary>
		public void ResetOriginX( )
		{
			this.OriginX = RowLayoutColumnInfo.Relative;
		}

		#endregion // ResetOriginX

		#region ResetOriginY

		/// <summary>
		/// Resets the property to its default value of <see cref="RowLayoutColumnInfo.Relative"/>.
		/// </summary>
		public void ResetOriginY( )
		{
			this.OriginY = RowLayoutColumnInfo.Relative;
		}

		#endregion // ResetOriginY

		#region ResetSpanX

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSpanX( )
		{
			this.SpanX = DEFAULT_SPAN;
		}

		#endregion // ResetSpanX

		#region ResetSpanY

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetSpanY( )
		{
			this.SpanY = DEFAULT_SPAN;
		}

		#endregion // ResetSpanY

		#region ResetWeightX

		/// <summary>
		/// Resets the property to its default value,
		/// </summary>
		public void ResetWeightX( )
		{
			this.WeightX = 0.0f;
		}

		#endregion // ResetWeightX

		#region ResetWeightY

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetWeightY( )
		{
			this.WeightY = 0.0f;
		}

		#endregion // ResetWeightY

		#region ResetLabelPosition

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetLabelPosition( )
		{
			this.LabelPosition = LabelPosition.Default;
		}

		#endregion // ResetLabelPosition

		#region ResetLabelSpan

		/// <summary>
		/// Resets the property to its default value of <see cref="RowLayoutColumnInfo.Relative"/>.
		/// </summary>
		public void ResetLabelSpan( )
		{
            // MRS - NAS 9.1 - Groups in RowLayout
			//this.LabelSpan = 1;            
            this.LabelSpan = this.DefaultLabelSpan;
		}

		#endregion // ResetLabelSpan

		#region ResetMinimumLabelSize

		/// <summary>
		/// Resets the property to its default value of size with 0 width and 0 height.
		/// </summary>
		public void ResetMinimumLabelSize( )
		{
			this.MinimumLabelSize = Size.Empty;
		}

		#endregion // ResetMinimumLabelSize

		#region ResetPreferredLabelSize

		/// <summary>
		/// Resets the property to its default value of size with 0 width and 0 height.
		/// </summary>
		public void ResetPreferredLabelSize( )
		{
			this.PreferredLabelSize = Size.Empty;
		}

		#endregion // ResetPreferredLabelSize

		#region ResetMinimumCellSize

		/// <summary>
		/// Resets the property to its default value of size with 0 width and 0 height.
		/// </summary>
		public void ResetMinimumCellSize( )
		{
			this.MinimumCellSize = Size.Empty;
		}

		#endregion // ResetMinimumCellSize

		#region ResetPreferredCellSize

		/// <summary>
		/// Resets the property to its default value of size with 0 width and 0 height.
		/// </summary>
		public void ResetPreferredCellSize( )
		{
			this.PreferredCellSize = Size.Empty;
		}

		#endregion // ResetPreferredCellSize

		#region ResetAllowCellSizing

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAllowCellSizing( )
		{
			this.AllowCellSizing = RowLayoutSizing.Default;
		}

		#endregion // ResetAllowCellSizing

		#region ResetAllowLabelSizing

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetAllowLabelSizing( )
		{
			this.AllowLabelSizing = RowLayoutSizing.Default;
		}

		#endregion // ResetAllowLabelSizing

		#region ResetCellInsets

		// SSP 12/1/03 UWG2641
		// Added CellInsets and LabelInsets properties.
		//
		/// <summary>
		/// Resets the CellInsets property to the default value of an empty insets object.
		/// </summary>
		public void ResetCellInsets( )
		{
			if ( null != this.cellInsets )
				this.cellInsets.Reset( );
		}
		
		#endregion // ResetCellInsets

		#region ResetLabelInsets

		// SSP 12/1/03 UWG2641
		// Added CellInsets and LabelInsets properties.
		//
		/// <summary>
		/// Resets the LabelInsets property to the default value of an empty insets object.
		/// </summary>
		public void ResetLabelInsets( )
		{
			if ( null != this.labelInsets )
				this.labelInsets.Reset( );
		}

		#endregion // ResetLabelInsets

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ResetRowLayoutContextType

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetRowLayoutContextType()
        {
            this.contextType = RowLayoutColumnInfoContext.Column;
        }

        #endregion // ResetRowLayoutContextType

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ResetParentGroup

        /// <summary>
        /// Resets the property to its default value.
        /// </summary>
        public void ResetParentGroup()
        {
            this.ParentGroup = null;
        }

        #endregion // ResetParentGroup

        #region ShouldSerialize

        /// <summary>pp
		/// Returns true is any of the properties have been set to non-default values.
		/// </summary>
		/// <returns></returns>
		public bool ShouldSerialize( )
		{
			return 
				this.ShouldSerializeOriginX( ) ||
				this.ShouldSerializeOriginY( ) ||
				this.ShouldSerializeSpanX( ) ||
				this.ShouldSerializeSpanY( ) ||
				this.ShouldSerializeWeightX( ) ||
				this.ShouldSerializeWeightY( ) ||
				this.ShouldSerializeLabelPosition( ) ||
				this.ShouldSerializeLabelSpan( ) ||
				this.ShouldSerializeMinimumCellSize( ) ||
				this.ShouldSerializeMinimumLabelSize( ) ||
				this.ShouldSerializePreferredCellSize( ) ||
				this.ShouldSerializePreferredLabelSize( ) ||
				this.ShouldSerializeAllowCellSizing( ) ||
				this.ShouldSerializeAllowLabelSizing( ) ||
				// SSP 12/1/03 UWG2641
				// Added CellInsets and LabelInsets properties.
				//
				this.ShouldSerializeCellInsets( ) ||
				this.ShouldSerializeLabelInsets( ) ||

                // MRS - NAS 9.1 - Groups in RowLayout
                this.ShouldSerializeContextType() ||
                this.ShouldSerializeParentGroupKey() ||
                this.ShouldSerializeParentGroupIndex() ||

                this.ShouldSerializeTag();
		}

		#endregion // ShouldSerialize

		#region Reset

		/// <summary>
		/// Resets all the properties to their default values.
		/// </summary>
		public void Reset( )
		{
			this.ResetOriginX( );
			this.ResetOriginY( );
			this.ResetSpanX( );
			this.ResetSpanY( );
			this.ResetWeightX( );
			this.ResetWeightY( );
			this.ResetLabelPosition( );
			this.ResetLabelSpan( );
			this.ResetMinimumCellSize( );
			this.ResetMinimumLabelSize( );
			this.ResetPreferredCellSize( );
			this.ResetPreferredLabelSize( );
			this.ResetAllowCellSizing( );
			this.ResetAllowLabelSizing( );
			// SSP 12/1/03 UWG2641
			// Added CellInsets and LabelInsets properties.
			//
			this.ResetCellInsets( );
			this.ResetLabelInsets( );

            // MRS - NAS 9.1 - Groups in RowLayout
            this.ResetParentGroup();
		}

		#endregion // Reset

        
        #region GetColumnFromLayoutItem
        // MRS 5/31/06 - BR12912

        /// <summary>
		/// Returns the column associated with the specified layout item.
		/// </summary>
		/// <param name="item">The layout item.</param>
        /// <returns>The column associated with the specified layout item.</returns>
        public static UltraGridColumn GetColumnFromLayoutItem(ILayoutItem item)
        {
            return GridUtils.GetColumnFromLayoutItem(item);
        }

        #endregion // GetColumnFromLayoutItem

		// MD 1/12/09 - Groups in RowLayout
		#region GetHeaderFromLayoutItem

		/// <summary>
		/// Returns the header associated with the specified layout item.
		/// </summary>
		/// <param name="item">The layout item.</param>
		/// <returns>The header associated with the specified layout item.</returns>
		public static HeaderBase GetHeaderFromLayoutItem( ILayoutItem item )
		{
			return GridUtils.GetHeaderFromLayoutItem( item );
		}

		#endregion GetHeaderFromLayoutItem

		#endregion // Public Methods

		#region Implementation of ISerializable

        // MRS 12/19/06 - fxCop
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			this.SerializeTag( info );

			// Serialize the column.
			//
			if ( null != this.Column )
			{
				SerializedColumnID colId = this.Column.Band.GetSerializedColumnId( this.Column );

				Debug.Assert( null != colId, "GetSerializedColumnId returned null." );
				if ( null != colId )
					Utils.SerializeProperty( info, "Column", colId );
			}
			else if ( null != this.serializedColumnId )
			{
				Utils.SerializeProperty( info, "Column", this.serializedColumnId );
			}
			else
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//Debug.Assert( false, "No column !" );
                //
                // Serialize the group.
                //
                if (null != this.Group)
                {
                    SerializedGroupID groupId = this.Group.Band.GetSerializedGroupId(this.Group);

                    Debug.Assert(null != groupId, "GetSerializedGroupId returned null.");
                    if (null != groupId)
                        Utils.SerializeProperty(info, "Group", groupId);
                }
                else if (null != this.serializedGroupId)
                {
                    Utils.SerializeProperty(info, "Group", this.serializedGroupId);
                }
                else
                {
                    Debug.Assert(false, "No column or group !");
                }
			}

            // MRS - NAS 9.1 - Groups in RowLayout
            if (this.ShouldSerializeParentGroupKey())
            {
                Utils.SerializeProperty(info, "ParentGroupKey", this.parentGroupKey);
            }

            // MRS - NAS 9.1 - Groups in RowLayout
            if (this.ShouldSerializeParentGroupIndex())
            {
                Utils.SerializeProperty(info, "ParentGroupIndex", this.parentGroupIndex);
            }
            
			if ( this.ShouldSerializeLabelPosition( ) )
			{
				Utils.SerializeProperty( info, "LabelPosition", this.labelPosition );
			}

			if ( this.ShouldSerializeLabelSpan( ) )
			{
				Utils.SerializeProperty( info, "LabelSpan", this.labelSpan );
			}

			if ( this.ShouldSerializeMinimumCellSize( ) )
			{
				Utils.SerializeProperty( info, "MinimumCellSize", this.minimumCellSize );
			}

			if ( this.ShouldSerializeMinimumLabelSize( ) )
			{
				Utils.SerializeProperty( info, "MinimumLabelSize", this.minimumLabelSize );
			}

			if ( this.ShouldSerializeOriginX( ) )
			{
				Utils.SerializeProperty( info, "OriginX", this.originX );
			}

			if ( this.ShouldSerializeOriginY( ) )
			{
				Utils.SerializeProperty( info, "OriginY", this.originY );
			}

			if ( this.ShouldSerializePreferredCellSize( ) )
			{
				Utils.SerializeProperty( info, "PreferredCellSize", this.preferredCellSize );
			}

			if ( this.ShouldSerializePreferredLabelSize( ) )
			{
				Utils.SerializeProperty( info, "PreferredLabelSize", this.preferredLabelSize );
			}

			if ( this.ShouldSerializeSpanX( ) )
			{
				Utils.SerializeProperty( info, "SpanX", this.spanX );
			}

			if ( this.ShouldSerializeSpanY( ) )
			{
				Utils.SerializeProperty( info, "SpanY", this.spanY );
			}

			if ( this.ShouldSerializeWeightX( ) )
			{
				Utils.SerializeProperty( info, "WeightX", this.weightX );
			}

			if ( this.ShouldSerializeWeightY( ) )
			{
				Utils.SerializeProperty( info, "WeightY", this.weightY );
			}

			if ( this.ShouldSerializeAllowCellSizing( ) )
			{
				Utils.SerializeProperty( info, "AllowCellSizing", this.allowCellSizing );
			}

			if ( this.ShouldSerializeAllowLabelSizing( ) )
			{
				Utils.SerializeProperty( info, "AllowLabelSizing", this.allowLabelSizing );
			}

			// SSP 12/1/03 UWG2641
			// Added CellInsets and LabelInsets properties.
			//
			// --------------------------------------------------------------------
			if ( this.ShouldSerializeCellInsets( ) )
			{
				Utils.SerializeProperty( info, "CellInsets", this.cellInsets );
			}

			if ( this.ShouldSerializeLabelInsets( ) )
			{
				Utils.SerializeProperty( info, "LabelInsets", this.labelInsets );
			}
			// --------------------------------------------------------------------

			// SSP 4/7/05 BR03193
			// We need to store the Hidden property values of the columns with a RowLayout.
			//
			if ( DefaultableBoolean.Default != this.columnHidden )
			{
				Utils.SerializeProperty( info, "ColumnHidden", this.columnHidden );
			}

            // MRS - NAS 9.1 - Groups in RowLayout
            if (this.ShouldSerializeContextType())
            {
                Utils.SerializeProperty(info, "RowLayoutColumnInfoContext", this.contextType);
            }
		}        
				
		#endregion // Implementation of ISerializable

		#region IGridBagConstraint interface implementation

		#region Anchor

		/// <summary>
		/// If the display area of the item is larger than the item, this property indicates where to anchor the item.
		/// </summary>
		AnchorType IGridBagConstraint.Anchor 
		{ 
			get
			{
				return AnchorType.Center;
			}
		}

		#endregion // Anchor

		#region Fill

		/// <summary>
		/// Fill indicates whether to resize the item to fill the extra spance if the layout item's display area is larger than its size.
		/// </summary>
		FillType IGridBagConstraint.Fill 
		{ 
			get
			{
				return Infragistics.Win.Layout.FillType.Both;
			}
		}

		#endregion // Fill

		#region Insets

		// SSP 12/1/03
		//
		private static Insets emptyInsets = new Insets( );
		
		/// <summary>
		/// Indicates the padding around the layout item.
		/// </summary>
		Insets IGridBagConstraint.Insets 
		{ 
			get
			{
				// Return an empty inset which is what the column's RowLayoutColumnInfo retruns.
				//
				// SSP 12/1/03
				//
				//return this.Insets;
				return RowLayoutColumnInfo.emptyInsets;
			}
		}

		#endregion // Insets

		#region OriginX

		/// <summary>
		/// <p>OriginX and OriginY define where the layout item will be placed in the virtual grid of the row layout. OriginX specifies the location horizontally while specifies the location vertically. These locations are the coordinates of the cells in the virtual grid that the row layout represents.</p>
		/// <p>The leftmost cell has OriginX of 0. The constant <see cref="GridBagConstraintConstants.Relative"/> specifies that the item be placed just to the right of the item that was added to the layout manager just before this item was added. </p>
		/// <p>The default value is <see cref="GridBagConstraintConstants.Relative"/>. OriginX should be a non-negative value.</p>
		/// </summary>
		int IGridBagConstraint.OriginX
		{ 
			get
			{
				return this.OriginX;
			}
		}

		#endregion // OriginX

		#region OriginY

		/// <summary>
		/// <p>OriginX and OriginY define where the layout item will be placed in the virtual grid of the row-layout. OriginX specifies the location horizontally while specifies the location vertically. These locations are the coordinates of the cells in the virtual grid that the row-layout represents.</p>
		/// <p>The topmost cell has OriginY of 0. The constant <see cref="GridBagConstraintConstants.Relative"/> specifies that the item be placed just below the item that was added to the layout manager just before this item was added.</p>
		/// <p>The default value is <see cref="GridBagConstraintConstants.Relative"/>. OriginY should be a non-negative value.</p>
		/// </summary>
		int IGridBagConstraint.OriginY 
		{ 
			get
			{
				return this.OriginY;
			}
		}

		#endregion // OriginY

		#region SpanX

		/// <summary>
		/// Specifies the number of cells this item will span horizontally. The constant <see cref="GridBagConstraintConstants.Remainder"/> specifies that this item be the last one in the row and thus occupy remaining space.
		/// </summary>
		int IGridBagConstraint.SpanX 
		{ 
			get
			{
				return this.SpanX;
			}
		}

		#endregion // SpanX

		#region SpanY

		/// <summary>
		/// Specifies the number of cells this item will span vertically. The constant <see cref="GridBagConstraintConstants.Remainder"/> specifies that this item be the last one in the column and thus occupy remaining space.
		/// </summary>
		int IGridBagConstraint.SpanY 
		{ 
			get
			{
				return this.SpanY;
			}
		}

		#endregion // SpanY

		#region WeightX

		/// <summary>
		/// Indicates how the extra horizontal space will be distributed among items. Default value is 0.0. Higher values give higher priority. The weight of the column in the virtual grid the row-layout layout represents is the maximum WeightX of all the items in the row.
		/// </summary>
		float IGridBagConstraint.WeightX 
		{ 
			get
			{
				return this.WeightX;
			}
		}

		#endregion // WeightX

		#region WeightY

		/// <summary>
		/// Indicates how the extra vertical space will be distributed among items. Default value is 0.0. Higher values give higher priority. The weight of the column in the virtual grid the grid-bag layout represents is the maximum WeightY of all the items in the column.
		/// </summary>
		float IGridBagConstraint.WeightY 
		{ 
			get
			{
				return this.WeightY;
			}
		}

		#endregion // WeightY

		#endregion // IGridBagConstraint interface implementation

		#region Implementation of ILayoutItem

		/// <summary>
		/// Returns the ideal size.
		/// </summary>
		System.Drawing.Size ILayoutItem.PreferredSize
		{
			get
			{
				// SSP 12/1/03
				// This ILayout item is used for figuring out the OriginX/OriginY/SpanX/SpanY Resolved
				// and thus does not need any size.
				//
				//return this.PreferredCellSize;
				return new Size( 0, 0 );
			}
		}

		/// <summary>
		/// Returns the minimum size.
		/// </summary>
		System.Drawing.Size ILayoutItem.MinimumSize
		{
			get
			{
				// SSP 12/1/03
				// This ILayout item is used for figuring out the OriginX/OriginY/SpanX/SpanY Resolved
				// and thus does not need any size.
				//
				//return this.PreferredLabelSize;
				return new Size( 0, 0 );
			}
		}

		/// <summary>
		/// Indicates whether this column is visible.
		/// </summary>
		bool ILayoutItem.IsVisible
		{
			get
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//return this.Column.IsVisibleInLayout;
                switch (this.ContextType)
                {
                    case RowLayoutColumnInfoContext.Column:
                        return this.Column.IsVisibleInLayout;
                    case RowLayoutColumnInfoContext.Group:
                        return this.Group.IsVisibleInLayout;
                    default:
                        Debug.Fail("Unknown ContextType");
                        return false;

                }                
			}
		}
		
		#endregion // Implementation of ILayoutItem

		#region RowLayoutColumnInfoTypeConverter Class

		/// <summary>
		/// RowLayoutTypeConverter type converter.
		/// </summary>
		public sealed class RowLayoutColumnInfoTypeConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if ( destinationType == typeof( InstanceDescriptor ) ) 
				{
					return true;
				}
				
				return base.CanConvertTo( context, destinationType );
			}

            /// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) 
			{
				if ( destinationType == null ) 
				{
					throw new ArgumentNullException( "destinationType" );
				}

				if ( destinationType == typeof( InstanceDescriptor ) && value is RowLayoutColumnInfo ) 
				{
					RowLayoutColumnInfo item = (RowLayoutColumnInfo)value;

                    // MRS - NAS 9.1 - Groups in RowLayout
                    #region Old Code
                    //ConstructorInfo ctor = typeof( RowLayoutColumnInfo ).GetConstructor(
                    //    // SSP 4/7/05 BR03193
                    //    // We need to store the Hidden property values of the columns with a RowLayout.
                    //    //
                    //    //new Type[] { typeof( string ), typeof( int ) } );
                    //    new Type[] { typeof( string ), typeof( int ), typeof( DefaultableBoolean ) } );

                    //if (null != ctor)
                    //{
                    //    // false as the last parameter here causes generation of a local variable for the type 

                    //    UltraGridColumn column = item.column;
                    //    int unboundColumnIndex = -1;

                    //    // MRS - NAS 9.1 - Groups in RowLayout

                    //    if (null != column && null != column.Band && null != column.Band.Columns && !column.IsBound)
                    //    {
                    //        unboundColumnIndex = column.Index - column.Band.Columns.BoundColumnsCount;
                    //    }

                    //    // SSP 4/7/05 BR03193
                    //    // We need to store the Hidden property values of the columns with a RowLayout.
                    //    //
                    //    //return new InstanceDescriptor( ctor, new object[] { item.Key, unboundColumnIndex }, false );
                    //    return new InstanceDescriptor(ctor, new object[] { item.Key, unboundColumnIndex, item.columnHidden }, false);
                    //}
                    #endregion //Old Code
                    //
                    ConstructorInfo ctor;                    
                    ctor = typeof(RowLayoutColumnInfo).GetConstructor(
                        new Type[] { typeof(RowLayoutColumnInfoContext), typeof(string), typeof(int), typeof(DefaultableBoolean) });
                    
                    int unboundColumnIndex = -1;                                        
                    if (item.ContextType == RowLayoutColumnInfoContext.Column)
                    {
                        UltraGridColumn column = item.Column;
                        if (null != column && null != column.Band && null != column.Band.Columns && !column.IsBound)
                        {
                            unboundColumnIndex = column.Index - column.Band.Columns.BoundColumnsCount;
                        }
                    }

                    return new InstanceDescriptor(ctor, new object[] { item.ContextType, item.Key, unboundColumnIndex, item.columnHidden }, false);
                }

				return base.ConvertTo(context, culture, value, destinationType);
			}
		}
		
		#endregion // RowLayoutColumnInfoTypeConverter Class

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

        #region DefaultLabelSpan
        private int DefaultLabelSpan
        {
            get
            {
                switch (this.ContextType)
                {
                    case RowLayoutColumnInfoContext.Group:
                        return -1;
                    case RowLayoutColumnInfoContext.Column:                        
                    default:
                        Debug.Assert(this.ContextType == RowLayoutColumnInfoContext.Column, "Unknown ContextType");
                        return 1;
                }
            }
        }
        private int DefaultLabelSpanResolved
        {
            get
            {
                if (this.ContextType == RowLayoutColumnInfoContext.Group)
                {
                    UltraGridBand band = this.GetBand();
                    if (band != null)
                    {
                        return band.AreColumnHeadersInSeparateLayoutArea 
                            ? 2
                            : 1;
                    }
                }
                
                return 1;                
            }
        }
        #endregion // DefaultLabelSpanResolved

        #endregion // NAS 9.1 - Groups in RowLayout
    }

	#endregion // RowLayoutColumnInfo

	#region ColumnLayoutInfo

	/// <summary>
	/// Class for holding row layout information for a column.
	/// </summary>
	[ Serializable( ), TypeConverter( typeof( ExpandableObjectConverter ) ) ]
	public class ColumnLayoutInfo : RowLayoutColumnInfo
	{
		internal ColumnLayoutInfo( UltraGridColumn column ) : base( column )
		{
		}

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected ColumnLayoutInfo(SerializationInfo info, StreamingContext context)
            : base(info, context)
		{
		}

		#region Clone

		// SSP 8/2/05 - NAS 5.3 Column Swapping in Row-Layout
		// 
		internal override RowLayoutColumnInfo Clone( )
		{
			ColumnLayoutInfo ci = new ColumnLayoutInfo( this.Column );
			ci.InitializeFrom( this );
			return ci;
		}

		#endregion // Clone
	}

	#endregion // ColumnLayoutInfo 

    // MRS - NAS 9.1 - Groups in RowLayout
    #region SerializedGroupID
    /// <summary>
    /// Internal class used for serialization.
    /// </summary>
    [Serializable()]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class SerializedGroupID : ISerializable
    {
        private int relativeIndex;        
        private string key;

        // Constructor
        //
        internal SerializedGroupID(int relativeIndex,                                     
                                     string key)
        {
            this.relativeIndex = relativeIndex;
            this.key = key;
        }

        internal int RelativeIndex { get { return this.relativeIndex; } }        
        internal string Key { get { return this.key; } }


        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info,
            StreamingContext context)
        {
            Utils.SerializeProperty(info, "Key", this.key);
            Utils.SerializeProperty(info, "RelativeIndex", this.relativeIndex);
        }

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected SerializedGroupID(SerializationInfo info, StreamingContext context)
        {
            foreach (SerializationEntry entry in info)
            {
                switch (entry.Name)
                {
                    case "Key":
                        this.key = (string)Utils.DeserializeProperty(entry, typeof(string), this.key);
                        break;

                    case "RelativeIndex":
                        this.relativeIndex = (int)Utils.DeserializeProperty(entry, typeof(int), this.relativeIndex);
                        break;                    
                }
            }
        }

    }
    #endregion //SerializedGroupID

    // MRS - NAS 9.1 - Groups in RowLayout
    #region GroupUITypeEditor

    /// <summary>
    /// A UITypeEditor providing the selection of a template without the
    /// UI that is provided by the IListSource implementation.
    /// </summary>
    [System.Runtime.InteropServices.ComVisible(false)]
    
    
    public class GroupUITypeEditor : Infragistics.Shared.UITypeEditorListBase
    {
        #region Base Class Overrides

        #region GetListItems

        /// <summary>
        /// Returns the set of items to show on the list.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that can be used to gain additional context information.</param>
        /// <param name="provider">An IServiceProvider that this editor can use to obtain services.</param>
        /// <param name="value">The object to edit.</param>
        /// <param name="currentEntry"></param>
        /// <returns>An array of objects containing the list items.</returns>
        protected override object[] GetListItems(ITypeDescriptorContext context, IServiceProvider provider, object value, ref object currentEntry)
        {
            RowLayoutColumnInfo rowLayoutColumnInfo = context.Instance as RowLayoutColumnInfo;
            if (rowLayoutColumnInfo == null)
                return null;

            UltraGridBand band = rowLayoutColumnInfo.GetBand();
            if (band == null)
                return null;

			// MD 1/26/09 - Groups in RowLayouts
			// Filter out the group from its own parent group list.
            //return band.Groups.All;
            // MRS 2/24/2009 - TFS14497
            //
            #region Old Code
            //object[] groups = band.Groups.All;

            //UltraGridGroup associatedGroup = rowLayoutColumnInfo.Group;
            //if ( associatedGroup != null )
            //{
            //    ArrayList groupsList = new ArrayList( groups );

            //    for ( int i = groupsList.Count - 1; i >= 0; i-- )
            //    {
            //        if ( (UltraGridGroup)groupsList[ i ] == associatedGroup )
            //            groupsList.RemoveAt( i );
            //    }

            //    groups = groupsList.ToArray();
            //}

            //return groups;
            #endregion //Old Code
            //
            UltraGridGroup associatedGroup = rowLayoutColumnInfo.Group;
            List<UltraGridGroup> groups = new List<UltraGridGroup>();
            foreach (UltraGridGroup group in band.Groups)
            {
                if (group == associatedGroup)
                    continue;

                if (UltraGridGroup.IsDescendantOfGroup(associatedGroup, group))
                    continue;

                groups.Add(group);
            }

            return groups.ToArray();
        }
        #endregion //GetListItems

        #region ValidateEditItem

        /// <summary>
        /// Validates an item selected by the user on the list and returns it.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that can be used to gain additional context information.</param>
        /// <param name="provider">An IServiceProvider that this editor can use to obtain services.</param>
        /// <param name="value">The object to edit.</param>
        /// <param name="selectedEntry"></param>
        /// <returns>An array of objects containing the list items.</returns>
        protected override object ValidateEditItem(ITypeDescriptorContext context, IServiceProvider provider, object value, object selectedEntry)
        {            
            return selectedEntry;
        }
        #endregion //ValidateEditItem

        #endregion //Base Class Overrides
    }
    #endregion //RowEditTemplateUITypeEditor
}
