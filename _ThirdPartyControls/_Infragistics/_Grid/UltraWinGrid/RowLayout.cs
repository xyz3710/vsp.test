#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;
using Infragistics.Win.Layout;

// SSP 1/30/03 - Row Layout Functionality
// Added RowLayout.cs file.
//

namespace Infragistics.Win.UltraWinGrid
{
	/// <summary>
	/// RowLayout class. The RowLayout class contains information on how the cells in a row get laid out.
	/// </summary>
	/// <remarks>
	/// <p><seealso cref="UltraGridBand.UseRowLayout"/>, <seealso cref="UltraGridColumn.RowLayoutColumnInfo"/></p>
	/// </remarks>
	[ TypeConverter( typeof( RowLayout.RowLayoutTypeConverter ) ) ]
	[ Serializable( )  ]
	public class RowLayout : KeyedSubObjectBase, ISerializable
	{
		#region Private Variables

		private RowLayoutColumnInfosCollection columnInfos = null;
		private RowLayoutsCollection parentCollection = null;
		private LabelPosition rowLayoutLabelPosition = LabelPosition.Default;
		private RowLayoutLabelStyle rowLayoutLabelStyle = RowLayoutLabelStyle.Separate;
		private bool cardView = false;
		private CardStyle cardViewStyle = CardStyle.MergedLabels;

        // MRS - NAS 9.1 - Groups in RowLayout
        private RowLayoutStyle rowLayoutStyle = RowLayoutStyle.None;
		#endregion // Private Variables

		#region Constructor

		/// <summary>
		/// Constructor.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public RowLayout( string key ) : base( key )
		{
			
		}
		
		internal RowLayout( RowLayoutsCollection parentCollection, string key ) : base( key )
		{
			if ( null == parentCollection )
				throw new ArgumentNullException( "parentCollection" );

			this.parentCollection = parentCollection;
			
			this.Reset( );
		}

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected RowLayout(SerializationInfo info, StreamingContext context)
		{
			this.Reset( );

			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop.
			//
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "Tag":
						this.DeserializeTag(entry);
						break;
					case "Key":
						this.Key = (string)Utils.DeserializeProperty( entry, typeof( string) , this.Key );
						break;
					case "ColumnInfos":
						this.columnInfos = (RowLayoutColumnInfosCollection)Utils.DeserializeProperty( entry, typeof( RowLayoutColumnInfosCollection ), null );
						break;
					case "RowLayoutLabelPosition":
						this.rowLayoutLabelPosition = (LabelPosition)Utils.DeserializeProperty( entry, typeof( Infragistics.Win.UltraWinGrid.LabelPosition ), this.rowLayoutLabelPosition );
						break;
					case "RowLayoutLabelStyle":
						this.rowLayoutLabelStyle = (RowLayoutLabelStyle)Utils.DeserializeProperty( entry, typeof( RowLayoutLabelStyle ), this.rowLayoutLabelStyle );
						break;
					case "CardView":
						this.cardView = (bool)Utils.DeserializeProperty( entry, typeof( bool ), this.cardView );
						break;
					case "CardViewStyle":
						this.cardViewStyle = (CardStyle)Utils.DeserializeProperty( entry, typeof( CardStyle ), this.cardViewStyle );
						break;
                    // MRS - NAS 9.1 - Groups in RowLayout
                    case "RowLayoutStyle":
                        this.rowLayoutStyle = (RowLayoutStyle)Utils.DeserializeProperty(entry, typeof(RowLayoutStyle), this.rowLayoutStyle);
                        break;
					default:
						Debug.Assert( false, "Invalid entry in RowLayout de-serialization constructor." );
						break;
				}
			}
		}

		#endregion // Constructor

		#region Overridden Properties

		/// <summary>
		/// Returns or sets a value that uniquely identifies an object in a collection.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Key</b> property is a unique, user-defined object identification string that can be used interchangeably with the <b>Index</b> of an object when accessing it through code. If you attempt to assign a value to the <b>Key</b> property is not unique in the collection, an error will occur.</p>
		/// <p class="body">The value of the <b>Index</b> property of an object can change when objects in the collection are reordered, such as when you add or remove objects. If you expect the <b>Index</b> property to change dynamically, refer to objects in a collection using the <b>Key</b> property. In addition, you can use the <b>Key</b> property to make your program "self-documenting" by assigning meaningful names to the objects in a collection.</p>
		/// <p class="body">You can set the <b>Key</b> property when you use the <b>Add</b> method to add an object to a collection. In some instances, the <b>Key</b> property of an object may be blank if that object does not appear in a collection.</p>
		/// <p class="body">Also, note that the uniqueness of keys is only enforced when the <b>Key</b> property is set to a value. If a collection supports objects with blank keys, that collection may contain multiple objects that whose <b>Key</b> property is empty. In that case, you must use <b>Index</b> property to differentiate between the objects that have blank keys.</p>
		/// </remarks>
		[ LocalizedCategory("LC_Default") ]
		[ MergableProperty( false ) ]
		[ Browsable( false ) ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ) ]
		public override String Key
		{
			get
			{
				return base.Key;
			}
		}

		#endregion // Overridden Properties

		#region Internal Properties/Methods

		#region ParentCollection

		internal RowLayoutsCollection ParentCollection
		{
			get
			{
				return this.parentCollection;
			}
		}

		#endregion // ParentCollection

		#region Band

		internal UltraGridBand Band
		{
			get
			{
				return null != this.ParentCollection ? this.ParentCollection.Band : null;
			}
		}

		#endregion // Band

		#region InitializeFrom
		
		internal void InitializeFrom( RowLayout source )
		{
			this.rowLayoutLabelPosition = source.rowLayoutLabelPosition;
			this.rowLayoutLabelStyle = source.rowLayoutLabelStyle;
			this.cardView = source.cardView;
			this.cardViewStyle = source.cardViewStyle;

            // MRS - NAS 9.1 - Groups in RowLayout
            this.rowLayoutStyle = source.rowLayoutStyle;

			if ( null != source.columnInfos )
				this.ColumnInfos.InitializeFrom( source.columnInfos );
			else if ( null != this.columnInfos )
				this.columnInfos.Reset( );
		}

		#endregion // InitializeFrom

		#region CopyCurrent

		private void CopyCurrent( )
		{
			this.Reset( );

			for ( int i = 0; i < this.Band.Columns.Count; i++ )
			{
				UltraGridColumn column = this.Band.Columns[ i ];
				this.ColumnInfos[ column ].InitializeFrom( column.RowLayoutColumnInfo );

				// SSP 4/7/05 BR03193
				// We need to store the Hidden property values of the columns with a RowLayout and
				// restore them. 
				//
				this.ColumnInfos[ column ].columnHidden = column.Hidden ? DefaultableBoolean.True : DefaultableBoolean.False;
			}

            // MRS - NAS 9.1 - Groups in RowLayout
            for (int i = 0; i < this.Band.Groups.Count; i++)
            {
                UltraGridGroup group = this.Band.Groups[i];
                this.ColumnInfos[group].InitializeFrom(group.RowLayoutGroupInfo);

                // SSP 4/7/05 BR03193
                // We need to store the Hidden property values of the groups with a RowLayout and
                // restore them. 
                //
                this.ColumnInfos[group].columnHidden = group.Hidden ? DefaultableBoolean.True : DefaultableBoolean.False;
            }

			this.RowLayoutLabelPosition = this.Band.RowLayoutLabelPosition;
			this.RowLayoutLabelStyle = this.Band.RowLayoutLabelStyle;
			this.CardView = this.Band.CardView;
			this.CardViewStyle = this.Band.CardSettings.Style;

            // MRS - NAS 9.1 - Groups in RowLayout
            this.RowLayoutStyle = this.Band.RowLayoutStyle;
		}

		#endregion // CopyCurrent

		#endregion // Internal Properties/Methods

		#region Public Properties

		#region ColumnInfos

		/// <summary>
		/// Returns a keyed collection containing RowLayoutColumnInfo objects for each column in the band.
		/// </summary>
		[ LocalizedDescription("LDR_RowLayout_P_ColumnInfos") ]
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Content ) ]
		public RowLayoutColumnInfosCollection ColumnInfos
		{
			get
			{
				if ( null == this.columnInfos )
					this.columnInfos = new RowLayoutColumnInfosCollection( this );

				return this.columnInfos;
			}
		}

		#endregion // ColumnInfos

		#region RowLayoutLabelPosition

		/// <summary>
		/// Gets or sets the label position. Label position indicates the position of the column labels in the row layout mode.
		/// </summary>
		/// <remarks>
		/// <p>Label position indicates where to position the column label in relation to the associated cell in row layout mode.</p>
        /// <p><see cref="UltraGridBand.RowLayoutLabelStyle"/> must be set to <b>WithCellData</b> for this to take effect in regular view (non-card view) mode. In card view, <see cref="UltraGridCardSettings.Style"/> must be set to a value other than <b>MergedLabels</b>.</p>
        /// <seealso cref="RowLayoutColumnInfo.LabelPosition"/> <seealso cref="UltraGridColumn.RowLayoutColumnInfo"/> <seealso cref="UltraGridBand.RowLayoutLabelPosition"/>
		/// </remarks>
		[ LocalizedDescription("LDR_RowLayout_P_RowLayoutLabelPosition") ]
		public LabelPosition RowLayoutLabelPosition
		{
			get
			{
				return this.rowLayoutLabelPosition;
			}
			set
			{
				if ( this.rowLayoutLabelPosition != value )
				{
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.LabelPosition ), value ) )
						throw new InvalidEnumArgumentException( "RowLayoutLabelPosition", (int)value, typeof( Infragistics.Win.UltraWinGrid.LabelPosition ) );

					this.rowLayoutLabelPosition = value;

					this.NotifyPropChange( PropertyIds.LabelPosition );
				}
			}
		}

		#endregion // RowLayoutLabelPosition
	
		#region RowLayoutLabelStyle

		/// <summary>
		/// Gets or sets the row-layout label style.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridBand.RowLayoutLabelStyle"/>
		/// </remarks>
		public RowLayoutLabelStyle RowLayoutLabelStyle
		{
			get
			{
				return this.rowLayoutLabelStyle;
			}
			set
			{
				if ( this.rowLayoutLabelStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( RowLayoutLabelStyle ), value ) )
						throw new InvalidEnumArgumentException( "RowLayoutLabelStyle", (int)value, typeof( RowLayoutLabelStyle ) );

					this.rowLayoutLabelStyle = value;
				}
			}
		}

		#endregion // RowLayoutLabelStyle

		#region CardView

		/// <summary>
		/// Gets or sets the card view.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridBand.CardView"/>
		/// </remarks>
		[ LocalizedDescription("LD_UltraGridBand_P_CardView")]
		public bool CardView
		{
			get
			{
				return this.cardView;
			}
			set
			{
				this.cardView = value;
			}
		}

		#endregion // CardView

		#region CardViewStyle

		/// <summary>
		/// Gets or sets the card-view style. Only has any effect if CardView is set to true.
		/// </summary>
		/// <remarks>
		/// <seealso cref="UltraGridCardSettings.Style"/>
		/// </remarks>
		[LocalizedDescription("LD_UltraGridCardSettings_P_Style")]
		public CardStyle CardViewStyle
		{
			get
			{
				return this.cardViewStyle;
			}
			set
			{
				if ( this.cardViewStyle != value )
				{
					if ( ! Enum.IsDefined( typeof( CardStyle ), value ) )
						throw new InvalidEnumArgumentException( "CardViewStyle", (int)value, typeof( CardStyle ) );

					this.cardViewStyle = value;
				}
			}
		}

		#endregion // CardViewStyle

        // MRS - NAS 9.1 - Groups in RowLayout
        #region RowLayoutStyle

        /// <summary>
        /// Gets or sets the row-layout style.
        /// </summary>        
        /// <seealso cref="UltraGridBand.RowLayoutStyle"/>
        public RowLayoutStyle RowLayoutStyle
        {
            get
            {
                return this.rowLayoutStyle;
            }
            set
            {
                if (this.rowLayoutStyle != value)
                {
                    if (!Enum.IsDefined(typeof(RowLayoutStyle), value))
                        throw new InvalidEnumArgumentException("RowLayoutStyle", (int)value, typeof(RowLayoutStyle));

                    this.rowLayoutStyle = value;
                }
            }
        }

        #endregion // RowLayoutStyle

        #endregion // Public Properties

        #region Public Methods

        #region Apply

        /// <summary>
		/// Applies the row layout.
		/// </summary>
		public void Apply( )
		{
			for ( int i = 0; i < this.ColumnInfos.Count; i++ )
			{
				RowLayoutColumnInfo ci = this.ColumnInfos[i];

				ColumnsCollection columns = this.Band.Columns;

                // MRS - NAS 9.1 - Groups in RowLayout
                GroupsCollection groups = this.Band.Groups;

				RowLayoutColumnInfo dest = null;

				// First try at the same location as the column info in the collection.
				//
				if ( i < columns.Count && columns[i].RowLayoutColumnInfo.CanInitializeFrom( ci ) )
				{
					// SSP 4/7/05 BR03193
					//
					// --------------------------------------------------------------
					//columns[i].RowLayoutColumnInfo.InitializeFrom( ci );
					//continue;
					dest = columns[i].RowLayoutColumnInfo;
					// --------------------------------------------------------------
				}

				// If that fails, then loop over all the columns and try to find a matching column.
				//
				if ( null == dest )
				{
                    // MRS - NAS 9.1 - Groups in RowLayout
					//for ( int j = 0; j < columns.Count; i++ )
                    for (int j = 0; j < columns.Count; j++)
					{
						if ( columns[j].RowLayoutColumnInfo.CanInitializeFrom( ci ) )
						{
							// SSP 4/7/05 BR03193
							//
							// --------------------------------------------------------------
							//columns[j].RowLayoutColumnInfo.InitializeFrom( ci );
							dest = columns[j].RowLayoutColumnInfo;
							// --------------------------------------------------------------
							break;
						}
					}
				}

                // MRS - NAS 9.1 - Groups in RowLayout
                // If that fails, then loop over all the groups and try to find a matching group.
                //
                if (null == dest)
                {
                    for (int k = 0; k < groups.Count; k++)
                    {
                        if (groups[k].RowLayoutGroupInfo.CanInitializeFrom(ci))
                        {
                            dest = groups[k].RowLayoutGroupInfo;
                            break;
                        }
                    }
                }

				// SSP 4/7/05 BR03193
				// We need to store the Hidden property values of the columns with a RowLayout and
				// restore them. 
				//
				if ( null != dest )
				{
					dest.InitializeFrom( ci );
                    if (DefaultableBoolean.Default != ci.columnHidden)
                    {
                        // MRS - NAS 9.1 - Groups in RowLayout
                        //dest.Column.Hidden = DefaultableBoolean.True == ci.columnHidden;
                        bool hidden = DefaultableBoolean.True == ci.columnHidden;
                        if (dest.Column != null)
                            dest.Column.Hidden = hidden;
                        else if (dest.Group != null)
                            dest.Group.Hidden = hidden;
                        else
                            Debug.Fail("Destination RowLayoutColumnInfo has neither a column nor a group; unexpected.");

                    }
				}
			}

			this.Band.RowLayoutLabelPosition = this.RowLayoutLabelPosition;
			this.Band.RowLayoutLabelStyle = this.RowLayoutLabelStyle;
			this.Band.CardView = this.CardView;
			this.Band.CardSettings.Style = this.CardViewStyle;

            // MRS - NAS 9.1 - Groups in RowLayout
            this.Band.RowLayoutStyle = this.RowLayoutStyle;

			this.Band.DirtyRowLayoutCachedInfo( );
			this.Band.NotifyPropChange( PropertyIds.RowLayout );			
		}

		#endregion // Apply

		#region Reset

		/// <summary>
		/// Resets the object to its default value.
		/// </summary>
		public void Reset( )
		{
			this.ResetColumnInfos( );
			this.ResetRowLayoutLabelPosition( );
			this.ResetRowLayoutLabelStyle( );
			this.ResetCardView( );
			this.ResetCardViewStyle( );
		}

		/// <summary>
		/// Resets this RowLayout object to the default value or to the current rowlayout from the band if fromBand is true.
		/// </summary>
		/// <param name="fromBand">True to reset to the current band settings. False to reset to the default.</param>
		public void Reset( bool fromBand )
		{
			if ( !fromBand )
			{
				this.Reset( );
			}
			else
			{
				this.CopyCurrent( );
			}
		}

		#endregion // Reset		

		#region ShouldSerialize

		/// <summary>
		/// Returns true if any of the properties of this object are set a non-default value.
		/// </summary>
		/// <returns></returns>
		public bool ShouldSerialize( )
		{
			return 
				this.ShouldSerializeKey( ) ||
				this.ShouldSerializeTag( ) ||
				this.ShouldSerializeCardView( ) ||
				this.ShouldSerializeCardViewStyle( ) ||
				this.ShouldSerializeRowLayoutLabelPosition( ) ||
				this.ShouldSerializeRowLayoutLabelStyle( ) ||
				this.ShouldSerializeColumnInfos( );
		}

		#endregion // ShouldSerialize

		#region ResetRowLayoutLabelStyle

		/// <summary>
		/// Resets the property to its default value of <b>Separate</b>.
		/// </summary>
		public void ResetRowLayoutLabelStyle( )
		{
			this.RowLayoutLabelStyle = Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.Separate;
		}

		#endregion // ResetRowLayoutLabelStyle

		#region ResetColumnInfos

		/// <summary>
		/// Resets the property to its default value.
		/// </summary>
		public void ResetColumnInfos( )
		{
			if ( null != this.columnInfos )
				this.columnInfos.Reset( );
		}

		#endregion ResetColumnInfos

		#region ResetRowLayoutLabelPosition

		/// <summary>
		/// Resets the RowLayoutLabelPosition property to its default value of <b>Default</b>.
		/// </summary>
		public void ResetRowLayoutLabelPosition( )
		{
			this.RowLayoutLabelPosition = LabelPosition.Default;
		}

		#endregion // ResetRowLayoutLabelPosition

		#region ResetCardView

		/// <summary>
		/// Resets the property to its default value of <b>false</b>.
		/// </summary>
		public void ResetCardView( )
		{
			this.CardView = false;
		}

		#endregion // ResetCardView

		#region ResetCardViewStyle

		/// <summary>
		/// Resets the property to its default value of <b>MergedLabels</b>.
		/// </summary>
		public void ResetCardViewStyle( )
		{
			this.CardViewStyle = CardStyle.MergedLabels;
		}

		#endregion // ResetCardViewStyle

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ResetRowLayoutStyle

        /// <summary>
        /// Resets the property to its default value of <b>Separate</b>.
        /// </summary>
        public void ResetRowLayoutStyle()
        {
            this.RowLayoutStyle = Infragistics.Win.UltraWinGrid.RowLayoutStyle.None;
        }

        #endregion // ResetRowLayoutStyle

		#endregion // Public Properties/Methods

		#region Protected Methods

		#region ShouldSerializeColumnInfos

		/// <summary>
		/// Returns true if the ColumnInfos preoprty contains a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeColumnInfos( )
		{
			return null != this.columnInfos && this.columnInfos.ShouldSerialize( );
		}

		#endregion // ShouldSerializeColumnInfos

		#region ShouldSerializeRowLayoutLabelPosition

		/// <summary>
		/// Returns true if the RowLayoutLabelPosition property is set to a value other than the default value of <b>Default</b>.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowLayoutLabelPosition( )
		{
			return LabelPosition.Default != this.rowLayoutLabelPosition;
		}

		#endregion // ShouldSerializeRowLayoutLabelPosition

		#region ShouldSerializeCardView

		/// <summary>
		/// Returns true if the property is set to a value other than its default value of <b>false</b>.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCardView( )
		{
			return this.cardView;
		}

		#endregion // ShouldSerializeCardView

		#region ShouldSerializeCardViewStyle

		/// <summary>
		/// Returns true if the property is set to a value other than its default value of <b>MergedLabels</b>.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeCardViewStyle( )
		{
			return CardStyle.MergedLabels != this.cardViewStyle;
		}

		#endregion // ShouldSerializeCardViewStyle

		#region ShouldSerializeRowLayoutLabelStyle

		/// <summary>
		/// Retruns true if the property is set to a non-default value.
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		protected bool ShouldSerializeRowLayoutLabelStyle( )
		{
			return Infragistics.Win.UltraWinGrid.RowLayoutLabelStyle.Separate != this.rowLayoutLabelStyle;
		}

		#endregion // ShouldSerializeRowLayoutLabelStyle

        // MRS - NAS 9.1 - Groups in RowLayout
        #region ShouldSerializeRowLayoutStyle

        /// <summary>
        /// Retruns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        protected bool ShouldSerializeRowLayoutStyle()
        {
            return Infragistics.Win.UltraWinGrid.RowLayoutStyle.None != this.rowLayoutStyle;
        }

        #endregion // ShouldSerializeRowLayoutStyle

		#endregion // Protected Methods

		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			this.SerializeTag( info );

			// Serialize the key.
			//
			if ( this.ShouldSerializeKey( ) )
				Utils.SerializeProperty( info, "Key", this.Key );

			if ( null != this.columnInfos && this.columnInfos.ShouldSerialize( ) )
				Utils.SerializeProperty( info, "ColumnInfos", this.columnInfos );

			if ( this.ShouldSerializeRowLayoutLabelPosition( ) )
				Utils.SerializeProperty( info, "RowLayoutLabelPosition", this.rowLayoutLabelPosition );

			if ( this.ShouldSerializeRowLayoutLabelStyle( ) )
				Utils.SerializeProperty( info, "RowLayoutLabelStyle", this.rowLayoutLabelStyle );

			if ( this.ShouldSerializeCardView( ) )
				Utils.SerializeProperty( info, "CardView", this.cardView );

			if ( this.ShouldSerializeCardViewStyle( ) )
				Utils.SerializeProperty( info, "CardViewStyle", this.cardViewStyle );

            // MRS - NAS 9.1 - Groups in RowLayout
            if (this.ShouldSerializeRowLayoutStyle())
                Utils.SerializeProperty(info, "RowLayoutStyle", this.rowLayoutStyle);
		}

		#endregion // ISerializable.GetObjectData

		#region RowLayoutTypeConverter Class

		/// <summary>
		/// RowLayoutTypeConverter type converter.
		/// </summary>
		public sealed class RowLayoutTypeConverter : ExpandableObjectConverter 
		{
            /// <summary>
            /// Returns whether this converter can convert the object to the specified type, using the specified context.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
            /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType) 
			{
				if ( destinationType == typeof( InstanceDescriptor ) ) 
				{
					return true;
				}
				
				return base.CanConvertTo( context, destinationType );
			}

            /// <summary>
            /// Converts the given value object to the specified type, using the specified
            /// context and culture information.
            /// </summary>
            /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
            /// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
            /// <param name="value">The System.Object to convert.</param>
            /// <param name="destinationType">The System.Type to convert the value parameter to.</param>
            /// <returns>An System.Object that represents the converted value.</returns>
            public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType) 
			{
				if ( destinationType == null ) 
				{
					throw new ArgumentNullException( "destinationType" );
				}

				if ( destinationType == typeof( InstanceDescriptor ) && value is RowLayout ) 
				{
					RowLayout item = (RowLayout)value;
					
					ConstructorInfo ctor = typeof( RowLayout ).GetConstructor( new Type[] { typeof( string ) } );

					if ( null != ctor ) 
					{
						// false as the last parameter here causes generation of a local variable for the type 

						return new InstanceDescriptor( ctor, new object[] { item.Key }, false );
					}
				}

				return base.ConvertTo(context, culture, value, destinationType);
			}		
		}
		
		#endregion // RowLayoutTypeConverter Class
	}
}
