#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	

	/// <summary>
	/// GroupByRowExpansionIndicatorUIElement
	/// </summary>
	public class GroupByRowExpansionIndicatorUIElement : Infragistics.Win.ExpansionIndicatorUIElement
	{		

//#if DEBUG
//		/// <summary>
//		/// Constructor.
//		/// </summary>
//		/// <param name="parent">The parent element</param>
//#endif
		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Initializes a new <b>GroupByRowExpansionIndicatorUIElement</b>
		/// </summary>
		/// <param name="parent">Parent element</param>
		//internal GroupByRowExpansionIndicatorUIElement( UIElement parent ) : base( parent )
		public GroupByRowExpansionIndicatorUIElement( UIElement parent ) : base( parent )
		{
		}

		#region InitAppearance
        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// SSP 5/5/06 - App Styling
			// Resolve the app-style role appearance settings.
			// 
			// ----------------------------------------------------------------------------------------------
			UltraGridGroupByRow row = this.GroupByRow;
			AppStyling.ResolutionOrderInfo order = StyleUtils.GetResolutionOrder( null != row ? row.Layout : null );
			AppStyling.UIRole role = this.UIRole;

			if ( ( order.UseStyleBefore || order.UseStyleAfter ) && null != role )
			{
				if ( this.IsOpen )
					role.ResolveAppearance( ref appearance, ref requestedProps, AppStyling.RoleState.Expanded );

				role.ResolveAppearance( ref appearance, ref requestedProps, AppStyling.RoleState.Normal );
			}
			// ----------------------------------------------------------------------------------------------

			//appearance.ForeColor = SystemColors.WindowText;
			//appearance.BorderColor = SystemColors.ControlDark;
			// SSP 5/5/06 - App Styling
			// Only set the property if it's not already been set.
			// 
			// ----------------------------------------------------------------------------------------------
			
			if ( 0 != ( AppearancePropFlags.BackColor & requestedProps ) )
			{
				appearance.BackColor = SystemColors.Control;
				requestedProps ^= AppearancePropFlags.BackColor;
			}

			if ( 0 != ( AppearancePropFlags.ForeColor & requestedProps ) )
			{
				appearance.ForeColor = SystemColors.ControlText;
				requestedProps ^= AppearancePropFlags.ForeColor;
			}
			// ----------------------------------------------------------------------------------------------

            // MRS 12/18/2007 - BR29119
            // If we don't do this, we end up inheriting the image from the parent element, and
            // we really don't want to do that for the ExpansionIndicator. 
            requestedProps &= ~AppearancePropFlags.AllImageProps;
		}
		#endregion InitAppearance

 
		/// <summary>
		/// The associated Row object (read-only)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridGroupByRow GroupByRow
		{
			get
			{
				return (Infragistics.Win.UltraWinGrid.UltraGridGroupByRow)this.Parent.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridGroupByRow ), true );
			}
		}

		#region Expansion Indicator Overrides

		/// <summary>
		/// Indicates if the expansion indicator is expanded/open.
		/// </summary>
		public override bool IsOpen
		{
			get { return this.GroupByRow.ShowAsExpanded; }
		}

		/// <summary>
		/// Handles toggling of the expansion indicator state.
		/// </summary>
		protected override void Toggle()
		{
			Infragistics.Win.UltraWinGrid.UltraGridGroupByRow row = this.GroupByRow;

			if ( row.Layout.ActiveCell != null )
			{
				// SSP 8/6/04 UWG3400
				// If the exitting of edit mode was canceled (may be because the cell contained 
				// invalid input) then don't proceed with expansion of the row.
				//
				//row.Layout.ActiveCell.ExitEditMode();
				if ( row.Layout.ActiveCell.ExitEditMode( ) )
					return;
			}

			row.Expanded = !row.ShowAsExpanded;
		}

		/// <summary>
		/// Indicates whether the associated object can be expanded.
		/// </summary>
		protected override bool CanExpand
		{
			get 
			{ 
				return true; 
			}
		}

		#endregion Expansion Indicator Overrides

		#region BorderStyle
        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 5/5/06 - App Styling
				// 
				
				//return UIElementBorderStyle.Raised;
				UltraGridBand band = this.GroupByRow.BandInternal;

				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( band, StyleUtils.CachedProperty.BorderStyleGroupByExpansionIndicator, out val ) )
				{
					val = StyleUtils.CacheBorderStylePropertyValue( band, StyleUtils.CachedProperty.BorderStyleGroupByExpansionIndicator,
						StyleUtils.Role.GroupByExpansionIndicator, UIElementBorderStyle.Default, UIElementBorderStyle.Raised );
				}

				return (UIElementBorderStyle)val;
				
			}
		}
		#endregion BorderStyle

		#region UIRole

		// SSP 5/5/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.GroupByRow.BandInternal, StyleUtils.Role.GroupByExpansionIndicator );
			}
		}

		#endregion // UIRole
	}

}
