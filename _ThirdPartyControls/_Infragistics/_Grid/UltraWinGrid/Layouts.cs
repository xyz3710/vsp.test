#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Collections;
    using System.Globalization;
    using System.ComponentModel;
//    using System.ComponentModel.Design;
    using System.Data;
    using Infragistics.Shared;
    using Infragistics.Shared.Serialization;
	using Infragistics.Win;
	using System.Diagnostics;
	using System.Runtime.Serialization;
	using System.Security.Permissions;
	

    /// <summary>
    /// A collection of Layouts.
    /// </summary>
	[ Serializable() ]
	[ ListBindable(false )]
    public class LayoutsCollection : KeyedSubObjectsCollectionBase,
									 IList,
									 ICloneable,
									 ISerializable
    {

		private int					initialCapacity = 5;

		// SSP 5/20/05 BR04176
		// Added backward pointer to the grid. This is necessary at deisgn time when a 
		// new layout is added to the layout's collection via the layout collection editor.
		// We need to initialize the layout with a grid so for example column collection
		// editor dialog has access to the grid as well to make the transactions work.
		// 
		private UltraGridBase grid = null;
		

		/// <summary>
		/// Constructor.
		/// </summary>
        public LayoutsCollection()
        {
			
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Constructor used during de-serialization only
		//        /// </summary>
		//#endif
		//        internal LayoutsCollection( int initialCapacity )
		//        {
		//            this.initialCapacity = initialCapacity;
		//        }

		#endregion Not Used

		// SSP 6/25/04 UWG2937
		// Added InitGrid method.
		//
		internal void InitGrid( UltraGridBase grid )
		{
			// SSP 5/20/05 BR04176
			// Added backward pointer to the grid. This is necessary at deisgn time when a 
			// new layout is added to the layout's collection via the layout collection editor.
			// We need to initialize the layout with a grid so for example column collection
			// editor dialog has access to the grid as well to make the transactions work.
			// 
			this.grid = grid;

			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridLayout layout = this[i];
				if ( null != layout )
					layout.InitGrid( grid );
			}
		}

		// SSP 5/20/05 BR04176
		// Added backward pointer to the grid. This is necessary at deisgn time when a 
		// new layout is added to the layout's collection via the layout collection editor.
		// We need to initialize the layout with a grid so for example column collection
		// editor dialog has access to the grid as well to make the transactions work.
		// 
		/// <summary>
		/// Returns the associated UltraGrid if any.
		/// </summary>
		public UltraGridBase Grid
		{
			get
			{
				return this.grid;
			}
		}
	
		internal int AddLayoutHelper( UltraGridLayout layout )
		{
			// SSP 5/20/05 BR04176
			// Added backward pointer to the grid. This is necessary at deisgn time when a 
			// new layout is added to the layout's collection via the layout collection editor.
			// We need to initialize the layout with a grid so for example column collection
			// editor dialog has access to the grid as well to make the transactions work.
			// 
			layout.InitGrid( this.Grid );

			// Add a listener for property changes.
			layout.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			// Add the layout and return the index
			// SSP 7/1/03 UWG2443
			// Notify the change so the grid notifies the designer that something has changed.
			//
			// ------------------------------------------------------------------------------
			//return this.InternalAdd(layout);
			int index = this.InternalAdd(layout);
			
			this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.Add );

			return index;
			// ------------------------------------------------------------------------------
		}

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Called from EndInit
		//        /// </summary>
		//#endif
		//        internal void OnDeserializationComplete( )
		//        {
		//            for ( int i = 0; i < this.Count; i++ )
		//            {
		//                this[i].OnDeserializationComplete();
		//            }
		//        }

		#endregion Not Used

		/// <summary>
		/// Returns a clone of the collection
		/// </summary>
		object ICloneable.Clone()
		{
			Infragistics.Win.UltraWinGrid.LayoutsCollection clone = new LayoutsCollection();
			
			for ( int i = 0; i < this.Count; i++ )
			{
				Infragistics.Win.UltraWinGrid.UltraGridLayout layout = new UltraGridLayout();

				layout.InitializeFrom( this[i], PropertyCategories.All );
				
				clone.AddLayoutHelper( layout );				
			}

			return clone;
		}

		

		/// <summary>
		/// Abstract property that specifies the initial capacity
		/// of the collection
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		protected override int InitialCapacity
		{
			 get
			 {
				return this.initialCapacity;
			 }
		}

		/// <summary>
		/// Returns false
		/// </summary>
        public override bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

		object IList.this[ int index ]
		{
			get
			{
				return base.GetItem( index );
			}
			set
			{
				throw new NotSupportedException(Shared.SR.GetString("LE_NotSupportedException_186"));
			}
		}
        
		/// <summary>
		/// indexer
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridLayout this[ int index ] 
        {
            get
            {
                return (UltraGridLayout)base.GetItem( index );
            }
        }

		       
		/// <summary>
		/// indexer (by string key)
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridLayout this[ String key ] 
        {
            get
            {
                return (UltraGridLayout)base.GetItem( key );
            }
        }

       
        
        /// <summary>
        ///    <para>
        ///       Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridLayout'/>
        ///       to the collection.
        ///    </para>
        /// </summary>
        /// <exception cref='System.ArgumentNullException'>
        ///    The <paramref name="layout"/>argument was <see langword='null'/>.
        /// </exception>
        /// <returns>The newly added layout object</returns>
        public Infragistics.Win.UltraWinGrid.UltraGridLayout Add() 
        {
            UltraGridLayout layout = new UltraGridLayout();

			// SSP 5/20/05 BR04176
			// Added backward pointer to the grid. This is necessary at deisgn time when a 
			// new layout is added to the layout's collection via the layout collection editor.
			// We need to initialize the layout with a grid so for example column collection
			// editor dialog has access to the grid as well to make the transactions work.
			// 
			//this.InternalAdd( layout );
			this.AddLayoutHelper( layout );

			return layout;
        }

		/// <summary>
		/// Returns false
		/// </summary>
		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
			
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Count", this.Count );
			//info.AddValue( "Count", this.Count );
			
			// add each layout in the collection
			//
			for ( int i = 0; i < this.Count; i++ )
			{
				UltraGridLayout layout = new UltraGridLayout();

				layout =  this[i];

				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, i.ToString(), layout );
				//info.AddValue( i.ToString(), layout );
			}

			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}

		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//private LayoutsCollection( SerializationInfo info, StreamingContext context)
		protected LayoutsCollection( SerializationInfo info, StreamingContext context)
		{

			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( UltraGridLayout ) )
				{
					// JJD 8/20/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					UltraGridLayout item = (UltraGridLayout)Utils.DeserializeProperty( entry, typeof(UltraGridLayout), null );

					// Add the item to the collection
					if ( item != null )
						this.AddLayoutHelper(item);
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							//this.initialCapacity = Math.Max( this.initialCapacity, (int)entry.Value );
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/17/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							this.initialCapacity = Math.Max(this.initialCapacity, (int)Utils.DeserializeProperty( entry, typeof(int), this.initialCapacity ));
							//this.initialCapacity = Math.Max(this.initialCapacity, (int)DisposableObject.ConvertValue( entry.Value, typeof(int) ));
							break;
						}

						// JJD 1/31/02
						// De-Serialize the tag property
						//
						case "Tag":
						{
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						}

						default:
						{
							Debug.Assert( false, "Invalid entry in LayoutsCollection de-serialization ctor" );
							break;
						}
					}
				}
			}
		}

		/// <summary>
		/// Adds the specified layout object to the layouts collection.
		/// </summary>
		/// <param name="layout">Layout object to add to the collection.</param>
		public void Add( UltraGridLayout layout )
		{
			// AS 1/3/02 UWG904 
			// Originally tried changing the IList implementation to a public method (not fully
			// qualified with IList.) but the VS IDE would throw an error about not being able to
			// locate the wingrid dll. Oddly, implementing a separate method that calls the IList
			// implementation already in place, works fine. Believe it or not.....
			//
			if (layout != null && layout.IsDisplayLayout)
				throw new NotSupportedException( Shared.SR.GetString("LE_NotSupportedException_187") );

			((IList)this).Add( layout );
		}

		/// <summary>
		/// Appends a layout to the collection
		/// </summary>
		/// <param name="value">Must be a Layout object</param>
		/// <returns>Index in collection</returns>
		int IList.Add( object value )
		{
			if ( value == null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_188") );
			
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = value as Infragistics.Win.UltraWinGrid.UltraGridLayout;

			if ( layout == null )
				throw new ArgumentException( SR.GetString("LER_Exception_320", value.GetType().Name) );

			return this.AddLayoutHelper( layout );
		
		}

		/// <summary>
		/// Inserts a layout to the collection
		/// </summary>
		/// <param name="index">Index in collection</param>
		/// <param name="value">Must be a Layout object</param>
		void IList.Insert( int index, object value )
		{
			if ( value == null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_188") );
			
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = value as Infragistics.Win.UltraWinGrid.UltraGridLayout;

			if ( layout == null )
				throw new ArgumentException(SR.GetString("LER_Exception_320", value.GetType().Name ));

			// Add a listener for property changes.
			layout.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			this.InternalInsert( index, layout );
		}

		// AS 1/8/03 - fxcop
		// need a strongly typed Insert method
		#region Insert
		/// <summary>
		/// Inserts a <see cref="UltraGridLayout"/> into the collection
		/// </summary>
		/// <param name="index">Index in collection at which the item should be inserted</param>
		/// <param name="layout">Layout to insert</param>
		public void Insert(int index, Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			((IList)this).Insert(index, layout);
		}
		#endregion //Insert

		// AS 1/8/03 - fxcop
		// need a strongly typed Remove method
		#region Remove
		/// <summary>
		/// Remove a <see cref="UltraGridLayout"/> from the collection
		/// </summary>
		/// <param name="layout">Layout to remove from the collection</param>
		public void Remove( Infragistics.Win.UltraWinGrid.UltraGridLayout layout )
		{
			this.Remove( (object)layout );
		}
		#endregion //Remove


		/// <summary>
		/// Remove a layout from the collection
		/// </summary>
		/// <param name="value">Must be a Layout object</param>
		public void	Remove( object value )
		{
			if ( value == null )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_189") );
			
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = value as Infragistics.Win.UltraWinGrid.UltraGridLayout;

			if ( layout == null )
				throw new ArgumentException( SR.GetString("LER_Exception_321", value.GetType().Name  ));

			// Remove a listener for property changes.
			layout.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

			this.InternalRemove( layout );
		}

		/// <summary>
		/// Remove a layout from the collection
		/// </summary>
		/// <param name="index">Index to be removed</param>
		public void	RemoveAt( int index )
		{
			if ( index > this.Count || index < 0 )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_116") );
			
			Infragistics.Win.UltraWinGrid.UltraGridLayout layout = this[ index ];

			this.InternalRemoveAt(index);			

			if ( layout != null )
			{
				// Remove a listener for property changes.
				layout.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
			}
		}




		/// <summary>
		/// Adds layout object to layout collection
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <returns>The newly added layout object</returns>
        public Infragistics.Win.UltraWinGrid.UltraGridLayout Add( String key ) 
        {
            this.ValidateKeyDoesNotExist( key );

            UltraGridLayout layout = new UltraGridLayout( key );

            this.AddLayoutHelper( layout );

			return layout;
        }

        /// <summary>
        /// Clears the collection
        /// </summary>
        public void Clear() 
        {
			// Remove all listeners.
			for ( int i =0 ;i < this.Count; ++i )
				this[i].SubObjectPropChanged -= this.SubObjectPropChangeHandler;				
				
            this.InternalClear();
        }

		// SSP 5/18/04 UWG3075
		// Overrode OnSubObjectPropChanged so we can propagate the change notification
		// to the grid so it can notify the design environment of the change in the
		// control it marks the control dirty.
		//
        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			this.NotifyPropChange( PropertyIds.Layouts, propChange );
		}

		
     
       
		/// <summary>
		/// IEnumerable Interface Implementation
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public LayoutEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new LayoutEnumerator(this);
        }
 
    }

    /// <summary>
    /// Enumerator for the LayoutsCollection
    /// </summary>
    public class LayoutEnumerator: DisposableObjectEnumeratorBase
    {   
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="layouts">The collection of layouts to enumerate.</param>
        public LayoutEnumerator( LayoutsCollection layouts ) : base( layouts )
        {
        }

       
		/// <summary>
		/// Type-safe version of Current
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridLayout Current 
        {
            get
            {
                return (UltraGridLayout)((IEnumerator)this).Current;
            }
        }
    }
    
}
