#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Collections;
	using System.Security.Permissions;
	using Infragistics.Shared.Serialization;
	using System.Collections.Generic;

	/// <summary>
	/// Returns a reference to the GroupByBox object. This property is read-only 
	/// at design-time and run-time.
	/// </summary>
	/// <remarks>
	/// <p class="body">This property returns a reference to a GroupByBox
	/// object that can be used to set properties of, and invoke methods on, 
	/// a GroupBy box. You can use this reference to access any of the GroupBy
	/// box's properties or methods.</p>
	/// </remarks>
	[ TypeConverter( typeof( System.ComponentModel.ExpandableObjectConverter ) ), Serializable()  ]
	sealed public class GroupByBox : SpecialBoxBase, ISerializable,
		// MRS 2/23/04
		// AddISupportPresets
		ISupportPresets
	{

		private AppearanceHolder				bandLabelAppearanceHolder = null;
		private AppearanceHolder				promptAppearanceHolder = null;
		private UIElementBorderStyle			bandLabelBorderStyle;
		private ShowBandLabels					showBandLabels;
		private UIElementBorderStyle			buttonBorderStyle = UIElementBorderStyle.Default;
		private GroupByBoxStyle					style			  = GroupByBoxStyle.Full;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//static internal readonly int BTN_CONNECTOR_LEFT_INDENT = 8;
		//static internal readonly int BTN_CONNECTOR_BOTTOM_INDENT = 4;
		//static internal readonly int BTN_BAND_HEIGHT_OFFSET = 6;
		internal const int BTN_CONNECTOR_LEFT_INDENT = 8;
		internal const int BTN_CONNECTOR_BOTTOM_INDENT = 4;
		internal const int BTN_BAND_HEIGHT_OFFSET = 6;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//static internal readonly int BTN_BAND_HEIGHT_OFFSET_COMPACT = 2;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//static internal readonly int BTN_MIN_SPACING_HORZ = 10;
		internal const int BTN_MIN_SPACING_HORZ = 10;

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid unused private fields
		//static internal readonly int BTN_MIN_SPACING_HORZ_COMPACT = 10;
		//static internal readonly int BTN_MIN_SPACING_VERT = 6;
		//static internal readonly int BTN_MIN_SPACING_VERT_COMPACT = 3;	

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Use literals where appropriate
		//static internal readonly int MIN_HEIGHT = 35;
		internal const int MIN_HEIGHT = 35;


		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="layout"></param>
		public GroupByBox( Infragistics.Win.UltraWinGrid.UltraGridLayout layout ) : base( layout )
		{
			this.Reset();

			// Default for groupby is false
			this.Hidden = false;
		}


		internal override void InitializeFrom( SpecialBoxBase source, PropertyCategories propertyCategories )
		{
			base.InitializeFrom( source, propertyCategories );

			if ( (propertyCategories & PropertyCategories.General) == 0 )
				return;
		
			GroupByBox groupByBox = source as GroupByBox;
			if ( null != groupByBox )
			{
				// SSP 11/4/05 BR07503
				// Clone copies over the AppearanceCollection as well. We don't want that.
				// 
				// ------------------------------------------------------------------------------
				//if ( groupByBox.bandLabelAppearanceHolder != null )
				//	this.bandLabelAppearanceHolder = groupByBox.bandLabelAppearanceHolder.Clone();

				if ( null != groupByBox.bandLabelAppearanceHolder &&
					groupByBox.bandLabelAppearanceHolder.HasAppearance )
				{
					// Get appearance to trigger allocation of holder
					//
					Infragistics.Win.AppearanceBase app = this.BandLabelAppearance;
			
					// init the holder from the source
					//
					this.bandLabelAppearanceHolder.InitializeFrom( groupByBox.bandLabelAppearanceHolder );
				}
				else
				{
					// reset the appearance holder
					//
					if ( null != this.bandLabelAppearanceHolder &&
						this.bandLabelAppearanceHolder.HasAppearance )
					{
						this.bandLabelAppearanceHolder.Reset();
					}
				}
				// ------------------------------------------------------------------------------
				
				this.BandLabelBorderStyle = groupByBox.BandLabelBorderStyle;
				this.ShowBandLabels = groupByBox.ShowBandLabels;
				this.ButtonBorderStyle = groupByBox.buttonBorderStyle;
				
				// SSP 7/9/02
				// Implemented group by box compact style.
				//
				this.style = groupByBox.style;

				// SSP 11/20/04 BR00659
				// Copy the prompt appearance.
				//
				if ( null != groupByBox.promptAppearanceHolder &&
					groupByBox.promptAppearanceHolder.HasAppearance )
				{
					// Get appearance to trigger allocation of holder
					//
					Infragistics.Win.AppearanceBase app = this.PromptAppearance;					
			
					// init the holder from the source
					//
					this.promptAppearanceHolder.InitializeFrom( groupByBox.promptAppearanceHolder );
				}
				else
				{
					// reset the appearance holder
					//
					if ( null != this.promptAppearanceHolder &&
						this.promptAppearanceHolder.HasAppearance )
					{
						this.promptAppearanceHolder.Reset();
					}
				}
			}
		}

		/// <summary>
		/// Returns true is any of the properties have been
		/// set to non-default values.
		/// </summary>
		public override bool ShouldSerialize() 
		{
			return base.ShouldSerialize( ) ||
				this.ShouldSerializeBandLabelAppearance( ) ||
				this.ShouldSerializeBandLabelBorderStyle( ) ||
				this.ShouldSerializeShowBandLabels( ) ||
				this.ShouldSerializeButtonBorderStyle( ) ||

				// SSP 1/3/02
				// Implemented PromptAppearance
				//
				this.ShouldSerializePromptAppearance( ) ||
				// SSP 7/9/02
				// Implemented group by box compact style.
				//
				this.ShouldSerializeStyle( );
		}

		/// <summary>
		/// Resets all properties back to their default values
		/// </summary>
		public override void Reset() 
		{
			base.Reset( );
			this.ResetBandLabelAppearance( );
			this.ResetBandLabelBorderStyle( );
			this.ResetShowBandLabels( );
			this.ResetButtonBorderStyle( );

			// SSP 1/3/02
			// Implemented PromptAppearance
			//
			this.ResetPromptAppearance( );
		}

		/// <summary>
		/// Called when a property has changed on a sub object
		/// </summary>
        /// <param name="propChange">A structure containing the property change information.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// If the base class did not find any matching propChange.Source
			// then fire NotifyPropChange with PropertyIds.AddNewBox
			if ( !base.OnSubObjectPropChangedHelper( propChange ) )		
			{		
				if ( this.HasBandLabelAppearance &&
					this.bandLabelAppearanceHolder.RootAppearance == propChange.Source )
					this.NotifyPropChange( PropertyIds.BandLabelAppearance, propChange );
				else
					this.NotifyPropChange( PropertyIds.GroupByBox, propChange );
			}
		}

		internal override string DefaultPrompt
		{
			get
			{
				// JJd 1/12/02
				// Change the default prompt if any bands are in card view.
				//
				if ( this.Layout != null && this.Layout.SortedBands.Count > 0)
				{
					if ( this.Layout.SortedBands[0].CardView )
						return SR.GetString("GroupByBoxDefaultPromptSingleBandCardView"); //"Drag a card label here to group by that column.";

					if ( this.Layout.SortedBands.Count > 1 &&
						 this.Layout.ViewStyleImpl.IsMultiBandDisplay )
					{
						for ( int i = 1; i < this.Layout.SortedBands.Count; i++ )
						{
							if ( this.Layout.SortedBands[i].HiddenResolved )
								continue;

							if ( this.Layout.SortedBands[i].CardView )
								return SR.GetString("GroupByBoxDefaultPromptMultiBandCardView"); //"Drag a column header or card label here to group by that column.";
						}
					}

				}
				return SR.GetString("GroupByBoxDefaultPrompt"); //"Drag a column header here to group by that column.";
			}
		}

		internal override bool DefaultHidden
		{
			get
			{
				return false;
			}
		}	


		/// <summary>
		/// Returns or sets the Appearance used for Band labels in the GroupByBox.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can assign an <see cref="Infragistics.Win.Appearance"/> object to this property to specify the formatting that will be applied to the Band labels that appear in the GroupByBox. You can also use this property to access any Appearance-related properties of the GroupByBox Band labels.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.Appearance"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridBand"/>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		[ LocalizedDescription("LD_GroupByBox_P_BandLabelAppearance") ]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.AppearanceBase BandLabelAppearance
		{
			get
			{
				if ( null == this.bandLabelAppearanceHolder )
				{
				    
					this.bandLabelAppearanceHolder = new Infragistics.Win.AppearanceHolder();
				
					// hook up the prop change notifications
					//
					this.bandLabelAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.bandLabelAppearanceHolder.Collection = this.Layout.Appearances;

				return this.bandLabelAppearanceHolder.Appearance;
			}
			set
			{
				
				if( this.bandLabelAppearanceHolder == null  ||
					!this.bandLabelAppearanceHolder.HasAppearance ||					
					this.bandLabelAppearanceHolder.Appearance != value )
				{
					// remove the old prop change notifications
					//
					if ( null == this.bandLabelAppearanceHolder )
					{
						this.bandLabelAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.bandLabelAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;						
					}
				         	
                    // Initialize the collection
					//
					if (this.Layout != null)
						this.bandLabelAppearanceHolder.Collection = this.Layout.Appearances;

					this.bandLabelAppearanceHolder.Appearance = value;
            
					
					this.NotifyPropChange( PropertyIds.BandLabelAppearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if a GroupByBox Band label Appearance object has been created.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
		Browsable( false ) ]		
		public bool HasBandLabelAppearance
		{
			get
			{
				return ( null != this.bandLabelAppearanceHolder &&
					this.bandLabelAppearanceHolder.HasAppearance );
			}
		}


		/// <summary>
		/// Returns or sets the Appearance used for the prompt in the GroupByBox.
		/// </summary>
		/// <remarks>
		/// <p class="body">You can assign an <see cref="Infragistics.Win.Appearance"/> object to this property to specify the formatting that will be applied to the prompt text that appears in the GroupByBox. You can also use this property to access any Appearance-related properties of the GroupByBox's prompt text, such as font or color.</p>
		/// </remarks>
		/// <seealso cref="Infragistics.Win.Appearance"/>
		[ DesignerSerializationVisibility(DesignerSerializationVisibility.Visible) ]
		[ LocalizedDescription("LD_GroupByBox_P_PromptAppearance") ]
		[ LocalizedCategory("LC_Appearance") ]
		public Infragistics.Win.AppearanceBase PromptAppearance
		{
			get
			{
				if ( null == this.promptAppearanceHolder )
				{
				    
					this.promptAppearanceHolder = new Infragistics.Win.AppearanceHolder();
				
					// hook up the prop change notifications
					//
					this.promptAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				// Initialize the collection
				//
				if (this.Layout != null)
					this.promptAppearanceHolder.Collection = this.Layout.Appearances;

				return this.promptAppearanceHolder.Appearance;
			}
			set
			{
				
				if( this.promptAppearanceHolder == null  ||
					!this.promptAppearanceHolder.HasAppearance ||					
					this.promptAppearanceHolder.Appearance != value )
				{
					// remove the old prop change notifications
					//
					if ( null == this.promptAppearanceHolder )
					{
						this.promptAppearanceHolder = new Infragistics.Win.AppearanceHolder();

						// hook up the new prop change notifications
						//
						this.promptAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;						
					}
				         	
					// Initialize the collection
					//
					if (this.Layout != null)
						this.promptAppearanceHolder.Collection = this.Layout.Appearances;

					this.promptAppearanceHolder.Appearance = value;
            
					
					this.NotifyPropChange( PropertyIds.PromptAppearance, null );
				}
			}
		}

		/// <summary>
		/// Returns true if a PromptAppearance object has been created.
		/// </summary>
		[ DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden ),
		Browsable( false ) ]		
		public bool HasPromptAppearance
		{
			get
			{
				return ( null != this.promptAppearanceHolder &&
					this.promptAppearanceHolder.HasAppearance );
			}
		}

		/// <summary>
		/// Returns true if PromptAppearance is not set to its default value.
		/// </summary>
		/// <returns></returns>
		public bool ShouldSerializePromptAppearance() 
		{
			return ( this.HasPromptAppearance &&
				this.promptAppearanceHolder.ShouldSerialize() );
		}

		 
		/// <summary>
		/// Resets the PromptAppearance.
		/// </summary>
		public void ResetPromptAppearance() 
		{
			if ( null != this.promptAppearanceHolder )
			{
				// remove the prop change notifications
				//				//
				//				this.bandLabelAppearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				//                    
				//				this.bandLabelAppearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.bandLabelAppearanceHolder.Appearance.Reset();

				// Call reset on the holder instead of the appearance
				//
				this.promptAppearanceHolder.Reset();
			}
		}


		internal ShowBandLabels ShowBandLabelsResolved
		{
			get
			{
				if ( ShowBandLabels.Default == this.ShowBandLabels )
				{
					// in a single band group by view, we don't display band labels
					if ( ViewStyle.SingleBand == this.Layout.ViewStyle )
					{
						return ShowBandLabels.None;
					}
					else
					{						
						// if too many buttons are shown in the group by box
						// then check if all of them are going to fit with
						// ShowBandLabels.IntermediateBandsOnly style. If not
						// then use ShowBandLabels.None so at least ass many
						// as possible group by column buttons are shown
						//
						UIElement gridElem = this.Layout.GetUIElement( false );

						if ( null != gridElem )
						{
							Size requiredSize = this.CalcRequiredSize(  ShowBandLabels.IntermediateBandsOnly );
                            if ( gridElem.RectInsideBorders.Width < requiredSize.Width )
								return ShowBandLabels.None;
						}

						return ShowBandLabels.IntermediateBandsOnly;
					}
				}
				
				return this.ShowBandLabels;
			}
		}

		private UltraGridBand LastBandWithGroupByColumns
		{
			get
			{
				BandsCollection bands = this.Layout.SortedBands;

				for ( int i = bands.Count - 1; i >= 0 ; i-- )
				{
					if ( bands[i].HasGroupBySortColumns )
						return bands[i];
				}
				
				return null;
			}
		}

		private UltraGridBand FirstBandWithGroupByColumns
		{
			get
			{
				BandsCollection bands = this.Layout.SortedBands;

				for ( int i = 0; i <  bands.Count; i++ )
				{
					if ( bands[i].HasGroupBySortColumns )
						return bands[i];
				}

				return null;
			}
		}

		internal struct ButtonDefinition
		{
			internal string caption;
			internal UltraGridColumn column;
			internal UltraGridBand	band;

			internal ButtonDefinition( UltraGridBand band, UltraGridColumn column, string caption )
			{
				this.band = band;
				this.column = column;
				this.caption = caption;
			}

			internal string Caption
			{
				get
				{
					if ( null != this.caption )
						return this.caption;

					if ( null != this.column )
						return column.Header.Caption;

					if ( null != this.band )
						return this.band.AddButtonCaptionResolved;

					return string.Empty;
				}
			}
		}
	


		internal ButtonDefinition[] GetButtons( ShowBandLabels bandLablesToShow )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( );
			List<ButtonDefinition> list = new List<ButtonDefinition>();

			BandsCollection bands = this.Layout.SortedBands;
			
			Debug.Assert( null != bands, "No bands off the layout !" );


			for ( int i = 0; i < bands.Count; i++ )
			{
				UltraGridBand band = bands[i];

				// JJD 10/02/01
				// Bypass hidden bands
				//
				if ( band.HiddenResolved )
					continue;

				if ( band.HasGroupBySortColumns )
				{
					for ( int j = 0; j < band.SortedColumns.Count; j++ )
					{
						if ( band.SortedColumns[j].IsGroupByColumn )
						{
							// SSP 11/25/02 UWG1859
							// If the caption is multiline, then replace the new line character with a space.
							//
							// --------------------------------------------------------------------------------
							//list.Add( new ButtonDefinition( null, band.SortedColumns[j], band.SortedColumns[j].Header.Caption ) );
							
							string caption = band.SortedColumns[j].Header.Caption;

							if ( caption.IndexOf( '\r' ) >= 0 || caption.IndexOf( '\n' ) >= 0 )
							{
								caption = caption.Replace( "\r\n", " " );
								caption = caption.Replace( '\n', ' ' );
								caption = caption.Replace( '\r', ' ' );
							}

							list.Add( new ButtonDefinition( null, band.SortedColumns[j], caption ) );
							// --------------------------------------------------------------------------------
						}
					}
				}

				bool showBandLabel = false;

				switch ( bandLablesToShow )
				{
					case ShowBandLabels.All:
						showBandLabel = true;
						break;
					case ShowBandLabels.AllWithGroupByColumnsOnly:
						showBandLabel = null != this.FirstBandWithGroupByColumns;
						break;
					case ShowBandLabels.IntermediateBandsOnly:
					{
						// JJD 10/04/01
						// Show all bands before the last band with group by columns
						//
//						showBandLabel = null != this.FirstBandWithGroupByColumns &&
//							band.Index >= this.FirstBandWithGroupByColumns.Index &&
//							band.Index < this.LastBandWithGroupByColumns.Index;
						UltraGridBand lastBand = this.LastBandWithGroupByColumns;

						if ( lastBand != null &&
							 band.Index < lastBand.Index )
							showBandLabel = true;

						break;
					}
					case ShowBandLabels.None:
						showBandLabel = false;
						break;
				}

				if ( showBandLabel )	
					list.Add( new ButtonDefinition( band, null, band.AddButtonCaptionResolved ) );
				
			}

			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (ButtonDefinition[])list.ToArray( typeof( ButtonDefinition ) );
			return list.ToArray();
		}



		internal ButtonDefinition[] GetButtons( )
		{
			return this.GetButtons( this.ShowBandLabelsResolved );
		}

		internal Size CalcRequiredSize( ShowBandLabels showBandLabels )
		{	
			Size promptSize = this.PromptSize;
			
			UIElement gridElem = this.Layout.GetUIElement( false );
				
			int borderWidth = this.Layout.GetBorderThickness( gridElem.BorderStyle );

			Rectangle thisRect = new Rectangle( 0, 0,
				gridElem.Rect.Width - 2 * borderWidth, this.Layout.Grid.Height - 2 * borderWidth );


			ButtonDefinition[] buttons = this.GetButtons( showBandLabels );

			if ( null == buttons || buttons.Length <= 0 )
			{
				return new Size( thisRect.Width, GroupByBox.MIN_HEIGHT );
			}

			Rectangle lastButtonRect = new Rectangle(
				thisRect.Left + GroupByBox.BTN_BAND_HEIGHT_OFFSET, 
				thisRect.Top + GroupByBox.BTN_BAND_HEIGHT_OFFSET,
				2 + promptSize.Width, 2 + promptSize.Height );
			lastButtonRect.Width = 0;
			// SSP 7/9/02
			// Only do so for full group by box style.
			//
			if ( GroupByBoxStyle.Full == this.Style )
				lastButtonRect.Y -= lastButtonRect.Height / 2;			
			
			if ( null != buttons && buttons.Length > 0 )
			{
				for ( int i = 0; i < buttons.Length; i++ )
				{
					Debug.Assert( null != buttons[i].Caption, "An invalid item enocuntered in buttons" );

					if ( null == buttons[i].Caption )
						continue;

					Size buttonSize = this.GetButtonSize( buttons[i] );
					
					// JJD 12/26/01
					// Don't add in the border width since that was already included 
					// in the call to GetButtonSize above
					//

					// SSP 7/9/02
					// Below two lines of code were commented out in the position child elemnets
					// of group by box ui element. So we don't need them here as well.
					//
					//int buttonHeight = buttonSize.Height;
					//int buttonWidth = Math.Max( 50, 12 + buttonSize.Width );
					
					
					Rectangle buttonRect = new Rectangle( 
						lastButtonRect.Right + GroupByBox.BTN_MIN_SPACING_HORZ,
						// SSP 7/9/02
						// Group by box compact style.
						//
						//lastButtonRect.Top + lastButtonRect.Height / 2,
						lastButtonRect.Top + ( GroupByBoxStyle.Full == this.Style ? lastButtonRect.Height / 2 : 0 ),
						// SSP 7/9/02
						//buttonWidth, buttonHeight );
						buttonSize.Width, buttonSize.Height );

					

					lastButtonRect = buttonRect;
				}
			}		
	
			Size size = Size.Empty;

			size.Width = lastButtonRect.Right - thisRect.Left + GroupByBox.BTN_BAND_HEIGHT_OFFSET;
			size.Height = lastButtonRect.Bottom - thisRect.Top + GroupByBox.BTN_BAND_HEIGHT_OFFSET;

			return size;
		}




	
		internal int IdealHeight
		{
			get
			{
				Size size = this.CalcRequiredSize( this.ShowBandLabelsResolved );

				return size.Height;
			}
		}


		internal bool HiddenResolved
		{
			get
			{
				if ( ViewStyleBand.OutlookGroupBy != this.Layout.ViewStyleBand )
					return true;

				return this.Hidden;
			}
		}


		/// <summary>
		/// Resolves all of the GroupByBox's Band label appearance properties.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to retrieve the actual values that are being used to format the Band labels of the GroupByBox. This method returns a value for all Appearance properties, tracing up the Appearance hierarchy if necessary.</p>
		/// </remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		public void ResolveBandLabelAppearance( ref AppearanceData appData )
		{
			this.ResolveBandLabelAppearance( ref appData, AppearancePropFlags.AllRenderAndCursor );
		}

		internal Size GetButtonSize( ButtonDefinition buttonDef )
		{
			Size size = Size.Empty;

			AppearanceData appData = new AppearanceData();
			this.ResolveGroupByButtonAppearance( 
				ref appData, 
				AppearancePropFlags.AllRender, 
				buttonDef );
	
			string caption = buttonDef.Caption;
		
			// JJD 12/14/01
			// Moved font height logic into CalculateFontHeight
			//
			// get the extext of the caption
			//
			if ( caption.Length > 0 )
				size = this.Layout.CalculateFontSize( ref appData, caption );
			else
				size = this.Layout.CalculateFontSize( ref appData, "WWWWy" );

			int borderWidth = this.Layout.GetBorderThickness( 
				null != buttonDef.column 
				? this.ButtonBorderStyleResolved 
				: this.BandLabelBorderStyleResolved
				);

			size.Height = 2 + size.Height + 2 * borderWidth;

			// SSP 12/12/06 BR18176
			// 
			// --------------------------------------------------------------------
			Image image = null;
			Size imageSize = Size.Empty;
			this.CalcDisplayedImageSize( appData, size.Height - 2 * borderWidth, out image, out imageSize );

			const int SORT_INDICATOR_WIDTH = 14;

			if ( null != buttonDef.column )
			{
				if ( caption.Length <= 0 && imageSize.Width > 0 )
					size.Width = 0;

				if ( Infragistics.Win.HAlign.Center != appData.ImageHAlign || 0 == size.Width )
					size.Width += 2 + imageSize.Width;

				size.Width += SORT_INDICATOR_WIDTH + 2 * borderWidth;
			}
			else
			{
				size.Width = Math.Max( 35, 2 + 2 * borderWidth + size.Width );
			}

            
			// --------------------------------------------------------------------

			return size;
		}


		internal void CalcDisplayedImageSize( AppearanceData appData, int maxHeight, out Image image, out Size size )
		{
			image = null;
			size = Size.Empty;

			if ( appData.Image != null )
			{
				image = appData.GetImage( this.Layout.Grid.ImageList );

				if ( image != null )
				{
					size = image.Size;

					// shrink the image size if necessary
					//
					if ( maxHeight > 2 && size.Height > maxHeight - 2 )
					{
						// proportinally shrink the width
						//
						size.Width  = (int)((double)size.Width * ( (double)(maxHeight - 2) / (double)size.Height ));

						// shrink the height
						//
						size.Height = maxHeight - 2;
					}
				}
			}
		}


		/// <summary>
		/// Resolves selected properties of the button's appearance
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to retrieve the actual values that are being used to format the GroupBy buttons of the GroupByBox. This method returns a value for all Appearance properties or only for specified ones, tracing up the Appearance hierarchy if necessary. You can combine the bit flags for this method to specify which properties should be resolved.</p>
		/// </remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		/// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
		/// <param name="buttonDef"></param>
		internal void ResolveGroupByButtonAppearance( 
			ref AppearanceData appData,			
			AppearancePropFlags requestedProps,
			ButtonDefinition buttonDef )
		{
			this.ResolveGroupByButtonAppearance( ref appData, ref requestedProps, buttonDef, true );
		}

		// SSP 2/21/06 BR10188 - HeaderStyle of WindowsXPCommand
		// Added an overload of ResolveGroupByButtonAppearance that takes in resolveDefaultColors.
		// 
		/// <summary>
		/// Resolves selected properties of the button's appearance
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to retrieve the actual values that are being used to format the GroupBy buttons of the GroupByBox. This method returns a value for all Appearance properties or only for specified ones, tracing up the Appearance hierarchy if necessary. You can combine the bit flags for this method to specify which properties should be resolved.</p>
		/// </remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		/// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
		/// <param name="buttonDef">Contains information on the kind of button.</param>
		/// <param name="resolveDefaultColors">Specifies whetehr to resolve the default colors or only to resolve
		/// the colors explicitly set on the appearance objects.</param>
		internal void ResolveGroupByButtonAppearance( 
			ref AppearanceData appData,			
			ref AppearancePropFlags requestedProps,
			ButtonDefinition buttonDef,
			bool resolveDefaultColors )
		{
			// If buttonDef has a valid column indicating it's a group by button (and not a
			// group by band label), resolve the header appearance
			if ( null != buttonDef.column )
				// SSP 2/21/06 BR10188 - HeaderStyle of WindowsXPCommand
				// Added an overload of ResolveGroupByButtonAppearance that takes in resolveDefaultColors.
				// 
				//buttonDef.column.Header.ResolveAppearance( ref appData, requestedProps );
				// SSP 3/14/06 - App Styling
				// Pass in the new isGroupByBoxButton parameter to indicate that the appearance is being resolved
				// for a column header in the group-by box so the header's ResolveAppearance uses appropriate role 
				// for column header in group-by box.
				// 
				//buttonDef.column.Header.ResolveAppearance( ref appData, ref requestedProps, resolveDefaultColors, false );
				buttonDef.column.Header.ResolveAppearance( ref appData, ref requestedProps, resolveDefaultColors, false, true );
			else
				this.ResolveBandLabelAppearance( ref appData, requestedProps );


			
		}

		// SSP 1/23/03 UWG981
		// Added this virtual method to SpecialBoxBas and overrode it here
		// to resolve prompt appearance (since the AddNewBox does not
		// have a PromptAppearance).
		//
		internal override void InternalResolvePromptAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			this.ResolvePromptAppearance( ref appData, ref flags );
		}

		/// <summary>
		/// Resolves selected properties of the GroupByBox's prompt appearance.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to retrieve the actual values that are being used to format the prompt of the GroupByBox. This method returns a value for all Appearance properties or only for specified ones, tracing up the Appearance hierarchy if necessary. You can combine the bit flags for this method to specify which properties should be resolved.</p>
		/// </remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		/// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
		public void ResolvePromptAppearance( ref AppearanceData appData, AppearancePropFlags requestedProps )
		{	
			this.ResolvePromptAppearance( ref appData, ref requestedProps );
		}

		// SSP 3/16/06
		// Added an overload of ResolvePromptAppearance that takes in requestedProps as ref param.
		// 
		internal void ResolvePromptAppearance( ref AppearanceData appData, ref AppearancePropFlags requestedProps )
		{
			// SSP 1/3/02 UWG706
			// Added this method to allow the user to be able to set the back color
			// of group by prompt.
			//


            // SSP 3/14/06 - App Styling
			// 
			AppStyling.ResolutionOrderInfo order;
			AppStyling.UIRole role = StyleUtils.GetRole( this.Layout, StyleUtils.Role.GroupByBoxPrompt, out order );
			if ( order.UseStyleBefore )
				StyleUtils.ResolveAppearance( role, AppStyling.RoleState.Normal, ref appData, ref requestedProps );

			// If we have prompt appearance set, the resolve appearance off it
			// first.
			//
			// SSP 3/14/06 - App Styling
			// 
			//if ( this.HasPromptAppearance )
			if ( this.HasPromptAppearance && order.UseControlInfo )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, this.PromptAppearance.Data, ref requestedProps );
				this.PromptAppearance.MergeData( ref appData, ref requestedProps );
			}

			// SSP 3/14/06 - App Styling
			// 
			if ( order.UseStyleAfter )
				StyleUtils.ResolveAppearance( role, AppStyling.RoleState.Normal, ref appData, ref requestedProps );

			// If the backColor is not set on the prompt appearance
			// then use the default of SystemColors.Control
			//
			if ( 0 != ( requestedProps & AppearancePropFlags.BackColor ) )
			{
				appData.BackColor = SystemColors.Control;
			}
		
			// Strip out the bits that shouldn't be inherited from the group by
			// box
			//
			requestedProps &= ~( 
				AppearancePropFlags.BackColor |
				AppearancePropFlags.BackColor2 |
				AppearancePropFlags.BackGradientStyle |
				AppearancePropFlags.BackColorDisabled |
				AppearancePropFlags.BackColorDisabled2 );

			// Resolve the rest off the appearance settings from the SpecialBoxBase's
			// appearance.
			//
			this.ResolveAppearance( ref appData, ref requestedProps );
		}

		/// <summary>
		/// Resolves selected properties of the BandLabel's appearance
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to retrieve the actual values that are being used to format the Band labels of the GroupByBox. This method returns a value for all Appearance properties or only for specified ones, tracing up the Appearance hierarchy if necessary. You can combine the bit flags for this method to specify which properties should be resolved.</p>
		/// </remarks>
		/// <param name="appData">The structure to contain the resolved apperance.</param>
		/// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
		public void ResolveBandLabelAppearance( ref AppearanceData appData,
			AppearancePropFlags requestedProps )
		{

			// Use OBJECT_TYPE_SpecialBoxBase instead of OBJECT_TYPE_HEADER so we don't get any
			// properties from the header appearance 
			//
			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.SpecialBoxBase), requestedProps );

			// SSP 3/13/06 - App Styling
			// 
            context.Role = StyleUtils.GetRole( this.Layout, StyleUtils.Role.GroupByBoxBandLabel, out context.ResolutionOrder );			
			if ( context.ResolutionOrder.UseStyleBefore )
				StyleUtils.ResolveAppearance( AppStyling.RoleState.Normal, ref appData, ref context );

			// first try to apply the addnew box's appearance
			//
			// SSP 3/13/06 - App Styling
			// Check for UseControlInfo.
			// 
			//if ( this.bandLabelAppearanceHolder != null )
			if ( this.bandLabelAppearanceHolder != null && context.ResolutionOrder.UseControlInfo )
			{
				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, 
				//	this.BandLabelAppearance.Data,
				//	ref context.UnresolvedProps );
				this.BandLabelAppearance.MergeData( ref appData, ref context.UnresolvedProps );
			}

			// SSP 3/13/06 - App Styling
			// 
			if ( context.ResolutionOrder.UseStyleAfter )
				StyleUtils.ResolveAppearance( AppStyling.RoleState.Normal, ref appData, ref context );

			// default the back and fore color if not already set
			//
			if ( ( (requestedProps & AppearancePropFlags.ForeColor) != 0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.ForeColor ) )
				appData.ForeColor = SystemColors.Control;

			// default the back and fore color if not already set
			//
			if ( ( (requestedProps & AppearancePropFlags.BorderColor) != 0) &&
				!appData.HasPropertyBeenSet(AppearancePropFlags.BorderColor ) )
				appData.BorderColor = SystemColors.Control;

			//Default the TextVAlign and
			//TextVAlign proeprties.
			if ( ( (requestedProps & AppearancePropFlags.ImageHAlign) != 0) && 
				!appData.HasPropertyBeenSet(AppearancePropFlags.ImageHAlign ) )
				appData.ImageHAlign = HAlign.Left;

			//Default the PictureAlign and
			//PictureVAlign proeprties.
			if ( ( (requestedProps & AppearancePropFlags.TextVAlign) != 0) && 
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextVAlign ) )
				appData.TextVAlign = VAlign.Middle;

			//Default the TextVAlign and
			//TextVAlign proeprties.
			if ( ( (requestedProps & AppearancePropFlags.TextVAlign) != 0) && 
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextVAlign ) )
				appData.TextVAlign = VAlign.Middle;

			if ( ( (requestedProps & AppearancePropFlags.TextHAlign) != 0) && 
				!appData.HasPropertyBeenSet(AppearancePropFlags.TextHAlign ) )
				appData.TextHAlign = HAlign.Center;

			//Default PictureBackgroundStyle to Centered.
			if ( ( (requestedProps & AppearancePropFlags.ImageBackgroundStyle) != 0) && 
				!appData.HasPropertyBeenSet(AppearancePropFlags.ImageBackgroundStyle) )
				appData.ImageBackgroundStyle = ImageBackgroundStyle.Centered;

			// Strip out all bits from the the request and resolved mask except
			// for font before calling m_Layout.ResolveAppearance
			// because that is all we ever want to inherit
			//
			context.UnresolvedProps &= AppearancePropFlags.FontData;

			// call the layout's resolve appearance to fill in any properties that
			// weren't set already
			//
			this.Layout.ResolveAppearance( ref appData, ref context );
		}


		/// <summary>
		/// Returns true if this property is not set to its default value
		/// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
		public bool ShouldSerializeBandLabelAppearance() 
		{
			return ( this.HasBandLabelAppearance &&
				this.bandLabelAppearanceHolder.ShouldSerialize() );
		}

		 
		/// <summary>
		/// Resets the BandLabelAppearance
		/// </summary>
		public void ResetBandLabelAppearance() 
		{
			if ( null != this.bandLabelAppearanceHolder )
			{
				// remove the prop change notifications
//				//
//				this.bandLabelAppearanceHolder.Appearance.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
//                    
//				this.bandLabelAppearanceHolder.Reset();
				// AS - 10/2/01   We only need to reset the appearance data, not unhook.
				//this.bandLabelAppearanceHolder.Appearance.Reset();

				// Call reset on the holder instead of the appearance
				//
				this.bandLabelAppearanceHolder.Reset();
			}
		}

		/// <summary>
		/// Called when the object is disposed of
		/// </summary>
		protected override void OnDispose()
		{
			
			// It is importantant to unhook the holder and call reset on it
			// when we are disposed
			//
			if ( this.bandLabelAppearanceHolder != null )
			{
				this.bandLabelAppearanceHolder.SubObjectPropChanged -= this.SubObjectPropChangeHandler;
				this.bandLabelAppearanceHolder.Reset();
				this.bandLabelAppearanceHolder = null;
			}


			base.OnDispose();
		}



		internal UIElementBorderStyle BandLabelBorderStyleResolved
		{
			get
			{
				if ( UIElementBorderStyle.Default == this.BandLabelBorderStyle )
					return UIElementBorderStyle.Solid;

				return this.BandLabelBorderStyle;
			}
		}


		/// <summary>
		/// Determines the style of the border for band labels.
		/// If set to 'Default' will use 'SolidLine'
		/// </summary>
		[ LocalizedDescription("LD_ButtonDefinition_P_BandLabelBorderStyle") ]
		[ LocalizedCategory("LC_Display") ]
		public UIElementBorderStyle BandLabelBorderStyle
		{
			get
			{
				return this.bandLabelBorderStyle;
			}
			set
			{
				if ( this.bandLabelBorderStyle != value )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( UIElementBorderStyle ), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_143") );

					this.bandLabelBorderStyle = value;

					this.NotifyPropChange( PropertyIds.BandLabelBorderStyle, null );
				}
			}
		}

		/// <summary>
		/// Checks to see if BandLabelBorderStyle property needs to be serialized
		/// </summary>
		public bool ShouldSerializeBandLabelBorderStyle( )
		{
			return  UIElementBorderStyle.Default != this.BandLabelBorderStyle;
		}
 
		/// <summary>
		/// Resets the BandLabelBorderStyle propetry to its default value
		/// </summary>
		public void ResetBandLabelBorderStyle( )
		{
			this.BandLabelBorderStyle = UIElementBorderStyle.Default;
		}

		/// <summary>
		/// Checks to see if Style property needs to be serialized.
		/// </summary>
		public bool ShouldSerializeStyle( )
		{
			return GroupByBoxStyle.Full != this.Style;
		}
 
		/// <summary>
		/// Resets the Style propetry to its default value.
		/// </summary>
		public void ResetStyle( )
		{
			this.Style = GroupByBoxStyle.Full;
		}
		
		
		/// <summary>
		/// Determines whether band labels are shown in the GroupByBox.
		/// </summary>
		/// <remarks>
		/// If set to 'Default' single band views will use 'None'
		/// and multi-band views will use 'IntermediateBandsOnly'
		/// </remarks>
		[ LocalizedDescription("LD_ButtonDefinition_P_ShowBandLabels") ]
		[ LocalizedCategory("LC_Display") ]
		public ShowBandLabels ShowBandLabels
		{
			get
			{
				return this.showBandLabels;
			}
			set
			{
				if ( this.showBandLabels != value )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( ShowBandLabels ), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_144") );

					this.showBandLabels = value;

					this.Layout.DirtyGridElement( );

					
					this.NotifyPropChange( PropertyIds.ShowBandLabels, null );
				}
			}
		}

		/// <summary>
		/// Returns or sets a value that determines the GroupBy box's display style.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property specifies the display style of the GroupBy box. When set to 0 (GroupByBoxStyle.Full) the full GroupBy Box will be displayed, with the arrangement of the buttons corresponding to the group by columns and bands. When the 1 (AddNewBoxStyleCompact) setting is used, the GroupBy Box will be displayed using as little real estate as possible while still maintaining a visually acceptable appearance.</p>
		/// </remarks>
		[ LocalizedDescription("LD_ButtonDefinition_P_Style") ]
		[ LocalizedCategory("LC_Display") ]
		public GroupByBoxStyle Style
		{
			get
			{		
				return this.style;						
			}
			set
			{
				if( this.style != value )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof( GroupByBoxStyle ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_330"), (int)value, typeof( GroupByBoxStyle ) );

					this.style = value;
					this.NotifyPropChange( PropertyIds.GroupByBoxStyle, null );
				}
				this.style = value;
			}
		}

		internal UIElement UIElement
		{
			get
			{
				UIElement elem = this.Layout.GetUIElement( false );

				return elem.GetDescendant( typeof( GroupByBoxUIElement ), this );
			}
		}

		/// <summary>
		/// Checks to see if  ShowBandLabels property needs to be serialized
		/// </summary>
		public bool ShouldSerializeShowBandLabels( )
		{
			return ShowBandLabels.Default != this.showBandLabels;
		}
 
		/// <summary>
		/// Resets ShowBandLabels property to its default value
		/// </summary>
		public void ResetShowBandLabels( )
		{
			this.ShowBandLabels = ShowBandLabels.Default;
		}



		/// <summary>
		/// Determines the style of borders around buttons in the GroupByBox.
		/// </summary>
		/// <seealso cref="Infragistics.Win.UIElementBorderStyle"/>
		[ LocalizedDescription("LD_ButtonDefinition_P_ButtonBorderStyle") ]
		[ LocalizedCategory("LC_Display") ]
		public Infragistics.Win.UIElementBorderStyle ButtonBorderStyle
		{
			get
			{
				return this.buttonBorderStyle;
			}

			set
			{
				if ( value != this.buttonBorderStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UIElementBorderStyle), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_163") );

					this.buttonBorderStyle = value;
					this.NotifyPropChange( PropertyIds.BorderStyle, null );
				}
			}
		}

		/// <summary>
		/// Returns true if the Button Border Style property needs to be serialized (not null )
		/// </summary>
		/// <returns></returns>
		public bool ShouldSerializeButtonBorderStyle() 
		{
			return ( this.buttonBorderStyle != Infragistics.Win.UIElementBorderStyle.Default );
		}
 
		/// <summary>
		/// Resets the Button Border Style to default
		/// </summary>
		public void ResetButtonBorderStyle() 
		{
			this.ButtonBorderStyle = Infragistics.Win.UIElementBorderStyle.Default;
		}


		internal Infragistics.Win.UIElementBorderStyle ButtonBorderStyleResolved
		{
			get
			{
				// SSP 3/28/06 - App Styling
				// 
				
				//return Infragistics.Win.UIElementBorderStyle.Default == this.ButtonBorderStyle ?
				//	Infragistics.Win.UIElementBorderStyle.Raised : this.ButtonBorderStyle;
				object val;
				if ( ! StyleUtils.GetCachedPropertyVal( this.Layout, StyleUtils.CachedProperty.BorderStyleGroupByBoxColumnHeader, out val ) )
				{
					val = StyleUtils.CacheBorderStylePropertyValue( this.Layout,
						StyleUtils.CachedProperty.BorderStyleGroupByBoxColumnHeader,
						StyleUtils.Role.GroupByBoxColumnHeader, 
						this.ButtonBorderStyle,
						UIElementBorderStyle.Raised );
				}

				return (UIElementBorderStyle)val;
				
			}
		}

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info,
			StreamingContext context )
		{
						
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			base.GetObjectData( info, context );

			if ( this.ShouldSerializeBandLabelAppearance( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BandLabelAppearance", this.bandLabelAppearanceHolder );
				//info.AddValue("BandLabelAppearance", this.bandLabelAppearanceHolder );
			}

			// SSP 1/3/02 UWG706
			// Implemented PromptAppearance property
			//
			if ( this.ShouldSerializePromptAppearance( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "PromptAppearance", this.promptAppearanceHolder );
				//info.AddValue( "PromptAppearance", this.promptAppearanceHolder );
			}

			if ( this.ShouldSerializeBandLabelBorderStyle( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "BandLabelBorderStyle", this.bandLabelBorderStyle );
				//info.AddValue("BandLabelBorderStyle", (int)this.bandLabelBorderStyle );
			}

			if ( this.ShouldSerializeShowBandLabels( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ShowBandLabels", this.showBandLabels );
				//info.AddValue("ShowBandLabels", (int)this.showBandLabels );
			}

			if ( this.ShouldSerializeButtonBorderStyle( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "ButtonBorderStyle", this.ButtonBorderStyle );
				//info.AddValue("ButtonBorderStyle", (int)this.ButtonBorderStyle );
			}

			// SSP 7/9/02
			// Implemented group by box compact style.
			//
			if ( this.ShouldSerializeStyle( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Style", this.Style );
				//info.AddValue( "Style", (int)this.Style );
			}
		}


        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        // AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//public GroupByBox( SerializationInfo info, StreamingContext context ) : base( info, context )
		private GroupByBox( SerializationInfo info, StreamingContext context ) : base( info, context )
		{
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "BandLabelAppearance":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.bandLabelAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.bandLabelAppearanceHolder = (AppearanceHolder)entry.Value;
						break;
					}
						// SSP 1/3/02
						// Implemented PromptAppearance
						//
					case "PromptAppearance":
					{
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.promptAppearanceHolder = (AppearanceHolder)Utils.DeserializeProperty( entry, typeof(AppearanceHolder), null );
						//this.promptAppearanceHolder = (AppearanceHolder)entry.Value;
						break;
					}
					case "BandLabelBorderStyle":
					{
						//this.BandLabelBorderStyle = ()(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.bandLabelBorderStyle = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.bandLabelBorderStyle );
						//this.bandLabelBorderStyle = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.BandLabelBorderStyle);
						break;
					}
					case "ShowBandLabels":
					{
						//this.ShowBandLabels = (ShowBandLabels)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.showBandLabels = (ShowBandLabels)Utils.DeserializeProperty( entry, typeof(ShowBandLabels), this.showBandLabels );
						//this.showBandLabels = (ShowBandLabels)Utils.ConvertEnum(entry.Value, this.ShowBandLabels);
						break;
					}

					case "ButtonBorderStyle":
					{
						//this.ButtonBorderStyle = (Infragistics.Win.UIElementBorderStyle)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.buttonBorderStyle = (UIElementBorderStyle)Utils.DeserializeProperty( entry, typeof(UIElementBorderStyle), this.buttonBorderStyle );
						//this.buttonBorderStyle = (UIElementBorderStyle)Utils.ConvertEnum(entry.Value, this.ButtonBorderStyle);
						break;
					}
						// SSP 7/9/02
						// Implemented group by box compact style.
						//
					case "Style":
					{
						//this.Style = (GroupByBoxStyle)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.style = (GroupByBoxStyle)Utils.DeserializeProperty( entry, typeof(GroupByBoxStyle), this.style );
						//this.style = (GroupByBoxStyle)Utils.ConvertEnum(entry.Value, this.Style);
						break;
					}
				}
			}
		}

		internal override void OnInitLayout()
		{
			// JJD 10/24/01
			// Init the cellappearance with the appearances collection
			// if necessary
			//
			if ( this.Layout != null )
			{
				if ( this.bandLabelAppearanceHolder != null )
					this.bandLabelAppearanceHolder.Collection = this.Layout.Appearances;

				base.OnInitLayout();
			}
		}

		// MRS 2/23/04 - Added Preset support
		#region Implementation of ISupportPresets
		/// <summary>
		/// Returns a list of properties which can be used in a Preset
		/// </summary>
		/// <param name="presetType">Determines which type(s) of properties are returned</param>
		/// <returns>An array of strings indicating property names</returns>
		string[] ISupportPresets.GetPresetProperties(Infragistics.Win.PresetType presetType)
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList properties = new ArrayList();
			List<string> properties = new List<string>();

			//Appearance
			if ((presetType & Infragistics.Win.PresetType.Appearance) == Infragistics.Win.PresetType.Appearance)
			{
				properties.Add("Appearance");				
				properties.Add("BandLabelAppearance");	
				properties.Add("BandLabelBorderStyle");	
				properties.Add("BorderStyle");	
				properties.Add("ButtonBorderStyle");	
				properties.Add("ButtonConnectorStyle");	
				properties.Add("ButtonConnectorStyle");	

				// MRS 5/26/04 - Moved to Behavior
				//properties.Add("Hidden");	

				properties.Add("Prompt");	
				properties.Add("PromptAppearance");	
				properties.Add("ShowBandLabels");	
				properties.Add("Style");	
			}

			//Behavior
			if ((presetType & Infragistics.Win.PresetType.Behavior) == Infragistics.Win.PresetType.Behavior)
			{
				// MRS 5/26/04 - Moved ffrom Appearance
				properties.Add("Hidden");	
			}
			
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (string[])properties.ToArray(typeof(string));
			return properties.ToArray();
		}

		/// <summary>
		/// Returns the TypeName of the Preset target
		/// </summary>
		/// <returns>Returns "GroupByBox"</returns>
		string ISupportPresets.GetPresetTargetTypeName()
		{
			return "GroupByBox";
		}
		#endregion Implementation of ISupportPresets
		
	}

	
}
