#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;
using System.Runtime.Serialization;
using Infragistics.Win.UltraWinMaskedEdit;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using System.Text;
using System.Security.Permissions;
using Infragistics.Shared.Serialization;
using Infragistics.Win.Layout;
using System.Collections.Generic;

// SSP 1/30/03 - Row Layout Functionality
// Added RowLayout.cs file.
//

namespace Infragistics.Win.UltraWinGrid
{
	#region RowLayoutColumnInfosCollection

	/// <summary>
	/// RowLayoutColumnInfosCollection class.
	/// </summary>
	[ Serializable( ) ]
	[ TypeConverter( typeof( RowLayoutColumnInfosCollectionConverter ) ) ]
	public class RowLayoutColumnInfosCollection : KeyedSubObjectsCollectionBase, ISerializable
	{
		#region Private Variables

		private RowLayout rowLayout = null;

        // MRS - NAS 9.1 - Groups in RowLayout
        // We need to track both columns and groups now. 
        //
		//private int columnsCollectionVersion = -1;
        private int columnsAndGroupsCollectionVersion = -1;

		private bool inSynchronizeWithColumnsColection = false;

		#endregion // Private Variables

		#region Constructor

		internal RowLayoutColumnInfosCollection( RowLayout rowLayout )
		{
			if ( null == rowLayout )
				throw new ArgumentNullException( "rowLayout" );

			this.rowLayout = rowLayout;
		}

        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        protected RowLayoutColumnInfosCollection(SerializationInfo info, StreamingContext context)
		{
			this.Reset( );

			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop.
			//
			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( RowLayoutColumnInfo ) )
				{
					RowLayoutColumnInfo item = (RowLayoutColumnInfo)Utils.DeserializeProperty( entry, typeof( RowLayoutColumnInfo ), null );

					// Add the item to the collection
					if ( item != null )
						this.InternalAdd( item );
				}
				else
				{
					switch ( entry.Name )
					{
						case "Tag":
							this.DeserializeTag(entry);
							break;
						default:
							Debug.Assert( false, "Invalid entry in RowLayoutColumnInfo de-serialization constructor." );
							break;
					}
				}
			}
		}

		#endregion // Constructor

		#region Interanl Properties/Methods

		#region RowLayout

		private RowLayout RowLayout
		{
			get
			{
				return this.rowLayout;
			}
		}

		#endregion // RowLayout

		#region Band

		internal UltraGridBand Band
		{
			get
			{
				return null != this.RowLayout ? this.RowLayout.Band : null;
			}
		}

		#endregion // Band

		#region InitializeFrom
		
		internal void InitializeFrom( RowLayoutColumnInfosCollection ciColl )
		{
			this.InternalClear( );

			for ( int i = 0; i < this.Count; i++ )
			{
				RowLayoutColumnInfo ci = this[i];

				// First try the item at the same location as the ci.
				//
				RowLayoutColumnInfo tmpCi = i < ciColl.List.Count 
					? ciColl.List[ i ] as RowLayoutColumnInfo : null;

				if ( null != tmpCi && ci.CanInitializeFrom( tmpCi ) )
				{
					ci.InitializeFrom( tmpCi );
					continue;
				}

				// If that fails, then loop over all the items trying to find one that matches.
				//
				for ( int j = 0; j < ciColl.List.Count; j++ )
				{
					tmpCi = ciColl.List[j] as RowLayoutColumnInfo;

					if ( null != tmpCi && ci.CanInitializeFrom( tmpCi ) )
					{
						ci.InitializeFrom( tmpCi );
						break;
					}
				}
			}
		}

		#endregion // InitializeFrom

		#region SynchronizeWithColumnsColection

		private void SynchronizeWithColumnsColection( )
		{
			if ( this.inSynchronizeWithColumnsColection )
				return;

			this.inSynchronizeWithColumnsColection = true;

			try
			{
				int i;
				UltraGridBand band = this.Band;

                // MRS - NAS 9.1 - Groups in RowLayout
                // We need to track both columns and groups now. 
                //
                #region Old Code
                //ColumnsCollection columns = null != band ? band.Columns : null;

                //if (null == band || null == columns)
                //    return;

                //// get the number of columns
                ////
                //int columnCount = columns.Count;


                //// If our count is the same then exit.
                ////
                //if (this.columnsCollectionVersion == band.ColumnsVersion && columnCount == this.List.Count)
                //    return;

                //// if there are no columns then just clear the cells collection
                //// and return
                ////
                //if (0 == columnCount)
                //{
                //    this.InternalClear();
                //    return;
                //}

                //Hashtable hash = new Hashtable(this.List.Count, 1.0f);

                //for (i = 0; i < this.List.Count; i++)
                //{
                //    RowLayoutColumnInfo ci = (RowLayoutColumnInfo)this.List[i];

                //    if (null != ci && null != ci.Column)
                //        hash[ci.Column.Key] = ci;
                //}

                //this.List.Clear();

                //this.List.Capacity = columnCount;

                //for (i = 0; i < columnCount; i++)
                //{
                //    UltraGridColumn column = columns[i];

                //    RowLayoutColumnInfo ci = null;
                //    if (hash.ContainsKey(column.Key))
                //        ci = hash[column.Key] as RowLayoutColumnInfo;

                //    if (null != ci)
                //        ci.InitColumn(column);

                //    this.List.Add(ci);
                //}

                

                //// Cache the version number of the columns collection.
                ////
                //this.columnsCollectionVersion = band.ColumnsVersion;
                #endregion //Old Code
                //
                if (band == null)
                    return;

                ColumnsCollection columns = band.Columns;
                GroupsCollection groups = band.Groups;

                if (null == columns &&
                    null == groups)
                {
                    return;
                }

                // get the number of columns
                //
                int columnCount = columns != null ? columns.Count : 0;

                // get the number of groups
                //
                int groupCount = groups != null ? groups.Count : 0;

                int bandVersion = band.ColumnsVersion + band.GroupsVersion;
                int bandCount = columnCount + groupCount;

                // If our versions and counts are the same, exit.
                //
                if (this.columnsAndGroupsCollectionVersion == bandVersion &&
                    bandCount == this.List.Count)
                {
                    return;
                }

                // if there are no columns or groups then just clear the cells collection
                // and return
                //
                if (0 == columnCount &&
                    0 == groupCount)
                {
                    this.InternalClear();
                    return;
                }

                Dictionary<UltraGridColumn, RowLayoutColumnInfo> columnLayoutInfos = new Dictionary<UltraGridColumn, RowLayoutColumnInfo>();
                Dictionary<UltraGridGroup, RowLayoutColumnInfo> groupLayoutInfos = new Dictionary<UltraGridGroup, RowLayoutColumnInfo>();                

                for (i = 0; i < this.List.Count; i++)
                {
                    RowLayoutColumnInfo ci = (RowLayoutColumnInfo)this.List[i];

                    switch (ci.ContextType)
                    {
                        case RowLayoutColumnInfoContext.Column:
                            columnLayoutInfos[ci.Column] = ci;
                            break;
                        case RowLayoutColumnInfoContext.Group:                            
                            groupLayoutInfos[ci.Group] = ci;
                            break;
                        default:
                            Debug.Fail("Unknown RowLayoutColumnInfoContext");
                            break;
                    }
                }

                this.List.Clear();

                this.List.Capacity = bandCount;

                // Add in the columns
                for (i = 0; i < columnCount; i++)
                {
                    UltraGridColumn column = columns[i];

                    RowLayoutColumnInfo ci = null;
                    bool success = columnLayoutInfos.TryGetValue(column, out ci);

                    if (success)
                        ci.InitColumn(column);

                    this.List.Add(ci);
                }

                // Add in the groups
                for (i = 0; i < groupCount; i++)
                {
                    UltraGridGroup group = groups[i];

                    RowLayoutColumnInfo ci = null;
                    bool success = groupLayoutInfos.TryGetValue(group, out ci);

                    if (success)
                        ci.InitGroup(group);

                    this.List.Add(ci);
                }

                // Cache the version number of the columns collection.
                //
                this.columnsAndGroupsCollectionVersion = bandVersion;
            }
			finally
			{
				this.inSynchronizeWithColumnsColection = false;
			}
		}

		#endregion //SynchronizeWithColumnsColection

		#region ShouldSerialize
		
		internal bool ShouldSerialize( )
		{
			return this.List.Count > 0;
		}

		#endregion // ShouldSerialize

		#endregion // Interanl Properties/Methods

		#region Base Overrides

		#region All

		/// <summary>
		/// The collection as an array of objects
		/// </summary>
		public override object[] All 
		{
			get 
			{
				object[] array = new object[ this.Count ];

				for ( int i = 0; i < this.Count; i++ )
				{
					array[ i ] = this[i];
				}

				return array;
			}
			set 
			{
				throw new NotSupportedException( );
			}
		}

		#endregion // All

		#region IndexOf

		/// <summary>
		/// Returns the index of the item in the collection that has the
		/// passed in key or -1 if key not found.
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <returns>The index of the item in the collection that has the passed in key, or -1
        /// if no item is found.</returns>
        public override int IndexOf(String key) 
		{
			// Get the index from the columns collection. Check for band being null because during 
			// deserialization it may be null.
			//
			if ( null != this.Band )
				return this.Band.Columns.IndexOf( key );

			return base.IndexOf( key );
		}

		#endregion //IndexOf

		#region GetItem

		/// <summary>
		/// Returns the cell of the columns that has the key
		/// </summary>
        /// <param name="key">A value that uniquely identifies an object in a collection.</param>
        /// <returns>The item in the collection that has the passed in key.</returns>
        protected override IKeyedSubObject GetItem(String key) 
		{
			// Syncronize with band's column's collection.
			//
			this.SynchronizeWithColumnsColection( );

			int index = this.IndexOf( key );

			if ( index < 0 )
				throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_106"), Shared.SR.GetString("LE_ArgumentException_320") );
 
			// Call GetObject below so that if the cell has not
			// been created it will be created now
			//
			return (IKeyedSubObject)this.GetItem( index );
		}

		/// <summary>
		/// Returns the cell at the specified index
		/// </summary>
        /// <param name="index">Index of the object to retrieve.</param>
        /// <returns>The object at the index</returns>
        public override object GetItem(int index)
		{
			// Syncronize with band's column's collection.
			//
			this.SynchronizeWithColumnsColection( );

			RowLayoutColumnInfo ci = (RowLayoutColumnInfo)this.List[ index ];

			// if the slot for this RowLayoutColumnInfo is null then create it now.
			//
			if ( null == ci && null != this.Band )
			{
                // MRS - NAS 9.1 - Groups in RowLayout
				//ci = new RowLayoutColumnInfo( this.Band.Columns[ index ] );
                //
                ColumnsCollection columns = this.Band.Columns;
                int columnCount = columns != null ? columns.Count : 0;
                if (index < columnCount)
                    ci = new RowLayoutColumnInfo(columns[index]);
                else
                    ci = new RowLayoutColumnInfo(this.Band.Groups[index - columnCount]);

				this.List[ index ] = ci;
			}

			return ci;
		}
		
		#endregion // GetItem

		#region Count

		/// <summary>
		/// Overridden. Returns the count.
		/// </summary>
		public override int Count
		{
			get
			{
				// Syncronize with band's column's collection.
				//
				this.SynchronizeWithColumnsColection( );

				return base.Count;
			}
		}

		#endregion // Count
		
		#region InitialCapacity

		/// <summary>
		/// Property that specifies the initial capacity
		/// of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return 5;
			}
		}

		#endregion // InitialCapacity

		#region IsReadOnly

		/// <summary>
		/// Returns true if the collection is read-only
		/// </summary>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		#endregion // IsReadOnly

		#region OnSubObjectPropChanged

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			this.NotifyPropChange( PropertyIds.RowLayoutColumnInfo, propChange );
		}

		#endregion // OnSubObjectPropChanged
		
		#region OnDispose

		/// <summary>
		/// Called when the object is being disposed.
		/// </summary>
		protected override void OnDispose( )
		{
			base.OnDispose( );
		}

		#endregion // OnDispose        

        // MRS - NAS 9.1 - Groups in RowLayout
        #region NAS 9.1 - Groups in RowLayout

        #region AllowDuplicateKeys
        /// <summary>
        /// Returns true if the collection allows 2 or more items to have the same key value.
        /// </summary>
        /// <remarks>
        /// <para class="body">This does not apply to items whose keys are null or empty. There can always be multiple items with null or empty keys unless the Infragistics.Shared.KeyedSubObjectsCollectionBase.AllowEmptyKeys property returns false.</para>
        /// </remarks>
        public override bool AllowDuplicateKeys
        {
            get
            {
                // This must return true because the collection now contains both columns and groups and the
                // can be duplicate keys. 
                return true;
            }
        }
        #endregion //AllowDuplicateKeys

        #endregion //NAS 9.1 - Groups in RowLayout

        #endregion // Base Overrides

        #region Public Properties/Methods

        #region Indexer

        /// <summary>
		/// indexer (by column)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo this[ UltraGridColumn column ] 
		{
			get
			{
				return (RowLayoutColumnInfo)this.GetItem( column.Index );
			}
		}

        // MRS - NAS 9.1 - Groups in RowLayout
        /// <summary>
        /// indexer (by group)
        /// </summary>
        public Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo this[UltraGridGroup group]
        {
            get
            {
                ColumnsCollection columns = this.Band.Columns;
                int columnCount = columns != null ? columns.Count : 0;
                return (RowLayoutColumnInfo)this.GetItem(group.Index + columnCount);                                
            }
        }

		/// <summary>
		/// indexer (by string key)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo this[ string columnKey ] 
		{
			get
			{
				return (RowLayoutColumnInfo)this.GetItem( columnKey );
			}
		}

		/// <summary>
		/// indexer
		/// </summary>
		public Infragistics.Win.UltraWinGrid.RowLayoutColumnInfo this[ int index ] 
		{
			get
			{
				// Syncronize with band's column's collection.
				//
				this.SynchronizeWithColumnsColection( );

				return (RowLayoutColumnInfo)this.GetItem( index );
			}
		}
		
		#endregion // Indexer

		#region Reset

		/// <summary>
		/// Resets the object.
		/// </summary>
		public void Reset( )
		{
			for ( int i = 0; i < this.List.Count; i++ )
			{
				RowLayoutColumnInfo ci = this.List[i] as RowLayoutColumnInfo;

				if ( null != ci )
					ci.Reset( );
			}
		}

		#endregion // Reset

		#region AddRange

		/// <summary>
		/// Adds a range of RowLayoutColumnInfo objects.
		/// </summary>
		/// <param name="ciArr"></param>
		[ EditorBrowsable( EditorBrowsableState.Never ) ]
		public void AddRange( RowLayoutColumnInfo[] ciArr )
		{
			if ( null != this.Band && null != this.Band.Layout && null != this.Band.Layout.Grid &&
				! this.Band.Layout.Grid.DesignMode && ! this.Band.Layout.Grid.Initializing )
			{
				throw new InvalidOperationException( "AddRange is not supported during runtime." );
			}

			for ( int i = 0; null != ciArr && i < ciArr.Length; i++ )
			{
                if (ciArr[i] is ColumnLayoutInfo ||
                    // MRS - NAS 9.1 - Groups in RowLayout
                    ciArr[i] is Infragistics.Win.UltraWinGrid.UltraGridGroup.GroupLayoutInfo)
                {
                    throw new InvalidOperationException("Only RowLayoutColumnInfo objects can be added and not ColumnLayoutInfo or GroupLayoutInfo.");
                }

				if ( null != ciArr[i] )
					this.InternalAdd( ciArr[i] );
			}
		}

		#endregion // AddRange

		#endregion // Public Properties/Methods

		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			this.SerializeTag( info );

			// Serialize the items. Notice we are working off the List here (accessing the 
			// Count and the indexer off the List rather than this object). That's because
			// we don't want to create RowLayoutColumnInfo objects unless already created.
			//
			for ( int i = 0; i < this.List.Count; i++ )
			{
				RowLayoutColumnInfo ci = this.List[i] as RowLayoutColumnInfo;

				if ( null != ci )
					Utils.SerializeProperty( info, i.ToString( ), ci );
			}
		}

		#endregion // ISerializable.GetObjectData
	}

	#endregion // RowLayoutColumnInfosCollection
	
	#region RowLayoutColumnInfosCollectionConverter Class

	/// <summary>
	/// RowLayoutsCollectionConverter Class
	/// </summary>
	public class RowLayoutColumnInfosCollectionConverter : TypeConverter
	{
		/// <summary>
		/// Constructor.
		/// </summary>
		public RowLayoutColumnInfosCollectionConverter( )
		{
		}

        /// <summary>
        /// Returns whether this object supports properties, using the specified context.
        /// </summary>
        /// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
        /// <returns>true if <see cref="System.ComponentModel.TypeConverter.GetProperties(System.Object)"/> should be called to find the properties of this object; otherwise, false.</returns>
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

        /// <summary>
        /// Returns a collection of properties for the type of array specified by the
        /// value parameter, using the specified context and attributes.
        /// </summary>
        /// <param name="context">An <see cref="System.ComponentModel.ITypeDescriptorContext"/> that provides a format context.</param>
        /// <param name="value">An <see cref="System.Object"/> that specifies the type of array for which to get properties.</param>
        /// <param name="attributes">An array of type <see cref="System.Attribute"/> that is used as a filter.</param>
        /// <returns>A <see cref="System.ComponentModel.PropertyDescriptorCollection"/> with the properties that are exposed for this data type, or null if there are no properties.</returns>
        public override PropertyDescriptorCollection GetProperties(
			ITypeDescriptorContext context,
			object value,
			Attribute[] attributes )
		{
			PropertyDescriptorCollection props = null;

			RowLayoutColumnInfosCollection ciColl = value as RowLayoutColumnInfosCollection;

			if ( null != ciColl && ciColl.Count > 0 )
			{
				PropertyDescriptor [] propArray = new PropertyDescriptor[ ciColl.Count ];

				for ( int i = 0; i < propArray.Length; i++ )
				{
					RowLayoutColumnInfo ci = ciColl[i];

					System.Text.StringBuilder sb = new System.Text.StringBuilder( 20 );

					sb.Append( i );

					string key = ci.Key;

					if ( key != null && key.Length > 0 )
					{
						sb.Append( " - " );
						sb.Append( key );
					}

					propArray[i] = new RowLayoutColumnInfoPropertyDescriptor( ci, sb.ToString( ) );	
				}

				props = new PropertyDescriptorCollection( propArray );
			}

			return props;
		}
	}

	#endregion // RowLayoutColumnInfosCollectionConverter Class

	#region RowLayoutColumnInfoPropertyDescriptor Class

	/// <summary>
	/// RowLayoutColumnInfosCollectionPropertyDescriptor class.
	/// </summary>
	public class RowLayoutColumnInfoPropertyDescriptor : PropertyDescriptor 
	{
		private RowLayoutColumnInfo ci = null ;
		private string name = null ;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="ci">The row layout column info.</param>
        /// <param name="name">The name of the property.</param>
		public RowLayoutColumnInfoPropertyDescriptor( RowLayoutColumnInfo ci, string name ) : base( name, null ) 
		{
			this.ci = ci;
			this.name = name;
		}


		/// <summary>
		/// Gets category
		/// </summary>
		public override string Category 
		{ 
			get 
			{
				return typeof( RowLayoutColumnInfo ).Name;
			}
		}
  
		/// <summary>
		/// Gets/SEts resource name
		/// </summary>
		public string ResourceName 
		{
			get { return name ; }
			set { name = value ; }
		}

		/// <summary>
		/// Gets resource value
		/// </summary>
		public object ResourceValue 
		{
			get { return this.ci ; }
		}

		/// <summary>
		/// Gets component type
		/// </summary>
		public override Type ComponentType 
		{
			get 
			{
				return typeof( RowLayoutColumnInfo );
			}
		}


		/// <summary>
		/// Return false
		/// </summary>
		public override bool IsReadOnly 
		{
			get 
			{
				return false;
			}
		}


		/// <summary>
		/// Gets property type
		/// </summary>
		public override Type PropertyType 
		{
			get 
			{
				return typeof( RowLayoutColumnInfo );
			}
		}


		/// <summary>
        /// Returns true
		/// </summary>
        /// <param name="component">The component to test for reset capability.</param>
        /// <returns>true</returns>
		public override bool CanResetValue(object component) 
		{
			return true ;
		}

		/// <summary>
		/// Returns band object
		/// </summary>
        /// <param name="component">The component with the property for which to retrieve the value.</param>
		/// <returns>The band object</returns>
		public override object GetValue(object component) 
		{
			return this.ci;
		}

		/// <summary>
		/// Does nothing
		/// </summary>
		/// <param name="component"></param>
		public override void ResetValue(object component) 
		{
			this.ci.Reset( );
		}

		/// <summary>
		/// Does nothing
		/// </summary>
        /// <param name="component">The component with the property value that is to be set.</param>
        /// <param name="value">The new value.</param>
		public override void SetValue(object component, object value) 
		{
		}

		/// <summary>
        /// Returns the ShouldSerialize of the row layout column info
		/// </summary>
        /// <param name="component">The component with the property to be examined for persistence.</param>
        /// <returns>The ShouldSerialize of the row layout column info</returns>
		public override bool ShouldSerializeValue(object component) 
		{
			// AS 2/26/06 Filter Modified Properties
			//return false;
			return this.ci.ShouldSerialize();
		}
	}

	#endregion // RowLayoutColumnInfoPropertyDescriptor Class
}
