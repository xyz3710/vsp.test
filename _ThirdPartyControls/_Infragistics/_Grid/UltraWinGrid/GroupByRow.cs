#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.ComponentModel;
	using System.Collections;
	using System.ComponentModel.Design;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Security.Permissions;
	using System.Collections.Generic;

	/// <summary>
	/// The GroupByRow object corresponds to a GroupBy Row displayed in the grid.
	/// </summary>
	/// <remarks>
	/// <p class="body">The GroupBy Row appears in a Band of the grid that has been placed in GroupBy mode and has had at least one field specified for grouping data. GroupBy Rows are similar to regular grid Rows except that they do not display data from multiple fields. Instead, they serve to visually separate rows of data according to the grouping to which they belong. A GroupBy Row displays only the data value from the filed or fields being used to group the data.</p>
	/// <p class="body">For example, if you have a Band that contains address data and you use GroupBy mode to group the data according to the value of the City field, You will see the a GroupBy Row appear for every city that appears in the data; the GroupBy Row displays just the name of each city. Following each GroupBy Row will be all the data records contain the same value for the City field as that displayed in the GroupBy Row.</p>
	/// <p class="body">GroupBy Rows can be expanded or collapsed to display or hide the rows corresponding to a particular value. The GroupBy Row also contains the user interface elements needed to expand and collapse rows. Programmatically, you can use this object to find out information about the state of the row grouping, such as whether the grouping can be expanded and how many rows are grouped under the GroupBy Row.</p>
	/// </remarks>
	// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
	// Changed the hierarchy to derive from UltraGridSpecialRowBase instead of the UltraGridRow.
	//
	//public sealed class UltraGridGroupByRow : Infragistics.Win.UltraWinGrid.UltraGridRow
	public sealed class UltraGridGroupByRow : Infragistics.Win.UltraWinGrid.UltraGridSpecialRowBase
	{
		private UltraGridColumn column = null;
		private object commonValue = null;
		// JJD 1/17/02
		// Added support for setting the groupby row's description
		private string groupByDescription = null; 

		// SSP 5/1/03
		// Now we are using the editor to convert the group-by-row value to the display
		// text. So there is no need for storing the last value list index. However do
		// store the editor context because that's what the editor uses to cache the
		// index of last matched item in the value list.
		//
		//private int lastValueListIndex = -1;
		private object editorContext = null;
		
		// SSP 3/1/02
		// Overrode Height property.
		//
		private int minGroupByRowHeight = 0;

        // MBS 5/11/09 - NA9.2 GroupByRowConnector Appearance
        private AppearanceHolder rowConnectorAppearanceHolder;
	
		// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
		// Added the following overload of the constructor.
		// 
		internal UltraGridGroupByRow( 
			UltraGridBand band, 
			Infragistics.Win.UltraWinGrid.RowsCollection parentCollection,
			UltraGridColumn groupByColumn, 
			UltraGridRow firstRow ) 
			: this( band, parentCollection, groupByColumn, firstRow.GetCellValue( groupByColumn ) )
		{
			object cellImage = null;
			UltraGridCell cell = firstRow.GetCellIfAllocated( groupByColumn );
			if ( null != cell && cell.HasAppearance )
				cellImage = cell.Appearance.Image;

			if ( null == cellImage )
				cellImage = firstRow.GetCellValue( groupByColumn ) as Image;

			// Take care of value list item images.
			// 
			if ( null == cellImage )
			{
				AppearanceData appData = new AppearanceData( );
				AppearancePropFlags flags = AppearancePropFlags.Image;
				firstRow.ResolveEditorCellValueAppearance( groupByColumn, ref appData, ref flags );
				cellImage = appData.Image;
			}

			// If the cell associated with the first descendant non-group-by row and the group-by column
			// contains a byte[] that EmbeddableImageRenderer can render as an image, then we need to
			// set the group-by row's Appearance.Image to a value that indicates the GroupByRowUIElement's
			// PositionChildElements that it needs to use the EmbeddableImageRenderer to render the image.
			// Appearance.Image will be set to an instance of UseCellDataAsImage class as such an indicator.
			// 
			if ( null == cellImage && groupByColumn.Editor is EmbeddableImageRenderer )
				cellImage = new UseCellDataAsImage( );

			if ( null != cellImage )
				this.Appearance.Image = cellImage;
		}

		// SSP 8/10/05 - NAS 5.3 Cell Image in Group-by Row
		// If the cell associated with the first descendant non-group-by row and the group-by column
		// contains a byte[] that EmbeddableImageRenderer can render as an image, then we need to
		// set the group-by row's Appearance.Image to a value that indicates the GroupByRowUIElement's
		// PositionChildElements that it needs to use the EmbeddableImageRenderer to render the image.
		// Appearance.Image will be set to an instance of this class as such an indicator.
		// 
		internal class UseCellDataAsImage
		{
		}
		
		internal UltraGridGroupByRow( UltraGridBand band, Infragistics.Win.UltraWinGrid.RowsCollection parentCollection,
			UltraGridColumn groupByColumn, object commonValue ) 
			// SSP 4/5/04 - Virtual Binding Related Optimizations
			//
			//: base( band, -1, parentCollection )
			// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
			// Changed the hierarchy to derive from UltraGridSpecialRowBase instead of the UltraGridRow.
			// UltraGridSpecialRowBase implements the following method in the same way.
			//
			//: base( band, parentCollection )
			: base( parentCollection )
		{
			this.column = groupByColumn;
			this.commonValue = commonValue;

			// JAS 2005 v2 GroupBy Row Extensions
			//
			if( band != null )
				this.Expanded = band.GroupByRowInitialExpansionStateResolved == GroupByRowInitialExpansionState.Expanded;
		}

		internal void InitializeCommonValue ( object commonValue )
		{
			this.commonValue = commonValue;
		}

		/// <summary>
		/// Overridden. Returns ChildBands collection with one ChildBand
		/// contaning child rows for this GroupByRow
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
		Browsable(false)   ]
		public override Infragistics.Win.UltraWinGrid.ChildBandsCollection ChildBands
		{
			get
			{
				if ( null == this.childBands )
				{
					this.childBands = new ChildBandsCollection( this );
					this.childBands.InternalAdd( new UltraGridChildBand( this.childBands, this.Band ) );
				}

				return this.childBands;
			}
		}

		#region IsGroupByRow

		// SSP 2/13/04
		// Added IsGroupByRow property.
		//
		/// <summary>
		/// Indicates whether this is a group-by row.
		/// </summary>
		/// <remarks>
		/// <p class="body"><b>IsGroupByRow</b> can be used to check if a row is a group-by row or a non-group-by row.</p>
		/// <seealso cref="UltraGridGroupByRow"/>
		/// </remarks>
		public override bool IsGroupByRow
		{
			get
			{
				return true;
			}
		}

		#endregion // IsGroupByRow

		#region GroupByRowLevel
		
		internal int GroupByRowLevel
		{
			get
			{
				// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
				// Added GroupByRowLevel property to RowsCollection with the same logic.
				//
				
				return this.ParentCollection.GroupByRowLevel;
			}
		}

		#endregion // GroupByRowLevel

		/// <summary>
		/// True only if this row and all of its ancestor rows are still valid. This property is read-only.
		/// </summary>
		public override bool IsStillValid 
		{ 
			get 
			{
				try
				{
					// if the associated column is no longer a groiupby column
					// or the view style doesn't support groupbyrows then
					// return false
					//
					if ( !this.Column.IsGroupByColumn ||
						 !this.Layout.ViewStyleImpl.SupportsGroupByRows )
						return false;

					// call the base implementation to do other validations
					//
					return base.IsStillValid;
				}
				catch(Exception)
				{
					// return false if an error was thrown
					//
					return false;
				}
			} 
		}

		/// <summary>
		/// Always returns False since GroupBy rows are never displayed as cards in CardView. This property is read-only.
		/// </summary>
		public override bool IsCard { get { return false; } }

		/// <summary>
		/// Returns True if the row should display an expansion indicator.
		/// </summary>
		/// <remarks>
		/// <p class="body">This property is used to indicate whether the GroupBy Row should display an expansion indicator. Generally, an expansion indicator is present whenever there are one or more data rows associated with the GroupBy Row.</p>
		/// </remarks>
		public override bool HasExpansionIndicator
		{
			get
			{
				return this.Rows.Count > 0;
			}
		}
		

		internal override void FireInitializeRow( )
		{
			this.inInitializeRowEvent = true;

			try 
			{
				UltraGrid grid = this.Band.Layout.Grid as UltraGrid;

				if ( null != grid )
				{

					// fire the initialize row event
					//
					grid.FireInitializeGroupRow( new InitializeGroupByRowEventArgs( this, this.initializeRowEventFired ) );
				}
			}
			finally
			{
				this.initializeRowEventFired = true;
				this.inInitializeRowEvent = false;
			}
		}

		// MD 8/3/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// finds the top level group by row for this band
		//        /// </summary>
		//#endif
		//        internal UltraGridGroupByRow FindTopLevelGroupByRow( )
		//        {
		//            // MD 7/27/07 - 7.3 Performance
		//            // Refactored - Prevent calling expensive getters multiple times
		//            //if ( null != this.ParentRow && ( this.ParentRow is UltraGridGroupByRow ) )
		//            //{
		//            //    UltraGridGroupByRow groupByParentRow = (UltraGridGroupByRow)this.ParentRow;
		//            UltraGridGroupByRow groupByParentRow = this.ParentRow as UltraGridGroupByRow;

		//            if ( groupByParentRow != null )
		//            {
		//                return groupByParentRow.FindTopLevelGroupByRow( );
		//            }

		//            return this;
		//        }

		#endregion Not Used

		/// <summary>
		/// Value common to all the rows in this GroupByRow (for the associated column)
		/// </summary>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridGroupByRow.Description"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.GroupByRowDescriptionMask"/>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.UltraGridColumn.GroupByEvaluator"/>
		public object Value
		{
			get
			{
				return this.commonValue;
			}
		}

		#region ValueAsDisplayText

		// SSP 12/6/07 BR27015
		// Added ValueAsDisplayText.
		// 
		/// <summary>
		/// Returns the value of this group as display text.
		/// </summary>
		/// <remarks>
		/// <para class="body">
		/// <see cref="Value"/> property returns the value of the group. <b>ValueAsDisplayText</b>
		/// returns the value converted to text. It takes into account various settings like the
		/// format, value list etc... to convert the value to the display text.
		/// </para>
		/// <seealso cref="Value"/>
		/// </remarks>
		public string ValueAsDisplayText
		{
			get
			{
				return this.GetDisplayText( );
			}
		}

		#endregion // ValueAsDisplayText

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Mark members as static
		//private string[] MyStringSplit( string str, char splitChar )
		private static string[] MyStringSplit( string str, char splitChar )
		{
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//ArrayList list = new ArrayList( 4 );
			List<string> list = new List<string>( 4 );
			
			int lastIndex = -1;
			int i = 0;
			for (  ; i < str.Length; i++ )
			{
				if ( splitChar == str[i] )
				{
					list.Add( str.Substring( 1 + lastIndex, i - lastIndex - 1 ) );

					lastIndex = i;
				}
			}

			if ( str.Length > 0 )
				list.Add( str.Substring( 1 + lastIndex, i - lastIndex - 1  ) );
            
			// MD 8/10/07 - 7.3 Performance
			// Use generics
			//return (string[])list.ToArray( typeof( string ) );
			return list.ToArray();
		}


		private string GetAtomValue( string atom )
		{
			// SSP 6/12/03 UWG2244
			// Use the visible row count rather than the total count of child rows because when the 
			// child rows are filtered out, the group-by row desciption shows the wrong count.
			//
			// SSP 4/23/05 - NAS 5.2 Filter Row/Summaries Feature Extensions
			// Do not include special rows into the visible row count.
			//
			//int visibleRowCount = this.Rows.VisibleRowCount;
			int visibleRowCount = this.Rows.GetVisibleRowCount( false );

			// JJD 11/28/01
			// Changed mask delimiters fron '<>' to '[]' so we could more
			// easily document it in XML
			//
			// AS 1/8/03 - fxcop
			// Must supply an explicit CultureInfo to the String.Compare method.
			//if ( 0 == String.Compare( atom, "[caption]", true ) )
			if ( 0 == String.Compare( atom, "[caption]", true, System.Globalization.CultureInfo.CurrentCulture ) )
			{
				return this.Column.Header.Caption;
			}
			// AS 1/8/03 - fxcop
			// Must supply an explicit CultureInfo to the String.Compare method.
			//else if ( 0 == String.Compare( atom, "[value]", true ) )
			else if ( 0 == String.Compare( atom, "[value]", true, System.Globalization.CultureInfo.CurrentCulture ) )
			{
				// SSP 11/9/01 UWG691
				// Use the new GetDisplayText method which takes into account the value
				// list and maps data value to display text if necessary.
				//
				//return this.Value.ToString( );
				return this.GetDisplayText( );
			}
			// AS 1/8/03 - fxcop
			// Must supply an explicit CultureInfo to the String.Compare method.
			//else if ( 0 == String.Compare( atom, "[count]", true ) )
			else if ( 0 == String.Compare( atom, "[count]", true, System.Globalization.CultureInfo.CurrentCulture ) )
			{
				// SSP 6/12/03 UWG2244
				// Use the visible row count rather than the total count of child rows because when the 
				// child rows are filtered out, the group-by row desciption shows the wrong count.
				//
				//return string.Empty + this.Rows.Count;
				return visibleRowCount.ToString( );
			}
			else 
			{
				if ( !atom.StartsWith( "[count" ) || !atom.EndsWith( "]" ) )
					return null;

				string temp = "[count,";
				// AS 1/8/03 fxcop
				// Explicitly specify a cultureinfo
				//
				//string lower = atom.ToLower( );
				string lower = atom.ToLower( System.Globalization.CultureInfo.CurrentCulture );

				if ( lower.StartsWith( temp ) )
				{
					// MD 7/26/07 - 7.3 Performance
					// FxCop - Mark members as static
					//string[] parts =  this.MyStringSplit( atom.Substring( 1, atom.Length - 2 ) , ',' );
					string[] parts = UltraGridGroupByRow.MyStringSplit( atom.Substring( 1, atom.Length - 2 ), ',' );

					// [count,x,y,z] has to have 4 parts. if not return null
					if ( 4 != parts.Length )
						return null;

					// SSP 6/12/03 UWG2244
					// Use the visible row count rather than the total count of child rows because when the 
					// child rows are filtered out, the group-by row desciption shows the wrong count.
					//
					
					if ( 0 == visibleRowCount )
						return parts[1];
					else if ( 1 == visibleRowCount )
						return parts[2];
					else 
						return parts[3];
				}
			}

			return null;
		}

		private string GetDescription( string descriptionMask )
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder( );

			string mask = descriptionMask;

			for ( int i = 0; i < mask.Length; i++ )
			{	
				string val = null;
				int index = -1;

				if ( '[' == mask[i] )
				{
					index = mask.IndexOf( "]", 1 + i );

					if ( index >= 0 )
					{
						string atom = mask.Substring( i, 1 + index - i );

						val = this.GetAtomValue( atom );
					}
				}

				if ( null != val )
				{
					sb.Append( val );
					i = index;
				}
				else
					sb.Append( mask[i] );
			}

			// SSP 11/25/02 UWG1859
			// If the caption is multiline, then replace the new line character with a space.
			//
			sb.Replace( "\r\n", " " );
			sb.Replace( '\n', ' ' );
			sb.Replace( '\r', ' ' );

			return sb.ToString( );
		}

		// SSP 5/1/03 - Cell Level Editor
		// Now we are using the editor to convert the group-by-row value to the display
		// text. So there is no need for storing the last value list index. However do
		// store the editor context because that's what the editor uses to cache the
		// index of last matched item in the value list.
		//
		internal object EditorContext
		{
			get
			{
				return this.editorContext;
			}
			set
			{
				this.editorContext = value;
			}
		}

		internal string GetDisplayText( )
		{

			object dataValue = this.Value;

            // MRS 1/4/2008 - BR29253
            // This is wrong, because it does not account for the format. We should just be using GetCellText
            #region Old Code
            //string str = null;

            //// SSP 5/1/03 - Cell Level Editor
            //// Use the editor to convert the data vlaue to text rather than doing it ourselves
            //// by going to the value list directly.
            ////
            //// --------------------------------------------------------------------------------
            
            //UltraGridColumn column = this.Column;
            //EmbeddableEditorBase editor = column.Editor;
            //// SSP 12/9/05 BR08226
            //// Apply the data filter.
            //// 
            //// ------------------------------------------------
            ////str = null != editor ? editor.DataValueToText( dataValue, this.Column.EditorOwnerInfo, this ) : null;
            //if ( null != editor )
            //{
            //    string error;
            //    str = column.ConvertDataValueToDisplayText( 
            //        editor, column.EditorOwnerInfo, this, dataValue, out error );
            //}
            //// ------------------------------------------------
            //// --------------------------------------------------------------------------------

            //if ( null == str )
            //    // SSP 1/25/02
            //    // Check for dataValue being null because if we have an unbound column
            //    // dataValue could potentially be null.
            //    //
            //    //str = dataValue.ToString( );
            //    str = null != dataValue 
            //           ? dataValue.ToString( ) 
            //           : string.Empty;

            //// AS 1/8/03 fxcop
            //// Explicitly specify a cultureinfo
            ////
            //if ( CharacterCasing.Upper == this.Column.CharacterCasing )
            //    //str = str.ToUpper( );
            //    str = str.ToUpper( System.Globalization.CultureInfo.CurrentCulture );
            //else if ( CharacterCasing.Lower == this.Column.CharacterCasing )
            //    //str = str.ToLower( );
            //    str = str.ToLower( System.Globalization.CultureInfo.CurrentCulture );

            //return str;
            #endregion //Old Code
            return this.Column.GetCellText(this, dataValue);
		}


		/// <summary>  
		/// Property: gets/sets description of the group by row 
		/// </summary>
		/// <remarks>
		/// By default the <see cref="Infragistics.Win.UltraWinGrid.UltraGridBand"/> or <see cref="Infragistics.Win.UltraWinGrid.UltraGridLayout"/> <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride"/>'s <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.GroupByRowDescriptionMask"/> will be used to format the group by row's <see cref="Infragistics.Win.UltraWinGrid.UltraGridGroupByRow.Value"/>.
		/// </remarks>
		public override string Description
		{
			get
			{
				// JJD 1/17/02
				// If the description has been set then use it
				//
				if ( this.groupByDescription != null &&
					 this.groupByDescription.Length > 0 )
					return this.groupByDescription;

				// SSP 11/9/01 UWG691
				// Changed the name of ApplyDescription to GetDescrption and took out the
				// argument of dataValue, since it's going to be used inside the GetDescription
				// method.
				//
				//return this.ApplyDescription( this.Value.ToString( ), this.Band.GroupByRowDescriptionMaskResolved );
				return this.GetDescription( this.Band.GroupByRowDescriptionMaskResolved );
			}
			set
			{
				// JJD 1/17/02
				// Added support for setting the groupby row's description
				//
				if( this.groupByDescription != value )
				{
					this.groupByDescription = value;
					this.NotifyPropChange( PropertyIds.Description );
				}
			}
		}

		#region DescriptionWithSummaries

		/// <summary>
		/// Returns the description of the group-by row, with any summaries appended to it.
		/// </summary>
		// MRS 11/7/05 - BR07250
		//
		//internal string DescriptionWithSummaries
		public string DescriptionWithSummaries
		{
			get
			{
				// SSP 9/2/03 UWG2330
				// If this is a group-by dummy row (HiddenResolved would be true )
				// then display a message indicating the rows are filtered out.
				// ----------------------------------------------------------------------
				if ( this.HiddenResolved )
					return SR.GetString( "AllRowsFilteredOut" );
				// ----------------------------------------------------------------------

				string description = this.Description;

				// If there are no summaries, then return the description.
				//
				if ( !this.Rows.HasSummaryValues )
					return description;

				// SSP 4/23/05 - NAS 5.2 Extension of Summaries Functionality
				// If GroupBySummaryDisplayStyle not Text then return without appending the summaries.
				//
				if ( GroupBySummaryDisplayStyle.Text != this.BandInternal.GroupBySummaryDisplayStyleResolved )
					return description;

				// Allocate a string builder only if necessary in the loop.
				//
				System.Text.StringBuilder summariesSB = null;

				SummaryValuesCollection summaryValues = this.Rows.SummaryValues;

				// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
				// Added ability to display summaries in group-by rows aligned with columns, 
				// summary footers for group-by row collections, fixed summary footers and
				// being able to display the summary on top of the row collection.
				//
				SummaryDisplayAreaContext context = SummaryDisplayAreaContext.GetInGroupByRowsSummariesContext( this );

				for ( int i = 0; i < summaryValues.Count; i++ )
				{
					SummaryValue summaryValue = summaryValues[i];

					// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
					// Added ability to display summaries in group-by rows aligned with columns, 
					// summary footers for group-by row collections, fixed summary footers and
					// being able to display the summary on top of the row collection.
					//
					//if ( summaryValue.SummarySettings.DisplayInGroupBy )
					if ( context.DoesSummaryMatch( summaryValue ) )
					{
						string summaryText = summaryValue.SummaryText;

						//Debug.Assert( null != summaryText && summaryText.Length > 0, "Null summary text !" );

						if ( null != summaryText && summaryText.Length > 0 )
						{
							if ( null == summariesSB )
								summariesSB = new System.Text.StringBuilder( 5 + description.Length + summaryText.Length );

							if ( summariesSB.Length > 0 )							
								summariesSB.Append( ", " );

							string columnName = null;

							if ( null != summaryValue.SummarySettings.SourceColumn )
								// SSP 12/2/02 UWG1865
								// Use the column caption instead of the column key.
								//
								//columnName = summaryValue.SummarySettings.SourceColumn.Key;
								columnName = summaryValue.SummarySettings.SourceColumn.Header.Caption;

							if ( null != columnName )
							{
								summariesSB.Append( columnName );
								summariesSB.Append( " " );
							}

							summariesSB.Append( summaryText );
						}
					}
				}

				// If we did not encounter any summaries that need to be displayed in
				// group by rows, then just return the description.
				//
				if ( null == summariesSB || summariesSB.Length <= 0 )
					return description;
				
				if ( null != description && description.Length > 0 )
				{
					summariesSB.Insert( 0, description );
					summariesSB.Insert( description.Length, " ");
				}

				// SSP 11/25/02 UWG1859 UWG1864
				// Replace the new line characters with space for group by row's captions.
				//
				summariesSB.Replace( "\r\n", " " );
				summariesSB.Replace( '\n', ' ' );
				summariesSB.Replace( '\r', ' ' );

				return summariesSB.ToString( );
			}
		}

		#endregion DescriptionWithSummaries

		/// <summary>
		/// Returns true since group by rows can always be expanded
		/// </summary>
		public override bool IsExpandable
		{
			get
			{
				// JAS 2005 v2 GroupBy Row Extensions
				//
				if( this.BandInternal != null )
				{
					GroupByRowExpansionStyle expansionStyle = this.BandInternal.GroupByRowExpansionStyleResolved;
				
                    // MRS 5/21/2009 - TFS17752
                    //return expansionStyle == GroupByRowExpansionStyle.ExpansionIndicatorAndDoubleClick ||
                    //       expansionStyle == GroupByRowExpansionStyle.DoubleClick;
                    switch (expansionStyle)
                    {
                        case GroupByRowExpansionStyle.Disabled:
                            return false;
                        case GroupByRowExpansionStyle.DoubleClick:
                        case GroupByRowExpansionStyle.ExpansionIndicator:
                        case GroupByRowExpansionStyle.ExpansionIndicatorAndDoubleClick:
                            return true;
                        case GroupByRowExpansionStyle.Default:
                        default:
                            Debug.Fail("Unknown or invalid expansionStyle");
                            return false;
                    }
				}

				return this.Rows.Count > 0;
			}
		}

		// SSP 1/14/02 UWG1799
		// Changed the access modifier from private to internal.
		//
		//private UltraGridRow FindFirstRegularRow( )
		internal UltraGridRow FindFirstRegularRow( )
		{
			if ( this.Rows.Count <= 0 )
				return null;

			UltraGridRow firstRow = this.Rows[0];

			// MD 8/7/07 - 7.3 Performance
			// FxCop - Do not cast unnecessarily
			//if ( firstRow is UltraGridGroupByRow )
			//    return ((UltraGridGroupByRow)firstRow).FindFirstRegularRow( );
			UltraGridGroupByRow groupByRow = firstRow as UltraGridGroupByRow;

			if ( groupByRow != null )
				return groupByRow.FindFirstRegularRow();

			return firstRow;
		}

		private bool DoesRowMatchGroupHelper( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			UltraGridColumn column = this.Column;

			// JAS v5.2 GroupBy Break Behavior 5/3/05
			//
			//IGroupByEvaluator evaluator = column.GroupByEvaluator;
			IGroupByEvaluator evaluator = column.GroupByEvaluatorResolved;

			// JJD 1/17/02
			// If they have specified an evaulator then call it
			//
			if ( evaluator != null )
				return evaluator.DoesGroupContainRow( this, row );

			IComparer comparer = column.SortComparer;

			// JJD 1/17/02
			// If they have specified a sort comparer then get the cell from
			// the first row in the collection and use that for comparison.
			//
			// SSP 1/23/02
			// Use the new FindFirstRegularRow method to get the first row.
			// FindFirstRegularRow drills down the descendant hierarchy and
			// gets a regular UltraGridRow (one that's not a UltraGridGroupByRow).
			//
			//if ( comparer != null && this.Rows.Count > 0 )
			if ( null != comparer )
			{
				UltraGridRow firstRow = this.FindFirstRegularRow( );

				Debug.Assert( null != firstRow, 
					"There must have been a firstRow.",
					"This method should not have been called when the descendent hierarchy of " +
					"this group by row does not contain at least one regular row." );					

				if ( null != firstRow )		
					// SSP 1/23/02
					// Use the firstRow that we got by means of FindFirstRegularRow method
					//
					//return comparer.Compare( this.Rows[0].Cells[ column ], row.Cells[ column ] ) == 0;
					return comparer.Compare( firstRow.Cells[ column ], row.Cells[ column ] ) == 0;
			}

			object x = this.Value;
			object y = row.GetCellValue( column );

			if ( x == null )
			{
				return ( y == null );
			}

			if ( y == null )
			{
				return false;
			}

			// SSP 1/19/05 BR01830
			// If not sorting (like when merging, or filtering), don't consider two objects 
			// as equal if their ToString results are equal because the ToString by default
			// returns the type name which would be the same even if the objects were 
			// different.
			//
			//return RowsCollection.DefaultCompare( x, y ) == 0;
			//
			// JAS v5.2 GroupBy Break Behavior 5/4/05 - Added 'column' argument.
			//
			//return RowsCollection.RowsSortComparer.DefaultCompare( x, y, false ) == 0;
			return RowsCollection.RowsSortComparer.DefaultCompare( x, y, false, column ) == 0;
		}


		internal bool DoesRowMatchGroup( Infragistics.Win.UltraWinGrid.UltraGridRow row, bool recursive )
		{
			// If the row does not match this group by row's criteria return false
			if ( !this.DoesRowMatchGroupHelper( row ) )
				return false;

			if ( recursive )
			{			
				UltraGridGroupByRow parent = this.ParentRow as UltraGridGroupByRow;

				// If we have a parent groupByRow, for the row to belong to this
				// group by row, it also has to belong to any parent groups. So
				// make sure that it does.
				//
				if ( null != parent )			
					return parent.DoesRowMatchGroup( row );
			}			

			return true;
		}

		/// <summary>
		/// Returns true if the row belongs in this group by row
		/// by walking up the parent group by rows if any and making
		/// sure that the row also matches them.
		/// </summary>
		/// <param name="row"></param>
		/// <returns></returns>
		internal bool DoesRowMatchGroup( Infragistics.Win.UltraWinGrid.UltraGridRow row )
		{
			return this.DoesRowMatchGroup( row, true );
		}


		// SSP 3/4/02
		// Added RefreshSortPosition method.
		//
		/// <summary>
		/// If the row is not at correct sort position, this method will
		/// reposition the row in the rows collection based on the current
		/// sort criteria.
		/// </summary>
		/// <remarks>
		/// This method can be useful in situations where a new row is added
		/// to the rows collection (which by default is appended at the end 
		/// of the collection) and you want to ensure the row is positioned
		/// at the right position in the collection based on the sort criteria
		/// without having to resort the whole collection. This method should
		/// not be used if the sort criteria itself changes which effects the
		/// whole rows collection.
		/// </remarks>
		public override void RefreshSortPosition( )
		{
			// SSP 12/20/04 BR00935
			// Following is for something I noticed while fixing above bug and is not required
			// to fix that bug. Implemented RefreshSortPosition for group-by rows.
			//
			this.ParentCollection.EnsureCorrectSortPosition( this );
		}

		/// <summary>
		/// Returns the column associated with this group-by row.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a Band is in GroupBy mode, the data is grouped according to data fields, which correspond to columns in the grid. To create a grouping, the user can drag a column header into the Band's GroupByBox and the data will be grouped according to the values found in that column.</p>
		/// <p class="body">The <b>Column</b> property of the <b>GroupByRow</b> specifies which column is being used to create the grouping of the current GroupBy Row.</p>
		/// </remarks>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}


		/// <summary>
		/// Overridden. Returns null since the group by rows don't have any cells collection.
		/// </summary>
		[
		Browsable(false),
		EditorBrowsable( EditorBrowsableState.Never )
		]
		public override CellsCollection Cells
		{
			get
			{
				Debug.Assert( false, "GroupByRow.Cells accessed !" );

				return null;
			}
		}

		/// <summary>
		/// Returns a collection of the Rows belonging to this GroupByRow.
		/// </summary>
		/// <remarks>
		/// <p class="body">When a Band is in GroupBy mode, the Rows of the band are grouped together under GroupByRows, based on the values of specified fields (columns) in the Band. When the user drags a column header into the Band's GroupByBox (which specifies the field to use for grouping Rows) the Rows of the grid are grouped together under GroupByRows that represent the values found in the field being used for grouping.</p>
		/// <p class="body">Each GroupByRow will have one or more data rows that share the same grouping criteria. The <b>Rows</b> property of the <b>GroupByRow</b> returns a collection of all the data rows that appear under the GroupByRow.</p>
		/// </remarks>
		public RowsCollection Rows
		{
			get
			{
				return this.ChildBands[0].Rows;
			}
		}


		private void ResolveHelper( ref AppearanceData appData,
			ref AppearancePropFlags requestedProps, bool isGroupByConnector )
		{

			ResolveAppearanceContext context = new ResolveAppearanceContext( typeof (Infragistics.Win.UltraWinGrid.UltraGridGroupByRow), requestedProps );

			UltraGridBand band = this.Band;
			UltraGridLayout layout = band.Layout;
			UltraGrid grid = layout.Grid as UltraGrid;

			// SSP 3/14/06 - App Styling
			// 
			context.Role = StyleUtils.GetRole( band, isGroupByConnector 
				? StyleUtils.Role.GroupByRowConnector : StyleUtils.Role.GroupByRow, out context.ResolutionOrder );

			// only resolve selected colors for the group by row
			// and not the connector
			//
            // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
            // We want to resolve a few other properties now when determining the appearance
            //
			//if ( !isGroupByConnector && (  this.Selected || ( null != grid && grid.Selected.Rows.Contains( this ) ) ) )
            if (!isGroupByConnector)
            {
                if (this.Selected || (null != grid && grid.Selected.Rows.Contains(this)))
                {
                    context.IsSelected = true;
                    band.ResolveAppearance(ref appData, ref context);

                    if (context.UnresolvedProps == 0)
                        return;

                    context.IsSelected = false;
                }
            }
            // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
            else
            {
                if (context.ResolutionOrder.UseControlInfo)
                {
                    if (this.HasRowConnectorAppearance)
                        this.RowConnectorAppearance.MergeData(ref appData, ref context.UnresolvedProps);

                    if (this.Column.HasGroupByRowConnectorAppearance)
                        this.Column.GroupByRowConnectorAppearance.MergeData(ref appData, ref context.UnresolvedProps);
                }
            }

            // MBS 5/21/09 - TFS17863
            AppearancePropFlags strippedProps = 0;

			// merge in the row's Appearance values (if any)
			//
			// SSP 3/14/06 - App Styling
			// 
			//if ( this.HasAppearance )
			if ( this.HasAppearance && context.ResolutionOrder.UseControlInfo )
			{
                // MBS 5/21/09 - TFS17863
                // We shouldn't pick up the ImageBackground from the GroupByRow's appearance, only when
                // it's directly set on a connector-specific appearance
                strippedProps = context.UnresolvedProps & AppearancePropFlags.AllImageBackgroundProps;
                context.UnresolvedProps &= ~strippedProps;

				// JJD 12/12/02 - Optimization
				// Call the Appearance object's MergeData method instead so
				// we don't make unnecessary copies of the data structure
				//AppearanceData.MergeAppearance( ref appData, 
				//	this.Appearance.Data,
				//	ref context.UnresolvedProps );
				this.Appearance.MergeData( ref appData, ref context.UnresolvedProps );

                // MBS 5/21/09 - TFS17863
                context.UnresolvedProps |= strippedProps;

				if ( 0 == context.UnresolvedProps )
					return;
			}

            // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
            if (isGroupByConnector)
            {
                band.MergeBLOverrideAppearances(ref appData, ref context,
                    UltraGridOverride.OverrideAppearanceIndex.GroupByRowConnectorAppearance, AppStyling.RoleState.Normal);

                // MBS 5/21/09 - TFS17863
                // We shouldn't pick up the ImageBackground from the GroupByRow's appearance, only when
                // it's directly set on a connector-specific appearance.  Since we've already resolved
                // any connector-specific appearances at this point, we'll just permanently strip
                // out these properties
                strippedProps = context.UnresolvedProps & AppearancePropFlags.AllImageBackgroundProps;
                context.UnresolvedProps &= ~strippedProps;
            }

            // MBS 5/11/09 - NA9.2 GroupByRowConnector Appearance
            // Resolve the appearance that has been added to the column now.  This is resolved for
            // both the row and the connector, since this is how we behaved previously
            if (context.ResolutionOrder.UseControlInfo && this.Column.HasGroupByRowAppearance)            
                this.Column.GroupByRowAppearance.MergeData(ref appData, ref context.UnresolvedProps);            

			// SSP 10/3/01 UWG430
			// Moved this down
			//
			//this.Band.ResolveAppearance (ref appData, ref context );

			// SSP 3/14/06 - App Styling / Optimization
			// 
			// ------------------------------------------------------------------------------------------
			band.MergeBLOverrideAppearances( ref appData, ref context, 
				UltraGridOverride.OverrideAppearanceIndex.GroupByRow, AppStyling.RoleState.Normal );
			
			// ------------------------------------------------------------------------------------------
		
			// SSP 10/3/01 UWG430
			// Moved this down here from above
			//
			// SSP 10/3/01 UWG693
			// This should be done after the default colors are resolved.
			//
			//this.Band.ResolveAppearance (ref appData, ref context );  		

			// finally call ResolveColors to make sure we have a valid
			// fore and back color
			//
			layout.ResolveColors( ref appData, ref context, 
				layout.Grid.BackColor,
				// SSP 11/14/01 UWG693 
				// Use ControlLightLight as the preferred border color
				//
				//SystemColors.WindowFrame, 
				SystemColors.ControlLightLight,
				SystemColors.WindowText, false );


			// SSP 10/3/01 UWG693
			// This should be done after the default colors are resolved.
			//
			band.ResolveAppearance( ref appData, ref context );
		}

		internal void ResolveGroupByConnectorAppearance( 
			ref AppearanceData appData,
			ref AppearancePropFlags requestedProps )
		{
			this.ResolveHelper( ref appData, ref requestedProps, true );
		}

		/// <summary>
		/// Overridden. Resolves the appearance for this GroupByRow.
		/// </summary>
		/// <remarks>
		/// <p class="body">Use this method to determine the actual values that are being used to format the GroupByRow. This method initializes the specified AppearanceData structure with the actual values that will be used to display the object. You can combine the bit flags for this method to specify which properties should be resolved.</p>
		/// </remarks>
        /// <param name="appData">The structure to contain the resolved apperance.</param>
        /// <param name="requestedProps">Bit flags indictaing which properties to resolve.</param>
        public override void ResolveAppearance(ref AppearanceData appData, AppearancePropFlags requestedProps)
		{
			this.ResolveHelper( ref appData, ref requestedProps, false );
		}

        internal void ResolveAppearance( ref AppearanceData appData, ref AppearancePropFlags requestedProps )
		{
			this.ResolveHelper( ref appData, ref requestedProps, false );
		}

		internal override ISelectionStrategy SelectionStrategyDefault
		{
			get
			{
				return ((ISelectionStrategyProvider)(this.Band)).SelectionStrategyGroupByRow;
			}
		}

		// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries Functionality
		// Changed the hierarchy to derive from UltraGridSpecialRowBase instead of the UltraGridRow.
		// UltraGridSpecialRowBase implements the following method in the same way.
		//
		

		// SSP 5/3/05 - NAS 5.2 Special Row Separators
		// Added the new RowSpacingBeforeBaseResolved and RowSpacingAfterBaseResolved properties.
		// Override those instead of the RowSpacingBeforeResolved and RowSpacingAfterResolved.
		// 
		//public override int RowSpacingBeforeResolved
		internal override int RowSpacingBeforeResolvedBase
		{
			get 
			{
				// SSP 12/21/04 BR01386
				// We weren't honoring RowSpacingBefore and RowSpacingAfter on group-by rows.
				// This is to fix that. Also added GroupByRowSpacingBefore and 
				// GroupByRowSpacingAfter on the override.
				//
				//return 0;
				int ret = this.RowSpacingBefore;
				return ret >= 0 ? ret : this.BandInternal.GroupByRowSpacingBeforeDefault;
			}
		}

		// SSP 5/3/05 - NAS 5.2 Special Row Separators
		// Added the new RowSpacingBeforeBaseResolved and RowSpacingAfterBaseResolved properties.
		// Override those instead of the RowSpacingBeforeResolved and RowSpacingAfterResolved.
		// 
		//public override int RowSpacingAfterResolved
		internal override int RowSpacingAfterResolvedBase
		{
			get 
			{
				// SSP 12/21/04 BR01386
				// We weren't honoring RowSpacingBefore and RowSpacingAfter on group-by rows.
				// This is to fix that. Also added GroupByRowSpacingBefore and 
				// GroupByRowSpacingAfter on the override.
				//
				//return 0;
				int ret = this.RowSpacingAfter;
				return ret >= 0 ? ret : this.BandInternal.GroupByRowSpacingAfterDefault;
			}                                                
		}

		/// <summary>
		/// Returns the height of the AutoPreview area for the row in pixels.
		/// </summary>
		/// <remarks>
		/// <p class="body">The AutoPreview area appears under a row and provides a way to display multiple lines of text associated with that row. You can specify how many lines of text should be displayed, and choose to either display the value from a cell in the row or a custom text string that you specify. One common use might be to display the contents of a memo field that initially appears off-screen when the grid is loaded.</p>
		/// </remarks>
		public override int AutoPreviewHeight 
		{
			get
			{
				// no auto previews for group by rows
				return 0;
			}
		}


		// SSP 3/1/02 
		// Overrode the Height property to allow the user to be able to
		// set the meninum height
		//
		/// <summary>
		/// Allows you to specify the minimum height of the group by row.
		/// It only effects the group by row you set this property on.
		/// </summary>
		/// <remarks>
		/// If a group by row is desired to have bigger height than the default
		/// calcualted minimum height, you can set this property to the 
		/// height desired. A good place to set this property would be
		/// in <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeGroupByRow"/>
		/// event.
		/// </remarks>
		public override int Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				if ( value != this.minGroupByRowHeight )
				{
					this.minGroupByRowHeight = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.Height );
				}
			}
		}


		// SSP 4/1/02 UWG1060
		// Overrode this method.
		//
		internal override int CalculateAutoHeight( )
		{
			return 0;
		}


		/// <summary>
		/// Resets the Height to it's default value so that it uses the
		/// default calcuated group by row height.
		/// </summary>
		public void ResetHeight( )
		{
			this.Height = 0;
		}

        // SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
		// Added GetDescriptionSize method. Code in there is moved from the MinRowHeight property.
		//

		/// <summary>
		/// Calculates the size of the scription. If calcWidth is false then it does not calculate
		/// the width, only the height. This is for efficiency reasons.
		/// </summary>
		/// <param name="calcWidth"></param>
		/// <returns></returns>
        // MRS - NAS 9.2 - Inline Summaries in GroupByRows for DocumentExporter
        // Made this public and marked it EditorBrowsableState.Advanced so that it can be used by         
        // the DocumentExporter. 
		//internal Size GetDescriptionSize( bool calcWidth )
        [EditorBrowsable(EditorBrowsableState.Advanced)]
        public Size GetDescriptionSize(bool calcWidth)
		//internal Size GetDescriptionSize( bool calcWidth )
		{
			UltraGridBand band = this.BandInternal;
			UltraGridLayout layout = band.Layout;

			AppearanceData appData = new AppearanceData();
			// SSP 2/2/07 BR19790
			// Resolve the image as well as we can take it into account when calculating the width.
			// 
			// ------------------------------------------------------------------------------------
			//this.ResolveAppearance( ref appData, Infragistics.Win.AppearancePropFlags.FontData );
			AppearancePropFlags flags = AppearancePropFlags.FontData;
			if ( calcWidth )
				flags |= AppearancePropFlags.Image | AppearancePropFlags.ImageHAlign;

			this.ResolveAppearance( ref appData, flags );
			// ------------------------------------------------------------------------------------
						
			// get the base font' height
			//
			int lineHeight = layout.Grid.Font.Height;
						
			// if we are overriding the size then adjust the 
			// height based on the oveeride size
			//
			if ( appData.HasFontData  &&
				appData.FontData.SizeInPoints > 0.0f )
			{
				lineHeight = (int)((float)lineHeight * appData.FontData.SizeInPoints / layout.Grid.Font.SizeInPoints);
			}

			// SSP 1/23/02 UWG982 UWG983
			// Also take into account various appearance settings to calculate
			// the minimum height of the row by calling newly created 
			// CalcMinGroupByRowHeight method off the band.
			//
			lineHeight = System.Math.Max( lineHeight, band.CalcMinGroupByRowHeight() );

			Size retSize = new Size( 0, lineHeight );

			if ( calcWidth )
			{
				Size tmpSize = layout.CalculateFontSize( ref appData, this.DescriptionWithSummaries );
				retSize.Width = tmpSize.Width;

				// SSP 2/2/07 BR19790
				// Take any image into account when calculating the width.
				// 
				if ( null != appData.Image )
				{
					Size imageSize = GroupByRowDescriptionUIElement.CalcImageSize( this, retSize.Height );
					retSize.Width += imageSize.Width + 2;
				}
			}

			return retSize;
		}

		internal override int MinRowHeight
		{
			get
			{	
				UltraGridBand band = this.BandInternal;

				// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
				// Added GetDescriptionSize method. Code in there is moved from here.
				//
				// --------------------------------------------------------------------------
				

				Size descriptionSize = this.GetDescriptionSize( false );
				int lineHeight = descriptionSize.Height;

				bool summariesWrapped;
				Rectangle expansionIndicatorRect, descriptionRect, summariesRect;
				Rectangle dummyRect = new Rectangle( 0, 0, int.MaxValue, int.MaxValue );
                // MRS 4/9/2008 - BR31806
                //GroupByRowUIElement.PositionChildElementsHelper( this, dummyRect,
                GroupByRowUIElement.PositionChildElementsHelper(this, dummyRect, null,
					out expansionIndicatorRect, out descriptionRect, out summariesRect, out summariesWrapped );

				if ( ! summariesRect.IsEmpty )
					lineHeight = summariesRect.Bottom - descriptionRect.Top;
				// --------------------------------------------------------------------------
	
				// SSP 4/25/05 - NAS 5.2 Extension of Summaries Functionality
				// GroupByRowBorderStyleResolved always returned Raised even though 
				// this.BorderStyleResolved always returns Solid. This caused the group-by 
				// row height to be taller than necessary.
				// 
				// --------------------------------------------------------------------------
				int borderWidth = band.Layout.GetBorderThickness( this.BorderStyleResolved );

				// This whole thing about the slack is to maintain the same exact group-by 
				// row heights as 5.1 and older versions. 5.1 and older versions miscalculated
				// the height to be always 3 pixels higher than necessary (essentially it was 
				// adding 2 * BorderWidthOfRaised = 4 even though the group-by row element 
				// always uses Solid border style and only Top as the BorderSides which amounted 
				// to 1 pixel and thus it was always 3 pixels higher). Following code emulates
				// that 3 pixel extra slack, however only if summaries do not cause the height
				// to be bigger to begin with. If this causes any problems, take it out or 
				// change it however keep in mind about the maintaing the same behavior as 5.1
				// (although it's not really that important as it's only heights).
				// 
				int newCodeSlack = Math.Max( 0, lineHeight - descriptionSize.Height );
				int oldCodeSlack = 2 * band.Layout.GetBorderThickness( UIElementBorderStyle.Raised ) - borderWidth;
				int slack = Math.Max( 0, oldCodeSlack - newCodeSlack );

				return Math.Max( this.minGroupByRowHeight, Math.Max( GroupByRowUIElement.EXPANSION_INDICATOR_WIDTH,
					slack + borderWidth + 2 * band.GroupByRowPaddingResolved + lineHeight ) );
				
				// --------------------------------------------------------------------------
			}
		}


		internal override int BaseHeight
		{
			get
			{
				return this.MinRowHeight;
			}
		}


		// SSP 3/15/05 - NAS 5.2 Filter Row Functionality
		// Changed the modifiers from internal to internal override because made the
		// BorderStyleResolved property on the base class virtual.
		// 
		//internal UIElementBorderStyle BorderStyleResolved
		internal override UIElementBorderStyle BorderStyleResolved
		{
			get
			{
				// SSP 11/14/01 UWG693
				// Use Solid instead of RaisedSoft so that it draws the border
				// using AppearanceData.BorderColor
				//
				//return UIElementBorderStyle.RaisedSoft;
				return UIElementBorderStyle.Solid;
			}
		}

		
		// SSP 4/24/05 - NAS 5.2 Extension of Summaries Functionality
		// Commented following property out as the base implementation does the same thing.
		// 
		

		/// <summary>
		/// Overridden. <b>Update</b> does not do anything in this context because this is a GroupByRow with no associated list object in the binding list.
		/// </summary>
		/// <returns></returns>
		public override bool Update()
		{
			return true;
		}



		// SSP 1/3/02 UWG897
		// Overrode this method for group by row to search for GroupByRowUIElement
		// return that.
		//
		/// <summary>
		/// Returns the UIElement object that is associated with an object.
		/// </summary>
        /// <param name="csr">The ColScrollRegion</param>
        /// <param name="rsr">The RowScrollRegion</param>
        /// <param name="verifyElements">Indicates whether to VerifyChildElements</param>
		/// <remarks>
		/// <p class="body">Invoke this method to return a reference to an object's UIElement. The reference can be used to set properties of, and invoke methods on, the UIElement object associated with an object. You can use this reference to access any of the UIElement's properties or methods.</p>
		/// <p class="body">The <b>Type</b> property can be used to determine what type of UIElement was returned. If no UIElement exists, meaning the object is not displayed, Nothing is returned.</p>
		/// <p class="body">The <b>ParentUIElement</b> property can be used to return a reference to a UIElement's parent UIElement object. The <b>UIElements</b> property can be used to return a reference to a collection of child UIElement objects for a UIElement.</p>
		/// <p class="body">The <b>UIElementFromPoint</b> method can be invoked to return a reference to an UIElement object residing at specific coordinates.</p>
		/// <p class="body"><b>CanResolveUIElement</b> method can be invoked to determine whether an object or one of its ancestors can be resolved as a specific type of UIElement.</p>
		/// <p class="body">The <b>GetUIElement</b> method does not take into account the presence of a pop-up edit window or the drop-down portion of a combo if these elements are present, and will never return a UIElement that corresponds to one of these elements. The <b>GetUIElementPopup</b> method can be invoked to return a reference to a popup window's UIElement.</p>
		/// </remarks>
        /// <returns>The UIElement associated with the object, in the specified row and column scrolling regions.</returns>
		public override UIElement GetUIElement( RowScrollRegion rsr, ColScrollRegion csr, bool verifyElements )
		{
			if ( null == rsr )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_146"));

			if ( null == csr )
				throw new ArgumentNullException(Shared.SR.GetString("LE_ArgumentNullException_41"));

			// get the grid's main element
			//
			UIElement element = this.Band.Layout.GetUIElement( verifyElements );

			if ( null != element )
			{
				element = element.GetDescendant( typeof( GroupByRowUIElement ), new object[] { this, rsr, csr } );
			}

			return element;
		}


		// SSP 10/11/01 UWG366
		internal void ClickRight( )
		{
			UltraGrid gridCtrl = this.Layout.Grid as UltraGrid;

			// SSP 4/2/04 UWG3128
			// Only select the row if the selection type is not None.
			//
			//if ( null != gridCtrl )
			// SSP 4/20/05 - NAS 5.2 Filter Row
			// Use the Selectable property instead.
			//
			//if ( null != gridCtrl && this.CanSelectRow )
			if ( null != gridCtrl && this.Selectable
				// SSP 10/2/07 BR25930
				// If the group-by row is already selected, even with other selected group-by rows,
				// take no actions.
				// 
				&& ! this.Selected )
			{
				//gridCtrl.ClearAllSelected( );
				//gridCtrl.Selected.Rows.InternalAdd( this ); 
				//this.Selected = true;
				gridCtrl.InternalSelectItem( this, true, true );
				gridCtrl.ActiveRow = this;
			}
		}
		
		#region FilterCode

		#region EnsureFiltersEvaluated

		// SSP 10/27/04 - UltraCalc
		// Moved the filter verification logic into the row collection. This is so
		// that we can evaluate filters at once on all the rows of a row collection
		// and fire rows synced event to the calc manager instead of having to fire
		// row deleted/added event for every row whose hidden state changes.
		//
		

		#endregion // EnsureFiltersEvaluated

		#region HasAnyVisibleChildRows

		internal bool HasAnyVisibleChildRows( )
		{
			RowsCollection rows = this.Rows;
			if ( null == rows || rows.Count <= 0 )
				return false;

			// SSP 10/9/06 BR15434
			//
			// ------------------------------------------------------------------------
			return rows.HasAnyVisibleRows( );

			
			// ------------------------------------------------------------------------
		}

		#endregion // HasAnyVisibleChildRows

		#region Hidden

		// SSP 3/27/02
		// Overrode Hidden property for filter feature. We have to hide the group by
		// row if all the rows belonging to the group by row are hidden.
		//
		/// <summary>
		/// Determines whether the object will be displayed. This property is not available at design-time.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>Hidden</b> property determines whether an object is visible. Hiding an object may have have effects that go beyond simply removing it from view. For example, hiding a band also hides all the rows in that band. Also, changing the <b>Hidden</b> property of an object affects all instances of that object. For example, a hidden column or row is hidden in all scrolling regions.</p>
		/// </remarks>
		public override bool Hidden
		{
			get
			{
				if ( base.Hidden )
					return true;

				// Hide the group by row if all the rows belonging to it get
				// filtered out.
				//
				// MD 7/26/07 - 7.3 Performance
				// HideGroupByRowsWithNoVisibleChildren was changed and always returns True now
				//if ( this.Band.HideGroupByRowsWithNoVisibleChildren &&
				//    !this.HasAnyVisibleChildRows( ) )
				if ( !this.HasAnyVisibleChildRows() )
				{
//					// SSP 9/2/03 UWG2330
//					//
//					// --------------------------------------------------------------------------
//					if ( this.IsFilterDummyRow )
//						return false;
//					// --------------------------------------------------------------------------

					return true;
				}

				return false;
			}
		}

		#endregion // Hidden

		

		#region HiddenResolved

		// SSP 5/12/05 - Optimization
		// Commented out the following code. Base implementation of HiddenResolved is sufficient.
		//
		

		#endregion // HiddenResolved

		#region ApplyFilters

		// SSP 5/24/05 BR04243
		// Made the method virtual and overrode it in group-by row.
		// 
		internal override void ApplyFilters( ColumnFiltersCollection columnFilters )
		{
			// Call EnsureFiltersEvaluated on the child rows collection. It will set the parent
			// group-by row's filtered out state.
			// 
			RowsCollection rows = this.Rows;
			if ( null != rows )
				rows.EnsureFiltersEvaluated( );
		}

		#endregion // ApplyFilters

		#endregion //FilterCode


		// SSP 4/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
		//
		#region NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		#region IsUIElementCompatible
		
		internal override bool IsUIElementCompatible( UIElement parent, UIElement elem )
		{
			return elem is GroupByRowUIElement;
		}

		#endregion // IsUIElementCompatible

		#region HasSameContext

		internal override bool HasSameContext( UIElement parent, UIElement elem )
		{
			GroupByRowUIElement groupByRowElem = elem as GroupByRowUIElement;
			return null != groupByRowElem && this == groupByRowElem.GroupByRow;
		}

		#endregion // HasSameContext

		#region CreateRowUIElement
		
		internal override UIElement CreateRowUIElement( UIElement parent )
		{
			return new GroupByRowUIElement( parent, this );
		}

		#endregion // CreateRowUIElement

		#region InitializeUIElement

		internal override void InitializeUIElement( UIElement elem )
		{
			GroupByRowUIElement groupByRowElem = (GroupByRowUIElement)elem;
			groupByRowElem.InitializeRow( this );
		}

		#endregion // InitializeUIElement

		#region GetElementLastClipRect

		internal override Rectangle GetElementLastClipRect( UIElement elem )
		{
			GroupByRowUIElement groupByRowElem = elem as GroupByRowUIElement;
			if ( null != groupByRowElem )
				return groupByRowElem.lastClipRect;

			return Rectangle.Empty;
		}

		#endregion // GetElementLastClipRect

		#region SetElementLastClipRect

		internal override void SetElementLastClipRect( UIElement elem, Rectangle clipRect )
		{
			GroupByRowUIElement groupByRowElem = elem as GroupByRowUIElement;
			if ( null != groupByRowElem )
				groupByRowElem.lastClipRect = clipRect;
		}

		#endregion // SetElementLastClipRect

		#region RowScrollTip

		// SSP 5/18/05 - NAS 5.2 Filter Row/Extension of Summaries
		// Added RowScrollTip property.
		// 
		internal override string RowScrollTip
		{
			get
			{
				return this.Description;
			}
		}

		#endregion // RowScrollTip

		#endregion // NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention

		// AS 1/26/06 BR09335
		// Added this so we could determine if we can access
		// the cells collection instead of having to check
		// if this is a group by row.
		//
		#region SupportsCells

		internal override bool SupportsCells
		{
			get { return false; }
		}
		#endregion //SupportsCells

		#region UseRowLayoutResolved

		// SSP 6/8/06 BR13501
		// 
		internal override bool UseRowLayoutResolved
		{
			get
			{
				return false;
			}
		}

		#endregion // UseRowLayoutResolved

        // MBS 5/8/09 - NA9.2 GroupByRowConnector Appearance
        #region RowConnectorAppearance

        /// <summary>
        /// Determines the appearance of the area that exists to the left of the child rows of a GroupByRow.
        /// </summary>
        [LocalizedDescription("LD_UltraGridOverride_P_GroupByRowConnectorAppearance")]
        [LocalizedCategory("LC_Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Infragistics.Win.AppearanceBase RowConnectorAppearance
        {
            get
            {
                // if we don't already have an appearance object then create it now
                //
                if (null == this.rowConnectorAppearanceHolder)
                {
                    this.rowConnectorAppearanceHolder = new Infragistics.Win.AppearanceHolder();

                    // hook up the prop change notifications
                    //
                    this.rowConnectorAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                }

                // Initialize the collection
                //
                if (this.Layout != null)
                    this.rowConnectorAppearanceHolder.Collection = this.Layout.Appearances;

                return this.rowConnectorAppearanceHolder.Appearance;
            }
            set
            {
                if (this.rowConnectorAppearanceHolder == null ||
                    !this.rowConnectorAppearanceHolder.HasAppearance ||
                    value != this.rowConnectorAppearanceHolder.Appearance)
                {
                    if (null == this.rowConnectorAppearanceHolder)
                    {
                        this.rowConnectorAppearanceHolder = new Infragistics.Win.AppearanceHolder();

                        // hook up the new prop change notifications
                        //
                        this.rowConnectorAppearanceHolder.SubObjectPropChanged += this.SubObjectPropChangeHandler;
                    }

                    if (this.Layout != null)
                        this.rowConnectorAppearanceHolder.Collection = this.Layout.Appearances;

                    this.rowConnectorAppearanceHolder.Appearance = value;

                    // notify listeners
                    //
                    this.NotifyPropChange(PropertyIds.GroupByRowConnectorAppearance);
                }
            }
        }

        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal bool HasRowConnectorAppearance
        {
            get
            {
                return null != this.rowConnectorAppearanceHolder
                    && this.rowConnectorAppearanceHolder.HasAppearance;
            }

        }

        /// <summary>
        /// Returns true if the property is set to a non-default value.
        /// </summary>
        /// <returns>Returns true if this property is not set to its default value</returns>
        private bool ShouldSerializeRowConnectorAppearance()
        {
            return this.HasRowConnectorAppearance
                && this.rowConnectorAppearanceHolder.ShouldSerialize();
        }

        /// <summary>
        /// Resets the <see cref="RowConnectorAppearance"/> property to its default value.
        /// </summary>
        public void ResetRowConnectorAppearance()
        {
            if (this.HasRowConnectorAppearance)
            {
                this.rowConnectorAppearanceHolder.Reset();

                // Notify listeners that the appearance has changed. 
                this.NotifyPropChange(PropertyIds.GroupByRowConnectorAppearance);
            }
        }
        #endregion 
        //
        #region OnSubObjectPropChanged

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
        protected override void OnSubObjectPropChanged(PropChangeInfo propChange)
        {
            if (this.HasRowConnectorAppearance &&
                propChange.Source == this.rowConnectorAppearanceHolder.RootAppearance)
            {
                this.Layout.DirtyDataAreaElement(false, true, true);
                this.NotifyPropChange(Infragistics.Win.UltraWinGrid.PropertyIds.GroupByRowConnectorAppearance, propChange);
                return;
            }
            base.OnSubObjectPropChanged(propChange);
        }
        #endregion //OnSubObjectPropChanged
    }
}
