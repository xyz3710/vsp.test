#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;

namespace Infragistics.Win.UltraWinGrid
{
	#region Activation

	/// <summary>
	/// Determines how the cell will behave when it is activated.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// This enum is used for specifying UltraGridColumn's <see cref="UltraGridColumn.CellActivation"/>,
	/// UltraGridRow's <see cref="UltraGridRow.Activation"/> and 
	/// UltraGridCell's <see cref="UltraGridCell.Activation"/> properties.
	/// </p>
	/// <seealso cref="UltraGridColumn.CellActivation"/>
	/// <seealso cref="UltraGridRow.Activation"/>
	/// <seealso cref="UltraGridCell.Activation"/>
	/// <seealso cref="UltraGridOverride.CellClickAction"/>
	/// </remarks>
	public enum Activation
	{
		/// <summary>
		/// Allow Edit. The grid will attempt to edit the content of the object.
		/// </summary>
		AllowEdit = 0,

		/// <summary>
        /// Activate Only. The object may be selected (to copy and paste) but may not be edited.
		/// </summary>
		ActivateOnly = 1,

		/// <summary>
		/// Disabled. The object may not be activated and text may not be selected or edited.
		/// </summary>
		Disabled = 2,

		/// <summary>
        /// No Edit. The object may be activated(text highlighted), but cells cannot be edited or selected(to copy and paste).
		/// </summary>
		NoEdit = 3
	};

	#endregion // Activation

	#region AddNewBoxStyle

	/// <summary>
	/// Constants that specify whether the AddNewBox will appear at full size or compacted.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// This enum is used for specifying the <see cref="AddNewBox.Style"/> property of the 
	/// <see cref="AddNewBox"/> object.
	/// </p>
	/// <seealso cref="AddNewBox"/>
	/// <seealso cref="AddNewBox.Style"/>
	/// <seealso cref="UltraGridLayout.AddNewBox"/>
	/// </remarks>
	public enum AddNewBoxStyle
	{
		/// <summary>
		/// Full. The AddNewBox will appear at its full size.
		/// </summary>
		Full = 0,

		/// <summary>
		/// Compact. The AddNewBox will appear compacted, occupying the least amount of screen real estate possible.
		/// </summary>
		Compact = 1
	};

	#endregion // AddNewBoxStyle

	#region AllowAddNew

	/// <summary>
	/// Enum used for specifying the Override's <see cref="UltraGridOverride.AllowAddNew"/> property.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>AllowAddNew</b> enum is used for specifying the 
	/// Override's <see cref="UltraGridOverride.AllowAddNew"/> property.
	/// </p>
	/// <seealso cref="UltraGridOverride.AllowAddNew"/>
	/// </remarks>
	public enum AllowAddNew
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Yes. New Rows can be added by the user.
		/// </summary>
		Yes = 1,

		/// <summary>
		/// No. New Rows cannot be added by the user.
		/// </summary>
		No = 2,

		/// <summary>
		/// New Rows can be added by the user, and pressing the TAB key from within the last cell on an AddNew row will automatically add a new row.
		/// </summary>
		TabRepeat = 3,

		// SSP 11/7/03 - Add Row Feature
		//
		/// <summary>
		/// Template add-row as the first row in rows-collections. <b>TemplateOnTop</b> option displays an add-row in every rows-collection as the first row in the row collection. The user can use this template add-row to add a new row by simply typing into it.
		/// </summary>
		TemplateOnTop	= 4,

		/// <summary>
		/// Same as <b>TemplateOnTop</b> except with <b>TemplateOnTopWithTabRepeat</b> tabbing off an add-row will tab into the template-add row associated with the rows-collection and thus providing a convenient way adding multiple rows via keyboard.
		/// </summary>
		TemplateOnTopWithTabRepeat	= 5,

		/// <summary>
		/// Template add-row as the last row in rows-collections. <b>TemplateOnBottom</b> option displays an add-row in every rows-collection as the last row in the row collection. The user can use this template add-row to add a new row by simply typing into it.
		/// </summary>
		TemplateOnBottom = 6,

		// SSP 3/9/05 - NAS 5.2 Outlook Style Fixed Add Rows
		// Added FixedAddRowOnTop and FixedAddRowOnBottom members.
		//
		/// <summary>
		/// Fixed template add-row as the first row in rows-collections. <b>FixedAddRowOnTop</b> option displays a fixed (non-scrolling) add-row in every rows-collection as the first row in the row collection. The user can use this template add-row to add a new row by simply typing into it.
		/// </summary>
		FixedAddRowOnTop = 7,

		/// <summary>
		/// Fixed template add-row as the last row in rows-collections. <b>FixedAddRowOnBottom</b> option displays a fixed (non-scrolling) add-row in every rows-collection as the last row in the row collection. The user can use this template add-row to add a new row by simply typing into it.
		/// </summary>
		FixedAddRowOnBottom = 8
	};

	#endregion // AllowAddNew

	#region AllowColMoving

	/// <summary>
	/// Used to specify whether columns can be moved.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// <b>AllowColMoving</b> enum is used for specifying the Override's <see cref="UltraGridOverride.AllowColMoving"/> property.
	/// </p>
	/// <seealso cref="UltraGridOverride.AllowColSizing"/>
	/// <seealso cref="UltraGridOverride.AllowColSwapping"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutColMoving"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSpanSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSpanSizing"/>
	/// </remarks>
	public enum AllowColMoving
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Not Allowed. Columns cannot be moved by the user.
		/// </summary>
		NotAllowed = 1,

		/// <summary>
		/// Within Group. Columns can be moved by the user within the same Group.
		/// </summary>
		WithinGroup = 2,

		/// <summary>
		/// Within Band. Columns can be moved by the user within the same Band.
		/// </summary>
		WithinBand = 3
	};

	#endregion // AllowColMoving

	#region AllowColSizing

	/// <summary>
	/// Used to specify whether columns can be resized.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// <b>AllowColSizing</b> enum is used for specifying the Override's <see cref="UltraGridOverride.AllowColSizing"/> property.
	/// </p>
	/// <seealso cref="UltraGridOverride.AllowColMoving"/>
	/// <seealso cref="UltraGridOverride.AllowColSwapping"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutColMoving"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSpanSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSpanSizing"/>
	/// </remarks>
	public enum AllowColSizing
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used.
		/// </summary>
		Default = 0,

		/// <summary>
		/// None. Columns cannot be sized by the user.
		/// </summary>
		None  = 1,

		/// <summary>
		/// Sync. Columns can be sized by the user. Columns in other Bands are sized as well.
		/// </summary>
		Synchronized = 2,

		/// <summary>
		/// Free. Columns can be sized by the user, with no effect on Columns in other Bands.
		/// </summary>
		Free = 3
	};

	#endregion // AllowColSizing

	#region AllowColSwapping

	/// <summary>
	/// Enumeration used for specifying whether to allow col swapping on columns.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// <b>AllowColSwapping</b> enum is used for specifying the Override's <see cref="UltraGridOverride.AllowColSwapping"/> property.
	/// </p>
	/// <seealso cref="UltraGridOverride.AllowGroupMoving"/>
	/// <seealso cref="UltraGridOverride.AllowGroupSwapping"/>
	/// <seealso cref="UltraGridOverride.AllowColMoving"/>
	/// <seealso cref="UltraGridOverride.AllowColSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutColMoving"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSpanSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSpanSizing"/>
	/// </remarks>
	public enum AllowColSwapping
	{
		/// <summary>
		/// default AllowColSwapping style
		/// </summary>
		Default = 0,

		/// <summary>
		/// disable col swapping
		/// </summary>
		NotAllowed = 1,

		/// <summary>
		/// allow col swapping within a group
		/// </summary>
		WithinGroup = 2,

		/// <summary>
		/// allow col swapping within a band
		/// </summary>
		WithinBand = 3
	};

	#endregion // AllowColSwapping

	#region AllowGroupMoving

	/// <summary>
	/// Used to specify whether groups can be moved.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// <b>AllowGroupMoving</b> enum is used for specifying the Override's <see cref="UltraGridOverride.AllowGroupMoving"/> property.
	/// </p>
	/// <seealso cref="UltraGridOverride.AllowGroupSwapping"/>
	/// <seealso cref="UltraGridOverride.AllowColMoving"/>
	/// <seealso cref="UltraGridOverride.AllowColSizing"/>
	/// <seealso cref="UltraGridOverride.AllowColSwapping"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutColMoving"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSpanSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSizing"/>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutLabelSpanSizing"/>
	/// </remarks>
	public enum AllowGroupMoving
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Not Allowed. Groups cannot be moved by the user.
		/// </summary>
		NotAllowed = 1,

		/// <summary>
		/// Within Band. Groups can be moved by the user within the same band.
		/// </summary>
		WithinBand = 2,

        // MRS 2/13/2009 - TFS13834
        /// <summary>
        /// Within Group. Groups can be moved by the user within the same Group. This option is only supported when <see cref="UltraGridBand.RowLayoutStyle"/> RowLayoutStyle is set to GroupLayout.
        /// </summary>
        WithinGroup = 3,
	};

	#endregion // AllowGroupMoving

	#region AllowGroupSwapping

	/// <summary>
	/// Enum for specifying whether groups are allowed to swap with other groups.
	/// </summary>
	public enum AllowGroupSwapping
	{
		/// <summary>
		/// default group swapping style
		/// </summary>
		Default = 0,

		/// <summary>
		/// do not allow swapping
		/// </summary>
		NotAllowed = 1,

		/// <summary>
		/// allow group swapping within bands
		/// </summary>
		WithinBand = 2
	};

	#endregion // AllowGroupSwapping

	#region AllowMultiCellOperation

	// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
	// 
	/// <summary>
	/// Enum used for specifying the <see cref="UltraGridOverride.AllowMultiCellOperations"/> property.
	/// </summary>
	[ Flags() ]
	public enum AllowMultiCellOperation
	{
		/// <summary>
		/// Default is resolved to <b>None</b>.
		/// </summary>
		Default		= 0x1,

		/// <summary>
		/// No multi cell operations are allowed.
		/// </summary>
		None		= 0x0,

		/// <summary>
		/// Allows copying of multiple cells to the clipboard.
		/// </summary>
		Copy		= 0x2,

		/// <summary>
		/// Same as <b>Copy</b> except that an extra row of column headers will be copied as well. 
		/// </summary>
		CopyWithHeaders = 0x4,

		/// <summary>
		/// Allows cutting of multiple cells to the clipboard. Cutting involves first copying
		/// of the cell values to the clipboard and then clearing the contents of the copied cells.
		/// </summary>
		Cut			= 0x8,

		/// <summary>
		/// Allows clearing contents of multiple cells. Cell contents are set to associated columns
		/// <see cref="UltraGridColumn.DefaultCellValue"/> property settings, which by default is
		/// <b>DBNull</b>.
		/// </summary>
		Delete		= 0x10,

		/// <summary>
		/// Allows pasting of multiple cells from the clipboard.
		/// </summary>
		Paste		= 0x20,

		/// <summary>
		/// Allows Undo operation. Last multi cell operation will be undone.
		/// </summary>
		Undo		= 0x40,

		/// <summary>
		/// Allows Redo operation.
		/// </summary>
		Redo		= 0x80,

		// NOTE: As new values are added, Reserved needs to be updated as well accordingly.
		// 

		// SSP 1/3/05 BR07358
		// This is for working around an MS issue that was introduced in Whidbey. The code that whidbey generates
		// when serializing out All is something along the lines of following:
		// 
		// Me.UltraGrid1.DisplayLayout.Override.SpecialRowSeparator = CType(((((((Infragistics.Win.UltraWinGrid.SpecialRowSeparator.Headers Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.FilterRow)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.TemplateAddRow)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.FixedRows)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.EmptyRows)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.2147483520
		// 
		// Above code causes a compiler error as SpecialRowSeparator.2147483520 is not a valid value.
		// The workaround seems to be that if we have a complement of all the values other than All
		// then their logic will write out that complement enum value instead of the decimal number.
		// 
		/// <summary>
		/// Reserved. For internal use only.
		/// </summary>
		Reserved = All & ~0xfe,

		/// <summary>
		/// Allows all of operations listed in this enum.
		/// </summary>
		All		= 0x7ffffffe
	}

	#endregion // AllowMultiCellOperation

	#region AllowRowSummaries

	/// <summary>
	/// Enum for specifying whether to allow row summaries at override level as well as individual column level.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// <b>AllowRowSummaries</b> enum is used for specifying the
	/// Override's <see cref="UltraGridOverride.AllowRowSummaries"/> and 
	/// Column's <see cref="UltraGridColumn.AllowRowSummaries"/> properties.
	/// </p>
	/// </remarks>
	public enum AllowRowSummaries
	{
		/// <summary>
		/// Default.
		/// </summary>
		Default			= 0,

		/// <summary>
		/// Allow row summaries.
		/// </summary>
		True			= 1,

		/// <summary>
		/// Do not allow row summaries.
		/// </summary>
		False			= 2,

		/// <summary>
		/// Base it on the column's data type.
		/// </summary>
		BasedOnDataType	= 3,

		// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
		// Added SingleSummary and SingleSummaryBasedOnDataType members.
		//
		/// <summary>
		/// Behaves like <b>True</b> however allows the user to select only a single summary per column.
		/// </summary>
		SingleSummary	= 4,

		// SSP 8/8/03 - Row Layout Functionality - Multiple Summaries
		//
		/// <summary>
		/// Behaves like <b>BasedOnDataType</b> however allows the user to select only a single summary per column.
		/// </summary>
		SingleSummaryBasedOnDataType	= 5
	}

	#endregion // AllowRowSummaries

	#region AutoFitStyle

	/// <summary>
	/// Enum for specifying <see cref="UltraGridLayout.AutoFitStyle"/> property.
	/// </summary>
	public enum AutoFitStyle
	{
		/// <summary>
		/// Auto resizing is disabled. This is the default value of the property.
		/// </summary>
		None = 0,

		/// <summary>
		/// Resizes all the columns to fit them in the available visible area. This
		/// value corresponds to the how the functionality previously worked before
		/// the deprecation of <b>UltraGridLayout.AutoFit</b> property.
		/// </summary>
		ResizeAllColumns = 1, 

		/// <summary>
		/// Extends the last column to occupy extra visible space. This setting does
		/// not have any effect in card-view functionality or in horizontal view style.
		/// When one or more of those features is enabled this setting behaves the same
		/// as <b>None</b> setting.
		/// </summary>
		ExtendLastColumn = 2 
	}

	#endregion // AutoFitStyle

	#region BandOrigin

	/// <summary>
	/// Enum for specifying what parts of the Band to include when calculating band origin.
	/// </summary>
	public enum BandOrigin
	{
		/// <summary>
		/// Pre-Row Area. The left edge of the pre-row area is used to determine the origin.
		/// </summary>
		PreRowArea = 0,

		/// <summary>
		/// Row Selectors. The left edge of the record selectors is used to determine the origin.
		/// </summary>
		RowSelector = 1,

		/// <summary>
		/// Cell Area. The left edge of the leftmost cell(s) is used to determine the origin.
		/// </summary>
		RowCellArea = 2
	};

	#endregion // BandOrigin

	#region ButtonDisplayStyle

	/// <summary>
	/// Enum to specify the style of displaying buttons in various column styles.
	/// </summary>
	public enum ButtonDisplayStyle
	{
		/// <summary>
		/// display button whenever the mouse enters the cell ui element
		/// </summary>		
		OnMouseEnter   = 0,

		/// <summary>
		/// always display buttons
		/// </summary>
		Always         = 1,

		/// <summary>
		/// display only when the cell is activated
		/// </summary>
		OnCellActivate = 2,

		/// <summary>
		/// display when the row the cell belongs to is activated
		/// </summary>
		OnRowActivate  = 3
	};

	#endregion // ButtonDisplayStyle

	#region Case

	/// <summary>
	/// Used to specify the case of text in a column.
	/// </summary>
	public enum Case
	{
		/// <summary>
		/// Unchanged. Text appears as it was entered.
		/// </summary>
		Unchanged = 0,

		/// <summary>
		/// Lower. All text appears as lowercase.
		/// </summary>
		Lower = 1,

		/// <summary>
		/// Upper. All text appears as uppercase.
		/// </summary>
		Upper = 2
	};

	#endregion // Case

	#region CardScrollbars

	// SSP 11/25/03 UWG1557 - Added a way to hide card-view scrollbars.
	//
	/// <summary>
	/// Used for specifying <see cref="UltraGridCardSettings.CardScrollbars"/> property.
	/// </summary>
	public enum CardScrollbars
	{
		/// <summary>
		/// <b>None</b> specifies that no scroll bars be shown.
		/// </summary>
		None		= 0,

		/// <summary>
		/// <b>Horizontal</b> specifies that the horizontal scroll bar be shown. If all the cards are visible then the scrollbar will be disabled.
		/// </summary>
		Horizontal	= 1
	}

	#endregion // CardScrollbars
    
	#region CardStyle

	/// <summary>
	/// Used to specify the Card Style.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridCardSettings.Style"/>
	/// </remarks>
	public enum CardStyle
	{
		/// <summary>
		/// Merged Labels. All card's within a display row only consist of cells, their 
		/// lables are merged into one separate UIElement.
		/// </summary>
		MergedLabels	= 0,

		/// <summary>
		/// Standard Labels.
		/// </summary>
		StandardLabels	= 1,

		/// <summary>
		/// Variable Height. Hides all empty cells within a card allowing cards to 
		/// vary in size.
		/// </summary>
		VariableHeight	= 2,

		// SSP 6/27/02
		// Compressed card view.
		//
		/// <summary>
		/// Compressed card view. Compresses all the cards showing only the card caption and giving the user the ability to expand cards.
		/// </summary>
		Compressed		= 3
	};

	#endregion // CardStyle

	#region CellClickAction

	/// <summary>
	/// Used to specify how a cell should react to being clicked.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// <b>CellClickAction</b> enum is used for specifying the Override's
	/// <see cref="UltraGridOverride.CellClickAction"/> and Column's
	/// <see cref="UltraGridColumn.CellClickAction"/> properties.
	/// </p>
	/// <seealso cref="UltraGridOverride.CellClickAction"/>
	/// </remarks>
	public enum CellClickAction
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used (Edit if parent has no default.)
		/// </summary>
		Default = 0,

		/// <summary>
		/// Edit. Selects and highlights the cell that was clicked and puts it in edit mode.
		/// </summary>
		Edit = 1,

		/// <summary>
		/// Row Select. Selects and highlights the entire row of the cell that was clicked. Note
		/// that if a cell is displaying a drop down button the clicking onto it will cause the
		/// cell to go into edit mode. To prevent the cell from going into edit mode see 
		/// <see cref="UltraGridColumn.CellActivation"/>. To prevent the drop down buttons from
		/// showing, set the <see cref="UltraGridColumn.ButtonDisplayStyle"/> property.
		/// </summary>
		RowSelect = 2,

		/// <summary>
		/// Cell Select. Selects and highlights the cell that was clicked. Note that if a cell 
		/// is displaying a drop down button the clicking onto it will cause the cell to go into 
		/// edit mode. To prevent the cell from going into edit mode see 
		/// <see cref="UltraGridColumn.CellActivation"/>. To prevent the drop down buttons from
		/// showing, set the <see cref="UltraGridColumn.ButtonDisplayStyle"/> property.
		/// </summary>
		CellSelect = 3,

		// JAS v5.2 Select Text On Cell Click 4/7/05
		//
		/// <summary>
		/// Edit and Select Text.  Puts the cell into edit mode and selects its text, if any.
		/// </summary>
		EditAndSelectText = 4
	};

	#endregion // CellClickAction

	#region CellDisplayStyle

	// SSP 5/12/03 - Optimizations
	// Added a way to just draw the text without having to embedd an embeddable ui element in
	// cells to speed up rendering.
	//
	/// <summary>
	/// Enum for specifying CellDisplayStyle property.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridOverride.CellDisplayStyle"/>
	/// <seealso cref="UltraGridColumn.CellDisplayStyle"/>
	/// <seealso cref="UltraGridCell.CellDisplayStyle"/>
	/// </remarks>
	public enum CellDisplayStyle
	{
		/// <summary>
		/// Default is resolved to <b>FullEditorDisplay</b>.
		/// </summary>
		Default				= 0,

		/// <summary>
		/// Plain text without any formatting.
		/// </summary>
		PlainText			= 1,

		/// <summary>
		/// Formatted text.
		/// </summary>
		FormattedText		= 2,

		/// <summary>
		/// Embeddable editor element.
		/// </summary>
		FullEditorDisplay	= 3
	}

	#endregion // CellDisplayStyel

	#region CellHottrackInvalidationStyle

	// SSP 8/28/06 - NAS 6.3
	// Added CellHottrackInvalidationStyle property on the layout.
	// 
	/// <summary>
	/// Enum for specifying <see cref="UltraGridLayout.CellHottrackInvalidationStyle"/> property.
	/// </summary>
	public enum CellHottrackInvalidationStyle
	{
		/// <summary>
		/// Default is to always invalidate the cell when the mouse enters or leaves the cell.
		/// </summary>
		Default,
		
		/// <summary>
		/// Always invalidate the cell when the mouse enters or leaves the cell.
		/// </summary>
		Always,

		/// <summary>
		/// Never invalidate the cell when the mouse enters or leaves the cell.
		/// </summary>
		Never,

		/// <summary>
		/// Only invalidate the cell when the mouse enters or leaves the cell if there are
		/// cell hottracking appearances.
		/// </summary>
		OnlyIfHottrackAppearance
	}

	#endregion // CellHottrackInvalidationStyle

	#region ChildRow

	/// <summary>
	/// Used to specify the relationship between the current row and its child rows.
	/// </summary>
	public enum ChildRow
	{
		/// <summary>
		/// First Child. Row is the first child of the current row.
		/// </summary>
		First = 0,

		/// <summary>
		/// Last Child. Row is the last child of the current row.
		/// </summary>
		Last = 1		
	};

	#endregion // ChildRow

	#region ClippingOverride

	/// <summary>
	/// Enum to specify if text should be clipped when sent to the printer.
	/// </summary>
	/// <remarks>
	/// <seealso cref="LogicalPageLayoutInfo.ClippingOverride"/>
	/// </remarks>
	public enum ClippingOverride
	{
		/// <summary>
		/// Clipping determine automatically
		/// </summary>
		Auto = 0,

		/// <summary>
		/// Clip text
		/// </summary>
		Yes	 = 1,

		/// <summary>
		/// Don't clip text
		/// </summary>
		No	 = 2

	};

	#endregion // ClippingOverride

	#region ColScrollAction

	/// <summary>
	/// Enum to specify how to scroll a column scroll region.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>ColScrollAction</b> enum is used for specifying what kind of scroll action
	/// should be performed when calling <see cref="ColScrollRegion.Scroll"/> method.
	/// </p>
	/// <seealso cref="ColScrollRegion.Scroll"/>
	/// </remarks>
	public enum ColScrollAction
	{
		/// <summary>
		/// scroll as if left arrow of the scroll bar was cliked
		/// </summary>
		LineLeft   = 0,

		/// <summary>
		/// scroll as if right arrow of the scroll bar was clicked
		/// </summary>
		LineRight  = 1,

		/// <summary>
		/// scroll one page to the left
		/// </summary>
		PageLeft   = 2,

		/// <summary>
		/// scroll one page to the right
		/// </summary>
		PageRight  = 3,

		/// <summary>
		/// scroll all the way to the left
		/// </summary>
		Left       = 6,

		/// <summary>
		/// scroll all the way to the right
		/// </summary>
		Right      = 7
	};

	#endregion // ColScrollAction

	#region ColumnAutoSizeMode

	// SSP 4/14/03
	// Added a way for the developer to control the auto sizing of the column.
	//
	/// <summary>
	/// Enum used for specifying how the column is autosized when the user double-clicks
	/// on the edge of a column.
	/// </summary>
	public enum ColumnAutoSizeMode
	{
		/// <summary>
		/// Default is to perform autosizing based on visible rows only.
		/// </summary>
		Default,

		/// <summary>
		/// Don't allow the user to perform autosizing on columns.
		/// </summary>
		None,

		/// <summary>
		/// Perform autosizing based on visible rows only.
		/// </summary>
		VisibleRows,

		/// <summary>
		/// Perform autosizing based on sibling rows (all rows in the rows collection associated with the row the header is visibly attached to).
		/// </summary>
		SiblingRowsOnly,

		/// <summary>
		/// Perform autosizing based on all the rows in band.
		/// </summary>
		AllRowsInBand,
	}

	#endregion // ColumnAutoSizeMode

	#region ColumnChooserStyle

	// SSP 6/17/05 - NAS 5.3 Column Chooser
	// 
	/// <summary>
	/// Enum for specifying the <see cref="UltraGridColumnChooser.Style"/> property.
	/// </summary>
	public enum ColumnChooserStyle
	{
		/// <summary>
		/// Only hidden columns are displayed. The user can double click a column to unhide 
		/// it however once it�s unhidden the only way to hide it by dragging it back over 
		/// the column chooser dialog.
		/// </summary>
		HiddenColumnsOnly,

		/// <summary>
		/// All columns, hidden and visible are displayed. Also a checkbox for each column is 
		/// displayed which can be checked or unchecked to unhide or hide the column. The user 
		/// can also toggle the hidden state of a column by double clicking on the column. 
		/// This option also allows a column to be dragged and dropped into the grid to unhide 
		/// it and into the column chooser to hide it.
		/// </summary>
		AllColumnsWithCheckBoxes,

		/// <summary>
		/// Same as <b>AllColumnsWithCheckBoxes</b> except that this option also lets the user
		/// hide child bands.
		/// </summary>
		AllColumnsAndChildBandsWithCheckBoxes
	}

	#endregion // ColumnChooserStyle

	#region ColumnDisplayOrder

	// SSP 8/28/06 BR14825
	// Added ColumnDisplayOrder on the UltraGridColumnChooser.
	// 
	/// <summary>
	/// Enum for specifying <see cref="UltraGridColumnChooser.ColumnDisplayOrder"/> property.
	/// </summary>
	public enum ColumnDisplayOrder
	{
		/// <summary>
		/// Columns are displayed in alphabetical order.
		/// </summary>
		Alphabetical,

		/// <summary>
		/// Columns are displayed in the same order as they appear in the UltraGrid. Hidden
		/// columns will assume the same order as in the UltraGrid if they were visible.
		/// </summary>
		SameAsGrid
	}

	#endregion // ColumnDisplayOrder

	#region ColumnSizingArea

	// SSP 11/16/04
	// Implemented column sizing using cells in non-rowlayout mode.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.ColumnSizingArea"/> property.
	/// </summary>
	public enum ColumnSizingArea
	{
		/// <summary>
		/// Default. Default is resolved to HeaderOnly.
		/// </summary>
		Default,

		/// <summary>
		/// Allow column sizing using column headers only.
		/// </summary>
		HeadersOnly,

		/// <summary>
		/// Allow column sizing using column cells only.
		/// </summary>
		CellsOnly,

		/// <summary>
		/// Allow column sizing using both cells and headers.
		/// </summary>
		EntireColumn
	}

	#endregion // ColumnSizingArea

	#region ColumnStyle

	/// <summary>
	/// Enum used for specifying the <see cref="UltraGridColumn.Style"/> property. 
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// This property is for convenience. It lets you set the edit style of a column to one of the 
	/// commonly used edit styles. This list does not include all the styles that are available. If a 
	/// style is not listed in this enum then you can use the <see cref="UltraGridColumn.Editor"/> 
	/// property along with other properties that the <see cref="UltraGridColumn"/> object exposes, 
	/// like <see cref="UltraGridColumn.MinValue"/>, <see cref="UltraGridColumn.MaxValue"/>, 
	/// <see cref="UltraGridColumn.MinValueExclusive"/>, <see cref="UltraGridColumn.MaxValueExclusive"/>, 
	/// <see cref="UltraGridColumn.MaskInput"/>, <see cref="UltraGridColumn.FormatInfo"/>, 
	/// <see cref="UltraGridColumn.Format"/>, <see cref="UltraGridColumn.MaxLength"/>, 
	/// <see cref="UltraGridColumn.RegexPattern"/> etc... to accomplish the desired column style.
	/// As a matter of fact some the styles set some of the these properties to accomplish the desired
	/// behavior. For example the <b>CurrencyPositive</b> style sets the <see cref="UltraGridColumn.Editor"/>
	/// to <see cref="Infragistics.Win.EditorWithMask"/> instance and the <see cref="UltraGridColumn.MinValueExclusive"/> 
	/// to 0. Also you can set these properties to further refine the behavior of a column style. This is 
	/// because these properties take precedence over the <b>Style</b> property.
	/// </p>
	/// <seealso cref="UltraGridColumn.Style"/>
	/// <seealso cref="UltraGridColumn.Editor"/>
	/// </remarks>
	public enum ColumnStyle
	{
		/// <summary>
		/// Use the default style. Default style is based on the column's data type and other column property settings.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Text box style.
		/// </summary>
		Edit = 1,

		/// <summary>
		/// Edit button style that has a text box as well as a button that users can click on.
		/// </summary>
		EditButton = 2,

		/// <summary>
		/// Checkbox style that is not a tri-state checkbox.
		/// </summary>
		CheckBox = 3,

		/// <summary>
		/// Tri-state checkbox style.
		/// </summary>
		TriStateCheckBox = 4,

		/// <summary>
		/// Drop down combobox style that has a text box and a small drop down 
		/// button that users can click on to display a drop down list to select
		/// items from. You must set the <see cref="UltraGridColumn.ValueList"/>
		/// property to a list of items you want to display in the drop down.
		/// Without that this style will not have any effect.
		/// </summary>
		DropDown = 5,

		/// <summary>
		/// Drop down list style that has a small drop down button that users 
		/// can click on to display a drop down list from which they can select
		/// an item. You must set the <see cref="UltraGridColumn.ValueList"/>
		/// property to a list of items you want to display in the drop down.
		/// Without that this style will not have any effect.
		/// </summary>
		DropDownList = 6,

		/// <summary>
		/// Same as DropDown style except that it will not allow the user to enter 
		/// text in the combobox' edit box that does not match any one of the values
		/// in the value list. You must set the <see cref="UltraGridColumn.ValueList"/>
		/// property to a list of items you want to display in the drop down.
		/// Without that this style will not have any effect.
		/// </summary>
		DropDownValidate = 7,

		/// <summary>
		/// Button style that has a button occupying the whole area of the cell.
		/// When the user clicks on the button ClickCellButton event is fired.
		/// </summary>
		Button = 8,

		/// <summary>
		/// Drop down calendar style that has a drop down arrow that users can
		/// click on to display a drop down calendar for selecting a date.
		/// </summary>
		DropDownCalendar = 9,

		// SSP 8/7/05 - NAS 5.3 New Column Styles
		// 
		// --------------------------------------------------------------------------------
		/// <summary>
		/// Style that allows entering dates. A drop down button is displayed that shows a drop down calendar. You can
		/// control the order of day, moth and year portions by setting the <see cref="UltraGridColumn.MaskInput"/>
		/// property. Minimum and maximum allowed values can be controlled using the <see cref="UltraGridColumn.MinValue"/> 
		/// and <see cref="UltraGridColumn.MaxValue"/> properties. <b>Note:</b> This style uses the masking functionality.
		/// </summary>
		Date = 10, 

		/// <summary>
		/// Same as <b>Date</b> style except the drop down button is not displayed.
		/// </summary>
		DateWithoutDropDown = 11,

		/// <summary>
		/// Same as <b>Date</b> style except the drop down button is not displayed and in its place spin buttons
		/// are displayed that can be used to increment or decrement each component of the date.
		/// </summary>
		DateWithSpin = 12, 

		/// <summary>
		/// Style that allows entering both date and time. Time uses short time pattern for the time porition. 
		/// Also a drop down button is displayed that shows a drop down calendar. You can further control the format
		/// of the date and time by setting the <see cref="UltraGridColumn.MaskInput"/> property.
		/// Minimum and maximum allowed values can be controlled using the <see cref="UltraGridColumn.MinValue"/> 
		/// and <see cref="UltraGridColumn.MaxValue"/> properties. <b>Note:</b> This style uses the masking functionality.
		/// </summary>
		DateTime = 13,
		
		/// <summary>
		/// Same as <b>DateTime</b> style except the drop down button is not displayed.
		/// </summary>
		DateTimeWithoutDropDown = 14,
	
		/// <summary>
		/// Same as <b>DateTime</b> style except the drop down button is not displayed and in its place spin buttons
		/// are displayed that can be used to increment or decrement each component of the date time.
		/// </summary>
		DateTimeWithSpin = 15,
		
		/// <summary>
		/// Style that allows entering Colors.
		/// </summary>
		Color = 16,

		/// <summary>
		/// Style that allows entering currency values. <b>Note:</b> This style uses the masking functionality. The 
		/// CurrentCulture is used to determine the number of digits and decimal places. You can control the number 
		/// of digits, decimal places etc... by setting the <see cref="UltraGridColumn.MaskInput"/> property. 
		/// <see cref="Infragistics.Win.EditorWithMask"/> is used.
		/// </summary>
		Currency = 17,

		/// <summary>
		/// Same as <b>Currency</b> except negative values aren't allowed.
		/// </summary>
		CurrencyNonNegative = 18,

		/// <summary>
		/// Same as <b>Currency</b> except zero and negative values aren't allowed.
		/// </summary>
		CurrencyPositive = 19,

		/// <summary>
		/// Style that allows entering double values. <b>Note:</b> This style uses the masking functionality. The 
		/// NumberFormatInfo of the CurrentCulture is used to determine the number of digits and decimal places. 
		/// You can control the number of digits, decimal places etc... by setting the <see cref="UltraGridColumn.MaskInput"/> 
		/// property. Minimum and maximum allowed values can be controlled using the <see cref="UltraGridColumn.MinValue"/> 
		/// and <see cref="UltraGridColumn.MaxValue"/> properties. <see cref="Infragistics.Win.EditorWithMask"/> is used.
		/// </summary>
		Double = 20,
		
		/// <summary>
		/// Same as <b>Double</b> except this style shows spin buttons.
		/// </summary>
		DoubleWithSpin = 21,

		/// <summary>
		/// Same as <b>Double</b> except the user is not allowed to enter negative numbers. <b>Note:</b> This style uses the masking functionality.
		/// </summary>
		DoubleNonNegative = 22,

		/// <summary>
		/// Same as <b>DoubleNonNegative</b> except this style shows spin buttons.
		/// </summary>
		DoubleNonNegativeWithSpin = 23,

		/// <summary>
		/// Same as <b>Double</b> except the user is not allowed to enter zero or negative numbers. <b>Note:</b> This style uses the masking functionality.
		/// </summary>
		DoublePositive = 24,

		/// <summary>
		/// Same as <b>DoublePositive</b> except this style shows spin buttons.
		/// </summary>
		DoublePositiveWithSpin = 25,

		/// <summary>
		/// Style that lets the user select a font. <see cref="Infragistics.Win.FontNameEditor"/> is used
		/// </summary>
		Font = 26,

		/// <summary>
		/// Style that uses the <see cref="Infragistics.Win.EmbeddableImageRenderer"/> to display images.
		/// </summary>
		Image = 27,

		/// <summary>
		/// Same as <b>Image</b> style except the images are displayed shadowed.
		/// </summary>
		ImageWithShadow = 28,

		/// <summary>
		/// Style that allows entering integer (whole numbers) values. You can control the number of digits by setting the
		/// <see cref="UltraGridColumn.MaskInput"/> property. <see cref="Infragistics.Win.EditorWithMask"/> is used.
		/// </summary>
		Integer = 29,

		/// <summary>
		/// Same as <b>Integer</b> except this style shows spin buttons.
		/// </summary>
		IntegerWithSpin = 30,
		
		/// <summary>
		/// Same as <b>Integer</b> except the user is not allowed to enter zero or negative numbers.
		/// </summary>
		IntegerPositive = 31,

		/// <summary>
		/// Same as <b>IntegerPositive</b> except this style shows spin buttons.
		/// </summary>
		IntegerPositiveWithSpin = 32,
		
		/// <summary>
		/// Same as <b>Integer</b> except the user is not allowed to enter negative numbers.
		/// </summary>
		IntegerNonNegative = 33,
	
		/// <summary>
		/// Same as <b>IntegerNonNegative</b> except this style shows spin buttons.
		/// </summary>
		IntegerNonNegativeWithSpin = 34,

		/// <summary>
		/// Style that allows entering time. This uses the short time pattern of the current culture. You can control
		/// various aspects of the time format by setting the <see cref="UltraGridColumn.MaskInput"/> property.
		/// Minimum and maximum allowed values can be controlled using the <see cref="UltraGridColumn.MinValue"/> 
		/// and <see cref="UltraGridColumn.MaxValue"/> properties. <b>Note:</b> This style uses the masking functionality.
		/// </summary>
		Time = 35,

		/// <summary>
		/// Same as <b>Time</b> style except spin buttons are displayed that can be used to increment or decrement
		/// each component of the time.
		/// </summary>
		TimeWithSpin = 36,

		/// <summary>
		/// Style that lets the user select time zone. <see cref="Infragistics.Win.TimeZoneEditor"/> is used.
		/// </summary>
		TimeZone = 37,
		// --------------------------------------------------------------------------------

		// MRS 1/11/06 - BR08740 NAS 2006 Vol. 1
		// Added two new styles to use the FormattedLinkRenderer
		//
		/// <summary>
		/// Style that displays the column data as a URL. <see cref="Infragistics.Win.FormattedLinkLabel.FormattedLinkEditor"/> is used.
		/// </summary>
		URL = 38,

		/// <summary>
		/// Style that displays the column data as formatted text. This does not allow editing. <see cref="Infragistics.Win.FormattedLinkLabel.FormattedLinkEditor"/> is used.
		/// </summary>
		FormattedText = 39,

		// SSP 9/7/06 - NAS 6.3
		// Added FormattedTextEditor style.
		// 
		/// <summary>
		/// Same as <b>FormattedText</b> except that this allows editing as well. <see cref="Infragistics.Win.FormattedLinkLabel.FormattedLinkEditor"/> is used.
		/// </summary>
        FormattedTextEditor = 40,

        // MRS 12/3/2007 - NAS v8.1
        /// <summary>
		/// Style which displays the column as a TrackBar.
		/// </summary>
        TrackBar = 41
	};

	#endregion // ColumnStyle

	#region CardCompressedState

	// JDN 11/17/04 Support for CardCompressedStateChanged events
	/// <summary>
	/// enum to specify different Card Compressed states for a Row in Compressed Card View
	/// </summary>
	public enum CardCompressedState
	{
		/// <summary>
		/// the card is expanded.
		/// </summary>
		Expanded = 0,

		/// <summary>
		/// the card is compressed
		/// </summary>
		Compressed = 1,

	}

	#endregion // CardCompressedState

	#region DataErrorSource

	/// <summary>
	/// Used to indicate how the Data error occurred.
	/// </summary>
	public enum DataErrorSource
	{
		/// <summary>
		/// Occurred while updating a cell
		/// </summary>
		CellUpdate	= 0,

		/// <summary>
		/// Occurred whil updating a row
		/// </summary>
		RowUpdate	= 1,

		/// <summary>
		/// Occurred while adding a new row
		/// </summary>
		RowAdd	= 2,

		/// <summary>
		/// Occurred while deleting a row
		/// </summary>
		RowDelete	= 3,

		/// <summary>
		/// Unspecified
		/// </summary>
		Unspecified		= 4

		//SourceProvider	= 4
	}

	#endregion // DataErrorSource

	#region EmptyRowStyle

	// SSP 7/6/05 - NAS 5.3 Empty Rows
	// 
	/// <summary>
	/// Enum for specifying <see cref="EmptyRowSettings.Style"/> property.
	/// </summary>
	/// <remarks>
	/// <seealso cref="EmptyRowSettings.Style"/>
	/// <seealso cref="EmptyRowSettings.ShowEmptyRows"/>
	/// </remarks>
	public enum EmptyRowStyle
	{
		/// <summary>
		/// Does not extend the empty rows. They are aligned with the data rows.
		/// </summary>
		AlignWithDataRows,

		/// <summary>
		/// Aligns the empty rows with the left edge of the UltraGrid (more precisely the 
		/// scroll region) by extending left the first visible cell of each row.
		/// </summary>
		ExtendFirstCell,

		/// <summary>
		/// Aligns the empty rows with the left edge of the UltraGrid (more precisely the 
		/// scroll region) by extending left the row selector of each row.
		/// </summary>
		ExtendRowSelector,

		/// <summary>
		/// Aligns the empty rows with the data rows however the row selector is hidden. 
		/// In its place the first cell is extended to occupy the space where the row 
		/// selector would have been. If row selectors are not enabled then this option 
		/// behaves the same as <b>AlignWithDataRows</b>.
		/// </summary>
		HideRowSelector,

		/// <summary>
		/// The area before the first cell is contains an empty cell. This option behaves 
		/// the same as <b>ExtendFirstCell</b> except instead of extending the first
		/// cell to occupy the space between the left edge of the grid and the row, another
		/// cell is displayed.
		/// </summary>
		PrefixWithEmptyCell
	}

	#endregion // EmptyRowStyle

	#region ErrorType

	/// <summary>
	/// Used for specifying what type of error.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>ErrorType</b> enum is used by the <see cref="ErrorEventArgs.ErrorType"/>
	/// property of the <see cref="ErrorEventArgs"/> object.
	/// </p>
	/// <seealso cref="ErrorEventArgs.ErrorType"/>
	/// </remarks>
	public enum ErrorType
	{
		/// <summary>
		/// Data error
		/// </summary>
		Data	= 1,

		/// <summary>
		/// Mask error
		/// </summary>
		Mask	= 2,

		/// <summary>
		/// Generic error
		/// </summary>
		Generic	= 3,

		// SSP 11/23/05 - NAS 6.1 Multi-cell Operations
		// 
		/// <summary>
		/// Multi-cell operation. <see cref="UltraGridOverride.AllowMultiCellOperations"/> property for more info.
		/// </summary>
		MultiCellOperation = 4,

        // MRS 4/27/2009 - TFS16619
        /// <summary>
		/// Printing error
		/// </summary>
		Printing = 5
	}

	#endregion // ErrorType

	#region ExcludeFromColumnChooser

	// SSP 6/17/05 - NAS 5.3 Column Chooser
	// 
	/// <summary>
	/// Enum for specifying the Column's <see cref="UltraGridColumn.ExcludeFromColumnChooser"/> and Band's <see cref="UltraGridBand.ExcludeFromColumnChooser"/> properties.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridColumn.ExcludeFromColumnChooser"/> 
	/// <seealso cref="UltraGridBand.ExcludeFromColumnChooser"/> 
	/// <seealso cref="UltraGridColumnChooser"/>
	/// <seealso cref="ColumnChooserDialog"/>
	/// </remarks>
	public enum ExcludeFromColumnChooser
	{
		/// <summary>
		/// Default. Resolved to <b>False</b>.
		/// </summary>
		Default,

		/// <summary>
		/// The column or band is not displayed in the column chooser and the user 
		/// can not drag or drop it.
		/// </summary>
		True,

		/// <summary>
		/// The column or band does participate in the column chooser functionality.
		/// </summary>
		False
	}

	#endregion // ExcludeFromColumnChooser

	#region Row Filtering Enums

	#region RowFilterMode

	// SSP 3/21/02
	// Version 2 Row Filter Implementation
	// Added RowFilterMode enum
	//
	/// <summary>
	/// Dicates whether row filtering takes place at band level or
	/// rows collection level.
	/// </summary>
	public enum RowFilterMode
	{
		/// <summary>
		/// Default
		/// </summary>
		Default,

		/// <summary>
		/// Row filtering will be done at the band level. ColumnFilters off the UltraGridBand will be
		/// used for filtering the rows and all the rows in the band associated with the
		/// override will be used. Also the contents of the filter drop-down will contain 
		/// unique values from all the rows in the band.
		/// </summary>
		AllRowsInBand,

		/// <summary>
		/// Row filtering will be done at the RowsCollection level. ColumnFilters off the RowsCollection
		/// will be used for filtering the rows in that RowsCollection (and not all the rows
		/// in the band). Also the contents of the filter drop-down will contain unique values
		/// from the rows in that rows collection. Note: When rows in a rows collection are grouped by 
		/// columns, all the descendant groups will share the same column filters.
		/// </summary>
		SiblingRowsOnly
	};

	#endregion // RowFilterMode

	#region RowLayoutSizing

	// SSP 3/13/03 - Row Layout Functionality
	// Added RowLayoutSizing enum.
	// 
	/// <summary>
	/// Enum for specifying label and cell resizing mode in the row-layout mode.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridOverride.AllowRowLayoutCellSizing"/>, <seealso cref="UltraGridOverride.AllowRowLayoutLabelSizing"/>, <seealso cref="RowLayoutColumnInfo.AllowCellSizing"/>, <seealso cref="RowLayoutColumnInfo.AllowLabelSizing"/>
	/// </remarks>
	public enum RowLayoutSizing
	{
		/// <summary>
		/// Default.
		/// </summary>
		Default		= 0,

		/// <summary>
		/// Disallow resizing.
		/// </summary>
		None		= 1,

		/// <summary>
		/// Allow resizing horizontally.
		/// </summary>
		Horizontal  = 2,

		/// <summary>
		/// Allow resizing vertically.
		/// </summary>
		Vertical	= 3,

		/// <summary>
		/// Allow resizing both horizontally and vertically.
		/// </summary>
		Both		= 4
	}

	#endregion // RowLayoutSizing

	#region RowLayoutLabelStyle

	// SSP 3/31/03 - Row Layout Functionality
	// Added RowLayoutLabelStyle enum.
	//
	/// <summary>
	/// Enum for specifying whether the column labels are positioned with the cells or
	/// in a separate column headers area above the rows.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridBand.RowLayoutLabelStyle"/> <seealso cref="UltraGridBand.RowLayoutLabelPosition"/>
	/// </remarks>
	public enum RowLayoutLabelStyle
	{
		/// <summary>
		/// Column labels are positioned in a separate area above the rows.
		/// </summary>
		Separate		= 0,

		/// <summary>
		/// Column labels are with the cells and repeated in every row.
		/// </summary>
		WithCellData	= 1	
	}

	#endregion // RowLayoutLabelStyle

	// SSP 8/1/03 UWG1654 - Filter Action
	// Implemented Filter action (ShowEnabled, ShowDisabled, Hide) and filter row/cell 
	// appearances.
	// Added RowFilterAction enum.
	//
	#region RowFilterAction

	/// <summary>
	/// Specifies what action to take on rows that are filtered out.
	/// </summary>
	public enum RowFilterAction
	{
		/// <summary>
		/// Default. Default is is resolved to <b>HideFilteredOutRows</b>.
		/// </summary>
		Default					= 0,

		/// <summary>
		/// <b>AppearancesOnly</b> applies the <see cref="UltraGridOverride.FilteredInRowAppearance"/> and <see cref="UltraGridOverride.FilteredInCellAppearance"/> to the rows that are filtered in (rows for which the filter conditions evaluate to true) and <see cref="UltraGridOverride.FilteredOutRowAppearance"/> and <see cref="UltraGridOverride.FilteredOutCellAppearance"/> to rows that are filtered out (rows for which the filter conditions evaluated to false).
		/// </summary>
		AppearancesOnly			= 1,

		/// <summary>
		/// In addition to applying the filter related appearances as mentioned in the help for <b>AppearancesOnly</b>, <b>DisableFilteredOutRows</b> disables the rows that are filtered out (rows for which the filter conditions evaluate to false).
		/// </summary>
		DisableFilteredOutRows	= 2,

		/// <summary>
		/// In addition to applying the filter related appearances as mentioned in the help for <b>AppearancesOnly</b>, <b>HideFilteredOutRows</b> hides the rows that are filtered out (rows for which the filter conditions evaluate to false).
		/// </summary>
		HideFilteredOutRows		= 3
	}

	#endregion // RowFilterAction

	#region FilterLogicalOperator

	// SSP 3/21/02
	// Version 2 Row Filter Implementation
	// Added FilterLogicalOperator enum
	//
	/// <summary>
	/// Enum for specifying logical operator that should be used to 
	/// combine all the FilterConditions in ColumnFilter.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>FilterLogicalOperator</b> enum is used for specifying the
	/// ColumnFilter's <see cref="ColumnFilter.LogicalOperator"/> 
	/// and ColumnFiltersCollection's <see cref="ColumnFiltersCollection.LogicalOperator"/>
	/// properties.
	/// </p>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter.LogicalOperator"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection.LogicalOperator"/>
	/// </remarks>
	public enum FilterLogicalOperator
	{
		/// <summary>
		/// And logical operator
		/// </summary>
		And,

		/// <summary>
		/// Or logical operator
		/// </summary>
		Or
	}

	#endregion // FilterLogicalOperator

	#region FilterClearButtonLocation

	// SSP 3/9/05 - NAS 5.2 Filter Row
	// Added FilterClearButtonLocation enum.
	//
	/// <summary>
	/// The enum for specifying <see cref="UltraGridOverride.FilterClearButtonLocation"/> property.
	/// </summary>
	public enum FilterClearButtonLocation
	{
		/// <summary>
		/// Default is resolved to RowAndCell.
		/// </summary>
		Default,

		/// <summary>
		/// Do not display Clear Filter Buttons.
		/// </summary>
		Hidden, 
		
		/// <summary>
		/// Display Clear Filter Button in every filter cell.
		/// </summary>
		Cell, 

		/// <summary>
		/// Display Clear Filter Button in the row selector.
		/// </summary>
		Row, 

		/// <summary>
		/// Display Clear Filter Button in row selector and every filter cell.
		/// </summary>
		RowAndCell
	}

	#endregion // FilterClearButtonLocation

	#region FilterComparisionOperator

	// SSP 3/21/02
	// Version 2 Row Filter Implementation
	// Added FilterComparisionOperator enum
	//
	/// <summary>
	/// Enum for specifying the comparision operator.
	/// </summary>
	/// <remarks>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterCondition"/>
	/// </remarks>
	public enum FilterComparisionOperator
	{
		/// <summary>
		/// Tests for two values being equal.
		/// </summary>
		Equals,

		/// <summary>
		/// Tests for two values being not equal.
		/// </summary>
		NotEquals,

		/// <summary>
		/// Tests for the column's value being less than the value.
		/// </summary>
		LessThan,

		/// <summary>
		/// Tests for the column's value being less than or equal to the value.
		/// </summary>
		LessThanOrEqualTo,

		/// <summary>
		/// Tests for the column's value being greater than the value.
		/// </summary>
		GreaterThan,

		/// <summary>
		/// Tests for the column's value being greater than or equal to the value.
		/// </summary>
		GreaterThanOrEqualTo,

		/// <summary>
		/// Will do a wildcard comparision of the column's value to the comparision value
		/// taking comparision value as the string with wild cards.
		/// </summary>
		Like,

		/// <summary>
		/// Will do a regular expression comparision of the column's value to the comparision
		/// value taking comparision value as regular expression string.
		/// </summary>
		Match,

		// SSP 3/9/05 - NAS 5.2 Filter Row
		// Added NotLike, DoesNotMatch, StartsWith, DoesNotStartWith, EndsWith, DoesNotEndWith,
		// Contains and DoesNotContain members.
		//
		/// <summary>
		/// Complement of Like.
		/// </summary>
		NotLike, 

		/// <summary>
		/// Complement of Match.
		/// </summary>
		DoesNotMatch,

		/// <summary>
		/// Tests to see if the cell value starts with the operand.
		/// </summary>
		StartsWith,

		/// <summary>
		/// Complement of StartsWith.
		/// </summary>
		DoesNotStartWith,

		/// <summary>
		/// Tests to see if the cell value ends with the operand.
		/// </summary>
		EndsWith,

		/// <summary>
		/// Complement of EndsWith.
		/// </summary>
		DoesNotEndWith,

		/// <summary>
		/// Tests to see if the cell value contains the operand.
		/// </summary>
		Contains,

		/// <summary>
		/// Complement of Contains.
		/// </summary>
		DoesNotContain,

		// SSP 8/21/03 UWG2603
		// Added a way for the users to implement their own custom filtering logic.
		//
		/// <summary>
		/// Used for creating custom filters. This is used when deriving a class from FilterCondition
		/// and implementing custom logic for filter evaluation by overriding the MeetsCriteria method of
		/// in the derived class. Default implementation of MeetsCriteria always returns true for
		/// Custom filter comparision operator and thus it's necessary to derive from FilterCondition
		/// and override the MeetsCriteria for custom filters.
		/// </summary>
		Custom
	}

	#endregion // FilterComparisionOperator

	#region FilterComparisonType

	// SSP 5/16/05 - NAS 5.2 Filter Row
	//
	/// <summary>
	/// Enum for specifying Override's <see cref="UltraGridOverride.FilterComparisonType"/> 
	/// and the Column's <see cref="UltraGridColumn.FilterComparisonType"/> properties.
	/// </summary>
	public enum FilterComparisonType
	{
		/// <summary>
		/// The default filter comparison type.
		/// </summary>
		Default,

		/// <summary>
		/// Filtering is performed without case sensitivity.
		/// </summary>
		CaseInsensitive,

		/// <summary>
		/// Filtering is performed with case sensitivity.
		/// </summary>
		CaseSensitive,
	}

	#endregion // FilterComparisonType

	#region FilterEvaluationTrigger

	// SSP 3/9/05 - NAS 5.2 Filter Row
	// Added FilterEvaluationTrigger enum.
	//
	/// <summary>
	/// Enum for specifying Override's <see cref="UltraGridOverride.FilterEvaluationTrigger"/> 
	/// and the Column's <see cref="UltraGridColumn.FilterEvaluationTrigger"/> properties.
	/// </summary>
	public enum FilterEvaluationTrigger
	{
		/// <summary>
		/// Default is resolved to OnCellValueChange.
		/// </summary>
		Default,

		/// <summary>
		/// Filter input typed into a filter cell is applied as characters are typed. More precisely as soon as the ValueChanged event is raised by the embeddable editor the filter cell is using for editing.
		/// </summary>
		OnCellValueChange,

		/// <summary>
		/// Filter input typed into a filter cell is applied when the cell is left.
		/// </summary>
		OnLeaveCell,

		/// <summary>
		/// Filter input typed into filter cells is applied when the filter row is left.
		/// </summary>
		OnLeaveRow, 

		/// <summary>
		/// Filter input typed into filter cells is applied when Enter key is pressed while the filter row is active.
		/// </summary>
		OnEnterKey,

		/// <summary>
		/// Filter input typed into a filter cell is applied when Enter key is pressed or when the filter cell is left.
		/// </summary>
		OnEnterKeyOrLeaveCell, 

		/// <summary>
		/// Filter input typed into filter cells is applied when Enter key is pressed while the filter row is active or when the filter row is left.
		/// </summary>
		OnEnterKeyOrLeaveRow
	}

	#endregion // FilterEvaluationTrigger

	#region FilterOperandStyle

	// SSP 3/9/05 - NAS 5.2 Filter Row
	// Added FilterOperandStyle enum.
	//
	/// <summary>
	/// Enum for specifying Override's <see cref="UltraGridOverride.FilterOperandStyle"/> 
	/// and Column's <see cref="UltraGridColumn.FilterOperandStyle"/> properties.
	/// </summary>
	public enum FilterOperandStyle
	{
		/// <summary>
		/// Default is resolved to Combo.
		/// </summary>
		Default	= 0,

		/// <summary>
		/// No operand cell is displayed.
		/// </summary>
		None = 1,

		/// <summary>
		/// Operand cell is displayed as disabled so the user can�t change filters.
		/// </summary>
		Disabled = 2,

		/// <summary>
		/// Cell uses EditorWithText for entering filter operand.
		/// </summary>
		Edit = 3,

		/// <summary>
		/// Cell uses EditorWithCombo for entering filter operand either by directly entering it or using the drop down list. The drop down list is populated in the same manner as the filter drop down list.
		/// </summary>
		Combo = 4,

		/// <summary>
		/// Same as Combo option except there is no edit portion.
		/// </summary>
		DropDownList = 5,

		/// <summary>
		/// Use the same editor as column. BeforeRowFilterDropDownPopulate and BeforeRowFilterDropDown events will NOT be fired with this option.
		/// </summary>
		UseColumnEditor = 6,

        // MBS 12/4/08 - NA9.1 Excel Style Filtering
        /// <summary>
        /// Use the editor that the resolved FilterUIProvider of the band provides, or the default editor if none provided.
        /// </summary>
        FilterUIProvider = 7,
	}

	#endregion // FilterOperandStyle

	#region FilterOperatorDefaultValue

	// SSP 3/9/05 - NAS 5.2 Filter Row
	// Added FilterOperatorDefaultValue enum.
	//
	/// <summary>
	/// Enum for specifying Override's <see cref="UltraGridOverride.FilterOperatorDefaultValue"/> 
	/// and Column's <see cref="UltraGridColumn.FilterOperatorDefaultValue"/> properties.
	/// </summary>
	/// <remarks>
	/// <seealso cref="FilterComparisionOperator"/>
	/// <seealso cref="UltraGridOverride.FilterOperatorDefaultValue"/>
	/// <seealso cref="UltraGridColumn.FilterOperatorDefaultValue"/>
	/// </remarks>
	public enum FilterOperatorDefaultValue
	{
		/// <summary>
		/// Default is resolved to <b>StartsWith</b>.
		/// </summary>
		Default,

		/// <summary>
		/// Corresponds to <b>Equals</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		Equals,

		/// <summary>
		/// Corresponds to <b>NotEquals</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		NotEquals,

		/// <summary>
		/// Corresponds to <b>LessThan</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		LessThan,

		/// <summary>
		/// Corresponds to <b>LessThanOrEqualTo</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		LessThanOrEqualTo,

		/// <summary>
		/// Corresponds to <b>GreaterThan</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		GreaterThan,

		/// <summary>
		/// Corresponds to <b>GreaterThanOrEqualTo</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		GreaterThanOrEqualTo,

		/// <summary>
		/// Corresponds to <b>Like</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		Like,

		/// <summary>
		/// Corresponds to <b>Match</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		Match,

		/// <summary>
		/// Corresponds to <b>NotLike</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		NotLike, 

		/// <summary>
		/// Corresponds to <b>DoesNotMatch</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		DoesNotMatch,

		/// <summary>
		/// Corresponds to <b>StartsWith</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		StartsWith,

		/// <summary>
		/// Corresponds to <b>DoesNotStartWith</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		DoesNotStartWith,

		/// <summary>
		/// Corresponds to <b>EndsWith</b> memebr of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		EndsWith,

		/// <summary>
		/// Corresponds to <b>DoesNotEndWith</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		DoesNotEndWith,

		/// <summary>
		/// Corresponds to <b>Contains</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		Contains,

		/// <summary>
		/// Corresponds to <b>DoesNotContain</b> member of <see cref="FilterComparisionOperator"/>.
		/// </summary>
		DoesNotContain
	}

	#endregion // FilterOperatorDefaultValue

	#region FilterOperatorLocation

	// SSP 3/9/05 - NAS 5.2 Filter Row
	// Added FilterOperatorLocation enum.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.FilterOperatorLocation"/> property.
	/// </summary>
	public enum FilterOperatorLocation
	{
		/// <summary>
		/// Default is resolved to DropDownList.
		/// </summary>
		Default,

		/// <summary>
		/// The user interface for specifying operators in filter rows is hidden.
		/// </summary>
		Hidden,

		/// <summary>
		/// The user interface for specifying operators in filter rows is displayed above the user inteface for specifying the operands.
		/// </summary>
		AboveOperand,

		/// <summary>
		/// The user interface for specifying operators in filter rows is displayed within the user inteface for specifying the operands. The operator ui element is positioned left of the operand ui element.
		/// </summary>
		WithOperand
	}

	#endregion // FilterOperatorLocation

	#region FilterOperatorDropDownItems

	// SSP 3/9/05 - NAS 5.2 Filter Row
	// Added FilterOperatorDropDownItems enum.
	//
	/// <summary>
	/// Enum for specifying Override's <see cref="UltraGridOverride.FilterOperatorDropDownItems"/> 
	/// and Column's <see cref="UltraGridColumn.FilterOperatorDropDownItems"/> properties.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridOverride.FilterOperatorDropDownItems"/>
	/// <seealso cref="UltraGridColumn.FilterOperatorDropDownItems"/>
	/// <seealso cref="FilterComparisionOperator"/>
	/// </remarks>
	[Flags] public enum FilterOperatorDropDownItems
	{
		/// <summary>
		/// Default is resolved to <b>All</b>.
		/// </summary>
		Default = 0x1,

		/// <summary>
		/// No items will be displayed in the operator drop down.
		/// </summary>
		None	= 0x0,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>Equals</b>.
		/// </summary>
		Equals = 0x2,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>NotEquals</b>.
		/// </summary>
		NotEquals = 0x4,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>LessThan</b>.
		/// </summary>
		LessThan = 0x8,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>LessThanOrEqualTo</b>.
		/// </summary>
		LessThanOrEqualTo = 0x10,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>GreaterThan</b>.
		/// </summary>
		GreaterThan = 0x20,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>GreaterThanOrEqualTo</b>.
		/// </summary>
		GreaterThanOrEqualTo = 0x40,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>Like</b>.
		/// </summary>
		Like = 0x80,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>Match</b>.
		/// </summary>
		Match = 0x100,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>NotLike</b>.
		/// </summary>
		NotLike = 0x200, 

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>DoesNotMatch</b>.
		/// </summary>
		DoesNotMatch = 0x400,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>StartsWith</b>.
		/// </summary>
		StartsWith = 0x800,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>DoesNotStartWith</b>.
		/// </summary>
		DoesNotStartWith = 0x1000,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>EndsWith</b>.
		/// </summary>
		EndsWith = 0x2000,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>DoesNotEndWith</b>.
		/// </summary>
		DoesNotEndWith = 0x4000,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>Contains</b>.
		/// </summary>
		Contains = 0x8000,

		/// <summary>
		/// Corresponds to the <see cref="FilterComparisionOperator"/> <b>DoesNotContain</b>.
		/// </summary>
		DoesNotContain = 0x10000,

		// NOTE: As new values are added, Reserved needs to be updated as well accordingly.
		// 

		// SSP 1/3/05 BR07358
		// This is for working around an MS issue that was introduced in Whidbey. The code that whidbey generates
		// when serializing out All is something along the lines of following:
		// 
		// Me.UltraGrid1.DisplayLayout.Override.SpecialRowSeparator = CType(((((((Infragistics.Win.UltraWinGrid.SpecialRowSeparator.Headers Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.FilterRow)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.TemplateAddRow)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.FixedRows)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.EmptyRows)  _
		//    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.2147483520
		// 
		// Above code causes a compiler error as SpecialRowSeparator.2147483520 is not a valid value.
		// The workaround seems to be that if we have a complement of all the values other than All
		// then their logic will write out that complement enum value instead of the decimal number.
		// 
		/// <summary>
		/// Reserved. For internal use only.
		/// </summary>
		Reserved = All & ~0x1fffe,

		/// <summary>
		/// All.
		/// </summary>
		All = 0x7ffffffe
	}

	#endregion // FilterOperatorDropDownItems

	#region FilterUIType

	// SSP 3/9/05 - NAS 5.2 Filter Row
	// Added FilterUIType enum.
	//
	/// <summary>
	/// Enum for specifying Override's <see cref="UltraGridOverride.FilterUIType"/> property.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridOverride.FilterUIType"/>
	/// </remarks>
	public enum FilterUIType
	{
		/// <summary>
		/// Default is resolved to HeaderIcons.
		/// </summary>
		Default,

		/// <summary>
		/// Filter icon is displayed in each column header. Clicking it drops down a drop down list from which the user can select a filter.
		/// </summary>
		HeaderIcons,

		/// <summary>
		/// Displays a fixed filter row in the headers area.
		/// </summary>
		FilterRow
	}

	#endregion // FilterUIType

	#endregion // Row Filtering Enums

	// SSP 7/18/03 - Fixed headers
	// Added FixedHeaderIndicator enum.
	//
	#region FixedHeaderIndicator

	/// <summary>
	/// Enum used for specifying whether the user is allowed to fix and unfix headers.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>FixedHeaderIndicator</b> enum is used for specifying Override's
	/// <see cref="UltraGridOverride.FixedHeaderIndicator"/> and Header's 
	/// <see cref="HeaderBase.FixedHeaderIndicator"/> properties.
	/// </p>
	/// <seealso cref="UltraGridOverride.FixedHeaderIndicator"/> 
	/// <seealso cref="HeaderBase.FixedHeaderIndicator"/>
	/// </remarks>
	public enum FixedHeaderIndicator
	{
		/// <summary>
		/// Default.
		/// </summary>
		Default			= 0,

		/// <summary>
		/// Don't allow the user to fix or unfix the header.
		/// </summary>
		None			= 1,

		/// <summary>
		/// Display a button that allows the user to fix or unfix the header. Button also displays whether the header is fixed or not.
		/// </summary>
		Button			= 2,

		/// <summary>
		/// Displays a <i>[Fix Header]</i> or <i>[Unfix Header]</i> item in the swap drop down depending on whether the header is currently fixed.
		/// </summary>
		InSwapDropDown	= 3
	}

	#endregion // FixedHeaderIndicator

	#region FixedRowIndicator

	// SSP 3/9/05 - NAS 5.2 Fixed Rows
	// Added FixedRowIndicator enum.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.FixedRowIndicator"/> property.
	/// </summary>
	public enum FixedRowIndicator
	{
		/// <summary>
		/// Default is resolved to None.
		/// </summary>
		Default, 

		/// <summary>
		/// No ui is provided for fixing/unfixing rows.
		/// </summary>
		None, 

		/// <summary>
		/// A state button with the pin icon is displayed in the row selectors. This option will increase the default width of the row selectors to accomodate the button.
		/// </summary>
		Button 
	}

	#endregion // FixedRowIndicator

	#region FixedRowSortOrder

	// SSP 3/9/05 - NAS 5.2 Fixed Rows
	// Added FixedRowSortOrder enum.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.FixedRowSortOrder"/> property.
	/// </summary>
	public enum FixedRowSortOrder
	{
		/// <summary>
		/// Default is resolved to Sorted.
		/// </summary>
		Default, 

		/// <summary>
		/// Fixed rows remain in the same order as they are fixed, even after the sorted columns are changed. Calling RefreshSort method on the FixedRowsCollection will resort the fixed rows based on the sorted columns.
		/// </summary>
		FixOrder, 

		/// <summary>
		/// Fixed rows are sorted when the user sorts columns. Note that however when the user fixes a row the row is always added at the end of the fixed rows. To insert it into the correct sort position call RefreshSort method on the associated FixedRowsCollection in AfterRowFixedStateChanged event handler.
		/// </summary>
		Sorted
	}

	#endregion // FixedRowSortOrder

	#region FixedRowStyle

	// SSP 3/9/05 - NAS 5.2 Fixed Rows
	// Added FixedRowStyle enum.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.FixedRowStyle"/> property.
	/// </summary>
	public enum FixedRowStyle
	{
		/// <summary>
		/// Default is resolved to Top.
		/// </summary>
		Default, 

		/// <summary>
		/// Rows will be fixed on top of the row collection.
		/// </summary>
		Top, 

		/// <summary>
		/// Rows will be fixed on bottom of the row collection.
		/// </summary>
		Bottom
	}

	#endregion // FixedRowStyle

	// JAS v5.2 GroupBy Break Behavior 5/3/05
	//
	#region GroupByMode

	/// <summary>
	/// Enum used for specifying the <see cref="UltraGridColumn.GroupByMode"/> property.
	/// </summary>
	public enum GroupByMode
	{
		/// <summary>
		/// The default mode.
		/// </summary>
		Default,

		/// <summary>
		/// The groups are based on the date portion of DateTime values found in the column's cells.
		/// The column's <see cref="UltraGridColumn.DataType"/> must be System.DateTime for this mode to work properly.
		/// </summary>
		Date,

		/// <summary>
		/// The groups are based on the first character of the cell text.
		/// This option is intended to be used when the column�s data type is String.
		/// </summary>
		FirstCharacter,

		/// <summary>
		/// The groups are based on the first two characters of the cell text.
		/// This option is intended to be used when the column�s data type is String.
		/// </summary>
		First2Characters,

		/// <summary>
		/// The groups are based on the first three characters of the cell text.
		/// This option is intended to be used when the column�s data type is String.
		/// </summary>
		First3Characters,

		/// <summary>
		/// The groups are based on the first four characters of the cell text.
		/// This option is intended to be used when the column�s data type is String.
		/// </summary>
		First4Characters,

		/// <summary>
		/// The groups are based on the date and hour portions of DateTime values found in the column's cells.
		/// The column's <see cref="UltraGridColumn.DataType"/> must be System.DateTime for this mode to work properly.
		/// </summary>
		Hour,

		/// <summary>
		/// The groups are based on the date, hour, and minute portions of DateTime values found in the column's cells.
		/// The column's <see cref="UltraGridColumn.DataType"/> must be System.DateTime for this mode to work properly.
		/// </summary>
		Minute,

		/// <summary>
		/// The groups are based on the year and month portions of DateTime values found in the column's cells.
		/// The column's <see cref="UltraGridColumn.DataType"/> must be System.DateTime for this mode to work properly.
		/// </summary>
		Month,

		/// <summary>
		/// The groups are created in the same way that Outlook 2003 creates groups for dates.
		/// The column's <see cref="UltraGridColumn.DataType"/> must be System.DateTime for this mode to work properly.
		/// </summary>
		OutlookDate,

		/// <summary>
		/// The groups are based on the quarter and year that the cell values are in.
		/// The column's <see cref="UltraGridColumn.DataType"/> must be System.DateTime for this mode to work properly.
		/// </summary>
		Quarter,

		/// <summary>
		/// The groups are based on the date, hour, minute, and second portions of DateTime values found in the column's cells.
		/// The column's <see cref="UltraGridColumn.DataType"/> must be System.DateTime for this mode to work properly.
		/// </summary>
		Second,

		/// <summary>
		/// The groups are based on the text of the cells, not their values.
		/// </summary>
		Text,

		/// <summary>
		/// The groups are based on the value of the cells, not their text.
		/// Note, the description of each groupby row will use the text of the cell by default.
		/// </summary>
		Value,

		/// <summary>
		/// The groups are based on the year portion of DateTime values found in the column's cells.
		/// The column's <see cref="UltraGridColumn.DataType"/> must be System.DateTime for this mode to work properly.
		/// </summary>
		Year
	}

	#endregion // GroupByMode

	// JAS 2005 v2 GroupBy Row Extensions
	//
	#region GroupByRowExpansionStyle

	/// <summary>
	/// Used by the <see cref="UltraGridOverride.GroupByRowExpansionStyle"/> property.
	/// </summary>
	public enum GroupByRowExpansionStyle
	{
		/// <summary>
		/// The default setting.
		/// </summary>
		Default,

		/// <summary>
		/// GroupBy rows can be expanded/collapsed via an expansion indicator and double clicking on the GroupBy row.
		/// </summary>
		ExpansionIndicatorAndDoubleClick,

		/// <summary>
		/// GroupBy rows can be expanded/collapsed via an expansion indicator on the GroupBy row.
		/// </summary>
		ExpansionIndicator,

		/// <summary>
		/// GroupBy rows can be expanded/collapsed via double clicking on the GroupBy row.
		/// When this setting is used the GroupBy rows will <b>not</b> contain expansion indicators.
		/// </summary>
		DoubleClick,

		/// <summary>
		/// GroupBy rows cannot be expanded/collapsed by the end-user.
		/// </summary>
		Disabled
	}

	#endregion // GroupByRowExpansionStyle

	// JAS 2005 v2 GroupBy Row Extensions
	//
	#region GroupByRowInitialExpansionState

	/// <summary>
	/// Used by the <see cref="UltraGridOverride.GroupByRowInitialExpansionState"/> property.
	/// </summary>
	public enum GroupByRowInitialExpansionState
	{
		/// <summary>
		/// The default setting.
		/// </summary>
		Default,

		/// <summary>
		/// When the grid groups on a column all of the rows will be collapsed.
		/// </summary>
		Collapsed,

		/// <summary>
		/// When the grid groups on a column all of the rows will be expanded.
		/// </summary>
		Expanded
	}

	#endregion // GroupByRowInitialExpansionState

	#region GroupByBoxStyle

	/// <summary>
	/// Constants that specify whether the GroupByBox will appear at full size or compacted
	/// </summary>
	public enum GroupByBoxStyle
	{
		/// <summary>
		/// Full. The GroupByBox will appear at its full size.
		/// </summary>
		Full = 0,

		/// <summary>
		/// Compact. The GroupByBox will appear compacted, occupying the least amount of screen real estate possible.
		/// </summary>
		Compact = 1
	};

	#endregion // GroupByBoxStyle

	#region GroupBySummaryDisplayStyle

	// SSP 3/9/05 - NAS 5.2 Extension of Summaries Functionality
	// Added ability to display summaries in group-by rows aligned with columns, 
	// summary footers for group-by row collections, fixed summary footers and
	// being able to display the summary on top of the row collection.
	// Added GroupBySummaryDisplayStyle enum.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.GroupBySummaryDisplayStyle"/> property.
	/// </summary>
	public enum GroupBySummaryDisplayStyle
	{
		/// <summary>
		/// Default is resolved to Text. This maintains the backward compatibility.
		/// </summary>
		Default, 

		/// <summary>
		/// Summaries are appended to the group-by row�s description.
		/// </summary>
		Text,

		/// <summary>
		/// Summaries are displayed as cells in group-by row. These summary cells are positioned according the SummarySettings� SummaryPosition settings. If the summary cells do not intersect with the group-by row description then they are positioned in the same level as the description. If at least one summary cell intersects with the group-by row description then all the summary cells are positioned below the description.
		/// </summary>
		SummaryCells,

		/// <summary>
		/// Summaries are displayed as cells in group-by row. These summary cells are positioned according the SummarySettings� SummaryPosition settings. All the summary cells are positioned below the description so they do not conflict with the group-by row description.
		/// </summary>
		SummaryCellsAlwaysBelowDescription
	}

	#endregion // GroupBySummaryDisplayStyle

	#region HeaderClickAction

	/// <summary>
	/// Used to specify how a column or group header should react to being clicked.
	/// </summary>
	public enum HeaderClickAction
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used (or 'Select' if not parent is set to default).
		/// </summary>
		Default = 0,

		/// <summary>
		/// Select. Selects the column or group (based on the setting of the SelectTypeCol property).
		/// </summary>
		Select = 1,

		/// <summary>
		/// Single Selection Sort. Changes the sort indicator for the column after clearing all other columns' sort indicators.
		/// </summary>
		SortSingle = 2,

		/// <summary>
		/// Multiple Selection Sort. Changes the sort indicator for the column. If the shift key is pressed does not clear other columns sort indicators.
		/// </summary>
		SortMulti = 3,

		// SSP 4/19/04 - Virtual Binding Related
		// Added ExternalSortSingle and ExternalSortMulti members.
		//
		/// <summary>
		/// Same as SortSingle except with <b>ExternalSortSingle</b> UltraGrid won't perform the sort itself. This is useful if you want to sort externally through the data source. You can hook into the <see cref="UltraGridBase.AfterSortChange"/> event and apply the sort to the external data source.
		/// </summary>
		ExternalSortSingle	= 4,

		/// <summary>
		/// Same as SortMulti except with <b>ExternalSortMulti</b> UltraGrid won't perform the sort itself. This is useful if you want to sort externally through the data source. You can hook into the <see cref="UltraGridBase.AfterSortChange"/> event and apply the sort to the external data source.
		/// </summary>
		ExternalSortMulti	= 5
	};

	#endregion // HeaderClickAction

	#region HeaderPlacement

	// SSP 7/14/05 - NAS 5.3 Header Placement
	// 
	/// <summary>
	/// Enum for specifying the <see cref="UltraGridOverride.HeaderPlacement"/> property.
	/// </summary>
	public enum HeaderPlacement
	{
		/// <summary>
		/// The default is resolved to RepeatOnBreak.
		/// </summary>
		Default, 

		/// <summary>
		/// Headers are repeated on every band break. The current row displays headers if the previous row is from a different row collection. This is the default behavior.
		/// </summary>
		RepeatOnBreak,

		/// <summary>
		/// Headers of all the bands are displayed on top. They always remain visible, even if a row from that band is currently not visible. You can hide headers of certain bands by setting the existing ColHeadersVisible property of the band.
		/// </summary>
		FixedOnTop,
		
		/// <summary>
		/// Headers are displayed only once per row island. When a row is expanded with visible children, the next sibling does not display the headers.
		/// </summary>
		OncePerRowIsland,

		/// <summary>
		/// Same as OncePerRowIsland however when rows are grouped by one or more columns, the headers are displayed above the group-by-rows. Here the row island is taken to mean the collection of rows associated with an IList.
		/// </summary>
		OncePerGroupedRowIsland 
	}

	#endregion // HeaderPlacement

	#region IncludeRowTypes

	// SSP 11/10/03 Add Row Feature
	// Added IncludeRowTypes enum.
	//
	[ Flags( ) ]
	internal enum IncludeRowTypes
	{
		DataRowsOnly	= 0x0,

		// SSP 4/15/05 - NAS 5.2 Filter Row/Fixed Add Row/Fixed Summary Footer
		// We have to take into account filter row, summary row etc...
		// Took out TemplateAddRow and added SpecialRows instead. Also replaced
		// every place we were using TemplateAddRow with SpecialRows.
		//
		//TemplateAddRow  = 0x1,
		SpecialRows = 0x1
	}

	#endregion // IncludeRowTypes

	#region GridRowType

	// SSP 12/13/05 - Recursive Row Enumerator
	// 
	/// <summary>
	/// Used for specifying the rowType parameter of the UltraGridBand's 
	/// <see cref="UltraGridBand.GetRowEnumerator"/> and RowsCollection's
	/// <see cref="RowsCollection.GetRowEnumerator"/> methods.
	/// </summary>
	[Flags]
	public enum GridRowType
	{
		/// <summary>
		/// Data rows.
		/// </summary>
		DataRow = 0x1,

		/// <summary>
		/// Group-by rows.
		/// </summary>
		GroupByRow = 0x2
	}

	#endregion // GridRowType

	#region InvalidValueBehavior

	// SSP 8/1/05 - NAS 5.3 Invalid Value Behavior
	// 
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.InvalidValueBehavior"/> and <see cref="UltraGridColumn.InvalidValueBehavior"/> properties.
	/// </summary>
	public enum InvalidValueBehavior
	{
		/// <summary>
		/// Default. Default is resolved to <b>RetainValueAndFocus</b>.
		/// </summary>
		Default,

		/// <summary>
		/// Retains the value entered by the user and also keeps the cell in edit mode.
		/// </summary>
		RetainValueAndFocus,

		/// <summary>
		/// Reverts the value back to the original value. The cell does not stay in edit mode.
		/// </summary>
		RevertValue,

		/// <summary>
		/// Reverts the value back to the original value and the cell is kept in edit mode.
		/// </summary>
		RevertValueAndRetainFocus
	}

	#endregion // InvalidValueBehavior

	#region LabelPosition

	// SSP 1/30/03 - Row Layout Functionality
	// Added LabelPosition enum.
	//
	/// <summary>
	/// LabelPosition is used to specify if and where the column header will be shown.
	/// </summary>
	/// <remarks>
	/// <seealso cref="RowLayoutColumnInfo.LabelPosition"/>
	/// <seealso cref="UltraGridBand.RowLayoutLabelPosition"/>
	/// </remarks>
	public enum LabelPosition
	{
		/// <summary>
		/// Default value. In card-view, Default will resolve to Left while in regular view, Default will resolve to None.
		/// </summary>
		Default	= 0,
		
		/// <summary>
		/// Column label will not be shown.
		/// </summary>
		None	= 1,

		/// <summary>
		/// Column label will be shown to the left of the cell.
		/// </summary>
		Left	= 2,

		/// <summary>
		/// Column label will be shown on the top of the cell.
		/// </summary>
		Top		= 3,

		/// <summary>
		/// Column label will be shown to the right of the cell.
		/// </summary>
		Right	= 4,

		/// <summary>
		/// Column label will be shown on the bottom of the cell.
		/// </summary>
		Bottom	= 5,

		// SSP 11/17/04
		// Added a way to hide the header or the cell of a column in the row layout mode.
		// This way you can do things like hide a row of headers or add unbound columns
		// and hide their cells to show grouping headers etc....
		//
		/// <summary>
		/// Cells of the column won't be shown. Only the label will be shown.
		/// </summary>
		LabelOnly = 6
	}

	#endregion // LabelPosition

	#region LoadStyle

	// SSP 4/13/04 - Virtual Binding
	// Added LoadStyle property to UltraGridLayout.
	//
	/// <summary>
	/// Enum associated with <see cref="UltraGridLayout.LoadStyle"/> property.
	/// </summary>
	public enum LoadStyle
	{
		/// <summary>
		/// Pre-loads all rows.
		/// </summary>
		PreloadRows = 0,

		/// <summary>
		/// Loads rows as they are needed. <b>NOTE:</b> Some operations, like sorting rows, 
		/// will cause the all rows to be loaded regardless of the load style setting because they 
		/// require access to all rows. For example if you have a summary that sums the values of a
		/// column then when the summary is calculated all the rows will be loaded.
		/// </summary>
		LoadOnDemand = 1
	}

	#endregion // LoadStyle

	#region MergedCellEvaluationType
	
	// SSP 1/19/05 - Merged Cell Feature
	//
	/// <summary>
	/// Enum for specifying the <see cref="UltraGridColumn.MergedCellEvaluationType"/> property.
	/// </summary>
	public enum MergedCellEvaluationType
	{
		/// <summary>
		/// Default.
		/// </summary>
		Default			= 0,

		/// <summary>
		/// Merge cells when their underlying values are the same.
		/// </summary>
		MergeSameValue	= 1,

		/// <summary>
		/// Merge cells when their display text are the same.
		/// </summary>
		MergeSameText	= 2
	}

	#endregion // MergedCellEvaluationType

	#region MergedCellStyle

	// SSP 11/3/04 - Merged Cell Feature
	//
	/// <summary>
	/// Enum for specifying the Override's <see cref="UltraGridOverride.MergedCellStyle"/> and Column's <see cref="UltraGridColumn.MergedCellStyle"/> properties.
	/// </summary>
	public enum MergedCellStyle
	{
		/// <summary>
		/// Default. Default will be resolved to <b>Never</b>.
		/// </summary>
		Default,

		/// <summary>
		/// Always merge cells.
		/// </summary>
		Always,

		/// <summary>
		/// Merge cells only when the column is a sort column.
		/// </summary>
		OnlyWhenSorted,

		/// <summary>
		/// Never merge cells.
		/// </summary>
		Never
	}

	#endregion // MergedCellStyle

	#region MergedCellContentArea

	// SSP 11/3/04 - Merged Cell Feature
	//
	/// <summary>
	/// Enum for specifying the <see cref="UltraGridOverride.MergedCellContentArea"/> and <see cref="UltraGridColumn.MergedCellContentArea"/> properties.
	/// </summary>
	public enum MergedCellContentArea
	{
		/// <summary>
		/// Default. Default will be resolved to <b>VirtualRect</b>.
		/// </summary>
		Default,

		/// <summary>
		/// The contents of the merged cell will be positioned within the entire area of the merged cell.
		/// </summary>
		VirtualRect,

		/// <summary>
		/// The contents of the merged cell will be positioned within the visible portion of the merged cell.
		/// </summary>
		VisibleRect
	}

	#endregion // MergedCellContentArea

	#region NavigateType

	internal enum NavigateType
	{
		First         = 0,
		Last          = 1,
		Next          = 2,
		Prev          = 3,
		Above         = 4,
		Below         = 5,
		Topmost       = 6,
		Bottommost    = 7
	};

	#endregion // NavigateType

	#region NewColumnLoadStyle

	// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
	// 
	/// <summary>
	/// The enum associated with the Layout's <see cref="UltraGridLayout.NewColumnLoadStyle"/> property.
	/// </summary>
	public enum NewColumnLoadStyle
	{
		/// <summary>
		/// Shows new columns.
		/// </summary>
		Show = 0,

		/// <summary>
		/// Hides new columns.
		/// </summary>
		Hide = 1
	}

	#endregion // NewColumnLoadStyle

	#region NewBandLoadStyle

	// SSP 5/3/05 - NewColumnLoadStyle & NewBandLoadStyle Properties
	// 
	/// <summary>
	/// The enum associated with the Layout's <see cref="UltraGridLayout.NewBandLoadStyle"/> property.
	/// </summary>
	public enum NewBandLoadStyle
	{
		/// <summary>
		/// Shows new columns.
		/// </summary>
		Show = 0,

		/// <summary>
		/// Hides new columns.
		/// </summary>
		Hide = 1
	}

	#endregion // NewBandLoadStyle

	#region Nullable

	/// <summary>
	/// Used to specify the way null values are stored.
	/// </summary>
	public enum Nullable
	{
		/// <summary>
		/// Automatic. Data stored as null if allowed, otherwise as empty string.
		/// </summary>
		Automatic = 0,

		/// <summary>
		/// DBNull. Data stored as DBNull value.
		/// </summary>
		Null = 1,

		/// <summary>
		/// Empty String. Data stored as a zero-length string.
		/// </summary>
		EmptyString = 2,

		// SSP 11/3/03 UWG2700
		// Added Nothing.
		//
		/// <summary>
		/// Nothing. Data stored as Nothing (C# null).
		/// </summary>
		Nothing	= 3,
		
		// SSP 11/30/04 - BR02429 - Added Disallow to Nullable
		// Changed the default behavior of the column's IsNullable to return true instead
		// of false in case the underlying property descriptor is not a DataColumn.
		//
		/// <summary>
		/// Column doesn't allow empty values.
		/// </summary>
		Disallow = 4
	};

	#endregion // Nullable

	#region PerformAutoSizeType

	// SSP 4/14/03
	// Added an overload of UltraGridColumn.PerformAutoResize that takes in an argument of type PerformAutoSizeType.
	//
	/// <summary>
	/// Enum for specifying the aruguement to an overload of <see cref="UltraGridColumn.PerformAutoResize()"/>.
	/// </summary>
	public enum PerformAutoSizeType
	{
		/// <summary>
		/// Calculate the width of the column based on cell data in visible rows.
		/// </summary>
		VisibleRows		= 0,

		/// <summary>
		/// Calculate the width of the column based on cell data in all rows in band.
		/// </summary>
		AllRowsInBand	= 1,

		// SSP 6/24/03 UWG2385
		// Added methods to calculate the autosize width of the column.
		// Added None enum member.
		//
		/// <summary>
		/// Don't base the auto-size width of the column based on the any cell data.
		/// </summary>
		None			= 2
	}

	#endregion // PerformAutoSizeType

	#region ProcessMode

	// JDN 11/16/04 Support for Synchronous Sorting and Filtering
	/// <summary>
	/// Enum specifying how to apply sorting and filtering
	/// </summary>
	public enum ProcessMode
	{	
		/// <summary>
		/// Process rows lazily
		/// </summary>
		Lazy	  = 0,

		/// <summary>
		/// Process all rows synchronously
		/// </summary>
		Synchronous  = 1,

		/// <summary>
		/// Process only expanded rows synchronously
		/// </summary>
		SynchronousExpanded    =    2
	};

	#endregion // ProcessMode

	#region PropertyCategories

	/// <summary>
	/// Category Properties
	/// </summary>
	public enum PropertyCategories
	{	
		/// <summary>
		/// Serialize Appearance Collection
		/// </summary>
		AppearanceCollection	= 0x1,
		/// <summary>
		/// Serialize Bands
		/// </summary>
		Bands				= 0x4,
		/// <summary>
		/// Serialize Groups
		/// </summary>
		Groups				= 0xc,
		/// <summary>
		/// Serialize UnBound Columns
		/// </summary>
		UnboundColumns		= 0x14,
		/// <summary>
		/// Serialize Sorted Columns
		/// </summary>
		SortedColumns			= 0x24,
		/// <summary>
		/// Serialize Column Scroll Regions
		/// </summary>
		ColScrollRegions		= 0x40,
		/// <summary>
		/// SerializeRow Scroll Regions
		/// </summary>
		RowScrollRegions		= 0x80,
		/// <summary>
		/// Serializes properties exposed off the layout as well as sub objects that are not explicitly listed in this enum.
		/// </summary>
		General				= 0x100,
		/// <summary>
		/// Serialize Value Lists
		/// </summary>
		ValueLists			= 0x400,

		// SSP 7/15/02 UWG1367
		// Added Summaries and ColumnFilters properties.
		//
		// SSP 4/14/03 UWG2198
		// We need to serialize the bands in order to serialize the summaries. Ored 0x800 with 0x4 (for bands).
		//
		/// <summary>
		/// Serialize summaries. This will also serialize bands as well.
		/// </summary>
		Summaries			= 0x804,

		// SSP 4/14/03 UWG2198
		// We need to serialize the bands in order to serialize the column filters. Ored 0x1000 with 0x4 (for bands).
		//
		/// <summary>
		/// Serialize ColumnFilers off the band. This will also serialize bands as well. NOTE: This does not serialize column filters off the RowsCollection.
		/// </summary>
		ColumnFilters		= 0x1004,

		/// <summary>
		/// Serialize All
		/// </summary>
		All					= -1	//0xffffffff
	};

	#endregion // PropertyCategories

	#region RecursionType

	// JDN 11/16/04 Support for Synchronous Sorting and Filtering
	/// <summary>
	/// Enum specifying level of recursion to apply to a RowCollection method
	/// </summary>
	public enum RecursionType
	{	
		/// <summary>
		/// No recursion
		/// </summary>
		None	  = 0,

		/// <summary>
		/// Apply recursively to all expanded rows
		/// </summary>
		Expanded  = 1,

		/// <summary>
		/// Apply recursively to all rows
		/// </summary>
		All    =    2
	};

	#endregion // RecursionType

	#region RefreshRow

	/// <summary>
	/// Used to specify the actions for the Refresh method.
	/// </summary>
	public enum RefreshRow
	{
		/// <summary>
		/// Display. Only causes the display to be repainted.
		/// </summary>
		RefreshDisplay = 0,

		/// <summary>
		/// Re-Initialize. Causes the InitializeRow event to occur.
		/// </summary>
		FireInitializeRow = 1,

		// SSP 11/25/02 UWG1684
		// Added ReloadData member which will reload rows from the underlying lists.
		//
		/// <summary>
		/// Reload rows.
		/// </summary>
		ReloadData
	};

	#endregion // RefreshRow

	// JAS v5.2 DoubleClick Events 4/27/05
	//
	#region RowArea

	/// <summary>
	/// Contains values which represent the various parts of a row in the grid.
	/// </summary>
	public enum RowArea
	{
		/// <summary>
		/// Represents a GroupBy row.
		/// </summary>
		GroupByRowArea,

		/// <summary>
		/// Represents a cell in a row.
		/// </summary>
		Cell, 

		/// <summary>
		/// Represents the area of a row containing cells.
		/// </summary>
		CellArea,

		/// <summary>
		/// Represents the row preview area.
		/// </summary>
		RowPreviewArea,

		/// <summary>
		/// Represents the row selector portion of a row.
		/// </summary>
		RowSelectorArea
	}

	#endregion // RowArea

	#region RowConnectorStyle

	/// <summary>
	/// Enum for specifying sibling row connector styles.
	/// </summary>
	public enum RowConnectorStyle
	{
		/// <summary>
		/// default style
		/// </summary>
		Default    = 0,

		/// <summary>
		/// no connector lines
		/// </summary>
		None       = 1,

		/// <summary>
		/// draw the connector line dotted
		/// </summary>
		Dotted     = 2,

		/// <summary>
		/// draw the connector line dashed
		/// </summary>
		Dashed     = 3,

		/// <summary>
		/// draw the connector line solid
		/// </summary>
		Solid      = 4,

		/// <summary>
		/// draw the connector line inset
		/// </summary>
		Inset      = 5,

		/// <summary>
		/// draw the connector line raised
		/// </summary>
		Raised     = 6
	};

	#endregion // RowConnectorStyle

	#region RowLayoutCellNavigation

	// SSP 8/17/05 BR05463
	// 
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.RowLayoutCellNavigationVertical"/> property.
	/// </summary>
	public enum RowLayoutCellNavigation
	{
		/// <summary>
		/// Default. Deault is resolved to Snaking.
		/// </summary>
		Default = 0,

		/// <summary>
		/// When navigating cells using left/right/up/down arrow keys, <b>Snaking</b> option 
		/// will traverse all the cells of a row before moving to a different row. For example,
		/// when navigating via down arrow if the current cell is the last cell in the logical
		/// row-layout column then the first cell in the next logical column of the same row
		/// will be activated.
		/// </summary>
		Snaking = 1,

		/// <summary>
		/// When navigating cells using left/right/up/down arrow keys, <b>Adjacent</b> option
		/// will move to the next row once the cells in current logical row-layout column or 
		/// row are exhausted. For example, when navigating via down arrow if the current cell
		/// is the last cell in the logical row-layout column then the first cell of that same
		/// logical row-layout column in the next row will be activated.
		/// </summary>
		Adjacent = 2
	}

	#endregion // RowLayoutCellNavigation

	#region RowSelectorNumberStyle

	// SSP 3/30/05 NAS 5.2 Row Numbers
	//
	/// <summary>
	/// Enum used for specifying <see cref="UltraGridOverride.RowSelectorNumberStyle"/> property.
	/// </summary>
	public enum RowSelectorNumberStyle
	{
		/// <summary>
		/// Default. Resolves to None.
		/// </summary>
		Default,

		/// <summary>
		/// No row numbers are displayed.
		/// </summary>
		None,

		/// <summary>
		/// Row's list index. This is the index associated with <see cref="UltraGridRow.ListIndex"/> property.
		/// </summary>
		ListIndex,

		/// <summary>
		/// Row's index in its parent rows collection. This is the index associated with <see cref="UltraGridRow.Index"/> property.
		/// </summary>
		RowIndex,

		/// <summary>
		/// Row's visible index in the parent rows collection. This is the index associated with <see cref="UltraGridRow.VisibleIndex"/> property.
		/// </summary>
		VisibleIndex
	}

	#endregion // RowSelectorNumberStyle

	#region RowPropertyCategories

	// SSP 1/14/03 UWG1892
	// Added an easier way to facilitate WYSIWIG printing.
	//
	/// <summary>
	/// Used for specifying which settings to carried over from grid rows to print rows when
	/// printing.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGrid.Print()"/>,<seealso cref="UltraGrid.PrintPreview()"/>
	/// </remarks>
	[Flags] public enum RowPropertyCategories
	{
		/// <summary>
		/// None.
		/// </summary>
		None		= 0x0,
		
		/// <summary>
		/// Row's expanded state (whether the row is expanded or collapsed).
		/// </summary>
		Expanded	= 0x1,
		
		/// <summary>
		/// Row's hidden state (whether the row is hidden or visible). Since filtered rows 
		/// are hidden, specifying Hidden is a good way to apply the filters to the print rows
		/// so all the filtered out rows in the grid also are filtered out in the print.
		/// </summary>
		Hidden		= 0x2,

		/// <summary>
		/// Unbound cell data.
		/// </summary>
		UnboundData = 0x4,

		/// <summary>
		/// Various appearance settings on the rows (Appearance, CellAppearance etc.).
		/// </summary>
		Appearances	= 0x10,

		// AS 5/30/03
		/// <summary>
		/// Appearance property on the cell objects.
		/// </summary>
		CellAppearance	= 0x20,

		// SSP 8/8/03 UWG2578
		// We need to copy row heights as well. Added Height member.
		//
		/// <summary>
		/// Row heights.
		/// </summary>
		Height			= 0x40,

		// SSP 9/5/03 UWG2630
		// Added Description member.
		//
		/// <summary>
		/// Row description.
		/// </summary>
		Description		= 0x80,

		// SSP 12/15/03 UWG2800
		// Added RowSpacing member.
		//
		/// <summary>
		/// BeforeRowSpacing and AfterRowSpacing settings of rows.
		/// </summary>
		RowSpacing		= 0x100,

		// SSP 12/27/05 BR08445
		// 
		/// <summary>
		/// Copies over per cell Editor and ValueList (see UltraGridCell's <see cref="UltraGridCell.ValueList"/>
		/// and <see cref="UltraGridCell.Editor"/>) settings.
		/// </summary>
		CellEditorSettings = 0x200,

		// SSP 1/2/07 BR18034
		// Added Tag.
		// 
		/// <summary>
		/// Copies over the row and cell tags.
		/// </summary>
		Tag = 0x400,

        // MRS NAS v8.2 - CardView Printing
        /// <summary>
        /// Copies over the expanded state of a card in Compressed <see cref="UltraGridCardSettings.Style"/>
        /// </summary>
        CompressedCardExpansionState = 0x800,

		/// <summary>
		/// All of above.
		/// </summary>
		All			= 0xffff
	}

	#endregion // RowPropertyCategories

	#region RowScrollAction

	/// <summary>
	/// Enum to specify how to scroll a row scroll region.
	/// </summary>
	public enum RowScrollAction
	{	
		/// <summary>
		/// scroll as if up arrow on the scroll bar was clicked
		/// </summary>
		LineUp	  = 0,

		/// <summary>
		/// scroll as if down arrow on the scroll bar was clicked
		/// </summary>
		LineDown  = 1,

		/// <summary>
		/// scroll one page up
		/// </summary>
		PageUp    = 2,

		/// <summary>
		/// scroll one page down
		/// </summary>
		PageDown  = 3,

		/// <summary>
		/// scroll all the way to the top of the row scroll region
		/// </summary>
		Top       = 6,

		/// <summary>
		/// scroll all the way to the bottom of the row scroll region
		/// </summary>
		Bottom    = 7
	};

	#endregion // RowScrollAction

	#region RowSelectorHeaderStyle

	//JDN 11/19/04 Added RowSelectorHeader
	/// <summary>
	/// Enum to specify the style of the row selector header.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridOverride.RowSelectorHeaderStyle"/>
	/// </remarks>
	public enum RowSelectorHeaderStyle
	{	
		/// <summary>
		/// Default style.
		/// </summary>
		Default	  = 0,

		/// <summary>
		/// No row selector header.
		/// </summary>
		None  = 1,

		/// <summary>
		/// Extend the first column header over the row selectors.
		/// </summary>
		ExtendFirstColumn    = 2,

		/// <summary>
		/// Show a RowSelectorHeader element.
		/// </summary>
		SeparateElement  = 3,

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// Added ColumnChooserButton member.
		// 
		/// <summary>
		/// Display a button which when clicked upon displays the column chooser dialog that
		/// lets the user select which columns to display in the UltraGrid.
		/// </summary>
		ColumnChooserButton = 4,

		// SSP 12/8/05 BR06669
		// Added row selector header style of ColumnChooserButtonFixedSize.
		// 
		/// <summary>
		/// Same as <b>ColumnChooserButton</b> except that <b>ColumnChooserButton</b>
		/// displays the button expanded to fit the entire area over the row selector
		/// where as this option displays the button with ideal size. <b>Note</b> that
		/// the alignment of the column chooser button can be controlled using the
		/// ImageHAlign and ImageVAlign property settings of the 
		/// <see cref="UltraGridOverride.RowSelectorHeaderAppearance"/>.
		/// </summary>
		ColumnChooserButtonFixedSize = 5
	};

	#endregion // RowSelectorHeaderStyle

	#region RowSizing

	/// <summary>
	/// Used to specify the way rows can be resized.
	/// </summary>
	public enum RowSizing
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Fixed. Rows cannot be resized by the user and will not be by the control.
		/// </summary>
		Fixed = 1,

		/// <summary>
		/// Free. Rows can be resized by the user on a row-by-row basis.
		/// </summary>
		Free = 2,

		/// <summary>
		/// Synchronized. All rows will have the same height. Rows can be resized simultaneously - resizing a single row resizes all rows.
		/// </summary>
		Sychronized = 3,

		/// <summary>
		/// Auto-Sizing Fixed. The control will resize each row to the size of the largest cell in that row.
		/// The user is not allowed to resize the rows.
		/// </summary>
		AutoFixed = 4,

		/// <summary>
		/// Auto-Sizing Free. The control will resize each row to the size of the largest cell in that row.
		/// The user is allowed to resize the rows.
		/// </summary>
		AutoFree = 5
	};

	#endregion // RowSizing

	#region RowSizingArea

	/// <summary>
	/// Used to specify the interface used to resize rows.
	/// </summary>
	public enum RowSizingArea
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Row Selectors Only. Only the edges of row selectors are used to resize rows.
		/// </summary>
		RowSelectorsOnly = 1,

		/// <summary>
		/// Borders Only. Only the borders between rows are used to resize rows.
		/// </summary>
		RowBordersOnly = 2,

		/// <summary>
		/// Entire Row. Row borders and record selector edges are both used to resize rows.
		/// </summary>
		EntireRow = 3
	};
 
	#endregion // RowSizingArea

	#region Commented Out Code

	// JJD 11/21/02 
	// The RowType enum was never used.
	//	/// <summary>
	//	/// Used to specify the type of row.
	//	/// </summary>
	//	public enum RowType
	//	{	
	//		/// <summary>
	//		/// Bound. Row is bound to a data source.
	//		/// </summary>
	//		Bound	= 1,
	//
	//		/// <summary>
	//		/// Add Row. Row is used to add new data.
	//		/// </summary>
	//		AddRow	= 2
	//	};

	#endregion // Commented Out Code

	#region RowUpdateCancelAction

	// SSP 1/14/03 UWG1928
	//
	/// <summary>
	/// Indicates the action should be taken when BeforeRowUpdate event is cancelled.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGrid.RowUpdateCancelAction"/>
	/// </remarks>
	public enum RowUpdateCancelAction
	{
		/// <summary>
		/// Cancelling the BeforeRowUpdate event will cause the UltraGrid to call CancelEdit on the
		/// underlying row object (if the row object is an IEditableObject).
		/// </summary>
		CancelUpdate = 0,


		/// <summary>
		/// Cancelling the BeforeRowUpdate event will cause the UltraGrid to retain the currently typed
		/// data and also retain activation on the row.
		/// </summary>
		RetainDataAndActivation = 1  
	}

	#endregion // RowUpdateCancelAction

	#region Scrollbars

	/// <summary>
	/// Specifies if vertical and/or horizontal scrollbars will be shown
	/// </summary>
	public enum Scrollbars
	{
		/// <summary>
		/// no scroll bars are to be shown
		/// </summary>
		None = 0,
		
		/// <summary>
		/// show horizontal scroll bar only
		/// </summary>
		Horizontal = 1,

		/// <summary>
		/// show vertical scroll bar only
		/// </summary>
		Vertical = 2,

		/// <summary>
		/// show both horizontal and vertical scroll bars
		/// </summary>
		Both = 3,

		/// <summary>
		/// display either scroll bar as necessary
		/// </summary>
		Automatic = 4
	};
    
	#endregion // Scrollbars

	#region Scrollbar

	/// <summary>
	/// Determines if a scrollbar will be shown on a specific row or column scroll region
	/// </summary>
	public enum Scrollbar
	{
		/// <summary>
		/// Default. The grid's ScrollBars property value determines if scrollbars are shown.
		/// </summary>
		Default = 0,

		/// <summary>
		/// always show the scroll bar
		/// </summary>
		Show = 1,

		/// <summary>
		/// only show it if needed
		/// </summary>
		ShowIfNeeded = 2,

		/// <summary>
		/// do not show the scroll bar
		/// </summary>
		Hide = 3
	};

	#endregion // Scrollbar

	#region ScrollbarVisibility

	// SSP 12/4/03 - Scrolling Till Last Row Visible
	// Added a way to specify whether the scrollbar is hidden by changing from bool to
	// an enum with three members.
	// 
	internal enum ScrollbarVisibility
	{
		Check,
		Visible,
		Hidden
	};

	#endregion // ScrollbarVisibility

	#region ScrollBounds

	// SSP 11/24/03 - Scrolling Till Last Row Visible
	// Added the functionality to not allow the user to scroll any further once the
	// last row is visible.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridLayout.ScrollBounds"/> property.
	/// </summary>
	public enum ScrollBounds
	{
		/// <summary>
		/// UltraGrid will stop scrolling once the last item is visible.
		/// </summary>
		ScrollToFill		= 0,

		/// <summary>
		/// UltraGrid will scroll until last item is the only item visible.
		/// </summary>
		ScrollToLastItem	= 1
	}

	#endregion // ScrollBounds

	#region ScrollStyle

	// SSP 2/28/02
	// Added ScrollStyle for vertical scrolling to allow for immediate
	// scrolling of rows when thumb is tracked.
	//
	/// <summary>
	/// Controls how scrolling is done in the grid.
	/// </summary>
	public enum ScrollStyle
	{
		/// <summary>
		/// When tracking the thumb on vertical scroll bars, rows are not 
		/// scrolled until the tumbtrack is released. Instead scroll tips are 
		/// shown.
		/// </summary>
		Deferred	= 0,
		
		/// <summary>
		/// Rows are scrolled immediately when vertical scrollbar thumb is tracked.
		/// Also the scroll tooltips are defaulted not to show. This setting can be
		/// overridden by <see cref="Infragistics.Win.UltraWinGrid.UltraGridOverride.TipStyleScroll"/>.
		/// </summary>
		Immediate	= 1,
	}

	#endregion // ScrollStyle

	#region SelectType

	/// <summary>
	/// Used to specify the type of selection that is allowed for an object.
	/// </summary>
	public enum SelectType
	{
		/// <summary>
		/// Use Default. The setting of the object's parent will be used.
		/// </summary>
		Default = 0,

		/// <summary>
		/// None. Objects may not be selected.
		/// </summary>
		None = 1,

		/// <summary>
		/// Single Select. Only one object may be selected at any time.
		/// </summary>
		Single = 2,

		/// <summary>
		/// Extended Select. Multiple objects may be selected at once.
		/// </summary>
		Extended = 3,

		/// <summary>
		/// Strategy used when multiple items can be selected but pressing the left
		/// button and dragging does not select other items but instead starts dragging
		/// the selected item immediately. 
		/// </summary>
		ExtendedAutoDrag = 4,

		/// <summary>
		/// Strategy used when only a single item can be selected and pressing the left
		/// button and dragging does not select other items but instead starts dragging
		/// the selected item immediately.
		/// </summary>
		SingleAutoDrag = 5
	};

	#endregion // SelectType

	#region ShowBandLabels

	/// <summary>
	/// Used for spcifying how band lables are displayed in the group-by-box.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>ShowBandLabels</b> enum is used for specifying the 
	/// <see cref="GroupByBox.ShowBandLabels"/> property of the <see cref="GroupByBox"/> object.
	/// </p>
	/// <seealso cref="GroupByBox.ShowBandLabels"/>
	/// <seealso cref="GroupByBox"/>
	/// </remarks>
	public enum ShowBandLabels
	{
		/// <summary>
		/// Default
		/// </summary>
		Default		= 0,

		/// <summary>
		/// Display all band labels.
		/// </summary>
		All			= 1,

		/// <summary>
		/// Don't display any band label.
		/// </summary>
		None		= 2,

		/// <summary>
		/// Display band lables for all bands if there are any group-by columns in any band.
		/// </summary>
		AllWithGroupByColumnsOnly	= 3,

		/// <summary>
		/// Show labels for bands that are between the first band with a 
		/// group-by-column and the last band with a group-by-column 
		/// (including them).
		/// if some but not all band labels are displayed an elipsis will apear.
		/// </summary>
		IntermediateBandsOnly		= 4
	}

	#endregion // ShowBandLabels

	#region ShowExpansionIndicator

	/// <summary>
	/// Enum used for specifying the Override's <see cref="UltraGridOverride.ExpansionIndicator"/> 
	/// and Row's <see cref="UltraGridRow.ExpansionIndicator"/> properties.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridOverride.ExpansionIndicator"/>
	/// <seealso cref="UltraGridRow.ExpansionIndicator"/>
	/// </remarks>
	public enum ShowExpansionIndicator
	{
		/// <summary>
		/// Use Default.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Always
		/// </summary>
		Always = 1,

		/// <summary>
		/// Never
		/// </summary>
		Never = 2,

		/// <summary>
		/// Check when the row is first displayed
		/// </summary>
		CheckOnDisplay = 3,

		/// <summary>
		/// After the row is expanded, we check to see if there are any children.
		/// If not, the indicator disappears.
		/// </summary>
		CheckOnExpand = 4
	};

	#endregion // ShowExpansionIndicator

	#region SiblingRow

	/// <summary>
	/// Used to specify the relationship between the current row and its sibling rows.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>SiblingRow</b> enum is used for specifying parameter to various methods
	/// like the <see cref="UltraGridRow.GetSibling(SiblingRow)"/> method.
	/// </p>
	/// <seealso cref="UltraGridRow.GetSibling(SiblingRow)"/>
	/// </remarks>
	public enum SiblingRow
	{
		/// <summary>
		/// First Sibling. Row is the first sibling in the current row's band.
		/// </summary>
		First = 0,

		/// <summary>
		/// Last Sibling. Row is the last sibling in the current row's band.
		/// </summary>
		Last = 1,

		/// <summary>
		/// Next Sibling. Row is the next sibling in the current row's band.
		/// </summary>
		Next = 2,

		/// <summary>
		/// Previous Sibling. Row is the previous sibling in the current row's band.
		/// </summary>
		Previous = 3
	};

	#endregion // SiblingRow

	#region SizingMode

	/// <summary>
	/// Used to specify how ScrollRegions can be resized.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>SizingMode</b> enum is used for specifying the 
	/// <see cref="ScrollRegionBase.SizingMode"/> property.
	/// </p>
	/// <seealso cref="ScrollRegionBase.SizingMode"/>
	/// <seealso cref="ColScrollRegion"/>
	/// <seealso cref="RowScrollRegion"/>
	/// <seealso cref="UltraGridLayout.ColScrollRegions"/>
	/// <seealso cref="UltraGridLayout.ColScrollRegions"/>
	/// <seealso cref="UltraGridLayout.RowScrollRegions"/>
	/// </remarks>
	public enum SizingMode
	{
		/// <summary>
		/// Free Sizing. ScrollRegion can be resized using the splitter.
		/// </summary>
		Free   = 0,

		/// <summary>
		/// Fixed Sizing. ScrollRegion cannot be resized by the user.
		/// </summary>
		Fixed  = 1
	};

	#endregion // SizingMode

	// JAS v5.2 GroupBy Break Behavior 5/4/05
	//
	#region SortComparisonType

	/// <summary>
	/// Used for specifying the Override's <see cref="UltraGridOverride.SortComparisonType"/> 
	/// and Column's <see cref="UltraGridColumn.SortComparisonType"/> properties.
	/// </summary>
	public enum SortComparisonType
	{
		/// <summary>
		/// The default sort comparison type.
		/// </summary>
		Default,

		/// <summary>
		/// Sorting is performed without case sensitivity.
		/// </summary>
		CaseInsensitive,

		/// <summary>
		/// Sorting is performed with case sensitivity.
		/// </summary>
		CaseSensitive,
	}

	#endregion // SortComparisonType

	#region SortIndicator

	/// <summary>
	/// Used to specify the method that should be used to sort the column.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>SortIndicator</b> enum is used for specifying the Column's
	/// <see cref="UltraGridColumn.SortIndicator"/> property.
	/// To allow the user to sort the columns set the <see cref="UltraGridOverride.HeaderClickAction"/>
	/// property of the Override object to <b>SortSingle</b> or <b>SortMulti</b>. This will
	/// allow the user to sort rows by a column by clicking on the column's header.
	/// To sort the rows by one or more columns in code, add the columns to 
	/// the Band's <see cref="UltraGridBand.SortedColumns"/> collection.
	/// </p>
	/// <seealso cref="UltraGridOverride.HeaderClickAction"/>
	/// <seealso cref="UltraGridColumn.SortIndicator"/>
	/// <seealso cref="UltraGridColumn.IsGroupByColumn"/>
	/// <seealso cref="UltraGridBand.SortedColumns"/>
	/// <seealso cref="SortedColumnsCollection.Add(UltraGridColumn, bool, bool)"/>
	/// </remarks>
	public enum SortIndicator
	{
		/// <summary>
		/// None. The column should not be sorted.
		/// </summary>
		None = 0,

		/// <summary>
		/// Ascending. The column should be sorted in ascending order.
		/// </summary>
		Ascending = 1,

		/// <summary>
		/// Descending. The column should be sorted in descending order.
		/// </summary>
		Descending = 2,

		/// <summary>
		/// Disabled. Sorting should be disabled for the column.
		/// </summary>
		Disabled = 3
	};
	
	#endregion // SortIndicator

	#region SpecialRowSeparator

	// SSP 3/9/05 - NAS 5.2 Fixed Rows
	// Added SpecialRowSeparator enum.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.SpecialRowSeparator"/> property.
	/// </summary>
	[Flags] public enum SpecialRowSeparator
	{
		/// <summary>
		/// Default is resolved to All.
		/// </summary>
		Default = 0x1, 

		/// <summary>
		/// No separator is displayed.
		/// </summary>
		None = 0x0, 

		/// <summary>
		/// A separator is displayed below the headers.
		/// </summary>
		Headers = 0x2,

		/// <summary>
		/// A separator is displayed below the filter row.
		/// </summary>
		FilterRow = 0x4,

		/// <summary>
		/// A separator is displayed below or above the template add-row depending upon
		/// whether the add-row is on top or bottom respectively.
		/// </summary>
		TemplateAddRow = 0x8, 

		/// <summary>
		/// A separator is displayed separating fixed rows from non-fixed rows is displayed.
		/// </summary>
		FixedRows = 0x10,

		/// <summary>
		/// A separator is displayed below or above the summary depending upon
		/// whether the summary row is fixed on top or bottom respectively.
		/// </summary>
		SummaryRow = 0x20, 

		/// <summary>
		/// A separator is displayed before the empty rows. 
		/// <see cref="UltraGridLayout.EmptyRowSettings"/> for more information on how to
		/// enable the empty rows functionality.
		/// </summary>
		EmptyRows = 0x40,

		// NOTE: As new values are added, Reserved needs to be updated as well accordingly.
		// 

		// SSP 1/3/05 BR07358
		// This is for working around an MS issue that was introduced in Whidbey. The code that whidbey generates
		// when serializing out All is something along the lines of following:
		// 
		// Me.UltraGrid1.DisplayLayout.Override.SpecialRowSeparator = CType(((((((Infragistics.Win.UltraWinGrid.SpecialRowSeparator.Headers Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.FilterRow)  _
        //    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.TemplateAddRow)  _
        //    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.FixedRows)  _
        //    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.SummaryRow)  _
        //    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.EmptyRows)  _
        //    Or Infragistics.Win.UltraWinGrid.SpecialRowSeparator.2147483520
		// 
		// Above code causes a compiler error as SpecialRowSeparator.2147483520 is not a valid value.
		// The workaround seems to be that if we have a complement of all the values other than All
		// then their logic will write out that complement enum value instead of the decimal number.
		// 
		/// <summary>
		/// Reserved. For internal use only.
		/// </summary>
		Reserved = All & ~0x7e,

		/// <summary>
		/// Separator is displayed for all special rows.
		/// </summary>
		All = 0x7ffffffe
	}

	#endregion // SpecialRowSeparator

	#region SummaryDisplayAreas

	// SSP 3/9/05 - NAS 5.2 Extension of Summaries Functionality
	// Added ability to display summaries in group-by rows aligned with columns, 
	// summary footers for group-by row collections, fixed summary footers and
	// being able to display the summary on top of the row collection.
	// Added SummaryDisplayAreas enum.
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.SummaryDisplayArea"/> property.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridOverride.SummaryDisplayArea"/>
	/// <seealso cref="SummarySettings.SummaryDisplayArea"/>
	/// </remarks>
	[Flags] public enum SummaryDisplayAreas
	{
		/// <summary>
		/// Default. Default is resolved to the <see cref="SummarySettings.Hidden"/> 
		/// and <see cref="SummarySettings.DisplayInGroupBy"/> property settings. 
		/// In other words if those properties are left to their default values, then 
		/// the summaries will be displayed in the summary footer at the end of each 
		/// row collection and in each group-by row (not aligned to columns).
		/// </summary>
		Default = 0x1, 

		/// <summary>
		/// Summary is not displayed anywhere. This overrides the <see cref="SummarySettings.Hidden"/> 
		/// and <see cref="SummarySettings.DisplayInGroupBy"/> properties.
		/// </summary>
		None = 0x0, 

		/// <summary>
		/// Summary is displayed in the beginning of the row collection.
		/// </summary>
		Top = 0x2, 

		/// <summary>
		/// Summary is displayed in the beginning of the row collection. In root row collection the summary 
		/// is fixed so it doesn't get scrolled out of view. In child row collections this behaves in the
		/// same way as <b>Top</b>.
		/// </summary>
		TopFixed = 0x4,

		/// <summary>
		/// Summary is displayed at the end of the row collection.
		/// </summary>
		Bottom = 0x8, 

		/// <summary>
		/// Summary is displayed at the end of the row collection. In root row collection the summary is 
		/// fixed so it doesn't get scrolled out of view. In child row collections this behaves in the 
		/// same way as <b>Bottom</b>.
		/// </summary>
		BottomFixed = 0x10, 

		/// <summary>
		/// Summary is displayed in each group-by row. This property overrides the 
		/// <see cref="SummarySettings.DisplayInGroupBy"/> property.
		/// </summary>
		InGroupByRows = 0x20, 

		/// <summary>
		/// Summary is displayed in the summary footers of group-by row collections. This flag must
		/// be combined with either <b>Top</b>, <b>TopFixed</b>, <b>Bottom</b> or <b>BottomFixed</b>
		/// in order for it to have any effect.
		/// </summary>
		GroupByRowsFooter = 0x40,

		// SSP 7/15/05 FR01423
		// Added the ability to show only the group-by row footers and only the top level footers.
		// 
		/// <summary>
		/// Hides the summary footers of data rows. This is useful when rows are grouped and 
		/// you want to show the summary footers of group-by rows only.
		/// </summary>
		HideDataRowFooters = 0x80,

		// SSP 7/15/05 FR01423
		// Added the ability to show only the group-by row footers and only the top level footers.
		// 
		/// <summary>
		/// Only displays the summary footers on the root rows collection (root level). 
		/// This is useful when rows are grouped and you want to show the summary footers 
		/// of the root group-by rows. When rows are not grouped this flag has no effect.
		/// <b>Note</b> that this does not have any effect on the workings of 
		/// <b>InGroupByRows</b> option. <b>InGroupByRows</b> will still work the same
		/// way regardless of the value of this flag.
		/// </summary>
		RootRowsFootersOnly = 0x100
	}

	#endregion // SummaryDisplayAreas

	#region SummaryPosition

	/// <summary>
	/// Indicates the location where a summary will be displayed.
	/// </summary>
	/// <remarks>
	/// <seealso cref="SummarySettings.SummaryPosition"/>
	/// <seealso cref="UltraGridOverride.SummaryDisplayArea"/>
	/// <seealso cref="SummarySettings.SummaryDisplayArea"/>
	/// </remarks>
	public enum SummaryPosition
	{
		/// <summary>
		/// The summary will be displayed on the left of the footer. If there are
		/// multiple SummarySettings objects with Left as the ShowLocation, then
		/// they will be displayed in order that they occur in the 
		/// SummarySettingsCollection they belong to.
		/// </summary>
		Left,

		/// <summary>
		/// The summary will be displayed on the right of the footer. If there are
		/// multiple SummarySettings objects with Right as the ShowLocation, then
		/// they will be displayed in order that they occur in the 
		/// SummarySettingsCollection they belong to.
		/// </summary>
		Right,

		/// <summary>
		/// The summary will be displayed on the center of the footer. If there are
		/// multiple SummarySettings objects with Center as the ShowLocation, then
		/// they will be displayed in order that they occur in the 
		/// SummarySettingsCollection they belong to.
		/// </summary>
		Center,

		/// <summary>
		/// If UseLocationColumn is specified, then the summary will be displayed
		/// under the LocationColumn. If LocationColumn is null, then the summary
		/// won't be displayed at all. LocationColumn column must be from the same
		/// band as the band SummariesCollection is from. If an attemt is made to
		/// assign a column not from the same band, an exception will be throw.
		/// </summary>
		UseSummaryPositionColumn
	}

	#endregion // SummaryPosition

	#region SummaryType

	/// <summary>
	/// Used to specify the type of summary to calculate on a <see cref="Infragistics.Win.UltraWinGrid.SummarySettings"/> object.
	/// </summary>
	/// <remarks>
	/// <seealso cref="SummarySettings.SummaryType"/>
	/// <seealso cref="UltraGridBand.Summaries"/>
	/// </remarks>
	public enum SummaryType
	{
		/// <summary>
		/// Average of values. Null values are taken as 0 and averaged.
		/// </summary>
		Average,

		/// <summary>
		/// Sum of all values.
		/// </summary>
		Sum,

		/// <summary>
		/// Minimum value. Null values are ignored and minimum of non-null values is calculated.
		/// </summary>
		Minimum,

		/// <summary>
		/// Maximum value.
		/// </summary>
		Maximum,

		/// <summary>
		/// Number of rows. Null values are taken into account as well.
		/// </summary>
		Count,

		/// <summary>
		/// Custom summary. CustomSummaryCalculator must be set to a
		/// valid instance of ICustomSummaryCalculator.
		/// </summary>
		Custom,

		// SSP 8/2/04 - UltraCalc
		// Added Formula property to the SummarySettings object.
		//
		/// <summary>
		/// Formula summary. Formula assigned to <see cref="SummarySettings.Formula"/> property will be evaluated and used as the summary value.
		/// </summary>
		Formula
	}

	#endregion // SummaryType

	#region SupportDataErrorInfo

	// SSP 12/14/04 - IDataErrorInfo Support
	//
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.SupportDataErrorInfo"/> property.
	/// </summary>
	public enum SupportDataErrorInfo
	{
		/// <summary>
		/// Default. Default is resolved to None.
		/// </summary>
		Default,

		/// <summary>
		/// None. Specifies not to use IDataErrorInfo.
		/// </summary>
		None,

		/// <summary>
		/// Rows only. Use IDataErrorInfo to display only row errors. Cell errors are ignored.
		/// </summary>
		RowsOnly,

		/// <summary>
		/// Cells only. Use IDataErrorInfo to display only cell errors. Row errors are ignored.
		/// </summary>
		CellsOnly,

		/// <summary>
		/// Both rows and cells. Use IDataErrorInfo to display row and cell errors.
		/// </summary>
		RowsAndCells
	}

	#endregion // SupportDataErrorInfo

	#region MultiCellOperation Enum

	// SSP 11/15/05 - NAS 6.1 Multi-cell Operations
	// 
	/// <summary>
	/// Used for specifying which operation is being performed.
	/// </summary>
	public enum MultiCellOperation
	{
		/// <summary>
		/// Copy operation.
		/// </summary>
		Copy,

		/// <summary>
		/// Cut operation.
		/// </summary>
		Cut,

		/// <summary>
		/// Delete cell values operation.
		/// </summary>
		Delete,

		/// <summary>
		/// Paste operation.
		/// </summary>
		Paste,

		/// <summary>
		/// Undo operation.
		/// </summary>
		Undo,

		/// <summary>
		/// Redo operation.
		/// </summary>
		Redo
	}

	#endregion // MultiCellOperation Enum

	#region MultiCellSelectionMode

	// SSP 2/24/06 BR09715
	// Added MultiCellSelectionMode property to support snaking cell selection.
	// 
	/// <summary>
	/// Enum for specifying <see cref="UltraGridOverride.MultiCellSelectionMode"/> property.
	/// </summary>
	public enum MultiCellSelectionMode
	{
		/// <summary>
		/// Default. Default is resolved to Standard.
		/// </summary>
		Default,

		/// <summary>
		/// Standard. Perform non-snaking selection.
		/// </summary>
		Standard,

		/// <summary>
		/// Snaking. Perform snaking selection.
		/// </summary>
		Snaking
	}

	#endregion // MultiCellSelectionMode

	#region MultipleBandSupport

	// SSP 6/17/05 - NAS 5.3 Column Chooser
	// 
	/// <summary>
	/// Enum for specifying the <see cref="UltraGridColumnChooser.MultipleBandSupport"/> property.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridColumnChooser"/>
	/// <seealso cref="UltraGridColumnChooser.MultipleBandSupport"/>
	/// <seealso cref="UltraGridColumnChooser.Style"/>
	/// <seealso cref="ColumnChooserDialog"/>
	/// </remarks>
	public enum MultipleBandSupport
	{
		/// <summary>
		/// A user interface for letting the user select a different band is displayed. 
		/// This is the default option. Initially if the <see cref="UltraGridColumnChooser.CurrentBand"/> 
		/// property is set then the columns from that band will be displayed otherwise columns from all 
		/// bands are displayed. The user then can choose a specific band to display the columns from by
		/// selecting that band from the band selector UI. <b>Note:</b> If there is a single band
		/// in the UltraGrid then this option will NOT show the band selection UI.
		/// </summary>
		ShowBandSelectionUI,

		/// <summary>
		/// Columns from all bands are displayed in the columns list, grouped by the band. Since columns
		/// from all bands are displayed, the user interface for selecting a band is not displayed. 
		/// <b>Note</b> that the <see cref="UltraGridColumnChooser.CurrentBand"/> property will be 
		/// ignored by this option and will be reset to <b>null</b>.
		/// </summary>
		DisplayColumnsFromAllBands,

		/// <summary>
		/// Only columns from a single band are displayed. No user interface for selecting a 
		/// different band is available. To specify the band whose columns to display , set the 
		/// <see cref="UltraGridColumnChooser.CurrentBand"/> property. If that property is left 
		/// to <b>null</b> then columns from root band will be displayed.
		/// </summary>
		SingleBandOnly
	}

	#endregion // MultipleBandSupport

	#region TabNavigation

	/// <summary>
	/// Used to specify how the TAB key is used when navigating the program.
	/// </summary>
	public enum TabNavigation
	{
		/// <summary>
		/// Next Cell. A TAB key will move the focus to the next cell in the grid control.
		/// </summary>
		NextCell = 0,

		/// <summary>
		/// Next Control. A TAB key will move the focus to the next control in the form's tab order.
		/// </summary>
		NextControl = 1,

		/// <summary>
		/// Next control from the last cell. A TAB key will move the focus to the next control in the form's tab order when the last cell is active, otherwise it will move from cell to cell.
		/// </summary>
		NextControlOnLastCell	= 2
	};

	#endregion // TabNavigation

	#region TemplateAddRowLocation

	// SSP 11/10/03 Add Row Feature
	// Added TemplateAddRowLocation enum.
	//
	internal enum TemplateAddRowLocation
	{
		None,
		Top,
		Bottom
	}

	#endregion // TemplateAddRowLocation

	#region TipStyle

	/// <summary>
	/// Used for specifying tool tip styles for sibling row connectors,
	/// cells and scroll tips.
	/// </summary>
	/// <remarks>
	/// <seealso cref="UltraGridOverride.TipStyleScroll"/>
	/// <seealso cref="UltraGridOverride.TipStyleCell"/>
	/// <seealso cref="UltraGridOverride.TipStyleRowConnector"/>
	/// <seealso cref="HeaderBase.ToolTipText"/>
	/// <seealso cref="UltraGridCell.ToolTipText"/>
	/// <seealso cref="UltraGridRow.ToolTipText"/>
	/// <seealso cref="SummarySettings.ToolTipText"/>
	/// <seealso cref="SummaryValue.ToolTipText"/>
	/// <seealso cref="UltraGridLayout.ScrollStyle"/>
	/// </remarks>
	public enum TipStyle
	{
		/// <summary>
		/// default tool tip style
		/// </summary>
		Default = 0,

		/// <summary>
		/// show tool tips
		/// </summary>
		Show = 1,

		/// <summary>
		/// do not show tool tips
		/// </summary>
		Hide = 2
	};

	#endregion // TipStyle

	#region UltraComboStyle

	/// <summary>
	/// The enumerator used for specifiying the style for an UltraCombo control.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>UltraComboStyle</b> enum is used for specifying the 
	/// UltraCombo's <see cref="UltraCombo.DropDownStyle"/> property.
	/// </p>
	/// <seealso cref="UltraCombo.DropDownStyle"/>
	/// </remarks>
	public enum UltraComboStyle
	{
		/// <summary>
		/// The user can select from the list or type into
		/// the textbox.
		/// </summary>
		DropDown = 0,

		/// <summary>
		/// The user can only select from the list.
		/// </summary>
		DropDownList = 1,

		// JJD 9/29/01
		// Comment out DropDownValidate. Future version possibly.
		
	}


	#endregion // UltraComboStyle

	#region UpdateMode

	/// <summary>
	/// Used for specifying how the bound IList is updated when any changes are 
	/// made to the data displayed in the grid. Default UpdateMode when not specified 
	/// is OnRowChangeOrLostFocus.
	/// </summary>
	public enum UpdateMode
	{
		/// <summary>
		/// the Bound IList is updated when the user modifies cell(s) in a row
		/// and then selects a different row or the grid loses focus
		/// </summary>
		OnRowChangeOrLostFocus	= 0,

		/// <summary>
		/// the Bound IList is updated when the user modifies cell(s) in a row
		/// and then selects a different row
		/// </summary>
		OnRowChange				= 1,
		
		/// <summary>
		/// the Bound IList is updated when the user modifies a cell and
		/// exits the edit mode or when the grid loses the focus
		/// </summary>
		OnCellChangeOrLostFocus	= 2,
		
		/// <summary>
		/// the Bound IList is updated when the user modifies a cell and
		/// exits the edit mode
		/// </summary>
		OnCellChange			= 3,		

		/// <summary>
		/// the Bound IList is updated when Update function 
		/// on the Grid is called
		/// </summary>
		OnUpdate				= 4
	};

	#endregion // UpdateMode

	#region UseScrollWindow

	// SSP 7/21/03 UWG1997
	// Added UseScrollWindow property so that the user can prevent the grid from using 
	// ScrollWindow method for scrolling.
	// Added UseScrollWindow enum.
	//
	/// <summary>
	/// Used for specifying <see cref="UltraGridLayout.UseScrollWindow"/> method.
	/// </summary>
	/// <remarks>
	/// <p><seealso cref="UltraGridLayout.UseScrollWindow"/></p>
	/// </remarks>
	public enum UseScrollWindow
	{
		/// <summary>
		/// Don't use scroll window method to scroll the grid when scrolling horizontally or vertically.
		/// </summary>
		None				= 0,

		/// <summary>
		/// Use scroll window method to scroll the grid when scrolling horizontally only.
		/// </summary>
		HorizontalOnly		= 1,

		/// <summary>
		/// Use scroll window method to scroll the grid when scrolling vertically only.
		/// </summary>
		VerticalOnly		= 2,

		/// <summary>
		/// Use scroll window method to scroll the grid when scrolling either horizontally or vertically.
		/// </summary>
		Both				= 3
	}

	#endregion // UseScrollWindow

	#region ViewStyle

	/// <summary>
	/// Used to specify whether the grid will display single or multiple bands.
	/// </summary>
	/// <remarks>
	/// <seealso cref="ViewStyleBand"/>
	/// <seealso cref="UltraGridLayout.ViewStyle"/>
	/// <seealso cref="UltraGridLayout.ViewStyleBand"/>
	/// </remarks>
	public enum ViewStyle
	{
		/// <summary>
		/// Single Band. The grid will display a flat recordset.
		/// </summary>
		SingleBand = 0,

		/// <summary>
		/// Multiple Band. The grid will display a hierarchical recordset.
		/// </summary>
		MultiBand = 1
	};

	#endregion // ViewStyle

	#region ViewStyleBand

	/// <summary>
	/// Used to specify whether bands will display horizontally or vertically.
	/// </summary>
	/// <remarks>
	/// <seealso cref="ViewStyle"/>
	/// <seealso cref="UltraGridLayout.ViewStyle"/>
	/// <seealso cref="UltraGridLayout.ViewStyleBand"/>
	/// </remarks>
	public enum ViewStyleBand
	{
		/// <summary>
		/// Vertical. Top of new rows appears under the parent, left of new rows appears indented.
		/// </summary>
		Vertical = 0,

		/// <summary>
		/// Horizontal. Top of new rows appears aligned with parent's top, left of new rows appears to right of parent.
		/// </summary>
		Horizontal = 1,

		/// <summary>
		/// Outlook group by view style
		/// </summary>
		OutlookGroupBy
	};

	#endregion // ViewStyleBand

	#region ViewStyleSpecialFlags

	[Flags] internal enum ViewStyleSpecialFlags
	{
		SkipTopBorder              = 0x01,
		SkipTopLevelHierarchyLines = 0x02,
		DrawLongParentConnector    = 0x04,
		DrawFocus                  = 0x08,
		Last                       = 0xFF
	};

	#endregion // ViewStyleSpecialFlags

	#region VisibleRowDimensions

	internal enum VisibleRowDimensions
	{
		HeaderArea          = 0,
		OutsideRow          = 1,
		InsideRowSelectors  = 2,
		InsideRowBorders    = 3
	};

	#endregion // VisibleRowDimensions

	#region VisibleRelation

	// JJD 1/21/02 - UWG526 - Added
	/// <summary>
	/// Used to specify the relationship between a column or group and another one in the same band in visible position order.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// The <b>VisibleRelation</b> enum is used for specifying which column to get
	/// in when calling Column's <see cref="UltraGridColumn.GetRelatedVisibleColumn"/>
	/// method.
	/// </p>
	/// <seealso cref="UltraGridColumn.GetRelatedVisibleColumn"/>
	/// <seealso cref="UltraGridGroup.GetRelatedVisibleGroup"/>
	/// </remarks>
	public enum VisibleRelation
	{
		/// <summary>
		/// First column or group in the band in visible position order.
		/// </summary>
		First = 0,

		/// <summary>
		/// Last column or group in the band in visible position order.
		/// </summary>
		Last = 1,

		/// <summary>
		/// Next column or group in the band in visible position order.
		/// </summary>
		Next = 2,

		/// <summary>
		/// Previous column or group in the band in visible position order.
		/// </summary>
		Previous = 3
	};

	#endregion // VisibleRelation

	#region FormulaRowIndexSource

	// SSP 10/17/04
	// Added FormulaRowIndexSource property on the override.
	//
	/// <summary>
	/// Enum used for specifying <see cref="UltraGridOverride.FormulaRowIndexSource"/> property.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// See Override's <see cref="UltraGridOverride.FormulaRowIndexSource"/> property for more information.
	/// </p>
	/// <seealso cref="UltraGridRow.Index"/>
	/// <seealso cref="UltraGridRow.VisibleIndex"/>
	/// <seealso cref="UltraGridRow.ListIndex"/>
	/// </remarks>
	public enum FormulaRowIndexSource
	{
		/// <summary>
		/// Default. Resolves to VisibleIndex.
		/// </summary>
		Default,

		/// <summary>
		/// Row's list index. This is the index associated with <see cref="UltraGridRow.ListIndex"/> property.
		/// </summary>
		ListIndex,

		/// <summary>
		/// Row's index in its parent rows collection. This is the index associated with <see cref="UltraGridRow.Index"/> property.
		/// </summary>
		RowIndex,

		/// <summary>
		/// Row's visible index in the parent rows collection. This is the index associated with <see cref="UltraGridRow.VisibleIndex"/> property.
		/// </summary>
		VisibleIndex
	}

	#endregion // FormulaRowIndexSource

	#region ComboSelectedItemChangeContext

	// MRS 10/28/04 - UWG2996
	internal enum ComboSelectedItemChangeContext
	{
		SelectedIndex,
		SelectedRow,
		Value,
		Text,
	
		// MRS 12/5/05 - BR08027
		// We need to distinguish between a ListChanged where we keep the 
		// Value and a ListChanged where we keep the text.
		//
//		//MRS 10/24/05 - BR06979
//		ListChanged,
		ListChangedValue,
		ListChangedText,
	}

	#endregion // ComboSelectedItemChangeContext

	// JAS 2005 v2 XSD Support
	//
	#region XsdConstraintFlags

	/// <summary>
	/// Used to specify the constraints found in an XSD file which should be applied to the bands and columns of the grid.
	/// </summary>
	[Flags]
	public enum XsdConstraintFlags
	{
		/// <summary>
		/// Specifies that all of the constraints should be applied.
		/// </summary>
		All = 0x7fffffff,

		/// <summary>
		/// Specifies that the DefaultValue constraint should be applied to columns.
		/// This corresponds to the 'default' attribute in XSD.
		/// </summary>
		DefaultValue = 1,

		/// <summary>
		/// Specifies that the Enumeration constraint should be applied to columns.
		/// This corresponds to the 'enumeration' facet in XSD.
		/// </summary>
		Enumeration = 2,

		/// <summary>
		/// Specifies that the FixedValue constraint should be applied to columns.
		/// This corresponds to the 'fixed' attribute in XSD.
		/// </summary>
		FixedValue = 4,

		/// <summary>
		/// Specifies that the implicit constraints imposed by the built-in XSD base type should be enforced.
		/// For example, if the base XSD type is 'positiveInteger' then the cell value must be greater than zero.
		/// </summary>
		ImplicitXsdBaseTypeConstraints = 8,

		/// <summary>
		/// Specifies that the MaxExclusive constraint should be applied to columns.
		/// This corresponds to the 'maxExclusive' facet in XSD.
		/// </summary>
		MaxExclusive = 16,

		/// <summary>
		/// Specifies that the MaxInclusive constraint should be applied to columns.
		/// This corresponds to the 'maxInclusive' facet in XSD.
		/// </summary>
		MaxInclusive = 32,

		/// <summary>
		/// Specifies that the MaxLength constraint should be applied to columns.
		/// This corresponds to the 'maxLength' facet in XSD.
		/// </summary>
		MaxLength = 64,

		/// <summary>
		/// Specifies that the MaxRows constraint should be applied to bands.
		/// This corresponds to the 'maxOccurs' attribute in XSD.
		/// </summary>
		MaxRows = 128,

		/// <summary>
		/// Specifies that the MinExclusive constraint should be applied to columns.
		/// This corresponds to the 'minExclusive' facet in XSD.
		/// </summary>
		MinExclusive = 256,

		/// <summary>
		/// Specifies that the MinInclusive constraint should be applied to columns.
		/// This corresponds to the 'minInclusive' facet in XSD.
		/// </summary>
		MinInclusive = 512,

		/// <summary>
		/// Specifies that the MinLength constraint should be applied to columns.
		/// This corresponds to the 'minLength' facet in XSD.
		/// </summary>
		MinLength = 1024,

		/// <summary>
		/// Specifies that the MinRows constraint should be applied to bands.
		/// This corresponds to the 'minOccurs' attribute in XSD.
		/// </summary>
		MinRows = 2048,

		/// <summary>
		/// Specifies that the Nullable constraint should be applied to columns.
		/// This corresponds to the 'nillable' attribute in XSD.
		/// </summary>
		Nullable = 4096,

		/// <summary>
		/// Specifies that the RegexPattern constraint should be applied to columns.
		/// This corresponds to the 'pattern' facet in XSD.
		/// </summary>
		RegexPattern = 8192,
	}

	#endregion // XsdConstraintFlags

	// MRS 5/24/05 - BR04224
	#region RowLayoutItemResizeType
	/// <summary>
	/// Indicates the type of resize taking place during the resizing or Span resizing of a Layout Item.
	/// </summary>
	public enum RowLayoutItemResizeType
	{
		/// <summary>
		/// The Size of the item is changing
		/// </summary>
		SizeChange = 0, 

		/// <summary>
		/// The SpanX of the item is changing
		/// </summary>
		SpanXChange = 1,

		/// <summary>
		/// The SpanY of the item is changing
		/// </summary>
		SpanYChange = 2,

		/// <summary>
		/// The LabelSpan of the item is changing
		/// </summary>
		LabelSpanChange = 3,
	}
	#endregion RowLayoutItemResizeType

	//MRS 4/18/06 - BR11468
	#region DropDownSearchMethod
	/// <summary>
	/// Indicates the method used to search an UltraDropDown or UltraCombo Control when searching for a DisplayText or DataValue. 
	/// </summary>
	public enum DropDownSearchMethod
	{
		/// <summary>
		/// Use the default search method (Binary).
		/// </summary>
		Default = 0,

		/// <summary>
		/// Use a linear search. Linear searches are slower than binary, but there is less of an initial delay when loading the control with data. 
		/// </summary>
		Linear = 1,

		/// <summary>
		/// Use a Binary search. Binary searches are faster than Linear searches, but require an initial performance hit to build and maintain a sorted list of data. 
		/// </summary>
		Binary = 2
	}
	#endregion DropDownSearchMethod

	//MRS 4/18/06 - BR11468
	#region EditAreaDisplayStyle
	/// <summary>
	/// Determines whether the <see cref="UltraCombo"/> displays the image of the selected item in the edit portion of the control. 
	/// </summary>
	public enum EditAreaDisplayStyle
	{
		/// <summary>
		/// Default (DisplayTextAndPicture).
		/// </summary>
		Default = 0,

		/// <summary>
		/// Show the DisplayText and the picture, if there is one. 
		/// </summary>
		DisplayTextAndPicture = 1,		

		/// <summary>
		/// Show only the DisplayText. Do not display the image. 
		/// </summary>
		DisplayText = 2
	}
	#endregion EditAreaDisplayStyle

    // MBS 2/20/08 - RowEditTemplate NA2008 V2
    #region RowEditTemplateUIType

    /// <summary>
    /// Indicates when the grid will show the RowEditTemplate, if set.
    /// </summary>
    [Flags()]
    public enum RowEditTemplateUIType
    {
        /// <summary>
        /// The default action will be taken.
        /// </summary>
        Default = 0x1,

        /// <summary>
        /// The template will never be shown automatically.
        /// </summary>
        None = 0,

        /// <summary>
        /// The template will be shown when the row is double-clicked.
        /// </summary>
        OnDoubleClickRow = 0x2,

        /// <summary>
        /// The template will be shown when attempting to enter any cell on a row.
        /// </summary>
        OnEnterEditMode = 0x4,

        /// <summary>
        /// An image will be displayed in the row selectors, when visible, that will open the template when clicked.
        /// </summary>
        RowSelectorImage = 0x8,
    }
    #endregion //RowEditTemplateUIType
    //
    #region RowEditTemplateDisplayMode

    /// <summary>
    /// Enum for specifying how the RowEditTemplate should be displayed.
    /// </summary>
    /// <seealso cref="UltraGridRowEditTemplate"/>
    public enum RowEditTemplateDisplayMode
    {
        /// <summary>
        /// The template is shown as a modal dialog.  Any clicks outside of the bounds of the
        /// template are ignored and the template will retain focus.
        /// </summary>
        Modal = 0,

        /// <summary>
        /// The template is shown as a top-level window.  Clicks outside of the bounds of the template
        /// will not cause it to close, though it can lose focus.
        /// </summary>
        Modeless = 1,

        /// <summary>
        /// The template is shown as a popup.  Clicking outside of the bounds of the template will
        /// cause it to close.
        /// </summary>
        Popup = 2,
    }
    #endregion //RowEditTemplateDisplayMode

    // MRS NAS v8.2 - CardView Printing
    #region AllowCardPrinting

    /// <summary>
    /// Enum for specifying <see cref="UltraGridLayout.AllowCardPrinting"/> property.
    /// </summary>
    public enum AllowCardPrinting
    {
        /// <summary>
        /// UltraGrid will never print cards, it will print all bands in tabular view, regardless of the <see cref="UltraGridBand.CardView"/> property.
        /// </summary>
        Never = 0,

        /// <summary>
        /// UltraGrid will honor the <see cref="UltraGridBand.CardView"/> property on the root-level band when printing.
        /// </summary>
        RootBandOnly = 1
    }

    #endregion // AllowCardPrinting

	// MD 9/23/08 - TFS6601
	#region ReserveSortIndicatorSpaceWhenAutoSizing

	/// <summary>
	/// Represents the different ways to auto-size a column with regard to the space needed for the sort indicator.
	/// </summary>
	public enum ReserveSortIndicatorSpaceWhenAutoSizing
	{
		/// <summary>
		/// Use the default functionality.
		/// </summary>
		Default,

		/// <summary>
		/// Only reserve space for the sort indicator when it is visible in the column header.
		/// </summary>
		WhenVisible,

		/// <summary>
		/// Reserve space for the sort indicator when the column can be sorted.
		/// </summary>
		WhenColumnIsSortable,
	} 

	#endregion ReserveSortIndicatorSpaceWhenAutoSizing

    // CDS NAS v9.1 Header CheckBox
    #region HeaderCheckBoxVisibility

    /// <summary>
    /// Enum used for specifying when the UltraGridColumn's Header checkbox should should be displayed
    /// </summary>
    public enum HeaderCheckBoxVisibility
    {
        /// <summary>
        /// Default. The default value for the enumeration. Generally, properties with a value of "Default" should resolve to "Never".
        /// </summary>
        Default = 0,

        /// <summary>
        /// Never. The checkbox is never displayed in the column header.
        /// </summary>
        Never = 1,

        /// <summary>
        /// Always. The checkbox will always be displayed in the column header.
        /// </summary>
        Always = 2,

        // CDS 2/24/09 TFS12452 - Changed BasedOnDataType to WhenUsingCheckEditor, to be more descriptive
        ///// <summary>
        ///// BasedOnDataType. The checkbox will be displayed when the column's DataType is a type for which a checkbox is normally displayed in the UltraGridCell (Boolean, DefaultableBoolean, Nullable Boolean).
        ///// </summary>
        //BasedOnDataType = 3,
        /// <summary>
        /// WhenUsingCheckEditor. The checkbox will be displayed when the column's Editor resolves to an editor of type CheckEditor.
        /// </summary>
        WhenUsingCheckEditor = 3,
    };

    #endregion // HeaderCheckBoxVisibility

    // CDS NAS v9.1 Header CheckBox
    #region HeaderCheckBoxAlignment

    /// <summary>
    /// Enum used for specifying the position of the UltraGridColumn's Header checkbox relative to the caption
    /// </summary>
    public enum HeaderCheckBoxAlignment
    {
        /// <summary>
        /// Default. The default value for the enumeration. Generally, properties with a value of "Default" should resolve to "Left" or "Top" depending on the orientation of the header text. If no text is displayed, the checkbox should be centered.
        /// </summary>
        Default = 0,

        /// <summary>
        /// Left. The checkbox is displayed on the left side of the Header caption. Valid when the header text is displayed horizontally.
        /// </summary>
        Left = 1,

        /// <summary>
        /// Center. The checkbox is displayed in the center of the header when no caption is used.
        /// </summary>
        Center = 2,

        /// <summary>
        /// Right. The checkbox is displayed on the right side of the Header caption. Valid when the header text is displayed horizontally.
        /// </summary>
        Right = 3,

        /// <summary>
        /// Top. The checkbox is displayed on the top side of the Header caption. Valid when the header text is displayed vertically.
        /// </summary>
        Top = 4,

        /// <summary>
        /// Bottom. The checkbox is displayed on the bottom side of the Header caption. Valid when the header text is displayed vertically.
        /// </summary>
        Bottom = 5,
    };

    #endregion // HeaderCheckBoxAlignment

    // CDS NAS v9.1 Header CheckBox
    #region HeaderCheckBoxSynchronization

    /// <summary>
    /// Enum used for specifying how the UltraGridCell Values will be synchronized with UltraGridColumn's Header checkbox
    /// </summary>
    public enum HeaderCheckBoxSynchronization
    {
        /// <summary>
        /// Default. The default value for the enumeration. Generally, the behavior for properties with a value of "Default" will resolve differently based on the value of other properties (such as HeaderCheckBoxVisibility).
        /// </summary>
        Default = 0,

        /// <summary>
        /// None. The header checkbox is not synchronized with any cell values.
        /// </summary>
        None = 1,

        /// <summary>
        /// Band. The header checkbox and the cell values are kept in synch. The entire band is affected.
        /// </summary>
        Band = 2,

        /// <summary>
        /// RowsCollection. The header checkbox and the cell values are kept in synch. Only the RowsCollection (island) is affected.
        /// </summary>
        RowsCollection = 3,

    };

    #endregion // HeaderCheckBoxSynchronization

    // MRS - NAS 9.1 - Groups in RowLayout

    #region NAS 9.1 - Groups in RowLayout

    #region RowLayoutColumnInfoContext
    /// <summary>
    /// An enum which indicates the type of context for a RowLayoutColumnInfo object.
    /// </summary>
    public enum RowLayoutColumnInfoContext
    {
        /// <summary>
        /// The RowLayoutColumnInfo context is an UltraGridColumn
        /// </summary>
        Column,

        /// <summary>
        /// The RowLayoutColumnInfo context is an UltraGridGroup
        /// </summary>
        Group,
    }
    #endregion //RowLayoutColumnInfoContext

    #region RowLayoutStyle
    /// <summary>
    /// Specifies the way in which the band's columns are arranged. 
    /// </summary>
    public enum RowLayoutStyle
    {
        /// <summary>
        /// The columns will be arranged in the standard style with no GridBagLayout applied. 
        /// </summary>
        None,

        /// <summary>
        /// The columns will be arranged using a GridBagLayout without Groups. Groups will be ignored.
        /// </summary>
        ColumnLayout,

        /// <summary>
        /// The columns will be arranged using GridBagLayouts on the row and possibly with nested layouts for groups. 
        /// </summary>
        GroupLayout,
    }    
    #endregion //RowLayoutStyle

    #endregion // NAS 9.1 - Groups in RowLayout

    // MBS 1/14/09 - TFS12274
    #region FilterValueListOptions

    internal enum FilterValueListOptions
    {
        None,
        CellValuesOnly,
        FilterUIProvider
    }
    #endregion //FilterValueListOptions

    // MBS 6/19/09 - TFS18639
    #region FilterOperandDropDownItems

    /// <summary>
    /// An enumeration that indicates which operands should be shown in the filter dropdown menu.
    /// </summary>
    [Flags()]
    public enum FilterOperandDropDownItems
    {
        /// <summary>
        /// The grid will not populate the list with any items.
        /// </summary>
        ShowNone = 0,

        /// <summary>
        /// The default operands will be shown.
        /// </summary>
        Default = 0x1,

        /// <summary>
        /// Show cells with blank values, such as null, DBNull, and an empty string.
        /// </summary>
        Blanks = 0x2,

        /// <summary>
        /// Show cells that are not empty.
        /// </summary>
        NonBlanks = 0x4,

        /// <summary>
        /// Show cells that have an error, determined by the <see cref="System.ComponentModel.IDataErrorInfo"/> interface.
        /// </summary>
        Errors = 0x8,

        /// <summary>
        /// Show cells that do not have an error, determined by the <see cref="System.ComponentModel.IDataErrorInfo"/> interface.
        /// </summary>
        NonErrors = 0x10,

        /// <summary>
        /// Provide the option of displaying a filter dialog for the user to manually create a group of filter conditions.
        /// </summary>
        Custom = 0x20,

        /// <summary>
        /// The values of the cells in the associated column will be added to the list.
        /// </summary>
        CellValues = 0x40,

        /// <summary>
        /// The (All) option will be displayed in the list, when used with a <see cref="FilterUIType"/> of <b>HeaderIcons</b>.
        /// </summary>
        All = 0x80,

        /// <summary>
        /// The grid will populate the list with all options.
        /// </summary>
        ShowAll = Blanks | NonBlanks | Errors | NonErrors | Custom | CellValues | All
    }
    #endregion //FilterOperandDropDownItems

    // MBS 8/7/09 - TFS18607
    #region AddRowEditNotificationInterface

    /// <summary>
    /// Enum for specifying the <see cref="UltraGridOverride.AddRowEditNotificationInterface"/> property.
    /// </summary>
    /// <seealso cref="System.ComponentModel.ICancelAddNew"/>
    /// <seealso cref="System.ComponentModel.IEditableObject"/>
    public enum AddRowEditNotificationInterface
    {
        /// <summary>
        /// Use the default functionality.
        /// </summary>
        Default = 0,

        /// <summary>
        /// The <i>ICancelAddNew</i> implementation will be called, if available, followed by the 
        /// <i>IEditableObject</i> implementation.
        /// </summary>
        ICancelAddNewAndIEditableObject,

        /// <summary>
        /// The <i>ICancelAddNew</i> implementation will be called.  If this interface is not implemented, or the 
        /// row is not an AddRow, the <i>IEditableObject</i> implementation will be called.
        /// </summary>
        ICancelAddNewOrIEditableObject,

        /// <summary>
        /// The <i>IEditableObject</i> implementation will be called, if available, followed by the 
        /// <i>ICancelAddNew</i> implementation.
        /// </summary>
        IEditableObjectAndICancelAddNew,

        /// <summary>
        /// The <i>IEditableObject</i> implementation will be called, if available.  Otherwise, if the row is an
        /// AddRow, the <i>ICancelAddNew</i> implemenation will be called.
        /// </summary>
        IEditableObjectOrICancelAddNew,
    }
    #endregion //AddRowEditNotificationInterface
}
