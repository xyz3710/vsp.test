#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Diagnostics;
using Infragistics.Shared;
using Infragistics.Win;

namespace Infragistics.Win.UltraWinGrid
{
	// SSP 10/20/03 UWG2707 UWG2617
	// Added ColumnClipMode property to LogicalPageLayoutInfo class.
	//
	/// <summary>
	/// Enum for specifying <see cref="LogicalPageLayoutInfo.ColumnClipMode"/> property.
	/// </summary>
	public enum ColumnClipMode
	{
		/// <summary>
		/// Default is resolved to SplitClippedColumns in groups and row-layout mode, and in the regular mode (non-group and non-row-layout modes) it's resolved to RepeatClippedColumns.
		/// </summary>
		Default					= 0,

		/// <summary>
		/// When a column is clipped because there is not enough remaining space on the page, it will be repeated on the next page to ensure that it's fully visible. Columns that are wider than the width of a full page will not be repeated.
		/// </summary>
		RepeatClippedColumns	= 1,

		/// <summary>
		/// When a column is clipped because there is not enough remaining space on the page, it will be displayed on the next page starting from the point where it got clipped on the previous page.
		/// </summary>
		SplitClippedColumns		= 2
	}

	// SSP 6/8/06 BR13501
	// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
	// 
	/// <summary>
	/// Enum for specifying <see cref="LogicalPageLayoutInfo.ClipRowBehavior"/> property.
	/// </summary>
	public enum ClipRowBehavior
	{
		/// <summary>
		/// Default. Default is resolved to <b>PixelBased</b>.
		/// </summary>
		Default,

		/// <summary>
		/// When a row is taller than the page height, it will be split across pages on a pixel
		/// basis. It will start on the next page where it was left off on the previous page.
		/// </summary>
		PixelBased,

		/// <summary>
		/// When a row is taller than the page height, it will be split across pages on a cell 
		/// basis. It will start on the next page from the tallest of the cells that were clipped
		/// on the previous page. In other words, it will repeat the cells that were cut off on
		/// the previous page.
		/// </summary>
		CellBased,
	}

	/// <summary>
	/// Summary description for LogicalPageInfo.
	/// </summary>
	internal class LogicalPageInfo
	{
		private UltraGridRow firstRow = null;
		private LogicalPageLayoutInfo logicalPageLayoutInfo = null;

		// SSP 3/17/05 BR02832
		// If a row being printed is taller than the page height (not the same
		// thing as if it cross the bottom of the page) then start the row on
		// the next page at the point where we left off.
		//
		private int scrollOffset = 0;
		
		// SSP 3/17/05 BR02832
		// If a row being printed is taller than the page height (not the same
		// thing as if it cross the bottom of the page) then start the row on
		// the next page at the point where we left off.
		// Added scrollOffset parameter.
		//
		//internal LogicalPageInfo(UltraGridRow firstRow, LogicalPageLayoutInfo logicalPageLayoutInfo)
		internal LogicalPageInfo( UltraGridRow firstRow, LogicalPageLayoutInfo logicalPageLayoutInfo, int scrollOffset )
		{
			this.firstRow = firstRow;		
			this.logicalPageLayoutInfo = logicalPageLayoutInfo.Copy();

			// SSP 3/17/05 BR02832
			// Related to above change.
			//
			this.scrollOffset = scrollOffset;
		}

		internal UltraGridRow FirstRow
		{
			get
			{
				return this.firstRow;
			}
		}

		internal LogicalPageLayoutInfo LogicalPageLayoutInfo
		{
			get
			{
				return this.logicalPageLayoutInfo;
			}
		}

		// SSP 3/17/05 BR02832
		// If a row being printed is taller than the page height (not the same
		// thing as if it cross the bottom of the page) then start the row on
		// the next page at the point where we left off.
		//
		internal int ScrollOffset
		{
			get
			{
				return this.scrollOffset;
			}
		}
	}

	/// <summary>
	/// Summary description for LogicalPageLayoutInfo.
	/// </summary>
	public class LogicalPageLayoutInfo
	{
		#region Member Variables

		private UIElementBorderStyle pageHeaderBorderStyle = UIElementBorderStyle.Default;
		private UIElementBorderStyle pageFooterBorderStyle = UIElementBorderStyle.Default;
		
		private string pageFooter = null;
		private string pageHeader = null;
		
		private Infragistics.Win.Appearance	pageFooterAppearance = null;
		private Infragistics.Win.Appearance	pageHeaderAppearance = null;

		private int pageFooterHeight = -1;
		private int pageHeaderHeight = -1;

		private UltraGrid grid = null;

		private int fitWidthToPages = 0;

		private bool inInitializePrintPageEvent = false;
		private bool hasAppearanceChanged = false;
		private ClippingOverride clippingOverride;

		// SSP 10/20/03 UWG2707 UWG2617
		// Added ColumnClipMode proeprty to LogicalPageLayoutInfo class.
		//
		private ColumnClipMode columnClipMode = ColumnClipMode.Default;

		// SSP 6/8/06 BR13501
		// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
		// 
		private ClipRowBehavior clipRowBehavior = ClipRowBehavior.Default;

		#endregion //Member Variables

		
		internal LogicalPageLayoutInfo( UltraGrid grid )
		{
			this.grid = grid;			
		}

		internal LogicalPageLayoutInfo Copy()
		{
			LogicalPageLayoutInfo newlpli = new LogicalPageLayoutInfo( this.grid );
			
			newlpli.pageHeaderBorderStyle = this.PageHeaderBorderStyle;
			newlpli.pageFooterBorderStyle = this.PageFooterBorderStyle;
				
		
			newlpli.pageFooter = this.PageFooter;
			newlpli.pageHeader = this.PageHeader;
		
			// AS 1/28/05 Optimization
			// Don't force the appearances to be created and copied if there was nothing set
			// or the appearance was not created.
			if (this.HasPageFooterAppearanceBeenModified)
				newlpli.pageFooterAppearance = (Appearance)this.PageFooterAppearance.Clone();

			// AS 1/28/05 Optimization
			// Don't force the appearances to be created and copied if there was nothing set
			// or the appearance was not created.
			if (this.HasPageHeaderAppearanceBeenModified)
				newlpli.pageHeaderAppearance = (Appearance)this.PageHeaderAppearance.Clone();

			newlpli.pageFooterHeight = this.PageFooterHeight;
			newlpli.pageHeaderHeight = this.PageHeaderHeight;	

			newlpli.fitWidthToPages = this.fitWidthToPages;

			newlpli.clippingOverride = this.clippingOverride;

			// SSP 10/20/03 UWG2707 UWG2617
			// Added ColumnClipMode proeprty to LogicalPageLayoutInfo class.
			//
			newlpli.columnClipMode = this.columnClipMode;

			// SSP 6/8/06 BR13501
			// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
			// 
			newlpli.clipRowBehavior = this.clipRowBehavior;

			//don't copy inInitializePrintPageEvent because we don't want it to
			//carry over to newlpli LogicalPageLayoutInfo
			//

			return newlpli;
		}

		// SSP 10/20/03 UWG2707 UWG2617
		// Added ColumnClipMode proeprty to LogicalPageLayoutInfo class.
		//
		/// <summary>
		/// Specifies the action to take when a column gets clipped due to lack of enough space on the page to fully display the column.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnClipMode"/>
		/// </remarks>
		public ColumnClipMode ColumnClipMode
		{
			get
			{
				return this.columnClipMode;
			}
			set
			{
				if ( this.columnClipMode != value )
				{
					if ( ! Enum.IsDefined( typeof( ColumnClipMode ), value ) )
						throw new InvalidEnumArgumentException( "ColumnClipMode", (int)value, typeof( ColumnClipMode ) );

					this.columnClipMode = value;
				}
			}
		}

		// SSP 6/8/06 BR13501
		// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
		// 
		/// <summary>
		/// Specifies the action to take when a column gets clipped due to lack of enough space on the page to fully display the column.
		/// </summary>
		/// <remarks>
		/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnClipMode"/>
		/// </remarks>
		public ClipRowBehavior ClipRowBehavior
		{
			get
			{
				return this.clipRowBehavior;
			}
			set
			{
				if ( this.clipRowBehavior != value )
				{
					GridUtils.ValidateEnum( typeof( ClipRowBehavior ), value );

					this.clipRowBehavior = value;
				}
			}
		}

		internal bool InInitializePrintPageEvent
		{
			set
			{
				if ( value != this.inInitializePrintPageEvent )
					this.inInitializePrintPageEvent = value;
			}
		}

		internal bool HasAppearanceChanged
		{
			get
			{
				return this.hasAppearanceChanged;
			}			
		}



		/// <summary>
		/// Returns or sets a value that specifies the maximum number of sheets of paper that will be used to print a single logical page of the report.
		/// </summary>
		/// <remarks>
		/// <p class="body">When you print a report using UltraWinGrid, you may find that the data from the grid does not easily fit onto a single sheet of paper. Although this is usually because there are too many rows to fit vertically, it is also possible that your data consists of too many columns to fit horizonatally. For this reason, the control must sometimes make a distinction between a single "logical" page and the "physical" page (or sheets of paper) that may be required to print it. Essentially, logical pages break only on row boundaries. If you print a report with enough columns to fill the widths of three sheets of paper, the first logical page will comprise three physical pages.</p>
		/// <p class="body">The <b>FitWidthToPages</b> property limits the number of physical pages that a report may span. The default value for this property is 0, which indicates that the report may span as many physical pages are required to print all of the columns. If you set this property to a non-zero value, the control will scale the output so that the columns in the report will fit onto the specified number of pages. Note that scaling is proportional; if the data is reduced in width to fit, it will also be reduced in height, resulting in smaller print and more rows on each page.</p>
		/// </remarks>
		public int FitWidthToPages
		{
			get
			{
				return this.fitWidthToPages;
			}

			set
			{
				if ( this.inInitializePrintPageEvent )
					throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_190") );
				
				else if ( this.fitWidthToPages != value )
					this.fitWidthToPages = value;
			}
		}

		/// <summary>
		/// Returns or sets a value that determines whether to use extended clipping.
		/// </summary>
		/// <remarks>
		/// <p class="body">There are inconsistencies in the way certain printer drivers implement the printing APIs that UltraWinGrid uses to create its reports. In most instances, the control can detect the presence of these drivers and compensate automatically.</p>
		/// <p class="body">Some printer drivers handle the clipping of text regions inconsistently. Without compensation, reports printed using these drivers may have text that overlaps multiple cells, or letters may extend below the grid lines dividing headers from rows, or rows from rows. Text wrapping may also be affected.</p>
		/// <p class="body">Note that, while rows can automatically resize themselves vertically to accommodate larger font sizes, column and group headers cannot. If the text in a column or group header is being clipped, you may simply need to resize the header so that all the text is showing. However, under normal circumstances the header text should never extend past the borders of the header, either vertically or horizontally.</p>
		/// <p class="body">The <b>ClippingOverride</b> property gives you the ability to determine how UltraWinGrid will handle the detection and correction of printer driver clipping inconsistencies. The default setting, ClippingOverrideAuto, causes the control to automatically detect whether the current driver requires compensation, and apply it if it does. This setting should work in most cases, and should not be changed unless you absolutely have to.</p>
		/// <p class="body">The other settings of <b>ClippingOverride</b> give you manual control over whether clipping compensation is applied. Setting the property to ClippingOverrideYes will apply text clipping compensation even if the control determines the driver does not require it. A setting of ClippingOverrideNo will turn off compensation, even if the control determines that it is necessary.</p>
		/// <p class="body">You may find that you only need to apply correction to certain versions of a particular printer driver. You can use the properties of the <b>PrintDocument</b> object to determine which printer dirver is being used to print the report and what the version of that driver is.</p>
		/// <p class="note"><b>Note</b> An incorrect setting for the <b>ClippingOverride</b> property may produce unpredictable results when printing reports. You may see text overlapping the lines in the grid, text extending outside the cell that contains it, or you may see a gap between the edge of the printed text and the edge of the cell. If you experience problems such as these, first try setting <b>ClippingOverride</b> to its default setting. If you find these types of problems occurring when using the default value, try setting the property to one of the other values.</p>
		/// <p class="body">You should also note that problems of this nature may stem from problems completely external to UltraWinGrid and your application, such as insufficient printer memory. When attempting to troubleshoot printing problems, make sure your printer settings are correct and try using different driver settings (such as printing TrueType fonts as graphics or printing at a lower resolution) before changing the value of this property.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.ClippingOverride ClippingOverride
		{
			get
			{
				return this.clippingOverride;
			}

			set
			{
				if ( this.inInitializePrintPageEvent )
					throw new ArgumentException( Shared.SR.GetString("LE_ArgumentException_191") );

				else if ( !Enum.IsDefined( typeof(Infragistics.Win.UltraWinGrid.ClippingOverride), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_192") );

				this.clippingOverride = value; 
			}
				
		}
		
		/// <summary>
		/// Returns or sets a value that determines the printed border style of the page header.
		/// </summary>
		/// <remarks>
		/// <p class="body">The border styles available for report headers are the same as those available for other types of UltraWinGrid objects, such as cells, rows and column headers. If you choose the default setting, the page header will be drawn without borders.</p>
		/// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
		/// </remarks>
		public UIElementBorderStyle PageHeaderBorderStyle
		{
			get
			{							
				 if ( this.pageHeaderBorderStyle == UIElementBorderStyle.Default )
					return UIElementBorderStyle.None;
				else
					return this.pageHeaderBorderStyle;				
			}
	
			set
			{
				if ( value != this.pageHeaderBorderStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UIElementBorderStyle), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_163") );

					this.pageHeaderBorderStyle = value;					
				}
			}
		}

		/// <summary>
		/// Returns or sets a value that determines the printed border style of the page footer. 
		/// </summary>
		/// <remarks>
		/// <p class="body">The border styles available for report footers are the same as those available for other types of UltraWinGrid objects, such as cells, rows and column headers. If you choose the default setting, the page footer will be drawn without borders.</p>
		/// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
		/// </remarks>
		public UIElementBorderStyle PageFooterBorderStyle
		{
			get
			{							
				 if ( this.pageFooterBorderStyle == UIElementBorderStyle.Default )
					return UIElementBorderStyle.None;
				else
					return this.pageFooterBorderStyle;				
			}	

			set
			{
				if ( value != this.pageFooterBorderStyle )
				{
					// test that the value is in range
					//
					if ( !Enum.IsDefined( typeof(Infragistics.Win.UIElementBorderStyle), value ) )
						throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_163") );

					this.pageFooterBorderStyle = value;					
				}
			}
		}

		/// <summary>
		/// Returns or sets the text that will be printed at the bottom of each page.
		/// </summary>
		/// <remarks>
		/// <p class="body">The text you specify for the page footer will appear at the bottom of each physical page. If you want to make changes to the text of the footer, you can do so in the <b>InitializeLogicalPrintPage</b> event handler. This will change the footer for the logical page only. (<i>See the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeLogicalPrintPage"/> event topic for a description of the difference between logical and physical pages.</i>)</p>
		/// <p class="body">You can insert the page number in to the text of your page footer by using a substitution code. The code will be replaced with the physical page number of the page being printed. To insert the physical page number into the page footer, add the following substitution code to the text string you assign to the <b>PageFooter</b> property: &lt;#&gt;</p>
		/// <p class="body">You can choose to include the physical page number, the logical page number or some combination of the two on each page. To insert the physical page number on each page, simply include the substitution code in the text of your page footer. To insert the logical page number, you can intitalize a logical page counter variable in the <b>IntializePrint</b> event, then use the <b>InitializeLogicalPrintPage</b> event to increment the counter variable and change the text of the page footer to include the new logical page count value.</p>
		/// <p class="body">You can justify individual sections of the page footer by specifying a tab-delimited string for the <b>PageFooter</b> property. Text specified will be left-aligned until a tab character is encountered. Text following the first tab character that comes before the second tab character will be centered. Text following the second tab character will be right-aligned. For example, you could right align the entire page footer by beginning the text string with two tab characters.</p> 
		/// <p class="body">Including a tab character in the footer text will override any alignments specified for the footer. If no tab characters are included in the text, the default alignment will be used (as determined by the settings of the Appearance object returned by <b>PageFooterAppearance</b>.)</p>
		/// </remarks>
		public string PageFooter
		{
			get
			{
				return this.pageFooter;
			}

			set
			{
				if ( this.pageFooter != value )
					this.pageFooter = value;
			}
		}

		/// <summary>
		/// Returns or sets the text that will be printed at the top of each page.
		/// </summary>
		/// <remarks>
		/// <p class="body">The text you specify for the page header will appear at the top of each physical page. If you want to make changes to the text of the header, you can do so in the <b>InitializeLogicalPrintPage</b> event. This will change the header for the logical page only. (<i>See the <see cref="Infragistics.Win.UltraWinGrid.UltraGrid.InitializeLogicalPrintPage"/> event topic for a description of the difference between logical and physical pages.</i>)</p>
		/// <p class="body">You can insert the page number in to the text of your page header by using a substitution code. The code will be replaced with the physical page number of the page being printed. To insert the physical page number into the page header, add the following substitution code to the text string you assign to the <b>PageHeader</b> property: &lt;#&gt;</p>
		/// <p class="body">You can choose to include the physical page number, the logical page number or some combination of the two on each page. To insert the physical page number on each page, simply include the substitution code in the text of your page header. To insert the logical page number, you can intitalize a logical page counter variable in the <b>IntializePrint</b> event, then use the <b>InitializeLogicalPrintPage</b> event to increment the counter variable and change the text of the page header to include the new logical page count value.</p>
		/// <p class="body">You can justify individual sections of the page header by specifying a tab-delimited string for the <b>PageHeader</b> property. Text specified will be left-aligned until a tab character is encountered. Text following the first tab character that comes before the second tab character will be centered. Text following the second tab character will be right-aligned. For example, you could right align the entire page header by beginning the text string with two tab characters.</p> 
		/// <p class="body">Including a tab character in the header text will override any alignments specified for the header. If no tab characters are included in the text, the default alignment will be used (as determined by the settings of the Appearance object returned by <b>PageHeaderAppearance</b>.)</p>
		/// </remarks>
		public string PageHeader
		{
			get
			{
				return this.pageHeader;
			}

			set
			{
				if ( this.pageHeader != value )
					this.pageHeader = value;
			}
		}

		/// <summary>
		/// Returns or sets a value that specifies the height of the page footer.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PageHeaderHeight</b> property determines the amount of space reserved on each page of the report for the page header information. The default value for this property is -1, which causes the control to allocate space for the header based on the size of the text specified in the <b>PageHeader</b> property.</p>
		/// </remarks>
		public int PageFooterHeight
		{
			get
			{
				return this.pageFooterHeight;
			}

			set
			{
				if ( this.pageFooterHeight != value )
					this.pageFooterHeight = value;
			}
		}

		/// <summary>
		/// Returns or sets a value that specifies the height of the page header.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PageFooterHeight</b> property determines the amount of space reserved on each page of the report for the page header information. The default value for this property is -1, which causes the control to allocate space for the footer based on the size of the text specified in the <b>PageFooter</b> property.</p>
		/// </remarks>
		public int PageHeaderHeight
		{
			get
			{
				return this.pageHeaderHeight;
			}

			set
			{
				if ( this.pageHeaderHeight != value )
					this.pageHeaderHeight = value;
			}
		}

		/// <summary>
		/// Returns or sets the Appearance object that controls the formatting of page footer.
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PageFooterAppearance</b> property provides access to the Appearance object being used to control the formatting of the text that appears at the bottom of the printout. The Appearance object has properties that control settings such as color, font, etc. For more information on how to use properties that end in "Appearance", consult the topic for the <b>Appearance</b> property.</p>
		/// </remarks>
		public Infragistics.Win.Appearance PageFooterAppearance
		{
			get
			{
				if ( null == this.pageFooterAppearance )
				{
				    this.pageFooterAppearance = new Infragistics.Win.Appearance();

					// hook up the prop change notifications
					//
					this.pageFooterAppearance.SubObjectPropChanged += new SubObjectPropChangeEventHandler( this.ObjectPropChange );
				
				}					

				return this.pageFooterAppearance;
			}

			set
			{
				if ( this.pageFooterAppearance != value )
					this.pageFooterAppearance = value;
			}
		}

		// AS 1/28/05 Optimization
		internal bool HasPageFooterAppearanceBeenModified
		{
			get
			{
				return this.pageFooterAppearance != null && this.pageFooterAppearance.HavePropertiesBeenSet(AppearancePropFlags.AllRenderAndCursor);
			}
		}

		/// <summary>
		/// Page Header's appearance
		/// </summary>
		/// <remarks>
		/// <p class="body">The <b>PageHeaderAppearance</b> property provides access to the Appearance object being used to control the formatting of the text that appears at the top of the printout. The Appearance object has properties that control settings such as color, font, etc. For more information on how to use properties that end in "Appearance", consult the topic for the <b>Appearance</b> property.</p>
		/// </remarks>
		public Infragistics.Win.Appearance PageHeaderAppearance
		{
			get
			{
				if ( null == this.pageHeaderAppearance )
				{
					this.pageHeaderAppearance = new Infragistics.Win.Appearance();
				
					// hook up the prop change notifications
					//
					this.PageHeaderAppearance.SubObjectPropChanged += new SubObjectPropChangeEventHandler( this.ObjectPropChange );
				
				}

				return this.pageHeaderAppearance;
			}

			set
			{
				if ( this.pageHeaderAppearance != value )
					this.pageHeaderAppearance = value;
			}
		}

		// AS 1/28/05 Optimization
		internal bool HasPageHeaderAppearanceBeenModified
		{
			get
			{
				return this.pageHeaderAppearance != null && this.pageHeaderAppearance.HavePropertiesBeenSet(AppearancePropFlags.AllRenderAndCursor);
			}
		}

		internal void Reset()
		{
			this.pageHeaderBorderStyle = UIElementBorderStyle.Default;
			this.pageFooterBorderStyle = UIElementBorderStyle.Default;
		
			this.pageFooter = null;
			this.pageHeader = null;
		
			this.pageFooterAppearance = null;
			this.pageHeaderAppearance = null;

			this.pageFooterHeight = -1;
			this.pageHeaderHeight = -1;	
		
			this.fitWidthToPages = 0;

			this.inInitializePrintPageEvent = false;
			this.hasAppearanceChanged = false;
			this.clippingOverride = ClippingOverride.Auto;

			// SSP 10/20/03 UWG2707 UWG2617
			// Added ColumnClipMode proeprty to LogicalPageLayoutInfo class.
			//
            this.columnClipMode = ColumnClipMode.Default;

			// SSP 6/8/06 BR13501
			// Added ClipRowBehavior property to LogicalPageLayoutInfo class.
			// 
			this.clipRowBehavior = ClipRowBehavior.Default;
		}

		private void ObjectPropChange ( PropChangeInfo propChange ) 
		{
			this.hasAppearanceChanged = true;
		}
	}
}
