#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.ComponentModel.Design;
	using System.Windows.Forms;
    using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;


    /// <summary>
    /// Collection of all VisibleHeaders within a specific
    /// Header scrolling region.
    /// </summary>
    public class VisibleHeadersCollection : SubObjectsCollectionBase
    {
        private bool    dirty = true;
        private ColScrollRegion 
                        colScrollRegion;
        private Stack   visibleHeaderCache = null;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="colScrollRegion">col scroll region this collection is associated with</param>
        public VisibleHeadersCollection( ColScrollRegion colScrollRegion )
        {
            this.colScrollRegion = colScrollRegion;
        }

		/// <summary>
		/// returns true if the collection is read-only
		/// </summary>
        public override bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// The column scroll region that owns this visible Headers
        /// collection
        /// </summary>
        public ColScrollRegion ColScrollRegion
        {
            get
            {
                return this.colScrollRegion;
            }
        }
 
		/// <summary>
		/// indexer
		/// </summary>
        public Infragistics.Win.UltraWinGrid.VisibleHeader this[ int index ]
        {
            get
            {
                return (VisibleHeader)this.GetItem( index );
            }
        }

        /// <summary>
        /// Specifies the initial capacity of the collection
        /// </summary>
        [ DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden ) ]
        protected override int InitialCapacity
        {
            get
            {
                return 30;
            }
        }

        /// <summary>
        /// Returns true if the visible Headers collection needs to
        /// be refreshed
        /// </summary>
        public bool Dirty
        {
            get 
            {
                return this.dirty;
            }
            set
            {
                this.dirty = value;
            }
        }

        /// <summary>
        /// Stack of visibleHeaders that we cache for later re-use
        /// </summary>
        protected Stack VisibleHeaderCache
        {
            get
            {
                if ( null == this.visibleHeaderCache )
                    this.visibleHeaderCache = new Stack(100);

                return this.visibleHeaderCache;
            }
        }

		internal VisibleHeader GetVisibleHeaderFromCache( )
        {
            // look in cache for an alrady allocated VisibleHeader
            // object to re-use. If the cache is empty then creat
            // a new instance
            //
            if ( this.VisibleHeaderCache.Count > 0 )
                return (VisibleHeader)VisibleHeaderCache.Pop();
            else
                return new VisibleHeader( this.ColScrollRegion );
        }

		// SSP 11/23/04 - Optimization
		// Added AddVisibleHeaderFromCache method so we don't duplicate the code.
		//
		internal VisibleHeader AddVisibleHeaderFromCache( HeaderBase header, int origin )
		{
			VisibleHeader vh = this.GetVisibleHeaderFromCache( );
			vh.Initialize( header, origin );
			this.InternalAdd( vh );
			return vh;
		}

		internal int InternalAdd( VisibleHeader visibleHeader ) 
        {
            return this.List.Add( visibleHeader );
        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Internal method called by the viewstyle logic
		//        /// to insert a visibleHeader
		//        /// </summary>
		//        /// <param name="index">zero based index to insert at</param>
		//        /// <param name="visibleHeader"></param>
		//#endif
		//        internal void InternalInsert( int index, VisibleHeader visibleHeader ) 
		//        {
		//            base.InternalInsert( index, visibleHeader );
		//        }

		#endregion Not Used

        /// <summary>
        /// Pushes the VisibleHeader onto the stack of visibleHeaders that
        /// we cache for later re-use
        /// </summary>
        /// <param name="visibleHeader">The visible header to cache.</param>
        protected void CacheVisibleHeaderForRecycling( VisibleHeader visibleHeader ) 
        {
            this.VisibleHeaderCache.Push( visibleHeader );

            // clear the Header reference
            //
            visibleHeader.Initialize( null, 0 );

        }

		// MD 7/26/07 - 7.3 Performance
		// FxCop - Avoid uncalled private code
		#region Not Used

		//#if DEBUG
		//        /// <summary>
		//        /// Internal method cal by the viewstyle logic
		//        /// to remove a specific visible Header
		//        /// </summary>
		//#endif
		//        internal void InternalRemove( VisibleHeader visibleHeader ) 
		//        {
		//            // first cache the Header for re-use later
		//            //
		//            this.CacheVisibleHeaderForRecycling( visibleHeader );

		//            this.List.Remove( visibleHeader );
		//        }

		#endregion Not Used

		internal new void InternalClear() 
        {
            // first cache each Header for re-use later
            //
            foreach( VisibleHeader vr in this )
                this.CacheVisibleHeaderForRecycling( vr );

            this.List.Clear();
        }


		/// <summary>
		/// IEnumerable Interface Implementation
        /// </summary>
        /// <returns>A type safe enumerator</returns>
        public VisibleHeaderEnumerator GetEnumerator() // non-IEnumerable version
        {
           return new VisibleHeaderEnumerator(this);
        }

		// AS 1/8/03 - FxCop
		// Added strongly typed CopyTo method.
		#region CopyTo
		/// <summary>
		/// Copies the elements of the collection into the array.
		/// </summary>
		/// <param name="array">The array to copy to</param>
		/// <param name="index">The index to begin copying to.</param>
		public void CopyTo( Infragistics.Win.UltraWinGrid.VisibleHeader[] array, int index)
		{
			base.CopyTo( (System.Array)array, index );
		}
		#endregion //CopyTo

		/// <summary>
		/// Enumerator for a collection of VisibleHeaders
		/// </summary>
        public class VisibleHeaderEnumerator : DisposableObjectEnumeratorBase
        {   
			/// <summary>
			/// Constructor.
			/// </summary>
			/// <param name="visibleHeaders">The visible headers collection to enumerate.</param>
            public VisibleHeaderEnumerator(VisibleHeadersCollection visibleHeaders) : base( visibleHeaders )
            {
            }

			/// <summary>
			/// non-IEnumerator version: type-safe
			/// </summary>
            public VisibleHeader Current 
            {
                get
                {
                    return (VisibleHeader)((IEnumerator)this).Current;
                }
            }
        }

        // MRS 3/23/2009 - TFS14941
        #region Clone
        internal VisibleHeadersCollection Clone(ColScrollRegion owningColScrollRegion)
        {
            VisibleHeadersCollection clone = new VisibleHeadersCollection(owningColScrollRegion);
            foreach (VisibleHeader header in this)
            {
                VisibleHeader clonedHeader = new VisibleHeader(owningColScrollRegion);
                clonedHeader.Initialize(header.Header, header.Origin);
                clone.List.Add(clonedHeader);
            }

            return clone;
        }
        #endregion //Clone
    }
}
