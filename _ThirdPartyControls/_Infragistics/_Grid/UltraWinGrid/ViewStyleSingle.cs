#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	internal class ViewStyleSingle : ViewStyleBase
    {
        internal ViewStyleSingle( Infragistics.Win.UltraWinGrid.UltraGridLayout layout ) : base( layout )
        {
        }

        /// <summary>
        /// Does nothing since we're a single-band implementation. See 
        /// Multiband implementations for meaty code.
        /// </summary>
        protected override bool AddChildRows ( ref VisibleRowFetchRowContext context,
                                               VisibleRow parentVisibleRow,
                                               UltraGridBand  childBand )
        {
			if ( parentVisibleRow.Row is UltraGridGroupByRow )
				return base.AddChildRows( ref context, parentVisibleRow, null );

            return false;
        }

		
		

		internal override bool RecreateHeaderList( ColScrollRegion csr )
        {
            csr.VisibleHeaders.InternalClear();

            if ( csr.Layout.SortedBands.Count < 1 )
                return false;
            
            // verify that the band origin and extents have been
            // calculated properly
            //
 //           csr.Layout.Bands.VerifyBandOrigins();

            for ( int i = 0; i < csr.Layout.SortedBands.Count; i++ )
            {
                if ( ! csr.Layout.SortedBands[i].HiddenResolved )
                {
                    this.AddBandHeaders( csr, csr.Layout.SortedBands[i] ); 

					// SSP 8/17/04 - UltraCalc
					// Added OnAfterRowListCreated and OnAfterHeaderListCreated methods. Call
					// OnAfterHeaderListCreated after the visible headers are regenerated.
					//
					// ----------------------------------------------------------------------------
					// MD 7/26/07 - 7.3 Performance
					// OnAfterHeaderListCreated does nothing right now, uncomment if logic is added to OnAfterHeaderListCreated
					//this.OnAfterHeaderListCreated( csr );
					// ----------------------------------------------------------------------------

                    return true;
                }
            }

            return false;
        }
    }
}
