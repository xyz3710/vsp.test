#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Diagnostics;

	#region RowSelectorUIElementBase

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the RowSelectorUIElement into RowSelectorUIElementBase class and 
	// RowSelectorUIElement class so the filter row selector element can derive from 
	// RowUIElementBase.
	//
	/// <summary>
	/// The DataAreaUIElement contains the row and column scrolling
	/// regions.
	/// </summary>	
				     						// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
	public class RowSelectorUIElementBase :  HeaderUIElementBase
	{
		#region Private Vars

		// SSP 1/4/05 - IDataErrorInfo Support
		// Added rowIndicatorIconRect member which is set in the PositionChildElements 
		// method. This rect is the rect in which the any row indicator (active, add, 
		// modified row indicators) should be drawn in DrawImage. This rect is the rect
		// of the row selector exlcuding the date error icon.
		// 
		/// <summary>
		/// Rectangle where the row indicator is displayed (active/modified/add-row indicators).
		/// </summary>
		protected Rectangle rowIndicatorIconRect = Rectangle.Empty;

		#endregion // Private Vars

		#region Constructor
		
		internal RowSelectorUIElementBase( UIElement parent ) : base( parent, false, true )
		{
		}

		#endregion // Constructor

		#region PositionChildElements

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// Since a Row Selector element doesn't have any child elements
		/// PositionChildElements doesn't do anything
		/// </summary>
		protected override void PositionChildElements( )
		{
			Rectangle workRect = this.RectInsideBorders;
			UltraGridRow row = this.Row;
			UltraGridBand band = row.BandInternal;

			// Extract the existing elem if any.
			//
			DataErrorIconUIElement dataErrorIconElement = null;
			FixedRowIndicatorUIElement fixedRowIndicatorElement = null;
			TextUIElement rowNumberTextElement = null;
            RowSelectorEditTemplateUIElement editTemplateElement = null;
			if ( null != this.childElementsCollection )
			{
				dataErrorIconElement = (DataErrorIconUIElement)RowSelectorUIElement.ExtractExistingElement( 
					this.childElementsCollection, typeof( DataErrorIconUIElement ), true );

				fixedRowIndicatorElement = (FixedRowIndicatorUIElement)RowSelectorUIElement.ExtractExistingElement( 
					this.childElementsCollection, typeof( FixedRowIndicatorUIElement ), true );

				rowNumberTextElement = (TextUIElement)RowSelectorUIElement.ExtractExistingElement( 
					this.childElementsCollection, typeof( TextUIElement ), true );

                // MBS 2/28/08 - RowEditTemplate NA2008 V2
                editTemplateElement = (RowSelectorEditTemplateUIElement)RowSelectorUIElement.ExtractExistingElement(
                    this.childElementsCollection, typeof(RowSelectorEditTemplateUIElement), true);

				this.childElementsCollection.Clear( );
			}

			// SSP 3/29/05 - NAS 5.2 Fixed Rows
			//
			// --------------------------------------------------------------------
			if ( FixedRowIndicator.Button == row.FixedRowIndicatorResolved )
			{
				Rectangle fixedRowIndicatorRect = workRect;
				workRect.Width -= FixedRowIndicatorUIElement.FIXED_ROW_INDICATOR_WIDTH 
									+ 2 * FixedRowIndicatorUIElement.FIXED_ROW_INDICATOR_PADDING;
				fixedRowIndicatorRect.X = workRect.Right + FixedRowIndicatorUIElement.FIXED_ROW_INDICATOR_PADDING;
				fixedRowIndicatorRect.Width = FixedRowIndicatorUIElement.FIXED_ROW_INDICATOR_WIDTH;

				if ( null == fixedRowIndicatorElement )
					fixedRowIndicatorElement = new FixedRowIndicatorUIElement( this );
                
				fixedRowIndicatorElement.Rect = fixedRowIndicatorRect;
				this.ChildElements.Add( fixedRowIndicatorElement );
			}
			// --------------------------------------------------------------------

			// SSP 12/22/04 - IDataErrorInfo Support
			//
			// ------------------------------------------------------------------------
			bool hasDataErrorIcon = row.HasDataError( );
			if ( hasDataErrorIcon )
			{
				// Find out the rect of the data error icon.
				//
				AppearanceData dataErrorImageData;
				Image dataErrorImage = row.GetDataErrorImage( null, out dataErrorImageData );
				int imageElemWidth  = DataErrorIconUIElement.DEFAULT_WIDTH;				

				if ( null != dataErrorImage )
				{
					imageElemWidth = 2 * DataErrorIconUIElement.PADDING + (int)( dataErrorImage.Width * Math.Min( 1.0f, (float)( workRect.Height - 2 * DataErrorIconUIElement.PADDING ) / dataErrorImage.Height ) );
					imageElemWidth = Math.Min( imageElemWidth, Math.Max( workRect.Width / 3, DataErrorIconUIElement.DEFAULT_WIDTH ) );
					imageElemWidth = Math.Min( imageElemWidth, workRect.Width );
				}

				Rectangle dataErrorIconRect = workRect;
				dataErrorIconRect.Width = imageElemWidth;

				if ( HAlign.Right == dataErrorImageData.ImageHAlign 
					|| HAlign.Default == dataErrorImageData.ImageHAlign )
				{
					workRect.Width -= imageElemWidth;					
					dataErrorIconRect.X = workRect.Right;
				}
				else
				{
					workRect.X += imageElemWidth;
					workRect.Width -= imageElemWidth;
				}

				this.rowIndicatorIconRect = workRect;

				if ( null == dataErrorIconElement )
					dataErrorIconElement = new DataErrorIconUIElement( this );

				dataErrorIconElement.InitImageAppData( ref dataErrorImageData );
				dataErrorIconElement.Rect = dataErrorIconRect;
				this.ChildElements.Add( dataErrorIconElement );
			}
			// ------------------------------------------------------------------------

			// SSP 3/30/05 - NAS 5.2 Row Numbers
			//
			// --------------------------------------------------------------------
			int rowNumberWidth = band.GetRowSelectorNumberWidth( );
			if ( rowNumberWidth > 0 && row.IsDataRow )
			{
				int rowNumber = row.RowSelectorNumer;

				// SSP 5/6/05 BR03745
				// Use as much of the row selector width as possible for the row number.
				//
				rowNumberWidth = Math.Max( rowNumberWidth, workRect.Width - UltraGridBand.ROW_SELECTOR_ROW_ICON_WIDTH );

				Rectangle rowNumberRect = workRect;
				rowNumberRect.Width = rowNumberWidth;
				workRect.Width = Math.Max( 0, workRect.Right - rowNumberRect.Right );
				workRect.X = rowNumberRect.Right;

				if ( rowNumber >= 0 )
				{
					// SSP 8/5/05
					// Moved this into the  row's RowSelectorNumer property.
					// 
					//const int NUMBER_OFFSET = 1;
					//rowNumber += NUMBER_OFFSET;

					if ( null == rowNumberTextElement )
						rowNumberTextElement = new TextUIElement( this, rowNumber.ToString( ) );
					else
						rowNumberTextElement.Text = rowNumber.ToString( );

					rowNumberTextElement.Rect = rowNumberRect;
					this.ChildElements.Add( rowNumberTextElement );
				}
			}
			// --------------------------------------------------------------------

            // MBS 2/28/08 - RowEditTemplate NA2008 V2
            if ((band.RowEditTemplateUITypeResolved & RowEditTemplateUIType.RowSelectorImage) != 0)
            {
                Rectangle editTemplateRect = workRect;
                editTemplateRect.Width = RowSelectorEditTemplateUIElement.ICON_EXTENT;
                workRect.Width = Math.Max(0, workRect.Right - editTemplateRect.Right - RowSelectorEditTemplateUIElement.PADDING);
                workRect.X = editTemplateRect.Right + RowSelectorEditTemplateUIElement.PADDING;

                if (editTemplateElement == null)
                    editTemplateElement = new RowSelectorEditTemplateUIElement(this);

                editTemplateElement.Rect = editTemplateRect;
                this.ChildElements.Add(editTemplateElement);
            }

			this.rowIndicatorIconRect = workRect;
		}
 
		#endregion // PositionChildElements
		
		#region Row
		
		// SSP 4/30/05
		// Made this public.
		//
		/// <summary>
		/// The associated Row object (read-only).
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return (Infragistics.Win.UltraWinGrid.UltraGridRow)this.Parent.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridRow ) );
			}
		}

		#endregion // Row

		#region OnClick

		/// <summary>
		/// Sets active row to this row
		/// </summary>
		protected override void OnClick( )
		{
			// SSP 11/21/01 UWG508
			// If the grid does not have the focus, then do not change the active
			// row because there could be a situation where some other control
			// is forcibly retaining the focus (like masked edit because it has
			// invalid input ) and the user clicks the row selectors, we shouln't
			// change the active row.
			//
			UltraGrid grid = this.Row.Layout.Grid as UltraGrid;
			if ( null != grid && !grid.HasFocus )
			{
				return;
			}


			// set the active row to this row when they click on the row
			// selector
			//
			this.Row.Band.Layout.Grid.ActiveRow = this.Row;
			this.Row.SetFocusAndActivate();
		}

		#endregion // OnClick

		#region InitAppearance 

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
			ref AppearancePropFlags requestedProps )
		{
			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Honor the colors set on the header appearances.
			// 
			UltraGridRow row = this.Row;
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingRow parameter.
			// 
			//row.ResolveRowSelectorAppearance( ref appearance, ref requestedProps, false );
			bool isRowHotTracking = this.IsHotTracking;
			row.ResolveRowSelectorAppearance( ref appearance, ref requestedProps, false, isRowHotTracking );

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
			// Give the base class the first chance to set some properties.
			//
			base.InitAppearance( ref appearance, ref requestedProps );

			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Honor the colors set on the header appearances.
			// 
			//this.Row.ResolveRowSelectorAppearance( ref appearance, requestedProps );
			// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
			// Added hotTrackingRow parameter.
			// 
			//row.ResolveRowSelectorAppearance( ref appearance, ref requestedProps, true );
			row.ResolveRowSelectorAppearance( ref appearance, ref requestedProps, true, isRowHotTracking );
		}

		#endregion // InitAppearance 
 
		#region InternalInitAppearance

		// SSP 8/12/05 BR05338
		// 
		internal void InternalInitAppearance( ref AppearanceData appData, ref AppearancePropFlags flags )
		{
			this.InitAppearance( ref appData, ref flags );
		}

		#endregion // InternalInitAppearance

		#region IsHotTracking
		
		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// 
		internal bool IsHotTracking
		{
			get
			{
				return GridUtils.IsRowHotTracking( this );
			}
		}

		#endregion // IsHotTracking

		#region BorderStyle

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <para>The border style of cells, rows, and headers can be set by the BorderStyleCell, BorderStyleRow, and BorderStyleHeader properties respectively.</para>
        /// <para>The border style of the AddNew box buttons can be set by the ButtonBorderStyle property.</para>
        /// <para>Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</para>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				// SSP 3/7/03 - Row Layout Functionality
				// Use the new BorderStyleRowSelector property.
				//
				//return this.Row.Band.BorderStyleHeaderResolved;
				return this.Row.BandInternal.BorderStyleRowSelectorResolved;
			}
		}

		#endregion // BorderStyle

		#region BorderSides

		/// <summary>
		/// Overrides the BorderSides to return the BorderSides from the UIElement
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return Border3DSide.All;
			}
		}

		#endregion // BorderSides

		#region Adjustable

		/// <summary>
		/// Returns true row can be resized (using row selector area).
		/// </summary>
		/// <returns></returns>
		public override bool Adjustable
		{
			get
			{
				return !this.Row.IsFixedHeight && 
					this.Row.Band.RowSizingAreaResolved != RowSizingArea.RowBordersOnly;

			}
		}

		#endregion // Adjustable

		#region GetAdjustableCursor

        /// <summary>
        /// Returns the cursor to use over the adjustable area of the element.
        /// </summary>
        /// <param name="point">The point that should be used to determine if the area is adjustable.</param>
        /// <returns>The cursor that should be used to represent an adjustable region.</returns>
        public override System.Windows.Forms.Cursor GetAdjustableCursor(System.Drawing.Point point)
		{
			if ( this.SupportsUpDownAdjustmentsFromPoint( point ) )
			{
				// SSP 11/26/03 UWG2344
				// Added PerformAutoSize to the UltraGridRow and perform auto size functionality for rows.
				//
				// ----------------------------------------------------------------------
				if ( this.SupportsAutoSizing )
				{
					System.Windows.Forms.Cursor cursor = this.Row.Band.Layout.RowAutoSizeCursor;

					if ( null != cursor )
						return cursor;
				}
				// ----------------------------------------------------------------------

				return System.Windows.Forms.Cursors.SizeNS;
			}
			
			return null;
		}

		#endregion // GetAdjustableCursor

		#region GetRowLayoutAdjustmentItem

		// SSP 8/29/05 BR05818
		// In row-layout mode if vertical resizing of cells is allowed then when the
		// user resizes the row selector, treat it as if a cell aligned with the bottom
		// edge of the row were resized. Resizing the row (setting the row's Height)
		// has the effect of resizing all the items proporationally. This poses the
		// following problem. When the user resizes the row and makes it bigger, it's
		// height is increased. Then when the user resizes a cell smaller, the row's
		// height remains the same. This may result in a no-op visually. 
		// 
		private bool RowLayoutAdjustHelper( Point delta )
		{
			Infragistics.Win.Layout.ILayoutItem layoutItem;
			UIElementAdjustmentRangeParams range = new UIElementAdjustmentRangeParams( );
			return this.RowLayoutAdjustHelper( true, delta, out layoutItem, ref range );
		}

		private bool RowLayoutAdjustHelper( ref UIElementAdjustmentRangeParams range )
		{
			Infragistics.Win.Layout.ILayoutItem layoutItem;
			return this.RowLayoutAdjustHelper( false, Point.Empty, out layoutItem, ref range );
		}

		private bool RowLayoutAdjustHelper( bool applyAdjustment, Point delta, 
			out Infragistics.Win.Layout.ILayoutItem layoutItem, ref UIElementAdjustmentRangeParams range )
		{
			layoutItem = null;
			UltraGridRow row = this.Row;
			if ( null != row && row.Band.UseRowLayoutResolved )
			{
				RowLayoutColumnInfo maxCI = null;

				foreach ( UltraGridColumn column in row.Band.Columns )
				{
					RowLayoutColumnInfo ci = column.RowLayoutColumnInfo;
					if ( column.IsVisibleInLayout 
						&& ( RowLayoutSizing.Vertical == ci.AllowCellSizingResolved 
						|| RowLayoutSizing.Both == ci.AllowCellSizingResolved ) )
					{
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Remove unused locals
						//int bottomLogicalRow = ci.OriginY + ci.SpanY;

						if ( null == maxCI || ci.OriginY + ci.SpanY > maxCI.OriginY + maxCI.SpanY )
							maxCI = ci;
					}
				}

				if ( null != maxCI && null != maxCI.Column )
				{
					layoutItem = maxCI.Column;
					RowCellAreaUIElementBase rowCellArea = null != this.Parent 
						? (RowCellAreaUIElementBase)this.Parent.GetDescendant( typeof( RowCellAreaUIElementBase ) )
						: null;
					Size containerSize = null != rowCellArea ? rowCellArea.GetLayoutContainerSize( ) : Size.Empty;

					if ( applyAdjustment )
					{
						row.Band.ResizeLayoutItem( rowCellArea, layoutItem, containerSize, delta );
					}
					else
					{
						int minWidth = 0, maxWidth = 0, minHeight = 0, maxHeight = 0;
						Size currSize;
						row.Band.GetLayoutItemResizeRange( rowCellArea, layoutItem, containerSize, ref minWidth, ref maxWidth, ref minHeight, ref maxHeight, out currSize );
						
						if ( maxHeight > 0 )
							range.maxDeltaDown = Math.Max( 0, maxHeight - currSize.Height );

						range.maxDeltaUp = Math.Min( 0, minHeight - currSize.Height );
					}

					return true;
				}
			}

			return false;
		}

		#endregion // GetRowLayoutAdjustmentItem

		#region GetAdjustmentRange 

		/// <summary>
		/// Returns the range limits for adjusting the element in either or both
		/// dimensions. It also returns the initial rects for the vertical and horizontal
		/// bars that will need to be inverted during the mouse drag operation.
		/// </summary>
		/// <param name="point">point</param>
		/// <param name="range">Returned limits</param>
		public override void GetAdjustmentRange ( System.Drawing.Point point,
			out UIElementAdjustmentRangeParams range )
		{
			base.GetAdjustmentRange( point, out range );

			if ( !this.SupportsUpDownAdjustmentsFromPoint( point ) )
				return;

			UltraGridRow  row  = this.Row;
			UltraGridBand band = this.Row.Band;

			Debug.Assert( null != row, "No band object in CUIElementRowSelector.GetAdjustmentRange");
			Debug.Assert( null != band, "No row object in CUIElementRowSelector.GetAdjustmentRange");

			if ( null != row && null != band )
			{
				if ( band.IsRowSizingFixed )
					return;

				Rectangle dataAreaRect   = this.GetAncestor( typeof ( DataAreaUIElement ) ).Rect;
				Rectangle metaRgionRect  = this.GetAncestor( typeof ( RowColRegionIntersectionUIElement ) ).Rect;

				// SSP 8/29/05 BR05818
				// In row-layout mode if vertical resizing of cells is allowed then when the
				// user resizes the row selector, treat it as if a cell aligned with the bottom
				// edge of the row were resized. Resizing the row (setting the row's Height)
				// has the effect of resizing all the items proporationally. This poses the
				// following problem. When the user resizes the row and makes it bigger, it's
				// height is increased. Then when the user resizes a cell smaller, the row's
				// height remains the same. This may result in a no-op visually. 
				// Added call to RowLayoutAdjustHelper and enclosed the code into the if block.
				// 
				if ( ! this.RowLayoutAdjustHelper( ref range ) )
				{        
					// get the band's minimum row height
					//
					int minRowHeight = row.MinRowHeight;
 
					if ( this.Rect.Height > minRowHeight )
					{
						range.maxDeltaUp = - System.Math.Min( metaRgionRect.Bottom - this.Rect.Top,
							this.Rect.Height - minRowHeight );
					} 
					else
					{
						range.maxDeltaUp = 0;
					}
				}

				range.maxDeltaDown = System.Math.Max( 0, metaRgionRect.Bottom - this.Rect.Bottom - 1 );	

				// set up the horz bar from left to right of the meta region
				range.upDownAdjustmentBar.X = dataAreaRect.Left;
				range.upDownAdjustmentBar.Width = dataAreaRect.Width;
			}
		}

		#endregion // GetAdjustmentRange 

		#region ApplyAdjustment

        /// <summary>
        /// Called after a move/resize operation. 
        /// </summary>
        /// <param name="delta">The delta</param>
        public override void ApplyAdjustment(Point delta)
		{	
			UltraGridRow row = this.Row;

			Debug.Assert( null != row, "Not row found in RowSelectorUIElement.ApplyAdjustment" );

			if ( 0 != delta.Y && null != row )
			{
				// SSP 8/29/05 BR05818
				// In row-layout mode if vertical resizing of cells is allowed then when the
				// user resizes the row selector, treat it as if a cell aligned with the bottom
				// edge of the row were resized. Resizing the row (setting the row's Height)
				// has the effect of resizing all the items proporationally. This poses the
				// following problem. When the user resizes the row and makes it bigger, it's
				// height is increased. Then when the user resizes a cell smaller, the row's
				// height remains the same. This may result in a no-op visually. 
				// 
				// --------------------------------------------------------------------------
				if ( this.RowLayoutAdjustHelper( delta ) )
					return;
				// --------------------------------------------------------------------------

				int newHeight = row.BaseHeight + delta.Y;

				// SSP 8/23/01 UWG218
				// Before resizing the row, we have to exit the edit mode so the text box
				// gets hidden
				if ( null != this.Row.Layout.ActiveCell &&
					this.Row.Layout.ActiveCell.IsInEditMode )
				{
					//RobA UWG418 10/5/01 Return if we did not exit edit mode
					//
					if ( this.Row.Layout.ActiveCell.ExitEditMode( ) )
						return;				
				}

				// tell the band that the row needs to be resized to the new height				
				row.Band.ResizeRow( row, newHeight, true );

				RowScrollRegion rsr = (RowScrollRegion)this.Parent.GetContext( typeof( RowScrollRegion ), true );
				Debug.Assert( null != rsr, "null RowScrollRegion" ); 
				if ( null != rsr )
					rsr.ScrollRowIntoView( row );
			}
		}

		#endregion // ApplyAdjustment

		// SSP 11/26/03 UWG2344
		// Added PerformAutoSize to the UltraGridRow and perform auto size functionality for rows.
		//
		#region Code for Row Auto-Size functionality

		#region SupportsAutoSizing

		private bool SupportsAutoSizing
		{
			get
			{
				return null != this.Row && this.Adjustable &&
					( RowSizing.Free == this.Row.Band.RowSizingResolved ||
					RowSizing.AutoFree == this.Row.Band.RowSizingResolved );
			}
		}

		#endregion // SupportsAutoSizing

		#region OnDoubleClick

        /// <summary>
        /// Called when the mouse is double clicked on this element.
        /// </summary>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        protected override void OnDoubleClick(bool adjustableArea)
		{
			if ( adjustableArea && this.SupportsAutoSizing )
			{
				this.Row.PerformAutoSize( );
				return;
			}

			base.OnDoubleClick( adjustableArea );
		}

		#endregion // OnDoubleClick

		#endregion // Code for Row Auto-Size functionality

		// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
		//
		#region Header/RowSelector Styles

			#region ActiveThemeMouseTracking

		/// <summary>
		/// Indicates if the element supports hot tracking over the header.
		/// </summary>
		protected override bool ActiveThemeMouseTracking
		{
			get
			{
                // MRS NAS v8.1 - Windows Vista Style for UltraWinTree
                //// Return true only if the HeaderStyle supports hottracking.
                //// Before this override was added, the base's default value of 'false' was always used.
                ////
                //return	this.HeaderStyle == HeaderStyle.WindowsXPCommand ||
                //        this.HeaderStyle == HeaderStyle.XPThemed;
                switch (this.HeaderStyle)
                {
                    case HeaderStyle.WindowsXPCommand:
                    case HeaderStyle.XPThemed:
                    case HeaderStyle.WindowsVista:
                        return true;
                    case HeaderStyle.Standard:
                        return false;
                    case HeaderStyle.Default:
                    default:
                        Debug.Fail("Unknown HeaderStyle");
                        return false;
                } 
			}
		}

			#endregion // ActiveThemeMouseTracking

			#region DrawTheme

        /// <summary>
        /// Renders the element using the System theme.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
        /// <returns>True if the element was able to be rendered using the system themes.</returns>
		protected override bool DrawTheme(ref Infragistics.Win.UIElementDrawParams drawParams)
		{
			// If the base implementation returns true, then we need to manually invoke the DrawImage draw phase
			// which would otherwise be skipped by UIElement.DrawElement() when themes are used.  Note, we only
			// execute this special logic if the row selectors are successfully rendered via the OS themeing API.
			//
			bool styleIsThemed = this.HeaderStyle == HeaderStyle.XPThemed;
			if( styleIsThemed && base.DrawTheme( ref drawParams ) )
			{
				this.DrawImage( ref drawParams );
				return true;
			}
			else
				return false;
		}

			#endregion // DrawTheme

			#region HeaderState

		/// <summary>
		/// Indicates the state of the header.
		/// </summary>
		protected override UIElementButtonState HeaderState
		{
			get
			{
				// This provides the "pressed" look for a row selector when the row is selected.
				//
				if( this.Row.Selected || this.HasCapture )
					return UIElementButtonState.MouseDownAndOver;

				return base.HeaderState;
			}
		}

			#endregion // HeaderState

			#region HeaderStyle

		/// <summary>
		/// Returns the HeaderStyle that this row selector should use.
		/// </summary>
		protected override HeaderStyle HeaderStyle
		{
			get
			{
				// SSP 5/20/05 - Optimizations
				// Don't call Row multiple times. Also no need to check for row/band 
				// being null since we don't do it in other places, ie in the get of 
				// the BorderStyle, InitAppearance etc...
				//
				
				return this.Row.BandInternal.RowSelectorStyleResolved;
			}
		}

			#endregion // HeaderStyle

			#region RequiresSeparator

		/// <summary>
		/// Returns a boolean indicating if the item requires the rendering a separator.
		/// </summary>
		protected override bool RequiresSeparator
		{
			// For hottracking to be rendered when using the XPThemed style, the element must have a separator.
			// We only want the separator to exist when hottracking.  At all other times, no separator is needed.
			//
			get { return (this.HeaderState & UIElementButtonState.MouseOver) != 0;  }
		}

			#endregion // RequiresSeparator

		#endregion // Header/RowSelector Styles

		#region Offset

		// SSP 5/18/05 - Optimizations
		// Overrode Offset so we can offset the rowIndicatorIconRect as well.
		//
		/// <summary>
		/// Overridden. Offsets this element's rect and (optionally) all of its descendant elements.
		/// </summary>
		/// <param name="deltaX">The number of pixels to offset left/right</param>
		/// <param name="deltaY">The number of pixels to offset up/down </param>
		/// <param name="recursive">If true will offset all descendant elements as well</param>
		public override void Offset( int deltaX, int deltaY, bool recursive )
		{
			if ( ! this.rowIndicatorIconRect.IsEmpty )
				this.rowIndicatorIconRect.Offset( deltaX, deltaY );

			base.Offset( deltaX, deltaY, recursive );
		}

		#endregion // Offset

		// SSP 8/8/05 - NAS 5.3 Row, Cell and Header Hot Tracking Appearances
		// Overrode OnMouseEnter and OnMouseLeave so we can invalidate the row if there
		// are hot tracking appearances.
		// 
		#region NAS 5.3 Row, Cell and Header Hot Tracking Appearances

		#region OnMouseEnterLeave

		private void OnMouseEnterLeave( bool enter )
		{
			RowCellAreaUIElementBase.OnMouseEnterLeaveHelper( this, enter );
		}

		#endregion // OnMouseEnterLeave

		#region OnMouseEnter

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseEnter( )
		{
			this.OnMouseEnterLeave( true );

			base.OnMouseEnter( );
		}

		#endregion // OnMouseEnter

		#region OnMouseLeave

		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void OnMouseLeave( )
		{
			this.OnMouseEnterLeave( false );

			base.OnMouseLeave( );
		}

		#endregion // OnMouseLeave

		#endregion // NAS 5.3 Row, Cell and Header Hot Tracking Appearances

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Row.BandInternal, StyleUtils.Role.RowSelector );
			}
		}

		#endregion //UIRole
	}
     #endregion // RowSelectorUIElementBase

    #region RowSelectorUIElement

	// SSP 3/15/05 - NAS 5.2 Fixed Rows/Filter Row/Outlook AddRow/Summaries Extention
	// Split the RowSelectorUIElement into RowSelectorUIElementBase class and 
	// RowSelectorUIElement class so the filter row selector element can derive from 
	// RowUIElementBase.
	//
    /// <summary>
    /// The DataAreaUIElement contains the row and column scrolling
    /// regions.
    /// </summary>
    public class RowSelectorUIElement : RowSelectorUIElementBase
    {
		#region Constructor
		
		internal RowSelectorUIElement( UIElement parent ) : base( parent )
        {
        }

		#endregion // Constructor

		#region Row

		/// <summary>
		/// The associated Row object (read-only)
		/// </summary>
		public new Infragistics.Win.UltraWinGrid.UltraGridRow Row
		{
			get
			{
				return base.Row;
			}
		}

		#endregion // Row

		#region DrawImage 

		/// <summary>
		/// Draws the row selector icons (if any).
		/// </summary> 
		protected override void DrawImage ( ref UIElementDrawParams drawParams )
		{
			// SSP 2/25/02 UWG1031
			// Draw the image if set on the appearance and return
			// otherwise draw whatever icons needing to be drawn.
			//
			//---------------------------------------------------------------
			AppearanceData appData = new AppearanceData( );
			AppearancePropFlags flags = 
				AppearancePropFlags.ImageHAlign |
				AppearancePropFlags.ImageVAlign |
				AppearancePropFlags.Image;
			this.InitAppearance( ref appData, ref flags );

			if ( appData.HasPropertyBeenSet( AppearancePropFlags.Image ) )
			{
				Image image = appData.GetImage( this.Row.Layout.Grid.ImageList );
                
                // CDS 9.2 RowSelector State-Specific Images
                #region Moved to DrawUserSpecifiedImage

                //if ( null != image )
                //{
                //    // SSP 1/4/05 - IDataErrorInfo Support
                //    // Added rowIndicatorIconRect member which is set in the PositionChildElements 
                //    // method. This rect is the rect in which the any row indicator (active, add, 
                //    // modified row indicators) should be drawn in DrawImage. This rect is the rect
                //    // of the row selector exlcuding the date error icon.
                //    // 
                //    //Rectangle rect = this.RectInsideBorders;
                //    Rectangle rect = this.rowIndicatorIconRect;
                //    Size imageSize = image.Size;
					
                //    Rectangle drawRect = Rectangle.Empty;

                //    if ( imageSize.Width < rect.Width && imageSize.Height < rect.Height )
                //    {
                //        drawRect = new Rectangle( rect.Location, imageSize );
                //    }
                //    else
                //    {
                //        // SSP 8/23/02 UWG1204
                //        // Scale the image keeping the aspect ratio rather than scaling to the
                //        // row selector's dimensions.
                //        // --------------------------------------------------------------------
                //        //drawRect = rect;

                //        double xScaleFactor = (double)rect.Width / imageSize.Width;
                //        double yScaleFactor = (double)rect.Height / imageSize.Height;

                //        // Use the minimum of x and y scale factors.
                //        //
                //        double scaleFactor = Math.Min( xScaleFactor, yScaleFactor );

                //        drawRect = new Rectangle( rect.X, rect.Y,
                //            (int)( scaleFactor * imageSize.Width ), (int)( scaleFactor * imageSize.Height ) );
                //        // --------------------------------------------------------------------
                //    }

                //    switch ( appData.ImageHAlign )
                //    {						
                //        case Infragistics.Win.HAlign.Left:							
                //            break;
                //        case Infragistics.Win.HAlign.Right:
                //            drawRect.X = rect.Right - drawRect.Width;
                //            break;
                //        case Infragistics.Win.HAlign.Center:
                //        case Infragistics.Win.HAlign.Default:
                //        default:
                //            drawRect.X += ( rect.Width - drawRect.Width ) / 2;
                //            break;
                //    }
					
                //    switch ( appData.ImageVAlign )
                //    {						
                //        case Infragistics.Win.VAlign.Top:
                //            break;
                //        case Infragistics.Win.VAlign.Bottom:
                //            drawRect.Y = rect.Bottom - drawRect.Height;
                //            break;
                //        case Infragistics.Win.VAlign.Middle:
                //        case Infragistics.Win.VAlign.Default:
                //        default:
                //            drawRect.Y += ( rect.Height - drawRect.Height ) / 2;
                //            break;
                //    }


                //    // AS 12/19/02 UWG1831
                //    // Use the drawparam's draw image method
                //    //
                //    /*
                //    drawParams.Graphics.DrawImage(
                //        image,
                //        drawRect,
                //        new Rectangle( new Point( 0, 0 ), imageSize ),
                //        GraphicsUnit.Pixel );
                //    */
                //    drawParams.DrawImage(
                //        image,
                //        drawRect,
                //        new Rectangle( new Point( 0, 0 ), imageSize ),
                //        null );

                //    // If the appearance has image set on it, then skip
                //    // drawing of below icons.
                //    //
                //    return;
                //}

                #endregion Moved to DrawUserSpecifiedImage
                if (this.DrawUserSpecifiedImage(ref drawParams, image, appData))
                    return;

            }
			//-------------------------------------------------------------

			Infragistics.Win.UltraWinGrid.UltraGridRow row = this.Row;
			UltraGridLayout layout = row.Layout;
			UltraGrid grid = layout.Grid as UltraGrid;

			// SSP 5/7/03 - Export Functionality
			//
			//if ( grid == null || this.Row.Band.Layout.IsPrintLayout )
			if ( grid == null || layout.IsPrintLayout || layout.IsExportLayout )
				return;

			// If the grid is disabled don't bother showing the row selector icons.
			//
			// Note: if customer feedback demands that we show these icons when the grid
			//       is disabled we will have to add logic to create the disabled bitmaps
			//
			// SSP 5/7/03 - Optimizations
			//
			//if ( !row.Band.Layout.Grid.Enabled )
			if ( !grid.Enabled )
				return;


			System.Drawing.Icon icon = null;

			// SSP 4/1/05 - Optimizations
			// Use the IsActiveRowInternal which does not go through the process of verifying 
			// that the active row is valid.
			//
			bool isActiveRow = row.IsActiveRowInternal;

            // CDS 9.2 RowSelector State-Specific Images
            RowSelectorImageInfo.RowState rowState = RowSelectorImageInfo.RowState.Default;
            Image stateSpecificImage = null;
            // CDS 6/04/09 TFS18217 We want to colorize by default. Only turn off colorizing when an image is set. 
            //bool colorize = false;
            bool colorize = true;

            #region Replaced

            //// Simplified/consolidated the icon drawing logic below. Eliminated redundant
            //// call to DrawIconEx
            ////
            //// SSP 11/11/03 Add Row Feature
            //// If the row is a template add-row then draw the add-row icon.
            //// Added below if block.
            ////
            //if ( row.IsTemplateAddRow )
            //{
            //    icon = grid.RowAddedIcon;
            //}
            //else if ( row.DataChanged )
            //{
            //    // Added new icon for the active row when it is modified
            //    //
             
            //    // Added separate icons for the add row
            //    //
            //    // SSP 4/1/05 - Optimizations
            //    // Use the IsActiveRowInternal which does not go through the process of verifying 
            //    // that the active row is valid.
            //    //
            //    //if ( row == row.Band.Layout.Grid.ActiveRow )
            //    if ( isActiveRow )
            //    {
            //        if ( row.IsAddRow )
            //            icon = grid.ActiveRowAddedIcon;
            //        else
            //            icon = grid.ActiveRowModifiedIcon;
            //    }
            //    else
            //    {
            //        if ( row.IsAddRow )
            //            icon = grid.RowAddedIcon;
            //        else
            //            icon = grid.RowModifiedIcon;
            //    }
            //}
            //    // SSP 4/1/05 - Optimizations
            //    // Use the IsActiveRowInternal which does not go through the process of verifying 
            //    // that the active row is valid.
            //    //
            //    //else if ( row == grid.ActiveRow )
            //else if ( isActiveRow )
            //{
            //    icon = grid.ActiveRowIcon;
            //}

            #endregion Replaced

            //calculate the row state
            if (row.IsTemplateAddRow)
                rowState = RowSelectorImageInfo.RowState.AddNewRow;
            else
            {
                if (row.DataChanged)
                    rowState |= RowSelectorImageInfo.RowState.DataChanged;

                if (row.IsActiveRow)
                    rowState |= RowSelectorImageInfo.RowState.Active;

                if (row.IsAddRow)
                    rowState |= RowSelectorImageInfo.RowState.AddNewRow;
            }

            // assign the icon and stateSpecificImage based on the row state
            switch (rowState)
            {
                case RowSelectorImageInfo.RowState.Active:
                // this state can be hit as an active new row automatically is considered data changed.
                // but the original algorithm would use the ActiveRowIcon is it was possible.
                case RowSelectorImageInfo.RowState.ActiveAndAddNew:                     
                    if (layout.HasRowSelectorImages)
                    {
                        stateSpecificImage = layout.RowSelectorImages.ActiveRowImageResolved;
                        colorize = layout.RowSelectorImages.ShouldColorizeActiveRowImage;
                    }
                    else
                        icon = grid.ActiveRowIcon;
                    break;
                case RowSelectorImageInfo.RowState.AddNewRow:
                case RowSelectorImageInfo.RowState.AddNewRow | RowSelectorImageInfo.RowState.DataChanged:
                    if (layout.HasRowSelectorImages)
                    {
                        stateSpecificImage = layout.RowSelectorImages.AddNewRowImageResolved;
                        colorize = layout.RowSelectorImages.ShouldColorizeAddNewRowImage;
                    }
                    else
                        icon = grid.RowAddedIcon;
                    break;
                case RowSelectorImageInfo.RowState.DataChanged:
                    if (layout.HasRowSelectorImages)
                    {
                        stateSpecificImage = layout.RowSelectorImages.DataChangedImageResolved;
                        colorize = layout.RowSelectorImages.ShouldColorizeDataChangedImage;
                    }
                    else
                        icon = grid.RowModifiedIcon;
                    break;
                case RowSelectorImageInfo.RowState.ActiveAndAddNew | RowSelectorImageInfo.RowState.DataChanged:
                    if (layout.HasRowSelectorImages)
                    {
                        stateSpecificImage = layout.RowSelectorImages.ActiveAndAddNewRowImageResolved;
                        colorize = layout.RowSelectorImages.ShouldColorizeActiveAndAddNewRowImage;
                    }
                    else
                        icon = grid.ActiveRowAddedIcon;
                    break;
                case RowSelectorImageInfo.RowState.ActiveAndDataChanged:
                    if (layout.HasRowSelectorImages)
                    {
                        stateSpecificImage = layout.RowSelectorImages.ActiveAndDataChangedImageResolved;
                        colorize = layout.RowSelectorImages.ShouldColorizeActiveAndDataChangedImage;
                    }
                    else
                        icon = grid.ActiveRowModifiedIcon;
                    break;
            }

            // attempt to draw the state-specific image. If one has not been set, DrawUserSpecifiedImage will return false.
            if (!colorize)
            {
                this.DrawUserSpecifiedImage(ref drawParams, stateSpecificImage, appData);
                return;
            }
            else
            {
            // CDS 9.2 RowSelector State-Specific Images
            //if ( icon != null )
            //{
            //    // SSP 4/9/07 BR20818 - Optimizations
            //    // 
            //    // --------------------------------------------------------------------
            //    System.Drawing.Bitmap bitmap = grid.GetCachedBitmapFromIcon( icon, drawParams.AppearanceData.ForeColor );
                System.Drawing.Bitmap bitmap = null;
                if (stateSpecificImage != null)
                    bitmap = grid.GetCachedBitmapFromIcon(stateSpecificImage, drawParams.AppearanceData.ForeColor);
                else if (icon != null)
                    bitmap = grid.GetCachedBitmapFromIcon(icon, drawParams.AppearanceData.ForeColor);
                else
                    return;

				Size bmpSize = bitmap.Size;
				Rectangle thisRect = this.rowIndicatorIconRect;
				Point pt = new Point( thisRect.Left + ( ( thisRect.Width - bmpSize.Width ) / 2 ), thisRect.Top + ( ( thisRect.Height - bmpSize.Height ) / 2 ) );
				drawParams.DrawImage( bitmap, new Rectangle( pt, bmpSize ), new Rectangle( Point.Empty, bmpSize ), null );
				
				// --------------------------------------------------------------------
			}
		}

		#endregion // DrawImage 

        // CDS 9.2 RowSelector State-Specific Images
        #region DrawUserSpecifiedImage

        private bool DrawUserSpecifiedImage(ref UIElementDrawParams drawParams, Image image, AppearanceData appData)
        {
            if (null == image)
                return false;

            Rectangle rect = this.rowIndicatorIconRect;
            Size imageSize = image.Size;

            Rectangle drawRect = Rectangle.Empty;

            if (imageSize.Width < rect.Width && imageSize.Height < rect.Height)
            {
                drawRect = new Rectangle(rect.Location, imageSize);
            }
            else
            {

                double xScaleFactor = (double)rect.Width / imageSize.Width;
                double yScaleFactor = (double)rect.Height / imageSize.Height;

                // Use the minimum of x and y scale factors.
                //
                double scaleFactor = Math.Min(xScaleFactor, yScaleFactor);

                drawRect = new Rectangle(rect.X, rect.Y,
                    (int)(scaleFactor * imageSize.Width), (int)(scaleFactor * imageSize.Height));
                // --------------------------------------------------------------------
            }

            switch (appData.ImageHAlign)
            {
                case Infragistics.Win.HAlign.Left:
                    break;
                case Infragistics.Win.HAlign.Right:
                    drawRect.X = rect.Right - drawRect.Width;
                    break;
                case Infragistics.Win.HAlign.Center:
                case Infragistics.Win.HAlign.Default:
                default:
                    drawRect.X += (rect.Width - drawRect.Width) / 2;
                    break;
            }

            switch (appData.ImageVAlign)
            {
                case Infragistics.Win.VAlign.Top:
                    break;
                case Infragistics.Win.VAlign.Bottom:
                    drawRect.Y = rect.Bottom - drawRect.Height;
                    break;
                case Infragistics.Win.VAlign.Middle:
                case Infragistics.Win.VAlign.Default:
                default:
                    drawRect.Y += (rect.Height - drawRect.Height) / 2;
                    break;
            }

            drawParams.DrawImage(
                image,
                drawRect,
                new Rectangle(new Point(0, 0), imageSize),
                null);

            // If the appearance has image set on it, then skip
            // drawing of below icons.
            //
            return true;
        }

        #endregion DrawUserSpecifiedImage    
    }

     #endregion // RowSelectorUIElement

    //JDN 11/19/04 Added RowSelectorHeader
    #region RowSelectorHeaderUIElement

    /// <summary>
    /// RowSelectorHeaderUIElement
    /// </summary>
    public class RowSelectorHeaderUIElement : Infragistics.Win.HeaderUIElementBase
    {
		#region Private Vars

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		//
		private bool cachedHasColumnChooserButton = false;

		#endregion // Private Vars

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        // Does not allow horizontal or vertical bar adjustments
		// SSP 4/11/05
		//
		//public RowSelectorHeaderUIElement( UIElement parent, RowsCollection rowsCollection ) : base( parent, false, false )
		public RowSelectorHeaderUIElement( UIElement parent ) : base( parent, false, false )
        {
			// SSP 4/11/05
			//
            //this.InitializeRowSelectorHeader( rowsCollection );
        }
        #endregion // Constructor

        #region InitializeRowSelectorHeader
		// SSP 4/11/05
		// Commented out.
		//
		
        #endregion // InitializeRowSelectorHeader

        #region InitAppearance
        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance,
                                                 ref AppearancePropFlags requestedProps )
        {
			// SSP 7/20/05 - HeaderStyle of WindowsXPCommand
			// Honor the colors set on the header appearances.
			// 
			UltraGridBand band = this.Band;
			band.ResolveRowSelectorHeaderAppearance( ref appearance, ref requestedProps, true, false );

			// JAS v5.2 New Default Look & Feel For UltraGrid 4/11/05
			// Give the base class the first chance to set some properties.
			//
			base.InitAppearance( ref appearance, ref requestedProps );

			// SSP 7/20/05 - NAS 5.3 Column Chooser
			// Added defaultToHeaderAppearance parameter. Pass in true for that. Note that this
			// doesn't change the behavior.
			// 
            //this.Band.ResolveRowSelectorHeaderAppearance( ref appearance, ref requestedProps );
			band.ResolveRowSelectorHeaderAppearance( ref appearance, ref requestedProps, true, true );
        }
        #endregion // InitAppearance

        #region BorderStyle
        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>		
        public override UIElementBorderStyle BorderStyle
        {
            get 
			{ 
				// SSP 6/17/05 - NAS 5.3 Column Chooser
				// If the row selector header is displaying a filter clear button then don't draw 
				// any borders around the row selector header otherwise the button will get shrunk 
				// to a very small size.
				//
				if ( this.cachedHasColumnChooserButton )
                    return UIElementBorderStyle.None;

				return this.Band.BorderStyleHeaderResolved; 
			}
        }
        #endregion // BorderStyle

        #region BorderSides
        /// <summary>
        /// Overrides the BorderSides to return the BorderSides from the UIElement
        /// </summary>
        public override Border3DSide BorderSides
        {
            get { return Border3DSide.All; }
        }
        #endregion BorderSides

        #region RequiresSeparator
        /// <summary>
        /// Returns a boolean indicating if the item requires the rendering a separator.
        /// </summary>
        protected override bool RequiresSeparator
        {
            // return true to draw a seperator with themes
            get { return true; }
        }
        #endregion // RequiresSeparator

        #region RowsCollection
        /// <summary>
        /// Returns the RowsCollection object associated with the UIElement. This property is read-only at run-time. This property is not available at design-time.
        /// </summary>
        /// <remarks>
        /// <p class="body">The <b>RowsCollection</b> property of an object refers to a collection of Infragistics.Win.UltraWinGrid.UltraGridRow objects.</p>
        /// <p class="body">The UltraGridRow objects in a Rows Collection make up a data island. The top-level Rows of a grid are all part of a single data island.</p>
        /// <p class="body">If the grid is hierarchical, then one or more top-level UltraGridRow objects will have its own Rows Collection, which contains one or more Row objects.</p>
        /// <p class="body">This creates a tree structure of parent and child rows that is used to display hierarchical data.</p>                                                                                                                                                                                                                                                                     
        /// </remarks>
        public RowsCollection RowsCollection
        {
            get 
			{ 
				// SSP 4/11/05
				// Just use the GetContext method.
				//
				//return this.PrimaryContext as RowsCollection; 
				return (RowsCollection)this.GetContext( typeof( RowsCollection ), true );
			}
        }
        #endregion // RowsCollection

        #region Band
        // Returns the UltraGridBand object associated with the UIElement
        internal UltraGridBand Band
        {
            get 
			{ 
				// SSP 4/11/05
				// Just use the GetContext method.
				//
				//return this.RowsCollection.Band; 
				return (UltraGridBand)this.GetContext( typeof( UltraGridBand ), true );
			}
        }
        #endregion // Band

		#region HeaderStyle

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// Overrode HeaderStyle. If the row selector header is displaying a column chooser
		// button then don't draw any borders around the row selector header otherwise 
		// the button will get shrunk to a very small size.
		//
		/// <summary>
		/// Returns the HeaderStyle that this row selector should use.
		/// </summary>
		protected override HeaderStyle HeaderStyle
		{
			get
			{
				// If the row selector header is displaying a filter clear button then don't draw 
				// any borders around the row selector header otherwise the button will get shrunk 
				// to a very small size.
				//
				if ( this.cachedHasColumnChooserButton )
					return HeaderStyle.Standard;

				return this.Band.HeaderStyleResolved;
			}
		}

		#endregion // HeaderStyle

		#region PositionChildElements

		// SSP 6/17/05 - NAS 5.3 Column Chooser
		// Overrode PositionChildElements so we can position the column chooser button if
		// RowSelectorHeaderStyle is set to SeparateElement.
		// 
		/// <summary>
		/// Overridden.
		/// </summary>
		protected override void PositionChildElements( )
		{
			UltraGridBand band = this.Band;

			ColumnChooserButtonUIElement columnChooserButtonElem = 
				(ColumnChooserButtonUIElement)RowSelectorHeaderUIElement.ExtractExistingElement(
					this.childElementsCollection, typeof( ColumnChooserButtonUIElement ), true );

			if ( null != this.childElementsCollection )
				this.childElementsCollection.Clear( );
			
			this.cachedHasColumnChooserButton = false;

			RowSelectorHeaderStyle rowSelectorHeaderStyle = null != band 
				? band.RowSelectorHeaderStyleResolved : RowSelectorHeaderStyle.Default;

			// SSP 12/8/05 BR06669
			// Added row selector header style of ColumnChooserButtonFixedSize.
			// 
			//if ( null != band && RowSelectorHeaderStyle.ColumnChooserButton == band.RowSelectorHeaderStyleResolved )
			if ( RowSelectorHeaderStyle.ColumnChooserButton == rowSelectorHeaderStyle
				|| RowSelectorHeaderStyle.ColumnChooserButtonFixedSize == rowSelectorHeaderStyle )
			{
				this.cachedHasColumnChooserButton = true;

				if ( null == columnChooserButtonElem )
					columnChooserButtonElem = new ColumnChooserButtonUIElement( this );

				AppearancePropFlags flags = AppearancePropFlags.ImageHAlign | AppearancePropFlags.ImageVAlign;
				AppearanceData appData = new AppearanceData( );
				band.ResolveRowSelectorHeaderAppearance( ref appData, ref flags, false, false );

				Rectangle thisRect = this.RectInsideBorders;
				Rectangle rect;

				// SSP 12/8/05 BR06669
				// Added row selector header style of ColumnChooserButtonFixedSize.
				// 
				if ( RowSelectorHeaderStyle.ColumnChooserButtonFixedSize == rowSelectorHeaderStyle )
				{
					// Center the button within the area of the row selector header element that's
					// directly above the row selectors. Do so only if the ImageHAlign is Default.
					// Otherwise follow the ImageHAlign and ImageVAlign settings.
					// 
					Rectangle outerRect = thisRect;
					if ( HAlign.Default == appData.ImageHAlign )
					{
						outerRect.Width = Math.Min( outerRect.Width, Math.Max( band.RowSelectorExtent, ColumnChooserButtonUIElement.DEFAULT_COLUMN_CHOOSER_BUTTON_SIZE ) );
						outerRect.X += thisRect.Width - outerRect.Width;
						appData.ImageHAlign = HAlign.Center;
					}

					if ( VAlign.Default == appData.ImageVAlign )
						appData.ImageVAlign = VAlign.Middle;
				
					rect = outerRect;
					rect.Width = ColumnChooserButtonUIElement.DEFAULT_COLUMN_CHOOSER_BUTTON_SIZE;
					rect.Height = ColumnChooserButtonUIElement.DEFAULT_COLUMN_CHOOSER_BUTTON_SIZE;

					// Align the element rect according to the image alignment settings.
					// 
					Infragistics.Win.DrawUtility.AdjustHAlign( appData.ImageHAlign, ref rect, outerRect );
					Infragistics.Win.DrawUtility.AdjustVAlign( appData.ImageVAlign, ref rect, outerRect );
				}
				else
				{
					rect = thisRect;
				}
				
				columnChooserButtonElem.Rect = rect;
				
				this.ChildElements.Add( columnChooserButtonElem );
			}
		}

		#endregion // PositionChildElements

		#region UIRole

		// SSP 3/20/06 - App Styling
		// 
		/// <summary>
		/// Returns the <see cref="Infragistics.Win.AppStyling.UIRole"/> associated with this element.
		/// </summary>
		public override AppStyling.UIRole UIRole
		{
			get 
			{ 
				return StyleUtils.GetRole( this.Band, StyleUtils.Role.RowSelectorHeader );
			}
		}

		#endregion // UIRole
    }

    #endregion // RowSelectorHeaderUIElement

    // MBS 2/28/08 - RowEditTemplate NA2008 V2
    #region RowSelectorEditTemplateUIElement

    /// <summary>
    /// A UIElement used for showing the RowEditTemplate.
    /// </summary>
    public class RowSelectorEditTemplateUIElement : ImageAndTextUIElement
    {
        #region Members

        internal const int PADDING = 2;
        internal const int ICON_EXTENT = 10;

        #endregion //Members

        #region Constructor

        /// <summary>
        /// Instantiates a new instance of the element.
        /// </summary>
        /// <param name="parent">The parent <see cref="RowSelectorUIElementBase"/>.</param>
        public RowSelectorEditTemplateUIElement(RowSelectorUIElementBase parent)
            : base(parent)
        {
        }
        #endregion //Constructor

        #region Properties

        #region Row

        private UltraGridRow Row
        {
            get
            {
                RowSelectorUIElementBase selector = this.parentElement as RowSelectorUIElementBase;
                if (selector != null)
                    return selector.Row;

                return null;
            }
        }
        #endregion //Row

        #endregion //Properties

        #region Base Class Overrides

        #region DrawImage

        /// <summary>
        /// Draws the image for the element, if any.
        /// </summary>
        /// <param name="drawParams">The drawparams used for rendering the image.</param>
        protected override void DrawImage(ref UIElementDrawParams drawParams)
        {
            Image image = drawParams.AppearanceData.GetImage(null);
            if(image != null)
                drawParams.DrawImage(drawParams.AppearanceData.GetImage(null), drawParams.Element.Rect, false, null);
        }
        #endregion //DrawImage

        #region InitAppearance

        /// <summary>
        /// Initialize the appearance structure for this element
        /// </summary>
        /// <param name="appearance">The appearance structure to initialize</param>
        /// <param name="requestedProps">The properties that are needed</param>
        protected override void InitAppearance(ref AppearanceData appearance, ref AppearancePropFlags requestedProps)
        {
            if (!appearance.HasPropertyBeenSet(AppearancePropFlags.Image) &&
                (requestedProps & AppearancePropFlags.Image) != 0)
            {
                if (this.Row != null)
                {
                    appearance.Image = this.Row.Band.RowEditTemplateRowSelectorImageResolved;
                    requestedProps ^= AppearancePropFlags.Image;
                }
            }

            if (!appearance.HasPropertyBeenSet(AppearancePropFlags.ImageVAlign) &&
                (requestedProps & AppearancePropFlags.ImageVAlign) != 0)
            {
                appearance.ImageVAlign = VAlign.Middle;
                requestedProps ^= AppearancePropFlags.ImageVAlign;
            }

            if (!appearance.HasPropertyBeenSet(AppearancePropFlags.ImageHAlign) &&
                (requestedProps & AppearancePropFlags.ImageHAlign) != 0)
            {
                appearance.ImageHAlign = HAlign.Center;
                requestedProps ^= AppearancePropFlags.ImageHAlign;
            }

            base.InitAppearance(ref appearance, ref requestedProps);
        }
        #endregion //InitAppearance

        #region OnMouseDown

        /// <summary>
        /// Called when the mouse down message is received over the element. 
        /// </summary>
        /// <param name="e">Mouse event arguments</param>
        /// <param name="adjustableArea">True if left clicked over adjustable area of element.</param>
        /// <param name="captureMouseForElement">If not null on return will capture the mouse and forward all mouse messages to this element.</param>
        /// <returns>If true then bypass default processing</returns>
        protected override bool OnMouseDown(MouseEventArgs e, bool adjustableArea, ref UIElement captureMouseForElement)
        {
            bool ret = base.OnMouseDown(e, adjustableArea, ref captureMouseForElement);

            // We don't want to show the template unless the user simply clicks on it with the left mouse button
            Keys modifierKeys = System.Windows.Forms.Control.ModifierKeys;
            if (e.Button != MouseButtons.Left ||
                (modifierKeys & Keys.Control) == Keys.Control ||
                (modifierKeys & Keys.Shift) == Keys.Shift ||
                (modifierKeys & Keys.Alt) == Keys.Alt)
                return ret;

            UltraGridRow row = this.Row;
            if (row != null && row.RowEditTemplateResolved != null)
            {
                // We need to activate the row ourselves, since we have to take action before the
                // selection manager can capture the mouse, since we don't get an OnClick if it does
                if (row.IsActiveRow == false)
                    row.Activate();

                // If we were unable to activate the row, we should bail out
                if (row.IsActiveRow == false)
                    return ret;
                
                // If we can show the edit template, then we need to make sure that we return true, 
                // otherwise if we're showing the template as a dialog, the default processing will
                // cause the grid to capture focus in such a way that clicking the containing form's
                // caption will not allow you to drag it, close, etc.
                bool success = row.ShowEditTemplate(true, TemplateDisplaySource.RowSelectorImage);
                if (success)
                    return true;

                return ret;
            }

            return ret;
        }
        #endregion //OnMouseDown

        #endregion //Base Class Overrides
    }
    #endregion //RowSelectorEditTemplateUIElement
}
