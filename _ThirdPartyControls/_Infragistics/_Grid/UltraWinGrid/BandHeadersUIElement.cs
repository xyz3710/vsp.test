#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Drawing;
    using System.ComponentModel;
	using System.Windows.Forms;
	using Infragistics.Shared;
	using Infragistics.Win;

	// SSP 2/19/03 - Row Layout Functionality
	//
	using Infragistics.Win.Layout;
	using System.Diagnostics;
	using System.Collections.Generic;

    /// <summary>
    /// The DataAreaUIElement contains the row and column scrolling
    /// regions.
    /// </summary>
	public class BandHeadersUIElement : UIElement
	{
		// SSP 3/21/02
		// Version 2 Row Filter Implementation
		// Added context of rows collection to the band header element for
		// row filtering.
		//
		private RowsCollection rowsContext = null;

		// JJD 1/5/04
		// Added row context which is needed for accessibility
		private UltraGridRow rowContext = null;


		private bool forceAlignLeft = false;

		// SSP 5/19/03 - Fixed headers
		internal bool usingFixedHeaders = false;

		// SSP 6/5/03 - Optimizations
		// Cache the header border style.
		//
        internal UIElementBorderStyle headerBorderStyle = UIElementBorderStyle.None;

		internal BandHeadersUIElement( UIElement parent ) : base( parent )
		{
		}

		// AS 1/14/03 DNF32
		// We need a public uielement constructor whenever possible.
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="band">Associated <b>Band</b></param>
		/// <param name="forceAlignLeft">True to adjust based on the ColScrollRegion</param>
		/// <param name="rowsContext">Associated rows</param>
		public BandHeadersUIElement( UIElement parent, Infragistics.Win.UltraWinGrid.UltraGridBand band, bool forceAlignLeft, RowsCollection rowsContext ) : this( parent )
		{
			this.InitializeBand(band, forceAlignLeft, rowsContext, null);
		}

		// JJD 1/5/04
		// Added row context which is needed for accessibility
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="parent">Parent element</param>
		/// <param name="band">Associated <b>Band</b></param>
		/// <param name="forceAlignLeft">True to adjust based on the ColScrollRegion</param>
		/// <param name="rowsContext">Associated rows</param>
		/// <param name="rowContext">The row this header is attached to.</param>
		public BandHeadersUIElement( UIElement parent, Infragistics.Win.UltraWinGrid.UltraGridBand band, bool forceAlignLeft, RowsCollection rowsContext, UltraGridRow rowContext ) : this( parent )
		{
			this.InitializeBand(band, forceAlignLeft, rowsContext, rowContext);
		}


		// SSP 3/15/02
		// Added context of rows colleciton to the band for row filter feature.
		//
		//internal void InitializeBand( Infragistics.Win.UltraWinGrid.UltraGridBand band, bool forceAlignLeft )
		// JJD 1/5/04
		// Added row context which is needed for accessibility
		//internal void InitializeBand( Infragistics.Win.UltraWinGrid.UltraGridBand band, bool forceAlignLeft, RowsCollection rowsContext )
		internal void InitializeBand( Infragistics.Win.UltraWinGrid.UltraGridBand band, bool forceAlignLeft, RowsCollection rowsContext, UltraGridRow rowContext )
		{
			// SSP 10/10/01 UWG470
			// Primary context should be BandHeader, not the band for
			// BandHeadersUIElement.
			//
			//this.PrimaryContext = band;
			this.PrimaryContext = band.Header;
			this.forceAlignLeft = forceAlignLeft;


			// SSP 3/21/02
			// Added context of rows collection to the band header for row filtering
			// feature
			//
			this.rowsContext = rowsContext;

			// JJD 1/5/04
			// Added row context which is needed for accessibility
			this.rowContext = rowContext;
		}

		// SSP 5/19/03 - Fixed headers
		// Overrode ClipChildren so we can clip the header elements.
		//
		/// <summary>
		/// Overridden. Returning true causes all drawing of this element's child elements to be
		/// expicitly clipped to the area inside this elements borders
		/// </summary>
		protected override bool ClipChildren 
		{ 
			get 
			{ 
				if ( this.Band.UseFixedHeaders )
					return true;
				else
					return base.ClipChildren;
			} 
		}

		// SSP 3/21/02
		// Overrode GetContext to return the context of rows collection.
		// This was added for row filtering
		//
		/// <summary>
		/// Returns an object of requested type that relates to the element or null.  
		/// </summary>
		/// <param name="type">The requested type or null to pick up default context object. </param>
		/// <param name="checkParentElementContexts">If true will walk up the parent chain looking for the context.</param>
        /// <returns>Returns an object of requested type that relates to the element or null.</returns>
		public override object GetContext( Type type, bool checkParentElementContexts )
		{
			// if the context is of the passed in type return it
			//
			if ( null != type  )
			{
				if ( null != this.rowsContext )
				{
					if ( typeof( RowsCollection ) == type )
					{
						return this.rowsContext;
					}
				}

				// JJD 1/5/04
				// Added row context which is needed for accessibility
				if ( null != this.rowContext )
				{
					if ( typeof( UltraGridRow ) == type )
					{
						return this.rowContext;
					}
				}

				// MRS 3/18/05
				// Added band context which is needed for allowing drag and drop in RowLayout mode. 
				if ( null != this.Band )
				{
					if ( typeof( UltraGridBand ) == type )
					{
						return this.Band;
					}
				}
			}

			return base.GetContext( type, checkParentElementContexts );
		}

		#region ContinueDescendantSearch

		// SSP 12/19/02
		// Optimizations.
		// Overrode ContinueDescendantSearch method.
		//
		/// <summary>
		/// This method is called from <see cref="Infragistics.Win.UIElement.GetDescendant(Type, object[])"/> as an optimization to
		/// prevent searching down element paths that can't possibly contain the element that is being searched for. 
		/// </summary>
		/// <remarks><seealso cref="Infragistics.Win.UIElement.ContinueDescendantSearch"/></remarks>
        /// <returns>Returns false if the search should stop walking down the element chain because the element can't possibly be found.</returns>
		protected override bool ContinueDescendantSearch( Type type, object[] contexts )
		{
			if ( null != contexts )
			{
				object primaryContext = this.PrimaryContext;
				Type   primaryContextType = null != primaryContext ? primaryContext.GetType( ) : null;

				for ( int i = 0; i < contexts.Length; i++ )
				{
					object context = contexts[i];

					if ( null != context )
					{
						if ( context is UltraGridRow ||
							 context is UltraGridCell )
							return false;

						Type contextType = context.GetType( );

						if ( primaryContextType == contextType && this.PrimaryContext != context )
							return false;

						if ( typeof( RowsCollection ) == contextType && this.rowsContext != context  )
							return false;
					}
				}
			}

			return true;
		}

		#endregion // ContinueDescendantSearch


		/// <summary>
		/// The UltraGridBand object represents all the rows that occur at a single level of a hierarchical data set. Bands can be expanded or collapsed to display the data in the rows they contain.
		/// </summary>
		/// <remarks>
		/// <p class="body">The UltraGridBand object represents all the records at one level of a hierarchical recordset. Bands are the foundation of hierarchical data in the UltraWinGrid. When bound to a recordset, each band corresponds to a single Command. (A band can also be considered as roughly equivalent to the table or query level of organization within a database.) Although the rows in a band may be visually separated (appearing grouped under the rows of the next higher band in the hierarchy) they are in fact one set of records. In the data hierarchy of the grid, bands come after the grid itself, but before rows and cells.</p>
		/// <p class="body">There is always at least one UltraGridBand present in the UltraWinGrid, even when it is displaying a single-level (flat) recordset. Most of the properties that apply to the control at the topmost (grid) level also apply to the UltraGridBand object, since the band rather than the control is the primary container object for data. There is also broad support for applying different formatting and behavior attributes to individual bands. Since a band is effectively "a grid within a grid" you may want to have bands be markedly different from one another. For example, one band might display column headers and row selectors for each group of records, while another might display only data cells.</p>
		/// <p class="body">Bands can be displayed either horizontally or vertically within the grid, depending on the setting of the <b>ViewStyleBand</b> property. You can also hide entire bands from view by setting the <b>Hidden</b> property of the UltraGridBand object.</p>
		/// </remarks>
		public Infragistics.Win.UltraWinGrid.UltraGridBand Band
		{
			get
			{
				// SSP 10/10/01 UWG470
				// Changing the primary context from Band to BandHeader constituted
				// below change
				// 
				//return (Infragistics.Win.UltraWinGrid.UltraGridBand)this.GetContext( typeof( Infragistics.Win.UltraWinGrid.UltraGridBand ) );
				return ((BandHeader)this.PrimaryContext).Band;
			}
		}

		#region RowLayoutDesignerElement

		// SSP 3/19/03 - Row Layout Functionality
		// Added a property to indicate whether we are in row layout designer mode.
		//
		private bool RowLayoutDesignerElement
		{
			get
			{
				DataAreaUIElement dataAreaElem = (DataAreaUIElement)this.GetAncestor( typeof( DataAreaUIElement ) );

				return null != dataAreaElem && dataAreaElem.RowLayoutDesignerElement;
			}
		}

		#endregion // RowLayoutDesignerElement

		/// <summary>
		/// Makes sure that the child elements for this element are in the
		/// ChildElements array and that they are positioned properly.
		/// This needs to be overridden if the element has any child elements.
		/// </summary>
		protected override void PositionChildElements()
		{

			ColScrollRegion colRegion = ((RowColRegionIntersectionUIElement)this.GetAncestor( typeof ( RowColRegionIntersectionUIElement ) ) ).ColScrollRegion;

			// SSP 6/11/03 - Optimizations
			// Rather than accessing Band and Rect properties all over the code, store them in local variables and
			// use that. Calls to Band and Rect properties have been replaced with band and thisRect respectively
			// in the code of this method however these changes have not been commented as it was deemed not be
			// necessary.
			//
			UltraGridBand band = this.Band;
			UltraGridLayout layout = band.Layout;
			Rectangle thisRect = this.Rect;

			UIElementsCollection oldElements = this.ChildElements;
			this.childElementsCollection = null;

			// SSP 5/2/05 - NAS 5.2 Fixed Rows
			// Special row separators functionality.
			//
			// --------------------------------------------------------------------------------
			if ( band.HasSpecialRowSeparatorAfterHeaders )
			{
				Rectangle specialRowSeparatorRect = thisRect;
				specialRowSeparatorRect.Height = Math.Min( thisRect.Height, band.SpecialRowSeparatorHeightResolved );
				specialRowSeparatorRect.Y = thisRect.Bottom - specialRowSeparatorRect.Height;
				thisRect.Height -= specialRowSeparatorRect.Height;

				if ( specialRowSeparatorRect.Height > 0 )
				{
					SpecialRowSeparatorUIElement separatorElem = (SpecialRowSeparatorUIElement)GridUtils.ExtractExistingElement( oldElements, typeof( SpecialRowSeparatorUIElement ), true );
					if ( null == separatorElem )
						separatorElem = new SpecialRowSeparatorUIElement( this );
				
					separatorElem.Initialize( this.rowsContext );
					separatorElem.Rect = specialRowSeparatorRect;
					this.ChildElements.Add( separatorElem );
				}
			}
			// --------------------------------------------------------------------------------

            //JDN 11/19/04 Added RowSelectorHeader
            int rowSelectorExtent = band.RowSelectorExtent;
            Rectangle rowSelectorHeaderRect = new Rectangle( thisRect.Left, thisRect.Top , rowSelectorExtent, thisRect.Height );
            RowSelectorHeaderStyle rowSelectorHeaderStyle = band.RowSelectorHeaderStyleResolved;

			// SSP 5/19/03 - Fixed headers
			// Set the flag indicating whether we are using fixed headers. We do this for optimization
			// purposes so rather than having to access this property through the band multiple times
			// we can just store it as a flag.
			//
			this.usingFixedHeaders = band.UseFixedHeaders;

			// SSP 6/5/03 - Optimizations
			//
			this.headerBorderStyle = band.BorderStyleHeaderResolved;

           
            
			// SSP 3/19/03 - Row Layout Functionality
			// Allow for col region being null in the row layout designer mode.
			//
			if ( ! this.RowLayoutDesignerElement && colRegion == null ) 
				return;

			Rectangle       rcWork          = new System.Drawing.Rectangle(0, 0, 0, 0);
			bool            bBandFound      = false;
			bool            bDrawHeaderCut  = false;
			bool            bDisplayHeader;

			// SSP 3/19/03 - Row Layout Functionality
			// In row layout DESIGNER, don't display the band header.
			//
			//int bandHeaderHeight = this.Band.Header.GetBandHeaderHeight();
			int bandHeaderHeight = ! this.RowLayoutDesignerElement ? band.Header.GetBandHeaderHeight() : 0;

			RowColRegionIntersectionUIElement rcrElem = (RowColRegionIntersectionUIElement)
				this.GetAncestor( typeof( RowColRegionIntersectionUIElement ) );

			// SSP 7/19/05 - NAS 5.3 Header Placement
			// 
			//int bandOrigin = 0;
			int extendFirstItemsLeft = 0;
			

			// SSP 3/19/03 - Row Layout Functionality
			// In row layout DESIGNER mode we don't need show the band header.
			// Enclosed the following block of code in the if block.
			//
			if ( ! this.RowLayoutDesignerElement )
			{
				// SSP 7/19/05 - NAS 5.3 Header Placement
				// 
				int headersAreaElemExtendedLeftBy = 0;

				if ( this.forceAlignLeft )
				{
					// SSP 10/18/01 UWG542
					// For single band outlook style we added the forceAlignLeft flag
					// so that the fixed header at the top of the grid will be left
					// aligned
					//
					// SSP 7/19/05 - NAS 5.3 Header Placement
					// 
					//int bandOrigin = rcrElem.ColScrollRegion.GetBandOrigin( band );
					headersAreaElemExtendedLeftBy = rcrElem.ColScrollRegion.GetBandOrigin( band );

					// SSP 11/27/01 UWG771
					// Also take into account the Band.PreRowAreaExtent.
					//
					if ( layout.Rows.IsGroupByRows )
						// SSP 7/19/05 - NAS 5.3 Header Placement
						// 
						//bandOrigin += band.PreRowAreaExtent;
						headersAreaElemExtendedLeftBy += band.PreRowAreaExtent;
				}
					// SSP 7/19/05 - NAS 5.3 Header Placement
					// If the headers are displayed above the group-by rows (HeaderPlacement of 
					// OncePerGroupedRowIsland) then extend the first header (or the seprate element
					// depending on the row selector header style) left so the headers are aligned
					// with the left edge of the top-level group-by rows.
					// Added the else block.
					// 
				else if ( HeaderPlacement.OncePerGroupedRowIsland == band.HeaderPlacementResolved
					// SSP 10/6/06 BR15166 
					// This is to fix issue raised by BR15166 sample when enabling row selectors 
					// and RowSelectorHeaderStyle of SeparateElement. Added the following line.
					// 
					|| layout.ViewStyleImpl.IsOutlookGroupBy && layout.ViewStyleImpl.BandHasFixedHeaders( band ) 
					)
				{
					headersAreaElemExtendedLeftBy = layout.ViewStyleImpl.CalcGroupByConnectorsExtent( band );
				}
    
				// SSP 7/20/05 - NAS 5.3 Header Placement
				// We need to also add the amount by which the headers are extended left farther than
				// the row selectors. This happens when there are group-by columns in in single band 
				// or when the header placement is OncePerGroupedRowIsland.
				// 
				rowSelectorHeaderRect.Width += headersAreaElemExtendedLeftBy;
				if ( RowSelectorHeaderStyle.ExtendFirstColumn == rowSelectorHeaderStyle )
					extendFirstItemsLeft = headersAreaElemExtendedLeftBy;

				//RobA UWG236 8/27/01 Added support for visible headers
				//
				//if band header is visible, add it
				//
				if ( band.HeaderVisible )
				{
					if ( band.CardView )
					{
						// JJD 12/26/01
						// The only header that can display in a band in CardView is
						// the band header so use the entire header area for it
						//
						rcWork = thisRect;
					}
					else
					{
						//	calculate the rect of the band header
						rcWork.Y = thisRect.Top;
						rcWork.X = band.Header.GetOnScreenOrigin( colRegion, band.HasRowSelectors );
						//RobA UWG268 8/29/01 don't add PreRowAreaExtent
						//+ this.Band.PreRowAreaExtent;

						rcWork.Height = bandHeaderHeight;

						// JJD 1/23/02 - UWG971
						// Use GetExtent method since the extent will be different for
						// exclusive col regions
						//
						//RobA UWG268 8/29/01 don't sub PreRowAreaExtent
						//rcWork.Width  = this.Band.Header.Extent;// - this.Band.PreRowAreaExtent;
					
						rcWork.Width = band.Header.GetExtent( colRegion );

						// SSP 4/28/06 BR11686
						// 
						if ( RowSelectorHeaderStyle.ExtendFirstColumn != rowSelectorHeaderStyle )
						{
							rcWork.X += rowSelectorExtent;
							rcWork.Width -= rowSelectorExtent;
						}

						// SSP 10/18/01 UWG542
						// If this.forceAlignLeft is true, then align the band header to the left edge
						// of the csr
						//
						// SSP 7/19/05 - NAS 5.3 Header Placement
						// 
						// --------------------------------------------------------
						
						rcWork.X -= extendFirstItemsLeft;
						rcWork.Width += extendFirstItemsLeft;
						// --------------------------------------------------------

						// SSP 6/11/03 - Fixed headers
						//
						// ------------------------------------------------------------------
						if ( rcWork.Right < thisRect.Right )
							rcWork.Width += thisRect.Right - rcWork.Right;

						if ( rcWork.Left < thisRect.Left )
						{
							// SSP 8/5/03 UWG2552
							// we want to resize the header only if the TextHAlign is set to
							// HAlign.Left.
							//
							AppearanceData appData = new AppearanceData( );
							AppearancePropFlags flags = AppearancePropFlags.TextHAlign;
							band.Header.ResolveAppearance( ref appData, flags );
							if ( HAlign.Left == appData.TextHAlign )
							{
								rcWork.Width -= thisRect.Left - rcWork.Left;
								rcWork.X = thisRect.Left;
							}
						}
						// ------------------------------------------------------------------

						// JJD 11/28/01
						// If the borders are mergeable merge the border with
						// the controls border.
						//
						// SSP 6/5/03 - Optimizations
						//
						//if  ( this.Band.Layout.CanMergeAdjacentBorders( this.Band.BorderStyleHeaderResolved ) )
						if  ( layout.CanMergeAdjacentBorders( this.headerBorderStyle ) )
						{
							// SSP 1/27/05 BR02053
							// Instead of checking IsPrinting flag, make use of IsDisplayLayout. What we are 
							// interested in finding out is this element is in the print layout or not. Simply
							// checking the IsPrinting flag won't tell us that the display layout could verify 
							// it's ui elements while printing (like when the grid gets a paint while printing,
							// especially with the new UltraGridPrintDocument).
							//
							//UltraGrid grid = band.Layout.Grid as UltraGrid;
							//if ( this.Parent.Rect.Top == thisRect.Top && ( grid == null || !grid.IsPrinting ) )
							if ( this.Parent.Rect.Top == thisRect.Top && layout.IsDisplayLayout )
							{
								rcWork.Y--;
								rcWork.Height++;
							}

							// JJD 1/17/02
							// If there are headers following us then ad 1 to the height 
							// so that they overlap.
							//
							if ( this.Band.ColHeadersVisibleInSeparateAreaResolved ||
								( this.Band.GroupsDisplayed && this.Band.GroupHeadersVisibleInSeparateAreaResolved ) )
							{
								rcWork.Height++;
							}

							if ( this.Parent.Rect.Left == thisRect.Left )
							{
								rcWork.X--;
								rcWork.Width++;
							}						

							if ( this.Parent.Rect.Right == thisRect.Right )
							{
								rcWork.Width++;
							}						
						}
					}								

					//	add the element
					// SSP 8/8/03 - Fixed headers
					// Clear the clip rect.
					//
					//this.AddElementHelper( oldElements, typeof(HeaderUIElement), rcWork, band.Header, true );			
					// MD 2/12/09 - TFS13812
					// Added an extra parameter to AddElementHelper to specify whether the element is a right fixed header separator element.
					//HeaderUIElement tmpElem = (HeaderUIElement)this.AddElementHelper( oldElements, typeof(HeaderUIElement), rcWork, band.Header, true );			
					HeaderUIElement tmpElem = (HeaderUIElement)this.AddElementHelper( oldElements, typeof( HeaderUIElement ), rcWork, band.Header, false, true );

					if ( null != tmpElem )
						tmpElem.InternalSetClipSelfRegion( Rectangle.Empty );
				}
			}

			// JJD 12/26/01
			// The only header that can display in a band in CardView is
			// the band header so we can exit out at this point
			//
			if ( band.CardView )
				return;

			// SSP 2/19/03 - Row Layout Functionality
			// NOTE: When groups are enabled, row layout functionality is disabled. So don't need to worry
			// about adding group header elements.
			//
			// -------------------------------------------------------------------------------------
			if ( band.UseRowLayoutResolved )
			{
				Rectangle colHeaderAreaRect = thisRect;
				colHeaderAreaRect.Y += bandHeaderHeight;
				colHeaderAreaRect.Height = thisRect.Bottom - colHeaderAreaRect.Y;

				// SSP 5/23/03 - Fixed headers
				// Take into account the fact the row cell area element could have been shrinked in
				// the fixed headers mode. However we still need to pass in the whole row cell area
				// rect to the layout manager and not the shrunk rect.
				//
				if ( band.UseFixedHeaders && null != colRegion )
				{
					int delta = band.GetFixedHeaders_OriginDelta( colRegion );
					colHeaderAreaRect.X -= delta;
					colHeaderAreaRect.Width += delta;
				}

				LayoutContainerHeaderArea lc = band.GetCachedLayoutContainerHeaderArea( );
				lc.Initialize( band, colHeaderAreaRect, this, oldElements );
				band.HeaderLayoutManager.LayoutContainer( lc, null );
                
                //JDN 11/19/04 Added RowSelectorHeader
                if ( rowSelectorHeaderStyle == RowSelectorHeaderStyle.SeparateElement
					// SSP 6/17/05 - NAS 5.3 Column Chooser
					// Also add the row selector header element if RowSelectorHeaderStyle 
					// is set to ColumnChooserButton. The ColumnChooserButtonUIElement will
					// be positioned within the RowSelectorHeaderUIElement.
					// 
					|| RowSelectorHeaderStyle.ColumnChooserButton == rowSelectorHeaderStyle
					// SSP 12/8/05 BR06669
					// Added row selector header style of ColumnChooserButtonFixedSize.
					// 
					|| RowSelectorHeaderStyle.ColumnChooserButtonFixedSize == rowSelectorHeaderStyle )
                {
                    this.AddRowSelectorHeaderElement( oldElements, rowSelectorHeaderRect );
                }

				return;
			}
			// -------------------------------------------------------------------------------------

			// SSP 3/19/03 - Row Layout Functionality
			// Moved this from the beginning of the method.
			//
			if ( colRegion == null ) 
				return;

			// SSP 5/19/03 - Fixed headers
			//
			int fixedHeadersAreaRight = 0;
			HeaderUIElement headerElem = null;
			bool isHeaderFixed;

			// MD 12/24/08 - TFS11568
			bool firstRightFixedHeaderPlaced = false;

			VisibleHeadersCollection visibleHeaders = colRegion.VisibleHeaders;

			// Iterate over the visible headers and create an element for each
			//
			
			
			
			for ( int index = 0, vhCount = visibleHeaders.Count; index < vhCount; index++ )
			{
				VisibleHeader vh = visibleHeaders[index]; 

				if ( vh.Header == null )
					continue;

				// If the header's band doesn't match our band then bypass this header
				//
				if( vh.Header.Band != band )
				{
					// if we have already processed our band's headers then stop iterating
					//
					if ( bBandFound )
						break;

					continue;
				}

				bBandFound      = true;

				// flag what type of header this is
				bool isGroup = false;
				bool isColumn = false;
				
				if ( vh.Header.IsGroup )
					isGroup = true;
				else
					isColumn = true;

				if( isGroup )
				{
					// if this is a group, the top is offset by the band header height,
					// and the bottom is offset by (band header height + group header height)
					rcWork.Y = thisRect.Top + bandHeaderHeight;
					rcWork.Height = band.GroupHeaderHeight;
				}

                if( isColumn )
				{
					//	if this is a column, the top is offset by (band header height + group header height)
					//	and the bottom = m_rect.bottom
					rcWork.Y = thisRect.Top + bandHeaderHeight + band.GroupHeaderHeight;
					rcWork.Height = thisRect.Height - ( bandHeaderHeight - band.GroupHeaderHeight );
				}
		
				
				//	calculate the rest of the rect and header cut        
				rcWork.X = vh.Header.GetOnScreenOrigin( colRegion, true );

				rcWork.Width = vh.Header.Extent;

				// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
				// In AutoFitStyle mode of ExtendLastColumn the band extents are adjusted to
				// occupy the extra visible space however header extents (column widths) aren't.
				// We need to do them here. If the column or group is the last in the row cell 
				// area then extend it so it occupies the rest of the space in the row cell area.
				//
				// ------------------------------------------------------------------------------
				if ( vh.Header.LastItem && layout.AutoFitExtendLastColumn )
				{
					if ( rcWork.Right < thisRect.Right )
						rcWork.Width += thisRect.Right - rcWork.Right;
				}
				// ------------------------------------------------------------------------------

				bDrawHeaderCut  = false;

				if ( vh.Header.FirstItem )
				{
					// SSP 10/18/01 UWG542
					// If this.forceAlignLeft is true, then align the band header to the left edge
					// of the csr
					//
					// SSP 7/19/05 - NAS 5.3 Header Placement
					// 
					// --------------------------------------------------------
					
					rcWork.X -= extendFirstItemsLeft;
					rcWork.Width += extendFirstItemsLeft;
					// --------------------------------------------------------

					// only draw the header cut if we are in a multi band display
					//
					if ( band.PreRowAreaExtent > 0 && !band.HeaderVisible )
						bDrawHeaderCut = true;
			
				}

				// Only add header elements if the headers are to be displayed
				//
				if ( vh.Header.IsGroup )
					bDisplayHeader = band.GroupHeadersVisibleInSeparateAreaResolved;
				else
					bDisplayHeader = band.ColHeadersVisibleInSeparateAreaResolved;

				if ( bDisplayHeader )
				{

					//If header borders can be merged
					//adjust widths and heights to overlap each other
					//
					// SSP 6/5/03 - Optimizations
					//
					//if  ( this.Band.Layout.CanMergeAdjacentBorders(vh.Header.Band.BorderStyleHeaderResolved ) )
					if  ( layout.CanMergeAdjacentBorders( this.headerBorderStyle ) )
					{
						//If we are printing we only want to merge the header
						//if it is not the first one
						//
						// SSP 1/27/05 BR02053
						// Instead of checking IsPrinting flag, make use of IsDisplayLayout. What we are 
						// interested in finding out is this element is in the print layout or not. Simply
						// checking the IsPrinting flag won't tell us that the display layout could verify 
						// it's ui elements while printing (like when the grid gets a paint while printing,
						// especially with the new UltraGridPrintDocument).
						// Commented out the original code and added the new code below.
						// --------------------------------------------------------------------------------
						
						if ( !vh.Header.FirstItem || this.Parent.Rect.Left == thisRect.Left && layout.IsDisplayLayout )
						{
							rcWork.X--;
							rcWork.Width++;
						}
						// --------------------------------------------------------------------------------
						
						//if this is the header under the caption
						//we need to merge
						//
						if ( this.Parent.Rect.Top == thisRect.Top &&
							( 
							// SSP 1/27/05 BR02053
							// Instead of checking IsPrinting flag, make use of IsDisplayLayout. What we are 
							// interested in finding out is this element is in the print layout or not. Simply
							// checking the IsPrinting flag won't tell us that the display layout could verify 
							// it's ui elements while printing (like when the grid gets a paint while printing,
							// especially with the new UltraGridPrintDocument).
							//
							//grid == null || !grid.IsPrinting 
							layout.IsDisplayLayout
							|| band.HeaderVisible) &&
                            // MBS 1/27/09 - TFS11764
                            // Don't merge the borders when the GroupByBox is above this element
                            layout.GroupByBox.HiddenResolved)
						{
							rcWork.Y--;
							rcWork.Height++;						
						}						
						
					}

					// SSP 7/17/03 - Fixed headers
					// If the column header is completely scrolled underneath the fixed
					// headers, then don't add it.
					//
					// ----------------------------------------------------------------------
					isHeaderFixed = false;
					if ( usingFixedHeaders )
					{
						isHeaderFixed = band.IsHeaderFixed( colRegion, vh.Header );
						if ( ! isHeaderFixed && rcWork.Right <= fixedHeadersAreaRight )
							continue;
					}
					// ----------------------------------------------------------------------

                    //JDN 11/19/04 Added RowSelectorHeader
                    // adjust rect for RowSelectorHeader placement
					// SSP 6/17/05 - NAS 5.3 Column Chooser
					// Also account for the new ColumnChooserButton member.
					// 
                    //if ( rowSelectorHeaderStyle  == RowSelectorHeaderStyle.None || rowSelectorHeaderStyle  == RowSelectorHeaderStyle.SeparateElement )
					if ( RowSelectorHeaderStyle.ExtendFirstColumn != rowSelectorHeaderStyle )
                    {
                        if ( vh.Header.FirstItem )
                        {                       
                            rcWork.X += rowSelectorExtent ;
                            rcWork.Width -= rowSelectorExtent ;                                            
                        }
                    }

					// add the group or column's header object
					//
					// MD 2/12/09 - TFS13812
					// Added an extra parameter to AddElementHelper to specify whether the element is a right fixed header separator element.
					//UIElement element = AddElementHelper( oldElements,
					//    typeof(HeaderUIElement),
					//    rcWork,
					//    vh.Header,
					//    bDrawHeaderCut );
					UIElement element = AddElementHelper( oldElements,
						typeof( HeaderUIElement ),
						rcWork,
						vh.Header,
						false,
						bDrawHeaderCut );

					// MD 12/24/08 - TFS11568
					// Place a 'separator' header element if this is the first right-fixed header. 
					// This is a header that is 2 pixels wide to just be the left border of the header.
					if ( firstRightFixedHeaderPlaced == false && 
						this.usingFixedHeaders &&
						band.IsHeaderFixed( colRegion, vh.Header ) &&
						vh.Header.FixOnRightResolved )
					{
						// MD 2/12/09 - TFS13812
						// We shouldn't be creating a new element each time. Use the AddElementHelper method which has now been changed to add right 
						// fixed header separators also.
						//HeaderUIElement separatorElement = new HeaderUIElement( this, vh.Header );
						//
						//Rectangle separatorRect = element.Rect;
						//separatorRect.X -= 2;
						//separatorRect.Width = 2;
						//separatorElement.Rect = separatorRect;
						//
						//this.ChildElements.Add( separatorElement );
						// MD 3/3/09 - TFS14837
						// The element will be null it's rect is out of view because there is not enough room to display it.
						// Use the rcWork variable, which is the Rectangle which is set on the element's Rect value anyway.
						//Rectangle separatorRect = element.Rect;
						Rectangle separatorRect = rcWork;

						// MD 2/20/09 - TFS11568
						// A minimum of 3 pixels is needed to see the border of the separator with the Windows XP Luna Blue theme.
						//separatorRect.X -= 2;
						//separatorRect.Width = 2;
						const int separatorWidth = 3;
						separatorRect.X -= separatorWidth;
						separatorRect.Width = separatorWidth;

						this.AddElementHelper( oldElements,
							typeof( HeaderUIElement ),
							separatorRect,
							vh.Header,
							true,
							bDrawHeaderCut );

						firstRightFixedHeaderPlaced = true;
					}

					// SSP 5/19/03 - Fixed headers
					// 
					// ------------------------------------------------------------------------------------
					headerElem = element as HeaderUIElement;
					if ( null != headerElem )
					{
						Rectangle clipSelfRect = Rectangle.Empty;
						
						if ( this.usingFixedHeaders )
						{
							if ( rcWork.Left < fixedHeadersAreaRight && ! isHeaderFixed )
							{
								clipSelfRect = rcWork;
								clipSelfRect.Width = clipSelfRect.Right - fixedHeadersAreaRight; 
								clipSelfRect.X = fixedHeadersAreaRight;

								if ( clipSelfRect.Width <= 0 )
								{
									// SSP 8/16/05 BR05458
									// This can happen if there is cell spacing or if the AddElementHelper
									// modifies the passed in elem rect so that it completely falls left of
									// the fixed headers are right. In such a case remove the header elem
									// and continue.
									// 
									//Debug.Assert( false, "This should not have happened !" );
									this.ChildElements.Remove( headerElem );

									continue;
								}
							}

							if ( isHeaderFixed )
								fixedHeadersAreaRight = Math.Max( fixedHeadersAreaRight, headerElem.Rect.Right );
						}

						headerElem.InternalSetClipSelfRegion( clipSelfRect );	
					}
					// ------------------------------------------------------------------------------------
				}

				// if this is a group we need to add all of its columns as well
				//
				// We only need to add the column headers is they are visible
				//
				if( vh.Header.IsGroup && band.ColHeadersVisibleInSeparateAreaResolved )
				{
					// SSP 6/14/05 BR04582
					// If the group header is not displayed we still need to adjust the rcWork (group header rect)
					// since the below logic assumes the rcWork would have been adjusted for the rowSelectorHeaderStyle.
					// 
					// ------------------------------------------------------------------------------------------------
					// SSP 6/17/05 - NAS 5.3 Column Chooser
					// Also account for the new ColumnChooserButton member.
					// 
					//if ( ! bDisplayHeader && ( rowSelectorHeaderStyle  == RowSelectorHeaderStyle.None 
					//	|| rowSelectorHeaderStyle  == RowSelectorHeaderStyle.SeparateElement ) )
					if ( ! bDisplayHeader && RowSelectorHeaderStyle.ExtendFirstColumn != rowSelectorHeaderStyle )
					{
						if ( vh.Header.FirstItem )
						{                       
							rcWork.X += rowSelectorExtent ;
							rcWork.Width -= rowSelectorExtent ;                                            
						}
					}
					// ------------------------------------------------------------------------------------------------

					// MD 1/21/09 - Groups in RowLayouts
					// Use the resolved columns because in GroupLayout style, it will return a different enumarator which returns the columns 
					// actually visible in the group
					//GroupColumnsCollection colList = vh.Header.Group.Columns;
					IEnumerable<UltraGridColumn> colList = vh.Header.Group.ColumnsResolved;

					if ( colList != null )
					{
						Rectangle rcColumn = new System.Drawing.Rectangle(0, 0, 0, 0);

						// MD 1/21/09 - Groups in RowLayouts
						// The colList is now an IEnumarable<UltraGridColumn> , so we must use a foreach.
						//UltraGridColumn column;
						//
						//// Iterate over the columns in this group and create an element for each
						////
						//for ( int i = 0; i < colList.Count; i++ )
						//{
						//    column = colList[i];
						// Iterate over the columns in this group and create an element for each
						//
						int columnCount = -1;
						foreach ( UltraGridColumn column in colList )
						{
							columnCount++;

							// If the column is hidden then continue
							//
							// MD 1/21/09 - Groups in RowLayouts
							// Use HiddenResolved instead of Hidden because it checks the group's hidden value.
							//if ( column.Hidden )
							if ( column.HiddenResolved )
								continue;

							Rectangle FixedHeaders = new Rectangle(0, 0, 0, 0 );
							column.CalculateHeaderRect( ref rcColumn, ref FixedHeaders );

							rcColumn.Offset((int)(rcWork.Left + column.RelativeOrigin), rcWork.Bottom );

							if ( vh.Header.FirstItem)
							{
								// SSP 10/20/03 UWG2707 UWG2617
								// Although above bugs are not caused by the following line of code, I noticed it 
								// when I was fixing above two bugs. Commented out the code. This code doesn't
								// make sense at all. Why are we offsetting by -PreRowAreaExtent. Either we should
								// be offsetting every header or no headers.
								// Commented out below line of code.
								//
								//rcColumn.Offset( -band.PreRowAreaExtent, 0 );

								// SSP 11/28/01 UWG771
								// If this.forceAlignLeft is true, then align the band header to the left edge
								// of the csr
                                // MRS 2/15/2008 - BR30577
                                // This check should have been removed when Sandip implemented Header Placement 
                                // int v5.3 and created the extendFirstItemsLeft variable.
                                // extendFirstItemsLeft will be 0 if no change is neccessary. 
                                //
                                //if ( this.forceAlignLeft )
								{
									// MD 1/21/09 - Groups in RowLayouts
									// Use the column count because there is no for loop veriable anymore
									//if ( 0 == i )
									if ( 0 == columnCount )
									{
										//rcColumn.X -= bandOrigin;
										// SSP 7/19/05 - NAS 5.3 Header Placement
										// 
										//rcColumn.Width += bandOrigin;
										rcColumn.Width += extendFirstItemsLeft;
									}
									else
									{
										// SSP 7/19/05 - NAS 5.3 Header Placement
										// 
										//rcColumn.X += bandOrigin;
										rcColumn.X += extendFirstItemsLeft;
									}
								}
							}

							// SSP 3/22/05 - NAS 5.2 Auto-fit Extend Last Column Functionality
							// In AutoFitStyle mode of ExtendLastColumn the band extents are adjusted to
							// occupy the extra visible space however header extents (column widths) aren't.
							// We need to do them here. If the column or group is the last in the row cell 
							// area then extend it so it occupies the rest of the space in the row cell area.
							//
							// ------------------------------------------------------------------------------
							if ( vh.Header.LastItem && column.LastItemInLevel && layout.AutoFitExtendLastColumn )
							{
								if ( rcColumn.Right < thisRect.Right )
									rcColumn.Width += thisRect.Right - rcColumn.Right;
							}
							// ------------------------------------------------------------------------------

							//If header borders can be merged
							//adjust widths and heights to overlap each other
							//
							// SSP 6/5/03 - Optimizations
							//
							//if  ( this.Band.Layout.CanMergeAdjacentBorders(vh.Header.Band.BorderStyleHeaderResolved ) )
							if  ( layout.CanMergeAdjacentBorders( this.headerBorderStyle ) )
							{

								//if the first header's left border needs to merge 
								//with the data area's
								//border or this is not the first header
								//increase the width
								//
								// JJD 1/15/02
								// Tweeked the column header adjustements so the headers
								// overlap properly
								//
								// SSP 8/4/05 BR05085
								// This logic has to be consistent with the logic further above for
								// group header border merging.
								// 
								// --------------------------------------------------------------------
								if ( ! column.FirstItemInLevel || ! vh.Header.FirstItem || this.Parent.Rect.Left == thisRect.Left )
								{
									if ( ! bDisplayHeader )
										rcColumn.X--;

									rcColumn.Width++;
								}
								
								// --------------------------------------------------------------------
								
								//if this is the header under the caption
								//we need to merge
								//
								// JJD 11/28/01
								// We always want to merge top and bottom borders if the
								// group and column headers are visible
								//
								if ( this.Parent.Rect.Top == thisRect.Top ||
									band.GroupHeadersVisibleInSeparateAreaResolved ||
									// MD 1/21/09 - Groups in RowLayouts
									// Use LevelResolved because it checks the band's row layout style
									//column.Level > 0 )
									column.LevelResolved > 0 )
								{
									rcColumn.Y--;
									rcColumn.Height++;						
								}								
							}
							
							// SSP 7/17/03 - Fixed headers
							// If the column header is completely scrolled underneath the fixed
							// headers, then don't add it.
							//
							// ----------------------------------------------------------------------
							isHeaderFixed = false;
							if ( usingFixedHeaders )
							{
								isHeaderFixed = band.IsHeaderFixed( colRegion, column.Header );
								if ( ! isHeaderFixed && rcColumn.Right <= fixedHeadersAreaRight )
									continue;
							}
							// ----------------------------------------------------------------------

                            //JDN 11/19/04 Added RowSelectorHeader
                            // adjust the column rect for RowSelectorHeader placement
							// SSP 6/17/05 - NAS 5.3 Column Chooser
							// Also account for the new ColumnChooserButton member.
							// 
                            //if ( rowSelectorHeaderStyle  == RowSelectorHeaderStyle.None || rowSelectorHeaderStyle  == RowSelectorHeaderStyle.SeparateElement )
							if ( RowSelectorHeaderStyle.ExtendFirstColumn != rowSelectorHeaderStyle )
                            {
                                if ( column.Header.FirstItem )                               
                                    rcColumn.Width -= rowSelectorExtent;                                
                                else if ( vh.Header.FirstItem )                                                               
                                    rcColumn.X -= rowSelectorExtent;                                                             
                            }

							// add the column's element object
							//
							UIElement element = AddElementHelper( oldElements,
								typeof(HeaderUIElement),
								rcColumn, column.Header,
								// MD 2/12/09 - TFS13812
								// Added an extra parameter to AddElementHelper to specify whether the element is a right fixed header separator element.
								false,
								// If the group header was added we don't want to display 
								// the cut on the first column. Otherwise pass the bDrawHeaderCut 
								// variable that was initialized above
								//
								band.GroupHeadersVisibleInSeparateAreaResolved ? false : bDrawHeaderCut );


							// SSP 5/19/03 - Fixed headers
							// 
							// ------------------------------------------------------------------------------------
							headerElem = element as HeaderUIElement;
							if ( null != headerElem )
							{
								Rectangle clipSelfRect = Rectangle.Empty;
						
								if ( this.usingFixedHeaders )
								{
									if ( rcColumn.Left < fixedHeadersAreaRight && ! isHeaderFixed )
									{
										clipSelfRect = rcColumn;
										clipSelfRect.Width = clipSelfRect.Right - fixedHeadersAreaRight; 
										clipSelfRect.X = fixedHeadersAreaRight;

										if ( clipSelfRect.Width <= 0 )
										{
											// SSP 8/16/05 BR05458
											// This can happen if there is cell spacing or if the AddElementHelper
											// modifies the passed in elem rect so that it completely falls left of
											// the fixed headers are right. In such a case remove the header elem
											// and continue.
											// 
											//Debug.Assert( false, "This should not have happened !" );
											this.ChildElements.Remove( headerElem );

											continue;
										}
									}

									if ( isHeaderFixed )
										fixedHeadersAreaRight = Math.Max( fixedHeadersAreaRight, headerElem.Rect.Right );
								}

								headerElem.InternalSetClipSelfRegion( clipSelfRect );
							}
							// ------------------------------------------------------------------------------------

							// Reset the bDrawHeaderCut flag so only the first column has the cut
							//
							bDrawHeaderCut = false;
						}
					}

				}

			}

            //JDN 11/19/04 Added RowSelectorHeader
            // Add RowSelectorHeader
            if ( rowSelectorHeaderStyle == RowSelectorHeaderStyle.SeparateElement
				// SSP 6/17/05 - NAS 5.3 Column Chooser
				// Also add the row selector header element if RowSelectorHeaderStyle 
				// is set to ColumnChooserButton. The ColumnChooserButtonUIElement will
				// be positioned within the RowSelectorHeaderUIElement.
				// 
				|| RowSelectorHeaderStyle.ColumnChooserButton == rowSelectorHeaderStyle 
				// SSP 12/8/05 BR06669
				// Added row selector header style of ColumnChooserButtonFixedSize.
				// 
				|| RowSelectorHeaderStyle.ColumnChooserButtonFixedSize == rowSelectorHeaderStyle )
            {
                this.AddRowSelectorHeaderElement( oldElements, rowSelectorHeaderRect );
            }
		}


		// Helper function for creating this elements sub elements. It will attempt to 
		// use an existing element from the old list instead of creating a new one.
		//
		// SSP 2/26/03 - Row Layout Functionality
		// Changed the access modifier from private (actually no modifier was specified) to internal.
		//
		// SSP 6/5/03 - Fixed headers
		// Changed the return type from void to UIElement. It will return the added element or null if
		// none was added.
		//
		internal UIElement AddElementHelper( UIElementsCollection oldList,
								Type type,
								Rectangle rect,
								HeaderBase header,
								// MD 2/12/09 - TFS13812
								// Added an extra parameter to AddElementHelper to specify whether the element is a right fixed header separator element.
								bool isRightFixedHeaderSeparator,
								bool bDrawHeaderCut )
		{

			// if the passed in rect doesn't intersect at all with this 
			// element's rect rect then just return
			//
			if ( !rect.IntersectsWith( this.rectValue ) )
				return null;

			// Set pElement to the first item in the list that matches the requested type
			//
			HeaderUIElement headerElement = 
				(HeaderUIElement)BandHeadersUIElement.ExtractExistingElement( oldList, typeof(HeaderUIElement), true );

			// if we didn't find one in the old list then create a new item now.
			// Otherwise, reset the rect and scroll regions for the old element
			//
			if ( headerElement == null )
				headerElement = new HeaderUIElement( this );

			//
			// MD 2/12/09 - TFS13812
			// Added an extra parameter to InitializeHeader to specify whether the element is a right fixed header separator element.
			//headerElement.InitializeHeader(header);
			headerElement.InitializeHeader( header, isRightFixedHeaderSeparator );
			
			headerElement.Rect = rect;

			if ( headerElement != null )
			{
				// insert the new element into the passed in list
				//
				this.ChildElements.Add( headerElement );
			}

			return headerElement;
		}

        //JDN 11/19/04 Added RowSelectorHeader
        #region AddRowSelectorHeaderElement

        // Helper function for creating the RowSelectorHeaderElement. It will attempt to 
        // use an existing element from the old list instead of creating a new one.
        internal UIElement AddRowSelectorHeaderElement( UIElementsCollection oldList, Rectangle rect )
        {
            // if the passed in rect doesn't intersect with this 
            // element's rect rect then just return
            if ( !rect.IntersectsWith( this.rectValue ) )
                return null;

            //  Extract element - create if necessary
            RowSelectorHeaderUIElement rowSelectorHeaderElement = 
                (RowSelectorHeaderUIElement)BandHeadersUIElement.ExtractExistingElement( oldList, typeof(RowSelectorHeaderUIElement), true );

            // if we didn't find one in the old list then create a new item now.
            // Otherwise, reset the rect and scroll regions for the old element
            if ( rowSelectorHeaderElement == null )
				// SSP 4/11/05
				// Got rid of rowsContext. The RowSelectorHeaderUIElement should just use the
				// GetContext method.
				//
                //rowSelectorHeaderElement = new RowSelectorHeaderUIElement( this, this.rowsContext );
				rowSelectorHeaderElement = new RowSelectorHeaderUIElement( this );
		
            rowSelectorHeaderElement.Rect = rect;

            if ( rowSelectorHeaderElement != null )
            {
                // insert the new element into the passed in list
                this.ChildElements.Add( rowSelectorHeaderElement );
            }

            return rowSelectorHeaderElement;
        }


        #endregion // AddRowSelectorHeaderElement


		// SSP 7/22/02 UWG1390
		// We don't want to check for band header being enabled here. This ui element
		// is used for the whole band headers area including the column and group headers.
		// There is a separate ui element for the actual physical band header header that
		// gets displayed and that's what should be disabled. Besides the way we
		// are doing it here is not right because by default the headers are enabled but
		// what if the grid is disabled. This will not look at that and the headers will
		// look enabled.
		// Commented out the Enabled override here and implemented a different fix
		// for UWG609.
		//
		

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawBackColor ( ref UIElementDrawParams drawParams )
		{
			// this element doesn't draw a background
		}

        /// <summary>
        /// This element doesn't draw a background.
        /// </summary>
        /// <param name="drawParams">The <see cref="Infragistics.Win.UIElementDrawParams"/> used to provide rendering information.</param>
		protected override void DrawImageBackground ( ref UIElementDrawParams drawParams )
		{
			// this element doesn't draw a background
		}

        /// <summary>
        /// Returns or sets a value that determines the border style of an object.
        /// </summary>
        /// <remarks>
        /// <p class="body">Note that not all styles are available on all operating systems. If the version of the OS that your program is running on does not support a particular border style, borders formatted with that style will be drawn using solid lines.</p>
        /// </remarks>
        public override UIElementBorderStyle BorderStyle
		{
			get
			{
				return UIElementBorderStyle.None;
			}
		}

		/// <summary>
		/// <see cref="Infragistics.Win.UIElement.BorderSides"/>
		/// </summary>
		public override Border3DSide BorderSides
		{
			get
			{
				return 0;
			}
		}


		// MRS 3/31/05 - SpanResizing support for RowLayouts
		#region Column Moving support for RowLayouts

		#region Private / Internal Methods

		#region GetRowLayoutAreaRect
		// MRS 4/11/05 - Calcalate the rect the layout is using
		internal Rectangle GetRowLayoutAreaRect()
		{
			UltraGridBand band = this.Band;
			Rectangle thisRect = this.Rect;

			int bandHeaderHeight = ! this.RowLayoutDesignerElement ? band.Header.GetBandHeaderHeight() : 0;

			Rectangle colHeaderAreaRect = thisRect;
			colHeaderAreaRect.Y += bandHeaderHeight;
			colHeaderAreaRect.Height = thisRect.Bottom - colHeaderAreaRect.Y;

			// SSP 5/2/05 - NAS 5.2 Fixed Rows
			// Special row separators functionality. Also take out the special row separator height.
			//
			if ( band.HasSpecialRowSeparatorAfterHeaders )
				colHeaderAreaRect.Height -= Math.Min( colHeaderAreaRect.Height, band.SpecialRowSeparatorHeightResolved );

			ColScrollRegion colRegion = ((RowColRegionIntersectionUIElement)this.GetAncestor( typeof ( RowColRegionIntersectionUIElement ) ) ).ColScrollRegion;

			// SSP 5/23/03 - Fixed headers
			// Take into account the fact the row cell area element could have been shrinked in
			// the fixed headers mode. However we still need to pass in the whole row cell area
			// rect to the layout manager and not the shrunk rect.
			//
			if ( band.UseFixedHeaders && null != colRegion )
			{
				int delta = band.GetFixedHeaders_OriginDelta( colRegion );
				colHeaderAreaRect.X -= delta;
				colHeaderAreaRect.Width += delta;
			}

			colHeaderAreaRect = LayoutContainerHelper.GetContainerRectHelper(band, colHeaderAreaRect, UIElementBorderStyle.None, band.BorderStyleHeaderResolved);
			
			return colHeaderAreaRect;
		}
		#endregion GetRowLayoutAreaRect

		#endregion Private / Internal Methods

		#endregion Column Moving support for RowLayouts
    }
}
