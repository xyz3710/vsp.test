#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
    using System;
	using System.Collections;
    using System.Data;
    using Infragistics.Shared;
    using Infragistics.Shared.Serialization;
    using Infragistics.Win;
	using System.Runtime.Serialization;
	using System.Diagnostics;
	using System.ComponentModel;
	using System.Security.Permissions;
	using System.Collections.Generic;


    /// <summary>
    /// A collection of Columns that belong to a specific group.
    /// </summary>
    [ Serializable()]
	[ ListBindable(false )]
	public class GroupColumnsCollection  :	KeyedSubObjectsCollectionBase, 
											ISerializable,
											// MD 1/21/09 - Groups in RowLayouts
											IEnumerable<UltraGridColumn>
	{
		private Infragistics.Win.UltraWinGrid.UltraGridGroup group;
		private bool readOnly = false;
		private SerializedColumnID[] serializedColumnIDs = null;
		private int	 initialCapacity = 5;


		/// <summary>
		/// Constructor of a collection of Columns that belong to a specific group.
		/// </summary>
		/// <param name="group">The UltraGridGroup.</param>
		public GroupColumnsCollection( Infragistics.Win.UltraWinGrid.UltraGridGroup group )
		{
			this.group = group;
		}


        /// <summary>
        /// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
        /// </summary>
        /// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
        /// <param name="context">Context for the deserialization</param>
        // AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//public GroupColumnsCollection( SerializationInfo info, StreamingContext context )
		protected GroupColumnsCollection( SerializationInfo info, StreamingContext context )
		{
			PropertyCategories propCat = context.Context is PropertyCategories
				? (PropertyCategories)context.Context	: PropertyCategories.All;

			if( ( propCat & PropertyCategories.Groups ) == 0 )
				return;

			//this will have to be set in InitGroup()
			this.group = null;

			foreach( SerializationEntry entry in info )
			{
				if ( entry.ObjectType == typeof( SerializedColumnID ) )
				{
					// JJD 8/19/02
					// Use the DeserializeProperty static method to de-serialize properties instead.
					SerializedColumnID item = (SerializedColumnID)Utils.DeserializeProperty( entry, typeof(SerializedColumnID), null );

					this.serializedColumnIDs[ int.Parse( entry.Name ) ] = item;
					//this.serializedColumnIDs[ int.Parse( entry.Name ) ] = (SerializedColumnID)entry.Value;
				}
				else
				{
					switch ( entry.Name )
					{
						case "Count":
						{
							//this.initialCapacity = Math.Max( this.initialCapacity, (int)entry.Value );
							// AS 8/14/02
							// Use the disposable object method to allow deserialization from xml, etc.
							//
							// JJD 8/17/02
							// Use the DeserializeProperty static method to de-serialize properties instead.
							int deserializedvalue = (int)Utils.DeserializeProperty( entry, typeof(int), 0);
							this.initialCapacity = Math.Max(this.initialCapacity, deserializedvalue);

							//this.initialCapacity = Math.Max(this.initialCapacity, (int)DisposableObject.ConvertValue( entry.Value, typeof(int) ));

							//this.serializedColumnIDs = new SerializedColumnID[(int)entry.Value];
							//this.serializedColumnIDs = new SerializedColumnID[(int)DisposableObject.ConvertValue( entry.Value, typeof(int) )];
							this.serializedColumnIDs = new SerializedColumnID[deserializedvalue];

							break;
						}

						// JJD 1/31/02
						// De-Serialize the tag property
						//
						case "Tag":
						{
							// AS 8/15/02
							// Use our tag deserializer.
							//
							//this.tagValue = entry.Value;
							this.DeserializeTag(entry);
							break;
						}

						default:
						{
							Debug.Assert( false, "Invalid entry in SortedColsCollection de-serialization ctor" );
							break;
						}
					}
				}
			}

		}
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// first add the count so we can set the initial capacity
			// efficiently when we de-serialize
			//
			if (this.Count > 0)
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Count", this.Count );
				//info.AddValue( "Count", this.Count );
			
				// add each column in the collection
				//
				for ( int i = 0; i < this.Count; i++ )
				{
					int relativeIndex		= this[i].Index;

					if ( !this[i].IsBound )
						relativeIndex -= this.group.Band.Columns.BoundColumnsCount;


					// SSP 10/26/01
					// Added extra parameter isGroupByColumn in SerializedColumnID contructor
					//
					//SerializedColumnID colid = new SerializedColumnID( relativeIndex, 
					//	this[i].IsBound, SortIndicator.None, this[i].Key );
					SerializedColumnID colid = new SerializedColumnID( relativeIndex, 
						this[i].IsBound, SortIndicator.None, false, this[i].Key );

					// JJD 8/20/02
					// Use SerializeProperty static method to serialize properties instead.
					Utils.SerializeProperty( info, i.ToString(), colid );
					//info.AddValue( i.ToString(), colid );
				}
			}
			else if (this.serializedColumnIDs != null && this.serializedColumnIDs.Length > 0)
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Count", this.serializedColumnIDs.Length );
				//info.AddValue( "Count", this.serializedColumnIDs.Length );
			
				// add each column in the collection
				//
				for ( int i = 0; i < this.serializedColumnIDs.Length; i++ )
				{
					// JJD 8/20/02
					// Use SerializeProperty static method to serialize properties instead.
					Utils.SerializeProperty( info, i.ToString(), this.serializedColumnIDs[i] );
					//info.AddValue( i.ToString(), this.serializedColumnIDs[i] );
				}
			}

			// JJD 1/31/02
			// Serialize the tag property
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);
		}
 
		private void ProcessSerializedColumnIDs( SerializedColumnID[] colids )
		{
			// if we don't have a colid array or a band reference then
			// just return
			//
			if ( colids == null && 
				this.group.Band != null )
				return;

			Infragistics.Win.UltraWinGrid.UltraGridColumn column = null;

			// loop over the temporary serializedColumnIDs array and
			// try to match each with the associated column. Then add
			// that column to the sorted cols collection
			//
			for( int i = 0; i < colids.Length; i++ )
			{
				column = null;
				
				// first try matching via the column's key
				//
				try
				{
					if ( colids[i].Key.Length > 0 )
						column = this.group.Band.Columns[ colids[i].Key ];

				}
				catch(Exception)
				{
				}

				// if key did't work then try the relative index
				//
				if ( column == null )
				{
					int index = colids[i].RelativeIndex;

					// if the column is unbound add the bound columns count
					// to get the correct index into the columns collection
					//
					if ( !colids[i].Bound )
						index += this.group.Band.Columns.BoundColumnsCount;

					try
					{
						column = this.group.Band.Columns[ index ];
					}
					catch(Exception)
					{
					}

				}

				if ( column != null )
				{
					column.Group = this.group;

					// hook up the prop change notifications
					//
					column.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}
			}
		}
		internal void InitializeFrom( GroupColumnsCollection source, PropertyCategories categories  )
		{
			// Since ssPropCatSortedCols has more than one bit tunred on we need
			// to check the anded bits for equality
			//
			if ( ( categories & PropertyCategories.Groups ) != PropertyCategories.Groups )
				return;

			// JJD 1/31/02
			// Only copy over the tag if the source's tag is a candidate
			// for serialization
			//
			if ( source.ShouldSerializeTag() )
				this.tagValue = source.Tag;

			this.Clear();

			if ( source.serializedColumnIDs != null )
			{
				this.ProcessSerializedColumnIDs( source.serializedColumnIDs );
				return;
			}

			// if there are no bands on the source just return
			//
			if ( source.Count < 1 )
				return;
			
			ColumnsCollection colCollect = this.group.Band.Columns;
			if ( colCollect == null )
				return;

            // MRS 5/14/2009 - TFS17620
            // Ripped this out because it's causing performance (TFS17620) and serilalization (TFS17457) issues.
            //
            ////try
            //{
                // MRS 5/14/2009 - TFS17620
                // Ripped this out because it's causing performance (TFS17620) and serilalization (TFS17457) issues.
                //
                //// MBS 10/7/08
                //// Discovered while addressing TFS8698.  We should suspend the recalculation
                //// of the metrics while looping through all the columns so that we don't 
                //// inefficiently recalculate the metrics after setting each header's VisiblePosition.
                //colCollect.SuspendMetricsRecalculation();

                for (int i = 0; i < source.Count; i++)
                {
                    // iterate over this bands columns looking for a logical match
                    //
                    for (int j = 0; j < colCollect.Count; j++)
                    {
                        // Call the column's CanInitializeFrom method to see if the column matches
                        // 
                        // MD 8/15/07 - 7.3 Performance
                        // Prevent calling expensive getters multiple times
                        //if ( colCollect[j].CanInitializeFrom(source[i]) )
                        //{
                        //    colCollect[j].Level= source[i].Level;
                        //    colCollect[j].Group = this.group;
                        //    colCollect[j].Header.VisiblePosition = source[i].Header.VisiblePosition;
                        //    break;
                        //}
                        UltraGridColumn column = colCollect[j];

                        if (column.CanInitializeFrom(source[i]))
                        {
                            column.Level = source[i].Level;
                            column.Group = this.group;
                            column.Header.VisiblePosition = source[i].Header.VisiblePosition;
                            break;
                        }
                    }
                }

            // MRS 5/14/2009 - TFS17620
            // Ripped this out because it's causing performance (TFS17620) and serilalization (TFS17457) issues.
            //
            //}
            //finally
            //{
            //    // MBS 10/7/08
            //    // We can resume the recalculation now, which will also trigger the metrics
            //    // to be recalculated at this point.
            //    colCollect.ResumeMetricsRecalculation();
            //}
		}


		/// <summary>
		/// Called when this object is disposed of
		/// </summary>
		protected override void OnDispose() 
		{        
			base.InternalClear();
		}

		/// <summary>
		/// Returns read only status
		/// </summary>
		public override bool IsReadOnly
		{
			get
			{
				return this.readOnly;
			}
		}

		internal void SetReadOnly( bool readOnly )
		{
			this.readOnly = readOnly;
		}


		/// <summary>
		/// Associated Group Object read-only
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridGroup Group
		{
			get
			{
				return this.group;
			}
		}

		
		/// <summary>
		/// indexer 
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn this[ int index ] 
		{
			get
			{
				return (UltraGridColumn)base.GetItem( index );
			}
		}

		
		/// <summary>
		/// indexer (by string key)
		/// </summary>
		public Infragistics.Win.UltraWinGrid.UltraGridColumn this[ String key ] 
		{
			get
			{
				return (UltraGridColumn)base.GetItem( key );
			}
		}

		// MD 1/19/09 - Groups in RowLayout
		// These properties have been moved to the Group, because the columns collection doesn't apply in GroupLayout style
		#region Moved

		

		#endregion Moved

		/// <summary>
		/// Abstract property that specifies the initial capacity
		/// of the collection
		/// </summary>
		protected override int InitialCapacity
		{
			get
			{
				return this.initialCapacity;
			}
		}

		/// <summary>
		/// The collection as an array of objects
		/// </summary>
		public override object[] All 
		{
			// AS 1/8/03 - fxcop
			// Properties should not be write only - even
			// though we know it wasn't anyway
			get { return base.All; }
			set 
			{
				throw new NotSupportedException();
			}
		}

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
        protected override void OnSubObjectPropChanged(PropChangeInfo propChange) 
		{
			// pass the notification along to our listeners
			//
			this.NotifyPropChange( PropertyIds.Column, propChange );
		}

		/// <summary>
		/// Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridColumn'/> to the collection.		
		/// </summary>
        /// <param name="column">The column to add to the group.</param>
        /// <returns>The position into which the new element was inserted.</returns>
		public int Add( UltraGridColumn column ) 
		{
			return this.Add( column, -1, 0 );
		}

        /// <summary>
        /// Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridColumn'/> to the collection.		
        /// </summary>
        /// <param name="column">The column to add to the group.</param>
        /// <param name="visiblePosition">The visible position into which the column should be added.</param>
        /// <returns>The position into which the new element was inserted.</returns>
		public int Add( UltraGridColumn column, int visiblePosition ) 
		{
			return this.Add( column, visiblePosition, 0 );
		}

        /// <summary>
        /// Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridColumn'/> to the collection.		
        /// </summary>
        /// <param name="column">The column to add to the group.</param>
        /// <param name="level">The level into which the column should be inserted.</param>
        /// <returns>The position into which the new element was inserted.</returns>
		public int Add( UltraGridColumn column, short level ) 
		{
			return this.Add( column, -1, level );
		}

        /// <summary>
        /// Adds a <see cref='Infragistics.Win.UltraWinGrid.UltraGridColumn'/> to the collection.		
        /// </summary>
        /// <param name="column">The column to add to the group.</param>
        /// <param name="visiblePosition">The visible position into which the column should be added.</param>
        /// <param name="level">The level into which the column should be inserted.</param>
        /// <returns>The position into which the new element was inserted.</returns>
		public int Add( UltraGridColumn column, int visiblePosition, short level ) 
		{
//			if ( this.readOnly )
//					throw new ArgumentException("This GroupColumns's collection is \'read only\'");

			if ( level < 0 || level >= this.Group.Band.LevelCount )
				throw new ArgumentOutOfRangeException(Shared.SR.GetString("LE_ArgumentOutOfRangeException_148"), level, Shared.SR.GetString("LE_ArgumentOutOfRangeException_311") );

			if ( null == column )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_149") );

			if ( this.group.Band != column.Band )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_153"));

			// JJD 10/22/01
			// Call the column's SetGroup method
			//
			column.SetGroup( this.Group, visiblePosition, level );

			return this.IndexOf( column );
		}

		/// <summary>
		/// Remove column from collection
		/// </summary>
		public void RemoveAt( int index ) 
		{
			this.Remove ( this[ index ] );
		}


		/// <summary>
		/// Remove column from collection
		/// </summary>
		public void Remove( String key ) 
		{
			this.Remove ( this[ key ] );
		}


		/// <summary>
		/// Remove column from collection
		/// </summary>
		public void Remove( UltraGridColumn column ) 
		{
			if ( this.readOnly )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_152"));

			if ( this.IndexOf( column ) >= 0 )
			{
				// unhook prop change notifications
				//
				column.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// remove from collection
				//
				this.InternalRemove( column ); 
			}
		}


		/// <summary>
		/// Clears the collection
		/// </summary>
		public void Clear() 
		{
			if ( this.readOnly )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_152"));

			foreach ( Infragistics.Win.UltraWinGrid.UltraGridColumn column in this )
			{
				// remove the prop change notifications
				//
				column.SubObjectPropChanged -= this.SubObjectPropChangeHandler;

				// JJD 10/03/01 - UWG249
				// null out the column's group reference
				//
				column.InternalSetGroup( null );
			}

			this.InternalClear();
		}

     
		
		/// <summary>
		/// IEnumerable Interface Implementation
		/// </summary>
        /// <returns>A type safe enumerator</returns>
        public GroupColumnEnumerator GetEnumerator() // non-IEnumerable version
		{
			return new GroupColumnEnumerator(this);
		}

		
		/// <summary>
		/// Returns a read-only clone of the collection
		/// </summary>
        /// <returns>a read-only clone of the collection</returns>
		public GroupColumnsCollection Clone() 
		{
			GroupColumnsCollection clone = new GroupColumnsCollection(this.group);
			clone.SetReadOnly( true );

			if ( clone != null )
			{
				for(int index = 0; index < this.Count; index++)
				{
					clone.InternalAdd(this[index]);
				}
			}
			return clone;
		}

		
		/// <summary>
		/// Called by GetWidthRange to adjust the calulated min/max range to accommodate each level.
		/// </summary>
		/// <param name="level"></param>
		/// <param name="levelWidth"></param>
		/// <param name="levelMinWidth"></param>
		/// <param name="levelMaxWidth"></param>
		/// <param name="maxActualWidth"></param>
		/// <param name="minWidth"></param>
		/// <param name="maxWidth"></param>
		protected void AdjustWidthRangeByLevel(	short		level,
												int		levelWidth, 
												int		levelMinWidth, 
												int		levelMaxWidth,
												ref int	maxActualWidth, 
												ref int	minWidth, 
												ref int	maxWidth )
		{
			
			maxActualWidth = Math.Max( maxActualWidth, levelWidth );

			if ( levelMinWidth > 0 )
				minWidth   = Math.Max ( minWidth, levelMinWidth );
    
			if ( levelMaxWidth > 0 )
			{
				if ( maxWidth == 0 )
					maxWidth   = level > 0 ? 0 : levelMaxWidth;
				else
					maxWidth   = Math.Min ( maxWidth, levelMaxWidth );
			}
			else
				if ( levelMaxWidth == 0 )

				maxWidth       = 0;
		}


		internal void GetColumnWidthRange( bool		byMouse,
										bool		adjustGroupWidthAlso, 
										UltraGridColumn		column, 
										ref int	minWidth, 
										ref int	maxWidth )
		{
			int		itemLevel          = column.Level;
			int    levelWidth         = 0;
			int    widthToTheRight    = 0;
			int    minWidthToTheRight = 0;
			int    maxWidthToTheRight = 0;
			bool    columnFound        = false;
		
		
			// if the passed in column can't be resized then return its
			// actual width as the min and max values
			//
			if ( column.LockedWidth )
			{
				minWidth = maxWidth = column.GetActualWidth( false );
				return;
			}

			// iterate over the columns collection calculating the min/max width for each level
			//
			for( int index = 0; index < this.Count; index++ )
			{
				// bypass columns until we get to the level of the passed in column
				//
				if( this[index].Level < itemLevel )
					continue;
				// break out of loop when we get past the level
				//
				if ( this[index].Level  > itemLevel )
					break;

				if ( column    == this[index] )
				{
					columnFound        = true;
				}
				else
				{
					// if the column has already been found we want to accumulate the 
					// min/max and actual width totals for all columns to its right
					//
					if ( columnFound )
					{
						widthToTheRight += this[index].GetActualWidth(false);

						// determine if this column can be resized at all
						//
						if ( this[index].IsWidthFixed(byMouse, false, null) )
						{
							minWidthToTheRight  += this[index].GetActualWidth(false);
							maxWidthToTheRight  += this[index].GetActualWidth(false);
						}
						else 
						{
							// accumulate the min width values
							//
							minWidthToTheRight      += (int)this[index].MinWidth;

							// If the max width of a column is zero set the unlimitedWidthColFound 
							// flag to true. Otherwise, accumulate the max width values.
							//
							if ( this[index].MaxWidth != 0 )
								maxWidthToTheRight  += (int)this[index].MaxWidth;
						}
					}// end of	if ( columnFound )
				}// end of else

				levelWidth += this[index].GetActualWidth(false);
			} // end of for loop

			if ( columnFound )
			{
				// get the current width of the group
				//
				minWidth = (int)column.MinWidth;

				if ( adjustGroupWidthAlso )
				{
					int groupWidth    = 0;    
					int groupMinWidth = 0;
					int groupMaxWidth = 0;

					// calculate the min/max for the entire group
					//
					GetGroupWidthRange( byMouse, false, ref groupMinWidth, ref groupMaxWidth, ref groupWidth );

					// Since we are resizing the group as well we need to constrain
					// both the maximum width value we return so that
					// they don't violate the group's max.
					//
					if ( groupMaxWidth == 0 )
						maxWidth = (int)column.MaxWidth;
					else 
						if ( column.MaxWidth == 0 )
						maxWidth = (int)column.GetActualWidth(false) + groupMaxWidth - levelWidth;
					else
						maxWidth = (int)Math.Min( column.GetActualWidth(false) + groupMaxWidth - levelWidth,
							column.MaxWidth );
				}
				else
				{
					int groupWidth =	this.group.GetActualWidth( false );
			
					// calcluate the amount of space currently unused by this level's columns
					//
					int levelUnusedSpace = ( groupWidth > levelWidth )
						? groupWidth - levelWidth : 0;   

					// we are not resizing the group also so we need to constrain the
					// max value by the min/max range of the column's to the right of the
					// the passed in column in the group
					// 
					maxWidth = column.GetActualWidth( false ) + levelUnusedSpace + widthToTheRight - minWidthToTheRight;
			
					if ( column.MaxWidth > 0 )
						maxWidth = (int)Math.Min( maxWidth, column.MaxWidth );
				}
			}

			else
			{
				Debug.Assert( columnFound, "Column not found inside CGroupColsColl::GetColumnWidthRange");

				// init the return values to the group's min/max width
				//
				minWidth = (int)column.MinWidth;
				maxWidth = (int)column.MaxWidth;
			}
		}

		internal void GetGroupWidthRange( bool		byMouse, 
										bool		groupResize, 
										ref int	minWidth, 
										ref int	maxWidth,
										ref int	width )
		{
			short   itemLevel;
			short   currLevel              = -1; // init to -1 to trigger first time logic 
			int    levelWidth             = 0;
			int    levelMinWidth          = 0;
			int    levelMaxWidth          = 0;
			bool    unlimitedWidthColFound = false;

			// initialize out parameters
			width      = 0;
			minWidth   = 0;
			maxWidth   = 0;

			// iterate over the columns collection calculating the min/max width for each level
			for(int index = 0; index < this.Count; index++ )
			{
				// get the item's level number
				//
				itemLevel = (short)this[index].Level;

				// MD 7/26/07 - 7.3 Performance
				// FxCop - Remove unused locals
				//UltraGridColumn col = this[index];

				if ( currLevel < 0 )
				{
					// first time thru just initialize current level
					//
					currLevel = itemLevel;
				}
				else
					if ( currLevel != itemLevel ) // level break
					{
						Debug.Assert( currLevel < itemLevel, "Level numbers should always go up in a group's columns" );

						// if we found at least one column in the last level that had can have an
						// unlimited width then set maxwidth to zero
						//
						if ( unlimitedWidthColFound )
							levelMaxWidth      = 0;

						// aggregate this level's min/max with previous levels
						//
						AdjustWidthRangeByLevel( currLevel, levelWidth, levelMinWidth, levelMaxWidth,
												ref width, ref minWidth, ref maxWidth );

						// The level has changed so keep track of the new level and
						// reset the min/max variables for the next level
						//
						currLevel          = itemLevel;
						levelWidth         = 0;
						levelMinWidth      = 0;
						levelMaxWidth      = 0;
					}

				// JJD 10/03/01 - UWG467
				// Use GetActualWidth instead since it will return 0
				// if the column is hidden
				//
				//levelWidth +=	this[index].Width;	
				levelWidth +=	this[index].GetActualWidth();

				// determine if this column can be resized at all
				//
				if ( this[index].IsWidthFixed(byMouse, groupResize, null) )
				{
					levelMinWidth      += this[index].GetActualWidth( false );
					levelMaxWidth      += this[index].GetActualWidth( false );
				}
				else 
				{
					// accumulate the min width values
					//
					levelMinWidth      += (int)this[index].MinWidth;

					// If the max width of a column is zero set the unlimitedWidthColFound 
					// flag to true. Otherwise, accumulate the max width values.
					//
					if ( this[index].MaxWidth == 0 )
						unlimitedWidthColFound  = true;
					else
						levelMaxWidth  += (int)this[index].MaxWidth;
				}

			}

			// if we found at least one column in the last level that had can have an
			// unlimited width then set maxwidth to zero
			//
			if ( unlimitedWidthColFound )
				levelMaxWidth      = 0;

			if ( currLevel > -1 )
			{
				// aggregate the last level's min/max with previous levels
				//
				AdjustWidthRangeByLevel( currLevel, levelWidth, levelMinWidth, levelMaxWidth,
										ref width, ref minWidth, ref maxWidth );
			}

			if ( groupResize )
			{
				// Use a stack variable to hold the temp value instead of of placing the method call 
				// inside the 'max' macro below since that causes the method to be called twice
				// 
				int temp = group.GetActualWidth( false );

				if ( minWidth > 0 )
					width = Math.Max ( minWidth, temp );

				if ( maxWidth > 0 )
					width = Math.Min ( maxWidth, temp );
			}
			
		}

		
		/// <summary>
		/// Adjusts the width of column's in each level to make sure the level width doesn't exceed the group width
		/// </summary>
		/// <param name="indexStart"></param>
		/// <param name="byMouse"></param>
		/// <param name="groupResize"></param>
		/// <param name="duringDrag"></param>
		/// <param name="groupWidth"></param>
		/// <param name="columnAdded"></param>
		/// <param name="column"></param>
		protected void AdjustLevelWidths( int	  indexStart,
										  bool	  byMouse, 
										  bool    groupResize,
										  bool    duringDrag,
										  int    groupWidth,
										  bool    columnAdded,
										  UltraGridColumn  column ) 
		{
			int indexEnd=0;

			short   proportionalCols = 0;
			short   proportionalUnlimitedCols = 0;
			bool    fixedWidth;
			int    levelWidth = 0;
			int    deltaWidth;
			int    width = 0;
			int    minWidth = 0;
			int    maxWidth = 0;
			int    fixedWidthLength = 0;

			// MD 7/26/07 - 7.3 Performance
			// FxCop - Remove unused locals
			//int    passedInColumnWidth = 0;

			int    resizableWidth = 0;
			int    resizableMinWidth = 0;
			int    resizableMaxWidth = 0;
			int    proportionalWidth = 0;
			int    proportionalMinWidth = 0;
			bool    unlimitedResizableColFound  = false;
			bool    passedInColumnFound = false;

			// get the level number
			//
			int itemLevel = this[indexStart].Level;

			// iterate over the columns in this level looking for candidates for resizing
			// 
			for( int index = indexStart; index < this.Count; index++ )
			{
				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				#region Old Code

				//if ( this[index].Hidden )
				//    continue;

				//if ( itemLevel != this[index].Level )
				//    break;

				//width      = this[index].GetActualWidth(true);
				//minWidth   = (int)this[index].MinWidth;
				//maxWidth   = (int)this[index].MaxWidth;

				//fixedWidth = this[index].IsWidthFixed( byMouse, groupResize, null );

				#endregion Old Code
				UltraGridColumn currentColumn = this[ index ];

                // MRS 2/23/2009 - Found while fixing TFS14354
				//if ( currentColumn.Hidden )
                if (currentColumn.HiddenResolved)
					continue;

				if ( itemLevel != currentColumn.Level )
					break;

				width = currentColumn.GetActualWidth( true );
				minWidth = (int)currentColumn.MinWidth;
				maxWidth = (int)currentColumn.MaxWidth;

				fixedWidth = currentColumn.IsWidthFixed( byMouse, groupResize, null );



				// SSP 10/24/03 UWG2264
				// If the maxWidth and minWidth are the same so that the column can not
				// be resized, then treat it as if it were fixed width column.
				//
				// --------------------------------------------------------------------
				if ( maxWidth > 0 && maxWidth - minWidth <= 0 )
					fixedWidth = true;
				// --------------------------------------------------------------------

				// If we found the passed in column then set the flag
				//
				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//if ( column != null && column == this[index] )
				if ( column != null && column == currentColumn )
					passedInColumnFound = true;

				if ( fixedWidth )
				{
					// accumulate the width of all the fixed headers in this level
					//
					fixedWidthLength += width;
				}
				else
					// MD 8/15/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( column != null && column == this[index] )
					if ( column != null && column == currentColumn )
					{
						// save the width of the passed in column (which
						// we know is not fixed) since fixedWidth is false
						//
						// MD 7/26/07 - 7.3 Performance
						// FxCop - Remove unused locals
						//passedInColumnWidth    = width;
					}
				else
				{
					// SSP 12/10/02 UWG1768
					// Use the Resolved version of the property.
					//
					//if ( this[index].ProportionalResize )
					// MD 8/15/07 - 7.3 Performance
					// Prevent calling expensive getters multiple times
					//if ( this[index].ProportionalResizeResolved )
					if ( currentColumn.ProportionalResizeResolved )
					{
						proportionalCols++;

						// accumulate the width of all proportional resizable columns in this level
						//
						proportionalWidth      += width;
						proportionalMinWidth   += minWidth;

						if ( 0 == maxWidth )
							proportionalUnlimitedCols++;
					}
					else
					{
						// accumulate the width of all (non-proportional) resizable columns in this level
						//
						resizableWidth    += width;
						resizableMinWidth += minWidth;
						resizableMaxWidth += maxWidth;

						if ( 0 == maxWidth )
							unlimitedResizableColFound  = true;
					}
				}

				// MD 8/15/07 - 7.3 Performance
				// Prevent calling expensive getters multiple times
				//levelWidth += this[index].GetActualWidth(true);
				levelWidth += currentColumn.GetActualWidth( true );

				// keep track of the iterator that points to the last column in this level
				//
				indexEnd = index;
			}

			// if there are no visible columns in this level then just return
			//
			if ( levelWidth == 0 )
				return;

			// If the passed in column is on this level then we don't need 
			// to adjust any of the other columns
			//
			if ( passedInColumnFound )
				return;

			// calculate the total amount we need to adjust the column widths
			//      
			deltaWidth = groupWidth - levelWidth;

			if ( deltaWidth == 0 )
				return;

			// if we found unlimited width columns in either group
			// reset their respective maxwidth accumulators 
			//
			if ( unlimitedResizableColFound )
				resizableMaxWidth = 0;

			// keep track of how much we still have to adjust
			//
			int deltaWidthRemaining = deltaWidth;
			int proportionColsDeltaWidth = 0;
			int colDelta;

			// Added totalAdjusted variable to keep a running total of all adjustments 
			//
			int totalAdjusted = 0;

			// if we have any proportional resize columns in this level adjust them first
			//
			if ( proportionalCols > 0 )
			{
				int proportionalColsRemaining = proportionalCols;

				if ( deltaWidth < 0 )
				{
					proportionColsDeltaWidth   = deltaWidthRemaining = Math.Max ( deltaWidth, proportionalMinWidth - proportionalWidth );
				}
				else
				{
					// The delta needs to be based on the entire level width
					//
					proportionColsDeltaWidth   = deltaWidthRemaining = groupWidth - levelWidth;
				}

				// Save the total delta for proportional calculations below
				//
				int totalProportionColsDeltaWidth = proportionColsDeltaWidth;

				// iterate over the columns in this level again adjusting the proportional
				// resizable columns only
				for( int index = indexStart; index < this.Count; index++ )
				{
					// break out when we don't have any delta left
					//
					if ( deltaWidthRemaining == 0  )
						break;

					if ( itemLevel != this[index].Level )
						break;

					fixedWidth = this[index].IsWidthFixed( byMouse, groupResize, null );

					if ( !fixedWidth && !( column != null && column == this[index] )
						// SSP 12/10/02 UWG1768
						// Use the Resolved version of the property.
						//
						//&& this[index].ProportionalResize 
						&& this[index].ProportionalResizeResolved
						)
					{
						width = this[index].GetActualWidth(true);

						// Decrement the proportionalColsRemaining count.
						//
						// Note: This allows us to use the deltaWidthRemaining for
						// the last proportional column so we can compensate
						// for any cumulative pixel rounding errors.
						//
						proportionalColsRemaining--;

						// SSP 12/10/02 UWG1768
						// Moved these from below if and else blocks. We need to get both of them. Before,
						// we one line was in the if block and the other line was in the else block and we
						// need to use both of them after the if-else statement.
						//
						minWidth   = (int)this[index].MinWidth;
						maxWidth   = (int)this[index].MaxWidth;

						// calculate the column's delta
						//
						if ( proportionColsDeltaWidth < 0 )
						{
							// SSP 12/10/02 UWG1768
							// Moved this before the if statement.
							//
							//minWidth   = (int)this[index].MinWidth;

							if ( proportionalColsRemaining > 0 ) // see note above

								// Use the total orig delta and take our propertion based on this 
								// column's width / the width or all proportionally sized columns
								//
								colDelta = (totalProportionColsDeltaWidth * width) / proportionalWidth;
							else
								colDelta = Math.Max ( deltaWidthRemaining, minWidth - width );
                    
							// make sure we don't adjust too far(shouldn't unnecessary)
							//
							colDelta = Math.Max ( deltaWidthRemaining, colDelta );
						}
						else
						{
							// SSP 12/10/02 UWG1768
							// Moved this before the if statement.
							//
							//maxWidth   = (int)this[index].MaxWidth;
    
							if ( proportionalColsRemaining > 0 ) // see note above

								// Use the total orig delta and take our propertion based on this 
								// column's width / the width or all proportionally sized columns
								//
								colDelta = (totalProportionColsDeltaWidth * width) / proportionalWidth;
							else if ( maxWidth > 0 )
								colDelta = Math.Min( deltaWidthRemaining, maxWidth - width );
							else
								colDelta = deltaWidthRemaining;

							// make sure we don't adjust too far(shouldn't unnecessary)
							//
							colDelta = Math.Min( deltaWidthRemaining, colDelta );
						}

						if ( colDelta != 0 )
						{
							// SSP 12/10/02 UWG1768
							// 
							// -------------------------------------------------------------------------------
							//int tempWidth = Math.Max ( minWidth, Math.Max ( maxWidth, width + colDelta ) );
							int tempWidth = width + colDelta;
							
							// SSP 12/11/02
							// Give higher priority to minWidth than the maxWidth. Switched below two
							// if statements.
							//
							if ( maxWidth > 0 && tempWidth > maxWidth )
								tempWidth = maxWidth;

							if ( minWidth > 0 && tempWidth < minWidth )
								tempWidth = minWidth;							
							// -------------------------------------------------------------------------------
							
							colDelta = tempWidth - width;

							// update the deltaWidthRemaining
							//
							deltaWidthRemaining -= colDelta;
                    
							// update the column's width
							//
							if ( duringDrag )
								this[index].SetWidthDuringDrag(width + colDelta);
							else
								// SSP 12/10/02 UWG1768
								// Added enforceColumnMinWidthMaxWidth parameter to SetWidth method. Pass true to it. What used to happen
								// before was the SetWidth was constraining the width of the column to the group's right edge. However we
								// we are at a point in the execution where the group's width has nott ben set yet. So it ends up
								// constraining the column width to the group's old width.
								//
								//this[index].SetWidth(width + colDelta, byMouse, false, false, false, groupResize );
								this[index].SetWidth(width + colDelta, byMouse, false, false, false, groupResize, true );

							// Keep a running total of all adjustments made so far.
							//
							totalAdjusted += colDelta;
						}
					}
				}
			}

			// Calculate the delta remaining.
			//
			deltaWidthRemaining  = deltaWidth - totalAdjusted;

			if ( deltaWidthRemaining != 0 )
			{
				// Since we will be iterating in reverse the first column we
				// process is actually the last column in this level
				//
				bool lastVisibleItemInLevel = true;

				// iterate over the columns in this level in reverse order adjusting any non-proportional 
				// columns as much as they can be adjusted until the deltaWidthRemaining is used up
				//
				for( int index = indexEnd; index >= 0; index-- )
				{
					if( itemLevel != this[index].Level )
						break;

					// Only process visible columns
					//
                    // MRS 2/23/2009 - Found while fixing TFS14354
					//if ( !this[index].Hidden )
                    if (!this[index].HiddenResolved)
					{
						fixedWidth = this[index].IsWidthFixed( byMouse, groupResize, null );

						if (    !fixedWidth
							&& !( column != null && column == this[index] )
							// SSP 12/10/02 UWG1768
							// Use the Resolved version of the property.
							//
							//&& !this[index].ProportionalResize
							&& !this[index].ProportionalResizeResolved
							)
						{

							width = this[index].GetActualWidth(true);

							// calculate the column's delta (goes as far as the column will permit
							//
							if ( deltaWidth < 0 )
							{
								minWidth   = (int)this[index].MinWidth;

								colDelta  = Math.Max ( deltaWidthRemaining, minWidth - width );
							}
							else
							{
								maxWidth   = (int)this[index].MaxWidth;
    
								if ( maxWidth == 0 )
									colDelta = deltaWidthRemaining;
								else
									colDelta = Math.Min( deltaWidthRemaining, maxWidth - width );
							}

							if ( colDelta != 0 )
							{
								// update the deltaWidthRemaining
								//
								deltaWidthRemaining -= colDelta;
                    
								// During an individual column resize, don't bother changing
								// the width of the last visible column on a level since it will
								// automatically be adjusted to fit the group
								//
								if ( !lastVisibleItemInLevel || groupResize )
								{
									// update the column's width
									//
									if ( duringDrag )
										this[index].SetWidthDuringDrag(width + colDelta);
									else
										// SSP 12/10/02 UWG1768
										// Added enforceColumnMinWidthMaxWidth parameter to SetWidth method. Pass true to it. What used to happen
										// before was the SetWidth was constraining the width of the column to the group's right edge. However we
										// we are at a point in the execution where the group's width has nott ben set yet. So it ends up
										// constraining the column width to the group's old width.
										//
										//this[index].SetWidth(width + colDelta, byMouse, false, false, false, groupResize );
										this[index].SetWidth(width + colDelta, byMouse, false, false, false, groupResize, true );
								}

								// break out when we don't have any delta left
								//
								if ( deltaWidthRemaining == 0 )
									break;
							}

						}

						// Reset the lastVisibleItemInLevel flag
						//
						lastVisibleItemInLevel = false;
					}

					// break out when we hit the beginning of the array 
					// 
					if ( this[index] == this[0] )
						break;
				}
			}
		}

		internal void AdjustGroupWidth( bool byMouse, bool groupResize, bool duringDrag )
		{
			int    minWidth      = 0;
			int    maxWidth      = 0;
			int    width         = 0;

			GetGroupWidthRange( byMouse, groupResize, ref minWidth, ref maxWidth, ref width );

			// call setwidth which won't do anything if the width is the same
			//
			if ( duringDrag )
				this.group.SetWidthDuringDrag(width);
			else
				this.group.SetWidth(width, byMouse, false, false, true );
		}

		internal void AdjustAllColumnWidths(	bool	byMouse, 
											bool    groupResize,
											bool    duringDrag,
											bool    fireGroupResizeEvent, 
											bool    columnAdded,
											UltraGridColumn	column )
		{
			short   itemLevel;
			short   currLevel     = -1; // init to -1 to trigger first time logic 
			int    minWidth      = 0;
			int    maxWidth      = 0;
			int    width         = 0;

			GetGroupWidthRange( byMouse, groupResize, ref minWidth, ref maxWidth, ref width );

			// make sure the current width is in the valid mio/max range
			//
			int    groupWidth    =  Math.Max( minWidth, width );

			if ( maxWidth > 0 )
				groupWidth    = Math.Min( groupWidth, maxWidth );

			for( int index = 0; index < this.Count; index++ )
			{
				// get the item's level number
				//
				itemLevel = (short)this[index].Level;

				if ( currLevel < 0 )
				{
					// first time initialize current level
					//
					currLevel      = itemLevel;

					AdjustLevelWidths( index, byMouse, groupResize, duringDrag, groupWidth, columnAdded, column );
				}
				else
					if ( currLevel != itemLevel ) // level break
					{
						Debug.Assert( currLevel < itemLevel, "Level numbers should always go up in a group's columns" );

						// The level has changed so keep track of the new level 
						currLevel = itemLevel;

						AdjustLevelWidths( index, byMouse, groupResize, duringDrag, groupWidth, columnAdded, column );
					}

			}

			// call setwidth which won't do anything if the width is the same
			//
			if ( duringDrag )
				this.group.SetWidthDuringDrag( groupWidth );
			else
				this.group.SetWidth(groupWidth, byMouse, fireGroupResizeEvent, false, false );
		}


		internal bool GetPosition(UltraGridColumn column, ref int visiblePosition, ref short level)
		{
			int   tmpVisiblePosition    = -1; // init to -1 in case column isn't found
			short itemLevel			= -1;
			short currLevel          = -1; // init to -1 to trigger first time logic 
			int   levelPosition      = 0;

			// iterate over the columns collection looking for the passed in column
			for(int index = 0; index < this.Count; index++)
			{
				// get the item's level number
				//
				itemLevel = (short)this[index].Level;

				if ( currLevel < 0 )
				{
					// first time thru just initialize current level
					//
					currLevel      = itemLevel;
				}
				else
					if ( currLevel != itemLevel )
				{
					Debug.Assert( currLevel < itemLevel, "Level numbers should always go up in a group's columns");
        
					// The level has changed so keep track of the new level and
					// reset the visible position to 0
					//
					currLevel      = itemLevel;
					levelPosition  = 0;
				}
				else
				{
					// we are on the same level so just increment the visible position
					//
					levelPosition++;
				}

				if ( this[index] == column )
				{
					tmpVisiblePosition = levelPosition;
					break;
				}
			}

			visiblePosition = tmpVisiblePosition;

			level = itemLevel;

			return true;
		}

		internal bool SetPosition(UltraGridColumn column, short newPosition, short newLevel, ref bool changed)
		{
			if ( this.readOnly )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_152"));

			// init out param
			//
			changed = false;

			// get the column's current position 
			//
			int position = -1;
			short level = -1;
    
			if (GetPosition(column, ref position, ref level)== false )
				return false;

			// if the column not found assert and return
			//
			if ( position < 0 )
			{
				Debug.Assert( false, "Column not in group, can't set its visible position");
				return false;
			}

			// if the VisiblePosition or Level hasn't changed then just return 
			//
			if ( position == newPosition &&
				level    == newLevel )
				return true;

			// first remove the column
			//
			this.Remove( column );

			// JJD 10/22/01
			// Reset the column's group
			//
			column.InternalSetGroup( this.Group );

			// then re-add it back in the correct spot
			//
			this.InternalAdd( column, newPosition, newLevel );

			changed = true;

			return true;
		}

		internal bool SetVisiblePosition(UltraGridColumn column, short newPosition, ref bool changed )
		{
			if ( this.readOnly )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_152"));

			short level = -1;
	    
			if (!GetLevel(column, ref level) )
				return false;

			return SetPosition(column, newPosition, level, ref changed);
		}


		//
		//
		internal bool GetVisiblePosition(UltraGridColumn column, ref int visiblePosition )
		{
			short level = -1;
			return GetPosition(column, ref visiblePosition, ref level );
		}


		//
		//
		internal bool SetLevel(UltraGridColumn column, short newLevel, ref bool changed)
		{
			if ( this.readOnly )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_152"));

			return SetPosition(column, -1, newLevel, ref changed);
		}


		//
		//
		internal bool GetLevel(UltraGridColumn column, ref short newLevel)
		{
			int position = -1;
			return GetPosition(column, ref position, ref newLevel);
		}


		 
		/// <summary>
		/// Swaps two columns in the group, taking Levels and VisiblePosition into account.
		/// </summary>
		/// <param name="ColOne">An UltraGridColumn</param>
        /// <param name="ColTwo">An UltraGridColumn</param>
		public void Swap(UltraGridColumn ColOne, UltraGridColumn ColTwo)
		{
			if ( this.readOnly )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_152"));

			int ColOnePos=-1;
			int ColTwoPos=-1;
			UltraGridColumn tmpColOne = null,  tmpColTwo = null;

			for(int index = 0; index < this.Count; index++)
			{
				//check if the columns are in the collection, if so
				//then save index and make a temporary copy
				if(ColOne == this[index])
				{	ColOnePos = index;
					tmpColOne = this[index];
				}
				else if (ColTwo == this[index])
				{	ColTwoPos = index;
					tmpColTwo = this[index];
				}
			}

			//if columns are not part of the collection, then can't swap
			if( ColOnePos == -1 || ColTwoPos == -1 )
				throw new System.ArgumentException(Shared.SR.GetString("LE_System.ArgumentException_152"));

			// Save the Level's the columns are currently at.
			int colOneLevel = ColOne.Level;
			int colTwoLevel = ColTwo.Level;

			// Set the levels 
			tmpColOne.InternalSetLevel( colTwoLevel, false );	//.Level = colTwoLevel;
			tmpColTwo.InternalSetLevel(	colOneLevel, false );	//Level = colOneLevel;

			//swap columns
			this.List[ColOnePos] = this.List[ColTwoPos];
			this.List[ColTwoPos] = tmpColOne;

			// Let the Group adjust width and reflect the changes
			AdjustGroupWidth(false, false, false);

			this.NotifyPropChange( PropertyIds.Columns, null );
		}

		internal int InternalAdd( UltraGridColumn column ) 
		{
//			if ( this.readOnly )
//					throw new ArgumentException("This GroupColumns's collection is \'read only\'");

			if ( null == column ||
				this.group.Band != column.Band )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_153"));

			// hook up the prop change notifications
			//
			column.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			return base.InternalAdd( column );
		}

		internal void InternalInsert(int index, UltraGridColumn column ) 
		{
			if ( this.readOnly )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_152"));

			if ( null == column ||
				this.group.Band != column.Band )
				throw new ArgumentException(Shared.SR.GetString("LE_ArgumentException_153"));

			// hook up the prop change notifications
			//
			column.SubObjectPropChanged += this.SubObjectPropChangeHandler;

			base.InternalInsert(index, column );
		}

		internal void InternalAdd( UltraGridColumn  column, int visiblePosition, int level)
		{
			int    index          = 0;
			int    insertAt       = -1; // init to -1 to default to doing an append
			int		itemLevel;
			int		currLevel      = -1; // init to -1 to trigger first time logic 
			int		levelPosition  = 0;

			for ( int i = 0; i < this.Count; i++ )
			{
				// get the item's level number
				//
				itemLevel = this[i].Level;

				if ( currLevel < 0 )
				{
					// first time thru just initialize current level
					//
					currLevel      = itemLevel;
				}
				else
					if ( currLevel != itemLevel )
				{
					Debug.Assert ( currLevel < itemLevel, "Level numbers should always go up in a group's columns" );
        
					// The level has changed so keep track of the new level and
					// reset the visible position to 0
					//
					currLevel      = itemLevel;
					levelPosition  = 0;
				}
				else
				{
					// we are on the same level so just increment the visible 
					// position
					//
					levelPosition++;
				}

				if ( level == currLevel )
				{
					// If the requested level and visible position match
					// insert it here
					//
					if ( visiblePosition == levelPosition )
					{
						insertAt = index;
						break;
					}
				}
				else
					// If the requested level is less than the current level
					// insert at the end of the last level
					//
					if ( level < currLevel )
					{
						insertAt = index;
						break;
					}

				// increment the index
				//
				index++;
			}

			if( insertAt >= 0 )
				this.InternalInsert ( (int)insertAt, column );
			else
				this.InternalAdd ( column );

			// Set the columns level number to the passed in level
			// before calling AdjustGroupWidth
			//
			column.InternalSetLevel( level, false );

			this.Group.OnColumnsChanged();

			this.AdjustGroupWidth( false, false, false );
		}

		internal void InternalRemove( UltraGridColumn column )
		{
			base.InternalRemove( column );

			// JJD 10/03/01 - UWG249
			// null out the column's group reference
			//
			column.InternalSetGroup( null );

			this.Group.OnColumnsChanged();

			this.AdjustGroupWidth( false, false, false );
		}

		internal void InitBand(UltraGridBand band, UltraGridGroup group)
		{
			this.group = group;
			this.ProcessSerializedColumnIDs( this.serializedColumnIDs );
		}

		internal void InitDuringDeserialization( UltraGridColumn [] columns )
		{
			base.InternalClear();

			for ( int i = 0; i < columns.Length; i++ )
			{
				if ( columns[ i ] != null )
					base.InternalAdd( columns[ i ] );
			}

			// SSP 7/16/04 UWG3500
			// Call AdjustGroupWidth to initialize the group width. We are not doing this
			// lazily so we need to call it after anything in the group is modified. If 
			// thix fix is found to be unsuitable then viability of performing lazy 
			// initialization of group width should be considered.
			//
			this.AdjustGroupWidth( false, false, false );
		}

		// MD 1/21/09 - Groups in RowLayouts
		#region IEnumerable<UltraGridColumn> Members

		IEnumerator<UltraGridColumn> IEnumerable<UltraGridColumn>.GetEnumerator()
		{
			foreach ( UltraGridColumn column in this )
				yield return column;
		}

		#endregion

	}//end of class

    /// <summary>
    /// Enumerator for the GroupColumnsCollection
    /// </summary>
    public class GroupColumnEnumerator: DisposableObjectEnumeratorBase
    {   
		/// <summary>
		/// Constructor
		/// </summary>
        /// <param name="columns">The GroupColumnsCollection to enumerate.</param>
        public GroupColumnEnumerator( GroupColumnsCollection columns ) : base( columns )
        {
        }

      
		/// <summary>
		/// Type-safe version of Current
		/// </summary>
        public Infragistics.Win.UltraWinGrid.UltraGridColumn Current 
        {
            get
            {
                return (UltraGridColumn)((IEnumerator)this).Current;
            }
        }
    }
}
