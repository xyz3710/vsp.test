#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

namespace Infragistics.Win.UltraWinGrid
{
	using System;
	using System.Drawing;
	using System.Collections;
	using System.Globalization;
	using System.ComponentModel;
	using System.ComponentModel.Design;
	using System.ComponentModel.Design.Serialization;
	using System.Reflection;
	using System.Windows.Forms;
	using System.Diagnostics;
	using Infragistics.Shared;
	using Infragistics.Win;
	using System.Runtime.Serialization;
	using Infragistics.Win.UltraWinMaskedEdit;
	using System.Drawing.Design;
	using System.Windows.Forms.Design;
	using System.Text;
	using System.Security.Permissions;
	using Infragistics.Shared.Serialization;

	// SSP 3/21/02
	// Version 2 Row Filter Implementation
	// Added ColumnFilter class
	//


	/// <summary>
	/// ColumnFilter class.
	/// </summary>
	/// <remarks>
	/// <p class="body">
	/// A <b>FilterCondition</b> object defines a single condition.
	/// Multiple FilterCondition instances can be added to the 
	/// <see cref="FilterConditionsCollection"/>. 
	/// A <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/> instance
	/// contains a <b>FilterConditionsCollection</b> instance. The <b>ColumnFilter</b>
	/// has <see cref="ColumnFilter.LogicalOperator"/> property which specifies
	/// how multiple conditions contained in the ColumnFilter's FilterConditionCollection
	/// are to be combined.
	/// A <see cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/> 
	/// can contain multiple <b>ColumnFilter</b> instances. Both the
	/// <see cref="UltraGridBand"/> and <see cref="RowsCollection"/> objects expose
	/// <b>ColumnFilters</b> property. This property returns a collection of
	/// <see cref="Infragistics.Win.UltraWinGrid.ColumnFilter"/> objects.
	/// UltraGrid will filter rows using either the 
	/// RowsCollection's <see cref="RowsCollection.ColumnFilters"/> or 
	/// UltraGridBand's <see cref="UltraGridBand.ColumnFilters"/> depending on the
	/// what the Override's <see cref="UltraGridOverride.RowFilterMode"/> property
	/// is set to. See <see cref="UltraGridOverride.RowFilterMode"/> for more information.
	/// </p>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterCondition"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.FilterConditionsCollection"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFilter.FilterConditions"/>
	/// <seealso cref="Infragistics.Win.UltraWinGrid.ColumnFiltersCollection"/>
	/// <seealso cref="UltraGridBand.ColumnFilters"/>
	/// <seealso cref="RowsCollection.ColumnFilters"/>
	/// <seealso cref="UltraGridOverride.RowFilterMode"/>
	/// <seealso cref="UltraGridOverride.FilterUIType"/>
	/// </remarks>
	[ Serializable() ]
	public class ColumnFilter : KeyedSubObjectBase, ISerializable
	{
		#region Private vars

		private UltraGridColumn column = null;
		private FilterConditionsCollection filterConditions = null;
		private Infragistics.Win.UltraWinGrid.FilterLogicalOperator logicalOperator;

		private SerializedColumnID serializedColumnId = null;

		// SSP 4/6/05 - NAS 5.2 Filter Row
		//
		internal object filterRowOperandValue = null;
		internal object filterRowOperatorValue = null;
		internal object filterRowOperatorEditorContext = null;
		internal bool filterOperandModifiedSinceLastApply = false;
		internal bool filterOperatorModifiedSinceLastApply = false;

		// SSP 10/23/07 BR25866
		// 
		internal string displayTextOverride = null;

		#endregion // Private vars

		#region Constructor

		internal ColumnFilter( UltraGridColumn column )
		{
			if ( null == column )
				throw new ArgumentNullException( Shared.SR.GetString("LE_ArgumentNullException_149"), Shared.SR.GetString("LE_ArgumentNullException_103") );

			this.column = column;

			// By default we are going to use logical operator of And.
			//
			logicalOperator = Infragistics.Win.UltraWinGrid.FilterLogicalOperator.And;
		}

		// SSP 8/1/03 UWG1654 - Filter Action
		// Added a way for the user to be able to construct filter conditions and evaluate
		// it themselves by calling the new UltraGridRow.MeetsCriteria method.
		// Added the following constructor.
		//
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="column">This column's values will be compared against the filter conditions in this column filter.</param>
		/// <param name="logicalOperator">How to combine the filter conditions in this column filter.</param>
		/// <remarks>
		/// <seealso cref="ColumnFilter.FilterConditions"/> <seealso cref="FilterCondition"/> <seealso cref="FilterLogicalOperator"/>
		/// </remarks>
		public ColumnFilter( UltraGridColumn column, FilterLogicalOperator logicalOperator ) : this( column )
		{
			this.logicalOperator = logicalOperator;	
		}

//#if DEBUG
//		/// <summary>
//		/// Deserialization constructor.
//		/// </summary>
//#endif
		/// <summary>
		/// Constructor used to deserialization to initialize a new instance of the class with the serialized property values
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> used to obtain the serialized properties of the object</param>
		/// <param name="context">Context for the deserialization</param>
		// AS 1/8/03 - fxcop
		// The "magic" serialization constructor should be protected if the class
		// is not sealed and private if the class is sealed.
		//
		//internal ColumnFilter(  SerializationInfo info, StreamingContext context )
		protected ColumnFilter(  SerializationInfo info, StreamingContext context )
		{			
			// Everything that was serialized in ISerializable.GetObjectData() 
			// is now loaded with this foreach loop
			//
			foreach( SerializationEntry entry in info )
			{
				switch ( entry.Name )
				{
					case "Tag":
						// AS 8/15/02
						// Use our tag deserializer.
						//
						//this.tagValue = entry.Value;
						this.DeserializeTag(entry);
						break;
					case "Key":
						//this.Key = (string)entry.Value;
						// AS 8/14/02
						// Use the disposable object method to allow deserialization from xml, etc.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.Key = (string)Utils.DeserializeProperty( entry, typeof(string), this.Key );
						//this.Key = (string)DisposableObject.ConvertValue( entry.Value, typeof(string) );
						break;
					case "LogicalOperator":
						//this.logicalOperator = (FilterLogicalOperator)(int)entry.Value;
						// AS 8/15/02
						// Use our enum serialization method to ensure that the
						// enum value is valid.
						//
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.logicalOperator = (FilterLogicalOperator)Utils.DeserializeProperty( entry, typeof(FilterLogicalOperator), this.logicalOperator );
						//this.logicalOperator = (FilterLogicalOperator)Utils.ConvertEnum(entry.Value, this.logicalOperator);
						break;
					case "Column":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.serializedColumnId = (SerializedColumnID)Utils.DeserializeProperty( entry, typeof(SerializedColumnID), null );
						//this.serializedColumnId = (SerializedColumnID)entry.Value;
						break;
					case "FilterConditions":
						// JJD 8/19/02
						// Use the DeserializeProperty static method to de-serialize properties instead.
						this.filterConditions = (FilterConditionsCollection)Utils.DeserializeProperty( entry, typeof(FilterConditionsCollection), null );
						//this.filterConditions = (FilterConditionsCollection)entry.Value;
						break;
					// SSP 10/23/07 BR25866
					// 
					case "displayTextOverride":
						this.displayTextOverride = (string)Utils.DeserializeProperty( entry, typeof( string ), null );
						break;
					default:
						Debug.Assert( false, "Invalid entry in ColumnFilter de-serialization ctor" );
						break;
				}
			}
		}

		#endregion // Constructor

		#region Public Properties

		#region Column

		/// <summary>
		/// Returns the column that will be used to get the associated value in
		/// rows when evaluating filter conditions.
		/// </summary>
		public UltraGridColumn Column
		{
			get
			{
				return this.column;
			}
		}

		#endregion // Column

		#region FilterConditions

		/// <summary>
		/// Filter conditions that will be evaluated when filtering rows.
		/// </summary>
		public FilterConditionsCollection FilterConditions
		{
			get
			{
				if ( null == this.filterConditions )
				{
					this.filterConditions = new FilterConditionsCollection( this );

					// Hook into the FilterConditionsCollection's SubObjectPropChanged event
					//
					this.filterConditions.SubObjectPropChanged += this.SubObjectPropChangeHandler;
				}

				return this.filterConditions;
			}
		}

		#endregion // FilterConditions
		
		#region LogicalOperator

		/// <summary>
		/// LogicalOperator that will be used for combining FilterConditions.
		/// </summary>
		public Infragistics.Win.UltraWinGrid.FilterLogicalOperator LogicalOperator
		{
			get
			{
				return this.logicalOperator;
			}
			set
			{
				if ( value != this.logicalOperator )
				{
					if ( !Enum.IsDefined( typeof( Infragistics.Win.UltraWinGrid.FilterLogicalOperator ), value ) )
						throw new InvalidEnumArgumentException( Shared.SR.GetString("LE_ArgumentException_104"), (int)value, typeof( Infragistics.Win.UltraWinGrid.FilterLogicalOperator ) );

					this.logicalOperator = value;

					this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.LogicalOperator );
				}
			}
		}

		#endregion // LogicalOperator

		#endregion // Public Properties

		#region Public Methods

		#region ClearFilterConditions

		// SSP 6/10/05 - NAS 5.2 Filter Row
		// Made ClearFilterConditions public.
		// 
		/// <summary>
		/// Clears filter conditions and also any value typed into the associated filter row cell.
		/// </summary>
		public void ClearFilterConditions( )
		{
			if ( null != this.filterConditions )
				this.filterConditions.Clear( );

			// SSP 6/10/05 - NAS 5.2 Filter Row
			// ClearFilterConditions should also clear out the contents of the cells in the 
			// filter row.
			// 
			// ----------------------------------------------------------------------------------
			// Clear out the filterRowOperandValue.
			// 
			this.filterRowOperandValue = null;

			// If a filter cell is in edit mode then we need to set the Value on the editor
			// to clear its contents.
			// 
			UltraGridFilterCell filterCell = null != this.Column && null != this.Column.Layout 
				? this.Column.Layout.ActiveCell as UltraGridFilterCell : null;
			if ( null != filterCell && filterCell.IsInEditMode && this == filterCell.ColumnFilter )
			{
				EmbeddableEditorBase editor = filterCell.EditorResolved;
				if ( null != editor && editor.IsInEditMode )
					editor.Value = DBNull.Value;
			}
			// ----------------------------------------------------------------------------------
		}
		
		#endregion // ClearFilterConditions

		#endregion // Public Methods

		#region Internal Properties/Methods

		#region InitializeFrom

		internal void InitializeFrom( ColumnFilter source )
		{
			if ( source.ShouldSerializeTag( ) )
				this.tagValue = source.tagValue;

			this.FilterConditions.InitializeFrom( source.FilterConditions );
			this.logicalOperator = source.logicalOperator;

			// SSP 10/23/07 BR25866
			// 
			this.displayTextOverride = source.displayTextOverride;
		}

		#endregion // InitializeFrom

		#region CanInitializeFrom

		internal bool CanInitializeFrom( ColumnFilter columnFilter )
		{
			UltraGridColumn column = columnFilter.column;

			if ( null != columnFilter.serializedColumnId &&
				columnFilter.serializedColumnId.Key.Length > 0 )
			{
				column = this.Column.Band.GetMatchingColumn( columnFilter.serializedColumnId );
			}

			if ( this.Column == column )
				return true;
				// SSP 12/16/02 UWG1892
				// If the column keys are the same, then return true.
				//
			else if ( null != column && null != this.Column && 				
				null != this.Column.Key && this.Column.Key.Equals( column.Key ) )
				return true;

			return false;
		}

		#endregion // CanInitializeFrom

		#region HasFilterConditions

		internal bool HasFilterConditions
		{
			get
			{
				return null != this.filterConditions && this.filterConditions.Count > 0;
			}
		}

		#endregion // HasFilterConditions
		
		#region DoesRowPassFilters

		internal bool DoesRowPassFilters( UltraGridRow row )
		{
			// If no filter conditions, then return true.
			//
			if ( !this.HasFilterConditions )
				return true;

			UltraGridColumn column = this.Column;

			Debug.Assert( null != column, "Null column !" );

			if ( null == column )
				return true;

			Debug.Assert( column.Band == row.Band, "Row has to belong to the same band as the one ColumnFilter is associated with." );

			if ( column.Band != row.Band )
				return true;

			
			if ( Infragistics.Win.UltraWinGrid.FilterLogicalOperator.And == this.LogicalOperator )
			{
				bool retValue = true;

				for ( int i = 0; i < this.FilterConditions.Count; i++ )
				{
					FilterCondition filterCondition = this.FilterConditions[i];

					bool passed = filterCondition.MeetsCriteria( row );

					retValue = retValue && passed;

					if ( !retValue )
						break;
				}

				return retValue;
			}
			else if ( Infragistics.Win.UltraWinGrid.FilterLogicalOperator.Or == this.LogicalOperator )
			{
				bool retValue = false;

				for ( int i = 0; i < this.FilterConditions.Count; i++ )
				{
					FilterCondition filterCondition = this.FilterConditions[i];

					bool passed = filterCondition.MeetsCriteria( row );

					retValue = retValue || passed;

					if ( retValue )
						break;
				}

				return retValue;
			}
			else
			{
				Debug.Assert( false, "Unknown LogicalOperator" );
				return true;
			}
		}

		#endregion // DoesRowPassFilters

		#region ShouldSerialize

		internal bool ShouldSerialize( )
		{
			return this.ShouldSerializeLogicalOperator( ) ||
				this.ShouldSerializeFilterConditions( );
		}

		#endregion // ShouldSerialize

		#region Reset

		internal void Reset( )
		{
			this.ResetLogicalOperator( );
			this.ResetFilterConditions( );						
		}

		#endregion // Reset

		#region ShouldSerializeLogicalOperator

		internal bool ShouldSerializeLogicalOperator( )
		{
			return FilterLogicalOperator.And != this.LogicalOperator;
		}

		#endregion // ShouldSerializeLogicalOperator

		#region ResetLogicalOperator
      
		internal void ResetLogicalOperator( )
		{
			this.LogicalOperator = FilterLogicalOperator.And;
		}

		#endregion // ResetLogicalOperator

		#region ShouldSerializeFilterConditions 
	
		internal bool ShouldSerializeFilterConditions( )
		{
			return null != this.filterConditions && this.filterConditions.Count > 0;
		}
	
		#endregion // ShouldSerializeFilterConditions

		#region ResetFilterConditions

		internal void ResetFilterConditions( )
		{
			if ( null != this.filterConditions )
				this.filterConditions.Clear( );
		}

		#endregion // ResetFilterConditions

		#region Clone

		// SSP 8/1/03
		// Added BeforeRowFilterChanged and AfterRowFilterChanged events.
		// We need to clone the column filter for BeforeRowFilterChanged event.
		//
        // MBS 2/26/09 - TFS14682
        // Made the method public so that we can always deal with a clone for the
        // FilterUIProvider
//#if DEBUG
		/// <summary>
		/// For Infragistics internal use only.  Clones this ColumnFilter instance and returns the clone.
		/// </summary>
		/// <returns></returns>
//#endif
        [EditorBrowsable(EditorBrowsableState.Never)]
		//internal ColumnFilter Clone( )
        public ColumnFilter Clone( )
		{
			ColumnFilter clone = new ColumnFilter( this.Column );
            clone.InitializeFrom( this );	
		
			return clone;
		}

		#endregion // Clone

		#region InternalEquals

		// SSP 4/18/05 - NAS 5.2 Filter Row
		//
		internal bool InternalEquals( ColumnFilter cf )
		{
			if ( this.LogicalOperator != cf.LogicalOperator )
				return false;

            // MRS 6/5/2008 - BR33648
            //if (this.FilterConditions.Count != cf.FilterConditions.Count)
            //    return false;

            //for (int i = 0; i < this.FilterConditions.Count; i++)
            //{
            //    if (!this.FilterConditions[i].InternalEquals(cf.FilterConditions[i]))
            //        return false;
            //}
            int thisFilterConditionCount = this.HasFilterConditions ? this.FilterConditions.Count : 0;
            int cfFilterConditionCount = cf.HasFilterConditions ? cf.FilterConditions.Count : 0;

            if (thisFilterConditionCount != cfFilterConditionCount)
                return false;

            for (int i = 0; i < thisFilterConditionCount; i++)
            {
                if (!this.FilterConditions[i].InternalEquals(cf.FilterConditions[i]))
                    return false;
            }

			return true;
		}

		#endregion // InternalEquals

		#region GetLogicalOperatorText
		
		// SSP 5/16/05 - NAS 5.2 Filter Row
		// Added GetLogicalOperatorText method.
		// 

		internal string GetLogicalOperatorText( )
		{
			return GetLogicalOperatorText( this.LogicalOperator );
		}

		internal static string GetLogicalOperatorText( FilterLogicalOperator logicalOperator )
		{
			string localizedName = FilterLogicalOperator.Or == logicalOperator
				? "RowFilterLogicalOperator_Or" : "RowFilterLogicalOperator_And";

			return SR.GetString( localizedName );
		}

		#endregion // GetLogicalOperatorText

		#region ClearEditModeInfo

		// SSP 5/24/06 BR12924
		// Added CleanEditModeInfo. Code in there is moved from the UltraGridFilterRow.ApplyFilters method.
		// 
		internal void ClearEditModeInfo( )
		{
			// Null out the operand value as well as reset the modified flags. Do not
			// null out the operator value because if operand is null and the user
			// has changed the operator we would end up losing the operator value. The
			// reason for it is that if the operand is null then we wouldn't have added
			// any filter condition above and thus GetOperatorValue would fallback to
			// using the default operator and the operator chosen by the user would get
			// lost.
			//
			this.filterRowOperandValue = null;
			this.filterOperandModifiedSinceLastApply = false;
			this.filterOperatorModifiedSinceLastApply = false;
		}

		#endregion // ClearEditModeInfo

		#endregion // Internal Properties

		#region Base Overrides

		#region OnSubObjectPropChanged

        /// <summary>
        /// Called when another sub object that we are listening to notifies
        /// us that one of its properties has changed.
        /// </summary>
        /// <param name="propChange">PropChangeInfo contains information on which subobject changed and which property of the subobject changed.</param>
		protected override void OnSubObjectPropChanged( PropChangeInfo propChange ) 
		{
			// SSP 10/23/07 BR25866
			// 
			this.displayTextOverride = null;

            // MBS 4/8/09 - TFS16470
            // If we've cleared all the filter conditions, we should clear our edit info too, otherwise the filter cel
            // value, and what is returned for the text, will become out-of-sync
            if ((PropertyIds)propChange.PropId == PropertyIds.Clear)
                this.ClearEditModeInfo();

			// Notify the listeners (ColumnFiltersCollection in this case) that the
			// ColumnFilter has changed.
			//
			this.NotifyPropChange( Infragistics.Win.UltraWinGrid.PropertyIds.ColumnFilter );
		}

		#endregion // OnSubObjectPropChanged
	
		#region ISerializable.GetObjectData

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.SerializationFormatter )]
		void ISerializable.GetObjectData( SerializationInfo info, StreamingContext context )
		{
			// AS 11/24/03 WTR524
			// Setting the assemblyname to a simple name when we
			// are not the ones performing the deserialization will
			// cause a problem because the deserializer will not be
			// able to locate the assembly/type to create.
			//
			//info.AssemblyName = "Infragistics.Win.UltraWinGrid";

			// Serialize the tag.
			//
			// AS 8/15/02
			// Use our serialization method since
			// we want to limit what gets serialized.
			//
			//if ( this.ShouldSerializeTag() )
			//	info.AddValue("Tag", this.Tag );
			this.SerializeTag(info);

			// Always serialize the key.
			//
			// JJD 8/20/02
			// Use SerializeProperty static method to serialize properties instead.
			Utils.SerializeProperty( info, "Key", this.Key );
			//info.AddValue( "Key", this.Key );

			if ( this.ShouldSerializeLogicalOperator( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "LogicalOperator", this.LogicalOperator );
				//info.AddValue( "LogicalOperator", (int)this.LogicalOperator );
			}

			// SSP 10/23/07 BR25866
			// 
			if ( !string.IsNullOrEmpty( this.displayTextOverride ) )
			{
				Utils.SerializeProperty( info, "displayTextOverride", this.displayTextOverride );
			}

			// Serialize the column
			//
			if ( null != this.Column )
			{
				SerializedColumnID colid = this.Column.Band.GetSerializedColumnId( this.Column );

				Debug.Assert( null != colid, "GetSerializedColumnId returned null." );
				if ( null != colid )
				{
					// JJD 8/20/02
					// Use SerializeProperty static method to serialize properties instead.
					Utils.SerializeProperty( info, "Column", colid );
					//info.AddValue( "Column", colid );
				}
			}
			else if ( null != this.serializedColumnId )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "Column", this.serializedColumnId );
				//info.AddValue( "Column", this.serializedColumnId );
			}
			else
				Debug.Assert( false, "No column !" );

			if ( this.ShouldSerializeFilterConditions( ) )
			{
				// JJD 8/20/02
				// Use SerializeProperty static method to serialize properties instead.
				Utils.SerializeProperty( info, "FilterConditions", this.FilterConditions );
				//info.AddValue( "FilterConditions", this.FilterConditions );
			}
		}

		#endregion // ISerializable.GetObjectData

		#region ToString

		// SSP 5/16/05 - NAS 5.2 Filter Row
		// Overrode ToString method. This is used to generate the filter cell tooltip.
		//
		/// <summary>
		/// Overridden. Returns the string representation of this filter condition.
		/// </summary>
		/// <returns></returns>
		public override string ToString( )
		{
			// SSP 10/23/07 BR25866
			// If displayTextOverride is set then return that.
			// 
			if ( !string.IsNullOrEmpty( this.displayTextOverride ) )
				return this.displayTextOverride;

			const char SPACE = ' ';
			FilterConditionsCollection conditions = this.FilterConditions;
			StringBuilder sb = new StringBuilder( );
			for ( int i = 0; i < conditions.Count; i++ )
			{
				FilterCondition fc = conditions[ i ];
				if ( null != fc )
				{
					if ( i > 0 )
						sb.Append( SPACE ).Append( this.GetLogicalOperatorText( ) ).Append( SPACE );

					fc.ToString( sb, false );
				}
			}

			return sb.ToString( );
		}

		#endregion // ToString

		#endregion // Base Overrides
	}

}
