#region Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved
/* ---------------------------------------------------------------------*
*                           Infragistics, Inc.                          *
*              Copyright (c) 2001-2009 All Rights reserved               *
*                                                                       *
*                                                                       *
* This file and its contents are protected by United States and         *
* International copyright laws.  Unauthorized reproduction and/or       *
* distribution of all or any portion of the code contained herein       *
* is strictly prohibited and will result in severe civil and criminal   *
* penalties.  Any violations of this copyright will be prosecuted       *
* to the fullest extent possible under law.                             *
*                                                                       *
* THE SOURCE CODE CONTAINED HEREIN AND IN RELATED FILES IS PROVIDED     *
* TO THE REGISTERED DEVELOPER FOR THE PURPOSES OF EDUCATION AND         *
* TROUBLESHOOTING. UNDER NO CIRCUMSTANCES MAY ANY PORTION OF THE SOURCE *
* CODE BE DISTRIBUTED, DISCLOSED OR OTHERWISE MADE AVAILABLE TO ANY     *
* THIRD PARTY WITHOUT THE EXPRESS WRITTEN CONSENT OF INFRAGISTICS, INC. *
*                                                                       *
* UNDER NO CIRCUMSTANCES MAY THE SOURCE CODE BE USED IN WHOLE OR IN     *
* PART, AS THE BASIS FOR CREATING A PRODUCT THAT PROVIDES THE SAME, OR  *
* SUBSTANTIALLY THE SAME, FUNCTIONALITY AS ANY INFRAGISTICS PRODUCT.    *
*                                                                       *
* THE REGISTERED DEVELOPER ACKNOWLEDGES THAT THIS SOURCE CODE           *
* CONTAINS VALUABLE AND PROPRIETARY TRADE SECRETS OF INFRAGISTICS,      *
* INC.  THE REGISTERED DEVELOPER AGREES TO EXPEND EVERY EFFORT TO       *
* INSURE ITS CONFIDENTIALITY.                                           *
*                                                                       *
* THE END USER LICENSE AGREEMENT (EULA) ACCOMPANYING THE PRODUCT        *
* PERMITS THE REGISTERED DEVELOPER TO REDISTRIBUTE THE PRODUCT IN       *
* EXECUTABLE FORM ONLY IN SUPPORT OF APPLICATIONS WRITTEN USING         *
* THE PRODUCT.  IT DOES NOT PROVIDE ANY RIGHTS REGARDING THE            *
* SOURCE CODE CONTAINED HEREIN.                                         *
*                                                                       *
* THIS COPYRIGHT NOTICE MAY NOT BE REMOVED FROM THIS FILE.              *
* --------------------------------------------------------------------- *
*/
#endregion Copyright (c) 2001-2009 Infragistics, Inc. All Rights Reserved

using System;
using System.Security.Permissions;

namespace Infragistics.Win.UltraWinGrid.Serialization
{
	// JJD 8/14/02
	// Added binder class to handle de-serialization for v2 since the
	// assembly name doesn't have ".v3" in it.
	/// <summary>
	/// Controls class loading and mandates what class to load.
	/// </summary>
	public class Binder : Infragistics.Win.Serialization.Binder
	{
		/// <summary>
		/// Controls the binding of a serialized object to a type
		/// </summary>
		/// <param name="assemblyName">Specifies the Assembly name of the serialized object</param>
		/// <param name="typeName">Specifies the Type name of the serialized object.</param>
		/// <returns>The type of the object the formatter creates a new instance of.</returns>
		// AS 1/28/03 - Added SecurityPermission attribute to avoid fxcop violation
		[ SecurityPermission( SecurityAction.Demand, SerializationFormatter = true ) ]
		public override Type BindToType ( string assemblyName, string typeName) 
		{
			// If the assembly name doesn't match this assembly then
			// call the base class implementation
			// AS 11/24/03 WTR524
			// Since we are no longer storing the simple name we can't
			// compare the assemblyname so check if it starts with what
			// we expect instead.
			//
			//if ( assemblyName != "Infragistics.Win.UltraWinGrid" )
			if ( assemblyName == null || !assemblyName.StartsWith("Infragistics.Win.UltraWinGrid") )
				return base.BindToType( assemblyName, typeName );

			Type typeToDeserialize = null;

			try
			{
				// Use reflection to get the fully qualified assembly name
				// from a type in this assembly
				assemblyName = typeof(Infragistics.Win.UltraWinGrid.UltraGridBase).Assembly.FullName;
			}
			catch
			{
				// Just in case we didn't have reflection rights for the call above
				// AS 11/24/03 WTR524
				// Since we are no longer storing the simple name we can't
				// just append the version number. Put the assembly name
				// we expect instead.
				//
				//assemblyName += ".v3";
				// AS 6/17/04 Moved to assemblyinfo.cs to make it easier to manage.
				//assemblyName = "Infragistics.Win.UltraWinGrid.v5.2";
				assemblyName = AssemblyRef.BinderAssemblyName;
			}

			// Call get type 
			typeToDeserialize = Type.GetType(String.Format("{0}, {1}", 
												typeName, assemblyName));

			// AS 1/13/03 WTB552
			// If we cannot locate a type, it could be because the type
			// is actually a subclass of one of our objects. In this case,
			// delegate to the base class so it can locate the type.
			//
			if (typeToDeserialize == null)
				return base.BindToType( assemblyName, typeName );

			return typeToDeserialize;
		}
	}
}
